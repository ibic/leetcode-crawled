---
title: "Find All Numbers Disappeared in an Array"
weight: 425
#id: "find-all-numbers-disappeared-in-an-array"
---
## Description
<div class="description">
<p>Given an array of integers where 1 &le; a[i] &le; <i>n</i> (<i>n</i> = size of array), some elements appear twice and others appear once.</p>

<p>Find all the elements of [1, <i>n</i>] inclusive that do not appear in this array.</p>

<p>Could you do it without extra space and in O(<i>n</i>) runtime? You may assume the returned list does not count as extra space.</p>

<p><b>Example:</b>
<pre>
<b>Input:</b>
[4,3,2,7,8,2,3,1]

<b>Output:</b>
[5,6]
</pre>
</p>
</div>

## Tags
- Array (array)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

#### Approach 1: Using Hash Map

**Intuition**

The intuition behind using a hash map is pretty clear in this case. We are given that the array would be of size `N` and it should contain numbers from `1` to `N`. However, some of the numbers are missing. All we have to do is keep track of which numbers we encounter in the array and then iterate from $$1 \cdots N$$ and check which numbers did not appear in the hash table. Those will be our missing numbers. Let's look at a formal algorithm based on this idea and then an animation explaining the same with the help of a simple example.

**Algorithm**

1. Initialize a hash map, `hash` to keep track of the numbers that we encounter in the array. Note that we can use a `set` data structure as well in this case since we are not concerned about the frequency counts of elements.

    <center>
    <img src="../Figures/448/anim1.png" width="700"/>
    </center>
    
    > Note that for the purposes of illustration, we have use a hash map of size 14 and have ordered the keys of the hash map from 0 to 14. Also, we will be using a simple hash function that directly maps the array entries to their corresponding keys in the hash map. Usually, the mapping is not this simple and is dependent upon the hash function being used in the implementation of the hash map. 
  
2. Next, iterate over the given array one element at a time and for each element, insert an entry in the hash map. Even if an entry were to exist before in the hash map, it will simply be over-written. For the above example, let's look at the final state of the hash map once we process the last element of the array.

    <center>
    <img src="../Figures/448/anim9.png" width="700"/>
    </center>

3. Now that we know the `unique` set of elements from the array, we can simply find out the missing elements from the range $$1 \cdots N$$.
4. Iterate over all the numbers from $$1 \cdots N$$ and for each number, check if there's an entry in the hash map. If there is no entry, add that missing number to a result array that we will return from the function eventually. 

<center>

!?!../Documents/448_Disappeared_Nums.json:600,413!?!

</center>

<iframe src="https://leetcode.com/playground/q4gopBpB/shared" frameBorder="0" width="100%" height="500" name="q4gopBpB"></iframe>


**Complexity Analysis**

* Time Complexity : $$O(N)$$
* Space Complexity : $$O(N)$$
<br/>
<br/>

---

#### Approach 2: O(1) Space InPlace Modification Solution

**Intuition**

We definitely need to keep track of all the `unique` numbers that appear in the array. However, we don't want to use any extra space for it. This solution that we will look at in just a moment springs from the fact that

> All the elements are in the range [1, N]

Since we are given this information, we can make use of the input array itself to somehow `mark visited` numbers and then find our missing numbers. Now, we don't want to change the actual data in the array but who's stopping us from changing the `magnitude` of numbers in the array? That is the basic idea behind this algorithm. 

> We will be negating the numbers seen in the array and use the sign of each of the numbers for finding our missing numbers. We will be treating numbers in the array as indices and mark corresponding locations in the array as negative.

**Algorithm**

1. Iterate over the input array one element at a time.
2. For each element `nums[i]`, mark the element at the corresponding location negative if it's not already marked so i.e. $$nums[\; nums[i] \;- 1\;] \times -1$$ .
3. Now, loop over numbers from $$1 \cdots N$$ and for each number check if `nums[j]` is negative. If it is negative, that means we've seen this number somewhere in the array. 
4. Add all the numbers to the resultant array which don't have their corresponding locations marked as negative in the original array.

<center>

!?!../Documents/448_Disappeared_Nums_2.json:600,302!?!

</center>

<iframe src="https://leetcode.com/playground/JTepYPe7/shared" frameBorder="0" width="100%" height="500" name="JTepYPe7"></iframe>

**Complexity Analysis**

* Time Complexity : $$O(N)$$
* Space Complexity : $$O(1)$$ since we are reusing the input array itself as a hash table and the space occupied by the output array doesn't count toward the space complexity of the algorithm. 
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python 4 lines with short explanation
- Author: leetcodeftw
- Creation Date: Mon Nov 21 2016 03:56:34 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 05:05:02 GMT+0800 (Singapore Standard Time)

<p>
For each number i in nums,
we mark the number that i points as negative.
Then we filter the list, get all the indexes
who points to a positive number.
Since those indexes are not visited.

```
class Solution(object):
    def findDisappearedNumbers(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        # For each number i in nums,
        # we mark the number that i points as negative.
        # Then we filter the list, get all the indexes
        # who points to a positive number
        for i in xrange(len(nums)):
            index = abs(nums[i]) - 1
            nums[index] = - abs(nums[index])

        return [i + 1 for i in range(len(nums)) if nums[i] > 0]

```
</p>


### c++ solution O(1) space
- Author: RushRab
- Creation Date: Wed Nov 02 2016 11:54:28 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 11:52:09 GMT+0800 (Singapore Standard Time)

<p>
The idea is very similar to problem 442. Find All Duplicates in an Array: https://leetcode.com/problems/find-all-duplicates-in-an-array/.

First iteration to negate values at position whose equal to values appear in array. Second iteration to collect all position whose value is positive, which are the missing values. Complexity is O(n) Time and O(1) space.
```
class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {
        int len = nums.size();
        for(int i=0; i<len; i++) {
            int m = abs(nums[i])-1; // index start from 0
            nums[m] = nums[m]>0 ? -nums[m] : nums[m];
        }
        vector<int> res;
        for(int i = 0; i<len; i++) {
            if(nums[i] > 0) res.push_back(i+1);
        }
        return res;
    }
};
```
</p>


### 5-line Java Easy-understanding
- Author: hu19
- Creation Date: Thu Nov 03 2016 05:43:24 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 05:04:47 GMT+0800 (Singapore Standard Time)

<p>
```
public List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> res = new ArrayList<>();
        int n = nums.length;
        for (int i = 0; i < nums.length; i ++) nums[(nums[i]-1) % n] += n;
        for (int i = 0; i < nums.length; i ++) if (nums[i] <= n) res.add(i+1);
        return res;
    }
````
</p>


