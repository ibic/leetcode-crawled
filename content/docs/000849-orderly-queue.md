---
title: "Orderly Queue"
weight: 849
#id: "orderly-queue"
---
## Description
<div class="description">
<p>A string <code>S</code> of lowercase letters is given.&nbsp; Then, we may make any number of <em>moves</em>.</p>

<p>In each move, we&nbsp;choose one&nbsp;of the first <code>K</code> letters (starting from the left), remove it,&nbsp;and place it at the end of the string.</p>

<p>Return the lexicographically smallest string we could have after any number of moves.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-1-1">&quot;cba&quot;</span>, K = <span id="example-input-1-2">1</span>
<strong>Output: </strong><span id="example-output-1">&quot;acb&quot;</span>
<strong>Explanation: </strong>
In the first move, we move the 1st character (&quot;c&quot;) to the end, obtaining the string &quot;bac&quot;.
In the second move, we move the 1st character (&quot;b&quot;) to the end, obtaining the final result &quot;acb&quot;.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-2-1">&quot;baaca&quot;</span>, K = <span id="example-input-2-2">3</span>
<strong>Output: </strong><span id="example-output-2">&quot;aaabc&quot;</span>
<strong>Explanation: </strong>
In the first move, we move the 1st character (&quot;b&quot;) to the end, obtaining the string &quot;aacab&quot;.
In the second move, we move the 3rd character (&quot;c&quot;) to the end, obtaining the final result &quot;aaabc&quot;.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= K &lt;= S.length&nbsp;&lt;= 1000</code></li>
	<li><code>S</code>&nbsp;consists of lowercase letters only.</li>
</ol>
</div>
</div>

</div>

## Tags
- Math (math)
- String (string)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Mathematical

**Intuition**

Call the move that takes the `K`th letter from the beginning and puts it on the end, a "*`K`-kick*" move.

Examining 1-kick moves, they let us consider the string as a "necklace" that may be rotated freely, where each bead of the necklace corresponds to a letter in the string.  (Formally, this is the equivalence class under 1-kick moves.)

Examining 2-kick moves (in the context of treating the string as a necklace), they allow us to swap the positions of two adjacent beads.  Thus, with 2-kick moves, every permutation of necklace is possible.  (To actually construct the necklace, we bring the second smallest bead to be after the smallest, then the third smallest to be after the second smallest, and so on.)

The previous insight may be difficult to find.  Another strategy is to write a brute force program to examine the result of 2-kick moves - then we might notice that 2-kick moves allow any permutation of the string.

Yet another strategy might be to explicitly construct new moves based on previous moves.  If we perform a 2 kick move followed by many 1 kick moves, we can move a string like `"xyzzzzzz" -> "xzzzzzzy" -> "yxzzzzzz"`, proving we can swap the positions of any two adjacent letters.

**Algorithm**

If `K = 1`, only rotations of `S` are possible, and the answer is the smallest rotation.

If `K > 1`, any permutation of `S` is possible, and the answer is the letters of `S` written in lexicographic order.

<iframe src="https://leetcode.com/playground/hXTUkSCe/shared" frameBorder="0" width="100%" height="327" name="hXTUkSCe"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `S`.

* Space Complexity:  $$O(N^2)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Sort String or Rotate String
- Author: lee215
- Creation Date: Sun Sep 02 2018 11:08:51 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 13:04:01 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**:
First, this is string rotation.
`12345` -> `23451` -> `34512` -> `45123` -> `51234`
I use number instead of letters to make it clear.

If `K == 1`, we can only rotate the whole string.
There are `S.length` different states and
we return the lexicographically smallest string.

If `K > 1`, it means we can:
1. rotate the whole string,
2. rotate the whole string except the first letter.
`012345` -> `023451` -> `034512` -> `045123` -> `051234`

We can rotate `i+1`th big letter to the start (method 1),
then rotate `i`th big letter to the end (method 2).
`2XXX01` -> `XXX012`

In this way, we can bubble sort the whole string lexicographically.
So just return sorted string.

**C++:**
```
    string orderlyQueue(string S, int K) {
        if (K > 1) {
            sort(S.begin(), S.end());
            return S;
        }
        string res = S;
        for (int i = 1; i < S.length(); i++)
            res = min(res, S.substr(i) + S.substr(0, i));
        return res;
    }
```

**Java:**
```
    public String orderlyQueue(String S, int K) {
        if (K > 1) {
            char S2[] = S.toCharArray();
            Arrays.sort(S2);
            return new String(S2);
        }
        String res = S;
        for (int i = 1; i < S.length(); i++) {
            String tmp = S.substring(i) + S.substring(0, i);
            if (res.compareTo(tmp) > 0) res = tmp;
        }
        return res;
    }
```
**Python:**
```
    def orderlyQueue(self, S, K):
        return "".join(sorted(S)) if K > 1 else min(S[i:] + S[:i] for i in range(len(S)))
```

</p>


### K>1 is bubblesort
- Author: AmekiKyou
- Creation Date: Sun Sep 02 2018 11:03:13 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 05 2018 12:42:44 GMT+0800 (Singapore Standard Time)

<p>
Remind what bubble sort eventually is: **swap pairs**

So, you have a buffer of at least 2 when K>1
you can put them back into the queue in different order: swap!

So, K>1 equals bubble sort

```
class Solution {
public:
    string orderlyQueue(string S, int K) {
        if (K>1){
            sort(S.begin(),S.end());
            return S;
        }
        string minS=S;
        for (int i=0;i<S.size();++i){
            S=S.substr(1)+S.substr(0,1);
            minS=min(S,minS);
        }
        return minS;
    }
};
```
</p>


### When k > 1 you can reorder any way you like [Proof]
- Author: yuxiangmusic
- Creation Date: Sun Sep 02 2018 11:46:38 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 14 2018 12:03:32 GMT+0800 (Singapore Standard Time)

<p>
For any String you can move any char to anywhere if you can swap adjacent characters
For any String `xxx[ab]xxx` you can always do

1. `xxx[ab]xxx`
2. `[ab]xxxxxx`
3. `xxxxxx[ba]`
4. `xxx[ba]xxx`

```
    public String orderlyQueue(String s, int k) {
        if (k > 1) {
            char[] arr = s.toCharArray();
            Arrays.sort(arr);
            return String.valueOf(arr);
        } else {
            String min = s;
            for (int i = 0; i < s.length(); i++) {
                s = s.substring(1) + s.charAt(0);
                min = min.compareTo(s) < 0 ? min : s;
            }
            return min;
        }
    }
```
</p>


