---
title: "Teemo Attacking"
weight: 469
#id: "teemo-attacking"
---
## Description
<div class="description">
<p>In LOL world, there is a hero called Teemo and his attacking can make his enemy Ashe be in poisoned condition. Now, given the Teemo&#39;s attacking <b>ascending</b> time series towards Ashe and the poisoning time duration per Teemo&#39;s attacking, you need to output the total time that Ashe is in poisoned condition.</p>

<p>You may assume that Teemo attacks at the very beginning of a specific time point, and makes Ashe be in poisoned condition immediately.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> [1,4], 2
<b>Output:</b> 4
<b>Explanation:</b> At time point 1, Teemo starts attacking Ashe and makes Ashe be poisoned immediately. 
This poisoned status will last 2 seconds until the end of time point 2. 
And at time point 4, Teemo attacks Ashe again, and causes Ashe to be in poisoned status for another 2 seconds. 
So you finally need to output 4.
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> [1,2], 2
<b>Output:</b> 3
<b>Explanation:</b> At time point 1, Teemo starts attacking Ashe and makes Ashe be poisoned. 
This poisoned status will last 2 seconds until the end of time point 2. 
However, at the beginning of time point 2, Teemo attacks Ashe again who is already in poisoned status. 
Since the poisoned status won&#39;t add up together, though the second poisoning attack will still work at time point 2, it will stop at the end of time point 3. 
So you finally need to output 3.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>You may assume the length of given time series array won&#39;t exceed 10000.</li>
	<li>You may assume the numbers in the Teemo&#39;s attacking time series and his poisoning time duration per attacking are non-negative integers, which won&#39;t exceed 10,000,000.</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags
- Array (array)

## Companies
- Riot Games - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: One pass

**Intuition**

The problem is an example of merge interval questions which are now [quite popular
in Google](https://leetcode.com/discuss/interview-question/280433/Google-or-Phone-screen-or-Program-scheduling).

Typically such problems could be solved in a linear time in the case
of sorted input, like [here](https://leetcode.com/articles/insert-interval/), 
and in $$\mathcal{O}(N \log N)$$ time otherwise,
[here is an example](https://leetcode.com/articles/merge-intervals/).

Here one deals with a sorted input, and the problem could be solved in one pass
with a constant space. 
The idea is straightforward: consider only the interval between two attacks. 
Ashe spends in a poisoned condition the whole time interval
if this interval is shorter than the poisoning time duration `duration`, 
and `duration` otherwise. 

**Algorithm**

- Initiate total time in poisoned condition `total = 0`.

- Iterate over `timeSeries` list. At each step add to the total time
the minimum between interval length and the poisoning time duration `duration`. 

- Return `total + duration` to take the last attack into account.  
 
**Implementation**

![pic](../Figures/495/ashe.png)

<iframe src="https://leetcode.com/playground/vRimYnRb/shared" frameBorder="0" width="100%" height="242" name="vRimYnRb"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$, where N is the length of the input list,
since we iterate the entire list.

* Space complexity : $$\mathcal{O}(1)$$, it's a constant space solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python Solution for Teemo
- Author: lilixu
- Creation Date: Tue Jan 31 2017 23:45:55 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 08:01:37 GMT+0800 (Singapore Standard Time)

<p>
![alt text](https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSa-yd3K7uW2ocdpJ-7mA3kMyF-XLUjkVproRQxIzhrRzh_osOW)

    class Solution(object):
        def findPoisonedDuration(self, timeSeries, duration):
            ans = duration * len(timeSeries)
            for i in range(1,len(timeSeries)):
                ans -= max(0, duration - (timeSeries[i] - timeSeries[i-1]))
            return ans

![alt text](https://www.baronsteal.net/images/champion/loading/Ashe_4.jpg)
</p>


### O(n) Java Solution using same idea of merge intervals
- Author: shawngao
- Creation Date: Sun Jan 29 2017 12:16:44 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 28 2018 15:15:41 GMT+0800 (Singapore Standard Time)

<p>
The same idea as https://leetcode.com/problems/merge-intervals/
Algorithm:
1. Use two variable to record current start and end point.
2. If the start of new interval is greater than current end, meaning NO overlapping, we can sum the current interval length to result and then update start and end.
3. Otherwise just update the current end;

```
public class Solution {
    public int findPosisonedDuration(int[] timeSeries, int duration) {
        if (timeSeries == null || timeSeries.length == 0 || duration == 0) return 0;
        
        int result = 0, start = timeSeries[0], end = timeSeries[0] + duration;
        for (int i = 1; i < timeSeries.length; i++) {
            if (timeSeries[i] > end) {
                result += end - start;
                start = timeSeries[i];
            }
            end = timeSeries[i] + duration;
        }
        result += end - start;
        
        return result;
    }
}
```
</p>


