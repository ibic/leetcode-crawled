---
title: "Tree Node"
weight: 1501
#id: "tree-node"
---
## Description
<div class="description">
<p>Given a table <code>tree</code>, <b>id</b> is identifier of the tree node and <b>p_id</b> is its parent node&#39;s <b>id</b>.</p>

<pre>
+----+------+
| id | p_id |
+----+------+
| 1  | null |
| 2  | 1    |
| 3  | 1    |
| 4  | 2    |
| 5  | 2    |
+----+------+
</pre>
Each node in the tree can be one of three types:

<ul>
	<li>Leaf: if the node is a leaf node.</li>
	<li>Root: if the node is the root of the tree.</li>
	<li>Inner: If the node is neither a leaf node nor a root node.</li>
</ul>

<p>&nbsp;</p>
Write a query to print the node id and the type of the node. Sort your output by the node id. The result for the above sample is:

<p>&nbsp;</p>

<pre>
+----+------+
| id | Type |
+----+------+
| 1  | Root |
| 2  | Inner|
| 3  | Leaf |
| 4  | Leaf |
| 5  | Leaf |
+----+------+
</pre>

<p>&nbsp;</p>

<p><b>Explanation</b></p>

<p>&nbsp;</p>

<ul>
	<li>Node &#39;1&#39; is root node, because its parent node is NULL and it has child node &#39;2&#39; and &#39;3&#39;.</li>
	<li>Node &#39;2&#39; is inner node, because it has parent node &#39;1&#39; and child node &#39;4&#39; and &#39;5&#39;.</li>
	<li>Node &#39;3&#39;, &#39;4&#39; and &#39;5&#39; is Leaf node, because they have parent node and they don&#39;t have child node.</li>
	<br />
	<li>And here is the image of the sample tree as below:
	<p>&nbsp;</p>

	<pre>
			  1
			/   \
                      2       3
                    /   \
                  4       5
</pre>

	<p><b>Note</b></p>

	<p>If there is only one node on the tree, you only need to output its root attributes.</p>
	</li>
</ul>

</div>

## Tags


## Companies
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---

#### Approach I: Using `UNION` [Accepted]

**Intuition**

We can print the node type by judging every record by its definition in this table.
* Root: it does not have a parent node at all
* Inner: it is the parent node of some nodes, and it has a not NULL parent itself.
* Leaf: rest of the cases other than above two

**Algorithm**

By transiting the node type definition, we can have the following code.

For the root node, it does not have a parent.
```sql
SELECT
    id, 'Root' AS Type
FROM
    tree
WHERE
    p_id IS NULL
```

For the leaf nodes, they do not have any children, and it has a parent.
```sql
SELECT
    id, 'Leaf' AS Type
FROM
    tree
WHERE
    id NOT IN (SELECT DISTINCT
            p_id
        FROM
            tree
        WHERE
            p_id IS NOT NULL)
        AND p_id IS NOT NULL
```

For the inner nodes, they have have some children and a parent.
```sql
SELECT
    id, 'Inner' AS Type
FROM
    tree
WHERE
    id IN (SELECT DISTINCT
            p_id
        FROM
            tree
        WHERE
            p_id IS NOT NULL)
        AND p_id IS NOT NULL
```
So, one solution to the problem is to combine these cases together using `UNION`.

**MySQL**

```sql
SELECT
    id, 'Root' AS Type
FROM
    tree
WHERE
    p_id IS NULL

UNION

SELECT
    id, 'Leaf' AS Type
FROM
    tree
WHERE
    id NOT IN (SELECT DISTINCT
            p_id
        FROM
            tree
        WHERE
            p_id IS NOT NULL)
        AND p_id IS NOT NULL

UNION

SELECT
    id, 'Inner' AS Type
FROM
    tree
WHERE
    id IN (SELECT DISTINCT
            p_id
        FROM
            tree
        WHERE
            p_id IS NOT NULL)
        AND p_id IS NOT NULL
ORDER BY id;
```

#### Approach II: Using flow control statement `CASE` [Accepted]

**Algorithm**

The idea is similar with the above solution but the code is simpler by utilizing the flow control statements, which is effective to output differently based on different input values. In this case, we can use [`CASE`](https://dev.mysql.com/doc/refman/5.7/en/case.html) statement.

**MySQL**

```sql
SELECT
    id AS `Id`,
    CASE
        WHEN tree.id = (SELECT atree.id FROM tree atree WHERE atree.p_id IS NULL)
          THEN 'Root'
        WHEN tree.id IN (SELECT atree.p_id FROM tree atree)
          THEN 'Inner'
        ELSE 'Leaf'
    END AS Type
FROM
    tree
ORDER BY `Id`
;
```
>MySQL provides different flow control statements besides `CASE`. You can try to rewrite the slution above using [`IF`](https://dev.mysql.com/doc/refman/5.7/en/control-flow-functions.html#function_if) flow control statement.

#### Approach III: Using `IF` function [Accepted]

**Algorithm**

Also, we can use a single [`IF`](https://dev.mysql.com/doc/refman/5.7/en/control-flow-functions.html#function_if) function instead of the complex flow control statements.

**MySQL**

```sql
SELECT
    atree.id,
    IF(ISNULL(atree.p_id),
        'Root',
        IF(atree.id IN (SELECT p_id FROM tree), 'Inner','Leaf')) Type
FROM
    tree atree
ORDER BY atree.id
```
>Note: This solution was inspired by [@richarddia](https://discuss.leetcode.com/user/richarddia)

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Solution with explanation: CASE + LEFT JOIN
- Author: tinalxj12
- Creation Date: Thu Jul 06 2017 00:19:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 15 2018 11:15:03 GMT+0800 (Singapore Standard Time)

<p>
![0_1499271537059_2c061b42-ed70-421a-8bd2-88f20a257d6d-image.png](/assets/uploads/files/1499271536894-2c061b42-ed70-421a-8bd2-88f20a257d6d-image.png) 

````
SELECT DISTINCT t1.id, (
    CASE
    WHEN t1.p_id IS NULL  THEN 'Root'
    WHEN t1.p_id IS NOT NULL AND t2.id IS NOT NULL THEN 'Inner'
    WHEN t1.p_id IS NOT NULL AND t2.id IS NULL THEN 'Leaf'
    END
) AS Type 
FROM tree t1
LEFT JOIN tree t2
ON t1.id = t2.p_id
````
</p>


### AC Solution
- Author: richarddia
- Creation Date: Sun Jun 04 2017 14:44:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 07:41:44 GMT+0800 (Singapore Standard Time)

<p>
```
select T.id, 
IF(isnull(T.p_id), 'Root', IF(T.id in (select p_id from tree), 'Inner', 'Leaf')) Type 
from tree T
```
</p>


### Simple answer can be used both for Ms sql & Mysql, faster than 90%, don't need join
- Author: CarolineHao_0422
- Creation Date: Fri May 01 2020 06:22:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 01 2020 06:22:18 GMT+0800 (Singapore Standard Time)

<p>
SELECT
       id,
      CASE WHEN p_id is null THEN \'Root\'
           WHEN id IN (SELECT DISTINCT p_id FROM tree) AND p_id is NOT null THEN \'Inner\'
           ELSE \'Leaf\' END AS Type
FROM tree
</p>


