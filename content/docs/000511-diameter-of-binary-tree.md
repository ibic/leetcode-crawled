---
title: "Diameter of Binary Tree"
weight: 511
#id: "diameter-of-binary-tree"
---
## Description
<div class="description">
<p>
Given a binary tree, you need to compute the length of the diameter of the tree. The diameter of a binary tree is the length of the <b>longest</b> path between any two nodes in a tree. This path may or may not pass through the root.
</p>

<p>
<b>Example:</b><br />
Given a binary tree <br />
<pre>
          1
         / \
        2   3
       / \     
      4   5    
</pre>
</p>
<p>
Return <b>3</b>, which is the length of the path [4,2,1,3] or [5,2,1,3].
</p>

<p><b>Note:</b>
The length of path between two nodes is represented by the number of edges between them.
</p>
</div>

## Tags
- Tree (tree)

## Companies
- Facebook - 31 (taggedByAdmin: true)
- Amazon - 10 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: true)
- ByteDance - 4 (taggedByAdmin: false)
- Deutsche Bank - 2 (taggedByAdmin: false)
- Microsoft - 7 (taggedByAdmin: false)
- Oracle - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Tencent - 2 (taggedByAdmin: false)
- Intel - 2 (taggedByAdmin: false)
- Qualtrics - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Depth-First Search [Accepted]

**Intuition**

Any path can be written as two *arrows* (in different directions) from some node, where an arrow is a path that starts at some node and only travels down to child nodes.

If we knew the maximum length arrows `L, R` for each child, then the best path touches `L + R + 1` nodes.

**Algorithm**

Let's calculate the depth of a node in the usual way: max(depth of node.left, depth of node.right) + 1. While we do, a path "through" this node uses 1 + (depth of node.left) + (depth of node.right) nodes. Let's search each node and remember the highest number of nodes used in some path. The desired length is 1 minus this number.

<iframe src="https://leetcode.com/playground/6X2NvWFQ/shared" frameBorder="0" width="100%" height="310" name="6X2NvWFQ"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$.  We visit every node once.

* Space Complexity: $$O(N)$$, the size of our implicit call stack during our depth-first search.

## Accepted Submission (python3)
```python3
from functools import wraps

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

def cached(func):
    cache = {}
    def decorate(self, node):
        if node in cache:
            return cache[node]
        else:
            result = func(self, node)
            cache[node] = result
            return result
    return decorate

class Solution:
    @cached
    def depth(self, root):
        if not root:
            return -1
        return max(self.depth(root.left), self.depth(root.right)) + 1

    def diameterOfBinaryTree(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0
        leftDepth = self.depth(root.left)
        rightDepth = self.depth(root.right)
        diameter = leftDepth + rightDepth + 2
        leftDiameter = self.diameterOfBinaryTree(root.left)
        rightDiameter = self.diameterOfBinaryTree(root.right)
        return max(diameter, leftDiameter, rightDiameter)

```

## Top Discussions
### Java Solution, MaxDepth
- Author: shawngao
- Creation Date: Sun Mar 19 2017 11:01:57 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 12:21:38 GMT+0800 (Singapore Standard Time)

<p>
For ```every``` node, length of longest path which ```pass it``` = MaxDepth of its left subtree + MaxDepth of its right subtree.

```
public class Solution {
    int max = 0;
    
    public int diameterOfBinaryTree(TreeNode root) {
        maxDepth(root);
        return max;
    }
    
    private int maxDepth(TreeNode root) {
        if (root == null) return 0;
        
        int left = maxDepth(root.left);
        int right = maxDepth(root.right);
        
        max = Math.max(max, left + right);
        
        return Math.max(left, right) + 1;
    }
}
```
</p>


### Simple Python
- Author: realisking
- Creation Date: Sun Mar 19 2017 11:21:09 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 05:49:26 GMT+0800 (Singapore Standard Time)

<p>
Just go through the tree.
```
class Solution(object):
    def diameterOfBinaryTree(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.ans = 0
        
        def depth(p):
            if not p: return 0
            left, right = depth(p.left), depth(p.right)
            self.ans = max(self.ans, left+right)
            return 1 + max(left, right)
            
        depth(root)
        return self.ans
```
</p>


### Java easy to understand solution
- Author: meredithgrey
- Creation Date: Mon Mar 20 2017 22:43:19 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 11 2018 08:33:22 GMT+0800 (Singapore Standard Time)

<p>
``` 
public class Solution {
    public int diameterOfBinaryTree(TreeNode root) {
        if(root == null){
            return 0;
        }
       int dia = depth(root.left) + depth(root.right);
       int ldia = diameterOfBinaryTree(root.left);
       int rdia = diameterOfBinaryTree(root.right);
       return Math.max(dia,Math.max(ldia,rdia));
        
    }
    public int depth(TreeNode root){
        if(root == null){
            return 0;
        }
        return 1+Math.max(depth(root.left), depth(root.right));
    }
    
}

 ```
</p>


