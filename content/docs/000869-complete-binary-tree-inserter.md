---
title: "Complete Binary Tree Inserter"
weight: 869
#id: "complete-binary-tree-inserter"
---
## Description
<div class="description">
<p>A <em>complete</em> binary tree is a binary tree in which every level, except possibly the last, is completely filled, and all nodes are as far left as possible.</p>

<p>Write a data structure&nbsp;<code>CBTInserter</code>&nbsp;that is initialized with a complete binary tree and supports the following operations:</p>

<ul>
	<li><code>CBTInserter(TreeNode root)</code> initializes the data structure on a given tree&nbsp;with head node <code>root</code>;</li>
	<li><code>CBTInserter.insert(int v)</code> will insert a <code>TreeNode</code>&nbsp;into the tree with value <code>node.val =&nbsp;v</code>&nbsp;so that the tree remains complete, <strong>and returns the value of the parent of the inserted <code>TreeNode</code></strong>;</li>
	<li><code>CBTInserter.get_root()</code> will return the head node of the tree.</li>
</ul>

<ol>
</ol>

<div>
<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>inputs = <span id="example-input-1-1">[&quot;CBTInserter&quot;,&quot;insert&quot;,&quot;get_root&quot;]</span>, inputs = <span id="example-input-1-2">[[[1]],[2],[]]</span>
<strong>Output: </strong><span id="example-output-1">[null,1,[1,2]]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>inputs = <span id="example-input-2-1">[&quot;CBTInserter&quot;,&quot;insert&quot;,&quot;insert&quot;,&quot;get_root&quot;]</span>, inputs = <span id="example-input-2-2">[[[1,2,3,4,5,6]],[7],[8],[]]</span>
<strong>Output: </strong><span id="example-output-2">[null,3,4,[1,2,3,4,5,6,7,8]]</span></pre>
</div>

<div>
<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>The initial given tree is complete and contains between <code>1</code> and <code>1000</code> nodes.</li>
	<li><code>CBTInserter.insert</code> is called at most <code>10000</code> times per test case.</li>
	<li>Every value of a given or inserted node is between <code>0</code> and <code>5000</code>.</li>
</ol>
</div>
</div>

<div>
<p>&nbsp;</p>

<div>&nbsp;</div>
</div>

</div>

## Tags
- Tree (tree)

## Companies
- Microsoft - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Deque

**Intuition**

Consider all the nodes numbered first by level and then left to right.  Call this the "number order" of the nodes.

At each insertion step, we want to insert into the node with the lowest number (that still has 0 or 1 children).

By maintaining a `deque` (double ended queue) of these nodes in number order, we can solve the problem.  After inserting a node, that node now has the highest number and no children, so it goes at the end of the deque.  To get the node with the lowest number, we pop from the beginning of the deque.

**Algorithm**

First, perform a breadth-first search to populate the `deque` with nodes that have 0 or 1 children, in number order.

Now when inserting a node, the parent is the first element of `deque`, and we add this new node to our `deque`.

<iframe src="https://leetcode.com/playground/RJczU3i3/shared" frameBorder="0" width="100%" height="500" name="RJczU3i3"></iframe>

**Complexity Analysis**

* Time Complexity:  The preprocessing is $$O(N)$$, where $$N$$ is the number of nodes in the tree.  Each insertion operation thereafter is $$O(1)$$.

* Space Complexity:  $$O(N_{\text{cur}})$$ space complexity, when the size of the tree during the current insertion operation is $$N_{\text{cur}}$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] O(1) Insert
- Author: lee215
- Creation Date: Sun Oct 07 2018 11:06:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 18:07:36 GMT+0800 (Singapore Standard Time)

<p>
Store tree nodes to a list `self.tree` in bfs order.
Node `tree[i]` has left child `tree[2 * i + 1]` and `tree[2 * i + 2]`

So when insert the `N`th node (0-indexed), we push it into the list.
we can find its parent `tree[(N - 1) / 2]` directly.


**C++:**
```
    vector<TreeNode*> tree;
    CBTInserter(TreeNode* root) {
        tree.push_back(root);
        for(int i = 0; i < tree.size();++i) {
            if (tree[i]->left) tree.push_back(tree[i]->left);
            if (tree[i]->right) tree.push_back(tree[i]->right);
        }
    }

    int insert(int v) {
        int N = tree.size();
        TreeNode* node = new TreeNode(v);
        tree.push_back(node);
        if (N % 2)
            tree[(N - 1) / 2]->left = node;
        else
            tree[(N - 1) / 2]->right = node;
        return tree[(N - 1) / 2]->val;
    }

    TreeNode* get_root() {
        return tree[0];
    }
```

**Java:**
```
    List<TreeNode> tree;
    public CBTInserter(TreeNode root) {
        tree = new ArrayList<>();
        tree.add(root);
        for (int i = 0; i < tree.size(); ++i) {
            if (tree.get(i).left != null) tree.add(tree.get(i).left);
            if (tree.get(i).right != null) tree.add(tree.get(i).right);
        }
    }

    public int insert(int v) {
        int N = tree.size();
        TreeNode node = new TreeNode(v);
        tree.add(node);
        if (N % 2 == 1)
            tree.get((N - 1) / 2).left = node;
        else
            tree.get((N - 1) / 2).right = node;
        return tree.get((N - 1) / 2).val;
    }

    public TreeNode get_root() {
        return tree.get(0);
    }
```

**Python:**
```
    def __init__(self, root):
        self.tree = [root]
        for i in self.tree:
            if i.left: self.tree.append(i.left)
            if i.right: self.tree.append(i.right)

    def insert(self, v):
        N = len(self.tree)
        self.tree.append(TreeNode(v))
        if N % 2:
            self.tree[(N - 1) / 2].left = self.tree[-1]
        else:
            self.tree[(N - 1) / 2].right = self.tree[-1]
        return self.tree[(N - 1) / 2].val

    def get_root(self):
        return self.tree[0]
```
</p>


### [Java/Python 3] 2 BFS straightforward codes, Initialization and insert time O(1), respectively.
- Author: rock
- Creation Date: Sun Oct 07 2018 11:06:48 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Apr 29 2020 00:36:37 GMT+0800 (Singapore Standard Time)

<p>
Logic is simple:
1. Traverse the binary tree by level order;
2. If the current node has left and right child, pop it out, and add both of its children into the queue; otherwise, insert new node as its child;
3. repeat the above till encounter the first node that does NOT have two children.

Method 1:
Init: O(1), insert(): O(n)

```java
    
    private TreeNode root;
    
    public CBTInserter(TreeNode root) { this.root = root; }
    
    public int insert(int v) {
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        while (true) {
            TreeNode n = q.poll();
            if (n.right != null) { // n has 2 children.
                q.offer(n.left);    
                q.offer(n.right);
            }else {                                  // n has at most 1 child.
                if (n.left == null) { 
                    n.left = new TreeNode(v); 
                }else { 
                   n.right = new TreeNode(v); 
                }
                return n.val;
            }
        } 
    }
    
    public TreeNode get_root() { return root; }

```
Improve the time complexity of `insert` to O(number of nodes at last level of the tree), though still O(n) in worst case. - credit to **@Windson**.
```java

    private final TreeNode root;
    private final Queue<TreeNode> q;
    
    public CBTInserter(TreeNode root) {
        this.root = root;
        q = new LinkedList<>();
        q.offer(root);
    }
    
    public int insert(int v) {
        while (q.peek().right != null) {
            q.offer(q.peek().left);    
            q.offer(q.poll().right);
        }
        TreeNode parent = q.peek(), n = new TreeNode(v);
        if (parent.left == null) {
            parent.left = n;
        }else {
            parent.right = n;
        }
        return parent.val;
    }
    
    public TreeNode get_root() {
        return root;    
    }
```
```python
    def __init__(self, root: TreeNode):
        self.root = root
        self.dq = collections.deque([root])

    def insert(self, v: int) -> int:
        while self.dq[0].right:
            node = self.dq.popleft()
            self.dq.append(node.left)
            self.dq.append(node.right)
        parent = self.dq[0]
        if parent.left:
            parent.right = TreeNode(v)     
        else:
            parent.left = TreeNode(v)    
        return parent.val

    def get_root(self) -> TreeNode:
        return self.root
```
Method 2: 
Init: O(n), insert(): O(1) 
```java
    private TreeNode root;
    private Queue<TreeNode> q = new LinkedList<>();
    
    public CBTInserter(TreeNode root) {
        this.root = root;
        q.offer(root);
        while (q.peek().right != null) {
            q.offer(q.peek().left);    
            q.offer(q.poll().right);
        } 
    }
    
    public int insert(int v) {
        TreeNode p = q.peek();
        if (p.left == null) { 
            p.left = new TreeNode(v); 
        }else { 
            p.right = new TreeNode(v); 
            q.offer(p.left);
            q.offer(p.right);
            q.poll();
        }
        return p.val;
    }
		
    public TreeNode get_root() { return root; }
```
```python
    def __init__(self, root: TreeNode):
        self.root = root
        self.dq = collections.deque([root])
        while self.dq[0].right:
            node = self.dq.popleft()
            self.dq.extend([node.left, node.right])

    def insert(self, v: int) -> int:
        parent = self.dq[0]
        if parent.left:
            parent.right = TreeNode(v)     
            self.dq.extend([parent.left, parent.right])
            self.dq.popleft()
        else:
            parent.left = TreeNode(v)    
        return parent.val

    def get_root(self) -> TreeNode:
        return self.root

```

</p>


### Java Solution: O(1) Insert VS. O(1) Pre-process Trade Off
- Author: FLAGbigoffer
- Creation Date: Sun Oct 07 2018 14:42:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 18 2019 14:24:47 GMT+0800 (Singapore Standard Time)

<p>
* For this question, if you discuss the trade off between O(1) insert and O(1) build tree with interviewer, you are awesome!
* I was thinking how to solve it by given a complete BST instead of binary tree, which could be a follow up question.
### 1. O(1) Insert + O(N) Build Tree: 
```
class CBTInserter {
    private TreeNode root;
    private Queue<TreeNode> queue;
    //O(N) build tree: Find the first node which doesn\'t have both left and right child.
    public CBTInserter(TreeNode root) {
        this.root = root;
        queue = new LinkedList<>();
        queue.offer(root);
        while (true) {
            TreeNode cur = queue.peek();
            if (cur.left != null && cur.right != null) {
                queue.offer(cur.left);
                queue.offer(cur.right);
                queue.poll();
            } else {
                break;
            }
        }
    }
    
    public int insert(int v) {
        TreeNode cur = queue.peek();
        if (cur.left == null) {
            cur.left = new TreeNode(v);
        } else {
            cur.right = new TreeNode(v);
            queue.offer(cur.left);
            queue.offer(cur.right);
            queue.poll();
        }
        return cur.val;
    }
    
    public TreeNode get_root() {
        return root;
    }
}
```

### 2. O(1) Build Tree + O(N) Insert:
```
class CBTInserter {
    private TreeNode root;
    
    public CBTInserter(TreeNode root) {
        this.root = root;    
    }
    
    public int insert(int v) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            TreeNode cur = queue.poll();
            if (cur.left != null) {
                queue.offer(cur.left);
            } else {
                cur.left = new TreeNode(v);
                return cur.val;
            }
            
            if (cur.right != null) {
                queue.offer(cur.right);
            } else {
                cur.right = new TreeNode(v);
                return cur.val;
            }
        }
        return 0;
    }
    
    public TreeNode get_root() {
        return root;
    }
}
```
</p>


