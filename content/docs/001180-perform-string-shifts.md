---
title: "Perform String Shifts"
weight: 1180
#id: "perform-string-shifts"
---
## Description
<div class="description">
<p>You are given a string <code>s</code>&nbsp;containing lowercase English letters, and a matrix&nbsp;<code>shift</code>, where&nbsp;<code>shift[i] = [direction, amount]</code>:</p>

<ul>
	<li><code>direction</code>&nbsp;can be <code>0</code>&nbsp;(for left shift) or <code>1</code>&nbsp;(for right shift).&nbsp;</li>
	<li><code>amount</code>&nbsp;is the amount by which string&nbsp;<code>s</code>&nbsp;is to be shifted.</li>
	<li>A left shift by 1 means remove the first character of <code>s</code> and append it to the end.</li>
	<li>Similarly, a right shift by 1 means remove the last character of <code>s</code> and add it to the beginning.</li>
</ul>

<p>Return the final string after all operations.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abc&quot;, shift = [[0,1],[1,2]]
<strong>Output:</strong> &quot;cab&quot;
<strong>Explanation:</strong>&nbsp;
[0,1] means shift to left by 1. &quot;abc&quot; -&gt; &quot;bca&quot;
[1,2] means shift to right by 2. &quot;bca&quot; -&gt; &quot;cab&quot;</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abcdefg&quot;, shift = [[1,1],[1,1],[0,2],[1,3]]
<strong>Output:</strong> &quot;efgabcd&quot;
<strong>Explanation:</strong>&nbsp; 
[1,1] means shift to right by 1. &quot;abcdefg&quot; -&gt; &quot;gabcdef&quot;
[1,1] means shift to right by 1. &quot;gabcdef&quot; -&gt; &quot;fgabcde&quot;
[0,2] means shift to left by 2. &quot;fgabcde&quot; -&gt; &quot;abcdefg&quot;
[1,3] means shift to right by 3. &quot;abcdefg&quot; -&gt; &quot;efgabcd&quot;</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 100</code></li>
	<li><code>s</code> only contains lower case English letters.</li>
	<li><code>1 &lt;= shift.length &lt;= 100</code></li>
	<li><code>shift[i].length == 2</code></li>
	<li><code>0 &lt;= shift[i][0] &lt;= 1</code></li>
	<li><code>0 &lt;= shift[i][1] &lt;= 100</code></li>
</ul>
</div>

## Tags
- Array (array)
- Math (math)

## Companies
- Goldman Sachs - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Simulation

**Intuition**

When given a shift operation, for example `[1, 4]`, we'll refer to the first part as the *shift-direction*, and the second part as the *shift-amount*.

The most obvious way of solving this problem is to simulate the shifts, one by one.

For example, suppose we have the string `"leetcodeiscool"`.

If we need to perform shift operation `[0, 5]`, then we'll shift the string *left* by 5, i.e.:

!?!../Documents/10004_left_shift_5.json:960,200!?!

And if we need to perform shift operation `[1, 5]`, then we'll shift the string *right* by 5, i.e.:

!?!../Documents/10004_right_shift_5.json:960,200!?!

We could write an algorithm that simulates the process in the animation for each shift operation. However, strings are immutable in many programming languages, including Java and Python. This means that once a string object has been created, it *cannot be modified*. Instead, a *new string* must be created if we want to change it. This makes "string modification" a very expensive operation, at $$O(L)$$, where $$L$$ is the length of the string. Therefore, we don't want to make lots of little modifications: we want to combine them into one single modification that can be applied with a single new string creation.

So, how can we do that here? Well, observe that a *left shift* is equivalent to taking *shift-amount* letters off the front, and putting them onto the end. Similarly, a *right shift* is equivalent to taking *shift-amount* letters off the end, and putting them onto the front.

![A left shift operation as a single concatenation.](../Figures/10004/concatentation_left_shift.png)

![A right shift operation as a single concatenation.](../Figures/10004/concatentation_right_shift.png)

This will work as long as *shift-amount* is *less than the length of the string*.

However, *shift-amounts* are allowed to be *longer* than the length of the string. For example, consider the word `"cabbage"` (which has seven letters). We need to apply the *right-shift* operation `[1, 9]` to it. If the word was more than nine letters long, we would have just taken nine letters off the end, and put them on the front. But there aren't nine letters to move! To find a solution, let's start by going back to the inefficient algorithm; moving the string by one position at a time.

!?!../Documents/10004_cabbage_shift_9.json:500,200!?!

Notice that after seven shifts we're back to `"cabbage"` again. Only the last two shifts mattered. The number seven is significant because it is the length of the input string. We can extend this a bit further by reasoning that if we had a *shift-amount* of 25, then we'd effectively have `7 + 7 + 7 + 4`. The `7`'s will do nothing, and it is only the final `4` that the string will be shifted by.

Recognize that the "remainder" after subtracting the length as many times as we can is simply taking the modulus of a number. i.e. `9 % 7 = 2` and `25 % 7 = 4`.

Therefore, if the *shift-amount* is greater than the length of the string, then we should start by reducing the *shift-amount* to modulo the length of the string. In fact, we don't even need the "if". For example, `6 % 7 = 6`. The modulo does nothing if the first number is smaller than the second.

To put this into a complete algorithm that solves the problem, we need to loop through the list of shift operations, doing a single concatenation for each one. Here is an animation of this process.

!?!../Documents/10004_repeated_concatenation_animation.json:960,360!?!

**Algorithm**

<iframe src="https://leetcode.com/playground/fr8QTH2k/shared" frameBorder="0" width="100%" height="344" name="fr8QTH2k"></iframe>


**Complexity Analysis**

Let $$L$$ be the length of the string and $$N$$ be the length of the `shift` array.

- Time complexity : $$O(N \cdot L)$$.

    Making a single modification to the input string has a cost of $$O(L)$$, as we need to create a new string with the modifications. We are making one modification for each shift operation. As there are $$N$$ shift operations, this gives us a total time complexity of $$O(N \cdot L)$$.

- Space complexity : $$O(L)$$.

    While performing a string modification, we'll have both the original string and the new string in memory. Therefore, the space complexity is $$O(L)$$.

    Note that if you're using a language with mutable strings (such as C or C++), then it is possible to get the space complexity down to $$O(1)$$ by doing the shift operations in-place with a suitable algorithm. Approach 3 or 4 of the [Rotate Strings Solution Article](https://leetcode.com/problems/rotate-array/solution/) would be a great way of going about this.

Before we came up with this approach, we briefly discussed a simpler approach where instead of doing each shift operation as a single string modification, we'd do it as *shift-amount* operations. What would the time complexity for this approach be? To simplify, we'll assume that *shift-amount* must be less than or equal to $$L$$ (we can use the modulo operator to ensure this). Under this assumption, the worst case is where all the *shift-amounts* are exactly $$L - 1$$. This means that applying a shift operation will do a $$O(L)$$ string modification, $$L-1$$ times. $$(S - 1) \cdot O(L) = O(L^2)$$. Then with $$N$$ shift operations to perform, we get a total of $$O(N \cdot L ^ 2)$$. This is a lot worse!

<br/>

---

#### Approach 2: Compute Net Shift

**Intuition**

In Approach 1, we calculated each shift at a cost of $$O(L)$$ each time (where $$L$$ is the length of the input string). However, you might've noticed that we can combine *all* the shifts, and then perform a *single* string modification for *all of them*.

For example, if we have two left shifts, `[0, 3]` and `[0, 6]`, then we can combine them into a single left shift `[0, 3 + 6] = [0, 9]`. Then, instead of performing two separate $$O(L)$$ modifications, we can perform just one.

![Combining two left shifts into a single left shift.](../Figures/10004/combine_shifts.png)

The same idea applies to the right shifts.

We could, therefore, add all the lefts together and apply them, and then add all the rights together, and apply them. This would mean we only needed to perform two $$O(L)$$ string modifications.


!?!../Documents/10004_left_right_additions.json:960,360!?!

We can do even better than this though, as *left shifts and right shifts cancel each other out*. For example, a *left shift* by `3`, followed by a *right shift* by `3`, brings us back to the original string. It is, therefore, wasted computation to do these shift operations at all!

![Left and right shifts by the same amount canceling each other out.](../Figures/10004/left_right_cancellation.png)

What about a *right shift* by `5`, followed by a *left shift* by `3`? 

![Left and right shifts by the same amount canceling each other out.](../Figures/10004/partial_left_right_cancellation.png)

In effect, we are left with a *right shift* by `2`.

So for this approach, we'll write an algorithm that pre-processes the `shift` list, and then applies a single string modification.

**Algorithm**

As described above, we should go through the `shift` list adding up all the *right shift-amounts* and *left shift-amounts* into two separate sums.

And then once we have these two amounts, we need to work out which is bigger, and partially cancel it out with the other. We then need to do the relevant shift with what's remaining. If both are the same, then the string won't be changed at all.

Here is the code for this algorithm. After this code, we'll look at a slight optimization/ simplification, that uses a little more math.

<iframe src="https://leetcode.com/playground/TVdEcrcJ/shared" frameBorder="0" width="100%" height="480" name="TVdEcrcJ"></iframe>


To simplify the code further, remember that *right shifts* and *left shifts* cancel each other out. We'll just keep track of how many *left shifts* there are, by treating *right shifts* as negative *left shifts*.

If after doing this, the final value of `left_shifts` is positive, then we need to do a *left shift operation*. If it is negative, then we need to do a *right shift operation*, i.e. if it is `-5`, then we need to *right shift* by `5`.

We can simplify it further still by recognizing that the number of *right shifts* can be converted into a number of *left shifts*. Notice that all "valid" shifts of a string can be represented in a clock-like circle. For example, here is the "clock" diagram we could make for the word `"leetcode"`.

![A clock of all shifts of the word "leetcode".](../Figures/10004/clock_combinations.png)

When we *left shift* the string, we are moving clockwise. When we *right shift* the string, we are moving anti-clockwise.

Now to get the overall left shift, we can simply take the total *left shifts* (regardless of whether it is positive or negative) modulo the length of the string:

`left_shifts = left_shifts % length of string`

In most programming languages, the modulo operator will add length to the shift amount until the result is positive. This is what we want. Note that the `%` operator doesn't work this way in *all* languages. In some languages, such as Java, *it does not make negative numbers positive*. Java's `Math.floorMod` function will do what we want though.

<iframe src="https://leetcode.com/playground/7cySqPMb/shared" frameBorder="0" width="100%" height="361" name="7cySqPMb"></iframe>


**Complexity Analysis**

Let $$L$$ be the length of the string and $$N$$ be the length of the `shift` array. Both sub-approaches here have the same complexity analysis.

- Time complexity : $$O(N + L)$$.
    
    The algorithms presented in Approach 2 both break the task into two sub-steps: calculating an overall shift and applying the shift. We'll analyze these one at a time and then combine the results.

    The first step loops through each of the $$N$$ entries in the `shift` array, adding up the total number of left shifts and the total number of right shifts. Handling each entry is an $$O(1)$$ operation, so this first step has a total cost of $$O(N)$$.

    The second step applies a single string-shift operation. As discussed in the previous approach, a string-shift operation has a cost of $$O(L)$$.

    Because we are doing these steps one-after-the-other, and we don't know whether $$N$$ or $$L$$ is bigger, we add them to get a final time complexity of $$O(N + L)$$.

- Space complexity : $$O(L)$$.

    The first step uses constant extra space to keep track of the counts. This leaves us with the space complexity of modifying a string, which, as discussed before, requires auxiliary space of $$O(L)$$.

    As stated in the previous approach, it is possible to get the space complexity down to $$O(1)$$ by using a language with mutable strings.

<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python O(len(s) + len(shift))
- Author: ddoudle
- Creation Date: Tue Apr 14 2020 15:10:53 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 14 2020 15:13:10 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def stringShift(self, s: str, shift: List[List[int]]) -> str:
        move = 0
        for x, y in shift:
            if x == 0:
                move -= y
            else:
                move += y
        move %= len(s)
        return s[-move:] + s[:-move]
```
</p>


### [Java/Python 3] Modular operation w/ brief explanation and analysis.
- Author: rock
- Creation Date: Tue Apr 14 2020 15:24:36 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 23 2020 22:25:50 GMT+0800 (Singapore Standard Time)

<p>
1. Let left shift be positive and right negative;
2. use `pos % len + len` to avoid negative index and `() % len` to avoid being out of bound.
Alternatively, use the following: - credit to **@user7776r**
```
        pos %= len;
        if (pos < 0) {
            pos += len;
        }
```
```java
    public String stringShift(String s, int[][] shift) {
        int pos = 0, len = s.length();
        for (int[] sh : shift) {
            pos += sh[0] == 0 ? sh[1] : -sh[1];
        }
     // pos = (pos % len + len) % len;
        pos %= len;
        if (pos < 0) {
            pos += len;
        }
        return s.substring(pos) + s.substring(0, pos);
    }
```
credit to **@tridnyabezedi** for making Python code clean (He points out that `functools.reduce(operator.add, list)` is actually `sum()`. This is really a **coding joke**, and I guess I made him a day.:P)
```python
    def stringShift(self, s: str, shift: List[List[int]]) -> str:
      # pos = sum((dist, -dist)[dir] for dir, dist in shift)          # just for fun to make the below line shorter.
        pos = sum(dist if dir == 0 else -dist for dir, dist in shift)
        pos %= len(s)
        return s[pos :] + s[: pos]
```
**Analysis:**
Time: O(n + S), space: O(S), where n = shift.length, S = s.length().
</p>


### [C++] STL, rotate
- Author: sky_io
- Creation Date: Tue Apr 14 2020 15:22:27 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 14 2020 16:23:11 GMT+0800 (Singapore Standard Time)

<p>
```c++
class Solution {
public:
    string stringShift(string s, vector<vector<int>>& shift) {
        int cnt = 0; // count left shift
        for (auto&vi: shift) cnt += (vi[0] ? -vi[1] : vi[1]);
        cnt %= int(s.size());
        if (cnt > 0) {
            rotate(s.begin(), next(s.begin(), cnt), s.end());
        } else {
            rotate(s.rbegin(), next(s.rbegin(), -cnt), s.rend());
        }
        return s;
    }
};
```

right rotate can also be done with left rotate:

```c++
class Solution {
public:
    string stringShift(string s, vector<vector<int>>& shift) {
        int cnt = 0, sz = s.size(); // count left shift
        for (auto&vi: shift) cnt += (vi[0] ? -vi[1] : vi[1]);
        rotate(s.begin(), next(s.begin(), (cnt%sz+sz)%sz), s.end());
        return s;
    }
};
```
</p>


