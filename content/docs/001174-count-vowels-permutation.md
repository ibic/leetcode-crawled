---
title: "Count Vowels Permutation"
weight: 1174
#id: "count-vowels-permutation"
---
## Description
<div class="description">
<p>Given an integer <code>n</code>, your task is to count how many strings of length <code>n</code> can be formed under the following rules:</p>

<ul>
	<li>Each character is a lower case vowel&nbsp;(<code>&#39;a&#39;</code>, <code>&#39;e&#39;</code>, <code>&#39;i&#39;</code>, <code>&#39;o&#39;</code>, <code>&#39;u&#39;</code>)</li>
	<li>Each vowel&nbsp;<code>&#39;a&#39;</code> may only be followed by an <code>&#39;e&#39;</code>.</li>
	<li>Each vowel&nbsp;<code>&#39;e&#39;</code> may only be followed by an <code>&#39;a&#39;</code>&nbsp;or an <code>&#39;i&#39;</code>.</li>
	<li>Each vowel&nbsp;<code>&#39;i&#39;</code> <strong>may not</strong> be followed by another <code>&#39;i&#39;</code>.</li>
	<li>Each vowel&nbsp;<code>&#39;o&#39;</code> may only be followed by an <code>&#39;i&#39;</code> or a&nbsp;<code>&#39;u&#39;</code>.</li>
	<li>Each vowel&nbsp;<code>&#39;u&#39;</code> may only be followed by an <code>&#39;a&#39;.</code></li>
</ul>

<p>Since the answer&nbsp;may be too large,&nbsp;return it modulo <code>10^9 + 7.</code></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> 5
<strong>Explanation:</strong> All possible strings are: &quot;a&quot;, &quot;e&quot;, &quot;i&quot; , &quot;o&quot; and &quot;u&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2
<strong>Output:</strong> 10
<strong>Explanation:</strong> All possible strings are: &quot;ae&quot;, &quot;ea&quot;, &quot;ei&quot;, &quot;ia&quot;, &quot;ie&quot;, &quot;io&quot;, &quot;iu&quot;, &quot;oi&quot;, &quot;ou&quot; and &quot;ua&quot;.
</pre>

<p><strong>Example 3:&nbsp;</strong></p>

<pre>
<strong>Input:</strong> n = 5
<strong>Output:</strong> 68</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 2 * 10^4</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Paypal - 2 (taggedByAdmin: false)
- C3.ai - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Detailed Explanation using Graphs [With Pictures] [O(n)]
- Author: Just__a__Visitor
- Creation Date: Sun Oct 06 2019 12:03:28 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 11 2019 18:12:56 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Let us visualize this as a graph problem. From the above rules, we can create a directed graph where an edge between characters `first` and `second` imply that it is permissible to write `second` immediately after first. Hence, the question converts to, **Given a directed graph, how many paths of length `n` are there?** 

![image](https://assets.leetcode.com/users/just__a__visitor/image_1570334589.png)

Now, Let us say that `dp[n][char]` denotes the number of directed paths of length `n` which end at a particular vertex `char`. Then, we know that the last vertex in our path was `char`. However, let\'s focus on the last second vertex. It could have been any of the vertex which has a direct edge to `char`. Hence, if we can find the number of paths of length `n-1` ending at these vertices, then we can append `char` at the end of every path and we would have exhausted all possibilites.

Hence, `dp[n+1][x] = sum of all dp[n][y]` such that there is a directed edge from `y` to `x`. 

```cpp
class Solution
{
public:
    int countVowelPermutation(int n);
};

int Solution :: countVowelPermutation(int n)
{
    vector<vector<long>> dp(n+1, vector<long>(5, 0));
    
    int MOD = 1e9 + 7;
    
    /* dp[i][j] denotes the number of valid strings of length i */
    
    for(int i = 0; i < 5; i++)
        dp[1][i] = 1;
    
    for(int i = 1; i < n; i++)
    {
        dp[i+1][0] = (dp[i][1] + dp[i][2] + dp[i][4]) %MOD;
        
        dp[i+1][1] = (dp[i][0] + dp[i][2]) % MOD;
        
        dp[i+1][2] = (dp[i][1] + dp[i][3]) % MOD;
        
        dp[i+1][3] = dp[i][2];
        
        dp[i+1][4] = (dp[i][2] + dp[i][3]) % MOD;
    }
    
    int res = 0;
    for(int i = 0; i < 5; i++)
        res = (res + dp[n][i]) % MOD;
    
    return res;
}
```



</p>


### [C++] Bottom-up and Recursive DPs O(n)/Matrix Exponentiation O(logn)
- Author: PhoenixDD
- Creation Date: Sun Oct 06 2019 12:01:04 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 07 2019 12:38:42 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
The question gives us a map of the vowels that can come after each vowel, we use this mapping to get the number of strings that
can be formed by those \'ending\' vowels.
* `a` can only be added after `i`, `e` and `u`. Thus strings ending with `a` in the next step will be sum of strings ending with `i`, `u` and `e` in the current step.
* `e` can only be added after `a` and `i`. Thus strings ending with `e` in the next step will be sum of strings ending with `i` and `a` in the current step.
* `i` can only be added after `o` and `e`. Thus strings ending with `i` in the next step will be sum of strings ending with `o` and  `e` in the current step.
* `o` can only be added after `i`. Thus strings ending with `o` in the next step will be equal to strings ending with `i` in the current step.
* `u` can only be added after `i` and `o`. Thus strings ending with `u` in the next step will be equal to strings ending with `i` and `o` in the current step.

We do this for all the other vowels and repeat this N-1 times to get our answer.

**Solution**
*Bottom-up:*
With inversed relationships as explained above (Totally depends on how you imagine the subproblems/DP).
```
static vector<vector<int>> relation={{1,2,4},{0,2},{1,3},{2},{2,3}};  //Relationships(can be followed after) of each vowel with others
class Solution {
public:
    int countVowelPermutation(int n) 
    {
        int MOD=1e9+7;
        vector<long long> vowels(5,1),vowels_copy;      //When N=1 all vowels are used once to for 1 letter strings
        long long result=0;
        while(--n)
        {
            vowels_copy=vowels;
            for(int i=0;i<5;i++)                         //Characters \'a\' \'e\' \'i\' \'o\' \'u\'.
            {
                vowels[i]=0;
                for(int &r:relation[i])
                    vowels[i]+=vowels_copy[r],vowels[i]%=MOD;  //Add the strings that end with characters that can have \'i\' after them.
            }
        }
        for(long long &i:vowels)
            result+=i,result%=MOD;
        return result;
    }
};
```
**Complexity**
Space: `O(1)`
Time: `O(n)`

*Top-Down (Recursive):*
```
static vector<vector<int>> relation={{1},{0,2},{0,1,3,4},{2,4},{0}};  //Relationships(followed by) of each vowel with others
class Solution {
public:
    int MOD=1e9+7;
    vector<vector<int>> memo;
    int dp(int n,int v)                         //\'v\' represents the vowel
    {
        if(n==1)
            return 1;
        if(memo[n][v]!=-1)
            return memo[n][v];
        memo[n][v]=0;
        for(int &i:relation[v])
            memo[n][v]+=dp(n-1,i),memo[n][v]%=MOD;
        return memo[n][v];
    }
    int countVowelPermutation(int n) 
    {
        int result=0;
        memo.resize(n+1,vector<int>(5,-1));
        for(int i=0;i<5;i++)
            result+=dp(n,i),result%=MOD;
        return result;
    }
};
```
**Complexity**
Space: `O(n)`
Time: `O(n)`

*Matrix Exponentiation:*
This problem basically boils down to number of paths of length N in a directed graph.
```
//Matrix exponentiation.
class Solution {
public:
    int MOD=1e9+7;
    vector<vector<int>> Multiply(vector<vector<int>> &l,vector<vector<int>> &r) //Multiply two matrices applying MOD.
    {
        vector<vector<int>> result(l.size(),vector<int>(r[0].size(),0));
        for(int i=0;i<l.size();i++)
            for(int j=0;j<r[0].size();j++)
                for(int k=0;k<l[0].size();k++)
                    result[i][j]+=(long long)l[i][k]*r[k][j]%MOD,result[i][j]%=MOD;
        return result;
    }
    int countVowelPermutation(int n) 
    {
        vector<vector<int>> M={ {0,1,0,0,0},      //Adjacency matrix of graph of the problem.
                                {1,0,1,0,0},
                                {1,1,0,1,1},
                                {0,0,1,0,1},
                                {1,0,0,0,0}},result(5,vector<int>(5));
        for(int i=0;i<5;i++)   //Create identity Matrix.
            result[i][i]=1;
        int sum=0;
        n--;
        while(n)    //log(n) Multiplication.
        {
            if(n&1)
                result=Multiply(M,result);
            n>>=1;
            M=Multiply(M,M);
        }
        for(vector<int> &i:result)          //Result holds M^(N-1).
            for(int &j:i)
                sum+=j,sum%=MOD;
        return sum;
    }
};
```
**Complexity**
Space: `O(1).`
Time: `O(logn).` Exponentiation.
</p>


### Simple Python (With Diagram)
- Author: elliotp
- Creation Date: Sun Oct 06 2019 12:11:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 12:11:18 GMT+0800 (Singapore Standard Time)

<p>
Each vowel allows some number of subsequent characters. These transitions are like a tree. This problem is asking, "what\'s the width of the tree with height *n*?"

![image](https://assets.leetcode.com/users/elliotp/image_1570334689.png)

My solution keeps track of the number of each vowel at a level in this tree. To calculate say \'A\', we calculate how many nodes in the previous level produce \'A\'. This is the number of \'E\', \'I\', and \'U\' nodes.

```
def count_vowel_permutations(n):
    a, e, i, o, u = 1, 1, 1, 1, 1
    for _ in range(n - 1):
        a, e, i, o, u = e + i + u, a + i, e + o, i, i + o
    return (a + e + i + o + u) % (10**9 + 7)
```
</p>


