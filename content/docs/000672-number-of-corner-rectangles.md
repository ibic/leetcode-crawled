---
title: "Number Of Corner Rectangles"
weight: 672
#id: "number-of-corner-rectangles"
---
## Description
<div class="description">
<p>Given a grid where each entry is only 0 or 1, find the number of corner rectangles.</p>

<p>A <em>corner rectangle</em> is 4 distinct 1s on the grid that form an axis-aligned rectangle. Note that only the corners need to have the value 1. Also, all four 1s used must be distinct.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> grid = 
[[1, 0, 0, 1, 0],
 [0, 0, 1, 0, 1],
 [0, 0, 0, 1, 0],
 [1, 0, 1, 0, 1]]
<strong>Output:</strong> 1
<strong>Explanation:</strong> There is only one corner rectangle, with corners grid[1][2], grid[1][4], grid[3][2], grid[3][4].
</pre>

<p>&nbsp;</p>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> grid = 
[[1, 1, 1],
 [1, 1, 1],
 [1, 1, 1]]
<strong>Output:</strong> 9
<strong>Explanation:</strong> There are four 2x2 rectangles, four 2x3 and 3x2 rectangles, and one 3x3 rectangle.
</pre>

<p>&nbsp;</p>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> grid = 
[[1, 1, 1, 1]]
<strong>Output:</strong> 0
<strong>Explanation:</strong> Rectangles must have four distinct corners.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>The number of rows and columns of <code>grid</code> will each be in the range <code>[1, 200]</code>.</li>
	<li>Each <code>grid[i][j]</code> will be either <code>0</code> or <code>1</code>.</li>
	<li>The number of <code>1</code>s in the grid will be at most <code>6000</code>.</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 5 (taggedByAdmin: false)
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Count Corners [Accepted]

**Intuition**

We ask the question: for each additional row, how many more rectangles are added?

For each pair of 1s in the new row (say at `new_row[i]` and `new_row[j]`), we could create more rectangles where that pair forms the base.  The number of new rectangles is the number of times some previous row had `row[i] = row[j] = 1`.

**Algorithm**

Let's maintain a count `count[i, j]`, the number of times we saw `row[i] = row[j] = 1`.  When we process a new row, for every pair `new_row[i] = new_row[j] = 1`, we add `count[i, j]` to the answer, then we increment `count[i, j]`.

<iframe src="https://leetcode.com/playground/RT5x7s8A/shared" frameBorder="0" width="100%" height="344" name="RT5x7s8A"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(R*C^2)$$ where $$R, C$$ is the number of rows and columns.

* Space Complexity: $$O(C^2)$$ in additional space.

---
#### Approach #2: Heavy and Light Rows [Accepted]

**Intuition and Algorithm**

Can we improve on the ideas in *Approach #1*?  When a row is filled with $$X$$ 1s, we do $$O(X^2)$$ work to enumerate every pair of 1s.  This is okay when $$X$$ is small, but expensive when $$X$$ is big.

Say the entire top row is filled with 1s.  When looking at the next row with say, `f` 1s that match the top row, the number of rectangles created is just the number of pairs of 1s, which is `f * (f-1) / 2`.  We could find each `f` quickly using a Set and a simple linear scan of each row.

Let's call a row to be *heavy* if it has more than $$\sqrt N$$ points.  The above algorithm changes the complexity of counting a heavy row from $$O(C^2)$$ to $$O(N)$$, and there are at most $$\sqrt N$$ heavy rows.

<iframe src="https://leetcode.com/playground/cHHVZ5iK/shared" frameBorder="0" width="100%" height="500" name="cHHVZ5iK"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N \sqrt N + R*C)$$ where $$N$$ is the number of ones in the grid.

* Space Complexity: $$O(N + R + C^2)$$ in additional space, for `rows`, `target`, and `count`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### short JAVA AC solution (O(m^2 * n)) with explanation.
- Author: jun1013
- Creation Date: Sun Dec 17 2017 12:19:23 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 07:47:10 GMT+0800 (Singapore Standard Time)

<p>
To find an axis-aligned rectangle, my idea is to fix two rows (or two columns) first, then check column by column to find "1" on both rows. Say you find n pairs, then just pick any 2 of those to form an axis-aligned rectangle (calculating how many in total is just high school math, hint: combination). 
```
class Solution {
    public int countCornerRectangles(int[][] grid) {
        int ans = 0;
        for (int i = 0; i < grid.length - 1; i++) {
            for (int j = i + 1; j < grid.length; j++) {
                int counter = 0;
                for (int k = 0; k < grid[0].length; k++) {
                    if (grid[i][k] == 1 && grid[j][k] == 1) counter++;
                }
                if (counter > 0) ans += counter * (counter - 1) / 2;
            }
        }
        return ans;
    }
}
```
</p>


### Summary of three solutions based on three different ideas
- Author: fun4LeetCode
- Creation Date: Mon Dec 18 2017 08:23:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 10:03:54 GMT+0800 (Singapore Standard Time)

<p>
It looks like there are multiple ways for approaching this problem. So here is a quick summary of these solutions. For notational convenience, we use `m` and `n` to denote respectively the number of rows and columns of the input `grid` matrix.

---

`Idea I -- Check each candidate rectangle one by one to see if it is a corner rectangle`

This is the most straightforward solution. In total, there are `(m * n)^2` candidate rectangles, each of which can be denoted symbolically as `(i, j, p, q)`, with `(i, j)` and `(p, q)` being its top-left and bottom-right corners, respectively.

However, if we try to implement this idea using four plain-nested loops, the solution will be met with `TLE`. The reason is that we were doing a lot of unnecessary checks even if we already know that some of the corners of the candidate rectangle is `0`, thus eliminates its possibility to be a corner rectangle. This can be avoided by checking the four corners one by one and skip as early as possible whenever it is found to be `0`. There are several ways for arranging these optimized-nested loops, here I list two of them for your reference:

**Solution 1 -- loop "i, j" first then "p, q"**

```
public int countCornerRectangles(int[][] grid) {
    int m = grid.length, n = grid[0].length, res = 0;
        
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            if (grid[i][j] == 0) continue;
            
            for (int p = i + 1; p < m; p++) {
                if (grid[p][j] == 0) continue;
                
                for (int q = j + 1; q < n; q++) {
                    res += grid[i][q] & grid[p][q];
                }
            }
        }
    }
    
    return res;
}
```

**Solution 2 -- loop "i, p" first then "j, q"**

```
public int countCornerRectangles(int[][] grid) {
    int m = grid.length, n = grid[0].length, res = 0;
        
    for (int i = 0; i < m; i++) {
        for (int p = i + 1; p < m; p++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 0 || grid[p][j] == 0) continue;
                    
                for (int q = j + 1; q < n; q++) {
                    res += grid[i][q] & grid[p][q];
                }
            }
        }
    }
    
    return res;
}
```

For both solutions, we have: 
**Time complexity:** `O(m^2 * n^2)`
**Space complexity:** `O(1)`

---

`Idea II -- Divide the candidate rectangles into groups and then count the number of corner rectangles for each group`

Well, some of you may wonder why I would post the second solution for `Idea I`, as the first solution is apparently more efficient in terms of skipping unnecessary checks. While this is true, there are some further optimizations that we can do which is more intuitive on top of this second solution.

First, here is my understanding of what this second solution is doing: within the "**i, p**" loop, all the candidate rectangles will be of the form `(i, *, p, *)`, that is, their top edges will come from row `i` and their bottom edges will come from row `p`, thus we can say each pair of indices "**i, p**" effectively defines a group of candidate rectangles. Then we are trying to count the number of corner rectangles for this group by looping through indices "**j, q**". This is done by first finding the left edge of the rectangle such that the left two corners `grid[i][j]` and `grid[p][j]` are `1`, then finding the right edge such that the right two corners `grid[i][q]` and `grid[p][q]` are also `1`.

If you look at the inner loops of indices "**j, q**" more carefully, what they do is essentially counting the number of pairs `(j, q)` such that `grid[i][j] == 1 && grid[p][j] == 1` as well as `grid[i][q] == 1 && grid[p][q] == 1`, assuming `j != q`. It turns out this can be done by looping through the rows only once, as explained below.

Imagine we have a `1-D` array `nums` of numbers `0` and `1`, and we'd like to count the number of pairs `(j, q)` such that `nums[j] == 1` as well as `nums[q] == 1`, assuming `j != q`. Of course, we can use two nested loops to find the answer. Now what if I tell you that the number of `1`s in the array is `c`? Can you figure out the answer immediately? I bet you can -- it's given by `(c * (c-1))/2`, which is equivalent to choosing two `1`s from `c` `1`s. Okay, do we need two nested loops to find the number of `1`s in the array? Surely we don't -- a linear scan would do the job perfectly. So in short, we can figure out the number of pairs `(j, q)` using a linear scan.

Now imagine we have two such `1-D` arrays, one located at row `i` of a `2-D` matrix `grid` and the other located at row `p`. And we'd like to count the number of pairs `(j, q)` such that `grid[i][j] == 1 && grid[p][j] == 1` as well as `grid[i][q] == 1 && grid[p][q] == 1`, again assuming `j != q`. By the same logic, we can do a linear scan to find the number of indices at which both rows have the value of `1`, and the number of pairs can be computed using exactly the same formula mentioned above. So here is the solution implementing this idea:

```
public int countCornerRectangles(int[][] grid) {
    int m = grid.length, n = grid[0].length, res = 0;
        
    for (int i = 0; i < m; i++) {
        for (int p = i + 1; p < m; p++) {
            int c = 0;
                
            for (int k = 0; k < n; k++) {
                c += grid[i][k] & grid[p][k];
            }
                
            res += (c * (c - 1)) >> 1;
        }
    }
        
    return res;
}
```

For this solution, we have:
**Time complexity:** `O(m^2 * n)`
**Space complexity:** `O(1)`

(**Note** that we can also divide the rectangles into groups of the form `(*, j, *, q)` and count the corner rectangles for each such group. The time complexity then would be `O(m * n^2)`.)

---

`Idea III -- Build the grid matrix row by row and then count the number of new corner rectangles resulting from the added row`

This idea is a little bit different from the one above. Here, we are trying to rebuild the `grid` matrix row by row, and increment the number of corner rectangles as new rows are added to the matrix. It sounds like a bottom-up DP and here is a detailed explanation.

Assume now we are trying to add the row with index `i` to the partially built matrix containing rows from index `0` up to index `i-1`. We want to know how many new corner rectangles will be created after this row with index `i` is added. Apparently all the new corner rectangles will have their bottom edges located at row `i`, otherwise they will not be new rectangles, let alone new corner rectangles. The bottom edge of a new rectangle can be characterized by a pair of indices `(j, q)`, and the new rectangle can be a corner rectangle only if we have `grid[i][j] == 1 && grid[i][q] == 1`. Assume this is the case, to figure out how many new corner rectangles can be formed with a bottom edge given by `(j, q)`, we need to find out how many top edges there are such that the top two corners of the rectangle also have value `1`.

Of course, we can go back to each of the previous rows and count the number of rows such that their values at both indices `j` and `q` are `1`.  But then for each newly added row, we have to repeat this process, which is not so efficient. So instead, we can cache these counts and simply look it up for later rows. The logic is as follows: let `T(i, j, q)` be the number of rows where for each row, its row index is no greater than `i` and its values at both indices `j` and `q` are `1`. Then we have this simple recurrence relations: `T(i, j, q) = T(i-1, j, q) + cnt` where `cnt = 1` if `grid[i][j] == 1 && grid[i][q] == 1`, otherwise `cnt = 0`. And the number of new corner rectangles with a bottom edge `(j, q)` after row `i` is added will be given by `T(i-1, j, q)`.

Note that both `j` and `q` are bound in the range `[0, n)`, so we can use a 2-D `n`-by-`n` matrix to store our counts for `T(i, j, q)`. We don't need to consider the dimension of index `i` since `T(i, j, q)` is only related to those with `i-1` so we can iterate in this dimension. The following is the solution based on this idea:

```
public int countCornerRectangles(int[][] grid) {
    int m = grid.length, n = grid[0].length, res = 0;
    
    int[][] dp = new int[n][n];
    
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            if (grid[i][j] == 0) continue;
            
            for (int q = j + 1; q < n; q++) {
                if (grid[i][q] == 0) continue;
                res += dp[j][q]++;
            }
        }
    }
    
    return res;
}
```

For this solution, we have:
**Time complexity:** `O(m * n^2)`
**Space complexity:** `O(n^2)`

(**Note** that we can also build the `grid` matrix column by column. Then the time complexity would be `O(m^2 * n)` and the space complexity would be `O(m^2)`.)

At first sight, it seems `Idea III` gains no advantage over the `Idea II` above, as their time complexity could be the same while the former requires some extra space. However, `Idea III` will be more efficient in skipping unnecessary countings than `Idea II`, thus actually yielding a better time performance (almost `50%` faster from my submissions).
</p>


### Very fast Java DP solution
- Author: yinyifan1991
- Creation Date: Thu Jul 12 2018 06:53:59 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 02:36:22 GMT+0800 (Singapore Standard Time)

<p>
According to the question description, as long as we have two corner points of the upper line of a rectangle and corresponding two corner points of the bottom line, it forms a rectangle. So we can scan each row of the input matrix, every time we find two corner points in this row and treat them as the bottom line of a rectangle, we try to find how many upper lines it can have in the previous scanned lines. Thus, we use a 2D array dp[n][n] to track the number of pairs of corner points before the current row with position in i, j, then the number of rectangles with a bottom line made up of the current two corner points will be dp[i][j]. After that, update dp[i][j] by incrementing one.
```
class Solution {
    public int countCornerRectangles(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        int[][] dp = new int[n][n];
        int res = 0;
        for(int i = 0;i < m;i++) {
            for(int j = 0;j < n;j++) {
                if(grid[i][j] != 1) continue;
                for(int k = j+1;k < n;k++) {
                    if(grid[i][k] == 1) {
                        res += dp[j][k];
                        dp[j][k]++;
                    }
                }
            }
        }
        return res;
    }
}
```
Time comlexity is O(n3) and space complexity is O(n2)
</p>


