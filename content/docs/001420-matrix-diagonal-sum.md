---
title: "Matrix Diagonal Sum"
weight: 1420
#id: "matrix-diagonal-sum"
---
## Description
<div class="description">
<p>Given a&nbsp;square&nbsp;matrix&nbsp;<code>mat</code>, return the sum of the matrix diagonals.</p>

<p>Only include the sum of all the elements on the primary diagonal and all the elements on the secondary diagonal that are not part of the primary diagonal.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/14/sample_1911.png" style="width: 336px; height: 174px;" />
<pre>
<strong>Input:</strong> mat = [[<strong>1</strong>,2,<strong>3</strong>],
&nbsp;             [4,<strong>5</strong>,6],
&nbsp;             [<strong>7</strong>,8,<strong>9</strong>]]
<strong>Output:</strong> 25
<strong>Explanation: </strong>Diagonals sum: 1 + 5 + 9 + 3 + 7 = 25
Notice that element mat[1][1] = 5 is counted only once.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> mat = [[<strong>1</strong>,1,1,<strong>1</strong>],
&nbsp;             [1,<strong>1</strong>,<strong>1</strong>,1],
&nbsp;             [1,<strong>1</strong>,<strong>1</strong>,1],
&nbsp;             [<strong>1</strong>,1,1,<strong>1</strong>]]
<strong>Output:</strong> 8
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> mat = [[<strong>5</strong>]]
<strong>Output:</strong> 5
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == mat.length == mat[i].length</code></li>
	<li><code>1 &lt;= n &lt;= 100</code></li>
	<li><code>1 &lt;= mat[i][j] &lt;= 100</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Python #minimalizm
- Author: votrubac
- Creation Date: Sun Sep 06 2020 07:21:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 12 2020 08:18:10 GMT+0800 (Singapore Standard Time)

<p>
One-liner solutions for C++ and Python, not sure it can be done in Java...

**C++**
```cpp
int diagonalSum(vector<vector<int>>& m, int i = -1) {
    return accumulate(begin(m), end(m), 0, [&](int s, vector<int> &v){
       return s + v[++i] + (i == v.size() - i - 1 ? 0 : v[v.size() - i - 1]);
    });
}
```
**Python**
```python
def diagonalSum(self, m: List[List[int]]) -> int:
	return sum(sum(r[j] for j in {i, len(r) - i - 1}) for i, r in enumerate(m))
```
</p>


### [Java/Python 3] Simple code w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Sep 06 2020 00:02:13 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 08 2020 23:20:11 GMT+0800 (Singapore Standard Time)

<p>
1. If the length of the matrix is odd, then the center cell will be overlapped on the two diagonals, we should deduct it from the result.
2. The cells on secondary (or skew) diagonal meet the rule: `i + j = n - 1`, where `i`, `j` are the coordinates of the cell and `n` is the length of the matrix.
```java
    public int diagonalSum(int[][] mat) {
        int n = mat.length, sum =  n % 2 == 1 ? -mat[n / 2][n / 2] : 0;
        for (int i = 0; i < n; ++i) {
            sum += mat[i][i] + mat[i][n - 1 - i]; 
        }
        return sum;        
    }
```
```python
    def diagonalSum(self, mat: List[List[int]]) -> int:
        n = len(mat)
        return sum(mat[i][i] + mat[i][n - i - 1] for i in range(n)) - (mat[n // 2][n // 2] if n % 2 == 1 else 0)
```
Or
```python
    def diagonalSum(self, mat: List[List[int]]) -> int:
        n = len(mat)
        return sum(row[r] + row[n - 1 - r] for r, row in enumerate(mat)) - mat[n // 2][n // 2])[n % 2]
```
Or **@votrubac**\'s method:
 
```python
    def diagonalSum(self, mat: List[List[int]]) -> int:
        n = len(mat)
        return sum(row[i] for r, row in enumerate(mat) for i in {r, n - 1 - r})
```
**Analysis:**

Time: O(n), space: O(1), where n = mat.length.
</p>


