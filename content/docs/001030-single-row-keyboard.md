---
title: "Single-Row Keyboard"
weight: 1030
#id: "single-row-keyboard"
---
## Description
<div class="description">
<p>There is a special keyboard with <strong>all keys in a single row</strong>.</p>

<p>Given a string <code>keyboard</code> of length 26 indicating the layout of the keyboard (indexed from 0 to&nbsp;25), initially your finger is at index 0. To type a character, you have to move your finger to the index of the desired character. The time taken to move your finger from index <code>i</code> to index <code>j</code> is <code>|i - j|</code>.</p>

<p>You want to type a string <code>word</code>. Write a function to calculate how much time it takes to type it with one finger.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> keyboard = &quot;abcdefghijklmnopqrstuvwxyz&quot;, word = &quot;cba&quot;
<strong>Output:</strong> 4
<strong>Explanation: </strong>The index moves from 0 to 2 to write &#39;c&#39; then to 1 to write &#39;b&#39; then to 0 again to write &#39;a&#39;.
Total time = 2 + 1 + 1 = 4. 
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> keyboard = &quot;pqrstuvwxyzabcdefghijklmno&quot;, word = &quot;leetcode&quot;
<strong>Output:</strong> 73
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>keyboard.length == 26</code></li>
	<li><code><font face="monospace">keyboard</font></code> contains each&nbsp;English lowercase letter&nbsp;exactly once in some order.</li>
	<li><code>1 &lt;= word.length &lt;= 10^4</code></li>
	<li><code>word[i]</code>&nbsp;is an English lowercase letter.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Google - 4 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Easy
- Author: miaoz
- Creation Date: Tue Aug 27 2019 07:01:30 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 27 2019 07:01:30 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution 
{
    public int calculateTime(String keyboard, String word) 
    {
        int[] index = new int[26];
        for (int i = 0; i < keyboard.length(); ++i)
        {
            index[keyboard.charAt(i) - \'a\'] = i;
        }
        
        int sum = 0;
        int last = 0;
        for (char c : word.toCharArray())
        {
            sum += Math.abs(index[c - \'a\'] - last);
            last = index[c - \'a\'];
        }
        
        return sum;
    }
}
```
</p>


### Python3 beats 100%
- Author: justin801514
- Creation Date: Mon Aug 26 2019 04:53:17 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 26 2019 04:54:18 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def calculateTime(self, keyboard: str, word: str) -> int:
        cur_index = 0
        time = 0
        for w in word:
            next_index = keyboard.index(w)
            time += abs(next_index - cur_index)
            cur_index = next_index
        return time
```
</p>


### Python beats 100%
- Author: lzhjiuzhi
- Creation Date: Mon Oct 14 2019 15:18:04 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 14 2019 15:18:04 GMT+0800 (Singapore Standard Time)

<p>
Using dic to record the index or word.

```\'class Solution:
    def calculateTime(self, keyboard: str, word: str) -> int:
        d = {}
        for i in range(len(keyboard)):
            d[keyboard[i]] = i
        res = 0
        temp = 0
        for c in word:
            res += abs(d[c]-temp)
            temp = d[c]
        return res
		```
		
	
</p>


