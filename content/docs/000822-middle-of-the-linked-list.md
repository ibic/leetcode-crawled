---
title: "Middle of the Linked List"
weight: 822
#id: "middle-of-the-linked-list"
---
## Description
<div class="description">
<p>Given a non-empty, singly&nbsp;linked list with head node <code>head</code>, return&nbsp;a&nbsp;middle node of linked list.</p>

<p>If there are two middle nodes, return the second middle node.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,2,3,4,5]</span>
<strong>Output: </strong>Node 3 from this list (Serialization: <span id="example-output-1">[3,4,5]</span>)
The returned node has value 3.  (The judge&#39;s serialization of this node is [3,4,5]).
Note that we returned a ListNode object ans, such that:
ans.val = 3, ans.next.val = 4, ans.next.next.val = 5, and ans.next.next.next = NULL.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[1,2,3,4,5,6]</span>
<strong>Output: </strong>Node 4 from this list (Serialization: <span id="example-output-2">[4,5,6]</span>)
Since the list has two middle nodes with values 3 and 4, we return the second one.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li>The number of nodes in the given list will be between <code>1</code>&nbsp;and <code>100</code>.</li>
</ul>
</div>
</div>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Goldman Sachs - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Samsung - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Qualcomm - 2 (taggedByAdmin: false)
- Walmart Labs - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Output to Array

**Intuition and Algorithm**

Put every node into an array `A` in order.  Then the middle node is just `A[A.length // 2]`, since we can retrieve each node by index.

<iframe src="https://leetcode.com/playground/6PnYCbjD/shared" frameBorder="0" width="100%" height="242" name="6PnYCbjD"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the given list.

* Space Complexity:  $$O(N)$$, the space used by `A`.
<br />
<br />


---
#### Approach 2: Fast and Slow Pointer

**Intuition and Algorithm**

When traversing the list with a pointer `slow`, make another pointer `fast` that traverses twice as fast.  When `fast` reaches the end of the list, `slow` must be in the middle.

<iframe src="https://leetcode.com/playground/R6iLrZdA/shared" frameBorder="0" width="100%" height="259" name="R6iLrZdA"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the given list.

* Space Complexity:  $$O(1)$$, the space used by `slow` and `fast`.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Slow and Fast Pointers
- Author: lee215
- Creation Date: Sun Jul 29 2018 11:05:45 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 14:07:21 GMT+0800 (Singapore Standard Time)

<p>
Each time, `slow` go 1 steps while `fast` go 2 steps.
When `fast` arrives at the end, `slow` will arrive right in the middle.

**C++:**
```
    ListNode* middleNode(ListNode* head) {
        ListNode *slow = head, *fast = head;
        while (fast && fast->next)
            slow = slow->next, fast = fast->next->next;
        return slow;
    }
```

**Java:**
```
    public ListNode middleNode(ListNode head) {
        ListNode slow = head, fast = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }
```
**Python:**
```
    def middleNode(self, head):
        slow = fast = head
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next
        return slow
```
</p>


### Remember this pattern for problems that require middle finding in a Linked List.
- Author: chih-chen
- Creation Date: Sun Jul 29 2018 21:15:00 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 21:21:36 GMT+0800 (Singapore Standard Time)

<p>
For those who read it, I wish you good luck and all the best for your interviews.
```
class Solution {
public:
    ListNode* middleNode(ListNode* head) {

        ListNode* fast = head;
        ListNode* slow = fast;

        while (fast && fast->next) {
            slow = slow->next;
            fast = fast->next->next;
        }

        return slow;        
    }
};


```
</p>


### Python two pointer, extremely simple, with explaination
- Author: zhouminping
- Creation Date: Sun Jul 29 2018 14:24:02 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 21:21:44 GMT+0800 (Singapore Standard Time)

<p>

    def middleNode(self, head):
        tmp = head
        while tmp and tmp.next:
            head = head.next
            tmp = tmp.next.next
        return head
				
We need two pointers, one is head with one step each iteration, and the other is tmp with two steps each iteration. So when the tmp reaches the end of the list, the head just reaches the half of it, which is exactly what we want. 

And one thing needs to be considered additionaly is that the number of the node can be odd and even, which may affect the termination condition of the iteration. To solve this, my idea is to try the algorithm in some small set of the examples, like the examples provided by the official. And you will find that if the tmp reaches Null or tmp.next reaches Null, the head is the result.
</p>


