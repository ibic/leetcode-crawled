---
title: "Fizz Buzz"
weight: 395
#id: "fizz-buzz"
---
## Description
<div class="description">
<p>Write a program that outputs the string representation of numbers from 1 to <i>n</i>.</p>

<p>But for multiples of three it should output “Fizz” instead of the number and for the multiples of five output “Buzz”. For numbers which are multiples of both three and five output “FizzBuzz”.</p>

<p><b>Example:</b>
<pre>
n = 15,

Return:
[
    "1",
    "2",
    "Fizz",
    "4",
    "Buzz",
    "Fizz",
    "7",
    "8",
    "Fizz",
    "Buzz",
    "11",
    "Fizz",
    "13",
    "14",
    "FizzBuzz"
]
</pre>
</p>
</div>

## Tags


## Companies
- Bloomberg - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- IBM - 2 (taggedByAdmin: false)
- Capital One - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Wayfair - 2 (taggedByAdmin: false)
- Apple - 7 (taggedByAdmin: false)
- LinkedIn - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Spotify - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---

You must have played FizzBuzz as kids. FizzBuzz charm never gets old. And so here we are looking at how you can take on one step at a time and impress your interviewer with a better and neat approach to solve this problem.

#### Approach 1: Naive Approach

**Intuition**

The moment you hear of FizzBuzz you think whether the number is divisible by `3`, `5` or both.

**Algorithm**

1. Initialize an empty answer list.
1. Iterate on the numbers from $$1 ... N$$.
2. For every number, if it is divisible by both 3 and 5, add FizzBuzz to the answer list.
3. Else, Check if the number is divisible by 3, add Fizz.
4. Else, Check if the number is divisible by 5, add Buzz.
5. Else, add the number.
</pre>

<br>

<iframe src="https://leetcode.com/playground/2NdBJpWG/shared" frameBorder="0" width="100%" height="500" name="2NdBJpWG"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$
* Space Complexity: $$O(1)$$
<br/>
<br/>
---

#### Approach 2: String Concatenation

**Intuition**

This approach won't reduce the asymptotic complexity, but proves to be a neater solution when `FizzBuzz` comes with a twist.
What if `FizzBuzz` is now `FizzBuzzJazz` i.e.
<pre>
3 ---> "Fizz" , 5 ---> "Buzz", 7 ---> "Jazz"
</pre>

If you try to solve this with the previous approach the program would have too many conditions to check:

1. Divisible by 3
2. Divisible by 5
3. Divisible by 7
4. Divisible by 3 and 5
5. Divisible by 3 and 7
6. Divisible by 7 and 3
7. Divisible by 3 and 5 and 7
8. Not divisible by 3 or 5 or 7.

This way if the `FizzBuzz` mappings increase, the conditions would grow exponentially in your program.  

**Algorithm**

Instead of checking for every combination of these conditions, check for divisibility by given numbers i.e. 3, 5 as given in the problem. If the number is divisible, concatenate the corresponding string mapping `Fizz` or `Buzz` to the current answer string.

For eg. If we are checking for the number 15, the steps would be:
<pre>
Condition 1: 15 % 3 == 0 , num_ans_str = "Fizz"
Condition 2: 15 % 5 == 0 , num_ans_str += "Buzz"
=> num_ans_str = "FizzBuzz"
</pre>

So for `FizzBuzz` we just check for two conditions instead of three conditions as in the first approach.

Similarly, for `FizzBuzzJazz` now we would just have three conditions to check for divisibility.

<iframe src="https://leetcode.com/playground/2HgzsfWd/shared" frameBorder="0" width="100%" height="500" name="2HgzsfWd"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$
* Space Complexity: $$O(1)$$
<br/>
<br/>
---

#### Approach 3: Hash it!

**Intuition**

This approach is an optimization over approach 2. When the number of mappings are limited, approach 2 looks good. But what if you face a tricky interviewer and he decides to add too many mappings?

Having a condition for every mapping is not feasible or may be we can say the code might get ugly and tough to maintain.

What if tomorrow we have to change a mapping or may be delete a mapping? Are we going to change the code every time we have a modification in the mappings?

We don't have to. We can put all these mappings in a `Hash Table`.

**Algorithm**

1. Put all the mappings in a hash table. The hash table `fizzBuzzHash` would look something like ``{ 3: 'Fizz', 5: 'Buzz' }``
2. Iterate on the numbers from $$1 ... N$$.
3. For every number, iterate over the `fizzBuzzHash` keys and check for divisibility.
4. If the number is divisible by the key, concatenate the corresponding hash value to the answer string for current number. We do this for every entry in the hash table.
5. Add the answer string to the answer list.

> This way you can add/delete mappings to/from to the hash table and not worry about changing the code.

So, for `FizzBuzzJazz` the hash table would look something like ``{ 3: 'Fizz', 5: 'Buzz', 7: 'Jazz' }``

<iframe src="https://leetcode.com/playground/gVrBhyPg/shared" frameBorder="0" width="100%" height="500" name="gVrBhyPg"></iframe>

**Complexity Analysis**

* Time Complexity : $$O(N)$$
* Space Complexity : $$O(1)$$
<br /><br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 4ms solution , Not using "%" operation
- Author: hand515
- Creation Date: Thu Oct 20 2016 23:43:35 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 06:40:20 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public List<String> fizzBuzz(int n) {
        List<String> ret = new ArrayList<String>(n);
        for(int i=1,fizz=0,buzz=0;i<=n ;i++){
            fizz++;
            buzz++;
            if(fizz==3 && buzz==5){
                ret.add("FizzBuzz");
                fizz=0;
                buzz=0;
            }else if(fizz==3){
                ret.add("Fizz");
                fizz=0;
            }else if(buzz==5){
                ret.add("Buzz");
                buzz=0;
            }else{
                ret.add(String.valueOf(i));
            }
        } 
        return ret;
    }
}
```
</p>


### Python Golf
- Author: StefanPochmann
- Creation Date: Thu Oct 20 2016 05:02:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 10:51:23 GMT+0800 (Singapore Standard Time)

<p>
Using [my old CheckiO solution](https://py.checkio.org/mission/fizz-buzz/publications/StefanPochmann/python-27/shortest-52-kill-me-now/):

    def fizzBuzz(self, n):
        return['FizzBuzz'[i%-3&-4:i%-5&8^12]or`i`for i in range(1,n+1)]

Maybe I could shorten it to use `range(n)`, but as you can tell from my above link, that was exhausting enough :-)

And a cleaner one I once saw somewhere:

    def fizzBuzz(self, n):
        return ['Fizz' * (not i % 3) + 'Buzz' * (not i % 5) or str(i) for i in range(1, n+1)]
</p>


### C++ 0ms Solution, beats 100.00% without  if-else
- Author: rawfh
- Creation Date: Sun Jul 15 2018 02:06:03 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 09:07:42 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    vector<string> fizzBuzz(int n) {
        vector<string> res(n);
        for(int i = 1;i <= n; i++) {
            res[i - 1] = to_string(i);
        }
        for(int i = 2;i < n; i += 3) {
            res[i] = "Fizz";
        }
        for(int i = 4;i < n; i += 5) {
            res[i] = "Buzz";
        }
        for(int i = 14;i < n; i += 15) {
            res[i] = "FizzBuzz";
        }
        return res;
    }
};
```
</p>


