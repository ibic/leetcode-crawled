---
title: "Ugly Number II"
weight: 247
#id: "ugly-number-ii"
---
## Description
<div class="description">
<p>Write a program to find the <code>n</code>-th ugly number.</p>

<p>Ugly numbers are<strong> positive numbers</strong> whose prime factors only include <code>2, 3, 5</code>.&nbsp;</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> n = 10
<strong>Output:</strong> 12
<strong>Explanation: </strong><code>1, 2, 3, 4, 5, 6, 8, 9, 10, 12</code> is the sequence of the first <code>10</code> ugly numbers.</pre>

<p><strong>Note: </strong>&nbsp;</p>

<ol>
	<li><code>1</code> is typically treated as an ugly number.</li>
	<li><code>n</code> <b>does not exceed 1690</b>.</li>
</ol>
</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)
- Heap (heap)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Two levels of optimisation

Let's imagine that the problem is solved somehow for the number n
and we've put the solution directly in `nthUglyNumber` method
of the `Solution` class.

Now let's check the context: there are 596 test cases, 
for the most of them n is larger than 50, 
and n is known to be smaller than 1691. 

Hence instead of computing $$596 \times 50 = 29800$$ ugly numbers in total, 
one could precompute all 1690 numbers, and significantly speed up
the submission. 

> How to precompute? Use another class `Ugly` with all 
computations in the constructor
and then declare `Ugly` instance as a static variable of `Solution` class. 

Now let's consider two different approaches to perform the 
preliminary computations. 
<br /> 
<br />


---
#### Approach 1: Heap

**Intuition**

Let's start from the heap which contains just one number: 1.  
To compute next ugly numbers, pop 1 from the heap and
push instead three numbers: $$1 \times 2$$, $$1 \times 3$$,
and $$1 \times 5$$. 

Now the smallest number in the heap is 2. 
To compute next ugly numbers, pop 2 from the heap and
push instead three numbers: $$2 \times 2$$, $$2 \times 3$$,
and $$2 \times 5$$. 

> One could continue like this to compute first 1690 ugly numbers. 
At each step, pop the smallest ugly number k from the heap,
and push instead three ugly numbers: $$k \times 2$$, $$k \times 3$$,
and $$k \times 5$$.

!?!../Documents/264_SEC.json:1000,394!?!

**Algorithm**

- Precompute 1690 ugly numbers:

    - Initiate array of precomputed ugly numbers `nums`, heap `heap`
    and hashset `seen` to track all elements already pushed in the heap
    in order to avoid duplicates.
    
    - Make a loop of 1690 steps. At each step:
    
        - Pop the smallest element `k` out of heap and add it into the 
        array of precomputed ugly numbers.
        
        - Push `2k`, `3k` and `5k` in the heap if they are not yet 
        in the hashset. Update the hashset of seen ugly numbers as well.
        
- Retrieve needed ugly number from the array of precomputed numbers.

**Implementation**

<iframe src="https://leetcode.com/playground/QFPP5PWU/shared" frameBorder="0" width="100%" height="500" name="QFPP5PWU"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$ to retrieve preliminary 
computed ugly number, and more than $$12 \times 10^6$$ 
operations for preliminary computations. 
Let's estimate the number of operations needed for the preliminary 
computations. `For` loop here has 1690 steps, and each step performs
1 pop, not more than 3 pushes and 3 `contains / in` operations 
for the hashset. Pop and push have logarithmic time complexity 
and hence much cheaper than the linear search, so let's estimate only the
last term. This arithmetic progression is easy to estimate:

    $$
    1 + 2 + 3 + ... + 1690 \times 3 = \frac{(1 + 1690 \times 3) \times 1690 \times 3}{2} > 4.5 \times 1690^2
    $$


* Space complexity : constant space to keep an array of 1690 ugly numbers,
the heap of not more than $$1690 \times 2$$ elements and the hashset of
not more than $$1690 \times 3$$ elements.
<br /> 
<br />


---
#### Approach 2: Dynamic Programming

**Intuition**

Preliminary computations in Approach 1 are quite heavy, and could 
be optimised with dynamic programming. 

Let's start from the array of ugly numbers which contains just one
number - 1. Let's use three pointers $$i_2$$, $$i_3$$ and $$i_5$$,
to mark the last ugly number which was multiplied by 2, 3 and 5, 
correspondingly. 

The algorithm is straightforward: choose 
the smallest ugly number among $$2 \times \textrm{nums}[i_2]$$, $$3 \times \textrm{nums}[i_3]$$,
and $$5 \times \textrm{nums}[i_5]$$ and add it into the array. 
Move the corresponding pointer by one step. Repeat till you'll have
1690 ugly numbers.

!?!../Documents/264_LIS.json:1000,394!?!

**Algorithm**

- Precompute 1690 ugly numbers:

    - Initiate array of precomputed ugly numbers `nums` and three 
    pointers `i2`, `i3` and `i5` to track the index of the 
    last ugly number used to produce the next ones. 
    
    - Make a loop of 1690 steps. At each step:
    
        - Choose the smallest number among `nums[i2] * 2`, `nums[i3] * 3`,
        and `nums[i5] * 5` and add it into `nums`.
        
        - Move by one the pointer which corresponds to the "ancestor" 
        of the added number.     
        
- Retrieve needed ugly number from the array of precomputed numbers.

**Implementation**

<iframe src="https://leetcode.com/playground/XmPfY3uT/shared" frameBorder="0" width="100%" height="446" name="XmPfY3uT"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$ to retrieve preliminary 
computed ugly number, and about $$1690 \times 5 = 8450$$ 
operations for preliminary computations. 

* Space complexity : constant space to keep an array of 1690 ugly numbers.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My 16ms C++ DP solution with short explanation
- Author: jmty0083
- Creation Date: Thu Aug 20 2015 02:02:57 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 10:25:50 GMT+0800 (Singapore Standard Time)

<p>
We have an array *k* of first n ugly number. We only know, at the beginning, the first one, which is 1. Then

k[1] = min( k[0]x2, k[0]x3, k[0]x5). The answer is k[0]x2. So we move 2's pointer to 1. Then we test:

k[2] = min( k[1]x2, k[0]x3, k[0]x5). And so on. Be careful about the cases such as 6, in which we need to forward both pointers of 2 and 3. 

x here is multiplication.

    class Solution {
    public:
        int nthUglyNumber(int n) {
            if(n <= 0) return false; // get rid of corner cases 
            if(n == 1) return true; // base case
            int t2 = 0, t3 = 0, t5 = 0; //pointers for 2, 3, 5
            vector<int> k(n);
            k[0] = 1;
            for(int i  = 1; i < n ; i ++)
            {
                k[i] = min(k[t2]*2,min(k[t3]*3,k[t5]*5));
                if(k[i] == k[t2]*2) t2++; 
                if(k[i] == k[t3]*3) t3++;
                if(k[i] == k[t5]*5) t5++;
            }
            return k[n-1];
        }
    };
</p>


### O(n) Java solution
- Author: syftalent
- Creation Date: Wed Aug 19 2015 05:04:54 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 10:18:05 GMT+0800 (Singapore Standard Time)

<p>
The idea of this solution is from this page:http://www.geeksforgeeks.org/ugly-numbers/
   
The ugly-number sequence is 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, \u2026
because every number can only be divided by 2, 3, 5, one way to look at the sequence is to split the sequence to three groups as below:

    (1) 1\xd72, 2\xd72, 3\xd72, 4\xd72, 5\xd72, \u2026
    (2) 1\xd73, 2\xd73, 3\xd73, 4\xd73, 5\xd73, \u2026
    (3) 1\xd75, 2\xd75, 3\xd75, 4\xd75, 5\xd75, \u2026
   
We can find that every subsequence is the ugly-sequence itself (1, 2, 3, 4, 5, \u2026) multiply 2, 3, 5. 

Then we use similar merge method as merge sort, to get every ugly number from the three subsequence.    

Every step we choose the smallest one, and move one step after,including nums with same value.

Thanks for this author about this brilliant idea. Here is my java solution

    public class Solution {
        public int nthUglyNumber(int n) {
            int[] ugly = new int[n];
            ugly[0] = 1;
            int index2 = 0, index3 = 0, index5 = 0;
            int factor2 = 2, factor3 = 3, factor5 = 5;
            for(int i=1;i<n;i++){
                int min = Math.min(Math.min(factor2,factor3),factor5);
                ugly[i] = min;
                if(factor2 == min)
                    factor2 = 2*ugly[++index2];
                if(factor3 == min)
                    factor3 = 3*ugly[++index3];
                if(factor5 == min)
                    factor5 = 5*ugly[++index5];
            }
            return ugly[n-1];
        }
    }
</p>


### Elegant C++ Solution O(N) space time with detailed explanation.
- Author: fentoyal
- Creation Date: Tue Sep 15 2015 03:45:54 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 05:43:09 GMT+0800 (Singapore Standard Time)

<p>
    struct Solution {
        int nthUglyNumber(int n) {
            vector <int> results (1,1);
            int i = 0, j = 0, k = 0;
            while (results.size() < n)
            {
                results.push_back(min(results[i] * 2, min(results[j] * 3, results[k] * 5)));
                if (results.back() == results[i] * 2) ++i;
                if (results.back() == results[j] * 3) ++j;
                if (results.back() == results[k] * 5) ++k;
            }
            return results.back();
        }
    };

**Explanation:**

The key is to realize each number can be and have to be generated by a former number multiplied by 2, 3 or 5
e.g. 
1 2 3 4 5 6 8 9 10 12 15..
what is next?
it must be x * 2 or y * 3 or z * 5, where x, y, z is an existing number.

How do we determine x, y, z then?
apparently, you can just *traverse the sequence generated by far* from 1 ... 15, until you find such x, y, z that x * 2, y * 3, z * 5 is just bigger than 15. In this case x=8, y=6, z=4. Then you compare x * 2, y * 3, z * 5 so you know next number will be x * 2 = 8 * 2 = 16.
k, now you have 1,2,3,4,....,15, 16,

Then what is next?
You wanna do the same process again to find the new x, y, z, but you realize, wait, do I have to 
*traverse the sequence generated by far* again? 

NO! since you know last time, x=8, y=6, z=4 and x=8 was used to generate 16, so this time, you can immediately know the new_x = 9 (the next number after 8 is 9 in the generated sequence), y=6, z=4.
Then you need to compare new_x * 2, y * 3, z * 5. You know next number is 9 * 2 = 18;

And you also know, the next x will be 10 since new_x = 9 was used this time.
But what is next y? apparently, if y=6, 6*3 = 18, which is already generated in this round. So you also need to update next y from 6 to 8.

Based on the idea above, you can actually generated x,y,z from very beginning, and update x, y, z accordingly. It ends up with a O(n) solution.
</p>


