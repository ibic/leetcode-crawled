---
title: "Monthly Transactions I"
weight: 1545
#id: "monthly-transactions-i"
---
## Description
<div class="description">
<p>Table: <code>Transactions</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| country       | varchar |
| state         | enum    |
| amount        | int     |
| trans_date    | date    |
+---------------+---------+
id is the primary key of this table.
The table has information about incoming transactions.
The state column is an enum of type [&quot;approved&quot;, &quot;declined&quot;].
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find for each month and country, the number of transactions and their total amount, the number of approved transactions and their total amount.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Transactions</code> table:
+------+---------+----------+--------+------------+
| id   | country | state    | amount | trans_date |
+------+---------+----------+--------+------------+
| 121  | US      | approved | 1000   | 2018-12-18 |
| 122  | US      | declined | 2000   | 2018-12-19 |
| 123  | US      | approved | 2000   | 2019-01-01 |
| 124  | DE      | approved | 2000   | 2019-01-07 |
+------+---------+----------+--------+------------+

Result table:
+----------+---------+-------------+----------------+--------------------+-----------------------+
| month    | country | trans_count | approved_count | trans_total_amount | approved_total_amount |
+----------+---------+-------------+----------------+--------------------+-----------------------+
| 2018-12  | US      | 2           | 1              | 3000               | 1000                  |
| 2019-01  | US      | 1           | 1              | 2000               | 2000                  |
| 2019-01  | DE      | 1           | 1              | 2000               | 2000                  |
+----------+---------+-------------+----------------+--------------------+-----------------------+
</pre>

</div>

## Tags


## Companies
- Wayfair - 0 (taggedByAdmin: true)
- Wish - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple SQL
- Author: zzzzhu
- Creation Date: Fri Oct 04 2019 04:26:27 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 04 2019 04:26:27 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT 
LEFT(trans_date, 7) AS month, country, 
COUNT(id) AS trans_count, 
SUM(state = \'approved\') AS approved_count, 
SUM(amount) AS trans_total_amount, 
SUM(CASE 
    WHEN state = \'approved\' THEN amount 
    ELSE 0 
    END) AS approved_total_amount
FROM Transactions
GROUP BY month, country
```
</p>


### Simple MySQL
- Author: DL_IC_MCS15
- Creation Date: Sat Sep 21 2019 22:55:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 21 2019 22:55:34 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT
    DATE_FORMAT(trans_date, \'%Y-%m\') AS month,
    country,
    COUNT(id) AS trans_count,
    COUNT(IF(state = \'approved\', 1, NULL)) AS approved_count,
    SUM(amount) AS trans_total_amount,
    SUM(IF(state=\'approved\', amount, 0)) AS approved_total_amount
FROM Transactions
GROUP BY 1, 2
```
</p>


### MySQL - 100% Faster
- Author: msfittechchic92
- Creation Date: Wed Sep 18 2019 00:06:28 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 18 2019 00:06:28 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT DATE_FORMAT(trans_date, \'%Y-%m\') as month, country, COUNT(*) as trans_count, SUM(CASE WHEN state = \'approved\' THEN 1 ELSE 0 END) as approved_count, SUM(amount) as trans_total_amount, SUM(CASE WHEN state = \'approved\' THEN amount ELSE 0 END) as approved_total_amount
FROM Transactions
GROUP BY DATE_FORMAT(trans_date, \'%Y-%m\'), country;
```
</p>


