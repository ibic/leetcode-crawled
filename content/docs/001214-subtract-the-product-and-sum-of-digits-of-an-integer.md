---
title: "Subtract the Product and Sum of Digits of an Integer"
weight: 1214
#id: "subtract-the-product-and-sum-of-digits-of-an-integer"
---
## Description
<div class="description">
Given an integer number <code>n</code>, return the difference between the product of its digits and the sum of its digits.
<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 234
<strong>Output:</strong> 15 
<b>Explanation:</b> 
Product of digits = 2 * 3 * 4 = 24 
Sum of digits = 2 + 3 + 4 = 9 
Result = 24 - 9 = 15
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 4421
<strong>Output:</strong> 21
<b>Explanation: 
</b>Product of digits = 4 * 4 * 2 * 1 = 32 
Sum of digits = 4 + 4 + 2 + 1 = 11 
Result = 32 - 11 = 21
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Uber - 14 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Quora - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Straight Forward Solution
- Author: lee215
- Creation Date: Sun Dec 08 2019 12:04:27 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Dec 11 2019 01:08:32 GMT+0800 (Singapore Standard Time)

<p>
## **Complexity**
Time `O(logN)`
Space `O(1)`
<br>

**Java:**
```java
    public int subtractProductAndSum(int n) {
        int sum = 0, product = 1;
        while (n > 0) {
            sum += n % 10;
            product *= n % 10;
            n /= 10;
        }
        return product - sum;
    }
```

**C++:**
```cpp
    int subtractProductAndSum(int n) {
        int sum = 0, product = 1;
        for (; n > 0; n /= 10) {
            sum += n % 10;
            product *= n % 10;
        }
        return product - sum;
    }
```

**Python:**
Space `O(logN)`
```python
    def subtractProductAndSum(self, n):
        A = map(int, str(n))
        return reduce(operator.mul, A) - sum(A)
```

</p>


### [Java/Python 3] Straight forword code w/ analysis.
- Author: rock
- Creation Date: Sun Dec 08 2019 12:02:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 22 2019 13:04:59 GMT+0800 (Singapore Standard Time)

<p>

```java
    public int subtractProductAndSum(int n) {
        int sum = 0, prod = 1;
        while (n > 0) {
            int digit = n % 10;
            sum += digit;
            prod *= digit;
            n /= 10;
        } 
        return prod - sum;       
    }
```
```python
    def subtractProductAndSum(self, n: int) -> int:
        sum, prod = 0, 1
        while n:
            digit = n % 10
            sum += digit
            prod *= digit
            n //= 10
        return prod - sum
```
simplification using `divmod`, credit to **@ajc2001**
```python
    def subtractProductAndSum(self, n: int) -> int:
        sum, prod = 0, 1
        while n:
            n, digit = divmod(n, 10)
            sum += digit
            prod *= digit
        return prod - sum
```
**Analysis:**
Time: `O(logn)`, space: `O(1)`.
</p>


### [Javascript]  Efficient and Easy to Read -- 44ms, 98%
- Author: lochness
- Creation Date: Sat Dec 14 2019 05:55:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Dec 14 2019 05:55:04 GMT+0800 (Singapore Standard Time)

<p>
```
var subtractProductAndSum = function(n) {
    const digits = Array.from(String(n), Number)
    const sum = digits.reduce((a,b) => a+b)
    const product = digits.reduce((a,b) => a*b)
    return product-sum
};
```

[Array.from -- MDN docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from)
</p>


