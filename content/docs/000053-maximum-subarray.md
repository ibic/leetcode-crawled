---
title: "Maximum Subarray"
weight: 53
#id: "maximum-subarray"
---
## Description
<div class="description">
<p>Given an integer array <code>nums</code>, find the contiguous subarray&nbsp;(containing at least one number) which has the largest sum and return <em>its sum</em>.</p>

<p><strong>Follow up:</strong>&nbsp;If you have figured out the <code>O(n)</code> solution, try coding another solution using the <strong>divide and conquer</strong> approach, which is more subtle.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [-2,1,-3,4,-1,2,1,-5,4]
<strong>Output:</strong> 6
<strong>Explanation:</strong> [4,-1,2,1] has the largest sum = 6.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [0]
<strong>Output:</strong> 0
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [-1]
<strong>Output:</strong> -1
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> nums = [-2147483647]
<strong>Output:</strong> -2147483647
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 2 * 10<sup>4</sup></code></li>
	<li><code>-2<sup>31</sup> &lt;= nums[i] &lt;= 2<sup>31</sup> - 1</code></li>
</ul>

</div>

## Tags
- Array (array)
- Divide and Conquer (divide-and-conquer)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 23 (taggedByAdmin: false)
- Google - 16 (taggedByAdmin: false)
- Apple - 13 (taggedByAdmin: false)
- Bloomberg - 10 (taggedByAdmin: true)
- Facebook - 10 (taggedByAdmin: false)
- Cisco - 9 (taggedByAdmin: false)
- Microsoft - 8 (taggedByAdmin: true)
- Uber - 5 (taggedByAdmin: false)
- Adobe - 4 (taggedByAdmin: false)
- Goldman Sachs - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- LinkedIn - 10 (taggedByAdmin: true)
- Expedia - 3 (taggedByAdmin: false)
- Capital One - 3 (taggedByAdmin: false)
- Zillow - 3 (taggedByAdmin: false)
- JPMorgan - 3 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Morgan Stanley - 2 (taggedByAdmin: false)
- Alibaba - 4 (taggedByAdmin: false)
- eBay - 4 (taggedByAdmin: false)
- SAP - 4 (taggedByAdmin: false)
- Asana - 4 (taggedByAdmin: false)
- Atlassian - 4 (taggedByAdmin: false)
- Intel - 2 (taggedByAdmin: false)
- Two Sigma - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Palantir Technologies - 2 (taggedByAdmin: false)
- IBM - 2 (taggedByAdmin: false)
- Qualcomm - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Divide and Conquer

**Intuition**

The problem is a classical example of 
[divide and conquer approach](https://leetcode.com/explore/learn/card/recursion-ii/470/divide-and-conquer/), 
and can be solved with the algorithm similar with the merge sort.

Let's follow here a solution template for the divide and conquer problems :

- Define the base case(s).

- Split the problem into subproblems and solve them recursively.

- Merge the solutions for the subproblems to obtain the solution for the original problem.

**Algorithm**

maxSubArray for array with `n` numbers:

- If `n == 1` : return this single element.

- `left_sum` = maxSubArray for the left subarray, 
*i.e.* for the first `n/2` numbers (middle element 
at index `(left + right) / 2` always belongs to the left subarray).

- `right_sum` = maxSubArray for the right subarray, 
_i.e._ for the last `n/2` numbers.

- `cross_sum` = maximum sum of the subarray containing elements from 
both left and right subarrays and hence crossing the middle element at index
`(left + right) / 2`.

- Merge the subproblems solutions, *i.e.* return 
`max(left_sum, right_sum, cross_sum)`.

![pic](../Figures/53/dc.png)

**Implementation**

<iframe src="https://leetcode.com/playground/Mo5AuA4B/shared" frameBorder="0" width="100%" height="500" name="Mo5AuA4B"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N \log N)$$. 
Let's compute the solution with the help of 
[master theorem](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)) 
$$T(N) = aT\left(\frac{b}{N}\right) + \Theta(N^d)$$.
The equation represents dividing the problem 
up into $$a$$ subproblems of size $$\frac{N}{b}$$ in $$\Theta(N^d)$$ time. 
Here one divides the problem in two subproblemes `a = 2`, the size of each subproblem 
(to compute left and right subtree) is a half of initial problem `b = 2`, 
and all this happens in a $$\mathcal{O}(N)$$ time `d = 1`.
That means that $$\log_b(a) = d$$ and hence we're dealing with 
[case 2](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)#Application_to_common_algorithms)
that means $$\mathcal{O}(N^{\log_b(a)} \log N) = \mathcal{O}(N \log N)$$ time complexity.

* Space complexity : $$\mathcal{O}(\log N)$$ to keep the recursion stack.
<br /> 
<br />


---
#### Approach 2: Greedy

**Intuition**

The problem to find maximum (or minimum) element (or sum) with a single array as the input 
is a good candidate to be solved by the greedy approach in linear time.
One can find the examples of linear time greedy solutions in our articles of   
[Super Washing Machines](https://leetcode.com/articles/super-washing-machines/), 
and [Gas Problem](https://leetcode.com/articles/gas-station/).

> Pick the _locally_ optimal move at each step, 
and that will lead to the _globally_ optimal solution. 

The algorithm is general and straightforward: iterate over the array and 
update at each step the standard set for such problems:

- current element

- current _local_ maximum sum (at this given point)

- _global_ maximum sum seen so far. 

![bla](../Figures/53/greedy.png)

**Implementation**

<iframe src="https://leetcode.com/playground/wvetqNWP/shared" frameBorder="0" width="100%" height="259" name="wvetqNWP"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since it's one pass along the array. 

* Space complexity : $$\mathcal{O}(1)$$, since it's 
a constant space solution. 
<br /> 
<br />


---
#### Approach 3: Dynamic Programming (Kadane's algorithm)

**Intuition**

The problem to find sum or maximum or minimum in an entire array or
in a fixed-size sliding window
could be solved by the dynamic programming (DP) approach in linear time.

There are two standard DP approaches suitable for arrays:

- Constant space one. 
Move along the array and modify the array itself.

- Linear space one. First move in the direction `left->right`, then
in the direction `right->left`. Combine the results.
[Here is an example](https://leetcode.com/articles/sliding-window-maximum/). 

Let's use here the first approach since one could modify the array 
to track the current local maximum sum at this given point.

Next step is to update the _global_ maximum sum, knowing the _local_ one.

![fig](../Figures/53/dp.png)

**Implementation**

<iframe src="https://leetcode.com/playground/VomJqgei/shared" frameBorder="0" width="100%" height="225" name="VomJqgei"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since it's one pass along the array. 

* Space complexity : $$\mathcal{O}(1)$$, since it's 
a constant space solution.

## Accepted Submission (java)
```java
public class Solution {
	public int maxSubArray(int[] nums) {
		if (nums.length <= 0) {
			return 0;
		}
		// we do this inline
		int max = nums[0];
		for (int i = 1; i < nums.length; i++) {
		    int pn = nums[i - 1];
			if (pn > 0) {
				nums[i] += pn;
			}
			int cn = nums[i];
			if (cn > max) {
			    max = cn;
			}
		}

		return max;
	}
}
```

## Top Discussions
### DP solution & some thoughts
- Author: FujiwaranoSai
- Creation Date: Sat Dec 27 2014 16:43:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 11:26:53 GMT+0800 (Singapore Standard Time)

<p>
Analysis of this problem:
 Apparently, this is a optimization problem, which can be usually solved by DP. So when it comes to DP, the first thing for us to figure out is the format of the sub problem(or the state of each sub problem).  The format of the sub problem can be helpful when we are trying to come up with the recursive relation. 

At first, I think the sub problem should look like: `maxSubArray(int A[], int i, int j)`, which means the maxSubArray for A[i: j]. In this way, our goal is to figure out what `maxSubArray(A, 0, A.length - 1)` is. However, if we define the format of the sub problem in this way, it's hard to find the connection from the sub problem to the original problem(at least for me). In other words, I can't find a way to divided the original problem into the sub problems and use the solutions of the sub problems to somehow create the solution of the original one. 

So I change the format of the sub problem into something like: `maxSubArray(int A[], int i)`, which means the maxSubArray for A[0:i ] which must has A[i] as the end element. Note that now the sub problem's format is less flexible and less powerful than the previous one because there's a limitation that A[i] should be contained in that sequence and we have to keep track of each solution of the sub problem to update the global optimal value. However, now the connect between the sub problem & the original one becomes clearer:
 

    maxSubArray(A, i) = maxSubArray(A, i - 1) > 0 ? maxSubArray(A, i - 1) : 0 + A[i]; 

And here's the code

    public int maxSubArray(int[] A) {
            int n = A.length;
            int[] dp = new int[n];//dp[i] means the maximum subarray ending with A[i];
            dp[0] = A[0];
            int max = dp[0];
            
            for(int i = 1; i < n; i++){
                dp[i] = A[i] + (dp[i - 1] > 0 ? dp[i - 1] : 0);
                max = Math.max(max, dp[i]);
            }
            
            return max;
    }
</p>


### Accepted O(n) solution in java
- Author: cbmbbz
- Creation Date: Tue Nov 11 2014 12:32:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 04:23:02 GMT+0800 (Singapore Standard Time)

<p>
this problem was discussed by Jon Bentley (Sep. 1984 Vol. 27 No. 9 Communications of the ACM P885)

the paragraph below was copied from his paper (with a little modifications)

algorithm that operates on arrays: it starts at the left end (element A[1]) and scans through to the right end (element A[n]), keeping track of the maximum sum subvector seen so far. The maximum is initially A[0]. Suppose we've solved the problem for A[1 .. i - 1]; how can we extend that to A[1 .. i]? The maximum 
sum in the first I elements is either the maximum sum in the first i - 1 elements (which we'll call MaxSoFar), or it is that of a subvector that ends in position i (which we'll call MaxEndingHere).  

MaxEndingHere is either A[i] plus the previous MaxEndingHere, or just A[i], whichever is larger.

    public static int maxSubArray(int[] A) {
        int maxSoFar=A[0], maxEndingHere=A[0];
        for (int i=1;i<A.length;++i){
        	maxEndingHere= Math.max(maxEndingHere+A[i],A[i]);
        	maxSoFar=Math.max(maxSoFar, maxEndingHere);	
        }
        return maxSoFar;
    }
</p>


### Easy Python Way
- Author: _LeetCode
- Creation Date: Sun Apr 10 2016 20:07:01 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 03:10:55 GMT+0800 (Singapore Standard Time)

<p>
    for i in range(1, len(nums)):
            if nums[i-1] > 0:
                nums[i] += nums[i-1]
        return max(nums)
</p>


