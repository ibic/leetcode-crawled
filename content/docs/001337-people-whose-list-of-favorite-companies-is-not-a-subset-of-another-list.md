---
title: "People Whose List of Favorite Companies Is Not a Subset of Another List"
weight: 1337
#id: "people-whose-list-of-favorite-companies-is-not-a-subset-of-another-list"
---
## Description
<div class="description">
<p>Given the array <code>favoriteCompanies</code> where <code>favoriteCompanies[i]</code> is the list of favorites companies for the <code>ith</code> person (<strong>indexed from 0</strong>).</p>

<p><em>Return the indices of people whose list of favorite companies is not a <strong>subset</strong> of any other list of favorites companies</em>. You must return the indices&nbsp;in increasing order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> favoriteCompanies = [[&quot;leetcode&quot;,&quot;google&quot;,&quot;facebook&quot;],[&quot;google&quot;,&quot;microsoft&quot;],[&quot;google&quot;,&quot;facebook&quot;],[&quot;google&quot;],[&quot;amazon&quot;]]
<strong>Output:</strong> [0,1,4] 
<strong>Explanation:</strong> 
Person with index=2 has favoriteCompanies[2]=[&quot;google&quot;,&quot;facebook&quot;] which is a subset of favoriteCompanies[0]=[&quot;leetcode&quot;,&quot;google&quot;,&quot;facebook&quot;] corresponding to the person with index 0. 
Person with index=3 has favoriteCompanies[3]=[&quot;google&quot;] which is a subset of favoriteCompanies[0]=[&quot;leetcode&quot;,&quot;google&quot;,&quot;facebook&quot;] and favoriteCompanies[1]=[&quot;google&quot;,&quot;microsoft&quot;]. 
Other lists of favorite companies are not a subset of another list, therefore, the answer is [0,1,4].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> favoriteCompanies = [[&quot;leetcode&quot;,&quot;google&quot;,&quot;facebook&quot;],[&quot;leetcode&quot;,&quot;amazon&quot;],[&quot;facebook&quot;,&quot;google&quot;]]
<strong>Output:</strong> [0,1] 
<strong>Explanation:</strong> In this case favoriteCompanies[2]=[&quot;facebook&quot;,&quot;google&quot;] is a subset of favoriteCompanies[0]=[&quot;leetcode&quot;,&quot;google&quot;,&quot;facebook&quot;], therefore, the answer is [0,1].
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> favoriteCompanies = [[&quot;leetcode&quot;],[&quot;google&quot;],[&quot;facebook&quot;],[&quot;amazon&quot;]]
<strong>Output:</strong> [0,1,2,3]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;=&nbsp;favoriteCompanies.length &lt;= 100</code></li>
	<li><code>1 &lt;=&nbsp;favoriteCompanies[i].length &lt;= 500</code></li>
	<li><code>1 &lt;=&nbsp;favoriteCompanies[i][j].length &lt;= 20</code></li>
	<li>All strings in <code>favoriteCompanies[i]</code> are <strong>distinct</strong>.</li>
	<li>All lists of favorite companies are <strong>distinct</strong>, that is, If we sort alphabetically each list then <code>favoriteCompanies[i] != favoriteCompanies[j].</code></li>
	<li>All strings consist of lowercase English letters only.</li>
</ul>

</div>

## Tags
- String (string)
- Sort (sort)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Union-Find 37 ms beat 100% with detailed explanation
- Author: bambloo
- Creation Date: Sun May 17 2020 12:01:31 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 20 2020 08:25:21 GMT+0800 (Singapore Standard Time)

<p>
#### idea
Typical union-find
For example: 
favoriteCompanies = [["leetcode","google","facebook"],["google","microsoft"],["google","facebook"],["google"],["amazon"]]

it is actually a graph:
```
	{lgf}     {gm}    {a}
      |  
    {gf}
	  |
	 {g}
```

with path compression, the root of each node can be found faster:
```
	{lgf}     {gm}    {a}
	/   \
 {gf}   {g}
```
In the end, we just need to return the index of the three roots of the graph.

#### code
1. preprocess input, turn them into List<Set<String>>, please refer to the update below
2. initialize father of each list as itself
3. for i from 0 to length
	1. for j from i+1 to length
		1. let a = root of list i, let b = root of list j
		1. if a==b, it means list i and j have the same root, they are already in the same tree, do nothing
		1. else if a contains b, let a becomes b\'s root
		1. else if b contains a, let b becomes a\'s root
1. in the end, add all unique roots into a set, and return it in res

#### Complexity
the time complexity of union-find with path compression and without rank is between N + MlogN and N+Mlog*N, (according to https://www.cs.princeton.edu/~rs/AlgsDS07/01UnionFind.pdf)
where M is times of search, and N is the number of nodes in the tree

the space complexity is O(N), as we use a array and list with length N, where N is the size of input

#### update
Special thanks to @ekchang, he mentioned
> List#containsAll is dramatically worse than Set#containsAll. The former ends up doing N^2 traversal while the latter does N traversal with hash code look ups.
> A simple optimization to dramatically improve runtime (using List with union find barely avoids TLE!) is to preprocess input as List<Set<String>> which should cut runtime down from ~1800 ms to ~50 ms.

Here is the code

```
class Solution {
    public List<Integer> peopleIndexes(List<List<String>> favoriteCompanies) {
        List<Integer> res= new LinkedList<>();
        List<Set<String>> fcs = new LinkedList<>();
        for (List<String> fc: favoriteCompanies) fcs.add(new HashSet<>(fc));
        int l = fcs.size();
        int[] f = new int[l];
        for (int i=0; i<l; i++) f[i]=i;
        for (int i=0; i<l; i++)
            for (int j=i+1; j<l; j++){
                int a = find(f, i), b = find(f, j);
                if (a==b) continue;
                else if (contains(fcs.get(a), fcs.get(b))) f[b]=a;
                else if (contains(fcs.get(b), fcs.get(a))) f[a]=b;
            }
        Set<Integer> set= new HashSet<>();
        for (int i: f) set.add(find(f, i));
        res.addAll(set);
        Collections.sort(res);
        return res;
    }
    public boolean contains(Set<String> a, Set<String> b){
        if (a.size()<=b.size()) return false;
        return a.containsAll(b);
    }
    public int find(int[] f, int i){
        while (f[i]!=i){
            f[i]=f[f[i]];
            i=f[i];
        }
        return i;
    }
}
```

Happy coding~


</p>


### C++ Bitset
- Author: incoming
- Creation Date: Sun May 17 2020 12:16:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 12:51:07 GMT+0800 (Singapore Standard Time)

<p>
Thanks to @edaengineer for pointing out that bitset size should be 50k
```
class Solution {
public:
    vector<int> peopleIndexes(vector<vector<string>>& favoriteCompanies) {
        unordered_map<string, int> m;
        int id = 1, sz = favoriteCompanies.size();
		
        for (const auto &fc: favoriteCompanies) {
            for (const auto &c: fc) {
                if (m.count(c) > 0) continue;
                m[c] = id++;
            }
        }
		
        vector<bitset<50001>> vec;
        for (const auto &fc: favoriteCompanies) {
            bitset<50001> b;
            for (const auto &c: fc) {
                b[m[c]] = 1;
            }
            vec.push_back(b);
        }
		
        vector<int> res;
        for (int i = 0; i < sz; ++i) {
            bool flag = true;
            for (int j = 0; j < sz and flag; ++j) {
                if (i == j) continue;
                if ((vec[i] & vec[j]) == vec[i]) flag = false;
            }
            if (flag) res.push_back(i);
        }
		
        return res;
    }
};
```
</p>


### C++ includes
- Author: votrubac
- Creation Date: Sun May 17 2020 12:04:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 12:10:29 GMT+0800 (Singapore Standard Time)

<p>
Not fast, but simple. Uses `includes` to check if the first sorted set includes the second one. We can re-map strings to integer ids to make it a bit faster (second solution).
```cpp
vector<int> peopleIndexes(vector<vector<string>>& favs) {
	for (auto &comps : favs)
		sort(begin(comps), end(comps));
	vector<int> res;
	for (auto i = 0; i < favs.size(); ++i) {
		bool notSubset = true;
		for (auto j = 0; j < favs.size() && notSubset; ++j) {
			if (i == j)
				continue;
			notSubset = !includes(begin(favs[j]), end(favs[j]), begin(favs[i]), end(favs[i]));
		}
		if (notSubset)
			res.push_back(i);
	}
	return res;
}
```
**Optimized Solution**
```cpp
vector<int> peopleIndexes(vector<vector<string>>& favs) {
    unordered_map<string, int> ids;
    int id = 0;
    vector<vector<int>> comps(favs.size());
    for (auto i = 0; i < favs.size(); ++i) {
        for (auto &comp : favs[i]) {
            if (!ids.count(comp)) {
                ids[comp] = id++;
            }
            comps[i].push_back(ids[comp]);
        }            
    }
    for (auto &comp: comps)
        sort(begin(comp), end(comp));
    vector<int> res;
    for (auto i = 0; i < comps.size(); ++i) {
        bool notSubset = true;            
        for (auto j = 0; j < comps.size() && notSubset; ++j) {
            if (i == j)
                continue;
            notSubset = !includes(begin(comps[j]), end(comps[j]), begin(comps[i]), end(comps[i]));
        }
        if (notSubset)
            res.push_back(i);
    }
    return res;
}
```
</p>


