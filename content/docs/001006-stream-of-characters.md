---
title: "Stream of Characters"
weight: 1006
#id: "stream-of-characters"
---
## Description
<div class="description">
<p>Implement the <code>StreamChecker</code> class as follows:</p>

<ul>
	<li><code>StreamChecker(words)</code>: Constructor, init the data structure with the given words.</li>
	<li><code>query(letter)</code>: returns true if and only if for some <code>k &gt;= 1</code>, the last <code>k</code>&nbsp;characters queried (in order from oldest to newest, including this letter just queried) spell one of the words in the given list.</li>
</ul>

<p>&nbsp;</p>

<p><strong>Example:</strong></p>

<pre>
StreamChecker streamChecker = new StreamChecker([&quot;cd&quot;,&quot;f&quot;,&quot;kl&quot;]); // init the dictionary.
streamChecker.query(&#39;a&#39;);          // return false
streamChecker.query(&#39;b&#39;);          // return false
streamChecker.query(&#39;c&#39;);          // return false
streamChecker.query(&#39;d&#39;);          // return true, because &#39;cd&#39; is in the wordlist
streamChecker.query(&#39;e&#39;);          // return false
streamChecker.query(&#39;f&#39;);          // return true, because &#39;f&#39; is in the wordlist
streamChecker.query(&#39;g&#39;);          // return false
streamChecker.query(&#39;h&#39;);          // return false
streamChecker.query(&#39;i&#39;);          // return false
streamChecker.query(&#39;j&#39;);          // return false
streamChecker.query(&#39;k&#39;);          // return false
streamChecker.query(&#39;l&#39;);          // return true, because &#39;kl&#39; is in the wordlist
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>1 &lt;= words.length &lt;= 2000</code></li>
	<li><code>1 &lt;= words[i].length &lt;= 2000</code></li>
	<li>Words will only consist of lowercase English letters.</li>
	<li>Queries will only consist of lowercase English letters.</li>
	<li>The number of queries is at most&nbsp;40000.</li>
</ul>

</div>

## Tags
- Trie (trie)

## Companies
- Google - 2 (taggedByAdmin: true)
- Facebook - 4 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Prerequisites

This design article uses [data structure Trie](https://leetcode.com/articles/add-and-search-word/). If you never worked
with trie before, you might want to check [this introduction article](https://leetcode.com/articles/add-and-search-word/)
first to simplify the further reading.

#### Approach 1: Trie

**Trie**

Trie is widely used in real life:
autocomplete search, spell checker, T9 predictive text, 
[IP routing (longest prefix matching)](https://www.researchgate.net/figure/An-example-routing-table-and-the-corresponding-binary-trie-built-from-it_fig3_4236637), 
[some GCC containers](https://gcc.gnu.org/onlinedocs/libstdc++/ext/pb_ds/trie_based_containers.html).

Trie is something to think about if you're asked to design 
a structure to dynamically add and search strings.

**Intuition**

The first idea is to add all input words in the trie and then implement 
a standard search.

![fig](../Figures/1032/naive4.png)
{:align="center"}

*Figure 1. Naive implementation.*
{:align="center"}

The problem is we don't know how many characters to match.
On the example above, should we try to match the last three stream characters
"jkl", the last two "kl", or the last one "l"?  

The way to solve the problem is to notice that we always know 
the last character to match. That gives us an idea to build a trie
of _reversed_ words, and try to match the _reversed_ stream of characters.

![fig](../Figures/1032/reverse.png)
{:align="center"}

*Figure 2. Trie of the reversed words and the reversed stream of characters.*
{:align="center"}

This way, instead of multiple choices to match, we always have one path: 
to match character by character starting from the end of the stream.
We could stop once we meet the "end of word" label, which means success.
If we couldn't match a character before we meet that label, that means fail.

**Constructor StreamChecker**

Trie is usually implemented as nested hashmaps.
At each step, one has to verify if the child node to add is already present. If yes, 
we go one level down. If not, we add the node into a trie and then go one step down.
The particularity of the current problem is that we add 
in the trie the _reversed_ words. 

The last thing to discuss is how to store the reversed stream of characters.
For that, we need a structure for which `appendleft` / `addFirst` 
operation takes a constant time. The good choice here is 
_double ended queue_, it's implemented as [deque](https://docs.python.org/3/library/collections.html#collections.deque)
in Python, and as [ArrayDeque](https://docs.oracle.com/javase/8/docs/api/java/util/ArrayDeque.html) in Java.

<iframe src="https://leetcode.com/playground/4ef8bHhB/shared" frameBorder="0" width="100%" height="446" name="4ef8bHhB"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N \cdot M)$$, where 
$$N$$ is a number of input words, and $$M$$ is the word length.
We have $$N$$ words to process. 
At each step, we either examine or create a node in the trie. 
That takes only $$M$$ operations.

* Space complexity: $$\mathcal{O}(N \cdot M)$$. 
In the worst-case newly inserted 
key doesn't share a prefix with the keys already added in the trie. 
We have to add $$N \cdot M$$ new nodes, which takes 
$$\mathcal{O}(N \cdot M)$$ space. 

**Query Implementation**

The search is very straightforward: we start from the end of the stream
and check character by character, going down in trie. 

![fig](../Figures/1032/query.png)
{:align="center"}

*Figure 3. Search in trie.*
{:align="center"}

<iframe src="https://leetcode.com/playground/hHQcMjV8/shared" frameBorder="0" width="100%" height="310" name="hHQcMjV8"></iframe>

* Time complexity: $$\mathcal{O}(M)$$, where $$M$$ is a max word length,
_i.e._ the depth of trie.

* Space complexity: $$\mathcal{O}(M)$$ to keep a stream of characters.
One could limit the size of deque to be equal to the length of the longest
input word. 

**Implementation**

Let's bring everything together. 

<iframe src="https://leetcode.com/playground/jUJVNMHN/shared" frameBorder="0" width="100%" height="500" name="jUJVNMHN"></iframe>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Trie Solution
- Author: Self_Learner
- Creation Date: Sun Apr 21 2019 12:14:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 21 2019 12:14:06 GMT+0800 (Singapore Standard Time)

<p>
Store the words in the trie with reverse order, and check the query string from the end
```
class StreamChecker {
    
    class TrieNode {
        boolean isWord;
        TrieNode[] next = new TrieNode[26];
    }

    TrieNode root = new TrieNode();
    StringBuilder sb = new StringBuilder();

    public StreamChecker(String[] words) {
        createTrie(words);
    }

    public boolean query(char letter) {
        sb.append(letter);
        TrieNode node = root;
        for (int i = sb.length() - 1; i >= 0 && node != null; i--) {
            char c = sb.charAt(i);
            node = node.next[c - \'a\'];
            if (node != null && node.isWord) {
                return true;
            }
        }
        return false;
    }

    private void createTrie(String[] words) {
        for (String s : words) {
            TrieNode node = root;
            int len = s.length();
            for (int i = len - 1; i >= 0; i--) {
                char c = s.charAt(i);
                if (node.next[c - \'a\'] == null) {
                    node.next[c - \'a\'] = new TrieNode();
                }
                node = node.next[c - \'a\'];
            }
            node.isWord = true;
        }
    }
}
</p>


### [Python] Trie Solution with Explanation
- Author: lee215
- Creation Date: Sat Apr 20 2019 17:04:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Feb 15 2020 15:04:39 GMT+0800 (Singapore Standard Time)

<p>
# **We say**
`W = max(words.length)`,the maximum length of all words.
`N = words.size`, the number of words
`Q`, the number of calls of function `query`
<br>

# **Solution 1: Check all words (TLE)**
If we save the whole input character stream and compare with `words` one by one,
The time complexity for each `query` will be `O(NW)`,
depending on the size of `words`.
<br>

# **Solution 2: Check Query Suffixes (Maybe AC, Maybe TLE)**

While the `words.size` can be really big,
the number of the suffixes of query stream is bounded.

For example, if the query stream is `"abcd"`,
the suffix can be `"abcd"`, `"bcd"`, `"cd"`, `"d"`.

We can save all hashed words to a set.
For each query, we check query stream\'s all suffixes.
The maximum length of words is `W`, we need to check `W` suffixes.

The time complexity for each `query` will be `O(W)` if we take the set search as `O(1)`.
The overall time is `O(WQ)`.
<br>

# **Solution 3: Trie (Accepted)**
Only a part of suffixes can be the prefix of a word,
waiting for characters coming to form a complete word.
Instead of checking all `W` suffixes in each query,
we can just save those possible waiting prefixes in a `waiting` list.

**Explanation**
Initialization:
1. Construct a trie
2. declare a global waiting list.

Query:
1. for each node in the `waiting` list,
check if there is child node for the new character.
If so, add it to the new waiting list.
2. return true if any node in the `waiting`list is the end of a word.

**Time Complexity**:
`waiting.size <= W`, where `W` is the maximum length of words.
So that `O(query) = O(waiting.size) = O(W)`
We will make `Q` queries, the overall time complexity is `O(QW)`

Note that it has same complexity in the worst case as solution 2 (like "aaaaaaaa" for words and query),
In general cases, it saves time checking all suffixes, and also the set search in a big set.

**Space Complexity**:

`waiting.size <= W`, where `W` is the maximum length of words.
`waiting` list will take `O(W)`

Assume we have initially `N` words, at most `N` leaves in the `trie`.
The size of trie is `O(NW)`.

**Python:**
Time: 6000+ms
```py
class StreamChecker(object):

    def __init__(self, words):
        T = lambda: collections.defaultdict(T)
        self.trie = T()
        for w in words: reduce(dict.__getitem__, w, self.trie)[\'#\'] = True
        self.waiting = []

    def query(self, letter):
        self.waiting = [node[letter] for node in self.waiting + [self.trie] if letter in node]
        return any("#" in node for node in self.waiting)
```
<br>

# Solution 4: Construct Trie with Reversed Words

Time: 600 ~ 700ms
Time complexity: `O(WQ)`

**Python**
```py
    def __init__(self, words):
        T = lambda: collections.defaultdict(T)
        self.trie = T()
        for w in words: reduce(dict.__getitem__, w[::-1], self.trie)[\'#\'] = True
        self.S = ""
        self.W = max(map(len, words))

    def query(self, letter):
        self.S = (letter + self.S)[:self.W]
        cur = self.trie
        for c in self.S:
            if c in cur:
                cur = cur[c]
                if cur[\'#\'] == True:
                    return True
            else:
                break
        return False
```

</p>


### Easily-implemented Python Trie Solution
- Author: otoc
- Creation Date: Thu Jun 27 2019 06:10:41 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 21 2019 01:00:56 GMT+0800 (Singapore Standard Time)

<p>
Please see and vote for my solutions for
[208. Implement Trie (Prefix Tree)](https://leetcode.com/problems/implement-trie-prefix-tree/discuss/320224/Simple-Python-solution)
[1233. Remove Sub-Folders from the Filesystem](https://leetcode.com/problems/remove-sub-folders-from-the-filesystem/discuss/409075/standard-python-prefix-tree-solution)
[1032. Stream of Characters](https://leetcode.com/problems/stream-of-characters/discuss/320837/Standard-Python-Trie-Solution)
[211. Add and Search Word - Data structure design](https://leetcode.com/problems/add-and-search-word-data-structure-design/discuss/319361/Simple-Python-solution)
[676. Implement Magic Dictionary](https://leetcode.com/problems/implement-magic-dictionary/discuss/320197/Simple-Python-solution)
[677. Map Sum Pairs](https://leetcode.com/problems/map-sum-pairs/discuss/320237/Simple-Python-solution)
[745. Prefix and Suffix Search](https://leetcode.com/problems/prefix-and-suffix-search/discuss/320712/Different-Python-solutions-with-thinking-process)
[425. Word Squares](https://leetcode.com/problems/word-squares/discuss/320916/Easily-implemented-Python-solution%3A-Backtrack-%2B-Trie)

```
class TrieNode():
    def __init__(self):
        self.children = {}
        self.isEnd = False

class Trie():
    def __init__(self):
        self.root = TrieNode()
    
    def insert(self, word):
        node = self.root
        for char in word:
            if char not in node.children:
                node.children[char] = TrieNode()
            node = node.children[char]
        node.isEnd = True

class StreamChecker:
    def __init__(self, words: List[str]):
        self.letters = []
        self.trie = Trie()
        for w in words:
            self.trie.insert(w[::-1])
        
    def query(self, letter: str) -> bool:
        self.letters.append(letter)
        i = len(self.letters) - 1
        node = self.trie.root
        while i >= 0:
            if node.isEnd:
                return True
            if self.letters[i] not in node.children:
                return False
            node = node.children[self.letters[i]]
            i -= 1
        return node.isEnd
```
</p>


