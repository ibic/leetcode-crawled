---
title: "Delete Columns to Make Sorted II"
weight: 905
#id: "delete-columns-to-make-sorted-ii"
---
## Description
<div class="description">
<p>We are given an array&nbsp;<code>A</code> of <code>N</code> lowercase letter strings, all of the same length.</p>

<p>Now, we may choose any set of deletion indices, and for each string, we delete all the characters in those indices.</p>

<p>For example, if we have an array <code>A = [&quot;abcdef&quot;,&quot;uvwxyz&quot;]</code> and deletion indices <code>{0, 2, 3}</code>, then the final array after deletions is <code>[&quot;bef&quot;,&quot;vyz&quot;]</code>.</p>

<p>Suppose we chose a set of deletion indices <code>D</code> such that after deletions, the final array has its elements in <strong>lexicographic</strong> order (<code>A[0] &lt;= A[1] &lt;= A[2] ... &lt;= A[A.length - 1]</code>).</p>

<p>Return the minimum possible value of <code>D.length</code>.</p>

<p>&nbsp;</p>

<div>
<div>
<ol>
</ol>
</div>
</div>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;ca&quot;,&quot;bb&quot;,&quot;ac&quot;]</span>
<strong>Output: </strong><span id="example-output-1">1</span>
<strong>Explanation: </strong>
After deleting the first column, A = [&quot;a&quot;, &quot;b&quot;, &quot;c&quot;].
Now A is in lexicographic order (ie. A[0] &lt;= A[1] &lt;= A[2]).
We require at least 1 deletion since initially A was not in lexicographic order, so the answer is 1.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span>[&quot;xc&quot;,&quot;yb&quot;,&quot;za&quot;]</span>
<strong>Output: </strong><span id="example-output-2">0</span>
<strong>Explanation: </strong>
A is already in lexicographic order, so we don&#39;t need to delete anything.
Note that the rows of A are not necessarily in lexicographic order:
ie. it is NOT necessarily true that (A[0][0] &lt;= A[0][1] &lt;= ...)
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[&quot;zyx&quot;,&quot;wvu&quot;,&quot;tsr&quot;]</span>
<strong>Output: </strong><span id="example-output-3">3</span>
<strong>Explanation: </strong>
We have to delete every column.
</pre>

<p>&nbsp;</p>

<div>
<div>
<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 100</code></li>
	<li><code>1 &lt;= A[i].length &lt;= 100</code></li>
</ol>
</div>
</div>
</div>
</div>
</div>

</div>

## Tags
- Greedy (greedy)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Greedy

**Intuition**

Instead of thinking about column deletions, let's think about which columns we will keep in the final answer.

If the first column isn't lexicographically sorted, we have to delete it.

Otherwise, we will argue that we can keep this first column without consequence.  There are two cases:

* If we don't keep the first column, then the final rows of the answer all have to be sorted.

* If we do keep the first column, then the final rows of the answer (minus the first column) only have to be sorted if they share the same first letter (coming from the first column).

  The above statement is hard to digest, so let's use an example:

  Say we have `A = ["axx","ayy","baa","bbb","bcc"]`.  When we keep the first column, the final rows are `R = ["xx","yy","aa","bb","cc"]`, and instead of the requirement that these all have to be sorted (ie. `R[0] <= R[1] <= R[2] <= R[3] <= R[4]`), we have a weaker requirement that they only have to be sorted if they share the same first letter of the first column, (ie. `R[0] <= R[1] and R[2] <= R[3] <= R[4]`).

Now, we applied this argument only for the first column, but it actually works for every column we could consider taking.  If we can't take a column, we have to delete it.  Otherwise, we take it because it can only make adding subsequent columns easier.

**Algorithm**

All our effort has led us to a simple algorithmic idea.

Start with no columns kept.  For each column, if we could keep it and have a valid answer, keep it - otherwise delete it.


<iframe src="https://leetcode.com/playground/hUKxm9un/shared" frameBorder="0" width="100%" height="500" name="hUKxm9un"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(NW^2)$$, where $$N$$ is the length of `A`, and $$W$$ is the length of `A[i]`.

* Space Complexity:  $$O(NW)$$.
<br />
<br />


---
#### Approach 2: Greedy with Optimizations

**Explanation**

It is also possible to implement the solution in *Approach 1* without using as much time and space.

The key idea is that we will record the "cuts" that each column makes.  In our first example from *Approach 1* with `A = ["axx","ayy","baa","bbb","bcc"]` (and `R` defined as in Approach 1), the first column cuts our condition from `R[0] <= R[1] <= R[2] <= R[3] <= R[4]` to `R[0] <= R[1]` and `R[2] <= R[3] <= R[4]`.  That is, the boundary `"a" == column[1] != column[2] == "b"` has 'cut' one of the conditions for `R` out.

At a high level, our algorithm depends on evaluating whether adding a new column will keep all the rows sorted.  By maintaining information about these cuts, we only need to compare characters in the newest column.


<iframe src="https://leetcode.com/playground/8imLqD22/shared" frameBorder="0" width="100%" height="500" name="8imLqD22"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(NW)$$, where $$N$$ is the length of `A`, and $$W$$ is the length of `A[i]`.

* Space Complexity:  $$O(N)$$ in additional space complexity.  (In Python, `zip(*A)` uses $$O(NW)$$ space.)
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Greedy Solution, O(MN)
- Author: lee215
- Creation Date: Sun Dec 09 2018 12:07:57 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 27 2019 15:03:41 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Solve it with a greed algorithme.

Initial `N` empty string.
For each column,
add the character to each row,
and check if all rows are still sorted.

If not, we have to delete this column.
<br>

## **Explanation**
Initial a boolean array `sorted`,
`sorted[i] = true` if and only if `A[i] < A[i + 1]`,
that is to say `A[i]` and `A[i + 1]` are sorted.

For each col, we check all unsorted rows.
If `A[i][j] > A[i + 1][j]`, we need to deleted this col, `res++`.

Otherwise, we can keep this col, and update all sorted rows.
<br>


**Java, Using array, O(MN), 14ms**
```
    public int minDeletionSize(String[] A) {
        int res = 0, n = A.length, m = A[0].length(), i, j;
        boolean[] sorted = new boolean[n - 1];
        for (j = 0; j < m; ++j) {
            for (i = 0; i < n - 1; ++i) {
                if (!sorted[i] && A[i].charAt(j) > A[i + 1].charAt(j)) {
                    res++;
                    break;
                }
            }
            if (i < n - 1) continue;
            for (i = 0; i < n - 1; ++i) {
                sorted[i] |= A[i].charAt(j) < A[i + 1].charAt(j);
            }
        }
        return res;
    }
```

**C++, Using vector, O(MN), 8ms**
```
    int minDeletionSize(vector<string>& A) {
        int res = 0, n = A.size(), m = A[0].length(), i, j;
        vector<bool> sorted(n - 1, false);
        for (j = 0; j < m; ++j) {
            for (i = 0; i < n - 1; ++i) {
                if (!sorted[i] && A[i][j] > A[i + 1][j]) {
                    res++;
                    break;
                }
            }
            if (i < n - 1) continue;
            for (i = 0; i < n - 1; ++i) {
                sorted[i] = sorted[i] || A[i][j] < A[i + 1][j];
            }
        }
        return res;
    }
```

**Python, Using list, `O(NM)`, 64ms**
```
    def minDeletionSize(self, A):
        res, n, m = 0, len(A), len(A[0])
        is_sorted = [0] * (n - 1)
        for j in range(m):
            is_sorted2 = is_sorted[:]
            for i in range(n - 1):
                if A[i][j] > A[i + 1][j] and is_sorted[i] == 0:
                    res += 1
                    break
                is_sorted2[i] |= A[i][j] < A[i + 1][j]
            else:
                is_sorted = is_sorted2
        return res
```
<br>

## More
Other version of python solution.

**Python, Brute force, `O(NlogN M^2)`\uFF0C 230ms**
```
    def minDeletionSize(self, A):
        res = 0
        cur = [""] * len(A)
        for col in zip(*A):
            cur2 = zip(cur, col)
            if cur2 == sorted(cur2): cur = cur2
            else: res += 1
        return res
```


**Python, Using set, `O(NM)`, 24ms**
```
    def minDeletionSize(self, A):
        res, n, m = 0, len(A), len(A[0])
        unsorted = set(range(n - 1))
        for j in range(m):
            if any(A[i][j] > A[i + 1][j] for i in unsorted):
                res += 1
            else:
                unsorted -= {i for i in unsorted if A[i][j] < A[i + 1][j]}
        return res
```

</p>


### C++ 12 ms brute force
- Author: votrubac
- Creation Date: Sun Dec 09 2018 12:03:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 09 2018 12:03:40 GMT+0800 (Singapore Standard Time)

<p>
We analyze words one-by-one. If in a word ```i``` a character ```j``` breaks the lexicographic order (```A[i - 1][j] > A[i][j]```), we add ```j``` column to the list of deleted columns (I am using a hash set for O(1) operations).

After we deleted a column, we need to restart the process from the beginning. The reason is that we a deleted column can break the lexicographic order for words we already analyzed. Note that now we ignore all columns that are already in the "deleted" list.
```
int minDeletionSize(vector<string>& A) {
    unordered_set<int> di;
    for (auto i = 1; i < A.size(); ++i) {
        for (auto j = 0; j < A[i].size(); ++j) {
            if (di.count(j) > 0 || A[i - 1][j] == A[i][j]) continue;
            if (A[i - 1][j] > A[i][j]) {
                di.insert(j);
                i = 0;
            }
            break;
        }
    }
    return di.size();
}
```
</p>


### python easy solution, beats 100%
- Author: mickey0524
- Creation Date: Mon Dec 10 2018 12:34:44 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 10 2018 12:34:44 GMT+0800 (Singapore Standard Time)

<p>
We use an array to record whether we need to compare

```
from copy import copy

class Solution(object):
    def minDeletionSize(self, A):
        """
        :type A: List[str]
        :rtype: int
        """
        A_len, str_len, res = len(A), len(A[0]), 0
        is_need_compare = [True] * A_len

        for i in xrange(str_len):
            tmp = copy(is_need_compare)
            for j in xrange(1, A_len):
                if is_need_compare[j]:
                    if A[j][i] < A[j - 1][i]:
                        res += 1
                        is_need_compare = tmp
                        break

                    elif A[j][i] > A[j - 1][i]:
                        is_need_compare[j] = False
                            
        return res
```
</p>


