---
title: "Number of Ways Where Square of Number Is Equal to Product of Two Numbers"
weight: 1440
#id: "number-of-ways-where-square-of-number-is-equal-to-product-of-two-numbers"
---
## Description
<div class="description">
<p>Given two arrays of integers&nbsp;<code>nums1</code> and <code>nums2</code>, return the number of triplets formed (type 1 and type 2) under the following rules:</p>

<ul>
	<li>Type 1: Triplet (i, j, k)&nbsp;if <code>nums1[i]<sup>2</sup>&nbsp;== nums2[j] * nums2[k]</code> where <code>0 &lt;= i &lt; nums1.length</code> and <code>0 &lt;= j &lt; k &lt; nums2.length</code>.</li>
	<li>Type 2:&nbsp;Triplet (i, j, k) if <code>nums2[i]<sup>2</sup>&nbsp;== nums1[j] * nums1[k]</code> where <code>0 &lt;= i &lt; nums2.length</code> and <code>0 &lt;= j &lt; k &lt; nums1.length</code>.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [7,4], nums2 = [5,2,8,9]
<strong>Output:</strong> 1
<strong>Explanation:</strong> Type 1: (1,1,2), nums1[1]^2 = nums2[1] * nums2[2]. (4^2 = 2 * 8). 
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [1,1], nums2 = [1,1,1]
<strong>Output:</strong> 9
<strong>Explanation:</strong> All Triplets are valid, because 1^2 = 1 * 1.
Type 1: (0,0,1), (0,0,2), (0,1,2), (1,0,1), (1,0,2), (1,1,2).  nums1[i]^2 = nums2[j] * nums2[k].
Type 2: (0,0,1), (1,0,1), (2,0,1). nums2[i]^2 = nums1[j] * nums1[k].
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [7,7,8,3], nums2 = [1,2,9,7]
<strong>Output:</strong> 2
<strong>Explanation:</strong> There are 2 valid triplets.
Type 1: (3,0,2).  nums1[3]^2 = nums2[0] * nums2[2].
Type 2: (3,0,1).  nums2[3]^2 = nums1[0] * nums1[1].
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [4,7,9,11,23], nums2 = [3,5,1024,12,18]
<strong>Output:</strong> 0
<strong>Explanation:</strong> There are no valid triplets.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums1.length, nums2.length &lt;= 1000</code></li>
	<li><code>1 &lt;= nums1[i], nums2[i] &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Math (math)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++, 20ms, 12MB, max(O(n1+n2), O(d1*d2)) (d1, d2 are number of distinct numbers in nums1 and nums2)
- Author: alvin-777
- Creation Date: Sun Sep 06 2020 13:27:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 14:32:50 GMT+0800 (Singapore Standard Time)

<p>
Firstly we count how many times each number appeared in each vector.

For each distinct number n1 in one set, we try to find in the other set:
  1. `n2 == n1` and n2 appeared at least twice
  2. `n2 < n1` and exists n2x that `n2*n2x == n1*n1` => `n2x == n1*n1/n2`

For `1`, if n1 appeared f1 times and n2 appeared f2 times, the total number of combinations is: `f1 * C(f2, 2)` = `f1 * (f2*(f2-1)/2)`
For `2`, if we can find both n2 and n2x, appeared f2 and f2x times respectively, the total number of combinations is: `f1 * (f2*f2x)` (note when n2 < n1, n2x > n1, we don\'t want to double count, so only check n2 < n1)

We count for type 1, switch them over and count for type 2, finally return the sum.

----

Complexity analysis:

1. Counting number frequency is `O(n1 + n2)`
2. Counting combinations, for each distinct number in one set, we check all numbers in the other set, then switch over and do it again: `O(freq1.size() * freq2.size() * 2)`, let d1=freq1.size(), d2=freq2.size() => `O(d1*d2)`

Either `1` or `2` can take longer time, for example, if we have all same numbers like `[1,1,1,1,...,1,1]` and `[1,1,1,1,...,1,1]`, `1` takes much longer time than `2`. On the other hand if we have a lot of distinct numbers then `2` will be longer.
So the overall time complexity is `max(O(n1+n2), O(d1*d2))`.

We use 2 hash maps to store the frequencies, so space complexity is `O(d1+d2)`.

```cpp
int numTriplets(vector<int>& nums1, vector<int>& nums2) {
    unordered_map<int64_t, int> freq1, freq2;
    for (int n : nums1) freq1[n]++;
    for (int n : nums2) freq2[n]++;

    auto Count = [](unordered_map<int64_t, int> &freqX, unordered_map<int64_t, int> &freqY) {
        int cnt = 0;
        for (auto [n1, f1] : freqX) {
            int c = 0;
            int64_t n1sq = n1*n1;
            for (auto [n2, f2] : freqY) {
                if (n2 == n1) {
                    if (f2 > 1) c += f2*(f2 - 1)/2;
                } else if (n2 < n1 && n1sq % n2 == 0) {
                    int n2x = n1sq/n2;
                    if (freqY.count(n2x)) c += f2*freqY[n2x];
                }
            }
            cnt += c*f1;
        }
        return cnt;
    };

    return Count(freq1, freq2) + Count(freq2, freq1);
}
```

</p>


### C++/Java Two Sum O(n * m)
- Author: votrubac
- Creation Date: Sun Sep 06 2020 12:00:32 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 07 2020 03:26:29 GMT+0800 (Singapore Standard Time)

<p>
This looks very similar to Two Sum, and we will use [Two Sum: Approach 3](https://leetcode.com/problems/two-sum/solution/).

We just need to call our function "Two Product" :)

> Update: see below for the optimized version!

**C++**
```cpp
int numTriplets(vector<int>& n1, vector<int>& n2) {
    return accumulate(begin(n1), end(n1), 0, [&](int s, long n) { return s + twoProduct(n * n, n2); }) 
        + accumulate(begin(n2), end(n2), 0, [&](int s, long n) { return s +  twoProduct(n * n, n1); });
}
int twoProduct(long i, vector<int> &n) {
    unordered_map<int, int> m;
    int cnt = 0;
    for (auto v : n) {
        if (i % v == 0)
            cnt += m[i / v];
        ++m[v];
    }
    return cnt;
} 
```

**Java**
```java
public int numTriplets(int[] n1, int[] n2) {
    long res = 0;
    for (long n : n1)
        res += twoProduct(n * n, n2);
    for (long n : n2)
        res += twoProduct(n * n, n1);
    return (int)res;
}
long twoProduct(long i, int[] n) {
    Map<Long, Long> m = new HashMap<>();
    long cnt = 0;
    for (long v : n) {
        if (i % v == 0)
            cnt += m.getOrDefault(i / v, 0l);
        m.put(v, m.getOrDefault(v, 0l) + 1);
    }
    return cnt;
}  
```

#### Optimized Version
We can memoise the result for a given `n`. This will speed up things quite a bit if there is a lot of duplicates. We can use a hashmap, or we can sort the array so duplicates are next to each other. The solution below is an improvement from ~800 ms to ~280 ms.

**C++**
```cpp
int numTriplets(vector<int>& n1, vector<int>& n2) {
    return countForArray(n1, n2) + countForArray(n2, n1);
}
int countForArray(vector<int>& n1, vector<int>& n2) {
    int res = 0, last_res = 0, last_n = 0;
    sort(begin(n1), end(n1));
    for (auto i = 0; i < n1.size(); last_n = n1[i++])
        if (n1[i] == last_n)
            res += last_res;
        else
            res += last_res = twoProduct((long)n1[i] * n1[i], n2);
    return res;
}
int twoProduct(long i, vector<int> &n) {
    unordered_map<int, int> m;
    int cnt = 0;
    for (auto v : n) {
        if (i % v == 0)
            cnt += m[i / v];
        ++m[v];
    }
    return cnt;
} 
```
</p>


### C++ O(MN) with explanation
- Author: lzl124631x
- Creation Date: Sun Sep 06 2020 12:02:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 09 2020 15:15:28 GMT+0800 (Singapore Standard Time)

<p>
See my latest update in repo [LeetCode](https://github.com/lzl124631x/LeetCode)

## Solution 1.

Type 1 and Type 2 are symmetrical so we can define a function `count(A, B)` which returns the count of the Type 1 triples. The answer is `count(A, B) + count(B, A)`.

For `count(A, B)`, we can use a `unordered_map<int, int> m` to store the frequency of the numbers in `B`. Then for each number `a` in `A`, the `target` value is `a * a`. We traverse the map `m` to count the triplets.

For each entry `(b, cnt)` in `m`:
* If `target` is not divisible by `b` or `target / b` is not in `m`, there is no triplets, skip.
* If `target / b == b`, we need to pick 2 out of `cnt` numbers so we can add `cnt * (cnt - 1)` triplets to the answer.
* Otherwise, we can add `cnt * m[target / b]` triplets to the answer.

Since we count the the pairs in `B` twice, we need to divide the answer by 2 before returning.

```cpp
// OJ: https://leetcode.com/problems/number-of-ways-where-square-of-number-is-equal-to-product-of-two-numbers/
// Author: github.com/lzl124631x
// Time: O(N^2)
// Space: O(N)
class Solution {
    int count(vector<int> &A, vector<int> &B) {
        int ans = 0;
        unordered_map<int, int> m;
        for (int n : B) m[n]++;
        for (int a : A) {
            long target = (long)a * a;
            for (auto &[b, cnt] : m) {
                if (target % b || m.count(target / b) == 0) continue;
                if (target / b == b) ans += cnt * (cnt - 1);
                else ans += cnt * m[target / b];
            }
        }
        return ans / 2;
    }
public:
    int numTriplets(vector<int>& A, vector<int>& B) {
        return count(A, B) + count(B, A);
    }
};
```
</p>


