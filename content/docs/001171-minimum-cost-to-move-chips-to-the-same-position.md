---
title: "Minimum Cost to Move Chips to The Same Position"
weight: 1171
#id: "minimum-cost-to-move-chips-to-the-same-position"
---
## Description
<div class="description">
<p>We have <code>n</code> chips, where the position of the <code>i<sup>th</sup></code> chip is <code>position[i]</code>.</p>

<p>We need to move all the chips to <strong>the same position</strong>. In one step, we can change the position of the <code>i<sup>th</sup></code> chip from <code>position[i]</code> to:</p>

<ul>
	<li><code>position[i] + 2</code> or <code>position[i] - 2</code> with <code>cost = 0</code>.</li>
	<li><code>position[i] + 1</code> or <code>position[i] - 1</code> with <code>cost = 1</code>.</li>
</ul>

<p>Return <em>the minimum cost</em> needed to move all the chips to the same position.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/15/chips_e1.jpg" style="width: 750px; height: 217px;" />
<pre>
<strong>Input:</strong> position = [1,2,3]
<strong>Output:</strong> 1
<strong>Explanation:</strong> First step: Move the chip at position 3 to position 1 with cost = 0.
Second step: Move the chip at position 2 to position 1 with cost = 1.
Total cost is 1.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/15/chip_e2.jpg" style="width: 750px; height: 306px;" />
<pre>
<strong>Input:</strong> position = [2,2,2,3,3]
<strong>Output:</strong> 2
<strong>Explanation:</strong> We can move the two chips at poistion 3 to position 2. Each move has cost = 1. The total cost = 2.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> position = [1,1000000000]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= position.length &lt;= 100</code></li>
	<li><code>1 &lt;= position[i] &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Array (array)
- Math (math)
- Greedy (greedy)

## Companies
- Morgan Stanley - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

#### Overview

Though marked as "Easy", this problem is a little tricky and requires some observations and insights. It's recommended to try a few examples to find out some regular patterns. 

Below, we will discuss a simple approach to solve this problem.

---

#### Approach 1: Moving Chips Cleverly

**Intuition**

Notice that we have two types of costs:

1. Costs `0` when moving to `position[i] + 2` or `position[i] - 2`.
2. Costs `1` when moving to `position[i] + 1` or `position[i] - 1`.

Since move to `position[i] + 2` or `position[i] - 2` is free, it is natural to think that firstly moving chips as close as possible, with 0 cost.

**In fact, we can move all chips at even positions to position `0`, and move all chips at the odd positions to position `1`.**

Then, we only have many chips at position `0` and other chips at position `1`. Next, we only need to move those two piles together.

Given two piles of chips located at `0` and `1` respectively, intuitively it would be less effort-taking (i.e. less cost) to move the smaller pile to the larger one, which makes the total cost to:

$$
Cost = min(even\_cnt, odd\_cnt)
$$

where $$even\_cnt$$ represents the number of chips at the even positions, and $$odd\_cnt$$ represents the number of chips at the odd positions.

Good, now we have a not bad cost. Can we do better?

Well, now we will prove that this cost is the smallest possible one.

> As for the final position of those chips pile, there are only two possibilities:
> 1. The final position is at the even position, or
> 2. The final position is at the odd position.
> 
> In the first case, we at least need to cost `odd_cnt` to move all the chips at the odd positions to the even positions. 
> Similarly, in the second case, we at least need to cost `even_cnt`.
> 
> Therefore, `min(even_cnt, odd_cnt)` is the smallest possible cost.

In conclusion, the policy we gave above will achieve the smallest possible cost. What we need to return is just `min(even_cnt, odd_cnt)`.

**Algorithm**

We just need to count the number of chips at the even positions and the number of chips at the odd positions and return the smaller one.

<iframe src="https://leetcode.com/playground/hdMAYRvy/shared" frameBorder="0" width="100%" height="293" name="hdMAYRvy"></iframe>

You can also count either the odd or the even numbers, and then deduct that number from the length to obtain the other one.

**Complexity Analysis**

Let $$N$$ be the length of `position`.

- Time Complexity : $$\mathcal{O}(N)$$ since we need to iterate `position` once.

- Space Complexity : $$\mathcal{O}(1)$$ since we only use two ints: `even_cnt` and `odd_cnt`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Not understanding the problem
- Author: jonathanthegoat
- Creation Date: Sun Oct 06 2019 12:22:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 12:22:26 GMT+0800 (Singapore Standard Time)

<p>
I thought the value in the value of the ith chip had nothing to do with the cost. Can anyone one explain? Or is the question unclear? From what I got, the value is calculated base on the moves and not the value of the chips, either odd or even.
</p>


### Python, simple odd/even approach with explanation.
- Author: Hai_dee
- Creation Date: Sun Oct 06 2019 12:28:16 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 07 2019 11:34:12 GMT+0800 (Singapore Standard Time)

<p>
You might initially think we need to try all possibilities, possibly using dynamic programming. But the question is an "easy" for a reason.

The key observation here is that it\'s free to move a chip from an even number to another even number, and it is free to move a chip from an odd number to another odd number. It only costs to move an even to an odd, or an odd to an even. Therefore, we want to minimise such moves.

All chips must be on the same position once we\'re done, which is either even or odd. Therefore, we want to calculate whether it is cheaper to move all the odd chips to even or all the even chips to odd. This will simply come down to how many even chips we start with, and how many odd chips. Each chip that has to be moved will cost exactly 1. 

To determine the cost, we need to count how many are even, and how many are odd, and then take the minimum of these two values.

```python
def minCostToMoveChips(self, chips: List[int]) -> int:
        even_parity = 0
        odd_parity = 0
        for chip in chips:
            if chip % 2 == 0:
                even_parity += 1
            else:
                odd_parity += 1
        return min(even_parity, odd_parity)
```

The cost is ```O(n)``` because we are looking at each chip in isolation, and we are using ```O(1)``` extra space because we only have a few variables to keep the counts in.

For those who like to write the shortest code possible, here you go :D

```python
def minCostToMoveChips(self, chips: List[int]) -> int:
	odds = sum(x % 2 for x in chips)
	return min(odds, len(chips) - odds)
```
</p>


### The most ill-written problem description ever in Leetcode
- Author: JackTsuname
- Creation Date: Mon Oct 07 2019 06:55:15 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 09 2019 09:58:03 GMT+0800 (Singapore Standard Time)

<p>
I stronngly suggest that LeetCode reorganises language of problem description of this problem.
Anyone who\'s in agree with me, please upvote me.
</p>


