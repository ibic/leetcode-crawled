---
title: "Bitwise AND of Numbers Range"
weight: 185
#id: "bitwise-and-of-numbers-range"
---
## Description
<div class="description">
<p>Given a range [m, n] where 0 &lt;= m &lt;= n &lt;= 2147483647, return the bitwise AND of all numbers in this range, inclusive.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> [5,7]
<strong>Output:</strong> 4
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> [0,1]
<strong>Output:</strong> 0</pre>
</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Overview

First of all, one of the most intuitive solutions that one might come up with might be to iterate all the numbers _**one by one**_ in the range and do the the bit AND operation to obtain the result.

This could work for test cases with a small range. Unfortunately it would exceed the time limit set by the online judge for test cases with a relative large range. In this article, we would illustrate some other solutions that do not require the iteration of all numbers.

First of all, let us look into the characteristic of the AND operation.

>For a series of bits, _e.g._ `[1, 1, 0, 1, 1]`, as long as there is one bit of zero value, then the result of AND operation on this series of bits would be zero.

Back to our problem, first we could represent each number in the range in its binary form which we could view as a string of binary numbers (_e.g._ `9 = 00001001`).
We then align the numbers according to the position of binary string.

![pic](../Figures/201/201_prefix.png)

In the above example, one might notice that after the AND operation on all the numbers, the remaining part of bit strings is the _**common prefix**_ of all these bit strings.

The final result asked by the problem consists of this common prefix of bit string as its left part, with the rest of bits as zeros.

More specifically, the _common prefix_ of all these bit string is also the common prefix between the **_starting_** and **_ending_** numbers of the specified range (_i.e._ 9 and 12 respectively in the above example).

>As a result, we then can reformulate the problem as _"given two integer numbers, we are asked to find the _**common prefix**_ of their binary strings."_

<br/>
<br/>

---
#### Approach 1: Bit Shift

**Intuition**

Given the above intuition about the problem, our task is to calculate the _common prefix_ for the bit strings of the two given numbers. One of the solutions would be to resort to the _**bit shift**_ operation.

>The idea is that we shift both numbers to the right, until the numbers become equal, _i.e._ the numbers are reduced into their common prefix. Then we append zeros to the common prefix in order to obtain the desired result, by shifting the common prefix to the left.

![pic](../Figures/201/201_bit_shifting.png)

**Algorithm**

As stated in the intuition section, the algorithm consists of two steps: 

- We reduce both numbers into their common prefix, by doing right shift iteratively. During the iteration, we keep the count on the number of shift operations we perform.
<br/>

- With the common prefix, we restore it to its previous position, by left shifting.

!?!../Documents/201_LIS.json:1000,296!?!

<iframe src="https://leetcode.com/playground/cSDSYtX2/shared" frameBorder="0" width="100%" height="259" name="cSDSYtX2"></iframe>


**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(1)$$. 

    - Although there is a loop in the algorithm, the number of iterations is bounded by the number of bits that an integer has, which is fixed.
    <br/>

    - Therefore, the time complexity of the algorithm is constant. 
    <br/>

- Space Complexity: $$\mathcal{O}(1)$$. The consumption of the memory for our algorithm is constant, regardless the input.
<br/>
<br/>

---
#### Approach 2: Brian Kernighan's Algorithm

**Intuition**

Speaking of bit shifting, there is another related algorithm called [Brian Kernighan's algorithm](http://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetKernighan) which is applied to turn off the rightmost bit of one in a number.

The secret sauce of the _Brian Kernighan's algorithm_ can be summarized as follows:

>When we do AND bit operation between `number` and `number-1`, the rightmost bit of one in the original `number` would be turned off (from one to zero).

![pic](../Figures/201/201_bk_example.png)

Based on the above trick, we could apply it to figure out the common prefix of two bit strings.

>The idea is that for a given range $$[m, n]$$ (_i.e._ $$m < n$$), we could iteratively apply the trick on the number $$n$$ to _turn off_ its rightmost bit of one until it becomes less or equal than the beginning of the range ($$m$$), which we denote as $$n'$$. Finally, we do AND operation between $$n'$$ and $$m$$ to obtain the final result.

By applying the Brian Kernighan's algorithm, we basically turn off the bits that lies on the right side of the _common prefix_, from the ending number $$n$$.
With the rest of bits reset, we then can easily obtain the desired result.

![pic](../Figures/201/201_kernighan.png)

In the example (`m=9, n=12`) shown in the above figure, the common prefix would be `00001`. After applying the Brian Kernighan's algorithm on the number `n`, its trailing 3 bits would all become zeros. Finally, we apply the AND operation between the reduced `n` and the `m` to obtain the common prefix.


**Algorithm**


<iframe src="https://leetcode.com/playground/FqqNbuzV/shared" frameBorder="0" width="100%" height="208" name="FqqNbuzV"></iframe>


By the way, one could refer to the problem called [Hamming distance](https://leetcode.com/articles/hamming-distance/) as another exercise to apply the Brian Kernighan's algorithm.


**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(1)$$.

    - Similar as the bit shift approach, the number of iteration in the algorithm is bounded by the number of bits in an integer number, which is constant.
    <br/>

    - Though having the same asymptotic complexity as the bit shift approach, the Brian Kernighan's algorithm requires less iterations, since it skips all the zero bits in between.
    <br/>

- Space Complexity: $$\mathcal{O}(1)$$, since no additional memory is consumed by the algorithm.
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Bit operation solution(JAVA)
- Author: zwangbo
- Creation Date: Thu Apr 16 2015 21:26:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Apr 18 2020 08:42:05 GMT+0800 (Singapore Standard Time)

<p>
The idea is very simple: 

1. last bit of (odd number & even number) is 0.   
2. when m != n, There is at least an odd number and an even number, so the last bit position result is 0.   
3. Move m and n rigth a position.

Keep doing step 1,2,3 until m equal to n, use a factor to record the iteration time.

    public class Solution {
        public int rangeBitwiseAnd(int m, int n) {
            if(m == 0){
                return 0;
            }
            int moveFactor = 1;
            while(m != n){
                m >>= 1;
                n >>= 1;
                moveFactor <<= 1;
            }
            return m * moveFactor;
        }
    }

</p>


### One line C++ solution
- Author: applewolf
- Creation Date: Fri May 08 2015 17:54:05 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 20:07:53 GMT+0800 (Singapore Standard Time)

<p>
   Consider the bits from low to high. if n > m, the lowest bit will be 0, and then we could transfer the problem to sub-problem:  rangeBitwiseAnd(m>>1, n>>1).  

    int rangeBitwiseAnd(int m, int n) {
        return (n > m) ? (rangeBitwiseAnd(m/2, n/2) << 1) : m;
    }
</p>


### Simple 3 line Java solution faster than 100%
- Author: yash0695
- Creation Date: Thu Apr 23 2020 16:18:51 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 23 2020 17:56:30 GMT+0800 (Singapore Standard Time)

<p>
The trick here is that :
**Bitwise-AND of any two numbers will always produce a number less than or equal to the smaller number.**

Consider the following example:

										12 ---- 1100
										11 ---- 1011
										10 ---- 1010
										9  ---- 1001
										8  ---- 1000
										7  ---- 0111
										6  ---- 0110
										5  ---- 0101
										
Desired Range: [5,12]										

Starting from 12, the loop will first do 
12 & 11 = 8

Next iteration, the loop will do 
8 & 7 = 0

why did we skip anding of 10,9? Because even if we did so, the result would eventually be anded with 8 whose value would be lesser than equal to 8. 

Hence, you start from the range end and keep working your way down the range till you reach the start. 

```
 while(n>m)
           n = n & n-1;
 return m&n;
```
</p>


