---
title: "Knight Dialer"
weight: 885
#id: "knight-dialer"
---
## Description
<div class="description">
<p>The chess knight has a <strong>unique movement</strong>,&nbsp;it may move two squares vertically and one square horizontally, or two squares horizontally and one square vertically (with both forming the shape of an <strong>L</strong>). The possible movements of chess knight are shown in this diagaram:</p>

<p>A chess knight can move as indicated in the chess diagram below:</p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/18/chess.jpg" style="width: 402px; height: 402px;" />
<p>We have a chess knight and a phone pad as shown below, the knight <strong>can only stand on a numeric cell</strong>&nbsp;(i.e. blue cell).</p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/18/phone.jpg" style="width: 242px; height: 322px;" />
<p>Given an integer <code>n</code>, return how many distinct phone numbers of length <code>n</code> we can dial.</p>

<p>You are allowed to place the knight <strong>on any numeric cell</strong> initially and then you should perform <code>n - 1</code> jumps to dial a number of length <code>n</code>. All jumps should be <strong>valid</strong> knight jumps.</p>

<p>As the answer may be very large, <strong>return the answer modulo</strong> <code>10<sup>9</sup> + 7</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> 10
<strong>Explanation:</strong> We need to dial a number of length 1, so placing the knight over any numeric cell of the 10 cells is sufficient.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2
<strong>Output:</strong> 20
<strong>Explanation:</strong> All the valid number we can dial are [04, 06, 16, 18, 27, 29, 34, 38, 40, 43, 49, 60, 61, 67, 72, 76, 81, 83, 92, 94]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 3
<strong>Output:</strong> 46
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 4
<strong>Output:</strong> 104
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 3131
<strong>Output:</strong> 136006598
<strong>Explanation:</strong> Please take care of the mod.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 5000</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Twilio - 5 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Facebook - 3 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Box - 7 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Dynamic Programming

**Intuition**

Let `f(start, n)` be the number of ways to dial an `n` digit number, where the knight starts at square `start`.  We can create a recursion, writing this in terms of `f(x, n-1)`'s.

**Algorithm**

By hand or otherwise, have a way to query what moves are available at each square.  This implies the exact recursion for `f`.  For example, from `1` we can move to `6, 8`, so `f(1, n) = f(6, n-1) + f(8, n-1)`.

After, let's keep track of `dp[start] = f(start, n)`, and update it for each n from `1, 2, ..., N`.

At the end, the answer is `f(0, N) + f(1, N) + ... + f(9, N) = sum(dp)`.

<iframe src="https://leetcode.com/playground/Xg9iwg4D/shared" frameBorder="0" width="100%" height="463" name="Xg9iwg4D"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### How to solve this problem explained for noobs!!!
- Author: sriramgopal03
- Creation Date: Thu Nov 08 2018 06:49:06 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Nov 08 2018 06:49:06 GMT+0800 (Singapore Standard Time)

<p>
We can think of this problem as the total number of unique paths the knight can travel making n hops because to dial distinct numbers, the path taken by the knight must be unique.

In this post I want to explain how I came up with a solution to this problem. This approach can be used to solve other similar problems such as Unique Paths, Minimum Path Sum etc.

Imagine an 8 x 8 chess board with Knight (k) sitting at some index `(i, j)`. The board would look as follows,

```
*   *   *   *   *   *   *   *
*   *   *   *   *   *   *   *
*   *   b   *   c   *   *   *
*   a   *   *   *   d   *   *
*   *   *   k   *   *   *   *
*   h   *   *   *   e   *   *
*   *   g   *   f   *   *   *
*   *   *   *   *   *   *   *
```


**[0]** If k is at index `(i, j)`, then in a single hop, k can move to 8 possible positions which are below. 


```
a (i - 1, j - 2)
b (i - 2, j - 1)
c (i - 2, j + 1)
d (i - 1, j + 2)
e (i + 1, j + 2)
f  (i + 2, j + 1)
g (i + 2, j - 1)
h (i + 1, j - 2)
```

**[1]** Conversely, you can also say that in a single hop, there are 8 possible places (a,b,c,d,e,f,g,h) from which you can move to k.

**Math behind the solution:**

Consider a function paths(i, j, n) which calculates the total number of unique paths to reach index (i, j) for a given n, where n is the number of hops. From [0] or [1], we can recusively define paths(i, j, n) for all non-trivial (n > 1, that is, more than one hop) cases as follows,

```
paths(i, j, n) = paths(i - 1, j - 2, n - 1) + 
                 paths (i - 2, j - 1, n - 1) +
                 paths (i - 2, j + 1, n - 1) +
                 paths (i - 1, j + 2, n - 1) +
                 paths (i + 1, j + 2, n - 1) +
                 paths  (i + 2, j + 1, n - 1) +
                 paths (i + 2, j - 1, n - 1) +
                 paths (i + 1, j - 2, n - 1)
```
											
If we translate this to plain english, all we are saying is "the total number of unique paths to (i, j) for certain hops n is equal to the sum of total number of unique paths to each valid position from which (i, j) can be reached using n - 1 hops".

If you are confused why it is n - 1 hops, note that when we are at (i, j), we already made one hop and we have n - 1 hops more to take.

For the trivial case (n = 1, that is no hops), the problem states that this must be considered as one path. Therefore, paths(i, j, n) = 1, for n = 1.

**A Sample Trace**

If the above recursive equation or it\'s translation is not very enlightening, you can follow this sample trace to get a better understanding of the logic.

Our keypad is a 4 x 3 matrix which looks like below.

```
1   2   3
4   5   6
7   8   9
*   0   *
```

Note that in the code there is no need to use a matrix like this. This is just for explanation purpose.

We shall trace the recursion tree of paths(0, 0, 3) in this section, that is, all the possible unique paths from 1 (0, 0) in 3 hops. From 1 (0, 0), in a single hop, we have two possible places to jump to - 6 and 8.

Wait! Didn\'t I say that a knight can jump to 8 possible places in a single hop some where in this post?

*..."in a single hop, k can move to 8 possible positions"...*

Yes, a knight can jump to 8 possible places in a single hop and this is still true. However, 6 of the other hops will take you outside of the matrix. You will see later in the code how this is being handled as a part of base case.

*A brief on notation*: 

I am representing each node in the recursion tree something like X (i, j, n). This means that we are at the call paths(0, 0, 3) and the knight is currently sitting on the number X with n - 1 hops remaining.

Below is the recursion tree for paths(0, 0, 3).

Note: The following tree diagram is not visually appealing with the old UI. I recommend to view this in the new UI for a proper visual representation.

                                  1 (0,0,3)
				               /		     \
					          /               \
				  	  6 (1,2,2)             8 (2,1,2)	
				   	/   |    \                |   	\
				   /	|	  \		          |  	 \
		1 (0,0,1)  0 (3,1,1)  7 (2,0,1)   1 (0,0,1)  3 (0,2,1)


Since, 6 jumps are invalid, that leaves us with only two valid jumps `e (i + 1, j + 2)` and  `f  (i + 2, j + 1)` from 1 (0, 0). In other words, from 1 the knight can jump only to 8 and 6. 

In fact, in this matrix, the maximum number of valid jumps you can make is 3 which is from 4, 6.

Explanation of this recursion tree is below,

From 1 (0, 0, 3), the knight can go to 6 (1, 2, 2) and 8 (2, 1, 2) in a single hop. As we go down the recursion tree, we pass the number of hops to make as 1 less than the current.

From 6 (1, 2, 2), it can go to 1 (0, 0, 1) , 0 (3, 1, 1) and 7 (2, 0, 1). We pass n as 1 as we are going down in the recursion. At this point, each of these calls return 1 (since n = 1 which is the trivial case). 

Therefore, **[2]** 6 (1, 2, 2) = 1 + 1 + 1 = 3, which means that there are 3 unique paths from 6 when n = 2 and they are 61, 60, 67.

From 8 (2,1,2), it can go to 1 (0, 0, 1) and 3 (0, 2, 1). At this point, each of these calls return 1 (since n = 1).

Therefore, **[3]** 8 (2, 1, 2) = 1 + 1 = 2, which means that there are 2 unique paths from 8 when n = 2 and they are 81, 83.

Finally, 1 (0, 0, 3) = 6 (1, 2, 2) + 8 (2, 1, 2)

From [2], [3], we can write this as,

1 (0, 0, 3) = 3 + 2 = 5, which means that there are 5 unique paths from 1 when n = 3 and they are 161, 160, 167, 181, 183.

**Naive Recursive Code**
```

public static final int max = (int) Math.pow(10, 9) + 7;
	
public int knightDialer(int n) {
   long s = 0;
   //do n hops from every i, j index (the very requirement of the problem)
   for(int i = 0; i < 4; i++) {
      for(int j = 0; j < 3; j++) {
         s = (s + paths(i, j, n)) % max;
      }
   }
   return (int) s;
}

private long paths(int i, int j, int n) {
   // if the knight hops outside of the matrix or to * return 0 
   //as there are no unique paths from here
   if(i < 0 || j < 0 || i >= 4 || j >= 3 || (i == 3 && j != 1)) return 0;
   //trivial case
   if(n == 1) return 1;
   //non trivial case
   long s = paths(i - 1, j - 2, n - 1) % max + // jump to a
            paths(i - 2, j - 1, n - 1) % max + // jump to b
            paths(i - 2, j + 1, n - 1) % max + // jump to c
            paths(i - 1, j + 2, n - 1) % max + // jump to d
            paths(i + 1, j + 2, n - 1) % max + // jump to e
            paths(i + 2, j + 1, n - 1) % max + // jump to f
            paths(i + 2, j - 1, n - 1) % max + // jump to g
            paths(i + 1, j - 2, n - 1) % max; // jump to h
   return s;
}
```

If you run this code for n = 50 in your favorite programming language, you will realize that it takes at least an hour to get the answer. 

This is because this problem not only has similar subproblems but each of those similar subproblems have overlapping subproblems. What does this mean? Let me explain with an example.

As seen in the above trace, a subproblem of 1 (0, 0, 3) is 8 (2, 1, 2). 

A subproblem of 3 (0, 2, 3) is also 8 (2, 1, 2) because you can get from 3 to 8 in a single hop.

We have already computed the solution to 8 (2, 1, 2) while computing the solution to 1 (0, 0, 3) and there is no need to re-compute this solution if were to store the solution somewhere in memory. The above recursive solution re-computes the solutions to overlapping subproblems and therefore is highly inefficient (runs in the order of O(3 ^ n) I believe).

**Top down Dynamic programming solution**

We use dynamic programming and store the solution of each subproblem in M. M is a 3D array and each index of M corresponds to a solution of n. Each n is again stored as a 2D array for (i, j) values.

All this combined, M will store the solution of each paths(i, j, n) call.

Below is the code.

```
public static final int max = (int) Math.pow(10, 9) + 7;
	
public int knightDialer(int n) {
   // A 3D array to store the solutions to the subproblems
   long M[][][] = new long[n + 1][4][3];
   long s = 0;
   //do n hops from every i, j index (the very requirement of the problem)
   for(int i = 0; i < 4; i++) {
      for(int j = 0; j < 3; j++) {
         s = (s + paths(M, i, j, n)) % max;
      }
   }
   return (int) s;
}

private long paths(long[][][] M, int i, int j, int n) {
   // if the knight hops outside of the matrix or to * return 0 
   //as there are no unique paths from here
   if(i < 0 || j < 0 || i >= 4 || j >= 3 || (i == 3 && j != 1)) return 0;
   if(n == 1) return 1;
   //if the subproblem\'s solution is already computed, then return it
   if(M[n][i][j] > 0) return M[n][i][j];
   //else compute the subproblem\'s solution and save it in memory
   M[n][i][j] = paths(M, i - 1, j - 2, n - 1) % max + // jump to a
                paths(M, i - 2, j - 1, n - 1) % max + // jump to b
                paths(M, i - 2, j + 1, n - 1) % max + // jump to c
                paths(M, i - 1, j + 2, n - 1) % max + // jump to d
                paths(M, i + 1, j + 2, n - 1) % max + // jump to e
                paths(M, i + 2, j + 1, n - 1) % max + // jump to f
                paths(M, i + 2, j - 1, n - 1) % max + // jump to g
                paths(M, i + 1, j - 2, n - 1) % max; // jump to h
   return M[n][i][j];
}
```

Any questions or feedback is welcome!

**P.S: The wysiwyg editor of leetcode sucks! :P**

</p>


### O(logN)
- Author: lee215
- Creation Date: Sun Nov 04 2018 11:03:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 04 2018 11:03:57 GMT+0800 (Singapore Standard Time)

<p>
<img src="https://assets.leetcode.com/users/lee215/image_1541301837.png" style="width:200px;"/>

**Naive recursion:**
`O(N)` time and `O(1)` space, good enough.

```
    def knightDialer(self, N):
        x1 = x2 = x3 = x4 = x5 = x6 = x7 = x8 = x9 = x0 = 1
        for i in range(N - 1):
            x1, x2, x3, x4, x5, x6, x7, x8, x9, x0 = \
                x6 + x8, x7 + x9, x4 + x8, \
                x3 + x9 + x0, 0, x1 + x7 + x0, \
                x2 + x6, x1 + x3, x2 + x4, \
                x4 + x6
        return (x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x0) % (10**9 + 7)
```


In fact, we recursively did pow operation.
This can be optimised to `O(log)` time.

Construct a 10 * 10 transformation matrix M.
`M[i][j] = 1` if i and j is connnected.

if N = 1, return 10.
if N > 1, return sum of `[1,1,1,1,1,1,1,1,1,1] * M ^ (N - 1)`

The power of matrix reveals the number of walks in an undirected graph.
Find more details on this link provide by @shankark:
https://math.stackexchange.com/questions/1890620/finding-path-lengths-by-the-power-of-adjacency-matrix-of-an-undirected-graph


```
    def knightDialer(self, N):
        mod = 10**9 + 7
        if N == 1: return 10
        M = np.matrix([[0, 0, 0, 0, 1, 0, 1, 0, 0, 0],
                       [0, 0, 0, 0, 0, 0, 1, 0, 1, 0],
                       [0, 0, 0, 0, 0, 0, 0, 1, 0, 1],
                       [0, 0, 0, 0, 1, 0, 0, 0, 1, 0],
                       [1, 0, 0, 1, 0, 0, 0, 0, 0, 1],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [1, 1, 0, 0, 0, 0, 0, 1, 0, 0],
                       [0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
                       [0, 1, 0, 1, 0, 0, 0, 0, 0, 0],
                       [0, 0, 1, 0, 1, 0, 0, 0, 0, 0]])
        res, N = 1, N - 1
        while N:
            if N % 2: res = res * M % mod
            M = M * M % mod
            N /= 2
        return int(np.sum(res)) % mod
</p>


### [Java] Top Down Memo DP O(N)
- Author: peritan
- Creation Date: Sun Nov 04 2018 11:14:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 04 2018 11:14:26 GMT+0800 (Singapore Standard Time)

<p>
The problem can be transformed into:
Traverse a directed graph (each node with a number as label and edges are defined by Knight\'s moving rule)
Start from `0 to 9`
Move `N - 1` step
Return `how many ways to reach the end`

Easy to come up with a DFS solution to start traversal from 0 to 9
In each recursion, move to one of the current node\'s neighbors and the remain step becomes `N-1`
Stop recursion when `N == 0`

**Optimization:**
Observe the recursive problem. The variances are:
1. Current Node
2. Remain Steps

Therefore, we can store these two variables as the memo to speed up DFS (then it\'s a Top Down DP)
```
    public static final int MOD = 1000000007;
    public int knightDialer(int N) {
        int[][] graph = new int[][]{{4,6},{6,8},{7,9},{4,8},{3,9,0},{},{1,7,0},{2,6},{1,3},{2,4}};
        int cnt = 0;
        Integer[][] memo = new Integer[N+1][10];
        for (int i = 0; i <= 9; i++)
            cnt = (cnt + helper(N-1, i, graph, memo)) % MOD;
        return cnt;
    }
    private int helper(int N, int cur, int[][] graph, Integer[][] memo) {
        if (N == 0)
            return 1;
        if (memo[N][cur] != null)
            return memo[N][cur];
        int cnt = 0;
        for (int nei : graph[cur])
            cnt = (cnt + helper(N-1, nei, graph, memo)) % MOD;
        memo[N][cur] = cnt;
        return cnt;
    }
```
**Time Complexity:** `O(10*N) = O(N)` fill the memo
**Space Complexity:** `O(N)` depth of the recursion 
</p>


