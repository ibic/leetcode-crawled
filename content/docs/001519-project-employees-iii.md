---
title: "Project Employees III"
weight: 1519
#id: "project-employees-iii"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Project</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| project_id  | int     |
| employee_id | int     |
+-------------+---------+
(project_id, employee_id) is the primary key of this table.
employee_id is a foreign key to <code>Employee</code> table.
</pre>

<p>Table:&nbsp;<code>Employee</code></p>

<pre>
+------------------+---------+
| Column Name      | Type    |
+------------------+---------+
| employee_id      | int     |
| name             | varchar |
| experience_years | int     |
+------------------+---------+
employee_id is the primary key of this table.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query that reports the <strong>most experienced</strong> employees in each project. In case of a tie, report all employees with the maximum number of experience years.</p>

<p>The query result format is in the following example:</p>

<pre>
Project table:
+-------------+-------------+
| project_id  | employee_id |
+-------------+-------------+
| 1           | 1           |
| 1           | 2           |
| 1           | 3           |
| 2           | 1           |
| 2           | 4           |
+-------------+-------------+

Employee table:
+-------------+--------+------------------+
| employee_id | name   | experience_years |
+-------------+--------+------------------+
| 1           | Khaled | 3                |
| 2           | Ali    | 2                |
| 3           | John   | 3                |
| 4           | Doe    | 2                |
+-------------+--------+------------------+

Result table:
+-------------+---------------+
| project_id  | employee_id   |
+-------------+---------------+
| 1           | 1             |
| 1           | 3             |
| 2           | 1             |
+-------------+---------------+
Both employees with id 1 and 3 have the most experience among the employees of the first project. For the second project, the employee with id 1 has the most experience.</pre>

</div>

## Tags


## Companies
- Facebook - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Three straightforward solutions (window function & subquery)
- Author: olivia612
- Creation Date: Thu Aug 29 2019 05:56:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 05:50:11 GMT+0800 (Singapore Standard Time)

<p>
MSSQL solution using Window Function. Runtime: 1098 ms

```
select t.project_id, t.employee_id
from
    (select project_id,
    p.employee_id,
    rank() over(partition by project_id order by experience_years desc) as rank
    from Project p join Employee e
    on p.employee_id = e.employee_id) t
where t.rank = 1;
```

MSSQL solution using With function instead of sub-query.
```
with employee_experience as (
    select p.project_id, p.employee_id,
    rank() over(partition by p.project_id order by experience_years desc) as rank
    from Project p join Employee e
    on p.employee_id = e.employee_id)

select project_id, employee_id
from employee_experience
where rank = 1;
```

MySQL solution using subquery. Runtime: 278 ms
```
select p.project_id, p.employee_id
from Project p join Employee e
on p.employee_id = e.employee_id
where (p.project_id, e.experience_years) in (
    select a.project_id, max(b.experience_years)
    from Project a join Employee b
    on a.employee_id = b.employee_id
    group by a.project_id);
```
</p>


### 97% MySQL Solution
- Author: ganyue246
- Creation Date: Wed Jul 24 2019 13:27:16 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 24 2019 13:27:16 GMT+0800 (Singapore Standard Time)

<p>
SELECT p.project_id, e.employee_id
from project as p
inner join employee as e on e.employee_id = p.employee_id
where (p.project_id, e.experience_years) in
(SELECT p.project_id, max(e.experience_years)
from project as p
inner join employee as e on e.employee_id = p.employee_id
group by project_id)

</p>


### Simple SQL
- Author: Merciless
- Creation Date: Sun Jun 09 2019 08:03:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 09 2019 08:03:19 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT project_id, employee_id
FROM project
JOIN employee
USING(employee_id)
WHERE (project_id, experience_years) in
(
    SELECT project_id, max(experience_years)
    FROM project
    JOIN employee
    USING(employee_id)
    group by project_id
)
</p>


