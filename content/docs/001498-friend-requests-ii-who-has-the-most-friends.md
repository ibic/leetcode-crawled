---
title: "Friend Requests II: Who Has the Most Friends"
weight: 1498
#id: "friend-requests-ii-who-has-the-most-friends"
---
## Description
<div class="description">
<p>In social network like Facebook or Twitter, people send friend requests and accept others&#39; requests as well.</p>

<p>&nbsp;</p>

<p>Table <code>request_accepted</code></p>

<pre>
+--------------+-------------+------------+
| requester_id | accepter_id | accept_date|
|--------------|-------------|------------|
| 1            | 2           | 2016_06-03 |
| 1            | 3           | 2016-06-08 |
| 2            | 3           | 2016-06-08 |
| 3            | 4           | 2016-06-09 |
+--------------+-------------+------------+
This table holds the data of friend acceptance, while <b>requester_id</b> and <b>accepter_id</b> both are the id of a person.
</pre>

<p>&nbsp;</p>

<p>Write a query to find the the people who has most friends and the most friends number under the following rules:</p>

<ul>
	<li>It is guaranteed there is only 1 people having the most friends.</li>
	<li>The friend request could only been accepted once, which mean there is no multiple records with the same <b>requester_id</b> and <b>accepter_id</b> value.</li>
</ul>

<p>For the sample data above, the result is:</p>

<pre>
Result table:
+------+------+
| id   | num  |
|------|------|
| 3    | 3    |
+------+------+
The person with id &#39;3&#39; is a friend of people &#39;1&#39;, &#39;2&#39; and &#39;4&#39;, so he has 3 friends in total, which is the most number than any others.
</pre>

<p><b>Follow-up:</b><br />
In the real world, multiple people could have the same most number of friends, can you find all these people in this case?</p>

</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach: Union *requester_id* and *accepter_id* [Accepted]

**Algorithm**

Being friends is bidirectional, so if one person accepts a request from another person, both of them will have one more friend.

Thus, we can union column *requester_id* and *accepter_id*, and then count the number of the occurrence of each person.

```mysql
select requester_id as ids from request_accepted
union all
select accepter_id from request_accepted;
```
>Note: Here we should use `union all` instead of `union` because `union all` will keep all the records even the 'duplicated' one.

Taking the sample as an example, the output is:

| ids |
|-----|
| 1   |
| 1   |
| 2   |
| 3   |
| 2   |
| 3   |
| 3   |
| 4   |

Then it will be fairly easy to get the 'ids' with most occurrence using the same technique as mentioned in problem [580. Customer Placing the Largest Number of Orders](https://leetcode.com/problems/count-student-number-in-departments/#/description).

**MySQL**

```mysql
select ids as id, cnt as num
from
(
select ids, count(*) as cnt
   from
   (
        select requester_id as ids from request_accepted
        union all
        select accepter_id from request_accepted
    ) as tbl1
   group by ids
   ) as tbl2
order by cnt desc
limit 1
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Shouldn't we use Union instead of Union all?
- Author: fangyuan0123
- Creation Date: Mon Jul 10 2017 06:35:49 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 13:41:06 GMT+0800 (Singapore Standard Time)

<p>
I think the answer missed the case when A send B a friend request, and B send A a friend request, and both requests got approved. In this case, A or B really just gained one friend. But the answer seems to count this case twice.
Isn't union (remove duplicates) should be used instead of union all?
```
select id1 as id, count(id2) as num
from
(select requester_id as id1, accepter_id as id2 
from request_accepted
union
select accepter_id as id1, requester_id as id2 
from request_accepted) tmp1
group by id1 
order by num desc limit 1
```
</p>


### Share My Accepted SQL Query using "union all", the first Accepted answer of all
- Author: richarddia
- Creation Date: Mon May 29 2017 12:16:11 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 29 2017 12:16:11 GMT+0800 (Singapore Standard Time)

<p>
```
select id, count(*) num from 
(
      (select requester_id id from request_accepted) 
      union all 
      (select accepter_id id from request_accepted)
) tb 
group by id order by num desc limit 1
```
</p>


### The test case is not correct?
- Author: rookieChao
- Creation Date: Wed Nov 01 2017 03:40:26 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 01 2017 03:40:26 GMT+0800 (Singapore Standard Time)

<p>
[[14,3,"2016/11/27"],
[16,2,"2016/11/22"],
[11,3,"2017/01/02"],
[12,17,"2017/01/19"],
[17,1,"2016/12/12"],[16,4,"2016/11/01"],[12,14,"2017/02/22"],[19,3,"2016/10/28"],[4,1,"2017/01/13"],[9,19,"2016/11/13"],[3,12,"2017/01/11"],[14,1,"2017/04/05"],[12,5,"2016/10/25"],[2,1,"2017/03/10"],[14,5,"2017/01/14"],[9,3,"2016/11/15"],[19,17,"2017/01/20"],[2,18,"2016/10/23"],[19,6,"2017/03/28"],[16,13,"2016/10/28"],[15,19,"2016/10/03"],[11,16,"2017/02/10"],[12,13,"2016/11/28"],[10,20,"2017/01/02"],[9,15,"2017/01/01"],[20,4,"2017/04/05"],[16,5,"2016/11/23"],[9,13,"2017/01/21"],[17,7,"2016/10/01"],[4,17,"2016/12/07"],[19,11,"2017/02/14"],[12,7,"2016/12/21"],[3,16,"2016/09/29"],[8,7,"2017/03/01"],[4,13,"2017/03/14"],[7,14,"2017/03/16"],[12,18,"2016/10/09"],[7,20,"2016/10/20"],[17,18,"2016/12/14"],[14,17,"2017/01/23"],[3,7,"2017/03/02"],[6,13,"2016/10/21"],[19,9,"2016/11/16"],[4,8,"2017/03/22"],[12,16,"2017/02/01"],[7,11,"2017/01/09"],[19,10,"2017/02/11"],[18,8,"2017/03/18"],[19,20,"2016/11/11"],[19,4,"2017/01/13"],[2,20,"2016/10/02"],[7,15,"2017/01/03"],[18,12,"2016/12/08"],[10,5,"2016/11/29"],[12,4,"2017/01/04"],[15,4,"2017/01/08"],[8,11,"2016/10/15"],[16,10,"2017/03/25"],[10,6,"2016/11/03"],[16,19,"2016/11/15"],[2,3,"2017/02/03"],[7,12,"2017/01/01"],[16,18,"2016/12/30"],[12,17,"2017/03/23"],[8,15,"2016/09/29"],[20,13,"2016/12/15"],[11,1,"2017/03/31"],[12,4,"2016/12/24"],[12,11,"2016/12/16"],[10,18,"2017/02/11"],[17,16,"2016/11/07"],[20,10,"2017/04/09"],[18,20,"2016/11/08"],[11,14,"2016/10/25"],[5,10,"2016/10/11"],[15,5,"2017/03/26"]]}}


(12,17) pair counts twice while the problem claims
"The friend request could only been accepted once, which mean there is no multiple records with the same requester_id and accepter_id value."
The UNION ALL solution can't detect such senarios.
</p>


