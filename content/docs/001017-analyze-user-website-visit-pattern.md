---
title: "Analyze User Website Visit Pattern"
weight: 1017
#id: "analyze-user-website-visit-pattern"
---
## Description
<div class="description">
<p>We are given some website visits: the user with name&nbsp;<code>username[i]</code> visited the website&nbsp;<code>website[i]</code> at time <code>timestamp[i]</code>.</p>

<p>A <em>3-sequence</em>&nbsp;is a list of&nbsp;websites of length 3 sorted in ascending order&nbsp;by the time of their visits.&nbsp; (The websites in a 3-sequence are not necessarily distinct.)</p>

<p>Find the 3-sequence visited&nbsp;by the largest number of users. If there is more than one solution, return the lexicographically smallest such 3-sequence.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>username = <span>[&quot;joe&quot;,&quot;joe&quot;,&quot;joe&quot;,&quot;james&quot;,&quot;james&quot;,&quot;james&quot;,&quot;james&quot;,&quot;mary&quot;,&quot;mary&quot;,&quot;mary&quot;]</span>, timestamp = <span id="example-input-1-2">[1,2,3,4,5,6,7,8,9,10]</span>, website = <span id="example-input-1-3">[&quot;home&quot;,&quot;about&quot;,&quot;career&quot;,&quot;home&quot;,&quot;cart&quot;,&quot;maps&quot;,&quot;home&quot;,&quot;home&quot;,&quot;about&quot;,&quot;career&quot;]</span>
<strong>Output: </strong><span id="example-output-1">[&quot;home&quot;,&quot;about&quot;,&quot;career&quot;]</span>
<strong>Explanation: </strong>
The tuples in this example are:
[&quot;joe&quot;, 1, &quot;home&quot;]
[&quot;joe&quot;, 2, &quot;about&quot;]
[&quot;joe&quot;, 3, &quot;career&quot;]
[&quot;james&quot;, 4, &quot;home&quot;]
[&quot;james&quot;, 5, &quot;cart&quot;]
[&quot;james&quot;, 6, &quot;maps&quot;]
[&quot;james&quot;, 7, &quot;home&quot;]
[&quot;mary&quot;, 8, &quot;home&quot;]
[&quot;mary&quot;, 9, &quot;about&quot;]
[&quot;mary&quot;, 10, &quot;career&quot;]
The 3-sequence (&quot;home&quot;, &quot;about&quot;, &quot;career&quot;) was visited at least once by <strong>2</strong> users.
The 3-sequence (&quot;home&quot;, &quot;cart&quot;, &quot;maps&quot;) was visited at least once by 1 user.
The 3-sequence (&quot;home&quot;, &quot;cart&quot;, &quot;home&quot;) was visited at least once by 1 user.
The 3-sequence (&quot;home&quot;, &quot;maps&quot;, &quot;home&quot;) was visited at least once by 1 user.
The 3-sequence (&quot;cart&quot;, &quot;maps&quot;, &quot;home&quot;) was visited at least once by 1 user.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>3 &lt;= N = username.length = timestamp.length = website.length &lt;= 50</code></li>
	<li><code>1 &lt;= username[i].length &lt;= 10</code></li>
	<li><code>0 &lt;= timestamp[i] &lt;= 10^9</code></li>
	<li><code>1 &lt;= website[i].length &lt;= 10</code></li>
	<li>Both <code>username[i]</code> and <code>website[i]</code> contain only lowercase characters.</li>
	<li>It is guaranteed that there is at least one user who visited at least 3 websites.</li>
	<li>No user visits two websites at the same time.</li>
</ol>

</div>

## Tags
- Array (array)
- Hash Table (hash-table)
- Sort (sort)

## Companies
- Amazon - 24 (taggedByAdmin: true)
- Wish - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Clarification required.
- Author: lakhansoren
- Creation Date: Sun Aug 11 2019 00:32:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 11 2019 00:32:57 GMT+0800 (Singapore Standard Time)

<p>
What does this statement mean : "A 3-sequence is a list of not necessarily different websites of length 3 " ?
For the example that has been provided can\'t the answer be ["home" , "home" , "home"] as "home" has been visited three different users ? 
I have not understood the criteria for forming a 3-sequence. Can someone explain it to me ?
</p>


### Java Very Easy Understand With Explanation
- Author: wushangzhen
- Creation Date: Sun Aug 11 2019 05:06:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 11 2019 10:11:44 GMT+0800 (Singapore Standard Time)

<p>
To be honest, this question\'s explanation is very confusing and I didn\'t understand it in contest. Let me explain it here:
Every user visits websites and the websites for each user visits can form 3 - sequences. Our goal is to find the most common 3-seq in those users. 
1. we collect the time and website info according to every user
2. n^3 traverse all 3-seq in every user and use count map to record the 3-seq ocurring times(remember to sort by time and avoid same sequence in the same user)
3. get the result
```
class Pair {
    int time;
    String web;
    public Pair(int time, String web) {
        this.time = time;
        this.web = web;
    }
}
class Solution {
    public List<String> mostVisitedPattern(String[] username, int[] timestamp, String[] website) {
        Map<String, List<Pair>> map = new HashMap<>();
        int n = username.length;
        // collect the website info for every user, key: username, value: (timestamp, website)
        for (int i = 0; i < n; i++) {
            map.putIfAbsent(username[i], new ArrayList<>());
            map.get(username[i]).add(new Pair(timestamp[i], website[i]));
        }
        // count map to record every 3 combination occuring time for the different user.
        Map<String, Integer> count = new HashMap<>();
        String res = "";
        for (String key : map.keySet()) {
            Set<String> set = new HashSet<>();
            // this set is to avoid visit the same 3-seq in one user
            List<Pair> list = map.get(key);
            Collections.sort(list, (a, b)->(a.time - b.time)); // sort by time
            // brutal force O(N ^ 3)
            for (int i = 0; i < list.size(); i++) {
                for (int j = i + 1; j < list.size(); j++) {
                    for (int k = j + 1; k < list.size(); k++) {
                        String str = list.get(i).web + " " + list.get(j).web + " " + list.get(k).web;
                        if (!set.contains(str)) {
                            count.put(str, count.getOrDefault(str, 0) + 1);
                            set.add(str);
                        }
                        if (res.equals("") || count.get(res) < count.get(str) || (count.get(res) == count.get(str) && res.compareTo(str) > 0)) {
                            // make sure the right lexi order
                            res = str;
                        }
                    }
                }
            }
        }
        // grab the right answer
        String[] r = res.split(" ");
        List<String> result = new ArrayList<>();
        for (String str : r) {
            result.add(str);
        }
        return result;
    }
}
```

</p>


### Python Solution
- Author: lee215
- Creation Date: Sun Aug 11 2019 00:04:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 11 2019 01:41:00 GMT+0800 (Singapore Standard Time)

<p>
**Python:**
```python
    def mostVisitedPattern(self, username, timestamp, website):
        dp = collections.defaultdict(list)
        for t, u, w in sorted(zip(timestamp, username, website)):
            dp[u].append(w)
        count = sum([collections.Counter(set(itertools.combinations(dp[u], 3))) for u in dp], collections.Counter())
        return list(min(count, key=lambda k: (-count[k], k)))
```

</p>


