---
title: "Two Sum II - Input array is sorted"
weight: 167
#id: "two-sum-ii-input-array-is-sorted"
---
## Description
<div class="description">
<p>Given an array of integers that is already <strong><em>sorted in ascending order</em></strong>, find two numbers such that they add up to a specific target number.</p>

<p>The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2.</p>

<p><strong>Note:</strong></p>

<ul>
	<li>Your returned answers (both index1 and index2) are not zero-based.</li>
	<li>You may assume that each input would have <em>exactly</em> one solution and you may not use the <em>same</em> element twice.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> numbers = [2,7,11,15], target = 9
<strong>Output:</strong> [1,2]
<strong>Explanation:</strong> The sum of 2 and 7 is 9. Therefore index1 = 1, index2 = 2.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> numbers = [2,3,4], target = 6
<strong>Output:</strong> [1,3]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> numbers = [-1,0], target = -1
<strong>Output:</strong> [1,2]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= nums.length &lt;= 3 * 10<sup>4</sup></code></li>
	<li><code>-1000 &lt;= nums[i] &lt;= 1000</code></li>
	<li><code>nums</code>&nbsp;is sorted in <strong>increasing order</strong>.</li>
	<li><code>-1000 &lt;= target &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)
- Binary Search (binary-search)

## Companies
- Amazon - 3 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Goldman Sachs - 4 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Pure Storage - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Two Pointers

**Algorithm**

We can apply [Two Sum's solutions](https://leetcode.com/articles/two-sum/) directly to get $$O(n^2)$$ time, $$O(1)$$ space using brute force and $$O(n)$$ time, $$O(n)$$ space using hash table. However, both existing solutions do not make use of the property where the input array is sorted. We can do better.

We use two indexes, initially pointing to the first and last element respectively. Compare the sum of these two elements with target. If the sum is equal to target, we found the exactly only solution. If it is less than target, we increase the smaller index by one. If it is greater than target, we decrease the larger index by one. Move the indexes and repeat the comparison until the solution is found.

Let $$[... , a, b, c, ... , d, e, f, ...]$$ be the input array that is sorted in ascending order and the element $$b$$, $$e$$ be the exactly only solution. Because we are moving the smaller index from left to right, and the larger index from right to left, at some point one of the indexes must reach either one of $$b$$ or $$e$$. Without loss of generality, suppose the smaller index reaches $$b$$ first. At this time, the sum of these two elements must be greater than target. Based on our algorithm, we will keep moving the larger index to its left until we reach the solution.

<iframe src="https://leetcode.com/playground/jg27qtb6/shared" frameBorder="0" width="100%" height="327" name="jg27qtb6"></iframe>

Do we need to consider if $$numbers[low] + numbers[high]$$ overflows? The answer is no. Even if adding two elements in the array may overflow, because there is exactly one solution, we will reach the solution first.

**Complexity analysis**

* Time complexity : $$O(n)$$.
Each of the $$n$$ elements is visited at most once, thus the time complexity is $$O(n)$$.

* Space complexity : $$O(1)$$.
We only use two indexes, the space complexity is $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python different solutions (two-pointer, dictionary, binary search).
- Author: OldCodingFarmer
- Creation Date: Wed Aug 19 2015 06:02:33 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 22:06:21 GMT+0800 (Singapore Standard Time)

<p>
        
    # two-pointer
    def twoSum1(self, numbers, target):
        l, r = 0, len(numbers)-1
        while l < r:
            s = numbers[l] + numbers[r]
            if s == target:
                return [l+1, r+1]
            elif s < target:
                l += 1
            else:
                r -= 1
     
    # dictionary           
    def twoSum2(self, numbers, target):
        dic = {}
        for i, num in enumerate(numbers):
            if target-num in dic:
                return [dic[target-num]+1, i+1]
            dic[num] = i
     
    # binary search        
    def twoSum(self, numbers, target):
        for i in xrange(len(numbers)):
            l, r = i+1, len(numbers)-1
            tmp = target - numbers[i]
            while l <= r:
                mid = l + (r-l)//2
                if numbers[mid] == tmp:
                    return [i+1, mid+1]
                elif numbers[mid] < tmp:
                    l = mid+1
                else:
                    r = mid-1
</p>


### Share my java AC solution.
- Author: titanduan3
- Creation Date: Sun Dec 21 2014 11:48:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 20:19:57 GMT+0800 (Singapore Standard Time)

<p>
Without HashMap, just have two pointers, A points to index 0, B points to index len - 1, shrink the scope based on the value and target comparison.

    public int[] twoSum(int[] num, int target) {
        int[] indice = new int[2];
        if (num == null || num.length < 2) return indice;
        int left = 0, right = num.length - 1;
        while (left < right) {
            int v = num[left] + num[right];
            if (v == target) {
                indice[0] = left + 1;
                indice[1] = right + 1;
                break;
            } else if (v > target) {
                right --;
            } else {
                left ++;
            }
        }
        return indice;
    }
</p>


### A simple O(n) solution
- Author: allbugs
- Creation Date: Fri Apr 24 2015 11:39:51 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 17 2018 04:03:55 GMT+0800 (Singapore Standard Time)

<p>
We only have to shrink the range to find the pair:

class Solution {

public:

    vector<int> twoSum(vector<int>& numbers, int target) {
        int lo=0, hi=numbers.size()-1;
        while (numbers[lo]+numbers[hi]!=target){
            if (numbers[lo]+numbers[hi]<target){
                lo++;
            } else {
                hi--;
            }
        }
        return vector<int>({lo+1,hi+1});
    }

};
</p>


