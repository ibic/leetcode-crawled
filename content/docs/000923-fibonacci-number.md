---
title: "Fibonacci Number"
weight: 923
#id: "fibonacci-number"
---
## Description
<div class="description">
<p>The&nbsp;<b>Fibonacci numbers</b>, commonly denoted&nbsp;<code>F(n)</code>&nbsp;form a sequence, called the&nbsp;<b>Fibonacci sequence</b>, such that each number is the sum of the two preceding ones, starting from <code>0</code> and <code>1</code>. That is,</p>

<pre>
F(0) = 0,&nbsp; &nbsp;F(1)&nbsp;= 1
F(N) = F(N - 1) + F(N - 2), for N &gt; 1.
</pre>

<p>Given <code>N</code>, calculate <code>F(N)</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 2
<strong>Output:</strong> 1
<strong>Explanation:</strong> F(2) = F(1) + F(0) = 1 + 0 = 1.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> 3
<strong>Output:</strong> 2
<strong>Explanation:</strong> F(3) = F(2) + F(1) = 1 + 1 = 2.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> 4
<strong>Output:</strong> 3
<strong>Explanation:</strong> F(4) = F(3) + F(2) = 2 + 1 = 3.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<p>0 &le; <code>N</code> &le; 30.</p>

</div>

## Tags
- Array (array)

## Companies
- JPMorgan - 17 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Microsoft - 6 (taggedByAdmin: false)
- Barclays - 6 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Recursion

**Intuition**

Use recursion to compute the Fibonacci number of a given integer.

![fib(5) Recursion diagram](../Figures/509/fibonacciRecursion5.png){:width="539px"}
{:align="center"}

*Figure 1. An example tree representing what `fib(5)` would look like*
{:align="center"}

**Algorithm**

- Check if the provided input value, N, is less than or equal to 1. If true, return N.
- Otherwise, the function `fib(int N)` calls itself, with the result of the 2 previous numbers being added to each other, passed in as the argument.
This is derived directly from the `recurrence relation`:
$$F_{n} = F_{n-1} + F_{n-2}$$

- Do this until all numbers have been computed, then return the resulting answer.

<iframe src="https://leetcode.com/playground/uoCAvhCh/shared" frameBorder="0" width="100%" height="191" name="uoCAvhCh"></iframe>

**Complexity Analysis**

* Time complexity : $$O(2^N)$$. This is the slowest way to solve the `Fibonacci Sequence` because it takes exponential time. The amount of operations needed, for each level of recursion, grows exponentially as the depth approaches `N`.

* Space complexity : $$O(N)$$. We need space proportionate to `N` to account for the max size of the stack, in memory. This stack keeps track of the function calls to `fib(N)`. This has the potential to be bad in cases that there isn't enough physical memory to handle the increasingly growing stack, leading to a `StackOverflowError`. The [Java docs](https://docs.oracle.com/javase/7/docs/api/java/lang/StackOverflowError.html) have a good explanation of this, describing it as an error that occurs because an application recurses too deeply.

<br />

---

#### Approach 2: Bottom-Up Approach using Memoization

**Intuition**

Improve upon the recursive option by using iteration, still solving for all of the sub-problems and returning the answer for N, using already computed Fibonacci values. In using a bottom-up approach, we can iteratively compute and store the values, only returning once we reach the result.

**Algorithm**

- If `N` is less than or equal to 1, return `N`
- Otherwise, iterate through `N`, storing each computed answer in an array along the way.
- Use this array as a reference to the 2 previous numbers to calculate the current Fibonacci number.
- Once we've reached the last number, return it's Fibonacci number.

<iframe src="https://leetcode.com/playground/uEUpwCPg/shared" frameBorder="0" width="100%" height="361" name="uEUpwCPg"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N)$$. Each number, starting at 2 up to and including `N`, is visited, computed and then stored for $$O(1)$$ access later on.

* Space complexity : $$O(N)$$. The size of the data structure is proportionate to `N`.

<br />

---

#### Approach 3: Top-Down Approach using Memoization

**Intuition**

Solve for all of the sub-problems, use memoization to store the pre-computed answers, then return the answer for N. We will leverage recursion, but in a smarter way by not repeating the work to calculate existing values.

**Algorithm**

- Check if `N <= 1`. If it is, return `N`.
- Call and return `memoize(N)`
- If `N` exists in the map, return the cached value for `N`
- Otherwise set the value of `N`, in our mapping, to the value of `memoize(N-1) + memoize(N-2)`

<iframe src="https://leetcode.com/playground/T6ZdXXX4/shared" frameBorder="0" width="100%" height="395" name="T6ZdXXX4"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N)$$. Each number, starting at 2 up to and including `N`, is visited, computed and then stored for $$O(1)$$ access later on.

* Space complexity : $$O(N)$$. The size of the stack in memory is proportionate to `N`.

<br />

---

#### Approach 4: Iterative Top-Down Approach

**Intuition**

Let's get rid of the need to use all of that space and instead use the minimum amount of space required. We can achieve $$O(1)$$ space complexity by only storing the value of the two previous numbers and updating them as we iterate to `N`.

**Algorithm**

- Check if `N <= 1`, if it is then we should return `N`.
- Check if `N == 2`, if it is then we should return `1` since `N` is 2 and `fib(2-1) + fib(2-2)` equals `1 + 0 = 1`.
- To use an iterative approach, we need at least 3 variables to store each state `fib(N)`, `fib(N-1)` and `fib(N-2)`.
- Preset the initial values:
    - Initialize `current` with 0.
    - Initialize `prev1` with 1, since this will represent `fib(N-1)` when computing the current value.
    - Initialize `prev2` with 1, since this will represent `fib(N-2)` when computing the current value.
- Iterate, incrementally by 1, all the way up to and including `N`. Starting at 3, since `0`, `1` and `2` are pre-computed.
- Set the `current` value to `fib(N-1) + fib(N-2)` because that is the value we are currently computing.
- Set the `prev2` value to `fib(N-1)`.
- Set the `prev1` value to `current_value`.
- When we reach `N+1`, we will exit the loop and return the previously set `current` value.

<iframe src="https://leetcode.com/playground/pM6EDZh2/shared" frameBorder="0" width="100%" height="412" name="pM6EDZh2"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N)$$. Each value from `2 to N` will be visited at least once. The time it takes to do this is directly proportionate to `N` where `N` is the `Fibonacci Number` we are looking to compute.

* Space complexity : $$O(1)$$. This requires 1 unit of Space for the integer `N` and 3 units of Space to store the computed values (`curr`, `prev1` and `prev2`) for every loop iteration. The amount of Space doesn't change so this is constant Space complexity.

<br />

---

#### Approach 5: Matrix Exponentiation

**Intuition**

Use Matrix Exponentiation to get the Fibonacci number from the element at (0, 0) in the resultant matrix.

In order to do this we can rely on the matrix equation for the Fibonacci sequence, to find the `Nth` Fibonacci number:
$$
\begin{pmatrix}
 1\,\,1 \\
 1\,\,0
\end{pmatrix}^{n}=\begin{pmatrix}
 \: F_{(n+1)}\;\;\:F_{(n)}\\
 \: F_{(n)}\;\;\:F_{(n-1)}
\end{pmatrix}
$$

**Algorithm**

- Check if `N` is less than or equal to 1. If it is, return `N`.
- Use a recursive function, `matrixPower`, to calculate the power of a given matrix `A`. The power will be `N-1`, where `N` is the `Nth Fibonacci number`.
- The `matrixPower` function will be performed for `N/2` of the Fibonacci numbers.
- Within `matrixPower`, call the `multiply` function to multiply 2 matrices.
- Once we finish doing the calculations, return `A[0][0]` to get the `Nth` Fibonacci number.


<iframe src="https://leetcode.com/playground/y9EwghPh/shared" frameBorder="0" width="100%" height="500" name="y9EwghPh"></iframe>

**Complexity Analysis**

* Time complexity : $$O(\log N)$$. By halving the `N` value in every `matrixPower`'s call to itself, we are halving the work needed to be done.

* Space complexity : $$O(\log N)$$. The size of the stack in memory is proportionate to the function calls to `matrixPower` plus the memory used to account for the matrices which takes up constant space.

<br />

---

#### Approach 6: Math

**Intuition**
Using the `golden ratio`, a.k.a `Binet's forumula`: $$ \varphi = \frac{1 + \sqrt{5}}{2} \approx 1.6180339887.... $$

Here's a [link](http://demonstrations.wolfram.com/GeneralizedFibonacciSequenceAndTheGoldenRatio/) to find out more about how the Fibonacci sequence and the golden ratio work.

We can derive the most efficient solution to this problem using only constant time and constant space!

**Algorithm**

- Use the `golden ratio` formula to calculate the `Nth` Fibonacci number.


<iframe src="https://leetcode.com/playground/vgmYRSh2/shared" frameBorder="0" width="100%" height="157" name="vgmYRSh2"></iframe>

**Complexity Analysis**

* Time complexity : $$O(1)$$. Constant time complexity since we are using no loops or recursion and the time is based on the result of performing the calculation using `Binet's formula`.

* Space complexity : $$O(1)$$. The space used is the space needed to create the variable to store the `golden ratio` formula.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solutions
- Author: pratik_patil
- Creation Date: Wed Jan 09 2019 16:09:41 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jan 09 2019 16:09:41 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1: Iterative**

Time complexity: `O(n)`
Space complexity: `O(1)`

```
class Solution 
{
    public int fib(int N)
    {
        if(N <= 1)
            return N;
        
		int a = 0, b = 1;
		
		while(N-- > 1)
		{
			int sum = a + b;
			a = b;
			b = sum;
		}
        return b;
    }
}
```

**Solution 2: Recursive**

Time complexity: `O(2^n) `- since `T(n) = T(n-1) + T(n-2) `is an exponential time
Space complexity: `O(n)` - space for recursive function call stack

```
class Solution 
{
    public int fib(int N)
    {
        if(N <= 1)
            return N;
        else
            return fib(N - 1) + fib(N - 2);
    }
}
```

**Solution 3: Dynamic Programming - Top Down Approach**

Time complexity: `O(n)`
Space complexity: `O(n)`

```
class Solution 
{
    int[] fib_cache = new int[31];
	
	public int fib(int N)
    {
        if(N <= 1)
            return N;
        else if(fib_cache[N] != 0)
            return fib_cache[N];
		else 
            return fib_cache[N] = fib(N - 1) + fib(N - 2);
    }
}
```

**Solution 4: Dynamic Programming - Bottom Up Approach**

Time complexity: `O(n)`
Space complexity: `O(n)`

```
class Solution 
{
    public int fib(int N)
    {
        if(N <= 1)
            return N;

		int[] fib_cache = new int[N + 1];
		fib_cache[1] = 1;

		for(int i = 2; i <= N; i++)
		{
			fib_cache[i] = fib_cache[i - 1] + fib_cache[i - 2];
		}
		return fib_cache[N];
    }
}
```
</p>


### 6 Python solutions
- Author: mereck
- Creation Date: Mon Jun 10 2019 01:10:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 06 2019 00:36:20 GMT+0800 (Singapore Standard Time)

<p>
naive recursive

```
def fib(N):
	if N == 0: return 0
	if N == 1: return 1
	return fib(N-1) + fib(N-2)
```
memoized recursive

```
memo = {}
def fib(N):

	if N == 0: return 0
	if N == 1: return 1

	if N-1 not in memo: memo[N-1] = fib(N-1)
	if N-2 not in memo: memo[N-2] = fib(N-2)

	return memo[N-1] + memo[N-2]
```

textbook LRU cache 
```
from functools import lru_cache
class Solution:
    @lru_cache(maxsize=None)
    def fib(self, n: int) -> int:
        if n < 2: return n
        return self.fib(n-1) + self.fib(n-2)
```

iterative space-optimized
```
def fib(N):
	if N == 0: return 0
	memo = [0,1]
	for _ in range(2,N+1):
		memo = [memo[-1], memo[-1] + memo[-2]]

	return memo[-1]
```

can use a tuple for better performance

```
def fib(N):
	if N == 0: return 0
	memo = (0,1)
	for _ in range(2,N+1):
		memo = (memo[-1], memo[-1] + memo[-2])

	return memo[-1]
```

or [some math](https://en.wikipedia.org/wiki/Fibonacci_number#Relation_to_the_golden_ratio)

```
def fib(self, N):
	golden_ratio = (1 + 5 ** 0.5) / 2
	return int((golden_ratio ** N + 1) / 5 ** 0.5)
```

</p>


### C++ 4 Solutions Explained [Recursive | Iterative with DP | Imperative | Binet's]
- Author: ShehabM
- Creation Date: Sun Jan 13 2019 20:49:49 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 09 2020 19:27:11 GMT+0800 (Singapore Standard Time)

<p>
### Solution 1 - Recursive Approach 
Time Complexity   - O(2^N)
Space Complexity - O(N) given the function call stack size
```
    int fib(int N) {
        if(N == 0)  return 0;
        if(N == 1)  return 1;
        return fib(N-1) + fib(N-2);
    }
```
### Solution 2 - Dynamic Programming Approach 
Use memoization to store perviously computed fibonacci values. 
Time Complexity - O(N)
Space Complexity - O(N)
```
    int fib(int N) {
        if(N < 2)
            return N;
        int memo[N+1];
        memo[0] = 0;
        memo[1] = 1;
        for(int i=2; i<=N; i++)
            memo[i] = memo[i-1] + memo[i-2];
        return memo[N];
    }
```
### Solution 3 - Imperative Approach (Bottom Up DP)
With Imperative approach, we step through the loop and optimize the space by storing only two previous fibonacci values in two variables.
Time Complexity   - O(N)
Space Complexity - O(1)
```
    int fib(int N) {
        if(N < 2) 
            return N;
    	int a = 0, b = 1, c = 0;
        for(int i = 1; i < N; i++)
        {
            c = a + b;
            a = b;
            b = c;
        }
        return c;
    }
```
### Solution 4 - Binet\'s Nth-term Formula
Using Binet\'s Formula for the Nth Fibonacci involves the usage of our golden section number **Phi**.
**Phi** = ( sqrt(5) + 1 ) / 2
Using approximation equation is good enough here, since we know N >= 0 && N <= 30, we can safely use the following rounded function
Fib(N) = round( ( **Phi**^N ) / sqrt(5) )
Full mathematical explanation of Binet\'s Formula [here](http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibFormula.html)
Time Complexity   - O(1)
Space Complexity - O(1)
```
    int fib(int N) {
        double phi = (sqrt(5) + 1) / 2;     
        return round(pow(phi, N) / sqrt(5));
    }
```

</p>


