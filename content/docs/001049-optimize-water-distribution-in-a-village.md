---
title: "Optimize Water Distribution in a Village"
weight: 1049
#id: "optimize-water-distribution-in-a-village"
---
## Description
<div class="description">
<p>There are <code><font face="monospace">n</font></code>&nbsp;houses in a village. We want to supply water for all the houses by building wells and laying pipes.</p>

<p>For each house <code>i</code>, we can either build a well inside it directly with cost <code>wells[i]</code>, or pipe in water from another well to it. The costs to lay pipes between houses are given by the array <code>pipes</code>, where each&nbsp;<code>pipes[i] = [house1, house2, cost]</code>&nbsp;represents the cost to connect&nbsp;<code>house1</code>&nbsp;and <code>house2</code>&nbsp;together using a pipe.&nbsp;Connections are bidirectional.</p>

<p>Find the minimum total cost to supply water to all houses.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/05/22/1359_ex1.png" style="width: 189px; height: 196px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 3, wells = [1,2,2], pipes = [[1,2,1],[2,3,1]]
<strong>Output:</strong> 3
<strong>Explanation: </strong>
The image shows the costs of connecting houses using pipes.
The best strategy is to build a well in the first house with cost 1 and connect the other houses to it with cost 2 so the total cost is 3.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n&nbsp;&lt;= 10000</code></li>
	<li><code>wells.length == n</code></li>
	<li><code>0 &lt;= wells[i] &lt;= 10^5</code></li>
	<li><code>1 &lt;= pipes.length &lt;= 10000</code></li>
	<li><code>1 &lt;= pipes[i][0], pipes[i][1] &lt;= n</code></li>
	<li><code>0 &lt;= pipes[i][2] &lt;= 10^5</code></li>
	<li><code>pipes[i][0] != pipes[i][1]</code></li>
</ul>

</div>

## Tags
- Union Find (union-find)
- Graph (graph)

## Companies
- Google - 3 (taggedByAdmin: true)
- Yahoo - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Python/Java] Hidden Well in House 0
- Author: lee215
- Creation Date: Sun Aug 25 2019 00:06:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 25 2019 01:33:09 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
I take it this way:
We cannot build any well.
There is one and only one hidding well in my house (house 0).
The cost to lay pipe between `house[i]` and my house is `wells[i]`.

In order to supply water to the whole village,
we need to make the village a coonected graph.
<br>

## **Explanation**
Merge all costs of pipes together and sort by key.
Greedily lay the pipes if it can connect two seperate union.
Appply union find to record which houses are connected.
<br>

## **Complexity**
Time `O(ElogE)`
Space `O(N)`
<br>


**C++**
```cpp
    vector<int> uf;
    int minCostToSupplyWater(int n, vector<int>& wells, vector<vector<int>>& pipes) {
        uf.resize(n + 1, 0);
        for (auto& p : pipes) swap(p[0], p[2]);
        for (int i = 0; i < n; ++i) {
            uf[i + 1] = i + 1;
            pipes.push_back({wells[i], 0, i + 1});
        }
        sort(pipes.begin(), pipes.end());

        int res = 0;
        for (int i = 0; n > 0; ++i) {
            int x = find(pipes[i][1]), y = find(pipes[i][2]);
            if (x != y) {
                res += pipes[i][0];
                uf[x] = y;
                --n;
            }
        }
        return res;
    }

    int find(int x) {
        if (x != uf[x]) uf[x] = find(uf[x]);
        return uf[x];
    }
```

**Python:**
```python
    def minCostToSupplyWater(self, n, wells, pipes):
        uf = {i: i for i in xrange(n + 1)}

        def find(x):
            if x != uf[x]:
                uf[x] = find(uf[x])
            return uf[x]

        w = [[c, 0, i] for i, c in enumerate(wells, 1)]
        p = [[c, i, j] for i, j, c in pipes]
        res = 0
        for c, x, y in sorted(w + p):
            x, y = find(x), find(y)
            if x != y:
                uf[find(x)] = find(y)
                res += c
                n -= 1
            if n == 0:
                return res
```

**Java**
```java
    int[] uf;
    public int minCostToSupplyWater(int n, int[] wells, int[][] pipes) {
        uf = new int[n + 1];
        List<int[]> edges = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            uf[i + 1] = i + 1;
            edges.add(new int[] {0, i + 1, wells[i]});
        }
        for (int[] p : pipes) {
            edges.add(p);
        }
        Collections.sort(edges, (a, b) -> Integer.compare(a[2], b[2]));

        int res = 0;
        for (int[] e : edges) {
            int x = find(e[0]), y = find(e[1]);
            if (x != y) {
                res += e[2];
                uf[x] = y;
                --n;
            }
        }
        return res;
    }

    private int find(int x) {
        if (x != uf[x]) uf[x] = find(uf[x]);
        return uf[x];
    }
```
</p>


### [C++] Kruskal's algorithm + Clever trick (with explanation)
- Author: mhelvens
- Creation Date: Sun Aug 25 2019 00:01:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 25 2019 01:01:58 GMT+0800 (Singapore Standard Time)

<p>
This already feels like a [Minimum Spanning Tree problem](https://en.wikipedia.org/wiki/Minimum_spanning_tree), except that in addition to connecting houses, we have the option of digging new wells.

The trick here is to view _"digging new wells"_ as _"laying new pipes to an original water-souce node 0"_. Then it becomes a straight-forward MST problem.

The code below uses [Kruskal\'s algorithm](https://en.wikipedia.org/wiki/Kruskal%27s_algorithm).

```C++
int minCostToSupplyWater(int n, vector<int>& wells, vector<vector<int>>& pipes) {
  // Represent the wells as additional pipes to a water-source node 0
  for (int i = 1; i <= n; ++i)
    pipes.push_back({0, i, wells[i-1]});

  // Kruskal\'s algorithm
  UnionFind uf(n+1);
  sort(pipes.begin(), pipes.end(), [](auto& a, auto& b){ return a[2] < b[2]; });
  int result = 0;
  for (auto& pipe : pipes) {
    if (uf.get(pipe[0]) != uf.get(pipe[1])) {
      result += pipe[2];
      uf.merge(pipe[0], pipe[1]);
    }
  }

  return result;
}
```

And here is the union-find datastructure I used (place it before your `Solution` class).

Note that it could be made more efficient with the [\'union by rank\' or \'union by size\' technique](https://en.wikipedia.org/wiki/Disjoint-set_data_structure#Union), but I chose to keep this one simple.

```C++
class UnionFind {
public:
  UnionFind(int N) : unionFind(N) {
    for (int i = 0; i < N; ++i)
      unionFind[i] = i;
  }

  void merge(int key1, int key2) {
    key1 = get(key1);
    key2 = get(key2);
    if (key1 == key2) return;
    unionFind[key2] = key1;
  }
  
  int get(int key) {
    while (key != unionFind[key])
      key = unionFind[key] = unionFind[unionFind[key]];
    return key;
  }
  
private:
  vector<int> unionFind;
};
```
</p>


### Finally I figured out why my Prim's didn't work! Why pipes[a][b] != pipes[b][a]???
- Author: WangQiuc
- Creation Date: Sun Aug 25 2019 04:46:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 31 2019 11:13:08 GMT+0800 (Singapore Standard Time)

<p>
I spent whole morning trying to figure out why my Prim\'s didn\'t pass!

My solution failed at a test case with 60 nodes. I just can\'t image how to debug on that and I could\'t figure out what\'s the issue in my solution until I saw @dylan20\'s [post](https://leetcode.com/problems/optimize-water-distribution-in-a-village/discuss/365986/Python-Dijkstra). Thank you so much!
The issue turned out to be the way I built my graph.
```
	for u, v, w in pipes:
		graph[u][v] = graph[v][u] = w
```
The test case, however, have occasions as `pipes[a][b] != pipes[b][a]`! That makes `graph[u][v]` could cost more than it should be if I naive setting `graph[u][v] = w`. Instead, it should be like:
```
graph[u][v] = graph[v][u] = min(graph[v].get(u, float(\'inf\')), w)
```
After I fixed that, the solution just passed:
```
def minCostToSupplyWaterPrim(n, wells, pipes):
	seen, graph = set(), defaultdict(dict)
	for u, v, w in pipes:
		graph[u][v] = graph[v][u] = min(graph[v].get(u, float(\'inf\')), w)
	cost, undone, pq = 0, n, [(w, i) for i, w in enumerate(wells, 1)]
	heapq.heapify(pq)
	while pq:
		c, x = heapq.heappop(pq)
		if x not in seen:
			cost += c
			undone -= 1
			if not undone:
				break
			seen.add(x)
			for y, w in graph[x].items():
				if y not in seen:
					heapq.heappush(pq, (w, y))
	return cost
```
But why cost connection `house1` and `house2`, and `house2` and `house1` are different?! That doesn\'t make sense to me. But still so glad that I\'ve found the issue. Could be a challenge in the interview or work.

**Meanwhile Kruskal\'s doesn\'t suffer from this issue as it iterates costs in the sorted way.**
```
def minCostToSupplyWaterKruskal(n, wells, pipes):
	p = list(range(n+1))

	def find(x):
		if x != p[x]:
			p[x] = find(p[x])
		return p[x]

	def union(x, y):
		px, py = find(x), find(y)
		if px != py:
			p[py] = px
			return True
		return False

	cost, undone = 0, n
	for u, v, w in sorted([[0,i,w] for i, w in enumerate(wells,1)] + pipes, key=lambda x:x[2]):
		if union(u, v):
			cost += w
			undone -= 1
		if not undone:
			break
	return cost
```
We can set `0` as water source that connected with each well which needs to be eventually unioned.

</p>


