---
title: "Minimum Number of Days to Make m Bouquets"
weight: 1361
#id: "minimum-number-of-days-to-make-m-bouquets"
---
## Description
<div class="description">
<p>Given an integer array <code>bloomDay</code>, an integer <code>m</code> and an integer <code>k</code>.</p>

<p>We need to make <code>m</code>&nbsp;bouquets. To make a bouquet,&nbsp;you need to use <code>k</code> <strong>adjacent flowers</strong> from the garden.</p>

<p>The garden consists of <code>n</code> flowers, the <code>ith</code> flower will bloom in the <code>bloomDay[i]</code>&nbsp;and then can be used in&nbsp;<strong>exactly one</strong> bouquet.</p>

<p>Return <em>the minimum number of days</em> you need to wait to be able to make <code>m</code> bouquets from the garden. If it is impossible to make <code>m</code> bouquets return <strong>-1</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> bloomDay = [1,10,3,10,2], m = 3, k = 1
<strong>Output:</strong> 3
<strong>Explanation:</strong> Let&#39;s see what happened in the first three days. x means flower bloomed and _ means flower didn&#39;t bloom in the garden.
We need 3 bouquets each should contain 1 flower.
After day 1: [x, _, _, _, _]   // we can only make one bouquet.
After day 2: [x, _, _, _, x]   // we can only make two bouquets.
After day 3: [x, _, x, _, x]   // we can make 3 bouquets. The answer is 3.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> bloomDay = [1,10,3,10,2], m = 3, k = 2
<strong>Output:</strong> -1
<strong>Explanation:</strong> We need 3 bouquets each has 2 flowers, that means we need 6 flowers. We only have 5 flowers so it is impossible to get the needed bouquets and we return -1.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> bloomDay = [7,7,7,7,12,7,7], m = 2, k = 3
<strong>Output:</strong> 12
<strong>Explanation:</strong> We need 2 bouquets each should have 3 flowers.
Here&#39;s the garden after the 7 and 12 days:
After day 7: [x, x, x, x, _, x, x]
We can make one bouquet of the first three flowers that bloomed. We cannot make another bouquet from the last three flowers that bloomed because they are not adjacent.
After day 12: [x, x, x, x, x, x, x]
It is obvious that we can make two bouquets in different ways.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> bloomDay = [1000000000,1000000000], m = 1, k = 1
<strong>Output:</strong> 1000000000
<strong>Explanation:</strong> You need to wait 1000000000 days to have a flower ready for a bouquet.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> bloomDay = [1,10,2,9,3,8,4,7,5,6], m = 4, k = 2
<strong>Output:</strong> 9
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>bloomDay.length == n</code></li>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
	<li><code>1 &lt;= bloomDay[i] &lt;= 10^9</code></li>
	<li><code>1 &lt;= m &lt;= 10^6</code></li>
	<li><code>1 &lt;= k &lt;= n</code></li>
</ul>
</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Binary Search
- Author: lee215
- Creation Date: Sun Jun 14 2020 12:04:17 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 15 2020 14:13:39 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
If `m * k > n`, it impossible, so return -1.
Otherwise, it\'s possible, we can binary search the result.
`left = 1` is the smallest days,
`right = 1e9` is surely big enough to get m bouquests.
So we are going to binary search in range `[left, right]`.
<br>

# **Explanation**
Given `mid` days, we can know which flowers blooms.
Now the problem is, given an array of `true` and `false`,
find out how many adjacent `true`  bouquest in total.

If `bouq < m`, `mid` is still small for `m` bouquest.
So we turn `left = mid + 1`

If `bouq >= m`, `mid` is big enough for `m` bouquest.
So we turn `right = mid`
<br>

# **Complexity**
Time `O(Nlog(maxA))`
Space `O(1)`

Note that the result must be one `A[i]`,
so actually we can sort A in `O(NlogK)`,
Where `K` is the number of different values.
and then binary search the index of different values.

Though I don\'t thik worth doing that.
<br>

**Java:**
```java
    public int minDays(int[] A, int m, int k) {
        int n = A.length, left = 1, right = (int)1e9;
        if (m * k > n) return -1;
        while (left < right) {
            int mid = (left + right) / 2, flow = 0, bouq = 0;
            for (int j = 0; j < n; ++j) {
                if (A[j] > mid) {
                    flow = 0;
                } else if (++flow >= k) {
                    bouq++;
                    flow = 0;
                }
            }
            if (bouq < m) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    }
```

**C++:**
```cpp
    int minDays(vector<int>& A, int m, int k) {
        int n = A.size(), left = 1, right = 1e9;
        if (m * k > n) return -1;
        while (left < right) {
            int mid = (left + right) / 2, flow = 0, bouq = 0;
            for (int j = 0; j < n; ++j) {
                if (A[j] > mid) {
                    flow = 0;
                } else if (++flow >= k) {
                    bouq++;
                    flow = 0;
                }
            }
            if (bouq < m) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    }
```

**Python:**
```py
    def minDays(self, A, m, k):
        if m * k > len(A): return -1
        left, right = 1, max(A)
        while left < right:
            mid = (left + right) / 2
            flow = bouq = 0
            for a in A:
                flow = 0 if a > mid else flow + 1
                if flow >= k:
                    flow = 0
                    bouq += 1
                    if bouq == m: break
            if bouq == m:
                right = mid
            else:
                left = mid + 1
        return left
```
<br>

# More Good Binary Search Problems
Here are some similar binary search problems.
Also find more explanations.
Good luck and have fun.

- 5455. [Minimum Number of Days to Make m Bouquets](https://leetcode.com/problems/minimum-number-of-days-to-make-m-bouquets/discuss/686316/javacpython-binary-search/578488)
- 1283. [Find the Smallest Divisor Given a Threshold](https://leetcode.com/problems/find-the-smallest-divisor-given-a-threshold/discuss/446376/javacpython-bianry-search/401806)
- 1231. [Divide Chocolate](https://leetcode.com/problems/divide-chocolate/discuss/408503/Python-Binary-Search)
- 1011. [Capacity To Ship Packages In N Days](https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/discuss/256729/javacpython-binary-search/351188?page=3)
- 875. [Koko Eating Bananas](https://leetcode.com/problems/koko-eating-bananas/discuss/152324/C++JavaPython-Binary-Search)
- 774. [Minimize Max Distance to Gas Station](https://leetcode.com/problems/minimize-max-distance-to-gas-station/discuss/113633/Easy-and-Concise-Solution-using-Binary-Search-C++JavaPython)
- 410. [Split Array Largest Sum](https://leetcode.com/problems/split-array-largest-sum/)
<br>

# Solution 2:
Note that the result must be one `A[i]`,
so actually we can sort A in `O(NlogK)`,
Where `K` is the number of different values.
and then binary search the index of different values.

I notcied many mentioned that.
I didn\'t do that for the 2 main reasons,
1. this didn\'t change the main idea of binary search,
and save just O(log1000) in time complexity,
in sacrifice much of writing complexity.
2. If we already sort `A` out,
we don\'t even need `O(NlogN)` binary search at all.
The problem can be already solved in `O(N)`

The idea of `O(N)` is that,
One pass the bloom days from in ascending order,
mark the bloomed flower,
calculate the the continous flowers connnected,
update the bouquets and compared with `m`.




</p>


### Intuition behind the Bin Search, video explanation, w/code
- Author: rachilies
- Creation Date: Sun Jun 14 2020 16:08:53 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 18 2020 03:41:26 GMT+0800 (Singapore Standard Time)

<p>
Hi folks,
Here is my attempt to explain the intuition behind the binary search in this video.
Hope it helps,
If there is any comment/feedback please do leave that in the comments section.

https://www.youtube.com/watch?v=paYIrQKxE7I

Also for other similar problems from leetcode, please checkout my channel,
https://www.youtube.com/channel/UC6uQdd7kLLOdlHSVklhV7Cw

</p>


### [C++] Binary search
- Author: PhoenixDD
- Creation Date: Sun Jun 14 2020 12:01:02 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 15 2020 02:31:16 GMT+0800 (Singapore Standard Time)

<p>
**Observation**

Can we get the number of bouquets that can form at day `d` ?
Yes we can, just run a simple count to get the number adjacent bloomed flowers that are greater than or equal to `k` by checking if the day they bloom is less than or equal to `d`(be careful of continuous flowers that can make 2 bouquets eg 2*k bloomed flowers).

Once we have the previous logic, we can simply run binary search on the range `min` and `max` days from the input and try to minimize the day where we can get atleast `m` bouquets.
In the end we only need to check if the minimized `day` has atleast `m` bouquets. This is our answer.

**Solution**
```c++
class Solution {
public:
    int getBouq(vector<int>& bloomDay,int day,int k)	//Get the number of bouquets of size k at a certain day.
    {
        int result=0,count=0;
        for(int i=0;i<bloomDay.size();i++)
        {
            if(bloomDay[i]<=day)
                count++;
            else
                count=0;
            if(count==k)    //Reset the window size to 0 to consider the case with 2*k or more continous bloomed flowers.
                result++,count=0;
        }
        return result;
    }
    int minDays(vector<int>& bloomDay, int m, int k) 
    {
        auto p=minmax_element(bloomDay.begin(),bloomDay.end());
        int j=*p.second,i=*p.first;
        int mid;
        while(i<j)				//Binary search on the range to get the minimum day where we may get `m` bouquets.
        {
            mid=(i+j)>>1;
            if(getBouq(bloomDay,mid,k)<m)
                i=mid+1;
            else
                j=mid;
        }
        return getBouq(bloomDay,i,k)>=m?i:-1;		//Final check if the result day can have atleast `m` bouquets.
    }
};
```
**Complexity**
Time: `O(nlog(max(bloomDay)-min(bloomDay))` where `max(bloomDay)` is the maximum value and `min(bloomDay)` is the minimum value of the `bloomDay` input.
Space: `O(1)`.
</p>


