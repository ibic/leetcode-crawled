---
title: "Triples with Bitwise AND Equal To Zero"
weight: 933
#id: "triples-with-bitwise-and-equal-to-zero"
---
## Description
<div class="description">
<p>Given an array of integers <code>A</code>, find the number of&nbsp;triples of indices (i, j, k)&nbsp;such that:</p>

<ul>
	<li><code>0 &lt;= i &lt; A.length</code></li>
	<li><code>0 &lt;= j &lt; A.length</code></li>
	<li><code>0 &lt;= k &lt; A.length</code></li>
	<li><code>A[i]&nbsp;&amp; A[j]&nbsp;&amp; A[k] == 0</code>, where <code>&amp;</code>&nbsp;represents the bitwise-AND operator.</li>
</ul>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[2,1,3]</span>
<strong>Output: </strong><span id="example-output-1">12</span>
<strong>Explanation: </strong>We could choose the following i, j, k triples:
(i=0, j=0, k=1) : 2 &amp; 2 &amp; 1
(i=0, j=1, k=0) : 2 &amp; 1 &amp; 2
(i=0, j=1, k=1) : 2 &amp; 1 &amp; 1
(i=0, j=1, k=2) : 2 &amp; 1 &amp; 3
(i=0, j=2, k=1) : 2 &amp; 3 &amp; 1
(i=1, j=0, k=0) : 1 &amp; 2 &amp; 2
(i=1, j=0, k=1) : 1 &amp; 2 &amp; 1
(i=1, j=0, k=2) : 1 &amp; 2 &amp; 3
(i=1, j=1, k=0) : 1 &amp; 1 &amp; 2
(i=1, j=2, k=0) : 1 &amp; 3 &amp; 2
(i=2, j=0, k=1) : 3 &amp; 2 &amp; 1
(i=2, j=1, k=0) : 3 &amp; 1 &amp; 2
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code><font face="monospace">1 &lt;= A.length &lt;= 1000</font></code></li>
	<li><code>0 &lt;= A[i] &lt; 2^16</code></li>
</ol>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 2 (taggedByAdmin: false)
- Flipkart - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java DP O(3 * 2^16 * n) time O(2^16) space
- Author: wangzi6147
- Creation Date: Sun Jan 27 2019 12:22:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 27 2019 12:22:40 GMT+0800 (Singapore Standard Time)

<p>
**Update:**

Thanks for the inspiring solutions in the comments using `HashMap` which runs `O(n^2)` time. I didn\'t come up with such solutions in the contest. That\'s really efficient for this problem specifically. However, I think my solution is more scalable in terms of **the number of indices to be picked** is very large. Say it\'s `M` instead of `3`, the runtime is `O(M * 2^16 * n)`.

Here I updated an `O(M * 2^16)` space version to help understanding, where `dp[i][j]` represents the number of combinations if we pick `i` numbers where the `AND` of these numbers is `j`:

```
class Solution {
    public int countTriplets(int[] A) {
        int N = 1 << 16, M = 3;
        int[][] dp = new int[M + 1][N];
        dp[0][N - 1] = 1;
        for (int i = 0; i < M; i++) {
            for (int k = 0; k < N; k++) {
                for (int a : A) {
                    dp[i + 1][k & a] += dp[i][k];
                }
            }
        }
        return dp[M][0];
    }
}
```

Appreciate for other solutions again!

**Original Post:**

For each `i`, assume that if we pick `i` numbers from `A` array(allow duplicates), the `AND` of these `i` numbers is `k`.
Then `dp[k]` represents the number of combinations for such `i` and `k`. We update this `dp` array for 3 times.

Example:
`i=2` means we pick `2` numbers.
`dp[10] = 5` means when we pick `2` numbers, the count of combinations is `5`, where the `AND` result of such numbers is `10`.

Tricky:
We initialize the `dp[(1<<16) - 1]` to 1 because the AND result of every number with `(1<<16) - 1` is the number itself.

Time complexity:
`O(3 * 2^16 * n)`

Space:
`O(2^16)`

```
class Solution {
    public int countTriplets(int[] A) {
        int N = 1 << 16;
        int[] dp = new int[N];
        dp[N - 1] = 1;
        for (int i = 0; i < 3; i++) {
            int[] temp = new int[N];
            for (int k = 0; k < N; k++) {
                for (int a : A) {
                    temp[k & a] += dp[k];
                }
            }
            dp = temp;
        }
        return dp[0];
    }
}
```
</p>


### C++ naive O(n * n)
- Author: votrubac
- Creation Date: Mon Jan 28 2019 07:16:47 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 28 2019 07:16:47 GMT+0800 (Singapore Standard Time)

<p>
First, we process all tuples ```A[i]``` and ```A[j]```, and store the result of ```A[i] & A[j]``` in the hash map ```tuples```. We also count there how many times each result was produced.

Then, we go through the array again, and check each element against all tuples. If it produces zero, we add the tuple counter to the result.
```
int countTriplets(vector<int>& A, int cnt = 0) {
  unordered_map<int, int> tuples;
  for (auto a : A)
    for (auto b : A) ++tuples[a & b];
  for (auto a : A)
    for (auto t : tuples)
      if ((t.first & a) == 0) cnt += t.second;
  return cnt;
}
```
If we know that the input is large, we could use an array instead of hash set. The size of this array will be ```1 << 16```. This will speed up things quite a bit.
```
int countTriplets(vector<int>& A, int cnt = 0) {
  int tuples[1 << 16] = {};
  for (auto a : A)
    for (auto b : A) ++tuples[a & b];
  for (auto a : A)
    for (auto i = 0; i < (1 << 16); ++i)
      if ((i & a) == 0) cnt += tuples[i];
  return cnt;
}
```
## Complexity Analysis
Time: *O(n * n)*. We can have 2^16 tuples at most. When n is large, n * n will dominate n * 2^16. So it is O(n * n) in the big O notation.
Memory: *O(n)* for the first solution; *O(2 ^ 16)*, or, stricter, *O(1)* for the second one.
</p>


### Fastest Solution Using Fast Walsh Hadamard Transform [32ms]
- Author: basselbakr
- Creation Date: Sun Jan 27 2019 14:08:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 27 2019 14:08:32 GMT+0800 (Singapore Standard Time)

<p>
*`O(n log n)`* where ***`n`*** is number of masks = *`2^16`*

The best part is that you can edit:
```cpp
for(auto& x : b) x = x * x * x;
```
to
```cpp
for(auto& x : b) x = pow(x, k);
```
if you want to choose ***`k`*** numbers
```cpp
class Solution {
public:
    int countTriplets(vector<int>& a) {
        int n = a.size();
        vector<int> b(1 << 16); 
        for(auto x : a) b[x]++;
        fwht(b, false);
        for(auto& x : b) x = x * x * x;
        fwht(b, true);
        return b[0];
    }
    
    void fwht(vector<int>& a, bool inv) {
        int n = a.size();
        for(int m = 1; 2 * m <= n; m *= 2) {
            for(int i = 0; i < n; i += 2 * m) {
                for(int j = 0; j < m; ++j) {
                    auto x = a[i + j];
                    auto y = a[i + j + m];
                    if(not inv) {
                        a[i + j] = y;
                        a[i + j + m] = x + y;
                    } else {                        
                        a[i + j] = -x + y;
                        a[i + j + m] = x;
                    }
                }
            }
        }
    }
};
```
</p>


