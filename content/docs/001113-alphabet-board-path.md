---
title: "Alphabet Board Path"
weight: 1113
#id: "alphabet-board-path"
---
## Description
<div class="description">
<p>On an alphabet board, we start at position <code>(0, 0)</code>, corresponding to character&nbsp;<code>board[0][0]</code>.</p>

<p>Here, <code>board = [&quot;abcde&quot;, &quot;fghij&quot;, &quot;klmno&quot;, &quot;pqrst&quot;, &quot;uvwxy&quot;, &quot;z&quot;]</code>, as shown in the diagram below.</p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/07/28/azboard.png" style="width: 250px; height: 317px;" /></p>

<p>We may make the following moves:</p>

<ul>
	<li><code>&#39;U&#39;</code> moves our position up one row, if the position exists on the board;</li>
	<li><code>&#39;D&#39;</code> moves our position down one row, if the position exists on the board;</li>
	<li><code>&#39;L&#39;</code> moves our position left one column, if the position exists on the board;</li>
	<li><code>&#39;R&#39;</code> moves our position right one column, if the position exists on the board;</li>
	<li><code>&#39;!&#39;</code>&nbsp;adds the character <code>board[r][c]</code> at our current position <code>(r, c)</code>&nbsp;to the&nbsp;answer.</li>
</ul>

<p>(Here, the only positions that exist on the board are positions with letters on them.)</p>

<p>Return a sequence of moves that makes our answer equal to <code>target</code>&nbsp;in the minimum number of moves.&nbsp; You may return any path that does so.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> target = "leet"
<strong>Output:</strong> "DDR!UURRR!!DDD!"
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> target = "code"
<strong>Output:</strong> "RR!DDRR!UUL!R!"
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= target.length &lt;= 100</code></li>
	<li><code>target</code> consists only of English lowercase letters.</li>
</ul>
</div>

## Tags
- Hash Table (hash-table)
- String (string)

## Companies
- Google - 6 (taggedByAdmin: true)
- Zulily - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java O(n)
- Author: votrubac
- Creation Date: Sun Jul 28 2019 12:12:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 30 2019 02:47:37 GMT+0800 (Singapore Standard Time)

<p>
Determine the coordinate and move there. Note that \'z\' is tricky as you cannot move left or right in the last row.

To account for that, make sure we move up before moving right, and move left before moving down.
## C++
```
string alphabetBoardPath(string target, int x = 0, int y = 0) {
  string res;
  for (auto ch : target) {
    int x1 = (ch - \'a\') % 5, y1 = (ch - \'a\') / 5;
    res += string(max(0, y - y1), \'U\') + string(max(0, x1 - x), \'R\') +
      string(max(0, x - x1), \'L\') + string(max(0, y1 - y), \'D\') + "!";
    x = x1, y = y1;
  }
  return res;
}
```
## Java
> Would be nice to have Java 11 support, so we can just do ```"U".repeat(Math.max(0, y - y1));```
```
public String alphabetBoardPath(String target) {
  int x = 0, y = 0;
  StringBuilder sb = new StringBuilder();
  for (char ch : target.toCharArray()) {
    int x1 = (ch - \'a\') % 5, y1 = (ch - \'a\') / 5;
    sb.append(String.join("", Collections.nCopies(Math.max(0, y - y1), "U")) +
      String.join("", Collections.nCopies(Math.max(0, x1 - x), "R")) +
      String.join("", Collections.nCopies(Math.max(0, x - x1), "L")) +
      String.join("", Collections.nCopies(Math.max(0, y1 - y), "D")) + "!");
    x = x1; y = y1;
  }
  return sb.toString();
}
```
</p>


### [Python] Easy Solution
- Author: lee215
- Creation Date: Sun Jul 28 2019 12:03:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 28 2019 12:03:35 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Calculate this difference of coordinates.
<br>

## **Explanation**
Notice that moving down and moving right,
may move into a square that doesn\'t exist.
To avoid this, we put `L U` before `R D`.
<br>

## **Complexity**
Time `O(N)`
Space `O(N)`
<br>

**Python:**
```python
    def alphabetBoardPath(self, target):
        m = {c: [i / 5, i % 5] for i, c in enumerate("abcdefghijklmnopqrstuvwxyz")}
        x0, y0 = 0, 0
        res = []
        for c in target:
            x, y = m[c]
            if y < y0: res.append(\'L\' * (y0 - y))
            if x < x0: res.append(\'U\' * (x0 - x))
            if x > x0: res.append(\'D\' * (x - x0))
            if y > y0: res.append(\'R\' * (y - y0))
            res.append(\'!\')
            x0, y0 = x, y
        return "".join(res)
```

</p>


### [Java] clean and short solution, easy to understand
- Author: tumei
- Creation Date: Sun Jul 28 2019 12:29:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 28 2019 13:08:57 GMT+0800 (Singapore Standard Time)

<p>
I tried to store the whole board first, then realized that we could just caculate the indices directly.
By comparing the current indices and the previous ones, the path could be found. 
```
public String alphabetBoardPath(String target) {
        if(target == null) return "";
        char[] chs = target.toCharArray();
        StringBuilder sb = new StringBuilder();
        int previ = 0, prevj = 0;
        for(int i = 0; i < chs.length; i++) {
            int curi = (chs[i] - \'a\') / 5;
            int curj = (chs[i] - \'a\') % 5;
            if(curi == previ && curj == prevj) {
                sb.append("!");
            } else {
                printPath(sb, previ, prevj, curi, curj); 
                sb.append("!");
                previ = curi; prevj = curj;
            }
        }
        
        return sb.toString();
    }
    
    private void printPath(StringBuilder sb, int previ, int prevj, int curi, int curj) {       
        while(curi < previ) {
            sb.append("U"); curi++;
        } 
        while(curj > prevj) {
            sb.append("R"); curj--;
        } 
        while(curj < prevj) {
            sb.append("L"); curj++;
        }  
        while(curi > previ) {
            sb.append("D"); curi--;
        }                      
    }
</p>


