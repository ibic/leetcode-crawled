---
title: "Find Minimum in Rotated Sorted Array II"
weight: 154
#id: "find-minimum-in-rotated-sorted-array-ii"
---
## Description
<div class="description">
<p>Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.</p>

<p>(i.e., &nbsp;<code>[0,1,2,4,5,6,7]</code>&nbsp;might become &nbsp;<code>[4,5,6,7,0,1,2]</code>).</p>

<p>Find the minimum element.</p>

<p>The array may contain duplicates.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> [1,3,5]
<strong>Output:</strong> 1</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> [2,2,2,0,1]
<strong>Output:</strong> 0</pre>

<p><strong>Note:</strong></p>

<ul>
	<li>This is a follow up problem to&nbsp;<a href="https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/description/">Find Minimum in Rotated Sorted Array</a>.</li>
	<li>Would allow duplicates affect the run-time complexity? How and why?</li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Variant of Binary Search

**Intuition**

Given a sorted array in ascending order (denoted as `L[i]`), the array is then rotated over certain _unknown pivot_, (denoted as `L'[i]`). We are asked to find the _minimum value_ of this sorted and rotated array, which is to find the value of the first element in the original array, _i.e._ `L[0]`.

The problem resembles a common problem of _finding a given value from a sorted array_, to which problem one could apply the **binary search** algorithm. Intuitively, one might wonder if we could apply a variant of binary search algorithm to solve our problem here.

Indeed, this is the right intuition, though the tricky part is to figure out a _**concise solution**_ that could work for all cases.

To illustrate the algorithm, we draw the array in a 2D dimension in the following graph, where the X axis indicates the index of each element in the array and the Y axis indicates the value of the element.

![pic](../Figures/154/154_axis.png)

_The main structure of our algorithm remains the same as the classical binary search algorithm._ As a reminder, we summarize it briefly as follows:

- We keep two pointers, _i.e._ `low`, `high` which point to the lowest and highest boundary of our search scope.
<br/>
- We then reduce the search scope by moving either of pointers, according to various situations. Usually we shift one of pointers to the mid point between `low` and `high`, (_i.e._ `pivot = (low+high)/2`), which reduces the search scope down to half. This is also where the name of the algorithm comes from.
<br/>
- The reduction of the search scope would stop, either we find the desired element or the two pointers converge (_i.e._ `low == high`).


**Algorithm**

In the classical binary search algorithm, we would compare the pivot element (_i.e._ `nums[pivot]`) with the value that we would like to locate. In our case, however, we would compare the pivot element to the element pointed by the upper bound pointer (_i.e._ `nums[high]`).

>Following the structure of the binary search algorithm, the essential part remained is to design the cases on how to update the two pointers.

Here we give one example on how we can break it down _**concisely**_ into three cases. Note that given the array, we consider the element pointed by the `low` index to be on the left-hand side of the array, and the element pointed by the `high` index to be on the right-hand side.  

>Case 1). `nums[pivot] < nums[high]`

![pic](../Figures/154/154_case_1.png)

- The pivot element resides in _the same half_ as the upper bound element.
- Therefore, the desired minimum element should reside to the **left-hand side** of pivot element. As a result, we then move the upper bound down to the pivot index, _i.e._ `high = pivot`.

>Case 2). `nums[pivot] > nums[high]`

![pic](../Figures/154/154_case_2.png)

- The pivot element resides in _the different half_ of array as the upper bound element.
- Therefore, the desired minium element should reside to the **right-hand side** of the pivot element. As a result, we then move the lower bound up next to the pivot index, _i.e._ `low = pivot + 1`. 

>Case 3). `nums[pivot] == nums[high]` 

![pic](../Figures/154/154_case_3_ii.png)

- In this case, we are not sure which side of the pivot that the desired minimum element would reside.
- To further reduce the search scope, a safe measure would be to reduce the upper bound by one (_i.e._ `high = high - 1`), rather than moving _aggressively_ to the pivot point.
- The above strategy would prevent the algorithm from stagnating (_i.e._ endless loop). More importantly, it maintains the **correctness** of the procedure, _i.e._ we would not end up with skipping the desired element.

To summarize, this algorithm differs to the classical binary search algorithm in two parts:

- We use the upper bound of search scope as the reference for the comparison with the pivot element, while in the classical binary search the reference would be the desired value.
<br/>
- When the result of comparison is equal (_i.e._ Case #3), we further move the upper bound, while in the classical binary search normally we would return the value immediately.

Here are some sample implementations based on the above algorithm. _Note:_ the idea is inspired by the post from [sheehan](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array-ii/discuss/48808/My-pretty-simple-code-to-solve-it) in the discussion forum.

<iframe src="https://leetcode.com/playground/SrgT6x5m/shared" frameBorder="0" width="100%" height="412" name="SrgT6x5m"></iframe>

**Complexity Analysis**

* Time complexity: on average $$\mathcal{O}(\log_{2}{N})$$ where $$N$$ is the length of the array, since in general it is a binary search algorithm. However, in the worst case where the array contains identical elements (_i.e._ case #3 `nums[pivot]==nums[high]`), the algorithm would deteriorate to iterating each element, as a result, the time complexity becomes $$\mathcal{O}(N)$$.

* Space complexity : $$\mathcal{O}(1)$$, it's a constant space solution.


**Discussion**

>The problem is a follow-up to the problem of [153. Find Minimum in Rotated Sorted Array](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/). The difference is that in this problem the array can contain duplicates.
_So the question is "Would allow duplicates affect the run-time complexity? How and why?"_

First of all, the problem of [153. Find Minimum in Rotated Sorted Array](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/) can be considered as a specific case of this problem, where it just happens that the array does not contain any duplicate. As a result, the very solutions of this problem would work for the problem of [#153](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/) as well. It is just that we would never come cross the case #3 (_i.e._ `nums[pivot] == nums[high]`) in the problem of [#153](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/).

It is due to the fact that there might exist some duplicates in the array, that we come up the case #3 which eventually render the time complexity of the algorithm to be linear $$\mathcal{O}(N)$$, rather than $$\mathcal{O}(\log_{2}{N})$$.

>One might wonder that whether it works in case #3 if we move the lower boundary (_i.e._ `low += 1`), rather than the upper boundary (_i.e._ `high -= 1`).

The short answer is that it could work for some cases, but not for all. For instance, given the input `[1, 3, 3]`, by moving the lower boundary, we would skip the correct answer.

>While we do `low = pivot + 1` to reduce the search scope, then why not do `high = pivot - 1` instead of `high = pivot`?
Or a similar question would be _"why don't we do check of `low <= high` rather than `low < high`"?_

As a matter of fact, the binary search algorithm has several [forms of implementation](https://en.wikipedia.org/wiki/Binary_search_algorithm), regarding how we set the boundaries and the loop conditions. One can refer to the [Explore card of Binary Search](https://leetcode.com/explore/learn/card/binary-search/) in LeetCode for more details. As simple as the idea of binary search might seem to be, it is tricky to make it work for all cases.

As one would discover from the card, the above implementation of binary search complies with the [template II](https://leetcode.com/explore/learn/card/binary-search/126/template-ii/937/) of binary search. And by replacing `high = pivot` with `high = pivot - 1`, the algorithm will not work.

As subtle as it looks like, the update of the pointers should be consistent with the conditions of the loop. As a rule of thumb, it is advised to stick with one form of binary search, and not to mix them up.

> One might notice that we are calculating the pivot with the formula of `pivot = low + (high-low)/2`, rather than the more intuitive term `pivot = (high+low)/2`.

Actually, this is done intentionally to prevent the numeric overflow issue, since the sum of two integers could exceed the limit of the integer number. As a fun fact, the above mistake prevails in many implementations of binary search, as revealed from a post titled [_"Nearly All Binary Searches and Mergesorts are Broken"_](https://ai.googleblog.com/2006/06/extra-extra-read-all-about-it-nearly.html) from googleblog in 2006.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My pretty simple code to solve it
- Author: sheehan
- Creation Date: Mon Dec 29 2014 09:25:21 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 17:04:33 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        int findMin(vector<int> &num) {
            int lo = 0;
            int hi = num.size() - 1;
            int mid = 0;
            
            while(lo < hi) {
                mid = lo + (hi - lo) / 2;
                
                if (num[mid] > num[hi]) {
                    lo = mid + 1;
                }
                else if (num[mid] < num[hi]) {
                    hi = mid;
                }
                else { // when num[mid] and num[hi] are same
                    hi--;
                }
            }
            return num[lo];
        }
    };

When num[mid] == num[hi], we couldn't sure the position of minimum in mid's left or right, so just let upper bound reduce one.
</p>


### Beats 100% Binary Search with Explanations
- Author: GraceMeng
- Creation Date: Sat Sep 08 2018 14:52:04 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 04:47:30 GMT+0800 (Singapore Standard Time)

<p>
**Logical Thought**
We assert the loop invariant is the index of  the minimum, `min`, is within the range `[lo, hi]`.
1) Before the loop, `min` is in `[0, nums.length - 1]`. To satisfy the invariant, `lo = 0, hi = nums.length - 1`
2) If we guess `mi`, 
if `nums[mi] > nums[hi]`, `min` should be always in `[mi + 1, hi]` (explained in **Essence**). To satisfy the invariant, `lo = mi + 1`;
else if `nums[mi] < nums[lo]`, min should be always in `[lo + 1, mi]` (explained in **Essence**), to satisfy the assertion, `hi = mi, lo = lo + 1`;
else (`nums[lo] <= nums[mi] <= nums[hi]`) min should be always nums[lo].
3) After the loop, lo = hi, min should be in `[lo, lo]`, to satisfy the assertion, `min = lo`.

**Essence**
If we split the array with mi into [lo, mi] and [mi, hi]. If [lo, mi] is not sorted, since we detect [lo, mi] is not sorted by nums[lo] > nums[mi] so nums[lo] cannot be min, `min` must be within `(lo, mi]`. If [mi, hi] is not sorted, `min` must be within `(mi, hi]` - since we detect [mi, hi] is not sorted by nums[mi] > nums[hi], nums[mi] cannot be min. If they are both sorted, `nums[lo]` is the min.
There are 4 kinds of relationship among num[lo], nums[mi], nums[hi]
```
nums[lo] <= nums[mi] <= nums[hi], min is nums[lo]
nums[lo] > nums[mi] <= nums[hi], (lo, mi] is not sorted, min is inside
nums[lo] <= nums[mi] > nums[hi], (mi, hi] is not sorted, min is inside
nums[lo] > nums[mi] > nums[hi], impossible
```
**Code**
```
    public int findMin(int[] nums) {
        
        int lo = 0, hi = nums.length - 1;
        while (lo < hi) {
            int mi = lo + (hi - lo) / 2;
            if (nums[mi] > nums[hi]) { 
                lo = mi + 1;
            }
            else if (nums[mi] < nums[lo]) { 
                hi = mi;
                lo++;
            }
            else { // nums[lo] <= nums[mi] <= nums[hi] 
                hi--;
            }
        }
        
        return nums[lo];
    }
```
I appreciate your VOTE UP (\u25B0\u2579\u25E1\u2579\u25B0)
</p>


### Stop wasting your time. It most likely has to be O(n).
- Author: daddev
- Creation Date: Fri Sep 09 2016 16:08:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 08:32:47 GMT+0800 (Singapore Standard Time)

<p>
There is no faster way than O(n) to solve an input like "1 1 1 1 1 0 1 1 1 1 1 1 1 1".  Binary search won't work in this case as your nums[start] == nums[mid] == nums[end], which half would you discard then?  In other words, you have to examine all elements. With that being said, this is probably the only way to solve it.  Run time: <big>6ms</big>.  Not bad at all.

<pre><code>
class Solution {
public:
    int findMin(vector<int>& nums) {
        int min = nums[0];
        for (int val : nums)
        {
            if (min > val)
            {
                min = val;
            }
        }
        
        return min;
    }
};
</code></pre>
</p>


