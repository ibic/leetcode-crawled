---
title: "Ones and Zeroes"
weight: 450
#id: "ones-and-zeroes"
---
## Description
<div class="description">
<p>Given an array, <code>strs</code>, with strings consisting of only <code>0s</code> and <code>1s</code>. Also two integers <code>m</code> and <code>n</code>.</p>

<p>Now your task is to find the maximum number of strings that you can form with given <b>m</b> <code>0s</code> and <b>n</b> <code>1s</code>. Each <code>0</code> and <code>1</code> can be used at most <b>once</b>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> strs = [&quot;10&quot;,&quot;0001&quot;,&quot;111001&quot;,&quot;1&quot;,&quot;0&quot;], m = 5, n = 3
<strong>Output:</strong> 4
<strong>Explanation:</strong> This are totally 4 strings can be formed by the using of 5 0s and 3 1s, which are &quot;10&quot;,&quot;0001&quot;,&quot;1&quot;,&quot;0&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> strs = [&quot;10&quot;,&quot;0&quot;,&quot;1&quot;], m = 1, n = 1
<strong>Output:</strong> 2
<b>Explanation:</b> You could form &quot;10&quot;, but then you&#39;d have nothing left. Better form &quot;0&quot; and &quot;1&quot;.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= strs.length &lt;= 600</code></li>
	<li><code>1 &lt;= strs[i].length &lt;= 100</code></li>
	<li><code>strs[i]</code> consists only of digits &#39;0&#39; and &#39;1&#39;.</li>
	<li><code>1 &lt;= m, n &lt;= 100</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

In the brute force approach we will consider every subset of $$strs$$ array and count the total number of zeroes and ones in that subset. The subset with zeroes less than equal to $$m$$ and ones less than equal to $$n$$ will be considered as the valid subsets. The maximum length subset among all valid subsets will be  our required subset.

Obviously, there are $$2^n$$ subsets possible for the list of length $$n$$ and here we are using int(32 bits) for iterating every subset. So this method will not work for the list having length greater than 32.

<iframe src="https://leetcode.com/playground/PB4G7F5Z/shared" frameBorder="0" name="PB4G7F5Z" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(2^l*x)$$. $$2^l$$ possible subsets, where $$l$$ is the length of the list $$strs$$ and $$x$$ is the average string length.

* Space complexity : $$O(1)$$. Constant Space required.

---
#### Approach #2 Better Brute Force [Time Limit Exceeded]

**Algorithm**

In the previous approach we were considering every possible subset and then we were counting its zeroes and ones. We can limit the number of subsets by breaking the loop when total number of zeroes exceed $$m$$ or total number of ones exceed $$n$$. This will reduce little computation not the complexity.


<iframe src="https://leetcode.com/playground/XyDP2sRL/shared" frameBorder="0" name="XyDP2sRL" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(2^l*x)$$. $$2^l$$ possible subsets, where $$l$$ is the length of the list $$strs$$ and $$x$$ is the average string length.

* Space complexity : $$O(1)$$. Constant Space required.

---

#### Approach #3 Using Recursion [Time Limit Exceeded]

**Algorithm**

In the above approaches we were considering every subset iteratively. The subset formation can also be done in a recursive manner. For this, we make use of a function `calculate(strs, i, ones, zeroes)`. This function takes the given list of strings $$strs$$ and gives the size of the largest subset with $$ones$$ 1's and $$zeroes$$  0's considering the strings lying after the $$i^{th}$$ index(including itself) in $$strs$$.

Now, in every function call of `calculate(...)`, we can:

1. Include the current string in the subset currently being considered. But if we include the current string, we'll need to deduct the number of 0's and 1's in the current string from the total available respective counts. Thus, we make a function call of the form $$calculate(strs,i+1,zeroes-zeroes_{current\_string},ones-ones_{current\_string})$$. We also need to increment the total number of strings considered so far by 1. We store the result obtained from this call(including the +1) in $$taken$$ variable.

2. Not include the current string in the current subset. In this case, we need not update the count of $$ones$$ and $$zeroes$$. Thus, the new function call takes the form $$calculate(strs,i+1,zeroes,ones)$$. The result obtained from this function call is stored in $$notTaken$$ variable.

The larger value out of $$taken$$ and $$notTaken$$ represents the required result to be returned for the current function call.

Thus, the function call $$calculate(strs, 0, m, n)$$ gives us the required maximum number of subsets possible satisfying the given constraints.



<iframe src="https://leetcode.com/playground/SjPQE6wg/shared" frameBorder="0" name="SjPQE6wg" width="100%" height="428"></iframe>

**Complexity Analysis**
* Time complexity : $$O(2^l*x)$$. $$2^l$$ possible subsets, where $$l$$ is the length of the list $$strs$$ and $$x$$ is the average string length.

* Space complexity : $$O(l)$$. Depth of recursion tree grows upto $$l$$.

---
#### Approach #4 Using Memoization [Accepted]

**Algorithm**

In the recursive approach just discussed, a lot of redundant function calls will be made taking the same values of $$(i, zeroes, ones)$$. This redundancy in the recursive tree can be pruned off by making use of a 3-D memoization array, $$memo$$.

$$memo[i][j][k]$$ is used to store the result obtained for the function call `calculate(strs, i, j, k)`. Or in other words, it stores the maximum number of subsets possible considering the strings starting from the $$i^{th}$$ index onwards, provided only $$j$$ 0's and $$k$$ 1's are available to be used.

Thus, whenever $$memo[i][j][k]$$ already contains a valid entry, we need not make use of the function calls again, but we can pick up the result directly from the $$memo$$ array.

The rest of the procedure remains the same as that of the recursive approach.

<iframe src="https://leetcode.com/playground/oVimjcfS/shared" frameBorder="0" name="oVimjcfS" width="100%" height="515"></iframe>
**Complexity Analysis**

* Time complexity : $$O(l*m*n)$$. $$memo$$ array of size $$l*m*n$$ is filled, where $$l$$ is the length of $$strs$$, $$m$$ and $$n$$ are the number of zeroes and ones respectively.

* Space complexity : $$O(l*m*n)$$. 3D array $$memo$$ is used.

---
#### Approach #5 Dynamic Programming [Accepted]

**Algorithm**

This problem can be solved by using 2-D Dynamic Programming. We can make use of a $$dp$$ array, such that an entry $$dp[i][j]$$ denotes the maximum number of strings that can be included in the subset given only $$i$$ 0's and $$j$$ 1's are available.

Now, let's look at the process by which we'll fill the $$dp$$ array. We traverse the given list of strings one by one. Suppose, at some point, we pick up any string $$s_k$$ consisting of $$x$$ zeroes and $$y$$ ones. Now, choosing to put this string in any of the subset possible by using the previous strings traversed so far will impact the element denoted by $$dp[i][j]$$ for $$i$$ and $$j$$ satisfying $$x &leq; i &leq; m$$, $$y &leq; j &leq; n$$. This is because for entries $$dp[i][j]$$ with $$i < x$$ or $$j < y$$, there won't be sufficient number of 1's and 0's available to accomodate the current string in any subset.

Thus, for every string encountered, we need to appropriately update the $$dp$$ entries within the correct range.

Further, while updating the $$dp$$ values, if we consider choosing the current string to be a part of the subset, the updated result will depend on whether it is profitable to include the current string in any subset or not. If included in the subset, the $$dp[i][j]$$ entry needs to be updated as $$dp[i][j]=1 + dp[i - zeroes_{current\_string}][j - ones_{current\_string}]$$, where the factor of +1 takes into account the number of elements in the current subset being increased due to a new entry.

But, it could be possible that the current string could be so long that it could be profitable not to include it in any of the subsets. Thus, the correct equation for $$dp$$ updation becomes:

$$dp[i][j]= max(1+dp[i - zeroes_{current\_string}][j - ones_{current\_string}],dp[i][j])$$

Thus, after the complete list of strings has been traversed, $$dp[m][n]$$ gives the required size of the largest subset.

Watch this animation for clear understanding:

!?!../Documents/474_Ones_Zeroes.json:1000,563!?!

<iframe src="https://leetcode.com/playground/uSvKvaat/shared" frameBorder="0" name="uSvKvaat" width="100%" height="411"></iframe>

**Complexity Analysis**

* Time complexity : $$O(l*m*n)$$. Three nested loops are their, where $$l$$ is the length of $$strs$$, $$m$$ and $$n$$ are the number of zeroes and ones respectively.


* Space complexity : $$O(m*n)$$. $$dp$$ array of size $$m*n$$ is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### c++ DP solution with comments
- Author: yangluphil
- Creation Date: Sun Dec 11 2016 18:01:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 13:09:39 GMT+0800 (Singapore Standard Time)

<p>
```
int findMaxForm(vector<string>& strs, int m, int n) {
  vector<vector<int>> memo(m+1, vector<int>(n+1, 0));
  int numZeroes, numOnes;

  for (auto &s : strs) {
    numZeroes = numOnes = 0;
    // count number of zeroes and ones in current string
    for (auto c : s) {
      if (c == '0')
	numZeroes++;
      else if (c == '1')
	numOnes++;
    }

    // memo[i][j] = the max number of strings that can be formed with i 0's and j 1's
    // from the first few strings up to the current string s
    // Catch: have to go from bottom right to top left
    // Why? If a cell in the memo is updated(because s is selected),
    // we should be adding 1 to memo[i][j] from the previous iteration (when we were not considering s)
    // If we go from top left to bottom right, we would be using results from this iteration => overcounting
    for (int i = m; i >= numZeroes; i--) {
	for (int j = n; j >= numOnes; j--) {
          memo[i][j] = max(memo[i][j], memo[i - numZeroes][j - numOnes] + 1);
	}
    }
  }
  return memo[m][n];
}
```
</p>


### 0-1 knapsack detailed explanation.
- Author: ZhuEason
- Creation Date: Sat Jan 21 2017 02:30:57 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 03 2019 08:44:04 GMT+0800 (Singapore Standard Time)

<p>
This problem is a typical 0-1 knapsack problem, we need to pick several strings in provided strings to get the maximum number of strings using limited number 0 and 1. We can create a three dimensional array, in which dp[i][j][k] means the maximum number of strings we can get from the first i argument strs using limited j number of \'0\'s and k number of \'1\'s.

For dp[i][j][k], we can get it by fetching the current string i or discarding the current string, which would result in dp[i][j][k] = dp[i-1][j-numOfZero(strs[i])][i-numOfOnes(strs[i])] and dp[i][j][k] = dp[i-1][j][k]; We only need to treat the larger one in it as the largest number for dp[i][j][k].

talking is cheap:

    public int findMaxForm(String[] strs, int m, int n) {
        int l = strs.length;
        int[][][] dp = new int[l+1][m+1][n+1];
        
        for (int i = 0; i < l+1; i++) {
            int[] nums = new int[]{0,0};
            if (i > 0) {
                nums = calculate(strs[i-1]);
            }
            for (int j = 0; j < m+1; j++) {
                for (int k = 0; k < n+1; k++) {
                    if (i == 0) {
                        dp[i][j][k] = 0;
                    } else if (j>=nums[0] && k>=nums[1]) {
                        dp[i][j][k] = Math.max(dp[i-1][j][k], dp[i-1][j-nums[0]][k-nums[1]]+1);
                    } else {
                        dp[i][j][k] = dp[i-1][j][k];
                    }
                }
            }
        }
        
        return dp[l][m][n];
    }
    
    private int[] calculate(String str) {
        int[] res = new int[2];
        Arrays.fill(res, 0);
        
        for (char ch : str.toCharArray()) {
            if (ch == \'0\') {
                res[0]++;
            } else if (ch == \'1\') {
                res[1]++;
            }
        }
        
        return res;
    }


By the way, 0-1 knapsack we cannot decrease the time complexity, but we can decrease the space complexity from i*j*k to j*k

    public int findMaxForm(String[] strs, int m, int n) {
        int l = strs.length;
        int[][] dp = new int[m+1][n+1];
        
        for (int i = 0; i < m+1; i++) {
            Arrays.fill(dp[i], 0);
        }
        
        int[] nums = new int[]{0,0};
        for (String str : strs) {
            nums = calculate(str);
            for (int j = m; j >= nums[0]; j--) {
                for (int k = n; k >= nums[1]; k--) {
                    if (j>=nums[0] && k>=nums[1]) {
                        dp[j][k] = Math.max(dp[j][k], dp[j-nums[0]][k-nums[1]]+1);
                    } else {
                        dp[j][k] = dp[j][k];
                    }
                }
            }
        }
        
        return dp[m][n];
    }
    
    private int[] calculate(String str) {
        int[] res = new int[2];
        Arrays.fill(res, 0);
        
        for (char ch : str.toCharArray()) {
            if (ch == \'0\') {
                res[0]++;
            } else if (ch == \'1\') {
                res[1]++;
            }
        }
        
        return res;
    }

</p>


### Java Iterative DP Solution - O(mn) Space
- Author: compton_scatter
- Creation Date: Sun Dec 11 2016 13:33:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 11 2016 13:33:30 GMT+0800 (Singapore Standard Time)

<p>
Time Complexity: O(kl + kmn), where k is the length of input string array and l is the average length of a string within the array.

```
public int findMaxForm(String[] strs, int m, int n) {
    int[][] dp = new int[m+1][n+1];
    for (String s : strs) {
        int[] count = count(s);
        for (int i=m;i>=count[0];i--) 
            for (int j=n;j>=count[1];j--) 
                dp[i][j] = Math.max(1 + dp[i-count[0]][j-count[1]], dp[i][j]);
    }
    return dp[m][n];
}
    
public int[] count(String str) {
    int[] res = new int[2];
    for (int i=0;i<str.length();i++)
        res[str.charAt(i) - '0']++;
    return res;
 }
```
Thanks @shawngao for some ways to make this solution more concise.
</p>


