---
title: "Interval List Intersections"
weight: 937
#id: "interval-list-intersections"
---
## Description
<div class="description">
<p>Given two lists&nbsp;of <strong>closed</strong> intervals, each list of intervals is pairwise disjoint and in sorted order.</p>

<p>Return the intersection of these two interval lists.</p>

<p><em>(Formally, a closed interval <code>[a, b]</code> (with <code>a &lt;= b</code>) denotes&nbsp;the set of real numbers <code>x</code> with <code>a &lt;= x &lt;= b</code>.&nbsp; The&nbsp;intersection of two closed intervals is a set of real numbers that is either empty, or can be represented as a closed interval.&nbsp; For example, the intersection of [1, 3] and [2, 4] is [2, 3].)</em></p>

<div>
<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/01/30/interval1.png" style="width: 506px; height: 140px;" /></strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[[0,2],[5,10],[13,23],[24,25]]</span>, B = <span id="example-input-1-2">[[1,5],[8,12],[15,24],[25,26]]</span>
<strong>Output: </strong><span id="example-output-1">[[1,2],[5,5],[8,10],[15,23],[24,24],[25,25]]</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= A.length &lt; 1000</code></li>
	<li><code>0 &lt;= B.length &lt; 1000</code></li>
	<li><code>0 &lt;= A[i].start, A[i].end, B[i].start, B[i].end &lt; 10^9</code></li>
</ol>
</div>

</div>

## Tags
- Two Pointers (two-pointers)

## Companies
- Facebook - 21 (taggedByAdmin: false)
- Amazon - 5 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Uber - 8 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- Intuit - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Cruise Automation - 2 (taggedByAdmin: false)
- DoorDash - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Merge Intervals

**Intuition**

In an interval `[a, b]`, call `b` the "endpoint".

Among the given intervals, consider the interval `A[0]` with the smallest endpoint.  (Without loss of generality, this interval occurs in array `A`.)

Then, among the intervals in array `B`, `A[0]` can only intersect one such interval in array `B`.  (If two intervals in `B` intersect `A[0]`, then they both share the endpoint of `A[0]` -- but intervals in `B` are disjoint, which is a contradiction.)

**Algorithm**

If `A[0]` has the smallest endpoint, it can only intersect `B[0]`.  After, we can discard `A[0]` since it cannot intersect anything else.

Similarly, if `B[0]` has the smallest endpoint, it can only intersect `A[0]`, and we can discard `B[0]` after since it cannot intersect anything else.

We use two pointers, `i` and `j`, to virtually manage "discarding" `A[0]` or `B[0]` repeatedly.

<iframe src="https://leetcode.com/playground/fJa25QAM/shared" frameBorder="0" width="100%" height="463" name="fJa25QAM"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(M + N)$$, where $$M, N$$ are the lengths of `A` and `B` respectively.

* Space Complexity:  $$O(M + N)$$, the maximum size of the answer.
<br />
<br />

## Accepted Submission (python3)
```python3
class Solution:
    def intervalIntersection(self, A: List[List[int]], B: List[List[int]]) -> List[List[int]]:
        r = []
        i, j = 0, 0
        while i < len(A) and j < len(B):
            left = max(A[i][0], B[j][0])
            right = min(A[i][1], B[j][1])
            if left <= right:
                r.append([left, right])
            if A[i][1] < B[j][1]:
                i += 1
            else:
                j += 1
        return r
        
```

## Top Discussions
### [Python] Two Pointer Approach + Thinking Process Diagrams
- Author: arkaung
- Creation Date: Sun May 24 2020 00:12:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 24 2020 00:27:10 GMT+0800 (Singapore Standard Time)

<p>
I love this problem. Even though it is a two pointer problem, making everything concise is quite hard!

Let\'s break down this problem into two parts:
**First part** - Finding overlapping range

Let\'s see possible types of overlaps
![image](https://assets.leetcode.com/users/arkaung/image_1590249876.png)

What\'s the condition for two ranges to have an overlapping range? Check out the figures below. The way I think is: if we can have a criss-crossing lock condition satisfied, we have essentially found an overlapping range.
![image](https://assets.leetcode.com/users/arkaung/image_1590249888.png)

After we have made sure that there is an overlapping range, we need to figure out the start and end of the overlapping range. I think of this as trying to squeeze the overlapping range as tight as possible (pushing as far *right* as possible for start and pushing as far *left* as possible for end)
![image](https://assets.leetcode.com/users/arkaung/image_1590249893.png)

**Second part** - Incrementing pointers

The idea behind is to increment the pointer based on the end values of two ranges. Let\'s say the current range in A has end value smaller than to equal to end value of the current range in B, that essentially means that you have exhausted that range in A and you should move on to the next range. Let\'s try to visually think about this. When you are going through the images, keep track of end values of the ranges and how the little pointer triangles progess.
![image](https://assets.leetcode.com/users/arkaung/image_1590249920.png)
![image](https://assets.leetcode.com/users/arkaung/image_1590249927.png)
![image](https://assets.leetcode.com/users/arkaung/image_1590249931.png)

**Python**
``` python
1. class Solution:
2.     def intervalIntersection(self, A: List[List[int]], B: List[List[int]]) -> List[List[int]]:
3.         i = 0
4.         j = 0
5.         
6.         result = []
7.         while i < len(A) and j < len(B):
8.             a_start, a_end = A[i]
9.             b_start, b_end = B[j]
10.             if a_start <= b_end and b_start <= a_end:                       # Criss-cross lock
11.                 result.append([max(a_start, b_start), min(a_end, b_end)])   # Squeezing
12.                 
13.             if a_end <= b_end:         # Exhausted this range in A
14.                 i += 1               # Point to next range in A
15.             else:                      # Exhausted this range in B
16.                 j += 1               # Point to next range in B
17.         return result
```
</p>


### C++ O(n), "merge sort"
- Author: votrubac
- Creation Date: Sun Feb 03 2019 12:03:20 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 10 2020 11:15:36 GMT+0800 (Singapore Standard Time)

<p>
Like in the "merge" phase of the merge sort, we use two pointers to combine sorted sequences. Nothing tricky here - just check all conditions carefully.
```
vector<Interval> intervalIntersection(vector<Interval>& A, vector<Interval>& B) {
  vector<Interval> res;
  for (auto i = 0, j = 0; i < A.size() && j < B.size(); ) {
    if (A[i].end < B[j].start) ++i;
    else if (B[j].end < A[i].start) ++j;
    else {
      res.push_back({ max(A[i].start, B[j].start), min(A[i].end, B[j].end) });
      if (A[i].end < B[j].end) ++i;
      else ++j;
    }
  }
  return res;
}
```
A slightly different version (with the updated method signature), inspired by [MiloLu](https://leetcode.com/milolu/). Could be easier to understand for some folks.
```cpp
vector<vector<int>> intervalIntersection(vector<vector<int>>& A, vector<vector<int>>& B) {
    vector<vector<int>> res;
    for (auto i = 0, j = 0; i < A.size() && j < B.size(); A[i][1] < B[j][1] ? ++i : ++j) {
        auto start = max(A[i][0], B[j][0]);
        auto end = min(A[i][1], B[j][1]);
        if (start <= end) 
            res.push_back({start, end});
    }
    return res;    
}
```
</p>


### Java two pointers O(m + n)
- Author: Self_Learner
- Creation Date: Sun Feb 03 2019 12:05:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 03 2019 12:05:50 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public Interval[] intervalIntersection(Interval[] A, Interval[] B) {
        if (A == null || A.length == 0 || B == null || B.length == 0) {
            return new Interval[] {};
        }
        
        int m = A.length, n = B.length;
        int i = 0, j = 0;
        List<Interval> res = new ArrayList<>();
        while (i < m && j < n) {
            Interval a = A[i];
            Interval b = B[j];

            // find the overlap... if there is any...
            int startMax = Math.max(a.start, b.start);
            int endMin = Math.min(a.end, b.end);
            
            if (endMin >= startMax) {
                res.add(new Interval(startMax, endMin));
            }
            
            //update the pointer with smaller end value...
            if (a.end == endMin) { i++; }
            if (b.end == endMin) { j++; }
        }
        
        //thanks EOAndersson for the concice expression of converting a list to an array
		//thanks Sithis for the explaination of using toArray (new T[0]) intead fo toArray newT[size])
        return res.toArray(new Interval[0]);
    }
}
        
//        int size = res.size();
//        Interval[] list = new Interval[size];
//        for (int k = 0; k < size; k++) {
//            list[k] = res.get(k);
//        }
//        return list;   


</p>


