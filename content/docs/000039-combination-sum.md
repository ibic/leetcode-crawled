---
title: "Combination Sum"
weight: 39
#id: "combination-sum"
---
## Description
<div class="description">
<p>Given an array of <strong>distinct</strong> integers <code>candidates</code> and a target integer <code>target</code>, return <em>a list of all <strong>unique combinations</strong> of </em><code>candidates</code><em> where the chosen numbers sum to </em><code>target</code><em>.</em> You may return the combinations in <strong>any order</strong>.</p>

<p>The <strong>same</strong> number may be chosen from <code>candidates</code> an <strong>unlimited number of times</strong>. Two combinations are unique if the frequency of at least one of the chosen numbers is different.</p>

<p>It is <strong>guaranteed</strong> that the number of unique combinations that sum up to <code>target</code> is less than <code>150</code> combinations for the given input.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> candidates = [2,3,6,7], target = 7
<strong>Output:</strong> [[2,2,3],[7]]
<strong>Explanation:</strong>
2 and 3 are candidates, and 2 + 2 + 3 = 7. Note that 2 can be used multiple times.
7 is a candidate, and 7 = 7.
These are the only two combinations.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> candidates = [2,3,5], target = 8
<strong>Output:</strong> [[2,2,2,2],[2,3,3],[3,5]]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> candidates = [2], target = 1
<strong>Output:</strong> []
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> candidates = [1], target = 1
<strong>Output:</strong> [[1]]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> candidates = [1], target = 2
<strong>Output:</strong> [[1,1]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= candidates.length &lt;= 30</code></li>
	<li><code>1 &lt;= candidates[i] &lt;= 200</code></li>
	<li>All elements of <code>candidates</code> are <strong>distinct</strong>.</li>
	<li><code>1 &lt;= target &lt;= 500</code></li>
</ul>

</div>

## Tags
- Array (array)
- Backtracking (backtracking)

## Companies
- Amazon - 8 (taggedByAdmin: false)
- Facebook - 8 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: true)
- eBay - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Airbnb - 8 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Snapchat - 3 (taggedByAdmin: true)
- Quora - 3 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- JPMorgan - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Square - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution


---
#### Overview

This is one of the problems in the series of combination sum.
They all can be solved with the same algorithm, _i.e._ **backtracking**.

Before tackling this problem, we would recommend one to start with another almost identical problem called [Combination Sum III](https://leetcode.com/problems/combination-sum-iii/), which is arguably easier and one can tweak the solution a bit to solve this problem.

For the sake of this article, we will present the backtracking algorithm.
Furthermore, we will list some other problems on LeetCode that one can solve with the same algorithm presented here.


---
#### Approach 1: Backtracking

**Intuition**

>As a reminder, [backtracking](https://en.wikipedia.org/wiki/Backtracking) is a general algorithm for finding all (or some) solutions to some computational problems. The idea is that it **_incrementally_** builds candidates to the solutions, and abandons a candidate ("backtrack") as soon as it determines that this candidate cannot lead to a final solution.

Specially, to our problem, we could _incrementally_ build the combination, and once we find the current combination is not valid, we _backtrack_ and try another option.

To demonstrate the idea, we showcase how it works with a concrete example in the following graph: 

![exploration tree](../Figures/39/39_exploration_tree.png)

For the given list of candidates `[3, 4, 5]` and a target sum `8`, we start off from empty combination `[]` as indicated as the root node in the above graph.

- Each node represents an action we take at a step, and within the node we also indicate the combination we build sofar.

- From top to down, at each level we descend, we add one more element into the current combination.

- The nodes marked in blue are the ones that could sum up to the target value, _i.e._ they are the desired combination solutions.

- The nodes marked in red are the ones that _exceed_ the target value. Since all the candidates are positive value, there is no way we could bring the sum down to the target value, if we explore further.

- At any instant, we can **only** be at one of the nodes.
When we _backtrack_, we are moving from a node to its parent node.

>An important detail on choosing the next number for the combination is that we select the candidates **in order**, where the total candidates are treated as a list.
Once a candidate is added into the current combination, we will not __look back__ to all the previous candidates in the next explorations.

![zoom in](../Figures/39/39_zoom_in.png)

To demonstrate the idea, let us zoom in a node (as we shown in the above graph) to see how we can choose the next numbers.

- When we are at the node of `[4]`, the precedent candidates are `[3]`, and the candidates followed are `[4, 5]`.

- We don't add the precedent numbers into the current node, since they would have been explored in the nodes in the left part of the subtree, _i.e._ the node of `[3]`.

- Even though we have already the element `4` in the current combination, we are giving the element _another chance_ in the next exploration, since the combination can contain **duplicate** numbers.

- As a result, we would branch out in two directions, by adding the element `4` and `5` respectively into the current combination.

**Algorithm**

As one can see, the above backtracking algorithm is unfolded as a _DFS_ (Depth-First Search) tree traversal, which is often implemented with recursion.

Here we define a recursive function of `backtrack(remain, comb, start)` (in Python), which populates the combinations, starting from the current combination (`comb`), the remaining sum to fulfill (`remain`) and the current cursor (`start`) to the list of candidates.
Note that, the signature of the recursive function is slightly different in Java. But the idea remains the same.

- For the first base case of the recursive function, if the `remain==0`, _i.e._ we fulfill the desired target sum, therefore we can add the current combination to the final list.

- As another base case, if `remain < 0`, _i.e._ we exceed the target value, we will cease the exploration here.

- Other than the above two base cases, we would then continue to explore the sublist of candidates as `[start ... n]`.
For each of the candidate, we invoke the recursive function itself with updated parameters.

    - Specifically, we add the current candidate into the combination.

    - With the added candidate, we now have less sum to fulfill, _i.e._ `remain - candidate`.

    - For the next exploration, still we start from the current cursor `start`.

    - At the end of each exploration, we __*backtrack*__ by popping out the candidate out of the combination.


<iframe src="https://leetcode.com/playground/WDJNupkM/shared" frameBorder="0" width="100%" height="500" name="WDJNupkM"></iframe>


**Complexity Analysis**

Let $$N$$ be the number of candidates, $$T$$ be the target value, and $$M$$ be the minimal value among the candidates.

- Time Complexity: $$\mathcal{O}(N^{\frac{T}{M}+1})$$ 

    - As we illustrated before, the execution of the backtracking is unfolded as a DFS traversal in a n-ary tree.
    The total number of steps during the backtracking would be the number of nodes in the tree.

    - At each node, it takes a constant time to process, except the leaf nodes which could take a linear time to make a copy of combination. So we can say that the time complexity is linear to the number of nodes of the execution tree.

    - Here we provide a _loose_ upper bound on the number of nodes.

        - First of all, the fan-out of each node would be bounded to $$N$$, _i.e._ the total number of candidates.

        - The maximal depth of the tree, would be $$\frac{T}{M}$$, where we keep on adding the smallest element to the combination.

        - As we know, the maximal number of nodes in N-ary tree of $$\frac{T}{M}$$ height would be $$N^{\frac{T}{M}+1}$$.

    - **Note that**, the actual number of nodes in the execution tree would be much smaller than the upper bound, since the fan-out of the nodes are decreasing level by level.

- Space Complexity: $$\mathcal{O}(\frac{T}{M})$$

    - We implement the algorithm in recursion, which consumes some additional memory in the function call stack.

    - The number of recursive calls can pile up to $$\frac{T}{M}$$, where we keep on adding the smallest element to the combination.
    As a result, the space overhead of the recursion is $$\mathcal{O}(\frac{T}{M})$$.

    - In addition, we keep a combination of numbers during the execution, which requires at most $$\mathcal{O}(\frac{T}{M})$$ space as well.

    - To sum up, the total space complexity of the algorithm would be $$\mathcal{O}(\frac{T}{M})$$.

    - Note that, we did not take into the account the space used to hold the final results for the space complexity.


---
#### Similar Problems

Once one figures out how it works with the backtracking algorithm for this problem, one can go ahead and apply this _"hammer"_ to solve a series of similar problems.

For instance, if one goes back to the problem of [Combination Sum III](https://leetcode.com/problems/combination-sum-iii/), if suffices to tweak a bit on the invocation of the recursive function to solve the problem, in addition to some other minor adjustments on the base cases.

More specifically, in this problem, we give the current candidate another chance in the further explorations, while for the problem of [Combination Sum III](https://leetcode.com/problems/combination-sum-iii/) we simply **move on** to the candidates followed.

Here are a series of problems that one can solve, with some tweaks of the backtracking algorithm presented in this article, 
thanks to the great list compiled by [issac3](https://leetcode.com/problems/combination-sum/discuss/16502/A-general-approach-to-backtracking-questions-in-Java-(Subsets-Permutations-Combination-Sum-Palindrome-Partitioning)) in the discussion forum.

- [Subsets](https://leetcode.com/problems/subsets)
- [Subsets II](https://leetcode.com/problems/subsets-ii)
- [Permutations](https://leetcode.com/problems/permutations/)
- [Permutations II](https://leetcode.com/problems/permutations-ii/)
- [Combinations](https://leetcode.com/problems/combinations/)
- [Combination Sum II](https://leetcode.com/problems/combination-sum-ii/)
- [Combination Sum III](https://leetcode.com/problems/combination-sum-iii/)
- [Palindrome Partition](https://leetcode.com/problems/palindrome-partitioning/)


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A general approach to backtracking questions in Java (Subsets, Permutations, Combination Sum, Palindrome Partitioning)
- Author: issac3
- Creation Date: Mon May 23 2016 23:55:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 05:05:04 GMT+0800 (Singapore Standard Time)

<p>
This structure might apply to many other backtracking questions, but here I am just going to demonstrate Subsets, Permutations, and Combination Sum.

Subsets : [https://leetcode.com/problems/subsets/][1]

    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, 0);
        return list;
    }
    
    private void backtrack(List<List<Integer>> list , List<Integer> tempList, int [] nums, int start){
        list.add(new ArrayList<>(tempList));
        for(int i = start; i < nums.length; i++){
            tempList.add(nums[i]);
            backtrack(list, tempList, nums, i + 1);
            tempList.remove(tempList.size() - 1);
        }
    }


Subsets II (contains duplicates) : [https://leetcode.com/problems/subsets-ii/][2]

    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, 0);
        return list;
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums, int start){
        list.add(new ArrayList<>(tempList));
        for(int i = start; i < nums.length; i++){
            if(i > start && nums[i] == nums[i-1]) continue; // skip duplicates
            tempList.add(nums[i]);
            backtrack(list, tempList, nums, i + 1);
            tempList.remove(tempList.size() - 1);
        }
    } 


----------

Permutations : [https://leetcode.com/problems/permutations/][3]

    public List<List<Integer>> permute(int[] nums) {
       List<List<Integer>> list = new ArrayList<>();
       // Arrays.sort(nums); // not necessary
       backtrack(list, new ArrayList<>(), nums);
       return list;
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums){
       if(tempList.size() == nums.length){
          list.add(new ArrayList<>(tempList));
       } else{
          for(int i = 0; i < nums.length; i++){ 
             if(tempList.contains(nums[i])) continue; // element already exists, skip
             tempList.add(nums[i]);
             backtrack(list, tempList, nums);
             tempList.remove(tempList.size() - 1);
          }
       }
    } 

Permutations II (contains duplicates) : [https://leetcode.com/problems/permutations-ii/][4]

    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, new boolean[nums.length]);
        return list;
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums, boolean [] used){
        if(tempList.size() == nums.length){
            list.add(new ArrayList<>(tempList));
        } else{
            for(int i = 0; i < nums.length; i++){
                if(used[i] || i > 0 && nums[i] == nums[i-1] && !used[i - 1]) continue;
                used[i] = true; 
                tempList.add(nums[i]);
                backtrack(list, tempList, nums, used);
                used[i] = false; 
                tempList.remove(tempList.size() - 1);
            }
        }
    }


----------

Combination Sum : [https://leetcode.com/problems/combination-sum/][5]

    public List<List<Integer>> combinationSum(int[] nums, int target) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, target, 0);
        return list;
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums, int remain, int start){
        if(remain < 0) return;
        else if(remain == 0) list.add(new ArrayList<>(tempList));
        else{ 
            for(int i = start; i < nums.length; i++){
                tempList.add(nums[i]);
                backtrack(list, tempList, nums, remain - nums[i], i); // not i + 1 because we can reuse same elements
                tempList.remove(tempList.size() - 1);
            }
        }
    }

Combination Sum II (can't reuse same element) : [https://leetcode.com/problems/combination-sum-ii/][6]

    public List<List<Integer>> combinationSum2(int[] nums, int target) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, target, 0);
        return list;
        
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums, int remain, int start){
        if(remain < 0) return;
        else if(remain == 0) list.add(new ArrayList<>(tempList));
        else{
            for(int i = start; i < nums.length; i++){
                if(i > start && nums[i] == nums[i-1]) continue; // skip duplicates
                tempList.add(nums[i]);
                backtrack(list, tempList, nums, remain - nums[i], i + 1);
                tempList.remove(tempList.size() - 1); 
            }
        }
    } 


Palindrome Partitioning : [https://leetcode.com/problems/palindrome-partitioning/][7]

    public List<List<String>> partition(String s) {
       List<List<String>> list = new ArrayList<>();
       backtrack(list, new ArrayList<>(), s, 0);
       return list;
    }
    
    public void backtrack(List<List<String>> list, List<String> tempList, String s, int start){
       if(start == s.length())
          list.add(new ArrayList<>(tempList));
       else{
          for(int i = start; i < s.length(); i++){
             if(isPalindrome(s, start, i)){
                tempList.add(s.substring(start, i + 1));
                backtrack(list, tempList, s, i + 1);
                tempList.remove(tempList.size() - 1);
             }
          }
       }
    }
    
    public boolean isPalindrome(String s, int low, int high){
       while(low < high)
          if(s.charAt(low++) != s.charAt(high--)) return false;
       return true;
    } 
 


  [1]: https://leetcode.com/problems/subsets/
  [2]: https://leetcode.com/problems/subsets-ii/
  [3]: https://leetcode.com/problems/permutations/
  [4]: https://leetcode.com/problems/permutations-ii/
  [5]: https://leetcode.com/problems/combination-sum/
  [6]: https://leetcode.com/problems/combination-sum-ii/
  [7]: https://leetcode.com/problems/palindrome-partitioning/
</p>


### Accepted 16ms c++ solution use backtracking, easy understand.
- Author: prime_tang
- Creation Date: Sun May 24 2015 17:19:05 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 14:07:46 GMT+0800 (Singapore Standard Time)

<p>
Accepted 16ms c++ solution use backtracking for [Combination Sum][1]:

    class Solution {
    public:
        std::vector<std::vector<int> > combinationSum(std::vector<int> &candidates, int target) {
            std::sort(candidates.begin(), candidates.end());
            std::vector<std::vector<int> > res;
            std::vector<int> combination;
            combinationSum(candidates, target, res, combination, 0);
            return res;
        }
    private:
        void combinationSum(std::vector<int> &candidates, int target, std::vector<std::vector<int> > &res, std::vector<int> &combination, int begin) {
            if (!target) {
                res.push_back(combination);
                return;
            }
            for (int i = begin; i != candidates.size() && target >= candidates[i]; ++i) {
                combination.push_back(candidates[i]);
                combinationSum(candidates, target - candidates[i], res, combination, i);
                combination.pop_back();
            }
        }
    };

Accepted 12ms c++ solution use backtracking for [Combination Sum II][2]:

    class Solution {
    public:
        std::vector<std::vector<int> > combinationSum2(std::vector<int> &candidates, int target) {
            std::sort(candidates.begin(), candidates.end());
            std::vector<std::vector<int> > res;
            std::vector<int> combination;
            combinationSum2(candidates, target, res, combination, 0);
            return res;
        }
    private:
        void combinationSum2(std::vector<int> &candidates, int target, std::vector<std::vector<int> > &res, std::vector<int> &combination, int begin) {
            if (!target) {
                res.push_back(combination);
                return;
            }
            for (int i = begin; i != candidates.size() && target >= candidates[i]; ++i)
                if (i == begin || candidates[i] != candidates[i - 1]) {
                    combination.push_back(candidates[i]);
                    combinationSum2(candidates, target - candidates[i], res, combination, i + 1);
                    combination.pop_back();
                }
        }
    };

Accepted 0ms c++ solution use backtracking for [Combination Sum III][3]:

    class Solution {
    public:
        std::vector<std::vector<int> > combinationSum3(int k, int n) {
            std::vector<std::vector<int> > res;
            std::vector<int> combination;
            combinationSum3(n, res, combination, 1, k);
            return res;
        }
    private:
        void combinationSum3(int target, std::vector<std::vector<int> > &res, std::vector<int> &combination, int begin, int need) {
            if (!target) {
                res.push_back(combination);
                return;
            }
            else if (!need)
                return;
            for (int i = begin; i != 10 && target >= i * need + need * (need - 1) / 2; ++i) {
                combination.push_back(i);
                combinationSum3(target - i, res, combination, i + 1, need - 1);
                combination.pop_back();
            }
        }
    };


  [1]: https://leetcode.com/problems/combination-sum/
  [2]: https://leetcode.com/problems/combination-sum-ii/
  [3]: https://leetcode.com/problems/combination-sum-iii/
</p>


### Python dfs solution.
- Author: OldCodingFarmer
- Creation Date: Wed Sep 02 2015 07:14:56 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 10 2020 22:35:48 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def combinationSum(self, candidates, target):
        ret = []
        self.dfs(candidates, target, [], ret)
        return ret
    
    def dfs(self, nums, target, path, ret):
        if target < 0:
            return 
        if target == 0:
            ret.append(path)
            return 
        for i in range(len(nums)):
            self.dfs(nums[i:], target-nums[i], path+[nums[i]], ret)
```
</p>


