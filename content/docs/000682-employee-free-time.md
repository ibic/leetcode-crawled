---
title: "Employee Free Time"
weight: 682
#id: "employee-free-time"
---
## Description
<div class="description">
<p>We are given a list <code>schedule</code> of employees, which represents the working time for each employee.</p>

<p>Each employee has a list of non-overlapping <code>Intervals</code>, and these intervals are in sorted order.</p>

<p>Return the list of finite intervals representing <b>common, positive-length free time</b> for <i>all</i> employees, also in sorted order.</p>

<p>(Even though we are representing <code>Intervals</code> in the form <code>[x, y]</code>, the objects inside are <code>Intervals</code>, not lists or arrays. For example, <code>schedule[0][0].start = 1</code>, <code>schedule[0][0].end = 2</code>, and <code>schedule[0][0][0]</code> is not defined).&nbsp; Also, we wouldn&#39;t include intervals like [5, 5] in our answer, as they have zero length.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> schedule = [[[1,2],[5,6]],[[1,3]],[[4,10]]]
<strong>Output:</strong> [[3,4]]
<strong>Explanation:</strong> There are a total of three employees, and all common
free time intervals would be [-inf, 1], [3, 4], [10, inf].
We discard any intervals that contain inf as they aren&#39;t finite.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> schedule = [[[1,3],[6,7]],[[2,4]],[[2,5],[9,12]]]
<strong>Output:</strong> [[5,6],[7,9]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= schedule.length , schedule[i].length &lt;= 50</code></li>
	<li><code>0 &lt;= schedule[i].start &lt; schedule[i].end &lt;= 10^8</code></li>
</ul>

</div>

## Tags
- Heap (heap)
- Greedy (greedy)

## Companies
- Pinterest - 5 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- DoorDash - 2 (taggedByAdmin: false)
- Airbnb - 3 (taggedByAdmin: true)
- Intuit - 3 (taggedByAdmin: false)
- Wayfair - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Coupang - 2 (taggedByAdmin: false)
- Postmates - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Events (Line Sweep) [Accepted]

**Intuition**

If some interval overlaps *any* interval (for any employee), then it won't be included in the answer.  So we could reduce our problem to the following: given a set of intervals, find all places where there are no intervals.

To do this, we can use an "events" approach present in other interval problems.  For each interval `[s, e]`, we can think of this as two events: `balance++` when `time = s`, and `balance--` when `time = e`.  We want to know the regions where `balance == 0`.

**Algorithm**

For each interval, create two events as described above, and sort the events.  Now for each event occuring at time `t`, if the `balance` is `0`, then the preceding segment `[prev, t]` did not have any intervals present, where `prev` is the previous value of `t`.

<iframe src="https://leetcode.com/playground/D4AH3ZsR/shared" frameBorder="0" width="100%" height="497" name="D4AH3ZsR"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(C\log C)$$, where $$C$$ is the number of intervals across all employees.

* Space Complexity: $$O(C)$$.

---
#### Approach #2: Priority Queue [Accepted]

**Intuition**

Say we are at some time where no employee is working.  That work-free period will last until the next time some employee has to work.

So let's maintain a heap of the next time an employee has to work, and it's associated job.  When we process the next time from the heap, we can add the next job for that employee.

**Algorithm**

Keep track of the latest time `anchor` that we don't know of a job overlapping that time.

When we process the earliest occurring job not yet processed, it occurs at time `t`, by employee `e_id`, and it was that employee's `e_jx`'th job.  If `anchor < t`, then there was a free interval `Interval(anchor, t)`.

<iframe src="https://leetcode.com/playground/D5CT9WQ8/shared" frameBorder="0" width="100%" height="500" name="D5CT9WQ8"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(C\log N)$$, where $$N$$ is the number of employees, and $$C$$ is the number of jobs across all employees.  The maximum size of the heap is $$N$$, so each push and pop operation is $$O(\log N)$$, and there are $$O(C)$$ such operations.

* Space Complexity: $$O(N)$$ in additional space complexity.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java Sort Solution - Using (Priority Queue) or Just ArrayList
- Author: diddit
- Creation Date: Sun Jan 07 2018 12:01:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 09:48:36 GMT+0800 (Singapore Standard Time)

<p>
The idea  is to just add all the intervals to the priority queue. (NOTE that it is not matter how many different people are there for the algorithm. becuase we just need to find a gap in the time line.

1. priority queue - sorted by start time, and for same start time sort by either largest end time or smallest (it is not matter).
2. Everytime you poll from priority queue, just make sure it doesn't intersect with previous interval. 
This mean that there is no coomon interval. Everyone is free time.

```java
    public List<Interval> employeeFreeTime(List<List<Interval>> avails) {

        List<Interval> result = new ArrayList<>();

        PriorityQueue<Interval> pq = new PriorityQueue<>((a, b) -> a.start - b.start);
        avails.forEach(e -> pq.addAll(e));

        Interval temp = pq.poll();
        while(!pq.isEmpty()) {
            if(temp.end < pq.peek().start) { // no intersect
                result.add(new Interval(temp.end, pq.peek().start));
                temp = pq.poll(); // becomes the next temp interval
            }else { // intersect or sub merged
                temp = temp.end < pq.peek().end ? pq.peek() : temp;
                pq.poll();
            }
        }
        return result;
    }
```

@Majcpatrick ask why priority queue. I think it's not needed. Below is just array list.

```java
    public List<Interval> employeeFreeTime(List<List<Interval>> avails) {
        List<Interval> result = new ArrayList<>();
        List<Interval> timeLine = new ArrayList<>();
        avails.forEach(e -> timeLine.addAll(e));
        Collections.sort(timeLine, ((a, b) -> a.start - b.start));

        Interval temp = timeLine.get(0);
        for(Interval each : timeLine) {
            if(temp.end < each.start) {
                result.add(new Interval(temp.end, each.start));
                temp = each;
            }else{
                temp = temp.end < each.end ? each : temp;
            }
        }
        return result;
    }
```
</p>


### Java PriorityQueue Solution, Time complexity O(N log K)
- Author: Aoi-silent
- Creation Date: Mon Nov 19 2018 06:05:45 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 19 2018 06:05:45 GMT+0800 (Singapore Standard Time)

<p>
N is the total number of intervals in all the lists. K is the number of people.
```
class Solution {
    public List<Interval> employeeFreeTime(List<List<Interval>> schedule) {
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> schedule.get(a[0]).get(a[1]).start - schedule.get(b[0]).get(b[1]).start);
        for (int i = 0; i < schedule.size(); i++) {
            pq.add(new int[] {i, 0});
        }
        List<Interval> res = new ArrayList<>();
        int prev = schedule.get(pq.peek()[0]).get(pq.peek()[1]).start;
        while (!pq.isEmpty()) {
            int[] index = pq.poll();
            Interval interval = schedule.get(index[0]).get(index[1]);
            if (interval.start > prev) {
                res.add(new Interval(prev, interval.start));
            }
            prev = Math.max(prev, interval.end);
            if (schedule.get(index[0]).size() > index[1] + 1) {
                pq.add(new int[] {index[0], index[1] + 1});
            }
        }
        return res;
    }
}
```
</p>


### Simple Python 9 liner beats 97% (with explanation)
- Author: Suralin
- Creation Date: Sun Sep 16 2018 01:30:48 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 27 2018 00:56:01 GMT+0800 (Singapore Standard Time)

<p>
Key points:
1) recognize that this is very similar to merging intervals (https://leetcode.com/problems/merge-intervals/description/)
2) it doesn\'t matter which employee an interval belongs to, so just flatten
3) can build result array while merging, don\'t have to do afterward (and don\'t need full merged arr)

```
def employeeFreeTime(self, schedule):
    ints = sorted([i for s in schedule for i in s], key=lambda x: x.start)
    res, pre = [], ints[0]
    for i in ints[1:]:
        if i.start <= pre.end and i.end > pre.end:
            pre.end = i.end
        elif i.start > pre.end:
            res.append(Interval(pre.end, i.start))
            pre = i
    return res
```
</p>


