---
title: "Count Servers that Communicate"
weight: 1206
#id: "count-servers-that-communicate"
---
## Description
<div class="description">
<p>You are given a map of a server center, represented as a <code>m * n</code> integer matrix&nbsp;<code>grid</code>, where 1 means that on that cell there is a server and 0 means that it is no server. Two servers are said to communicate if they are on the same row or on the same column.<br />
<br />
Return the number of servers&nbsp;that communicate with any other server.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/11/14/untitled-diagram-6.jpg" style="width: 202px; height: 203px;" /></p>

<pre>
<strong>Input:</strong> grid = [[1,0],[0,1]]
<strong>Output:</strong> 0
<b>Explanation:</b>&nbsp;No servers can communicate with others.</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/11/13/untitled-diagram-4.jpg" style="width: 203px; height: 203px;" /></strong></p>

<pre>
<strong>Input:</strong> grid = [[1,0],[1,1]]
<strong>Output:</strong> 3
<b>Explanation:</b>&nbsp;All three servers can communicate with at least one other server.
</pre>

<p><strong>Example 3:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/11/14/untitled-diagram-1-3.jpg" style="width: 443px; height: 443px;" /></p>

<pre>
<strong>Input:</strong> grid = [[1,1,0,0],[0,0,1,0],[0,0,1,0],[0,0,0,1]]
<strong>Output:</strong> 4
<b>Explanation:</b>&nbsp;The two servers in the first row can communicate with each other. The two servers in the third column can communicate with each other. The server at right bottom corner can&#39;t communicate with any other server.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m == grid.length</code></li>
	<li><code>n == grid[i].length</code></li>
	<li><code>1 &lt;= m &lt;= 250</code></li>
	<li><code>1 &lt;= n &lt;= 250</code></li>
	<li><code>grid[i][j] == 0 or 1</code></li>
</ul>

</div>

## Tags
- Array (array)
- Graph (graph)

## Companies
- Google - 2 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Simple Preprocessing
- Author: PhoenixDD
- Creation Date: Sun Nov 24 2019 12:01:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 25 2019 04:40:35 GMT+0800 (Singapore Standard Time)

<p>
**Observation**

For each server, the row or the column must have another server except the current one.
We can simply keep a count of servers in each row and column and use this information to get the result while traversing the grid.

**Solution**
```c++
class Solution {
public:
    int countServers(vector<vector<int>>& grid) 
    {
        vector<int> rows(grid.size(),0),columns(grid[0].size(),0);	//Stores count of servers in rows and colums
        for(int i=0;i<grid.size();i++)						//Fill the count vectors
            for(int j=0;j<grid[i].size();j++)
                if(grid[i][j])
                    rows[i]++,columns[j]++;
        int result=0;
        for(int i=0;i<grid.size();i++)			//Traverse the grid to get result count
            for(int j=0;j<grid[i].size();j++)
                if(grid[i][j]&&(columns[j]>1||rows[i]>1))	//Check if there are any other server except the current one in it\'s corresponding row or column.
                    result++;
        return result;
    }
};
```
**Complexity**
Here `m` is the number of rows and `n` is the number of columns.
Space: `O(m+n).`This can be reduced to `min(m,n).`
Time: `O(m*n).`
</p>


### Java | Clean And Simple | Beats 100 %
- Author: ping_pong
- Creation Date: Sun Nov 24 2019 12:06:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 24 2019 12:06:30 GMT+0800 (Singapore Standard Time)

<p>
```
public int countServers(int[][] grid) {
    if (grid == null || grid.length == 0 || grid[0].length == 0) return 0;
    int numRows = grid.length;
    int numCols = grid[0].length;
    int rowCount[] = new int[numRows];
    int colCount[] = new int[numCols];
    int totalServers = 0;
    for (int row = 0; row < numRows; row++) {
        for (int col = 0; col < numCols; col++) {
            if (grid[row][col] == 1) {
                rowCount[row]++;
                colCount[col]++;
                totalServers++;
            }
        }
    }
    for (int row = 0; row < numRows; row++) {
        for (int col = 0; col < numCols; col++) {
            if (grid[row][col] == 1) {
                if (rowCount[row] == 1 && colCount[col] == 1) {
                    totalServers--;
                }
            }
        }
    }
    return totalServers;
}
```
</p>


### [Python] Simple and Concise
- Author: lee215
- Creation Date: Mon Nov 25 2019 00:42:39 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 25 2019 00:42:39 GMT+0800 (Singapore Standard Time)

<p>
Count the number of servers in each row to `X`
Count the number of servers in each col to `Y`
If `X[i] + Y[j] > 2`, the server at `A[i][j]` (if exists) communicate.

**Python:**
```python
    def countServers(self, A):
        X, Y = map(sum, A), map(sum, zip(*A))
        return sum(X[i] + Y[j] > 2 for i in xrange(len(A)) for j in xrange(len(A[0])) if A[i][j])
```

</p>


