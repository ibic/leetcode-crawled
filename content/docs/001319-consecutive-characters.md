---
title: "Consecutive Characters"
weight: 1319
#id: "consecutive-characters"
---
## Description
<div class="description">
<p>Given a string <code>s</code>, the power of the string is the maximum length of a non-empty substring that&nbsp;contains only one unique character.</p>

<p>Return <em>the power</em>&nbsp;of the string.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;leetcode&quot;
<strong>Output:</strong> 2
<strong>Explanation:</strong> The substring &quot;ee&quot; is of length 2 with the character &#39;e&#39; only.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abbcccddddeeeeedcba&quot;
<strong>Output:</strong> 5
<strong>Explanation:</strong> The substring &quot;eeeee&quot; is of length 5 with the character &#39;e&#39; only.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;triplepillooooow&quot;
<strong>Output:</strong> 5
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;hooraaaaaaaaaaay&quot;
<strong>Output:</strong> 11
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;tourist&quot;
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 500</code></li>
	<li><code>s</code> contains only lowercase English letters.</li>
</ul>
</div>

## Tags
- String (string)

## Companies


## Official Solution
[TOC]

## Solution

---

### Overview

This problem is very similar to [674. Longest Continuous Increasing Subsequence](https://leetcode.com/problems/longest-continuous-increasing-subsequence/), and the only difference is that we need a substring with the same characters instead of an increasing one. Therefore, similar methods can be applied. Below, a similar and simple approach is introduced.

---

### Approach #1: One Pass

**Intuition and Algorithm**

Recall the problem, we need to find "the maximum length of a non-empty substring that contains only one unique character".

In other words, we need to find the Longest Substring with **the same characters**.

We can iterate over the given string, and use a variable `count` to record the length of that substring.

When the next character is the same as the previous one, we increase `count` by one. Else, we reset `count` to 1.

With this method, when reaching the end of a substring with the same characters, `count` will be the length of that substring, since we reset the `count` when that substring starts, and increase `count` when iterate that substring.

Therefore, the maximum value of `count` is what we need. Another variable is needed to store the maximum while iterating.


<iframe src="https://leetcode.com/playground/Ua6cWMdS/shared" frameBorder="0" width="100%" height="395" name="Ua6cWMdS"></iframe>

**Complexity Analysis**

Let $$N$$ be the length of `s`.

* Time Complexity: $$O(N)$$, since we perform one loop through `s`.

* Space Complexity: $$O(1)$$, since we only have two integer variables `count` and `max_count`(`maxCount`), and one character variable `previous`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Simple code w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun May 17 2020 00:04:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 00:59:16 GMT+0800 (Singapore Standard Time)

<p>
1. Increase the counter by 1 if current char same as the previous one; otherwise, reset the counter to 1;
2. Update the max value of the counter during each iteration.

```java
    public int maxPower(String s) {
        int ans = 1;
        for (int i = 1, cnt = 1; i < s.length(); ++i) {
            if (s.charAt(i) == s.charAt(i - 1))
                cnt++;
            else 
                cnt = 1;
            ans = Math.max(ans, cnt);
        }
        return ans;        
    }
```
```python
    def maxPower(self, s: str) -> int:
        ans = cnt = 1
        for i in range(1, len(s)):
            if s[i] == s[i - 1]:
                cnt += 1
            else:
                cnt = 1
            ans = max(ans,cnt)        
        return ans
```

**Analysis:**
Time: O(n), Space: O(1), where n = s.length().
</p>


### [Python] One line
- Author: lee215
- Creation Date: Sun May 17 2020 00:02:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 00:02:57 GMT+0800 (Singapore Standard Time)

<p>

**Python:**
```py
    def maxPower(self, s):
        return max(len(list(b)) for a, b in itertools.groupby(s))
```

</p>


### easy faster than 90% cpp solution
- Author: jinkim
- Creation Date: Sat Jun 27 2020 06:06:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 27 2020 06:06:57 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    int maxPower(string s) {
        size_t max = 1;
        size_t curr = 1;
        for (size_t i = 1; i < s.size(); i++) {
            if (s[i - 1] == s[i]) curr++;
            else {
                if (curr > max) max = curr;
                curr = 1;
            }
        }
        if (curr > max) max = curr; // edge case
        return max;
    }
};
```
</p>


