---
title: "Make Array Strictly Increasing"
weight: 1154
#id: "make-array-strictly-increasing"
---
## Description
<div class="description">
<p>Given two integer arrays&nbsp;<code>arr1</code> and <code>arr2</code>, return the minimum number of operations (possibly zero) needed&nbsp;to make <code>arr1</code> strictly increasing.</p>

<p>In one operation, you can choose two indices&nbsp;<code>0 &lt;=&nbsp;i &lt; arr1.length</code>&nbsp;and&nbsp;<code>0 &lt;= j &lt; arr2.length</code>&nbsp;and do the assignment&nbsp;<code>arr1[i] = arr2[j]</code>.</p>

<p>If there is no way to make&nbsp;<code>arr1</code>&nbsp;strictly increasing,&nbsp;return&nbsp;<code>-1</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr1 = [1,5,3,6,7], arr2 = [1,3,2,4]
<strong>Output:</strong> 1
<strong>Explanation:</strong> Replace <code>5</code> with <code>2</code>, then <code>arr1 = [1, 2, 3, 6, 7]</code>.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr1 = [1,5,3,6,7], arr2 = [4,3,1]
<strong>Output:</strong> 2
<strong>Explanation:</strong> Replace <code>5</code> with <code>3</code> and then replace <code>3</code> with <code>4</code>. <code>arr1 = [1, 3, 4, 6, 7]</code>.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr1 = [1,5,3,6,7], arr2 = [1,6,3,3]
<strong>Output:</strong> -1
<strong>Explanation:</strong> You can&#39;t make <code>arr1</code> strictly increasing.</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr1.length, arr2.length &lt;= 2000</code></li>
	<li><code>0 &lt;= arr1[i], arr2[i] &lt;= 10^9</code></li>
</ul>

<p>&nbsp;</p>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python DP solution with explanation.
- Author: davyjing
- Creation Date: Sun Sep 08 2019 12:09:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 12:43:02 GMT+0800 (Singapore Standard Time)

<p>
Similar to Longest-increasing-sub-sequence problem.
Record the possible states of each position and number of operations to get this state.
When we check i-th element in the arr1,  ```dp``` record the possible values we can place at this position, and the number of operations to get to this state. 
Now, we need to build ```dp``` for (i+1)-th position, so for (i+1)-th element,
if it\'s larger than the possible state from i-th state, we have two choices: 
	    1, keep it so no operation needs to be made.
	    2, choose from arr2 a smaller element that is larger than i-th element and add one operation. 
If it\'s not larger than the i-th state, we definitely need to make a possible operation.
```
class Solution:
    def makeArrayIncreasing(self, arr1: List[int], arr2: List[int]) -> int:
        dp = {-1:0}
        arr2.sort()
        for i in arr1:
            tmp = collections.defaultdict(lambda: float(\'inf\'))
            for key in dp:
                if i > key:
                    tmp[i] = min(tmp[i],dp[key])
                loc = bisect.bisect_right(arr2,key)
                if loc < len(arr2):
                    tmp[arr2[loc]] = min(tmp[arr2[loc]],dp[key]+1)
            dp = tmp
        if dp:
            return min(dp.values())
        return -1
```
</p>


### Java dp solution : A simple change from Longest Increasing Subsequence
- Author: x372p
- Creation Date: Sun Sep 08 2019 12:53:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 13:01:16 GMT+0800 (Singapore Standard Time)

<p>
This problem is similiar to the Longest Increasing Subsequence (LIS) problem  (LC300). The only difference is that, while we are not only calculating the longest increasing subsequence, we also need to make sure that, base on this sequence, we can form a new arr1 from swapping with arr2, to be a complete increasing array.

So to start, the dp relationship in LIS problem is:
```
//dp[i] -> longest increasing subsequence length ending at arr[i]
if (arr1[j] < arr1[i])
	dp[i] = Math.max(dp[i], dp[j] + 1)
```
We can start from this relationship, just add a simple check function: the function checks if arr1[j] to arr1[i] can be swapped from arr2, to make a complete increasing array, ending at arr1[i].
So now the relationship becomes:
```
//dp[i] -> the answer we need (nubmer of swapping operations), ending at arr1[i]
if (arr1[j] < arr1[i] && dp[j] != Integer.MAX_VALUE){
	int change = check(arr1, arr2, j, i);
	if (change >= 0){
		dp[i] = Math.min(dp[i], dp[j] + change);
    }
}
```
Now things are done! The check function is actually simple to implement after sorting arr2 and binary search.
Some tricky edge cases here are we have to add one min and one max to the both side of arr1. And we need to deal with the duplicates in arr2. Sorry I didn\'t came up with a more elegant way to do this. I just created two new arrays.


```
class Solution {
    public int makeArrayIncreasing(int[] arr1, int[] arr2) {
        int n = arr1.length;
		
        //sort and generate new arr2
        Arrays.sort(arr2);
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < arr2.length; i++){
            if (i+1 < arr2.length && arr2[i] == arr2[i+1])
                continue;
            list.add(arr2[i]);
        }
        int[] newarr2 = new int[list.size()];
        for (int i = 0; i < list.size(); i++)
            newarr2[i] = list.get(i);
        arr2 = newarr2;
        
		//generate new arr1
        int[] newarr1 = new int[n+2];
        for (int i = 0; i < n; i++)
            newarr1[i+1] = arr1[i];
        newarr1[n+1] = Integer.MAX_VALUE;
        newarr1[0] = Integer.MIN_VALUE;
        arr1 = newarr1;
        
		//perform dp based on LIS
        int[] dp = new int[n+2];
        Arrays.fill(dp, Integer.MAX_VALUE);
        //dp[i] -> answer to change array 0 to i
        dp[0] = 0;
        for (int i = 1; i < n+2; i++){
            for (int j = 0; j < i; j++){
                if (arr1[j] < arr1[i] && dp[j] != Integer.MAX_VALUE){
                    int change = check(arr1, arr2, j, i);
                    if (change >= 0){
                        dp[i] = Math.min(dp[i], dp[j] + change);
                    }
                }
            }
        }
        return dp[n+1] == Integer.MAX_VALUE? -1:dp[n+1];
    }
    
    //change number from start+1 to end-1
    private int check(int[] arr1, int[] arr2, int start, int end){
        if (start+1 == end)
            return 0;
        int min = arr1[start];
        int max = arr1[end];
        int idx = Arrays.binarySearch(arr2, min);
        if (idx < 0)
            idx = -idx-1;
        else
            idx = idx+1;
        
        int maxcount = end-start-1;
        int endi = idx + maxcount-1;
        if (endi < arr2.length && arr2[endi] < max)
            return maxcount;
        else
            return -1;
    }
}
```
</p>


### Simple Java DP Solution + TreeSet with Explanation beats 100%
- Author: laiyinlg
- Creation Date: Sun Sep 08 2019 17:03:33 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 10 2019 01:18:52 GMT+0800 (Singapore Standard Time)

<p>
dp[i][j] : for the index of j in arr1, if we changed i times and then maintain a strickly increasing array from 0 to j , the minimal value for index of j is dp[i][j](we want to make the front numbers as small as possible); if dp[i][j] = Integer.MAX_VALUE, means there is no way to maintain a strictly increasing array with i times from 0 to j. For the last index arr1.length - 1, return the smallest i we can get since it means the minimal steps of change for the whole arr1. 

```
class Solution {
    public int makeArrayIncreasing(int[] arr1, int[] arr2) {
        if (arr1 == null || arr1.length == 0) return -1;
        if (arr1.length == 1) return 0;
        TreeSet<Integer> ts = new TreeSet<>();
        if (arr2 != null) {
            for (int i = 0; i < arr2.length; i++) ts.add(arr2[i]);
        }
        
        int[][] dp = new int[arr1.length + 1][arr1.length + 1];
        for (int i = 0; i < dp.length; i++) Arrays.fill(dp[i], Integer.MAX_VALUE);
        dp[0][0] = Integer.MIN_VALUE;
        
        for (int j = 1; j < dp.length; j++) {
            for (int i = 0; i <= j; i++) {
                if (arr1[j - 1] > dp[i][j - 1]) {
                    dp[i][j] = arr1[j - 1];
                }
                if (i > 0 && ts.higher(dp[i - 1][j - 1]) != null) {
                    dp[i][j] = Math.min(dp[i][j], ts.higher(dp[i - 1][j - 1]));
                }
                if (j == dp.length - 1 && dp[i][j] != Integer.MAX_VALUE) return i; 
            } 
        }
        return -1;
    }
}
</p>


