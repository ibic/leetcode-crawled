---
title: "Friendly Movies Streamed Last Month"
weight: 1585
#id: "friendly-movies-streamed-last-month"
---
## Description
<div class="description">
<p>Table: <code>TVProgram</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| program_date  | date    |
| content_id    | int     |
| channel       | varchar |
+---------------+---------+
(program_date, content_id) is the primary key for this table.
This table contains information of the programs on the TV.
content_id is the id of the program in some channel on the TV.</pre>

<p>&nbsp;</p>

<p>Table: <code>Content</code></p>

<pre>
+------------------+---------+
| Column Name      | Type    |
+------------------+---------+
| content_id       | varchar |
| title            | varchar |
| Kids_content     | enum    |
| content_type     | varchar |
+------------------+---------+
content_id is the primary key for this table.
Kids_content is an enum that takes one of the values (&#39;Y&#39;, &#39;N&#39;) where: 
&#39;Y&#39; means is content for kids otherwise &#39;N&#39; is not content for kids.
content_type&nbsp;is the category of the content as movies, series, etc.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to&nbsp;report the distinct titles of the kid-friendly movies streamed in June 2020.</p>

<p>Return the result table in any order.</p>

<p>The query result format is in the following example.</p>

<p>&nbsp;</p>

<pre>
<code>TVProgram</code> table:
+--------------------+--------------+-------------+
| program_date       | content_id   | channel     |
+--------------------+--------------+-------------+
| 2020-06-10 08:00   | 1            | LC-Channel  |
| 2020-05-11 12:00   | 2            | LC-Channel  |
| 2020-05-12 12:00   | 3            | LC-Channel  |
| 2020-05-13 14:00   | 4            | Disney Ch   |
| 2020-06-18 14:00   | 4            | Disney Ch   |
| 2020-07-15 16:00   | 5            | Disney Ch   |
+--------------------+--------------+-------------+

<code>Content</code> table:
+------------+----------------+---------------+---------------+
| content_id | title          | Kids_content  | content_type  |
+------------+----------------+---------------+---------------+
| 1          | Leetcode Movie | N             | Movies        |
| 2          | Alg. for Kids  | Y             | Series        |
| 3          | Database Sols  | N             | Series        |
| 4          | Aladdin        | Y             | Movies        |
| 5          | Cinderella     | Y             | Movies        |
+------------+----------------+---------------+---------------+

Result table:
+--------------+
| title        |
+--------------+
| Aladdin      |
+--------------+
&quot;Leetcode Movie&quot; is not a content for kids.
&quot;Alg. for Kids&quot; is not a movie.
&quot;Database Sols&quot; is not a movie
&quot;Alladin&quot; is a movie, content for kids and was streamed in June 2020.
&quot;Cinderella&quot; was not streamed in June 2020.
</pre>
</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [MSSQL] Straightforward - (INNER) JOIN, BETWEEN and DISTINCT
- Author: osoleve
- Creation Date: Sun Jun 28 2020 02:36:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 02:36:44 GMT+0800 (Singapore Standard Time)

<p>
Since we need information from both tables for every result, we use an inner join (which in MSSQL can be written as just JOIN). Remember when using BETWEEN that it\'s inclusive of the end date if you don\'t specify a time.

```
SELECT DISTINCT title
FROM TVProgram p
JOIN Content c ON p.content_id = c.content_id
WHERE
    program_date BETWEEN \'6/1/2020\' AND \'6/30/2020\'
AND Kids_content = \'Y\'
AND content_type = \'Movies\'
```
</p>


### Fast MySQL w/ 1 join
- Author: emmuonnhinanhkhoc
- Creation Date: Thu Aug 13 2020 03:42:00 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 13 2020 03:42:00 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below

SELECT DISTINCT c.title
FROM Content c
JOIN TVProgram p
    ON c.content_id = p.content_id
WHERE c.Kids_content = \'Y\'
    AND c.content_type = \'Movies\'
    AND MONTH(p.program_date) = 6
    AND YEAR(p.program_date) = 2020;
```
</p>


### Oracle - Solution faster than 100% of Oracle Submission
- Author: Sadhana_K
- Creation Date: Mon Jun 29 2020 03:23:24 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 29 2020 03:23:24 GMT+0800 (Singapore Standard Time)

<p>
select distinct c.title
from tvprogram t join content c on c.content_id=t.content_id
where c.content_type = \'Movies\' and c.kids_content = \'Y\'
and t.program_date like \'%-06-%\'

--Aadi
</p>


