---
title: "Jump Game IV"
weight: 1241
#id: "jump-game-iv"
---
## Description
<div class="description">
<p>Given an array of&nbsp;integers <code>arr</code>, you are initially positioned at the first index of the array.</p>

<p>In one step you can jump from index <code>i</code> to index:</p>

<ul>
	<li><code>i + 1</code> where:&nbsp;<code>i + 1 &lt; arr.length</code>.</li>
	<li><code>i - 1</code> where:&nbsp;<code>i - 1 &gt;= 0</code>.</li>
	<li><code>j</code> where: <code>arr[i] == arr[j]</code> and <code>i != j</code>.</li>
</ul>

<p>Return <em>the minimum number of steps</em> to reach the <strong>last index</strong> of the array.</p>

<p>Notice that you can not jump outside of the array at any time.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [100,-23,-23,404,100,23,23,23,3,404]
<strong>Output:</strong> 3
<strong>Explanation:</strong> You need three jumps from index 0 --&gt; 4 --&gt; 3 --&gt; 9. Note that index 9 is the last index of the array.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [7]
<strong>Output:</strong> 0
<strong>Explanation:</strong> Start index is the last index. You don&#39;t need to jump.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [7,6,9,6,9,6,9,7]
<strong>Output:</strong> 1
<strong>Explanation:</strong> You can jump directly from index 0 to index 7 which is last index of the array.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [6,1,9]
<strong>Output:</strong> 2
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> arr = [11,22,7,7,7,7,7,7,7,22,13]
<strong>Output:</strong> 3
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 5 * 10^4</code></li>
	<li><code>-10^8 &lt;= arr[i] &lt;= 10^8</code></li>
</ul>

</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies
- Google - 6 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

### Overview

You probably can guess from the problem title, this is the fourth problem in the series of [Jump Game](https://leetcode.com/problems/jump-game/) problems. Those problems are similar, but have considerable differences, making their solutions quite different.

Here, two approaches are introduced: *Breadth-First Search* approach and *Bidirectional BFS* approach. 

---

#### Approach 1: Breadth-First Search

Most solutions start from a brute force approach and are optimized by removing unnecessary calculations. Same as this one.

A naive brute force approach is to iterate all the possible routes and check if there is one reaches the last index. However, if we already checked one index, we do not need to check it again. We can mark the index as visited by storing them in a `visited` set. 

From convenience, we can store nodes with the same value together in a `graph` dictionary. With this method, when searching, we do not need to iterate the whole list to find the nodes with the same value as the next steps, but only need to ask the precomputed dictionary. However, to prevent stepping back, we need to clear the dictionary after we get to that value.

<iframe src="https://leetcode.com/playground/dgKoQc9w/shared" frameBorder="0" width="100%" height="500" name="dgKoQc9w"></iframe>


**Complexity Analysis**

Assume $$N$$ is the length of `arr`.

* Time complexity: $$\mathcal{O}(N)$$ since we will visit every node at most once.
 
* Space complexity: $$\mathcal{O}(N)$$ since it needs `curs` and `nex` to store nodes.

---

#### Approach 2: Bidirectional BFS

In the later part of our original BFS method, the layer may be long and takes a long time to compute the next layer. In this situation, we can compute the layer from the end, which may be short and takes less time.

<iframe src="https://leetcode.com/playground/QBPAhKSq/shared" frameBorder="0" width="100%" height="500" name="QBPAhKSq"></iframe>

**Complexity Analysis**

Assume $$N$$ is the length of `arr`.

* Time complexity: $$\mathcal{O}(N)$$ since we will visit every node at most once, but usually faster than approach 1.
 
* Space complexity: $$\mathcal{O}(N)$$ since it needs `curs`, `other` and `nex` to store nodes.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] BFS Solution - Clean code - O(N)
- Author: hiepit
- Creation Date: Sun Feb 09 2020 00:00:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Feb 15 2020 01:42:29 GMT+0800 (Singapore Standard Time)

<p>
**Java**
```java
class Solution {
    public int minJumps(int[] arr) {
        int n = arr.length;
        HashMap<Integer, List<Integer>> indicesOfValue = new HashMap<>();
        for (int i = 0; i < n; i++)
            indicesOfValue.computeIfAbsent(arr[i], x -> new LinkedList<>()).add(i);
        boolean[] visited = new boolean[n]; visited[0] = true;
        Queue<Integer> q = new LinkedList<>(); q.offer(0);
        int step = 0;
        while (!q.isEmpty()) {
            for (int size = q.size(); size > 0; --size) {
                int i = q.poll();
                if (i == n - 1) return step; // Reached to last index
                List<Integer> next = indicesOfValue.get(arr[i]);
                next.add(i - 1); next.add(i + 1);
                for (int j : next) {
                    if (j >= 0 && j < n && !visited[j]) {
                        visited[j] = true;
                        q.offer(j);
                    }
                }
                next.clear(); // avoid later lookup indicesOfValue arr[i]
            }
            step++;
        }
        return 0;
    }
}
```

**C++**
```C++
class Solution {
public:
    int minJumps(vector<int>& arr) {
        int n = arr.size();
        unordered_map<int, vector<int>> indicesOfValue;
        for (int i = 0; i < n; i++)
            indicesOfValue[arr[i]].push_back(i);
        vector<bool> visited(n); visited[0] = true;
        queue<int> q; q.push(0);
        int step = 0;
        while (!q.empty()) {
            for (int size = q.size(); size > 0; --size) {
                int i = q.front(); q.pop();
                if (i == n - 1) return step; // Reached to last index
                vector<int>& next = indicesOfValue[arr[i]];
                next.push_back(i - 1); next.push_back(i + 1);
                for (int j : next) {
                    if (j >= 0 && j < n && !visited[j]) {
                        visited[j] = true;
                        q.push(j);
                    }
                }
                next.clear(); // avoid later lookup indicesOfValue arr[i]
            }
            step++;
        }
        return 0;
    }
};
```
**Complexity**
- Time & Space: `O(N)`

**Expain Time O(N):** In the case where each index has the same value, the algorithm goes to the neighbor (the same value) once then breaks all the edge by using: `next.clear()`
So the algorithm will traverse up to `N` edges for `j` and `2N` edges for `(i+1, i-1)`.
That\'s why time complexity is `O(3N)` ~ `O(N)`
</p>


### [Python] Simple Solution with Explanations
- Author: yanrucheng
- Creation Date: Sun Feb 09 2020 00:01:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 09 2020 00:06:20 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
Start from position 0. Use breadth first search until the last position is found. 

**Trick**
1. Track all the values we have explored. For a given value, we only add it to the frontier once
2. Track all the positions we have explored.
3. Python trick: `nei[num] * (num not in num_met)`

**Complexity**
Time: `O(N)`
Space: `O(N)`

**Python 3**
```
def minJumps(self, arr):
    nei = collections.defaultdict(list)
    _ = [nei[x].append(i) for i, x in enumerate(arr)]

    frontier = collections.deque([(0,0)])
    num_met, pos_met = set(), set()
    while frontier:
        pos, step = frontier.popleft() # state: position, step
        if pos == len(arr) - 1: return step
        num = arr[pos]
        pos_met.add(pos) # track explored positions

        for p in [pos - 1, pos + 1] + nei[num] * (num not in num_met):
            if p in pos_met or not 0 <= p < len(arr): continue
            frontier.append((p, step + 1))

        num_met.add(num) # track explored values
```

</p>


### 9 lines Python BFS
- Author: ManuelP
- Creation Date: Sun Feb 09 2020 06:23:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 09 2020 06:58:45 GMT+0800 (Singapore Standard Time)

<p>
```
def minJumps(self, A):
    indices = collections.defaultdict(list)
    for i, a in enumerate(A):
        indices[a].append(i)
    done, now = {-1}, {0}
    for steps in itertools.count():
        done |= now
        if len(A) - 1 in done:
            return steps
        now = {j for i in now for j in [i-1, i+1] + indices.pop(A[i], [])} - done
```
Avoids stepping outside on the left by putting `-1` in `done`. No need to avoid stepping outside on the right, that\'s only possible from the goal index, and from the goal we don\'t move further.

First time we step off some value, we `pop` all its indices so we don\'t use them again later.
</p>


