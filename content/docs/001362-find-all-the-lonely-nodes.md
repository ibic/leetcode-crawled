---
title: "Find All The Lonely Nodes"
weight: 1362
#id: "find-all-the-lonely-nodes"
---
## Description
<div class="description">
<p>In a binary tree, a <strong>lonely</strong> node is a node that is the only child of its parent node. The root of the tree is not lonely because it does not have a parent node.</p>

<p>Given the <code>root</code> of a binary tree, return <em>an array containing the values of all lonely nodes</em> in the tree. Return the list <strong>in any order</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/06/03/e1.png" style="width: 203px; height: 202px;" />
<pre>
<strong>Input:</strong> root = [1,2,3,null,4]
<strong>Output:</strong> [4]
<strong>Explanation:</strong> Light blue node is the only lonely node.
Node 1 is the root and is not lonely.
Nodes 2 and 3 have the same parent and are not lonely.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/06/03/e2.png" style="width: 442px; height: 282px;" />
<pre>
<strong>Input:</strong> root = [7,1,4,6,null,5,3,null,null,null,null,null,2]
<strong>Output:</strong> [6,2]
<strong>Explanation:</strong> Light blue nodes are lonely nodes.
Please remember that order doesn&#39;t matter, [2,6] is also an acceptable answer.
</pre>

<p><strong>Example 3:</strong></p>
<strong><img alt="" src="https://assets.leetcode.com/uploads/2020/06/03/tree.png" style="width: 363px; height: 202px;" /> </strong>

<pre>
<strong>
Input:</strong> root = [11,99,88,77,null,null,66,55,null,null,44,33,null,null,22]
<strong>Output:</strong> [77,55,33,66,44,22]
<strong>Explanation:</strong> Nodes 99 and 88 share the same parent. Node 11 is the root.
All other nodes are lonely.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root = [197]
<strong>Output:</strong> []
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> root = [31,null,78,null,28]
<strong>Output:</strong> [78,28]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the&nbsp;<code>tree</code>&nbsp;is in the range&nbsp;<code>[1, 1000].</code></li>
	<li>Each node&#39;s value is between&nbsp;<code>[1, 10^6]</code>.</li>
</ul>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java recursive - top down as parent passes isLonely to each children
- Author: ChrisShoban
- Creation Date: Thu Jun 04 2020 15:39:30 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 04 2020 15:41:50 GMT+0800 (Singapore Standard Time)

<p>
```
public List<Integer> getLonelyNodes(TreeNode root) {
    List<Integer> nodes = new ArrayList<>();
    getLonelyNodes(root, false, nodes); // root is not lonely
    return nodes;
}
````
    private void getLonelyNodes(TreeNode root, boolean isLonely, List<Integer> nodes) {
        if (root == null) return;
        
        if (isLonely) {
            nodes.add(root.val);
        }
        
        getLonelyNodes(root.left, root.right == null, nodes);
        getLonelyNodes(root.right, root.left == null, nodes);
    }
</p>


### Python 3 - Simple recursive solution
- Author: duyh
- Creation Date: Thu Jun 04 2020 14:19:43 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 04 2020 14:19:43 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def getLonelyNodes(self, root: TreeNode) -> List[int]:
      self.lonely_nodes = []

      def dfs(node):
		# We\'ll append the child\'s value only if it is an only child
        if node.left and not node.right:
          self.lonely_nodes.append(node.left.val)
        if node.right and not node.left:
          self.lonely_nodes.append(node.right.val)
		  
		# Then dfs left and/or right, whichever is present
        if node.left:
          dfs(node.left)
        if node.right:
          dfs(node.right)

      dfs(root)
      return self.lonely_nodes
```

</p>


### С++ Recursive and Iterative Solutions
- Author: koval_victor
- Creation Date: Sat Jun 06 2020 20:10:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 06 2020 20:10:29 GMT+0800 (Singapore Standard Time)

<p>
Recursive:
```
class Solution {
    void dfs(TreeNode* root, vector<int>& lonely) {
        if (!root)
            return;
        if (root->left && !root->right)
            lonely.push_back(root->left->val);
        if (!root->left && root->right)
            lonely.push_back(root->right->val);
        dfs(root->left, lonely);
        dfs(root->right, lonely);
    }
public:
    vector<int> getLonelyNodes(TreeNode* root) {
        vector<int> lonely;
        dfs(root, lonely);
        return lonely;
    }
};
```
Iterative:
```
class Solution {
public:
    vector<int> getLonelyNodes(TreeNode* root) {
        vector<int> lonely;
        if (!root)
            return lonely;
        stack<TreeNode*> stack;
        stack.push(root);
        while (!stack.empty()) {
            TreeNode* node = stack.top();
            stack.pop();
            if (node->left) {
                stack.push(node->left);
                if (!node->right)
                    lonely.push_back(node->left->val);
            }
            if (node->right) {
                stack.push(node->right);
                if (!node->left)
                    lonely.push_back(node->right->val);
            }
        }
        return lonely;
    }
};
```
</p>


