---
title: "Maximum Depth of N-ary Tree"
weight: 695
#id: "maximum-depth-of-n-ary-tree"
---
## Description
<div class="description">
<p>Given a n-ary tree, find its maximum depth.</p>

<p>The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.</p>

<p><em>Nary-Tree input serialization&nbsp;is represented in their level order traversal, each group of children is separated by the null value (See examples).</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/12/narytreeexample.png" style="width: 100%; max-width: 300px;" /></p>

<pre>
<strong>Input:</strong> root = [1,null,3,2,4,null,5,6]
<strong>Output:</strong> 3
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/11/08/sample_4_964.png" style="width: 296px; height: 241px;" /></p>

<pre>
<strong>Input:</strong> root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
<strong>Output:</strong> 5
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The depth of the n-ary tree is less than or equal to <code>1000</code>.</li>
	<li>The total number of nodes is between <code>[0,&nbsp;10^4]</code>.</li>
</ul>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Google - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

**Tree definition**

First of all, please refer to [this article](https://leetcode.com/articles/maximum-depth-of-binary-tree/) 
for the solution in case of binary tree.
This article offers the same ideas with a bit of generalisation. 

Here is the definition of the ```TreeNode``` which we would use.

<iframe src="https://leetcode.com/playground/PuxNCs9x/shared" frameBorder="0" width="100%" height="259" name="PuxNCs9x"></iframe>
<br />
<br />


---
#### Approach 1: Recursion

**Algorithm**

The intuitive approach is to solve the problem by recursion.
Here we demonstrate an example with the DFS (Depth First Search) strategy. 

<iframe src="https://leetcode.com/playground/wHobGx37/shared" frameBorder="0" width="100%" height="310" name="wHobGx37"></iframe>

**Complexity analysis**

* Time complexity : we visit each node exactly once, 
thus the time complexity is $$\mathcal{O}(N)$$,
where $$N$$ is the number of nodes.

* Space complexity : in the worst case, the tree is completely unbalanced,
*e.g.* each node has only one child node, the recursion call would occur
 $$N$$ times (the height of the tree), therefore the storage to keep the call stack would be $$\mathcal{O}(N)$$.
 But in the best case (the tree is completely balanced), the height of the tree would be $$\log(N)$$.
 Therefore, the space complexity in this case would be $$\mathcal{O}(\log(N))$$.
<br />
<br />


---
#### Approach 2: Iteration

We could also convert the above recursion into iteration, with the help of stack.

>The idea is to visit each node with the DFS strategy,
while updating the max depth at each visit.

So we start from a stack which contains the root node and the corresponding depth 
which is ```1```.
Then we proceed to the iterations: pop the current node out of the stack and
push the child nodes. The depth is updated at each step. 

<iframe src="https://leetcode.com/playground/JMEzQ32K/shared" frameBorder="0" width="100%" height="429" name="JMEzQ32K"></iframe>  

**Complexity analysis**

* Time complexity : $$\mathcal{O}(N)$$.

* Space complexity : $$\mathcal{O}(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java BFS Iterative Solution
- Author: TKroos
- Creation Date: Fri Oct 19 2018 06:36:07 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 19:24:08 GMT+0800 (Singapore Standard Time)

<p>
    public int maxDepth(Node root) {
        if(root == null) return 0;
        
        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);
        
        int depth = 0;
        
        while(!queue.isEmpty())
        {
            int size = queue.size();
            
            for(int i = 0; i < size; i++)
            {
                Node current = queue.poll();
                for(Node child: current.children) queue.offer(child);
            }
            
            depth++;
        }
        
        return depth;
    }
</p>


### Java solution with explain, same logic with Maximum Depth of binary tree
- Author: iChuan
- Creation Date: Mon Jul 23 2018 05:26:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 06:32:34 GMT+0800 (Singapore Standard Time)

<p>
This question is same as the binary one.
First, let\'s review the 104. Maximum Depth of Binary Tree
```
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int left = maxDepth(root.left);
        int right = maxDepth(root.right);
        return Math.max(left, right) + 1;
    }
}
```
For current node, the maximum depth of this node is Math.max(node.left.depth, node.right.depth).
So, what is the depth of node.left and node.right? You can put node.left as node. The recursive  method has been built.

Now, for this question, there are not only left and right, but there are children. So, only change left, right as a children for loop.
```
/*
// Definition for a Node.
class Node {
    public int val;
    public List<Node> children;

    public Node() {}

    public Node(int _val,List<Node> _children) {
        val = _val;
        children = _children;
    }
};
*/
class Solution {
    public int maxDepth(Node root) {
        if (root == null) {
            return 0;
        }

        int max = 0;
        for (Node child : root.children) { //replace left&right to for loop
            int value = maxDepth(child);
            
            if (value > max) {
                max = value;
            }
        }
        return max +1;
    }
}
```
Actually, this code also works for binary tree.
</p>


### DFS & BFS solutions in C++.
- Author: ttsugrii
- Creation Date: Wed Sep 05 2018 14:40:50 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 18:45:15 GMT+0800 (Singapore Standard Time)

<p>
Recursive DFS:
```
class Solution {
public:
    int maxDepth(Node* root) {
        if (root == nullptr) return 0;
        int depth = 0;
        for (auto child : root->children) depth = max(depth, maxDepth(child));
        return 1 + depth;
    }
};
```

BFS:
```
class Solution {
public:
    int maxDepth(Node* root) {
        if (root == nullptr) return 0;
        queue<Node*> q; q.push(root);
        int depth = 0;
        while (!q.empty()) {
            depth += 1;
            int breadth = q.size();
            for (int _ = 0; _ < breadth; ++_) {
                auto node = q.front(); q.pop();
                for (auto child : node->children) if (child) q.push(child);
            }
        }
        return depth;
    }
};
```
</p>


