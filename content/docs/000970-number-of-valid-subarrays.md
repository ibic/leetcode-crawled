---
title: "Number of Valid Subarrays"
weight: 970
#id: "number-of-valid-subarrays"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of integers, return the number of <strong>non-empty continuous subarrays</strong> that satisfy the following condition:</p>

<p>The leftmost element of the subarray is not larger than other elements in the subarray.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,4,2,5,3]</span>
<strong>Output: </strong><span id="example-output-1">11</span>
<strong>Explanation: </strong>There are 11 valid subarrays: [1],[4],[2],[5],[3],[1,4],[2,5],[1,4,2],[2,5,3],[1,4,2,5],[1,4,2,5,3].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[3,2,1]</span>
<strong>Output: </strong><span id="example-output-2">3</span>
<strong>Explanation: </strong>The 3 valid subarrays are: [3],[2],[1].
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[2,2,2]</span>
<strong>Output: </strong><span id="example-output-3">6</span>
<strong>Explanation: </strong>There are 6 valid subarrays: [2],[2],[2],[2,2],[2,2],[2,2,2].
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 50000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 100000</code></li>
</ol>
</div>

## Tags
- Stack (stack)

## Companies
- Hulu - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ O(n) stack
- Author: votrubac
- Creation Date: Tue Jun 18 2019 15:26:32 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 09 2020 07:15:30 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
If element `i` is the smallest one we encountered so far, it does not form any valid subarray with previous elements. Otherwise, it forms a valid subarray starting from previous element `j`, if `min(nums[j..i]) == nums[j]`.

For this example `[2, 4, 6, 8, 6, 5, 3]`:
- `8`: forms 4 valid subarrays (starting from 2, 4, 6, and 8)
- `6` forms 3 valid subarrays (2, 4, and 6)
- `5` forms 3 valid subarrays (2,4, and 5)
- `3` forms 2 valid subarray (2 anf 3)
# Solution
Maintain monotonically increased values in a stack. The size of the stack is the number of valid subarrays between the first and last element in the stack.
```
int validSubarrays(vector<int>& nums, int res = 0) {
  vector<int> s;
  for (auto n : nums) {
    while (!s.empty() && n < s.back()) s.pop_back();
    s.push_back(n);
    res += s.size();
  }
  return res;
}
```
# Complexity Analysis
Runtime: *O(n)*. We process each element no more than twice.
Memory: *O(n)*.
</p>


### Java solution with thoughts process
- Author: yrq
- Creation Date: Tue Sep 17 2019 03:37:06 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 17 2019 03:39:35 GMT+0800 (Singapore Standard Time)

<p>
First
brute force solution with O(n^2) solution
```
example : [1, 4, 2, 5, 3]
for each number in array, we find maximum continuous length
idx = 0, [1, 4, 2, 5, 3] => max length = 5
idx = 1, [4] => max length = 1
idx = 2, [2, 5, 3] => max length = 3
idx = 3, [5] => max length = 1
idx = 4, [3] => max length = 1
res = array.length + (5 -1 ) + (1 - 1) + (3 - 1) + (1 - 1) + (1 - 1) = 11
```


Now, why don\'t we keep acending order in stack ?  
```
[1, 4, 2, 5, 3]
count += array.length
idx = 0    stack [1]
idx = 1    (4 >= 1) stack [1, 4]   , count += 1 ([1, 4] is valid)
idx = 2    (2 < 4 ) stack [1, 2] , count += 1 ([1, 4, 2] is valid)
idx = 3    (5 > 2)  stack [1, 2, 5] , count += 2 ([1, 4, 2, 5] and [2, 5] are valid)
idx = 4    (3 < 5)  stack [1, 2, 3] , count += 2 ([1, 4, 2, 5, 3] and [2, 5, 3] are valid)
```
here is the solution
```
class Solution {
    public int validSubarrays(int[] nums) {
        int res = nums.length;
        if(nums.length <= 1) {
            return res;
        }
		Deque<Integer> stack = new ArrayDeque<>();
        stack.push(nums[0]);
        for(int i = 1; i < nums.length; i++) {
            int num = nums[i];
            while(!stack.isEmpty() && num < stack.peek()) {
                stack.pop();
            }
            res += stack.size();
            stack.push(num);
        }
        return res;
    }
}
```
</p>


### java concise solution using stack
- Author: trimli
- Creation Date: Wed Jun 12 2019 10:11:30 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 12 2019 10:11:30 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int validSubarrays(int[] nums) {
        Deque<Integer> stack = new ArrayDeque<>();
        int n = nums.length, res = 0;
        for(int i = n - 1; i >= 0; --i){
            while(!stack.isEmpty() && nums[i] <= nums[stack.peek()]) stack.pop();
            res += (stack.isEmpty() ? n : stack.peek()) - i;
            stack.push(i);
        }
        return res;
    }
}
</p>


