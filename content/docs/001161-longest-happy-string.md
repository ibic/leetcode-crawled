---
title: "Longest Happy String"
weight: 1161
#id: "longest-happy-string"
---
## Description
<div class="description">
<p>A string is called <em>happy</em> if it does&nbsp;not have any of the strings <code>&#39;aaa&#39;</code>, <code>&#39;bbb&#39;</code>&nbsp;or <code>&#39;ccc&#39;</code>&nbsp;as a substring.</p>

<p>Given three integers <code>a</code>, <code>b</code> and <code>c</code>, return <strong>any</strong> string <code>s</code>,&nbsp;which satisfies following conditions:</p>

<ul>
	<li><code>s</code> is <em>happy&nbsp;</em>and longest possible.</li>
	<li><code>s</code> contains <strong>at most</strong> <code>a</code>&nbsp;occurrences of the letter&nbsp;<code>&#39;a&#39;</code>, <strong>at most</strong> <code>b</code>&nbsp;occurrences of the letter <code>&#39;b&#39;</code> and <strong>at most</strong> <code>c</code> occurrences of the letter <code>&#39;c&#39;</code>.</li>
	<li><code>s&nbsp;</code>will only contain <code>&#39;a&#39;</code>, <code>&#39;b&#39;</code> and <code>&#39;c&#39;</code>&nbsp;letters.</li>
</ul>

<p>If there is no such string <code>s</code>&nbsp;return the empty string <code>&quot;&quot;</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> a = 1, b = 1, c = 7
<strong>Output:</strong> &quot;ccaccbcc&quot;
<strong>Explanation:</strong> &quot;ccbccacc&quot; would also be a correct answer.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> a = 2, b = 2, c = 1
<strong>Output:</strong> &quot;aabbc&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> a = 7, b = 1, c = 0
<strong>Output:</strong> &quot;aabaa&quot;
<strong>Explanation:</strong> It&#39;s the only correct answer in this case.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= a, b, c &lt;= 100</code></li>
	<li><code>a + b + c &gt; 0</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Greedy (greedy)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Wayfair - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java a >= b >= c
- Author: votrubac
- Creation Date: Sun Apr 05 2020 12:04:36 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 06 2020 10:59:24 GMT+0800 (Singapore Standard Time)

<p>
**Tip:** to simplify the logic, preprocess to assume `a >= b >= c`.

> Update: I added the code with different variable names: `lg`, `med` and `sm`, and we preprocess so that `lg >= med >= sm`. Perhaps, it\'s easier to understand. See "Same Approach, Different Names" below.

**Intuition:** this is almost identical to [984. String Without AAA or BBB](https://leetcode.com/problems/string-without-aaa-or-bbb/discuss/226649/JavaC++-(and-Python)-simple-greedy). We just need to ignore the smallest count in each round. Aassuming `a >= b >= c`: always try to add \'aa\'. If `a - 2 >= b`, add `\'b\'` (if not, the next round will add `\'bb\'`). Repeat recursivelly for the remaining counts.

In other words, we are greedily use two characters from the largest pile. We cusion these two characters with a character from the medium pile. For `[11,5,3]`, as an example, we first generate `\'aabaabaab\'`, and our piles become `[5,2,3]`. At this time, `c` becomes the medium pile, and we generate `\'..aac`\' (`[3,2,2]`).  After we add one more `\'..aa\'`, `c` becomes the largest pile and we pull two characters from it `\'..cc\'` (`[1,2,0]`). We add the rest `\'..bba\'`, and the resulting string is `\'aabaabaabaacaaccbba\'`.

This algorithm can be esilly proved to be correct by a contradiction (and counter-examples).

**C++**
```cpp
string longestDiverseString(int a, int b, int c, char aa = \'a\', char bb = \'b\', char cc = \'c\') {
    if (a < b)
        return longestDiverseString(b, a, c, bb, aa, cc);
    if (b < c)
        return longestDiverseString(a, c, b, aa, cc, bb);
    if (b == 0)
        return string(min(2, a), aa);
    auto use_a = min(2, a), use_b = a - use_a >= b ? 1 : 0; 
    return string(use_a, aa) +  string(use_b, bb) + 
		longestDiverseString(a - use_a, b - use_b, c, aa, bb, cc);
}
```
**Java**
```java
String generate(int a, int b, int c, String aa, String bb, String cc) {
    if (a < b)
        return generate(b, a, c, bb, aa, cc);
    if (b < c)
        return generate(a, c, b, aa, cc, bb);
    if (b == 0)
        return aa.repeat(Math.min(2, a));
    int use_a = Math.min(2, a), use_b = a - use_a >= b ? 1 : 0; 
    return aa.repeat(use_a) + bb.repeat(use_b) +
        generate(a - use_a, b - use_b, c, aa, bb, cc);    
}
public String longestDiverseString(int a, int b, int c) {
    return generate(a, b, c, "a", "b", "c");
}
```

#### Same Approach, Different Names
```cpp
string longestDiverseString(int lg, int med, int sm, char ch_lg = \'a\', char ch_med = \'b\', char ch_sm = \'c\') {
    if (lg < med)
        return longestDiverseString(med, lg, sm, ch_med, ch_lg, ch_sm);
    if (med < sm)
        return longestDiverseString(lg, sm, med, ch_lg, ch_sm, ch_med);
    if (med == 0)
        return string(min(2, lg), ch_lg);
    auto use_lg = min(2, lg), use_med = lg - use_lg >= med ? 1 : 0; 
    return string(use_lg, ch_lg) +  string(use_med, ch_med) + 
		longestDiverseString(lg - use_lg, med - use_med, sm, ch_lg, ch_med, ch_sm);
}
```
</p>


### [Java] Happy Greedy String [without PQ]
- Author: fuqi1001
- Creation Date: Sun Apr 05 2020 12:04:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 05 2020 12:09:10 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public String longestDiverseString(int a, int b, int c) {
        StringBuilder sb = new StringBuilder();
        int size = a + b + c;
        int A = 0, B = 0, C = 0;
        for(int i = 0; i < size; i++) {
            if ((a >= b && a >= c && A != 2) || (B == 2 && a > 0) || (C == 2 && a > 0))  {
                sb.append("a");
                a--;
                A++;
                B = 0;
                C = 0;  
            } else if ((b >= a && b >= c && B != 2) || (A == 2 && b > 0) || (C == 2 && b > 0)) {
                sb.append("b");
                b--;
                B++;
                A = 0;
                C = 0;
            } else if ((c >= a && c >= b && C != 2) || (B == 2 && c > 0) || (A == 2 && c > 0)) {
                sb.append("c");
                c--;
                C++;
                A = 0;
                B = 0;  
            }
        }
        return sb.toString();
    }
}
```
</p>


### Python HEAP solution with explanation
- Author: lechen999
- Creation Date: Sun Apr 05 2020 12:01:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 05 2020 12:47:50 GMT+0800 (Singapore Standard Time)

<p>
```
from heapq import heappush, heappop
class Solution:
    def longestDiverseString(self, a: int, b: int, c: int) -> str:
        max_heap = []
        if a != 0:
            heappush(max_heap, (-a, \'a\'))
        if b != 0:
            heappush(max_heap, (-b, \'b\'))
        if c != 0:
            heappush(max_heap, (-c, \'c\'))
        s = []
        while max_heap:
            first, char1 = heappop(max_heap) # char with most rest numbers
            if len(s) >= 2 and s[-1] == s[-2] == char1: # check whether this char is the same with previous two
                if not max_heap: # if there is no other choice, just return
                    return \'\'.join(s)
                second, char2 = heappop(max_heap) # char with second most rest numbers
                s.append(char2)
                second += 1 # count minus one, because the second here is negative, thus add 1
                if second != 0: # only if there is rest number count, add it back to heap
                    heappush(max_heap, (second, char2))
                heappush(max_heap, (first, char1)) # also need to put this part back to heap
                continue
			
			#  situation that this char can be directly added to answer
            s.append(char1)
            first += 1
            if first != 0:
                heappush(max_heap, (first, char1))
        return \'\'.join(s)
```
</p>


