---
title: "My Calendar II"
weight: 652
#id: "my-calendar-ii"
---
## Description
<div class="description">
<p>Implement a <code>MyCalendarTwo</code> class to store your events. A new event can be added if adding the event will not cause a <b>triple</b> booking.</p>

<p>Your class will have one method, <code>book(int start, int end)</code>. Formally, this represents a booking on the half open interval <code>[start, end)</code>, the range of real numbers <code>x</code> such that <code>start &lt;= x &lt; end</code>.</p>

<p>A <i>triple booking</i> happens when <b>three</b> events have some non-empty intersection (ie., there is some time that is common to all 3 events.)</p>

<p>For each call to the method <code>MyCalendar.book</code>, return <code>true</code> if the event can be added to the calendar successfully without causing a <b>triple</b> booking. Otherwise, return <code>false</code> and do not add the event to the calendar.</p>
Your class will be called like this: <code>MyCalendar cal = new MyCalendar();</code> <code>MyCalendar.book(start, end)</code>

<p><b>Example 1:</b></p>

<pre>
MyCalendar();
MyCalendar.book(10, 20); // returns true
MyCalendar.book(50, 60); // returns true
MyCalendar.book(10, 40); // returns true
MyCalendar.book(5, 15); // returns false
MyCalendar.book(5, 10); // returns true
MyCalendar.book(25, 55); // returns true
<b>Explanation:</b> 
The first two events can be booked.  The third event can be double booked.
The fourth event (5, 15) can&#39;t be booked, because it would result in a triple booking.
The fifth event (5, 10) can be booked, as it does not use time 10 which is already double booked.
The sixth event (25, 55) can be booked, as the time in [25, 40) will be double booked with the third event;
the time [40, 50) will be single booked, and the time [50, 55) will be double booked with the second event.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ul>
	<li>The number of calls to <code>MyCalendar.book</code> per test case will be at most <code>1000</code>.</li>
	<li>In calls to <code>MyCalendar.book(start, end)</code>, <code>start</code> and <code>end</code> are integers in the range <code>[0, 10^9]</code>.</li>
</ul>

<p>&nbsp;</p>
</div>

## Tags
- Ordered Map (ordered-map)

## Companies
- Google - 5 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Brute Force [Accepted]

**Intuition**

Maintain a list of bookings and a list of double bookings.  When booking a new event `[start, end)`, if it conflicts with a double booking, it will have a triple booking and be invalid.  Otherwise, parts that overlap the calendar will be a double booking.

**Algorithm**

Evidently, two events `[s1, e1)` and `[s2, e2)` do *not* conflict if and only if one of them starts after the other one ends: either `e1 <= s2` OR `e2 <= s1`.  By De Morgan's laws, this means the events conflict when `s1 < e2` AND `s2 < e1`.

If our event conflicts with a double booking, it's invalid.  Otherwise, we add conflicts with the calendar to our double bookings, and add the event to our calendar.

<iframe src="https://leetcode.com/playground/84Q7rNgy/shared" frameBorder="0" width="100%" height="395" name="84Q7rNgy"></iframe>


**Complexity Analysis**

* Time Complexity: $$O(N^2)$$, where $$N$$ is the number of events booked.  For each new event, we process every previous event to decide whether the new event can be booked.  This leads to $$\sum_k^N O(k) = O(N^2)$$ complexity.

* Space Complexity: $$O(N)$$, the size of the `calendar`.

---
#### Approach #2: Boundary Count [Accepted]

**Intuition and Algorithm**

When booking a new event `[start, end)`, count `delta[start]++` and `delta[end]--`.  When processing the values of `delta` in sorted order of their keys, the running sum `active` is the number of events open at that time.  If the sum is 3 or more, that time is (at least) triple booked.

A Python implementation was not included for this approach because there is no analog to *TreeMap* available.

```java
class MyCalendarTwo {
    TreeMap<Integer, Integer> delta;

    public MyCalendarTwo() {
        delta = new TreeMap();
    }

    public boolean book(int start, int end) {
        delta.put(start, delta.getOrDefault(start, 0) + 1);
        delta.put(end, delta.getOrDefault(end, 0) - 1);

        int active = 0;
        for (int d: delta.values()) {
            active += d;
            if (active >= 3) {
                delta.put(start, delta.get(start) - 1);
                delta.put(end, delta.get(end) + 1);
                if (delta.get(start) == 0)
                    delta.remove(start);
                return false;
            }
        }
        return true;
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$, where $$N$$ is the number of events booked.  For each new event, we traverse `delta` in $$O(N)$$ time.

* Space Complexity: $$O(N)$$, the size of `delta`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### N^2 Python, Short and Elegant
- Author: awice
- Creation Date: Sun Nov 19 2017 12:24:21 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 05:10:45 GMT+0800 (Singapore Standard Time)

<p>
We store an array `self.overlaps` of intervals that are double booked, and `self.calendar` for intervals which have been single booked.  We use the line `start < j and end > i` to check if the ranges `[start, end)` and `[i, j)` overlap.

The clever idea is we do not need to "clean up" ranges in `calendar`: if we have `[1, 3]` and `[2, 4]`, this will be `calendar = [[1,3],[2,4]]` and `overlaps = [[2,3]]`.  We don't need to spend time transforming the calendar to `calendar = [[1,4]]`.

This solution is by @zestypanda .
```python
class MyCalendarTwo:
    def __init__(self):
        self.overlaps = []
        self.calendar = []

    def book(self, start, end):
        for i, j in self.overlaps:
            if start < j and end > i:
                return False
        for i, j in self.calendar:
            if start < j and end > i:
                self.overlaps.append((max(start, i), min(end, j)))
        self.calendar.append((start, end))
        return True
```
</p>


### Simple AC by TreeMap
- Author: Luckman
- Creation Date: Sun Nov 19 2017 15:46:03 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 07:20:10 GMT+0800 (Singapore Standard Time)

<p>
```
class MyCalendarTwo {

    private TreeMap<Integer, Integer> map;    
    
    public MyCalendarTwo() {
        map = new TreeMap<>();
    }
    
    public boolean book(int start, int end) {
        map.put(start, map.getOrDefault(start, 0) + 1);
        map.put(end, map.getOrDefault(end, 0) - 1);
        int count = 0;
        for(Map.Entry<Integer, Integer> entry : map.entrySet()) {
            count += entry.getValue();
            if(count > 2) {
                map.put(start, map.get(start) - 1);
                if(map.get(start) == 0) {
                    map.remove(start);
                }
                map.put(end, map.get(end) + 1);
                if(map.get(end) == 0) {
                    map.remove(end);
                }
                return false;
            }
        }
        return true;
    }
}
```
</p>


### [Java/C++] Clean Code with Explanation
- Author: alexander
- Creation Date: Sun Nov 19 2017 12:01:44 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 18:51:04 GMT+0800 (Singapore Standard Time)

<p>
The big idea is pretty simple:
Each time of `book`, instead of `fail` a book when there is 1 or more `overlap` with existing books as in `MyCalendar I`, we just want to make sure these overlaps does not overlap - having `overlap` is now ok, but `overlapped` period cannot be `overlapped` again.
So we just need to keep track of all the `overlaps` with any previous `books`

`MyCalendar I` can be reused to track the `overlaps` during each book.

**How to calculate overlap of 2 intervals**
Assume `a` start earlier than `b`, (if not reverse), there could be 3 case, but in any case, an overlap(either positive or negative) can always be represented as:
`` (max(a0, b0), min(a1, b1))``
```
case 1: b ends before a ends:
a: a0 |-------------| a1
b:     b0 |-----| b1

case 2: b ends after a ends:
a: a0 |--------| a1
b:     b0 |--------| b1

case 3: b starts after a ends: (negative overlap)
a: a0 |----| a1
b:              b0 |----| b1
```

**Java**
```
class MyCalendarTwo {
    private List<int[]> books = new ArrayList<>();    
    public boolean book(int s, int e) {
        MyCalendar overlaps = new MyCalendar();
        for (int[] b : books)
            if (Math.max(b[0], s) < Math.min(b[1], e)) // overlap exist
                if (!overlaps.book(Math.max(b[0], s), Math.min(b[1], e))) return false; // overlaps overlapped
        books.add(new int[]{ s, e });
        return true;
    }

    private static class MyCalendar {
        List<int[]> books = new ArrayList<>();
        public boolean book(int start, int end) {
            for (int[] b : books)
                if (Math.max(b[0], start) < Math.min(b[1], end)) return false;
            books.add(new int[]{ start, end });
            return true;
        }
    }
}
```
**C++**
```
class MyCalendar {
    vector<pair<int, int>> books;
public:
    bool book(int start, int end) {
        for (pair<int, int> p : books)
            if (max(p.first, start) < min(end, p.second)) return false;
        books.push_back({start, end});
        return true;
    }
};

class MyCalendarTwo {
    vector<pair<int, int>> books;
public:
    bool book(int start, int end) {
        MyCalendar overlaps;
        for (pair<int, int> p : books) {
            if (max(p.first, start) < min(end, p.second)) { // overlap exist
                pair<int, int> overlapped = getOverlap(p.first, p.second, start, end);
                if (!overlaps.book(overlapped.first, overlapped.second)) return false; // overlaps overlapped
            }
        }
        books.push_back({ start, end });
        return true;
    }

    pair<int, int> getOverlap(int s0, int e0, int s1, int e1) {
        return { max(s0, s1), min(e0, e1)};
    }
};
```

**Another way to calculate overlap of 2 intervals**
`a started with b`, or, `b started within a`:
```
a:                     |---------|
b:
a0<b0 & a1<b0:  |----|
a0<b0 & a1>b0:  |------------| (a started within b)
a0<b0 & a1>b1:  |-------------------| (a started within b)
a0>b0 & a0<b1:            |----|  (b started within a)
a0>b0 & a0>b1:            |---------| (b started within a)
a0>b1 & a1>b1:                      |----|

```
</p>


