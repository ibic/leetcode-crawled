---
title: "Max Dot Product of Two Subsequences"
weight: 1342
#id: "max-dot-product-of-two-subsequences"
---
## Description
<div class="description">
<p>Given two arrays <code>nums1</code>&nbsp;and <code><font face="monospace">nums2</font></code><font face="monospace">.</font></p>

<p>Return the maximum dot product&nbsp;between&nbsp;<strong>non-empty</strong> subsequences of nums1 and nums2 with the same length.</p>

<p>A subsequence of a array is a new array which is formed from the original array by deleting some (can be none) of the characters without disturbing the relative positions of the remaining characters. (ie,&nbsp;<code>[2,3,5]</code>&nbsp;is a subsequence of&nbsp;<code>[1,2,3,4,5]</code>&nbsp;while <code>[1,5,3]</code>&nbsp;is not).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [2,1,-2,5], nums2 = [3,0,-6]
<strong>Output:</strong> 18
<strong>Explanation:</strong> Take subsequence [2,-2] from nums1 and subsequence [3,-6] from nums2.
Their dot product is (2*3 + (-2)*(-6)) = 18.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [3,-2], nums2 = [2,-6,7]
<strong>Output:</strong> 21
<strong>Explanation:</strong> Take subsequence [3] from nums1 and subsequence [7] from nums2.
Their dot product is (3*7) = 21.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [-1,-1], nums2 = [1,1]
<strong>Output:</strong> -1
<strong>Explanation: </strong>Take subsequence [-1] from nums1 and subsequence [1] from nums2.
Their dot product is -1.</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums1.length, nums2.length &lt;= 500</code></li>
	<li><code>-1000 &lt;= nums1[i], nums2[i] &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Common DP pattern with diagram, explanation,  Q&A
- Author: interviewrecipes
- Creation Date: Sun May 24 2020 12:18:13 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 26 2020 04:29:15 GMT+0800 (Singapore Standard Time)

<p>
![image](https://assets.leetcode.com/users/bhaveshm/image_1590354633.png)


**Bottom up DP**
This is a common pattern among dynamic programming problems. The line of thinking is -

**Idea is to think like this**
Lets say, you know the solution for all combinations of i and j where `i < X` and `j < Y`.
i.e. You know the answer for (0, 0), (0, 1) ... (0, Y-1), (1, 0), (1, 1), ... (1, Y-1), ..., ... , (X-1, 0), (X-1, 1), ... , (X-1, Y-1).

**What do I mean by answer for (i, j)?**
Answer for `(i, j)` is the max dot product of subsequences if we only consider the first list from 0th index to ith index (nums1) and the second list from 0th index to jth index (nums2).

**Using all the answers that we already know, can we find the answer for `(X,Y)`?**

```
F(X, Y) = max (
            nums1[X]*nums2[Y],       // ignore previous F(.., ..) because it might be better to not add it at all (i.e. if it is negative).
            F(X-1, Y),                             // ignore the last number from first
            F(X, Y-1),                            // ignore the last number from second
            F(X-1, Y-1) + nums[X] * nums[Y],  // use last numbers from both the first and the second
          )
```

**How to get started?**
Say `len(nums1) = n` and `len(nums2) = m`.
Simply consider the first list as empty and second list has some elements. Easily you understand that the answer for all `(0, j)` where  `j = 0 to m-1` is going to be **0**.
Similarly, `(i, 0)` will be **0** for all `i = 0 to n-1`.

However, there is a catch. For this particular problem, we need to choose at least one number as a part of subsequence. Hence instead of initializing to 0, which we would normally do, we need to initialize it to some large negative number so that whenever we are choosing max() out of 4 possibilities, we never choose that large negative number. As the limits for numbers is -1000 to +1000, maximum negative value will be -10e6. So any number greater than -10e6 should be fine. I chose -10e7. Be careful when choosing INT_MIN though because it may lead to integer underflow(?). 

Now, the ground work is done. The only task is to implement the `F(X, Y)` and go from bottom to up.

Intentially keeping the code more obvious (There is a scope of improvement and reduce a couple of iterations).

**Call for action!**
What questions do you still have? I would be happy to add more details. At this time, I am not sure what else should I add. I directly jumpled at how to attack the problems.

```
class Solution {
public:
        const int INF = 10e7;
        int maxDotProduct(vector<int>& nums1, vector<int>& nums2) {
        int n = nums1.size();
        int m = nums2.size();
        vector<vector<int>> dp(n+1, vector<int>(m+1, -INF));
        for (int i=1; i<=n; i++) {
            for (int j=1; j<=m; j++) {
                dp[i][j] = max({
                                nums1[i-1]*nums2[j-1],
                                dp[i-1][j-1] + nums1[i-1]*nums2[j-1],
                                dp[i-1][j],
                                dp[i][j-1]
                               });
            }
        }
        return dp[n][m];
    }
};
```

**Interesting question and answers from comments below**
Q 1
```
For the transfer function, what if we want to ignore both the last numbers?

F(X, Y) = max (
F(X-1, Y), // ignore the last number from first
F(X, Y-1), // ignore the last number from second
F(X-1, Y-1) + nums[X] * nums[Y], // use last numbers from both the first and the second
nums[X] * nums[Y], // only use last numbers from both the first and the second
F(X-1, Y-1), // ignore both the last numbers
)
```
A 1
```
Actually it\'s not necessary to consider, because in that case F(X, Y) would be F(X-1, Y-1).
If F(X-1, Y-1) is indeed the actual answer, it will propagate via F(X-1, Y) or F(X, Y-1).

Why?
F(X-1, Y) = max(F(X-1, Y-1) ,
F(X-2, Y),
F(X-2, Y-1) + nums1[X-1]*nums[Y])
Similarly for F(X, Y-1) = ....
```

Q 2
```
Can you explain how does dp[i][j] tells which numbers were exactly picked as dot product tuple?
```
```
To know exactly what numbers were picked is a bit involved problem. It\'s not hard, it\'s just that it uses more memory and hence constraints 
need to be smaller I believe.

To answer the question -
We need to always keep track of what numbers are getting multiplied. How is dp[1][1] in the diagram? Its a multiplication of 3 and 2. 
So store (3,2) in some parallel matrix, say path[1][1] = [(3, 2)]. Now dp[1][2] is also 6. But it is because of the value propagated 
from left, so also propogate in path i.e. path[1][2] = [(3, 2)]. So there is no new multiplication. Fast forward... dp[3][3] is 18. Now 
this is not simply because of propogation from top or left, there is a new multiplication that led to 18. dp[3][3] is 18 because of 
dp[2][2] + (-6)*(-2). So in path[3][3], you will add new pair (-6, -2). Path[2][2] will have [(3, 2)]. Path[3][3] = Path[2][2] + (-6, -2) 
= [(3, 2), (-6, -2)].

Does it make sense? I know it\'s a bit vague, but let me know if I should add more.

Idea is to keep track of new multiplication that change the value at dp[i][j].
```

Q 3
```
Can you post some of the similar questions? It would help.
```
```
In May 30 days challenge, today\'s question is just a variation of this problem.
https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/537/week-4-may-22nd-may-28th/3340/

Additional problems you might want to look -
Easy
https://leetcode.com/problems/unique-paths-ii/
https://leetcode.com/problems/unique-paths/

Medium
https://leetcode.com/problems/minimum-path-sum/
https://leetcode.com/problems/longest-common-subsequence/

Hard
https://leetcode.com/problems/edit-distance/

```

**Please upvote if this helps. It encourages me to put in more efforts.**
</p>


### [Java/C++/Python] the Longest Common Sequence
- Author: lee215
- Creation Date: Sun May 24 2020 12:06:02 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 26 2020 13:50:09 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Have you ever heard the problem:
Find out the longest common sequence.
Similar problem: [1035. Uncrossed Lines](https://leetcode.com/problems/uncrossed-lines/discuss/282842/JavaC++Python-DP-The-Longest-Common-Subsequence)
<br>

## **Complexity**
Time `O(MN)`
Space `O(MN)`
<br>

**C++**
```cpp
    int maxDotProduct(vector<int>& A, vector<int>& B) {
        int n = A.size(), m = B.size();
        vector<vector<int>> dp(n, vector<int>(m));
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                dp[i][j] = A[i] * B[j];
                if (i && j) dp[i][j] += max(dp[i - 1][j - 1], 0);
                if (i) dp[i][j] = max(dp[i][j], dp[i - 1][j]);
                if (j) dp[i][j] = max(dp[i][j], dp[i][j - 1]);
            }
        }
        return dp[n - 1][m - 1];
    }
```
**Java**
```java
    public int maxDotProduct(int[] A, int[] B) {
        int n = A.length, m = B.length, dp[][] = new int[n][m];
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                dp[i][j] = A[i] * B[j];
                if (i > 0 && j > 0) dp[i][j] += Math.max(dp[i-1][j-1], 0);
                if (i > 0) dp[i][j] = Math.max(dp[i][j], dp[i-1][j]);
                if (j > 0) dp[i][j] = Math.max(dp[i][j], dp[i][j - 1]);
            }
        }
        return dp[n-1][m-1];
    }
```
**Python:**
```py
    def maxDotProduct(self, A, B):
        n, m = len(A), len(B)
        dp = [[0] * (m) for i in xrange(n)]
        for i in xrange(n):
            for j in xrange(m):
                dp[i][j] = A[i] * B[j]
                if i and j: dp[i][j] += max(dp[i - 1][j - 1], 0)
                if i: dp[i][j] = max(dp[i][j], dp[i - 1][j])
                if j: dp[i][j] = max(dp[i][j], dp[i][j - 1])
        return dp[-1][-1]
```

</p>


### [C++/Python] Simple DP
- Author: yanrucheng
- Creation Date: Sun May 24 2020 12:00:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 24 2020 12:13:53 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
This is a typical Dynamic Programming problem. The only trick lies in the **non-emptiness**.

- Target: We want to calculate the maximal dot product for nums1[0:i] and nums2[0:j]
- Base case
    - When `i == 0` or `j == 0`, we return -inf. (Because this is an **empty** case, which is intolerable)
- State Transition, for any `i > 0` and `j > 0`, there are 4 possibilities
    - `nums1[i - 1]` is not selected, `dp[i][j] = dp[i - 1][j]`
    - `nums2[j - 1]` is not selected, `dp[i][j] = dp[i][j - 1]`
    - Neither `nums1[i - 1]` or `nums2[j - 1]` is selected, `dp[i][j] = dp[i - 1][j - 1]`
    - Both `nums1[i - 1]` and `nums2[j - 1]` are selected, `dp[i][j] = max(dp[i - 1][j - 1], 0) + nums1[i - 1] * nums2[j - 1]`
        - **Since we already selected one pair (nums1[i - 1], nums2[j - 1]), we can assume the minimal proceeding value is `0`**

**Complexity**
- Time: `O(mn)`
- Space: `O(m)`

**C++, dp**
```
class Solution {
public:
    int maxDotProduct(vector<int>& nums1, vector<int>& nums2) {
        int n = int(nums1.size()), m = int(nums2.size());
        vector<vector<int>> dp(n + 1, vector<int>(m + 1, INT_MIN));
        for (int i = 1; i <= n; ++i) {
            for (int j = 1; j <= m; ++j) {
                dp[i][j] = max(dp[i][j], dp[i - 1][j]);
                dp[i][j] = max(dp[i][j], dp[i][j - 1]);
                dp[i][j] = max(dp[i][j], dp[i - 1][j - 1]);
                dp[i][j] = max(dp[i][j], max(dp[i - 1][j - 1], 0) + nums1[i - 1] * nums2[j - 1]);
            }
        }
        return dp[n][m];
    }
};
```

**Python 3, recursion with memoization**
```
from functools import lru_cache
class Solution:
    def maxDotProduct(self, nums1: List[int], nums2: List[int]) -> int:
        @lru_cache(None)
        def helper(i, j):
            if i == 0 or j == 0: return -math.inf
            return max(helper(i - 1, j - 1), helper(i, j - 1), helper(i - 1, j),
                       max(helper(i - 1, j - 1), 0) + nums1[i - 1] * nums2[j - 1])
        return helper(len(nums1), len(nums2))
```

**Python 3, dp**
```
class Solution:
    def maxDotProduct(self, nums1: List[int], nums2: List[int]) -> int:
        n, m = len(nums1), len(nums2)
        dp = [-math.inf] * (m + 1)
        for i in range(1, n + 1):
            dp, old_dp = [-math.inf], dp
            for j in range(1, m + 1):
                dp += max(
                    old_dp[j], # not select i
                    dp[-1], # not select j
                    old_dp[j - 1], # not select either
                    max(old_dp[j - 1], 0) + nums1[i - 1] * nums2[j - 1], # select both
                ),
        return dp[-1]     
```


</p>


