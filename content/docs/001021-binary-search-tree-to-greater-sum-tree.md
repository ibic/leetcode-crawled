---
title: "Binary Search Tree to Greater Sum Tree"
weight: 1021
#id: "binary-search-tree-to-greater-sum-tree"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a Binary Search Tree (BST), convert it to a Greater Tree such that every key of the original BST is changed to the original key plus sum of all keys greater than the original key in BST.</p>

<p>As a reminder, a <em>binary search tree</em> is a tree that satisfies these constraints:</p>

<ul>
	<li>The left subtree of a node contains only nodes with keys&nbsp;<strong>less than</strong>&nbsp;the node&#39;s key.</li>
	<li>The right subtree of a node contains only nodes with keys&nbsp;<strong>greater than</strong>&nbsp;the node&#39;s key.</li>
	<li>Both the left and right subtrees must also be binary search trees.</li>
</ul>

<p><strong>Note:</strong> This question is the same as 538:&nbsp;<a href="https://leetcode.com/problems/convert-bst-to-greater-tree/">https://leetcode.com/problems/convert-bst-to-greater-tree/</a></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/05/02/tree.png" style="width: 550px; height: 375px;" />
<pre>
<strong>Input:</strong> root = [4,1,6,0,2,5,7,null,null,null,3,null,null,null,8]
<strong>Output:</strong> [30,36,21,36,35,26,15,null,null,null,33,null,null,null,8]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = [0,null,1]
<strong>Output:</strong> [1,null,1]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [1,0,2]
<strong>Output:</strong> [3,3,2]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root = [3,2,4,1]
<strong>Output:</strong> [7,9,4,10]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is in the range <code>[1, 100]</code>.</li>
	<li><code>0 &lt;= Node.val &lt;= 100</code></li>
	<li>All the values in the tree are <strong>unique</strong>.</li>
	<li><code>root</code> is guaranteed to be a valid binary search tree.</li>
</ul>

</div>

## Tags
- Binary Search Tree (binary-search-tree)

## Companies
- Amazon - 6 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Revered Inorder Traversal
- Author: lee215
- Creation Date: Sun May 05 2019 12:22:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 05 2019 12:22:20 GMT+0800 (Singapore Standard Time)

<p>
We need to do the work from biggest to smallest, right to left.
`pre` will record the previous value the we get, which the total sum of bigger values.
For each node, we update `root.val` with `root.val + pre`.

<br>

**Java:**
```
    int pre = 0;
    public TreeNode bstToGst(TreeNode root) {
        if (root.right != null) bstToGst(root.right);
        pre = root.val = pre + root.val;
        if (root.left != null) bstToGst(root.left);
        return root;
    }
```

**C++:**
```
    int pre = 0;
    TreeNode* bstToGst(TreeNode* root) {
        if (root->right) bstToGst(root->right);
        pre = root->val = pre + root->val;
        if (root->left) bstToGst(root->left);
        return root;
    }
```

**Python:**
```
    val = 0
    def bstToGst(self, root):
        if root.right: self.bstToGst(root.right)
        root.val = self.val = self.val + root.val
        if root.left: self.bstToGst(root.left)
        return root
```

</p>


### is it a horriable description?
- Author: xd386
- Creation Date: Mon May 06 2019 08:53:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 06 2019 08:53:03 GMT+0800 (Singapore Standard Time)

<p>
modify it so that **every node has a new value equal to the sum of the values of the original tree** that are greater than or equal to node.val
 
can any one explain me how does the example meet the condition?
</p>


### [Java] 3 iterative and recursive codes w/ comments and explanation.
- Author: rock
- Creation Date: Sun May 05 2019 17:42:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 20 2019 16:03:28 GMT+0800 (Singapore Standard Time)

<p>
# **Method 1:**

Iterative version: use stack to **pop out the nodes in reversed in order sequence**.

Initially, use `cur` to point to the root,
1. push into Stack the right-most path of current subtree;
2. pop out a node, update sum and the node value;
3. point `cur` to the node\'s left child, if any;
Repeat the above till the stack is empty and `cur` has no left child.

```
    public TreeNode bstToGst(TreeNode root) {
        Deque<TreeNode> stk = new ArrayDeque<>();
        TreeNode cur = root;
        int sum = 0;
        while (cur != null || !stk.isEmpty()) {
            while (cur != null) { // save right-most path of the current subtree
                stk.push(cur);
                cur = cur.right;
            }
            cur = stk.pop(); // pop out by reversed in-order.
            sum += cur.val; // update sum.
            cur.val = sum; // update node value.
            cur = cur.left; // move to left branch.
        }    
        return root;
    }
```
**Analysis:**

Time & space: O(n).


# **Method 2:**

Recursive version: **using a sum TreeNode (more safety) instead of an instance variable**.

Obviously, `sum` updates its value by reversed in-order traversal of nodes.

```
    public TreeNode bstToGst(TreeNode root) {
        reversedInorder(root, new TreeNode(0));
        return root;
    }
    private void reversedInorder(TreeNode node, TreeNode sum) {
        if (node == null) { return; }
        reversedInorder(node.right, sum);
        sum.val += node.val;
        node.val = sum.val;
        reversedInorder(node.left, sum);
    }
```
better version from @thekensai:

```
    public TreeNode bstToGst(TreeNode root) {
        reversedInorder(root, 0);
        return root;
    }
    private int reversedInorder(TreeNode node, int sum) {
        if (node == null) { return sum; } // base case.
        node.val += reversedInorder(node.right, sum); // recurse to right subtree.
        return reversedInorder(node.left, node.val); // recurse to left subtree.
    }
```
**Analysis:**

Time: O(n), space: O(n) if considering recursion stack.

# **Method 3:**

Iterative version.

Morris algorithm - for pictures and explanation in details please refer to [here](http://www.learn4master.com/algorithms/morris-traversal-inorder-tree-traversal-without-recursion-and-without-stack).

![image](https://assets.leetcode.com/users/rock/image_1557055556.png)


Note: typically the description of Morris algorithm is about in-order traversal, not reversed in-order. So I add comments for the following code to make beginners more comfortable, hopefully.

```
    public TreeNode convertBST(TreeNode root) {
        TreeNode cur = root;
        int sum = 0;
        while (cur != null) {
            if (cur.right != null) { // traverse right subtree.
                TreeNode leftMost = cur.right;
                while (leftMost.left != null && leftMost.left != cur) { // locate the left-most node of cur\'s right subtree.
                    leftMost = leftMost.left;
                }
                if (leftMost.left == null) { // never visit the left-most node yet.
                    leftMost.left = cur; // construct a way back to cur.
                    cur = cur.right; // explore right.
                }else { // visited leftMost already, which implies now on way back.
                    leftMost.left = null; // cut off the fabricated link.
                    sum += cur.val; // update sum.
                    cur.val = sum; // update node value.
                    cur = cur.left; // continue on way back.
                }
            }else { // no right child: 1) cur is the right-most of unvisited nodes; 2) must traverse left.
                sum += cur.val; // update sum.
                cur.val = sum; // update node value.
                cur = cur.left; // continue on way back.
            }
        }
        return root;
    }
```

**Analysis:**

Time: O(n), space: O(1).
</p>


