---
title: "Race Car"
weight: 757
#id: "race-car"
---
## Description
<div class="description">
<p>Your car starts at position 0 and speed +1 on an infinite number line.&nbsp; (Your car can go into negative positions.)</p>

<p>Your car drives automatically according to a sequence of instructions A (accelerate) and R (reverse).</p>

<p>When you get an instruction &quot;A&quot;, your car does the following:&nbsp;<code>position += speed, speed *= 2</code>.</p>

<p>When you get an instruction &quot;R&quot;, your car does the following: if your speed is positive then&nbsp;<code>speed = -1</code>&nbsp;, otherwise&nbsp;<code>speed = 1</code>.&nbsp; (Your position stays the same.)</p>

<p>For example, after commands &quot;AAR&quot;, your car goes to positions 0-&gt;1-&gt;3-&gt;3, and your speed goes to 1-&gt;2-&gt;4-&gt;-1.</p>

<p>Now for some target position, say the <strong>length</strong> of the shortest sequence of instructions to get there.</p>

<pre>
<strong>Example 1:</strong>
<strong>Input:</strong> 
target = 3
<strong>Output:</strong> 2
<strong>Explanation:</strong> 
The shortest instruction sequence is &quot;AA&quot;.
Your position goes from 0-&gt;1-&gt;3.
</pre>

<pre>
<strong>Example 2:</strong>
<strong>Input:</strong> 
target = 6
<strong>Output:</strong> 5
<strong>Explanation:</strong> 
The shortest instruction sequence is &quot;AAARA&quot;.
Your position goes from 0-&gt;1-&gt;3-&gt;7-&gt;7-&gt;6.
</pre>

<p>&nbsp;</p>

<p><strong>Note: </strong></p>

<ul>
	<li><code>1 &lt;= target &lt;= 10000</code>.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Heap (heap)

## Companies
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

---

#### Approach Framework

**Explanation**

Let $$A^k$$ denote the command $$A A A \cdots A$$ (k times).

Starting with an `"R"` command doesn't help, and as the final sequence does not end on an `"R"`, so we have some sequence like $$R A^{k_1} R A^{k_2} \cdots R A^{k_n}$$ which could be instead $$A^{k_2} R A^{k_3} R \cdots A^{k_n} R R A^{k_1}$$ for the same final position of the car.  (Here, $$k_i \geq 0$$, where $$A^0$$ means no command.)

So let's suppose our command is always of the form $$A^{k_1} R A^{k_2} R \cdots A^{k_n}$$.  Note that under such a command, the car will move to final position $$(2^{k_1} - 1) - (2^{k_2} - 1) + (2^{k_3} - 1) - \cdots $$.

Without loss of generality, we can say that ($$k_i$$, $$i$$ odd) is monotone decreasing, and ($$k_i$$, $$i$$ even) is also monotone decreasing.

Also because terms will cancel out, we can also ignore the possibility that $$k_i = k_j$$ (for $$i, j$$ with different parity).

A key claim is that $$k_i$$ is bounded by $$a+1$$, where $$a$$ is the smallest integer such that $$2^a \geq \text{target}$$ - basically, if you drive past the target, you don't need to keep driving.  This is because it adds another power of two (as $$2^{k_i} - 1 = \sum_{j < k_i} 2^j$$) to the position that must get erased by one or more negative terms later (in whole or in part), as it is not part of the target.

---

#### Approach #1: Dijkstra's [Accepted]

**Intuition**

With some `target`, we have different moves we can perform (such as $$k_1 = 0, 1, 2, \cdots$$, using the notation from our *Approach Framework*), with different costs.

This is an ideal setup for Dijkstra's algorithm, which can help us find the shortest cost path in a weighted graph.  

**Algorithm**

Dijkstra's algorithm uses a priority queue to continually searches the path with the lowest cost to destination, so that when we reach the target, we know it must have been through the lowest cost path.  Refer to [this link](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm) for more detail.

Back to the problem, as described above, we have some `barrier` where we are guaranteed to never cross.  We will also handle negative targets; in total we will have `2 * barrier + 1` nodes.

After, we could move `walk = 2**k - 1` steps for a cost of `k + 1` (the `1` is to reverse).  If we reach our destination exactly, we don't need the `R`, so it is just `k` steps.

<iframe src="https://leetcode.com/playground/fijRbd73/shared" frameBorder="0" width="100%" height="500" name="fijRbd73"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(T \log T)$$.  There are $$O(T)$$ nodes, we process each one using $$O(\log T)$$ work (both popping from the heap and adding the edges).

* Space Complexity: $$O(T)$$.

---

#### Approach #2: Dynamic Programming [Accepted]

**Intuition and Algorithm**

As in our *Approach Framework*, we've framed the problem as a series of moves $$k_i$$.

Now say we have some target `2**(k-1) <= t < 2**k` and we want to know the cost to go there, if we know all the other costs `dp[u]` (for `u < t`).

If `t == 2**k - 1`, the cost is just `k`: we use the command $$A^k$$, and clearly we can't do better.

Otherwise, we might drive without crossing the target for a position change of $$2^{k-1} - 2**j$$, by the command $$A^{k-1} R A^{j} R$$, for a total cost of $$k - 1 + j + 2$$.

Finally, we might drive $$2^k - 1$$ which crosses the target, by the command $$A^k R$$, for a total cost of $$k + 1$$.

We can use dynamic programming together with the above recurrence to implement the code below.

<iframe src="https://leetcode.com/playground/zePbfZFi/shared" frameBorder="0" width="100%" height="412" name="zePbfZFi"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(T \log T)$$.  Each node `i` does $$\log i$$ work.

* Space Complexity: $$O(T)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Summary of the BFS and DP solutions with intuitive explanation
- Author: fun4LeetCode
- Creation Date: Wed Apr 18 2018 06:09:40 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 06:50:25 GMT+0800 (Singapore Standard Time)

<p>
`I -- BFS solution`

Well, the BFS solution is straightforward: we can keep track of all the possible positions of the racecar after `n` instructions (`n = 0, 1, 2, 3, 4, ...`) and return the smallest `n` such that the target position is reached. Naive BFS will run at `O(2^n)` since for each position we have two choices: either accelerate or reverse. Further observations reveal that there may be overlapping among  intermediate states so we need to memorize visited states (each state is characterized by two integers: car position and car speed). However, the total number of unique states still blows up for large target positions (because the position and speed can grow unbounded), so we need further pruning of the search space.

---

`II -- DP solution`

DP solution works by noting that after each reverse, the car\'s speed returns to `1` (the sign can be interpreted as the direction of the speed). So we can redefine the problem in terms of the position of the car while leave out the speed: let `T(i)` be the length of the shortest instructions to move the car from position `0` to position `i`, with initail speed of `1` and its direction pointing towards position `i`. Then our original problem will be `T(target)`, and the base case is `T(0) = 0`. Next we need to figure out the recurrence relations for `T(i)`.

Note that to apply the definition of `T(i)` to subproblems, the car has to start with speed of `1`, which implies we can only apply `T(i)` right after the **reverse** instruction. Also we need to make sure the direction of the initial speed when applying `T(i)` is pointing towards the final target position.

However, we don\'t really know how many **accelerate** instructions there should be before the **reverse** instruction, so theoretically we need to try all possible cases: zero `A`, one `A`, two `A`, three `A`, ... and so on. For each case, we can obtain the position of the car right before the **reverse** instruction, which will be denoted as `j = 2^m - 1`, with `m` the number of `A`\'s. Then depending on the relation between `i` and `j`, there will be three cases:

1. `j < i`: the reverse instruction is issued before the car reaches `i`. In this case, we cannot apply the definition of `T(i)` to the subproblems directly, because even though the speed of the car returns to `1`, its direction is **pointing away from** the target position (in this case position `i`). So we have to wait until the second reverse instruction is issued. Again, we don\'t really know how many accelerate instructions there should be in between these two reverse instructions, so we will try each of the cases: zero `A`, one `A`, two `A`, three `A`, ..., etc. Assume the number of `A` is `q`, then the car will end up at position `j - p` right before the second reverse instruction, where `p = 2^q - 1`. Then after the second reverse instruction, our car will start from position `j - p` with speed of `1` and its direction pointing towards our target position `i`. Since we want the length of the total instruction sequence to be minimized, we certainly wish to use minimum number of instructions to move the car from `j - p` to `i`, which by definition will be given by `T(i-(j-p))` (note that in the definition of `T(i)`, we move the car from position `0` to position `i`. If the start position is not `0`, we need to shift both the start and target positions so that the start position is aligned with `0`). So in summary, for this case, the total length of the instruction will be given by: `m + 1 + q + 1 + T(i-(j-p))`, where `m` is the number of `A` before the first `R`, `q` is the number of `A` before the second `R`, the two `1`\'s correspond to the two `R`\'s, and lastly `T(i-(j-p))` is the length of instructions moving the car from position `j - p` to the target position `i`.

2. `j == i`: the target position is reached without any reverse instructions. For this case, the total length of the instruction will be given by `m`. 

3. `j > i`: the reverse instruction is issued after the car goes beyond `i`. In this case, we don\'t need to wait for a second reverse instruction, because after the first reverse instruction, the car\'s speed returns to `1` and its direction **will be pointing towards** our target position `i`. So we can apply the definition of `T(i)` directly to the subproblem, which will be `T(j-i)`. Note that not only do we need to shift the start and target positions, but also need to swap them as well as the directions. So for this case, the total length of the instructions will be given by `m + 1 + T(j-i)`. 

Our final answer for `T(i)` will be the minimum of the above three cases.

---

`III -- Intuitive explanation of the optimizations`

As I mentioned in section `I`, we need further optimizations for the BFS solution to work efficiently. This turns out also to be the case for the DP solution. To see why, recall that in the first case of the DP solution, we don\'t really impose any upper limit on the value of `q` (we do have limit for the value of `m` though: `j = 2^m-1 < i`), while in the third case, we don\'t really have any upper limit for the value of `m`. Apparently we cannot explore every possible values of `m` and `q` (there are infinitely many).

to be updated...

---

`IV -- Solutions`

Here is a list of solutions: one BFS, one top-down DP and one bottom-up DP. 

The BFS runs at `O(target * log(target))` in the worst case, with `O(target * log(target))` space. The reasoning is as follows: in the worst case, all positions in the range `[-target, target]` will be visited and for each position there can be as many as `2 * log(target)` different speeds.

Both the top-down DP and bottom-up DP run at  `O(target * (log(target))^2)` with `O(target)` space. However, the top-down DP may be slightly more efficient as it may skip some of the intermediate cases that must be computed explicitly for the bottom-up DP. Though the nominal time complexity are the same, both DP solutions will be much more efficient in practice compared to the BFS solution, which has to deal with `(position, speed)` pairs and their keys for hashing, etc.

<br>

BFS solution:

```
public int racecar(int target) {
    Deque<int[]> queue = new LinkedList<>();
    queue.offerLast(new int[] {0, 1}); // starts from position 0 with speed 1
    
    Set<String> visited = new HashSet<>();
    visited.add(0 + " " + 1);
    
    for (int level = 0; !queue.isEmpty(); level++) {
        for(int k = queue.size(); k > 0; k--) {
            int[] cur = queue.pollFirst();  // cur[0] is position; cur[1] is speed
            
            if (cur[0] == target) {
                return level;
            }
            
            int[] nxt = new int[] {cur[0] + cur[1], cur[1] << 1};  // accelerate instruction
            String key = (nxt[0] + " " + nxt[1]);
            
            if (!visited.contains(key) && 0 < nxt[0] && nxt[0] < (target << 1)) {
                queue.offerLast(nxt);
                visited.add(key);
            }
            
            nxt = new int[] {cur[0], cur[1] > 0 ? -1 : 1};  // reverse instruction
            key = (nxt[0] + " " + nxt[1]);
            
            if (!visited.contains(key) && 0 < nxt[0] && nxt[0] < (target << 1)) {
                queue.offerLast(nxt);
                visited.add(key);
            }
        }
    }
    
    return -1;
}
```

<br>

Top-down DP:

```
public int racecar(int target) {
    int[] dp = new int[target + 1];
    Arrays.fill(dp, 1, dp.length, -1);
    return racecar(target, dp);
}

private int racecar(int i, int[] dp) {
    if (dp[i] >= 0) {
        return dp[i];
    }
    
    dp[i] = Integer.MAX_VALUE;
    
    int m = 1, j = 1;
    
    for (; j < i; j = (1 << ++m) - 1) {
        for (int q = 0, p = 0; p < j; p = (1 << ++q) - 1) {
            dp[i] = Math.min(dp[i],  m + 1 + q + 1 + racecar(i - (j - p), dp));
        }
    }
    
    dp[i] = Math.min(dp[i], m + (i == j ? 0 : 1 + racecar(j - i, dp)));
    
    return dp[i];
}
```

<br>

Bottom-up DP:

```
public int racecar(int target) {
    int[] dp = new int[target + 1];
    
    for (int i = 1; i <= target; i++) {
        dp[i] = Integer.MAX_VALUE;
        
        int m = 1, j = 1;
        
        for (; j < i; j = (1 << ++m) - 1) {
            for (int q = 0, p = 0; p < j; p = (1 << ++q) - 1) {
                dp[i] = Math.min(dp[i], m + 1 + q + 1 + dp[i - (j - p)]);
            }
        }
        
        dp[i] = Math.min(dp[i], m + (i == j ? 0 : 1 + dp[j - i]));
    }
    
    return dp[target];
}
```
</p>


### [Java/C++/Python] DP solution
- Author: lee215
- Creation Date: Sun Apr 15 2018 11:04:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 26 2020 14:17:58 GMT+0800 (Singapore Standard Time)

<p>
# Example
For the input 5, we can reach with only 7 steps: AARARAA. Because we can step back.
<br>

# Explanation
Let\'s say `n` is the length of `target` in binary and  we have `2 ^ (n - 1) <= target < 2 ^ n`
We have 2 strategies here:

**1. Go pass our target , stop and turn back**
We take `n` instructions of `A`.
`1 + 2 + 4 + ... + 2 ^ (n-1) = 2 ^ n - 1`
Then we turn back by one `R` instruction.
In the end, we get closer by `n + 1` instructions.

**2. Go as far as possible before pass target, stop and turn back**
We take `n - 1` instruction of `A` and one `R`.
Then we take `m` instructions of `A`, where `m < n`
<br>

# Complexity
Time `O(TlogT)`
Space `O(T)`
<br>

**Java**:
```java
    int[] dp = new int[10001];
    public int racecar(int t) {
        if (dp[t] > 0) return dp[t];
        int n = (int)(Math.log(t) / Math.log(2)) + 1;
        if (1 << n == t + 1) {
            dp[t] = n;
        } else {
            dp[t] = racecar((1 << n) - 1 - t) + n + 1;
            for (int m = 0; m < n - 1; ++m) {
                dp[t] = Math.min(dp[t], racecar(t - (1 << (n - 1)) + (1 << m)) + n + m + 1);
            }
        }
        return dp[t];
    }
```
**C++:**
```cpp
    int dp[10001];
    int racecar(int t) {
        if (dp[t] > 0) return dp[t];
        int n = floor(log2(t)) + 1, res;
        if (1 << n == t + 1) dp[t] = n;
        else {
            dp[t] = racecar((1 << n) - 1 - t) + n + 1;
            for (int m = 0; m < n - 1; ++m)
                dp[t] = min(dp[t], racecar(t - (1 << (n - 1)) + (1 << m)) + n + m + 1);
        }
        return dp[t];
    }
```

**Python**
```py
    dp = {0: 0}
    def racecar(self, t):
        if t in self.dp:
            return self.dp[t]
        n = t.bit_length()
        if 2**n - 1 == t:
            self.dp[t] = n
        else:
            self.dp[t] = self.racecar(2**n - 1 - t) + n + 1
            for m in range(n - 1):
                self.dp[t] = min(self.dp[t], self.racecar(t - 2**(n - 1) + 2**m) + n + m + 1)
        return self.dp[t]
```
</p>


### Accepted Java solution with BFS
- Author: AdaZhang
- Creation Date: Sun Apr 15 2018 12:02:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 00:34:57 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    class CarInfo{
        int pos, speed;
        public CarInfo(int p, int s) {
            pos = p;
            speed = s;
        }
    }
    public int racecar(int target) {
        Set<String> visited = new HashSet<>();
        String begin = 0 + "/" + 1;
        visited.add(begin);
        Queue<CarInfo> queue = new LinkedList<>();
        queue.add(new CarInfo(0,1));
        int level = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for(int i = 0; i < size; i++) {
                CarInfo cur = queue.poll();
                if (cur.pos == target) return level;
                String s1 = (cur.pos + cur.speed) + "/" + (cur.speed * 2);
                String s2 = cur.pos + "/" + (cur.speed > 0 ? -1 : 1);
                if (Math.abs(cur.pos + cur.speed - target) < target && !visited.contains(s1)) {
                    visited.add(s1);
                    queue.add(new CarInfo(cur.pos + cur.speed, cur.speed * 2));
                }
                if (Math.abs(cur.pos - target) < target && !visited.contains(s2)) {
                    visited.add(s2);
                    queue.add(new CarInfo(cur.pos, cur.speed > 0 ? -1 : 1));
                }
            }
            
            level++;
        }
        return -1;
    }

}
```
</p>


