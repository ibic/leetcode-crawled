---
title: "Binary Tree Maximum Path Sum"
weight: 124
#id: "binary-tree-maximum-path-sum"
---
## Description
<div class="description">
<p>Given a <strong>non-empty</strong> binary tree, find the maximum path sum.</p>

<p>For this problem, a path is defined as any sequence of nodes from some starting node to any node in the tree along the parent-child connections. The path must contain <strong>at least one node</strong> and does not need to go through the root.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> [1,2,3]

       <strong>1</strong>
      <strong>/ \</strong>
     <strong>2</strong>   <strong>3</strong>

<strong>Output:</strong> 6
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> [-10,9,20,null,null,15,7]

&nbsp;  -10
&nbsp; &nbsp;/ \
&nbsp; 9 &nbsp;<strong>20</strong>
&nbsp; &nbsp; <strong>/ &nbsp;\</strong>
&nbsp; &nbsp;<strong>15 &nbsp; 7</strong>

<strong>Output:</strong> 42
</pre>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Facebook - 37 (taggedByAdmin: false)
- Amazon - 11 (taggedByAdmin: false)
- Google - 8 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: true)
- ByteDance - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Walmart Labs - 3 (taggedByAdmin: false)
- Intuit - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)
- AppDynamics - 2 (taggedByAdmin: false)
- Baidu - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Recursion

**Intuition**

First of all, let's simplify the problem and implement a function 
`max_gain(node)`
which takes a node as an argument and computes a maximum contribution
that this node and one/zero of its subtrees could add.  

> In other words, it's a _maximum gain_ one could have including 
the node (and maybe one of its subtrees) into the path.

![gains](../Figures/124/124_gains.png)

Hence if one would know for sure that the max path contains `root`, 
the problem would be solved as `max_gain(root)`.
Unfortunately, _the max path does not need to go through the root_, and
here is an example of such a tree

![gains](../Figures/124/124_max_path.png)

That means one needs to modify the above function and to check at
each step what is better : to continue the current path or
to start a new path with the current node as a highest node
in this new path. 

**Algorithm**

Now everything is ready to write down an algorithm.

* Initiate `max_sum` as the smallest possible integer and 
call `max_gain(node = root)`.
* Implement `max_gain(node)` 
with a check to continue the old path/to start a new path:
    * Base case : if node is null, the max gain is `0`.
    * Call `max_gain` recursively for the node children to
    compute max gain from the left and right subtrees :
    `left_gain = max(max_gain(node.left), 0)` and  
    `right_gain = max(max_gain(node.right), 0)`.
    * Now check to continue the old path or to start a new path.
    To start a new path would cost 
    `price_newpath = node.val + left_gain + right_gain`.
    Update `max_sum` if it's better to start a new path.
    * For the recursion return the max gain 
    the node and one/zero of its subtrees could add 
    to the current path : 
    `node.val + max(left_gain, right_gain)`.       

**Tree Node**

Here is the definition of the `TreeNode` which we would use
in the following implementation.

<iframe src="https://leetcode.com/playground/UwSQTvV8/shared" frameBorder="0" width="100%" height="225" name="UwSQTvV8"></iframe>

**Implementation**

!?!../Documents/124_LIS.json:1000,494!?!

<iframe src="https://leetcode.com/playground/mvwsxCCt/shared" frameBorder="0" width="100%" height="500" name="mvwsxCCt"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, where `N` is number of nodes,
since we visit each node not more than 2 times.

* Space complexity: $$\mathcal{O}(H)$$, where $$H$$ is a tree height, 
to keep the recursion stack. In the average case of balanced tree, the tree height
$$H = \log N$$, in the worst case of skewed tree, $$H = N$$.

## Accepted Submission (python3)
```python3
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def maxPathSum(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        def maxTill(node, me):
            if not node:
                return 0
            left = maxTill(node.left, me)
            right = maxTill(node.right, me)
            r = max(0, max(left, right)) + node.val
            total = node.val
            if left > 0:
                total += left
            if right > 0:
                total += right
            if total > me.maxSum:
                me.maxSum = total
            return r
        
        self.maxSum = float('-inf')
        maxTill(root, self)
        return self.maxSum
```

## Top Discussions
### Accepted short solution in Java
- Author: wei-bung
- Creation Date: Sat Oct 25 2014 16:36:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 12:18:36 GMT+0800 (Singapore Standard Time)

<p>
Here's my ideas:

 - A path from start to end, goes up on the tree for 0 or more steps, then goes down for 0 or more steps. Once it goes down, it can't go up. Each path has a highest node, which is also the lowest common ancestor of all other nodes on the path.
 - A recursive method `maxPathDown(TreeNode node)` (1) computes the maximum path sum with highest node is the input node, update maximum if necessary (2) returns the maximum sum of the path that can be extended to input node's parent.

Code:

    public class Solution {
        int maxValue;
        
        public int maxPathSum(TreeNode root) {
            maxValue = Integer.MIN_VALUE;
            maxPathDown(root);
            return maxValue;
        }
        
        private int maxPathDown(TreeNode node) {
            if (node == null) return 0;
            int left = Math.max(0, maxPathDown(node.left));
            int right = Math.max(0, maxPathDown(node.right));
            maxValue = Math.max(maxValue, left + right + node.val);
            return Math.max(left, right) + node.val;
        }
    }
</p>


### [Python] Recursion stack thinking process diagram
- Author: arkaung
- Creation Date: Thu Apr 30 2020 00:01:54 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 30 2020 09:04:07 GMT+0800 (Singapore Standard Time)

<p>
This problem requires quite a bit of quirky thinking steps. Take it slow until you fully grasp it.

# **Basics**
![image](https://assets.leetcode.com/users/arkaung/image_1588177330.png)


# **Base cases**
![image](https://assets.leetcode.com/users/arkaung/image_1588177335.png)


# **Important Observations**
* These important observations are very important to understand `Line 9` and `Line 10` in the code.
	* For example, in the code (`Line 9`), we do something like `max(get_max_gain(node.left), 0)`. The important part is: why do we take maximum value between 0 and maximum gain we can get from left branch? Why 0?
	* Check the two images below first.
![image](https://assets.leetcode.com/users/arkaung/image_1588177343.png)
![image](https://assets.leetcode.com/users/arkaung/image_1588177349.png)

* The important thing is "We can only get any sort of gain IF our branches are not below zero. If they are below zero, why do we even bother considering them? Just pick 0 in that case. Therefore, we do `max(<some gain we might get or not>, 0)`.

# **Going down the recursion stack for one example**
![image](https://assets.leetcode.com/users/arkaung/image_1588177356.png)
![image](https://assets.leetcode.com/users/arkaung/image_1588177362.png)
![image](https://assets.leetcode.com/users/arkaung/image_1588177368.png)

* Because of this, we do `Line 12` and `Line 13`. It is important to understand the different between looking for the maximum path INVOLVING the current node in process and what we return for the node which starts the recursion stack. `Line 12` and `Line 13` takes care of the former issue and `Line 15` (and the image below) takes care of the latter issue.

![image](https://assets.leetcode.com/users/arkaung/image_1588177373.png)

* Because of this fact, we have to return like `Line 15`. For our example, for node 1, which is the recursion call that node 3 does for `max(get_max_gain(node.left), 0)`, node 1 cannot include both node 6 and node 7 for a path to include node 3. Therefore, we can only pick the max gain from left path or right path of node 1.


**Python**
``` python
1. class Solution:
2.     def maxPathSum(self, root: TreeNode) -> int:
3. 		max_path = float("-inf") # placeholder to be updated
4. 		def get_max_gain(node):
5. 			nonlocal max_path # This tells that max_path is not a local variable
6. 			if node is None:
7. 				return 0
8. 				
9. 			gain_on_left = max(get_max_gain(node.left), 0) # Read the part important observations
10. 		gain_on_right = max(get_max_gain(node.right), 0)  # Read the part important observations
11. 			
12. 		current_max_path = node.val + gain_on_left + gain_on_right # Read first three images of going down the recursion stack
13. 		max_path = max(max_path, current_max_path) # Read first three images of going down the recursion stack
14. 			
15. 		return node.val + max(gain_on_left, gain_on_right) # Read the last image of going down the recursion stack
16. 			
17. 			
18. 	get_max_gain(root) # Starts the recursion chain
19. 	return max_path		
```
</p>


### Elegant Java solution
- Author: jeantimex
- Creation Date: Sun Jul 05 2015 13:22:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 22:21:55 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        int max = Integer.MIN_VALUE;
        
        public int maxPathSum(TreeNode root) {
            helper(root);
            return max;
        }
        
        // helper returns the max branch 
        // plus current node's value
        int helper(TreeNode root) {
            if (root == null) return 0;
            
            int left = Math.max(helper(root.left), 0);
            int right = Math.max(helper(root.right), 0);
            
            max = Math.max(max, root.val + left + right);
            
            return root.val + Math.max(left, right);
        }
    }
</p>


