---
title: "Count Substrings with Only One Distinct Letter"
weight: 1038
#id: "count-substrings-with-only-one-distinct-letter"
---
## Description
<div class="description">
<p>Given a string <code>S</code>,&nbsp;return the number of substrings that have&nbsp;only <strong>one distinct</strong> letter.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> S = &quot;aaaba&quot;
<strong>Output:</strong> 8
<strong>Explanation: </strong>The substrings with one distinct letter are &quot;aaa&quot;, &quot;aa&quot;, &quot;a&quot;, &quot;b&quot;.
&quot;aaa&quot; occurs 1 time.
&quot;aa&quot; occurs 2 times.
&quot;a&quot; occurs 4 times.
&quot;b&quot; occurs 1 time.
So the answer is 1 + 2 + 4 + 1 = 8.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> S = &quot;aaaaaaaaaa&quot;
<strong>Output:</strong> 55
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= S.length &lt;= 1000</code></li>
	<li><code>S[i]</code> consists of only lowercase English letters.</li>
</ul>

</div>

## Tags
- Math (math)
- String (string)

## Companies
- Virtu Financial - 4 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python 3 Easy Math Method 24ms O(1) Memory with Explanation Beats 99.06%
- Author: hjt486
- Creation Date: Thu Nov 21 2019 14:10:13 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Nov 21 2019 14:31:24 GMT+0800 (Singapore Standard Time)

<p>
This is quite self-explanatory, but I\'ll try my best to explain, as you can see from the example:
S = "aaaba", For every continuous letters like \'aaa\', you can calculate as:

\'a\' for 3 times,
\'aa\' for 2 times,
\'a\' for 1 time.

so you only need to know how many of these continuous letters are, and then for each of them, calculate the sum by adding from 1 to the length of the continuous letters, for \'aaa\' it\'s 1+2+3=6. In our example, there are \'aaa\', \'b\' and \'a\', so it\'s (1+2+3) + 1 + 1.

At the beginning of our code, to make the code easier to write without considering too many edge cases, I concatenate two spaces at the beginning and the end of the string.
 
Then we have two variables, one as \'total\' to hold the sum, and one \'count\'  as the counter for the continuous letters. If we found i-th letter is not same as the i-1th letter, we reset the counter to 1, otherwise they\'re the same, we add 1 to the counter. For each time, we simply add the counter value to the total, then at the end we return \'total\'.
 
```
class Solution:
    def countLetters(self, S: str) -> int:
        S = \' \'+ S + \' \'
        total, count = 0, 1
        for i in range(1, len(S)-1):
            if S[i] != S[i-1]:
                count = 1
            else:
                count += 1 
            total += count
        return total
```
</p>


### [Java] Simple O(n) code brief explanation and analysis.
- Author: rock
- Creation Date: Sun Sep 08 2019 00:35:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 00:58:46 GMT+0800 (Singapore Standard Time)

<p>
For any repeating chars `S = "aaaaa"`, we can choose its lower and upper bounds from [0, S.length()) to get a substring. Therefore, we have C(S.length(), 2) combinations, and hence get the following algorithm:
**Loop through `S` and count the consecutive repeating chars, then sum all of the corresponding combinations.**
```
    public int countLetters(String S) {
        int ans = 0, repeat = 1;
        for (int i = 1; i < S.length(); ++i, ++repeat) {
            if (S.charAt(i) != S.charAt(i - 1)) { // previous char consectively occurs \'repeat\' times.
                ans += repeat * (repeat + 1) / 2;
                repeat = 0;
            }
        }
        return ans + repeat * (repeat + 1) / 2;
    }
```

**Analysis:**

Time: O(n), space: O(1), where n = S.length().
</p>


### C++ O(n), two pointers
- Author: votrubac
- Creation Date: Sun Sep 08 2019 10:27:33 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 12 2019 12:34:34 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
If a letter repeats n times, it forms n * (n  + 1) / 2 valid substrings:
```
"aaaa": "a" * 4, "aa" * 3, "aaa" * 2, and "aaaa" * 1 = 4 * (4 + 1) / 2 = 10.
```
# Solution
```
int countLetters(string S, int res = 0) {
  for (auto i = 1, j = 0; i <= S.size(); ++i) {
    if (i == S.size() || S[i] != S[j]) {
        res += (i - j + 1) * (i - j) / 2;
        j = i;
    } 
  }
  return res;
}
```
Another way to solve this is to accumulate substring size as the letter repeats (starting from two), and add ```S.size()``` in the end to account for one-letter substrings.
```
"aaaa": 1 ("aa") + 2 ("aaa") + 3 ("aaaa") + "aaaa".size() = 6 + 4 = 10.
```
# Simplified Solution
```
int countLetters(string S, int res = 0) {
  for (auto i = 1, j = 0; i < S.size(); ++i) {
    if (S[i] != S[j]) j = i;
    res += i - j;  
  }
  return res + S.size();
}
```
## Complexity Analysis
Runtime: O(n)
Memory: O(1)
</p>


