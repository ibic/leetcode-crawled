---
title: "Number of Steps to Reduce a Number to Zero"
weight: 1238
#id: "number-of-steps-to-reduce-a-number-to-zero"
---
## Description
<div class="description">
<p>Given a non-negative integer <code>num</code>, return the number of steps to reduce it to zero. If the current number is even, you have to divide it by 2, otherwise, you have to subtract 1 from it.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> num = 14
<strong>Output:</strong> 6
<strong>Explanation:</strong>&nbsp;
Step 1) 14 is even; divide by 2 and obtain 7.&nbsp;
Step 2) 7 is odd; subtract 1 and obtain 6.
Step 3) 6 is even; divide by 2 and obtain 3.&nbsp;
Step 4) 3 is odd; subtract 1 and obtain 2.&nbsp;
Step 5) 2 is even; divide by 2 and obtain 1.&nbsp;
Step 6) 1 is odd; subtract 1 and obtain 0.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> num = 8
<strong>Output:</strong> 4
<strong>Explanation:</strong>&nbsp;
Step 1) 8 is even; divide by 2 and obtain 4.&nbsp;
Step 2) 4 is even; divide by 2 and obtain 2.&nbsp;
Step 3) 2 is even; divide by 2 and obtain 1.&nbsp;
Step 4) 1 is odd; subtract 1 and obtain 0.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> num = 123
<strong>Output:</strong> 12
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= num &lt;= 10^6</code></li>
</ul>

</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Microsoft - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- HRT - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Simulation

**Intuition**

The most intuitive, and easiest, solution is to simply simulate the rules and count how many steps are carried out in order to reach zero.

As an example, assume our starting value is `43`. The steps we need to take will be:

```python
43 # Odd, subtract 1
42 # Even, divide by 2
21 # Odd, subtract 1
20 # Even, divide by 2
10 # Even, divide by 2
 5 # Odd, subtract 1
 4 # Even divide by 2
 2 # Even, divide by 2
 1 # Odd, subtract 1
 0
```

This is a total of `9` steps.

Here's an animation of this same algorithm being used with a starting value of `211`.

!?!../Documents/1342_simulation_animation.json:960,250!?!

**Algorithm**

The algorithm works by simulating each step of the rules; if the current number is even then divide it by `2`. Else if it's odd, subtract `1` from it. Each time we perform one of these actions, we increment the steps we've taken by `1` so that we can return it at the end.

To check if a number is even or odd, we can use the *modulus* (`%`) operator. Recall that the *modulus* operator gives us the *remainder* when we divide two numbers. If `number % 2` is `1`, then we know `number` must be **odd**. Otherwise, the only other value it could be is `0`, which means `number` must be **even**.

<iframe src="https://leetcode.com/playground/Bk3NkxYu/shared" frameBorder="0" width="100%" height="259" name="Bk3NkxYu"></iframe>

**Complexity Analysis**

Let $$n = num$$.

- Time Complexity : $$O(\log \, n)$$.

    At each step, what we did depended on whether the remaining $$num$$ was odd or even. If $$num$$ was even, we *halved* what was left. If it was odd, we only subtracted $$1$$. *However*, by subtracting $$1$$, we were making it *even*, and so on the next step we were *guaranteed* to halve it. 
    
    What this means is that in the worst case, we're halving it on every *second* step. We treat the $$\frac{1}{2}$$ of the time as a constant though, so in essence, we say that at each step, $$num$$ is being halved.

    When something is halved at every step, it has a $$O(\log \, n)$$ time complexity.

- Space Complexity : $$O(1)$$.

    We only use a constant number of integer variables, and so the space complexity is $$O(1)$$.

It's impossible for us to do better than a time complexity of $$O(\log \, n)$$—unless we were willing to hardcode all 1 million possible cases we could be given (the problem statement says `0 <= num <= 10^6`). But we really don't want to do that! By the end of this article, you'll be able to see why it's *impossible* for an algorithm to do better.

</br>

---

#### Approach 2: Counting Bits

*Note: Approach 2 and 3 don't change the time complexity, but they offer a different way of thinking about the problem that studying will hopefully help you expand your problem solving skills! A prerequisite for these last 2 approaches is knowing how numbers are represented in binary.*

At each step, we either subtract `1` from `num`, or we divide `num` by `2`. In binary, these two operations do something very simple, but very interesting, to a number!

Recall that odd numbers always have a last bit of `1`. Subtracting `1`, *from an odd number*, **changes the last bit** from `1` to `0`.

![Showing 53 - 1 changes the last bit to zero](../Figures/1342/subtract_1_from_odd.png)

Dividing by `2` **removes the last bit** from the number.

![Showing 52 / 2 removes the last bit](../Figures/1342/divide_by_2.png)

For example, have a look at the binary representation of `210` as it's reduced to zero.

!?!../Documents/1342_simulation_binary_animation.json:960,250!?!

The bits slid along, and each became the "last" bit. Notice how the `0`s took **one** step to remove, and the `1`s took **two** steps to remove.

This means that we could simply analyze the binary representation of the starting `num` to determine the number of steps needed to reduce it.

So, to get our answer, we can just add two steps for every `1`, and add one step for every `0`, for each bit in the binary representation.

![Showing the bits of a number map to either one or two steps](../Figures/1342/bits_to_steps.png)

There's one thing to be careful of, and that is not inadvertently counting the last bit as two steps. The last bit to remove will *always* be a `1`—it was the most significant bit in the original `num`. The algorithm above would add *2* for removing this final `1`. But actually, when we subtract `1` from it, it goes to zero. So we don't need add two steps for this bit. The simplest way of handling this case is to subtract `1` from our *final* `steps` count, as we know this "off-by-one-error" will always happen (except when the initial `num` is `0`, we need to be careful of that edge case too!).

Let's look at another example. The number we'll use is `78`; this can be written in binary as `1001110`. The binary contains four `1`s and three `0`s, so our total number of steps must be `(4 * 2) + (3 * 1) - 1 = 10`. This the correct result!

**Algorithm**

To count the bits we'll convert our number into a binary string, for each character if it's a `"1"` we'll add two steps, else if it's `"0"` we'll add one step.

In **Java**, we can use `Integer.toBinaryString(...)` to convert an int to binary. The binary number is represented as `String`.

In **Python**, we can use `bin(...)` to convert an int to binary. Like in Java, the binary number is represented as a `str`. However, it also contains `0b` on the start—this is simply a code to say the `str` is a binary number. The "pythonic" thing to do is chop these two characters off with a list splice. i.e., to get the binary for `num`, you would do `bin(num)[2:]`.

<iframe src="https://leetcode.com/playground/x7j2EwiD/shared" frameBorder="0" width="100%" height="361" name="x7j2EwiD"></iframe>

In Python, we can do this really elegantly using the `string.count(...)` and `len(...)` functions.

<iframe src="https://leetcode.com/playground/xVBPXgVb/shared" frameBorder="0" width="100%" height="140" name="xVBPXgVb"></iframe>


**Complexity Analysis**

Let $$n = num$$.

- Time Complexity : $$O(\log \, n)$$.

    Converting a number into string can be done in $$\log \, n$$ time.

    We then loop over each bit, doing a single operation each time. The number of bits in a number is $$\log_2 \, number$$, so the time complexity is $$O(\log \, n)$$.

- Space Complexity : $$O(\log \, n)$$.

    Because we convert the number into a string, we'll have $$\log_2 \, number$$ characters in our string. This gives us a space complexity of $$O(\log \, n)$$.

</br>

---

#### Approach 3: Counting Bits with Bitwise Operators

In Approach 2, we needed to convert the number into a string representation. Strings are considerably larger than the integer they represent though. Another way of inspecting bits, to check if they're `1` or `0`, is to use the bitwise-and (`&`) operator.

The result of `a & b` (`a` bitwise-and `b`) looks at each bit in both `a` and `b` at the same time. If both bits are `1` then bitwise-and sets the same bit of the result to `1`, but if either are `0` it sets the bit to `0`.

For example, `109` and `57` can be written as `1101101` and `111001` respectively. This image shows what happens when we bitwise-and them.

![Bitwise and of 109 and 57 showing result](../Figures/1342/bitwise_and.png)

So, to actually inspect a specific bit, we can use a number that has a `1` followed by enough `0`s to put the `1` at the position we want it (we commonly call this a "bitmask"). With this number, we bitwise-and (`&`) it with the input number. If the input number has a `1` at the same position, it'll output `1` at that position, and because all other numbers are `0` they will be `0` in the output as well.

These numbers of the form `1`, followed by some number of `0`s, are actually just the powers of two, where the power is the number of `0`s after the one. As such, we can check if a bit is a `1` in a number by doing `num & (1 << bit)` where `bit` is the bit we want to check (0-indexed from the right).

For example, let's check if the *sixth* bit of `109` is a `1`.

![Bitwise and of 109 and 32 showing result of 32](../Figures/1342/check_sixth_digit.png)

The output is `100000`, which *is not* zero. Therefore, we know that the sixth bit has to be a `1`.

Let's also check if the the fifth bit of `109` is a `1`.

![Bitwise and of 109 and 16 showing result of 0](../Figures/1342/check_fifth_digit.png)

The output is `0000000`, which *is* zero. Therefore, we know that the fifth bit must be a `0`.

Just like Approach 2, we look at each bit, and if it's a `1` we add `2` to `steps`, otherwise if it's a `0`, we add `1` to `steps`.

**Algorithm**

Unlike the previous approach, this approach won't work correctly when `num = 0` is the input. The previous approach did an iteration for the lone `0` bit as it was in the string, but for this approach the loop won't run at all. `-1` will then be returned because of the `steps - 1`. The solution is to check for `num == 0` at the start and `return 0` if it is detected.

<iframe src="https://leetcode.com/playground/Mx39FfpK/shared" frameBorder="0" width="100%" height="378" name="Mx39FfpK"></iframe>

**Complexity Analysis**

Let $$n = num$$.

- Time Complexity : $$O(\log \, n)$$.

    We're pulling out each of the $$\log \, n$$ bits from `num` and performing an $$O(1)$$ operation on each one. Therefore, the total time complexity is, again, $$O(\log \, n)$$.

- Space Complexity : $$O(1)$$.

    We only use a constant number of integer variables, and so the space complexity is $$O(1)$$.

</br>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### just count number of 0 and 1 in binary
- Author: xzj104
- Creation Date: Sun Feb 09 2020 00:24:39 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 10 2020 23:05:08 GMT+0800 (Singapore Standard Time)

<p>
For the binary representation from right to left(until we find the leftmost 1):
if we meet 0, result += 1 because we are doing divide;
if we meet 1, result += 2 because we first do "-1" then do a divide;
ony exception is the leftmost 1, we just do a "-1" and it becomse 0 already.
```
int numberOfSteps (int num) {
		if(!num) return 0;
        int res = 0;
        while(num) {
            res += (num & 1) ? 2 : 1;
            num >>= 1;
        }
        return res - 1;
    }
```
</p>


### C++ O(1) solution without iteration/recursion
- Author: Errichto
- Creation Date: Thu Feb 13 2020 16:47:58 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Feb 13 2020 16:47:58 GMT+0800 (Singapore Standard Time)

<p>
The answer depends on the number of ones and the length of a binary representation - you can get this in O(1) by counting leading zeros.
```
return num ? __builtin_popcount(num) + 31 - __builtin_clz(num) : 0;
```
</p>


### Clean Python 3, count bits in 2 lines
- Author: lenchen1112
- Creation Date: Sun Feb 09 2020 00:01:08 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 25 2020 18:27:53 GMT+0800 (Singapore Standard Time)

<p>
Count each `0` as 1.
Count each `1` as 2 except the first `1`.
Time: `O(bits of num)`
Space: `O(bits of num)`
However num is bounded in 0 <= num <= 10^6, so time and space are both O(1) in this problem.
```
class Solution:
    def numberOfSteps (self, num: int) -> int:
        digits = f\'{num:b}\'
        return digits.count(\'1\') - 1 + len(digits)
```
</p>


