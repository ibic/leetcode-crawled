---
title: "Strange Printer II"
weight: 1437
#id: "strange-printer-ii"
---
## Description
<div class="description">
<p>There is a strange printer with the following two special requirements:</p>

<ul>
	<li>On each turn, the printer will print a solid rectangular pattern of a single color on the grid. This will cover up the existing colors in the rectangle.</li>
	<li>Once the printer has used a color for the above operation, <strong>the same color cannot be used again</strong>.</li>
</ul>

<p>You are given a <code>m x n</code> matrix <code>targetGrid</code>, where <code>targetGrid[row][col]</code> is the color in the position <code>(row, col)</code> of the grid.</p>

<p>Return <code>true</code><em> if it is possible to print the matrix </em><code>targetGrid</code><em>,</em><em> otherwise, return </em><code>false</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/08/15/sample_1_1929.png" style="width: 483px; height: 138px;" /></p>

<pre>
<strong>Input:</strong> targetGrid = [[1,1,1,1],[1,2,2,1],[1,2,2,1],[1,1,1,1]]
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/08/15/sample_2_1929.png" style="width: 483px; height: 290px;" /></p>

<pre>
<strong>Input:</strong> targetGrid = [[1,1,1,1],[1,1,3,3],[1,1,3,4],[5,5,1,4]]
<strong>Output:</strong> true
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> targetGrid = [[1,2,1],[2,1,2],[1,2,1]]
<strong>Output:</strong> false
<strong>Explanation:</strong> It is impossible to form targetGrid because it is not allowed to print the same color in different turns.</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> targetGrid = [[1,1,1],[3,1,3]]
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m == targetGrid.length</code></li>
	<li><code>n == targetGrid[i].length</code></li>
	<li><code>1 &lt;= m, n &lt;= 60</code></li>
	<li><code>1 &lt;= targetGrid[row][col] &lt;= 60</code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Straight Forward
- Author: lee215
- Creation Date: Sun Sep 20 2020 00:05:18 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 24 2020 14:18:19 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
For each color, find its index of left most, top most, right most, bottom most point.
Then we need to paint this color from [top, left] to [bottom, right].

If in the rectangle, all the colors are either the same or 0,
we mark all of them to 0.

If we can mark the whole grid to 0, it means the target if printable.
<br>

# **Complexity**
Time `O(CCMN)`
Space `O(4C)`
<br>

**Python:**
```py
    def isPrintable(self, A):
        m, n = len(A), len(A[0])
        pos = [[m, n, 0, 0] for i in xrange(61)]
        colors = set()
        for i in xrange(m):
            for j in xrange(n):
                c = A[i][j]
                colors.add(c)
                pos[c][0] = min(pos[c][0], i)
                pos[c][1] = min(pos[c][1], j)
                pos[c][2] = max(pos[c][2], i)
                pos[c][3] = max(pos[c][3], j)

        def test(c):
            for i in xrange(pos[c][0], pos[c][2] + 1):
                for j in xrange(pos[c][1], pos[c][3] + 1):
                    if A[i][j] > 0 and A[i][j] != c:
                        return False
            for i in xrange(pos[c][0], pos[c][2] + 1):
                for j in xrange(pos[c][1], pos[c][3] + 1):
                    A[i][j] = 0
            return True

        while colors:
            colors2 = set()
            for c in colors:
                if not test(c):
                    colors2.add(c)
            if len(colors2) == len(colors):
                return False
            colors = colors2
        return True
```

</p>


### C++ O(n^3) solution, checking cycle in dependency graph
- Author: blackskygg
- Creation Date: Sun Sep 20 2020 00:01:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 20 2020 00:01:41 GMT+0800 (Singapore Standard Time)

<p>
**Idea**: Instead of thinking how to gradually print colors, we can think of the problem reversely and see this as a process of **removing colors** from the target grid to get a blank grid. In this process, a color rectangle can only be removed if and only if all the color rectangles \u201Ccovering\u201D it are removed first. We can model this dependency relationship between colors as a directed graph. The target can be achieved if and only if the graph doesn\u2019t have any cycles, which can be easily checked by using a standard DFS. 

```
class Solution {
public:
    bool isPrintable(vector<vector<int>>& targetGrid) {
        const int m = targetGrid.size(), n = targetGrid[0].size();
        // Dependency graph: dep[i] = j means color j covers color i;
        vector<unordered_set<int>> dep(61);
        for (int i = 1; i <= 60; ++i) {
            // Determine the rectangle of the current color i.
            int minx = m, miny = n, maxx = -1, maxy = -1;
            for (int x = 0; x < m; ++x) {
                for (int y = 0; y < n; ++y) {
                    if (targetGrid[x][y] == i) {
                        minx = min(x, minx);
                        miny = min(y, miny);
                        maxx = max(x, maxx);
                        maxy = max(y, maxy);
                    }
                }
            }
            // Add any color covering the current color as dependence.
            for (int tx = minx; tx <= maxx; ++tx) {
                for (int ty = miny; ty <= maxy; ++ty) {
                   if (targetGrid[tx][ty] != i) dep[i].insert(targetGrid[tx][ty]);
                }
            }
        }
        // Standard DFS to check the existence of a cycle.
        vector<int> vis(61, 0);
        for (int i = 1; i <= 60; ++i) {
            if (!vis[i] && hasCircle(i, dep, vis)) return false;
        }
        return true;
    }
    
private:
    bool hasCircle(int curr, const vector<unordered_set<int>>& dep, vector<int>& vis) {
        vis[curr] = 1;
        for (const int d : dep[curr]) {
            if (vis[d] == 2) continue;
            if (vis[d] == 1) return true;
            if (hasCircle(d, dep, vis)) return true;
        }
        vis[curr] = 2;
        return false;
    }
};
```
</p>


### [Java]Topological Sort
- Author: DCXiaoBing
- Creation Date: Sun Sep 20 2020 00:10:20 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 30 2020 23:37:26 GMT+0800 (Singapore Standard Time)

<p>
Well, this is a straight forward solution:

1. if `color a` contains `color b`, then we must fill `color a` then `color b`. This is pretty similar to `to finish course b, you need to finish course a first.`
2. So we can build a graph: `color a` pointed by all other colors contained in `color a`\'s rectangle.
3. topological sort(or dfs) to check whether we can reach all node. (Please refer to the idea behind [207. Course Schedule](https://leetcode.com/problems/course-schedule/))



```
class Solution {
    // same color should form a rectangle
    // topological sort
    // if a rectangle contains another one, then there is an edge between these two color
    
    public boolean isPrintable(int[][] targetGrid) {
        List<List<Integer>> graph = new ArrayList<>();
        int[] inDegree = new int[61];
        for(int i = 0; i <= 60; i++) graph.add(new ArrayList<>());
        for(int i = 1; i <= 60; i++) search(targetGrid, i, graph, inDegree);
        
        Deque<Integer> zeros = new ArrayDeque<>();
        HashSet<Integer> seen = new HashSet<>();
        for(int i = 0; i < inDegree.length; i++) if(inDegree[i] == 0) zeros.add(i);
        
        while(!zeros.isEmpty()) {
            int cur = zeros.poll();
            if(!seen.add(cur)) continue;
            
            for(Integer nbh : graph.get(cur)) {
                inDegree[nbh]--;
                if(inDegree[nbh] == 0) zeros.add(nbh);
            }
        }
        return seen.size() == 61;
    }
    
    private void search(int[][] grid, int color, List<List<Integer>> graph, int[] inDegree) {
        // get range
        int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE, maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;
        for(int i = 0; i < grid.length; i++) for(int j = 0; j < grid[0].length; j++) {
            if(grid[i][j] == color) {
                minX = Math.min(minX, i);
                maxX = Math.max(maxX, i);
                minY = Math.min(minY, j);
                maxY = Math.max(maxY, j);
            }
        }
        
        if(minX == Integer.MAX_VALUE) return ;
        
        // check relations
        for(int i = minX; i <= maxX; i++) for(int j = minY; j <= maxY; j++) {
            if(grid[i][j] != color) {
                graph.get(grid[i][j]).add(color); // to paint current color, we need to paint color in grid[i][j] first
                inDegree[color]++;
            }
        }
    }
}
```
</p>


