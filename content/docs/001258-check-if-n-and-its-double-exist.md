---
title: "Check If N and Its Double Exist"
weight: 1258
#id: "check-if-n-and-its-double-exist"
---
## Description
<div class="description">
<p>Given an array <code>arr</code> of integers, check if there exists two integers <code>N</code> and <code>M</code> such that <code>N</code> is the double of <code>M</code> ( i.e. <code>N = 2 * M</code>).</p>

<p>More formally check if there exists&nbsp;two indices <code>i</code> and <code>j</code> such that :</p>

<ul>
	<li><code>i != j</code></li>
	<li><code>0 &lt;= i, j &lt; arr.length</code></li>
	<li><code>arr[i] == 2 * arr[j]</code></li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [10,2,5,3]
<strong>Output:</strong> true
<strong>Explanation:</strong> N<code> = 10</code> is the double of M<code> = 5</code>,that is, <code>10 = 2 * 5</code>.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [7,1,14,11]
<strong>Output:</strong> true
<strong>Explanation:</strong> N<code> = 14</code> is the double of M<code> = 7</code>,that is, <code>14 = 2 * 7</code>.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [3,1,7,11]
<strong>Output:</strong> false
<strong>Explanation:</strong> In this case does not exist N and M, such that N = 2 * M.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= arr.length &lt;= 500</code></li>
	<li><code>-10^3 &lt;= arr[i] &lt;= 10^3</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] HashSet w/ analysis.
- Author: rock
- Creation Date: Sun Feb 09 2020 12:01:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 18 2020 02:02:18 GMT+0800 (Singapore Standard Time)

<p>
**Q & A**:
Q: Can you please explain this line ?
```
if (seen.contains(2 * i) || i % 2 == 0 && seen.contains(i / 2))
```

A: `i` is half or 2 times of a number in `seen`.

**end of Q & A**

----

```java
    public boolean checkIfExist(int[] arr) {
        Set<Integer> seen = new HashSet<>();   
        for (int i : arr) {
            if (seen.contains(2 * i) || i % 2 == 0 && seen.contains(i / 2))
                return true;
            seen.add(i);
        }
        return false;
    }
```
```python
    def checkIfExist(self, arr: List[int]) -> bool:
        seen = set()
        for i in arr:
          # if 2 * i in seen or i % 2 == 0 and i // 2 in seen:
            if 2 * i in seen or i / 2 in seen: # credit to @PeterBohai
                return True
            seen.add(i)
        return False
```

**Analysis:**

Time & space: O(n), n = arr.length.
</p>


### JAVA - Easy 5 Line Solution with EXPLANATION
- Author: anubhavjindal
- Creation Date: Mon Feb 10 2020 14:07:39 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 10 2020 14:07:39 GMT+0800 (Singapore Standard Time)

<p>
HashSet provides O(1) lookup, so it ideal for this question. For each number in the array we check, if we have already seen it\'s half or it\'s double.
**NOTE:** When checking half, we need to ensure that the current number is even, else we will get wrong anwer like in the case of 3 and 7 being in the input. Here for 7, 7/2 would give 3 (not 3.5) which is present in the HashSet but not what we need. 
```
public boolean checkIfExist(int[] arr) {
	HashSet<Integer> set = new HashSet<>();
	for(int a : arr) {
		if(set.contains(a*2) || (a%2 == 0 && set.contains(a/2))) return true;
		set.add(a);
	}
	return false;
}
```
</p>


### Python3 Counter, O(n) 100%
- Author: mudin
- Creation Date: Sun Feb 09 2020 13:36:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 09 2020 13:36:36 GMT+0800 (Singapore Standard Time)

<p>
count all numbers and store in counter,
1. if there are multiple zero return True
2. check each number\'s double in counter, if it is at least one, return True,
3. return False if above conditions fail
```
class Solution:
  def checkIfExist(self, arr: List[int]) -> bool:
    
    s = collections.Counter(arr)
    
    #check if there are more than one zeros
    if(s[0]>1): return True;
    
    
    for num in arr:
      if s[2*num] and num!=0:
        return True
    return False
```
</p>


