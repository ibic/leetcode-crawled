---
title: "2 Keys Keyboard"
weight: 582
#id: "2-keys-keyboard"
---
## Description
<div class="description">
<p>Initially on a notepad only one character &#39;A&#39; is present. You can perform two operations on this notepad for each step:</p>

<ol>
	<li><code>Copy All</code>: You can copy all the characters present on the notepad (partial copy is not allowed).</li>
	<li><code>Paste</code>: You can paste the characters which are copied <b>last time</b>.</li>
</ol>

<p>&nbsp;</p>

<p>Given a number <code>n</code>. You have to get <b>exactly</b> <code>n</code> &#39;A&#39; on the notepad by performing the minimum number of steps permitted. Output the minimum number of steps to get <code>n</code> &#39;A&#39;.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> 3
<b>Output:</b> 3
<b>Explanation:</b>
Intitally, we have one character &#39;A&#39;.
In step 1, we use <b>Copy All</b> operation.
In step 2, we use <b>Paste</b> operation to get &#39;AA&#39;.
In step 3, we use <b>Paste</b> operation to get &#39;AAA&#39;.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>The <code>n</code> will be in the range [1, 1000].</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Prime Factorization [Accepted]

**Intuition**

We can break our moves into groups of `(copy, paste, ..., paste)`.  Let `C` denote copying and `P` denote pasting.  Then for example, in the sequence of moves `CPPCPPPPCP`, the groups would be `[CPP][CPPPP][CP]`.

Say these groups have lengths `g_1, g_2, ...`.  After parsing the first group, there are `g_1` `'A'`s.  After parsing the second group, there are `g_1 * g_2` `'A'`s, and so on.  At the end, there are `g_1 * g_2 * ... * g_n` `'A'`s.

We want exactly `N = g_1 * g_2 * ... * g_n`.  If any of the `g_i` are composite, say `g_i = p * q`, then we can split this group into two groups (the first of which has one copy followed by `p-1` pastes, while the second group having one copy and `q-1` pastes).

Such a split never uses more moves: we use `p+q` moves when splitting, and `pq` moves previously.  As `p+q <= pq` is equivalent to `1 <= (p-1)(q-1)`, which is true as long as `p >= 2` and `q >= 2`.

**Algorithm**
By the above argument, we can suppose `g_1, g_2, ...` is the prime factorization of `N`, and the answer is therefore the sum of these prime factors.

<iframe src="https://leetcode.com/playground/pskPum7u/shared" frameBorder="0" width="100%" height="276" name="pskPum7u"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(\sqrt{N})$$.  When `N` is the square of a prime, our loop does $$O(\sqrt{N})$$ steps.

* Space Complexity: $$O(1)$$, the space used by `ans` and `d`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java DP Solution
- Author: LinfinityLab
- Creation Date: Sun Jul 30 2017 11:09:05 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 05:31:57 GMT+0800 (Singapore Standard Time)

<p>
```
    public int minSteps(int n) {
        int[] dp = new int[n+1];

        for (int i = 2; i <= n; i++) {
            dp[i] = i;
            for (int j = i-1; j > 1; j--) {
                if (i % j == 0) {
                    dp[i] = dp[j] + (i/j);
                    break;
                }
                
            }
        }
        return dp[n];
    }
```
</p>


### Loop best case log(n), no DP, no extra space, no recursion, with explanation
- Author: yuxiangmusic
- Creation Date: Sun Jul 30 2017 12:14:39 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 04:46:24 GMT+0800 (Singapore Standard Time)

<p>
We look for a divisor `d` so that we can make `d` copies of `(n / d)` to get `n`

The process of making `d` copies takes `d` steps (`1` step of **Copy All** and `d - 1` steps of **Paste**)

We keep reducing the problem to a smaller one in a loop.

The **best** cases occur when `n` is decreasing fast, and method is almost `O(log(n))`

For example, when `n = 1024` then `n` will be divided by `2` for only `10` iterations, which is much faster than `O(n)` DP method.

The **worst** cases occur when `n` is some multiple of large prime, e.g. `n = 997` but such cases are rare.

```
    public int minSteps(int n) {
        int s = 0;
        for (int d = 2; d <= n; d++) {
            while (n % d == 0) {
                s += d;
                n /= d;
            }
        }
        return s;
    }
```
</p>


### Very Simple Java Solution With Detail Explanation
- Author: keshavk
- Creation Date: Mon Jul 31 2017 07:11:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 10:33:35 GMT+0800 (Singapore Standard Time)

<p>
To get the DP solution, analyse the pattern first by generating first few solutions
1: 0
2: 2
3: 3
4: 4
5: 5
6: 5
7: 7
8: 6
9: 6
10: 7
11: 11
12: 7
13: 13
14: 9
15: 8

Now, check the solution.
Eg: n=6
To get 6, we need to copy 3 'A's two time. (2)
To get 3 'A's, copy the 1 'A' three times. (3)
So the answer for 6 is 5

Now, take n=9.
We need the lowest number just before 9 such that (9% number =0). So the lowest number is 3.
So 9%3=0. We need to copy 3 'A's three times to get 9. (3)
For getting 3 'A's, we need to copy 1 'A' three times. (3)
So the answer is 6

Finally to analyse the below code, take n=81.
To get 81 we check 
if (81 % 2 ==0) No
if (81 % 3 ==0) Yes
So we need to copy 81/3 = 27 'A's three times (3)
Now to get 27 'A's, we need to copy 27/3= 9 'A's three times (3)
To get 9 'A's, we need to copy 9/3=3 'A's three times (3)
And to get 3 'A's, we need to copy 3/3=1 'A's three times (3)
Final answer is 3+3+3+3 = 12

Last Example, n=18
18/2 = 9   Copy 9 'A's 2 times (2)
9/3=3 Copy 3 'A's 3 times (3)
3/3=1 Copy 1'A's 3 times (3)
Answer: 2+3+3= 8

    public int minSteps(int n) {
        int res = 0;
        for(int i=2;i<=n;i++){
            while(n%i == 0){
                res+= i;
                n=n/i;
            }
        }
        return res;
    }
</p>


