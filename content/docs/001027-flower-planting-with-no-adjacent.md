---
title: "Flower Planting With No Adjacent"
weight: 1027
#id: "flower-planting-with-no-adjacent"
---
## Description
<div class="description">
<p>You have <code>n</code> gardens, labeled from&nbsp;<code>1</code> to <code>n</code>, and an array <code>paths</code> where&nbsp;<code>paths[i] = [x<sub>i</sub>, y<sub>i</sub>]</code> describes the existence of a bidirectional path from garden <code>x<sub>i</sub></code> to garden <code>y<sub>i</sub></code>. In each garden, you want to plant one of 4 types of flowers.</p>

<p>There is no garden that has more than three paths coming into or leaving it.</p>

<p>Your task is to choose a flower type for each garden such that,&nbsp;for any two gardens connected by a path, they have different types of flowers.</p>

<p>Return <strong>any</strong> such a choice as an array <code>answer</code>, where&nbsp;<code>answer[i]</code> is the type of flower&nbsp;planted in the <code>(i+1)<sup>th</sup></code> garden.&nbsp; The flower types are denoted&nbsp;<font face="monospace">1</font>, <font face="monospace">2</font>, <font face="monospace">3</font>, or <font face="monospace">4</font>.&nbsp; It is guaranteed an answer exists.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> n = 3, paths = [[1,2],[2,3],[3,1]]
<strong>Output:</strong> [1,2,3]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> n = 4, paths = [[1,2],[3,4]]
<strong>Output:</strong> [1,2,1,2]
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> n = 4, paths = [[1,2],[2,3],[3,4],[4,1],[1,3],[2,4]]
<strong>Output:</strong> [1,2,3,4]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10<sup>4</sup></code></li>
	<li><code>0 &lt;= paths.length &lt;= 2 * 10<sup>4</sup></code></li>
	<li><code>paths[i].length == 2</code></li>
	<li><code>1 &lt;= x<sub>i</sub>, y<sub>i</sub> &lt;= n</code></li>
	<li><code>x<sub>i</sub> != y<sub>i</sub></code></li>
	<li>No garden has <strong>four or more</strong> paths coming into or leaving it.</li>
</ul>

</div>

## Tags
- Graph (graph)

## Companies
- LinkedIn - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Greedily Paint
- Author: lee215
- Creation Date: Sun May 12 2019 12:03:17 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 13 2019 14:48:22 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Greedily paint nodes one by one.
Because there is no node that has more than 3 neighbors,
always one possible color to choose.

## **Complexity**
Time `O(N)` with `O(paths) = O(1.5N)`
Space `O(N)`


**Java**
```
    public int[] gardenNoAdj(int N, int[][] paths) {
        Map<Integer, Set<Integer>> G = new HashMap<>();
        for (int i = 0; i < N; i++) G.put(i, new HashSet<>());
        for (int[] p : paths) {
            G.get(p[0] - 1).add(p[1] - 1);
            G.get(p[1] - 1).add(p[0] - 1);
        }
        int[] res = new int[N];
        for (int i = 0; i < N; i++) {
            int[] colors = new int[5];
            for (int j : G.get(i))
                colors[res[j]] = 1;
            for (int c = 4; c > 0; --c)
                if (colors[c] == 0)
                    res[i] = c;
        }
        return res;
    }
```
**C++**
```
    vector<int> gardenNoAdj(int N, vector<vector<int>>& paths) {
        vector<int> res(N);
        vector<vector<int>> G(N);
        for (vector<int>& p : paths) {
            G[p[0] - 1].push_back(p[1] - 1);
            G[p[1] - 1].push_back(p[0] - 1);
        }
        for (int i = 0; i < N; ++i) {
            int colors[5] = {};
            for (int j : G[i])
                colors[res[j]] = 1;
            for (int c = 4; c > 0; --c)
                if (!colors[c])
                    res[i] = c;
        }
        return res;
    }
```
**Python:**
```
    def gardenNoAdj(self, N, paths):
        res = [0] * N
        G = [[] for i in range(N)]
        for x, y in paths:
            G[x - 1].append(y - 1)
            G[y - 1].append(x - 1)
        for i in range(N):
            res[i] = ({1, 2, 3, 4} - {res[j] for j in G[i]}).pop()
        return res
```

</p>


### Lee's Solution with Comments
- Author: EddieCarrillo
- Creation Date: Sat Jul 06 2019 06:13:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 06 2019 06:13:49 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int[] gardenNoAdj(int N, int[][] paths) {
        //Create a graph
        Map<Integer, Set<Integer>> graph = new HashMap<>();
        //... via adjacency list
        for (int i = 0; i < N; i++) graph.put(i, new HashSet<>());
        //Add the edges 
        
        for (int[] path : paths){
            int x = path[0] - 1; //Due to 1-based indexing 
            int y = path[1] - 1; //Due to 1-based indexing
            //Undirected edge
            graph.get(x).add(y);
            graph.get(y).add(x);
        }
        //Here is our solution vector where res[i] represents color of garden i+1
        int[] res = new int[N];
        
        //Now run graph painting algorithm
        
        //For each garden
        for (int i = 0; i < N; i++){
            int[] colors = new int[5]; //Use 5 instead of 4 so we can easily use 1-based indexing of the garden colors
            for (int nei : graph.get(i)){
                colors[res[nei]] = 1; //Mark the color as used if neighbor has used it before.
            }
            //Now just use a color that has not been used yet
            for (int c = 4; c >= 1; c--){
                if (colors[c] != 1) //colors[c] == 0 => the color has not been used yet,
                    res[i] = c; //so let\'s use that one
            }
        }
        
        return res;
        
    }
}
```
</p>


### Easy & Intuitive Java solution using Graph!! O(n) 90%
- Author: tomo3284
- Creation Date: Wed Mar 11 2020 15:01:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 22 2020 18:15:44 GMT+0800 (Singapore Standard Time)

<p>
This type of problems can be solve by implementing graph.
Each graden has one of 4 types of flower, and connected gardens. Each graden should have unique flower type than any connected garden.

**Q. Wait. How can we detect which flower type is not taken by connected garden?**
A. Visit to the connected garden and see what they have!
**Q. Ok... But how can you visit the connected garden in easy way?**
A. Create the class of Garden that has attributes of List\<Garden> connectedGarden, and visit iteratively!

Here we go, I created the class call Garden, that has attributes of flowerType that the garden has their own, and the list of connected garden. But as we know, flowerType have to be unique from other connected garden! That\'s why we initialize that by assigning -1.
```
class Garden {
    public static final int[] FLOWER_TYPES = {1,2,3,4};
    int flowerType;
    List<Garden> connectedGardens;
    
    public Garden() {
        flowerType = -1;
        connectedGardens = new ArrayList<Garden>();
    }
}
```

Now, the main part of this question...
**Q. Ok, enough of this long explanation. Tell me exactly how to detect which flower type is not taken by connected gardens.**
A. Here is how we do it! **Create this method inside the Garden class!**
```
public void setUniqueFlowerType() {
	HashSet<Integer> takenByConnectedGarden = new HashSet();
	for(Garden garden : connectedGardens){
		if(garden.flowerType != -1){
			takenByConnectedGarden.add(garden.flowerType);
		}
	}
        
    for(int flowerType : FLOWER_TYPES){
        if(!takenByConnectedGarden.contains(flowerType)){
            this.flowerType = flowerType;
			break;
		}
	}
}
```
This method would iterate through connected garden and add the flower type that is taken by connected garden into HashSet. Now, we know which flower is taken, we can assign proper flower type to the current garden that we are in!


After creating this class, it should be relatively simple to solve this problem :)
Steps:
1. Create N garden
2. Connect the garden
3. Set unique flower type for each garden
4. Return what you planted on each garden!

Here is my code looks like:
```
class Solution {
    public int[] gardenNoAdj(int N, int[][] paths) {
        /*
        instansiate N garden
        connect the path
        setUniqueFlowerType for each node
        return int[] array of each garden\'s flower type
        */
        
        Garden[] graph = new Garden[N];// array of gardens
        for(int i=0; i<N; i++){
            graph[i] = new Garden();
        }
        
        // connect path
        for(int[] path : paths){
			// -1 because of 0-based index
            int p1 = path[0]-1;
            int p2 = path[1]-1;
            Garden garden1 = graph[p1];
            Garden garden2 = graph[p2];
			// when garden1\'s neighbor is garden2, then garden2\'s neighbor should also be garden1
            garden1.connectedGardens.add(garden2);
            garden2.connectedGardens.add(garden1);
        }
        
        int idx = 0;
        int[] res = new int[N];
        for(Garden garden : graph){
            garden.setUniqueFlowerType();
            res[idx++] = garden.flowerType;
        }
        
        return res;
    }
}
```
</p>


