---
title: "Paint House III"
weight: 1354
#id: "paint-house-iii"
---
## Description
<div class="description">
<p>There is&nbsp;a row of&nbsp;<code>m</code>&nbsp;houses in a small city, each house must be painted with one of the&nbsp;<code>n</code>&nbsp;colors (labeled from 1 to <code>n</code>), some houses that has been painted last summer should not be painted again.</p>

<p>A neighborhood is a maximal group of continuous houses that are painted with the same color. (For example: houses = [1,2,2,3,3,2,1,1] contains 5 neighborhoods&nbsp; [{1}, {2,2}, {3,3}, {2}, {1,1}]).</p>

<p>Given an array <code>houses</code>, an&nbsp;<code>m * n</code>&nbsp;matrix <code>cost</code> and&nbsp;an integer <code><font face="monospace">target</font></code>&nbsp;where:</p>

<ul>
	<li><code>houses[i]</code>:&nbsp;is the color of the house <code>i</code>, <strong>0</strong> if the house is not painted yet.</li>
	<li><code>cost[i][j]</code>: is the cost of paint the house <code>i</code> with the color <code>j+1</code>.</li>
</ul>

<p>Return the minimum cost of painting all the&nbsp;remaining houses in such a way that there are exactly <code>target</code> neighborhoods, if&nbsp;not possible return <strong>-1</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> houses = [0,0,0,0,0], cost = [[1,10],[10,1],[10,1],[1,10],[5,1]], m = 5, n = 2, target = 3
<strong>Output:</strong> 9
<strong>Explanation:</strong> Paint houses of this way [1,2,2,1,1]
This array contains target = 3 neighborhoods, [{1}, {2,2}, {1,1}].
Cost of paint all houses (1 + 1 + 1 + 1 + 5) = 9.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> houses = [0,2,1,2,0], cost = [[1,10],[10,1],[10,1],[1,10],[5,1]], m = 5, n = 2, target = 3
<strong>Output:</strong> 11
<strong>Explanation:</strong> Some houses are already painted, Paint the houses of this way [2,2,1,2,2]
This array contains target = 3 neighborhoods, [{2,2}, {1}, {2,2}]. 
Cost of paint the first and last house (10 + 1) = 11.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> houses = [0,0,0,0,0], cost = [[1,10],[10,1],[1,10],[10,1],[1,10]], m = 5, n = 2, target = 5
<strong>Output:</strong> 5
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> houses = [3,1,2,3], cost = [[1,1,1],[1,1,1],[1,1,1],[1,1,1]], m = 4, n = 3, target = 3
<strong>Output:</strong> -1
<strong>Explanation:</strong> Houses are already painted with a total of 4 neighborhoods [{3},{1},{2},{3}] different of target = 3.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m == houses.length == cost.length</code></li>
	<li><code>n == cost[i].length</code></li>
	<li><code>1 &lt;= m &lt;= 100</code></li>
	<li><code>1 &lt;= n &lt;= 20</code></li>
	<li><code>1 &lt;= target&nbsp;&lt;= m</code></li>
	<li><code>0 &lt;= houses[i]&nbsp;&lt;= n</code></li>
	<li><code>1 &lt;= cost[i][j] &lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Paypal - 5 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Python explanation, and why I prefer top-down DP to bottom-up
- Author: grawlixes
- Creation Date: Sun Jun 07 2020 12:01:18 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 11 2020 00:58:37 GMT+0800 (Singapore Standard Time)

<p>
Knowing dynamic programming is like having a cheat code to do well on every contest (and most interviews)! It lets me do whatever stupid backtracking approach I want, while still being fast enough to pass the judge.

Let\'s say we want to paint the house at index i and all houses to the right of it with t unique neighborhoods, and the house at index i - 1 is color p. If house i is not yet painted (i.e. houses[i] == 0), then we have two options: 

1. We can match the color p of the previous house and not create any new neighborhoods starting at house i, which means we carry over a need for t neighbors starting at house i + 1.
2. We can make it a different color from p and thus we\'ll need t - 1 neighborhoods starting from house i + 1.

It\'s easier if the ith house is already painted (i.e. houses[i] != 0) because there\'s no choice in this case. Just move on to i + 1, and subtract a potential neighborhood only if p != houses[i].

When we reach the end of the array, we have 3 possibilities depending on the number of required neighborhoods (t):
1. t > 0. This means we colored the m houses with fewer than target neighborhoods, and so we failed.
2. t < 0. This means the opposite of the above - we colored the houses with too many neighborhoods, so we failed.
3. t == 0. We succeeded in coloring the houses with the appropriate number of neighborhoods.

Note that this also makes it easy to handle the case where the houses are originally in an unsolvable layout. We don\'t have to specially code for it, which I was actually about to do at first.

For anyone struggling with dynamic programming, the most important thing to note is that you want to search with the values you\'re caching (i, t, and p in this case). That\'s why I like doing this recursively and with memoization rather than tabluation; it\'s just more obvious to me when the cache values are in the recursive function header. I also like the idea of building your current case off of later cases; in every multi-dimensional DP problem I\'ve seen, it has always been more intuitive to me. I know that the using the call stack is usually more expensive, but it\'s also very useful.

```
class Solution:
    def minCost(self, houses: List[int], cost: List[List[int]], m: int, n: int, target: int) -> int:
        # maps (i, t, p) -> the minimum cost to paint houses i <= h < m with t neighborhoods, where house i - 1 is color p
        dp = {}
        
		# You can use this functools line instead of dp to make it faster, but I cache 
		# manually because I don\'t want to abstract the caching away from the reader.
		# @functools.lru_cache(None)
        def dfs(i, t, p):
            key = (i, t, p)
            
            if i == len(houses) or t < 0 or m - i < t:
                # base case - we\'re trying to color 0 houses. Only i == len(houses) is necessary
				# to check here, but it prunes a bit of the search space to make things faster.
                return 0 if t == 0 and i == len(houses) else float(\'inf\')
            
            if key not in dp:
                if houses[i] == 0:
                    dp[key] = min(dfs(i + 1, t - (nc != p), nc) + cost[i][nc - 1] for nc in range(1, n + 1))
                else:
                    dp[key] = dfs(i + 1, t - (houses[i] != p), houses[i])
                
            return dp[key]
            
        ret = dfs(0, target, -1)
        # if ret is infinity, then we failed every case because there were too many neighborhoods
        # to start. If we could paint over houses that were previously painted, we could remedy that,
        # but the problem doesn\'t allow that. so, we return -1 in that case.
        return ret if ret < float(\'inf\') else -1
```

**Time and space complexity**
since 0 <= i < m, 1 <= t <= target, and 1 <= p <= n, the space complexity of this solution is O(m * t * n). We access individual cache members up to n times per search node, so time complexity is O(m * t * n^2).
</p>


### [C++/Python] Simple DP top-down / bottom-up
- Author: yanrucheng
- Creation Date: Sun Jun 07 2020 12:00:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 07 2020 12:23:03 GMT+0800 (Singapore Standard Time)

<p>
**Complexity**
Time: `O(n * n * m * target)`
Space: `O(n * m * target) (can be optimized to O(n * target))`

**C++, bottom-up, DP**
```
class Solution {
public:
    int minCost(vector<int>& houses, vector<vector<int>>& cost, int m, int n, int target) {
        int INF = 1000000;
        vector<vector<vector<int>>> dp(m, vector<vector<int>>(target + 1, vector<int>(n, INF)));
            
        for (int c = 1; c <= n; c++)
            if (houses[0] == c)
                dp[0][1][c - 1] = 0;
            else if (!houses[0])
                dp[0][1][c - 1] = cost[0][c - 1];
        
        for (int i = 1; i < m; i++)
            for (int k = 1; k <= min(target, i + 1); k++)
                for (int c = 1; c <= n; c++) {
                    if (houses[i] && c != houses[i]) continue;
                    int same_neighbor = dp[i - 1][k][c - 1];
                    int diff_neighbor = INF;
                    for (int c_ = 1; c_ <= n; c_++)
                        if (c_ != c)
                            diff_neighbor = min(diff_neighbor, dp[i - 1][k - 1][c_ - 1]);
                    int paint_cost = cost[i][c - 1] * int(!houses[i]);
                    dp[i][k][c - 1] = min(same_neighbor, diff_neighbor) + paint_cost;
                }
        int res = *min_element(dp.back().back().begin(), dp.back().back().end());
        return (res < INF) ? res : -1;            
    }
};
```

**Python, bottom-up, DP**
```
class Solution:
    def minCost(self, houses: List[int], cost: List[List[int]], m: int, n: int, target: int) -> int:
        # dp[i][c][k]: i means the ith house, c means the cth color, k means k neighbor groups
        dp = [[[math.inf for _ in range(n)] for _ in range(target + 1)] for _ in range(m)]
        
        for c in range(1, n + 1):
            if houses[0] == c: dp[0][1][c - 1] = 0
            elif not houses[0]: dp[0][1][c - 1] = cost[0][c - 1]
                
        for i in range(1, m):
            for k in range(1, min(target, i + 1) + 1):
                for c in range(1, n + 1):
                    if houses[i] and c != houses[i]: continue
                    same_neighbor_cost = dp[i - 1][k][c - 1]
                    diff_neighbor_cost = min([dp[i - 1][k - 1][c_] for c_ in range(n) if c_ != c - 1] or [math.inf])
                    paint_cost = cost[i][c - 1] * (not houses[i])
                    dp[i][k][c - 1] = min(same_neighbor_cost, diff_neighbor_cost) + paint_cost
        res = min(dp[-1][-1])
        return res if res < math.inf else -1
```

**Python, top-down, recursion**
```
from functools import lru_cache
class Solution:
    def minCost(self, houses: List[int], cost: List[List[int]], m: int, n: int, target: int) -> int:
        
        @lru_cache(None)
        def helper(i, k, c):
            # min cost for paint the ith house in color c, resulting k neighborhood
            if i < 0 and k <= 1: return 0
            if k > i + 1 or k < 1: return math.inf
            if houses[i] and c != houses[i]: return math.inf
            
            paint_cost = cost[i][c - 1] * (not houses[i])
            prev_cost = min(helper(i - 1, k, c), min([helper(i - 1, k - 1, c_) \
                    for c_ in range(1, n + 1) if c_ != c] or [math.inf]))
            
            return paint_cost + prev_cost
        
        res = min(helper(m - 1, target, c) for c in range(1, n + 1))
        return res if res < math.inf else -1
```
</p>


### Python Solution
- Author: lee215
- Creation Date: Sun Jun 07 2020 12:07:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 07 2020 12:11:15 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
`dp[color, blocks]` means the minimum cost with
current house are painted in `color` and `blocks` neighborhoods up to now.

For each cell, try to paint into all possible colors,
and calculated the number of neighborhoods and cost.

I also calculated the range of possible neighborhoods number,
in order to reduce the runtime,
I find it not necessary to get accepted.
So I removed this part.
<br>

# **Complexity**
for `m` houses,
`dp` can save up to `nt` states (`n` colors * `t` blocks)
`1` or `n` choices for current house to paint.

Time `O(MTNN)`
Space `O(TN)`
<br>

**Python:**
```py
    def minCost(self, A, cost, m, n, target):
        dp, dp2 = {(0, 0): 0}, {}
        for i, a in enumerate(A):
            for cj in (range(1, n + 1) if a == 0 else [a]):
                for ci, b in dp:
                    b2 = b + (ci != cj)
                    if b2 > target: continue
                    dp2[cj, b2] = min(dp2.get((cj,b2), float(\'inf\')), dp[ci, b] + (cost[i][cj - 1] if cj != a else 0))
            dp, dp2 = dp2, {}
        return min([dp[c, b] for c, b in dp if b == target] or [-1])
```

</p>


