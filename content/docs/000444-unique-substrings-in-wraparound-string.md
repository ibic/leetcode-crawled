---
title: "Unique Substrings in Wraparound String"
weight: 444
#id: "unique-substrings-in-wraparound-string"
---
## Description
<div class="description">
<p>Consider the string <code>s</code> to be the infinite wraparound string of "abcdefghijklmnopqrstuvwxyz", so <code>s</code> will look like this: "...zabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcd....".</p>

<p>Now we have another string <code>p</code>. Your job is to find out how many unique non-empty substrings of <code>p</code> are present in <code>s</code>. In particular, your input is the string <code>p</code> and you need to output the number of different non-empty substrings of <code>p</code> in the string <code>s</code>.</p>

<p><b>Note:</b> <code>p</code> consists of only lowercase English letters and the size of p might be over 10000.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> "a"
<b>Output:</b> 1

<b>Explanation:</b> Only the substring "a" of string "a" is in the string s.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> "cac"
<b>Output:</b> 2
<b>Explanation:</b> There are two substrings "a", "c" of string "cac" in the string s.
</pre>
</p>

<p><b>Example 3:</b><br />
<pre>
<b>Input:</b> "zab"
<b>Output:</b> 6
<b>Explanation:</b> There are six substrings "z", "a", "b", "za", "ab", "zab" of string "zab" in the string s.
</pre>
</p>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 2 (taggedByAdmin: false)
- MAQ Software - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise Java solution using DP
- Author: shawngao
- Creation Date: Sun Dec 04 2016 13:35:01 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 11:49:12 GMT+0800 (Singapore Standard Time)

<p>
After failed with pure math solution and time out with DFS solution, I finally realized that this is a DP problem... 
The idea is, if we know the max number of unique substrings in ```p``` ends with ```'a', 'b', ..., 'z'```, then the summary of them is the answer. Why is that?
1. The max number of unique substring ends with a letter equals to the length of max contiguous substring ends with that letter. Example ```"abcd"```, the max number of unique substring ends with ```'d'``` is 4, apparently they are ```"abcd", "bcd", "cd" and "d"```.
2. If there are overlapping, we only need to consider the longest one because it covers all the possible substrings. Example: ```"abcdbcd"```, the max number of unique substring ends with ```'d'``` is 4 and all substrings formed by the 2nd ```"bcd"``` part are covered in the 4 substrings already.
3. No matter how long is a contiguous substring in ```p```, it is in ```s``` since ```s``` has infinite length.
4. Now we know the max number of unique substrings in ```p``` ends with ```'a', 'b', ..., 'z'``` and those substrings are all in ```s```. Summary is the answer, according to the question.

Hope I made myself clear...
```
public class Solution {
    public int findSubstringInWraproundString(String p) {
        // count[i] is the maximum unique substring end with ith letter.
        // 0 - 'a', 1 - 'b', ..., 25 - 'z'.
        int[] count = new int[26];
        
        // store longest contiguous substring ends at current position.
        int maxLengthCur = 0; 

        for (int i = 0; i < p.length(); i++) {
            if (i > 0 && (p.charAt(i) - p.charAt(i - 1) == 1 || (p.charAt(i - 1) - p.charAt(i) == 25))) {
                maxLengthCur++;
            }
            else {
                maxLengthCur = 1;
            }
            
            int index = p.charAt(i) - 'a';
            count[index] = Math.max(count[index], maxLengthCur);
        }
        
        // Sum to get result
        int sum = 0;
        for (int i = 0; i < 26; i++) {
            sum += count[i];
        }
        return sum;
    }
}
```
</p>


### Python Concise Solution
- Author: lee215
- Creation Date: Sat Oct 07 2017 07:07:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 16:26:04 GMT+0800 (Singapore Standard Time)

<p>
```
def findSubstringInWraproundString(self, p):
        res = {i: 1 for i in p}
        l = 1
        for i, j in zip(p, p[1:]):
            l = l + 1 if (ord(j) - ord(i)) % 26 == 1 else 1
            res[j] = max(res[j], l)
        return sum(res.values())
</p>


### Evolve from brute force to optimal
- Author: yu6
- Creation Date: Fri Jan 20 2017 06:05:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jan 20 2017 06:05:45 GMT+0800 (Singapore Standard Time)

<p>
1. O(n^3) check each substr, a hashtable is used to remove duplicate strings.
```
    int findSubstringInWraproundString(string p) {
        int n = p.size();
        unordered_set<string> ht;
        for(int i=0;i<n;i++)
            for(int j=i;j<n;j++) {
                if(j>i && p[j-1]+1!=p[j] && p[j-1]-p[j]!=25) break;
                ht.insert(p.substr(i,j-i+1));
            }
        return ht.size();
    }
```
2. O(n^2 logn), Each valid substr can be represented by the first char and the length, instead of the whole string.
```
    int findSubstringInWraproundString(string p) {
        int n = p.size();
        set<pair<char,int>> bst;
        for(int i=0;i<n;i++)
            for(int j=i;j<n;j++) {
                if(j>i && p[j-1]+1!=p[j] && p[j-1]-p[j]!=25) break;
                bst.insert(pair<char,int>(p[i],j-i+1));
            }
        return bst.size();
    }
```
3. O(n^2). For substrs starting at the same char, we only need to record the longest one. Because it covers all the shorter substrs starting from the char. The length is the number of substrings starting at the char.
```
    int findSubstringInWraproundString(string p) {
        int n = p.size(), len[26]={0};
        for(int i=0;i<n;i++)
            for(int j=i;j<n;j++) {
                if(j>i && p[j-1]+1!=p[j] && p[j-1]-p[j]!=25) break;
                len[p[i]-'a'] = max(len[p[i]-'a'],j-i+1);
            }
        return accumulate(len,len+26,0);
    }
```
4. O(n). Getting the longest substr starting from each char can be done in linear time. We can use two pointers to keep track of the current valid substring.
```
    int findSubstringInWraproundString(string p) {
        int len[26]={0}, i = 0, n = p.size();
        for(int j=0;j<n;j++)
            if(j>i && p[j-1]+1!=p[j] && p[j-1]-p[j]!=25) {
                for(int k=i;k<min(j,i+26);k++) len[p[k]-'a'] = max(len[p[k]-'a'],j-k);
                i=j--;
            }
        for(int k=i;k<min(n,i+26);k++) len[p[k]-'a'] = max(len[p[k]-'a'],n-k);
        return accumulate(len,len+26,0);
    }
```
</p>


