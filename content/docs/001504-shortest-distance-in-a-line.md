---
title: "Shortest Distance in a Line"
weight: 1504
#id: "shortest-distance-in-a-line"
---
## Description
<div class="description">
Table <code>point</code> holds the x coordinate of some points on x-axis in a plane, which are all integers.
<p>&nbsp;</p>
Write a query to find the shortest distance between two points in these points.

<p>&nbsp;</p>

<pre>
| x   |
|-----|
| -1  |
| 0   |
| 2   |
</pre>

<p>&nbsp;</p>
The shortest distance is &#39;1&#39; obviously, which is from point &#39;-1&#39; to &#39;0&#39;. So the output is as below:

<p>&nbsp;</p>

<pre>
| shortest|
|---------|
| 1       |
</pre>

<p>&nbsp;</p>
<b>Note:</b> Every point is unique, which means there is no duplicates in table <code>point</code>.

<p>&nbsp;</p>
<b>Follow-up:</b> What if all these points have an id and are arranged from the left most to the right most of x axis?

<p>&nbsp;</p>

</div>

## Tags


## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach: Using `ABS()` and `MIN()` functions [Accepted]

**Intuition**

Calculate the distances between each two points first, and then display the minimum one.

**Algorithm**

To get the distances of each two points, we need to join this table with itself and use `ABS()` function since the distance is nonnegative.
One trick here is to add the condition in the join to avoid calculating the distance between a point with itself.

```sql
SELECT
    p1.x, p2.x, ABS(p1.x - p2.x) AS distance
FROM
    point p1
        JOIN
    point p2 ON p1.x != p2.x
;
```
>Note: The columns p1.x, p2.x are only for demonstrating purpose, so they are not actually needed in the end.

Taking the sample data for example, the output would be as below.
```
| x  | x  | distance |
|----|----|----------|
| 0  | -1 | 1        |
| 2  | -1 | 3        |
| -1 | 0  | 1        |
| 2  | 0  | 2        |
| -1 | 2  | 3        |
| 0  | 2  | 2        |
```

At last, use `MIN()` to select the smallest value in the *distance* column.

**MySQL**

```sql
SELECT
    MIN(ABS(p1.x - p2.x)) AS shortest
FROM
    point p1
        JOIN
    point p2 ON p1.x != p2.x
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short & Simple
- Author: cenkay
- Creation Date: Thu Jul 12 2018 15:54:29 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 02:14:12 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT MIN(a.x - b.x) AS shortest
FROM point a, point b
WHERE a.x > b.x;
```
</p>


### The real no join solution that beats 99.71% submissions
- Author: EasonS
- Creation Date: Tue Sep 26 2017 11:24:57 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 26 2017 11:24:57 GMT+0800 (Singapore Standard Time)

<p>
To speed up, no join is allowed. The "no join" posts actually use self join by doing ```select ... from point p1, point p2```.

The trick is the follow-up: when the points are sorted, the min distance must come from adjacent points. Since the distance of adjacent points is equivalent to the difference between two rows, we can use user-defined variable to keep the value from the previous row to do the calculation.

The code below runs in O(3n), comparing to the O(n^2) join methods \uff08if merge join is used\uff09.
```
# Initialize the prev variable to a big negative number so that the first value of difference will never get selected
set @prev := -100000000; 
select min(diff) as shortest
from (select (x - @prev) as diff, @prev := x 
      from (select * from point order by x) t
     ) tt
;
```
</p>


### Self join
- Author: john700
- Creation Date: Tue Jun 06 2017 04:17:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 26 2018 00:34:32 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT MIN(ABS(P1.x - P2.x)) AS shortest FROM point AS P1
JOIN point AS P2 ON P1.x <> P2.x
```
</p>


