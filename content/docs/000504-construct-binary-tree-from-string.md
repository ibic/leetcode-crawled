---
title: "Construct Binary Tree from String"
weight: 504
#id: "construct-binary-tree-from-string"
---
## Description
<div class="description">
<p>You need to construct a binary tree from a string consisting of parenthesis and integers.</p>

<p>The whole input represents a binary tree. It contains an integer followed by zero, one or two pairs of parenthesis. The integer represents the root&#39;s value and a pair of parenthesis contains a child binary tree with the same structure.</p>

<p>You always start to construct the <b>left</b> child node of the parent first if it exists.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/02/butree.jpg" style="width: 382px; height: 322px;" />
<pre>
<strong>Input:</strong> s = &quot;4(2(3)(1))(6(5))&quot;
<strong>Output:</strong> [4,2,6,3,1,5]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;4(2(3)(1))(6(5)(7))&quot;
<strong>Output:</strong> [4,2,6,3,1,5,7]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;-4(2(3)(1))(6(5)(7))&quot;
<strong>Output:</strong> [-4,2,6,3,1,5,7]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= s.length &lt;= 3 * 10<sup>4</sup></code></li>
	<li><code>s</code> consists of digits, <code>&#39;(&#39;</code>, <code>&#39;)&#39;</code>, and <code>&#39;-&#39;</code> only.</li>
</ul>

</div>

## Tags
- String (string)
- Tree (tree)

## Companies
- Facebook - 4 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java stack solution
- Author: fallcreek
- Creation Date: Sun Mar 12 2017 13:27:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 23:32:03 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public TreeNode str2tree(String s) {
        Stack<TreeNode> stack = new Stack<>();
        for(int i = 0, j = i; i < s.length(); i++, j = i){
            char c = s.charAt(i);
            if(c == ')')    stack.pop();
            else if(c >= '0' && c <= '9' || c == '-'){
                while(i + 1 < s.length() && s.charAt(i + 1) >= '0' && s.charAt(i + 1) <= '9') i++;
                TreeNode currentNode = new TreeNode(Integer.valueOf(s.substring(j, i + 1)));
                if(!stack.isEmpty()){
                    TreeNode parent = stack.peek();
                    if(parent.left != null)    parent.right = currentNode;
                    else parent.left = currentNode;
                }
                stack.push(currentNode);
            }
        }
        return stack.isEmpty() ? null : stack.peek();
    }
}
```
</p>


### Java Recursive Solution
- Author: compton_scatter
- Creation Date: Sun Mar 12 2017 12:19:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 10:41:57 GMT+0800 (Singapore Standard Time)

<p>
```
public TreeNode str2tree(String s) {
    if (s == null || s.length() == 0) return null;
    int firstParen = s.indexOf("(");
    int val = firstParen == -1 ? Integer.parseInt(s) : Integer.parseInt(s.substring(0, firstParen));
    TreeNode cur = new TreeNode(val);
    if (firstParen == -1) return cur;
    int start = firstParen, leftParenCount = 0;
    for (int i=start;i<s.length();i++) {
        if (s.charAt(i) == '(') leftParenCount++;
        else if (s.charAt(i) == ')') leftParenCount--;
        if (leftParenCount == 0 && start == firstParen) {cur.left = str2tree(s.substring(start+1,i)); start = i+1;}
        else if (leftParenCount == 0) cur.right = str2tree(s.substring(start+1,i));
    }
    return cur;
}
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Mar 12 2017 13:00:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 10:57:54 GMT+0800 (Singapore Standard Time)

<p>
We perform a recursive solution.  There are four cases for what the string might look like:
1. empty
2. [integer] 
3. [integer] ( [tree] ) 
4. [integer] ( [tree] ) ( [tree] )

When there is no '(', we are in one of the first two cases and proceed appropriately.
Else, we find the index "jx" of the ')' character that marks the end of the first tree.  We do this by keeping a tally of how many left brackets minus right brackets we've seen.  When we've seen 0, we must be at the end of the first tree.  The second tree is going to be the expression S[jx + 2: -1], which might be empty if we are in case #3.

```
def str2tree(self, S):
    ix = S.find('(')
    if ix < 0:
        return TreeNode(int(S)) if S else None
        
    bal = 0
    for jx, u in enumerate(S):
        if u == '(': bal += 1
        if u == ')': bal -= 1
        if jx > ix and bal == 0:
            break

    root = TreeNode(int(S[:ix]))
    root.left = self.str2tree(S[ix+1:jx])
    root.right = self.str2tree(S[jx+2:-1])
    return root
```
</p>


