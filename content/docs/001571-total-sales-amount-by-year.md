---
title: "Total Sales Amount by Year"
weight: 1571
#id: "total-sales-amount-by-year"
---
## Description
<div class="description">
<p>Table: <code>Product</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| product_id    | int     |
| product_name  | varchar |
+---------------+---------+
product_id is the primary key for this table.
product_name is the name of the product.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Sales</code></p>

<pre>
+---------------------+---------+
| Column Name         | Type    |
+---------------------+---------+
| product_id          | int     |
| period_start        | varchar |
| period_end          | date    |
| average_daily_sales | int     |
+---------------------+---------+
product_id is the primary key for this table. 
period_start&nbsp;and period_end&nbsp;indicates the start and end date for sales period, both dates are inclusive.
The average_daily_sales column holds the average daily&nbsp;sales amount of the items for the&nbsp;period.

</pre>

<p>Write an SQL query to&nbsp;report the&nbsp;Total sales&nbsp;amount of each item for each year, with corresponding product name,&nbsp;product_id, product_name and report_year.</p>

<p>Dates of the sales years are between 2018 to 2020.&nbsp;Return the result table <strong>ordered</strong> by product_id and report_year.</p>

<p>The query result format is in the following example:</p>

<pre>

<code>Product</code> table:
+------------+--------------+
| product_id | product_name |
+------------+--------------+
| 1          | LC Phone     |
| 2          | LC T-Shirt   |
| 3          | LC Keychain  |
+------------+--------------+

<code>Sales</code> table:
+------------+--------------+-------------+---------------------+
| product_id | period_start | period_end  | average_daily_sales |
+------------+--------------+-------------+---------------------+
| 1          | 2019-01-25   | 2019-02-28  | 100                 |
| 2          | 2018-12-01   | 2020-01-01  | 10                  |
| 3          | 2019-12-01   | 2020-01-31  | 1                   |
+------------+--------------+-------------+---------------------+

Result table:
+------------+--------------+-------------+--------------+
| product_id | product_name | report_year | total_amount |
+------------+--------------+-------------+--------------+
| 1          | LC Phone     |    2019     | 3500         |
| 2          | LC T-Shirt   |    2018     | 310          |
| 2          | LC T-Shirt   |    2019     | 3650         |
| 2          | LC T-Shirt   |    2020     | 10           |
| 3          | LC Keychain  |    2019     | 31           |
| 3          | LC Keychain  |    2020     | 31           |
+------------+--------------+-------------+--------------+
LC Phone was sold for the period of 2019-01-25 to 2019-02-28, and there are 35 days for this period. Total amount 35*100 = 3500.&nbsp;
LC T-shirt was sold for the period of 2018-12-01&nbsp;to 2020-01-01, and there are 31, 365, 1 days for years 2018, 2019 and 2020 respectively.
LC Keychain was sold for the period of 2019-12-01&nbsp;to 2020-01-31, and there are 31, 31 days for years 2019 and 2020 respectively.
</pre>
</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise MySQL solution Runtime: 188 ms
- Author: yehong
- Creation Date: Sun Mar 22 2020 09:54:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2020 09:54:41 GMT+0800 (Singapore Standard Time)

<p>

```
SELECT a.product_id, b.product_name, a.report_year, a.total_amount
FROM (
    SELECT product_id, \'2018\' AS report_year,
        average_daily_sales * (DATEDIFF(LEAST(period_end, \'2018-12-31\'), GREATEST(period_start, \'2018-01-01\'))+1) AS total_amount
    FROM Sales
    WHERE YEAR(period_start)=2018 OR YEAR(period_end)=2018

    UNION ALL

    SELECT product_id, \'2019\' AS report_year,
        average_daily_sales * (DATEDIFF(LEAST(period_end, \'2019-12-31\'), GREATEST(period_start, \'2019-01-01\'))+1) AS total_amount
    FROM Sales
    WHERE YEAR(period_start)<=2019 AND YEAR(period_end)>=2019

    UNION ALL

    SELECT product_id, \'2020\' AS report_year,
        average_daily_sales * (DATEDIFF(LEAST(period_end, \'2020-12-31\'), GREATEST(period_start, \'2020-01-01\'))+1) AS total_amount
    FROM Sales
    WHERE YEAR(period_start)=2020 OR YEAR(period_end)=2020
) a
LEFT JOIN Product b
ON a.product_id = b.product_id
ORDER BY a.product_id, a.report_year

```
</p>


### Using Recursive CTE to get all the possible dates (MSSQL)
- Author: liurenjie
- Creation Date: Fri Mar 20 2020 07:36:00 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Apr 10 2020 22:47:11 GMT+0800 (Singapore Standard Time)

<p>
1. Using Recursive CTE to get all the possible dates
2. Product left outer join the sales table, and left outer join to the dates table , in order to get the sales amount for each day.
3. Sum up the sales amount, and group by year, and cast the product_id into varchar.

*Note*
*Not sure why the result set asks for product_id in VARCHAR(string), the column in source table is INTEGER, the CAST operation in the last step is not necessary to be varchar(200) or varchar(10)... just my personal choice.*


```
with dates as
(select s_date =  min(period_start), e_date = max(period_end) from sales
	union all
 select dateadd(day, 1 ,s_date) , e_date from dates
 where s_date<e_date
) 
select
PRODUCT_ID  = cast(p.product_id  as varchar(200))
,PRODUCT_NAME = p.product_name
,REPORT_YEAR = cast(year(s_date) as varchar(10))
,TOTAL_AMOUNT = sum(average_daily_sales)
from product p 
left outer join sales s on p.product_id = s.product_id
left outer join dates d on d.s_date between s.period_start and s.period_end 
group by p.product_id , p.product_name, year(s_date)
order by 1,3 
option(maxrecursion 0)
```
</p>


### The Correct Solution to Create a master list of Report_Year
- Author: mirandanathan
- Creation Date: Tue Jun 09 2020 06:09:04 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 09 2020 06:50:15 GMT+0800 (Singapore Standard Time)

<p>
I am talking this because one of my friend failed in the interview with FB for a DE position. FB changed the test case. period_start: 2010-05-26, period_end: 2020-06-02. You do not have 2011 - 2019 in the table, so solution B does not work here, and also solution A "Hard Code" is not accepted by the interviewer.

I offer solution C for everyone, hope it will work for you guys! 

Solution A: Hard Code

Solution B: Union Existing Date
```
select distinct year(period_start) as report_year from Sales S1
UNION select distinct year(period_end) as report_year from Sales S2
```

Solution C: Dynamic
```
with cte as
(select min(year(period_start)) as start_year, max(year(period_end)) as end_year from sales),

cte1 as
(select start_year as report_year from cte
 union all
 select report_year + 1 from cte1,cte where report_year < end_year)
 
select * from cte1
```

The source code is as below:
```
with cte as
(select min(year(period_start)) as start_year, max(year(period_end)) as end_year from sales),

cte1 as
(select start_year as report_year from cte
 union all
 select report_year + 1 from cte1,cte where report_year < end_year)

select 
Sales.product_id,
Product.product_name,
cast(cte1.report_year as varchar) as report_year,
(datediff(day, case when year(period_start) <> cte1.report_year then cast(cast(cte1.report_year as varchar)+\'01\'+\'01\' as date) else period_start end, case when year(period_end) <> cte1.report_year then cast(cast(cte1.report_year as varchar)+\'12\'+\'31\' as date) else period_end end)+1) * average_daily_sales as total_amount
from Sales 
left join Product on Sales.product_id = Product.product_id
left join cte1 on cte1.report_year >= year(period_start) and cte1.report_year <= year(period_end)

order by 1,2 asc
```
</p>


