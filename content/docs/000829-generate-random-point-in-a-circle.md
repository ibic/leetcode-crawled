---
title: "Generate Random Point in a Circle"
weight: 829
#id: "generate-random-point-in-a-circle"
---
## Description
<div class="description">
<p>Given the radius and x-y positions of the center of a circle, write a function <code>randPoint</code>&nbsp;which&nbsp;generates a uniform random&nbsp;point in the circle.</p>

<p>Note:</p>

<ol>
	<li>input and output values are&nbsp;in&nbsp;<a href="https://www.webopedia.com/TERM/F/floating_point_number.html" target="_blank">floating-point</a>.</li>
	<li>radius and x-y position of the center of the circle is passed into the class constructor.</li>
	<li>a point on the circumference of the circle is considered to be&nbsp;in the circle.</li>
	<li><code>randPoint</code>&nbsp;returns&nbsp;a size 2 array containing x-position and y-position of the random point, in that order.</li>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: 
</strong><span id="example-input-1-1">[&quot;Solution&quot;,&quot;randPoint&quot;,&quot;randPoint&quot;,&quot;randPoint&quot;]
</span><span id="example-input-1-2">[[1,0,0],[],[],[]]</span>
<strong>Output: </strong><span id="example-output-1">[null,[-0.72939,-0.65505],[-0.78502,-0.28626],[-0.83119,-0.19803]]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: 
</strong><span id="example-input-2-1">[&quot;Solution&quot;,&quot;randPoint&quot;,&quot;randPoint&quot;,&quot;randPoint&quot;]
</span><span id="example-input-2-2">[[10,5,-7.5],[],[],[]]</span>
<strong>Output: </strong><span id="example-output-2">[null,[11.52438,-8.33273],[2.46992,-16.21705],[11.13430,-12.42337]]</span></pre>
</div>

<p><strong>Explanation of Input Syntax:</strong></p>

<p>The input is two lists:&nbsp;the subroutines called&nbsp;and their&nbsp;arguments.&nbsp;<code>Solution</code>&#39;s&nbsp;constructor has three arguments, the radius, x-position of the center, and y-position of the center of the circle. <code>randPoint</code> has no arguments.&nbsp;Arguments&nbsp;are&nbsp;always wrapped with a list, even if there aren&#39;t any.</p>
</div>

</div>

## Tags
- Math (math)
- Random (random)
- Rejection Sampling (rejection-sampling)

## Companies
- Leap Motion - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---

#### Approach 1: Rejection Sampling

**Intuition**

It is easy to generate a random point in a square. Could we use randomly generated points in a square to get random points in a circle? Which generated points could we use, and which ones would we need to toss away? How often would we generate points that we could use?

**Algorithm**

<p align="center">
    <img src="../Figures/883/squareCircleOverlay.png" alt="Square_Circle_Overlay" style="height: 300px;"/>

A square of size length 2R overlaid with a circle of radius R.

</p>

To get uniform random points in a circle $$C$$ of radius $$R$$, we can generate uniform random points in the square $$S$$ of side length $$2R$$, keeping all of the points which are at most [euclidean distance](https://en.wikipedia.org/wiki/Euclidean_distance) $$R$$ from the center, and rejecting all which are farther away than that. This technique is called [rejection sampling](https://en.wikipedia.org/wiki/Rejection_sampling). Each possible location on the circle has the same probability of being generated, so the sampling of points will be uniformly distributed.

The area of the square is $$(2R)^2 = 4R$$ and the area of the circle is $$\pi R \approx  3.14R$$. $$\dfrac{3.14R}{4R} = \dfrac{3.14}{4} = .785$$. Therefore, we will get a usable sample approximately $$78.5\%$$ of the time and the expected number of times that we will need to sample until we get a usable sample is $$\dfrac{1}{.785} \approx 1.274$$ times.

<iframe src="https://leetcode.com/playground/6Cs7kHZJ/shared" frameBorder="0" width="100%" height="446" name="6Cs7kHZJ"></iframe>


**Complexity Analysis**

* Time Complexity: $$O(1)$$ on average. $$O(\infty)$$ worst case. (per $$\text{randPoint}$$ call)
* Space Complexity: $$O(1)$$.

<br/>

---

#### Approach 2: Inverse Transform Sampling (Math)

**Disclaimer**

This solution relies on advanced math which is not expected knowledge for a coding interview. It is presented here only for educational purposes.

**Algorithm**

Assume that we are given a circle $$C$$ of radius $$1$$ that is centered at the [origin](https://en.wikipedia.org/wiki/Origin_(mathematics)), and our task is to sample uniform random points on this circle.

Lets imagine another circle $$B$$ of radius $$\frac{1}{2}$$ which is also centered at the origin.

The circumference of $$C$$ is twice the circumference of $$B$$, because the circumference of a circle is [directly proportional](https://en.wikipedia.org/wiki/Proportionality_(mathematics)#Direct_proportionality) to the radius. Also, the probability of sampling a point from a subregion in circle $$C$$ is directly proportional to the area of the subregion. Therefore, the probability of sampling a point on the perimeter of $$C$$ is twice that of sampling a point on the perimeter of $$B$$.

More generally, what is implied is that the sampling probability is directly proportional to the distance from the origin, from $$0$$ up to $$R$$. This can be modeled as a [probability density function](https://en.wikipedia.org/wiki/Probability_density_function) $$f$$, where $$x$$ is the distance from the origin and $$f(x)$$ is the relative sampling probability at $$x$$.

The area under any probability density function curve must be $$1$$. Therefore, the equation must be $$f(x) = 2x$$.

Using our probability density function $$f$$, we can compute the [cumulative distribution function](https://en.wikipedia.org/wiki/Cumulative_distribution_function) $$F$$, where $$F(x)$$ is the probability of sampling a point within a distance of $$x$$ from the origin.

The cumulative distribution function is the integral of the probability density function.

$$
F(x) = \int{f(x)} = \int{2x} = x^2
$$

Lastly, we can use our cumulative distribution function $$F$$ to compute the inverse cumulative distribution function $$F^{-1}$$, which accepts uniform random value between $$0$$ and $$1$$ and returns a random distance from origin in accordance with $$f$$. $$^{[\dagger]}$$

$$
\begin{align}
& F^{-1}(F(x)) = x \\
& F^{-1}(x^2) = x \\
& F^{-1}(x) = \sqrt{x}
\end{align}
$$

Now, to generate a uniform random point on $$C$$, we just need to compute a random distance $$D$$ from origin using $$F^{-1}$$ and a uniform random angle $$\theta$$ over the range $$[0, 2 \cdot PI)$$.

The points will be generated as [polar coordinates](https://en.wikipedia.org/wiki/Polar_coordinate_system). To convert to [cartesian coordinates](https://en.wikipedia.org/wiki/Cartesian_coordinate_system), we can use the following formulas.

$$
X = D \cdot \cos(\theta) \\
Y = D \cdot \sin(\theta)
$$

<iframe src="https://leetcode.com/playground/N8hzSUdQ/shared" frameBorder="0" width="100%" height="344" name="N8hzSUdQ"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(1)$$ per $$\text{randPoint}$$ call.
* Space Complexity: $$O(1)$$

**Footnotes**

* 　$$^{[\dagger]}$$ This technique of using the inverse cumulative distribution function to sample numbers at random from the corresponding probability distribution is called [inverse transform sampling](https://en.wikipedia.org/wiki/Inverse_transform_sampling).
* This solution is inspired by  [this](https://stackoverflow.com/a/50746409) answer on Stack Overflow.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Polar Coordinates 10 lines
- Author: caraxin
- Creation Date: Fri Jul 27 2018 11:54:34 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 13:21:45 GMT+0800 (Singapore Standard Time)

<p>
The point is that we should not use ```x=rand(len)*cos(rand(degree))```, we should use ```x=sqrt(rand(len))*cos(rand(degree))```.
![image](https://s3-lc-upload.s3.amazonaws.com/users/caraxin/image_1532665245.png)

Reference: http://www.anderswallin.net/2009/05/uniform-random-points-in-a-circle-using-polar-coordinates/
```
class Solution {
    double radius, x_center, y_center;
    public Solution(double radius, double x_center, double y_center) {
        this.radius=radius;
        this.x_center=x_center;
        this.y_center=y_center;
    }
    
    public double[] randPoint() {
        double len= Math.sqrt(Math.random())*radius;
        double deg= Math.random()*2*Math.PI;
        double x= x_center+len*Math.cos(deg);
        double y= y_center+len*Math.sin(deg);
        return new double[]{x,y};
    }
}
```
**update**: thanks @jianhu for pointing this out, we should use```double deg = Math.random() * Math.PI * 2``` here.
Happy Coding!
</p>


### Explanation with Graphs why using Math.sqrt()
- Author: fd8783
- Creation Date: Wed Aug 01 2018 17:30:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 12:27:37 GMT+0800 (Singapore Standard Time)

<p>
    public double[] randPoint() {
        double len= Math.sqrt(Math.random())*radius;
        double deg= Math.random()*2*Math.PI;
        double x= x_center+len*Math.cos(deg);
        double y= y_center+len*Math.sin(deg);
        return new double[]{x,y};
    }
Above is the solution that some currently most voted post used to solve this question.
Posters are using **Math.sqrt()** to get the graph with correct random points.
However, what\'s happening here? Why the points will go near to center if we don\'t use Math.sqrt()?
Here is some expanation with graph to show what\'s behind this.

First, in above solution, we need to know that we are using one random **Length** to multiply on a random **normalized vector**(x^2 + y^2 = 1) to get the random point.
Sounds good right?
However, if we directly multiply them, we will get a graph that points placed averagely according to **Length/Radius**. 
![image](https://s3-lc-upload.s3.amazonaws.com/users/fd8783/image_1533180618.png)  -->   ![image](https://s3-lc-upload.s3.amazonaws.com/users/fd8783/image_1533114715.png)
<h3>In the smallest circle (i.e. radius = 0.1), it can get a same amount of points with the larger circle (i.e. radius = 0.9)!</h3>

Therefore, we need to decrease the chance that the point can placed there according to how close it is to the center (how short is the that position\'s radius).

Then, finally, Here is Math.sqrt()
![image](https://s3-lc-upload.s3.amazonaws.com/users/fd8783/image_1533115147.png)
***Red** line is normal random, **Green** line is Math.sqrt(random)
This is exactly what we want!

After using this, we can get a correct graph!
![image](https://s3-lc-upload.s3.amazonaws.com/users/fd8783/image_1533180675.png) -->   ![image](https://s3-lc-upload.s3.amazonaws.com/users/fd8783/image_1533116036.png)



Undeniably, using Math.sqrt() is a clever method.
However, I think it is quite difficult to understand without explanation.
I hope this can help if you don\'t understand why Math.sqrt().



</p>


### Very simple Python solution
- Author: HeroKillerEver
- Creation Date: Fri Jul 27 2018 15:00:19 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 11:18:18 GMT+0800 (Singapore Standard Time)

<p>
Since we are given the `(x, y)` and `r`, so we can formulate this rectangle of `[x-r, y-r, x+r, y+r]`. It is very easy for us to make samples from squares uniformly, and we just reject the samples outside the circle.

```
import random
class Solution(object):

    def __init__(self, radius, x_center, y_center):
        """
        :type radius: float
        :type x_center: float
        :type y_center: float
        """
        self.x_min, self.x_max = x_center - radius, x_center + radius
        self.y_min, self.y_max = y_center - radius, y_center + radius
        self.radius = radius
        self.x_center = x_center
        self.y_center = y_center

    def randPoint(self):
        """
        :rtype: List[float]
        """
        while True:
            x, y = random.uniform(self.x_min, self.x_max), random.uniform(self.y_min, self.y_max)
            if (x - self.x_center)**2 + (y - self.y_center)**2 <= self.radius**2:
                return [x, y]
```
</p>


