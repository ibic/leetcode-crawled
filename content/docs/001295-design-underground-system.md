---
title: "Design Underground System"
weight: 1295
#id: "design-underground-system"
---
## Description
<div class="description">
<p>Implement the class <code>UndergroundSystem</code> that supports three methods:</p>

<p>1.<code>&nbsp;checkIn(int id, string stationName, int t)</code></p>

<ul>
	<li>A customer with id card equal to <code>id</code>, gets in the station <code>stationName</code> at time <code>t</code>.</li>
	<li>A customer&nbsp;can only be checked into one place at a time.</li>
</ul>

<p>2.<code>&nbsp;checkOut(int id, string stationName, int t)</code></p>

<ul>
	<li>A customer with id card equal to <code>id</code>, gets out from the station <code>stationName</code> at time <code>t</code>.</li>
</ul>

<p>3.&nbsp;<code>getAverageTime(string startStation, string endStation)</code>&nbsp;</p>

<ul>
	<li>Returns the average time to travel between the <code>startStation</code> and the <code>endStation</code>.</li>
	<li>The average time is computed from all the previous traveling from <code>startStation</code> to <code>endStation</code> that happened <strong>directly</strong>.</li>
	<li>Call to <code>getAverageTime</code> is always valid.</li>
</ul>

<p>You can assume all calls to <code>checkIn</code> and <code>checkOut</code> methods are consistent. That is, if a customer gets in at time <strong>t<sub>1</sub></strong> at some station, then it gets out at time <strong>t<sub>2</sub></strong> with <strong>t<sub>2</sub> &gt; t<sub>1</sub></strong>.&nbsp;All events happen in chronological order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;UndergroundSystem&quot;,&quot;checkIn&quot;,&quot;checkIn&quot;,&quot;checkIn&quot;,&quot;checkOut&quot;,&quot;checkOut&quot;,&quot;checkOut&quot;,&quot;getAverageTime&quot;,&quot;getAverageTime&quot;,&quot;checkIn&quot;,&quot;getAverageTime&quot;,&quot;checkOut&quot;,&quot;getAverageTime&quot;]
[[],[45,&quot;Leyton&quot;,3],[32,&quot;Paradise&quot;,8],[27,&quot;Leyton&quot;,10],[45,&quot;Waterloo&quot;,15],[27,&quot;Waterloo&quot;,20],[32,&quot;Cambridge&quot;,22],[&quot;Paradise&quot;,&quot;Cambridge&quot;],[&quot;Leyton&quot;,&quot;Waterloo&quot;],[10,&quot;Leyton&quot;,24],[&quot;Leyton&quot;,&quot;Waterloo&quot;],[10,&quot;Waterloo&quot;,38],[&quot;Leyton&quot;,&quot;Waterloo&quot;]]

<strong>Output</strong>
[null,null,null,null,null,null,null,14.00000,11.00000,null,11.00000,null,12.00000]

<strong>Explanation</strong>
UndergroundSystem undergroundSystem = new UndergroundSystem();
undergroundSystem.checkIn(45, &quot;Leyton&quot;, 3);
undergroundSystem.checkIn(32, &quot;Paradise&quot;, 8);
undergroundSystem.checkIn(27, &quot;Leyton&quot;, 10);
undergroundSystem.checkOut(45, &quot;Waterloo&quot;, 15);
undergroundSystem.checkOut(27, &quot;Waterloo&quot;, 20);
undergroundSystem.checkOut(32, &quot;Cambridge&quot;, 22);
undergroundSystem.getAverageTime(&quot;Paradise&quot;, &quot;Cambridge&quot;); &nbsp; &nbsp; &nbsp; // return 14.00000. There was only one travel from &quot;Paradise&quot; (at time 8) to &quot;Cambridge&quot; (at time 22)
undergroundSystem.getAverageTime(&quot;Leyton&quot;, &quot;Waterloo&quot;); &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;// return 11.00000. There were two travels from &quot;Leyton&quot; to &quot;Waterloo&quot;, a customer with id=45 from time=3 to time=15 and a customer with id=27 from time=10 to time=20. So the average time is ( (15-3) + (20-10) ) / 2 = 11.00000
undergroundSystem.checkIn(10, &quot;Leyton&quot;, 24);
undergroundSystem.getAverageTime(&quot;Leyton&quot;, &quot;Waterloo&quot;); &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;// return 11.00000
undergroundSystem.checkOut(10, &quot;Waterloo&quot;, 38);
undergroundSystem.getAverageTime(&quot;Leyton&quot;, &quot;Waterloo&quot;); &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;// return 12.00000
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input</strong>
[&quot;UndergroundSystem&quot;,&quot;checkIn&quot;,&quot;checkOut&quot;,&quot;getAverageTime&quot;,&quot;checkIn&quot;,&quot;checkOut&quot;,&quot;getAverageTime&quot;,&quot;checkIn&quot;,&quot;checkOut&quot;,&quot;getAverageTime&quot;]
[[],[10,&quot;Leyton&quot;,3],[10,&quot;Paradise&quot;,8],[&quot;Leyton&quot;,&quot;Paradise&quot;],[5,&quot;Leyton&quot;,10],[5,&quot;Paradise&quot;,16],[&quot;Leyton&quot;,&quot;Paradise&quot;],[2,&quot;Leyton&quot;,21],[2,&quot;Paradise&quot;,30],[&quot;Leyton&quot;,&quot;Paradise&quot;]]

<strong>Output</strong>
[null,null,null,5.00000,null,null,5.50000,null,null,6.66667]

<strong>Explanation</strong>
UndergroundSystem undergroundSystem = new UndergroundSystem();
undergroundSystem.checkIn(10, &quot;Leyton&quot;, 3);
undergroundSystem.checkOut(10, &quot;Paradise&quot;, 8);
undergroundSystem.getAverageTime(&quot;Leyton&quot;, &quot;Paradise&quot;); // return 5.00000
undergroundSystem.checkIn(5, &quot;Leyton&quot;, 10);
undergroundSystem.checkOut(5, &quot;Paradise&quot;, 16);
undergroundSystem.getAverageTime(&quot;Leyton&quot;, &quot;Paradise&quot;); // return 5.50000
undergroundSystem.checkIn(2, &quot;Leyton&quot;, 21);
undergroundSystem.checkOut(2, &quot;Paradise&quot;, 30);
undergroundSystem.getAverageTime(&quot;Leyton&quot;, &quot;Paradise&quot;); // return 6.66667
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>There will be at most <code><font face="monospace">20000</font></code>&nbsp;operations.</li>
	<li><code>1 &lt;= id, t &lt;= 10^6</code></li>
	<li>All strings consist of uppercase, lowercase English letters and digits.</li>
	<li><code>1 &lt;=&nbsp;stationName.length &lt;= 10</code></li>
	<li>Answers within&nbsp;<code>10^-5</code>&nbsp;of the actual value will be accepted as correct.</li>
</ul>

</div>

## Tags
- Design (design)

## Companies
- Bloomberg - 58 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

If you were asked this question in a real interview, you should expect to discuss real-world issues related to it. For example, it would not be realistic to store the data in volatile computer memory. In practice, computers fail (e.g. lose power) so we need to store the data in a permanent medium, such as a database. 

Additionally, we might need to consider scalability. In a large city, such as Tokyo, the metro system gets 7 million passenger trips per day! With a little math, we can quickly estimate that during peak travel time, there must be *thousands* of check-ins and check-outs every *second*. This is a lot of data that one computer would need to receive through its network connection! To make this work, we'd probably be using more than one computer. This introduces concurrency issues that would need to be addressed.

It's also likely that this module would have to fit in with other code in the passenger-tracking system. It's not likely that a little average time tracker will get exclusive use of all resources.

For your actual implementation, you'll probably be expected to use `HashMap`s. This is common in system design interview questions: you're given a complex real world problem to explore and then are asked to implement a small piece of it.

<br/>

---

#### Approach 1: Two HashMaps

**Intuition**

The first thing to do when faced with a design problem like this one is to carefully read through what each of the methods needs to do. Observe that two of the methods take input but don't return anything; `checkIn(...)` and `checkOut(...)`. On the other hand, the `getAverageTime(...)` method both takes input and returns a value. 

This effectively means that there's only one place the data structure needs to *output* data. Therefore, a good place to start is by looking at what exactly `getAverageTime(...)` needs to return, and what it needs to do this. `getAverageTime(...)` takes two input parameters; a start station and an end station. It then needs to return the average trip time between these two stations. This means that we need to somehow obtain "trip times", collated by a pair of stations; start and end. We could represent this data as a table of some kind. We'll talk more about this in a bit!

![Table of sample trip times](../Figures/1396/trip_times.png)

The "trip times" are obtained, unsurprisingly, through the `checkIn(...)` and `checkOut(...)` methods. A customer with a given `id` checks into station `stationName` at time `t`, using the `checkIn(...)` method. At some point in the future, the customer checks out with the `checkOut(...)` method. The input to the `checkOut(...)` method is the same `id`, but a different `stationName` and `t`, representing the exit station and exit time. The start station, end station, start time, and end time (that were all under the same `id`) can be used to calculate a journey time between the start station and end station, that could be entered into the table we discussed above.

![Table of sample check in times and stations](../Figures/1396/check_in_table.png)

The `checkIn(...)` and `checkOut(...)` method calls for a customer with a particular `id` aren't necessarily made one-after-the-other. In fact, many other customers could have been processed in-between. We'll need to store the entry station and time that go with a particular customer `id` so that we can then get them back when that same customer `id` does a `checkOut(...)`. One way we could do this is to use `HashMap`s to map `id`s to check-in stations and times.

```text

averagesTable = a table of some kind
checkInTimes = a new HashMap (id -> checkInTime)
checkInStations = a new HashMap (id -> stationName)
 
define function checkIn(id, stationName, t):
    checkInTimes.put(id, t)
    checkInStations.put(id, stationName)

define function checkOut(id, stationName, t):
    startTime = checkInTimes.get(id)
    startStation = checkInStations.get(id)
    journeyTime = t - startTime
    journey = (startTime, startStation)
    # We'll worry about how exactly this bit works soon...
    averagesTable.enter(journey, journeyTime)
```

Now when `getAverageTime(...)` is called, we can look in `averagesTable` for the information needed to answer the query. Let's go back to looking at how we could represent the `averagesTable`.

One way is to use a `HashMap` that maps pairs of stations to `List`s of journey times. Then when `getAverageTime(...)` is called, it could look up the relevant `List`, add the times in it together, and then divide by the number of times that were in the list (this is the standard way of calculating an average).

![Table of sample trip times with average time column](../Figures/1396/trip_times_with_average_time.png)

Instead of needing to add them up every time though, we could just store the total so far. This will work fine, seeing as we never need to re-access the individual journey times. If we do this, we'll also need to store the total number of journeys. Then with these 2 pieces of information, we can calculate the average time with $$O(1)$$ time complexity. This is a lot better than storing a list of individual journey times; repeated calls to `getAverageTime(...)` would have, in the worst case, repeatedly added thousands of times together. 

Our `checkOut(...)` function should be modified as follows.

```text
journeyTotals = a new HashMap (startStation, endStation -> total)
numberOfJourneys = a new HashMap (startStation, endStation -> count) 

define function checkOut(id, stationName, t):
    startTime = checkInTimes.get(id)
    startStation = checkInStations.get(id)
    journeyTime = t - startTime
    journey = (startTime, startStation)
    set journeyTotals.get(journey) to journeyTotals.get(journey) + journeyTime
    set numberOfJourneys.get(journey) to numberOfJourneys.get(journey) + 1
```

Using these new data structures, `getAverageTime(...)` can be implemented as follows.

```text
define function getAverageTime(startStation, endStation):
    journey = (startTime, startStation)
    total = journeyTotals.get(journey)
    count = numberOfJourneys.get(journey)
    return total / count
```

One detail we overlooked earlier w1as deleting data from `checkInTimes` and `checkInStations` after the corresponding call to `checkOut(...)`. Seeing as we can assume `checkIn(...)` and `checkOut(...)` calls are always valid, we know that if the user with `id` comes back later, after doing their `checkOut(...)`, that they must now be doing a `checkIn(...)`. This will *overwrite* the *old* check-in data, without causing any issues. In short, we don't *have* to explicitly delete the old data.

However, it would actually be better to delete the data. We don't need it (hence the reason it being overwritten isn't an issue), and in fact keeping it around will slowly increase the memory usage of the program over time. This is because every `id` that had ever used the program will have an entry in the check-in tables. If, instead, we delete the old entries, then only `id`s that are currently undergoing a journey (have checked-in but not checked-out) will be in the table. Removing old entries is an $$O(1)$$ time complexity operation. Therefore, it's best to remove the entries to save memory.

**Algorithm**

Some of the implementation design decisions here have no "right" answer.

***Saving the total time vs the average time***

One design decision we need to make is whether to *store* the total-time or the average-time for each possible route. The benefit of storing the average-time is that the system will be able to store a lot more data before being affected by overflow (remember, the total-count is always *eventually* going to be affected). The downside of storing average-time though is that we need to update it with division every time a new journey is made on that route. This leads to compounded floating-point error. 

Given that overflow isn't an issue with the problem constraints we're given here, it's safest to store the total-time so that we can avoid the floating-point error. In a real world system, you would use a `Decimal` library that supports arbitrary precision and doesn't suffer from floating-point error.

***Number of HashMaps to use***

In the discussion above, we broke the data into 4 separate `HashMaps`. While this would be an okay way to implement it, we often group similar data together. This would give us the following two `HashMaps`:

```text
checkInData = a new HashMap (id -> startStation, checkInTime)
journeyData = a new HashMap (startStation, endStation -> total, count)
```

In a realistic system, there's likely to be more operations than just `getAverageTime(...)`, and the data will likely be put into and pulled from a database. Database design is another massive area of system design where many factors need to be taken into account to come up with the design that best achieves what is needed for the particular queries that will need to be answered.

Here, we chose to use the `Pair` class for the Java implementation, but whether or not this is good design is probably down to personal preference. It does have the benefit of being less code to write (and less whiteboard space in an interview!). However, it is a bit contextually strange that one member of the `Pair` is known as "the key" and the other as "the value".

Where we need to represent a key with both a start station and an end station, the code below appends the strings with a `->` between them to make a single string. This is safe for the constraints we're given, as station names can only be letters or numbers. In a real world system, this would need to be thought through carefully. In Python, we can simply use a tuple as the key (without the code becoming needlessly verbose).

Other options would include using nested `HashMaps` or defining your own objects.

<iframe src="https://leetcode.com/playground/kQLqztoa/shared" frameBorder="0" width="100%" height="500" name="kQLqztoa"></iframe>


**Complexity Analysis**

- Time complexity : $$O(1)$$ for all.

    - `checkIn(...)`: Inserting a key/value pair into a `HashMap` is an $$O(1)$$ operation.  

    - `checkOut(...)`: Additionally, getting the corresponding value for a key from a `HashMap` is also an $$O(1)$$ operation.

    - `getAverageTime(...)`: Dividing two numbers is also an $$O(1)$$ operation.

- Space complexity : $$O(P + S^2)$$, where $$S$$ is the number of stations on the network, and $$P$$ is the number of passengers making a journey concurrently during peak time.

    - The program uses two `HashMap`s. We need to determine the maximum sizes these could become.

    - Firstly, we'll consider `checkInData`. This `HashMap` holds one entry for each passenger who has `checkIn(...)`ed, but not `checkOut(...)`ed. Therefore, the maximum size this `HashMap` could be is the maximum possible number of passengers making a journey at the same time, which we defined to be $$P$$. Therefore, the size of this `HashMap` is $$O(P)$$.

    - Secondly, we need to consider `journeyData`. This `HashMap` has one entry for each pair of stations that has had at least one passenger start and end a journey at those stations. Over time, we could reasonably expect every possible pair of the $$S$$ stations on the network to have an entry in this `HashMap`, which would be $$O(S^2)$$.

    - Seeing as we don't know whether $$S^2$$ or $$P$$ is larger, we need to add these together, giving a total space complexity of $$O(P + S^2)$$.

<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] HashMap & Pair - Clean code - O(1)
- Author: hiepit
- Creation Date: Sun Mar 29 2020 12:10:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 29 2020 12:44:31 GMT+0800 (Singapore Standard Time)

<p>
**Java**
In Java 8, a [Pair<V, K>](https://docs.oracle.com/javafx/2/api/javafx/util/Pair.html) is added in `javafx.util` package. The class represent key-value pairs and supports very basic operations like `getKey()`, `getValue()`, `hashCode()`, `equals(java.lang.Object o)`.
```java
class UndergroundSystem {
    HashMap<String, Pair<Integer, Integer>> checkoutMap = new HashMap<>(); // Route - {TotalTime, Count}
    HashMap<Integer, Pair<String, Integer>> checkInMap = new HashMap<>();  // Uid - {StationName, Time}
    
	public UndergroundSystem() {}
    
	public void checkIn(int id, String stationName, int t) {
        checkInMap.put(id, new Pair<>(stationName, t));
    }
    
	public void checkOut(int id, String stationName, int t) {
        Pair<String, Integer> checkIn = checkInMap.get(id);
        String route = checkIn.getKey() + "_" + stationName;
        int totalTime = t - checkIn.getValue();
        Pair<Integer, Integer> checkout = checkoutMap.getOrDefault(route, new Pair<>(0, 0));
        checkoutMap.put(route, new Pair<>(checkout.getKey() + totalTime, checkout.getValue() + 1));
    }
    
	public double getAverageTime(String startStation, String endStation) {
        String route = startStation + "_" + endStation;
        Pair<Integer, Integer> checkout = checkoutMap.get(route);
        return (double) checkout.getKey() / checkout.getValue();
    }
}
```

**C++**
```c++
class UndergroundSystem {
public:
    unordered_map<string, pair<int, int>> checkoutMap; // Route - {TotalTime, Count}
    unordered_map<int, pair<string, int>> checkInMap; // Uid - {StationName, Time}
    
    UndergroundSystem() {}
    
    void checkIn(int id, string stationName, int t) {
        checkInMap[id] = {stationName, t};
    }
    
    void checkOut(int id, string stationName, int t) {
        auto& checkIn = checkInMap[id];
        string route = checkIn.first + "_" + stationName;
        checkoutMap[route].first += t - checkIn.second;
        checkoutMap[route].second += 1;
    }
    
    double getAverageTime(string startStation, string endStation) {
        string route = startStation + "_" + endStation;
        auto& checkout = checkoutMap[route];
        return (double) checkout.first / checkout.second;
    }
};
```
</p>


### C++ #minimalizm
- Author: votrubac
- Creation Date: Sun Mar 29 2020 12:13:19 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 31 2020 08:15:07 GMT+0800 (Singapore Standard Time)

<p>
We need a hash map for check-ins `m`, and hash map for the statistic `stats`. For each combinatino of `startStation + endStation`, we track the total duration and the number of trips.

```cpp
unordered_map<int, pair<string, int>> m;
unordered_map<string, pair<int, int>> stats;
void checkIn(int id, string startStation, int timeIn) {
    m[id] = {startStation, timeIn};
}
void checkOut(int id, string endStation, int timeOut) {
    const auto &[startStation, timeIn] = m[id];
    auto &[totalDuration, tripsCnt] = stats[startStation + ">" + endStation];
    totalDuration += timeOut - timeIn;
    ++tripsCnt;
}
double getAverageTime(string startStation, string endStation) {
    auto [totalDuration, tripsCnt] = stats[startStation + ">" + endStation];
    return (double)totalDuration / tripsCnt;
}
```
</p>


### Java solution for easy understanding using OOPS
- Author: venkatakrishnan-srinivasaraj
- Creation Date: Sat Jun 06 2020 12:15:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 06 2020 12:15:57 GMT+0800 (Singapore Standard Time)

<p>
```
class Passenger {
    int checkinTime;
    int checkoutTime;
    String checkinLocation;
    String checkoutLocation;

    public Passenger(String checkinLocation, int checkinTime) {
        this.checkinLocation = checkinLocation;
        this.checkinTime = checkinTime;
    }

    void checkout(String checkoutLocation, int checkoutTime) {
        this.checkoutLocation = checkoutLocation;
        this.checkoutTime = checkoutTime;
    }

}

class Route {
    String startStation;
    String endStation;
    int totalNumberOfTrips;
    long totalTimeSpentInTrips;

    public Route(String startStation, String endStation) {
        this.startStation = startStation;
        this.endStation = endStation;
    }

    double getAverageTime() {
        return (double) totalTimeSpentInTrips / totalNumberOfTrips;
    }

    void addTrip(int startTime, int endTime) {
        totalTimeSpentInTrips += endTime - startTime;
        totalNumberOfTrips++;
    }
}

class UndergroundSystem {

    Map<Integer, Passenger> currentPassengerMap;
    Map<String, Route> routeMap;

    public UndergroundSystem() {
        currentPassengerMap = new HashMap<>();
        routeMap = new HashMap<>();
    }

    public void checkIn(int id, String stationName, int t) {
        if (!currentPassengerMap.containsKey(id)) {
            Passenger passenger = new Passenger(stationName, t);
            currentPassengerMap.put(id, passenger);
        }
    }

    public void checkOut(int id, String stationName, int t) {
        if (currentPassengerMap.containsKey(id)) {
            Passenger passenger = currentPassengerMap.get(id);
            passenger.checkout(stationName, t);
            String routeKey = passenger.checkinLocation + passenger.checkoutLocation;
            Route route = routeMap.getOrDefault(routeKey, new Route(passenger.checkinLocation, passenger.checkoutLocation));
            route.addTrip(passenger.checkinTime, passenger.checkoutTime);
            routeMap.put(routeKey, route);
            currentPassengerMap.remove(id);
        }
    }

    public double getAverageTime(String startStation, String endStation) {
        return routeMap.get(startStation + endStation).getAverageTime();
    }
}
```
</p>


