---
title: "Merge Two Binary Trees"
weight: 556
#id: "merge-two-binary-trees"
---
## Description
<div class="description">
<p>Given two binary trees and imagine that when you put one of them to cover the other, some nodes of the two trees are overlapped while the others are not.</p>

<p>You need to merge them into a new binary tree. The merge rule is that if two nodes overlap, then sum node values up as the new value of the merged node. Otherwise, the NOT null node will be used as the node of new tree.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> 
	Tree 1                     Tree 2                  
          1                         2                             
         / \                       / \                            
        3   2                     1   3                        
       /                           \   \                      
      5                             4   7                  
<b>Output:</b> 
Merged tree:
	     3
	    / \
	   4   5
	  / \   \ 
	 5   4   7
</pre>

<p>&nbsp;</p>

<p><b>Note:</b> The merging process must start from the root nodes of both trees.</p>

</div>

## Tags
- Tree (tree)

## Companies
- Apple - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Huawei - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Using Recursion [Accepted]

We can traverse both the given trees in a preorder fashion. At every step, we check if the current node exists(isn't null) for both the trees. If so, we add the values in the current nodes of both the trees and update the value in the current node of the first tree to reflect this sum obtained. At every step, we also call the original function `mergeTrees()` with the left children and then with the right children of the current nodes of the two trees. If at any step, one of these children happens to be null, we return the child of the other tree(representing the corresponding child subtree) to be added as a child subtree to the calling parent node in the first tree. At the end, the first tree will represent the required resultant merged binary tree.

The following animation illustrates the process.

!?!../Documents/617_Merge_Trees_Recursion.json:1000,563!?!

<iframe src="https://leetcode.com/playground/d9nZDPEJ/shared" frameBorder="0" name="d9nZDPEJ" width="100%" height="428"></iframe>

**Complexity Analysis**

* Time complexity : $$O(m)$$. A total of $$m$$ nodes need to be traversed. Here, $$m$$ represents the minimum number of nodes from the two given trees.

* Space complexity : $$O(m)$$. The depth of the recursion tree can go upto $$m$$ in the case of a skewed tree. In average case, depth will be $$O(logm)$$.

---
#### Approach #2 Iterative Method [Accepted]

**Algorithm**

In the current approach, we again traverse the two trees, but this time we make use of a $$stack$$ to do so instead of making use of recursion. Each entry in the $$stack$$ strores data in the form $$[node_{tree1}, node_{tree2}]$$. Here, $$node_{tree1}$$ and $$node_{tree2}$$ are the nodes of the first tree and the second tree respectively.

We start off by pushing the root nodes of both the trees onto the $$stack$$. Then, at every step, we remove a node pair from the top of the stack. For every node pair removed, we add the values corresponding to the two nodes and update the value of the corresponding node in the first tree. Then, if the left child of the first tree exists, we push the left child(pair) of both the trees onto the stack. If the left child of the first tree doesn't exist, we append the left child(subtree) of the second tree to the current node of the first tree. We do the same for the right child pair as well. 

If, at any step, both the current nodes are null, we continue with popping the next nodes from the $$stack$$.

The following animation depicts the process.

!?!../Documents/617_Merge_Trees_Stack.json:1000,563!?!

<iframe src="https://leetcode.com/playground/v2TK7i2x/shared" frameBorder="0" name="v2TK7i2x" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We traverse over a total of $$n$$ nodes. Here, $$n$$ refers to the smaller of the number of nodes in the two trees.

* Space complexity : $$O(n)$$. The depth of stack can grow upto $$n$$ in case of a skewed tree.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution, 6 lines, Tree Traversal
- Author: shawngao
- Creation Date: Sun Jun 11 2017 11:17:35 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 04:00:43 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null) return null;
        
        int val = (t1 == null ? 0 : t1.val) + (t2 == null ? 0 : t2.val);
        TreeNode newNode = new TreeNode(val);
        
        newNode.left = mergeTrees(t1 == null ? null : t1.left, t2 == null ? null : t2.left);
        newNode.right = mergeTrees(t1 == null ? null : t1.right, t2 == null ? null : t2.right);
        
        return newNode;
    }
}
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Mon Jun 12 2017 02:20:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 13:31:08 GMT+0800 (Singapore Standard Time)

<p>
Let's create a recursive solution.
* If both trees are empty then we return empty.
* Otherwise, we will return a tree.  The root value will be t1.val + t2.val, except these values are 0 if the tree is empty.
* The left child will be the merge of t1.left and t2.left, except these trees are empty if the parent is empty.
* The right child is similar.

```
def mergeTrees(self, t1, t2):
    if not t1 and not t2: return None
    ans = TreeNode((t1.val if t1 else 0) + (t2.val if t2 else 0))
    ans.left = self.mergeTrees(t1 and t1.left, t2 and t2.left)
    ans.right = self.mergeTrees(t1 and t1.right, t2 and t2.right)
    return ans
```
</p>


### Short Recursive Solution w/ Python & C++
- Author: zqfan
- Creation Date: Sun Jun 11 2017 13:04:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 23 2020 16:05:27 GMT+0800 (Singapore Standard Time)

<p>
as the problem description says: Otherwise, the NOT null node will be used as the node of new tree. so here comes this solution.

python solution
```
class Solution(object):
    def mergeTrees(self, t1, t2):
        if t1 and t2:
            root = TreeNode(t1.val + t2.val)
            root.left = self.mergeTrees(t1.left, t2.left)
            root.right = self.mergeTrees(t1.right, t2.right)
            return root
        else:
            return t1 or t2
```
c++ solution
```
class Solution {
public:
    TreeNode* mergeTrees(TreeNode* t1, TreeNode* t2) {
        if ( t1 && t2 ) {
            TreeNode * root = new TreeNode(t1->val + t2->val);
            root->left = mergeTrees(t1->left, t2->left);
            root->right = mergeTrees(t1->right, t2->right);
            return root;
        } else {
            return t1 ? t1 : t2;
        }
    }
};
```
</p>


