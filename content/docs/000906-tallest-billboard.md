---
title: "Tallest Billboard"
weight: 906
#id: "tallest-billboard"
---
## Description
<div class="description">
<p>You are installing a billboard and want it to have the largest height.&nbsp; The billboard will have two steel supports, one on each side.&nbsp; Each steel support must be an equal height.</p>

<p>You have a collection of <code>rods</code> which can be welded together.&nbsp; For example, if you have rods of lengths 1, 2, and 3, you can weld them together to make a support of length 6.</p>

<p>Return the largest possible height of your billboard installation.&nbsp; If you cannot support the billboard, return 0.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,2,3,6]</span>
<strong>Output: </strong><span id="example-output-1">6</span>
<strong>Explanation:</strong> We have two disjoint subsets {1,2,3} and {6}, which have the same sum = 6.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[1,2,3,4,5,6]</span>
<strong>Output: </strong><span id="example-output-2">10</span>
<strong>Explanation:</strong> We have two disjoint subsets {2,3,5} and {4,6}, which have the same sum = 10.
</pre>
</div>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[1,2]</span>
<strong>Output: </strong><span id="example-output-3">0</span>
<strong>Explanation: </strong>The billboard cannot be supported, so we return 0.
</pre>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= rods.length &lt;= 20</code></li>
	<li><code>1 &lt;= rods[i] &lt;= 1000</code></li>
	<li><code>The sum of rods is at most 5000.</code></li>
</ol>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach 1: Dynamic Programming

**Intuition**

For each rod `x`, we can write `+x`, `-x`, or `0`.  Our goal is to write `0` using the largest sum of positive terms.  For writings that have a sum of `0`, let's call the sum of the positive terms written the *score*.  For example, `+1 +2 +3 -6` has a score of `6`.

Since `sum(rods)` is bounded, it suggests to us to use that fact it in some way.  Indeed, if we already wrote some sum in the first few terms, it doesn't matter how we got it.  For example, with `rods = [1,2,2,3]`, we could arrive at a sum of `3` in 3 different ways, but the effective score is `3`.  This upper-bounds the number of states we have to consider to `10001`, as there are only this many possible sums in the interval `[-5000, 5000]`.

**Algorithm**

Let `dp[i][s]` be the largest score we can get using `rods[j]` `(j >= i)`, after previously writing a sum of `s` (that isn't included in the score).  For example, with `rods = [1,2,3,6]`, we might have `dp[1][1] = 5`, as after writing `1`, we could write `+2 +3 -6` with the remaining `rods[i:]` for a score of `5`.

In the base case, `dp[rods.length][s]` is `0` when `s == 0`, and `-infinity` everywhere else.  The recursion is `dp[i][s] = max(dp[i+1][s], dp[i+1][s-rods[i]], rods[i] + dp[i+1][s+rods[i]])`.


<iframe src="https://leetcode.com/playground/2nKqKRGu/shared" frameBorder="0" width="100%" height="480" name="2nKqKRGu"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(NS)$$, where $$N$$ is the length of `rods`, and $$S$$ is the maximum of $$\sum \text{rods}[i]$$.

* Space Complexity:  $$O(NS)$$.
<br />
<br />


---
#### Approach 2: Meet in the Middle

**Intuition**

Typically, the complexity of brute force can be reduced with a "meet in the middle" technique.  As applied to this problem, we have $$3^N$$ possible states, from writing either `+x`, `-x`, or `0` for each rod `x`, and we want to make this brute force faster.

What we can do is write the first and last $$3^{N/2}$$ states separately, and attempt to combine them.  For example, if we have rods `[1, 3, 5, 7]`, then the first two rods create up to nine states: `[0+0, 0+3, 0-3, 1+0, 1+3, 1-3, -1+0, -1+3, -1-3]`, and the last two rods also create nine states.

We will store each state as the sum of positive terms, and the sum of absolute values of the negative terms.  For example, `+1 +2 -3 -4` becomes `(3, 7)`.  Let's also call the difference `3 - 7` to be the *delta* of this state, so this state has a delta of `-4`.

Our high level goal is to combine states with deltas that sum to `0`.  The score of a state will be the sum of the positive terms, and we want the highest score.  Note that for each delta, we only care about the state that has the highest score.

**Algorithm**

Split the rods into two halves: left and right.

For each half, use brute force to compute the reachable states as defined above.  Then, for each state, record the delta and the maximum score.

Now, we have a left and right halves with `[(delta, score)]` information.  We'll find the largest total score, with total delta `0`.

<iframe src="https://leetcode.com/playground/sDk6ZeMf/shared" frameBorder="0" width="100%" height="500" name="sDk6ZeMf"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(3^{N/2})$$, where $$N$$ is the length of `rods`.

* Space Complexity:  $$O(3^{N/2})$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] DP, min(O(SN/2), O(3^N/2 * N)
- Author: lee215
- Creation Date: Sun Dec 09 2018 12:07:36 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Mar 26 2020 09:52:17 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
`dp[d]` mean the maximum pair of sum we can get with pair difference `d`

If have a pair of sum `(a, b)` with `a > b`, then `dp[a - b] = b`

If we have `dp[diff] = a`,
it means we have a pair of sum `(a, a + diff)`.
And this is the biggest pair with difference `diff`

# Example
`dp` is initializes with `dp[0] = 0`;

Assume we have an init state like this
------- y ------|----- d -----|
------- y ------|

**case 1**
If put `x` to tall side
------- y ------|----- d -----|----- x -----|
------- y ------|

We update `dp[d + x] = max(dp[d + x],  y )`

**case 2.1**
Put `x` to low side and `d >= x`:
-------y------|----- d -----|
-------y------|--- x ---|

We update `dp[d-x] = max(dp[d - x], y + x)`

**case 2.2**
Put `x` to low side and `d < x`:
------- y ------|----- d -----|
------- y ------|------- x -------|
We update `dp[x - d] = max(dp[x - d], y + d)`

case 2.1 and case2.2 can merge into `dp[abs(x - d)] = max(dp[abs(x - d)], y + min(d, x))`
<br>

# Time Complexity
`O(NM)`, where we have
`N = rod.length <= 20`
`S = sum(rods) <= 5000`
`M = all possible sum = min(3^N, S)`

There are 3 ways to arrange a number: in the first group, in the second, not used.
The number of difference will be less than `3^N`.
The only case to reach `3^N` is when rod = `[1,3,9,27,81...]`
<br>

**Java, O(SN) using array, just for better reading:**
```java
    public int tallestBillboard(int[] rods) {
        int[] dp = new int[5001];
        for (int d = 1; d < 5001; d++) dp[d] = -10000;
        for (int x : rods) {
            int[] cur = dp.clone();
            for (int d = 0; d + x < 5001; d++) {
                dp[d + x] = Math.max(dp[d + x], cur[d]);
                dp[Math.abs(d - x)] = Math.max(dp[Math.abs(d - x)], cur[d] + Math.min(d, x));
            }
        }
        return dp[0];
    }
```

**Java, using HashMap:**
```java
    public int tallestBillboard(int[] rods) {
        Map<Integer, Integer> dp = new HashMap<>(), cur;
        dp.put(0, 0);
        for (int x : rods) {
            cur = new HashMap<>(dp);
            for (int d : cur.keySet()) {
                dp.put(d + x, Math.max(cur.get(d), dp.getOrDefault(x + d, 0)));
                dp.put(Math.abs(d - x), Math.max(cur.get(d) + Math.min(d, x), dp.getOrDefault(Math.abs(d - x), 0)));
            }
        }
        return dp.get(0);
    }
```

**C++:**
```cpp
    int tallestBillboard(vector<int>& rods) {
        unordered_map<int, int> dp;
        dp[0] = 0;
        for (int x : rods) {
            unordered_map<int, int> cur(dp);
            for (auto it: cur) {
                int d = it.first;
                dp[d + x] = max(dp[d + x],cur[d]);
                dp[abs(d - x)] = max(dp[abs(d - x)], cur[d] + min(d, x));
            }
        }
        return dp[0];
    }
```

**Python:**
```py
    def tallestBillboard(self, rods):
        dp = {0: 0}
        for x in rods:
            for d, y in dp.items():
                dp[d + x] = max(dp.get(x + d, 0), y)
                dp[abs(d - x)] = max(dp.get(abs(d - x), 0), y + min(d, x))
        return dp[0]
```
<br>

# One Optimisation

We do the same thing for both half of `rods`.
Then we try to find the same difference in both results.

**Time Complexity**:
`O(NM)`, where we have
`N = rod.length <= 20`
`S = sum(rods) <= 5000`
`M = all possible sum = min(3^N/2, S)`

<br>

**Python:**
```py
    def tallestBillboard(self, rods):
        def helper(A):
            dp = {0: 0}
            for x in A:
                for d, y in dp.items():
                    dp[d + x] = max(dp.get(x + d, 0), y)
                    dp[abs(d - x)] = max(dp.get(abs(d - x), 0), y + min(d, x))
            return dp

        dp, dp2 = helper(rods[:len(rods) / 2]), helper(rods[len(rods) / 2:])
        return max(dp[d] + dp2[d] + d for d in dp if d in dp2)
```
</p>


### Java knapsack O(N*sum)
- Author: wangzi6147
- Creation Date: Sun Dec 09 2018 13:28:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 09 2018 13:28:32 GMT+0800 (Singapore Standard Time)

<p>
Consider this problem as:
Given a list of numbers, multiply each number with `1` or `0` or `-1`, make the sum of all numbers to 0. Find a combination which has the largest sum of all positive numbers.
e.g. Given `[1,2,3,4,5,6]`, we have `1*0 + 2 + 3 - 4 + 5 - 6 = 0`, the sum of all positive numbers is `2 + 3 + 5 = 10`. The answer is 10.

This is a knapsack problem.
`dp[i][j]` represents whether the sum of first `i` numbers can be `j - 5000`. `dp[0][5000] = true`.
Then `dp[i + 1][j] = dp[i][j - rods[i]] | dp[i][j + rods[i]] | dp[i][j]`.
`max[i][j]` represents the largest sum of all positive numbers when the sum of first `i` numbers is `j - 5000`.

Time complexity: `O(N*sum)`

```
class Solution {
    public int tallestBillboard(int[] rods) {
        int n = rods.length;
        boolean[][] dp = new boolean[n + 1][10001];
        int[][] max = new int[n + 1][10001];
        dp[0][5000] = true;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= 10000; j++) {
                if (j - rods[i] >= 0 && dp[i][j - rods[i]]) {
                    dp[i + 1][j] = true;
                    max[i + 1][j] = Math.max(max[i + 1][j], max[i][j - rods[i]] + rods[i]);
                }
                if (j + rods[i] <= 10000 && dp[i][j + rods[i]]) {
                    dp[i + 1][j] = true;
                    max[i + 1][j] = Math.max(max[i + 1][j], max[i][j + rods[i]]);
                }
                if (dp[i][j]) {
                    dp[i + 1][j] = true;
                    max[i + 1][j] = Math.max(max[i + 1][j], max[i][j]);
                }
            }
        }
        return max[n][5000];
    }
}
```

P.S. This solution can be optimized to O(sum) space and use `max` array only.
</p>


### C++ 16 ms DFS + memo
- Author: votrubac
- Creation Date: Wed Dec 12 2018 01:42:18 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Dec 12 2018 01:42:18 GMT+0800 (Singapore Standard Time)

<p>
I know it\'s not as cool as a DP solution, though during the context it may be quicker to do DFS and add memoisation if you get TLE. 

So we go through all rods, and either skip the rod, add it to the first support (```s1```), or to the second support (```s2```). The result is the maximum of these three operations. When we exhausted all rods (```i >= rs.size()```), we return the rod size if both rods are the same, or zero. This way, our simple DFS solution can be implemented in just a few lines of code:
```
int tallestBillboard(vector<int>& rods, int i = 0, int s1 = 0, int s2 = 0) {
  if (i >= rods.size()) return s1 == s2 ? s1 : 0;
  return max({ tallestBillboard(rods, i + 1, s1, s2), 
               tallestBillboard(rods, i + 1, s1 + rods[i], s2), 
               tallestBillboard(rods, i + 1, s1, s2 + rods[i]) });
}
```
You\'ll probably get TLE (certainly, in this particular case) for a simple DFS solution, so the next step is to think about memoisation to avoid processing identical conditions over and over again. We could memoise support sizes ```s1``` and ```s2``` for the current rod number ```i```. However, that would require a lot of memory (and we will get MLE or TLE). 

The intuition here is that we do not need to memoise the actual support sizes; all is what it\'s important is the delta: ```abs(s1 - s2)```. For example, if for ```i``` rod, first support is ```s1 == 50```, the second is  ```s2 == 30```, and in the final suport sizes matches and equals 200, we will record ```m[i][50 - 30] = 200 - 50``` or ```m[i][20] = 150```. Next time we process ```i``` and support sizes are 100 and 80 (the delta is 20), we know that there are matched size in the end that adds 150 to the larger support: ```m[i][100 - 80] + max(100, 80) = m[i][20] + 100 = 150 + 100 = 250```.
```
int dfs(vector<int>& rs, int i, int s1, int s2, int m_sum, vector<vector<int>> &m) {
  if (s1 > m_sum || s2 > m_sum) return -1;
  if (i >= rs.size()) return s1 == s2 ? s1 : -1;
  if (m[i][abs(s1 - s2)] == -2) {
    m[i][abs(s1 - s2)] = max(-1, max({ dfs(rs, i + 1, s1, s2, m_sum, m),
      dfs(rs, i + 1, s1 + rs[i], s2, m_sum, m), dfs(rs, i + 1, s1, s2 + rs[i], m_sum, m) }) - max(s1, s2));
  }
  return m[i][abs(s1 - s2)] + (m[i][abs(s1 - s2)] == -1 ? 0 : max(s1, s2));
}
int tallestBillboard(vector<int>& rods) {
  int m_sum = accumulate(begin(rods), end(rods), 0) / 2;
  vector<vector<int>> m(rods.size(), vector<int>(m_sum + 1, -2));
  return max(0, dfs(rods, 0, 0, 0, m_sum, m));
}
```
As an additinal optimization, I am also calculating maximum possible billboard (sum of all rods divide by 2), and using it for pruning and vector allocation. As the result, this solution runtime beats most of DP solutions other folks posted.
</p>


