---
title: "Water Bottles"
weight: 1390
#id: "water-bottles"
---
## Description
<div class="description">
<p>Given <code>numBottles</code>&nbsp;full water bottles, you can exchange <code>numExchange</code> empty water bottles for one full water bottle.</p>

<p>The operation of drinking a full water bottle turns it into an empty bottle.</p>

<p>Return the <strong>maximum</strong> number of water bottles you can&nbsp;drink.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/07/01/sample_1_1875.png" style="width: 480px; height: 240px;" /></strong></p>

<pre>
<strong>Input:</strong> numBottles = 9, numExchange = 3
<strong>Output:</strong> 13
<strong>Explanation:</strong> You can exchange 3 empty bottles to get 1 full water bottle.
Number of water bottles you can&nbsp;drink: 9 + 3 + 1 = 13.
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/07/01/sample_2_1875.png" style="width: 790px; height: 290px;" /></p>

<pre>
<strong>Input:</strong> numBottles = 15, numExchange = 4
<strong>Output:</strong> 19
<strong>Explanation:</strong> You can exchange 4 empty bottles to get 1 full water bottle. 
Number of water bottles you can&nbsp;drink: 15 + 3 + 1 = 19.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> numBottles = 5, numExchange = 5
<strong>Output:</strong> 6
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> numBottles = 2, numExchange = 3
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;=&nbsp;numBottles &lt;= 100</code></li>
	<li><code>2 &lt;=&nbsp;numExchange &lt;= 100</code></li>
</ul>
</div>

## Tags
- Greedy (greedy)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] O(logN) and O(1) codes, w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Jul 19 2020 12:02:08 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 21 2020 12:08:37 GMT+0800 (Singapore Standard Time)

<p>
**O(logN):**

As long as there are no less than `numExchange` bottles left, we can drink them and use the empty bottles to exchange full bottle(s).
```java
    public int numWaterBottles(int numBottles, int numExchange) {
        int ans = numBottles;
        while (numBottles >= numExchange) {
            int remainder = numBottles % numExchange;
            numBottles /= numExchange;
            ans += numBottles;
            numBottles += remainder;
        }
        return ans;
    }
```
```python
    def numWaterBottles(self, numBottles: int, numExchange: int) -> int:
        ans = numBottles
        while numBottles >= numExchange:
            numBottles, remainder = divmod(numBottles, numExchange)
            ans += numBottles
            numBottles += remainder
        return ans
```
**Analysis:**

Time: O(log(numBottles)), space: O(1).

----

**O(1):**
**Key observation: A full bottle of water = A empty bottle + One (bottle) unit of water.**

e.g., for test case: 9, 3:
3 empty bottles can exchange a full bottle of water implies 3 empty bottles exchange a empty bottle + one (bottle) unit of water, which in turn means **`(3 - 1) = 2` empty bottles can exchange one (bottle) unit of water**.

Further more, **if you only have `2` empty bottles remaining, you can NOT exchange it for a (bottle) unit of water**. 

Substitue the number `3` above by `numExchange`, we get a important conclusion:
**(numExchange - 1) empty bottles can exchange one (bottle) unit of water, unless you have only numExchange left**.

we need to get the floor value of `numBottles / (numExchange - 1)`, that is why we need to minus `1` additionally in the numerator: `(numBottles - 1)/ (numExchange - 1)`.

See the explanation of a more general question [here](https://math.stackexchange.com/questions/2363298/can-this-problem-be-generalised-mathematically/2363331#2363331) for the derivation details.

```java
    public int numWaterBottles(int numBottles, int numExchange) {
        return numBottles + (numBottles - 1) / (numExchange - 1);
    }
```
```python
    def numWaterBottles(self, numBottles: int, numExchange: int) -> int:
        return numBottles + (numBottles - 1) // (numExchange - 1)
```
**Analysis:**

Time & space: O(1).


</p>


### One-liner in Python
- Author: webtrading
- Creation Date: Sun Jul 19 2020 12:10:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 19 2020 12:10:13 GMT+0800 (Singapore Standard Time)

<p>
```
def numWaterBottles(self, numBottles: int, numExchange: int) -> int:
        return int(numBottles + (numBottles - 1) / (numExchange - 1))
```
Please upvote if you like it
</p>


### Python 1-liner with math explained
- Author: jthunder
- Creation Date: Mon Jul 20 2020 13:55:16 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 20 2020 14:05:13 GMT+0800 (Singapore Standard Time)

<p>
Ok, this is the same code as seen in a bunch of the top posts, but no one seems to be willing to explain the math.

`return numBottles + (numBottles -1)// (numExchange-1)`

or equivalently 

```
return (numBottles*numExchange-1)//(numExchange-1)
```

This problem is the geometric series.

For example, if numExchange = 3, that means for every 3 bottles, we get one back. This can be represented for the first exchange as numBottles * (1/3). For the second exchange, we take the number of bottles left after the first exchange, and multiply by (1/3) again.

first = numBottles * (1/3)
second = numBottles * (1/3) * (1/3) = numBottles * (1/3)^2
nth = numBottles * (1/3) ^ n

Summing them up, we get numBottles * (1/3 + 1/9 + ... (1/3)^n), aka the geometric series.

https://en.wikipedia.org/wiki/Geometric_series

Taking the wiki solution for the geometric series to the nth term, we get a(1-r^n) / (1-r), where r is our (1/numExchange), and n is the number of steps, or 1 + # exchanges we want from the series(counting the 0th exchange as 1), and a = numBottles.

n = log_numExchange (numBottles) + 1, the one counting the first term with r^0

for example, if we have 9 bottles, and exchange rate of 3, the number of exchanges we can do is equal to the log (base 3) of 9 = 2.

In this example, a*r^n = r is demonstrated by 9 * (1/3)^(2+1) = 1/3

Simplifying the equation gives:
a(1-r^n) / (1-r)
= (a - a*r^n) / (1-r)
= (a - r) / (1-r)
This line here is represented by
```
numBottles-numExchange**-1)//(1 - numExchange**-1)
```
Which annoyingly does not pass all test cases due to floating point precision, so we reduce the number of divisions required by simplifying further, and eliminating the 1/numExchange, until we get

```
return (numBottles*numExchange-1)//(numExchange-1)
```
</p>


