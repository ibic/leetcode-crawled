---
title: "Evaluate Boolean Expression"
weight: 1578
#id: "evaluate-boolean-expression"
---
## Description
<div class="description">
<p>Table <code>Variables</code>:</p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| name          | varchar |
| value         | int     |
+---------------+---------+
name is the primary key for this table.
This table contains the stored variables and their values.
</pre>

<p>&nbsp;</p>

<p>Table <code>Expressions</code>:</p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| left_operand  | varchar |
| operator      | enum    |
| right_operand | varchar |
+---------------+---------+
(left_operand, operator, right_operand) is the primary key for this table.
This table contains a boolean expression that should be evaluated.
operator is an enum that takes one of the values (&#39;&lt;&#39;, &#39;&gt;&#39;, &#39;=&#39;)
The values of left_operand and right_operand are guaranteed to be in the Variables table.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to evaluate the boolean expressions in <code>Expressions</code> table.</p>

<p>Return the result table in any order.</p>

<p>The query result format is in the following example.</p>

<pre>
Variables table:
+------+-------+
| name | value |
+------+-------+
| x    | 66    |
| y    | 77    |
+------+-------+

Expressions table:
+--------------+----------+---------------+
| left_operand | operator | right_operand |
+--------------+----------+---------------+
| x            | &gt;        | y             |
| x            | &lt;        | y             |
| x            | =        | y             |
| y            | &gt;        | x             |
| y            | &lt;        | x             |
| x            | =        | x             |
+--------------+----------+---------------+

Result table:
+--------------+----------+---------------+-------+
| left_operand | operator | right_operand | value |
+--------------+----------+---------------+-------+
| x            | &gt;        | y             | false |
| x            | &lt;        | y             | true  |
| x            | =        | y             | false |
| y            | &gt;        | x             | true  |
| y            | &lt;        | x             | false |
| x            | =        | x             | true  |
+--------------+----------+---------------+-------+
As shown, you need find the value of each boolean exprssion in the table using the variables table.
</pre>
</div>

## Tags


## Companies
- Point72 - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy MySQL
- Author: yehong
- Creation Date: Thu May 07 2020 03:38:44 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 07 2020 03:38:44 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT e.left_operand, e.operator, e.right_operand,
    (
        CASE
            WHEN e.operator = \'<\' AND v1.value < v2.value THEN \'true\'
            WHEN e.operator = \'=\' AND v1.value = v2.value THEN \'true\'
            WHEN e.operator = \'>\' AND v1.value > v2.value THEN \'true\'
            ELSE \'false\'
        END
    ) AS value
FROM Expressions e
JOIN Variables v1
ON e.left_operand = v1.name
JOIN Variables v2
ON e.right_operand = v2.name
```
</p>


### Only Joins - simplest and cleanest solution
- Author: bunxi
- Creation Date: Thu Jul 23 2020 09:43:55 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 23 2020 09:43:55 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT e.left_operand, e.operator, e.right_operand,
       CASE WHEN operator = \'>\' THEN IF(v1.value > v2.value, \'true\', \'false\')
            WHEN operator = \'<\' THEN IF(v1.value < v2.value, \'true\', \'false\')
            ELSE IF(v1.value = v2.value, \'true\', \'false\')
       END AS value
FROM Expressions e
JOIN Variables v1 ON v1.name = e.left_operand
JOIN Variables v2 ON v2.name = e.right_operand;
```
</p>


### Clean solution
- Author: rd255
- Creation Date: Sat May 16 2020 11:21:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 16 2020 11:21:25 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT A.LEFT_OPERAND, A.OPERATOR, A.RIGHT_OPERAND,
        CASE WHEN A.OPERATOR = \'>\' AND B.VALUE > C.VALUE THEN \'true\'
             WHEN A.OPERATOR = \'<\' AND B.VALUE < C.VALUE THEN \'true\'
             WHEN A.OPERATOR = \'=\' AND B.VALUE = C.VALUE THEN \'true\'
        ELSE \'false\' END AS VALUE        
FROM EXPRESSIONS A
JOIN 
VARIABLES B
ON A.LEFT_OPERAND = B.NAME
INNER JOIN
VARIABLES C
ON A.RIGHT_OPERAND = C.NAME;
```
</p>


