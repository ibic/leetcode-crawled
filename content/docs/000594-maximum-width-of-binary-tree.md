---
title: "Maximum Width of Binary Tree"
weight: 594
#id: "maximum-width-of-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree, write a function to get the maximum width of the given tree. The maximum width of a tree is the maximum width among all levels.</p>

<p>The width of one level is defined as the length between the end-nodes (the leftmost and right most non-null nodes in the level, where the <code>null</code> nodes between the end-nodes are also counted into the length calculation.</p>

<p>It is <strong>guaranteed</strong> that the answer will in the range of 32-bit signed integer.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> 

           1
         /   \
        3     2
       / \     \  
      5   3     9 

<b>Output:</b> 4
<b>Explanation:</b> The maximum width existing in the third level with the length 4 (5,3,null,9).
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> 

          1
         /  
        3    
       / \       
      5   3     

<b>Output:</b> 2
<b>Explanation:</b> The maximum width existing in the third level with the length 2 (5,3).
</pre>

<p><b>Example 3:</b></p>

<pre>
<b>Input:</b> 

          1
         / \
        3   2 
       /        
      5      

<b>Output:</b> 2
<b>Explanation:</b> The maximum width existing in the second level with the length 2 (3,2).
</pre>

<p><b>Example 4:</b></p>

<pre>
<b>Input:</b> 

          1
         / \
        3   2
       /     \  
      5       9 
     /         \
    6           7
<b>Output:</b> 8
<b>Explanation:</b>The maximum width existing in the fourth level with the length 8 (6,null,null,null,null,null,null,7).
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The&nbsp;given binary tree will have between&nbsp;<code>1</code>&nbsp;and&nbsp;<code>3000</code>&nbsp;nodes.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 3 (taggedByAdmin: true)
- eBay - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Overview

The problem defines the concept of **_width_** for the binary tree.
In essence, it is about binary tree traversal, since we need to traverse the tree in order to measure its width.

As one would probably know, the common strategies to traverse a binary tree are Breadth-First Search (_a.k.a._ BFS) and Depth-First Search (_a.k.a._ DFS).
Furthermore, the DFS strategy can be distinguished as _preorder_ DFS, _inorder_ DFS and _postorder_ DFS, depending on the relative order of visit among the node itself and its child nodes.

If one is not familiar with the concepts of BFS and DFS, we have an Explore card called [Queue & Stack](https://leetcode.com/explore/learn/card/queue-stack/) where we cover the [BFS traversal](https://leetcode.com/explore/learn/card/queue-stack/231/practical-application-queue/) as well as the [DFS traversal](https://leetcode.com/explore/learn/card/queue-stack/232/practical-application-stack/). Hence, in this article, we won't repeat ourselves on these concepts.

**Intuition**

>The key to solve the problem though lie on how we **index** the nodes that are on the same level.

Suppose that the indices for the first and the last nodes of one particular level are $$C_1$$ and $$C_n$$ respectively, we could then calculate the _width_ of this level as $$C_n - C_i + 1$$.

Now, let us try to come up with a schema to index the nodes, so that the problem can be solved easily with the above formula.

As we know, for a _full_ binary tree, the number of nodes double at each level, since each parent node has two child nodes.
Naturally, the range of our node index would double as well.

>If the index of a parent node is $$C_i$$, accordingly we can define the index of its **_left_** child node as $$2\cdot C_i$$ and the index of its **_right_** child node as $$2 \cdot C_i + 1$$.

In the following graph, we show an example of how the index works for a full binary tree, where on each node we label its index rather than its value.

![full binary tree with index](../Figures/662/662_full_binary_tree.png)

With the above indexing schema, we manage to assign a unique index for each node on the same level, and in addition there is no _gap_ among all the indices if it is a full binary tree.

For a non-full binary tree, the relationship between the indices of a parent and its child node still holds.

Now that we have an indexing schema, all we need to do is to assign an index for each node in the tree.
Once it is done, we can calculate the _width_ for each level, and finally we could return the maximal value among them as the solution.

Voila. This is the key insight to solve the problem. With this hint, we believe that one could definitely come up with some solutions.

As a spoiler alert, we will cover how to implement different solutions with BFS and DFS traversal strategies in the remaining sections.

---
#### Approach 1: BFS Traversal

**Intuition**

Naturally, one might resort to the BFS traversal.
After all, the width is measured among the nodes on the same level.
So let us get down to the BFS traversal first.

There are several ways to implement the BFS traversal. Almost all of them share a common point, _i.e._ using the `queue` data structure to maintain the order of visits.

In brief, we push the nodes into the queue level by level.
As a result, the priorities of visiting would roll out from top to down and from left to right, due to the FIFO (First-In First-Out) principle of the queue data structure, _i.e._ the element that enters the queue first would exit first as well.

![BFS traversal](../Figures/662/662_bfs_traversal.png)

In the above graph, we show an example of BFS traversal on a full binary tree where we indicate the _global_ order of visiting along with each node.

**Algorithm**

Here are a few steps to implement a solution with the BFS traversal.

- First of all, we create a `queue` data structure, which would be used to hold elements of tuple as `(node, col_index)`, where the `node` is the tree node and the `col_index` is the corresponding index that is assigned to the node based on our indexing schema.
Also, we define a global variable called `max_width` which holds the maximal width that we've seen so far.

- Then we append the root node along with its index 0, to kick off the BFS traversal.

- The BFS traversal is basically an iteration over the elements of queue. We visit the nodes _level by level_ until there are no more elements in the queue.

    - At the end of each level, we use the indices of the first and the last elements on the same level, in order to obtain the width of the level.

- At the end of BFS traversal, we then return the maximal width that we've seen over all levels.

<iframe src="https://leetcode.com/playground/iD9irdqP/shared" frameBorder="0" width="100%" height="500" name="iD9irdqP"></iframe>


**Note:** in the above implementation, we use the `size` of the queue as a delimiter to determine the boundary between each levels.

One could also use a specific dummy element as a marker to separate nodes of different levels in the queue.


**Complexity Analysis**

Let $$N$$ be the total number of nodes in the input tree.

- Time Complexity: $$\mathcal{O}(N)$$ 

    - We visit each node once and only once. And at each visit, it takes a constant time to process.


- Space Complexity: $$\mathcal{O}(N)$$

    - We used a queue to maintain the nodes along with its indices, which is the main memory consumption of the algorithm.

    - Due to the nature of BFS, at any given moment, the queue holds no more than *two levels* of nodes. In the worst case, a level in a full binary tree contains at most half of the total nodes (_i.e._ $$\frac{N}{2}$$), _i.e._ this is also the level where the leaf nodes reside.

    - Hence, the overall space complexity of the algorithm is $$\mathcal{O}(N)$$.
<br/>
<br/>

---
#### Approach 2: DFS Traversal

**Intuition**

Although it is definitely more intuitive to implement a solution with BFS traversal, it is not impossible to do it with DFS.

It might sound twisted, but we don't need to visit the nodes strictly in the order of BFS.
All we need is to compare the indices between the first and the last elements of the same level.

>We could build a table that records the indices of nodes grouped by level.
Then we could scan the indices level by level to obtain the **_maximal_** difference among them, which is also the width of the level.

With the above idea, as we can see, any traversal will do, including the BFS and DFS.

>Better yet, we don't need to keep the indices of the entire level, but the first and the last index.

We could use the table to keep **only** the index of the first element for each level, _i.e._ `depth -> first_col_index`, which we illustrate in the following graph.

![tree with first_col_index](../Figures/662/662_first_col_index.png)

Along with the traversal, we could compare the index of every node with the corresponding first index of its level (_i.e._ `first_col_index`).

Rather than keeping all the indices in the table, we save time and space by keeping only the index of the first element per level.

**Algorithm**

The tricky part is how we can obtain the index for the first element of each level.

As we discussed before, we use a table with `depth` of the node as the key and the index of the first element for that depth (level) as the value.

>If we can make sure that we visit the first element of a level before the rest of elements on that level, we then can easily populate the table along with the traversal.

In fact, a DFS traversal can assure the above priority that we desire.
Even better, it could be either _preorder_, _inorder_ or _postorder_ DFS traversal, as long as we **_prioritize_** the visit of the left child node over the right child node.

>Although in principle DFS prioritizes depth over breadth, it could also ensure the _level-wise_ priority.
By visiting the left node before the right child node in DFS traversal, we can ensure that the nodes that lean more to the left got visited earlier.

We showcase a **preorder DFS** traversal, with an example in the following graph:

![DFS traversal](../Figures/662/662_preorder_dfs_traversal.png)

We label each node with a number that indicates the _global_ order of visit.
As one can see, the nodes at the same level do get visited from left to right.
For instance, on the second level, the first node would be visited at the step 2, while the next node at the same level would be visited at the step 5.

We give some sample implementations of DFS in the following.

<iframe src="https://leetcode.com/playground/MjhhC66k/shared" frameBorder="0" width="100%" height="500" name="MjhhC66k"></iframe>



**Complexity Analysis**

Let $$N$$ be the total number of nodes in the input tree.

- Time Complexity: $$\mathcal{O}(N)$$.

    - Similar to the BFS traversal, we visit each node once and only once in DFS traversal. And each visit takes a constant time to process as well.


- Space Complexity: $$\mathcal{O}(N)$$

    - Unlike the BFS traversal, we used an additional table to keep the index for the first element per level.
    In the worst case where the tree is extremely skewed, there could be as many levels as the number of nodes.
    As a result, the space complexity of the table would be $$\mathcal{O}(N)$$.

    - Since we implement DFS traversal with recursion which would incur some additional memory consumption in the function call stack, we need to take this into account for the space complexity.

    - The consumption of function stack is proportional to the depth of recursion. Again, in the same worst case above, where the tree is extremely skewed, the depth of the recursion would be equal to the number of nodes in the tree.
    Therefore, the space complexity of the function stack would be $$\mathcal{O}(N)$$.

    - To sum up, the overall space complexity of the algorithm is $$\mathcal{O}(N) + \mathcal{O}(N) = \mathcal{O}(N)$$.
<br/>

---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] Very simple dfs solution
- Author: caihao0727mail
- Creation Date: Sun Aug 20 2017 11:14:21 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 09:01:51 GMT+0800 (Singapore Standard Time)

<p>
We know that  a binary tree can be represented by an array  (assume the root begins from the position with  index ```1``` in the array). If the index of a node is ```i```, the indices of its two children are ```2*i ``` and ```2*i + 1```. The idea is to use two arrays (```start[]``` and ```end[]```) to record the the indices of the leftmost node and rightmost node in each level, respectively. For each level of the tree, the width is  ```end[level] - start[level] + 1```. Then, we just need to find the maximum width.

Java version:
```
    public int widthOfBinaryTree(TreeNode root) {
        return dfs(root, 0, 1, new ArrayList<Integer>(), new ArrayList<Integer>());
    }
    
    public int dfs(TreeNode root, int level, int order, List<Integer> start, List<Integer> end){
        if(root == null)return 0;
        if(start.size() == level){
            start.add(order); end.add(order);
        }
        else end.set(level, order);
        int cur = end.get(level) - start.get(level) + 1;
        int left = dfs(root.left, level + 1, 2*order, start, end);
        int right = dfs(root.right, level + 1, 2*order + 1, start, end);
        return Math.max(cur, Math.max(left, right));
    }
```

C++ version (use ```vector<pair<int,int>>``` to replace the arrays ```start``` and ```end``` in Java ):
```
   int widthOfBinaryTree(TreeNode* root) {
        return dfs(root, 0, 1, vector<pair<int, int>>() = {});
    }
    
    int dfs(TreeNode* root, int level, int order, vector<pair<int, int>>& vec){
        if(root == NULL)return 0;
        if(vec.size() == level)vec.push_back({order, order});
        else vec[level].second = order;
        return max({vec[level].second - vec[level].first + 1, dfs(root->left, level + 1, 2*order, vec), dfs(root->right, level + 1, 2*order + 1, vec)});
    }
```
</p>


### Java One Queue Solution with HashMap
- Author: Candle309
- Creation Date: Sun Aug 20 2017 12:57:44 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 08:19:47 GMT+0800 (Singapore Standard Time)

<p>
The idea is to traverse all the node in the tree in level order(Here I use one Queue to store each level's nodes. The time I traverse each level is the queue's size(the number of nodes from upper level)). Each time a node is traversed, the position of the node(as it is in a full binary tree) is stored in the HashMap. If the position of the parent node is 'n', then the left child is '2 * n' and the right child is '2 * n + 1'. The width of each level is the last node's position in this level subtracts the first node's position in this level plus 1.
```
class Solution {
    public int widthOfBinaryTree(TreeNode root) {
        if(root == null) return 0;
        Queue<TreeNode> q = new LinkedList<TreeNode>();
        Map<TreeNode, Integer> m = new HashMap<TreeNode, Integer>();
        q.offer(root);
        m.put(root, 1);
        int curW = 0;
        int maxW = 0;
        while(!q.isEmpty()){
            int size = q.size();
            int start = 0;
            int end = 0;
            for(int i = 0; i < size; i++){
                TreeNode node = q.poll();
                if(i == 0) start = m.get(node);
                if(i == size - 1) end = m.get(node);
                if(node.left != null){
                    m.put(node.left, m.get(node) * 2);
                    q.offer(node.left);
                }
                if(node.right != null){
                    m.put(node.right, m.get(node) * 2 + 1);
                    q.offer(node.right);
                }
            }
            curW = end - start + 1;
            maxW = Math.max(curW, maxW);
        }
        return maxW;
    }
}
</p>


### [C++/Java] * [BFS/DFS/3liner] Clean Code  With Explanation
- Author: alexander
- Creation Date: Sun Aug 20 2017 11:17:37 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 03:33:44 GMT+0800 (Singapore Standard Time)

<p>
The idea is to use heap indexing:
```
        1
   2         3
 4   5     6   7
8 9 x 11  x 13 x 15
```
Regardless whether these nodes exist:
- Always make the id of left child as `parent_id * 2`;
- Always make the id of right child as `parent_id * 2 + 1`;

So we can just:
1. Record the `id` of `left most node` when first time at each level of the tree during an pre-order run.(you can tell by check the size of the container to hold the first nodes);
2. At each node, compare the `distance` from it the left most node with the current `max width`;

**DFS - Return Value**
C++
```
class Solution {
public:
    int widthOfBinaryTree(TreeNode* root) {
        vector<int> lefts; // left most nodes at each level;
        return dfs(root, 1, 0, lefts);
    }
private:
    int dfs(TreeNode* n, int id, int d, vector<int>& lefts) { // d : depth
        if (!n) return 0;
        if (d >= lefts.size()) lefts.push_back(id);  // add left most node
        return max({id + 1 - lefts[d], dfs(n->left, id * 2, d + 1, lefts), dfs(n->right, id * 2 + 1, d + 1, lefts)});
    }
};
```
3 liner
```
class Solution {
public:
    int widthOfBinaryTree(TreeNode* root) {
        unordered_map<int, vector<int>> m;
        function<int(TreeNode*, int, int)> dfs = [&](TreeNode* n, int id, int d){ return n ? m[d].push_back(id), max({id+1-m[d][0], dfs(n->left, id*2, d+1), dfs(n->right, id*2+1, d+1)}) : 0; };
        return dfs(root, 1, 0);
    }
};
```
Java
```
class Solution {
    public int widthOfBinaryTree(TreeNode root) {
        List<Integer> lefts = new ArrayList<Integer>(); // left most nodes at each level;
        return dfs(root, 1, 0, lefts);
    }

    private int dfs(TreeNode n, int id, int d, List<Integer> lefts) { // d : depth
        if (n == null) return 0;
        if (d >= lefts.size()) lefts.add(id);   // add left most node
        return Math.max(id + 1 - lefts.get(d), Math.max(dfs(n.left, id*2, d+1, lefts), dfs(n.right, id*2+1, d+1, lefts)));
    }
}
```
**DFS - Side Effect**
C++
```
class Solution {
public:
    int widthOfBinaryTree(TreeNode* root) {
        vector<int> lefts; // left most nodes at each level;
        int maxwidth = 0;
        dfs(root, 1, 0, lefts, maxwidth);
        return maxwidth;
    }
private:
    void dfs(TreeNode* node, int id, int depth, vector<int>& lefts, int& maxwidth) {
        if (!node) return;
        if (depth >= lefts.size()) lefts.push_back(id);  // add left most node
        maxwidth = max(maxwidth, id + 1 - lefts[depth]);
        dfs(node->left, id * 2, depth + 1, lefts, maxwidth);
        dfs(node->right, id * 2 + 1, depth + 1, lefts, maxwidth);
    }
};
```
Java
```
class Solution {
    public int widthOfBinaryTree(TreeNode root) {
        List<Integer> lefts = new ArrayList<Integer>(); // left most nodes at each level;
        int[] res = new int[1]; // max width
        dfs(root, 1, 0, lefts, res);
        return res[0];
    }
    private void dfs(TreeNode node, int id, int depth, List<Integer> lefts, int[] res) {
        if (node == null) return;
        if (depth >= lefts.size()) lefts.add(id);   // add left most node
        res[0] = Integer.max(res[0], id + 1 - lefts.get(depth));
        dfs(node.left,  id * 2,     depth + 1, lefts, res);
        dfs(node.right, id * 2 + 1, depth + 1, lefts, res);
    }
}
```
**BFS**
C++
```
class Solution {
public:
    int widthOfBinaryTree(TreeNode* root) {
        if (!root) return 0;
        int max = 0;
        queue<pair<TreeNode*, int>> q;
        q.push(pair<TreeNode*, int>(root, 1));
        while (!q.empty()) {
            int l = q.front().second, r = l; // right started same as left
            for (int i = 0, n = q.size(); i < n; i++) {
                TreeNode* node = q.front().first;
                r = q.front().second;
                q.pop();
                if (node->left) q.push(pair<TreeNode*, int>(node->left, r * 2));
                if (node->right) q.push(pair<TreeNode*, int>(node->right, r * 2 + 1));
            }
            max = std::max(max, r + 1 - l);
        }
        return max;
    }
};
```
Java
```
import java.util.AbstractMap;
class Solution {
    public int widthOfBinaryTree(TreeNode root) {
        if (root == null) return 0;
        int max = 0;
        Queue<Map.Entry<TreeNode, Integer>> q = new LinkedList<Map.Entry<TreeNode, Integer>>();
        q.offer(new AbstractMap.SimpleEntry(root, 1));

        while (!q.isEmpty()) {
            int l = q.peek().getValue(), r = l; // right started same as left
            for (int i = 0, n = q.size(); i < n; i++) {
                TreeNode node = q.peek().getKey();
                r = q.poll().getValue();
                if (node.left != null) q.offer(new AbstractMap.SimpleEntry(node.left, r * 2));
                if (node.right != null) q.offer(new AbstractMap.SimpleEntry(node.right, r * 2 + 1));
            }
            max = Math.max(max, r + 1 - l);
        }

        return maxwidth;
    }
}
```
</p>


