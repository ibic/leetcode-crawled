---
title: "Map Sum Pairs"
weight: 609
#id: "map-sum-pairs"
---
## Description
<div class="description">
<p>
Implement a MapSum class with <code>insert</code>, and <code>sum</code> methods.
</p>

<p>
For the method <code>insert</code>, you'll be given a pair of (string, integer). The string represents the key and the integer represents the value. If the key already existed, then the original key-value pair will be overridden to the new one.
</p>

<p>
For the method <code>sum</code>, you'll be given a string representing the prefix, and you need to return the sum of all the pairs' value whose key starts with the prefix.
</p>

<p><b>Example 1:</b><br />
<pre>
Input: insert("apple", 3), Output: Null
Input: sum("ap"), Output: 3
Input: insert("app", 2), Output: Null
Input: sum("ap"), Output: 5
</pre>
</p>

</div>

## Tags
- Trie (trie)

## Companies
- Akuna Capital - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Brute Force [Accepted]

**Intuition and Algorithm**

For each key in the map, if that key starts with the given prefix, then add it to the answer.

<iframe src="https://leetcode.com/playground/jNhyy639/shared" frameBorder="0" name="jNhyy639" width="100%" height="360"></iframe>
**Complexity Analysis**

* Time Complexity: Every insert operation is $$O(1)$$.  Every sum operation is $$O(N * P)$$ where $$N$$ is the number of items in the map, and $$P$$ is the length of the input prefix.

* Space Complexity: The space used by `map` is linear in the size of all input `key` and `val` values combined.

---

#### Approach #2: Prefix Hashmap [Accepted]

**Intuition and Algorithm**

We can remember the answer for all possible prefixes in a HashMap `score`.  When we get a new `(key, val)` pair, we update every prefix of `key` appropriately: each prefix will be changed by `delta = val - map[key]`, where `map` is the previous associated value of `key` (zero if undefined.)


<iframe src="https://leetcode.com/playground/QYzALHGM/shared" frameBorder="0" name="QYzALHGM" width="100%" height="394"></iframe>

**Complexity Analysis**

* Time Complexity: Every insert operation is $$O(K^2)$$, where $$K$$ is the length of the key, as $$K$$ strings are made of an average length of $$K$$.  Every sum operation is $$O(1)$$.

* Space Complexity: The space used by `map` and `score` is linear in the size of all input `key` and `val` values combined.

---

#### Approach #3: Trie [Accepted]

**Intuition and Algorithm**

Since we are dealing with prefixes, a Trie (prefix tree) is a natural data structure to approach this problem.  For every node of the trie corresponding to some prefix, we will remember the desired answer (score) and store it at this node.  As in *Approach #2*, this involves modifying each node by `delta = val - map[key]`.

<iframe src="https://leetcode.com/playground/FbmbbgFJ/shared" frameBorder="0" name="FbmbbgFJ" width="100%" height="513"></iframe>



**Complexity Analysis**

* Time Complexity: Every insert operation is $$O(K)$$, where $$K$$ is the length of the key.  Every sum operation is $$O(K)$$.

* Space Complexity: The space used is linear in the size of the total input.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution, Trie
- Author: shawngao
- Creation Date: Sun Sep 17 2017 13:56:48 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 01:41:16 GMT+0800 (Singapore Standard Time)

<p>
```
class MapSum {
    class TrieNode {
        Map<Character, TrieNode> children;
        boolean isWord;
        int value;

        public TrieNode() {
            children = new HashMap<Character, TrieNode>();
            isWord = false;
            value = 0;
        }
    }
    
    TrieNode root;
    
    /** Initialize your data structure here. */
    public MapSum() {
        root = new TrieNode();
    }
    
    public void insert(String key, int val) {
        TrieNode curr = root;
        for (char c : key.toCharArray()) {
            TrieNode next = curr.children.get(c);
            if (next == null) {
                next = new TrieNode();
                curr.children.put(c, next);
            }
            curr = next;
        }
        curr.isWord = true;
        curr.value = val;
    }
    
    public int sum(String prefix) {
        TrieNode curr = root;
	for (char c : prefix.toCharArray()) {
	    TrieNode next = curr.children.get(c);
	    if (next == null) {
	        return 0;
	    }
	    curr = next;
        }
		
        return dfs(curr);
    }
    
    private int dfs(TrieNode root) {
        int sum = 0;
        for (char c : root.children.keySet()) {
            sum += dfs(root.children.get(c));
        }
        return sum + root.value;
    }
}
```
</p>


### C++, Easy solution, ordered map
- Author: zestypanda
- Creation Date: Mon Sep 18 2017 00:11:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 13:57:28 GMT+0800 (Singapore Standard Time)

<p>
```
class MapSum {
public:
    /** Initialize your data structure here. */    
    void insert(string key, int val) {
        mp[key] = val;
    }
    
    int sum(string prefix) {
        int sum = 0, n = prefix.size();
        for (auto it = mp.lower_bound(prefix); it != mp.end() && it->first.substr(0, n) == prefix; it++) 
            sum += it->second;
        return sum;
    }
private:
    map<string, int> mp;
};
```
</p>


### [Python] 3 Lines Solution
- Author: lee215
- Creation Date: Sun Sep 17 2017 19:28:51 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 08 2019 12:47:15 GMT+0800 (Singapore Standard Time)

<p>
```
class MapSum(object):

    def __init__(self): 
        self.d = {}

    def insert(self, key, val): 
        self.d[key] = val

    def sum(self, prefix):
        return sum(self.d[i] for i in self.d if i.startswith(prefix))
````
Edited after Stefan\'s suggestion.
</p>


