---
title: "Find Elements in a Contaminated Binary Tree"
weight: 1201
#id: "find-elements-in-a-contaminated-binary-tree"
---
## Description
<div class="description">
<p>Given a&nbsp;binary tree with the following rules:</p>

<ol>
	<li><code>root.val == 0</code></li>
	<li>If <code>treeNode.val == x</code> and <code>treeNode.left != null</code>, then <code>treeNode.left.val == 2 * x + 1</code></li>
	<li>If <code>treeNode.val == x</code> and <code>treeNode.right != null</code>, then <code>treeNode.right.val == 2 * x + 2</code></li>
</ol>

<p>Now the binary tree is contaminated, which means all&nbsp;<code>treeNode.val</code>&nbsp;have&nbsp;been changed to <code>-1</code>.</p>

<p>You need to first recover the binary tree and then implement the <code>FindElements</code> class:</p>

<ul>
	<li><code>FindElements(TreeNode* root)</code>&nbsp;Initializes the object with a&nbsp;contamined binary tree, you need to recover it first.</li>
	<li><code>bool find(int target)</code>&nbsp;Return if the <code>target</code> value exists in the recovered binary tree.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/11/06/untitled-diagram-4-1.jpg" style="width: 320px; height: 119px;" /></strong></p>

<pre>
<strong>Input</strong>
[&quot;FindElements&quot;,&quot;find&quot;,&quot;find&quot;]
[[[-1,null,-1]],[1],[2]]
<strong>Output</strong>
[null,false,true]
<strong>Explanation</strong>
FindElements findElements = new FindElements([-1,null,-1]); 
findElements.find(1); // return False 
findElements.find(2); // return True </pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/11/06/untitled-diagram-4.jpg" style="width: 400px; height: 198px;" /></strong></p>

<pre>
<strong>Input</strong>
[&quot;FindElements&quot;,&quot;find&quot;,&quot;find&quot;,&quot;find&quot;]
[[[-1,-1,-1,-1,-1]],[1],[3],[5]]
<strong>Output</strong>
[null,true,true,false]
<strong>Explanation</strong>
FindElements findElements = new FindElements([-1,-1,-1,-1,-1]);
findElements.find(1); // return True
findElements.find(3); // return True
findElements.find(5); // return False</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/11/07/untitled-diagram-4-1-1.jpg" style="width: 306px; height: 274px;" /></strong></p>

<pre>
<strong>Input</strong>
[&quot;FindElements&quot;,&quot;find&quot;,&quot;find&quot;,&quot;find&quot;,&quot;find&quot;]
[[[-1,null,-1,-1,null,-1]],[2],[3],[4],[5]]
<strong>Output</strong>
[null,true,false,false,true]
<strong>Explanation</strong>
FindElements findElements = new FindElements([-1,null,-1,-1,null,-1]);
findElements.find(2); // return True
findElements.find(3); // return False
findElements.find(4); // return False
findElements.find(5); // return True
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>TreeNode.val == -1</code></li>
	<li>The height of the binary tree is less than or equal to <code>20</code></li>
	<li>The total number of nodes is between <code>[1,&nbsp;10^4]</code></li>
	<li>Total calls of <code>find()</code> is between <code>[1,&nbsp;10^4]</code></li>
	<li><code>0 &lt;= target &lt;= 10^6</code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Tree (tree)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python Special Way for find() without HashSet  O(1) Space O(logn) Time
- Author: qingdu_river
- Creation Date: Sun Nov 17 2019 13:32:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 13:32:30 GMT+0800 (Singapore Standard Time)

<p>
It\'s obvious to use `BFS` for the initial part. However, a lot of people use HashSet(`set()` in python) to pre-store all the values in the initial part, which may cause MLE when the values are huge. There is a special way to implement `find()` that  costs O(1) in space and O(logn) in time. 

Firstly, let\'s see what a complete tree will look like in this problem: 


It\'s very easy to find that numbers in each layer range from `[2^i-1,2^(i+1)-2]`
what if we add 1 to each number? Then it should range from `[2^i, 2^(i+1)-1]`
See?  the binary of all numbers in each layer should be: `100..00` to `111...11`

Hence we could discover that maybe we could use the `binary number` of `target+1` to find a path:

![image](https://assets.leetcode.com/users/qingdu_river/image_1573968285.png)

I\'m not proficient in English, so I would prefer to show my code here to explain my idea:

```python
def find(self, target: int) -> bool:
		binary = bin(target+1)[3:]                  # remove the useless first `1`
        index = 0
        root = self.root                                    # use a new pointer `root` to traverse the tree
        while root and index <= len(binary): # traverse the binary number from left to right
            if root.val == target:
                return True
            if  binary[index] == \'0\':  # if it\'s 0, we have to go left
                root = root.left
            else:  # if it\'s 1, we have to go right
                root = root.right
            index += 1
        return False
```
</p>


### [Java/Python 3] DFS and BFS clean codes w/ analysis.
- Author: rock
- Creation Date: Sun Nov 17 2019 12:10:57 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 25 2020 00:42:13 GMT+0800 (Singapore Standard Time)

<p>
**DFS**

```java
    private Set<Integer> seen = new HashSet<>();
    
    public FindElements(TreeNode root) {
        dfs(root, 0);
    }
    private void dfs(TreeNode n, int v) {
        if (n == null) return;
        seen.add(v);
        n.val = v;
        dfs(n.left, 2 * v + 1);
        dfs(n.right, 2 * v + 2);
    }
    
    public boolean find(int target) {
        return seen.contains(target);
    }
```
```python
    def __init__(self, root: TreeNode):
        self.seen = set()
        
        def dfs(node: TreeNode, v: int) -> None:
            if node:
                node.val = v    
                self.seen.add(v)
                dfs(node.left, 2 * v + 1)
                dfs(node.right, 2 * v + 2)
            
        dfs(root, 0)
        
    def find(self, target: int) -> bool:
        return target in self.seen
```

----

**BFS** - inspired by **@MichaelZ**.
```java
    private Set<Integer> seen = new HashSet<>();
    
    public FindElements(TreeNode root) {
        if (root != null) {
            root.val = 0;
            seen.add(root.val);
            bfs(root);
        }
    }
    
    public boolean find(int target) {
        return seen.contains(target);
    }
    
    private void bfs(TreeNode node) {
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(node);
        while (!q.isEmpty()) {
            TreeNode cur = q.poll();
            if (cur.left != null) {
                cur.left.val = 2 * cur.val + 1;
                q.offer(cur.left);
                seen.add(cur.left.val);
            }
            if (cur.right != null) {
                cur.right.val = 2 * cur.val + 2;
                q.offer(cur.right);
                seen.add(cur.right.val);
            }
        }
    }
```
```python
    def __init__(self, root: TreeNode):

        def bfs(root: TreeNode) -> None:
            dq = collections.deque([root])
            while dq:
                node = dq.popleft()
                if node.left:
                    node.left.val = 2 * node.val + 1
                    dq.append(node.left)
                    self.seen.add(node.left.val)
                if node.right:
                    node.right.val = 2 * node.val + 2
                    dq.append(node.right)
                    self.seen.add(node.right.val)
            
        self.seen = set()
        if root:
            root.val = 0
            bfs(root)
        
    def find(self, target: int) -> bool:
        return target in self.seen    
```

**Analysis:**
HashSet cost space `O(N)`, *dfs()* cost space ((H) and time `O(N)`, *bfs()* cost time and space O(N), therefore

*FindElements() (dfs() and bfs()) cost*
time & space: `O(N)`.

*find() cost*
time & space: `O(1)` excluding the space of HashSet.

Where `N` is the total number of nodes in the tree.
</p>


### Java bit path  Time : O(logn)
- Author: JiaweiZhang1994
- Creation Date: Sun Nov 17 2019 16:21:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 18 2019 10:50:24 GMT+0800 (Singapore Standard Time)

<p>
Think about it with complete binary tree and the whole idea is to binary serialize the target number. It\'s actually tha order we doing the dfs.
Let\'s say our target : 9 we shall take it as 10 : 1010  ignoring the first digit 1 the path ( 010 )  means left -> right -> left  
So \'0\' means going to the left and \'1\' means going to the right.
Here is the picture, very straightforward.

![image](https://assets.leetcode.com/users/jiaweizhang1994/image_1574045418.png)









	TreeNode root;

    public FindElements(TreeNode root) {
        this.root = root;
    }
    
    public boolean find(int target) {
        return dfs(root, binSerialize(target + 1), 1);
    }
	
	public boolean dfs(TreeNode root, String str, int pos){
        if(root == null)    return false;
        if(pos == str.length()) return true;
        return str.charAt(pos) == \'0\'? dfs(root.left, str, pos+1) : dfs(root.right, str, pos+1);
    }
    
    public String binSerialize(int num){
        StringBuilder sb = new StringBuilder();
        while(num > 0){
            sb.insert(0, num & 1);
            num /= 2;
        }
        return sb.toString();
    }
    


</p>


