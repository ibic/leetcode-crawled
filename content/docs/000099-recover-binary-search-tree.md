---
title: "Recover Binary Search Tree"
weight: 99
#id: "recover-binary-search-tree"
---
## Description
<div class="description">
<p>Two elements of a binary search tree (BST) are swapped by mistake.</p>

<p>Recover the tree without changing its structure.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> [1,3,null,null,2]

&nbsp;  1
&nbsp; /
&nbsp;3
&nbsp; \
&nbsp;  2

<strong>Output:</strong> [3,1,null,null,2]

&nbsp;  3
&nbsp; /
&nbsp;1
&nbsp; \
&nbsp;  2
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> [3,1,4,null,null,2]

  3
 / \
1   4
&nbsp;  /
&nbsp; 2

<strong>Output:</strong> [2,1,4,null,null,3]

  2
 / \
1   4
&nbsp;  /
 &nbsp;3
</pre>

<p><strong>Follow up:</strong></p>

<ul>
	<li>A solution using O(<em>n</em>) space is pretty straight forward.</li>
	<li>Could you devise a constant space solution?</li>
</ul>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 5 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Goldman Sachs - 3 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Sort an Almost Sorted Array Where Two Elements Are Swapped

**Intuition**

Let's start from straightforward but not optimal solution 
with a linear time and space complexity. 
This solution serves to identify and discuss all subproblems.
  
It's known that [inorder traversal of BST is an array sorted in
the ascending order](https://leetcode.com/articles/delete-node-in-a-bst/).
Here is how one could compute an inorder traversal

<iframe src="https://leetcode.com/playground/iTZxdEcz/shared" frameBorder="0" width="100%" height="157" name="iTZxdEcz"></iframe>

Here two nodes are swapped, and hence inorder traversal is 
an almost sorted array where only two elements are swapped.
To identify two swapped elements in a sorted array is 
a classical problem that could be solved in linear time.
Here is a solution code

<iframe src="https://leetcode.com/playground/4U4tHxF7/shared" frameBorder="0" width="100%" height="293" name="4U4tHxF7"></iframe>

When swapped nodes are known, one could traverse the tree 
again and swap their values.

<img src="../Figures/99/iinorde.png" width="700">

**Algorithm**

Here is the algorithm:

1. Construct inorder traversal of the tree. 
It should be an almost sorted list where only two elements are swapped.

2. Identify two swapped elements x and y in an almost sorted array 
in linear time.

3. Traverse the tree again. Change value x to y and value y to x.

**Implementation**

<iframe src="https://leetcode.com/playground/rGFSV3Vz/shared" frameBorder="0" width="100%" height="500" name="rGFSV3Vz"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$. To compute inorder traversal takes
$$\mathcal{O}(N)$$ time, to identify and to swap back swapped nodes : $$\mathcal{O}(1)$$
in the best case and $$\mathcal{O}(N)$$ in the worst.

* Space complexity : $$\mathcal{O}(N)$$ since we keep inorder traversal 
`nums` with N elements.
<br />
<br />


---
#### What Is Coming Next

In approach 1 we discussed three easy subproblems of this hard problem:

1. Construct inorder traversal.

2. Find swapped elements in 
an almost sorted array where only two elements are swapped.

3. Swap values of two nodes.

Now we will discuss three more approaches, and basically they are 
all the same :

- Merge steps 1 and 2, i.e. identify swapped nodes during the
inorder traversal.

- Swap node values.

The difference in-between the following approaches is in a chosen 
method to implement inorder traversal :

- Approach 2 : Iterative.

- Approach 3 : Recursive.

- Approach 4 : Morris.

<img src="../Figures/99/mmorris.png" width="700">

Iterative and recursive approaches here do less than _one pass_,
and they both need up to $$\mathcal{O}(H)$$ space to keep stack,
where H is a tree height.

Morris approach is _two pass_ approach, but it's a constant-space one.
<br />
<br />


---
#### Approach 2: Iterative Inorder Traversal

**Intuition**

Here we construct inorder traversal by iterations 
and identify swapped nodes at the same time, in one pass.

> Iterative inorder traversal is simple:
go left as far as you can, then one step right. Repeat till 
the end of nodes in the tree.  

To identify swapped nodes, 
track the last node `pred` in the inorder traversal (i.e. the
_predecessor_ of the current node)
and compare it with current node value.
If the current node value is smaller than its predecessor `pred` value,
the swapped node is here. 

There are only two swapped nodes here, and hence one could break after 
having the second node identified.

Doing so, one could get directly nodes (and not only their values),
and hence swap node values in $$\mathcal{O}(1)$$ time, drastically
reducing the time needed for step 3.

!?!../Documents/99_LIS.json:1000,310!?!

**Implementation**

[Don't use Stack in Java, use ArrayDeque instead](https://docs.oracle.com/javase/8/docs/api/java/util/Stack.html).

<iframe src="https://leetcode.com/playground/HkVBKysQ/shared" frameBorder="0" width="100%" height="500" name="HkVBKysQ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$ in the best case, and 
$$\mathcal{O}(N)$$ in the worst case
when one of the swapped nodes is a rightmost leaf.
* Space complexity : up to $$\mathcal{O}(H)$$ to keep the stack
where H is a tree height.
<br />
<br />


---
#### Approach 3: Recursive Inorder Traversal

Iterative approach 2 could be converted into recursive one.

Recursive inorder traversal is extremely simple: 
follow `Left->Node->Right` direction, i.e. do the recursive call
for the _left_ child, then do all the business with the node
(= if the node is the swapped one or not), and
then do the recursive call for the _right_ child.

On the following figure the nodes are numerated in the order you visit them, 
please follow `1-2-3-4-5` to compare different DFS strategies.

<img src="../Figures/99/ddfs.png" width="700">

**Implementation**

<iframe src="https://leetcode.com/playground/pJ6vtUxU/shared" frameBorder="0" width="100%" height="497" name="pJ6vtUxU"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$ in the best case, and 
$$\mathcal{O}(N)$$ in the worst case
when one of the swapped nodes is a rightmost leaf.
* Space complexity : up to $$\mathcal{O}(H)$$ to keep the recursion stack,
where H is a tree height.
<br />
<br />


---
#### Approach 4: Morris Inorder Traversal

We discussed already iterative and recursive inorder traversals,
which both have great time complexity though use up to
$$\mathcal{O}(H)$$ to keep stack. 
We could trade in performance to save space. 

The idea of Morris inorder traversal is simple:
to use no space but to traverse the tree.

> How that could be even possible? At each node one has to decide where to go:
left or right, traverse left subtree or traverse right subtree. 
How one could know that the left subtree is already done if no 
additional memory is allowed?  

The idea of [Morris](https://www.sciencedirect.com/science/article/pii/0020019079900681)
algorithm is to set the _temporary link_ between the node and its 
[predecessor](https://leetcode.com/articles/delete-node-in-a-bst/):
`predecessor.right = root`.
So one starts from the node, computes its predecessor and
verifies if the link is present.

- There is no link? Set it and go to the left subtree.

- There is a link? Break it and go to the right subtree.  

There is one small issue to deal with : what if there is no
left child, i.e. there is no left subtree? 
Then go straightforward to the right subtree.  

!?!../Documents/99_SEC.json:1000,377!?!

**Implementation**

<iframe src="https://leetcode.com/playground/hpy96J6w/shared" frameBorder="0" width="100%" height="500" name="hpy96J6w"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since we visit each node up to
two times.
 
* Space complexity : $$\mathcal{O}(1)$$.

## Accepted Submission (python3)
```python3
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def recoverTree(self, root: TreeNode) -> None:
        """
        Do not return anything, modify root in-place instead.
        """
        def visit(node):
            nonlocal pair
            nonlocal prev
            if node.val < prev.val:
                if pair:
                    pair[1] = node
                else:
                    pair = [prev, node]
            prev = node

        def inorder(node):
            if not node:
                return
            inorder(node.left)
            visit(node)
            inorder(node.right)

        pair = []
        prev = TreeNode(float('-inf'))
        inorder(root)
        a = pair[0]
        b = pair[1]
        tmp = a.val
        a.val = b.val
        b.val = tmp
```

## Top Discussions
### No Fancy Algorithm, just Simple and Powerful In-Order Traversal
- Author: qwl5004
- Creation Date: Sun Oct 12 2014 09:15:54 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 12:41:50 GMT+0800 (Singapore Standard Time)

<p>
This question appeared difficult to me but it is really just a simple in-order traversal! I got really frustrated when other people are showing off Morris Traversal which is totally not necessary here. 

Let's start by writing the in order traversal:

    private void traverse (TreeNode root) {
       if (root == null)
          return;
       traverse(root.left);
       // Do some business
       traverse(root.right);
    }

So when we need to print the node values in order, we insert System.out.println(root.val) in the place of "Do some business".

What is the business we are doing here?
We need to find the first and second elements that are not in order right?

How do we find these two elements? For example, we have the following tree that is printed as in order traversal:

6, 3, 4, 5, 2

We compare each node with its next one and we can find out that 6 is the first element to swap because 6 > 3 and 2 is the second element to swap because 2 < 5.

Really, what we are comparing is the current node and its previous node in the "in order traversal". 

Let us define three variables, firstElement, secondElement, and prevElement. Now we just need to build the "do some business" logic as finding the two elements. See the code below:

    public class Solution {
        
        TreeNode firstElement = null;
        TreeNode secondElement = null;
        // The reason for this initialization is to avoid null pointer exception in the first comparison when prevElement has not been initialized
        TreeNode prevElement = new TreeNode(Integer.MIN_VALUE);
        
        public void recoverTree(TreeNode root) {
            
            // In order traversal to find the two elements
            traverse(root);
            
            // Swap the values of the two nodes
            int temp = firstElement.val;
            firstElement.val = secondElement.val;
            secondElement.val = temp;
        }
        
        private void traverse(TreeNode root) {
            
            if (root == null)
                return;
                
            traverse(root.left);
            
            // Start of "do some business", 
            // If first element has not been found, assign it to prevElement (refer to 6 in the example above)
            if (firstElement == null && prevElement.val >= root.val) {
                firstElement = prevElement;
            }
        
            // If first element is found, assign the second element to the root (refer to 2 in the example above)
            if (firstElement != null && prevElement.val >= root.val) {
                secondElement = root;
            }        
            prevElement = root;

            // End of "do some business"

            traverse(root.right);
    }

And we are done, it is just that easy!
</p>


### Detail Explain about How Morris Traversal Finds two Incorrect Pointer
- Author: siyang3
- Creation Date: Thu Feb 26 2015 06:29:57 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 09:21:20 GMT+0800 (Singapore Standard Time)

<p>
To understand this, you need to first understand Morris Traversal or Morris Threading Traversal.
It take use of leaf nodes' right/left pointer to achieve O(1) space Traversal on a Binary Tree.
Below is a standard Inorder Morris Traversal, referred from http://www.cnblogs.com/AnnieKim/archive/2013/06/15/morristraversal.html (a Chinese Blog, while the graphs are great for illustration)

    public void morrisTraversal(TreeNode root){
    		TreeNode temp = null;
    		while(root!=null){
    			if(root.left!=null){
    				// connect threading for root
    				temp = root.left;
    				while(temp.right!=null && temp.right != root)
    					temp = temp.right;
    				// the threading already exists
    				if(temp.right!=null){
    					temp.right = null;
    					System.out.println(root.val);
    					root = root.right;
    				}else{
    					// construct the threading
    					temp.right = root;
    					root = root.left;
    				}
    			}else{
    				System.out.println(root.val);
    				root = root.right;
    			}
    		}
    	}

In the above code, `System.out.println(root.val);`appear twice, which functions as outputing the Node in ascending order (BST). Since these places are in order, replace them with

        if(pre!=null && pre.val > root.val){
        	if(first==null){first = pre;second = root;}
        	else{second = root;}
      }
    pre = root;

each time, the pre node and root are in order as `System.out.println(root.val);` outputs them in order.

Then, come to how to specify the first wrong node and second wrong node.

When they are not consecutive, the first time we meet `pre.val > root.val` ensure us the first node is the pre node, since root should be traversal ahead of pre, pre should be at least at small as root. The second time we meet `pre.val > root.val` ensure us the second node is the root node, since we are now looking for a node to replace with out first node, which is found before.

When they are consecutive, which means the case `pre.val > cur.val` will appear only once. We need to take case this case without destroy the previous analysis. So the first node will still be pre, and the second will be just set to root. Once we meet this case again, the first node will not be affected.

Below is the updated version on Morris Traversal.


    public void recoverTree(TreeNode root) {
            TreeNode pre = null;
            TreeNode first = null, second = null;
            // Morris Traversal
            TreeNode temp = null;
    		while(root!=null){
    			if(root.left!=null){
    				// connect threading for root
    				temp = root.left;
    				while(temp.right!=null && temp.right != root)
    					temp = temp.right;
    				// the threading already exists
    				if(temp.right!=null){
    				    if(pre!=null && pre.val > root.val){
    				        if(first==null){first = pre;second = root;}
    				        else{second = root;}
    				    }
    				    pre = root;
    				    
    					temp.right = null;
    					root = root.right;
    				}else{
    					// construct the threading
    					temp.right = root;
    					root = root.left;
    				}
    			}else{
    				if(pre!=null && pre.val > root.val){
    				    if(first==null){first = pre;second = root;}
    				    else{second = root;}
    				}
    				pre = root;
    				root = root.right;
    			}
    		}
    		// swap two node values;
    		if(first!= null && second != null){
    		    int t = first.val;
    		    first.val = second.val;
    		    second.val = t;
    		}
        }
</p>


### Tree Deserializer and Visualizer for Python
- Author: StefanPochmann
- Creation Date: Sat Jun 20 2015 01:50:05 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 21:03:13 GMT+0800 (Singapore Standard Time)

<p>
Wrote some tools for my own local testing. For example `deserialize('[1,2,3,null,null,4,null,null,5]')` will turn that into a tree and return the root [as explained in the FAQ](https://leetcode.com/faq/). I also wrote a visualizer. Two examples:

`drawtree(deserialize('[1,2,3,null,null,4,null,null,5]'))`:

![enter image description here][1]

`drawtree(deserialize('[2,1,3,0,7,9,1,2,null,1,0,null,null,8,8,null,null,null,null,7]'))`:

![enter image description here][2]

Here's the code. If you save it as a Python script and run it, it should as a demo show the above two pictures in turtle windows (one after the other). And you can of course import it from other scripts and then it will only provide the class/functions and not show the demo.

    class TreeNode:
        def __init__(self, val, left=None, right=None):
            self.val = val
            self.left = left
            self.right = right
        def __repr__(self):
            return 'TreeNode({})'.format(self.val)
        
    def deserialize(string):
        if string == '{}':
            return None
        nodes = [None if val == 'null' else TreeNode(int(val))
                 for val in string.strip('[]{}').split(',')]
        kids = nodes[::-1]
        root = kids.pop()
        for node in nodes:
            if node:
                if kids: node.left  = kids.pop()
                if kids: node.right = kids.pop()
        return root
    
    def drawtree(root):
        def height(root):
            return 1 + max(height(root.left), height(root.right)) if root else -1
        def jumpto(x, y):
            t.penup()
            t.goto(x, y)
            t.pendown()
        def draw(node, x, y, dx):
            if node:
                t.goto(x, y)
                jumpto(x, y-20)
                t.write(node.val, align='center', font=('Arial', 12, 'normal'))
                draw(node.left, x-dx, y-60, dx/2)
                jumpto(x, y-20)
                draw(node.right, x+dx, y-60, dx/2)
        import turtle
        t = turtle.Turtle()
        t.speed(0); turtle.delay(0)
        h = height(root)
        jumpto(0, 30*h)
        draw(root, 0, 30*h, 40*h)
        t.hideturtle()
        turtle.mainloop()
        
    if __name__ == '__main__':
        drawtree(deserialize('[1,2,3,null,null,4,null,null,5]'))
        drawtree(deserialize('[2,1,3,0,7,9,1,2,null,1,0,null,null,8,8,null,null,null,null,7]'))

  [1]: http://pochmann.org/leetcode/images/tree1.png
  [2]: http://pochmann.org/leetcode/images/tree2.png
</p>


