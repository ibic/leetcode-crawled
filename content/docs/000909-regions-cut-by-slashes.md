---
title: "Regions Cut By Slashes"
weight: 909
#id: "regions-cut-by-slashes"
---
## Description
<div class="description">
<p>In a N x N&nbsp;<code>grid</code> composed of 1 x 1 squares, each 1 x 1 square consists of a <code>/</code>, <code>\</code>, or blank space.&nbsp; These characters divide the square into contiguous regions.</p>

<p>(Note that backslash characters are escaped, so a <code>\</code>&nbsp;is represented as <code>&quot;\\&quot;</code>.)</p>

<p>Return the number of regions.</p>

<p>&nbsp;</p>

<div>
<div>
<div>
<div>
<div>
<ol>
</ol>
</div>
</div>
</div>
</div>
</div>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:
</strong><span id="example-input-1-1">[
&nbsp; &quot; /&quot;,
&nbsp; &quot;/ &quot;
]</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>The 2x2 grid is as follows:
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/15/1.png" style="width: 82px; height: 82px;" />
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:
</strong><span id="example-input-2-1">[
&nbsp; &quot; /&quot;,
&nbsp; &quot;  &quot;
]</span>
<strong>Output: </strong><span id="example-output-2">1</span>
<strong>Explanation: </strong>The 2x2 grid is as follows:
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/15/2.png" style="width: 82px; height: 82px;" />
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:
</strong><span id="example-input-3-1">[
&nbsp; &quot;\\/&quot;,
&nbsp; &quot;/\\&quot;
]</span>
<strong>Output: </strong><span id="example-output-3">4</span>
<strong>Explanation: </strong>(Recall that because \ characters are escaped, &quot;\\/&quot; refers to \/, and &quot;/\\&quot; refers to /\.)
The 2x2 grid is as follows:
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/15/3.png" style="width: 82px; height: 82px;" />
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:
</strong><span id="example-input-4-1">[
&nbsp; &quot;/\\&quot;,
&nbsp; &quot;\\/&quot;
]</span>
<strong>Output: </strong><span id="example-output-4">5</span>
<strong>Explanation: </strong>(Recall that because \ characters are escaped, &quot;/\\&quot; refers to /\, and &quot;\\/&quot; refers to \/.)
The 2x2 grid is as follows:
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/15/4.png" style="width: 82px; height: 82px;" />
</pre>

<div>
<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:
</strong><span id="example-input-5-1">[
&nbsp; &quot;//&quot;,
&nbsp; &quot;/ &quot;
]</span>
<strong>Output: </strong><span id="example-output-5">3</span>
<strong>Explanation: </strong>The 2x2 grid is as follows:
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/15/5.png" style="width: 82px; height: 82px;" />
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= grid.length == grid[0].length &lt;= 30</code></li>
	<li><code>grid[i][j]</code> is either <code>&#39;/&#39;</code>, <code>&#39;\&#39;</code>, or <code>&#39; &#39;</code>.</li>
</ol>
</div>
</div>
</div>
</div>
</div>
</div>

## Tags
- Depth-first Search (depth-first-search)
- Union Find (union-find)
- Graph (graph)

## Companies
- Uber - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Union-Find

**Intuition**

To find the number of components in a graph, we can use either depth-first search or union find.  The main difficulty with this problem is in specifying the graph.

One "brute force" way to specify the graph is to associate each grid square with 4 nodes (north, south, west, and east), representing 4 triangles inside the square if it were to have both slashes.  Then, we can connect all 4 nodes if the grid square is `" "`, and connect two pairs if the grid square is `"/"` or `"\"`.  Finally, we can connect all neighboring nodes (for example, the east node of the square at `grid[0][0]` connects with the west node of the square at `grid[0][1]`).

This is the most straightforward approach, but there are other approaches that use less nodes to represent the underlying information.

**Algorithm**

Create `4*N*N` nodes, one for each grid square, and connect them as described above.  After, we use a union find structure to find the number of connected components.

We will skip the explanation of how a DSU structure is implemented.  Please refer to [https://leetcode.com/problems/redundant-connection/solution/](https://leetcode.com/problems/redundant-connection/solution/) for a tutorial on DSU.

<iframe src="https://leetcode.com/playground/MCjyfDe6/shared" frameBorder="0" width="100%" height="500" name="MCjyfDe6"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N * N * \alpha(N))$$, where $$N$$ is the length of the grid, and $$\alpha$$ is the Inverse-Ackermann function (if we were to use union-find by rank.)

* Space Complexity:  $$O(N * N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ with picture, DFS on upscaled grid
- Author: votrubac
- Creation Date: Sun Dec 16 2018 12:02:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 16 2018 12:02:40 GMT+0800 (Singapore Standard Time)

<p>
We can upscale the input grid to [n * 3][n * 3] grid and draw "lines" there. Then, we can paint empty regions using DFS and count them. Picture below says it all. Note that [n * 2][n * 2]  grid does not work as "lines" are too thick to identify empty areas correctly.
![image](https://assets.leetcode.com/users/votrubac/image_1544935075.png)
```
void dfs(vector<vector<int>>& g, int i, int j) {
  if (i >= 0 && j >= 0 && i < g.size() && j < g.size() && g[i][j] == 0) {
    g[i][j] = 1;
    dfs(g, i - 1, j), dfs(g, i + 1, j), dfs(g, i, j - 1), dfs(g, i, j + 1);
  }
}
int regionsBySlashes(vector<string>& grid, int regions = 0) {
  vector<vector<int>> g(grid.size() * 3, vector<int>(grid.size() * 3, 0));
  for (auto i = 0; i < grid.size(); ++i)
    for (auto j = 0; j < grid.size(); ++j) {
      if (grid[i][j] == \'/\') g[i * 3][j * 3 + 2] = g[i * 3 + 1][j * 3 + 1] = g[i * 3 + 2][j * 3] = 1;
      if (grid[i][j] == \'\\\') g[i * 3][j * 3] = g[i * 3 + 1][j * 3 + 1] = g[i * 3 + 2][j * 3 + 2] = 1;
    }
  for (auto i = 0; i < g.size(); ++i)
    for (auto j = 0; j < g.size(); ++j) if (g[i][j] == 0) dfs(g, i, j), ++regions;
  return regions;
}
```
</p>


### [Java/C++/Python] Split 4 parts and Union Find
- Author: lee215
- Creation Date: Sun Dec 16 2018 12:06:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 17 2020 14:10:34 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Split a cell in to 4 parts like this.
We give it a number top is 0, right is 1, bottom is 2 left is 3.

![image](https://assets.leetcode.com/users/images/c1f05c28-9c76-4211-b25a-4fe8df539938_1600322994.7242403.png)
(photo by @Sabbi_coder)

Two adjacent parts in different cells are contiguous regions.
In case `\'/\'`, top and left are contiguous, botton and right are contiguous.
In case `\'\\\'`, top and right are contiguous, bottom and left are contiguous.
In case `\' \'`, all 4 parts are contiguous.

Congratulation.
Now you have another problem of counting the number of islands.
<br>

# Explanation
DFS will be good enough to solve it.
As I did in [947.Most Stones Removed with Same Row or Column](https://leetcode.com/problems/most-stones-removed-with-same-row-or-column/discuss/197668)
I solved it with union find.
<br>

# **Complexity**:
Time `O(N^2)`
Space `O(N^2)`
<br>

**Java:**
```java
    int count, n;
    int[] f;
    public int regionsBySlashes(String[] grid) {
        n = grid.length;
        f = new int[n * n * 4];
        count = n * n * 4;
        for (int i = 0; i < n * n * 4; ++i)
            f[i] = i;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (i > 0) union(g(i - 1, j, 2), g(i, j, 0));
                if (j > 0) union(g(i , j - 1, 1), g(i , j, 3));
                if (grid[i].charAt(j) != \'/\') {
                    union(g(i , j, 0), g(i , j,  1));
                    union(g(i , j, 2), g(i , j,  3));
                }
                if (grid[i].charAt(j) != \'\\\') {
                    union(g(i , j, 0), g(i , j,  3));
                    union(g(i , j, 2), g(i , j,  1));
                }
            }
        }
        return count;
    }

    public int find(int x) {
        if (x != f[x]) {
            f[x] = find(f[x]);
        }
        return f[x];
    }
    public void union(int x, int y) {
        x = find(x); y = find(y);
        if (x != y) {
            f[x] = y;
            count--;
        }
    }
    public int g(int i, int j, int k) {
        return (i * n + j) * 4 + k;
    }
```

**C++:**
```cpp
   int count, n;
   vector<int> f;
   int regionsBySlashes(vector<string>& grid) {
        n = grid.size();
        count = n * n * 4;
        for (int i = 0; i < n * n * 4; ++i)
            f.push_back(i);
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (i > 0) uni(g(i - 1, j, 2), g(i, j, 0));
                if (j > 0) uni(g(i , j - 1, 1), g(i , j, 3));
                if (grid[i][j] != \'/\') {
                    uni(g(i , j, 0), g(i , j,  1));
                    uni(g(i , j, 2), g(i , j,  3));
                }
                if (grid[i][j] != \'\\\') {
                    uni(g(i , j, 0), g(i , j,  3));
                    uni(g(i , j, 2), g(i , j,  1));
                }
            }
        }
        return count;
    }

    int find(int x) {
        if (x != f[x]) {
            f[x] = find(f[x]);
        }
        return f[x];
    }
    void uni(int x, int y) {
        x = find(x); y = find(y);
        if (x != y) {
            f[x] = y;
            count--;
        }
    }
    int g(int i, int j, int k) {
        return (i * n + j) * 4 + k;
    }
```

**Python:**
```py
    def regionsBySlashes(self, grid):
        f = {}
        def find(x):
            f.setdefault(x, x)
            if x != f[x]:
                f[x] = find(f[x])
            return f[x]
        def union(x, y):
            f[find(x)] = find(y)

        for i in xrange(len(grid)):
            for j in xrange(len(grid)):
                if i:
                    union((i - 1, j, 2), (i, j, 0))
                if j:
                    union((i, j - 1, 1), (i, j, 3))
                if grid[i][j] != "/":
                    union((i, j, 0), (i, j, 1))
                    union((i, j, 2), (i, j, 3))
                if grid[i][j] != "\\":
                    union((i, j, 3), (i, j, 0))
                    union((i, j, 1), (i, j, 2))
        return len(set(map(find, f)))
```

</p>


### Mark the boundary and then the problem become Number of Islands (dfs or bfs)
- Author: yz5548
- Creation Date: Sun Dec 16 2018 12:28:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 16 2018 12:28:07 GMT+0800 (Singapore Standard Time)

<p>
First mark the boundary by painting [n * 3][m * 3] grid, then use the algorithm count number of island ([leetcode200](https://leetcode.com/problems/number-of-islands/)) using either bfs or dfs
Using a 3X3 array to represent \'/\' or \'\\\'
0 0 1            
0 1 0               
1 0 0        

1 0 0
0 1 0
0 0 1
Why need to use 3x3 array to represent \'/\' or \'\\\'? For example ["//","/ "]
use 1d: can\'t not distinct individual component
1 1
1 0
use 2d: some connected area was isolated 
0 1 0 1
1 0 1 0
0 1 0 0
1 0 0 0

use 3d:
0 0 1 0 0 1
0 1 0 0 1 0
1 0 0 1 0 0
0 0 1 0 0 0
0 1 0 0 0 0
1 0 0 0 0 0
use 4d or more: it works, but not necessary
```
class Solution {
    public int regionsBySlashes(String[] grid) {
        int n = grid.length, m = grid[0].length();
        int cnt = 0;
        int[][] g = new int[n*3][m*3];
        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                if(grid[i].charAt(j)==\'/\'){
                    g[i * 3][j * 3 + 2] = 1;
                    g[i * 3 + 1][j * 3 + 1] = 1;
                    g[i * 3 + 2][j * 3] = 1;
                }else if(grid[i].charAt(j)==\'\\\'){
                    g[i * 3][j * 3] = 1;
                    g[i * 3 + 1][j * 3 + 1] = 1;
                    g[i * 3 + 2][j * 3 + 2] = 1;
                }
            }
        }
        for(int i=0;i<g.length;i++){
            for(int j=0;j<g[0].length;j++){
                if(g[i][j]==0){
                    dfs(g,i,j);
                    cnt++;
                }
            }
        }
        return cnt;
    }
    
    void dfs(int[][] g, int i, int j){
        int n = g.length, m = g[0].length;
        if(i<0 || i>=n || j<0 || j>=m || g[i][j]==1) return;
        g[i][j]=1;
        int d[] = {0,-1,0,1,0};
        for(int k=0;k<4;k++){
            dfs(g,i+d[k],j+d[k+1]);
        }
    }
}
```
</p>


