---
title: "Number of Burgers with No Waste of Ingredients"
weight: 1210
#id: "number-of-burgers-with-no-waste-of-ingredients"
---
## Description
<div class="description">
<p>Given two integers <code>tomatoSlices</code>&nbsp;and <code>cheeseSlices</code>. The ingredients of different burgers are as follows:</p>

<ul>
	<li><strong>Jumbo Burger:</strong> 4 tomato slices&nbsp;and 1 cheese slice.</li>
	<li><strong>Small Burger:</strong> 2 Tomato slices&nbsp;and 1 cheese slice.</li>
</ul>

<p>Return <code>[total_jumbo, total_small]</code> so that the number of remaining <code>tomatoSlices</code>&nbsp;equal to 0 and the number of remaining <code>cheeseSlices</code> equal to 0. If it is not possible to make the remaining <code>tomatoSlices</code>&nbsp;and <code>cheeseSlices</code> equal to 0 return <code>[]</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> tomatoSlices = 16, cheeseSlices = 7
<strong>Output:</strong> [1,6]
<strong>Explantion:</strong> To make one jumbo burger and 6 small burgers we need 4*1 + 2*6 = 16 tomato and 1 + 6 = 7 cheese. There will be no remaining ingredients.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> tomatoSlices = 17, cheeseSlices = 4
<strong>Output:</strong> []
<strong>Explantion:</strong> There will be no way to use all ingredients to make small and jumbo burgers.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> tomatoSlices = 4, cheeseSlices = 17
<strong>Output:</strong> []
<strong>Explantion:</strong> Making 1 jumbo burger there will be 16 cheese remaining and making 2 small burgers there will be 15 cheese remaining.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> tomatoSlices = 0, cheeseSlices = 0
<strong>Output:</strong> [0,0]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> tomatoSlices = 2, cheeseSlices = 1
<strong>Output:</strong> [0,1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= tomatoSlices &lt;= 10^7</code></li>
	<li><code>0 &lt;= cheeseSlices &lt;= 10^7</code></li>
</ul>
</div>

## Tags
- Math (math)
- Greedy (greedy)

## Companies
- DeliveryHero - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python]  Chickens and Rabbits
- Author: lee215
- Creation Date: Sun Dec 01 2019 12:03:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 01 2019 15:38:36 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Classic problem, https://w.wiki/D2S
Sorry that I don\'t know the name in English.
Maybe  Chickens and Rabbits problem
<br>

# **Explanation**
tomate number `t` should not be odd,
and it should valid that `c * 2 <= t && t <= c * 4`.

From
`jumbo + small = cheese`
`jumbo * 2 + small = tomate / 2`

We can get that
`jumb0 = tomate / 2 - cheese`
So that
`small = cheese * 2 - tomate / 2`
<br>

# **Complexity**
Time `O(1)`
Space `O(1)`
<br>

**Java:**
```java
    public List<Integer> numOfBurgers(int t, int c) {
        return t % 2 == 0 && c * 2 <= t && t <= c * 4 ? Arrays.asList(t / 2 - c, c * 2 - t / 2) :  new ArrayList();
    }
```

**C++:**
```cpp
    vector<int> numOfBurgers(int t, int c) {
        if (t % 2 == 0 && c * 2 <= t && t <= c * 4)
            return {t / 2 - c, c * 2 - t / 2};
        return {};
    }
```

**Python:**
```python
    def numOfBurgers(self, t, c):
        return [t / 2 - c, c * 2 - t / 2] if t % 2 == 0 and c * 2 <= t <= c * 4 else []
```

</p>


### [Java/Python 3] Solve a linear equations group w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Dec 01 2019 12:06:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 08 2019 14:52:39 GMT+0800 (Singapore Standard Time)

<p>
According the description of the problem, we have the following:
```
Jumbo Burger: 4 tomato slices and 1 cheese slice.
Small Burger: 2 Tomato slices and 1 cheese slice.
```
Therefore:
```
4 * Jumbo Burger + 2 * Small Burger = tomatoSlices
Jumbo Burger + Small Burger = cheeseSlices
```
Let `x` and `y` indicate `jumbo` and `small` Burger, respectively, then
```
4x + 2y = tomatoSlices
x + y = cheeseSlices
```
=>
```
2x = tomatoSlices - 2 * cheeseSlices
```
=>
```
x = (tomatoSlices - 2 * cheeseSlices) / 2
y = cheeseSlices - x
```

----

```java
    public List<Integer> numOfBurgers(int tomatoSlices, int cheeseSlices) {
        int twoX = tomatoSlices - 2 * cheeseSlices, x = twoX / 2, y = cheeseSlices - x;
        return twoX >= 0 && twoX % 2 == 0 && y >= 0 ? Arrays.asList(x, y) : Arrays.asList();        
    }
```
```python
    def numOfBurgers(self, tomatoSlices: int, cheeseSlices: int) -> List[int]:
        two_x = tomatoSlices - 2 * cheeseSlices
        x = two_x // 2
        y = cheeseSlices - x
        return [x, y] if two_x >= 0 and not two_x % 2 and y >= 0 else []
```
**Analysis**

Time & space: O(1).
</p>


### The only realistic solution
- Author: StefanPochmann
- Creation Date: Sun Dec 01 2019 13:36:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 01 2019 13:38:31 GMT+0800 (Singapore Standard Time)

<p>
I believe this is how you\'d solve it after eating a lot of burgers.
```
    vector<int> numOfBurgers(int tomatoSlices, int cheeseSlices) {
        int burgers = cheeseSlices;
        for (int jumbo = 0; jumbo <= burgers; jumbo++) {
            int small = burgers - jumbo;
            if (tomatoSlices == 4*jumbo + 2*small)
                return {jumbo, small};
        }
        return {};
    }
```
</p>


