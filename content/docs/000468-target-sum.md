---
title: "Target Sum"
weight: 468
#id: "target-sum"
---
## Description
<div class="description">
<p>You are given a list of non-negative integers, a1, a2, ..., an, and a target, S. Now you have 2 symbols <code>+</code> and <code>-</code>. For each integer, you should choose one from <code>+</code> and <code>-</code> as its new symbol.</p>

<p>Find out how many ways to assign symbols to make sum of integers equal to target S.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> nums is [1, 1, 1, 1, 1], S is 3. 
<b>Output:</b> 5
<b>Explanation:</b> 

-1+1+1+1+1 = 3
+1-1+1+1+1 = 3
+1+1-1+1+1 = 3
+1+1+1-1+1 = 3
+1+1+1+1-1 = 3

There are 5 ways to assign symbols to make the sum of nums be target 3.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The length of the given array is positive and will not exceed 20.</li>
	<li>The sum of elements in the given array will not exceed 1000.</li>
	<li>Your output answer is guaranteed to be fitted in a 32-bit integer.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Depth-first Search (depth-first-search)

## Companies
- Facebook - 9 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force

**Algorithm**

The brute force approach is based on recursion. We need to try to put both the `+` and `-` symbols at every location in the given $$nums$$ array and find out the assignments which lead to the required result $$S$$.

For this, we make use of a recursive function `calculate(nums, i, sum, S)`, which returns the assignments leading to the sum $$S$$, starting from the $$i^{th}$$ index onwards, provided the sum of elements upto the $$i^{th}$$ element is $$sum$$. This function appends a `+` sign and a `-` sign both to the element at the current index and calls itself with the updated $$sum$$ as $$sum + nums[i]$$ and $$sum - nums[i]$$ repectively along with the updated current index as $$i+1$$.  Whenver, we reach the end of the array, we compare the sum obtained with $$S$$. If they are equal, we increment the $$count$$ value to be returned.

Thus, the function call `calculate(nums, 0, 0, S)` retuns the required no. of assignments.

<iframe src="https://leetcode.com/playground/XkCd4VLD/shared" frameBorder="0" width="100%" height="327" name="XkCd4VLD"></iframe>

**Complexity Analysis**

* Time complexity : $$O(2^n)$$. Size of recursion tree will be $$2^n$$. $$n$$ refers to the size of $$nums$$ array.

* Space complexity : $$O(n)$$. The depth of the recursion tree can go upto $$n$$.
<br />
<br />


---
#### Approach 2: Recursion with Memoization

**Algorithm**

It can be easily observed that in the last approach, a lot of redundant function calls could be made with the same value of $$i$$ as the current index and the same value of $$sum$$ as the current sum, since the same values could be obtained through multiple paths in the recursion tree. In order to remove this redundancy, we make use of memoization as well to store the results which have been calculated earlier.

Thus, for every call to `calculate(nums, i, sum, S)`, we store the result obtained in $$memo[i][sum + 1000]$$. The factor of 1000 has been added as an offset to the $$sum$$ value to map all the $$sum$$s possible to positive integer range. By making use of memoization, we can prune the search space to a good extent.

<iframe src="https://leetcode.com/playground/E8qGFmM8/shared" frameBorder="0" width="100%" height="480" name="E8qGFmM8"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(l \cdot n)$$. The `memo` array of size $$l*n$$ has been filled just once. Here, $$l$$ refers to the range of $$sum$$ and $$n$$ refers to the size of $$nums$$ array.

* Space complexity: $$\mathcal{O}(l \cdot n)$$. The depth of recursion tree can go upto $$n$$.
The `memo` array contains $$l \cdot n$$ elements. 
<br />
<br />


---
#### Approach 3: 2D Dynamic Programming

**Algorithm**

The idea behind this approach is as follows. Suppose we can find out the number of times a particular sum, say $$sum_i$$ is possible upto a particular index, say $$i$$, in the given $$nums$$ array, which is given by say $$count_i$$. Now, we can find out the number of times the sum $$sum_i + nums[i]$$ can occur easily as $$count_i$$. Similarly, the number of times the sum $$sum_i - nums[i]$$ occurs is also given by $$count_i$$. 

Thus, if we know all the sums $$sum_j$$'s which are possible upto the $$j^{th}$$ index by using various assignments, along with the corresponding count of assignments, $$count_j$$, leading to the same sum, we can determine all the sums possible upto the $$(j+1)^{th}$$ index  along with the corresponding count of assignments leading to the new sums.

Based on this idea, we make use of a $$dp$$ to determine the number of assignments which can lead to the given sum. $$dp[i][j]$$ refers to the number of assignments which can lead to a sum of $$j$$ upto the $$i^{th}$$ index. To determine the number of assignments which can lead to a sum of $$sum + nums[i]$$ upto the $$(i+1)^{th}$$ index, we can use $$dp[i][sum + nums[i]] = dp[i][sum + nums[i]] + dp[i-1][sum]$$. Similarly, $$dp[i][sum - nums[i]] = dp[i][sum + nums[i]] + dp[i-1][sum]$$. We iterate over the $$dp$$ array in a rowwise fashion i.e. Firstly we obtain all the sums which are possible upto a particular index along with the corresponding count of assignments and then proceed for the next element(index) in the $$nums$$ array.

But, since the $$sum$$ can range from -1000 to +1000, we need to add an offset of 1000 to the sum indices (column number) to map all the sums obtained to positive range only. 

At the end, the value of $$dp[n-1][S+1000]$$ gives us the required number of assignments. Here, $$n$$ refers to the number of elements in the $$nums$$ array.

The animation below shows the way various sums are generated along with the corresponding indices. The example assumes $$sum$$ values to lie in the range of -6 to +6 just for the purpose of illustration. This animation is inspired by [@Chidong](http://leetcode.com/Chidong)

!?!../Documents/494_Target_Sum.json:1000,563!?!


<iframe src="https://leetcode.com/playground/dXnMf2yv/shared" frameBorder="0" width="100%" height="327" name="dXnMf2yv"></iframe>

**Complexity Analysis**

* Time complexity : $$O(l*n)$$. The entire $$nums$$ array is travesed 2001(constant no.: $$l$$) times. $$n$$ refers to the size of $$nums$$ array. $$l$$ refers to the range of $$sum$$ possible.

* Space complexity : $$O(l*n)$$. $$dp$$ array of size $$l*n$$ is used.
<br />
<br />


---
#### Approach 4: 1D Dynamic Programming

**Algorithm**

If we look closely at the last solution, we can observe that for the evaluation of the current row of $$dp$$, only the values of the last row of $$dp$$ are needed. Thus, we can save some space by using a 1D DP array instead of a 2-D DP array. The only difference that needs to be made is that now the same $$dp$$ array will be updated for every row traversed. 

Below code is inspired by [@Chidong](http://leetcode.com/Chidong)

<iframe src="https://leetcode.com/playground/FEdnxLzM/shared" frameBorder="0" width="100%" height="361" name="FEdnxLzM"></iframe>

**Complexity Analysis**

* Time complexity : $$O(l.n)$$. The entire $$nums$$ array is traversed $$l$$ times. $$n$$ refers to the size of $$nums$$ array. $$l$$ refers to the range of $$sum$$ possible.

* Space complexity : $$O(n)$$. $$dp$$ array of size $$n$$ is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java (15 ms) C++ (3 ms) O(ns) iterative DP solution using subset sum with explanation
- Author: yuxiangmusic
- Creation Date: Sun Jan 22 2017 03:23:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 09:54:14 GMT+0800 (Singapore Standard Time)

<p>
The recursive solution is very slow, because its runtime is exponential

The original problem statement is equivalent to:
Find a **subset** of ```nums``` that need to be positive, and the rest of them negative, such that the sum is equal to ```target```

Let ```P``` be the positive subset and ```N``` be the negative subset
For example:
Given ```nums = [1, 2, 3, 4, 5]``` and ```target = 3``` then one possible solution is ```+1-2+3-4+5 = 3```
Here positive subset is ```P = [1, 3, 5]``` and negative subset is ```N = [2, 4]```

Then let's see how this can be converted to a subset sum problem:
```
                  sum(P) - sum(N) = target
sum(P) + sum(N) + sum(P) - sum(N) = target + sum(P) + sum(N)
                       2 * sum(P) = target + sum(nums)
``` 
So the original problem has been converted to a subset sum problem as follows:
Find a **subset** ```P``` of ```nums``` such that ```sum(P) = (target + sum(nums)) / 2```

Note that the above formula has proved that ```target + sum(nums)``` must be even
We can use that fact to quickly identify inputs that do not have a solution (Thanks to @BrunoDeNadaiSarnaglia for the suggestion)
For detailed explanation on how to solve subset sum problem, you may refer to [Partition Equal Subset Sum](https://leetcode.com/problems/partition-equal-subset-sum/)

Here is Java solution (15 ms)
```
    public int findTargetSumWays(int[] nums, int s) {
        int sum = 0;
        for (int n : nums)
            sum += n;
        return sum < s || (s + sum) % 2 > 0 ? 0 : subsetSum(nums, (s + sum) >>> 1); 
    }   

    public int subsetSum(int[] nums, int s) {
        int[] dp = new int[s + 1]; 
        dp[0] = 1;
        for (int n : nums)
            for (int i = s; i >= n; i--)
                dp[i] += dp[i - n]; 
        return dp[s];
    } 
```

Here is C++ solution (3 ms)
```
class Solution {
public:
    int findTargetSumWays(vector<int>& nums, int s) {
        int sum = accumulate(nums.begin(), nums.end(), 0);
        return sum < s || (s + sum) & 1 ? 0 : subsetSum(nums, (s + sum) >> 1); 
    }   

    int subsetSum(vector<int>& nums, int s) {
        int dp[s + 1] = { 0 };
        dp[0] = 1;
        for (int n : nums)
            for (int i = s; i >= n; i--)
                dp[i] += dp[i - n];
        return dp[s];
    }
};
```
</p>


### DP IS EASY! 5 Steps to Think Through DP Questions.
- Author: teampark
- Creation Date: Thu Dec 19 2019 02:27:52 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Dec 19 2019 02:35:11 GMT+0800 (Singapore Standard Time)

<p>
**WARNING:** This will not be a tabulated, perfectly optimized DP solution. We have enough of those. 

This post will walk you through the THINKING process behind Dynamic Programming so that you can solve these questions on your own. 

1. Category
Most dynamic programming questions can be boiled down to a few categories. It\'s important to recognize the category because it allows us to FRAME a new question into something we already know. Frame means use the framework, not copy an approach from another problem into the current problem. You must understand that every DP problem is different. 

	**Question:** Identify this problem as one of the categories below before continuing.

	* 0/1 Knapsack
	* Unbounded Knapsack
	* Shortest Path (eg: Unique Paths I/II)
	* Fibonacci Sequence (eg: House Thief, Jump Game)
	* Longest Common Substring/Subsequeunce 

	**Answer:** 0/1 Knapsack 
	
	*Why 0/1 Knapsack?* Our \'Capacity\' is the target we want to reach \'S\'. Our \'Items\' are the numbers in the input subset and the \'Weights\' of the items are the values of the numbers itself. This question follows 0/1 and not unbounded knapsack because we can use each number ONCE. 
	
	*What is the variation?* The twist on this problem from standard knapsack is that we must add ALL items in the subset to our knapsack. We can reframe the question into adding the positive or negative value of the current number to our knapsack in order to reach the target capacity \'S\'.

2.  States
What variables we need to keep track of in order to reach our optimal result? This Quora post explains state beautifully, so please refer to this link if you are confused: www.quora.com/What-does-a-state-represent-in-terms-of-Dynamic-Programming

	**Question:** Determine State variables.
	*Hint:* As a general rule, Knapsack problems will require 2 states at minimum. 
	
	**Answer:** Index and Current Sum 
	*Why Index?* Index represents the index of the input subset we are considering. This tells us what values we have considered, what values we haven\'t considered, and what value we are currently considering. As a general rule, index is a required state in nearly all dynamic programming problems, except for shortest paths which is row and column instead of a single index but we\'ll get into that in a seperate post. 
	
	*Why Current Sum?* The question asks us if we can sum every item (either the positive or negative value of that item) in the subset to reach the target value. Current Sum gives us the sum of all the values we have processed so far. Our answer revolves around Current Sum being equal to Target. 
	
3.  Decisions
	Dynamic Programming is all about making the optimal decision. In order to make the optimal decision, we will have to try all decisions first. The MIT lecture on DP (highly recommended) refers to this as the guessing step. My brain works better calling this a decision instead of a guess. Decisions will have to bring us closer to the base case and lead us towards the question we want to answer. Base case is covered in Step 4 but really work in tandem with the decision step. 
	
	**Question:** What decisions do we have to make at each recursive call?
	*Hint:* As a general rule, Knapsack problems will require 2 decisions. 
	
	**Answer:** This problem requires we take ALL items in our input subset, so at every step we will be adding an item to our knapsack. Remember, we stated in Step 2 that *"The question asks us if we can sum every item (either the positive or negative value of that item) in the subset to reach the target value."* The decision is: 
	1. Should we add the current numbers positive value 
	2. Should we add the current numbers negative value 

	As a note,  knapsack problems usually don\'t require us to take all items, thus a usual knapsack decision is to take the item or leave the item. 
	
4. Base Case
	Base cases need to relate directly to the conditions required by the answer we are seeking. This is why it is important for our decisions to work towards our base cases, as it means our decisions are working towards our answer. 
	
	Let\'s revisit the conditions for our answers. 
	1. We use all numbers in our input subset.
	2. The sum of all numbers is equal to our target \'S\'.
	
	**Question:** Identify the base cases.
	*Hint:* There are 2 base cases. 
	
	**Answer:** We need 2 base cases. One for when the current state is valid and one for when the current state is invalid.
	1. Valid: Index is out of bounds AND current sum is equal to target \'S\'
	2. Invalid: Index is out of bounds 

	*Why Index is out of bounds?* A condition for our answer is that we use EVERY item in our input subset. When the index is out of bounds, we know we have considered every item in our input subset.
	
   *Why current sum is equal to target?* A condition for our answer is that the sum of using either the positive or negative values of items in our input subet equal to the target sum \'S\'.
   
   If we have considered all the items in our input subset and our current sum is equal to our target, we have successfully met both conditions required by our answer.
   
   On the other hand, if we have considered all the items in our input subset and our current sum is NOT equal to our target, we have only met condition required by our answer. No bueno.
   
5. Code it
If you\'ve thought through all the steps and understand the problem, it\'s trivial to code the actual solution. 
   ```
    def findTargetSumWays(self, nums, S):
        index = len(nums) - 1
        curr_sum = 0
        return self.dp(nums, S, index, curr_sum)
        
    def dp(self, nums, target, index, curr_sum):
		# Base Cases
        if index < 0 and curr_sum == target:
            return 1
        if index < 0:
            return 0 
        
		# Decisions
        positive = self.dp(nums, target, index-1, curr_sum + nums[index])
        negative = self.dp(nums, target, index-1, curr_sum + -nums[index])
        
        return positive + negative
   ```
   
6. Optimize
   Once we introduce memoization, we will only solve each subproblem ONCE. We can remove recursion altogether and avoid the overhead and potential of a stack overflow by introducing tabulation. It\'s important to note that the top down recursive and bottom up tabulation methods perform the EXACT same amount of work. The only different is memory. If they peform the exact same amount of work, the conversion just requires us to specify the order in which problems should be solved. This post is really long now so I won\'t cover these steps here, possibly in a future post.
 
Memoization Solution for Reference
```
class Solution:
    def findTargetSumWays(self, nums, S):
        index = len(nums) - 1
        curr_sum = 0
        self.memo = {}
        return self.dp(nums, S, index, curr_sum)
        
    def dp(self, nums, target, index, curr_sum):
        if (index, curr_sum) in self.memo:
            return self.memo[(index, curr_sum)]
        
        if index < 0 and curr_sum == target:
            return 1
        if index < 0:
            return 0 
        
        positive = self.dp(nums, target, index-1, curr_sum + nums[index])
        negative = self.dp(nums, target, index-1, curr_sum + -nums[index])
        
        self.memo[(index, curr_sum)] = positive + negative
        return self.memo[(index, curr_sum)]
```

Leave a comment on what DP problems you would like this type of post for next and upvote this solution if you found it helpful. I\'d like to get this to the top because I\'m honestly tired of seeing straight optimized tabulated solutions with no THINKING process behind it. 

DP IS EASY!

Thanks.
</p>


### Short Java DP Solution with Explanation
- Author: chidong
- Creation Date: Sun Jan 22 2017 06:11:51 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 21 2018 05:55:55 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public int findTargetSumWays(int[] nums, int s) {
        int sum = 0; 
        for(int i: nums) sum+=i;
        if(s>sum || s<-sum) return 0;
        int[] dp = new int[2*sum+1];
        dp[0+sum] = 1;
        for(int i = 0; i<nums.length; i++){
            int[] next = new int[2*sum+1];
            for(int k = 0; k<2*sum+1; k++){
                if(dp[k]!=0){
                    next[k + nums[i]] += dp[k];
                    next[k - nums[i]] += dp[k];
                }
            }
            dp = next;
        }
        return dp[sum+s];
    }
}
```

![0_1485048724190_Screen Shot 2017-01-21 at 8.31.48 PM.jpg](/uploads/files/1485048726667-screen-shot-2017-01-21-at-8.31.48-pm.jpg)
</p>


