---
title: "String Compression"
weight: 420
#id: "string-compression"
---
## Description
<div class="description">
<p>Given an array of characters <code>chars</code>, compress it using the following algorithm:</p>

<p>Begin with an empty string <code>s</code>. For each group of <strong>consecutive repeating characters</strong> in <code>chars</code>:</p>

<ul>
	<li>If the group&#39;s length is 1, append the character to&nbsp;<code>s</code>.</li>
	<li>Otherwise, append the character followed by the group&#39;s length.</li>
</ul>

<p>The compressed string&nbsp;<code>s</code> <strong>should not be returned separately</strong>, but instead be stored&nbsp;<strong>in the input character array&nbsp;<code>chars</code></strong>. Note that group lengths that are 10 or longer will be split into multiple characters in&nbsp;<code>chars</code>.</p>

<p>After you are done <b>modifying the input array</b>, return <em>the new length of the array</em>.</p>
&nbsp;

<p><b>Follow up:</b><br />
Could you solve it using only <code>O(1)</code> extra space?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> chars = [&quot;a&quot;,&quot;a&quot;,&quot;b&quot;,&quot;b&quot;,&quot;c&quot;,&quot;c&quot;,&quot;c&quot;]
<strong>Output:</strong> Return 6, and the first 6 characters of the input array should be: [&quot;a&quot;,&quot;2&quot;,&quot;b&quot;,&quot;2&quot;,&quot;c&quot;,&quot;3&quot;]
<strong>Explanation:</strong>&nbsp;The groups are &quot;aa&quot;, &quot;bb&quot;, and &quot;ccc&quot;. This compresses to &quot;a2b2c3&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> chars = [&quot;a&quot;]
<strong>Output:</strong> Return 1, and the first character of the input array should be: [&quot;a&quot;]
<strong>Explanation:</strong>&nbsp;The only group is &quot;a&quot;, which remains uncompressed since it&#39;s a single character.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> chars = [&quot;a&quot;,&quot;b&quot;,&quot;b&quot;,&quot;b&quot;,&quot;b&quot;,&quot;b&quot;,&quot;b&quot;,&quot;b&quot;,&quot;b&quot;,&quot;b&quot;,&quot;b&quot;,&quot;b&quot;,&quot;b&quot;]
<strong>Output:</strong> Return 4, and the first 4 characters of the input array should be: [&quot;a&quot;,&quot;b&quot;,&quot;1&quot;,&quot;2&quot;].
<strong>Explanation:</strong>&nbsp;The groups are &quot;a&quot; and &quot;bbbbbbbbbbbb&quot;. This compresses to &quot;ab12&quot;.</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> chars = [&quot;a&quot;,&quot;a&quot;,&quot;a&quot;,&quot;b&quot;,&quot;b&quot;,&quot;a&quot;,&quot;a&quot;]
<strong>Output:</strong> Return 6, and the first 6 characters of the input array should be: [&quot;a&quot;,&quot;3&quot;,&quot;b&quot;,&quot;2&quot;,&quot;a&quot;,&quot;2&quot;].
<strong>Explanation:</strong>&nbsp;The groups are &quot;aaa&quot;, &quot;bb&quot;, and &quot;aa&quot;. This compresses to &quot;a3b2a2&quot;. Note that each group is independent even if two groups have the same character.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= chars.length &lt;= 2000</code></li>
	<li><code>chars[i]</code> is a lower-case English letter, upper-case English letter, digit, or symbol.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Goldman Sachs - 17 (taggedByAdmin: false)
- Amazon - 6 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: true)
- Facebook - 4 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- IBM - 2 (taggedByAdmin: false)
- Expedia - 5 (taggedByAdmin: true)
- Zillow - 4 (taggedByAdmin: false)
- Yandex - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Spotify - 2 (taggedByAdmin: false)
- Wayfair - 4 (taggedByAdmin: false)
- Redfin - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Akuna Capital - 2 (taggedByAdmin: false)
- GoDaddy - 0 (taggedByAdmin: true)
- Snapchat - 0 (taggedByAdmin: true)
- Yelp - 0 (taggedByAdmin: true)
- Lyft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


#### Approach #1: Read and Write Heads [Accepted]

**Intuition**

We will use separate pointers `read` and `write` to mark where we are reading and writing from.  Both operations will be done left to right alternately:  we will read a contiguous group of characters, then write the compressed version to the array.  At the end, the position of the `write` head will be the length of the answer that was written.

**Algorithm**

Let's maintain `anchor`, the start position of the contiguous group of characters we are currently reading.

Now, let's read from left to right.  We know that we must be at the end of the block when we are at the last character, or when the next character is different from the current character.

When we are at the end of a group, we will write the result of that group down using our `write` head.  `chars[anchor]` will be the correct character, and the length (if greater than 1) will be `read - anchor + 1`.  We will write the digits of that number to the array.

**Python**
```python
class Solution(object):
    def compress(self, chars):
        anchor = write = 0
        for read, c in enumerate(chars):
            if read + 1 == len(chars) or chars[read + 1] != c:
                chars[write] = chars[anchor]
                write += 1
                if read > anchor:
                    for digit in str(read - anchor + 1):
                        chars[write] = digit
                        write += 1
                anchor = read + 1
        return write
```

**Java**
```java
class Solution {
    public int compress(char[] chars) {
        int anchor = 0, write = 0;
        for (int read = 0; read < chars.length; read++) {
            if (read + 1 == chars.length || chars[read + 1] != chars[read]) {
                chars[write++] = chars[anchor];
                if (read > anchor) {
                    for (char c: ("" + (read - anchor + 1)).toCharArray()) {
                        chars[write++] = c;
                    }
                }
                anchor = read + 1;
            }
        }
        return write;
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `chars`.

* Space Complexity: $$O(1)$$, the space used by `read`, `write`, and `anchor`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Easy to Understand Java solution
- Author: siddheshshirke
- Creation Date: Mon Oct 30 2017 07:53:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 01:36:32 GMT+0800 (Singapore Standard Time)

<p>
```
public int compress(char[] chars) {
        int indexAns = 0, index = 0;
        while(index < chars.length){
            char currentChar = chars[index];
            int count = 0;
            while(index < chars.length && chars[index] == currentChar){
                index++;
                count++;
            }
            chars[indexAns++] = currentChar;
            if(count != 1)
                for(char c : Integer.toString(count).toCharArray()) 
                    chars[indexAns++] = c;
        }
        return indexAns;
    }
```
</p>


### Beats 100% Java with Explanations
- Author: GraceMeng
- Creation Date: Fri Sep 28 2018 06:24:42 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 03:22:20 GMT+0800 (Singapore Standard Time)

<p>
**Logic**
For a character `ch` in chars, we count its frequency `freq`. 
If freq = 1, we append `ch` only.
Or else, we append `ch` then `freq` (when freq >= 10, freq should cost `String.valueOf(freq).length()` indexes, e.g. \'a12\' should be [a, 1, 2]).
**Code**
```
    public int compress(char[] chars) {
        
        if (chars == null || chars.length == 0)
            return 0;
        
        int index = 0, n = chars.length, i = 0;
        while (i < n) {
            char ch = chars[i];
            int j = i;
            while (j < n && chars[i] == chars[j]) { // chars[i..j - 1] are ch.
                j++;
            }
            int freq = j - i; // The frequency of ch.
            chars[index++] = ch;
            if (freq == 1) {
                
            }                
            else if (freq < 10) {
                chars[index++] = (char)(freq + \'0\');
            }
            else {
                String strFreq = String.valueOf(freq); 
                for (char chFreq : strFreq.toCharArray())
                    chars[index++] = chFreq;
            }
            i = j;
        }
        
        return index;
    }
```
**I appreciate your VOTE UP (\u25B0\u2579\u25E1\u2579\u25B0)**
</p>


### Python Two Pointers - O(n) time O(1) space
- Author: yangshun
- Creation Date: Sun Oct 29 2017 11:26:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 15:05:00 GMT+0800 (Singapore Standard Time)

<p>
1. Group the array into repeated chunks, keeping track of the character and the count. This forms the encoded contents.
2. Update the original array with the encodede contents. We maintain a `left` pointer to know which position to update the original array with the encoded contents and increment it according to the length of the encoded contents.

The encoded contents will definitely be shorter than the original contents, so we can overwrite the original without worries.

*- Yangshun*

```
class Solution(object):
    def compress(self, chars):
        left = i = 0
        while i < len(chars):
            char, length = chars[i], 1
            while (i + 1) < len(chars) and char == chars[i + 1]:
                length, i = length + 1, i + 1
            chars[left] = char
            if length > 1:
                len_str = str(length)
                chars[left + 1:left + 1 + len(len_str)] = len_str
                left += len(len_str)
            left, i = left + 1, i + 1
        return left
```
</p>


