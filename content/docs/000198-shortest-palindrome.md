---
title: "Shortest Palindrome"
weight: 198
#id: "shortest-palindrome"
---
## Description
<div class="description">
<p>Given a string <em><b>s</b></em>, you are allowed to convert it to a palindrome by adding characters in front of it. Find and return the shortest palindrome you can find by performing this transformation.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><code>&quot;aacecaaa&quot;</code>
<strong>Output:</strong> <code>&quot;aaacecaaa&quot;</code>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><code>&quot;abcd&quot;</code>
<strong>Output:</strong> <code>&quot;dcbabcd&quot;</code></pre>
</div>

## Tags
- String (string)

## Companies
- Bloomberg - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Facebook - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Pocket Gems - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach #1 Brute force [Accepted]

**Intuition**

According to the question, we are allowed to insert the characters only at the beginning of the string. Hence, we can find the largest segment from the beginning that is a palindrome, and we can then easily reverse the remaining segment and append to the beginning. This must be the required answer as no shorter palindrome could be found than this by just appending at the beginning.

For example: Take the string $$\text{"abcbabcab"}$$. Here, the largest palindrome segment from beginning is $$\text{"abcba"}$$, and the remaining segment is $$\text{"bcab"}$$. Hence the required string is reverse of $$\text{"bcab"}$$( = $$\text{"bacb"}$$) + original string( = $$\text{"abcbabcab"}$$) = $$\text{"bacbabcbabcab"}$$.

**Algorithm**

* Create the reverse of the original string $$s$$, say $$\text{rev}$$. This is used for comparison to find the largest palindrome segment from the front.
* Iterate over the variable $$i$$ from 0 to the $$\text{size(s)}-1$$:
    * If $$s[0:n-i] == rev[i:]$$ (i.e. substring of $$s$$ from $$0$$ to $$n-i$$ is equal to the substring of $$\text{rev}$$ from $$i$$ to the end of string). This essentially means that that substring from $$0$$ to $$n-i$$ is a palindrome, as $$\text{rev}$$ is the reverse of $$s$$.
    * Since, we find the larger palindromes first, we can return reverse of largest palindrome + $$s$$ as soon as we get it.


<iframe src="https://leetcode.com/playground/ofq6FrQW/shared" frameBorder="0" name="ofq6FrQW" width="100%" height="258"></iframe>

**Complexity Analysis**

* Time complexity: $$O(n^2)$$.
    * We iterate over the entire length of string $$s$$.
    * In each iteration, we compare the substrings which is linear in size of substrings to be compared.
    * Hence, the total time complexity is $$O(n*n) = O(n^2)$$.

* Space complexity: $$O(n)$$ extra space for the reverse string $$\text{rev}$$.

---
#### Approach #2 Two pointers and recursion [Accepted]

**Intuition**

In Approach #1, we found the largest palindrome substring from the string using substring matching which is $$O(n)$$ in length of substring. We could make the process more efficient if we could reduce the size of string to search for the substring without checking the complete substring each time.

Lets take a string $$\text{"abcbabcaba"}$$. Let us consider 2 pointers $$i$$ and $$j$$.
Initialize $$i = 0$$. Iterate over $$j$$ from $$n-1$$ to $$0$$, incrementing $$i$$ each time $$\text{s[i]==s[j]}$$. Now, we just need to search in range $$\text[0,i)$$. This way, we have reduced the size of string to search for the largest palindrome substring from the beginning. The range $$\text{[0,i)}$$ must always contain the largest palindrome substring. The proof of correction is that: Say the string was a perfect palindrome, $$i$$ would be incremented $$n$$ times. Had there been other characters at the end, $$i$$ would still be incremented by the size of the palindrome. Hence, even though there is a chance that the range $$\text{[0,i)}$$ is not always tight, it is ensured that it will always contain the longest palindrome from the beginning.  

The best case for the algorithm is when the entire string is palindrome and the worst case is string like $$\text{"aababababababa"}$$, wherein $$i$$ first becomes $$12$$(check by doing on paper), and we need to recheck in [0,12) corresponding to string $$\text{"aabababababa"}$$. Again continuing in the same way, we get $${i=10}$$.  In such a case, the string is reduced only by as few as 2 elements at each step. Hence, the number of steps in such cases is linear($$n/2$$).

This reduction of length could be easily done with the help of a recursive routine, as shown in the algorithm section.

**Algorithm**

The routine $$\text{shortestPalindrome}$$ is recursive and takes string $$s$$ as parameter:

* Initialize $$i=0$$
* Iterate over $$j$$ from $$n-1$$ to $$0$$:
    * If $$\text{s[i]==s[j]}$$, increase $$i$$ by $$1$$
* If $$i$$ equals the size of $$s$$, the entire string is palindrome, and hence return the entire string $$s$$.
* Else:
    * Return reverse of remaining substring after $$i$$ to the end of string + $$\text{shortestPalindrome}$$ routine on substring from start to index $$i-1$$ + remaining substring after $$i$$ to the end of string.


<iframe src="https://leetcode.com/playground/zeLz2M4w/shared" frameBorder="0" name="zeLz2M4w" width="100%" height="292"></iframe>

**Complexity analysis**

* Time complexity: $$O(n^2)$$.
    * Each iteration of $$\text{shortestPalindrome}$$ is linear in size of substring and the maximum number of recursive calls can be $$n/2$$ times as shown in the Intuition section.
    * Let the time complexity of the algorithm be T(n). Since, at the each step for the worst case, the string can be divide into 2 parts and we require only one part for further computation. Hence, the time complexity for the worst case can be represented as : $$T(n)=T(n-2)+O(n)$$. So, $$T(n) = O(n) + O(n-2) + O(n-4) + ... + O(1)$$ which is  $$O(n^2)$$.

Thanks @CONOVER for the time complexity analysis.

* Space complexity: $$O(n)$$ extra space for $$\text{remain_rev}$$ string.

---
#### Approach #3 KMP [Accepted]

**Intuition**

We have seen that the question boils down to  finding the largest palindrome substring from the beginning.

The people familiar with KMP(Knuth–Morris–Pratt) algorithm may wonder that the task at hand can be easily be compared with the concept of the lookup table in KMP.

*KMP Overview:*

KMP is a string matching algorithm that runs in $$O(n+m)$$ times, where $$n$$ and $$m$$ are sizes of the text and string to be searched respectively. The key component of KMP is the failure function lookup table,say $$f(s)$$. The purpose of the lookup table is to store the length of the proper prefix of the string $$b_{1}b_{2}...b_{s}$$ that is also a suffix of $$b_{1}b_{2}...b_{s}$$. This table is important because if we are trying to match a text string for $$b_{1}b_{2}...b_{n}$$, and we have matched the first $$s$$ positions, but when we fail, then the value of lookup table for $$s$$ is the longest prefix of $$b_{1}b_{2}...b_{n}$$ that could possibly match the text string upto the point we are at. Thus, we don't need to start all over again, and can resume searching from the matching prefix.

The algorithm to generate the lookup table is easy and inutitive, as given below:

```
f(0) = 0
for(i = 1; i < n; i++)
{
	t = f(i-1)
	while(t > 0 && b[i] != b[t])
		t = f(t-1)
	if(b[i] == b[t]){
		++t
	f(i) = t
}
```

* Here, we first set f(0)=0 since, no proper prefix is available.
* Next, iterate over $$i$$ from $$1$$ to $$n-1$$:
    * Set $$t=f(i-1)$$
    * While t>0 and char at $$i$$ doesn't match the char at $$t$$ position, set $$t=f(t)$$, which essentially means that we have problem matching and must consider a shorter prefix, which will be $$b_{f(t-1)}$$, until we find a match or t becomes 0.
    * If $$b_{i}==b_{t}$$, add 1 to t
    * Set $$f(i)=t$$  

The lookup table generation is as illustrated below:

![KMP](../Figures/214/shortest_palindrome.png){:width="600px"}
{:align="center"}

*Wait! I get it!!*

In Approach #1, we reserved the original string $$s$$ and stored it as $$\text{rev}$$. We iterate over $$i$$ from $$0$$ to $$n-1$$ and check for $$s[0:n-i] == rev[i:]$$.
Pondering over this statement, had the $$\text{rev}$$ been concatenated to $$s$$, this statement is just finding the longest prefix that is equal to the suffix. Voila!

**Algorithm**

* We use the KMP lookup table generation
* Create $$\text{new_s}$$ as $$s + \text{"#"} + \text{reverse(s)}$$ and use the string in the lookup-generation algorithm
  	* The "#" in the middle is required, since without the #, the  2 strings could mix with each ther, producing wrong answer. For example, take the string $$\text{"aaaa"}$$. Had we not inserted "#" in the middle, the new string would be $$\text{"aaaaaaaa"}$$ and the largest prefix size would be 7 corresponding to "aaaaaaa" which would be obviously wrong. Hence, a delimiter is required at the middle.
* Return reversed string after the largest palindrome from beginning length(given by $$n-\text{f[n_new-1]}$$) + original string $$s$$


<iframe src="https://leetcode.com/playground/Uu5sN23P/shared" frameBorder="0" name="Uu5sN23P" width="100%" height="360"></iframe>

**Complexity analysis**

* Time complexity: $$O(n)$$.
    * In every iteration of the inner while loop, $$t$$ decreases until it reaches 0 or until it matches. After that, it is incremented by one. Therefore, in the worst case, $$t$$ can only be decreased up to $$n$$ times and increased up to $$n$$ times.
    * Hence, the algorithm is linear with maximum $$(2 * n) * 2$$ iterations.

* Space complexity: $$O(n)$$. Additional space for the reverse string and the concatenated string.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Clean KMP solution with super detailed explanation
- Author: myfavcat123
- Creation Date: Fri Oct 16 2015 12:17:58 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 01:06:33 GMT+0800 (Singapore Standard Time)

<p>
Firstly, let me share my understanding of KMP algorithm.
The key of KMP is to build a look up table that records the match result of prefix and postfix.
Value in the table means the max len of matching substring that exists in both prefix and postfix.
In the prefix this substring should starts from 0, while in the postfix this substring should ends at current index.

For example, now we have a string "ababc"
The KMP table will look like this:

> a b a b c
> 
> 0 0 1 2 0

(Note: we will not match substring with itself, so we will skip index 0)

So how does this table help us search string match faster? 

Well, the answer is if we are trying to match a char after postfix with target string and failed, then we can smartly shift the string, so that the matching string in prefix will replace postfix and now we can try to match the char after prefix with this char in target. 

Take above string as an example.

Now we try to match string "ababc" with "abababc".

We will initially have match as below

> a b a b a b c                  (string x)
> 
> a b a b c                        (string y)
> 
> 0 1 2 3 4 5 6

We found char at index 4 does not match, then we can use lookup table and shift the string y wisely.
We found table[3] = 2, which means we can shift the string y rightward by 2, and still have same but shorter prefix before index 4, like this:

> a b a b a b c (string x)

> ___a b a b c (string y)
> 
> 0 1 2 3 4 5 6

If there is a long gap between prefix and postfix, this shift can help us save a lot of time.
In the brute force way, we cannot do that because we have no information of the string. We have to compare each possible pair of chars. While in kmp, we know the information of string y so we can move smartly. We can directly jump to the next possible matching pair while discard useless pair of chars.

We are almost done with KMP, but we still have one special case that needs to be taken care of.

Say now we have a input like this:

> a a b a a a  (input String)

> 0 1 2 3 4 5  (index)

> 0 1 0 1 2 ? (KMP table)

How should we build the KMP table for this string?

Say the pointer in prefix is "x", which is at index 2 now and the pointer in postfix is "y" which is at index 5 now. we need to match "b" pointed by x with "a" pointed by y. It is an unmatched pair, how should we update the cell?

Well, we really don't need to reset it to 0, that will make us skip a valid shorter matching substring "aa". 
What we do now is just to shorten the length of substring by 1 unit and try to match a shorter substring "aa". This can be done by moving pointer x to the index recorded in [indexOf(x)-1] while keep pointer y stay still. This is because by following the value in KMP table we can always make sure previous part of prefix and postfix is matched even we have shorten their length, so we only need to care about the char after matched part in prefix and postfix.


Use above example:

Firstly we try to compare prefix "aab" with postfix "aaa", pointer in prefix now points to "b" while pointer in postfix now points to "a". So this means current len of postfix/prefix will not give a match, we need to shorten it.


So in the second step, we will fix pointer in postfix, and move pointer in prefix so that we can compare shorter prefix and postfix. The movement of pointer in prefix (say at index x) is done by using KMP table. We will set pointer in prefix to be table [indexOf(x)-1].  In this case, we will move prefix pointer to index 1. So now we try to compare prefix "aa" with postfix "aa".

Finally, we found the matching prefix and postfix, we just update the cell accordingly.


Above is my understanding of KMP algorithm, so how could we apply KMP to this problem



========================== I am just a splitter =================================



This problem asks us to add string before the input so the result string will be a palindrome.
We can convert it to an alternative problem"find the longest palindrome substring starts from index 0".
If we can get the length of such substring, then we can easily build a palindrome string by inserting the reverse part of substring after such substring before the original string. 

Example:

input string:

>  abacd

longest palindrome substring starts from 0:

> aba

Insert the reverse part of substring after palindrome substring before the head:

> dcabacd

Now the problem becomes how to find the longest palindrome substring starts from 0.
We can solve it by using a trick + KMP.

The trick is to build a temp string like this:

> s + "#" + reverse(s)

Then we run KMP on it, the value in last cell will be our solution. In this problem, we don't need to use KMP
to match strings but instead we use the lookup table in KMP to find the palindrome.

We add "#" here to force the match in reverse(s) starts from its first index
What we do in KMP here is trying to find a match between prefix in s  and a postfix in reverse(s). The match part will be palindrome substring.

Example:
input:

> catacb

Temp String:

> catacb # bcatac

KMP table:

> c  a  t  a  c  b  #  b  c  a  t  a  c
> 
> 0  0 0  0  1  0  0 0  1  2  3  4  5

In the last cell, we got a value  5. It means in s we have a substring of length 5 that is palindrome.

So, above is my understanding of KMP any solution towards this problem. Below is my code


    public String shortestPalindrome(String s) {
        String temp = s + "#" + new StringBuilder(s).reverse().toString();
        int[] table = getTable(temp);
        
        //get the maximum palin part in s starts from 0
        return new StringBuilder(s.substring(table[table.length - 1])).reverse().toString() + s;
    }
    
    public int[] getTable(String s){
        //get lookup table
        int[] table = new int[s.length()];
        
        //pointer that points to matched char in prefix part
        
        int index = 0;
        //skip index 0, we will not match a string with itself
        for(int i = 1; i < s.length(); i++){
            if(s.charAt(index) == s.charAt(i)){
                //we can extend match in prefix and postfix
                table[i] = table[i-1] + 1;
                index ++;
            }else{
                //match failed, we try to match a shorter substring
                
                //by assigning index to table[i-1], we will shorten the match string length, and jump to the 
                //prefix part that we used to match postfix ended at i - 1
                index = table[i-1];
                
                while(index > 0 && s.charAt(index) != s.charAt(i)){
                    //we will try to shorten the match string length until we revert to the beginning of match (index 1)
                    index = table[index-1];
                }
                
                //when we are here may either found a match char or we reach the boundary and still no luck
                //so we need check char match
                if(s.charAt(index) == s.charAt(i)){
                    //if match, then extend one char 
                    index ++ ;
                }
                
                table[i] = index;
            }
            
        }
        
        return table;
    }

If I messed up or misunderstood something, please leave comment below. Thanks ~
</p>


### AC in 288 ms, simple brute force
- Author: StefanPochmann
- Creation Date: Fri May 22 2015 22:29:01 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 06:50:08 GMT+0800 (Singapore Standard Time)

<p>
    def shortestPalindrome(self, s):
        r = s[::-1]
        for i in range(len(s) + 1):
            if s.startswith(r[i:]):
                return r[:i] + s

Example: s = `dedcba`. Then r = `abcded` and I try these overlays (the part in `(...)` is the prefix I cut off, I just include it in the display for better understanding):

      s          dedcba
      r[0:]      abcded    Nope...
      r[1:]   (a)bcded     Nope...
      r[2:]  (ab)cded      Nope...
      r[3:] (abc)ded       Yes! Return abc + dedcba
</p>


### My 7-lines recursive Java solution
- Author: xcv58
- Creation Date: Tue Aug 11 2015 01:22:08 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 06:03:30 GMT+0800 (Singapore Standard Time)

<p>
The idea is to use two anchors `j` and `i` to compare the String from beginning and end.
If `j` can reach the end, the String itself is Palindrome. Otherwise, we divide the String by `j`, and get `mid = s.substring(0, j)` and `suffix`.

We reverse `suffix` as beginning of result and recursively call `shortestPalindrome` to get result of `mid` then appedn `suffix` to get result.

        int j = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) == s.charAt(j)) { j += 1; }
        }
        if (j == s.length()) { return s; }
        String suffix = s.substring(j);
        return new StringBuffer(suffix).reverse().toString() + shortestPalindrome(s.substring(0, j)) + suffix;
</p>


