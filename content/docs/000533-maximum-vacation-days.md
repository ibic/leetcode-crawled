---
title: "Maximum Vacation Days"
weight: 533
#id: "maximum-vacation-days"
---
## Description
<div class="description">
<p>
LeetCode wants to give one of its best employees the option to travel among <b>N</b> cities to collect algorithm problems. But all work and no play makes Jack a dull boy, you could take vacations in some particular cities and weeks. Your job is to schedule the traveling to maximize the number of vacation days you could take, but there are certain rules and restrictions you need to follow.
</p>

<p><b>Rules and restrictions:</b><br>
<ol>
<li>You can only travel among <b>N</b> cities, represented by indexes from 0 to N-1. Initially, you are in the city indexed 0 on <b>Monday</b>.</li>
<li>The cities are connected by flights. The flights are represented as a <b>N*N</b> matrix (not necessary symmetrical), called <b>flights</b> representing the airline status from the city i to the city j. If there is no flight from the city i to the city j, <b>flights[i][j] = 0</b>; Otherwise, <b>flights[i][j] = 1</b>. Also, <b>flights[i][i] = 0</b> for all i.</li>
<li>You totally have <b>K</b> weeks (<b>each week has 7 days</b>) to travel. You can only take flights at most once <b>per day</b> and can only take flights on each week's <b>Monday</b> morning. Since flight time is so short, we don't consider the impact of flight time.</li>
<li>For each city, you can only have restricted vacation days in different weeks, given an <b>N*K</b> matrix called <b>days</b> representing this relationship. For the value of <b>days[i][j]</b>, it represents the maximum days you could take vacation in the city <b>i</b> in the week <b>j</b>.</li>
</ol>
</p>

<p>You're given the <b>flights</b> matrix and <b>days</b> matrix, and you need to output the maximum vacation days you could take during <b>K</b> weeks.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b>flights = [[0,1,1],[1,0,1],[1,1,0]], days = [[1,3,1],[6,0,3],[3,3,3]]
<b>Output:</b> 12
<b>Explanation:</b> <br>Ans = 6 + 3 + 3 = 12. <br>
One of the best strategies is:
1st week : fly from city 0 to city 1 on Monday, and play 6 days and work 1 day. <br/>(Although you start at city 0, we could also fly to and start at other cities since it is Monday.) 
2nd week : fly from city 1 to city 2 on Monday, and play 3 days and work 4 days.
3rd week : stay at city 2, and play 3 days and work 4 days.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b>flights = [[0,0,0],[0,0,0],[0,0,0]], days = [[1,1,1],[7,7,7],[7,7,7]]
<b>Output:</b> 3
<b>Explanation:</b> <br>Ans = 1 + 1 + 1 = 3. <br>
Since there is no flights enable you to move to another city, you have to stay at city 0 for the whole 3 weeks. <br/>For each week, you only have one day to play and six days to work. <br/>So the maximum number of vacation days is 3.
</pre>
</p>

<p><b>Example 3:</b><br />
<pre>
<b>Input:</b>flights = [[0,1,1],[1,0,1],[1,1,0]], days = [[7,0,0],[0,7,0],[0,0,7]]
<b>Output:</b> 21
<b>Explanation:</b><br>Ans = 7 + 7 + 7 = 21<br>
One of the best strategies is:
1st week : stay at city 0, and play 7 days. 
2nd week : fly from city 0 to city 1 on Monday, and play 7 days.
3rd week : fly from city 1 to city 2 on Monday, and play 7 days.
</pre>
</p>


<p><b>Note:</b><br>
<ol>
<li><b>N and K</b> are positive integers, which are in the range of [1, 100].</li>
<li>In the matrix <b>flights</b>, all the values are integers in the range of [0, 1].</li>
<li>In the matrix <b>days</b>, all the values are integers in the range [0, 7].</li>
<li>You could stay at a city beyond the number of vacation days, but you should <b>work</b> on the extra days, which won't be counted as vacation days.</li>
<li>If you fly from the city A to the city B and take the vacation on that day, the deduction towards vacation days will count towards the vacation days of city B in that week.</li>
<li>We don't consider the impact of flight hours towards the calculation of vacation days.</li>
</ol>
</p>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Using Depth First Search [Time Limit Exceeded]

**Algorithm**

In the brute force approach, we make use of a recursive function $$dfs$$, which returns the number of vacations which can be taken startring from $$cur\_city$$ as the current city and $$weekno$$ as the starting week. 

In every function call, we traverse over all the cities(represented by $$i$$) and find out all the cities which are connected to the current city, $$cur\_city$$. Such a city is represented by a 1 at the corresponding $$flights[cur\_city][i]$$ position. Now, for the current city, we can either travel to the city which is connected to it or we can stay in the same city. Let's say the city to which we change our location from the current city be represented by $$j$$. Thus, after changing the city, we need to find the number of vacations which we can take from the new city as the current city and the incremented week as the new starting week. This count of vacations can be represented as: $$days[j][weekno] + dfs(flights, days, j, weekno + 1)$$. 

Thus, for the current city, we obtain a number of vacations by choosing different cities as the next cities. Out of all of these vacations coming from different cities, we can find out the maximum number of vacations that need to be returned for every $$dfs$$ function call.

<iframe src="https://leetcode.com/playground/7nvGhHgq/shared" frameBorder="0" name="7nvGhHgq" width="100%" height="377"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^k)$$. Depth of Recursion tree will be $$k$$ and each node contains $$n$$ branches in the worst case. Here $$n$$ represents the number of cities and $$k$$ is the total number of weeks.

* Space complexity : $$O(k)$$. The depth of the recursion tree is $$k$$.

---

#### Approach #2 Using DFS with memoization [Accepted]:

**Algorithm**

In the last approach, we make a number of redundant function calls, since the same function call of the form `dfs(flights, days, cur_city, weekno)` can be made multiple number of times with the same $$cur\_city$$ and $$weekno$$. These redundant calls can be pruned off if we make use of memoization. 

In order to remove these redundant function calls, we make use of a 2-D memoization array $$memo$$. In this array, $$memo[i][j]$$ is used to store the number of vacactions that can be taken using the $$i^{th}$$ city as the current city and the $$j^{th}$$ week as the starting week. This result is equivalent to that obtained using the function call: `dfs(flights, days, i, j)`. Thus, if the $$memo$$ entry corresponding to the current function call already contains a valid value, we can directly obtain the result from this array instead of going deeper into recursion.

<iframe src="https://leetcode.com/playground/JawnLfLv/shared" frameBorder="0" name="JawnLfLv" width="100%" height="445"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2k)$$. $$memo$$ array of size $$n*k$$ is filled and each cell filling takes O(n) time .

* Space complexity : $$O(n*k)$$. $$memo$$ array of size $$n*k$$ is used. Here $$n$$ represents the number of cities and $$k$$ is the total number of weeks.

---

#### Approach #3 Using 2-D Dynamic Programming [Accepted]:

**Algorithm**

The idea behind this approach is as follows. The maximum number of vacations that can be taken given we start from the $$i^{th}$$ city in the $$j^{th}$$ week is not dependent on the the vacations that can be taken in the earlier weeks. It only depends on the number of vacations that can be taken in the upcoming weeks and also on the connections between the various cities($$flights$$).

Therefore, we can make use of a 2-D $$dp$$, in which $$dp[i][k]$$ represents the maximum number of vacations which can be taken starting from the $$i^{th}$$ city in the $$k^{th}$$ week. This $$dp$$ is filled in the backward manner(in terms of the week number).

While filling up the entry for $$dp[i][k]$$, we need to consider the following cases:

1. We start from the $$i^{th}$$ city in the $$k^{th}$$ week and stay in the same city for the $$(k+1)^{th}$$ week. Thus, the factor to be considered for updating the $$dp[i][k]$$ entry will be given by: $$days[i][k] + dp[i, k+1]$$.

2. We start from the $$i^{th}$$ city in the $$k^{th}$$ week and move to the $$j^{th}$$ city in the $$(k+1)^{th}$$ week. But, for changing the city in this manner, we need to be able to move from the $$i^{th}$$ city to the $$j^{th}$$ city i.e. $$flights[i][j]$$ should be 1 for such $$i$$ and $$j$$. 

But, while changing the city from $$i^{th}$$ city in the $$k^{th}$$ week, we can move to any $$j^{th}$$ city such that a connection exists between the $$i^{th}$$ city and the $$j^{th}$$ city i.e. $$flights[i][j]=1$$. But, in order to maximize the number of vacations that can be taken starting from the $$i^{th}$$ city in the $$k^{th}$$ week, we need to choose the destination city that leads to maximum no. of vacations. Thus, the factor to be considered here, is given by: $$\text{max}days[j][k] + days[j, k+1]$$, for all $$i$$, $$j$$, $$k$$ satisfying $$flights[i][j] = 1$$, $$0 &leq; i,j &leq; n, where $$n$$ refers to the number of cities.

At the end, we need to find the maximum out of these two factors to update the $$dp[i][k]$$ value.

In order to fill the $$dp$$ values, we start by filling the entries for the last week and proceed backwards. At last, the value of $$dp[0][0]$$ gives the required result.

The following animation illustrates the process of filling the $$dp$$ array.

!?!../Documents/568_Maximum_Vacation_Days.json:1000,563!?!

Below code is inspired by [@hackerhuang](http://leetcode.com/hackerhuang)

<iframe src="https://leetcode.com/playground/hLAaTwUh/shared" frameBorder="0" name="hLAaTwUh" width="100%" height="343"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2k)$$. $$dp$$ array of size $$n*k$$ is filled and each cell filling takes O(n) time. Here $$n$$ represents the number of cities and $$k$$ is the total number of weeks.

* Space complexity : $$O(n*k)$$. $$dp$$ array of size $$n*k$$ is used. 

---

#### Approach #4 Using 1-D Dynamic Programming [Accepted]:

**Algorithm**

As can be observed in the previous approach, in order to update the $$dp$$ entries for $$i^{th}$$ week, we only need the values corresponding to $$(i+1)^{th}$$ week along with the $$days$$ and $$flights$$ array. Thus, instead of using a 2-D $$dp$$ array, we can omit the dimension corresponding to the weeks and make use of a 1-D $$dp$$ array. 

Now, $$dp[i]$$ is used to store the number of vacations that provided that we start from the $$i^{th}$$ city in the current week. The procedure remains the same as that of the previous approach, except that we make the updations in the same $$dp$$ row again and again. In order to store the $$dp$$ values corresponding to the current week temporarily, we make use of a $$temp$$ array so that the original $$dp$$ entries corresponding to $$week+1$$ aren't altered.

<iframe src="https://leetcode.com/playground/6eAfgP4u/shared" frameBorder="0" name="6eAfgP4u" width="100%" height="394"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2k)$$. $$dp$$ array of size $$n*k$$ is filled and each cell filling takes O(n) time. Here $$n$$ represents the number of cities and $$k$$ is the total number of weeks.

* Space complexity : $$O(k)$$. $$dp$$ array of size $$nk$$ is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java DFS(TLE) and DP Solutions
- Author: shawngao
- Creation Date: Sun Apr 30 2017 11:12:16 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 10:02:08 GMT+0800 (Singapore Standard Time)

<p>
Solution 1. DFS. The idea is just try each ```possible``` city for every week and keep tracking the ```max``` vacation days. Time complexity O(N^K). Of course it will TLE....
```
public class Solution {
    int max = 0, N = 0, K = 0;
    
    public int maxVacationDays(int[][] flights, int[][] days) {
        N = flights.length;
        K = days[0].length;
        dfs(flights, days, 0, 0, 0);
        
        return max;
    }
    
    private void dfs(int[][] f, int[][] d, int curr, int week, int sum) {
        if (week == K) {
            max = Math.max(max, sum);
            return;
        }
        
        for (int dest = 0; dest < N; dest++) {
            if (curr == dest || f[curr][dest] == 1) {
                dfs(f, d, dest, week + 1, sum + d[dest][week]);
            }
        }
    }
}
```
Solution 2. DP.  ```dp[i][j]``` stands for the max vacation days we can get in week ```i``` staying in city ```j```. It's obvious that ```dp[i][j] = max(dp[i - 1][k] + days[j][i]) (k = 0...N - 1, if we can go from city k to city j)```. Also because values of week ```i``` only depends on week ```i - 1```, so we can compress two dimensional ```dp``` array to one dimension. Time complexity O(K * N * N) as we can easily figure out from the 3 level of loops.
```
public class Solution {
    public int maxVacationDays(int[][] flights, int[][] days) {
        int N = flights.length;
        int K = days[0].length;
        int[] dp = new int[N];
        Arrays.fill(dp, Integer.MIN_VALUE);
        dp[0] = 0;
        
        for (int i = 0; i < K; i++) {
            int[] temp = new int[N];
            Arrays.fill(temp, Integer.MIN_VALUE);
            for (int j = 0; j < N; j++) {
                for(int k = 0; k < N; k++) {
                    if (j == k || flights[k][j] == 1) {
                        temp[j] = Math.max(temp[j], dp[k] + days[j][i]);
                    }
                }
            }
            dp = temp;
        }
        
        int max = 0;
        for (int v : dp) {
            max = Math.max(max, v);
        }
        
        return max;
    }
}
```
</p>


### Java DP Solution
- Author: JingXXX
- Creation Date: Mon Jan 01 2018 01:11:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 11:43:36 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int maxVacationDays(int[][] flights, int[][] days) {
        
        if(days == null || days.length == 0 || flights == null || flights.length == 0
          || days[0].length == 0 || flights[0].length == 0) return 0;
        
        int n = days.length, k = days[0].length;
        int[][] dp = new int[n][k];
        for(int i = 0; i < n; i ++) {
            for(int j = 0; j < k; j ++) {
                dp[i][j] = -1;
            }
        }
        dp[0][0] = days[0][0];
        for(int i = 1; i < n; i ++) {
            if(flights[0][i] == 1) dp[i][0] = days[i][0];
        }
        
        for(int week = 1; week < k; week ++) {
            for(int dest = 0; dest < n; dest ++) {
                for(int depart = 0; depart < n; depart ++) {
                    if(dp[depart][week - 1] != -1 && (flights[depart][dest] == 1 || depart == dest)) {
                        dp[dest][week] = Math.max(dp[dest][week], dp[depart][week - 1] + days[dest][week]);
                    }
                }
            }
        }
        
        int res = 0;
        for(int city = 0; city < n; city ++) {
            if(dp[city][k - 1] > res) res = dp[city][k - 1];
        }
        return res;
    }
}
```
</p>


### Python, Simple with Explanation
- Author: awice
- Creation Date: Sun Apr 30 2017 11:22:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 30 2017 11:22:08 GMT+0800 (Singapore Standard Time)

<p>
Let's maintain ```best[i]```, the most vacation days you can have ending in city ```i``` on week t.  At the end, we simply want max(best), the best answer for any ending city.

For every flight ```i -> j``` (including staying in the same city, when ```i == j```), we have a candidate answer best[j] = best[i] + days[j][t], and we want the best answer of those.

When the graph is sparse, we can precompute ```flights_available[i] = [j for j, adj in enumerate(flights[i]) if adj or i == j]``` instead to save some time, but this is not required.

```
def maxVacationDays(self, flights, days):
    NINF = float('-inf')
    N, K = len(days), len(days[0])
    best = [NINF] * N
    best[0] = 0
    
    for t in xrange(K):
        cur = [NINF] * N
        for i in xrange(N):
            for j, adj in enumerate(flights[i]):
                if adj or i == j:
                    cur[j] = max(cur[j], best[i] + days[j][t])
        best = cur
    return max(best)
```
</p>


