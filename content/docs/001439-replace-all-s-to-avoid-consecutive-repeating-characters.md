---
title: "Replace All ?'s to Avoid Consecutive Repeating Characters"
weight: 1439
#id: "replace-all-s-to-avoid-consecutive-repeating-characters"
---
## Description
<div class="description">
<p>Given a string&nbsp;<code>s</code><var>&nbsp;</var>containing only lower case English letters&nbsp;and the &#39;?&#39;&nbsp;character, convert <strong>all </strong>the &#39;?&#39; characters into lower case letters such that the final string does not contain any <strong>consecutive repeating&nbsp;</strong>characters.&nbsp;You <strong>cannot </strong>modify the non &#39;?&#39; characters.</p>

<p>It is <strong>guaranteed </strong>that there are no consecutive repeating characters in the given string <strong>except </strong>for &#39;?&#39;.</p>

<p>Return the final string after all the conversions (possibly zero) have been made. If there is more than one solution, return any of them.&nbsp;It can be shown that an answer is always possible with the given constraints.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;?zs&quot;
<strong>Output:</strong> &quot;azs&quot;
<strong>Explanation</strong>: There are 25 solutions for this problem. From &quot;azs&quot; to &quot;yzs&quot;, all are valid. Only &quot;z&quot; is an invalid modification as the string will consist of consecutive repeating characters in &quot;zzs&quot;.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;ubv?w&quot;
<strong>Output:</strong> &quot;ubvaw&quot;
<strong>Explanation</strong>: There are 24 solutions for this problem. Only &quot;v&quot; and &quot;w&quot; are invalid modifications as the strings will consist of consecutive repeating characters in &quot;ubvvw&quot; and &quot;ubvww&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;j?qg??b&quot;
<strong>Output:</strong> &quot;jaqgacb&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;??yw?ipkj?&quot;
<strong>Output:</strong> &quot;acywaipkja&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length&nbsp;&lt;= 100</code></li>
	<li><code>s</code> contains&nbsp;only lower case English letters and <code>&#39;?&#39;</code>.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Microsoft - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Python O(n)
- Author: votrubac
- Creation Date: Sun Sep 06 2020 12:01:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 12:59:25 GMT+0800 (Singapore Standard Time)

<p>
We do not need more than 3 letters to build a non-repeating character sequence.

For Python, we can use set difference to determine which one to use.

**Python**
```python
def modifyString(self, s: str) -> str:
	res, prev = "", \'?\'
	for i, c in enumerate(s):
		next = s[i + 1] if i + 1 < len(s) else \'?\'
		prev = c if c != \'?\' else {\'a\', \'b\', \'c\'}.difference({prev, next}).pop()
		res += prev
	return res
```

**C++**
```cpp
string modifyString(string s) {
    for (auto i = 0; i < s.size(); ++i)
        if (s[i] == \'?\')
            for (s[i] = \'a\'; s[i] <= \'c\'; ++s[i])
                if ((i == 0 || s[i - 1] != s[i]) && (i == s.size() - 1 || s[i + 1] != s[i]))
                    break;
    return s;
}
```
</p>


### Java Simple O(n) loop
- Author: hobiter
- Creation Date: Sun Sep 06 2020 12:01:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 12:01:14 GMT+0800 (Singapore Standard Time)

<p>
for each char, just try \u2018a\u2019, \u2018b\u2019, \u2018c\u2019, and select the one not the same as neighbors.

```
    public String modifyString(String s) {
        char[] arr = s.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == \'?\') {
                for (int j = 0; j < 3; j++) {
                    if (i > 0 && arr[i - 1] - \'a\' == j) continue;
                    if (i + 1 < arr.length && arr[i + 1] - \'a\' == j) continue;
                    arr[i] = (char) (\'a\' + j);
                    break;
                }
            }
        }
        return String.valueOf(arr);
    }
```

</p>


### [Python3] one of three letters
- Author: ye15
- Creation Date: Sun Sep 06 2020 12:02:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 12:02:34 GMT+0800 (Singapore Standard Time)

<p>

```
class Solution:
    def modifyString(self, s: str) -> str:
        s = list(s)
        for i in range(len(s)):
            if s[i] == "?": 
                for c in "abc": 
                    if (i == 0 or s[i-1] != c) and (i+1 == len(s) or s[i+1] != c): 
                        s[i] = c
                        break 
        return "".join(s)
```
</p>


