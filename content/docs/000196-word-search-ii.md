---
title: "Word Search II"
weight: 196
#id: "word-search-ii"
---
## Description
<div class="description">
<p>Given a 2D board and a list of words from the dictionary, find all words in the board.</p>

<p>Each word must be constructed from letters of sequentially adjacent cell, where &quot;adjacent&quot; cells are those horizontally or vertically neighboring. The same letter cell may not be used more than once in a word.</p>

<p>&nbsp;</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> 
<b>board </b>= [
  [&#39;<span style="color:#d70">o</span>&#39;,&#39;<span style="color:#d70">a</span>&#39;,&#39;a&#39;,&#39;n&#39;],
  [&#39;e&#39;,&#39;<span style="color:#d30">t</span>&#39;,&#39;<span style="color:#d00">a</span>&#39;,&#39;<span style="color:#d00">e</span>&#39;],
  [&#39;i&#39;,&#39;<span style="color:#d70">h</span>&#39;,&#39;k&#39;,&#39;r&#39;],
  [&#39;i&#39;,&#39;f&#39;,&#39;l&#39;,&#39;v&#39;]
]
<b>words</b> = <code>[&quot;oath&quot;,&quot;pea&quot;,&quot;eat&quot;,&quot;rain&quot;]</code>

<strong>Output:&nbsp;</strong><code>[&quot;eat&quot;,&quot;oath&quot;]</code>
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>All inputs are consist of lowercase letters <code>a-z</code>.</li>
	<li>The values of&nbsp;<code>words</code> are distinct.</li>
</ol>

</div>

## Tags
- Backtracking (backtracking)
- Trie (trie)

## Companies
- Amazon - 22 (taggedByAdmin: false)
- Apple - 11 (taggedByAdmin: false)
- Microsoft - 6 (taggedByAdmin: true)
- Google - 3 (taggedByAdmin: true)
- Cisco - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Snapchat - 4 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Zillow - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Uber - 7 (taggedByAdmin: false)
- Airbnb - 3 (taggedByAdmin: true)
- Yahoo - 3 (taggedByAdmin: false)
- Roblox - 2 (taggedByAdmin: false)
- Houzz - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Backtracking with Trie

**Intuition**

The problem is actually a simplified crossword puzzle game, where the word solutions have been given on the board embedded with some noise letters. All we need to to do is to *cross* them out.

>Intuitively, in order to cross out all potential words, the overall strategy would be to iterate the cell one by one, and from each cell we walk along its neighbors in four potential directions to find matched words.
While wandering around the board, we would stop the exploration when we know it would not lead to the discovery of new words.

One might have guessed the paradigm that we would use for this problem. Yes, it is **backtracking**, which would be the backbone of the solution. It is fairly simply to construct a solution of backtracking. One could even follow a template given in our [Explore card of Recursion II](https://leetcode.com/explore/learn/card/recursion-ii/472/backtracking/2793/).

The key of the solution lies on _how we find the matches of word from the dictionary_. Intuitively, one might resort to the hashset data structure (_e.g._ `set()` in Python). This could work.

However, during the backtracking process, one would encounter more often the need to tell if there exists any word that contains certain prefix, rather than if a string exists as a word in the dictionary. Because if we know that there does not exist any match of word in the dictionary for a given prefix, then we would not need to further explore certain direction. And this, would greatly reduce the exploration space, therefore improve the performance of the backtracking algorithm.

The capability of finding matching prefix is where the data structure called [Trie](https://leetcode.com/explore/learn/card/trie/) would shine, comparing the hashset data structure. Not only can Trie tell the membership of a word, but also it can instantly find the words that share a given prefix. As it turns out, the choice of data structure (`Trie` _VS._ `hashset`), which could end with a solution that ranks either the top $$5\%$$ or the bottom $$5\%$$.

Here we show an example of Trie that is built from a list of words. As one can see from the following graph, at the node denoted `d`, we would know there are at least two words with the prefix `d` from the dictionary.

![pic](../Figures/212/212_trie_example.png)

We have a problem about [implementing a Trie data structure](https://leetcode.com/problems/implement-trie-prefix-tree/). One can start with the Trie problem as warm up, and come back this problem later.


**Algorithm**

The overall workflow of the algorithm is intuitive, which consists of a loop over each cell in the board and a recursive function call starting from the cell. Here is the skeleton of the algorithm.

- We build a Trie out of the words in the dictionary, which would be used for the matching process later.

- Starting from each cell, we start the backtracking exploration (_i.e._ `backtracking(cell)`), if there exists any word in the dictionary that starts with the letter in the cell.

- During the recursive function call `backtracking(cell)`, we explore the neighbor cells (_i.e._ `neighborCell`) around the current cell for the next recursive call `backtracking(neighborCell)`. At each call, we check if the sequence of letters that we traverse so far matches any word in the dictionary, with the help of the Trie data structure that we built at the beginning.

Here is an overall impression how the algorithm works. We give some sample implementation based on the rough idea above. And later, we detail some optimization that one could further apply to the algorithm. 

![pic](../Figures/212/212_trie_algo.png)

<iframe src="https://leetcode.com/playground/5ayu6UzY/shared" frameBorder="0" width="100%" height="500" name="5ayu6UzY"></iframe>

To better understand the backtracking process, we demonstrate how we find the match of `dog` along the Trie in the following animation.

!?!../Documents/212_LIS.json:1000,474!?!

#### Optimizations

In the above implementation, we applied a few tricks to further speed up the running time, in addition to the application of the Trie data structure. In particular, the Python implementation could run faster than `98%` of the submissions. We detail the tricks as follows, ordered by their significance.

>_Backtrack along the nodes in Trie._

- One could use Trie simply as a dictionary to quickly find the match of words and prefixes, _i.e._ at _each step_ of backtracking, we start all over from the root of the Trie. This could work.
- However, a more efficient way would be to traverse the Trie together with the progress of backtracking, _i.e._ at each step of `backtracking(TrieNode)`, the depth of the `TrieNode` corresponds to the length of the prefix that we've matched so far. This measure could lift your solution out of the bottom $$5\%$$ of submissions.

>_Gradually **prune** the nodes in Trie during the backtracking._

- The idea is motivated by the fact that the time complexity of the overall algorithm sort of depends on the size of the Trie. For a leaf node in Trie, once we traverse it (_i.e._ find a matched word), we would no longer need to traverse it again. As a result, we could prune it out from the Trie.

- Gradually, those non-leaf nodes could become leaf nodes later, since we trim their children leaf nodes. In the extreme case, the Trie would become empty, once we find a match for all the words in the dictionary. This pruning measure could reduce up to $$50\%$$ of the running time for the test cases of the online judge.

![pic](../Figures/212/212_trie_prune.png)

> _Keep words in the Trie_.

- One might use a flag in the Trie node to indicate if the path to the current code match any word in the dictionary. It is not necessary to keep the words in the Trie.
- However, doing so could improve the performance of the algorithm a bit. One benefit is that one would not need to pass the prefix as the parameter in the `backtracking()` call. And this could speed up a bit the recursive call. Similarly, one does not need to reconstruct the matched word from the prefix, if we keep the words in Trie.

>_Remove the matched words from the Trie._

- In the problem, we are asked to return all the matched words, rather than the number of potential matches. Therefore, once we reach certain Trie node that contains a match of word, we could simply remove the match from the Trie.
- As a _side_ benefit, we do not need to check if there is any duplicate in the result set. As a result, we could simply use a list instead of set to keep the results, which could speed up the solution a bit.
<br/>


**Complexity**

- Time complexity: $$\mathcal{O}(M(4\cdot3^{L-1}))$$, where $$M$$ is the number of cells in the board and $$L$$ is the maximum length of words.
  
    - It is tricky is calculate the exact number of steps that a backtracking algorithm would perform. We provide a upper bound of steps for the worst scenario for this problem. The algorithm loops over all the cells in the board, therefore we have $$M$$ as a factor in the complexity formula. It then boils down to the _maximum_ number of steps we would need for each starting cell (_i.e._$$4\cdot3^{L-1}$$).
  
    - Assume the maximum length of word is $$L$$, starting from a cell, initially we would have at most 4 directions to explore. Assume each direction is valid (_i.e._ worst case), during the following exploration, we have at most 3 neighbor cells (excluding the cell where we come from) to explore. As a result, we would traverse at most $$4\cdot3^{L-1}$$ cells during the backtracking exploration.

    - One might wonder what the worst case scenario looks like. Well, here is an example. Imagine, each of the cells in the board contains the letter `a`, and the word dictionary contains a single word `['aaaa']`. Voila. This is one of the worst scenarios that the algorithm would encounter.
![pic](../Figures/212/212_complexity_example.png)
  
    - Note that, the above time complexity is estimated under the assumption that the Trie data structure would not change once built. If we apply the optimization trick to gradually remove the nodes in Trie, we could greatly improve the time complexity, since the cost of backtracking would reduced to zero once we match all the words in the dictionary, _i.e._ the Trie becomes empty.

<br/>

- Space Complexity: $$\mathcal{O}(N)$$, where $$N$$ is the total number of letters in the dictionary.

    - The main space consumed by the algorithm is the Trie data structure we build. In the worst case where there is no overlapping of prefixes among the words, the Trie would have as many nodes as the letters of all words. And optionally, one might keep a copy of words in the Trie as well. As a result, we might need $$2N$$ space for the Trie.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 15ms Easiest Solution (100.00%)
- Author: yavinci
- Creation Date: Tue Jan 05 2016 10:09:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 04:01:01 GMT+0800 (Singapore Standard Time)

<p>
<h2>Backtracking + Trie</h2>
<hr>

Intuitively, start from every cell and try to build a word in the dictionary. `Backtracking (dfs)` is the powerful way to exhaust every possible ways. Apparently, we need to do `pruning` when current character is not in any word. 

1. How do we instantly know the current character is invalid? `HashMap`? 
2. How do we instantly know what's the next valid character? `LinkedList`?
3. But the next character can be chosen from a list of characters. `"Mutil-LinkedList"`?

Combing them, `Trie` is the natural choice. Notice that:

1. `TrieNode` is all we need. `search` and `startsWith` are useless.
2. No need to store character at TrieNode. `c.next[i] != null` is enough.
3. Never use `c1 + c2 + c3`. Use `StringBuilder`.
4. No need to use `O(n^2)` extra space `visited[m][n].` 
5. No need to use `StringBuilder`. Storing `word` itself at leaf node is enough.
6. No need to use `HashSet` to de-duplicate. Use "one time search" trie.

For more explanations, check out [dietpepsi's blog][1].

<hr>
<h2>Code Optimization</h2>
<hr>

UPDATE: Thanks to @dietpepsi we further improved from `17ms` to `15ms`.

1. `59ms`: Use `search` and `startsWith` in Trie class like [this popular solution.][2]
2. `33ms`: Remove Trie class which unnecessarily starts from `root` in every `dfs` call. 
3. `30ms`: Use `w.toCharArray()` instead of `w.charAt(i)`.
4. `22ms`: Use `StringBuilder` instead of `c1 + c2 + c3`.
5. `20ms`: Remove `StringBuilder` completely by storing `word` instead of `boolean` in TrieNode.
6. `20ms`: Remove `visited[m][n]` completely by modifying `board[i][j] = '#'` directly.
7. `18ms`: check validity, e.g., `if(i > 0) dfs(...)`, before going to the next `dfs`.
8. `17ms`: De-duplicate `c - a` with one variable `i`.
9. `15ms`: Remove `HashSet` completely. dietpepsi's idea is awesome. 

The final run time is `15ms`. Hope it helps!

<hr>

    public List<String> findWords(char[][] board, String[] words) {
        List<String> res = new ArrayList<>();
        TrieNode root = buildTrie(words);
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                dfs (board, i, j, root, res);
            }
        }
        return res;
    }
    
    public void dfs(char[][] board, int i, int j, TrieNode p, List<String> res) {
        char c = board[i][j];
        if (c == '#' || p.next[c - 'a'] == null) return;
        p = p.next[c - 'a'];
        if (p.word != null) {   // found one
            res.add(p.word);
            p.word = null;     // de-duplicate
        }

        board[i][j] = '#';
        if (i > 0) dfs(board, i - 1, j ,p, res); 
        if (j > 0) dfs(board, i, j - 1, p, res);
        if (i < board.length - 1) dfs(board, i + 1, j, p, res); 
        if (j < board[0].length - 1) dfs(board, i, j + 1, p, res); 
        board[i][j] = c;
    }
    
    public TrieNode buildTrie(String[] words) {
        TrieNode root = new TrieNode();
        for (String w : words) {
            TrieNode p = root;
            for (char c : w.toCharArray()) {
                int i = c - 'a';
                if (p.next[i] == null) p.next[i] = new TrieNode();
                p = p.next[i];
           }
           p.word = w;
        }
        return root;
    }
    
    class TrieNode {
        TrieNode[] next = new TrieNode[26];
        String word;
    }


  [1]: http://algobox.org/word-search-ii/
  [2]: https://leetcode.com/discuss/36337/my-simple-and-clean-java-code-using-dfs-and-trie
</p>


### Python dfs solution (directly use Trie implemented).
- Author: OldCodingFarmer
- Creation Date: Sun Aug 30 2015 05:09:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 05:46:28 GMT+0800 (Singapore Standard Time)

<p>
Here is an implementation based on  [Implement Trie][1] in LeetCode. TrieNode, Trie, Solution are treated as seperated classes. 

    class TrieNode():
        def __init__(self):
            self.children = collections.defaultdict(TrieNode)
            self.isWord = False
        
    class Trie():
        def __init__(self):
            self.root = TrieNode()
        
        def insert(self, word):
            node = self.root
            for w in word:
                node = node.children[w]
            node.isWord = True
        
        def search(self, word):
            node = self.root
            for w in word:
                node = node.children.get(w)
                if not node:
                    return False
            return node.isWord
        
    class Solution(object):
        def findWords(self, board, words):
            res = []
            trie = Trie()
            node = trie.root
            for w in words:
                trie.insert(w)
            for i in xrange(len(board)):
                for j in xrange(len(board[0])):
                    self.dfs(board, node, i, j, "", res)
            return res
        
        def dfs(self, board, node, i, j, path, res):
            if node.isWord:
                res.append(path)
                node.isWord = False
            if i < 0 or i >= len(board) or j < 0 or j >= len(board[0]):
                return 
            tmp = board[i][j]
            node = node.children.get(tmp)
            if not node:
                return 
            board[i][j] = "#"
            self.dfs(board, node, i+1, j, path+tmp, res)
            self.dfs(board, node, i-1, j, path+tmp, res)
            self.dfs(board, node, i, j-1, path+tmp, res)
            self.dfs(board, node, i, j+1, path+tmp, res)
            board[i][j] = tmp


  [1]: https://leetcode.com/problems/implement-trie-prefix-tree/
</p>


### My simple and clean Java code using DFS and Trie
- Author: Lnic
- Creation Date: Tue May 19 2015 16:14:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 01:03:34 GMT+0800 (Singapore Standard Time)

<p>
Compared with [Word Search][1], I make my DFS with a tire but a word. The Trie is formed by all the words in given *words*. Then during the DFS, for each current formed word, I check if it is in the Trie.

    public class Solution {
        Set<String> res = new HashSet<String>();
        
        public List<String> findWords(char[][] board, String[] words) {
            Trie trie = new Trie();
            for (String word : words) {
                trie.insert(word);
            }
            
            int m = board.length;
            int n = board[0].length;
            boolean[][] visited = new boolean[m][n];
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    dfs(board, visited, "", i, j, trie);
                }
            }
            
            return new ArrayList<String>(res);
        }
        
        public void dfs(char[][] board, boolean[][] visited, String str, int x, int y, Trie trie) {
            if (x < 0 || x >= board.length || y < 0 || y >= board[0].length) return;
            if (visited[x][y]) return;
            
            str += board[x][y];
            if (!trie.startsWith(str)) return;
            
            if (trie.search(str)) {
                res.add(str);
            }
            
            visited[x][y] = true;
            dfs(board, visited, str, x - 1, y, trie);
            dfs(board, visited, str, x + 1, y, trie);
            dfs(board, visited, str, x, y - 1, trie);
            dfs(board, visited, str, x, y + 1, trie);
            visited[x][y] = false;
        }
    }
 

  [1]: https://leetcode.com/problems/word-search/
</p>


