---
title: "N-ary Tree Preorder Traversal"
weight: 696
#id: "n-ary-tree-preorder-traversal"
---
## Description
<div class="description">
<p>Given an n-ary tree, return the <i>preorder</i> traversal of its nodes&#39; values.</p>

<p><em>Nary-Tree input serialization&nbsp;is represented in their level order traversal, each group of children is separated by the null value (See examples).</em></p>

<p>&nbsp;</p>

<p><strong>Follow up:</strong></p>

<p>Recursive solution is trivial, could you do it iteratively?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/12/narytreeexample.png" style="width: 100%; max-width: 300px;" /></p>

<pre>
<strong>Input:</strong> root = [1,null,3,2,4,null,5,6]
<strong>Output:</strong> [1,3,5,6,2,4]
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/11/08/sample_4_964.png" style="width: 296px; height: 241px;" /></p>

<pre>
<strong>Input:</strong> root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
<strong>Output:</strong> [1,2,3,6,7,11,14,4,8,12,5,9,13,10]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The height of the n-ary tree is less than or equal to <code>1000</code></li>
	<li>The total number of nodes is between <code>[0,&nbsp;10^4]</code></li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Iterations

**Algorithm**

First of all, please refer to [this article](https://leetcode.com/articles/binary-tree-preorder-transversal/) 
for the solution in case of binary tree.
This article offers the same ideas with a bit of generalisation. 

First of all, here is the definition of the tree ```Node``` which we would use
in the following implementation.

<iframe src="https://leetcode.com/playground/565ADxkZ/shared" frameBorder="0" width="100%" height="259" name="565ADxkZ"></iframe>

Let's start from the root and then at each iteration 
pop the current node out of the stack and
push its child nodes. 
In the implemented strategy we push nodes into output list 
following the order ```Top->Bottom``` and ```Left->Right```, that
naturally reproduces preorder traversal.

<iframe src="https://leetcode.com/playground/vCK9MjUf/shared" frameBorder="0" width="100%" height="395" name="vCK9MjUf"></iframe>


**Complexity Analysis**

* Time complexity : we visit each node exactly once,
 and for each visit, the complexity of the operation
 (*i.e.* appending the child nodes) is
 proportional to the number of child nodes ```n``` (n-ary tree).
 Therefore the overall time complexity is $$\mathcal{O}(N)$$,
where $$N$$ is the number of nodes, *i.e.* the size of tree.

* Space complexity : depending on the tree structure, 
we could keep up to the entire tree, therefore, the space complexity is $$\mathcal{O}(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Iterative and Recursive Solutions
- Author: icydragoon
- Creation Date: Tue Jul 10 2018 15:55:45 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 11:48:36 GMT+0800 (Singapore Standard Time)

<p>
Iterative Solution
```
class Solution {
    public List<Integer> preorder(Node root) {
        List<Integer> list = new ArrayList<>();
        if (root == null) return list;
        
        Stack<Node> stack = new Stack<>();
        stack.add(root);
        
        while (!stack.empty()) {
            root = stack.pop();
            list.add(root.val);
            for (int i = root.children.size() - 1; i >= 0; i--)
                stack.add(root.children.get(i));
        }
        
        return list;
    }
}
```

Recursive Solution
```
class Solution {
    public List<Integer> list = new ArrayList<>();
    public List<Integer> preorder(Node root) {
        if (root == null)
            return list;
        
        list.add(root.val);
        for(Node node: root.children)
            preorder(node);
                
        return list;
    }
}
```
</p>


### C++ 44ms beats 100% both iterative and recursive
- Author: MrQuin
- Creation Date: Fri Jul 13 2018 11:42:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 14:04:14 GMT+0800 (Singapore Standard Time)

<p>
recursive
```
class Solution {
private:
    void travel(Node* root, vector<int>& result) {
        if (root == nullptr) {
            return;
        }
        
        result.push_back(root -> val);
        for (Node* child : root -> children) {
            travel(child, result);
        }
    }
public:
    vector<int> preorder(Node* root) {
        vector<int> result;
        travel(root, result);
        return result;
    }
};
```

iterative:
Added an example to elaborate how stack works in the preorder traversal.
Let\'s have a tree like below:
```
          1
     /        \
   2            3
 /   \         /   \
4     5       6     7
```
The right preorder sequence will be:
```
1  2  4  5  3  6  7
```
From the code we use a stack to simulate the process:
1. we push 1 to the stack.
2. we pop 1 out, add 1 into result; Add the children of 1 into stack. The value in the stack will be 3, 2 and 2 at the top position;
3. we pop 2 out and add it to result; Then we add children of 2 into stack. So the stack will be like 3, 5, 4 and with 4 at the top.
4. we pop 4 and 5 out of stack since they are leaf node. Currently result will be like 1, 2, 4, 5.
5. we pop 3 out and add its children into stack. The stack is like 7, 6 with 6 at the top.
6. we pop 6 and 7 out and the stack becomes empty.
So the final result will be 1, 2, 4, 5, 3, 6, 7
```
class Solution {
public:
    vector<int> preorder(Node* root) {
        vector<int> result;
        if (root == nullptr) {
            return result;
        }
        
        stack<Node*> stk;
        stk.push(root);
        while (!stk.empty()) {
            Node* cur = stk.top();
            stk.pop();
            result.push_back(cur -> val);
            for (int i = cur -> children.size() - 1; i >= 0; i--) {
                if (cur -> children[i] != nullptr) {
                    stk.push(cur -> children[i]);
                }
            }
        }
        return result;
    }
};
```
</p>


### Python short iterative solution beats 100% // 66 ms faster than fastest !
- Author: cenkay
- Creation Date: Fri Jul 13 2018 00:53:26 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 10:51:01 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def preorder(self, root):
        ret, q = [], root and [root]
        while q:
            node = q.pop()
            ret.append(node.val)
            q += [child for child in node.children[::-1] if child]
        return ret
```
</p>


