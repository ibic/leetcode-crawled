---
title: "Divisor Game"
weight: 995
#id: "divisor-game"
---
## Description
<div class="description">
<p>Alice and Bob take turns playing a game, with Alice starting first.</p>

<p>Initially, there is a number <code>N</code>&nbsp;on the chalkboard.&nbsp; On each player&#39;s turn, that player makes a <em>move</em>&nbsp;consisting of:</p>

<ul>
	<li>Choosing&nbsp;any <code>x</code> with <code>0 &lt; x &lt; N</code> and <code>N % x == 0</code>.</li>
	<li>Replacing&nbsp;the number&nbsp;<code>N</code>&nbsp;on the chalkboard with <code>N - x</code>.</li>
</ul>

<p>Also, if a player cannot make a move, they lose the game.</p>

<p>Return <code>True</code> if and only if Alice wins the game, assuming both players play optimally.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">2</span>
<strong>Output: </strong><span id="example-output-1">true</span>
<strong>Explanation:</strong> Alice chooses 1, and Bob has no more moves.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">3</span>
<strong>Output: </strong><span id="example-output-2">false</span>
<strong>Explanation:</strong> Alice chooses 1, Bob chooses 1, and Alice has no more moves.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= N &lt;= 1000</code></li>
</ol>
</div>
</div>
</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Visa - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### just return N % 2 == 0 (proof)
- Author: bupt_wc
- Creation Date: Sun Apr 14 2019 12:00:50 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 16 2019 13:49:26 GMT+0800 (Singapore Standard Time)

<p>
prove it by two steps:
1. if Alice will lose for N, then Alice will must win for N+1, since Alice can first just make N decrease 1.
2. for any odd number N, it only has odd factor, so after the first move, it will be an even number

let\'s check the inference
fisrt N = 1, Alice lose. then Alice will must win for 2.
if N = 3, since all even number(2) smaller than 3 will leads Alice win, so Alice will lose for 3
3 lose -> 4 win
all even number(2,4) smaller than 5 will leads Alice win, so Alice will lose for 5
...

Therefore, Alice will always win for even number, lose for odd number.

</p>


### [Java/C++/Python] return N % 2 == 0
- Author: lee215
- Creation Date: Sun Apr 14 2019 12:05:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 14 2019 12:05:56 GMT+0800 (Singapore Standard Time)

<p>
## **Conclusion**
If `N` is even, can win.
If `N` is odd, will lose.

## **Recursive Prove \uFF08Top-down)**

1. If `N` is even.
We can choose `x = 1`.
The opponent will get N - 1, which is a odd.
Reduce to the case odd and he will lose.

2. If `N` is odd,
2.1 If `N = 1`, lose directly.
2.2 We have to choose an odd `x`.
The opponent will get N - x, which is a even.
Reduce to the case even and he will win.

3. So the N will change odd and even alternatively until N = 1.

## **Mathematical Induction Prove \uFF08Bottom-up)**

1. `N = 1`, lose directly
2. `N = 2`, will win choosing `x = 1`.
3. `N = 3`, must lose choosing `x = 1`.
4. `N = 4`, will win choosing `x = 1`.

....

For N <= n, we have find that:
If `N` is even, can win.
If `N` is odd, will lose.

**For the case `N = n + 1`**
**If `N` is even**, we can win choosing `x = 1`,
give the opponent an odd number `N - 1 = n`,
force him to lose,
because we have found that all odd `N <= n` will lose.

**If `N` is odd**, there is no even `x` that `N % x == 0`.
As a result, we give the opponent a even number `N - x`,
and the opponent can win,
because we have found that all even `N <= n` can win.

Now we prove that, for all `N <= n`,
If `N` is even, can win.
If `N` is odd, will lose.


**Java/C++:**
```
        return N % 2 == 0;
```

**Python:**
```
        return N % 2 == 0
```

</p>


### Come on in, different explanation from others
- Author: 416486188
- Creation Date: Wed May 22 2019 09:24:14 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 18 2019 03:23:03 GMT+0800 (Singapore Standard Time)

<p>
After second consideration after writing this post, I think the most upvoted solution is the still better. So please just take it as fun :( 

---
**Prerequiste**

3 "definites" you need to know
1. **Anyone who gets `1` definitely loses** since not exist a `x` in range of `(0, 1)`
2. **Anyone who gets `2` definitely wins** since he always/or only can make `2` become `1`. Because `x` can only be 1, `2 % 1 == 0` and `2 - 1 = 1`
3. **For any `N >= 2`, `N` will definitely be reduced to `2`**

	Just illustrate some `N` below. Based on the "chain", you can see, no matter what `N` starts, all leads to `2` in the end:
	```
	//  2 -> 1
	    3 -> 2
	//  4 -> 2, 3
	    5 -> 4
	//  6 -> 3, 4, 5
		7 -> 6
	//  8 -> 4, 6, 7
	    9 -> 6, 8
	//  10 -> 5, 8, 9
	    11 -> 10
	//  12 -> 8, 9, 10, 11
		13 -> 12
	//  14 -> 7, 12, 13
		15 -> 10, 12
	//  16 -> 8, 12, 14
		17 -> 16
	//  18 -> 9, 12, 15, 16
		19 -> 18
	//  20 -> 10, 15, 16, 18
	...
	```


So Alice and Bob both say: `Give me 2`! They fight for `2` and it becomes the point of their "life". But how can they gurantee that they can get `2`?

However after study, they find out:
1. For odd `N`, no matter what `x` is, `x` must be odd, then `N - x` must be even, i.e. `odd =< even`
2. For even `N`, we can always choose `x` as `1`, then `N - x = N - 1` must be odd, `even => odd`

They get the **conclusion**: 
```
The one who gets even number `N` has the choice to get all the even numbers including 2(since 2 is even), so here comes win.
```

---
So if `N` is even initially, 
**1. What is Alice\'s optimal strategy?**
Choose `x = 1`, `N = N - 1`, send the odd back to Bob
**2. What is Bob\'s optimal strategy?**
Choose `x` as large as possible to make `N` reduce as fast as possible such that the pain can end as early as possible, since nothing will change the fact that he will lose. Just don\'t suffer.

---
**Strategy**

From above, we know that if Alice meets even `N`, she will win. So we just need to check if `N` is divisble by `2`.

---
**Final Code**

```java
class Solution {
    public boolean divisorGame(int N) {
        return (N & 1) == 0;   
    }
}
```
</p>


