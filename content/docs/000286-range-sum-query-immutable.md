---
title: "Range Sum Query - Immutable"
weight: 286
#id: "range-sum-query-immutable"
---
## Description
<div class="description">
<p>Given an integer array <code>nums</code>, find the sum of the elements between indices <code>i</code> and <code>j</code> <code>(i &le; j)</code>, inclusive.</p>

<p>Implement the&nbsp;<code>NumArray</code> class:</p>

<ul>
	<li><code>NumArray(int[] nums)</code> Initializes the object with the integer array <code>nums</code>.</li>
	<li><code>int sumRange(int i, int j)</code> Return the sum of the elements of the <code>nums</code> array in the range <code>[i, j]</code> inclusive (i.e., <code>sum(nums[i], nums[i + 1], ... , nums[j])</code>)</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;NumArray&quot;, &quot;sumRange&quot;, &quot;sumRange&quot;, &quot;sumRange&quot;]
[[[-2, 0, 3, -5, 2, -1]], [0, 2], [2, 5], [0, 5]]
<strong>Output</strong>
[null, 1, -1, -3]

<strong>Explanation</strong>
NumArray numArray = new NumArray([-2, 0, 3, -5, 2, -1]);
numArray.sumRange(0, 2); // return 1 ((-2) + 0 + 3)
numArray.sumRange(2, 5); // return -1 (3 + (-5) + 2 + (-1)) 
numArray.sumRange(0, 5); // return -3 ((-2) + 0 + 3 + (-5) + 2 + (-1))
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= nums.length &lt;= 10<sup>4</sup></code></li>
	<li><code>-10<sup>5</sup>&nbsp;&lt;= nums[i] &lt;=&nbsp;10<sup>5</sup></code></li>
	<li><code>0 &lt;= i &lt;= j &lt; nums.length</code></li>
	<li>At most <code>10<sup>4</sup></code> calls will be made to <code>sumRange</code>.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Palantir Technologies - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach #1 (Brute Force) [Time Limit Exceeded]

Each time *sumRange* is called, we use a for loop to sum each individual element from index $$i$$ to $$j$$.

```java
private int[] data;

public NumArray(int[] nums) {
    data = nums;
}

public int sumRange(int i, int j) {
    int sum = 0;
    for (int k = i; k <= j; k++) {
        sum += data[k];
    }
    return sum;
}
```

**Complexity analysis:**

* Time complexity : $$O(n)$$ time per query.
Each *sumRange* query takes $$O(n)$$ time.

* Space complexity : $$O(1)$$. Note that `data` is a *reference* to `nums` and is not a copy of it.

---
#### Approach #2 (Caching) [Accepted]

Imagine that *sumRange* is called one thousand times with the exact same arguments. How could we speed that up?

We could trade in extra space for speed. By pre-computing all range sum possibilities and store its results in a hash table, we can speed up the query to constant time.

```java
private Map<Pair<Integer, Integer>, Integer> map = new HashMap<>();

public NumArray(int[] nums) {
    for (int i = 0; i < nums.length; i++) {
        int sum = 0;
        for (int j = i; j < nums.length; j++) {
            sum += nums[j];
            map.put(Pair.create(i, j), sum);
        }
    }
}

public int sumRange(int i, int j) {
    return map.get(Pair.create(i, j));
}
```

**Complexity analysis**

* Time complexity : $$O(1)$$ time per query, $$O(n^2)$$ time pre-computation.
The pre-computation done in the constructor takes $$O(n^2)$$ time. Each *sumRange* query's time complexity is $$O(1)$$ as the hash table's look up operation is constant time.

* Space complexity : $$O(n^2)$$.
The extra space required is $$O(n^2)$$ as there are $$n$$ candidates for both $$i$$ and $$j$$.

---
#### Approach #3 (Caching) [Accepted]

The above approach takes a lot of space, could we optimize it?

Imagine that we pre-compute the cummulative sum from index $$0$$ to $$k$$. Could we use this information to derive $$Sum(i, j)$$?

Let us define $$sum[k]$$ as the cumulative sum for $$nums[0 \cdots k-1]$$ (inclusive):

$$
sum[k] = \left\{ \begin{array}{rl} \sum_{i=0}^{k-1}nums[i] & , k > 0 \\ 0 &, k = 0 \end{array} \right.
$$

Now, we can calculate *sumRange* as following:

$$
sumRange(i, j) = sum[j + 1] - sum[i]
$$

```java
private int[] sum;

public NumArray(int[] nums) {
    sum = new int[nums.length + 1];
    for (int i = 0; i < nums.length; i++) {
        sum[i + 1] = sum[i] + nums[i];
    }
}

public int sumRange(int i, int j) {
    return sum[j + 1] - sum[i];
}
```

Notice in the code above we inserted a dummy 0 as the first element in the *sum* array. This trick saves us from an extra conditional check in *sumRange* function.

**Complexity analysis**

* Time complexity : $$O(1)$$ time per query, $$O(n)$$ time pre-computation.
Since the cumulative sum is cached, each *sumRange* query can be calculated in $$O(1)$$ time.

* Space complexity : $$O(n)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java simple O(n) init and O(1) query solution
- Author: arthur13
- Creation Date: Tue Nov 10 2015 11:37:12 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 02:13:11 GMT+0800 (Singapore Standard Time)

<p>
public class NumArray {

    int[] nums;
    
    public NumArray(int[] nums) {
        for(int i = 1; i < nums.length; i++)
            nums[i] += nums[i - 1];
        
        this.nums = nums;
    }

    public int sumRange(int i, int j) {
        if(i == 0)
            return nums[j];
        
        return nums[j] - nums[i - 1];
    }
}
</p>


### 5-lines C++, 4-lines Python
- Author: jianchao-li
- Creation Date: Tue Nov 10 2015 12:41:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 21:58:21 GMT+0800 (Singapore Standard Time)

<p>
The idea is fairly straightforward: create an array `accu` that stores the accumulated sum for `nums` such that `accu[i] = nums[0] + ... + nums[i - 1]` in the initializer of `NumArray`. Then just return `accu[j + 1] - accu[i]` in `sumRange`. You may try the example in the problem statement to convince yourself of this idea.

The code is as follows.

----------
**C++**

    class NumArray {
    public:
        NumArray(vector<int> &nums) {
            accu.push_back(0);
            for (int num : nums)
                accu.push_back(accu.back() + num);
        }
    
        int sumRange(int i, int j) {
            return accu[j + 1] - accu[i];
        }
    private:
        vector<int> accu;
    };
    
    
    // Your NumArray object will be instantiated and called as such:
    // NumArray numArray(nums);
    // numArray.sumRange(0, 1);
    // numArray.sumRange(1, 2); 

----------
**Python**

    class NumArray(object):
        def __init__(self, nums):
            """
            initialize your data structure here.
            :type nums: List[int]
            """
            self.accu = [0]
            for num in nums: 
                self.accu += self.accu[-1] + num,
    
        def sumRange(self, i, j):
            """
            sum of elements nums[i..j], inclusive.
            :type i: int 
            :type j: int
            :rtype: int 
            """
            return self.accu[j + 1] - self.accu[i]
    
    
    # Your NumArray object will be instantiated and called as such:
    # numArray = NumArray(nums)
    # numArray.sumRange(0, 1)
    # numArray.sumRange(1, 2)
</p>


### C++ O(1) queries - just 2 extra lines of code
- Author: rantos22
- Creation Date: Mon Nov 23 2015 04:14:07 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 18 2018 00:32:11 GMT+0800 (Singapore Standard Time)

<p>
    class NumArray {
    public:
        NumArray(vector<int> &nums) : psum(nums.size()+1, 0) {
            partial_sum( nums.begin(), nums.end(), psum.begin()+1);
        }
    
        int sumRange(int i, int j) {
            return psum[j+1] - psum[i];
        }
    private:
        vector<int> psum;    
    };
</p>


