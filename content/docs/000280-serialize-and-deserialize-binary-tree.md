---
title: "Serialize and Deserialize Binary Tree"
weight: 280
#id: "serialize-and-deserialize-binary-tree"
---
## Description
<div class="description">
<p>Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, or transmitted across a network connection link to be reconstructed later in the same or another computer environment.</p>

<p>Design an algorithm to serialize and deserialize a binary tree. There is no restriction on how your serialization/deserialization algorithm should work. You just need to ensure that a binary tree can be serialized to a string and this string can be deserialized to the original tree structure.</p>

<p><strong>Clarification:</strong> The input/output format is the same as <a href="/faq/#binary-tree">how LeetCode serializes a binary tree</a>. You do not necessarily need to follow this format, so please be creative and come up with different approaches yourself.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/15/serdeser.jpg" style="width: 442px; height: 324px;" />
<pre>
<strong>Input:</strong> root = [1,2,3,null,null,4,5]
<strong>Output:</strong> [1,2,3,null,null,4,5]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = []
<strong>Output:</strong> []
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [1]
<strong>Output:</strong> [1]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root = [1,2]
<strong>Output:</strong> [1,2]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is in the range <code>[0, 10<sup>4</sup>]</code>.</li>
	<li><code>-1000 &lt;= Node.val &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Tree (tree)
- Design (design)

## Companies
- Facebook - 43 (taggedByAdmin: true)
- Amazon - 23 (taggedByAdmin: true)
- Google - 5 (taggedByAdmin: true)
- Microsoft - 5 (taggedByAdmin: true)
- eBay - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Square - 2 (taggedByAdmin: false)
- LinkedIn - 11 (taggedByAdmin: true)
- Uber - 6 (taggedByAdmin: true)
- Quora - 4 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Qualtrics - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Expedia - 4 (taggedByAdmin: false)
- Tableau - 3 (taggedByAdmin: false)
- Indeed - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Cohesity - 2 (taggedByAdmin: false)
- Bloomberg - 0 (taggedByAdmin: true)
- Yahoo - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Depth First Search (DFS)


**Intuition**

<center><img src="../Figures/297_BST.png" width="550px" /></center>

The **serialization** of a `Binary Search Tree` is essentially to encode
its values and more importantly its structure. 
One can traverse the tree to accomplish the above task.
And it is well know that we have two general strategies to do so:

- *Breadth First Search* (`BFS`)

    We scan through the tree level by level, following the order of height,
    from top to bottom. The nodes on higher level would be visited before
    the ones with lower levels.
     
- *Depth First Search* (`DFS`)

    In this strategy, we adopt the `depth` as the priority, so that one
    would start from a root and reach all the way down to certain leaf,
    and then back to root to reach another branch.

    The DFS strategy can further be distinguished as
    `preorder`, `inorder`, and `postorder` depending on the relative order
    among the root node, left node and right node.
    
In this task, however, the `DFS` strategy is more adapted for our needs,
since the linkage among the adjacent nodes is naturally encoded in the order,
which is rather helpful for the later task of **deserialization**. 

Therefore, in this solution, we demonstrate an example with the `preorder` DFS strategy.
One can check out more tutorial about `Binary Search Tree` on 
the [LeetCode Explore](https://leetcode.com/explore/learn/card/introduction-to-data-structure-binary-search-tree/).

**Algorithm**

First of all, here is the definition of the ```TreeNode``` which we would use
in the following implementation.

<iframe src="https://leetcode.com/playground/gKbx4EPz/shared" frameBorder="0" width="100%" height="225" name="gKbx4EPz"></iframe>



The preorder DFS traverse follows *recursively* the order of  
`root -> left subtree -> right subtree`.

As an example, let's serialize the following tree.
Note that serialization contains information about the node values
as well as the information about the tree structure.


<!--![LIS](../Figures/297/297_tr.gif)-->
!?!../Documents/297_LIS.json:1000,622!?!


We start from the root, node `1`, the serialization string is ```1,```.
Then we jump to its left subtree with the root node `2`, and the serialization string becomes ```1,2,```.
Now starting from node `2`, 
we visit its left node `3` (```1,2,3,None,None,```) 
and right node `4` (```1,2,3,None,None,4,None,None```) sequentially.
Note that ```None,None,``` appears for each leaf to mark the absence of left and right child node, 
this is how we save the tree structure during the serialization.
And finally, we get back to the root node `1` and visit its right subtree
which happens to be a leaf node `5`. Finally, the serialization string is done
as ```1,2,3,None,None,4,None,None,5,None,None,```.


<iframe src="https://leetcode.com/playground/KzKSosy9/shared" frameBorder="0" width="100%" height="395" name="KzKSosy9"></iframe>

Now let's deserialize the serialization string constructed above ```1,2,3,None,None,4,None,None,5,None,None,```.
It goes along the string, initiate the node value and 
then calls itself to construct its left and right child nodes. 

<iframe src="https://leetcode.com/playground/byCTAyWj/shared" frameBorder="0" width="100%" height="446" name="byCTAyWj"></iframe>

**Complexity Analysis**

* Time complexity : in both serialization and deserialization functions,
 we visit each node exactly once, thus the time complexity is $$O(N)$$,
 where $$N$$ is the number of nodes, *i.e.* the size of tree. 

* Space complexity : in both serialization and deserialization functions,
 we keep the entire tree, either at the beginning or at the end,
 therefore, the space complexity is $$O(N)$$. 
 
The solutions with BFS or other DFS strategies normally will have the same
time and space complexity.
 
**Further Space Optimization**

In the above solution, we store the node value and 
the references to ```None``` child nodes, 
which means $$N \cdot V + 2N$$ complexity, 
where $$V$$ is the size of value. 
That is called *natural serialization*, and has was implemented above.

The $$N \cdot V$$ component here is the encoding of values, can't be optimized further,
but there is a way to reduce $$2N$$ part 
which is the encoding of the tree structure.

The number of unique binary tree structures that can be 
constructed using `n` nodes is $$C(n)$$, where $$C(n)$$ is the `nth` Catalan
number. Please refer to [this article](https://leetcode.com/articles/unique-binary-search-trees/) for more information.

There are $$C(n)$$ possible structural configurations of 
a binary tree with n nodes, so the largest index value 
that we might need to store is $$C(n) - 1$$.
That means storing the index value could require up to 
1 bit for $$n \leq 2$$, or $$\lceil log_2(C(n) - 1) \rceil$$ bits for $$n > 2$$.

In this way one could reduce the encoding of the tree structure by $$\log N$$.
More precisely, the [Catalan numbers](https://en.wikipedia.org/wiki/Catalan_number) grow as
$$C(n) \sim \frac{4^n}{n^{3/2}\sqrt{\pi}}$$ and hence the theoretical minimum of storage
 for the tree structure that could be achieved
is $$log(C(n)) \sim 2n - \frac{3}{2}\log(n) - \frac{1}{2}\log(\pi)$$

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy to understand Java Solution
- Author: gavinlinasd
- Creation Date: Mon Oct 26 2015 15:18:09 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 15:29:03 GMT+0800 (Singapore Standard Time)

<p>
The idea is simple: print the tree in pre-order traversal and use "X" to denote null node and split node with ",". We can use a StringBuilder for building the string on the fly. For deserializing, we use a Queue to store the pre-order traversal and since we have "X" as null node, we know exactly how to where to end building subtress.

    public class Codec {
        private static final String spliter = ",";
        private static final String NN = "X";
    
        // Encodes a tree to a single string.
        public String serialize(TreeNode root) {
            StringBuilder sb = new StringBuilder();
            buildString(root, sb);
            return sb.toString();
        }
    
        private void buildString(TreeNode node, StringBuilder sb) {
            if (node == null) {
                sb.append(NN).append(spliter);
            } else {
                sb.append(node.val).append(spliter);
                buildString(node.left, sb);
                buildString(node.right,sb);
            }
        }
        // Decodes your encoded data to tree.
        public TreeNode deserialize(String data) {
            Deque<String> nodes = new LinkedList<>();
            nodes.addAll(Arrays.asList(data.split(spliter)));
            return buildTree(nodes);
        }
        
        private TreeNode buildTree(Deque<String> nodes) {
            String val = nodes.remove();
            if (val.equals(NN)) return null;
            else {
                TreeNode node = new TreeNode(Integer.valueOf(val));
                node.left = buildTree(nodes);
                node.right = buildTree(nodes);
                return node;
            }
        }
    }
</p>


### Recursive preorder, Python and C++, O(n)
- Author: StefanPochmann
- Creation Date: Mon Oct 26 2015 19:03:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 10:27:03 GMT+0800 (Singapore Standard Time)

<p>
**Python**

    class Codec:
    
        def serialize(self, root):
            def doit(node):
                if node:
                    vals.append(str(node.val))
                    doit(node.left)
                    doit(node.right)
                else:
                    vals.append('#')
            vals = []
            doit(root)
            return ' '.join(vals)
    
        def deserialize(self, data):
            def doit():
                val = next(vals)
                if val == '#':
                    return None
                node = TreeNode(int(val))
                node.left = doit()
                node.right = doit()
                return node
            vals = iter(data.split())
            return doit()

---

**C++**

    class Codec {
    public:
    
        string serialize(TreeNode* root) {
            ostringstream out;
            serialize(root, out);
            return out.str();
        }
    
        TreeNode* deserialize(string data) {
            istringstream in(data);
            return deserialize(in);
        }
    
    private:
    
        void serialize(TreeNode* root, ostringstream& out) {
            if (root) {
                out << root->val << ' ';
                serialize(root->left, out);
                serialize(root->right, out);
            } else {
                out << "# ";
            }
        }
    
        TreeNode* deserialize(istringstream& in) {
            string val;
            in >> val;
            if (val == "#")
                return nullptr;
            TreeNode* root = new TreeNode(stoi(val));
            root->left = deserialize(in);
            root->right = deserialize(in);
            return root;
        }
    };
</p>


### Short and straight forward BFS Java code with a queue
- Author: annieqt
- Creation Date: Mon Dec 07 2015 13:57:31 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 11:26:39 GMT+0800 (Singapore Standard Time)

<p>
Here I use typical BFS method to handle a binary tree. I use string `n` to represent null values. The string of the binary tree in the example will be `"1 2 3 n n 4 5 n n n n "`.

When deserialize the string, I assign left and right child for each not-null node, and add the not-null children to the queue, waiting to be handled later.



    public class Codec {
        public String serialize(TreeNode root) {
            if (root == null) return "";
            Queue<TreeNode> q = new LinkedList<>();
            StringBuilder res = new StringBuilder();
            q.add(root);
            while (!q.isEmpty()) {
                TreeNode node = q.poll();
                if (node == null) {
                    res.append("n ");
                    continue;
                }
                res.append(node.val + " ");
                q.add(node.left);
                q.add(node.right);
            }
            return res.toString();
        }

        public TreeNode deserialize(String data) {
            if (data == "") return null;
            Queue<TreeNode> q = new LinkedList<>();
            String[] values = data.split(" ");
            TreeNode root = new TreeNode(Integer.parseInt(values[0]));
            q.add(root);
            for (int i = 1; i < values.length; i++) {
                TreeNode parent = q.poll();
                if (!values[i].equals("n")) {
                    TreeNode left = new TreeNode(Integer.parseInt(values[i]));
                    parent.left = left;
                    q.add(left);
                }
                if (!values[++i].equals("n")) {
                    TreeNode right = new TreeNode(Integer.parseInt(values[i]));
                    parent.right = right;
                    q.add(right);
                }
            }
            return root;
        }
    }
</p>


