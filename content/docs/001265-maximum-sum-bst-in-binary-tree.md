---
title: "Maximum Sum BST in Binary Tree"
weight: 1265
#id: "maximum-sum-bst-in-binary-tree"
---
## Description
<div class="description">
<p>Given a <strong>binary tree</strong> <code>root</code>, the task is to return the maximum sum of all keys of <strong>any</strong>&nbsp;sub-tree which is also a Binary Search Tree (BST).</p>

<p>Assume a BST is defined as follows:</p>

<ul>
	<li>The left subtree of a node contains only nodes with keys&nbsp;<strong>less than</strong>&nbsp;the node&#39;s key.</li>
	<li>The right subtree of a node contains only nodes with keys&nbsp;<strong>greater than</strong>&nbsp;the node&#39;s key.</li>
	<li>Both the left and right subtrees must also be binary search trees.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/01/30/sample_1_1709.png" style="width: 320px; height: 250px;" /></p>

<pre>
<strong>Input:</strong> root = [1,4,3,2,4,2,5,null,null,null,null,null,null,4,6]
<strong>Output:</strong> 20
<strong>Explanation:</strong> Maximum sum in a valid Binary search tree is obtained in root node with key equal to 3.
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/01/30/sample_2_1709.png" style="width: 134px; height: 180px;" /></p>

<pre>
<strong>Input:</strong> root = [4,3,null,1,2]
<strong>Output:</strong> 2
<strong>Explanation:</strong> Maximum sum in a valid Binary search tree is obtained in a single root node with key equal to 2.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [-4,-2,-5]
<strong>Output:</strong> 0
<strong>Explanation:</strong> All values are negatives. Return an empty BST.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root = [2,1,3]
<strong>Output:</strong> 6
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> root = [5,4,8,3,null,6,3]
<strong>Output:</strong> 7
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The&nbsp;given binary tree will have between&nbsp;<code>1</code>&nbsp;and&nbsp;<code>40000</code>&nbsp;nodes.</li>
	<li>Each node&#39;s value is between <code>[-4 * 10^4&nbsp;, 4 * 10^4]</code>.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Binary Search Tree (binary-search-tree)

## Companies
- Amazon - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Post Order Traverse with Comment - Clean code
- Author: hiepit
- Creation Date: Sun Mar 08 2020 00:02:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 17:51:40 GMT+0800 (Singapore Standard Time)

<p>
```java
class Solution {
    private int maxSum = 0;
    public int maxSumBST(TreeNode root) {
        postOrderTraverse(root);
        return maxSum;
    }
    private int[] postOrderTraverse(TreeNode root) {
        if (root == null) return new int[]{Integer.MAX_VALUE, Integer.MIN_VALUE, 0}; // {min, max, sum}, initialize min=MAX_VALUE, max=MIN_VALUE
        int[] left = postOrderTraverse(root.left);
        int[] right = postOrderTraverse(root.right);
        // The BST is the tree:
        if (!(     left != null             // the left subtree must be BST
                && right != null            // the right subtree must be BST
                && root.val > left[1]       // the root\'s key must greater than maximum keys of the left subtree
                && root.val < right[0]))    // the root\'s key must lower than minimum keys of the right subtree
            return null;
        int sum = root.val + left[2] + right[2]; // now it\'s a BST make `root` as root
        maxSum = Math.max(maxSum, sum);
        int min = Math.min(root.val, left[0]);
        int max = Math.max(root.val, right[1]);
        return new int[]{min, max, sum};
    }
}
```
**Complexity:**
- Time: `O(n)`
- Space: `O(h)`, where `h` is the height of the binary tree


**Similar problem**
[98. Validate Binary Search Tree](https://leetcode.com/problems/validate-binary-search-tree/)
</p>


### [Java] One pass post order DFS O(N)
- Author: yuhwu
- Creation Date: Tue Mar 10 2020 11:58:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 10 2020 11:58:56 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    int max;
    public int maxSumBST(TreeNode root) {
        max = 0;
        findMaxSum(root);
        return max;
    }
    
    //int[]{isBST(0/1), largest, smallest, sum}
    public int[] findMaxSum(TreeNode node){
        if(node==null){
            return new int[]{1, Integer.MIN_VALUE, Integer.MAX_VALUE, 0};
        }
        int[] left = findMaxSum(node.left);
        int[] right = findMaxSum(node.right);
        boolean isBST = left[0]==1 && right[0]==1 && node.val>left[1] && node.val<right[2];
        int sum = node.val + left[3] + right[3];
        if(isBST){
            max = Math.max(max, sum);
        }
        return new int[]{isBST?1:0, Math.max(node.val,right[1]), Math.min(node.val,left[2]), sum};
    }
}
```
</p>


### [Python] Easy traversal with explanation
- Author: yanrucheng
- Creation Date: Sun Mar 08 2020 00:00:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 00:07:19 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
For each subtree, we return 4 elements.
1. the status of this subtree, `1` means it\'s empty, `2` means it\'s a BST, `0` means it\'s not a BST
2. size of this subtree (we only care about size of BST though)
3. the minimal value in this subtree
4. the maximal value in this subtree

Then we only need to make sure for every BST
- both of its children are BST
- the right bound of its left child is smaller than `root.val`
- the left bound of its right child is larger than `root.val`

**Complexity**
Time: `O(N)`
Space: `O(logN)` for function calls, worst case `O(N)` if the given tree is not balanced

**Python 3**
```
class Solution:
    def maxSumBST(self, root: TreeNode) -> int:
        res = 0
        def traverse(root):
            \'\'\'return status_of_bst, size_of_bst, left_bound, right_bound\'\'\'
            nonlocal res
            if not root: return 1, 0, None, None # this subtree is empty
            
            ls, l, ll, lr = traverse(root.left)
            rs, r, rl, rr = traverse(root.right)
            
            if ((ls == 2 and lr < root.val) or ls == 1) and ((rs == 2 and rl > root.val) or rs == 1):
		        # this subtree is a BST
                size = root.val + l + r
                res = max(res, size)
                return 2, size, (ll if ll is not None else root.val), (rr if rr is not None else root.val)
            return 0, None, None, None # this subtree is not a BST
        
        traverse(root)
        return res
```
</p>


