---
title: "Check If Word Is Valid After Substitutions"
weight: 954
#id: "check-if-word-is-valid-after-substitutions"
---
## Description
<div class="description">
<p>Given a string <code>s</code>, determine if it is <strong>valid</strong>.</p>

<p>A string <code>s</code> is <strong>valid</strong> if, starting with an empty string <code>t = &quot;&quot;</code>, you can <strong>transform </strong><code>t</code><strong> into </strong><code>s</code> after performing the following operation <strong>any number of times</strong>:</p>

<ul>
	<li>Insert string <code>&quot;abc&quot;</code> into any position in <code>t</code>. More formally, <code>t</code> becomes <code>t<sub>left</sub> + &quot;abc&quot; + t<sub>right</sub></code>, where <code>t == t<sub>left</sub> + t<sub>right</sub></code>. Note that <code>t<sub>left</sub></code> and <code>t<sub>right</sub></code> may be <strong>empty</strong>.</li>
</ul>

<p>Return <code>true</code> <em>if </em><code>s</code><em> is a <strong>valid</strong> string, otherwise, return</em> <code>false</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aabcbc&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong>
&quot;&quot; -&gt; &quot;<u>abc</u>&quot; -&gt; &quot;a<u>abc</u>bc&quot;
Thus, &quot;aabcbc&quot; is valid.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abcabcababcc&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong>
&quot;&quot; -&gt; &quot;<u>abc</u>&quot; -&gt; &quot;abc<u>abc</u>&quot; -&gt; &quot;abcabc<u>abc</u>&quot; -&gt; &quot;abcabcab<u>abc</u>c&quot;
Thus, &quot;abcabcababcc&quot; is valid.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abccba&quot;
<strong>Output:</strong> false
<strong>Explanation:</strong> It is impossible to get &quot;abccba&quot; using the operation.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;cababc&quot;
<strong>Output:</strong> false
<strong>Explanation:</strong> It is impossible to get &quot;cababc&quot; using the operation.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 2 * 10<sup>4</sup></code></li>
	<li><code>s</code> consists of letters <code>&#39;a&#39;</code>, <code>&#39;b&#39;</code>, and <code>&#39;c&#39;</code></li>
</ul>

</div>

## Tags
- String (string)
- Stack (stack)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Nutanix - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python/C++] Stack Solution O(N)
- Author: lee215
- Creation Date: Sun Mar 03 2019 12:31:53 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 22 2019 13:30:21 GMT+0800 (Singapore Standard Time)

<p>
## **Solution 1, Brute Force**

Brute force using replace will get accepted. Though it\'s not expected.

Time complexity `O(N^2)`, space `O(N^2)` (depending on implementation).
**Python**
```
    def isValid(self, S):
        S2 = ""
        while S != S2:
            S, S2 = S.replace("abc", ""), S
        return S == ""
```
<br>

## **Solution 2**

Keep a stack, whenever meet `\'c\'`,
pop `a` and `b` at the end of stack.
Otherwise return `false`.

**Java**
```
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (char c: s.toCharArray()) {
            if (c == \'c\') {
                if (stack.isEmpty() || stack.pop() != \'b\') return false;
                if (stack.isEmpty() || stack.pop() != \'a\') return false;
            } else {
                stack.push(c);
            }
        }
        return stack.isEmpty();
    }
```

**C++**
```
    bool isValid(string S) {
        vector<char> stack;
        for (char c : S) {
            if (c == \'c\') {
                int n = stack.size();
                if (n < 2 || stack[n - 1] != \'b\' || stack[n - 2] != \'a\') return false;
                stack.pop_back(), stack.pop_back();
            } else {
                stack.push_back(c);
            }
        }
        return stack.size() == 0;
    }
```

**Python**
```
    def isValid(self, S):
        stack = []
        for i in S:
            if i == \'c\':
                if stack[-2:] != [\'a\', \'b\']:
                    return False
                stack.pop()
                stack.pop()
            else:
                stack.append(i)
        return not stack
```


<br>

## **Solution 3,  Accepted Wrong Solution...**
One wrong solution is that check that if `count[a] >= count[b] >= count[c]` always valid.
It\'s hard for Leetcode to construct all kinds of false positive test cases.
One easy counterexample can be `"aabbcc"`, expecting Leetcode to add it later.

```
    def isValid(self, S):
        count = [0, 0, 0]
        for i in S:
            count[ord(i) - ord(\'a\')] += 1
            if not count[0] >= count[1] >= count[2]:
                return False
        return count[0] == count[1] == count[2]
```
</p>


### C++ 3 lines, search and remove
- Author: votrubac
- Creation Date: Sun Mar 03 2019 12:03:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 03 2019 12:03:33 GMT+0800 (Singapore Standard Time)

<p>
We search for "abc" substring and remove it from the string. If, in the end, the string is empty - return true.
```
bool isValid(string S) {
  for (auto i = S.find("abc"); i != string::npos; i = S.find("abc"))
    S.erase(i, 3);
  return S.empty();
}
```
## Complexity Analysis
Runtime: *O(n * n)*, where n is the number of characters. ```find``` is O(n + 3) and ```erase``` is O(n), and we repeat it n / 3 times.
Memory: *O(n)*.
</p>


### Java 3 lines solution
- Author: rss106
- Creation Date: Sun Mar 03 2019 12:43:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 03 2019 12:43:17 GMT+0800 (Singapore Standard Time)

<p>
```
    	String abc = "abc";
    	
    	while(S.contains(abc)) {
    		S = S.replace(abc, "");
    	}
    	
        return S.isEmpty();


```
</p>


