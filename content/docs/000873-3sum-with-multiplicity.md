---
title: "3Sum With Multiplicity"
weight: 873
#id: "3sum-with-multiplicity"
---
## Description
<div class="description">
<p>Given an integer array <code>A</code>, and an integer <code>target</code>, return the number of&nbsp;tuples&nbsp;<code>i, j, k</code>&nbsp; such that <code>i &lt; j &lt; k</code> and&nbsp;<code>A[i] + A[j] + A[k] == target</code>.</p>

<p>As the answer can be very large, return it <strong>modulo</strong>&nbsp;<code>10<sup>9</sup> + 7</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> A = [1,1,2,2,3,3,4,4,5,5], target = 8
<strong>Output:</strong> 20
<strong>Explanation: </strong>
Enumerating by the values (A[i], A[j], A[k]):
(1, 2, 5) occurs 8 times;
(1, 3, 4) occurs 8 times;
(2, 2, 4) occurs 2 times;
(2, 3, 3) occurs 2 times.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> A = [1,1,2,2,2,2], target = 5
<strong>Output:</strong> 12
<strong>Explanation: </strong>
A[i] = 1, A[j] = A[k] = 2 occurs 12 times:
We choose one 1 from [1,1] in 2 ways,
and two 2s from [2,2,2,2] in 6 ways.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>3 &lt;= A.length &lt;= 3000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 100</code></li>
	<li><code>0 &lt;= target &lt;= 300</code></li>
</ul>

</div>

## Tags
- Two Pointers (two-pointers)

## Companies
- Quora - 24 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach Notes

The approaches described below assume some familiarity with the two pointer technique that can be used to solve the LeetCode problem "Two Sum".

In the problem, we have a sorted array `A` of unique elements, and want to know how many `i < j` with `A[i] + A[j] == target`.

The idea that does it in linear time, is that for each `i` in increasing order, the `j`'s that satisfy the equation `A[i] + A[j] == target` are decreasing.

```python
def solve(A, target):
    # Assume A already sorted
    i, j = 0, len(A) - 1
    ans = 0
    while i < j:
        if A[i] + A[j] < target:
            i += 1
        elif A[i] + A[j] > target:
            j -= 1
        else:
            ans += 1
            i += 1
            j -= 1
    return ans
```

This is not a complete explanation.  For more on this problem, please review the LeetCode problem "Two Sum".

---
#### Approach 1: Three Pointer

**Intuition and Algorithm**

Sort the array.  For each `i`, set `T = target - A[i]`, the remaining target.  We can try using a two-pointer technique to find `A[j] + A[k] == T`.  This approach is the natural continuation of trying to make the two-pointer technique we know from previous problems, work on this problem.

Because some elements are duplicated, we have to be careful.  In a typical case, the target is say, `8`, and we have a remaining array (`A[i+1:]`) of `[2,2,2,2,3,3,4,4,4,5,5,5,6,6]`.  We can analyze this situation with cases.

Whenever `A[j] + A[k] == T`, we should count the multiplicity of `A[j]` and `A[k]`.  In this example, if `A[j] == 2` and `A[k] == 6`, the multiplicities are `4` and `2`, and the total number of pairs is `4 * 2 = 8`.  We then move to the remaining window `A[j:k+1]` of `[3,3,4,4,4,5,5,5]`.

As a special case, if `A[j] == A[k]`, then our manner of counting would be incorrect.  If for example the remaining window is `[4,4,4]`, there are only 3 such pairs.  In general, when `A[j] == A[k]`, we have $$\binom{M}{2} = \frac{M*(M-1)}{2}$$ pairs `(j,k)` (with `j < k`) that satisfy `A[j] + A[k] == T`, where $$M$$ is the multiplicity of `A[j]` (in this case $$M=3$$).

For more details, please see the inline comments.

<iframe src="https://leetcode.com/playground/EWTFv5RC/shared" frameBorder="0" width="100%" height="500" name="EWTFv5RC"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.
<br />
<br />


---
#### Approach 2: Counting with Cases

**Intuition and Algorithm**

Let `count[x]` be the number of times that `x` occurs in `A`.  For every `x+y+z == target`, we can try to count the correct contribution to the answer.  There are a few cases:

* If `x`, `y`, and `z` are all different, then the contribution is `count[x] * count[y] * count[z]`.

* If `x == y != z`, the contribution is $$\binom{\text{count[x]}}{2} * \text{count[z]}$$.

* If `x != y == z`, the contribution is $$\text{count[x]} * \binom{\text{count[y]}}{2}$$.

* If `x == y == z`, the contribution is $$\binom{\text{count[x]}}{3}$$.

(*Here, $$\binom{n}{k}$$ denotes the binomial coefficient $$\frac{n!}{(n-k)!k!}$$.*)

Each case is commented in the implementations below.

<iframe src="https://leetcode.com/playground/QMn7ASUe/shared" frameBorder="0" width="100%" height="500" name="QMn7ASUe"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N + W^2)$$, where $$N$$ is the length of `A`, and $$W$$ is the maximum possible value of `A[i]`.  (Note that this solution can be adapted to be $$O(N^2)$$ even in the case that $$W$$ is very large.)

* Space Complexity:  $$O(W)$$.
<br />
<br />


---
#### Approach 3: Adapt from Three Sum

**Intuition and Algorithm**

As in *Approach 2*, let `count[x]` be the number of times that `x` occurs in `A`.  Also, let `keys` be a sorted list of unique values of `A`.  We will try to adapt a 3Sum algorithm to work on `keys`, but add the correct answer contributions.

For example, if `A = [1,1,2,2,3,3,4,4,5,5]` and `target = 8`, then `keys = [1,2,3,4,5]`.  When doing 3Sum on `keys` (with `i <= j <= k`), we will encounter some tuples that sum to the target, like `(x,y,z) = (1,2,5), (1,3,4), (2,2,4), (2,3,3)`.  We can then use `count` to calculate how many such tuples there are in each case.

This approach assumes familiarity with *3Sum*.  For more, please visit the associated LeetCode problem here [https://leetcode.com/problems/3sum](https://leetcode.com/problems/3sum).

<iframe src="https://leetcode.com/playground/vJkoVsKq/shared" frameBorder="0" width="100%" height="500" name="vJkoVsKq"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] O(N + 101 * 101)
- Author: lee215
- Creation Date: Sun Oct 14 2018 11:18:04 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 05:50:04 GMT+0800 (Singapore Standard Time)

<p>
Count the occurrence of each number.
using hashmap or array up to you.

Loop `i` on all numbers,
loop `j` on all numbers,
check if `k = target - i - j` is valid.

Add the number of this combination to result.
3 cases covers all possible combination:
1. `i == j == k` 
2. ` i == j != k`
3. `i < k && j < k`

**Time Complexity**:
`3 <= A.length <= 3000`, so N = 3000
But `0 <= A[i] <= 100`
So my solution is `O(N + 101 * 101)`

**C++:**
```
    int threeSumMulti(vector<int>& A, int target) {
        unordered_map<int, long> c;
        for (int a : A) c[a]++;
        long res = 0;
        for (auto it : c)
            for (auto it2 : c) {
                int i = it.first, j = it2.first, k = target - i - j;
                if (!c.count(k)) continue;
                if (i == j && j == k)
                    res += c[i] * (c[i] - 1) * (c[i] - 2) / 6;
                else if (i == j && j != k)
                    res += c[i] * (c[i] - 1) / 2 * c[k];
                else if (i < j && j < k)
                    res += c[i] * c[j] * c[k];
            }
        return res % int(1e9 + 7);
    }
```

**Java:**
```
    public int threeSumMulti(int[] A, int target) {
        long[] c = new long[101];
        for (int a : A) c[a]++;
        long res = 0;
        for (int i = 0; i <= 100; i++)
            for (int j = i; j <= 100; j++) {
                int k = target - i - j;
                if (k > 100 || k < 0) continue;
                if (i == j && j == k)
                    res += c[i] * (c[i] - 1) * (c[i] - 2) / 6;
                else if (i == j && j != k)
                    res += c[i] * (c[i] - 1) / 2 * c[k];
                else if (j < k)
                    res += c[i] * c[j] * c[k];
            }
        return (int)(res % (1e9 + 7));
    }
```
**Python:**
```
    def threeSumMulti(self, A, target):
        c = collections.Counter(A)
        res = 0
        for i, j in itertools.combinations_with_replacement(c, 2):
            k = target - i - j
            if i == j == k: res += c[i] * (c[i] - 1) * (c[i] - 2) / 6
            elif i == j != k: res += c[i] * (c[i] - 1) / 2 * c[k]
            elif k > i and k > j: res += c[i] * c[j] * c[k]
        return res % (10**9 + 7)
```

</p>


### 10 lines Super Super Easy Java Solution
- Author: FLAGbigoffer
- Creation Date: Sun Oct 14 2018 11:16:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 02:16:17 GMT+0800 (Singapore Standard Time)

<p>
## Think Outside of The Box! 
Intuitively, you will try to solve it based on the solution of 3Sum.
But... Build a map for counting different sums of two numbers. The rest of things are straightfoward.
```
class Solution {
    public int threeSumMulti(int[] A, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        
        int res = 0;
        int mod = 1000000007;
        for (int i = 0; i < A.length; i++) {
            res = (res + map.getOrDefault(target - A[i], 0)) % mod;
            
            for (int j = 0; j < i; j++) {
                int temp = A[i] + A[j];
                map.put(temp, map.getOrDefault(temp, 0) + 1);
            }
        }
        return res;
    }
}
```
</p>


### Knapsack O(n * target) or Straightforward O(n^2)
- Author: wangzi6147
- Creation Date: Sun Oct 14 2018 11:16:02 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 05:48:53 GMT+0800 (Singapore Standard Time)

<p>
`dp[i][j][k]` represents number of combinations using `k` numbers within `A[0] ... A[i]` with the sum of `j`.
Then `dp[n][target][3]` is the result. O(n * target)
```
class Solution {
    public int threeSumMulti(int[] A, int target) {
        int n = A.length, M = (int)1e9 + 7;
        int[][][] dp = new int[n + 1][target + 1][4];
        for (int i = 0; i <= n; i++) {
            dp[i][0][0] = 1;
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= target; j++) {
                for (int k = 1; k <= 3; k++) {
                    dp[i + 1][j][k] = dp[i][j][k];
                    dp[i + 1][j][k] %= M;
                    if (j >= A[i]) {
                        dp[i + 1][j][k] += dp[i][j - A[i]][k - 1];
                        dp[i + 1][j][k] %= M;
                    }
                }
            }
        }
        return dp[n][target][3];
    }
}
```

O(target) space:
```
class Solution {
    public int threeSumMulti(int[] A, int target) {
        int n = A.length, M = (int)1e9 + 7;
        int[][] dp = new int[target + 1][4];
        dp[0][0] = 1;
        for (int i = 0; i < n; i++) {
            for (int j = target; j >= A[i]; j--) {
                for (int k = 3; k >= 1; k--) {
                    dp[j][k] += dp[j - A[i]][k - 1];
                    dp[j][k] %= M;
                }
            }
        }
        return dp[target][3];
    }
}
```

O(n^2) Straightforward:
Guarantee `a <= b <= c`
```
class Solution {
    public int threeSumMulti(int[] A, int target) {
        Map<Integer, Long> map = new HashMap<>();
        for (int a : A) {
            map.put(a, map.getOrDefault(a, 0l) + 1);
        }
        long result = 0;
        int M = (int)1e9 + 7;
        for (int a : map.keySet()) {
            for (int b : map.keySet()) {
                if (b < a || a == b && map.get(a) == 1) {
                    continue;
                }
                int c = target - a - b;
                if (c < b || !map.containsKey(c)) {
                    continue;
                }
                if (a == b && b == c) {
                    if (map.get(a) == 2) {
                        continue;
                    }
                    result += nCk(map.get(a), 3);
                } else if (a == b) {
                    result += nCk(map.get(a), 2) * map.get(c);
                } else if (b == c) {
                    if (map.get(b) == 1) {
                        continue;
                    }
                    result += nCk(map.get(b), 2) * map.get(a);
                } else {
                    result += (map.get(a) * map.get(b) * map.get(c));
                }
                result %= M;
            }
        }
        return (int)result;
    }
    private long nCk(long n, int k) {
        if (k == 3) {
            return (n * (n - 1) * (n - 2)) / 6;
        } else {
            return n * (n - 1) / 2;
        }
    }
}
```
</p>


