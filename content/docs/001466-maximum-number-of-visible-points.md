---
title: "Maximum Number of Visible Points"
weight: 1466
#id: "maximum-number-of-visible-points"
---
## Description
<div class="description">
<p>You are given an array <code>points</code>, an integer <code>angle</code>, and your <code>location</code>, where <code>location = [pos<sub>x</sub>, pos<sub>y</sub>]</code> and <code>points[i] = [x<sub>i</sub>, y<sub>i</sub>]</code> both denote <strong>integral coordinates</strong> on the X-Y plane.</p>

<p>Initially, you are facing directly east from your position. You <strong>cannot move</strong> from your position, but you can <strong>rotate</strong>. In other words, <code>pos<sub>x</sub></code> and <code>pos<sub>y</sub></code> cannot be changed. Your field of view in <strong>degrees</strong> is represented by <code>angle</code>, determining&nbsp;how wide you can see from any given view direction. Let <code>d</code> be the amount in degrees that you rotate counterclockwise. Then, your field of view is the <strong>inclusive</strong> range of angles <code>[d - angle/2, d + angle/2]</code>.</p>

<p>
<video autoplay="" controls="" height="360" muted="" style="max-width:100%;height:auto;" width="480"><source src="https://assets.leetcode.com/uploads/2020/09/30/angle.mp4" type="video/mp4" />Your browser does not support the video tag or this video format.</video>
</p>

<p>You can <strong>see</strong> some set of points if, for each point, the <strong>angle</strong> formed by the point, your position, and the immediate east direction from your position is <strong>in your field of view</strong>.</p>

<p>There can be multiple points at one coordinate. There may be points at your location, and you can always see these points regardless of your rotation. Points do not obstruct your vision to other points.</p>

<p>Return <em>the maximum number of points you can see</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/30/89a07e9b-00ab-4967-976a-c723b2aa8656.png" style="width: 400px; height: 300px;" />
<pre>
<strong>Input:</strong> points = [[2,1],[2,2],[3,3]], angle = 90, location = [1,1]
<strong>Output:</strong> 3
<strong>Explanation:</strong> The shaded region represents your field of view. All points can be made visible in your field of view, including [3,3] even though [2,2] is in front and in the same line of sight.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> points = [[2,1],[2,2],[3,4],[1,1]], angle = 90, location = [1,1]
<strong>Output:</strong> 4
<strong>Explanation:</strong> All points can be made visible in your field of view, including the one at your location.
</pre>

<p><strong>Example 3:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/30/5010bfd3-86e6-465f-ac64-e9df941d2e49.png" style="width: 690px; height: 348px;" />
<pre>
<strong>Input:</strong> points = [[1,0],[2,1]], angle = 13, location = [1,1]
<strong>Output:</strong> 1
<strong>Explanation:</strong> You can only see one of the two points, as shown above.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= points.length &lt;= 10<sup>5</sup></code></li>
	<li><code>points[i].length == 2</code></li>
	<li><code>location.length == 2</code></li>
	<li><code>0 &lt;= angle &lt; 360</code></li>
	<li><code>0 &lt;= pos<sub>x</sub>, pos<sub>y</sub>, x<sub>i</sub>, y<sub>i</sub> &lt;= 10<sup>9</sup></code></li>
</ul>

</div>

## Tags
- Two Pointers (two-pointers)
- Geometry (geometry)

## Companies
- Nvidia - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ - Clean with Explanation
- Author: shivankacct
- Creation Date: Sun Oct 04 2020 12:02:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 05 2020 04:49:37 GMT+0800 (Singapore Standard Time)

<p>
- Find the angle between each point and your location (against the x axis)
- Duplicate entire array and append it to the end. Then increase angle by 360 for the appended portion - this is since we\'re working with a cyclic array. Once you reach the end of the array, you want to continue comparing those values with the values at the beginning of the array.
- Use a sliding window to find the max that fit within the given degree
- Special case: If the given point is the same as location (for eg. both are [1, 1]) then count that separately - since there is no way to know the angle if the points overlap

```
#define PI 3.14159265
#define MARGIN 1e-9

class Solution {
public:
    double get_angle(int x_diff, int y_diff) {
        return atan2(y_diff, x_diff) * 180 / PI;
    }
    int visiblePoints(vector<vector<int>>& points, int angle, vector<int>& location) {
        int i, j, n = points.size(), common = 0;
        vector<double> vals;
        for(i=0; i<n; i++) {
            int x = points[i][0] - location[0];
            int y = points[i][1] - location[1];
            if(x == 0 && y == 0) {
                common++;
            }
            else {
                double A = get_angle(x, y);
                if(A < 0) A += 360;
                vals.emplace_back(A);
            }
        }

        sort(vals.begin(), vals.end());
        vector<double> a = vals;
        a.insert(a.end(), vals.begin(), vals.end());
        for(i=vals.size(); i<a.size(); i++)
            a[i] += 360;
        int ret = 0;
        for(i=0, j=0; i<a.size(); i++) {
            while(j < a.size() && (a[j]-a[i]<=angle + MARGIN))
                  j++;
            ret = max(ret, j - i);
        }
        return ret + common;
    }
};
```
</p>


### [Python] clean sliding window solution with explanation
- Author: alanlzl
- Creation Date: Sun Oct 04 2020 12:13:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 12:44:38 GMT+0800 (Singapore Standard Time)

<p>
**Idea**

Here are the steps:

1) convert all coordinates to radians 
2) sort the array
3) use sliding window to find the longest window that satisfies `arr[r] - arr[l] <= angle`.

Note that we need to go around the circle, so we duplicate the array and offset the second half by 2*pi.

</br>


**Complexity**

Time complexity: `O(NlogN)`
Space complexity: `O(N)`

</br>

**Python**
```Python
class Solution:
    def visiblePoints(self, points: List[List[int]], angle: int, location: List[int]) -> int:
        
        arr, extra = [], 0
        xx, yy = location
        
        for x, y in points:
            if x == xx and y == yy:
                extra += 1
                continue
            arr.append(math.atan2(y - yy, x - xx))
        
        arr.sort()
        arr = arr + [x + 2.0 * math.pi for x in arr]
        angle = math.pi * angle / 180
        
        l = ans = 0
        for r in range(len(arr)):
            while arr[r] - arr[l] > angle:
                l += 1
            ans = max(ans, r - l + 1)
            
        return ans + extra
```
</p>


### What is the freaking d here?
- Author: LeetCodeFunker
- Creation Date: Sun Oct 04 2020 12:02:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 12:48:18 GMT+0800 (Singapore Standard Time)

<p>
Well, though leetcode gives us a short video, which is not very helpful to understand the question. I am really confused on what the the *d* here it is. Please do not forget to explain all your variables you brought in the question...

In the end, I only did a random guess that d could be any angle, but please do not let your user to guess anything next time, leetcode. [sigh]

I saw a lot of users comment like "I guess" or "feel like", see, that is the biggest problem here. And my concern was if d is free to set, then does the counterclock/clockwise matter? I am waiting for the correction from leetcode.

```
    public int visiblePoints(List<List<Integer>> points, int angle, List<Integer> location) {
        ArrayList<Double> list = new ArrayList<>();
        ArrayList<Double> listCounter = new ArrayList<>();
        int res = 0, count = 0, left = 0;
        for (List<Integer> point : points) {
            if (point.equals(location)) {
                count++;
            } else {
                list.add(getDegree(point, location));
            }
        }
        Collections.sort(list);
        for (int right = 0; right < list.size(); right++) {
            while (list.get(right) - list.get(left) > angle) {
                left++;
            }
            res = Math.max(res, right - left + 1);
        }
        for (double i : list) {
            listCounter.add((i + 180) % 360);
        }

        Collections.sort(listCounter);
        left = 0;
        for (int right = 0; right < listCounter.size(); right++) {
            while (listCounter.get(right) - listCounter.get(left) > angle) {
                left++;
            }
            res = Math.max(res, right - left + 1);
        }
        return res + count;
    }

    public double getDegree(List<Integer> point, List<Integer> location) {
        double deltaX = point.get(0) - location.get(0);
        double deltaY = -(point.get(1) - location.get(1));
        double arc = Math.atan2(deltaY, deltaX);
		arc = arc < 0 ? Math.abs(arc) : 2 * Math.PI - arc;
        return Math.toDegrees(arc);
    }
```
</p>


