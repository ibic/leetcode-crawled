---
title: "Game Play Analysis IV"
weight: 1526
#id: "game-play-analysis-iv"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Activity</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| player_id    | int     |
| device_id    | int     |
| event_date   | date    |
| games_played | int     |
+--------------+---------+
(player_id, event_date) is the primary key of this table.
This table shows the activity of players of some game.
Each row is a record of a player who logged in and played a number of games (possibly 0) before logging out on some day using some device.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query that reports the <strong>fraction</strong> of players that logged in again on the day after the day they first logged in,&nbsp;<strong>rounded to 2 decimal places</strong>.&nbsp;In other words, you need to count the number of players that logged in for at least two consecutive days starting from their first login date, then divide that number by the total number of players.</p>

<p>The query result format is in the following example:</p>

<pre>
Activity table:
+-----------+-----------+------------+--------------+
| player_id | device_id | event_date | games_played |
+-----------+-----------+------------+--------------+
| 1         | 2         | 2016-03-01 | 5            |
| 1         | 2         | 2016-03-02 | 6            |
| 2         | 3         | 2017-06-25 | 1            |
| 3         | 1         | 2016-03-02 | 0            |
| 3         | 4         | 2018-07-03 | 5            |
+-----------+-----------+------------+--------------+

Result table:
+-----------+
| fraction  |
+-----------+
| 0.33      |
+-----------+
Only the player with id 1 logged back in after the first day he had logged in so the answer is 1/3 = 0.33
</pre>

</div>

## Tags


## Companies
- Facebook - 2 (taggedByAdmin: false)
- GSN Games - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Very simple MYSQL solution
- Author: katie_hou
- Creation Date: Thu Jun 20 2019 12:51:23 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 20 2019 12:51:23 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT ROUND(COUNT(t2.player_id)/COUNT(t1.player_id),2) AS fraction
FROM
(SELECT player_id, MIN(event_date) AS first_login FROM Activity GROUP BY player_id) t1 LEFT JOIN Activity t2
ON t1.player_id = t2.player_id AND t1.first_login = t2.event_date - 1
```
</p>


### MySQL solution with STEP-BY-STEP explanation
- Author: GodFirst
- Creation Date: Tue Jul 02 2019 05:57:25 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 02 2019 05:57:25 GMT+0800 (Singapore Standard Time)

<p>
#### ***1. subquery (each playerid with his/her first login date)***
(select player_id, min(event_date) as min_date
from activity
group by player_id) as temp

#### ***2. numerator (the number of players that \'logged in for at least two consecutive days starting from their first login date\')***
sum(case
when temp.min_date + 1 = a.event_date then 1
else 0
end)

#### ***3. denominator (the total number of players)***
count(distinct temp.player_id)

#### ***4. divide***
round(numerator/denominator\uFF0C 2) ---> rounded to 2 decimal places

#### ***5. bring \'em together***
select round(sum(case when temp.min_date + 1 = a.event_date then 1 else 0 end)
														/
					   count(distinct temp.player_id), 2) as fraction
from (select player_id, min(event_date) as min_date from activity group by player_id) as temp
join activity a
on temp.player_id = a.player_id
</p>


### MySQL Without JOIN, with Explanation
- Author: samadDotDev
- Creation Date: Fri Feb 14 2020 02:07:12 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Feb 14 2020 02:07:12 GMT+0800 (Singapore Standard Time)

<p>
**Solution:**

```
SELECT ROUND(COUNT(DISTINCT player_id) / (SELECT COUNT(DISTINCT player_id) FROM Activity),2) as fraction 
FROM Activity
WHERE (player_id, DATE_SUB(event_date, INTERVAL 1 DAY)) 
IN (SELECT player_id, MIN(event_date) as first_login FROM Activity GROUP BY player_id)
```


**Explanation:**

1. Select the first login date of every player. This will be used as subquery for condition in our main query.
```
SELECT player_id, MIN(event_date) as first_login FROM Activity GROUP BY player_id
```

2. Search only for same players but with event dates one day ahead of first login date (subtract 1 day from current to match with previous date, if available)
```
SELECT ...
FROM Activity
WHERE (player_id, DATE_SUB(event_date, INTERVAL 1 DAY)) 
```

3. Count players which match WHERE condition in (2) and divide them by total number of distinct players in activity, round off to 2 decimal places.

```
SELECT ROUND(COUNT(DISTINCT player_id) / (SELECT COUNT(DISTINCT player_id) FROM Activity),2) as fraction 
```
</p>


