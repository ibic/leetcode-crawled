---
title: "Count Triplets That Can Form Two Arrays of Equal XOR"
weight: 1328
#id: "count-triplets-that-can-form-two-arrays-of-equal-xor"
---
## Description
<div class="description">
<p>Given an array of&nbsp;integers <code>arr</code>.</p>

<p>We want to select three indices <code>i</code>, <code>j</code> and <code>k</code> where <code>(0 &lt;= i &lt; j &lt;= k &lt; arr.length)</code>.</p>

<p>Let&#39;s define <code>a</code> and <code>b</code> as follows:</p>

<ul>
	<li><code>a = arr[i] ^ arr[i + 1] ^ ... ^ arr[j - 1]</code></li>
	<li><code>b = arr[j] ^ arr[j + 1] ^ ... ^ arr[k]</code></li>
</ul>

<p>Note that <strong>^</strong> denotes the <strong>bitwise-xor</strong> operation.</p>

<p>Return <em>the number of triplets</em> (<code>i</code>, <code>j</code> and <code>k</code>) Where <code>a == b</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,3,1,6,7]
<strong>Output:</strong> 4
<strong>Explanation:</strong> The triplets are (0,1,2), (0,2,2), (2,3,4) and (2,4,4)
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,1,1,1,1]
<strong>Output:</strong> 10
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,3]
<strong>Output:</strong> 0
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,3,5,7,9]
<strong>Output:</strong> 3
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> arr = [7,11,12,9,5,2,7,17,22]
<strong>Output:</strong> 8
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 300</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 10^8</code></li>
</ul>
</div>

## Tags
- Array (array)
- Math (math)
- Bit Manipulation (bit-manipulation)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] One Pass, O(N^4) to O(N)
- Author: lee215
- Creation Date: Sun May 10 2020 12:05:53 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 11 2020 15:45:44 GMT+0800 (Singapore Standard Time)

<p>
# **Solution 1: Brute Force (TLE)**
Brute force all combinations, will TLE.
Time `O(N^4)`
Space `O(1)`
<br>

# **Solution 2: Brute Force with prefix**
Calculate all prefix of bitwise-xor operation.
`prefix[0] = 0`
`prefix[i] = A[0]^A[1]^...^A[i - 1]`
So that for each `(i, j)`,
we can get `A[i]^A[i+1]^...^A[j]` by `prefix[j+1]^prefix[i]`
in O(1) time

Time `O(N^3)`
Space `O(N)`
Space `O(1)` if changing the input

<br>

# **Solution 3: Prefix XOR**
`a = arr[i] ^ arr[i + 1] ^ ... ^ arr[j - 1]`
`b = arr[j] ^ arr[j + 1] ^ ... ^ arr[k]`

Assume `a == b`, thus
`a ^ a = b ^ a`, thus
`0 = b ^ a`, thus
`arr[i] ^ arr[i + 1] ^ ... ^ arr[j - 1] ^ arr[j] ^ arr[j + 1] ^ ... ^ arr[k] = 0`
`prefix[k+1] = prefix[i]`

We only need to find out how many pair `(i, k)` of prefix value are equal.
So we can calculate the `prefix` array first,
then brute force count the pair.

Because we once we determine the pair `(i,k)`,
j can be any number that `i < j <= k`,
so we need to plus `k - i - 1` to the result `res`.

Time `O(N^2)`
Space `O(N)`
Space `O(1)` if changing the input

**Java:**
```java
    public int countTriplets(int[] A) {
        int n = A.length + 1, res = 0, prefix[] = new int[n];
        for (int i = 1; i < n; ++i)
            prefix[i] = A[i - 1] ^ prefix[i - 1];
        for (int i = 0; i < n; ++i)
            for (int j = i + 1; j < n; ++j)
                if (prefix[i] == prefix[j])
                    res += j - i - 1;
        return res;
    }
```

**C++:**
```cpp
    int countTriplets(vector<int>& A) {
        A.insert(A.begin(), 0);
        int n = A.size(), res = 0;
        for (int i = 1; i < n; ++i)
            A[i] ^= A[i - 1];
        for (int i = 0; i < n; ++i)
            for (int j = i + 1; j < n; ++j)
                if (A[i] == A[j])
                    res += j - i - 1;
        return res;
    }
```

**Python:**
```py
    def countTriplets(self, A):
        A.insert(0, 0)
        n = len(A)
        for i in xrange(n - 1):
            A[i + 1] ^= A[i]
        res = 0
        for i in xrange(n):
            for j in xrange(i + 1, n):
                if A[i] == A[j]:
                    res += j - i - 1
        return res
```
<br>

# **Solution 4: Prefix XOR, O(N)**
The problem now is, given an array,
find out the sum of index distance with `A[i] = A[j]`.
Let me know if there is a duplicate problem on LeetCode,
so I can attach a link to help explain.

Basicly, for the same value in the array,
we need to count the frequncy and the total value at the same time.

Time `O(N)`
Space `O(N)`
Space `O(1)` if changing the input

**C++**
```cpp
    int countTriplets(vector<int>& A) {
        A.insert(A.begin(), 0);
        int n = A.size(), res = 0;
        for (int i = 1; i < n; ++i)
            A[i] ^= A[i - 1];
        unordered_map<int, int> count, total;
        for (int i = 0; i < n; ++i) {
            res += count[A[i]]++ * (i - 1) - total[A[i]];
            total[A[i]] += i;
        }
        return res;
    }
```
<br>

# **Solution 5: Prefix XOR, One Pass**
Time `O(N)` for one pass
Space `O(N)`
**C++**
```cpp
    int countTriplets(vector<int>& A) {
        int n = A.size(), res = 0, prefix = 0;
        unordered_map<int, int> count = {{0, 1}}, total;
        for (int i = 0; i < n; ++i) {
            prefix ^= A[i];
            res += count[prefix]++ * i - total[prefix];
            total[prefix] += i + 1;
        }
        return res;
    }
```
**Java**
```java
    public int countTriplets(int[] A) {
        int n = A.length, res = 0, prefix = 0, c, t;
        Map<Integer, Integer> count = new HashMap<>(), total = new HashMap<>();
        count.put(0, 1);
        for (int i = 0; i < n; ++i) {
            prefix ^= A[i];
            c = count.getOrDefault(prefix, 0);
            t = total.getOrDefault(prefix, 0);
            res += c * i - t;
            count.put(prefix, c + 1);
            total.put(prefix, t + i + 1);
        }
        return res;
    }
```
**Python**
```py
    def countTriplets(self, A):
        res = cur = 0
        count = {0: [1, 0]}
        for i, a in enumerate(A):
            cur ^= a
            n, total = count.get(cur, [0, 0])
            res += i * n - total
            count[cur] = [n + 1, total + i + 1]
        return res
```
</p>


### [Java] Use the Trick of XOR operation!!!
- Author: Admin007
- Creation Date: Sun May 10 2020 12:06:56 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 11 2020 06:49:33 GMT+0800 (Singapore Standard Time)

<p>
Give the observation: For all pairs of `i` and `k`, where `arr[i] ^ arr[i + 1] ^ ... ^ arr[k] = 0`, then any `j (i < j <= k)` will be good to set as the answer (hint: proof by contradiction, if you cannot understand this trick immediately). So you just need to find all segments where XOR equals zero. 
```
    public int countTriplets(int[] arr) {
        int ans = 0;
        int length = arr.length;
        for (int i = 0; i < length; i++) {
            int xor = arr[i];
            for (int j = i + 1; j < length; j++) {
                xor ^= arr[j];
                if (xor == 0) {
                    ans += (j - i);
                }
            }
        }
        return ans;
    }
```

Time complexity: `O(N^2)`
Space complexity: `O(1)`
</p>


### Clean Python 3, accumulative xor result, O(N^2)/O(1) time/space
- Author: lenchen1112
- Creation Date: Sun May 10 2020 12:04:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 10 2020 12:04:07 GMT+0800 (Singapore Standard Time)

<p>
Choose positions of `i` and `k` first.
If the accumulative xor is equal to 0, then count the positions that we can insert `j`.
Time: `O(N^2)`
Space: `O(1)`
```
class Solution:
    def countTriplets(self, arr: List[int]) -> int:
        count, n = 0, len(arr)
        for i in range(n - 1):
            accu = arr[i]
            for k in range(i + 1, n):
                accu ^= arr[k]
                if accu == 0: count += k - i
        return count
```
</p>


