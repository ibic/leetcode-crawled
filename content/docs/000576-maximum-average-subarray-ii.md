---
title: "Maximum Average Subarray II"
weight: 576
#id: "maximum-average-subarray-ii"
---
## Description
<div class="description">
<p>
Given an array consisting of <code>n</code> integers, find the contiguous subarray whose <b>length is greater than or equal to</b> <code>k</code> that has the maximum average value. And you need to output the maximum average value.
</p>


<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [1,12,-5,-6,50,3], k = 4
<b>Output:</b> 12.75
<b>Explanation:</b>
when length is 5, maximum average value is 10.8,
when length is 6, maximum average value is 9.16667.
Thus return 12.75.
</pre>
</p>


<p><b>Note:</b><br>
<ol>
<li>1 <= <code>k</code> <= <code>n</code> <= 10,000.</li>
<li>Elements of the given array will be in range [-10,000, 10,000].</li>
<li>The answer with the calculation error less than 10<sup>-5</sup> will be accepted.</li>
</ol>
</p>
</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


## Solution

---
#### Approach #1 Iterative method [Time Limit Exceeded]

One of the simplest solutions is to consider the sum of every possible subarray with length greater than or equal to $$k$$ and to determine the maximum average from out of those. But, instead of finding out this sum in a naive manner for every subarray with length greater than or equal to $$k$$ separately, we can do as follows. 

For every starting point, $$s$$, considered, we can iterate over the elements of $$nums$$ starting from $$nums$$, and keep a track of the $$sum$$ found till the current index($$i$$). Whenever the index reached is such that the number of elements lying between $$s$$ and $$i$$ is greater than or equal to $$k$$, we can check if the average of the elements between $$s$$ and $$i$$ is greater than the average found till now or not.

<iframe src="https://leetcode.com/playground/uX3gpV7T/shared" frameBorder="0" name="uX3gpV7T" width="100%" height="309"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Two for loops iterating over the whole length of $$nums$$ with $$n$$ elements.

* Space complexity : $$O(1)$$. Constant extra space is used.

---
#### Approach #2 Using Binary Search [Accepted]

**Algorithm**

To understand the idea behind this method, let's look at the following points. 

Firstly, we know that the value of the average could lie between the range $$(min, max)$$. Here, $$min$$ and $$max$$ refer to the minimum and the maximum values out of the given $$nums$$ array. This is because, the average can't be lesser than the minimum value and can't be larger than the maximum value. 

But, in this case, we need to find the maximum average of a subarray with atleast $$k$$ elements. The idea in this method is to try to approximate(guess) the solution and to try to find if this solution really exists. 


If it exists, we can continue trying to approximate the solution even to a further precise value, but choosing a larger number as the next approximation. But, if the initial guess is wrong, and the initial maximum average value(guessed) isn't possible, we need to try with a smaller number as the next approximate.

Now, instead of doing the guesses randomly, we can make use of Binary Search. With $$min$$ and $$max$$ as the initial numbers to begin with, we can find out the $$mid$$ of these two numbers given by $$(min+max)/2$$. Now, we need to find if a subarray with length greater than or equal to $$k$$ is possible with an average sum greater than this $$mid$$ value.

To determine if this is possible in a single scan, let's look at an observation. Suppose, there exist $$j$$ elements, $$a_1, a_2, a_3..., a_j$$ in a subarray within $$nums$$ such that their average is greater than $$mid$$. In this case, we can say that 

$$(a_1+a_2+ a_3...+a_j)/j &geq; mid$$ or

$$(a_1+a_2+ a_3...+a_j) &geq; j*mid$$ or

$$(a_1-mid) +(a_2-mid)+ (a_3-mid) ...+(a_j-mid) &geq; 0$$

Thus, we can see that if after subtracting the $$mid$$ number from the elements of a subarray with more than $$k-1$$ elements, within $$nums$$, if the sum of elements of this reduced subarray is greater than 0, we can achieve an average value greater than $$mid$$. Thus, in this case, we need to set the $$mid$$ as the new minimum element and continue the process. 

Otherwise, if this reduced sum is lesser than 0 for all subarrays with greater than or equal to $$k$$ elements, we can't achieve $$mid$$ as the average. Thus, we need to set $$mid$$ as the new maximum element and continue the process.

In order to determine if such a subarray exists in a linear manner, we keep on adding $$nums[i]-mid$$ to the $$sum$$ obtained till the $$i^{th}$$ element while traversing over the $$nums$$ array. If on traversing the first $$k$$ elements, the $$sum$$ becomes greater than or equal to 0, we can directly determine that we can increase the average beyond $$mid$$. Otherwise, we continue making additions to $$sum$$ for elements beyond the $$k^{th}$$ element, making use of the following idea.

If we know the cumulative sum upto indices $$i$$ and $$j$$, say $$sum_i$$ and $$sum_j$$ respectively, we can determine the sum of the subarray between these indices(including $$j$$) as $$sum_j - sum_i$$. In our case, we want this difference between the cumulative sums to be greater than or equal to 0 as discusssed above. 

Further, for $$sum_i$$ as the cumulative sum upto the current($$i^{th}$$) index, all we need is $$sum_j - sum_i &geq; 0$$ such that $$j - i &geq; k$$. 

To achive this, instead of checking with all possible values of $$sum_i$$, we can just consider the minimum cumulative sum upto the index $$j - k$$. This is because if the required condition can't be sastisfied with the minimum $$sum_i$$, it can never be satisfied with a larger value.

To fulfil this, we make use of a $$prev$$ variable which again stores the cumulative sums but, its current index(for cumulative sum) lies behind the current index for $$sum$$ at an offset of $$k$$ units. Thus, by finding the minimum out of $$prev$$ and the last minimum value, we can easily find out the required minimum sum value.

Every time after checking the possiblility with a new $$mid$$ value, at the end, we need to settle at some value as the average. But, we can observe that eventually, we'll reach a point, where we'll keep moving near some same value with very small changes. In order to keep our precision in control, we limit this process to $$10^-5$$ precision, by making use of $$error$$ and continuing the process till $$error$$ becomes lesser than 0.00001 .



<iframe src="https://leetcode.com/playground/9co4Jxkv/shared" frameBorder="0" name="9co4Jxkv" width="100%" height="515"></iframe>

**Complexity Analysis**

Let `N` be the number of element in the array, and `range` be the difference between the maximal and minimal values in the array, _i.e._ `range = max_val - min_val`, and finally the `error` be the precision required in the problem.

* Time complexity : $$O\big(N \cdot \log_2{\frac{(\text{max\_val} - \text{min\_val})}{0.00001}} \big)$$.

    - The algorithm consists of a binary search loop in the function of `findMaxAverage()`.

    - At each iteration of the loop, the `check()` function dominates the time complexity, which is of $$O(N)$$ for each invocation.
    
    - It now boils down to how many iterations the loop would run eventually. To calculate the number of iterations, let us break it down in the following steps.

    - After the first iteration, the $$\text{error}$$ would be $$\frac{\text{range}}{2}$$, as one can see. Further on, at each iteration, the $$\text{error}$$ would be reduced into half. For example, after the second iteration, we would have the $$\text{error}$$ as $$\frac{\text{range}}{2}\cdot \frac{1}{2}$$.

    - As a result, after $$K$$ iterations, the error would become $$\text{error} = \text{range}\cdot 2^{-K}$$. Given the condition of the loop, _i.e._ $$\text{error} < 0.00001 $$, we can deduct that $$K > \log_2{\frac{\text{range}}{0.00001}} = \log_2{\frac{(\text{max\_val} - \text{min\_val})}{0.00001}} $$
    
    - To sum up, the time complexity of the algorithm would be $$O\big( N \cdot K \big) = O\big(N \cdot \log_2{\frac{(\text{max\_val} - \text{min\_val})}{0.00001}} \big)$$.

* Space complexity : $$O(1)$$. Constant Space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution O(nlogM) Binary search the answer
- Author: KakaHiguain
- Creation Date: Sun Jul 16 2017 16:33:04 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 07:01:19 GMT+0800 (Singapore Standard Time)

<p>
(nums[i]+nums[i+1]+...+nums[j])/(j-i+1)>x
=>nums[i]+nums[i+1]+...+nums[j]>x*(j-i+1)
=>(nums[i]-x)+(nums[i+1]-x)+...+(nums[j]-x)>0

```
public class Solution {
    boolean check(int[] nums,int k,double x) //Check whether we can find a subarray whose average is bigger than x
    {
        int n=nums.length;
        double[] a=new double[n];
        for (int i=0;i<n;i++) a[i]=nums[i]-x; //Transfer to a[i], find whether there is a subarray whose sum is bigger than 0
        double now=0,last=0;
        for (int i=0;i<k;i++) now+=a[i];
        if (now>=0) return true;
        for (int i=k;i<n;i++)
        {
            now+=a[i];
            last+=a[i-k];
            if (last<0) 
            {
                now-=last;
                last=0;
            }
            if (now>=0) return true;
        }
        return false;
    }
    public double findMaxAverage(int[] nums, int k) {
        double l=Integer.MIN_VALUE,r=Integer.MAX_VALUE;
        while (r-l>0.000004) //Binary search the answer
        {
            double mid=(l+r)/2;
            if (check(nums,k,mid)) l=mid; else r=mid;
        }
        return r;
    }
}
```
</p>


### Python, Advanced O(n) solution (Convex Hull Window)
- Author: awice
- Creation Date: Sun Jul 16 2017 20:16:25 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 21:19:53 GMT+0800 (Singapore Standard Time)

<p>
Let ```d(x, y)``` be the density of segment ```[x, y]```, ie. ```d(x, y) = (A[x]+...+A[y]) / (y-x+1)```.  It can be computed quickly with prefix sums.

Now we refer to section 3 of [*Kai-min Chung, Hsueh-I Lu - An Optimal Algorithm for the Maximum-Density Segment Problem. 2008.*](https://arxiv.org/pdf/cs/0311020.pdf)

For each ending index ```j```, the current interval for ```i``` under consideration, ```[0, j-K+1]``` (minus parts on the left we have already discarded), has been decomposed into *minimum* density segments of longest length ```[hull[i], hull[i+1]-1]```, and we discard these segments as appropriate.  That is, for each ```i``` in increasing order, ```hull[i+1]``` is the largest index in ```[hull[i], j-K+1]``` so that ```[hull[i], hull[i+1]-1]``` has minimum density.

This is simply a lower hull of candidate points ```i```, in a geometric interpretation where ```d(a, b)``` is the slope of the line segment ```(a, P[a]) to (b+1, P[b+1])```.  Then, we can prove that discarding components with lower density than our current candidate ```d(hull[0], j)``` must leave us with the highest density option remaining.

```
def findMaxAverage(self, A, K):
    N = len(A)
    P = [0]
    for x in A:
        P.append(P[-1] + x)

    def d(x, y):
        return (P[y+1] - P[x]) / float(y+1-x)

    hull = collections.deque()
    ans = float('-inf')

    for j in xrange(K-1, N):
        while len(hull) >= 2 and d(hull[-2], hull[-1]-1) >= d(hull[-2], j-K):
            hull.pop()
        hull.append(j-K + 1)
        while len(hull) >= 2 and d(hull[0], hull[1]-1) <= d(hull[0], j):
            hull.popleft()
        ans = max(ans, d(hull[0], j))

    return ans
```
</p>


### [C++] Clean binary search solution with explanation
- Author: caihao0727mail
- Creation Date: Mon Jul 17 2017 10:43:36 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 01:05:46 GMT+0800 (Singapore Standard Time)

<p>
The idea is using the binary search to find the maximum average value. We know that the maximum average value must be between the minimal value (```left``` in the code ) and maximum value (```right``` in the code ) in ```nums```. Each time we can check if ```mid = (left+right)/2``` is larger or less than the the maximum average value:
    
we use ```max_ave``` to denote the maximum average value. Then,  for any ```i, j (j-i>=k-1)```,  we can have ```(nums[i] - max_ave) + (nums[i+1] - max_ave)+...+  (nums[j] - max_ave) <=0```. Therefore, for some ```i, j (j-i>=k-1)```, if we find ```(nums[i] - mid) + (nums[i+1] - mid)+...+  (nums[j] - mid) >0```, then ```mid```  must be smaller than ```max_ave```. The code is as follows:

```
    double findMaxAverage(vector<int>& nums, int k) {
        double left = INT_MAX, right = INT_MIN, mid;
        for(int num:nums){
            right = max(right, double(num));
            left = min(left, double(num));
        }
        while(left + 0.00001 < right){
            mid = left + (right - left)/2;
            if(islarger(nums, mid, k))right = mid;
            else left = mid;
        }
        return left;
    }
    
    //Return true when mid is larger than or equal to the maximum average value;
    //Return false when mid is smaller than the maximum average value.
    bool islarger(vector<int>& nums, double mid, int k){
        // sum: the sum from nums[0] to nums[i];
        // prev_sum:  the sum from nums[0] to nums[i-k];
        // min_sum: the minimal sum from nums[0] to nums[j] ( 0=< j  <= i-k );
        double sum = 0, prev_sum = 0, min_sum = 0;
        for(int i = 0; i < nums.size(); i++){
            sum += nums[i] - mid;
            if(i >= k){
                prev_sum += nums[i-k] - mid;                        
                min_sum = min(prev_sum, min_sum); 
            }
            if(i >= k-1 && sum > min_sum)return false;
        }
        return true;                                               
    }
```
</p>


