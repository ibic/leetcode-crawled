---
title: "Immediate Food Delivery II"
weight: 1543
#id: "immediate-food-delivery-ii"
---
## Description
<div class="description">
<p>Table: <code>Delivery</code></p>

<pre>
+-----------------------------+---------+
| Column Name                 | Type    |
+-----------------------------+---------+
| delivery_id                 | int     |
| customer_id                 | int     |
| order_date                  | date    |
| customer_pref_delivery_date | date    |
+-----------------------------+---------+
delivery_id is the primary key of this table.
The table holds information about food delivery to customers that make orders at some date and specify a preferred delivery date (on the same order date or after it).
</pre>

<p>&nbsp;</p>

<p>If the preferred delivery date of the customer is the same as the&nbsp;order date&nbsp;then the order is called <em>immediate</em>&nbsp;otherwise it&#39;s called <em>scheduled</em>.</p>

<p>The&nbsp;<em>first order</em>&nbsp;of a customer is the order with the earliest order date that customer made. It is guaranteed that a customer has exactly one first order.</p>

<p>Write an SQL query to find the&nbsp;percentage of immediate orders in the first orders of all customers,&nbsp;<strong>rounded to 2 decimal places</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
Delivery table:
+-------------+-------------+------------+-----------------------------+
| delivery_id | customer_id | order_date | customer_pref_delivery_date |
+-------------+-------------+------------+-----------------------------+
| 1           | 1           | 2019-08-01 | 2019-08-02                  |
| 2           | 2           | 2019-08-02 | 2019-08-02                  |
| 3           | 1           | 2019-08-11 | 2019-08-12                  |
| 4           | 3           | 2019-08-24 | 2019-08-24                  |
| 5           | 3           | 2019-08-21 | 2019-08-22                  |
| 6           | 2           | 2019-08-11 | 2019-08-13                  |
| 7           | 4           | 2019-08-09 | 2019-08-09                  |
+-------------+-------------+------------+-----------------------------+

Result table:
+----------------------+
| immediate_percentage |
+----------------------+
| 50.00                |
+----------------------+
The customer id 1 has a first order with delivery id 1 and it is scheduled.
The customer id 2 has a first order with delivery id 2 and it is immediate.
The customer id 3 has a first order with delivery id 5 and it is scheduled.
The customer id 4 has a first order with delivery id 7 and it is immediate.
Hence, half the customers have immediate first orders.
</pre>

</div>

## Tags


## Companies
- DoorDash - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL solution
- Author: mpractice
- Creation Date: Fri Aug 30 2019 08:07:26 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 30 2019 08:07:26 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT
    ROUND(100*SUM(CASE WHEN order_date = customer_pref_delivery_date THEN 1
    ELSE 0 END)/ COUNT(distinct customer_id) ,2) AS immediate_percentage
FROM
    Delivery
WHERE
    (customer_id, order_date)
IN
(SELECT
    customer_id, min(order_date) as min_date
FROM
    Delivery
GROUP BY
    customer_id
);
```
</p>


### Simple MySQL Solution
- Author: adityajhanwar
- Creation Date: Sun Sep 29 2019 01:30:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 29 2019 01:30:25 GMT+0800 (Singapore Standard Time)

<p>
```
select 
	round(avg(case when order_date = customer_pref_delivery_date then 1 else 0 end)*100, 2) \'immediate_percentage\'
from 
	Delivery
where 
	(customer_id, order_date) in (select customer_id, min(order_date) from Delivery group by customer_id);
```
</p>


### MySQL solution: Easy to understand
- Author: jh4016
- Creation Date: Mon Sep 16 2019 08:29:39 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 16 2019 08:29:39 GMT+0800 (Singapore Standard Time)

<p>
```
select round(count(*)/(select count(distinct customer_id) from Delivery) * 100,2)
as immediate_percentage
from 
(select customer_id
from Delivery
group by customer_id
having min(order_date) = min(customer_pref_delivery_date)) as temp
```
</p>


