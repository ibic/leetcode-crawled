---
title: "Four Divisors"
weight: 1148
#id: "four-divisors"
---
## Description
<div class="description">
<p>Given an integer array <code>nums</code>, return the sum of divisors of the integers in that array that have exactly four divisors.</p>

<p>If there is no such integer in the array, return <code>0</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [21,4,7]
<strong>Output:</strong> 32
<b>Explanation:</b>
21 has 4 divisors: 1, 3, 7, 21
4 has 3 divisors: 1, 2, 4
7 has 2 divisors: 1, 7
The answer is the sum of divisors of 21 only.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10^4</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Capital One - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java Easy Check
- Author: votrubac
- Creation Date: Sun Mar 22 2020 12:08:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2020 13:43:10 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
Any number `n` greater than `1` already have two divisors: `1` and `n`. So, for a number to have exact 4 divisors, there should be only one pair of divisors `d` and `n / d`.

> Catch: if `d == n / d`, the number has 3 divisors, not four.

**C++**
```cpp
int sumFourDivisors(vector<int>& nums) {
    auto sum = 0;
    for (auto n : nums) {
        auto last_d = 0;
        for (auto d = 2; d * d <= n; ++d) {
            if (n % d == 0) {
                if (last_d == 0)
                    last_d = d;
                else {
                    last_d = 0;
                    break;
                }
            }
        }
        if (last_d > 0 && last_d != n / last_d) {
            sum += 1 + n + last_d + n / last_d;
        }            
    }
    return sum;
}
```
**Java**
```java
public int sumFourDivisors(int[] nums) {
    int sum = 0;
    for (int n : nums) {
        int last_d = 0;
        for (int d = 2; d * d <= n; ++d) {
            if (n % d == 0) {
                if (last_d == 0)
                    last_d = d;
                else {
                    last_d = 0;
                    break;
                }
            }
        }
        if (last_d > 0 && last_d != n / last_d) {
            sum += 1 + n + last_d + n / last_d;
        }            
    }
    return sum;        
}
```
</p>


### [Python3] Short Easy Solution
- Author: localhostghost
- Creation Date: Sun Mar 22 2020 12:20:48 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 24 2020 21:40:53 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def sumFourDivisors(self, nums: List[int]) -> int:
        res = 0
        for num in nums:
            divisor = set() 
            for i in range(1, floor(sqrt(num)) + 1):
                if num % i == 0:
                    divisor.add(num//i)
                    divisor.add(i)
                if len(divisor) > 4:    
                    break
                    
            if len(divisor) == 4:
                res += sum(divisor)
        return res  
```
</p>


### [C++] O(n*sqrt(n)) Solution | Simple divisor count
- Author: uds5501
- Creation Date: Thu Jul 09 2020 16:20:19 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 09 2020 16:20:19 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    int sumFourDivisors(vector<int>& nums) {
        int ans = 0, sum, cnt;
        for(auto &num: nums) {
            sum = 0;
            cnt = 0;
            for(int j=1; j*j<=num; j++) {
                if (num%j==0) {
                    if (num/j == j) {
                        sum += j;
                        cnt++;
                    } else {
                        sum += j + (num/j);
                        cnt += 2;
                    }
                }
            }
            if (cnt == 4) ans += sum;
        }
        return ans;
    }
};
```
</p>


