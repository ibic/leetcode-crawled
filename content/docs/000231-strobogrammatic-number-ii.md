---
title: "Strobogrammatic Number II"
weight: 231
#id: "strobogrammatic-number-ii"
---
## Description
<div class="description">
<p>A strobogrammatic number is a number that looks the same when rotated 180 degrees (looked at upside down).</p>

<p>Find all strobogrammatic numbers that are of length = n.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b>  n = 2
<b>Output:</b> <code>[&quot;11&quot;,&quot;69&quot;,&quot;88&quot;,&quot;96&quot;]</code>
</pre>

</div>

## Tags
- Math (math)
- Recursion (recursion)

## Companies
- Facebook - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Cisco - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### AC clean Java solution
- Author: jeantimex
- Creation Date: Thu Aug 06 2015 05:07:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 12:43:37 GMT+0800 (Singapore Standard Time)

<p>
    public List<String> findStrobogrammatic(int n) {
        return helper(n, n);
    }
    
    List<String> helper(int n, int m) {
        if (n == 0) return new ArrayList<String>(Arrays.asList(""));
        if (n == 1) return new ArrayList<String>(Arrays.asList("0", "1", "8"));
        
        List<String> list = helper(n - 2, m);
        
        List<String> res = new ArrayList<String>();
        
        for (int i = 0; i < list.size(); i++) {
            String s = list.get(i);
            
            if (n != m) res.add("0" + s + "0");
            
            res.add("1" + s + "1");
            res.add("6" + s + "9");
            res.add("8" + s + "8");
            res.add("9" + s + "6");
        }
        
        return res;
    }
</p>


### Python recursive solution, need some observation, so far 97%
- Author: orbuluh
- Creation Date: Sun Sep 20 2015 14:41:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 04:39:38 GMT+0800 (Singapore Standard Time)

<p>
Some observation to the sequence:

n == 1:   [0, 1, 8]

n == 2:   [11, 88, 69, 96]

How about n == `3`? 
=> it can be retrieved if you insert `[0, 1, 8]` to the middle of solution of n == `2`

n == `4`?
=> it can be retrieved if you insert `[11, 88, 69, 96, 00]` to the middle of solution of n == `2`

n == `5`?
=> it can be retrieved if you insert `[0, 1, 8]` to the middle of solution of n == `4`

the same, for n == `6`, it can be retrieved if you insert `[11, 88, 69, 96, 00]` to the middle of solution of n == `4`

        
        
    
    def findStrobogrammatic(self, n):
        evenMidCandidate = ["11","69","88","96", "00"]
        oddMidCandidate = ["0", "1", "8"]
        if n == 1:
            return oddMidCandidate
        if n == 2:
            return evenMidCandidate[:-1]
        if n % 2:
            pre, midCandidate = self.findStrobogrammatic(n-1), oddMidCandidate
        else: 
            pre, midCandidate = self.findStrobogrammatic(n-2), evenMidCandidate
        premid = (n-1)/2
        return [p[:premid] + c + p[premid:] for c in midCandidate for p in pre]
</p>


### Simple Java solution without recursion
- Author: lilixi
- Creation Date: Sat Nov 07 2015 09:27:21 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 14:28:43 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public List<String> findStrobogrammatic(int n) {
            List<String> one = Arrays.asList("0", "1", "8"), two = Arrays.asList(""), r = two;
            if(n%2 == 1)
                r = one;
            for(int i=(n%2)+2; i<=n; i+=2){
                List<String> newList = new ArrayList<>();
                for(String str : r){
                    if(i != n)
                        newList.add("0" + str + "0");
                    newList.add("1" + str + "1");
                    newList.add("6" + str + "9");
                    newList.add("8" + str + "8");
                    newList.add("9" + str + "6");
                }
                r = newList;
            }
            return r;   
        }
    
    }
</p>


