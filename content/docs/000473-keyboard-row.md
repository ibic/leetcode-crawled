---
title: "Keyboard Row"
weight: 473
#id: "keyboard-row"
---
## Description
<div class="description">
<p>Given a List of words, return the words that can be typed using letters of <b>alphabet</b> on only one row&#39;s of American keyboard like the image below.</p>

<p>&nbsp;</p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2018/10/12/keyboard.png" style="width: 100%; max-width: 600px" /></p>
&nbsp;

<p><b>Example:</b></p>

<pre>
<b>Input:</b> [&quot;Hello&quot;, &quot;Alaska&quot;, &quot;Dad&quot;, &quot;Peace&quot;]
<b>Output:</b> [&quot;Alaska&quot;, &quot;Dad&quot;]
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>You may use one character in the keyboard more than once.</li>
	<li>You may assume the input string will only contain letters of alphabet.</li>
</ol>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Mathworks - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy Understand Solution
- Author: lee215
- Creation Date: Sun Feb 05 2017 01:07:49 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 09 2019 15:10:45 GMT+0800 (Singapore Standard Time)

<p>
````
if you are python user {
 please upvote if it makes sense;
} else if you are C++/Java user {
 please let me know if somewhere is not clear;
}
````
I have used ```set``` to check the word.
I firstly make every line a set of letter. 
Then I check every word if this word set is the subset if any line set.

````
def findWords(self, words):
    line1, line2, line3 = set(\'qwertyuiop\'), set(\'asdfghjkl\'), set(\'zxcvbnm\')
    ret = []
    for word in words:
      w = set(word.lower())
      if w <= line1 or w <= line2 or w <= line3:
        ret.append(word)
    return ret
````
</p>


### Java 1-Line Solution via Regex and Stream
- Author: lixx2100
- Creation Date: Sun Feb 05 2017 01:05:13 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 02:14:19 GMT+0800 (Singapore Standard Time)

<p>
```Java
public String[] findWords(String[] words) {
    return Stream.of(words).filter(s -> s.toLowerCase().matches("[qwertyuiop]*|[asdfghjkl]*|[zxcvbnm]*")).toArray(String[]::new);
}
```
</p>


### Short Easy Java with Explanation
- Author: chidong
- Creation Date: Sun Feb 05 2017 01:37:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 01 2018 01:51:31 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public String[] findWords(String[] words) {
        String[] strs = {"QWERTYUIOP","ASDFGHJKL","ZXCVBNM"};
        Map<Character, Integer> map = new HashMap<>();
        for(int i = 0; i<strs.length; i++){
            for(char c: strs[i].toCharArray()){
                map.put(c, i);//put <char, rowIndex> pair into the map
            }
        }
        List<String> res = new LinkedList<>();
        for(String w: words){
            if(w.equals("")) continue;
            int index = map.get(w.toUpperCase().charAt(0));
            for(char c: w.toUpperCase().toCharArray()){
                if(map.get(c)!=index){
                    index = -1; //don't need a boolean flag. 
                    break;
                }
            }
            if(index!=-1) res.add(w);//if index != -1, this is a valid string
        }
        return res.toArray(new String[0]);
    }
}
```
</p>


