---
title: "Repeated String Match"
weight: 618
#id: "repeated-string-match"
---
## Description
<div class="description">
<p>Given two strings&nbsp;<code>a</code> and <code>b</code>, return the minimum number of times you should repeat string&nbsp;<code>a</code>&nbsp;so that string&nbsp;<code>b</code>&nbsp;is a substring of it. If it is&nbsp;impossible for&nbsp;<code>b</code>​​​​​​ to be a substring of&nbsp;<code>a</code> after repeating it, return&nbsp;<code>-1</code>.</p>

<p><strong>Notice:</strong>&nbsp;string&nbsp;<code>&quot;abc&quot;</code>&nbsp;repeated 0 times is&nbsp;<code>&quot;&quot;</code>,&nbsp; repeated 1 time is&nbsp;<code>&quot;abc&quot;</code>&nbsp;and repeated 2 times is&nbsp;<code>&quot;abcabc&quot;</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> a = &quot;abcd&quot;, b = &quot;cdabcdab&quot;
<strong>Output:</strong> 3
<strong>Explanation:</strong> We return 3 because by repeating a three times &quot;ab<strong>cdabcdab</strong>cd&quot;, b is a substring of it.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> a = &quot;a&quot;, b = &quot;aa&quot;
<strong>Output:</strong> 2
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> a = &quot;a&quot;, b = &quot;a&quot;
<strong>Output:</strong> 1
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> a = &quot;abc&quot;, b = &quot;wxyz&quot;
<strong>Output:</strong> -1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= a.length &lt;= 10<sup>4</sup></code></li>
	<li><code>1 &lt;= b.length &lt;= 10<sup>4</sup></code></li>
	<li><code>a</code>&nbsp;and&nbsp;<code>b</code>&nbsp;consist of lower-case English letters.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Google - 2 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Ad-Hoc [Accepted]

**Intuition**

The question can be summarized as "What is the smallest `k` for which `B` is a substring of `A * k`?"  We can just try every `k`.

**Algorithm**

Imagine we wrote `S = A+A+A+...`.  If `B` is to be a substring of `S`, we only need to check whether some `S[0:], S[1:], ..., S[len(A) - 1:]` starts with `B`, as `S` is long enough to contain `B`, and `S` has period at most `len(A)`.

Now, suppose `q` is the least number for which `len(B) <= len(A * q)`.  We only need to check whether `B` is a substring of `A * q` or `A * (q+1)`.  If we try `k < q`, then `B` has larger length than `A * q` and therefore can't be a substring.  When `k = q+1`, `A * k` is already big enough to try all positions for `B`; namely, `A[i:i+len(B)] == B` for `i = 0, 1, ..., len(A) - 1`.

<iframe src="https://leetcode.com/playground/gTtmgvev/shared" frameBorder="0" name="gTtmgvev" width="100%" height="224"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N*(N+M))$$, where $$M, N$$ are the lengths of strings `A, B`.  We create two strings `A * q`, `A * (q+1)` which have length at most `O(M+N)`.  When checking whether `B` is a substring of `A`, this check takes naively the product of their lengths.

* Space complexity: As justified above, we created strings that used $$O(M+N)$$ space.

---

#### Approach #2: Rabin-Karp (Rolling Hash) [Accepted]

**Intuition**

As in *Approach #1*, we've reduced the problem to deciding whether B is a substring of some `A * k`.  Using the following technique, we can decide whether `B` is a substring in $$O(len(A) * k)$$ time.

**Algorithm**

For strings $$S$$, consider each $$S[i]$$ as some integer ASCII code.  Then for some prime $$p$$, consider the following function modulo some prime modulus $$\mathcal{M}$$:

$$\text{hash}(S) = \sum_{0 \leq i < len(S)} p^i * S[i]$$

Notably, $$\text{hash}(S[1:] + x) = \frac{(\text{hash}(S) - S[0])}{p} + p^{n-1} x$$.  This shows we can get the hash of every substring of `A * q` in time complexity linear to it's size.  (We will also use the fact that $$p^{-1} = p^{\mathcal{M}-2} \mod \mathcal{M}$$.)

However, hashes may collide haphazardly.  To be absolutely sure in theory, we should check the answer in the usual way.  The expected number of checks we make is in the order of $$1 + \frac{s}{\mathcal{M}}$$ where $$s$$ is the number of substrings we computed hashes for (assuming the hashes are equally distributed), which is effectively 1.

<iframe src="https://leetcode.com/playground/DKSFgXSr/shared" frameBorder="0" name="DKSFgXSr" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(M+N)$$ (at these sizes), where $$M, N$$ are the lengths of strings `A, B`.  As in *Approach #1*, we justify that `A * (q+1)` will be of length $$O(M + N)$$, and computing the rolling hashes was linear work.  We will also do a linear $$O(N)$$ final check of our answer $$1 + O(M) / \mathcal{M}$$ times.  In total, this is $$O(M+N + N(1 + \frac{M}{\mathcal{M}}))$$ work.  Since $$M \leq 10000 < \mathcal{M} = 10^9 + 7$$, we can consider this to be linear behavior.

* Space complexity:  $$O(1)$$.  Only integers were stored with additional memory.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Intuitive Python 2-liner
- Author: yangshun
- Creation Date: Sun Oct 01 2017 12:43:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 21:25:48 GMT+0800 (Singapore Standard Time)

<p>
Let `n` be the answer, the minimum number of times `A` has to be repeated. 

For `B` to be inside `A`, `A` has to be repeated sufficient times such that it is at least as long as `B` (or one more), hence we can conclude that the theoretical lower bound for the answer would be length of `B` / length of `A`.

Let `x` be the theoretical lower bound, which is `ceil(len(B)`/`len(A))`.

The answer `n` can only be `x` or `x + 1` (in the case where `len(B)` is a multiple of `len(A)` like in `A = "abcd"` and `B = "cdabcdab"`) and not more. Because if `B` is already in `A * n`, `B` is definitely in `A * (n + 1)`. 

Hence we only need to check whether `B in A * x` or `B in A * (x + 1)`, and if both are not possible return -1.

*- Yangshun*

Here's the cheeky two-liner suggested by @liping5:

```
class Solution(object):
    def repeatedStringMatch(self, A, B):
        t = -(-len(B) // len(A)) # Equal to ceil(len(b) / len(a))
        return t * (B in A * t) or (t + 1) * (B in A * (t + 1)) or -1
```

But don't do the above in interviews. Doing the following is more readable.

```
class Solution(object):
    def repeatedStringMatch(self, A, B):
        times = -(-len(B) // len(A)) # Equal to ceil(len(b) / len(a))
        for i in range(2):
          if B in (A * (times + i)):
            return times + i
        return -1

```

Thanks @ManuelP for suggesting that `times = int(math.ceil(float(len(B)) / len(A)))` can be written as `times = -(-len(B) // len(A))`.
</p>


### Java Solution - Just keep building (OJ Missing Test Cases)
- Author: diddit
- Creation Date: Sun Oct 01 2017 11:00:57 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 03:17:11 GMT+0800 (Singapore Standard Time)

<p>
Since LC has so many test cases missing, I wrote new code.
The idea is to keep string builder and appending until the length A is greater or equal to B.

     public int repeatedStringMatch(String A, String B) {

        int count = 0;
        StringBuilder sb = new StringBuilder();
        while (sb.length() < B.length()) {
            sb.append(A);
            count++;
        }
        if(sb.toString().contains(B)) return count;
        if(sb.append(A).toString().contains(B)) return ++count;
        return -1;
    }

Here's the old idea I used which got accepted with multiple bugs.

~~Idea is to count all A in B as first step. 
Then remove all A in B. 
Then remaining B is either present in A or A+A.~~

    public int repeatedStringMatch(String A, String B) {
        int i = 0, count = 0;
        while (i < B.length()) {
            int idx = B.indexOf(A, i);
            if (idx == -1) break;
            i = idx + A.length();
            count++;
        }
        B = B.replaceAll(A, ""); // remaining B if valid, should be smaller than A
        if (!B.isEmpty()) {
            if (A.startsWith(B)) count++; // B is substring AND first part of A
            else if(A.contains(B)) return -1; // B is substring somewhere in between
            else if ((A + A).contains(B)) count += 2; // B in rotating A
            else return -1;
        }
        return count;
    }
</p>


### C++ 4 lines O(m * n) | O(1) and KMP O(m + n) | O(n)
- Author: votrubac
- Creation Date: Sun Oct 01 2017 11:09:04 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 15 2020 12:16:42 GMT+0800 (Singapore Standard Time)

<p>
This is basically a modified version of string find, which does not stop at the end of A, but continue matching by looping through A.
```
int repeatedStringMatch(string A, string B) {
    for (auto i = 0, j = 0; i < A.size(); ++i) {
        for (j = 0; j < B.size() && A[(i + j) % A.size()] == B[j]; ++j);
        if (j == B.size()) 
            return (i + j - 1) / A.size() + 1;
    }
    return -1;
}
```
As suggested by @k_j, I am also providing O(n + m) version that uses a prefix table (KMP). We first compute the prefix table using the suffix and prefix pointers. Then we are going through A only once, shifting B using the prefix table.

This solution requires O(n) extra memory for the prefix table, but it\'s the fastest out there (OJ runtime is 3 ms). However, we do not need extra memory to append A multiple times, as in many other solutions.

Also thanks to:
- @mylzsd for the review and the optimization to move *i* faster (skip multiple search characters) rather incrementing it one by one.
- @liudonggalaxy for finding and fixing the logic bug in the prefix table computation.
```
int repeatedStringMatch(string a, string b) {
    vector<int> prefTable(b.size() + 1); // 1-based to avoid extra checks.
    for (auto sp = 1, pp = 0; sp < b.size(); )
      if (b[pp] == b[sp]) 
          prefTable[++sp] = ++pp;
      else if 
          (pp == 0) prefTable[++sp] = pp;
      else 
          pp = prefTable[pp];	
    for (auto i = 0, j = 0; i < a.size(); i += max(1, j - prefTable[j]), j = prefTable[j]) {
        while (j < b.size() && a[(i + j) % a.size()] == b[j]) ++j;
        if (j == b.size()) 
            return (i + j - 1) / a.size() + 1;
    }
    return -1;
}
```
Since several fellow coders had this question, I am copying here a simple example demonstrating how the KMP algorithm uses the prefix table to skip previously matched prefixes. 

A: **aabaa**baac
B: **aabaa**c

We will start matching from A[i = 0] and B[j = 0], match "aabaa" and stop on B[j = 5] (\'c\' != \'b\'). Without the prefix table, we will have to match again starting from A[i = 1] and B[j = 0].

The prefix table for A looks like this (it\'s 1-based to avoid extra checks): [0, 0, 1, 0, 1, 2, 3, 4, 5, 2].It tells us that we already matched "aa", and we can continue matching from A[i + j = 5] and B[j = 2].

i += max(1, j - prefTable[j]); i = 0 + 5 - prefTable[5] = 0 + 5 - 2 = 3;
j = prefTable[5] = 2;

A: ~~aab~~aa**baac**
B: ___aa**baac**
</p>


