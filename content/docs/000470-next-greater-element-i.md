---
title: "Next Greater Element I"
weight: 470
#id: "next-greater-element-i"
---
## Description
<div class="description">
<p>
You are given two arrays <b>(without duplicates)</b> <code>nums1</code> and <code>nums2</code> where <code>nums1</code>’s elements are subset of <code>nums2</code>. Find all the next greater numbers for <code>nums1</code>'s elements in the corresponding places of <code>nums2</code>. 
</p>

<p>
The Next Greater Number of a number <b>x</b> in <code>nums1</code> is the first greater number to its right in <code>nums2</code>. If it does not exist, output -1 for this number.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> <b>nums1</b> = [4,1,2], <b>nums2</b> = [1,3,4,2].
<b>Output:</b> [-1,3,-1]
<b>Explanation:</b>
    For number 4 in the first array, you cannot find the next greater number for it in the second array, so output -1.
    For number 1 in the first array, the next greater number for it in the second array is 3.
    For number 2 in the first array, there is no next greater number for it in the second array, so output -1.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> <b>nums1</b> = [2,4], <b>nums2</b> = [1,2,3,4].
<b>Output:</b> [3,-1]
<b>Explanation:</b>
    For number 2 in the first array, the next greater number for it in the second array is 3.
    For number 4 in the first array, there is no next greater number for it in the second array, so output -1.
</pre>
</p>


<p><b>Note:</b><br>
<ol>
<li>All elements in <code>nums1</code> and <code>nums2</code> are unique.</li>
<li>The length of both <code>nums1</code> and <code>nums2</code> would not exceed 1000.</li>
</ol>
</p>
</div>

## Tags
- Stack (stack)

## Companies
- Amazon - 9 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Twitter - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary

You are given two arrays (without duplicates) $$findNums$$ and $$nums$$ where $$findNums$$’s elements are subset of $$nums$$.Find all the
next greater
numbers for $$findNums$$'s elements in the corresponding places of $$nums$$.

The Next Greater Number of a number x in $$findNums$$ is the first greater number to its right in $$nums$$. If it does not exist, output -1 for
this number.

## Solution

---
#### Approach #1 Brute Force [Accepted]

 In this method, we pick up every element of the $$findNums$$ array(say $$findNums[i]$$) and then search for its own occurence in the $$nums$$ array(which is
 indicated by setting $$found$$ to True). After this, we look linearly for a number in $$nums$$ which is greater than $$findNums[i]$$, which
 is also added to the $$res$$ array to be returned. If no such element is found, we put a $$\text{-1}$$ at the corresponding location.

 
<iframe src="https://leetcode.com/playground/FEQ5JHVr/shared" frameBorder="0" name="FEQ5JHVr" width="100%" height="428"></iframe>

 **Complexity Analysis**

 * Time complexity : $$O(m*n)$$. The complete $$nums$$ array(of size $$n$$) needs to be scanned for all the $$m$$ elements of $$findNums$$ in
 the worst case.
 * Space complexity : $$O(m)$$. $$res$$ array of size $$m$$ is used, where $$m$$ is the length of $$findNums$$ array.

---

#### Approach #2 Better Brute Force [Accepted]

Instead of searching for the occurence of $$findNums[i]$$ linearly in the $$nums$$ array, we can make use of a hashmap $$hash$$ to store
the elements of $$nums$$ in the form of $$(element, index)$$. By doing this, we can find $$findNums[i]$$'s index in $$nums$$ array directly and
then continue to search for the next larger element in a linear fashion.

 
<iframe src="https://leetcode.com/playground/xtBuL7iz/shared" frameBorder="0" name="xtBuL7iz" width="100%" height="445"></iframe>

 **Complexity Analysis**

 * Time complexity : $$O(m*n)$$. The whole $$nums$$ array, of length $$n$$ needs to be scanned for all the $$m$$ elements of $$finalNums$$ in the worst case.

 * Space complexity : $$O(m)$$. $$res$$ array of size $$m$$ is used. A hashmap $$hash$$ of size $$m$$ is used, where $$m$$ refers to the
 length of the $$findNums$$ array.

---

#### Approach #3 Using Stack [Accepted]

In this approach, we make use of pre-processing first so as to make the results easily available later on.
 We make use of a stack($$stack$$) and a hashmap($$map$$). $$map$$ is used to store the result for every posssible number in $$nums$$ in
the form of $$(element, next\_greater\_element)$$. Now, we look at how to make entries in $$map$$.

We iterate over the $$nums$$ array
from the left to right. We push every element $$nums[i]$$ on the stack if it is less than the previous element on the top of the stack
($$stack[top]$$). No entry is made in $$map$$ for such $$nums[i]'s$$ right now. This happens because
the $$nums[i]'s$$ encountered so far are coming in a descending order.

If we encounter an element $$nums[i]$$ such that $$nums[i] > stack[top]$$, we keep on popping all the elements
from $$stack[top]$$ until we encounter $$stack[k]$$ such that $$stack[k] &leq; nums[i]$$. For every element popped out of the stack
$$stack[j]$$, we put the popped element along with its next greater number(result) into the hashmap $$map$$, in the form
$$(stack[j], nums[i])$$ . Now, it is obvious that the
next greater element for all elements $$stack[j]$$, such that $$k < j &le; top$$ is $$nums[i]$$(since this larger element caused all the
$$stack[j]$$'s to be popped out). We stop popping the elements at $$stack[k]$$ because this $$nums[i]$$ can't act as the next greater element
for the next elements on the stack.

Thus, an element is popped out of the stack whenever a next greater element is found for it. Thus, the elements remaining in the stack are the
ones for which no next greater element exists in the $$nums$$ array. Thus, at the end of the iteration over $$nums$$, we pop the remaining
elements from the $$stack$$ and put their entries in $$hash$$ with a $$\text{-1}$$ as their corresponding results.

Then, we can simply iterate over the $$findNums$$ array to find the corresponding results from $$map$$ directlhy.

The following animation makes the method clear:

!?!../Documents/496_Next_Greater_Element_I.json:1280,720!?!



 
<iframe src="https://leetcode.com/playground/hvr7nhcH/shared" frameBorder="0" name="hvr7nhcH" width="100%" height="360"></iframe>

 **Complexity Analysis**

 * Time complexity : $$O(m+n)$$. The entire $$nums$$ array(of size $$n$$) is scanned only once. The stack's $$n$$ elements are popped
 only once. The $$findNums$$ array is also scanned only once.

 * Space complexity : $$O(m+n)$$. $$stack$$ and $$map$$ of size $$n$$ is used. $$res$$ array of size $$m$$ is used, where $$n$$ refers to the
 length of the $$nums$$ array and $$m$$ refers to the length of the $$findNums$$ array.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 10 lines linear time complexity O(n) with explanation
- Author: yuxiangmusic
- Creation Date: Sun Feb 05 2017 13:29:32 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 06:51:21 GMT+0800 (Singapore Standard Time)

<p>
Key observation:
Suppose we have a decreasing sequence followed by a greater number
For example ```[5, 4, 3, 2, 1, 6]``` then the greater number ```6``` is the next greater element for all previous numbers in the sequence

We use a stack to keep a **decreasing** sub-sequence, whenever we see a number ```x``` greater than ```stack.peek()``` we pop all elements less than ```x``` and for all the popped ones, their next greater element is ```x```
For example ```[9, 8, 7, 3, 2, 1, 6]```
The stack will first contain ```[9, 8, 7, 3, 2, 1]``` and then we see ```6``` which is greater than ```1``` so we pop ```1 2 3``` whose next greater element should be ```6```
```
    public int[] nextGreaterElement(int[] findNums, int[] nums) {
        Map<Integer, Integer> map = new HashMap<>(); // map from x to next greater element of x
        Stack<Integer> stack = new Stack<>();
        for (int num : nums) {
            while (!stack.isEmpty() && stack.peek() < num)
                map.put(stack.pop(), num);
            stack.push(num);
        }   
        for (int i = 0; i < findNums.length; i++)
            findNums[i] = map.getOrDefault(findNums[i], -1);
        return findNums;
    }
```
</p>


### Confusing statement
- Author: LeonCheng
- Creation Date: Wed Aug 30 2017 10:47:37 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 13:34:49 GMT+0800 (Singapore Standard Time)

<p>
The description of the problems is very confusing, I spent much more time figuring out what the statement means than figuring out the algorithm.
</p>


### C++ stack + unordered_map
- Author: lzl124631x
- Creation Date: Wed Feb 08 2017 20:29:38 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 22:09:46 GMT+0800 (Singapore Standard Time)

<p>
See more in my github repo [LeetCode](https://github.com/lzl124631x/LeetCode)

```cpp
// OJ: https://leetcode.com/problems/next-greater-element-i/
// Author: github.com/lzl124631x
// Time: O(N)
// Space: O(N)
class Solution {
public:
    vector<int> nextGreaterElement(vector<int>& findNums, vector<int>& nums) {
        stack<int> s;
        unordered_map<int, int> m;
        for (int n : nums) {
            while (s.size() && s.top() < n) {
                m[s.top()] = n;
                s.pop();
            }
            s.push(n);
        }
        vector<int> ans;
        for (int n : findNums) ans.push_back(m.count(n) ? m[n] : -1);
        return ans;
    }
};
```
</p>


