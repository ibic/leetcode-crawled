---
title: "Pairs of Songs With Total Durations Divisible by 60"
weight: 964
#id: "pairs-of-songs-with-total-durations-divisible-by-60"
---
## Description
<div class="description">
<p>In a list of songs, the <code>i</code>-th&nbsp;song has a duration of&nbsp;<code>time[i]</code> seconds.&nbsp;</p>

<p>Return the number of pairs of songs for which their total&nbsp;duration in seconds is divisible by <code>60</code>.&nbsp; Formally, we want the number of&nbsp;indices <code>i</code>, <code>j</code> such that&nbsp;<code>i &lt; j</code> with <code>(time[i] + time[j]) % 60 == 0</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[30,20,150,100,40]</span>
<strong>Output: </strong><span id="example-output-1">3</span>
<strong>Explanation: </strong>Three pairs have a total duration divisible by 60:
(time[0] = 30, time[2] = 150): total duration 180
(time[1] = 20, time[3] = 100): total duration 120
(time[1] = 20, time[4] = 40): total duration 60
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[60,60,60]</span>
<strong>Output: </strong><span id="example-output-2">3</span>
<strong>Explanation: </strong>All three pairs have a total duration of 120, which is divisible by 60.
</pre>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>1 &lt;= time.length &lt;= 60000</code></li>
	<li><code>1 &lt;= time[i] &lt;= 500</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Citrix - 4 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Pocket Gems - 5 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Dropbox - 3 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Two Sum with K = 60
- Author: lee215
- Creation Date: Sun Mar 17 2019 12:02:09 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 28 2020 10:43:02 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Calculate the `time % 60` then it will be exactly same as two sum problem.
<br>

## **Explanation**
`t % 60` gets the remainder from 0 to 59.
We count the occurrence of each remainders in a array/hashmap `c`.

we want to know that, for each `t`, 
how many `x` satisfy `(t + x) % 60 = 0`.

The straight forward idea is to take `x % 60 = 60 - t % 60`,
which is valid for the most cases.
But if `t % 60 = 0`, `x % 60 = 0` instead of 60.

One solution is to use `x % 60 = (60 - t % 60) % 60`,
the other idea is to use `x % 60 = (600 - t) % 60`.
Not sure which one is more straight forward.
<br>

**Java:**
```java
    public int numPairsDivisibleBy60(int[] time) {
        int c[]  = new int[60], res = 0;
        for (int t : time) {
            res += c[(600 - t) % 60];
            c[t % 60] += 1;
        }
        return res;
    }
```

**C++:**
```cpp
    int numPairsDivisibleBy60(vector<int>& time) {
        vector<int> c(60);
        int res = 0;
        for (int t : time) {
            res += c[(600 - t) % 60];
            c[t % 60] += 1;
        }
        return res;
    }
```

**Python:**
```py
    def numPairsDivisibleBy60(self, time):
        c = [0] * 60
        res = 0
        for t in time:
            res += c[-t % 60]
            c[t % 60] += 1
        return res
```
</p>


### [Java/Python 3] O(n) code w/ comment, similar to Two Sum
- Author: rock
- Creation Date: Sun Mar 17 2019 12:01:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 20 2020 01:02:30 GMT+0800 (Singapore Standard Time)

<p>
Let `target` in Two Sum be `60` and each item in `time % 60`, then the two problems are very similar to each other. 

----

**Explain the statement `theOther = (60 - t % 60) % 60;`**

Let `theOther` be in the pair with `t`, then

```
(t + theOther) % 60 == 0
```

so we have

```
t % 60 + theOther % 60 = 0 or 60
```

then

```
theOther % 60 = t % 60 = 0 
or
theOther % 60 = 60 - t % 60
```

Note that it is possible that `t % 60 == 0`, which results `60 - t % 60 == 60`,

therefore, we should have

```
theOther % 60 = (60 - t % 60) % 60
```
Let `0 <= theOther < 60`, therefore `thOther = theOther % 60`.
use `theOther` to replace `theOther % 60`, we get 
```
theOther = (60 - t % 60) % 60;
```

----

**Code:**
```java
    public int numPairsDivisibleBy60(int[] time) {
        Map<Integer, Integer> count = new HashMap<>();
        int ans = 0;
        for (int t : time) {
            int theOther = (60 - t % 60) % 60;
            ans += count.getOrDefault(theOther, 0); // in current HashMap, get the number of songs that if adding t equals to a multiple of 60.
            count.put(t % 60, 1 + count.getOrDefault(t % 60, 0)); // update the number of t % 60.
        }
        return ans;
    }
```

```python
    def numPairsDivisibleBy60(self, time: List[int]) -> int:
        ans, cnt = 0, collections.Counter()
        for t in time:
            theOther = -t % 60
            ans += cnt[theOther]
            cnt[t % 60] += 1
        return ans
```

**Analysis:**

Time & space: O(n), where n = time.length.
</p>


### Java solution from combination perspective with best explanation
- Author: 416486188
- Creation Date: Tue May 21 2019 07:13:20 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 14 2020 13:02:08 GMT+0800 (Singapore Standard Time)

<p>
**Analysis**

From combination perspective, we have:
`# pais to make up 60 = (# of time whose % 60 is i) * (# of time whose % 60 is 60 - i) ` for all possible `i`

For example: 
If we have `5` numbers who `% 60 = 20`, and  `7` numbers who `% 60 = 40`, then we can get **`5 * 7 = 35`** pairs to make up `60`.

Above all, if we represents the number of `time` whose `% 60 = i` as `map[i]`, then the result = 

```
map[0]? + // why ? explain below
map[1] * map[59] +
map[2] * map[58] +
map[3] * map[57] +
... +
map[28] * map[32] +
map[29] * map[31] +
map[30]? // why ? explain below
```

Notice that I put question mark `?` after `map[0]` and `map[30]` on the above formula. Because for `map[0]` and `map[30]`, they are **self-pairing** to make up `60`, so:
1. **`map[0]? = C(n, 2) = map[0] * (map[0] - 1)/2`**
2. **`map[30]? = C(n, 2) = map[30] * (map[30] - 1)/2`**

Thus, final formula:

```
map[0] * (map[0] - 1)/2 +
map[1] * map[59] +
map[2] * map[58] +
map[3] * map[57] +
... +
map[28] * map[32] +
map[29] * map[31] +
map[30] * (map[30] - 1)/2
```
---

**Data structure**
We create `int[] map` to store the `<remainder, frequency>` 

Of course, `Map<Integer, Integer>` also works, but clearly it is not as efficient as `int[] map`. Just like we prefer to use `int[] map = new int[26]` to keep track of `<letter, frequency>`

---
**Strategy**

1. Iterate each num in `time`, and populate `map`
2. Complete the formular above

---
**Final code**

```java
class Solution {
    public int numPairsDivisibleBy60(int[] time) {
        if(time == null || time.length == 0) return 0;
        
        int n = time.length;
        int[] map = new int[60];
        int res = 0;
        for(int i = 0; i < n; i++){
            int remainder = time[i] % 60;
            map[remainder]++;
        }
        
        res += map[0] * (map[0] - 1)/2;
        res += map[30] * (map[30] - 1)/2;
        for(int i = 1; i < 30; i++){
            res += map[i] * map[60 - i];
        }
        
        return res;
    }
}
```

---
**Complexity Anayalsis**
TC(Time Complexity): `O(n)`
SC(Space Complexity): `O(60) = O(1)`

---
**Last but not least**
If you have any confusion or any opinion for where you think can be better described please leave a comment below, I will insist on updating it for **at least 100 years**.

</p>


