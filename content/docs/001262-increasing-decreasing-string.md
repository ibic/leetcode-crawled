---
title: "Increasing Decreasing String"
weight: 1262
#id: "increasing-decreasing-string"
---
## Description
<div class="description">
<p>Given a string <code>s</code>. You should re-order the string using the following algorithm:</p>

<ol>
	<li>Pick the <strong>smallest</strong> character from <code>s</code> and <strong>append</strong> it to the result.</li>
	<li>Pick the <strong>smallest</strong> character from <code>s</code> which is greater than the last appended character to the result and <strong>append</strong> it.</li>
	<li>Repeat step 2 until you cannot pick more characters.</li>
	<li>Pick the <strong>largest</strong>&nbsp;character from <code>s</code> and <strong>append</strong> it to the result.</li>
	<li>Pick the <strong>largest</strong>&nbsp;character from <code>s</code> which is smaller than the last appended character to the result and <strong>append</strong> it.</li>
	<li>Repeat step 5 until you cannot pick more characters.</li>
	<li>Repeat the steps from 1 to 6 until you pick all characters from <code>s</code>.</li>
</ol>

<p>In each step, If the smallest or the largest character appears more than once you can choose any occurrence and append it to the result.</p>

<p>Return <em>the result string</em> after sorting <code>s</code>&nbsp;with this algorithm.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aaaabbbbcccc&quot;
<strong>Output:</strong> &quot;abccbaabccba&quot;
<strong>Explanation:</strong> After steps 1, 2 and 3 of the first iteration, result = &quot;abc&quot;
After steps 4, 5 and 6 of the first iteration, result = &quot;abccba&quot;
First iteration is done. Now s = &quot;aabbcc&quot; and we go back to step 1
After steps 1, 2 and 3 of the second iteration, result = &quot;abccbaabc&quot;
After steps 4, 5 and 6 of the second iteration, result = &quot;abccbaabccba&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;rat&quot;
<strong>Output:</strong> &quot;art&quot;
<strong>Explanation:</strong> The word &quot;rat&quot; becomes &quot;art&quot; after re-ordering it with the mentioned algorithm.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;leetcode&quot;
<strong>Output:</strong> &quot;cdelotee&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;ggggggg&quot;
<strong>Output:</strong> &quot;ggggggg&quot;
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;spo&quot;
<strong>Output:</strong> &quot;ops&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 500</code></li>
	<li><code>s</code> contains only lower-case English letters.</li>
</ul>

</div>

## Tags
- String (string)
- Sort (sort)

## Companies
- Akuna Capital - 6 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Two clean codes w/ explanation and analysis.
- Author: rock
- Creation Date: Sun Mar 08 2020 00:01:57 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 13 2020 00:57:18 GMT+0800 (Singapore Standard Time)

<p>
**Q & A:**

Q1: TreeSet<>(tm.keySet()) vs TreeSet(tm.descendingKeySet()).
In first Java code, I thought TreeSet will always sort from small to large, if you don\'t specify descending order, no matter what order you put in the ().. can you explain why this works..

A1: `TreeSet(SortedSet())` will use the same ordering  as the `SortedSet()`.

**End of Q & A.**

----
**Java**
1. Use TreeMap to count each char in `s`;
2. Append to StringBuilder the keys in TreeMap in sorted order, decrease the count of each key, and remove the entry if reaching `0`;
3. Do the similar operation in step 2, but in descending order of the keys;
4. Repeat 2 and 3 till the TreeMap is empty.

```java
    public String sortString(String s) {
        StringBuilder ans = new StringBuilder();
        TreeMap<Character, Integer> tm = new TreeMap<>();
        for (char c : s.toCharArray()) {
            tm.put(c, 1 + tm.getOrDefault(c, 0));
        }
        boolean asc = true;
        while (!tm.isEmpty()) {
            for (char c : asc ? new TreeSet<>(tm.keySet()) : new TreeSet(tm.descendingKeySet())) {
                ans.append(c);
                tm.put(c, tm.get(c) - 1);
                tm.remove(c, 0);
            }
            asc = !asc; // same as asc ^= true;
        }
        return ans.toString();
    }
```
Use array instead of TreeMap to get better time performance. runtime: 1 ms, beat 100%.
```java
    public String sortString(String s) {
        StringBuilder ans = new StringBuilder();
        int[] count = new int[26];
        for (char c : s.toCharArray())
            ++count[c - \'a\'];
        while (ans.length() < s.length()) {
            add(count, ans, true);
            add(count, ans, false);
        }
        return ans.toString();
    }
    private void add(int[] cnt, StringBuilder ans, boolean asc) {
        for (int i = 0; i < 26; ++i) {
            int j = asc ? i : 25 - i;
            if (cnt[j]-- > 0)
                ans.append((char)(j + \'a\'));
        }
    }
```
**Python 3**
```python
    def sortString(self, s: str) -> str:
        cnt, ans, asc = collections.Counter(s), [], True
        while cnt:                                                                  # if Counter not empty.
            for c in sorted(cnt.keys()) if asc else reversed(sorted(cnt.keys())):   # traverse keys in ascending/descending order.
                ans.append(c)                                                       # append the key.
                cnt[c] -= 1                                                         # decrease the count.
                if cnt[c] == 0:                                                     # if the count reaches to 0.
                    del cnt[c]                                                      # remove the key from the Counter.
            asc = not asc                                                           # change the direction, same as asc ^= True.
        return \'\'.join(ans)
```
Similar version, but no removal of key from the Counter.
```python
    def sortString(self, s: str) -> str:
        cnt, ans, asc  = collections.Counter(s), [], True
        while len(ans) < len(s):                                # if not finish.
            for i in range(26):                                 # traverse lower case letters.
                c = string.ascii_lowercase[i if asc else ~i]    # in ascending/descending order.
                if cnt[c] > 0:                                  # if the count > 0.
                    ans.append(c)                               # append the character.
                    cnt[c] -= 1                                 # decrease the count.
            asc = not asc                                       # change direction.
        return \'\'.join(ans)
```
**Analysis:**
Since there are only `26` distinct lower case letters, sorting and each TreeMap operation cost at most `26 * log26`, which can be regarded as a constant.

Time & space: `O(n)`, where `n` = `s.length()`.
</p>


### [Python] Easy-to-understand solution explained
- Author: m-Just
- Creation Date: Sun Mar 08 2020 00:48:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 01:26:06 GMT+0800 (Singapore Standard Time)

<p>
Steps:
1. Count the number of occurrences of every letter.
2. Sort in alphabetic order.
3. Follow the procedure defined in the problem statement.
```python
def sortString(self, s: str) -> str:
    d = sorted([c, n] for c, n in collections.Counter(s).items())
    r = []
    while len(r) < len(s):
        for i in range(len(d)):
            if d[i][1]:
                r.append(d[i][0])
                d[i][1] -= 1
        for i in range(len(d)):
            if d[~i][1]:
                r.append(d[~i][0])
                d[~i][1] -= 1
    return \'\'.join(r)
```
Vote up if you find this helpful, thanks!
</p>


### C++ Counts
- Author: votrubac
- Creation Date: Sun Mar 08 2020 17:13:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 17:15:32 GMT+0800 (Singapore Standard Time)

<p>
```cpp
string sortString(string s, string res = "") {
    int cnt[26] = {};
    for (auto ch: s)
        ++cnt[ch - \'a\'];
    while (s.size() != res.size()) {
        for (auto i = 0; i < 26; ++i)
            res += string(--cnt[i] >= 0 ? 1 : 0, \'a\' + i); 
        for (int i = 25; i >=0; --i)
            res += string(--cnt[i] >= 0 ? 1 : 0, \'a\' + i);          
    }
    return res;
}
```
</p>


