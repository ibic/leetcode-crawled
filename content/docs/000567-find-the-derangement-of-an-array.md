---
title: "Find the Derangement of An Array"
weight: 567
#id: "find-the-derangement-of-an-array"
---
## Description
<div class="description">
<p>
In combinatorial mathematics, a derangement is a permutation of the elements of a set, such that no element appears in its original position.
</p>

<p>
There's originally an array consisting of <code>n</code> integers from 1 to <code>n</code> in ascending order, you need to find the number of derangement it can generate.
</p>

<p>
Also, since the answer may be very large, you should return the output mod 10<sup>9</sup> + 7.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> 3
<b>Output:</b> 2
<b>Explanation:</b> The original array is [1,2,3]. The two derangements are [2,3,1] and [3,1,2].
</pre>
</p>

<p><b>Note:</b><br/>
<code>n</code> is in the range of [1, 10<sup>6</sup>].
</p>
</div>

## Tags
- Math (math)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- IXL - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force


The simplest solution is to consider every possible permutation of the given numbers from $$1$$ to $$n$$ and count the number of permutations which are dereangements of the 
original arrangement. 


**Complexity Analysis**

* Time complexity : $$O\big((n+1)!\big)$$. $$n!$$ permutations are possible for $$n$$ numbers. For each permutation, we need to traverse over the whole arrangement to check if it 
is a derangement or not, which takes $$O(n)$$ time.

* Space complexity : $$O(n)$$. Each permutation would require $$n$$ space to be stored.
<br />
<br />
---
#### Approach 2: Recursion

**Algorithm**

In order to find the number of derangements for $$n$$ numbers, firstly we can consider the the original array to be 
`[1,2,3,...,n]`. Now, in order to generate the derangements of this array, assume that firstly, we move the number 1 from 
its original position and place at the place of the number $$i$$. But, now, this $$i^{th}$$ position can be chosen 
in $$n-1$$ ways. Now, for placing the number $$i$$ we have got two options:

1. We place $$i$$ at the place of $$1$$: By doing this, the problem of finding the derangements reduces to finding the derangements of the 
remaining $$n-2$$ numbers, since we've got $$n-2$$ numbers and $$n-2$$ places, such that every number can't be placed at exactly one position.

2. We don't place $$i$$ at the place of $$1$$: By doing this, the problem of finding the derangements reduces to finding the 
derangements for the $$n-1$$ elements(except 1). This is because, now we've got $$n-1$$ elements and these $$n-1$$ elements can't be placed at 
exactly one location(with $$i$$ not being placed at the first position).

![Derangement_Split](../Figures/634/634_Find_Derangements_split.PNG)
{:align="center"}

Based, on the above discussion, if $$d(n)$$ represents the number of derangements for $$n$$ elements, it can be obtained as:

$$d(n) = (n-1) \cdot [d(n-1) + d(n-2)]$$

This is a recursive equation and can thus, be solved easily by making use of a recursive function.

But, if we go with the above method, a lot of duplicate function calls wiil be made, with the same parameters being passed. This is because the same state can be reached through various paths in the recursive tree. In order to avoid these duplicate calls, we can store the result of a function call, once its made, 
into a memoization array. Thus, whenever the same function call is made again, we can directly return the result from this memoization array. 
This helps to prune the search space to a great extent.

<iframe src="https://leetcode.com/playground/w5a4kZLy/shared" frameBorder="0" width="100%" height="327" name="w5a4kZLy"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. $$memo$$ array of length $$n$$ is filled once only.

* Space complexity : $$O(n)$$. $$memo$$ array of length $$n$$ is used.
<br />
<br />
---

#### Approach 3: Dynamic Programming

**Algorithm**

As we've discussed above, the recursive formula for finding the derangements for $$n$$ elements is given by:

$$d(n) = (n-1) \cdot [d(n-1) + d(n-2)]$$

From this expression, we can see that the result for derangements for $$i$$ numbers depends only on the result of the derangments 
of numbers lesser than $$i$$. Thus, we can solve the given problem by making use of Dynamic Programming.

The equation for Dynamic Programming remains identical to the recursive equation.

$$dp[i] = (i - 1) \cdot (dp[i-1]+dp[i-2])$$

Here, $$dp[i]$$ is used to store the number of derangements for $$i$$ elements. We start filling the $$dp$$ array from $$i=0$$ and move towards the larger values of $$i$$. At the end, the value of 
$$dp[n]$$ gives the required result.

The following animation illustrates the $$dp$$ filling process.

!?!../Documents/634_Find_Derangements.json:1000,563!?!

<iframe src="https://leetcode.com/playground/MnmJJgky/shared" frameBorder="0" width="100%" height="293" name="MnmJJgky"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Single loop upto $$n$$ is required to fill the $$dp$$ array of size $$n$$.

* Space complexity : $$O(n)$$. $$dp$$ array of size $$n$$ is used.
<br />
<br />
---
#### Approach 4: Constant Space Dynamic Programming

**Algorithm**

In the last approach, we can easily observe that the result for $$dp[i]$$ depends only on the previous two elements, $$dp[i-1]$$ and 
$$dp[i-2]$$. Thus, instead of maintaining the entire 1-D array, we can just keep a track of the last two values required to calculate the 
value of the current element. By making use of this observation, we can save the space required by the $$dp$$ array in the last approach.

<iframe src="https://leetcode.com/playground/ddGwNAKa/shared" frameBorder="0" width="100%" height="310" name="ddGwNAKa"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Single loop upto $$n$$ is required to find the required result.

* Space complexity : $$O(1)$$. Constant extra space is used.
<br />
<br />
---
#### Approach 5: Formula

**Algorithm**

Before discussing this approach, we need to look at some preliminaries.

In combinatorics (combinatorial mathematics), the inclusion–exclusion principle is a counting 
technique which generalizes the familiar method of obtaining the number of elements in the union of two 
finite sets; symbolically expressed as

$$\left|A\cup B\right|=\left|A\right|+\left|B\right|-\left|A\cap B\right|$$ 

where $$A$$ and $$B$$ are two finite sets and $$\left|S\right|$$ indicates the cardinality of a set $$S$$ 
(which may be considered as the number of elements of the set, if the set is finite). 

![AUB](../Figures/634/634_AUB.PNG)
{:align="center"}

The formula expresses the fact that the sum of the sizes of the two sets may be too large since
 some elements may be counted twice. The double-counted elements are those in the intersection of 
 the two sets and the count is corrected by subtracting the size of the intersection.

The principle is more clearly seen in the case of three sets, which for the sets $$A$$, $$B$$ and $$C$$ is given by

$$\left|A\cup B\cup C\right|=\left|A\right|+\left|B\right|+\left|C\right|-\left|A\cap B\right|-\left|A\cap C\right|-\left|B\cap C\right|+\left|A\cap B\cap C\right|$$.

This formula can be verified by counting how many times each region in the 
Venn diagram figure shown below. 

![AUBUC](../Figures/634/634_AUBUC.png)
{:align="center"}

In this case, 
when removing the contributions of over-counted elements, the number of elements in the mutual 
intersection of the three sets has been subtracted too often, so must be added back in to get the correct total.

In its general form, the principle of inclusion–exclusion states that for finite sets $$A_1, ..., A_n$$, one
 has the identity

$$\bigg|\bigcup _{i=1}^{n}A_{i}\bigg|=\sum_{i=1}^{n}\left|A_{i}\right|-\sum_{1 \leq; i < j \leq; n}\left|A_{i}\cap A_{j}\right|+...$$

$$+...+\sum_{1 \leq; i < j < k \leq; n}\left|A_{i}\cap A_{j}\cap A_{k}\right|-..... +(-1)^{n}\left|A_{1}\cap... \cap A_{n}\right|$$


By applying De-Morgan's law to the above equation, we can obtain

$$\bigg|\bigcap _{i=1}^{n}\bar{A_{i}}\bigg|=\bigg|S-\bigcup _{i=1}^{n}A_{i}\bigg|=\left|S\right|-$$  $$\sum_{i=1}^{n}\left|A_{i}\right|+\sum_{1 \leq; i < j \leq; n}\left|A_{i}\cap A_{j}\right|-.... +(-1)^{n}\left|A_{1}\cap....\cap A_{n}\right|$$

Here, $$S$$ represents the universal set containing all of the $$A_i$$ and $$\bar{A_{i}}$$ denotes the complement of $$A_i$$ in $$S$$.

Now, let $$A_i$$ denote the set of permutations  which leave $$A_i$$ in its natural position. Thus, the number of permutations in which 
the $$i^{th}$$ element remains at its natural position is $$(n-1)!$$. Thus, the component $$\sum_{i=1}^{n}\left|A_{i}\right|$$ above 
becomes $${{n}\choose{1}} (n-1)!$$. Here, $${{n}\choose{1}}$$ represents the number of ways of choosing 1 element out of $$n$$ elements.

 Making use of this notation, the required number of derangements can be denoted by $$\left|\bigcap _{i=1}^{n}\bar{A_{i}}\right|$$ term. 

This is the same term which has been expanded in the last equation. Putting appropriate values of the elements, we can expand the above equation as:

$$\bigg|\bigcap _{i=1}^{n}\bar{A_{i}}\bigg|=n! -{n \choose 1}(n-1)! + {n\choose 2}(n-2)! - {n \choose 3}(n-3)! +...$$
$$...+(-1)^{p}{n \choose p}(n-p)! +...+ (-1)^{n}{n \choose n} (n-n)!$$

$$ = n! - \frac{n!}{1!} + \frac{n!}{2!} - \frac{n!}{3!}+...+(-1)^n \frac{n!}{n!}$$

We can make use of this formula to obtain the required number of derangements.

<iframe src="https://leetcode.com/playground/q9v7Lbcj/shared" frameBorder="0" width="100%" height="225" name="q9v7Lbcj"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Single loop upto $$n$$ is used.

* Space complexity : $$O(1)$$. Constant space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### If you don't understand
- Author: PrefixTree
- Creation Date: Wed Sep 20 2017 13:27:57 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 14:32:00 GMT+0800 (Singapore Standard Time)

<p>
This is actually a simple DP problem.
DP formula is: D(n) = (n-1) [D(n-2) + D(n-1)]

I don't understand this at start so I read other discuss, didn't found their explanation very clear, so I'd like to do a more detailed explanation here.

let's consider what D(n) means, it means the number of derangement for an array with index from 1 to n, and value from 1 to n. 
Then let's think about value n, we know it can not be put on index n, instead, it can be put on index 1 to n-1, so there are n-1 possibilities.
For each of the situation above, let's say value n is put on index i, then we need to discuss about where we put value i:
1.if value i is put on index n (looks like value i and value n swapped their positions), then we can just ignore value i, value n, index i, index n, what's left are n-2 different values and n-2 different indexes, the problem becomes D(n-2).
2.if value i is not put on index n, then we can only ignore value n and index i, what's left are n-1 different values and n-1 different indexes, each value has an index that it can not be put on. (value i can not be put on index n here) So the problem becomes D(n-1).

Therefore, D(n) = (n-1) [D(n-2) + D(n-1)].

Simple DP solution:
```
class Solution {
    public int findDerangement(int n) {
        if (n <= 1) return 0;
        long dp[] = new long[n + 1];
        long mod = 1000000007;
        dp[2] = 1;
        for (int i = 3; i <= n; i++) {
            dp[i] = (i - 1) * (dp[i - 1] + dp[i - 2]) % mod;
        }
        return (int)dp[n];        
    }
}
```
Optimize space to O(1):
```
class Solution {
    public int findDerangement(int n) {
        if (n <= 1) return 0;
        if (n == 2) return 1;
        long prevPrev = 0, prev = 1, curr = 0;
        long mod = 1000000007;
        for (int i = 3; i <= n; i++) {
            curr = (i - 1) * (prevPrev + prev) % mod;
            prevPrev = prev;
            prev = curr;
        }
        return (int)curr;
    }    
}
```
</p>


### [Java] 5 lines O(1) space solution
- Author: AlphaZero
- Creation Date: Sun Jul 02 2017 11:20:11 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 07 2018 13:19:13 GMT+0800 (Singapore Standard Time)

<p>
Details can be found from wiki:
[https://en.wikipedia.org/wiki/Derangement#Counting_derangements](https://en.wikipedia.org/wiki/Derangement#Counting_derangements)
```    
    private static final int M = 1000000007;
    public int findDerangement(int n) {
        long ans = 1;
        for (int i = 1; i <= n; i++) 
            ans = (i * ans % M + (i % 2 == 0 ? 1 : -1)) % M;
        return (int) ans;
    }
    
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Jul 02 2017 11:06:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 09:49:40 GMT+0800 (Singapore Standard Time)

<p>
```
def findDerangement(self, N):
    MOD = 10**9 + 7
    X, Y = 1, 0
    for n in xrange(2, N+1):
        X, Y = Y, (n - 1) * (X + Y) % MOD
    return Y
```

Let ```D(N)``` be the required answer.  The recursion for the number of derangements of N is: ```D(N) = (N-1) * (D(N-1) + D(N-2))```.  With this recursion in hand, the problem becomes similar to finding the N-th fibonacci number.

To prove it, suppose there are people and hats labelled ```1...N```.  We want the number of ways to put a hat on each person such that no person is wearing their hat.  The first person has N-1 choices to put on a hat, say he wears hat X.  Now consider what hat person X is wearing.  Either he takes hat 1, and we have ```D(N-2)``` ways to arrange the remaining hats among people; or he doesn't take hat 1, which if we relabelled it as hat X, would have ```D(N-1)``` ways to arrange the remaining hats.
</p>


