---
title: "Average Salary Excluding the Minimum and Maximum Salary"
weight: 1355
#id: "average-salary-excluding-the-minimum-and-maximum-salary"
---
## Description
<div class="description">
<p>Given an array of <strong>unique</strong> integers <code>salary</code>&nbsp;where <code>salary[i]</code> is the salary of the employee <code>i</code>.</p>

<p>Return the average salary of employees excluding the minimum and maximum salary.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> salary = [4000,3000,1000,2000]
<strong>Output:</strong> 2500.00000
<strong>Explanation: </strong>Minimum salary and maximum salary are 1000 and 4000 respectively.
Average salary excluding minimum and maximum salary is (2000+3000)/2= 2500
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> salary = [1000,2000,3000]
<strong>Output:</strong> 2000.00000
<strong>Explanation: </strong>Minimum salary and maximum salary are 1000 and 3000 respectively.
Average salary excluding minimum and maximum salary is (2000)/1= 2000
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> salary = [6000,5000,4000,3000,2000,1000]
<strong>Output:</strong> 3500.00000
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> salary = [8000,9000,2000,3000,6000,1000]
<strong>Output:</strong> 4750.00000
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>3 &lt;= salary.length &lt;= 100</code></li>
	<li><code>10^3&nbsp;&lt;= salary[i] &lt;= 10^6</code></li>
	<li><code>salary[i]</code> is unique.</li>
	<li>Answers within <code>10^-5</code> of the actual value will be accepted as correct.</li>
</ul>
</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Netsuite - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ one-liner
- Author: votrubac
- Creation Date: Sun Jun 28 2020 00:00:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 00:33:54 GMT+0800 (Singapore Standard Time)

<p>
Note the double conversion (`0.` - with a dot - as the initial value for `accumulate`).

Somehow I missed this in a hurry and got 5 minutes penalty during the contest!!

```cpp
double average(vector<int>& s) {
    return (accumulate(begin(s), end(s), 0.) - *min_element(begin(s), end(s)) 
        - *max_element(begin(s), end(s))) / (s.size() - 2);
}
```
</p>


### [Java/Python 3] 2 methods: One Pass and short code.
- Author: rock
- Creation Date: Sun Jun 28 2020 00:20:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 22:18:06 GMT+0800 (Singapore Standard Time)

<p>
**One Pass:**

```java
    public double average(int[] salary) {
        double sum = 0d;
        int M = Integer.MIN_VALUE, m = Integer.MAX_VALUE;
        for (int s : salary) {
            m = Math.min(m, s);
            M = Math.max(M, s);
            sum += s;
        }
        return (sum - m - M) / (salary.length - 2);
    }
```
```python
    def average(self, salary: List[int]) -> float:
        m, M, total = math.inf, -math.inf, 0.0
        for s in salary:
            m, M = min(m, s), max(M, s)
            total += s
        return (total - m - M) / (len(salary) - 2)
```

----
**Short code:**

```java

import java.util.IntSummaryStatistics;

    public double average(int[] salary) {
        IntSummaryStatistics stat = Arrays.stream(salary).boxed().collect(Collectors.summarizingInt(i -> i));
        return (double)(stat.getSum() - stat.getMax() - stat.getMin()) / (stat.getCount() - 2);
    }
```


```python
    def average(self, salary: List[int]) -> float:
        return (sum(salary) - max(salary) - min(salary)) / (len(salary) - 2)
```
</p>


### Clean Python 3, one pass O(N)
- Author: lenchen1112
- Creation Date: Sun Jun 28 2020 00:02:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 00:02:42 GMT+0800 (Singapore Standard Time)

<p>
Time: `O(N)`
Space: `O(1)`
```
class Solution:
    def average(self, salary: List[int]) -> float:
        s, m, M = 0, float(\'inf\'), float(\'-inf\')
        for num in salary:
            s += num
            m, M = min(m, num), max(M, num)
        return (s - m - M) / (len(salary) - 2)
```
</p>


