---
title: "Palindromic Substrings"
weight: 579
#id: "palindromic-substrings"
---
## Description
<div class="description">
<p>Given a string, your task is to count how many palindromic substrings in this string.</p>

<p>The substrings with different start indexes or end indexes are counted as different substrings even they consist of same characters.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> &quot;abc&quot;
<b>Output:</b> 3
<b>Explanation:</b> Three palindromic strings: &quot;a&quot;, &quot;b&quot;, &quot;c&quot;.
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> &quot;aaa&quot;
<b>Output:</b> 6
<b>Explanation:</b> Six palindromic strings: &quot;a&quot;, &quot;a&quot;, &quot;a&quot;, &quot;aa&quot;, &quot;aa&quot;, &quot;aaa&quot;.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>The input string length won&#39;t exceed 1000.</li>
</ol>

<p>&nbsp;</p>
</div>

## Tags
- String (string)
- Dynamic Programming (dynamic-programming)

## Companies
- Goldman Sachs - 15 (taggedByAdmin: false)
- Facebook - 11 (taggedByAdmin: true)
- Apple - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Twitter - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Roblox - 2 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)
- Pure Storage - 14 (taggedByAdmin: false)
- Coursera - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Citrix - 2 (taggedByAdmin: false)
- LinkedIn - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Expand Around Center [Accepted]

**Intuition**

Let `N` be the length of the string.  The middle of the palindrome could be in one of `2N - 1` positions: either at letter or between two letters.

For each center, let's count all the palindromes that have this center.  Notice that if `[a, b]` is a palindromic interval (meaning `S[a], S[a+1], ..., S[b]` is a palindrome), then `[a+1, b-1]` is one too.

**Algorithm**

For each possible palindrome center, let's expand our candidate palindrome on the interval `[left, right]` as long as we can.  The condition for expanding is `left >= 0 and right < N and S[left] == S[right]`.  That means we want to count a new palindrome `S[left], S[left+1], ..., S[right]`.

<iframe src="https://leetcode.com/playground/WX9gV6sf/shared" frameBorder="0" width="100%" height="310" name="WX9gV6sf"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$ where $$N$$ is the length of `S`.  Each expansion might do $$O(N)$$ work.

* Space Complexity: $$O(1)$$.

---
#### Approach #2: Manacher's Algorithm [Accepted]

**Intuition**

Manacher's algorithm is a textbook algorithm that finds in linear time, the maximum size palindrome for any possible palindrome center.  If we had such an algorithm, finding the answer is straightforward.

What follows is a discussion of why this algorithm works.

**Algorithm**

Our loop invariants will be that `center, right` is our knowledge of the palindrome with the largest right-most boundary with `center < i`, centered at `center` with right-boundary `right`.  Also, `i > center`, and we've already computed all `Z[j]`'s for `j < i`.

When `i < right`, we reflect `i` about `center` to be at some coordinate `j = 2 * center - i`.  Then, limited to the interval with radius `right - i` and center `i`, the situation for `Z[i]` is the same as for `Z[j]`.

For example, if at some time `center = 7, right = 13, i = 10`, then for a string like `A = '@#A#B#A#A#B#A#＄'`, the `center` is at the `'#'` between the two middle `'A'`'s, the right boundary is at the last `'#'`, `i` is at the last `'B'`, and `j` is at the first `'B'`.

Notice that limited to the interval `[center - (right - center), right]` (the interval with center `center` and right-boundary `right`), the situation for `i` and `j` is a reflection of something we have already computed.  Since we already know `Z[j] = 3`, we can quickly find `Z[i] = min(right - i, Z[j]) = 3`.

Now, why is this algorithm linear?  The while loop only checks the condition more than once when `Z[i] = right - i`.  In that case, for each time `Z[i] += 1`, it increments `right`, and `right` can only be incremented up to `2*N+2` times.

Finally, we sum up `(v+1) / 2` for each `v in Z`.  Say the longest palindrome with some given center C has radius R.  Then, the substring with center C and radius R-1, R-2, R-3, ..., 0 are also palindromes.  Example:  `abcdedcba` is a palindrome with center `e`, radius 4:  but `e`, `ded`, `cdedc`, `bcdedcb`, and `abcdedcba` are all palindromes.

We are dividing by 2 because we were using half-lengths instead of lengths.  For example we actually had the palindrome `a#b#c#d#e#d#c#b#a`, so our length is twice as big.

<iframe src="https://leetcode.com/playground/GFqVCAyZ/shared" frameBorder="0" width="100%" height="500" name="GFqVCAyZ"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ where $$N$$ is the length of `S`.  As discussed above, the complexity is linear.

* Space Complexity: $$O(N)$$, the size of `A` and `Z`.

## Accepted Submission (python3)
```python3
#
# @lc app=leetcode id=647 lang=python3
#
# [647] Palindromic Substrings
#
class Solution:
    def countSubstrings(self, s: str) -> int:
        count = 0
        ls = len(s)
        for i in range(ls):
            # odd palindrome
            for j in range(min(i + 1, ls - i)):
                if s[i - j] == s[i + j]:
                    count += 1
                else:
                    break
            # even palindrome
            for j in range(min(i + 1, ls - i - 1)):
                if s[i - j] == s[i + 1 + j]:
                    count += 1
                else:
                    break
        return count

if __name__ == "__main__":
    sol = Solution()
    s = "abc"
    print(s, sol.countSubstrings(s))
    s = "aaa"
    print(s, sol.countSubstrings(s))


```

## Top Discussions
### Java solution, 8 lines, extendPalindrome
- Author: shawngao
- Creation Date: Sun Jul 23 2017 11:34:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 23:39:03 GMT+0800 (Singapore Standard Time)

<p>
Idea is start from each index and try to extend palindrome for both odd and even length.

```
public class Solution {
    int count = 0;
    
    public int countSubstrings(String s) {
        if (s == null || s.length() == 0) return 0;
        
        for (int i = 0; i < s.length(); i++) { // i is the mid point
            extendPalindrome(s, i, i); // odd length;
            extendPalindrome(s, i, i + 1); // even length
        }
        
        return count;
    }
    
    private void extendPalindrome(String s, int left, int right) {
        while (left >=0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
            count++; left--; right++;
        }
    }
}
```
</p>


### Very Simple Java Solution with Detail Explanation
- Author: keshavk
- Creation Date: Sun Jul 23 2017 14:01:47 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 15:31:40 GMT+0800 (Singapore Standard Time)

<p>
A very easy explanation with an example
Lets take a string "aabaa"

**Step 1:** Start a for loop to point at every single character from where we will trace the palindrome string.
checkPalindrome(s,i,i);   //To check the palindrome of odd length palindromic sub-string
checkPalindrome(s,i,i+1);    //To check the palindrome of even length palindromic sub-string

**Step 2:** From each character of the string, we will keep checking if the sub-string is a palindrome and increment the palindrome count. To check the palindrome, keep checking the left and right of the character if it is same or not.

**First Loop:**
![0_1500788783696_300147d3-e98e-4977-83f1-9eb8213a485e-image.png](/assets/uploads/files/1500788789821-300147d3-e98e-4977-83f1-9eb8213a485e-image.png) 
Palindrome: a  (Count=1)
![0_1500788808121_fec1dec5-ab5f-44cf-8dbd-eb2780e8d65f-image.png](/assets/uploads/files/1500788808273-fec1dec5-ab5f-44cf-8dbd-eb2780e8d65f-image.png) 
Palindrome: aa  (Count=2)

**Second Loop:**
![0_1500788845582_881440b8-6dde-4b6f-a864-24fef277069b-image.png](/assets/uploads/files/1500788845825-881440b8-6dde-4b6f-a864-24fef277069b-image.png) 
Palindrome: a   (Count=3)
![0_1500788872920_61fc20cb-0cb2-4179-8f5a-529cbad7a2ec-image.png](/assets/uploads/files/1500788872992-61fc20cb-0cb2-4179-8f5a-529cbad7a2ec-image.png) 
Palindrome: No Palindrome

**Third Loop:**
![0_1500788901120_bf12b13b-ff32-4703-86cf-0bcb54465428-image.png](/assets/uploads/files/1500788901208-bf12b13b-ff32-4703-86cf-0bcb54465428-image.png) 
Palindrome: b,aba,aabaa   (Count=6)
![0_1500788934388_5cc2c31d-404c-456a-a77d-1432bb0c679b-image.png](/assets/uploads/files/1500788934464-5cc2c31d-404c-456a-a77d-1432bb0c679b-image.png) 
Palindrome: No Palindrome

**Forth Loop:**
![0_1500788981884_a2d3f30e-0745-4a75-b2c0-940834bd6a84-image.png](/assets/uploads/files/1500788981974-a2d3f30e-0745-4a75-b2c0-940834bd6a84-image.png) 
Palindrome: a   (Count=7)
![0_1500789009429_f38aa5c2-17ac-47db-8fe9-b9bb4ceb1407-image.png](/assets/uploads/files/1500789009507-f38aa5c2-17ac-47db-8fe9-b9bb4ceb1407-image.png) 
Palindrome: aa   (Count=8)

Count = 9 (For the last character)

**Answer = 9** 

'''

 
    int count =1;
    public int countSubstrings(String s) {
        if(s.length()==0) 
            return 0;
        for(int i=0; i<s.length()-1; i++){
            checkPalindrome(s,i,i);     //To check the palindrome of odd length palindromic sub-string
            checkPalindrome(s,i,i+1);   //To check the palindrome of even length palindromic sub-string
        }
        return count;
    }    

    private void checkPalindrome(String s, int i, int j) {
        while(i>=0 && j<s.length() && s.charAt(i)==s.charAt(j)){    //Check for the palindrome string 
            count++;    //Increment the count if palindromin substring found
            i--;    //To trace string in left direction
            j++;    //To trace string in right direction
        }
    }


'''
</p>


### Java / Python DP solution based on longest palindromic substring
- Author: li-_-il
- Creation Date: Sun Jul 23 2017 11:34:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 03 2020 10:39:28 GMT+0800 (Singapore Standard Time)

<p>
This solution is almost same as the DP solution for longest palindromic substring, instead of storing the longest, just get the count of palindromic substrings.

recursion:

i ,i+1, ..........j-1,j 

is a palindrome only if i+1, ..........j-1is a palindrome and a[i] == a[j] 
For 1 element and 2 element cases  j-i+1 < 3, its enough to check just for a[i] == a[j] [where i == j for 1 element and j == i+1 for 2 elements]


The outer for loop has to start from i - 1 (the end) why ? because the recursion is looking forward, referring to i+1.
The inner forward can run normally, but since we are only interested in one half of the diagonal, we start at i, everytime.



```java
public int countSubstrings(String s) {
    int n = s.length();
    int res = 0;
    boolean[][] dp = new boolean[n][n];
    for (int i = n - 1; i >= 0; i--) {
        for (int j = i; j < n; j++) {
            dp[i][j] = s.charAt(i) == s.charAt(j) && (j - i  + 1 < 3 || dp[i + 1][j - 1]);
            if(dp[i][j]) ++res;
        }
    }
    return res;
}

```

Python:

```python
def countSubstrings(self, s: str) -> int:
        n = len(s)
        dp = [[0] * n for _ in range(n)]
        
        res = 0
        for i in range(n-1, -1, -1):
            for j in range(i, n):
                dp[i][j] = s[i] == s[j] and ((j-i+1) < 3 or dp[i+1][j-1])
                res += dp[i][j]
        return res
```
</p>


