---
title: "Range Sum Query - Mutable"
weight: 290
#id: "range-sum-query-mutable"
---
## Description
<div class="description">
<p>Given an integer array <i>nums</i>, find the sum of the elements between indices <i>i</i> and <i>j</i> (<i>i</i> &le; <i>j</i>), inclusive.</p>

<p>The <i>update(i, val)</i> function modifies <i>nums</i> by updating the element at index <i>i</i> to <i>val</i>.</p>

<p><b>Example:</b></p>

<pre>
Given nums = [1, 3, 5]

sumRange(0, 2) -&gt; 9
update(1, 2)
sumRange(0, 2) -&gt; 8
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The array is only modifiable by the <i>update</i> function.</li>
	<li>You may assume the number of calls to <i>update</i> and <i>sumRange</i> function is distributed evenly.</li>
	<li><code>0 &lt;= i &lt;= j &lt;= nums.length - 1</code></li>
</ul>

</div>

## Tags
- Binary Indexed Tree (binary-indexed-tree)
- Segment Tree (segment-tree)

## Companies
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Twitter - 4 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary
This article is for intermediate level readers. It introduces the following concepts:
Range sum query, Sqrt decomposition, Segment tree.

## Solution

---
#### Approach 1: Naive

**Algorithm**

A trivial solution for Range Sum Query - `RSQ(i, j)` is to iterate the array from index $$i$$ to $$j$$ and sum each element.


<iframe src="https://leetcode.com/playground/LxdPEC6m/shared" frameBorder="0" width="100%" height="276" name="LxdPEC6m"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$ - range sum query, $$O(1)$$ - update query

    For range sum query we access each element from the array for constant time and in the worst case we access $$n$$ elements. Therefore time complexity is $$O(n)$$. Time complexity of update query is $$O(1)$$.

* Space complexity : $$O(1)$$.
<br/>
<br/>

---
#### Approach 2: Sqrt Decomposition

**Intuition**

The idea is to  split the array in blocks with length of $$\sqrt{n}$$. Then we calculate the sum of each block and store it in auxiliary memory `b`.
To query `RSQ(i, j)`, we will add the sums of all the blocks lying inside and those that partially overlap with range $$[i \ldots j]$$.

**Algorithm**

![Range sum query using SQRT decomposition](../Figures/307/307_RSQ_Sqrt.png)
{:align="center"}

*Figure 1. Range sum query using SQRT decomposition.*
{:align="center"}

In the example above, the array `nums`'s length is `9`, which is split into blocks of size $$\sqrt{9}$$. To get `RSQ(1, 7)` we add `b[1]`.  It stores the sum of `range [3, 5]` and partially sums from `block 0`  and `block 2`, which are overlapping boundary blocks.


<iframe src="https://leetcode.com/playground/bxCiR2kw/shared" frameBorder="0" width="100%" height="500" name="bxCiR2kw"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$ - preprocessing, $$O(\sqrt{n})$$ - range sum query, $$O(1)$$ - update query

    For range sum query in the worst-case scenario we have to sum approximately $$3 \sqrt{n}$$ elements. In this case the range includes $$\sqrt{n} - 2$$ blocks, which total sum costs $$\sqrt{n} - 2$$ operations. In addition to this we have to add the sum of the two boundary blocks. This takes another $$2 (\sqrt{n} - 1)$$ operations. The total amount of operations is around $$3 \sqrt{n}$$.

* Space complexity : $$O(\sqrt{n})$$.

    We need additional $$\sqrt{n}$$ memory to store all block sums.
<br/>
<br/>

---
#### Approach 3: Segment Tree

**Algorithm**

Segment tree is a very flexible data structure, because it is used to solve numerous range query problems like finding minimum, maximum, sum, greatest common divisor, least common denominator in array in logarithmic time.

![Illustration of Segment Tree](../Figures/307/307_segment_tree.png)

*Figure 2. Illustration of Segment tree.*
{:align="center"}

The segment tree for array $$a[0, 1, \ldots ,n-1]$$ is a binary tree in which each node contains **aggregate** information (min, max, sum, etc.) for a subrange $$[i \ldots j]$$ of the array, as its left and right child hold information for range $$[i \ldots \frac{i+j}{2}]$$ and $$[\frac{i + j}{2} + 1, j]$$.

Segment tree could be implemented using either an array or a tree. For an array implementation, if the element at index $$i$$ is not a leaf, its left and right child are stored at index $$2i$$ and $$2i + 1$$ respectively.

In the example above (Figure 2), every leaf node contains the initial array elements `{2,4,5,7}`.
The internal nodes contain the sum of the corresponding elements in range, _e.g._ `(6)` is the sum for the elements from index 0 to index 1. The root `(18)` is the sum  of its children `(6)` and `(12)`, which also holds the total sum of the entire array.

Segment Tree can be broken down to the three following steps:

1. Pre-processing step which builds the segment tree from a given array.
2. Update the segment tree when an element is modified.
3. Calculate the Range Sum Query using the segment tree.

##### 1. Build segment tree

We will use a very effective bottom-up approach to build segment tree. We already know from the above that if some node $$p$$ holds the sum of $$[i \ldots j]$$ range, its left and right children hold the sum for range $$[i \ldots \frac{i + j}{2}]$$ and $$[\frac{i + j}{2} + 1, j]$$ respectively.

Therefore to find the sum of node $$p$$, we need to calculate the sum of its right and left child in advance.

We begin from the leaves, initialize them with input array elements $$a[0, 1, \ldots, n-1]$$. Then we move upward to the higher level to calculate the parents' sum till we get to the root of the segment tree.


<iframe src="https://leetcode.com/playground/FHnQhAAi/shared" frameBorder="0" width="100%" height="310" name="FHnQhAAi"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$

    Time complexity is  $$O(n)$$, because we calculate the sum of one node during each iteration of the for loop. There are approximately $$2n$$ nodes in a segment tree.

    This could be proved in the following way: Segmented tree for array with $$n$$ elements has $$n$$ leaves (the array elements itself). The number of nodes in each level is half the number in the level below.

    So if we sum the number by level we will get:

    $$
    n + n/2  + n/4 + n/8 + \ldots + 1 \approx 2n
    $$

* Space complexity : $$O(n)$$.

    We used $$2n$$ extra space to store the segment tree.

##### 2. Update segment tree

When we update the array at some index $$i$$ we need to rebuild the segment tree, because there are tree nodes which contain the sum of the modified element. Again we will use a bottom-up approach. We update the leaf node that stores $$a[i]$$. From there we will follow the path up to the root updating the value of each parent as a sum of its children values.


<iframe src="https://leetcode.com/playground/Vbdx6nEb/shared" frameBorder="0" width="100%" height="327" name="Vbdx6nEb"></iframe>

**Complexity Analysis**

* Time complexity : $$O(\log n)$$.

    Algorithm  has $$O(\log n)$$ time complexity, because there are a few tree nodes with range that include  $$i$$th array element, one on each level. There are $$\log(n)$$  levels.

* Space complexity : $$O(1)$$.

##### 3. Range Sum Query

We can find range sum query  $$[L, R]$$ using segment tree in the following way:

Algorithm hold loop invariant:

$$l \le r$$ and sum of $$[L \ldots l]$$ and $$[r \ldots R]$$ has been calculated, where $$l$$ and $$r$$ are the left and right boundary of calculated sum.
Initially we set $$l$$ with left leaf $$L$$ and $$r$$ with right leaf $$R$$.
Range $$[l, r]$$ shrinks on each iteration till range borders meets after approximately $$\log n$$ iterations of the algorithm

* Loop till $$l \le r$$

    * Check if $$l$$ is right child of its parent $$P$$

        * $$l$$ is right child of $$P$$. Then $$P$$ contains sum of range of $$l$$ and another  child which is outside the range $$[l, r]$$ and we don't need parent $$P$$ sum. Add $$l$$ to $$sum$$ without its parent $$P$$ and set $$l$$ to point to the right of $$P$$ on the upper level.

        * $$l$$ is not right child of $$P$$. Then parent $$P$$ contains sum of range which lies in $$[l, r]$$. Add $$P$$ to $$sum$$ and set $$l$$ to point to the parent of $$P$$

    * Check if $$r$$ is left child of its parent $$P$$

        * $$r$$ is left child of $$P$$. Then $$P$$ contains sum of range of $$r$$ and another  child which is outside the range $$[l, r]$$ and we don't need parent $$P$$ sum. Add $$r$$  to $$sum$$ without its parent $$P$$ and set $$r$$ to point to the left of $$P$$ on the upper level.

        * $$r$$ is not left child of $$P$$. Then parent $$P$$ contains sum of range which lies in $$[l, r]$$. Add $$P$$ to $$sum$$ and set $$r$$ to point to the parent of $$P$$


<iframe src="https://leetcode.com/playground/WE9ntW2e/shared" frameBorder="0" width="100%" height="395" name="WE9ntW2e"></iframe>

**Complexity Analysis**

* Time complexity : $$O(\log n)$$

    Time complexity is $$O(\log n)$$ because on each iteration of the algorithm we move one level up, either to the parent of the  current node or to the next sibling of parent to the left or right direction till the two boundaries meet. In the worst-case scenario this happens at the root after $$\log n$$ iterations of the algorithm.

* Space complexity : $$O(1)$$.


## Further Thoughts

The iterative version of Segment Trees was introduced in this article. A more intuitive, recursive version of Segment Trees to solve this problem is discussed [here](https://leetcode.com/articles/a-recursive-approach-to-segment-trees-range-sum-queries-lazy-propagation/). The concept of Lazy Propagation is also introduced there.

There is an alternative solution of the problem using Binary Indexed Tree. It is faster and simpler to code.
You can find it [here](https://leetcode.com/problems/range-sum-query-mutable/discuss/75753/Java-using-Binary-Indexed-Tree-with-clear-explanation).

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 17 ms Java solution with segment tree
- Author: 2guotou
- Creation Date: Wed Nov 18 2015 22:01:16 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 09:52:24 GMT+0800 (Singapore Standard Time)

<p>
    public class NumArray {
    
        class SegmentTreeNode {
            int start, end;
            SegmentTreeNode left, right;
            int sum;
    
            public SegmentTreeNode(int start, int end) {
                this.start = start;
                this.end = end;
                this.left = null;
                this.right = null;
                this.sum = 0;
            }
        }
          
        SegmentTreeNode root = null;
       
        public NumArray(int[] nums) {
            root = buildTree(nums, 0, nums.length-1);
        }
    
        private SegmentTreeNode buildTree(int[] nums, int start, int end) {
            if (start > end) {
                return null;
            } else {
                SegmentTreeNode ret = new SegmentTreeNode(start, end);
                if (start == end) {
                    ret.sum = nums[start];
                } else {
                    int mid = start  + (end - start) / 2;             
                    ret.left = buildTree(nums, start, mid);
                    ret.right = buildTree(nums, mid + 1, end);
                    ret.sum = ret.left.sum + ret.right.sum;
                }         
                return ret;
            }
        }
       
        void update(int i, int val) {
            update(root, i, val);
        }
       
        void update(SegmentTreeNode root, int pos, int val) {
            if (root.start == root.end) {
               root.sum = val;
            } else {
                int mid = root.start + (root.end - root.start) / 2;
                if (pos <= mid) {
                     update(root.left, pos, val);
                } else {
                     update(root.right, pos, val);
                }
                root.sum = root.left.sum + root.right.sum;
            }
        }
    
        public int sumRange(int i, int j) {
            return sumRange(root, i, j);
        }
        
        public int sumRange(SegmentTreeNode root, int start, int end) {
            if (root.end == end && root.start == start) {
                return root.sum;
            } else {
                int mid = root.start + (root.end - root.start) / 2;
                if (end <= mid) {
                    return sumRange(root.left, start, end);
                } else if (start >= mid+1) {
                    return sumRange(root.right, start, end);
                }  else {    
                    return sumRange(root.right, mid+1, end) + sumRange(root.left, start, mid);
                }
            }
        }
    }
</p>


### Java using Binary Indexed Tree with clear explanation
- Author: rikimberley
- Creation Date: Sun Dec 13 2015 08:16:47 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 04:32:06 GMT+0800 (Singapore Standard Time)

<p>
This is to share the explanation of the BIT and the meaning of the bit operations.

    public class NumArray {
		/**
		 * Binary Indexed Trees (BIT or Fenwick tree):
		 * https://www.topcoder.com/community/data-science/data-science-
		 * tutorials/binary-indexed-trees/
		 * 
		 * Example: given an array a[0]...a[7], we use a array BIT[9] to
		 * represent a tree, where index [2] is the parent of [1] and [3], [6]
		 * is the parent of [5] and [7], [4] is the parent of [2] and [6], and
		 * [8] is the parent of [4]. I.e.,
		 * 
		 * BIT[] as a binary tree:
		 *            ______________*
		 *            ______*
		 *            __*     __*
		 *            *   *   *   *
		 * indices: 0 1 2 3 4 5 6 7 8
		 * 
		 * BIT[i] = ([i] is a left child) ? the partial sum from its left most
		 * descendant to itself : the partial sum from its parent (exclusive) to
		 * itself. (check the range of "__").
		 * 
		 * Eg. BIT[1]=a[0], BIT[2]=a[1]+BIT[1]=a[1]+a[0], BIT[3]=a[2],
		 * BIT[4]=a[3]+BIT[3]+BIT[2]=a[3]+a[2]+a[1]+a[0],
		 * BIT[6]=a[5]+BIT[5]=a[5]+a[4],
		 * BIT[8]=a[7]+BIT[7]+BIT[6]+BIT[4]=a[7]+a[6]+...+a[0], ...
		 * 
		 * Thus, to update a[1]=BIT[2], we shall update BIT[2], BIT[4], BIT[8],
		 * i.e., for current [i], the next update [j] is j=i+(i&-i) //double the
		 * last 1-bit from [i].
		 * 
		 * Similarly, to get the partial sum up to a[6]=BIT[7], we shall get the
		 * sum of BIT[7], BIT[6], BIT[4], i.e., for current [i], the next
		 * summand [j] is j=i-(i&-i) // delete the last 1-bit from [i].
		 * 
		 * To obtain the original value of a[7] (corresponding to index [8] of
		 * BIT), we have to subtract BIT[7], BIT[6], BIT[4] from BIT[8], i.e.,
		 * starting from [idx-1], for current [i], the next subtrahend [j] is
		 * j=i-(i&-i), up to j==idx-(idx&-idx) exclusive. (However, a quicker
		 * way but using extra space is to store the original array.)
		 */

		int[] nums;
		int[] BIT;
		int n;

		public NumArray(int[] nums) {
			this.nums = nums;

			n = nums.length;
			BIT = new int[n + 1];
			for (int i = 0; i < n; i++)
				init(i, nums[i]);
		}

		public void init(int i, int val) {
			i++;
			while (i <= n) {
				BIT[i] += val;
				i += (i & -i);
			}
		}

		void update(int i, int val) {
			int diff = val - nums[i];
			nums[i] = val;
			init(i, diff);
		}

		public int getSum(int i) {
			int sum = 0;
			i++;
			while (i > 0) {
				sum += BIT[i];
				i -= (i & -i);
			}
			return sum;
		}

		public int sumRange(int i, int j) {
			return getSum(j) - getSum(i - 1);
		}
	}

	// Your NumArray object will be instantiated and called as such:
	// NumArray numArray = new NumArray(nums);
	// numArray.sumRange(0, 1);
	// numArray.update(1, 10);
	// numArray.sumRange(1, 2);
</p>


### Python: Well commented solution using Segment Trees
- Author: tejask91
- Creation Date: Sat May 14 2016 10:02:13 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 20:35:43 GMT+0800 (Singapore Standard Time)

<p>
This solution is based on the top voted solution by  2guotou, which is in Java.

    """
        The idea here is to build a segment tree. Each node stores the left and right
        endpoint of an interval and the sum of that interval. All of the leaves will store
        elements of the array and each internal node will store sum of leaves under it.
        Creating the tree takes O(n) time. Query and updates are both O(log n).
    """
    
    #Segment tree node
    class Node(object):
        def __init__(self, start, end):
            self.start = start
            self.end = end
            self.total = 0
            self.left = None
            self.right = None
            
    
    class NumArray(object):
        def __init__(self, nums):
            """
            initialize your data structure here.
            :type nums: List[int]
            """
            #helper function to create the tree from input array
            def createTree(nums, l, r):
                
                #base case
                if l > r:
                    return None
                    
                #leaf node
                if l == r:
                    n = Node(l, r)
                    n.total = nums[l]
                    return n
                
                mid = (l + r) // 2
                
                root = Node(l, r)
                
                #recursively build the Segment tree
                root.left = createTree(nums, l, mid)
                root.right = createTree(nums, mid+1, r)
                
                #Total stores the sum of all leaves under root
                #i.e. those elements lying between (start, end)
                root.total = root.left.total + root.right.total
                    
                return root
            
            self.root = createTree(nums, 0, len(nums)-1)
                
        def update(self, i, val):
            """
            :type i: int
            :type val: int
            :rtype: int
            """
            #Helper function to update a value
            def updateVal(root, i, val):
                
                #Base case. The actual value will be updated in a leaf.
                #The total is then propogated upwards
                if root.start == root.end:
                    root.total = val
                    return val
            
                mid = (root.start + root.end) // 2
                
                #If the index is less than the mid, that leaf must be in the left subtree
                if i <= mid:
                    updateVal(root.left, i, val)
                    
                #Otherwise, the right subtree
                else:
                    updateVal(root.right, i, val)
                
                #Propogate the changes after recursive call returns
                root.total = root.left.total + root.right.total
                
                return root.total
            
            return updateVal(self.root, i, val)
    
        def sumRange(self, i, j):
            """
            sum of elements nums[i..j], inclusive.
            :type i: int
            :type j: int
            :rtype: int
            """
            #Helper function to calculate range sum
            def rangeSum(root, i, j):
                
                #If the range exactly matches the root, we already have the sum
                if root.start == i and root.end == j:
                    return root.total
                
                mid = (root.start + root.end) // 2
                
                #If end of the range is less than the mid, the entire interval lies
                #in the left subtree
                if j <= mid:
                    return rangeSum(root.left, i, j)
                
                #If start of the interval is greater than mid, the entire inteval lies
                #in the right subtree
                elif i >= mid + 1:
                    return rangeSum(root.right, i, j)
                
                #Otherwise, the interval is split. So we calculate the sum recursively,
                #by splitting the interval
                else:
                    return rangeSum(root.left, i, mid) + rangeSum(root.right, mid+1, j)
            
            return rangeSum(self.root, i, j)
                    
    
    
    # Your NumArray object will be instantiated and called as such:
    # numArray = NumArray(nums)
    # numArray.sumRange(0, 1)
    # numArray.update(1, 10)
    # numArray.sumRange(1, 2)
</p>


