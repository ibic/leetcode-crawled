---
title: "Rearrange String k Distance Apart"
weight: 341
#id: "rearrange-string-k-distance-apart"
---
## Description
<div class="description">
<p>Given a non-empty string <b>s</b> and an integer <b>k</b>, rearrange the string such that the same characters are at least distance <b>k</b> from each other.</p>

<p>All input strings are given in lowercase letters. If it is not possible to rearrange the string, return an empty string <code>&quot;&quot;</code>.</p>

<p><strong>Example 1:</strong></p>

<div>
<pre>
<strong>Input: </strong>s = <span id="example-input-1-1">&quot;aabbcc&quot;</span>, k = <span id="example-input-1-2">3</span>
<strong>Output: </strong><span id="example-output-1">&quot;abcabc&quot; 
<strong>Explanation: </strong></span>The same letters are at least distance 3 from each other.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>s = <span id="example-input-2-1">&quot;aaabc&quot;</span>, k = <span id="example-input-2-2">3</span>
<strong>Output: </strong><span id="example-output-2">&quot;&quot; 
<strong>Explanation:</strong> </span>It is not possible to rearrange the string.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>s = <span id="example-input-3-1">&quot;aaadbbcc&quot;</span>, k = <span id="example-input-3-2">2</span>
<strong>Output: </strong><span id="example-output-3">&quot;abacabcd&quot;
</span><span id="example-output-2"><strong>Explanation:</strong> </span>The same letters are at least distance 2 from each other.
</pre>
</div>
</div>
</div>
</div>

## Tags
- Hash Table (hash-table)
- Heap (heap)
- Greedy (greedy)

## Companies
- Microsoft - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 7 version of PriorityQueue O(nlogn) with comments and explanations
- Author: xuyirui
- Creation Date: Tue Jun 14 2016 11:13:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 13:49:04 GMT+0800 (Singapore Standard Time)

<p>
The greedy algorithm is that in each step, select the char with highest remaining count if possible (if it is not in the waiting queue). PQ is used to achieve the greedy. A regular queue waitQueue is used to "freeze" previous appeared char in the period of k.

In each iteration, we need to add current char to the waitQueue and also release the char at front of the queue, put back to maxHeap. The "impossible" case happens when the maxHeap is empty but there is still some char in the waitQueue.


    public class Solution {
        public String rearrangeString(String str, int k) {
            
            StringBuilder rearranged = new StringBuilder();
            //count frequency of each char
            Map<Character, Integer> map = new HashMap<>();
            for (char c : str.toCharArray()) {
                if (!map.containsKey(c)) {
                    map.put(c, 0);
                }
                map.put(c, map.get(c) + 1);
            }
            
            //construct a max heap using self-defined comparator, which holds all Map entries, Java is quite verbose
            Queue<Map.Entry<Character, Integer>> maxHeap = new PriorityQueue<>(new Comparator<Map.Entry<Character, Integer>>() {
                public int compare(Map.Entry<Character, Integer> entry1, Map.Entry<Character, Integer> entry2) {
                    return entry2.getValue() - entry1.getValue();
                }
            });
            
            Queue<Map.Entry<Character, Integer>> waitQueue = new LinkedList<>();
            maxHeap.addAll(map.entrySet());
            
            while (!maxHeap.isEmpty()) {
                
                Map.Entry<Character, Integer> current = maxHeap.poll();
                rearranged.append(current.getKey());
                current.setValue(current.getValue() - 1);
                waitQueue.offer(current);
                
                if (waitQueue.size() < k) { // intial k-1 chars, waitQueue not full yet
                    continue;
                }
                // release from waitQueue if char is already k apart
                Map.Entry<Character, Integer> front = waitQueue.poll();
                //note that char with 0 count still needs to be placed in waitQueue as a place holder
                if (front.getValue() > 0) {
                    maxHeap.offer(front);
                }
            }
            
            return rearranged.length() == str.length() ? rearranged.toString() : "";
        }
        
    }
</p>


### Java 15ms Solution with Two auxiliary array. O(N) time.
- Author: OrlandoChen0308
- Creation Date: Thu Jun 16 2016 02:30:04 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 08:49:01 GMT+0800 (Singapore Standard Time)

<p>
This is a greedy problem.  
Every time we want to find the best candidate: which is the character with the largest remaining count. Thus we will be having two arrays.   
One count array to store the remaining count of every character. Another array to keep track of the most left position that one character can appear.
We will iterated through these two array to find the best candidate for every position. Since the array is fixed size, it will take constant time to do this.  
After we find the candidate, we update two arrays.

    public class Solution {
        public String rearrangeString(String str, int k) {
            int length = str.length();
            int[] count = new int[26];
            int[] valid = new int[26];
            for(int i=0;i<length;i++){
                count[str.charAt(i)-'a']++;
            }
            StringBuilder sb = new StringBuilder();
            for(int index = 0;index<length;index++){
                int candidatePos = findValidMax(count, valid, index);
                if( candidatePos == -1) return "";
                count[candidatePos]--;
                valid[candidatePos] = index+k;
                sb.append((char)('a'+candidatePos));
            }
            return sb.toString();
        }
        
       private int findValidMax(int[] count, int[] valid, int index){
           int max = Integer.MIN_VALUE;
           int candidatePos = -1;
           for(int i=0;i<count.length;i++){
               if(count[i]>0 && count[i]>max && index>=valid[i]){
                   max = count[i];
                   candidatePos = i;
               }
           }
           return candidatePos;
       }
    }

At first I was considering using PriorityQueue and referring to this post:  
[c++ unordered_map priority_queue solution using cache][1]  
I have doubts for this solution. If we have "abba", k=2; It seems we might end up with "abba" as the result. Since in the second while loop, I'm not sure 'a' or 'b' will be polled out first.  
In Java, when two keys in PriorityQueue have same value, there is no guarantee on the order poll() will return.  But I'm not sure how heap is implemented in C++.


  [1]: https://leetcode.com/discuss/108174/c-unordered_map-priority_queue-solution-using-cache
</p>


### C++ unordered_map priority_queue solution using cache
- Author: sxycwzwzq
- Creation Date: Tue Jun 14 2016 07:16:15 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 06:29:11 GMT+0800 (Singapore Standard Time)

<p>
key point: using cache during processing heap data.

new version:

    class Solution {
    public:
        string rearrangeString(string str, int k) {
            if(k == 0) return str;
            int length = (int)str.size(); 
            
            string res;
            unordered_map<char, int> dict;
            priority_queue<pair<int, char>> pq;
            
            for(char ch : str) dict[ch]++;
            for(auto it = dict.begin(); it != dict.end(); it++){
                pq.push(make_pair(it->second, it->first));
            }
            
            while(!pq.empty()){
                vector<pair<int, char>> cache; //store used char during one while loop
                int count = min(k, length); //count: how many steps in a while loop
                for(int i = 0; i < count; i++){
                    if(pq.empty()) return "";
                    auto tmp = pq.top();
                    pq.pop();
                    res.push_back(tmp.second);
                    if(--tmp.first > 0) cache.push_back(tmp);
                    length--;
                }
                for(auto p : cache) pq.push(p);
            }
            return res;
        }
    };

old version:

    class Solution {
        struct mycompare{
            bool operator()(pair<int, char>& p1, pair<int, char>& p2){
                if(p1.first == p2.first) return p1.second > p2.second;
                return p1.first < p2.first;
            }
        };
    public:
        string rearrangeString(string str, int k) {
            if(k == 0) return str;
            unordered_map<char, int> dict;
            for(char ch : str) dict[ch]++;
            int left = (int)str.size();
            priority_queue<pair<int, char>, vector<pair<int, char>>, mycompare > pq;
            for(auto it = dict.begin(); it != dict.end(); it++){
                pq.push(make_pair(it->second, it->first));
            }
            string res;
            
            while(!pq.empty()){
                vector<pair<int, char>> cache;
                int count = min(k, left);
                for(int i = 0; i < count; i++){
                    if(pq.empty()) return "";
                    auto tmp = pq.top();
                    pq.pop();
                    res.push_back(tmp.second);
                    if(--tmp.first > 0) cache.push_back(tmp);
                    left--;
                }
                for(auto p : cache){
                    pq.push(p);
                }
            }
            return res;
        }
    };
</p>


