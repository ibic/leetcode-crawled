---
title: "Second Highest Salary"
weight: 1471
#id: "second-highest-salary"
---
## Description
<div class="description">
<p>Write a SQL query to get the second highest salary from the <code>Employee</code> table.</p>

<pre>
+----+--------+
| Id | Salary |
+----+--------+
| 1  | 100    |
| 2  | 200    |
| 3  | 300    |
+----+--------+
</pre>

<p>For example, given the above Employee table, the query should return <code>200</code> as the second highest salary. If there is no second highest salary, then the query should return <code>null</code>.</p>

<pre>
+---------------------+
| SecondHighestSalary |
+---------------------+
| 200                 |
+---------------------+
</pre>

</div>

## Tags


## Companies
- Amazon - 8 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Walmart Labs - 4 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using sub-query and `LIMIT` clause [Accepted]

**Algorithm**

Sort the distinct salary in descend order and then utilize the [`LIMIT`](https://dev.mysql.com/doc/refman/5.7/en/select.html) clause to get the second highest salary.

```sql
SELECT DISTINCT
    Salary AS SecondHighestSalary
FROM
    Employee
ORDER BY Salary DESC
LIMIT 1 OFFSET 1
```

However, this solution will be judged as 'Wrong Answer' if there is no such second highest salary since there might be only one record in this table. To overcome this issue, we can take this as a temp table.

**MySQL**

```sql
SELECT
    (SELECT DISTINCT
            Salary
        FROM
            Employee
        ORDER BY Salary DESC
        LIMIT 1 OFFSET 1) AS SecondHighestSalary
;
```

#### Approach: Using `IFNULL` and `LIMIT` clause [Accepted]

Another way to solve the 'NULL' problem is to use `IFNULL` funtion as below.

**MySQL**
```sql
SELECT
    IFNULL(
      (SELECT DISTINCT Salary
       FROM Employee
       ORDER BY Salary DESC
        LIMIT 1 OFFSET 1),
    NULL) AS SecondHighestSalary
```

## Accepted Submission (mysql)
```mysql
# Write your MySQL query statement below
select if((select count(*) from Employee) >= 2,
          (select distinct Salary from Employee order by Salary desc limit 1 offset 1),
          null
) as SecondHighestSalary
```

## Top Discussions
### Simple query which handles the NULL situation
- Author: lookbackinanger
- Creation Date: Thu Jan 15 2015 01:20:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 07:18:26 GMT+0800 (Singapore Standard Time)

<p>
SELECT max(Salary)
FROM Employee
WHERE Salary < (SELECT max(Salary) FROM Employee)

Using max() will return a NULL if the value doesn't exist. So there is no need to UNION a NULL. Of course, if the second highest value is guaranteed to exist, using LIMIT 1,1 will be the best answer.
</p>


### A Simple Answer
- Author: bolder9
- Creation Date: Fri Oct 23 2015 12:51:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 22:34:43 GMT+0800 (Singapore Standard Time)

<p>

    select (
      select distinct Salary from Employee order by Salary Desc limit 1 offset 1
    )as second

Change the number after 'offset' gives u n-th highest salary
</p>


### Accepted solution
- Author: wei71
- Creation Date: Mon Feb 23 2015 10:31:39 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 23 2015 10:31:39 GMT+0800 (Singapore Standard Time)

<p>
Select MAX(Salary) from Employee
where Salary < (Select MAX(Salary) from Employee)
</p>


