---
title: "Patching Array"
weight: 313
#id: "patching-array"
---
## Description
<div class="description">
<p>Given a sorted positive integer array <i>nums</i> and an integer <i>n</i>, add/patch elements to the array such that any number in range <code>[1, n]</code> inclusive can be formed by the sum of some elements in the array. Return the minimum number of patches required.</p>

<p><b>Example 1:</b></p>

<pre>
<strong>Input: </strong><i>nums</i> = <code>[1,3]</code>, <i>n</i> = <code>6</code>
<strong>Output: </strong>1 
<strong>Explanation:</strong>
Combinations of <i>nums</i> are <code>[1], [3], [1,3]</code>, which form possible sums of: <code>1, 3, 4</code>.
Now if we add/patch <code>2</code> to <i>nums</i>, the combinations are: <code>[1], [2], [3], [1,3], [2,3], [1,2,3]</code>.
Possible sums are <code>1, 2, 3, 4, 5, 6</code>, which now covers the range <code>[1, 6]</code>.
So we only need <code>1</code> patch.</pre>

<p><b>Example 2:</b></p>

<pre>
<strong>Input: </strong><i>nums</i> = <code>[1,5,10]</code>, <i>n</i> = <code>20</code>
<strong>Output:</strong> 2
<strong>Explanation: </strong>The two patches can be <code>[2, 4]</code>.
</pre>

<p><b>Example 3:</b></p>

<pre>
<strong>Input: </strong><i>nums</i> = <code>[1,2,2]</code>, <i>n</i> = <code>5</code>
<strong>Output:</strong> 0
</pre>
</div>

## Tags
- Greedy (greedy)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
## Solution

**Intuition**

For any missing number, if we want to cover it, we have to add at least one number smaller or equal to that number. Otherwise, it will not be covered.
Imagine you want to give someone change of $$x$$ cents, and you don't have enough coins. You will need to ask for coin less than or equal to $$x$$.

**Algorithm**

Suppose `miss` is the smallest missing number, then we know that `[1, miss)` (left-closed, right-open) is already covered . In order to cover `miss`, we have to add something smaller than or equal to `miss`. Otherwise, there is no way we can cover it.

For example, you have any array `nums = [1,2,3,8]` and `n = 16`. The numbers already covered is in the ranges `[1, 6]` and `[8, 14]`. In other words, `7, 15, 16` are missing. If you add patches larger than `7`, then `7` is still missing.

Suppose the number we added is $$x$$ then, the ranges `[1, miss)` and `[x, x + miss)` are both covered. And since we know that `x <= miss`, the two ranges will cover the range `[1, x + miss)`. We want to choose $$x$$ as large as possible so that the range can cover as large as possible. Therefore, the best option is `x = miss`.

After we covered `miss`, we can recalculate the coverage and see what's the new smallest missing number. We then patch that number. We do this repeatedly until no missing number.  

Here is the recipe of this greedy algorithm:

* Initialize the range `[1, miss)` = `[1, 1)` = empty
* While n is not covered yet
  * if the current element `nums[i]` is less than or equal to `miss`
    * extends the range to `[1, miss + nums[i])`
    * increase `i` by 1
  * otherwise
    * patch the array with `miss`, extends the range to `[1, miss + miss)`
    * increase the number of patches
* Return the number of patches  

**Example:**

`nums = [1,2,3,8]` and `n = 80`

| iteration | `miss` | covered range | `i` | `nums[i]` | patches | comment  |
|:---------:|:------:|:-------------:|:-:|:-------:|:-------:|----------|
|     0     |    1   |     [1, 1)    | 0 |    1    |    0    |          |
|     1     |    2   |     [1, 2)    | 1 |    2    |    0    |          |
|     2     |    4   |     [1, 4)    | 2 |    3    |    0    |          |
|     3     |    7   |     [1, 7)    | 3 |    8    |    0    |          |
|     4     |   14   |    [1, 14)    | 3 |    8    |    1    | patch 7  |
|     5     |   22   |    [1, 22)    | 4 |   none  |    1    |          |
|     6     |   44   |    [1, 44)    | 4 |   none  |    2    | patch 22 |
|     7     |   88   |    [1, 88)    | 4 |   none  |    3    | patch 44 |


**Correctness**

One may ask how do you know this is correct? In this section, we will prove that the greedy algorithm always gives the smallest possible number of patches:

*Lemma*

> If the greedy algorithm needs to patch $$k$$ numbers to cover $$[1, n]$$, it is impossible to patch less than $$k$$ numbers to do the same.

*Proof by contradiction*

 For a given number $$n$$ and array $$\mathrm{nums}$$, suppose the $$k$$ patches found by greedy algorithm is $$X_1 \lt X_2 \lt \ldots \lt X_k \leq n$$. If there is another set of patches $$Y_1 \leq Y_2 \leq \ldots \leq Y_{k'} \leq n$$, with $$k' \lt k$$, then we have $$Y_1 \leq X_1$$, otherwise $$X_1$$ is not covered. And since adding $$X_1$$ cannot cover $$X_2$$ which means the sum of all the elements including $$X_1$$ is smaller than $$X_2$$. Thus adding $$Y_1$$ will also not cover $$X_2$$. And so we have:

$$
Y_2 \leq X_2
$$

otherwise $$X_2$$ will not be covered and so on so forth.

$$
Y_i \leq X_i, i = 1, 2, \ldots k'
$$

Finally, we can see that since $$X_1, X_2, \ldots X_{k'}$$ is not enough to cover $$X_k$$, therefore $$Y_1, Y_2, \ldots, Y_{k'}$$ is also not enough to cover $$X_k \leq n$$. This contradicts the fact that $$Y_1 \leq Y_2 \leq \ldots \leq Y_{k'} \leq n$$ covers $$[1, n]$$. **This completes the proof.**



Thus, the greedy algorithm will always return the fewest patches possible. Even through for particular cases, there could be many different ways to patch. But none of them will have fewer patches than the greedy algorithm does.



<iframe src="https://leetcode.com/playground/mWaJh5cz/shared" frameBorder="0" name="mWaJh5cz" width="100%" height="309"></iframe>

**Complexity Analysis**

* Time complexity : $$O(m + \log n)$$. In each iteration, we either increase the index `i` or we double the variable `miss`. The total number of increment for index `i` is $$m$$ and the total number of doubling `miss` is $$\log n$$

* Space complexity : $$O(1)$$. We only need three variables, `patches`, `i` and `miss`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Solution + explanation
- Author: StefanPochmann
- Creation Date: Wed Jan 27 2016 13:07:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 08:57:14 GMT+0800 (Singapore Standard Time)

<p>
**Solution**

    int minPatches(vector<int>& nums, int n) {
        long miss = 1, added = 0, i = 0;
        while (miss <= n) {
            if (i < nums.size() && nums[i] <= miss) {
                miss += nums[i++];
            } else {
                miss += miss;
                added++;
            }
        }
        return added;
    }

---

**Explanation**

Let `miss` be the smallest sum in `[0,n]` that we might be missing. Meaning we already know we can build all sums in `[0,miss)`. Then if we have a number `num <= miss` in the given array, we can add it to those smaller sums to build all sums in `[0,miss+num)`. If we don't, then we must add such a number to the array, and it's best to add `miss` itself, to maximize the reach.

---

**Example:** Let's say the input is `nums = [1, 2, 4, 13, 43]` and `n = 100`. We need to ensure that all sums in the range [1,100] are possible.

Using the given numbers 1, 2 and 4, we can already build all sums from 0 to 7, i.e., the range [0,8). But we can't build the sum 8, and the next given number (13) is too large. So we insert 8 into the array. Then we can build all sums in [0,16).

Do we need to insert 16 into the array? No! We can already build the sum 3, and adding the given 13 gives us sum 16. We can also add the 13 to the other sums, extending our range to [0,29).

And so on. The given 43 is too large to help with sum 29, so we must insert 29 into our array. This extends our range to [0,58). But then the 43 becomes useful and expands our range to [0,101). At which point we're done.

---

**Another implementation**, though I prefer the above one.

    int minPatches(vector<int>& nums, int n) {
        int count = 0, i = 0;
        for (long miss=1; miss <= n; count++)
            miss += (i < nums.size() && nums[i] <= miss) ? nums[i++] : miss;
        return count - i;
    }
</p>


### Share my thinking process
- Author: bravejia
- Creation Date: Fri Jan 29 2016 11:52:47 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 11:55:13 GMT+0800 (Singapore Standard Time)

<p>
The question asked for the "**minimum** number of patches required". In other words, it asked for an optimal solution. Lots of problems involving optimal solution can be solved by dynamic programming and/or greedy algorithm. I started with greedy algorithm which is conceptually easy to design. Typically, a greedy algorithm needs selection of best moves for a subproblem. So what is our best move? 

Think about this example: nums = [1, 2, 3, 9].  We naturally want to iterate through nums from left to right and see what we would discover. After we encountered 1, we know 1...1 is patched completely. After encountered 2, we know 1...3 (1+2) is patched completely. After we encountered 3, we know 1...6 (1+2+3) is patched completely. After we encountered 9, the smallest number we can get is 9. So we must patch a new number here so that we don't miss 7, 8. To have 7, the numbers we can patch is 1, 2, 3 ... 7. Any number greater than 7 won't help here. Patching  8 will not help you get 7. So we have 7 numbers (1...7) to choose from. I hope you can see number 7 works best here because if we chose number 7, we can move all the way up to 1+2+3+7 = 13. (1...13 is patched completely) and it makes us reach n as quickly as possible. After we patched 7 and reach 13, we can consider last element 9 in nums. Having 9 makes us reach 13+9 = 22, which means 1...22 is completely patched. If we still did't reach n, we can then patch 23, which makes 1...45 (22+23) completely patched. We continue until we reach n.
</p>


### C++, 8ms, greedy solution with explanation
- Author: Dragon.PW
- Creation Date: Sun May 15 2016 01:47:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 10:35:01 GMT+0800 (Singapore Standard Time)

<p>
show the algorithm with an example,

let nums=[1 2 5 6 20], n = 50.

Initial value: with 0 nums, we can only get 0 maximumly.

Then we need to get 1, since nums[0]=1, then we can get 1 using [1]. now the maximum number we can get is 1. (actually, we can get all number no greater than the maximum number)

    number used [1], number added []
    can achieve 1~1

Then we need to get 2 (maximum number +1). Since nums[1]=2, we can get 2. Now we can get all number between 1 ~ 3 (3=previous maximum value + the new number 2). and 3 is current maximum number we can get.

    number used [1 2], number added []
    can achieve 1~3

Then we need to get 4 (3+1). Since nums[2]=5>4; we need to add a new number to get 4. The optimal solution is to add 4 directly. In this case, we could achieve maximumly 7, using [1,2,4].

    number used [1 2], number added [4]
    can achieve 1~7

Then we need to get 8 (7+1). Since nums[2]=5<8, we can first try to use 5. Now the maximum number we can get is 7+5=12. Since 12>8, we successfully get 8.

    number used [1 2 5], number added [4]
    can achieve 1~12

Then we need to get 13 (12+1), Since nums[3]=6<13, we can first try to use 6. Now the maximum number we can get is 12+6=18. Since 18>13, we successfully get 13.

    number used [1 2 5 6], number added [4]
    can achieve 1~18

Then we need to get 19 (18+1), Since nums[4]=20>19, we need to add a new number to get 19. The optimal solution is to add 19 directly. In this case, we could achieve maximumly 37.

    number used [1 2 5 6], number added [4 19]
    can achieve 1~37

Then we need to get 38(37+1), Since nums[4]=20<38, we can first try to use 20. Now the maximum number we can get is 37+20=57. Since 57>38, we successfully get 38.

    number used [1 2 5 6 20], number added [4 19]
    can achieve 1~57

Since 57>n=50, we can all number no greater than 50. 

The extra number we added are 4 and 19,  so we return 2.


The code is given as follows

    class Solution {
    public:
    int minPatches(vector<int>& nums, int n) {
        int cnt=0,i=0;
        long long maxNum=0;
        while (maxNum<n){
           if (i<nums.size() && nums[i]<=maxNum+1)
                maxNum+=nums[i++];
           else{
                maxNum+=maxNum+1;cnt++;
           }
       }
       return cnt;
    }
    };
</p>


