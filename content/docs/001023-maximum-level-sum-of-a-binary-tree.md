---
title: "Maximum Level Sum of a Binary Tree"
weight: 1023
#id: "maximum-level-sum-of-a-binary-tree"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a binary tree, the level of its root is <code>1</code>,&nbsp;the level of its children is <code>2</code>,&nbsp;and so on.</p>

<p>Return the <strong>smallest</strong> level <code>X</code> such that the sum of all the values of nodes at level <code>X</code> is <strong>maximal</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/05/03/capture.JPG" style="width: 200px; height: 175px;" />
<pre>
<strong>Input:</strong> root = [1,7,0,7,-8,null,null]
<strong>Output:</strong> 2
<strong>Explanation: </strong>
Level 1 sum = 1.
Level 2 sum = 7 + 0 = 7.
Level 3 sum = 7 + -8 = -1.
So we return the level with the maximum sum which is level 2.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = [989,null,10250,98693,-89388,null,null,null,-32127]
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is in the range <code>[1, 10<sup>4</sup>]</code>.</li>
	<li><code>-10<sup>5</sup> &lt;= Node.val &lt;=&nbsp;10<sup>5</sup></code></li>
</ul>

</div>

## Tags
- Tree (tree)
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

There are two general strategies to traverse a tree:
     
- *Depth First Search* (`DFS`)

    In this strategy, `depth` is adopted as the priority, so that one
    would start from a root and reach all the way down to certain leaf,
    and then back to root to reach another branch.

    The DFS strategy can further be distinguished as
    `preorder`, `inorder`, and `postorder` depending on the relative order
    among the root node, left node and right node.
    
    DFS traversals could be implemented in three possible ways: `recursion`,
    `iteration` and `Morris`. The simplest one is to use a recursion.
    
![postorder](../Figures/1161/dfs2.png) 
    
- *Breadth First Search* (`BFS`)

    In this strategy, he tree is scanned `level by level`, 
    following the order of height,
    from top to bottom. The nodes on higher level would be visited before
    the ones with lower levels. 
    
    The standard implementation is `iteration with queue`.
    
On the following figure the nodes are enumerated in the order you visit them,
please follow `1-2-3-4-5` to compare different strategies.

![postorder](../Figures/1161/bfs_dfs2.png)

The current problem could be solved with both strategies: DFS and BFS.
Recently we discussed DFS implementations quite a lot, here are several 
problems: [Recover BST](https://leetcode.com/articles/recover-binary-search-tree/), 
[Construct Binary Tree from Inorder and Postorder Traversals](https://leetcode.com/articles/construct-binary-tree-from-inorder-and-postorder-t/), 
[Kth Smallest Element in BST](https://leetcode.com/articles/kth-smallest-element-in-a-bst/). 

So let's implement here very simple recursive inorder DFS 
and then profit this problem to discuss BFS iterative traversal 
with queue in more details.
<br /> 
<br />


---
#### Approach 1: DFS : Recursive Inorder Traversal 

Recursive inorder traversal is extremely simple: 
follow `Left->Node->Right` direction, i.e. do the recursive call
for the _left_ child, then do all the business with the node, and
then do the recursive call for the _right_ child.

The business here is to update a structure which keeps
a sum for the current level. 
Let's use hashmap for the Python solution 
and array for the Java one, 
because of known problems with 
[Java HashMap performance](https://github.com/vavr-io/vavr/issues/571).

**Algorithm**

- Implement recursive inorder traversal. Create a function
`inorder(node, level)` which takes node and its level as input
variables and recursively updates `level_sum[level]` structure. 

- Return key with max value in `level_sum`.

**Implementation**

<iframe src="https://leetcode.com/playground/wCw6Baxn/shared" frameBorder="0" width="100%" height="412" name="wCw6Baxn"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$, to visit each node 
exactly once during the inorder traversal, and then to iterate over `level_sum` structure. 

* Space complexity : $$\mathcal{O}(10000) = \mathcal{O}(1)$$ for the Java solution
to keep `level_sum`. $$\mathcal{O}(D)$$ for the Python solution, where D is 
a tree depth, that results in $$\mathcal{O}(\log N)$$ in the average case
of balanced tree and in $$\mathcal{O}(N)$$ in the worst case of the skewed tree.
<br /> 
<br />


---
#### Approach 2: BFS: Iterative Level Order Traversal with Queue

The drawback of DFS solution is that one has to keep all level sums.
That could be fixed with the level by level BFS solution :

- Initiate `max_sum = 0`. 

- Do BFS traversal, level by level. Compute the `curr_sum` for each level,
and update global maximum at the end of each level: 
`max_sum = max(max_sum, curr_sum)`.

Let's implement very standard BFS traversal with queue (FIFO structure) :

1. Push all nodes from the current level into the queue.

2. Iterate over that queue. At each step pop out a node from the 
left side of the queue, and push the children of that node into the right 
side of the queue. 

![postorder](../Figures/1161/bfs_queue2.png)

This way one could traverse the tree from root to leafs. 
Though one should stop somehow at the end of each level
to compute that level sum.

> Interview tip. Use _marker_ nodes to separate nodes 
in the queue / linked lists / trees. 
Example: [LRU Cache](https://leetcode.com/articles/lru-cache/). 

Here null marker node could be used to mark the end of the level.

![postorder](../Figures/1161/marker2.png)

**Algorithm** 

- Initiate variables:

    - Set current level and level with max sum to be equal to 1.
    
    - Set current sum to 0, and max sum to root value.
    
    - Let's use null as a marker. Initiate the queue :
    push the root and then push the marker, 
    to mark the end of level number 1.
    
- While there is more nodes than one marker in the queue, _i.e._
while the queue length is larger than 1:

    - Pop the node from the left side of the queue. 
    
    - If it's not the marker node, the current level continues:
        
        - Update current sum.
        
        - Push the children of that 
        node in the right side of the queue. 
    
    - If it _is_ the marker node, the level is ended. 
        
        - Update max sum and a level with max sum.
        
        - Set current sum to 0.
        
        - Increase the level number by 1, 
        and push marker node into queue to mark next level end.
        
- Return the number of level with max sum.

**Implementation**

<iframe src="https://leetcode.com/playground/gu4wVgzV/shared" frameBorder="0" width="100%" height="500" name="gu4wVgzV"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since each node is
processed exactly twice: to push into queue and then to pop out. 

* Space complexity: $$\mathcal{O}(N)$$ to keep the queue.
At each moment the queue contains not more than all nodes 
from one level. The max number of nodes on one level is
$$(N + 1)/2$$, 
in the case of [perfect tree](https://en.wikipedia.org/wiki/Binary_tree#Properties_of_binary_trees).
<br /> 
<br />


---
#### Approach 3: BFS: Short Python Solution 

In Python Approach 2 could be rewritten much shorter with 
the help of list comprehension. 

**Implementation**

<iframe src="https://leetcode.com/playground/B5Aaaqsf/shared" frameBorder="0" width="100%" height="344" name="B5Aaaqsf"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$. 

* Space complexity: $$\mathcal{O}(N)$$ to keep the queue.
At each moment the queue contains not more than all nodes 
from one level. The max number of nodes on one level is
$$(N + 1)/2$$, 
in the case of [perfect tree](https://en.wikipedia.org/wiki/Binary_tree#Properties_of_binary_trees).

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Two codes / language: BFS level traversal and DFS level sum.
- Author: rock
- Creation Date: Sun Aug 18 2019 12:03:04 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 11 2019 13:40:33 GMT+0800 (Singapore Standard Time)

<p>
**Method 1: BFS**

Use BFS to find the sum of each level, then locate the level with largest sum.

<iframe src="https://leetcode.com/playground/3ukQC8xD/shared" frameBorder="0" width="500" height="450"></iframe>

---------------------

**Method 2: DFS**
Use DFS to compute and store the sum of each level in an ArrayList, then locate the level with largest sum.
1. Recurse down from root, level of which is 0, increase level by 1 for each recursion down;
2. Use the level as the index of an ArrayList to store the sum of the correspoinding level;
3. Find the index of the max sum, then plus 1.

**Java**

```
    public int maxLevelSum(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        dfs(root, list, 0);
        return 1 + IntStream.range(0, list.size()).reduce(0, (a, b) -> list.get(a) < list.get(b) ? b : a);
    }
    private void dfs(TreeNode n, List<Integer> l, int level) {
        if (n == null) { return; } 
        if (l.size() == level) { l.add(n.val); } // never reach this level before, add first value.
        else { l.set(level, l.get(level) + n.val); } // reached the level before, accumulate current value to old value.
        dfs(n.left, l, level + 1);
        dfs(n.right, l, level + 1);
    }
```
In case you are NOT comfortable with the Java 8 stream in the return statement, it can be written as:
```
        int maxLevel = 0;
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(maxLevel) < list.get(i)) {
                maxLevel = i;
            }
        }
        return maxLevel + 1;
```

----

**Python 3**

```
    def maxLevelSum(self, root: TreeNode) -> int:
        def dfs(node: TreeNode, list: List, level: int) -> None:
            if not node:
                return
            if len(list) == level:
                list.append(node.val)
            else:
                list[level] += node.val
            dfs(node.left, list, level + 1)
            dfs(node.right, list, level + 1)
        list = []    
        dfs(root, list, 0)
        return 1 + list.index(max(list))
```

----

**Analysis:**

Time & space: O(n), n is the number of total nodes.
</p>


### C++ track level sum
- Author: votrubac
- Creation Date: Sun Aug 18 2019 12:03:24 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Mar 19 2020 05:13:40 GMT+0800 (Singapore Standard Time)

<p>
We can approach this with order traversal (DFS) or level-order traversal (BFS).

#### DFS
This is DFS solution that uses an array to track the sum for each level.

```cpp
vector<int> sums;
void dfs(TreeNode* r, size_t lvl) {
  if (r != nullptr) {
    sums.resize(max(sums.size(), lvl));
    sums[lvl - 1] += r->val;
    dfs(r->left, lvl + 1);
    dfs(r->right, lvl + 1);
  }
}
int maxLevelSum(TreeNode* r) {
  dfs(r, 1);
  return distance(begin(sums), max_element(begin(sums), end(sums))) + 1;
}
```
**Complexity Analysis**
- Time: *O(n)*
- Memory: *O(h)* for the stack and to store sums for each level. This could be O(n) in the worst case (when the tree is a vine).

#### Level-Order Traversal
Since we process the entire level before moving down, we do not need an array to accumulate sums for each level.
```cpp
int maxLevelSum(TreeNode* r) {
  vector<TreeNode*> q{r};
  int msum = 0, lvl = 0, msum_lvl = 0;
  while (!q.empty()) {
      vector<TreeNode*> q1;
      int sum = 0;
      ++lvl;
      for (auto n : q) {
          sum += n->val;
          if (n->left != nullptr) 
              q1.push_back(n->left);
          if (n->right != nullptr) 
              q1.push_back(n->right);
      }
      msum_lvl = sum < msum ? msum_lvl : lvl;
      msum = max(sum, msum);
      swap(q, q1);
  }
  return msum_lvl;
}
```
**Complexity Analysis**
- Time: *O(n)*
- Memory: *O(n / 2)* to process all nodes in the level. *n / 2* is the number of nodes in the last level of the full tree.
</p>


### c++ bfs
- Author: lei0108
- Creation Date: Sun Aug 25 2019 04:05:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 25 2019 04:05:35 GMT+0800 (Singapore Standard Time)

<p>
standard queue level order traversal.

```tmp_sum``` records sum of current level, ```global_sum``` records largest level sum so far.

If ```tmp_sum > global_sum```, it means sum of current level is large, assign current ```level``` to ```res```.

```
class Solution {
public:
    int maxLevelSum(TreeNode* root) {
        if (root == NULL) return 0;
        int res = 0, level = 0, global_sum = 0;
        queue<TreeNode*> q;
        q.push(root);
        while (!q.empty()) {
            int count = q.size(), tmp_sum = 0;
            while (count--) {
                TreeNode* tmp = q.front();
                q.pop();
                tmp_sum += tmp->val;
                if (tmp->left) q.push(tmp->left);
                if (tmp->right) q.push(tmp->right);
            }
            level++;
            if (tmp_sum > global_sum) res = level;
            global_sum = max(tmp_sum, global_sum);
        }
        return res;
    }
};
```
</p>


