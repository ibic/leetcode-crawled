---
title: "Count Univalue Subtrees"
weight: 234
#id: "count-univalue-subtrees"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a binary tree, return the number of <strong>uni-value</strong> subtrees.</p>

<p>A <strong>uni-value subtree</strong> means all nodes of the subtree have the same value.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/21/unival_e1.jpg" style="width: 450px; height: 258px;" />
<pre>
<strong>Input:</strong> root = [5,1,5,5,5,null,5]
<strong>Output:</strong> 4
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = []
<strong>Output:</strong> 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [5,5,5,5,5,null,5]
<strong>Output:</strong> 6
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The numbrt of the node in the tree will be in the range <code>[0, 1000]</code>.</li>
	<li><code>-1000 &lt;= Node.val &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Google - 4 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Box - 4 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Depth First Search

**Intuition**

Given a node in our tree, we know that it is a univalue subtree if it meets one of the following criteria:

   1. The node has no children (base case)
   2. All of the node's children are univalue subtrees, and the node and its children all have the same value

With this in mind we can perform a depth-first-search on our tree, and test if each subtree is uni-value in a bottom-up manner.


!?!../Documents/250_Count_Univalue_SUbtrees.json:1000,518!?!

**Algorithm**

<iframe src="https://leetcode.com/playground/dN5bMg4Z/shared" frameBorder="0" width="100%" height="500" name="dN5bMg4Z"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$.

    Due to the algorithm's depth-first nature, the `is_uni` status of each node is computed from bottom up. When given the `is_uni` status of its children, computing the `is_uni` status of a node occurs in $$O(1)$$

    This gives us $$O(1)$$ time for each node in the tree with $$O(N)$$ total nodes for a time complexity of $$O(N)$$

* Space complexity : $$O(H)$$, with `H` being the height of the tree. Each recursive call of `is_uni` requires stack space. Since we fully process `is_uni(node.left)` before calling `is_uni(node.right)`, the recursive stack is bound by the longest path from the root to a leaf - in other words the height of the tree.
<br />
<br />

---

#### Approach 2: Depth First Search - Pass Parent Values

**Algorithm**

We can use the intuition from approach one to further simplify our algorithm. Instead of checking if a node has no children, we treat `null` values as univalue subtrees that we don't add to the count.

In this manner, if a node has a `null` child, that child is automatically considered to a valid subtree, which results in the algorithm only checking if other children are invalid.

Finally, the helper function checks if the current node is a valid subtree but returns a boolean indicating if it is a valid component for its parent. This is done by passing in the value of the parent node.


<iframe src="https://leetcode.com/playground/KkbW68w3/shared" frameBorder="0" width="100%" height="446" name="KkbW68w3"></iframe>

The above code is a commented version of the code [here](https://leetcode.com/problems/count-univalue-subtrees/discuss/67602/Java-11-lines-added), originally written by [Stefan Pochmann](https://leetcode.com/stefanpochmann/).


**Complexity Analysis**

* Time complexity : $$O(N)$$. Same as the previous approach.

* Space complexity : $$O(H)$$, with `H` being the height of the tree. Same as the previous approach.

<br />

---

Written by [@alwinpeng](https://leetcode.com/alwinpeng/).

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My Concise JAVA Solution
- Author: stevenye
- Creation Date: Wed Aug 05 2015 23:41:23 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 01:33:14 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public int countUnivalSubtrees(TreeNode root) {
            int[] count = new int[1];
            helper(root, count);
            return count[0];
        }
        
        private boolean helper(TreeNode node, int[] count) {
            if (node == null) {
                return true;
            }
            boolean left = helper(node.left, count);
            boolean right = helper(node.right, count);
            if (left && right) {
                if (node.left != null && node.val != node.left.val) {
                    return false;
                }
                if (node.right != null && node.val != node.right.val) {
                    return false;
                }
                count[0]++;
                return true;
            }
            return false;
        }
    }
</p>


### Java, 11 lines added
- Author: StefanPochmann
- Creation Date: Thu Aug 06 2015 06:20:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 05:53:52 GMT+0800 (Singapore Standard Time)

<p>
Helper `all` tells whether all nodes in the given tree have the given value. And while doing that, it also counts the uni-value subtrees.

    public class Solution {
        int count = 0;
        boolean all(TreeNode root, int val) {
            if (root == null)
                return true;
            if (!all(root.left, root.val) | !all(root.right, root.val))
                return false;
            count++;
            return root.val == val;
        }
        public int countUnivalSubtrees(TreeNode root) {
            all(root, 0);
            return count;
        }
    }
</p>


### Python solution with comments (bottom-up).
- Author: OldCodingFarmer
- Creation Date: Sun Aug 16 2015 01:18:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 16 2015 01:18:14 GMT+0800 (Singapore Standard Time)

<p>
        
    def countUnivalSubtrees(self, root):
        self.count = 0
        self.checkUni(root)
        return self.count
    
    # bottom-up, first check the leaf nodes and count them, 
    # then go up, if both children are "True" and root.val is 
    # equal to both children's values if exist, then root node
    # is uniValue suntree node. 
    def checkUni(self, root):
        if not root:
            return True
        l, r = self.checkUni(root.left), self.checkUni(root.right)
        if l and r and (not root.left or root.left.val == root.val) and \
        (not root.right or root.right.val == root.val):
            self.count += 1
            return True
        return False
</p>


