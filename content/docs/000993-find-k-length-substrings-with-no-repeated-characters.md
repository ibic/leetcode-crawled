---
title: "Find K-Length Substrings With No Repeated Characters"
weight: 993
#id: "find-k-length-substrings-with-no-repeated-characters"
---
## Description
<div class="description">
<p>Given a string <code>S</code>, return the number of substrings of length <code>K</code> with no repeated characters.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-1-1">&quot;havefunonleetcode&quot;</span>, K = <span id="example-input-1-2">5</span>
<strong>Output: </strong><span id="example-output-1">6</span>
<strong>Explanation: </strong>
There are 6 substrings they are : &#39;havef&#39;,&#39;avefu&#39;,&#39;vefun&#39;,&#39;efuno&#39;,&#39;etcod&#39;,&#39;tcode&#39;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-2-1">&quot;home&quot;</span>, K = <span id="example-input-2-2">5</span>
<strong>Output: </strong><span id="example-output-2">0</span>
<strong>Explanation: </strong>
Notice K can be larger than the length of S. In this case is not possible to find any substring.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= S.length &lt;= 10^4</code></li>
	<li>All characters of S are lowercase English letters.</li>
	<li><code>1 &lt;= K &lt;= 10^4</code></li>
</ol>

</div>

## Tags
- String (string)
- Sliding Window (sliding-window)

## Companies
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Sliding Window
- Author: lee215
- Creation Date: Sun Jun 30 2019 00:02:28 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 22 2019 13:44:57 GMT+0800 (Singapore Standard Time)

<p>
# Solution 1: O(N) Brute Force
The brute force would check all substring and it\'s `O(KS)`.
But actually there are only 26 characters.
As a result, `K > 26` there won\'t be substring without repeated characters.
This turns the brute force into only `O(N)` time.

Here comes Python 1 line version.

**Python:**
```python
    def numKLenSubstrNoRepeats(self, S, K):
        return 0 if K > 26 else sum(len(set(S[i:i + K])) == K for i in xrange(n - K + 1))
```
<br>


# Solution 2: Sliding Window
For each letter `A[j]` in the string,
find the longest no repeated substring ending at `A[j]`

And If `j - i + 1 >= K`, among all these strings,
there one and only one no repeated substring,
whose length is `K`.
<br>

**Java:**
```java
    public int numKLenSubstrNoRepeats(String S, int K) {
        HashSet<Character> cur = new HashSet<>();
        int res = 0, i = 0;
        for (int j = 0; j < S.length(); ++j) {
            while (cur.contains(S.charAt(j)))
                cur.remove(S.charAt(i++));
            cur.add(S.charAt(j));
            res += j - i + 1 >= K ? 1 : 0;
        }
        return res;
    }
```

**C++:**
```cpp
    int numKLenSubstrNoRepeats(string S, int K) {
        unordered_set<int> cur;
        int res = 0, i = 0;
        for (int j = 0; j < S.size(); ++j) {
            while (cur.count(S[j]))
                cur.erase(S[i++]);
            cur.insert(S[j]);
            res += j - i + 1 >= K;
        }
        return res;
    }
```

**Python**
```python
    def numKLenSubstrNoRepeats(self, S, K):
        res, i = 0, 0
        cur = set()
        for j in xrange(len(S)):
            while S[j] in cur:
                cur.remove(S[i])
                i += 1
            cur.add(S[j])
            res += j - i + 1 >= K
        return res
```
</p>


### Java sliding window, clear code
- Author: tianchi-seattle
- Creation Date: Wed Jul 31 2019 11:35:30 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 31 2019 11:35:30 GMT+0800 (Singapore Standard Time)

<p>
```java
class Solution {
    public int numKLenSubstrNoRepeats(String S, int K) {
        int ans = 0;
        Set<Character> set = new HashSet<>();
        int i = 0;
        
        for (int j = 0; j < S.length(); j++) {
            while (set.contains(S.charAt(j))) {
                set.remove(S.charAt(i++));
            }
            set.add(S.charAt(j));
            
            if (j - i + 1 == K) {
                ans++;
                set.remove(S.charAt(i++));
            }
        }
        return ans;
    }
}
```
</p>


### [Java] Sliding Window two O(n) codes w/ comments and analysis.
- Author: rock
- Creation Date: Sun Jun 30 2019 00:38:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 30 2019 19:47:20 GMT+0800 (Singapore Standard Time)

<p>
**Method 1:**
Use HashSet to avoid duplicate.

```
    public int numKLenSubstrNoRepeats(String S, int K) {
        Set<Character> seen = new HashSet<>();
        int ans = 0;
        for (int hi = 0, lo = 0; hi < S.length(); ++hi) {
            while (!seen.add(S.charAt(hi))) // if duplicate char in window [lo, hi]
                seen.remove(S.charAt(lo++)); // shrink window by increasing low bound.
            if (seen.size() > K) // if the window wider than K.
                seen.remove(S.charAt(lo++)); // shrink the window by increasing low bound.
            if (seen.size() == K) // a solution found.
                ++ans;
        }
        return ans;
    }
```
**Method 2:**
Not using Set.

1. Use `cnt` to count the chars in sliding window [lo, hi], then loop through the input String `S`;
2. Whenever the count of a char increses from 0 to 1, check if the size of the window reaches K; if yes, found a window met requirement, increase the result by 1, then decrease the low bound `lo` by 1, decrease the corresponding `cnt` element accordingly; if the count of char is not 1, then it must be 2, then we have to increase the low bound `lo`, till the window excluds the aforementioned char ( that is, the count of that char decrease from 2 to 1);


```
     public int numKLenSubstrNoRepeats(String S, int K) {
        int[] cnt = new int[26];
        int ans = 0;
        for (int hi = 0, lo = 0; hi < S.length(); ++hi) {
            if (++cnt[S.charAt(hi) - \'a\'] == 1) {
                if (hi - lo + 1 == K) {
                    ++ans;
                    --cnt[S.charAt(lo++) - \'a\'];
                }
            }else {
                do {
                    --cnt[S.charAt(lo) - \'a\'];
                } while (S.charAt(hi) != S.charAt(lo++));
            }
        }
        return ans;
    }

```
**Analysis:**

Time: O(n), space: O(1), where n = S.length().
</p>


