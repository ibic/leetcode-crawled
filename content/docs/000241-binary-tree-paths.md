---
title: "Binary Tree Paths"
weight: 241
#id: "binary-tree-paths"
---
## Description
<div class="description">
<p>Given a binary tree, return all root-to-leaf paths.</p>

<p><strong>Note:</strong>&nbsp;A leaf is a node with no children.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong>

   1
 /   \
2     3
 \
  5

<strong>Output:</strong> [&quot;1-&gt;2-&gt;5&quot;, &quot;1-&gt;3&quot;]

<strong>Explanation:</strong> All root-to-leaf paths are: 1-&gt;2-&gt;5, 1-&gt;3
</pre>
</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Facebook - 6 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Apple - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Binary tree definition

First of all, here is the definition of the ```TreeNode``` which we would use
in the following implementation.

<iframe src="https://leetcode.com/playground/uJU5jdnZ/shared" frameBorder="0" width="100%" height="225" name="uJU5jdnZ"></iframe>
<br />
<br />


---
#### Approach 1: Recursion

The most intuitive way is to use a recursion here.
One is going through the tree 
by considering at each step the node itself and its children.
If node *is not* a leaf, one extends the current path by a node value and
calls recursively the path construction for its children.
If node *is* a leaf, one closes the current path and adds it into 
the list of paths.

<iframe src="https://leetcode.com/playground/ueSCRjpj/shared" frameBorder="0" width="100%" height="395" name="ueSCRjpj"></iframe>

**Complexity Analysis**

* Time complexity : we visit each node exactly once, 
thus the time complexity is $$\mathcal{O}(N)$$,
where $$N$$ is the number of nodes.
* Space complexity : $$\mathcal{O}(N)$$. Here we use the space for a stack call and for a 
`paths` list to store the answer. `paths` contains as many elements as leafs in the tree and 
hence couldn't be larger than $$\log N$$ for the trees containing more than one element. 
Hence the space complexity is determined by a stack call.
In the worst case, when the tree is completely unbalanced,
*e.g.* each node has only one child node, the recursion call would occur
$$N$$ times (the height of the tree), therefore the storage to keep the call stack would be $$\mathcal{O}(N)$$.
But in the best case (the tree is balanced), the height of the tree would be $$\log(N)$$.
Therefore, the space complexity in this case would be $$\mathcal{O}(\log(N))$$.
<br/>
<br/>


---
#### Approach 2: Iterations

The approach above could be rewritten with the help of iterations. 
This way we initiate the stack by a root node and then at each step
we pop out one node and its path.
If the poped node *is* a leaf, one update the list of all paths.
If not, one pushes its child nodes and
corresponding paths into stack till all nodes are checked.

<!--![LIS](../Figures/257/257_tr.gif)-->
!?!../Documents/257_LIS.json:1000,491!?!

<iframe src="https://leetcode.com/playground/4znAcEDG/shared" frameBorder="0" width="100%" height="500" name="4znAcEDG"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since each node is visited exactly once. 
* Space complexity : $$\mathcal{O}(N)$$ as we could keep up to the entire tree.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted Java simple solution in 8 lines
- Author: xhadfasd
- Creation Date: Sun Aug 16 2015 11:51:40 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 18:15:24 GMT+0800 (Singapore Standard Time)

<p>
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> answer = new ArrayList<String>();
        if (root != null) searchBT(root, "", answer);
        return answer;
    }
    private void searchBT(TreeNode root, String path, List<String> answer) {
        if (root.left == null && root.right == null) answer.add(path + root.val);
        if (root.left != null) searchBT(root.left, path + root.val + "->", answer);
        if (root.right != null) searchBT(root.right, path + root.val + "->", answer);
    }
</p>


### Python solutions (dfs+stack, bfs+queue, dfs recursively).
- Author: OldCodingFarmer
- Creation Date: Mon Aug 17 2015 02:41:47 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 04 2020 23:16:02 GMT+0800 (Singapore Standard Time)

<p>
```
        
    # dfs + stack
    def binaryTreePaths1(self, root):
        if not root:
            return []
        res, stack = [], [(root, "")]
        while stack:
            node, ls = stack.pop()
            if not node.left and not node.right:
                res.append(ls+str(node.val))
            if node.right:
                stack.append((node.right, ls+str(node.val)+"->"))
            if node.left:
                stack.append((node.left, ls+str(node.val)+"->"))
        return res
        
    # bfs + queue
    def binaryTreePaths2(self, root):
        if not root:
            return []
        res, queue = [], collections.deque([(root, "")])
        while queue:
            node, ls = queue.popleft()
            if not node.left and not node.right:
                res.append(ls+str(node.val))
            if node.left:
                queue.append((node.left, ls+str(node.val)+"->"))
            if node.right:
                queue.append((node.right, ls+str(node.val)+"->"))
        return res
        
    # dfs recursively
    def binaryTreePaths(self, root):
        if not root:
            return []
        res = []
        self.dfs(root, "", res)
        return res
    
    def dfs(self, root, ls, res):
        if not root.left and not root.right:
            res.append(ls+str(root.val))
        if root.left:
            self.dfs(root.left, ls+str(root.val)+"->", res)
        if root.right:
            self.dfs(root.right, ls+str(root.val)+"->", res)
			
    def binaryTreePaths1(self, root):
        return self.dfs(root, "")
    
    def dfs(self, root, path):
        if not root:
            return []
        path += str(root.val)
        if not root.left and not root.right:
            return [path]
        path += "->"
        return self.dfs(root.left, path) + self.dfs(root.right, path)
    
    def binaryTreePaths(self, root): # inorder
        stack, ret = [(root, "")], []
        while stack:
            node, path = stack.pop()
            if node:
                if not node.left and not node.right:
                    ret.append(path+str(node.val))
                s = path + str(node.val) + "->"
                stack.append((node.right, s))
                stack.append((node.left, s))    
        return ret
        
```
</p>


### C++ simple 4ms recursive solution
- Author: jaewoo
- Creation Date: Sun Aug 16 2015 06:16:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 16 2015 06:16:08 GMT+0800 (Singapore Standard Time)

<p>
    void binaryTreePaths(vector<string>& result, TreeNode* root, string t) {
        if(!root->left && !root->right) {
            result.push_back(t);
            return;
        }

        if(root->left) binaryTreePaths(result, root->left, t + "->" + to_string(root->left->val));
        if(root->right) binaryTreePaths(result, root->right, t + "->" + to_string(root->right->val));
    }

    vector<string> binaryTreePaths(TreeNode* root) {
        vector<string> result;
        if(!root) return result;
        
        binaryTreePaths(result, root, to_string(root->val));
        return result;
    }
</p>


