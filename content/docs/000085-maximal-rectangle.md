---
title: "Maximal Rectangle"
weight: 85
#id: "maximal-rectangle"
---
## Description
<div class="description">
<p>Given a <code>rows x cols</code>&nbsp;binary <code>matrix</code> filled with <code>0</code>&#39;s and <code>1</code>&#39;s, find the largest rectangle containing only <code>1</code>&#39;s and return <em>its area</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/14/maximal.jpg" style="width: 402px; height: 322px;" />
<pre>
<strong>Input:</strong> matrix = [[&quot;1&quot;,&quot;0&quot;,&quot;1&quot;,&quot;0&quot;,&quot;0&quot;],[&quot;1&quot;,&quot;0&quot;,&quot;1&quot;,&quot;1&quot;,&quot;1&quot;],[&quot;1&quot;,&quot;1&quot;,&quot;1&quot;,&quot;1&quot;,&quot;1&quot;],[&quot;1&quot;,&quot;0&quot;,&quot;0&quot;,&quot;1&quot;,&quot;0&quot;]]
<strong>Output:</strong> 6
<strong>Explanation:</strong> The maximal rectangle is shown in the above picture.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> matrix = []
<strong>Output:</strong> 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> matrix = [[&quot;0&quot;]]
<strong>Output:</strong> 0
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> matrix = [[&quot;1&quot;]]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> matrix = [[&quot;0&quot;,&quot;0&quot;]]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>rows == matrix.length</code></li>
	<li><code>cols == matrix.length</code></li>
	<li><code>0 &lt;= row, cols &lt;= 200</code></li>
	<li><code>matrix[i][j]</code> is <code>&#39;0&#39;</code> or <code>&#39;1&#39;</code>.</li>
</ul>

</div>

## Tags
- Array (array)
- Hash Table (hash-table)
- Dynamic Programming (dynamic-programming)
- Stack (stack)

## Companies
- Google - 13 (taggedByAdmin: false)
- Amazon - 7 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Bloomberg - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Wayfair - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Karat - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Indeed - 5 (taggedByAdmin: false)
- VMware - 4 (taggedByAdmin: false)
- Samsung - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: true)
- Cloudera - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Brute Force

**Algorithm**

Trivially we can enumerate every possible rectangle. This is done by iterating over all possible combinations of coordinates `(x1, y1)` and `(x2, y2)` and letting them define a rectangle with the coordinates being opposite corners. This is too slow to pass all test cases.


**Complexity Analysis**

* Time complexity : $$O(N^3M^3)$$, with `N` being the number of rows and `M` the number of columns.

    Iterating over all possible coordinates is $$O(N^2M^2)$$, and iterating over the rectangle defined by two coordinates is an additional $$O(NM)$$. $$O(NM) * O(N^2M^2) = O(N^3M^3)$$.

* Space complexity : $$O(1)$$.
<br />
<br />

---

#### Approach 2: Dynamic Programming - Better Brute Force on Histograms

**Algorithm**

We can compute the maximum width of a rectangle that ends at a given coordinate in constant time. We do this by keeping track of the number of consecutive ones each square in each row. As we iterate over each row we update the maximum possible width at that point. This is done using `row[i] = row[i - 1] + 1 if row[i] == '1'`.

!?!../Documents/85_maximal_rectangle_anim1.json:2000,500!?!

Once we know the maximum widths for each point above a given point, we can compute the maximum rectangle with the lower right corner at that point in linear time. As we iterate up the column, we know that the maximal width of a rectangle spanning from the original point to the current point is the running minimum of each maximal width we have encountered.

We define:

$$maxWidth = min(maxWidth, widthHere)$$

$$curArea = maxWidth * (currentRow - originalRow + 1)$$

$$maxArea = max(maxArea, curArea)$$

The following animation makes this more clear. Given the maximal width of all points above it, let's calculate the maximum area of any rectangle at the bottom yellow square:

!?!../Documents/85_maximal_rectangle_anim3.json:1400,1125!?!

Repeating this process for every point in our input gives us the global maximum.

Note that our method of precomputing our maximum width essentially breaks down our input into a set of histograms, with each column being a new histogram. We are computing the maximal area for each histogram.

![Histograms](../Figures/85/histogram.jpg)

As a result, the above approach is essentially a repeated use of the better brute force approach detailed in [84 - Largest Rectangle in Histogram](https://leetcode.com/problems/largest-rectangle-in-histogram/solution/).

<iframe src="https://leetcode.com/playground/CP9EugZb/shared" frameBorder="0" width="100%" height="497" name="CP9EugZb"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N^2M)$$. Computing the maximum area for one point takes $$O(N)$$ time, since it iterates over the values in the same column. This is done for all $$N * M$$ points, giving $$O(N) * O(NM) = O(N^2M)$$.

* Space complexity : $$O(NM)$$. We allocate an equal sized array to store the maximum width at each point.
<br />
<br />

---

#### Approach 3: Using Histograms - Stack

**Algorithm**

In the previous approach we discussed breaking the input into a set of histograms - one histogram representing the substructure at each column. To compute the maximum area in our rectangle, we merely have to compute the maximum area of each histogram and find the global maximum (note that the below approach builds a histogram for each row instead of each column, but the idea is still the same).

Since [Largest Rectangle in Histogram](https://leetcode.com/problems/largest-rectangle-in-histogram/) is already a problem on leetcode, we can just borrow the fastest stack-based solution [here](https://leetcode.com/problems/largest-rectangle-in-histogram/solution/) and apply it onto each histogram we generate. For an in-depth explanation on how the Largest Rectangle in Histogram algorithm works, please use the links above.

<iframe src="https://leetcode.com/playground/kDw63HS4/shared" frameBorder="0" width="100%" height="500" name="kDw63HS4"></iframe>

Note that the code under the function `leetcode84` is a direct copy paste from the final solution in [84 - Largest Rectangle in Histogram](https://leetcode.com/problems/largest-rectangle-in-histogram/solution/).

**Complexity Analysis**

* Time complexity : $$O(NM)$$. Running `leetcode84` on each row takes `M` (length of each row) time. This is done `N` times for $$O(NM)$$.

* Space complexity : $$O(M)$$. We allocate an array the size of the the number of columns to store our widths at each row.
<br />
<br />

---

#### Approach 4: Dynamic Programming - Maximum Height at Each Point

**Intuition**

Imagine an algorithm where for each point we computed a rectangle by doing the following:

 * Finding the maximum height of the rectangle by iterating upwards until a 0 is reached

 * Finding the maximum width of the rectangle by iterating outwards left and right until a height that doesn't accommodate the maximum height of the rectangle

 For example finding the rectangle defined by the yellow point:

 !?!../Documents/85_maximal_rectangle_anim2.json:1125,1125!?!

 We know that the maximal rectangle must be one of the rectangles constructed in this manner.

 Given a maximal rectangle with height `h`, left bound `l`, and right bound `r`, there must be a point on the interval `[l, r]` on the rectangle's base where the number of consecutive ones (height) above the point is `<=h`. If this point exists, then the rectangle defined by the point in the above manner will be the maximal rectangle, as it will reach height `h` iterating upward and then expand to the bounds of `[l, r]` as all heights within those bounds must accommodate `h` for the rectangle to exist.


 If this point does not exist, then the rectangle cannot be maximum, as you would be able to create a bigger rectangle by simply increasing the height of original rectangle, since all heights on the interval `[l, r]` would be greater than `h`.

 As a result for each point you only need to compute `h`, `l`, and `r` - the height, left bound, and right bound of the rectangle it defines.

 Using dynamic programming, we can use the `h`, `l`, and `r` of each point in the previous row to compute the `h`, `l`, and `r` for every point in the next row in linear time.


**Algorithm**

Given row `matrix[i]`, we keep track of the `h`, `l`, and `r` of each point in the row by defining three arrays - `height`, `left`, and `right`.

`height[j]` will correspond to the height of `matrix[i][j]`, and so on and so forth with the other arrays.

The question now becomes how to update each array.

Height:

This one is easy. `h` is defined as the number of continuous ones in a line from our point. We explored how to compute this in Approach 2 in one row with:

    row[j] = row[j - 1] + 1 if row[j] == '1'

We can just make a minor modification for it to work for us here:

    new_height[j] = old_height[j] + 1 if row[j] == '1' else 0


Left:

Consider what causes changes to the left bound of our rectangle. Since all instances of zeros occurring in the row above the current one have already been factored into the current version of `left`, the only thing that affects our `left` is if we encounter a zero in our current row.

As a result we can define:

    new_left[j] = max(old_left[j], cur_left)

`cur_left` is one greater than rightmost occurrence of zero we have encountered. When we "expand" the rectangle to the left, we know it can't expand past that point, otherwise it'll run into the zero.

Right:

Here we can reuse our reasoning in `left` and define:

    new_right[j] = min(old_right[j], cur_right)

`cur_right` is the leftmost occurrence of zero we have encountered. For the sake of simplicity, we don't decrement `cur_right` by one (like how we increment `cur_left`) so we can compute the area of the rectangle with `height[j] * (right[j] - left[j])` instead of `height[j] * (right[j] + 1 - left[j])`.

This means that _technically_ the base of the rectangle is defined by the half-open interval `[l, r)` instead of the closed interval `[l, r]`, and `right` is really one greater than right boundary. Although the algorithm will still work if we don't do this with `right`, doing it this way makes the area calculation a little cleaner.

Note that to keep track of our `cur_right` correctly, we must iterate from right to left, so this is what is done when updating `right`.

With our `left`, `right`, and `height` arrays appropriately updated, all that there is left to do is compute the area of each rectangle.

Since we know the bounds and height of rectangle `j`, we can trivially compute it's area with `height[j] * (right[j] - left[j])`, and change our `max_area` if we find that rectangle `j`'s area is greater.


<iframe src="https://leetcode.com/playground/zVYwyeYz/shared" frameBorder="0" width="100%" height="500" name="zVYwyeYz"></iframe>

The code and idea for the above solution originates from user [morrischen2008](https://leetcode.com/morrischen2008/).

**Complexity Analysis**

* Time complexity : $$O(NM)$$. In each iteration over `N` we iterate over `M` a constant number of times.

* Space complexity : $$O(M)$$. `M` is the length of the additional arrays we keep.

<br />

---

Written by [@alwinpeng](https://leetcode.com/alwinpeng/).

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Share my DP solution
- Author: morrischen2008
- Creation Date: Sat Jan 03 2015 04:48:08 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 02:56:37 GMT+0800 (Singapore Standard Time)

<p>
The DP solution proceeds row by row, starting from the first row. Let the maximal rectangle area at row i and column j be computed by [right(i,j) - left(i,j)]*height(i,j).

All the 3 variables left, right, and height can be determined by the information from previous row, and also information from the current row. So it can be regarded as a DP solution. The transition equations are:

> left(i,j) = max(left(i-1,j), cur_left), cur_left can be determined from the current row

> right(i,j) = min(right(i-1,j), cur_right), cur_right can be determined from the current row 

> height(i,j) = height(i-1,j) + 1,  if matrix[i][j]=='1'; 

> height(i,j) = 0,  if matrix[i][j]=='0'



The code is as below. The loops can be combined for speed but I separate them for more clarity of the algorithm.

    class Solution {public:
    int maximalRectangle(vector<vector<char> > &matrix) {
        if(matrix.empty()) return 0;
        const int m = matrix.size();
        const int n = matrix[0].size();
        int left[n], right[n], height[n];
        fill_n(left,n,0); fill_n(right,n,n); fill_n(height,n,0);
        int maxA = 0;
        for(int i=0; i<m; i++) {
            int cur_left=0, cur_right=n; 
            for(int j=0; j<n; j++) { // compute height (can do this from either side)
                if(matrix[i][j]=='1') height[j]++; 
                else height[j]=0;
            }
            for(int j=0; j<n; j++) { // compute left (from left to right)
                if(matrix[i][j]=='1') left[j]=max(left[j],cur_left);
                else {left[j]=0; cur_left=j+1;}
            }
            // compute right (from right to left)
            for(int j=n-1; j>=0; j--) {
                if(matrix[i][j]=='1') right[j]=min(right[j],cur_right);
                else {right[j]=n; cur_right=j;}    
            }
            // compute the area of rectangle (can do this from either side)
            for(int j=0; j<n; j++)
                maxA = max(maxA,(right[j]-left[j])*height[j]);
        }
        return maxA;
    }
};


If you think this algorithm is not easy to understand, you can try this example:

    0 0 0 1 0 0 0 
    0 0 1 1 1 0 0 
    0 1 1 1 1 1 0

The vector "left" and "right" from row 0 to row 2 are as follows

row 0:
 

    l: 0 0 0 3 0 0 0
    r: 7 7 7 4 7 7 7

row 1:

    l: 0 0 2 3 2 0 0
    r: 7 7 5 4 5 7 7 

row 2:

    l: 0 1 2 3 2 1 0
    r: 7 6 5 4 5 6 7

The vector "left" is computing the left boundary. Take (i,j)=(1,3) for example. On current row 1, the left boundary is at j=2. However, because matrix[1][3] is 1, you need to consider the left boundary on previous row as well, which is 3. So the real left boundary at (1,3) is 3. 

I hope this additional explanation makes things clearer.
</p>


### A O(n^2) solution based on Largest Rectangle in Histogram
- Author: wangyushawn
- Creation Date: Mon May 12 2014 06:25:45 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 09:14:45 GMT+0800 (Singapore Standard Time)

<p>
This question is similar as [\[Largest Rectangle in Histogram\]][1]:

You can maintain a row length of Integer array H recorded its height of '1's, and scan and update row by row to find out the largest rectangle of each row.

For each row, if matrix[row][i] == '1'. H[i] +=1, or reset the H[i] to zero.
and accroding the algorithm of [Largest Rectangle in Histogram], to update the maximum area.

    public class Solution {
        public int maximalRectangle(char[][] matrix) {
            if (matrix==null||matrix.length==0||matrix[0].length==0)
                return 0;
            int cLen = matrix[0].length;    // column length
            int rLen = matrix.length;       // row length
            // height array 
            int[] h = new int[cLen+1];
            h[cLen]=0;
            int max = 0;
            
            
            for (int row=0;row<rLen;row++) {
                Stack<Integer> s = new Stack<Integer>();
                for (int i=0;i<cLen+1;i++) {
                    if (i<cLen)
                        if(matrix[row][i]=='1')
                            h[i]+=1;
                        else h[i]=0;
                    
                    if (s.isEmpty()||h[s.peek()]<=h[i])
                        s.push(i);
                    else {
                        while(!s.isEmpty()&&h[i]<h[s.peek()]){
                            int top = s.pop();
                            int area = h[top]*(s.isEmpty()?i:(i-s.peek()-1));
                            if (area>max)
                                max = area;
                        }
                        s.push(i);
                    }
                }
            }
            return max;
        }
    }

  [1]: http://oj.leetcode.com/problems/largest-rectangle-in-histogram/
</p>


### AC Python DP solutioin 120ms based on largest rectangle in histogram
- Author: dietpepsi
- Creation Date: Sat Oct 24 2015 01:36:29 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 02:24:19 GMT+0800 (Singapore Standard Time)

<p>
    def maximalRectangle(self, matrix):
        if not matrix or not matrix[0]:
            return 0
        n = len(matrix[0])
        height = [0] * (n + 1)
        ans = 0
        for row in matrix:
            for i in xrange(n):
                height[i] = height[i] + 1 if row[i] == '1' else 0
            stack = [-1]
            for i in xrange(n + 1):
                while height[i] < height[stack[-1]]:
                    h = height[stack.pop()]
                    w = i - 1 - stack[-1]
                    ans = max(ans, h * w)
                stack.append(i)
        return ans

    # 65 / 65 test cases passed.
    # Status: Accepted
    # Runtime: 120 ms
    # 100%

The solution is based on [largest rectangle in histogram][1] solution. Every row in the matrix is viewed as the ground with some buildings on it. The building height is the count of consecutive 1s from that row to above rows. The rest is then the same as [this solution for largest rectangle in histogram][2]


  [1]: https://leetcode.com/problems/largest-rectangle-in-histogram/
  [2]: https://leetcode.com/discuss/65647/ac-python-clean-solution-using-stack-76ms
</p>


