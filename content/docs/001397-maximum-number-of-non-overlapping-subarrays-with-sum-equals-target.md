---
title: "Maximum Number of Non-Overlapping Subarrays With Sum Equals Target"
weight: 1397
#id: "maximum-number-of-non-overlapping-subarrays-with-sum-equals-target"
---
## Description
<div class="description">
<p>Given an array <code>nums</code> and an integer <code><font face="monospace">target</font></code>.</p>

<p>Return the maximum number of <strong>non-empty</strong>&nbsp;<strong>non-overlapping</strong> subarrays such that the sum of values in each subarray is equal to <code><font face="monospace">target</font></code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,1,1,1,1], target = 2
<strong>Output:</strong> 2
<strong>Explanation: </strong>There are 2 non-overlapping subarrays [<strong>1,1</strong>,1,<strong>1,1</strong>] with sum equals to target(2).
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [-1,3,5,1,4,2,-9], target = 6
<strong>Output:</strong> 2
<strong>Explanation: </strong>There are 3 subarrays with sum equal to 6.
([5,1], [4,2], [3,5,1,4,2,-9]) but only the first 2 are non-overlapping.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [-2,6,6,3,5,4,1,2,8], target = 10
<strong>Output:</strong> 3
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,0,0], target = 0
<strong>Output:</strong> 3
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;=&nbsp;10^5</code></li>
	<li><code>-10^4 &lt;= nums[i] &lt;=&nbsp;10^4</code></li>
	<li><code>0 &lt;= target &lt;= 10^6</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Detailed Explanation - DP/Map/Prefix - O(N)
- Author: frankkkkk
- Creation Date: Sun Aug 09 2020 12:01:45 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 10 2020 00:49:52 GMT+0800 (Singapore Standard Time)

<p>
**Key Notes:**
- It is very similar to other prefix sum/map problems, but O(N2) cannot pass instead.
- O(N2) solution is pretty naive, dp[i] means: optimal solution when using 0...i elements. But it is TLE. Transition function is: **dp[i] = Math.max(dp[i], 1 + dp[j])** if you can make a sum from j...i which is **equal to target.**
	- **Observations for O(N2) solution:**
		- Search from i - 1 to 0, if you find a sum equal to target, **you can stop searching**, since longer dp[j] is alwasy better than shorter dp[j].
		- You don\'t need to search one by one, **but using a map**.
		- Let\'s coding!
```java
Map<Integer, Integer> map = new HashMap<>();
map.put(0, 0);

int res = 0;
int sum = 0;

for (int i = 0; i < nums.length; ++i) {
	sum += nums[i];
	if (map.containsKey(sum - target)) {
		res = Math.max(res, map.get(sum - target) + 1);
	}
	map.put(sum, res);
}

return res;
```
  

**TLE O(N2) DP Solution:**
```java
int N = nums.length;
int[] dp = new int[N + 1];

for (int i = 0; i < N; ++i) {

	int sum = 0;
	for (int j = i; j >= 0; --j) {
		sum += nums[j];
		dp[i + 1] = Math.max(dp[i + 1], dp[j] + (sum == target ? 1 : 0));
	}
}

return dp[N];
```
</p>


### Java 14 lines Greedy PrefixSum with line-by-line explanation easy to understand
- Author: FunBam
- Creation Date: Sun Aug 09 2020 12:01:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 09 2020 12:03:04 GMT+0800 (Singapore Standard Time)

<p>
![image](https://assets.leetcode.com/users/images/db055156-9c75-4f94-ae84-30891792fe7e_1596945767.5826385.png)

```
class Solution {
    public int maxNonOverlapping(int[] nums, int target) {
        Map<Integer, Integer> map= new HashMap<>();
        int prefixSum=0, availableIdx=-1, res=0;
        map.put(0,-1);
        for (int i=0; i<nums.length; i++){
            prefixSum+=nums[i];
            int remain = prefixSum - target;
            if (map.containsKey(remain) && map.get(remain)>=availableIdx){
                res++;
                availableIdx=i;
            }
            map.put(prefixSum, i);
        }
        return res;
    }
}
```
Happy Coding!
</p>


### C++ O(n)
- Author: votrubac
- Creation Date: Sun Aug 09 2020 12:03:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 09 2020 12:26:14 GMT+0800 (Singapore Standard Time)

<p>
Well, we all know how to find all arrays that sum to a target in O(n) - track prefix sum in a hash map and look up complements (`sum - target`) there.

Now, how do we count non-overlapping arrays? The intuition here is that the `right` side of our array is only increasing, and we can greedily track the smallest `right`. Value in `dp` is the left index of the subarray with the target sum. If we find a non-overlapping array (`right` <= `left`), we increase `cnt` and update `right` to the current position.

```cpp
int maxNonOverlapping(vector<int>& nums, int target) {
    unordered_map<int, int> dp;
    dp[0] = -1;
    int sum = 0, right = -1, cnt = 0;
    for (int i = 0; i < nums.size(); ++i) {
        sum += nums[i];
        if (dp.count(sum - target)) {
            int left = dp[sum - target];
            if (right <= left) {
                ++cnt;
                right = i;
            }
        }
        dp[sum] = i;
    }
    return cnt;
}
```
</p>


