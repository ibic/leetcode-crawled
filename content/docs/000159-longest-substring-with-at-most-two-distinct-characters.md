---
title: "Longest Substring with At Most Two Distinct Characters"
weight: 159
#id: "longest-substring-with-at-most-two-distinct-characters"
---
## Description
<div class="description">
<p>Given a string <strong><em>s</em></strong> , find the length of the longest substring&nbsp;<strong><em>t&nbsp;&nbsp;</em></strong>that contains <strong>at most </strong>2 distinct characters.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> &quot;eceba&quot;
<strong>Output: </strong>3
<strong>Explanation: <em>t</em></strong><em> </em>is &quot;ece&quot; which its length is 3.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> &quot;ccaabbb&quot;
<strong>Output: </strong>5
<strong>Explanation: <em>t</em></strong><em> </em>is &quot;aabbb&quot; which its length is 5.
</pre>

</div>

## Tags
- Hash Table (hash-table)
- Two Pointers (two-pointers)
- String (string)
- Sliding Window (sliding-window)

## Companies
- Google - 3 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)
- Amazon - 6 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Sliding Window

**Intuition**

To solve the problem in one pass
let's use here _sliding window_ approach with two set pointers
`left` and `right` serving as the window boundaries.

The idea is to set both pointers in the position `0` and
then move `right` pointer to the right while the
window contains not more than two distinct characters. 
If at some point we've got `3` distinct characters,
let's move `left` pointer to keep not more than `2`
distinct characters in the window.

![compute](../Figures/159/sliding.png)

Basically that's the algorithm : to move sliding window along the string,
to keep not more than `2` distinct characters in the window, and
to update max substring length at each step.

> There is just one more question to reply - 
how to move the left pointer
to keep only `2` distinct characters in the string?

Let's use for this purpose hashmap containing all characters 
in the sliding window as keys and their rightmost positions 
as values. At each moment, this hashmap could contain 
not more than `3` elements.

![compute](../Figures/159/move_left.png)

For example, using this hashmap one knows that the rightmost position
of character `e` in `"eeeeeeeet"` window is `8` and so one has
to move `left` pointer in the position `8 + 1 = 9` to
exclude the character `e` from the sliding window.  

Do we have here the best possible time complexity?
Yes, we do - it's the only one pass along the string with 
`N` characters and the time complexity is $$\mathcal{O}(N)$$.

**Algorithm**

Now one could write down the algortihm.

- Return `N` if the string length `N` is smaller than `3`. 
- Set both set pointers in the beginning 
 of the string `left = 0` and `right = 0` and init max substring
 length `max_len = 2`.
- While `right` pointer is less than `N`:
    * If hashmap contains less than `3` distinct characters,
    add the current character `s[right]` in the hashmap and
    move `right` pointer to the right.
    * If hashmap contains `3` distinct characters,
    remove the leftmost character from the hashmap
    and move the `left` pointer so that sliding window contains
    again `2` distinct characters only.
    * Update `max_len`.

**Implementation**

!?!../Documents/159_LIS.json:1000,492!?!

<iframe src="https://leetcode.com/playground/C9AUoZNe/shared" frameBorder="0" width="100%" height="500" name="C9AUoZNe"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ where `N`
is a number of characters in the input string.
 
* Space complexity : $$\mathcal{O}(1)$$ since additional 
space is used only for a hashmap with at most `3` elements.

**Problem generalization**

The same sliding window approach could be used to solve 
the generalized problem :

[Longest Substring with At Most K Distinct Characters](https://leetcode.com/problems/longest-substring-with-at-most-k-distinct-characters/)

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Sliding Window algorithm template to solve all the Leetcode substring search problem.
- Author: chaoyanghe
- Creation Date: Tue Dec 13 2016 15:08:24 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 12 2019 15:13:59 GMT+0800 (Singapore Standard Time)

<p>
**Among all leetcode questions, I find that there are at least 5 substring search problem which could be solved by the sliding window algorithm.** 
so I sum up the algorithm template here. wish it will help you!

1) ***the template***:
```
public class Solution {
    public List<Integer> slidingWindowTemplateByHarryChaoyangHe(String s, String t) {
        //init a collection or int value to save the result according the question.
        List<Integer> result = new LinkedList<>();
        if(t.length()> s.length()) return result;
        
        //create a hashmap to save the Characters of the target substring.
        //(K, V) = (Character, Frequence of the Characters)
        Map<Character, Integer> map = new HashMap<>();
        for(char c : t.toCharArray()){
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        //maintain a counter to check whether match the target string.
        int counter = map.size();//must be the map size, NOT the string size because the char may be duplicate.
        
        //Two Pointers: begin - left pointer of the window; end - right pointer of the window
        int begin = 0, end = 0;
        
        //the length of the substring which match the target string.
        int len = Integer.MAX_VALUE; 
        
        //loop at the begining of the source string
        while(end < s.length()){
            
            char c = s.charAt(end);//get a character
            
            if( map.containsKey(c) ){
                map.put(c, map.get(c)-1);// plus or minus one
                if(map.get(c) == 0) counter--;//modify the counter according the requirement(different condition).
            }
            end++;
            
            //increase begin pointer to make it invalid/valid again
            while(counter == 0 /* counter condition. different question may have different condition */){
                
                char tempc = s.charAt(begin);//***be careful here: choose the char at begin pointer, NOT the end pointer
                if(map.containsKey(tempc)){
                    map.put(tempc, map.get(tempc) + 1);//plus or minus one
                    if(map.get(tempc) > 0) counter++;//modify the counter according the requirement(different condition).
                }
                
                /* save / update(min/max) the result if find a target*/
                // result collections or result int value
                
                begin++;
            }
        }
        return result;
    }
}
````

1) Firstly, here is my sliding solution this question. I will sum up the template below this code.


**2) the similar questions are:**

https://leetcode.com/problems/minimum-window-substring/
https://leetcode.com/problems/longest-substring-without-repeating-characters/\u2028https://leetcode.com/problems/substring-with-concatenation-of-all-words/
https://leetcode.com/problems/longest-substring-with-at-most-two-distinct-characters/
https://leetcode.com/problems/find-all-anagrams-in-a-string/

**3) I will give my solution for these questions use the above template one by one** 

**Minimum-window-substring**
https://leetcode.com/problems/minimum-window-substring/
```
public class Solution {
    public String minWindow(String s, String t) {
        if(t.length()> s.length()) return "";
        Map<Character, Integer> map = new HashMap<>();
        for(char c : t.toCharArray()){
            map.put(c, map.getOrDefault(c,0) + 1);
        }
        int counter = map.size();
        
        int begin = 0, end = 0;
        int head = 0;
        int len = Integer.MAX_VALUE;
        
        while(end < s.length()){
            char c = s.charAt(end);
            if( map.containsKey(c) ){
                map.put(c, map.get(c)-1);
                if(map.get(c) == 0) counter--;
            }
            end++;
            
            while(counter == 0){
                char tempc = s.charAt(begin);
                if(map.containsKey(tempc)){
                    map.put(tempc, map.get(tempc) + 1);
                    if(map.get(tempc) > 0){
                        counter++;
                    }
                }
                if(end-begin < len){
                    len = end - begin;
                    head = begin;
                }
                begin++;
            }
            
        }
        if(len == Integer.MAX_VALUE) return "";
        return s.substring(head, head+len);
    }
}
````
you may find that I only change a little code above to solve the question "Find All Anagrams in a String":
change 
```
                if(end-begin < len){
                    len = end - begin;
                    head = begin;
                }
````
to
```
                if(end-begin == t.length()){
                    result.add(begin);
                }
````

**longest substring without repeating characters**
https://leetcode.com/problems/longest-substring-without-repeating-characters/\u2028
```
public class Solution {
    public int lengthOfLongestSubstring(String s) {
        Map<Character, Integer> map = new HashMap<>();
        int begin = 0, end = 0, counter = 0, d = 0;

        while (end < s.length()) {
            // > 0 means repeating character
            //if(map[s.charAt(end++)]-- > 0) counter++;
            char c = s.charAt(end);
            map.put(c, map.getOrDefault(c, 0) + 1);
            if(map.get(c) > 1) counter++;
            end++;
            
            while (counter > 0) {
                //if (map[s.charAt(begin++)]-- > 1) counter--;
                char charTemp = s.charAt(begin);
                if (map.get(charTemp) > 1) counter--;
                map.put(charTemp, map.get(charTemp)-1);
                begin++;
            }
            d = Math.max(d, end - begin);
        }
        return d;
    }
}
````
**Longest Substring with At Most Two Distinct Characters**
https://leetcode.com/problems/longest-substring-with-at-most-two-distinct-characters/
```
public class Solution {
    public int lengthOfLongestSubstringTwoDistinct(String s) {
        Map<Character,Integer> map = new HashMap<>();
        int start = 0, end = 0, counter = 0, len = 0;
        while(end < s.length()){
            char c = s.charAt(end);
            map.put(c, map.getOrDefault(c, 0) + 1);
            if(map.get(c) == 1) counter++;//new char
            end++;
            while(counter > 2){
                char cTemp = s.charAt(start);
                map.put(cTemp, map.get(cTemp) - 1);
                if(map.get(cTemp) == 0){
                    counter--;
                }
                start++;
            }
            len = Math.max(len, end-start);
        }
        return len;
    }
}
````

**Substring with Concatenation of All Words**
https://leetcode.com/problems/substring-with-concatenation-of-all-words/
```
public class Solution {
    public List<Integer> findSubstring(String S, String[] L) {
        List<Integer> res = new LinkedList<>();
        if (L.length == 0 || S.length() < L.length * L[0].length())   return res;
        int N = S.length();
        int M = L.length; // *** length
        int wl = L[0].length();
        Map<String, Integer> map = new HashMap<>(), curMap = new HashMap<>();
        for (String s : L) {
            if (map.containsKey(s))   map.put(s, map.get(s) + 1);
            else                      map.put(s, 1);
        }
        String str = null, tmp = null;
        for (int i = 0; i < wl; i++) {
            int count = 0;  // remark: reset count 
            int start = i;
            for (int r = i; r + wl <= N; r += wl) {
                str = S.substring(r, r + wl);
                if (map.containsKey(str)) {
                    if (curMap.containsKey(str))   curMap.put(str, curMap.get(str) + 1);
                    else                           curMap.put(str, 1);
                    
                    if (curMap.get(str) <= map.get(str))    count++;
                    while (curMap.get(str) > map.get(str)) {
                        tmp = S.substring(start, start + wl);
                        curMap.put(tmp, curMap.get(tmp) - 1);
                        start += wl;
                        
                        //the same as https://leetcode.com/problems/longest-substring-without-repeating-characters/
                        if (curMap.get(tmp) < map.get(tmp)) count--;
                        
                    }
                    if (count == M) {
                        res.add(start);
                        tmp = S.substring(start, start + wl);
                        curMap.put(tmp, curMap.get(tmp) - 1);
                        start += wl;
                        count--;
                    }
                }else {
                    curMap.clear();
                    count = 0;
                    start = r + wl;//not contain, so move the start
                }
            }
            curMap.clear();
        }
        return res;
    }
}
````

**Find All Anagrams in a String**
https://leetcode.com/problems/find-all-anagrams-in-a-string/
```
public class Solution {
    public List<Integer> findAnagrams(String s, String t) {
        List<Integer> result = new LinkedList<>();
        if(t.length()> s.length()) return result;
        Map<Character, Integer> map = new HashMap<>();
        for(char c : t.toCharArray()){
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        int counter = map.size();
        
        int begin = 0, end = 0;
        int head = 0;
        int len = Integer.MAX_VALUE;
        
        
        while(end < s.length()){
            char c = s.charAt(end);
            if( map.containsKey(c) ){
                map.put(c, map.get(c)-1);
                if(map.get(c) == 0) counter--;
            }
            end++;
            
            while(counter == 0){
                char tempc = s.charAt(begin);
                if(map.containsKey(tempc)){
                    map.put(tempc, map.get(tempc) + 1);
                    if(map.get(tempc) > 0){
                        counter++;
                    }
                }
                if(end-begin == t.length()){
                    result.add(begin);
                }
                begin++;
            }
            
        }
        return result;
    }
}
````
</p>


### Simple O(n) java solution - easily extend to k characters
- Author: kevinhsu
- Creation Date: Fri Oct 09 2015 01:56:49 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 04:08:29 GMT+0800 (Singapore Standard Time)

<p>
The main idea is to maintain a sliding window with 2 unique characters. The key is to store the last occurrence of each character as the value in the hashmap. This way, whenever the size of the hashmap exceeds 2, we can traverse through the map to find the character with the left most index, and remove 1 character from our map. Since the range of characters is constrained, we should be able to find the left most index in constant time. 

    public class Solution {
        public int lengthOfLongestSubstringTwoDistinct(String s) {
            if(s.length() < 1) return 0;
            HashMap<Character,Integer> index = new HashMap<Character,Integer>();
            int lo = 0;
            int hi = 0;
            int maxLength = 0;
            while(hi < s.length()) {
                if(index.size() <= 2) {
                    char c = s.charAt(hi);
                    index.put(c, hi);
                    hi++;
                }
                if(index.size() > 2) {
                    int leftMost = s.length();
                    for(int i : index.values()) {
                        leftMost = Math.min(leftMost,i);
                    }
                    char c = s.charAt(leftMost);
                    index.remove(c);
                    lo = leftMost+1;
                }
                maxLength = Math.max(maxLength, hi-lo);
            }
            return maxLength;
        }
    }
</p>


### Share my c++ solution
- Author: morrischen2008
- Creation Date: Fri Jan 16 2015 06:57:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 01:45:46 GMT+0800 (Singapore Standard Time)

<p>
This question belong to the same category as those such as "longest substring without repeating characters", "minimum window substring", and "substring with concatenation of all words". To solve this kind of question we can use two pointers and a hash table. When the key of the hash table is char, we can simply use an array as the hash table. The most important idea in solving this kind of questions is "how to update the "start" pointer" and the solution to these questions seem usually differ only in this respect. 

    int lengthOfLongestSubstringTwoDistinct(string s) {
        if(s.empty()) return 0;
        
        int dict[256]; 
        fill_n(dict,256,0);
        int start = 0, len = 1, count = 0;
        for(int i=0; i<s.length(); i++) {
            dict[s[i]]++;
            if(dict[s[i]] == 1) { // new char
                count++;
                while(count > 2) {
                    dict[s[start]]--;
                    if(dict[s[start]] == 0) count--; 
                    start++;
                }
            }
            if(i-start+1 > len) len = i-start+1;
        }
        return len;
    }
</p>


