---
title: "Word Subsets"
weight: 866
#id: "word-subsets"
---
## Description
<div class="description">
<p>We are given two arrays <code>A</code> and <code>B</code> of words.&nbsp; Each word is a string of lowercase letters.</p>

<p>Now, say that&nbsp;word <code>b</code> is a subset of word <code>a</code><strong>&nbsp;</strong>if every letter in <code>b</code> occurs in <code>a</code>, <strong>including multiplicity</strong>.&nbsp; For example, <code>&quot;wrr&quot;</code> is a subset of <code>&quot;warrior&quot;</code>, but is not a subset of <code>&quot;world&quot;</code>.</p>

<p>Now say a word <code>a</code> from <code>A</code> is <em>universal</em> if for every <code>b</code> in <code>B</code>, <code>b</code>&nbsp;is a subset of <code>a</code>.&nbsp;</p>

<p>Return a list of all universal words in <code>A</code>.&nbsp; You can return the words in any order.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[&quot;amazon&quot;,&quot;apple&quot;,&quot;facebook&quot;,&quot;google&quot;,&quot;leetcode&quot;]</span>, B = <span id="example-input-1-2">[&quot;e&quot;,&quot;o&quot;]</span>
<strong>Output: </strong><span id="example-output-1">[&quot;facebook&quot;,&quot;google&quot;,&quot;leetcode&quot;]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">[&quot;amazon&quot;,&quot;apple&quot;,&quot;facebook&quot;,&quot;google&quot;,&quot;leetcode&quot;]</span>, B = <span id="example-input-2-2">[&quot;l&quot;,&quot;e&quot;]</span>
<strong>Output: </strong><span id="example-output-2">[&quot;apple&quot;,&quot;google&quot;,&quot;leetcode&quot;]</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-3-1">[&quot;amazon&quot;,&quot;apple&quot;,&quot;facebook&quot;,&quot;google&quot;,&quot;leetcode&quot;]</span>, B = <span id="example-input-3-2">[&quot;e&quot;,&quot;oo&quot;]</span>
<strong>Output: </strong><span id="example-output-3">[&quot;facebook&quot;,&quot;google&quot;]</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-4-1">[&quot;amazon&quot;,&quot;apple&quot;,&quot;facebook&quot;,&quot;google&quot;,&quot;leetcode&quot;]</span>, B = <span id="example-input-4-2">[&quot;lo&quot;,&quot;eo&quot;]</span>
<strong>Output: </strong><span id="example-output-4">[&quot;google&quot;,&quot;leetcode&quot;]</span>
</pre>

<div>
<p><strong>Example 5:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-5-1">[&quot;amazon&quot;,&quot;apple&quot;,&quot;facebook&quot;,&quot;google&quot;,&quot;leetcode&quot;]</span>, B = <span id="example-input-5-2">[&quot;ec&quot;,&quot;oc&quot;,&quot;ceo&quot;]</span>
<strong>Output: </strong><span id="example-output-5">[&quot;facebook&quot;,&quot;leetcode&quot;]</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length, B.length &lt;= 10000</code></li>
	<li><code>1 &lt;= A[i].length, B[i].length&nbsp;&lt;= 10</code></li>
	<li><code>A[i]</code> and <code>B[i]</code> consist only of lowercase letters.</li>
	<li>All words in <code>A[i]</code> are unique: there isn&#39;t <code>i != j</code> with <code>A[i] == A[j]</code>.</li>
</ol>
</div>
</div>
</div>
</div>
</div>

</div>

## Tags
- String (string)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Reduce to Single Word in B

**Intuition**

If `b` is a subset of `a`, then say `a` is a superset of `b`.  Also, say $$N_{\text{"a"}}(\text{word})$$ is the count of the number of $$\text{"a"}$$'s in the word.

When we check whether a word `wordA` in `A` is a superset of `wordB`, we are individually checking the counts of letters: that for each $$\text{letter}$$, we have $$N_{\text{letter}}(\text{wordA}) \geq N_{\text{letter}}(\text{wordB})$$.

Now, if we check whether a word `wordA` is a superset of all words $$\text{wordB}_i$$, we will check for each letter and each $$i$$, that $$N_{\text{letter}}(\text{wordA}) \geq N_{\text{letter}}(\text{wordB}_i)$$.  This is the same as checking $$N_{\text{letter}}(\text{wordA}) \geq \max\limits_i(N_{\text{letter}}(\text{wordB}_i))$$.

For example, when checking whether `"warrior"` is a superset of words `B = ["wrr", "wa", "or"]`,  we can combine these words in `B` to form a "maximum" word `"arrow"`, that has the maximum count of every letter in each word in `B`.

**Algorithm**

Reduce `B` to a single word `bmax` as described above, then compare the counts of letters between words `a` in `A`, and `bmax`.

<iframe src="https://leetcode.com/playground/fYa89qiH/shared" frameBorder="0" width="100%" height="500" name="fYa89qiH"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\mathcal{A} + \mathcal{B})$$, where $$\mathcal{A}$$ and $$\mathcal{B}$$ is the total amount of information in `A` and `B` respectively.

* Space Complexity:  $$O(A\text{.length} + B\text{.length})$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Sep 30 2018 11:04:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 07 2020 14:03:36 GMT+0800 (Singapore Standard Time)

<p>
For each word `b` in `B`,
we use function `counter` to count occurrence of each letter.
We take the maximum occurrences of counts, and use it as a filter of `A`.

**C++:**
```
    vector<string> wordSubsets(vector<string>& A, vector<string>& B) {
        vector<int> count(26), tmp(26);
        int i;
        for (string b : B) {
            tmp = counter(b);
            for (i = 0; i < 26; ++i)
                count[i] = max(count[i], tmp[i]);
        }
        vector<string> res;
        for (string a : A) {
            tmp = counter(a);
            for (i = 0; i < 26; ++i)
                if (tmp[i] < count[i])
                    break;
            if (i == 26) res.push_back(a);
        }
        return res;
    }

    vector<int> counter(string& word) {
        vector<int> count(26);
        for (char c : word) count[c - \'a\']++;
        return count;
    }
```
**Java:**
```
    public List<String> wordSubsets(String[] A, String[] B) {
        int[] count = new int[26], tmp;
        int i;
        for (String b : B) {
            tmp = counter(b);
            for (i = 0; i < 26; ++i)
                count[i] = Math.max(count[i], tmp[i]);
        }
        List<String> res = new ArrayList<>();
        for (String a : A) {
            tmp = counter(a);
            for (i = 0; i < 26; ++i)
                if (tmp[i] < count[i])
                    break;
            if (i == 26) res.add(a);
        }
        return res;
    }

    int[] counter(String word) {
        int[] count = new int[26];
        for (char c : word.toCharArray()) count[c - \'a\']++;
        return count;
    }
```
**Python:**
@grter2 suggested:
```
    def wordSubsets(self, A, B):
        count = collections.Counter()
        for b in B:
            count = count | collections.Counter(b)
        return [a for a in A if Counter(a) & count == count]
```

</p>


### My Python solution. Beats 98.4%.
- Author: sharadbhat
- Creation Date: Fri Nov 02 2018 02:37:57 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Nov 02 2018 02:37:57 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def wordSubsets(self, A, B):
        """
        :type A: List[str]
        :type B: List[str]
        :rtype: List[str]
        """
        s = set(A)
        letters_required = {}
        for i in B:
            for j in i:
                count = i.count(j)
                if j not in letters_required or count > letters_required[j]:
                    letters_required[j] = count

        for i in A:
            for j in letters_required:
                if i.count(j) < letters_required[j]:
                    s.remove(i)
                    break
        return list(s)
```
</p>


### [Java/Python 3] Time O(A + B) clean codes - count the most frequent char of words in B
- Author: rock
- Creation Date: Sun Sep 30 2018 11:03:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 01 2020 02:24:06 GMT+0800 (Singapore Standard Time)

<p>
e.g., if B = ["o", "oo"], count[\'o\' - \'a\'] = 2 (NOT 3);
Do NOT use count[0][o - \'a\'] = 1, count[1][o - \'a\'] = 2, which would cost too much time. I failed by using such way with a TLE, before correcting it.
```java
    public List<String> wordSubsets(String[] A, String[] B) {
        int[] count = new int[26];
        for (String b : B) { 
            int[] bCnt = new int[26];
            for (char c : b.toCharArray()) { ++bCnt[c - \'a\']; } // count b\'s char. 
            for (int i = 0; i < 26; ++i) { count[i] = Math.max(count[i], bCnt[i]); } // count the max frequency.
        } 
        List<String> ans = new ArrayList<>();
        outer: for (String a : A) {
            int[] aCnt = new int[26]; 
            for (char c : a.toCharArray()) { ++aCnt[c - \'a\']; } // count a\'s char.
            // if the occurrency of char (\'a\' + i) in B is more frequent than 
            // that in a, ignore it.
            for (int i = 0; i < 26; ++i) { if (count[i] > aCnt[i]) continue outer; }    
            ans.add(a);
        }
        return ans;
    }
```
```python
    def wordSubsets(self, A: List[str], B: List[str]) -> List[str]:
        cnt = collections.Counter
        bCnt = cnt()
        for b in B:
            bCnt |= cnt(b)
        return [a for a in A if not (bCnt - cnt(a))]
```
**Analysis:**

Time & space: O(A + B), where A & B are the number of total characters in `A` & `B`, respectively.
</p>


