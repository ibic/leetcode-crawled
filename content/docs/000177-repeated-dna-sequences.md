---
title: "Repeated DNA Sequences"
weight: 177
#id: "repeated-dna-sequences"
---
## Description
<div class="description">
<p>All DNA is composed of a series of nucleotides abbreviated as A, C, G, and T, for example: &quot;ACGAATTCCG&quot;. When studying DNA, it is sometimes useful to identify repeated sequences within the DNA.</p>

<p>Write a function to find all the 10-letter-long sequences (substrings) that occur more than once in a DNA molecule.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT&quot;

<strong>Output:</strong> [&quot;AAAAACCCCC&quot;, &quot;CCCCCAAAAA&quot;]
</pre>

</div>

## Tags
- Hash Table (hash-table)
- Bit Manipulation (bit-manipulation)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Google - 8 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

Follow-up here is to solve the same problem for 
arbitrary sequence length $$L$$,
and to check the situation when $$L$$ is quite large. 
Hence let's use $$L = 10$$ notation everywhere to ease the problem generalisation.

> We will discuss three different ideas how to proceed.
They are all based on sliding window + hashset. 
The key point is how to implement a window slice. 

Linear-time window slice $$\mathcal{O}(L)$$ is easy stupid,
just take a substring. 
Overall that would result in 
$$\mathcal{O}((N - L) L)$$ time complexity and 
huge space consumption in the case of large sequences.

Constant-time slice $$\mathcal{O}(1)$$ is where the fun starts,
because the way you choose will show your actual background.
There are two ways to proceed: 

- Rabin-Karp = constant-time slice using rolling hash algorithm.

- Bit manipulation = constant-time slice using bitmasks.

Last two approaches have $$\mathcal{O}(N - L)$$ time complexity and 
moderate space consumption even in the case of large sequences.

![pic](../Figures/187/algorithms.png)
<br /> 
<br />


---
#### Approach 1: Linear-time Slice Using Substring + HashSet

The idea is straightforward : 

- Move a sliding window of length L along the string of length N.
 
- Check if the sequence in the sliding window
is in the hashset of already seen sequences. 

    - If yes, the repeated sequence is right here. Update the output.
    
    - If not, save the sequence in the sliding window in the hashset.  

![pic](../Figures/187/hashes2.png)

<iframe src="https://leetcode.com/playground/KJnqFkpQ/shared" frameBorder="0" width="100%" height="293" name="KJnqFkpQ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}((N - L)L)$$, that is $$\mathcal{O}(N)$$
for the constant $$L = 10$$. 
In the loop executed $$N - L + 1$$ one builds a 
substring of length $$L$$. Overall that results in $$\mathcal{O}((N - L)L)$$
time complexity.

* Space complexity : $$\mathcal{O}((N - L)L)$$ to keep the hashset, 
that results in $$\mathcal{O}(N)$$ for the constant $$L = 10$$. 
<br /> 
<br />


---
#### Approach 2: Rabin-Karp : Constant-time Slice Using Rolling Hash

Rabin-Karp algorithm is used to perform a multiple pattern search.
It's used for plagiarism detection and 
in bioinformatics to look for similarities in two or more proteins. 

Detailed implementation of Rabin-Karp algorithm for quite a 
complex case you could find in the article
[Longest Duplicate Substring](https://leetcode.com/articles/longest-duplicate-substring/),
here we do a very basic implementation.

> The idea is to slice over the string and to compute
the hash of the sequence in the sliding window, both in a constant time.

Let's use string `AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT` as an example.
First, convert string to integer array:

- 'A' -> 0, 'C' -> 1, 'G' -> 2, 'T' -> 3

`AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT` -> `00000111110000011111100000222333`.
Time to compute hash for the first sequence of length L:
`0000011111`. The sequence could be considered as a number 
in a [numeral system](https://en.wikipedia.org/wiki/Numeral_system) 
with the base 4 and hashed as 

$$h_0 = \sum_{i = 0}^{L - 1}{c_i 4^{L - 1 - i}}$$

Here $$c_{0..4} = 0$$ and $$c_{5..9} = 1$$ are digits of `0000011111`.

Now let's consider the slice `AAAAACCCCC` -> `AAAACCCCCA`. 
For int arrays that means `0000011111` -> `0000111110`, 
to remove leading 0 and to add trailing 0.
One integer in, and one out, let's recompute the hash:

$$h_1 = (h_0 \times 4 - c_0 4^L) + c_{L + 1}$$.

Voila, window slice and hash recomputation are both done in a constant time.
 
**Algorithm**
        
- Iterate over the start position of sequence : from 1 to $$N - L$$.

    - If `start == 0`, compute the hash of the first sequence `s[0: L]`. 
        
    - Otherwise, compute rolling hash from the previous hash value.
            
    - If hash is in the hashset,
    one met a repeated sequence, time to update the output. 
            
    - Otherwise, add hash in the hashset.
         
- Return output list.
 
**Implementation**

<iframe src="https://leetcode.com/playground/LHZ8DjUE/shared" frameBorder="0" width="100%" height="500" name="LHZ8DjUE"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N - L)$$, that is $$\mathcal{O}(N)$$
for the constant $$L = 10$$. 
In the loop executed $$N - L + 1$$ one builds a 
hash in a constant time, that results in $$\mathcal{O}(N - L)$$
time complexity.

* Space complexity : $$\mathcal{O}(N - L)$$ to keep the hashset, 
that results in $$\mathcal{O}(N)$$ for the constant $$L = 10$$.
<br /> 
<br />


---
#### Approach 3: Bit Manipulation : Constant-time Slice Using Bitmask

> The idea is to slice over the string and to compute
the bitmask of the sequence in the sliding window, both in a constant time.

As for Rabin-Karp, let's start from conversion of string to 2-bits integer array:

$$A -> 0 = 00_2, \quad C -> 1 = 01_2, \quad G -> 2 = 10_2, \quad T -> 3 = 11_2$$

`GAAAAACCCCCAAAAACCCCCCAAAAAGGGTTT` -> `200000111110000011111100000222333`.

Time to compute bitmask for the first sequence of length L: `2000001111`.
Each digit in the sequence (0, 1, 2 or 3) takes not more than 2 bits:

$$0 = 00_2, \quad 1 = 01_2, \quad 2 = 10_2, \quad 3 = 11_2$$

Hence the bitmask could be computed in the loop:

- Do left shift to free the last two bits: `bitmask <<= 2`

- Save current digit from `2000001111` 
in these last two bits: `bitmask |= nums[i]`

![pic](../Figures/187/first_bitmask2.png)

Now let's consider the slice `GAAAAACCCCC` -> `AAAAACCCCC`. 
For int arrays that means `20000011111` -> `0000011111`, 
to remove leading 2 and to add trailing 1.

![pic](../Figures/187/slice.png)

To add trailing 1 is simple, the same idea as just above: 

- Do left shift to free the last two bits: `bitmask <<= 2`

- Save 1 into these last two bits: `bitmask |= 1`

Now the problem is to remove two leading bits, which contain 2.
In other words, the problem is to set 2L-bit and (2L + 1)-bit to zero.

> Let's use bitwise trick to unset n-th bit: `bitmask &= ~(1 << n)`.

This trick is very simple:

- `1 << n` is to set n-th bit equal to 1.

- `~(1 << n)` is to set n-th bit equal to 0, and all lower bits to 1.

- `bitmask &= ~(1 << n)` is to set n-th bit of bitmask equal to 0.

Straightforward trick usage is to unset first 2L-bit and then (2L + 1)-bit:
`bitmask &= ~(1 << 2 * L) & ~(1 << (2 * L + 1)`. That could be simplified
as `bitmask &= ~(3 << 2 * L)`:

- $$3 = (11)_2$$, and hence `3 << 2 * L` would set 2L-bit and (2L + 1)-bit
equal to 1.

- `~(3 << 2 * L)` would set 2L-bit and (2L + 1)-bit equal to 0,
and all lower bits to 1.

- `bitmask &= ~(3 << 2 * L)` would set 2L-bit and (2L + 1)-bit 
of bitmask equal to 0.

![pic](../Figures/187/unset.png)

Voila, window slice and bitmask recomputation are both done in a constant time.

**Algorithm**

- Iterate over the start position of sequence : from 1 to $$N - L$$.

    - If `start == 0`, compute the bitmask of the first sequence `s[0: L]`. 
        
    - Otherwise, compute bitmask from the previous bitmask.
            
    - If bitmask is in the hashset,
    one met a repeated sequence, time to update the output. 
            
    - Otherwise, add bitmask in the hashset.
         
- Return output list.

**Implementation**

<iframe src="https://leetcode.com/playground/zSLkPVuQ/shared" frameBorder="0" width="100%" height="500" name="zSLkPVuQ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N - L)$$, that is $$\mathcal{O}(N)$$
for the constant $$L = 10$$. 
In the loop executed $$N - L + 1$$ one builds a 
bitmask in a constant time, that results in $$\mathcal{O}(N - L)$$
time complexity.

* Space complexity : $$\mathcal{O}(N - L)$$ to keep the hashset, 
that results in $$\mathcal{O}(N)$$ for the constant $$L = 10$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 7 lines simple Java, O(n)
- Author: StefanPochmann
- Creation Date: Mon Oct 19 2015 21:35:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 03:50:04 GMT+0800 (Singapore Standard Time)

<p>
    public List<String> findRepeatedDnaSequences(String s) {
        Set seen = new HashSet(), repeated = new HashSet();
        for (int i = 0; i + 9 < s.length(); i++) {
            String ten = s.substring(i, i + 10);
            if (!seen.add(ten))
                repeated.add(ten);
        }
        return new ArrayList(repeated);
    }
</p>


### Clean Java solution (hashmap + bits manipulation)
- Author: crazyirontoiletpaper
- Creation Date: Sun Feb 15 2015 09:03:46 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 08:22:32 GMT+0800 (Singapore Standard Time)

<p>
    public List<String> findRepeatedDnaSequences(String s) {
        Set<Integer> words = new HashSet<>();
        Set<Integer> doubleWords = new HashSet<>();
        List<String> rv = new ArrayList<>();
        char[] map = new char[26];
        //map['A' - 'A'] = 0;
        map['C' - 'A'] = 1;
        map['G' - 'A'] = 2;
        map['T' - 'A'] = 3;

        for(int i = 0; i < s.length() - 9; i++) {
            int v = 0;
            for(int j = i; j < i + 10; j++) {
                v <<= 2;
                v |= map[s.charAt(j) - 'A'];
            }
            if(!words.add(v) && doubleWords.add(v)) {
                rv.add(s.substring(i, i + 10));
            }
        }
        return rv;
    }
</p>


### I did it in 10 lines of C++
- Author: JayXon
- Creation Date: Fri Feb 06 2015 13:54:05 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Feb 06 2015 13:54:05 GMT+0800 (Singapore Standard Time)

<p>
The main idea is to store the substring as int in map to bypass the memory limits.

There are only four possible character A, C, G, and T, but I want to use 3 bits per letter instead of 2.

Why? It's easier to code.

A is 0x41, C is 0x43, G is 0x47, T is 0x54. Still don't see it? Let me write it in octal.

A is 0101, C is 0103, G is 0107, T is 0124. The last digit in octal are different for all four letters. That's all we need!

We can simply use `s[i] & 7` to get the last digit which are just the last 3 bits, it's much easier than lookup table or switch or a bunch of if and else, right?

We don't really need to generate the substring from the int. While counting the number of occurrences, we can push the substring into result as soon as the count becomes 2, so there won't be any duplicates in the result.

    vector<string> findRepeatedDnaSequences(string s) {
        unordered_map<int, int> m;
        vector<string> r;
        int t = 0, i = 0, ss = s.size();
        while (i < 9)
            t = t << 3 | s[i++] & 7;
        while (i < ss)
            if (m[t = t << 3 & 0x3FFFFFFF | s[i++] & 7]++ == 1)
                r.push_back(s.substr(i - 10, 10));
        return r;
    }

BTW, the OJ doesn't seems to have test cases which the given string length is smaller than 9, so I didn't check it to make the code simpler.

Any suggestions?

Update:

I realised that I can use `s[i] >> 1 & 3` to get 2 bits, but then I won't be able to remove the first loop as 1337c0d3r suggested.
</p>


