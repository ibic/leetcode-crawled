---
title: "Big Countries"
weight: 1494
#id: "big-countries"
---
## Description
<div class="description">
<p>There is a table <code>World</code></p>

<pre>
+-----------------+------------+------------+--------------+---------------+
| name            | continent  | area       | population   | gdp           |
+-----------------+------------+------------+--------------+---------------+
| Afghanistan     | Asia       | 652230     | 25500100     | 20343000      |
| Albania         | Europe     | 28748      | 2831741      | 12960000      |
| Algeria         | Africa     | 2381741    | 37100000     | 188681000     |
| Andorra         | Europe     | 468        | 78115        | 3712000       |
| Angola          | Africa     | 1246700    | 20609294     | 100990000     |
+-----------------+------------+------------+--------------+---------------+
</pre>

<p>A country is big if it has an area of bigger than 3 million square km or a population of more than 25 million.</p>

<p>Write a SQL solution to output big countries&#39; name, population and area.</p>

<p>For example, according to the above table, we should output:</p>

<pre>
+--------------+-------------+--------------+
| name         | population  | area         |
+--------------+-------------+--------------+
| Afghanistan  | 25500100    | 652230       |
| Algeria      | 37100000    | 2381741      |
+--------------+-------------+--------------+
</pre>

<p>&nbsp;</p>

</div>

## Tags


## Companies
- Adobe - 4 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach I: Using `WHERE` clause and `OR` [Accepted]

**Intuition**

Use `WHERE` clause in SQL to filter these records and get the target countries.

**Algorithm**

According to the definition, a big country meets at least one of the following two conditions:
1. It has an area of bigger than 3 million square km.
2. It has a population of more than 25 million.

So for the first condition, we can use the following code to get the big countries of this type.
```sql
SELECT name, population, area FROM world WHERE area > 3000000
```

In addition, we can use below code to get big countries of more than 25 million people.
```sql
SELECT name, population, area FROM world WHERE population > 25000000
```

As most people may already come into mind, we can use `OR` to combine these two solutions for the two sub-problems together.

**MySQL**

```sql
SELECT
    name, population, area
FROM
    world
WHERE
    area > 3000000 OR population > 25000000
;
```

#### Approach II: Using `WHERE` clause and `UNION` [Accepted]

**Algorithm**

The idea of this approach is the same as the first one. However, we use `UNION` instead of `OR`.

**MySQL**

```sql
SELECT
    name, population, area
FROM
    world
WHERE
    area > 3000000

UNION

SELECT
    name, population, area
FROM
    world
WHERE
    population > 25000000
;
```
>Note: This solution runs a little bit faster than the first one. However, they do not have big difference.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Union and OR and the Explanation
- Author: new2500
- Creation Date: Sun Jul 16 2017 11:27:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 08:22:35 GMT+0800 (Singapore Standard Time)

<p>
Two obvious solutions:
```
#OR
SELECT name, population, area
FROM World
WHERE area > 3000000 OR population > 25000000
```
And Faster Union
```
#Union
SELECT name, population, area
FROM World
WHERE area > 3000000 

UNION

SELECT name, population, area
FROM World
WHERE population > 25000000
```

Why `Union` is faster than `OR`? 

Strictly speaking, Using ` UNION` is faster when it comes to cases like **scan two different column like this**. 

(Of course using `UNION ALL` is much faster than `UNION` since we don't need to sort the result. But it violates the requirements)


Suppose we are searching `population` and `area`, Given that MySQL usually uses one one index per table in a given query, so when it uses the 1st index rather than 2nd index, it would still have to do a table-scan to find rows that fit the 2nd index. 

When using `UNION`, each sub-query can use the index of its search, then combine the sub-query by `UNION`.


I quote from a [benchmark](http://www.sql-server-performance.com/2011/union-or-sql-server-queries/) about `UNION` and `OR`, feel free to check it out:

```
Scenario 3: Selecting all columns for different fields
            CPU      Reads        Duration       Row Counts
OR           47       1278           443           1228
UNION        31       1334           400           1228

Scenario 4: Selecting Clustered index columns for different fields
            CPU      Reads        Duration       Row Counts
OR           0         319           366           1228
UNION        0          50           193           1228
```
</p>


### Description and author's solution are inconsistent
- Author: Viers
- Creation Date: Fri Jun 16 2017 22:56:32 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 16 2017 22:56:32 GMT+0800 (Singapore Standard Time)

<p>
A description says, that 

"A country is big if it has an area of bigger than 3 million square km or a population of more than 25 million."

yet an author's solution also think that the country is big if it has _exactly_ 3 million sq. km or 25 million population. There is no test for that case, but perhaps it will be better to update the author's solution to strict comparsion, or rewrite the description like

"A country is big if it has an area of 3 million square km or bigger, or a population of 25 million or more."
</p>


### Easy AC
- Author: LeiYu
- Creation Date: Mon May 22 2017 20:16:38 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 22 2017 20:16:38 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT name,population,area 
FROM World 
WHERE population>25000000 OR area>3000000;
```
</p>


