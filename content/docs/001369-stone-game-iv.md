---
title: "Stone Game IV"
weight: 1369
#id: "stone-game-iv"
---
## Description
<div class="description">
<p>Alice and Bob take turns playing a game, with Alice starting first.</p>

<p>Initially, there are <code>n</code> stones in a pile.&nbsp; On each player&#39;s turn, that player makes a&nbsp;<em>move</em>&nbsp;consisting of removing <strong>any</strong> non-zero <strong>square number</strong> of stones in the pile.</p>

<p>Also, if a player cannot make a move, he/she loses the game.</p>

<p>Given a positive&nbsp;integer <code>n</code>.&nbsp;Return&nbsp;<code>True</code>&nbsp;if and only if Alice wins the game otherwise return <code>False</code>, assuming both players play optimally.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> true
<strong>Explanation: </strong>Alice can remove 1 stone winning the game because Bob doesn&#39;t have any moves.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2
<strong>Output:</strong> false
<strong>Explanation: </strong>Alice can only remove 1 stone, after that Bob removes the last one winning the game (2 -&gt; 1 -&gt; 0).</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 4
<strong>Output:</strong> true
<strong>Explanation:</strong> n is already a perfect square, Alice can win with one move, removing 4 stones (4 -&gt; 0).
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 7
<strong>Output:</strong> false
<strong>Explanation: </strong>Alice can&#39;t win the game if Bob plays optimally.
If Alice starts removing 4 stones, Bob will remove 1 stone then Alice should remove only 1 stone and finally Bob removes the last one (7 -&gt; 3 -&gt; 2 -&gt; 1 -&gt; 0). 
If Alice starts removing 1 stone, Bob will remove 4 stones then Alice only can remove 1 stone and finally Bob removes the last one (7 -&gt; 6 -&gt; 2 -&gt; 1 -&gt; 0).</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 17
<strong>Output:</strong> false
<strong>Explanation: </strong>Alice can&#39;t win the game if Bob plays optimally.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

### Overview

You probably can guess from the problem title, this is the fourth problem in the series of [Stone Game](https://leetcode.com/problems/stone-game/) problems. Those problems are similar, but have considerable differences, making their solutions quite different. It's highly recommended to finish them all.

Here, two approaches are introduced: DFS with memorization and DP approach.

---

#### Approach 1: DFS with memorization

**Intuition**

First, let's analyze the problem.

According to [Zermelo's_theorem](https://en.wikipedia.org/wiki/Zermelo%27s_theorem_(game_theory)), given `n` stones, either Alice has a must-win strategy, or Bob has one. Therefore, for Alice, the current state is either "must-win" or "must-lose". But how to determine which one it is?

> For convenience, in the following context, "the current player" refers to the player now removing the stones, and "state `i`" refers to when there is `i` stones remaining.
>
> Now the problem asks whether the current player will win in state `n`.

If we can go to a known state where Bob must lose, then we know Alice must win in the current state. All Alice has to do is to move the corresponding number of stones to go to that state. Therefore we need to find out which state Bob must lose.

Note that after going to the next state, Bob becomes the player removing the stones, which is the position of Alice in the current state. Therefore, to find out whether Bob will lose in the next state, we just need to check whether our function gives `False` for remaining stones.

**Algorithm**

Let function `dfs(remain)` represents whether the current player must win with `remain` stones remaining.

To find out the result of `dfs(n)`, we need to iterate `k` from 0 to check whether there exits `dfs(remain - k*k)==False`. To prevent redundant calculate, use a map to store the result of `dfs` function.

Don't forget the base case `dfs(0)==False` and `dfs(1)==True`.

> Note: After reading the Algorithm part, it is recommended to try to write the code on your own before reading the solution code.

<iframe src="https://leetcode.com/playground/kBWFpEMv/shared" frameBorder="0" width="100%" height="463" name="kBWFpEMv"></iframe>

There some tricks that we used in the code above.

In the Python code, we use `@lru_cache` instead of a map to store the result of dfs. It's a useful grammar sugar in Python.

In the Java code, we don't have things like `@lru_cache` in Python, so here we use a simple HashMap. However, we can still use some tricks, if you want -- using an array instead of a map: we can use `0` to mark the unvisited nodes, use `1` to mark the `true` results, and use `2` to mark the `false` results. Also, we can just use an array of bytes, which uses less memory than an array of ints.

Note that the speed would be a little faster if you iterate `i` from `sqrt_root` to `0` due to the data characteristics.

**Complexity Analysis**

Assume $$N$$ is the length of `arr`.

* Time complexity: $$\mathcal{O}(N\sqrt{N})$$ since we spend $$\mathcal{O}(\sqrt{N})$$ at most for each dfs call, and there are $$\mathcal{O}({N})$$ dfs calls in total.
 
* Space complexity: $$\mathcal{O}(N)$$ since we need spaces of $$\mathcal{O}(N)$$ to store the result of dfs.

---

#### Approach 2: DP

**Intuition**

DFS with memorization is very similar to dp. We can also use dp to solve this problem.

We can just use a single `dp[i]` array to store whether the player now removing stones wins with `i` stones remaining.

**Algorithm**

Let `dp[i]` represents the result of the game with `i` stones. `dp[i]==True` means the current player must win, and `dp[i]==False` means the current player must lose, when both players play optimally.

The next step is to find out how to calculate `dp[i]`.

We can iterate all possible movements, and check if we can move to a `False` state. If we can, then we found a must-win strategy, otherwise, we must lose since the opponent has a must-win strategy in this case.

More clearly, we can iterate `k` from 0 and check if there exists `dp[i - k*k]==False`. Of course, `i - k*k >= 0`.

Finally, we only need to return `dp[n]`.

> Note: After reading the Algorithm part, it is recommended to try to write the code on your own before reading the solution code.

<iframe src="https://leetcode.com/playground/85MnJ7NY/shared" frameBorder="0" width="100%" height="293" name="85MnJ7NY"></iframe>

Also, we can employ DP in a slightly different way.

**Intuition**

Let's think in the backtrack way. If we have a state `i` that we know the current player must lose, what can we infer?

-- Any other states that can go to `i` must be `True`. 

Let's say in another state `j` the current player in `j` can go to `i` by removing stones. In this case, the state `j` is `True` since the current player must win.

How to find all the state `j`? Well, we can iterate over the square numbers and add them to `i`.

**Algorithm**

Still, let `dp[i]` represent the result of the game of `i` stones. `dp[i]==True` means the first player (Alice) must win, and `dp[i]==False` means the second player (Bob) must win when both players play optimally.

When we get to a `False` state, we mark all accessible states as `True`. When we get to a `True` state, just continue (Why? Well... because it's useless).

Finally, we only need to return `dp[n]`.

> Note: After reading the Algorithm part, it is recommended to try to write the code on your own before reading the solution code.

<iframe src="https://leetcode.com/playground/6StYw7u8/shared" frameBorder="0" width="100%" height="293" name="6StYw7u8"></iframe>

**Complexity Analysis**

Assume $$N$$ is the length of `arr`.

* Time complexity: $$\mathcal{O}(N\sqrt{N})$$ since we iterate over the `dp` array and spend $$\mathcal{O}(\sqrt{N})$$ at most on each element.
 
* Space complexity: $$\mathcal{O}(N)$$ since we need a `dp` array.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] DP
- Author: lee215
- Creation Date: Sun Jul 12 2020 00:05:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 12 2020 00:13:46 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
`dp[i]` means the result for `n = i`.

if there is any `k` that `dp[i - k * k] == false`,
it means we can make the other lose the game,
so we can win the game an `dp[i] = true`.
<br>

# **Complexity**
Time `O(n^1.5)`
Space `O(N)`
<br>

**Java**
```java
    public boolean winnerSquareGame(int n) {
        boolean[] dp = new boolean[n + 1];
        for (int i = 1; i <= n; ++i) {
            for (int k = 1; k * k <= i; ++k) {
                if (!dp[i - k * k]) {
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[n];
    }
```
**C++**
```cpp
    bool winnerSquareGame(int n) {
        vector<bool> dp(n + 1, false);
        for (int i = 1; i <= n; ++i) {
            for (int k = 1; k * k <= i; ++k) {
                if (!dp[i - k * k]) {
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[n];
    }
```
**Python:**
```py
    def winnerSquareGame(self, n):
        dp = [False] * (n + 1)
        for i in xrange(1, n + 1):
            dp[i] = not all(dp[i - k * k] for k in xrange(1, int(i**0.5) + 1))
        return dp[-1]
```

</p>


### Java | Heavily Commented | Subproblems Visualised
- Author: khaufnak
- Creation Date: Sun Jul 12 2020 00:00:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 12 2020 13:23:14 GMT+0800 (Singapore Standard Time)

<p>
Let\'s enumerate all possible moves for ```n = 7```: 

![image](https://assets.leetcode.com/users/images/6a068930-6f08-4529-8e40-4c748d685c36_1594483737.314722.png)

As you can see, for Alice, there is no subtree that can make him win.

Now let\'s consider for ```n = 8```. Can Alice choose a subtree that can make him win?

![image](https://assets.leetcode.com/users/images/66acab22-ffbe-4265-b26e-5d21792722ae_1594485510.75967.png)

Yes. Alive will go (-1) first move which can make him win.

>Observation: Each of the players asks this question to themselves to play optimally: Can I make a move that will push the other person to a subtree which will eventually make me win.

Note: You can see repeating subproblems, therefore, dynamic programming.

```
class Solution {
    Boolean[] dp = new Boolean[100000 + 1];
    public boolean winnerSquareGame(int n) {
        if (dp[n] != null) {
            return dp[n];
        }
        Boolean answer = false;
        for (int move = 1; n - move * move >= 0; ++move) {
            if (n - move * move == 0) {
                // current player won
                answer = true;
                break;
            } else {
                // Hand over turn to other player.
                // If there is any subtree, where the other person loses. We use that subtree.
                // 1. OR means we choose any winning subtree.
                // 2. ! in !solve(n - move*move, dp) means we hand over turn to other player after reducing n by move*move
                answer |= !winnerSquareGame(n - move * move);
            }
        }
        return dp[n] = answer;
    }
}
```

Complexity: ```O(sqrt(n) * n)```
</p>


### C++ DP solution | Explained
- Author: asthacs
- Creation Date: Sun Jul 12 2020 00:08:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 12 2020 01:37:23 GMT+0800 (Singapore Standard Time)

<p>
Both the players play optimally. dp[i] = true represents that for i th number, Alice can win. False means Alice loses.
Lets assume Alice loses for n=j.
Thus, if at any point i Alice can remove a square number such that the remaining number is equal to j, and j is false, then Alice can win at the point i.

*Time complexity: O(n sqrt(n) )
Space complexity: O(n)*

```
bool winnerSquareGame(int n) {
        if(n==0)
            return false;
			
        vector<bool> dp(n+1, false);        
        for(int i=1; i<=n; i++)
        {
            for(int j=1; j*j<=i; j++)
            {
                if(dp[i-(j*j)]==false)
                {
                    dp[i] = true;
                    break;
                }
            }
        }
        
        return dp[n];
    }
	
</p>


