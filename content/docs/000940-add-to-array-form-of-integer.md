---
title: "Add to Array-Form of Integer"
weight: 940
#id: "add-to-array-form-of-integer"
---
## Description
<div class="description">
<p>For a non-negative integer <code>X</code>, the&nbsp;<em>array-form of <code>X</code></em>&nbsp;is an array of its digits in left to right order.&nbsp; For example, if <code>X = 1231</code>, then the array form is&nbsp;<code>[1,2,3,1]</code>.</p>

<p>Given the array-form <code>A</code> of a non-negative&nbsp;integer <code>X</code>, return the array-form of the integer <code>X+K</code>.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[1,2,0,0]</span>, K = 34
<strong>Output: </strong><span id="example-output-1">[1,2,3,4]</span>
<strong>Explanation: </strong>1200 + 34 = 1234
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">[2,7,4]</span>, K = <span id="example-input-2-2">181</span>
<strong>Output: </strong><span id="example-output-2">[4,5,5]</span>
<strong>Explanation: </strong>274 + 181 = 455
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-3-1">[2,1,5]</span>, K = <span id="example-input-3-2">806</span>
<strong>Output: </strong><span id="example-output-3">[1,0,2,1]</span>
<strong>Explanation: </strong>215 + 806 = 1021
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-4-1">[9,9,9,9,9,9,9,9,9,9]</span>, K = <span id="example-input-4-2">1</span>
<strong>Output: </strong><span id="example-output-4">[1,0,0,0,0,0,0,0,0,0,0]</span>
<strong>Explanation: </strong>9999999999 + 1 = 10000000000
</pre>

<p>&nbsp;</p>

<p><strong>Note：</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 10000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 9</code></li>
	<li><code>0 &lt;= K &lt;= 10000</code></li>
	<li>If <code>A.length &gt; 1</code>, then <code>A[0] != 0</code></li>
</ol>
</div>
</div>
</div>
</div>
</div>

## Tags
- Array (array)

## Companies
- Google - 3 (taggedByAdmin: false)
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Schoolbook Addition

**Intuition**

Let's add numbers in a schoolbook way, column by column.  For example, to add 123 and 912, we add 3+2, then 2+1, then 1+9.  Whenever our addition result is more than 10, we carry the 1 into the next column.  The result is 1035.

**Algorithm**

We can do a variant of the above idea that is easier to implement - we put the entire addend in the first column from the right.

Continuing the example of 123 + 912, we start with [1, 2, 3+912].  Then we perform the addition 3+912, leaving 915.  The 5 stays as the digit, while we 'carry' 910 into the next column which becomes 91.

We repeat this process with [1, 2+91, 5].  We have 93, where 3 stays and 90 is carried over as 9.  Again, we have [1+9, 3, 5] which transforms into [1, 0, 3, 5].

<iframe src="https://leetcode.com/playground/RGn8i5Uh/shared" frameBorder="0" width="100%" height="361" name="RGn8i5Uh"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\max(N, \log K))$$ where $$N$$ is the length of `A`.

* Space Complexity:  $$O(\max(N, \log K))$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Take K itself as a Carry
- Author: lee215
- Creation Date: Sun Feb 10 2019 12:04:03 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 18 2020 11:39:15 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Take `K` as a carry.
Add it to the lowest digit,
Update carry `K`,
and keep going to higher digit.
<br>

## **Complexity**
Insert will take `O(1)` time or `O(N)` time on shifting, depending on the data stucture.
But in this problem `K` is at most 5 digit so this is restricted.
So this part doesn\'t matter.

The overall time complexity is `O(N)`.
For space I\'ll say `O(1)`
<br>

**Java**
```java
    public List<Integer> addToArrayForm(int[] A, int K) {
        List<Integer> res = new LinkedList<>();
        for (int i = A.length - 1; i >= 0; --i) {
            res.add(0, (A[i] + K) % 10);
            K = (A[i] + K) / 10;
        }
        while (K > 0) {
            res.add(0, K % 10);
            K /= 10;
        }
        return res;
    }
```
**Java**
With one loop.
```java
    public List<Integer> addToArrayForm(int[] A, int K) {
        List res = new LinkedList<>();
        for (int i = A.length - 1; i >= 0 || K > 0; --i) {
            res.add(0, (i >= 0 ? A[i] + K : K) % 10);
            K = (i >= 0 ? A[i] + K : K) / 10;
        }
        return res;
    }
```

**C++:**
```cpp
    vector<int> addToArrayForm(vector<int> A, int K) {
        for (int i = A.size() - 1; i >= 0 && K > 0; --i) {
            A[i] += K;
            K = A[i] / 10;
            A[i] %= 10;
        }
        while (K > 0) {
            A.insert(A.begin(), K % 10);
            K /= 10;
        }
        return A;
    }
```

**Python:**
```py
    def addToArrayForm(self, A, K):
        for i in range(len(A) - 1, -1, -1):
            K, A[i] = divmod(A[i] + K, 10)
        return [int(i) for i in str(K)] + A if K else A
```

</p>


### [Java/Python 3] 6 liner w/ comment and analysis
- Author: rock
- Creation Date: Sun Feb 10 2019 12:59:06 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 15 2020 23:29:07 GMT+0800 (Singapore Standard Time)

<p>
# **Note:**

I read several other java solutions, and found `ArrayList.add(0, K % 10)` was used, and it is not `O(1)` but `O(n)` instead. 

`LinkedList.add(0, i)` or `offerFirst(i)` is `O(1)`.

Correct me if I am wrong.

```java
    public List<Integer> addToArrayForm(int[] A, int K) {
        LinkedList<Integer> ans = new LinkedList<>();
        for (int i = A.length - 1; K > 0 || i >= 0; --i, K /= 10) { // loop through A and K, from right to left.
            K += i >= 0 ? A[i] : 0; // Use K as carry over, and add A[i].
            ans.offerFirst(K % 10); // add the least significant digit of K.
        }
        return ans;
    }
```	
```python
    def addToArrayForm(self, A: List[int], K: int) -> List[int]:
        ans, i = [], len(A) - 1
        while K > 0 or i >= 0:
            K, rmd = divmod(K + (A[i] if i >= 0 else 0), 10)
            ans.append(rmd)
            i -= 1
        return reversed(ans)
```
**Analysis:**

**Time & space: O(n + logK)**, where n = A.length.
</p>


### C++ Well Commented Solution [With Explanation] [100%]
- Author: Just__a__Visitor
- Creation Date: Sun Feb 10 2019 12:36:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 10 2019 12:36:32 GMT+0800 (Singapore Standard Time)

<p>
```
/* An important observation ---
1) num%10 gives us the last digit of a number
2) num = num/10 cuts off the last digit of the number 
3) numVector.back() gives us the last digit of the number in vector form
4) numVector.pop_back() cuts off the last digit of the number in vector form
5) The extra space required can be reduced by overwriting the first vector. 
*/


class Solution
{
public:
    vector<int> addToArrayForm(vector<int>& a, int k);
};

/* Returns the sum of 2 numbers in vector form */
vector<int> Solution :: addToArrayForm(vector<int>& a, int k)
{
    // Get the length of the first number
    int n = a.size();
    
    // Vector to store the answer
    vector<int> answer;
    
    /* Start adding both the numbers from the end */
    
    int carry = 0;
    // As long as one of the number exists, keep adding them
    while(!a.empty() || k!=0)
    {
        // Get the last digits of both the numbers. If a vector has finished off, the last digit is zero
        int lastDigit_1 = a.empty() ? 0 : a.back();
        int lastDigit_2 = k%10;
        
        // Sum up the digits and add the carry
        int sum = lastDigit_1 + lastDigit_2 + carry;
        answer.push_back(sum%10);
        carry = sum/10;
        
        // Remove the last digits of both the numbers
        if(!a.empty()) a.pop_back();
        k = k/10;
    }
    
    // If the carry is remaining, add it
    if(carry!=0) answer.push_back(carry);
    
    // Reverse the answer, since we were summing up from the end
    reverse(answer.begin(), answer.end());
    
    // Return the answer in vector format
    return answer;
}
```
</p>


