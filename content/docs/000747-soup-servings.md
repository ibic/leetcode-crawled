---
title: "Soup Servings"
weight: 747
#id: "soup-servings"
---
## Description
<div class="description">
<p>There are two types of soup: type A and type B. Initially we have <code>N</code> ml of each type of soup. There are four kinds of operations:</p>

<ol>
	<li>Serve&nbsp;100 ml of soup A and 0 ml of soup B</li>
	<li>Serve&nbsp;75 ml of soup A and 25&nbsp;ml of soup B</li>
	<li>Serve 50 ml of soup A and 50 ml of soup B</li>
	<li>Serve 25&nbsp;ml of soup A and 75&nbsp;ml of soup B</li>
</ol>

<p>When we serve some soup, we give it to someone and we no longer have it.&nbsp; Each turn,&nbsp;we will choose from the four operations with equal probability 0.25. If the remaining volume of soup is not enough to complete the operation, we will serve&nbsp;as much as we can.&nbsp; We stop once we no longer have some quantity of both types of soup.</p>

<p>Note that we do not have the operation where all 100 ml&#39;s of soup B are used first.&nbsp;&nbsp;</p>

<p>Return the probability that soup A will be empty&nbsp;first, plus half the probability that A and B become empty at the same time.</p>

<p>&nbsp;</p>

<pre>
<strong>Example:</strong>
<strong>Input:</strong> N = 50
<strong>Output:</strong> 0.625
<strong>Explanation:</strong> 
If we choose the first two operations, A will become empty first. For the third operation, A and B will become empty at the same time. For the fourth operation, B will become empty first. So the total probability of A becoming empty first plus half the probability that A and B become empty at the same time, is 0.25 * (1 + 1 + 0.5 + 0) = 0.625.

</pre>

<p><strong>Notes: </strong></p>

<ul>
	<li><code>0 &lt;= N &lt;= 10^9</code>.&nbsp;</li>
	<li>Answers within&nbsp;<code>10^-6</code>&nbsp;of the true value will be accepted as correct.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Dynamic Programming [Accepted]

**Intuition**

First, we can simplify all the numbers by dividing by 25.  More specifically, each unit is 25ml, and partial quantities of 25ml are rounded up to a full quantity.

When `N` is small, this is a relatively straightforward dynamic programming problem: we have quantities of soup represented by the state `(x, y)`, and we can either go to `(x-4, y-0)`, `(x-3, y-1)`, `(x-2, y-2)`, or `(x-1, y-3)` each with equal probability.  

When `N` is very large, this approach fails, so we need a different idea.

Instead of serving in batches of `(4, 0), (3, 1), (2, 2), (1, 3)`, pretend we serve `(1, 0)` on the side first, and then serve from the fair distribution `(3, 0), (2, 1), (1, 2), (0, 3)`.  If the pots of soup initially start at `(N, N)`, then after roughly less than `N/2` servings, one pot will still have soup.  Because of the `(1, 0)` servings on the side, this means that roughly speaking, pot `A` is used first if we serve `N/2` fairly from the first pot before `N` from the second pot.

When `N` is very large, this almost always happens (better than 99.9999%, so we can output 1), and we can check this either experimentally or mathematically.

**Algorithm**

We convert all units by dividing by 25 and rounding up.  If `N >= 500` (in new units), then by the above argument the answer is `1`.

Otherwise, we will perform a dynamic programming algorithm to find the answer.  Our Java implementation showcases a "bottom-up" approach, that fills `memo` diagonally from top left to bottom right, where `s = i + j` is the sum of the indices.  Our Python implemtation showcases a "top-down" approach that uses memoization.

<iframe src="https://leetcode.com/playground/jfNqbm3S/shared" frameBorder="0" width="100%" height="497" name="jfNqbm3S"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(1)$$.  (There exists a constant `C` such that the algorithm never performs more than `C` steps.)

* Space Complexity: $$O(1)$$.  (There exists a constant `C` such that the algorithm never uses more than `C` space.)

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] When N > 4800, just return 1
- Author: lee215
- Creation Date: Sun Apr 01 2018 11:28:22 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 19 2020 11:52:41 GMT+0800 (Singapore Standard Time)

<p>
## **First, 25ml is annoying**
The decription is very difficult to understand, and all ```25ml``` just make it worse.
When I finally figure it out, I consider only how many servings left in A and B.
`1 serving = 25ml`.
Well, it works similar to your milk powder or protin powder.
If the left part is less than `25ml`, it is still considered as one serving.

## **Second, DP or recursion with memory**
Now it\'s easy to solve this problem.
```f(a,b)``` means the result probability for a ml of soup A and b ml of soup B.
```f(a-4,b)``` means that we take the first operation: Serve 100 ml of soup A and 0 ml of soup B. ```f(a-3,b-1), f(a-2,b-2), f(a-1,b-3)``` are other 3 operations.
The condition ```a <= 0 and b <= 0``` means that we run out of soup A and B at the same time, so we should return a probability of ```0.5```, which is half of ```1.0```.
The same idea for other two conditions.
I cached the process as we do for Fibonacci sequence. It calculate every case for only once and it can be reused for every test case. No worries for TLE.

## **Third, take the hint for big N**
"Note that we do not have the operation where all 100 ml\'s of soup B are used first. "
It\'s obvious that A is easier to be empty than B. And when ```N``` gets bigger, we have less chance to run out of B first.
So as ```N``` increases, our result increases and it gets closer to 100 percent = 1.

Answers within `10^-5` of the true value will be accepted as correct.
Now it\'s obvious that when ```N``` is big enough, result is close enough to 1 and we just need to return 1.
When I incresed the value of ```N```, I find that:
When ```N = 4800```, the ```result = 0.999994994426```
When ```N = 4801```, the ```result = 0.999995382315```
So if N>= 4800, just return 1 and it will be enough.

## **Complexity Analysis**

I have to say **this conversion process is necessary**.

The solution using hashmap may luckly get accepted.
Thanks to leetcode infrastructure,
every test cases will run in seperate instances.
(this can be easily tested).

In this case it\'s the same for space and time using hashmap.

But are you writing codes running only once?
How about the case running multiple test cases within the same instance?

Without this conversion,
it needs `O(200 * 200 * 25)` time & space if A == B,
it needs `O(5000 * 5000)` time & space if A != B, (which sounds like 250mb)

But in our solution above, we use only `O(200 * 200)` time & space.


**C++:**
```cpp
    double memo[200][200];
    double soupServings(int N) {
        return N > 4800 ?  1.0 : f((N + 24) / 25, (N + 24) / 25);
    }
    double f(int a, int b) {
        if (a <= 0 && b <= 0) return 0.5;
        if (a <= 0) return 1;
        if (b <= 0) return 0;
        if (memo[a][b] > 0) return memo[a][b];
        memo[a][b] = 0.25 * (f(a-4,b)+f(a-3,b-1)+f(a-2,b-2)+f(a-1,b-3));
        return memo[a][b];
    }
```

**Java:**
```java
    double[][] memo = new double[200][200];
    public double soupServings(int N) {
        return N > 4800 ?  1.0 : f((N + 24) / 25, (N + 24) / 25);
    }

    public double f(int a, int b) {
        if (a <= 0 && b <= 0) return 0.5;
        if (a <= 0) return 1;
        if (b <= 0) return 0;
        if (memo[a][b] > 0) return memo[a][b];
        memo[a][b] = 0.25 * (f(a - 4, b) + f(a - 3, b - 1) + f(a - 2, b - 2) + f(a - 1, b - 3));
        return memo[a][b];
    }
```

**Python:**
```py
class Solution(object):
    memo = {}
    def soupServings(self, N):
        if N > 4800: return 1
        def f(a, b):
            if (a, b) in self.memo: return self.memo[a, b]
            if a <= 0 and b <= 0: return 0.5
            if a <= 0: return 1
            if b <= 0: return 0
            self.memo[(a, b)] = 0.25 * (f(a - 4, b) + f(a - 3, b - 1) + f(a - 2, b - 2) + f(a - 1, b - 3))
            return self.memo[(a, b)]
        N = math.ceil(N / 25.0)
        return f(N, N)

</p>


### A Mathematical Analysis of the Soup Servings Problem
- Author: zerustech
- Creation Date: Mon Nov 19 2018 18:08:10 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 28 2019 18:37:30 GMT+0800 (Singapore Standard Time)

<p>
# <a name="title"></a>A Mathematical Analysis of the Soup Servings Problem

Thank you for this great article @awice about the `soup servings`[[1]] problem,
I really like your solution. However, the following issues have been bothering
me a lot:
1. It seems that `leetcode` is using `4800` as the benchmark value for [`N`][9],
   but what is the logic behind that?
1. After running several experiments, it is not hard to make a guess that the
   limit of the possibility for `A being used up before B` is `1` as `N`
approaches `infinity`. However, is there a mathematical proof for this?

In order to find reasonable answers to these questions, I did some research and
came up with this analysis report. I hope this report might be helpful to those
who have similar confusions as me.

Some of the ideas were inspired by the comment from @lddlinan [[36]].

## <a name="1"></a>1. The threshold of `4800` is wrong!
I commented out the line that returns `1` from your code and tested it with
`4800`, and the result is `0.999994994425816`, so obviously `4800` is not a
correct threshold.  I then experimented your code with several values and found
`5550` is an ideal value:
* <a name="5550"></a>`N = 5550`, `result = 0.9999989925525751`
* `N = 5551`, `result = 0.999999070257936`

I will try to explain and prove this result in the following sections.

## 2. <a name="2"></a>Preparation
First of all, let\'s define some basic terms and variables:
1. <a name="operation"></a>An operation is defined as a pair of integers
   `(x,y)`, representing the amount served for `A`, and `B` respectively, so the
available operations are `(4,0)`, `(3,1)`, `(2,2)`, and `(1,3)`.
1. Assume both `A` and `B` have the same initial volume <a name="n"></a>`N`.
1. <a name="m"></a>`M = N/25`.
1. <a name="max"></a>Define `max = M/2` when `M` is even, and `max = (M+1)/2`
   when `M` is odd.
1. <a name="sa"></a>`SA` = the total virtual amount that has been served for
   `A`.
1. <a name="sb"></a>`SB` = the total virtual amount that has been served for
   `B`.
1. <a name="virtual-amount"></a>In order to normalize the calculation process,
   we don\'t drop the value of `SA` or `SB` to `M` when it is greater than `M`,
so `A` is empty when `SA \u2265 M`, and `B` is empty when `SB \u2265 M`, that is why it is
called `virtual amount`
1. <a name="operation-sequence"></a>An operation sequence is a list of
   operations, seperated by commas, for example `(4,0), (2,2), (1,1)`.
1. <a name="z"></a>`Z = SA - SB`.

Then we will have the following theorems.

### <a name="2.1"></a>2.1 Theorem 1
> Either `A` or `B`, or both must be used up after [`max`][11] serves.

**Proof of theorem 1**:

_Without loss of generality, let\'s assume [`M`][10] is even, so `max = M/2`._

1. Assume both `A` and `B` have been served for `M/2` times, and the [`operation
   sequence`][14] is `(x1, y1), (x2, y2), ... , (xm, ym)`, where `m = M/2`, then
we will have the following equations:
    1. [`SA`][12] `= x1 + x2 + ... + xm`
    1. `xi + yi = 4` 
    1. [`SB`][13] `= y1 + y2 + ... + ym = 4m - SA = 2M - SA`
1. If `SA > M`, then `SB < M`.
1. If `SA = M`, then `SB = M`.
1. If `SA < M`, then `SB > M`.

### <a name="2.2"></a>2.2 Theorem 2
> The value of [`max`][11] is the maximum number of serves. In other words,
> neither `A` nor `B` may be used up in more than `max` serves, before its
> opposite being used up.

**Proof of theorem 2**:

Actually, this is a deduction of [`theorem 1`][18], because at least one type of
soup must have been used up after the `max-th` serve, so if `A` is not used up,
`B` must have, and vice versa, so in either case, it is impossible to exceed the
`max-th` serve.

### <a name="2.3"></a>2.3 Theorem 3
> If `A` and `B` are used up at the same time, they are always used up in exact
> [`max`][11] serves, and [`SA`][12]`=`[`SB`][13]`=`[`M`][10].

**Proof of theorem 3**:

_Again, let\'s assume M is even._

1. Assume both `A` and `B` have been used up after `m` serves, and the
   [`operation sequence`][14] is `(x1, y1), (x2, y2), ... , (xm, ym)`, then we
will have the following statements:
    1. `SA = x1 + x2 + ... + xm \u2265 M`
    1. `SB = y1 + y2 + ... + ym = 4m - SA \u2265 M`
    1. `4m - M \u2265 SA \u2265 M`
1. So we have `m \u2265 M/2`. According to [`theorem 2`][19], `M/2` is the maximum
   number of serves, so `m = M/2`, therefore `SA = SB = M`.

### <a name="2.4"></a>2.4 Theorem 4
> Either `A` or `B` can be used up in less than [`max`][11] serves, but not both
> of them.

**Proof of theorem 4**:

1. Assume both `A` and `B` have been used up by `max` `(2,2)` [`operations`][8].
1. If we replace the last two `(2,2)` operations with a `(4,0)` operation, then
   `A` is used up in `max - 1` serves.
1. If we replace the last three operations with `(1,3), (1,3)`, then `B` is used
   up in `max - 1` serves. 
1. So it is possible to use up either one in less than `max` serves.
1. According to [`theorem 3`][20], it is impossible to use up both A and B in
   less than `max` serves.


### <a name="2.5"></a>2.5 Theorem 5

> If `A` is used up in less than [`max`][11] serves, according to [`theorem
> 4`][21], `B` must not be empty, then obviously we will have
> [`SA`][12]`\u2265`[`M`][10], [`SB`][13]`< M`, and `SA - SB > 0`.
From this point, if we continue to serve until the `max-th` serve has been
reached, then for **each** of the `max-th` serve, we will still have `SA \u2265 M`,
`SB < M`, and `SA - SB > 0`.

> In other words, in this case, extending the number of serves to `max` won\'t
> change the probability.

For example, assume `M = 4`, and `A` has been used up by operation `(4,0)`, so
`SA = 4`, `SB = 0`, `SA - SB = 4`. Now extending the number of serves to `2`, we
might have the following results:
1. Operation `(4,0)` results in `SA = 8`, `SB = 0`, and `SA - SB = 8`.
1. Operation `(3,1)` results in `SA = 7`, `SB = 1`, and `SA - SB = 6`.
1. Operation `(2,2)` results in `SA = 6`, `SB = 2`, and `SA - SB = 4`.
1. Operation `(1,3)` results in `SA = 5`, `SB = 3`, and `SA - SB = 2`.

**Proof of theorem 5**:

_Assume `M` is even._

1. Assume `A` has been used up in `m` serves, and the current [`operation
   sequence`][14] is `(x1, y1), (x2, y2), ... , (xm, ym)`, where `m` is less
than `M/2`, so we will have the following statements:
    1. `SA = x1 + x2 + ... + xm`, `SA \u2265 M`
    1. `SB = y1 + y2 + ... + ym = 4m - SA`, `SB < M`
    1. `m < M/2`
    1. `SA - SB > 0`

1. We want to prove that after extending the operation sequence to `M/2`
   operations, we still have `SA - SB > 0`, so if we always serve the minimal
amount for `A`, and the maximum amount for `B`, in each new operation, and still
have `SA - SB > 0`, then we have proved theorem 5. So we will always use `(1,3)`
as the new [`operation`][8], and we will have the following result after
extending the sequence.
1. `SA\' = SA + M/2 - m`
1. `SB\' = SB + 3(M/2 - m) = 4m - SA + 3(M/2 - m) = m - SA + 3M/2`
1. `SA\' - SB\' = SA + M/2 - m - m + SA - 3M/2 = (SA - M) + (SA - 2m) > 0`
1. `SB\' - M = m - SA + 3M/2 - M = (m - M/2) + (M - SA) < 0`

### <a name="2.6"></a>2.6 Theorem 6

> If `B` is used up in less than `max` serves, according to [`theorem 4`][21],
> `A` must not be empty, then obviously we will have [`SB`][13]`\u2265`[`M`][10],
> [`SA`][12]` < M`, and `SB - SA > 0`.
From this point, if we continue to serve until the `max-th` serve has been
reached, then the new `max-th` serves will have the following states.

> State 1: if `SB > M`, then for each of the new `max-th` serves, we will have
> `SB > M`, `SA < M`, and `SB - SA > 0` ( _`B` is used up, and `A` is not
> empty_. )

> State 2: if `SB = M`, and **at least one [operation][8] is NOT `(4,0)`**, then
> we will have `SB > M`, `SA < M`, `SB - SA > 0`( _`B` is used up, and `A` is
> not empty_. )

> <a name="state-3"></a>State 3: if `SB = M`, and **ONLY operation `(4,0)` has
> been used** to extend the [operation sequence][14], then we will have `SB = SA
> = M` ( in this very special case, `A` and
`B` will both be used up after the `max-th` serve. )

**Obviously, there will be at most ONE(1) serve of [state 3][31], which is the
worst case, so when M is big enough, the impact on the probability can be
ignored**

**Proof of theorem 6**:

_Assume `M` is even._

1. Assume `B` has been used up in `m` serves, and the current operation sequence
is `(x1, y1), (x2, y2), ... , (xm, ym)`, where `m` is less than `M/2`, so we
will have the following statements:
    1. `SA = x1 + x2 + ... + xm = 4m - SB`, `SA < M`
    1. `SB = y1 + y2 + ... + ym`, `SB \u2265 M`
    1. `m < M/2`
    1. `SB - SA > 0`
1. If `SB > M` and only `(4,0)` is used to extend the operation sequence, then
we have the following statements:
    1. `SA\' = SA + 4(M/2 - m) = 4m - SB + 4(M/2 - m) = 2M - SB`
    1. `SB\' = SB`
    1. `SB\' - SA\' = 2(SB - M) > 0`
    1. `SA\' - M = M - SB < 0`
1. If `SB = M`, we have the following statements:
    1. If at least one of the operation is not `(4,0)`, we have the following
statements:
        1. `SA\' \u2264 (4m - M) + 4(M/2 - m) - 1 = M - 1`
        1. `SB\' \u2265 M + 1`
        1. `SB\' - SA\' \u2265 2 > 0`
        1. `SA\' - M \u2264 -1 < 0`
    1. If only `(4,0)` is used, we have the following statements:
        1. `SA\' = 2M - SB = M`
        1. `SB\' = M`
        1. `SA\' = SB\' = M`

## <a name="3"></a>3. Problem normalization for big `M`

### <a name="3.1"></a>3.1 The Q-tree

The `soup servings` problem can be presented as a `Q-tree`[[7]], which has exact
4 children for each parent node. The value of each node is a pair of integers
`(a,b)` that represent current remaining volume of `A` and `B` respectively. So
we have the following equations:
1. [`SA`][12]=[`M`][10]` - a`.
1. [`SB`][13]` = M - b`.
1. `SA - SB = b - a`.

According to theorems [1][18], [2][19], and [3][20], the depth of this tree is
[`max`][11], but according to [theorem 4][21], there might be some leaf nodes
reside between level `0` and level `max` in the tree, so it is impossible to
calculate the probability in a generalized way. Fortunately, according to
theorems [5][22], and [6][23], when `M` is big enough, we can always extend the
intermediate nodes to the `max-th` level to make a complete tree without
affecting the correctness of the probability.

In a complete Q-tree, all leaf nodes reside at the `max-th` level, and there are
exact `4\u1D50\u1D43\u02E3` leaf nodes. For each leaf node, it may only be in one of the
following three states:
1. `SA \u2265 M`, and `SB < M`: `A` is empty, and `B` is not empty.
1. `SA = M`, and `SB = M`: 
    1. `A` and `B` are used up at the same time; or
    1. `B` has been used up before `max-th` serve, and `A` is used up by `(4,0)`
       [`operations`][8] ([`state 3`][31] in [`theorem 6`][23]).
1. `SB > M`, and `SA < M`: `B` is empty, and `A` is not empty.

According to theorems [1][18] and [2][19], `A` and `B` **CAN NOT BOTH BE NOT
EMPTY** at level `max`

So theoretically, when `M` is big enough, the probability could be calculated as
follows:

1. `P(A) = count(SA \u2265 M and SB < M) / 4\u1D50\u1D43\u02E3`
1. `P(AB) = count(SA = M and SB = M) / 4\u1D50\u1D43\u02E3`
1. `P(B) = count(SA < M and SB > M ) / 4\u1D50\u1D43\u02E3`
1. `Result = P(A) + 0.5P(AB) = 1 - P(B) - 0.5P(AB)`

**NOTE: when `M` is big enough, the impact of [`state 3`][31] in [`theorem
6`][23] can be ignored**

Of course, we are not going to resolve the `soup servings` problem in this way,
however, the Q-tree, especially a complete Q-tree, is a good tool for analyzing
this problem.

### <a name="3.2"></a>3.2 The normal distribution

According to the analysis above, when [`M`][10] is big enough, the probability
could be calculated as follows:

1. `P(A) = count(`[`SA`][12]` \u2265 `[`M`][10]` and `[`SB`][13]` < M) / 4\u1D50\u1D43\u02E3`
1. `P(AB) = count(SA = M and SB = M) / 4\u1D50\u1D43\u02E3`
1. `P(B) = count(SA < M and SB > M ) / 4\u1D50\u1D43\u02E3`
1. `Result = P(A) + 0.5P(AB) = 1 - P(B) - 0.5P(AB)`

Consider the relationships between `SA` and `SB`, the probability could also be
calculated in another way:

1. `P(A) = count(SA - SB > 0) / 4\u1D50\u1D43\u02E3`
1. `P(AB) = count(SA - SB = 0) / 4\u1D50\u1D43\u02E3`
1. `P(B) = count(SA - SB < 0) / 4\u1D50\u1D43\u02E3`
1. `Result = P(A) + 0.5P(AB) = 1 - P(B) - 0.5P(AB)`

Now define `m = `[`max`][11], we then have the following statements:
1. `SA = x1 + x2 + ... + xm`
1. `SB = y1 + y2 + ... + ym`
1. `SA - SB = (x1 - y1) + (x2 - y2) + ... + (xm - ym)`

If we define `zi = xi - yi`, we will have [`Z`][32]` = SA - SB = z1 + z2 + ... +
zm`

Define a random variable `z = x - y`, because `z \u2208 {4, 2, 0, -2}` and each one
of the four operations is selected with equal probability, `{z1, z2, ..., zm}` is actually
> a sequence of independent and identically distributted random variables drawn from a distribution of expected value given by `\xB5 = 1` and finite variance given by `\u03C3\xB2 = 5`.

According to the `central limit theorem`[[4]], the normalized average value `V =
(z1 + z2 + ... + zm) / m` tends toward a `normal distribution`[[2]] `V ~ N(1,
5/m)`:
1. The limit of `(V1 + V2 + ... + Vt) / t` is 1 when `t` approaches `infinity`.
1. The limit of `[(V1 - 1)\xB2 + (V2 - 1)\xB2 + ... + (Vt - 1)\xB2] / t` is `5/m`, when
   `t` approaches `infinity`.

Because `Z = mV = z1 + z2 + ... + zm`, so `Z ~ N(m, 5m)`:
1. The limit of `(Z1 + Z2 + ... + Zt)/t = m(V1 + V2 + ... Vt)/t` is `m`, when `t`
approaches `infinity`.
1. The limit of `[(Z1 - m)\xB2 + (Z2 - m)\xB2 + ... + (Zt - m)\xB2]/t = m\xB2[(V1 - 1)\xB2 +
(V2 - 1)\xB2 + ... + (Vt - 1)\xB2]/t` is `5m`, when `t` approaches `infinity`.

Then the probability could be calculated as follows:
1. `P(A) = P(Z > 0)`
1. `P(AB) = P(Z = 0) = 0`
1. `P(B) = P(Z < 0)`
1. `Result = P(A) = 1 - P(B)`

### <a name="3.3"></a>3.3 The standard normal distribution

[`Z`][32] does not conform to a `standard normal distribution`[[3]], so we
define <a name="w"></a>`W = Z - m / (5m)\u2070\u22C5\u2075`, which tends toward a `standard
normal distribution` `W ~ N(0, 1)`, and we will have the following statements:
1. `P(Z > x) = P(W > x - m / (5m)\u2070\u22C5\u2075)`
1. `P(Z = x) = P(W = x - m / (5m)\u2070\u22C5\u2075)`
1. `P(Z < x) = P(W < x - m / (5m)\u2070\u22C5\u2075)`

## <a name="4"></a>4. Proof of the problem

We are now ready to prove the following statements:

### <a name="4.1"></a>4.1 Statement 1
> When [`M`][10] is big enough, the probability of `A being used up before B`
> approaches `1`.  In other words, the probability of `B being used up before
> A` approaches `0`.

**Proof of statement 1**:

Because [`Z`][32]` ~ N(m, 5m)`, function `f(m) = m / (5m)\u2070\u22C5\u2075 = (m/5)\u2070\u22C5\u2075` denotes
how many `deviations` can be covered by range `[0, m]` or `[m, 2m]`. Investigate
the deriative of `f(m)`, we have `f\'(m) = 0.5(5m)\u207B\u2070\u22C5\u2075 > 0`, so `f(m)` is a
monotone increasing function, and the number of `deviations` covered by `[0, m]`
or `[m, 2m]` increases as the growth of `m`.

According to `Chebyshev\'s Theorme`[[5]], the more `deviations` covered by `[0,
m]` and `[m, 2m]`, the greater `P(A \u2208 [0, 2m])` will be, so when `m` is big
enough, `P(Z > 0)` approaches `1`, and `P(Z < 0) = 1 - P(Z > 0)` approaches `0`.

### <a name="4.2"></a>4.2 Statement 2
> The difference between the probability of `A being used up before B` and `1`
> is less than `0.000001` when [`M`][10] is greater than `5650`.

**Proof of statement 2**:

As per the analysis so far, the difference between the probability of `A being
used up before B` and `1` is `1 - P(Z > 0) = P(Z < 0)`, so the problem becomes
"find the value of `m`, where `P(Z < 0) = 0.000001`."

Because `P(Z < 0) = P(Z > 2m)`, so the problem is equivalent to "finding the
value of `m`, where `P(Z > 2m) = 0.000001`." 

According to [`section 3.3`][27], `P(Z > 2m) = P(W > m / (5m)\u2070\u22C5\u2075) = 0.000001`,
by looking up the `standard normal distribution table`[[6]], we have `m / (5m)\u2070\u22C5\u2075 =
4.75343`, therefore `m = 5*4.75343\xB2 = 113`.

According to the analysis of [`section 3.1`][25], `m = M/2` when `M` is even, or
`m = (M+1)/2` when `M` is odd, so `M = max(2m, 2m - 1) = 226`.

So finally, we have [`N`][9]` = M * 25 = 5650`, which is greater than and very
close to [`5550`][34], which makes it an ideal threshold, and the `100`
difference between `5650` and `5550` might be caused by [`state 3`][31] in
[`theorem 6`][23].

## References

A                                       | B
----------------------------------------|---------------------------------------
\[1\] [Soup Servings Problem][1]        | \[2\] [Normal Distribution][2]
\[3\] [Standard Normal Distribution][3] | \[4\] [Central Limit Theorem][4]
\[5\] [Chebyshev\'s Inequality][5]       | \[6\] [Standard Normal Distribution Table][6]
\[7\] [Q-tree][7]                       | \[36\] [Magic Number][36]

## Terms
A|B|C|D|E
-|-|-|-|-
[`N`][9] | [`M`][10] | [`SA`][12] | [`SB`][13] | [`Z`][32] | [`W`][33]
 [`max`][11] | [`Operation`][8] | [`Operation Sequence`][14] |  [`Virtual Amount`][35]

 [1]: https://leetcode.com/articles/soup-servings/ "Soup Servings Problem"
 [2]: http://statistics.wikidot.com/ch7 "Normal Distribution"
 [3]: http://statistics.wikidot.com/ch7 "Standard Normal Distribution"
 [4]: https://en.wikipedia.org/wiki/Central_limit_theorem "Central Limit Theorem"
 [5]: https://en.wikipedia.org/wiki/Chebyshev%27s_inequality "Chebyshev\'s Inequality"
 [6]: https://keisan.casio.com/exec/system/1180573188 "Standard Normal Distribution Table"
 [7]: https://en.wikipedia.org/wiki/Quadtree "Q-tree"
 [8]: #operation "Operation"
 [9]: #n "N"
[10]: #m "M" 
[11]: #max "max"
[12]: #sa "SA"
[13]: #sb "SB"
[14]: #operation-sequence "Operation Sequence"
[15]: #title "Title"
[16]: #1 "Section 1"
[17]: #2 "Section 2"
[18]: #2.1 "Section 2.1"
[19]: #2.2 "Section 2.2"
[20]: #2.3 "Section 2.3"
[21]: #2.4 "Section 2.4"
[22]: #2.5 "Section 2.5"
[23]: #2.6 "Section 2.6"
[24]: #3 "Section 3"
[25]: #3.1 "Section 3.1"
[26]: #3.2 "Section 3.2"
[27]: #3.3 "Section 3.3"
[28]: #4 "Section 4"
[29]: #4.1 "Section 4.1"
[30]: #4.2 "Section 4.2"
[31]: #state-3 "State 3"
[32]: #z "Z"
[33]: #w "W"
[34]: #5550 "5550"
[35]: #virtual-amount "Virutal Amount"
[36]: https://leetcode.com/problems/soup-servings/discuss/134449/How-do-you-explain-the-magic-number-in-interview-Oh-because-I-did-experiment-before-interview. "Magic Number"

</p>


### Straightforward Java Recursion with Memorization
- Author: myCafeBabe
- Creation Date: Sun Apr 01 2018 13:22:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 20 2018 11:52:36 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public double soupServings(int N) {
        if (N > 5000) {  // trick
            return 1.0;
        }
        return helper(N, N, new Double[N + 1][N + 1]);
    }
    
    public double helper(int A, int B, Double[][] memo) {
        if (A <= 0 && B <= 0) return 0.5;     // base case 1
        if (A <= 0) return 1.0;               // base case 2
        if (B <= 0) return 0.0;               // base case 3
        if (memo[A][B] != null) {
            return memo[A][B];
        }
        int[] serveA = {100, 75, 50, 25};
        int[] serveB = {0, 25, 50, 75};
        memo[A][B] = 0.0;
        for (int i = 0; i < 4; i++) {
            memo[A][B] += helper(A - serveA[i], B - serveB[i], memo);
        }
        return memo[A][B] *= 0.25;
    }
}
```
</p>


