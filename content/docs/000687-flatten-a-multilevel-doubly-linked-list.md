---
title: "Flatten a Multilevel Doubly Linked List"
weight: 687
#id: "flatten-a-multilevel-doubly-linked-list"
---
## Description
<div class="description">
<p>You are given a doubly linked list which in addition to the next and previous pointers, it could have a child pointer, which may or may not point to a separate doubly linked list. These child lists may have one or more children of their own, and so on, to produce a multilevel data structure, as shown in the example below.</p>

<p>Flatten the list so that all the nodes appear in a single-level, doubly linked list. You are given the head of the first level of the list.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> head = [1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]
<strong>Output:</strong> [1,2,3,7,8,11,12,9,10,4,5,6]
<strong>Explanation:
</strong>
The multilevel linked list in the input is as follows:

<img src="https://assets.leetcode.com/uploads/2018/10/12/multilevellinkedlist.png" style="width: 640px;" />

After flattening the multilevel linked list it becomes:

<img src="https://assets.leetcode.com/uploads/2018/10/12/multilevellinkedlistflattened.png" style="width: 1100px;" />
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> head = [1,2,null,3]
<strong>Output:</strong> [1,3,2]
<strong>Explanation:

</strong>The input multilevel linked list is as follows:

  1---2---NULL
  |
  3---NULL
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> head = []
<strong>Output:</strong> []
</pre>

<p>&nbsp;</p>

<p><strong>How&nbsp;multilevel linked list is represented in test case:</strong></p>

<p>We use the&nbsp;multilevel linked list from <strong>Example 1</strong> above:</p>

<pre>
 1---2---3---4---5---6--NULL
         |
         7---8---9---10--NULL
             |
             11--12--NULL</pre>

<p>The serialization of each level is as follows:</p>

<pre>
[1,2,3,4,5,6,null]
[7,8,9,10,null]
[11,12,null]
</pre>

<p>To serialize all levels together we will add nulls in each level to signify no node connects to the upper node of the previous level. The serialization becomes:</p>

<pre>
[1,2,3,4,5,6,null]
[null,null,7,8,9,10,null]
[null,11,12,null]
</pre>

<p>Merging the serialization of each level and removing trailing nulls we obtain:</p>

<pre>
[1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>Number of Nodes will not exceed 1000.</li>
	<li><code>1 &lt;= Node.val &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Linked List (linked-list)
- Depth-first Search (depth-first-search)

## Companies
- Bloomberg - 21 (taggedByAdmin: false)
- Amazon - 5 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Google - 14 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: DFS by Recursion

**Intuition**

People might ask themselves in which scenario that one would use such an awkward data structure. Well, one of the scenarios could be a simplified version of git branching. By flattening the multilevel list, one can think it as merging all git branches together, though it is not at all how the git merge works.

First of all, to clarify what is the desired result of the flatten operation, we illustrate with an example below.

![pic](../Figures/430/430_list_step_1.png)

In the above example, we distinguish nodes in different levels with different colors. We could flatten the list in two steps as follows:

![pic](../Figures/430/430_list_step_2.png)

![pic](../Figures/430/430_list_step_3.png)

>As we can see, by _flatten_, we basically **fold or embed** the sublist that is branched from the child pointer into its parent list.

This is one way to interpret the _flatten_ operation. However, as intuitive as the problem seems to be, one might stumble over the implementation. It is because the above intuition does not quite catch the true nature of the problem.

>Actually, if we turn the above list in 90 degrees around the clock, then suddenly a **binary tree** appear in front of us. And the flatten operation is basically what we call _**preorder DFS traversal**_ (Depth-First Search).

![pic](../Figures/430/430_dfs_tree.png)

Indeed, as shown in the above graph, we could consider the `child` pointer as the `left` pointer in binary tree which points to the left sub-tree (sublist). And similarly, the `next` pointer can be considered as the `right` pointer in binary tree. Then if we traverse the tree in preorder DFS, it would generate the same visiting sequence as the flatten operation in our problem. 


**Algorithm**

Now that we know the problem is basically asking us to do a _preorder DFS_ traversal over a disguised binary tree, we could use this intuition to guide the implementation.

As many of you would know that there are generally two manners to implement the DFS traversal: _recursion_ and _iteration_. We here start with the recursion, since many find it more intuitive.

Here it goes with the recursive DFS algorithm:

- First of all, we define our recursive function as `flatten_dfs(prev, curr)` which takes two pointers as input and returns the pointer of tail in the _flattened_ list. The `curr` pointer leads to the sub-list that we would like to flatten, and the `prev` pointer points to the element that should precede the `curr` element.
<br/>

- Within the recursive function `flatten_dfs(prev, curr)`, we first establish the double links between the `prev` and `curr` nodes, as in the _**preorder**_ DFS we take care of the **current state** first before looking into the children.
<br/>

- Further in the function `flatten_dfs(prev, curr)`, we then go ahead to flatten the **left subtree** (_i.e._ the sublist pointed by the `curr.child` pointer) with the call of `flatten_dfs(curr, curr.child)`, which returns the `tail` element to the flattened sublist. Then with the `tail` element of the previous sublist, we then further flatten the **right subtree** (_i.e._ the sublist pointed by the `curr.next` pointer), with the call of `flatten_dfs(tail, curr.next)`.
<br/>

- And voila, that is our core function. There are two additional important details that we should attend to, in order to obtain the correct result:
    - We should make a copy of the `curr.next` pointer before the first recursive call of `flatten_dfs(curr, curr.child)`, since the `curr.next` pointer might be altered within the function.
    - After we flatten the sublist pointed by the `curr.child` pointer, we should remove the child pointer since it is no longer needed in the final result.
<br/>

- Last by not the least, one would notice in the following implementation that we create a `pseudoHead` variable in the function. This is not absolutely necessary, but it would help us to make the solution more concise and elegant by **reducing the null pointer checks** (_e.g._ `if prev == null`). And with less branching tests, it certainly helps with the performance as well. Sometimes people might call it _**sentinel**_ node. As one might have seen before, this is a useful trick that one could apply to many problems related with linked lists (_e.g._ [LRU cache](https://leetcode.com/articles/lru-cache/)).

<iframe src="https://leetcode.com/playground/yRidcS5K/shared" frameBorder="0" width="100%" height="500" name="yRidcS5K"></iframe>


**Complexity**

- Time Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the number of nodes in the list. The DFS algorithm traverses each node once and only once.
<br/>

- Space Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the number of nodes in the list. In the worst case, the binary tree might be extremely unbalanced (_i.e._ the tree leans to the left), which corresponds to the case where nodes are chained with each other only with the `child` pointers. In this case, the recursive calls would pile up, and it would take $$N$$ space in the function call stack. 
<br/>
<br/>

---

#### Approach 2: DFS by Iteration

**Intuition**

Following the intuition of the above DFS preorder traversal approach, here we demonstrate how one can implement the solution via **_iteration_**. 

>The key is to use the data structure called _**stack**_, which is a container that follows the principle of _LIFO_ (_last in, first out_). The element that enters the stack at last would come out first, similar with the scenario of a packed elevator.

The stack data structure helps us to construct the iteration sequence as the one created by recursion. The stack here mimics the behavior of the function _call stack_, so that we could obtain the same result without resorting to recursion.

**Algorithm**

- First of all, we create a stack and then we push the head node to the stack. In addition, we create a variable called `prev` which would help us to track the precedent node at each step during the iteration.
<br/>

- Then we enter a loop to iterate the stack until the stack becomes empty.
<br/>

- Within the loop, at each step, we first pop out a node (named `curr`) from the stack. Then we establish the links between `prev` and `curr`. Then in the following steps, we take care of the nodes pointed by the `curr.next` and `curr.child` pointers respectively, and strictly in this order.

    - First, if the `curr.next` does exist (_i.e._ there exists a right subtree), we then push the node into the stack for the next iteration.

    - Secondly, if the `curr.child` does exist (_i.e._ there exists a left subtree), we then push the node into the stack. In addition, unlike the `child.next` pointer, we need to clean up the `curr.child` pointer since it should not be present in the final result.
<br/>

- And voila. Lastly, we also employ the `pseudoHead` node to render the algorithm more elegant, as we discussed in the previous approach.

To better illustrate how the algorithm works, we create an animation that shows the evolution of stack step by step, as follows:

!?!../Documents/430_LIS.json:1000,561!?!

Here are some sample implementations.

<iframe src="https://leetcode.com/playground/M3e6Jd9M/shared" frameBorder="0" width="100%" height="500" name="M3e6Jd9M"></iframe>



**Complexity**

- Time Complexity: $$\mathcal{O}(N)$$. The iterative solution has the same time complexity as the recursive.
<br/>

- Space Complexity: $$\mathcal{O}(N)$$. Again, the iterative solution has the same space complexity as the recursive one.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy Understanding Java beat 95.7% with Explanation
- Author: kyleluchina
- Creation Date: Mon Jul 16 2018 12:09:47 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 05:52:38 GMT+0800 (Singapore Standard Time)

<p>
Basic idea is straight forward: 
1. Start form the `head` , move one step each time to the next node
2. When meet with a node with child, say node `p`, follow its `child chain` to the end and connect the tail node with `p.next`, by doing this we merged the `child chain` back to the `main thread`
3.  Return to `p` and proceed until find next node with child.
4.  Repeat until reach `null`

```
class Solution {
    public Node flatten(Node head) {
        if( head == null) return head;
	// Pointer
        Node p = head; 
        while( p!= null) {
            /* CASE 1: if no child, proceed */
            if( p.child == null ) {
                p = p.next;
                continue;
            }
            /* CASE 2: got child, find the tail of the child and link it to p.next */
            Node temp = p.child;
            // Find the tail of the child
            while( temp.next != null ) 
                temp = temp.next;
            // Connect tail with p.next, if it is not null
            temp.next = p.next;  
            if( p.next != null )  p.next.prev = temp;
            // Connect p with p.child, and remove p.child
            p.next = p.child; 
            p.child.prev = p;
            p.child = null;
        }
        return head;
    }
}
```
</p>


### c++, about 10 lines solution
- Author: zjzh
- Creation Date: Sat Jul 21 2018 20:31:55 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 13:29:12 GMT+0800 (Singapore Standard Time)

<p>
```
Node* flatten(Node* head) {
	for (Node* h = head; h; h = h->next)
	{
		if (h->child)
		{
			Node* next = h->next;
			h->next = h->child;
			h->next->prev = h;
			h->child = NULL;
			Node* p = h->next;
			while (p->next) p = p->next;
			p->next = next;
			if (next) next->prev = p;
		}
	}
	return head;
}
```
</p>


### Python easy solution using stack 
- Author: shrivilb
- Creation Date: Mon Jul 30 2018 07:29:31 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 19 2018 03:03:55 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def flatten(self, head):
        if not head:
            return
        
        dummy = Node(0,None,head,None)     
        stack = []
        stack.append(head)
        prev = dummy
        
        while stack:
            root = stack.pop()

            root.prev = prev
            prev.next = root
            
            if root.next:
                stack.append(root.next)
                root.next = None
            if root.child:
                stack.append(root.child)
                root.child = None
            prev = root        
            
        
        dummy.next.prev = None
        return dummy.next
```
</p>


