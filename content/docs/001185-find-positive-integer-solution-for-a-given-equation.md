---
title: "Find Positive Integer Solution for a Given Equation"
weight: 1185
#id: "find-positive-integer-solution-for-a-given-equation"
---
## Description
<div class="description">
<p>Given a&nbsp;function&nbsp; <code>f(x, y)</code>&nbsp;and a value <code>z</code>, return all positive integer&nbsp;pairs <code>x</code> and <code>y</code> where <code>f(x,y) == z</code>.</p>

<p>The function is constantly increasing, i.e.:</p>

<ul>
	<li><code>f(x, y) &lt; f(x + 1, y)</code></li>
	<li><code>f(x, y) &lt; f(x, y + 1)</code></li>
</ul>

<p>The function interface is defined like this:&nbsp;</p>

<pre>
interface CustomFunction {
public:
&nbsp; // Returns positive integer f(x, y) for any given positive integer x and y.
&nbsp; int f(int x, int y);
};
</pre>

<p>For custom testing purposes you&#39;re given an integer <code>function_id</code> and a target <code>z</code> as input, where <code>function_id</code> represent one function from an secret internal list, on the examples you&#39;ll know only two functions from the list. &nbsp;</p>

<p>You may return the solutions in any order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> function_id = 1, z = 5
<strong>Output:</strong> [[1,4],[2,3],[3,2],[4,1]]
<strong>Explanation:</strong>&nbsp;function_id = 1 means that f(x, y) = x + y</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> function_id = 2, z = 5
<strong>Output:</strong> [[1,5],[5,1]]
<strong>Explanation:</strong>&nbsp;function_id = 2 means that f(x, y) = x * y
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= function_id &lt;= 9</code></li>
	<li><code>1 &lt;= z &lt;= 100</code></li>
	<li>It&#39;s guaranteed that the solutions of <code>f(x, y) == z</code> will be on the range <code>1 &lt;= x, y &lt;= 1000</code></li>
	<li>It&#39;s also guaranteed that <code>f(x, y)</code> will fit in 32 bit signed integer if <code>1 &lt;= x, y &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Math (math)
- Binary Search (binary-search)

## Companies
- Google - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] O(X+Y)
- Author: lee215
- Creation Date: Sun Oct 27 2019 12:44:27 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 29 2019 09:24:28 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Rephrase the problem:
Given a matrix, each row and each column is increasing.
Find all coordinates `(i,j)` that `A[i][j] == z`

## **Complexity**
In binary search,
time complexity is `O(XlogY)` or `O(YlogX)`

In this solution,
time complexity is stable `O(X + Y)`.

Bianry search is really an **unnecessay** operation,
and it won\'t help improve the conplexity at all.

Space is `O(X)`

**Java**
```java
    public List<List<Integer>> findSolution(CustomFunction customfunction, int z) {
        List<List<Integer>> res = new ArrayList<>();
        int x = 1, y = 1000;
        while (x <= 1000 && y > 0) {
            int v = customfunction.f(x, y);
            if (v > z) --y;
            else if (v < z) ++x;
            else res.add(Arrays.asList(x++, y--));
        }
        return res;
    }
```
**C++:**
```cpp
    vector<vector<int>> findSolution(CustomFunction& customfunction, int z) {
        vector<vector<int>> res;
        int y = 1000;
        for (int x = 1; x <= 1000; ++x) {
            while (y > 1 && customfunction.f(x,y) > z) y--;
            if (customfunction.f(x,y) == z)
                res.push_back({x, y});
        }
        return res;
    }
```
**Python**
```
    def findSolution(self, customfunction, z):
        res = []
        y = 1000
        for x in xrange(1, 1001):
            while y > 1 and customfunction.f(x, y) > z:
                y -= 1
            if customfunction.f(x, y) == z:
                res.append([x, y])
        return res
```
</p>


### Not "easy"
- Author: deleted_user
- Creation Date: Sun Oct 27 2019 12:01:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 27 2019 23:14:01 GMT+0800 (Singapore Standard Time)

<p>
How is this an "easy" problem?  If you try brute force, you get
time limit exceeded.  And if that happens, you have no idea where
to start debugging!  You\'re just told "mystery function #7" caused
time limit exceeded, whatever that means.

The uncertainty of the problem, plus not allowing a brute force
approach, in my view, makes this a medium problem - not easy.  I
spent more time solving this problem than a supposedly medium question (the third one).

UPDATE: In another thread @twu54 pointed out this problem is a duplicate of a medium problem:
https://leetcode.com/problems/find-positive-integer-solution-for-a-given-equation/discuss/414251/Duplicate-problem-from-Leetcode-240
Which is another indication it\'s not an easy problem: it\'s the same as a medium problem.
</p>


### [Java/Python 3] 3 methods: time O(x + y), O(xlogy) and O(x + logy) w/ analysis.
- Author: rock
- Creation Date: Sun Oct 27 2019 12:03:03 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Nov 15 2019 11:39:31 GMT+0800 (Singapore Standard Time)

<p>
**Method 1: Linear time complexity**
Time: O(x + y), space: O(1) excluding return list.

```
    public List<List<Integer>> findSolution(CustomFunction customFunction, int z) {
        List<List<Integer>> ans = new ArrayList<>();
        int x = 1, y = 1000;
        while (x < 1001 && y > 0) {
            int val = customFunction.f(x, y);
            if (val < z) { 
                ++x; 
            }else if (val > z) { 
                --y; 
            }else {
                ans.add(Arrays.asList(x++, y--));
            }
        }
        return ans;
    }
```
```
    def findSolution(self, customfunction: \'CustomFunction\', z: int) -> List[List[int]]:
        x, y, ans = 1, 1000, []
        while x < 1001 and y > 0:
            val = customfunction.f(x, y)
            if val > z:
                y -= 1
            elif val < z:
                x += 1
            else:
                ans += [[x, y]]
                x += 1
                y -= 1
        return ans
```
**Method 2: Binary Search**
Time: O((xlogy), space: O(1) excluding return list.

```
    public List<List<Integer>> findSolution(CustomFunction customFunction, int z) {
        List<List<Integer>> ans = new ArrayList<>();
        for (int x = 1; x < 1001; ++x) {
            int l = 1, r = 1001;
            while (l < r) {
                int y = (l + r) / 2;
                if (customFunction.f(x, y) < z) {
                    l = y + 1;
                }else {
                    r = y;
                }
            }
            if (customFunction.f(x, l) == z) {
                ans.add(Arrays.asList(x, l));
            }
        }
        return ans;
    }
```
```
    def findSolution(self, customfunction: \'CustomFunction\', z: int) -> List[List[int]]:
        ans = []
        for x in range(1, 1001):
            l, r = 1, 1001
            while l < r:
                y = (l + r) // 2
                if customfunction.f(x, y) < z:
                    l = y + 1;
                else:
                    r = y
            if  customfunction.f(x, l) == z:
                ans += [[x, l]]
        return ans
```

----
**Method 3: Optimized Binary Search**
left and right boundaries change according to result of previous binary search; after first round of binary search, which cost `logy`, the remaining binary seach only cost amortized `O(1)` (Denote its average as *C*) for each value of `x`.
Therefore the time cost `O(Cx + logy)`

Time: `O(x + logy)`, space: `O(1)` excluding return list.
```
    public List<List<Integer>> findSolution(CustomFunction customFunction, int z) {
        List<List<Integer>> ans = new ArrayList<>();
        int left = 1, right = 1001;
        for (int x = 1; x < 1001; ++x) {
            if (customFunction.f(x, left) > z || customFunction.f(x, right) < z)
                continue;
            int l = left, r = right;
            while (l < r) {
                int y = (l + r) / 2;
                if (customFunction.f(x, y) < z) {
                    l = y + 1;
                }else {
                    r = y;
                }
            }
            int val = customFunction.f(x, l);
            if (val >= z) {
                if (val == z) ans.add(Arrays.asList(x, l));
                right = l;
            }else {
                left = l;
            }
        }
        return ans;
    }
```
```
    def findSolution(self, customfunction: \'CustomFunction\', z: int) -> List[List[int]]:
        left, right, ans, f = 1, 1001, [], customfunction.f
        for x in range(1, 1001):
            if f(x, left) > z or f(x, right) < z: continue
            l, r = left, right
            while l < r:
                y = (l + r) // 2
                if f(x, y) < z:
                    l = y + 1;
                else:
                    r = y
            val = f(x, l)
            if val >= z:
                ans += [[x, l]] if val == z else []
                right = l
            else:
                left = l
        return ans
```
</p>


