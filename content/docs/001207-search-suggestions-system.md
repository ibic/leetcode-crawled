---
title: "Search Suggestions System"
weight: 1207
#id: "search-suggestions-system"
---
## Description
<div class="description">
<p>Given an array of strings <code>products</code> and a string <code>searchWord</code>. We want to design a system that suggests at most three product names from <code>products</code>&nbsp;after each character of&nbsp;<code>searchWord</code> is typed. Suggested products should have common prefix with the searchWord. If there are&nbsp;more than three products with a common prefix&nbsp;return the three lexicographically minimums products.</p>

<p>Return <em>list of lists</em> of the suggested <code>products</code> after each character of&nbsp;<code>searchWord</code> is typed.&nbsp;</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> products = [&quot;mobile&quot;,&quot;mouse&quot;,&quot;moneypot&quot;,&quot;monitor&quot;,&quot;mousepad&quot;], searchWord = &quot;mouse&quot;
<strong>Output:</strong> [
[&quot;mobile&quot;,&quot;moneypot&quot;,&quot;monitor&quot;],
[&quot;mobile&quot;,&quot;moneypot&quot;,&quot;monitor&quot;],
[&quot;mouse&quot;,&quot;mousepad&quot;],
[&quot;mouse&quot;,&quot;mousepad&quot;],
[&quot;mouse&quot;,&quot;mousepad&quot;]
]
<strong>Explanation:</strong> products sorted lexicographically = [&quot;mobile&quot;,&quot;moneypot&quot;,&quot;monitor&quot;,&quot;mouse&quot;,&quot;mousepad&quot;]
After typing m and mo all products match and we show user [&quot;mobile&quot;,&quot;moneypot&quot;,&quot;monitor&quot;]
After typing mou, mous and mouse the system suggests [&quot;mouse&quot;,&quot;mousepad&quot;]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> products = [&quot;havana&quot;], searchWord = &quot;havana&quot;
<strong>Output:</strong> [[&quot;havana&quot;],[&quot;havana&quot;],[&quot;havana&quot;],[&quot;havana&quot;],[&quot;havana&quot;],[&quot;havana&quot;]]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> products = [&quot;bags&quot;,&quot;baggage&quot;,&quot;banner&quot;,&quot;box&quot;,&quot;cloths&quot;], searchWord = &quot;bags&quot;
<strong>Output:</strong> [[&quot;baggage&quot;,&quot;bags&quot;,&quot;banner&quot;],[&quot;baggage&quot;,&quot;bags&quot;,&quot;banner&quot;],[&quot;baggage&quot;,&quot;bags&quot;],[&quot;bags&quot;]]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> products = [&quot;havana&quot;], searchWord = &quot;tatiana&quot;
<strong>Output:</strong> [[],[],[],[],[],[],[]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= products.length &lt;= 1000</code></li>
	<li>There are no&nbsp;repeated elements in&nbsp;<code>products</code>.</li>
	<li><code>1 &lt;= &Sigma; products[i].length &lt;= 2 * 10^4</code></li>
	<li>All characters of <code>products[i]</code> are lower-case English letters.</li>
	<li><code>1 &lt;= searchWord.length &lt;= 1000</code></li>
	<li>All characters of <code>searchWord</code>&nbsp;are lower-case English letters.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Amazon - 11 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Sort and Binary Search the Prefix
- Author: lee215
- Creation Date: Mon Nov 25 2019 00:57:25 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Feb 05 2020 01:10:11 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
In a sorted list of words,
for any word `A[i]`,
all its sugested words must following this word in the list.

For example, if `A[i]` is a prefix of `A[j]`,
A[i] must be the prefix of `A[i + 1], A[i + 2], ..., A[j]`

## **Explanation**
With this observation,
we can binary search the position of each prefix of search `word`,
and check if the next 3 words is a valid suggestion.
<br>

## **Complexity**
Time `O(NlogN)` for sorting
Space `O(logN)` for quick sort.

Time `O(logN)` for each query
Space `O(query)` for each query
where I take word operation as `O(1)`
<br>

**C++**
```cpp
    vector<vector<string>> suggestedProducts(vector<string>& A, string searchWord) {
        auto it = A.begin();
        sort(it, A.end());
        vector<vector<string>> res;
        string cur = "";
        for (char c : searchWord) {
            cur += c;
            vector<string> suggested;
            it = lower_bound(it, A.end(), cur);
            for (int i = 0; i < 3 && it + i != A.end(); i++) {
                string& s = *(it + i);
                if (s.find(cur)) break;
                suggested.push_back(s);
            }
            res.push_back(suggested);
        }
        return res;
    }
```

**Java**
from @krisdu, space `O(N)`
```java
public List<List<String>> suggestedProducts(String[] products, String searchWord) {
        List<List<String>> res = new ArrayList<>();
        TreeMap<String, Integer> map = new TreeMap<>();
        Arrays.sort(products);
        List<String> productsList = Arrays.asList(products);

        for (int i = 0; i < products.length; i++) {
            map.put(products[i], i);
        }

        String key = "";
        for (char c : searchWord.toCharArray()) {
            key += c;
            String ceiling = map.ceilingKey(key);
            String floor = map.floorKey(key + "~");
            if (ceiling == null || floor == null) break;
            res.add(productsList.subList(map.get(ceiling), Math.min(map.get(ceiling) + 3, map.get(floor) + 1)));
        }

        while (res.size() < searchWord.length()) res.add(new ArrayList<>());
        return res;
    }
```

**Python:**
```python
    def suggestedProducts(self, A, word):
        A.sort()
        res, prefix, i = [], \'\', 0
        for c in word:
            prefix += c
            i = bisect.bisect_left(A, prefix, i)
            res.append([w for w in A[i:i + 3] if w.startswith(prefix)])
        return res
```

</p>


### [Java/Python 3] Simple Trie and Binary Search codes w/ comment and brief analysis.
- Author: rock
- Creation Date: Sun Nov 24 2019 12:02:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 23 2020 09:11:29 GMT+0800 (Singapore Standard Time)

<p>
**Q & A:**
Q1: In method 1, why we needs two checks on root whther it is null as here:
```
            if (root != null) // if current Trie is NOT null.
                root = root.sub[c - \'a\'];
            ans.add(root == null ? Arrays.asList() : root.suggestion); // add it if there exist products with current prefix.
```
A1: 
Two checks ensure neither current Trie nor next level Trie is `null`, otherwise, it would throw `NullPointerException`.

Q2: In method 2, why the space complexity of sorting and binary search algorithm is O(L*m)?
A2: It includes return list `ans`, which cost space O(L * M).

**End of Q & A**

----
**Method 1: Trie**
```Java
    class Trie {
        Trie[] sub = new Trie[26];
        LinkedList<String> suggestion = new LinkedList<>();
    }
    public List<List<String>> suggestedProducts(String[] products, String searchWord) {
        Trie root = new Trie();
        for (String p : products) { // build Trie.
            insert(p, root); // insert a product into Trie.
        }
        return search(searchWord, root);
    }
    private void insert(String p, Trie root) {
        Trie t = root;
        for (char c : p.toCharArray()) { // insert current product into Trie.
            if (t.sub[c - \'a\'] == null)
                t.sub[c - \'a\'] = new Trie();
            t = t.sub[c - \'a\'];
            t.suggestion.offer(p); // put products with same prefix into suggestion list.
            Collections.sort(t.suggestion);
            if (t.suggestion.size() > 3) // maintain 3 lexicographically minimum strings.
                t.suggestion.pollLast();        
        }
    }
    private List<List<String>> search(String searchWord, Trie root) {
        List<List<String>> ans = new ArrayList<>();
        for (char c : searchWord.toCharArray()) { // search product.
            if (root != null) // if there exist products with current prefix.
                root = root.sub[c - \'a\'];
            ans.add(root == null ? Arrays.asList() : root.suggestion); // add it if there exist products with current prefix.
        }
        return ans;
    }
```
```python
class Trie:
    def __init__(self):
        self.sub = {}
        self.suggestion = []
class Solution:
    def suggestedProducts(self, products: List[str], searchWord: str) -> List[List[str]]:
        root = Trie()
        for product in sorted(products):
            self._insert(product, root)
        return self._search(searchWord, root)    
    def _insert(self, product: str, root: Trie) -> None:
        trie = root
        for char in product:
            if char not in trie.sub:
                trie.sub[char] = Trie()
            trie = trie.sub[char]
            trie.suggestion.append(product)
            trie.suggestion.sort()
            if len(trie.suggestion) > 3:
                trie.suggestion.pop()
    def _search(self, searchWord: str, root: Trie) -> List[List[str]]:              
        ans = []
        for char in searchWord:
            if root:
                root = root.sub.get(char)
            ans.append(root.suggestion if root else [])            
        return ans
```

**Analysis:**

Complexity depends on the sorting, the process of building Trie and the length of `searchWord`. Sorting cost time `O(m * n)`, due to involving comparing String, which cost time `O(m)` for each comparison, building Trie cost `O(m * n)`. Therefore,
*Time*: `O(m * n + L)`, *space*: `O(m * n + L * m)` - including return list `ans`, where `m` = average length of `products`, `n` = `products.length`, `L = searchWord.length()`.

----

**Method 2: Sort then Binary Search**

```Java
    public List<List<String>> suggestedProducts(String[] products, String searchWord) {
        List<List<String>> ans = new ArrayList<>();
        Arrays.sort(products); // sort products.
        for (int i = 1; i <= searchWord.length(); ++i) {
            String cur = searchWord.substring(0, i);
            int k = Arrays.binarySearch(products, cur);
            while (k > 0 && cur.equals(products[k - 1])) // in case there are more than 1 cur in products.
                --k; // find the first one. 
            if (k < 0) // no cur in products. 
                k = ~k; // find the first one larger than cur.
            List<String> suggestion = new ArrayList<>();
            for (int j = k + 3; k < products.length && k < j && products[k].startsWith(cur); ++k)
                suggestion.add(products[k]);
            ans.add(suggestion);
        }
        return ans;
    }
```
```python
    def suggestedProducts(self, products: List[str], searchWord: str) -> List[List[str]]:
        products.sort()
        cur, ans = \'\', []
        for char in searchWord:
            cur += char
            i = bisect.bisect_left(products, cur)
            ans.append([product for product in products[i : i + 3] if product.startswith(cur)])
        return ans
```
**Analysis:**
Sorting and searching cost `O(n * m * log n)` and `O(L * m * logn)`, respectively; Therefore,
Time: `O((n + L) * m * logn)`, space: `O(L * m)` -  - including return list `ans`, but excluding space cost of sorting, where `m` = average length of `products`, `n` = `products.length`, `L = searchWord.length()`.
</p>


### Stupid Test case
- Author: Rookie_Alex
- Creation Date: Sun Nov 24 2019 12:06:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 24 2019 12:06:44 GMT+0800 (Singapore Standard Time)

<p>
There is a stupid test case which allow the system recommand the same word to the users.  I never saw something like the following in any search engine :)

["eucgsmpsyndddijvpxfagngnjbzxuajxmrmszwtjvwswgdroje"	
"eucgsmpsyndddijvpxfagngnjbzxuajxmrmszwtjvwswgdroje"	
"eucgsmpsyndddijvpxfagngnjbzxuajxmrmszwtjvwswgdrojp"]


</p>


