---
title: "Average Selling Price"
weight: 1553
#id: "average-selling-price"
---
## Description
<div class="description">
<p>Table: <code>Prices</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| product_id    | int     |
| start_date    | date    |
| end_date      | date    |
| price         | int     |
+---------------+---------+
(product_id, start_date, end_date) is the primary key for this table.
Each row of this table indicates the price of the product_id in the period from start_date to end_date.
For each product_id there will be no two overlapping periods. That means there will be no two intersecting periods for the same product_id.
</pre>

<p>&nbsp;</p>

<p>Table: <code>UnitsSold</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| product_id    | int     |
| purchase_date | date    |
| units         | int     |
+---------------+---------+
There is no primary key for this table, it may contain duplicates.
Each row of this table indicates the date, units and product_id of each product sold. 
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the average selling price for each product.</p>

<p><code>average_price&nbsp;</code>should be&nbsp;<strong>rounded to 2 decimal places</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
Prices table:
+------------+------------+------------+--------+
| product_id | start_date | end_date   | price  |
+------------+------------+------------+--------+
| 1          | 2019-02-17 | 2019-02-28 | 5      |
| 1          | 2019-03-01 | 2019-03-22 | 20     |
| 2          | 2019-02-01 | 2019-02-20 | 15     |
| 2          | 2019-02-21 | 2019-03-31 | 30     |
+------------+------------+------------+--------+
 
UnitsSold table:
+------------+---------------+-------+
| product_id | purchase_date | units |
+------------+---------------+-------+
| 1          | 2019-02-25    | 100   |
| 1          | 2019-03-01    | 15    |
| 2          | 2019-02-10    | 200   |
| 2          | 2019-03-22    | 30    |
+------------+---------------+-------+

Result table:
+------------+---------------+
| product_id | average_price |
+------------+---------------+
| 1          | 6.96          |
| 2          | 16.96         |
+------------+---------------+
Average selling price = Total Price of Product / Number of products sold.
Average selling price for product 1 = ((100 * 5) + (15 *&nbsp;20)) / 115 =&nbsp;6.96
Average selling price for product 2 = ((200 * 15) + (30&nbsp;* 30)) / 230 =&nbsp;16.96
</pre>
</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple and concise MySQL solution beats 100%
- Author: eminem18753
- Creation Date: Fri Nov 08 2019 11:18:50 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Nov 08 2019 23:28:55 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below
SELECT a.product_id,ROUND(SUM(b.units*a.price)/SUM(b.units),2) as average_price
FROM Prices as a
JOIN UnitsSold as b
ON a.product_id=b.product_id AND (b.purchase_date BETWEEN a.start_date AND a.end_date)
GROUP BY product_id;
```
</p>


### MySQL Solution- the easiest
- Author: Masquerader
- Creation Date: Mon Jan 06 2020 07:33:26 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 06 2020 07:34:18 GMT+0800 (Singapore Standard Time)

<p>
```
select 
u.product_id, 
round(sum(u.units * p.price) / sum(u.units), 2) as average_price from 
prices p 
join unitssold u 
on p.product_id = u.product_id 
and u.purchase_date between p.start_date and p.end_date
group by 
u.product_id
</p>


### Faster than 81 % | Easy to Understand | MYSQL
- Author: Mrmagician
- Creation Date: Thu Mar 19 2020 17:22:02 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Mar 19 2020 17:23:05 GMT+0800 (Singapore Standard Time)

<p>
```
select 
	p.product_id, 
	round(sum(u.units*p.price)/sum(u.units), 2)  as average_price 
from prices as p join unitssold as u 
	where 
		u.product_id = p.product_id and 
		u.purchase_date <= p.end_date and 
		u.purchase_date >= p.start_date 
	group by p.product_id;
```

**I hope that you\'ve found it useful.**
*In that case, please do upvote*
</p>


