---
title: "Valid Parenthesis String"
weight: 610
#id: "valid-parenthesis-string"
---
## Description
<div class="description">
<p>
Given a string containing only three types of characters: '(', ')' and '*', write a function to check whether this string is valid. We define the validity of a string by these rules:
<ol>
<li>Any left parenthesis <code>'('</code> must have a corresponding right parenthesis <code>')'</code>.</li>
<li>Any right parenthesis <code>')'</code> must have a corresponding left parenthesis <code>'('</code>.</li>
<li>Left parenthesis <code>'('</code> must go before the corresponding right parenthesis <code>')'</code>.</li>
<li><code>'*'</code> could be treated as a single right parenthesis <code>')'</code> or a single left parenthesis <code>'('</code> or an empty string.</li>
<li>An empty string is also valid.</li>
</ol>
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> "()"
<b>Output:</b> True
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> "(*)"
<b>Output:</b> True
</pre>
</p>

<p><b>Example 3:</b><br />
<pre>
<b>Input:</b> "(*))"
<b>Output:</b> True
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The string size will be in the range [1, 100].</li>
</ol>
</p>
</div>

## Tags
- String (string)

## Companies
- Facebook - 8 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Alibaba - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Brute Force [Time Limit Exceeded]

**Intuition and Algorithm**

For each asterisk, let's try both possibilities.

<iframe src="https://leetcode.com/playground/HHVFGh2C/shared" frameBorder="0" name="HHVFGh2C" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N * 3^{N})$$, where $$N$$ is the length of the string.  For each asterisk we try 3 different values.  Thus, we could be checking the validity of up to $$3^N$$ strings.  Then, each check of validity is $$O(N)$$.

* Space Complexity:  $$O(N)$$, the space used by our character array.

---
#### Approach #2: Dynamic Programming [Accepted]

**Intuition and Algorithm**

Let `dp[i][j]` be `true` if and only if the interval `s[i], s[i+1], ..., s[j]` can be made valid.  Then `dp[i][j]` is true only if:

* `s[i]` is `'*'`, and the interval `s[i+1], s[i+2], ..., s[j]` can be made valid;

* or, `s[i]` can be made to be `'('`, and there is some `k` in `[i+1, j]` such that `s[k]` can be made to be `')'`, plus the two intervals cut by `s[k]` (`s[i+1: k]` and `s[k+1: j+1]`) can be made valid;

<iframe src="https://leetcode.com/playground/c2qhBzko/shared" frameBorder="0" name="c2qhBzko" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^3)$$, where $$N$$ is the length of the string.  There are $$O(N^2)$$ states corresponding to entries of `dp`, and we do an average of $$O(N)$$ work on each state.

* Space Complexity:  $$O(N^2)$$, the space used to store intermediate results in `dp`.

---
#### Approach #3: Greedy [Accepted]

**Intuition**

When checking whether the string is valid, we only cared about the "`balance`": the number of extra, open left brackets as we parsed through the string.  For example, when checking whether '(()())' is valid, we had a balance of `1, 2, 1, 2, 1, 0` as we parse through the string: `'('` has 1 left bracket, `'(('` has 2, `'(()'` has 1, and so on.  This means that after parsing the first `i` symbols, (which may include asterisks,) we only need to keep track of what the `balance` could be.

For example, if we have string `'(***)'`, then as we parse each symbol, the set of possible values for the `balance` is `[1]` for `'('`; `[0, 1, 2]` for `'(*'`; `[0, 1, 2, 3]` for `'(**'`; `[0, 1, 2, 3, 4]` for `'(***'`, and `[0, 1, 2, 3]` for `'(***)'`.

Furthermore, we can prove these states always form a contiguous interval.  Thus, we only need to know the left and right bounds of this interval.  That is, we would keep those intermediate states described above as `[lo, hi] = [1, 1], [0, 2], [0, 3], [0, 4], [0, 3]`.

**Algorithm**

Let `lo, hi` respectively be the smallest and largest possible number of open left brackets after processing the current character in the string.

If we encounter a left bracket (`c == '('`), then `lo++`, otherwise we could write a right bracket, so `lo--`.  If we encounter what can be a left bracket (`c != ')'`), then `hi++`, otherwise we must write a right bracket, so `hi--`.  If `hi < 0`, then the current prefix can't be made valid no matter what our choices are.  Also, we can never have less than `0` open left brackets.  At the end, we should check that we can have exactly 0 open left brackets.



<iframe src="https://leetcode.com/playground/AP7MmhXJ/shared" frameBorder="0" name="AP7MmhXJ" width="100%" height="258"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of the string.  We iterate through the string once.

* Space Complexity:  $$O(1)$$, the space used by our `lo` and `hi` pointers.  However, creating a new character array will take $$O(N)$$ space.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short Java O(n) time, O(1) space, one pass
- Author: sansi
- Creation Date: Sun Sep 17 2017 13:46:03 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 09:07:17 GMT+0800 (Singapore Standard Time)

<p>
The idea is to similar to validate a string only contains '(' and ')'. But extend it to tracking the lower and upper bound of valid '(' counts. My thinking process is as following.

scan from left to right, and record counts of unpaired \u2018(\u2019 for all possible cases. For \u2018(\u2019 and \u2018)\u2019, it is straightforward, just increment and decrement all counts, respectively. 
When the character is '\*', there are three cases, \u2018(\u2019, empty, or \u2018)\u2019, we can think those three cases as three branches in the ongoing route. 
Take \u201c(**())\u201d as an example. There are 6 chars:
----At step 0: only one count = 1.
----At step 1: the route will be diverted into three branches. 
so there are three counts: 1 - 1, 1, 1 + 1 which is 0, 1, 2, for \u2018)\u2019, empty and \u2018(\u2019 respectively.
----At step 2 each route is diverged into three routes again. so there will be 9 possible routes now. 
--	For count = 0, it will be diverted into 0 \u2013 1, 0, 0 + 1, which is -1, 0, 1, but when the count is -1, that means there are more \u2018)\u2019s than \u2018(\u2019s, and we need to stop early at that route, since it is invalid. we end with 0, 1.
--	For count = 1, it will be diverted into 1 \u2013 1, 1, 1 + 1, which is 0, 1, 2
--	For count = 2, it will be diverted into 2 \u2013 1, 2, 2 + 1, which is 1, 2, 3
To summarize step 2, we end up with counts of 0,1,2,3
----At step 3, increment all counts --> 1,2,3,4
----At step 4, decrement all counts --> 0,1,2,3
----At step 5, decrement all counts --> -1, 0,1,2,  the route with count -1 is invalid, so stop early at that route. Now we have 0,1,2.
In the very end, we find that there is a route that can reach count == 0. Which means all \u2018(\u2019 and \u2018)\u2019 are cancelled. So, the original String is valid.
Another finding is counts of unpaired \u2018(\u2019 for all valid routes are consecutive integers. So we only need to keep a lower and upper bound of that consecutive integers to save space.
One case doesn\u2019t show up in the example is: if the upper bound is negative, that means all routes have more \u2018)\u2019 than \u2018(\u2019 --> all routes are invalid --> stop and return false.

Hope this explanation helps.

```
    public boolean checkValidString(String s) {
        int low = 0;
        int high = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                low++;
                high++;
            } else if (s.charAt(i) == ')') {
                if (low > 0) {
                    low--;
                }
                high--;
            } else {
                if (low > 0) {
                    low--;
                }
                high++;
            }
            if (high < 0) {
                return false;
            }
        }
        return low == 0;
    }
```
</p>


### [Java] Count Open Parenthesis - O(n) time, O(1) space - Picture Explain
- Author: hiepit
- Creation Date: Wed Mar 18 2020 23:00:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 18:16:09 GMT+0800 (Singapore Standard Time)

<p>
**Problem 1: Check Valid Parenthesis of a string containing only two types of characters: \'(\', \')\'**
```java
class Solution {
    public boolean checkValidString(String s) {
        int openCount = 0;
        for (char c : s.toCharArray()) {
            if (c == \'(\') {
                openCount++;
            } else if (c == \')\') {
                openCount--;
            }
            if (openCount < 0) return false;    // Currently, don\'t have enough open parentheses to match close parentheses-> Invalid
                                                // For example: ())(
        }
        return openCount == 0; // Fully match open parentheses with close parentheses
    }
}
```

**Problem 2: Check Valid Parenthesis of a string containing only three types of characters: \'(\', \')\', \'*\'**
Example:
![image](https://assets.leetcode.com/users/hiepit/image_1587291349.png)


```java
class Solution {
    public boolean checkValidString(String s) {
        int cmin = 0, cmax = 0; // open parentheses count in range [cmin, cmax]
        for (char c : s.toCharArray()) {
            if (c == \'(\') {
                cmax++;
                cmin++;
            } else if (c == \')\') {
                cmax--;
                cmin--;
            } else if (c == \'*\') {
                cmax++; // if `*` become `(` then openCount++
                cmin--; // if `*` become `)` then openCount--
                // if `*` become `` then nothing happens
                // So openCount will be in new range [cmin-1, cmax+1]
            }
            if (cmax < 0) return false; // Currently, don\'t have enough open parentheses to match close parentheses-> Invalid
                                        // For example: ())(
            cmin = Math.max(cmin, 0);   // It\'s invalid if open parentheses count < 0 that\'s why cmin can\'t be negative
        }
        return cmin == 0; // Return true if can found `openCount == 0` in range [cmin, cmax]
    }
}
```
Complexity
- Time: `O(n)`
- Space: `O(1)`

Please help to **UPVOTE** if this post is useful for you. 
If you have any questions, feel free to comment below.
Happy coding!
</p>


### [Java/C++/Python] One Pass, Count the Open Parenthesis
- Author: lee215
- Creation Date: Sun Sep 17 2017 19:39:55 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 03:04:02 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**:
One pass on the string `S`,
we need to know,
how many \')\' we are waiting for.

If we meet too many \')\', we can return `false` directly.
If we wait for no \')\' at the end, then we are good.

<br>

## **Explanation**:
We count the number of \')\' we are waiting for,
and it\'s equal to the number of open parenthesis.
This number will be in a range and we count it as ```[cmin, cmax]```

```cmax``` counts the maximum open parenthesis,
which means the maximum number of unbalanced \'(\' that **COULD** be paired.
```cmin``` counts the minimum open parenthesis,
which means the number of unbalanced \'(\' that **MUST** be paired.

<br>

## **Example**:
It\'s quite straight forward actually.
When you met "(", you know you need one only one ")", `cmin = 1` and `cmax = 1`.
When you met "(*(", you know you need one/two/three ")",  `cmin = 1` and `cmax = 3`.

The string is valid for 2 condition:
1. ```cmax``` will never be negative.
2. ```cmin``` is 0 at the end.

<br>

## **Time Complexity**:
One pass `O(N)` time, Space `O(1)`

<br>

**Java:**
```
    public boolean checkValidString(String s) {
        int cmin = 0, cmax = 0;
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (c == \'(\') {
                cmax++;
                cmin++;
            } else if (c == \')\') {
                cmax--;
                cmin = Math.max(cmin - 1, 0);
            } else {
                cmax++;
                cmin = Math.max(cmin - 1, 0);
            }
            if (cmax < 0) return false;
        }
        return cmin == 0;
    }
```

**C++:**
```
    bool checkValidString(string s) {
        int cmin = 0, cmax = 0;
        for (char c : s) {
            if (c == \'(\')
                cmax++, cmin++;
            if (c == \')\')
                cmax--, cmin = max(cmin - 1, 0);
            if (c == \'*\')
                cmax++, cmin = max(cmin - 1, 0);
            if (cmax < 0) return false;
        }
        return cmin == 0;
    }
```

**Python:**
Edited after vonzcy\'s suggestion.
````
def checkValidString(self, s):
        cmin = cmax = 0
        for i in s:
            if i == \'(\':
                cmax += 1
                cmin += 1
            if i == \')\':
                cmax -= 1
                cmin = max(cmin - 1, 0)
            if i == \'*\':
                cmax += 1
                cmin = max(cmin - 1, 0)
            if cmax < 0:
                return False
        return cmin == 0
````
or shorter

````
    def checkValidString(self, s):
        cmin = cmax = 0
        for i in s:
            cmax = cmax - 1 if i == ")" else cmax + 1
            cmin = cmin + 1 if i == \'(\' else max(cmin - 1, 0)
            if cmax < 0: return False
        return cmin == 0
````
</p>


