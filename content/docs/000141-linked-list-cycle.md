---
title: "Linked List Cycle"
weight: 141
#id: "linked-list-cycle"
---
## Description
<div class="description">
<p>Given <code>head</code>, the head of a linked list, determine if the linked list has a cycle in it.</p>

<p>There is a cycle in a linked list if there is some node in the list that can be reached again by continuously following the&nbsp;<code>next</code>&nbsp;pointer. Internally, <code>pos</code>&nbsp;is used to denote the index of the node that&nbsp;tail&#39;s&nbsp;<code>next</code>&nbsp;pointer is connected to.&nbsp;<strong>Note that&nbsp;<code>pos</code>&nbsp;is not passed as a parameter</strong>.</p>

<p>Return&nbsp;<code>true</code><em> if there is a cycle in the linked list</em>. Otherwise, return <code>false</code>.</p>

<p><strong>Follow up:</strong></p>

<p>Can you solve it using <code>O(1)</code> (i.e. constant) memory?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/07/circularlinkedlist.png" style="width: 300px; height: 97px; margin-top: 8px; margin-bottom: 8px;" />
<pre>
<strong>Input:</strong> head = [3,2,0,-4], pos = 1
<strong>Output:</strong> true
<strong>Explanation:</strong> There is a cycle in the linked list, where the tail connects to the 1st node (0-indexed).
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/07/circularlinkedlist_test2.png" style="width: 141px; height: 74px;" />
<pre>
<strong>Input:</strong> head = [1,2], pos = 0
<strong>Output:</strong> true
<strong>Explanation:</strong> There is a cycle in the linked list, where the tail connects to the 0th node.
</pre>

<p><strong>Example 3:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/07/circularlinkedlist_test3.png" style="width: 45px; height: 45px;" />
<pre>
<strong>Input:</strong> head = [1], pos = -1
<strong>Output:</strong> false
<strong>Explanation:</strong> There is no cycle in the linked list.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of the nodes in the list is in the range <code>[0, 10<sup>4</sup>]</code>.</li>
	<li><code>-10<sup>5</sup> &lt;= Node.val &lt;= 10<sup>5</sup></code></li>
	<li><code>pos</code> is <code>-1</code> or a <strong>valid index</strong> in the linked-list.</li>
</ul>

</div>

## Tags
- Linked List (linked-list)
- Two Pointers (two-pointers)

## Companies
- Microsoft - 4 (taggedByAdmin: true)
- Facebook - 3 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: true)
- Goldman Sachs - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: true)
- Walmart Labs - 3 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Bloomberg - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Summary

This article is for beginners. It introduces the following ideas: Linked List, Hash Table and Two Pointers.

## Solution
---
#### Approach 1: Hash Table

**Intuition**

To detect if a list is cyclic, we can check whether a node had been visited before. A natural way is to use a hash table.

**Algorithm**

We go through each node one by one and record each node's reference (or memory address) in a hash table. If the current node is `null`, we have reached the end of the list and it must not be cyclic. If current node’s reference is in the hash table, then return true.

<iframe src="https://leetcode.com/playground/6LXcssV3/shared" frameBorder="0" width="100%" height="259" name="6LXcssV3"></iframe>

**Complexity analysis**

* Time complexity : $$O(n)$$.
We visit each of the $$n$$ elements in the list at most once. Adding a node to the hash table costs only $$O(1)$$ time.

* Space complexity: $$O(n)$$.
The space depends on the number of elements added to the hash table, which contains at most $$n$$ elements.
<br />
<br />
---
#### Approach 2: Two Pointers

**Intuition**

Imagine two runners running on a track at different speed. What happens when the track is actually a circle?

**Algorithm**

The space complexity can be reduced to $$O(1)$$ by considering two pointers at **different speed** - a slow pointer and a fast pointer. The slow pointer moves one step at a time while the fast pointer moves two steps at a time.

If there is no cycle in the list, the fast pointer will eventually reach the end and we can return false in this case.

Now consider a cyclic list and imagine the slow and fast pointers are two runners racing around a circle track. The fast runner will eventually meet the slow runner. Why? Consider this case (we name it case A) - The fast runner is just one step behind the slow runner. In the next iteration, they both increment one and two steps respectively and meet each other.

How about other cases? For example, we have not considered cases where the fast runner is two or three steps behind the slow runner yet. This is simple, because in the next or next's next iteration, this case will be reduced to case A mentioned above.

<iframe src="https://leetcode.com/playground/CbGgPHtF/shared" frameBorder="0" width="100%" height="361" name="CbGgPHtF"></iframe>

**Complexity analysis**

* Time complexity : $$O(n)$$.
Let us denote $$n$$ as the total number of nodes in the linked list. To analyze its time complexity, we consider the following two cases separately.

    - ***List has no cycle:***  
    The fast pointer reaches the end first and the run time depends on the list's length, which is $$O(n)$$.

    - ***List has a cycle:***  
    We break down the movement of the slow pointer into two steps, the non-cyclic part and the cyclic part:

        1. The slow pointer takes "non-cyclic length" steps to enter the cycle. At this point, the fast pointer has already reached the cycle. $$\text{Number of iterations} = \text{non-cyclic length} = N$$

        2. Both pointers are now in the cycle. Consider two runners running in a cycle - the fast runner moves 2 steps while the slow runner moves 1 steps at a time. Since the speed difference is 1, it takes $$\dfrac{\text{distance between the 2 runners}}{\text{difference of speed}}$$ loops for the fast runner to catch up with the slow runner. As the distance is at most "$$\text{cyclic length K}$$" and the speed difference is 1, we conclude that      
        $$\text{Number of iterations} = \text{almost}$$ "$$\text{cyclic length K}$$".

    Therefore, the worst case time complexity is $$O(N+K)$$, which is $$O(n)$$.

* Space complexity : $$O(1)$$.
We only use two nodes (slow and fast) so the space complexity is $$O(1)$$.

## Accepted Submission (python3)
```python3
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def hasCycle(self, head: ListNode) -> bool:
        fast = head
        slow = head
        try:
            while True:
                slow = slow.next
                fast = fast.next.next
                if fast == slow:
                    return True
        except:
            return False

```

## Top Discussions
### O(1) Space Solution
- Author: fabrizio3
- Creation Date: Wed Apr 22 2015 16:01:19 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 23:08:26 GMT+0800 (Singapore Standard Time)

<p>
```
public boolean hasCycle(ListNode head) {
    if(head==null) return false;
    ListNode walker = head;
    ListNode runner = head;
    while(runner.next!=null && runner.next.next!=null) {
        walker = walker.next;
        runner = runner.next.next;
        if(walker==runner) return true;
    }
    return false;
}
```
 1. Use two pointers, **walker** and **runner**.
 2. **walker** moves step by step. **runner** moves two steps at time.
 3. if the Linked List has a cycle **walker** and **runner** will meet at some
    point.
</p>


### Except-ionally fast Python
- Author: StefanPochmann
- Creation Date: Sat Jun 13 2015 06:21:12 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 05:12:29 GMT+0800 (Singapore Standard Time)

<p>
Took 88 ms and the "Accepted Solutions Runtime Distribution" doesn't show any faster Python submissions. The "trick" is to not check all the time whether we have reached the end but to handle it via an exception. ["Easier to ask for forgiveness than permission."](https://docs.python.org/3/glossary.html#term-eafp)

The algorithm is of course [Tortoise and hare](https://en.wikipedia.org/wiki/Cycle_detection#Tortoise_and_hare).

    def hasCycle(self, head):
        try:
            slow = head
            fast = head.next
            while slow is not fast:
                slow = slow.next
                fast = fast.next.next
            return True
        except:
            return False
</p>


### Confusing input
- Author: HT_Wang
- Creation Date: Sun Jul 14 2019 07:30:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 14 2019 07:30:29 GMT+0800 (Singapore Standard Time)

<p>
I don\'t see why we need \'pos\' in the inputs, while we don\'t see it in the code. It\'s kind confusing.
</p>


