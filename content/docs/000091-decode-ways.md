---
title: "Decode Ways"
weight: 91
#id: "decode-ways"
---
## Description
<div class="description">
<p>A message containing letters from <code>A-Z</code> is being encoded to numbers using the following mapping:</p>

<pre>
&#39;A&#39; -&gt; 1
&#39;B&#39; -&gt; 2
...
&#39;Z&#39; -&gt; 26
</pre>

<p>Given a <strong>non-empty</strong> string containing only digits, determine the total number of ways to decode it.</p>

<p>The answer is guaranteed to fit in a <strong>32-bit</strong> integer.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;12&quot;
<strong>Output:</strong> 2
<strong>Explanation:</strong> It could be decoded as &quot;AB&quot; (1 2) or &quot;L&quot; (12).
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;226&quot;
<strong>Output:</strong> 3
<strong>Explanation:</strong> It could be decoded as &quot;BZ&quot; (2 26), &quot;VF&quot; (22 6), or &quot;BBF&quot; (2 2 6).
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;0&quot;
<strong>Output:</strong> 0
<strong>Explanation:</strong> There is no character that is mapped to a number starting with &#39;0&#39;. We cannot ignore a zero when we face it while decoding. So, each &#39;0&#39; should be part of &quot;10&quot; --&gt; &#39;J&#39; or &quot;20&quot; --&gt; &#39;T&#39;.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1&quot;
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 100</code></li>
	<li><code>s</code> contains only digits and may contain leading zero(s).</li>
</ul>

</div>

## Tags
- String (string)
- Dynamic Programming (dynamic-programming)

## Companies
- JPMorgan - 98 (taggedByAdmin: false)
- Cisco - 25 (taggedByAdmin: false)
- Facebook - 10 (taggedByAdmin: true)
- Amazon - 7 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: true)
- Bloomberg - 3 (taggedByAdmin: false)
- Goldman Sachs - 3 (taggedByAdmin: false)
- Lyft - 3 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Morgan Stanley - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Databricks - 2 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Square - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Uber - 8 (taggedByAdmin: true)
- Baidu - 3 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- Huawei - 2 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- Barclays - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

The most important point to understand in this problem is that at any given step when you are trying to decode a string of numbers it can either be a single digit decode e.g. `1` to `A` or a double digit decode e.g. `25` to `Y`. As long as it's a valid decoding we move ahead to decode the rest of the string.

The subproblem could be thought of as number of ways decoding a substring.

<center>
<img src="../Figures/91/91_Decode_Ways_1.png" width="800"/>
</center>
<br>
The above diagram shows string "326" could be decoded in two ways.
<br/>
<br/>

---

#### Approach 1: Recursive Approach with Memoization

**Intuition**

The problem deals with finding number of ways of decoding a string. What helps to crack the problem is to think why there would be many ways to decode a string. The reason is simple since at any given point we either decode using `two digits` or `single` digit. This choice while decoding can lead to different combinations.

<center>
<img src="../Figures/91/91_Decode_Ways_2.png" width="800"/>
</center>
<br>

Thus at any given time for a string we enter a recursion after successfully decoding two digits to a single character or a single digit to a character. This leads to multiple paths to decoding the entire string. If a given path leads to the end of the string this means we could successfully decode the string. If at any point in the traversal we encounter digits which cannot be decoded, we backtrack from that path.

<center>
<img src="../Figures/91/91_Decode_Ways_3.png" width="500"/>
</center>
<br>

In the following diagram we can see how the paths have to deal with similar subproblems. Overlapping subproblems means we can reuse the answer. Thus, we do memoization to solve this problem.

<center>
<img src="../Figures/91/91_Decode_Ways_4.png" width=800"/>
</center>
<br>

**Algorithm**

1. Enter recursion with the given string i.e. start with index 0.

2. For the terminating case of the recursion we check for the end of the string. If we have reached the end of the string we return `1`.

3. Every time we enter recursion it's for a substring of the original string. For any recursion if the first character is `0` then terminate that path by returning `0`. Thus this path won't contribute to the number of ways.

4. Memoization helps to reduce the complexity which would otherwise be exponential. We check the dictionary `memo` to see if the result for the given substring already exists.

5. If the result is already in `memo` we return the result. Otherwise the number of ways for the given string is determined by making a recursive call to the function with `index + 1` for next substring string and `index + 2` after checking for valid 2-digit decode. The result is also stored in `memo` with key as current index, for saving for future overlapping subproblems.

<iframe src="https://leetcode.com/playground/TVe5UR8v/shared" frameBorder="0" width="100%" height="500" name="TVe5UR8v"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is length of the string. Memoization helps in pruning the recursion tree and hence decoding for an index only once. Thus this solution is linear time complexity.

* Space Complexity: $$O(N)$$. The dictionary used for memoization would take the space equal to the length of the string. There would be an entry for each index value. The recursion stack would also be equal to the length of the string.
<br/>
<br/>

---

#### Approach 2: Iterative Approach

The iterative approach might be a little bit less intuitive. Let's try to understand it. We use an array for DP to store the results for subproblems. A cell with index `i` of the `dp` array is used to store the number of decode ways for substring of `s` from index `0` to index `i-1`.

We initialize the starting two indices of the `dp` array. It's similar to relay race where the first runner is given a `baton` to be passed to the subsequent runners. The first two indices of the `dp` array hold a baton. As we iterate the `dp` array from left to right this baton which signifies the number of ways of decoding is passed to the next index or not depending on whether the decode is possible.

`dp[i]` can get the baton from two other previous indices, either `i-1` or `i-2`. Two previous indices are involved since both single and two digit decodes are possible.

Unlike the relay race we don't get only one baton in the end. The batons add up as we pass on. If someone has one baton, they can provide a copy of it to everyone who comes to them with a success. Thus, leading to number of ways of reaching the end.
<center>
<img src="../Figures/91/91_Decode_Ways_5.png" width="800"/>
</center>
<br>

`dp[i]` = Number of ways of decoding substring `s[:i]`. So we might say `dp[i] = dp[i-1] + dp[i-2]`, which is not always true for this decode ways problem. As shown in the above diagram, only when the decode is possible we add the results of the previous indices. Thus, in this race we don't just pass the baton. The baton is passed to the next index or not depending on possibility of the decode.

**Algorithm**

1. If the string `s` is empty or null we return the result as `0`.

2. Initialize `dp` array. dp[0] = 1 to provide the baton to be passed.

3. If the first character of the string is zero then no decode is possible hence initialize dp[1] to `0`, otherwise the first character is valid to pass on the baton, dp[1] = 1.

3. Iterate the `dp` array starting at index 2. The index `i` of dp is the i-th character of the string `s`, that is character at index `i-1` of s.

4. We check if valid single digit decode is possible. This just means the character at index `s[i-1]` is non-zero. Since we do not have a decoding for zero. If the valid single digit decoding is possible then we add `dp[i-1]` to `dp[i]`. Since all the ways up to (i-1)-th character now lead up to i-th character too.

5. We check if valid two digit decode is possible. This means the substring `s[i-2]s[i-1]` is between 10 to 26. If the valid two digit decoding is possible then we add `dp[i-2]` to `dp[i]`.

6. Once we reach the end of the `dp` array we would have the number of ways of decoding string `s`.

<iframe src="https://leetcode.com/playground/oSu7daX8/shared" frameBorder="0" width="100%" height="500" name="oSu7daX8"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is length of the string. We iterate the length of `dp` array which is $$N+1$$.

* Space Complexity: $$O(N)$$. The length of the DP array.
<br/>
<br/>

---

#### Follow up : Optimized Iterative Approach, $$O(1)$$ Space

In Approach 2 we are using an array `dp` to save the results for future. As we move ahead character by character of the given string, we look back only two steps. For calculating `dp[i]` we need to know `dp[i-1]` and `dp[i-2]` only. Thus, we can easily cut down our $$O(N)$$ space requirement to $$O(1)$$ by using only two variables to store the last two results.

Thanks [Takkes](https://leetcode.com/takkes/) for suggesting this optimization to second approach.
<br/>

## Accepted Submission (scala)
```scala
object Solution {
    def numDecodings(s: String): Int = {
        val sl = s.length()
        var ways = new Array[Int](sl + 1)
        ways(sl) = 1
        ways(sl - 1) = if (s(sl - 1) == '0') 0 else 1
        for (i <- sl - 2 to 0 by -1) {
            if (s(i) != '0') {
                val vi = s.substring(i, i + 2).toInt
                if (vi <= 26) {
                    ways(i) = ways(i + 1) + ways(i + 2)
                } else {
                    ways(i) = ways(i + 1)
                }
            }
        }
        return ways(0)
    }
}
```

## Top Discussions
### Java clean DP solution with explanation
- Author: yfcheng
- Creation Date: Sun Jan 31 2016 01:41:04 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 13 2020 08:28:31 GMT+0800 (Singapore Standard Time)

<p>
I used a dp array of size n + 1 to save subproblem solutions.  `dp[0]` means an empty string will have one way to decode, `dp[1]` means the way to decode a string of size 1.  I then check one digit and two digit combination and save the results along the way.  In the end, `dp[n]` will be the end result.

    public class Solution {
        public int numDecodings(String s) {
            if (s == null || s.length() == 0) {
                return 0;
            }
            int n = s.length();
            int[] dp = new int[n + 1];
            dp[0] = 1;
            dp[1] = s.charAt(0) != \'0\' ? 1 : 0;
            for (int i = 2; i <= n; i++) {
                int first = Integer.valueOf(s.substring(i - 1, i));
                int second = Integer.valueOf(s.substring(i - 2, i));
                if (first >= 1 && first <= 9) {
                   dp[i] += dp[i-1];  
                }
                if (second >= 10 && second <= 26) {
                    dp[i] += dp[i-2];
                }
            }
            return dp[n];
        }
    }
</p>


### Evolve from recursion to dp
- Author: yu6
- Creation Date: Thu Nov 03 2016 07:14:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 29 2020 15:28:39 GMT+0800 (Singapore Standard Time)

<p>
1. Recursion O(2^n). A char may be decoded alone or by pairing with the next char.
* c++
```
    int numDecodings(string s) {
        return s.empty() ? 0: numDecodings(0,s);    
    }
    int numDecodings(int p, string& s) {
        int n = s.size();
        if(p == n) return 1;
        if(s[p] == \'0\') return 0; // sub string starting with 0 is not a valid encoding
        int res = numDecodings(p+1,s);
        if( p < n-1 && (s[p]==\'1\'|| (s[p]==\'2\'&& s[p+1]<\'7\'))) res += numDecodings(p+2,s);
        return res;
    }
```
* java
```
	public int numDecodings(String s) {
        return s.length()==0?0:numDecodings(0,s);      
    }
    private int numDecodings(int p, String s) {
        int n=s.length();
        if(p==n) return 1;
        if(s.charAt(p)==\'0\') return 0;
        int res=numDecodings(p+1,s);
        if(p<n-1&&(s.charAt(p)==\'1\'||s.charAt(p)==\'2\'&&s.charAt(p+1)<\'7\')) 
			res+=numDecodings(p+2,s);
        return res;
    }
```
2. Memoization O(n)
* c++
```
    int numDecodings(string s) {
        int n = s.size();
        vector<int> mem(n+1,-1);
        mem[n]=1;
        return s.empty()? 0 : num(0,s,mem);   
    }
    int num(int i, string &s, vector<int> &mem) {
        if(mem[i]>-1) return mem[i];
        if(s[i]==\'0\') return mem[i] = 0;
        int res = num(i+1,s,mem);
        if(i<s.size()-1 && (s[i]==\'1\'||s[i]==\'2\'&&s[i+1]<\'7\')) res+=num(i+2,s,mem);
        return mem[i] = res;
    }
``` 
* java
```
	public int numDecodings(String s) {
        int n=s.length();
        Integer[] mem=new Integer[n];
        return s.length()==0?0:numDecodings(0,s,mem);      
    }
    private int numDecodings(int p, String s, Integer[] mem) {
        int n=s.length();
        if(p==n) return 1;
        if(s.charAt(p)==\'0\') return 0;
        if(mem[p]!=null) return mem[p];
        int res=numDecodings(p+1,s,mem);
        if(p<n-1&&(s.charAt(p)==\'1\'||s.charAt(p)==\'2\'&&s.charAt(p+1)<\'7\')) 
			res+=numDecodings(p+2,s,mem);
        return mem[p]=res;
    }
```
3. dp O(n) time and space, this can be converted from #2 with copy and paste.
* c++
```
    int numDecodings(string s) {
        int n = s.size();
        vector<int> dp(n+1);
        dp[n] = 1;
        for(int i=n-1;i>=0;i--) {
            if(s[i]==\'0\') dp[i]=0;
            else {
                dp[i] = dp[i+1];
                if(i<n-1 && (s[i]==\'1\'||s[i]==\'2\'&&s[i+1]<\'7\')) dp[i]+=dp[i+2];
            }
        }
        return s.empty()? 0 : dp[0];   
    }
```
* java
```
	public int numDecodings(String s) {
        int n=s.length();
        int[] dp=new int[n+1];
        dp[n]=1;
        for(int i=n-1;i>=0;i--)
            if(s.charAt(i)!=\'0\') {
                dp[i]=dp[i+1];
                if(i<n-1&&(s.charAt(i)==\'1\'||s.charAt(i)==\'2\'&&s.charAt(i+1)<\'7\')) 
					dp[i]+=dp[i+2];
            }
        return dp[0];   
    }
```
4. dp constant space
* c++
```
    int numDecodings(string s) {
        int p = 1, pp, n = s.size();
        for(int i=n-1;i>=0;i--) {
            int cur = s[i]==\'0\' ? 0 : p;
            if(i<n-1 && (s[i]==\'1\'||s[i]==\'2\'&&s[i+1]<\'7\')) cur+=pp;
            pp = p;
            p = cur;
        }
        return s.empty()? 0 : p;   
    }
```
* java
```
	public int numDecodings(String s) {
        int dp1=1, dp2=0, n=s.length();
        for(int i=n-1;i>=0;i--) {
            int dp=s.charAt(i)==\'0\'?0:dp1;
            if(i<n-1&&(s.charAt(i)==\'1\'||s.charAt(i)==\'2\'&&s.charAt(i+1)<\'7\'))
                dp+=dp2;
            dp2=dp1;
            dp1=dp;
        }
        return dp1;
    }
```
A follow up of the problem is[ 639. Decode Ways II](https://leetcode.com/problems/decode-ways-ii/discuss/686044/Evolve-from-recursion-to-dp).
</p>


### DP Solution (Java) for reference
- Author: manky
- Creation Date: Sun Aug 03 2014 23:23:09 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 08:16:16 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public int numDecodings(String s) {
            int n = s.length();
            if (n == 0) return 0;
            
            int[] memo = new int[n+1];
            memo[n]  = 1;
            memo[n-1] = s.charAt(n-1) != '0' ? 1 : 0;
            
            for (int i = n - 2; i >= 0; i--)
                if (s.charAt(i) == '0') continue;
                else memo[i] = (Integer.parseInt(s.substring(i,i+2))<=26) ? memo[i+1]+memo[i+2] : memo[i+1];
            
            return memo[0];
        }
    }
</p>


