---
title: "Check If All 1's Are at Least Length K Places Away"
weight: 1324
#id: "check-if-all-1s-are-at-least-length-k-places-away"
---
## Description
<div class="description">
<p>Given an array <code>nums</code> of 0s and 1s and an integer <code>k</code>, return <code>True</code> if all 1&#39;s are at least <code>k</code> places away from each other, otherwise return <code>False</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/04/15/sample_1_1791.png" style="width: 214px; height: 90px;" /></strong></p>

<pre>
<strong>Input:</strong> nums = [1,0,0,0,1,0,0,1], k = 2
<strong>Output:</strong> true
<strong>Explanation:</strong> Each of the 1s are at least 2 places away from each other.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/04/15/sample_2_1791.png" style="width: 160px; height: 86px;" /></strong></p>

<pre>
<strong>Input:</strong> nums = [1,0,0,1,0,1], k = 2
<strong>Output:</strong> false
<strong>Explanation: </strong>The second 1 and third 1 are only one apart from each other.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,1,1,1,1], k = 0
<strong>Output:</strong> true
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,1,0,1], k = 1
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10^5</code></li>
	<li><code>0 &lt;= k &lt;= nums.length</code></li>
	<li><code>nums[i]</code>&nbsp;is&nbsp;<code>0</code>&nbsp;or&nbsp;<code>1</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- United Health Group - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Why is this question medium? Python O(N)
- Author: danchenkong
- Creation Date: Mon May 04 2020 02:35:56 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 28 2020 04:38:35 GMT+0800 (Singapore Standard Time)

<p>
Just one pass check then got the answer. Time O(N)

```Python
class Solution:
    def kLengthApart(self, nums: List[int], k: int) -> bool:
        idx = -1
        for i in range(len(nums)):           
            if nums[i] == 1:
                if idx != -1 and i - idx - 1 < k:
                    return False
                idx = i
        return True
```
</p>


### Java Simple O(N) Solution
- Author: aniruddha_agrawal
- Creation Date: Sun May 03 2020 12:41:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 12:41:37 GMT+0800 (Singapore Standard Time)

<p>
class Solution {
 
	public boolean kLengthApart(int[] nums, int k) {
 
		for(int start = -1, i = 0; i < nums.length; i++) 
			if(nums[i] == 1) {
				if(start != -1 && i - start - 1 < k) return false;
				start = i;
			}
		return true;
	}
}

Please help to **UPVOTE** if this post is useful for you.
If you have any questions, feel free to comment below.
**Happy coding:)**

</p>


### Java O(n) Easy
- Author: vikrant_pc
- Creation Date: Sun May 03 2020 12:00:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 12:00:55 GMT+0800 (Singapore Standard Time)

<p>
```
public boolean kLengthApart(int[] nums, int k) {
	for(Integer previousOneIndex = null,i=0; i<nums.length; i++)
		if(nums[i]==1) {
			if(previousOneIndex != null && i-previousOneIndex <= k) return false;
			previousOneIndex = i;
		}
	return true;
}
```
</p>


