---
title: "Find Eventual Safe States"
weight: 741
#id: "find-eventual-safe-states"
---
## Description
<div class="description">
<p>In a directed graph, we start at some node and every turn, walk along a directed edge of the graph.&nbsp; If we reach a node that is terminal (that is, it has no outgoing directed edges), we stop.</p>

<p>Now, say our starting node is <em>eventually safe&nbsp;</em>if and only if we must eventually walk to a terminal node.&nbsp; More specifically, there exists a natural number <code>K</code> so that for any choice of where to walk, we must have stopped at a terminal node in less than <code>K</code> steps.</p>

<p>Which nodes are eventually safe?&nbsp; Return them as an array in sorted order.</p>

<p>The directed graph has <code>N</code> nodes with labels <code>0, 1, ..., N-1</code>, where <code>N</code> is the length of <code>graph</code>.&nbsp; The&nbsp;graph is given in the following form: <code>graph[i]</code> is a list of labels <code>j</code> such that <code>(i, j)</code> is a directed edge of the graph.</p>

<pre>
<strong>Example:</strong>
<strong>Input:</strong> graph = [[1,2],[2,3],[5],[0],[5],[],[]]
<strong>Output:</strong> [2,4,5,6]
Here is a diagram of the above graph.

</pre>

<p><img alt="Illustration of graph" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/03/17/picture1.png" style="height:86px; width:300px" /></p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>graph</code> will have length at most <code>10000</code>.</li>
	<li>The number of edges in the graph will not exceed <code>32000</code>.</li>
	<li>Each <code>graph[i]</code> will be a sorted list of different integers, chosen within the range <code>[0, graph.length - 1]</code>.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Graph (graph)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Reverse Edges [Accepted]

**Intuition**

The crux of the problem is whether you can reach a cycle from the node you start in.  If you can, then there is a way to avoid stopping indefinitely; and if you can't, then after some finite number of steps you'll stop.

Thinking about this property more, a node is eventually safe if all it's outgoing edges are to nodes that are eventually safe.

This gives us the following idea: we start with nodes that have no outgoing edges - those are eventually safe.  Now, we can update any nodes which only point to eventually safe nodes - those are also eventually safe.  Then, we can update again, and so on.

However, we'll need a good algorithm to make sure our updates are efficient.

**Algorithm**

We'll keep track of `graph`, a way to know for some node `i`, what the outgoing edges `(i, j)` are.  We'll also keep track of `rgraph`, a way to know for some node `j`, what the incoming edges `(i, j)` are.

Now for every node `j` which was declared eventually safe, we'll process them in a queue.  We'll look at all parents `i = rgraph[j]` and remove the edge `(i, j)` from the graph (from `graph`).  If this causes the `graph` to have no outgoing edges `graph[i]`, then we'll declare it eventually safe and add it to our queue.

Also, we'll keep track of everything we ever added to the queue, so we can read off the answer in sorted order later.

<iframe src="https://leetcode.com/playground/U4VgbRzv/shared" frameBorder="0" width="100%" height="500" name="U4VgbRzv"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N + E)$$, where $$N$$ is the number of nodes in the given graph, and $$E$$ is the total number of edges.

* Space Complexity: $$O(N)$$ in additional space complexity.


---
#### Approach #2: Depth-First Search [Accepted]

**Intuition**

As in *Approach #1*, the crux of the problem is whether you reach a cycle or not.

Let us perform a "brute force": a cycle-finding DFS algorithm on each node individually.  This is a classic "white-gray-black" DFS algorithm that would be part of any textbook on DFS.  We mark a node gray on entry, and black on exit.  If we see a gray node during our DFS, it must be part of a cycle.  In a naive view, we'll clear the colors between each search.

**Algorithm**

We can improve this approach, by noticing that we don't need to clear the colors between each search.

When we visit a node, the only possibilities are that we've marked the entire subtree black (which must be eventually safe), or it has a cycle and we have only marked the members of that cycle gray.  So indeed, the invariant that gray nodes are always part of a cycle, and black nodes are always eventually safe is maintained.

In order to exit our search quickly when we find a cycle (and not paint other nodes erroneously), we'll say the result of visiting a node is `true` if it is eventually safe, otherwise `false`.  This allows information that we've reached a cycle to propagate up the call stack so that we can terminate our search early.

<iframe src="https://leetcode.com/playground/fBBucNrE/shared" frameBorder="0" width="100%" height="500" name="fBBucNrE"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N + E)$$, where $$N$$ is the number of nodes in the given graph, and $$E$$ is the total number of edges.

* Space Complexity: $$O(N)$$ in additional space complexity.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### cut the crap in problem description
- Author: thekensai
- Creation Date: Tue Feb 26 2019 09:01:49 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Feb 26 2019 09:01:49 GMT+0800 (Singapore Standard Time)

<p>
\'Now, say our starting node is eventually safe if and only if we must eventually walk to a terminal node.  More specifically, there exists a natural number K so that for any choice of where to walk, we must have stopped at a terminal node in less than K steps.\'

ain\'t it just saying \'A node is evetually safe if all path from the node ends at a terminal node\'
</p>


### Straightforward Java solution, easy to understand!
- Author: kevincongcc
- Creation Date: Mon Mar 19 2018 15:06:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 15:15:35 GMT+0800 (Singapore Standard Time)

<p>
value of color represents three states:
0:have not been visited
1:safe
2:unsafe
For DFS,we need to do some optimization.When we travel a path,we mark the node with 2 which represents having been visited,and when we encounter a node which results in a cycle,we return false,all node in the path stays 2 and it represents unsafe.And in the following traveling,whenever we encounter a node which points to a node marked with 2,we know it will results in a cycle,so we can stop traveling.On the contrary,when a node is safe,we can mark it with 1 and whenever we encounter a safe node,we know it will not results in a cycle.

```
class Solution {
    public List<Integer> eventualSafeNodes(int[][] graph) {
        List<Integer> res = new ArrayList<>();
        if(graph == null || graph.length == 0)  return res;
        
        int nodeCount = graph.length;
        int[] color = new int[nodeCount];
        
        for(int i = 0;i < nodeCount;i++){
            if(dfs(graph, i, color))    res.add(i);
        }
        
        return res;
    }
    public boolean dfs(int[][] graph, int start, int[] color){
        if(color[start] != 0)   return color[start] == 1;
        
        color[start] = 2;
        for(int newNode : graph[start]){
            if(!dfs(graph, newNode, color))    return false;
        }
        color[start] = 1;
        
        return true;
    }
}
```
</p>


### 20-line Python concise sol by removing 0 out degree nodes
- Author: LuckyPants
- Creation Date: Mon Mar 19 2018 11:41:06 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 11:02:35 GMT+0800 (Singapore Standard Time)

<p>
This is equal to find nodes which doesn\'t lead to a circle in any path.
We can solve it by walking along the path reversely.

1. Find nodes with out degree 0, they are terminal nodes, we remove them from graph and they are added to result
2. For nodes who are connected terminal nodes, since terminal nodes are removed, we decrease in-nodes\' out degree by 1 and if its out degree equals to 0, it become new terminal nodes
3. Repeat 2 until no terminal nodes can be found.

Here is my 300ms 20-line Python Code:

```
def eventualSafeNodes(self, graph):
    """
    :type graph: List[List[int]]
    :rtype: List[int]
    """
    n = len(graph)
    out_degree = collections.defaultdict(int)
    in_nodes = collections.defaultdict(list) 
    queue = []
    ret = []
    for i in range(n):
        out_degree[i] = len(graph[i])
        if out_degree[i]==0:
            queue.append(i)
        for j in graph[i]:
            in_nodes[j].append(i)  
    while queue:
        term_node = queue.pop(0)
        ret.append(term_node)
        for in_node in in_nodes[term_node]:
            out_degree[in_node] -= 1
            if out_degree[in_node]==0:
                queue.append(in_node)
    return sorted(ret)
```

</p>


