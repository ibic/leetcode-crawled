---
title: "Surrounded Regions"
weight: 130
#id: "surrounded-regions"
---
## Description
<div class="description">
<p>Given a 2D board containing <code>&#39;X&#39;</code> and <code>&#39;O&#39;</code> (<strong>the letter O</strong>), capture all regions surrounded by <code>&#39;X&#39;</code>.</p>

<p>A region is captured by flipping all <code>&#39;O&#39;</code>s into <code>&#39;X&#39;</code>s in that surrounded region.</p>

<p><strong>Example:</strong></p>

<pre>
X X X X
X O O X
X X O X
X O X X
</pre>

<p>After running your function, the board should be:</p>

<pre>
X X X X
X X X X
X X X X
X O X X
</pre>

<p><strong>Explanation:</strong></p>

<p>Surrounded regions shouldn&rsquo;t be on the border, which means that any <code>&#39;O&#39;</code>&nbsp;on the border of the board are not flipped to <code>&#39;X&#39;</code>. Any <code>&#39;O&#39;</code>&nbsp;that is not on the border and it is not connected to an <code>&#39;O&#39;</code>&nbsp;on the border will be flipped to <code>&#39;X&#39;</code>. Two cells are connected if they are adjacent cells connected horizontally or vertically.</p>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)
- Union Find (union-find)

## Companies
- Uber - 2 (taggedByAdmin: false)
- Google - 6 (taggedByAdmin: false)
- Amazon - 6 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Splunk - 2 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Overview

This problem is _almost_ identical as the [capture rule](https://en.wikipedia.org/wiki/Rules_of_Go#Capture) of the Go game, where one captures the opponent's stones by surrounding them. The difference is that in the Go game the borders of the board are considered to the walls that surround the stones, while in this problem a group of cells (_i.e._ region) is considered to be _**escaped**_ from the surrounding if it reaches any border.

![pic](../Figures/130/130_eg.png)

This problem is yet another problem concerning the _**traversal of 2D grid**_, _e.g._ [Robot room cleaner](https://leetcode.com/articles/robot-room-cleaner/).

>As similar to the traversal problems in a tree structure, there are generally two approaches in terms of solution: _**DFS**_ (Depth-First Search) and _**BFS**_ (Breadth-First Search).

One can apply either of the above strategies to traverse the 2D grid, while taking some specific actions to resolve the problems.

Given a traversal strategy (_DFS_ or _BFS_), there could be a thousand implementations for a thousand people, if we indulge ourselves to exaggerate a bit. However, there are some common neat _**techniques**_ that we could apply along with both of the strategies, in order to obtain a more optimized solution.
<br/>
<br/>

---
#### Approach 1: DFS (Depth-First Search)

**Intuition**

>The goal of this problem is to mark those _**captured**_ cells. 

If we are asked to summarize the algorithm in one sentence, it would be that we enumerate all those candidate cells (_i.e._ the ones filled with `O`), and check _one by one_ if they are _captured_ or not, _i.e._ we start with a candidate cell (`O`), and then apply either DFS or BFS strategy to explore its surrounding cells.

**Algorithm**

Let us start with the DFS algorithm, which usually results in a more concise code than the BFS algorithm. The algorithm consists of three steps:

- Step 1). We select all the cells that are located on the borders of the board.

- Step 2). Start from each of the above selected cell, we then perform the _DFS_ traversal.

    - If a cell on the border happens to be `O`, then we know that this cell is _alive_, together with the other `O` cells that are _connected_ to this border cell, based on the description of the problem. Two cells are _connected_, if there exists a path consisting of only `O` letter that bridges between the two cells.

    - Based on the above conclusion, the goal of our DFS traversal would be to _mark_ out all those _**connected**_ `O` cells that is originated from the border, with any distinguished letter such as `E`.

- Step 3). Once we iterate through all border cells, we would then obtain three types of cells:

    - The one with the `X` letter: the cell that we could consider as the wall.

    - The one with the `O` letter: the cells that are spared in our _DFS_ traversal, _i.e._ these cells has no connection to the border, therefore they are _**captured**_. We then should replace these cell with `X` letter.

    - The one with the `E` letter: these are the cells that are marked during our DFS traversal, _i.e._ these are the cells that has at least one connection to the borders, therefore they are not _captured_. As a result, we would revert the cell to its original letter `O`.
 
 We demonstrate how the DFS works with an example in the following animation. 

!?!../Documents/130_LIS.json:1000,521!?!

<iframe src="https://leetcode.com/playground/iCFkae9G/shared" frameBorder="0" width="100%" height="500" name="iCFkae9G"></iframe>

**Optimizations**

In the above implementation, there are a few techniques that we applied _**under the hood**_, in order to further optimize our solution. Here we list them one by one.

>Rather than iterating all candidate cells (the ones filled with `O`), we check only the ones on the **_borders_**.

In the above implementation, our starting points of _DFS_ are those cells that meet two conditions: 1). on the border. 2). filled with `O`.

_As an alternative solution, one might decide to iterate all `O` cells, which is less optimal compared to our starting points._

As one can see, during DFS traversal, the alternative solution would traverse the cells that eventually might be captured, which is not necessary in our approach.

>Rather than using a sort of `visited[cell_index]` map to keep track of the visited cells, we simply mark visited cell _**in place**_.

_This technique helps us gain both in the space and time complexity._

As an alternative approach, one could use a additional data structure to keep track of the visited cells, which goes without saying would require additional memory. And also it requires additional calculation for the comparison. Though one might argue that we could use the hash table data structure for the `visited[]` map, which has the $$\mathcal{O}(1)$$ asymptotic time complexity, but it is still more expensive than the simple comparison on the value of cell.

>Rather than doing the boundary check within the `DFS()` function, we do it _**before**_ the invocation of the function. 

As a comparison, here is the implementation where we do the boundary check within the `DFS()` function.

<iframe src="https://leetcode.com/playground/GTZUV2MF/shared" frameBorder="0" width="100%" height="208" name="GTZUV2MF"></iframe>

_This measure reduces the number of recursion, therefore it reduces the overheads with the function calls._

As trivial as this modification might seem to be, it actually reduces the runtime of the Python implementation from _148 ms_ to _124 ms_, _i.e._ _16%_ of reduction, which beats 97% of submissions instead of 77% at the moment.


**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the number of cells in the board. In the worst case where it contains only the `O` cells on the board, we would traverse each cell twice: once during the DFS traversal and the other time during the cell reversion in the last step.

- Space Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the number of cells in the board. There are mainly two places that we consume some additional memory.

    - We keep a list of border cells as starting points for our traversal. We could consider the number of border cells is proportional to the total number ($$N$$) of cells.

    - During the recursive calls of `DFS()` function, we would consume some space in the function call stack, _i.e._ the call stack will pile up along with the depth of recursive calls. And the maximum depth of recursive calls would be $$N$$ as in the worst scenario mentioned in the time complexity.

    - As a result, the overall space complexity of the algorithm is $$\mathcal{O}(N)$$.
<br/>
<br/>

---
#### Approach 2: BFS (Breadth-First Search)

**Intuition**

>In contrary to the DFS strategy, in BFS (Breadth-First Search) we prioritize the visit of a cell's neighbors before moving further (deeper) into the neighbor's neighbor. 

Though the order of visit might differ between DFS and BFS, eventually both strategies would visit the same set of cells, for most of the 2D grid traversal problems. This is also the case for this problem.

**Algorithm**

We could reuse the bulk of the DFS approach, while simply replacing the `DFS()` function with a `BFS()` function.
Here we just elaborate the implementation of the `BFS()` function.

- Essentially we can implement the BFS with the help of queue data structure, which could be of `Array` or more preferably `LinkedList` in Java or `Deque` in Python.

- Through the queue, we maintain the order of visit for the cells. Due to the **FIFO** (First-In First-Out) property of the queue, the one at the head of the queue would have the highest priority to be visited. 

- The main logic of the algorithm is a loop that iterates through the above-mentioned queue. At each iteration of the loop, we _pop out_ the **head** element from the queue.

    - If the popped element is of the candidate cell (_i.e._ `O`), we mark it as escaped, otherwise we skip this iteration.

    - For a candidate cell, we then simply append its neighbor cells into the queue, which would get their turns to be visited in the next iterations.

As comparison, we demonstrate how BFS works with the same example in DFS, in the following animation. 

!?!../Documents/130_SEC.json:1000,533!?!

<iframe src="https://leetcode.com/playground/Vuo9LUBt/shared" frameBorder="0" width="100%" height="500" name="Vuo9LUBt"></iframe>


**From BFS to DFS**

In the above implementation of BFS, the fun part is that we could easily convert the BFS strategy to DFS by changing one single line of code. And the obtained DFS implementation is done in iteration, instead of recursion.

>The key is that instead of using the _**queue**_ data structure which follows the principle of FIFO (First-In First-Out), if we use the _**stack**_ data structure which follows the principle of LIFO (Last-In First-Out), we then switch the strategy from BFS to DFS.

Specifically, at the moment we pop an element from the queue, instead of popping out the _head_ element, we pop the _tail_ element, which then changes the behavior of the container from queue to stack. Here is how it looks like.

<iframe src="https://leetcode.com/playground/RM9t4VNg/shared" frameBorder="0" width="100%" height="446" name="RM9t4VNg"></iframe>

Note that, though the above implementations indeed follow the DFS strategy, they are NOT equivalent to the previous _**recursive**_ version of DFS, _i.e._ they do not produce the exactly same sequence of visit.

In the recursive DFS, we would visit the _right-hand side_ neighbor `(row, col+1)` first, while in the iterative DFS, we would visit the _up_ neighbor `(row-1, col)` first.

In order to obtain the same order of visit as the recursive DFS, one should _reverse_ the processing order of neighbors in the above iterative DFS.

**Complexity**

- Time Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the number of cells in the board. In the worst case where it contains only the `O` cells on the board, we would traverse each cell twice: once during the BFS traversal and the other time during the cell reversion in the last step.

- Space Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the number of cells in the board. There are mainly two places that we consume some additional memory.

    - We keep a list of border cells as starting points for our traversal. We could consider the number of border cells is proportional to the total number ($$N$$) of cells.

    - Within each invocation of `BFS()` function, we use a queue data structure to hold the cells to be visited. We then need to estimate the upper bound on the size of the queue. _Intuitively we could imagine the unfold of BFS as the structure of an onion._
    Each layer of the onion represents the cells that has the same distance to the starting point. 
    Any given moment the queue would contain no more than two layers of _onion_, which in the worst case might cover _almost_ all cells in the board.

    - As a result, the overall space complexity of the algorithm is $$\mathcal{O}(N)$$.
<br/>
<br/>

## Accepted Submission (python3)
```python3
class Solution:
    def solve(self, board):
        """
        :type board: List[List[str]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        width = len(board)
        if width <= 0:
            return
        height = len(board[0])
        if height <= 0:
            return

        setOfKeptOs = {}
        for i in range(width):
            for j in range(height):
                letter = board[i][j]
                if letter == 'O':
                    if (i, j) not in setOfKeptOs:
                        currentOSet = {}
                        oLevel = {(i, j): True}
                        keepO = False
                        while oLevel:
                            for pos in oLevel:
                                currentOSet[pos] = True
                                if pos[0] == 0 or pos[0] == width - 1 or pos[1] == 0 or pos[1] == height - 1:
                                    keepO = True
                            newOLevel = {}
                            for pos in oLevel:
                                for newPos in [
                                    (pos[0] - 1, pos[1]),
                                    (pos[0] + 1, pos[1]),
                                    (pos[0], pos[1] - 1),
                                    (pos[0], pos[1] + 1)
                                ]:
                                    if newPos[0] >= 0 and newPos[0] < width \
                                        and newPos[1] >= 0 and newPos[1] < height \
                                        and newPos not in oLevel \
                                        and newPos not in currentOSet \
                                        and board[newPos[0]][newPos[1]] == 'O':
                                            newOLevel[newPos] = True
                            oLevel = newOLevel
                        if keepO:
                            setOfKeptOs.update(currentOSet)
                        else:
                            for pos in currentOSet:
                                board[pos[0]][pos[1]] = 'X'

```

## Top Discussions
### A really simple and readable C++ solution\uff0conly cost 12ms
- Author: sugeladi
- Creation Date: Sat Jun 27 2015 10:50:31 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 07:27:01 GMT+0800 (Singapore Standard Time)

<p>
 - First, check the four border of the matrix. If there is a element is
   'O', alter it and all its neighbor 'O' elements to '1'.
   
   
 - Then ,alter all the 'O' to 'X'
 - At last,alter all the '1' to 'O'

For example:

             X X X X           X X X X             X X X X
             X X O X  ->       X X O X    ->       X X X X
             X O X X           X 1 X X             X O X X
             X O X X           X 1 X X             X O X X
        

    class Solution {
    public:
    	void solve(vector<vector<char>>& board) {
            int i,j;
            int row=board.size();
            if(!row)
            	return;
            int col=board[0].size();

    		for(i=0;i<row;i++){
    			check(board,i,0,row,col);
    			if(col>1)
    				check(board,i,col-1,row,col);
    		}
    		for(j=1;j+1<col;j++){
    			check(board,0,j,row,col);
    			if(row>1)
    				check(board,row-1,j,row,col);
    		}
    		for(i=0;i<row;i++)
    			for(j=0;j<col;j++)
    				if(board[i][j]=='O')
    					board[i][j]='X';
    		for(i=0;i<row;i++)
    			for(j=0;j<col;j++)
    				if(board[i][j]=='1')
    					board[i][j]='O';
        }
    	void check(vector<vector<char> >&vec,int i,int j,int row,int col){
    		if(vec[i][j]=='O'){
    			vec[i][j]='1';
    			if(i>1)
    				check(vec,i-1,j,row,col);
    			if(j>1)
    				check(vec,i,j-1,row,col);
    			if(i+1<row)
    				check(vec,i+1,j,row,col);
    			if(j+1<col)
    				check(vec,i,j+1,row,col);
    		}
    	}
    };
</p>


### 9 lines, Python 148 ms
- Author: StefanPochmann
- Creation Date: Tue Jul 14 2015 19:40:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 15:22:20 GMT+0800 (Singapore Standard Time)

<p>
Phase 1: "Save" every O-region touching the border, changing its cells to 'S'.  
Phase 2: Change every 'S' on the board to 'O' and everything else to 'X'.

    def solve(self, board):
        if not any(board): return
    
        m, n = len(board), len(board[0])
        save = [ij for k in range(m+n) for ij in ((0, k), (m-1, k), (k, 0), (k, n-1))]
        while save:
            i, j = save.pop()
            if 0 <= i < m and 0 <= j < n and board[i][j] == 'O':
                board[i][j] = 'S'
                save += (i, j-1), (i, j+1), (i-1, j), (i+1, j)

        board[:] = [['XO'[c == 'S'] for c in row] for row in board]

In case you don't like my last line, you could do this instead:

        for row in board:
            for i, c in enumerate(row):
                row[i] = 'XO'[c == 'S']
</p>


### Solve it using Union Find
- Author: jinming.he.5
- Creation Date: Sun Jun 15 2014 17:29:41 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 16:06:05 GMT+0800 (Singapore Standard Time)

<p>
 

    class UF
    {
    private:
    	int* id;     // id[i] = parent of i
    	int* rank;  // rank[i] = rank of subtree rooted at i (cannot be more than 31)
    	int count;    // number of components
    public:
    	UF(int N)
    	{
    		count = N;
    		id = new int[N];
    		rank = new int[N];
    		for (int i = 0; i < N; i++) {
    			id[i] = i;
    			rank[i] = 0;
    		}
    	}
    	~UF()
    	{
    		delete [] id;
    		delete [] rank;
    	}
    	int find(int p) {
    		while (p != id[p]) {
    			id[p] = id[id[p]];    // path compression by halving
    			p = id[p];
    		}
    		return p;
    	}
    	int getCount() {
    		return count;
    	}
    	bool connected(int p, int q) {
    		return find(p) == find(q);
    	}
    	void connect(int p, int q) {
    		int i = find(p);
    		int j = find(q);
    		if (i == j) return;
    		if (rank[i] < rank[j]) id[i] = j;
    		else if (rank[i] > rank[j]) id[j] = i;
    		else {
    			id[j] = i;
    			rank[i]++;
    		}
    		count--;
    	}
    };
    
    class Solution {
    public:
        void solve(vector<vector<char>> &board) {
            int n = board.size();
            if(n==0)    return;
            int m = board[0].size();
            UF uf = UF(n*m+1);
            
            for(int i=0;i<n;i++){
                for(int j=0;j<m;j++){
                    if((i==0||i==n-1||j==0||j==m-1)&&board[i][j]=='O') // if a 'O' node is on the boundry, connect it to the dummy node
                        uf.connect(i*m+j,n*m);
                    else if(board[i][j]=='O') // connect a 'O' node to its neighbour 'O' nodes
                    {
                        if(board[i-1][j]=='O')
                            uf.connect(i*m+j,(i-1)*m+j);
                        if(board[i+1][j]=='O')
                            uf.connect(i*m+j,(i+1)*m+j);
                        if(board[i][j-1]=='O')
                            uf.connect(i*m+j,i*m+j-1);
                        if(board[i][j+1]=='O')
                            uf.connect(i*m+j,i*m+j+1);
                    }
                }
            }
            
            for(int i=0;i<n;i++){
                for(int j=0;j<m;j++){
                    if(!uf.connected(i*m+j,n*m)){ // if a 'O' node is not connected to the dummy node, it is captured
                        board[i][j]='X';
                    }
                }
            }
        }
    };

Hi. So here is my accepted code using **Union Find** data structure. The idea comes from the observation that if a region is NOT captured, it is connected to the boundry. So if we connect all the 'O' nodes on the boundry to a dummy node, and then connect each 'O' node to its neighbour 'O' nodes, then we can tell directly whether a 'O' node is captured by checking whether it is connected to the dummy node.
For more about Union Find, the first assignment in the algo1 may help:
https://www.coursera.org/course/algs4partI
</p>


