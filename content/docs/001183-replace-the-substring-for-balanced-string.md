---
title: "Replace the Substring for Balanced String"
weight: 1183
#id: "replace-the-substring-for-balanced-string"
---
## Description
<div class="description">
<p>You are given a string containing only 4&nbsp;kinds of characters <code>&#39;Q&#39;,</code> <code>&#39;W&#39;, &#39;E&#39;</code> and&nbsp;<code>&#39;R&#39;</code>.</p>

<p>A string is said to be&nbsp;<strong>balanced</strong><em>&nbsp;</em>if each of its characters appears&nbsp;<code>n/4</code> times where <code>n</code> is the length of the string.</p>

<p>Return the minimum length of the substring that can be replaced with <strong>any</strong> other string of the same length to make the original string <code>s</code>&nbsp;<strong>balanced</strong>.</p>

<p>Return 0 if the string is already <strong>balanced</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;QWER&quot;
<strong>Output:</strong> 0
<strong>Explanation: </strong>s is already balanced.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;QQWE&quot;
<strong>Output:</strong> 1
<strong>Explanation: </strong>We need to replace a &#39;Q&#39; to &#39;R&#39;, so that &quot;RQWE&quot; (or &quot;QRWE&quot;) is balanced.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;QQQW&quot;
<strong>Output:</strong> 2
<strong>Explanation: </strong>We can replace the first &quot;QQ&quot; to &quot;ER&quot;. 
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;QQQQ&quot;
<strong>Output:</strong> 3
<strong>Explanation: </strong>We can replace the last 3 &#39;Q&#39; to make s = &quot;QWER&quot;.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10^5</code></li>
	<li><code>s.length</code> is a multiple of <code>4</code></li>
	<li><code>s&nbsp;</code>contains only <code>&#39;Q&#39;</code>, <code>&#39;W&#39;</code>, <code>&#39;E&#39;</code> and&nbsp;<code>&#39;R&#39;</code>.</li>
</ul>

</div>

## Tags
- Two Pointers (two-pointers)
- String (string)

## Companies
- Accolite - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Sliding Window
- Author: lee215
- Creation Date: Sun Oct 20 2019 12:02:49 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Feb 25 2020 11:36:27 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
We want a minimum length of substring,
which leads us to the solution of sliding window.
Specilly this time we don\'t care the count of elements inside the window,
we want to know the count outside the window.
<br>

# **Explanation**
One pass the all frequency of `"QWER"`.
Then slide the windon in the string `s`.

Imagine that we erase all character inside the window,
as we can modyfy it whatever we want,
and it will always increase the count outside the window.

So we can make the whole string balanced,
as long as `max(count[Q],count[W],count[E],count[R]) <= n / 4`.
<br>

# **Important**
Does `i <= j + 1` makes more sense than `i <= n`.
Strongly don\'t think, and `i <= j + 1` makes no sense.

Answer the question first:
Why do we need such a condition in sliding window problem?

Actually, we never need this condition in sliding window solution
(Check all my other solutions link at the bottom).

Usually count the element inside sliding window,
and `i` won\'t be bigger than `j` because nothing left in the window.

The only reason that we need a condition is in order to prevent index out of range.
And how do we do that? Yes we use `i < n`

1. Does `i <= j + 1` even work?
2. When will `i` even reach `j + 1`?
3. Does `i <= j + 1` work better than `i <= j`?
<br>

Please **upvote** for this important tip.
Also let me know if there is any unclear, glad to hear different voices.
But please, have a try, and show the code if necessary.

Some people likes to criticize without even a try,
and solve the problem by talking.
Why talk to me? Talk to the white board.
<br>

# **Complexity**
Time `O(N)`, one pass for counting, one pass for sliding window
Space `O(1)`
<br>

**Java**
```java
    public int balancedString(String s) {
        int[] count = new int[128];
        int n = s.length(), res = n, i = 0, k = n / 4;
        for (int j = 0; j < n; ++j) {
            ++count[s.charAt(j)];
        }
        for (int j = 0; j < n; ++j) {
            --count[s.charAt(j)];
            while (i < n && count[\'Q\'] <= k && count[\'W\'] <= k && count[\'E\'] <= k && count[\'R\'] <= k) {
                res = Math.min(res, j - i + 1);
                ++count[s.charAt(i++)];
            }
        }
        return res;
    }
```
**C++**
```cpp
    int balancedString(string s) {
        unordered_map<int, int> count;
        int n = s.length(), res = n, i = 0, k = n / 4;
        for (int j = 0; j < n; ++j) {
            count[s[j]]++;
        }
        for (int j = 0; j < n; ++j) {
            count[s[j]]--;
            while (i < n && count[\'Q\'] <= k && count[\'W\'] <= k && count[\'E\'] <= k && count[\'R\'] <= k) {
                res = min(res, j - i + 1);
                count[s[i++]] += 1;
            }
        }
        return res;
    }
```
**Python:**
```python
    def balancedString(self, s):
        count = collections.Counter(s)
        res = n = len(s)
        i = 0
        for j, c in enumerate(s):
            count[c] -= 1
            while i < n and all(n / 4 >= count[c] for c in \'QWER\'):
                res = min(res, j - i + 1)
                count[s[i]] += 1
                i += 1
        return res
```
<br>

# More Similar Sliding Window Problems
Here are some similar sliding window problems.
Also find more explanations.
Good luck and have fun.

- 1248. [Count Number of Nice Subarrays](https://leetcode.com/problems/count-number-of-nice-subarrays/discuss/419378/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
- 1234. [Replace the Substring for Balanced String](https://leetcode.com/problems/replace-the-substring-for-balanced-string/discuss/408978/javacpython-sliding-window/367697)
- 1004. [Max Consecutive Ones III](https://leetcode.com/problems/max-consecutive-ones-iii/discuss/247564/javacpython-sliding-window/379427?page=3)
-  930. [Binary Subarrays With Sum](https://leetcode.com/problems/binary-subarrays-with-sum/discuss/186683/)
-  992. [Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/discuss/234482/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
-  904. [Fruit Into Baskets](https://leetcode.com/problems/fruit-into-baskets/discuss/170740/Sliding-Window-for-K-Elements)
-  862. [Shortest Subarray with Sum at Least K](https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/discuss/143726/C%2B%2BJavaPython-O(N)-Using-Deque)
-  209. [Minimum Size Subarray Sum](https://leetcode.com/problems/minimum-size-subarray-sum/discuss/433123)
<br>
</p>


### "WWEQERQWQWWRWWERQWEQ"  Why need return 4 ?? Anyone help?
- Author: bushiwo741
- Creation Date: Sun Oct 20 2019 12:15:56 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 21 2019 02:11:43 GMT+0800 (Singapore Standard Time)

<p>
"WWEQERQWQWWRWWERQWEQ"   <----- one of the test case.

It says that need 4 times replacing. But I think it only needs 3 times could make this string balanced.


![image](https://assets.leetcode.com/users/bushiwo741/image_1571595098.png)



Each of its characters appears n/4 times where n is the length of the string.   

I don\'t know what\'s wrong with my understanding, anyone can help me explain it\uFF1F






**===========================================================**

**========================== Update ===========================**

**===========================================================**

**Thank you guys\uFF01  I finally got it.**

![image](https://assets.leetcode.com/users/bushiwo741/image_1571545922.png)

</p>


### [JAVA] Sliding Window Solution with Explanation
- Author: kkzeng
- Creation Date: Sun Oct 20 2019 12:06:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 12:13:02 GMT+0800 (Singapore Standard Time)

<p>
**Explanation:**

The idea is to first count up each type of character. Since we know there are only 4 characters: Q, W, E, R, we can easily count them up using an int[] arr of length 4.

Then once we count them up, we look at the number of occurrences of each and see if any of them > N/4 (where N is the length of the String). If they are, this means that we need this freq[character] - (N/4) number of this character in the substring we choose to replace.

E.g. If we have N = 12 and freq[Q] = freq[0] = 6. Since we know each character must occur N/4 = 12/4 = 3 times. We have 3 extra Qs. So we need to make sure our substring at the end has 3 Qs in it. The same principle applies when there are multiple characters > (N/4).

Essentially, we reduced the problem to finding a minimum substring containing a certain number of each character.

Then we go to the freq array and subtract (N/4) from each of freq[Q], freq[W], freq[E], freq[R].
If it is below 0 (this means our substring does not need to contain this letter since we are already in demand of this letter), then we just set it to 0.

Then for our sliding window approach - see more: https://www.geeksforgeeks.org/window-sliding-technique/

We update freq[] so that freq[char] always represents how many characters we need still of each char to get the substring that we need. It is okay for freq[char] to be < 0 as this mean we have more characters than we need (which is fine). Each time we have an eligible substring, we update our minLen variable and try to shrink the window from the left as much as possible.

In the end we get the minimum length of a substring containing at least the characters we need to replace with other characters.

*Forgive me for messy code - contest code can get a bit less elegant.*

**Code:**
```
class Solution {
	// Checks that freq[char] <= 0 meaning we have an elligible substring
    private boolean fulfilled(int[] freq) {
        boolean fulfilled = true;
        for(int f: freq) {
            if(f > 0) fulfilled = false;
        }
        return fulfilled;
    }
    
	 // Q 0 W 1 E 2 R 3
    private int charToIdx(char c) {
        switch(c) {
            case \'Q\': return 0;
            case \'W\': return 1;
            case \'E\': return 2;
        }
        return 3;
    }
    
    public int balancedString(String s) {
        // 1) Find freq of each first
        int N = s.length();
        int required = N/4;
       
        int[] freq = new int[4];
        for(int i = 0; i < N; ++i) {
            char c = s.charAt(i);
            ++freq[charToIdx(c)];
        }
        
        // 2) Determine the ones we need to change
        boolean equal = true;
        for(int i = 0; i < 4; ++i) {
            if(freq[i] != required) equal = false;
            freq[i] = Math.max(0, freq[i] - required);
        }
        
        if(equal) return 0; // Early return if all are equal
        
        // 3) Use sliding window and try to track what more is needed to find smallest window
        int start = 0;
        int minLen = N; // Maximum will only ever be N
        
        for(int end = 0; end < N; ++end) {
            char c = s.charAt(end);
            --freq[charToIdx(c)];
            
            while(fulfilled(freq)) {
                minLen = Math.min(end - start + 1, minLen);

                char st = s.charAt(start);
                ++freq[charToIdx(st)];
                ++start;
            }
        }
        
        return minLen;
    }
}
```

Time complexity: O(N)
Space complexity: O(1)
</p>


