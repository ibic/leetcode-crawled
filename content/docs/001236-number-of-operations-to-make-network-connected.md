---
title: "Number of Operations to Make Network Connected"
weight: 1236
#id: "number-of-operations-to-make-network-connected"
---
## Description
<div class="description">
<p>There are&nbsp;<code>n</code>&nbsp;computers numbered from&nbsp;<code>0</code>&nbsp;to&nbsp;<code>n-1</code>&nbsp;connected by&nbsp;ethernet cables&nbsp;<code>connections</code>&nbsp;forming a network where&nbsp;<code>connections[i] = [a, b]</code>&nbsp;represents a connection between computers&nbsp;<code>a</code>&nbsp;and&nbsp;<code>b</code>. Any computer&nbsp;can reach any other computer directly or indirectly through the network.</p>

<p>Given an initial computer network <code>connections</code>. You can extract certain cables between two directly connected computers, and place them between any pair of disconnected computers to make them directly connected. Return the <em>minimum number of times</em> you need to do this in order to make all the computers connected. If it&#39;s not possible, return -1.&nbsp;</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/01/02/sample_1_1677.png" style="width: 570px; height: 167px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 4, connections = [[0,1],[0,2],[1,2]]
<strong>Output:</strong> 1
<strong>Explanation:</strong> Remove cable between computer 1 and 2 and place between computers 1 and 3.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/01/02/sample_2_1677.png" style="width: 660px; height: 167px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 6, connections = [[0,1],[0,2],[0,3],[1,2],[1,3]]
<strong>Output:</strong> 2
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 6, connections = [[0,1],[0,2],[0,3],[1,2]]
<strong>Output:</strong> -1
<strong>Explanation:</strong> There are not enough cables.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 5, connections = [[0,1],[0,2],[3,4],[2,3]]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
	<li><code>1 &lt;= connections.length &lt;= min(n*(n-1)/2, 10^5)</code></li>
	<li><code>connections[i].length == 2</code></li>
	<li><code>0 &lt;= connections[i][0], connections[i][1]&nbsp;&lt; n</code></li>
	<li><code>connections[i][0] != connections[i][1]</code></li>
	<li>There are no repeated connections.</li>
	<li>No two computers are connected by more than one cable.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)
- Union Find (union-find)

## Companies
- Akuna Capital - 2 (taggedByAdmin: true)
- Mathworks - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (python3)
```python3
class Solution:
    def mark(self, i: int, g: List[set], visited: List[bool]):
        if visited[i]:
            return 0
        visited[i] = True

        for j in g[i]:
            self.mark(j, g, visited)
        return 1

    def makeConnectedDfs(self, n: int, connections: List[List[int]]) -> int:
        lc = len(connections)
        if lc < n - 1:
            return -1
        g = [set() for i in range(n)]

        visited = [False] * n
        for i, j in connections:
            g[i].add(j)
            g[j].add(i)

        return sum(self.mark(i, g, visited) for i in range(n)) - 1

    def makeConnected(self, n: int, connections: List[List[int]]) -> int:
        def findParent(i):
            if i == parent[i]:
                return i
            else:
                return findParent(parent[i])

        parent = [i for i in range(n)]
        lc = len(connections)
        extraEdges = 0
        points = 0
        for i, j in connections:
            ip = findParent(i)
            jp = findParent(j)
            if ip == jp:
                extraEdges += 1
            else:
                parent[ip] = jp

        for i in range(n):
            if i == parent[i]:
                points += 1
        if extraEdges >= points - 1:
            return points - 1
        else:
            return -1

```

## Top Discussions
### [Python] Count the Connected Networks
- Author: lee215
- Creation Date: Sun Jan 12 2020 12:02:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 12 2020 14:09:32 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
We need at least `n - 1` cables to connect all nodes (like a tree).
If `connections.size() < n - 1`, we can directly return `-1`.

One trick is that, if we have enough cables,
we don\'t need to worry about where we can get the cable from.

We only need to count the number of connected networks.
To connect two unconneccted networks, we need to set one cable.

The number of operations we need = the number of **connected networks** - 1
<br>

## **Complexity**
Time `O(connections)`
Space `O(n)`
<br>

**Python:**
```python
    def makeConnected(self, n, connections):
        if len(connections) < n - 1: return -1
        G = [set() for i in xrange(n)]
        for i, j in connections:
            G[i].add(j)
            G[j].add(i)
        seen = [0] * n

        def dfs(i):
            if seen[i]: return 0
            seen[i] = 1
            for j in G[i]: dfs(j)
            return 1

        return sum(dfs(i) for i in xrange(n)) - 1
```

</p>


### [Java] Count number of connected components - Clean code
- Author: hiepit
- Creation Date: Sun Jan 12 2020 12:01:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Feb 22 2020 12:39:30 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1a: Naive Union-Find ~ 23ms**
```java
class Solution {
    public int makeConnected(int n, int[][] connections) {
        if (connections.length < n - 1) return -1; // To connect all nodes need at least n-1 edges
        int[] parent = new int[n];
        for (int i = 0; i < n; i++) parent[i] = i;
        int components = n;
        for (int[] c : connections) {
            int p1 = findParent(parent, c[0]);
            int p2 = findParent(parent, c[1]);
            if (p1 != p2) {
                parent[p1] = p2; // Union 2 component
                components--;
            }
        }
        return components - 1; // Need (components-1) cables to connect components together
    }
    private int findParent(int[] parent, int i) {
        while (i != parent[i]) i = parent[i];
        return i; // Without Path Compression
    }
}
```
Complexity:
- Time: `O(n*m)`, `m` is the length of connections. Can cause **TLE** with this testcase:  https://ideone.com/ZQHtLm
- Space: `O(n)`

**Solution 1b: Union-Find with Path Compression ~ 3ms**
```java
class Solution {
    public int makeConnected(int n, int[][] connections) {
        if (connections.length < n - 1) return -1; // To connect all nodes need at least n-1 edges
        int[] parent = new int[n];
        for (int i = 0; i < n; i++) parent[i] = i;
        int components = n;
        for (int[] c : connections) {
            int p1 = findParent(parent, c[0]);
            int p2 = findParent(parent, c[1]);
            if (p1 != p2) {
                parent[p1] = p2; // Union 2 component
                components--;
            }
        }
        return components - 1; // Need (components-1) cables to connect components together
    }
    private int findParent(int[] parent, int i) {
        if (i == parent[i]) return i;
        return parent[i] = findParent(parent, parent[i]); // Path compression
    }
}
```
Complexity:
- Time: `O(n+mlogn)`, `m` is the length of connections
- Space: `O(n)`

**Solution 1c: Union-Find with Path Compression and Union by Size ~ 3ms**
```java
class Solution {
    public int makeConnected(int n, int[][] connections) {
        if (connections.length < n - 1) return -1; // To connect all nodes need at least n-1 edges
        int[] parent = new int[n];
        int[] size = new int[n];
        for (int i = 0; i < n; i++) {
            parent[i] = i;
            size[i] = 1;
        }
        int components = n;
        for (int[] c : connections) {
            int p1 = findParent(parent, c[0]);
            int p2 = findParent(parent, c[1]);
            if (p1 != p2) {
                if (size[p1] < size[p2]) { // Merge small size to large size
                    size[p2] += size[p1];
                    parent[p1] = p2;
                } else {
                    size[p1] += size[p2];
                    parent[p2] = p1;
                }
                components--;
            }
        }
        return components - 1; // Need (components-1) cables to connect components together
    }
    private int findParent(int[] parent, int i) {
        if (i == parent[i]) return i;
        return parent[i] = findParent(parent, parent[i]); // Path compression
    }
}
```
Complexity:
- Time: `O(n + m*\u03B1(n))` \u2248 `O(n + m)`, `m` is the length of connections (union operations). 

Explanation: Using both **path compression** and **union by size** ensures that the **amortized time** per operation is only `\u03B1(n)`, which is optimal, where `\u03B1(n)` is the inverse Ackermann function. This function has a value `\u03B1(n) < 5` for any value of n that can be written in this physical universe, so the disjoint-set operations take place in essentially constant time.
Reference: https://en.wikipedia.org/wiki/Disjoint-set_data_structure or https://www.slideshare.net/WeiLi73/time-complexity-of-union-find-55858534 for more information.

- Space: `O(n)`

**Solution 2: DFS ~ 11ms**
```java
class Solution {
    public int makeConnected(int n, int[][] connections) {
        if (connections.length < n - 1) return -1; // To connect all nodes need at least n-1 edges
        List<Integer>[] graph = new List[n];
        for (int i = 0; i < n; i++) graph[i] = new ArrayList<>();
        for (int[] c : connections) {
            graph[c[0]].add(c[1]);
            graph[c[1]].add(c[0]);
        }
        int components = 0;
        boolean[] visited = new boolean[n];
        for (int v = 0; v < n; v++) components += dfs(v, graph, visited);
        return components - 1; // Need (components-1) cables to connect components together
    }
    int dfs(int u, List<Integer>[] graph, boolean[] visited) {
        if (visited[u]) return 0;
        visited[u] = true;
        for (int v : graph[u]) dfs(v, graph, visited);
        return 1;
    }
}
```
Complexity:
- Time: `O(n+m)`, `m` is the length of connections
- Space: `O(n)`

**Solution 3: BFS ~ 12ms**
```java
class Solution {
    public int makeConnected(int n, int[][] connections) {
        if (connections.length < n - 1) return -1; // To connect all nodes need at least n-1 edges
        List<Integer>[] graph = new List[n];
        for (int i = 0; i < n; i++) graph[i] = new ArrayList<>();
        for (int[] c : connections) {
            graph[c[0]].add(c[1]);
            graph[c[1]].add(c[0]);
        }
        int components = 0;
        boolean[] visited = new boolean[n];
        for (int v = 0; v < n; v++) components += bfs(v, graph, visited);
        return components - 1; // Need (components-1) cables to connect components together
    }
    int bfs(int src, List<Integer>[] graph, boolean[] visited) {
        if (visited[src]) return 0;
        visited[src] = true;
        Queue<Integer> q = new LinkedList<>();
        q.offer(src);
        while (!q.isEmpty()) {
            int u = q.poll();
            for (int v : graph[u]) {
                if (!visited[v]) {
                    visited[v] = true;
                    q.offer(v);
                }
            }
        }
        return 1;
    }
}
```
Complexity:
- Time: `O(n+m)`, `m` is the length of connections
- Space: `O(n)`
</p>


### [Java] Union Find (count components and extra edges)
- Author: manrajsingh007
- Creation Date: Sun Jan 12 2020 12:02:12 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 13 2020 13:44:17 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public static int findParent(int[] par, int i) {
        if(par[i] == i) return i;
        return par[i] = findParent(par, par[i]);
    }
    public int makeConnected(int n, int[][] connections) {
        int[] parent = new int[n];
        for(int i = 0; i < n; i++) parent[i] = i;
        int m = connections.length;
        int components = 0;
        int extraEdge = 0;
        for(int i = 0; i < m; i++) {
            int p1 = findParent(parent, connections[i][0]);
            int p2 = findParent(parent, connections[i][1]);
            if(p1 == p2) extraEdge++;
            else parent[p1] = p2;
        }
        for(int i = 0; i < n; i++) if(parent[i] == i) components++;
        return (extraEdge >= components - 1) ? components - 1 : -1;
    }
}
</p>


