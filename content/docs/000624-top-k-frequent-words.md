---
title: "Top K Frequent Words"
weight: 624
#id: "top-k-frequent-words"
---
## Description
<div class="description">
<p>Given a non-empty list of words, return the <i>k</i> most frequent elements.</p>
<p>Your answer should be sorted by frequency from highest to lowest. If two words have the same frequency, then the word with the lower alphabetical order comes first.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> ["i", "love", "leetcode", "i", "love", "coding"], k = 2
<b>Output:</b> ["i", "love"]
<b>Explanation:</b> "i" and "love" are the two most frequent words.
    Note that "i" comes before "love" due to a lower alphabetical order.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
<b>Output:</b> ["the", "is", "sunny", "day"]
<b>Explanation:</b> "the", "is", "sunny" and "day" are the four most frequent words,
    with the number of occurrence being 4, 3, 2 and 1 respectively.
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>You may assume <i>k</i> is always valid, 1 &le; <i>k</i> &le; number of unique elements.</li>
<li>Input words contain only lowercase letters.</li>
</ol>
</p>

<p><b>Follow up:</b><br />
<ol>
<li>Try to solve it in <i>O</i>(<i>n</i> log <i>k</i>) time and <i>O</i>(<i>n</i>) extra space.</li>
</ol>
</p>
</div>

## Tags
- Hash Table (hash-table)
- Heap (heap)
- Trie (trie)

## Companies
- Amazon - 113 (taggedByAdmin: true)
- Facebook - 6 (taggedByAdmin: false)
- Bloomberg - 4 (taggedByAdmin: true)
- Google - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Yelp - 4 (taggedByAdmin: true)
- Microsoft - 3 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: true)
- Apple - 3 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- TripAdvisor - 5 (taggedByAdmin: false)
- Expedia - 4 (taggedByAdmin: false)
- Salesforce - 3 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Pocket Gems - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


#### Approach #1: Sorting [Accepted]

**Intuition and Algorithm**

Count the frequency of each word, and sort the words with a custom ordering relation that uses these frequencies.  Then take the best `k` of them.

**Python**
```python
class Solution(object):
    def topKFrequent(self, words, k):
        count = collections.Counter(words)
        candidates = count.keys()
        candidates.sort(key = lambda w: (-count[w], w))
        return candidates[:k]
```

**Java**
```java
class Solution {
    public List<String> topKFrequent(String[] words, int k) {
        Map<String, Integer> count = new HashMap();
        for (String word: words) {
            count.put(word, count.getOrDefault(word, 0) + 1);
        }
        List<String> candidates = new ArrayList(count.keySet());
        Collections.sort(candidates, (w1, w2) -> count.get(w1).equals(count.get(w2)) ?
                w1.compareTo(w2) : count.get(w2) - count.get(w1));

        return candidates.subList(0, k);
```

**Complexity Analysis**

* Time Complexity: $$O(N \log{N})$$, where $$N$$ is the length of `words`.  We count the frequency of each word in $$O(N)$$ time, then we sort the given words in $$O(N \log{N})$$ time.

* Space Complexity: $$O(N)$$, the space used to store our `candidates`.

---

#### Approach #2: Heap [Accepted]

**Intuition and Algorithm**

Count the frequency of each word, then add it to heap that stores the best `k` candidates.  Here, "best" is defined with our custom ordering relation, which puts the worst candidates at the top of the heap.  At the end, we pop off the heap up to `k` times and reverse the result so that the best candidates are first.

In Python, we instead use `heapq.heapify`, which can turn a list into a heap in linear time, simplifying our work.

**Java**
```java
class Solution {
    public List<String> topKFrequent(String[] words, int k) {
        Map<String, Integer> count = new HashMap();
        for (String word: words) {
            count.put(word, count.getOrDefault(word, 0) + 1);
        }
        PriorityQueue<String> heap = new PriorityQueue<String>(
                (w1, w2) -> count.get(w1).equals(count.get(w2)) ?
                w2.compareTo(w1) : count.get(w1) - count.get(w2) );

        for (String word: count.keySet()) {
            heap.offer(word);
            if (heap.size() > k) heap.poll();
        }

        List<String> ans = new ArrayList();
        while (!heap.isEmpty()) ans.add(heap.poll());
        Collections.reverse(ans);
        return ans;
    }
}
```

```python
class Solution(object):
    def topKFrequent(self, words, k):
        count = collections.Counter(words)
        heap = [(-freq, word) for word, freq in count.items()]
        heapq.heapify(heap)
        return [heapq.heappop(heap)[1] for _ in xrange(k)]
```

**Complexity Analysis**

* Time Complexity: $$O(N \log{k})$$, where $$N$$ is the length of `words`.  We count the frequency of each word in $$O(N)$$ time, then we add $$N$$ words to the heap, each in $$O(\log {k})$$ time.  Finally, we pop from the heap up to $$k$$ times.  As $$k \leq N$$, this is $$O(N \log{k})$$ in total.

  In Python, we improve this to $$O(N + k \log {N})$$: our `heapq.heapify` operation and counting operations are $$O(N)$$, and each of $$k$$ `heapq.heappop` operations are $$O(\log {N})$$.

* Space Complexity: $$O(N)$$, the space used to store our `count`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My simple Java solution using HashMap & PriorityQueue - O(nlogk) time & O(n) space
- Author: sequeira
- Creation Date: Fri Oct 20 2017 10:35:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 01:05:54 GMT+0800 (Singapore Standard Time)

<p>
The idea is to keep a count of each word in a HashMap and then insert in a Priority Queue.
While inserting in pq, if the count of two words is same then insert based on string compare of the keys.
```
class Solution {
    public List<String> topKFrequent(String[] words, int k) {
        
        List<String> result = new LinkedList<>();
        Map<String, Integer> map = new HashMap<>();
        for(int i=0; i<words.length; i++)
        {
            if(map.containsKey(words[i]))
                map.put(words[i], map.get(words[i])+1);
            else
                map.put(words[i], 1);
        }
        
        PriorityQueue<Map.Entry<String, Integer>> pq = new PriorityQueue<>(
                 (a,b) -> a.getValue()==b.getValue() ? b.getKey().compareTo(a.getKey()) : a.getValue()-b.getValue()
        );
        
        for(Map.Entry<String, Integer> entry: map.entrySet())
        {
            pq.offer(entry);
            if(pq.size()>k)
                pq.poll();
        }

        while(!pq.isEmpty())
            result.add(0, pq.poll().getKey());
        
        return result;
    }
}
```
</p>


### Python 3 solution with O(nlogk) and O(n)
- Author: cixuuz
- Creation Date: Mon Oct 16 2017 03:05:28 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 16 2017 03:05:28 GMT+0800 (Singapore Standard Time)

<p>
```
import collections
import heapq
import functools

@functools.total_ordering
class Element:
    def __init__(self, count, word):
        self.count = count
        self.word = word
        
    def __lt__(self, other):
        if self.count == other.count:
            return self.word > other.word
        return self.count < other.count
    
    def __eq__(self, other):
        return self.count == other.count and self.word == other.word

class Solution(object):
    def topKFrequent(self, words, k):
        """
        :type words: List[str]
        :type k: int
        :rtype: List[str]
        """
        counts = collections.Counter(words)   
        
        freqs = []
        heapq.heapify(freqs)
        for word, count in counts.items():
            heapq.heappush(freqs, (Element(count, word), word))
            if len(freqs) > k:
                heapq.heappop(freqs)
        
        res = []
        for _ in range(k):
            res.append(heapq.heappop(freqs)[1])
        return res[::-1]
    
```
</p>


### Java O(n) solution using HashMap, BucketSort and Trie - 22ms Beat 81%
- Author: hackerhuang
- Creation Date: Sun Oct 15 2017 05:17:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 01:27:41 GMT+0800 (Singapore Standard Time)

<p>
This problem is quite similar to the problem [Top K Frequent Elements](https://leetcode.com/problems/top-k-frequent-elements/description/). You can refer to [this post](https://discuss.leetcode.com/topic/44237/java-o-n-solution-bucket-sort) for the solution of the problem.

We can solve this problem with the similar idea:
Firstly, we need to calculate the frequency of each word and store the result in a hashmap.

Secondly, we will use bucket sort to store words. Why? Because the minimum frequency is greater than or equal to 1 and the maximum frequency is less than or equal to the length of the input string array. 

Thirdly, we can define a trie within each bucket to store all the words with the same frequency. With Trie, it ensures that the lower alphabetical word will be met first, saving the trouble to sort the words within the bucket. 

From the above analysis, we can see the time complexity is O(n).
Here is my code:
```
public List<String> topKFrequent(String[] words, int k) {
        // calculate frequency of each word
        Map<String, Integer> freqMap = new HashMap<>();
        for(String word : words) {
            freqMap.put(word, freqMap.getOrDefault(word, 0) + 1);
        }
        // build the buckets
        TrieNode[] count = new TrieNode[words.length + 1];
        for(String word : freqMap.keySet()) {
            int freq = freqMap.get(word);
            if(count[freq] == null) {
                count[freq] = new TrieNode();
            }
            addWord(count[freq], word);
        }
        // get k frequent words
        List<String> list = new LinkedList<>();
        for(int f = count.length - 1; f >= 1 && list.size() < k; f--) {
            if(count[f] == null) continue;
            getWords(count[f], list, k);
        }
        return list;
    }
    
    private void getWords(TrieNode node, List<String> list, int k) {
        if(node == null) return;
        if(node.word != null) {
            list.add(node.word);
        }
        if(list.size() == k) return;
        for(int i = 0; i < 26; i++) {
            if(node.next[i] != null) {
                getWords(node.next[i], list, k);
            }
        }
    }
    
    private boolean addWord(TrieNode root, String word) {
        TrieNode curr = root;
        for(char c : word.toCharArray()) {
            if(curr.next[c - 'a'] == null) {
                curr.next[c - 'a'] = new TrieNode();
            }
            curr = curr.next[c - 'a'];
        }
        curr.word = word;
        return true;
    }
    
    class TrieNode {
        TrieNode[] next;
        String word;
        TrieNode() {
            this.next = new TrieNode[26];
            this.word = null;
        }
    }
```
</p>


