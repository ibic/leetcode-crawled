---
title: "Moving Stones Until Consecutive II"
weight: 1020
#id: "moving-stones-until-consecutive-ii"
---
## Description
<div class="description">
<p>On an <strong>infinite</strong> number line, the position of the i-th stone is given by&nbsp;<code>stones[i]</code>.&nbsp; Call a stone an <em>endpoint stone</em> if it has the smallest or largest position.</p>

<p>Each turn, you pick up an endpoint stone and move it to an unoccupied position so that it is no longer an endpoint stone.</p>

<p>In particular,&nbsp;if the stones are at say, <code>stones = [1,2,5]</code>, you <strong>cannot</strong> move the endpoint stone at position 5, since moving it to any position (such as 0, or 3) will still keep that stone as an endpoint stone.</p>

<p>The game ends when you cannot make any more moves, ie. the stones are in consecutive positions.</p>

<p>When the game ends, what is the minimum and maximum number of moves that you could have made?&nbsp; Return the answer as an length 2 array:&nbsp;<code>answer = [minimum_moves, maximum_moves]</code></p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[7,4,9]</span>
<strong>Output: </strong><span id="example-output-1">[1,2]</span>
<strong>Explanation: </strong>
We can move 4 -&gt; 8 for one move to finish the game.
Or, we can move 9 -&gt; 5, 4 -&gt; 6 for two moves to finish the game.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[6,5,4,3,10]</span>
<strong>Output: </strong><span id="example-output-2">[2,3]</span>
We can move 3 -&gt; 8 then 10 -&gt; 7 to finish the game.
Or, we can move 3 -&gt; 7, 4 -&gt; 8, 5 -&gt; 9 to finish the game.
Notice we cannot move 10 -&gt; 2 to finish the game, because that would be an illegal move.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[100,101,104,102,103]</span>
<strong>Output: </strong><span id="example-output-3">[0,0]</span></pre>

<p>&nbsp;</p>
</div>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>3 &lt;= stones.length &lt;= 10^4</code></li>
	<li><code>1 &lt;= stones[i] &lt;= 10^9</code></li>
	<li><code>stones[i]</code> have distinct values.</li>
</ol>

<div>
<div>
<div>&nbsp;</div>
</div>
</div>

</div>

## Tags
- Array (array)
- Sliding Window (sliding-window)

## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Sliding Window
- Author: lee215
- Creation Date: Sun May 05 2019 12:11:08 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Mar 12 2020 23:17:15 GMT+0800 (Singapore Standard Time)

<p>
## Lower Bound
As I mentioned in my video last week,
in case of `n` stones, 
we need to find a consecutive `n` positions and move the stones in.

This idea led the solution with sliding windows.

Slide a window of size `N`, and find how many stones are already in this window.
We want moves other stones into this window.
For each missing stone, we need at least one move.

Generally, the number of missing stones and the moves we need are the same.
Only one corner case in this problem, we need to move the endpoint to no endpoint.


For case `1,2,4,5,10`,
1 move  needed from `10` to `3`.

For case `1,2,3,4,10`,
2 move needed from `1` to `6`, then from `10` to `5`.
<br>

## Upper Bound
We try to move all stones to leftmost or rightmost.
For example of to rightmost.
We move the `A[0]` to `A[1] + 1`.
Then each time, we pick the stone of left endpoint, move it to the next empty position.
During this process, the position of leftmost stones increment 1 by 1 each time.
Until the leftmost is at `A[n - 1] - n + 1`.
<br>

## Complexity
Time of quick sorting `O(NlogN)`
Time of sliding window `O(N)`
Space `O(1)`
<br>

**Java:**
```
    public int[] numMovesStonesII(int[] A) {
        Arrays.sort(A);
        int i = 0, n = A.length, low = n;
        int high = Math.max(A[n - 1] - n + 2 - A[1], A[n - 2] - A[0] - n + 2);
        for (int j = 0; j < n; ++j) {
            while (A[j] - A[i] >= n) ++i;
            if (j - i + 1 == n - 1 && A[j] - A[i] == n - 2)
                low = Math.min(low, 2);
            else
                low = Math.min(low, n - (j - i + 1));
        }
        return new int[] {low, high};
    }
```

**C++:**
```
    vector<int> numMovesStonesII(vector<int>& A) {
        sort(A.begin(), A.end());
        int i = 0, n = A.size(), low = n;
        int high = max(A[n - 1] - n + 2 - A[1], A[n - 2] - A[0] - n + 2);
        for (int j = 0; j < n; ++j) {
            while (A[j] - A[i] >= n) ++i;
            if (j - i + 1 == n - 1 && A[j] - A[i] == n - 2)
                low = min(low, 2);
            else
                low = min(low, n - (j - i + 1));
        }
        return {low, high};
    }
```

**Python:**
```
    def numMovesStonesII(self, A):
        A.sort()
        i, n, low = 0, len(A), len(A)
        high = max(A[-1] - n + 2 - A[1], A[-2] - A[0] - n + 2)
        for j in range(n):
            while A[j] - A[i] >= n: i += 1
            if j - i + 1 == n - 1 and A[j] - A[i] == n - 2:
                low = min(low, 2)
            else:
                low = min(low, n - (j - i + 1))
        return [low, high]
```

</p>


### c++ with picture
- Author: xiongqiangcs
- Creation Date: Thu May 09 2019 18:29:20 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 10 2019 16:07:57 GMT+0800 (Singapore Standard Time)

<p>
## the max move

**intuition we need the max move** `A[n-1]-A[0]-n+1`

use the case `1 2 99 100`

![image](https://assets.leetcode.com/users/xiongqiangcs/image_1557397438.png)


**but the endpoint stone exist an hole**,  if you move the endpoint stone firstly, the hole will disappear

use the case `1 100 102 105 200`

![image](https://assets.leetcode.com/users/xiongqiangcs/image_1557396468.png)


* move the leftmost endpoint stone firstly, the left hole disappear, and the  move is `A[n-1]-A[1]-n+2`
![image](https://assets.leetcode.com/users/xiongqiangcs/image_1557397242.png)

* samely\uFF0Cmove the rightmost endpoint stone firstly,  the right hole disappear, and the max move is `A[n-2]-A[0]-n+2`
![image](https://assets.leetcode.com/users/xiongqiangcs/image_1557397698.png)

* last, the max move is `min(A[n-1]-A[1]-n+2, A[n-2]-A[0]-n+2)`

### the min move

![image](https://assets.leetcode.com/users/xiongqiangcs/image_1557475637.png)


```
vector<int> numMovesStonesII(vector<int>& A) {
	sort(A.begin(), A.end());
	int n = A.size(), low = n;
	for (int i = 0, j = 0; j < n; ++ j) {
		while (A[j]-A[i]+1 > n) ++ i;
		int already_store = (j-i+1);
		if (already_store == n - 1 &&  A[j] - A[i] + 1 == n-1) {
			low = min(low, 2);
		} else {
			low = min(low, n - already_store);
		}
	}
	return  {low, max(A[n-1]-A[1]-n+2, A[n-2]-A[0]-n+2)};
}
```
</p>


### Poor description.
- Author: NeroP
- Creation Date: Tue May 07 2019 20:38:37 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 07 2019 20:38:37 GMT+0800 (Singapore Standard Time)

<p>
Please, explain in detail. It is difficult to understand.
</p>


