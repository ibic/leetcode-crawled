---
title: "Vowel Spellchecker"
weight: 916
#id: "vowel-spellchecker"
---
## Description
<div class="description">
<p>Given a&nbsp;<code>wordlist</code>, we want to implement a spellchecker that converts a query word into a correct word.</p>

<p>For a given <code>query</code> word, the spell checker handles two categories of spelling mistakes:</p>

<ul>
	<li>Capitalization: If the query matches a word in the wordlist (<strong>case-insensitive</strong>), then the query word is returned with the same case as the case in the wordlist.

	<ul>
		<li>Example: <code>wordlist = [&quot;yellow&quot;]</code>, <code>query = &quot;YellOw&quot;</code>: <code>correct = &quot;yellow&quot;</code></li>
		<li>Example: <code>wordlist = [&quot;Yellow&quot;]</code>, <code>query = &quot;yellow&quot;</code>: <code>correct = &quot;Yellow&quot;</code></li>
		<li>Example: <code>wordlist = [&quot;yellow&quot;]</code>, <code>query = &quot;yellow&quot;</code>: <code>correct = &quot;yellow&quot;</code></li>
	</ul>
	</li>
	<li>Vowel Errors: If after replacing the vowels (&#39;a&#39;, &#39;e&#39;, &#39;i&#39;, &#39;o&#39;, &#39;u&#39;) of the query word with any vowel individually, it matches a word in the wordlist (<strong>case-insensitive</strong>), then the query word is returned with the same case as the match in the wordlist.
	<ul>
		<li>Example: <code>wordlist = [&quot;YellOw&quot;]</code>, <code>query = &quot;yollow&quot;</code>: <code>correct = &quot;YellOw&quot;</code></li>
		<li>Example: <code>wordlist = [&quot;YellOw&quot;]</code>, <code>query = &quot;yeellow&quot;</code>: <code>correct = &quot;&quot;</code> (no match)</li>
		<li>Example: <code>wordlist = [&quot;YellOw&quot;]</code>, <code>query = &quot;yllw&quot;</code>: <code>correct = &quot;&quot;</code> (no match)</li>
	</ul>
	</li>
</ul>

<p>In addition, the spell checker operates under the following precedence rules:</p>

<ul>
	<li>When the query exactly matches a word in the wordlist (<strong>case-sensitive</strong>), you should return the same word back.</li>
	<li>When the query matches a word up to capitlization, you should return the first such match in the wordlist.</li>
	<li>When the query matches a word up to vowel errors, you should return the first such match in the wordlist.</li>
	<li>If the query has no matches in the wordlist, you should return the empty string.</li>
</ul>

<p>Given some <code>queries</code>, return a&nbsp;list of words <code>answer</code>, where <code>answer[i]</code>&nbsp;is&nbsp;the correct word for <code>query = queries[i]</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>wordlist = <span id="example-input-1-1">[&quot;KiTe&quot;,&quot;kite&quot;,&quot;hare&quot;,&quot;Hare&quot;]</span>, queries = <span id="example-input-1-2">[&quot;kite&quot;,&quot;Kite&quot;,&quot;KiTe&quot;,&quot;Hare&quot;,&quot;HARE&quot;,&quot;Hear&quot;,&quot;hear&quot;,&quot;keti&quot;,&quot;keet&quot;,&quot;keto&quot;]</span>
<strong>Output: </strong><span id="example-output-1">[&quot;kite&quot;,&quot;KiTe&quot;,&quot;KiTe&quot;,&quot;Hare&quot;,&quot;hare&quot;,&quot;&quot;,&quot;&quot;,&quot;KiTe&quot;,&quot;&quot;,&quot;KiTe&quot;]</span></pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>1 &lt;= wordlist.length &lt;= 5000</code></li>
	<li><code>1 &lt;= queries.length &lt;= 5000</code></li>
	<li><code>1 &lt;= wordlist[i].length &lt;= 7</code></li>
	<li><code>1 &lt;= queries[i].length &lt;= 7</code></li>
	<li>All strings in <code>wordlist</code> and <code>queries</code> consist only of <strong>english</strong>&nbsp;letters.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- String (string)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Thumbtack - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: HashMap

**Intuition and Algorithm**

We analyze the 3 cases that the algorithm needs to consider: when the query is an exact match, when the query is a match up to capitalization, and when the query is a match up to vowel errors.

In all 3 cases, we can use a hash table to query the answer.

* For the first case (exact match), we hold a set of words to efficiently test whether our query is in the set.
* For the second case (capitalization), we hold a hash table that converts the word from its lowercase version to the original word (with correct capitalization).
* For the third case (vowel replacement), we hold a hash table that converts the word from its lowercase version with the vowels masked out, to the original word.

The rest of the algorithm is careful planning and reading the problem carefully.

<iframe src="https://leetcode.com/playground/GscxVJxY/shared" frameBorder="0" width="100%" height="500" name="GscxVJxY"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\mathcal{C})$$, where $$\mathcal{C}$$ is the total *content* of `wordlist` and `queries`.

* Space Complexity:  $$O(\mathcal{C})$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Two HashMap
- Author: lee215
- Creation Date: Sun Dec 30 2018 12:02:54 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 26 2019 00:25:37 GMT+0800 (Singapore Standard Time)

<p>
For each word in the wordlist,
get its the lower pattern and devowel pattern,

For each lower pattern, record the first such match to hashmap `cap`.
For each vowel pattern, record the first such match to hashmap `vowel`.

For each query,
check if it\'s in the `words` set,
check if there is a match in `cap`,
check if there is a match in `vowel`,
otherwise return `""`.


**Java:**
```
    public String[] spellchecker(String[] wordlist, String[] queries) {
        Set<String> words = new HashSet<>(Arrays.asList(wordlist));
        HashMap<String, String> cap = new HashMap<>();
        HashMap<String, String> vowel = new HashMap<>();
        for (String w : wordlist) {
            String lower = w.toLowerCase(), devowel = lower.replaceAll("[aeiou]", "#");
            cap.putIfAbsent(lower, w);
            vowel.putIfAbsent(devowel, w);
        }
        for (int i = 0; i < queries.length; ++i) {
            if (words.contains(queries[i])) continue;
            String lower = queries[i].toLowerCase(), devowel = lower.replaceAll("[aeiou]", "#");
            if (cap.containsKey(lower)) {
                queries[i] = cap.get(lower);
            } else if (vowel.containsKey(devowel)) {
                queries[i] = vowel.get(devowel);
            } else {
                queries[i] = "";
            }
        }
        return queries;
    }
```

**C++:**
```
    vector<string> spellchecker(vector<string>& wordlist, vector<string> queries) {
        unordered_set<string> words(wordlist.begin(), wordlist.end());
        unordered_map<string, string> cap, vowel;
        for (string w : wordlist) {
            string lower = tolow(w), devowel = todev(w);
            cap.insert({lower, w});
            vowel.insert({devowel, w});
        }
        for (int i = 0; i < queries.size(); ++i) {
            if (words.count(queries[i])) continue;
            string lower = tolow(queries[i]), devowel = todev(queries[i]);
            if (cap.count(lower)) {
                queries[i] = cap[lower];
            } else if (vowel.count(devowel)) {
                queries[i] = vowel[devowel];
            } else {
                queries[i] = "";
            }
        }
        return queries;
    }

    string tolow(string w) {
        for (auto & c: w)
            c = tolower(c);
        return w;
    }

    string todev(string w) {
        w = tolow(w);
        for (auto & c: w)
            if (c == \'a\' || c == \'e\' || c == \'i\' || c == \'o\' || c == \'u\')
                c = \'#\';
        return w;
    }
```

**Python:**
```
    def spellchecker(self, wordlist, queries):
        words = {w: w for w in wordlist}
        cap = {w.lower(): w for w in wordlist[::-1]}
        vowel = {re.sub("[aeiou]", \'#\', w.lower()): w for w in wordlist[::-1]}
        return [words.get(w) or cap.get(w.lower()) or vowel.get(re.sub("[aeiou]", \'#\', w.lower()), "") for w in queries]
```

</p>


### C++ Hash Map ("Yellow", "_yellow", "y*ll*w")
- Author: votrubac
- Creation Date: Sun Dec 30 2018 12:21:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 30 2018 12:21:36 GMT+0800 (Singapore Standard Time)

<p>
Insert into a single hash map:
1. The original string capitalization ("Yellow").
2. The lowered case string with a prefix (e.g. ```\'_\'```) for lover-case matching ("_yellow").
3. The lowered case string with all vowels replaced (e.g. ```\'*\'```) for vowel errors matching ("y*ll*w").
Note that ```unordered_map::insert``` only inserts the key for the first time.

For our queries, transform it the same way as above, and search in our hash map.
```
string lowerKey(string &s) {
  return accumulate(begin(s), end(s), string("_"), [](string k, char c) { return k + (char)tolower(c); });
}
string vowelKey(string &s) {
  return accumulate(begin(s), end(s), string(""), [](string k, char c) { return k +
    (char)(string("aeiou").find(tolower(c)) != string::npos ? \'*\' : tolower(c)); });
}
vector<string> spellchecker(vector<string>& wordlist, vector<string>& queries) {
  unordered_map<string, string> words;
  for (auto w : wordlist) {
    words.insert({ w, w }), words.insert({ lowerKey(w), w }), words.insert({ vowelKey(w), w });
  }
  vector<string> res;
  for (auto q : queries) {
    auto it = words.find(q);
    if (it == words.end()) it = words.find(lowerKey(q));
    if (it == words.end()) it = words.find(vowelKey(q));
    if (it != words.end()) res.push_back(it->second);
    else res.push_back("");
  }
  return res;
}
```
</p>


### Very Easy Python Solution | Beats 97% Time, 100% Space
- Author: ghost_system
- Creation Date: Tue Sep 24 2019 07:42:21 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 24 2019 07:42:21 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def spellchecker(self, wordlist: List[str], queries: List[str]) -> List[str]:
        
        if len(queries) == 0:
            return []
        
		# Set for Perfect Word
        wordPerfect = set(wordlist)
		# Dictionary to hold Capital Word Form
        wordUpper = {}
		# Dictionary to hold Vowel Chnaged Form
        wordVowel = {}
        
		# Setting up the Dictionaries and saving only the first correct occurance of the word in wordList
        for word in wordlist:
            tempLowerWord = word.lower()
            if tempLowerWord not in wordUpper:
                wordUpper[tempLowerWord] = word
			# Converting to Vowel Transformed Word
            tempWordVowel = self.changeVowelWord(tempLowerWord)
            if tempWordVowel not in wordVowel:
                wordVowel[tempWordVowel] = word
        
        ans = []
        for word in queries:
            tempWord = word.lower()
            tempWordVowel = self.changeVowelWord(tempWord)
			# If-Else priority wise, Capital before Vowel Transformed
            if word in wordPerfect:
                ans.append(word)
            elif tempWord in wordUpper:
                ans.append(wordUpper[tempWord])
            elif tempWordVowel in wordVowel:
                ans.append(wordVowel[tempWordVowel])
            else:
                ans.append("")
        
        return ans
        
    def changeVowelWord(self, word):
        
        myWord = ""
        for ch in word:
            if ch in "aeiou":
                myWord += \'*\'
            else:
                myWord += ch
        
        return myWord
        
```

</p>


