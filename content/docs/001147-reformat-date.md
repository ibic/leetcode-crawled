---
title: "Reformat Date"
weight: 1147
#id: "reformat-date"
---
## Description
<div class="description">
<p>Given a <code>date</code> string in the form&nbsp;<code>Day Month Year</code>, where:</p>

<ul>
	<li><code>Day</code>&nbsp;is in the set <code>{&quot;1st&quot;, &quot;2nd&quot;, &quot;3rd&quot;, &quot;4th&quot;, ..., &quot;30th&quot;, &quot;31st&quot;}</code>.</li>
	<li><code>Month</code>&nbsp;is in the set <code>{&quot;Jan&quot;, &quot;Feb&quot;, &quot;Mar&quot;, &quot;Apr&quot;, &quot;May&quot;, &quot;Jun&quot;, &quot;Jul&quot;, &quot;Aug&quot;, &quot;Sep&quot;, &quot;Oct&quot;, &quot;Nov&quot;, &quot;Dec&quot;}</code>.</li>
	<li><code>Year</code>&nbsp;is in the range <code>[1900, 2100]</code>.</li>
</ul>

<p>Convert the date string to the format <code>YYYY-MM-DD</code>, where:</p>

<ul>
	<li><code>YYYY</code> denotes the 4 digit year.</li>
	<li><code>MM</code> denotes the 2 digit month.</li>
	<li><code>DD</code> denotes the 2 digit day.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> date = &quot;20th Oct 2052&quot;
<strong>Output:</strong> &quot;2052-10-20&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> date = &quot;6th Jun 1933&quot;
<strong>Output:</strong> &quot;1933-06-06&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> date = &quot;26th May 1960&quot;
<strong>Output:</strong> &quot;1960-05-26&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The given dates are guaranteed to be valid, so no error handling is necessary.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Twilio - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java simple and concise Map
- Author: hobiter
- Creation Date: Sun Jul 12 2020 03:42:34 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 15 2020 06:27:56 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
	private static final Map<String, String> months = getMonths();
    public String reformatDate(String date) {
        String[] ss = date.split("\\s+");
        StringBuilder sb = new StringBuilder();
       
        sb.append(ss[2]).append("-");
        sb.append(months.get(ss[1])).append("-");
        sb.append(ss[0].length() == 3 ? ("0" + ss[0].substring(0, 1)) : ss[0].substring(0, 2));
        return sb.toString();
    }
    
    private static Map<String, String> getMonths(){
        Map<String, String> months = new HashMap<>();
        months.put("Jan", "01");
        months.put("Feb", "02");
        months.put("Mar", "03");
        months.put("Apr", "04");
        months.put("May", "05");
        months.put("Jun", "06");
        months.put("Jul", "07");
        months.put("Aug", "08");
        months.put("Sep", "09");
        months.put("Oct", "10");
        months.put("Nov", "11");
        months.put("Dec", "12");
        return months;
    }
}
```
</p>


### Python 3 - 24ms - Better than 100%
- Author: mb557x
- Creation Date: Sun Jul 12 2020 02:30:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 12 2020 02:30:33 GMT+0800 (Singapore Standard Time)

<p>
Approach:
1. Create a ```month``` dictionary.
2. Date can be of two lengths, i.e. 13 or 14. If the date is single digit (e.g. **5th** June 2020), then ```len(date) = 13```, otherwise ```len(date) = 14``` if it is a double digit **(i.e. 10 <= date <= 31)**

```
class Solution:
    def reformatDate(self, date: str) -> str:
        M = {"Jan" : "01", "Feb" : "02", "Mar" : "03", "Apr" : "04", "May" : "05", "Jun" : "06", "Jul" : "07", "Aug" : "08", "Sep" : "09", "Oct" : "10", "Nov" : "11", "Dec" : "12", }
        
        D = ""
        if (len(date) == 13):
            D += date[-4:] + "-" + M[date[-8:-5]] + "-" + date[:2]
        else:
            D += date[-4:] + "-" + M[date[-8:-5]] + "-0" + date[0]
        return D
```
```
#Runtime: 24ms
#100%
```
</p>


### Easy C++ Solution
- Author: arshad_yameen
- Creation Date: Sun Jul 12 2020 00:20:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 12 2020 00:20:16 GMT+0800 (Singapore Standard Time)

<p>
```
string reformatDate(string date) {
        string res;
        if(date.length()==13)
            res=date.substr(9,4)+\'-\'+ret(date.substr(5,3))+\'-\'+date.substr(0,2);
        else
            res=date.substr(8,4)+\'-\'+ret(date.substr(4,3))+\'-\'+\'0\'+date.substr(0,1);
        return res;
    }
    string ret(string s){
        if(s=="Jan")    return "01";
        else if(s=="Feb")   return "02";
        else if(s=="Mar")   return "03";
        else if(s=="Apr")   return "04";
        else if(s=="May")   return "05";
        else if(s=="Jun")   return "06";
        else if(s=="Jul")   return "07";
        else if(s=="Aug")   return "08";
        else if(s=="Sep")   return "09";
        else if(s=="Oct")   return "10";
        else if(s=="Nov")   return "11";
        else        return "12";
    }
</p>


