---
title: "Kth Smallest Number in Multiplication Table"
weight: 600
#id: "kth-smallest-number-in-multiplication-table"
---
## Description
<div class="description">
<p>
Nearly every one have used the <a href="https://en.wikipedia.org/wiki/Multiplication_table">Multiplication Table</a>. But could you find out the <code>k-th</code> smallest number quickly from the multiplication table?
</p>

<p>
Given the height <code>m</code> and the length <code>n</code> of a <code>m * n</code> Multiplication Table, and a positive integer <code>k</code>, you need to return the <code>k-th</code> smallest number in this table.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> m = 3, n = 3, k = 5
<b>Output:</b> 
<b>Explanation:</b> 
The Multiplication Table:
1	2	3
2	4	6
3	6	9

The 5-th smallest number is 3 (1, 2, 2, 3, 3).
</pre>
</p>


<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> m = 2, n = 3, k = 6
<b>Output:</b> 
<b>Explanation:</b> 
The Multiplication Table:
1	2	3
2	4	6

The 6-th smallest number is 6 (1, 2, 2, 3, 4, 6).
</pre>
</p>


<p><b>Note:</b><br>
<ol>
<li>The <code>m</code> and <code>n</code> will be in the range [1, 30000].</li>
<li>The <code>k</code> will be in the range [1, m * n]</li>
</ol>
</p>
</div>

## Tags
- Binary Search (binary-search)

## Companies
- Uber - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1: Brute Force [Memory Limit Exceeded]

**Intuition and Algorithm**

Create the multiplication table and sort it, then take the $$k^{th}$$ element.

<iframe src="https://leetcode.com/playground/JNTnTCLa/shared" frameBorder="0" name="JNTnTCLa" width="100%" height="258"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(m*n)$$ to create the table, and $$O(m*n\log(m*n))$$ to sort it.

* Space Complexity:  $$O(m*n)$$ to store the table.

---
#### Approach #2: Next Heap [Time Limit Exceeded]

**Intuition**

Maintain a heap of the smallest unused element of each row.  Then, finding the next element is a pop operation on the heap.

**Algorithm**

Our `heap` is going to consist of elements $$\text{(val, root)}$$, where $$\text{val}$$ is the next unused value of that row, and $$\text{root}$$ was the starting value of that row.

We will repeatedly find the next lowest element in the table.  To do this, we pop from the heap.  Then, if there's a next lowest element in that row, we'll put that element back on the heap.

<iframe src="https://leetcode.com/playground/Evrh9ssK/shared" frameBorder="0" name="Evrh9ssK" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(k * m \log m) = O(m^2 n \log m)$$.  Our initial heapify operation is $$O(m)$$.  Afterwards, each pop and push is $$O(m \log m)$$, and our outer loop is $$O(k) = O(m*n)$$

* Space Complexity:  $$O(m)$$.  Our heap is implemented as an array with $$m$$ elements.

---
#### Approach #3: Binary Search [Accepted]

**Intuition**

As $$\text{k}$$ and $$\text{m*n}$$ are up to $$9 * 10^8$$, linear solutions will not work.  This motivates solutions with $$\log$$ complexity, such as binary search.

**Algorithm**

Let's do the binary search for the answer $$\text{A}$$.

Say `enough(x)` is true if and only if there are $$\text{k}$$ or more values in the multiplication table that are less than or equal to $$\text{x}$$.  Colloquially, `enough` describes whether $$\text{x}$$ is large enough to be the $$k^{th}$$ value in the multiplication table.

Then (for our answer $$\text{A}$$), whenever $$\text{x &geq; A}$$, `enough(x)` is `True`; and whenever $$\text{x < A}$$, `enough(x)` is `False`.

In our binary search, our loop invariant is `enough(hi) = True`.  At the beginning, `enough(m*n) = True`, and whenever `hi` is set, it is set to a value that is "enough" (`enough(mi) = True`).  That means `hi` will be the lowest such value at the end of our binary search.

This leaves us with the task of counting how many values are less than or equal to $$\text{x}$$.  For each of $$\text{m}$$ rows, the $$i^{th}$$ row looks like $$\text{[i, 2*i, 3*i, ..., n*i]}$$.  The largest possible $$\text{k*i &leq; x}$$ that could appear is $$\text{k = x // i}$$. However, if $$\text{x}$$ is really big, then perhaps $$\text{k > n}$$, so in total there are $$\text{min(k, n) = min(x // i, n)}$$ values in that row that are less than or equal to $$\text{x}$$.

After we have the count of how many values in the table are less than or equal to $$\text{x}$$, by the definition of `enough(x)`, we want to know if that count is greater than or equal to $$\text{k}$$.

<iframe src="https://leetcode.com/playground/4ankdsg9/shared" frameBorder="0" name="4ankdsg9" width="100%" height="377"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(m * \log (m*n))$$.  Our binary search divides the interval $$\text{[lo, hi]}$$ into half at each step.  At each step, we call `enough` which requires $$O(m)$$ time.

* Space Complexity:  $$O(1)$$.  We only keep integers in memory during our intermediate calculations.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution, binary search
- Author: shawngao
- Creation Date: Sun Aug 27 2017 11:23:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 06:49:41 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int findKthNumber(int m, int n, int k) {
    	int low = 1 , high = m * n + 1;
        
    	while (low < high) {
    	    int mid = low + (high - low) / 2;
    	    int c = count(mid, m, n);
    	    if (c >= k) high = mid;
            else low = mid + 1;
    	}
        
    	return high;
    }
    
    private int count(int v, int m, int n) {
	int count = 0;
	for (int i = 1; i <= m; i++) {
	    int temp = Math.min(v / i , n);
	    count += temp;
	}
	return count;
    }
}
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Aug 27 2017 15:42:09 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 15:08:11 GMT+0800 (Singapore Standard Time)

<p>
Let's binary search for the answer `A`.

Say `enough(x)` is true if and only if there are `k` or more values in the multiplication table that are less than or equal to `x`.  Colloquially, `enough` describes whether `x` is large enough to be the `k-th` value in the multiplication table.

Then (for our answer `A`), whenever `x >= A`, `enough(x)` is `True`; and whenever `x < A`, `enough(x)` is `False`.

In our binary search, our loop invariant is that `enough(hi) = True`.  More specifically, if we were to apply `enough` onto each argument in the interval `[lo, hi]`, we would see 0 or more `False`, followed by 1 or more `True`.  Once `lo == hi`, we know that `enough(lo) = True`, and it must have been the smallest such one, because `lo` must have been `hi-1` or `hi-2` at some point, and `mi = hi-1` would have been checked.

This leaves us with the task of counting how many values are less than or equal to `x`.  For each of `m` rows, the `i`-th row looks like `[i, 2*i, 3*i, ..., n*i]`, and there are `min(x // i, n)` values in that row that are less than or equal to `x`.

```
def findKthNumber(self, m, n, k):
    def enough(x):
        return sum(min(x / i, n) for i in xrange(1, m+1)) >= k

    lo, hi = 1, m*n
    while lo < hi:
        mi = (lo + hi) / 2
        if not enough(mi):
            lo = mi + 1
        else:
            hi = mi
    return lo
```
</p>


### Python Binary Search -- Need to Return the Smallest Candidate
- Author: WangQiuc
- Creation Date: Tue Mar 26 2019 15:12:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 26 2019 15:12:00 GMT+0800 (Singapore Standard Time)

<p>
To find the kth smallest number in a multiplication table, a brute force way is to iterate from 1 to m * n and check whether there are k numbers no larger than it. If it isn\'t, we keep iterating until it is. Then we find the kth smallest number.

Since we have low end (1) and high end (m * n) of our search space, we can use binary search here. The key function is to check there are k numbers in M table no larger than our current searching value x:
```
def check(x):
	cnt = 0
	for r in range(1, m+1):
		cnt += min(x//r, n)  # \'x//r\' ensure \'x\' is included so that fits \'no larger than\'
		if cnt >= k: return False
	return True
```
For example, m=n=4
```
1  2  3  4
2  4  6  8
3  6  9  12
4  8  12 16
```
x=4, cnt=4+2+1+1=8, there are 8 number less or equal than 4. 
So if k <= 8, check(4) return False; if k > 8, check(4) return True. 

Each ```check(x)``` costs O(m). And ```sum(min(mid//r, n) for r in range(1,m+1)) < k``` produced the same result as ```check(x)``` but just doesn\'t break earlier. And we can implement our binary search in this way:
```
def findKthNumber(m, n, k):
	if m > n: m, n = n, m
	lo, hi = 1, m*n
	while lo < hi:
		mid = lo + hi >> 1
		if sum(min(mid//r, n) for r in range(1,m+1)) < k: lo = mid + 1
		else: hi = mid
	return lo
```
And you can notice that we don\'t return ```mid``` when ```sum(...) == k```  but keep binary searching until ```lo < hi```. This is because we need to return the **smallest candidate**.

This is tricky.
For example, m=34, n=42, k=401. You will find both 126 and 127 fit criteria that there are 401 numbers no larger than 126 and 127. **This is because 127 is not in the multiplication table!**. 127 is a prime and its only factorization is 1 * 127 which is out of the boundary of 34 * 42. And that\'s the reason we need to return the smallest candidate: To ensure our candidate is in M table.

So why the smallest candidate is in M table? 
Because if the smallest candidate(no smaller than k numbers in M table), saying x, is not in M table, then x-1 will also be a candidate(no smaller than k numbers in M table) since x is not in the table. Then x is not the smallest candidate.
For example, we have such range in our flattern sorted M table:
```
Val: 1,..., xi,  xi, xj,  xj,  ...
Seq: 1,..., p-1, p,  p+1, p+2, ...
```
When k=p, {xi, xi+1,..., xj-1} are all valid candidates. There are p numbers no larger than xi or xi+1or xj-1 becasue {xi+1,..., xj-1} are not in M table. And we need to return xi which is in the M table and the smallest candidate as well.
When k=p+1, the smallest candidate would be xj.

As for time complexity, it would be log(mn) times of binary search so total is O(mlog(mn)). 
And we can swap m, n if m > n and ensure time complexity to be O(min(m,n) * log(mn))

Hope above explanation is clear to you. This is a good example to show the tricky issue in binary search design.

</p>


