---
title: "Design Add and Search Words Data Structure"
weight: 195
#id: "design-add-and-search-words-data-structure"
---
## Description
<div class="description">
<p>Design a data structure that supports adding new words and finding if a string matches any previously added string.</p>

<p>Implement the <code>WordDictionary</code> class:</p>

<ul>
	<li><code>WordDictionary()</code>&nbsp;Initializes the object.</li>
	<li><code>void addWord(word)</code> Adds <code>word</code> to the data structure, it can be matched later.</li>
	<li><code>bool search(word)</code>&nbsp;Returns <code>true</code> if there is any string in the data structure that matches <code>word</code>&nbsp;or <code>false</code> otherwise. <code>word</code> may contain dots <code>&#39;.&#39;</code> where dots can be matched with any letter.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example:</strong></p>

<pre>
<strong>Input</strong>
[&quot;WordDictionary&quot;,&quot;addWord&quot;,&quot;addWord&quot;,&quot;addWord&quot;,&quot;search&quot;,&quot;search&quot;,&quot;search&quot;,&quot;search&quot;]
[[],[&quot;bad&quot;],[&quot;dad&quot;],[&quot;mad&quot;],[&quot;pad&quot;],[&quot;bad&quot;],[&quot;.ad&quot;],[&quot;b..&quot;]]
<strong>Output</strong>
[null,null,null,null,false,true,true,true]

<strong>Explanation</strong>
WordDictionary wordDictionary = new WordDictionary();
wordDictionary.addWord(&quot;bad&quot;);
wordDictionary.addWord(&quot;dad&quot;);
wordDictionary.addWord(&quot;mad&quot;);
wordDictionary.search(&quot;pad&quot;); // return False
wordDictionary.search(&quot;bad&quot;); // return True
wordDictionary.search(&quot;.ad&quot;); // return True
wordDictionary.search(&quot;b..&quot;); // return True
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= word.length &lt;= 500</code></li>
	<li><code>word</code> in <code>addWord</code> consists lower-case English letters.</li>
	<li><code>word</code> in <code>search</code> consist of&nbsp; <code>&#39;.&#39;</code> or lower-case English letters.</li>
	<li>At most <code>50000</code>&nbsp;calls will be made to <code>addWord</code>&nbsp;and <code>search</code>.</li>
</ul>

</div>

## Tags
- Backtracking (backtracking)
- Design (design)
- Trie (trie)

## Companies
- Facebook - 38 (taggedByAdmin: true)
- Amazon - 14 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Airbnb - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Data Structure Trie 

This article introduces the data structure [trie](https://en.wikipedia.org/wiki/Trie). 
It could be pronounced in two different ways: 
as "tree" or "try". 
Trie  which is also called a digital tree or a prefix tree 
is a kind of search ordered tree data structure 
mostly used for the efficient dynamic add/search operations with the strings. 

Trie is widely used in real life:
autocomplete search, spell checker, T9 predictive text, 
[IP routing (longest prefix matching)](https://www.researchgate.net/figure/An-example-routing-table-and-the-corresponding-binary-trie-built-from-it_fig3_4236637), 
[some GCC containers](https://gcc.gnu.org/onlinedocs/libstdc++/ext/pb_ds/trie_based_containers.html). 

Here is how it looks like

![fig](../Figures/211/trie.png)
{:align="center"}

*Figure 1. Data structure trie.*
{:align="center"}

There are two main types of trie interview questions:

- [Standard Trie](https://en.wikipedia.org/wiki/Trie). Design a structure to dynamically add and search strings, for example 

    - [Add and Search Word]().
    
    - [Word Search II](https://leetcode.com/articles/word-search-ii).
    
    - [Design Search Autocomplete System](https://leetcode.com/articles/design-search-autocomplete-system/).
    
- [Bitwise Trie](https://en.wikipedia.org/wiki/Trie#Bitwise_tries). Design a structure to dynamically add *binary* strings and compute
maximum/minimum XOR/AND/etc, 
for example

    - [Maximum XOR of Two Number in an Array](https://leetcode.com/articles/maximum-xor-of-two-numbers-in-an-array/).
    
#### Why Trie and not HashMap

It's quite easy to write the solution using such data structures as 
hashmap or balanced tree.

<iframe src="https://leetcode.com/playground/BFg6PW3U/shared" frameBorder="0" width="100%" height="500" name="BFg6PW3U"></iframe>

This solution passes all leetcode test cases, and 
formally has $$\mathcal{O}(M \cdot N)$$ time complexity for the search,
where $$M$$ is a length of the word to find, and $$N$$ is the number of words.
Although this solution is not efficient for the most important
practical use cases:

- Finding all keys with a common prefix.

- Enumerating a dataset of strings in lexicographical order.

- Scaling for the large datasets. Once the hash table increases in size, 
there are a lot of hash collisions and the search time complexity 
could degrade to $$\mathcal{O}(N^2 \cdot M)$$, 
where $$N$$ is the number of the inserted keys. 

    Trie could use less space compared to hashmap when storing 
    many keys with the same prefix. 
    In this case, using trie has only $$\mathcal{O}(M \cdot N)$$ time complexity, 
    where $$M$$ is the key length, and $$N$$ is the number of keys. 

<br />
<br />


---
#### Approach 1: Trie

**How to Implement Trie: addWord function**

![fig](../Figures/211/add_word.png)
{:align="center"}

*Figure 2. Trie implementation.*
{:align="center"}

In trie, each path from the root to the "word" node represents one of the input words,
for example, o -> a -> t -> h is "oath". 

Trie implementation is pretty straightforward, 
it's basically nested hashmaps.
At each step, one has to verify, 
if the child node to add is already present. 
If yes, just go one step down.
If not, add it into the trie and then go one step down. 

!?!../Documents/211_LIS.json:1000,472!?!

<iframe src="https://leetcode.com/playground/cdAhqU3W/shared" frameBorder="0" width="100%" height="500" name="cdAhqU3W"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(M)$$, where $$M$$ is the key length.
At each step, we either examine or create a node in the trie. That takes only $$M$$ operations.

* Space complexity: $$\mathcal{O}(M)$$. In the worst-case newly inserted 
key doesn't share a prefix with the keys already inserted in the trie. 
We have to add $$M$$ new nodes, which takes $$\mathcal{O}(M)$$ space.

**Search in Trie**

In the absence of '.' characters, the search would be as simple as `addWord`.
Each key is represented in the trie as a path from the root 
to the internal node or leaf. 
We start from the root and go down in trie, checking character by character. 

![fig](../Figures/211/search2.png)
{:align="center"}

*Figure 3. Search in trie.*
{:align="center"}

The presence of '.' characters forces us to explore all possible paths
at each `.` level.

![fig](../Figures/211/search_dot.png)
{:align="center"}

*Figure 4. Search in trie.*
{:align="center"}

<iframe src="https://leetcode.com/playground/kPcZkH2S/shared" frameBorder="0" width="100%" height="500" name="kPcZkH2S"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(M)$$ for the "well-defined" words 
without dots, where $$M$$ is the key length, and $$N$$ is a number of keys,
and $$\mathcal{O}(M \cdot N)$$ for the "undefined" words.
That corresponds to the worst-case situation of searching an undefined word 
$$\underbrace{.........}_\text{(M + 1) times}$$ which is one character longer than
all inserted keys.

* Space complexity: $$\mathcal{O}(1)$$ for the search of "well-defined" words
without dots, and up to $$\mathcal{O}(M)$$ for the "undefined" words, to keep
the recursion stack.

**Implementation**

<iframe src="https://leetcode.com/playground/c8epxoMs/shared" frameBorder="0" width="100%" height="500" name="c8epxoMs"></iframe>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My simple and clean Java code
- Author: Lnic
- Creation Date: Sat May 16 2015 14:29:53 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 22:42:09 GMT+0800 (Singapore Standard Time)

<p>
Using backtrack to check each character of word to search.

    public class WordDictionary {
        public class TrieNode {
            public TrieNode[] children = new TrieNode[26];
            public String item = "";
        }
        
        private TrieNode root = new TrieNode();
    
        public void addWord(String word) {
            TrieNode node = root;
            for (char c : word.toCharArray()) {
                if (node.children[c - 'a'] == null) {
                    node.children[c - 'a'] = new TrieNode();
                }
                node = node.children[c - 'a'];
            }
            node.item = word;
        }
    
        public boolean search(String word) {
            return match(word.toCharArray(), 0, root);
        }
        
        private boolean match(char[] chs, int k, TrieNode node) {
            if (k == chs.length) return !node.item.equals("");   
            if (chs[k] != '.') {
                return node.children[chs[k] - 'a'] != null && match(chs, k + 1, node.children[chs[k] - 'a']);
            } else {
                for (int i = 0; i < node.children.length; i++) {
                    if (node.children[i] != null) {
                        if (match(chs, k + 1, node.children[i])) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
</p>


### Python easy to follow solution using Trie.
- Author: OldCodingFarmer
- Creation Date: Tue Sep 08 2015 21:25:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 04:23:41 GMT+0800 (Singapore Standard Time)

<p>
    

    class TrieNode():
        def __init__(self):
            self.children = collections.defaultdict(TrieNode)
            self.isWord = False
        
    class WordDictionary(object):
        def __init__(self):
            self.root = TrieNode()

        def addWord(self, word):
            node = self.root
            for w in word:
                node = node.children[w]
            node.isWord = True

        def search(self, word):
            node = self.root
            self.res = False
            self.dfs(node, word)
            return self.res
        
        def dfs(self, node, word):
            if not word:
                if node.isWord:
                    self.res = True
                return 
            if word[0] == ".":
                for n in node.children.values():
                    self.dfs(n, word[1:])
            else:
                node = node.children.get(word[0])
                if not node:
                    return 
                self.dfs(node, word[1:])
</p>


### C++ Trie
- Author: jianchao-li
- Creation Date: Sun Jun 07 2015 00:17:44 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 12:06:49 GMT+0800 (Singapore Standard Time)

<p>
This is a vanilla application of Trie. The main difficulty lies in how to deal with the `.` character. I use a naive solution in this place: each time when I reach a `.`, I simply traverse all the children of the current node and recursively search the remaining substring starting from each of those children.

```cpp
class TrieNode {
public:
    bool word;
    TrieNode* children[26];
    TrieNode() {
        word = false;
        memset(children, NULL, sizeof(children));
    }
};

class WordDictionary {
public:
    /** Initialize your data structure here. */
    WordDictionary() {
        
    }
    
    /** Adds a word into the data structure. */
    void addWord(string word) {
        TrieNode* node = root;
        for (char c : word) {
            if (!node -> children[c - \'a\']) {
                node -> children[c - \'a\'] = new TrieNode();
            }
            node = node -> children[c - \'a\'];
        }
        node -> word = true;
    }
    
    /** Returns if the word is in the data structure. A word could contain the dot character \'.\' to represent any one letter. */
    bool search(string word) {
        return search(word.c_str(), root);
    }
private:
    TrieNode* root = new TrieNode();
    
    bool search(const char* word, TrieNode* node) {
        for (int i = 0; word[i] && node; i++) {
            if (word[i] != \'.\') {
                node = node -> children[word[i] - \'a\'];
            } else {
                TrieNode* tmp = node;
                for (int j = 0; j < 26; j++) {
                    node = tmp -> children[j];
                    if (search(word + i + 1, node)) {
                        return true;
                    }
                }
            }
        }
        return node && node -> word;
    }
};
 ```
</p>


