---
title: "Reverse Subarray To Maximize Array Value"
weight: 1128
#id: "reverse-subarray-to-maximize-array-value"
---
## Description
<div class="description">
<p>You are given an integer array <code>nums</code>. The <em>value</em> of this array is defined as the sum of <code>|nums[i]-nums[i+1]|</code>&nbsp;for all&nbsp;<code>0 &lt;= i &lt; nums.length-1</code>.</p>

<p>You are allowed to select any subarray of the given array and reverse it. You can perform this operation <strong>only once</strong>.</p>

<p>Find maximum possible value of the final array.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [2,3,1,5,4]
<strong>Output:</strong> 10
<b>Explanation: </b>By reversing the subarray [3,1,5] the array becomes [2,5,1,3,4] whose value is 10.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [2,4,9,24,2,1,10]
<strong>Output:</strong> 68
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 3*10^4</code></li>
	<li><code>-10^5 &lt;= nums[i] &lt;= 10^5</code></li>
</ul>
</div>

## Tags
- Array (array)
- Math (math)

## Companies
- Codenation - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### O(n) Solution with explanation
- Author: neal99
- Creation Date: Sun Jan 26 2020 02:28:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 26 2020 13:44:32 GMT+0800 (Singapore Standard Time)

<p>
Assume the list is : `x, y, ..., a, [b, ..., c], d, ...`, and we are going to reverse `[b, ..., c]`.  We are now going to look at how the value will change after reversion.

It will only have two situations:  interval `[min(a,b), max(a,b)]`intersects with `[min(c,d), max(c,d)]` or not.

1.  interval `[min(a,b), max(a,b)]` **does** intersect with `[min(c,d), max(c,d)]`.
	
	Note that the reversion will not change `abs(nums[i] - nums[i+1])`, except for a, b, c, and d. Hence the amount of total value change is `-[abs(a-b)+abs(c-d)] + [abs(a-c)+abs(b-d)]`
	Now try to draw them in the graph. There will be two patterns of graph. Let\'s check the first one.![image](https://assets.leetcode.com/users/neal99/image_1579975186.png)
	The two red lines represent  `abs(a-b)` and `abs(c-d)`. The blue and green lines represent two possible results for `[abs(a-c)+abs(b-d)]`. Therefore, the total change is the difference of the total length of red lines and blue/green lines. Note that in this situation the total change can not be positive. So we will not reverse `[b, ..., c]` in this situation.
	
	For another pattern:
	![image](https://assets.leetcode.com/users/neal99/image_1580016911.png)

	Simiarly, we also will not reverse `[b, ..., c]` in this situation.
	
2.  interval `[min(a,b), max(a,b)]` **does not** intersect with `[min(c,d), max(c,d)]`.
	Similarly, total value change is still `-[abs(a-b)+abs(c-d)]+[abs(a-c)+abs(b-d)]`. Now try to draw them.![image](https://assets.leetcode.com/users/neal99/image_1579975983.png)
	As same as above, the two red lines represent  `abs(a-b)` and `abs(c-d)`. The blue and green lines represent two possible results for `[abs(a-c)+abs(b-d)]`. Notice that the total lengths of the blue lines and green lines are the same.  In this situation, the total change is the difference of the total length of red lines and blue/green lines, which is two times the length of the yellow line.
	
	Therefore, we only need to find the maximum length of the yellow line, and then times 2 and plus the original value.
	
	Notice `length of the yellow line = min(c,d) - max(a,b)`, we only need to find out `max(min(c,d) for any c,d)` and `min(max(a,b) for any a,b))`. An iteration is enough.
	
	Don\'t forget the boundary situation where `b` is `nums[0]` or `c` is `nums[n-1]`. Just another iteration will do. 

The final code (in Python) is as below.
```python
class Solution:
    def maxValueAfterReverse(self, nums: List[int]) -> int:
        maxi, mini = -math.inf, math.inf
        
        for a, b in zip(nums, nums[1:]):
            maxi = max(min(a, b), maxi)
            mini = min(max(a, b), mini)
        change = max(0, (maxi - mini) * 2)
        
        # solving the boundary situation
        for a, b in zip(nums, nums[1:]):
            tmp1 = - abs(a - b) + abs(nums[0] - b)
            tmp2 = - abs(a - b) + abs(nums[-1] - a)
            change = max([tmp1, tmp2, change])
			
        original_value = sum(abs(a - b) for a, b in zip(nums, nums[1:]))
        return  original_value + change
```
Notice that you can put the two iterations together, but i decide to devide them for more readability. : )
</p>


### So many people cheats for a AC solution, cheating reported.
- Author: ToffeeLu
- Creation Date: Sun Jan 26 2020 00:14:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 26 2020 00:14:20 GMT+0800 (Singapore Standard Time)

<p>
I\'ve checked guys with AC solution but failed too many times, cheating reported for hard code edge cases.
![image](https://assets.leetcode.com/users/toffeelu/image_1579968475.png)

Also I found some people got AC with no failures, but hard code also, two accounts? really clever.
![image](https://assets.leetcode.com/users/toffeelu/image_1579968696.png)


</p>


### [Java/C++/Python] One Pass, O(1) Space
- Author: lee215
- Creation Date: Sun Jan 26 2020 00:11:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 26 2020 01:32:27 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
`total` calculate the total sum of `|A[i] - A[j]|`.
`res` record the value the we can improve.

Assume the current pair is (a,b) = (A[i], A[i+1]).

If we reverse all element from `A[0]` to `A[i]`,
we will improve `abs(A[0] - b) - abs(a - b)`

If we reverse all element from `A[i+1]` to `A[n-1]`,
we will improve `abs(A[n - 1] - a) - abs(a - b)`

As we iterate the whole array,
We also record the maximum pair and the minimum pair.
We can break these two pair and reverse all element in the middle.
This will improve `(max2 - min2) * 2`
<br>

## **Complexity**
Time `O(N)`
Space `O(1)`
<br>

**Java**
```java
    public int maxValueAfterReverse(int[] A) {
        int total = 0, res = 0, min2 = 123456, max2 = -123456, n = A.length;
        for (int i = 0; i < n - 1; ++i) {
            int a = A[i], b = A[i + 1];
            total += Math.abs(a - b);
            res = Math.max(res, Math.abs(A[0] - b) - Math.abs(a - b));
            res = Math.max(res, Math.abs(A[n - 1] - a) - Math.abs(a - b));
            min2 = Math.min(min2, Math.max(a, b));
            max2 = Math.max(max2, Math.min(a, b));
        }
        return total + Math.max(res, (max2 - min2) * 2);
    }
```
**C++**
```cpp
    int maxValueAfterReverse(vector<int>& A) {
        int total = 0, res = 0, min2 = 123456, max2 = -123456, n = A.size();
        for (int i = 0; i < n - 1; ++i) {
            int a = A[i], b = A[i + 1];
            total += abs(a - b);
            res = max(res, abs(A[0] - b) - abs(a - b));
            res = max(res, abs(A[n - 1] - a) - abs(a - b));
            min2 = min(min2, max(a, b));
            max2 = max(max2, min(a, b));
        }
        return total + max(res, (max2 - min2) * 2);
    }
```
**Python:**
```python
    def maxValueAfterReverse(self, A):
        total, res, min2, max2 = 0, 0, float(\'inf\'), -float(\'inf\')
        for a, b in zip(A, A[1:]):
            total += abs(a - b)
            res = max(res, abs(A[0] - b) - abs(a - b))
            res = max(res, abs(A[-1] - a) - abs(a - b))
            min2, max2 = min(min2, max(a, b)), max(max2, min(a, b))
        return total + max(res, (max2 - min2) * 2)
```

</p>


