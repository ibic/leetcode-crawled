---
title: "Maximum Profit of Operating a Centennial Wheel"
weight: 1459
#id: "maximum-profit-of-operating-a-centennial-wheel"
---
## Description
<div class="description">
<p>You are the operator of a Centennial Wheel that has <strong>four gondolas</strong>, and each gondola has room for <strong>up</strong> <strong>to</strong> <strong>four people</strong>. You have the ability to rotate the gondolas <strong>counterclockwise</strong>, which costs you <code>runningCost</code> dollars.</p>

<p>You are given an array <code>customers</code> of length <code>n</code> where <code>customers[i]</code> is the number of new customers arriving just before the <code>i<sup>th</sup></code> rotation (0-indexed). This means you <strong>must rotate the wheel </strong><code>i</code><strong> times before the </strong><code>customers[i]</code><strong> customers arrive</strong>. <strong>You cannot make customers wait if there is room in the gondola</strong>. Each customer pays <code>boardingCost</code> dollars when they board on the gondola closest to the ground and will exit once that gondola reaches the ground again.</p>

<p>You can stop the wheel at any time, including <strong>before</strong> <strong>serving</strong> <strong>all</strong> <strong>customers</strong>. If you decide to stop serving customers, <strong>all subsequent rotations are free</strong> in order to get all the customers down safely. Note that if there are currently more than four customers waiting at the wheel, only four will board the gondola, and the rest will wait <strong>for the next rotation</strong>.</p>

<p>Return<em> the minimum number of rotations you need to perform to maximize your profit.</em> If there is <strong>no scenario</strong> where the profit is positive, return <code>-1</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/09/wheeldiagram12.png" style="width: 906px; height: 291px;" />
<pre>
<strong>Input:</strong> customers = [8,3], boardingCost = 5, runningCost = 6
<strong>Output:</strong> 3
<strong>Explanation:</strong> The numbers written on the gondolas are the number of people currently there.
1. 8 customers arrive, 4 board and 4 wait for the next gondola, the wheel rotates. Current profit is 4 * $5 - 1 * $6 = $14.
2. 3 customers arrive, the 4 waiting board the wheel and the other 3 wait, the wheel rotates. Current profit is 8 * $5 - 2 * $6 = $28.
3. The final 3 customers board the gondola, the wheel rotates. Current profit is 11 * $5 - 3 * $6 = $37.
The highest profit was $37 after rotating the wheel 3 times.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> customers = [10,9,6], boardingCost = 6, runningCost = 4
<strong>Output:</strong> 7
<strong>Explanation:</strong>
1. 10 customers arrive, 4 board and 6 wait for the next gondola, the wheel rotates. Current profit is 4 * $6 - 1 * $4 = $20.
2. 9 customers arrive, 4 board and 11 wait (2 originally waiting, 9 newly waiting), the wheel rotates. Current profit is 8 * $6 - 2 * $4 = $40.
3. The final 6 customers arrive, 4 board and 13 wait, the wheel rotates. Current profit is 12 * $6 - 3 * $4 = $60.
4. 4 board and 9 wait, the wheel rotates. Current profit is 16 * $6 - 4 * $4 = $80.
5. 4 board and 5 wait, the wheel rotates. Current profit is 20 * $6 - 5 * $4 = $100.
6. 4 board and 1 waits, the wheel rotates. Current profit is 24 * $6 - 6 * $4 = $120.
7. 1 boards, the wheel rotates. Current profit is 25 * $6 - 7 * $4 = $122.
The highest profit was $122 after rotating the wheel 7 times.

</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> customers = [3,4,0,5,1], boardingCost = 1, runningCost = 92
<strong>Output:</strong> -1
<strong>Explanation:</strong>
1. 3 customers arrive, 3 board and 0 wait, the wheel rotates. Current profit is 3 * $1 - 1 * $92 = -$89.
2. 4 customers arrive, 4 board and 0 wait, the wheel rotates. Current profit is 7 * $1 - 2 * $92 = -$177.
3. 0 customers arrive, 0 board and 0 wait, the wheel rotates. Current profit is 7 * $1 - 3 * $92 = -$269.
4. 5 customers arrive, 4 board and 1 waits, the wheel rotates. Current profit is 11 * $1 - 4 * $92 = -$357.
5. 1 customer arrives, 2 board and 0 wait, the wheel rotates. Current profit is 13 * $1 - 5 * $92 = -$447.
The profit was never positive, so return -1.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> customers = [10,10,6,4,7], boardingCost = 3, runningCost = 8
<strong>Output:</strong> 9
<strong>Explanation:</strong>
1. 10 customers arrive, 4 board and 6 wait, the wheel rotates. Current profit is 4 * $3 - 1 * $8 = $4.
2. 10 customers arrive, 4 board and 12 wait, the wheel rotates. Current profit is 8 * $3 - 2 * $8 = $8.
3. 6 customers arrive, 4 board and 14 wait, the wheel rotates. Current profit is 12 * $3 - 3 * $8 = $12.
4. 4 customers arrive, 4 board and 14 wait, the wheel rotates. Current profit is 16 * $3 - 4 * $8 = $16.
5. 7 customers arrive, 4 board and 17 wait, the wheel rotates. Current profit is 20 * $3 - 5 * $8 = $20.
6. 4 board and 13 wait, the wheel rotates. Current profit is 24 * $3 - 6 * $8 = $24.
7. 4 board and 9 wait, the wheel rotates. Current profit is 28 * $3 - 7 * $8 = $28.
8. 4 board and 5 wait, the wheel rotates. Current profit is 32 * $3 - 8 * $8 = $32.
9. 4 board and 1 waits, the wheel rotates. Current profit is 36 * $3 - 9 * $8 = $36.
10. 1 board and 0 wait, the wheel rotates. Current profit is 37 * $3 - 10 * $8 = $31.
The highest profit was $36 after rotating the wheel 9 times.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == customers.length</code></li>
	<li><code>1 &lt;= n &lt;= 10<sup>5</sup></code></li>
	<li><code>0 &lt;= customers[i] &lt;= 50</code></li>
	<li><code>1 &lt;= boardingCost, runningCost &lt;= 100</code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- peak6 - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Simple O(N) greedy
- Author: hobiter
- Creation Date: Sun Sep 27 2020 12:03:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 27 2020 13:20:51 GMT+0800 (Singapore Standard Time)

<p>
Cleaner ones:
time: O(N), N is the total passenger count / 4;
```
    public int minOperationsMaxProfit(int[] cs, int bc, int rc) {
        int run = 0, maxRun = 1, prof = 0, maxProf = prof, sum = 0, i = 0;
        while (sum > 0 || i < cs.length) {
            if (i < cs.length) sum += cs[i++];
            int bd = Math.min(4, sum);  // boarding people by greedy. 
            sum -= bd;
            prof = prof + bd * bc - rc;
            run++;
            if (prof > maxProf) {
                maxProf = prof;
                maxRun = run;
            }
        }
        return maxProf > 0 ? maxRun : -1;
    }
```

</p>


### Very Easy Idea, Simulation Problem, Readable Code with comments
- Author: interviewrecipes
- Creation Date: Sun Sep 27 2020 12:02:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 27 2020 12:14:54 GMT+0800 (Singapore Standard Time)

<p>
```

/*

One small idea that makes the code a lot simpler is to only accept 4 customers from customers[i] 
at a time and defer the rest into
customer[i+1].
At the end we can deal with all the waiting customers at once.

*/

class Solution {
	int rotations = 0;  // Stores number of rotations made so far.
    int profit = 0;        // Stores current profit after "rotations" rotations.
	int maxProfit = 0;  // Stores max profit reached so far.
	int bestRotations = 0;   // Stores rotations corresponding to "maxProfit".


    int bCost, rCost;  // Boarding and Running costs.
public:

    // Updates the necessary variables and computes maxProfit, bestRotations etc.
    void report(int boarded) {
        profit += (boarded*bCost - rCost);  // add the profit earned.
        rotations++;                        // count this rotation.
        if (profit > maxProfit) {           // update maximum profit.
            maxProfit = profit;
            bestRotations = rotations;
        }
    }
    
    int minOperationsMaxProfit(vector<int>& customers, int boardingCost, int runnningCost) {
        bCost = boardingCost;
        rCost = runnningCost;
        int n = customers.size();
        for (int i=0; i<n-1; i++) {   // For each group of customers -
            if (customers[i] > 4) {   // Limit it to just 4
                customers[i+1] += (customers[i]-4);  // Add remaining to next
                customers[i] = 4;  // update the current group.
            }
            report(customers[i]);  // generate report of this rotation
        }
        
        int waiting = customers[n-1]; 
        while (waiting > 0) {
            int boarded = min(4, waiting); // max 4 at a time.
            waiting -= boarded; // reduce the people in waiting
            report(boarded);  // generate report
        }
        
        return maxProfit > 0 ? bestRotations : -1;
    }
};
```
</p>


### Easy O(N) Solution with explanation and clean 9 line code
- Author: akku_1
- Creation Date: Sun Sep 27 2020 12:02:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 27 2020 12:13:34 GMT+0800 (Singapore Standard Time)

<p>
The solution is based on simply simulating the the rotations and keep track of waiting customers 
for better understanding watch the video :)




link https://youtu.be/Ra8Z2AfFy2o?t=1


Space O(1)
Time O(n)


code

		int minOperationsMaxProfit(vector<int>& customers, int boardingCost, int runningCost)
    {
        int c=0,rem=0,r=0,p_max=-1,ans;
        for(int i=0;i<customers.size()||(rem);i++)
        {
            r++;
            rem+=i<customers.size()?customers[i]:0;
            if(rem<4) c+=rem,rem=0;
            else c+=4,rem-=4;
            int p=c*boardingCost-r*runningCost;
            if(p>p_max)p_max=p,ans=r;
        }
        return p_max<0?-1:ans;
        
    }


</p>


