---
title: "Minimum Depth of Binary Tree"
weight: 111
#id: "minimum-depth-of-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree, find its minimum depth.</p>

<p>The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.</p>

<p><strong>Note:</strong>&nbsp;A leaf is a node with no children.</p>

<p><strong>Example:</strong></p>

<p>Given binary tree <code>[3,9,20,null,null,15,7]</code>,</p>

<pre>
    3
   / \
  9  20
    /  \
   15   7</pre>

<p>return its minimum&nbsp;depth = 2.</p>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

**Tree definition**

First of all, here is the definition of the ```TreeNode``` which we would use.

<iframe src="https://leetcode.com/playground/nLbQ3LkB/shared" frameBorder="0" width="100%" height="225" name="nLbQ3LkB"></iframe>
<br />
<br />


---
#### Approach 1: Recursion

**Algorithm**

The intuitive approach is to solve the problem by recursion.
Here we demonstrate an example with the DFS (Depth First Search) strategy. 

<iframe src="https://leetcode.com/playground/pryAjnkm/shared" frameBorder="0" width="100%" height="412" name="pryAjnkm"></iframe>

**Complexity analysis**

* Time complexity : we visit each node exactly once, 
thus the time complexity is $$\mathcal{O}(N)$$,
where $$N$$ is the number of nodes.

* Space complexity : in the worst case, the tree is completely unbalanced,
*e.g.* each node has only one child node, the recursion call would occur
 $$N$$ times (the height of the tree), therefore the storage to keep the call stack would be $$\mathcal{O}(N)$$.
 But in the best case (the tree is completely balanced), the height of the tree would be $$\log(N)$$.
 Therefore, the space complexity in this case would be $$\mathcal{O}(\log(N))$$.
<br />
<br />


---
#### Approach 2: DFS Iteration

We could also convert the above recursion into iteration, with the help of stack.

>The idea is to visit each leaf with the DFS strategy,
while updating the minimum depth when we reach the leaf node.

So we start from a stack which contains the root node and the corresponding depth 
which is ```1```.
Then we proceed to the iterations: pop the current node out of the stack and
push the child nodes. The minimum depth is updated at each leaf node. 

<iframe src="https://leetcode.com/playground/siLRMujK/shared" frameBorder="0" width="100%" height="500" name="siLRMujK"></iframe>  

**Complexity analysis**

* Time complexity : each node is visited exactly once and time complexity is 
$$\mathcal{O}(N)$$.

* Space complexity : in the worst case we could keep up to the entire tree,
that results in $$\mathcal{O}(N)$$ space complexity.
<br />
<br />


---
#### Approach 3: BFS Iteration

The drawback of the DFS approach in this case is that all nodes should be visited
to ensure that the minimum depth would be found. Therefore, this results in a $$\mathcal{O}(N)$$
complexity.
One way to optimize the complexity is to use the BFS strategy.
We iterate the tree level by level, and the first leaf we reach
 corresponds to the minimum depth. As a result, we do not need to iterate all nodes.

<iframe src="https://leetcode.com/playground/zECMhTcF/shared" frameBorder="0" width="100%" height="500" name="zECMhTcF"></iframe>  

**Complexity analysis**

* Time complexity : in the worst case for a balanced tree we need
 to visit all nodes level by level up to the tree height, 
 that excludes the bottom level only.
 This way we visit $$N/2$$ nodes,
 and thus the time complexity is $$\mathcal{O}(N)$$.

* Space complexity : is the same as time complexity here 
$$\mathcal{O}(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My 4 Line java solution
- Author: caiqi8877
- Creation Date: Thu Feb 12 2015 00:54:12 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 20:03:21 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public int minDepth(TreeNode root) {
            if(root == null) return 0;
            int left = minDepth(root.left);
            int right = minDepth(root.right);
            return (left == 0 || right == 0) ? left + right + 1: Math.min(left,right) + 1;
           
        }
    }
</p>


### 3 lines in Every Language
- Author: StefanPochmann
- Creation Date: Tue Jun 23 2015 22:12:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 17:09:00 GMT+0800 (Singapore Standard Time)

<p>
We need to add the smaller one of the child depths - except if that's zero, then add the larger one. The first Python solution is the clearest because it lets me directly say exactly that.

**Python versions:**

    def minDepth(self, root):
        if not root: return 0
        d = map(self.minDepth, (root.left, root.right))
        return 1 + (min(d) or max(d))

    def minDepth(self, root):
        if not root: return 0
        d, D = sorted(map(self.minDepth, (root.left, root.right)))
        return 1 + (d or D)

**C++ versions:**

    int minDepth(TreeNode* root) {
        if (!root) return 0;
        int L = minDepth(root->left), R = minDepth(root->right);
        return 1 + (min(L, R) ? min(L, R) : max(L, R));
    }

    int minDepth(TreeNode* root) {
        if (!root) return 0;
        int L = minDepth(root->left), R = minDepth(root->right);
        return 1 + (L && R ? min(L, R) : max(L, R));
    }

    int minDepth(TreeNode* root) {
        if (!root) return 0;
        int L = minDepth(root->left), R = minDepth(root->right);
        return 1 + (!L-!R ? max(L, R) : min(L, R));
    }

    int minDepth(TreeNode* root) {
        if (!root) return 0;
        int L = minDepth(root->left), R = minDepth(root->right);
        return L<R && L || !R ? 1+L : 1+R;
    }

**Java versions:**

    public int minDepth(TreeNode root) {
        if (root == null) return 0;
        int L = minDepth(root.left), R = minDepth(root.right);
        return 1 + (Math.min(L, R) > 0 ? Math.min(L, R) : Math.max(L, R));
    }

    public int minDepth(TreeNode root) {
        if (root == null) return 0;
        int L = minDepth(root.left), R = minDepth(root.right), m = Math.min(L, R);
        return 1 + (m > 0 ? m : Math.max(L, R));
    }

    public int minDepth(TreeNode root) {
        if (root == null) return 0;
        int L = minDepth(root.left), R = minDepth(root.right);
        return L<R && L>0 || R<1 ? 1+L : 1+R;
    }

**Ruby version:**

    def min_depth(root)
        return 0 if !root
        d, e = [min_depth(root.left), min_depth(root.right)].sort
        1 + (d>0 ? d : e)
    end

**Javascript version:**

    var minDepth = function(root) {
        if (!root) return 0
        var L = minDepth(root.left), R = minDepth(root.right)
        return 1 + (Math.min(L, R) || Math.max(L, R))
    };

**C version:**

    int minDepth(struct TreeNode* root) {
        if (!root) return 0;
        int L = minDepth(root->left), R = minDepth(root->right);
        return L<R && L || !R ? 1+L : 1+R;
    }

**C# version:**

    public int MinDepth(TreeNode root) {
        if (root == null) return 0;
        int L = MinDepth(root.left), R = MinDepth(root.right);
        return L<R && L>0 || R<1 ? 1+L : 1+R;
    }
</p>


