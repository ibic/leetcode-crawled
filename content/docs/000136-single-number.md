---
title: "Single Number"
weight: 136
#id: "single-number"
---
## Description
<div class="description">
<p>Given a <strong>non-empty</strong>&nbsp;array of integers <code>nums</code>, every element appears <em>twice</em> except for one. Find that single one.</p>

<p><strong>Follow up:</strong>&nbsp;Could you implement a solution with a linear runtime complexity and without using extra memory?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> nums = [2,2,1]
<strong>Output:</strong> 1
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> nums = [4,1,2,1,2]
<strong>Output:</strong> 4
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> nums = [1]
<strong>Output:</strong> 1
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 3 * 10<sup>4</sup></code></li>
	<li><code>-3 * 10<sup>4</sup> &lt;= nums[i] &lt;= 3 * 10<sup>4</sup></code></li>
	<li>Each element in the array appears twice except for one element which appears only once.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Bit Manipulation (bit-manipulation)

## Companies
- Facebook - 6 (taggedByAdmin: false)
- Amazon - 5 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Tencent - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)
- Palantir Technologies - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: List operation

**Algorithm**

1. Iterate over all the elements in $$\text{nums}$$
2. If some number in $$\text{nums}$$ is new to array, append it
3. If some number is already in the array, remove it

<iframe src="https://leetcode.com/playground/7avfRKL7/shared" frameBorder="0" width="100%" height="293" name="7avfRKL7"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. We iterate through $$\text{nums}$$, taking $$O(n)$$ time. We search the whole list to find whether there is duplicate number, taking $$O(n)$$ time. Because search is in the `for` loop, so we have to multiply both time complexities which is $$O(n^2)$$.

* Space complexity : $$O(n)$$.  We need a list of size $$n$$ to contain elements in $$\text{nums}$$.
<br />
<br />


---
#### Approach 2: Hash Table

**Algorithm**

We use hash table to avoid the $$O(n)$$ time required for searching the elements.

1. Iterate through all elements in `nums` and set up key/value pair.
2. Return the element which appeared only once.

<iframe src="https://leetcode.com/playground/kFCNodJN/shared" frameBorder="0" width="100%" height="310" name="kFCNodJN"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n \cdot 1) = O(n)$$.  Time complexity of `for` loop is $$O(n)$$. Time complexity of hash table(dictionary in python) operation `pop` is $$O(1)$$.

* Space complexity : $$O(n)$$. The space required by $$hash\_table$$ is equal to the number of elements in $$\text{nums}$$.
<br />
<br />


---
#### Approach 3: Math

**Concept**

$$2 * (a + b + c) - (a + a + b + b + c) = c$$

<iframe src="https://leetcode.com/playground/4UnTgakr/shared" frameBorder="0" width="100%" height="310" name="4UnTgakr"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n + n) = O(n)$$. `sum` will call `next` to iterate through $$\text{nums}$$.
We can see it as `sum(list(i, for i in nums))` which means the time complexity is $$O(n)$$ because of the number of elements($$n$$) in $$\text{nums}$$.

* Space complexity : $$O(n + n) = O(n)$$. `set` needs space for the elements in `nums`
<br />
<br />


---
#### Approach 4: Bit Manipulation

**Concept**

- If we take XOR of zero and some bit, it will return that bit
    - $$a \oplus 0 = a$$
- If we take XOR of two same bits, it will return 0
    - $$a \oplus a = 0$$
- $$a \oplus b \oplus a = (a \oplus a) \oplus b = 0 \oplus b = b$$

So we can XOR all bits together to find the unique number.

<iframe src="https://leetcode.com/playground/3d3aMiFy/shared" frameBorder="0" width="100%" height="225" name="3d3aMiFy"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$.  We only iterate through $$\text{nums}$$, so the time complexity is the number of elements in $$\text{nums}$$.

* Space complexity : $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My O(n) solution using XOR
- Author: Ivantsang
- Creation Date: Thu Jun 12 2014 20:14:00 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 17:46:13 GMT+0800 (Singapore Standard Time)

<p>
known that A XOR A = 0 and the XOR operator is commutative, the solution will be very straightforward.
`

    int singleNumber(int A[], int n) {
        int result = 0;
        for (int i = 0; i<n; i++)
        {
			result ^=A[i];
        }
		return result;
    }
`
</p>


### Python different solutions.
- Author: OldCodingFarmer
- Creation Date: Thu Jul 23 2015 21:23:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 00:43:25 GMT+0800 (Singapore Standard Time)

<p>
   
    def singleNumber1(self, nums):
        dic = {}
        for num in nums:
            dic[num] = dic.get(num, 0)+1
        for key, val in dic.items():
            if val == 1:
                return key
    
    def singleNumber2(self, nums):
        res = 0
        for num in nums:
            res ^= num
        return res
        
    def singleNumber3(self, nums):
        return 2*sum(set(nums))-sum(nums)
        
    def singleNumber4(self, nums):
        return reduce(lambda x, y: x ^ y, nums)
        
    def singleNumber(self, nums):
        return reduce(operator.xor, nums)
</p>


### Easy Java solution (tell you why using bitwise XOR)
- Author: Nkeys
- Creation Date: Fri Aug 21 2015 22:02:12 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 15:34:59 GMT+0800 (Singapore Standard Time)

<p>
  
we use bitwise XOR to solve this problem : 

first , we have to know the bitwise XOR in java

 1. **0 ^ N = N**
 2. **N ^ N = 0**


So..... if N is the single number

N1 ^ N1 ^ N2 ^ N2 ^..............^ Nx ^ Nx ^ N  

  = (N1^N1) ^ (N2^N2) ^..............^ (Nx^Nx) ^ N

  = 0 ^ 0 ^ ..........^ 0 ^ N

  = N  


  
 



    public int singleNumber(int[] nums) {
        int ans =0;
        
        int len = nums.length;
        for(int i=0;i!=len;i++)
            ans ^= nums[i];
        
        return ans;
        
    }
</p>


