---
title: "Maximum Product of Two Elements in an Array"
weight: 1347
#id: "maximum-product-of-two-elements-in-an-array"
---
## Description
<div class="description">
Given the array of integers <code>nums</code>, you will choose two different indices <code>i</code> and <code>j</code> of that array. <em>Return the maximum value of</em> <code>(nums[i]-1)*(nums[j]-1)</code>.
<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,4,5,2]
<strong>Output:</strong> 12 
<strong>Explanation:</strong> If you choose the indices i=1 and j=2 (indexed from 0), you will get the maximum value, that is, (nums[1]-1)*(nums[2]-1) = (4-1)*(5-1) = 3*4 = 12. 
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,5,4,5]
<strong>Output:</strong> 16
<strong>Explanation:</strong> Choosing the indices i=1 and j=3 (indexed from 0), you will get the maximum value of (5-1)*(5-1) = 16.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,7]
<strong>Output:</strong> 12
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= nums.length &lt;= 500</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 10^3</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Samsung - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ Biggest and Second Biggest
- Author: votrubac
- Creation Date: Sun May 31 2020 12:07:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 31 2020 12:23:21 GMT+0800 (Singapore Standard Time)

<p>
Just one linear scan to find the biggest `m1`, and the second biggest `m2` numbers.

```cpp
int maxProduct(vector<int>& nums) {
	auto m1 = 0, m2 = 0;
	for (auto n: nums) {
		if (n > m1)
            m2 = exchange(m1, n);
		else
			m2 = max(m2, n);
	}
	return (m1 - 1) * (m2 - 1);
}
```
</p>


### [Java/Python 3] Find the max 2 numbers.
- Author: rock
- Creation Date: Sun May 31 2020 12:02:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 31 2020 12:02:24 GMT+0800 (Singapore Standard Time)

<p>


```java
    public int maxProduct(int[] nums) {
        int m = Integer.MIN_VALUE, n = m;
        for (int num: nums) {
            if (num >= m) {
                n = m;
                m = num;
            }else if (num > n) {
                n = num;
            }
        } 
        return (m - 1) * (n - 1);
    }
```

```python
    def maxProduct(self, nums: List[int]) -> int:
        m = n = -math.inf
        for num in nums:
            if num >= m:
                n = m
                m = num
            elif num > n:
                n = num
        return (m - 1) * (n - 1)
```
</p>


### Accepted Java O(N), O(1) (beats 100%)
- Author: priyankad93
- Creation Date: Sun May 31 2020 12:02:34 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 08 2020 11:45:20 GMT+0800 (Singapore Standard Time)

<p>

Time : O(N)
Space : O(1)
Iterate array
    1.) if cur val is more than max1, put curr val in max1 and max1 val in max2
    2.) else if curr val is more than max2, put curr val in max2

```
class Solution {
    public int maxProduct(int[] nums) {
        int max1 = 0;
        int max2 = 0;
        for(int i:nums){
            if(i>max1){
                max2 = max1;
                max1 = i;
            }
            else if(i>max2){
                max2 = i;
            }
        }
        return (max1-1)*(max2-1);
    }
}
```
</p>


