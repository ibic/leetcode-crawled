---
title: "Longest Word in Dictionary"
weight: 641
#id: "longest-word-in-dictionary"
---
## Description
<div class="description">
<p>Given a list of strings <code>words</code> representing an English Dictionary, find the longest word in <code>words</code> that can be built one character at a time by other words in <code>words</code>.  If there is more than one possible answer, return the longest word with the smallest lexicographical order.</p>  If there is no answer, return the empty string.

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> 
words = ["w","wo","wor","worl", "world"]
<b>Output:</b> "world"
<b>Explanation:</b> 
The word "world" can be built one character at a time by "w", "wo", "wor", and "worl".
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> 
words = ["a", "banana", "app", "appl", "ap", "apply", "apple"]
<b>Output:</b> "apple"
<b>Explanation:</b> 
Both "apply" and "apple" can be built from other words in the dictionary. However, "apple" is lexicographically smaller than "apply".
</pre>
</p>

<p><b>Note:</b>
<li>All the strings in the input will only contain lowercase letters.</li>
<li>The length of <code>words</code> will be in the range <code>[1, 1000]</code>.</li>
<li>The length of <code>words[i]</code> will be in the range <code>[1, 30]</code>.</li>
</p>
</div>

## Tags
- Hash Table (hash-table)
- Trie (trie)

## Companies
- Goldman Sachs - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Pinterest - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Brute Force [Accepted]

**Intuition**

For each word, check if all prefixes word[:k] are present.  We can use a `Set` structure to check this quickly.

**Algorithm**

Whenever our found word would be superior, we check if all it's prefixes are present, then replace our answer.

Alternatively, we could have sorted the words beforehand, so that we know the word we are considering would be the answer if all it's prefixes are present.

**Python**
```python
class Solution(object):
    def longestWord(self, words):
    ans = ""
    wordset = set(words)
    for word in words:
        if len(word) > len(ans) or len(word) == len(ans) and word < ans:
            if all(word[:k] in wordset for k in xrange(1, len(word))):
                ans = word

    return ans
```

*Alternate Implementation*
```python
class Solution(object):
    def longestWord(self, words):
        wordset = set(words)
        words.sort(key = lambda c: (-len(c), c))
        for word in words:
            if all(word[:k] in wordset for k in xrange(1, len(word))):
                return word

        return ""
```

**Java**

```java
class Solution {
    public String longestWord(String[] words) {
        String ans = "";
        Set<String> wordset = new HashSet();
        for (String word: words) wordset.add(word);
        for (String word: words) {
            if (word.length() > ans.length() ||
                    word.length() == ans.length() && word.compareTo(ans) < 0) {
                boolean good = true;
                for (int k = 1; k < word.length(); ++k) {
                    if (!wordset.contains(word.substring(0, k))) {
                        good = false;
                        break;
                    }
                }
                if (good) ans = word;
            }    
        }
        return ans;
    }
}
```

*Alternate Implementation*
```java
class Solution {
    public String longestWord(String[] words) {
        Set<String> wordset = new HashSet();
        for (String word: words) wordset.add(word);
        Arrays.sort(words, (a, b) -> a.length() == b.length()
                    ? a.compareTo(b) : b.length() - a.length());
        for (String word: words) {
            boolean good = true;
            for (int k = 1; k < word.length(); ++k) {
                if (!wordset.contains(word.substring(0, k))) {
                    good = false;
                    break;
                }
            }
            if (good) return word;
        }

        return "";
    }
}
```

**Complexity Analysis**

* Time complexity : $$O(\sum w_i^2)$$, where $$w_i$$ is the length of `words[i]`.  Checking whether all prefixes of `words[i]` are in the set is $$O(\sum w_i^2)$$.

* Space complexity : $$O(\sum w_i^2)$$ to create the substrings.

---
#### Approach #2: Trie + Depth-First Search [Accepted]

**Intuition**

As prefixes of strings are involved, this is usually a natural fit for a *trie* (a prefix tree.)

**Algorithm**

Put every word in a trie, then depth-first-search from the start of the trie, only searching nodes that ended a word.  Every node found (except the root, which is a special case) then represents a word with all it's prefixes present.  We take the best such word.

In Python, we showcase a method using defaultdict, while in Java, we stick to a more general object-oriented approach.

**Python**
```python
class Solution(object):
    def longestWord(self, words):
        Trie = lambda: collections.defaultdict(Trie)
        trie = Trie()
        END = True

        for i, word in enumerate(words):
            reduce(dict.__getitem__, word, trie)[END] = i

        stack = trie.values()
        ans = ""
        while stack:
            cur = stack.pop()
            if END in cur:
                word = words[cur[END]]
                if len(word) > len(ans) or len(word) == len(ans) and word < ans:
                    ans = word
                stack.extend([cur[letter] for letter in cur if letter != END])

        return ans
```

**Java**
```java
class Solution {
    public String longestWord(String[] words) {
        Trie trie = new Trie();
        int index = 0;
        for (String word: words) {
            trie.insert(word, ++index); //indexed by 1
        }
        trie.words = words;
        return trie.dfs();
    }
}
class Node {
    char c;
    HashMap<Character, Node> children = new HashMap();
    int end;
    public Node(char c){
        this.c = c;
    }
}

class Trie {
    Node root;
    String[] words;
    public Trie() {
        root = new Node('0');
    }

    public void insert(String word, int index) {
        Node cur = root;
        for (char c: word.toCharArray()) {
            cur.children.putIfAbsent(c, new Node(c));
            cur = cur.children.get(c);
        }
        cur.end = index;
    }

    public String dfs() {
        String ans = "";
        Stack<Node> stack = new Stack();
        stack.push(root);
        while (!stack.empty()) {
            Node node = stack.pop();
            if (node.end > 0 || node == root) {
                if (node != root) {
                    String word = words[node.end - 1];
                    if (word.length() > ans.length() ||
                            word.length() == ans.length() && word.compareTo(ans) < 0) {
                        ans = word;
                    }
                }
                for (Node nei: node.children.values()) {
                    stack.push(nei);
                }
            }
        }
        return ans;
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(\sum w_i)$$, where $$w_i$$ is the length of `words[i]`.  This is the complexity to build the trie and to search it.

  If we used a BFS instead of a DFS, and ordered the children in an array, we could drop the need to check whether the candidate word at each node is better than the answer, by forcing that the last node visited will be the best answer.

* Space Complexity: $$O(\sum w_i)$$, the space used by our trie.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] Clean Code
- Author: alexander
- Creation Date: Sun Nov 05 2017 11:16:41 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 03:47:12 GMT+0800 (Singapore Standard Time)

<p>
1. Sort the words alphabetically, therefore shorter words always comes before longer words;
2. Along the sorted list, populate the words that can be built;
3. Any prefix of a word must comes before that  word.

**Java**
```
class Solution {
    public String longestWord(String[] words) {
        Arrays.sort(words);
        Set<String> built = new HashSet<String>();
        String res = "";
        for (String w : words) {
            if (w.length() == 1 || built.contains(w.substring(0, w.length() - 1))) {
                res = w.length() > res.length() ? w : res;
                built.add(w);
            }
        }
        return res;
    }
}
```
**C++**
```
class Solution {
public:
    string longestWord(vector<string>& words) {
        sort(words.begin(), words.end());
        unordered_set<string> built;
        string res;
        for (string w : words) {
            if (w.size() == 1 || built.count(w.substr(0, w.size() - 1))) {
                res = w.size() > res.size() ? w : res;
                built.insert(w);
            }
        }
        return res;
    }
};
```
</p>


### Python Elegant and Extremely Easy to Understand
- Author: yangshun
- Creation Date: Sun Nov 05 2017 11:52:28 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 01:14:12 GMT+0800 (Singapore Standard Time)

<p>
Firstly sort the words to ensure that we iterate in a lexicographical fashion. Also, maintain a set of seen words to check whether there is a "path" to the current word we are iterating through. We can lengthen the path when we see that there is a previous word that meets the criteria. Lastly, in every iteration where there is a path lengthening, check to see if we can update the `longest_word`.

*By Yangshun*

```
class Solution(object):
    def longestWord(self, words):
        words.sort()
        words_set, longest_word = set(['']), ''
        for word in words:
            if word[:-1] in words_set:
                words_set.add(word)
                if len(word) > len(longest_word):
                    longest_word = word
        return longest_word
```
</p>


### [JAVA 16ms (99%) @ 20180108] Trie+DFS: clean, easy, explained and illustrated
- Author: vegito2002
- Creation Date: Tue Jan 09 2018 03:23:59 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 12:08:46 GMT+0800 (Singapore Standard Time)

<p>
Build a trie in the normal way, then do a dfs to find the longest **continuous** downward path from the root. This is not a particularly hard question in the context of trie, the point of this solution is to present a generic way of trie building and inserting that can be easily adapted to similar questions. Code:

```java
class Solution {
    public String longestWord(String[] words) {
        TrieNode root = new TrieNode ();
        root.word = "-";
        for (String word : words)
            root.insert (word);
        return dfs (root, "");
    }

    String dfs (TrieNode node, String accum) {
        if (node == null || node.word.length () == 0)
            return accum;
        String res = "";
        if (!node.word.equals ("-"))
            accum = node.word;
        for (TrieNode child : node.links) {
            String curRes = dfs (child, accum);
            if (curRes.length () > res.length () || (curRes.length () == res.length () && curRes.compareTo (res) < 0))
                res = curRes;
        }
        return res;
    }

    /* Hand write this class every time you need to so you can remember well */
    static class TrieNode {
        String word = "";
        TrieNode[] links = new TrieNode[26];

        void insert (String s) {
            char[] chs = s.toCharArray ();
            TrieNode curNode = this;
            for (int i = 0; i < chs.length; i++) {
                int index = chs[i] - 'a';
                if (curNode.links[index] == null)
                    curNode.links[index] = new TrieNode ();
                curNode = curNode.links[index];
            }
            curNode.word = s;
        }
    }
}
```

A typical trie for the list of `"ab", "ac"`:
<img src="http://i63.tinypic.com/2jbtwjl.png" width = "300"/>
</p>


