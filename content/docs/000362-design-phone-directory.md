---
title: "Design Phone Directory"
weight: 362
#id: "design-phone-directory"
---
## Description
<div class="description">
<p>Design a Phone Directory which supports the following operations:</p>

<p>&nbsp;</p>

<ol>
	<li><code>get</code>: Provide a number which is not assigned to anyone.</li>
	<li><code>check</code>: Check if a number is available or not.</li>
	<li><code>release</code>: Recycle or release a number.</li>
</ol>

<p>&nbsp;</p>

<p><b>Example:</b></p>

<pre>
// Init a phone directory containing a total of 3 numbers: 0, 1, and 2.
PhoneDirectory directory = new PhoneDirectory(3);

// It can return any available phone number. Here we assume it returns 0.
directory.get();

// Assume it returns 1.
directory.get();

// The number 2 is available, so return true.
directory.check(2);

// It returns 2, the only number that is left.
directory.get();

// The number 2 is no longer available, so return false.
directory.check(2);

// Release number 2 back to the pool.
directory.release(2);

// Number 2 is available again, return true.
directory.check(2);
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;=&nbsp;maxNumbers &lt;= 10^4</code></li>
	<li><code>0 &lt;= number &lt; maxNumbers</code></li>
	<li>The total number of call of the methods is between <code>[0 - 20000]</code></li>
</ul>

</div>

## Tags
- Linked List (linked-list)
- Design (design)

## Companies
- Dropbox - 7 (taggedByAdmin: false)
- Google - 6 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(1) time, o(n) space, single Array, 99ms beats 100%
- Author: hueyhy
- Creation Date: Mon Apr 09 2018 06:56:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 08:21:55 GMT+0800 (Singapore Standard Time)

<p>
use an array `next[]` to store the next available number.
when a number `k` is issued, move pointer `pos = next[k]` to the next available position. set `next[k]=-1` and 
when a number is recycled, sipmly move pointer from `pos`  to the recycled number, and change the recycled number\'s "next" point to `pos`. 

```
class PhoneDirectory {

    /** Initialize your data structure here
        @param maxNumbers - The maximum numbers that can be stored in the phone directory. */
    int[] next;
    int pos;
    public PhoneDirectory(int maxNumbers) {
        next = new int[maxNumbers];
        for (int i=0; i<maxNumbers; ++i){
            next[i] = (i+1)%maxNumbers;
        }
        pos=0;
    }
    
    /** Provide a number which is not assigned to anyone.
        @return - Return an available number. Return -1 if none is available. */
    public int get() {
        if (next[pos]==-1) return -1;
        int ret = pos;
        pos = next[pos];
        next[ret]=-1;
        return ret;
    }
    
    /** Check if a number is available or not. */
    public boolean check(int number) {
        return next[number]!=-1;
    }
    
    /** Recycle or release a number. */
    public void release(int number) {
        if (next[number]!=-1) return;
        next[number] = pos;
        pos = number;
    }
}
```
</p>


### Java AC solution using queue and set
- Author: mylzsd
- Creation Date: Wed Aug 03 2016 12:21:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 23:53:31 GMT+0800 (Singapore Standard Time)

<p>
```
    Set<Integer> used = new HashSet<Integer>();
    Queue<Integer> available = new LinkedList<Integer>();
    int max;
    public PhoneDirectory(int maxNumbers) {
        max = maxNumbers;
        for (int i = 0; i < maxNumbers; i++) {
            available.offer(i);
        }
    }
    
    public int get() {
        Integer ret = available.poll();
        if (ret == null) {
            return -1;
        }
        used.add(ret);
        return ret;
    }
    
    public boolean check(int number) {
        if (number >= max || number < 0) {
            return false;
        }
        return !used.contains(number);
    }
    
    public void release(int number) {
        if (used.remove(number)) {
            available.offer(number);
        }
    }
```
</p>


### 4-line Python solution using only 1 set and no queue
- Author: agave
- Creation Date: Wed Aug 03 2016 15:42:02 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 24 2018 11:23:19 GMT+0800 (Singapore Standard Time)

<p>
```
   def __init__(self, maxNumbers):
        self.available = set(range(maxNumbers))

    def get(self):
        return self.available.pop() if self.available else -1

    def check(self, number):
        return number in self.available

    def release(self, number):
        self.available.add(number)
</p>


