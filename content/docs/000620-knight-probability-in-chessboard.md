---
title: "Knight Probability in Chessboard"
weight: 620
#id: "knight-probability-in-chessboard"
---
## Description
<div class="description">
<p>On an <code>N</code>x<code>N</code> chessboard, a knight starts at the <code>r</code>-th row and <code>c</code>-th column and attempts to make exactly <code>K</code> moves. The rows and columns are 0 indexed, so the top-left square is <code>(0, 0)</code>, and the bottom-right square is <code>(N-1, N-1)</code>.</p>

<p>A chess knight has 8 possible moves it can make, as illustrated below. Each move is two squares in a cardinal direction, then one square in an orthogonal direction.</p>

<p>&nbsp;</p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/12/knight.png" style="width: 200px; height: 200px;" /></p>

<p>&nbsp;</p>

<p>Each time the knight is to move, it chooses one of eight possible moves uniformly at random (even if the piece would go off the chessboard) and moves there.</p>

<p>The knight continues moving until it has made exactly <code>K</code> moves or has moved off the chessboard. Return the probability that the knight remains on the board after it has stopped moving.</p>

<p>&nbsp;</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b> 3, 2, 0, 0
<b>Output:</b> 0.0625
<b>Explanation:</b> There are two moves (to (1,2), (2,1)) that will keep the knight on the board.
From each of those positions, there are also two moves that will keep the knight on the board.
The total probability the knight stays on the board is 0.0625.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ul>
	<li><code>N</code> will be between 1 and 25.</li>
	<li><code>K</code> will be between 0 and 100.</li>
	<li>The knight always initially starts on the board.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 4 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Goldman Sachs - 3 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Dynamic Programming [Accepted]

**Intuition and Algorithm**

Let `f[r][c][steps]` be the probability of being on square `(r, c)` after `steps` steps.  Based on how a knight moves, we have the following recursion:

$$f[r][c][steps] = \sum_{dr, dc} f[r+dr][c+dc][steps-1] / 8.0$$

where the sum is taken over the eight $$(dr, dc)$$ pairs $$(2, 1),$$ $$(2, -1),$$ $$(-2, 1),$$ $$(-2, -1),$$ $$(1, 2),$$ $$(1, -2),$$ $$(-1, 2),$$ $$(-1, -2)$$.

Instead of using a three-dimensional array `f`, we will use two two-dimensional ones `dp` and `dp2`, storing the result of the two most recent layers we are working on.  `dp2` will represent `f[][][steps]`, and `dp` will represent `f[][][steps-1]`.

<iframe src="https://leetcode.com/playground/VTNPLt6H/shared" frameBorder="0" name="VTNPLt6H" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2 K)$$ where $$N, K$$ are defined as in the problem.  We do $$O(1)$$ work on each layer `dp` of $$N^2$$ elements, and there are $$K$$ layers considered.

* Space Complexity: $$O(N^2)$$, the size of `dp` and `dp2`.

---
#### Approach #2: Matrix Exponentiation [Accepted]

**Intuition**

The recurrence expressed in *Approach #1* expressed states that transitioned to a linear combination of other states.  Any time this happens, we can represent the entire transition as a matrix of those linear combinations.  Then, the $$n$$-th power of this matrix represents the transition of $$n$$ moves, and thus we can reduce the problem to a problem of matrix exponentiation.

**Algorithm**

First, there is a lot of symmetry on the board that we can exploit.  Naively, there are $$N^2$$ possible states the knight can be in (assuming it is on the board).  Because of symmetry through the horizontal, vertical, and diagonal axes, we can assume that the knight is in the top-left quadrant of the board, and that the column number is equal to or larger than the row number.  For any square, the square that is found by reflecting about these axes to satisfy these conditions will be the *canonical index* of that square.

This will reduce the number of states from $$N^2$$ to approximately $$\frac{N^2}{8}$$, which makes the following (cubic) matrix exponentiation on this $$O(\frac{N^2}{8}) \times O(\frac{N^2}{8})$$ matrix approximately $$8^3$$ times faster.

Now, if we know that every state becomes some linear combination of states after one move, then let's write a transition matrix $$\mathcal{T}$$ of them, where the $$i$$-th row of $$\mathcal{T}$$ represents the linear combination of states that the $$i$$-th state goes to.  Then, $$\mathcal{T}^n$$ represents a transition of $$n$$ moves, for which we want the sum of the $$i$$-th row, where $$i$$ is the index of the starting square.

<iframe src="https://leetcode.com/playground/ARu5yUUd/shared" frameBorder="0" name="ARu5yUUd" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^6 \log(K))$$ where $$N, K$$ are defined as in the problem.  There are approximately $$\frac{N^2}{8}$$ canonical states, which makes our matrix multiplication $$O(N^6)$$.  To find the $$K$$-th power of this matrix, we make $$O(\log(K))$$ matrix multiplications.

* Space Complexity: $$O(N^4)$$.  The matrix has approximately $$\frac{N^4}{64}$$ elements.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Evolve from recursive to dp,beats 94%
- Author: kevincongcc
- Creation Date: Mon Feb 05 2018 10:18:09 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 23:40:21 GMT+0800 (Singapore Standard Time)

<p>
recursive version(TLE):
```
class Solution {
    private int[][]dir = new int[][]{{-2,-1},{-1,-2},{1,-2},{2,-1},{2,1},{1,2},{-1,2},{-2,1}};
    public double knightProbability(int N, int K, int r, int c) {
        return find(N,K,r,c);
    }
    public double find(int N,int K,int r,int c){
        if(r < 0 || r > N - 1 || c < 0 || c > N - 1) return 0;
        if(K == 0)  return 1;
        double rate = 0;
        for(int i = 0;i < dir.length;i++){
            rate += 0.125 * find(N,K - 1,r + dir[i][0],c + dir[i][1]);
        }
        return rate;
    }
}
```

dp version:
```
class Solution {
    private int[][]dir = new int[][]{{-2,-1},{-1,-2},{1,-2},{2,-1},{2,1},{1,2},{-1,2},{-2,1}};
    private double[][][] dp;
    public double knightProbability(int N, int K, int r, int c) {
        dp = new double[N][N][K + 1];
        return find(N,K,r,c);
    }
    public double find(int N,int K,int r,int c){
        if(r < 0 || r > N - 1 || c < 0 || c > N - 1) return 0;
        if(K == 0)  return 1;
        if(dp[r][c][K] != 0) return dp[r][c][K];
        double rate = 0;
        for(int i = 0;i < dir.length;i++)   rate += 0.125 * find(N,K - 1,r + dir[i][0],c + dir[i][1]);
        dp[r][c][K] = rate;
        return rate;
    }
}
```
</p>


### My accepted DP solution
- Author: szlghl1
- Creation Date: Sun Oct 01 2017 11:02:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 11:36:15 GMT+0800 (Singapore Standard Time)

<p>
```
int[][] moves = {{1, 2}, {1, -2}, {2, 1}, {2, -1}, {-1, 2}, {-1, -2}, {-2, 1}, {-2, -1}};
public double knightProbability(int N, int K, int r, int c) {
    int len = N;
    double dp0[][] = new double[len][len];
    for(double[] row : dp0) Arrays.fill(row, 1);
    for(int l = 0; l < K; l++) {
        double[][] dp1 = new double[len][len];
        for(int i = 0; i < len; i++) {
            for(int j = 0; j < len; j++) {
                for(int[] move : moves) {
                    int row = i + move[0];
                    int col = j + move[1];
                    if(isLegal(row, col, len)) dp1[i][j] += dp0[row][col];
                }
            }
        }
        dp0 = dp1;
    }
    return dp0[r][c] / Math.pow(8, K); 
}
private boolean isLegal(int r, int c, int len) {
    return r >= 0 && r < len && c >= 0 && c < len;
}
```
</p>


### Simple Java DP solution with explanation
- Author: seanmsha
- Creation Date: Fri Aug 24 2018 04:41:29 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 06:20:43 GMT+0800 (Singapore Standard Time)

<p>
The first solution uses O(k*n^2) space for simplicity:
At every k and position i j we store the probability that the knight landed at position i j at step k.  We know that this probability is the sum of probabilities of the 8 directions in the previous step k-1 because in the previous step all 8 of those knight\'s have a chance of moving here.  For example since one of the directions is 2, 1 we take the current i-2 and j-1 and add that probability/8.0 (because if the knight is currently at i-2, j-1 the chance is only /8.0 that he\'ll choose this direction out of his 7 other choices).  
We initialize the r , c index of the k==0 board to 1, because at step 0, we already have the knight at position r, c so the chance it lands there in 0 steps is 100%.
The result is the sum of probabilities in all areas of the board in the Kth index Board.
```
class Solution {
    int [][] direction =new int[][]{{2,1},{-2,1},{2,-1},{-2,-1},{1,2},{1,-2},{-1,2},{-1,-2}};
   
    public double knightProbability(int N, int K, int r, int c) {
        double [][][] ways = new double[K+1][N][N];
        ways[0][r][c]=1;
        for(int k=1; k<=K;++k){
            for(int i=0; i<N;++i){
                for(int j=0; j<N;++j){
                    for(int [] dir: direction){
                        int oldR = i-dir[0];
                        int oldC = j-dir[1];
                        if(oldR>=0 && oldC>=0 && oldR<N && oldC<N){
                            ways[k][i][j]+=(ways[k-1][oldR][oldC]/8.0);
                        }
                    }
                }
            }
        }
        double res = 0;
        for(int i=0; i<N;++i){
            for(int j=0; j<N;++j){
                res+=ways[K][i][j];
            }
        }
        return res;
    }
}
```
because we only use the k and k-1 at every step we can just make the new Kth board be a variable and set the previous one to a previous variable, reducing the space to O(n^2):
```
class Solution {
    int [][] direction =new int[][]{{2,1},{-2,1},{2,-1},{-2,-1},{1,2},{1,-2},{-1,2},{-1,-2}};
   
    public double knightProbability(int N, int K, int r, int c) {
        double [][] prevWays = new double[N][N];
        prevWays[r][c]=1;
        double res = 0;
        for(int k=1; k<=K;++k){
            double [][] ways = new double[N][N];
            for(int i=0; i<N;++i){
                for(int j=0; j<N;++j){
                    for(int [] dir: direction){
                        int oldR = i-dir[0];
                        int oldC = j-dir[1];
                        if(oldR>=0 && oldC>=0 && oldR<N && oldC<N){
                            ways[i][j]+=(prevWays[oldR][oldC]/8.0);
                        }
                    }
                }
            }
            prevWays=ways;
        }
        for(int i=0; i<N;++i){
            for(int j=0; j<N;++j){
                res+=prevWays[i][j];
            }
        }
        return res;
    }
}
```
</p>


