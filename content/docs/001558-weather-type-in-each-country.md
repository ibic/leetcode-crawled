---
title: "Weather Type in Each Country"
weight: 1558
#id: "weather-type-in-each-country"
---
## Description
<div class="description">
<p>Table: <code>Countries</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| country_id    | int     |
| country_name  | varchar |
+---------------+---------+
country_id is the primary key for this table.
Each row of this table contains the ID and the name of one country.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Weather</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| country_id    | int     |
| weather_state | varchar |
| day           | date    |
+---------------+---------+
(country_id, day) is the primary key for this table.
Each row of this table indicates the weather state in a country for one day.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the type of weather in each country for November 2019.</p>

<p>The type of weather is <strong>Cold</strong> if the average <code>weather_state</code> is less than or equal 15, <strong>Hot</strong> if the average <code>weather_state</code> is greater than or equal 25 and <strong>Warm</strong> otherwise.</p>

<p>Return result table in any order.</p>

<p>The query result format is in the following example:</p>

<pre>
Countries table:
+------------+--------------+
| country_id | country_name |
+------------+--------------+
| 2          | USA          |
| 3          | Australia    |
| 7          | Peru         |
| 5          | China        |
| 8          | Morocco      |
| 9          | Spain        |
+------------+--------------+
Weather table:
+------------+---------------+------------+
| country_id | weather_state | day        |
+------------+---------------+------------+
| 2          | 15            | 2019-11-01 |
| 2          | 12            | 2019-10-28 |
| 2          | 12            | 2019-10-27 |
| 3          | -2            | 2019-11-10 |
| 3          | 0             | 2019-11-11 |
| 3          | 3             | 2019-11-12 |
| 5          | 16            | 2019-11-07 |
| 5          | 18            | 2019-11-09 |
| 5          | 21            | 2019-11-23 |
| 7          | 25            | 2019-11-28 |
| 7          | 22            | 2019-12-01 |
| 7          | 20            | 2019-12-02 |
| 8          | 25            | 2019-11-05 |
| 8          | 27            | 2019-11-15 |
| 8          | 31            | 2019-11-25 |
| 9          | 7             | 2019-10-23 |
| 9          | 3             | 2019-12-23 |
+------------+---------------+------------+
Result table:
+--------------+--------------+
| country_name | weather_type |
+--------------+--------------+
| USA          | Cold         |
| Austraila    | Cold         |
| Peru         | Hot          |
| China        | Warm         |
| Morocco      | Hot          |
+--------------+--------------+
Average weather_state in USA in November is (15) / 1 = 15 so weather type is Cold.
Average weather_state in Austraila in November is (-2 + 0 + 3) / 3 = 0.333 so weather type is Cold.
Average weather_state in Peru in November is (25) / 1 = 25 so weather type is Hot.
Average weather_state in China in November is (16 + 18 + 21) / 3 = 18.333 so weather type is Warm.
Average weather_state in Morocco in November is (25 + 27 + 31) / 3 = 27.667 so weather type is Hot.
We know nothing about average weather_state in Spain in November so we don&#39;t include it in the result table. 
</pre>
</div>

## Tags


## Companies
- Point72 - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Need to take care of decimal points for SQL Server solution when using AVG
- Author: liurenjie
- Creation Date: Thu Dec 19 2019 04:10:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Dec 21 2019 10:24:33 GMT+0800 (Singapore Standard Time)

<p>
The weather_state is given as varchar datatype, when SQL Server evaluates avg on varchar, it implicitly converts the data to Integer. Avg on Integer will round down to the nearest Integer, like using the floor() function.

Below is one example - 

```
create table test (id int) ;
insert into test values (15);
insert into test values (14);
select avg(id) avg_id from test;
```

Same test case will result in different for SQL Server and MySQL.

SQL Server shows 14, while MySQL shows 14.5

Apart from the decimal points could mess up with the AVG, other parts are mundane.

```

/* Write your T-SQL query statement below */
select country_name,
weather_type = case when AVG(weather_state*1.0)<=15.0 then \'Cold\'
					when AVG(weather_state*1.0)>=25.0 then \'Hot\'
					else \'Warm\'
			   end
from Countries c inner join Weather  w
on c.country_id  = w.country_id
where [day] between \'2019-11-01\' and \'2019-11-30\'
group by country_name
```
</p>


### Simple MySQL solution beats 100% using CASE WHEN and JOIN
- Author: eminem18753
- Creation Date: Thu Dec 19 2019 10:09:52 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Dec 19 2019 10:12:07 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below
SELECT a.country_name,
CASE WHEN AVG(weather_state)<=15 THEN "Cold"
WHEN AVG(weather_state)>=25 THEN "Hot"
ELSE "Warm" END as weather_type FROM Countries as a
JOIN Weather as b
ON a.country_id=b.country_id
WHERE b.day BETWEEN "2019-11-01" AND "2019-11-30"
GROUP BY a.country_id;
```
</p>


### [MySQL] a straightforward solution
- Author: ye15
- Creation Date: Thu Feb 06 2020 00:40:09 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Feb 06 2020 00:40:09 GMT+0800 (Singapore Standard Time)

<p>


```
SELECT 
    country_name, 
    CASE WHEN AVG(weather_state) <= 15 THEN "Cold"
         WHEN AVG(weather_state) >= 25 THEN "Hot"
    ELSE "Warm" END AS weather_type
FROM Weather JOIN Countries USING (country_id)
WHERE day BETWEEN DATE("2019-11-01") AND DATE("2019-11-30")
GROUP BY country_id; 
```
</p>


