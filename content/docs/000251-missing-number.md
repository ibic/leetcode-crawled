---
title: "Missing Number"
weight: 251
#id: "missing-number"
---
## Description
<div class="description">
<p>Given an array <code>nums</code> containing <code>n</code> distinct numbers in the range <code>[0, n]</code>, return <em>the only number in the range that is missing from the array.</em></p>

<p><b>Follow up:</b> Could you implement a solution using only <code>O(1)</code> extra space complexity and <code>O(n)</code> runtime complexity?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,0,1]
<strong>Output:</strong> 2
<b>Explanation</b><strong>:</strong> n = 3 since there are 3 numbers, so all numbers are in the range [0,3]. 2 is the missing number in the range since it does not appear in nums.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,1]
<strong>Output:</strong> 2
<b>Explanation</b><strong>:</strong> n = 2 since there are 2 numbers, so all numbers are in the range [0,2]. 2 is the missing number in the range since it does not appear in nums.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [9,6,4,2,3,5,7,0,1]
<strong>Output:</strong> 8
<b>Explanation</b><strong>:</strong> n = 9 since there are 9 numbers, so all numbers are in the range [0,9]. 8 is the missing number in the range since it does not appear in nums.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [0]
<strong>Output:</strong> 1
<b>Explanation</b><strong>:</strong> n = 1 since there is 1 number, so all numbers are in the range [0,1]. 1 is the missing number in the range since it does not appear in nums.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == nums.length</code></li>
	<li><code>1 &lt;= n &lt;= 10<sup>4</sup></code></li>
	<li><code>0 &lt;= nums[i] &lt;= n</code></li>
	<li>All the numbers of <code>nums</code> are <strong>unique</strong>.</li>
</ul>

</div>

## Tags
- Array (array)
- Math (math)
- Bit Manipulation (bit-manipulation)

## Companies
- Amazon - 9 (taggedByAdmin: false)
- Capital One - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: true)
- Apple - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: true)
- Bloomberg - 3 (taggedByAdmin: true)
- JPMorgan - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Google - 7 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Arista Networks - 3 (taggedByAdmin: false)
- Zillow - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- IBM - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1 Sorting [Accepted]

**Intuition**

If `nums` were in order, it would be easy to see which number is missing.

**Algorithm**

First, we sort `nums`. Then, we check the two special cases that can be
handled in constant time - ensuring that 0 is at the beginning and that $$n$$
is at the end. Given that those assumptions hold, the missing number must
somewhere between (but not including) 0 and $$n$$. To find it, we ensure that
the number we expect to be at each index is indeed there. Because we handled
the edge cases, this is simply the previous number plus 1. Thus, as soon as
we find an unexpected number, we can simply return the expected number.

<iframe src="https://leetcode.com/playground/BnbJDXiA/shared" frameBorder="0" width="100%" height="480" name="BnbJDXiA"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(nlgn)$$

    The only elements of the algorithm that have asymptotically nonconstant
    time complexity are the main `for` loop (which runs in $$\mathcal{O}(n)$$ time), and
    the `sort` invocation (which runs in $$\mathcal{O}(nlgn)$$ time for Python and Java).
    Therefore, the runtime is dominated by `sort`, and the entire runtime is
    $$\mathcal{O}(nlgn)$$.

* Space complexity : $$\mathcal{O}(1)$$ (or $$\mathcal{O}(n)$$)

    In the sample code, we sorted `nums` in place, allowing us to avoid
    allocating additional space. If modifying `nums` is forbidden, we can
    allocate an $$\mathcal{O}(n)$$ size copy and sort that instead.

---

#### Approach #2 HashSet [Accepted]

**Intuition**

A brute force method for solving this problem would be to simply check for
the presence of each number that we expect to be present. The naive
implementation might use a linear scan of the array to check for containment,
but we can use a `HashSet` to get constant time containment queries and
overall linear runtime.

**Algorithm**

This algorithm is almost identical to the brute force approach, except we
first insert each element of `nums` into a set, allowing us to later query
for containment in $$\mathcal{O}(1)$$ time.

<iframe src="https://leetcode.com/playground/RwNHwYQp/shared" frameBorder="0" width="100%" height="293" name="RwNHwYQp"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n)$$

    Because the set allows for $$\mathcal{O}(1)$$ containment queries, the main loop
    runs in $$\mathcal{O}(n)$$ time. Creating `num_set` costs $$\mathcal{O}(n)$$ time, as each set insertion
    runs in amortized $$\mathcal{O}(1)$$ time, so the overall runtime is $$\mathcal{O}(n + n) = \mathcal{O}(n)$$.

* Space complexity : $$\mathcal{O}(n)$$

    `nums` contains $$n-1$$ distinct elements, so it costs $$\mathcal{O}(n)$$ space to
    store a set containing all of them.

---

#### Approach #3 Bit Manipulation [Accepted]

**Intuition**

We can harness the fact that XOR is its own inverse to find the missing
element in linear time.

**Algorithm**

Because we know that `nums` contains $$n$$ numbers and that it is missing
exactly one number on the range $$[0..n-1]$$, we know that $$n$$ definitely
replaces the missing number in `nums`. Therefore, if we initialize an integer
to $$n$$ and XOR it with every index and value, we will be left with the
missing number. Consider the following example (the values have been sorted
for intuitive convenience, but need not be):

 <table>
  <tr>
    <th>Index</th>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
  </tr>
  <tr>
    <th>Value</th>
    <td>0</td>
    <td>1</td>
    <td>3</td>
    <td>4</td>
  </tr>
</table> 



$$
\begin{align}
    missing &= 4 \wedge (0 \wedge 0) \wedge (1 \wedge 1) \wedge (2 \wedge 3) \wedge (3 \wedge 4) \\
            &= (4 \wedge 4) \wedge (0 \wedge 0) \wedge (1 \wedge 1) \wedge (3 \wedge 3) \wedge 2 \\
            &= 0 \wedge 0 \wedge 0 \wedge 0 \wedge 2 \\ 
            &= 2
\end{align}
$$

<iframe src="https://leetcode.com/playground/rA2RM54n/shared" frameBorder="0" width="100%" height="208" name="rA2RM54n"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n)$$

    Assuming that XOR is a constant-time operation, this algorithm does
    constant work on $$n$$ iterations, so the runtime is overall linear.

* Space complexity : $$\mathcal{O}(1)$$

    This algorithm allocates only constant additional space.

---


#### Approach #4 Gauss' Formula [Accepted]

**Intuition**

One of the most well-known stories in mathematics is of a young Gauss, forced
to find the sum of the first 100 natural numbers by a lazy teacher. Rather
than add the numbers by hand, he deduced a [closed-form
expression](https://brilliant.org/wiki/sum-of-n-n2-or-n3/) for the sum, or so
the story goes. You can see the formula below:

$$
    \sum_{i=0}^{n}i = \frac{n(n+1)}{2}
$$

**Algorithm**

We can compute the sum of `nums` in linear time, and by Gauss' formula, we
can compute the sum of the first $$n$$ natural numbers in constant time. Therefore,
the number that is missing is simply the result of Gauss' formula minus the sum of `nums`,
as `nums` consists of the first $$n$$ natural numbers minus some number.

<iframe src="https://leetcode.com/playground/GrrQG25C/shared" frameBorder="0" width="100%" height="191" name="GrrQG25C"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n)$$

    Although Gauss' formula can be computed in $$\mathcal{O}(1)$$ time, summing `nums`
    costs us $$\mathcal{O}(n)$$ time, so the algorithm is overall linear. Because we have
    no information about _which_ number is missing, an adversary could always
    design an input for which any algorithm that examines fewer than $$n$$
    numbers fails. Therefore, this solution is asymptotically optimal.

* Space complexity : $$\mathcal{O}(1)$$

    This approach only pushes a few integers around, so it has constant
    memory usage.

## Accepted Submission (java)
```java
public class Solution {
    public int missingNumber(int[] nums) {
        int xor = 0;
        for (int i: nums) {
            xor ^= i;
        }

        for (int i = 0; i <= nums.length; i++) {
            xor ^= i;
        }

        return xor;
    }
}
```

## Top Discussions
### 4 Line Simple Java Bit Manipulate Solution with Explaination
- Author: mo10
- Creation Date: Thu Sep 17 2015 08:02:59 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 04:11:41 GMT+0800 (Singapore Standard Time)

<p>
The basic idea is to use XOR operation. We all know that a^b^b =a, which means two xor operations with the same number will eliminate the number and reveal the original number.
In this solution, I apply XOR operation to both the index and value of the array. In a complete array with no missing numbers, the index and value should be perfectly corresponding( nums[index] = index), so in a missing array, what left finally is the missing number.



    public int missingNumber(int[] nums) {

        int xor = 0, i = 0;
		for (i = 0; i < nums.length; i++) {
			xor = xor ^ i ^ nums[i];
		}

		return xor ^ i;
    }
</p>


### 3 different ideas: XOR, SUM, Binary Search. Java code
- Author: mingjun
- Creation Date: Sat Sep 05 2015 05:23:59 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 06:51:58 GMT+0800 (Singapore Standard Time)

<p>
1.XOR
-----

    public int missingNumber(int[] nums) { //xor
        int res = nums.length;
        for(int i=0; i<nums.length; i++){
            res ^= i;
            res ^= nums[i];
        }
        return res;
    }

2.SUM
-----

    public int missingNumber(int[] nums) { //sum
        int len = nums.length;
        int sum = (0+len)*(len+1)/2;
        for(int i=0; i<len; i++)
            sum-=nums[i];
        return sum;
    }

3.Binary Search
---------------

    public int missingNumber(int[] nums) { //binary search
        Arrays.sort(nums);
        int left = 0, right = nums.length, mid= (left + right)/2;
        while(left<right){
            mid = (left + right)/2;
            if(nums[mid]>mid) right = mid;
            else left = mid+1;
        }
        return left;
    }

Summary:
--------

If the array is in order, I prefer `Binary Search` method. Otherwise, the `XOR` method is better.
</p>


### C++ solution using bit manipulation
- Author: CodingKing
- Creation Date: Mon Aug 24 2015 09:41:45 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 13:30:16 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        int missingNumber(vector<int>& nums) {
            int result = nums.size();
            int i=0;
            
            for(int num:nums){
                result ^= num;
                result ^= i;
                i++;
            }
            
            return result;
        }
    };

There are several similar problems in the problem list.
</p>


