---
title: "Wiggle Subsequence"
weight: 359
#id: "wiggle-subsequence"
---
## Description
<div class="description">
<p>A sequence of numbers is called a <strong>wiggle sequence</strong> if the differences between successive numbers strictly alternate between positive and negative. The first difference (if one exists) may be either positive or negative. A sequence with fewer than two elements is trivially a wiggle sequence.</p>

<p>For example, <code>[1,7,4,9,2,5]</code> is a wiggle sequence because the differences <code>(6,-3,5,-7,3)</code> are alternately positive and negative. In contrast, <code>[1,4,7,2,5]</code> and <code>[1,7,4,5,5]</code> are not wiggle sequences, the first because its first two differences are positive and the second because its last difference is zero.</p>

<p>Given a sequence of integers, return the length of the longest subsequence that is a wiggle sequence. A subsequence is obtained by deleting some number of elements (eventually, also zero) from the original sequence, leaving the remaining elements in their original order.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,7,4,9,2,5]</span>
<strong>Output: </strong><span id="example-output-1">6
<strong>Explanation:</strong> </span>The entire sequence is a wiggle sequence.</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[1,17,5,10,13,15,10,5,16,8]</span>
<strong>Output: </strong><span id="example-output-2">7
</span><span id="example-output-1"><strong>Explanation: </strong></span>There are several subsequences that achieve this length. One is [1,17,10,13,10,16,8].</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[1,2,3,4,5,6,7,8,9]</span>
<strong>Output: </strong><span id="example-output-3">2</span></pre>

<p><b>Follow up:</b><br />
Can you do it in O(<i>n</i>) time?</p>
</div>
</div>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Greedy (greedy)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary

We need to find the length of the longest wiggle subsequence. A wiggle subsequence consists of a subsequence with numbers which appears in alternating ascending / descending order.


## Solution
#### Approach #1 Brute Force

Here, we can find the length of every possible wiggle subsequence and find the maximum length out of them. To implement this, we use a recursive function, $$\text{calculate}(\text{nums}, \text{index}, \text{isUp})$$ which takes the array $$\text{nums}$$, the $$\text{index}$$ from which we need to find the length of the longest wiggle subsequence, boolean variable $$\text{isUp}$$ to tell whether we need to find an increasing wiggle or decreasing wiggle respectively. If the function $$\text{calculate}$$ is called after an increasing wiggle, we need to find the next decreasing wiggle with the same function. If the function $$\text{calculate}$$ is called after a decreasing wiggle, we need to find the next increasing wiggle with the same function.


<iframe src="https://leetcode.com/playground/JXWefkVB/shared" frameBorder="0" name="JXWefkVB" width="100%" height="326"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n!)$$. $$\text{calculate}()$$ will be called maximum $$n!$$ times.
* Space complexity : $$O(n)$$. Recursion of depth $$n$$ is used.

---
#### Approach #2  Dynamic Programming 

**Algorithm**

To understand this approach, take two arrays for dp named $$up$$ and $$down$$.

Whenever we pick up any element of the array to be a part of the wiggle subsequence, that element could be a part of a rising wiggle or a falling wiggle depending upon which element we have taken prior to it.

$$up[i]$$ refers to the length of the longest wiggle subsequence obtained so far considering $$i^{th}$$ element as the last element of the wiggle subsequence and ending with a rising wiggle.

Similarly, $$down[i]$$ refers to the length of the longest wiggle subsequence obtained so far considering $$i^{th}$$ element as the last element of the wiggle subsequence and ending with a falling wiggle.

$$up[i]$$ will be updated every time we find a rising wiggle ending with the $$i^{th}$$ element. Now, to find $$up[i]$$, we need to consider the maximum out of all the previous wiggle subsequences ending with a falling wiggle i.e. $$down[j]$$, for every $$j&lt;i$$ and $$nums[i]&gt;nums[j]$$. Similarly, $$down[i]$$ will be updated.



<iframe src="https://leetcode.com/playground/5DeX9SiP/shared" frameBorder="0" name="5DeX9SiP" width="100%" height="360"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Loop inside a loop.
* Space complexity : $$O(n)$$. Two arrays of the same length are used for dp.

---
#### Approach #3 Linear Dynamic Programming 

**Algorithm**

Any element in the array could correspond to only one of the three possible states:

1. up position, it means $$nums[i] > nums[i-1]$$
2. down position, it means $$nums[i] < nums[i-1]$$
3. equals to position, $$nums[i] == nums[i-1]$$

The updates are done as:

If $$nums[i] > nums[i-1]$$, that means it wiggles up. The element before it must be a down position. So $$up[i] = down[i-1] + 1$$, $$down[i]$$ remains the same as $$down[i-1]$$.
If $$nums[i] < nums[i-1]$$, that means it wiggles down. The element before it must be a up position. So $$down[i] = up[i-1] + 1$$, $$up[i]$$ remains the same as $$up[i-1]$$.
If $$nums[i] == nums[i-1]$$, that means it will not change anything becaue it didn't wiggle at all. So both $$down[i]$$ and $$up[i]$$ remain the same as $$down[i-1]$$ and $$up[i-1]$$.

At the end, we can find the larger out of $$up[length-1]$$ and $$down[length-1]$$ to find the max. wiggle subsequence length, where $$length$$ refers to the number of elements in the given array.

The process can be illustrated with the following example:

<!--![Wiggle gif](https://leetcode.com/media/original_images/376_Wiggle_Subsequence.gif)-->
!?!../Documents/376_Wiggle.json:1000,563!?!

<iframe src="https://leetcode.com/playground/iKGkFpFG/shared" frameBorder="0" name="iKGkFpFG" width="100%" height="428"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Only one pass over the array length.
* Space complexity : $$O(n)$$. Two arrays of the same length are used for dp.

---

#### Approach #4 Space-Optimized Dynamic Programming 

**Algorithm**

This approach relies on the same concept as [Approach #3](https://leetcode.com/articles/wiggle-subsequence/#approach-3-linear-dynamic-programming-accepted). But we can observe that in the DP approach, for updating elements $$up[i]$$ and $$down[i]$$, we need only the elements $$up[i-1]$$ and $$down[i-1]$$. Thus, we can save space by not using the whole array, but only the last elements.


<iframe src="https://leetcode.com/playground/hUCEjR4D/shared" frameBorder="0" name="hUCEjR4D" width="100%" height="292"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Only one pass over the array length.
* Space complexity : $$O(1)$$. Constant space is used.

---

#### Approach #5 Greedy Approach 

**Algorithm**

We need not necessarily need dp to solve this problem. This problem is equivalent to finding the number of alternating max. and min. peaks in the array. Since, if we choose any other intermediate number to be a part of the current wiggle subsequence, the maximum length of that wiggle subsequence will always be less than or equal to the one obtained by choosing only the consecutive max. and min. elements.

This can be clarified by looking at the following figure:
![Wiggle Peaks](https://leetcode.com/media/original_images/376_Wiggle_Subsequence.PNG)

From the above figure, we can see that if we choose **C** instead of **D** as the 2nd point in the wiggle subsequence, we can't include the point **E**. Thus, we won't obtain the maximum length wiggle subsequence.

Thus, to solve this problem, we maintain a variable $$\text{prevdiff}$$, where $$\text{prevdiff}$$ is used to indicate whether the current subsequence of numbers lies in an increasing or decreasing wiggle. If $$\text{prevdiff} > 0$$, it indicates that we have found the increasing wiggle and are looking for a decreasing wiggle now. Thus, we update the length of the found subsequence when $$\text{diff}$$ ($$nums[i]-nums[i-1]$$) becomes negative. Similarly, if $$\text{prevdiff} < 0$$, we will update the count when $$\text{diff}$$ ($$nums[i]-nums[i-1]$$) becomes positive.

When the complete array has been traversed, we get the required count, which represents the length of the longest wiggle subsequence.


<iframe src="https://leetcode.com/playground/AqoaR5Ks/shared" frameBorder="0" name="AqoaR5Ks" width="100%" height="326"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n)$$. We traverse the given array once.

* Space complexity : $$O(1)$$. No extra space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy understanding DP solution with O(n), Java version
- Author: TenffY
- Creation Date: Sun Jul 24 2016 06:06:21 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 22:12:03 GMT+0800 (Singapore Standard Time)

<p>
For every position in the array, there are only three possible statuses for it. 
- up position, it means nums[i] > nums[i-1]
- down position, it means nums[i] < nums[i-1]
- equals to position, nums[i] == nums[i-1]

So we can use two arrays up[] and down[] to record *the max wiggle sequence length so far* at index ***i***. 
If nums[i] > nums[i-1], that means it  wiggles up. the element before it must be a down position. so up[i] = down[i-1] + 1;  down[i] keeps the same with before. 
If nums[i] < nums[i-1], that means it  wiggles down. the element before it must be a up position. so down[i] = up[i-1] + 1;  up[i] keeps the same with before. 
 If nums[i] == nums[i-1], that means it will not change anything becasue it didn't wiggle at all. so both down[i] and up[i] keep the same.

In fact, we can reduce the space complexity to O(1), but current way is more easy to understanding.

```
public class Solution {
    public int wiggleMaxLength(int[] nums) {
        
        if( nums.length == 0 ) return 0;
        
        int[] up = new int[nums.length];
        int[] down = new int[nums.length];
        
        up[0] = 1;
        down[0] = 1;
        
        for(int i = 1 ; i < nums.length; i++){
            if( nums[i] > nums[i-1] ){
                up[i] = down[i-1]+1;
                down[i] = down[i-1];
            }else if( nums[i] < nums[i-1]){
                down[i] = up[i-1]+1;
                up[i] = up[i-1];
            }else{
                down[i] = down[i-1];
                up[i] = up[i-1];
            }
        }
        
        return Math.max(down[nums.length-1],up[nums.length-1]);
    }
}
```
</p>


### Very Simple Java Solution with detail explanation
- Author: keshavk
- Creation Date: Fri Jul 22 2016 16:38:15 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 21:10:59 GMT+0800 (Singapore Standard Time)

<p>
In Wiggle Subsequence, think that the solution we need should be in a way that we get alternative higher, lower,higher number.
Eg: 2, 5, 3, 8, 6, 9 
In above example, the sequence of numbers is small,big,small,big,small,big numbers (In shape of hill).

Now for explanation, we take example series:
2,1,4,5,6,3,3,4,8,4

First we check  if the series is starting as (big, small) or (small, big). So as 2,1 is big, small. So we will start the loop as we need small number first that is 1 as 2 is already there.
```
Step 1: First we check our requirement is to get small number. As 1<2 so the series will be
 2,1
```
```
Step 2: Now we need big number that is  greater than 1. As 4>1 so series  will be
2,1,4
```
```
Step 3: Now we need small number. But 5>4 so 4 will be replaced by 5. So the series will become
2,1,5
```
```
Step 4:  We need small number. But 6>5. Series will be
2,1,6
```
```
Step 5: Require small number. 3<6. Series will be
2,1,6,3
```
```
Step 6: Require big number. 3=3. No change in series
2,1,6,3
```
```
Step 7: Require big number. 4>3. Series will become
2,1,6,3,4
```
```
Step 8:  Require small number. 8>4. 8 will  replace 4 and series will become
2,1,6,3,8
```
```
Step 9: Require small number. 4<8. So final series will  be
2,1,6,3,8,4
```
Answer is 6.

In the code, for constant space O(1) we will  modify the same 'num' array to store the (small, big, small) hill shape values. So the code will not only calculate the length of the sequence but if the interviewers asks for the Wiggle series also then we can return the series too. The leetcode Online Judge skipped a test case if the series starts with same set of numbers. Thanks to @ztq63830398, modified the  code to consider that test case also.    

Code:
```
    public class Solution {
	public int wiggleMaxLength(int[] nums) {
		if (nums.length == 0 || nums.length == 1) {
			return nums.length;
		}
		int k = 0;
		while (k < nums.length - 1 && nums[k] == nums[k + 1]) {  //Skips all the same numbers from series beginning eg 5, 5, 5, 1
			k++;
		}
		if (k == nums.length - 1) {
			return 1;
		}
		int result = 2;     // This will track the result of result array
		boolean smallReq = nums[k] < nums[k + 1];       //To check series starting pattern
		for (int i = k + 1; i < nums.length - 1; i++) {
			if (smallReq && nums[i + 1] < nums[i]) {
				nums[result] = nums[i + 1];
				result++;
				smallReq = !smallReq;    //Toggle the requirement from small to big number
			} else {
				if (!smallReq && nums[i + 1] > nums[i]) {
					nums[result] = nums[i + 1];
					result++;
					smallReq = !smallReq;    //Toggle the requirement from big to small number
				}
			}
		}
		return result;
	}
}
```
</p>


### C++ 0ms O(N) dynamic programming solution
- Author: adpa
- Creation Date: Sun Jul 24 2016 01:11:08 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 21:06:24 GMT+0800 (Singapore Standard Time)

<p>
````
class Solution {
public:
    int wiggleMaxLength(vector<int>& nums) {
        int size = nums.size();
        
        if (size == 0) {
            return 0;
        }
        
        /** up[i] is the length of a longest wiggle subsequence of {nums[0],...,nums[i]}, with a
            positive difference between its last two numbers. This subsequence may or may not
            include nums[i] and there may be several such subsequences (of the same length).
            We call this a subsequence of type U.
         */
        vector<int> up(size, 0);
        /** down[i] is the length of a longest wiggle subsequence of {nums[0],...,nums[i]}, with a
            negative difference between its last two numbers. This subsequence may or may not
            include nums[i] and there may be several such subsequences (of the same length).
            We call this a subsequence of type D.
         */
        vector<int> down(size, 0);
        
        // At i=0, there is only one number and we can use it as a subsequence, i.e up[0]=down[0]=1
        up[0] = 1;
        down[0] = 1;
        for(int i=1; i<size; ++i){
            
            if (nums[i] > nums[i-1]) {
                /** If nums[i] > nums[i-1], then we can use nums[i] to make a longer subsequence of type U
                    Proof: We consider a subsequence of type D in {0,...,i-1} (its length is down[i-1]).
                    Let N be the last number of this subsequence.
                    - If nums[i] > N, then we can add nums[i] to the subsequence and it gives us a longer
                    valid subsequence of type U.
                    - If nums[i] <= N, then:
                    (1) N cannot be nums[i-1], because nums[i-1] < nums[i] <= N i.e. nums[i-1] < N
                    (2) We can replace N with nums[i-1] (we still have a valid
                    subsequence of type D since N >= nums[i] > nums[i-1] i.e. N > nums[i-1]),
                    and then add nums[i] to the subsequence, and we have a longer subsequence of type U.
                    Therefore up[i] = down[i-1] + 1
                    
                    There is no gain in using nums[i] to make a longer subsequence of type D.
                    Proof: Let N be the last number of a subsequence of type U
                    in {0,...,i-1}.
                    Assume we can use nums[i] to make a longer subsequence of type D. Then:
                    (1) N cannot be nums[i-1], otherwise we would not be able to use nums[i]
                    to make a longer subsequence of type D as nums[i] > nums[i-1]
                    (2) Necessarily nums[i] < N, and therefore nums[i-1] < N since nums[i-1] < nums[i].
                    But this means that we could have used nums[i-1] already to make a longer
                    subsequence of type D.
                    So even if we can use nums[i], there is no gain in using it, so we keep the old value of
                    down (down[i] = down[i-1])
                */
                up[i] = down[i-1] + 1;
                down[i] = down[i-1];
            }
            else if (nums[i] < nums[i-1]) {
                /** The reasoning is similar if nums[i] < nums[i-1] */
                down[i] = up[i-1] + 1;
                up[i] = up[i-1];
            }
            else {
                /** if nums[i] == nums[i-1], we cannot do anything more than what we did with
                     nums[i-1] so we just keep the old values of up and down
                */
                up[i] = up[i-1];
                down[i] = down[i-1];
            }
        }
        return max(up[size-1], down[size-1]);
    }
};
````
</p>


