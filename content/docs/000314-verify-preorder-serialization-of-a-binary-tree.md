---
title: "Verify Preorder Serialization of a Binary Tree"
weight: 314
#id: "verify-preorder-serialization-of-a-binary-tree"
---
## Description
<div class="description">
<p>One way to serialize a binary tree is to use pre-order traversal. When we encounter a non-null node, we record the node&#39;s value. If it is a null node, we record using a sentinel value such as <code>#</code>.</p>

<pre>
     _9_
    /   \
   3     2
  / \   / \
 4   1  #  6
/ \ / \   / \
# # # #   # #
</pre>

<p>For example, the above binary tree can be serialized to the string <code>&quot;9,3,4,#,#,1,#,#,2,#,6,#,#&quot;</code>, where <code>#</code> represents a null node.</p>

<p>Given a string of comma separated values, verify whether it is a correct preorder traversal serialization of a binary tree. Find an algorithm without reconstructing the tree.</p>

<p>Each comma separated value in the string must be either an integer or a character <code>&#39;#&#39;</code> representing <code>null</code> pointer.</p>

<p>You may assume that the input format is always valid, for example it could never contain two consecutive commas such as <code>&quot;1,,3&quot;</code>.</p>

<p><b>Example 1:</b></p>

<pre>
<strong>Input: </strong><code>&quot;9,3,4,#,#,1,#,#,2,#,6,#,#&quot;</code>
<strong>Output: </strong><code>true</code></pre>

<p><b>Example 2:</b></p>

<pre>
<strong>Input: </strong><code>&quot;1,#&quot;</code>
<strong>Output: </strong><code>false</code>
</pre>

<p><b>Example 3:</b></p>

<pre>
<strong>Input: </strong><code>&quot;9,#,#,1&quot;</code>
<strong>Output: </strong><code>false</code></pre>
</div>

## Tags
- Stack (stack)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Iteration

**Intuition**

Let's start from the simplest but not optimal solution to discuss the idea.

Binary tree could be considered as a number of slots to fulfill.
At the start there is just one slot available for a number 
or null node. 
Both number and null node take one slot to be placed.
For the null node the story ends up here, 
whereas the number will add into the tree two slots 
for the child nodes. Each child node could be,
again, a number or a null.  
 
> The idea is straightforward : 
take the nodes one by one from preorder traversal, 
and compute the number of available slots.
If at the end all available slots are used up, the preorder
traversal represents the valid serialization. 

- In the beginning there is one available slot.   

- Each number or null consumes one slot.

- Null node adds no slots, whereas each number adds two slots for the
child nodes.

![fig](../Figures/331/rules.png)

**Algorithm**

- Initiate the number of available slots: `slots = 1`.

- Split preorder traversal by comma, and iterate over the resulting array. 
At each step :

    - Both a number or a null node take one slot : `slots = slot - 1`.
    
    - If the number of available slots is negative, the 
    preorder traversal is invalid, return False.
    
    - Non-empty node `node != '#'` creates two more available slots:
    `slots = slots + 2`.
    
- Preorder traversal is valid if all available slots are used up : 
return `slots == 0`.

**Implementation**

<iframe src="https://leetcode.com/playground/aF3KSqaH/shared" frameBorder="0" width="100%" height="395" name="aF3KSqaH"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ to iterate over the 
string of length N. 

* Space complexity : $$\mathcal{O}(N)$$ to keep split array in memory. 
<br /> 
<br />


---
#### Approach 2: One pass

**Intuition**

Approach 1 uses $$\mathcal{O}(N)$$ space to keep split array in memory,
and for sure that should be optimised. The idea is to 
iterate over the string itself and not over the array of nodes.

During the iteration, one has to update the number of
available slots at each comma character. 
First, one should decrease the number of slots by one, because 
both empty and non-empty node take one slot.
Second, if the node is a non-empty one, i.e. 
the character just before the comma is not equal to `#`, one
should add two more slots for the child nodes. 

The last node should be considered separately, since there is 
no comma after it.

!?!../Documents/331_LIS.json:1000,520!?!

**Algorithm**

- Initiate the number of available slots: `slots = 1`.

- Iterate over the string. At each comma :

    - Both a number or a null node take one slot : `slots = slot - 1`.
    
    - If the number of available slots is negative, the 
    preorder traversal is invalid, return False.
    
    - Non-empty node, detected by non-`#` character before comma,
    creates two more available slots: `slots = slots + 2`.
    
- The last node should be considered separately, since there is 
no comma after it. 
    
- Preorder traversal is valid if all available slots are used up : 
return `slots == 0`.

**Implementation**

<iframe src="https://leetcode.com/playground/oUyFXgvU/shared" frameBorder="0" width="100%" height="480" name="oUyFXgvU"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ to iterate over the 
string of length N. 

* Space complexity : $$\mathcal{O}(1)$$, it's a constant space 
solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 7 lines Easy Java Solution
- Author: dietpepsi
- Creation Date: Mon Feb 01 2016 07:44:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 11:21:25 GMT+0800 (Singapore Standard Time)

<p>
Some used stack. Some used the depth of a stack. Here I use a different perspective. In a binary tree, if we consider null as leaves, then

* all non-null node provides 2 outdegree and 1 indegree (2 children and 1 parent), except root
* all null node provides 0 outdegree and 1 indegree (0 child and 1 parent).

Suppose we try to build this tree. During building, we record the difference between out degree and in degree `diff` = `outdegree - indegree`. When the next node comes, we then decrease `diff` by 1, because the node provides an in degree. If the node is not `null`, we increase diff by `2`, because it provides two out degrees. If a serialization is correct, diff should never be negative and diff will be zero when finished.


    public boolean isValidSerialization(String preorder) {
        String[] nodes = preorder.split(",");
        int diff = 1;
        for (String node: nodes) {
            if (--diff < 0) return false;
            if (!node.equals("#")) diff += 2;
        }
        return diff == 0;
    }
</p>


### The simplest python solution with explanation (no stack, no recursion)
- Author: 28554010
- Creation Date: Wed Feb 03 2016 07:18:16 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 14:42:47 GMT+0800 (Singapore Standard Time)

<p>
We just need to remember how many empty slots we have during the process. 

Initially we have one ( for the root ). 

for each node we check if we still have empty slots to put it in. 

 - a null node occupies one slot.
 - a non-null node occupies one slot before he creates two more. the net gain is one. 

----------
    class Solution(object):
        def isValidSerialization(self, preorder):
            """
            :type preorder: str
            :rtype: bool
            """
            # remember how many empty slots we have
            # non-null nodes occupy one slot but create two new slots
            # null nodes occupy one slot
            
            p = preorder.split(',')
            
            #initially we have one empty slot to put the root in it
            slot = 1
            for node in p:
                
                # no empty slot to put the current node
                if slot == 0:
                    return False
                    
                # a null node?
                if node == '#':
                    # ocuppy slot
                    slot -= 1
                else:
                    # create new slot
                    slot += 1
            
            #we don't allow empty slots at the end
            return slot==0
</p>


### Java intuitive 22ms solution with stack
- Author: yanggao
- Creation Date: Mon Feb 01 2016 07:00:51 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 20:08:23 GMT+0800 (Singapore Standard Time)

<p>
See detailed comments below. Time complexity is O(n), space is also O(n) for the stack.

    public class Solution {
        public boolean isValidSerialization(String preorder) {
            // using a stack, scan left to right
            // case 1: we see a number, just push it to the stack
            // case 2: we see #, check if the top of stack is also #
            // if so, pop #, pop the number in a while loop, until top of stack is not #
            // if not, push it to stack
            // in the end, check if stack size is 1, and stack top is #
            if (preorder == null) {
                return false;
            }
            Stack<String> st = new Stack<>();
            String[] strs = preorder.split(",");
            for (int pos = 0; pos < strs.length; pos++) {
                String curr = strs[pos];
                while (curr.equals("#") && !st.isEmpty() && st.peek().equals(curr)) {
                    st.pop();
                    if (st.isEmpty()) {
                        return false;
                    }
                    st.pop();
                }
                st.push(curr);
            }
            return st.size() == 1 && st.peek().equals("#");
        }
    }
</p>


