---
title: "Clone Binary Tree With Random Pointer"
weight: 1375
#id: "clone-binary-tree-with-random-pointer"
---
## Description
<div class="description">
<p>A binary tree is given such that each node contains an additional random pointer which could point to any node in the tree or null.</p>

<p>Return a&nbsp;<a href="https://en.wikipedia.org/wiki/Object_copying#Deep_copy" target="_blank"><strong>deep copy</strong></a>&nbsp;of the tree.</p>

<p>The tree is represented in the same input/output way as normal binary trees where each node is represented as a pair of&nbsp;<code>[val, random_index]</code>&nbsp;where:</p>

<ul>
	<li><code>val</code>: an integer representing&nbsp;<code>Node.val</code></li>
	<li><code>random_index</code>: the index of the node (in the input) where the random pointer points to, or&nbsp;<code>null</code>&nbsp;if it does not point to any node.</li>
</ul>

<p>You will be given the tree in class <code>Node</code> and you should return the cloned tree in class <code>NodeCopy</code>. <code>NodeCopy</code> class is just a clone of <code>Node</code> class with the same attributes and constructors.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/06/17/clone_1.png" style="width: 500px; height: 473px;" />
<pre>
<strong>Input:</strong> root = [[1,null],null,[4,3],[7,0]]
<strong>Output:</strong> [[1,null],null,[4,3],[7,0]]
<strong>Explanation:</strong> The original binary tree is [1,null,4,7].
The random pointer of node one is null, so it is represented as [1, null].
The random pointer of node 4 is node 7, so it is represented as [4, 3] where 3 is the index of node 7 in the array representing the tree.
The random pointer of node 7 is node 1, so it is represented as [7, 0] where 0 is the index of node 1 in the array representing the tree.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/06/17/clone_2.png" style="width: 500px; height: 540px;" />
<pre>
<strong>Input:</strong> root = [[1,4],null,[1,0],null,[1,5],[1,5]]
<strong>Output:</strong> [[1,4],null,[1,0],null,[1,5],[1,5]]
<strong>Explanation:</strong> The random pointer of a node can be the node itself.
</pre>

<p><strong>Example 3:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/06/17/clone_3.png" style="width: 500px; height: 426px;" />
<pre>
<strong>Input:</strong> root = [[1,6],[2,5],[3,4],[4,3],[5,2],[6,1],[7,0]]
<strong>Output:</strong> [[1,6],[2,5],[3,4],[4,3],[5,2],[6,1],[7,0]]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root = []
<strong>Output:</strong> []
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> root = [[1,null],null,[2,null],null,[1,null]]
<strong>Output:</strong> [[1,null],null,[2,null],null,[1,null]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the&nbsp;<code>tree</code>&nbsp;is in the range&nbsp;<code>[0, 1000].</code></li>
	<li>Each node&#39;s value is between&nbsp;<code>[1, 10^6]</code>.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Tree (tree)
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple ~Single Pass~ DFS with a HashMap - O(n) [Java]
- Author: Anonymouso
- Creation Date: Thu Jun 18 2020 07:47:10 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 18 2020 23:12:54 GMT+0800 (Singapore Standard Time)

<p>
This can be solved with a simple DFS with a HashMap.
First we check if the node is null, if not, we check if it\'s in our map, if not - we create a copy of our current node and run DFS again on the left, right and random nodes.

One edge case to notice is that if there\'s a cycle in the tree (a random pointer looking at previous visited node) then the map would never be filled.
To solve this we need to create the CopyNode, and immediately put it in the map.
That way, when there\'s a cycle - we\'d just return the node we put in the map and end the infinite loop.

```
class Solution {
    public NodeCopy copyRandomBinaryTree(Node root) {
        Map<Node,NodeCopy> map = new HashMap<>();
        return dfs(root, map);
    }
    
    public NodeCopy dfs(Node root, Map<Node,NodeCopy> map){
        if (root==null) return null;        
        
        if (map.containsKey(root)) return map.get(root);
        
        NodeCopy newNode = new NodeCopy(root.val);
        map.put(root,newNode);
        
        newNode.left=dfs(root.left,map);
        newNode.right=dfs(root.right,map);
        newNode.random=dfs(root.random,map);
        
        return newNode;
        
    }
}
```

Time complexity: O(n) - we run on the entire tree once. Our map acts as memoization to prevent redoing work.
Space copmlexity: O(n) - We\'re creating a hashmap to save all nodes in the tree.
</p>


### Java, easy to understand
- Author: leetcode07051
- Creation Date: Mon Jun 22 2020 11:58:20 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 22 2020 11:58:20 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public NodeCopy copyRandomBinaryTree(Node root) {
        Map<Node, NodeCopy> map = new HashMap<>();
        return doClone(root, map);
    }
    
    private NodeCopy doClone(Node root, Map<Node, NodeCopy> map) {
        if (root == null) {
            return null;
        }
        if (map.containsKey(root)) {
            return map.get(root);
        } else {
            NodeCopy cur = new NodeCopy(root.val);
            map.put(root, cur);
            cur.left = doClone(root.left, map);
            cur.right = doClone(root.right, map);
            cur.random = doClone(root.random, map);
            return cur;
        }
    }
}
```
</p>


### Java BFS
- Author: frouds
- Creation Date: Sun Jun 21 2020 04:41:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 21 2020 04:41:36 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public NodeCopy copyRandomBinaryTree(Node root) {
        if( root == null ){
            return null;
        }
        Map<Node, NodeCopy> map = new HashMap<>();
        map.put(root, new NodeCopy(root.val));
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while( !queue.isEmpty() ){
            Node node = queue.poll();
            NodeCopy newNode = map.get(node);
            if( node.random != null ){
                if( !map.containsKey(node.random) ){
                    map.put(node.random, new NodeCopy(node.random.val));
                }
                newNode.random = map.get(node.random);
            }
            if( node.left != null ){
                if( !map.containsKey(node.left) ){
                    map.put(node.left, new NodeCopy(node.left.val));
                }
                newNode.left = map.get(node.left);
                queue.add(node.left);
            }
            if( node.right != null ){
                if( !map.containsKey(node.right) ){
                    map.put(node.right, new NodeCopy(node.right.val));
                }
                newNode.right = map.get(node.right);
                queue.add(node.right);
            }
        }
        return map.get(root);
    }
}
```
</p>


