---
title: "Binary Subarrays With Sum"
weight: 880
#id: "binary-subarrays-with-sum"
---
## Description
<div class="description">
<p>In an array <code>A</code> of <code>0</code>s and <code>1</code>s, how many <strong>non-empty</strong> subarrays have sum <code>S</code>?</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[1,0,1,0,1]</span>, S = <span id="example-input-1-2">2</span>
<strong>Output: </strong><span id="example-output-1">4</span>
<strong>Explanation: </strong>
The 4 subarrays are bolded below:
[<strong>1,0,1</strong>,0,1]
[<strong>1,0,1,0</strong>,1]
[1,<strong>0,1,0,1</strong>]
[1,0,<strong>1,0,1</strong>]
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>A.length &lt;= 30000</code></li>
	<li><code>0 &lt;= S &lt;= A.length</code></li>
	<li><code>A[i]</code>&nbsp;is either <code>0</code>&nbsp;or <code>1</code>.</li>
</ol>
</div>

## Tags
- Hash Table (hash-table)
- Two Pointers (two-pointers)

## Companies
- C3 IoT - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Index of Ones

**Intuition**

Say we number the `1`s in `A`: $$(x_1, x_2, \cdots, x_n)$$ with $$A[x_i] = 1$$.

Then, if we have a subarray of sum $$S$$, it has to use the ones $$x_i, x_{i+1}, \cdots, x_{i+S-1}$$.  For each $$i$$, we can count the number of such subarrays individually.

**Algorithm**

In general, the number of such subarrays (for $$i$$) is $$(x_i - x_{i-1}) * (x_{i+S} - x_{i+S-1})$$.

For example, if $$S = 2$$, then in `A = [1,0,1,0,1,0,0,1]`, let's count the number of subarrays `[i, j]` that use the middle two `1`s.  There are 2 choices for the `i` `(i = 1, 2)` and 3 choices for the `j` `(j = 4, 5, 6)`.

The corner cases are when $$S = 0$$, $$i = 1$$, or $$i+S = n+1$$. We can handle these gracefully.

<iframe src="https://leetcode.com/playground/Cb2DKSB7/shared" frameBorder="0" width="100%" height="500" name="Cb2DKSB7"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Prefix Sums

**Intuition**

Let `P[i] = A[0] + A[1] + ... + A[i-1]`.  Then `P[j+1] - P[i] = A[i] + A[i+1] + ... + A[j]`, the sum of the subarray `[i, j]`.

Hence, we are looking for the number of `i < j` with `P[j] - P[i] = S`.

**Algorithm**

For each `j`, let's count the number of `i` with `P[j] = P[i] + S`.  This is analogous to counting the number of subarrays ending in `j` with sum `S`.

It comes down to counting how many `P[i] + S` we've seen before.  We can keep this count on the side to help us find the final answer.

<iframe src="https://leetcode.com/playground/fqvGJzPd/shared" frameBorder="0" width="100%" height="344" name="fqvGJzPd"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 3: Three Pointer

**Intuition**

For each `j`, let's try to count the number of `i`'s that have the subarray `[i, j]` equal to `S`.

It is easy to see these `i`'s form an interval `[i_lo, i_hi]`, and each of `i_lo`, `i_hi` are increasing with respect to `j`.  So we can use a "two pointer" style approach.

**Algorithm**

For each `j` (in increasing order), let's maintain 4 variables:

* `sum_lo` : the sum of subarray `[i_lo, j]`
* `sum_hi` : the sum of subarray `[i_hi, j]`
* `i_lo` : the smallest `i` so that `sum_lo <= S`
* `i_hi` : the largest `i` so that `sum_hi <= S`

Then, (provided that `sum_lo == S`), the number of subarrays ending in `j` is `i_hi - i_lo + 1`.

As an example, with `A = [1,0,0,1,0,1]` and `S = 2`, when `j = 5`, we want `i_lo = 1` and `i_hi = 3`.

<iframe src="https://leetcode.com/playground/BQEUvTEm/shared" frameBorder="0" width="100%" height="480" name="BQEUvTEm"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Sliding Window, O(1) Space
- Author: lee215
- Creation Date: Sun Oct 28 2018 11:10:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 23:10:21 GMT+0800 (Singapore Standard Time)

<p>
# Solution 1: HashMap
Count the occurrence of all prefix sum.

I didn\'t notice that the array contains only 0 and 1,
so this solution also works if have negatives.

Space `O(N)`
Time `O(N)`
<br>

**C++:**
```cpp
    int numSubarraysWithSum(vector<int>& A, int S) {
        unordered_map<int, int> c({{0, 1}});
        int psum = 0, res = 0;
        for (int i : A) {
            psum += i;
            res += c[psum - S];
            c[psum]++;
        }
        return res;
    }
```

**Java:**
```java
    public int numSubarraysWithSum(int[] A, int S) {
        int psum = 0, res = 0, count[] = new int[A.length + 1];
        count[0] = 1;
        for (int i : A) {
            psum += i;
            if (psum >= S)
                res += count[psum - S];
            count[psum]++;
        }
        return res;
    }
```

**Python:**
```python
    def numSubarraysWithSum(self, A, S):
        c = collections.Counter({0: 1})
        psum = res = 0
        for i in A:
            psum += i
            res += c[psum - S]
            c[psum] += 1
        return res
```
<br>

# Solution 2: Sliding Window
We have done this hundreds time.
Space `O(1)`
Time `O(N)`
<br>

**C++:**
```cpp
    int numSubarraysWithSum(vector<int>& A, int S) {
        return atMost(A, S) - atMost(A, S - 1);
    }

    int atMost(vector<int>& A, int S) {
        if (S < 0) return 0;
        int res = 0, i = 0, n = A.size();
        for (int j = 0; j < n; j++) {
            S -= A[j];
            while (S < 0)
                S += A[i++];
            res += j - i + 1;
        }
        return res;
    }
```

**Java:**
```java
    public int numSubarraysWithSum(int[] A, int S) {
        return atMost(A, S) - atMost(A, S - 1);
    }

    public int atMost(int[] A, int S) {
        if (S < 0) return 0;
        int res = 0, i = 0, n = A.length;
        for (int j = 0; j < n; j++) {
            S -= A[j];
            while (S < 0)
                S += A[i++];
            res += j - i + 1;
        }
        return res;
    }
```

**Python:**
```python
    def numSubarraysWithSum(self, A, S):
        def atMost(S):
            if S < 0: return 0
            res = i = 0
            for j in xrange(len(A)):
                S -= A[j]
                while S < 0:
                    S += A[i]
                    i += 1
                res += j - i + 1
            return res
        return atMost(S) - atMost(S - 1)
```
<br>

# More Similar Sliding Window Problems
Here are some similar sliding window problems.
Also find more explanations.
Good luck and have fun.


- 1358. [Number of Substrings Containing All Three Characters](https://leetcode.com/problems/number-of-substrings-containing-all-three-characters/discuss/516977/JavaC++Python-Easy-and-Concise)
- 1248. [Count Number of Nice Subarrays](https://leetcode.com/problems/count-number-of-nice-subarrays/discuss/419378/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
- 1234. [Replace the Substring for Balanced String](https://leetcode.com/problems/replace-the-substring-for-balanced-string/discuss/408978/javacpython-sliding-window/367697)
- 1004. [Max Consecutive Ones III](https://leetcode.com/problems/max-consecutive-ones-iii/discuss/247564/javacpython-sliding-window/379427?page=3)
-  930. [Binary Subarrays With Sum](https://leetcode.com/problems/binary-subarrays-with-sum/discuss/186683/)
-  992. [Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/discuss/234482/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
-  904. [Fruit Into Baskets](https://leetcode.com/problems/fruit-into-baskets/discuss/170740/Sliding-Window-for-K-Elements)
-  862. [Shortest Subarray with Sum at Least K](https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/discuss/143726/C%2B%2BJavaPython-O(N)-Using-Deque)
-  209. [Minimum Size Subarray Sum](https://leetcode.com/problems/minimum-size-subarray-sum/discuss/433123/JavaC++Python-Sliding-Window)
<br>
</p>


### [Java] Clean Solution 2 Sum + Prefix Sum Caching
- Author: EddieCarrillo
- Creation Date: Sun Oct 28 2018 11:01:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 28 2018 11:01:38 GMT+0800 (Singapore Standard Time)

<p>
TLDR; ```Two Sum + Prefix Sum Caching```

```Logic:``` In this problem we are required to find some interval ```[i:j] ,i < j``` where ```sum[i:j] = target```. We know that ```sum[i:j] = A[i] + A[i+1] +... + A[j]```.
Then we also know that 
Let\'s define ```prefixSum[j] = A[0] + A[1] + ... + A[j]  0 <= j <= n-1 (n = A.length)```
It is easy to see that, 
```sum[i:j] = A[i] + A[i+1] ... + A[j] =```
```(A[0] + A[1] + ... A[i] ...  + A[j]) - (A[0] + A[1] + ... A[i-1]) ``` = 
```prefix[j] - prefix[i-1]```.

Now we the problem reduces to finding # of pairs (i, j) (i < j) such that
```prefix[j] - prefix[i-1]``` = **target**
```This becomes prefix[i-1] = prefix[j] - target with some algebra.```
So we use the hashmap to find all pairs that satisfy the above equations.

We only need to track the prefix sum up to this point however, since we already saved all the previous results in the map.

```if (sum == target) total++``` Here I am checking for the case where the current element is equal to the sum (it needs no interval to produce the sum).




```
class Solution {
    public int numSubarraysWithSum(int[] A, int target) {
        Map<Integer, Integer> presum = new HashMap<>();
	//Prefix sum
        int sum = 0;
	//Answer
        int total = 0;
        for (int i = 0; i < A.length; i++){
            sum += A[i];
            if (presum.get(sum - target) != null){
                total += presum.get(sum - target);
            }
            if (sum == target) total++;
            //Also put this sum into the map as well
            presum.put(sum, presum.getOrDefault(sum, 0) + 1);
        }
        
        return total;
        
    }
}
```

Using a hashmap is overkill in this problem since the only sums that are possible are [0, 1, ..., n].
Therefore, we can just an array as our map instread. Credits to @davidluoyes for pointing this out.
```
class Solution {
    public int numSubarraysWithSum(int[] A, int target) {
        //The largest sum we can have is len(A) = n Why? What if array A[] has all 1\'s.
        int n = A.length;
        //Everything is initialized to zero
        int[] presum = new int[n+1];
        int sum = 0;
        //Case where it\'s just it\'s own 
        int total = 0;
        
        for (int i = 0; i < A.length; i++){
            sum += A[i];
            int compliment = sum - target;
            
            if (compliment >= 0)
            total += presum[compliment];
            
            if (sum == target) total++;
            //Also put this sum into the map as well
            presum[sum]+=1;
        }
        
        return total;
        
    }
}
```
</p>


### 3 ways to solve this kind of problem.
- Author: TCoherence
- Creation Date: Mon Oct 29 2018 04:06:30 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 29 2018 04:06:30 GMT+0800 (Singapore Standard Time)

<p>
This problem is a simplified problem of 560. This is the [link](https://leetcode.com/problems/subarray-sum-equals-k/description/).
1. brute force: $Time: O(N^2), Space: O(1)$  
loop all [i, j] combination.

2. presum $Time: O(N), Space: O(N)$  
store previous sum and the times of this sum, because sum[i, j] = sum[0, j] - sum[0, i - 1], this is a **very very important idea**  

3. two pointer: (positive value) 
if there no 0, we can use 2 pointers easily to solve this problem, because the right pointer moving forward makes the sum larger, and left pointer moving forward makes the sum smaller.
BUT, if there exists 0, we need to pay attention to pattern like this :
[x,x,x,0,0,0,y,y,y,0,0,0,z,z,z], assuming that y+y+y in the mediumm is what we want, so when *lo* is the idx of the first y, and *hi* is the idx of the last y, now *prev* is the idx of the last x, which is the closest non-0 to the first y(A[lo]), then we can know that up to *hi*, we have 1 + (lo - prev - 1) subarrays satisfying the sum equals K. This in java likes this:
```java
if ( sum + A[hi] == K ) {
    sum += A[hi];
    hi++;
    cnt += 1 + (lo - prev - 1);
}
```
There are two more branches to consider, one is that sum + A[hi] is smaller than K, which we need to move hi forward to make sum bigger. This in java likes this:
```java
if ( sum + A[hi] < K ) {
    sum += A[hi];
    hi++;
}
```
the other one is that sum + A[hi] is bigger than K, which we need to move lo forward to make sum smaller, BUT, there are one more condition we need to pay attention to, that is when we move forward, we need to update current *prev* to the preivous lo. And to make sure A[lo] is non-0, we need to move lo over the 0 in [y,y,y]. This in java likes this:
```java
if ( sum + A[hi] > K ) {
    sum -= A[lo];
    prev = lo;
    lo++;
    while ( A[lo] == 0 ) {
        lo++;
    }
}
```
So now we have the code like this:
```java
if ( sum + A[hi] == K ) {
    sum += A[hi];
    hi++;
    cnt += 1 + (lo - prev - 1);
}
else if ( sum + A[hi] < K ) {
    sum += A[hi];
    hi++;
}
else {
    sum -= A[hi];
    prev = lo;
    lo++;
    while ( A[lo] == 0 ) {
        lo++;
    }
}
```

And also we need to pay attention to S = 0. And case like [0,0,0,0,0] S > 0. So the whole code is as follows:
``` java
public int numSubarraysWithSum(int[] A, int S) {
    if ( S == 0 ) return helper(A); //deal with S == 0 
    int prev = -1;
    int lo = 0, hi = 0;
    int sum = 0;
    int cnt = 0;
    // deal with S > 0 but cases like[0,0,0,0,0]
    while ( lo < A.length && A[lo] == 0 ) {
        lo++;
        hi++;
    }
    while ( hi < A.length ) {
        if ( sum + A[hi] == S ) {
            sum += A[hi];
            cnt += 1 + (lo - prev - 1);
            hi++;
        }
        else if ( sum + A[hi] < S ) {
            sum += A[hi];
            hi++;
        }
        else {
            sum -= A[lo];
            prev = lo;
            lo++;
            while ( A[lo] == 0 ) 
                lo++;
        }
    }
    return cnt;
}
public int helper(int[] A) {
    int cnt = 0;
    int res = 0;
    for ( int i = 0; i < A.length; i++ ) {
        if ( A[i] == 0 ) {
            cnt++;
            res += cnt;
        }
        else {
            cnt = 0;
        }
    }
    return res;
}
```
</p>


