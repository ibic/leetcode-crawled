---
title: "Dungeon Game"
weight: 174
#id: "dungeon-game"
---
## Description
<div class="description">
<style type="text/css">table.dungeon, .dungeon th, .dungeon td {
  border:3px solid black;
}

 .dungeon th, .dungeon td {
    text-align: center;
    height: 70px;
    width: 70px;
}
</style>
<p>The demons had captured the princess (<strong>P</strong>) and imprisoned her in the bottom-right corner of a dungeon. The dungeon consists of M x N rooms laid out in a 2D grid. Our valiant knight (<strong>K</strong>) was initially positioned in the top-left room and must fight his way through the dungeon to rescue the princess.</p>

<p>The knight has an initial health point represented by a positive integer. If at any point his health point drops to 0 or below, he dies immediately.</p>

<p>Some of the rooms are guarded by demons, so the knight loses health (<em>negative</em> integers) upon entering these rooms; other rooms are either empty (<em>0&#39;s</em>) or contain magic orbs that increase the knight&#39;s health (<em>positive</em> integers).</p>

<p>In order to reach the princess as quickly as possible, the knight decides to move only rightward or downward in each step.</p>

<p>&nbsp;</p>

<p><strong>Write a function to determine the knight&#39;s minimum initial health so that he is able to rescue the princess.</strong></p>

<p>For example, given the dungeon below, the initial health of the knight must be at least <strong>7</strong> if he follows the optimal path <code>RIGHT-&gt; RIGHT -&gt; DOWN -&gt; DOWN</code>.</p>

<table class="dungeon">
	<tbody>
		<tr>
			<td>-2 (K)</td>
			<td>-3</td>
			<td>3</td>
		</tr>
		<tr>
			<td>-5</td>
			<td>-10</td>
			<td>1</td>
		</tr>
		<tr>
			<td>10</td>
			<td>30</td>
			<td>-5 (P)</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li>The knight&#39;s health has no upper bound.</li>
	<li>Any room can contain threats or power-ups, even the first room the knight enters and the bottom-right room where the princess is imprisoned.</li>
</ul>

</div>

## Tags
- Binary Search (binary-search)
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 4 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Dynamic Programming

**Overview**
 
 Like many problems with 2D grid, often the case one can apply either the technique of [backtracking](https://leetcode.com/explore/learn/card/recursion-ii/472/backtracking/) or dynamic programming.

 Specifically, as it turns out, _**dynamic programming**_ would work perfectly for this problem.

>As a general pattern of dynamic programming, usually we construct a array of one or two dimensions (_i.e._ `dp[i]`) where each element holds the optimal solution for the corresponding subproblem.

To calculate one particular element in the `dp[i]` array, we would refer to the previously calculated elements. And the **last** element that we figure out in the array would be the desired solution for the original problem.

**Intuition**

Following the above guideline, here is how we break down the problem into subproblems and apply the dynamic programming algorithm.

We are asked to calculate the minimal health point that the knight needs, in order to recuse the princess. The knight would move from the up-left corner of the grid to reach the down-right corner where the princess is located (_e.g._ as shown in the following graph). 

![pic](../Figures/174/174_problem_new.png)

>Though the **down-right** corner is the final destination of the knight, we could start from the destination and deduce _**backwards**_ the minimal health point that the knight would need at each step along the way.

So starting from the destination where the princess is locked down, as one can see from the following graph, the knight would need _at least_ 6 health points to survive the damage (5 points) caused by the daemon.

![pic](../Figures/174/174_start_new.png)

Let us now take one step back. Before reaching the destination, there are two possible positions that the knight might situate, _i.e._ the one right above the destination so that the knight would take a _<u>down</u>_ step, and the one to the left of the destination so that the knight would take a _<u>right</u>_ step.

Let us look at the cell (denoted as _<u>cell U</u>_) right above the destination, as shown in the following graph. As we know now, the knight should possess at least 6 health points upon reaching the destination. Since at the _<u>cell U</u>_ we have a magic org which would increase the health of knight by 1 point, the knight would just need to possess 5 health points at the arrival of _<u>cell U</u>_.

![pic](../Figures/174/174_up_new.png)

As another alternative to reach the destination, the knight might situate at the cell (denoted as _<u>cell L</u>_) to the left side of the destination, as shown in the following graph. In this case, similarly the knight would encounter a magic orb which would give him a 30-points boost on health. With this boost of health, it would be more than enough for the knight to survive the final daemon in the destination. As a result, the knight just needs to possess the minimal 1 health point upon entering the _<u>cell L</u>_.

![pic](../Figures/174/174_left_new.png)

Now that we have calculated the minimal health points that the knight would need before reaching the destination from two of the possible directions, we can carry on to one more step further from the destination. Let us look at the cell (denoted as _<u>cell UL</u>_) located at the up-left corner from the destination.

Following the same logic as we have seen in the above steps, we could obtain two values for this cell, which represent the minimal health points that the knight would need for each of the directions that he takes. As one can see from the following graph, at the _<u>cell UL</u>_, if the knight takes a <u>right</u> step next, he would need at least $$5 + 10 = 15$$ health points, in order to rescue the princess at the end. If he takes a <u>down</u> step next, he would need at least $$1 + 10 = 11$$ health points.

![pic](../Figures/174/174_up_left_new.png)

With all the 3 examples above, we conclude with the following graph where each cell is marked with two minimal health points respectively for each direction that the knight might take, except the destination cell. As one can see, starting from the up-left corner of the grid, the knight would only need 7 health points to rescue the princess.

![pic](../Figures/174/174_final.png)

**Algorithm**

Given the above intuition, let us see how we can model it with the general code pattern of dynamic programming algorithm.

First, we define a matrix `dp[row][col]`, where the element `dp[row][col]` indicates the minimal health points that the knight would need, starting from the corresponding dungeon cell `dungeon[row][col]`, in order to reach the destination.

In the following graph, we show what the `dp` matrix looks like, for the examples that we listed in the intuition section.

![pic](../Figures/174/174_dp_breakdown_new.png)

>The main idea of the algorithm is clear: we need to calculate the values in the `dp` matrix. And the last value we calculate for the matrix would be the desired solution for the problem.

In order to calculate the values of `dp` matrix, we start from the down-right corner of the dungeon, and walk following the orders of **from-right-to-left** and **from-down-to-up**. Along with each cell in the dungeon, we calculate the corresponding value of `dp[row][col]` in the matrix.

The value of `dp[row][col]` is determined by the following conditions:

- If possible, by taking the right step from the current dungeon cell, the knight might need `right_health` health points.
<br/>

- If possible, by taking the down step from the current dungeon cell, the knight would might `down_health` health points.
<br/>

- If either of the above two alternatives exists, we then take the minimal value of them as the value for `dp[row][col]`.
<br/>

- If none of the above alternatives exists, _i.e._ we are at the destination cell, there are two sub-cases:

    - If the current cell is of magic orb, then 1 health point would suffice.

    - If the current cell is of daemon, then the knight should possess one health point plus the damage points that would be caused by the daemon.

<iframe src="https://leetcode.com/playground/YfNrPyu6/shared" frameBorder="0" width="100%" height="500" name="YfNrPyu6"></iframe>


**Complexity**

- Time Complexity: $$\mathcal{O}(M \cdot N)$$ where $$M \cdot N$$ is the size of the dungeon. We iterate through the entire dungeon once and only once.

- Space Complexity: $$\mathcal{O}(M \cdot N)$$ where $$M \cdot N$$ is the size of the dungeon. In the algorithm, we keep a dp matrix that is of the same size as the dungeon.
<br/>
<br/>

---
#### Approach 2: Dynamic Programming with Circular Queue

**Intuition**

In the above dynamic programming algorithm, there is not much we can do to optimize the time complexity, other than reducing the costy condition checks with some tricks on the initial values of the dp matrix.

On the other hand, we could reduce the space complexity of the algorithm from $$\mathcal{O}(M \cdot N)$$ to $$\mathcal{O}(N)$$ where $$N$$ is the number of columns.

First of all, let us flatten the dp matrix into 1D array, _i.e._ `dp[row][col] = dp[row * N + col]`.

>As one might notice in the above process, in order to calculate each `dp[i]`, we would refer to at most two previously calculated `dp` values, _i.e._ `dp[i-1]` and `dp[i-N]`. Therefore, once we calculate the value for `dp[i]`, we could discard all the previous values that are beyond the range of `N`.

The above characteristics of the dp array might remind you the container named **_CircularQueue_** which could serve as a sliding window to scan a long list.

![pic](../Figures/174/174_dp_circular_queue_new.png)

Indeed, we could use the CircularQueue to calculate the dp array, as we show in the above graph. At any moment, the size of the CircularQueue would not exceed the predefined capacity, which would be `N` in our case. As a result, we reduce the overall space complexity of the algorithm to $$\mathcal{O}(N)$$.


**Algorithm**

<iframe src="https://leetcode.com/playground/dCfHanvL/shared" frameBorder="0" width="100%" height="500" name="dCfHanvL"></iframe>



**Complexity**

- Time Complexity: $$\mathcal{O}(M \cdot N)$$ where $$M \cdot N$$ is the size of the dungeon. We iterate through the entire dungeon once and only once.

- Space Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the number of columns in the dungeon.
<br/>
<br/>

## Accepted Submission (java)
```java
class Solution {
    public int calculateMinimumHP(int[][] dungeon) {
        int width = dungeon.length;
        if (width <= 0) {
            return 0;
        }
        int height = dungeon[0].length;
        if (height <= 0) {
            return 0;
        }
        int[][] healthNeeded = new int[width + 1][height + 1];
        for (int i = 0; i < width; i++) {
            healthNeeded[i][height] = Integer.MAX_VALUE;
        }
        for (int j = 0; j < height; j++) {
            healthNeeded[width][j] = Integer.MAX_VALUE;
        }
        healthNeeded[width][height - 1] = 1;
        healthNeeded[width - 1][height] = 1;
//        MyUtil.printMatrix(healthNeeded);
//        System.out.println("----");
        for (int i = width - 1; i >= 0; i--) {
            for (int j = height - 1; j >= 0; j--) {
                int damage = dungeon[i][j];
                int down = healthNeeded[i][j + 1];
                int right = healthNeeded[i + 1][j];
                int better = Math.min(down, right);
                int needed = better - damage;
                if (needed > 0) {
                    healthNeeded[i][j] = needed;
                } else {
                    healthNeeded[i][j] = 1;
                }
            }
        }
//        MyUtil.printMatrix(healthNeeded);
        return healthNeeded[0][0];
    }
}

```

## Top Discussions
### C++ DP solution
- Author: sidbai
- Creation Date: Wed Jan 07 2015 17:22:43 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 19:18:59 GMT+0800 (Singapore Standard Time)

<p>
Use hp[i][j] to store the min hp needed at position (i, j), then do the calculation from right-bottom to left-up.

Note: adding dummy row and column would make the code cleaner.

    class Solution {
    public:
        int calculateMinimumHP(vector<vector<int> > &dungeon) {
            int M = dungeon.size();
            int N = dungeon[0].size();
            // hp[i][j] represents the min hp needed at position (i, j)
            // Add dummy row and column at bottom and right side
            vector<vector<int> > hp(M + 1, vector<int>(N + 1, INT_MAX));
            hp[M][N - 1] = 1;
            hp[M - 1][N] = 1;
            for (int i = M - 1; i >= 0; i--) {
                for (int j = N - 1; j >= 0; j--) {
                    int need = min(hp[i + 1][j], hp[i][j + 1]) - dungeon[i][j];
                    hp[i][j] = need <= 0 ? 1 : need;
                }
            }
            return hp[0][0];
        }
    };
</p>


### [Python] Short DP, 7 lines, O(mn), top-down, explained
- Author: DBabichev
- Creation Date: Sun Jun 21 2020 16:10:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 21 2020 22:46:35 GMT+0800 (Singapore Standard Time)

<p>
Probably when you see this problem and you have some experience in this type of problems you can guess, that this is **dynamic programming** problem. However even if you understand this, it is not easy to solve it. Let us use top-down dp, that is `Let dp[i][j]` be the minimum **hp** we need to reach the **princess** if we start from point `(i,j)`. Let us consider the following example:

| -2  | -3  | +3 |
|---------|---------|--------|
| **-5**  | **-10** | **+1** |
| **+10** | **+30** | **-5** |

Let us add bottom dummy row and right dummy column to handle border cases more easy. We fill it with infinities, except two ones - neibours of our princess. I will explain it a bit later.

How we can evaluate `dp[i][j]`? We need to look at two cells: `dp[i+1][j]` and `dp[i][j+1]` and evaluate two possible candidates: `dp[i+1][j]-dungeon[i][j]` and `dp[i][j+1]-dungeon[i][j]`.
1. If at least one of these two numbers is negative, it means that we can survive just with `1` hp: (look at number `+30` in our table for example)
2. If both this numbers are positive, we need to take the mimumum of them, see for example number `-10` in our table: to survive we need either `5- -10 = 15` if we go right and `1- -10 = 11` if we go down, of course we choose `11`.
3. This conditions can be written in one a bit ugly line: `dp[i][j] = max(min(dp[i+1][j],dp[i][j+1])-dungeon[i][j],1)`.
4. Finally, why I put `1` to two neibors of princess? To make this formula valid for princess cell: if we have negative number like `-5` in this cell, we need `6` hp to survive, if we have non-negative number in this cell, we need `1` hp to survive.

| 7  | 5   | 2 | inf |
|---------|---------|-------|---------|
| **6**   | **11**  | **5** | **inf** |
| **1**   | **1**   | **6** | **1**   |
| **inf** | **inf** | **1** | **#**   |

**Complexity**: both time and space is `O(mn)`. Space complexity can be reduced to `O(min(m,n))` as usual, because we look only to neibour cells. However code becomes a bit more difficult to follow.

```
class Solution:
    def calculateMinimumHP(self, dungeon):
        m, n = len(dungeon), len(dungeon[0])
        dp = [[float("inf")]*(n+1) for _ in range(m+1)]
        dp[m-1][n], dp[m][n-1] = 1, 1
            
        for i in range(m-1,-1,-1):
            for j in range(n-1,-1,-1):
                dp[i][j] = max(min(dp[i+1][j],dp[i][j+1])-dungeon[i][j],1)
        
        return dp[0][0]
```

**Further discussion** It is possible to do it with down-top dp as well, howerer in this case you need to use **binary search**, because you do not know in advance if you survive starting say `1000` hp or not. Complexity will be `O(nm log(MAX_INT))` in this case.

If you have any questions, feel free to ask. If you like solution and explanations, please **Upvote!**
</p>


### My AC Java Version, Suggestions are welcome
- Author: vime2
- Creation Date: Tue Jan 06 2015 19:38:54 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 06:15:03 GMT+0800 (Singapore Standard Time)

<p>
    public int calculateMinimumHP(int[][] dungeon) {
        if (dungeon == null || dungeon.length == 0 || dungeon[0].length == 0) return 0;
        
        int m = dungeon.length;
        int n = dungeon[0].length;
        
        int[][] health = new int[m][n];

        health[m - 1][n - 1] = Math.max(1 - dungeon[m - 1][n - 1], 1);

        for (int i = m - 2; i >= 0; i--) {            
            health[i][n - 1] = Math.max(health[i + 1][n - 1] - dungeon[i][n - 1], 1);
        }

        for (int j = n - 2; j >= 0; j--) {
            health[m - 1][j] = Math.max(health[m - 1][j + 1] - dungeon[m - 1][j], 1);
        }

        for (int i = m - 2; i >= 0; i--) {
            for (int j = n - 2; j >= 0; j--) {
                int down = Math.max(health[i + 1][j] - dungeon[i][j], 1);
                int right = Math.max(health[i][j + 1] - dungeon[i][j], 1);
                health[i][j] = Math.min(right, down);
            }
        }

        return health[0][0];
    }
</p>


