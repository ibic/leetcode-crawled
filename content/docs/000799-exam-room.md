---
title: "Exam Room"
weight: 799
#id: "exam-room"
---
## Description
<div class="description">
<p>In an exam room, there are <code>N</code> seats in a single row, numbered <code>0, 1, 2, ..., N-1</code>.</p>

<p>When a student enters the room, they must sit in the seat that maximizes the distance to the closest person.&nbsp; If there are multiple such seats, they sit in the seat with the lowest number.&nbsp; (Also, if no one is in the room, then the student sits at seat number 0.)</p>

<p>Return a class <code>ExamRoom(int N)</code>&nbsp;that exposes two functions: <code>ExamRoom.seat()</code>&nbsp;returning an <code>int</code>&nbsp;representing what seat the student sat in, and <code>ExamRoom.leave(int p)</code>&nbsp;representing that the student in seat number <code>p</code>&nbsp;now leaves the room.&nbsp; It is guaranteed that any calls to <code>ExamRoom.leave(p)</code> have a student sitting in seat <code>p</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;ExamRoom&quot;,&quot;seat&quot;,&quot;seat&quot;,&quot;seat&quot;,&quot;seat&quot;,&quot;leave&quot;,&quot;seat&quot;]</span>, <span id="example-input-1-2">[[10],[],[],[],[],[4],[]]</span>
<strong>Output: </strong><span id="example-output-1">[null,0,9,4,2,null,5]</span>
<span><strong>Explanation</strong>:
ExamRoom(10) -&gt; null
seat() -&gt; 0, no one is in the room, then the student sits at seat number 0.
seat() -&gt; 9, the student sits at the last seat number 9.
seat() -&gt; 4, the student sits at the last seat number 4.
seat() -&gt; 2, the student sits at the last seat number 2.
leave(4) -&gt; null
seat() -&gt; 5, the student sits at the last seat number 5.</span>
</pre>

<p><span>​​​​​​​</span></p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= N &lt;= 10^9</code></li>
	<li><code>ExamRoom.seat()</code> and <code>ExamRoom.leave()</code> will be called at most <code>10^4</code> times across all test cases.</li>
	<li>Calls to <code>ExamRoom.leave(p)</code> are guaranteed to have a student currently sitting in seat number <code>p</code>.</li>
</ol>

</div>

## Tags
- Ordered Map (ordered-map)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Quip (Salesforce) - 3 (taggedByAdmin: true)
- Quora - 3 (taggedByAdmin: true)
- Google - 3 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach 1: Maintain Sorted Positions

**Intuition**

We'll maintain `ExamRoom.students`, a sorted `list` (or `TreeSet` in Java) of positions the students are currently seated in.

**Algorithm**

The `ExamRoom.leave(p)` operation is clear - we will just `list.remove` (or `TreeSet.remove`) the student from `ExamRoom.students`.

Let's focus on the `ExamRoom.seat() : int` operation.  For each pair of adjacent students `i` and `j`, the maximum distance to the closest student is `d = (j - i) / 2`, achieved in the left-most seat `i + d`.  Otherwise, we could also sit in the left-most seat, or the right-most seat.

Finally, we should handle the case when there are no students separately.

For more details, please review the comments made in the implementations.

<iframe src="https://leetcode.com/playground/3vnqyp2k/shared" frameBorder="0" width="100%" height="500" name="3vnqyp2k"></iframe>

**Complexity Analysis**

* Time Complexity:  Each `seat` operation is $$O(P)$$, (where $$P$$ is the number of students sitting), as we iterate through every student.  Each `leave` operation is $$O(P)$$ ($$\log P$$ in Java).

* Space Complexity:  $$O(P)$$, the space used to store the positions of each student sitting.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Jun 17 2018 11:12:49 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 08:13:02 GMT+0800 (Singapore Standard Time)

<p>
Very straight forward idea.
Use a list `L` to record the index of seats where people sit.

`seat()`:
    1. find the biggest distance at the start, at the end and in the middle.
    2. insert index of seat
    3. return index

`leave(p)`: pop out `p`


**Time Complexity**:
O(N) for `seat()` and `leave()`


**C++:**
```
    int N;
    vector<int> L;
    ExamRoom(int n) {
        N = n;
    }

    int seat() {
        if (L.size() == 0) {
            L.push_back(0);
            return 0;
        }
        int d = max(L[0], N - 1 - L[L.size() - 1]);
        for (int i = 0; i < L.size() - 1; ++i) d = max(d, (L[i + 1] - L[i]) / 2);
        if (L[0] == d) {
            L.insert(L.begin(), 0);
            return 0;
        }
        for (int i = 0; i < L.size() - 1; ++i)
            if ((L[i + 1] - L[i]) / 2 == d) {
                L.insert(L.begin() + i + 1, (L[i + 1] + L[i]) / 2);
                return L[i + 1];
            }
        L.push_back(N - 1);
        return N - 1;
    }

    void leave(int p) {
        for (int i = 0; i < L.size(); ++i) if (L[i] == p) L.erase(L.begin() + i);
    }
```

**Java:**
```
    int N;
    ArrayList<Integer> L = new ArrayList<>();
    public ExamRoom(int n) {
        N = n;
    }

    public int seat() {
        if (L.size() == 0) {
            L.add(0);
            return 0;
        }
        int d = Math.max(L.get(0), N - 1 - L.get(L.size() - 1));
        for (int i = 0; i < L.size() - 1; ++i) d = Math.max(d, (L.get(i + 1) - L.get(i)) / 2);
        if (L.get(0) == d) {
            L.add(0, 0);
            return 0;
        }
        for (int i = 0; i < L.size() - 1; ++i)
            if ((L.get(i + 1) - L.get(i)) / 2 == d) {
                L.add(i + 1, (L.get(i + 1) + L.get(i)) / 2);
                return L.get(i + 1);
            }
        L.add(N - 1);
        return N - 1;
    }

    public void leave(int p) {
        for (int i = 0; i < L.size(); ++i) if (L.get(i) == p) L.remove(i);
    }
```
**Python:**
```
    def __init__(self, N):
        self.N, self.L = N, []

    def seat(self):
        N, L = self.N, self.L
        if not L: res = 0
        else:
            d, res = L[0], 0
            for a, b in zip(L, L[1:]):
                if (b - a) / 2 > d:
                    d, res = (b - a) / 2, (b + a) / 2
            if N - 1 - L[-1] > d: res = N - 1
        bisect.insort(L, res)
        return res

    def leave(self, p):
        self.L.remove(p)
```
</p>


### [Python] O(log n) time for both seat() and leave() with heapq and dicts - Detailed explanation
- Author: spatar
- Creation Date: Sun Jun 17 2018 14:24:42 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 20:35:31 GMT+0800 (Singapore Standard Time)

<p>
We will name a range of unoccupied seats from `i` to `j` an *Available segment* with first index `i` and last index `j`. For each available segment we can say how far the "best" seat in this segment is from the closest occupied seat. The number of empty seats in between is **priority** of the segment. The higher the priority the better seat you can get from a segment. For the edge cases when segment starts with index `0` or ends with index `N - 1` priority equals to `segment_size - 1`. For segments in the middle of the row priority can be calculated as `(segment_size - 1) // 2` or `(last - first) // 2`. Please note that two segments of different size may have equal priority. For example, segments with 3 seats and with 4 seats have the same priority "1".


We will use priority queue `self.heap` to store all currently available segments. Python implements `heapq` as **min heap**, so we will use negated priority to keep the best availabe segment on top of the queue. If two segments have equal priority, then the one with lower `first` index is better. Taken this into account, we will store availabale segment in `self.heap` as 4-items list: `[-segment_priority, first_index_of_segment, last_index_of_segment, is_valid]`. The first two items `-segment_priority` and `first_index_of_segment` guarantee correct priority queue order.


A helper function `put_segment()` takes `first` and `last` index of the available segment, calculates its priority and pushes a list object into `self.heap`. In addition, it puts this list object into two dicts: `self.avail_first[first] = segment` and `self.avail_last[last] = segment`. These dicts will be used later in `leave()`.


We start with only one available segment [0, N - 1]. When `seat()` is called, we pop best available segment from `self.heap`. If segment\'s `is_valid` flag is `False` then we pop another one, until we get a valid available segment. There are two edge cases when popped segment starts at 0 or ends at N - 1. For these cases we return the edge seat number (0 or N - 1 respectively) and push new segment into `self.heap`. Otherwize, when the popped segment is in the middle of the row, we return its middle seat and create up to two new available segments of smaller size, and push them into `self.heap`.


Now, `leave()` implementation is quite interesting. When seat `p` is vacated, we need to check if there are adjacent available segment(s) in the heap, and merge them together with `p`. We use dicts `self.avail_first` and `self.avail_last` to check for adjacent available segments. If these segment(s) are found, they need to be excluded from `self.heap`. Deleting items in `self.heap` will break heap invariant and requires subsequent `heapify()` call that executes in O(n log n) time. Instead we can just mark segments as invalid by setting `is_valid` flag: `segment[3] = False`. Invalid segments will be skipped upon `heappop()` in `seat()`.


```python
from heapq import heappop, heappush


class ExamRoom(object):

    def __init__(self, N):
        """
        :type N: int
        """
        self.N = N
        self.heap = []
        self.avail_first = {}
        self.avail_last = {}
        self.put_segment(0, self.N - 1)

    def put_segment(self, first, last):

        if first == 0 or last == self.N - 1:
            priority = last - first
        else:
            priority = (last - first) // 2

        segment = [-priority, first, last, True]

        self.avail_first[first] = segment
        self.avail_last[last] = segment

        heappush(self.heap, segment)

    def seat(self):
        """
        :rtype: int
        """
        while True:
            _, first, last, is_valid = heappop(self.heap)

            if is_valid:
                del self.avail_first[first]
                del self.avail_last[last]
                break

        if first == 0:
            ret = 0
            if first != last:
                self.put_segment(first + 1, last)

        elif last == self.N - 1:
            ret = last
            if first != last:
                self.put_segment(first, last - 1)

        else:
            ret = first + (last - first) // 2

            if ret > first:
                self.put_segment(first, ret - 1)

            if ret < last:
                self.put_segment(ret + 1, last)

        return ret

    def leave(self, p):
        """
        :type p: int
        :rtype: void
        """
        first = p
        last = p

        left = p - 1
        right = p + 1

        if left >= 0 and left in self.avail_last:
            segment_left = self.avail_last.pop(left)
            segment_left[3] = False
            first = segment_left[1]

        if right < self.N and right in self.avail_first:
            segment_right = self.avail_first.pop(right)
            segment_right[3] = False
            last = segment_right[2]

        self.put_segment(first, last)
```
</p>


### Python heapq clean solution with explanation, seat O(log n) leave O(n)
- Author: sfdye
- Creation Date: Thu Oct 25 2018 20:11:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 01 2020 09:43:20 GMT+0800 (Singapore Standard Time)

<p>
seat: `O(log n)`
leave: `O(n)`

This solution is largely inspired by:
https://leetcode.com/problems/exam-room/discuss/148595/Java-PriorityQueue-with-customized-object.-seat:-O(logn)-leave-O(n)-with-explanation

with a few modification to work in Python. A few things I\'ve learned during the process:

1. `heapq` implements a `minheap`, to use a `maxheap`, simpy negate the proirity (x -> -x)
2. If you want to store the priority together with the data, you can use tuple as the element data structure, e.g. `heapq.heapify([(1, "data")])`
3. In case there is a tie, you can specify a second priority, if you are using tuple that would be the second item, e.g. `(1, 1, 3) < (1, 2, 2)`
4. If you remove an item from the underlying list (using either `del` or `.remove`), you have do `heapify` again on the list, otherwise it will lose the heap property.

```python
class ExamRoom:
    
    def dist(self, x, y):  # length of the interval (x, y)
        if x == -1:        # note here we negate the value to make it maxheap
            return -y
        elif y == self.N:
            return -(self.N - 1 - x)
        else:
            return -(abs(x-y)//2) 
        
    def __init__(self, N):
        self.N = N
        self.pq = [(self.dist(-1, N), -1, N)]  # initialize heap
        
    def seat(self):
        _, x, y = heapq.heappop(self.pq)  # current max interval 
        if x == -1:
            seat = 0
        elif y == self.N:
            seat = self.N - 1
        else:
            seat = (x+y) // 2
        heapq.heappush(self.pq, (self.dist(x, seat), x, seat))  # push two intervals by breaking at seat
        heapq.heappush(self.pq, (self.dist(seat, y), seat, y))
        return seat
        
    def leave(self, p):
        head = tail = None
        for interval in self.pq:  # interval is in the form of (d, x, y)
            if interval[1] == p:  
                tail = interval
            if interval[2] == p:  
                head = interval
            if head and tail:
                break
        self.pq.remove(head)
        self.pq.remove(tail)
		heapq.heapify(self.pq)  # important! re-heapify after deletion
        heapq.heappush(self.pq, (self.dist(head[1], tail[2]), head[1], tail[2]))
```
</p>


