---
title: "Rising Temperature"
weight: 1481
#id: "rising-temperature"
---
## Description
<div class="description">
<p>Table: <code>Weather</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| recordDate    | date    |
| temperature   | int     |
+---------------+---------+
id is the primary key for this table.
This table contains information about the temperature in a certain day.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find all dates&#39; <code>id</code>&nbsp;with higher temperature compared to its previous dates (yesterday).</p>

<p>Return the result table in <strong>any order</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Weather</code>
+----+------------+-------------+
| id | recordDate | Temperature |
+----+------------+-------------+
| 1  | 2015-01-01 | 10          |
| 2  | 2015-01-02 | 25          |
| 3  | 2015-01-03 | 20          |
| 4  | 2015-01-04 | 30          |
+----+------------+-------------+

Result table:
+----+
| id |
+----+
| 2  |
| 4  |
+----+
In 2015-01-02, temperature was higher than the previous day (10 -&gt; 25).
In 2015-01-04, temperature was higher than the previous day (30 -&gt; 20).
</pre>

</div>

## Tags


## Companies
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `JOIN` and `DATEDIFF()` clause [Accepted]

**Algorithm**

MySQL uses [DATEDIFF](https://dev.mysql.com/doc/refman/5.7/en/date-and-time-functions.html#function_datediff) to compare two date type values.

So, we can get the result by joining this table **weather** with itself and use this `DATEDIFF()` function.

**MySQL**

```sql
SELECT
    weather.id AS 'Id'
FROM
    weather
        JOIN
    weather w ON DATEDIFF(weather.recordDate, w.recordDate) = 1
        AND weather.Temperature > w.Temperature
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Solution
- Author: fabrizio3
- Creation Date: Tue Mar 31 2015 14:00:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 02:11:42 GMT+0800 (Singapore Standard Time)

<p>
    SELECT wt1.Id 
    FROM Weather wt1, Weather wt2
    WHERE wt1.Temperature > wt2.Temperature AND 
          TO_DAYS(wt1.DATE)-TO_DAYS(wt2.DATE)=1;


EXPLANATION:

**TO_DAYS(wt1.DATE)** return the number of days between from year 0 to date DATE
**TO_DAYS(wt1.DATE)-TO_DAYS(wt2.DATE)=1** check if wt2.DATE is yesterday respect to wt1.DATE

We select from the joined tables the rows that have 

**wt1.Temperature > wt2.Temperature** 

and difference between dates in days of 1 (yesterday):

**TO_DAYS(wt1.DATE)-TO_DAYS(wt2.DATE)=1;**
</p>


### My simple solution using inner join
- Author: JGeek
- Creation Date: Mon Aug 17 2015 17:25:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 29 2018 12:01:13 GMT+0800 (Singapore Standard Time)

<p>
    SELECT t1.Id
    FROM Weather t1
    INNER JOIN Weather t2
    ON TO_DAYS(t1.Date) = TO_DAYS(t2.Date) + 1
    WHERE t1.Temperature > t2.Temperatur
</p>


### Two Solutions..........
- Author: hsldymq
- Creation Date: Tue Apr 28 2015 01:47:46 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 28 2015 01:47:46 GMT+0800 (Singapore Standard Time)

<p>
1.

    SELECT a.Id FROM Weather AS a, Weather AS b
    WHERE DATEDIFF(a.Date, b.Date)=1 AND a.Temperature > b.Temperature

2.

    SELECT Id FROM (
        SELECT CASE
            WHEN Temperature > @prevtemp AND DATEDIFF(Date, @prevdate) = 1 THEN Id ELSE NULL END AS Id,
            @prevtemp:=Temperature,
            @prevdate:=Date
        FROM Weather, (SELECT @prevtemp:=NULL) AS A, (SELECT @prevdate:=NULL) AS B ORDER BY Date ASC
    ) AS D WHERE Id IS NOT NULL
</p>


