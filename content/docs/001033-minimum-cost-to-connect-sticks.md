---
title: "Minimum Cost to Connect Sticks"
weight: 1033
#id: "minimum-cost-to-connect-sticks"
---
## Description
<div class="description">
<p>You have some number of sticks with positive integer lengths. These lengths are given as an array&nbsp;<code>sticks</code>, where&nbsp;<code>sticks[i]</code>&nbsp;is the length of the&nbsp;<code>i<sup>th</sup></code>&nbsp;stick.</p>

<p>You can connect any two sticks of lengths <code>x</code> and <code>y</code> into one stick&nbsp;by paying a cost of <code>x + y</code>. You must connect&nbsp;all the sticks until there is only one stick remaining.</p>

<p>Return&nbsp;<em>the minimum cost of connecting all the given sticks into one stick in this way</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> sticks = [2,4,3]
<strong>Output:</strong> 14
<strong>Explanation:</strong>&nbsp;You start with sticks = [2,4,3].
1. Combine sticks 2 and 3 for a cost of 2 + 3 = 5. Now you have sticks = [5,4].
2. Combine sticks 5 and 4 for a cost of 5 + 4 = 9. Now you have sticks = [9].
There is only one stick left, so you are done. The total cost is 5 + 9 = 14.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> sticks = [1,8,3,5]
<strong>Output:</strong> 30
<strong>Explanation:</strong> You start with sticks = [1,8,3,5].
1. Combine sticks 1 and 3 for a cost of 1 + 3 = 4. Now you have sticks = [4,8,5].
2. Combine sticks 4 and 5 for a cost of 4 + 5 = 9. Now you have sticks = [9,8].
3. Combine sticks 9 and 8 for a cost of 9 + 8 = 17. Now you have sticks = [17].
There is only one stick left, so you are done. The total cost is 4 + 9 + 17 = 30.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> sticks = [5]
<strong>Output:</strong> 0
<strong>Explanation:</strong> There is only one stick, so you don&#39;t need to do anything. The total cost is 0.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code><span>1 &lt;= sticks.length &lt;= 10<sup>4</sup></span></code></li>
	<li><code><span>1 &lt;= sticks[i] &lt;= 10<sup>4</sup></span></code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Amazon - 14 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Can't understand the problem
- Author: suhailamir
- Creation Date: Tue Oct 22 2019 21:39:07 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 22 2019 21:39:07 GMT+0800 (Singapore Standard Time)

<p>
Can\'t understand the problem. Can someone please explain? Thanks.
</p>


### [Python] Greedy Solution
- Author: lee215
- Creation Date: Sun Aug 25 2019 00:11:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 25 2019 00:11:41 GMT+0800 (Singapore Standard Time)

<p>
**Python:**
```python
    def connectSticks(self, A):
        heapq.heapify(A)
        res = 0
        while len(A) > 1:
            x, y = heapq.heappop(A), heapq.heappop(A)
            res += x + y
            heapq.heappush(A, x + y)
        return res
```

</p>


### An explanation of the problem statement:
- Author: IveGot12Months
- Creation Date: Fri Mar 27 2020 20:48:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 27 2020 20:48:45 GMT+0800 (Singapore Standard Time)

<p>
It killed me to understand this, I will try to explain it as best as I can.

The question IS NOT asking what the length of the stick will be at the end. It\'s asking to find the cost of assembling the final stick.

So before any work is done, we have 3 sticks, and 0 work completed:

0.) sticks: [2, 4, 3], work == 0

1.) in the first iteration, we combine the sticks of length 2 and 3, for a workload of 5:
sticks: [4, 5] (because 2 + 3), work == 5

2.) Next, we combine the sticks 4 and 5.
sticks: [9] (because 4 + 5) work == 9

Now, to compute the final result, sum up all of the WORK from ALL OF THE STEPS.

work 1 == 5
work 2 == 9

result = 9 + 5 == 14.

Please let me know if you have any further questions, I don\'t want anyone else to suffer like I did!


</p>


