---
title: "Print Words Vertically"
weight: 1243
#id: "print-words-vertically"
---
## Description
<div class="description">
<p>Given a string <code>s</code>.&nbsp;Return&nbsp;all the words vertically in the same order in which they appear in <code>s</code>.<br />
Words are returned as a list of strings, complete with&nbsp;spaces when is necessary. (Trailing spaces are not allowed).<br />
Each word would be put on only one column and that in one column there will be only one word.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;HOW ARE YOU&quot;
<strong>Output:</strong> [&quot;HAY&quot;,&quot;ORO&quot;,&quot;WEU&quot;]
<strong>Explanation: </strong>Each word is printed vertically. 
 &quot;HAY&quot;
&nbsp;&quot;ORO&quot;
&nbsp;&quot;WEU&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;TO BE OR NOT TO BE&quot;
<strong>Output:</strong> [&quot;TBONTB&quot;,&quot;OEROOE&quot;,&quot;   T&quot;]
<strong>Explanation: </strong>Trailing spaces is not allowed. 
&quot;TBONTB&quot;
&quot;OEROOE&quot;
&quot;   T&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;CONTEST IS COMING&quot;
<strong>Output:</strong> [&quot;CIC&quot;,&quot;OSO&quot;,&quot;N M&quot;,&quot;T I&quot;,&quot;E N&quot;,&quot;S G&quot;,&quot;T&quot;]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 200</code></li>
	<li><code>s</code>&nbsp;contains only upper case English letters.</li>
	<li>It&#39;s guaranteed that there is only one&nbsp;space between 2 words.</li>
</ul>
</div>

## Tags
- String (string)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] 1-Line Zip Longest
- Author: lee215
- Creation Date: Sun Jan 19 2020 12:03:10 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 23 2020 20:31:23 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
`rstrip` help remove the trailling space.
`zip_longest`, as it say, will zip the longest array.
Default `fillvalue` value is `None`, here we use empty space `\' \'`.
<br>

**Python3:**
```python
    def printVertically(self, s):
        return [\'\'.join(a).rstrip() for a in itertools.zip_longest(*s.split(), fillvalue=\' \')]
```

</p>


### [Java/Python 3] Straight forward codes w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Jan 19 2020 12:07:01 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 21 2020 01:01:30 GMT+0800 (Singapore Standard Time)

<p>
1. Find the size of the longest word;
2. Iterate chars of all words at index 0, 1, ..., max - 1, pack with space if the index if out of the range of current word;
3. Remove the trailing space for each string.
 
```java
    public List<String> printVertically(String s) {
        String[] words = s.split(" ");
        int mx = 0;
        for (int i = 0; i < words.length; ++i)
            mx = Math.max(mx, words[i].length());
        List<String> ans = new ArrayList<>();
        for (int i = 0; i < mx; ++i) {
            StringBuilder sb = new StringBuilder();
            for (String word : words)
                sb.append(i < word.length() ? word.charAt(i) : " ");
            while (sb.charAt(sb.length() - 1) == \' \')
                sb.deleteCharAt(sb.length() - 1); // remove trailing space.
            ans.add(sb.toString());
        }
        return ans;
    }
```
```python
    def printVertically(self, s: str) -> List[str]:
        words = s.split(\' \')
        mx = max(len(word) for word in words)
        ans = []
        for i in range(mx):
            l = []
            for word in words:
                l.append(word[i] if i < len(word) else \' \')
            ans.append(\'\'.join(l).rstrip())
        return ans
```
or make it shorter as follows:
```python
    def printVertically(self, s: str) -> List[str]:
        words = s.split(\' \')
        mx = max(len(word) for word in words)
        return [\'\'.join([word[i] if i < len(word) else \' \' for word in words]).rstrip() for i in range(mx)]
```
**Analysis:**

Time & space: `O(n)`. where `n = s.length()`
</p>


### Python zip_longest with steps
- Author: StefanPochmann
- Creation Date: Sun Jan 19 2020 12:03:09 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jan 22 2020 04:56:57 GMT+0800 (Singapore Standard Time)

<p>
With matrices or so, we\'d use `zip` to transpose (= flip so that rows become columns and columns become rows). But `zip` stops at the shortest, and here the columns aren\'t all the same length. So we use `zip_longest` instead and tell it to fill with spaces.

Nice version in three steps:
```
def printVertically(self, s):
    columns = s.split()
    rows = itertools.zip_longest(*columns, fillvalue=\' \')
    return [\'\'.join(row).rstrip() for row in rows]
```
Oneliner (which is what I wrote during the contest):
```
def printVertically(self, s):
    return [\'\'.join(r).rstrip() for r in itertools.zip_longest(*s.split(), fillvalue=\' \')]
```

</p>


