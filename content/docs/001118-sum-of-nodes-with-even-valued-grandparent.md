---
title: "Sum of Nodes with Even-Valued Grandparent"
weight: 1118
#id: "sum-of-nodes-with-even-valued-grandparent"
---
## Description
<div class="description">
<p>Given a binary tree, return the sum of values of nodes with even-valued grandparent.&nbsp; (A <em>grandparent</em> of a node is the parent of its parent, if it exists.)</p>

<p>If there are no nodes with an even-valued grandparent, return&nbsp;<code>0</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/07/24/1473_ex1.png" style="width: 350px; height: 214px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [6,7,8,2,7,1,3,9,null,1,4,null,null,null,5]
<strong>Output:</strong> 18
<b>Explanation:</b> The red nodes are the nodes with even-value grandparent while the blue nodes are the even-value grandparents.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is between&nbsp;<code>1</code>&nbsp;and&nbsp;<code>10^4</code>.</li>
	<li>The value of nodes is between&nbsp;<code>1</code>&nbsp;and&nbsp;<code>100</code>.</li>
</ul>
</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 5 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] 1-Line Recursive Solution
- Author: lee215
- Creation Date: Sun Jan 12 2020 00:03:01 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 06 2020 10:08:13 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Let children know who their grandparent is.
<br>

## **Explanation**
Assume `node` has `parent.val = 1` and `grandparent.val = 1`.
Recursive iterate the whole tree and pass on the value of parent and grandparent.
Count the `node.val` when grandparant is even-valued.
<br>

## **Complexity**
Time `O(N)`
Space `O(height)`
<br>

**Java:**
```java
    public int sumEvenGrandparent(TreeNode root) {
        return helper(root, 1, 1);
    }

    public int helper(TreeNode node, int p, int gp) {
        if (node == null) return 0;
        return helper(node.left, node.val, p) + helper(node.right, node.val, p) + (gp % 2 == 0 ? node.val : 0);
    }
```

**1-line C++:**
```cpp
    int sumEvenGrandparent(TreeNode* root, int p = 1, int gp = 1) {
        return root ? sumEvenGrandparent(root->left, root->val, p)
               + sumEvenGrandparent(root->right, root->val, p)
               + (gp % 2 ? 0 : root->val)  : 0;
    }
```

**1-line Python:**
```python
    def sumEvenGrandparent(self, root, p=1, gp=1):
        return self.sumEvenGrandparent(root.left, root.val, p) \
            + self.sumEvenGrandparent(root.right, root.val, p) \
            + root.val * (1 - gp % 2) if root else 0
```

</p>


### Easy DFS solution
- Author: JatinYadav96
- Creation Date: Sun Jan 12 2020 00:13:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 12 2020 00:13:54 GMT+0800 (Singapore Standard Time)

<p>
```
    public static int sum = 0;

    public int sumEvenGrandparent(TreeNode root) {
        dfs(root, null, null);
        return sum;
    }

    void dfs(TreeNode current, TreeNode parent, TreeNode grandParent) {
        if (current == null) return; // base case 
        if (grandParent != null && grandParent.val % 2 == 0) {
            sum += current.val;
        }
        dfs(current.left, current, parent);// ( newChild, parent, GrandParent)
        dfs(current.right, current, parent);
    }

```
</p>


### Easy BFS solution in Java
- Author: nikcode
- Creation Date: Fri Jan 17 2020 23:31:47 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jan 17 2020 23:31:47 GMT+0800 (Singapore Standard Time)

<p>
```
public int sumEvenGrandparent(TreeNode root) {
        int sum = 0;
        Queue<TreeNode> q = new LinkedList();
        q.add(root);
        
        //LevelOrderTraversal
        while(!q.isEmpty()) {
            TreeNode node = q.poll();
            if(node.left != null) {
                q.add(node.left);
                if(node.val % 2 == 0) {
                    if(node.left.left != null) {
                        sum += node.left.left.val;
                    }
                    if(node.left.right != null) {
                        sum += node.left.right.val;
                    }
                }
            }

            if(node.right != null) {
                q.add(node.right);
                if(node.val % 2 == 0) {
                    if(node.right.left != null) {
                        sum += node.right.left.val;
                    }
                    if(node.right.right != null) {
                        sum += node.right.right.val;
                    }
                }
            }
        }
        
        return sum;
    }
```
</p>


