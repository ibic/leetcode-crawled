---
title: "Calculate Salaries"
weight: 1582
#id: "calculate-salaries"
---
## Description
<div class="description">
<p>Table <code>Salaries</code>:</p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| company_id    | int     |
| employee_id   | int     |
| employee_name | varchar |
| salary        | int     |
+---------------+---------+
(company_id, employee_id) is the primary key for this table.
This table contains the company id, the id, the name and the salary for an employee.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the salaries of the employees after applying taxes.</p>

<p>The tax rate is calculated for each company based on the following criteria:</p>

<ul>
	<li>0% If the max salary of any employee in the company is less than&nbsp;1000$.</li>
	<li>24% If the max salary of any employee in the company is in the range [1000, 10000] inclusive.</li>
	<li>49% If the max salary of any employee in the company is greater than 10000$.</li>
</ul>

<p>Return the result table <b>in any order</b>. Round the salary to the nearest integer.</p>

<p>The query result format is in the following example:</p>

<pre>
Salaries table:
+------------+-------------+---------------+--------+
| company_id | employee_id | employee_name | salary |
+------------+-------------+---------------+--------+
| 1          | 1           | Tony          | 2000   |
| 1          | 2           | Pronub        | 21300  |
| 1          | 3           | Tyrrox        | 10800  |
| 2          | 1           | Pam           | 300    |
| 2          | 7           | Bassem        | 450    |
| 2          | 9           | Hermione      | 700    |
| 3          | 7           | Bocaben       | 100    |
| 3          | 2           | Ognjen        | 2200   |
| 3          | 13          | Nyancat       | 3300   |
| 3          | 15          | Morninngcat   | 1866   |
+------------+-------------+---------------+--------+

Result table:
+------------+-------------+---------------+--------+
| company_id | employee_id | employee_name | salary |
+------------+-------------+---------------+--------+
| 1          | 1           | Tony          | 1020   |
| 1          | 2           | Pronub        | 10863  |
| 1          | 3           | Tyrrox        | 5508   |
| 2          | 1           | Pam           | 300    |
| 2          | 7           | Bassem        | 450    |
| 2          | 9           | Hermione      | 700    |
| 3          | 7           | Bocaben       | 76     |
| 3          | 2           | Ognjen        | 1672   |
| 3          | 13          | Nyancat       | 2508   |
| 3          | 15          | Morninngcat   | 5911   |
+------------+-------------+---------------+--------+
For company 1, Max salary is 21300. Employees in company 1 have taxes = 49%
For company 2, Max salary is 700. Employees in company 2 have taxes = 0%
For company 3, Max salary is 7777. Employees in company 3 have taxes = 24%
The salary after taxes = salary - (taxes percentage / 100) * salary
For example, Salary for Morninngcat (3, 15) after taxes = 7777 - 7777 * (24 / 100) = 7777 - 1866.48 = 5910.52, which is rounded to 5911.
</pre>
</div>

## Tags


## Companies
- Startup - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple, easy MySql sol fast 99%, memory 100%, Using case and Join
- Author: jitenderphogat
- Creation Date: Sat Jun 06 2020 21:28:35 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 14 2020 09:14:24 GMT+0800 (Singapore Standard Time)

<p>
Simple MySQL using Case and Join!!

Please upvote if it\'s useful!!

```
select s.company_id, s.employee_id, s.employee_name,
round(
		case when x.max_sal between 1000 and 10000 then salary * 0.76
		when x.max_sal > 10000 then salary * 0.51 else salary end, 0) as salary

from salaries s inner join
(select company_id, max(salary) max_sal from salaries group by company_id) x

on s.company_id = x.company_id;
```
</p>


### Simple MySQL using Common Table Expression
- Author: ringabel
- Creation Date: Fri Jun 05 2020 15:48:06 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 05 2020 15:48:06 GMT+0800 (Singapore Standard Time)

<p>
WITH tax_multiplier AS (
    SELECT company_id, 
        CASE WHEN MAX(salary) < 1000 THEN 1.0 
             WHEN MAX(salary) <= 10000 THEN 0.76 
             ELSE 0.51 END AS mult
    FROM Salaries
    GROUP BY company_id
)
SELECT s.company_id, s.employee_id, s.employee_name, ROUND(s.salary * t.mult) AS salary
FROM Salaries s JOIN tax_multiplier t ON s.company_id = t.company_id;
</p>


### MySQL: CASE statement & window function
- Author: Huasah
- Creation Date: Sun Sep 27 2020 15:08:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 27 2020 15:10:10 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT company_id, employee_id, employee_name, 
CASE WHEN max_salary < 1000 THEN ROUND(salary * (1 - 0.00), 0)
     WHEN max_salary >= 1000 AND max_salary <= 10000 THEN ROUND(salary * (1 - 0.24), 0)
     WHEN max_salary > 10000 THEN ROUND(salary * (1 - 0.49), 0) END AS \'salary\'
FROM(SELECT *, MAX(salary) OVER (PARTITION BY company_id) AS \'max_salary\' FROM Salaries) M;
```
</p>


