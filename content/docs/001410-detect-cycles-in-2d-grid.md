---
title: "Detect Cycles in 2D Grid"
weight: 1410
#id: "detect-cycles-in-2d-grid"
---
## Description
<div class="description">
<p>Given a 2D array of characters&nbsp;<code>grid</code>&nbsp;of size <code>m x n</code>, you need to find if there exists any cycle consisting of the <strong>same value</strong>&nbsp;in&nbsp;<code>grid</code>.</p>

<p>A cycle is a path of <strong>length 4&nbsp;or more</strong>&nbsp;in the grid that starts and ends at the same cell. From a given cell, you can move to one of the cells adjacent to it - in one of the four directions (up, down, left, or right), if it has the <strong>same value</strong> of the current cell.</p>

<p>Also, you cannot move to the cell that you visited in your last move. For example, the cycle&nbsp;<code>(1, 1) -&gt; (1, 2) -&gt; (1, 1)</code>&nbsp;is invalid because from&nbsp;<code>(1, 2)</code>&nbsp;we visited&nbsp;<code>(1, 1)</code>&nbsp;which was the last visited cell.</p>

<p>Return&nbsp;<code>true</code>&nbsp;if any cycle of the same value exists in&nbsp;<code>grid</code>, otherwise, return&nbsp;<code>false</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/07/15/1.png" style="width: 231px; height: 152px;" /></strong></p>

<pre>
<strong>Input:</strong> grid = [[&quot;a&quot;,&quot;a&quot;,&quot;a&quot;,&quot;a&quot;],[&quot;a&quot;,&quot;b&quot;,&quot;b&quot;,&quot;a&quot;],[&quot;a&quot;,&quot;b&quot;,&quot;b&quot;,&quot;a&quot;],[&quot;a&quot;,&quot;a&quot;,&quot;a&quot;,&quot;a&quot;]]
<strong>Output:</strong> true
<strong>Explanation: </strong>There are two valid cycles shown in different colors in the image below:
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/15/11.png" style="width: 225px; height: 163px;" />
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/07/15/22.png" style="width: 236px; height: 154px;" /></strong></p>

<pre>
<strong>Input:</strong> grid = [[&quot;c&quot;,&quot;c&quot;,&quot;c&quot;,&quot;a&quot;],[&quot;c&quot;,&quot;d&quot;,&quot;c&quot;,&quot;c&quot;],[&quot;c&quot;,&quot;c&quot;,&quot;e&quot;,&quot;c&quot;],[&quot;f&quot;,&quot;c&quot;,&quot;c&quot;,&quot;c&quot;]]
<strong>Output:</strong> true
<strong>Explanation: </strong>There is only one valid cycle highlighted in the image below:
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/15/2.png" style="width: 229px; height: 157px;" />
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/07/15/3.png" style="width: 183px; height: 120px;" /></strong></p>

<pre>
<strong>Input:</strong> grid = [[&quot;a&quot;,&quot;b&quot;,&quot;b&quot;],[&quot;b&quot;,&quot;z&quot;,&quot;b&quot;],[&quot;b&quot;,&quot;b&quot;,&quot;a&quot;]]
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m == grid.length</code></li>
	<li><code>n == grid[i].length</code></li>
	<li><code>1 &lt;= m &lt;= 500</code></li>
	<li><code>1 &lt;= n &lt;= 500</code></li>
	<li><code>grid</code>&nbsp;consists only of lowercase&nbsp;English letters.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Nutanix - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### DFS | Simple Explanation
- Author: khaufnak
- Creation Date: Sun Aug 23 2020 00:00:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 23 2020 01:31:29 GMT+0800 (Singapore Standard Time)

<p>
At each point (i, j) in grid, do following:
* Start DFS from (i, j).
* Don\'t visit the last visited point as stated in question.
* Only visit a point if it has same character as starting position
* If you still reach a visited point again, there is a cycle.

```
class Solution {
    int[] DIR_X = {1, -1, 0, 0};
    int[] DIR_Y = {0, 0, 1, -1};
	
    private boolean dfs(int curX, int curY, int lastX, int lastY, int n, int m, boolean[][] vis, char[][] grid, char startChar) {
        vis[curX][curY] = true;
        boolean hasCycle = false;
        // Visit all directions
        for (int i = 0; i < 4; ++i) {
            int newX = curX + DIR_X[i];
            int newY = curY + DIR_Y[i];
            // Valid point?
            if (newX >= 0 && newX < n && newY >= 0 && newY < m) {
                // Don\'t visit last visited point
                if (!(newX == lastX && newY == lastY)) {
                    // Only visit nodes that equal start character
                    if (grid[newX][newY] == startChar) {
                        if (vis[newX][newY]) {
						    // Still visited? There is a cycle.
                            return true;
                        } else {
                            hasCycle |= dfs(newX, newY, curX, curY, n, m, vis, grid, startChar);
                        }
                    }
                }
            }
        }
        return hasCycle;
    }
	
    public boolean containsCycle(char[][] grid) {
        int n = grid.length;
        int m = grid[0].length;
        boolean[][] vis = new boolean[n][m];
        boolean hasCycle = false;
        for (int i = 0; i < grid.length; ++i) {
            for (int j = 0; j < grid[i].length; ++j) {
                if (!vis[i][j]) {
                    hasCycle |= dfs(i, j, -1, -1, n, m, vis, grid, grid[i][j]);
                }
            }
        }
        return hasCycle;
    }
}
```
Complexity: ```O(n*m)```
</p>


### C++ BFS
- Author: votrubac
- Creation Date: Sun Aug 23 2020 00:00:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 23 2020 08:21:17 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**
We have a cycle if we arrive at the same cell from multiple directions during BFS.

For simplicity, I am marking a cell as \'visited\' by subtracting 26 from it. If needed, we can restore the grid to its prestine state in the end.

```cpp
vector<int> d = {1, 0, -1, 0, 1};
bool containsCycle(vector<vector<char>>& g) {
    for (int i = 0; i < g.size(); ++i)
        for (int j = 0; j < g[i].size(); ++j) {
            if (g[i][j] >= \'a\') {
                char val = g[i][j];
                vector<pair<int, int>> q = {{i, j}};
                while (!q.empty()) {
                    vector<pair<int, int>> q1;
                    for (auto [x, y] : q) {
                        if (g[x][y] < \'a\')
                            return true;
                        g[x][y] -= 26;
                        for (auto k = 0; k < 4; ++k) {
                            int dx = x + d[k], dy = y + d[k + 1];
                            if (dx >= 0 && dy >= 0 && dx < g.size() && dy < g[dx].size() && g[dx][dy] == val)
                                q1.push_back({dx, dy});
                        }
                    }
                    swap(q, q1);
                }
            }
        }
    return false;
}
```
</p>


### C++ | DFS | Easy to understand
- Author: di_b
- Creation Date: Sun Aug 23 2020 00:05:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 23 2020 00:05:19 GMT+0800 (Singapore Standard Time)

<p>
```
    vector<int> dir = { 0, 1, 0, -1, 0 }; 
    bool isCyclic(vector<vector<char>>& grid, vector<vector<bool>>& visited, int i, int j, int x, int y)
    {
        visited[i][j] = true;
        for(int d = 0; d < 4; ++d)
        {
            int a = i+dir[d];
            int b = j+dir[d+1];
            if(a >= 0 && a < grid.size() && b >= 0 && b < grid[0].size() && grid[a][b] == grid[i][j] && !(x == a && y == b))
                if(visited[a][b] || isCyclic(grid, visited, a,b,i,j))
                    return true;
        }
        return false;
    }
    bool containsCycle(vector<vector<char>>& grid) {
        int n = grid.size(), m = grid[0].size();
        vector<vector<bool>> visited(n, vector<bool>(m, false));
        for(int i = 0; i < n; ++i)
            for(int j = 0; j < m; ++j)
                if(!visited[i][j] && isCyclic(grid, visited, i, j, -1, -1))
                    return true;
        return false;
    }
</p>


