---
title: "Repeated Substring Pattern"
weight: 436
#id: "repeated-substring-pattern"
---
## Description
<div class="description">
<p>Given a non-empty string check if it can be constructed by taking a substring of it and appending multiple copies of the substring together. You may assume the given string consists of lowercase English letters only and its length will not exceed 10000.</p>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> &quot;abab&quot;
<b>Output:</b> True
<b>Explanation:</b> It&#39;s the substring &quot;ab&quot; twice.
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> &quot;aba&quot;
<b>Output:</b> False
</pre>

<p><b>Example 3:</b></p>

<pre>
<b>Input:</b> &quot;abcabcabcabc&quot;
<b>Output:</b> True
<b>Explanation:</b> It&#39;s the substring &quot;abc&quot; four times. (And the substring &quot;abcabc&quot; twice.)
</pre>

</div>

## Tags
- String (string)

## Companies
- Google - 2 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Overview

The problem could be solved in many ways. 
Easy approaches have $$\mathcal{O}(N^2)$$ time complexity, though 
one could improve it by using one of 
[string searching algorithms](https://en.wikipedia.org/wiki/String-searching_algorithm#Single-pattern_algorithms).

![overview](../Figures/459/overall3.png)

<br />
<br />


---
#### Approach 1: Regex

To use regex during the interviews is like to use built-in functions,
the community has no single opinion about it yet, and it's a sort of risk. 

![python_regex](../Figures/459/python_regex3.png)

---

![java_regex](../Figures/459/java_regex2.png)

**Implementation**

<iframe src="https://leetcode.com/playground/GgXZXJnA/shared" frameBorder="0" width="100%" height="174" name="GgXZXJnA"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N^2)$$ because we use greedy regex
pattern. Once we have a `+`, the pattern is greedy.

    The difference between the greedy and the non-greedy match is the following:

    - the non-greedy match will try to match as few repetitions of 
    the quantified pattern as possible.
     
    - the greedy match will try to match as many repetitions as possible. 
    
    The worst-case situation here is to check all possible pattern lengths
    from `N` to `1` that would result in $$\mathcal{O}(N^2)$$ time complexity.
    
* Space complexity: $$\mathcal{O}(1)$$. We don't use any 
additional data structures, and everything depends on internal regex implementation,
which is evolving quite fast nowadays. [If you're interested to dig depeer,
here is a famous article by Russ Cox](https://swtch.com/~rsc/regexp/regexp1.html) 
which inspired a lot of [discussions and code changes in Python community](https://mail.python.org/pipermail/python-ideas/2007-April/000407.html). 
 
<br />
<br />


---
#### Approach 2: Concatenation

Repeated pattern string looks like `PatternPattern`, and the others
like `Pattern1Pattern2`.

Let's double the input string:

`PatternPattern` --> `PatternPatternPatternPattern`

`Pattern1Pattern2` --> `Pattern1Pattern2Pattern1Pattern2`

Now let's cut the first and the last characters in the doubled string: 

`PatternPattern` --> `*atternPatternPatternPatter*`

`Pattern1Pattern2` --> `*attern1Pattern2Pattern1Pattern*`

It's quite evident that if the new string contains the input string,
the input string is a repeated pattern string. 

**Implementation**

<iframe src="https://leetcode.com/playground/anmdtNtt/shared" frameBorder="0" width="100%" height="157" name="anmdtNtt"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N^2)$$ because of the way 
`in` and `contains` are implemented. 

* Space complexity: $$\mathcal{O}(N)$$, the space is implicitly used to keep 
`s + s` string. 
<br />
<br />


---
#### Approach 3: Find Divisors + Rabin-Karp

**Rabin-Karp**

Rabin-Karp is a linear-time $$\mathcal{O}(N)$$ string searching algorithm:

- Move a sliding window of length $$L$$ along the string of length $$N$$.

- Check hash of the string in the sliding window.

![rabin_karp](../Figures/459/rabin.png)

In some situations, one has to implement a particular hash 
algorithm to fit in a linear time, for example, we used 
_rolling hash algorithm_ for the problem
[Longest Duplicate Substring](https://leetcode.com/problems/longest-duplicate-substring/). 

For the current problem the standard `hash` / `hashCode` 
is enough because the idea is to check only lengths `L`, which are divisors of `N`.
This way we're not _sliding_, we're _jumping_:

- the first string is `0..L`

- the second string is `L..2L`

- ...

- the last string is `N - L..N` 

To copy characters in sliding window takes time `L`,
to compute hash - time `L` as well. 
In total, there are `N / L` substrings, that makes it all work
in a linear time $$\mathcal{O}(N)$$.

**Find divisors**

Now the only problem is to find divisors of `N`. 
Let's iterate to the square root of `N`, 
and for each identified divisor `i` 
calculate the paired divisor `N / i`.

**Algorithm**

- Deal with base cases: `n <= 2`. 

- Iterate from $$\sqrt{n}$$ to 1. 

    - For each divisor `n % i == 0`:
    
        - Compute paired divisor `n / i`.
        
        - Use Rabin-Karp to check substrings of the lengths `l = i` and `l = n / i`:
        
            - Take as a reference hash `first_hash` the hash of the first 
            substring of length `l`.
            
            - Jump along the string with a step of length `l` while
            the hash of the current substring is equal to `first_hash`.
            
            - If the hashes of all substrings along the way
            are equal, the input string consists of repeated patterns
            of length `l`. Return True. 
               
> Side note. The good practice is to verify the equality of 
two substrings after the hash match. This logic is not hard to add, 
and it could bring you kudos during the interview.

**Implementation**

<iframe src="https://leetcode.com/playground/g39p73rZ/shared" frameBorder="0" width="100%" height="500" name="g39p73rZ"></iframe>

**Complexity Analysis**

* Time complexity: up to $$\mathcal{O}(N \sqrt{N})$$. 
$$\mathcal{O}(\sqrt{N})$$ to compute all divisors and 
$$\mathcal{O}(N)$$ for each divisor "verification". 
That's an upper-bound estimation because 
[divisor function grows slower than $$\sqrt{N}$$](https://en.wikipedia.org/wiki/Divisor_function#Growth_rate).

* Space complexity: up to $$\mathcal{O}(\sqrt{N})$$ to keep a copy 
of each substring during the hash computation. 
<br />
<br />


---
#### Approach 4: Knuth-Morris-Pratt Algorithm (KMP)

**Lookup Table**

Rabin-Karp is the best fit for the _multiple_ pattern search, 
whereas KMP is typically used for the _single_ pattern search.


The key to KMP is the _partial match table_, often called _lookup table_,
or _failure function table_.
**It stores the length of the longest prefix that is also a suffix.**

Here are two examples

![kmp_example1](../Figures/459/kmp.png)

---

![kmp_example2](../Figures/459/kmp2.png)

**How to Get an Answer**

Once we have a lookup table, we know the length `l` of common prefix/suffix
for the input string: `l = dp[n - 1]`.

That means that `n - l` should the length of the repeated sequence.
To confirm that, one should verify if `n - l` is a divisor of `n`. 

![kmp_solution_example_1](../Figures/459/repeated_pattern_ex2.png)

---

![kmp_solution_example_2](../Figures/459/ex3.png)

**Algorithm**

- Construct lookup table:

    - `dp[0] = 0` since one character is not enough to speak about
    proper prefix / suffix.
    
    - Iterate over `i` from `1` to `n`:
    
        - Introduce the second pointer `j = dp[i - 1]`.
        
        - While `j > 0` and there is no match `s[i] != s[j]`,
        do one step back to consider a shorter prefix: `j = dp[j - 1]`.
        
        - If we found a match `s[i] == s[j]`, move forward: `j += 1`
        
        - Write down the length of common prefix / suffix: `dp[i] = j`.
    
- Now we have a length of common prefix / suffix for the entire string:
`l = dp[n - 1]`.

- The string is a repeated pattern string if this length is nonzero and
`n - l` is a divisor of `n`. Return `l != 0 and n % (n - l) == 0`.

**Implementation**

<iframe src="https://leetcode.com/playground/Pqv9Grov/shared" frameBorder="0" width="100%" height="500" name="Pqv9Grov"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$. 
During the execution, `j` could be decreased at most $$N$$ times and then increased 
at most $$N$$ times, that makes overall execution time to be linear $$\mathcal{O}(N)$$.

* Space complexity: $$\mathcal{O}(N)$$ to keep the lookup table.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy python solution with explaination
- Author: rsrs3
- Creation Date: Thu Nov 17 2016 01:03:09 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 09:07:46 GMT+0800 (Singapore Standard Time)

<p>
Basic idea:

1) First char of input string is first char of repeated substring
2) Last char of input string is last char of repeated substring
3) Let S1 = S + S (where S in input string)
4) Remove 1 and last char of S1. Let this be S2
5) If S exists in S2 then return true else false
6) Let i be index in S2 where S starts then repeated substring length i + 1 and repeated substring S[0: i+1] 

```
def repeatedSubstringPattern(self, str):

        """
        :type str: str
        :rtype: bool
        """
        if not str:
            return False
            
        ss = (str + str)[1:-1]
        return ss.find(str) != -1
```
</p>


### Java Simple Solution with Explanation
- Author: fabrizio3
- Creation Date: Tue Nov 15 2016 17:21:03 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 13:59:50 GMT+0800 (Singapore Standard Time)

<p>
```
public boolean repeatedSubstringPattern(String str) {
	int l = str.length();
	for(int i=l/2;i>=1;i--) {
		if(l%i==0) {
			int m = l/i;
			String subS = str.substring(0,i);
			StringBuilder sb = new StringBuilder();
			for(int j=0;j<m;j++) {
				sb.append(subS);
			}
			if(sb.toString().equals(str)) return true;
		}
	}
	return false;
}
```
1. The length of the repeating substring must be a divisor of the length of the input string
2. Search for all possible divisor of `str.length`, starting for `length/2`
3. If `i` is a divisor of `length`, repeat the substring from `0` to `i` the number of times `i` is contained in `s.length`
4. If the repeated substring is equals to the input `str` return `true`
</p>


### Simple Java solution, 2 lines
- Author: fhqplzj
- Creation Date: Sun Dec 04 2016 22:03:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 13:59:11 GMT+0800 (Singapore Standard Time)

<p>
```
    public boolean repeatedSubstringPattern(String str) {
        String s = str + str;
        return s.substring(1, s.length() - 1).contains(str);
    }
```
</p>


