---
title: "Number of Subsequences That Satisfy the Given Sum Condition"
weight: 1373
#id: "number-of-subsequences-that-satisfy-the-given-sum-condition"
---
## Description
<div class="description">
<p>Given an array of integers <code>nums</code> and an integer <code>target</code>.</p>

<p>Return the number of <strong>non-empty</strong> subsequences of <code>nums</code> such that the sum of the minimum and maximum element on it is less or equal than <code>target</code>.</p>

<p>Since the answer&nbsp;may be too large,&nbsp;return it modulo&nbsp;10^9 + 7.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,5,6,7], target = 9
<strong>Output:</strong> 4
<strong>Explanation: </strong>There are 4 subsequences that satisfy the condition.
[3] -&gt; Min value + max value &lt;= target (3 + 3 &lt;= 9)
[3,5] -&gt; (3 + 5 &lt;= 9)
[3,5,6] -&gt; (3 + 6 &lt;= 9)
[3,6] -&gt; (3 + 6 &lt;= 9)
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,3,6,8], target = 10
<strong>Output:</strong> 6
<strong>Explanation: </strong>There are 6 subsequences that satisfy the condition. (nums can have repeated numbers).
[3] , [3] , [3,3], [3,6] , [3,6] , [3,3,6]</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [2,3,3,4,6,7], target = 12
<strong>Output:</strong> 61
<strong>Explanation: </strong>There are 63 non-empty subsequences, two of them don&#39;t satisfy the condition ([6,7], [7]).
Number of valid subsequences (63 - 2 = 61).
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [5,2,4,1,7,6,8], target = 16
<strong>Output:</strong> 127
<strong>Explanation: </strong>All non-empty subset satisfy the condition (2^7 - 1) = 127</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 10^6</code></li>
	<li><code>1 &lt;= target &lt;= 10^6</code></li>
</ul>

</div>

## Tags
- Sort (sort)
- Sliding Window (sliding-window)

## Companies
- Facebook - 4 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Two Sum
- Author: lee215
- Creation Date: Sun Jun 28 2020 13:10:46 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 17 2020 16:14:12 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Almost same as problem two sum.
If we want to know the count of subarray in sorted array `A`,
then it\'s exactly the same.
Make sure you can do two sum before continue.
<br>

# **Explanation**
Sort input `A` first,
For each `A[i]`, find out the maximum `A[j]`
that `A[i] + A[j] <= target`.

For each elements in the subarray `A[i+1] ~ A[j]`,
we can pick or not pick,
so there are `2 ^ (j - i)` subsequences in total.
So we can update `res = (res + 2 ^ (j - i)) % mod`.

We don\'t care the original elements order,
we only want to know the count of sub sequence.
So we can sort the original `A`, and the result won\'t change.
<br>

# **Complexity**
Time `O(NlogN)`
Space `O(1)` for python
(`O(N)` space for java and c++ can be save anyway)
<br>

**Java**
```java
    public int numSubseq(int[] A, int target) {
        Arrays.sort(A);
        int res = 0, n = A.length, l = 0, r = n - 1, mod = (int)1e9 + 7;
        int[] pows = new int[n];
        pows[0] = 1;
        for (int i = 1 ; i < n ; ++i)
            pows[i] = pows[i - 1] * 2 % mod;
        while (l <= r) {
            if (A[l] + A[r] > target) {
                r--;
            } else {
                res = (res + pows[r - l++]) % mod;
            }
        }
        return res;
    }
```
**C++**
```cpp
    int numSubseq(vector<int>& A, int target) {
        sort(A.begin(), A.end());
        int res = 0, n = A.size(), l = 0, r = n - 1, mod = 1e9 + 7;
        vector<int> pows(n, 1);
        for (int i = 1 ; i < n ; ++i)
            pows[i] = pows[i - 1] * 2 % mod;
        while (l <= r) {
            if (A[l] + A[r] > target) {
                r--;
            } else {
                res = (res + pows[r - l++]) % mod;
            }
        }
        return res;
    }
```
**Python:**
```py
    def numSubseq(self, A, target):
        A.sort()
        l, r = 0, len(A) - 1
        res = 0
        mod = 10**9 + 7
        while l <= r:
            if A[l] + A[r] > target:
                r -= 1
            else:
                res += pow(2, r - l, mod)
                l += 1
        return res % mod
```
<br>

</p>


### C++ Precomputed Pow
- Author: votrubac
- Creation Date: Sun Jun 28 2020 13:28:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 13:53:32 GMT+0800 (Singapore Standard Time)

<p>
Similar to the other solutions, but with precomputing power of 2 modulo `10 ^ 9 + 7`.

Well, Python folks are in luck. In C++, computing `2 ^ n` inline gives TLE. I could not find an efficient modulo power function. So, I just precomputed the `pow(i, 2) % mod` values iterativelly.

```cpp
int numSubseq(vector<int>& nums, int target) {
    int res = 0, mod = 1000000007;
    vector<int> pre = {0, 1};
    for (auto i = pre.size(); i <= nums.size(); ++i)
        pre.push_back((pre.back() << 1) % mod);       
    sort(begin(nums), end(nums));
    for (int i = 0, j = nums.size() - 1; i <= j; ++i) {
        while (i <= j && nums[i] + nums[j] > target)
            --j;
        res = (res + pre[j - i + 1]) % mod;
    }
    return res;
}
```
</p>


### Python Two pointers Complete Explanation
- Author: akhil_ak
- Creation Date: Wed Jul 01 2020 09:13:52 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 01 2020 09:13:52 GMT+0800 (Singapore Standard Time)

<p>
Show some love with upvotes if you found this helpful.Now,into the solution ===>

Sort the array, because subsequence is nothing but a subset.

reason: A window can be maintained [imin,j] such that
if A[imin]+A[j]<=target
    then,
    for all i such that imin<=i<j
        A[imin]+A[i] <= target is true.

        
The idea is to find the imin for every j.
Then,add all the possible subsequences that can be generated from this [imin,j] window.

for a window [imin,j], we can change the max by reducing j.
But,when imin changes the window shifts and the min+max<=target may not
be true anymore.

```
No of subsequences for a window of [imin,j] are calculated as below:
    Example:
        sorted nums : [a,b,c,d]
        lets say a+d<=target.
        no of subsequences are:
            1)when max reduces the min+max cant exceed target,so
              [a,b,c,d], [a,b,c],[a,b],[a] => 3
            2)[a,b,c,d] gives [a,b,d],[a,c,d],[a,d] with same min+max
            3)[a,b,c] gives [a,c] with same min+max
            4)[a,b] doesnt give anything new
            5)[a] also remains same

        The key thing to observe here is that, [a ,......] is the pattern
        for every subsequence.
        The no of subsets of the rest of the numbers
        is the total no of subsequnces. i.e 1<<(numberofelements) or power(2,numberoflements)
```

Time : O(nlogn)
Space:O(1)

```python
class Solution:
    def numSubseq(self, nums: List[int], target: int) -> int:
        nums.sort()
        n = len(nums)
        res = 0
        #one length subsequence
        mod = 10**9 + 7
        i,j = 0,n-1
        
        
        for i in range(n):
            while i<=j and nums[i]+nums[j] > target:
                j-=1
            
            if i<=j and nums[i] + nums[j] <= target:
                res += pow(2,(j-i) , mod)
                res %= mod
        
        return res
```            

</p>


