---
title: "Range Module"
weight: 636
#id: "range-module"
---
## Description
<div class="description">
<p>A Range Module is a module that tracks ranges of numbers. Your task is to design and implement the following interfaces in an efficient manner.</p>

<p><li><code>addRange(int left, int right)</code> Adds the half-open interval <code>[left, right)</code>, tracking every real number in that interval.  Adding an interval that partially overlaps with currently tracked numbers should add any numbers in the interval <code>[left, right)</code> that are not already tracked.</li></p>

<p><li><code>queryRange(int left, int right)</code> Returns true if and only if every real number in the interval <code>[left, right)</code>
 is currently being tracked.</li></p>

<p><li><code>removeRange(int left, int right)</code> Stops tracking every real number currently being tracked in the interval <code>[left, right)</code>.</li></p>

<p><b>Example 1:</b><br />
<pre>
<b>addRange(10, 20)</b>: null
<b>removeRange(14, 16)</b>: null
<b>queryRange(10, 14)</b>: true (Every number in [10, 14) is being tracked)
<b>queryRange(13, 15)</b>: false (Numbers like 14, 14.03, 14.17 in [13, 15) are not being tracked)
<b>queryRange(16, 17)</b>: true (The number 16 in [16, 17) is still being tracked, despite the remove operation)
</pre>
</p>

<p><b>Note:</b>
<li>A half open interval <code>[left, right)</code> denotes all real numbers <code>left <= x < right</code>.</li>

<li><code>0 < left < right < 10^9</code> in all calls to <code>addRange, queryRange, removeRange</code>.</li>
<li>The total number of calls to <code>addRange</code> in a single test case is at most <code>1000</code>.</li>
<li>The total number of calls to <code>queryRange</code> in a single test case is at most <code>5000</code>.</li>
<li>The total number of calls to <code>removeRange</code> in a single test case is at most <code>1000</code>.</li>
</p>
</div>

## Tags
- Segment Tree (segment-tree)
- Ordered Map (ordered-map)

## Companies
- Google - 5 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Machine Zone - 0 (taggedByAdmin: true)
- Coupang - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


#### Approach #1: Maintain Sorted Disjoint Intervals [Accepted]

**Intuition**

Because `left, right < 10^9`, we need to deal with the coordinates abstractly.  Let's maintain some sorted structure of disjoint intervals.  These intervals will be closed (eg. we don't store `[[1, 2], [2, 3]]`; we would store `[[1, 3]]` instead.)

In this article, we will go over Python and Java versions separately, as the data structures available to us that are relevant to the problem are substantially different.

**Algorithm (Python)**

We will maintain the structure as a *list* `self.ranges = []`.  

*Adding a Range*

When we want to add a range, we first find the indices `i, j = self._bounds(left, right)` for which `self.ranges[i: j+1]` touches (in a closed sense - not halfopen) the given interval `[left, right]`.  We can find this in log time by making steps of size 100, 10, then 1 in our linear search from both sides.

Every interval touched by `[left, right]` will be replaced by the single interval `[min(left, self.ranges[i][0]), max(right, self.ranges[j][1])]`.

*Removing a Range*

Again, we use `i, j = self._bounds(...)` to only work in the relevant subset of `self.ranges` that is in the neighborhood of our given range `[left, right)`.  For each interval `[x, y)` from `self.ranges[i:j+1]`, we may have some subset of that interval to the left and/or right of `[left, right)`.  We replace our current interval `[x, y)` with those (up to 2) new intervals.

*Querying a Range*

As the intervals are sorted, we use binary search to find the single interval that could intersect `[left, right)`, then verify that it does.

**Python**
```python
class RangeModule(object):
    def __init__(self):
        self.ranges = []

    def _bounds(self, left, right):
        i, j = 0, len(self.ranges) - 1
        for d in (100, 10, 1):
            while i + d - 1 < len(self.ranges) and self.ranges[i+d-1][1] < left:
                i += d
            while j >= d - 1 and self.ranges[j-d+1][0] > right:
                j -= d
        return i, j

    def addRange(self, left, right):
        i, j = self._bounds(left, right)
        if i <= j:
            left = min(left, self.ranges[i][0])
            right = max(right, self.ranges[j][1])
        self.ranges[i:j+1] = [(left, right)]

    def queryRange(self, left, right):
        i = bisect.bisect_left(self.ranges, (left, float('inf')))
        if i: i -= 1
        return (bool(self.ranges) and
                self.ranges[i][0] <= left and
                right <= self.ranges[i][1])

    def removeRange(self, left, right):
        i, j = self._bounds(left, right)
        merge = []
        for k in xrange(i, j+1):
            if self.ranges[k][0] < left:
                merge.append((self.ranges[k][0], left))
            if right < self.ranges[k][1]:
                merge.append((right, self.ranges[k][1]))
        self.ranges[i:j+1] = merge
```

---

**Algorithm (Java)**

We will maintain the structure as a *TreeSet* `ranges = new TreeSet<Interval>();`.  We introduce a new *Comparable* class `Interval` to represent our half-open intervals.  They compare by *right-most* coordinate as later we will see that it simplifies our work.  Also note that this ordering is consistent with equals, which is important when dealing with *Sets*.

*Adding and Removing a Range*

The basic structure of adding and removing a range is the same.  First, we must iterate over the relevant subset of `ranges`.  This is done using iterators so that we can `itr.remove` on the fly, and breaking when the intervals go too far to the right.

The critical logic of `addRange` is simply to make `left, right` the smallest and largest seen coordinates.  After, we add one giant interval representing the union of all intervals seen that touched `[left, right]`.

The logic of `removeRange` is to remember in `todo` the intervals we wanted to replace the removed interval with.  After, we can add them all back in.

*Querying a Range*

As the intervals are sorted, we search to find the single interval that could intersect `[left, right)`, then verify that it does.  As the TreeSet uses a balanced (red-black) tree, this has logarithmic complexity.

**Java**
```java
class RangeModule {
    TreeSet<Interval> ranges;
    public RangeModule() {
        ranges = new TreeSet();
    }

    public void addRange(int left, int right) {
        Iterator<Interval> itr = ranges.tailSet(new Interval(0, left - 1)).iterator();
        while (itr.hasNext()) {
            Interval iv = itr.next();
            if (right < iv.left) break;
            left = Math.min(left, iv.left);
            right = Math.max(right, iv.right);
            itr.remove();
        }
        ranges.add(new Interval(left, right));
    }

    public boolean queryRange(int left, int right) {
        Interval iv = ranges.higher(new Interval(0, left));
        return (iv != null && iv.left <= left && right <= iv.right);
    }

    public void removeRange(int left, int right) {
        Iterator<Interval> itr = ranges.tailSet(new Interval(0, left)).iterator();
        ArrayList<Interval> todo = new ArrayList();
        while (itr.hasNext()) {
            Interval iv = itr.next();
            if (right < iv.left) break;
            if (iv.left < left) todo.add(new Interval(iv.left, left));
            if (right < iv.right) todo.add(new Interval(right, iv.right));
            itr.remove();
        }
        for (Interval iv: todo) ranges.add(iv);
    }
}

class Interval implements Comparable<Interval>{
    int left;
    int right;

    public Interval(int left, int right){
        this.left = left;
        this.right = right;
    }

    public int compareTo(Interval that){
        if (this.right == that.right) return this.left - that.left;
        return this.right - that.right;
    }
}
```

**Complexity Analysis**

* Time Complexity: Let $$K$$ be the number of elements in `ranges`.  `addRange` and `removeRange` operations have $$O(K)$$ complexity.  `queryRange` has $$O(\log K)$$ complexity.  Because `addRange, removeRange` adds at most 1 interval at a time, you can bound these further.  For example, if there are $$A$$ `addRange`, $$R$$ `removeRange`, and $$Q$$ `queryRange` number of operations respectively, we can express our complexity as $$O((A+R)^2 Q \log(A+R))$$. 

* Space Complexity: $$O(A+R)$$, the space used by `ranges`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java TreeMap
- Author: Aoi-silent
- Creation Date: Sun Oct 22 2017 12:31:38 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 12:52:27 GMT+0800 (Singapore Standard Time)

<p>
Combined a few logics together to make code look cleaner.
`TreeMap<Integer, Integer>`, `key` is the starting index and `value` is the ending index of the interval.
Maintainence is done to make sure no overlap intervals exist in the Map.
```
class RangeModule {
    TreeMap<Integer, Integer> map;
    public RangeModule() {
        map = new TreeMap<>();
    }
    
    public void addRange(int left, int right) {
        if (right <= left) return;
        Integer start = map.floorKey(left);
        Integer end = map.floorKey(right);
        if (start == null && end == null) {
            map.put(left, right);
        } else if (start != null && map.get(start) >= left) {
            map.put(start, Math.max(map.get(end), Math.max(map.get(start), right)));
    	} else {
    	    map.put(left, Math.max(map.get(end), right));
    	}
        // clean up intermediate intervals
        Map<Integer, Integer> subMap = map.subMap(left, false, right, true);
        Set<Integer> set = new HashSet(subMap.keySet());
        map.keySet().removeAll(set);
    }
    
    public boolean queryRange(int left, int right) {
        Integer start = map.floorKey(left);
        if (start == null) return false;
        return map.get(start) >= right;
    }
    
    public void removeRange(int left, int right) {
        if (right <= left) return;
        Integer start = map.floorKey(left);
        Integer end = map.floorKey(right);
    	if (end != null && map.get(end) > right) {
            map.put(right, map.get(end));
    	}
    	if (start != null && map.get(start) > left) {
            map.put(start, left);
    	}
        // clean up intermediate intervals
        Map<Integer, Integer> subMap = map.subMap(left, true, right, false);
        Set<Integer> set = new HashSet(subMap.keySet());
        map.keySet().removeAll(set);
        
    }
}
```
</p>


### Ultra-concise Python (only 6 lines of actual code) (also 236ms, beats 100%)
- Author: gd303
- Creation Date: Wed Sep 12 2018 12:13:44 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 03:03:39 GMT+0800 (Singapore Standard Time)

<p>
Time complexities are:

- O(n) for `addRange` and `removeRange`, since slice overwriting dominates binary search
- O(log n) for `queryRange`, since just binary search

where n is the number of disjoint intervals currently stored.

```python
from bisect import bisect_left as bl, bisect_right as br

class RangeModule:

    def __init__(self):
        self._X = []

    def addRange(self, left, right):
        i, j = bl(self._X, left), br(self._X, right)
        self._X[i:j] = [left]*(i%2 == 0) + [right]*(j%2 == 0)

    def queryRange(self, left, right):
        i, j = br(self._X, left), bl(self._X, right)
        return i == j and i%2 == 1

    def removeRange(self, left, right):
        i, j = bl(self._X, left), br(self._X, right)
        self._X[i:j] = [left]*(i%2 == 1) + [right]*(j%2 == 1)
```

Partial explanation:
- `k in [i, j) = [bl(X, left), br(X, right))` iff `X[k] in [left, right]`
- `k in [i, j) = [br(X, left), bl(X, right))` iff `X[k] in (left, right)`
- Note that while `x in [a, b)` iff `a <= x < b`, for an interval `[x, y)` to be contained
within `[a, b)` we require `a <= x < y <= b`. In other words, both `x` and `y` are in `[a, b]`.
This is how the transition from half-open to either open or closed occurs.
</p>


### C++, vector O(n) and map O(logn), compare two solutions
- Author: zestypanda
- Creation Date: Sun Oct 22 2017 12:14:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 28 2018 02:26:00 GMT+0800 (Singapore Standard Time)

<p>
The solution using vector of intervals (pair<int, int>) is very straightforward. The runtime is O(n) for addRange and deleteRange, and O(logn) for queryRange, where n is total number of intervals.

Another option is to use map (ordered map). And the runtime is still O(logn) for queryRange, but O(klogn) for addRange and deleteRange, where k is number of overlapping ranges. 

For a single operation, it is hard to tell whether vector or map is better, because k is unknown. However, the k overlapping ranges will be erased after either add or remove ranges. Let's assume m is the total operations of add or delete ranges. Then total number of possible ranges is O(m) because add or delete may increase ranges by 1. So both n and k is O(m).

In summary, the run time for query is the same. However, the total run time for add and delete using vector is O(m^2), and that using map is O(mlogm). So amortized cost for delete and add is O(m) for vector, and O(logm) for map.

Vector
```
class RangeModule {
public:
   void addRange(int left, int right) {
        int n = invals.size();
        vector<pair<int, int>> tmp;
        for (int i = 0; i <= n; i++) {
            if (i == n || invals[i].first > right) {
                tmp.push_back({left, right});
                while (i < n) tmp.push_back(invals[i++]);
            }
            else if (invals[i].second < left) 
                tmp.push_back(invals[i]);
            else {
                left = min(left, invals[i].first);
                right = max(right, invals[i].second);
            }
        }
        swap(invals, tmp);
    }
    
    bool queryRange(int left, int right) {
        int n = invals.size(), l = 0, r = n-1;
        while (l <= r) {
            int m = l+(r-l)/2;
            if (invals[m].first >= right)
                r = m-1;
            else if (invals[m].second <= left)
                l = m+1;
            else 
                return invals[m].first <= left && invals[m].second >= right;
        }
        return false;
    }
    
    void removeRange(int left, int right) {
        int n = invals.size();
        vector<pair<int, int>> tmp;
        for (int i = 0; i < n; i++) {
            if (invals[i].second <= left || invals[i].first >= right)
                tmp.push_back(invals[i]);
            else {
                if (invals[i].first < left)  tmp.push_back({invals[i].first, left});
                if (invals[i].second > right) tmp.push_back({right, invals[i].second});
            }
        }
        swap(invals, tmp);
    }
private:
    vector<pair<int, int>> invals;
};
```
Using map
```
class RangeModule {
public:
    void addRange(int left, int right) {
        auto l = invals.upper_bound(left), r = invals.upper_bound(right); 
        if (l != invals.begin()) {
            l--;
            if (l->second < left) l++;
        }
        if (l != r) {
            left = min(left, l->first);
            right = max(right, (--r)->second);
            invals.erase(l,++r);
        }
        invals[left] = right;
    }
    
    bool queryRange(int left, int right) {
        auto it = invals.upper_bound(left);
        if (it == invals.begin() || (--it)->second < right) return false;
        return true;
    }
    
    void removeRange(int left, int right) {
        auto l = invals.upper_bound(left), r = invals.upper_bound(right); 
        if (l != invals.begin()) {
            l--;
            if (l->second < left) l++;
        }
        if (l == r) return;
        int l1 = min(left, l->first), r1 = max(right, (--r)->second);
        invals.erase(l, ++r);
        if (l1 < left) invals[l1] = left;
        if (r1 > right) invals[right] = r1;
    }
private:
    map<int, int> invals;
};
```
</p>


