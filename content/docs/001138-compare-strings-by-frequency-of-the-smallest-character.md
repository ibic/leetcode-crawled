---
title: "Compare Strings by Frequency of the Smallest Character"
weight: 1138
#id: "compare-strings-by-frequency-of-the-smallest-character"
---
## Description
<div class="description">
<p>Let&#39;s define a function <code>f(s)</code> over a non-empty string <code>s</code>, which calculates the frequency of the smallest character in <code>s</code>. For example,&nbsp;if <code>s = &quot;dcce&quot;</code> then <code>f(s) = 2</code> because the smallest character is <code>&quot;c&quot;</code> and its frequency is 2.</p>

<p>Now, given string arrays <code>queries</code>&nbsp;and <code>words</code>, return an integer array <code>answer</code>, where each <code>answer[i]</code>&nbsp;is the number of words such that <code>f(queries[i])</code>&nbsp;&lt; <code>f(W)</code>, where <code>W</code>&nbsp;is a word in <code>words</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> queries = [&quot;cbd&quot;], words = [&quot;zaaaz&quot;]
<strong>Output:</strong> [1]
<strong>Explanation:</strong> On the first query we have f(&quot;cbd&quot;) = 1, f(&quot;zaaaz&quot;) = 3 so f(&quot;cbd&quot;) &lt; f(&quot;zaaaz&quot;).
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> queries = [&quot;bbb&quot;,&quot;cc&quot;], words = [&quot;a&quot;,&quot;aa&quot;,&quot;aaa&quot;,&quot;aaaa&quot;]
<strong>Output:</strong> [1,2]
<strong>Explanation:</strong> On the first query only f(&quot;bbb&quot;) &lt; f(&quot;aaaa&quot;). On the second query both f(&quot;aaa&quot;) and f(&quot;aaaa&quot;) are both &gt; f(&quot;cc&quot;).
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= queries.length &lt;= 2000</code></li>
	<li><code>1 &lt;= words.length &lt;= 2000</code></li>
	<li><code>1 &lt;= queries[i].length, words[i].length &lt;= 10</code></li>
	<li><code>queries[i][j]</code>, <code>words[i][j]</code> are English lowercase letters.</li>
</ul>

</div>

## Tags
- Array (array)
- String (string)

## Companies
- Google - 2 (taggedByAdmin: true)
- Oracle - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Keep track of count of frequencies Beats 100%
- Author: dev1988
- Creation Date: Sun Aug 25 2019 21:26:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 25 2019 21:26:54 GMT+0800 (Singapore Standard Time)

<p>
The idea is to keep track of the count of frequencies(max can be 10, as mentioned in constraints).

By keeping a cumulative frequency vector, we are easily able to answer queries[i].

```
class Solution {
public:
    vector<int> numSmallerByFrequency(vector<string>& queries, vector<string>& words) {
        vector<int> fr(12, 0);
        int f;
        vector<int> q;
        for(int i = 0; i < words.size(); i++){
            f = getF(words[i]);
            fr[f]++;
        }
		//Find cumulative frequency i.e. ith item will have sum of i..end.
        for(int i = 9; i >= 0; i--){
            fr[i] = fr[i] + fr[i+1];
        }
        
        for(int i = 0; i < queries.size(); i++){
            f = getF(queries[i]);
            q.push_back(fr[f+1]);
        }
        return q;
    }
    /*Helper function to calculate frequency of smallest element*/
    int getF(string &s){
        int a[26] = {0,};
        for(int i = 0; i < s.size(); i++){
            a[s[i]-\'a\']++;
        }
        
        for(int i = 0; i < 26; i++){
            if(a[i] != 0) return a[i];
        }
        return 0;
    }
};
```
</p>


### [Python] 2-liner
- Author: cenkay
- Creation Date: Mon Aug 26 2019 03:13:37 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 29 2019 02:54:52 GMT+0800 (Singapore Standard Time)

<p>
* min(w) will give smallest character for each word \'w\' in \'words\' list
* w.count(min(w)) will give frequency of smallest character for each word \'w\' in \'words\' list
* \'f\' is the sorted frequency list of \'words\'
* For each query \'q\' in \'queries\' list, we find its rightmost suitable index in the frequency \'f\' list
* Total length of frequency list \'f\' minus index will give answer[i]
* Index is determined by bisect module, which gives number of words having frequency of their smallest character less than or equal to query \'q\'
```
class Solution:
    def numSmallerByFrequency(self, queries: List[str], words: List[str]) -> List[int]:
        f = sorted(w.count(min(w)) for w in words)
        return [len(f) - bisect.bisect(f, q.count(min(q))) for q in queries]
```
</p>


### Please fix the description
- Author: darynkaypbaev
- Creation Date: Thu Nov 21 2019 00:01:21 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Nov 21 2019 00:01:21 GMT+0800 (Singapore Standard Time)

<p>
Please add the word "lexicographically" before "the smallest". Because I stuck for a while to realize what does "the smallest" mean?
</p>


