---
title: "Ads Performance"
weight: 1562
#id: "ads-performance"
---
## Description
<div class="description">
<p>Table: <code>Ads</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| ad_id         | int     |
| user_id       | int     |
| action        | enum    |
+---------------+---------+
(ad_id, user_id) is the primary key for this table.
Each row of this table contains the ID of an Ad, the ID of a user and the action taken by this user regarding this Ad.
The action column is an ENUM type of (&#39;Clicked&#39;, &#39;Viewed&#39;, &#39;Ignored&#39;).
</pre>

<p>&nbsp;</p>

<p>A company is running Ads and wants to calculate the performance of each Ad.</p>

<p>Performance of the Ad is measured using&nbsp;Click-Through Rate (CTR) where:</p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/01/17/sql1.png" style="width: 600px; height: 75px;" /></p>

<p>Write an SQL query to find the <code>ctr</code> of each Ad.</p>

<p><strong>Round</strong> <code>ctr</code>&nbsp;to 2 decimal points. <strong>Order</strong> the result table by <code>ctr</code>&nbsp;in descending order&nbsp;and by&nbsp;<code>ad_id</code>&nbsp;in ascending order in case of a tie.</p>

<p>The query result format is in the following example:</p>

<pre>
Ads table:
+-------+---------+---------+
| ad_id | user_id | action  |
+-------+---------+---------+
| 1     | 1       | Clicked |
| 2     | 2       | Clicked |
| 3     | 3       | Viewed  |
| 5     | 5       | Ignored |
| 1     | 7       | Ignored |
| 2     | 7       | Viewed  |
| 3     | 5       | Clicked |
| 1     | 4       | Viewed  |
| 2     | 11      | Viewed  |
| 1     | 2       | Clicked |
+-------+---------+---------+
Result table:
+-------+-------+
| ad_id | ctr   |
+-------+-------+
| 1     | 66.67 |
| 3     | 50.00 |
| 2     | 33.33 |
| 5     | 0.00  |
+-------+-------+
for ad_id = 1, ctr = (2/(2+1)) * 100 = 66.67
for ad_id = 2, ctr = (1/(1+2)) * 100 = 33.33
for ad_id = 3, ctr = (1/(1+1)) * 100 = 50.00
for ad_id = 5, ctr = 0.00, Note that ad_id = 5 has no clicks or views.
Note that we don&#39;t care about Ignored Ads.
Result table is ordered by the ctr. in case of a tie we order them by ad_id
</pre>

</div>

## Tags


## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MYSQL Easy solution
- Author: zy07621
- Creation Date: Mon Jan 20 2020 04:52:18 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 20 2020 04:52:18 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT ad_id, IFNULL(ROUND(AVG(CASE WHEN action = \'Clicked\' THEN 1
                         WHEN action = \'Viewed\' THEN 0
                         ELSE NULL END)*100,2),0) AS ctr
FROM Ads
GROUP BY ad_id
ORDER BY ctr DESC, ad_id
```
</p>


### MySQL solution
- Author: lianj
- Creation Date: Wed Jan 29 2020 11:00:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jan 29 2020 11:00:39 GMT+0800 (Singapore Standard Time)

<p>
```
select ad_id, 
ifnull(round(sum(case when action = \'Clicked\' then 1 else 0 end) / sum(case when action = \'Clicked\' or action = \'Viewed\' then 1 else 0 end) * 100, 2), 0) as ctr
from Ads
group by ad_id
order by ctr desc, ad_id asc
```

This could potentially by optimized if you change case when to action = \'Clicked\',  true value would be evaluated as 1.
</p>


### faster than 100%
- Author: jerryheziyu
- Creation Date: Wed Mar 04 2020 08:45:00 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Mar 04 2020 08:45:00 GMT+0800 (Singapore Standard Time)

<p>
\'\'\'
SELECT ad_id, 
ROUND(CASE 
    WHEN SUM(CASE When action=\'Clicked\' or action=\'Viewed\' Then 1 ELSE 0 END) = 0 THEN 0
    ELSE SUM(CASE When action=\'Clicked\' Then 1 ELSE 0 END)/SUM(CASE When action=\'Clicked\' or action=\'Viewed\' Then 1 ELSE 0 END)*100
END,2) AS ctr
FROM Ads
GROUP BY ad_id 
ORDER BY ctr DESC, ad_id 
\'\'\'
</p>


