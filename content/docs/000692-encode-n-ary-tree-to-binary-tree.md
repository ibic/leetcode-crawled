---
title: "Encode N-ary Tree to Binary Tree"
weight: 692
#id: "encode-n-ary-tree-to-binary-tree"
---
## Description
<div class="description">
<p>Design an algorithm to encode an N-ary tree into a binary tree and decode the binary tree to get the original N-ary tree. An N-ary tree is a rooted tree in which each node has no more than N children. Similarly, a binary tree is a rooted tree in which each node has no more than 2 children. There is no restriction on how your encode/decode algorithm should work. You just need to ensure that an N-ary tree can be encoded to a binary tree and this binary tree can be decoded to the original N-nary tree structure.</p>

<p><em>Nary-Tree input serialization&nbsp;is represented in their level order traversal, each group of children is separated by the null value (See following example).</em></p>

<p>For example, you may encode the following&nbsp;<code>3-ary</code>&nbsp;tree to a binary tree in this way:</p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/12/narytreebinarytreeexample.png" style="width: 100%; max-width: 640px" /></p>

<pre>
<strong>Input:</strong> root = [1,null,3,2,4,null,5,6]
</pre>

<p>Note that the above is just an example which&nbsp;<em>might or might not</em>&nbsp;work. You do not necessarily need to follow this format, so please be creative and come up with different approaches yourself.</p>

<ol>
</ol>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The height of the n-ary tree is less than or equal to <code>1000</code></li>
	<li>The total number of nodes is between <code>[0,&nbsp;10^4]</code></li>
	<li>Do not use class member/global/static variables to store states. Your encode and decode algorithms should be stateless.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Flipkart - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Intuition

There are several ways to encode the N-ary tree to a binary tree. However, a majority of the algorithms could all be traced back to the one that is documented on the [Wikipedia](https://en.wikipedia.org/wiki/M-ary_tree#Convert_a_m-ary_tree_to_binary_tree).

Here we would like to illustrate intuitively the idea first, which we would implement in different manners in the following sections.

![pic](../Figures/431/431_nary_tree.png)

To put it simple, the algorithm can be summarized in two steps. We use the above N-ary tree as an example for demonstration.

>Step 1). Link all **_siblings_** together, like a singly-linked list.

Each node in the original N-ary tree would correspond uniquely to a node in the resulting binary tree.

In the first step, we first chain up all the sibling nodes together, _i.e._ nodes that share the same parent. By _chaining up_, we would link the nodes via either `left` or `right` child pointers of the binary tree node. Here we choose to do with the _right_ pointer.

![pic](../Figures/431/431_sibling_list.png)

>Step 2). Link the **_head_** of the obtained list of siblings with its **_parent_** node. 

Now that the siblings are chained up, we just need to link this sibling list with their parent node. 

As one can see, we don't have to link each one of the siblings to its parent, and we cannot do so either, since we only got two pointers for a node in binary tree. It suffices to pick one of the siblings. Naturally, we could link the head of the list with its parent node.

![pic](../Figures/431/431_binary_tree.png)

_Before one notices, after the above two steps, we have already converted the N-ary tree to a binary tree !_

It might not be evident from the above graph. But if one turns the graph 45 degrees clockwise, a binary tree would appear.

![pic](../Figures/431/431_binary_tree_format.png)

As one can imagine, based on the above idea, one can create some variants. For instance, instead of linking the child nodes with the `right` pointers, we could use the `left` pointers. And accordingly, we could start from the last child node to chain up the siblings. Here is the variant.

![pic](../Figures/431/431_variant.png)


<br/>
<br/>

---
#### Approach 1: BFS (Breadth-First Search) Traversal

**Intuition**

There are generally two strategies to traverse the tree data structure: _BFS (Breadth-First Search)_ and _DFS (Depth-First Search)_.

Based on the intuition in the above section, one might find it fit well with the BFS strategy, since we are traversing the nodes _level by level_, _i.e._ we chain up the sibling nodes which reside in the same level of the tree. Indeed, we could implement the algorithm with the BFS strategy. But actually, as we would demonstrate later, we could also implement it via the DFS strategy.

**Algorithm**

Let us start with the BFS on the `encode(root)` function:

- Speaking about BFS, one shall recall that it is essentially implemented via the **_queue_** data structure. Indeed, first of all, all the sibling nodes would be pushed into the queue in sequence. And the one at the head of the queue would be processed first, which follows the principle of the queue data structure, **_FIFO_** (_First In, First Out_).

- The main body of the algorithm consists of a _**loop**_ that iterates through the queue until it becomes empty. At each step of the loop, we _pop_ out a node from the head of the queue, and process it. 

- For the popped out node, we then run another **_loop_** over its children nodes. As one notices, this is a nested loop inside the previous loop over the queue. At each step of this _nested loop_, for each child node, we do _two_ things:

    - First, we chain this child node with its previous neighbor sibling node.

    - Second, we append this child node into the queue, in order that it would have its turn to be processed as a parent node to encode its own children nodes.

- Voila. That is it. An important note is that we do the traversing of the N-ary tree in parallel with the construction of the desired Binary Tree. As a result, we keep each entry in the queue as a **_tandem_**, _i.e._ `pair(n-ary_tree_node, binary_tree_node)`. 

- To render the algorithm more robust, we could handle the case where the input N-ary tree is empty at the beginning of the function.

!?!../Documents/431_LIS.json:1000,501!?!

<iframe src="https://leetcode.com/playground/HNC8u3BM/shared" frameBorder="0" width="100%" height="500" name="HNC8u3BM"></iframe>

As to the `decode(node)` function, similarly with our encoding function, we could implement in the _BFS_ manner.

- Again, the main algorithm is organized as a loop around a _queue_ data structure. 

- We start from the root node of the encoded binary tree by pushing it into the queue.

- At each step of the iteration, we pop out a binary node from the tree, we then take the `left` child node of the node as its corresponding first child node of the original N-ary node.

- We then recover the rest of the child nodes by following the `right` pointer of the binary nodes.


**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the number of nodes in the N-ary tree. We traverse each node in the tree once and only once.

- Space Complexity: $$\mathcal{O}(L)$$ where $$L$$ is the maximum number of nodes that reside at the same level.

    - We use a queue data structure to do BFS traversal, _i.e._ visiting the nodes level by level.

    - **At any given moment, the queue contains nodes that are _at most_ spread into _two levels_**. As a result, assuming the maximum number of nodes at one level is $$L$$, the size of the queue would be less than $$2L$$ at any time.

    - Therefore, the space complexity of both `encode()` and `decode()` functions is $$\mathcal{O}(L)$$. 
<br/>
<br/>

---
#### Approach 2: DFS (Depth-First Search) Traversal

**Intuition**

As it turned out, we could also implement the idea at the beginning of the article through DFS (Depth-First Search) traversal strategy.

Often the case, we implement the DFS algorithm with the technique of **_recursion_** which could greatly simplify the logic. Instead of ironing out all iterative steps, we could implement the function with the help of the function itself.

>The idea is that while we traverse the N-ary tree _node by node_ in the DFS manner, we **_weave_** the nodes together into a Binary tree, following the same intuition of encoding in the previous approach.


**Algorithm**

Again, let's demonstrate the `encode(node)` function as an example.

>The main idea of the algorithm is that for each node, we only take care the encoding of the node itself, and we invoke the function itself to encode each of its child node, _i.e._ `encode(node.children[i])`.

- At the beginning of the `encode(node)` function, we create a binary tree node to contain the value of the current node.

- Then we put the first child of the N-ary tree node as the left node of the newly-created binary tree node. We call the encoding function recursively to encode the first child node as well.

- For the rest of the children nodes of the N-ary tree node, we chain them up with the `right` pointer of the binary tree node. And again, we call recursively the encoding function to encode each of the child node.

![pic](../Figures/431/431_DFS.png)

_Note:_ the following implementation is inspired from the post by [wangzi6147](https://leetcode.com/problems/encode-n-ary-tree-to-binary-tree/discuss/153061/Java-Solution-(Next-Level-greater-left-Same-Level-greater-right)) in the discussion forum.

<iframe src="https://leetcode.com/playground/94Sqgr7a/shared" frameBorder="0" width="100%" height="500" name="94Sqgr7a"></iframe>


**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the number of nodes in the N-ary tree. We traverse each node in the tree once and only once.

- Space Complexity: $$\mathcal{O}(D)$$ where $$D$$ is the depth of the N-ary tree.

    - Unlike the BFS algorithm, we don't use the queue data structure in the DFS algorithm. However, implicitly the algorithm would consume more space in the function _call stack_ due to the recursive function calls. 
    
    - And this consumption of call stack space is the main space complexity for our DFS algorithm. As we can see, the size of the call stack at any moment is exactly _the number of **level**_ where the currently visited node resides, _e.g._ for the root node (level _0_), the recursive call stack is empty.
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution (Next Level -> left, Same Level -> right)
- Author: wangzi6147
- Creation Date: Tue Jul 24 2018 09:27:27 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 12:04:52 GMT+0800 (Singapore Standard Time)

<p>
```
   1
 / | \
2  3  4

to:

 1
/
2
 \
  3
   \
    4
```

```
class Codec {

    // Encodes an n-ary tree to a binary tree.
    public TreeNode encode(Node root) {
        if (root == null) {
            return null;
        }
        TreeNode result = new TreeNode(root.val);
        if (root.children.size() > 0) {
            result.left = encode(root.children.get(0));
        }
        TreeNode cur = result.left;
        for (int i = 1; i < root.children.size(); i++) {
            cur.right = encode(root.children.get(i));
            cur = cur.right;
        }
        return result;
    }

    // Decodes your binary tree to an n-ary tree.
    public Node decode(TreeNode root) {
        if (root == null) {
            return null;
        }
        Node result = new Node(root.val, new LinkedList<>());
        TreeNode cur = root.left;
        while (cur != null) {
            result.children.add(decode(cur));
            cur = cur.right;
        }
        return result;
    }
}
```
</p>


### Super Simple Java Beats 99% 2ms with clear Explanation
- Author: bmask
- Creation Date: Fri Aug 17 2018 09:37:29 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 04 2018 07:53:39 GMT+0800 (Singapore Standard Time)

<p>
Here is the logic:
We start reading the N-ary nodes from the root. Put all the first root\'s children at the RIGHT side of the Binary Tree, the next root is the child node of the current node (parent). What we do here for this node is exactly same except we put all of this node\'s children to the LEFT side of the Binary Tree (left side of this node in binary tree). For their childs we do the reverse, which is putting their children in RIGHT side of the Binary Tree again, and so on. We keep continuing puting children of the current nodes at RIGHT, and LEFT sides till we reach to the end.
For decoding, we just read as we create it. Right, then Left, Right, then Left, and so on.
The picture illustrates the process:
![image](https://s3-lc-upload.s3.amazonaws.com/users/bmask/image_1534469654.png)

Here is the code:
```
class Codec {

 // Encodes an n-ary tree to a binary tree.
    public TreeNode encode(Node root) {
		return encode(root, true);       
    }
    TreeNode encode(Node root, boolean isRight){
	if(root==null) return null;
	TreeNode newRoot = new TreeNode(root.val);
	TreeNode curr = newRoot;
	if(isRight){
		for(Node child:root.children){
			curr.right=encode(child,false);  //if current is RIGHT then flip to LEFT for the child\'s children!
			curr=curr.right;
		}
	}else{
		for(Node child:root.children){
			curr.left=encode(child,true);  //if current is LEFT then flip to RIGHT for the child\'s children!
			curr=curr.left;
		}
	}
	return newRoot;
    }

    // Decodes your binary tree to an n-ary tree.
    public Node decode(TreeNode root) {
		return decode(root,true);        
    }
    Node decode(TreeNode root, boolean isRight){
	if(root==null) return null;
	Node newRoot = new Node(root.val, new ArrayList<Node>());
	if(isRight){
		while(root.right!=null){ 
			root=root.right;
			newRoot.children.add(decode(root,false));				
		}
	}else{
		while(root.left!=null){ 
			root=root.left;
			newRoot.children.add(decode(root,true));				 
		}
	}
	return newRoot;
    }
}
```
</p>


### Python - left child for children, right child for siblings
- Author: yorkshire
- Creation Date: Fri Jul 20 2018 14:42:28 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 06:34:50 GMT+0800 (Singapore Standard Time)

<p>
The left child of a binary node is the subtree encoding all the children of the corresponding n-ary node.
The right child of a binary node is a chain of the binary root nodes encoding each sibling of the n-ary node.
Hence the root node has no right binary child, because the root has no sibilings.
```
class Codec:

    def encode(self, root):
        if not root:
            return None

        binary = TreeNode(root.val)                 # create a binary root
        if not root.children:
            return binary

        binary.left = self.encode(root.children[0]) # left child of binary is the encoding of all n-ary children,
        node = binary.left                          #     starting with the first child.
        for child in root.children[1:]:             # other children of n-ary root are right child of previous child
            node.right = self.encode(child)
            node = node.right

        return binary

    def decode(self, data):
        if not data:
            return None

        nary = Node(data.val, [])                   # create n-ary root
        node = data.left                            # move to first child of n-ary root
        while node:                                 # while more children of n-ary root
            nary.children.append(self.decode(node)) # append to list
            node = node.right                       # and move to next child
            
        return nary
```
</p>


