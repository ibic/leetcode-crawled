---
title: "Dot Product of Two Sparse Vectors"
weight: 1452
#id: "dot-product-of-two-sparse-vectors"
---
## Description
<div class="description">
<p>Given two sparse vectors, compute their dot product.</p>

<p>Implement class <code>SparseVector</code>:</p>

<ul data-indent="0" data-stringify-type="unordered-list">
	<li><code>SparseVector(nums)</code>&nbsp;Initializes the object with the vector <code>nums</code></li>
	<li><code>dotProduct(vec)</code>&nbsp;Compute the dot product between the instance of <em>SparseVector</em> and <code>vec</code></li>
</ul>

<p>A <strong>sparse vector</strong> is a vector that has mostly zero values, you should store the sparse vector&nbsp;<strong>efficiently </strong>and compute the dot product between two <em>SparseVector</em>.</p>

<p><strong>Follow up:&nbsp;</strong>What if only one of the vectors is sparse?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [1,0,0,2,3], nums2 = [0,3,0,4,0]
<strong>Output:</strong> 8
<strong>Explanation:</strong> v1 = SparseVector(nums1) , v2 = SparseVector(nums2)
v1.dotProduct(v2) = 1*0 + 0*3 + 0*0 + 2*4 + 3*0 = 8
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [0,1,0,0,0], nums2 = [0,0,0,0,2]
<strong>Output:</strong> 0
<strong>Explanation:</strong> v1 = SparseVector(nums1) , v2 = SparseVector(nums2)
v1.dotProduct(v2) = 0*0 + 1*0 + 0*0 + 0*0 + 0*2 = 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [0,1,0,0,2,0,0], nums2 = [1,0,0,0,3,0,4]
<strong>Output:</strong> 6
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == nums1.length == nums2.length</code></li>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
	<li><code>0 &lt;= nums1[i], nums2[i]&nbsp;&lt;= 100</code></li>
</ul>

</div>

## Tags
- Array (array)
- Hash Table (hash-table)
- Two Pointers (two-pointers)

## Companies
- Facebook - 12 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python dictionary
- Author: lichuan199010
- Creation Date: Thu Sep 03 2020 13:28:27 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 30 2020 14:17:33 GMT+0800 (Singapore Standard Time)

<p>
Since the vector is sparse, we only store indices with values that are nonzero.

	class SparseVector:
		def __init__(self, nums: List[int]):
			self.seen = {}
			for i, n in enumerate(nums):
				if n != 0:
					self.seen[i] = n              

		# Return the dotProduct of two sparse vectors
		def dotProduct(self, vec: \'SparseVector\') -> int:
			res = 0
			for j, n in vec.seen.items():
				if j in self.seen:
					res += n * self.seen[j]
			return res
			
A very beautiful Pythonic implementation by @claytonjwong in the comments section.
I personally like the longer one better, because it is easier for people to understand regardless of the coding languages they are using. But one-liners are always so cool!
 

	class SparseVector:
		def __init__(self, A: List[int]):
			self.m = {i: x for i, x in enumerate(A) if x}
		def dotProduct(self, other: \'SparseVector\') -> int:
			return sum([x * y for i, x in self.m.items() for j, y in other.m.items() if i == j])
</p>


### Python 3 Lines
- Author: dibdidib
- Creation Date: Tue Sep 08 2020 04:19:37 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 08 2020 04:19:37 GMT+0800 (Singapore Standard Time)

<p>
We are instructed to store the sparse vector efficently. This is a clue to use a dict/map.

```python
class SparseVector:

    def __init__(self, nums):
        self.m = {i: x for i, x in enumerate(nums) if x}

    def dotProduct(self, vec):
        a, b = self, vec
        return sum(a.m[i] * b.m[i] for i in a.m if i in b.m)
```
If one vector is sparse, and the other is not, we can update `dotProduct()` to iterate through the shorter:
```python
class SparseVector:

    # as above

    def dotProduct(self, vec):
        a, b = self, vec
        if len(a.m) > len(b.m):
            a, b = b, a
        return sum(a.m[i] * b.m[i] for i in a.m if i in b.m)
```
</p>


### C++ 3 lines #minimalizm
- Author: votrubac
- Creation Date: Fri Sep 04 2020 09:13:36 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 04 2020 09:20:21 GMT+0800 (Singapore Standard Time)

<p>
First, a bit wordier but more efficient version.
```cpp
vector<pair<int, int>> v;
SparseVector(vector<int> &nums) {
    for (int i = 0; i < nums.size(); ++i)
        if (nums[i])
            v.push_back({i, nums[i]});
}
int dotProduct(SparseVector& vec, int res = 0) {
    for (int i = 0, j = 0; i < v.size() && j < vec.v.size(); ++i, ++j)
        if (v[i].first < vec.v[j].first)
            --j;
        else if (v[i].first > vec.v[j].first)
            --i;
        else
            res += v[i].second * vec.v[j].second;
    return res;
}
```

Next, shorter version using STL.
```cpp
vector<int> v, n;
SparseVector(vector<int> &nums) : n(nums) {
    for (int i = 0; i < n.size(); ++i)
        if (n[i])
            v.push_back(i);
}
int dotProduct(SparseVector& vec) {
    vector<int> res;
    set_intersection(begin(v), end(v), begin(vec.v), end(vec.v), back_inserter(res));
    return accumulate(begin(res), end(res), 0, [&](int s, int i) { return s + n[i] * vec.n[i]; });
}
```

Finally, a really ugly #minimalizm variation.
```cpp
vector<int> v, n;
SparseVector(vector<int> &nums, int i = 0) : n(nums) {
    for_each(begin(n), end(n), [&](int k) { if (k) v.push_back(i); ++i; });
}
int dotProduct(SparseVector& vec, vector<int> res = {}) {
    set_intersection(begin(v), end(v), begin(vec.v), end(vec.v), back_inserter(res));
    return accumulate(begin(res), end(res), 0, [&](int s, int i) { return s + n[i] * vec.n[i]; });
}
```
</p>


