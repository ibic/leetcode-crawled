---
title: "Search Insert Position"
weight: 35
#id: "search-insert-position"
---
## Description
<div class="description">
<p>Given a sorted array of distinct integers and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> nums = [1,3,5,6], target = 5
<strong>Output:</strong> 2
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> nums = [1,3,5,6], target = 2
<strong>Output:</strong> 1
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> nums = [1,3,5,6], target = 7
<strong>Output:</strong> 4
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> nums = [1,3,5,6], target = 0
<strong>Output:</strong> 0
</pre><p><strong>Example 5:</strong></p>
<pre><strong>Input:</strong> nums = [1], target = 0
<strong>Output:</strong> 0
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10<sup>4</sup></code></li>
	<li><code>-10<sup>4</sup> &lt;= nums[i] &lt;= 10<sup>4</sup></code></li>
	<li><code>nums</code> contains <strong>distinct</strong> values sorted in <strong>ascending</strong> order.</li>
	<li><code>-10<sup>4</sup> &lt;= target &lt;= 10<sup>4</sup></code></li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Apple - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Bloomberg - 4 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Binary Search 

**Intuition**

Based on the description of the problem, we can see that it could be 
 a good match with the [binary search](https://en.wikipedia.org/wiki/Binary_search_algorithm) algorithm. 

> Binary search is a search algorithm that find the position of a target
 value within a _sorted_ array.
 
Usually, within binary search, we compare the target value to 
the middle element of the array at each iteration.

- If the target value is equal to the middle element, the job is done.

- If the target value is less than the middle element, 
continue to search on the left.

- If the target value is greater than the middle element, 
continue to search on the right.

Here we showcase a simple example on how it works.

![simple](../Figures/35/simple.png) 

To mark the search boundaries, one could use two pointers: 
`left` and `right`. 
Starting from `left = 0` and `right = n - 1`, we then move either of the pointers
 according to various situations:

- While `left <= right`:

    - Pivot index is the one in the middle: `pivot = (left + right) / 2`.
      The pivot also divides the original array into two subarray.

    - If the target value is equal to the pivot element:
    `target == nums[pivot]`, we're done.
    
    - If the target value is less than the pivot element `target < nums[pivot]`,
    continue to search on the left subarray by moving 
    the right pointer `right = pivot - 1`.
    
    - If the target value is greater than the pivot element `target > nums[pivot]`,
    continue to search on the right subarray by moving the left pointer `left = pivot + 1`.

![two](../Figures/35/two_pointers.png)

> What if the target value is not found? 

In this case, the loop will be stopped at the moment when
`right < left` and `nums[right] < target < nums[left]`. 
Hence, the proper position to insert the target is at the index `left`.

![two](../Figures/35/not_simple.png)

**Integer Overflow**

Let us now stress the fact that `pivot = (left + right) // 2` works fine for 
Python3, which has arbitrary precision integers, but it
could cause some issues in Java and C++. 

If `left + right` is greater than the maximum int 
value $$2^{31} - 1$$, it overflows to a negative value.
In Java, it would trigger an exception of `ArrayIndexOutOfBoundsException`, 
and in C++ it causes an illegal write, which leads to memory corruption 
and unpredictable results. 

Here is a simple way to fix it:

<iframe src="https://leetcode.com/playground/Hw4bwtRt/shared" frameBorder="0" width="100%" height="72" name="Hw4bwtRt"></iframe>

and here is a bit more complicated but probably faster way 
 using the bit shift operator.

<iframe src="https://leetcode.com/playground/Eb2XWpAK/shared" frameBorder="0" width="100%" height="72" name="Eb2XWpAK"></iframe>


**Algorithm**

- Initialize the `left` and `right` pointers : `left = 0`, `right = n - 1`.

- While `left <= right`:

    - Compare middle element of the array `nums[pivot]` to the target
    value `target`.
    
        - If the middle element _is_ the target, _i.e._ `target == nums[pivot]`: 
        return `pivot`. 
        
        - If the target is not here: 
        
            - If `target < nums[pivot]`, continue to search on the left subarray.
            `right = pivot - 1`.
            
            - Else continue to search on the right subarray.
            `left = pivot + 1`.
            
- Return `left`.
            
**Implementation**

!?!../Documents/35_LIS.json:1000,340!?!

<iframe src="https://leetcode.com/playground/DRQfdPog/shared" frameBorder="0" width="100%" height="276" name="DRQfdPog"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(\log N)$$. 

    Let us compute the time complexity with the help of 
    [master theorem](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)) 
    $$T(N) = aT\left(\frac{N}{b}\right) + \Theta(N^d)$$.
    The equation represents dividing the problem 
    up into $$a$$ subproblems of size $$\frac{N}{b}$$ in $$\Theta(N^d)$$ time. 
    Here at each step there is only one subproblem _i.e._ `a = 1`, its size 
    is a half of the initial problem _i.e._ `b = 2`, 
    and all this happens in a constant time _i.e._ `d = 0`.
    As a result, $$\log_b{a} = d$$ and hence we're dealing with 
    [case 2](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)#Case_2_example)
    that results in $$\mathcal{O}(n^{\log_b{a}} \log^{d + 1} N)$$
    = $$\mathcal{O}(\log N)$$ time complexity.
 
* Space complexity : $$\mathcal{O}(1)$$ since it's a constant space
solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My 8 line Java solution
- Author: algorithmatic
- Creation Date: Sun Jan 25 2015 07:30:45 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 21:18:17 GMT+0800 (Singapore Standard Time)

<p>
        public int searchInsert(int[] A, int target) {
            int low = 0, high = A.length-1;
            while(low<=high){
                int mid = (low+high)/2;
                if(A[mid] == target) return mid;
                else if(A[mid] > target) high = mid-1;
                else low = mid+1;
            }
            return low;
        }
</p>


### C++ O(logn) Binary Search that handles duplicate
- Author: a0806449540
- Creation Date: Thu Jun 11 2015 21:49:49 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 04:32:50 GMT+0800 (Singapore Standard Time)

<p>
If there are duplicate elements equal to *target*, my code will always return the one with smallest index.

    class Solution {
    public:
        int searchInsert(vector<int>& nums, int target) {
            int low = 0, high = nums.size()-1;
    
            // Invariant: the desired index is between [low, high+1]
            while (low <= high) {
                int mid = low + (high-low)/2;
    
                if (nums[mid] < target)
                    low = mid+1;
                else
                    high = mid-1;
            }
    
            // (1) At this point, low > high. That is, low >= high+1
            // (2) From the invariant, we know that the index is between [low, high+1], so low <= high+1. Follwing from (1), now we know low == high+1.
            // (3) Following from (2), the index is between [low, high+1] = [low, low], which means that low is the desired index
            //     Therefore, we return low as the answer. You can also return high+1 as the result, since low == high+1
            return low;
        }
    };
</p>


### Python one liner 48ms
- Author: yao9
- Creation Date: Tue Nov 24 2015 12:18:53 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 19:28:46 GMT+0800 (Singapore Standard Time)

<p>
    class Solution(object):
        def searchInsert(self, nums, target):
            """
            :type nums: List[int]
            :type target: int
            :rtype: int
            """       
            return len([x for x in nums if x<target])
</p>


