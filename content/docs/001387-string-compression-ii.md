---
title: "String Compression II"
weight: 1387
#id: "string-compression-ii"
---
## Description
<div class="description">
<p><a href="http://en.wikipedia.org/wiki/Run-length_encoding">Run-length encoding</a> is a string compression method that works by&nbsp;replacing consecutive identical characters (repeated 2 or more times) with the concatenation of the character and the number marking the count of the characters (length of the run). For example, to compress the string&nbsp;<code>&quot;aabccc&quot;</code>&nbsp;we replace <font face="monospace"><code>&quot;aa&quot;</code></font>&nbsp;by&nbsp;<font face="monospace"><code>&quot;a2&quot;</code></font>&nbsp;and replace <font face="monospace"><code>&quot;ccc&quot;</code></font>&nbsp;by&nbsp;<font face="monospace"><code>&quot;c3&quot;</code></font>. Thus the compressed string becomes <font face="monospace"><code>&quot;a2bc3&quot;</code>.</font></p>

<p>Notice that in this problem, we are not adding&nbsp;<code>&#39;1&#39;</code>&nbsp;after single characters.</p>

<p>Given a&nbsp;string <code>s</code>&nbsp;and an integer <code>k</code>. You need to delete <strong>at most</strong>&nbsp;<code>k</code> characters from&nbsp;<code>s</code>&nbsp;such that the run-length encoded version of <code>s</code>&nbsp;has minimum length.</p>

<p>Find the <em>minimum length of the run-length encoded&nbsp;version of </em><code>s</code><em> after deleting at most </em><code>k</code><em> characters</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aaabcccd&quot;, k = 2
<strong>Output:</strong> 4
<b>Explanation: </b>Compressing s without deleting anything will give us &quot;a3bc3d&quot; of length 6. Deleting any of the characters &#39;a&#39; or &#39;c&#39; would at most decrease the length of the compressed string to 5, for instance delete 2 &#39;a&#39; then we will have s = &quot;abcccd&quot; which compressed is abc3d. Therefore, the optimal way is to delete &#39;b&#39; and &#39;d&#39;, then the compressed version of s will be &quot;a3c3&quot; of length 4.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aabbaa&quot;, k = 2
<strong>Output:</strong> 2
<b>Explanation: </b>If we delete both &#39;b&#39; characters, the resulting compressed string would be &quot;a4&quot; of length 2.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aaaaaaaaaaa&quot;, k = 0
<strong>Output:</strong> 3
<strong>Explanation: </strong>Since k is zero, we cannot delete anything. The compressed string is &quot;a11&quot; of length 3.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 100</code></li>
	<li><code>0 &lt;= k &lt;= s.length</code></li>
	<li><code>s</code> contains only lowercase English letters.</li>
</ul>

</div>

## Tags
- String (string)
- Dynamic Programming (dynamic-programming)

## Companies
- Toptal - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python dynamic programming
- Author: klimkina
- Creation Date: Sun Jul 26 2020 12:23:40 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 27 2020 06:25:20 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def getLengthOfOptimalCompression(self, s: str, k: int) -> int:
		# this decorator automatically use memo with key = (start, last, last_count, left)
        @lru_cache(None)
        def counter(start, last, last_count, left): #count the cost of compressing from the start
            if left < 0:
                return float(\'inf\') # this is impossible
            if start >= len(s):
                return 0
            if s[start] == last:
				# we have a stretch of the last_count of the same chars, what is the cost of adding one more? 
                incr = 1 if last_count == 1 or last_count == 9 or last_count == 99 else 0
				# no need to delete here, if we have a stretch of chars like \'aaaaa\' - we delete it from the beginning in the else delete section
                return incr + counter(start+1, last, last_count+1, left) # we keep this char for compression
            else:
				# keep this char for compression - it will increase the result length by 1 plus the cost of compressing the rest of the string 
				keep_counter = 1 + counter(start+1, s[start], 1, left)
				# delete this char
				del_counter =  counter(start + 1, last, last_count, left - 1)
                return min(keep_counter, del_counter)
            
        return counter(0, "", 0, k)
```
</p>


### C++ Top-Down DP with explanation, 64ms, short and clear
- Author: plus2047
- Creation Date: Sun Jul 26 2020 12:44:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 19:49:04 GMT+0800 (Singapore Standard Time)

<p>
We say a group of the chars that coding as `char+num` in the final result as a "group". Each group covers some subarray in the origin input, some chars in this group are removed, and the remained chars build the "group". For example, we remove "bb" from subarray "abbaaa" and remained "aaaa", which build a group "a4".

The key point is that the first group of chars must cover a subrange begin from index 0. So we can check all subrange indexed from 0 and just keep the most chars in those ranges.

For all problem like this one, which we need to build some "group", I always try to build the first group, and leaving the rest as a subproblem and solve it by recursive.

```
class Solution {
    const static int N = 127;

    // dp[left][k] means the minimal coding size for substring 
    // s[left:] and removing at most k chars
    int dp[N][N];

    string str;
    int n;

    // get length of digit
    inline int xs(int x) { return x == 1 ? 0 : x < 10 ? 1 : x < 100 ? 2 : 3; }

    int solve(int left, int k) {
        if(k < 0) return N;  // invalid, return INF
        if(left >= n or n - left <= k) return 0;  // empty

        int& res = dp[left][k];
        if(res != -1) return res;
        res = N;

        int cnt[26] = {0};
        // we try to make s[left:j] (both inculded) as one group,
        // and all chars in this group should be the same.
        // so we must keep the most chars in this range and remove others
        // the range length is (j - left + 1)
        // and the number of chars we need to remove is (j - left + 1 - most)
        for(int j = left, most = 0; j < n; j++) {
            most = max(most, ++cnt[str[j] - \'a\']);  // most = max(count(s[left:j])
            res = min(res, 1 + xs(most) + solve(j + 1, k - (j - left + 1 - most)));
        }
        return res;
    }
public:
    int getLengthOfOptimalCompression(string s, int k) {
        memset(dp, -1, sizeof(dp));
        str = s;
        n = s.size();
        return solve(0, k);
    }
};
```
</p>


### C++ top down dynamic programming with explanation
- Author: ttzztztz
- Creation Date: Sun Jul 26 2020 12:01:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 13:39:11 GMT+0800 (Singapore Standard Time)

<p>
state: `index, remain k, last char, concat number`
for each index, you have two choices: delete this char or concat
we calculate the contribution to the total len, get the min length

finally we get the answer

Time complexity: O(26 * 10 * N^2)
Space: O(26 * 10 * N^2)

This is an `optimization solution`.

Since N <= 100, we don\'t need a space of `100`  for the last dimension of dp array. because `x >= 10 && x <= 99` doesn\'t increment a length contribution to the total length.
However, we need a special check for the case which has 100 same chars.

Runtime: 276 ms, faster than 100.00% of C++ online submissions for String Compression II.
Memory Usage: 24.7 MB, less than 100.00% of C++ online submissions for String Compression II.

```cpp
int f[105][105][30][15];

class Solution {
public:
    int getLengthOfOptimalCompression(string s, int k) {
        this->s = s;
        
        const int N = s.size();
        if (N == 100 && k == 0) {
            bool allSame = true;
            for (int i = 1; i < N; i++) {
                if (s[i] != s[i - 1]) {
                    allSame = false;
                    break;
                }
            }

            if (allSame) {
                return 4;
            }
        }
        
        for (int i = 0; i <= N; i++) {
            for (int j = 0; j <= N; j++) {
                for (int k = 0; k <= 26; k++) {
                    for (int m = 0; m <= 10; m++) {
                        f[i][j][k][m] = -1;
                    }
                }
            }
        }
        
        return dfs(0, k, 26, 0);
    }
private:
    string s;
    int dfs(int idx, int k, int last, int concat) {
        if (k < 0) return 9999999;
        if (idx == s.size()) return 0;
        
        int& val = f[idx][k][last][concat];
        if (val != -1) return val;
        
        int answer = 9999999;
		// delete
        answer = min(answer, dfs(idx + 1, k - 1, last, concat));
        // concat
        if (last == s[idx] - \'a\') {
            const int fac = (concat == 1 || concat == 9) ? 1 : 0;
            answer = min(answer, fac + dfs(idx + 1, k, last, min(10, concat + 1)));
        } else {
            answer = min(answer, 1 + dfs(idx + 1, k, s[idx] - \'a\', 1));
        }
        return val = answer;
    }
};
```

This may got `TLE` this is the original solution:

Runtime: 1352 ms, faster than 100.00% of C++ online submissions for String Compression II.
Memory Usage: 131.8 MB, less than 100.00% of C++ online submissions for String Compression II.

Time complexity: O(26 * N^3)
Space: O(26 * N^3)

```cpp
int f[105][105][30][105];

class Solution {
public:
    int getLengthOfOptimalCompression(string s, int k) {
        this->s = s;
        
        const int N = s.size();
        for (int i = 0; i <= N; i++) {
            for (int j = 0; j <= N; j++) {
                for (int k = 0; k <= 26; k++) {
                    for (int m = 0; m <= N; m++) {
                        f[i][j][k][m] = -1;
                    }
                }
            }
        }
        
        return dfs(0, k, 26, 0);
    }
private:
    string s;
    int dfs(int idx, int k, int last, int concat) {
        if (k < 0) return 9999999;
        if (idx == s.size()) return 0;
        
        int& val = f[idx][k][last][concat];
        if (val != -1) return val;
        
        int answer = 9999999;
	    // delete
        answer = min(answer, dfs(idx + 1, k - 1, last, concat));
        // concat
        if (last == s[idx] - \'a\') {
            const int fac = (concat == 1 || concat == 9 || concat == 99) ? 1 : 0;
            answer = min(answer, fac + dfs(idx + 1, k, last, concat + 1));
        } else {
            answer = min(answer, 1 + dfs(idx + 1, k, s[idx] - \'a\', 1));
        }
        return val = answer;
    }
};
```
</p>


