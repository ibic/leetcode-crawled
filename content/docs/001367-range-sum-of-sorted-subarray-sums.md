---
title: "Range Sum of Sorted Subarray Sums"
weight: 1367
#id: "range-sum-of-sorted-subarray-sums"
---
## Description
<div class="description">
<p>Given the array <code>nums</code> consisting of <code>n</code> positive integers. You computed the sum of all non-empty continous subarrays from&nbsp;the array and then sort them in non-decreasing order, creating a new array of <code>n * (n + 1) / 2</code>&nbsp;numbers.</p>

<p><em>Return the sum of the numbers from index </em><code>left</code><em> to index </em><code>right</code> (<strong>indexed from 1</strong>)<em>, inclusive, in the&nbsp;new array.&nbsp;</em>Since the answer can be a huge number return it modulo 10^9 + 7.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4], n = 4, left = 1, right = 5
<strong>Output:</strong> 13 
<strong>Explanation:</strong> All subarray sums are 1, 3, 6, 10, 2, 5, 9, 3, 7, 4. After sorting them in non-decreasing order we have the new array [1, 2, 3, 3, 4, 5, 6, 7, 9, 10]. The sum of the numbers from index le = 1 to ri = 5 is 1 + 2 + 3 + 3 + 4 = 13. 
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4], n = 4, left = 3, right = 4
<strong>Output:</strong> 6
<strong>Explanation:</strong> The given array is the same as example 1. We have the new array [1, 2, 3, 3, 4, 5, 6, 7, 9, 10]. The sum of the numbers from index le = 3 to ri = 4 is 3 + 3 = 6.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4], n = 4, left = 1, right = 10
<strong>Output:</strong> 50
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10^3</code></li>
	<li><code>nums.length == n</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 100</code></li>
	<li><code>1 &lt;= left &lt;= right&nbsp;&lt;= n * (n + 1) / 2</code></li>
</ul>

</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ priority_queue solution
- Author: milu
- Creation Date: Sun Jul 12 2020 00:01:38 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 15 2020 04:19:50 GMT+0800 (Singapore Standard Time)

<p>
The element in priority queue is a pair of integers. The first integer is the `nums[i] + ... + nums[j]`, and the second integer is `j+1`.

The running time is `O(right * lgn)` worst case. The real benefit of using priority_queue is that memory usage is `O(n)`.
```
int rangeSum(vector<int>& nums, int n, int left, int right) {
	priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> mqueue;
	for (int i=0; i<n; i++)
		mqueue.push({nums[i], i+1});

	int ans = 0, mod = 1e9+7;
	for (int i=1; i<=right; i++) {
		auto p = mqueue.top();
		mqueue.pop();
		if (i >= left)
			ans = (ans + p.first) % mod;
		if (p.second < n) {
			p.first += nums[p.second++];
			mqueue.push(p);
		}
	}
	return ans;
}
```

Some more explaination...
Suppose `nums=[1, 2, 3]`, the possible continous subarrays and their sums are:
`[1], [1, 2], [1, 2, 3]` => `[1, 3, 6]`
                `[2], [2, 3]` => `[2, 5]`
                           `[3]` => `[3]`
As you can see, those sums are constructed in this way so that they are sorted. What we need from now on is very similiar to [Merge K Sorted Lists](https://leetcode.com/problems/merge-k-sorted-lists/).
</p>


### [Python3] priority queue
- Author: ye15
- Creation Date: Sun Jul 12 2020 02:39:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 12 2020 02:39:20 GMT+0800 (Singapore Standard Time)

<p>
Approach 1 (brute force) 
Collect all "range sum" and sort them. Return the sum of numbers between `left` and `right` modulo `1_000_000_007`. 

`O(N^2 logN)` time & `O(N^2)` space (376ms)
```
class Solution:
    def rangeSum(self, nums: List[int], n: int, left: int, right: int) -> int:
        ans = []
        for i in range(len(nums)):
            prefix = 0
            for ii in range(i, len(nums)):
                prefix += nums[ii]
                ans.append(prefix)
        ans.sort()
        return sum(ans[left-1:right]) % 1_000_000_007
```

Approach 2 
This solution is inspired by the brilliant idea of @milu in this [post](https://leetcode.com/problems/range-sum-of-sorted-subarray-sums/discuss/730511/C%2B%2B-priority_queue-solution). Credit goes to the original author. 

The idea is to keep track of range sums starting from `nums[i]` (`i=0...n-1`) in parallel in a priority queue of size `n`. In the beginning, the queue is initialized with `(nums[i], i)`, i.e. just the starting element and its position. At each following step, pop the smallest `x, i` out of the queue, and perform below operations:
1) check if step has reached `left`, add `x` to `ans`; 
2) extend the range sum `x` (currently ending at `i`) by adding `nums[i+1]` and in the meantime update the ending index to `i+1`. 

After `right` steps, `ans` would be the correct answer to return. 

`O(N^2 logN)` worst (but much faster on average) & `O(N)` space (32ms 100%)
```
class Solution:
    def rangeSum(self, nums: List[int], n: int, left: int, right: int) -> int:
        h = [(x, i) for i, x in enumerate(nums)] #min-heap 
        heapify(h)
        
        ans = 0
        for k in range(1, right+1): #1-indexed
            x, i = heappop(h)
            if k >= left: ans += x
            if i+1 < len(nums): 
                heappush(h, (x + nums[i+1], i+1))
                
        return ans % 1_000_000_007
```
</p>


### [Python] Binary Search, Time O(NlogSum(A))
- Author: lee215
- Creation Date: Mon Jul 13 2020 03:24:43 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 11 2020 17:12:15 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
`count_sum_under` counts the number of subarray sums that  `<= score`
`sum_k_sums` returns the sum of k smallest sums of sorted subarray sums.
`kth_score` returns the `k`th sum in sorted subarray sums.

Oral explanation refers to youtube channel.
<br>

# Complexity
Time `O(NlogSum(A))`
Space `O(N)`
<br>

**Python**
```py
    def rangeSum(self, A, n, left, right):
        # B: partial sum of A
        # C: partial sum of B
        # Use prefix sum to precompute B and C
        B, C = [0] * (n + 1), [0] * (n + 1)
        for i in range(n):
            B[i + 1] = B[i] + A[i]
            C[i + 1] = C[i] + B[i + 1]

        # Use two pointer to
        # calculate the total number of cases if B[j] - B[i] <= score
        def count_sum_under(score):
            res = i = 0
            for j in range(n + 1):
                while B[j] - B[i] > score:
                    i += 1
                res += j - i
            return res

        # calculate the sum for all numbers whose indices are <= index k
        def sum_k_sums(k):
            score = kth_score(k)
            res = i = 0
            for j in range(n + 1):
                # Proceed until B[i] and B[j] are within score
                while B[j] - B[i] > score:
                    i += 1
                res += B[j] * (j - i + 1) - (C[j] - (C[i - 1] if i else 0))
            return res - (count_sum_under(score) - k) * score

        # use bisearch to find how many numbers ae below k
        def kth_score(k):
            l, r = 0, B[n]
            while l < r:
                m = (l + r) / 2
                if count_sum_under(m) < k:
                    l = m + 1
                else:
                    r = m
            return l

        # result between left and right can be converted to [0, right] - [0, left-1] (result below right - result below left-1)
        return sum_k_sums(right) - sum_k_sums(left - 1)
```
</p>


