---
title: "Maximum Students Taking Exam"
weight: 1261
#id: "maximum-students-taking-exam"
---
## Description
<div class="description">
<p>Given a <code>m&nbsp;* n</code>&nbsp;matrix <code>seats</code>&nbsp;&nbsp;that represent seats distributions&nbsp;in a classroom.&nbsp;If a seat&nbsp;is&nbsp;broken, it is denoted by <code>&#39;#&#39;</code> character otherwise it is denoted by a <code>&#39;.&#39;</code> character.</p>

<p>Students can see the answers of those sitting next to the left, right, upper left and upper right, but he cannot see the answers of the student sitting&nbsp;directly in front or behind him. Return the <strong>maximum </strong>number of students that can take the exam together&nbsp;without any cheating being possible..</p>

<p>Students must be placed in seats in good condition.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img height="200" src="https://assets.leetcode.com/uploads/2020/01/29/image.png" width="339" />
<pre>
<strong>Input:</strong> seats = [[&quot;#&quot;,&quot;.&quot;,&quot;#&quot;,&quot;#&quot;,&quot;.&quot;,&quot;#&quot;],
&nbsp;               [&quot;.&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;.&quot;],
&nbsp;               [&quot;#&quot;,&quot;.&quot;,&quot;#&quot;,&quot;#&quot;,&quot;.&quot;,&quot;#&quot;]]
<strong>Output:</strong> 4
<strong>Explanation:</strong> Teacher can place 4 students in available seats so they don&#39;t cheat on the exam. 
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> seats = [[&quot;.&quot;,&quot;#&quot;],
&nbsp;               [&quot;#&quot;,&quot;#&quot;],
&nbsp;               [&quot;#&quot;,&quot;.&quot;],
&nbsp;               [&quot;#&quot;,&quot;#&quot;],
&nbsp;               [&quot;.&quot;,&quot;#&quot;]]
<strong>Output:</strong> 3
<strong>Explanation:</strong> Place all students in available seats. 

</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> seats = [[&quot;#&quot;,&quot;.&quot;,&quot;<strong>.</strong>&quot;,&quot;.&quot;,&quot;#&quot;],
&nbsp;               [&quot;<strong>.</strong>&quot;,&quot;#&quot;,&quot;<strong>.</strong>&quot;,&quot;#&quot;,&quot;<strong>.</strong>&quot;],
&nbsp;               [&quot;<strong>.</strong>&quot;,&quot;.&quot;,&quot;#&quot;,&quot;.&quot;,&quot;<strong>.</strong>&quot;],
&nbsp;               [&quot;<strong>.</strong>&quot;,&quot;#&quot;,&quot;<strong>.</strong>&quot;,&quot;#&quot;,&quot;<strong>.</strong>&quot;],
&nbsp;               [&quot;#&quot;,&quot;.&quot;,&quot;<strong>.</strong>&quot;,&quot;.&quot;,&quot;#&quot;]]
<strong>Output:</strong> 10
<strong>Explanation:</strong> Place students in available seats in column 1, 3 and 5.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>seats</code>&nbsp;contains only characters&nbsp;<code>&#39;.&#39;<font face="sans-serif, Arial, Verdana, Trebuchet MS">&nbsp;and</font></code><code>&#39;#&#39;.</code></li>
	<li><code>m ==&nbsp;seats.length</code></li>
	<li><code>n ==&nbsp;seats[i].length</code></li>
	<li><code>1 &lt;= m &lt;= 8</code></li>
	<li><code>1 &lt;= n &lt;= 8</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- SAP - 5 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A simple tutorial on this bitmasking problem
- Author: zerotrac2
- Creation Date: Sun Feb 09 2020 13:22:14 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 10 2020 04:55:00 GMT+0800 (Singapore Standard Time)

<p>
Bitmasking DP rarely appears in weekly contests. This tutorial will introduce my own perspective of bitmasking DP as well as several coding tricks when dealing with bitmasking problems. I will also give my solution to this problem at the end of this tutorial.

What is bitmasking? Bitmasking is something related to **bit** and **mask**. For the **bit** part, everything is encoded as a single **bit**, so the whole state can be encoded as a group of **bits**, i.e. a binary number. For the **mask** part, we use 0/1 to represent the state of something. In most cases, 1 stands for the valid state while 0 stands for the invalid state.

Let us consider an example. There are 4 cards on the table and I am going to choose several of them. We can encode the 4 cards as 4 bits. Say, if we choose cards 0, 1 and 3, we will use the binary number "1011" to represent the state. If we choose card 2, we will use the binary number "0100" then. The bits on the right represent cards with smaller id.

As we all know, integers are stored as binary numbers in the memory but appear as decimal numbers when we are coding. As a result, we tend to use a decimal number instead of a binary number to represent a state. In the previous example, we would use "11" and "4" instead of "1011" and "0100".

When doing Bitmasking DP, we are always handling problems like "what is the i-th bit in the state" or "what is the number of valid bits in a state". These problems can be very complicated if we do not handle them properly. I will show some coding tricks below which we can make use of and solve this problem.

- We can use **(x >> i) & 1** to get i-th bit in state **x**, where **>>** is the right shift operation. If we are doing this in an if statement (i.e. to check whether the i-th bit is 1), we can also use **x & (1 << i)**, where the **<<** is the left shift operation.

- We can use **(x & y) == x** to check if **x** is a subset of **y**. The subset means every state in **x** could be 1 only if the corresponding state in **y** is 1.

- We can use **(x & (x >> 1)) == 0** to check if there are no adjancent valid states in **x**.

Now we can come to the problem. We can use a bitmask of n bits to represent the validity of each row in the classroom. The i-th bit is 1 if and only if the i-th seat is not broken. For the first example in this problem, the bitmasks will be "010010", "100001" and "010010". When we arrange the students to seat in this row, we can also use n bits to represent the students. The i-th bit is 1 if and only if the i-th seat is occupied by a student. We should notice that n bits representing students must be a subset of n bits representing seats.

We denote **dp[i][mask]** as the maximum number of students for the first **i** rows while the students in the i-th row follow the masking **mask**. There should be no adjancent valid states in **mask**. The transition function is:

```
dp[i][mask] = max(dp[i - 1][mask\']) + number of valid bits(mask)
```

where **mask\'** is the masking for the (i-1)-th row. To prevent students from cheating, the following equation must hold:

- **(mask & (mask\' >> 1)) == 0**, there should be no students in the upper left position for every student.

- **((mask >> 1) & mask\') == 0**, there should be no students in the upper right position for every student.

If these two equation holds and **dp[i - 1][mask\']** itself is valid, we could then transit from **dp[i - 1][mask\']** to **dp[i][mask]** according to the transition function.

And the last question is, how can we compute the number of valid bits in a masking efficiently? In C++, we can simply use the built-in function **__builtin_popcount(mask)**. For other programming languages, we can pre-compute by using **count[i] = count[i/2] + (i % 2 == 1)** and store them in an array.

```C++
class Solution {
public:
    int maxStudents(vector<vector<char>>& seats) {
        int m = seats.size();
        int n = seats[0].size();
        vector<int> validity; // the validity of each row in the classroom
        for (int i = 0; i < m; ++i) {
            int cur = 0;
            for (int j = 0; j < n; ++j) {
				// the j-th bit is 1 if and only if the j-th seat is not broken
				// here the 0th bit is the most significant bit
                cur = cur * 2 + (seats[i][j] == \'.\');
            }
            validity.push_back(cur);
        }
		
		// all the f states are set -1 as invalid states in the beginning
		// here f[i][mask] represents the first i-1 rows to handle corner cases
        vector<vector<int>> f(m + 1, vector<int>(1 << n, -1));
		// f[0][0] is a valid state
		// think of a virtual row in the front and no students are sitting in that row
        f[0][0] = 0;
        for (int i = 1; i <= m; ++i) {
            int valid = validity[i - 1];
			// the interval [0, 1 << n) includes all the n-bit states for a row of students
			// please note that state 0 represents no student sitting in this row
			// which is always a valid state
            for (int j = 0; j < (1 << n); ++j) {
				// (j & valid) == j: check if j is a subset of valid
				// !(j & (j >> 1)): check if there is no adjancent students in the row
                if ((j & valid) == j && !(j & (j >> 1))) {
					// f[i][j] may transit from f[i -1][k]
                    for (int k = 0; k < (1 << n); ++k) {
						// !(j & (k >> 1)): no students in the upper left positions
						// !((j >> 1) & k): no students in the upper right positions
						// f[i - 1][k] != -1: the previous state is valid
                        if (!(j & (k >> 1)) && !((j >> 1) & k) && f[i - 1][k] != -1) {
                            f[i][j] = max(f[i][j], f[i - 1][k] + __builtin_popcount(j));
                        }
                    }
                }
            }
        }
        
		// the answer is the maximum among all f[m][mask]
        return *max_element(f[m].begin(), f[m].end());
    }
};
```

**Update 1:** Fix several typos and add comments to my code.


</p>


### [Python] Hungarian Time O(m^2*n^2) Space O(m*n), beat 100%
- Author: wxy9018
- Creation Date: Sun Feb 09 2020 14:46:35 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 10 2020 03:10:27 GMT+0800 (Singapore Standard Time)

<p>
The idea is that seats on even columns and seats on odd columns form a bipartite graph. Therefore, the maximum independent set on the bipartite graph is the solution.

Solving such a problem with Hungarian is O(VE) where V is the number of vertices and E is the number of edges. In this problem, we have O(mn) nodes and O(mn) edges, thus total O(m^2  * n^2), where m and n are the dimensions of the input matrix.

Credit goes to @cuiaoxiang @zerotrac2 for the idea and discussion.

```
    def maxStudents(self, seats: List[List[str]]) -> int:
        R, C = len(seats), len(seats[0])
        
        matching = [[-1] * C for _ in range(R)]
        
        def dfs(node, seen):
            r, c = node
            for nr, nc in [[r-1,c-1], [r,c-1],[r,c+1],[r-1,c+1],[r+1,c-1],[r+1,c+1]]: # assume a virtual edge connecting students who can spy
                if 0 <= nr < R and 0 <= nc < C and seen[nr][nc] == False and seats[nr][nc] == \'.\':
                    seen[nr][nc] = True
                    if matching[nr][nc] == -1 or dfs(matching[nr][nc], seen):
                        matching[nr][nc] = (r,c)
                        return True
            return False
        
        def Hungarian():
            res = 0
            for c in range(0,C,2):
                for r in range(R):
                    if seats[r][c] == \'.\':
                        seen = [[False] * C for _ in range(R)]
                        if dfs((r,c), seen):
                            res += 1
            return res
        
        res = Hungarian()
                
        count = 0
        for r in range(R):
            for c in range(C):
                if seats[r][c] == \'.\':
                    count += 1
        return count - res
```
</p>


### [JAVA] Top-Down DP Solution
- Author: FrenkieDeJong
- Creation Date: Sun Feb 09 2020 12:05:22 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 10 2020 13:25:40 GMT+0800 (Singapore Standard Time)

<p>
* process row by row
* mask represents occupacy status for a row, for example 100001 means 1st  and 6th seats are taken
	* use backtrack to get all combination of seat assignment for a row based on previous row\'s mask
* memo[i][j] stores the max student count for row range in [i, RowCount - 1] when the previous row\'s mask is j
```
class Solution {
    
    int r, c;
    int[][] memo;
    List<Integer> masks;
    
    public int maxStudents(char[][] seats) {
        r = seats.length;
        c = seats[0].length;
        memo = new int[r][1<<c];
        for (int i = 0; i < r; i++) {
            Arrays.fill(memo[i], -1);
        }
        return getMax(seats, 0, 0);
    }
    
    private int getMax(char[][] seats, int curRow, int prevRowMask) {
        if (curRow == r) {
            return 0;
        }
        if (memo[curRow][prevRowMask] != -1){
            return memo[curRow][prevRowMask];
        }
        masks = new LinkedList<>(); // reset the masks list for backtrack
        backtrack(seats[curRow], 0, prevRowMask, 0); // backtrack results store in masks list
        int res = 0;
        for (int m : masks) {
            res = Math.max(res, Integer.bitCount(m) + getMax(seats, curRow + 1, m));
        }
        memo[curRow][prevRowMask] = res;
        return res;
    }
    
    // this returns all combination of legal seat assignment for a given row based on prevous row\'s mask
    private void backtrack(char[] seats, int cur, int prevRowMask, int curRowMask) {
        if (cur == c) {
            masks.add(curRowMask);
            return;
        }
        // cur seat is not taken
        backtrack(seats, cur + 1, prevRowMask, curRowMask);
        
        // cur seat is taken, check if left, upper left and upper right positions are empty
        if (seats[cur] != \'#\' 
            && (cur == 0 || (((curRowMask & (1 << (cur-1))) == 0) && (prevRowMask & (1 << (cur-1))) == 0))
            && (cur == c - 1 || ((prevRowMask & (1 << (cur+1))) == 0))) {
            curRowMask |= (1 << (cur));
            backtrack(seats, cur + 1, prevRowMask, curRowMask);
            curRowMask ^= (1 << (cur));
        }
    }
}
```
</p>


