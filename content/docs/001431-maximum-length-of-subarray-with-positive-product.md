---
title: "Maximum Length of Subarray With Positive Product"
weight: 1431
#id: "maximum-length-of-subarray-with-positive-product"
---
## Description
<div class="description">
<p>Given an array of integers&nbsp;<code>nums</code>, find&nbsp;the maximum length of a subarray where the product of all its elements is positive.</p>

<p>A subarray of an array is a consecutive sequence of zero or more values taken out of that array.</p>

<p>Return&nbsp;<em>the maximum length of a subarray with positive product</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,-2,-3,4]
<strong>Output:</strong> 4
<strong>Explanation: </strong>The array nums already has a positive product of 24.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,1,-2,-3,-4]
<strong>Output:</strong> 3
<strong>Explanation: </strong>The longest subarray with positive product is [1,-2,-3] which has a product of 6.
Notice that we cannot include 0 in the subarray since that&#39;ll make the product 0 which is not positive.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [-1,-2,-3,0,1]
<strong>Output:</strong> 2
<strong>Explanation: </strong>The longest subarray with positive product is [-1,-2] or [-2,-3].
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [-1,2]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,5,-6,4,0,10]
<strong>Output:</strong> 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10^5</code></li>
	<li><code>-10^9 &lt;= nums[i]&nbsp;&lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Arcesium - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java, O(n) time, O(1) space
- Author: fighting_for_flag
- Creation Date: Sun Aug 30 2020 12:00:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 30 2020 12:24:20 GMT+0800 (Singapore Standard Time)

<p>
```
    public int getMaxLen(int[] nums) {
        // sum is used to count the number of negative numbers from zeroPosition to current index
        int firstNegative = -1, zeroPosition = -1, sum = 0, max = 0;
        for(int i = 0;i < nums.length; i++){
            if(nums[i] < 0){
                sum++;
				// we only need to know index of first negative number
                if(firstNegative == -1) firstNegative = i;
            }
			// if current number is 0, we can\'t use any element from index 0 to i anymore, so update zeroPosition, and reset sum and firstNegative. If it is a game, we should refresh the game when we meet 0. 
            if(nums[i] == 0){
                sum = 0;
                firstNegative = -1;
                zeroPosition = i;
            }
            else{
			    // consider index of zero
                if(sum%2 == 0) max = Math.max(i - zeroPosition, max);
				// consider index of first negative number
                else max = Math.max(i - firstNegative, max);   
            }
        }
        return max;
    }
```
</p>


### [Python3/Go/Java] Dynamic Programming O(N) time O(1) space
- Author: kunqian
- Creation Date: Sun Aug 30 2020 12:07:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 19 2020 11:43:35 GMT+0800 (Singapore Standard Time)

<p>
### Explanation
"pos[i]", "neg[i]" represent longest consecutive numbers ending with nums[i] forming a positive/negative product.

#### Complexity
`time`: O(N)
`space`: O(N)

#### Python3:

```python
def getMaxLen(self, nums: List[int]) -> int:
        n = len(nums)
        pos, neg = [0] * n, [0] * n
        if nums[0] > 0: pos[0] = 1
        if nums[0] < 0: neg[0] = 1
        ans = pos[0]
        for i in range(1, n):
            if nums[i] > 0:
                pos[i] = 1 + pos[i - 1]
                neg[i] = 1 + neg[i - 1] if neg[i - 1] > 0 else 0
            elif nums[i] < 0:
                pos[i] = 1 + neg[i - 1] if neg[i - 1] > 0 else 0
                neg[i] = 1 + pos[i - 1]
            ans = max(ans, pos[i])
        return ans
```


#### Golang:

```Golang
func getMaxLen(nums []int) int {
    n := len(nums)
    pos := make([]int, n)
    neg := make([]int, n)
    if nums[0] > 0 {
        pos[0] = 1
    } else if nums[0] < 0 {
        neg[0] = 1
    }
    ans := pos[0]
    for i := 1; i < n; i++ {
        if nums[i] > 0 {
            pos[i] = 1 + pos[i - 1]
            if neg[i - 1] > 0 {
                neg[i] = 1 + neg[i - 1]
            } else {
                neg[i] = 0
            }
        } else if nums[i] < 0 {
            if neg[i - 1] > 0 {
                pos[i] = 1 + neg[i - 1]
            } else {
                pos[i] = 0
            }
            neg[i] = 1 + pos[i - 1]
        }
        ans = Max(ans, pos[i])
    }
    return ans
}

func Max(a, b int) int {
    if a > b {
        return a
    }
    return b
}
```


#### Java:

```Java
public int getMaxLen(int[] nums) {
        int n = nums.length;
        int[] pos = new int[n];
        int[] neg = new int[n];
        if (nums[0] > 0) pos[0] = 1;
        if (nums[0] < 0) neg[0] = 1;
        int ans = pos[0];
        for (int i = 1; i < n; i++) {
            if (nums[i] > 0) {
                pos[i] = 1 + pos[i - 1];
                neg[i] = neg[i - 1] > 0 ? 1 + neg[i - 1]:0;
            } else if (nums[i] < 0) {
                pos[i] = neg[i - 1] > 0 ? 1 + neg[i - 1]:0;
                neg[i] = 1 + pos[i - 1];
            }
            ans = Math.max(ans, pos[i]);
        }
        return ans;
    }
```


### Space Optimization

### Complexity
`time`: O(N)
`space`: O(1)

Thanks @Spin0za for pointing out.
Since only the previous one value of pos/neg is used, we can use 2 variables instead of 2 lists.

```python
def getMaxLen(self, nums: List[int]) -> int:
        n = len(nums)
        pos, neg = 0, 0
        if nums[0] > 0: pos = 1
        if nums[0] < 0: neg = 1
        ans = pos
        for i in range(1, n):
            if nums[i] > 0:
                pos = 1 + pos
                neg = 1 + neg if neg > 0 else 0
            elif nums[i] < 0:
                pos, neg = 1 + neg if neg > 0 else 0, 1 + pos
            else:
                pos, neg = 0, 0
            ans = max(ans, pos)
        return ans
```
</p>


### Video solution and code O(N) time and O(1) space
- Author: leadcoding
- Creation Date: Sun Aug 30 2020 12:01:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 30 2020 12:06:23 GMT+0800 (Singapore Standard Time)

<p>
https://youtu.be/vmY9ctncXQI?t=1

code:

		class Solution {
		public:
			int getMaxLen(vector<int>& nums)
			{
				int n=nums.size();
				int ans=0;
				for(int i=0;i<n;)
				{
					int s=i;
					while(s<n&&nums[s]==0)s++;
					int e=s;
					int c=0;
					int sn=-1,en=-1;
					while(e<n&&nums[e]!=0)
					{
						if(nums[e]<0)
						{
							c++;
							if(sn==-1)sn=e;
							en=e;
						}
						e++;
					}
					if(c%2==0)ans=max(ans,e-s);
					else
					{
						if(sn!=-1)ans=max(ans,e-sn-1);
						if(en!=-1)ans=max(ans,en-s);
					}
					i=e+1;
				}
				return ans;
			}
		};
</p>


