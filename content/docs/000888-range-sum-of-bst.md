---
title: "Range Sum of BST"
weight: 888
#id: "range-sum-of-bst"
---
## Description
<div class="description">
<p>Given the <code>root</code> node of a binary search tree, return the sum of values of all nodes with value between <code>L</code> and <code>R</code> (inclusive).</p>

<p>The binary search tree is guaranteed to have unique values.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>root = <span id="example-input-1-1">[10,5,15,3,7,null,18]</span>, L = <span id="example-input-1-2">7</span>, R = <span id="example-input-1-3">15</span>
<strong>Output: </strong><span id="example-output-1">32</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>root = <span id="example-input-2-1">[10,5,15,3,7,13,18,1,null,6]</span>, L = <span id="example-input-2-2">6</span>, R = <span id="example-input-2-3">10</span>
<strong>Output: </strong><span id="example-output-2">23</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>The number of nodes in the tree is at most <code>10000</code>.</li>
	<li>The final answer is guaranteed to be less than <code>2^31</code>.</li>
</ol>
</div>
</div>
</div>

## Tags
- Tree (tree)
- Recursion (recursion)

## Companies
- Facebook - 41 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Depth First Search

**Intuition and Algorithm**

We traverse the tree using a depth first search.  If `node.val` falls outside the range `[L, R]`, (for example `node.val < L`), then we know that only the right branch could have nodes with value inside `[L, R]`.

We showcase two implementations - one using a recursive algorithm, and one using an iterative one.

**Recursive Implementation**

<iframe src="https://leetcode.com/playground/TDxy9GFN/shared" frameBorder="0" width="100%" height="378" name="TDxy9GFN"></iframe>

**Iterative Implementation**

<iframe src="https://leetcode.com/playground/K5yP4ksS/shared" frameBorder="0" width="100%" height="378" name="K5yP4ksS"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the tree.

* Space Complexity:  $$O(H)$$, where $$H$$ is the height of the tree.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] 3 similar recursive and 1 iterative methods w/ comment & analysis.
- Author: rock
- Creation Date: Sun Nov 11 2018 12:02:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 29 2020 00:40:15 GMT+0800 (Singapore Standard Time)

<p>
Three similar recursive and one iterative methods, choose one you like.

**Method 1:**
```
    public int rangeSumBST(TreeNode root, int L, int R) {
        if (root == null) return 0; // base case.
        if (root.val < L) return rangeSumBST(root.right, L, R); // left branch excluded.
        if (root.val > R) return rangeSumBST(root.left, L, R); // right branch excluded.
        return root.val + rangeSumBST(root.right, L, R) + rangeSumBST(root.left, L, R); // count in both children.
    }
```
```
    def rangeSumBST(self, root: TreeNode, L: int, R: int) -> int:
        if not root:
            return 0
        elif root.val < L:
            return self.rangeSumBST(root.right, L, R)
        elif root.val > R:
            return self.rangeSumBST(root.left, L, R)
        return root.val + self.rangeSumBST(root.left, L, R) + self.rangeSumBST(root.right, L, R)
```
The following are two more similar recursive codes.

**Method 2:**
```
    public int rangeSumBST(TreeNode root, int L, int R) {
        if (root == null) return 0; // base case.
        return (L <= root.val && root.val <= R ? root.val : 0) + rangeSumBST(root.right, L, R) + rangeSumBST(root.left, L, R);
    }
```
```
    def rangeSumBST(self, root: TreeNode, L: int, R: int) -> int:
        if not root:
            return 0
        return self.rangeSumBST(root.left, L, R) + \
                self.rangeSumBST(root.right, L, R) + \
                (root.val if L <= root.val <= R else 0)
```
**Method 3:**
```
    public int rangeSumBST(TreeNode root, int L, int R) {
        if (root == null) { return 0; }
        int sum = 0;
        if (root.val > L) { sum += rangeSumBST(root.left, L, R); } // left child is a possible candidate.
        if (root.val < R) { sum += rangeSumBST(root.right, L, R); } // right child is a possible candidate.
        if (root.val >= L && root.val <= R) { sum += root.val; } // count root in.
        return sum;
    }
```
```
    def rangeSumBST(self, root: TreeNode, L: int, R: int) -> int:
        if not root:
            return 0
        sum = 0
        if root.val > L:
            sum += self.rangeSumBST(root.left, L, R)
        if root.val < R:
            sum += self.rangeSumBST(root.right, L, R)
        if L <= root.val <= R:
            sum += root.val     
        return sum
```

**Method 4: Iterative version**
```
    public int rangeSumBST(TreeNode root, int L, int R) {
        Stack<TreeNode> stk = new Stack<>();
        stk.push(root);
        int sum = 0;
        while (!stk.isEmpty()) {
            TreeNode n = stk.pop();
            if (n == null) { continue; }
            if (n.val > L) { stk.push(n.left); } // left child is a possible candidate.
            if (n.val < R) { stk.push(n.right); } // right child is a possible candidate.
            if (L <= n.val && n.val <= R) { sum += n.val; }
        }
        return sum;
    }
```
```
    def rangeSumBST(self, root: TreeNode, L: int, R: int) -> int:
        stk, sum = [root], 0
        while stk:
            node = stk.pop()
            if node:
                if node.val > L:
                    stk.append(node.left)    
                if node.val < R:
                    stk.append(node.right)
                if L <= node.val <= R:
                    sum += node.val    
        return sum
```
**Analysis:**

All 4 methods will DFS traverse all nodes in worst case, and if we count in the recursion trace space cost, the complexities are as follows:

**Time: O(n), space: O(h)**, where n is the number of total nodes, h is the height of the tree..
</p>


### "Between" here means arithmetical not graphically
- Author: Letitia_Xi
- Creation Date: Thu Jul 25 2019 00:40:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 25 2019 00:40:04 GMT+0800 (Singapore Standard Time)

<p>
I was confused about it for a while, hope it can help. So for the first example, it is 7+10+15=32, 5 is in the path but not counted.
</p>


### Java 4 lines Beats 100%
- Author: gordchan
- Creation Date: Sat Dec 15 2018 01:34:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Dec 15 2018 01:34:17 GMT+0800 (Singapore Standard Time)

<p>
```
 public int rangeSumBST(TreeNode root, int L, int R) {
        if(root == null) return 0;
        if(root.val > R) return rangeSumBST(root.left, L, R);
        if(root.val < L) return rangeSumBST(root.right, L, R);
        return root.val + rangeSumBST(root.left, L, R) + rangeSumBST(root.right, L, R);      
    }
```
</p>


