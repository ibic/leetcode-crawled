---
title: "Russian Doll Envelopes"
weight: 337
#id: "russian-doll-envelopes"
---
## Description
<div class="description">
<p>You have a number of envelopes with widths and heights given as a pair of integers <code>(w, h)</code>. One envelope can fit into another if and only if both the width and height of one envelope is greater than the width and height of the other envelope.</p>

<p>What is the maximum number of envelopes can you Russian doll? (put one inside other)</p>

<p><b>Note:</b><br />
Rotation is not allowed.</p>

<p><strong>Example:</strong></p>

<div>
<pre>
<strong>Input: </strong><span id="example-input-1-1">[[5,4],[6,4],[6,7],[2,3]]</span>
<strong>Output: </strong><span id="example-output-1">3 
<strong>Explanation: T</strong></span>he maximum number of envelopes you can Russian doll is <code>3</code> ([2,3] =&gt; [5,4] =&gt; [6,7]).
</pre>
</div>

</div>

## Tags
- Binary Search (binary-search)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: true)
- Microsoft - 4 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Intuition

The problem boils down to a two dimensional version of the longest increasing subsequence problem (LIS).

We must find the longest sequence `seq` such that the elements in `seq[i+1]` are greater than the corresponding elements in `seq[i]` (this means that `seq[i]` can fit into `seq[i+1]`).

The problem we run into is that the items we are given come in arbitrary order - we can't just run a standard LIS algorithm because we're allowed to rearrange our data. How can we order our data in a way such that our LIS algorithm will always find the best answer?

**Notes on the LIS algorithm**

You can find the longest increasing subsequence problem with a solution [here](https://leetcode.com/problems/longest-increasing-subsequence/). If you're not familiar with the $$O(N \log N)$$ algorithm please go visit that question as it's a prerequisite for this one.

For the sake of completeness here's a brief explanation on how the LIS algorithm used below works:

`dp` is an array such that `dp[i]` is the smallest element that ends an increasing subsequence of length `i + 1`. Whenever we encounter a new element `e`, we binary search inside `dp` to find the largest index `i` such that `e` can end that subsequence. We then update `dp[i]` with `e`.

The length of the LIS is the same as the length of `dp`, as if `dp` has an index `i`, then it must have a subsequence of length `i+1`.

---

#### Approach 1: Sort + Longest Increasing Subsequence

**Algorithm**

We answer the question from the intuition by sorting. Let's pretend that we found the best arrangement of envelopes. We know that each envelope must be increasing in `w`, thus our best arrangement has to be a subsequence of all our envelopes sorted on `w`.

After we sort our envelopes, we can simply find the length of the longest increasing subsequence on the second dimension (`h`). Note that we use a clever trick to solve some edge cases:

Consider an input `[[1, 3], [1, 4], [1, 5], [2, 3]]`.
If we simply sort and extract the second dimension we get `[3, 4, 5, 3]`, which implies that we can fit three envelopes (3, 4, 5). The problem is that we can only fit one envelope, since envelopes that are equal in the first dimension can't be put into each other.

In order fix this, we don't just sort increasing in the first dimension - we also sort _decreasing_ on the second dimension, so two envelopes that are equal in the first dimension can never be in the same increasing subsequence.

Now when we sort and extract the second element from the input we get `[5, 4, 3, 3]`, which correctly reflects an LIS of one.

**Implementation**
<iframe src="https://leetcode.com/playground/KpUprDGE/shared" frameBorder="0" width="100%" height="500" name="KpUprDGE"></iframe>


**Complexity Analysis**

* Time complexity : $$O(N \log N)$$, where $$N$$ is the length of the input. Both sorting the array and finding the LIS happen in $$O(N \log N)$$

* Space complexity : $$O(N)$$. Our `lis` function requires an array `dp` which goes up to size $$N$$. Also the sorting algorithm we use may also take additional space.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java NLogN Solution with Explanation
- Author: TianhaoSong
- Creation Date: Tue Jun 07 2016 14:40:32 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 15:13:03 GMT+0800 (Singapore Standard Time)

<p>
 1. Sort the array. Ascend on width and descend on height if width are same.
 2. Find the [longest increasing subsequence][1] based on height. 


----------

 - Since the width is increasing, we only need to consider height. 
 - [3, 4] cannot contains [3, 3], so we need to put [3, 4] before [3, 3] when sorting otherwise it will be counted as an increasing number if the order is [3, 3], [3, 4]


----------


    public int maxEnvelopes(int[][] envelopes) {
        if(envelopes == null || envelopes.length == 0 
           || envelopes[0] == null || envelopes[0].length != 2)
            return 0;
        Arrays.sort(envelopes, new Comparator<int[]>(){
            public int compare(int[] arr1, int[] arr2){
                if(arr1[0] == arr2[0])
                    return arr2[1] - arr1[1];
                else
                    return arr1[0] - arr2[0];
           } 
        });
        int dp[] = new int[envelopes.length];
        int len = 0;
        for(int[] envelope : envelopes){
            int index = Arrays.binarySearch(dp, 0, len, envelope[1]);
            if(index < 0)
                index = -(index + 1);
            dp[index] = envelope[1];
            if(index == len)
                len++;
        }
        return len;
    }


  [1]: https://leetcode.com/problems/longest-increasing-subsequence/
</p>


### A Trick to solve this problem.
- Author: anjani12
- Creation Date: Tue Jul 12 2016 07:40:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 09 2018 07:15:30 GMT+0800 (Singapore Standard Time)

<p>
You can solve this problem in this way :

let's suppose the values are given as...
[2,3]
[4,6]
[3,7]
[4,8]

If we **Sort** this envelopes in a tricky way that *Sort the envelopes according to width BUT when the values of height are same, we can sort it in reverse way* like this :

[2,3]
[3,7]
**[4,8]
[4,6]**

Now just **Do LIS on the all height values, you will get the answer**
</p>


### Simple DP solution
- Author: larrywang2014
- Creation Date: Tue Jun 07 2016 06:12:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 19:27:23 GMT+0800 (Singapore Standard Time)

<p>
    public int maxEnvelopes(int[][] envelopes) {
        if (   envelopes           == null
            || envelopes.length    == 0
            || envelopes[0]        == null
            || envelopes[0].length == 0){
            return 0;    
        }
        
        Arrays.sort(envelopes, new Comparator<int[]>(){
            @Override
            public int compare(int[] e1, int[] e2){
                return Integer.compare(e1[0], e2[0]);
            }
        });
        
        int   n  = envelopes.length;
        int[] dp = new int[n];
        
        int ret = 0;
        for (int i = 0; i < n; i++){
            dp[i] = 1;
            
            for (int j = 0; j < i; j++){
                if (   envelopes[i][0] > envelopes[j][0]
                    && envelopes[i][1] > envelopes[j][1]){
                    dp[i] = Math.max(dp[i], 1 + dp[j]);    
                }
            }
            
            ret = Math.max(ret, dp[i]);
        }
        return ret;
    }
</p>


