---
title: "Boundary of Binary Tree"
weight: 513
#id: "boundary-of-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree, return the values of its boundary in <b>anti-clockwise</b> direction starting from root. Boundary includes left boundary, leaves, and right boundary in order without duplicate <strong>nodes</strong>.&nbsp; (The values of the nodes may still be duplicates.)</p>

<p><b>Left boundary</b> is defined as the path from root to the <b>left-most</b> node. <b>Right boundary</b> is defined as the path from root to the <b>right-most</b> node. If the root doesn&#39;t have left subtree or right subtree, then the root itself is left boundary or right boundary. Note this definition only applies to the input binary tree, and not applies to any subtrees.</p>

<p>The <b>left-most</b> node is defined as a <b>leaf</b> node you could reach when you always firstly travel to the left subtree if exists. If not, travel to the right subtree. Repeat until you reach a leaf node.</p>

<p>The <b>right-most</b> node is also defined by the same way with left and right exchanged.</p>

<p><b>Example 1</b></p>

<pre>
<b>Input:</b>
  1
   \
    2
   / \
  3   4

<b>Ouput:</b>
[1, 3, 4, 2]

<b>Explanation:</b>
The root doesn&#39;t have left subtree, so the root itself is left boundary.
The leaves are node 3 and 4.
The right boundary are node 1,2,4. Note the anti-clockwise direction means you should output reversed right boundary.
So order them in anti-clockwise without duplicates and we have [1,3,4,2].
</pre>

<p>&nbsp;</p>

<p><b>Example 2</b></p>

<pre>
<b>Input:</b>
    ____1_____
   /          \
  2            3
 / \          / 
4   5        6   
   / \      / \
  7   8    9  10  
       
<b>Ouput:</b>
[1,2,4,7,8,9,10,6,3]

<b>Explanation:</b>
The left boundary are node 1,2,4. (4 is the left-most node according to definition)
The leaves are node 4,7,8,9,10.
The right boundary are node 1,3,6,10. (10 is the right-most node).
So order them in anti-clockwise without duplicate nodes we have [1,2,4,7,8,9,10,6,3].
</pre>

<p>&nbsp;</p>

</div>

## Tags
- Tree (tree)

## Companies
- eBay - 5 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: true)
- Audible - 4 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Simple Solution [Accepted]

**Algorithm**

One simple approach is to divide this problem into three subproblems- left boundary, leaves and right boundary.

* Left Boundary: We keep on traversing the tree towards the left and keep on adding the nodes in the $$res$$ array, provided the current node isn't a leaf node. If at any point, we can't find the left child of a node, but its right child exists, we put the right child in the $$res$$ and continue the process. The following animation depicts the process.

<!--![Left_Boundary](../Figures/545_Boundary_Left.gif)-->

!?!../Documents/545_Boundary_Of_Binary_Tree1.json:1000,563!?!

* Leaf Nodes: We make use of a recursive function `addLeaves(res,root)`, in which we change the root node for every recursive call. If the current root node happens to be a leaf node, it is added to the $$res$$ array. Otherwise, we make the recursive call using the left child of the current node as the new root. After this, we make the recursive call using the right child of the current node as the new root. The following animation depicts the process.

<!--![Leaf_Boundary](../Figures/545_Boundary_Leaf.gif)-->
!?!../Documents/545_Boundary_Of_Binary_Tree2.json:1000,563!?!

* Right Boundary: We perform the same process as the left boundary. But, this time, we traverse towards the right. If the right child doesn't exist, we move towards the left child. Also, instead of putting the traversed nodes in the $$res$$ array, we push them over a stack during the traversal. After the complete traversal is done, we pop the element from over the stack and append them to the $$res$$ array. The following animation depicts the process.

<!--![Right_Boundary](../Figures/545_Boundary_Right.gif)-->
!?!../Documents/545_Boundary_Of_Binary_Tree3.json:1000,563!?!



<iframe src="https://leetcode.com/playground/xEtX5sQc/shared" frameBorder="0" name="xEtX5sQc" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$ One complete traversal for leaves and two traversals upto depth of binary tree for left and right boundary.

* Space complexity : $$O(n)$$ $$res$$ and $$stack$$ is used.

---

#### Approach #2 Using PreOrder Traversal [Accepted]

**Algorithm**

Before we dive into this approach, let's look at the preorder traversal of a simple Binary Tree as shown below:

![Preorder Traversal](../Figures/545_Preorder.png)

From the above figure, we can observe that our problem statement is very similar to the Preorder traversal. Actually, the order of traversal is the same(except for the right boundary nodes, for which it is the reverse), but we need to selectively include the nodes in the return result list. Thus, we need to include only those nodes in the result, which are either on the left boundary, the leaves or the right boundary.

In order to distinguish between the various kinds of nodes, we make use of a $$flag$$ as follows:

* Flag=0: Root Node.

* Flag=1: Left Boundary Node.

* Flag=2: Right Boundary Node.

* Flag=3: Others(Middle Node).

We make use of three lists $$\text{left_boundary}$$, $$\text{right_boundary}$$, $$\text{leaves}$$ to store the appropriate nodes and append the three lists at the end.

We go for the normal preorder traversal, but while calling the recursive function for preorder traversal using the left child or the right child of the current node, we also pass the $$flag$$ information indicating the type of node that the current child behaves like.

For obtaining the flag information about the left child of the current node, we make use of the function `leftChildFlag(node, flag)`. In the case of a left child, the following cases are possible, as can be verified by looking at the figure above:

* The current node is a left boundary node: In this case, the left child will always be a left boundary node. e.g. relationship between E & J in the above figure.

* The current node is a root node: In this case, the left child will always be a left boundary node. e.g. relationship between A & B in the above figure.

* The current node is a right boundary node: In this case, if the right child of the current node doesn't exist, the left child always acts as the right boundary node. e.g. G & N. But, if the right child exists, the left child always acts as the middle node. e.g. C & F.

Similarly, for obtaining the flag information about the right child of the current node, we make use of the function `rightChildFlag(node, flag)`. In the case of a right child, the following cases are possible, as can be verified by looking at the figure above:

* The current node is a right boundary node: In this case, the right child will always be a right boundary node. e.g. relationship between C & G in the above figure.

* The current node is a root node: In this case, the right child will always be a left boundary node. e.g. relationship between A & C in the above figure.

* The current node is a left boundary node: In this case, if the left child of the current node doesn't exist, the right child always acts as the left boundary node. e.g. B & E. But, if the left child exists, the left child always acts as the middle node.

Making use of the above information, we set the $$flag$$ appropriately, which is used to determine the list in which the current node has to be appended.


<iframe src="https://leetcode.com/playground/2t96fXpN/shared" frameBorder="0" name="2t96fXpN" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$ One complete traversal of the tree is done.

* Space complexity : $$O(n)$$ The recursive stack can grow upto a depth of $$n$$. Further, $$\text{left_boundary}$$, $$\text{right_boundary}$$ and $$\text{leaves}$$ combined together can be of size $$n$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java(12ms) - left boundary, left leaves, right leaves, right boundary
- Author: earlme
- Creation Date: Sun Mar 26 2017 11:17:17 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 14:06:26 GMT+0800 (Singapore Standard Time)

<p>



    List<Integer> nodes = new ArrayList<>(1000);
    public List<Integer> boundaryOfBinaryTree(TreeNode root) {
        
        if(root == null) return nodes;

        nodes.add(root.val);
        leftBoundary(root.left);
        leaves(root.left);
        leaves(root.right);
        rightBoundary(root.right);
        
        return nodes;
    }
    public void leftBoundary(TreeNode root) {
        if(root == null || (root.left == null && root.right == null)) return;
        nodes.add(root.val);
        if(root.left == null) leftBoundary(root.right);
        else leftBoundary(root.left);
    }
    public void rightBoundary(TreeNode root) {
        if(root == null || (root.right == null && root.left == null)) return;
        if(root.right == null)rightBoundary(root.left);
        else rightBoundary(root.right);
        nodes.add(root.val); // add after child visit(reverse)
    }
    public void leaves(TreeNode root) {
        if(root == null) return;
        if(root.left == null && root.right == null) {
            nodes.add(root.val);
            return;
        }
        leaves(root.left);
        leaves(root.right);
    }
</p>


### [Java] [C++] Clean Code  (1 Pass perorder postorder hybrid)
- Author: alexander
- Creation Date: Mon Mar 27 2017 02:15:47 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 07 2018 09:05:46 GMT+0800 (Singapore Standard Time)

<p>
1. `node.left` is `left bound` if `node` is left bound;
    `node.right` could also be left bound if `node` is left bound && `node` has no right child;
2. Same applys for `right bound`;
3. if node is `left bound`, add it `before` 2 child - pre order;
    if node is `right bound`, add it `after` 2 child - post order;
4. A `leaf node` that is neither left or right bound belongs to the bottom line;

**C++**
```
class Solution {
public:
    vector<int> boundaryOfBinaryTree(TreeNode* root) {
        vector<int> bounds;
        if (root) {
            bounds.push_back(root->val);
            getBounds(root->left, bounds, true, false);
            getBounds(root->right, bounds, false, true);
        }
        return bounds;
    }

private:
    void getBounds(TreeNode* node, vector<int>& res, bool lb, bool rb) {
        if (!node)  return;
        if (lb) res.push_back(node->val);
        if (!lb && !rb && !node->left && !node->right)  res.push_back(node->val);
        getBounds(node->left, res, lb, rb && !node->right);
        getBounds(node->right, res, lb && !node->left, rb);
        if (rb) res.push_back(node->val);
    }
};
```
**Java**
```
public class Solution {
    public List<Integer> boundaryOfBinaryTree(TreeNode root) {
        List<Integer> res = new ArrayList<Integer>();
        if (root != null) {
            res.add(root.val);
            getBounds(root.left, res, true, false);
            getBounds(root.right, res, false, true);
        }
        return res;
    }

    private void getBounds(TreeNode node, List<Integer> res, boolean lb, boolean rb) {
        if (node == null) return;
        if (lb) res.add(node.val);
        if (!lb && !rb && node.left == null && node.right == null) res.add(node.val);
        getBounds(node.left, res, lb, rb && node.right == null);
        getBounds(node.right, res, lb && node.left == null, rb);
        if (rb) res.add(node.val);
    }
}
```
</p>


### python dfs solution
- Author: zqfan
- Creation Date: Sun Mar 26 2017 11:34:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 10:24:14 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def boundaryOfBinaryTree(self, root):
        def dfs_leftmost(node):
            if not node or not node.left and not node.right:
                return
            boundary.append(node.val)
            if node.left:
                dfs_leftmost(node.left)
            else:
                dfs_leftmost(node.right)

        def dfs_leaves(node):
            if not node:
                return
            dfs_leaves(node.left)
            if node != root and not node.left and not node.right:
                boundary.append(node.val)
            dfs_leaves(node.right)

        def dfs_rightmost(node):
            if not node or not node.left and not node.right:
                return
            if node.right:
                dfs_rightmost(node.right)
            else:
                dfs_rightmost(node.left)
            boundary.append(node.val)

        if not root:
            return []
        boundary = [root.val]
        dfs_leftmost(root.left)
        dfs_leaves(root)
        dfs_rightmost(root.right)
        return boundary
```
</p>


