---
title: "Replace Elements with Greatest Element on Right Side"
weight: 1108
#id: "replace-elements-with-greatest-element-on-right-side"
---
## Description
<div class="description">
<p>Given an array <code>arr</code>,&nbsp;replace every element in that array with the greatest element among the elements to its&nbsp;right, and replace the last element with <code>-1</code>.</p>

<p>After doing so, return the array.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> arr = [17,18,5,4,6,1]
<strong>Output:</strong> [18,6,6,6,1,-1]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 10^4</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 10^5</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Amazon - 5 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Dec 29 2019 00:12:45 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 31 2019 00:05:56 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Iterate from the back to the start,
We initilize `mx = 1`, where `mx` represent the max on the right.
Each round, we set `A[i] = mx`, where `mx` is its mas on the right.
Also we update `mx = max(mx, A[i])`, where `A[i]` is its  original value.
<br>

## **Complexity**
Time `O(N)`
Space `O(1)`
<br>

**Java:**
```java
    public int[] replaceElements(int[] A) {
        for (int i = A.length - 1, mx = -1; i >= 0; --i)
            mx = Math.max(A[i], A[i] = mx);
        return A;
    }
```

**Java, expanded version:**
```java
    public int[] replaceElements2(int[] A) {
        int mx = -1, n = A.length, a;
        for (int i = n - 1; i >= 0; --i) {
            a = A[i];
            A[i] = mx;
            mx = Math.max(mx, a);
        }
        return A;
    }
```

**C++:**
@0xFFFFFFFF suggest using `exchange`
```cpp
    vector<int> replaceElements(vector<int>& A, int mx = -1) {
        for (int i = A.size() - 1; i >= 0; --i)
            mx = max(mx, exchange(A[i], mx));
        return A;
    }
```

**Python:**
```python
    def replaceElements(self, A, mx = -1):
        for i in xrange(len(A) - 1, -1, -1):
            A[i], mx = mx, max(mx, A[i])
        return A
```

</p>


### [Java/Python 3] Scan from right to left.
- Author: rock
- Creation Date: Sun Dec 29 2019 00:07:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 29 2019 02:12:37 GMT+0800 (Singapore Standard Time)

<p>
```java
    public int[] replaceElements(int[] arr) {
        for (int i = arr.length - 1, max = -1; i >= 0; --i) {
            int tmp = arr[i];
            arr[i] = max;
            max = Math.max(max, tmp);
        }
        return arr;
    }
```
```python
    def replaceElements(self, arr: List[int]) -> List[int]:
        mx = -1
        for i in range(len(arr) - 1, -1, -1):
            arr[i], mx = mx, max(arr[i], mx)
        return arr
```
</p>


### I don't understand the question.
- Author: dhtmlkitchen
- Creation Date: Fri Mar 20 2020 00:59:17 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 20 2020 01:19:19 GMT+0800 (Singapore Standard Time)

<p>
> Given an array arr, replace every element in that array with the greatest element among the elements to its right, and replace the last element with -1.

After doing so, return the array.

 

Example 1:

Input: arr = [17,18,5,4,6,1]
Output: [18,6,6,6,1,-1]

-------------------------------------

Every element in `arr` is `[17,18,5,4,6,1]`. If we replace every element with the greatest element among the elements to its right.. Well there is no other element to the right of the array. After the array, the input is ended. 

If they meant repalce _each_ value in that array with the value of the greatest element to that element\'s right, that\'s different. Every and each have specific and distinct meaning.

Why are they lying in their question again? To confuse us?

Following what I\'ve written, whcih is different, we can get it to pass. We take ` [17,18,5,4,6,1]` and result in  [**18**,18,5,4,6,1], then [18,**6**,5,4,6,1], then [18,6,**6**,4,6,1], then [18,6,6,**6**,6,1], then , then [18,6,6,6,1,1]. 

And then, replacing the last element with `-1`, it becomes [18,6,6,6,1,-**1**].
```
/**
 * @param {number[]} arr
 * @return {number[]}
 */
const replaceElements = function(arr) {
    const b = Array.from(arr);
    for(let i = 0; i < b.length; i++) {
        b[i] = Math.max(...b.slice(i+1));
    }
    b[b.length-1] = -1;
    return b;
};
```
Is it a problem with LeetCode not knowing English? Or do they not know how to proofread? Or are they deliberately fucking the questions up to make easy questions impossible?
</p>


