---
title: "Maximum Area of a Piece of Cake After Horizontal and Vertical Cuts"
weight: 1348
#id: "maximum-area-of-a-piece-of-cake-after-horizontal-and-vertical-cuts"
---
## Description
<div class="description">
<p>Given a rectangular cake with height <code>h</code> and width <code>w</code>, and two arrays of integers <code>horizontalCuts</code> and <code>verticalCuts</code> where <code>horizontalCuts[i]</code> is the distance from the top of the rectangular cake to the <code>ith</code> horizontal cut&nbsp;and similarly, <code>verticalCuts[j]</code> is the distance from the&nbsp;left of the rectangular cake to the <code>jth</code>&nbsp;vertical cut.</p>

<p><em>Return the maximum area of a piece of cake after you cut at each horizontal and vertical position provided in the arrays <code>horizontalCuts</code> and <code>verticalCuts</code>.&nbsp;</em>Since the answer can be a huge number, return this modulo 10^9 + 7.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/05/14/leetcode_max_area_2.png" style="width: 300px; height: 320px;" /></p>

<pre>
<strong>Input:</strong> h = 5, w = 4, horizontalCuts = [1,2,4], verticalCuts = [1,3]
<strong>Output:</strong> 4 
<strong>Explanation:</strong> The figure above represents the given rectangular cake. Red lines are the horizontal and vertical cuts. After you cut the cake, the green piece of cake has the maximum area.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/05/14/leetcode_max_area_3.png" style="width: 300px; height: 320px;" /></strong></p>

<pre>
<strong>Input:</strong> h = 5, w = 4, horizontalCuts = [3,1], verticalCuts = [1]
<strong>Output:</strong> 6
<strong>Explanation:</strong> The figure above represents the given rectangular cake. Red lines are the horizontal and vertical cuts. After you cut the cake, the green and yellow pieces of cake have the maximum area.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> h = 5, w = 4, horizontalCuts = [3], verticalCuts = [3]
<strong>Output:</strong> 9
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= h,&nbsp;w &lt;= 10^9</code></li>
	<li><code>1 &lt;=&nbsp;horizontalCuts.length &lt;&nbsp;min(h, 10^5)</code></li>
	<li><code>1 &lt;=&nbsp;verticalCuts.length &lt; min(w, 10^5)</code></li>
	<li><code>1 &lt;=&nbsp;horizontalCuts[i] &lt; h</code></li>
	<li><code>1 &lt;=&nbsp;verticalCuts[i] &lt; w</code></li>
	<li>It is guaranteed that all elements in&nbsp;<code>horizontalCuts</code>&nbsp;are distinct.</li>
	<li>It is guaranteed that all elements in <code>verticalCuts</code>&nbsp;are distinct.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- BNY Mellon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Do you like visual explanation? You got it. :-) With 2 code variations.
- Author: interviewrecipes
- Creation Date: Sun May 31 2020 13:05:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 08 2020 13:29:37 GMT+0800 (Singapore Standard Time)

<p>
![image](https://assets.leetcode.com/users/images/d77449f7-8915-4f15-9fb6-6bff8b5e0682_1592971442.3801637.png)

**Key idea**:
If we need the area, we must think of lengths and breadths of each cake piece. 

**Assume**
LENGTH as total horizontal length of the original cake.
HEIGHT as total vertical height of the original cake.
H as number of horizontal cuts. HCUTS is array of horizontal cuts. 
Similarly, V as number of vertical cuts and VCUTS is an array of vertical cuts.

**Solution**
1. Lets only think of horizontal cuts. Each HCUTS[i] would create a piece with length LENGTH and height, say, `heights[i]`.
As there are H cuts, there will be (H+1) pieces of length LENGTH.

2. Now each vertical cut VCUTS[i] will cut each of the horizontal pieces that we got in step 1.
We already know the height of each piece (step 1), now with each vertical cut, we will know the length of each piece as well.

3. Because we want the maximize the area, we must try to maximize the length and height of each piece.

**Algorithm**
1. Find heights of pieces if we only perform the horizontal cuts. Say this array is heights[].
2. Find lengths of pieces if we only perform the vertical cuts. Say this arrays is lengths[].
3. Find max of heights[] and lengths[].
4. Multiply those two max and take mod 10e7.
5. Return the answer


Is there anything that is still unclear? Let me know and I can elaborate.

**Thanks**
Please upvote if you find this helpful, it will be encouraging.

**Code**
Variation 1 - Quick to understand, but a bit verbose.
```
class Solution {
public:
    const long long int mod = 1000000007;
    int maxArea(int hh, int ww, vector<int>& h, vector<int>& v) {
        // Sort
        sort(h.begin(), h.end());
        sort(v.begin(), v.end());
        
        
        // Horizontal
        vector<int> heights = {h[0]};
        int nh = h.size();
        for (int i=1; i<nh; i++) {
            heights.push_back(h[i]-h[i-1]);
        }
        heights.push_back(hh-h[nh-1]);
        
        // Vertical
        vector<int> lengths = {v[0]};
        int nv = v.size();
        for (int i=1; i<nv; i++) {
            lengths.push_back(v[i]-v[i-1]);
        }
        lengths.push_back(ww-v[nv-1]);
        
        // Take max
        long long int a = *max_element(heights.begin(), heights.end());
        long long int b = *max_element(lengths.begin(), lengths.end());
        
        // Multiply and return
        return (int)(a%mod*b%mod);
    }
};
```

Variation 2 - Compact, clean
```
class Solution {
    int getMax(int hh, vector<int> &h) {
        sort(h.begin(), h.end()); 
        int nh = h.size();
        int maxHeight = max(hh-h[nh-1], h[0]);
        for (int i=1; i<nh; i++) {
            maxHeight = max(maxHeight, h[i]-h[i-1]);
        }
        return maxHeight;
    }
public:
    int maxArea(int hh, int ww, vector<int>& h, vector<int>& v) {
        return (int)((long)getMax(hh, h)*getMax(ww, v)%1000000007);
    }
};
```
</p>


### C++/Java Maximum Gap Between Cuts
- Author: votrubac
- Creation Date: Sun May 31 2020 12:01:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 31 2020 12:31:35 GMT+0800 (Singapore Standard Time)

<p>
The biggest piece is formed by the biggest vertical, and the biggest horizontal gap between cuts.

So, we sort cuts, and find the maximum gap. The gap for the first and the last cut is defined by the cake\'s edge.

**C++**
```cpp
int maxArea(int h, int w, vector<int>& hCuts, vector<int>& vCuts) {
    sort(begin(hCuts), end(hCuts));
    sort(begin(vCuts), end(vCuts));
    auto max_h = max(hCuts[0], h - hCuts.back());
    auto max_v = max(vCuts[0], w - vCuts.back());
    for (auto i = 0; i < hCuts.size() - 1; ++i)
        max_h = max(max_h, hCuts[i + 1] - hCuts[i]);
    for (auto i = 0; i < vCuts.size() - 1; ++i)
        max_v = max(max_v, vCuts[i + 1] - vCuts[i]);        
    return (long)max_h * max_v % 1000000007;
}
```

**Java**
```java
public int maxArea(int h, int w, int[] hCuts, int[] vCuts) {
    Arrays.sort(hCuts);
    Arrays.sort(vCuts);
    int max_h = Math.max(hCuts[0], h - hCuts[hCuts.length - 1]);
    int max_v = Math.max(vCuts[0], w - vCuts[vCuts.length - 1]);  
    for (int i = 0; i < hCuts.length - 1; ++i)
        max_h = Math.max(max_h, hCuts[i + 1] - hCuts[i]);
    for (int i = 0; i < vCuts.length - 1; ++i)
        max_v = Math.max(max_v, vCuts[i + 1] - vCuts[i]);        
    return (int)((long)max_h * max_v % 1000000007);        
}
```
**Complexity Analysis**
- Time: O(v log v + h log h), where *v* and *h* are the number of vertical and horizontal cuts.
- Memory: O(1), plus memory required by the sort algorithm (from O(log n) to O(n), where *n* is the number of elements we sort).
</p>


### Java max dist btw cuts
- Author: hobiter
- Creation Date: Sun May 31 2020 12:01:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 01 2020 01:14:30 GMT+0800 (Singapore Standard Time)

<p>
Just find max distance btw edges or each cuts horizontally and vertically.
```
	public int maxArea(int h, int w, int[] hc, int[] vc) {
        return (int) ((getMaxDist(h, hc) * getMaxDist(w, vc)) % (Math.pow(10, 9) + 7));
    }
    
    private long getMaxDist(int end, int[] cuts) {
        Arrays.sort(cuts);
        long res = 0, from = 0;
        for (int c : cuts) {
            res = Math.max(res, c - from);
            from = c;
        }
        return Math.max(res, end - from);
    }
```
</p>


