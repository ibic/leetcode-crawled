---
title: "Minimum Swaps to Make Strings Equal"
weight: 1191
#id: "minimum-swaps-to-make-strings-equal"
---
## Description
<div class="description">
<p>You are given two strings&nbsp;<code>s1</code>&nbsp;and&nbsp;<code>s2</code>&nbsp;of equal length&nbsp;consisting of letters <code>&quot;x&quot;</code> and <code>&quot;y&quot;</code> <strong>only</strong>. Your task is to&nbsp;make these two strings equal to each other. You can swap any two characters that belong to <strong>different</strong> strings,&nbsp;which means: swap <code>s1[i]</code> and <code>s2[j]</code>.</p>

<p>Return&nbsp;the minimum number of swaps required&nbsp;to make&nbsp;<code>s1</code>&nbsp;and <code>s2</code> equal, or return&nbsp;<code>-1</code>&nbsp;if it is&nbsp;impossible to do so.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s1 = &quot;xx&quot;, s2 = &quot;yy&quot;
<strong>Output:</strong> 1
<strong>Explanation: 
</strong>Swap s1[0] and s2[1], s1 = &quot;yx&quot;, s2 = &quot;yx&quot;.</pre>

<p><strong>Example 2:&nbsp;</strong></p>

<pre>
<strong>Input:</strong> s1 = &quot;xy&quot;, s2 = &quot;yx&quot;
<strong>Output:</strong> 2
<strong>Explanation: 
</strong>Swap s1[0] and s2[0], s1 = &quot;yy&quot;, s2 = &quot;xx&quot;.
Swap s1[0] and s2[1], s1 = &quot;xy&quot;, s2 = &quot;xy&quot;.
Note that you can&#39;t swap s1[0] and s1[1] to make s1 equal to &quot;yx&quot;, cause we can only swap chars in different strings.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s1 = &quot;xx&quot;, s2 = &quot;xy&quot;
<strong>Output:</strong> -1
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s1 = &quot;xxyyxyxyxx&quot;, s2 = &quot;xyyxyxxxyx&quot;
<strong>Output:</strong> 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s1.length, s2.length &lt;= 1000</code></li>
	<li><code>s1, s2</code>&nbsp;only contain <code>&#39;x&#39;</code> or <code>&#39;y&#39;</code>.</li>
</ul>

</div>

## Tags
- String (string)
- Greedy (greedy)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- JP Morgan Chase - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution with detailed comments
- Author: jeremyasm
- Creation Date: Sun Nov 03 2019 12:02:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 03 2019 14:20:24 GMT+0800 (Singapore Standard Time)

<p>
```java
    public int minimumSwap(String s1, String s2) {
        int x1 = 0; // number of \'x\' in s1 (skip equal chars at same index)
		int y1 = 0; // number of \'y\' in s1 (skip equal chars at same index)
		int x2 = 0; // number of \'x\' in s2 (skip equal chars at same index)
		int y2 = 0; // number of \'y\' in s2 (skip equal chars at same index)

        for(int i = 0; i < s1.length(); i ++){
            char c1 = s1.charAt(i);
            char c2 = s2.charAt(i);
            if(c1 == c2){ // skip chars that are equal at the same index in s1 and s2
                continue;
            }
            if(c1 == \'x\'){
                x1 ++;
            }else{
                y1 ++;
            }
            if(c2 == \'x\'){
                x2 ++;
            }else{
                y2 ++;
            }
        } // end for
        
        // After skip "c1 == c2", check the number of  \'x\' and \'y\' left in s1 and s2.
        if((x1 + x2) % 2 != 0 || (y1 + y2) % 2 != 0){
            return -1; // if number of \'x\' or \'y\' is odd, we can not make s1 equals to s2
        }
        
        int swaps = x1 / 2 + y1 / 2 + (x1 % 2) * 2;
        // Cases to do 1 swap:
        // "xx" => x1 / 2 => how many pairs of \'x\' we have ?
        // "yy" => y1 / 2 => how many pairs of \'y\' we have ?
        // 
        // Cases to do 2 swaps:
        // "xy" or "yx" =>  x1 % 2
                 
        return swaps;        
    } 
```

Example:
Input: 
s1 = "xxyyxyxyxx", 
s2 = "xyyxyxxxyx"

Skip equals chars, then:
s1 = "xyxyyx", 
s2 = "yxyxxy"

x1 = 3, y1 = 3
x2 = 3, y2 = 3

There are 6 \'x\' and 6 \'y\', so it\'s valid to do swaps.

check \'x\' pairs in s1
s1 = "**x**y**x**yyx", 
s2 = "**y**x**y**xxy"

do 1 swap
s1 = "**y**y**x**yyx", 
s2 = "**y**x**x**xxy"

ignore the equal chars
s1 = "yyyx"
s2 = "xxxy"

check \'y\' pairs in s1
s1 = "**yy**yx"
s2 = "**xx**xy"

do 1 swap
s1 = "**yx**yx"
s2 = "**yx**xy"

ignore the equal chars
s1 =  "yx"
s2 = "xy"

do 2 swaps
s1 = "xy"
s2 = "xy"

Here it is.




</p>


### Simply Simple Python Solution - with detailed explanation
- Author: limitless_
- Creation Date: Mon Nov 04 2019 00:31:31 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 05 2019 08:52:15 GMT+0800 (Singapore Standard Time)

<p>
In this problem, we just need to find the count of different characters in both strings. When I use "x_y" i.e. I have x in s1 at index i and y in s2 at same index i. Similary "y_x" means I have y in s1 at index j and x in s2 at same index j. 

#### Example 1:
s1 = "xx"
s2 = "yy"

if we iterate through both string and keep checking every index, we get two indexes with different values in both s1 and s2:
"x_y" at index 0 and "x_y"  at index 1.
if we have 2 "x_y" then we only need 1 swap to make them equal. Swap x at index 0 in s1 with y at index 1 in s2.

#### Example 2:
s1 = "yy"
s2 = "xx"

Here also we have 2 different values: "y_x"  at index 0 and "y_x" at index 1. so it will also take 1 swap to make them equal.

#### Example 3:
s1 = "xy"
s2 = "yx"

Here we have one count of "x_y" at index 0 and one count of "y_x" at index 1. We need 2 swaps to make these indexes equal.
Swap s1[0] and s2[0], s1 = "yy", s2 = "xx".
Swap s1[0] and s2[1], s1 = "xy", s2 = "xy".

#### Example 4:
s1 = "xxyyxyxyxx", 
s2 = "xyyxyxxxyx"

First remove the indexes with same characters:
s1 = "xyxyyx"
s2 = "yxyxxy"

"x_y" count = 3 (index 0, 2, 5)
"y_x" count = 3 (index 1, 3, 4)

index 0 and 2 can be made equal in just 1 swap.  see Example 1.
index 1 and 3 can also be made equal in just 1 swap. see Example 2.

index 5 and 4 can be made Equal in 2 swaps. see Example 3

so we only need 4 swaps.

### Steps:
1. Get the count of "x_y" and "y_x"
2. If sum of both counts is odd then return -1. We need a pair to make the strings equal
3. Each 2 count of "x_y" needs just 1 swap. So add half of "x_y" count to the result
4. Each 2 count of "y_x" needs just 1 swap. So add half of "y_x" count to the result
5. if we still have 1 count of "x_y" and 1 count of "y_x" then they need 2 swaps so add 2 in result.

```
def minimumSwap(self, s1: str, s2: str) -> int:
        x_y, y_x = 0, 0
        for c1, c2 in zip(s1, s2):
            if c1 != c2:
                if c1 == \'x\':
                    x_y += 1
                else:
                    y_x += 1
                    
        if (x_y + y_x) % 2 == 1:
            return -1
        # Both x_y and y_x count shall either be even or odd to get the result.
		# x_y + y_x should be even
        
        res = x_y // 2
        res += y_x // 2
        
        if x_y % 2 == 1:
            res += 2
        # If there count is odd i.e. we have "xy" and "yx" situation
        # so we need 2 more swaps to make them equal
            
        return res
```

</p>


### Why is this question tagged EASY ??
- Author: Coder-Boy
- Creation Date: Sun Nov 03 2019 13:55:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 03 2019 13:55:19 GMT+0800 (Singapore Standard Time)

<p>
I was able to solve the medium questions of the contest but I was not able to solve this. I wonder why this is tagged as an EASY question !
</p>


