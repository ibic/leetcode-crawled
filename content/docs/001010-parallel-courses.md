---
title: "Parallel Courses"
weight: 1010
#id: "parallel-courses"
---
## Description
<div class="description">
<p>There are <code>N</code> courses, labelled from 1 to <code>N</code>.</p>

<p>We are given&nbsp;<code>relations[i] = [X, Y]</code>, representing a prerequisite relationship between course <code>X</code> and course <code>Y</code>:&nbsp;course <code>X</code>&nbsp;has to be studied before course <code>Y</code>.</p>

<p>In one semester you can study any number of courses as long as you have studied all the prerequisites for the course you are studying.</p>

<p>Return the minimum number of semesters needed to study all courses.&nbsp; If there is no way to study all the courses, return <code>-1</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/04/20/1316_ex1.png" style="width: 126px; height: 101px;" /></strong></p>

<pre>
<strong>Input: </strong>N = <span id="example-input-1-1">3</span>, relations = <span id="example-input-1-2">[[1,3],[2,3]]</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>
In the first semester, courses 1 and 2 are studied. In the second semester, course 3 is studied.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/04/20/1316_ex2.png" style="width: 126px; height: 101px;" /></strong></p>

<pre>
<strong>Input: </strong>N = <span id="example-input-2-1">3</span>, relations = <span id="example-input-2-2">[[1,2],[2,3],[3,1]]</span>
<strong>Output: </strong><span id="example-output-2">-1</span>
<strong>Explanation: </strong>
No course can be studied because they depend on each other.
</pre>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ol>
	<li><code>1 &lt;= N &lt;= 5000</code></li>
	<li><code>1 &lt;= relations.length &lt;= 5000</code></li>
	<li><code>relations[i][0] != relations[i][1]</code></li>
	<li>There are no repeated relations in the input.</li>
</ol>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Depth-first Search (depth-first-search)
- Graph (graph)

## Companies
- Twitch - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: true)
- Uber - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### google follow up questions
- Author: timmy2
- Creation Date: Wed Aug 21 2019 02:51:16 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 21 2019 02:51:16 GMT+0800 (Singapore Standard Time)

<p>
If only pick at most k courses in one semester, the shortest time to finish all courses
</p>


### [Java] Topological Sort + BFS w/ comment and analysis.
- Author: rock
- Creation Date: Sun Jul 28 2019 00:02:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 28 2019 00:47:22 GMT+0800 (Singapore Standard Time)

<p>
```
    public int minimumSemesters(int N, int[][] relations) {
        Map<Integer, List<Integer>> g = new HashMap<>(); // key: prerequisite, value: course list. 
        int[] inDegree = new int[N + 1]; // inDegree[i]: number of prerequisites for i.
        for (int[] r : relations) {
            g.computeIfAbsent(r[0], l -> new ArrayList<>()).add(r[1]); // construct graph.
            ++inDegree[r[1]]; // count prerequisites for r[1].
        }
        Queue<Integer> q = new LinkedList<>(); // save current 0 in-degree vertices.
        for (int i = 1; i <= N; ++i)
            if (inDegree[i] == 0)
                q.offer(i);
        int semester = 0;
        while (!q.isEmpty()) { // BFS traverse all currently 0 in degree vertices.
            for (int sz = q.size(); sz > 0; --sz) { // sz is the search breadth.
                int c = q.poll();
                --N;
                if (!g.containsKey(c)) continue; // c\'s in-degree is currently 0, but it has no prerequisite.
                for (int course : g.remove(c))
                    if (--inDegree[course] == 0) // decrease the in-degree of course\'s neighbors.
                        q.offer(course); // add current 0 in-degree vertex into Queue.
            }
            ++semester; // need one more semester.
        }
        return N == 0 ? semester : -1;
    }
```
**Analysis:**
Time & space: O(V + E).
</p>


### C++ dfs 15 line  and bfs 15 line
- Author: Sanzenin_Aria
- Creation Date: Sun Jul 28 2019 05:43:07 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 01 2019 03:18:17 GMT+0800 (Singapore Standard Time)

<p>
DFS method
```
    int minimumSemesters(int N, vector<vector<int>>& relations)
    {
        vector<int> vDepth(N, 1), visitState(N, 0); // visite state  -1 :visiting;  0:not visit;  1:visited
        vector<vector<int>> g(N);
        for (auto& v : relations) g[v[0] - 1].push_back(v[1] - 1);
        for (int i = 0; i < N; i++) if (!dfs(i, g, vDepth, visitState)) return -1;
        return *max_element(vDepth.begin(), vDepth.end());
    }

    // return false if there is a circle
    bool dfs(int i, const vector<vector<int>> & g, vector<int> & vDepth, vector<int> & visitState) {
        if (visitState[i] == 1) return true;
        if (visitState[i] == -1) return false; // circle  
        visitState[i] = -1; //visiting
        for (auto j : g[i]) {
            if (!dfs(j, g, vDepth, visitState)) return false;
            vDepth[i] = max(vDepth[i], 1 + vDepth[j]);
        }
        visitState[i] = 1; //visited;
        return true;
    }
```


BFS method
```
	int minimumSemesters(int N, vector<vector<int>>& relations) {
        vector<int> indegree(N, 0);
        vector<set<int>> g(N);
        for (auto& v : relations) {
            g[v[0] - 1].insert(v[1] - 1);
            indegree[v[1] - 1]++;
        }
        int maxDepth = 0, numStudied = 0;
        queue<pair<int, int>> q; // vertex, depth
        for (int i = 0; i < N; i++) if (indegree[i] == 0) q.push({ i,1 });
        for (; !q.empty(); numStudied++) {
            auto [i, depth] = q.front(); q.pop();
            maxDepth = depth;
            for (auto j : g[i]) if (--indegree[j] == 0) q.push({ j, depth + 1 });
        }
        return numStudied == N ? maxDepth : -1;
    }
</p>


