---
title: "Merge Sorted Array"
weight: 88
#id: "merge-sorted-array"
---
## Description
<div class="description">
<p>Given two sorted integer arrays <em>nums1</em> and <em>nums2</em>, merge <em>nums2</em> into <em>nums1</em> as one sorted array.</p>

<p><strong>Note:</strong></p>

<ul>
	<li>The number of elements initialized in <em>nums1</em> and <em>nums2</em> are <em>m</em> and <em>n</em> respectively.</li>
	<li>You may assume that <em>nums1</em> has enough space (size that is&nbsp;<strong>equal</strong> to <em>m</em> + <em>n</em>) to hold additional elements from <em>nums2</em>.</li>
</ul>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong>
nums1 = [1,2,3,0,0,0], m = 3
nums2 = [2,5,6],       n = 3

<strong>Output:</strong>&nbsp;[1,2,2,3,5,6]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>-10^9 &lt;= nums1[i], nums2[i] &lt;= 10^9</code></li>
	<li><code>nums1.length == m + n</code></li>
	<li><code>nums2.length == n</code></li>
</ul>

</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)

## Companies
- Facebook - 50 (taggedByAdmin: true)
- Amazon - 11 (taggedByAdmin: false)
- Apple - 10 (taggedByAdmin: false)
- Microsoft - 8 (taggedByAdmin: true)
- Bloomberg - 4 (taggedByAdmin: true)
- IBM - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- LinkedIn - 6 (taggedByAdmin: false)
- Oracle - 4 (taggedByAdmin: false)
- Yandex - 3 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Cisco - 4 (taggedByAdmin: false)
- Expedia - 3 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Quip (Salesforce) - 2 (taggedByAdmin: false)
- Tableau - 2 (taggedByAdmin: false)
- Netflix - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1 : Merge and sort

**Intuition**

The naive approach would be to merge both lists into one and then to sort.
It's a one line solution (2 lines in Java) with a pretty bad time complexity 
$$\mathcal{O}((n + m)\log(n + m))$$ because here one doesn't
profit from the fact that both arrays are already sorted.

**Implementation**

<iframe src="https://leetcode.com/playground/nhucMvY4/shared" frameBorder="0" width="100%" height="225" name="nhucMvY4"></iframe>

* Time complexity : $$\mathcal{O}((n + m)\log(n + m))$$. 
* Space complexity : $$\mathcal{O}(1)$$.
<br />
<br />

---
#### Approach 2 : Two pointers / Start from the beginning

**Intuition**

Typically, one could achieve $$\mathcal{O}(n + m)$$ time complexity
in a sorted array(s) with the help of _two pointers approach_.

The straightforward implementation would be to set get pointer
`p1` in the beginning of `nums1`, `p2` in the beginning of `nums2`,
and push the smallest value in the output array at each step.

Since `nums1` is an array used for output, one has to keep 
first `m` elements of `nums1` somewhere aside, 
that means $$\mathcal{O}(m)$$ space complexity
for this approach. 

![compute](../Figures/88/88_beginning.png)

**Implementation**

<iframe src="https://leetcode.com/playground/Rg6KMALa/shared" frameBorder="0" width="100%" height="500" name="Rg6KMALa"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n + m)$$. 
* Space complexity : $$\mathcal{O}(m)$$.
<br />
<br />

---
#### Approach 3 : Two pointers / Start from the end

**Intuition**

Approach 2 already demonstrates the best possible time 
complexity $$\mathcal{O}(n + m)$$ but still uses an additional space.
This is because one has to keep somewhere the elements of array `nums1`
while overwriting it starting from the beginning.

> What if we start to overwrite `nums1` from the end, where 
there is no information yet? Then no additional space is needed.

The set pointer `p` here 
is used to track the position of an added element.   

![compute](../Figures/88/88_end.png)

**Implementation**

!?!../Documents/88_LIS.json:1000,470!?!

<iframe src="https://leetcode.com/playground/sQtdCZ72/shared" frameBorder="0" width="100%" height="500" name="sQtdCZ72"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n + m)$$.
* Space complexity : $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### This is my AC code, may help you
- Author: leetchunhui
- Creation Date: Mon Jul 28 2014 22:56:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 14:07:17 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        void merge(int A[], int m, int B[], int n) {
            int i=m-1;
    		int j=n-1;
    		int k = m+n-1;
    		while(i >=0 && j>=0)
    		{
    			if(A[i] > B[j])
    				A[k--] = A[i--];
    			else
    				A[k--] = B[j--];
    		}
    		while(j>=0)
    			A[k--] = B[j--];
        }
    };
</p>


### Beautiful Python Solution
- Author: cffls
- Creation Date: Thu Jul 23 2015 09:54:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 10:07:29 GMT+0800 (Singapore Standard Time)

<p>
    def merge(self, nums1, m, nums2, n):
            while m > 0 and n > 0:
                if nums1[m-1] >= nums2[n-1]:
                    nums1[m+n-1] = nums1[m-1]
                    m -= 1
                else:
                    nums1[m+n-1] = nums2[n-1]
                    n -= 1
            if n > 0:
                nums1[:n] = nums2[:n]
</p>


### 4ms C++ solution with single loop
- Author: deck
- Creation Date: Wed Jun 24 2015 14:58:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 23:18:14 GMT+0800 (Singapore Standard Time)

<p>
This code relies on the simple observation that once all of the numbers from `nums2` have been merged into `nums1`, the rest of the numbers in `nums1` that were not moved are already in the correct place.

    class Solution {
    public:
        void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
            int i = m - 1, j = n - 1, tar = m + n - 1;
            while (j >= 0) {
                nums1[tar--] = i >= 0 && nums1[i] > nums2[j] ? nums1[i--] : nums2[j--];
            }
        }
    };
</p>


