---
title: "All Nodes Distance K in Binary Tree"
weight: 807
#id: "all-nodes-distance-k-in-binary-tree"
---
## Description
<div class="description">
<p>We are given a binary tree (with root node&nbsp;<code>root</code>), a <code>target</code> node, and an integer value <code>K</code>.</p>

<p>Return a list of the values of all&nbsp;nodes that have a distance <code>K</code> from the <code>target</code> node.&nbsp; The answer can be returned in any order.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>root = <span id="example-input-1-1">[3,5,1,6,2,0,8,null,null,7,4]</span>, target = <span id="example-input-1-2">5</span>, K = <span id="example-input-1-3">2</span>

<strong>Output: </strong><span id="example-output-1">[7,4,1]</span>

<strong>Explanation: </strong>
The nodes that are a distance 2 from the target node (with value 5)
have values 7, 4, and 1.

<img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/06/28/sketch0.png" style="width: 280px; height: 240px;" />

Note that the inputs &quot;root&quot; and &quot;target&quot; are actually TreeNodes.
The descriptions of the inputs above are just serializations of these objects.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>The given tree is non-empty.</li>
	<li>Each node in the tree has unique values&nbsp;<code>0 &lt;= node.val &lt;= 500</code>.</li>
	<li>The <code>target</code>&nbsp;node is a node in the tree.</li>
	<li><code>0 &lt;= K &lt;= 1000</code>.</li>
</ol>
</div>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 17 (taggedByAdmin: true)
- Facebook - 8 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Uber - 5 (taggedByAdmin: false)
- DiDi - 2 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Hulu - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Annotate Parent

**Intuition**

If we know the parent of every node `x`, we know all nodes that are distance `1` from `x`.  We can then perform a breadth first search from the `target` node to find the answer.

**Algorithm**

We first do a depth first search where we annotate every node with information about it's parent.

After, we do a breadth first search to find all nodes a distance `K` from the `target`.

<iframe src="https://leetcode.com/playground/AyanwAkX/shared" frameBorder="0" width="100%" height="500" name="AyanwAkX"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the given tree.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Percolate Distance

**Intuition**

From `root`, say the `target` node is at depth `3` in the left branch.  It means that any nodes that are distance `K - 3` in the right branch should be added to the answer.

**Algorithm**

Traverse every `node` with a depth first search `dfs`.  We'll add all nodes `x` to the answer such that `node` is the node on the path from `x` to `target` that is closest to the `root`.

To help us, `dfs(node)` will return the distance from `node` to the `target`.  Then, there are 4 cases:

* If `node == target`, then we should add nodes that are distance `K` in the subtree rooted at `target`.

* If `target` is in the left branch of `node`, say at distance `L+1`, then we should look for nodes that are distance `K - L - 1` in the right branch.

* If `target` is in the right branch of `node`, the algorithm proceeds similarly.

* If `target` isn't in either branch of `node`, then we stop.

In the above algorithm, we make use of the auxillary function `subtree_add(node, dist)` which adds the nodes in the subtree rooted at `node` that are distance `K - dist` from the given `node`.

<iframe src="https://leetcode.com/playground/2KNRHywH/shared" frameBorder="0" width="100%" height="500" name="2KNRHywH"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the given tree.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 1ms beat 100% simple Java dfs with(without) hashmap, including explanation
- Author: BingleLove
- Creation Date: Sun Jul 01 2018 14:25:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 05 2019 05:24:50 GMT+0800 (Singapore Standard Time)

<p>
As we know, if the distance from a node to target node is `k`, the distance from its child to the target node is `k + 1` unless the child node is closer to the target node which means the target node is in it\'s subtree.

To avoid this situation, we need to travel the tree first to find the path from `root` to `target`, to:
* store the value of distance in hashamp from the `all nodes in that path` to `target`

Then we can easily use dfs to travel the whole tree. Every time when we meet a treenode which has already stored in map, use the stored value in hashmap **instead of** the value from its parent node.


```Java
class Solution {
    
    Map<TreeNode, Integer> map = new HashMap<>();
        
    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        List<Integer> res = new LinkedList<>();
        find(root, target);
        dfs(root, target, K, map.get(root), res);
        return res;
    }
    
    // find target node first and store the distance in that path that we could use it later directly
    private int find(TreeNode root, TreeNode target) {
        if (root == null) return -1;
        if (root == target) {
            map.put(root, 0);
            return 0;
        }
        int left = find(root.left, target);
        if (left >= 0) {
            map.put(root, left + 1);
            return left + 1;
        }
		int right = find(root.right, target);
		if (right >= 0) {
            map.put(root, right + 1);
            return right + 1;
        }
        return -1;
    }
    
    private void dfs(TreeNode root, TreeNode target, int K, int length, List<Integer> res) {
        if (root == null) return;
        if (map.containsKey(root)) length = map.get(root);
        if (length == K) res.add(root.val);
        dfs(root.left, target, K, length + 1, res);
        dfs(root.right, target, K, length + 1, res);
    }
}
```

--------------------------------------------------------------------------
**version** without using hashmap: same idea, combine two recursive function

```Java
class Solution {
            
    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        List<Integer> res = new LinkedList<>();
        if (K == 0) {
            res.add(target.val);
        } else {
            dfs(res, root, target.val, K ,0);
        }
        return res;
    }
    
    private int dfs(List<Integer> res, TreeNode node, int target, int K, int depth) {
        if (node == null) return 0;
        
        if (depth == K) {
            res.add(node.val);
            return 0;
        }
        
        int left, right;
        if (node.val == target || depth > 0) {
            left = dfs(res, node.left, target, K, depth + 1);
            right = dfs(res, node.right, target, K, depth + 1);
        } else {
            left = dfs(res, node.left, target, K, depth);
            right = dfs(res, node.right, target, K, depth);
        }
        
        if (node.val == target) return 1;
        
        if (left == K || right == K) {
            res.add(node.val);
            return 0;
        }
        
        if (left > 0) {
            dfs(res, node.right, target, K, left + 1);
            return left + 1;
        }
        
        if (right > 0) {
            dfs(res, node.left, target, K, right + 1);
            return right + 1;
        }
        
        return 0;
    }
}
```
</p>


### Python DFS and BFS
- Author: lee215
- Creation Date: Sun Jul 01 2018 11:06:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Apr 18 2020 12:05:58 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
A recursive dfs funciton `connect` help to build up a map `conn`.
The key of map is node\'s val and the value of map is node\'s connected nodes\' vals.
Then we do N times bfs search loop to find all nodes of distance `K`
<br>

**Python**
commented by @0alexzhong0
```py
def distanceK(self, root, target, K):
    conn = collections.defaultdict(list)
    def connect(parent, child):
        # both parent and child are not empty
        if parent and child:
            # building an undirected graph representation, assign the
            # child value for the parent as the key and vice versa
            conn[parent.val].append(child.val)
            conn[child.val].append(parent.val)
        # in-order traversal
        if child.left: connect(child, child.left)
        if child.right: connect(child, child.right)
    # the initial parent node of the root is None
    connect(None, root)
    # start the breadth-first search from the target, hence the starting level is 0
    bfs = [target.val]
    seen = set(bfs)
    # all nodes at (k-1)th level must also be K steps away from the target node
    for i in range(K):
        # expand the list comprehension to strip away the complexity
        new_level = []
        for q_node_val in bfs:
            for connected_node_val in conn[q_node_val]:
                if connected_node_val not in seen:
                    new_level.append(connected_node_val)
        bfs = new_level
        # add all the values in bfs into seen
        seen |= set(bfs)
    return bfs
```
</p>


### JAVA Graph + BFS
- Author: Self_Learner
- Creation Date: Sun Jul 01 2018 11:36:50 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 18:20:26 GMT+0800 (Singapore Standard Time)

<p>
```
//Method 1: use HashMap
//1. build a undirected graph using treenodes as vertices, and the parent-child relation as edges
//2. do BFS with source vertice (target) to find all vertices with distance K to it.
class Solution {
    Map<TreeNode, List<TreeNode>> map = new HashMap();
//here can also use Map<TreeNode, TreeNode> to only store the child - parent mapping, since parent-child mapping is inherent in the tree structure
    
    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
         List<Integer> res = new ArrayList<Integer> ();
        if (root == null || K < 0) return res;
        buildMap(root, null); 
        if (!map.containsKey(target)) return res;
        Set<TreeNode> visited = new HashSet<TreeNode>();
        Queue<TreeNode> q = new LinkedList<TreeNode>();
        q.add(target);
        visited.add(target);
        while (!q.isEmpty()) {
            int size = q.size();
            if (K == 0) {
                for (int i = 0; i < size ; i++) res.add(q.poll().val);
                return res;
            }
            for (int i = 0; i < size; i++) {
                TreeNode node = q.poll();
                for (TreeNode next : map.get(node)) {
                    if (visited.contains(next)) continue;
                    visited.add(next);
                    q.add(next);
                }
            }
            K--;
        }
        return res;
    }
    
    private void buildMap(TreeNode node, TreeNode parent) {
        if (node == null) return;
        if (!map.containsKey(node)) {
            map.put(node, new ArrayList<TreeNode>());
            if (parent != null)  { map.get(node).add(parent); map.get(parent).add(node) ; }
            buildMap(node.left, node);
            buildMap(node.right, node);
        }
    }    
}

//Method 2: No HashMap
//kind of like clone the tree, in the meanwhile add a parent link to the node
class Solution {
    private GNode targetGNode;
    
    private class GNode {
        TreeNode node;
        GNode parent, left, right;
        GNode (TreeNode node) {
            this.node = node;
        }
    }           
    
    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        List<Integer> res = new ArrayList<Integer> ();
        if (root == null || K < 0) return res;
        cloneGraph(root, null, target);
        if (targetGNode == null) return res;
        Set<GNode> visited = new HashSet<GNode>();
        Queue<GNode> q = new LinkedList<GNode>();
        q.add(targetGNode);
        visited.add(targetGNode);
        while (!q.isEmpty()) {
            int size = q.size();
            if (K == 0) {
                for (int i = 0; i < size ; i++) res.add(q.poll().node.val);
                return res;
            }
            for (int i = 0; i < size; i++) {
                GNode gNode = q.poll();
                if (gNode.left != null && !visited.contains(gNode.left)) { visited.add(gNode.left); q.add(gNode.left); }
                if (gNode.right != null && !visited.contains(gNode.right)) { visited.add(gNode.right); q.add(gNode.right); }
                if (gNode.parent != null && !visited.contains(gNode.parent)) { visited.add(gNode.parent); q.add(gNode.parent); }
            }
            K--;
        }
        return res;
    }
    
    private GNode cloneGraph(TreeNode node, GNode parent, TreeNode target) {
        if (node == null) return null;
        GNode gNode = new GNode(node);
        if (node == target) targetGNode = gNode;
        gNode.parent = parent;
        gNode.left = cloneGraph(node.left, gNode, target);
        gNode.right = cloneGraph(node.right, gNode, target);
        return gNode;
    }
}

</p>


