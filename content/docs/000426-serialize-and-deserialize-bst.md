---
title: "Serialize and Deserialize BST"
weight: 426
#id: "serialize-and-deserialize-bst"
---
## Description
<div class="description">
<p>Serialization is converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, or transmitted across a network connection link to be reconstructed later in the same or another computer environment.</p>

<p>Design an algorithm to serialize and deserialize a <b>binary search tree</b>. There is no restriction on how your serialization/deserialization algorithm should work. You need to ensure that a binary search tree can be serialized to a string, and this string can be deserialized to the original tree structure.</p>

<p><b>The encoded string should be as compact as possible.</b></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> root = [2,1,3]
<strong>Output:</strong> [2,1,3]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> root = []
<strong>Output:</strong> []
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is in the range <code>[0, 10<sup>4</sup>]</code>.</li>
	<li><code>0 &lt;= Node.val &lt;= 10<sup>4</sup></code></li>
	<li>The input tree is <strong>guaranteed</strong> to be a binary search tree.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 6 (taggedByAdmin: true)
- ByteDance - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- LinkedIn - 4 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Qualtrics - 3 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### How to make the encoded string as compact as possible

This question is similar to the [Google interview question discussed last week](https://leetcode.com/discuss/interview-experience/297576/google-onsite-interview-sde1-new-grad-mountain-view-ca).

[To serialize](https://en.wikipedia.org/wiki/Serialization) 
a binary tree means to 

- Encode tree structure. 

- Encode node values. 

- Choose delimiters to separate the values in the encoded string.

![bla](../Figures/449/tree_struct.png)

Hence there are three axes of optimisation here.
<br /> 
<br />


---
#### Approach 1: Postorder traversal to optimise space for the tree structure.

**Intuition**

Let's use here the fact that BST could be constructed from
preorder or postorder traversal only. 
Please [check this article](https://leetcode.com/problems/construct-binary-search-tree-from-preorder-traversal/solution/)
for the detailed discussion.
In brief, it's a consequence of two facts:

- [Binary tree could be constructed from preorder/postorder and inorder traversal](https://leetcode.com/articles/construct-binary-tree-from-postorder-and-inorder-t/).

- [Inorder traversal of BST is an array sorted in the ascending order: 
`inorder = sorted(preorder)`](https://leetcode.com/articles/delete-node-in-a-bst/).

That means that BST structure is already encoded in the preorder or
postorder traversal and hence they are both suitable for the 
compact serialization. 

Serialization could be easily implemented with both strategies,
but for optimal deserialization better to choose the postorder traversal because member/global/static variables
are not allowed here. 

![pic](../Figures/449/approach1.png)

**Implementation**

<iframe src="https://leetcode.com/playground/BZzcMign/shared" frameBorder="0" width="100%" height="500" name="BZzcMign"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ both for serialization
and deserialization. Let's compute the solution with the help of 
[master theorem](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)) 
$$T(N) = aT\left(\frac{b}{N}\right) + \Theta(N^d)$$.
The equation represents dividing the problem 
up into $$a$$ subproblems of size $$\frac{N}{b}$$ in $$\Theta(N^d)$$ time. 
Here one divides the problem in two subproblemes `a = 2`, the size of each subproblem 
(to compute left and right subtree) is a half of initial problem `b = 2`, 
and all this happens in a constant time `d = 0`.
That means that $$\log_b(a) > d$$ and hence we're dealing with 
[case 1](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)#Case_1_example)
that means $$\mathcal{O}(N^{\log_b(a)}) = \mathcal{O}(N)$$ time complexity.

* Space complexity : $$\mathcal{O}(N)$$, since we store the entire tree.
Encoded string: one needs to store
$$(N - 1)$$ delimiters, and $$N$$ node values in the encoded string. 
Tree structure is encoded in the order of values and uses no space.
<br /> 
<br />


---
#### Approach 2: Convert int to 4-bytes string to optimise space for node values.

**Intuition**

Approach 1 works fine with the small node values but starts to
consume more and more space in the case of large ones. 

For example, the tree `[2,null,3,null,4]` is encoded as a string
`"4 3 2"` which uses `5` bytes to store the values and delimiters, `1` byte per
value or delimiter. So far everything is fine. 

Let's consider now the tree `[12345,null,12346,null,12347]` which is
encoded as `"12347 12346 12345"` and consumes `17` bytes to store 
3 integers and 2 delimiters, `15` bytes for node values only.
At the same time it's known that `4` bytes is enough to store an int value,
_i.e._ `12` bytes should be enough for 3 integers. 
`15 > 12` and hence the storage of values could be optimised.

> How to do it? Convert each integer into 4-bytes string.

![pic2](../Figures/449/four_bytes.png)

**Implementation**

<iframe src="https://leetcode.com/playground/n5xXhukb/shared" frameBorder="0" width="100%" height="500" name="n5xXhukb"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ both for serialization
and deserialization. 

* Space complexity : $$\mathcal{O}(N)$$, since we store the entire tree.
Encoded string: one needs $$2(N - 1)$$ bytes for the delimiters, 
and $$4 N$$ bytes for the node values in the encoded string. 
Tree structure is encoded in the order of node values and uses no space. 
<br /> 
<br />


---
#### Approach 3: Get rid of delimiters.

**Intuition**

Approach 2 works well except for delimiter usage.

Since all node values are now encoded as 4-bytes strings,
one could just split the encoded string into 4-bytes chunks,
convert each chunk back to the integer and proceed further. 

![pic3](../Figures/449/no_delimiters.png)

**Implementation**

<iframe src="https://leetcode.com/playground/Lg9f24zm/shared" frameBorder="0" width="100%" height="500" name="Lg9f24zm"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ both for serialization
and deserialization. 

* Space complexity : $$\mathcal{O}(N)$$, since we store the entire tree.
Encoded string: no delimiters, no additional space for the tree structure,
just $$4 N$$ bytes for the node values in the encoded string.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### the General Solution for Serialize and Deserialize BST and Serialize and Deserialize BT
- Author: GreatLim
- Creation Date: Fri Oct 05 2018 04:28:05 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 15:25:29 GMT+0800 (Singapore Standard Time)

<p>
### BST
use  upper and lower boundaries to check whether we should add `null`
```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuilder sb = new StringBuilder();
        serialize(root, sb);
        return sb.toString();
    }
    
    public void serialize(TreeNode root, StringBuilder sb) {
        if (root == null) return;
        sb.append(root.val).append(",");
        serialize(root.left, sb);
        serialize(root.right, sb);
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (data.isEmpty()) return null;
        Queue<String> q = new LinkedList<>(Arrays.asList(data.split(",")));
        return deserialize(q, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }
    
    public TreeNode deserialize(Queue<String> q, int lower, int upper) {
        if (q.isEmpty()) return null;
        String s = q.peek();
        int val = Integer.parseInt(s);
        if (val < lower || val > upper) return null;
        q.poll();
        TreeNode root = new TreeNode(val);
        root.left = deserialize(q, lower, val);
        root.right = deserialize(q, val, upper);
        return root;
    }
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.deserialize(codec.serialize(root));
```

### Binary Tree
use `#` whether we should add `null`
```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuilder sb = new StringBuilder();
        serialize(root, sb);
        return sb.toString();
    }
    
    public void serialize(TreeNode root, StringBuilder sb) {
        if (root == null) {
            sb.append("#").append(",");
        } else {
            sb.append(root.val).append(",");
            serialize(root.left, sb);
            serialize(root.right, sb);
        }
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        Queue<String> q = new LinkedList<>(Arrays.asList(data.split(",")));
        return deserialize(q);
    }
    
    public TreeNode deserialize(Queue<String> q) {
        String s = q.poll();
        if (s.equals("#")) return null;
        TreeNode root = new TreeNode(Integer.parseInt(s));
        root.left = deserialize(q);
        root.right = deserialize(q);
        return root;
    }
    
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.deserialize(codec.serialize(root));
```


</p>


### Java PreOrder + Queue solution
- Author: magicshine
- Creation Date: Mon Nov 07 2016 06:39:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 09:01:25 GMT+0800 (Singapore Standard Time)

<p>
Hi all, I think my solution is pretty straightforward and easy to understand, not that efficient though. And the serialized tree is compact.
Pre order traversal of BST will output root node first, then left children, then right.
```
root left1 left2 leftX right1 rightX
```
If we look at the value of the pre-order traversal we get this:
```
rootValue (<rootValue) (<rootValue) (<rootValue) |separate line| (>rootValue) (>rootValue)
```
Because of BST's property: before the |separate line| all the node values are **less than root value**, all the node values after |separate line| are **greater than root value**. We will utilize this to build left and right tree.

Pre-order traversal is BST's serialized string. I am doing it iteratively.
To deserialized it, use a queue to recursively get root node, left subtree and right subtree.


I think time complexity is O(NlogN).
Errr, my bad, as @ray050899 put below, worst case complexity should be O(N^2), when the tree is really unbalanced. 


My implementation
```
public class Codec {
    private static final String SEP = ",";
    private static final String NULL = "null";
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuilder sb = new StringBuilder();
        if (root == null) return NULL;
        //traverse it recursively if you want to, I am doing it iteratively here
        Stack<TreeNode> st = new Stack<>();
        st.push(root);
        while (!st.empty()) {
            root = st.pop();
            sb.append(root.val).append(SEP);
            if (root.right != null) st.push(root.right);
            if (root.left != null) st.push(root.left);
        }
        return sb.toString();
    }

    // Decodes your encoded data to tree.
    // pre-order traversal
    public TreeNode deserialize(String data) {
        if (data.equals(NULL)) return null;
        String[] strs = data.split(SEP);
        Queue<Integer> q = new LinkedList<>();
        for (String e : strs) {
            q.offer(Integer.parseInt(e));
        }
        return getNode(q);
    }
    
    // some notes:
    //   5
    //  3 6
    // 2   7
    private TreeNode getNode(Queue<Integer> q) { //q: 5,3,2,6,7
        if (q.isEmpty()) return null;
        TreeNode root = new TreeNode(q.poll());//root (5)
        Queue<Integer> samllerQueue = new LinkedList<>();
        while (!q.isEmpty() && q.peek() < root.val) {
            samllerQueue.offer(q.poll());
        }
        //smallerQueue : 3,2   storing elements smaller than 5 (root)
        root.left = getNode(samllerQueue);
        //q: 6,7   storing elements bigger than 5 (root)
        root.right = getNode(q);
        return root;
    }
}
```
</p>


### Concise C++ 19ms solution beating 99.4%
- Author: code9yitati
- Creation Date: Sun Feb 19 2017 15:06:38 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 12:46:45 GMT+0800 (Singapore Standard Time)

<p>
Sharing my solution which doesn't have to parse string for comma at all!

The encoding schema is preorder of BST, and to decode this we can use the same preorder traversal to do it in one pass with recursion in O(n) time.

To minimize the memory, I used binary format instead of ascii format for each integer, just burn those int into 4 chars will save you a lot!!!

Really if using ASCII numbers you are paying a lot of penalty memory for integers over 4 digit long and parsing comma is just as painful.

```
class Codec {
public:

    // Encodes a tree to a single string.
    string serialize(TreeNode* root) {
        string order;
        inorderDFS(root, order);
        return order;
    }
    
    inline void inorderDFS(TreeNode* root, string& order) {
        if (!root) return;
        char buf[4];
        memcpy(buf, &(root->val), sizeof(int)); //burn the int into 4 chars
        for (int i=0; i<4; i++) order.push_back(buf[i]);
        inorderDFS(root->left, order);
        inorderDFS(root->right, order);
    }

    // Decodes your encoded data to tree.
    TreeNode* deserialize(string data) {
        int pos = 0;
        return reconstruct(data, pos, INT_MIN, INT_MAX);
    }
    
    inline TreeNode* reconstruct(const string& buffer, int& pos, int minValue, int maxValue) {
        if (pos >= buffer.size()) return NULL; //using pos to check whether buffer ends is better than using char* directly.
        
        int value;
        memcpy(&value, &buffer[pos], sizeof(int));
        if (value < minValue || value > maxValue) return NULL;
        
        TreeNode* node = new TreeNode(value);
        pos += sizeof(int);
        node->left = reconstruct(buffer, pos, minValue, value);
        node->right = reconstruct(buffer, pos, value, maxValue);
        return node;
    }
};
```
</p>


