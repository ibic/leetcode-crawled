---
title: "Implement Stack using Queues"
weight: 209
#id: "implement-stack-using-queues"
---
## Description
<div class="description">
<p>Implement a last in first out (LIFO) stack using only two queues. The implemented stack should support all the functions of a normal queue (<code>push</code>, <code>top</code>, <code>pop</code>, and <code>empty</code>).</p>

<p>Implement the <code>MyStack</code> class:</p>

<ul>
	<li><code>void push(int x)</code> Pushes element x to the top of the stack.</li>
	<li><code>int pop()</code> Removes the element on the top of the stack and returns it.</li>
	<li><code>int top()</code> Returns the element on the top of the stack.</li>
	<li><code>boolean empty()</code> Returns <code>true</code> if the stack is empty, <code>false</code> otherwise.</li>
</ul>

<p><b>Notes:</b></p>

<ul>
	<li>You must use <strong>only</strong> standard operations of a queue, which means only <code>push to back</code>, <code>peek/pop from front</code>, <code>size</code>, and <code>is empty</code> operations are valid.</li>
	<li>Depending on your language, the queue may not be supported natively. You may simulate a queue using a list or deque (double-ended queue), as long as you use only a queue&#39;s standard operations.</li>
</ul>

<p><strong>Follow-up:</strong> Can you implement the stack such that each operation is <strong><a href="https://en.wikipedia.org/wiki/Amortized_analysis" target="_blank">amortized</a></strong> <code>O(1)</code> time complexity? In other words, performing <code>n</code> operations will take overall <code>O(n)</code> time even if one of those operations may take longer.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;MyStack&quot;, &quot;push&quot;, &quot;push&quot;, &quot;top&quot;, &quot;pop&quot;, &quot;empty&quot;]
[[], [1], [2], [], [], []]
<strong>Output</strong>
[null, null, null, 2, 2, false]

<strong>Explanation</strong>
MyStack myStack = new MyStack();
myStack.push(1);
myStack.push(2);
myStack.top(); // return 2
myStack.pop(); // return 2
myStack.empty(); // return False
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= x &lt;= 9</code></li>
	<li>At most <code>100</code>&nbsp;calls will be made to <code>push</code>, <code>pop</code>, <code>top</code>, and <code>empty</code>.</li>
	<li>All the calls to <code>pop</code> and <code>top</code> are valid.</li>
</ul>

</div>

## Tags
- Stack (stack)
- Design (design)

## Companies
- Microsoft - 7 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)
- Twilio - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Bloomberg - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Summary
This article is for beginners. It introduces the following ideas:
Stack, Queue.

## Solution
---
#### Approach #1 (Two Queues, push - $$O(1)$$, pop $$O(n)$$ )

**Intuition**

Stack is **LIFO** (last in - first out) data structure, in which elements are added and removed from the same end, called `top`.
In general stack is implemented using array or linked list, but in the current article we will review a different approach for implementing stack using queues. In contrast queue is **FIFO** (first in - first out) data structure, in which elements are added only from the one side - `rear` and removed from the other - `front`. In order to implement stack using queues, we need to maintain two queues `q1` and `q2`. Also we will keep top stack element in a constant memory.

**Algorithm**

**Push**

The new element is always added to the rear of queue `q1` and it is kept as `top` stack element

![Push an element in stack](https://leetcode.com/media/original_images/225_stack_using_queues_pushA.png){:width="539px"}
{:align="center"}

*Figure 1. Push an element in stack*
{:align="center"}

**Java**

```java
private Queue<Integer> q1 = new LinkedList<>();
private Queue<Integer> q2 = new LinkedList<>();
private int top;

// Push element x onto stack.
public void push(int x) {
    q1.add(x);
    top = x;
}
```
**Complexity Analysis**

* Time complexity : $$O(1)$$. Queue is implemented as linked list and `add` operation has $$O(1)$$ time complexity.

* Space complexity : $$O(1)$$

**Pop**

We need to remove the element from the top of the stack. This is the last inserted element in `q1`.
Because queue is FIFO (first in - first out) data structure, the last inserted element could be removed only after all elements, except it, have been removed. For this reason we need to maintain additional queue `q2`, which will serve as a temporary storage to enqueue the removed elements from q1. The last inserted element in `q2` is kept as top. Then the algorithm removes the last element in `q1`. We swap `q1` with `q2` to avoid copying all elements from `q2` to `q1`.

![Pop an element from stack](https://leetcode.com/media/original_images/225_stack_using_queues_popA.png){:width="539px"}
{:align="center"}

*Figure 2. Pop an element from stack*
{:align="center"}

**Java**

```java
// Removes the element on top of the stack.
public void pop() {
    while (q1.size() > 1) {
        top = q1.remove();
        q2.add(top);
    }
    q1.remove();
    Queue<Integer> temp = q1;
    q1 = q2;
    q2 = temp;
}
```

**Complexity Analysis**

* Time complexity : $$O(n)$$. The algorithm  dequeues n elements from `q1` and enqueues $$n - 1$$ elements to `q2`, where $$n$$ is the stack size. This gives $$2n - 1$$ operations.
* Space complexity : $$O(1)$$.

---
#### Approach #2 (Two Queues, push - $$O(n)$$, pop $$O(1)$$ )

**Algorithm**

**Push**

The algorithm inserts each new element to queue `q2` and keep it as the `top` element. In case queue `q1` is not empty (there are elements in the stack), we remove all elements from `q1` and add them to `q2`. In this way the new inserted element (`top` element in the stack) will be always positioned at the front of `q2`. We swap `q1` with `q2` to avoid copying all elements from `q2` to `q1`.

![Push an element in stack](https://leetcode.com/media/original_images/225_stack_using_queues_pushB.png){:width="539px"}
{:align="center"}

*Figure 3. Push an element in stack*
{:align="center"}

**Java**

```java
public void push(int x) {
    q2.add(x);
    top = x;
    while (!q1.isEmpty()) {                
        q2.add(q1.remove());
    }
    Queue<Integer> temp = q1;
    q1 = q2;
    q2 = temp;
}
```

**Complexity Analysis**

* Time complexity : $$O(n)$$. The algorithm  removes n elements from `q1` and inserts $$n + 1$$ elements to `q2`, where n is the stack size. This gives $$2n + 1$$ operations. The operations `add` and `remove` in linked lists has $$O(1)$$ complexity.

* Space complexity : $$O(1)$$.

**Pop**

The algorithm dequeues an element from  queue `q1` and keeps front element of `q1` as `top`.

![Pop an element from stack](https://leetcode.com/media/original_images/225_stack_using_queues_popB.png){:width="539px"}
{:align="center"}

*Figure 4. Pop an element from stack*
{:align="center"}

**Java**

```java
// Removes the element on top of the stack.
public void pop() {
    q1.remove();
    if (!q1.isEmpty()) {
    	top = q1.peek();
    }
}
```

**Complexity Analysis**

* Time complexity : $$O(1)$$.
* Space complexity : $$O(1)$$.

In both approaches `empty` and `top` operations have the same implementation.

**Empty**

Queue `q1` always contains all stack elements, so the algorithm checks `q1` size to return if the stack is empty.

```java
// Return whether the stack is empty.
public boolean empty() {
    return q1.isEmpty();
}
```
Time complexity : $$O(1)$$.

Space complexity : $$O(1)$$.

**Top**

The `top` element is kept in constant memory and is modified each time when we push or pop an element.

```java
// Get the top element.
public int top() {
    return top;
}
```
Time complexity : $$O(1)$$.
 The `top` element has been calculated in advance and only returned in `top` operation.

Space complexity : $$O(1)$$.

---
#### Approach #3 (One Queue, push - $$O(n)$$, pop $$O(1)$$ )

The mentioned above two approaches have one weakness, they use two queues. This could be optimized as we use only one queue, instead of two.

**Algorithm**

**Push**

When we push an element into a queue, it will be stored at back of the queue due to queue's properties.
But we need to implement a stack, where last inserted element should be in the front of the queue, not at the back. To achieve this we can invert the order of queue elements when pushing a new element.

![Push an element in stack](https://leetcode.com/media/original_images/225_stack_using_queues_pushC.png){:width="539px"}
{:align="center"}

*Figure 5. Push an element in stack*
{:align="center"}

**Java**

```java
private LinkedList<Integer> q1 = new LinkedList<>();

// Push element x onto stack.
public void push(int x) {
    q1.add(x);
    int sz = q1.size();
    while (sz > 1) {
        q1.add(q1.remove());
        sz--;
    }
}
```

**Complexity Analysis**

* Time complexity : $$O(n)$$. The algorithm  removes n elements and inserts $$n + 1$$ elements to `q1` , where n is the stack size. This gives $$2n + 1$$ operations. The operations `add` and `remove` in linked lists has $$O(1)$$ complexity.

* Space complexity : $$O(1)$$.

**Pop**

 The last inserted element is always stored at the front of `q1` and we can pop it for constant time.

**Java**
```java
// Removes the element on top of the stack.
public void pop() {
    q1.remove();
}
```
**Complexity Analysis**

* Time complexity : $$O(1)$$.
* Space complexity : $$O(1)$$.

**Empty**

Queue `q1` contains all stack elements, so the algorithm checks if `q1` is empty.

```java
// Return whether the stack is empty.
public boolean empty() {
    return q1.isEmpty();
}
```
Time complexity : $$O(1)$$.

Space complexity : $$O(1)$$.

**Top**

The `top` element is always positioned at the front of `q1`. Algorithm return it.

```java
// Get the top element.
public int top() {
    return q1.peek();
}
```
Time complexity : $$O(1)$$.

Space complexity : $$O(1)$$.

Analysis written by: @elmirap.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A simple C++ solution
- Author: sjtuldk
- Creation Date: Mon Jul 20 2015 10:13:51 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 15:56:45 GMT+0800 (Singapore Standard Time)

<p>
	class Stack {
	public:
		queue<int> que;
		// Push element x onto stack.
		void push(int x) {
			que.push(x);
			for(int i=0;i<que.size()-1;++i){
				que.push(que.front());
				que.pop();
			}
		}

		// Removes the element on top of the stack.
		void pop() {
			que.pop();
		}

		// Get the top element.
		int top() {
			return que.front();
		}

		// Return whether the stack is empty.
		bool empty() {
			return que.empty();
		}
	};
</p>


### Only push is O(n), others are O(1). Using one queue. Combination of two shared solutions
- Author: YYZ90
- Creation Date: Sat Jun 13 2015 18:15:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 02:42:59 GMT+0800 (Singapore Standard Time)

<p>
    class MyStack 
    {
        Queue<Integer> queue;
        
        public MyStack()
        {
            this.queue=new LinkedList<Integer>();
        }
        
        // Push element x onto stack.
        public void push(int x) 
        {
           queue.add(x);
           for(int i=0;i<queue.size()-1;i++)
           {
               queue.add(queue.poll());
           }
        }
    
        // Removes the element on top of the stack.
        public void pop() 
        {
            queue.poll();
        }
    
        // Get the top element.
        public int top() 
        {
            return queue.peek();
        }
    
        // Return whether the stack is empty.
        public boolean empty() 
        {
            return queue.isEmpty();
        }
    }
</p>


### Concise 1 Queue - Java, C++, Python
- Author: StefanPochmann
- Creation Date: Thu Jun 11 2015 20:22:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 02:39:07 GMT+0800 (Singapore Standard Time)

<p>
**Explanation:**

Just use a queue where you *"push to front"* by pushing to back and then rotating the queue until the new element is at the front (i.e., size-1 times move the front element to the back).

---

**C++:** 0 ms

    class Stack {
        queue<int> q;
    public:
        void push(int x) {
            q.push(x);
            for (int i=1; i<q.size(); i++) {
                q.push(q.front());
                q.pop();
            }
        }
    
        void pop() {
            q.pop();
        }
    
        int top() {
            return q.front();
        }
    
        bool empty() {
            return q.empty();
        }
    };

---

**Java:** 140 ms

    class MyStack {
    
        private Queue<Integer> queue = new LinkedList<>();
    
        public void push(int x) {
            queue.add(x);
            for (int i=1; i<queue.size(); i++)
                queue.add(queue.remove());
        }
    
        public void pop() {
            queue.remove();
        }
    
        public int top() {
            return queue.peek();
        }
    
        public boolean empty() {
            return queue.isEmpty();
        }
    }

---

**Python:** 36 ms

    class Stack:
    
        def __init__(self):
            self._queue = collections.deque()
    
        def push(self, x):
            q = self._queue
            q.append(x)
            for _ in range(len(q) - 1):
                q.append(q.popleft())
            
        def pop(self):
            return self._queue.popleft()
    
        def top(self):
            return self._queue[0]
        
        def empty(self):
            return not len(self._queue)
</p>


