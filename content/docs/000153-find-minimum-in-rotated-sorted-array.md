---
title: "Find Minimum in Rotated Sorted Array"
weight: 153
#id: "find-minimum-in-rotated-sorted-array"
---
## Description
<div class="description">
<p>Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.</p>

<p>(i.e., &nbsp;<code>[0,1,2,4,5,6,7]</code>&nbsp;might become &nbsp;<code>[4,5,6,7,0,1,2]</code>).</p>

<p>Find the minimum element.</p>

<p>You may assume no duplicate exists in the array.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> [3,4,5,1,2] 
<strong>Output:</strong> 1
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> [4,5,6,7,0,1,2]
<strong>Output:</strong> 0
</pre>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Amazon - 8 (taggedByAdmin: false)
- eBay - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Goldman Sachs - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---

#### Approach 1: Binary Search

**Intuition**

A very brute way of solving this question is to search the entire array and find the minimum element. The time complexity for that would be $$O(N)$$ given that `N` is the size of the array.

A very cool way of solving this problem is using the `Binary Search` algorithm. In binary search we find out the mid point and decide to either search on the left or right depending on some condition.

Since the given array is sorted, we can make use of binary search. However, the array is rotated. So simply applying the binary search won't work here.

In this question we would essentially apply a modified version of binary search where the `condition` that decides the search direction would be different than in a standard binary search.

We want to find the smallest element in a rotated sorted array. What if the array is not rotated? How do we check that?

If the array is not rotated and the array is in ascending order, then `last element > first element`.

<center>
<img src="../Figures/153/153_Minimum_Rotated_Sorted_Array_1.png" width="500"/>
</center>

In the above example `7 > 2`. This means that the array is still sorted and has no rotation.

<center>
<img src="../Figures/153/153_Minimum_Rotated_Sorted_Array_2.png" width="500"/>
</center>

In the above example `3 < 4`. Hence the array is rotated. This happens because the array was initially `[2, 3 ,4 ,5 ,6 ,7]`. But after the rotation the smaller elements`[2,3]` go at the back. i.e. [4, 5, 6, 7, `2, 3]`. Because of this the first element `[4]` in the rotated array becomes greater than the last element.

This means there is a point in the array at which you would notice a change. This is the point which would help us in this question. We call this the `Inflection Point`.

<center>
<img src="../Figures/153/153_Minimum_Rotated_Sorted_Array_3.png" width="500"/>
</center>

In this modified version of binary search algorithm, we are looking for this point. In the above example notice the `Inflection Point` .

> All the elements to the left of inflection point > first element of the array.<br/>
All the elements to the right of inflection point < first element of the array.


**Algorithm**

1. Find the `mid` element of the array.

2. If `mid element > first element of array` this means that we need to look for the inflection point on the right of `mid`.
3.  If `mid element < first element of array` this that we need to look for the inflection point on the left of `mid`.

<center>
<img src="../Figures/153/153_Minimum_Rotated_Sorted_Array_4.png" width="500"/>
</center>

In the above example mid element `6` is greater than first element `4`. Hence we continue our search for the inflection point to the right of mid.

4 . We stop our search when we find the inflection point, when either of the two conditions is satisfied:

`nums[mid] > nums[mid + 1]` Hence, **mid+1** is the smallest.

`nums[mid - 1] > nums[mid]` Hence, **mid** is the smallest.

<center>
<img src="../Figures/153/153_Minimum_Rotated_Sorted_Array_5.png" width="500"/>
</center>

In the above example. With the marked left and right pointers. The mid element is `2`. The element just before `2` is `7` and `7>2` i.e. `nums[mid - 1] > nums[mid]`. Thus we have found the point of inflection and `2` is the smallest element.

<iframe src="https://leetcode.com/playground/kRh5nyRU/shared" frameBorder="0" width="100%" height="500" name="kRh5nyRU"></iframe>

**Complexity Analysis**

* Time Complexity : Same as Binary Search $$O(\log N)$$
* Space Complexity : $$O(1)$$
<br /><br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Compact and clean C++ solution
- Author: changhaz
- Creation Date: Thu Oct 16 2014 12:55:20 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 18:07:59 GMT+0800 (Singapore Standard Time)

<p>
Classic binary search problem. 

Looking at subarray with index [start,end]. We can find out that if the first member is less than the last member, there's no rotation in the array. So we could directly return the first element in this subarray.

If the first element is larger than the last one, then we compute the element in the middle, and compare it with the first element. If  value of the element in the middle is larger than the first element, we know the rotation is at the second half of this array. Else, it is in the first half in the array.
 
Welcome to put your comments and suggestions.
 

     int findMin(vector<int> &num) {
            int start=0,end=num.size()-1;
            
            while (start<end) {
                if (num[start]<num[end])
                    return num[start];
                
                int mid = (start+end)/2;
                
                if (num[mid]>=num[start]) {
                    start = mid+1;
                } else {
                    end = mid;
                }
            }
            
            return num[start];
        }

Some corner cases will be discussed  [here][1]


 


  [1]: http://changhaz.wordpress.com/2014/10/15/leetcode-find-minimum-in-rotated-sorted-array/
</p>


### Beat 100%: Very Simple (Python), Very Detailed Explanation
- Author: water1111
- Creation Date: Sun Aug 12 2018 10:21:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Apr 11 2020 05:01:01 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def findMin(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        # set left and right bounds
        left, right = 0, len(nums)-1
                
        # left and right both converge to the minimum index;
        # DO NOT use left <= right because that would loop forever
        while left < right:
            # find the middle value between the left and right bounds (their average);
			# can equivalently do: mid = left + (right - left) // 2,
			# if we are concerned left + right would cause overflow (which would occur
			# if we are searching a massive array using a language like Java or C that has
			# fixed size integer types)
            mid = (left + right) // 2
                        
            # the main idea for our checks is to converge the left and right bounds on the start
            # of the pivot, and never disqualify the index for a possible minimum value.

            # in normal binary search, we have a target to match exactly,
            # and would have a specific branch for if nums[mid] == target.
            # we do not have a specific target here, so we just have simple if/else.
                        
            if nums[mid] > nums[right]:
                # we KNOW the pivot must be to the right of the middle:
                # if nums[mid] > nums[right], we KNOW that the
                # pivot/minimum value must have occurred somewhere to the right
                # of mid, which is why the values wrapped around and became smaller.

                # example:  [3,4,5,6,7,8,9,1,2] 
                # in the first iteration, when we start with mid index = 4, right index = 9.
                # if nums[mid] > nums[right], we know that at some point to the right of mid,
                # the pivot must have occurred, which is why the values wrapped around
                # so that nums[right] is less then nums[mid]

                # we know that the number at mid is greater than at least
                # one number to the right, so we can use mid + 1 and
                # never consider mid again; we know there is at least
                # one value smaller than it on the right
                left = mid + 1

            else:
                # here, nums[mid] <= nums[right]:
                # we KNOW the pivot must be at mid or to the left of mid:
                # if nums[mid] <= nums[right], we KNOW that the pivot was not encountered
                # to the right of middle, because that means the values would wrap around
                # and become smaller (which is caught in the above if statement).
                # this leaves the possible pivot point to be at index <= mid.
                            
                # example: [8,9,1,2,3,4,5,6,7]
                # in the first iteration, when we start with mid index = 4, right index = 9.
                # if nums[mid] <= nums[right], we know the numbers continued increasing to
                # the right of mid, so they never reached the pivot and wrapped around.
                # therefore, we know the pivot must be at index <= mid.

                # we know that nums[mid] <= nums[right].
                # therefore, we know it is possible for the mid index to store a smaller
                # value than at least one other index in the list (at right), so we do
                # not discard it by doing right = mid - 1. it still might have the minimum value.
                right = mid
                
        # at this point, left and right converge to a single index (for minimum value) since
        # our if/else forces the bounds of left/right to shrink each iteration:

        # when left bound increases, it does not disqualify a value
        # that could be smaller than something else (we know nums[mid] > nums[right],
        # so nums[right] wins and we ignore mid and everything to the left of mid).

        # when right bound decreases, it also does not disqualify a
        # value that could be smaller than something else (we know nums[mid] <= nums[right],
        # so nums[mid] wins and we keep it for now).

        # so we shrink the left/right bounds to one value,
        # without ever disqualifying a possible minimum
        return nums[left]
```
</p>


### A concise solution with proof in the comment
- Author: chuchao333
- Creation Date: Wed Dec 17 2014 13:43:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 22:11:45 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        int findMin(vector<int> &num) {
            int low = 0, high = num.size() - 1;
            // loop invariant: 1. low < high
            //                 2. mid != high and thus A[mid] != A[high] (no duplicate exists)
            //                 3. minimum is between [low, high]
            // The proof that the loop will exit: after each iteration either the 'high' decreases
            // or the 'low' increases, so the interval [low, high] will always shrink.
            while (low < high) {
                auto mid = low + (high - low) / 2;
                if (num[mid] < num[high])
                    // the mininum is in the left part
                    high = mid;
                else if (num[mid] > num[high])
                    // the mininum is in the right part
                    low = mid + 1;
            }
    
            return num[low];
        }
    };
</p>


