---
title: "Remove Nth Node From End of List"
weight: 19
#id: "remove-nth-node-from-end-of-list"
---
## Description
<div class="description">
<p>Given the <code>head</code> of a linked list, remove the <code>n<sup>th</sup></code> node from the end of the list and return its head.</p>

<p><strong>Follow up:</strong>&nbsp;Could you do this in one pass?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/10/03/remove_ex1.jpg" style="width: 542px; height: 222px;" />
<pre>
<strong>Input:</strong> head = [1,2,3,4,5], n = 2
<strong>Output:</strong> [1,2,3,5]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> head = [1], n = 1
<strong>Output:</strong> []
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> head = [1,2], n = 1
<strong>Output:</strong> [1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the list is <code>sz</code>.</li>
	<li><code>1 &lt;= sz &lt;= 30</code></li>
	<li><code>0 &lt;= Node.val &lt;= 100</code></li>
	<li><code>1 &lt;= n &lt;= sz</code></li>
</ul>

</div>

## Tags
- Linked List (linked-list)
- Two Pointers (two-pointers)

## Companies
- Amazon - 12 (taggedByAdmin: false)
- Facebook - 10 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Google - 6 (taggedByAdmin: false)
- Adobe - 5 (taggedByAdmin: false)
- Apple - 5 (taggedByAdmin: false)
- Yandex - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary
This article is for beginners. It introduces the following idea:
Linked List traversal and removal of nth element from the end.

## Solution

---
#### Approach 1: Two pass algorithm

**Intuition**

 We notice that the problem could be simply reduced to another one : Remove the $$(L - n + 1)$$ th node from the beginning in the list , where $$L$$ is the list length. This problem is easy to solve once we found list length $$L$$.

**Algorithm**

First we will add an auxiliary "dummy" node, which points to the list head. The "dummy" node is used to simplify some corner cases such as a list with only one node, or removing the head of the list. On the first pass, we find the list length $$L$$. Then we set a pointer to the dummy node and start to move it through the list till it comes to the $$(L - n)$$ th node. We relink `next` pointer of the $$(L - n)$$ th node to the $$(L - n + 2)$$ th node and we are done.

![Remove the nth element from a list](https://leetcode.com/media/original_images/19_Remove_nth_node_from_end_of_listA.png)
{:align="center"}

*Figure 1. Remove the L - n + 1 th element from a list.*
{:align="center"}

<iframe src="https://leetcode.com/playground/mjMSbADc/shared" frameBorder="0" width="100%" height="361" name="mjMSbADc"></iframe>

**Complexity Analysis**

* Time complexity : $$O(L)$$.

    The algorithm makes two traversal of the list, first to calculate list length $$L$$ and second to find the $$(L - n)$$ th node. There are $$2L-n$$ operations and time complexity is $$O(L)$$.

* Space complexity : $$O(1)$$.

    We only used constant extra space.
<br />
<br />

---
#### Approach 2: One pass algorithm

**Algorithm**

The above algorithm could be optimized to one pass. Instead of one pointer, we could use two pointers. The first pointer advances the list by $$n+1$$ steps from the beginning, while the second pointer starts from the beginning of the list. Now, both pointers are exactly separated by $$n$$ nodes apart. We maintain this constant gap by advancing both pointers together until the first pointer arrives past the last node. The second pointer will be pointing at the $$n$$th node counting from the last.
We relink the next pointer of the node referenced by the second pointer to point to the node's next next node.

![Remove the nth element from a list](https://leetcode.com/media/original_images/19_Remove_nth_node_from_end_of_listB.png)
{:align="center"}

*Figure 2. Remove the nth element from end of a list.*
{:align="center"}

<iframe src="https://leetcode.com/playground/BPxLi8Wz/shared" frameBorder="0" width="100%" height="344" name="BPxLi8Wz"></iframe>

**Complexity Analysis**

* Time complexity : $$O(L)$$.

    The algorithm makes one traversal of the list of $$L$$ nodes. Therefore time complexity is $$O(L)$$.

* Space complexity : $$O(1)$$.

    We only used constant extra space.

## Accepted Submission (java)
```java
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode pn = dummy;
        for (int i = 0; i < n; pn = pn.next, i++);
        ListNode ptail = pn.next;
        ListNode pm = dummy;
        while (ptail != null) {
            ptail = ptail.next;
            pm = pm.next;
        }
        pm.next = pm.next.next;
        return dummy.next;
    }
}
```

## Top Discussions
### Simple Java solution in one pass
- Author: TMS
- Creation Date: Fri Jan 09 2015 17:53:44 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 14:10:24 GMT+0800 (Singapore Standard Time)

<p>
A one pass solution can be done using  pointers. Move one pointer **fast** -->  **n+1** places forward, to maintain a gap of n between the two pointers and then move both at the same speed. Finally, when the fast pointer reaches the end, the slow pointer will be **n+1** places behind - just the right spot for it to be able to skip the next node.

Since the question gives that **n** is valid, not too many checks have to be put in place. Otherwise, this would be necessary.

    public ListNode removeNthFromEnd(ListNode head, int n) {
        
        ListNode start = new ListNode(0);
        ListNode slow = start, fast = start;
        slow.next = head;
        
        //Move fast in front so that the gap between slow and fast becomes n
        for(int i=1; i<=n+1; i++)   {
            fast = fast.next;
        }
        //Move fast to the end, maintaining the gap
        while(fast != null) {
            slow = slow.next;
            fast = fast.next;
        }
        //Skip the desired node
        slow.next = slow.next.next;
        return start.next;
	}
</p>


### 3 short Python solutions
- Author: StefanPochmann
- Creation Date: Mon May 25 2015 08:54:07 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 22:37:09 GMT+0800 (Singapore Standard Time)

<p>
**Value-Shifting - AC in 64 ms**

My first solution is "cheating" a little. Instead of really removing the nth *node*, I remove the nth *value*. I recursively determine the indexes (counting from back), then shift the values for all indexes larger than n, and then always drop the head.

    class Solution:
        def removeNthFromEnd(self, head, n):
            def index(node):
                if not node:
                    return 0
                i = index(node.next) + 1
                if i > n:
                    node.next.val = node.val
                return i
            index(head)
            return head.next

---

**Index and Remove - AC in 56 ms**

In this solution I recursively determine the indexes again, but this time my helper function removes the nth node. It returns two values. The index, as in my first solution, and the possibly changed head of the remaining list.

    class Solution:
        def removeNthFromEnd(self, head, n):
            def remove(head):
                if not head:
                    return 0, head
                i, head.next = remove(head.next)
                return i+1, (head, head.next)[i+1 == n]
            return remove(head)[1]

---

**n ahead - AC in 48 ms**

The standard solution, but without a dummy extra node. Instead, I simply handle the special case of removing the head right after the fast cursor got its head start.

    class Solution:
        def removeNthFromEnd(self, head, n):
            fast = slow = head
            for _ in range(n):
                fast = fast.next
            if not fast:
                return head.next
            while fast.next:
                fast = fast.next
                slow = slow.next
            slow.next = slow.next.next
            return head
</p>


### My short C++ solution
- Author: taotaou
- Creation Date: Mon Nov 24 2014 10:17:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 28 2018 20:47:22 GMT+0800 (Singapore Standard Time)

<p>
    class Solution
    {
    public:
        ListNode* removeNthFromEnd(ListNode* head, int n)
        {
            ListNode** t1 = &head, *t2 = head;
            for(int i = 1; i < n; ++i)
            {
                t2 = t2->next;
            }
            while(t2->next != NULL)
            {
                t1 = &((*t1)->next);
                t2 = t2->next;
            }
            *t1 = (*t1)->next;
            return head;
        }
    };
</p>


