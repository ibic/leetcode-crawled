---
title: "Index Pairs of a String"
weight: 984
#id: "index-pairs-of-a-string"
---
## Description
<div class="description">
<p>Given a <code>text</code>&nbsp;string and <code>words</code> (a list of strings), return all index pairs <code>[i, j]</code> so that the substring <code>text[i]...text[j]</code>&nbsp;is in the list of <code>words</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>text = <span id="example-input-1-1">&quot;thestoryofleetcodeandme&quot;</span>, words = <span id="example-input-1-2">[&quot;story&quot;,&quot;fleet&quot;,&quot;leetcode&quot;]</span>
<strong>Output: </strong><span id="example-output-1">[[3,7],[9,13],[10,17]]</span>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>text = <span id="example-input-2-1">&quot;ababa&quot;</span>, words = <span id="example-input-2-2">[&quot;aba&quot;,&quot;ab&quot;]</span>
<strong>Output: </strong><span id="example-output-2">[[0,1],[0,2],[2,3],[2,4]]</span>
<strong>Explanation: </strong>
Notice that matches can overlap, see &quot;aba&quot; is found in [0,2] and [2,4].
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>All strings contains only lowercase English letters.</li>
	<li>It&#39;s guaranteed that all strings in <code>words</code> are different.</li>
	<li><code>1 &lt;= text.length &lt;= 100</code></li>
	<li><code>1 &lt;= words.length &lt;= 20</code></li>
	<li><code>1 &lt;= words[i].length &lt;= 50</code></li>
	<li>Return the pairs <code>[i,j]</code> in sorted order (i.e. sort them by their first coordinate in case of ties sort them by their second coordinate).</li>
</ol>
</div>

## Tags
- String (string)
- Trie (trie)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Trie Java 2ms 100%
- Author: YifeiGong
- Creation Date: Sun Jun 02 2019 22:22:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 22:37:45 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int[][] indexPairs(String text, String[] words) {
        /*initializing tire and put all word from words into Trie.*/
        Trie trie=new Trie();
        for(String s:words){
            Trie cur=trie;
            for(char c:s.toCharArray()){
                if(cur.children[c-\'a\']==null){
                    cur.children[c-\'a\']=new Trie();
                }
                cur=cur.children[c-\'a\'];
            }
            cur.end=true;       /*mark there is a word*/
        }
        
        /*if text is "ababa", check "ababa","baba","aba","ba","a" individually.*/
        int len=text.length();
        List<int[]> list=new ArrayList<>();
        for(int i=0;i<len;i++){
            Trie cur=trie;
            char cc=text.charAt(i);
            int j=i;   /*j is our moving index*/
            
            while(cur.children[cc-\'a\']!=null){ 
                cur=cur.children[cc-\'a\'];
                if(cur.end){   /*there is a word ending here, put into our list*/
                    list.add(new int[]{i,j});
                }
                j++;
                if(j==len){  /*reach the end of the text, we stop*/
                    break;
                }
                else{
                    cc=text.charAt(j);  
                }
            }
        }
        /*put all the pairs from list into array*/
        int size=list.size();
        int[][] res=new int[size][2];
        int i=0;
        for(int[] r:list){
            res[i]=r;
            i++;
        }
        return res;
    }
}
class Trie{
    Trie[] children;
    boolean end;   /*indicate whether there is a word*/
    public Trie(){
        end=false;
        children=new Trie[26];
    }
}
```


Python version:
```
class Solution:
    def indexPairs(self, text: str, words: List[str]) -> List[List[int]]:
        trie={}
        for w in words:
            cur=trie
            for c in w:
                if c not in cur.keys():
                    cur[c]={}
                cur=cur[c]
            cur[\'#\']=True
        
        res=[]
        lens=len(text)
        for i in range(lens):
            cur=trie
            cc=text[i]
            j=i
            while cc in cur.keys():
                cur=cur[cc]
                if \'#\' in cur.keys():
                    res.append([i,j])
                j+=1
                if j==lens:
                    break
                else:
                    cc=text[j]
        return res
```
</p>


### Clean Python Trie Solution
- Author: BoboBiggins
- Creation Date: Tue Aug 27 2019 05:01:55 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 27 2019 05:01:55 GMT+0800 (Singapore Standard Time)

<p>
```
class TrieNode:
    def __init__(self):
        self.children, self.is_word = {}, False

    @staticmethod
    def construct_trie(words):
        root = TrieNode()
        for word in words:
            node = root
            for c in word:
                node.children.setdefault(c, TrieNode())
                node = node.children[c]
            node.is_word = True
        return root

class Solution:
    def indexPairs(self, text, words):
        res, trie = [], TrieNode.construct_trie(words)
        for l in range(len(text)):
            node = trie
            for r in range(l, len(text)):
                if text[r] not in node.children:
                    break
                node = node.children[text[r]]
                if node.is_word:
                    res.append((l, r))
        return res
```
</p>


### C++ Find and Sort
- Author: votrubac
- Creation Date: Thu Jun 13 2019 15:15:45 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 07 2020 01:56:31 GMT+0800 (Singapore Standard Time)

<p>
Just search for all indeces and then sort them. This naive approach gives us a good runtime (4 ms).
```cpp
vector<vector<int>> indexPairs(string text, vector<string>& words) {
    vector<vector<int>> res;
    for(auto w : words) {
        int p = text.find(w);
        while (p != string::npos) {
            res.push_back({p, p + (int)w.size() - 1});
            p = text.find(w, p + 1);
        }
    }
    sort(begin(res), end(res));
    return res;
}
```
</p>


