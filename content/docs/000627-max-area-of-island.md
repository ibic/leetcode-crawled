---
title: "Max Area of Island"
weight: 627
#id: "max-area-of-island"
---
## Description
<div class="description">
<p>Given a non-empty 2D array <code>grid</code> of 0&#39;s and 1&#39;s, an <b>island</b> is a group of <code>1</code>&#39;s (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.</p>

<p>Find the maximum area of an island in the given 2D array. (If there is no island, the maximum area is 0.)</p>

<p><b>Example 1:</b></p>

<pre>
[[0,0,1,0,0,0,0,1,0,0,0,0,0],
 [0,0,0,0,0,0,0,1,1,1,0,0,0],
 [0,1,1,0,1,0,0,0,0,0,0,0,0],
 [0,1,0,0,1,1,0,0,<b>1</b>,0,<b>1</b>,0,0],
 [0,1,0,0,1,1,0,0,<b>1</b>,<b>1</b>,<b>1</b>,0,0],
 [0,0,0,0,0,0,0,0,0,0,<b>1</b>,0,0],
 [0,0,0,0,0,0,0,1,1,1,0,0,0],
 [0,0,0,0,0,0,0,1,1,0,0,0,0]]
</pre>
Given the above grid, return <code>6</code>. Note the answer is not 11, because the island must be connected 4-directionally.

<p><b>Example 2:</b></p>

<pre>
[[0,0,0,0,0,0,0,0]]</pre>
Given the above grid, return <code>0</code>.

<p><b>Note:</b> The length of each dimension in the given <code>grid</code> does not exceed 50.</p>

</div>

## Tags
- Array (array)
- Depth-first Search (depth-first-search)

## Companies
- Facebook - 11 (taggedByAdmin: false)
- DoorDash - 10 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: false)
- Bloomberg - 4 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Palantir Technologies - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Cruise Automation - 2 (taggedByAdmin: false)
- Qualtrics - 7 (taggedByAdmin: false)
- Twitch - 3 (taggedByAdmin: false)
- Walmart Labs - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Affirm - 8 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)
- Intuit - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Depth-First Search (Recursive) [Accepted]

**Intuition and Algorithm**

We want to know the area of each connected shape in the grid, then take the maximum of these.

If we are on a land square and explore every square connected to it 4-directionally (and recursively squares connected to those squares, and so on), then the total number of squares explored will be the area of that connected shape.

To ensure we don't count squares in a shape more than once, let's use `seen` to keep track of squares we haven't visited before.  It will also prevent us from counting the same shape more than once.

<iframe src="https://leetcode.com/playground/CQGNqDhr/shared" frameBorder="0" name="CQGNqDhr" width="100%" height="479"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(R*C)$$, where $$R$$ is the number of rows in the given `grid`, and $$C$$ is the number of columns.  We visit every square once.

* Space complexity: $$O(R*C)$$, the space used by `seen` to keep track of visited squares, and the space used by the call stack during our recursion.

---
#### Approach #2: Depth-First Search (Iterative) [Accepted]

**Intuition and Algorithm**

We can try the same approach using a stack based, (or "iterative") depth-first search.

Here, `seen` will represent squares that have either been visited or are added to our list of squares to visit (`stack`).  For every starting land square that hasn't been visited, we will explore 4-directionally around it, adding land squares that haven't been added to `seen` to our `stack`.

On the side, we'll keep a count `shape` of the total number of squares seen during the exploration of this shape.  We'll want the running max of these counts.

<iframe src="https://leetcode.com/playground/khZHhSir/shared" frameBorder="0" name="khZHhSir" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(R*C)$$, where $$R$$ is the number of rows in the given `grid`, and $$C$$ is the number of columns.  We visit every square once.

* Space complexity: $$O(R*C)$$, the space used by `seen` to keep track of visited squares, and the space used by `stack`.

## Accepted Submission (java)
```java
class Solution {
    private List<int[]> getNeighborList(int y, int x, int height, int width) {
        return getNeighborStream(y, x, height, width).collect(Collectors.toList());
    }

    private Stream<int[]> getNeighborStream(int y, int x, int height, int width) {
        List<int[]> points = new ArrayList<int[]>();
        points.add(new int[]{y, x - 1});
        points.add(new int[]{y, x + 1});
        points.add(new int[]{y - 1, x});
        points.add(new int[]{y + 1, x});
        return points.stream().filter((int[] p) ->
            p[0] >= 0 && p[0] < height && p[1] >= 0 && p[1] < width);
    }

    private int dfs(int[][] grid, int y, int x, int height, int width) {
        grid[y][x] = 0;
        int r = 1;
        List<int[]> neighbors = getNeighborList(y, x, height, width);
        for (int[] point: neighbors) {
            int y1 = point[0];
            int x1 = point[1];
            if (grid[y1][x1] != 0) {
                r += dfs(grid, y1, x1, height, width);
            }
        }
        return r;
    }

    public int maxAreaOfIsland(int[][] grid) {
        int max = 0;
        int height = grid.length;
        int width = grid[0].length;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (grid[y][x] != 0) {
                    int area = dfs(grid, y, x, height, width);
                    max = area > max ? area : max;
                }
            }
        }
        return max;
    }
}
```

## Top Discussions
### [Java/C++] Straightforward  dfs solution
- Author: caihao0727mail
- Creation Date: Sun Oct 08 2017 11:49:20 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 04:25:21 GMT+0800 (Singapore Standard Time)

<p>
The idea is to count the area of each island using dfs. During the dfs, we set the value of each point in the island to ```0```. The time complexity is ```O(mn)```. 

Java version:
```
    public int maxAreaOfIsland(int[][] grid) {
        int max_area = 0;
        for(int i = 0; i < grid.length; i++)
            for(int j = 0; j < grid[0].length; j++)
                if(grid[i][j] == 1)max_area = Math.max(max_area, AreaOfIsland(grid, i, j));
        return max_area;
    }
    
    public int AreaOfIsland(int[][] grid, int i, int j){
        if( i >= 0 && i < grid.length && j >= 0 && j < grid[0].length && grid[i][j] == 1){
            grid[i][j] = 0;
            return 1 + AreaOfIsland(grid, i+1, j) + AreaOfIsland(grid, i-1, j) + AreaOfIsland(grid, i, j-1) + AreaOfIsland(grid, i, j+1);
        }
        return 0;
    }
```

C++ version:
```
    int maxAreaOfIsland(vector<vector<int>>& grid) {
        int max_area = 0;
        for(int i = 0; i < grid.size(); i++)
            for(int j = 0; j < grid[0].size(); j++)
                if(grid[i][j] == 1)max_area = max(max_area, AreaOfIsland(grid, i, j));
        return max_area;
    }
    
    int AreaOfIsland(vector<vector<int>>& grid, int i, int j){
        if( i >= 0 && i < grid.size() && j >= 0 && j < grid[0].size() && grid[i][j] == 1){
            grid[i][j] = 0;
            return 1 + AreaOfIsland(grid, i+1, j) + AreaOfIsland(grid, i-1, j) + AreaOfIsland(grid, i, j-1) + AreaOfIsland(grid, i, j+1);
        }
        return 0;
    }
```
</p>


### easy python
- Author: fhqplzj
- Creation Date: Sun Oct 08 2017 20:30:15 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 14:09:41 GMT+0800 (Singapore Standard Time)

<p>
```
    def maxAreaOfIsland(self, grid):
        m, n = len(grid), len(grid[0])

        def dfs(i, j):
            if 0 <= i < m and 0 <= j < n and grid[i][j]:
                grid[i][j] = 0
                return 1 + dfs(i - 1, j) + dfs(i, j + 1) + dfs(i + 1, j) + dfs(i, j - 1)
            return 0

        areas = [dfs(i, j) for i in range(m) for j in range(n) if grid[i][j]]
        return max(areas) if areas else 0
</p>


### Very simple DFS Java solution
- Author: fishercoder
- Creation Date: Sun Oct 08 2017 22:48:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 12:41:44 GMT+0800 (Singapore Standard Time)

<p>
```
    public int maxAreaOfIsland(int[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }
        int m = grid.length;
        int n = grid[0].length;
        int max = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    int area = dfs(grid, i, j, m, n, 0);
                    max = Math.max(area, max);
                }
            }
        }
        return max;
    }

    int dfs(int[][] grid, int i, int j, int m, int n, int area) {
        if (i < 0 || i >= m || j < 0 || j >= n || grid[i][j] == 0) {
            return area;
        }
        grid[i][j] = 0;
        area++;
        area = dfs(grid, i + 1, j, m, n, area);
        area = dfs(grid, i, j + 1, m, n, area);
        area = dfs(grid, i - 1, j, m, n, area);
        area = dfs(grid, i, j - 1, m, n, area);
        return area;
    }
```
</p>


