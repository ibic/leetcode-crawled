---
title: "User Activity for the Past 30 Days I"
weight: 1535
#id: "user-activity-for-the-past-30-days-i"
---
## Description
<div class="description">
<p>Table: <code>Activity</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| user_id       | int     |
| session_id    | int     |
| activity_date | date    |
| activity_type | enum    |
+---------------+---------+
There is no primary key for this table, it may have duplicate rows.
The activity_type column is an ENUM of type (&#39;open_session&#39;, &#39;end_session&#39;, &#39;scroll_down&#39;, &#39;send_message&#39;).
The table shows the user activities for a social media website. 
Note that each session belongs to exactly one user.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the daily active user count for a period of 30 days ending <strong>2019-07-27</strong>&nbsp;inclusively. A user was active on some day if he/she made at least one activity on that day.</p>

<p>The query result format is in the following example:</p>

<pre>
Activity table:
+---------+------------+---------------+---------------+
| user_id | session_id | activity_date | activity_type |
+---------+------------+---------------+---------------+
| 1       | 1          | 2019-07-20    | open_session  |
| 1       | 1          | 2019-07-20    | scroll_down   |
| 1       | 1          | 2019-07-20    | end_session   |
| 2       | 4          | 2019-07-20    | open_session  |
| 2       | 4          | 2019-07-21    | send_message  |
| 2       | 4          | 2019-07-21    | end_session   |
| 3       | 2          | 2019-07-21    | open_session  |
| 3       | 2          | 2019-07-21    | send_message  |
| 3       | 2          | 2019-07-21    | end_session   |
| 4       | 3          | 2019-06-25    | open_session  |
| 4       | 3          | 2019-06-25    | end_session   |
+---------+------------+---------------+---------------+

Result table:
+------------+--------------+ 
| day        | active_users |
+------------+--------------+ 
| 2019-07-20 | 2            |
| 2019-07-21 | 2            |
+------------+--------------+ 
Note that we do not care about days with zero active users.
</pre>

</div>

## Tags


## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy MySQL Solution
- Author: anisg1976
- Creation Date: Thu Aug 22 2019 14:11:45 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 22 2019 14:11:45 GMT+0800 (Singapore Standard Time)

<p>
```
select activity_date as day, count(distinct user_id) as active_users 
from Activity
where datediff(\'2019-07-27\', activity_date) <30
group by activity_date
```
</p>


### easy to understand MySQL solution beats 99.17%
- Author: xiaozhouxiao
- Creation Date: Mon Aug 05 2019 10:22:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 05 2019 10:22:03 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT activity_date AS day, COUNT(DISTINCT user_id) AS active_users
FROM Activity
GROUP BY activity_date
HAVING activity_date <= \'2019-07-27\' AND activity_date > \'2019-06-27\'
```
</p>


### It should be user activity for past 29 days
- Author: sudipta_lc
- Creation Date: Thu Sep 26 2019 13:55:25 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 26 2019 13:55:25 GMT+0800 (Singapore Standard Time)

<p>
It should be user activity for past 29 days.
</p>


