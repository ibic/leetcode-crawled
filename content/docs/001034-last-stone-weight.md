---
title: "Last Stone Weight"
weight: 1034
#id: "last-stone-weight"
---
## Description
<div class="description">
<p>We have a collection of stones, each stone&nbsp;has a positive integer weight.</p>

<p>Each turn, we choose the two <strong>heaviest</strong>&nbsp;stones&nbsp;and smash them together.&nbsp; Suppose the stones have weights <code>x</code> and <code>y</code> with <code>x &lt;= y</code>.&nbsp; The result of this smash is:</p>

<ul>
	<li>If <code>x == y</code>, both stones are totally destroyed;</li>
	<li>If <code>x != y</code>, the stone of weight <code>x</code> is totally destroyed, and the stone of weight <code>y</code> has new weight <code>y-x</code>.</li>
</ul>

<p>At the end, there is at most 1 stone left.&nbsp; Return the weight of this stone (or 0 if there are no stones left.)</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>[2,7,4,1,8,1]
<strong>Output: </strong>1
<strong>Explanation: </strong>
We combine 7 and 8 to get 1 so the array converts to [2,4,1,1,1] then,
we combine 2 and 4 to get 2 so the array converts to [2,1,1,1] then,
we combine 2 and 1 to get 1 so the array converts to [1,1,1] then,
we combine 1 and 1 to get 0 so the array converts to [1] then that&#39;s the value of last stone.</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= stones.length &lt;= 30</code></li>
	<li><code>1 &lt;= stones[i] &lt;= 1000</code></li>
</ol>

</div>

## Tags
- Heap (heap)
- Greedy (greedy)

## Companies
- Amazon - 7 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Array-Based Simulation

**Intuition**

Conceptually, the simplest way we could solve this problem is to repeatedly search for the 2 largest stones in the array, delete them, and then if they are not the same size, add the new stone size back in. We can repeat this process until there is only one stone left.

**Algorithm**

Because the array is not sorted, there is no need to preserve the original order. Removals should be done by swapping with the last value, not by shuffling all values along.

<iframe src="https://leetcode.com/playground/NDbPRc5g/shared" frameBorder="0" width="100%" height="500" name="NDbPRc5g"></iframe>


**Complexity Analysis**

Let $$N$$ be the **length of stones**. Here on LeetCode, we're only testing your code with cases where $$N ≤ 30$$. In an interview though, be very careful about such assumptions. It is very likely your interviewer expects you to come up with the best possible algorithm you could (thus handling the highest possible value of $$N$$ you can).

- Time complexity : $$O(N^2)$$.

    The only non-$$O(1)$$ method of `StoneArray` is `findAndRemoveMax()`. This method does a single pass over the array, to find the index of the maximum value. This pass has a cost of $$O(N)$$. Once we find the maximum value, we delete it, although this only has a cost of $$O(1)$$ because instead of shuffling along, we're simply swapping with the end. 

    Each time around the main loop, there is a net loss of either 1 or 2 stones. Starting with $$N$$ stones and needing to get to $$1$$ stone, this is up to $$N - 1$$ iterations. On each of these iterations, it finds the maximum twice. In total, we get $$O(N^2)$$.

    Note that even if we'd shuffled instead of swapped with the end, the `findAndRemoveMax()` method still would have been $$O(N)$$, as the pass and then deletion are done one-after-the-other. However, it's often best to avoid needlessly large constants.

- Space complexity : $$O(N)$$ or $$O(1)$$.

    For the Python: We are not allocating any new space for data structures, and instead are modifying the input list. Note that this *modifies the input*. This has its pros and cons; it saves space, but it means that other functions can't use the same array.

    For the Java: We need to convert the input to an ArrayList, and therefore the `int`s to `Integer`s. It is possible to write a $$O(1)$$ space solution for Java, however it is long-winded and a lot of work for what is a poor overall approach anyway.

<br/>

---

#### Approach 2: Sorted Array-Based Simulation

**Intuition**

*Note: This approach is no better than Approach 1. We're only including so that we can look at *why* it doesn't work as well as one might initially assume. See Approach 3 for the optimal approach.*

To simplify the search-for-maximum process, we could instead maintain a sorted array. We'd need to sort the array at the start, and then ensure that each time we need to add a stone back, that we're maintaining the sorted order.

Unfortunately, inserting a stone into a *sorted* array is an $$O(N)$$ operation. While we can use binary search to determine where we should put it, inserting it still ultimately requires shifting all of the stones after it down by one place. This makes the approach no better than the previous one from a complexity point-of-view (in fact, it's actually worse because the space complexity is now unlikely to be $$O(1)$$).

**Algorithm**

<iframe src="https://leetcode.com/playground/Y6MfiCQt/shared" frameBorder="0" width="100%" height="500" name="Y6MfiCQt"></iframe>

**Complexity Analysis**

Let $$N$$ be the **length of stones**.

- Time complexity : $$O(N^2)$$.

    The first part of the algorithm is sorting the list. This has a cost of $$O(N \, \log \, N)$$.

    Like before, we're repeating the main loop up to $$N - 1$$ times. And again, we're doing an $$O(N)$$ operation each time; adding the new stone back into the array, maintaining sorted order by shuffling existing stones along to make space for it. Identifying the two largest stones was $$O(1)$$ in this approach, but unfortunately this was subsumed by the inefficient adds. This gives us a total of $$O(N^2)$$.

    Because $$O(N^2)$$ is strictly larger than $$O(N \, \log \, N)$$, we're left with a final time complexity of $$O(N^2)$$.

- Space complexity : Varies from $$O(N)$$ to $$O(1)$$.

    Like in Approach 1, we can choose whether or not to modify the input list. If we do modify the input list, this will cost anywhere from $$O(N)$$ to $$O(1)$$ space, depending on the sorting algorithm used. However, if we don't, it will always cost at least $$O(N)$$ to make a copy. Modifying the input has its pros and cons; it saves space, but it means that other functions can't use the same array.

An alternative to this approach is to simply sort inside the loop every time. This will be even worse, with a time complexity of $$O(N^2 \, \log \, N)$$.

<br/>

---

#### Approach 3: Heap-Based Simulation

**Intuition**

Approach 1 found and removed the maximum stones in $$O(N)$$ time, and added the new stone in $$O(1)$$ time. Approach 2 inverted this, as finding and removing the maximum stones took $$O(1)$$ time, but adding the new stone took $$O(N)$$ time. In both cases, we're left with an overall time complexity of $$O(N)$$ per stone-smash turn.

We want to find a solution that makes both removing the maximums, and adding a new stone, *less than* $$O(N)$$.

For this kind of maximum-maintenance, we use a **Max-Heap**, also known as a **Max-Priority Queue**. A Max-Heap is a data structure that can take items, and can remove and return the maximum, with both operations taking $$O(\log \, N)$$ time. It does this by maintaining the items in a special order (within the array), or as a balanced binary tree. We don't need to know these details though, almost all programming languages have a Heap data structure!

Here is the pseudocode using a Heap.

```text
define function last_stone_weight(stones):
    heap = a new Max-Heap
    add all stones to heap
    while heap contains more than 1 stone:
        heavy_stone_1 = remove max from heap
        heavy_stone_2 = remove max from heap
        if heavy_stone_1 is heavier than heavy_stone_2:
            new_stone = heavy_stone_1 - heavy_stone_2
            add new_stone to heap
    if heap is empty:
        return 0
    return last stone on heap
```

**Algorithm**

While most programming languages have a **Heap/ Priority Queue** data structure, some, such as Python and Java, only have **Min-Heap**. Just as the name suggests, this is a Heap that instead of always returning the maximum item, it returns the minimum. There are two solutions to this problem:

1. Multiply all numbers going into the heap by `-1`, and then multiply them by `-1` to restore them when they come out.
2. Pass a comparator in (language-dependent).

In Python, we'll use the first solution, and in Java we'll use the second.

<iframe src="https://leetcode.com/playground/HmX4vu3j/shared" frameBorder="0" width="100%" height="480" name="HmX4vu3j"></iframe>

**Complexity Analysis**

Let $$N$$ be the **length of stones**.

- Time complexity : $$O(N \, \log \, N)$$.

    Converting an array into a Heap takes $$O(N)$$ time (it isn't actually sorting; it's putting them into an order that allows us to get the maximums, each in $$O(\log \, N)$$ time).

    Like before, the main loop iterates up to $$N - 1$$ times. This time however, it's doing up to three $$O(\log \, N)$$ operation each time; two removes, and an optional add. Like always, the three is an ignored constant. This means that we're doing $$N \cdot O(\log \, N) = O(N \, \log \, N)$$ operations.

- Space complexity : $$O(N)$$ or $$O(\log \, N)$$.

    In Python, converting a list to a heap is done in-place, requiring $$O(1)$$ auxillary space, giving a total space complexity of $$O(1)$$. Modifying the input has its pros and cons; it saves space, but it means that other functions can't use the same array.

    In Java though, it's $$O(N)$$ to create the `PriorityQueue`.

    We could reduce the space complexity to $$O(1)$$ by implementing our own iterative heapfiy, if needed.

<br/>

---

#### Approach 4: Bucket Sort

**Intuition**

*This approach is only viable when the maximum stone weight is small, or is at least smaller than the number of stones.*

Let $$W$$ be the maximum stone weight in the input array. We can create a bucket array of size $$W + 1$$, where each index of the bucket array represents a stone weight. Then, we can bucket "sort" the stones in $$O(N)$$ time by iterating over them and incrementing the relevant bucket array index by 1. 

![The buckets for the input array.](../Figures/1046/buckets.png)

We can then process the buckets as shown in the following animation.

!?!../Documents/1046_bucket_approach.json:960,250!?!

**Algorithm**

<iframe src="https://leetcode.com/playground/XrkMtyCr/shared" frameBorder="0" width="100%" height="500" name="XrkMtyCr"></iframe>

**Complexity Analysis**

- Time complexity : $$O(N + W)$$.

    Putting the $$N$$ stones of the input array into the bucket array is $$O(N)$$, because inserting each stone is an $$O(1)$$ operation.

    In the worst case, the main loop iterates through all of the $$W$$ indexes of the bucket array. Processing each bucket is an $$O(1)$$ operation. This, therefore, is $$O(W)$$.

    Seeing as we don't know which is larger out of $$N$$ and $$W$$, we get a total of $$O(N + W)$$.

    Technically, this algorithm is *pseudo-polynomial*, as its time complexity is dependent on the *numeric value of the input*. Pseudo-polynomial algorithms are useful when there is no "true" polynomial alternative, but in situations such as this one where we have an $$O(N \, \log \, N)$$ alternative (Approach 3), they are only useful for very specific inputs.

    With the small values of $$W$$ that your code is tested against for this question here on LeetCode, this approach turns out to be faster than Approach 3. But that does *not* make it the better approach.

- Space complexity : $$O(W)$$.
    
    We allocated a new array of size $$W$$.

When I looked through the discussion forum for this question, I was surprised to see a number of people arguing that this approach is $$O(N)$$, on the basis that we could say $$W$$ is a constant, due to the problem description stating it has a maximum value of $$1000$$. The trouble with this argument is that $$N$$ also has a maximum specified (of $$30$$, in fact), and so it is arbitrary to argue that $$W$$ is a constant, yet $$N$$ is not. These constraints on LeetCode problems are intended to help you determine whether or not your algorithm will be fast enough. They are not supposed to imply some variables can be treated as "constants". A correct time/ space complexity should treat them as *unbounded*.

<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Priority Queue
- Author: lee215
- Creation Date: Sun May 19 2019 12:21:18 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jan 08 2020 23:50:09 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
Put all elements into a priority queue.
Pop out the two biggest, push back the difference,
until there are no more two elements left.
<br>

# **Complexity**
Time `O(NlogN)`
Space `O(N)`
<br>

**Java, PriorityQueue**
```java
    public int lastStoneWeight(int[] A) {
        PriorityQueue<Integer> pq = new PriorityQueue<>((a, b)-> b - a);
        for (int a : A)
            pq.offer(a);
        while (pq.size() > 1)
            pq.offer(pq.poll() - pq.poll());
        return pq.poll();
    }
```

**C++, priority_queue**
```cpp
    int lastStoneWeight(vector<int>& A) {
        priority_queue<int> pq(begin(A), end(A));
        while (pq.size() > 1) {
            int x = pq.top(); pq.pop();
            int y = pq.top(); pq.pop();
            if (x > y) pq.push(x - y);
        }
        return pq.empty() ? 0 : pq.top();
    }
```
**Python, using heap, O(NlogN) time**
```py
    def lastStoneWeight(self, A):
        h = [-x for x in A]
        heapq.heapify(h)
        while len(h) > 1 and h[0] != 0:
            heapq.heappush(h, heapq.heappop(h) - heapq.heappop(h))
        return -h[0]
```

**Python, using binary insort, O(N^2) time**
```py
    def lastStoneWeight(self, A):
        A.sort()
        while len(A) > 1:
            bisect.insort(A, A.pop() - A.pop())
        return A[0]
```
</p>


### Java simple to complex solutions explained -- 0 ms top 100% time 100% memory 2 lines of code only!
- Author: tiagodsilva
- Creation Date: Sun Apr 12 2020 18:57:33 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 14 2020 16:19:18 GMT+0800 (Singapore Standard Time)

<p>
At every step of the algorithm, we need to know the top heaviest stone.
The most efficient way to retrieve the max for large input sizes is to use a max heap, which in Java is a PriorityQueue (min heap) with a reverse comparator:

O(n log (n)) time O(n) space 
1 ms time 37.5 MB space
91% time 100% space

```
    public int lastStoneWeight(int[] stones) {
        PriorityQueue<Integer> queue = new PriorityQueue<>(Collections.reverseOrder());
        for(int i : stones) {
            queue.add(i);
        }
        int x;
        int y;
        while(queue.size() > 1) {
            y = queue.poll();
            x = queue.poll();
            if(y > x) {
                queue.offer(y-x);   
            }
        }
        return queue.isEmpty() ? 0 : queue.poll();
    }
```

But we can do better, right? If you have done a few of these problems you might know to use BucketSort with has constant access and no typical "sorting", we can do O(n) time. 

However, it is more accurate to say the time would be O(n + maxStoneWeight) because we will build a bucket for every possible weight. And usually a O(n + 1000) would be a great solution, but the test cases here have a very short input size. the number of stones goes only from 0 to 30, so this solution actually performs worse than O(n) since n is at most 30! O(30) == O(1030) but 30 < 1030. Both have the same complexity, but the first runs faster, and you might have not noticed why unless you check the inputs given in the tests.

```
    int[] buckets = new int[1001];
    for(int stone: stones)
      buckets[stone]++;
    int i = 1000;
    int j;
    while(i > 0) {
      if(buckets[i] == 0) {
        i--;
      } else {
        buckets[i] = buckets[i] % 2;
        if(buckets[i] != 0) {
          j = i-1;
          while(j > 0 && buckets[j] == 0)
            j--;
          if(j == 0)
            return i;
          buckets[i - j]++;
          buckets[j]--;
          i--;
        }
      }
    }
    return 0;
```

Runs even slower! Remember to not always apply a solution that seems faster because you didn\'t consider your context or use cases. here the number of stones is much smaller than the weight of the stones.

So if we know that the number of stones is quite small, can we do even better than the PriorityQueue? What is a very fast sorting algorithm for small sets, better than building a heap? Sort in place in the array! Don\'t waste time building new objects or copies of the input.

O( n^2 log(n) ) because for every stone n, we sort the array O(nlog(n))
O(1) space, no extra space, sort in place
**0 ms time 36.9 MB	space
faster than 100% and less space used than 100% of other solutions**

```
  public static int lastStoneWeight(int[] stones) {
    Arrays.sort(stones);
    for(int i=stones.length-1; i>0; i--) {
      stones[i-1] = stones[i] - stones[i-1];
      Arrays.sort(stones);
    }
    return stones[0];
  }
```

And just for fun, let\'s mangle it into **2 lines**:
```
    for(int i=stones.length; i>0; stones[i-1] = i==stones.length? stones[i-1] : stones[i] - stones[i-1], Arrays.sort(stones), i--);
    return stones[0];
```

EDIT: 
To be completely clear, if the input size was unrestricted and not less than 30 stones, BucketSort would be the best solution because it runs in O(maxStoneWeight). If the stones weight is not unrestricted, then BucketSort cannot work as you would need 2,147,483,647 buckets. In that case, PriorityQueue would be the best solution.

**In an interview, you should only discuss either BucketSort or PriorityQueue (max head) solutions**. The only reason I mentioned the sorting solution, which is much worse than the two other solutions, is because the input size in the tests of this challenge are so small that this solution is faster.
</p>


### [Java/Python 3] easy code using PriorityQueue/heapq w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun May 19 2019 12:35:25 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 14 2020 19:10:04 GMT+0800 (Singapore Standard Time)

<p>
Sort stones descendingly in PriorityQueue, then pop out pair by pair, compute the difference between them and add back to PriorityQueue.

Note: since we already know the first poped out is not smaller, it is not necessary to use Math.abs().

```java
    public int lastStoneWeight(int[] stones) {
    //  PriorityQueue<Integer> q = new PriorityQueue<>(Comparator.reverseOrder());
        var q = new PriorityQueue<Integer>(Comparator.reverseOrder());  // Credit to @YaoFrankie.
        for (int st : stones) { 
            q.offer(st); 
        }
        while (q.size() > 1) {
            q.offer(q.poll() - q.poll());
        }
        return q.peek();
    }
```
```
    def lastStoneWeight(self, stones: List[int]) -> int:
        q = [-stone for stone in stones]
        heapq.heapify(q)
        while (len(q)) > 1:
            heapq.heappush(q, heapq.heappop(q) - heapq.heappop(q))
        return -q[0]
```
**Analysis:**

Time: O(nlogn), space: O(n), where n = stones.length.

----

**Q & A:**

Q: If not adding zeroes in the queue when polling out two elements are equal, is the result same as the above code?

A: Yes. 0s are always at the end of the PriorityQueue. No matter a positive deduct 0 or 0 deduct 0, the result is same as NOT adding 0s into the PriorityQueue.
</p>


