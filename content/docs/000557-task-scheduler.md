---
title: "Task Scheduler"
weight: 557
#id: "task-scheduler"
---
## Description
<div class="description">
<p>Given a characters array <code>tasks</code>, representing the tasks a CPU needs to do, where each letter represents a different task. Tasks could be done in any order. Each task is done in one unit of time. For each unit of time, the CPU could complete either one task or just be idle.</p>

<p>However, there is a non-negative integer&nbsp;<code>n</code> that represents the cooldown period between&nbsp;two <b>same tasks</b>&nbsp;(the same letter in the array), that is that there must be at least <code>n</code> units of time between any two same tasks.</p>

<p>Return <em>the least number of units of times that the CPU will take to finish all the given tasks</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> tasks = [&quot;A&quot;,&quot;A&quot;,&quot;A&quot;,&quot;B&quot;,&quot;B&quot;,&quot;B&quot;], n = 2
<strong>Output:</strong> 8
<strong>Explanation:</strong> 
A -&gt; B -&gt; idle -&gt; A -&gt; B -&gt; idle -&gt; A -&gt; B
There is at least 2 units of time between any two same tasks.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> tasks = [&quot;A&quot;,&quot;A&quot;,&quot;A&quot;,&quot;B&quot;,&quot;B&quot;,&quot;B&quot;], n = 0
<strong>Output:</strong> 6
<strong>Explanation:</strong> On this case any permutation of size 6 would work since n = 0.
[&quot;A&quot;,&quot;A&quot;,&quot;A&quot;,&quot;B&quot;,&quot;B&quot;,&quot;B&quot;]
[&quot;A&quot;,&quot;B&quot;,&quot;A&quot;,&quot;B&quot;,&quot;A&quot;,&quot;B&quot;]
[&quot;B&quot;,&quot;B&quot;,&quot;B&quot;,&quot;A&quot;,&quot;A&quot;,&quot;A&quot;]
...
And so on.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> tasks = [&quot;A&quot;,&quot;A&quot;,&quot;A&quot;,&quot;A&quot;,&quot;A&quot;,&quot;A&quot;,&quot;B&quot;,&quot;C&quot;,&quot;D&quot;,&quot;E&quot;,&quot;F&quot;,&quot;G&quot;], n = 2
<strong>Output:</strong> 16
<strong>Explanation:</strong> 
One possible solution is
A -&gt; B -&gt; C -&gt; A -&gt; D -&gt; E -&gt; A -&gt; F -&gt; G -&gt; A -&gt; idle -&gt; idle -&gt; A -&gt; idle -&gt; idle -&gt; A
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= task.length &lt;= 10<sup>4</sup></code></li>
	<li><code>tasks[i]</code> is upper-case English letter.</li>
	<li>The integer <code>n</code> is in the range <code>[0, 100]</code>.</li>
</ul>

</div>

## Tags
- Array (array)
- Greedy (greedy)
- Queue (queue)

## Companies
- Facebook - 22 (taggedByAdmin: true)
- Amazon - 5 (taggedByAdmin: false)
- Bloomberg - 5 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Nutanix - 3 (taggedByAdmin: false)
- Rubrik - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---

#### Overview

We're asked to execute all the tasks in the input array.
The main problem is the presence of cooling interval between 
the same tasks. 

![fig](../Figures/621/cooling2.png)

<br />
<br />


---
#### Approach 1: Greedy

**Intuition**

The total number of CPU intervals we need consists of busy and idle slots. 
Number of busy slots is defined by the number of tasks to execute: 
`len(tasks)`. The problem is to compute a number of idle slots.

Maximum possible number of idle slots is defined by the 
frequency of the most frequent task: `idle_time <= (f_max - 1) * n`.

![fig](../Figures/621/idle.png)

This maximum could be decreased because one 
doesn't need to keep the CPU idle during cooling periods. 
It could execute different tasks as well. 

To compute the minimum number of idle slots, 
one could use a greedy strategy.
The idea is to sort the tasks by frequency in the descending order 
and fulfill as many idle slots as one could.

!?!../Documents/621_LIS.json:1000,492!?!

**Algorithm**

- The maximum number of tasks is 26. Let's allocate an array 
`frequencies` of 26 elements to keep the frequency of each task.

- Iterate over the input array and store the frequency of task A 
at index 0, the frequency of task B at index 1, etc. 

- Sort the array and retrieve the maximum frequency `f_max`.
This frequency defines the max possible idle time:
`idle_time = (f_max - 1) * n`.

- Pick the elements in the descending order one by one.
At each step, decrease the idle time by `min(f_max - 1, f)` where 
`f` is a current frequency. 
Remember, that `idle_time` is greater or equal to 0. 

- Return busy slots + idle slots: `len(tasks) + idle_time`.

**Implementation**

<iframe src="https://leetcode.com/playground/DnNQuVqB/shared" frameBorder="0" width="100%" height="429" name="DnNQuVqB"></iframe>

**Complexity Analysis**

* Time Complexity: $$\mathcal{O}(N_{total})$$, where $$N_{total}$$ is
a number of tasks to execute. This time is needed to iterate over the
input array `tasks` and compute the array `frequencies`.
Array `frequencies` contains 26 elements, and hence all operations
with it takes constant time.

* Space Complexity: $$O(1)$$, to keep the array `frequencies` 
of 26 elements.
<br />
<br />


---
#### Approach 2: Math

**Intuition**

Let's use some math to _compute_ the answer. 
There are two possible situations: 

- The most frequent task is not frequent enough to force 
the presence of idle slots.

![fig](../Figures/621/all2.png)

- The most frequent task is frequent enough to force some 
idle slots.

![fig](../Figures/621/frequent2.png)

> The answer is the maximum between these two.

The first situation is straightforward because the total number of slots 
is defined by the number of tasks: `len(tasks)`.

The second situation is a bit more tricky and requires to know 
the number `n_max` and the frequency `f_max` of the most frequent tasks.

![fig](../Figures/621/f_max.png)

Now it's easy to compute:

![fig](../Figures/621/compute.png)


**Algorithm**

- The maximum number of tasks is 26. Let's allocate an array 
`frequencies` of 26 elements to keep the frequency of each task.

- Iterate over the input array and store the frequency of task A 
at index 0, the frequency of task B at index 1, etc. 

- Find the maximum frequency: `f_max = max(frequencies)`.

- Find the number of tasks which have the max frequency:
`n_max = frequencies.count(f_max)`.

- If the number of slots to use is defined by the most frequent task,
it's equal to `(f_max - 1) * (n + 1) + n_max`.

- Otherwise, the number of slots to use is defined by the overall
number of tasks: `len(tasks)`.

- Return the maximum of these two: 
`max(len(tasks), (f_max - 1) * (n + 1) + n_max)`.

**Implementation**

<iframe src="https://leetcode.com/playground/YjmrQFAv/shared" frameBorder="0" width="100%" height="446" name="YjmrQFAv"></iframe>

**Complexity Analysis**

* Time Complexity: $$\mathcal{O}(N_{total})$$, where $$N_{total}$$ is
a number of tasks to execute. This time is needed to iterate over the
input array `tasks` and to compute the array `frequencies`.
Array `frequencies` contains 26 elements, and hence all operations
with it takes constant time.

* Space Complexity: $$O(1)$$, to keep the array `frequencies` 
of 26 elements.
<br />
<br />


---

## Accepted Submission (python3)
```python3
class Solution:
    def leastInterval(self, tasks: List[str], n: int) -> int:
        set = [0] * 26
        for t in tasks:
            set[ord(t) - ord('A')] += 1
        maxy = 0
        repeats = 0
        for count in set:
            if count > maxy:
                maxy = count
                repeats = 1
            elif count == maxy:
                repeats += 1
        return max((maxy - 1) * (n + 1) + repeats, len(tasks))
```

## Top Discussions
### Java O(n) time O(1) space 1 pass, no sorting solution with detailed explanation
- Author: g27as
- Creation Date: Mon Jun 19 2017 02:00:53 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 20:48:27 GMT+0800 (Singapore Standard Time)

<p>
The key is to find out how many idles do we need.
Let's first look at how to arrange them. it's not hard to figure out that we can do a "greedy arrangement": always arrange task with most frequency first.
E.g. we have following tasks : 3 A, 2 B, 1 C. and we have n = 2. According to what we have above, we should first arrange A, and then B and C. Imagine there are "slots" and we need to arrange tasks by putting them into "slots". Then A should be put into slot 0, 3, 6 since we need to have at least n = 2 other tasks between two A. After A put into slots, it looks like this:

A ? ? A ? ? A
"?" is "empty" slots.

Now we can use the same way to arrange B and C. The finished schedule should look like this:

A B C A B # A
"#" is idle

Now we have a way to arrange tasks. But the problem only asks for number of CPU intervals, so we don't need actually arrange them. Instead we only need to get the total idles we need and the answer to problem is just number of idles + number of tasks.
Same example: 3 A, 2 B, 1 C, n = 2. After arranging A, we have:
A ? ? A ? ? A
We can see that A separated slots into (count(A) - 1) = 2 parts, each part has length n. With the fact that A is the task with most frequency, it should need more idles than any other tasks. **In this case if we can get how many idles we need to arrange A, we will also get number of idles needed to arrange all tasks.** Calculating this is not hard, we first get number of parts separated by A: partCount = count(A) - 1; then we can know number of empty slots: emptySlots = partCount * n; we can also get how many tasks we have to put into those slots: availableTasks = tasks.length - count(A). Now if we have emptySlots > availableTasks which means we have not enough tasks available to fill all empty slots, we must fill them with idles. Thus we have **idles = max(0, emptySlots - availableTasks);**
Almost done. One special case: what if there are more than one task with most frequency? OK, let's look at another example: 3 A 3 B 2 C 1 D,  n = 3
Similarly we arrange A first:
A ? ? ? A ? ? ? A
Now it's time to arrange B, we find that we have to arrange B like this:
A B ? ? A B ? ? A B
we need to put every B right after each A. Let's look at this in another way, think of sequence "A B" as a special task "X", then we got:
X ? ? X ? ? X
Comparing to what we have after arranging A:
A ? ? ? A ? ? ? A
The only changes are length of each parts (from 3 to 2) and available tasks. By this we can get more general equations:
**partCount = count(A) - 1
emptySlots = partCount * (n - (count of tasks with most frequency - 1))
availableTasks = tasks.length - count(A) * count of tasks with most frenquency
idles = max(0, emptySlots - availableTasks)
result = tasks.length + idles**

What if we have more than n tasks with most frequency and we got emptySlot negative? Like 3A, 3B, 3C, 3D, 3E, n = 2. In this case seems like we can't put all B C S inside slots since we only have n = 2.
Well partCount is actually the "minimum" length of each part required for arranging A. You can always make the length of part longer.
E.g. 3A, 3B, 3C, 3D, 2E, n = 2.
You can always first arrange A, B, C, D as:
A B C D | A B C D | A B C D
in this case you have already met the "minimum" length requirement for each part (n = 2), you can always put more tasks in each part if you like:
e.g.
A B C D E | A B C D E | A B C D

emptySlots < 0 means you have already got enough tasks to fill in each part to make arranged tasks valid. But as I said you can always put more tasks in each part once you met the "minimum" requirement.

To get count(A) and count of tasks with most frequency, we need to go through inputs and calculate counts for each distinct char. This is O(n) time and O(26) space since we only handle upper case letters.
All other operations are O(1) time O(1) space which give us total time complexity of O(n) and space O(1)
```
public class Solution {
    public int leastInterval(char[] tasks, int n) {
        int[] counter = new int[26];
        int max = 0;
        int maxCount = 0;
        for(char task : tasks) {
            counter[task - 'A']++;
            if(max == counter[task - 'A']) {
                maxCount++;
            }
            else if(max < counter[task - 'A']) {
                max = counter[task - 'A'];
                maxCount = 1;
            }
        }
        
        int partCount = max - 1;
        int partLength = n - (maxCount - 1);
        int emptySlots = partCount * partLength;
        int availableTasks = tasks.length - max * maxCount;
        int idles = Math.max(0, emptySlots - availableTasks);
        
        return tasks.length + idles;
    }
}
```
</p>


### concise Java Solution O(N) time O(26) space
- Author: fatalme
- Creation Date: Sun Jun 18 2017 11:09:54 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 12:07:05 GMT+0800 (Singapore Standard Time)

<p>
```
// (c[25] - 1) * (n + 1) + 25 - i  is frame size
// when inserting chars, the frame might be "burst", then tasks.length takes precedence
// when 25 - i > n, the frame is already full at construction, the following is still valid.
public class Solution {
    public int leastInterval(char[] tasks, int n) {

        int[] c = new int[26];
        for(char t : tasks){
            c[t - 'A']++;
        }
        Arrays.sort(c);
        int i = 25;
        while(i >= 0 && c[i] == c[25]) i--;

        return Math.max(tasks.length, (c[25] - 1) * (n + 1) + 25 - i);
    }
}
```
First consider the most frequent characters, we can determine their relative positions first and use them as a frame to insert the remaining less frequent characters. Here is a proof by construction:

Let F be the set of most frequent chars with frequency k.
We can create k chunks, each chunk is identical and is a string consists of chars in F in a specific fixed order.
Let the heads of these chunks to be H_i; then H_2 should be at least n chars away from H_1, and so on so forth; then we insert the less frequent chars into the gaps between these chunks sequentially one by one ordered by frequency in a decreasing order and try to fill the k-1 gaps as full or evenly as possible each time you insert a character. **In summary, append the less frequent characters to the end of each chunk of the first k-1 chunks sequentially and round and round, then join the chunks and keep their heads' relative distance from each other to be at least n**.

Examples:

AAAABBBEEFFGG 3

here X represents a space gap:

    Frame: "AXXXAXXXAXXXA"
    insert 'B': "ABXXABXXABXXA" <--- 'B' has higher frequency than the other characters, insert it first.
    insert 'E': "ABEXABEXABXXA"
    insert 'F': "ABEFABEXABFXA" <--- each time try to fill the k-1 gaps as full or evenly as possible.
    insert 'G': "ABEFABEGABFGA"

AACCCBEEE 2

    3 identical chunks "CE", "CE CE CE" <-- this is a frame
    insert 'A' among the gaps of chunks since it has higher frequency than 'B' ---> "CEACEACE"
    insert 'B' ---> "CEABCEACE" <----- result is tasks.length;

AACCCDDEEE 3

    3 identical chunks "CE", "CE CE CE" <--- this is a frame.
    Begin to insert 'A'->"CEA CEA CE"
    Begin to insert 'B'->"CEABCEABCE" <---- result is tasks.length;

ACCCEEE 2

    3 identical chunks "CE", "CE CE CE" <-- this is a frame
    Begin to insert 'A' --> "CEACE CE" <-- result is (c[25] - 1) * (n + 1) + 25 -i = 2 * 3 + 2 = 8
</p>


### C++ 8lines O(n)
- Author: beetlecamera
- Creation Date: Sun Jun 18 2017 12:10:19 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 15:44:28 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    int leastInterval(vector<char>& tasks, int n) {
        unordered_map<char,int>mp;
        int count = 0;
        for(auto e : tasks)
        {
            mp[e]++;
            count = max(count, mp[e]);
        }
        
        int ans = (count-1)*(n+1);
        for(auto e : mp) if(e.second == count) ans++;
        return max((int)tasks.size(), ans);
    }
};
</p>


