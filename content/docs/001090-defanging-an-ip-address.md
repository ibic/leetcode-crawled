---
title: "Defanging an IP Address"
weight: 1090
#id: "defanging-an-ip-address"
---
## Description
<div class="description">
<p>Given a valid (IPv4) IP <code>address</code>, return a defanged version of that IP address.</p>

<p>A <em>defanged&nbsp;IP address</em>&nbsp;replaces every period <code>&quot;.&quot;</code> with <code>&quot;[.]&quot;</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> address = "1.1.1.1"
<strong>Output:</strong> "1[.]1[.]1[.]1"
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> address = "255.100.50.0"
<strong>Output:</strong> "255[.]100[.]50[.]0"
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The given <code>address</code> is a valid IPv4 address.</li>
</ul>
</div>

## Tags
- String (string)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: true)
- Apple - 3 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Adobe - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] 3 One liners + one w/o lib.
- Author: rock
- Creation Date: Sun Jul 07 2019 12:07:06 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 03 2020 02:26:06 GMT+0800 (Singapore Standard Time)

<p>
```java
    public String defangIPaddr(String address) {
        return address.replace(".", "[.]");
    }
    public String defangIPaddr(String address) {
        return String.join("[.]", address.split("\\."));
    }
    public String defangIPaddr(String address) {
        return address.replaceAll("\\.", "[.]");
    }
    public String defangIPaddr(String address) {
        StringBuilder sb = new StringBuilder();
        for (char c : address.toCharArray()) {
            sb.append(c == \'.\' ? "[.]" : c);
        }
        return sb.toString();
    }
```

```python
    def defangIPaddr(self, address: str) -> str:
        return address.replace(\'.\', \'[.]\')
    def defangIPaddr(self, address: str) -> str:
        return \'[.]\'.join(address.split(\'.\'))
    def defangIPaddr(self, address: str) -> str:
        return re.sub(\'\.\', \'[.]\', address)
    def defangIPaddr(self, address: str) -> str:
        return \'\'.join(\'[.]\' if c == \'.\' else c for c in address)
```
</p>


### C++ 1-liner (regex_replace)
- Author: votrubac
- Creation Date: Sun Jul 07 2019 12:02:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 07 2019 12:02:22 GMT+0800 (Singapore Standard Time)

<p>
```
string defangIPaddr(string address) {
  return regex_replace(address, regex("[.]"), "[.]");
}
```
</p>


### Java Solution
- Author: Lhphan
- Creation Date: Thu Jul 11 2019 11:43:01 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 11 2019 11:43:01 GMT+0800 (Singapore Standard Time)

<p>
Here is my solution. Runs in O(n) runtime.
```
class Solution {
    public String defangIPaddr(String address) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < address.length(); i++){
            if (address.charAt(i) == \'.\'){
                str.append("[.]");
            } else {
                str.append(address.charAt(i));
            }
        }
        return str.toString();
    }
}
```
</p>


