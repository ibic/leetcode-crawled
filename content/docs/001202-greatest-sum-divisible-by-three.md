---
title: "Greatest Sum Divisible by Three"
weight: 1202
#id: "greatest-sum-divisible-by-three"
---
## Description
<div class="description">
<p>Given an array&nbsp;<code>nums</code>&nbsp;of integers, we need to find the maximum possible sum of elements of the array such that it is divisible by three.</p>

<ol>
</ol>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,6,5,1,8]
<strong>Output:</strong> 18
<strong>Explanation:</strong> Pick numbers 3, 6, 1 and 8 their sum is 18 (maximum sum divisible by 3).</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [4]
<strong>Output:</strong> 0
<strong>Explanation:</strong> Since 4 is not divisible by 3, do not pick any number.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4,4]
<strong>Output:</strong> 12
<strong>Explanation:</strong> Pick numbers 1, 3, 4 and 4 their sum is 12 (maximum sum divisible by 3).
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 4 * 10^4</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- ByteDance - 4 (taggedByAdmin: false)
- DE Shaw - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] One Pass, O(1) space
- Author: lee215
- Creation Date: Sun Nov 17 2019 12:04:54 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 19 2019 23:16:54 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
`seen[i]` means the current maximum possible sum that `sum % 3 = i`
<br>

## **Complexity**
Time `O(N)`
Space `O(1)`
<br>

**Java**
```java
    public int maxSumDivThree(int[] A) {
        int[] dp = new int[]{0, Integer.MIN_VALUE, Integer.MIN_VALUE};
        for (int a : A) {
            int[] dp2 = new int[3];
            for (int i = 0; i < 3; ++i)
                dp2[(i + a) % 3] = Math.max(dp[(i + a) % 3], dp[i] + a);
            dp = dp2;
        }
        return dp[0];
    }
```
**C++**
```cpp
    int maxSumDivThree(vector<int>& A) {
        vector<int> dp = {0, 0, 0}, dp2;
        for (int a : A) {
            dp2 = dp;
            for (int i: dp2) {
                dp[(i + a) % 3] = max(dp[(i + a) % 3], i + a);
            }
        }
        return dp[0];
    }
```
**Python:**
```python
    def maxSumDivThree(self, A):
        seen = [0, 0, 0]
        for a in A:
            for i in seen[:]:
                seen[(i + a) % 3] = max(seen[(i + a) % 3], i + a)
        return seen[0]
```

</p>


### Java O(N) solution Simple Math O(1) space
- Author: tlj77
- Creation Date: Sun Nov 17 2019 12:11:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Nov 23 2019 05:25:23 GMT+0800 (Singapore Standard Time)

<p>
Add all together, if sum%3==0, return sum. 
if sum%3==1, remove the smallest number which has n%3==1.
if sum%3==2, remove the smallest number which has n%3==2.

one pass, and we need to keep the smallest two numbers that have n1%3==1 and n2%3==2.
```
class Solution {
    public int maxSumDivThree(int[] nums) {
        int res = 0, leftOne = 20000, leftTwo = 20000;
        for(int n:nums){
            res+=n;
            if(n%3==1){
                leftTwo = Math.min(leftTwo,leftOne+n);
                leftOne = Math.min(leftOne,n);
            }
            if(n%3==2) {
                leftOne = Math.min(leftOne,leftTwo+n);
                leftTwo = Math.min(leftTwo,n);
            }
        }
        if(res%3==0) return res;
        if(res%3==1) return res-leftOne;
        return res - leftTwo;
        
    }
}
```
Dp solution for K problem:
```
class Solution {
    public int maxSumDivThree(int[] nums) {
        return maxSumDivK(nums,3);
    }
    public int maxSumDivK(int[] nums, int k){
        if(k==0) return -1;
        int[] dp = new int[k];
        for(int num : nums){
            int tmp[] = Arrays.copyOf(dp,k);
            for(int i=0;i<k;i++){
                dp[(num+tmp[i])%k] = Math.max(dp[(num+tmp[i])%k],num+tmp[i]);
            }
        }
        return dp[0];
    }
}
```
</p>


### Python Math Solution
- Author: davyjing
- Creation Date: Sun Nov 17 2019 12:01:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 12:03:19 GMT+0800 (Singapore Standard Time)

<p>
Divide the whole list into three parts: mod_0, mod_1, mod_2.
Think about the sum of the original list, if it mods 3 == 0, then we can just return the sum.
If tot_sum % 3 == 1, then we should remove one smallest element from mod_1 or two smallest ones from mod_2.
If tot_sum % 3 == 2, then we should remove one smallest element from mod_2 or two smallest ones from mod_1.
```
class Solution:
    def maxSumDivThree(self, nums: List[int]) -> int:
        mod_1, mod_2,res,remove = [], [], 0, float(\'inf\')
        for i in nums:
            if i%3 == 0: res += i
            if i%3 == 1: mod_1 += [i]
            if i %3 == 2: mod_2 += [i]
        mod_1.sort(reverse = True)
        mod_2.sort(reverse = True)
        tmp = sum(mod_1) +sum(mod_2)
        if tmp % 3 == 0:
            return res + tmp
        elif tmp% 3 == 1:
            if len(mod_1): remove = min(remove,mod_1[-1])
            if len(mod_2) > 1: remove = min(mod_2[-1]+mod_2[-2],remove)
        elif tmp % 3 == 2:
            if len(mod_2): remove = min(remove,mod_2[-1])
            if len(mod_1) > 1: remove = min(mod_1[-1]+mod_1[-2],remove)
        return res + tmp - remove
```
</p>


