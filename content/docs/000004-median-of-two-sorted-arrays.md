---
title: "Median of Two Sorted Arrays"
weight: 4
#id: "median-of-two-sorted-arrays"
---
## Description
<div class="description">
<p>Given two sorted arrays <code>nums1</code> and <code>nums2</code> of size <code>m</code> and <code>n</code> respectively, return <strong>the median</strong> of the two sorted arrays.</p>

<p><strong>Follow up:</strong> The overall run time complexity should be <code>O(log (m+n))</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [1,3], nums2 = [2]
<strong>Output:</strong> 2.00000
<strong>Explanation:</strong> merged array = [1,2,3] and median is 2.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [1,2], nums2 = [3,4]
<strong>Output:</strong> 2.50000
<strong>Explanation:</strong> merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [0,0], nums2 = [0,0]
<strong>Output:</strong> 0.00000
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [], nums2 = [1]
<strong>Output:</strong> 1.00000
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [2], nums2 = []
<strong>Output:</strong> 2.00000
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>nums1.length == m</code></li>
	<li><code>nums2.length == n</code></li>
	<li><code>0 &lt;= m &lt;= 1000</code></li>
	<li><code>0 &lt;= n &lt;= 1000</code></li>
	<li><code>1 &lt;= m + n &lt;= 2000</code></li>
	<li><code>-10<sup>6</sup> &lt;= nums1[i], nums2[i] &lt;= 10<sup>6</sup></code></li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)
- Divide and Conquer (divide-and-conquer)

## Companies
- Amazon - 21 (taggedByAdmin: false)
- Goldman Sachs - 15 (taggedByAdmin: false)
- Apple - 13 (taggedByAdmin: true)
- Google - 5 (taggedByAdmin: true)
- Microsoft - 5 (taggedByAdmin: true)
- Facebook - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Adobe - 5 (taggedByAdmin: true)
- Uber - 5 (taggedByAdmin: false)
- Oracle - 4 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: true)
- Reddit - 2 (taggedByAdmin: false)
- VMware - 4 (taggedByAdmin: false)
- Walmart Labs - 4 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Zillow - 3 (taggedByAdmin: false)
- Rubrik - 3 (taggedByAdmin: false)
- Zulily - 3 (taggedByAdmin: false)
- Visa - 3 (taggedByAdmin: false)
- DiDi - 2 (taggedByAdmin: false)
- Huawei - 2 (taggedByAdmin: false)
- Houzz - 2 (taggedByAdmin: false)
- JPMorgan - 2 (taggedByAdmin: false)
- Garena - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- GoDaddy - 2 (taggedByAdmin: false)
- Zenefits - 0 (taggedByAdmin: true)
- Dropbox - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Recursive Approach

To solve this problem, we need to understand "What is the use of median". In statistics, the median is used for:

>Dividing a set into two equal length subsets, that one subset is always greater than the other.

If we understand the use of median for dividing, we are very close to the answer.

First let's cut $$\text{A}$$ into two parts at a random position $$i$$:

```
          left_A             |        right_A
    A[0], A[1], ..., A[i-1]  |  A[i], A[i+1], ..., A[m-1]
```

Since $$\text{A}$$ has $$m$$ elements, so there are $$m+1$$ kinds of cutting ($$i = 0 \sim m$$).

And we know:

>$$\text{len}(\text{left\_A}) = i, \text{len}(\text{right\_A}) = m - i$$.
>
>Note: when $$i = 0$$, $$\text{left\_A}$$ is empty, and when $$i = m$$, $$\text{right\_A}$$ is empty.

With the same way, cut $$\text{B}$$ into two parts at a random position $$j$$:

```

          left_B             |        right_B
    B[0], B[1], ..., B[j-1]  |  B[j], B[j+1], ..., B[n-1]
```

Put $$\text{left\_A}$$ and $$\text{left\_B}$$ into one set, and put $$\text{right\_A}$$ and $$\text{right\_B}$$ into another set. Let's name them $$\text{left\_part}$$ and $$\text{right\_part}$$:

```
          left_part          |        right_part
    A[0], A[1], ..., A[i-1]  |  A[i], A[i+1], ..., A[m-1]
    B[0], B[1], ..., B[j-1]  |  B[j], B[j+1], ..., B[n-1]
```

If we can ensure:

>1. $$\text{len}(\text{left\_part}) = \text{len}(\text{right\_part})$$
>2. $$\max(\text{left\_part}) \leq \min(\text{right\_part})$$

then we divide all elements in $$\{\text{A}, \text{B}\}$$ into two parts with equal length, and one part is always greater than the other. Then

$$
\text{median} = \frac{\text{max}(\text{left}\_\text{part}) + \text{min}(\text{right}\_\text{part})}{2}
$$

To ensure these two conditions, we just need to ensure:

>1. $$i + j = m - i + n - j$$ (or: $$m - i + n - j + 1$$)  
>   if $$n \geq m$$, we just need to set:  $$ \ i = 0 \sim m,\  j = \frac{m + n + 1}{2} - i \\$$  
>  
>  
>2.  $$\text{B}[j-1] \leq \text{A}[i]$$ and $$\text{A}[i-1] \leq \text{B}[j]$$

ps.1 For simplicity, I presume $$\text{A}[i-1], \text{B}[j-1], \text{A}[i], \text{B}[j]$$ are always valid even if $$i=0$$, $$i=m$$, $$j=0$$, or $$j=n$$.
I will talk about how to deal with these edge values at last.

ps.2 Why $$n \geq m$$? Because I have to make sure $$j$$ is non-negative since $$0 \leq i \leq m$$ and $$j = \frac{m + n + 1}{2} - i$$. If $$n < m$$, then $$j$$ may be negative, that will lead to wrong result.

So, all we need to do is:

>Searching $$i$$ in $$[0, m]$$, to find an object $$i$$ such that:
>  
>$$\qquad \text{B}[j-1] \leq \text{A}[i]\ $$ and $$\ \text{A}[i-1] \leq \text{B}[j],\ $$ where $$j = \frac{m + n + 1}{2} - i$$

And we can do a binary search following steps described below:

1. Set $$\text{imin} = 0$$, $$\text{imax} = m$$, then start searching in $$[\text{imin}, \text{imax}]$$
2. Set $$i = \frac{\text{imin} + \text{imax}}{2}$$, $$j = \frac{m + n + 1}{2} - i$$
3. Now we have $$\text{len}(\text{left}\_\text{part})=\text{len}(\text{right}\_\text{part})$$. And there are only 3 situations that we may encounter:  

    - $$\text{B}[j-1] \leq \text{A}[i]$$ and $$\text{A}[i-1] \leq \text{B}[j]$$  
      Means we have found the object $$i$$, so stop searching.  

    - $$\text{B}[j-1] > \text{A}[i]$$  
      Means $$\text{A}[i]$$ is too small. We must adjust $$i$$ to get $$\text{B}[j-1] \leq \text{A}[i]$$.  
      Can we increase $$i$$?  
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yes. Because when $$i$$ is increased, $$j$$ will be decreased.  
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;So $$\text{B}[j-1]$$ is decreased and $$\text{A}[i]$$ is increased, and $$\text{B}[j-1] \leq \text{A}[i]$$ may  
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;be satisfied.  
      Can we decrease $$i$$?  
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No! Because when $$i$$ is decreased, $$j$$ will be increased.  
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;So $$\text{B}[j-1]$$ is increased and $$\text{A}[i]$$ is decreased, and $$\text{B}[j-1] \leq \text{A}[i]$$ will  
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;be never satisfied.  
      So we must increase $$i$$. That is, we must adjust the searching range to $$[i+1, \text{imax}]$$.  
      So, set $$\text{imin} = i+1$$, and goto 2.

    - $$\text{A}[i-1] > \text{B}[j]$$:  
      Means $$\text{A}[i-1]$$ is too big. And we must decrease $$i$$ to get   $$\text{A}[i-1]\leq \text{B}[j]$$.  
      That is, we must adjust the searching range to $$[\text{imin}, i-1]$$.  
      So, set $$\text{imax} = i-1$$, and goto 2.


When the object $$i$$ is found, the median is:

>$$\max(\text{A}[i-1], \text{B}[j-1]), \ $$ when $$m + n$$ is odd

>$$\frac{\max(\text{A}[i-1], \text{B}[j-1]) + \min(\text{A}[i], \text{B}[j])}{2}, \ $$ when $$m + n$$ is even

Now let's consider the edges values $$i=0,i=m,j=0,j=n$$ where $$\text{A}[i-1],\text{B}[j-1],\text{A}[i],\text{B}[j]$$ may not exist.
Actually this situation is easier than you think.

What we need to do is ensuring that $$\text{max}(\text{left}\_\text{part}) \leq \text{min}(\text{right}\_\text{part})$$. So, if $$i$$ and $$j$$ are not edges values (means $$\text{A}[i-1],
\text{B}[j-1],\text{A}[i],\text{B}[j]$$ all exist), then we must check both $$\text{B}[j-1] \leq \text{A}[i]$$ and $$\text{A}[i-1] \leq \text{B}[j]$$.
But if some of $$\text{A}[i-1],\text{B}[j-1],\text{A}[i],\text{B}[j]$$ don't exist, then we don't need to check one (or both) of these two conditions.
For example, if $$i=0$$, then $$\text{A}[i-1]$$ doesn't exist, then we don't need to check $$\text{A}[i-1] \leq \text{B}[j]$$.
So, what we need to do is:

>Searching $$i$$ in $$[0, m]$$, to find an object $$i$$ such that:
>
>$$(j = 0$$ or $$i = m$$ or $$\text{B}[j-1] \leq \text{A}[i])$$ and  
>$$(i = 0$$ or $$j = n$$ or $$\text{A}[i-1] \leq \text{B}[j]),$$  where $$j = \frac{m + n + 1}{2} - i$$

And in a searching loop, we will encounter only three situations:

>1. $$(j = 0$$ or $$i = m$$ or $$\text{B}[j-1] \leq \text{A}[i])$$ and  
    $$(i = 0$$ or $$j = n$$ or $$\text{A}[i-1] \leq \text{B}[j])$$  
    Means $$i$$ is perfect, we can stop searching.
>2. $$j > 0$$ and $$i < m$$ and $$\text{B}[j - 1] > \text{A}[i]$$  
    Means $$i$$ is too small, we must increase it.
>3. $$i > 0$$ and $$j < n$$ and $$\text{A}[i - 1] > \text{B}[j]$$  
    Means $$i$$ is too big, we must decrease it.

Thanks to [@Quentin.chen](https://leetcode.com/Quentin.chen) for pointing out that: $$i < m \implies j > 0$$ and $$i > 0 \implies j < n$$. Because:


>$$m \leq n,\  i < m \implies j = \frac{m+n+1}{2} - i > \frac{m+n+1}{2} - m \geq \frac{2m+1}{2} - m \geq 0$$
>
>$$m \leq n,\  i > 0 \implies j = \frac{m+n+1}{2} - i < \frac{m+n+1}{2} \leq \frac{2n+1}{2} \leq n$$


So in situation 2. and 3. , we don't need to check whether $$j > 0$$ and whether $$j < n$$.

<iframe src="https://leetcode.com/playground/X5mgSxnd/shared" frameBorder="0" width="100%" height="500" name="X5mgSxnd"></iframe>

**Complexity Analysis**

* Time complexity: $$O\big(\log\big(\text{min}(m,n)\big)\big)$$.  
At first, the searching range is $$[0, m]$$.
And the length of this searching range will be reduced by half after each loop.
So, we only need $$\log(m)$$ loops. Since we do constant operations in each loop, so the time complexity is $$O\big(\log(m)\big)$$.
Since $$m \leq n$$, so the time complexity is $$O\big(\log\big(\text{min}(m,n)\big)\big)$$.

* Space complexity: $$O(1)$$.  
We only need constant memory to store $$9$$ local variables, so the space complexity is $$O(1)$$.

## Accepted Submission (python3)
```python3
# -*- coding: UTF-8 -*-
from typing import List

#
# @lc app=leetcode id=4 lang=python3
#
# [4] Median of Two Sorted Arrays
#
# https://leetcode.com/problems/median-of-two-sorted-arrays/description/
#
# algorithms
# Hard (25.68%)
# Total Accepted:    394K
# Total Submissions: 1.5M
# Testcase Example:  '[1,3]\n[2]'
#
# There are two sorted arrays nums1 and nums2 of size m and n respectively.
#
# Find the median of the two sorted arrays. The overall run time complexity
# should be O(log (m+n)).
#
# You may assume nums1 and nums2 cannot be both empty.
#
# Example 1:
#
#
# nums1 = [1, 3]
# nums2 = [2]
#
# The median is 2.0
#
#
# Example 2:
#
#
# nums1 = [1, 2]
# nums2 = [3, 4]
#
# The median is (2 + 3)/2 = 2.5
#
#
#
class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        Inf = float('inf')
        NInf = float('-inf')
        len1 = len(nums1)
        len2 = len(nums2)
        # so that list `b` will always be not shorter than a
        if len1 < len2:
            a = nums1
            b = nums2
        else:
            a = nums2
            b = nums1
        la = len(a)
        lb = len(b)
        # if one of them is empty, easy
        if la == 0:
            mb = lb // 2
            return (b[mb - 1] + b[mb])/2 if lb % 2 == 0 else b[mb]
        # bl/br is for the element on the right:
        # - the right can overflow (==lb, and for a it can be ==la)
        # - in case (la + lb) is odd, ma & mb takes the middle element
        bl = (lb - la) // 2
        br = min((lb + la) // 2, lb - 1)
        # check before looping to avoid overflow
        if b[bl] >= a[-1]:
            if (la + lb) % 2 == 0:
                return (b[bl] + max(NInf if bl == 0 else b[bl-1], a[-1]))/2
            else:
                return b[bl]
        while True: # we do the bl >= br check within the loop, so that we don't miss a final loop
            mb = (bl + br)//2
            ma = (la + lb)//2 - mb
            mbl = mb - 1
            mal = ma - 1
            if bl == br:
                break
            # reaching here, `ma` can't be zero or less,
            # because ma == 0 will trigger the bl >= br condition and exit loop
            if b[mb] < a[mal]:
                bl = mb + 1
            elif b[mbl] > a[ma]:
                br = mb - 1
            else:
                bl = mb
                break

        # bl, ma now are the correct right 2 elements
        if (la + lb) % 2 == 0:
            return (
                min(Inf if ma == la else a[ma], b[bl]) +
                max(NInf if ma == 0 else a[ma-1], b[bl-1]))/2
        else:
            return min(Inf if ma == la else a[ma], b[bl])

# la = [1,2]
# lb = [1,2,3]
# so = Solution()
# print(so.findMedianSortedArrays(la, lb))

```

## Top Discussions
### Share my O(log(min(m,n))) solution with explanation
- Author: MissMary
- Creation Date: Tue Nov 11 2014 10:29:58 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 18 2019 14:38:12 GMT+0800 (Singapore Standard Time)

<p>
To solve this problem, we need to understand "What is the use of median". In statistics, the median is used for `dividing a set into two equal length subsets, that one subset is always greater than the other`. If we understand the use of median for dividing, we are very close to the answer.

First let\'s cut **A** into two parts at a random position **i**:
    
          left_A             |        right_A
    A[0], A[1], ..., A[i-1]  |  A[i], A[i+1], ..., A[m-1]

Since **A** has **m** elements, so there are **m+1** kinds of cutting( **i = 0 ~ m** ). And we know: **len(left\_A) = i, len(right\_A) = m - i** . Note: when **i = 0** , **left\_A** is empty, and when **i = m** , **right\_A** is empty.

With the same way, cut **B** into two parts at a random position **j**:

          left_B             |        right_B
    B[0], B[1], ..., B[j-1]  |  B[j], B[j+1], ..., B[n-1]

Put **left\_A** and **left\_B** into one set, and put **right\_A** and **right\_B** into another set. Let\'s name them **left\_part** and **right\_part** :

          left_part          |        right_part
    A[0], A[1], ..., A[i-1]  |  A[i], A[i+1], ..., A[m-1]
    B[0], B[1], ..., B[j-1]  |  B[j], B[j+1], ..., B[n-1]

If we can ensure:

    1) len(left_part) == len(right_part)
    2) max(left_part) <= min(right_part)

then we divide all elements in **{A, B}** into two parts with equal length, and one part is always greater than the other. Then **median = (max(left\_part) + min(right\_part))/2**.

To ensure these two conditions, we just need to ensure:

    (1) i + j == m - i + n - j (or: m - i + n - j + 1)
        if n >= m, we just need to set: i = 0 ~ m, j = (m + n + 1)/2 - i
    (2) B[j-1] <= A[i] and A[i-1] <= B[j]

ps.1 For simplicity, I presume **A[i-1],B[j-1],A[i],B[j]** are always valid even if **i=0/i=m/j=0/j=n** . I will talk about how to deal with these edge values at last.

ps.2 Why n >= m? Because I have to make sure j is non-nagative since 0 <= i <= m and j = (m + n + 1)/2 - i. If n < m , then j may be nagative, that will lead to wrong result.

So, all we need to do is:

    Searching i in [0, m], to find an object `i` that:
        B[j-1] <= A[i] and A[i-1] <= B[j], ( where j = (m + n + 1)/2 - i )

And we can do a binary search following steps described below:

    <1> Set imin = 0, imax = m, then start searching in [imin, imax]

    <2> Set i = (imin + imax)/2, j = (m + n + 1)/2 - i

    <3> Now we have len(left_part)==len(right_part). And there are only 3 situations
         that we may encounter:
        <a> B[j-1] <= A[i] and A[i-1] <= B[j]
            Means we have found the object `i`, so stop searching.
        <b> B[j-1] > A[i]
            Means A[i] is too small. We must `ajust` i to get `B[j-1] <= A[i]`.
            Can we `increase` i?
                Yes. Because when i is increased, j will be decreased.
                So B[j-1] is decreased and A[i] is increased, and `B[j-1] <= A[i]` may
                be satisfied.
            Can we `decrease` i?
                `No!` Because when i is decreased, j will be increased.
                So B[j-1] is increased and A[i] is decreased, and B[j-1] <= A[i] will
                be never satisfied.
            So we must `increase` i. That is, we must ajust the searching range to
            [i+1, imax]. So, set imin = i+1, and goto <2>.
        <c> A[i-1] > B[j]
            Means A[i-1] is too big. And we must `decrease` i to get `A[i-1]<=B[j]`.
            That is, we must ajust the searching range to [imin, i-1].
            So, set imax = i-1, and goto <2>.

When the object **i** is found, the median is:

    max(A[i-1], B[j-1]) (when m + n is odd)
    or (max(A[i-1], B[j-1]) + min(A[i], B[j]))/2 (when m + n is even)

Now let\'s consider the edges values **i=0,i=m,j=0,j=n** where **A[i-1],B[j-1],A[i],B[j]** may not exist. Actually this situation is easier than you think. 

What we need to do is ensuring that `max(left_part) <= min(right_part)`. So, if **i** and **j** are not edges values(means **A[i-1],B[j-1],A[i],B[j]** all exist), then we must check both **B[j-1] <= A[i]** and **A[i-1] <= B[j]**. But if some of **A[i-1],B[j-1],A[i],B[j]** don\'t exist, then we don\'t need to check one(or both) of these two conditions. For example, if **i=0**, then **A[i-1]** doesn\'t exist, then we don\'t need to check **A[i-1] <= B[j]**. So, what we need to do is:

    Searching i in [0, m], to find an object `i` that:
        (j == 0 or i == m or B[j-1] <= A[i]) and
        (i == 0 or j == n or A[i-1] <= B[j])
        where j = (m + n + 1)/2 - i

And in a searching loop, we will encounter only three situations:

    <a> (j == 0 or i == m or B[j-1] <= A[i]) and
        (i == 0 or j = n or A[i-1] <= B[j])
        Means i is perfect, we can stop searching.

    <b> j > 0 and i < m and B[j - 1] > A[i]
        Means i is too small, we must increase it.

    <c> i > 0 and j < n and A[i - 1] > B[j]
        Means i is too big, we must decrease it.

Thank @Quentin.chen , him pointed out that: `i < m ==> j > 0` and `i > 0 ==> j < n` . Because:

    m <= n, i < m ==> j = (m+n+1)/2 - i > (m+n+1)/2 - m >= (2*m+1)/2 - m >= 0    
    m <= n, i > 0 ==> j = (m+n+1)/2 - i < (m+n+1)/2 <= (2*n+1)/2 <= n

So in situation \<b\> and \<c\>, we don\'t need to check whether `j > 0` and whether `j < n`.

Below is the accepted code:

     def median(A, B):
        m, n = len(A), len(B)
        if m > n:
            A, B, m, n = B, A, n, m
        if n == 0:
            raise ValueError

        imin, imax, half_len = 0, m, (m + n + 1) / 2
        while imin <= imax:
            i = (imin + imax) / 2
            j = half_len - i
            if i < m and B[j-1] > A[i]:
                # i is too small, must increase it
                imin = i + 1
            elif i > 0 and A[i-1] > B[j]:
                # i is too big, must decrease it
                imax = i - 1
            else:
                # i is perfect

                if i == 0: max_of_left = B[j-1]
                elif j == 0: max_of_left = A[i-1]
                else: max_of_left = max(A[i-1], B[j-1])

                if (m + n) % 2 == 1:
                    return max_of_left

                if i == m: min_of_right = B[j]
                elif j == n: min_of_right = A[i]
                else: min_of_right = min(A[i], B[j])

                return (max_of_left + min_of_right) / 2.0

[\u4E2D\u6587\u7FFB\u8BD1](https://zhuanlan.zhihu.com/p/70654378)\uFF08 [@zhgy](https://leetcode.com/zhgy/) \u63D0\u4F9B\uFF09
</p>


### Very concise O(log(min(M,N))) iterative solution with detailed explanation
- Author: stellari
- Creation Date: Mon Jun 22 2015 22:47:22 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 01:26:21 GMT+0800 (Singapore Standard Time)

<p>
This problem is notoriously hard to implement due to all the corner cases. Most implementations consider odd-lengthed and even-lengthed arrays as two different cases and treat them separately. As a matter of fact, with a little mind twist. These two cases can be combined as one, leading to a very simple solution where (almost) no special treatment is needed.

First, let\'s see the concept of \'MEDIAN\' in a slightly unconventional way. That is: 

> "**if we cut the sorted array to two halves of EQUAL LENGTHS, then
> median is the AVERAGE OF Max(lower_half) and Min(upper_half), i.e. the
> two numbers immediately next to the cut**".

For example, for [2 3 5 7], we make the cut between 3 and 5:

    [2 3 / 5 7]

then the median = (3+5)/2. **Note that I\'ll use \'/\' to represent a cut, and (number / number) to represent a cut made through a number in this article**.

for [2 3 4 5 6], we make the cut right through 4 like this:

[2 3 (4/4) 5 7]

Since we split 4 into two halves, we say now both the lower and upper subarray contain 4. This notion also leads to the correct answer: (4 + 4) / 2 = 4;

For convenience, let\'s use L to represent the number immediately left to the cut, and R the right counterpart. In [2 3 5 7], for instance, we have L = 3 and R = 5, respectively. 

We observe the index of L and R have the following relationship with the length of the array N:

    N        Index of L / R
    1               0 / 0
    2               0 / 1
    3               1 / 1  
    4               1 / 2      
    5               2 / 2
    6               2 / 3
    7               3 / 3
    8               3 / 4

It is not hard to conclude that index of L = (N-1)/2, and R is at N/2. Thus, the median can be represented as 

    (L + R)/2 = (A[(N-1)/2] + A[N/2])/2

----------------

To get ready for the two array situation, let\'s add a few imaginary \'positions\' (represented as #\'s) in between numbers, and treat numbers as \'positions\' as well. 

    [6 9 13 18]  ->   [# 6 # 9 # 13 # 18 #]    (N = 4)
    position index     0 1 2 3 4 5  6 7  8     (N_Position = 9)
			  
    [6 9 11 13 18]->   [# 6 # 9 # 11 # 13 # 18 #]   (N = 5)
    position index      0 1 2 3 4 5  6 7  8 9 10    (N_Position = 11)

As you can see, there are always exactly 2*N+1 \'positions\' regardless of length N. Therefore, the middle cut should always be made on the Nth position (0-based). Since index(L) = (N-1)/2 and index(R) = N/2 in this situation, we can infer that **index(L) = (CutPosition-1)/2, index(R) = (CutPosition)/2**. 

------------------------

Now for the two-array case:

    A1: [# 1 # 2 # 3 # 4 # 5 #]    (N1 = 5, N1_positions = 11)
    
    A2: [# 1 # 1 # 1 # 1 #]     (N2 = 4, N2_positions = 9)

Similar to the one-array problem, we need to find a cut that divides the two arrays each into two halves such that 

> "any number in the two left halves" <= "any number in the two right
> halves".

We can also make the following observations\uFF1A

1. There are 2*N1 + 2*N2 + 2 position altogether. Therefore, there must be exactly N1 + N2 positions on each side of the cut, and 2 positions directly on the cut.

2. Therefore, when we cut at position C2 = K in A2, then the cut position in A1 must be C1 = N1 + N2 - k. For instance, if C2 = 2, then we must have C1 = 4 + 5 - C2 = 7.

        [# 1 # 2 # 3 # (4/4) # 5 #]    
    
        [# 1 / 1 # 1 # 1 #]   

3. When the cuts are made, we\'d have two L\'s and two R\'s. They are

        L1 = A1[(C1-1)/2]; R1 = A1[C1/2];
        L2 = A2[(C2-1)/2]; R2 = A2[C2/2];

In the above example, 

        L1 = A1[(7-1)/2] = A1[3] = 4; R1 = A1[7/2] = A1[3] = 4;
        L2 = A2[(2-1)/2] = A2[0] = 1; R2 = A1[2/2] = A1[1] = 1;


Now how do we decide if this cut is the cut we want? Because L1, L2 are the greatest numbers on the left halves and R1, R2 are the smallest numbers on the right, we only need

    L1 <= R1 && L1 <= R2 && L2 <= R1 && L2 <= R2

to make sure that any number in lower halves <= any number in upper halves. As a matter of fact, since 
L1 <= R1 and L2 <= R2 are naturally guaranteed because A1 and A2 are sorted, we only need to make sure:

L1 <= R2 and L2 <= R1.

Now we can use simple binary search to find out the result.

    If we have L1 > R2, it means there are too many large numbers on the left half of A1, then we must move C1 to the left (i.e. move C2 to the right); 
    If L2 > R1, then there are too many large numbers on the left half of A2, and we must move C2 to the left.
    Otherwise, this cut is the right one. 
    After we find the cut, the medium can be computed as (max(L1, L2) + min(R1, R2)) / 2;

Two side notes: 

A. Since C1 and C2 can be mutually determined from each other, we can just move one of them first, then calculate the other accordingly. However, it is much more practical to move C2 (the one on the shorter array) first. The reason is that on the shorter array, all positions are possible cut locations for median, but on the longer array, the positions that are too far left or right are simply impossible for a legitimate cut. For instance, [1], [2 3 4 5 6 7 8]. Clearly the cut between 2 and 3 is impossible, because the shorter array does not have that many elements to balance out the [3 4 5 6 7 8] part if you make the cut this way. Therefore, for the longer array to be used as the basis for the first cut, a range check must be performed. It would be just easier to do it on the shorter array, which requires no checks whatsoever. Also, moving only on the shorter array gives a run-time complexity of O(log(min(N1, N2))) (edited as suggested by @baselRus)

B. The only edge case is when a cut falls on the 0th(first) or the 2*Nth(last) position. For instance, if C2 = 2*N2, then R2 = A2[2*N2/2] = A2[N2], which exceeds the boundary of the array. To solve this problem, we can imagine that both A1 and A2 actually have two extra elements, INT_MAX at A[-1] and INT_MAX at A[N]. These additions don\'t change the result, but make the implementation easier: If any L falls out of the left boundary of the array, then L = INT_MIN, and if any R falls out of the right boundary, then R = INT_MAX.

-----------------

I know that was not very easy to understand, but all the above reasoning eventually boils down to the following concise code:

     double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        int N1 = nums1.size();
        int N2 = nums2.size();
        if (N1 < N2) return findMedianSortedArrays(nums2, nums1);	// Make sure A2 is the shorter one.
        
        int lo = 0, hi = N2 * 2;
        while (lo <= hi) {
            int mid2 = (lo + hi) / 2;   // Try Cut 2 
            int mid1 = N1 + N2 - mid2;  // Calculate Cut 1 accordingly
            
            double L1 = (mid1 == 0) ? INT_MIN : nums1[(mid1-1)/2];	// Get L1, R1, L2, R2 respectively
            double L2 = (mid2 == 0) ? INT_MIN : nums2[(mid2-1)/2];
            double R1 = (mid1 == N1 * 2) ? INT_MAX : nums1[(mid1)/2];
            double R2 = (mid2 == N2 * 2) ? INT_MAX : nums2[(mid2)/2];
            
            if (L1 > R2) lo = mid2 + 1;		// A1\'s lower half is too big; need to move C1 left (C2 right)
            else if (L2 > R1) hi = mid2 - 1;	// A2\'s lower half too big; need to move C2 left.
            else return (max(L1,L2) + min(R1, R2)) / 2;	// Otherwise, that\'s the right cut.
        }
        return -1;
    } 
If you have any suggestions to make the logic and implementation even more cleaner. Please do let me know!
</p>


### Concise JAVA solution based on Binary Search
- Author: Cheng_Zhang
- Creation Date: Mon Nov 02 2015 06:03:08 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 23:52:06 GMT+0800 (Singapore Standard Time)

<p>
**Explanation**

The key point of this problem is to ignore half part of A and B each step recursively by comparing the median of remaining A and B:



```
if (aMid < bMid) Keep [aRight + bLeft]    
else Keep [bRight + aLeft]
```

As the following: **time=O(log(m + n))** 


    public double findMedianSortedArrays(int[] A, int[] B) {
    	    int m = A.length, n = B.length;
    	    int l = (m + n + 1) / 2;
    	    int r = (m + n + 2) / 2;
    	    return (getkth(A, 0, B, 0, l) + getkth(A, 0, B, 0, r)) / 2.0;
    	}
    
    public double getkth(int[] A, int aStart, int[] B, int bStart, int k) {
    	if (aStart > A.length - 1) return B[bStart + k - 1];            
    	if (bStart > B.length - 1) return A[aStart + k - 1];                
    	if (k == 1) return Math.min(A[aStart], B[bStart]);
    	
    	int aMid = Integer.MAX_VALUE, bMid = Integer.MAX_VALUE;
    	if (aStart + k/2 - 1 < A.length) aMid = A[aStart + k/2 - 1]; 
    	if (bStart + k/2 - 1 < B.length) bMid = B[bStart + k/2 - 1];        
    	
    	if (aMid < bMid) 
    	    return getkth(A, aStart + k/2, B, bStart,       k - k/2);// Check: aRight + bLeft 
    	else 
    	    return getkth(A, aStart,       B, bStart + k/2, k - k/2);// Check: bRight + aLeft
    }
</p>


