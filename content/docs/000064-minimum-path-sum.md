---
title: "Minimum Path Sum"
weight: 64
#id: "minimum-path-sum"
---
## Description
<div class="description">
<p>Given a <em>m</em> x <em>n</em> grid filled with non-negative numbers, find a path from top left to bottom right which <em>minimizes</em> the sum of all numbers along its path.</p>

<p><strong>Note:</strong> You can only move either down or right at any point in time.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong>
[
&nbsp; [1,3,1],
  [1,5,1],
  [4,2,1]
]
<strong>Output:</strong> 7
<strong>Explanation:</strong> Because the path 1&rarr;3&rarr;1&rarr;1&rarr;1 minimizes the sum.
</pre>

</div>

## Tags
- Array (array)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 12 (taggedByAdmin: false)
- Goldman Sachs - 8 (taggedByAdmin: false)
- Bloomberg - 5 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Dropbox - 3 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)
- Rubrik - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary

We have to find the minimum sum of numbers over a path from the top left to the bottom right of the given matrix .

## Solution

---
#### Approach 1: Brute Force

The Brute Force approach involves recursion. For each element, we consider two paths, rightwards and downwards and find the minimum sum out of those two. It specifies whether we need to take a right step or downward step to minimize the sum.

$$
\mathrm{cost}(i, j)=\mathrm{grid}[i][j] + \min \big(\mathrm{cost}(i+1, j), \mathrm{cost}(i, j+1) \big)
$$


<iframe src="https://leetcode.com/playground/trbHWoJe/shared" frameBorder="0" width="100%" height="225" name="trbHWoJe"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(2^{m+n}\big)$$. For every move, we have atmost 2 options.
* Space complexity : $$O(m+n)$$. Recursion of depth $$m+n$$.
<br />
<br />
---
#### Approach 2: Dynamic Programming 2D

**Algorithm**

We use an extra matrix $$dp$$ of the same size as the original matrix. In this matrix, $$dp(i, j)$$ represents the minimum sum of the path from the index $$(i, j)$$ to
the bottom rightmost element. We start by initializing the bottom rightmost element
of $$dp$$ as the last element of the given matrix. Then for each element starting from
the bottom right, we traverse backwards and fill in the matrix with the required
minimum sums. Now, we need to note that at every element, we can move either
rightwards or downwards. Therefore, for filling in the minimum sum, we use the
equation:

$$
dp(i, j)= \mathrm{grid}(i,j)+\min\big(dp(i+1,j),dp(i,j+1)\big)
$$

taking care of the boundary conditions.

The following figure illustrates the process:
<!--![Minimum Path Sum](https://leetcode.com/media/original_images/64_Minimum_Path_Sum.gif)-->
!?!../Documents/64_Minimum_Path_Sum.json:859,390!?!

<iframe src="https://leetcode.com/playground/TL2RboTL/shared" frameBorder="0" width="100%" height="361" name="TL2RboTL"></iframe>

**Complexity Analysis**

* Time complexity : $$O(mn)$$. We traverse the entire matrix once.

* Space complexity : $$O(mn)$$. Another matrix of the same size is used.
<br />
<br />
---
#### Approach 3: Dynamic Programming 1D

**Algorithm**

In the previous case, instead of using a 2D matrix for dp, we can do the same
work using a $$dp$$ array of the row size, since for making the current entry all we need is the dp entry for the bottom and
 the right element. Thus,
we start by initializing only the last element of the array as the last element of the given matrix.
The last entry is the bottom rightmost element of the given matrix. Then, we start
moving towards the left and update the entry $$dp(j)$$ as:

$$
dp(j)=\mathrm{grid}(i,j)+\min\big(dp(j),dp(j+1)\big)
$$

We repeat the same process for every row as we move upwards. At the end $$dp(0)$$ gives the
 required minimum sum.


 <iframe src="https://leetcode.com/playground/HSZpohSU/shared" frameBorder="0" width="100%" height="378" name="HSZpohSU"></iframe>

**Complexity Analysis**

* Time complexity : $$O(mn)$$. We traverse the entire matrix once.

* Space complexity : $$O(n)$$. Another array of row size is used.
<br />
<br />
---
#### Approach 4: Dynamic Programming (Without Extra Space)

**Algorithm**

This approach is same as [Approach 2](#approach-2-dynamic-programming-2d), with a slight difference. Instead of using
another $$dp$$ matrix. We can store the minimum sums in the original matrix itself,
since we need not retain the original matrix here. Thus, the governing equation
now becomes:

$$
\mathrm{grid}(i, j)=\mathrm{grid}(i,j)+\min \big(\mathrm{grid}(i+1,j), \mathrm{grid}(i,j+1)\big)
$$

<iframe src="https://leetcode.com/playground/FsCWyoPx/shared" frameBorder="0" width="100%" height="310" name="FsCWyoPx"></iframe>

**Complexity Analysis**

* Time complexity : $$O(mn)$$. We traverse the entire matrix once.

* Space complexity : $$O(1)$$. No extra space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ DP
- Author: jianchao-li
- Creation Date: Tue Jun 02 2015 16:46:23 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 06:03:50 GMT+0800 (Singapore Standard Time)

<p>
This is a typical DP problem. Suppose the minimum path sum of arriving at point `(i, j)` is `S[i][j]`, then the state equation is `S[i][j] = min(S[i - 1][j], S[i][j - 1]) + grid[i][j]`.

Well, some boundary conditions need to be handled. The boundary conditions happen on the topmost row (`S[i - 1][j]` does not exist) and the leftmost column (`S[i][j - 1]` does not exist). Suppose `grid` is like `[1, 1, 1, 1]`, then the minimum sum to arrive at each point is simply an accumulation of previous points and the result is `[1, 2, 3, 4]`.

Now we can write down the following (unoptimized) code.

    class Solution {
    public:
        int minPathSum(vector<vector<int>>& grid) {
            int m = grid.size();
            int n = grid[0].size(); 
            vector<vector<int> > sum(m, vector<int>(n, grid[0][0]));
            for (int i = 1; i < m; i++)
                sum[i][0] = sum[i - 1][0] + grid[i][0];
            for (int j = 1; j < n; j++)
                sum[0][j] = sum[0][j - 1] + grid[0][j];
            for (int i = 1; i < m; i++)
                for (int j = 1; j < n; j++)
                    sum[i][j]  = min(sum[i - 1][j], sum[i][j - 1]) + grid[i][j];
            return sum[m - 1][n - 1];
        }
    };

As can be seen, each time when we update `sum[i][j]`, we only need `sum[i - 1][j]` (at the current column) and `sum[i][j - 1]` (at the left column). So we need not maintain the full `m*n` matrix. Maintaining two columns is enough and now we have the following code.

    class Solution {
    public:
        int minPathSum(vector<vector<int>>& grid) {
            int m = grid.size();
            int n = grid[0].size();
            vector<int> pre(m, grid[0][0]);
            vector<int> cur(m, 0);
            for (int i = 1; i < m; i++)
                pre[i] = pre[i - 1] + grid[i][0];
            for (int j = 1; j < n; j++) { 
                cur[0] = pre[0] + grid[0][j]; 
                for (int i = 1; i < m; i++)
                    cur[i] = min(cur[i - 1], pre[i]) + grid[i][j];
                swap(pre, cur); 
            }
            return pre[m - 1];
        }
    };

Further inspecting the above code, it can be seen that maintaining `pre` is for recovering `pre[i]`, which is simply `cur[i]` before its update. So it is enough to use only one vector. Now the space is further optimized and the code also gets shorter.

    class Solution {
    public:
        int minPathSum(vector<vector<int>>& grid) {
            int m = grid.size();
            int n = grid[0].size();
            vector<int> cur(m, grid[0][0]);
            for (int i = 1; i < m; i++)
                cur[i] = cur[i - 1] + grid[i][0]; 
            for (int j = 1; j < n; j++) {
                cur[0] += grid[0][j]; 
                for (int i = 1; i < m; i++)
                    cur[i] = min(cur[i - 1], cur[i]) + grid[i][j];
            }
            return cur[m - 1];
        }
    };
</p>


### DP with O(N*N) space complexity
- Author: wdj0xda
- Creation Date: Thu Nov 27 2014 03:59:17 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 11:31:31 GMT+0800 (Singapore Standard Time)

<p>
	public int minPathSum(int[][] grid) {
		int m = grid.length;// row
		int n = grid[0].length; // column
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0 && j != 0) {
					grid[i][j] = grid[i][j] + grid[i][j - 1];
				} else if (i != 0 && j == 0) {
					grid[i][j] = grid[i][j] + grid[i - 1][j];
				} else if (i == 0 && j == 0) {
					grid[i][j] = grid[i][j];
				} else {
					grid[i][j] = Math.min(grid[i][j - 1], grid[i - 1][j])
							+ grid[i][j];
				}
			}
		}

		return grid[m - 1][n - 1];
	}
</p>


### Simple python dp 70ms
- Author: luanmaova
- Creation Date: Fri May 08 2015 16:05:21 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 19:19:13 GMT+0800 (Singapore Standard Time)

<p>


    def minPathSum(self, grid):
        m = len(grid)
        n = len(grid[0])
        for i in range(1, n):
            grid[0][i] += grid[0][i-1]
        for i in range(1, m):
            grid[i][0] += grid[i-1][0]
        for i in range(1, m):
            for j in range(1, n):
                grid[i][j] += min(grid[i-1][j], grid[i][j-1])
        return grid[-1][-1]
</p>


