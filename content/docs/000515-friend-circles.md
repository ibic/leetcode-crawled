---
title: "Friend Circles"
weight: 515
#id: "friend-circles"
---
## Description
<div class="description">
<p>There are <b>N</b> students in a class. Some of them are friends, while some are not. Their friendship is transitive in nature. For example, if A is a <b>direct</b> friend of B, and B is a <b>direct</b> friend of C, then A is an <b>indirect</b> friend of C. And we defined a friend circle is a group of students who are direct or indirect friends.</p>

<p>Given a <b>N*N</b> matrix <b>M</b> representing the friend relationship between students in the class. If M[i][j] = 1, then the i<sub>th</sub> and j<sub>th</sub> students are <b>direct</b> friends with each other, otherwise not. And you have to output the total number of friend circles among all the students.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> 
[[1,1,0],
 [1,1,0],
 [0,0,1]]
<b>Output:</b> 2
<b>Explanation:</b>The 0<sub>th</sub> and 1<sub>st</sub> students are direct friends, so they are in a friend circle. 
The 2<sub>nd</sub> student himself is in a friend circle. So return 2.
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> 
[[1,1,0],
 [1,1,1],
 [0,1,1]]
<b>Output:</b> 1
<b>Explanation:</b>The 0<sub>th</sub> and 1<sub>st</sub> students are direct friends, the 1<sub>st</sub> and 2<sub>nd</sub> students are direct friends, 
so the 0<sub>th</sub> and 2<sub>nd</sub> students are indirect friends. All of them are in the same friend circle, so return 1.

</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= N &lt;= 200</code></li>
	<li><code>M[i][i] == 1</code></li>
	<li><code>M[i][j] == M[j][i]</code></li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Union Find (union-find)

## Companies
- Goldman Sachs - 18 (taggedByAdmin: false)
- Dropbox - 9 (taggedByAdmin: false)
- Amazon - 7 (taggedByAdmin: false)
- Two Sigma - 5 (taggedByAdmin: true)
- Facebook - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Pocket Gems - 12 (taggedByAdmin: false)
- Twitter - 4 (taggedByAdmin: false)
- Atlassian - 3 (taggedByAdmin: false)
- LinkedIn - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Drawbridge - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Bloomberg - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Using Depth First Search[Accepted]

**Algorithm**

The given matrix can be viewed as the Adjacency Matrix of a graph. By viewing the matrix in such a manner, our problem reduces
to the problem of finding the number of connected components in an undirected graph. In order to understand the above statement, consider the
example matrix below:

```
M= [1 1 0 0 0 0

    1 1 0 0 0 0

    0 0 1 1 1 0

    0 0 1 1 0 0

    0 0 1 0 1 0

    0 0 0 0 0 1]

```

If we view this matrix M as the adjancency matrix of a graph, the following graph is formed:

![Friend_Circles](../Figures/547/547_Friend_Circles_1.jpg)

In this graph, the node numbers represent the indices in the matrix M and an edge exists between the nodes numbered $$i$$ and $$j$$,
if there is a 1 at the corresponding $$M[i][j]$$.

In order to find the number of connected components in an undirected graph, one of the simplest methods is to make use of Depth First Search
starting from every node. We make use of $$visited$$ array of size $$N$$($$M$$ is of size $$NxN$$). This $$visited[i]$$ element
is used to indicate that the $$i^{th}$$ node has already been visited while undergoing a Depth First Search from some node.

To undergo DFS, we pick up a node and visit all its directly connected nodes. But, as soon as we visit any of those nodes, we recursively apply the same process to them as well. Thus, we try to go as deeper into the levels of the graph as possible starting from a current node first, leaving the other direct neighbour nodes to be visited later on.

The depth first search for an arbitrary graph is shown below:

<!--![Friend_Circles](../Figures/547/547_Friend_Circles2.gif)-->

!?!../Documents/547_Friend_Circles_dfs.json:1000,563!?!

From the graph, we can see that the components which are connected can be reached starting from any single node of the connected group.
Thus, to find the number of connected components, we start from every node which isn't visited right now and apply DFS starting with it.
We increment the $$count$$ of connected components for every new starting node.


<iframe src="https://leetcode.com/playground/YcWQPXNS/shared" frameBorder="0" name="YcWQPXNS" width="100%" height="445"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. The complete matrix of size $$n^2$$ is traversed.

* Space complexity : $$O(n)$$. $$visited$$ array of size $$n$$ is used.

---

#### Approach #2 Using Breadth First Search[Accepted]

**Algorithm**

As discussed in the above method, if we view the given matrix as an adjacency matrix of a graph, we can use graph algorithms easily to find the number of connected components. This approach makes use of Breadth First Search for a graph.

In case of Breadth First Search, we start from a particular node and visit all its directly connected nodes first. After all the direct neighbours have been visited, we apply the same process to the neighbour nodes as well. Thus, we exhaust the nodes of a graph on a level by level basis. An example of Breadth First Search is shown below:

<!--![Friend_Circles](../Figures/547/547_Friend_Circles3.gif)-->

!?!../Documents/547_Friend_Circles_bfs.json:1000,563!?!

In this case also, we apply BFS starting from one of the nodes. We make use of a $$visited$$ array to keep a track of the already visited nodes. We increment the $$count$$ of connected components whenever we need to start off with a new node as the root node for applying BFS which hasn't been already visited.

<iframe src="https://leetcode.com/playground/srJD42Lf/shared" frameBorder="0" name="srJD42Lf" width="100%" height="428"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. The complete matrix of size $$n^2$$ is traversed.

* Space complexity : $$O(n)$$. A $$queue$$ and $$visited$$ array of size $$n$$ is used.

---


#### Approach #3 Using Union-Find Method[Accepted]

**Algorithm**

Another method that can be used to determine the number of connected components in a graph is the union find method. The method is simple.

We make use of a $$parent$$ array of size $$N$$. We traverse over all the nodes of the graph. For every node traversed, we traverse over all the nodes directly connected to it and assign them to a single group which is represented by their $$parent$$ node. This process is called forming a $$union$$. Every group has a single $$parent$$ node, whose own parent is given by $$\text{-1}$$.

For every new pair of nodes found, we look for the parents of both the nodes. If the parents nodes are the same, it indicates that they have already been united into the same group. If the parent nodes differ, it means they are yet to be united. Thus, for the pair of nodes $$(x, y)$$, while forming the union, we assign $$parent\big[parent[x]\big]=parent[y]$$, which ultimately combines them into the same group.

The following animation depicts the process for a simple matrix:

<!--![Friend_Circles](../Figures/547/547_Friend_Circles4.gif)-->

!?!../Documents/547_Friend_Circles_union.json:1000,563!?!

At the end, we find the number of groups, or the number of parent nodes. Such nodes have their parents indicated by a $$\text{-1}$$. This gives us the required count.


<iframe src="https://leetcode.com/playground/LC2i2T7b/shared" frameBorder="0" name="LC2i2T7b" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^3)$$. We traverse over the complete matrix once. Union and find operations take $$O(n)$$ time in the worst case.
* Space complexity : $$O(n)$$. $$parent$$ array of size $$n$$ is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Neat DFS java solution
- Author: vinod23
- Creation Date: Sun Apr 02 2017 11:16:47 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 16:12:16 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public void dfs(int[][] M, int[] visited, int i) {
        for (int j = 0; j < M.length; j++) {
            if (M[i][j] == 1 && visited[j] == 0) {
                visited[j] = 1;
                dfs(M, visited, j);
            }
        }
    }
    public int findCircleNum(int[][] M) {
        int[] visited = new int[M.length];
        int count = 0;
        for (int i = 0; i < M.length; i++) {
            if (visited[i] == 0) {
                dfs(M, visited, i);
                count++;
            }
        }
        return count;
    }
}
</p>


### Java solution, Union Find
- Author: shawngao
- Creation Date: Sun Apr 02 2017 11:26:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 16:10:09 GMT+0800 (Singapore Standard Time)

<p>
This is a typical ```Union Find``` problem. I abstracted it as a standalone class. Remember the template, you will be able to use it later.
```
public class Solution {
    class UnionFind {
        private int count = 0;
        private int[] parent, rank;
        
        public UnionFind(int n) {
            count = n;
            parent = new int[n];
            rank = new int[n];
            for (int i = 0; i < n; i++) {
                parent[i] = i;
            }
        }
        
        public int find(int p) {
        	while (p != parent[p]) {
                parent[p] = parent[parent[p]];    // path compression by halving
                p = parent[p];
            }
            return p;
        }
        
        public void union(int p, int q) {
            int rootP = find(p);
            int rootQ = find(q);
            if (rootP == rootQ) return;
            if (rank[rootQ] > rank[rootP]) {
                parent[rootP] = rootQ;
            }
            else {
                parent[rootQ] = rootP;
                if (rank[rootP] == rank[rootQ]) {
                    rank[rootP]++;
                }
            }
            count--;
        }
        
        public int count() {
            return count;
        }
    }
    
    public int findCircleNum(int[][] M) {
        int n = M.length;
        UnionFind uf = new UnionFind(n);
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (M[i][j] == 1) uf.union(i, j);
            }
        }
        return uf.count();
    }
}
```
</p>


### Python, Simple Explanation
- Author: awice
- Creation Date: Sun Apr 02 2017 11:35:31 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 10:46:54 GMT+0800 (Singapore Standard Time)

<p>
From some source, we can visit every connected node to it with a simple DFS.  As is the case with DFS's, **seen** will keep track of nodes that have been visited.  

For every node, we can visit every node connected to it with this DFS, and increment our answer as that represents one friend circle (connected component.)

```
def findCircleNum(self, A):
    N = len(A)
    seen = set()
    def dfs(node):
        for nei, adj in enumerate(A[node]):
            if adj and nei not in seen:
                seen.add(nei)
                dfs(nei)
    
    ans = 0
    for i in xrange(N):
        if i not in seen:
            dfs(i)
            ans += 1
    return ans
```
</p>


