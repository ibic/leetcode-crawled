---
title: "Implement Queue using Stacks"
weight: 216
#id: "implement-queue-using-stacks"
---
## Description
<div class="description">
<p>Implement a first in first out (FIFO) queue using only two stacks. The implemented queue should support all the functions of a normal queue (<code>push</code>, <code>peek</code>, <code>pop</code>, and <code>empty</code>).</p>

<p>Implement the <code>MyQueue</code> class:</p>

<ul>
	<li><code>void push(int x)</code> Pushes element x to the back of the queue.</li>
	<li><code>int pop()</code> Removes the element from the front of the queue and returns it.</li>
	<li><code>int peek()</code> Returns the element at the front of the queue.</li>
	<li><code>boolean empty()</code> Returns <code>true</code> if the queue is empty, <code>false</code> otherwise.</li>
</ul>

<p><b>Notes:</b></p>

<ul>
	<li>You must use <strong>only</strong> standard operations of a stack, which means only <code>push to top</code>, <code>peek/pop from top</code>, <code>size</code>, and <code>is empty</code> operations are valid.</li>
	<li>Depending on your language, the stack may not be supported natively. You may simulate a stack using a list or deque (double-ended queue) as long as you use only a stack&#39;s standard operations.</li>
</ul>

<p><strong>Follow-up:</strong> Can you implement the queue such that each operation is <strong><a href="https://en.wikipedia.org/wiki/Amortized_analysis" target="_blank">amortized</a></strong> <code>O(1)</code> time complexity? In other words, performing <code>n</code> operations will take overall <code>O(n)</code> time even if one of those operations may take longer.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;MyQueue&quot;, &quot;push&quot;, &quot;push&quot;, &quot;peek&quot;, &quot;pop&quot;, &quot;empty&quot;]
[[], [1], [2], [], [], []]
<strong>Output</strong>
[null, null, null, 1, 1, false]

<strong>Explanation</strong>
MyQueue myQueue = new MyQueue();
myQueue.push(1); // queue is: [1]
myQueue.push(2); // queue is: [1, 2] (leftmost is front of the queue)
myQueue.peek(); // return 1
myQueue.pop(); // return 1, queue is [2]
myQueue.empty(); // return false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= x &lt;= 9</code></li>
	<li>At most <code>100</code>&nbsp;calls will be made to <code>push</code>, <code>pop</code>, <code>peek</code>, and <code>empty</code>.</li>
	<li>All the calls to <code>pop</code> and <code>peek</code> are valid.</li>
</ul>

</div>

## Tags
- Stack (stack)
- Design (design)

## Companies
- Microsoft - 5 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Mathworks - 4 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary
This article is for beginners. It introduces the following ideas:
Queue, Stack.

## Solution

Queue is **FIFO** (first in - first out) data structure, in which the elements are inserted from one side - `rear` and removed from the other - `front`.
The most intuitive way to implement it is with linked lists, but this article will introduce another approach  using stacks.
Stack is **LIFO** (last in - first out) data structure, in which elements are added and removed from the same end, called `top`.
To satisfy **FIFO** property of a queue we need to keep two stacks. They serve to reverse arrival order of the  elements and one of them store the queue elements in their final order.

---
#### Approach #1 (Two Stacks) Push - $$O(n)$$ per operation, Pop - $$O(1)$$ per operation.

**Algorithm**

**Push**

A queue is FIFO (first-in-first-out) but a stack is LIFO (last-in-first-out). This means the newest element must be pushed to the bottom of the stack. To do so we first transfer all `s1` elements to auxiliary stack `s2`. Then the newly arrived element is pushed on top of `s2` and all its elements are popped and pushed to `s1`.

![Push an element in queue](https://leetcode.com/media/original_images/232_queue_using_stacksBPush.png){:width="539px"}
{:align="center"}

*Figure 1. Push an element in queue*
{:align="center"}

**Java**

```java
private int front;

public void push(int x) {
    if (s1.empty())
        front = x;
    while (!s1.isEmpty())
        s2.push(s1.pop());
    s2.push(x);
    while (!s2.isEmpty())
        s1.push(s2.pop());
}
```

**Complexity Analysis**

* Time complexity : $$O(n)$$.

 Each element, with the exception of the newly arrived, is pushed and popped twice. The last inserted element is popped and pushed once. Therefore this gives  $$4 n + 2$$  operations where $$n$$ is the queue size. The  `push` and `pop` operations have $$O(1)$$ time complexity.

* Space complexity : $$O(n)$$.
We need additional memory to store the queue elements

**Pop**

The algorithm pops an element from  the stack `s1`, because `s1` stores always on its top the first inserted element in the queue.
The front element of the queue is kept as `front`.

![Pop an element from queue](https://leetcode.com/media/original_images/232_queue_using_stacksBPop.png){:width="539px"}
{:align="center"}

*Figure 2. Pop an element from queue*
{:align="center"}

**Java**

```java

// Removes the element from the front of queue.
public void pop() {
    s1.pop();
    if (!s1.empty())
        front = s1.peek();
}
```

**Complexity Analysis**

* Time complexity : $$O(1)$$.
* Space complexity : $$O(1)$$.

**Empty**

Stack `s1` contains all stack elements, so the algorithm checks `s1` size to return if the queue is empty.

```java
// Return whether the queue is empty.
public boolean empty() {
    return s1.isEmpty();
}
```

Time complexity : $$O(1)$$.

Space complexity : $$O(1)$$.

**Peek**

The `front` element is kept in constant memory and is modified when we push or pop an element.

```java
// Get the front element.
public int peek() {
  return front;
}
```

Time complexity : $$O(1)$$.
The `front` element has been calculated in advance and only returned in `peek` operation.

Space complexity : $$O(1)$$.

---
#### Approach #2 (Two Stacks) Push - $$O(1)$$ per operation, Pop - Amortized $$O(1)$$ per operation.

**Algorithm**

**Push**

The newly arrived element is always added on top of stack `s1` and the first element is kept as `front` queue element

![Push an element in queue](https://leetcode.com/media/original_images/232_queue_using_stacksAPush.png){:width="539px"}
{:align="center"}

*Figure 3. Push an element in queue*
{:align="center"}

**Java**

```java

private Stack<Integer> s1 = new Stack<>();
private Stack<Integer> s2 = new Stack<>();

// Push element x to the back of queue.
public void push(int x) {
    if (s1.empty())
        front = x;
    s1.push(x);
}
```

**Complexity Analysis**

* Time complexity : $$O(1)$$.

 Аppending an element to a stack is an O(1) operation.

* Space complexity : $$O(n)$$.
We need additional memory to store the queue elements

**Pop**

We have to remove element in front of the queue. This is the first inserted element in the stack `s1` and it is positioned at the bottom of the stack because of stack's `LIFO (last in - first out)` policy. To remove the bottom element  from  `s1`, we have to pop all elements from `s1` and to push them on to an additional stack `s2`, which helps us to store the elements of `s1` in reversed order. This way  the bottom element of `s1` will be positioned on top of `s2` and we can simply pop it from stack `s2`. Once `s2` is empty, the algorithm transfer data from `s1` to `s2` again.

![Pop an element from stack](https://leetcode.com/media/original_images/232_queue_using_stacksAPop.png){:width="539px"}
{:align="center"}

*Figure 4. Pop an element from stack*
{:align="center"}

**Java**

```java
// Removes the element from in front of queue.
public void pop() {
    if (s2.isEmpty()) {
        while (!s1.isEmpty())
            s2.push(s1.pop());
    }
    s2.pop();    
}
```

**Complexity Analysis**

* Time complexity: Amortized $$O(1)$$, Worst-case $$O(n)$$.

In the worst case scenario when stack `s2` is empty, the algorithm pops $$n$$ elements from stack s1 and pushes $$n$$ elements to `s2`, where $$n$$ is the queue size. This gives $$2n$$ operations, which is $$O(n)$$. But when stack `s2` is not empty the algorithm has $$O(1)$$ time complexity. So what does it mean by Amortized $$O(1)$$? Please see the next section on Amortized Analysis for more information.

* Space complexity : $$O(1)$$.

**Amortized Analysis**

Amortized analysis gives the average performance (over time) of each operation in the worst case. The basic idea is that a worst case operation can alter the state in such a way that the worst case cannot occur again for a long time, thus amortizing its cost.

Consider this example where we start with an empty queue with the following sequence of operations applied:

$$
push_1, push_2, \ldots, push_n, pop_1,pop_2 \ldots, pop_n
$$

The worst case time complexity of a single pop operation is $$O(n)$$. Since we have $$n$$ pop operations, using the worst-case per operation analysis gives us a total of $$O(n^2)$$ time.

However, in a sequence of operations the worst case does not occur often in each operation - some operations may be cheap, some may be expensive. Therefore, a traditional worst-case per operation analysis can give overly pessimistic bound. For example, in a dynamic array only some inserts take a linear time, though others - a constant time.

In the example above, the number of times pop operation can be called is limited by the number of push operations before it. Although a single pop operation could be expensive, it is expensive only once per `n` times (queue size), when `s2` is empty and there is a need for data transfer between `s1` and `s2`. Hence the total time complexity of the sequence is : `n` (for push operations) + `2*n` (for first pop operation) + `n - 1` ( for pop operations) which is $$O(2*n)$$.This gives $$O(2n/2n)$$ = $$O(1)$$ average time per operation.

**Empty**

Both stacks `s1` and `s2` contain all stack elements, so the algorithm checks `s1` and `s2` size to return if the queue is empty.

```java

// Return whether the queue is empty.
public boolean empty() {
    return s1.isEmpty() && s2.isEmpty();
}
```

Time complexity : $$O(1)$$.

Space complexity : $$O(1)$$.


**Peek**

The `front` element is kept in constant memory and is modified when we push an element. When `s2` is not empty, front element is positioned on the top of `s2`

```java
// Get the front element.
public int peek() {
    if (!s2.isEmpty()) {
            return s2.peek();
    }
    return front;
}
```

Time complexity : $$O(1)$$.

The `front` element was either previously calculated or returned as a top element of stack `s2`. Therefore complexity is $$O(1)$$

Space complexity : $$O(1)$$.

Analysis written by: @elmirap.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short O(1) amortized, C++ / Java / Ruby
- Author: StefanPochmann
- Creation Date: Tue Jul 07 2015 05:38:21 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 08:45:55 GMT+0800 (Singapore Standard Time)

<p>
I have one input stack, onto which I push the incoming elements, and one output stack, from which I peek/pop. I move elements from input stack to output stack when needed, i.e., when I need to peek/pop but the output stack is empty. When that happens, I move all elements from input to output stack, thereby reversing the order so it's the correct order for peek/pop.

The loop in `peek` does the moving from input to output stack. Each element only ever gets moved like that once, though, and only after we already spent time pushing it, so the overall amortized cost for each operation is O(1).

**Ruby**

    class Queue
        def initialize
            @in, @out = [], []
        end
    
        def push(x)
            @in << x
        end
    
        def pop
            peek
            @out.pop
        end
    
        def peek
            @out << @in.pop until @in.empty? if @out.empty?
            @out.last
        end
    
        def empty
            @in.empty? && @out.empty?
        end
    end

**Java**

    class MyQueue {
    
        Stack<Integer> input = new Stack();
        Stack<Integer> output = new Stack();
        
        public void push(int x) {
            input.push(x);
        }
    
        public void pop() {
            peek();
            output.pop();
        }
    
        public int peek() {
            if (output.empty())
                while (!input.empty())
                    output.push(input.pop());
            return output.peek();
        }
    
        public boolean empty() {
            return input.empty() && output.empty();
        }
    }

**C++**

    class Queue {
        stack<int> input, output;
    public:
    
        void push(int x) {
            input.push(x);
        }
    
        void pop(void) {
            peek();
            output.pop();
        }
    
        int peek(void) {
            if (output.empty())
                while (input.size())
                    output.push(input.top()), input.pop();
            return output.top();
        }
    
        bool empty(void) {
            return input.empty() && output.empty();
        }
    };
</p>


### Share my python solution (32ms)
- Author: morrischen2008
- Creation Date: Sun Oct 04 2015 09:34:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2015 09:34:24 GMT+0800 (Singapore Standard Time)

<p>
The idea is to simulate a queue using two stacks (same as previous posts). I use python list as the underlying data structure for stack and add a "move()" method to simplify code: it moves all elements of the "inStack" to the "outStack" when the "outStack" is empty. Here is the code


    class Queue(object):
        def __init__(self):
            """
            initialize your data structure here.
            """
            self.inStack, self.outStack = [], []
    
        def push(self, x):
            """
            :type x: int
            :rtype: nothing
            """
            self.inStack.append(x)
    
        def pop(self):
            """
            :rtype: nothing
            """
            self.move()
            self.outStack.pop()
    
        def peek(self):
            """
            :rtype: int
            """
            self.move()
            return self.outStack[-1]
    
        def empty(self):
            """
            :rtype: bool
            """
            return (not self.inStack) and (not self.outStack) 
            
        def move(self):
            """
            :rtype nothing
            """
            if not self.outStack:
                while self.inStack:
                    self.outStack.append(self.inStack.pop())
</p>


### Easy Java solution, just edit push() method
- Author: yangneu2015
- Creation Date: Sun Nov 01 2015 00:57:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 12:33:22 GMT+0800 (Singapore Standard Time)

<p>
    class MyQueue {
    Stack<Integer> queue = new Stack<Integer>();
    // Push element x to the back of queue.
    public void push(int x) {
        Stack<Integer> temp = new Stack<Integer>();
        while(!queue.empty()){
            temp.push(queue.pop());
        }
        queue.push(x);
        while(!temp.empty()){
            queue.push(temp.pop());
        }
    }

    // Removes the element from in front of queue.
    public void pop() {
        queue.pop();
    }

    // Get the front element.
    public int peek() {
        return queue.peek();
    }

    // Return whether the queue is empty.
    public boolean empty() {
        return queue.empty();
    }
}
</p>


