---
title: "Interleaving String"
weight: 97
#id: "interleaving-string"
---
## Description
<div class="description">
<p>Given <code>s1</code>, <code>s2</code>, and <code>s3</code>, find whether <code>s3</code> is formed by the interleaving of <code>s1</code> and <code>s2</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/02/interleave.jpg" style="width: 561px; height: 203px;" />
<pre>
<strong>Input:</strong> s1 = &quot;aabcc&quot;, s2 = &quot;dbbca&quot;, s3 = &quot;aadbbcbcac&quot;
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s1 = &quot;aabcc&quot;, s2 = &quot;dbbca&quot;, s3 = &quot;aadbbbaccc&quot;
<strong>Output:</strong> false
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s1 = &quot;&quot;, s2 = &quot;&quot;, s3 = &quot;&quot;
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= s1.length, s2.length &lt;= 100</code></li>
	<li><code>0 &lt;= s3.length &lt;= 200</code></li>
	<li><code>s1</code>, <code>s2</code>, and <code>s3</code> consist of lower-case English letters.</li>
</ul>

</div>

## Tags
- String (string)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 6 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- VMware - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)

## Official Solution
[TOC]
## Summary

We need to determine whether a given string can be formed by interleaving the other two strings.

## Solution

---
#### Approach 1: Brute Force

The most basic idea is to find every string possible by all interleavings of the two given strings $$s1$$ and $$s2$$.
In order to implement this method, we are using recursion. We start by taking the current character of the
first string $$s1$$ and then appending all possible interleavings of the remaining portion of the first string $$s1$$ and the second string $$s2$$ and comparing each result formed with the required interleaved string $$s3$$.
Similarly, we choose one character from the second string $$s2$$ and form all the interleavings with the remaining portion of $$s2$$ and $$s1$$ to check if the required string $$s1$$ can be formed.

For implementing the recursive function, we make the function call recursively as
$$is\_Interleave(s1,i+1,s2,j,res+s1.charAt(i),s3)$$, in which we have chosen the current character from $$s1$$ and then make another function call $$is\_Interleave(s1,i,s2,j+1,res+s2.charAt(j),s3)$$, in which the current character of $$s2$$ is chosen. Here, $$res$$ refers to that portion(interleaved) of $$s1$$ and $$s2$$ which has already been processed. If anyone of these calls return the result as $$True$$, it means that atleast one interleaving gives the required result $$s3$$. The recursive calls end when both the strings $$s1$$ and $$s2$$ have been fully processed.

Let's look at a small example to see how the execution proceeds.

```
s1="abc"
s2="bcd"
s3="abcbdc"
```

Firstly we choose 'a' of $$s1$$ as the processed part i.e. res and call the recursive function considering the new strings as $$s1$$="bc",
$$s2$$="bcd", $$s3$$="abcbdc". When this function returns a result, we again call the recursive function but with the new strings as $$s1$$="abc", $$s2$$="cd", $$s3$$="abcbdc"


<iframe src="https://leetcode.com/playground/PAo7duuo/shared" frameBorder="0" width="100%" height="344" name="PAo7duuo"></iframe>

**Complexity Analysis**

* Time complexity : $$O(2^{m+n})$$. $$m$$ is the length of $$s1$$ and $$n$$ is the length of $$s2$$.

* Space complexity : $$O(m+n)$$. The size of stack for recursive calls can go upto $$m+n$$.
<br />
<br />
---
#### Approach 2: Recursion with memoization

**Algorithm**

In the recursive approach discussed above, we are considering every possible string formed by interleaving the two given
strings. But, there will be cases encountered in which, the same portion of $$s1$$ and $$s2$$ would have been processed already
but in different orders(permutations). But irrespective of the order of processing, if the resultant string formed till now
is matching with the required string($$s3$$), the final result is dependent only on the remaining portions of $$s1$$ and $$s2$$, but
not on the already processed portion. Thus, the recursive approach leads to redundant computations.

This redundancy can be removed by making use of memoization along with recursion. For this, we maitain 3 pointers $$i, j, k$$
 which correspond to the index of the current character of $$s1, s2, s3$$ respectively. Also, we maintain a 2D memo array to keep a track of the substrings processed so far. $$memo[i][j]$$ stores a 1/0 or -1 depending on
 whether the current portion of strings i.e. upto $$i^{th}$$ index for $$s1$$ and upto $$j^{th}$$ index for s2 has already been evaluated. Again, we start by selecting the current character of $$s1$$ (pointed by $$i$$). If it matches the current character
 of $$s3$$ (pointed by $$k$$), we include it in the processed string and call the same function recurively as:
 $$is\_Interleave(s1, i+1, s2, j, s3, k+1,memo)$$

Thus, here we have called the function by incrementing the pointers $$i$$ and $$k$$ since the portion of strings upto those indices
 has already been processed. Similarly, we choose one character from the second string and continue. The recursive function
 ends when either of the two strings $$s1$$ or $$s2$$ has been fully processed. If, let's say, the string $$s1$$ has been fully processed,
 we directly compare the remaining portion of $$s2$$ with the remaining portion of $$s3$$. When the backtrack occurs from the recursive
 calls, we store the value returned by the recursive functions in the memoization array memo appropriatelys so that when it is encountered the next time, the recursive function won't be called, but the memoization array itself will return the previous generated result.

 <iframe src="https://leetcode.com/playground/SRBHo5XR/shared" frameBorder="0" width="100%" height="500" name="SRBHo5XR"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(m \cdot n)$$, where 
$$m$$ is the length of $$s1$$ and $$n$$ is the length of $$s2$$.
That's a consequence of the fact that each `(i, j)` combination is computed only once.

* Space complexity: $$\mathcal{O}(m \cdot n)$$ to keep double array `memo`.
<br />
<br />
---
#### Approach 3: Using 2D Dynamic Programming

**Algorithm**


The recursive approach discussed in above solution included a character from one of the strings $$s1$$ or $$s2$$ in the resultant
interleaved string and called a recursive function to check whether the remaining portions of $$s1$$ and $$s2$$ can be interleaved
to form the remaining portion of $$s3$$. In the current approach, we
 look at the same problem the other way around. Here, we include one character from $$s1$$ or $$s2$$ and check whether the
 resultant string formed so far by one particular interleaving of the the current prefix of $$s1$$ and $$s2$$ form a prefix of $$s3$$.

 Thus, this approach relies on the fact that the in order to determine whether a substring
 of $$s3$$(upto index $$k$$), can be formed by interleaving strings $$s1$$ and $$s2$$ upto indices $$i$$ and $$j$$ respectively, solely depends
 on the characters of $$s1$$ and $$s2$$ upto indices $$i$$ and $$j$$ only and not on the characters coming afterwards.

 To implement this method, we'll make use of a 2D boolean array $$dp$$. In this array $$dp[i][j]$$ implies if it is possible to
 obtain a substring of length $$(i+j+2)$$ which is a prefix of $$s3$$ by some interleaving of prefixes of strings $$s1$$ and $$s2$$ having
 lengths $$(i+1)$$ and $$(j+1)$$ respectively. For filling in the entry of $$dp[i][j]$$, we need to consider two cases:

 1. The character
 just included i.e. either at $$i^{th}$$ index of $$s1$$ or at $$j^{th}$$ index of $$s2$$ doesn't match the character at $$k^{th}$$ index of $$s3$$, where $$k=i+j+1$$.
 In this case, the resultant string formed using some interleaving of prefixes of $$s1$$ and $$s2$$ can never result in a prefix of length $$k+1$$ in $$s3$$. Thus, we enter $$False$$ at the character $$dp[i][j]$$.

 2. The character
 just included i.e. either at $$i^{th}$$ index of $$s1$$ or at $$j^{th}$$ index of $$s2$$  matches the character at $$k^{th}$$ index of $$s3$$, where $$k=i+j+1$$.
Now, if the character just included(say $$x$$) which matches the character at $$k^{th}$$ index of $$s3$$, is the character at $$i^{th}$$ index of $$s1$$, we need to keep $$x$$ at the last position in the resultant interleaved string formed so far. Thus, in order to use string $$s1$$ and $$s2$$ upto indices $$i$$ and $$j$$ to form a resultant string of length $$(i+j+2)$$ which is a prefix of $$s3$$, we need to ensure that the strings $$s1$$ and $$s2$$ upto indices $$(i-1)$$ and $$j$$ respectively obey the same property.

 Similarly, if we just included the $$j^{th}$$ character of $$s2$$, which matches with the $$k^{th}$$ character of $$s3$$, we need to ensure that the strings $$s1$$ and $$s2$$ upto indices $$i$$ and $$(j-1)$$ also obey the same
property to enter a $$true$$ at $$dp[i][j]$$.

This can be made clear with the following example:

```
s1="aabcc"
s2="dbbca"
s3="aadbbcbcac"
```

<!--![97_Interleaving](../Figures/97_Interleaving.gif)-->
!?!../Documents/97_Interleaving.json:1000,563!?!


<iframe src="https://leetcode.com/playground/44ZakeEz/shared" frameBorder="0" width="100%" height="429" name="44ZakeEz"></iframe>

**Complexity Analysis**

* Time complexity : $$O(m \cdot n)$$. dp array of size $$m*n$$ is filled.

* Space complexity : $$O(m \cdot n)$$. 2D dp of size $$(m+1)*(n+1)$$ is required. $$m$$ and $$n$$ are the lengths of strings $$s1$$ and $$s2$$ respectively.
<br />
<br />
---
#### Approach 4: Using 1D Dynamic Programming

**Algorithm**

This approach is the same as the previous approach except that we have used only 1D $$dp$$ array to store the results of the
 prefixes of the strings processed so far. Instead of maintaining a 2D array, we can maintain a 1D array only and update the
 array's element $$dp[i]$$ when needed using $$dp[i-1]$$ and the previous value of $$dp[i]$$.

 <iframe src="https://leetcode.com/playground/MnuPUPCy/shared" frameBorder="0" width="100%" height="429" name="MnuPUPCy"></iframe>

**Complexity Analysis**

* Time complexity : $$O(m \cdot n)$$. dp array of size $$n$$ is filled $$m$$ times.

* Space complexity : $$O(n)$$. $$n$$ is the length of the string $$s1$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My DP solution in C++
- Author: sherryxmhe
- Creation Date: Mon Sep 22 2014 23:25:03 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 11:35:25 GMT+0800 (Singapore Standard Time)

<p>
 

     bool isInterleave(string s1, string s2, string s3) {
        
        if(s3.length() != s1.length() + s2.length())
            return false;
        
        bool table[s1.length()+1][s2.length()+1];
        
        for(int i=0; i<s1.length()+1; i++)
            for(int j=0; j< s2.length()+1; j++){
                if(i==0 && j==0)
                    table[i][j] = true;
                else if(i == 0)
                    table[i][j] = ( table[i][j-1] && s2[j-1] == s3[i+j-1]);
                else if(j == 0)
                    table[i][j] = ( table[i-1][j] && s1[i-1] == s3[i+j-1]);
                else
                    table[i][j] = (table[i-1][j] && s1[i-1] == s3[i+j-1] ) || (table[i][j-1] && s2[j-1] == s3[i+j-1] );
            }
            
        return table[s1.length()][s2.length()];
    }
    


Here is some explanation:

DP table represents if s3 is interleaving at (i+j)th position when s1 is at ith position, and s2 is at jth position. 0th position means empty string.

So if both s1 and s2 is currently empty, s3 is empty too, and it is considered interleaving. If only s1 is empty, then if previous s2 position is interleaving and current s2 position char is equal to s3 current position char, it is considered interleaving. similar idea applies to when s2 is empty. when both s1 and s2 is not empty, then if we arrive i, j from i-1, j, then if i-1,j is already interleaving and i and current s3 position equal, it s interleaving. If we arrive i,j from i, j-1, then if i, j-1 is already interleaving and j and current s3 position equal. it is interleaving.
</p>


### 8ms C++ solution using BFS, with explanation
- Author: grapeot
- Creation Date: Wed Dec 31 2014 09:25:10 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 21:55:11 GMT+0800 (Singapore Standard Time)

<p>
If we expand the two strings s1 and s2 into a chessboard, then this problem can be transferred into a path seeking problem from the top-left corner to the bottom-right corner. The key is, each cell (y, x) in the board corresponds to an interval between y-th character in s1 and x-th character in s2. And adjacent cells are connected with like a grid. A BFS can then be efficiently performed to find the path.

Better to illustrate with an example here:

Say s1 = "aab" and s2 = "abc". s3 = "aaabcb". Then the board looks like

    o--a--o--b--o--c--o
    |     |     |     |
    a     a     a     a
    |     |     |     |
    o--a--o--b--o--c--o
    |     |     |     |
    a     a     a     a
    |     |     |     |
    o--a--o--b--o--c--o
    |     |     |     |
    b     b     b     b
    |     |     |     |
    o--a--o--b--o--c--o

Each "o" is a cell in the board. We start from the top-left corner, and try to move right or down. If the next char in s3 matches the edge connecting the next cell, then we're able to move. When we hit the bottom-right corner, this means s3 can be represented by interleaving s1 and s2. One possible path for this example is indicated with "x"es below:

    x--a--x--b--o--c--o
    |     |     |     |
    a     a     a     a
    |     |     |     |
    o--a--x--b--o--c--o
    |     |     |     |
    a     a     a     a
    |     |     |     |
    o--a--x--b--x--c--x
    |     |     |     |
    b     b     b     b
    |     |     |     |
    o--a--o--b--o--c--x

Note if we concatenate the chars on the edges we went along, it's exactly s3. And we went through all the chars in s1 and s2, in order, exactly once.

Therefore if we view this board as a graph, such path finding problem is trivial with BFS. I use an `unordered_map` to store the visited nodes, which makes the code look a bit complicated. But a `vector` should be enough to do the job. 

Although the worse case timeis also O(mn), typically it doesn't require us to go through every node to find a path. Therefore it's faster than regular DP than average.

    struct MyPoint {
        int y, x; 
        bool operator==(const MyPoint &p) const {
            return p.y == y && p.x == x;
        }
    };
    namespace std {
        template <>
        struct hash<MyPoint> {
            size_t operator () (const MyPoint &f) const {
                return (std::hash<int>()(f.x) << 1) ^ std::hash<int>()(f.y);
            }
        };
    }
    
    class Solution {
    public:
        bool isInterleave(string s1, string s2, string s3) {
            if (s1.size() + s2.size() != s3.size()) return false;

            queue<MyPoint> q;
            unordered_set<MyPoint> visited;
            bool isSuccessful = false;
            int i = 0;
    
            q.push(MyPoint { 0, 0 });
            q.push(MyPoint { -1, -1 });
            while (!(1 == q.size() && -1 == q.front().x)) {
                auto p = q.front();
                q.pop();
                if (p.y == s1.size() && p.x == s2.size()) {
                    return true;
                }
                if (-1 == p.y) {
                    q.push(p);
                    i++;
                    continue;
                }
                if (visited.find(p) != visited.end()) { continue; }
                visited.insert(p);
    
                if (p.y < s1.size()) { // down
                    if (s1[p.y] == s3[i]) { q.push(MyPoint { p.y + 1, p.x }); }
                }
                if (p.x < s2.size()) { // right 
                    if (s2[p.x] == s3[i]) { q.push(MyPoint { p.y, p.x + 1 }); }
                }
            }
            return false;
        }
    };
</p>


### 1ms tiny DFS beats 94.57%
- Author: yavinci
- Creation Date: Sun Dec 20 2015 14:56:13 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 08:59:58 GMT+0800 (Singapore Standard Time)

<p>
To solve this problem, let's look at if `s1[0 ~ i]`  `s2[0 ~ j]` can be interleaved to `s3[0 ~ k]`.

- Start from indices`0, 0, 0` and compare `s1[i] == s3[k]` or `s2[j] == s3[k]`
- Return valid only if either `i` or `j` match `k` and the remaining is also valid
- Caching is the key to performance. This is very similar to top down dp
- Only need to cache `invalid[i][j]` since most of the case `s1[0 ~ i]` and `s2[0 ~ j]` does not form `s3[0 ~ k]`. Also tested caching `valid[i][j]` the run time is also `1ms`
- Many guys use `substring` but it's duplicate code since `substring` itself is checking char by char. We are already doing so


Hope it helps!

    public boolean isInterleave(String s1, String s2, String s3) {
        char[] c1 = s1.toCharArray(), c2 = s2.toCharArray(), c3 = s3.toCharArray();
    	int m = s1.length(), n = s2.length();
    	if(m + n != s3.length()) return false;
    	return dfs(c1, c2, c3, 0, 0, 0, new boolean[m + 1][n + 1]);
    }
    
    public boolean dfs(char[] c1, char[] c2, char[] c3, int i, int j, int k, boolean[][] invalid) {
    	if(invalid[i][j]) return false;
    	if(k == c3.length) return true;
    	boolean valid = 
    	    i < c1.length && c1[i] == c3[k] && dfs(c1, c2, c3, i + 1, j, k + 1, invalid) || 
            j < c2.length && c2[j] == c3[k] && dfs(c1, c2, c3, i, j + 1, k + 1, invalid);
    	if(!valid) invalid[i][j] = true;
        return valid;
    }
</p>


