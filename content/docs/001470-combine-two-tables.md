---
title: "Combine Two Tables"
weight: 1470
#id: "combine-two-tables"
---
## Description
<div class="description">
<p>Table: <code>Person</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| PersonId    | int     |
| FirstName   | varchar |
| LastName    | varchar |
+-------------+---------+
PersonId is the primary key column for this table.
</pre>

<p>Table: <code>Address</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| AddressId   | int     |
| PersonId    | int     |
| City        | varchar |
| State       | varchar |
+-------------+---------+
AddressId is the primary key column for this table.
</pre>

<p>&nbsp;</p>

<p>Write a SQL query for a report that provides the following information for each person in the Person table, regardless if there is an address for each of those people:</p>

<pre>
FirstName, LastName, City, State
</pre>

</div>

## Tags


## Companies
- Adobe - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `outer join` [Accepted]

**Algorithm**

Since the *PersonId* in table **Address** is the foreign key of table **Person**, we can join this two table to get the address information of a person.

Considering there might not be an address information for every person, we should use `outer join` instead of the default `inner join`.

**MySQL**

```sql
select FirstName, LastName, City, State
from Person left join Address
on Person.PersonId = Address.PersonId
;
```
>Note: Using `where` clause to filter the records will fail if there is no address information for a person because it will not display the name information.

## Accepted Submission (mysql)
```mysql
# Write your MySQL query statement below
select FirstName, LastName, City, State
from Person
left outer join Address
    on Person.PersonId = Address.PersonId;
```

## Top Discussions
### Its a simple question of Left Join. My solution attached
- Author: lazyprogrammer
- Creation Date: Sun Jan 11 2015 04:24:46 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 09:02:55 GMT+0800 (Singapore Standard Time)

<p>
    SELECT Person.FirstName, Person.LastName, Address.City, Address.State from Person LEFT JOIN Address on Person.PersonId = Address.PersonId;
</p>


### Comparative solution between LEFT JOIN, LEFT JOIN USING and NATURAL LEFT JOIN
- Author: ohini
- Creation Date: Thu Aug 20 2015 11:45:57 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 20 2015 11:45:57 GMT+0800 (Singapore Standard Time)

<p>
basic left join: 902ms.

    SELECT FirstName, LastName, City, State
    FROM Person
    LEFT JOIN Address
    ON Person.PersonId = Address.PersonId;

left join + using: 907ms

    SELECT FirstName, LastName, City, State
    FROM Person
    LEFT JOIN Address
    USING(PersonId);

natural left join: 940ms

    SELECT FirstName, LastName, City, State
    FROM Person
    NATURAL LEFT JOIN Address;

left join is the fastest compare to the two others.
</p>


### Why cannot using where
- Author: wxs2204
- Creation Date: Wed Feb 17 2016 04:14:11 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 28 2018 02:47:56 GMT+0800 (Singapore Standard Time)

<p>
    select p.FirstName, p.LastName, a.City, a. State
    from Person p, Address a 
    where p.PersonId = a.PersonId;

didnt using sql for two years, might be a stupid question
</p>


