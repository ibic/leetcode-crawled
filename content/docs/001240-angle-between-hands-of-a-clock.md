---
title: "Angle Between Hands of a Clock"
weight: 1240
#id: "angle-between-hands-of-a-clock"
---
## Description
<div class="description">
<p>Given two numbers, <code>hour</code> and <code>minutes</code>. Return the smaller angle (in degrees) formed between the <code>hour</code> and the <code>minute</code> hand.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/12/26/sample_1_1673.png" style="width: 230px; height: 225px;" /></p>

<pre>
<strong>Input:</strong> hour = 12, minutes = 30
<strong>Output:</strong> 165
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/12/26/sample_2_1673.png" style="width: 230px; height: 225px;" /></p>

<pre>
<strong>Input:</strong> hour = 3, minutes = 30
<strong>Output:</strong> 75
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/12/26/sample_3_1673.png" style="width: 230px; height: 225px;" /></strong></p>

<pre>
<strong>Input:</strong> hour = 3, minutes = 15
<strong>Output:</strong> 7.5
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> hour = 4, minutes = 50
<strong>Output:</strong> 155
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> hour = 12, minutes = 0
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= hour &lt;= 12</code></li>
	<li><code>0 &lt;= minutes &lt;= 59</code></li>
	<li>Answers within&nbsp;<code>10^-5</code>&nbsp;of the actual value will be accepted as correct.</li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Facebook - 4 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Math

**Intuition**

The idea is to calculate separately the angles between 0-minutes 
vertical line and each hand. The answer is the difference between these
two angles. 

![fig](../Figures/1344/diff.png)

**Minute Hand Angle**

Let's start from the minute hand. The whole circle is equal to $$360°$$ or 
60 minutes, _i.e._ minute hand moves $$1 \text{ min} = 360° / 60 = 6°$$ degree
at each minute.

![fig](../Figures/1344/one_min2.png)

Now one could easily find an angle between 0-minutes vertical line and 
a minute hand: $$\text{minutes\_angle} = \text{minutes} \times 6°$$.

![fig](../Figures/1344/qq_min2.png)

**Hour Hand Angle**

Similarly with the minute hand angle,
the whole circle is equal to $$360°$$ or 
12 hours, hence for each hour,
the hour hand moves $$1 \text{h} = 360° / 12 = 30°$$ degree.

![fig](../Figures/1344/hour.png)

Now for the "minutes = 0" case one could easily find an angle 
between 12-hour vertical line and an hour hand: 
$$\text{hour\_angle} = \text{hour} \times 30°$$.

![fig](../Figures/1344/qq_h.png)

Note that for 12-hour the actual angle is zero, therefore the
expression has to be corrected 
$$\text{hour\_angle} = (\text{hour mod } 12) \ \times 30°$$.

In a more general case where "minutes > 0", 
one has to take into account an additional
movement of hour hand: it doesn't jump between the integer values
but follows the movement of minute hand as well

$$
\text{hour\_angle} = \left(\text{hour mod } 12 + \text{minutes} / 60 \right)\times 30°
$$

![fig](../Figures/1344/minutes_corr2.png)

**Algorithm**

- Initialize the constants: `one_min_angle = 6`,
`one_hour_angle = 30`.

- The angle between minute hand and 0-minutes vertical line is
`minutes_angle = one_min_angle * minutes`.

- The angle between hour hand and 12-hour vertical line is 
`hour_angle = (hour % 12 + minutes / 60) * one_hour_angle`.

- Find the difference: `diff = abs(hour_angle - minutes_angle)`.

- Return the smallest angle: `min(diff, 360 - diff)`.

**Implementation**

<iframe src="https://leetcode.com/playground/WRkvokQq/shared" frameBorder="0" width="100%" height="276" name="WRkvokQq"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$.
 
* Space complexity : $$\mathcal{O}(1)$$.
<br /> 
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python/C++] Simple Math on Clock angles
- Author: sankalpdayal5
- Creation Date: Sun Feb 09 2020 00:04:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 19 2020 02:31:43 GMT+0800 (Singapore Standard Time)

<p>
**Basic Unitary Method**
(Credits - @rajcm)

**Hour Hand**
In 12 hours Hour hand complete whole circle and cover 360\xB0
So, 1 hour = 360\xB0 / 12 = 30\xB0

Since 1 hours = 30\xB0
In 1 minute, hours hand rotate -> 30\xB0 / 60 = 0.5\xB0
So total angle because of minutes by hour hand is `minutes/60 * 30` or `minutes * 0.5`

**Minute Hand**
In 60 minutes Minute Hand completes whole circle and cover 360\xB0.
So, 1 minute -> 360\xB0 / 60 = 6\xB0

<br><br>

**Java**
```
class Solution {
    public double angleClock(int hour, int minutes) {
        
         // Degree covered by hour hand (hour area + minutes area)
        double h = (hour%12 * 30) + ((double)minutes/60 * 30);
        
         // Degree covered by minute hand (Each minute = 6 degree)
        double m = minutes * 6;
        
         // Absolute angle between them
        double angle = Math.abs(m - h);
        
         // If the angle is obtuse (>180), convert it to acute (0<=x<=180)
        if (angle > 180) angle = 360.0 - angle;
        
        return angle;
    }
}
```
<br><br>

**Python**

```
class Solution:
    def angleClock(self, hour: int, minutes: int) -> float:
        
        # Degree covered by hour hand (hour area + minutes area)
        h = (hour%12 * 30) + (minutes/60 * 30)
        
        # Degree covered by minute hand (Each minute = 6 degree)
        m = minutes * 6
        
        # Absolute angle between them
        angle = abs(m - h)
        
        # If the angle is obtuse (>180), convert it to acute (0<=x<=180)
        if angle > 180:
            angle = 360.0 - angle
        
        return (angle)
```

**C++**
Credits : [MichaelZ](https://leetcode.com/michaelz/)
Thanks for the C++ code.
```
double angleClock(int hour, int minutes) {
        double minute=minutes*6, hr=hour*30+(double)minutes/2, diff=abs(hr-minute);
        return min(diff, 360-diff);
    }
```	

Please upvote if you found this useful.
If you have any queries, please post in comment section.
Thank you
</p>


### Basic Unitary Method | Old School Math | Explained 2-line Code
- Author: rajcm
- Creation Date: Tue Jul 14 2020 20:28:34 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 15 2020 13:42:00 GMT+0800 (Singapore Standard Time)

<p>
**Basic Unitary Method**

**Hour Hand**
In `12` hours Hour hand complete whole circle and cover `360\xB0`
1 hour -  360\xB0 / 12 = 30\xB0

**Minute Hand**
In `60` minutes Minute Hand coplete whole circle and cover `360\xB0`.
1 minute -> 360\xB0 / 60 = 6\xB0

1 hour - 60 minutes
1 hours - 30\xB0
1 minute hours hand rotate -> 30\xB0 / 60 = 0.5\xB0

**Video Explanation** https://www.youtube.com/watch?v=38pOVOeEB4Y

`In below clock \u03B81 is angle by hour hand and \u03B82 angle by minute and so angle between hour and minute hand ->  \u03B82 - \u03B81`

![image](https://assets.leetcode.com/users/images/83532ad3-be96-4038-82c7-7217cc8a6ef0_1594729624.8749366.png)

********

**Java**

```
class Solution {
    public double angleClock(int hour, int minutes) {
        double angle = Math.abs(30.0 * hour + 0.5  * minutes - 6 * minutes);
        return angle <= 180.0 ? angle : 360 - angle;
    }
}
```

********
**Python**

```
class Solution:
    def angleClock(self, hour: int, minutes: int) -> float:
        angle = abs(30.0 * hour + 0.5  * minutes - 6 * minutes)
        return min(angle, 360 - angle)
```

`TC - O(1)`
`SC - O(1)`

If any doubt ask in comment. if you like solution **upvote**.
</p>


### [Python] math solution + Oneliner, explained
- Author: DBabichev
- Creation Date: Tue Jul 14 2020 15:57:07 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 14 2020 16:45:05 GMT+0800 (Singapore Standard Time)

<p>
To solve this problem we need to understand the speeds of Hands of a clock.

1. Let us find the place, where **hour hand** is. First, whe have `12` hours in total, for `360` degrees, that means `30` degrees per hour. Also, for every `60` minutes, our hour hand rotated by `1` hour, that is `30` degrees, so for every minute, it is rotated by `0.5` degrees. So, final place for hour hand is `30*hour + 0.5*minutes`
2. Let us find the place, where **minute hand** is: every `60` minutes minute hand makes full rotation, that means we have `6` degrees for each minute.
3. Finally, we evaluate absolute difference between these two numbers, and if angle is more than `180` degrees, we return complementary angle.

**Complexity**: time and space is `O(1)`, we just use some mathematical formulae.

```
class Solution:
    def angleClock(self, hour, minutes):
        H_place = 30*hour + 0.5*minutes
        M_place = 6*minutes
        diff = abs(H_place - M_place)
        return diff if diff <= 180 else 360 - diff
```

**Oneliner**
We can write this as oneliner as well:
```
return min(abs(30*hour-5.5*minutes),360-abs(30*hour-5.5*minutes))
```

If you have any questions, feel free to ask. If you like solution and explanations, please **Upvote!**
</p>


