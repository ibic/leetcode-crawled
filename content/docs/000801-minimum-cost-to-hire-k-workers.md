---
title: "Minimum Cost to Hire K Workers"
weight: 801
#id: "minimum-cost-to-hire-k-workers"
---
## Description
<div class="description">
<p>There are <code>N</code> workers.&nbsp; The <code>i</code>-th worker has a <code>quality[i]</code> and a minimum wage expectation <code>wage[i]</code>.</p>

<p>Now we want to hire exactly <code>K</code>&nbsp;workers to form a <em>paid group</em>.&nbsp; When hiring a group of K workers, we must pay them according to the following rules:</p>

<ol>
	<li>Every worker in the paid group should be paid in the ratio of their quality compared to other workers in the paid group.</li>
	<li>Every worker in the paid group must be paid at least their minimum wage expectation.</li>
</ol>

<p>Return the least amount of money needed to form a paid group satisfying the above conditions.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>quality = <span id="example-input-1-1">[10,20,5]</span>, wage = <span id="example-input-1-2">[70,50,30]</span>, K = <span id="example-input-1-3">2</span>
<strong>Output: </strong><span id="example-output-1">105.00000
<strong>Explanation</strong>: </span><span>We pay 70 to 0-th worker and 35 to 2-th worker.</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>quality = <span id="example-input-2-1">[3,1,10,10,1]</span>, wage = <span id="example-input-2-2">[4,8,2,2,7]</span>, K = <span id="example-input-2-3">3</span>
<strong>Output: </strong><span id="example-output-2">30.66667
<strong>Explanation</strong>: </span><span>We pay 4 to 0-th worker, 13.33333 to 2-th and 3-th workers seperately.</span> 
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= K &lt;= N &lt;= 10000</code>, where <code>N = quality.length = wage.length</code></li>
	<li><code>1 &lt;= quality[i] &lt;= 10000</code></li>
	<li><code>1 &lt;= wage[i] &lt;= 10000</code></li>
	<li>Answers within <code>10^-5</code> of the correct answer will be considered correct.</li>
</ol>
</div>
</div>

</div>

## Tags
- Heap (heap)

## Companies
- Google - 18 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Greedy

**Intuition**

At least one worker will be paid their minimum wage expectation.  If not, we could scale all payments down by some factor and still keep everyone earning more than their wage expectation.

**Algorithm**

For each `captain` worker that will be paid their minimum wage expectation, let's calculate the cost of hiring K workers where each point of quality is worth `wage[captain] / quality[captain]` dollars.  With this approach, the remaining implementation is straightforward.

Note that this algorithm would not be efficient enough to pass larger test cases.

<iframe src="https://leetcode.com/playground/jyS5Ysxy/shared" frameBorder="0" width="100%" height="500" name="jyS5Ysxy"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2 \log N)$$, where $$N$$ is the number of workers.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Heap

**Intuition**

As in *Approach #1*, at least one worker is paid their minimum wage expectation.

Additionally, every worker has some minimum `ratio` of dollars to quality that they demand.  For example, if `wage[0] = 100` and `quality[0] = 20`, then the `ratio` for worker 0 is `5.0`.

The key insight is to iterate over the ratio.  Let's say we hire workers with a ratio `R` or lower.  Then, we would want to know the `K` workers with the lowest quality, and the sum of that quality.  We can use a heap to maintain these variables.

**Algorithm**

Maintain a max heap of quality.  (We're using a minheap, with negative values.)  We'll also maintain `sumq`, the sum of this heap.

For each worker in order of ratio, we know all currently considered workers have lower ratio.  (This worker will be the 'captain', as described in *Approach #1*.)  We calculate the candidate answer as this ratio times the sum of the smallest K workers in quality.

<iframe src="https://leetcode.com/playground/fcEg98JB/shared" frameBorder="0" width="100%" height="500" name="fcEg98JB"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the number of workers.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Detailed explanation O(NlogN)
- Author: lee215
- Creation Date: Sun Jun 24 2018 11:02:50 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 11:46:44 GMT+0800 (Singapore Standard Time)

<p>
Let\'s read description first and figure out the two rules:

**"1. Every worker in the paid group should be paid in the ratio of their quality compared to other workers in the paid group."**
So for any two workers in the paid group,
we have `wage[i] : wage[j] = quality[i] : quality[j]`
So we have `wage[i] : quality[i] = wage[j] : quality[j]`
We pay wage to every worker in the group with the same ratio compared to his own quality.

**"2. Every worker in the paid group must be paid at least their minimum wage expectation."
For every worker, he has an expected ratio of wage compared to his quality.**

So to minimize the total wage, we want a small ratio.
So we sort all workers with their expected ratio, and pick up `K` first worker.
Now we have a minimum possible ratio for `K` worker and we their total quality.

As we pick up next worker with bigger ratio, we increase the ratio for whole group.
Meanwhile we remove a worker with highest quality so that we keep `K` workers in the group.
We calculate the current ratio * total quality = total wage for this group.

We redo the process and we can find the minimum total wage. 
Because workers are sorted by ratio of wage/quality. 
For every ratio, we find the minimum possible total quality of K workers.

**Time Complexity**
`O(NlogN)` for sort.
`O(NlogK)` for priority queue.

**C++:**
```
    double mincostToHireWorkers(vector<int> q, vector<int> w, int K) {
        vector<vector<double>> workers;
        for (int i = 0; i < q.size(); ++i)
            workers.push_back({(double)(w[i]) / q[i], (double)q[i]});
        sort(workers.begin(), workers.end());
        double res = DBL_MAX, qsum = 0;
        priority_queue<int> pq;
        for (auto worker: workers) {
            qsum += worker[1], pq.push(worker[1]);
            if (pq.size() > K) qsum -= pq.top(), pq.pop();
            if (pq.size() == K) res = min(res, qsum * worker[0]);
        }
        return res;
    }
```

**Java:**
```
    public double mincostToHireWorkers(int[] q, int[] w, int K) {
        double[][] workers = new double[q.length][2];
        for (int i = 0; i < q.length; ++i)
            workers[i] = new double[]{(double)(w[i]) / q[i], (double)q[i]};
        Arrays.sort(workers, (a, b) -> Double.compare(a[0], b[0]));
        double res = Double.MAX_VALUE, qsum = 0;
        PriorityQueue<Double> pq = new PriorityQueue<>();
        for (double[] worker: workers) {
            qsum += worker[1];
            pq.add(-worker[1]);
            if (pq.size() > K) qsum += pq.poll();
            if (pq.size() == K) res = Math.min(res, qsum * worker[0]);
        }
        return res;
    }
```
**Python:**
```
    def mincostToHireWorkers(self, quality, wage, K):
        workers = sorted([float(w) / q, q] for w, q in zip(wage, quality))
        res = float(\'inf\')
        qsum = 0
        heap = []
        for r, q in workers:
            heapq.heappush(heap, -q)
            qsum += q
            if len(heap) > K: qsum += heapq.heappop(heap)
            if len(heap) == K: res = min(res, qsum * r)
        return res
```

**FAQ**:
**Question:** "However, it is possible that current worker has the highest quality, so you removed his quality in the last step, which leads to the problem that you are "using his ratio without him".
**Answer:** It doesn\'t matter. The same group will be calculated earlier with smaller ratio.
And it doesn\'t obey my logic here: For a given ratio of wage/quality, find minimum total wage of K workers.
</p>


### Java with Explanations
- Author: GraceMeng
- Creation Date: Wed Oct 24 2018 09:31:00 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 14 2019 11:51:09 GMT+0800 (Singapore Standard Time)

<p>
Every worker in the paid group should be paid in the ratio of their quality compared to other workers in the paid group.

```
  cost[i]        quality[i]
 _________  =    ________
  cost[j]        quality[j] 
  
  cost[i]        quality[i]
 _________  =    ________
 total cost      total quality
```

So
```
cost[i]  / quality[i] =  cost[j]  / quality[j]

group of workers share the same "cost[i]  / quality[i]", let\'s call it PAID_RATIO
```

```
cost[i]  / quality[i] = total cost / total quality

PAID_RATIO = total cost / total quality
```

Every worker in the paid group must be paid at least their minimum wage expectation.

```
cost[i] >= wage[i]
```

So
```
cost[i] / quality[i] >= wage[i] / quality[i]

PAID_RATIO must be larger than maximum wage[i] / quality[i]  (let\'s call it RATIO) within the group
```

We try putting each worker in k-group and updating PAID_RATIO by RATIO, and remove a worker when group is full. If we remove the worker with largest RATIO in group, PAID_RATIO should be 2nd largest RATIO -- which takes extra effort to maintain.

Instead, we try putting worker with lower RATIO first and updating PAID_RATIO, even if we remove a worker, PAID_RATIO will be of the "current" worker.

Which worker should we remove? According to `PAID_RATIO = total cost / total quality`, the less total quality, the better. So we remove the worker with maximum quality in group when group is full.

Is it possible that the "current" worker, which holds highest PAID_RATIO, is with the maximum quality?

That\'s possible, in this case, current higher PAID_RATIO * previous totalQuality will be calculated.
But that doesn\'t matter, since it must be bigger than `minCost` -- it won\'t affect the final result.
****
```
class Solution {
    public double mincostToHireWorkers(int[] quality, int[] wage, int K) {
        List<Worker> workers = new ArrayList<>();
        
        // Sort by ratio increasingly
        Collections.sort(workers, (a, b) -> (a.ratio - b.ratio));
        
        // Maxheap always pop highest quality
        PriorityQueue<Worker> maxHeap = new PriorityQueue<>((a, b) -> (b.quality - a.quality));
        
        int totalQuality = 0;
        for (Worker worker : workers) {
            maxHeap.offer(worker);
            totalQuality += worker.quality;
            
            if (maxHeap.size() > K) {
                Worker w = maxHeap.poll();
                totalQuality -= w.quality;
            } 
            
            if (maxHeap.size() == K) {
                minCost = Math.min(totalQuality * worker.ratio, minCost);
            }
        }
        
        return minCost;
    }
    
    class Worker {
        int quality;
        int wage;
        int ratio;
        
        public Worker(int quality, int wage) {
            this.quality = quality;
            this.wage = wage;
            ratio = wage[i] / quality[i];
        }
    }
}
```

</p>


### N log N explanation, no code
- Author: betker
- Creation Date: Thu Oct 04 2018 04:31:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 11:54:32 GMT+0800 (Singapore Standard Time)

<p>
If you\'re looking for a hint, but want to solve the code on you\'re own, here\'s my notes assembled as I solved this problem:
```

    // Some things I derived while figuring out this problem:
    // Most importantly: You must pay at least one worker his minimum requested wage (otherwise you are wasting money).
    //                   Who this worker is allows you to pretty easily assemble the rest of the team. To minimize cost,
    //                   you want to minimize the total experience in the group, because the equation for total cost is:
    //         Given fixed worker with wage Wf and quality Qf and total sum of quality in the group Qs:
    //         Cost = Wf * Qs / Qf
    //                   Thus, given a fixed worker that is paid their wage, you can minimize the cost by minimizing Qs, the
    //                   total sum of "quality" in the group.
    //
    // However: Who you pick as the worker with the fixed wage determines who you can add to the group. This is because the ratio
    //          of quality to minimum cost will mean you would pay some workers you added to the group less then their minimum wage.
    //          Given the Wf, Qf above, it can be derived that you will pay any worker with quality Q added to the group:
    //          Wage = Wf * Qs / Qf * Q / Qs (which simplifies to:)
    //               = Wf * Q / Qf
    //          With the above equation on hand, and the knowledge that you must pay any worker in the group their minimum wage, W, you can 
    //          derive a more useful equation:
    //          W <= Wf * Q / Qf (which simlifies to:)
    //          W / Q <= Wf / Qf
    //          This means that the ratio of Wf / Qf for the worker you select must be the highest in the group you assemble.
    //
    // From here, solving this problem involves a few steps:
    // 1) Sort the input array by Wf / Qf.
    // 2) Assemble a list of K workers with the lowest Wf / Qf ratios. This is the group with the smallest possible Wf / Qf ratios, dominated
    //    by a "fixed" worker (Wf/Qf from above) at the last index/node. You should be able to compute the cost of this group of workers
    //    using the first equation derived above.
    // 3) Work along the rest of the sorted Wf/Qf workers. For each additional worker, remove the worker with the highest "quality" from the
    //    list from (2) and add in your new "next-highest" Wf/Qf worker. This will allow you to keep track of your Wf/Qf values as well as
    //    maintain a group of optimally low quality to minimize total cost. Compute the cost for each step and record the minimum.
```

</p>


