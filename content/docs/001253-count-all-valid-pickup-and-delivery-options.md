---
title: "Count All Valid Pickup and Delivery Options"
weight: 1253
#id: "count-all-valid-pickup-and-delivery-options"
---
## Description
<div class="description">
<p>Given <code>n</code> orders, each order consist in pickup and delivery services.&nbsp;</p>

<p>Count all valid pickup/delivery possible sequences such that delivery(i) is always after of&nbsp;pickup(i).&nbsp;</p>

<p>Since the answer&nbsp;may be too large,&nbsp;return it modulo&nbsp;10^9 + 7.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> 1
<strong>Explanation:</strong> Unique order (P1, D1), Delivery 1 always is after of Pickup 1.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2
<strong>Output:</strong> 6
<strong>Explanation:</strong> All possible orders: 
(P1,P2,D1,D2), (P1,P2,D2,D1), (P1,D1,P2,D2), (P2,P1,D1,D2), (P2,P1,D2,D1) and (P2,D2,P1,D1).
This is an invalid order (P1,D2,P2,D1) because Pickup 2 is after of Delivery 2.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 3
<strong>Output:</strong> 90
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 500</code></li>
</ul>

</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- DoorDash - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy and Concise
- Author: lee215
- Creation Date: Sun Feb 23 2020 00:02:57 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 06 2020 10:32:00 GMT+0800 (Singapore Standard Time)

<p>
# Intuition 1
Assume we have already `n - 1` pairs, now we need to insert the `n`th pair.
To insert the first element, there are `n * 2 - 1` chioces of position\u3002
To insert the second element, there are `n * 2` chioces of position\u3002
So there are `(n * 2 - 1) * n * 2` permutations.
Considering that delivery(i) is always after of pickup(i), we need to divide 2.
So it\'s `(n * 2 - 1) * n`.
<br>

# Intuition 2
We consider the first element in all 2n elements.
The first must be a pickup, and we have n pickups as chioce.
It\'s pair can be any positino in the rest of `n*2-1` positions.
So it\'s `(n * 2 - 1) * n`.
<br>

# Intuition 3
The total number of all permutation obviously eauqls to 2n!.
For each pair, the order is determined, so we need to divide by 2.
So the final result is `(2n)!/(2^n)`
<br>

# Complexity
For each run, Time `O(N)`, Space `O(1)`.
Also we can cache the result, so that O(1) amortized for each `n`.
But in doesn\'t help in case of LC.
Also we can pre calculate all results, so that we have `O(N)` space and `O(1)` time.
<br>

# Solution 1: Bottom up
**Java:**
```java
    public int countOrders(int n) {
        long res = 1, mod = (long)1e9 + 7;
        for (int i = 1; i <= n; ++i)
            res = res * (i * 2 - 1) * i % mod;
        return (int)res;
    }
```

**C++:**
```cpp
    int countOrders(int n) {
        long res = 1, mod = 1e9 + 7;
        for (int i = 1; i <= n; ++i)
            res = res * (i * 2 - 1) * i % mod;
        return res;
    }
```

**Python:**
```python
    def countOrders(self, n):
        res, mod = 1, 10**9 + 7
        for i in xrange(2, n + 1):
            res = res * (i * 2 - 1) * (i * 2) / 2 % mod
        return res
```

# Solution 2: Recusrion

**Java:**
`O(N)` space for recursion
```java
    public int countOrders(int n) {
        return n > 0 ? (int)((long)countOrders(n - 1) * (n * 2 - 1) * n % ((long)1e9 + 7)) : 1;
    }
```

**C++:**
```cpp
    int countOrders(int n, long res = 1) {
        return n ? countOrders(n - 1, res * (n * 2 - 1) * n % long(1e9+7)) : res;
    }
```
<br>

# Solution 3: 1-line Python for fun
**Python:**
```py
    def countOrders(self, n):
        return (math.factorial(n * 2) >> n) % (10**9 + 7)
```
</p>


### [C++/Python] 1-line, Simple permutation with explanation
- Author: yanrucheng
- Creation Date: Sun Feb 23 2020 00:00:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 08:56:19 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
Denote `pickup 1, pickup 2, pickup 3, ...` as `A, B, C, ...`
Denote `delivery 1, delivery 2, delivery 3, ...` as `a, b, c, ...`
We need to ensure `a` is behind `A`, `b` is behind `B`, ...

This solution involves  2 stages.
- Stage 1
    - We decide the order of all the pickups. It is trivial to tell there are `n!` possibilities
- Stage 2
    - Given one possibility. Let\'s say the pickups are ordered  like this `A B C`
    - We can now insert the corresponding deliveries one by one.
        - We start with the last pickup we made, namely, insert `c`, and there is only 1 valid slot.
            - `A B C c`
        - We continue with the second last pickup we made, namely, insert `b`, and there are 3 valid slots.
            - `A B` **x** `C` **x** `c` **x** (where **x** denotes the location of valid slots for `b`)
        - Let\'s only consider one case `A B C c b`. We continue with the third last pickup we made, namely, insert `a`, and there are 5 valid slots.
            - `A` **x** `B` **x** `C` **x** `c` **x** `b` **x**, (where **x** denotes the location of valid slots for `a`)
- In conclusion. we have in total `1 * 3 * 5 * ... * (2n-1)` possibilities
Thus, the final solution is `n! * (1 * 3 * 5 * ... * (2n-1)) % 1000000007`

**Complexity**
Time: `O(n)`, can be improved to `O(1)` with memoization
Space: `O(1)`

**C++**
```
int countOrders(int n) {
    long long res = 1, cap = 1000000007;
    for (int i=1; i<n+1; ++i) res = res * i % cap;
    for (int i=1; i<2*n; i+=2) res = res * i % cap;
    return res;
}
```

**Python 3, with explanations and meaningful variable names**
```
from functools import reduce
from operator import mul
class Solution:
    def countOrders(self, n: int) -> int:
        cap = 10**9 + 7
        pickup_permutation = math.factorial(n) % cap
        delivery_permutation = reduce(mul, range(1, 2*n, 2), 1) % cap
        return pickup_permutation * delivery_permutation % cap
```

**Python 3, 1-line, mod along the way, O(1) space (12.8 MB)**
```
from functools import reduce
class Solution:
    def countOrders(self, n: int) -> int:
		# remember to use generator instead of tuple for O(1) space complexity
        return reduce(lambda x, y: x * y % (10**9+7), (v for x in range(1,n+1) for v in (x, 2*x-1)), 1)
```

**Python 3, 1-line, short, O(n) space**
```
from functools import reduce
from operator import mul
class Solution:
    def countOrders(self, n: int) -> int:
        return reduce(mul, (*range(1,n+1), *range(1,2*n,2)), 1) % (10**9+7)
```
</p>


### Simple Java DP
- Author: Poorvank
- Creation Date: Sun Feb 23 2020 00:01:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 00:06:01 GMT+0800 (Singapore Standard Time)

<p>
Pickup needs to be before Delivery.
Consider one of the valid cases for 2 pickups and 2 deliveries:

P1 P2 D1 D2.

Now lets Try to place P3 D3 within the above sequence:

Since we have 4 parts to this ( 2 pickups & 2 deliveries) we can insert (P3D3) at any of the below 5 spaces:

__ P1 __ P2 __ D1 __ D2 __


If we place (P3 D3) at space 1 :  we can have five combinations: 5
If we place (P3 D3) at space 2 :  we can have four combinations: 4
If we place (P3 D3) at space 3 :  we can have three combinations: 3
If we place (P3 D3) at space 4 :  we can have two combinations: 2
If we place (P3 D3) at space 5 :  we can have two combinations: 1

Total (5+4+3+2+1) = 15 => (5*(5+1)/2).. sum from 1 to n is (n*(n+1)/2);

Since with 2 pickups and 2 deliveries we can have 6 combinations, hence adding (P3D3) will result in :
(15*6) = 90 combinations.

Hence we first count the spaces, the calculate the number of times we can insert the new (Pi,Di) and finally multiply it with the existing combinations
to get our answer.

```
private int mod = (int) Math.pow(10,9) + 7;
    long[] dp = new long[501];
    public int countOrders(int n) {
        dp[1]=1L;
        dp[2]=6L;
        for (int i=3;i<=n;i++) {
            int spaceCount = (i-1)*2 + 1;
            long val = (spaceCount)*(spaceCount+1)/2;
            dp[i] = (dp[i-1]*val)%mod;
        }
        return (int) dp[n];
    }
```
</p>


