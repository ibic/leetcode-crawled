---
title: "Longest Valid Parentheses"
weight: 32
#id: "longest-valid-parentheses"
---
## Description
<div class="description">
<p>Given a string containing just the characters <code>&#39;(&#39;</code> and <code>&#39;)&#39;</code>, find the length of the longest valid (well-formed) parentheses substring.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;(()&quot;
<strong>Output:</strong> 2
<strong>Explanation:</strong> The longest valid parentheses substring is &quot;()&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;)()())&quot;
<strong>Output:</strong> 4
<strong>Explanation:</strong> The longest valid parentheses substring is &quot;()()&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;&quot;
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= s.length &lt;= 3 * 10<sup>4</sup></code></li>
	<li><code>s[i]</code> is <code>&#39;(&#39;</code>, or <code>&#39;)&#39;</code>.</li>
</ul>

</div>

## Tags
- String (string)
- Dynamic Programming (dynamic-programming)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Adobe - 4 (taggedByAdmin: false)
- Samsung - 4 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)

## Official Solution
[TOC]
## Summary

We need to determine the length of the largest valid substring of parentheses from a given string.

## Solution
---
#### Approach 1: Brute Force

**Algorithm**

In this approach, we consider every possible non-empty even length substring from the given string and check whether it's
a valid string of parentheses or not. In order to check the validity, we use the Stack's Method.

Every time we
encounter a $$\text{‘(’}$$, we push it onto the stack. For every $$\text{‘)’}$$ encountered, we pop a $$\text{‘(’}$$ from the stack. If $$\text{‘(’}$$ isn't
 available on the stack for popping at anytime or if stack contains some elements after processing complete substring, the substring of parentheses is invalid. In this way, we repeat the
 process for every possible substring and we keep on
  storing the length of the longest valid string found so far.
```
Example:
"((())"

(( --> invalid
(( --> invalid
() --> valid, length=2
)) --> invalid
((()--> invalid
(())--> valid, length=4
maxlength=4
```

<iframe src="https://leetcode.com/playground/smDecW2X/shared" frameBorder="0" width="100%" height="497" name="smDecW2X"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^3)$$. Generating every possible substring from a string of length $$n$$ requires $$O(n^2)$$. Checking validity of a string of length $$n$$ requires $$O(n)$$.

* Space complexity : $$O(n)$$. A stack of depth $$n$$ will be required for the longest substring.
<br />
<br />
---

#### Approach 2: Using Dynamic Programming

**Algorithm**

This problem can be solved by using Dynamic Programming. We make use of a $$\text{dp}$$ array where $$i$$th element of $$\text{dp}$$ represents the length of the longest valid substring ending at $$i$$th index. We initialize the complete $$\text{dp}$$ array with 0's. Now, it's obvious that the valid substrings must end with $$\text{‘)’}$$. This further leads to the conclusion that the substrings ending with $$\text{‘(’}$$ will always contain '0' at their corresponding $$\text{dp}$$ indices. Thus, we update the $$\text{dp}$$ array only when $$\text{‘)’}$$ is encountered.

To fill $$\text{dp}$$ array we will check every two consecutive characters of the string and if

1. $$\text{s}[i] = \text{‘)’}$$ and $$\text{s}[i - 1] = \text{‘(’}$$, i.e. string looks like $$``.......()" \Rightarrow$$

    $$
    \text{dp}[i]=\text{dp}[i-2]+2
    $$

    We do so because the ending "()" portion is a valid substring anyhow and leads to an increment of 2 in the length of the just previous valid substring's length.

2. $$\text{s}[i] = \text{‘)’}$$ and $$\text{s}[i - 1] = \text{‘)’}$$, i.e. string looks like $$``.......))" \Rightarrow$$

    if $$\text{s}[i - \text{dp}[i - 1] - 1] = \text{‘(’}$$ then

    $$
    \text{dp}[i]=\text{dp}[i-1]+\text{dp}[i-\text{dp}[i-1]-2]+2
    $$

   The reason behind this is that if the 2nd last $$\text{‘)’}$$ was a part of a valid substring (say $$sub_s$$), for the last $$\text{‘)’}$$ to be a part of a larger substring, there must be a corresponding starting $$\text{‘(’}$$ which lies before the valid substring of which the 2nd last $$\text{‘)’}$$ is a part (i.e. before $$sub_s$$). Thus, if the character before $$sub_s$$ happens to be $$\text{‘(’}$$, we update the $$\text{dp}[i]$$ as an addition of $$2$$ in the length of $$sub_s$$ which is $$\text{dp}[i-1]$$. To this, we also add the length of the valid substring just before the term "(,sub_s,)" , i.e. $$\text{dp}[i-\text{dp}[i-1]-2]$$.

For better understanding of this method, see this example:

<!--![Longest_Valid_Parenthesis](../Figures/32_LongestValidParenthesisDP.gif)-->
!?!../Documents/32_Longest_Valid2.json:1000,563!?!

<iframe src="https://leetcode.com/playground/YGuAh4tp/shared" frameBorder="0" width="100%" height="344" name="YGuAh4tp"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Single traversal of string to fill dp array is done.

* Space complexity : $$O(n)$$. dp array of size $$n$$ is used.
<br />
<br />
---
#### Approach 3: Using Stack

**Algorithm**

Instead of finding every possible string and checking its validity, we can make use of stack while scanning
the given string to check if the string scanned so far is valid, and also the length of the longest valid string. In order to do so, we start by pushing $$-1$$ onto the stack.

 For every $$\text{‘(’}$$ encountered, we push its index onto the stack.

 For every $$\text{‘)’}$$ encountered, we pop the topmost element and subtract the current element's index from the top element of the stack, which gives the length of the currently encountered valid string of parentheses. If while popping the element, the stack becomes empty, we push the current element's index onto the stack. In this way, we keep on calculating the lengths of the valid substrings, and return the length of the longest valid string at the end.

See this example for better understanding.

<!--![Longest_Valid_Parenthesis](../Figures/32_LongestValidParenthesisSTACK.gif)-->
!?!../Documents/32_Longest_Valid_stack_new.json:1000,563!?!

<iframe src="https://leetcode.com/playground/A2oPe4yE/shared" frameBorder="0" width="100%" height="412" name="A2oPe4yE"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. $$n$$ is the length of the given string..

* Space complexity : $$O(n)$$. The size of stack can go up to $$n$$.
<br />
<br />
---
#### Approach 4: Without extra space

**Algorithm**

In this approach, we make use of two counters $$left$$ and $$right$$. First, we start traversing the string from the left towards the right and for every $$\text{‘(’}$$ encountered, we increment the $$left$$ counter and for every $$\text{‘)’}$$ encountered, we increment the $$right$$ counter. Whenever $$left$$ becomes equal to $$right$$, we calculate the length of the current valid string and keep track of maximum length substring found so far. If $$right$$ becomes greater than $$left$$ we reset $$left$$ and $$right$$ to $$0$$.

Next, we start traversing the string from right to left and similar procedure is applied.

Example of this approach:

<!--![Longest_Valid_Parenthesis](../Figures/32_LongestValidParenthesisLR.gif)-->
!?!../Documents/32_Longest_Validlr.json:1000,563!?!

<iframe src="https://leetcode.com/playground/RsBpRHK7/shared" frameBorder="0" width="100%" height="500" name="RsBpRHK7"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Two traversals of the string.

* Space complexity : $$O(1)$$. Only two extra variables $$left$$ and $$right$$ are needed.

## Accepted Submission (python3)
```python3
class Solution:
    def longestValidParentheses(self, s):
        """
        :type s: str
        :rtype: int
        """
        longest = 0
        stack = [0]
        for c in s:
            if c == '(':
                stack.append(0)
            else:
                if len(stack) > 1:
                    count = stack.pop()
                    stack[-1] += count + 2
                    longest = max(longest, stack[-1])
                else:
                    stack = [0]
        return longest
```

## Top Discussions
### My O(n) solution using a stack
- Author: cjxxm
- Creation Date: Tue Jul 15 2014 23:35:48 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 17:10:26 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        int longestValidParentheses(string s) {
            int n = s.length(), longest = 0;
            stack<int> st;
            for (int i = 0; i < n; i++) {
                if (s[i] == '(') st.push(i);
                else {
                    if (!st.empty()) {
                        if (s[st.top()] == '(') st.pop();
                        else st.push(i);
                    }
                    else st.push(i);
                }
            }
            if (st.empty()) longest = n;
            else {
                int a = n, b = 0;
                while (!st.empty()) {
                    b = st.top(); st.pop();
                    longest = max(longest, a-b-1);
                    a = b;
                }
                longest = max(longest, a);
            }
            return longest;
        }
    };

The workflow of the solution is as below.

 1. Scan the string from beginning to end.  
 2. If current character is '(',
    push its index to the stack. If current character is ')' and the
    character at the index of the top of stack is '(', we just find a
    matching pair so pop from the stack. Otherwise, we push the index of
    ')' to the stack.
 3. After the scan is done, the stack will only
    contain the indices of characters which cannot be matched. Then
    let's use the opposite side -  substring between adjacent indices
    should be valid parentheses. 
 4. If the stack is empty, the whole input
    string is valid. Otherwise, we can scan the stack to get longest
    valid substring as described in step 3.
</p>


### My DP, O(n) solution without using stack
- Author: jerryrcwong
- Creation Date: Fri Jul 25 2014 17:38:46 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 12:45:14 GMT+0800 (Singapore Standard Time)

<p>
My solution uses DP. The main idea is as follows: I construct a array <b>longest[]</b>, for any longest[i], it stores the longest length of valid parentheses which is end at i.
<br>And the DP idea is :
<br> If s[i] is '(', set longest[i] to 0,because any string end with '(' cannot be a valid one.
<br>Else if s[i] is ')'
<br>\xa0\xa0\xa0\xa0 If s[i-1] is '(', longest[i] = longest[i-2] + 2
<br>\xa0\xa0\xa0\xa0 Else if s[i-1] is ')' **and s[i-longest[i-1]-1] == '('**, longest[i] = longest[i-1] + 2 + longest[i-longest[i-1]-2]
<br> For example, input "()(())", at i = 5, longest array is [0,2,0,0,2,0], longest[5] = longest[4] + 2 + longest[1] = 6.
<br>
 

       int longestValidParentheses(string s) {
                if(s.length() <= 1) return 0;
                int curMax = 0;
                vector<int> longest(s.size(),0);
                for(int i=1; i < s.length(); i++){
                    if(s[i] == ')'){
                        if(s[i-1] == '('){
                            longest[i] = (i-2) >= 0 ? (longest[i-2] + 2) : 2;
                            curMax = max(longest[i],curMax);
                        }
                        else{ // if s[i-1] == ')', combine the previous length.
                            if(i-longest[i-1]-1 >= 0 && s[i-longest[i-1]-1] == '('){
                                longest[i] = longest[i-1] + 2 + ((i-longest[i-1]-2 >= 0)?longest[i-longest[i-1]-2]:0);
                                curMax = max(longest[i],curMax);
                            }
                        }
                    }
                    //else if s[i] == '(', skip it, because longest[i] must be 0
                }
                return curMax;
            }

Updated: thanks to **Philip0116**, I have a more concise solution(though this is not as readable as the above one, but concise):

    int longestValidParentheses(string s) {
            if(s.length() <= 1) return 0;
            int curMax = 0;
            vector<int> longest(s.size(),0);
            for(int i=1; i < s.length(); i++){
                if(s[i] == ')' && i-longest[i-1]-1 >= 0 && s[i-longest[i-1]-1] == '('){
                        longest[i] = longest[i-1] + 2 + ((i-longest[i-1]-2 >= 0)?longest[i-longest[i-1]-2]:0);
                        curMax = max(longest[i],curMax);
                }
            }
            return curMax;
        }
</p>


### Simple JAVA solution, O(n) time, one stack
- Author: nmjnmjnmj
- Creation Date: Tue Jan 13 2015 12:45:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 23 2018 04:24:40 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public int longestValidParentheses(String s) {
        Stack<Integer> stack = new Stack<Integer>();
        int max=0;
        int left = -1;
        for(int j=0;j<s.length();j++){
            if(s.charAt(j)=='(') stack.push(j);            
            else {
                if (stack.isEmpty()) left=j;
                else{
                    stack.pop();
                    if(stack.isEmpty()) max=Math.max(max,j-left);
                    else max=Math.max(max,j-stack.peek());
                }
            }
        }
        return max;
    }
}
```
</p>


