---
title: "Remove Max Number of Edges to Keep Graph Fully Traversable"
weight: 1442
#id: "remove-max-number-of-edges-to-keep-graph-fully-traversable"
---
## Description
<div class="description">
<p>Alice and Bob have an undirected graph of&nbsp;<code>n</code>&nbsp;nodes&nbsp;and 3 types of edges:</p>

<ul>
	<li>Type 1: Can be traversed by Alice only.</li>
	<li>Type 2: Can be traversed by Bob only.</li>
	<li>Type 3: Can by traversed by both Alice and Bob.</li>
</ul>

<p>Given an array&nbsp;<code>edges</code>&nbsp;where&nbsp;<code>edges[i] = [type<sub>i</sub>, u<sub>i</sub>, v<sub>i</sub>]</code>&nbsp;represents a bidirectional edge of type&nbsp;<code>type<sub>i</sub></code>&nbsp;between nodes&nbsp;<code>u<sub>i</sub></code>&nbsp;and&nbsp;<code>v<sub>i</sub></code>, find the maximum number of edges you can remove so that after removing the edges, the graph can still be fully traversed by both Alice and Bob. The graph is fully traversed by Alice and Bob if starting from any node, they can reach all other nodes.</p>

<p>Return <em>the maximum number of edges you can remove, or return</em> <code>-1</code> <em>if it&#39;s impossible for the graph to be fully traversed by Alice and Bob.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/08/19/ex1.png" style="width: 179px; height: 191px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 4, edges = [[3,1,2],[3,2,3],[1,1,3],[1,2,4],[1,1,2],[2,3,4]]
<strong>Output:</strong> 2
<strong>Explanation: </strong>If we remove the 2 edges [1,1,2] and [1,1,3]. The graph will still be fully traversable by Alice and Bob. Removing any additional edge will not make it so. So the maximum number of edges we can remove is 2.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/08/19/ex2.png" style="width: 178px; height: 190px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 4, edges = [[3,1,2],[3,2,3],[1,1,4],[2,1,4]]
<strong>Output:</strong> 0
<strong>Explanation: </strong>Notice that removing any edge will not make the graph fully traversable by Alice and Bob.
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/08/19/ex3.png" style="width: 178px; height: 190px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 4, edges = [[3,2,3],[1,1,2],[2,3,4]]
<strong>Output:</strong> -1
<b>Explanation: </b>In the current graph, Alice cannot reach node 4 from the other nodes. Likewise, Bob cannot reach 1. Therefore it&#39;s impossible to make the graph fully traversable.</pre>

<p>&nbsp;</p>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
	<li><code>1 &lt;= edges.length &lt;= min(10^5, 3 * n * (n-1) / 2)</code></li>
	<li><code>edges[i].length == 3</code></li>
	<li><code>1 &lt;= edges[i][0] &lt;= 3</code></li>
	<li><code>1 &lt;= edges[i][1] &lt; edges[i][2] &lt;= n</code></li>
	<li>All tuples&nbsp;<code>(type<sub>i</sub>, u<sub>i</sub>, v<sub>i</sub>)</code>&nbsp;are distinct.</li>
</ul>

</div>

## Tags
- Union Find (union-find)

## Companies
- Uber - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++ / Java] Textbook Union-Find Data Structure, Code with Explanation and comments
- Author: interviewrecipes
- Creation Date: Sun Sep 06 2020 12:02:03 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 08 2020 12:34:48 GMT+0800 (Singapore Standard Time)

<p>
The idea here is to think that initially the graph is empty and now we want to add the edges into the graph such that graph is connected.

Union-Find is an easiest way to solve such problem where we start with all nodes in separate components and merge the nodes as we add edges into the graph.

As some edges are available to only Bob while some are available only to Alice, we will have two different union find objects to take care of their own traversability.

Key thing to remember is that we should prioritize type 3 edges over type 1 and 2 because they help both of them at the same time.
**C++**
```
/* You can simply plug in this class any many different codes. This class is a generic implementation of union-find. */
class UnionFind {
    vector<int> component;
    int distinctComponents;
public:
    /*
     *   Initially all \'n\' nodes are in different components.
     *   e.g. component[2] = 2 i.e. node 2 belong to component 2.
     */
    UnionFind(int n) {
	    distinctComponents = n;
        for (int i=0; i<=n; i++) {
            component.push_back(i);
        }
    }
    
    /*
     *   Returns true when two nodes \'a\' and \'b\' are initially in different
     *   components. Otherwise returns false.
     */
    bool unite(int a, int b) {       
        if (findComponent(a) == findComponent(b)) {
            return false;
        }
        component[findComponent(a)] = b;
        distinctComponents--;
        return true;
    }
    
    /*
     *   Returns what component does the node \'a\' belong to.
     */
    int findComponent(int a) {
        if (component[a] != a) {
            component[a] = findComponent(component[a]);
        }
        return component[a];
    }
    
    /*
     *   Are all nodes united into a single component?
     */
    bool united() {
        return distinctComponents == 1;
    }
};



// ----------------- Actual Solution --------------
class Solution {
    
public:
    int maxNumEdgesToRemove(int n, vector<vector<int>>& edges) {
        // Sort edges by their type such that all type 3 edges will be at the beginning.
        sort(edges.begin(), edges.end(), [] (vector<int> &a, vector<int> &b) { return a[0] > b[0]; });
        
        int edgesAdded = 0; // Stores the number of edges added to the initial empty graph.
        
        UnionFind bob(n), alice(n); // Track whether bob and alice can traverse the entire graph,
                                    // are there still more than one distinct components, etc.
        
        for (auto &edge: edges) { // For each edge -
            int type = edge[0], one = edge[1], two = edge[2];
            switch(type) {
                case 3:
                    edgesAdded += (bob.unite(one, two) | alice.unite(one, two));
                    break;
                case 2:
                    edgesAdded += bob.unite(one, two);
                    break;
                case 1:
                    edgesAdded += alice.unite(one, two);
                    break;
            }
        }
        
        return (bob.united() && alice.united()) ? (edges.size()-edgesAdded) : -1; // Yay, solved.
    }
};
```

**Java** (Credits to @binlei)
```
class Solution {
    public int maxNumEdgesToRemove(int n, int[][] edges) {
        Arrays.sort(edges, (a, b) -> b[0] - a[0]);
        
        int edgeAdd = 0;
        
        UnionFind alice = new UnionFind(n);
        UnionFind bob = new UnionFind(n);
        
        for (int[] edge : edges) {
            int type = edge[0];
            int a = edge[1];
            int b = edge[2];
            
            switch (type) {
                case 3:
                    if (alice.unite(a, b) | bob.unite(a, b)) {
                        edgeAdd++;
                    }
                    break;
                case 2:
                    if (bob.unite(a, b)) {
                        edgeAdd++;
                    }
                    break;
                case 1:
                    if (alice.unite(a, b)) {
                        edgeAdd++;
                    } 
                    break;
            }
        }
        
        return (alice.united() && bob.united()) ? edges.length - edgeAdd : -1;
    }
    
    private class UnionFind {
        int[] component;
        int distinctComponents;
        
        public UnionFind(int n) {
            component = new int[n+1];
            for (int i = 0; i <= n; i++) {
                component[i] = i;
            }
            distinctComponents = n;
        }
        // unite. For example, if previously we have component {0, 4, 4, 4, 4, 6, 7, 7}, then invoke this method with a=1, b=5, then after invoke, {0, 4, 4, 4, 5, 7, 7, 7}
        private boolean unite(int a, int b) {
            if (findComponent(a) != findComponent(b)) {
                component[findComponent(a)] = b;
                distinctComponents--;
                return true;
            }
            
            return false;
        }
        
        // find and change component
        // for example, if previously we have component:{0, 2, 3, 4, 4, 6, 7, 7}, then after invoke this method with a=1, the component become {0, 4, 4, 4, 4, 6, 7, 7}
        private int findComponent(int a) {
            if (component[a] != a) {
                component[a] = findComponent(component[a]);
            }
            return component[a];
        }
        
        private boolean united() {
            return distinctComponents == 1;
        }
        
    }
}
```
</p>


### [Python] Union Find
- Author: lee215
- Creation Date: Sun Sep 06 2020 12:06:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 14:28:39 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Add Type3 first, then check Type 1 and Type 2.
<br>

# **Explanation**
Go through all edges of type 3 (Alice and Bob)
If not necessary to add, increment `res`.
Otherwith increment `e1` and `e2`.

Go through all edges of type 1 (Alice)
If not necessary to add, increment `res`.
Otherwith increment `e1`.

Go through all edges of type 2 (Bob)
If not necessary to add, increment `res`.
Otherwith increment `e2`.

If Alice\'s\'graph is connected, `e1 == n - 1` should valid.
If Bob\'s graph is connected, `e2 == n - 1` should valid.
In this case we return `res`,
otherwise return `-1`.
<br>

# Complexity
Time O(E), if union find with compression and rank
Space O(E)
<br>

**Python:**
```py
    def maxNumEdgesToRemove(self, n, edges):
        # Union find
        def find(i):
            if i != root[i]:
                root[i] = find(root[i])
            return root[i]

        def uni(x, y):
            x, y = find(x), find(y)
            if x == y: return 0
            root[x] = y
            return 1

        res = e1 = e2 = 0

        # Alice and Bob
        root = range(n + 1)
        for t, i, j in edges:
            if t == 3:
                if uni(i, j):
                    e1 += 1
                    e2 += 1
                else:
                    res += 1
        root0 = root[:]

        # only Alice
        for t, i, j in edges:
            if t == 1:
                if uni(i, j):
                    e1 += 1
                else:
                    res += 1

        # only Bob
        root = root0
        for t, i, j in edges:
            if t == 2:
                if uni(i, j):
                    e2 += 1
                else:
                    res += 1

        return res if e1 == e2 == n - 1 else -1
```

</p>


### [Java] Union-find solution
- Author: marvinbai
- Creation Date: Sun Sep 06 2020 12:00:50 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 07 2020 03:12:26 GMT+0800 (Singapore Standard Time)

<p>
When iterating over each edge, we check if the roots of the two nodes are the same: 

* If not, then this is a critical path which cannot be removed. And we connect these two nodes.

* If yes, then this is a redundant path which can be removed. The result needs to increment by 1.

Here order matters. Therefore we need to sort the edge by type. Type 3 comes first, then type 1 or 2. For type 3 edge, if and only if it is redundant for both players, then we can say that this path can be removed, otherwise not.

Another thing we need to check is whether the original graph is traversable for both players. So whenever we connect two new nodes, we decrease the total number of components in the graph by 1. The graph is only traversable when the total number of components is 1.

```
class Solution {
    public int maxNumEdgesToRemove(int n, int[][] edges) {
        Arrays.sort(edges, (a, b) -> (b[0] - a[0]));
        int[] roots1 = new int[n + 1];
        int[] roots2 = new int[n + 1];
        for(int i = 1; i <= n; i++) {
            roots1[i] = i;
            roots2[i] = i;
        }
        int n1 = n, n2 = n; // Number of components for two players.
        int res = 0;
        for(int[] e : edges) {
            if(e[0] == 1) {
                int root_a = find(e[1], roots1);
                int root_b = find(e[2], roots1);
                if(root_a == root_b) { // If roots are the same, then this is a redundant edge and can be removed.
                    res++;
                } else {
                    roots1[root_a] = root_b; // If roots are different, we connect two different components.
                    n1--;
                }
            } else if(e[0] == 2) {
                int root_a = find(e[1], roots2);
                int root_b = find(e[2], roots2);
                if(root_a == root_b) {
                    res++;
                } else {
                    roots2[root_a] = root_b;
                    n2--;
                }
            } else {
                int root_a1 = find(e[1], roots1);
                int root_b1 = find(e[2], roots1);
                int root_a2 = find(e[1], roots2);
                int root_b2 = find(e[2], roots2);
                if(root_a1 != root_b1) {
                    roots1[root_a1] = root_b1;
                    n1--;
                }
                if(root_a2 != root_b2) {
                    roots2[root_a2] = root_b2;
                    n2--;
                }
                if(root_a1 == root_b1 && root_a2 == root_b2) {
                    res++;
                }
            }
        }
        if(n1 != 1 || n2 != 1) return -1; // If total number of components is not one for either players, return -1.
        return res;
    }
    
    private int find(int i, int[] roots) {
        int j = i;
        while(roots[i] != i) {
            i = roots[i];
        }
        roots[j] = i;
        return i;
    }
}
```
</p>


