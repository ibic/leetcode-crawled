---
title: "Winning Candidate"
weight: 1486
#id: "winning-candidate"
---
## Description
<div class="description">
<p>Table: <code>Candidate</code></p>

<pre>
+-----+---------+
| id  | Name    |
+-----+---------+
| 1   | A       |
| 2   | B       |
| 3   | C       |
| 4   | D       |
| 5   | E       |
+-----+---------+  
</pre>

<p>Table: <code>Vote</code></p>

<pre>
+-----+--------------+
| id  | CandidateId  |
+-----+--------------+
| 1   |     2        |
| 2   |     4        |
| 3   |     3        |
| 4   |     2        |
| 5   |     5        |
+-----+--------------+
id is the auto-increment primary key,
CandidateId is the id appeared in Candidate table.
</pre>

<p>Write a sql to find the name of the winning candidate, the above example will return the winner <code>B</code>.</p>

<pre>
+------+
| Name |
+------+
| B    |
+------+
</pre>

<p><b>Notes:</b></p>

<ol>
	<li>You may assume <b>there is no tie</b>, in other words there will be <b>only one</b> winning candidate.</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags


## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach: Using `JOIN` and a temporary table [Accepted]

**Algorithm**

Query in the **Vote** table to get the winner's id and then join it with the **Candidate** table to get the name.

**MySQL**

```sql
SELECT
    name AS 'Name'
FROM
    Candidate
        JOIN
    (SELECT
        Candidateid
    FROM
        Vote
    GROUP BY Candidateid
    ORDER BY COUNT(*) DESC
    LIMIT 1) AS winner
WHERE
    Candidate.id = winner.Candidateid
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A stupid testing case
- Author: Peter233767
- Creation Date: Thu Sep 20 2018 21:51:03 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 23:57:08 GMT+0800 (Singapore Standard Time)

<p>
There is a case when people vote for someone who is not in the candidate. The solution is finding out the winner id from vote and match to candidate. Simple join will not work.
# Write your MySQL query statement below
SELECT C.Name FROM Candidate C
WHERE id = (SELECT CandidateId FROM Vote GROUP BY CandidateId ORDER BY COUNT(id) DESC LIMIT 1);
</p>


### my accepted solution
- Author: zenmzn
- Creation Date: Wed May 03 2017 00:48:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 08:46:00 GMT+0800 (Singapore Standard Time)

<p>
```
Select distinct c.Name As Name
from Candidate c
where c.id = (Select CandidateId 
from Vote
Group by CandidateId  
order by count(CandidateId) desc
limit 1)








```
</p>


### What about a tie?
- Author: ramcruise2
- Creation Date: Tue Mar 05 2019 02:29:18 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 05 2019 02:29:18 GMT+0800 (Singapore Standard Time)

<p>
All the solutions are assuming that there is never a tie I guess.

This takes care of the case when there is a tie too.

select C.Name from
Candidate C
JOIN(
select CandidateId, dense_rank() OVER (ORDER BY count(CandidateId) desc) as rnk from Vote
GROUP BY CandidateId)Voting 
ON C.id = Voting.CandidateId
WHERE Voting.rnk=1
</p>


