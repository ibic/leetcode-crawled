---
title: "House Robber III"
weight: 320
#id: "house-robber-iii"
---
## Description
<div class="description">
<p>The thief has found himself a new place for his thievery again. There is only one entrance to this area, called the &quot;root.&quot; Besides the root, each house has one and only one parent house. After a tour, the smart thief realized that &quot;all houses in this place forms a binary tree&quot;. It will automatically contact the police if two directly-linked houses were broken into on the same night.</p>

<p>Determine the maximum amount of money the thief can rob tonight without alerting the police.</p>

<p><b>Example 1:</b></p>

<pre>
<strong>Input: </strong>[3,2,3,null,3,null,1]

     <font color="red">3</font>
    / \
   2   3
    \   \ 
     <font color="red">3   1
</font>
<strong>Output:</strong> 7 
<strong>Explanation:</strong>&nbsp;Maximum amount of money the thief can rob = <font color="red" style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;">3</font><span style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;"> + </span><font color="red" style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;">3</font><span style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;"> + </span><font color="red" style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;">1</font><span style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;"> = </span><b style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;">7</b><span style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;">.</span></pre>

<p><b>Example 2:</b></p>

<pre>
<strong>Input: </strong>[3,4,5,1,3,null,1]

&nbsp;    3
    / \
   <font color="red">4</font>   <font color="red">5</font>
  / \   \ 
 1   3   1

<strong>Output:</strong> 9
<strong>Explanation:</strong>&nbsp;Maximum amount of money the thief can rob = <font color="red">4</font> + <font color="red">5</font> = <b>9</b>.
</pre>
</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Google - 5 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

### Overview

This problem is an extension of the original [House Robber Problem](https://leetcode.com/problems/house-robber/). In this problem, our poor thief does not have a straight street, but have a binary-tree-like area instead. 

To help better understand the problem, below is an example robbing plan on a binary tree:

![one example robbing](../Figures/337/337_overview.png)

Well, it looks a bit more complicated. For problems related to the tree data structure, often we could apply the recursion. Also, due to the characteristic of this problem, the [memoization](https://en.wikipedia.org/wiki/Memoization) approach and the DP approach are available.

Below, we will discuss three approaches: *Recursion* , *Recursion with Memoization*, and *Dynamic Programming*. They are similar but have some differences. Generally, we recommend the first and the second approaches, and provide the third approach as an extension reading for interested readers.

---

#### Approach 1: Recursion

**Intuition**

In this part, we explain how to think of this approach step by step. If you are only interested in the pure algorithm, you can jump to the algorithm part.

Since the tree itself is a recursive data structure, usually recursion is used to tackle these tree-relative problems.

Now we need a recursive function, let's call it `helper` (or whatever you want to call it).

Usually, we use a node as the input to the `helper` and add other arguments if we need more information.

>The pseudo code of the common structure to solve recursive problems is as below:

<pre>
function helper(node, other_information) {
    // basic case, such as node is null
    if node is null:
        return things like 0 or null
    else:
        do something relates to helper(node.left) and helper(node.right)
}
function answerToProblem(root) {
    return helper(root, other_information)
}
</pre>

>In some cases, we can use `answerToProblem` itself as the `helper` function.

OK, back to our problem. The next question is what should our `helper` function return.

Since the problem asks us to find out the maximum amount of money the thief can get, we could try using this maximum value as the return value of the `helper` function.

So `helper` receives a `node` as input, and returns the maximum amount the thief can get starting from this `node`.

Let's try writing the actual code. Well, it's a bit of trouble...

<pre>
function helper(node) { // return the maximum by starting from this node
    if node is null: // basic case
        return 0
    else:
        two choices: rob this node or not?
        if not rob... we have: helper(node.left) + helper(node.right)

        what if rob? we get node.val!
        what about node.left and node.right? we can not rob them.
        Hmm... maybe we need to touch node.left.left and its other siblings... troublesome!
}
</pre>

If we need to touch the grandchildren of this node, the case becomes complicated. Well, it is not infeasible but requires extra effort and code. Often, the best practice is to only touch its children, not its grandchildren.

The ideal situation is to make `node.left` and `node.right` automatically handle the grandchildren for us.

How to do it? Well, we can let them know whether we robbed this node or not by passing this information as input, like this:

<pre>
    two choices: rob this node or not?

    rob = node.val + helper(node.left, parent_robbed=True)
                + helper(node.right,  parent_robbed=True)

    not_rob = helper(node.left, parent_robbed=False)
            + helper(node.right, parent_robbed=False)

    return max(rob, not_rob)
</pre>

Cool, we also need to change the input correspondingly:

<pre>
function helper(node, the parent is robbed or not?) {
    // return the maximum by starting from this node
    tackle basic case...

    if the parent is robbed:
        we can not rob this node.
        return helper(node.left, True) + helper(node.right, True)

    if the parent is not robbed:
        two choices: rob this node or not?
        calculate `rob` and `not_rob`...
        return max(rob, not_rob)
}
</pre>

Good, now we have a functioning code. But the code is still not perfect.

An obvious problem is that the `helper` is called too many times. Ideally, it should only be called as least as possible to reduce redundant calculations.

For example, when calculating `rob` and `not_rob`:

<pre>
    rob = node.val + helper(node.left, True) + helper(node.right, True)
    not_rob = helper(node.left, False) + helper(node.right, False)
</pre>

The `helper` is called four times. Moreover, when we call `helper(node.left, True)` and `helper(node.left, False)`, those two involve same calculations internally, such as `helper(node.left.left, False)`. 

>In other words, `helper(node.left.left, False)` is called inside `helper(node.left, True)`, and also is called inside `helper(node.left, False)`. It is calculated twice! We do not want that.

What if... we can combine them together?

We return the results of `helper(node.left, True)` and `helper(node.left, False)` in a single function: `helper(node.left)`. Those two results can be stored in a two-element array.

<pre>
function helper(node) {
    // return original [`helper(node.left, True)`, `helper(node.left, False)`]
    tackle basic case...
    left = helper(node.left)
    right = helper(node.right)
    some calculation...
    return [max_if_rob, max_if_not_rob]
}
</pre>

In this case, we fully use the calculation results without redundant calculation.

>Also, you can reduce extra calculations by [memoization](https://en.wikipedia.org/wiki/Memoization) or by DP, which we will discuss in the following approaches.

**Algorithm**

Use a helper function which receives a node as input and returns a two-element array, where the first element represents the maximum amount of money the thief can rob if starting from this node without robbing this node, and the second element represents the maximum amount of money the thief can rob if starting from this node and robbing this node.

The basic case of the helper function should be `null` node, and in this case, it returns two zeros.

Finally, call the `helper(root)` in the main function, and return its maximum value.

**Implementation**

<iframe src="https://leetcode.com/playground/Z9YAG6aa/shared" frameBorder="0" width="100%" height="412" name="Z9YAG6aa"></iframe>

**Complexity Analysis**

Let $$N$$ be the number of nodes in the binary tree.

* Time complexity: $$\mathcal{O}(N)$$ since we visit all nodes once.

* Space complexity: $$\mathcal{O}(N)$$ since we need stacks to do recursion, and the maximum depth of the recursion is the height of the tree, which is $$\mathcal{O}(N)$$ in the worst case and $$\mathcal{O}(\log(N))$$ in the best case.

---

#### Approach 2: Recursion with Memoization

**Intuition**

Recall in the later part of approach 1, we have the following `helper` function:

<pre>
function helper(node, the parent is robbed or not?) {
    // return the maximum by starting from this node
    tackle basic case...
    if the parent is robbed:
        return helper(node.left, True) + helper(node.right, True)

    if the parent is not robbed:
        two choices: rob this node or not?
        calculate `rob` and `not_rob`...
        return max(rob, not_rob)
}
</pre>

However, this code is not good since it involves some redundant calculations. Improvements are needed.

To avoid those redundant calculations, using memoization is an option. i.e., save the results of the functions in hash maps, and return the stored results when are called next time.

>You can also add memoization to the "grandchildren" method we mentioned in approach 1. That would be another functioning approach. There is a lot of possibilities.

**Algorithm**
 
Use a helper function which receives a node and a bool variable as input, and if that variable is true, it returns the maximum amount of money the thief can rob if starting from this node and robbing this node, else returns the maximum amount of money the thief can rob if starting from this node without robbing this node.

The result of this helper function would be saved in the maps, and return from the maps when are called next time.

The basic case of the helper function should be `null` node, and in this case, it returns zero.

Finally, call the `helper(root)` in the main function, and return its value.

**Implementation**

<iframe src="https://leetcode.com/playground/oEGJWXSy/shared" frameBorder="0" width="100%" height="500" name="oEGJWXSy"></iframe>


**Complexity Analysis**

Let $$N$$ be the number of nodes in the binary tree.

* Time complexity: $$\mathcal{O}(N)$$ since we run the `helper` function for all nodes once, and saved the results to prevent the second calculation.

* Space complexity: $$\mathcal{O}(N)$$ since we need two maps with the size of $$\mathcal{O}(N)$$ to store the results, and $$\mathcal{O}(N)$$ space for stacks to start recursion.

---

#### Approach 3: Dynamic Programming

**Intuition**

As we mentioned before, we could also apply the Dynamic Programming (DP) algorithm to this problem. Tree DP is a good method to tackle some tree problems.

>Technically, the memoization approach we mentioned above can also be viewed as a DP approach. However, in this approach, we use DP arrays to generalize it.

>This approach is a little hard, and you can treat it as extra reading for interested readers.

To perform a tree DP, we still need DP arrays, and by filling these arrays to get the final result.

Here we create two arrays: `dp_rob` and `dp_not_rob`.

`dp_rob[i]` represents the max money we can rob if we start from node `i` and rob this node.

`dp_not_rob[i]` represents the max money we can rob if we start from node `i` and do not rob this node.

Here comes a question: what is node `i`? We do not have an index `i` for each node.

It seems like we need to index or map the nodes in the tree to some integers.

Well, there are many available mapping methods. Of course, you can choose whatever kind of mapping you want.

Below is the mapping we use in this approach: From left to right, from up to down:

![the example mapping](../Figures/337/337_approach_2_1.png)

This mapping is used in many situations, such as heap and segment tree, where you store a tree in an array.

The Breadth-First Search (BFS) suffices to construct this mapping.

>In some cases, we are provided with an array of node's values and a hashmap storing the relationship between nodes. If so, we can directly perform the dp since we already have the mapping.
>
>However, in this problem, we have to use a lot of code to construct the mapping.

Also, we need to build a graph to store the relationships between those integers -- the parents and children's relationships:

![the example mapping with graph](../Figures/337/337_approach_2_2.png)

OK. Now we have two questions remained: the basic cases of our DP, and the transition equations.

For the first question, the basic cases should be leaf nodes. For those leaf nodes, `dp_rob[i]` is just `node.val`, and `dp_not_rob[i]` is 0.

For the the transition equations, let's say we need to calculate `dp_rob[i]` and `dp_not_rob[i]`.

If we want to rob node `i`, then we can not rob its children. Each child provides `dp_not_rob[child]`. Therefore:

$$
\text{dp\_rob}_i = \text{tree}_i + \sum_{\text{child} \in \text{graph}_i}\text{dp\_not\_rob}_{\text{child}}
$$

If we do not want to rob node `i`, then we can choose to rob its children or not. In this case, each child provides the maximum of `dp_rob[child]` and `dp_not_rob[child]`.

Therefore:

$$
\text{dp\_not\_rob}_i = \sum_{\text{child} \in \text{graph}_i}\max(\text{dp\_rob}_{\text{child}}, \text{dp\_not\_rob}_{\text{child}})
$$

Note that the child's index is always larger than the parent's index in our mapping, so we should iterate the dp arrays backward.

**Algorithm**

Transform the tree from node-based into an array-based `tree` and a map `graph`.

Then create two DP arrays, where `dp_rob[i]` represents the maximum amount of money the thief can rob if starting from node `i` with robbing this node, and `dp_not_rob[i]` represents the maximum amount of money the thief can rob if starting from node `i` without robbing this node.

The transition equations is:

$$
\text{dp\_rob}_i = \text{tree}_i + \sum_{\text{child} \in \text{graph}_i}\text{dp\_not\_rob}_{\text{child}}
$$

$$
\text{dp\_not\_rob}_i = \sum_{\text{child} \in \text{graph}_i}\max(\text{dp\_rob}_{\text{child}}, \text{dp\_not\_rob}_{\text{child}})
$$

Finally, return the maximum of `dp_rob[0]` and `dp_not_rob[0]`.

**Implementation**

<iframe src="https://leetcode.com/playground/FZm8uZLz/shared" frameBorder="0" width="100%" height="500" name="FZm8uZLz"></iframe>


**Complexity Analysis**

Let $$N$$ be the number of nodes in the binary tree.

* Time complexity: $$\mathcal{O}(N)$$ since we visit all nodes once to form the tree-array, and then iterate two DP array, which both have length $$\mathcal{O}(N)$$.

* Space complexity: $$\mathcal{O}(N)$$ since we need an array of length $$\mathcal{O}(N)$$ to store the tree, and two DP arrays of length $$\mathcal{O}(N)$$. Also, the sizes of other data structures in code do not exceed $$\mathcal{O}(N)$$.
*

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Step by step tackling of the problem
- Author: fun4LeetCode
- Creation Date: Mon Mar 14 2016 03:04:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Nov 09 2019 01:46:02 GMT+0800 (Singapore Standard Time)

<p>
**Step I -- Think naively**

At first glance, the problem exhibits the feature of "optimal substructure": if we want to rob maximum amount of money from current binary tree (rooted at `root`), we surely hope that we can do the same to its left and right subtrees. 

So going along this line, let\'s define the function `rob(root)` which will return the maximum amount of money that we can rob for the binary tree rooted at `root`; the key now is to construct the solution to the original problem from solutions to its subproblems, i.e., how to get `rob(root)` from `rob(root.left), rob(root.right), ...` etc.

Apparently the analyses above suggest a recursive solution. And for recursion, it\'s always worthwhile figuring out the following two properties:

 1. Termination condition: when do we know the answer to `rob(root)` without any calculation? Of course when the tree is empty ---- we\'ve got nothing to rob so the amount of money is zero.

 2. Recurrence relation: i.e., how to get `rob(root)` from `rob(root.left), rob(root.right), ...` etc. From the point of view of the tree root, there are only two scenarios at the end: `root` is robbed or is not. If it is, due to the constraint that "we cannot rob any two directly-linked houses", the next level of subtrees that are available would be the four "**grandchild-subtrees**" (`root.left.left, root.left.right, root.right.left, root.right.right`).  However if `root` is not robbed, the next level of available subtrees would just be the two "**child-subtrees**" (`root.left, root.right`). We only need to choose the scenario which yields the larger amount of money.

Here is the program for the ideas above:

    public int rob(TreeNode root) {
        if (root == null) return 0;
        
        int val = 0;
        
        if (root.left != null) {
            val += rob(root.left.left) + rob(root.left.right);
        }
        
        if (root.right != null) {
            val += rob(root.right.left) + rob(root.right.right);
        }
        
        return Math.max(val + root.val, rob(root.left) + rob(root.right));
    }

However the solution runs very slowly (`1186 ms`) and barely got accepted (the time complexity turns out to be exponential, see my [comments](https://discuss.leetcode.com/topic/39834/step-by-step-tackling-of-the-problem/26?page=2) below).

---

**Step II -- Think one step further**

In step `I`, we only considered the aspect of "optimal substructure", but think little about the possibilities of overlapping of the subproblems. For example, to obtain `rob(root)`, we need `rob(root.left), rob(root.right), rob(root.left.left), rob(root.left.right), rob(root.right.left), rob(root.right.right)`; but to get `rob(root.left)`, we also need `rob(root.left.left), rob(root.left.right)`, similarly for `rob(root.right)`. The naive solution above computed these subproblems repeatedly, which resulted in bad time performance. Now if you recall the two conditions for dynamic programming (DP): "**optimal substructure**" + "**overlapping of subproblems**", we actually have a DP problem. A naive way to implement DP here is to use a hash map to record the results for visited subtrees. 

And here is the improved solution:

    public int rob(TreeNode root) {
        return robSub(root, new HashMap<>());
    }
    
    private int robSub(TreeNode root, Map<TreeNode, Integer> map) {
        if (root == null) return 0;
        if (map.containsKey(root)) return map.get(root);
        
        int val = 0;
        
        if (root.left != null) {
            val += robSub(root.left.left, map) + robSub(root.left.right, map);
        }
        
        if (root.right != null) {
            val += robSub(root.right.left, map) + robSub(root.right.right, map);
        }
        
        val = Math.max(val + root.val, robSub(root.left, map) + robSub(root.right, map));
        map.put(root, val);
        
        return val;
    }

The runtime is sharply reduced to `9 ms`, at the expense of `O(n)` space cost (`n` is the total number of nodes; stack cost for recursion is not counted).

---

**Step III -- Think one step back**

In step `I`, we defined our problem as `rob(root)`, which will yield the maximum amount of money that can be robbed of the binary tree rooted at `root`. This leads to the DP problem summarized in step `II`. 

Now let\'s take one step back and ask why we have overlapping subproblems. If you trace all the way back to the beginning, you\'ll find the answer lies in the way how we have defined `rob(root)`. As I mentioned, for each tree root, there are two scenarios: it is robbed or is not. `rob(root)` does not distinguish between these two cases, so "information is lost as the recursion goes deeper and deeper", which results in repeated subproblems.

If we were able to maintain the information about the two scenarios for each tree root, let\'s see how it plays out. Redefine `rob(root)` as a new function which will return an array of two elements, the first element of which denotes the maximum amount of money that can be robbed if `root` is **not robbed**, while the second element signifies the maximum amount of money robbed if it is **robbed**. 

Let\'s relate `rob(root)` to `rob(root.left)` and `rob(root.right)...`, etc. For the 1st element of `rob(root)`, we only need to sum up the larger elements of `rob(root.left)` and `rob(root.right)`, respectively, since `root` is not robbed and we are free to rob its left and right subtrees. For the 2nd element of `rob(root)`, however, we only need to add up the 1st elements of `rob(root.left)` and `rob(root.right)`, respectively, plus the value robbed from `root` itself, since in this case it\'s guaranteed that we cannot rob the nodes of `root.left` and `root.right`. 

As you can see, by keeping track of the information of both scenarios, we decoupled the subproblems and the solution essentially boiled down to a greedy one. Here is the program:

    public int rob(TreeNode root) {
        int[] res = robSub(root);
        return Math.max(res[0], res[1]);
    }
    
    private int[] robSub(TreeNode root) {
        if (root == null) return new int[2];
        
        int[] left = robSub(root.left);
        int[] right = robSub(root.right);
        int[] res = new int[2];

        res[0] = Math.max(left[0], left[1]) + Math.max(right[0], right[1]);
        res[1] = root.val + left[0] + right[0];
        
        return res;
    }
</p>


### Easy understanding solution with dfs
- Author: jiangbowei2010
- Creation Date: Sat Mar 12 2016 08:08:53 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 07:33:43 GMT+0800 (Singapore Standard Time)

<p>
dfs all the nodes of the tree, each node return two number, int[] num, num[0] is the max value while rob this node, num[1] is max value while not rob this value. Current node return value only depend on its children's value. Transform function should be very easy to understand.

    public class Solution {
        public int rob(TreeNode root) {
            int[] num = dfs(root);
            return Math.max(num[0], num[1]);
        }
        private int[] dfs(TreeNode x) {
            if (x == null) return new int[2];
            int[] left = dfs(x.left);
            int[] right = dfs(x.right);
            int[] res = new int[2];
            res[0] = left[1] + right[1] + x.val;
            res[1] = Math.max(left[0], left[1]) + Math.max(right[0], right[1]);
            return res;
        }
    }
</p>


### Simple C++ solution
- Author: espuer
- Creation Date: Fri Mar 25 2016 03:44:28 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 13 2018 06:26:52 GMT+0800 (Singapore Standard Time)

<p>
    /**
     * Definition for a binary tree node.
     * struct TreeNode {
     *     int val;
     *     TreeNode *left;
     *     TreeNode *right;
     *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
     * };
     */
    class Solution {
    public:
        int tryRob(TreeNode* root, int& l, int& r) {
            if (!root)
                return 0;
                
            int ll = 0, lr = 0, rl = 0, rr = 0;
            l = tryRob(root->left, ll, lr);
            r = tryRob(root->right, rl, rr);
            
            return max(root->val + ll + lr + rl + rr, l + r);
        }
    
        int rob(TreeNode* root) {
            int l, r;
            return tryRob(root, l, r);
        }
    };

Basically you want to compare which one is bigger between 1) you + sum of your grandchildren and 2) sum of your children. Personally I like my solution better than the most voted solution because I don't need complex data structures like map.
</p>


