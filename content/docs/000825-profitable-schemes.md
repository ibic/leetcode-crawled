---
title: "Profitable Schemes"
weight: 825
#id: "profitable-schemes"
---
## Description
<div class="description">
<p>There is a group of&nbsp;<code>G</code> members, and a list of various crimes they could commit.</p>

<p>The <code>i<sup>th</sup></code>&nbsp;crime generates a <code>profit[i]</code> and requires <code>group[i]</code>&nbsp;members to participate in it.</p>

<p>If a&nbsp;member participates in one crime, that member can&#39;t participate in another crime.</p>

<p>Let&#39;s call a <em>profitable&nbsp;scheme</em>&nbsp;any subset of these crimes that generates at least <code>P</code> profit, and the total number of&nbsp;members participating in that subset of crimes is at most <code>G</code>.</p>

<p>How many schemes can be chosen?&nbsp; Since the answer may be very&nbsp;large, <strong>return it modulo</strong> <code>10^9 + 7</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>G = <span id="example-input-1-1">5</span>, P = <span id="example-input-1-2">3</span>, group = <span id="example-input-1-3">[2,2]</span>, profit = <span id="example-input-1-4">[2,3]</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>
To make a profit of at least 3, the group could either commit crimes 0 and 1, or just crime 1.
In total, there are 2 schemes.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>G = <span id="example-input-2-1">10</span>, P = <span id="example-input-2-2">5</span>, group = <span id="example-input-2-3">[2,3,5]</span>, profit = <span id="example-input-2-4">[6,7,8]</span>
<strong>Output: </strong><span id="example-output-2">7</span>
<strong>Explanation: </strong>
To make a profit of at least 5, the group could commit any crimes, as long as they commit one.
There are 7 possible schemes: (0), (1), (2), (0,1), (0,2), (1,2), and (0,1,2).
</pre>

<p>&nbsp;</p>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= G &lt;= 100</code></li>
	<li><code>0 &lt;= P &lt;= 100</code></li>
	<li><code>1 &lt;= group[i] &lt;= 100</code></li>
	<li><code>0 &lt;= profit[i] &lt;= 100</code></li>
	<li><code>1 &lt;= group.length = profit.length &lt;= 100</code></li>
</ol>

<div>
<div>&nbsp;</div>
</div>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Dynamic Programming

**Intuition**

We don't care about the profit of the scheme if it is $$\geq P$$, because it surely will be over the threshold of profitability required.  Similarly, we don't care about the number of people required in the scheme if it is $$> G$$, since we know the scheme will be too big for the gang to execute.

As a result, the bounds are small enough to use dynamic programming.  Let's keep track of `cur[p][g]`, the number of schemes with profitability $$p$$ and requiring $$g$$ gang members: except we'll say (without changing the answer) that all schemes that profit *at least* `P` dollars will instead profit *exactly* `P` dollars.

**Algorithm**

Keeping track of `cur[p][g]` as defined above, let's understand how it changes as we consider 1 extra crime that will profit `p0` and require `g0` gang members.  We will put the updated counts into `cur2`.

For each possible scheme with profit `p1` and group size `g1`, that scheme plus the extra crime (`p0, g0`) being considered, has a profit of `p2 = min(p1 + p0, P)`, and uses a group size of `g2 = g1 + g0`.

<iframe src="https://leetcode.com/playground/tdGPqVbN/shared" frameBorder="0" width="100%" height="500" name="tdGPqVbN"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N * P * G)$$, where $$N$$ is the number of crimes available to the gang.

* Space Complexity:  $$O(P * G)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] DP
- Author: lee215
- Creation Date: Sun Jul 29 2018 11:05:02 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 04:40:49 GMT+0800 (Singapore Standard Time)

<p>
Well, it is a Knapsack problem and my first intuition is dp.

`dp[i][j]` means the count of schemes with `i` profit and `j` members.

The dp equation is simple here:
`dp[i + p][j + g] += dp[i][j])` if `i + p < P`
`dp[P][j + g] += dp[i][j])` if `i + p >= P`

Don\'t forget to take care of overflow.

For each pair `(p, g)` of `(profit, group)`, I update the count in `dp`.

**Time Complexity**:
O(NPG)

**C++:**
```
    int profitableSchemes(int G, int P, vector<int> group, vector<int> profit) {
        vector<vector<int>> dp(P + 1, vector<int>(G + 1, 0));
        dp[0][0] = 1;
        int res = 0, mod = 1e9 + 7;
        for (int k = 0; k < group.size(); k++) {
            int g = group[k], p = profit[k];
            for (int i = P; i >= 0; i--)
                for (int j = G - g; j >= 0; j--)
                    dp[min(i + p, P)][j + g] = (dp[min(i + p, P)][j + g] + dp[i][j]) % mod;
        }
        for (int x: dp[P]) res = (res + x) % mod;
        return res;
    }
```

**Java:**
```
    public int profitableSchemes(int G, int P, int[] group, int[] profit) {
        int[][] dp = new int[P + 1][G + 1];
        dp[0][0] = 1;
        int res = 0, mod = (int)1e9 + 7;
        for (int k = 0; k < group.length; k++) {
            int g = group[k], p = profit[k];
            for (int i = P; i >= 0; i--)
                for (int j = G - g; j >= 0; j--)
                    dp[Math.min(i + p, P)][j + g] = (dp[Math.min(i + p, P)][j + g] + dp[i][j]) % mod;
        }
        for (int x : dp[P]) res = (res + x) % mod;
        return res;
    }
```
**Python:**
```
    def profitableSchemes(self, G, P, group, profit):
        dp = [[0] * (G + 1) for i in range(P + 1)]
        dp[0][0] = 1
        for p, g in zip(profit, group):
            for i in range(P, -1, -1):
                for j in range(G - g, -1, -1):
                    dp[min(i + p, P)][j + g] += dp[i][j]
        return sum(dp[P]) % (10**9 + 7)
```
</p>


### Java original 3d to 2d DP solution
- Author: muma2
- Creation Date: Mon Aug 06 2018 10:08:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 05:27:53 GMT+0800 (Singapore Standard Time)

<p>
dp[k][i][j] means for first k crime with i members and ```at least``` j profit, what is total schemes can be chosen.
And we need this ```Math.max(0, j - p)```, because this is for at least j profit.

```dp[k][i][j] = dp[k - 1][i][j] + dp[k - 1][i - current group][Math.max(0, j - current profit)]```

This is 3d original solution:
```
class Solution {
    private int mod = (int)1e9 + 7;
    public int profitableSchemes(int G, int P, int[] group, int[] profit) {
        int[][][] dp = new int[group.length + 1][G + 1][P + 1];
        dp[0][0][0] = 1;
        for (int k = 1; k <= group.length; k++) {
            int g = group[k - 1];
            int p = profit[k - 1];
            for (int i = 0; i <= G; i++) {
                for (int j = 0; j <= P; j++) {
                    dp[k][i][j] = dp[k - 1][i][j];
                    if (i >= g) {
                        dp[k][i][j] = (dp[k][i][j] + dp[k - 1][i - g][Math.max(0, j - p)])%mod;
                    }
                }
            }
        }
        int sum = 0;                                                       
        for(int i = 0; i <= G; i++){
            sum = (sum + dp[group.length][i][P])%mod;
        }
        return sum;
    }
}
```
Because all k dimension only depends on k - 1 dimension, so we can improve it to 2D array which only has i and j dimension.
Just be careful about that i and j should be decrease, in order to get correct old k - 1 dimension value.

```
class Solution {
    private int mod = (int)1e9 + 7;
    public int profitableSchemes(int G, int P, int[] group, int[] profit) {
        int[][] dp = new int[G + 1][P + 1];
        dp[0][0] = 1;
        for (int k = 1; k <= group.length; k++) {
            int g = group[k - 1];
            int p = profit[k - 1];
            for (int i = G; i >= g; i--) {
                for (int j = P; j >= 0; j--) {
                    dp[i][j] = (dp[i][j] + dp[i - g][Math.max(0, j - p)])%mod;
                }
            }
        }
        int sum = 0;                                                       
        for(int i = 0; i <= G; i++){
            sum = (sum + dp[i][P])%mod;
        }
        return sum;
    }
}
```
</p>


### LeetCode Weekly Contest 95 screencast
- Author: cuiaoxiang
- Creation Date: Sun Jul 29 2018 11:04:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 29 2018 11:04:45 GMT+0800 (Singapore Standard Time)

<p>
https://www.youtube.com/watch?v=LKcTXCIh668

I wasted 5+ mins b/c some unknown LeetCode bug in test case 2, problem 3. You can find it from 12:00 to 17:00. Argueable if I\'m more experienced, this bug (not mine) should cost me less time.
</p>


