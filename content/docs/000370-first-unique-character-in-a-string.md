---
title: "First Unique Character in a String"
weight: 370
#id: "first-unique-character-in-a-string"
---
## Description
<div class="description">
<p>Given a string, find the first non-repeating character in it and return its index. If it doesn&#39;t exist, return -1.</p>

<p><b>Examples:</b></p>

<pre>
s = &quot;leetcode&quot;
return 0.

s = &quot;loveleetcode&quot;
return 2.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b> You may assume the string contains only lowercase English letters.</p>

</div>

## Tags
- Hash Table (hash-table)
- String (string)

## Companies
- Goldman Sachs - 19 (taggedByAdmin: false)
- Bloomberg - 16 (taggedByAdmin: true)
- Amazon - 10 (taggedByAdmin: true)
- Facebook - 5 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Zillow - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- Microsoft - 6 (taggedByAdmin: true)
- LinkedIn - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- JPMorgan - 2 (taggedByAdmin: false)
- Huawei - 2 (taggedByAdmin: false)
- Zulily - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Linear time solution

The best possible solution here could be of a linear time 
because to ensure 
that the character is unique 
you have to check the whole string anyway. 

The idea is to go through the string and 
save in a hash map the number of times 
each character appears in the string. 
That would take $$\mathcal{O}(N)$$ time, 
where `N` is a number of characters in the string.
 
And then we go through the string the second time, this time 
we use the hash map as a reference to check if a character 
is unique or not.   
If the character is unique, one could just return its index. 
The complexity of the second iteration is $$\mathcal{O}(N)$$ as well.

!?!../Documents/387_LIS.json:1000,621!?!

<iframe src="https://leetcode.com/playground/B3jVCyWZ/shared" frameBorder="0" width="100%" height="361" name="B3jVCyWZ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since we go 
through the string of length `N` two times. 
* Space complexity : $$\mathcal{O}(1)$$ because English alphabet contains 
26 letters.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 7 lines solution 29ms
- Author: ZachC
- Creation Date: Tue Aug 23 2016 00:53:12 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 12:28:35 GMT+0800 (Singapore Standard Time)

<p>
Hey guys. My solution is pretty straightforward. It takes O(n) and goes through the string twice:
1) Get the frequency of each character.
2) Get the first character that has a frequency of one.

Actually the code below passes all the cases. However, according to @xietao0221, we could change the size of the frequency array to 256 to store other kinds of characters. Thanks for all the other comments and suggestions. Fight on!
```
public class Solution {
    public int firstUniqChar(String s) {
        int freq [] = new int[26];
        for(int i = 0; i < s.length(); i ++)
            freq [s.charAt(i) - 'a'] ++;
        for(int i = 0; i < s.length(); i ++)
            if(freq [s.charAt(i) - 'a'] == 1)
                return i;
        return -1;
    }
}
```
</p>


### Python 3 lines beats 100% (~ 60ms) !
- Author: gregs
- Creation Date: Mon Oct 31 2016 03:07:39 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 08:57:48 GMT+0800 (Singapore Standard Time)

<p>
```   
    def firstUniqChar(self, s):
        """
        :type s: str
        :rtype: int
        """
        
        letters='abcdefghijklmnopqrstuvwxyz'
        index=[s.index(l) for l in letters if s.count(l) == 1]
        return min(index) if len(index) > 0 else -1
```
</p>


### Java One Pass Solution with LinkedHashMap
- Author: nnnomm
- Creation Date: Thu Aug 25 2016 06:23:13 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 10:49:55 GMT+0800 (Singapore Standard Time)

<p>
LinkedHashMap will not be the fastest answer for this question because the input characters are just from 'a' to 'z', but in other situations it might be faster than two pass solutions. I post this just for inspiration.
```
public int firstUniqChar(String s) {
        Map<Character, Integer> map = new LinkedHashMap<>();
        Set<Character> set = new HashSet<>();
        for (int i = 0; i < s.length(); i++) {
            if (set.contains(s.charAt(i))) {
                if (map.get(s.charAt(i)) != null) {
                    map.remove(s.charAt(i));
                }
            } else {
                map.put(s.charAt(i), i);
                set.add(s.charAt(i));
            }
        }
        return map.size() == 0 ? -1 : map.entrySet().iterator().next().getValue();
    }
```
</p>


