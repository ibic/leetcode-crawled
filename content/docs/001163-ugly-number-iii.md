---
title: "Ugly Number III"
weight: 1163
#id: "ugly-number-iii"
---
## Description
<div class="description">
<p>Write a program to find the&nbsp;<code>n</code>-th ugly number.</p>

<p>Ugly numbers are<strong>&nbsp;positive integers</strong>&nbsp;which are divisible by&nbsp;<code>a</code>&nbsp;<strong>or</strong>&nbsp;<code>b</code>&nbsp;<strong>or</strong> <code>c</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 3, a = 2, b = 3, c = 5
<strong>Output:</strong> 4
<strong>Explanation: </strong>The ugly numbers are 2, 3, 4, 5, 6, 8, 9, 10... The 3rd is 4.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 4, a = 2, b = 3, c = 4
<strong>Output:</strong> 6
<strong>Explanation: </strong>The ugly numbers are 2, 3, 4, 6, 8, 9, 10, 12... The 4th is 6.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 5, a = 2, b = 11, c = 13
<strong>Output:</strong> 10
<strong>Explanation: </strong>The ugly numbers are 2, 4, 6, 8, 10, 11, 12, 13... The 5th is 10.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 1000000000, a = 2, b = 217983653, c = 336916467
<strong>Output:</strong> 1999999984
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n, a, b, c &lt;= 10^9</code></li>
	<li><code>1 &lt;= a * b * c &lt;= 10^18</code></li>
	<li>It&#39;s guaranteed that the result will be in range&nbsp;<code>[1,&nbsp;2 * 10^9]</code></li>
</ul>

</div>

## Tags
- Math (math)
- Binary Search (binary-search)

## Companies
- American Express - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [cpp] Binary Search with picture & Binary Search Template
- Author: insomniacat
- Creation Date: Sun Sep 22 2019 12:04:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 23 2019 14:55:41 GMT+0800 (Singapore Standard Time)

<p>
For every integer N, `F(N) = (total number of positive integers <= N which are divisible by a or b or c.).`
![image](https://assets.leetcode.com/users/insomniacat/image_1569133734.png)

`The left-top circle stands for numbers <= N that are divisible by a` 
others the same idea, and the intersection of two/three circles is the total number of positive integers  <= N which are divisible by the` least common multiple` of them.
`F(N) =  a + b + c -  a \u2229 c - a \u2229 b - b \u2229 c + a \u2229 b \u2229 c `
`F(N) =    N/a  + N/b +  N/c - N/lcm(a, c) -  N/lcm(a, b) -   N/lcm(b, c) + N/lcm(a, b, c) `(lcm = least common multiple)
Find the least integer `N` that satisfies the condition `F(N) >= K` 
```
class Solution {
public:    
   int nthUglyNumber(int k, int A, int B, int C) {
        int lo = 1, hi = 2 * (int) 1e9;
        long a = long(A), b = long(B), c = long(C);
        long ab = a * b / __gcd(a, b);
        long bc = b * c / __gcd(b, c);
        long ac = a * c / __gcd(a, c);
        long abc = a * bc / __gcd(a, bc);
        while(lo < hi) {
            int mid = lo + (hi - lo)/2;
            int cnt = mid/a + mid/b + mid/c - mid/ab - mid/bc - mid/ac + mid/abc;
            if(cnt < k) 
                lo = mid + 1;
            else
			   //the condition: F(N) >= k
                hi = mid;
        }
        return lo;
    }
};
```
Complexity
* Time: O(log(2* 1e9))
* Space: O(1)

Update: 
I used to solve the Binary Search problems with this template:
```
while(lo < hi) {
int mid = lo + (hi - lo) / 2;
if(Special condition passed)(optional):
	return mid; 
if(condition passed)
  hi = mid;
else 
  lo = mid + 1;
}
return lo;
```
The key idea is that the range of searching is monotonic, i.e., `If F(a) == true, then for every b > a, F(b) = true`. So our goal is to find the leftmost point a that `F(a) == true`, which can be solved by binary search. In order to find the leftmost point which satisfies the condition, do not break the loop immediately when u find a valid point (There may exist some valid points on the left side). Instead, what u can do is to `narrow the searching range`, and the `lo` point will be the answer. In some cases there\'s no answer and `lo` will return as the initial `hi` value, check that case too.
Most of binary search problems can be solved by the template above.
Try [875](https://leetcode.com/problems/koko-eating-bananas/)
[1011](https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/)
[668](https://leetcode.com/problems/kth-smallest-number-in-multiplication-table/)
[719](https://leetcode.com/problems/find-k-th-smallest-pair-distance/)
Hope it helps.


</p>


### [Java/C++] Binary Search with Venn Diagram Explain Math Formula
- Author: hiepit
- Creation Date: Sun Sep 22 2019 15:55:51 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 07 2020 11:03:16 GMT+0800 (Singapore Standard Time)

<p>
**Formula**
Calculate how many numbers from `1` to `num` are divisible by either `a`, `b` or `c` by using below formula:
`num/a + num/b + num/c \u2013 num/lcm(a, b) \u2013 num/lcm(b, c) \u2013 num/lcm(a, c) + num/lcm(a, b, c)`

**Explain**
![image](https://assets.leetcode.com/users/hiepit/image_1569139496.png)

**Complexity**
- Time: `O(log(MAX_ANS))`, MAX_ANS = 2*10^9
- Space: `O(1)`

**Java**
```java
public class Solution {
    int MAX_ANS = (int) 2e9; // 2*10^9
    public int nthUglyNumber(int n, int a, int b, int c) {
        int left = 0, right = MAX_ANS, result = 0;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (count(mid, a, b, c) >= n) {
                result = mid;
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return result;
    }
    int count(long num, long a, long b, long c) {
        return (int) (num / a + num / b + num / c
                - num / lcm(a, b)
                - num / lcm(b, c)
                - num / lcm(a, c)
                + num / (lcm(a, lcm(b, c))));
    }
    long gcd(long a, long b) {
        if (a == 0) return b;
        return gcd(b % a, a);
    }
    long lcm(long a, long b) {
        return a * b / gcd(a, b);
    }
}
```

**C++**
```c++
typedef long long ll;
#define MAX_ANS 2e9 // 2 * 10^9

class Solution {
public:
    int nthUglyNumber(int n, int a, int b, int c) {
        int left = 0, right = MAX_ANS, result = 0;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (count(mid, a, b, c) >= n) { // find mid as small as possible that count == n
                result = mid;
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return result;
    }
    int count(ll num, ll a, ll b, ll c) {
        return (int)(num / a + num / b + num / c
            - num / lcm(a, b)
            - num / lcm(b, c)
            - num / lcm(a, c)
            + num / (lcm(a, lcm(b, c))));
    }
    ll gcd(ll a, ll b) {
        if (a == 0) return b;
        return gcd(b % a, a);
    }
    ll lcm(ll a, ll b) {
        return a * b / gcd(a, b);
    }
};
```

Thanks for watching. If you have any question, feel free to comment below. :D
</p>


### Math BinarySearch Solution (JAVA)
- Author: Poorvank
- Creation Date: Sun Sep 22 2019 12:20:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 22 2019 12:23:48 GMT+0800 (Singapore Standard Time)

<p>
Calculate how many numbers from 1 to num are divisible by either a, b or c by using the formula: 
`(num / a) + (num / b) + (num / c) \u2013 (num / lcm(a, b)) \u2013 (num / lcm(b, c)) \u2013 (num / lcm(a, c)) + (num / lcm(a, b, c))`
```
 private long gcd(long a, long b) {
        if (a == 0)
            return b;

        return gcd(b % a, a);
    }

    private long lcm(long a, long b) {
        return (a * b) / gcd(a, b);
    }

    private int count(long a, long b, long c, long num) {
        return (int) ((num / a) + (num / b) + (num / c)
                - (num / lcm(a, b))
                - (num / lcm(b, c))
                - (num / lcm(a, c))
                + (num / lcm(a, lcm(b, c)))); // lcm(a,b,c) = lcm(a,lcm(b,c))
    }

    public int nthUglyNumber(int n, int a, int b, int c) {
        int low = 1, high = Integer.MAX_VALUE, mid;

        while (low < high) {
            mid = low + (high - low) / 2;

            if (count((a), b, c, mid) < n)
                low = mid + 1;
            else
                high = mid;
        }

        return high;
    }

```
</p>


