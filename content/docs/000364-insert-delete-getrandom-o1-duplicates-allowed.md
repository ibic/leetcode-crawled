---
title: "Insert Delete GetRandom O(1) - Duplicates allowed"
weight: 364
#id: "insert-delete-getrandom-o1-duplicates-allowed"
---
## Description
<div class="description">
<p>Design a data structure that supports all following operations in <i>average</i> <b>O(1)</b> time.</p>
<b>Note: Duplicate elements are allowed.</b>
<p>
<ol>
<li><code>insert(val)</code>: Inserts an item val to the collection.</li>
<li><code>remove(val)</code>: Removes an item val from the collection if present.</li>
<li><code>getRandom</code>: Returns a random element from current collection of elements. The probability of each element being returned is <b>linearly related</b> to the number of same value the collection contains.</li>
</ol>
</p>

<p><b>Example:</b>
<pre>
// Init an empty collection.
RandomizedCollection collection = new RandomizedCollection();

// Inserts 1 to the collection. Returns true as the collection did not contain 1.
collection.insert(1);

// Inserts another 1 to the collection. Returns false as the collection contained 1. Collection now contains [1,1].
collection.insert(1);

// Inserts 2 to the collection, returns true. Collection now contains [1,1,2].
collection.insert(2);

// getRandom should return 1 with the probability 2/3, and returns 2 with the probability 1/3.
collection.getRandom();

// Removes 1 from the collection, returns true. Collection now contains [1,2].
collection.remove(1);

// getRandom should return 1 and 2 both equally likely.
collection.getRandom();
</pre>
</p>
</div>

## Tags
- Array (array)
- Hash Table (hash-table)
- Design (design)

## Companies
- Facebook - 6 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: false)
- Affirm - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Uber - 5 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Databricks - 3 (taggedByAdmin: false)
- Yelp - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Intuition

We must support three operations with duplicates:

1. `insert`
2. `remove`
3. `getRandom`

To `getRandom` in $$O(1)$$ and have it scale linearly with the number of copies of a value. The simplest solution is to store all values in a list. Once all values are stored, all we have to do is pick a random index.

We don't care about the order of our elements, so `insert` can be done in $$O(1)$$ using a dynamic array (`ArrayList` in Java or `list` in Python).

The issue we run into is how to go about an `O(1)` remove. Generally we learn that removing an element from an array takes a place in $$O(N)$$, unless it is the last element in which case it is $$O(1)$$.

The key here is that _we don't care about order_. For the purposes of this problem, if we want to remove the element at the `i`th index, we can simply swap the `i`th element and the last element, and perform an $$O(1)$$ pop (_technically_ we don't have to swap, we just have to copy the last element into index `i` because it's popped anyway).

With this in mind, the most difficult part of the problem becomes _finding_ the index of the element we have to remove. All we have to do is have an accompanying data structure that maps the element values to their index.

---
#### Approach 1: ArrayList + HashMap

**Algorithm**

We will keep a `list` to store all our elements. In order to make finding the index of elements we want to remove $$O(1)$$, we will use a `HashMap` or dictionary to map values to all indices that have those values. To make this work each value will be mapped to a set of indices. The tricky part is properly updating the `HashMap` as we modify the `list`.

- `insert`: Append the element to the `list` and add the index to `HashMap[element]`.
- `remove`: This is the tricky part. We find the index of the element using the `HashMap`.  We use the trick discussed in the intuition to remove the element from the `list` in $$O(1)$$. Since the last element in the list gets moved around, we have to update its value in the `HashMap`. We also have to get rid of the index of the element we removed from the `HashMap`.
- `getRandom`: Sample a random element from the list.

**Implementation**
<iframe src="https://leetcode.com/playground/d5ryduyc/shared" frameBorder="0" width="100%" height="500" name="d5ryduyc"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N)$$, with $$N$$ being the number of operations. All of our operations are $$O(1)$$, giving $$N * O(1) = O(N)$$.

* Space complexity : $$O(N)$$, with $$N$$ being the number of operations. The worst case scenario is if we get $$N$$ `add` operations, in which case our `ArrayList` and our `HashMap` grow to size $$N$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ 128ms Solution, Real O(1) Solution
- Author: Kouei
- Creation Date: Tue Aug 16 2016 00:47:29 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 03 2020 15:22:31 GMT+0800 (Singapore Standard Time)

<p>
There are two data member in the solution
1. a vector **nums**
2. an unordered_map **m**

The **key** of **m** is the **val**, the **value** of **m** is a vector contains indices where the **val** appears in **nums**.
Each element of **nums** is a pair, the first element of the pair is **val** itself, the second element of the pair is an index into **m[val]**.
There is a relationship between **nums** and **m**:

**m[nums[i].first][nums[i].second] == i;**

```
class RandomizedCollection {
public:
    /** Initialize your data structure here. */
    RandomizedCollection() {
        
    }
    
    /** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
    bool insert(int val) {
        auto result = m.find(val) == m.end();
        
        m[val].push_back(nums.size());
        nums.push_back(pair<int, int>(val, m[val].size() - 1));
        
        return result;
    }
    
    /** Removes a value from the collection. Returns true if the collection contained the specified element. */
    bool remove(int val) {
        auto result = m.find(val) != m.end();
        if(result)
        {
            auto last = nums.back();
            m[last.first][last.second] = m[val].back();
            nums[m[val].back()] = last;
            m[val].pop_back();
            if(m[val].empty()) m.erase(val);
            nums.pop_back();
        }
        return result;
    }
    
    /** Get a random element from the collection. */
    int getRandom() {
        return nums[rand() % nums.size()].first;
    }
private:
    vector<pair<int, int>> nums;
    unordered_map<int, vector<int>> m;
};
```
</p>


### Java HaspMap, LinkedHashSet, ArrayList (155 ms)
- Author: yubad2000
- Creation Date: Tue Aug 09 2016 14:45:48 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 07:09:12 GMT+0800 (Singapore Standard Time)

<p>
See my previous post [here](https://discuss.leetcode.com/topic/53216/java-solution-using-a-hashmap-and-an-arraylist-along-with-a-follow-up-131-ms/4).
I modified the code by replacing HashSet with LinkedHashSet because the set.iterator() might be costly when a number has too many duplicates. Using LinkedHashSet can be considered as O(1) if we only get the first element to remove. 
```
public class RandomizedCollection {
    ArrayList<Integer> nums;
	HashMap<Integer, Set<Integer>> locs;
	java.util.Random rand = new java.util.Random();
    /** Initialize your data structure here. */
    public RandomizedCollection() {
        nums = new ArrayList<Integer>();
	    locs = new HashMap<Integer, Set<Integer>>();
    }
    
    /** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
    public boolean insert(int val) {
        boolean contain = locs.containsKey(val);
	    if ( ! contain ) locs.put( val, new LinkedHashSet<Integer>() ); 
	    locs.get(val).add(nums.size());        
	    nums.add(val);
	    return ! contain ;
    }
    
    /** Removes a value from the collection. Returns true if the collection contained the specified element. */
    public boolean remove(int val) {
        boolean contain = locs.containsKey(val);
	    if ( ! contain ) return false;
	    int loc = locs.get(val).iterator().next();
	    locs.get(val).remove(loc);
	    if (loc < nums.size() - 1 ) {
	       int lastone = nums.get( nums.size()-1 );
	       nums.set( loc , lastone );
	       locs.get(lastone).remove( nums.size()-1);
	       locs.get(lastone).add(loc);
	    }
	    nums.remove(nums.size() - 1);
	   
	    if (locs.get(val).isEmpty()) locs.remove(val);
	    return true;
    }
    
    /** Get a random element from the collection. */
    public int getRandom() {
        return nums.get( rand.nextInt(nums.size()) );
    }
}
```
</p>


### Frugal Python code
- Author: agave
- Creation Date: Thu Aug 11 2016 15:29:30 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 24 2018 23:19:28 GMT+0800 (Singapore Standard Time)

<p>
```
import random

class RandomizedCollection(object):

    def __init__(self):
        self.vals, self.idxs = [], collections.defaultdict(set)
        

    def insert(self, val):
        self.vals.append(val)
        self.idxs[val].add(len(self.vals) - 1)
        return len(self.idxs[val]) == 1
        

    def remove(self, val):
        if self.idxs[val]:
            out, ins = self.idxs[val].pop(), self.vals[-1]
            self.vals[out] = ins
            if self.idxs[ins]:
                self.idxs[ins].add(out)
                self.idxs[ins].discard(len(self.vals) - 1)
            self.vals.pop()
            return True
        return False 

    def getRandom(self):
        return random.choice(self.vals)
</p>


