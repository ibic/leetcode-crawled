---
title: "Loud and Rich"
weight: 795
#id: "loud-and-rich"
---
## Description
<div class="description">
<p>In a group of N people (labelled <code>0, 1, 2, ..., N-1</code>), each person has different amounts of money, and different levels of quietness.</p>

<p>For convenience, we&#39;ll call the person with label <code>x</code>, simply &quot;person <code>x</code>&quot;.</p>

<p>We&#39;ll say that <code>richer[i] = [x, y]</code> if person <code>x</code>&nbsp;definitely has more money than person&nbsp;<code>y</code>.&nbsp; Note that <code>richer</code>&nbsp;may only be a subset of valid observations.</p>

<p>Also, we&#39;ll say <code>quiet[x] = q</code> if person <font face="monospace">x</font>&nbsp;has quietness <code>q</code>.</p>

<p>Now, return <code>answer</code>, where <code>answer[x] = y</code> if <code>y</code> is the least quiet person (that is, the person <code>y</code> with the smallest value of <code>quiet[y]</code>), among all people&nbsp;who definitely have&nbsp;equal to or more money than person <code>x</code>.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>richer = <span id="example-input-1-1">[[1,0],[2,1],[3,1],[3,7],[4,3],[5,3],[6,3]]</span>, quiet = <span id="example-input-1-2">[3,2,5,4,6,1,7,0]</span>
<strong>Output: </strong><span id="example-output-1">[5,5,2,5,4,5,6,7]</span>
<strong>Explanation: </strong>
answer[0] = 5.
Person 5 has more money than 3, which has more money than 1, which has more money than 0.
The only person who is quieter (has lower quiet[x]) is person 7, but
it isn&#39;t clear if they have more money than person 0.

answer[7] = 7.
Among all people that definitely have equal to or more money than person 7
(which could be persons 3, 4, 5, 6, or 7), the person who is the quietest (has lower quiet[x])
is person 7.

The other answers can be filled out with similar reasoning.
</pre>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= quiet.length = N &lt;= 500</code></li>
	<li><code>0 &lt;= quiet[i] &lt; N</code>, all <code>quiet[i]</code> are different.</li>
	<li><code>0 &lt;= richer.length &lt;= N * (N-1) / 2</code></li>
	<li><code>0 &lt;= richer[i][j] &lt; N</code></li>
	<li><code>richer[i][0] != richer[i][1]</code></li>
	<li><code>richer[i]</code>&#39;s are all different.</li>
	<li>The&nbsp;observations in <code>richer</code> are all logically consistent.</li>
</ol>

</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Cached Depth-First Search [Accepted]

**Intuition**

Consider the directed graph with edge `x -> y` if `y` is richer than `x`.

For each person `x`, we want the quietest person in the subtree at `x`.

**Algorithm**

Construct the graph described above, and say `dfs(person)` is the quietest person in the subtree at `person`.   Notice because the statements are logically consistent, the graph must be a DAG - a directed graph with no cycles.

Now `dfs(person)` is either `person`, or `min(dfs(child) for child in person)`.  That is to say, the quietest person in the subtree is either the `person` itself, or the quietest person in some subtree of a child of `person`.

We can cache values of `dfs(person)` as `answer[person]`, when performing our *post-order traversal* of the graph.  That way, we don't repeat work.  This technique reduces a quadratic time algorithm down to linear time.

<iframe src="https://leetcode.com/playground/A96H6rD8/shared" frameBorder="0" width="100%" height="500" name="A96H6rD8"></iframe>

**Complexity Analysis**

* Time Complexity: $$\mathcal{O}(N^2)$$, where $$N$$ is the number of people.
We are iterating here over array `richer`. It could contain up to 
$$1 + ... + N - 1 = N(N - 1) / 2$$ elements, for example, in the situation 
when each new person is richer than the previous one.  

* Space Complexity: $$\mathcal{O}(N^2)$$, to keep the graph with $$N^2$$ edges.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Concise DFS 
- Author: lee215
- Creation Date: Sun Jun 10 2018 11:06:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 03:07:41 GMT+0800 (Singapore Standard Time)

<p>
**Explanation**:
The description is not easy to understand.
In fact it\'s a basic dfs traversal problem.
For every people, call a sub function `dfs` to compare the `quiet` with others, who is richer than him.
Also we will note this answer to avoid repeated calculation.


**Time Complexity**:
O(richer.length),
Sub function `dfs` traverse every people only once, and every `richer` is traversed only one once.

**C++:**
```
    unordered_map<int, vector<int>> richer2;
    vector<int> res;
    vector<int> loudAndRich(vector<vector<int>>& richer, vector<int>& quiet) {
        for (auto v : richer) richer2[v[1]].push_back(v[0]);
        res = vector<int> (quiet.size(), -1);
        for (int i = 0; i < quiet.size(); i++) dfs(i, quiet);
        return res;
    }

    int dfs(int i, vector<int>& quiet) {
        if (res[i] >= 0) return res[i];
        res[i] = i;
        for (int j : richer2[i]) if (quiet[res[i]] > quiet[dfs(j, quiet)]) res[i] = res[j];
        return res[i];
    }
```

**Java:**
```
    HashMap<Integer, List<Integer>> richer2 = new HashMap<>();
    int res[];
    public int[] loudAndRich(int[][] richer, int[] quiet) {
        int n = quiet.length;
        for (int i = 0; i < n; ++i) richer2.put(i, new ArrayList<Integer>());
        for (int[] v : richer) richer2.get(v[1]).add(v[0]);
        res = new int[n]; Arrays.fill(res, -1);
        for (int i = 0; i < n; i++) dfs(i, quiet);
        return res;
    }

    int dfs(int i, int[] quiet) {
        if (res[i] >= 0) return res[i];
        res[i] = i;
        for (int j : richer2.get(i)) if (quiet[res[i]] > quiet[dfs(j, quiet)]) res[i] = res[j];
        return res[i];
    }
```

**Python:**
```
class Solution(object):

    def loudAndRich(self, richer, quiet):
        m = collections.defaultdict(list)
        for i, j in richer: m[j].append(i)
        res = [-1] * len(quiet)

        def dfs(i):
            if res[i] >= 0: return res[i]
            res[i] = i
            for j in m[i]:
                if quiet[res[i]] > quiet[dfs(j)]: res[i] = res[j]
            return res[i]

        for i in range(len(quiet)): dfs(i)
        return res
```

</p>


### Example is confusing
- Author: vkumarra
- Creation Date: Sun Jun 10 2018 11:15:27 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 27 2018 11:06:30 GMT+0800 (Singapore Standard Time)

<p>
Can someone please explain how is this possible:

"answer[7] = 7.
There isn\'t anyone who definitely has more money than person 7, so the person who definitely has
equal to or more money than person 7 is just person 7."

Given [3, 7] entry in richer, we know for sure that atleaset 3 has more money that 7.

Can someone please explain it.
</p>


### C++ with topological sorting
- Author: codefever
- Creation Date: Sun Jun 10 2018 21:32:11 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 12:27:02 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    vector<int> loudAndRich(vector<vector<int>>& richer, vector<int>& quiet) {
        const int N = quiet.size();
        vector<vector<int>> graph(N);
        vector<int> indegrees(N, 0);
        for (auto& r : richer) {
            ++indegrees[r[1]];
            graph[r[0]].push_back(r[1]);
        }
        
        deque<int> q;
        for (int i = 0; i < indegrees.size(); ++i) {
            if (indegrees[i] == 0) {
                q.push_back(i);
            }
        }
        
        vector<int> answers(N);
        iota(answers.begin(), answers.end(), 0);
        while (!q.empty()) {
            auto idx = q.front();
            q.pop_front();
            for (auto next : graph[idx]) {
                if (quiet[answers[next]] > quiet[answers[idx]]) {
                    answers[next] = answers[idx];
                }
                if (--indegrees[next] == 0) {
                    q.push_back(next);
                }
            }
        }
        return answers;
    }
};
```
</p>


