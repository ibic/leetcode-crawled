---
title: "Reconstruct Original Digits from English"
weight: 406
#id: "reconstruct-original-digits-from-english"
---
## Description
<div class="description">
<p>Given a <b>non-empty</b> string containing an out-of-order English representation of digits <code>0-9</code>, output the digits in ascending order.</p>

<p><b>Note:</b><br />
<ol>
<li>Input contains only lowercase English letters.</li>
<li>Input is guaranteed to be valid and can be transformed to its original digits. That means invalid inputs such as "abc" or "zerone" are not permitted.</li>
<li>Input length is less than 50,000.</li>
</ol>
</p>

<p><b>Example 1:</b><br />
<pre>
Input: "owoztneoer"

Output: "012"
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
Input: "fviefuro"

Output: "45"
</pre>
</p>
</div>

## Tags
- Math (math)

## Companies


## Official Solution
[TOC]

## Solution

---

#### Approach 1: Hashmap

**Intuition**

The naive approach would be to construct as many `"zero"`s 
as it's possible from letters available in the input string, 
then as many `"one"`s as it's possible, etc. 
The problem is that letters `"o"`, `"n"`, `"e"` could be present
as well in the another numbers that means that 
the straightforward approach could be misleading.

![compute](../Figures/423/misleading.png)

Hence the idea is to look for something unique. One could notice
that all even numbers contain each a unique letter :

* Letter "z" is present only in "zero".
* Letter "w" is present only in "two".
* Letter "u" is present only in "four".
* Letter "x" is present only in "six".
* Letter "g" is present only in "eight".

> Hence there is a good way to count even numbers.

That is actually the key how to count `3`s, `5`s and `7`s since
some letters are present only in one odd and one even number
(and all even numbers has already been counted) :

* Letter "h" is present only in "three" and "eight".
* Letter "f" is present only in "five" and "four".
* Letter "s" is present only in "seven" and "six".

Now one needs to count `9`s and `1`s only, and the logic is
basically the same :

* Letter "i" is present in "nine", "five", "six", and "eight".
* Letter "n" is present in "one", "seven", and "nine".

**Implementation**

!?!../Documents/423_LIS.json:1000,626!?!

<iframe src="https://leetcode.com/playground/zuooUwrg/shared" frameBorder="0" width="100%" height="500" name="zuooUwrg"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ where `N`
is a number of characters in the input string. 
$$\mathcal{O}(N)$$ time is needed to compute hashmap `count`
"letter -> its frequency in the input string". Then we deal with 
a data structure `out` which contains `10` elements only and 
all operations are done in a constant time.
 
* Space complexity : $$\mathcal{O}(1)$$.
`count` contains constant number of elements since 
input string contains only lowercase English letters, and
`out` contains not more than `10` elements.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### one pass O(n) JAVA Solution, Simple and Clear
- Author: markieff
- Creation Date: Sun Oct 16 2016 12:43:03 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 00:24:08 GMT+0800 (Singapore Standard Time)

<p>
The idea is:

for zero, it's the only word has letter 'z',
for two, it's the only word has letter 'w',
......
so we only need to count the unique letter of each word, Coz the input is always valid.

Code:

    public String originalDigits(String s) {
        int[] count = new int[10];
        for (int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if (c == 'z') count[0]++;
            if (c == 'w') count[2]++;
            if (c == 'x') count[6]++;
            if (c == 's') count[7]++; //7-6
            if (c == 'g') count[8]++;
            if (c == 'u') count[4]++; 
            if (c == 'f') count[5]++; //5-4
            if (c == 'h') count[3]++; //3-8
            if (c == 'i') count[9]++; //9-8-5-6
            if (c == 'o') count[1]++; //1-0-2-4
        }
        count[7] -= count[6];
        count[5] -= count[4];
        count[3] -= count[8];
        count[9] = count[9] - count[8] - count[5] - count[6];
        count[1] = count[1] - count[0] - count[2] - count[4];
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i <= 9; i++){
            for (int j = 0; j < count[i]; j++){
                sb.append(i);
            }
        }
        return sb.toString();
    }
</p>


### Fun fact
- Author: StefanPochmann
- Creation Date: Sun Oct 16 2016 19:42:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 10:32:27 GMT+0800 (Singapore Standard Time)

<p>
The **even** digits all have a unique letter while the **odd** digits all don't:

`zero`: Only digit with `z`
`two`: Only digit with `w`
`four`: Only digit with `u`
`six`: Only digit with `x`
`eight`: Only digit with `g`

The odd ones for easy looking, each one's letters all also appear in other digit words:
`one`, `three`, `five`, `seven`, `nine`
</p>


### Share my simple and easy O(N) solution
- Author: YutingLiu
- Creation Date: Sun Oct 16 2016 12:16:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 16 2016 12:16:16 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public String originalDigits(String s) {
        if(s==null || s.length()==0) return "";
        int[] count = new int[128];
        for(int i=0;i<s.length();i++)  count[s.charAt(i)]++;
        int[] num = new int[10];
        num[0] = count['z'];
        num[2] = count['w'];
        num[4] = count['u'];
        num[6] = count['x'];
        num[8] = count['g'];
        num[1] = count['o']-count['z']-count['w']-count['u'];
        num[3] = count['h']-count['g'];
        num[5] = count['v']-count['s']+count['x'];
        num[7] = count['s']-count['x'];
        num[9] = count['i']-count['x']-count['g']-count['v']+count['s']-count['x'];
        String ret = new String();
        for(int i=0;i<10;i++)
            for(int j=num[i];j>0;j--) ret += String.valueOf(i);
        return ret;
    }
}
```
</p>


