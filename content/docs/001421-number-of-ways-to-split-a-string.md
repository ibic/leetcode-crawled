---
title: "Number of Ways to Split a String"
weight: 1421
#id: "number-of-ways-to-split-a-string"
---
## Description
<div class="description">
<p>Given a binary string <code>s</code> (a string consisting only of &#39;0&#39;s and &#39;1&#39;s),&nbsp;we can split <code>s</code>&nbsp;into 3 <strong>non-empty</strong> strings s1, s2, s3 (s1+ s2+ s3 = s).</p>

<p>Return the number of ways <code>s</code> can be split such that the number of&nbsp;characters &#39;1&#39; is the same in s1, s2, and s3.</p>

<p>Since the answer&nbsp;may be too large,&nbsp;return it modulo&nbsp;10^9 + 7.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;10101&quot;
<strong>Output:</strong> 4
<strong>Explanation:</strong> There are four ways to split s in 3 parts where each part contain the same number of letters &#39;1&#39;.
&quot;1|010|1&quot;
&quot;1|01|01&quot;
&quot;10|10|1&quot;
&quot;10|1|01&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1001&quot;
<strong>Output:</strong> 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;0000&quot;
<strong>Output:</strong> 3
<strong>Explanation:</strong> There are three ways to split s in 3 parts.
&quot;0|0|00&quot;
&quot;0|00|0&quot;
&quot;00|0|0&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;100100010100110&quot;
<strong>Output:</strong> 12
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>3 &lt;= s.length &lt;= 10^5</code></li>
	<li><code>s[i]</code> is <code>&#39;0&#39;</code>&nbsp;or&nbsp;<code>&#39;1&#39;</code>.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Multiplication of the ways of 1st and 2nd cuts w/ explanation and analysis.
- Author: rock
- Creation Date: Sun Sep 06 2020 00:04:33 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 10 2020 14:43:34 GMT+0800 (Singapore Standard Time)

<p>
Please refer to the outstanding elaboration from `@lionkingeatapple`, who deserves more upvotes than me:

----

We have three different scenarios.

`scenarios_1:` If the total number of ones in the input string is not the multiple of three, there is no way we can cut the string into three blocks with an equal number of ones. `=> Count the total number of the 1s, if not divisible by 3, then return 0;`

`scenarios_2:` If give input string are all zeros, it doesn\'t matter how you want to cut, and each block will have the same number of ones. In other words, the number of ones equals zero in each block. Therefore, the total number of ways to cut the input string is `(n - 2) + (n - 3) + ... + 2 + 1 = (n - 2) * (n - 1) / 2`. (Please reference the proof from @rock explanation).

`scenarios_3:` This is a more general situation. First, think about it, in order to cut the input string to three parts, we need only two cuts, so we can get three blocks. Let\'s say these three blocks are `b1, b2, b3`. Now, we want the number of ones to be equal for all three blocks and the number of ones we need for each block is `total number of ones / 3`. The questions now become where we need to do our cut, remember we need to only two cuts to make three blocks, so we need to decide where we need to do our first cut, and where we can do our second cut, with all of these we can know our final answer.

![image](https://assets.leetcode.com/users/images/09967820-7819-4666-a942-94e8a157cb58_1599570737.6272898.png)



Starts from begin of the input string, we can not cut until the number of ones in the substring is less than `total number of ones / 3`. If the number of ones in the substring is equal to `total number of ones / 3`, then that is our place to do the first cut. For example, we have an input string 100100010100110. The number of ones for each block will be `total number of ones / 3 = 6 / 3 = 2`. The first cut place can be either after index `3,4,5 and 6` because of those substrings `( substring(0, 4), substring(0, 5), substring(0, 6), substring(0, 7)` will have the same number of ones, which is `2` and the number of ways to cut at first place is `4` (see diagram below).
![image](https://assets.leetcode.com/users/images/771a758a-a710-4e78-b1c1-e8903d079f7f_1599570788.3038762.png)

**Our first block (b1) could be either one of them below:**
![image](https://assets.leetcode.com/users/images/d130e864-8c78-4df7-b4de-cdcc7e622e95_1599571027.4380488.png)



After, we know about our first places to cut the input string, we need to know the second place to cut. When it comes to the second block, the number of ones we want are `the number of ones in the first block + the number of ones in the second block = 1s in b1 + 1s in b2.` Based on the example above, `1s in b1 + 1s in b2 = 2 + 2 = 4`, since the ones between blocks are equal, we can also simply do `2 * (number of ones each block) = 2 * 2 = 4`. Therefore, we need to find the substring with the total number of ones is equal to `4`. Those substrings are `substring(0, 10), substring(0, 11), substring(0, 12)` and our second cut places can be either after index `9, 10, 11` and the number of ways to cut at second place is `3`. (see diagram below)

![image](https://assets.leetcode.com/users/images/fd879990-243a-4b1b-950c-1457fb50b4a7_1599571105.2501438.png)



Finally, after two cuts, we got our `3 blocks` and we have known all the places we can do these two cuts, so we can calculate the total ways of cutting are `number of ways to cut at first place x number of ways to cut at second place = 4 x 3 = 12`.

**Follow up question**
`Return number of ways split input string s into N non-empty strings, each string has an equal number of ones. (2 < N < length of input string)`

Thinking:
In order to have `N` substrings, we need to cut input string `N - 1` times, so we will have `N - 1` blocks. Then, we have `b1, b2, b3, b4, ... ...b(n-1)`. Each of the blocks needs to have `number of ones = total number of ones / N`. And let\'s assume the number of ways to split string for each cut is `Wi`, so the total number of ways to cut will be `W1 x W2 x W3 x ... ...x Wn-1`. I didn\'t code this yet. I think you guys are smart enough to get an answer.

----

1. Count the total number of the  `1`s, if not divisible by `3`, then return `0`;
2. If the count is `0`, then we can choose 1st `0` as our 1st cut, correspondingly, the 2nd cut between the other `2` non-empty subarrays will have `n - 2` options, where `n = s.length()`; Similarly, we can also choose  2nd `0` as 1st cut, then we have `n - 3` options for the 2nd cut, ..., etc, totally, the result is `(n - 2) + (n - 3) + ... + 2 + 1 = (n - 2) * (n - 1) / 2`;
3. Otherwise, traverse the input array: count the `1`s again, if the count reaches the `1 / 3` of the totoal number of `1`s, we begin to accumulate the number of the ways of the 1st cut, until the count is greater than `1 / 3 * total ones`; when the count reaches the `2 / 3` of the total number of the `1`s, start to accumulate the number of the ways of the 2nd cut, until the count is greater than `2 / 3 * total ones`;
4. Multification of the numbers of the ways of 1st and 2nd cuts is the result..

```java
    private static final int m = 1_000_000_007;
    public int numWays(String s) {
        int ones = 0, n = s.length();
        for (int i = 0; i < n; ++i) {
            ones += s.charAt(i) - \'0\';
        }
        if (ones == 0) {
            return (int)((n - 2L) * (n - 1L) / 2 % m);
        }
        if (ones % 3 != 0) {
            return 0;
        }
        int onesInEachSplitedBlock = ones / 3;
        long waysOfFirstCut = 0, waysOfSecondCut = 0;
        for (int i = 0, count = 0; i < n; ++i) {
            count += s.charAt(i) - \'0\';
            if (count == onesInEachSplitedBlock) {
                ++waysOfFirstCut;
            }else if (count == 2 * onesInEachSplitedBlock) {
                ++waysOfSecondCut;
            }
        }
        return (int)(waysOfFirstCut * waysOfSecondCut % m);        
```
```python
    def numWays(self, s: str) -> int:
        ones, n, m = s.count(\'1\'), len(s), 10 ** 9 + 7
        if ones == 0:
            return (n - 2) * (n - 1) // 2 % m
        if ones % 3 != 0:
            return 0
        ones_in_each_splited_block = ones // 3
        count = ways_of_first_cut = ways_of_second_cut = 0
        for char in s:
            if char == \'1\':
                count += 1
            if count == ones_in_each_splited_block:
                ways_of_first_cut += 1
            elif count == 2 * ones_in_each_splited_block:
                ways_of_second_cut += 1
        return ways_of_first_cut * ways_of_second_cut % m        
```
**Analysis:**

Two Pass.

Time: O(n), space: O(1), where n = s.length().
</p>


### Python | One-pass | Explained & Visualised
- Author: since2020
- Creation Date: Sun Sep 06 2020 00:22:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 00:42:25 GMT+0800 (Singapore Standard Time)

<p>
Scan ```s``` to record positions for ```\'1\'```. After checking the edge cases, the result is simply a combination of choices.
![image](https://assets.leetcode.com/users/images/18f66338-ce4e-4667-a750-549ff019d9a2_1599323828.9086614.png)

```Python
class Solution:
    def numWays(self, s: str) -> int:
        n,ones = len(s),[]
        for i,val in enumerate(s):
            if val == \'1\':
                ones.append(i)
        total = len(ones)
        if total == 0:
		    # ref: https://en.wikipedia.org/wiki/Combination
			# combination of selecting 2 places to split the string out of n-1 places
            return ((n-1)*(n-2)//2) % (10**9+7) 
        if total % 3 != 0:
            return 0
        target = total // 3
        return (ones[target]-ones[target-1])*(ones[target*2]-ones[target*2-1])%(10**9+7)
```
</p>


### [C++] Basic Mathematics and Arrangement Logic
- Author: rahulanandyadav2000
- Creation Date: Sun Sep 06 2020 00:32:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 13 2020 02:36:35 GMT+0800 (Singapore Standard Time)

<p>
Approach to the solution

Following three cases arise
1. If the number of 1\'s is not divisible by 3
2. If the number of 1\'s is zero
3. If the number of 1\'s is divisble by 3

Case 1:
	Since 1\'s cannot be dividede into three parts return 0;

Case 2: 
	Min length of array should be one, therfore number of places to cut the string is N-1, 
	where n is the length of the string.
	Now the problem is reduced to arrangements.
	Number of ways to keep two identical balls from N-1 different places= (N-1)*(N-2)/2;
	Choose for first from n-1 and then for second we have n-2 choices. Since objects are identical divide it by 2!. (2= number of identical objects).
	Also don\'t forget to mod at every step to keep it to int.
			
Case3: 
Let number of 1\'s be 6;
then we divide them into 3 parts of two 1\'s.
Example 1 1 0 0 1 0 1 0 0 0 1 1
first part can end (and second part start) from i=1 to i=3 (Why? think yourself).....section 1
similarly second part can end from (and third part start) from i=5 to i=8.....section 2
so in section 1 we have three places to partition and in second section we have four place to partition therefore the answer is 3*4;
			
			

```
class Solution {
public:
    int numWays(string s) {
        long long int i,n=s.size();
        int one=0;
        long long mod=1000000007;
        long long ans;
        for(i=0;i<n;i++)
            if(s[i]==\'1\')
                one++;
        if(one%3!=0)
            return 0;
        else if(one==0)
        {
           ans =((n-1)*(n-2)/2)%mod;
        }
        else
        {
            long long countOfone=one/3;
            long long index1=-1,index1end=-1;
            long long  index2=-1,index2end=-1;
            long long count=0;
            for(i=0;i<n;i++)
            {   
                if(s[i]==\'1\')
                    count++;
                if(count==countOfone&&index1==-1)
                    index1=i;
                if(count==countOfone+1&&index2==-1)
                    index2=i;
                if(count==countOfone*2&&index1end==-1)
                    index1end=i;
                if(count==countOfone*2+1&&index2end==-1)
                    index2end=i;
            
            }
            ans=((index2-index1)%mod)*((index2end-index1end)%mod)%mod;
        }
        return (int)ans;
    }
};

```
Feel free to raise doubts.
Time Complexity O(n)

PS: This is my first blog :)
</p>


