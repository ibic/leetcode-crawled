---
title: "Minimum Possible Integer After at Most K Adjacent Swaps On Digits"
weight: 1379
#id: "minimum-possible-integer-after-at-most-k-adjacent-swaps-on-digits"
---
## Description
<div class="description">
<p>Given a string <code>num</code> representing <strong>the digits</strong> of&nbsp;a very large integer and an integer <code>k</code>.</p>

<p>You are allowed to swap any two adjacent digits of the integer <strong>at most</strong> <code>k</code> times.</p>

<p>Return <em>the minimum integer</em> you can obtain also as a string.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/06/17/q4_1.jpg" style="width: 500px; height: 40px;" />
<pre>
<strong>Input:</strong> num = &quot;4321&quot;, k = 4
<strong>Output:</strong> &quot;1342&quot;
<strong>Explanation:</strong> The steps to obtain the minimum integer from 4321 with 4 adjacent swaps are shown.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> num = &quot;100&quot;, k = 1
<strong>Output:</strong> &quot;010&quot;
<strong>Explanation:</strong> It&#39;s ok for the output to have leading zeros, but the input is guaranteed not to have any leading zeros.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> num = &quot;36789&quot;, k = 1000
<strong>Output:</strong> &quot;36789&quot;
<strong>Explanation:</strong> We can keep the number without any swaps.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> num = &quot;22&quot;, k = 22
<strong>Output:</strong> &quot;22&quot;
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> num = &quot;9438957234785635408&quot;, k = 23
<strong>Output:</strong> &quot;0345989723478563548&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= num.length &lt;= 30000</code></li>
	<li><code>num</code> contains <strong>digits</strong> only and doesn&#39;t have <strong>leading zeros</strong>.</li>
	<li><code>1 &lt;= k &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### O(n logn) | Detailed Explanation
- Author: khaufnak
- Creation Date: Sun Jul 05 2020 15:21:49 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 19 2020 01:09:11 GMT+0800 (Singapore Standard Time)

<p>
A hard question = Lots of observation + data structures 

Let\'s figure out observations and then find out data structure to implement them.

>Observation 0: Well, we know that if we could get smallest digit to the left, then we will be able to make number smaller than we currently have. In that sense, a sorted number(Ascending) will already be smallest. 

So, let\'s take pen and paper and try to find smallest number we can form for this:

```"4321", k = 4```

So, let\'s try to move 1 to left most postion. From now on, I\'ll call the current digit we are moving to left as ```d```:
```"4321", k = 4```
```"4312", k = 3```
```"4132", k = 2```
```"1432", k = 1```
Hmm, we can clearly observe: 

>Observation 1: when we move a digit to left, other digit are shifted to right. i.e 432 got shifted to right by 1.

But, wait.  What if there was another digit to right of ```d```?
```"43219", k = 4```
```"43129", k = 3```
```"41329", k = 2```
```"14329", k = 1```

Well, location of 9 didn\'t change. Therefore, we can make some correction to our above observation. 

>Corrected observation 1: Only digits to the left of ```d``` get their position shifted.

Alright, what\'s next?

But what if the ```k``` was really small and we couldn\'t move 1 to left most?
```"43219", k = 2```
```"43129", k = 1```
```"41329", k = 0```

Hmm, something is fishy, we clearly didn\'t reach smallest number here. Smallest for ```k=2``` would be ```24319```.

We can observe here, that we should choose smallest ```d``` that is in the reach of ```k```. 

>Observation 2: Choose first smallest ```d``` that is in reach of ```k```. 

If we combine all the observation, we can see that we will iterate from left to right and try to place digits 0 through 9.
Let\'s work through a bigger example:

```"9438957234785635408", k = 23```

We will start from left. Let\'s try to place 0 here. ```0``` is within reach of ```k```,  ```0``` is 17 shifts away to right. So, we will get:
```"0943895723478563548", k = 6```

Observe that all the number got shifted to right except the once to the right of ```d``` (8, here).  

Now, let\'s move to next position:
Let\'s try to place 0 here, again. But wait, we don\'t 0 left, so try 1, which is not there also. So let\'s try 2, 2 is 8 distance away from this position. ```8 > k```, so we cannot choose 2. Let\'s try 3, 3 is 2 distance away. 2 < k, therefore let\'s choose 3.

```"0394895723478563548", k = 4```

and we can continue like this.

>For observation 1, to calculate the correct number of shifts, we will need to also store how many elements before ```d``` already shifted. We will use segment tree for this.
>For observation 2, We will use queue to choose latest occurence of each digit.


```
class Solution {
    public String minInteger(String num, int k) {
        //pqs stores the location of each digit.
        List<Queue<Integer>> pqs = new ArrayList<>();
        for (int i = 0; i <= 9; ++i) {
            pqs.add(new LinkedList<>());
        }

        for (int i = 0; i < num.length(); ++i) {
            pqs.get(num.charAt(i) - \'0\').add(i);
        }
        String ans = "";
        SegmentTree seg = new SegmentTree(num.length());

        for (int i = 0; i < num.length(); ++i) {
            // At each location, try to place 0....9
            for (int digit = 0; digit <= 9; ++digit) {
                // is there any occurrence of digit left?
                if (pqs.get(digit).size() != 0) {
                    // yes, there is a occurrence of digit at pos
                    Integer pos = pqs.get(digit).peek();
					// Since few numbers already shifted to left, this `pos` might be outdated.
                    // we try to find how many number already got shifted that were to the left of pos.
                    int shift = seg.getCountLessThan(pos);
                    // (pos - shift) is number of steps to make digit move from pos to i.
                    if (pos - shift <= k) {
                        k -= pos - shift;
                        seg.add(pos); // Add pos to our segment tree.
                        pqs.get(digit).remove();
                        ans += digit;
                        break;
                    }
                }
            }
        }
        return ans;
    }

    class SegmentTree {
        int[] nodes;
        int n;

        public SegmentTree(int max) {
            nodes = new int[4 * (max)];
            n = max;
        }

        public void add(int num) {
            addUtil(num, 0, n, 0);
        }

        private void addUtil(int num, int l, int r, int node) {
            if (num < l || num > r) {
                return;
            }
            if (l == r) {
                nodes[node]++;
                return;
            }
            int mid = (l + r) / 2;
            addUtil(num, l, mid, 2 * node + 1);
            addUtil(num, mid + 1, r, 2 * node + 2);
            nodes[node] = nodes[2 * node + 1] + nodes[2 * node + 2];
        }

        // Essentialy it tells count of numbers < num.
        public int getCountLessThan(int num) {
            return getUtil(0, num, 0, n, 0);
        }

        private int getUtil(int ql, int qr, int l, int r, int node) {
            if (qr < l || ql > r) return 0;
            if (ql <= l && qr >= r) {
                return nodes[node];
            }

            int mid = (l + r) / 2;
            return getUtil(ql, qr, l, mid, 2 * node + 1) + getUtil(ql, qr, mid + 1, r, 2 * node + 2);
        }
    }

}
```

If you don\'t know about segment tree. This is simplest segment tree and you can look up ```sum of ranges using segment tree``` on Google and you will find million articles. You can also use Balanced BST, BIT (Thanks @giftwei for suggestion) to get same complexity.


</p>


### The constraint was not very helpful... [C++/Python] Clean 56ms O(n2) solution
- Author: lichuan199010
- Creation Date: Sun Jul 05 2020 12:15:42 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 08 2020 06:24:42 GMT+0800 (Singapore Standard Time)

<p>
1 <= num.length <= 30000
1 <= k <= 10^9

Given these two constraints, how could leetcode expect someone to know that O(n2) and O(n2logn) would pass the OJ?
It was not hard to figure out the bubble sort as a naive solution (pick the smallest possible number and move to the front) and think about fast return with sorted digits if k >= n*(n-1)//2 (when all numbers can be rearranged freely), but I spent the last hour trying to implement a clever solution, but failed...
I think BIT or segment tree will do, but didn\'t manage to get them right.

I would suggest either make the constraint less daunting, or reinforce the constraint as claimed...
Well, I guess maybe sometimes we just need to be bold enough and jump off the cliff to realize it was just a pit...

Usually, I would think the total number of computation acceptable would be below 10^8. So, if n > 10000, I would hesitate to try O(n2).

--------------------
Wow, so many people voted up, so I attached a solution below, beat 100% in Python (56ms in total).
This is an O(n2) solution just for fun.

	class Solution:
		def minInteger(self, num: str, k: int) -> str:
		    # base case
		    if k <= 0: return num
		    # the total number of swaps if you need to reverse the whole string is n*(n-1)//2.
			# therefore, if k is >= this number, any order is achievable.
			n = len(num)
			if k >= n*(n-1)//2: 
				return "".join(sorted(list(num)))
            
		    # starting from the smallest number
			for i in range(10):
			    # find the smallest index
				ind = num.find(str(i))
				# if this index is valid
				if 0 <= ind <= k:
				    # move the digit to the front and deal with the rest of the string recursively.
					return str(num[ind]) + self.minInteger(num[0:ind] + num[ind+1:], k-ind)

To copy and paste:

	class Solution:
		def minInteger(self, num: str, k: int) -> str:
			if k <= 0: return num
			n = len(num)
			if k >= n*(n-1)//2: 
				return "".join(sorted(list(num)))

			# for each number, find the first index
			for i in range(10):
				ind = num.find(str(i))
				if 0 <= ind <= k:
					return str(num[ind]) + self.minInteger(num[0:ind] + num[ind+1:], k-ind)
					
C++ version by @wanli2019. Thank you.

	class Solution {
	public:
		string minInteger(string num, int k) {
			if(k <= 0) 
				return num;
			int n = num.size();
			if(k>=n*(n+1)/2){ 
				sort(num.begin(), num.end());
				return num;
			}
			for(int i=0; i<10; i++){
				int idx = num.find(to_string(i));
				if(idx>=0 && idx<=k)
					return num[idx]+minInteger(num.substr(0,idx)+num.substr(idx+1), k-idx);
			}
			return num;
		}
	};
</p>


### [Python] 17 lines O(nlogn) solution
- Author: qqwqert007
- Creation Date: Sun Jul 05 2020 16:13:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 05 2020 16:15:34 GMT+0800 (Singapore Standard Time)

<p>
The idea is quite straightforward: 
`In each round, pick the smallest number within k distance and move it to the front.`
For each number, we save its indexes in a deque.
In each round, we check from 0 to 9 to see if the nearest index is within k distance.
But here comes the tricky part:
`The index of a number may change due to the swaps we made in the previous rounds.`
For example, if 3 numbers after `num[i]` are moved to the front,
then the index of `num[i]` becomes `i + 3` in the new array.
So we need a data structure to store the indexes of picked numbers,
to support fast calculation of the new index of each remaining number.
BIT, Segment Tree and Balanced Binary Search Tree can do this.
Here I use `sortedcontainers`, which is an implementation of Balanced Binary Search Tree.

```
from collections import defaultdict, deque
from sortedcontainers import SortedList
from string import digits
class Solution:
    def minInteger(self, num: str, k: int) -> str:
        d = defaultdict(deque)
        for i, a in enumerate(num):
            d[a].append(i)
        ans, seen = \'\', SortedList()
        for _ in range(len(num)):
            for a in digits:
                if d[a]:
                    i = d[a][0]
                    ni = i + (len(seen) - seen.bisect(i))
                    dis = ni - len(seen)
                    if dis <= k:
                        k -= dis
                        d[a].popleft()
                        ans += a
                        seen.add(i)
                        break
        return ans
```
</p>


