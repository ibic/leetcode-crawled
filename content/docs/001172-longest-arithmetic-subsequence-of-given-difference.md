---
title: "Longest Arithmetic Subsequence of Given Difference"
weight: 1172
#id: "longest-arithmetic-subsequence-of-given-difference"
---
## Description
<div class="description">
<p>Given an integer array <code>arr</code>&nbsp;and an integer <code><font face="monospace">difference</font></code>, return the length of the longest subsequence in <font face="monospace"><code>arr</code>&nbsp;</font>which is an arithmetic sequence such that the difference between adjacent elements in the subsequence equals&nbsp;<code>difference</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,3,4], difference = 1
<strong>Output:</strong> 4
<strong>Explanation: </strong>The longest arithmetic subsequence is [1,2,3,4].</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,3,5,7], difference = 1
<strong>Output:</strong> 1
<strong>Explanation: </strong>The longest arithmetic subsequence is any single element.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,5,7,8,5,3,4,2,1], difference = -2
<strong>Output:</strong> 4
<strong>Explanation: </strong>The longest arithmetic subsequence is [7,5,3,1].
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 10^5</code></li>
	<li><code>-10^4 &lt;= arr[i], difference &lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] O(n) DP using Hashmap
- Author: PhoenixDD
- Creation Date: Sun Oct 06 2019 12:02:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jan 04 2020 13:48:08 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
At each integer `i` in the array we can consider it as the end of AP(Arithmetic Progression/Sequence) and check the length of that AP which will be the (length of AP that ends with `i-difference`) `+ 1`. We can thus use a hashmap to store this while traversing the array.

**Solution**
```c++
class Solution {
public:
    int longestSubsequence(vector<int>& arr, int difference) 
    {
        unordered_map<int,int> lengths;
        int result=1;
        for(int &i:arr)
            result=max(result,lengths[i]=1+lengths[i-difference]); //Length of AP ending with \'i\' with difference of \'difference\' will be 1 + length of AP ending with \'i-difference\'. result stores Max at each end
        return result;
    }
};
```
**Complexity**
Space: `O(n).`
Time: `O(n).`
</p>


### 5 lines. OMG can't believe!!!
- Author: evil_zone321
- Creation Date: Sun Oct 06 2019 12:01:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 13:39:07 GMT+0800 (Singapore Standard Time)

<p>
	class Solution {
		public int longestSubsequence(int[] arr, int difference) {
			HashMap<Integer, Integer> dp = new HashMap<>();
			int longest = 0;
			for(int i=0; i<arr.length; i++) {
				dp.put(arr[i], dp.getOrDefault(arr[i] - difference, 0) + 1);
				longest = Math.max(longest, dp.get(arr[i]));
			}
			return longest;
		}
	}
</p>


### Python 4 lines
- Author: li-_-il
- Creation Date: Sun Oct 06 2019 12:03:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 12:03:04 GMT+0800 (Singapore Standard Time)

<p>
```
def longestSubsequence(self, arr: List[int], diff: int) -> int:
        res = {}
        for num in arr:
            res[num] = res[num - diff] + 1 if (num - diff) in res else 1
        return max(res.values())
```
</p>


