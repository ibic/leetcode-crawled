---
title: "Department Top Three Salaries"
weight: 1479
#id: "department-top-three-salaries"
---
## Description
<div class="description">
<p>The <code>Employee</code> table holds all employees. Every employee has an Id, and there is also a column for the department Id.</p>

<pre>
+----+-------+--------+--------------+
| Id | Name  | Salary | DepartmentId |
+----+-------+--------+--------------+
| 1  | Joe   | 85000  | 1            |
| 2  | Henry | 80000  | 2            |
| 3  | Sam   | 60000  | 2            |
| 4  | Max   | 90000  | 1            |
| 5  | Janet | 69000  | 1            |
| 6  | Randy | 85000  | 1            |
| 7  | Will  | 70000  | 1            |
+----+-------+--------+--------------+
</pre>

<p>The <code>Department</code> table holds all departments of the company.</p>

<pre>
+----+----------+
| Id | Name     |
+----+----------+
| 1  | IT       |
| 2  | Sales    |
+----+----------+
</pre>

<p>Write a SQL query to find employees who earn the top three salaries in each of the department. For the above tables, your SQL query should return the following rows (order of rows does not matter).</p>

<pre>
+------------+----------+--------+
| Department | Employee | Salary |
+------------+----------+--------+
| IT         | Max      | 90000  |
| IT         | Randy    | 85000  |
| IT         | Joe      | 85000  |
| IT         | Will     | 70000  |
| Sales      | Henry    | 80000  |
| Sales      | Sam      | 60000  |
+------------+----------+--------+
</pre>

<p><strong>Explanation:</strong></p>

<p>In IT department, Max earns the highest salary, both Randy and Joe earn the second highest salary, and Will earns the third highest salary. There are only two employees in the Sales department, Henry earns the highest salary while Sam earns the second highest salary.</p>

</div>

## Tags


## Companies
- Amazon - 12 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Netflix - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `JOIN` and sub-query [Accepted]

**Algorithm**

A top 3 salary in this company means there is no more than 3 salary bigger than itself in the company.

```sql
select e1.Name as 'Employee', e1.Salary
from Employee e1
where 3 >
(
    select count(distinct e2.Salary)
    from Employee e2
    where e2.Salary > e1.Salary
)
;
```

In this code, we count the salary number of which is bigger than e1.Salary. So the output is as below for the sample data.
```
| Employee | Salary |
|----------|--------|
| Henry    | 80000  |
| Max      | 90000  |
| Randy    | 85000  |
```

Then, we need to join the **Employee** table with **Department** in order to retrieve the department information.

**MySQL**

```sql
SELECT
    d.Name AS 'Department', e1.Name AS 'Employee', e1.Salary
FROM
    Employee e1
        JOIN
    Department d ON e1.DepartmentId = d.Id
WHERE
    3 > (SELECT
            COUNT(DISTINCT e2.Salary)
        FROM
            Employee e2
        WHERE
            e2.Salary > e1.Salary
                AND e1.DepartmentId = e2.DepartmentId
        )
;
```

```
| Department | Employee | Salary |
|------------|----------|--------|
| IT         | Joe      | 70000  |
| Sales      | Henry    | 80000  |
| Sales      | Sam      | 60000  |
| IT         | Max      | 90000  |
| IT         | Randy    | 85000  |
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted solution without group by or order by
- Author: baiji
- Creation Date: Sun Feb 08 2015 04:14:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 02:18:33 GMT+0800 (Singapore Standard Time)

<p>
    select d.Name Department, e1.Name Employee, e1.Salary
    from Employee e1 
    join Department d
    on e1.DepartmentId = d.Id
    where 3 > (select count(distinct(e2.Salary)) 
                      from Employee e2 
                      where e2.Salary > e1.Salary 
                      and e1.DepartmentId = e2.DepartmentId
                      );
</p>


### Simple solution, easy to understand
- Author: lemonxixi
- Creation Date: Fri Aug 28 2015 01:32:43 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 28 2018 23:10:19 GMT+0800 (Singapore Standard Time)

<p>
    Select dep.Name as Department, emp.Name as Employee, emp.Salary from Department dep, 
    Employee emp where emp.DepartmentId=dep.Id and 
    (Select count(distinct Salary) From Employee where DepartmentId=dep.Id and Salary>emp.Salary)<3
</p>


### My tidy solution
- Author: Swordmctster
- Creation Date: Sat Jan 24 2015 14:35:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jan 24 2015 14:35:58 GMT+0800 (Singapore Standard Time)

<p>
    select D.Name as Department, E.Name as Employee, E.Salary as Salary 
      from Employee E, Department D
       where (select count(distinct(Salary)) from Employee 
               where DepartmentId = E.DepartmentId and Salary > E.Salary) in (0, 1, 2)
             and 
               E.DepartmentId = D.Id 
             order by E.DepartmentId, E.Salary DESC;
</p>


