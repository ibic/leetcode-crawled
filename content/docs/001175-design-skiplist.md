---
title: "Design Skiplist"
weight: 1175
#id: "design-skiplist"
---
## Description
<div class="description">
<p>Design a Skiplist without using any built-in libraries.</p>

<p><em>A Skiplist is a data structure that takes&nbsp;O(log(n)) time&nbsp;to <code>add</code>, <code>erase</code> and <code>search</code>. Comparing with treap and red-black tree which has the same function and performance, the code length of Skiplist can be&nbsp;comparatively short and the idea behind Skiplists are just simple linked lists.</em></p>

<p><em>For example:&nbsp;we have a Skiplist containing <code>[30,40,50,60,70,90]</code> and we want to add <code>80</code> and <code>45</code> into it. The&nbsp;Skiplist works this way:</em></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/09/27/1506_skiplist.gif" style="width: 960px; height: 332px;" /><br />
<small>Artyom Kalinin [CC BY-SA 3.0], via <a href="https://commons.wikimedia.org/wiki/File:Skip_list_add_element-en.gif" target="_blank" title="Artyom Kalinin [CC BY-SA 3.0 (https://creativecommons.org/licenses/by-sa/3.0)], via Wikimedia Commons">Wikimedia Commons</a></small></p>

<p><em>You can see there are many layers in the Skiplist. Each layer is a sorted linked list. With the help of the top layers, <code>add</code>&nbsp;,&nbsp;<code>erase</code>&nbsp;and <code>search&nbsp;</code>can be faster than O(n).&nbsp;It can be proven&nbsp;that the average time complexity for each operation is O(log(n)) and space complexity is O(n).</em></p>

<p>To be specific, your design should include these functions:</p>

<ul>
	<li><code>bool search(int target)</code> : Return whether&nbsp;the <code>target</code> exists in the Skiplist&nbsp;or not.</li>
	<li><code>void add(int num)</code>:&nbsp;Insert a value into the SkipList.&nbsp;</li>
	<li><code>bool erase(int num)</code>: Remove a value in&nbsp;the Skiplist.&nbsp;If <code>num</code>&nbsp;does not exist in the Skiplist, do nothing and return false. If there exists multiple <code>num</code> values, removing&nbsp;any one of them is fine.</li>
</ul>

<p>See more about Skiplist :&nbsp;<a href="https://en.wikipedia.org/wiki/Skip_list" target="_blank">https://en.wikipedia.org/wiki/Skip_list</a></p>

<p>Note that duplicates may exist in the Skiplist, your code needs to handle this situation.</p>

<p>&nbsp;</p>

<p><b>Example:</b></p>

<pre>
Skiplist skiplist = new Skiplist();

skiplist.add(1);
skiplist.add(2);
skiplist.add(3);
skiplist.search(0);   // return false.
skiplist.add(4);
skiplist.search(1);   // return true.
skiplist.erase(0);    // return false, 0 is not in skiplist.
skiplist.erase(1);    // return true.
skiplist.search(1);   // return false, 1 has already been erased.</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= num, target&nbsp;&lt;= 20000</code></li>
	<li>At most <code>50000</code>&nbsp;calls will be made to <code>search</code>, <code>add</code>, and <code>erase</code>.</li>
</ul>
</div>

## Tags
- Design (design)

## Companies
- Microsoft - 2 (taggedByAdmin: true)
- Google - 0 (taggedByAdmin: true)
- Twitter - 0 (taggedByAdmin: true)
- Pure Storage - 0 (taggedByAdmin: true)
- eBay - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (python3)
```python3
import math
import random

class Node:
    def __init__(self, val: int = 0, next = None, down = None):
        self.val = val
        self.next = next
        self.down = down

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '{}->{}'.format(
            self.val,
            self.next.val if self.next else 'N')

class Skiplist:
    random.seed(0)
    # random.seed()

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        li = []
        node = self.head
        while node:
            lily = []
            no = node
            while no:
                lily.append('{}{}'.format(no.val, '' if no.down else '.'))
                no = no.next
            li.append(lily)
            node = node.down
        return '\n'.join(map(lambda l: '->'.join(l), li))

    def __init__(self):
        self.head = None
        self.tail = None
        self._addOneLevel()

    def _addOneLevel(self):
        self.tail = Node(math.inf, None, self.tail)
        self.head = Node(-math.inf, self.tail, self.head)


    def _growUp(self):
        return random.getrandbits(1) % 2 == 0

    def _search(self, target: int) -> bool:
        linodes = []
        node = self.head
        while node:
            if node.next.val >= target:
                linodes.append(node)
                node = node.down
            else:
                node = node.next
        return linodes

    def levels(self):
        r = 0
        node = self.head
        while node:
            r += 1
            node = node.down
        return r

    def search(self, target: int) -> bool:
        return self._search(target)[-1].next.val == target

    def add(self, num: int) -> None:
        linodes = self._search(num)
        tip = linodes[-1]
        below = Node(num, tip.next)
        tip.next = below
        totalLevel = len(linodes)
        level = 1
        while self._growUp() and level <= totalLevel:
            if level == totalLevel: # maximally grows one level a time
                self._addOneLevel()
                prev = self.head
            else:
                prev = linodes[totalLevel - 1 - level]
            below = Node(num, prev.next, below)
            prev.next = below
            level += 1

    def erase(self, num: int) -> bool:
        linodes = self._search(num)
        tip = linodes[-1]
        if tip.next.val != num:
            return False
        totalLevel = len(linodes)
        level = totalLevel - 1
        while level >= 0 and linodes[level].next.val == num:
            linodes[level].next = linodes[level].next.next
            level -= 1
        return True


# Your Skiplist object will be instantiated and called as such:
# obj = Skiplist()
# param_1 = obj.search(target)
# obj.add(num)
# param_3 = obj.erase(num)
```

## Top Discussions
### Python, 1 node per value and 100%
- Author: awice
- Creation Date: Mon Sep 30 2019 08:14:19 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 30 2019 08:14:19 GMT+0800 (Singapore Standard Time)

<p>
Each node has a list `node.levels` of forward pointers.  `Skiplist._iter(num)` iterates over the node prior to `num` for each level.  After this setup, everything is easy.

```python
class Node:
    __slots__ = \'val\', \'levels\'
    def __init__(self, val, levels):
        self.val = val
        self.levels = [None] * levels

class Skiplist(object):
    def __init__(self):
        self.head = Node(-1, 16) 
    
    def _iter(self, num):
        cur = self.head
        for level in range(15, -1, -1):
            while True:
                future = cur.levels[level]
                if future and future.val < num:
                    cur = future
                else:
                    break
            yield cur, level

    def search(self, target):
        for prev, level in self._iter(target):
            pass
        cur = prev.levels[0]
        return cur and cur.val == target

    def add(self, num):
        nodelvls = min(16, 1 + int(math.log2(1.0 / random.random())))
        node = Node(num, nodelvls)
        
        for cur, level in self._iter(num):
            if level < nodelvls:
                future = cur.levels[level]
                cur.levels[level] = node
                node.levels[level] = future

    def erase(self, num):
        ans = False
        for cur, level in self._iter(num):
            future = cur.levels[level]
            if future and future.val == num:
                ans = True
                cur.levels[level] = future.levels[level]
        return ans
```
</p>


### Java Solution beats 100%
- Author: LifeExtender
- Creation Date: Mon Sep 30 2019 02:46:06 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 30 2019 02:46:06 GMT+0800 (Singapore Standard Time)

<p>
# 1206. Design Skiplist

<!-- TOC -->

- [1206. Design Skiplist](#1206-design-skiplist)
    - [Description](#description)
    - [Intuition](#intuition)
    - [Solution](#solution)
    - [Reference](#reference)

<!-- /TOC -->

## Description

[Description](https://leetcode.com/problems/design-skiplist/)

## Intuition

I heard this data structure back in 2015/2016 when I was taking 15213, but I never implemented one due to my misunderstanding of the course policy.

I implemented this solution based on the first 15 minutes of [YouTube Video](https://www.youtube.com/watch?v=7pWkspmYUVo&t=1617s), which means I cannot guarantee the `search`, `add` and `remove` is the exact same with this video.

## Solution

```java
  // Sentinel
  //          \
  // level 3: -Inf ----------------------------> 4
  // level 2: -Inf ------------> 2 ------------> 4
  // level 1: -Inf ----> 1 ----> 2 ------------> 4
  // level 0: -Inf ----> 1 ----> 2 ----> 3 ----> 4 : this level is the most concrete level
  private static final double DEFAULT_PROB = 0.5;
  private final Random rand = new Random();
  private final List<Node> sentinels = new ArrayList<>();

  {
    sentinels.add(new Node(Integer.MIN_VALUE));
  }

  public boolean search(int target) {
    Node smallerOrEquals = getSmallerOrEquals(target);
    return smallerOrEquals.val == target;
  }

  public void add(int num) {
    Node cur = getSmallerOrEquals(num);
    // cur \u6700\u4E0B\u5C42\uFF0C\u6BD4\u4ED6\u5C0F\u6216\u8005\u7B49\u4E8E num
    final Node toInsert = new Node(num);
    append(cur, toInsert);
    // populate the level
    populateLevelUp(toInsert);
  }

  private void populateLevelUp(final Node toInsert) {
    Node curPrev = toInsert.left, cur = toInsert;

    while (flipCoin()) {
      while (curPrev.left != null && curPrev.up == null) {
        curPrev = curPrev.left;
      }
      if (curPrev.up == null) {
        final Node newSentinel = new Node(Integer.MIN_VALUE);
        curPrev.up = newSentinel;
        newSentinel.down = curPrev;
        sentinels.add(curPrev.up);
      }
      curPrev = curPrev.up;
      final Node newNode = new Node(toInsert.val);
      cur.up = newNode;
      newNode.down = cur;
      cur = cur.up;
      curPrev.right = cur;
      cur.left = curPrev;
    }
  }

  private void append(Node prev, Node cur) {
    final Node next = prev.right;
    prev.right = cur;
    cur.left = prev;
    if (next != null) {
      next.left = cur;
      cur.right = next;
    }
  }

  public boolean erase(int num) {
    final Node toRemove = getSmallerOrEquals(num);
    if (toRemove.val != num) {
      return false;
    }
    Node cur = toRemove;
    while (cur != null) {
      final Node prev = cur.left, next = cur.right;
      prev.right = next;
      if (next != null) {
        next.left = prev;
      }
      cur = cur.up;
    }
    return true;
  }

  private Node getSmallerOrEquals(final int target) {
    Node cur = getSentinel();
    while (cur != null) {
      if (cur.right == null || cur.right.val > target) {
        if (cur.down == null) break;
        cur = cur.down;
      } else {
        cur = cur.right;
      }
    }
    return cur;
  }

  private boolean flipCoin() {
    return rand.nextDouble() < DEFAULT_PROB;
  }

  private Node getSentinel() {
    return sentinels.get(sentinels.size() - 1);
  }

  public String toString() {
    Node node = sentinels.get(0);
    final StringBuilder sb = new StringBuilder();
    while (node != null) {
      Node itr = node;
      while (itr != null) {
        sb.append(itr.val).append(",");
        itr = itr.up;
      }
      sb.append("\
");
      node = node.right;
    }
    return sb.toString();
  }

  private static final class Node {
    private final int val;
    private Node left, right, up, down;

    private Node(int val) {
      this.val = val;
    }
  }
```

## Reference

- [YouTube Video](https://www.youtube.com/watch?v=7pWkspmYUVo&t=1617s)

</p>


### Short Java solution (average > 90% in time and 100% in memory)
- Author: oldsui
- Creation Date: Sun Dec 01 2019 09:26:20 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 13 2020 16:36:13 GMT+0800 (Singapore Standard Time)

<p>
The purpose of coin toss is to ensure that each node at current level will be duplicated to its upper level with a probability of 0.5, so the number of nodes at the upper level is roughly half of the current level.

So in the extreme case, SkipList is equivalent to a balanced binary search tree.

The solution below is modified based on the video from https://leetcode.com/problems/design-skiplist/discuss/393499/Java-Solution-beats-100 and [opendatastructures](https://opendatastructures.org/ods-java/4_2_SkiplistSSet_Efficient_.html) ([pdf]( https://opendatastructures.org/versions/edition-0.1d/ods-java.pdf))

The example illustrations below are from opendatastructures.org and meant for SkiplistSSet, which does not contain duplicates.

![image](https://assets.leetcode.com/users/oldsui/image_1575162896.png)
**Initial skiplist**

![image](https://assets.leetcode.com/users/oldsui/image_1575163040.png)
**Search for 4: return its predecessor (similar to prev of linked list problems)**
```
cur = sentinel.top
while (cur != null) 
    if (cur.next.val >= target)   #overshoot
	    cur goes down
	else 
	    cur goes right
```
![image](https://assets.leetcode.com/users/oldsui/image_1575163112.png)
**Add a number 3.5**
Grow node for 3.5 using coin toss process.

![image](https://assets.leetcode.com/users/oldsui/image_1575163191.png)
**Remove a number 3**


**Some key ideas**
* Assume max height is 33
* We use # trailing 0s of a random Integer to simulate the coin toss process to determine the height for a newly inserted node



```
class Skiplist {

    static class Node {
        int val;
        int count; // to handle duplicates
        int h; // highest level: [0,32]
        Node[] next;
        public Node(int a, int b) {
            val = a; h = b; count = 1;
            next = new Node[33];
        }
    }

    Node sentinel = new Node(Integer.MIN_VALUE, 32);
    int topLevel = 0;
    Node[] stack = new Node[33];  // to keep track of search path
    Random rand = new Random();

    public Skiplist() {
    }

    // find the predecessor
    private Node findPred(int num) {
        Node cur = sentinel;
        for (int r = topLevel; r >= 0; r--) {
            while (cur.next[r] != null && cur.next[r].val < num)  cur = cur.next[r];
            stack[r] = cur;
        }
        return cur;
    }

    public boolean search(int target) {
        Node pred = findPred(target);
        return pred.next[0] != null && pred.next[0].val == target;
    }

    public void add(int num) {
        Node pred = findPred(num);
        if (pred.next[0] != null && pred.next[0].val == num) {
            pred.next[0].count++;
            return;
        }
        Node w = new Node(num, pickHeight());
        while (topLevel < w.h) stack[++topLevel] = sentinel;
        for (int i = 0; i <= w.h; i++) {
            w.next[i] = stack[i].next[i];
            stack[i].next[i] = w;
        }
    }

    public boolean erase(int num) {
        Node pred = findPred(num);
        if (pred.next[0] == null || pred.next[0].val != num) return false;
        boolean noNeedToRemove = --pred.next[0].count != 0;
        if (noNeedToRemove) return true;
        for (int r = topLevel; r >= 0; r--) {
            Node cur = stack[r];
            if (cur.next[r] != null && cur.next[r].val == num) cur.next[r] = cur.next[r].next[r];
            if (cur == sentinel && cur.next[r] == null) topLevel--;
        }
        return true;
    }

    // number of trailing 0s of a random Integer: [0, 32]
    private int pickHeight() {
        return Integer.numberOfTrailingZeros(rand.nextInt());
    }
}
```


**Skiplist for Sorted Set** (for reference):

```
class Skiplist {

    static class Node {
        int val, top;
        Node[] next;
        public Node(int a, int b) {
            val = a; top = b; next = new Node[33];
        }
    }
    static Random rand = new Random();

    Node sentinel = new Node(Integer.MIN_VALUE, 32);
    int topLevel = 0;
    Node[] stack = new Node[33];

    private Node findPred(int num) {
        Node cur = sentinel;
        for (int r = topLevel; r >= 0; r--) {
            while (cur.next[r] != null && cur.next[r].val < num) cur = cur.next[r];
            stack[r] = cur;
        }
        return cur;
    }

    public boolean search(int target) {
        Node pred = findPred(target);
        return pred.next[0] != null && pred.next[0].val == target;
    }

    private int pickHeight() {
        return Integer.numberOfTrailingZeros(rand.nextInt());
    }

    public void add(int num) {
        if (search(num)) return;
        Node w = new Node(num, pickHeight());
        while (topLevel < w.top) stack[++topLevel] = sentinel;
        for (int i = 0; i <= w.top; i++) {
            w.next[i] = stack[i].next[i];
            stack[i].next[i] = w;
        }
    }

    public boolean erase(int num) {
        if (!search(num)) return false;
        for (int r = topLevel; r >= 0; r--) {
            if (stack[r].next[r] != null && stack[r].next[r].val == num) 
                stack[r].next[r] = stack[r].next[r].next[r];
            if (stack[r] == sentinel && stack[r].next[r] == null) 
                topLevel--;
        }
        return true;
    }
}
```
</p>


