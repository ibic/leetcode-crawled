---
title: "Countries You Can Safely Invest In"
weight: 1586
#id: "countries-you-can-safely-invest-in"
---
## Description
<div class="description">
<p>Table <code>Person</code>:</p>

<pre>
+----------------+---------+
| Column Name    | Type    |
+----------------+---------+
| id             | int     |
| name           | varchar |
| phone_number   | varchar |
+----------------+---------+
id is the primary key for this table.
Each row of this table contains the name of a person and their phone number.
Phone number will be in the form &#39;xxx-yyyyyyy&#39; where xxx is the country code (3 characters) and yyyyyyy is the phone number (7 characters) where x and y are digits. Both can contain leading zeros.
</pre>

<p>Table <code>Country</code>:</p>

<pre>
+----------------+---------+
| Column Name    | Type    |
+----------------+---------+
| name           | varchar |
| country_code   | varchar |
+----------------+---------+
country_code is the primary key for this table.
Each row of this table contains the country name and its code. country_code will be in the form &#39;xxx&#39; where x is digits.
</pre>

<p>&nbsp;</p>

<p>Table <code>Calls</code>:</p>

<pre>
+-------------+------+
| Column Name | Type |
+-------------+------+
| caller_id   | int  |
| callee_id   | int  |
| duration    | int  |
+-------------+------+
There is no primary key for this table, it may contain duplicates.
Each row of this table contains the caller id, callee id and the duration of the call in minutes. caller_id != callee_id
</pre>

<p>A telecommunications company wants to invest in new countries. The company intends to invest in the countries where the average call duration of the calls in this country is strictly greater than the global average call duration.</p>

<p>Write an SQL query to find the countries where this company can invest.</p>

<p>Return the result table in any order.</p>

<p>The query result format is in the following example.</p>

<pre>
<code>Person</code> table:
+----+----------+--------------+
| id | name     | phone_number |
+----+----------+--------------+
| 3  | Jonathan | 051-1234567  |
| 12 | Elvis    | 051-7654321  |
| 1  | Moncef   | 212-1234567  |
| 2  | Maroua   | 212-6523651  |
| 7  | Meir     | 972-1234567  |
| 9  | Rachel   | 972-0011100  |
+----+----------+--------------+

<code>Country</code> table:
+----------+--------------+
| name     | country_code |
+----------+--------------+
| Peru     | 051          |
| Israel   | 972          |
| Morocco  | 212          |
| Germany  | 049          |
| Ethiopia | 251          |
+----------+--------------+

Calls table:
+-----------+-----------+----------+
| caller_id | callee_id | duration |
+-----------+-----------+----------+
| 1         | 9         | 33       |
| 2         | 9         | 4        |
| 1         | 2         | 59       |
| 3         | 12        | 102      |
| 3         | 12        | 330      |
| 12        | 3         | 5        |
| 7         | 9         | 13       |
| 7         | 1         | 3        |
| 9         | 7         | 1        |
| 1         | 7         | 7        |
+-----------+-----------+----------+

Result table:
+----------+
| country  |
+----------+
| Peru     |
+----------+
The average call duration for Peru is (102 + 102 + 330 + 330 + 5 + 5) / 6 = 145.666667
The average call duration for Israel is (33 + 4 + 13 + 13 + 3 + 1 + 1 + 7) / 8 = 9.37500
The average call duration for Morocco is (33 + 4 + 59 + 59 + 3 + 7) / 6 = 27.5000 
Global call duration average = (2 * (33 + 3 + 59 + 102 + 330 + 5 + 13 + 3 + 1 + 7)) / 20 = 55.70000
Since Peru is the only country where average call duration is greater than the global average, it&#39;s the only recommended country.
</pre>

</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### sexy af MySQL solution
- Author: fmxy
- Creation Date: Sat Jul 18 2020 16:12:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 18 2020 16:12:57 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT Country.name AS country
FROM Person JOIN Calls ON Calls.caller_id = Person.id OR Calls.callee_id = Person.id
JOIN Country ON Country.country_code = LEFT(Person.phone_number, 3)
GROUP BY Country.name
HAVING AVG(duration) > (SELECT AVG(duration) FROM Calls)
```
</p>


### Straightforward GROUP BY/HAVING : faster than 100.00% online submissions
- Author: playgames3
- Creation Date: Fri Jul 03 2020 02:51:14 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 06 2020 03:02:26 GMT+0800 (Singapore Standard Time)

<p>
Following solution works in MySQL, MS SQL Server. For Oracle, replace SUBSTRING with SUBSTR
```
SELECT
 co.name AS country
FROM
 person p
 JOIN
     country co
     ON SUBSTRING(phone_number,1,3) = country_code
 JOIN
     calls c
     ON p.id IN (c.caller_id, c.callee_id)
GROUP BY
 co.name
HAVING
 AVG(duration) > (SELECT AVG(duration) FROM calls)
```
</p>


### Easy MSSQL CTEs and Union all
- Author: Pythagoras_the_3rd
- Creation Date: Fri Sep 18 2020 00:23:50 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 18 2020 00:23:50 GMT+0800 (Singapore Standard Time)

<p>
```
with ps as (
    select p.*, c.name as country from person p
    left join country c on c.country_code = substring(p.phone_number, 1, 3)
),
call as(
    select caller_id, duration from calls
    union all
    select callee_id, duration from calls
),
cntry as(
    select ps.country, avg(c.duration) as avgd from call c
    left join ps on ps.id = c.caller_id
    group by ps.country
)
select country from cntry
where avgd > (select avg(duration) from calls)
```
</p>


