---
title: "Minimum Falling Path Sum II"
weight: 1103
#id: "minimum-falling-path-sum-ii"
---
## Description
<div class="description">
<p>Given a square grid&nbsp;of integers&nbsp;<code>arr</code>, a <em>falling path with non-zero shifts</em>&nbsp;is a choice of&nbsp;exactly one element from each row of <code>arr</code>, such that no two elements chosen in adjacent rows are in&nbsp;the same column.</p>

<p>Return the&nbsp;minimum&nbsp;sum of a falling path with non-zero shifts.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [[1,2,3],[4,5,6],[7,8,9]]
<strong>Output:</strong> 13
<strong>Explanation: </strong>
The possible falling paths are:
[1,5,9], [1,5,7], [1,6,7], [1,6,8],
[2,4,8], [2,4,9], [2,6,7], [2,6,8],
[3,4,8], [3,4,9], [3,5,7], [3,5,9]
The falling path with the smallest sum is&nbsp;[1,5,7], so the answer is&nbsp;13.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length == arr[i].length &lt;= 200</code></li>
	<li><code>-99 &lt;= arr[i][j] &lt;= 99</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Roblox - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] DP, O(MN)
- Author: lee215
- Creation Date: Sun Dec 15 2019 00:02:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 15 2019 00:02:52 GMT+0800 (Singapore Standard Time)

<p>

Find the 2 minimum value in the previous row.

**Python:**
```python
    def minFallingPathSum(self, A):
        for i in xrange(1, len(A)):
            r = heapq.nsmallest(2, A[i - 1])
            for j in xrange(len(A[0])):
                A[i][j] += r[1] if A[i - 1][j] == r[0] else r[0]
        return min(A[-1])
```
</p>


### C++ O(nm)  | O(1)
- Author: votrubac
- Creation Date: Sun Dec 15 2019 18:00:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 15 2019 18:06:06 GMT+0800 (Singapore Standard Time)

<p>
Think about the most straightforward solution: find the minimum element in the first row, add it to the minimum element in the second row, and so on.

That should work, unless two minimum elements are in the same column. To account for that, let\'s remember the position `pos` of the minimum element `fm`. Also, we need to remember the second minimum element `sm`.

For the next row, we will use `fm` for columns different than `pos`, and `sm` otherwise.
```
int minFallingPathSum(vector<vector<int>>& arr) {
    int fm = 0, sm = 0, pos = -1;
    for (auto i = 0; i < arr.size(); ++i) {
        auto fm2 = INT_MAX, sm2 = INT_MAX, pos2 = -1;
        for (auto j = 0; j < arr[i].size(); ++j) {
            auto mn = j != pos ? fm : sm;
            if (arr[i][j] + mn < fm2) {
                sm2 = fm2;
                fm2 = arr[i][j] + mn;
                pos2 = j;
            } else sm2 = min(sm2, arr[i][j] + mn);
        }
        fm = fm2, sm = sm2, pos = pos2;
    }
    return fm;
}
```
</p>


### Simple DP(Java) [Same problem as Paint House 2] (265)
- Author: Poorvank
- Creation Date: Sun Dec 15 2019 00:01:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 15 2019 21:27:49 GMT+0800 (Singapore Standard Time)

<p>
For every element in every column starting from second last row,
count minimum on the left & minimum on the right.

```
public int minFallingPathSum(int[][] arr) {
        int n = arr.length;
        for (int row=n-2;row>=0;row--) {
            for (int col=0;col<n;col++) {
                int min = Integer.MAX_VALUE;
                // Values to the left.
                for(int k=0;k<col;k++) {
                    min = Math.min(arr[row+1][k],min);
                }
                // Values to the right.
                for (int k=col+1;k<n;k++) {
                    min = Math.min(arr[row+1][k],min);
                }
                arr[row][col]+= min;
            }
        }
        int result = Integer.MAX_VALUE;
        for (int i = 0; i < n; ++i) {
            result = Math.min(result, arr[0][i]);
        }
        return result;
    }
```

As mentioned by @wushangzhen, This problem is very similar to problem no 265 [Paint House 2](https://leetcode.com/problems/paint-house-ii/).(PREMIUM)
</p>


