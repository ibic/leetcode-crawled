---
title: "Customers Who Bought Products A and B but Not C"
weight: 1573
#id: "customers-who-bought-products-a-and-b-but-not-c"
---
## Description
<div class="description">
<p>Table: <code>Customers</code></p>

<pre>
+---------------------+---------+
| Column Name         | Type    |
+---------------------+---------+
| customer_id         | int     |
| customer_name       | varchar |
+---------------------+---------+
customer_id is the primary key for this table.
customer_name is the name of the customer.</pre>

<p>&nbsp;</p>

<p>Table: <code>Orders</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| order_id      | int     |
| customer_id   | int     |
| product_name  | varchar |
+---------------+---------+
order_id is the primary key for this table.
customer_id is the id of the customer who bought the product &quot;product_name&quot;.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to&nbsp;report the&nbsp;customer_id and customer_name of customers who bought products &quot;A&quot;, &quot;B&quot; but did not buy the product &quot;C&quot; since we want to recommend them buy this product.</p>

<p>Return the result table <strong>ordered</strong> by customer_id.</p>

<p>The query result format is in the following example.</p>

<p>&nbsp;</p>

<pre>
Customers table:
+-------------+---------------+
| customer_id | customer_name |
+-------------+---------------+
| 1           | Daniel        |
| 2           | Diana         |
| 3           | Elizabeth     |
| 4           | Jhon          |
+-------------+---------------+

Orders table:
+------------+--------------+---------------+
| order_id   | customer_id  | product_name  |
+------------+--------------+---------------+
| 10         |     1        |     A         |
| 20         |     1        |     B         |
| 30         |     1        |     D         |
| 40         |     1        |     C         |
| 50         |     2        |     A         |
| 60         |     3        |     A         |
| 70         |     3        |     B         |
| 80         |     3        |     D         |
| 90         |     4        |     C         |
+------------+--------------+---------------+

Result table:
+-------------+---------------+
| customer_id | customer_name |
+-------------+---------------+
| 3           | Elizabeth     |
+-------------+---------------+
Only the customer_id with id 3 bought the product A and B but not the product C.</pre>

</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Straightforward approach using "where in" both 100%
- Author: gdkou90
- Creation Date: Fri Apr 03 2020 22:44:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Apr 03 2020 22:44:22 GMT+0800 (Singapore Standard Time)

<p>
```
select distinct customer_id, customer_name
from Customers
where customer_id in
(
    select customer_id
    from Orders
    where product_name=\'A\'
) and customer_id in
(
    select customer_id
    from Orders
    where product_name=\'B\'
) and customer_id not in
(
    select customer_id
    from Orders
    where product_name=\'C\'
) 
```
</p>


### Simple Solution MySQL - One join
- Author: saranpadmakar
- Creation Date: Tue Apr 21 2020 03:47:52 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 21 2020 05:33:04 GMT+0800 (Singapore Standard Time)

<p>
```
select a.customer_id, a.customer_name
from customers a , orders b
where a.customer_id  = b.customer_id
group by a.customer_id
having sum(b.product_name="A") >0 and sum(b.product_name="B") > 0 and sum(b.product_name="C")=0
```
</p>


### MySQL (CASE Solution, only 1 Join)
- Author: c0ding
- Creation Date: Fri Apr 03 2020 19:59:03 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Apr 03 2020 19:59:03 GMT+0800 (Singapore Standard Time)

<p>
Having fun with using CASE WHEN statements. Minimizes number of joins required.

```
select c.customer_id, c.customer_name 
from Customers as c
    inner join
    (select customer_id, 
        sum(CASE
        WHEN product_name = \'A\' THEN 1
        WHEN product_name = \'B\' THEN 1
        WHEN product_name = \'C\' THEN -1
        ELSE 0 END) as tot   
    from Orders
    group by customer_id
    having tot > 1) as o
where c.customer_id = o.customer_id
```
</p>


