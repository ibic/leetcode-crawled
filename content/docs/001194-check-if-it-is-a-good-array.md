---
title: "Check If It Is a Good Array"
weight: 1194
#id: "check-if-it-is-a-good-array"
---
## Description
<div class="description">
<p>Given an array <code>nums</code> of&nbsp;positive integers. Your task is to select some subset of <code>nums</code>, multiply each element by an integer and add all these numbers.&nbsp;The array is said to be&nbsp;<strong>good&nbsp;</strong>if you can obtain a sum of&nbsp;<code>1</code>&nbsp;from the array by any possible subset and multiplicand.</p>

<p>Return&nbsp;<code>True</code>&nbsp;if the array is <strong>good&nbsp;</strong>otherwise&nbsp;return&nbsp;<code>False</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [12,5,7,23]
<strong>Output:</strong> true
<strong>Explanation:</strong> Pick numbers 5 and 7.
5*3 + 7*(-2) = 1
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [29,6,10]
<strong>Output:</strong> true
<strong>Explanation:</strong> Pick numbers 29, 6 and 10.
29*1 + 6*(-3) + 10*(-1) = 1
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,6]
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Dropbox - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Chinese Remainder Theorem
- Author: lee215
- Creation Date: Sun Nov 03 2019 12:03:56 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 17 2020 10:10:05 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
This problem is realated a Chinese theorem:
Chinese remainder theorem, created in 5th centry.
Also well known as Hanson Counting.
<br>

## **Explanation**
If `a % x = 0` and `b % x = 0`,
appareantly we have `(pa + qb) % x == 0`
If `x > 1`, making it impossible `pa + qb = 1`.

Well, I never heard of Bezout\'s identity.
Even though the intuition only proves the necessary condition,
it\'s totally enough.

The process of gcd,
is exactly the process to get the factor.
The problem just doesn\'t ask this part.
<br>

## **Complexity**
Of course you can return true as soon as `gcd = 1`
I take gcd calculate as `O(1)`.

Time `O(N)`\uFF0C
Space `O(N)`

<br>

**Java:**
```java
    public boolean isGoodArray(int[] A) {
        int x = A[0], y;
        for (int a: A) {
            while (a > 0) {
                y = x % a;
                x = a;
                a = y;
            }
        }
        return x == 1;
    }
```

**C++:**
```cpp
    bool isGoodArray(vector<int>& A) {
        int res = A[0];
        for (int a: A)
            res = gcd(res, a);
        return res == 1;
    }
```

**Python:**
```python
    def isGoodArray(self, A):
        gcd = A[0]
        for a in A:
            while a:
                gcd, a = a, gcd % a
        return gcd == 1
```

**Python, 1-line using fractions**
```python
import fractions
class Solution(object):
    def isGoodArray(self, A):
        return reduce(fractions.gcd, A) < 2
```
</p>


### Bezout's Identity
- Author: songsari
- Creation Date: Sun Nov 03 2019 12:01:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 03 2019 12:08:56 GMT+0800 (Singapore Standard Time)

<p>
Read (https://brilliant.org/wiki/bezouts-identity/, https://en.wikipedia.org/wiki/B\xE9zout%27s_identity)

The basic idea is that for integers a and b, if gcd(a,b) = d, then there exist integers x and y, s.t  a * x + b * y = d;

This can be generalized for (n >= 2) .  e.g.  if gcd(a,b,c) = d, then there exist integers x, y, and z, s.t, a* x + b*y + c * z = d.

Now this problem is just asking if gcd(x1, ......, xn) = 1

```
class Solution {

public:
    bool isGoodArray(vector<int>& nums) {
        if (nums.size() == 1) return nums[0] == 1;
        
        int a = nums[0];
        
        for (int i = 1; i < nums.size(); i++) {
            if (__gcd(a, nums[i]) == 1) return true;
            a = __gcd(a, nums[i]);
        }
        
        return false;
    }
};
```
</p>


### C++ 3 lines
- Author: votrubac
- Creation Date: Sun Nov 03 2019 15:13:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 03 2019 15:13:15 GMT+0800 (Singapore Standard Time)

<p>
```
bool isGoodArray(vector<int>& nums) {
    auto gcd = nums[0];
    for (auto n : nums) gcd = __gcd(gcd, n);
    return gcd == 1;
}
```
</p>


