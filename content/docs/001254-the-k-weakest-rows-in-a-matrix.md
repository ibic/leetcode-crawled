---
title: "The K Weakest Rows in a Matrix"
weight: 1254
#id: "the-k-weakest-rows-in-a-matrix"
---
## Description
<div class="description">
<p>Given a <code>m&nbsp;* n</code>&nbsp;matrix <code>mat</code> of <em>ones</em>&nbsp;(representing soldiers) and <em>zeros</em>&nbsp;(representing civilians), return the indexes of the <code>k</code> weakest rows in the matrix ordered from the weakest to the strongest.</p>

<p>A row <em><strong>i</strong></em> is weaker than row <em><strong>j</strong></em>, if the number of soldiers in row <em><strong>i</strong></em> is less than the number of soldiers in row <em><strong>j</strong></em>, or they have the same number of soldiers but <em><strong>i</strong></em> is less than <em><strong>j</strong></em>. Soldiers are <strong>always</strong> stand in the frontier of a row, that is, always <em>ones</em>&nbsp;may appear first and then <em>zeros</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> mat = 
[[1,1,0,0,0],
 [1,1,1,1,0],
 [1,0,0,0,0],
 [1,1,0,0,0],
 [1,1,1,1,1]], 
k = 3
<strong>Output:</strong> [2,0,3]
<strong>Explanation:</strong> 
The number of soldiers for each row is: 
row 0 -&gt; 2 
row 1 -&gt; 4 
row 2 -&gt; 1 
row 3 -&gt; 2 
row 4 -&gt; 5 
Rows ordered from the weakest to the strongest are [2,0,3,1,4]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> mat = 
[[1,0,0,0],
&nbsp;[1,1,1,1],
&nbsp;[1,0,0,0],
&nbsp;[1,0,0,0]], 
k = 2
<strong>Output:</strong> [0,2]
<strong>Explanation:</strong> 
The number of soldiers for each row is: 
row 0 -&gt; 1 
row 1 -&gt; 4 
row 2 -&gt; 1 
row 3 -&gt; 1 
Rows ordered from the weakest to the strongest are [0,2,3,1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m == mat.length</code></li>
	<li><code>n == mat[i].length</code></li>
	<li><code><font face="monospace">2 &lt;= n, m &lt;= 100</font></code></li>
	<li><code>1 &lt;= k &lt;= m</code></li>
	<li><code>matrix[i][j]</code> is either 0 <strong>or</strong> 1.</li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

#### Overview

We'll use the following grid as our example, with `k = 5`.

![The input grid.](../Figures/1337/input_grid.png)

Let's say the "strength" of a row is the number of `1`'s (soldiers) in it. Because we need to compare rows based on their "strength", let's start by calculating the "strength" of each row. The simplest way of doing this is to loop over each row, and count how many `1`'s there are. We'll put these counts into a new array of length `m` (remember, `m` is the number of rows).

![Calculating the strengths.](../Figures/1337/row_counts.png)

We need to return the `k` rows with the lowest "strength". So, perhaps we should sort the "strengths" we've found and take the first `k` from the sorted list? Doing the sort will give us the following array. The first `k` "strengths" are highlighted.

![Sorting the strengths.](../Figures/1337/sorted_row_counts.png)

Hold on a minute though, the question requires us to return the *indexes* of the lowest "strengths"! Because of the sorting, we no longer know which "strength" was originally at what index. We'll need to keep track of the indexes that went with the "strengths".

We'll go through several different approaches for solving this problem. Some can be more easily implemented using certain programming languages.

<br />

---

#### Approach 1: Linear Search and Sorting

**Intuition**

The simplest approach to this problem depends on which programming language you're using. This first approach is recommended for *Python*. It is doable in Java and C++, but requires implementing a [Comparator](https://docs.oracle.com/javase/8/docs/api/java/util/Comparator.html). This is perhaps too much work, as better options exist for those programming languages.

The first approach we'll look at is multi-tier sorting. Instead of only inserting "strengths" into the list, we'll also insert indexes. We can represent each "strength" and index pair as a `tuple`. We should put the "strength" *first* in each tuple because we'll be sorting based on "strength".

For now, we'll calculate the "strength" of each row using the linear search approach described above (we'll optimize it in a later approach).

Here is the list of tuples you'll get for the above example.

![The generated list of tuples.](../Figures/1337/tuples.png)

Now we can sort the list using Python's built-in sort.

![The sorted list of tuples.](../Figures/1337/sorted_tuples.png)

When told to sort tuples, Python firstly sorts on the first element of the tuple, and then breaks any ties by sorting on the second element. Quite conveniently, this is exactly what we wanted here! Where there is a tie, the lower indexes are first. The only thing left to do is pull the indexes out of the first `k` tuples.

**Algorithm**

<iframe src="https://leetcode.com/playground/WSRFQauq/shared" frameBorder="0" width="100%" height="500" name="WSRFQauq"></iframe>

Here is a more Pythonic version of the code, using list comprehensions.

<iframe src="https://leetcode.com/playground/zzgiegiH/shared" frameBorder="0" width="100%" height="242" name="zzgiegiH"></iframe>

**Complexity Analysis**

- Time Complexity : $$O(m \cdot (n + \log \,m))$$.

    For the first phase, we're calculating the "strength" of each row. Calculating the "strength" of a row (with this algorithm) and putting it into the list is $$O(n)$$ in the worst case, and there are $$m$$ rows. This gives us $$O(n \cdot m)$$.

    For the second phase, we are sorting the list (which is of length $$m$$). Sorting a list using the built-in sort is $$O(m \, \log \, m)$$.

    To get our final time, we'll add the 2 complexities together. Whether $$n \cdot m$$ or $$m \, \log \,m$$ is bigger depends on the relative sizes of $$m$$ and $$n$$. This means that we have to add them, giving  $$m \cdot n + m \, \log\,m = m \cdot (n + \log \, m))$$.

    If $$k$$ was really small, an optimization would be to use selection sort instead of the built in sort to avoid needing to sort the entire list.

- Space Complexity : $$O(m)$$.

    Constructing the list requires $$O(m)$$ space.

<br />

---

#### Approach 2: Linear Search and Map

**Intuition**

This approach is recommended for Java and C++. In Python, it offers no advantage over Approach #1.

Another way we can keep track of the indexes is to put them into a `Map`. We'll go with a `HashMap` because more people are familiar with it, but a `TreeMap` could also be used (and has some nice advantages, but doesn't change the overall time complexity). The code in the next section shows both.

Each time we calculate the "strength" of a row, we should insert the index into the `Map` under its "strength". Because multiple indexes could have the same "strengths", the values of the `Map` should be *lists* of indexes, not single values. If we do this to the entire example from above, we get the following:

!?!../Documents/1337_hash_map.json:960,540!?!

Next, we'll need to sort the keys and iterate over them, pulling indexes out until we have `k` of them. Remember that because of the way we generated the `Map`, the indexes within a list are already sorted. The indexes that you'll need to pull out for the above example (`k = 5`) are highlighted.

![The k items that are returned.](../Figures/1337/pulling_out_k.png)

**Algorithm**

Firstly, here is the solution using a `HashMap`.

<iframe src="https://leetcode.com/playground/6E55HG5v/shared" frameBorder="0" width="100%" height="500" name="6E55HG5v"></iframe>

Secondly, here is the solution using a `TreeMap`. The difference between a `HashMap` and a `TreeMap` is that the `TreeMap` maintains the keys in sorted order. Note that this doesn't change the overall time complexity of the algorithm though, because insertion into a `TreeMap` is more expensive (because it's having to do more work to maintain that sorted order). In terms of good coding practice, a `TreeMap` is definitely better here. I haven't provided Python code for this solution, because Python doesn't have a built in `TreeMap`.

<iframe src="https://leetcode.com/playground/bLRyLhSp/shared" frameBorder="0" width="100%" height="500" name="bLRyLhSp"></iframe>

**Complexity Analysis**

- Time Complexity : $$O(m \cdot (n + \log \,m))$$.


    For each of the $$m$$ rows, we're calculating the "strength", which costs $$O(n)$$, and then we're inserting it into a `HashMap`, which costs $$O(1)$$. This part gives us a total of $$O(m \cdot n)$$.

    Next, we're sorting the $$m$$ keys, which costs $$O(m \, \log \, m)$$. We're then pulling the values out of the `Map`, which costs $$O(k)$$. Overall, this costs $$O(m \cdot m)$$ because $$k$$ is always less than $$m$$.

    To get our final time, we'll add the 2 complexities together. Whether $$n \cdot m$$ or $$m \, \log \, m$$ is bigger depends on the relative sizes of $$m$$ and $$n$$. This means that we have to add them, giving $$m \cdot n + m \, \log \,m  = m \cdot (n + \log\,m)$$.

    Using a `TreeMap` would have had the same time complexity because inserting $$m$$ values into a tree map costs $$O(m \, \log \, m)$$, and doesn't require the explicit sorting because values in a TreeMap are already sorted.

- Space Complexity : $$O(m)$$.

    Constructing the `Map` requires $$O(m)$$ space, regardless of whether we use a `TreeMap` or `HashMap`.

<br>

---

#### Approach 3: Binary Search and Sorting/ Map

**Intuition**

This approach uses **Binary Search**. If you're not familiar with this algorithm, have a look at the [Explore Module on Binary Search](https://leetcode.com/explore/learn/card/binary-search/) and do the first couple of problems.

The way that we calculated the "strength" of each row wasn't very efficient. What we used above was *linear search*, because it scanned through the row until it encountered a 0 (and if the row had been all 1's, it would have had to check the entire row!). Instead, we could find the index of the *first* `0` in each row, and use this to calculate the "strength".

![Link between index of first civilian and strength of row.](../Figures/1337/index_civillian_link.png)

So, let's think through how we could implement a *binary search* to find the first `0` in a given row.

Recall that *binary search* starts by looking at the middle element. It then decides which half of the array the "target" element (in this case the first 0 in the row) must be in and repeats the same process on that half until there's only one element left in the search space.

For example, here is the middle of a really long row. Which half of the array is the "target" in?

![](../Figures/1337/binary_search_example_1.png)

What about this one? You can't actually see the target element, but it's possible to know which half it's in.

![](../Figures/1337/binary_search_example_2.png)

And what about this one?

![](../Figures/1337/binary_search_example_3.png)

If the current "middle" element is a `0`, we know we've gone too far and the solution must be to the left. And if the current "middle" element is a `1`, then we know we haven't gone far enough, and the solution is to the right.

Here's the pseudocode for the binary search algorithm.

```python
low = 0
high = n
while low < high:
    mid = low + (high - low) // 2
    if row[mid] == 1:
        low = mid + 1
    else:
        high = mid
return low
```

And here's an animation showing the algorithm in action.

!?!../Documents/1337_binary_search.json:960,540!?!

**Algorithm**

<iframe src="https://leetcode.com/playground/VSmLvHSU/shared" frameBorder="0" width="100%" height="500" name="VSmLvHSU"></iframe>

**Complexity Analysis**

- Time Complexity : $$O(m \, \log m  n)$$.

    We determined above that Approach #1 and Approach #2 both have the same time complexity. This was $$O(m \cdot n)$$ to calculate the "strengths", and $$O(m \, \log \, m)$$ to get them into sorted order. For this approach though, we calculated the "strengths" using binary search instead of linear search. Calculating each row "strength" cost $$O(\log \,n)$$, and there were $$m$$ rows to calculate. This is, therefore, $$O(m \, \log \, n)$$. The second part will still be $$O(m \, \log \, m)$$.

    Like before, we don't know whether $$m$$ or $$n$$ is bigger. Therefore, we have to add the time complexities, which gives $$O(m \, \log \,n + m \, \log \, m) = O(m \cdot (\log \, n + \log \, m)) = O(m \, \log \, m n)$$.

- Space Complexity : $$O(m)$$.

    Same as above, as we're still relying on the same data structures.

<br />

---

#### Approach 4: Binary Search and Priority Queue

**Intuition**

Note: This approach is easier to code in Python than in Java/ C++, because it requires the implementation of a [Comparator](https://docs.oracle.com/javase/8/docs/api/java/util/Comparator.html).

The previous approaches use $$O(n)$$ space for gathering up row "strength" data. We then throw away $$n - k$$ of these, returning $$k$$ of them. Is there a way we can reduce this space usage to $$O(k)$$, by only keeping the smallest $$k$$ we've seen so far?

Problems like this can often be solved using a **Priority Queue**. Recall that a Priority Queue is a data structure that allows us to insert items, and to efficiently remove the *largest* item in the case of a **Max-Priority Queue**, or the *smallest* in the case of a **Min-Priority Queue**.

For this problem, we could start by inserting `k` "strengths" (along with their indexes) into the Priority Queue. After that, we'd only want to insert a "strength"/index pair if it was one of the `k` smallest we've seen so far. We would then also need to remove the largest to bring the total back down to `k`. For this, it makes sense to use a *Max*-Priority Queue. Here is an animation showing this process.

!?!../Documents/1337_priority_queue.json:960,540!?!

Once we've finished adding all the "strengths", we'll have the `k` smallest "strength"/index pairs in it. If we remove them from the PriorityQueue one-by-one, they'll be sorted from *largest to smallest*. We could either do this and then reverse, or we could iterate backwards over the output array inserting them.

**Algorithm**

Python has a *Min*-Priority Queue called *heapq*. We can convert it into a *Max*-Priority Queue by putting a negative sign in front of all the numbers going into it.

Java's `PriorityQueue` requires a [Comparator](https://docs.oracle.com/javase/8/docs/api/java/util/Comparator.html). We can make it behave as a *Max*-Priority Queue using this.

<iframe src="https://leetcode.com/playground/VVX5Smvf/shared" frameBorder="0" width="100%" height="500" name="VVX5Smvf"></iframe>

**Complexity Analysis**

- Time Complexity : $$O(m \, \log \, nk)$$.

    This approach is very similar to Approach #3. The only difference is that we're putting the "strengths" into a Priority Queue, and storing at most $$k$$ of them at a time.

    Calculating the strengths is still $$O(m \, \log \, n)$$.

    Inserting an item into a Priority Queue has a cost of $$O(\log\, x)$$, where $$x$$ is the maximum number of items that will be in the Priority Queue. For this algorithm, the maximum $$x$$ value is $$k$$ (not $$m$$). Therefore, each insertion costs $$log(k)$$. There are $$m$$ of these insertions, giving a total of $$O(m \, \log \,k)$$.

    Like before, we need to add $$m \, \log \, n + m \, \log \,k$$, and again we can't assume which is bigger out of $$\log \, n$$ and $$\log\,k$$. Therefore, the total time complexity is $$m \, \log \, n + m \, \log \, k = m \cdot (\log \, n + \log \, k) = O(m \,  \log nk)$$.

- Space Complexity : $$O(k)$$.

    We are keeping at most $$k$$ pieces of "strength" data at a time. Therefore, the space complexity is $$O(k)$$.

<br />

---

#### Approach 5: Vertical Iteration

**Intuition**

There's another, completely different, way of looking at the problem which as we'll see, decreases the space usage at the cost of time. Instead of going row-by-row calculating the "strengths", we can instead go column-by-column. Interestingly, we don't actually calculate the row "strengths" at all! This approach was inspired by the code of [lenchen1112](https://leetcode.com/problems/the-k-weakest-rows-in-a-matrix/discuss/496644/Clean-Python-3-beats-100-without-sort-or-heap) on the discussion forum.

On each cell we pass that is a `0`, we check if the cell to the left was a `1`. If it was, then we're on the first `0` of that row and should add its index to our output list. Once there are `k` indexes in the output list, we simply return the list. The order in which the rows are found using this approach turns out to be the sorted order we want!

Here is an animation showing the algorithm.

One edge case to be careful of is that it is possible some of the `k` rows will contain entirely `1`'s. (e.g. if the whole grid was `1`'s).

!?!../Documents/1337_vertical_algorithm.json:960,540!?!

**Algorithm**

<iframe src="https://leetcode.com/playground/d5g2T6jN/shared" frameBorder="0" width="100%" height="500" name="d5g2T6jN"></iframe>

**Complexity Analysis**

- Time Complexity : $$O(m \cdot n)$$.

    We are visiting each of the first $$m \cdot n - 1$$ cells at most once, and the last column of $$m$$ cells at most twice. In big-oh notation, $$O(m \cdot (n - 1) + 2 \cdot m) = O(m \cdot n)$$. At each of the cells we do a simple $$O(1)$$ check to determine whether or not it should be added to the output list. The output list doesn't need any further processing, and so does not add anything further to the time complexity. This leaves us with $$O(m \cdot n)$$.

- Space Complexity : $$O(1)$$.

    Because the output array is used *only* for gathering up the outputs to return, and these outputs require no further processing, this algorithm is considered to be $$O(1)$$ space. This is in contrast to the previous approaches that were also using the output array as working memory.

    Another way of looking at it is that if you needed to return the output values one-by-one (i.e. a generator function) for this algorithm, the array would disappear entirely. This is not true of the earlier approaches, which still require it to gather and then sort the values.


<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Best Solution, 100% Time/Space, Binary Search + Heap
- Author: seafmch
- Creation Date: Sun Feb 02 2020 12:04:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 02 2020 12:04:45 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int[] kWeakestRows(int[][] mat, int k) {
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> a[0] != b[0] ? b[0] - a[0] : b[1] - a[1]);
        int[] ans = new int[k];
        
        for (int i = 0; i < mat.length; i++) {
            pq.offer(new int[] {numOnes(mat[i]), i});
            if (pq.size() > k)
                pq.poll();
        }
        
        while (k > 0)
            ans[--k] = pq.poll()[1];
        
        return ans;
    }
    
    private int numOnes(int[] row) {
        int lo = 0;
        int hi = row.length;
        
        while (lo < hi) {
            int mid = lo + (hi - lo) / 2;
            
            if (row[mid] == 1)
                lo = mid + 1;
            else
                hi = mid;
        }
        
        return lo;
    }
}
```
</p>


### [Python] One-Liner using Sorting
- Author: C0R3
- Creation Date: Sun Feb 02 2020 12:44:47 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 10 2020 00:11:52 GMT+0800 (Singapore Standard Time)

<p>
We can sort the indexes of the weakest rows by using their sums as the key in sorting. Then we return the k first values.
```python
class Solution:
    def kWeakestRows(self, mat: List[List[int]], k: int) -> List[int]:
        return sorted(range(len(mat)), key=lambda x: sum(mat[x]))[:k]
```
Calculating the sum of a row has a time complexity of ```O(n)``` and is done ```m``` times. Python\'s key function will do a [Schwartzian transform](https://en.wikipedia.org/wiki/Schwartzian_transform) and cache the sums. Sorting has a complexity of ```O(m log m)```.

Time complexity: ```O(n * m + m log m)```
Space complexity: ```O(m)```

**Long and optimized algorithm**

If we want to improve on this we have to use more lines of code. We have to return the sorted indexes of the k weakest rows. Heaps allow us to sort an input while keeping only the desired k items in memory.
Heaps in Python will pop the smallest values first. Therefore we need to push tuples with inverted values onto our heap.
We still have to iterate over all rows in ```O(m)```. But now we only need ```O(log k)``` to add a value to the heap. We also need only ```O(k)``` space.
We can further improve our algorithm by using binary search to find the number of soldiers. That reduces the complexity per row to ```O(log n)```.
In the end our heap contains the desired row indexes. But they are not yet sorted. Therefore we need to push them onto a list until the heap is exhausted. We also discard the numbers of rows and invert the indexes back.
Since the heap returns the maximum items first we need to return the list in reversed order.

Time complexity: ```O(m * (log n + log k) + k log k)```
Space complexity: ```O(k)```
```python
from heapq import heappushpop, heappush, heappop

class Solution:
    def kWeakestRows(self, mat: List[List[int]], k: int) -> List[int]:
        heap = [] # Size: O(k)
        
        # Iterate over the rows in O(m).
        for index, row in enumerate(mat):
            soldier_count = self.soldier_count(row)
            
            # Push values to the heap in O(log k)
            if len(heap) == k:
                heappushpop(heap, (-soldier_count, -index))
            else:
                heappush(heap, (-soldier_count, -index))
        
        weakest_rows = [] # Size: O(k)
        
        # Push the heap values into our result list in O(k log k).
        while heap:
            weakest_rows.append(-heappop(heap)[1])
        
        # Return the result in reversed order.
        return weakest_rows[::-1]
    
    # Find the number of soldiers in a row using Binary Search in O(log n).
    def soldier_count(self, row: List[int]) -> int:
        low, high = 0, len(row) - 1

        while low < high:
            mid = (low + high + 1) // 2

            if not row[mid]:
                high = mid - 1
            else:
                low = mid

        # We need to return a count and not an index.
        # Therefore we need to increase the result by one if soldiers have been found.
        if row[0]:
            low += 1
        
        return low
```

**Optimized One-Liner**

This algorithm caches the row sums and uses heapq to keep the complexity down. The heap is filled using a generator expression with enumerate to keep the space complexity low. Sadly the built-in bisect can\'t be used because the rows would have to be reversed and that would take as long as taking the sum directly.

Time complexity: ```O(m * n + k log k)```
Space complexity: ```O(k)```
```python
class Solution:
    def kWeakestRows(self, mat: List[List[int]], k: int) -> List[int]:
        return [x[1] for x in heapq.nsmallest(k, ((sum(s), i) for i, s in enumerate(mat)))]
```
</p>


### C++ Set
- Author: votrubac
- Creation Date: Sun Feb 02 2020 12:01:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 02 2020 12:14:18 GMT+0800 (Singapore Standard Time)

<p>
Count the power of each row and store it in a set. We also add the row index to the set to {power, index} is sorted in the ascending order.

Then, return `k` first indices.

```CPP
vector<int> kWeakestRows(vector<vector<int>>& mat, int k) {
    set<pair<int, int>> m;
    for (auto i = 0; i < mat.size(); ++i) {
        auto p = accumulate(begin(mat[i]), end(mat[i]), 0);
        m.insert({p, i});
    }
    vector<int> res;
    for (auto it = begin(m); k > 0; ++it, --k)
        res.push_back(it->second);
    return res;
}
```
</p>


