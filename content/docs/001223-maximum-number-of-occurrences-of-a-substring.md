---
title: "Maximum Number of Occurrences of a Substring"
weight: 1223
#id: "maximum-number-of-occurrences-of-a-substring"
---
## Description
<div class="description">
<p>Given a string <code>s</code>, return the maximum number of ocurrences of <strong>any</strong> substring&nbsp;under the following rules:</p>

<ul>
	<li>The number of unique characters in the substring must be less than or equal to <code>maxLetters</code>.</li>
	<li>The substring size must be between <code>minSize</code> and <code>maxSize</code>&nbsp;inclusive.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aababcaab&quot;, maxLetters = 2, minSize = 3, maxSize = 4
<strong>Output:</strong> 2
<strong>Explanation:</strong> Substring &quot;aab&quot; has 2 ocurrences in the original string.
It satisfies the conditions, 2 unique letters and size 3 (between minSize and maxSize).
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aaaa&quot;, maxLetters = 1, minSize = 3, maxSize = 3
<strong>Output:</strong> 2
<strong>Explanation:</strong> Substring &quot;aaa&quot; occur 2 times in the string. It can overlap.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aabcabcab&quot;, maxLetters = 2, minSize = 2, maxSize = 3
<strong>Output:</strong> 3
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abcde&quot;, maxLetters = 2, minSize = 3, maxSize = 3
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= maxLetters &lt;= 26</code></li>
	<li><code>1 &lt;= minSize &lt;= maxSize &lt;= min(26, s.length)</code></li>
	<li><code>s</code> only contains lowercase English letters.</li>
</ul>
</div>

## Tags
- String (string)
- Bit Manipulation (bit-manipulation)

## Companies
- Roblox - 4 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Greedy approach + Sliding window O(n).
- Author: PhoenixDD
- Creation Date: Sun Dec 22 2019 12:01:40 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Dec 26 2019 10:53:10 GMT+0800 (Singapore Standard Time)

<p>
**Observation**

Let\'s assume we have a fixed size of window (`minSize`), we can easily use a sliding window of that size and check for the contraints within that window.
This can be done in `O(n)` using a hashmap to store the unique count of letters in the window.

The greedy part of the solution is that, we also notice that we only need `minSize` since if the `minSize` satisfies the constraints to have `distinct letters <= maxLetters ` any substring greater than that size would only add at max a new distinct letter.
Which esentially means that since the substring of size greater than minSize starisfies the constraint of `distinct letters <= maxLetters ` there will be a substring of this selected substring of size minSize that\'ll satisfy the same constraint and the frequency of this substring will be atleast as much as the parent substring.
We also know that number of shorter substrings are more than the longer ones , thus we only need to check for substrings of minSize satisfying the condition.

Eg: `s=abcabc, minSize=3, maxSize=6, maxLetter=3`
substrings satisfying the constraints are `abc`, `abca` ,`abcab`, `abcabc`, `bca`, `bcab`, `bcabc`, `cab`, `cabc` and `abc` again, we can see that all the substrings have a substring within it of `size=minSize` that satisfies all the conditions, like we have `abc` in first 4 substrings. Thus we only need to check for substrings of `size=minSize` that satisfy the condition to get our result.

The only problem is that this question ask for the max count of substrings. We can do this in two ways 1 we store the entire sub-string and it\'s count in a hashmap which is an `O(n)` operation (in this case `O(1)` since we can have at max `26` distinct characters) or we can use rolling hash during the sliding window to make it `O(1)`.

**Solution 1**
This solution stores substring frequency in hash map which is and `O(n)` operation (`O(26)` `~O(1)` in this case, since this creation and insertion of substring is at most `O(26)`).
```c++
class Solution {
public:
    int maxFreq(string s, int maxLetters, int minSize, int maxSize) 
    {
        int start=0,result=0;
        unordered_map<int,int> count;           	//Stores count of letters in the window.
        unordered_map<string,int> occurence;        //Stores count of occurence of that string.
        for(int i=0;i<s.length();i++)			//Sliding window
        {
            count[s[i]]++;						//Increase the count of letter at i.
            if(i-start+1>minSize)                  //Reduce the size of the window if increased to size.
            {
                if(--count[s[start]]==0)			//Remove the letter from map if count 0 to track unique letters.
                    count.erase(s[start]);
                start++;
            }
            if(i-start+1==minSize&&count.size()<=maxLetters)	//When the substring in the window matches the constraints.
                result=max(result,++occurence[s.substr(start,i-start+1)]); //Increase the occurence count of the string and get the max.
        }
        return result;
    }
};
```
**Complexity**
Space:  `O(26n) = O(n)`. Since `count` can have at max `26` characters and the number of substrings stored in occurence is at max `~n` with at max `26` characters each.
Time: `O(26n) = O(n)`. Since we create and store the substring in the hashMap which is `O(26)` `~O(1)` operation as the substring length can at max be `26`.

**Solution 2 (Rolling hash)**
Since, as explained above, in this solution we create hash from substrings and store in the hashmap which is an `O(1)` operation and thus this is an `O(n)` algorithm (explaination below).
Since the probability of collisions is very less I am avoiding an extra check for collisions, however it can be added to make the solution correct for the remaining cases of collisions.
```c++
static const unsigned long long MOD=(unsigned long long) pow(2,63)-1;               //Large mod least collisions.
class Solution {
public:
    unsigned long long int powMod(unsigned long long x,int y) {           //Modular power
        unsigned long long res=1;
        while (y>0) 
        { 
            if(y&1) 
                res=(res*x)%MOD; 
            y=y>>1;
            x=(x*x)%MOD;   
        } 
        return res; 
    } 
    int maxFreq(string s, int maxLetters, int minSize, int maxSize) 
    {
        unsigned long long hash=0,Pow=powMod(51,minSize-1);
        int start=0,result=0;
        unordered_map<int,int> count;           	//Stores count of letters in the window.
        unordered_map<long long,int> occurence;        //Stores count of occurence of that string.
        for(int i=0;i<s.length();i++)			//Sliding window
        {
            count[s[i]]++;						//Increase the count of letter at i.
            if(i-start+1>minSize)                  //Reduce the size of the window if increased to size.
            {
                hash-=(s[start]-\'a\')*Pow;
                if(--count[s[start]]==0)			//Remove the letter from map if count 0 to track unique letters.
                    count.erase(s[start]);
                start++;
            }
            hash=(hash*51+s[i]-\'a\')%MOD;
            if(i-start+1==minSize&&count.size()<=maxLetters)	//When the substring in the window matches the constraints.
                result=max(result,++occurence[hash]);     //Increase the occurence count of the string and get max.
        }
        return result;
    }
};
```
**Complexity**
Space: `O(n)`. Since `count` can have at max `26` charcters and the number of hashes stored in occurence is at max `~n`.
Time: `O(n)`. Since we store the string as an integer in the hashMap which is `O(1)` operation.

</p>


### [Python] 2-lines Counter
- Author: lee215
- Creation Date: Fri Dec 27 2019 20:31:09 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 30 2020 14:40:27 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
If a string have occurrences `x` times,
any of its substring must appear at least `x` times.

There must be a substring of length `minSize`, that has the most occurrences.
So that we just need to count the occurrences of all substring with length `minSize`.
<br>

## **Explanation**
Find the maximum occurrences of all substrings  with length `k = minSize`
<br>

## **Complexity**
Time `O(KN)`, where `K = minSize`
Space `O(KN)`
<br>

**Python:**
```python
    def maxFreq(self, s, maxLetters, k, maxSize):
        count = collections.Counter(s[i:i + k] for i in xrange(len(s) - k + 1))
        return max([count[w] for w in count if len(set(w)) <= maxLetters] + [0])
```

</p>


### Python solution, we don't need maxSize, do we?
- Author: dl3182
- Creation Date: Sun Dec 22 2019 12:04:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 22 2019 12:33:25 GMT+0800 (Singapore Standard Time)

<p>
Considering that the minsize always get the largest frequency, we can only check the minSize. Please correct me if I am wrong.

I may just realize that the problem is confusing. It means the distinct chars rather than unique chars if the solution below is correct.

```
import collections
def maxFreq(self, s, maxLetters, minSize, maxSize):
	counts = dict()
	for j in range(len(s)-minSize+1):
		word = s[j:j+minSize]
		if word in counts:
			counts[word]+=1
		else:
			if len(collections.Counter(word))<=maxLetters:
				counts[word]=1
	return max(counts.values()) if len(counts)!=0 else 0
	```
</p>


