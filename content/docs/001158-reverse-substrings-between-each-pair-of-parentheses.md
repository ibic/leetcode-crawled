---
title: "Reverse Substrings Between Each Pair of Parentheses"
weight: 1158
#id: "reverse-substrings-between-each-pair-of-parentheses"
---
## Description
<div class="description">
<p>You are given a string <code>s</code> that consists of lower case English letters and brackets.&nbsp;</p>

<p>Reverse the strings&nbsp;in each&nbsp;pair of matching parentheses, starting&nbsp;from the innermost one.</p>

<p>Your result should <strong>not</strong> contain any brackets.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;(abcd)&quot;
<strong>Output:</strong> &quot;dcba&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;(u(love)i)&quot;
<strong>Output:</strong> &quot;iloveu&quot;
<strong>Explanation:</strong>&nbsp;The substring &quot;love&quot; is reversed first, then the whole string is reversed.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;(ed(et(oc))el)&quot;
<strong>Output:</strong> &quot;leetcode&quot;
<strong>Explanation:</strong>&nbsp;First, we reverse the substring &quot;oc&quot;, then &quot;etco&quot;, and finally, the whole string.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;a(bcdefghijkl(mno)p)q&quot;
<strong>Output:</strong> &quot;apmnolkjihgfedcbq&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= s.length &lt;= 2000</code></li>
	<li><code>s</code> only contains lower case English characters and parentheses.</li>
	<li>It&#39;s guaranteed that all parentheses are balanced.</li>
</ul>

</div>

## Tags
- Stack (stack)

## Companies
- Postmates - 4 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Adobe - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Tenet, O(N) Solution
- Author: lee215
- Creation Date: Tue Sep 17 2019 01:40:31 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 15 2020 10:44:18 GMT+0800 (Singapore Standard Time)

<p>
# **Solution 1: Brute Force**
Here is the **brute force** solution, which seems really easy to write.
Nothing more to talk about.

Time `O(N^2)`, Space `O(N)`
<br>

**Python**
```py
    def reverseParentheses(self, s):
        res = [\'\']
        for c in s:
            if c == \'(\':
                res.append(\'\')
            elif c == \')\':
                res[len(res) - 2] += res.pop()[::-1]
            else:
                res[-1] += c
        return "".join(res)
```

**C++**
```cpp
    string reverseParentheses(string s) {
        vector<int> opened;
        string res;
        for (int i = 0; i < s.length(); ++i) {
            if (s[i] == \'(\')
                opened.push_back(res.length());
            else if (s[i] == \')\') {
                int j = opened.back();
                opened.pop_back();
                reverse(res.begin() + j, res.end());
            } else {
                res += s[i];
            }
        }
        return res;
    }
```
<br>

# **Solution 2: Wormholes**

## **Intuition**
Nice. I got a green accpeted with solution 1.
Now before move on, let us check the solutions in the discuss.

Hey hey hey wait, `ALL` solutions are **BRUTE FORCE** ?
Hmmmm... why not `O(N)`?

Fine fine fine, here comes an easy `O(N)` solution.

## **Explanation**

In the first pass,
use a stack to find all paired parentheses,
I assume you can do this.

Now just imgine that all parentheses are wormholes.
As you can see in the diagram,
the paired parentheses are connected to each other.

![image](https://assets.leetcode.com/users/lee215/image_1571315420.png)

First it follows the left green arrrow,
go into the left wormhole and get out from the right wormhole.
Then it iterates the whole content between parentheses.
Finally it follows the right arrow,
go into the left wormhole,
get out from the right wormhole and finish the whole trip.

So in the second pass of our solution,
it traverses through the paired parentheses
and generate the result string `res`.

`i` is the index of current position.
`d` is the direction of traversing.
<br>

## **Complexity**
Time `O(N)` for two passes
Space `O(N)`
<br>

**Java:**
```java
    public String reverseParentheses(String s) {
        int n = s.length();
        Stack<Integer> opened = new Stack<>();
        int[] pair = new int[n];
        for (int i = 0; i < n; ++i) {
            if (s.charAt(i) == \'(\')
                opened.push(i);
            if (s.charAt(i) == \')\') {
                int j = opened.pop();
                pair[i] = j;
                pair[j] = i;
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0, d = 1; i < n; i += d) {
            if (s.charAt(i) == \'(\' || s.charAt(i) == \')\') {
                i = pair[i];
                d = -d;
            } else {
                sb.append(s.charAt(i));
            }
        }
        return sb.toString();
    }
```

**C++:**
```cpp
    string reverseParentheses(string s) {
        int n = s.length();
        vector<int> opened, pair(n);
        for (int i = 0; i < n; ++i) {
            if (s[i] == \'(\')
                opened.push_back(i);
            if (s[i] == \')\') {
                int j = opened.back();
                opened.pop_back();
                pair[i] = j;
                pair[j] = i;
            }
        }
        string res;
        for (int i = 0, d = 1; i < n; i += d) {
            if (s[i] == \'(\' || s[i] == \')\')
                i = pair[i], d = -d;
            else
                res += s[i];
        }
        return res;
    }
```

**Python:**
```python
    def reverseParentheses(self, s):
        opened = []
        pair = {}
        for i, c in enumerate(s):
            if c == \'(\':
                opened.append(i)
            if c == \')\':
                j = opened.pop()
                pair[i], pair[j] = j, i
        res = []
        i, d = 0, 1
        while i < len(s):
            if s[i] in \'()\':
                i = pair[i]
                d = -d
            else:
                res.append(s[i])
            i += d
        return \'\'.join(res)
```

</p>


### [Python3] straightforward & easiest on discussion
- Author: cenkay
- Creation Date: Sun Sep 15 2019 22:03:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 15 2019 22:03:17 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def reverseParentheses(self, s: str) -> str:
        stack = [\'\']
        for c in s:
            if c == \'(\':
                stack.append(\'\')
            elif c == \')\':
                add = stack.pop()[::-1]
                stack[-1] += add
            else:
                stack[-1] += c
        return stack.pop()
```
</p>


### Simple Stack and Queue Solution
- Author: ebou
- Creation Date: Sun Sep 15 2019 12:04:47 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 16 2019 02:12:24 GMT+0800 (Singapore Standard Time)

<p>
```
    public String reverseParentheses(String s) {
        Stack<Character> st = new Stack<>();
        for(char c: s.toCharArray()){
            if( c == \')\'){
                Queue<Character> p = new LinkedList<>();
                while(!st.isEmpty() && st.peek() != \'(\') p.add(st.pop());
                if(!st.isEmpty()) st.pop();
                while(!p.isEmpty()) st.push(p.remove());
            } else {
                st.push(c);
            }
        }
        StringBuilder sb = new StringBuilder();
        while(!st.isEmpty()) sb.append(st.pop());
        
        return sb.reverse().toString();
    }
```
</p>


