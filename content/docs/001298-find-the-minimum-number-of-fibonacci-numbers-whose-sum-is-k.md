---
title: "Find the Minimum Number of Fibonacci Numbers Whose Sum Is K"
weight: 1298
#id: "find-the-minimum-number-of-fibonacci-numbers-whose-sum-is-k"
---
## Description
<div class="description">
<p>Given an integer&nbsp;<code>k</code>, <em>return the minimum number of Fibonacci numbers whose sum is equal to </em><code>k</code>. The same Fibonacci number can be used multiple times.</p>

<p>The Fibonacci numbers are defined as:</p>

<ul>
	<li><code>F<sub>1</sub> = 1</code></li>
	<li><code>F<sub>2</sub> = 1</code></li>
	<li><code>F<sub>n</sub> = F<sub>n-1</sub> + F<sub>n-2</sub></code> for <code>n &gt; 2.</code></li>
</ul>
It is guaranteed that for the given constraints we can always find such Fibonacci numbers that sum up to <code>k</code>.
<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> k = 7
<strong>Output:</strong> 2 
<strong>Explanation:</strong> The Fibonacci numbers are: 1, 1, 2, 3, 5, 8, 13, ... 
For k = 7 we can use 2 + 5 = 7.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> k = 10
<strong>Output:</strong> 2 
<strong>Explanation:</strong> For k = 10 we can use 2 + 8 = 10.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> k = 19
<strong>Output:</strong> 3 
<strong>Explanation:</strong> For k = 19 we can use 1 + 5 + 13 = 19.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= k &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Array (array)
- Greedy (greedy)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy Prove
- Author: lee215
- Creation Date: Sun Apr 19 2020 00:08:18 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 20 2020 16:44:56 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
We greedily subtract the biggest Fibonacci number `x` from `k`,
and recursive to find the result for `k - x`.
Then `f(k) = f(k - x) + 1`
<br>

# **Intuition II**
The feeling seems good,
but what if the interviewer ask for a prove?

Well, I can figure this out.
Will the interviewer really expect the prove during 45min?

If the interviewer doesn\'t request it,
can I ask them for the prove?

If they cannot even give me a prove,
why the hell they ask me this question?

Weak application for the company!
<br>

# **Prove**

Among all resolution with the minimum number of Fibonacci numbers,
we are find the lexicographically largest one.
In this resolution, we must:
1. uses each Fibonacci number at most once
2. never uses two consecutive Fibonacci numbers

**First**
In best resolution of `k`,
there is no two adjacent Fibonacci number.

This can be easily obtained,
because if we have `fibo[i] + fibo[i+1]`,
we can have `fibo[i+2]` instead,
and we save one number.
<br>

**Second**
In one of the best resolution of `k`,
there can be no duplicate Fibonacci number.
Assume we have two `fibo[i]`.

I noticed that `fibo[i] * 2 = fibo[i - 2] + fibo[i + 1]`
For example
Assuming `fibo = [1,2,3,5,8,13,21,33]`
2 + 2 = 1 + 3
3 + 3 = 1 + 5
5 + 5 = 2 + 8
8 + 8 = 3 + 13
13 + 13 = 5 + 21
21 + 21 = 8 + 33

If we have two same number,
we can always transfor to two different number.

Don\'t need worrying about if this process will cause other duplicate.
We clear the duplicate from the small to big.
<br>

**Third**
If no dup, no adjacent, we must take the biggest.
`fibo[0] + fibo[2] + fibo[4] + ... + fibo[2n] = fibo[2n + 1] - 1`
`fibo[1] + fibo[3] + fibo[5] + .... + fibo[2n-1] = fibo[2n] - 1`

For example:
1 = 2 - 1
1 + 3 = 5 - 1
1 + 3 + 8 = 13 - 1
1 + 3 + 8 + 21 = 34 - 1

2 = 3 - 1
2 + 5 = 8 - 1
2 + 5 + 13 = 21 - 1
2 + 5 + 13 + 34 = 55 - 1

So if `k >= fibo[2n]`,
and we don\'t take `fibo[2n]`
the rest sum of every two Fibo numbers can only reach a maximum `fibo[2n] - 1`

The same for the case where `k >= fibo[2n + 1]`
<br>

@OneSheep supported a supplement explanation here:
If we use [1,1,2,3,5,8,13,21,34].

f[2n+1]
=f[2n]+f[2n-1]
=f[2n]+f[2n-2]+f[2n-3]
=f[2n]+f[2n-2]+...+f[6]+f[5]
=...+f[6]+f[4]+f[3]
=...+f[6]+f[4]+f[2]+f[1]
=...+f[6]+f[4]+f[2]+f[0] (because f[0]==f[1])

f[2n]
=f[2n-1]+f[2n-2]
=f[2n-1]+f[2n-3]+f[2n-4]
=...+f[5]+f[4]
=...+f[5]+f[3]+f[2]
=...+f[5]+f[3]+f[1]+f[0]
<br>


# Solution 1: Recursion
Time `O((logk)^2)`, since `O(log k)` Fibonacci numbers smaller than `k`.
Space `O(logK)`, can be saved by tail recursion.

**Java**
```java
    public int findMinFibonacciNumbers(int k) {
        if (k < 2) return k;
        int a = 1, b = 1;
        while (b <= k) {
            b += a;
            a = b - a;
        }
        return 1 + findMinFibonacciNumbers(k - a);
    }
```
**C++**
```cpp
    int findMinFibonacciNumbers(int k) {
        if (k < 2) return k;
        int a = 1, b = 1;
        while (b <= k) {
            swap(a, b);
            b += a;
        }
        return 1 + findMinFibonacciNumbers(k - a);
    }
```
**Python:**
```py
    def findMinFibonacciNumbers(self, k):
        if k < 2: return k
        a, b = 1, 1
        while b <= k:
            a, b = b, a + b
        return self.findMinFibonacciNumbers(k - a) + 1
```

# Solution 2: Iteration
Time `O(logk)`
Space `O(1)
`
**Python:**
```py
    def findMinFibonacciNumbers(self, k):
        res, a, b = 0, 1, 1
        while b <= k:
            a, b = b, a + b
        while a > 0:
            if a <= k:
                k -= a
                res += 1
            a, b = b - a, a
        return res
```

</p>


### Java Greedy by TreeSet
- Author: hobiter
- Creation Date: Fri Jun 05 2020 08:12:35 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 05 2020 08:12:35 GMT+0800 (Singapore Standard Time)

<p>
Greedy, find next max fib lower or equal than k, as adder.
TreeSet is the best tool to use for O(lgN) to search fib;
```
public int findMinFibonacciNumbers(int k) {
        if (k < 2) return k;
        TreeSet<Integer> set = new TreeSet<>();
        for (int a = 0, b = 1; b < k;) {
            int tmp = b; 
            b += a;
            a = tmp;
            set.add(b);
        }
        int res = 0;
        while (k > 0) {
            int fib = set.floor(k);
            k -= fib;
            res++;
        }
        return res;
    }
```
</p>


### C++ Greedy
- Author: votrubac
- Creation Date: Sun Apr 19 2020 00:02:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 09:03:26 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**
This problem is 4 point (easy), so greedy should work. This is good enough for the contest; for an interview you can start by working out few examples to get on the right track with your interviewer, write the code, and then perhaps come up with more-less formalized proof.

You can check out this proof [by Lee](https://leetcode.com/problems/find-the-minimum-number-of-fibonacci-numbers-whose-sum-is-k/discuss/?currentPage=1&orderBy=most_votes&query=).

I found another one [here](http://codeforces.com/blog/entry/67171) (by misof). TL;DR:
> Each number has exactly one representation that uses each Fibonacci number at most once and never uses two consecutive Fibonacci numbers. The greedy algorithm constructs this representation and therefore its output is always optimal.

```cpp
int findMinFibonacciNumbers(int k) {
    int f1 = 0, f2 = 1;
    while (f2 <= k) {
        swap(f1, f2);
        f2 += f1;
    }
    return 1 + (k == f1 ? 0 : findMinFibonacciNumbers(k - f1));
}
```
</p>


