---
title: "Maximum Nesting Depth of the Parentheses"
weight: 1614
#id: "maximum-nesting-depth-of-the-parentheses"
---
## Description
<div class="description">
<p>A string is a <strong>valid parentheses string</strong> (denoted <strong>VPS</strong>) if it meets one of the following:</p>

<ul>
	<li>It is an empty string <code>&quot;&quot;</code>, or a single character not equal to <code>&quot;(&quot;</code> or <code>&quot;)&quot;</code>,</li>
	<li>It can be written as <code>AB</code> (<code>A</code> concatenated with <code>B</code>), where <code>A</code> and <code>B</code> are <strong>VPS</strong>&#39;s, or</li>
	<li>It can be written as <code>(A)</code>, where <code>A</code> is a <strong>VPS</strong>.</li>
</ul>

<p>We can similarly define the <strong>nesting depth</strong> <code>depth(S)</code> of any VPS <code>S</code> as follows:</p>

<ul>
	<li><code>depth(&quot;&quot;) = 0</code></li>
	<li><code>depth(A + B) = max(depth(A), depth(B))</code>, where <code>A</code> and <code>B</code> are <strong>VPS</strong>&#39;s</li>
	<li><code>depth(&quot;(&quot; + A + &quot;)&quot;) = 1 + depth(A)</code>, where <code>A</code> is a <strong>VPS</strong>.</li>
</ul>

<p>For example, <code>&quot;&quot;</code>, <code>&quot;()()&quot;</code>, and <code>&quot;()(()())&quot;</code> are <strong>VPS</strong>&#39;s (with nesting depths 0, 1, and 2), and <code>&quot;)(&quot;</code> and <code>&quot;(()&quot;</code> are not <strong>VPS</strong>&#39;s.</p>

<p>Given a <strong>VPS</strong> represented as string <code>s</code>, return <em>the <strong>nesting depth</strong> of </em><code>s</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;(1+(2*3)+((<u>8</u>)/4))+1&quot;
<strong>Output:</strong> 3
<strong>Explanation:</strong> Digit 8 is inside of 3 nested parentheses in the string.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;(1)+((2))+(((<u>3</u>)))&quot;
<strong>Output:</strong> 3
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1+(<u>2</u>*3)/(2-1)&quot;
<strong>Output:</strong> 1
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;<u>1</u>&quot;
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 100</code></li>
	<li><code>s</code> consists of digits <code>0-9</code> and characters <code>&#39;+&#39;</code>, <code>&#39;-&#39;</code>, <code>&#39;*&#39;</code>, <code>&#39;/&#39;</code>, <code>&#39;(&#39;</code>, and <code>&#39;)&#39;</code>.</li>
	<li>It is guaranteed that parentheses expression <code>s</code> is a <strong>VPS</strong>.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Intel - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Parentheses Problem Foundation
- Author: lee215
- Creation Date: Sun Oct 11 2020 12:03:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 12:03:35 GMT+0800 (Singapore Standard Time)

<p>
# **Exlanation**
Ignore digits and signs,
only count the current open parentheses `cur`.

The depth equals to the maximum open parentheses.
<br>

# **Complexity**
Time `O(N)`
Space `O(1)`
<br>

**Java:**
```java
    public int maxDepth(String s) {
        int res = 0, cur = 0;
        for (int i = 0; i < s.length(); ++i) {
            if (s.charAt(i) == \'(\')
                res = Math.max(res, ++cur);
            if (s.charAt(i) == \')\')
                cur--;
        }
        return res;
    }
```

**C++:**
```cpp
    int maxDepth(string s) {
        int res = 0, cur = 0;
        for (char& c: s) {
            if (c == \'(\')
                res = max(res, ++cur);
            if (c == \')\')
                cur--;
        }
        return res;
    }
```

**Python:**
```py
    def maxDepth(self, s):
        res = cur = 0
        for c in s:
            if c == \'(\':
                cur += 1
                res = max(res, cur)
            if c == \')\':
                cur -= 1
        return res
```
<br><br>

# More Parentheses Problem To Advance
Here is a ladder of parentheses problem, in order of problem number.

- 1541. [Minimum Insertions to Balance a Parentheses String](https://leetcode.com/problems/minimum-insertions-to-balance-a-parentheses-string/discuss/780199/JavaC%2B%2BPython-Straight-Forward-One-Pass)
- 1249. Minimum Remove to Make Valid Parentheses
- 1111. [Maximum Nesting Depth of Two Valid Parentheses Strings](https://leetcode.com/problems/maximum-nesting-depth-of-two-valid-parentheses-strings/discuss/328841/JavaC%2B%2BPython-O(1)-Extra-Space-Except-Output)
- 1190. [Reverse Substrings Between Each Pair of Parentheses](https://leetcode.com/problems/reverse-substrings-between-each-pair-of-parentheses/discuss/383670/JavaC%2B%2BPython-Tenet-O(N)-Solution)
- 1021. [Remove Outermost Parentheses](https://leetcode.com/problems/remove-outermost-parentheses/discuss/270022/JavaC%2B%2BPython-Count-Opened-Parenthesis)
- 921. [Minimum Add to Make Parentheses Valid](https://leetcode.com/problems/minimum-add-to-make-parentheses-valid/discuss/181132/C%2B%2BJavaPython-Straight-Forward-One-Pass)
- 856. [Score of Parentheses](https://leetcode.com/problems/score-of-parentheses/discuss/141777/C%2B%2BJavaPython-O(1)-Space)

</p>


### [Java/Python 3] Parentheses balance: O(n) code w/ brief explanation and analysis..
- Author: rock
- Creation Date: Sun Oct 11 2020 12:03:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 13:21:38 GMT+0800 (Singapore Standard Time)

<p>
Count the depth, plus `1` and minus `1` for `(` and `)` respectively; The max value of `depth` is the solution.

```java
    public int maxDepth(String s) {
        int maxDepth = 0;
        for (int i = 0, depth = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (c == \'(\' || c == \')\')
                depth += c == \'(\' ? 1 : -1;
            maxDepth = Math.max(maxDepth, depth);
        }
        return maxDepth;
    }
```
```python
    def maxDepth(self, s: str) -> int:
        d = {\'(\' : 1, \')\' : -1}
        max_depth = depth = 0
        for c in s:
            depth += d.get(c, 0)
            max_depth = max(depth, max_depth)
        return max_depth
```
**Analysis:**

Time: O(n), space: O(1).

----
Similar problems:

[20. Valid Parentheses](https://leetcode.com/problems/valid-parentheses)
[22. Generate Parentheses](https://leetcode.com/problems/generate-parentheses)
[32. Longest Valid Parentheses ](https://leetcode.com/problems/longest-valid-parentheses)
[241. Different Ways to Add Parentheses](https://leetcode.com/problems/different-ways-to-add-parentheses)
[301. Remove Invalid Parentheses](https://leetcode.com/problems/remove-invalid-parentheses)
[678. Valid Parenthesis String](https://leetcode.com/problems/valid-parenthesis-string)
[856. Score of Parentheses](https://leetcode.com/problems/score-of-parentheses)
[921. Minimum Add to Make Parentheses Valid](https://leetcode.com/problems/minimum-add-to-make-parentheses-valid/)
[1021. Remove Outermost Parentheses](https://leetcode.com/problems/remove-outermost-parentheses/)
[1111. Maximum Nesting Depth of Two Valid Parentheses Strings](https://leetcode.com/problems/maximum-nesting-depth-of-two-valid-parentheses-strings/)
[1190. Reverse Substrings Between Each Pair of Parentheses](https://leetcode.com/problems/reverse-substrings-between-each-pair-of-parentheses/)
[1249. Minimum Remove to Make Valid Parentheses](https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses)
[1541. Minimum Insertions to Balance a Parentheses String](https://leetcode.com/problems/minimum-insertions-to-balance-a-parentheses-string/)
</p>


### C++ O(n)
- Author: votrubac
- Creation Date: Sun Oct 11 2020 12:05:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 12:05:49 GMT+0800 (Singapore Standard Time)

<p>
```cpp
int maxDepth(string s) {
    int d = 0, m_d = 0;
    for (auto ch : s) {
        d += ch == \'(\' ? 1 : ch == \')\' ? -1 : 0;
        m_d = max(m_d, d);
    }
    return m_d;
}
```
</p>


