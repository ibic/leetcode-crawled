---
title: "Contiguous Array"
weight: 495
#id: "contiguous-array"
---
## Description
<div class="description">
<p>Given a binary array, find the maximum length of a contiguous subarray with equal number of 0 and 1. </p>


<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [0,1]
<b>Output:</b> 2
<b>Explanation:</b> [0, 1] is the longest contiguous subarray with equal number of 0 and 1.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> [0,1,0]
<b>Output:</b> 2
<b>Explanation:</b> [0, 1] (or [1, 0]) is a longest contiguous subarray with equal number of 0 and 1.
</pre>
</p>

<p><b>Note:</b>
The length of the given binary array will not exceed 50,000.
</p>
</div>

## Tags
- Hash Table (hash-table)

## Companies
- Facebook - 5 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Quora - 6 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Robinhood - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

**Algorithm**

The brute force approach is really simple. We consider every possible subarray within the given array and count the number of zeros and ones in each subarray. Then, we find out the maximum size subarray with equal no. of zeros and ones out of them.

<iframe src="https://leetcode.com/playground/sPZqbexo/shared" frameBorder="0" name="sPZqbexo" width="100%" height="428"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. We consider every possible subarray by traversing over the complete array for every start point possible.

* Space complexity : $$O(1)$$. Only two variables $$zeroes$$ and $$ones$$ are required.

---
#### Approach #2 Using Extra Array [Accepted]

**Algorithm**

In this approach, we make use of a $$count$$ variable, which is used to store the relative number of ones and zeros encountered so far while traversing the array. The $$count$$ variable is incremented by one for every $$\text{1}$$ encountered and the same is decremented by one for every $$\text{0}$$ encountered.

We start traversing the array from the beginning. If at any moment, the $$count$$ becomes zero, it implies that we've encountered equal number of zeros and ones from the beginning till the current index of the array($$i$$). Not only this, another point to be noted is that  if we encounter the same $$count$$ twice while traversing the array, it means that the number of zeros and ones are equal between the indices corresponding to the equal $$count$$ values. The following figure illustrates the observation for the sequence `[0 0 1 0 0 0 1 1]`:

![Contiguous_Array](../Figures/535_Contiguous_Array.PNG)

In the above figure, the subarrays between (A,B), (B,C) and (A,C) (lying between indices corresponing to $$count = 2$$) have equal number of zeros and ones.

Another point to be noted is that the largest subarray is the one between the points (A, C). Thus, if we keep a track of the indices corresponding to the same $$count$$ values that lie farthest apart, we can determine the size of the largest subarray with equal no. of zeros and ones easily.

Now, the $$count$$ values can range between $$\text{-n}$$ to $$\text{+n}$$, with the extreme points corresponding to the complete array being filled with all 0's and all 1's respectively. Thus, we make use of an array $$arr$$(of size $$\text{2n+1}$$to keep a track of the various $$count$$'s encountered so far. We make an entry containing the current element's index ($$i$$) in the $$arr$$ for a new $$count$$ encountered everytime. Whenever, we come across the same $$count$$ value later while traversing the array, we determine the length of the subarray lying between the indices corresponding to the same $$count$$ values.

<iframe src="https://leetcode.com/playground/Nvw6WnPN/shared" frameBorder="0" name="Nvw6WnPN" width="100%" height="411"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. The complete array is traversed only once.

* Space complexity : $$O(n)$$. $$arr$$ array of size $$\text{2n+1}$$ is used.

---
#### Approach #3 Using HashMap [Accepted]

**Algorithm**

This approach relies on the same premise as the previous approach. But, we need not use an array of size $$\text{2n+1}$$, since it isn't necessary that we'll encounter all the $$count$$ values possible. Thus, we make use of a HashMap $$map$$ to store the entries in the form of $$(index, count)$$. We make an entry for a $$count$$ in the $$map$$ whenever the $$count$$ is encountered first, and later on use the correspoding index to find the length of the largest subarray with equal no. of zeros and ones when the same $$count$$ is encountered again.

The following animation depicts the process:
<!--![Contiguous_Array](../Figures/525_Contiguous_Array.gif)-->
!?!../Documents/525_Contiguous_Array.json:1000,563!?!

<iframe src="https://leetcode.com/playground/nG5CTUD8/shared" frameBorder="0" name="nG5CTUD8" width="100%" height="360"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n)$$. The entire array is traversed only once.

* Space complexity : $$O(n)$$. Maximum size of the HashMap $$map$$ will be $$\text{n}$$, if all the elements are either 1 or 0.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python O(n) Solution with Visual Explanation
- Author: NoAnyLove
- Creation Date: Mon Feb 20 2017 06:51:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 11:13:13 GMT+0800 (Singapore Standard Time)

<p>
My idea is very similar to others, but let me try to explain it more visually. My thought was inspired by 121. Best Time to Buy and Sell Stock.

Let's have a variable `count` initially equals 0 and traverse through `nums`. Every time we meet a 0, we decrease `count` by 1, and increase `count` by 1 when we meet 1. It's pretty easy to conclude that we have a contiguous subarray with equal number of 0 and 1 when `count` equals 0. 

What if we have a sequence `[0, 0, 0, 0, 1, 1]`? the maximum length is 4, the `count` starting from 0, will equal -1, -2, -3, -4, -3, -2, and won't go back to 0 again. But wait, the longest subarray with equal number of 0 and 1 started and ended when `count` equals -2. We can plot the changes of `count` on a graph, as shown below. Point (0,0) indicates the initial value of `count` is 0, so we count the sequence starting from index 1. The longest subarray is from index 2 to 6.

![0_1487543028189_figure_1.png](/uploads/files/1487543036101-figure_1.png) 

From above illustration, we can easily understand that two points with the same y-axis value indicates the sequence between these two points has equal number of 0 and 1.

Another example, sequence `[0, 0, 1, 0, 0, 0, 1, 1]`, as shown below,

![0_1487543752969_figure_2.png](/uploads/files/1487543760956-figure_2.png) 

There are 3 points have the same y-axis value -2. So subarray from index 2 to 4 has equal number of 0 and 1, and subarray from index 4 to 8 has equal number of 0 and 1. We can add them up to form the longest subarray from index 2 to 8, so the maximum length of the subarray is 8 - 2 = 6.

Yet another example, sequence [0, 1, 1, 0, 1, 1, 1, 0], as shown below. The longest subarray has the y-axis value of 0.

![0_1487544400951_figure_3.png](/uploads/files/1487544408978-figure_3.png) 

To find the maximum length, we need a dict to store the value of `count` (as the key) and its associated index (as the value). We only need to save a `count` value and its index at the first time, when the same `count` values appear again, we use the new index subtracting the old index to calculate the length of a subarray. A variable `max_length` is used to to keep track of the current maximum length.

```python
class Solution(object):
    def findMaxLength(self, nums):
        count = 0
        max_length=0
        table = {0: 0}
        for index, num in enumerate(nums, 1):
            if num == 0:
                count -= 1
            else:
                count += 1
            
            if count in table:
                max_length = max(max_length, index - table[count])
            else:
                table[count] = index
        
        return max_length
```
</p>


### Easy Java O(n) Solution, PreSum + HashMap
- Author: shawngao
- Creation Date: Sun Feb 19 2017 12:07:36 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 02:18:42 GMT+0800 (Singapore Standard Time)

<p>
The idea is to change ```0``` in the original array to ```-1```. Thus, if we find ```SUM[i, j] == 0``` then we know there are even number of ```-1``` and ```1``` between index ```i``` and ```j```. Also put the ```sum``` to ```index``` mapping to a HashMap to make search faster.

```
public class Solution {
    public int findMaxLength(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) nums[i] = -1;
        }
        
        Map<Integer, Integer> sumToIndex = new HashMap<>();
        sumToIndex.put(0, -1);
        int sum = 0, max = 0;
        
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            if (sumToIndex.containsKey(sum)) {
                max = Math.max(max, i - sumToIndex.get(sum));
            }
            else {
                sumToIndex.put(sum, i);
            }
        }
        
        return max;
    }
}
```
</p>


### C++ - Crystal Clear Explanation
- Author: Chronoviser
- Creation Date: Mon Apr 13 2020 15:50:23 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 13 2020 15:50:23 GMT+0800 (Singapore Standard Time)

<p>
**We have to find the longest contigous subarray with equal number of ones and zeroes**

Thinking Process:
1. Starting from left of array and keep adding elements to a variable **sum**
2. Add **-1 for 0** and **1 for 1**
3. Now, everytime sum becomes zero, we know all the elements from begining of array have been neutralized , meaning we have got equal number of ones and zeroes,
 let this occurs at index i, so longestContArray = i+1 (\'coz w\'re dealing with indices)  
 4. But we are not done yet!, consider the case : [1,**1,0**,1,1]
*  In this case sum will never become zero,
*  but there exists a subarray of length 2, having equal  0 & 1
*  let\'s see the value of sum at index : 1 and 3
*  Ohh!! sum = 2 for both indices
*  what does this mean???
* **This means that if we get the same sum value for two indices i and j, then all the elements within the range [i,j) or (i,j] have been neutralized**
*  What datastructure can remember the sum and index
*  Ofcourse ! Map, so we use a map to store the sum and index values, 
*  if sum == 0 then we have already solved the cases
*  but if sum!=0 and this sum doesn\'t already exist in the map,
then store it with it\'s corresponding index
* but if sum !=0 and sum already exists in the map then, 
dependig on whether i - m[sum] > LongestContArray, update LongestContArray
* Note we need to store a unique sum value only once, after that whenever we encounter the same sum again our interval length is going to increase only and that is what we want
ex- [1,0,1,0,1] we get sum = 1 three times 
we store sum = 1 for index  = 0 only
and never update it as we want longest length

CODE:
```
int findMaxLength(vector<int>& nums) {
       int sum=0;
       unordered_map<int,int> m;
       unsigned int longestContArray = 0;
       
        for(int i=0;i<nums.size();i++){
           sum += (nums[i]==0)?-1:1;
           
           auto it = m.find(sum);
           
           if(sum == 0){
              if(longestContArray < i+1)
               longestContArray = i+1;
           }
           else if(it != m.end()){
              if(longestContArray < i-it->second)
               longestContArray = i-it->second;
           }
           else if(it == m.end())
                m.insert({sum,i});
       }
        return longestContArray;
    }
</p>


