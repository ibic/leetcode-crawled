---
title: "Two City Scheduling"
weight: 1004
#id: "two-city-scheduling"
---
## Description
<div class="description">
<p>A company is planning to interview <code>2n</code> people. Given the array <code>costs</code> where <code>costs[i] = [aCost<sub>i</sub>, bCost<sub>i</sub>]</code>,&nbsp;the cost of flying the <code>i<sup>th</sup></code> person to city <code>a</code> is <code>aCost<sub>i</sub></code>, and the cost of flying the <code>i<sup>th</sup></code> person to city <code>b</code> is <code>bCost<sub>i</sub></code>.</p>

<p>Return <em>the minimum cost to fly every person to a city</em> such that exactly <code>n</code> people arrive in each city.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> costs = [[10,20],[30,200],[400,50],[30,20]]
<strong>Output:</strong> 110
<strong>Explanation: </strong>
The first person goes to city A for a cost of 10.
The second person goes to city A for a cost of 30.
The third person goes to city B for a cost of 50.
The fourth person goes to city B for a cost of 20.

The total minimum cost is 10 + 30 + 50 + 20 = 110 to have half the people interviewing in each city.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> costs = [[259,770],[448,54],[926,667],[184,139],[840,118],[577,469]]
<strong>Output:</strong> 1859
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> costs = [[515,563],[451,713],[537,709],[343,819],[855,779],[457,60],[650,359],[631,42]]
<strong>Output:</strong> 3086
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2n == costs.length</code></li>
	<li><code>2 &lt;= costs.length &lt;= 100</code></li>
	<li><code>costs.length</code> is even.</li>
	<li><code>1 &lt;= aCost<sub>i</sub>, bCost<sub>i</sub> &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Bloomberg - 12 (taggedByAdmin: true)
- Google - 4 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Greedy.

**Greedy algorithms**

Greedy problems usually look like 
"Find minimum number of _something_ to do _something_" or 
"Find maximum number of _something_ to fit in _some conditions_", 
and typically propose an unsorted input.

> The idea of greedy algorithm is to pick the _locally_
optimal move at each step, that will lead to the _globally_ optimal solution.

The standard solution has $$\mathcal{O}(N \log N)$$ time complexity and consists of two parts:

- Figure out how to sort the input data ($$\mathcal{O}(N \log N)$$ time).
That could be done directly by a sorting or indirectly by a heap usage. 
Typically sort is better than the heap usage because of gain in space.

- Parse the sorted input to have a solution ($$\mathcal{O}(N)$$ time). 

Please notice that in case of well-sorted input one doesn't need the first 
part and the greedy solution could have $$\mathcal{O}(N)$$ time complexity,
[here is an example](https://leetcode.com/articles/gas-station/).

> How to prove that your greedy algorithm provides globally optimal solution?

Usually you could use the [proof by contradiction](https://en.wikipedia.org/wiki/Proof_by_contradiction). 

**Intuition**

Let's figure out how to sort the input here.
The input should be sorted by a parameter which indicates a money
lost for the company. 

The company would pay anyway : `price_A` to send a person to the city
A, or `price_B` to send a person to the city B.
By sending the person to the city A, the company would lose `price_A - price_B`,
which could negative or positive. 

![bla](../Figures/1029/users.png)

To optimize the total costs, let's sort the persons by `price_A - price_B`
and then send the first `n` persons to the city A, 
and the others to the city B, because this way the 
company costs are minimal.    


**Algorithm**

Now the algorithm is straightforward :

- Sort the persons in the ascending order by `price_A - price_B` 
parameter, which indicates the company additional costs.

- To minimise the costs, send `n` persons with the smallest `price_A - price_B`
to the city A, and the others to the city B.

**Implementation**

<iframe src="https://leetcode.com/playground/j7VyU39i/shared" frameBorder="0" width="100%" height="395" name="j7VyU39i"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N \log N)$$ because of sorting of
input data.
 
* Space complexity : $$\mathcal{O}(1)$$ since it's a constant 
space solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java | C++ | Python3 | With detailed explanation
- Author: logan138
- Creation Date: Wed Jun 03 2020 15:34:03 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 03 2020 21:20:16 GMT+0800 (Singapore Standard Time)

<p>
```
EXPLANATION:-
The problem is to send n persons to city A 
and n persons to city B with minimum cost.

The idea is to send each person to city A.
costs = [[10,20],[30,200],[400,50],[30,20]]

So, totalCost = 10 + 30 + 400 + 30 = 470

Now, we need to send n persons to city B.
Which persons do we need to send city B?

Here, we need to minimize the cost.
We have already paid money to go to city A.
So, Send the persons to city B who get more refund
so that our cost will be minimized.

So, maintain refunds of each person
refund[i] = cost[i][1] - cost[i][0]

So, refunds of each person
    refund = [10, 170, -350, -10]

Here, refund +ve means we need to pay
             -ve means we will get refund.

So, sort the refund array.

refund = [-350, -10, 10, 170]

Now, get refund for N persons,
totalCost += 470 + -350 + -10 = 110

So, minimum cost is 110


IF YOU HAVE ANY DOUBTS, FEEL FREE TO ASK.
IF YOU UNDERSTAND, DON\'T FORGET TO UPVOTE.
```
```
Java:-
class Solution {
    public int twoCitySchedCost(int[][] costs) {
        int N = costs.length/2;
        int[] refund = new int[N * 2];
 \xA0 \xA0 \xA0 \xA0int minCost = 0, index = 0;
        for(int[] cost : costs){
            refund[index++] = cost[1] - cost[0];
            minCost += cost[0];
        }
        Arrays.sort(refund);
        for(int i = 0; i < N; i++){
            minCost += refund[i];
        }
        return minCost;
    }
}

C++:-
class Solution {
public:
    int twoCitySchedCost(vector<vector<int>>& costs) {
        vector<int> refund;
        int N = costs.size()/2;
 \xA0 \xA0 \xA0 \xA0int minCost = 0;
 \xA0 \xA0 \xA0  for(vector<int> cost : costs){
            minCost += cost[0];
            refund.push_back(cost[1] - cost[0]); 
        }
        sort(refund.begin(), refund.end());
        for(int i = 0; i < N; i++){
            minCost += refund[i];
        }
        return minCost;
    }
};

Python3:-
class Solution:
    def twoCitySchedCost(self, costs: List[List[int]]) -> int:
        refund = []
        N = len(costs)//2
        minCost = 0
        for A, B in costs:
            refund.append(B - A)
            minCost += A
        refund.sort()
        for i in range(N):
            minCost += refund[i]
        return minCost
```
</p>


### C++ O(n log n) sort by savings
- Author: votrubac
- Creation Date: Sun Apr 21 2019 12:01:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 21 2019 12:01:57 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
How much money can we save if we fly a person to A vs. B? To minimize the total cost, we should fly the person with the maximum saving to A, and with the minimum - to B.

Example: [30, 100], [40, 90], [50, 50], [70, 50].
Savings: 70, 50, 0, -20.

Obviously, first person should fly to A, and the last - to B.
# Solution
We sort the array by the difference between costs for A and B. Then, we fly first ```N``` people to A, and the rest - to B.
```
int twoCitySchedCost(vector<vector<int>>& cs, int res = 0) {
  sort(begin(cs), end(cs), [](vector<int> &v1, vector<int> &v2) {
    return (v1[0] - v1[1] < v2[0] - v2[1]);
  });
  for (auto i = 0; i < cs.size() / 2; ++i) {
    res += cs[i][0] + cs[i + cs.size() / 2][1];
  }
  return res;
}
```
# Optimized Solution
Actually, we do not need to perfectly sort all cost differences, we just need the biggest savings (to fly to A) to be in the first half of the array. So, we can use the quick select algorithm (```nth_element``` in C++) and use the middle of the array as a pivot. 

This brings the runtime down from 8 ms to 4 ms (thanks [@popeye1](https://leetcode.com/popeye1/) for the tip!)
```
int twoCitySchedCost(vector<vector<int>>& cs, int res = 0) {
  nth_element(begin(cs), begin(cs) + cs.size() / 2, end(cs), [](vector<int> &a, vector<int> &b) {
    return (a[0] - a[1] < b[0] - b[1]);
  });
  for (auto i = 0; i < cs.size() / 2; ++i) {
    res += cs[i][0] + cs[i + cs.size() / 2][1];
  }
  return res;
}
```
# Complexity Analysis
Runtime: *O(n log n)*. We sort the array then go through it once. The second solution has a better average case runtime.
Memory: *O(1)*. We sort the array in-place.
</p>


### Java DP - Easy to Understand
- Author: yhcddup
- Creation Date: Sun Apr 21 2019 12:03:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 21 2019 12:03:53 GMT+0800 (Singapore Standard Time)

<p>
`dp[i][j]` represents the cost when considering first `(i + j)` people in which `i` people assigned to city A and `j` people assigned to city B.

```
class Solution {
    public int twoCitySchedCost(int[][] costs) {
        int N = costs.length / 2;
        int[][] dp = new int[N + 1][N + 1];
        for (int i = 1; i <= N; i++) {
            dp[i][0] = dp[i - 1][0] + costs[i - 1][0];
        }
        for (int j = 1; j <= N; j++) {
            dp[0][j] = dp[0][j - 1] + costs[j - 1][1];
        }
        for (int i = 1; i <= N; i++) {
            for (int j = 1; j <= N; j++) {
                dp[i][j] = Math.min(dp[i - 1][j] + costs[i + j - 1][0], dp[i][j - 1] + costs[i + j - 1][1]);
            }
        }
        return dp[N][N];
    }
}
```
</p>


