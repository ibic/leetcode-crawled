---
title: "Most Common Word"
weight: 758
#id: "most-common-word"
---
## Description
<div class="description">
<p>Given a paragraph&nbsp;and a list of banned words, return the most frequent word that is not in the list of banned words.&nbsp; It is guaranteed there is at least one word that isn&#39;t banned, and that the answer is unique.</p>

<p>Words in the list of banned words are given in lowercase, and free of punctuation.&nbsp; Words in the paragraph are not case sensitive.&nbsp; The answer is in lowercase.</p>

<p>&nbsp;</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> 
paragraph = &quot;Bob hit a ball, the hit BALL flew far after it was hit.&quot;
banned = [&quot;hit&quot;]
<strong>Output:</strong> &quot;ball&quot;
<strong>Explanation:</strong> 
&quot;hit&quot; occurs 3 times, but it is a banned word.
&quot;ball&quot; occurs twice (and no other word does), so it is the most frequent non-banned word in the paragraph. 
Note that words in the paragraph are not case sensitive,
that punctuation is ignored (even if adjacent to words, such as &quot;ball,&quot;), 
and that &quot;hit&quot; isn&#39;t the answer even though it occurs more because it is banned.
</pre>

<p>&nbsp;</p>

<p><strong>Note: </strong></p>

<ul>
	<li><code>1 &lt;= paragraph.length &lt;= 1000</code>.</li>
	<li><code>0 &lt;= banned.length &lt;= 100</code>.</li>
	<li><code>1 &lt;= banned[i].length &lt;= 10</code>.</li>
	<li>The answer is unique, and written in lowercase (even if its occurrences in <code>paragraph</code>&nbsp;may have&nbsp;uppercase symbols, and even if it is a proper noun.)</li>
	<li><code>paragraph</code> only consists of letters, spaces, or the punctuation symbols <code>!?&#39;,;.</code></li>
	<li>There are no hyphens or hyphenated words.</li>
	<li>Words only consist of letters, never apostrophes or other punctuation symbols.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Amazon - 76 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Overview

This problem is a good exercise to brush up one's skills on string manipulation.

The String data type is almost omnipresent in all programming languages.
However, each language has its own implementation of String type, as well as various APIs for string manipulation.
For instance, String is _mutable_ in C++, while immutable in Python and Java.

The problem is not difficult. But due to the diversity of String type and string manipulation APIs, one could come up many different solutions.

Here we give two general approaches in the following sections.

- In one approach, we will construct a pipeline to process strings in several stages, where naturally each string would be traversed for several times.

- In another approach, we will traverse the input string _once and only once_, on the character base and do the processing _**on-the-fly**_.
<br/>
<br/>

---
#### Approach 1: String Processing in Pipeline

**Intuition**

We can solve the problem by breaking it into a series of _sequential tasks_.
Each task functions like a stage in a pipeline, which takes the input from the previous stage and then channels its output to the next stage.

More specifically, for this problem, we could break it down into the following stages:

![string processing pipeline](../Figures/819/819_pipeline_.png)

1. We replace all the punctuations with spaces and at the same time convert each letter to its lowercase. One could also accomplish this in two stages. Here we merge them together in one stage.

2. We split the output in the above step into words, with the separator of spaces.

3. We then iterate through the words to count the appearance of each unique word, excluding the words from the banned list.

4. With the hashmap of `{word->count}`, we then walk through all the items to find the word with the highest frequency.

**Algorithm**

Following the stages we explained before, here are some sample implementations.

<iframe src="https://leetcode.com/playground/jT9MHP5u/shared" frameBorder="0" width="100%" height="480" name="jT9MHP5u"></iframe>


**Complexity Analysis**

Let $$N$$ be the number of characters in the input string and $$M$$ be the number of characters in the banned list.

- Time Complexity: $$\mathcal{O}(N + M)$$.

    - It would take $$\mathcal{O}(N)$$ time to process each stage of the pipeline as we built.

    - In addition, we built a set out of the list of banned words, which would take $$\mathcal{O}(M)$$ time.

    - Hence, the overall time complexity of the algorithm is $$\mathcal{O}(N + M)$$.

- Space Complexity: $$\mathcal{O}(N + M)$$.

    - We built a hashmap to count the frequency of each unique word, whose space would be of $$\mathcal{O}(N)$$.

    - Similarly, we built a set out of the banned word list, which would consume additional $$\mathcal{O}(M)$$ space.

    - Therefore, the overall space complexity of the algorithm is $$\mathcal{O}(N + M)$$.
<br/>
<br/>

---
#### Approach 2: Character Processing in One-Pass

**Intuition**

With the approach of String manipulation pipeline, it is clear and easy to debug, since we could locate and inspect each stage if anything goes wrong.

However, one might argue that it is probably not the most efficient way to solve the problem, since we scan the input string multiple times.

Indeed, it is possible to process the input string once and only once to accomplish the tasks.

>We could iterate through the string character by character, and do the processing _**on-the-fly**_, rather than delaying the processing to the latter stages of the pipeline.

The idea is that we consume the input string on the character base.
At the moment we reach the end of one word, we can then start to perform the word-based logics such as checking if the word is in the banned list, updating the frequency of the word and also updating the most frequent word we've seen so far _etc._

**Algorithm**

We could implement the algorithm in one single loop, over the characters of the input string.

- At each iteration, the character is either of letter (maybe digit), or punctuation or space in other cases.

![character pointers](../Figures/819/819_character_pointers_.png)

- Further more, we could divide it into the following two cases:

    - **Case (1):** we are in the middle of a word.

    - **Case (2):** we in in-between the words, _e.g._ punctuations between the words or at the end of the paragraph.

- We then can organize the logics into the above two cases.

    - In case (1), we simply append the character into the word buffer.

    - In case (2), we do the rest of the logics, as follows:

        - check if the word is enlisted in the banned list.

        - if not, update the frequency of the word.

        - update the most common word that we've seen so far.

<iframe src="https://leetcode.com/playground/Zb62a34Y/shared" frameBorder="0" width="100%" height="500" name="Zb62a34Y"></iframe>


**Complexity Analysis**

Let $$N$$ be the number of characters in the input string and $$M$$ be the number of characters in the banned list.

- Time Complexity: $$\mathcal{O}(N + M)$$.

    - We traverse each character in the input string once and only once. At each iteration, it takes constant time to perform the operations, except the operation that we build a new string out of the buffer. Excluding the cost of string-building out of the iteration, we can consider the cost of iterations as $$\mathcal{O}(N)$$.

    - If we combine all the string-building operations all together, in total it would take another $$\mathcal{O}(N)$$ time.

    - In addition, we built a set out of the list of banned words, which would take $$\mathcal{O}(M)$$ time.

    - Hence, the overall time complexity of the algorithm is $$\mathcal{O}(N) + \mathcal{O}(N) + \mathcal{O}(M) = \mathcal{O}(N + M)$$.

- Space Complexity: $$\mathcal{O}(N + M)$$.

    - We built a hashmap to count the frequency of each unique word, whose space would be of $$\mathcal{O}(N)$$.

    - Similarly, we built a set out of the banned word list, which would consume additional $$\mathcal{O}(M)$$ space.

    - Therefore, the overall space complexity of the algorithm is $$\mathcal{O}(N + M)$$.

---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Easy Solution with Explanation
- Author: lee215
- Creation Date: Sun Apr 15 2018 11:12:50 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 22:41:26 GMT+0800 (Singapore Standard Time)

<p>
The description doesn\'t clearify the case like "word,word,word".
And it doesn\'t have this kind of corner cases at first.
The test cases and OJ may not be perfect on this problem.

Anyway I didn\'t bother thinking this issue.
In my opinion, the problem demands to extract words with only letters.
All I did is just extract words with only letters.

4 steps:
1. remove all punctuations
2. change to lowercase
3. words count for each word not in banned set
4. return the most common word

**C++:**
```
    string mostCommonWord(string p, vector<string>& banned) {
        unordered_set<string> ban(banned.begin(), banned.end());
        unordered_map<string, int> count;
        for (auto & c: p) c = isalpha(c) ? tolower(c) : \' \';
        istringstream iss(p);
        string w;
        pair<string, int> res ("", 0);
        while (iss >> w)
            if (ban.find(w) == ban.end() && ++count[w] > res.second)
                res = make_pair(w, count[w]);
        return res.first;
    }
```
**Java:**
```
    public String mostCommonWord(String p, String[] banned) {
        Set<String> ban = new HashSet<>(Arrays.asList(banned));
        Map<String, Integer> count = new HashMap<>();
        String[] words = p.replaceAll("\\W+" , " ").toLowerCase().split("\\s+");
        for (String w : words) if (!ban.contains(w)) count.put(w, count.getOrDefault(w, 0) + 1);
        return Collections.max(count.entrySet(), Map.Entry.comparingByValue()).getKey();
    }
```

**Python:**
Thanks to @sirxudi I change one line from
`words = re.sub(r\'[^a-zA-Z]\', \' \', p).lower().split()`
to 
`words = re.findall(r\'\w+\', p.lower())
`
```
    def mostCommonWord(self, p, banned):
        ban = set(banned)
        words = re.findall(r\'\w+\', p.lower())
        return collections.Counter(w for w in words if w not in ban).most_common(1)[0][0]
```
</p>


### last test case is weird
- Author: GaryPKU
- Creation Date: Wed Sep 12 2018 11:22:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 03:20:53 GMT+0800 (Singapore Standard Time)

<p>
Input:
"a, a, a, a, b,b,b,c, c"
["a"]
Output:
"b"
Expected:
"b,b,b,c"

That\'s not what the original description says\u3002
</p>


### Simple Java Solution
- Author: saobiaozi
- Creation Date: Mon Apr 16 2018 02:20:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 21 2018 02:25:03 GMT+0800 (Singapore Standard Time)

<p>
```
    public String mostCommonWord(String paragraph, String[] banned) {
        String[] words = paragraph.toLowerCase().split("[ !?\',;.]+");
        HashMap<String, Integer> map = new HashMap<>();
        for(String word : words) map.put(word, map.getOrDefault(word, 0) + 1);
        for(String word : banned) if(map.containsKey(word)) map.remove(word);
        String res = null;
        for(String word : map.keySet())
            if(res == null || map.get(word) > map.get(res))
                res = word;
        return res;
    }
```

</p>


