---
title: "Remove Outermost Parentheses"
weight: 987
#id: "remove-outermost-parentheses"
---
## Description
<div class="description">
<p>A valid parentheses string is either empty <code>(&quot;&quot;)</code>, <code>&quot;(&quot; + A + &quot;)&quot;</code>, or <code>A + B</code>, where <code>A</code> and <code>B</code> are valid parentheses strings, and <code>+</code> represents string concatenation.&nbsp; For example, <code>&quot;&quot;</code>, <code>&quot;()&quot;</code>, <code>&quot;(())()&quot;</code>, and <code>&quot;(()(()))&quot;</code> are all valid parentheses strings.</p>

<p>A valid parentheses string <code>S</code> is <strong>primitive</strong> if it is nonempty, and there does not exist a way to split it into <code>S = A+B</code>, with <code>A</code> and <code>B</code> nonempty valid parentheses strings.</p>

<p>Given a valid parentheses string <code>S</code>, consider its primitive decomposition: <code>S = P_1 + P_2 + ... + P_k</code>, where <code>P_i</code> are primitive valid parentheses strings.</p>

<p>Return <code>S</code> after removing the outermost parentheses of every primitive string in the primitive decomposition of <code>S</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;(()())(())&quot;</span>
<strong>Output: </strong><span id="example-output-1">&quot;()()()&quot;</span>
<strong>Explanation: </strong>
The input string is &quot;(()())(())&quot;, with primitive decomposition &quot;(()())&quot; + &quot;(())&quot;.
After removing outer parentheses of each part, this is &quot;()()&quot; + &quot;()&quot; = &quot;()()()&quot;.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;(()())(())(()(()))&quot;</span>
<strong>Output: </strong><span id="example-output-2">&quot;()()()()(())&quot;</span>
<strong>Explanation: </strong>
The input string is &quot;(()())(())(()(()))&quot;, with primitive decomposition &quot;(()())&quot; + &quot;(())&quot; + &quot;(()(()))&quot;.
After removing outer parentheses of each part, this is &quot;()()&quot; + &quot;()&quot; + &quot;()(())&quot; = &quot;()()()()(())&quot;.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">&quot;()()&quot;</span>
<strong>Output: </strong><span id="example-output-3">&quot;&quot;</span>
<strong>Explanation: </strong>
The input string is &quot;()()&quot;, with primitive decomposition &quot;()&quot; + &quot;()&quot;.
After removing outer parentheses of each part, this is &quot;&quot; + &quot;&quot; = &quot;&quot;.
</pre>

<p>&nbsp;</p>
</div>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>S.length &lt;= 10000</code></li>
	<li><code>S[i]</code> is <code>&quot;(&quot;</code> or <code>&quot;)&quot;</code></li>
	<li><code>S</code> is a valid parentheses string</li>
</ol>

<div>
<div>
<div>&nbsp;</div>
</div>
</div>
</div>

## Tags
- Stack (stack)

## Companies
- Google - 2 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Count Opened Parenthesis
- Author: lee215
- Creation Date: Sun Apr 07 2019 12:11:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 07 2019 12:11:05 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Quote from @shubhama,
Primitive string will have equal number of opened and closed parenthesis.

## **Explanation**:
`opened` count the number of opened parenthesis.
Add every char to the result,
unless the first left parenthesis,
and the last right parenthesis.

## **Time Complexity**:
`O(N)` Time, `O(N)` space

<br>

**Java:**
```
    public String removeOuterParentheses(String S) {
        StringBuilder s = new StringBuilder();
        int opened = 0;
        for (char c : S.toCharArray()) {
            if (c == \'(\' && opened++ > 0) s.append(c);
            if (c == \')\' && opened-- > 1) s.append(c);
        }
        return s.toString();
    }
```

**C++:**
```
    string removeOuterParentheses(string S) {
        string res;
        int opened = 0;
        for (char c : S) {
            if (c == \'(\' && opened++ > 0) res += c;
            if (c == \')\' && opened-- > 1) res += c;
        }
        return res;
    }
```

**Python:**
```
    def removeOuterParentheses(self, S):
        res, opened = [], 0
        for c in S:
            if c == \'(\' and opened > 0: res.append(c)
            if c == \')\' and opened > 1: res.append(c)
            opened += 1 if c == \'(\' else -1
        return "".join(res)
```

</p>


### My Java 3ms Straight Forward Solution | Beats 100%
- Author: debdattakunda
- Creation Date: Mon Apr 08 2019 03:53:33 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 08 2019 03:53:33 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public String removeOuterParentheses(String S) {
        
        StringBuilder sb = new StringBuilder();
        int open=0, close=0, start=0;
        for(int i=0; i<S.length(); i++) {
            if(S.charAt(i) == \'(\') {
                open++;
            } else if(S.charAt(i) == \')\') {
                close++;
            }
            if(open==close) {
                sb.append(S.substring(start+1, i));
                start=i+1;
            }
        }
        return sb.toString();
    }
}
```
</p>


### C++ 0ms solution
- Author: Dan0907
- Creation Date: Sat May 11 2019 01:29:30 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 29 2019 21:14:01 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    string removeOuterParentheses(string S) {
        int count = 0;
        std::string str;
        for (char c : S) {
            if (c == \'(\') {
                if (count++) {
                    str += \'(\';
                }
            } else {
                if (--count) {
                    str += \')\';
                }
            }
        }
        return str;
    }
};
```
</p>


