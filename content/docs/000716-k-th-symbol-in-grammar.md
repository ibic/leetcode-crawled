---
title: "K-th Symbol in Grammar"
weight: 716
#id: "k-th-symbol-in-grammar"
---
## Description
<div class="description">
<p>On the first row, we write a <code>0</code>. Now in every subsequent row, we look at the previous row and replace each occurrence of <code>0</code> with <code>01</code>, and each occurrence of <code>1</code> with <code>10</code>.</p>

<p>Given row <code>N</code> and index <code>K</code>, return the <code>K</code>-th indexed symbol in row <code>N</code>. (The values of <code>K</code> are 1-indexed.) (1 indexed).</p>

<pre>
<strong>Examples:</strong>
<strong>Input:</strong> N = 1, K = 1
<strong>Output:</strong> 0

<strong>Input:</strong> N = 2, K = 1
<strong>Output:</strong> 0

<strong>Input:</strong> N = 2, K = 2
<strong>Output:</strong> 1

<strong>Input:</strong> N = 4, K = 5
<strong>Output:</strong> 1

<strong>Explanation:</strong>
row 1: 0
row 2: 01
row 3: 0110
row 4: 01101001
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li><code>N</code> will be an integer in the range <code>[1, 30]</code>.</li>
	<li><code>K</code> will be an integer in the range <code>[1, 2^(N-1)]</code>.</li>
</ol>

</div>

## Tags
- Recursion (recursion)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Brute Force

**Intuition and Algorithm**

We'll make each row exactly as directed in the problem statement.  We only need to remember the last row.

Unfortunately, the strings could have length around 1 billion, as they double on each row, so this approach is not efficient enough.

<iframe src="https://leetcode.com/playground/58ghcQJ8/shared" frameBorder="0" width="100%" height="259" name="58ghcQJ8"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(2^N)$$.  We parse rows with lengths $$2^0 + 2^1 + \cdots + 2^{N-1}$$.

* Space Complexity:  $$O(2^N)$$, the length of the `lastrow`.
<br />
<br />


---
#### Approach 2: Recursion (Parent Variant)

**Intuition and Algorithm**

Since each row is made only using information from the previous row, let's try to write the answer in terms of bits from the previous row.

<br />
<center>
    <img src="../Figures/779/parent.png" alt="Diagram of digits with relationship to their parent" width="350"/>
</center>
<br />

In particular, if we write say `"0110"` which generates `"01101001"`, then the first `"0"` generates the first `"01"` in the next row; the next digit `"1"` generates the next `"10"`, the next `"1"` generates the next `"10"`, and the last `"0"` generates the last `"01"`.

<br />
<center>
    <img src="../Figures/779/link.png" alt="Diagram of digits through repeated function calls" width="350"/>
</center>
<br />

In general, the `K`th digit's parent is going to be `(K+1) / 2`.  If the parent is `0`, then the digit will be the same as `1 - (K%2)`.  If the parent is `1`, the digit will be the opposite, ie. `K%2`.

<iframe src="https://leetcode.com/playground/PMHaEnuq/shared" frameBorder="0" width="100%" height="157" name="PMHaEnuq"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$.  It takes $$N-1$$ steps to find the answer.

* Space Complexity:  $$O(1)$$.
<br />
<br />


---
#### Approach 3: Recursion (Flip Variant)

**Intuition and Algorithm**

As in *Approach #2*, we could try to write the bit in terms of it's previous bit.

When writing a few rows of the sequence, we notice a pattern: the second half is always the first half "flipped": namely, that `'0'` becomes `'1'` and `'1'` becomes `'0'`.

We can prove this assertion by induction.  The key idea is if a string $$X$$ generates $$Y$$, then a flipped string $$X'$$ generates $$Y'$$.

This leads to the following algorithm idea: if `K` is in the second half, then we could put `K -= (1 << N-2)` so that it is in the first half, and flip the final answer.

<iframe src="https://leetcode.com/playground/75hNYqmP/shared" frameBorder="0" width="100%" height="191" name="75hNYqmP"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$.  It takes $$N-1$$ steps to find the answer.

* Space Complexity:  $$O(1)$$.
<br />
<br />


---
#### Approach 4: Binary Count

**Intuition and Algorithm**

As in *Approach #3*, the second half of every row is the first half flipped.

When the indexes `K` are written in binary (now indexing from zero), indexes of the second half of a row are ones with the first bit set to 1.

This means when applying the algorithm in *Approach #3* virtually, the number of times we will flip the final answer is just the number of `1`s in the binary representation of `K-1`.

<iframe src="https://leetcode.com/playground/hdbGHCg7/shared" frameBorder="0" width="100%" height="140" name="hdbGHCg7"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\log N)$$, the number of binary bits in `N`.  If $$\log N$$ is taken to be bounded, this can be considered to be $$O(1)$$.

* Space Complexity:  $$O(1)$$.  (In Python, `bin(X)` creates a string of length $$O(\log X)$$, which could be avoided.)
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My 3 lines C++ recursive solution
- Author: grandyang
- Creation Date: Sun Feb 04 2018 12:00:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 20:27:12 GMT+0800 (Singapore Standard Time)

<p>
The whole structure can be viewed a binary tree, when a node is 0, their two children nodes are 0 and 1, similarly, when a node is 1, two children nodes are 1 and 0. We can know whether the position of K is  a left node or a right node by dividing 2. If K is even, current node is right child, and its parent is the (K/2)th node in previous row; else if K is odd, current node is left child and its parent is the ((K+1)/2)th node in previous row. 
The value of current node depends on its parent node, without knowing its parent node value, we still cannot determine current node value. That's why we need recursion, we keep going previous row to find the parent node until reach the first row. Then all the parent node value will be determined after the recursion function returns. 

```
class Solution {
public:
    int kthGrammar(int N, int K) {
	if (N == 1) return 0;
	if (K % 2 == 0) return (kthGrammar(N - 1, K / 2) == 0) ? 1 : 0;
	else return (kthGrammar(N - 1, (K + 1) / 2) == 0) ? 0 : 1;
    }
};
</p>


### [JAVA] one line
- Author: tyuan73
- Creation Date: Sun Feb 04 2018 12:27:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 08 2018 03:07:53 GMT+0800 (Singapore Standard Time)

<p>
    public int kthGrammar(int N, int K) {
        return Integer.bitCount(K-1) & 1;
    }

***Updates***: `(all index discussed below are 0-based)`
***Observation 1***: **N** does not matter as long as "K will be an integer in the range [1, 2^(N-1)]". We can ignore **N**.

***Observation 2***: let `f(k)` be the value of `k`th position (0-based), then:
``    f(2 * k) = 0 {if f(k) = 0} or, 1 {if f(k) = 1}`` => ``f(2 * k) = f(k) xor 0``
    ``f(2 * k + 1) = 0 {if f(k) = 1} or 1 {if f(k) = 0}`` => ``f(2 * k + 1) = f(k) xor 1``

***Obervation 3***: if binary string of **k** is used, let **k = 1001010**, then we have:
`f(1001010) = f(100101) ^ 0 = f(10010) ^ 1 ^ 0 = f(1001) ^ 0 ^ 1 ^ 0 = ... = f(0) ^ 1 ^ 0 ^ 0 ^1 ^ 0 ^ 1 ^ 0 = 1 ^ 0 ^ 0 ^1 ^ 0 ^ 1 ^ 0`
So, the result is the **xor** operation on all bits of **k**. Since 0 does not change **xor** result, we can ignore all 0s.
`f(1001010) = 1 ^ 1 ^ 1 = (1^1) ^ 1 = 0 ^ 1 = 1`
`f(11110011) = 1 ^ 1^ 1 ^ 1 ^ 1 ^1 = (1 ^ 1) ^ (1 ^ 1) ^ (1 ^1) = 0`
Now, it's easy to tell `f(k) = 0` if **k** has **even** number of 1s in binary representation, and `f(k) = 1` when **k** has **odd** number of 1s
</p>


### [Python/Java/C++] Easy 1-line Solution with detailed explanation
- Author: lee215
- Creation Date: Thu Feb 08 2018 15:06:16 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 14 2020 21:43:58 GMT+0800 (Singapore Standard Time)

<p>
# Explanation

**First**, you can easily find that prefix of every row is exactly the same.
It not difficult to understand.
Beacause every `2 * L` fisrt letters are generated by the same `L` fist letters.

**Then**, we know that every row is the start part of the same long sequence.
Moreover K is guaranteed to be an integer in the `range[1, 2 ^ (N - 1)]`.
So result depends only on value `K`

Suppose `2^L < K <= 2^(L + 1)`.
`2^L` is the biggest `2`\'s power smaller than `K`.
`Kth` number is generated from `K - 2^L`
`Kth` number is also different from `K - 2^L`
So we toggle `K` to `K - 2^L` by subtracting `2^L`

We repeat the process until we toggle `K` to `1`,
which mean we need to substract `K - 1` in total.

What we should do is transfer `K - 1` in binary,
so that we can easily find how many times we need to toggle.
<br>

# Example
**For example `K = 8`**
we need to substract `K - 1 = 7 = 111` in binary.
So we need to substract `100`, `10` and `1`, which are `4, 2, 1` in decimal.
We toggle `K` from `8` to `4`, `2` and finally `1`.

**For example `K = 9`**
we need to substract `K - 1 = 8 = 1000` in binary.
So we need to substract `1000`, which is `8` in decimal.
We toggle `K` from `9` to `1` directly.
<br>

# Conclusion
we turn this problem to just count 1 bits.
We can observe that the answer depend on whether the number of 1 bits in binary `K - 1` is odd or even.

Now you may have many ways to solve this problem.
In Python, `bin` transform it into binary directly.
In Java, build-in method `Integer.bitCount` is ready for use.
In C++, I use the trick `K & (K - 1)` to drops the lowest bit.
<br>

# Complexity
Time `O(logN)`
Space `O(1)`
<br>

# More
Here is another idea
https://leetcode.com/problems/k-th-symbol-in-grammar/discuss/121544/C++JavaPython-Another-Solution
<br>

**Java**
```java
    public int kthGrammar(int N, int K) {
        return Integer.bitCount(K - 1) & 1;
    }
```

**C++**
```cpp
    int kthGrammar(int N, int K) {
        int n;
        for (n = 0, K -= 1; K ; K &= (K - 1)) n++;
        return n & 1;
    }
```

**Python**
`O(logN)`space using `bin`
```py
    def kthGrammar(self, N, K):
        return bin(K - 1).count(\'1\') & 1
```
</p>


