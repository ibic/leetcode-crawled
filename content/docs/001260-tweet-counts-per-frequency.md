---
title: "Tweet Counts Per Frequency"
weight: 1260
#id: "tweet-counts-per-frequency"
---
## Description
<div class="description">
<p>Implement the class <code>TweetCounts</code> that supports two methods:</p>

<p>1.<code> recordTweet(string tweetName, int time)</code></p>

<ul>
	<li>Stores the <code>tweetName</code> at the recorded <code>time</code> (in <strong>seconds</strong>).</li>
</ul>

<p>2.<code> getTweetCountsPerFrequency(string freq, string tweetName, int startTime, int endTime)</code></p>

<ul>
	<li>Returns the total number of occurrences for the given <code>tweetName</code> per <strong>minute</strong>, <strong>hour</strong>, or <strong>day</strong> (depending on <code>freq</code>) starting from the <code>startTime</code> (in <strong>seconds</strong>) and ending at the <code>endTime</code> (in <strong>seconds</strong>).</li>
	<li><code>freq</code> is always <strong>minute</strong><em>, </em><strong>hour</strong><em>&nbsp;or <strong>day</strong></em>, representing the time interval to get the total number of occurrences for the given&nbsp;<code>tweetName</code>.</li>
	<li>The first time interval always starts from the <code>startTime</code>, so the time intervals are <code>[startTime, startTime + delta*1&gt;, &nbsp;[startTime + delta*1, startTime + delta*2&gt;, [startTime + delta*2, startTime + delta*3&gt;, ... , [startTime + delta*i, <strong>min</strong>(startTime + delta*(i+1), endTime + 1)&gt;</code> for some non-negative number <code>i</code> and <code>delta</code> (which depends on <code>freq</code>).&nbsp;&nbsp;</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example:</strong></p>

<pre>
<strong>Input</strong>
[&quot;TweetCounts&quot;,&quot;recordTweet&quot;,&quot;recordTweet&quot;,&quot;recordTweet&quot;,&quot;getTweetCountsPerFrequency&quot;,&quot;getTweetCountsPerFrequency&quot;,&quot;recordTweet&quot;,&quot;getTweetCountsPerFrequency&quot;]
[[],[&quot;tweet3&quot;,0],[&quot;tweet3&quot;,60],[&quot;tweet3&quot;,10],[&quot;minute&quot;,&quot;tweet3&quot;,0,59],[&quot;minute&quot;,&quot;tweet3&quot;,0,60],[&quot;tweet3&quot;,120],[&quot;hour&quot;,&quot;tweet3&quot;,0,210]]

<strong>Output</strong>
[null,null,null,null,[2],[2,1],null,[4]]

<strong>Explanation</strong>
TweetCounts tweetCounts = new TweetCounts();
tweetCounts.recordTweet(&quot;tweet3&quot;, 0);
tweetCounts.recordTweet(&quot;tweet3&quot;, 60);
tweetCounts.recordTweet(&quot;tweet3&quot;, 10);                             // All tweets correspond to &quot;tweet3&quot; with recorded times at 0, 10 and 60.
tweetCounts.getTweetCountsPerFrequency(&quot;minute&quot;, &quot;tweet3&quot;, 0, 59); // return [2]. The frequency is per minute (60 seconds), so there is one interval of time: 1) [0, 60&gt; - &gt; 2 tweets.
tweetCounts.getTweetCountsPerFrequency(&quot;minute&quot;, &quot;tweet3&quot;, 0, 60); // return [2, 1]. The frequency is per minute (60 seconds), so there are two intervals of time: 1) [0, 60&gt; - &gt; 2 tweets, and 2) [60,61&gt; - &gt; 1 tweet.
tweetCounts.recordTweet(&quot;tweet3&quot;, 120);                            // All tweets correspond to &quot;tweet3&quot; with recorded times at 0, 10, 60 and 120.
tweetCounts.getTweetCountsPerFrequency(&quot;hour&quot;, &quot;tweet3&quot;, 0, 210);  // return [4]. The frequency is per hour (3600 seconds), so there is one interval of time: 1) [0, 211&gt; - &gt; 4 tweets.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>There will be at most <code>10000</code>&nbsp;operations considering both <code>recordTweet</code> and <code>getTweetCountsPerFrequency</code>.</li>
	<li><code>0 &lt;= time, startTime, endTime &lt;=&nbsp;10^9</code></li>
	<li><code>0 &lt;= endTime - startTime &lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Design (design)

## Companies
- Twitter - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Oh my god!! What is wrong with this problem ?
- Author: Hccct
- Creation Date: Sun Feb 09 2020 12:01:19 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 17 2020 00:22:31 GMT+0800 (Singapore Standard Time)

<p>
The bug maybe has been solved now. Thanks for your time to read this article.
In Feb / 08/ 2020
It is a weekly contest medium question, but it has some bugs when users submit.
So, You guys will find that the accepted rated is very low and this problem has many dislike.

</p>


### Getting correct answer in RUN TEST locally, but wrong answer when I submitted
- Author: zhun
- Creation Date: Sun Feb 09 2020 12:03:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 09 2020 12:03:08 GMT+0800 (Singapore Standard Time)

<p>
Can someone show me what\'s wrong with the code?
```
class TweetCounts {
    HashMap<String, TreeSet<Integer>> map;
    public TweetCounts() {
        map = new HashMap<>();
    }
    
    public void recordTweet(String tweetName, int time) {
        if (map.containsKey(tweetName))
        {
            TreeSet<Integer> set = map.get(tweetName);
            set.add(time);
            map.put(tweetName, set);
        }
        else
        {
            TreeSet<Integer> set = new TreeSet<>();
            set.add(time);
            map.put(tweetName, set);
        }
        
    }
    
    public List<Integer> getTweetCountsPerFrequency(String freq, String tweetName, long startTime, long endTime) {
        
        if (freq.equals("minute"))
        {
            return solve(60, tweetName, startTime, endTime);
        }
        else if (freq.equals("hour"))
        {
            return solve(3600, tweetName, startTime, endTime);
        }
        else if (freq.equals("day"))
        {
            return solve(86400, tweetName, startTime, endTime);
        }
        else
        {
            return new ArrayList<>();
        }
    }
    
    private List<Integer> solve(long interval, String tweetName, long startTime, long endTime)
    {
        List<Integer> res = new ArrayList<>();
        if (!map.containsKey(tweetName))
        {
            for(long i = startTime; i <= endTime; i += interval)
            {
                res.add(0);
            }
            return res;
        }
        
        TreeSet<Integer> set = map.get(tweetName);
        for(long i = startTime; i <= endTime; i += interval)
        {
            res.add(set.subSet((int) i, (int) Math.min(i + interval, endTime + 1)).size());
        }
        return res;
    }
}
```
</p>


### [Java] TreeMap Accepted Solution - Easy Understand
- Author: frankkkkk
- Creation Date: Sun Feb 09 2020 12:02:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 15 2020 11:54:01 GMT+0800 (Singapore Standard Time)

<p>

```
class TweetCounts {

    private Map<String, TreeMap<Integer, Integer>> map;
    
    public TweetCounts() {
        map = new HashMap<>();
    }
    
    public void recordTweet(String tweetName, int time) {
        
        if (!map.containsKey(tweetName)) map.put(tweetName, new TreeMap<>());
        TreeMap<Integer, Integer> tweetMap = map.get(tweetName);
        tweetMap.put(time, tweetMap.getOrDefault(time, 0) + 1);
    }
    
    public List<Integer> getTweetCountsPerFrequency(String freq, String tweetName, int startTime, int endTime) {
	
	    if (!map.containsKey(tweetName)) return null;
        List<Integer> res = new LinkedList<>();
        
        int interval = 0, size = 0;
        if (freq.equals("minute")) {
            interval = 60;
        } else if (freq.equals("hour")) {
            interval = 3600;
        } else {
            interval = 86400;
        }
        size = ((endTime - startTime) / interval) + 1;
        
        int[] buckets = new int[size];
        
        TreeMap<Integer, Integer> tweetMap = map.get(tweetName);
        
        for (Map.Entry<Integer, Integer> entry : tweetMap.subMap(startTime, endTime + 1).entrySet()) {
            
            int index = (entry.getKey() - startTime) / interval;
            buckets[index] += entry.getValue();
        }
        
        for (int num : buckets) res.add(num);
        
        return res;
    }
}
```
</p>


