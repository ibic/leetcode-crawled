---
title: "Island Perimeter"
weight: 440
#id: "island-perimeter"
---
## Description
<div class="description">
<p>You are given <code>row x col</code> <code>grid</code> representing a map where <code>grid[i][j] = 1</code> represents&nbsp;land and <code>grid[i][j] = 0</code> represents water.</p>

<p>Grid cells are connected <strong>horizontally/vertically</strong> (not diagonally). The <code>grid</code> is completely surrounded by water, and there is exactly one island (i.e., one or more connected land cells).</p>

<p>The island doesn&#39;t have &quot;lakes&quot;, meaning the water inside isn&#39;t connected to the water around the island. One cell is a square with side length 1. The grid is rectangular, width and height don&#39;t exceed 100. Determine the perimeter of the island.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img src="https://assets.leetcode.com/uploads/2018/10/12/island.png" style="width: 221px; height: 213px;" />
<pre>
<strong>Input:</strong> grid = [[0,1,0,0],[1,1,1,0],[0,1,0,0],[1,1,0,0]]
<strong>Output:</strong> 16
<strong>Explanation:</strong> The perimeter is the 16 yellow stripes in the image above.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1]]
<strong>Output:</strong> 4
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,0]]
<strong>Output:</strong> 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>row == grid.length</code></li>
	<li><code>col == grid[i].length</code></li>
	<li><code>1 &lt;= row, col &lt;= 100</code></li>
	<li><code>grid[i][j]</code> is <code>0</code> or <code>1</code>.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Facebook - 10 (taggedByAdmin: false)
- Bloomberg - 6 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Amazon - 5 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Simple Counting

**Intuition**

Go through every cell on the grid and whenever you are at cell
 `1` (land cell), look for surrounding (`UP`, `RIGHT`, `DOWN`, `LEFT`) cells. A land cell without any surrounding land cell will have a perimeter of `4`. Subtract `1` for each 
 surrounding land cell. 
 
 When you are at cell `0` (water cell), you don't need to do anything. Just proceed to another cell.

!?!../Documents/463_Island_Perimeter_1.json:1200,600!?!

**Implementation**

<iframe src="https://leetcode.com/playground/ihQ6wHqP/shared" frameBorder="0" width="100%" height="500" name="ihQ6wHqP"></iframe>

**Complexity Analysis**

* Time complexity : $$O(mn)$$ where $$m$$ is the number of rows of the grid and $$n$$ is 
the number of columns of the grid. Since two `for` loops  go through all 
the cells on the grid, for a two-dimensional grid of size $$m\times n$$, the algorithm 
would have to check $$mn$$ cells. 

* Space complexity : $$O(1)$$. Only the `result` variable is updated and there is 
no other space requirement.

<br />

---

#### Approach 2: Better Counting

*Approach 2 has the same time and space complexity as Approach 1. Even though they 
have the same time and space complexities, Approach 2 is slightly more efficient 
than the Approach 1. Rather than checking 4 surrounding neighbors, we only need to check two neighbors (`LEFT` and `UP`) in Approach 2.*

**Intuition**

Since we are traversing the grid from left to right, and from top to bottom, 
for each land cell we are currently at, we only need to check whether the 
`LEFT` and `UP` cells are land cells with a slight modification on previous 
approach.

* As you go through each cell on the grid, treat all the land cells as having a perimeter of `4` and add that up to the accumulated `result`. 
* If that land cell has a neighboring land cell, remove `2` sides (one from each land cell) which will be touching between these two cells.
    * If your current land cell has a `UP` land cell, subtract `2` from your accumulated `result`. 
    * If your current land cell has a `LEFT` land cell,
subtract `2` from your accumulated `result`.

!?!../Documents/463_Island_Perimeter_2.json:1200,600!?!

**Implementation**

<iframe src="https://leetcode.com/playground/PW6pYKfT/shared" frameBorder="0" width="100%" height="480" name="PW6pYKfT"></iframe>

**Complexity Analysis**

* Time complexity : $$O(mn)$$ where $$m$$ is the number of rows of the grid and $$n$$ is 
the number of columns of the grid. Since two `for` loops  go through all 
the cells on the grid, for a two-dimensional grid of size $$m\times n$$, the algorithm 
would have to check $$mn$$ cells. 

* Space complexity : $$O(1)$$. Only the `result` variable is updated and there is 
no other space requirement.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### clear and easy java solution
- Author: holyghost
- Creation Date: Sun Nov 20 2016 18:05:38 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 17:08:57 GMT+0800 (Singapore Standard Time)

<p>
1. loop over the matrix and count the number of islands;
2. if the current dot is an island, count if it has any right neighbour or down neighbour;
3. the result is islands * 4 - neighbours * 2

```
public class Solution {
    public int islandPerimeter(int[][] grid) {
        int islands = 0, neighbours = 0;

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 1) {
                    islands++; // count islands
                    if (i < grid.length - 1 && grid[i + 1][j] == 1) neighbours++; // count down neighbours
                    if (j < grid[i].length - 1 && grid[i][j + 1] == 1) neighbours++; // count right neighbours
                }
            }
        }

        return islands * 4 - neighbours * 2;
    }
}
```
</p>


### Java 9 line solution, add 4 for each land and remove 2 for each internal edge
- Author: dreamchase
- Creation Date: Tue Nov 22 2016 03:01:34 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 09:24:28 GMT+0800 (Singapore Standard Time)

<p>
```
public static int islandPerimeter(int[][] grid) {
        if (grid == null || grid.length == 0 || grid[0].length == 0) return 0;
        int result = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 1) {
                    result += 4;
                    if (i > 0 && grid[i-1][j] == 1) result -= 2;
                    if (j > 0 && grid[i][j-1] == 1) result -= 2;
                }
            }
        }
        return result;
    }
```
</p>


### Short Python
- Author: StefanPochmann
- Creation Date: Sun Nov 20 2016 16:49:23 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 09:09:41 GMT+0800 (Singapore Standard Time)

<p>
Since there are no lakes, every pair of neighbour cells with different values is part of the perimeter (more precisely, the edge between them is). So just count the differing pairs, both horizontally and vertically (for the latter I simply transpose the grid).

    def islandPerimeter(self, grid):
        return sum(sum(map(operator.ne, [0] + row, row + [0]))
                   for row in grid + map(list, zip(*grid)))
</p>


