---
title: "Path Crossing"
weight: 1371
#id: "path-crossing"
---
## Description
<div class="description">
<p>Given a string&nbsp;<code>path</code>, where <code>path[i] =&nbsp;&#39;N&#39;</code>, <code>&#39;S&#39;</code>, <code>&#39;E&#39;</code>&nbsp;or&nbsp;<code>&#39;W&#39;</code>, each representing moving one unit north, south, east, or west, respectively. You start at the origin <code>(0, 0)</code> on a 2D plane and walk on the path specified by <code>path</code>.</p>

<p>Return <code>True</code> if the path crosses itself at any point, that is, if at any time you are on a location you&#39;ve previously visited. Return <code>False</code> otherwise.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/06/10/screen-shot-2020-06-10-at-123929-pm.png" style="width: 250px; height: 224px;" /></p>

<pre>
<strong>Input:</strong> path = &quot;NES&quot;
<strong>Output:</strong> false 
<strong>Explanation:</strong> Notice that the path doesn&#39;t cross any point more than once.
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/06/10/screen-shot-2020-06-10-at-123843-pm.png" style="width: 250px; height: 212px;" /></p>

<pre>
<strong>Input:</strong> path = &quot;NESWW&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong> Notice that the path visits the origin twice.</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= path.length &lt;= 10^4</code></li>
	<li><code>path</code> will only consist of characters in&nbsp;<code>{&#39;N&#39;, &#39;S&#39;, &#39;E&#39;, &#39;W}</code></li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Detailed Steps using Set | Java Python
- Author: rajmc
- Creation Date: Sun Jun 28 2020 14:30:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 29 2020 02:32:37 GMT+0800 (Singapore Standard Time)

<p>
This is very easy problem - Just take x, y cordinates for cartesian plane and a Set to store all the codinates.
`N`= y++
`S` = y--
`E` = x++
`W` = x--

check if the same x, y coordinate exit in set  return true.
in the end return false

**Video Explanation** https://www.youtube.com/watch?v=IZHyrENiOCg

**Java**

```
class Solution {
    public boolean isPathCrossing(String path) {
        int x = 0, y = 0;
        Set<String> set = new HashSet();
        set.add(x + "$" + y);
        for (char c : path.toCharArray()) {
            if (c == \'N\') y++;
            else if (c == \'S\') y--;
            else if (c == \'E\') x++;
            else x--;
            String key = x + "$" + y;
            if (set.contains(key))
                return true;
            set.add(key);
        }
        return false;
    }
}
```

****
**Python**

```
class Solution:
    def isPathCrossing(self, path):
        x = y = 0
        set = {(0, 0)}
        
        for c in path:
            if c == \'N\':
                y += 1
            elif c == \'S\':
                y -= 1
            elif c == \'E\':
                x += 1
            else:
                x -= 1
            
            if (x, y) in set:
                return True
            set.add((x, y))
        return False
```

`TC - O(n)` where n is the length of Path
</p>


### C++| Simple solution with set
- Author: ashwinfury1
- Creation Date: Sun Jun 28 2020 13:05:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 13:05:29 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    bool isPathCrossing(string path) {
        int x = 0,y=0;
        set<pair<int,int>>s;
        s.insert({0,0});
        for(char p: path){
            if(p == \'N\') y++;
            else if(p == \'S\')y--;
            else if(p == \'E\') x++;
            else x--;
            
            if(s.find({x,y}) != s.end()) return true;
            else s.insert({x,y});
        }
        return false;
    }
};
```
</p>


### [Java/Python 3] Using Set to check duplicate w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Jun 28 2020 13:06:09 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 21 2020 01:50:33 GMT+0800 (Singapore Standard Time)

<p>
**Update:**

For the creation of the hash in the form of `x * m + y`, there are pre-conditions for `m`:
1. The `m` in the form of `x * m + y` must be greater than the range of `y`, that is, `m > yMax - yMin`;
2. There will NOT integer overflow during computation of the hash `x * m + y` for any point `(x, y)`.

Then the hash `x * m + y` and its corresponding point `(x, y)` are bijective, and there will NOT hash collision.

**End of Update**

----


Create a hash for each point (x, y), there are two ways: 

1. x * 20001 + y, where `20001` >  `yMax - yMin`, then use Set to check duplicate.

```java
    public boolean isPathCrossing(String path) {
        Set<Integer> seen = new HashSet<>();
        seen.add(0);
        for (int i = 0, x = 0, y = 0; i < path.length(); ++i) {
            char c = path.charAt(i);
            if (c == \'N\') {
                y += 1;
            }else if (c == \'S\') {
                y -= 1;
            }else if (c == \'E\') {
                x -= 1;
            }else {
                x += 1;
            }
            if (!seen.add(x * 20001 + y)) {
                return true;
            }
        }
        return false;
    }
```
2. use string `x + ", " + y`.

In case you are uncomfortable with create hash number, here is a string version of hash:
```java
    public boolean isPathCrossing(String path) {
        Set<String> seen = new HashSet<>();
        seen.add(0 + ", " + 0);
        for (int i = 0, x = 0, y = 0; i < path.length(); ++i) {
            char c = path.charAt(i);
            if (c == \'N\') {
                y += 1;
            }else if (c == \'S\') {
                y -= 1;
            }else if (c == \'E\') {
                x -= 1;
            }else {
                x += 1;
            }
            if (!seen.add(x + ", " + y)) {
                return true;
            }
        }
        return false;
    }
```
```python
    def isPathCrossing(self, path: str) -> bool:
        x = y = 0
        seen = {(0, 0)}
        for char in path:
            if char == \'N\':
                y += 1
            elif char == \'S\':
                y -= 1
            elif char == \'E\':
                x += 1
            else:
                x -= 1
            if (x, y) in seen:
                return True    
            seen.add((x, y))
        return False        
```
**Concise Python 3 version:** - credit to **@_xavier_**.
```python
    def isPathCrossing(self, path: str) -> bool:
        cur = (0, 0)
        seen = {cur}
        dir = {\'N\': (0, 1), \'S\': (0, -1), \'E\': (1, 0), \'W\': (-1, 0)}
        for char in path:
            cur = tuple(map(operator.add, cur, dir[char]))
            if cur in seen:
                return True
            seen.add(cur)
        return False
```
**Analysis:**

Time & space: O(n), where n = path.length().
</p>


