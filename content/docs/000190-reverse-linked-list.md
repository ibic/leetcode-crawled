---
title: "Reverse Linked List"
weight: 190
#id: "reverse-linked-list"
---
## Description
<div class="description">
<p>Reverse a singly linked list.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> 1-&gt;2-&gt;3-&gt;4-&gt;5-&gt;NULL
<strong>Output:</strong> 5-&gt;4-&gt;3-&gt;2-&gt;1-&gt;NULL
</pre>

<p><b>Follow up:</b></p>

<p>A linked list can be reversed either iteratively or recursively. Could you implement both?</p>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Apple - 11 (taggedByAdmin: true)
- Amazon - 9 (taggedByAdmin: true)
- Adobe - 7 (taggedByAdmin: true)
- Microsoft - 6 (taggedByAdmin: true)
- Google - 5 (taggedByAdmin: false)
- Citrix - 4 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: true)
- Mathworks - 3 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Deutsche Bank - 2 (taggedByAdmin: false)
- Zillow - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- Qualcomm - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Spotify - 2 (taggedByAdmin: false)
- Nvidia - 4 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: true)
- Walmart Labs - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Intel - 2 (taggedByAdmin: false)
- Tencent - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Docusign - 2 (taggedByAdmin: false)
- Cisco - 9 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: true)
- Alibaba - 3 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- TripAdvisor - 2 (taggedByAdmin: false)
- Electronic Arts - 2 (taggedByAdmin: false)
- Snapchat - 0 (taggedByAdmin: true)
- Zenefits - 0 (taggedByAdmin: true)
- Yelp - 0 (taggedByAdmin: true)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach #1 (Iterative) [Accepted]

Assume that we have linked list `1 → 2 → 3 → Ø`, we would like to change it to `Ø ← 1 ← 2 ← 3`.

While you are traversing the list, change the current node's next pointer to point to its previous element. Since a node does not have reference to its previous node, you must store its previous element beforehand. You also need another pointer to store the next node before changing the reference. Do not forget to return the new head reference at the end!

```java
public ListNode reverseList(ListNode head) {
    ListNode prev = null;
    ListNode curr = head;
    while (curr != null) {
        ListNode nextTemp = curr.next;
        curr.next = prev;
        prev = curr;
        curr = nextTemp;
    }
    return prev;
}
```

**Complexity analysis**

* Time complexity : $$O(n)$$.
Assume that $$n$$ is the list's length, the time complexity is $$O(n)$$.

* Space complexity : $$O(1)$$.

---
#### Approach #2 (Recursive) [Accepted]

The recursive version is slightly trickier and the key is to work backwards. Assume that the rest of the list had already been reversed, now how do I reverse the front part? Let's assume the list is: n<sub>1</sub> → … → n<sub>k-1</sub> → n<sub>k</sub> → n<sub>k+1</sub> → … → n<sub>m</sub> → Ø

Assume from node n<sub>k+1</sub> to n<sub>m</sub> had been reversed and you are at node n<sub>k</sub>.

n<sub>1</sub> → … → n<sub>k-1</sub> → <b>n<sub>k</sub></b> → n<sub>k+1</sub> ← … ← n<sub>m</sub>

We want n<sub>k+1</sub>’s next node to point to n<sub>k</sub>.

So,

n<sub>k</sub>.next.next = n<sub>k</sub>;

Be very careful that n<sub>1</sub>'s next must point to Ø. If you forget about this, your linked list has a cycle in it. This bug could be caught if you test your code with a linked list of size 2.


```java
public ListNode reverseList(ListNode head) {
    if (head == null || head.next == null) return head;
    ListNode p = reverseList(head.next);
    head.next.next = head;
    head.next = null;
    return p;
}
```

**Complexity analysis**

* Time complexity : $$O(n)$$.
Assume that $$n$$ is the list's length, the time complexity is $$O(n)$$.

* Space complexity : $$O(n)$$.
The extra space comes from implicit stack space due to recursion. The recursion could go up to $$n$$ levels deep.

## Accepted Submission (python3)
```python3
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def reverseListInterative(self, head: ListNode) -> ListNode:
        # iterative
        pre = None
        while head:
            th = head
            head = head.next
            th.next = pre
            pre = th
        return pre

    def reverseList(self, head: ListNode) -> ListNode:
        return self.reverseListRecur(head, None)

    def reverseListRecur(self, head: ListNode, pre: ListNode) -> ListNode:
        if head == None:
            return pre
        nextHead = head.next
        head.next = pre
        return self.reverseListRecur(nextHead, head)

```

## Top Discussions
### In-place iterative and recursive Java solution
- Author: braydenCN
- Creation Date: Tue May 05 2015 06:18:53 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 15:42:21 GMT+0800 (Singapore Standard Time)

<p>
    public ListNode reverseList(ListNode head) {
        /* iterative solution */
        ListNode newHead = null;
        while (head != null) {
            ListNode next = head.next;
            head.next = newHead;
            newHead = head;
            head = next;
        }
        return newHead;
    }
    
    public ListNode reverseList(ListNode head) {
        /* recursive solution */
        return reverseListInt(head, null);
    }
    
    private ListNode reverseListInt(ListNode head, ListNode newHead) {
        if (head == null)
            return newHead;
        ListNode next = head.next;
        head.next = newHead;
        return reverseListInt(next, head);
    }
</p>


### Python Iterative and Recursive Solution
- Author: tusizi
- Creation Date: Sat May 16 2015 19:51:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 04:48:55 GMT+0800 (Singapore Standard Time)

<p>
    class Solution:
    # @param {ListNode} head
    # @return {ListNode}
    def reverseList(self, head):
        prev = None
        while head:
            curr = head
            head = head.next
            curr.next = prev
            prev = curr
        return prev


Recursion

    class Solution:
    # @param {ListNode} head
    # @return {ListNode}
    def reverseList(self, head):
        return self._reverse(head)

    def _reverse(self, node, prev=None):
        if not node:
            return prev
        n = node.next
        node.next = prev
        return self._reverse(n, node)
</p>


### C++ Iterative and Recursive
- Author: jianchao-li
- Creation Date: Mon Jul 06 2015 15:27:59 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 17:58:09 GMT+0800 (Singapore Standard Time)

<p>
Well, since the `head` pointer may also be modified, we create a `pre` that points to it to facilitate the reverse process.

For the example list `1 -> 2 -> 3 -> 4 -> 5` in the problem statement, it will become `0 -> 1 -> 2 -> 3 -> 4 -> 5` (we init `pre -> val` to be `0`). We also set a pointer `cur` to `head`. Then we keep inserting `cur -> next` after `pre` until `cur` becomes the last node. This idea uses three pointers (`pre`, `cur` and `temp`). You may implement it as follows.

```cpp
class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        ListNode *pre = new ListNode(0), *cur = head;
        pre -> next = head;
        while (cur && cur -> next) {
            ListNode* temp = pre -> next;
            pre -> next = cur -> next;
            cur -> next = cur -> next -> next;
            pre -> next -> next = temp;
        }
        return pre -> next;
    }
};
```

Or

```cpp
class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        ListNode *pre = new ListNode(0), *cur = head;
        pre -> next = head;
        while (cur && cur -> next) {
            ListNode* temp = cur -> next;
            cur -> next = temp -> next;
            temp -> next = pre -> next;
            pre -> next = temp;
        }
        return pre -> next;
    }
};
```

We can even use fewer pointers. The idea is to reverse one node at a time from the beginning of the list.

```cpp
class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        ListNode* cur = NULL;
        while (head) {
            ListNode* next = head -> next;
            head -> next = cur;
            cur = head;
            head = next;
        }
        return cur;
    }
};
```

All the above solutions are iterative. A recursive one is as follows. First reverse all the nodes after `head`. Then we need to set `head` to be the final node in the reversed list. We simply set its next node in the original list (`head -> next`) to point to it and sets its `next` to `NULL`. 

```cpp
class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        if (!head || !(head -> next)) {
            return head;
        }
        ListNode* node = reverseList(head -> next);
        head -> next -> next = head;
        head -> next = NULL;
        return node;
    }
};
```
</p>


