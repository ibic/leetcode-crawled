---
title: "Contain Virus"
weight: 671
#id: "contain-virus"
---
## Description
<div class="description">
<p>
A virus is spreading rapidly, and your task is to quarantine the infected area by installing walls.
</p><p>
The world is modeled as a 2-D array of cells, where <code>0</code> represents uninfected cells, and <code>1</code> represents cells contaminated with the virus.  A wall (and only one wall) can be installed <b>between any two 4-directionally adjacent cells</b>, on the shared boundary.
</p><p>
Every night, the virus spreads to all neighboring cells in all four directions unless blocked by a wall.
Resources are limited. Each day, you can install walls around only one region -- the affected area (continuous block of infected cells) that threatens the most uninfected cells the following night. There will never be a tie.
</p><p>
Can you save the day? If so, what is the number of walls required? If not, and the world becomes fully infected, return the number of walls used.
</p><p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> grid = 
[[0,1,0,0,0,0,0,1],
 [0,1,0,0,0,0,0,1],
 [0,0,0,0,0,0,0,1],
 [0,0,0,0,0,0,0,0]]
<b>Output:</b> 10
<b>Explanation:</b>
There are 2 contaminated regions.
On the first day, add 5 walls to quarantine the viral region on the left. The board after the virus spreads is:

[[0,1,0,0,0,0,1,1],
 [0,1,0,0,0,0,1,1],
 [0,0,0,0,0,0,1,1],
 [0,0,0,0,0,0,0,1]]

On the second day, add 5 walls to quarantine the viral region on the right. The virus is fully contained.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> grid = 
[[1,1,1],
 [1,0,1],
 [1,1,1]]
<b>Output:</b> 4
<b>Explanation:</b> Even though there is only one cell saved, there are 4 walls built.
Notice that walls are only built on the shared boundary of two different cells.
</pre>
</p>

<p><b>Example 3:</b><br />
<pre>
<b>Input:</b> grid = 
[[1,1,1,0,0,0,0,0,0],
 [1,0,1,0,1,1,1,1,1],
 [1,1,1,0,0,0,0,0,0]]
<b>Output:</b> 13
<b>Explanation:</b> The region on the left only builds two new walls.
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The number of rows and columns of <code>grid</code> will each be in the range <code>[1, 50]</code>.</li>
<li>Each <code>grid[i][j]</code> will be either <code>0</code> or <code>1</code>.</li>
<li>Throughout the described process, there is always a contiguous viral region that will infect <b>strictly more</b> uncontaminated squares in the next round.</li>
</ol>
</p>
</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Bloomberg - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Simulation [Accepted]

**Intuition**

Let's work on simulating one turn of the process.  We can repeat this as necessary while there are still infected regions.

**Algorithm**

Though the implementation is long, the algorithm is straightforward.  We perform the following steps:

* Find all viral regions (connected components), additionally for each region keeping track of the frontier (neighboring uncontaminated cells), and the perimeter of the region.

* Disinfect the most viral region, adding it's perimeter to the answer.

* Spread the virus in the remaining regions outward by 1 square.

<iframe src="https://leetcode.com/playground/r4k43gyU/shared" frameBorder="0" width="100%" height="500" name="r4k43gyU"></iframe>

**Complexity Analysis**

* Time Complexity: $$O((R*C)^{\frac{4}{3}})$$ where $$R, C$$ is the number of rows and columns.  After time $$t$$, viral regions that are alive must have size at least $$t^2 + (t-1)^2$$, so the total number removed across all time is $$\Omega(t^3) \leq R*C$$.

* Space Complexity: $$O(R*C)$$ in additional space.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### python solution (BFS 112ms)
- Author: likew_
- Creation Date: Tue Jun 12 2018 00:35:41 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 12 2018 00:35:41 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    """
    \u601D\u8DEF\uFF1ABFS
        1. \u83B7\u53D6\u611F\u67D3\u533A\u3001\u6269\u6563\u533A\u548C\u5899\u6570\uFF1A\u83B7\u53D6\u611F\u67D3\u533A\u4E2D\u6240\u6709\u70B9\u3001\u53CA\u6BCF\u4E2A\u533A\u57DF\u7684\u6269\u6563\u70B9\u3001\u5899\u6570
        2. \u5EFA\u5899\uFF1A\u9009\u53D6\u6269\u6563\u70B9\u6570\u6700\u591A\u7684\u533A\u57DF\uFF0C\u7EDF\u8BA1\u5899\u6570\uFF0C\u5EFA\u5899\u540E\u5899\u5185\u8282\u70B9\u8BBE\u7F6E\u4E3A\u5B89\u5168\u533A
        3. \u6269\u6563\uFF1A\u5176\u4ED6\u533A\u57DF\u7684\u6269\u6563\u70B9\u8BBE\u4E3A\u611F\u67D3\u533A
        4. \u91CD\u590D\u4EE5\u4E0A\u8FC7\u7A0B\uFF0C\u76F4\u81F3\u6CA1\u6709\u611F\u67D3\u533A
    """
    def containVirus(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        m,n = len(grid),len(grid[0])
        
        def adj(i,j):
            for ii,jj in [(i-1,j),(i+1,j),(i,j-1),(i,j+1)]:
                if 0 <= ii < m and 0 <= jj< n:
                    yield ii,jj
        
        def get_virus_areas(grid):
            areas = []
            dangers = []
            walls = []
            color = [[0] * n for i in range(m)]
            
            for i in range(m):
                for j in range(n):
                    if grid[i][j] == 1 and color[i][j] == 0:
                        area = [(i,j)]
                        danger = set()
                        wall = 0
                        Q = [(i,j)]
                        color[i][j] = 1
                        while Q:
                            s,t = Q.pop(0)
                            for ii,jj in adj(s,t):
                                if grid[ii][jj] == 1 and color[ii][jj] == 0:
                                    color[ii][jj] = 1
                                    Q.append((ii,jj))
                                    area.append((ii,jj))
                                if grid[ii][jj] == 0:
                                    wall += 1
                                    danger.add((ii,jj))
                        areas.append(area)
                        dangers.append(danger)
                        walls.append(wall)
            return areas,dangers,walls
        
        def spread(dangers):
            for danger in dangers:
                for i,j in danger:
                    grid[i][j] = 1
        
        wall_count = 0
        areas,dangers,walls = get_virus_areas(grid)
        while areas:
            # \u5982\u679C\u5168\u662F\u611F\u67D3\u533A\uFF0C\u8FD4\u56DE
            n_area = len(areas)
            if sum(len(area) for area in areas) == m * n:
                return wall_count
            
            # \u83B7\u53D6\u5371\u9669\u70B9\u6700\u591A\u7684\u533A\u57DF
            dangerest_i = 0
            for i in xrange(n_area):
                if len(dangers[i]) > len(dangers[dangerest_i]):
                    dangerest_i = i
            
            # \u5EFA\u5899\uFF0C\u7EDF\u8BA1\u5899\u6570\uFF0C\u5C06\u5BF9\u5E94\u611F\u67D3\u533A\u53D8\u4E3A\u5B89\u5168\u533A
            wall_count += walls[dangerest_i]
            for i,j in areas[dangerest_i]:
                grid[i][j] = -1
            
            # \u5176\u4ED6\u611F\u67D3\u533A\u6269\u6563
            spread(dangers[:dangerest_i] + dangers[dangerest_i+1:])
            
            # \u91CD\u65B0\u83B7\u53D6\u611F\u67D3\u533A
            areas,dangers,walls = get_virus_areas(grid)
        
        return wall_count
```      
                
                                    
                                    
                                    
            
        
</p>


### Java, DFS, 9 ms, Explanation with comments
- Author: gagarwal
- Creation Date: Tue Mar 03 2020 02:57:36 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Mar 05 2020 11:59:50 GMT+0800 (Singapore Standard Time)

<p>
This class **Region** hold the information for each region.
```
private class Region {
    // Using Set<Integer> instead of List<int[]> (int[] -> row and col pair),
    // as need to de-dupe the elements.
    // If N rows and M cols.
    // Total elements = NxM.
    // Given row and col calculate X as: X = row * M + col.
    // Given X calculate row and col as: row = X / M and col = X % M.
	
    // Infected nodes represented by 1.
    Set<Integer> infected = new HashSet<>();

    // Uninfected neighbors represented by 0 are the ones this region can infect if not contained.
    Set<Integer> uninfectedNeighbors = new HashSet<>();
   
    // Number of walls required to contain all the infected nodes (1s) in this region.
    // Note that two infected 1s can infect the same 0, so in this case we need two walls to save one 0 from two 1s.
    int wallsRequired = 0;
}
```

**Steps**:
1. Find all the regions.
2. Get the region which has most number of uninfected neighbors. This region will cause maximum damage.
3. For region in (2) contain the region. Mark all the infected nodes in this region as `2` to denote that these nodes are now contained and will not cause any more damage.
4. For the remaining regions, expand by infecting the uninfected neighbors around these regions.
5. Repeat steps (1) to (4) until there are no more regions with uninfected neighbors.

```
public int containVirus(int[][] grid) {
    if (grid == null || grid.length == 0) {
        return 0;
    }

    int rows = grid.length;
    int cols = grid[0].length;

    int result = 0;

    while (true) {
        List<Region> regions = new ArrayList<>();

        // Find all the regions using DFS (or can also use BFS).
        boolean[][] visited = new boolean[rows][cols];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                if (grid[row][col] == 1 && !visited[row][col]) {
                    Region region = new Region();
                    dfs(grid, row, col, visited, region);

                    // Add region to list of regions only if it can cause infection.
                    if (region.uninfectedNeighbors.size() > 0) {
                        regions.add(region);
                    }
                }
            }
        }

        // No more regions that can cause further infection, we are done.
        if (regions.size() == 0) {
            break;
        }

        // Sort the regions. Region which can infected most neighbors first.
        regions.sort(new Comparator<Region>() {
            @Override
            public int compare(Region o1, Region o2) {
                return o2.uninfectedNeighbors.size() - o1.uninfectedNeighbors.size();
            }
        });

        // Build wall around region which can infect most neighbors.
        Region regionThatCauseMostInfection = regions.remove(0);
        result += regionThatCauseMostInfection.wallsRequired;

        for (int neighbor : regionThatCauseMostInfection.infected) {
            int row = neighbor / cols;
            int col = neighbor % cols;

            // Choosing 2 as to denote that this cell is now contained
            // and will not cause any more infection.
            grid[row][col] = 2;
        }

        // For remaining regions, expand (neighbors are now infected).
        for (Region region : regions) {
            for (int neighbor : region.uninfectedNeighbors) {
                int row = neighbor / cols;
                int col = neighbor % cols;
                grid[row][col] = 1;
            }
        }
    }

    return result;
}

private void dfs(int[][] grid, int row, int col, boolean[][] visited, Region region) {
    int rows = grid.length;
    int cols = grid[0].length;

    if (row < 0 || row >= rows || col < 0 || col >= cols || grid[row][col] == 2) {
        return;
    }

    if (grid[row][col] == 1) {
        // 1 is already infected.
        // Add to the list, since we are using Set it will be deduped if added multiple times.
        region.infected.add(row * cols + col);

        // If already visited, return as we do not want to go into infinite recursion.
        if (visited[row][col]) {
            return;
        }
    }

    visited[row][col] = true;

    if (grid[row][col] == 0) {
        // If 0 it is uninfected neighbor, we need a wall.
        // Remeber we can reach this 0 multiple times from different infected neighbors i.e. 1s,
        // and this will account for numbers of walls need to be built around this 0.
        region.wallsRequired++;

        // Add to uninfected list, it will be de-duped as we use Set.
        region.uninfectedNeighbors.add(row * cols + col);
        return;
    }

    // Visit neighbors.
    dfs(grid, row + 1, col, visited, region);
    dfs(grid, row - 1, col, visited, region);
    dfs(grid, row, col + 1, visited, region);
    dfs(grid, row, col - 1, visited, region);
}
```
</p>


### Funniest output answer I've ever seen on LeetCode
- Author: cshshshzx
- Creation Date: Sun Dec 17 2017 12:24:41 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 30 2018 10:48:09 GMT+0800 (Singapore Standard Time)

<p>
Not sure do I need to match this in my code.
![0_1513484653195_9e7da87e-7c7f-482f-b805-dfd9fffe7372-image.png](/assets/uploads/files/1513484657849-9e7da87e-7c7f-482f-b805-dfd9fffe7372-image-resized.png)
</p>


