---
title: "Get the Maximum Score"
weight: 1406
#id: "get-the-maximum-score"
---
## Description
<div class="description">
<p>You are given two <strong>sorted</strong> arrays of distinct integers <code>nums1</code> and <code>nums2.</code></p>

<p>A <strong>valid<strong><em> </em></strong>path</strong> is defined as follows:</p>

<ul>
	<li>Choose&nbsp;array nums1 or nums2 to traverse (from index-0).</li>
	<li>Traverse the current array from left to right.</li>
	<li>If you are reading any value that is present in <code>nums1</code> and <code>nums2</code>&nbsp;you are allowed to change your path to the other array. (Only one repeated value is considered in the&nbsp;valid path).</li>
</ul>

<p><em>Score</em> is defined as the sum of uniques values in a valid path.</p>

<p>Return the maximum <em>score</em> you can obtain of all possible&nbsp;<strong>valid&nbsp;paths</strong>.</p>

<p>Since the answer&nbsp;may be too large,&nbsp;return it modulo&nbsp;10^9 + 7.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/07/16/sample_1_1893.png" style="width: 538px; height: 163px;" /></strong></p>

<pre>
<strong>Input:</strong> nums1 = [2,4,5,8,10], nums2 = [4,6,8,9]
<strong>Output:</strong> 30
<strong>Explanation:</strong>&nbsp;Valid paths:
[2,4,5,8,10], [2,4,5,8,9], [2,4,6,8,9], [2,4,6,8,10],  (starting from nums1)
[4,6,8,9], [4,5,8,10], [4,5,8,9], [4,6,8,10]    (starting from nums2)
The maximum is obtained with the path in green <strong>[2,4,6,8,10]</strong>.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [1,3,5,7,9], nums2 = [3,5,100]
<strong>Output:</strong> 109
<strong>Explanation:</strong>&nbsp;Maximum sum is obtained with the path <strong>[1,3,5,100]</strong>.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [1,2,3,4,5], nums2 = [6,7,8,9,10]
<strong>Output:</strong> 40
<strong>Explanation:</strong>&nbsp;There are no common elements between nums1 and nums2.
Maximum sum is obtained with the path [6,7,8,9,10].
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums1 = [1,4,5,8,9,11,19], nums2 = [2,3,4,11,12]
<strong>Output:</strong> 61
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums1.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= nums2.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= nums1[i], nums2[i] &lt;= 10^7</code></li>
	<li><code>nums1</code> and <code>nums2</code> are strictly increasing.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- MindTickle - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Two Pointers, O(1) Space
- Author: lee215
- Creation Date: Sun Aug 02 2020 12:05:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 03 2020 14:54:20 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
We must take the common elements and won\'t miss them;
And there will be two path between the common elements,
and we will take and only take one path.

We calculate the sum of both path, and take the bigger one.
<br>

## **Explanation**
So we apply two points solutions,
and always take the step in the smaller element.

If two elements are the same,
we compare the accumulated sum in the both paths,
and we pick the bigger one.
<br>

## **Complexity**
Time `O(N)`
Space `O(1)`
<br>

**Java:**
```java
    public int maxSum(int[] A, int[] B) {
        int i = 0, j = 0, n = A.length, m = B.length;
        long a = 0, b = 0, mod = (long)1e9 + 7;
        while (i < n || j < m) {
            if (i < n && (j == m || A[i] < B[j])) {
                a += A[i++];
            } else if (j < m && (i == n || A[i] > B[j])) {
                b += B[j++];
            } else {
                a = b = Math.max(a, b) + A[i];
                i++; j++;
            }
        }
        return (int)(Math.max(a, b) % mod);
    }
```

**C++:**
```cpp
    int maxSum(vector<int>& A, vector<int>& B) {
        int i = 0, j = 0, n = A.size(), m = B.size();
        long a = 0, b = 0, mod = 1e9 + 7;
        while (i < n || j < m) {
            if (i < n && (j == m || A[i] < B[j])) {
                a += A[i++];
            } else if (j < m && (i == n || A[i] > B[j])) {
                b += B[j++];
            } else {
                a = b = max(a, b) + A[i];
                i++, j++;
            }
        }
        return max(a, b) % mod;
    }
```
**Python**
```py
    def maxSum(self, A, B):
        i, j, n, m = 0, 0, len(A), len(B)
        a, b, mod = 0, 0, 10**9 + 7
        while i < n or j < m:
            if i < n and (j == m or A[i] < B[j]):
                a += A[i]
                i += 1
            elif j < m and (i == n or A[i] > B[j]):
                b += B[j]
                j += 1
            else:
                a = b = max(a, b) + A[i]
                i += 1
                j += 1
        return max(a, b) % mod
```
</p>


### Java 19 lines dfs+memo with line-by-line explanation easy to understand
- Author: FunBam
- Creation Date: Sun Aug 02 2020 12:01:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 02 2020 12:55:30 GMT+0800 (Singapore Standard Time)

<p>
![image](https://assets.leetcode.com/users/images/8287b1db-1420-4f8a-8eef-974ec8b0f5c4_1596344125.888696.png)

```
class Solution {
    int M = (int)1e9+7;
    public int maxSum(int[] nums1, int[] nums2) {
        Map<Integer, List<Integer>> map= new HashMap<>();
        for (int i=0; i<nums1.length-1; i++)
            map.computeIfAbsent(nums1[i], k -> new LinkedList<>()).add(nums1[i+1]);
        for (int i=0; i<nums2.length-1; i++)
            map.computeIfAbsent(nums2[i], k -> new LinkedList<>()).add(nums2[i+1]);
        Map<Integer, Long> memo = new HashMap<>();
        return (int)Math.max(greedy(nums1[0], map, memo)%M, greedy(nums2[0], map, memo)%M);
    }
    
    long greedy(int cur, Map<Integer, List<Integer>> map, Map<Integer, Long> memo){
        if (memo.containsKey(cur)) return memo.get(cur);
        if (!map.containsKey(cur)) return cur;
        long maxSum=0;
        for (int next: map.get(cur)){
            maxSum = Math.max(maxSum, greedy(next, map, memo));
        }
        maxSum+=cur;
        memo.put(cur, maxSum);
        return maxSum;
    }
}
```
Happy Coding!
</p>


### Python3. O(N) Time, O(1) space; Two pointers; explanation added
- Author: yaroslav-repeta
- Creation Date: Sun Aug 02 2020 12:02:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 02 2020 12:53:06 GMT+0800 (Singapore Standard Time)

<p>
Logic is simple:
Let\'s say that a place where `nums1[i] = nums2[j]` is checkpoint.
Then result will be max prefix sum of two arrays + checkpoint + max sum postfix of two arrays
Or: `max(sum(nums1[0:i]), sum(nums2[0:j]) + checkpoint + max(sum(nums1[i + 1:]), sum(nums2[j + 1:]))` 

So what we need to do is:
1. Iterate through two arrays with calculating sum until we find checkpoint
2. Add larger sum to result.
3. Add checkpoint to result.
4. Reset sums.
5. Repeat.

```python
class Solution:
    def maxSum(self, nums1: List[int], nums2: List[int]) -> int:
		M, N = len(nums1), len(nums2)
        sum1, sum2 = 0, 0
        i, j = 0, 0
        res = 0
        while i < M and j < N:
            if nums1[i] < nums2[j]:
                sum1 += nums1[i]
                i += 1
            elif nums1[i] > nums2[j]:
                sum2 += nums2[j]
                j += 1
            else:
                res += max(sum1, sum2) + nums1[i]
                i += 1
                j += 1
                sum1 = 0
                sum2 = 0
                
        while i < M:
            sum1 += nums1[i]
            i += 1
        while j < N:
            sum2 += nums2[j]
            j += 1
        return (res + max(sum1, sum2)) % 1000000007
```

Time: O(N), where N is the total number of elements in arrays.
Space: O(1)
</p>


