---
title: "Insert into a Binary Search Tree"
weight: 705
#id: "insert-into-a-binary-search-tree"
---
## Description
<div class="description">
<p>You are given the <code>root</code> node of a binary search tree (BST) and a <code>value</code> to insert into the tree. Return <em>the root node of the BST after the insertion</em>. It is <strong>guaranteed</strong> that the new value does not exist in the original BST.</p>

<p><strong>Notice</strong>&nbsp;that there may exist&nbsp;multiple valid ways for the&nbsp;insertion, as long as the tree remains a BST after insertion. You can return <strong>any of them</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/10/05/insertbst.jpg" style="width: 752px; height: 221px;" />
<pre>
<strong>Input:</strong> root = [4,2,7,1,3], val = 5
<strong>Output:</strong> [4,2,7,1,3,5]
<strong>Explanation:</strong> Another accepted tree is:
<img alt="" src="https://assets.leetcode.com/uploads/2020/10/05/bst.jpg" style="width: 352px; height: 301px;" />
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = [40,20,60,10,30,50,70], val = 25
<strong>Output:</strong> [40,20,60,10,30,50,70,null,null,25]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [4,2,7,1,3,null,null,null,null,null,null], val = 5
<strong>Output:</strong> [4,2,7,1,3,5]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in&nbsp;the tree will be in the range <code>[0,&nbsp;10<sup>4</sup>]</code>.</li>
	<li><code>-10<sup>8</sup> &lt;= Node.val &lt;= 10<sup>8</sup></code></li>
	<li>All the values <code>Node.val</code> are <strong>unique</strong>.</li>
	<li><code>-10<sup>8</sup> &lt;= val &lt;= 10<sup>8</sup></code></li>
	<li>It&#39;s <strong>guaranteed</strong> that <code>val</code> does not exist in the original BST.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Intuition

One of the huge BST advantages is a [search](https://leetcode.com/problems/search-in-a-binary-search-tree/) 
for _arbitrary_ element in $$\mathcal{O}(\log N)$$ time.
Here we'll see that the insert time is $$\mathcal{O}(\log N)$$, too, in the average case. 

The problem solution is very simple - one could always insert new node as a child of the leaf.
To define which leaf to use, one could follow the standard BST logic :

- If `val > node.val` - go to insert into the right subtree.

- If `val < node.val` - go to insert into the left subtree.

![bla](../Figures/701/insert.png)

<br />
<br />


---
#### Approach 1: Recursion

The recursion implementation is very straightforward :

- If `root` is null - return `TreeNode(val)`.

- If `val > root.val` - go to insert into the right subtree.

- If `val < root.val` - go to insert into the left subtree.

- Return `root`.

!?!../Documents/701_LIS.json:1000,420!?!

<iframe src="https://leetcode.com/playground/c8NnS28B/shared" frameBorder="0" width="100%" height="259" name="c8NnS28B"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(H)$$, where $$H$$ is a tree height. That results in
$$\mathcal{O}(\log N)$$ in the average case, and $$\mathcal{O}(N)$$ in the worst case. 

    Let's compute time complexity with the help of 
    [master theorem](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)) 
    $$T(N) = aT\left(\frac{N}{b}\right) + \Theta(N^d)$$.
    The equation represents dividing the problem 
    up into $$a$$ subproblems of size $$\frac{N}{b}$$ in $$\Theta(N^d)$$ time. 
    Here at step there is only one subproblem `a = 1`, its size 
    is a half of the initial problem `b = 2`, 
    and all this happens in a constant time `d = 0`, as for
    the binary search.
    That means that $$\log_b{a} = d$$ and hence we're dealing with 
    [case 2](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)#Case_2_example)
    that results in $$\mathcal{O}(n^{\log_b{a}} \log^{d + 1} N)$$
    = $$\mathcal{O}(\log N)$$ time complexity.
    
* Space complexity : $$\mathcal{O}(H)$$ to keep the recursion stack,
i.e. $$\mathcal{O}(\log N)$$ in the average case, 
and $$\mathcal{O}(N)$$ in the worst case.

<br />
<br />


---
#### Approach 2: Iteration

The recursion above could be converted into the iteration

<iframe src="https://leetcode.com/playground/bEboLTtL/shared" frameBorder="0" width="100%" height="497" name="bEboLTtL"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(H)$$, where $$H$$ is a tree height. That results in
$$\mathcal{O}(\log N)$$ in the average case, and $$\mathcal{O}(N)$$ in the worst case. 

    Let's compute time complexity with the help of 
    [master theorem](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)) 
    $$T(N) = aT\left(\frac{N}{b}\right) + \Theta(N^d)$$.
    The equation represents dividing the problem 
    up into $$a$$ subproblems of size $$\frac{N}{b}$$ in $$\Theta(N^d)$$ time. 
    Here at step there is only one subproblem `a = 1`, its size 
    is a half of the initial problem `b = 2`, 
    and all this happens in a constant time `d = 0`, as for
    the binary search.
    That means that $$\log_b{a} = d$$ and hence we're dealing with 
    [case 2](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)#Case_2_example)
    that results in $$\mathcal{O}(n^{\log_b{a}} \log^{d + 1} N)$$
    = $$\mathcal{O}(\log N)$$ time complexity.
    
* Space complexity : $$\mathcal{O}(1)$$ since it's a constant space
solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### java iterative 100%
- Author: trustno1
- Creation Date: Tue Jul 17 2018 15:09:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 17:19:20 GMT+0800 (Singapore Standard Time)

<p>
```
    public TreeNode insertIntoBST(TreeNode root, int val) {
        if(root == null) return new TreeNode(val);
        TreeNode cur = root;
        while(true) {
            if(cur.val <= val) {
                if(cur.right != null) cur = cur.right;
                else {
                    cur.right = new TreeNode(val);
                    break;
                }
            } else {
                if(cur.left != null) cur = cur.left;
                else {
                    cur.left = new TreeNode(val);
                    break;
                }
            }
        }
        return root;
    }
</p>


### Python - 4 line clean recursive solution
- Author: dmerrick
- Creation Date: Fri Oct 12 2018 01:49:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 03:09:41 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def insertIntoBST(self, root, val):
        if(root == None): return TreeNode(val);
        if(root.val < val): root.right = self.insertIntoBST(root.right, val);
        else: root.left = self.insertIntoBST(root.left, val);
        return(root)
```
</p>


### Java easy to understand solution
- Author: elsawelf
- Creation Date: Tue Aug 28 2018 13:00:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 00:26:51 GMT+0800 (Singapore Standard Time)

<p>
public TreeNode insertIntoBST(TreeNode root, int val) {
        if (root == null) {
            return new TreeNode(val);
        }
        if (root.val > val) {
            root.left = insertIntoBST(root.left, val);
        } else {
            root.right = insertIntoBST(root.right, val);
        }
        return root;
    }
</p>


