---
title: "Guess the Majority in a Hidden Array"
weight: 1424
#id: "guess-the-majority-in-a-hidden-array"
---
## Description
<div class="description">
<p>We have an integer array&nbsp;<code>nums</code>, where all the integers in&nbsp;<code>nums</code>&nbsp;are <strong>0</strong> or <strong>1</strong>. You will not be given direct access to the array, instead, you will have an&nbsp;<strong>API</strong>&nbsp;<code>ArrayReader</code>&nbsp;which have the following functions:</p>

<ul>
	<li><code>int query(int a, int b, int c, int d)</code>: where <code>0 &lt;= a &lt; b &lt; c &lt; d&nbsp;&lt;&nbsp;ArrayReader.length()</code>.&nbsp;The function returns the distribution of the value of the 4 elements and returns:

	<ul>
		<li><strong>4 </strong>: if the values of the 4 elements are the same (0 or 1).</li>
		<li><strong>2</strong>&nbsp;: if three&nbsp;elements have a value&nbsp;equal to 0&nbsp;and one&nbsp;element has value equal to 1&nbsp;or vice versa.</li>
		<li><strong>0&nbsp;</strong>:&nbsp;if two element have a value equal to 0 and two elements have a value equal to 1.</li>
	</ul>
	</li>
	<li><code>int length()</code>: Returns the size of the array.</li>
</ul>

<p>You are allowed to call&nbsp;<code>query()</code>&nbsp;<b>2 * n&nbsp;times</b> at most where n is equal to <code>ArrayReader.length()</code>.</p>

<p>Return <strong>any</strong> index of the most frequent value in <code>nums</code>,&nbsp;in case of tie, return -1.</p>

<p><strong>Follow up:</strong> What is the minimum number of calls needed to find the majority element?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,0,1,0,1,1,1,1]
<strong>Output:</strong> 5
<strong>Explanation:</strong> The following calls to the API
reader.length() // returns 8 because there are 8 elements in the hidden array.
reader.query(0,1,2,3) // returns 2 this is a query that compares the elements nums[0], nums[1], nums[2], nums[3]
// Three elements have a value equal to 0 and one element has value equal to 1 or viceversa.
reader.query(4,5,6,7) // returns 4 because nums[4], nums[5], nums[6], nums[7] have the same value.
we can infer that the most frequent value is found in the last 4 elements.
Index 2, 4, 6, 7 is also a correct answer.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,0,1,1,0]
<strong>Output:</strong> 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,0,1,0,1,0,1,0]
<strong>Output:</strong> -1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>5 &lt;= nums.length&nbsp;&lt;= 10^5</code></li>
	<li><code>0 &lt;= nums[i] &lt;= 1</code></li>
</ul>

</div>

## Tags


## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python O(1) space with explanation
- Author: yuanzhi247012
- Creation Date: Fri Aug 07 2020 10:37:37 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 07 2020 11:18:34 GMT+0800 (Singapore Standard Time)

<p>
If 0,1,2,3 equals 0,1,2,4, then nums[3]==nums[4], do the same to 0,1,2,5 and 0,1,2,6, ..., this way, we can group all numbers after nums[3] into two groups, those equal to nums[3] and those not equal to nums[3].
The rest is just finding which group nums[0], nums[1], nums[2] belongs to.
We calculate [1,2,3,4], [0,2,3,4], [0,1,3,4] respectively and compare their result with [0,1,2,4]. 
Taking [1,2,3,4] for example, its common elements with [0,1,2,4] are 1,2,4, different elements are nums[0] and nums[3], if their results are same, it means nums[0]==nums[3].

```
    def guessMajority(self, reader: \'ArrayReader\') -> int:
        n,groupA,groupB,aIdx,bIdx=reader.length(),1,0,None,None
        first,second=reader.query(0,1,2,3),reader.query(0,1,2,4)
        for i in range(4,n):
            if reader.query(0,1,2,i)==first:
                groupA,aIdx=groupA+1,i
            else:
                groupB,bIdx=groupB+1,i
        for i in range(3):
            nxt=[v for v in [0,1,2,3,4] if v!=i]
            if reader.query(*nxt)==second:
                groupA,aIdx=groupA+1,i
            else:
                groupB,bIdx=groupB+1,i
        return aIdx if groupA>groupB else bIdx if groupB>groupA else -1
```
</p>


### Java n times query Solution
- Author: ljr1226
- Creation Date: Thu Aug 06 2020 15:57:14 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 06 2020 15:57:14 GMT+0800 (Singapore Standard Time)

<p>
1.  Assign arr[0] to value 1;
2.  Compare to figure out the first 5 numbers;
3.  for(i = 5;i<n;i++) querry(i-3,i-2,i-1,i);
     Compare to the value of query(i-4,i-3,i-2,i-1) to know whether arr[i] and arr[i-4] are same or different;
```
class Solution {
    
    // query n times;
    public int guessMajority(ArrayReader reader) {
        int n = reader.length();
        int[] map = new int[n];   
        map[0] = 1;
        int a = reader.query(0,1,2,3);
        int e = reader.query(1,2,3,4); map[4] = a==e?1:2;
        int b = reader.query(0,2,3,4); map[1] = b==e?1:2;
        int c = reader.query(0,1,3,4); map[2] = b==c?map[1]:3-map[1];
        int d = reader.query(0,1,2,4); map[3] = c==d?map[2]:3-map[2];
        int count1 = 0, count2 = 0;
        int id1 = -1, id2 =-1;
        int res = -1;
        for(int i=0;i<5;i++) {
            if(map[i] == 1) {
                count1++; id1 = i;
            }
            else {
                count2 ++; id2= i;
            }
        }
        int last = e;
        for(int i=5;i<n;i++){
            int cur = reader.query(i-3,i-2,i-1,i);
            map[i] = cur == last? map[i-4]:(3-map[i-4]);
            last = cur;
            if(map[i] == 1) {
                count1++; id1 = i;
            }
            else {
                count2 ++; id2= i;
            }
        } 
        //System.out.println(Arrays.toString(map));
        if(count1 == count2) return -1;
        return count1 > count2? id1:id2;
    }
}
```
</p>


### Java O(n) Time O(1) Space, Use Map to Store Transition Sequence, Detailed
- Author: REED_W
- Creation Date: Fri Aug 07 2020 14:11:40 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 07 2020 14:29:49 GMT+0800 (Singapore Standard Time)

<p>
first let\'s figure out the transition sequence

4: [1111, 0000]
2: [0111, 1011, 1101, 1110, 1000, 0100, 0010, 0001]
0: [0011, 0101, 0110, 1001, 1010, 1100]

let\'s define two sequences as neibor if they have 3 commen elements, like 

```
s[i,i+1,i+2,i+3] and s[i+1,i+2,i+3,i+4], 
s[i,i+1,i+2,i+3] and s[i,i+1,i+3,i+4], 
```

we can easily find that neibor sequeen (for eample: s[i,i+1,i+2,i+3] and s[i+1,i+2,i+3,i+4]) transition can only be [0->0, 0->2, 2->0, 2->2, 2->4, 4->2, 4->4]
and when 

0->0,  s[i] == s[i+4]
0->2,  s[i] != s[i+4]
2->0, s[i] != s[i+4]
2->2, s[i] == s[i+4]
2->4, s[i] == s[i+4]
4->2, s[i] != s[i+4]
4->4, s[i] == s[i+4]

we can use a map to store these transitions:

        map[0][2] = 1;
        map[2][0] = 1;
        map[2][4] = 1;
        map[4][2] = 1;

but we first need to figure out the first 5 values.
let\'s assume we have an array stroring the values, and 
```
arr[0] = 0;
```
then we can use reader.query(0,1,2,3) to get the initial status, noted as "prev"
second we can get status of (1,2,3,4) as curr=reader.query(1,2,3,4),
using the transition of this two status we can have 

```
arr[4] = arr[0]^map[prev][curr]
```
we can use similar method to caculate  arr[1], arr[2], arr[3] as:

```
  curr = reader.query(0,2,3,4);
  arr[1] = arr[4]^map[prev][curr];
  curr = reader.query(0,1,3,4);
  arr[2] = arr[4]^map[prev][curr];
  curr = reader.query(0,1,2,4);
  arr[3] = arr[4]^map[prev][curr];
```
then we can start to loop through the rest of array to find the value one by one,

code:

    //O(n)/O(n)
    private int sln1(ArrayReader reader){
        int[][] map = new int[5][5];
        map[0][2] = 1;
        map[2][0] = 1;
        map[2][4] = 1;
        map[4][2] = 1;
        
        int n = reader.length();
        int[] arr = new int[n];
        int prev = reader.query(0,1,2,3);
        int curr = reader.query(1,2,3,4);
        arr[4] = arr[0]^map[prev][curr];
        curr = reader.query(0,2,3,4);
        arr[1] = arr[4]^map[prev][curr];
        curr = reader.query(0,1,3,4);
        arr[2] = arr[4]^map[prev][curr];
        curr = reader.query(0,1,2,4);
        arr[3] = arr[4]^map[prev][curr];
        for(int i = 4;i<n;i++){
            curr = reader.query(i-3, i-2, i-1, i);
            arr[i] = arr[i-4]^map[prev][curr];
            prev = curr;
        }
        int[] count = new int[2];
        for(int i: arr) count[i]++;
        if(count[0]==count[1]) return -1;
        int max = count[0]>count[1]? 0:1;
        for(int i=0;i< arr.length;i++)if(arr[i]==max) return i;
        return -1;
    }
	
	
we can use rolling index to optimize the space to O(1):

    //O(n)/O(1)
    private int sln2(ArrayReader reader){
        int[][] map = new int[5][5];
        map[0][2] = 1;
        map[2][0] = 1;
        map[2][4] = 1;
        map[4][2] = 1;
        
        int n = reader.length();
        int[] arr = new int[5];
        int prev = reader.query(0,1,2,3);
        int curr = reader.query(1,2,3,4);
        arr[4] = arr[0]^map[prev][curr];
        curr = reader.query(0,2,3,4);
        arr[1] = arr[4]^map[prev][curr];
        curr = reader.query(0,1,3,4);
        arr[2] = arr[4]^map[prev][curr];
        curr = reader.query(0,1,2,4);
        arr[3] = arr[4]^map[prev][curr];
        
        int[] count= new int[2];
        int[] last = new int[2];
        for(int i =0;i<4;i++){
            count[arr[i]]++;
            last[arr[i]] = i;
        }
        
        for(int i = 4;i<n;i++){
            curr = reader.query(i-3, i-2, i-1, i);
            arr[i%5] = arr[(i-4)%5]^map[prev][curr];
            prev = curr;
            count[arr[i%5]]++;
            last[arr[i%5]] = i;
        }
        if(count[0]>count[1]) return last[0];
        else if(count[0]< count[1]) return last[1];
        else return -1;
    }
	


</p>


