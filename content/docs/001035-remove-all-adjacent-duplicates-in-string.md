---
title: "Remove All Adjacent Duplicates In String"
weight: 1035
#id: "remove-all-adjacent-duplicates-in-string"
---
## Description
<div class="description">
<p>Given a string <code>S</code> of lowercase letters, a <em>duplicate removal</em> consists of choosing two adjacent and equal letters, and removing&nbsp;them.</p>

<p>We repeatedly make duplicate removals on S until we no longer can.</p>

<p>Return the final string after all such duplicate removals have been made.&nbsp; It is guaranteed the answer is unique.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;abbaca&quot;</span>
<strong>Output: </strong><span id="example-output-1">&quot;ca&quot;</span>
<strong>Explanation: </strong>
For example, in &quot;abbaca&quot; we could remove &quot;bb&quot; since the letters are adjacent and equal, and this is the only possible move.&nbsp; The result of this move is that the string is &quot;aaca&quot;, of which only &quot;aa&quot; is possible, so the final string is &quot;ca&quot;.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= S.length &lt;= 20000</code></li>
	<li><code>S</code> consists only of English lowercase letters.</li>
</ol>
</div>

## Tags
- Stack (stack)

## Companies
- Facebook - 6 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Bloomberg - 5 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- PayTM - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Replace

One could use here the standard replace function.
String consists of English lowercase letters, and hence all 26
possible duplicates are known in advance.

The idea is very simple:

1. Generate hashset of all 26 possible duplicates from `aa` to `zz`.

2. Iterate over that 26 duplicates and replace them all in string by empty char.

Note that such a strategy could introduce some new duplicates,
for example `abbaca` -> `aaca`, and hence step number 2 sometimes
should be repeated several times. 
The idea is to repeat step 2 till the string still changes after the 
replacements. That could be checked by the string length.

![fig](../Figures/1047/repl.png)

**Algorithm**

- Generate hashset of all 26 possible duplicates from `aa` to `zz`.

- Initiate 'one step before' string length by `prevLength = -1`.

- While previous length is still different from the current one 
`prevLength != S.length()`

    - Set 'one step before' length to be equal to the string length
    `prevLength = S.length()`.

    - Iterate over all 26 duplicates and replace them in string by empty char.
    
- Return S.

**Implementation**

<iframe src="https://leetcode.com/playground/GapLYJTx/shared" frameBorder="0" width="100%" height="395" name="GapLYJTx"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N^2)$$, where N is a string length.
Here we have an onion : while -> for -> replace. 
`while` is executed not more then $$N/2$$ times, 
`for` is always run 26 times, and `replace` has $$\mathcal{O}(N)$$
run time in average. In total that results in 
$$\mathcal{O}(\frac{N}{2} \times 26 \times N)$$ = $$\mathcal{O}(N^2)$$.

* Space complexity : $$\mathcal{O}(N)$$. The hashset of duplicates 
has the constant length 26, but replace function 
actually creates a copy of the string and thus uses $$\mathcal{O}(N)$$ space. 
<br /> 
<br />


---
#### Approach 2: Stack

We could trade an extra space for speed. 
The idea is to use an output stack to keep track of only
non duplicate characters. Here is how it works:

- Current string character is equal to the last element in stack? 
Pop that last element out of stack.

- Current string character is _not_ equal to the last element in stack? 
Add the current character into stack.

> Which data structure to use as the stack here? 

Something that is fast to convert to string for output, 
for example list in Python and StringBuilder in Java.

!?!../Documents/1047_LIS.json:1000,478!?!

**Algorithm**

- Initiate an empty output stack.

- Iterate over all characters in the string. 

    - Current element is equal to the last element in stack? 
    Pop that last element out of stack.
    
    - Current element is not equal to the last element in stack? 
    Add the current element into stack.
    
- Convert stack into string and return it.

**Implementation**

<iframe src="https://leetcode.com/playground/iZNtRfEX/shared" frameBorder="0" width="100%" height="310" name="iZNtRfEX"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$, where N is a string length.
* Space complexity : $$\mathcal{O}(N - D)$$ where D is a total length 
for all duplicates.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Two Pointers and Stack Solution
- Author: lee215
- Creation Date: Sun May 19 2019 12:03:23 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 07 2020 14:27:19 GMT+0800 (Singapore Standard Time)

<p>
## Solution 1: Two Pointers
`i` refers to the index to set next character in the output string.
`j` refers to the index of current iteration in the input string.

Iterate characters of `S` one by one by increasing `j`.

If `S[j]` is same as the current last character `S[i - 1]`,
we remove duplicates by doing `i -= 2`.

If `S[j]` is different as the current last character `S[i - 1]`,
we set `S[i] = S[j]` and increment `i++`.
<br>

**Java**
```java
    public String removeDuplicates(String s) {
        int i = 0, n = s.length();
        char[] res = s.toCharArray();
        for (int j = 0; j < n; ++j, ++i) {
            res[i] = res[j];
            if (i > 0 && res[i - 1] == res[i]) // count = 2
                i -= 2;
        }
        return new String(res, 0, i);
    }
```

**C++**
```cpp
    string removeDuplicates(string s) {
        int i = 0, n = s.length();
        for (int j = 0; j < n; ++j, ++i) {
            s[i] = s[j];
            if (i > 0 && s[i - 1] == s[i]) // count = 2
                i -= 2;
        }
        return s.substr(0, i);
    }
```
<br>

## Why This solution
You can easily update this solution to remove more duplicates.
Now it\'s a specil case where we only remove replicates `k = 2`.
<br>

## Solution 2: Stack

Keep a `res` as a characters stack.
Iterate characters of `S` one by one.

If the next character is same as the last character in `res`,
pop the last character from `res`.
In this way, we remove a pair of adjacent duplicates characters.

If the next character is different,
we append it to the end of `res`.
<br>

**C++**
```cpp
    string removeDuplicates(string S) {
        string res = "";
        for (char& c : S)
            if (res.size() && c == res.back())
                res.pop_back();
            else
                res.push_back(c);
        return res;
    }
```
**Java**
```java
    public String removeDuplicates(String S) {
        StringBuilder sb = new StringBuilder();
        for (char c : S.toCharArray()) {
            int size = sb.length();
            if (size > 0 && sb.charAt(size - 1) == c) {
                sb.deleteCharAt(size - 1);
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
```
**Python**
```py
    def removeDuplicates(self, S):
        res = []
        for c in S:
            if res and res[-1] == c:
                res.pop()
            else:
                res.append(c)
        return "".join(res)
```

**Python 1-line**
```py
    def removeDuplicates(self, S):
        return reduce(lambda s, c: s[:-1] if s[-1:] == c else s + c, S)
```
## **Complexity**
These two solution actually work in the similar way.
Time `O(N)` for one pass
Space `O(N)` for output
<br>
</p>


### [Java/Python 3] three easy iterative codes w/ brief explanation, analysis and follow-up.
- Author: rock
- Creation Date: Sun May 19 2019 12:22:23 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 09 2020 10:41:20 GMT+0800 (Singapore Standard Time)

<p>
**Method 1: ArrayDeque**

If current char is same as previous char in the ArrayDeque, pop out the previous char; 
Otherwise, add current char into the ArrayDeque.


```
    public String removeDuplicates(String S) {
        Deque<Character> dq = new ArrayDeque<>();
        for (char c : S.toCharArray()) {
            if (!dq.isEmpty() && dq.peekLast() == c) { 
                dq.pollLast();
            }else {
                dq.offer(c);
            }
        }
        StringBuilder sb = new StringBuilder();
        for (char c : dq) { sb.append(c); }
        return sb.toString();
    }
```
```
    def removeDuplicates(self, S: str) -> str:
        dq = collections.deque()
        for c in S:
            if dq and dq[-1] == c:
                dq.pop()
            else:
                dq.append(c)
        return \'\'.join(dq)
```
Or just use list:
```
    def removeDuplicates(self, S: str) -> str:
        stack = []
        for c in S:
            if stack and stack[-1] == c:
                stack.pop()
            else:
                stack.append(c)
        return \'\'.join(stack)
```

**Method 2: StringBuilder.**

Get rid of the ArrayDeque in method 1, use only StringBuilder.

If current char is same as the end of the StringBuilder, delete the char at end; otherwise, append it at the end.

```
    public String removeDuplicates(String S) {
        StringBuilder sb = new StringBuilder();
        for (char c : S.toCharArray()) {
            int size = sb.length();
            if (size > 0 && sb.charAt(size - 1) == c) { 
                sb.deleteCharAt(size - 1); 
            }else { 
                sb.append(c); 
            }
        }
        return sb.toString();
    }
``` 

**Method 3: two pointers**

If current char is same as the end of non-adjacent-duplicate chars, decrease the counter `end` by 1;
otherwise, copy the current char to its end.

```
    public String removeDuplicates(String S) {
        char[] a = S.toCharArray();
        int end = -1;
        for (char c : a) {
            if (end >= 0 && a[end] == c) { 
                --end; 
            }else { 
                a[++end] = c; 
            }
        }
        return String.valueOf(a, 0, end + 1);
    }
```
```
    def removeDuplicates(self, S: str) -> str:
        end, a = -1, list(S)
        for c in a:
            if end >= 0 and a[end] == c:
                end -= 1
            else:
                end += 1
                a[end] = c
        return \'\'.join(a[: end + 1])
```
**Analysis for all methods:**

Time & space: O(n), where n = S.length().

----

**Follow-up:**

Q: What if we are required to **remove all k-in-a-row duplicates in a string (for more-than-k-in-a-row, remove only first k duplicates)**? - credit to **@betterztt**.

A: Use 2 stacks, one for characters, and the other for the count of adjacent characters.

```java
    public String removeDuplicates(String S, int k) {
        Deque<Character> charStk = new ArrayDeque<>();
        Deque<Integer> cntStk = new ArrayDeque<>();
        for (int i = 0; i < S.length(); ++i) {
            char c = S.charAt(i);
            if (charStk.isEmpty() || charStk.peek() != c) { // no char in stack yet, or top char is different from the current.
                charStk.push(c);
                cntStk.push(1);
            }else if (cntStk.peek() + 1 < k) { // top char is same as the current, but less than k after appending the current.
                cntStk.push(cntStk.pop() + 1);
            }else { // found k-in-a-row duplicates, remove them.
                charStk.pop();
                cntStk.pop();
            }
        }
        StringBuilder sb = new StringBuilder();
        for (char c : charStk) {
            int cnt = cntStk.pop();
            while (cnt-- > 0) {
                sb.append(c);
            }
        }
        return sb.reverse().toString(); // Do NOT forget reverse().
    }
```
```python
    def removeDuplicates(self, S: str, k: int) -> str:
         stk = []
        for char in S:
            if not stk or stk[-1][0] != char:
                stk.append([char, 1])
            elif stk[-1][1] + 1 < k:
                stk[-1][1] += 1
            else:
                stk.pop()
        return \'\'.join(char * cnt for char, cnt in stk)
```

</p>


### Python 100% Stack
- Author: khui0207
- Creation Date: Mon May 20 2019 06:09:13 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 20 2019 06:09:13 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def removeDuplicates(self, S: str) -> str:
        stack = []
        for char in S:
            if stack and stack[-1] == char:
                stack.pop()
            else:
                stack.append(char)
        
        return \'\'.join(stack)
```
</p>


