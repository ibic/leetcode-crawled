---
title: "Product Sales Analysis II"
weight: 1515
#id: "product-sales-analysis-ii"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Sales</code></p>

<pre>
+-------------+-------+
| Column Name | Type  |
+-------------+-------+
| sale_id     | int   |
| product_id  | int   |
| year        | int   |
| quantity    | int   |
| price       | int   |
+-------------+-------+
sale_id is the primary key of this table.
product_id is a foreign key to <code>Product</code> table.
Note that the price is per unit.
</pre>

<p>Table:&nbsp;<code>Product</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| product_id   | int     |
| product_name | varchar |
+--------------+---------+
product_id is the primary key of this table.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query that reports the total quantity sold for every product id.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Sales</code> table:
+---------+------------+------+----------+-------+
| sale_id | product_id | year | quantity | price |
+---------+------------+------+----------+-------+ 
| 1       | 100        | 2008 | 10       | 5000  |
| 2       | 100        | 2009 | 12       | 5000  |
| 7       | 200        | 2011 | 15       | 9000  |
+---------+------------+------+----------+-------+

Product table:
+------------+--------------+
| product_id | product_name |
+------------+--------------+
| 100        | Nokia        |
| 200        | Apple        |
| 300        | Samsung      |
+------------+--------------+

Result table:
+--------------+----------------+
| product_id   | total_quantity |
+--------------+----------------+
| 100          | 22             |
| 200          | 15             |
+--------------+----------------+</pre>

</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Why they provide the second table
- Author: qifeicheng
- Creation Date: Wed Jun 05 2019 13:14:37 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 05 2019 13:14:37 GMT+0800 (Singapore Standard Time)

<p>
It seems that the second product table is useless...
</p>


### Simple 1337ms solution
- Author: brandacus
- Creation Date: Mon Jun 03 2019 04:43:42 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 03 2019 04:43:42 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT product_id, SUM(quantity) AS total_quantity
FROM Sales
GROUP BY product_id
```
</p>


### Simple GROUP BY solution
- Author: tankztc
- Creation Date: Sun Jun 02 2019 10:22:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 10:22:51 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT product_id, sum(quantity) AS total_quantity
FROM Sales
GROUP BY product_id;
```
</p>


