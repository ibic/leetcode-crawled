---
title: "Find K Pairs with Smallest Sums"
weight: 356
#id: "find-k-pairs-with-smallest-sums"
---
## Description
<div class="description">
<p>You are given two integer arrays <b>nums1</b> and <b>nums2</b> sorted in ascending order and an integer <b>k</b>.</p>

<p>Define a pair <b>(u,v)</b> which consists of one element from the first array and one element from the second array.</p>

<p>Find the k pairs <b>(u<sub>1</sub>,v<sub>1</sub>),(u<sub>2</sub>,v<sub>2</sub>) ...(u<sub>k</sub>,v<sub>k</sub>)</b> with the smallest sums.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>nums1 = <span id="example-input-1-1">[1,7,11]</span>, nums2 = <span id="example-input-1-2">[2,4,6]</span>, k = <span id="example-input-1-3">3</span>
<strong>Output: </strong><span id="example-output-1">[[1,2],[1,4],[1,6]] 
<strong>Explanation: </strong></span>The first 3 pairs are returned from the sequence: 
&nbsp;            [1,2],[1,4],[1,6],[7,2],[7,4],[11,2],[7,6],[11,4],[11,6]</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>nums1 = [1,1,2], nums2 = [1,2,3], k = 2
<strong>Output: </strong>[1,1],[1,1]<span>
<strong>Explanation: </strong></span>The first 2 pairs are returned from the sequence: 
&nbsp;            [1,1],[1,1],[1,2],[2,1],[1,2],[2,2],[1,3],[1,3],[2,3]</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>nums1 = [1,2], nums2 = [3], k = 3
<strong>Output: </strong>[1,3],[2,3]<span>
<strong>Explanation: </strong></span>All possible pairs are returned from the sequence: [1,3],[2,3]
</pre>

</div>

## Tags
- Heap (heap)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- LinkedIn - 5 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Facebook - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Uber - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### simple Java O(KlogK) solution with explanation
- Author: bbccyy1
- Creation Date: Tue Jul 12 2016 06:37:29 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 11:48:30 GMT+0800 (Singapore Standard Time)

<p>
Basic idea: Use min_heap to keep track on next minimum pair sum, and we only need to maintain K possible candidates in the data structure.

Some observations: For every numbers in nums1, its best partner(yields min sum) always strats from nums2[0] since arrays are all sorted; And for a specific number in nums1, its next candidate sould be **[this specific number]** + **nums2[current_associated_index + 1]**, unless out of boundary;)

Here is a simple example demonstrate how this algorithm works.

![image](https://cloud.githubusercontent.com/assets/8743900/17332795/0bb46cfe-589e-11e6-90b5-5d3c9696c4f0.png)


The run time complexity is O(kLogk) since que.size <= k and we do at most k loop.

```
public class Solution {
    public List<int[]> kSmallestPairs(int[] nums1, int[] nums2, int k) {
        PriorityQueue<int[]> que = new PriorityQueue<>((a,b)->a[0]+a[1]-b[0]-b[1]);
        List<int[]> res = new ArrayList<>();
        if(nums1.length==0 || nums2.length==0 || k==0) return res;
        for(int i=0; i<nums1.length && i<k; i++) que.offer(new int[]{nums1[i], nums2[0], 0});
        while(k-- > 0 && !que.isEmpty()){
            int[] cur = que.poll();
            res.add(new int[]{cur[0], cur[1]});
            if(cur[2] == nums2.length-1) continue;
            que.offer(new int[]{cur[0],nums2[cur[2]+1], cur[2]+1});
        }
        return res;
    }
}
```
</p>


### Slow 1-liner to Fast solutions
- Author: StefanPochmann
- Creation Date: Thu Jul 07 2016 16:24:37 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 14 2020 02:47:21 GMT+0800 (Singapore Standard Time)

<p>
Several solutions from naive to more elaborate. I found it helpful to visualize the input as an **m\xD7n matrix** of sums, for example for nums1=[1,7,11], and nums2=[2,4,6]:

          2   4   6
       +------------
     1 |  3   5   7
     7 |  9  11  13
    11 | 13  15  17

Of course the smallest pair overall is in the top left corner, the one with sum 3. We don\'t even need to look anywhere else. After including that pair in the output, the next-smaller pair must be the next on the right (sum=5) or the next below (sum=9). We can keep a "horizon" of possible candidates, implemented as a heap / priority-queue, and roughly speaking we\'ll grow from the top left corner towards the right/bottom. That\'s what my solution 5 does. Solution 4 is similar, not quite as efficient but a lot shorter and my favorite.
<br>

## **Solution 1: Brute Force** <sup>(accepted in 560 ms)</sup>

Just produce all pairs, sort them by sum, and return the first k.

    def kSmallestPairs(self, nums1, nums2, k):
        return sorted(itertools.product(nums1, nums2), key=sum)[:k]

## **Solution 2: Clean Brute Force** <sup>(accepted in 532 ms)</sup>

The above produces tuples and while the judge doesn\'t care, it\'s cleaner to make them lists as requested:

    def kSmallestPairs(self, nums1, nums2, k):
        return map(list, sorted(itertools.product(nums1, nums2), key=sum)[:k])

## **Solution 3: Less Brute Force** <sup>(accepted in 296 ms)</sup>

Still going through all pairs, but only with a generator and `heapq.nsmallest`, which uses a heap of size k. So this only takes O(k) extra memory and O(mn log k) time.

    def kSmallestPairs(self, nums1, nums2, k):
        return map(list, heapq.nsmallest(k, itertools.product(nums1, nums2), key=sum))

Or (accepted in 368 ms):

    def kSmallestPairs(self, nums1, nums2, k):
        return heapq.nsmallest(k, ([u, v] for u in nums1 for v in nums2), key=sum)

## **Solution 4: Efficient**  <sup>(accepted in 112 ms)</sup>

The brute force solutions computed the whole matrix (see visualization above). This solution doesn\'t. It turns each row into a generator of triples [u+v, u, v], only computing the next when asked for one. And then merges these generators with a heap. Takes O(m + k\*log(m)) time and O(m) extra space.

    def kSmallestPairs(self, nums1, nums2, k):
        streams = map(lambda u: ([u+v, u, v] for v in nums2), nums1)
        stream = heapq.merge(*streams)
        return [suv[1:] for suv in itertools.islice(stream, k)]

## **Solution 5: More efficient**  <sup>(accepted in 104 ms)</sup>

The previous solution right away considered (the first pair of) all matrix rows (see visualization above). This one doesn\'t. It starts off only with the very first pair at the top-left corner of the matrix, and expands from there as needed. Whenever a pair is chosen into the output result, the next pair in the row gets added to the priority queue of current options. Also, if the chosen pair is the first one in its row, then the first pair in the next row is added to the queue.

Here\'s a visualization of a possible state:
```
# # # # # ? . .
# # # ? . . . .
# ? . . . . . .   "#" means pair already in the output
# ? . . . . . .   "?" means pair currently in the queue
# ? . . . . . .
? . . . . . . .
. . . . . . . .
```
As I mentioned in the comments, that could be further improved. Two of those `?` don\'t actually need to be in the queue yet. I\'ll leave that as an exercise for the reader :-)
```
    def kSmallestPairs(self, nums1, nums2, k):
        queue = []
        def push(i, j):
            if i < len(nums1) and j < len(nums2):
                heapq.heappush(queue, [nums1[i] + nums2[j], i, j])
        push(0, 0)
        pairs = []
        while queue and len(pairs) < k:
            _, i, j = heapq.heappop(queue)
            pairs.append([nums1[i], nums2[j]])
            push(i, j + 1)
            if j == 0:
                push(i + 1, 0)
        return pairs
</p>


### Clean 16ms C++ O(N) Space O(KlogN) Time Solution using Priority queue
- Author: fentoyal
- Creation Date: Thu Jul 07 2016 22:19:39 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 30 2018 03:40:45 GMT+0800 (Singapore Standard Time)

<p>
Here, N = min(k, n).
          K = min(k, mn) 
        where m, n is the size of two arrays and k is the k in the problem.
```
class Solution {
public:
    vector<pair<int, int>> kSmallestPairs(vector<int>& nums1, vector<int>& nums2, int k) {
        vector<pair<int,int>> result;
        if (nums1.empty() || nums2.empty() || k <= 0)
            return result;
        auto comp = [&nums1, &nums2](pair<int, int> a, pair<int, int> b) {
            return nums1[a.first] + nums2[a.second] > nums1[b.first] + nums2[b.second];};
        priority_queue<pair<int, int>, vector<pair<int, int>>, decltype(comp)> min_heap(comp);
        min_heap.emplace(0, 0);
        while(k-- > 0 && min_heap.size())
        {
            auto idx_pair = min_heap.top(); min_heap.pop();
            result.emplace_back(nums1[idx_pair.first], nums2[idx_pair.second]);
            if (idx_pair.first + 1 < nums1.size())
                min_heap.emplace(idx_pair.first + 1, idx_pair.second);
            if (idx_pair.first == 0 && idx_pair.second + 1 < nums2.size())
                min_heap.emplace(idx_pair.first, idx_pair.second + 1);
        }
        return result;
    }
};
```
</p>


