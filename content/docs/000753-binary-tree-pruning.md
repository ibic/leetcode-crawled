---
title: "Binary Tree Pruning"
weight: 753
#id: "binary-tree-pruning"
---
## Description
<div class="description">
<p>We are given the head node <code>root</code>&nbsp;of a binary tree, where additionally every node&#39;s value is either a 0 or a 1.</p>

<p>Return the same tree where every subtree (of the given tree) not containing a 1 has been removed.</p>

<p>(Recall that the subtree of a node X is X, plus every node that is a descendant of X.)</p>

<pre>
<strong>Example 1:</strong>
<strong>Input:</strong> [1,null,0,0,1]
<strong>Output: </strong>[1,null,0,null,1]
 
<strong>Explanation:</strong> 
Only the red nodes satisfy the property &quot;every subtree not containing a 1&quot;.
The diagram on the right represents the answer.

<img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/04/06/1028_2.png" style="width:450px" />
</pre>

<pre>
<strong>Example 2:</strong>
<strong>Input:</strong> [1,0,1,0,0,0,1]
<strong>Output: </strong>[1,null,1,null,1]


<img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/04/06/1028_1.png" style="width:450px" />
</pre>

<pre>
<strong>Example 3:</strong>
<strong>Input:</strong> [1,1,0,1,1,0,1,0]
<strong>Output: </strong>[1,1,0,1,1,null,1]


<img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/04/05/1028.png" style="width:450px" />
</pre>

<p><strong>Note: </strong></p>

<ul>
	<li>The binary tree&nbsp;will&nbsp;have&nbsp;at&nbsp;most <code>200 nodes</code>.</li>
	<li>The value of each node will only be <code>0</code> or <code>1</code>.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Capital One - 7 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Hulu - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Recursion [Accepted]

**Intuition**

Prune children of the tree recursively.  The only decisions at each node are whether to prune the left child or the right child.

**Algorithm**

We'll use a function `containsOne(node)` that does two things: it tells us whether the subtree at this `node` contains a `1`, and it also prunes all subtrees not containing `1`.

If for example, `node.left` does not contain a one, then we should prune it via `node.left = null`.

Also, the parent needs to be checked.  If for example the tree is a single node `0`, the answer is an empty tree.

<iframe src="https://leetcode.com/playground/Xd8odLWd/shared" frameBorder="0" width="100%" height="293" name="Xd8odLWd"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the tree.  We process each node once.

* Space Complexity: $$O(H)$$, where $$H$$ is the height of the tree.  This represents the size of the implicit call stack in our recursion.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python]  Self-Explaining Solution and 2-lines
- Author: lee215
- Creation Date: Sun Apr 08 2018 11:10:53 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 24 2018 11:36:15 GMT+0800 (Singapore Standard Time)

<p>
Of course. We should take care about free memory.
But in my opinion, we can delete a node only if we know it was allocated with new, right?
So in my cpp, I didn\'t do this part.
My rule is that, If you don\'t new it, don\'t delete it.

**Recursive Solution, very self-explaining**
```
if root == null: return null
root.left = pruneTree(root.left)
root.right = pruneTree(root.right)
if root.left == null and root.right == null and root.val == 0: return null
return root
```

**C++**
```
    TreeNode* pruneTree(TreeNode* root) {
        if (!root) return NULL;
        root->left = pruneTree(root->left);
        root->right = pruneTree(root->right);
        if (!root->left && !root->right && root->val == 0) return NULL;
        return root;
    }
```
**Java:**
```
    public TreeNode pruneTree(TreeNode root) {
        if (root == null) return null;
        root.left = pruneTree(root.left);
        root.right = pruneTree(root.right);
        if (root.left == null && root.right == null && root.val == 0) return null;
        return root
    }
```
**Python**
```
    def pruneTree(self, root):
        if not root: return None
        root.left = self.pruneTree(root.left)
        root.right = self.pruneTree(root.right)
        if not root.left and not root.right and not root.val: return None
        return root
```
**If you like less lines:**
**2-lines C++**
```
    TreeNode* pruneTree(TreeNode* root) {
        if (root) root->left = pruneTree(root->left), root->right = pruneTree(root->right);
        return (root && (root->left || root->right || root->val)) ? root : NULL;
    }
```


**2-lines Python**
```
    def pruneTree(self, root):
        if root: root.left, root.right = self.pruneTree(root.left), self.pruneTree(root.right)
        if root and (root.left or root.right or root.val): return root
```
</p>


### Java 4 lines Solution using Recursion
- Author: myCafeBabe
- Creation Date: Sun Apr 08 2018 11:19:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 02 2018 22:41:36 GMT+0800 (Singapore Standard Time)

<p>
This is a kind of divide and conquer process. Beginning from the bottom, for null nodes we just return null. If the node is 1, or if there is any child of it is not null, we return itself, otherwise we return null. In this way, only when a node has two null children, and itself is also 0, we return null and the null will be assigned to its parent\u2019s left/right, so we pruned the tree.
```
class Solution {
    public TreeNode pruneTree(TreeNode root) {
        if (root == null) return null;
        root.left = pruneTree(root.left);
        root.right = pruneTree(root.right);
        return (root.val == 1 || root.left != null || root.right != null) ? root : null;
    }
}
```
</p>


### 3 recursive lines dedicated to python
- Author: Hemant_323
- Creation Date: Sun Apr 08 2018 11:25:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 08 2018 11:25:02 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def pruneTree(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        if not root: return root
        root.left, root.right = self.pruneTree(root.left), self.pruneTree(root.right)
        return None if not root.val and not root.left and not root.right else root
```
</p>


