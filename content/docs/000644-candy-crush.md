---
title: "Candy Crush"
weight: 644
#id: "candy-crush"
---
## Description
<div class="description">
<p>This question is about implementing a basic elimination algorithm for Candy Crush.</p>

<p>Given a 2D integer array <code>board</code> representing the grid of candy, different positive integers <code>board[i][j]</code> represent different types of candies. A value of <code>board[i][j] = 0</code> represents that the cell at position <code>(i, j)</code> is empty. The given board represents the state of the game following the player&#39;s move. Now, you need to restore the board to a <i>stable state</i> by crushing candies according to the following rules:</p>

<ol>
	<li>If three or more candies of the same type are adjacent vertically or horizontally, &quot;crush&quot; them all at the same time - these positions become empty.</li>
	<li>After crushing all candies simultaneously, if an empty space on the board has candies on top of itself, then these candies will drop until they hit a candy or bottom at the same time. (No new candies will drop outside the top boundary.)</li>
	<li>After the above steps, there may exist more candies that can be crushed. If so, you need to repeat the above steps.</li>
	<li>If there does not exist more candies that can be crushed (ie. the board is <i>stable</i>), then return the current board.</li>
</ol>

<p>You need to perform the above rules until the board becomes stable, then return the current board.</p>

<p>&nbsp;</p>

<p><b>Example:</b></p>

<pre style="white-space: pre-line">
<b>Input:</b>
board = 
[[110,5,112,113,114],[210,211,5,213,214],[310,311,3,313,314],[410,411,412,5,414],[5,1,512,3,3],[610,4,1,613,614],[710,1,2,713,714],[810,1,2,1,1],[1,1,2,2,2],[4,1,4,4,1014]]

<b>Output:</b>
[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[110,0,0,0,114],[210,0,0,0,214],[310,0,0,113,314],[410,0,0,213,414],[610,211,112,313,614],[710,311,412,613,714],[810,411,512,713,1014]]

<b>Explanation:</b> 
<img src="https://assets.leetcode.com/uploads/2018/10/12/candy_crush_example_2.png" style="width: 777px; height: 532px;" />
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>The length of <code>board</code> will be in the range [3, 50].</li>
	<li>The length of <code>board[i]</code> will be in the range [3, 50].</li>
	<li>Each <code>board[i][j]</code> will initially start as an integer in the range [1, 2000].</li>
</ol>

</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)

## Companies
- Bloomberg - 11 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Twitch - 2 (taggedByAdmin: false)
- Rubrik - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Ad-Hoc 

**Intuition**

We need to simply perform the algorithm as described.  It consists of two major steps: a crush step, and a gravity step.  We work through each step individually.

**Algorithm**

*Crushing Step*

When crushing, one difficulty is that we might accidentally crush candy that is part of another row.  For example, if the board is:

```python
123
145
111
```
and we crush the vertical row of `1`s early, we may not see there was also a horizontal row.

To remedy this, we should flag candy that should be crushed first.  We could use an auxillary `toCrush` boolean array, or we could mark it directly on the board by making the entry negative (ie. `board[i][j] = -Math.abs(board[i][j])`)

As for how to scan the board, we have two approaches.  Let's call a *line* any row or column of the board.  

For each line, we could use a sliding window (or `itertools.groupby` in Python) to find contiguous segments of the same character.  If any of these segments have length 3 or more, we should flag them.

Alternatively, for each line, we could look at each width-3 slice of the line: if they are all the same, then we should flag those 3.

After, we can crush the candy by setting all flagged `board` cells to zero.

*Gravity Step*

For each column, we want all the candy to go to the bottom.  One way is to iterate through and keep a stack of the (uncrushed) candy, popping and setting as we iterate through the column in reverse order.

Alternatively, we could use a sliding window approach, maintaining a read and write head.  As the read head iterates through the column in reverse order, when the read head sees candy, the write head will write it down and move one place.  Then, the write head will write zeroes to the remainder of the column.

We showcase the simplest approaches to these steps in the solutions below.

<iframe src="https://leetcode.com/playground/WktHXw9m/shared" frameBorder="0" width="100%" height="500" name="WktHXw9m"></iframe>
**Complexity Analysis**

* Time Complexity: $$O((R*C)^2)$$, where $$R, C$$ is the number of rows and columns in `board`.  We need $$O(R*C)$$ to scan the board, and we might crush only 3 candies repeatedly.

* Space Complexity: $$O(1)$$ additional complexity, as we edit the board in place.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 15 ms Short Java Solution - Mark crush with negative value
- Author: kekezi
- Creation Date: Fri Feb 09 2018 07:42:39 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 11:19:17 GMT+0800 (Singapore Standard Time)

<p>
The idea is to traverse the entire matrix again and again to remove crush until no crush can be found.

For each traversal of the matrix, we only check two directions, rightward and downward. There is no need to check upward and leftward because they would have been checked from previous cells. 

For each cell, if there are at least three candies of the same type rightward or downward, set them all to its negative value to mark them.

After each traversal, we need to remove all those negative values by setting them to 0, then let the rest drop down to their correct position. A easy way is to iterate through each column, for each column, move positive values to the bottom then set the rest to 0.

```
class Solution {
    public int[][] candyCrush(int[][] board) {
        int N = board.length, M = board[0].length;
        boolean found = true;
        while (found) {
            found = false;
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    int val = Math.abs(board[i][j]);
                    if (val == 0) continue;
                    if (j < M - 2 && Math.abs(board[i][j + 1]) == val && Math.abs(board[i][j + 2]) == val) {
                        found = true;
                        int ind = j;
                        while (ind < M && Math.abs(board[i][ind]) == val) board[i][ind++] = -val;
                    }
                    if (i < N - 2 && Math.abs(board[i + 1][j]) == val && Math.abs(board[i + 2][j]) == val) {
                        found = true;
                        int ind = i;
                        while (ind < N && Math.abs(board[ind][j]) == val) board[ind++][j] = -val;           
                    }
                }
            }
            if (found) { // move positive values to the bottom, then set the rest to 0
                for (int j = 0; j < M; j++) {
                    int storeInd = N - 1;
                    for (int i = N - 1; i >= 0; i--) {
                        if (board[i][j] > 0) {
                            board[storeInd--][j] = board[i][j];
                        }
                    }
                    for (int k = storeInd; k >= 0; k--) board[k][j] = 0;
                }
            }
        }
        return board;
    }
}
```
</p>


### Another Java Solution
- Author: jeantimex
- Creation Date: Sun Oct 07 2018 08:49:26 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 15:25:40 GMT+0800 (Singapore Standard Time)

<p>
I found this solution from http://storypku.com/2017/11/leetcode-question-723-candy-crush/, and I convert it to Java. It\'s not the shortest, but the logic is clear to me.

The idea is that we try to crush the candy horizontally, then vertically, and drop them vertically (because we have to fill the empty spots). The way we mark whether a candy needs to be crushed(set to 0) is to set an opposite value to it, so that we don\'t have to maintain another data structure.

```java
public int[][] candyCrush(int[][] board) {
  int m = board.length;
  int n = board[0].length;

  boolean shouldContinue = false;

  // Crush horizontally
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n - 2; j++) {
      int v = Math.abs(board[i][j]);
      if (v > 0 && v == Math.abs(board[i][j + 1]) && v == Math.abs(board[i][j + 2])) {
        board[i][j] = board[i][j + 1] = board[i][j + 2] = -v;
        shouldContinue = true;
      }
    }
  }

  // Crush vertically
  for (int i = 0; i < m - 2; i++) {
    for (int j = 0; j < n; j++) {
      int v = Math.abs(board[i][j]);
      if (v > 0 && v == Math.abs(board[i + 1][j]) && v == Math.abs(board[i + 2][j])) {
        board[i][j] = board[i + 1][j] = board[i + 2][j] = -v;
        shouldContinue = true;
      }
    }
  }

  // Drop vertically
  for (int j = 0; j < n; j++) {
    int r = m - 1;
    for (int i = m - 1; i >= 0; i--) {
      if (board[i][j] >= 0) {
        board[r--][j] = board[i][j];
      }
    }
    for (int i = r; i >= 0; i--) {
      board[i][j] = 0;
    }
  }

  return shouldContinue ? candyCrush(board) : board;
}
```
</p>


### Short Python
- Author: jingkuan
- Creation Date: Fri Nov 09 2018 12:05:30 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Nov 09 2018 12:05:30 GMT+0800 (Singapore Standard Time)

<p>
Just Brute Force
```
class Solution:
    def candyCrush(self, M):
        while True:
            # 1, Check
            crush = set()
            for i in range(len(M)):
                for j in range(len(M[0])):
                    if j > 1 and M[i][j] and M[i][j] == M[i][j - 1] == M[i][j - 2]:
                        crush |= {(i, j), (i, j - 1), (i, j - 2)}
                    if i > 1 and M[i][j] and M[i][j] == M[i - 1][j] == M[i - 2][j]:
                        crush |= {(i, j), (i - 1, j), (i - 2, j)}

            # 2, Crush
            if not crush: break
            for i, j in crush: M[i][j] = 0

            # 3, Drop
            for j in range(len(M[0])):
                idx = len(M) - 1
                for i in reversed(range(len(M))):
                    if M[i][j]: M[idx][j] = M[i][j]; idx -= 1
                for i in range(idx + 1): M[i][j] = 0
        return M
```
</p>


