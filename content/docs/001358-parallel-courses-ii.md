---
title: "Parallel Courses II"
weight: 1358
#id: "parallel-courses-ii"
---
## Description
<div class="description">
<p>Given the integer <code>n</code> representing the number of courses at some university labeled from <code>1</code> to <code>n</code>, and the array <code>dependencies</code> where <code>dependencies[i] = [x<sub>i</sub>, y<sub>i</sub>]</code> &nbsp;represents a prerequisite relationship, that is, the course <code>x<sub>i</sub></code>&nbsp;must be taken before the course <code>y<sub>i</sub></code>. &nbsp;Also, you are given the&nbsp;integer <code>k</code>.</p>

<p>In one semester you can take <strong>at most</strong> <code>k</code> courses as long as you have taken all the prerequisites for the courses you are taking.</p>

<p><em>Return the minimum number of semesters to take all courses</em>.&nbsp;It is guaranteed that you can take all courses in some way.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/05/22/leetcode_parallel_courses_1.png" style="width: 300px; height: 164px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 4, dependencies = [[2,1],[3,1],[1,4]], k = 2
<strong>Output:</strong> 3 
<strong>Explanation:</strong> The figure above represents the given graph. In this case we can take courses 2 and 3 in the first semester, then take course 1 in the second semester and finally take course 4 in the third semester.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/05/22/leetcode_parallel_courses_2.png" style="width: 300px; height: 234px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 5, dependencies = [[2,1],[3,1],[4,1],[1,5]], k = 2
<strong>Output:</strong> 4 
<strong>Explanation:</strong> The figure above represents the given graph. In this case one optimal way to take all courses is: take courses 2 and 3 in the first semester and take course 4 in the second semester, then take course 1 in the third semester and finally take course 5 in the fourth semester.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 11, dependencies = [], k = 2
<strong>Output:</strong> 6
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 15</code></li>
	<li><code>1 &lt;= k &lt;= n</code></li>
	<li><code>0 &lt;=&nbsp;dependencies.length &lt;= n * (n-1) / 2</code></li>
	<li><code>dependencies[i].length == 2</code></li>
	<li><code>1 &lt;= x<sub>i</sub>, y<sub>i</sub>&nbsp;&lt;= n</code></li>
	<li><code>x<sub>i</sub> != y<sub>i</sub></code></li>
	<li>All prerequisite relationships are distinct, that is, <code>dependencies[i] != dependencies[j]</code>.</li>
	<li>The given graph is a directed acyclic graph.</li>
</ul>

</div>

## Tags
- Graph (graph)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Weak test case, most solutions posted using depth or outdgree are wrong
- Author: flyingpenguin
- Creation Date: Sun Jun 28 2020 01:36:41 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 06 2020 05:48:46 GMT+0800 (Singapore Standard Time)

<p>
6
[[2,5],[1,5],[3,5],[3,4],[3,6]]
2

output should be 3:

take 3,1 first

then 2,4

then 5,6

sort by depth will give you 4, in this order

1,2
3
4,5
6

Sort by out degree is outright wrong as others mentiond.

This is an NP problem with 3^n complexity. n ==15 after all...  

https://leetcode.com/problems/parallel-courses-ii/discuss/708263/Can-anyone-explain-the-bit-mask-method

My own Java translation according to the approach in the above post with some comment

```
class Solution {
   
    public int minNumberOfSemesters(int n, int[][] deps, int k) {
        int[] preq = new int[n];
        for (int[] dep : deps) {
            // to study j, what are the prerequisites?  each set bit is a class that we need to take. ith bit means ith class
            // -1 because classes are 1 to n
            preq[dep[1] - 1] |= 1 << (dep[0] - 1);
        }
        int[] dp = new int[1 << n];
        Arrays.fill(dp, n);
        dp[0] = 0;
        for (int i = 0; i < (1 << n); i++) {
            // we are now at status i. we can "influence" a later status from this status
            int canStudy = 0;   // what are the classes we can study?
            for (int j = 0; j < n; j++) {
                // a & b== b means b is a\'s subset
                // so if preq[j] is i\'s subset, we can now study j given status i
                if ((i & preq[j]) == preq[j]) {
                    canStudy |= (1 << j);
                }
            }
            canStudy &= ~i;
            // take out i, so that we only enumerate a subset canStudy without i.
            // note we will | later so here we need a set that has no intersection with i to reduce the enumeration cost
            for (int sub = canStudy; sub > 0; sub = (sub - 1) & canStudy) {
                // we can study one or more courses indicated by set "canStudy". we need to enumerate all non empty subset of it. 
				// This for loop is a typical way to enumerate all subsets of a given set "canStudy"
                // we studied i using dp[i] semesters. now if we also study the subset sub, we need dp [i ]+1 semesters, 
                // and the status we can "influence" is dp[ i | sub] because at that state, we studied what we want to study in "sub" 
                if (Integer.bitCount(sub) <= k) {
                    dp[i | sub] = Math.min(dp[i | sub], dp[i] + 1);
                }
            }
        }
        return dp[(1 << n) - 1];
    }
}
```

P.S. if you think you can greedy it in another way, try these  out first :)

6
[[2,5],[1,5],[3,5],[3,4],[3,6]]
2

10
[[1,5],[1,6],[1,7],[2,5],[2,6],[2,7],[9,10],[10,5],[10,6],[10,7],[10,8],[3,5],[3,6],[4,5],[4,6]]
2

(credit to @ddoudle)
9
[[1,4],[1,5],[3,5],[3,6],[2,6],[2,7],[8,4],[8,5],[9,6],[9,7]]
3

(credit to @s_tsat)
12
[[1,2],[2,3],[4,5],[5,6],[7,8],[8,9],[10,11],[11,12]]
3

(credit to @changeme)
6
[[2,1],[5,1],[3,1],[3,4],[3,6]]
2
</p>


### Can anyone explain the bit mask method?
- Author: Hideidforawhile
- Creation Date: Sun Jun 28 2020 00:24:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 00:24:14 GMT+0800 (Singapore Standard Time)

<p>
I saw that bit mask method has been used by some top players
Can anyone explain?

Rank6 	Heltion  :
```
class Solution {
public:
    int minNumberOfSemesters(int n, vector<vector<int>>& dependencies, int k) {
        vector<int> pre(n);
        for(auto& e : dependencies){
            e[0] -= 1;
            e[1] -= 1;
            pre[e[1]] |= 1 << e[0];
        }
        vector<int> dp(1 << n, n);
        dp[0] = 0;
        for(int i = 0; i < (1 << n); i += 1){
            int ex = 0;
            for(int j = 0; j < n; j += 1) if((i & pre[j]) == pre[j]) ex |= 1 << j;
            ex &= ~i;
            for(int s = ex; s; s = (s - 1) & ex) if(__builtin_popcount(s) <= k){
                dp[i | s] = min(dp[i | s], dp[i] + 1);
            }
        }
        return dp.back();
    }
};
```
</p>


### C++ O(3^n) bitmask  dynamic programming code with comments and tutorial
- Author: Wonter
- Creation Date: Sun Jun 28 2020 14:09:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 14:09:09 GMT+0800 (Singapore Standard Time)

<p>
```cpp
class Solution {
public:
    int minNumberOfSemesters(int n, vector<vector<int>>& dependencies, int k) {
        // dependency[i]: dependency mask of course i, the set bits is dependent
        vector<int> dependency(n, 0);
        for (size_t i = 0; i < dependencies.size(); ++i) {
            int course = dependencies[i][1] - 1;
            int prerequisite = dependencies[i][0] - 1;
            dependency[course] |= 1 << prerequisite;
        }

        // prerequisites[i]: prerequisites mask of mask i, the set bits is prerequisites
        vector<int> prerequisites(1 << n, 0);
        // iterate all mask and generate prerequisites mask of each mask
        for (int i = 0; i < (1 << n); ++i) {
            for (int j = 0; j < n; ++j) {
                if (i & (1 << j)) {
                    prerequisites[i] |= dependency[j];
                }
            }
        }

        // dp[i]: minimum number of semesters of mask i, the set bits are courses that have not been taken
        vector<int> dp(1 << n, n + 1);
        dp[0] = 0;
        for (int i = 1; i < (1 << n); ++i) {
            // iterate all submask of mask i, and this mask is the mask of last semester
            // see: https://cp-algorithms.com/algebra/all-submasks.html
            for (int j = i; j; j = (j - 1) & i) {
                if (count_setbit(j) > k) {
                    continue;
                }

                int already_taken = i ^ ((1 << n) - 1);
                if ((already_taken & prerequisites[j]) == prerequisites[j]) {
                    dp[i] = min(dp[i], dp[i ^ j] + 1);
                }
            }
        }

        return dp[(1 << n) - 1];
    }

private:
    int count_setbit(int mask) {
        if (mask == 0) {
            return 0;
        }
        return 1 + count_setbit(mask & (mask - 1));
    }
};
```
</p>


