---
title: "Smallest Subsequence of Distinct Characters"
weight: 1060
#id: "smallest-subsequence-of-distinct-characters"
---
## Description
<div class="description">
<p>Return the lexicographically smallest subsequence of <code>s</code> that contains all the distinct characters of <code>s</code> exactly once.</p>

<p><strong>Note:</strong> This question is the same as 316:&nbsp;<a href="https://leetcode.com/problems/remove-duplicate-letters/" target="_blank">https://leetcode.com/problems/remove-duplicate-letters/</a></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;bcabc&quot;
<strong>Output:</strong> &quot;abc&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;cbacdcbc&quot;
<strong>Output:</strong> &quot;acdb&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 1000</code></li>
	<li><code>s</code> consists of lowercase English letters.</li>
</ul>

</div>

## Tags
- String (string)
- Stack (stack)
- Greedy (greedy)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- FactSet - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Stack Solution O(N)
- Author: lee215
- Creation Date: Sun Jun 09 2019 12:03:55 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Nov 29 2019 23:30:57 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**:
Find the index of last occurrence for each character.
Use a `stack` to keep the characters for result.
Loop on each character in the input string `S`,
if the current character is smaller than the last character in the stack,
and the last character exists in the following stream,
we can pop the last character to get a smaller result.
<br>

## **Time Complexity**:
Time `O(N)`
Space `O(26)`

<br>

**Java:**
```java
    public String smallestSubsequence(String S) {
        Stack<Integer> stack = new Stack<>();
        int[] last = new int[26], seen = new int[26];
        for (int i = 0; i < S.length(); ++i)
            last[S.charAt(i) - \'a\'] = i;
        for (int i = 0; i < S.length(); ++i) {
            int c = S.charAt(i) - \'a\';
            if (seen[c]++ > 0) continue;
            while (!stack.isEmpty() && stack.peek() > c && i < last[stack.peek()])
                seen[stack.pop()] = 0;
            stack.push(c);
        }
        StringBuilder sb = new StringBuilder();
        for (int i : stack) sb.append((char)(\'a\' + i));
        return sb.toString();
    }
```

**C++**
```cpp
    string smallestSubsequence(string s) {
        string res = "";
        int last[26] = {}, seen[26] = {}, n = s.size();
        for (int i = 0; i < n; ++i)
            last[s[i] - \'a\'] = i;
        for (int i = 0; i < n; ++i) {
            if (seen[s[i] - \'a\']++) continue;
            while (!res.empty() && res.back() > s[i] && i < last[res.back() - \'a\'])
                seen[res.back() - \'a\'] = 0, res.pop_back();
            res.push_back(s[i]);
        }
        return res;
    }
```

**Python:**
```py
    def smallestSubsequence(self, S):
        last = {c: i for i, c in enumerate(S)}
        stack = []
        for i, c in enumerate(S):
            if c in stack: continue
            while stack and stack[-1] > c and i < last[stack[-1]]:
                stack.pop()
            stack.append(c)
        return "".join(stack)
```
<br>

# More Good Stack Problems
Here are some problems that impressed me.
Good luck and have fun.

- 1130. [Minimum Cost Tree From Leaf Values](https://leetcode.com/problems/minimum-cost-tree-from-leaf-values/discuss/339959/One-Pass-O(N)-Time-and-Space)
- 907. [Sum of Subarray Minimums](https://leetcode.com/problems/sum-of-subarray-minimums/discuss/170750/C++JavaPython-Stack-Solution)
- 901. [Online Stock Span](https://leetcode.com/problems/online-stock-span/discuss/168311/C++JavaPython-O(1))
- 856. [Score of Parentheses](https://leetcode.com/problems/score-of-parentheses/discuss/141777/C++JavaPython-O(1)-Space)
- 503. [Next Greater Element II](https://leetcode.com/problems/next-greater-element-ii/discuss/98270/JavaC++Python-Loop-Twice)
- 496. Next Greater Element I
- 84. Largest Rectangle in Histogram
- 42. Trapping Rain Water
<br>
</p>


### C++ O(n), identical to #316
- Author: votrubac
- Creation Date: Sun Jun 09 2019 12:01:40 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 13 2019 00:57:41 GMT+0800 (Singapore Standard Time)

<p>
The solution is exactly the same as for [316. Remove Duplicate Letters](https://leetcode.com/problems/remove-duplicate-letters).

From the input string, we are trying to greedily build a monotonically increasing result string. If the next character is smaller than the back of the result string, we remove larger characters from the back **providing** there are more occurrences of that character **later** in the input string.
```
string smallestSubsequence(string s, string res = "") {
  int cnt[26] = {}, used[26] = {};
  for (auto ch : s) ++cnt[ch - \'a\'];
  for (auto ch : s) {
    --cnt[ch - \'a\'];
    if (used[ch - \'a\']++ > 0) continue;
    while (!res.empty() && res.back() > ch && cnt[res.back() - \'a\'] > 0) {
      used[res.back() - \'a\'] = 0;
      res.pop_back();
    }
    res.push_back(ch);
  }
  return res;
}   
```
## Complexity Analysis
Runtime: O(n). We process each input character no more than 2 times.
Memory: O(1). We need the constant memory to track up to 26 unique letters.
</p>


### show my thinking process
- Author: bupt_wc
- Creation Date: Sun Jun 09 2019 12:05:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 09 2019 12:05:57 GMT+0800 (Singapore Standard Time)

<p>
consider s = \'ecbacaba\', ans = \'eacb\'
why the first letter ans is not \'a\'? since there are only one \'e\' and its index is smaller than the first \'a\'
Now we find the key point!

for example, s = \'cdadabcc\', `s` contains `a, b, c, d`
index[\'a\'] = [2, 4]
index[\'b\'] = [5]
index[\'c\'] = [0, 6, 7]
index[\'d\'] = [1, 3]

which will be the first letter in ans?
it will be letter \'a\', cause `2 < min(5, 7, 3)`, (5,7,3) is the `last index` of other letters

after append \'a\', now the index becomes
index[\'b\'] = [5]
index[\'c\'] = [6, 7]
index[\'d\'] = [3]
`we delete all index less than 2`
obviously, the next letter will be \'d\'.
analyse one by one.

```python
class Solution:
    def smallestSubsequence(self, text: str) -> str:
        n = len(text)
        d = collections.defaultdict(collections.deque)
        # record index
        for i,v in enumerate(text):
            d[ord(v)-ord(\'a\')].append(i)
        
        # search orderly
        keys = sorted(d.keys())
        res = []
        c = len(d)
        last_index = -1
        while len(res) < c: # len(res) will not larger than 26
            # O(26*26) time, search the first smallest letter
            for i in range(len(keys)):
                if all(d[keys[i]][0] < d[keys[j]][-1] for j in range(i+1, len(keys))):
                    res.append(chr(ord(\'a\')+keys[i]))
                    last_index = d[keys[i]][0]
                    keys.remove(keys[i])
                    break
            # O(n) time, delete all index less than last_index
            for i in range(len(keys)):
                while d[keys[i]] and d[keys[i]][0] < last_index:
                    d[keys[i]].popleft()
  
        return \'\'.join(res)
```
</p>


