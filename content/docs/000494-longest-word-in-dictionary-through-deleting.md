---
title: "Longest Word in Dictionary through Deleting"
weight: 494
#id: "longest-word-in-dictionary-through-deleting"
---
## Description
<div class="description">
<p>
Given a string and a string dictionary, find the longest string in the dictionary that can be formed by deleting some characters of the given string. If there are more than one possible results, return the longest word with the smallest lexicographical order. If there is no possible result, return the empty string.
</p>
<p><b>Example 1:</b><br>
<pre>
<b>Input:</b>
s = "abpcplea", d = ["ale","apple","monkey","plea"]

<b>Output:</b> 
"apple"
</pre>
</p>

</p>
<p><b>Example 2:</b><br>
<pre>
<b>Input:</b>
s = "abpcplea", d = ["a","b","c"]

<b>Output:</b> 
"a"
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>All the strings in the input will only contain lower-case letters.</li>
<li>The size of the dictionary won't exceed 1,000.</li>
<li>The length of all the strings in the input won't exceed 1,000.</li>
</ol>
</p>
</div>

## Tags
- Two Pointers (two-pointers)
- Sort (sort)

## Companies
- Goldman Sachs - 6 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force

**Algorithm**

The idea behind this approach is as follows. We create a list of all the possible strings that can be formed by deleting one or more characters from the given string $$s$$. In order to do so, we make use of a recursive function `generate(s, str, i, l)` which creates a string by adding and by removing the current character($$i^{th}$$) from the string $$s$$ to the string $$str$$ formed till the index $$i$$. Thus, it adds the $$i^{th}$$ character to $$str$$ and calls itself as `generate(s, str + s.charAt(i), i + 1, l)`. It also omits the $$i^{th}$$ character to $$str$$ and calls itself as `generate(s, str, i + 1, l)`.

Thus, at the end the list $$l$$ contains all the required strings that can be formed using $$s$$. Then, we look for the strings formed in $$l$$ into the dictionary available to see if a match is available. Further, in case of a match, we check for the length of the matched string to maximize the length and we also take care to consider the lexicographically smallest string in case of length match as well.

<iframe src="https://leetcode.com/playground/o5pC5BUY/shared" frameBorder="0" width="100%" height="429" name="o5pC5BUY"></iframe>

**Complexity Analysis**

* Time complexity : $$O(2^n)$$. `generate` calls itself $$2^n$$ times. Here, $$n$$ refers to the length of string $$s$$. 

* Space complexity : $$O(2^n)$$. List $$l$$ contains $$2^n$$ strings.
<br>
<br>

---
#### Approach 2: Iterative Brute Force

**Algorithm**

Instead of using recursive `generate` to create the list of possible strings that can be formed using $$s$$ by performing delete operations, we can also do the same process iteratively. To do so, we use the concept of binary number generation. 

We can treat the given string $$s$$ along with a binary represenation corresponding to the indices of $$s$$. The rule is that the character at the position $$i$$ has to be added to the newly formed string $$str$$ only if there is a boolean 1 at the corresponding index in the binary representation of a number currently considered.

We know a total of $$2^n$$ such binary numbers are possible if there are $$n$$ positions to be filled($$n$$ also corresponds to the number of characters in $$s$$). Thus, we consider all the numbers from $$0$$ to $$2^n$$ in their binary representation in a serial order and generate all the strings possible using the above rule.

The figure below shows an example of the strings generated for the given string $$s$$:"sea".

![Longest_Word](../Figures/524_Longest_Word_Binary.PNG)

A problem with this method is that the maximum length of the string can be 32 only, since we make use of an integer and perform the shift operations on it to generate the binary numbers.

<iframe src="https://leetcode.com/playground/2tTWWkwp/shared" frameBorder="0" width="100%" height="412" name="2tTWWkwp"></iframe>

**Complexity Analysis**

* Time complexity : $$O(2^n)$$. $$2^n$$ strings are generated. 

* Space complexity : $$O(2^n)$$. List $$l$$ contains $$2^n$$ strings.
<br>
<br>

---
#### Approach 3: Sorting and Checking Subsequence

**Algorithm**

The matching condition in the given problem requires that we need to consider the matching string in the dictionary with the longest length and in case of same length, the string which is smallest lexicographically. To ease the searching process, we can sort the given dictionary's strings based on the same criteria, such that the more favorable string appears earlier in the sorted dictionary.

Now, instead of performing the deletions in $$s$$, we can directly check if any of the words given in the dictionary(say $$x$$) is a subsequence of the given string $$s$$, starting from the beginning of the dictionary. This is because, if $$x$$ is a subsequence of $$s$$, we can obtain $$x$$ by performing delete operations on $$s$$. 

If $$x$$ is a subsequence of $$s$$ every character of $$x$$ will be present in $$s$$. The following figure shows the way the subsequence check is done for one example:

!?!../Documents/524_Longest_Word.json:1000,563!?!

As soon as we find any such $$x$$, we can stop the search immediately since we've already processed $$d$$ to our advantage.

<iframe src="https://leetcode.com/playground/WZJBwacd/shared" frameBorder="0" width="100%" height="412" name="WZJBwacd"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n \cdot x \log n + n \cdot x)$$. Here $$n$$ refers to the number of strings in list $$d$$ and $$x$$ refers to average string length. Sorting takes $$O(n\log n)$$ and `isSubsequence` takes $$O(x)$$ to check whether a string is a subsequence of another string or not.  

* Space complexity : $$O(\log n)$$. Sorting takes $$O(\log n)$$ space in average case.
<br>
<br>

---
#### Approach 4: Without Sorting

**Algorithm**

Since sorting the dictionary could lead to a huge amount of extra effort, we can skip the sorting and directly look for the strings $$x$$ in the unsorted dictionary $$d$$ such that $$x$$ is a subsequence in $$s$$. If such a string $$x$$ is found, we compare it with the other matching strings found till now based on the required length and lexicographic criteria. Thus, after considering every string in $$d$$, we can obtain the required result.

<iframe src="https://leetcode.com/playground/sZpQg4Aw/shared" frameBorder="0" width="100%" height="378" name="sZpQg4Aw"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n \cdot x)$$. One iteration over all strings is required. Here $$n$$ refers to the number of strings in list $$d$$ and $$x$$ refers to average string length.

* Space complexity : $$O(x)$$. $$max\_str$$ variable is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short Java Solutions - Sorting Dictionary and Without Sorting
- Author: compton_scatter
- Creation Date: Sun Feb 26 2017 12:01:38 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 18:34:50 GMT+0800 (Singapore Standard Time)

<p>
We sort the input dictionary by longest length and lexicography. Then, we iterate through the dictionary exactly once. In the process, the first dictionary word in the sorted dictionary which appears as a subsequence in the input string s must be the desired solution.

```
public String findLongestWord(String s, List<String> d) {
    Collections.sort(d, (a,b) -> a.length() != b.length() ? -Integer.compare(a.length(), b.length()) :  a.compareTo(b));
    for (String dictWord : d) {
        int i = 0;
        for (char c : s.toCharArray()) 
            if (i < dictWord.length() && c == dictWord.charAt(i)) i++;
        if (i == dictWord.length()) return dictWord;
    }
    return "";
}
```

An alternate, more efficient solution which avoids sorting the dictionary: 

```
public String findLongestWord(String s, List<String> d) {
    String longest = "";
    for (String dictWord : d) {
        int i = 0;
        for (char c : s.toCharArray()) 
            if (i < dictWord.length() && c == dictWord.charAt(i)) i++;

        if (i == dictWord.length() && dictWord.length() >= longest.length()) 
            if (dictWord.length() > longest.length() || dictWord.compareTo(longest) < 0)
                longest = dictWord;
    }
    return longest;
}
```

Time Complexity: O(nk), where n is the length of string s and k is the number of words in the dictionary.
</p>


### Short Python solutions
- Author: StefanPochmann
- Creation Date: Sun Feb 26 2017 17:22:11 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 08:48:12 GMT+0800 (Singapore Standard Time)

<p>
    def findLongestWord(self, s, d):
        def isSubsequence(x):
            it = iter(s)
            return all(c in it for c in x)
        return max(sorted(filter(isSubsequence, d)) + [''], key=len)

More efficient version (no sorting):

    def findLongestWord(self, s, d):
        def isSubsequence(x):
            it = iter(s)
            return all(c in it for c in x)
        return min(filter(isSubsequence, d) + [''], key=lambda x: (-len(x), x))

Different style:

    def findLongestWord(self, s, d):
        best = ''
        for x in d:
            if (-len(x), x) < (-len(best), best):
                it = iter(s)
                if all(c in it for c in x):
                    best = x
        return best

Optimized as suggested by @easton042, testing from longest to shortest and returning the first valid one without testing the rest:

    def findLongestWord(self, s, d):
        def isSubsequence(x):
            it = iter(s)
            return all(c in it for c in x)
        d.sort(key=lambda x: (-len(x), x))
        return next(itertools.ifilter(isSubsequence, d), '')

Or:

    def findLongestWord(self, s, d):
        for x in sorted(d, key=lambda x: (-len(x), x)):
            it = iter(s)
            if all(c in it for c in x):
                return x
        return ''

And taking that even further by not sorting unnecessarily much:

    def findLongestWord(self, s, d):
        heap = [(-len(word), word) for word in d]
        heapq.heapify(heap)
        while heap:
            word = heapq.heappop(heap)[1]
            it = iter(s)
            if all(c in it for c in word):
                return word
        return ''
</p>


### Python Simple (Two pointer)
- Author: awice
- Creation Date: Sun Feb 26 2017 12:12:44 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 12:28:40 GMT+0800 (Singapore Standard Time)

<p>
Let's check whether each word is a subsequence of S individually by "best" order (largest size, then lexicographically smallest.)  Then if we find a match, we know the word being considered must be the best possible answer, since better answers were already considered beforehand.

Let's figure out how to check if a needle (***word***) is a subsequence of a haystack (***S***).  This is a classic problem with the following solution: walk through S, keeping track of the position (***i***) of the needle that indicates that word[i:] still remains to be matched to S at this point in time.  Whenever word[i] matches the current character in S, we only have to match word[i+1:], so we increment i.  At the end of this process, i == len(word) if and only if we've matched every character in word to some character in S in order of our walk.

```
def findLongestWord(self, S, D):
    D.sort(key = lambda x: (-len(x), x))
    for word in D:
        i = 0
        for c in S:
            if i < len(word) and word[i] == c:
                i += 1
        if i == len(word):
            return word
    return ""
```
</p>


