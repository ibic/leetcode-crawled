---
title: "Convert Integer to the Sum of Two No-Zero Integers"
weight: 1234
#id: "convert-integer-to-the-sum-of-two-no-zero-integers"
---
## Description
<div class="description">
<p>Given an integer <code>n</code>. No-Zero integer is a positive integer which <strong>doesn&#39;t contain any 0</strong> in its decimal representation.</p>

<p>Return <em>a list of two integers</em> <code>[A, B]</code> where:</p>

<ul>
	<li><code>A</code> and <code>B</code> are No-Zero integers.</li>
	<li><code>A + B = n</code></li>
</ul>

<p>It&#39;s guarateed that there is at least one valid solution. If there are many valid solutions you can return any of them.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 2
<strong>Output:</strong> [1,1]
<strong>Explanation:</strong> A = 1, B = 1. A + B = n and both A and B don&#39;t contain any 0 in their decimal representation.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 11
<strong>Output:</strong> [2,9]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 10000
<strong>Output:</strong> [1,9999]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 69
<strong>Output:</strong> [1,68]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 1010
<strong>Output:</strong> [11,999]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 10^4</code></li>
</ul>
</div>

## Tags
- Math (math)

## Companies
- HRT - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python
- Author: StefanPochmann
- Creation Date: Sun Jan 12 2020 12:06:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 19 2020 01:56:03 GMT+0800 (Singapore Standard Time)

<p>
Generator expression:
```
def getNoZeroIntegers(self, n):
    return next([a, n-a] for a in range(n) if \'0\' not in f\'{a}{n-a}\')
```
While-loop:
```
def getNoZeroIntegers(self, n):
    a = 1
    while \'0\' in f\'{a}{n-a}\':
        a += 1
    return [a, n-a]
```
For-loop:
```
def getNoZeroIntegers(self, n):
    for a in range(n):
        if \'0\' not in f\'{a}{n-a}\':
            return [a, n-a]
```
</p>


### Java intuitive non brute force
- Author: serdaroquai
- Creation Date: Sun Jan 12 2020 19:20:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 12 2020 19:20:56 GMT+0800 (Singapore Standard Time)

<p>
build two non zero numbers that sums `n` , starting from the least significant digit.
trick is to treat `0` and `1` as `10` and `11`

```java
    public int[] getNoZeroIntegers(int n) {
        
        int a=0, b=0, step=1;
        
        while (n>0) {
            int d = n % 10; // digit
            n /= 10;
            
            if ((d == 0 || d == 1) && n>0) { // n>0 evades the case when 1 is the most significant digit
                a += step*(1+d);
                b += step*9;
                n--; // handle carry
            } else {
                a += step*1;
                b += step*(d-1);
            }
            step *= 10;
        }
        
        return new int[]{a,b};
    }
```
</p>


### Java Simple Solution beats 100%
- Author: Blue_Epoch
- Creation Date: Sun Jan 12 2020 18:33:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 12 2020 21:40:29 GMT+0800 (Singapore Standard Time)

<p>
```
public int[] getNoZeroIntegers(int n) {
        for (int i = 1; i < n; i++) {
            if (nonZero(i) && nonZero(n - i)) {
                return new int[]{i, n - i};
            }
        }
        return new int[]{-1, -1};
    }
    
    private boolean nonZero(int n) {
        while (n > 0) {
            if (n % 10 == 0) return false;
            n /= 10;
        }
        return true;
    }
```
</p>


