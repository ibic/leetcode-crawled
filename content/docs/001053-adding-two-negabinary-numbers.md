---
title: "Adding Two Negabinary Numbers"
weight: 1053
#id: "adding-two-negabinary-numbers"
---
## Description
<div class="description">
<p>Given two numbers <code>arr1</code> and <code>arr2</code> in base <strong>-2</strong>, return the result of adding them together.</p>

<p>Each number is given in <em>array format</em>:&nbsp; as an array of 0s and 1s, from most significant bit to least significant bit.&nbsp; For example, <code>arr = [1,1,0,1]</code> represents the number <code>(-2)^3&nbsp;+ (-2)^2 + (-2)^0 = -3</code>.&nbsp; A number <code>arr</code> in <em>array, format</em> is also guaranteed to have no leading zeros: either&nbsp;<code>arr == [0]</code> or <code>arr[0] == 1</code>.</p>

<p>Return the result of adding <code>arr1</code> and <code>arr2</code> in the same format: as an array of 0s and 1s with no leading zeros.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr1 = [1,1,1,1,1], arr2 = [1,0,1]
<strong>Output:</strong> [1,0,0,0,0]
<strong>Explanation: </strong>arr1 represents 11, arr2 represents 5, the output represents 16.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr1 = [0], arr2 = [0]
<strong>Output:</strong> [0]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr1 = [0], arr2 = [1]
<strong>Output:</strong> [1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr1.length,&nbsp;arr2.length &lt;= 1000</code></li>
	<li><code>arr1[i]</code>&nbsp;and <code>arr2[i]</code> are&nbsp;<code>0</code> or <code>1</code></li>
	<li><code>arr1</code> and <code>arr2</code> have no leading zeros</li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Grab - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Python] Convert from Base 2 Addition
- Author: lee215
- Creation Date: Sun Jun 02 2019 12:03:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 12:03:44 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**:
Quite similar to [1017. Convert to Base -2](https://leetcode.com/problems/convert-to-base-2/discuss/265507/JavaC++Python-2-lines-Exactly-Same-as-Base-2)
1. Maybe write a base2 addition first?
2. Add minus `-`?
3. Done.
<br>


## **Complexity**
Time `O(N)`, Space `O(N)`
<br>


**C++:**
```
    vector<int> addBinary(vector<int>& A, vector<int>& B) {
        vector<int> res;
        int carry = 0, i = A.size() - 1, j = B.size() - 1;
        while (i >= 0 || j >= 0 || carry) {
            if (i >= 0) carry += A[i--];
            if (j >= 0) carry += B[j--];
            res.push_back(carry & 1);
            carry = (carry >> 1);
        }
        reverse(res.begin(), res.end());
        return res;
    }

    vector<int> addNegabinary(vector<int>& A, vector<int>& B) {
        vector<int> res;
        int carry = 0, i = A.size() - 1, j = B.size() - 1;
        while (i >= 0 || j >= 0 || carry) {
            if (i >= 0) carry += A[i--];
            if (j >= 0) carry += B[j--];
            res.push_back(carry & 1);
            carry = -(carry >> 1);
        }
        while (res.size() > 1 && res.back() == 0)
            res.pop_back();
        reverse(res.begin(), res.end());
        return res;
    }
```

**Python:**
```
    def addBinary(self, A, B):
        res = []
        carry = 0
        while A or B or carry:
            carry += (A or [0]).pop() + (B or [0]).pop()
            res.append(carry & 1)
            carry = carry >> 1
        return res[::-1]

    def addNegabinary(self, A, B):
        res = []
        carry = 0
        while A or B or carry:
            carry += (A or [0]).pop() + (B or [0]).pop()
            res.append(carry & 1)
            carry = -(carry >> 1)
        while len(res) > 1 and res[-1] == 0:
            res.pop()
        return res[::-1]
```

</p>


### C++ From Wikipedia
- Author: votrubac
- Creation Date: Sun Jun 02 2019 12:18:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 12:41:40 GMT+0800 (Singapore Standard Time)

<p>
See [Wikipedia\'s Negative Base Addition](https://en.wikipedia.org/wiki/Negative_base#Addition).

We start from the least signifficant bit, and do the binary summation as usual. However, our carry can have three states: -1, 0, and 1.

If the sum for the current bit is greater than 2, the carry becomes -1, not 1 as in the base 2 case. Because of that, our sum can become -1. In this case, the carry should be set to 1.
```
vector<int> addNegabinary(vector<int>& a1, vector<int>& a2) {
  vector<int> res;
  int bit = 0, carry = 0, sz = max(a1.size(), a2.size());
  for (auto bit = 0; bit < sz || carry != 0; ++bit) {
    auto b1 = bit < a1.size() ? a1[a1.size() - bit - 1] : 0;
    auto b2 = bit < a2.size() ? a2[a2.size() - bit - 1] : 0;
    auto sum = b1 + b2 + carry;
    res.push_back(abs(sum) % 2); 
    carry = sum < 0 ? 1 : sum > 1 ? -1 : 0;
  }
  while (res.size() > 1 && res.back() == 0) res.pop_back();
  reverse(begin(res), end(res));
  return res;
}
```
</p>


### [Python beats 100%]This is my first question I contributed on leetcode, here is my approach
- Author: calvinchankf
- Creation Date: Sun Jun 02 2019 14:17:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 14:17:32 GMT+0800 (Singapore Standard Time)

<p>
I was asked during an interview and I found that it was quite interesting so I contributed it here.

Actually, during the interview, my intuition was to transform the arr1 and arr2 to numbers and add and transform it back to a negabinary number using the way like #1017. 

But later I found out from the [wikipedia](https://en.wikipedia.org/wiki/Negative_base#Addition) that there is a much better way.

```py
class Solution(object):
    def addNegabinary(self, arr1, arr2):
        """
        ref:
            - https://en.wikipedia.org/wiki/Negative_base#Addition

            Carry:          1 \u22121  0 \u22121  1 \u22121  0  0  0
            First addend:         1  0  1  0  1  0  1
            Second addend:        1  1  1  0  1  0  0 + addition
                            --------------------------
            Number:         1 \u22121  2  0  3 \u22121  2  0  1 when we come with a number, lookup from the hashtable
            -----------------------------------------
            Bit (result):   1  1  0  0  1  1  0  0  1
            Carry:          0  1 \u22121  0 \u22121  1 \u22121  0  0 <- each number will be moved to the carry on the top in the next bit
        """
        lookup = {
            -2: (0, 1), # useless becos \u22122 occurs only during subtraction
            -1: (1, 1),
            0: (0, 0),
            1: (1, 0),
            2: (0, -1),
            3: (1, -1),
        }
        carry = 0
        result = []
        # do addition
        while len(arr1) > 0 or len(arr2) > 0:
            a = 0
            if len(arr1) > 0:
                a = arr1.pop()
            b = 0
            if len(arr2) > 0:
                b = arr2.pop()
            temp = a + b + carry
            res, carry = lookup[temp]
            result.append(res)
        # if there is still a carry
        while carry != 0:
            res, carry = lookup[carry]
            result.append(res)
        # remove leading zeros
        while len(result) > 1 and result[-1] == 0:
            result.pop()
        return result[::-1]
```

Honestly, I don\'t think I can come up with this. But now I see there are some better approaches, so thank you everyone.
- [from @lee215](https://leetcode.com/problems/adding-two-negabinary-numbers/discuss/303751/C%2B%2BPython-Convert-from-Base-2-Addition)
- [from @tiankonguse](https://leetcode.com/problems/adding-two-negabinary-numbers/discuss/303831/ChineseC%2B%2B-1073)
- [from @litian220](https://leetcode.com/problems/adding-two-negabinary-numbers/discuss/303809/Java-Use-stack-Beat-100)
- [from @amitnsky](https://leetcode.com/problems/adding-two-negabinary-numbers/discuss/303875/Adjust-or-Carry-method)
- [from @bupt_wc](https://leetcode.com/problems/convert-to-base-2/discuss/265688/4-line-python-clear-solution-with-explanation)
</p>


