---
title: "Distance Between Bus Stops"
weight: 1151
#id: "distance-between-bus-stops"
---
## Description
<div class="description">
<p>A bus&nbsp;has <code>n</code> stops numbered from <code>0</code> to <code>n - 1</code> that form&nbsp;a circle. We know the distance between all pairs of neighboring stops where <code>distance[i]</code> is the distance between the stops number&nbsp;<code>i</code> and <code>(i + 1) % n</code>.</p>

<p>The bus goes along both directions&nbsp;i.e. clockwise and counterclockwise.</p>

<p>Return the shortest distance between the given&nbsp;<code>start</code>&nbsp;and <code>destination</code>&nbsp;stops.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/09/03/untitled-diagram-1.jpg" style="width: 388px; height: 240px;" /></p>

<pre>
<strong>Input:</strong> distance = [1,2,3,4], start = 0, destination = 1
<strong>Output:</strong> 1
<strong>Explanation:</strong> Distance between 0 and 1 is 1 or 9, minimum is 1.</pre>

<p>&nbsp;</p>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/09/03/untitled-diagram-1-1.jpg" style="width: 388px; height: 240px;" /></p>

<pre>
<strong>Input:</strong> distance = [1,2,3,4], start = 0, destination = 2
<strong>Output:</strong> 3
<strong>Explanation:</strong> Distance between 0 and 2 is 3 or 7, minimum is 3.
</pre>

<p>&nbsp;</p>

<p><strong>Example 3:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/09/03/untitled-diagram-1-2.jpg" style="width: 388px; height: 240px;" /></p>

<pre>
<strong>Input:</strong> distance = [1,2,3,4], start = 0, destination = 3
<strong>Output:</strong> 4
<strong>Explanation:</strong> Distance between 0 and 3 is 6 or 4, minimum is 4.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n&nbsp;&lt;= 10^4</code></li>
	<li><code>distance.length == n</code></li>
	<li><code>0 &lt;= start, destination &lt; n</code></li>
	<li><code>0 &lt;= distance[i] &lt;= 10^4</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n) solution -- Easy to understand
- Author: GoGoogle2020
- Creation Date: Sun Sep 08 2019 12:21:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 14:11:39 GMT+0800 (Singapore Standard Time)

<p>
1. Make sure start comes before end;
2. Caculate the sum of target interval as well as the total sum;
3. The result will be the min of target interval sum and the remaining interval sum.
```
class Solution {
    public int distanceBetweenBusStops(int[] distance, int start, int destination) {
        if (start > destination) {
            int temp = start;
            start = destination;
            destination = temp;;
        }
        int res = 0, total = 0;
        for (int i = 0; i < distance.length; i++) {
            if (i >= start && i < destination) {
                res += distance[i];
            }
            total += distance[i];
        }
        return Math.min(res, total - res);
    }
}

```
</p>


### python 3 easy to understand
- Author: akaghosting
- Creation Date: Sun Sep 08 2019 13:47:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 13:47:21 GMT+0800 (Singapore Standard Time)

<p>
	class Solution:
		def distanceBetweenBusStops(self, distance: List[int], start: int, destination: int) -> int:
			a = min(start, destination)
			b = max(start, destination)
			return min(sum(distance[a:b]), sum(distance) - sum(distance[a:b]))
</p>


### C++ one pass
- Author: votrubac
- Creation Date: Sun Sep 08 2019 14:28:11 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 28 2020 10:43:53 GMT+0800 (Singapore Standard Time)

<p>
```
int distanceBetweenBusStops(vector<int>& distance, int start, int destination) {
  int sum1 = 0, sum2 = 0;
  if (start > destination) 
      swap(start, destination);
  for (auto i = 0; i < distance.size(); ++i) {
    if (i >= start && i < destination) 
        sum1 += distance[i];
    else 
        sum2 += distance[i];
  }
  return min(sum1, sum2);
}
```
</p>


