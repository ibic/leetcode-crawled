---
title: "Longest Palindromic Subsequence"
weight: 487
#id: "longest-palindromic-subsequence"
---
## Description
<div class="description">
<p>Given a string s, find the longest palindromic subsequence&#39;s length in s. You may assume that the maximum length of s is 1000.</p>

<p><b>Example 1:</b><br />
Input:</p>

<pre>
&quot;bbbab&quot;
</pre>
Output:

<pre>
4
</pre>
One possible longest palindromic subsequence is &quot;bbbb&quot;.

<p>&nbsp;</p>

<p><b>Example 2:</b><br />
Input:</p>

<pre>
&quot;cbbd&quot;
</pre>
Output:

<pre>
2
</pre>
One possible longest palindromic subsequence is &quot;bb&quot;.
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 1000</code></li>
	<li><code>s</code> consists only of lowercase English letters.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Microsoft - 4 (taggedByAdmin: false)
- Amazon - 5 (taggedByAdmin: true)
- Facebook - 4 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- LinkedIn - 6 (taggedByAdmin: false)
- Uber - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Straight forward Java DP solution
- Author: tankztc
- Creation Date: Fri Feb 10 2017 07:43:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 00:09:53 GMT+0800 (Singapore Standard Time)

<p>
```dp[i][j]```: the longest palindromic subsequence\'s length of substring(i, j), here i, j represent left, right indexes in the string
```State transition```: 
```dp[i][j] = dp[i+1][j-1] + 2``` if s.charAt(i) == s.charAt(j)
                                     otherwise, ```dp[i][j] = Math.max(dp[i+1][j], dp[i][j-1])```
```Initialization```: ```dp[i][i] = 1```
```
public class Solution {
    public int longestPalindromeSubseq(String s) {
        int[][] dp = new int[s.length()][s.length()];
        
        for (int i = s.length() - 1; i >= 0; i--) {
            dp[i][i] = 1;
            for (int j = i+1; j < s.length(); j++) {
                if (s.charAt(i) == s.charAt(j)) {
                    dp[i][j] = dp[i+1][j-1] + 2;
                } else {
                    dp[i][j] = Math.max(dp[i+1][j], dp[i][j-1]);
                }
            }
        }
        return dp[0][s.length()-1];
    }
}
```


Top bottom recursive method with memoization
```
public class Solution {
    public int longestPalindromeSubseq(String s) {
        return helper(s, 0, s.length() - 1, new Integer[s.length()][s.length()]);
    }
    
    private int helper(String s, int i, int j, Integer[][] memo) {
        if (memo[i][j] != null) {
            return memo[i][j];
        }
        if (i > j)      return 0;
        if (i == j)     return 1;
        
        if (s.charAt(i) == s.charAt(j)) {
            memo[i][j] = helper(s, i + 1, j - 1, memo) + 2;
        } else {
            memo[i][j] = Math.max(helper(s, i + 1, j, memo), helper(s, i, j - 1, memo));
        }
        return memo[i][j];
    }
}
```
</p>


### DP Problem Classifications - Helpful Notes
- Author: adityakrverma
- Creation Date: Mon Jan 21 2019 07:55:14 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 06 2019 14:30:59 GMT+0800 (Singapore Standard Time)

<p>
Below is just to summarise problem types seem by me while working on this topic.  Posting here so that , it can help people to get feel of overall DP problem categories.  BTW, these are just my personal notes, where I have also provided reference link from Tushar Roy video which help with concepts. And definately if you all problems mentioned below - you will get fairly good idea and thought process behind Dynamic programming.

========================= DYNAMIC PROGRAMMING PROBLEMS ======================================

Problem categories and related videos: (below)
----------------------------------------
Palindrome Based: LC 516, 5 647
Palindrome Partitioning: LC 132 133
Decode ways: LC 91
Stocks: LC 121 122 309 714
Path : LC 62 63 64
Jump Game : 55 45
Stairs: 70 746
Wildcard: LC 44 10
Word Break: 139 140
Max Sq & Rect: 85 221
Super Egg: LC 887
Coin Change: LC 322, 518, 441
Longest Common substring:  718

Others :
--------
House Robber: LC 213, 198
Paint House : LC 256 265
Subarray : LC 53, 152
Subsequence LIS : LC 300 354
Math: LC 279 343 204

============================================================================================
Jump Game : LC 55, 45
https://www.youtube.com/watch?v=cETfFsSTGJI&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=22

--------------------------------------------------------------------------------------------
Stair case : LC 70, 746
https://www.youtube.com/watch?v=CFQk7OQO_xM&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=31

--------------------------------------------------------------------------------------------
Paths ( Unique path and Min Path): LC 62,63,64
https://www.youtube.com/watch?v=lBRtnuxg-gU&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=21
https://www.youtube.com/watch?v=lBRtnuxg-gU&index=21&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr

--------------------------------------------------------------------------------------------
Stock Based: LC 121,122
https://www.youtube.com/watch?v=oDhu5uGq_ic&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=20

--------------------------------------------------------------------------------------------
Egg Dropping: LC-887
https://www.youtube.com/watch?v=3hcaVyX00_4&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=13

--------------------------------------------------------------------------------------------
Wildcard Matching: LC 44 && Regular Expression: LC 10
https://www.youtube.com/watch?v=3ZDZ-N0EPV0&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=24
https://www.youtube.com/watch?v=l3hda49XcDE&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=17

--------------------------------------------------------------------------------------------
Longest Palindromic Subsequence: LC 516, 5, 647
https://www.youtube.com/watch?v=_nCsPn7_OgI&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=9

--------------------------------------------------------------------------------------------
Min Edit distance: LC 72
https://www.youtube.com/watch?v=We3YDTzNXEk&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=7

--------------------------------------------------------------------------------------------
Longest Incresing Subsequence: LC 300, 354
https://www.youtube.com/watch?v=CE2b_-XfVDk&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=8

--------------------------------------------------------------------------------------------
Coin Change I & II: LC 322, 518, 441
https://www.youtube.com/watch?v=NJuKJ8sasGk&index=10&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr
https://www.youtube.com/watch?v=_fgjrs570YE&index=11&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr

--------------------------------------------------------------------------------------------
Max Size Rectangle: LC 85, 84
https://www.youtube.com/watch?v=g8bSdXCG-lA&index=18&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr

--------------------------------------------------------------------------------------------
Max Subsquare in 2D Matrix: LC 221
https://www.youtube.com/watch?v=_Lf1looyJMU&index=28&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr

--------------------------------------------------------------------------------------------
Palindrome Partition : LC 132, 133
https://www.youtube.com/watch?v=lDYIvtBVmgo&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=26

--------------------------------------------------------------------------------------------
Interleaving String: LC 97
https://www.youtube.com/watch?v=ih2OZ9-M3OM&index=32&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr

--------------------------------------------------------------------------------------------
Word Break : LC 139 140 ( But I didn\'t follow this link)
https://www.youtube.com/watch?v=WepWFGxiwRs&index=19&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr

============================================================================================

Subset Sum:
https://www.youtube.com/watch?v=s6FhG--P7z0&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=4

Longest Common substring:
https://www.youtube.com/watch?v=BysNXJHzCEs&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=16

Max Sum Rectangle Submatrix in Matrix: Similar 85, 363
https://www.youtube.com/watch?v=yCQN096CwWM&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=15

=====================================================================================================================================================================================



</p>


### Evolve from brute force to dp
- Author: yu6
- Creation Date: Fri Feb 10 2017 09:53:30 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 23:32:04 GMT+0800 (Singapore Standard Time)

<p>
1. O(2^n) Brute force. If the two ends of a string are the same, then they must be included in the longest palindrome subsequence. Otherwise, both ends cannot be included in the longest palindrome subsequence.
```
    int longestPalindromeSubseq(string s) {
        return longestPalindromeSubseq(0,s.size()-1,s); 
    }
    int longestPalindromeSubseq(int l, int r, string &s) {
        if(l==r) return 1;
        if(l>r) return 0;  //happens after "aa" 
        return s[l]==s[r] ? 2 + longestPalindromeSubseq(l+1,r-1, s) : 
            max(longestPalindromeSubseq(l+1,r, s),longestPalindromeSubseq(l,r-1, s)); 
    }
```
2. O(n^2) Memoization
```
    int longestPalindromeSubseq(string s) {
        int n = s.size();
        vector<vector<int>> mem(n,vector<int>(n));
        return longestPalindromeSubseq(0,n-1, s,mem); 
    }
    int longestPalindromeSubseq(int l, int r, string &s, vector<vector<int>>& mem) {
        if(l==r) return 1;
        if(l>r) return 0;
        if(mem[l][r]) return mem[l][r];
        return mem[l][r] = s[l]==s[r] ? 2 + longestPalindromeSubseq(l+1,r-1, s,mem) : 
            max(longestPalindromeSubseq(l+1,r, s,mem),longestPalindromeSubseq(l,r-1, s,mem)); 
        
    }
```
3. O(n^2) dp
```
    int longestPalindromeSubseq(string s) {
        int n = s.size();
        vector<vector<int>> dp(n+1,vector<int>(n));
        for(int i=0;i<n;i++) dp[1][i]=1;
        for(int i=2;i<=n;i++) //length
            for(int j=0;j<n-i+1;j++) {//start index 
                dp[i][j] = s[j]==s[i+j-1]?2+dp[i-2][j+1]:max(dp[i-1][j],dp[i-1][j+1]);
        return dp[n][0]; 
    }
```
4. O(n^2) time, O(n) space dp. In #3, the current row is computed from the previous 2 rows only. So we don't need to keep all the rows.
```
    int longestPalindromeSubseq(string s) {
        int n = s.size();
        vector<int> v0(n), v1(n,1), v(n), *i_2=&v0, *i_1=&v1, *i_=&v;
        for(int i=2;i<=n;i++) {//length
            for(int j=0;j<n-i+1;j++)//start index
                i_->at(j) = s[j]==s[i+j-1]?2+i_2->at(j+1):max(i_1->at(j),i_1->at(j+1));
            swap(i_1,i_2);    
            swap(i_1,i_); //rotate i_2, i_1, i_
        }
        return i_1->at(0); 
    }
```
</p>


