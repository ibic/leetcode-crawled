---
title: "Reverse Words in a String II"
weight: 176
#id: "reverse-words-in-a-string-ii"
---
## Description
<div class="description">
<p>Given an input string<strong><em>&nbsp;</em></strong>, reverse the string word by word.&nbsp;</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:  </strong>[&quot;t&quot;,&quot;h&quot;,&quot;e&quot;,&quot; &quot;,&quot;s&quot;,&quot;k&quot;,&quot;y&quot;,&quot; &quot;,&quot;i&quot;,&quot;s&quot;,&quot; &quot;,&quot;b&quot;,&quot;l&quot;,&quot;u&quot;,&quot;e&quot;]
<strong>Output: </strong>[&quot;b&quot;,&quot;l&quot;,&quot;u&quot;,&quot;e&quot;,&quot; &quot;,&quot;i&quot;,&quot;s&quot;,&quot; &quot;,&quot;s&quot;,&quot;k&quot;,&quot;y&quot;,&quot; &quot;,&quot;t&quot;,&quot;h&quot;,&quot;e&quot;]</pre>

<p><strong>Note:&nbsp;</strong></p>

<ul>
	<li>A word is defined as a sequence of non-space characters.</li>
	<li>The input string does not contain leading or trailing spaces.</li>
	<li>The words are always separated by a single space.</li>
</ul>

<p><strong>Follow up:&nbsp;</strong>Could you do it <i>in-place</i> without allocating extra space?</p>

</div>

## Tags
- String (string)

## Companies
- Microsoft - 2 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)
- Uber - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Reverse the Whole String and Then Reverse Each Word

To have this problem in Amazon interview is a good situation, since input 
is a mutable structure and hence one could
aim $$\mathcal{O}(1)$$ space solution without any technical difficulties.

> The idea is simple: reverse the whole string and then reverse each word. 

![fig](../Figures/186/reverse.png)

**Algorithm**

Let's first implement two functions:

- `reverse(l: list, left: int, right: int)`, which reverses array characters 
between left and right pointers. C++ users could directly use built-in `std::reverse`. 

- `reverse_each_word(l: list)`, which uses two pointers to mark the boundaries of
each word and previous function to reverse it. 

Now `reverseWords(s: List[str])` implementation is straightforward:

- Reverse the whole string: `reverse(s, 0, len(s) - 1)`.

- Reverse each word: `reverse_each_word(s)`.

**Implementation**

!?!../Documents/186_LIS.json:1000,507!?!

<iframe src="https://leetcode.com/playground/Lg8ZGuUy/shared" frameBorder="0" width="100%" height="500" name="Lg8ZGuUy"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, it's two passes along the string.

* Space complexity: $$\mathcal{O}(1)$$, it's a constant space solution.
<br /> 
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My Java solution with explanation
- Author: xzhu@kabaminc.com
- Creation Date: Wed Feb 04 2015 07:12:16 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 09:12:46 GMT+0800 (Singapore Standard Time)

<p>

    public void reverseWords(char[] s) {
        // Three step to reverse
        // 1, reverse the whole sentence
        reverse(s, 0, s.length - 1);
        // 2, reverse each word
        int start = 0;
        int end = -1;
        for (int i = 0; i < s.length; i++) {
            if (s[i] == ' ') {
                reverse(s, start, i - 1);
                start = i + 1;
            }
        }
        // 3, reverse the last word, if there is only one word this will solve the corner case
        reverse(s, start, s.length - 1);
    }
    
    public void reverse(char[] s, int start, int end) {
        while (start < end) {
            char temp = s[start];
            s[start] = s[end];
            s[end] = temp;
            start++;
            end--;
        }
    }
</p>


### Python solution
- Author: autekwing
- Creation Date: Thu Sep 03 2015 16:19:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 11:46:13 GMT+0800 (Singapore Standard Time)

<p>
reverse the whole string and then reverse words by words

    def reverseWords(self, s):
        self.reverse(s, 0, len(s) - 1)
        
        beg = 0
        for i in xrange(len(s)):
            if s[i] == ' ':
                self.reverse(s, beg, i-1)
                beg = i + 1
            elif i == len(s) -1:
                self.reverse(s, beg, i)
    
    def reverse(self, s, start, end):
        while start < end:
            s[start], s[end] = s[end], s[start]
            start += 1
            end -= 1
</p>


### Six lines solution in C++
- Author: weibest
- Creation Date: Tue Feb 03 2015 13:36:50 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 13:22:40 GMT+0800 (Singapore Standard Time)

<p>
    void reverseWords(string &s) {
        reverse(s.begin(), s.end());
        for (int i = 0, j = 0; i < s.size(); i = j + 1) {
            for (j = i; j < s.size() && !isblank(s[j]); ++j);
            reverse(s.begin()+i, s.begin()+j);
        }
    }
</p>


