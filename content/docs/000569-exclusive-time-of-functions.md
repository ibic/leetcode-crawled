---
title: "Exclusive Time of Functions"
weight: 569
#id: "exclusive-time-of-functions"
---
## Description
<div class="description">
<p>On a <strong>single-threaded</strong> CPU, we execute a program containing <code>n</code> functions. Each function has a unique ID between <code>0</code> and <code>n-1</code>.</p>

<p>Function calls are <strong>stored in a <a href="https://en.wikipedia.org/wiki/Call_stack">call stack</a></strong>: when a function call starts, its ID is pushed onto the stack, and when a function call ends, its ID is popped off the stack. The function whose ID is at the top of the stack is <strong>the current function being executed</strong>. Each time a function starts or ends, we write a log with the ID, whether it started or ended, and the timestamp.</p>

<p>You are given a list <code>logs</code>, where <code>logs[i]</code> represents the <code>i<sup>th</sup></code> log message formatted as a string <code>&quot;{function_id}:{&quot;start&quot; | &quot;end&quot;}:{timestamp}&quot;</code>. For example, <code>&quot;0:start:3&quot;</code> means a function call with function ID <code>0</code> <strong>started at the beginning</strong> of timestamp <code>3</code>, and <code>&quot;1:end:2&quot;</code> means a function call with function ID <code>1</code> <strong>ended at the end</strong> of timestamp <code>2</code>. Note that a function can be called <b>multiple times, possibly recursively</b>.</p>

<p>A function&#39;s <strong>exclusive time</strong> is the sum of execution times for all function calls in the program. For example, if a function is called twice, one call executing for <code>2</code> time units and another call executing for <code>1</code> time unit, the <strong>exclusive time</strong> is <code>2 + 1 = 3</code>.</p>

<p>Return <em>the <strong>exclusive time</strong> of each function in an array, where the value at the </em><code>i<sup>th</sup></code><em> index represents the exclusive time for the function with ID </em><code>i</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/04/05/diag1b.png" style="width: 550px; height: 239px;" />
<pre>
<strong>Input:</strong> n = 2, logs = [&quot;0:start:0&quot;,&quot;1:start:2&quot;,&quot;1:end:5&quot;,&quot;0:end:6&quot;]
<strong>Output:</strong> [3,4]
<strong>Explanation:</strong>
Function 0 starts at the beginning of time 0, then it executes 2 for units of time and reaches the end of time 1.
Function 1 starts at the beginning of time 2, executes for 4 units of time, and ends at the end of time 5.
Function 0 resumes execution at the beginning of time 6 and executes for 1 unit of time.
So function 0 spends 2 + 1 = 3 units of total time executing, and function 1 spends 4 units of total time executing.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 1, logs = [&quot;0:start:0&quot;,&quot;0:start:2&quot;,&quot;0:end:5&quot;,&quot;0:start:6&quot;,&quot;0:end:6&quot;,&quot;0:end:7&quot;]
<strong>Output:</strong> [8]
<strong>Explanation:</strong>
Function 0 starts at the beginning of time 0, executes for 2 units of time, and recursively calls itself.
Function 0 (recursive call) starts at the beginning of time 2 and executes for 4 units of time.
Function 0 (initial call) resumes execution then immediately calls itself again.
Function 0 (2nd recursive call) starts at the beginning of time 6 and executes for 1 unit of time.
Function 0 (initial call) resumes execution at the beginning of time 7 and executes for 1 unit of time.
So function 0 spends 2 + 4 + 1 + 1 = 8 units of total time executing.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 2, logs = [&quot;0:start:0&quot;,&quot;0:start:2&quot;,&quot;0:end:5&quot;,&quot;1:start:6&quot;,&quot;1:end:6&quot;,&quot;0:end:7&quot;]
<strong>Output:</strong> [7,1]
<strong>Explanation:</strong>
Function 0 starts at the beginning of time 0, executes for 2 units of time, and recursively calls itself.
Function 0 (recursive call) starts at the beginning of time 2 and executes for 4 units of time.
Function 0 (initial call) resumes execution then immediately calls function 1.
Function 1 starts at the beginning of time 6, executes 1 units of time, and ends at the end of time 6.
Function 0 resumes execution at the beginning of time 6 and executes for 2 units of time.
So function 0 spends 2 + 4 + 1 = 7 units of total time executing, and function 1 spends 1 unit of total time executing.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 2, logs = [&quot;0:start:0&quot;,&quot;0:start:2&quot;,&quot;0:end:5&quot;,&quot;1:start:7&quot;,&quot;1:end:7&quot;,&quot;0:end:8&quot;]
<strong>Output:</strong> [8,1]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 1, logs = [&quot;0:start:0&quot;,&quot;0:end:0&quot;]
<strong>Output:</strong> [1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 100</code></li>
	<li><code>1 &lt;= logs.length &lt;= 500</code></li>
	<li><code>0 &lt;= function_id &lt; n</code></li>
	<li><code>0 &lt;= timestamp &lt;= 10<sup>9</sup></code></li>
	<li>No two start events will happen at the same timestamp.</li>
	<li>No two end events will happen at the same timestamp.</li>
	<li>Each function has an <code>&quot;end&quot;</code> log for each <code>&quot;start&quot;</code> log.</li>
</ul>

</div>

## Tags
- Stack (stack)

## Companies
- Facebook - 17 (taggedByAdmin: true)
- Amazon - 5 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Oracle - 6 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: false)
- Qualtrics - 2 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Using Stack [Time Limit Exceeded]

Before starting off with the solution, let's discuss a simple idea. Suppose we have three functions $$func_1$$, $$func_2$$ and $$func_3$$ such that $$func_1$$ calls $$func_2$$ and then $$func_2$$ calls $$func_3$$. In this case, $$func_3$$ starts at the end and ends first, $$func_2$$ starts at 2nd position and ends at the 2nd last step. Similarly, $$func_1$$ starts first and ends at the last position. Thus, we can conclude that the function which is entered at the end finishes first and the one which is entered first ends at the last position. 

From the above discussion, we can conclude that we can make use of a $$stack$$ to solve the given problem. We can start by pushing the first function's id from the given $$logs$$ list onto the array. We also keep a track of the current $$time$$. We also make use of a $$res$$ array, such that $$res[i]$$ is to keep a track of the exclusive time spent by the Fucntion with function id $$i$$ till the current time. 

Now, we can move on to the next function in $$logs$$. The start/end time of the next function will obviously be larger than the start time of the function on the $$stack$$. We keep on incrementing the current $$time$$ and the exclusive time for the function on the top of the $$stack$$ till the current time becomes equal to the start/end time of the next function in the $$logs$$ list. 

Thus, now, we've reached a point, where the control shifts from the last function to a new function, due to a function call(indicated by a start label for the next function), or the last function could exit(indicated by the end label for the next function). Thus, we can no longer continue with the same old function. 

If the next function includes a start label, we push this function on the top of the $$stack$$, since the last function would need to be revisited again in the future. On the other hand, if the next function includes an end label, it means the last function on the top of the $$stack$$ is terminating.

We also know that an end label indicates that this function executes till the end of the given time. Thus, we need to increment the current $$time$$ and the exclusive time of the last function as well to account for this fact. Now, we can remove(pop) this function from the $$stack$$.  We can continue this process for every function in the $$logs$$ list. 

At the end, the $$res$$ array gives the exclusive times for each function.

Summarizing the above process, we need to do the following:

1. Push the function id of the first function in the $$logs$$ list on the $$stack$$.

2. Keep incrementing the exlusive time(along with the current time) corresponding to the function on the top of the $$stack$$(in the $$res$$ array), till the current time equals the start/end time corresponding to the next function in the $$logs$$ list.

3. If the next function has a 'start' label, push this function's id onto the stack. Otherwise, increment the last function's exclusive time(along with the current time), and pop the function id from the top of the stack.

4. Repeat steps 2 and 3 till all the functions in the $$logs$$ list have been considered.

5. Return the resultant exlcusive time($$res$$).

<iframe src="https://leetcode.com/playground/RqRjdFmv/shared" frameBorder="0" name="RqRjdFmv" width="100%" height="496"></iframe>


**Complexity Analysis**

* Time complexity : $$O(t)$$. We increment the time till all the functions are done with the execution. Here, $$t$$ refers to the end time of the last function in the $$logs$$ list.

* Space complexity : $$O(n)$$. The $$stack$$ can grow upto a depth of atmost $$n/2$$. Here, $$n$$ refers to the number of elements in the given $$logs$$ list.

---
#### Approach #2 Better Approach [Accepted]

**Algorithm**

In the last approach, for every function on the top of the $$stack$$, we incremented the current time and the exclusive time of this same function till the current time became equal to the start/end time of the next function. 

Instead of doing this incrementing step by step, we can directly use the difference between the next function's start/stop time and the current function's start/stop time. The rest of the process remains the same as in the last approach. 

The following animation illustrates the process.

!?!../Documents/636_Exclusive_Time_of_Functions.json:1000,563!?!

<iframe src="https://leetcode.com/playground/rZkuT7RU/shared" frameBorder="0" name="rZkuT7RU" width="100%" height="462"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We iterate over the entire $$logs$$ array just once. Here, $$n$$ refers to the number of elements in the $$logs$$ list.

* Space complexity : The $$stack$$ can grow upto a depth of atmost $$n/2$$. Here, $$n$$ refers to the number of elements in the given $$logs$$ list.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Jul 16 2017 11:45:46 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 11:10:00 GMT+0800 (Singapore Standard Time)

<p>
We examine two approaches - both will be stack based.

In a more conventional approach, let's look between adjacent events, with duration ```time - prev_time```.  If we started a function, and we have a function in the background, then it was running during this time.  Otherwise, we ended the function that is most recent in our stack.
```
def exclusiveTime(self, N, logs):
    ans = [0] * N
    stack = []
    prev_time = 0

    for log in logs:
        fn, typ, time = log.split(':')
        fn, time = int(fn), int(time)

        if typ == 'start':
            if stack:
                ans[stack[-1]] += time - prev_time 
            stack.append(fn)
            prev_time = time
        else:
            ans[stack.pop()] += time - prev_time + 1
            prev_time = time + 1

    return ans
```

<hr>

In the second approach, we try to record the "penalty" a function takes.  For example, if function 0 is running at time [1, 10], and function 1 runs at time [3, 5], then we know function 0 ran for 10 units of time, less a 3 unit penalty.  The idea is this: **Whenever a function completes using T time, any functions that were running in the background take a penalty of T.**  Here is a slow version to illustrate the idea:

```
def exclusiveTime(self, N, logs):
    ans = [0] * N
    #stack = SuperStack()
    stack = []

    for log in logs:
        fn, typ, time = log.split(':')
        fn, time = int(fn), int(time)

        if typ == 'start':
            stack.append(time)
        else:
            delta = time - stack.pop() + 1
            ans[fn] += delta
            #stack.add_across(delta)
            stack = [t+delta for t in stack] #inefficient

    return ans
```

This code already ACs, but it isn't efficient.  However, we can easily upgrade our stack to a "superstack" that supports ```self.add_across```: addition over the whole array in constant time.

```
class SuperStack(object):
    def __init__(self):
        self.A = []
    def append(self, x):
        self.A.append([x, 0])
    def pop(self):
        x, y = self.A.pop()
        if self.A:
            self.A[-1][1] += y
        return x + y
    def add_across(self, y):
        if self.A:
            self.A[-1][1] += y
```
</p>


### Java Stack Solution O(n) Time O(n) Space
- Author: compton_scatter
- Creation Date: Sun Jul 16 2017 11:12:16 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 09:34:51 GMT+0800 (Singapore Standard Time)

<p>
```
public int[] exclusiveTime(int n, List<String> logs) {
    int[] res = new int[n];
    Stack<Integer> stack = new Stack<>();
    int prevTime = 0;
    for (String log : logs) {
        String[] parts = log.split(":");
        if (!stack.isEmpty()) res[stack.peek()] +=  Integer.parseInt(parts[2]) - prevTime; 
        prevTime = Integer.parseInt(parts[2]);
        if (parts[1].equals("start")) stack.push(Integer.parseInt(parts[0]));
        else {
            res[stack.pop()]++;
            prevTime++;
        }
    }
    return res;
}
```
</p>


### Java solution using stack, wrapper class and calculation when pop element from the stack.
- Author: AntiGameZ
- Creation Date: Wed Jul 25 2018 15:35:55 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 04:38:33 GMT+0800 (Singapore Standard Time)

<p>
* Extract the log parsing logic as a inner class.
* Calculate the function\'s running time when encounter an "end" log entry. If current ended func has a main func still running (in the stack), substract the running time advance. So we **don\'t need** to use a "prev" variable.
* Another idea is using a field in the inner class to track the real running time for a function. I believe this way would be the most straightforward for myself.
* Both methods follows the O(n) time complexiy, and O(n/2) extra space consumption.

Method 1
``` JAVA
class Solution {
    public int[] exclusiveTime(int n, List<String> logs) {
        Deque<Log> stack = new ArrayDeque<>();
        int[] result = new int[n];
        for (String content : logs) {
            Log log = new Log(content);
            if (log.isStart) {
                stack.push(log);
            } else {
                Log top = stack.pop();
                result[top.id] += (log.time - top.time + 1);
                if (!stack.isEmpty()) {
                    result[stack.peek().id] -= (log.time - top.time + 1);
                }
            }
        }
        
        return result;
    }
    
    public static class Log {
        public int id;
        public boolean isStart;
        public int time;
        
        public Log(String content) {
            String[] strs = content.split(":");
            id = Integer.valueOf(strs[0]);
            isStart = strs[1].equals("start");
            time = Integer.valueOf(strs[2]);
        }
    }
}
```

Method 2
``` JAVA
class Solution {
    public int[] exclusiveTime(int n, List<String> logs) {
        Deque<Log> stack = new ArrayDeque<>();
        int[] result = new int[n];
        int duration = 0;
        for (String content : logs) {
            Log log = new Log(content);
            if (log.isStart) {
                stack.push(log);
            } else {
                Log top = stack.pop();
                result[top.id] += (log.time - top.time + 1 - top.subDuration);
                if (!stack.isEmpty()) {
                    stack.peek().subDuration += (log.time - top.time + 1);
                }
            }
        }
        
        return result;
    }
    
    public static class Log {
        public int id;
        public boolean isStart;
        public int time;
        public int subDuration;
        
        public Log(String content) {
            String[] strs = content.split(":");
            id = Integer.valueOf(strs[0]);
            isStart = strs[1].equals("start");
            time = Integer.valueOf(strs[2]);
            subDuration = 0;
        }
    }
}
```
</p>


