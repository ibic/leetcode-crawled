---
title: "Largest Unique Number"
weight: 1007
#id: "largest-unique-number"
---
## Description
<div class="description">
<p>Given an array of integers <code>A</code>, return the largest integer that only occurs once.</p>

<p>If no integer occurs once, return -1.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[5,7,3,9,4,9,8,3,1]</span>
<strong>Output: </strong><span id="example-output-1">8</span>
<strong>Explanation: </strong>
The maximum integer in the array is 9 but it is repeated. The number 8 occurs only once, so it&#39;s the answer.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[9,9,8,8]</span>
<strong>Output: </strong><span id="example-output-1">-1</span>
<strong>Explanation: </strong>
There is no number that occurs only once.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 2000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 1000</code></li>
</ol>

</div>

## Tags
- Array (array)
- Hash Table (hash-table)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n) Solution using HashMap
- Author: Sycmtic
- Creation Date: Sat Sep 07 2019 08:03:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 07 2019 08:03:18 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int largestUniqueNumber(int[] A) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int a : A) map.put(a, map.getOrDefault(a, 0) + 1);

        int res = -1;
        for (int a : A) {
            if (map.get(a) == 1 && a > res) res = a;
        }
        return res;
    }
}
```
</p>


### [Python 3] One liner
- Author: vilchinsky
- Creation Date: Sun Jul 28 2019 00:11:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 28 2019 00:11:26 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def largestUniqueNumber(self, A: List[int]) -> int:        
        return max([-1] + [k for k,v in collections.Counter(A).items() if v == 1])
```
</p>


### C++ O(n), 4 lines
- Author: votrubac
- Creation Date: Sun Jul 28 2019 00:13:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 28 2019 00:13:42 GMT+0800 (Singapore Standard Time)

<p>
```
int largestUniqueNumber(vector<int>& A) {
  short a[1001] = {}, i = 0;
  for (auto n : A) ++a[n];
  for (i = 1000; i >= 0 && a[i] != 1; --i);
  return i;
}
```
</p>


