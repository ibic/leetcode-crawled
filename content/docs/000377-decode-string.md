---
title: "Decode String"
weight: 377
#id: "decode-string"
---
## Description
<div class="description">
<p>Given an encoded string, return its decoded string.</p>

<p>The encoding rule is: <code>k[encoded_string]</code>, where the <i>encoded_string</i> inside the square brackets is being repeated exactly <i>k</i> times. Note that <i>k</i> is guaranteed to be a positive integer.</p>

<p>You may assume that the input string is always valid; No extra white spaces, square brackets are well-formed, etc.</p>

<p>Furthermore, you may assume that the original data does not contain any digits and that digits are only for those repeat numbers, <i>k</i>. For example, there won&#39;t be input like <code>3a</code> or <code>2[4]</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> s = "3[a]2[bc]"
<strong>Output:</strong> "aaabcbc"
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> s = "3[a2[c]]"
<strong>Output:</strong> "accaccacc"
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> s = "2[abc]3[cd]ef"
<strong>Output:</strong> "abcabccdcdcdef"
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> s = "abc3[cd]xyz"
<strong>Output:</strong> "abccdcdcdxyz"
</pre>
</div>

## Tags
- Stack (stack)
- Depth-first Search (depth-first-search)

## Companies
- Bloomberg - 30 (taggedByAdmin: false)
- Amazon - 17 (taggedByAdmin: false)
- Cisco - 15 (taggedByAdmin: false)
- Google - 13 (taggedByAdmin: true)
- Microsoft - 4 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Hulu - 2 (taggedByAdmin: false)
- Facebook - 7 (taggedByAdmin: true)
- Atlassian - 2 (taggedByAdmin: false)
- Tencent - 2 (taggedByAdmin: false)
- Huawei - 5 (taggedByAdmin: false)
- AppDynamics - 4 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- Cruise Automation - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Coupang - 0 (taggedByAdmin: true)
- Yelp - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Overview
We are given string $$s$$ in a particular form `k[string]` and we have to decode it  as `string` repeated `k` times . For example,`2[b]` is decoded as  `bb`.

 The problem seems straightforward at first glance. But the trick here is that there can be nested encoded strings like `k[string k[string]]`. For example, string s =`3[a2[c]]`. In such cases, we must decode the innermost string and continue in an outward direction until the entire string is decoded.

 ![img](../Figures/394/decode_overview.png)


If you have solved similar problem such as [Evaluate Polish Notation](https://leetcode.com/problems/evaluate-reverse-polish-notation/) or [Simplify Path](https://leetcode.com/problems/simplify-path/) , it is clear that [Stack Data Structure](https://en.wikipedia.org/wiki/Stack_(abstract_data_type))  is best suited to implement such problems. We could implement a stack data structure or recursively build the solution by using an internal call stack. Let's understand both approaches in detail.

---
#### Approach 1: Using Stack

**Intuition**

We have to decode the result in a particular pattern. We know that the input is always valid. The pattern begins with a number `k`, followed by opening braces `[`, followed by `string`. Post that, there could be one of the following cases :
1) There is another nested pattern `k[string k[string]]`
2) There is a closing bracket  `k[string]`

Since we have to start decoding the innermost pattern first, continue iterating over the string `s`, pushing each character to the stack until it is not a closing bracket `]`.  Once we encounter the closing bracket `]`, we must start decoding the pattern.

As we know that stack follows the Last In First Out (LIFO) Principle, the top of the stack would have the data we must decode.

**Algorithm**

The input can contain an alphabet `(a-z)`, digit `(0-9)`, opening braces `[` or closing braces `]`. Start traversing string `s` and process each character based on the following rules:

Case 1) Current character is not a closing bracket `]`.

Push the current character to stack.

Case 2) Current character is a closing bracket `]`.

Start decoding the last traversed string by popping the string `decodedString` and number `k` from the top of the stack.
- Pop from the stack until the next character is not an opening bracket `[` and append each character (`a-z`) to the `decodedString`.
- Pop opening bracket `[` from the stack.
- Pop from the stack until the next character is a digit `(0-9)` and build the number `k`.

Now that we have `k` and `decodedString` , decode the pattern `k[decodedString]`  by pushing the `decodedString` to stack `k` times.

Once the entire string is traversed, pop the `result` from stack and return.

!?!../Documents/394_LIS.json:1430,1004!?!

**Implementation**

<iframe src="https://leetcode.com/playground/dTjdfLc4/shared" frameBorder="0" width="100%" height="500" name="dTjdfLc4"></iframe>

**Complexity Analysis**

Assume, $$n$$ is the length of the string $$s$$.
* Time Complexity: $$\mathcal{O}(\text{maxK} \cdot n)$$,  where $$\text{maxK}$$ is the maximum value of $$k$$ in the string $$s$$.
Example, for s = `1000[a200b]]`, $$\text{maxK}$$ is $$1000$$

* Space Complexity: $$\mathcal{O}(\text{maxK} \cdot n)$$, where $$\text{maxK}$$ is the maximum value of $$k$$ in the string $$s$$.
As we are pushing each `decodedString` to stack `k` times, the maximum stack space required could be $$\mathcal{O}(\text{maxK} \cdot n)$$.
---
#### Approach 2: Using Recursion

**Intuition**

In the previous approach, we implemented an external stack to keep the track of each character traversed. Ideally, a stack is required when we have nested encoded string in the form `k[string k[string]]`.

Using this intuition, we could start by building `k` and `string` and recursively decode for each nested substring. The recursion uses an internal call stack to store the previous state. Let's understand the algorithm in detail.

**Algorithm**

-  Build `result` until next character is letter `(a-z)` and build the number k until next character is a digit `(0-9)` by iterating over string `s`.
- Ignore the next `[` character and recursively find the nested `decodedString`.
- Decode the current pattern `k[decodedString]` and append it to the result.
- Return the current `result`.

The above steps are repeated recursively for each pattern until the entire string `s` is traversed.

Base Condition: We must define a base condition that must be satisfied to backtrack from the recursive call.
In this case, we would backtrack and return the `result` when we have traversed the string `s` or the next character is `]` and there is no nested substring.

Thanks to [@bluedawnstar](https://leetcode.com/bluedawnstar/) for suggesting the solution.

**Implementation**

<iframe src="https://leetcode.com/playground/d3EHGxv3/shared" frameBorder="0" width="100%" height="500" name="d3EHGxv3"></iframe>

**Complexity Analysis**

Assume, $$n$$ is the length of the string $$s$$.
* Time Complexity: $$\mathcal{O}(\text{maxK} \cdot n)$$ as in _Approach 1_

* Space Complexity: $$\mathcal{O}(n)$$. This is the space used to store the internal call stack used for recursion. As we are recursively decoding each nested pattern, the maximum depth of recursive call stack would not be more than $$n$$

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python solution using stack
- Author: simkieu
- Creation Date: Thu Sep 15 2016 12:00:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 05:37:04 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def decodeString(self, s):
        stack = []; curNum = 0; curString = ''
        for c in s:
            if c == '[':
                stack.append(curString)
                stack.append(curNum)
                curString = ''
                curNum = 0
            elif c == ']':
                num = stack.pop()
                prevString = stack.pop()
                curString = prevString + num*curString
            elif c.isdigit():
                curNum = curNum*10 + int(c)
            else:
                curString += c
        return curString
```
</p>


### Simple Java Solution using Stack
- Author: sampsonchan
- Creation Date: Mon Sep 05 2016 07:41:14 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 23:27:08 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public String decodeString(String s) {
        String res = "";
        Stack<Integer> countStack = new Stack<>();
        Stack<String> resStack = new Stack<>();
        int idx = 0;
        while (idx < s.length()) {
            if (Character.isDigit(s.charAt(idx))) {
                int count = 0;
                while (Character.isDigit(s.charAt(idx))) {
                    count = 10 * count + (s.charAt(idx) - '0');
                    idx++;
                }
                countStack.push(count);
            }
            else if (s.charAt(idx) == '[') {
                resStack.push(res);
                res = "";
                idx++;
            }
            else if (s.charAt(idx) == ']') {
                StringBuilder temp = new StringBuilder (resStack.pop());
                int repeatTimes = countStack.pop();
                for (int i = 0; i < repeatTimes; i++) {
                    temp.append(res);
                }
                res = temp.toString();
                idx++;
            }
            else {
                res += s.charAt(idx++);
            }
        }
        return res;
    }
}
</p>


### 0ms simple C++ solution
- Author: bluedawnstar
- Creation Date: Mon Sep 05 2016 12:00:17 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 15:55:59 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    string decodeString(const string& s, int& i) {
        string res;
        
        while (i < s.length() && s[i] != ']') {
            if (!isdigit(s[i]))
                res += s[i++];
            else {
                int n = 0;
                while (i < s.length() && isdigit(s[i]))
                    n = n * 10 + s[i++] - '0';
                    
                i++; // '['
                string t = decodeString(s, i);
                i++; // ']'
                
                while (n-- > 0)
                    res += t;
            }
        }
        
        return res;
    }

    string decodeString(string s) {
        int i = 0;
        return decodeString(s, i);
    }
};
```
</p>


