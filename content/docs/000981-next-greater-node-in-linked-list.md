---
title: "Next Greater Node In Linked List"
weight: 981
#id: "next-greater-node-in-linked-list"
---
## Description
<div class="description">
<p>We are given a linked list with&nbsp;<code>head</code>&nbsp;as the first node.&nbsp; Let&#39;s number the&nbsp;nodes in the list: <code>node_1, node_2, node_3, ...</code> etc.</p>

<p>Each node may have a <em>next larger</em> <strong>value</strong>: for <code>node_i</code>,&nbsp;<code>next_larger(node_i)</code>&nbsp;is the <code>node_j.val</code> such that <code>j &gt; i</code>, <code>node_j.val &gt; node_i.val</code>, and <code>j</code> is the smallest possible choice.&nbsp; If such a <code>j</code>&nbsp;does not exist, the next larger value is <code>0</code>.</p>

<p>Return an array of integers&nbsp;<code>answer</code>, where <code>answer[i] = next_larger(node_{i+1})</code>.</p>

<p>Note that in the example <strong>inputs</strong>&nbsp;(not outputs) below, arrays such as <code>[2,1,5]</code>&nbsp;represent the serialization of a linked list with a head node value of 2, second node value of 1, and third node value of 5.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[2,1,5]</span>
<strong>Output: </strong><span id="example-output-1">[5,5,0]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[2,7,4,3,5]</span>
<strong>Output: </strong><span id="example-output-2">[7,0,5,5,0]</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[1,7,5,1,9,2,5,1]</span>
<strong>Output: </strong><span id="example-output-3">[7,9,9,9,0,5,0,0]</span>
</pre>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ol>
	<li><code><span>1 &lt;= node.val&nbsp;&lt;= 10^9</span></code><span>&nbsp;for each node in the linked list.</span></li>
	<li>The given list has length in the range <code>[0, 10000]</code>.</li>
</ol>
</div>
</div>
</div>
</div>

## Tags
- Linked List (linked-list)
- Stack (stack)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Next Greater Element
- Author: lee215
- Creation Date: Sun Mar 31 2019 12:02:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 31 2019 12:02:48 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**:
Very similar to this problem [503. Next Greater Element II](https://leetcode.com/problems/next-greater-element-ii/discuss/98270/)
<br>
## **Time Complexity**:
O(N) Time, O(N) Space
<br>
**Java:**
Transform the linked list to an arraylist,
then it\'s a normal "next larger element" problem,
solved by stack.

```
    public int[] nextLargerNodes(ListNode head) {
        ArrayList<Integer> A = new ArrayList<>();
        for (ListNode node = head; node != null; node = node.next)
            A.add(node.val);
        int[] res = new int[A.size()];
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < A.size(); ++i) {
            while (!stack.isEmpty() && A.get(stack.peek()) < A.get(i))
                res[stack.pop()] = A.get(i);
            stack.push(i);
        }
        return res;
    }
```


**C++:**
Push nodes\' values to `vector<int> res`.
`vector<int> stack` will save the indices of elements that need to find next greater element.
In the end, we reset 0 to all elements that have no next greater elements.

```
    vector<int> nextLargerNodes(ListNode* head) {
        vector<int> res, stack;
        for (ListNode* node = head; node; node = node->next) {
            while (stack.size() && res[stack.back()] < node->val) {
                res[stack.back()] = node->val;
                stack.pop_back();
            }
            stack.push_back(res.size());
            res.push_back(node->val);
        }
        for (int i: stack) res[i] = 0;
        return res;
    }
```

**Python:**
Save <index, value> pair to the stack.

```
    def nextLargerNodes(self, head):
        res, stack = [], []
        while head:
            while stack and stack[-1][1] < head.val:
                res[stack.pop()[0]] = head.val
            stack.append([len(res), head.val])
            res.append(0)
            head = head.next
        return res
```

</p>


### C++ O(n), stack
- Author: votrubac
- Creation Date: Sun Mar 31 2019 12:29:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 31 2019 12:29:15 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
We can convert list into the array (we need to return an array anyways). So, this problem becomes "Next greater element". Search on YouTube for this title, there are tons of videos there.

# Solution
We go right-to-left, and maintain the monotonically decreasing stack:
- We remove elements from the stack until the top of the stack is larger than the current element.
  - We do not need those elements - the current value will be used instead for the remaining elements.
- The top of the stack is now our next greater element.
- We push the current element to the stack.
```
vector<int> nextLargerNodes(ListNode* h) {
  vector<int> res, stack;
  for (auto p = h; p != nullptr; p = p->next) res.push_back(p->val);
  for (int i = res.size() - 1; i >= 0; --i) {
    auto val = res[i];
    while (!stack.empty() && stack.back() <= res[i]) stack.pop_back();
    res[i] = stack.empty() ? 0 : stack.back();
    stack.push_back(val);
  }
  return res;
}
```
## Complexity Analysis
Runtime: *O(n)*. We process each input element no more than 2 times.
Memory: *O(n)*. We use up to *n* memory for the stack.
</p>


### Java - one pass O(n) time, O(n) space - code with comments :)
- Author: JeanBean
- Creation Date: Sat May 11 2019 00:59:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 11 2019 00:59:08 GMT+0800 (Singapore Standard Time)

<p>
O(n) runtime due to passing through the LinkedList once (list of size n).
O(n) space due to Stack, ArrayList, fixed array `result` for outputting at the very end.

```java
public int[] nextLargerNodes(ListNode head) {
    
	// Keeps track of indices of values in nums
    Stack<Integer> stack = new Stack<>();
	
	// Store node values as we go, 
	// updates to output value ("next greatest") within while loop as we see them
    List<Integer> nums = new ArrayList<>();
    
    ListNode n = head;
	
	// For generating the corresponding index in nums as we step through LinkedList
    int index = 0;
    
    while (n != null) {
      
      nums.add(n.val);
      
	  // Process anything that is less than current node value
	  // i.e. current node value is the "next"greatest for elements (index-referenced) in the stack
      while (!stack.isEmpty() && nums.get(stack.peek()) < n.val) {
        nums.set(stack.pop(), n.val);
      }
      
	  // Set up for next iteration. 
	  // Note: Every node gets into the stack.
      stack.push(index);
      n = n.next;
      index++;
    }
    
    // Handle remaining items in stack / write in 0 (no "next greatest" found for these)
    while (!stack.isEmpty()) {
      nums.set(stack.pop(), 0);
    }
    
    // Format output
    int[] result = new int[nums.size()];
    for (int i = 0; i < result.length; i++) {
      result[i] = nums.get(i);
    }
    
    return result;
  }
```
</p>


