---
title: "Stickers to Spell Word"
weight: 623
#id: "stickers-to-spell-word"
---
## Description
<div class="description">
<p>
We are given N different types of stickers.  Each sticker has a lowercase English word on it.
</p><p>
You would like to spell out the given <code>target</code> string by cutting individual letters from your collection of stickers and rearranging them.
</p><p>
You can use each sticker more than once if you want, and you have infinite quantities of each sticker.
</p><p>
What is the minimum number of stickers that you need to spell out the <code>target</code>?  If the task is impossible, return -1.
</p>

<p><b>Example 1:</b></p>
<p>Input:<pre>
["with", "example", "science"], "thehat"
</pre></p>

<p>Output:<pre>
3
</pre></p>

<p>Explanation:<pre>
We can use 2 "with" stickers, and 1 "example" sticker.
After cutting and rearrange the letters of those stickers, we can form the target "thehat".
Also, this is the minimum number of stickers necessary to form the target string.
</pre></p>

<p><b>Example 2:</b></p>
<p>Input:<pre>
["notice", "possible"], "basicbasic"
</pre></p>

<p>Output:<pre>
-1
</pre></p>

<p>Explanation:<pre>
We can't form the target "basicbasic" from cutting letters from the given stickers.
</pre></p>

<p><b>Note:</b>
<li><code>stickers</code> has length in the range <code>[1, 50]</code>.</li>
<li><code>stickers</code> consists of lowercase English words (without apostrophes).</li>
<li><code>target</code> has length in the range <code>[1, 15]</code>, and consists of lowercase English letters.</li>
<li>In all test cases, all words were chosen <u>randomly</u> from the 1000 most common US English words, and the target was chosen as a concatenation of two random words.</li>
<li>The time limit may be more challenging than usual.  It is expected that a 50 sticker test case can be solved within 35ms on average.</li>
</p>
</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Backtracking (backtracking)

## Companies
- IXL - 2 (taggedByAdmin: true)
- Facebook - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach 1: Optimized Exhaustive Search

<br>

**Intuition**

A natural answer is to exhaustively search combinations of stickers.  Because the data is randomized, there are many heuristics available to us that will make this faster.

* For all stickers, we can ignore any letters that are not in the target word.

* When our candidate answer won't be smaller than an answer we have already found, we can stop searching this path.

* We should try to have our exhaustive search bound the answer as soon as possible, so the effect described in the above point happens more often.

* When a sticker dominates another, we shouldn't include the dominated sticker in our sticker collection.  [Here, we say a sticker `A` dominates `B` if `A.count(letter) >= B.count(letter)` for all letters.]

<br>

**Algorithm**

Firstly, for each sticker, let's create a count of that sticker (a mapping `letter -> sticker.count(letter)`) that does not consider letters not in the target word.  Let `A` be an array of these counts.  Also, let's create `t_count`, a count of our `target` word.

Secondly, let's remove dominated stickers.  Because dominance is a transitive relation, we only need to check if a sticker is not dominated by any other sticker once - the ones that aren't dominated are included in our collection.

We are now ready to begin our exhaustive search.  A call to `search(ans)` denotes that we want to decide the minimum number of stickers we can used in `A` to satisfy the target count `t_count`.  `ans` will store the currently formed answer, and `best` will store the current best answer.

If our current answer can't beat our current best answer, we should stop searching.  Also, if there are no stickers left and our target is satisfied, we should update our answer.

Otherwise, we want to know the maximum number of this sticker we can use.  For example, if this sticker is `'abb'` and our target is `'aaabbbbccccc'`, then we could use a maximum of 3 stickers.  This is the maximum of `math.ceil(target.count(letter) / sticker.count(letter))`, taken over all `letter`s in `sticker`.  Let's call this quantity `used`.

After, for the sticker we are currently considering, we try to use `used` of them, then `used - 1`, `used - 2` and so on.  The reason we do it in this order is so that we can arrive at a value for `best` more quickly, which will stop other branches of our exhaustive search from continuing.

The Python version of this solution showcases using `collections.Counter` as a way to simplify some code sections, whereas the Java solution sticks to arrays.

<iframe src="https://leetcode.com/playground/KP3fS7G3/shared" frameBorder="0" name="KP3fS7G3" width="100%" height="515"></iframe>

<br>

**Complexity Analysis**

* Time Complexity: Let $$N$$ be the number of stickers, and $$T$$ be the number of letters in the target word.  A bound for time complexity is $$O(N^{T+1} T^2)$$: for each sticker, we'll have to try using it up to $$T+1$$ times, and updating our target count costs $$O(T)$$, which we do up to $$T$$ times.  Alternatively, since the answer is bounded at $$T$$, we can prove that we can only search up to $$\binom{N+T-1}{T-1}$$ times.  This would be $$O(\binom{N+T-1}{T-1} T^2)$$.

* Space Complexity: $$O(N+T)$$, to store `stickersCount`, `targetCount`, and handle the recursive callstack when calling `search`.

<br>

---
#### Approach 2: Dynamic Programming

<br>

**Intuition**

Suppose we need `dp[state]` stickers to satisfy all `target[i]`'s for which the `i`-th bit of `state` is set.  We would like to know `dp[(1 << len(target)) - 1]`.

<br>

**Algorithm**

For each `state`, let's work with it as `now` and look at what happens to it after applying a sticker.  For each letter in the sticker that can satisfy an unset bit of `state`, we set the bit (`now |= 1 << i`).  At the end, we know `now` is the result of applying that sticker to `state`, and we update our `dp` appropriately.

When using Python, we will need some extra techniques from *Approach #1* to pass in time.

<iframe src="https://leetcode.com/playground/JTZ2SYco/shared" frameBorder="0" name="JTZ2SYco" width="100%" height="515"></iframe>

<br>


**Complexity Analysis**

* Time Complexity: $$O(2^T * S * T)$$ where $$S$$ be the total number of letters in all stickers, and $$T$$ be the number of letters in the target word.  We can examine each loop carefully to arrive at this conclusion.

* Space Complexity: $$O(2^T)$$, the space used by `dp`.

<br>

## Accepted Submission (python3)
```python3
from collections import Counter
from functools import reduce
import operator

class Solution:
    def minStickers(self, stickers: List[str], target: str) -> int:
        def removeUnusedLetters(counter):
            for key in list(counter):
                if key not in targetCounter:
                    del counter[key]
            return counter

        def isDominated(child, parent):
            return all(parent[key] >= child[key] for key in child)

        def removeDominated(counters):
            #lenc = reduce(operator.add, (1 for _ in counters))
            lenc = len(counters)
            i = lenc - 1
            while i >= 0: # delete from the back
                if any(isDominated(counters[i], counters[j]) for j in range(i)) or \
                    any(isDominated(counters[i], counters[j]) for j in range(i + 1, lenc)):
                    del counters[i]
                    lenc -= 1
                i -= 1

        def counterToStr(counter):
            return ''.join(k+str(counter[k]) for k in sorted(counter) if counter[k] > 0)
        
        def dfs(tCounter):
            #print(tCounter)
            tcstr = counterToStr(tCounter)
            if tcstr not in memory:
                #r = 1 + min(dfs(tCounter - sticker) for sticker in stickerCounters if any(c in tCounter for c in sticker))
                r = 1 + min((dfs(tCounter - sticker) for sticker in stickerCounters if next(iter(tCounter)) in sticker), default=float('inf'))
                memory[tcstr] = r
            return memory[tcstr]
        # entry starts here
        memory = {'': 0}
        targetCounter = Counter(target)
        stickerCounters = list(map(
            removeUnusedLetters,
            map(
                lambda x: Counter(x), stickers)))
        removeDominated(stickerCounters)
        r = dfs(targetCounter)
        return -1 if float('inf') == r else r
```

## Top Discussions
### C++/Java/Python, DP + Memoization with optimization, 29 ms (C++)
- Author: zestypanda
- Creation Date: Sun Oct 08 2017 11:08:40 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 11:23:02 GMT+0800 (Singapore Standard Time)

<p>
There are potentially a lot of overlapping sub problems, but meanwhile we don't exactly know what those sub problems are. DP with memoization works pretty well in such cases. The workflow is like backtracking, but with memoization. Here I simply use a sorted string of target as the key for the unordered_map DP. A sorted target results in a unique sub problem for possibly different strings.
```
dp[s] is the minimum stickers required for string s (-1 if impossible). Note s is sorted.
clearly, dp[""] = 0, and the problem asks for dp[target].
```
The DP formula is:
```
dp[s] = min(1+dp[reduced_s]) for all stickers, 
here reduced_s is a new string after certain sticker applied
```
Optimization: If the target can be spelled out by a group of stickers, at least one of them has to contain character target[0]. So I explicitly require next sticker containing target[0], which significantly reduced the search space.

BTW, I am not good at Java. Looking forward to your revision!

C++
```
class Solution {
public:
    int minStickers(vector<string>& stickers, string target) {
        int m = stickers.size();
        vector<vector<int>> mp(m, vector<int>(26, 0));
        unordered_map<string, int> dp;
        // count characters a-z for each sticker 
        for (int i = 0; i < m; i++) 
            for (char c:stickers[i]) mp[i][c-'a']++;
        dp[""] = 0;
        return helper(dp, mp, target);
    }
private:
    int helper(unordered_map<string, int>& dp, vector<vector<int>>& mp, string target) {
        if (dp.count(target)) return dp[target];
        int ans = INT_MAX, n = mp.size();
        vector<int> tar(26, 0);
        for (char c:target) tar[c-'a']++;
        // try every sticker
        for (int i = 0; i < n; i++) {
            // optimization
            if (mp[i][target[0]-'a'] == 0) continue; 
            string s;
            // apply a sticker on every character a-z
            for (int j = 0; j < 26; j++) 
                if (tar[j]-mp[i][j] > 0) s += string(tar[j]-mp[i][j], 'a'+j);
            int tmp = helper(dp, mp, s);
            if (tmp!= -1) ans = min(ans, 1+tmp);
        }
        dp[target] = ans == INT_MAX? -1:ans;
        return dp[target];
    }
};
```
Java 
```
class Solution {
    public int minStickers(String[] stickers, String target) {
        int m = stickers.length;
        int[][] mp = new int[m][26];
        Map<String, Integer> dp = new HashMap<>();
        for (int i = 0; i < m; i++) 
            for (char c:stickers[i].toCharArray()) mp[i][c-'a']++;
        dp.put("", 0);
        return helper(dp, mp, target);
    }
    private int helper(Map<String, Integer> dp, int[][] mp, String target) {
        if (dp.containsKey(target)) return dp.get(target);
        int ans = Integer.MAX_VALUE, n = mp.length;
        int[] tar = new int[26];
        for (char c:target.toCharArray()) tar[c-'a']++;
        // try every sticker
        for (int i = 0; i < n; i++) {
            // optimization
            if (mp[i][target.charAt(0)-'a'] == 0) continue;
            StringBuilder sb = new StringBuilder();
            // apply a sticker on every character a-z
            for (int j = 0; j < 26; j++) {
                if (tar[j] > 0 ) 
                    for (int k = 0; k < Math.max(0, tar[j]-mp[i][j]); k++)
                        sb.append((char)('a'+j));
            }
            String s = sb.toString();
            int tmp = helper(dp, mp, s);
            if (tmp != -1) ans = Math.min(ans, 1+tmp);
        }
        dp.put(target, ans == Integer.MAX_VALUE? -1:ans);
        return dp.get(target);
    }
}
```
Python
```
class Solution(object):
    def minStickers(self, stickers, target):
        m = len(stickers)
        mp = [[0]*26 for y in range(m)] 
        for i in range(m):
            for c in stickers[i]:
                mp[i][ord(c)-ord('a')] += 1    
        dp = {}
        dp[""] = 0
        
        def helper(dp, mp, target):
            if target in dp:
                return dp[target]
            n = len(mp)
            tar = [0]*26
            for c in target:
                tar[ord(c)-ord('a')] += 1   
            ans = sys.maxint
            for i in xrange(n):
                if mp[i][ord(target[0])-ord('a')] == 0:
                    continue
                s = ''
                for j in range(26):
                    if tar[j] > mp[i][j]:
                        s += chr(ord('a')+j)*(tar[j] - mp[i][j]) 
                tmp = helper(dp, mp, s)
                if (tmp != -1): 
                    ans = min(ans, 1+tmp)    
            dp[target] = -1 if ans == sys.maxint else ans
            return dp[target]
                
        return helper(dp, mp, target)
```
</p>


### Rewrite of contest winner's solution
- Author: StefanPochmann
- Creation Date: Sun Oct 08 2017 20:11:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 08 2017 20:11:46 GMT+0800 (Singapore Standard Time)

<p>
@dreamoon's [contest solution](https://leetcode.com/contest/leetcode-weekly-contest-53/ranking) is quite nice, but it uses lots of macros. I rewrote it without them and also made it faster (about 130 ms vs 200 ms). Thanks to @feng-peng-3597 for [pointing it out](https://discuss.leetcode.com/topic/106337/dreamoon-s-code-with-comment-added-by-me).

    int minStickers(vector<string>& stickers, string target) {
        int n = target.size(), N = 1 << n;
        vector<uint> dp(N, -1);
        dp[0] = 0;
        for (int i = 0; i < N; i++) if (dp[i] != -1) {
            for (string& s : stickers) {
                int now = i;
                for (char c : s) {
                    for (int r = 0; r < n; r++) {
                        if (target[r] == c && !((now >> r) & 1)) {
                            now |= 1 << r;
                            break;
                        }
                    }
                }
                dp[now] = min(dp[now], dp[i] + 1);
            }
        }
        return dp[N-1];
    }
</p>


### My Python DFS Solution
- Author: cenkay
- Creation Date: Mon Aug 20 2018 00:35:21 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 20 2018 00:35:21 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def minStickers(self, stickers, target):
        cnt, res, n = collections.Counter(target), [float("inf")], len(target)  
        def dfs(dic, used, i):
            if i == n: res[0] = min(res[0], used)
            elif dic[target[i]] >= cnt[target[i]]: dfs(dic, used, i + 1)
            elif used < res[0] - 1:
                for sticker in stickers:
                    if target[i] in sticker:
                        for s in sticker: dic[s] += 1
                        dfs(dic, used + 1, i + 1)
                        for s in sticker: dic[s] -= 1
        dfs(collections.defaultdict(int), 0, 0)
        return res[0] < float("inf") and res[0] or -1
```
</p>


