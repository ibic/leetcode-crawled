---
title: "Design Tic-Tac-Toe"
weight: 331
#id: "design-tic-tac-toe"
---
## Description
<div class="description">
<p>Assume the following rules are for the tic-tac-toe game on an <code>n x n</code> board between two players:</p>

<ol>
	<li>A move is guaranteed to be valid and is placed on an empty block.</li>
	<li>Once a winning condition is reached, no more moves are allowed.</li>
	<li>A player who succeeds in placing <code>n</code> of their marks in a horizontal, vertical, or diagonal row wins the game.</li>
</ol>

<p>Implement the&nbsp;<code>TicTacToe</code> class:</p>

<ul>
	<li><code>TicTacToe(int n)</code> Initializes the object the size of the board <code>n</code>.</li>
	<li><code>int move(int row, int col, int player)</code> Indicates that player with id <code>player</code> plays at the cell <code>(row, col)</code> of the board. The move is guaranteed to be a valid move.</li>
</ul>

<p><b>Follow up:</b><br />
Could you do better than <code>O(<i>n</i><sup>2</sup>)</code> per <code>move()</code> operation?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;TicTacToe&quot;, &quot;move&quot;, &quot;move&quot;, &quot;move&quot;, &quot;move&quot;, &quot;move&quot;, &quot;move&quot;, &quot;move&quot;]
[[3], [0, 0, 1], [0, 2, 2], [2, 2, 1], [1, 1, 2], [2, 0, 1], [1, 0, 2], [2, 1, 1]]
<strong>Output</strong>
[null, 0, 0, 0, 0, 0, 0, 1]

<strong>Explanation</strong>
TicTacToe ticTacToe = new TicTacToe(3);
Assume that player 1 is &quot;X&quot; and player 2 is &quot;O&quot; in the board.
ticTacToe.move(0, 0, 1); // return 0 (no one wins)
|X| | |
| | | |    // Player 1 makes a move at (0, 0).
| | | |

ticTacToe.move(0, 2, 2); // return 0 (no one wins)
|X| |O|
| | | |    // Player 2 makes a move at (0, 2).
| | | |

ticTacToe.move(2, 2, 1); // return 0 (no one wins)
|X| |O|
| | | |    // Player 1 makes a move at (2, 2).
| | |X|

ticTacToe.move(1, 1, 2); // return 0 (no one wins)
|X| |O|
| |O| |    // Player 2 makes a move at (1, 1).
| | |X|

ticTacToe.move(2, 0, 1); // return 0 (no one wins)
|X| |O|
| |O| |    // Player 1 makes a move at (2, 0).
|X| |X|

ticTacToe.move(1, 0, 2); // return 0 (no one wins)
|X| |O|
|O|O| |    // Player 2 makes a move at (1, 0).
|X| |X|

ticTacToe.move(2, 1, 1); // return 1&nbsp;(player 1 wins)
|X| |O|
|O|O| |    // Player 1 makes a move at (2, 1).
|X|X|X|
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 100</code></li>
	<li>player is <code>1</code> or <code>2</code>.</li>
	<li><code>1 &lt;= row, col &lt;= n</code></li>
	<li><code>(row, col)</code>&nbsp;are <strong>unique</strong> for each different call to <code>move</code>.</li>
	<li>At most <code>n<sup>2</sup></code> calls will be made to <code>move</code>.</li>
</ul>

</div>

## Tags
- Design (design)

## Companies
- Amazon - 14 (taggedByAdmin: false)
- Facebook - 8 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- TripleByte - 11 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Uber - 3 (taggedByAdmin: false)
- DoorDash - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(1) solution, easy to understand
- Author: bdwalker
- Creation Date: Thu May 05 2016 11:14:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 06:48:48 GMT+0800 (Singapore Standard Time)

<p>
Initially, I had not read the Hint in the question and came up with an O(n) solution.  After reading the extremely helpful hint; a much easier approach became apparent.  The key observation is that in order to win Tic-Tac-Toe you must have the entire row or column.  Thus, we don't need to keep track of an entire n^2 board.  We only need to keep a count for each row and column.  If at any time a row or column matches the size of the board then that player has won. 

To keep track of which player, I add one for Player1 and -1 for Player2.  There are two additional variables to keep track of the count of the diagonals.  Each time a player places a piece we just need to check the count of that row, column, diagonal and anti-diagonal. 

Also see a very similar answer that I believe had beaten me to the punch.  We came up with our solutions independently but they are very similar in principle. 
[Aeonaxx's soln][1]

    public class TicTacToe {
    private int[] rows;
    private int[] cols;
    private int diagonal;
    private int antiDiagonal;
    
    /** Initialize your data structure here. */
    public TicTacToe(int n) {
        rows = new int[n];
        cols = new int[n];
    }
    
    /** Player {player} makes a move at ({row}, {col}).
        @param row The row of the board.
        @param col The column of the board.
        @param player The player, can be either 1 or 2.
        @return The current winning condition, can be either:
                0: No one wins.
                1: Player 1 wins.
                2: Player 2 wins. */
    public int move(int row, int col, int player) {
        int toAdd = player == 1 ? 1 : -1;
        
        rows[row] += toAdd;
        cols[col] += toAdd;
        if (row == col)
        {
            diagonal += toAdd;
        }
        
        if (col == (cols.length - row - 1))
        {
            antiDiagonal += toAdd;
        }
        
        int size = rows.length;
        if (Math.abs(rows[row]) == size ||
            Math.abs(cols[col]) == size ||
            Math.abs(diagonal) == size  ||
            Math.abs(antiDiagonal) == size)
        {
            return player;
        }
        
        return 0;
    }
}


  [1]: https://leetcode.com/discuss/101123/simple-o-1-time-c-solution-following-provided-hints
</p>


### Python 13 lines, easy to understand
- Author: newman2
- Creation Date: Sun Aug 21 2016 12:18:46 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 23:35:39 GMT+0800 (Singapore Standard Time)

<p>
Record the number of moves for each rows, columns, and two diagonals.
For each move, we -1 for each player 1's move and +1 for player 2's move.
Then we just need to check whether any of the recored numbers equal to n or -n.

```
class TicTacToe(object):

    def __init__(self, n):
        self.row, self.col, self.diag, self.anti_diag, self.n = [0] * n, [0] * n, 0, 0, n
        
    def move(self, row, col, player):
        offset = player * 2 - 3
        self.row[row] += offset
        self.col[col] += offset
        if row == col:
            self.diag += offset
        if row + col == self.n - 1:
            self.anti_diag += offset
        if self.n in [self.row[row], self.col[col], self.diag, self.anti_diag]:
            return 2
        if -self.n in [self.row[row], self.col[col], self.diag, self.anti_diag]:
            return 1
        return 0
```
</p>


### Simple Java Solution (ideas from n^2 to n to constant time)
- Author: charlie11
- Creation Date: Sun Aug 05 2018 01:12:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 05 2018 01:12:59 GMT+0800 (Singapore Standard Time)

<p>
It is only the last step that triggers the win so we just need to check the row/col/diagonal where the last step/"cell" belongs to, which means we don\'t really need to check all the rows/cols/diagonals for each move. This will help us to get O(n) for each move instead of O(n^2)
```
// O(n) solution
class TicTacToe {
    
    int[][] grid;
    
    /** Initialize your data structure here. */
    public TicTacToe(int n) {
        grid = new int[n][n];
    }
    
    /** Player {player} makes a move at ({row}, {col}).
        @param row The row of the board.
        @param col The column of the board.
        @param player The player, can be either 1 or 2.
        @return The current winning condition, can be either:
                0: No one wins.
                1: Player 1 wins.
                2: Player 2 wins. */
    public int move(int row, int col, int player) {
        // validation TODO
        if (row >= grid.length || col >= grid.length) return 0; // out of the grid
        if (grid[row][col] != 0) return 0; // cell is used
        grid[row][col] = player == 1 ? 1 : 2;
        if (checkVerticallyWin(col, player)) return player;
        if (checkHorizontallyWin(row, player)) return player;
        if (checkDiagonallyWin(row, col, player)) return player;
        return 0;
    }
    
    private boolean checkVerticallyWin(int col, int player) {
        for (int i=0; i<grid.length; i++) {
            if (grid[i][col] != player) return false;
        }
        return true;
    }
    
    private boolean checkHorizontallyWin(int row, int player) {
        for (int j=0; j<grid[0].length; j++) {
            if (grid[row][j] != player) return false;
        }
        return true;
    }
    
    private boolean checkDiagonallyWin(int row, int col, int player) {
        if (row != col && row+col != grid.length-1) return false;
        boolean topLeftToBottomRight = true;
        boolean topRightToBottomLeft = true;
        for (int i=0; i<grid.length; i++) {
            if (grid[i][i] != player) topLeftToBottomRight = false;
        }
        for (int i=0; i<grid.length; i++) {
            if (grid[i][grid.length-1-i] != player) topRightToBottomLeft = false;
        }
        return topRightToBottomLeft || topLeftToBottomRight;
    }
}
// TC: O(n)
// AS: O(n^2)
```

Can we do better? The anwser is yes! We can also keep track of the sum of each row/col/diagonals which makes us to achieve O(1):
```
// O(1) solution
class TicTacToe {
    
    int[] rows;
    int[] cols;
    int topLeftToBottomRight;
    int topRightToBottomLeft;
    
    /** Initialize your data structure here. */
    public TicTacToe(int n) {
        rows = new int[n];
        cols = new int[n];
        topLeftToBottomRight = 0;
        topRightToBottomLeft = 0;
    }
    
    /** Player {player} makes a move at ({row}, {col}).
        @param row The row of the board.
        @param col The column of the board.
        @param player The player, can be either 1 or 2.
        @return The current winning condition, can be either:
                0: No one wins.
                1: Player 1 wins.
                2: Player 2 wins. */
    public int move(int row, int col, int player) {
        rows[row] += player == 1 ? 1 : -1; // player 1 --> +1 / player 2 --> -1 
        cols[col] += player == 1 ? 1 : -1;
        if (row == col) topLeftToBottomRight += player == 1 ? 1 : -1;
        if (row+col == rows.length-1) topRightToBottomLeft += player == 1 ? 1 : -1;
        
        if (rows[row] == rows.length || cols[col] == rows.length 
            || topLeftToBottomRight == rows.length || topRightToBottomLeft == rows.length) return 1;
        if (rows[row] == -rows.length || cols[col] == -rows.length 
            || topLeftToBottomRight == -rows.length || topRightToBottomLeft == -rows.length) return 2;
        
        return 0;    
    }
}
// TC: O(1)
// AS: O(n)
```


</p>


