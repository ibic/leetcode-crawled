---
title: "Maximum Number of Coins You Can Get"
weight: 1426
#id: "maximum-number-of-coins-you-can-get"
---
## Description
<div class="description">
<p>There are 3n&nbsp;piles of coins of&nbsp;varying size, you and your friends will take piles of coins as follows:</p>

<ul>
	<li>In each step, you will choose <strong>any&nbsp;</strong>3 piles of coins (not necessarily consecutive).</li>
	<li>Of your choice,&nbsp;Alice&nbsp;will pick&nbsp;the pile with the maximum number of coins.</li>
	<li>You will pick the next pile with maximum number of coins.</li>
	<li>Your friend Bob will pick the last pile.</li>
	<li>Repeat&nbsp;until&nbsp;there are no more piles of coins.</li>
</ul>

<p>Given an array of integers <code>piles</code>&nbsp;where <code>piles[i]</code> is the number of coins in the <code>i<sup>th</sup></code> pile.</p>

<p>Return the maximum number of coins which you can have.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> piles = [2,4,1,2,7,8]
<strong>Output:</strong> 9
<strong>Explanation: </strong>Choose the triplet (2, 7, 8), Alice Pick the pile with 8 coins, you the pile with <strong>7</strong> coins and Bob the last one.
Choose the triplet (1, 2, 4), Alice Pick the pile with 4 coins, you the pile with <strong>2</strong> coins and Bob the last one.
The maximum number of coins which you can have are: 7 + 2 = 9.
On the other hand if we choose this arrangement (1, <strong>2</strong>, 8), (2, <strong>4</strong>, 7) you only get 2 + 4 = 6 coins which is not optimal.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> piles = [2,4,5]
<strong>Output:</strong> 4
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> piles = [9,8,7,6,5,1,2,3,4]
<strong>Output:</strong> 18
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>3 &lt;= piles.length &lt;= 10^5</code></li>
	<li><code>piles.length % 3 == 0</code></li>
	<li><code>1 &lt;= piles[i] &lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Sort (sort)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Sort Piles | Detailed Steps | Example Dry Run
- Author: karprabin1
- Creation Date: Sun Aug 23 2020 12:10:05 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 25 2020 00:01:48 GMT+0800 (Singapore Standard Time)

<p>
**Idea:**

As, we can choose any 3 piles of coins (not necessarily consecutive) so why  can\'t sort and then  select according to point 2.

1. Sort the piles.
2. Pick two right most pile and 1 left most so that you can score max coins. Among 2 rightmost pile Alice will take maximum one and you will get second max.
3. Keep repeat step 2 for n / 3  times.

**Example Dry Run**
```
Example: [9,8,7,6,5,1,2,3,4]
After sorting [1,2,3,4,5,6,7,8,9]

in first step you will pick [1,8,9] -> 8
in second step you will pick [2,6,7] -> 6
in third step you will pick [3,4,5] -> 4

your coins => 18
```

**Detailed Video Explanation** https://www.youtube.com/watch?v=qnpr3aDM9HE


********
**With For Loop**
```
class Solution {
    public int maxCoins(int[] piles) {
        Arrays.sort(piles);
        int coins = 0;
        for(int j= 0, i = piles.length - 2; j < piles.length / 3; j++, i-=2)
            coins += piles[i];
        return coins;
    }
}
```


********
**With While Loop**
```
class Solution {
    public int maxCoins(int[] piles) {
        Arrays.sort(piles);
        int coins = 0;
        int i = piles.length - 2;
        int j = 0;
        while (j++ < piles.length / 3) {
            coins += piles[i];
            i -= 2;
        }
        return coins;
    }
}
```

`TC - O(nlogn)`
`SC - O(1)`

If any doubt ask in comments. If you like solution **upvote**.
</p>


### [Java/C++/Python] Stright Forward
- Author: lee215
- Creation Date: Sun Aug 23 2020 12:01:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 23 2020 12:01:21 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
We want to compare the numbers, we need to sort.
We give the smallest values Bob,
give the biggest to Alice,
and leave the second biggest to us.
Then we repeat this process.
<br>

## **Explanation**
We give small value to Bob,
give the large value to Alice,
and we pick the medium values.

The final assignment will be like:
S S S S S S M L M L M L M L M L

The first third part is given to Bob,
start from `A[n/3]`, we pick one element from every two.
<br>

## **Complexity**
Time `O(sort)`
Space `O(sort)`
<br>

**Java:**
```java
    public int maxCoins(int[] A) {
        Arrays.sort(A);
        int res = 0, n = A.length;
        for (int i = n / 3; i < n; i += 2)
            res += A[i];
        return res;
    }
```

**C++:**
```cpp
    int maxCoins(vector<int>& A) {
        sort(A.begin(), A.end());
        int res = 0, n = A.size();
        for (int i = n / 3; i < n; i += 2)
            res += A[i];
        return res;
    }
```

**Python:**
```py
    def maxCoins(self, A):
        return sum(sorted(A)[len(A) / 3::2])
```

</p>


### [Java/Python 3] 5/1 liners w/ comment and.analysis: Sort and pick the 2nd largest of each triple.
- Author: rock
- Creation Date: Sun Aug 23 2020 12:02:00 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 24 2020 02:54:33 GMT+0800 (Singapore Standard Time)

<p>
Each time pick the remaining largest `2` and the smallest one to form a triple, choose the `2nd` largest out of the triple, repeats till the end; Therefore, we can simply sort the array and choose the `2nd, 4th, 6th`, ..., from the largest, till we have `n / 3` piles.
We can equally start from the `(piles.length / 3 + 1)th` pile, followed by `(piles.length / 3 + 3)th`, `(piles.length / 3 + 5)th`, ..., till the end of the array `piles`.

```java
    public int maxCoins(int[] piles) {
        Arrays.sort(piles);
        int ans = 0;
        for (int i = piles.length / 3; i < piles.length; i += 2)
            ans += piles[i];
        return ans;
    }
```
**Analysis:**
Time: O(nlogn), space: O(1) - excluding space used during sort.

----
```python
    def maxCoins(self, piles: List[int]) -> int:
        return sum(sorted(piles)[len(piles)// 3 :: 2])
```

**Analysis:**
Time: O(nlogn), space: O(n).
</p>


