---
title: "Rotated Digits"
weight: 725
#id: "rotated-digits"
---
## Description
<div class="description">
<p>X is a good number if after rotating each digit individually by 180 degrees, we get a valid number that is different from X.&nbsp; Each digit must be rotated - we cannot choose to leave it alone.</p>

<p>A number is valid if each digit remains a digit after rotation. 0, 1, and 8 rotate to themselves; 2 and 5 rotate to each other (on this case they are rotated in a different direction, in other words 2 or 5 gets mirrored); 6 and 9 rotate to each other, and the rest of the numbers do not rotate to any other number and become invalid.</p>

<p>Now&nbsp;given a positive number <code>N</code>, how many numbers X from <code>1</code> to <code>N</code> are good?</p>

<pre>
<strong>Example:</strong>
<strong>Input:</strong> 10
<strong>Output:</strong> 4
<strong>Explanation:</strong> 
There are four good numbers in the range [1, 10] : 2, 5, 6, 9.
Note that 1 and 10 are not good numbers, since they remain unchanged after rotating.
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li>N&nbsp; will be in range <code>[1, 10000]</code>.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Facebook - 4 (taggedByAdmin: false)
- Google - 10 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Brute Force [Accepted]

**Intuition**

For each `X` from `1` to `N`, let's analyze whether `X` is good.

* If `X` has a digit that does not have a valid rotation (`3`, `4`, or `7`), then it can't be good.

* If `X` doesn't have a digit that rotates to a different digit (`2`, `5`, `6`, or `9`), it can't be good because `X` will be the same after rotation.

* Otherwise, `X` will successfully rotate to a valid different number.

**Algorithm**

To handle checking the digits of `X`, we showcase two implementations.  The most obvious way is to parse the string; another way is to recursively check the last digit of `X`.

See the comments in each implementation for more details.

<iframe src="https://leetcode.com/playground/Dz8HSECt/shared" frameBorder="0" width="100%" height="395" name="Dz8HSECt"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$.  For each `X`, we check each digit.

* Space Complexity:  $$O(\log N)$$, the space stored either by the string, or the recursive call stack of the function `good`.


---
#### Approach #2: Dynamic Programming On Digits [Accepted]

**Intuition**

Say we are writing a good number digit by digit.  The necessary and sufficient conditions are: we need to write using only digits from `0125689`, write a number less than or equal to `N`, and write at least one digit from `2569`.

We can use dynamic programming to solve this efficiently.  Our state will be how many digits `i` we have written, whether we have previously written a `j`th digit lower than the `j`th digit of `N`, and whether we have previously written a digit from `2569`.  We will represent this state by three variables: `i, equality_flag, involution_flag`.

`dp(i, equality_flag, involution_flag)` will represent the number of ways to write the suffix of `N` corresponding to these above conditions.  The answer we want is `dp(0, True, False)`.

**Algorithm**

If `equality_flag` is true, the `i`th digit (0 indexed) will be at most the `i`th digit of `N`.  For each digit `d`, we determine if we can write `d` based on the flags that are currently set.

In the below implementations, we showcase both top-down and bottom-up approaches.  The four lines in the top-down approach (Python) from `for d in xrange(...` to before `memo[...] = ans` clearly illustrates the recursive relationship between states in our dynamic programming.

<iframe src="https://leetcode.com/playground/U7sRpfEw/shared" frameBorder="0" width="100%" height="500" name="U7sRpfEw"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\log N)$$.  We do constant-time work for each digit of `N`.

* Space Complexity:  $$O(\log N)$$, the space stored by `memo`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java dp solution 9ms
- Author: rome753
- Creation Date: Tue Mar 06 2018 16:03:19 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 09:19:00 GMT+0800 (Singapore Standard Time)

<p>
Using a int[] for dp.
dp[i] = 0, invalid number
dp[i] = 1, valid and same number
dp[i] = 2, valid and different number
```
    public int rotatedDigits(int N) {
        int[] dp = new int[N + 1];
        int count = 0;
        for(int i = 0; i <= N; i++){
            if(i < 10){
                if(i == 0 || i == 1 || i == 8) dp[i] = 1;
                else if(i == 2 || i == 5 || i == 6 || i == 9){
                    dp[i] = 2;
                    count++;
                }
            } else {
                int a = dp[i / 10], b = dp[i % 10];
                if(a == 1 && b == 1) dp[i] = 1;
                else if(a >= 1 && b >= 1){
                    dp[i] = 2;
                    count++;
                }
            }
        }
        return count;
    }
```
</p>


### Easily Understood Java Solution
- Author: JLepere2
- Creation Date: Sun Feb 25 2018 12:24:09 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 02:01:53 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int rotatedDigits(int N) {
        int count = 0;
        for (int i = 1; i <= N; i ++) {
            if (isValid(i)) count ++;
        }
        return count;
    }
    
    public boolean isValid(int N) {
        /*
         Valid if N contains ATLEAST ONE 2, 5, 6, 9
         AND NO 3, 4 or 7s
         */
        boolean validFound = false;
        while (N > 0) {
            if (N % 10 == 2) validFound = true;
            if (N % 10 == 5) validFound = true;
            if (N % 10 == 6) validFound = true;
            if (N % 10 == 9) validFound = true;
            if (N % 10 == 3) return false;
            if (N % 10 == 4) return false;
            if (N % 10 == 7) return false;
            N = N / 10;
        }
        return validFound;
    }
}
```
</p>


### [Java/Python] O(logN) Time, O(1) Space
- Author: lee215
- Creation Date: Sun Feb 25 2018 12:08:48 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 05 2020 11:00:35 GMT+0800 (Singapore Standard Time)

<p>
# Soluton 1
5 lines, self-explaining:
```py
def rotatedDigits(self, N):
        s1 = set([1, 8, 0])
        s2 = set([1, 8, 0, 6, 9, 2, 5])
        def isGood(x):
            s = set([int(i) for i in str(x)])
            return s.issubset(s2) and not s.issubset(s1)
        return sum(isGood(i) for i in range(N + 1))
```
<br>

# Solution 2
`O(NlogN)` solution is simple in all languages.
Here is solution in `O(logN)` complexity.
```py
def rotatedDigits(self, N):
        s1 = set([0, 1, 8])
        s2 = set([0, 1, 8, 2, 5, 6, 9])
        s = set()
        res = 0
        N = map(int, str(N))
        for i, v in enumerate(N):
            for j in range(v):
                if s.issubset(s2) and j in s2:
                    res += 7**(len(N) - i - 1)
                if s.issubset(s1) and j in s1:
                    res -= 3**(len(N) - i - 1)
            if v not in s2:
                return res
            s.add(v)
        return res + (s.issubset(s2) and not s.issubset(s1))
```
**Java**
```java
public int rotatedDigits(int N) {
    char[] chars = Integer.toString(N).toCharArray();
    int res = 0;
    HashSet<Integer> digits = new HashSet<>();
    for (int i = 0; i < chars.length; i++) {
        int digit = chars[i] - \'0\';
        for (int j = 0; j < digit; j++) {
            if (set2.contains(j)) {
                res += (int)Math.pow(7, chars.length - i - 1);
            }
            if (set1.containsAll(digits) && set1.contains(j)) {
                res -= (int)Math.pow(3, chars.length - i - 1);
            }
        }
        digits.add(digit);
        if (!set2.contains(digit)) {
            return res;
        }

    }

    return res + (!set1.containsAll(digits) ? 1 : 0);
}
```
</p>


