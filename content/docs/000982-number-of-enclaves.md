---
title: "Number of Enclaves"
weight: 982
#id: "number-of-enclaves"
---
## Description
<div class="description">
<p>Given a 2D array <code>A</code>, each cell is 0 (representing sea) or 1 (representing land)</p>

<p>A move consists of walking from one land square 4-directionally to another land square, or off the boundary of the grid.</p>

<p>Return the number of land squares in the grid for which we <strong>cannot</strong> walk off the boundary of the grid in any number of moves.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[0,0,0,0],[1,0,1,0],[0,1,1,0],[0,0,0,0]]</span>
<strong>Output: </strong><span id="example-output-1">3</span>
<strong>Explanation: </strong>
There are three 1s that are enclosed by 0s, and one 1 that isn&#39;t enclosed because its on the boundary.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[[0,1,1,0],[0,0,1,0],[0,0,1,0],[0,0,0,0]]</span>
<strong>Output: </strong><span id="example-output-2">0</span>
<strong>Explanation: </strong>
All 1s are either on the boundary or can reach the boundary.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 500</code></li>
	<li><code>1 &lt;= A[i].length &lt;= 500</code></li>
	<li><code>0 &lt;= A[i][j] &lt;= 1</code></li>
	<li>All rows have the same size.</li>
</ol>
</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ with picture, DFS and BFS
- Author: votrubac
- Creation Date: Sun Mar 31 2019 12:30:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 31 2019 12:30:41 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
We flood-fill the land (change 1 to 0) from the boundary of the grid. Then, we count the remaining land.
![image](https://assets.leetcode.com/users/votrubac/image_1554006685.png)
# DFS Solution
The first cycle does DFS for the boundary cells. The second cycle counts the remaining land.
```
void dfs(vector<vector<int>>& A, int i, int j) {
  if (i < 0 || j < 0 || i == A.size() || j == A[i].size() || A[i][j] != 1) return;
  A[i][j] = 0;
  dfs(A, i + 1, j), dfs(A, i - 1, j), dfs(A, i, j + 1), dfs(A, i, j - 1);
}
int numEnclaves(vector<vector<int>>& A) {
  for (auto i = 0; i < A.size(); ++i)
    for (auto j = 0; j < A[0].size(); ++j) 
      if (i * j == 0 || i == A.size() - 1 || j == A[i].size() - 1) dfs(A, i, j);

  return accumulate(begin(A), end(A), 0, [](int s, vector<int> &r)
    { return s + accumulate(begin(r), end(r), 0); });
}
```
# BFS Solution
Alternativelly, we can use a queue to implement BFS solution. For this problem, I don\'t think it\'s matter whether we do DFS or BFS. The runtime of the both solution is almost the same as well.
```
int numEnclaves(vector<vector<int>>& A, int res = 0) {
  queue<pair<int, int>> q;
  for (auto i = 0; i < A.size(); ++i)
    for (auto j = 0; j < A[0].size(); ++j) {
      res += A[i][j];
      if (i * j == 0 || i == A.size() - 1 || j == A[i].size() - 1) q.push({ i, j });
    }
  while (!q.empty()) {
    auto x = q.front().first, y = q.front().second; q.pop();
    if (x < 0 || y < 0 || x == A.size() || y == A[x].size() || A[x][y] != 1) continue;
    A[x][y] = 0;
    --res;
    q.push({ x + 1, y }), q.push({ x - 1, y }), q.push({ x, y + 1 }), q.push({ x, y - 1 });
  }
  return res;
}
```
## Complexity Analysis
Runtime: *O(n * m)*, where *n* and *m* are the dimensions of the grid.
Memory: *O(n * m)*. DFS can enumerate all elements in the worst case, and we need to store each element on the stack for the recursion.
</p>


### Easy Java DFS 6ms solution
- Author: arjun_sharma
- Creation Date: Mon Apr 01 2019 06:44:34 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 01 2019 06:44:34 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int numEnclaves(int[][] A) {
        int result = 0;
        for(int i = 0; i < A.length; i++) {
            for(int j = 0; j < A[i].length; j++) {
                if(i == 0 || j == 0 || i == A.length - 1 || j == A[i].length - 1)
                    dfs(A, i, j);
            }
        }
        
        for(int i = 0; i < A.length; i++) {
            for(int j = 0; j < A[i].length; j++) {
                if(A[i][j] == 1)
                    result++;
            }
        }
        
        return result;
    }
    
    public void dfs(int a[][], int i, int j) {
        if(i >= 0 && i <= a.length - 1 && j >= 0 && j <= a[i].length - 1 && a[i][j] == 1) {
            a[i][j] = 0;
            dfs(a, i + 1, j);
            dfs(a, i - 1, j);
            dfs(a, i, j + 1);
            dfs(a, i, j - 1);
        }
    }
}
```
</p>


### Python clean DFS solution
- Author: cenkay
- Creation Date: Sun Mar 31 2019 12:26:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 31 2019 12:26:21 GMT+0800 (Singapore Standard Time)

<p>
* We check edges of A matrix.
* If A[i][j] is 1 on the edge, do DFS and clean all connected 1\'s
* Return sum of left 1\'s
```
class Solution:
    def numEnclaves(self, A: List[List[int]]) -> int:
        def dfs(i, j):
            A[i][j] = 0
            for x, y in (i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1):
                if 0 <= x < m and 0 <= y < n and A[x][y]:
                    dfs(x, y)
        m, n = len(A), len(A[0])
        for i in range(m):
            for j in range(n):
                if A[i][j] == 1 and (i == 0 or j == 0 or i == m - 1 or j == n - 1):
                    dfs(i, j)
        return sum(sum(row) for row in A)
```
Time complexity: O(m * n)
Space complexity: O(m * n) due to recursion
</p>


