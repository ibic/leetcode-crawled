---
title: "Parse Lisp Expression"
weight: 657
#id: "parse-lisp-expression"
---
## Description
<div class="description">
<p>
You are given a string <code>expression</code> representing a Lisp-like expression to return the integer value of.
</p><p>
The syntax for these expressions is given as follows.
</p><p>
<li>An expression is either an integer, a let-expression, an add-expression, a mult-expression, or an assigned variable.  Expressions always evaluate to a single integer.</li>
</p><p>
<li>(An integer could be positive or negative.)</li>
</p><p>
<li>A let-expression takes the form <code>(let v1 e1 v2 e2 ... vn en expr)</code>, where <code>let</code> is always the string <code>"let"</code>, then there are 1 or more pairs of alternating variables and expressions, meaning that the first variable <code>v1</code> is assigned the value of the expression <code>e1</code>, the second variable <code>v2</code> is assigned the value of the expression <code>e2</code>, and so on <b>sequentially</b>; and then the value of this let-expression is the value of the expression <code>expr</code>.</li>
</p><p>
<li>An add-expression takes the form <code>(add e1 e2)</code> where <code>add</code> is always the string <code>"add"</code>, there are always two expressions <code>e1, e2</code>, and this expression evaluates to the addition of the evaluation of <code>e1</code> and the evaluation of <code>e2</code>.</li>
</p><p>
<li>A mult-expression takes the form <code>(mult e1 e2)</code> where <code>mult</code> is always the string <code>"mult"</code>, there are always two expressions <code>e1, e2</code>, and this expression evaluates to the multiplication of the evaluation of <code>e1</code> and the evaluation of <code>e2</code>.</li>
</p><p>
<li>For the purposes of this question, we will use a smaller subset of variable names.  A variable starts with a lowercase letter, then zero or more lowercase letters or digits.  Additionally for your convenience, the names "add", "let", or "mult" are protected and will never be used as variable names.</li>
</p><p>
<li>Finally, there is the concept of scope.  When an expression of a variable name is evaluated, <b>within the context of that evaluation</b>, the innermost scope (in terms of parentheses) is checked first for the value of that variable, and then outer scopes are checked sequentially.  It is guaranteed that every expression is legal.  Please see the examples for more details on scope.</li>
</p>

<p><b>Evaluation Examples:</b><br />
<pre>
<b>Input:</b> (add 1 2)
<b>Output:</b> 3

<b>Input:</b> (mult 3 (add 2 3))
<b>Output:</b> 15

<b>Input:</b> (let x 2 (mult x 5))
<b>Output:</b> 10

<b>Input:</b> (let x 2 (mult x (let x 3 y 4 (add x y))))
<b>Output:</b> 14
<b>Explanation:</b> In the expression (add x y), when checking for the value of the variable x,
we check from the innermost scope to the outermost in the context of the variable we are trying to evaluate.
Since x = 3 is found first, the value of x is 3.

<b>Input:</b> (let x 3 x 2 x)
<b>Output:</b> 2
<b>Explanation:</b> Assignment in let statements is processed sequentially.

<b>Input:</b> (let x 1 y 2 x (add x y) (add x y))
<b>Output:</b> 5
<b>Explanation:</b> The first (add x y) evaluates as 3, and is assigned to x.
The second (add x y) evaluates as 3+2 = 5.

<b>Input:</b> (let x 2 (add (let x 3 (let x 4 x)) x))
<b>Output:</b> 6
<b>Explanation:</b> Even though (let x 4 x) has a deeper scope, it is outside the context
of the final x in the add-expression.  That final x will equal 2.

<b>Input:</b> (let a1 3 b2 (add a1 1) b2) 
<b>Output</b> 4
<b>Explanation:</b> Variable names can contain digits after the first character.

</pre>

<p><b>Note:</b>
<li>The given string <code>expression</code> is well formatted: There are no leading or trailing spaces, there is only a single space separating different components of the string, and no space between adjacent parentheses.  The expression is guaranteed to be legal and evaluate to an integer.</li>
<li>The length of <code>expression</code> is at most 2000.  (It is also non-empty, as that would not be a legal expression.)</li>
<li>The answer and all intermediate calculations of that answer are guaranteed to fit in a 32-bit integer.</li>
</p>
</div>

## Tags
- String (string)

## Companies
- Google - 4 (taggedByAdmin: false)
- Affirm - 5 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Recursive Parsing [Accepted]

**Intuition and Algorithm**

This question is relatively straightforward in terms of the idea of the solution, but presents substantial difficulties in the implementation.

Expressions may involve the evaluation of other expressions, which motivates a recursive approach.

One difficulty is managing the correct scope of the variables.  We can use a stack of hashmaps.  As we enter an inner scope defined by parentheses, we need to add that scope to our stack, and when we exit, we need to pop that scope off.

Our main `evaluate` function will go through each case of what form the `expression` could take.

* If the expression starts with a digit or '-', it's an integer: return it.

* If the expression starts with a letter, it's a variable.  Recall it by checking the current scope in reverse order.

* Otherwise, group the tokens (variables or expressions) within this expression by counting the "balance" `bal` of the occurrences of `'('` minus the number of occurrences of `')'`.  When the balance is zero, we have ended a token.  For example, `(add 1 (add 2 3))` should have tokens `'1'` and `'(add 2 3)'`.

* For add and mult expressions, evaluate each token and return the addition or multiplication of them.

* For let expressions, evaluate each expression sequentially and assign it to the variable in the current scope, then return the evaluation of the final expression.

<iframe src="https://leetcode.com/playground/Ypg4TKUJ/shared" frameBorder="0" width="100%" height="500" name="Ypg4TKUJ"></iframe>


**Complexity Analysis**

* Time Complexity: $$O(N^2)$$, where $$N$$ is the length of `expression`.  Each expression is evaluated once, but within that evaluation we may search the entire scope.

* Space Complexity: $$O(N^2)$$.  We may pass $$O(N)$$ new strings to our `evaluate` function when making intermediate evaluations, each of length $$O(N)$$.  With effort, we could reduce the total space complexity to $$O(N)$$ with interning or passing pointers.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A Clean Java Solution
- Author: zc94zc
- Creation Date: Thu Feb 08 2018 12:28:08 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 11:05:58 GMT+0800 (Singapore Standard Time)

<p>
```
public int evaluate(String expression) {
    return eval(expression, new HashMap<>());
}
private int eval(String exp, Map<String, Integer> parent) {
    if (exp.charAt(0) != '(') {
        // just a number or a symbol
        if (Character.isDigit(exp.charAt(0)) || exp.charAt(0) == '-')
            return Integer.parseInt(exp);
        return parent.get(exp);
    }
    // create a new scope, add add all the previous values to it
    Map<String, Integer> map = new HashMap<>();
    map.putAll(parent);
    List<String> tokens = parse(exp.substring(exp.charAt(1) == 'm' ? 6 : 5, exp.length() - 1));
    if (exp.startsWith("(a")) { // add
        return eval(tokens.get(0), map) + eval(tokens.get(1), map);
    } else if (exp.startsWith("(m")) { // mult
        return eval(tokens.get(0), map) * eval(tokens.get(1), map);
    } else { // let
        for (int i = 0; i < tokens.size() - 2; i += 2)
            map.put(tokens.get(i), eval(tokens.get(i + 1), map));
        return eval(tokens.get(tokens.size() - 1), map);
    }
}
private List<String> parse(String str) {
    // seperate the values between two parentheses
    List<String> res = new ArrayList<>();
    int par = 0;
    StringBuilder sb = new StringBuilder();
    for (char c: str.toCharArray()) {
        if (c == '(') par++;
        if (c == ')') par--;
        if (par == 0 && c == ' ') {
            res.add(new String(sb));
            sb = new StringBuilder();
        } else {
            sb.append(c);
        }
    }
    if (sb.length() > 0) res.add(new String(sb));
    return res;
}
```
</p>


### python solution using stacks.
- Author: sherlock321
- Creation Date: Sun Nov 26 2017 12:47:03 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 09:07:11 GMT+0800 (Singapore Standard Time)

<p>
Using a stack, when encountering '(', save the current tokens and variable states in the stack.
```
def evaluate(self, expression):
    st, d, tokens = [], {}, ['']

    def getval(x):
        return d.get(x, x)

    def evaluate(tokens):
        if tokens[0] in ('add', 'mult'):
            tmp = map(int, map(getval, tokens[1:]))
            return str(tmp[0] + tmp[1] if tokens[0] == 'add' else tmp[0] * tmp[1])
        else: #let
            for i in xrange(1, len(tokens)-1, 2):
                if tokens[i+1]:
                    d[tokens[i]] = getval(tokens[i+1])
            return getval(tokens[-1])

    for c in expression:
        if c == '(':
            if tokens[0] == 'let':
                evaluate(tokens)
            st.append((tokens, dict(d)))
            tokens =  ['']
        elif c == ' ':
            tokens.append('')
        elif c == ')':
            val = evaluate(tokens)
            tokens, d = st.pop()
            tokens[-1] += val
        else:
            tokens[-1] += c
    return int(tokens[0])
```
</p>


### I don't think this problem is good one for interview to be honest
- Author: evanl1208
- Creation Date: Sun Nov 26 2017 17:15:48 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 04:22:27 GMT+0800 (Singapore Standard Time)

<p>
Such problem might be good for ACM contest, but definitely not good for interview. With such a long explanation of the problem itself, I don't think it is a good one for interview. Better to select some practical interview problem as contest problem. Just my two cents. Don't want Leetcode to be congested with lot of non-interview coding problem.
</p>


