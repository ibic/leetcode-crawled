---
title: "The Earliest Moment When Everyone Become Friends"
weight: 994
#id: "the-earliest-moment-when-everyone-become-friends"
---
## Description
<div class="description">
<p>In a social group, there are <code>N</code> people, with unique integer ids from <code>0</code> to <code>N-1</code>.</p>

<p>We have a list of <code>logs</code>, where each <code>logs[i] = [timestamp, id_A, id_B]</code> contains a non-negative&nbsp;integer timestamp, and the ids of two different people.</p>

<p>Each log represents the time in which two different people became friends. &nbsp;Friendship is symmetric: if A is friends with B, then B is friends with A.</p>

<p>Let&#39;s say that person A is acquainted with person B if A is friends with B, or A is a friend of someone acquainted with B.</p>

<p>Return the earliest time for which every person became acquainted with every other person. Return -1 if there is no&nbsp;such&nbsp;earliest time.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>logs = <span id="example-input-1-1">[[20190101,0,1],[20190104,3,4],[20190107,2,3],[20190211,1,5],[20190224,2,4],[20190301,0,3],[20190312,1,2],[20190322,4,5]]</span>, N = <span id="example-input-1-2">6</span>
<strong>Output: </strong><span id="example-output-1">20190301</span>
<strong>Explanation: </strong>
The first event occurs at timestamp = 20190101 and after 0 and 1 become friends we have the following friendship groups [0,1], [2], [3], [4], [5].
The second event occurs at timestamp = 20190104 and after 3 and 4 become friends we have the following friendship groups [0,1], [2], [3,4], [5].
The third event occurs at timestamp = 20190107 and after 2 and 3 become friends we have the following friendship groups [0,1], [2,3,4], [5].
The fourth event occurs at timestamp = 20190211 and after 1 and 5 become friends we have the following friendship groups [0,1,5], [2,3,4].
The fifth event occurs at timestamp = 20190224 and as 2 and 4 are already friend anything happens.
The sixth event occurs at timestamp = 20190301 and after 0 and 3 become friends we have that all become friends.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>2 &lt;= N &lt;= 100</code></li>
	<li><code>1 &lt;= logs.length &lt;= 10^4</code></li>
	<li><code>0 &lt;= logs[i][0] &lt;= 10^9</code></li>
	<li><code>0 &lt;= logs[i][1], logs[i][2] &lt;= N - 1</code></li>
	<li>It&#39;s guaranteed that all timestamps in <code>logs[i][0]</code> are different.</li>
	<li><code>logs </code>are not necessarily ordered by some criteria.</li>
	<li><code>logs[i][1] != logs[i][2]</code></li>
</ol>

</div>

## Tags
- Union Find (union-find)

## Companies
- Expedia - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Union-Find
- Author: lee215
- Creation Date: Sun Jun 30 2019 00:17:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 30 2019 00:17:02 GMT+0800 (Singapore Standard Time)

<p>
**Python:**
```python
    def earliestAcq(self, logs, N):
        uf = {x: x for x in xrange(N)}
        self.groups = N

        def merge(x, y):
            x, y = find(x), find(y)
            if x != y:
                self.groups -= 1
                uf[x] = y

        def find(x):
            if uf[x] != x:
                uf[x] = find(uf[x])
            return uf[x]

        for t, x, y in sorted(logs):
            merge(x, y)
            if self.groups == 1:
                return t
        return -1
```

</p>


### C++ Sort & Union-Find
- Author: votrubac
- Creation Date: Sat Jul 06 2019 06:42:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 06 2019 07:03:37 GMT+0800 (Singapore Standard Time)

<p>
Obviously, we need to process logs in the timestamp order. We can sort the input, or use a min-heap. Min-heap is better as we only need to get the next timestapm, one-by-one. However, I did not see a difference in performance in OJ, so I used sort below as it\'s shorter. 

Then, unite friends, tracking how many friends are there. When that number is ```N```, everyone is a friend.

Note that in my disjoint set structure, I am using a negative value to indicate the root and the number of joint elements.
```
int ds_find(vector<int>& ds, int i) {
  return ds[i] < 0 ? i : ds[i] = ds_find(ds, ds[i]);
}
int ds_union(vector<int>& ds, int i, int j) {
  i = ds_find(ds, i), j = ds_find(ds, j);
  if (i != j) {
    ds[i] += ds[j];
    ds[j] = i;
  }
  return -ds[i];
}
int earliestAcq(vector<vector<int>>& logs, int N) {
  vector<int> ds(N, -1);
  sort(begin(logs), end(logs));
  for (auto &l : logs)
    if (ds_union(ds, l[1], l[2]) == N) return l[0];
  return -1;
}
```
Complexity Analysis
Runtime: *O(n log n)* as we need to sort the input. Union-Find is *O(n)*.
Memory: *O(n)* for the disjoint set.
</p>


### Union-Find(Java)
- Author: Veronica_Sheng
- Creation Date: Thu Jul 04 2019 00:32:35 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 04 2019 00:32:35 GMT+0800 (Singapore Standard Time)

<p>
1. sort  timestamp in increasing order.
2. union element who has same parent.
3. when there is only one parent, done.
```
class Solution {
    public class UF{
        int[] parent;
        int res;
        UF(int size){
            parent = new int[size];
            for(int i = 0; i < size; i++)
                parent[i] = i;
            res = size;
        }
        public int find(int child){
            if(parent[child] != child){
                int p = find(parent[child]); //find parent recursively
                parent[child] = p;//update child\'s parent
            }
                
            return parent[child];
        }
        public void union(int a, int b){
            int pa = find(a);
            int pb = find(b);
            if(pa != pb){
                parent[pb] = pa;
                res--;//when union happens, we decrement one parent
            }
             
        }
    }
    public int earliestAcq(int[][] logs, int N) {
        Arrays.sort(logs, (a, b) -> (a[0] - b[0]));
        UF uf = new UF(N);
        for(int[]log : logs){
            uf.union(log[1], log[2]);
            if(uf.res == 1)
                return log[0];
        }
        return -1;
    } 
}
```
</p>


