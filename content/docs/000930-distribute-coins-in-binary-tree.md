---
title: "Distribute Coins in Binary Tree"
weight: 930
#id: "distribute-coins-in-binary-tree"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a binary tree with <code>N</code> nodes, each <code>node</code>&nbsp;in the tree has <code>node.val</code> coins, and there are <code>N</code> coins total.</p>

<p>In one move, we may choose two adjacent nodes and move one coin from one node to another.&nbsp; (The move may be from parent to child, or from child to parent.)</p>

<p>Return the number of moves required to make every node have exactly one coin.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/01/18/tree1.png" style="width: 150px; height: 142px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[3,0,0]</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>From the root of the tree, we move one coin to its left child, and one coin to its right child.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/01/18/tree2.png" style="width: 150px; height: 142px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[0,3,0]</span>
<strong>Output: </strong><span id="example-output-2">3</span>
<strong>Explanation: </strong>From the left child of the root, we move two coins to the root [taking two moves].  Then, we move one coin from the root of the tree to the right child.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/01/18/tree3.png" style="width: 150px; height: 142px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[1,0,2]</span>
<strong>Output: </strong><span id="example-output-3">2</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/01/18/tree4.png" style="width: 155px; height: 156px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">[1,0,0,null,3]</span>
<strong>Output: </strong><span id="example-output-4">4</span>
</pre>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ol>
	<li><code>1&lt;= N &lt;= 100</code></li>
	<li><code>0 &lt;= node.val &lt;= N</code></li>
</ol>
</div>
</div>
</div>
</div>
</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Google - 2 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Depth First Search

**Intuition**

If the leaf of a tree has 0 coins (an excess of -1 from what it needs), then we should push a coin from its parent onto the leaf.  If it has say, 4 coins (an excess of 3), then we should push 3 coins off the leaf.  In total, the number of moves from that leaf to or from its parent is `excess = Math.abs(num_coins - 1)`.  Afterwards, we never have to consider this leaf again in the rest of our calculation.

**Algorithm**

We can use the above fact to build our answer.  Let `dfs(node)` be the *excess* number of coins in the subtree at or below this `node`: namely, the number of coins in the subtree, minus the number of nodes in the subtree.  Then, the number of moves we make from this node to and from its children is `abs(dfs(node.left)) + abs(dfs(node.right))`.  After, we have an excess of `node.val + dfs(node.left) + dfs(node.right) - 1` coins at this node.

<iframe src="https://leetcode.com/playground/BQYPwrpr/shared" frameBorder="0" width="100%" height="327" name="BQYPwrpr"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the tree.

* Space Complexity:  $$O(H)$$, where $$H$$ is the height of the tree.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ with picture, post-order traversal
- Author: votrubac
- Creation Date: Sun Jan 20 2019 12:03:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 20 2019 12:03:59 GMT+0800 (Singapore Standard Time)

<p>
We traverse childs first (post-order traversal), and return the ballance of coins. For example, if we get \'+3\' from the left child, that means that the left subtree has 3 extra coins to move out. If we get \'-1\' from the right child, we need to move 1 coin in. So, we increase the number of moves by 4 (3 moves out left + 1 moves in right). We then return the final ballance: r->val (coins in the root) + 3 (left) + (-1) (right) - 1 (keep one coin for the root).
![image](https://assets.leetcode.com/users/votrubac/image_1548011422.png)
```
int traverse(TreeNode* r, int &moves) {
  if (r == nullptr) return 0;
  int left = traverse(r->left, moves), right = traverse(r->right, moves);
  moves += abs(left) + abs(right);
  return r->val + left + right - 1;
}
int distributeCoins(TreeNode* r, int moves = 0) {
  traverse(r, moves);
  return moves;
}
```
If we can modify values of tree nodes, we can store the balance in nodes, and use the return value to accumulate the number of moves. This way we can get rid of the helper method. The solution below is courtesy of [Lee](https://leetcode.com/lee215/):
```
int distributeCoins(TreeNode* r, TreeNode* p = nullptr) {
  if (r == nullptr) return 0;
  int res = distributeCoins(r->left, r) + distributeCoins(r->right, r);
  if (p != nullptr) p->val += r->val - 1;
  return res + abs(r->val - 1);
}
```
</p>


### [Java/C++/Python] Recursive Solution
- Author: lee215
- Creation Date: Sun Jan 20 2019 12:02:53 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 13 2020 00:15:09 GMT+0800 (Singapore Standard Time)

<p>
# Solution 1
Quite similar problem as this one 968.Binary Tree Cameras.
So I put this as the first solution.
Write a dfs helper, return the number of coins given to the parent.
<br>

**Java:**
```java
    int res = 0;
    public int distributeCoins(TreeNode root) {
        dfs(root);
        return res;
    }

    public int dfs(TreeNode root) {
        if (root == null) return 0;
        int left = dfs(root.left), right = dfs(root.right);
        res += Math.abs(left) + Math.abs(right);
        return root.val + left + right - 1;
    }
```
**C++:**
```cpp
    int res = 0;
    int distributeCoins(TreeNode* root) {
        dfs(root);
        return res;
    }
    int dfs(TreeNode* root) {
        if (!root) return 0;
        int left = dfs(root->left), right = dfs(root->right);
        res += abs(left) + abs(right);
        return root->val + left + right - 1;
    }
```
**Python:**
```py
    res = 0
    def distributeCoins(self, root):
        def dfs(root):
            if not root: return 0
            left = dfs(root.left)
            right = dfs(root.right)
            self.res += abs(left) + abs(right)
            return root.val + left + right - 1
        dfs(root)
        return self.res
```

# Solution 2

As somebody may not like global variable,
here is the recursive version without helper.
<br>

**Java:**
```java
    public int distributeCoins(TreeNode root) {
        int res = 0;
        if (root.left != null) {
            res += distributeCoins(root.left);
            root.val += root.left.val - 1;
            res += Math.abs(root.left.val - 1);
        }
        if (root.right != null) {
            res += distributeCoins(root.right);
            root.val += root.right.val - 1;
            res += Math.abs(root.right.val - 1);
        }
        return res;
    }
```
**C++:**
```cpp
    int distributeCoins(TreeNode* root) {
        int res = 0;
        if (root->left) {
            res += distributeCoins(root->left);
            root->val += root->left->val - 1;
            res += abs(root->left->val - 1);
        }
        if (root->right) {
            res += distributeCoins(root->right);
            root->val += root->right->val - 1;
            res += abs(root->right->val - 1);
        }
        return res;
    }
```
<br>

# Solution 3
Give the parent node so we can move the coins to the parent directly.

**C++:**
```cpp
    int distributeCoins(TreeNode* root, TreeNode* pre = NULL) {
        if (!root) return 0;
        int res = distributeCoins(root->left, root) + distributeCoins(root->right, root);
        if (pre) pre->val += root->val - 1;
        return res + abs(root->val - 1);
    }
```
**Python:**
```py
    def distributeCoins(self, root, pre=None):
        if not root: return 0
        res = self.distributeCoins(root.left, root) + self.distributeCoins(root.right, root)
        if pre: pre.val += root.val - 1
        return res + abs(root.val - 1)
```
</p>


### Java Recursion - REALLY easy to understand!!
- Author: zhiying_qian
- Creation Date: Sat Mar 16 2019 11:24:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 16 2019 11:24:21 GMT+0800 (Singapore Standard Time)

<p>
I\'d like to share my recursive solution. I know there are better ways, which only record one value instead of two. But my solution is more understandable ----

For each node, we use an int array to record the information **[# of nodes in the subtree (include itself), # of total coins in the subtree (include itself)]**. Then we know that for a certain node, if there are **more coins than nodes** in the subtree, the node must "contribute" the redundant coins to its parent. Else, if there are **more nodes than coins** in the subtree, then the node must take some coins from the parent.

Both of these two operations will create moves in the tree. And we just need to add the **absolute value** of the (# nodes - # coins) to the final result because the move can be "contribute" or "take". The time complexity is O(n) because we are just traversing the tree.

```
class Solution {
    int moves = 0;
    public int distributeCoins(TreeNode root) {
        getNumAndCoins(root);
        return moves;
    }
    
    /*
     * return [number_of_nodes_in_subtree, number_of_total_coins_in_subtree]
     */
    private int[] getNumAndCoins(TreeNode node) {
        if (node == null) return new int[] {0, 0};
        int[] left = getNumAndCoins(node.left);
        int[] right = getNumAndCoins(node.right);
        moves += Math.abs(left[0] - left[1]) + Math.abs(right[0] - right[1]);
        return new int[] {left[0] + right[0] + 1, left[1] + right[1] + node.val};
    }
}
```

After understanding this, it will be trivial to understand others\' posts, which just use the value of **(# of nodes in the subtree - # of total coins in the subtree)** as the return value.
</p>


