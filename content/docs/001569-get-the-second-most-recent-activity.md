---
title: "Get the Second Most Recent Activity"
weight: 1569
#id: "get-the-second-most-recent-activity"
---
## Description
<div class="description">
<p>Table: <code>UserActivity</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| username      | varchar |
| activity      | varchar |
| startDate     | Date    |
| endDate       | Date    |
+---------------+---------+
This table does not contain primary key.
This table contain information about the activity performed of each user in a period of time.
A person with username performed a activity from startDate to endDate.

</pre>

<p>Write an SQL query to&nbsp;show&nbsp;the <strong>second most recent activity </strong>of each user.</p>

<p>If the user only has one activity, return that one.&nbsp;</p>

<p>A&nbsp;user can&#39;t perform more than one activity at the same time. Return the result table in <strong>any</strong> order.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>UserActivity</code> table:
+------------+--------------+-------------+-------------+
| username   | activity     | startDate   | endDate     |
+------------+--------------+-------------+-------------+
| Alice      | Travel       | 2020-02-12  | 2020-02-20  |
| Alice      | Dancing      | 2020-02-21  | 2020-02-23  |
| Alice      | Travel       | 2020-02-24  | 2020-02-28  |
| Bob        | Travel       | 2020-02-11  | 2020-02-18  |
+------------+--------------+-------------+-------------+

Result table:
+------------+--------------+-------------+-------------+
| username   | activity     | startDate   | endDate     |
+------------+--------------+-------------+-------------+
| Alice      | Dancing      | 2020-02-21  | 2020-02-23  |
| Bob        | Travel       | 2020-02-11  | 2020-02-18  |
+------------+--------------+-------------+-------------+

The most recent activity of Alice is Travel from 2020-02-24 to 2020-02-28, before that she was dancing from 2020-02-21 to 2020-02-23.
Bob only has one record, we just take that one.
</pre>
</div>

## Tags


## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL solution, 184ms, no subquery
- Author: Maduce
- Creation Date: Sat Mar 07 2020 06:37:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 07 2020 06:38:12 GMT+0800 (Singapore Standard Time)

<p>

```
SELECT * 
FROM UserActivity 
GROUP BY username 
HAVING COUNT(*) = 1

UNION ALL

SELECT u1.*
FROM UserActivity u1 
LEFT JOIN UserActivity u2 
    ON u1.username = u2.username AND u1.endDate < u2.endDate
GROUP BY u1.username, u1.endDate
HAVING COUNT(u2.endDate) = 1

```
</p>


### MS SQL simple solution
- Author: ruoxinl2
- Creation Date: Mon Mar 16 2020 05:59:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 16 2020 05:59:10 GMT+0800 (Singapore Standard Time)

<p>
```
select username, activity, startDate, endDate
from (
select *, count(activity) over(partition by username)cnt, 
ROW_NUMBER() over(partition by username order by startdate desc) n from UserActivity) tbl
where n=2 or cnt<2
```
</p>


### MYSQL: Just one window function handles it all
- Author: liuhaigang1989
- Creation Date: Fri Jun 26 2020 08:15:42 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 26 2020 08:15:42 GMT+0800 (Singapore Standard Time)

<p>
Use window function to calculate the count and order the starting date. And we select either rank = 2 or count = 1 in statement. Pretty nice. 
```
select username, activity, startDate, endDate from (
select
    username,
    activity,
    startDate,
    endDate,
    row_number() over (partition by username order by startDate desc) as ranks,
    count(username) over (partition by username) as counts
from UserActivity) as t
where counts = 1 or ranks = 2;
```
</p>


