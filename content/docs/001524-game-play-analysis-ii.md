---
title: "Game Play Analysis II"
weight: 1524
#id: "game-play-analysis-ii"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Activity</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| player_id    | int     |
| device_id    | int     |
| event_date   | date    |
| games_played | int     |
+--------------+---------+
(player_id, event_date) is the primary key of this table.
This table shows the activity of players of some game.
Each row is a record of a player who logged in and played a number of games (possibly 0) before logging out on some day using some device.
</pre>

<p>&nbsp;</p>

<p>Write a&nbsp;SQL query that reports&nbsp;the <strong>device</strong>&nbsp;that is first logged in&nbsp;for each player.</p>

<p>The query result format is in the following example:</p>

<pre>
Activity table:
+-----------+-----------+------------+--------------+
| player_id | device_id | event_date | games_played |
+-----------+-----------+------------+--------------+
| 1         | 2         | 2016-03-01 | 5            |
| 1         | 2         | 2016-05-02 | 6            |
| 2         | 3         | 2017-06-25 | 1            |
| 3         | 1         | 2016-03-02 | 0            |
| 3         | 4         | 2018-07-03 | 5            |
+-----------+-----------+------------+--------------+

Result table:
+-----------+-----------+
| player_id | device_id |
+-----------+-----------+
| 1         | 2         |
| 2         | 3         |
| 3         | 1         |
+-----------+-----------+</pre>

</div>

## Tags


## Companies
- GSN Games - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL Solution
- Author: mbodke41
- Creation Date: Sat Jun 15 2019 23:24:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 15 2019 23:24:50 GMT+0800 (Singapore Standard Time)

<p>
```
select player_id, device_id 
from activity 
where (player_id, event_date) in (
                                select player_id, min(event_date)
                                from activity 
                                group by player_id
                                 ) 
```
</p>


### Using Rank()
- Author: rash24
- Creation Date: Fri Aug 02 2019 10:50:05 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 02 2019 10:50:05 GMT+0800 (Singapore Standard Time)

<p>
```
select player_id, device_id
FROM (
    select player_id, device_id, rank() over(partition by player_id order by event_date asc) as rank
    from activity
) tmp
where tmp.rank = 1
```
</p>


### MySQL Solution
- Author: sjin6
- Creation Date: Fri Aug 02 2019 06:31:38 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 02 2019 06:31:38 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT player_id, device_id
FROM Activity
WHERE (player_id, event_date) IN
      (SELECT player_id, MIN(event_date)
       FROM Activity
       GROUP BY 1)
```
</p>


