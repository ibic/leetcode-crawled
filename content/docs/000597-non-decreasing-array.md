---
title: "Non-decreasing Array"
weight: 597
#id: "non-decreasing-array"
---
## Description
<div class="description">
<p>Given an array <code>nums</code> with <code>n</code> integers, your task is to check if it could become non-decreasing by modifying <b>at most</b> <code>1</code> element.</p>

<p>We define an array is non-decreasing if <code>nums[i] &lt;= nums</code><code>[i + 1]</code> holds for every <code>i</code>&nbsp;(0-based) such that <code>(0&nbsp;&lt;= i &lt;= n - 2)</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [4,2,3]
<strong>Output:</strong> true
<strong>Explanation:</strong> You could modify the first <code>4</code> to <code>1</code> to get a non-decreasing array.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [4,2,1]
<strong>Output:</strong> false
<strong>Explanation:</strong> You can&#39;t get a non-decreasing array by modify at most one element.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10 ^ 4</code></li>
	<li><code>- 10 ^ 5&nbsp;&lt;= nums[i] &lt;= 10 ^ 5</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- eBay - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1: Brute Force [Time Limit Exceeded]

**Intuition**

For the given array $$\text{A}$$, if we are changing at most one element $$\text{A[i]}$$, we should change $$\text{A[i]}$$ to $$\text{A[i-1]}$$, as it would be guaranteed that $$\text{A[i-1]} &leq; \text{A[i]}$$, and $$\text{A[i]}$$ would be the smallest possible to try and achieve $$\text{A[i]} &leq; \text{A[i+1]}$$.

**Algorithm**

For each possible change $$\text{A[i]}$$, check if the sequence is monotone increasing.  We'll modify $$\text{new}$$, a copy of the array $$\text{A}$$.

<iframe src="https://leetcode.com/playground/FK7JPfxR/shared" frameBorder="0" name="FK7JPfxR" width="100%" height="343"></iframe>

**Complexity Analysis**

* Time Complexity: Let $$N$$ be the length of the given array.  For each element $$\text{A[i]}$$, we check if some sequence is monotone increasing, which takes $$O(N)$$ steps.  In total, this is a complexity of $$O(N^2)$$.

* Space Complexity: To hold our array $$\text{new}$$, we need $$O(N)$$ space.

---
#### Approach #2: Reduce to Smaller Problem [Accepted]

**Intuition**

If $$\text{A[0]} &leq; \text{A[1]} &leq; \text{A[2]}$$, then we may remove $$\text{A[0]}$$ without changing the answer.  Similarly, if $$\text{A}\big[\text{len(A)-3}\big] &leq; \text{A}\big[\text{len(A)-2}\big] &leq; \text{A}\big[\text{len(A)-1}\big]$$, we may remove $$\text{A[len(A)-1]}$$ without changing the answer.

If the problem is solvable, then after these removals, very few numbers will remain.

**Algorithm**

Consider the interval $$\text{[i, j]}$$ corresponding to the subarray $$\big[\text{A[i], A[i+1], ..., A[j]}\big]$$.  When $$\text{A[i]} &leq; \text{A[i+1]} &leq; \text{A[i+2]}$$, we know we do not need to modify $$\text{A[i]}$$, and we can consider solving the problem on the interval $$\text{[i+1, j]}$$ instead.  We use a similar approach for $$j$$.

Afterwards, with the length of the interval under consideration being $$\text{j - i + 1}$$, if the interval has size 2 or less, then we did not find any problem.  

If our interval under consideration has 5 or more elements, then there are two disjoint problems that cannot be fixed with one replacement.  

Otherwise, our problem size is now at most 4 elements, which we can easily brute force.


<iframe src="https://leetcode.com/playground/4ypTHUiy/shared" frameBorder="0" name="4ypTHUiy" width="100%" height="343"></iframe>

**Complexity Analysis**

* Time Complexity: Let $$N$$ be the length of the given array.  Our pointers $$i$$ and $$j$$ move at most $$O(N)$$ times.  Our brute force is constant time as there are at most 4 elements in the array.  Hence, the complexity is $$O(N)$$.

* Space Complexity:  The extra array $$\text{A[i: j+1]}$$ only has at most 4 elements, so it is constant space, and so is the space used by our auxillary brute force algorithm.  In total, the space complexity is $$O(1)$$.

---
#### Approach #3: Locate and Analyze Problem Index [Accepted]

**Intuition**

Consider all indices $$p$$ for which $$\text{A[p]} > \text{A[p+1]}$$.  If there are zero, the answer is `True`.  If there are 2 or more, the answer is `False`, as more than one element of the array must be changed for $$\text{A}$$ to be monotone increasing.

At the problem index $$p$$, we only care about the surrounding elements.  Thus, immediately the problem is reduced to a very small size that can be analyzed by casework.

**Algorithm**

As before, let $$p$$ be the unique problem index for which $$\text{A[p]} > \text{A[p+1]}$$.  If this is not unique or doesn't exist, the answer is `False` or `True` respectively.  We analyze the following cases:

* If $$\text{p = 0}$$, then we could make the array good by setting $$\text{A[p] = A[p+1]}$$.
* If $$\text{p = len(A) - 2}$$, then we could make the array good by setting $$\text{A[p+1] = A[p]}$$.
* Otherwise, $$\text{A[p-1], A[p], A[p+1], A[p+2]}$$ all exist, and:
    * We could change $$\text{A[p]}$$ to be between $$\text{A[p-1]}$$ and $$\text{A[p+1]}$$ if possible, or;
    * We could change $$\text{A[p+1]}$$ to be between $$\text{A[p]}$$ and $$\text{A[p+2]}$$ if possible.

<iframe src="https://leetcode.com/playground/NGHYqESJ/shared" frameBorder="0" name="NGHYqESJ" width="100%" height="241"></iframe>

**Complexity Analysis**

* Time Complexity: Let $$N$$ be the length of the given array.  We loop through the array once, so our time complexity is $$O(N)$$.

* Space Complexity:  We only use $$p$$ and $$i$$, and the answer itself as the additional space.  The additional space complexity is $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] Simple greedy like solution with explanation
- Author: caihao0727mail
- Creation Date: Sun Aug 27 2017 11:31:53 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 10:09:48 GMT+0800 (Singapore Standard Time)

<p>
This problem is like a greedy problem. When you find ```nums[i-1] > nums[i]``` for some ```i```, you will prefer to change ```nums[i-1]```'s value, since a larger ```nums[i]``` will give you more risks that you get inversion errors after position ```i```. But, if you also find ```nums[i-2] > nums[i]```, then you have to change ```nums[i]```'s value instead, or else you need to change both of ```nums[i-2]```'s and ```nums[i-1]```'s values.

Java version:
```
 public boolean checkPossibility(int[] nums) {
        int cnt = 0;                                                                    //the number of changes
        for(int i = 1; i < nums.length && cnt<=1 ; i++){
            if(nums[i-1] > nums[i]){
                cnt++;
                if(i-2<0 || nums[i-2] <= nums[i])nums[i-1] = nums[i];                    //modify nums[i-1] of a priority
                else nums[i] = nums[i-1];                                                //have to modify nums[i]
            }
        }
        return cnt<=1; 
    }
```
C++ version:
```
bool checkPossibility(vector<int>& nums) {
        int cnt = 0;                                                                    //the number of changes
        for(int i = 1; i < nums.size() && cnt<=1 ; i++){
            if(nums[i-1] > nums[i]){
                cnt++;
                if(i-2<0 || nums[i-2] <= nums[i])nums[i-1] = nums[i];                    //modify nums[i-1] of a priority
                else nums[i] = nums[i-1];                                                //have to modify nums[i]
            }
        }
        return cnt<=1;
    } 
```
</p>


### Python Extremely Easy to Understand
- Author: yangshun
- Creation Date: Sun Aug 27 2017 11:41:30 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 10:17:09 GMT+0800 (Singapore Standard Time)

<p>
First, find a pair where the order is wrong. Then there are two possibilities, either the first in the pair can be modified or the second can be modified to create a valid sequence. We simply modify both of them and check for validity of the modified arrays by comparing with the array after sorting.

I find this approach the easiest to reason about and understand.

*- Yangshun*

```
class Solution(object):
    def checkPossibility(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        one, two = nums[:], nums[:]
        for i in range(len(nums) - 1):
            if nums[i] > nums[i + 1]:
                one[i] = nums[i + 1]
                two[i + 1] = nums[i]
                break
        return one == sorted(one) or two == sorted(two)
```

Thanks to @xiaoyudeng666 for the tip (:
</p>


### [C++] [Java] Clean Code - 6 liner Without Modifying Input
- Author: alexander
- Creation Date: Sun Aug 27 2017 11:03:43 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 25 2018 14:20:38 GMT+0800 (Singapore Standard Time)

<p>
The strategy is to `lower a[i-1]` to match `a[i]` if possible - (`a[i-2]` not exist or no smaller than `a[i]`);
otherwise `rise a[i]` to match `a[i-1]`.
```
2 Examples:
 0  ...  i ...
 1 1 2 4[2]5 6  - in this case we can just raise a[i] to 4;
         4
 1 1 2[4]2 3 4  - in this case lower a[i-1] is better;
       2
```
**Java - Modifying Input**
```
class Solution {
    public boolean checkPossibility(int[] a) {
        int modified = 0;
        for (int i = 1; i < a.length; i++) {
            if (a[i] < a[i - 1]) {
                if (modified++ > 0) return false;
                if (i - 2 < 0 || a[i - 2] <= a[i]) a[i - 1] = a[i]; // lower a[i - 1]
                else a[i] = a[i - 1]; // rise a[i]
            }
        }
        return true;
    }
}
```
We can also do it without modifying the input by using a variable `prev` to hold the `a[i-1]`; if we have to `lower a[i]` to match `a[i-1]` instead of `raising a[i-1]`, simply skip updating `prev`;
**Java - Without Modifying Input**
```
class Solution {
    public boolean checkPossibility(int[] a) {
        int modified = 0;
        for (int i = 1, prev = a[0]; i < a.length; i++) {
            if (a[i] < prev) {
                if (modified++ > 0) return false;
                if (i - 2 >= 0 && a[i - 2] > a[i]) continue;
            }
            prev = a[i];
        }
        return true;
    }
}
```
Or
```
class Solution {
    public boolean checkPossibility(int[] a) {
        int modified = 0;
        for (int i = 1, prev = a[0]; i < a.length; i++) {
            if (a[i] < prev && modified++ > 0) return false;
            if (a[i] < prev && i - 2 >= 0 && a[i - 2] > a[i]) continue;
            prev = a[i];
        }
        return true;
    }
}
```

**C++ - Modifying Input**
```
class Solution {
public:
    bool checkPossibility(vector<int>& a) {
        bool modified = false;
        for (int i = 1; i < a.size(); i++) {
            if (a[i] < a[i - 1]) {
                if (modified++) return false;
                (i - 2 < 0 || a[i - 2] <= a[i]) ? a[i - 1] = a[i] : a[i] = a[i - 1];
            }
        }
        return true;
    }
};
```
**C++ - Without Modifying Input**
```
class Solution {
public:
    bool checkPossibility(vector<int>& a) {
        bool modified = false;
        for (int i = 1, prev = a[0]; i < a.size(); i++) {
            if (a[i] < prev && modified++) return false;
            if (a[i] < prev && i - 2 >= 0 && a[i - 2] > a[i]) continue;
            prev = a[i];
        }
        return true;
    }
};
```
</p>


