---
title: "Longest Well-Performing Interval"
weight: 1099
#id: "longest-well-performing-interval"
---
## Description
<div class="description">
<p>We are given <code>hours</code>, a list of the number of hours&nbsp;worked per day for a given employee.</p>

<p>A day is considered to be a <em>tiring day</em> if and only if the number of hours worked is (strictly) greater than <code>8</code>.</p>

<p>A <em>well-performing interval</em> is an interval of days for which the number of tiring days is strictly larger than the number of non-tiring days.</p>

<p>Return the length of the longest well-performing interval.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> hours = [9,9,6,0,6,6,9]
<strong>Output:</strong> 3
<strong>Explanation: </strong>The longest well-performing interval is [9,9,6].
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= hours.length &lt;= 10000</code></li>
	<li><code>0 &lt;= hours[i] &lt;= 16</code></li>
</ul>

</div>

## Tags
- Stack (stack)

## Companies
- infosys - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] O(N) Solution, Life needs 996 and 669
- Author: lee215
- Creation Date: Sun Jul 14 2019 12:02:14 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 17 2020 10:10:52 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
If working hour > 8 hours, yes it\'s tiring day.
But I doubt if 996 is a well-performing interval.
Life needs not only 996 but also 669.
<br>

## **Explanation**
We starts with a `score = 0`,
If the working `hour > 8`, we plus 1 point.
Otherwise we minus 1 point.
We want find the maximum interval that have strict positive score.

After one day of work, if we find the total `score > 0`,
the whole interval has positive score,
so we set `res = i + 1`.

If the score is a new lowest score, we record the day by `seen[cur] = i`.
`seen[score]` means the first time we see the `score` is `seen[score]`th day.

We want a positive score, so we want to know the first occurrence of `score - 1`.
`score - x` also works, but it comes later than `score - 1`.
So the maximum interval is `i - seen[score - 1]`
<br>

## **Complexity**
Time `O(N)` for one pass.
Space `O(N)` in worst case if no tiring day.

<br>

**Java:**
```java
    public int longestWPI(int[] hours) {
        int res = 0, score = 0, n = hours.length;
        Map<Integer, Integer> seen = new HashMap<>();
        for (int i = 0; i < n; ++i) {
            score += hours[i] > 8 ? 1 : -1;
            if (score > 0) {
                res = i + 1;
            } else {
                seen.putIfAbsent(score, i);
                if (seen.containsKey(score - 1))
                    res = Math.max(res, i - seen.get(score - 1));
            }
        }
        return res;
    }
```

**C++:**
```cpp
    int longestWPI(vector<int>& hours) {
        int res = 0, score = 0, n = hours.size();
        unordered_map<int, int> seen;
        for (int i = 0; i < n; ++i) {
            score += hours[i] > 8 ? 1 : -1;
            if (score > 0) {
                res = i + 1;
            } else {
                if (seen.find(score) == seen.end())
                    seen[score] = i;
                if (seen.find(score - 1) != seen.end())
                    res = max(res, i - seen[score - 1]);
            }
        }
        return res;
    }
```

**Python:**
```python
    def longestWPI(self, hours):
        res = score = 0
        seen = {}
        for i, h in enumerate(hours):
            score = score + 1 if h > 8 else score - 1
            if score > 0:
                res = i + 1
            seen.setdefault(score, i)
            if score - 1 in seen:
                res = max(res, i - seen[score - 1])
        return res
```

</p>


### O(N) Without Hashmap. Generalized Problem&Solution: Find Longest Subarray With Sum >= K.
- Author: jiah
- Creation Date: Mon Jul 15 2019 02:10:19 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 05 2020 09:45:26 GMT+0800 (Singapore Standard Time)

<p>
You may have noticed that almost all `O(n)` `hashmap` solution posts are based on the `+1, -1` property. And these solutions try to find a longest subarrary with `sum > 0`.

Here I propose a more generalized problem and a solution to it.

Problem:
- input: arrary `arr` in which elements are arbitrary integers.
- output: length of a longest subarray `arr[i, j)` with `sum(arr[i], ... , arr[j-1]) >= K`.

Solution:
1. Compute prefix sum of `arr` as `prefixSum` where `prefixSum[i] = sum(arr[0], ... arr[i-1])` for `i > 0` and `prefixSum[0] = 0`.
2. Iterate through `prefixSum` **from begin to end** and build a **strictly monotone decreasing stack** `smdStack`. (`smdStack.top()` is the smallest)
3. Iterate through `prefixSum` **from end to begin**. For each `prefixSum[i]`, while `smdStack.top()` is less than `prefixSum[i]` by at least `K`, pop `smdStack`and try to update result by subarray [index of top,i). Until top element is not less than it by K.
4. Return result.

Time complexity: O(n)
- step1, compute prefixSum O(n)
- step2, build strictly monotone decreasing stack O(n)
- step3, iterate prefixSum O(n). pop top elements in smdStack O(n) 

Explanation:
For simplicity, call `(i, j)` a valid pair if the inequation `prefixSum[j] - prefixSum[i] >= K` holds. Our goal is to **optimize** `j-i` over all valid pair `(i, j)`. 
- Firstly, fix `j` and minimize `i`.  Consider any `i1` and `i2` that `i1 < i2 < j` and `prefixSum[i1] <= prefixSum[i2]`. It is obvious that `(i2, j)` can\'t be a candidate of optimal subarray because `(i1, j)` is more promising to be valid and longer than `(i2, j)`. Therefor candidates are monotone decreasing, and we can use a **strictly monotonic descresing stack** to find all candidates. (Monotone stack are just normal stack with a constraint that the elements in them are monotonically increasing or decreasing. When processing a new element, we either pop top elements then push new elem or discard this new element to guarantee the property of monotonicity. If you are not familiar with this data structure, you\'d better solve some similar leetcode problems before this one). 
- Secondly, fix `i` and maximize `j`. Consider any `j1` and `j2` that `i < j1 < j2` and `prefixSum[j2] - prefix[i] >= K`. We can find that `(i, j1)` can\'t be a candidate of optimal subarrary because `(i, j2)` is better. This discovery tells us that we should iterate `j` from end to begin and if we find a valid `(i, j)`, we don\'t need to keep `i` in `smdStack` any longer.

CPP solution for problem 1124 using monotone stack. (20ms, 11.2MB, faster than 100%)
```cpp
class Solution {
public:
    int longestWPI(vector<int>& hours) {
        int len = hours.size();
        vector<int> prefixSum(len+1, 0);
        for (int i = 1; i <= len; ++i) {
            prefixSum[i] = prefixSum[i-1] + (hours[i-1] > 8 ? 1 : -1);
        }
        stack<int> smdStack;
        for (int i = 0; i <= len; ++i) {
            if (smdStack.empty() || prefixSum[smdStack.top()] > prefixSum[i]) {
				// Trick, store index than value.
                smdStack.push(i);
            }
        }
        int res = 0;
        for (int j = len; j >= 0; --j) {
		    // For generalized problem:
		    // while (!smdStack.empty() && prefixSum[smdStack.top()]+K <= prefixSum[j]) {
            
			// For this problem:
			while (!smdStack.empty() && prefixSum[smdStack.top()] < prefixSum[j]) {
                res = max(res, j - smdStack.top());
                smdStack.pop();
            }
        }
        return res;
    }
};
```


Similar problems that can be solved by monotone increasing or decreasing stack/queue/list:
- [683. K Empty Slots](https://leetcode.com/problems/k-empty-slots/)
- [862. Shortest Subarray with Sum at Least K](https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/)
- [962. Maximum Width Ramp](https://leetcode.com/problems/maximum-width-ramp/)

</p>


### Java HashMap O(n) solution with explanation, similar to lc525
- Author: garhomlee
- Creation Date: Sun Jul 14 2019 12:20:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 23 2019 13:06:30 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int longestWPI(int[] hours) {
        if (hours.length == 0) return 0;
        int maxLen = 0;
        Map<Integer, Integer> map = new HashMap();  // key is possible sum in hours array, value is index where that sum appears for the first time
        int sum = 0;  // sum at index i indicates the sum of hours[0:i] after transformation
		
        for (int i = 0; i < hours.length; i++) {
            sum += hours[i] > 8 ? 1 : -1;  // transform each hour to 1 or -1
            if (!map.containsKey(sum)) {
                map.put(sum, i);  // record where the sum appears for the first time
            }
			
            if (sum > 0) {  // in hours[0:i], more 1s than -1s
                maxLen = i + 1;
            } else if (map.containsKey(sum - 1)) {  // get the index j where sum of hours[0:j] is sum - 1, so that sum of hours[j+1:i] is 1
                maxLen = Math.max(maxLen, i - map.get(sum - 1));
            }            
            
        }
        
        return maxLen;
    }
}
```
</p>


