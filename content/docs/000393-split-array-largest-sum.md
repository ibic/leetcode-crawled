---
title: "Split Array Largest Sum"
weight: 393
#id: "split-array-largest-sum"
---
## Description
<div class="description">
<p>Given an array <code>nums</code> which consists of non-negative integers and an integer <code>m</code>, you can split the array into <code>m</code> non-empty continuous subarrays.</p>

<p>Write an algorithm to minimize the largest sum among these <code>m</code> subarrays.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [7,2,5,10,8], m = 2
<strong>Output:</strong> 18
<strong>Explanation:</strong>
There are four ways to split nums into two subarrays.
The best way is to split it into [7,2,5] and [10,8],
where the largest sum among the two subarrays is only 18.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4,5], m = 2
<strong>Output:</strong> 9
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,4,4], m = 3
<strong>Output:</strong> 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 1000</code></li>
	<li><code>0 &lt;= nums[i] &lt;= 10<sup>6</sup></code></li>
	<li><code>1 &lt;= m &lt;= min(50, nums.length)</code></li>
</ul>

</div>

## Tags
- Binary Search (binary-search)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Baidu - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1 Brute Force [Time Limit Exceeded]

**Intuition**

Check all possible splitting plans to find the minimum largest value for subarrays.

**Algorithm**

We can use depth-first search to generate all possible splitting plan. For each element in the array, we can choose to append it to the previous subarray or start a new subarray starting with that element (if the number of subarrays does not exceed `m`). The sum of the current subarray can be updated at the same time.



<iframe src="https://leetcode.com/playground/2C8sEFce/shared" frameBorder="0" width="100%" height="500" name="2C8sEFce"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^m)$$. To split `n` elements into `m` parts, we can have $$\binom{n - 1}{m - 1}$$ different solutions. This is equivalent to $$ n ^ m$$.

* Space complexity : $$O(n)$$. We only need the space to store the array.

---
#### Approach #2 Dynamic Programming [Accepted]

**Intuition**

The problem satisfies the non-aftereffect property. We can try to use dynamic programming to solve it.

The non-aftereffect property means, once the state of a certain stage is determined, it is not affected by the state in the future. In this problem, if we get the largest subarray sum for splitting `nums[0..i]` into `j` parts, this value will not be affected by how we split the remaining part of `nums`.

To know more about non-aftereffect property, this link may be helpful : <http://www.programering.com/a/MDOzUzMwATM.html>

**Algorithm**

Let's define `f[i][j]` to be the minimum largest subarray sum for splitting `nums[0..i]` into `j` parts.

Consider the `j`th subarray. We can split the array from a smaller index `k` to `i` to form it. Thus `f[i][j]` can be derived from `max(f[k][j - 1], nums[k + 1] +  ... + nums[i])`. For all valid index `k`, `f[i][j]` should choose the minimum value of the above formula.

The final answer should be `f[n][m]`, where `n` is the size of the array.

For corner situations, all the invalid `f[i][j]` should be assigned with `INFINITY`, and `f[0][0]` should be initialized with `0`.


<iframe src="https://leetcode.com/playground/hwfoZm6N/shared" frameBorder="0" width="100%" height="480" name="hwfoZm6N"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2 * m)$$. The total number of states is $$O(n * m)$$. To compute each state `f[i][j]`, we need to go through the whole array to find the optimum `k`. This requires another $$O(n)$$ loop. So the total time complexity is $$O(n ^ 2 * m)$$.

* Space complexity : $$O(n * m)$$. The space complexity is equivalent to the number of states, which is $$O(n * m)$$.

---
#### Approach #3 Binary Search + Greedy [Accepted]

**Intuition**

We can easily find a property for the answer:
> If we can find a splitting method that ensures the maximum largest subarray sum will not exceed a value  `x`, then we can also find a splitting method that ensures the maximum largest subarray sum will not exceed any value `y` that is greater than `x`.

Lets define this property as `F(x)` for the value `x`. `F(x)` is true means we can find a splitting method that ensures the maximum largest subarray sum will not exceed `x`.

From the discussion above, we can find out that for `x` ranging from `-INFINITY` to `INFINITY`, `F(x)` will start with false, then from a specific value `x0`, `F(x)` will turn to true and stay true forever.

 Obviously, the specific value `x0` is our answer.

**Algorithm**

We can use Binary search to find the value `x0`. Keeping a value `mid = (left + right) / 2`. If `F(mid)` is false, then we will search the range `[mid + 1, right]`; If `F(mid)` is true, then we will search `[left, mid - 1]`.

For a given `x`, we can get the answer of `F(x)` using a greedy approach. Using an accumulator `sum` to store the sum of the current processing subarray and a counter `cnt` to count the number of existing subarrays. We will process the elements in the array one by one. For each element `num`, if `sum + num <= x`, it means we can add `num` to the current subarray without exceeding the limit. Otherwise, we need to make a cut here, start a new subarray with the current element `num`. This leads to an increment in the number of subarrays.

After we have finished the whole process, we need to compare the value `cnt` to the size limit of subarrays `m`. If `cnt <= m`, it means we can find a splitting method that ensures the maximum largest subarray sum will not exceed `x`. Otherwise, `F(x)` should be false.

<iframe src="https://leetcode.com/playground/Q43EfjVB/shared" frameBorder="0" width="100%" height="500" name="Q43EfjVB"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n * log(sum \ of \ array))$$. The binary search costs $$O(log(sum \ of \ array))$$, where `sum of array` is the sum of elements in `nums`. For each computation of `F(x)`, the time complexity is $$O(n)$$ since we only need to go through the whole array.

* Space complexity : $$O(n)$$. Same as the Brute Force approach.  We only need the space to store the array.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Clear Explanation: 8ms Binary Search Java
- Author: dax1ng
- Creation Date: Sun Oct 02 2016 17:29:43 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 13:08:52 GMT+0800 (Singapore Standard Time)

<p>
1. The answer is between maximum value of input array numbers and sum of those numbers.
2. Use binary search to approach the correct answer. We have ``` l = max number of array; r = sum of all numbers in the array;```Every time we do ```mid = (l + r) / 2;```
3. Use greedy to narrow down left and right boundaries in binary search.
    3.1 Cut the array from left.
    3.2 Try our best to make sure that the sum of numbers between each two cuts (inclusive) is large enough but still less than ```mid```.
   3.3 We'll end up with two results: either we can divide the array into more than m subarrays or we cannot.
         **If we can**, it means that the ```mid ``` value we pick is too small because we've already tried our best to make sure each part holds as many non-negative numbers as we can but we still have numbers left. So, it is impossible to cut the array into m parts and make sure each parts is no larger than ```mid```. We should increase m. This leads to ```l = mid + 1;```
        **If we can't**, it is either we successfully divide the array into m parts and the sum of each part is less than ```mid```, or we used up all numbers before we reach m. Both of them mean that we should lower ```mid``` because we need to find the minimum one. This leads to ```r = mid - 1;```
```
public class Solution {
    public int splitArray(int[] nums, int m) {
        int max = 0; long sum = 0;
        for (int num : nums) {
            max = Math.max(num, max);
            sum += num;
        }
        if (m == 1) return (int)sum;
        //binary search
        long l = max; long r = sum;
        while (l <= r) {
            long mid = (l + r)/ 2;
            if (valid(mid, nums, m)) {
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        return (int)l;
    }
    public boolean valid(long target, int[] nums, int m) {
        int count = 1;
        long total = 0;
        for(int num : nums) {
            total += num;
            if (total > target) {
                total = num;
                count++;
                if (count > m) {
                    return false;
                }
            }
        }
        return true;
    }
}
```
* list item
</p>


### [C++ / Fast / Very clear explanation / Clean Code] Solution with Greedy Algorithm and Binary Search
- Author: HaoxiangXu
- Creation Date: Mon Oct 03 2016 05:53:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 17:23:24 GMT+0800 (Singapore Standard Time)

<p>
First thing first, below is the code:
```
class Solution {
private:
    bool doable (const vector<int>& nums, int cuts, long long max) {
        int acc = 0;
        for (num : nums) {
            // This step is unnecessary for this problem. I didn't discard this line because I want doable function more generalized.
            if (num > max) return false;
            else if (acc + num <= max) acc += num;
            else {
                --cuts;
                acc = num;
                if (cuts < 0) return false;
            }
        }
        return true;
    }
    
public:
    int splitArray(vector<int>& nums, int m) {
        long long left = 0, right = 0;
        for (num : nums) {
            left = max(left, (long long)num);
            right += num;
        }
        
        while (left < right) {
            long long mid = left + (right - left) / 2;
            if (doable(nums, m - 1, mid)) right = mid;
            else left = mid + 1;
        }
        return left;
    }
};
```
---------------
Introduction to this problem:
---------------

We can break this problem into two smaller problems:
* Given an array (*A*), number of cuts (*CUTS*), and the **Largest sum of sub-arrays** (*MAX*). **Can you use at most *CUTS* cuts to segment array *A* into *CUTS + 1* sub-arrays, such that the sum of each sub-array is smaller or equal to *MAX***?
* Given a lower bound (*left*), an upper bound (*right*), an unknown bool array (*B*), and an API uses *i* as input and tells you whether *B[i]* is true. If we know there exists an index *k*, **that *B[i]* is false when i < k, and *B[i]* is true when i >= k**. What is the fastest way to **find this *k* (the lower bound)?**
-------------
Solution to the first sub-problem (Skip this part if you already knew how to solve 1st sub-problem):
-------------
For the first question, we can follow these steps:
* For each element in the array, if its value is larger than *MAX*, we know it's not possible to cut this array into groups that the sum of all groups are smaller than *MAX*. (Reason is straightforward, if *A* is [10, 2, 3, 5] and *MAX* is 6, even you have 3 cuts by which you can cut *A* as [[10], [2], [3], [5]], the group containing 10 will still be larger than 6).
* Use **greedy algorithm** to cut *A*. Use an **accumulator *ACC*** to store the sum of the currently processed group, and process elements in *A* one by one. For each element *num*, if we add *num* with *ACC* and the new sum is still no larger than *MAX*, we **update *ACC* to *ACC + num***, which means we can **merge *num* into the current group**. If not, we must **use a cut before *num* to segment this array**, then *num* will be the first element in the new group.
* If we **didn't go through *A* but already used up all cuts**, then it's not possible only using *CUTS* cuts to segment this array into groups to make sure **sum of each sub-array** is smaller than *MAX*. Otherwise, if we can reach the end of *A* with cuts left (or use exactly *CUTS* cuts). It's possible to do so.

Then the first question is solved.

Solution to the second sub-problem(Skip this part if you already knew how to solve 2nd sub-problem):
-------------
* The array *B* will be something like [false, false, ..., false, true, true, ..., true]. We want to find **the index of the first true**.
* Use **binary search** to find this *k*. Keep a value *mid*, **mid = (left + right) / 2**. If B[mid] = false, then move the search range to the upper half of the original search range, a.k.a **left = mid + 1**, otherwise move search range to the lower half, a.k.a **right = mid**. 
--------------
Why this algorithm is correct...
--------------
* No matter how we cut the array *A*, the **Largest sum of sub-arrays** will fall into a range [left, right]. ***Left* is the value of the largest element in this array. *right* is the sum of this array.** (e.g., Given array [1, 2, 3, 4, 5], if we have 4 cuts and cut it as [[1], [2], [3], [4], [5]], the **Largest sum of sub-arrays** is 5, it cannot be smaller. And if we have 0 cut, and the only sub-array is [[1, 2, 3, 4, 5]], the **Largest sum of sub-arrays** is 15, it cannot be larger).
* However, we cannot decide the number of cuts (*CUTS*), this is an given constraint. But we know there must be a magic number *k*, which is the smallest value of the **Largest sum of sub-arrays** when given *CUTS* cuts. When the **Largest sum of sub-arrays** is larger than *k*, we can always find a way to cut *A* within *CUTS* cuts. When the **Largest sum of sub-arrays** is smaller than *k*, there is no way to do this.

Example
------
For example, given array *A* **[1, 2, 3, 4, 5]**. We can use **2** cuts.
* No matter how many cuts are allowed, the range of the possible value of the **Largest sum of sub-arrays** is [5, 15].
* When given 2 cuts, we can tell the magic number *k* here is 6, the result of segmentation is [[1, 2, 3], [4], [5]].
* When **Largest sum of sub-arrays** is in range [6, 15], we can always find a way to cut this array within two cuts. You can have a try.
* However, when **Largest sum of sub-arrays** is in range [5, 5], there is no way to do this.
* This mapped this problem into the second sub-problem. Bool array *B* here is [5:false, 6:true, 7:true, 8:true, ..., 15:true]. We want to find the **index *i* of the first true in *B*, which is the answer of this entire question**, and by solving the first sub-problem, we have an API that can tell us **given an *i* (*Largest sum of sub-arrays*), whether *B[i]* is true (whether we can find a way to cut *A* to satisfy the constraint)**.

**Below is the code with comment, just in case you don't have time to read the explanations above.**
```
class Solution {
private:
    /* 
        Params:
            nums - The input array; 
            cuts - How many cuts are available (cuts = #groups - 1); 
            max - The maximum of the (sum of elements in one group);
        Rtn:
            Whether we can use at most 'cuts' number of cuts to segment the entire array, 
            such that the sum of each group will not exceed 'max'.
     */
    bool doable (const vector<int>& nums, int cuts, long long max) {
        
        // 'acc' is the temporary accumulator for the currently processed group.
        
        int acc = 0;
        for (num : nums) {
            
            // If the current processed element in this array is larger than 'max', we cannot segment the array.
            // (Reason is straightforward, if 'nums' is [10, 2, 3, 5] and 'max' is 6, even you can have 3 cuts
            // (by which you can cut array as [[10], [2], [3], [5]]), the group containing 10 will be larger than 6, 
            //  there is no way to do this).
            // Ps: This step is unnecessary in this solution. Because 'left' in the splitArray() function can assure 
            // 'max' will be larger than every single element. I just want to write a generalized doable() function :)
            
            if (num > max) return false;
            
            // If the (sum of the currently processed group) + (current element) is smaller than max, we can add current 
            // element into this group.
            
            else if (acc + num <= max) acc += num;
            
            // If not, we will make a cut before this element, and this element will be the first element in the new group.
            
            else {
                --cuts;
                acc = num;
                
                // If we've used up all cuts, this means this 'max' is not doable.
                if (cuts < 0) return false;
            }
        }
        
        // If we can reach here, this means we've used at most 'cuts' cut to segment the array, and the sum of each groups is
        // not larger than 'max'. Yeah!
        return true;
    }
    
public:
    int splitArray(vector<int>& nums, int m) {
        // Use long long to avoid overflow.
        long long left = 0, right = 0;
        // The smallest possible value ('left') is the the value of the largest element in this array.
        // The largest possible value ('right') is the sum of all elements in this array.
        for (num : nums) {
            left = max(left, (long long)num);
            right += num;
        }
        
        // Use binary search, find the lower bound of the possible (minimum sum of groups within m - 1 cuts).
        while (left < right) {
            long long mid = left + (right - left) / 2;
            if (doable(nums, m - 1, mid)) right = mid;
            else left = mid + 1;
        }
        return left;
    }
};
```
</p>


### Python solution with detailed explanation
- Author: gabbu
- Creation Date: Sat Feb 11 2017 14:03:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 07 2018 04:25:04 GMT+0800 (Singapore Standard Time)

<p>
**Solution**

**Split Array Largest Sum** https://leetcode.com/problems/split-array-largest-sum/

**BruteForce**
```
class Solution(object):
    def helper(self, nums, m):
        if nums == []:
            return 0
        elif m == 1:
            return sum(nums)
        else:
            min_result = float('inf')
            for j in range(1,len(nums)+1):
                left, right = sum(nums[:j]), self.helper(nums[j:], m-1)
                min_result = min(min_result, max(left, right))
            return min_result
    
    def splitArray(self, nums, m):
        """
        :type nums: List[int]
        :type m: int
        :rtype: int
        """
        return self.helper(nums, m)
```

**Memoization**
```
from collections import defaultdict    
class Solution(object):
    def helper(self, i, nums, m, cache):
        if i == len(nums):
            return 0
        elif m == 1:
            return sum(nums[i:])
        else:
            if i in cache and m in cache[i]:
                return cache[i][m]
            cache[i][m] = float('inf')
            for j in xrange(1,len(nums)+1):
                left, right = sum(nums[i:i+j]), self.helper(i+j, nums, m-1, cache)
                cache[i][m] = min(cache[i][m], max(left, right))
                if left > right:
                    break
            return cache[i][m]
    
    def splitArray(self, nums, m):
        """
        :type nums: List[int]
        :type m: int
        :rtype: int
        """
        cache = defaultdict(dict)
        return self.helper(0, nums, m, cache)
```

**Memoization + Cumulative Sum**
```
from collections import defaultdict            
class Solution(object):
    def helper(self, i, nums, m, cache, cums):
        if i == len(nums):
            return 0
        elif m == 1:
            return sum(nums[i:])
        else:
            if i in cache and m in cache[i]:
                return cache[i][m]
            cache[i][m] = float('inf')
            for j in range(1,len(nums)+1):
                left, right = cums[i+j] - cums[i], self.helper(i+j, nums, m-1, cache, cums)
                cache[i][m] = min(cache[i][m], max(left, right))
                if left > right:
                    break
            return cache[i][m]
    
    def splitArray(self, nums, m):
        """
        :type nums: List[int]
        :type m: int
        :rtype: int
        """
        cums = [0]
        for x in nums:
            cums.append(cums[-1]+x)
        cache = defaultdict(dict)            
        return self.helper(0, nums, m, cache, cums)
```

**Binary Search Based Solution**
* Imagine we split an array into m different sub-arrays. There can be several ways to do this split. Let us assume we take one possible split.
* In this particular split, we take the sum of each subarray j and call it S(j) where j is from 1 to m. Then we figure out the sub-array which has the maximum sum from all of these m different sums and call it max_sum(array, m). 
* What is the least possible value of max_sum(array, m)? Answer will be max(array) - this must be obvious. The max(array) value must be in one of the m sub-arrays. The least possible amongst all possible m different sub-arrays would be a sub-array with a single element as the max(array).
* What is the maximum possible value of max_sum(array, m)? Answer will be sum(array) - a subarray with all elements.
* So the range of max_sum(array, m) is max(array) to sum(array).
* We now have a search problem - we need to search within the range max(array) to sum(array) such that  we find the minimum value in this range with which we can form at-most m sub-arrays such no sub-array has sum more than this value. To efficiently search a sorted range we use binary search.
* Imagine we pick a value mid and find that we could make more sub-arrays than m. This means we picked too small value (check the code to understand this). We should set low = mid + 1.
* Imagine we pick a value mid and find we could make less sub-arrays than m. Now we can easily split those sub-arrays to increase the count and still make sure that the maximum sum of those sub-arrays is less than mid (splitting will only decrease mid). In this case, we record a potential solution and make high = mid-1, hoping to get an even better solution later.
* Lets use an example: [7,2,5,10,8] and 2
* max_sum([7,2,5,10,8], 2) will be in the range [10, 32] - i.e. any split of the array into 2 sub-array will have sum of the sub-array between [10, 32].
* Now we want to find the minimum value in this range with which we can form 2 sub-arrays. Lets do this linearly. Can we use 10? Using 10, we can form [7, 2]; [5]; [10]; [8] - 4 subarrays. We clearly need to increase the minimum value so that we can reduce from 4 subarrays. 
* What if we used binary search and started with mid = (10+32)/2 = 21. This gives us [7,2,5]; [10,8] - This is valid solution. Can we do better? We record 21 and reduce our range to [10, 20].
* This gives us mid as 15. [7,2,5]; [10]; [8] - Invalid! we got more than 2 sub-arrays. We need to increase low to mid+1 and search in the range [16, 20].
* [16, 20] gives us 18. [7,2,5]; [10,8] - This is a valid solution. Can we do better than 18? Let us search in the range [16,17]
* [16,17] gives mid as 16. [7,2,5]; [10]; [8]. This is invalid and we need to increase range. New range is [17,17]. This again gives [7,2,5]; [10]; [8] and we get the new range as [18,17].
* [18,17] breaks the while loop! We have recorded 18 as the last answer and return it.

```
class Solution(object):
    def is_valid(self, nums, m, mid):
        # assume mid is < max(nums)
        cuts, curr_sum  = 0, 0
        for x in nums:
            curr_sum += x
            if curr_sum > mid:
                cuts, curr_sum = cuts+1, x
        subs = cuts + 1
        return (subs <= m)
    
    def splitArray(self, nums, m):
        """
        :type nums: List[int]
        :type m: int
        :rtype: int
        """
        low, high, ans = max(nums), sum(nums), -1
        while low <= high:
            mid = (low+high)//2
            if self.is_valid(nums, m, mid): # can you make at-most m sub-arrays with maximum sum atmost mid 
                ans, high = mid, mid-1
            else:
                low = mid + 1
        return ans
```

https://discuss.leetcode.com/topic/61395/c-fast-very-clear-explanation-clean-code-solution-with-greedy-algorithm-and-binary-search/2
https://discuss.leetcode.com/topic/61315/java-easy-binary-search-solution-8ms
</p>


