---
title: "Shortest Distance in a Plane"
weight: 1503
#id: "shortest-distance-in-a-plane"
---
## Description
<div class="description">
Table <code>point_2d</code> holds the coordinates (x,y) of some unique points (more than two) in a plane.
<p>&nbsp;</p>
Write a query to find the shortest distance between these points rounded to 2 decimals.

<p>&nbsp;</p>

<pre>
| x  | y  |
|----|----|
| -1 | -1 |
| 0  | 0  |
| -1 | -2 |
</pre>

<p>&nbsp;</p>
The shortest distance is 1.00 from point (-1,-1) to (-1,2). So the output should be:

<p>&nbsp;</p>

<pre>
| shortest |
|----------|
| 1.00     |
</pre>

<p>&nbsp;</p>
<b>Note:</b> The longest distance among all the points are less than 10000.

<p>&nbsp;</p>

</div>

## Tags


## Companies
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Using `SQRT`, `POW()` functions and math knowledge [Accepted]

**Intuition**

Calculate the distances between each two points and then display the smallest one.

**Algorithm**

The [euclidean distance](https://en.wikipedia.org/wiki/Euclidean_distance) between two points P1(x1,y1) and P2(x2, y2) in two dimensions is defined as $$\sqrt{(x1-x2)^2+(y1-y2)^2}$$. So in order to get the distances, we can join this table with itself, and then utilize the built-in function `POW()` and `SQRT()` like below.

```sql
SELECT
    p1.x,
    p1.y,
    p2.x,
    p2.y,
    SQRT((POW(p1.x - p2.x, 2) + POW(p1.y - p2.y, 2))) AS distance
FROM
    point_2d p1
        JOIN
    point_2d p2 ON p1.x != p2.x OR p1.y != p2.y
;
```

>Note:
> - The condition 'p1.x != p2.x OR p2.y != p2.y' is to avoid calculating the distance of a point with itself.
> Otherwise, the minimum distance will be always zero.
> - The columns p1.x, p1.y, p2.x and p2.y are for demonstrating. They are not necessary for the final solution.

So the output would be as below after running this code on the sample data.
```
| x  | y  | x  | y  | distance           |
|----|----|----|----|--------------------|
| 0  | 0  | -1 | -1 | 1.4142135623730951 |
| -1 | -2 | -1 | -1 | 1                  |
| -1 | -1 | 0  | 0  | 1.4142135623730951 |
| -1 | -2 | 0  | 0  | 2.23606797749979   |
| -1 | -1 | -1 | -2 | 1                  |
| 0  | 0  | -1 | -2 | 2.23606797749979   |
```

At last, choose the minimum distance and round it to 2 decimals as required.

**MySQL**

```sql
SELECT
    ROUND(SQRT(MIN((POW(p1.x - p2.x, 2) + POW(p1.y - p2.y, 2)))), 2) AS shortest
FROM
    point_2d p1
        JOIN
    point_2d p2 ON p1.x != p2.x OR p1.y != p2.y
;
```
>Note: To put the MIN() inside of SQRT() will slightly improve the performance.

#### Approach 2: Optimize to avoid reduplicate calculations [Accepted]

**Intuition**

It is unnecessary to calculate the distance between all points to all other points since some of them may already be done.
So how to avoid the reduplicate calculations?

**Algorithm**

When join the table with itself, we can claim to only calculate the distance between one point to another point in a certain rule such ponts with bigger x value.
By following this rule, we can avoid quite a lot of reduplicate calculations.

```sql
SELECT
    t1.x,
    t1.y,
    t2.x,
    t2.y,
    SQRT((POW(t1.x - t2.x, 2) + POW(t1.y - t2.y, 2))) AS distance
FROM
    point_2d t1
        JOIN
    point_2d t2 ON (t1.x <= t2.x AND t1.y < t2.y)
        OR (t1.x <= t2.x AND t1.y > t2.y)
        OR (t1.x < t2.x AND t1.y = t2.y)
;
```

The output is as below for the sample data. You may notice that there are only 4 records, 1/3 less than the previous solution.

```
| x  | y  | x  | y  | distance           |
|----|----|----|----|--------------------|
| -1 | -2 | -1 | -1 | 1                  |
| -1 | -1 | 0  | 0  | 1.4142135623730951 |
| -1 | -2 | 0  | 0  | 2.23606797749979   |
| -1 | -1 | -1 | -2 | 1                  |
```

>Note:
The best case is to compare n*(n-1)/2 times, but practically it is not always true considering two points may have same x value or y value.
In this case, you may notice the distance between (-1, -2) and (-1, -1) appearing twice in the first and last line in the output.

Here comes the solution to select the shortest distance and round to two decimals.

**MySQL**

```sql
SELECT
    ROUND(SQRT(MIN((POW(p1.x - p2.x, 2) + POW(p1.y - p2.y, 2)))),2) AS shortest
FROM
    point_2d p1
        JOIN
    point_2d p2 ON (p1.x <= p2.x AND p1.y < p2.y)
        OR (p1.x <= p2.x AND p1.y > p2.y)
        OR (p1.x < p2.x AND p1.y = p2.y)
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My easy understood solution
- Author: artistscript
- Creation Date: Sun Jun 04 2017 21:37:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 06:50:43 GMT+0800 (Singapore Standard Time)

<p>
```
select ROUND(SQRT(min((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y))),2) as shortest
from point_2d p1,point_2d p2
where p1.x <> p2.x or p1.y <> p2.y;
```
</p>


### There is an error in the problem description
- Author: eldol
- Creation Date: Sun Feb 23 2020 03:14:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 03:14:07 GMT+0800 (Singapore Standard Time)

<p>
This error in the description: 
```
The shortest distance is 1.00 from point (-1,-1) to (-1,2). So the output should be: 1
```
This doesn\'t make sense, it should be should be:
```
The shortest distance is 1.00 from point (-1,-1) to (-1,-2). So the output should be: 1
```
</p>


### Simplify the logic with window function
- Author: tainangao
- Creation Date: Sat Jul 25 2020 00:32:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 25 2020 00:36:42 GMT+0800 (Singapore Standard Time)

<p>
This solution is faster than 88% so far
```
with t as    
    (select *, row_number() over() row_n
    from point_2d)

select round(sqrt(min(power((a.x-b.x),2) + power((a.y-b.y),2))), 2) shortest
from t a
join t b on a.row_n>b.row_n
```
` a.row_n>b.row_n` also avoids calculating duplicate point pairs
</p>


