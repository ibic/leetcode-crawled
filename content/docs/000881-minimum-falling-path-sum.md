---
title: "Minimum Falling Path Sum"
weight: 881
#id: "minimum-falling-path-sum"
---
## Description
<div class="description">
<p>Given a <strong>square</strong> array of integers <code>A</code>, we want the <strong>minimum</strong> sum of a <em>falling path</em> through <code>A</code>.</p>

<p>A falling path starts at any element in the first row, and chooses one element from each row.&nbsp; The next row&#39;s choice must be in a column that is different from the previous row&#39;s column by at most one.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[1,2,3],[4,5,6],[7,8,9]]</span>
<strong>Output: </strong><span id="example-output-1">12</span>
<strong>Explanation: </strong>
The possible falling paths are:
</pre>

<ul>
	<li><code>[1,4,7], [1,4,8], [1,5,7], [1,5,8], [1,5,9]</code></li>
	<li><code>[2,4,7], [2,4,8], [2,5,7], [2,5,8], [2,5,9], [2,6,8], [2,6,9]</code></li>
	<li><code>[3,5,7], [3,5,8], [3,5,9], [3,6,8], [3,6,9]</code></li>
</ul>

<p>The falling path with the smallest sum is <code>[1,4,7]</code>, so the answer is <code>12</code>.</p>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= A.length == A[0].length &lt;= 100</code></li>
	<li><code>-100 &lt;= A[i][j] &lt;= 100</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Goldman Sachs - 4 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Dynamic Programming

**Intuition**

This problem has an optimal substructure, meaning that the solutions to subproblems can be used to solve larger instances of this problem.  This makes dynamic programming an ideal candidate.

Let `dp(r, c)` be the minimum total weight of a falling path starting at `(r, c)` and reaching the bottom row.

Then, `dp(r, c) = A[r][c] + min(dp(r+1, c-1), dp(r+1, c), dp(r+1, c+1))`, and the answer is $$\min\limits_c \text{dp}(0, c)$$.

**Algorithm**

Usually, we would make an auxillary array `dp` to cache intermediate values `dp(r, c)`.  However, this time we will use `A` to cache these values.  Our goal is to transform the values of `A` into the values of `dp`.

We process each row, starting with the second last.  (The last row is already correct.)  We set `A[r][c] = min(A[r+1][c-1], A[r+1][c], A[r+1][c+1])`, handling boundary conditions gracefully.

Let's look at the recursion a little more to get a handle on why it works.  For an array like `A = [[1,1,1],[5,3,1],[2,3,4]]`, imagine you are at `(1, 0)` (`A[1][0] = 5`).  You can either go to `(2, 0)` and get a weight of 2, or `(2, 1)` and get a weight of 3.  Since 2 is lower, we say that the minimum total weight at `(1, 0)` is `dp(1, 0) = 5 + 2` (5 for the original `A[r][c]`.)

After visiting `(1, 0)`, `(1, 1)`, and `(1, 2)`, `A` [which is storing the values of our `dp`], looks like `[[1,1,1],[7,5,4],[2,3,4]]`.  We do this procedure again by visiting `(0, 0)`, `(0, 1)`, `(0, 2)`.  We get `A = [[6,5,5],[7,5,4],[2,3,4]]`, and the final answer is `min(A[0]) = 5`

<iframe src="https://leetcode.com/playground/rH8Cg2WS/shared" frameBorder="0" width="100%" height="412" name="rH8Cg2WS"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$ in *additional* space complexity.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java 4 lines DP
- Author: votrubac
- Creation Date: Sun Oct 28 2018 11:05:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 28 2018 11:05:49 GMT+0800 (Singapore Standard Time)

<p>
The minimum path to get to element ```A[i][j]``` is the minimum of ```A[i - 1][j - 1]```, ```A[i - 1][j]``` and ```A[i - 1][j + 1]```. 
Starting from row 1, we add the minumum path to each element. The smallest number in the last row is the miminum path sum.
Example:
[1, 2, 3]
[4, 5, 6] => [5, 6, 8]
[7, 8, 9] => [7, 8, 9] => [12, 13, 15]
```
int minFallingPathSum(vector<vector<int>>& A) {
  for (auto i = 1; i < A.size(); ++i)
    for (auto j = 0; j < A.size(); ++j)
      A[i][j] += min({ A[i-1][j], A[i-1][max(0,j-1)], A[i-1][min((int)A.size()-1,j+1)] });
  return *min_element(begin(A[A.size() - 1]), end(A[A.size() - 1]));
}
```
Java version:
```
public int minFallingPathSum(int[][] A) {
  for (int i = 1; i < A.length; ++i)
    for (int j = 0; j < A.length; ++j)
      A[i][j] += Math.min(A[i - 1][j], Math.min(A[i - 1][Math.max(0, j - 1)], A[i - 1][Math.min(A.length - 1, j + 1)]));
  return Arrays.stream(A[A.length - 1]).min().getAsInt();
}        
```
</p>


### Java DP solution, with graph illustrated explanations
- Author: YFGu0618
- Creation Date: Sun Oct 28 2018 11:11:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 28 2018 11:11:51 GMT+0800 (Singapore Standard Time)

<p>
The solution is definitely not optimal, nor is the code as "elegant", but the idea is straght forward, hope it helps :)

Given matrix like the left part in the graph below, we first initialize a 2D DP matrix, and then iterate the original matrix row by row. For each element in DP matrix, we sum up the **corresponding element from original matrix** with the **minimum neighbors from previous row in DP matrix**. 
In order to save the future hassles dealing with index boundries while iterating the matrix, I added the **two extra columns** and assigned them as `Integer.MAX_VALUE`.

The idea is illustrated as following:
![image](https://assets.leetcode.com/users/yfgu0618/image_1540698728.png)

Following is the code:
```java
public static int minFallingPathSum(int[][] A) {
  int rows = A.length;
  int cols = A[0].length;
  // DP matrix has 2 extra columns
  int[][] dp = new int[rows][cols + 2];
  // Fill the first row of DP matrix
  for (int i = 1; i <= cols; i++) {
    dp[0][i] = A[0][i - 1];
  }
  // Fill Integer.MAX_VALUE into first and last column of DP matrix
  for (int i = 0; i < rows; i++) {
    dp[i][0] = Integer.MAX_VALUE;
    dp[i][cols + 1] = Integer.MAX_VALUE;
  }
  // Building the DP matrix
  for (int i = 1; i < rows; i++) {
    for (int j = 1; j <= cols; j++) {
      // Find the minimum neighbor from previous row in DP matrix
      int minNeighbor = Math.min(dp[i - 1][j - 1], dp[i - 1][j]);
      minNeighbor = Math.min(minNeighbor, dp[i - 1][j + 1]);
      dp[i][j] = A[i][j - 1] + minNeighbor;
    }
  }
  // The minimum path sum is minimum of the last row in DP matrix
  int min = Integer.MAX_VALUE;
  for (int i = 1; i <= cols; i++) {
    min = Math.min(min, dp[rows - 1][i]);
  }
  return min;
}
```

</p>


### Ambiguous/Confusing Wording
- Author: cashstramel
- Creation Date: Sun Apr 19 2020 03:33:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 03:33:58 GMT+0800 (Singapore Standard Time)

<p>
Am I the only one that was a bit confused but the way the problem was worded?

Break up the statement.

"*The next row\'s choice must be in a column that is different from the previous row\'s column...*" and "*... by at most one*".

"***must be in a column that is different***"

Given the sample, [[1,2,3],[4,5,6],[7,8,9]], as an example, I was thinking that starting at (0,1), val = 2, the valid next options were (1,0), val = 4 and (1,2), val = 6.  An invalid next option would (1,1), val = 5 because it\'s in the same column as the previous row\'s choice.  But looking that example given, (1,1), val = 5 is valid.

I\'d rephrase it for clarity.

"*The next row\'s choice must be in a column that is at most one different from the previous row\'s column.*"
</p>


