---
title: "Inorder Successor in BST"
weight: 268
#id: "inorder-successor-in-bst"
---
## Description
<div class="description">
<p>Given a binary search tree and a node in it, find the in-order successor of that node in the BST.</p>

<p>The successor of a node&nbsp;<code>p</code>&nbsp;is the node with the smallest key greater than&nbsp;<code>p.val</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/01/23/285_example_1.PNG" style="width: 122px; height: 117px;" />
<pre>
<strong>Input: </strong>root = <span id="example-input-1-1">[2,1,3]</span>, p = <span id="example-input-1-2">1</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>1&#39;s in-order successor node is 2. Note that both p and the return value is of TreeNode type.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/01/23/285_example_2.PNG" style="width: 246px; height: 229px;" />
<pre>
<strong>Input: </strong>root = <span id="example-input-2-1">[5,3,6,2,4,null,null,1]</span>, p = <span id="example-input-2-2">6</span>
<strong>Output: </strong><span id="example-output-2">null</span>
<strong>Explanation: </strong>There is no in-order successor of the current node, so the answer is <code>null</code>.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>If the given node has no in-order successor in the tree, return <code>null</code>.</li>
	<li>It&#39;s guaranteed that the values of the tree are unique.</li>
</ol>

</div>

## Tags
- Tree (tree)

## Companies
- Citadel - 5 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: true)
- Bloomberg - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Quip (Salesforce) - 2 (taggedByAdmin: false)
- Palantir Technologies - 3 (taggedByAdmin: false)
- Zillow - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Pocket Gems - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Iterative Inorder Traversal

[Inorder traversal of BST 
is an array sorted in the ascending order](https://leetcode.com/articles/delete-node-in-a-bst/). 

> Successor is the smallest node in the inorder traversal 
_after_ the current one. 

There could be two situations :

1. If the node has a right child, the successor is somewhere lower
in the tree, see red nodes on the Fig. below.

2. Otherwise, the successor is somewhere upper in the tree,
see blue nodes on the Fig.

![img](../Figures/285/succ.png)

Let's first check the simple case 1.
To find a successor, go one step right 
and then left till you can. 

![pic](../Figures/285/right.png)

This approach has $$\mathcal{O}(H_p)$$ time complexity,
where $$H_p$$ is a height of the node $$p$$.

Now let's consider the case 2. 
There is no access to the parent nodes here,
and hence one has to traverse the 
tree starting from the root and _not_ from the node. 

[Inorder traversal could be implemented in three 
different ways: recursive, iterative and Morris](https://leetcode.com/articles/recover-binary-search-tree/).
Here it's better to choose the iterative traversal 
in order to optimise the performance and minimize the space
at the same time. 

> Iterative inorder traversal is simple: 
go left till you can, and then one step right. 
Repeat till the end of nodes in the tree.

The idea is to keep just one previous node during the 
inorder traversal. If that previous node is equal to `p`,
then the current node is a successor of `p`.  

![pac](../Figures/285/case2.png)

This approach has $$\mathcal{O}(H)$$ time complexity,
where $$H$$ is a tree height. Basically, this 
approach is universal and could be used for the case 1 as well. 
We simply don't do that because the approach 1 is faster. 

**Algorithm**

- If the node has a right child, 
go one step right and then left till you can.
Return the successor.

- Otherwise, implement iterative inorder traversal. 
While there are still nodes in the tree or in the stack:
    
    - Go left till you can, adding nodes in stack. 
    
    - Pop out the last node. 
    If its predecessor is equal to `p`, return that last node.
    Otherwise, save that node to be the predecessor
    in the next turn of the loop.
    
    - Go one step right.
    
- If we're here that means the successor doesn't exit.
Return null. 

**Implementation**

<iframe src="https://leetcode.com/playground/XNBpVR2N/shared" frameBorder="0" width="100%" height="500" name="XNBpVR2N"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(H_p)$$ in the best case,
when node `p` has a right child. Here $$H_p$$ is a height of node `p`.
$$\mathcal{O}(H)$$ in the worst case of no right child. 
Here $$H$$ is a tree height.

* Space complexity : $$\mathcal{O}(1)$$ in the best case,
when node `p` has a right child. Otherwise, up to $$\mathcal{O}(H)$$ 
to keep the stack.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Share my Java recursive solution
- Author: jeantimex
- Creation Date: Wed Sep 23 2015 01:28:26 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 22:38:20 GMT+0800 (Singapore Standard Time)

<p>
Just want to share my recursive solution for both getting the successor and predecessor for a given node in BST.

**Successor**

    public TreeNode successor(TreeNode root, TreeNode p) {
      if (root == null)
        return null;
    
      if (root.val <= p.val) {
        return successor(root.right, p);
      } else {
        TreeNode left = successor(root.left, p);
        return (left != null) ? left : root;
      }
    }


**Predecessor**

    public TreeNode predecessor(TreeNode root, TreeNode p) {
      if (root == null)
        return null;
    
      if (root.val >= p.val) {
        return predecessor(root.left, p);
      } else {
        TreeNode right = predecessor(root.right, p);
        return (right != null) ? right : root;
      }
    }
</p>


### Java/Python solution, O(h) time and O(1) space, iterative
- Author: dietpepsi
- Creation Date: Tue Sep 29 2015 12:05:46 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 15:00:12 GMT+0800 (Singapore Standard Time)

<p>
The inorder traversal of a BST is the nodes in ascending order. To find a successor, you just need to find the  smallest one that is larger than the given value since there are no duplicate values in a BST. It just like the binary search in a sorted list. The time complexity should be `O(h)` where h is the depth of the result node. `succ` is a pointer that keeps the possible successor. Whenever you go left the current root is the new possible successor, otherwise the it remains the same.

Only in a balanced BST `O(h) = O(log n)`. In the worst case `h` can be as large as `n`.

**Java**

    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        TreeNode succ = null;
        while (root != null) {
            if (p.val < root.val) {
                succ = root;
                root = root.left;
            }
            else
                root = root.right;
        }
        return succ;
    }

    // 29 / 29 test cases passed.
    // Status: Accepted
    // Runtime: 5 ms

**Python**

    def inorderSuccessor(self, root, p):
        succ = None
        while root:
            if p.val < root.val:
                succ = root
                root = root.left
            else:
                root = root.right
        return succ

    # 29 / 29 test cases passed.
    # Status: Accepted
    # Runtime: 112 ms
</p>


### *Java* 5ms short code with explanations
- Author: ElementNotFoundException
- Creation Date: Tue Jan 05 2016 05:53:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 03:02:48 GMT+0800 (Singapore Standard Time)

<p>
The idea is to compare `root`'s value with `p`'s value if `root` is not null, and consider the following two cases:

 - `root.val > p.val`. In this case, `root` can be a possible answer, so we store the root node first and call it `res`. However, we don't know if there is anymore node on `root`'s left that is larger than `p.val`. So we move root to its left and check again.

 - `root.val <= p.val`. In this case, `root` cannot be `p`'s inorder successor, neither can `root`'s left child. So we only need to consider `root`'s right child, thus we move root to its right and check again.

We continuously move `root` until exhausted. To this point, we only need to return the `res` in case 1.

Code in Java:

    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        TreeNode res = null;
        while(root!=null) {
	        if(root.val > p.val) {
	        	res = root;
	        	root = root.left;
	        }
	        else root = root.right;
        }
        return res;
    }
</p>


