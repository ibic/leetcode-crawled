---
title: "Consecutive Numbers"
weight: 1474
#id: "consecutive-numbers"
---
## Description
<div class="description">
<p>Write a SQL query to find all numbers that appear at least three times consecutively.</p>

<pre>
+----+-----+
| Id | Num |
+----+-----+
| 1  |  1  |
| 2  |  1  |
| 3  |  1  |
| 4  |  2  |
| 5  |  1  |
| 6  |  2  |
| 7  |  2  |
+----+-----+
</pre>

<p>For example, given the above <code>Logs</code> table, <code>1</code> is the only number that appears consecutively for at least three times.</p>

<pre>
+-----------------+
| ConsecutiveNums |
+-----------------+
| 1               |
+-----------------+
</pre>

</div>

## Tags


## Companies
- Amazon - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `DISTINCT` and `WHERE` clause [Accepted]

**Algorithm**

Consecutive appearing means the Id of the Num are next to each others. Since this problem asks for numbers appearing at least three times consecutively, we can use 3 aliases for this table **Logs**, and then check whether 3 consecutive numbers are all the same.

```sql
SELECT *
FROM
    Logs l1,
    Logs l2,
    Logs l3
WHERE
    l1.Id = l2.Id - 1
    AND l2.Id = l3.Id - 1
    AND l1.Num = l2.Num
    AND l2.Num = l3.Num
;
```
| Id | Num | Id | Num | Id | Num |
|----|-----|----|-----|----|-----|
| 1  | 1   | 2  | 1   | 3  | 1   |
>Note: The first two columns are from l1, then the next two are from l2, and the last two are from l3.

Then we can select any *Num* column from the above table to get the target data. However, we need to add a keyword `DISTINCT` because it will display a duplicated number if one number appears more than 3 times consecutively.

**MySQL**

```sql
SELECT DISTINCT
    l1.Num AS ConsecutiveNums
FROM
    Logs l1,
    Logs l2,
    Logs l3
WHERE
    l1.Id = l2.Id - 1
    AND l2.Id = l3.Id - 1
    AND l1.Num = l2.Num
    AND l2.Num = l3.Num
;
```

## Accepted Submission (mysql)
```mysql
# Write your MySQL query statement below
select distinct l1.Num as ConsecutiveNums
from Logs l1, Logs l2, Logs l3
where
    l1.Num = L2.Num and
    l2.Num = l3.Num and
    l1.Id = L2.Id + 1 and
    l2.Id = L3.Id + 1
```

## Top Discussions
### Simple solution
- Author: lemonxixi
- Creation Date: Thu Aug 27 2015 05:35:38 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 14:24:29 GMT+0800 (Singapore Standard Time)

<p>
    Select DISTINCT l1.Num from Logs l1, Logs l2, Logs l3 
    where l1.Id=l2.Id-1 and l2.Id=l3.Id-1 
    and l1.Num=l2.Num and l2.Num=l3.Num
</p>


### Runtime: 299 ms, faster than 94.39%
- Author: luokiss39
- Creation Date: Thu Jun 27 2019 15:33:30 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 27 2019 15:33:30 GMT+0800 (Singapore Standard Time)

<p>
Runtime: 299 ms, faster than 94.39% of MySQL online submissions for Consecutive Numbers.
Memory Usage: N/A

```
select distinct Num as ConsecutiveNums
from Logs
where (Id + 1, Num) in (select * from Logs) and (Id + 2, Num) in (select * from Logs)
```
</p>


### Solution with user defined variables
- Author: kent-huang
- Creation Date: Tue Jan 13 2015 13:42:32 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 13 2015 13:42:32 GMT+0800 (Singapore Standard Time)

<p>
    select DISTINCT num FROM
    (select num,
    	case 
    		when @record = num then @count:=@count+1
    		when @record <> @record:=num then @count:=1 end as n
        from 
    	    Logs ,(select @count:=0,@record:=(SELECT num from Logs limit 0,1)) r
    ) a
    where a.n>=3
</p>


