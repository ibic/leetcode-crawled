---
title: "Binary Gap"
weight: 813
#id: "binary-gap"
---
## Description
<div class="description">
<p>Given a positive integer <code>n</code>, find and return <em>the <strong>longest distance</strong> between any two <strong>adjacent</strong> </em><code>1</code><em>&#39;s in the binary representation of </em><code>n</code><em>. If there are no two adjacent </em><code>1</code><em>&#39;s, return </em><code>0</code><em>.</em></p>

<p>Two <code>1</code>&#39;s are <strong>adjacent</strong> if there are only <code>0</code>&#39;s separating them (possibly no <code>0</code>&#39;s). The <b>distance</b> between two <code>1</code>&#39;s is the absolute difference between their bit positions. For example, the two <code>1</code>&#39;s in <code>&quot;1001&quot;</code> have a distance of 3.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 22
<strong>Output:</strong> 2
<strong>Explanation:</strong> 22 in binary is &quot;10110&quot;.
The first adjacent pair of 1&#39;s is &quot;<u>1</u>0<u>1</u>10&quot; with a distance of 2.
The second adjacent pair of 1&#39;s is &quot;10<u>11</u>0&quot; with a distance of 1.
The answer is the largest of these two distances, which is 2.
Note that &quot;<u>1</u>01<u>1</u>0&quot; is not a valid pair since there is a 1 separating the two 1&#39;s underlined.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 5
<strong>Output:</strong> 2
<strong>Explanation:</strong> 5 in binary is &quot;101&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 6
<strong>Output:</strong> 1
<strong>Explanation:</strong> 6 in binary is &quot;110&quot;.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 8
<strong>Output:</strong> 0
<strong>Explanation:</strong> 8 in binary is &quot;1000&quot;.
There aren&#39;t any adjacent pairs of 1&#39;s in the binary representation of 8, so we return 0.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10<sup>9</sup></code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Twitter - 0 (taggedByAdmin: true)
- eBay - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Store Indexes

**Intuition**

Since we wanted to inspect the distance between consecutive 1s in the binary representation of `N`, let's write down the index of each `1` in that binary representation.  For example, if `N = 22 = 0b10110`, then we'll write `A = [1, 2, 4]`.  This makes it easier to proceed, as now we have a problem about adjacent values in an array.

**Algorithm**

Let's make a list `A` of indices `i` such that `N` has the `i`th bit set.

With this array `A`, finding the maximum distance between consecutive `1`s is much easier: it's the maximum distance between adjacent values of this array.

<iframe src="https://leetcode.com/playground/QWUGW4Hq/shared" frameBorder="0" width="100%" height="293" name="QWUGW4Hq"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\log N)$$.  Note that $$\log N$$ is the number of digits in the binary representation of $$N$$.

* Space Complexity:  $$O(\log N)$$, the space used by `A`.
<br />
<br />


---
#### Approach 2: One Pass

**Intuition**

In *Approach 1*, we created an array `A` of indices `i` for which `N` had the `i`th bit set.

Since we only care about consecutive values of this array `A`, we don't need to store the whole array.  We only need to remember the last value seen.

**Algorithm**

We'll store `last`, the last value added to the *virtual* array `A`.  If `N` has the `i`th bit set, a candidate answer is `i - last`, and then the new last value added to `A` would be `last = i`.

<iframe src="https://leetcode.com/playground/CCx5cfNc/shared" frameBorder="0" width="100%" height="276" name="CCx5cfNc"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\log N)$$.  Note that $$\log N$$ is the number of digits in the binary representation of $$N$$.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Dividing by 2
- Author: lee215
- Creation Date: Sun Jul 15 2018 11:05:32 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 21:30:35 GMT+0800 (Singapore Standard Time)

<p>
One pass on `N` in binary from right to left.
`d` means the distance from the last 1 position.
`d` is initial to a small enough value `-32`

**C++:**
```
    int binaryGap(int N) {
        int res = 0;
        for (int d = -32; N; N /= 2, d++)
            if (N % 2) res = max(res, d), d = 0;
        return res;
    }
```

**Java:**
```
    public int binaryGap(int N) {
        int res = 0;
        for (int d = -32; N > 0; N /= 2, d++)
            if (N % 2 == 1) {
                res = Math.max(res, d);
                d = 0;
            }
        return res;
    }
```
**Python:**
```
    def binaryGap(self, N):
        index = [i for i, v in enumerate(bin(N)) if v == \'1\']
        return max([b - a for a, b in zip(index, index[1:])] or [0])
```

</p>


### Simple Java (10 ms)
- Author: asm123
- Creation Date: Sun Jul 15 2018 13:36:02 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 05:56:01 GMT+0800 (Singapore Standard Time)

<p>
```java
class Solution {
    public int binaryGap(int N) {
        int max = 0;
        int pos = 0;
        int lastPos = -1;
        while (N != 0) {
            pos++;
            if ((N & 1) == 1) {
                if (lastPos != -1) {
                    max = Math.max(max, pos-lastPos);
                }
                lastPos = pos;
            }
            N >>= 1;
        }
        return max;
    }
}
```
</p>


