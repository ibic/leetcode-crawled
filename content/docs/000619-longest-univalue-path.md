---
title: "Longest Univalue Path"
weight: 619
#id: "longest-univalue-path"
---
## Description
<div class="description">
<p>Given a binary tree, find the length of the longest path where each node in the path has the same value. This path may or may not pass through the root.</p>

<p>The length of path between two nodes is represented by the number of edges between them.</p>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<p><strong>Input:</strong></p>

<pre>
              5
             / \
            4   5
           / \   \
          1   1   5
</pre>

<p><strong>Output:</strong>&nbsp;2</p>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<p><strong>Input:</strong></p>

<pre>
              1
             / \
            4   5
           / \   \
          4   4   5
</pre>

<p><strong>Output:</strong>&nbsp;2</p>

<p>&nbsp;</p>

<p><b>Note:</b> The given binary tree has not more than 10000 nodes. The height of the tree is not more than 1000.</p>

</div>

## Tags
- Tree (tree)
- Recursion (recursion)

## Companies
- Apple - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Recursion [Accepted]

**Intuition**

We can think of any path (of nodes with the same values) as up to two arrows extending from it's root.

Specifically, the *root* of a path will be the unique node such that the parent of that node does not appear in the path, and an *arrow* will be a path where the root only has one child node in the path.

Then, for each node, we want to know what is the longest possible arrow extending left, and the longest possible arrow extending right?  We can solve this using recursion.

**Algorithm**

Let `arrow_length(node)` be the length of the longest arrow that extends from the `node`.  That will be `1 + arrow_length(node.left)` if `node.left` exists and has the same value as `node`.  Similarly for the `node.right` case.

While we are computing arrow lengths, each candidate answer will be the sum of the arrows in both directions from that node.  We record these candidate answers and return the best one.

<iframe src="https://leetcode.com/playground/DjHbgZUi/shared" frameBorder="0" name="DjHbgZUi" width="100%" height="428"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of nodes in the tree.  We process every node once.

* Space Complexity: $$O(H)$$, where $$H$$ is the height of the tree.  Our recursive call stack could be up to $$H$$ layers deep.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### java solution with global variable
- Author: tyuan73
- Creation Date: Sun Oct 01 2017 11:24:24 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 04:20:57 GMT+0800 (Singapore Standard Time)

<p>
    int len = 0; // global variable
    public int longestUnivaluePath(TreeNode root) {
        if (root == null) return 0;
        len = 0;
        getLen(root, root.val);
        return len;
    }
    
    private int getLen(TreeNode node, int val) {
        if (node == null) return 0;
        int left = getLen(node.left, node.val);
        int right = getLen(node.right, node.val);
        len = Math.max(len, left + right);
        if (val == node.val)  return Math.max(left, right) + 1;
        return 0;
    }
</p>


### [Java/C++] Clean Code
- Author: alexander
- Creation Date: Sun Oct 01 2017 11:00:58 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 22:17:00 GMT+0800 (Singapore Standard Time)

<p>
1. `Longest-Univalue-Path` of a tree is among those `Longest-Univalue-Path-Across` at each node;
2. `Longest-Univalue-Path-Across` a node is sum of { `Longest-Univalue-Path-Start-At` each child with same value } + 1
3. Let an `dfs` to return `Longest-Univalue-Path-Start-At` each node, the `Longest-Univalue-Path-Across` `node` can be calculated by combine the `Longest-Univalue-Path-Start-At` of its 2 child; and we can use an global variable `res` to hold the max value and compare with each intermediate result.
 
**Java**
```
class Solution {
    public int longestUnivaluePath(TreeNode root) {
        int[] res = new int[1];
        if (root != null) dfs(root, res);
        return res[0];
    }

    private int dfs(TreeNode node, int[] res) {
        int l = node.left != null ? dfs(node.left, res) : 0; // Longest-Univalue-Path-Start-At - left child
        int r = node.right != null ? dfs(node.right, res) : 0; // Longest-Univalue-Path-Start-At - right child
        int resl = node.left != null && node.left.val == node.val ? l + 1 : 0; // Longest-Univalue-Path-Start-At - node, and go left
        int resr = node.right != null && node.right.val == node.val ? r + 1 : 0; // Longest-Univalue-Path-Start-At - node, and go right
        res[0] = Math.max(res[0], resl + resr); // Longest-Univalue-Path-Across - node
        return Math.max(resl, resr);
    }
}
```
**C++**
```
class Solution {
public:
    int longestUnivaluePath(TreeNode* root) {
        int lup = 0;
        if (root) dfs(root, lup);
        return lup;
    }

private:
    int dfs(TreeNode* node, int& lup) {
        int l = node->left ? dfs(node->left, lup) : 0;
        int r = node->right ? dfs(node->right, lup) : 0;
        int resl = node->left && node->left->val == node->val ? l + 1 : 0;
        int resr = node->right && node->right->val == node->val ? r + 1 : 0;
        lup = max(lup, resl + resr);
        return max(resl, resr);
    }
};
```
**Varables**
`l` is the length of single direction `Longest-Univalue-Path` start from `left-child`, 
`r` is the length of single direction `Longest-Univalue-Path` start from `right-child`, 
`resl` is the length of single direction `Longest-Univalue-Path` start from `parent` go left, 
`resr` is the length of single direction `Longest-Univalue-Path` start from `parent` go right.
`int dfs(node)` returns the `Longest-Univalue-Path-Start-At` that `node`, and update the result of `Longest-Univalue-Path-Across` that `node` through side effect.
It is really hard to name those variables to reflect these concept.

**Example:**
```
                ...
               /   
              4 (res = resl + resr = 3)
  (resl = 2) / \ (resr= 1)
    (l = 1) 4   4 (r = 0)
           /     
          4
```
resl is `Longest-Univalue-Path-Start-At` left node + 1,
resr is `Longest-Univalue-Path-Start-At` right node + 1,
in here the local result of `Longest-Univalue-Path-Across` at this node is the sum of the 2;
</p>


### Python Simple to Understand
- Author: yangshun
- Creation Date: Sun Oct 01 2017 12:48:17 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 10 2018 06:30:34 GMT+0800 (Singapore Standard Time)

<p>
The approach is similar to the [Diameter of Binary Tree](https://leetcode.com/problems/diameter-of-binary-tree/) question except that we reset the left/right to 0 whenever the current node does not match the children node value.

In the Diameter of Binary Tree question, the path can either go through the root or it doesn't.

![Imgur](https://i.imgur.com/N0zniqa.png)

Hence at the end of each recursive loop, return the longest length using that node as the root so that the node's parent can potentially use it in its longest path computation.

We also use an external variable `longest` that keeps track of the longest path seen so far.

*- Yangshun*

```
class Solution(object):
    def longestUnivaluePath(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        # Time: O(n)
        # Space: O(n)
        longest = [0]
        def traverse(node):
            if not node:
                return 0
            left_len, right_len = traverse(node.left), traverse(node.right)
            left = (left_len + 1) if node.left and node.left.val == node.val else 0
            right = (right_len + 1) if node.right and node.right.val == node.val else 0
            longest[0] = max(longest[0], left + right)
            return max(left, right)
        traverse(root)
        return longest[0]
```
</p>


