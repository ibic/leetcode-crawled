---
title: "Distinct Echo Substrings"
weight: 1119
#id: "distinct-echo-substrings"
---
## Description
<div class="description">
<p>Return the number of <strong>distinct</strong> non-empty substrings of <code>text</code>&nbsp;that can be written as the concatenation of some string with itself (i.e. it can be written as <code>a + a</code>&nbsp;where <code>a</code> is some string).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;abcabcabc&quot;
<strong>Output:</strong> 3
<b>Explanation: </b>The 3 substrings are &quot;abcabc&quot;, &quot;bcabca&quot; and &quot;cabcab&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;leetcodeleetcode&quot;
<strong>Output:</strong> 2
<b>Explanation: </b>The 2 substrings are &quot;ee&quot; and &quot;leetcodeleetcode&quot;.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= text.length &lt;= 2000</code></li>
	<li><code>text</code>&nbsp;has only lowercase English letters.</li>
</ul>

</div>

## Tags
- String (string)
- Rolling Hash (rolling-hash)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Am I the only one who didn't understand the description?
- Author: infmount
- Creation Date: Sun Jan 12 2020 00:12:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 12 2020 00:12:11 GMT+0800 (Singapore Standard Time)

<p>
Spent 30 minutes trying to decipher the 2 examples, still don\'t understand. In the first example, why is "abc" not a echo substring? The explanation in the description didn\'t really explain anything like why "abcabc" is an echo substring of "abcabcabc".
</p>


### [Java] Brute force & Hash Solution - Clean code
- Author: hiepit
- Creation Date: Sun Jan 12 2020 02:04:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 12 2020 14:03:27 GMT+0800 (Singapore Standard Time)

<p>
**1. Brute force Solution**
```java
class Solution {
    public int distinctEchoSubstrings(String str) {
        HashSet<String> set = new HashSet<>();
        int n = str.length();
        for (int i = 0; i < n; i++) {
            for (int len = 2; i + len <= n; len += 2) {
                int mid = i + len / 2;
                String subStr1 = str.substring(i, mid);
                String subStr2 = str.substring(mid, i + len);
                if (subStr1.equals(subStr2)) set.add(subStr1);
            }
        }
        return set.size();
    }
}
```
**Complexity:**
- Time: O(n^3), `n` is length of string `str`
- Space: O(n)

**2. Hash Solution**
```java
class Solution {
    long BASE = 29L, MOD = 1000000007L;
    public int distinctEchoSubstrings(String str) {
        HashSet<Long> set = new HashSet<>();
        int n = str.length();
        long[] hash = new long[n + 1]; // hash[i] is hash value from str[0..i]
        long[] pow = new long[n + 1]; // pow[i] = BASE^i
        pow[0] = 1;
        for (int i = 1; i <= n; i++) {
            hash[i] = (hash[i - 1] * BASE + str.charAt(i - 1)) % MOD;
            pow[i] = pow[i - 1] * BASE % MOD;
        }
        for (int i = 0; i < n; i++) {
            for (int len = 2; i + len <= n; len += 2) {
                int mid = i + len / 2;
                long hash1 = getHash(i, mid, hash, pow);
                long hash2 = getHash(mid, i + len, hash, pow);
                if (hash1 == hash2) set.add(hash1);
            }
        }
        return set.size();
    }

    long getHash(int l, int r, long[] hash, long[] pow) {
        return (hash[r] - hash[l] * pow[r - l] % MOD + MOD) % MOD;
    }
}
```
**Complexity**
- Time: O(n^2), `n` is length of string `str`
- Space: O(n)
</p>


### [Intuitive][100%] Sliding Window, Rolling Counter (with pictures)
- Author: oa5cuxe0To3
- Creation Date: Wed Jan 29 2020 03:15:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 30 2020 17:31:59 GMT+0800 (Singapore Standard Time)

<p>
**Side note**
You can find here 2 solutions, 1st - Rolling Counter (60%), 2nd - Rolling Counter + Rolling Hash (100%). If you think of any improvements, please let me know in the comments.

**Solution I. Rolling Counter.**
* ***Idea***
We will use 2 pointers \'l\' and \'r\' to count equal characters. 
Let\'s consider these 3 cases:
	* If counter reaches \'len\' (window length) then we record a substring. 
<img src="https://assets.leetcode.com/users/jkyncmeg2h86/image_1580237915.png" width="350" height="150" />
	* We disregard current substring when we encounter chars that are not equal (\'b\' and \'c\'), thus we reset the counter.
<img src="https://assets.leetcode.com/users/jkyncmeg2h86/image_1580238662.png" width="350" height="210" />
	* When we record a substring, we move windows thus leaving a character behind, this means we have to decrease the counter.
<img src="https://assets.leetcode.com/users/jkyncmeg2h86/image_1580237916.png" width="350" height="150" />


* ***Implementation***
	```java
	public int distinctEchoSubstrings(String text) {
		Set<String> set = new HashSet<>();
		int n = text.length();

		for (int len = 1; len <= n / 2; len++) {
			for (int l = 0, r = len, counter = 0; l < n - len; l++, r++) {
				if (text.charAt(l) == text.charAt(r)) {
					counter++;
				} else {
					counter = 0;
				}

				if (counter == len) {
					set.add(text.substring(l - len + 1, l + 1));
					counter--;
				}
			}
		}

		return set.size();
	}
	```

**Solution II. Rolling Hash.**
* ***Idea***
Extended previous solution with rolling hash.

* ***Implementation***
	```java
	import java.math.*;
	public class Solution {
		private int R = 256; // radix
		private long Q; // large prime
		private long[] powers; // precomputed powers of R
		private long[] hashes; // precomputed hashes of prefixes

		public int distinctEchoSubstrings(String text) {
			Set<Long> set = new HashSet<>();
			int n = text.length();
			preprocess(n, text);

			for (int len = 1; len <= n / 2; len++) {
				for (int l = 0, r = len, counter = 0; l < n - len; l++, r++) {
					if (text.charAt(l) == text.charAt(r)) {
						counter++;
					} else {
						counter = 0;
					}

					if (counter == len) {
						set.add(getHash(l - len + 1, l + 1));
						counter--;
					}
				}
			}

			return set.size();
		}

		private void preprocess(int n, String text) {
			Q = calculateRandomPrime();
			hashes = new long[n + 1];
			powers = new long[n + 1];
			powers[0] = 1;
			for (int i = 1; i <= n; i++) {
				hashes[i] = (hashes[i - 1] * R + text.charAt(i - 1)) % Q;
				powers[i] = (powers[i - 1] * R) % Q;
			}
		}

		private long calculateRandomPrime() {
			BigInteger prime = BigInteger.probablePrime(31, new Random());
			return prime.longValue();
		}

		private long getHash(int l, int r) {
			return (hashes[r] + Q - hashes[l] * powers[r - l] % Q) % Q;
		}
	}
	```
	
	
**Similar problems:**
* [Longest Duplicate Substring](https://leetcode.com/problems/longest-duplicate-substring/)
* [Repeated String Match](https://leetcode.com/problems/repeated-string-match/)
* [Permutation in String](https://leetcode.com/problems/permutation-in-string/)
* [Shortest Palindrome](https://leetcode.com/problems/shortest-palindrome/)
</p>


