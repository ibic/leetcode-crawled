---
title: "Unique Number of Occurrences"
weight: 1166
#id: "unique-number-of-occurrences"
---
## Description
<div class="description">
<p>Given an array of integers <code>arr</code>,&nbsp;write a function that returns <code>true</code> if and only if the number of occurrences of each value in the array is unique.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,2,1,1,3]
<strong>Output:</strong> true
<strong>Explanation:</strong>&nbsp;The value 1 has 3 occurrences, 2 has 2 and 3 has 1. No two values have the same number of occurrences.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2]
<strong>Output:</strong> false
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [-3,0,1,-3,1,1,1,-3,10,0]
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length&nbsp;&lt;= 1000</code></li>
	<li><code>-1000 &lt;= arr[i] &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] 4 liner and 2 liner Using Map and Set w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Sep 29 2019 12:03:49 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 30 2019 01:36:35 GMT+0800 (Singapore Standard Time)

<p>
1. Count the occurrences of each char;
2. Compare if the numbers of distinct chars and distinct counts are equal.
```
    public boolean uniqueOccurrences(int[] arr) {
        Map<Integer, Integer> count = new HashMap<>();
        for (int a : arr)
            count.put(a, 1 + count.getOrDefault(a, 0));
        return count.size() == new HashSet<>(count.values()).size();
    }
```
```
    def uniqueOccurrences(self, arr: List[int]) -> bool:
        c = collections.Counter(arr)
        return len(c) == len(set(c.values())) 
```
**Analysis:**

Time & space: O(n), where n = arr.length
</p>


### C++ 2 approaches
- Author: votrubac
- Creation Date: Sun Sep 29 2019 12:02:17 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 04 2019 12:57:51 GMT+0800 (Singapore Standard Time)

<p>
# Approach 1: Map and set
Count occurences of each number using a hash map. Insert all occurences into a hash set. If, after the insert, the sizes of hash map and set are equal, then all occurences are unique.
```
bool uniqueOccurrences(vector<int>& arr) {
  unordered_map<int, int> m;
  unordered_set<int> s;
  for (auto n : arr) ++m[n];
  for (auto& p : m) s.insert(p.second);
  return m.size() == s.size();
}
```
We can improve this for the average case by checking the result of ```s.insert()```, which returns false if an element is already there.
```
bool uniqueOccurrences(vector<int>& arr) {
  unordered_map<int, int> m;
  unordered_set<int> s;
  for (auto n : arr) ++m[n];
  for (auto& p : m)
      if (!s.insert(p.second).second) return false;
  return true;
}
```
## Complexity Analysis
Time: O(n), where n is the size of input array.
Memory: O(m), where m is the number of unique elements (we store counts in hash map and set).
# Approach 2: Counting sort
Since our values are limited to ```[-1000, 1000]```, we can use an array instead of hash set to count occurrences. Then, we can sort our array and check that no adjacent numbers are the same.
```
bool uniqueOccurrences(vector<int>& arr) {
  short m[2001] = {};
  for (auto n : arr) ++m[n + 1000];
  sort(begin(m), end(m));
  for (auto i = 1; i < 2001; ++i)
      if (m[i] && m[i] == m[i - 1]) return false;
  return true;
}
```
We can also note that the array length is limited to 1000, so no element will repeat more than 1000 times. Therefore we can use another array to do the counting sort over the occurrences.
```
bool uniqueOccurrences(vector<int>& arr) {
  short m[2001] = {}, s[1001] = {};
  for (auto n : arr) ++m[n + 1000];
  for (auto i = 0; i < 2001; ++i)
      if (m[i] && ++s[m[i]] > 1) return false;
  return true;
}
```
## Complexity Analysis
Time: 
- First solution: O(n + m log m).
- Second solution: O(n).

Memory: O(m).
</p>


### Java HashMap & Set
- Author: varunu28
- Creation Date: Mon Sep 30 2019 04:33:49 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 30 2019 04:34:15 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public boolean uniqueOccurrences(int[] arr) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : arr) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        Set<Integer> set = new HashSet<>(map.values());
        return map.size() == set.size();
    }
}
```
</p>


