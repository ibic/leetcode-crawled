---
title: "Shortest Path in a Grid with Obstacles Elimination"
weight: 1220
#id: "shortest-path-in-a-grid-with-obstacles-elimination"
---
## Description
<div class="description">
<p>Given a <code>m * n</code> grid, where each cell is either <code>0</code> (empty)&nbsp;or <code>1</code> (obstacle).&nbsp;In one step, you can move up, down, left or right from and to an empty cell.</p>

<p>Return the minimum number of steps to walk from the upper left corner&nbsp;<code>(0, 0)</code>&nbsp;to the lower right corner&nbsp;<code>(m-1, n-1)</code> given that you can eliminate&nbsp;<strong>at most</strong> <code>k</code> obstacles. If it is not possible to find such&nbsp;walk return -1.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 
grid = 
[[0,0,0],
&nbsp;[1,1,0],
 [0,0,0],
&nbsp;[0,1,1],
 [0,0,0]], 
k = 1
<strong>Output:</strong> 6
<strong>Explanation: 
</strong>The shortest path without eliminating any obstacle is 10.&nbsp;
The shortest path with one obstacle elimination at position (3,2) is 6. Such path is <code>(0,0) -&gt; (0,1) -&gt; (0,2) -&gt; (1,2) -&gt; (2,2) -&gt; <strong>(3,2)</strong> -&gt; (4,2)</code>.
</pre>

<p>&nbsp;</p>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> 
grid = 
[[0,1,1],
&nbsp;[1,1,1],
&nbsp;[1,0,0]], 
k = 1
<strong>Output:</strong> -1
<strong>Explanation: 
</strong>We need to eliminate at least two obstacles to find such a walk.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>grid.length&nbsp;== m</code></li>
	<li><code>grid[0].length&nbsp;== n</code></li>
	<li><code>1 &lt;= m, n &lt;= 40</code></li>
	<li><code>1 &lt;= k &lt;= m*n</code></li>
	<li><code>grid[i][j] == 0 <strong>or</strong> 1</code></li>
	<li><code>grid[0][0] == grid[m-1][n-1] == 0</code></li>
</ul>

</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies
- Google - 7 (taggedByAdmin: true)
- Amazon - 5 (taggedByAdmin: false)
- DiDi - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] O(m*n*k) BFS Solution with Explanation
- Author: laser
- Creation Date: Sun Dec 15 2019 12:01:48 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 16 2019 08:47:11 GMT+0800 (Singapore Standard Time)

<p>
**Solution Explanation**
- Because we are trying to find the shortest path, use BFS here to exit immediately when a path reaches the bottom right most cell.
- Use a set to keep track of already visited paths. We only need to keep track of the row, column, and the eliminate obstacle usage count. We don\'t need to keep track of the steps because remember we are using BFS for the shortest path. That means there is no value storing a 4th piece of the data, the current steps. This will reduce the amount of repeat work.

m = rows
n = columns
k = allowed elimination usages

**Time Complexity**
O(m\*n\*k) time complexity
This is because for every cell (m*n), in the worst case we have to put that cell into the queue/bfs k times.

Runtime: 68 ms, faster than 33.33% of Python3 online submissions

**Space Complexity**
O(m\*n\*k) space complexity
This is because for every cell (m*n), in the worst case we have to put that cell into the queue/bfs k times which means we need to worst case store all of those steps/paths in the visited set.

Memory Usage: 13.9 MB, less than 100.00% of Python3 online submissions

**Code**
```
from collections import deque
class Solution:
    def shortestPath(self, grid: List[List[int]], k: int) -> int:
        if len(grid) == 1 and len(grid[0]) == 1:
            return 0

        queue = deque([(0,0,k,0)])
        visited = set([(0,0,k)])

        if k > (len(grid)-1 + len(grid[0])-1):
            return len(grid)-1 + len(grid[0])-1

        while queue:
            row, col, eliminate, steps = queue.popleft()
            for new_row, new_col in [(row-1,col), (row,col+1), (row+1, col), (row, col-1)]:
                if (new_row >= 0 and
                    new_row < len(grid) and
                    new_col >= 0 and
                    new_col < len(grid[0])):
                    if grid[new_row][new_col] == 1 and eliminate > 0 and (new_row, new_col, eliminate-1) not in visited:
                        visited.add((new_row, new_col, eliminate-1))
                        queue.append((new_row, new_col, eliminate-1, steps+1))
                    if grid[new_row][new_col] == 0 and (new_row, new_col, eliminate) not in visited:
                        if new_row == len(grid)-1 and new_col == len(grid[0])-1:
                            return steps+1
                        visited.add((new_row, new_col, eliminate))
                        queue.append((new_row, new_col, eliminate, steps+1))

        return -1
```
</p>


### Manhattan distance instead of normal goal check
- Author: StefanPochmann
- Creation Date: Tue Dec 17 2019 10:09:23 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Dec 18 2019 22:47:08 GMT+0800 (Singapore Standard Time)

<p>
I\'ve seen some solutions doing this optimization at the start before the search:
```
if k >= m + n - 2:
    return m + n - 2
```
Because such a large `k` means we can just go straight down and right to the goal.

Since `grid[m-1][n-1] == 0` is guaranteed, we can actually do this:
```
if k >= m + n - 3:
    return m + n - 2
```
Also, most people use this to stop the search:
```
if i == m-1 and j == n-1:
    return stepsUntilHere
```
You can **replace** that by generalizing the above Manhattan distance trick, so you\'re applying that trick the whole time during the search instead of just once before the search:
```
if remainingK >= m + n - 3 - i - j:
    return stepsUntilHere + m + n - 2 - i - j
```
(I actually first saw the original trick [&rarr;suggested by @dylan20](https://leetcode.com/problems/shortest-path-in-a-grid-with-obstacles-elimination/discuss/451787/Python-O(m*n*k)-BFS-Solution-with-Explanation/406539) when asked for a complexity improvement, maybe that\'s why it was only used *before* the search, as in that context the use *during* the search probably doesn\'t help or at least it\'s not obvious.)

Granted, it looks a bit unwieldy. So I\'m instead walking from `(m-1, n-1)` to `(0, 0)` so it becomes this:
```
if remainingK >= i + j - 1:
    return stepsUntilHere + i + j
```
My below otherwise naive BFS solution using this got accepted in 32 ms, "faster than 99.65%". And it actually only takes about 0.5 ms, the rest is LeetCode\'s overhead baseline (see [&rarr;my benchmarking](https://leetcode.com/problems/shortest-path-in-a-grid-with-obstacles-elimination/discuss/453652/Manhattan-distance-instead-of-normal-goal-check/408238) in the comments).
```
def shortestPath(self, grid: List[List[int]], k: int) -> int:
    m, n = len(grid), len(grid[0])
    start = m-1, n-1, k
    queue = [(0, start)]
    seen = {start}
    for steps, (i, j, k) in queue:
        if k >= i + j - 1:
            return steps + i + j
        for i, j in (i+1, j), (i-1, j), (i, j+1), (i, j-1):
            if m > i >= 0 <= j < n:
                state = i, j, k - grid[i][j]
                if state not in seen and state[2] >= 0:
                    queue.append((steps + 1, state))
                    seen.add(state)
    return -1
```
![image](https://assets.leetcode.com/users/stefanpochmann/image_1576555322.png)

</p>


### [Java] Straightforward BFS O(MNK) time | O(MNK) space
- Author: yuhwu
- Creation Date: Sun Dec 15 2019 12:02:38 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 17 2019 16:39:52 GMT+0800 (Singapore Standard Time)

<p>
Thanks for all the comments! The solution is improved to use boolean mratix to keep track of the visited node, inspired by @tlj77. 
```
class Solution {
    int[][] dirs = new int[][]{{0,1},{0,-1},{1,0},{-1,0}};
    public int shortestPath(int[][] grid, int k) {
        int n = grid.length;
        int m = grid[0].length;
        Queue<int[]> q = new LinkedList();
        boolean[][][] visited = new boolean[n][m][k+1];
        visited[0][0][0] = true;
        q.offer(new int[]{0,0,0});
        int res = 0;
        while(!q.isEmpty()){
            int size = q.size();
            for(int i=0; i<size; i++){
                int[] info = q.poll();
                int r = info[0], c = info[1], curK = info[2];
                if(r==n-1 && c==m-1){
                    return res;
                }
                for(int[] dir : dirs){
                    int nextR = dir[0] + r;
                    int nextC = dir[1] + c;
                    int nextK = curK;
                    if(nextR>=0 && nextR<n && nextC>=0 && nextC<m){
                        if(grid[nextR][nextC]==1){
                            nextK++;
                        }
                        if(nextK<=k && !visited[nextR][nextC][nextK]){
                            visited[nextR][nextC][nextK] = true;
                            q.offer(new int[]{nextR, nextC, nextK});
                        }
                    }
                }                
            }
            res++;
        }
        return -1;
    }
}
```
</p>


