---
title: "Reverse Words in a String"
weight: 151
#id: "reverse-words-in-a-string"
---
## Description
<div class="description">
<p>Given an input string <code>s</code>, reverse the order of the <strong>words</strong>.</p>

<p>A <strong>word</strong> is defined as a sequence of non-space characters. The <strong>words</strong> in <code>s</code> will be separated by at least one space.</p>

<p>Return <em>a string of the words in reverse order concatenated by a single space.</em></p>

<p><b>Note</b> that <code>s</code> may contain leading or trailing spaces or multiple spaces between two words. The returned string should only have a single space separating the words. Do not include any extra spaces.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;the sky is blue&quot;
<strong>Output:</strong> &quot;blue is sky the&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;  hello world  &quot;
<strong>Output:</strong> &quot;world hello&quot;
<strong>Explanation:</strong> Your reversed string should not contain leading or trailing spaces.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;a good   example&quot;
<strong>Output:</strong> &quot;example good a&quot;
<strong>Explanation:</strong> You need to reduce multiple spaces between two words to a single space in the reversed string.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;  Bob    Loves  Alice   &quot;
<strong>Output:</strong> &quot;Alice Loves Bob&quot;
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;Alice does not even like bob&quot;
<strong>Output:</strong> &quot;bob like even not does Alice&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10<sup>4</sup></code></li>
	<li><code>s</code> contains English letters (upper-case and lower-case), digits, and spaces <code>&#39; &#39;</code>.</li>
	<li>There is <strong>at least one</strong> word in <code>s</code>.</li>
</ul>

<p>&nbsp;</p>

<p><strong>Follow up:</strong></p>

<ul>
	<li>Could you solve it <strong>in-place</strong> with <code>O(1)</code> extra space?</li>
</ul>

<p>&nbsp;</p>

</div>

## Tags
- String (string)

## Companies
- Microsoft - 8 (taggedByAdmin: true)
- Facebook - 4 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: true)
- Qualcomm - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Bloomberg - 4 (taggedByAdmin: true)
- Salesforce - 3 (taggedByAdmin: false)
- Nvidia - 3 (taggedByAdmin: false)
- Cisco - 3 (taggedByAdmin: false)
- Zillow - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Snapchat - 0 (taggedByAdmin: true)
- Yelp - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

Different interviewers would probably expect different approaches for 
this problem. The holy war question is to use or not use built-in methods.
As you may notice, most of design problems on Leetcode are voted down 
because of two main reasons:

1. There was no approach with built-in methods / data structures in the article.

2. One of the approaches in the article did contain built-in methods / data structures.

Seems like the community has no common opinion yet, and 
in practice that means an unpredictable interview experience for some
sort of problems. 
 
Here we consider three different solutions 
of linear time and space complexity:

1. Use built-in split and reverse.
Benefits: in-place in Python (in-place, but linear space complexity!) 
and the simplest one to write.  

2. The most straightforward one. Trim the whitespaces, reverse the
whole string and then reverse each word.  
Benefits: could be done in-place for the languages with mutable strings.

3. Two passes approach with a deque. 
Move along the string, word by word, and 
push each new word in front of the deque. 
Convert the deque back into string.
Benefits: two passes.

#### Approach 1: Built-in Split + Reverse

![fig](../Figures/151/fun2.png)

**Implementation**

<iframe src="https://leetcode.com/playground/f4BKPtsx/shared" frameBorder="0" width="100%" height="225" name="f4BKPtsx"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, where N is a number of characters
in the input string.

* Space complexity: $$\mathcal{O}(N)$$, to store the result of split
by spaces.

<br /> 
<br />


---
#### Approach 2: Reverse the Whole String and Then Reverse Each Word

The implementation of this approach will be different for 
Java/Python (= immutable strings) and C++ (= mutable strings). 

In the case of immutable strings one has first to convert string into
mutable data structure, and hence it makes sense to trim all spaces during
that conversion. 

![fig](../Figures/151/reverse_whole2.png)

In the case of _mutable_ strings, there is no need to allocate 
an additional data structure, one could make all job done in-place.
In such a case it makes sense to reverse words and trim spaces at the 
same time.

![fig](../Figures/151/mutable2.png)

**Implementation**

<iframe src="https://leetcode.com/playground/628bbiFc/shared" frameBorder="0" width="100%" height="500" name="628bbiFc"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$.

* Space complexity: $$\mathcal{O}(N)$$.
<br /> 
<br />


---
#### Approach 3: Deque of Words

![fig](../Figures/151/deque2.png)

**Implementation**

<iframe src="https://leetcode.com/playground/fnSejDt6/shared" frameBorder="0" width="100%" height="500" name="fnSejDt6"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$.

* Space complexity: $$\mathcal{O}(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Clean Java two-pointers solution (no trim( ), no split( ), no StringBuilder)
- Author: jeantimex
- Creation Date: Thu Jul 09 2015 09:25:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 14:00:22 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
      
      public String reverseWords(String s) {
        if (s == null) return null;
        
        char[] a = s.toCharArray();
        int n = a.length;
        
        // step 1. reverse the whole string
        reverse(a, 0, n - 1);
        // step 2. reverse each word
        reverseWords(a, n);
        // step 3. clean up spaces
        return cleanSpaces(a, n);
      }
      
      void reverseWords(char[] a, int n) {
        int i = 0, j = 0;
          
        while (i < n) {
          while (i < j || i < n && a[i] == ' ') i++; // skip spaces
          while (j < i || j < n && a[j] != ' ') j++; // skip non spaces
          reverse(a, i, j - 1);                      // reverse the word
        }
      }
      
      // trim leading, trailing and multiple spaces
      String cleanSpaces(char[] a, int n) {
        int i = 0, j = 0;
          
        while (j < n) {
          while (j < n && a[j] == ' ') j++;             // skip spaces
          while (j < n && a[j] != ' ') a[i++] = a[j++]; // keep non spaces
          while (j < n && a[j] == ' ') j++;             // skip spaces
          if (j < n) a[i++] = ' ';                      // keep only one space
        }
      
        return new String(a).substring(0, i);
      }
      
      // reverse a[] from a[i] to a[j]
      private void reverse(char[] a, int i, int j) {
        while (i < j) {
          char t = a[i];
          a[i++] = a[j];
          a[j--] = t;
        }
      }
      
    }
</p>


### In place simple solution
- Author: yuruofeifei
- Creation Date: Sat Sep 13 2014 10:21:45 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 14:17:01 GMT+0800 (Singapore Standard Time)

<p>
First, reverse the whole string, then reverse each word.

    void reverseWords(string &s) {
        reverse(s.begin(), s.end());
        int storeIndex = 0;
        for (int i = 0; i < s.size(); i++) {
            if (s[i] != ' ') {
                if (storeIndex != 0) s[storeIndex++] = ' ';
                int j = i;
                while (j < s.size() && s[j] != ' ') { s[storeIndex++] = s[j++]; }
                reverse(s.begin() + storeIndex - (j - i), s.begin() + storeIndex);
                i = j;
            }
        }
        s.erase(s.begin() + storeIndex, s.end());
    }
</p>


### Java 3-line builtin solution
- Author: tusizi
- Creation Date: Sat Apr 11 2015 20:01:38 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 20:19:44 GMT+0800 (Singapore Standard Time)

<p>
    public String reverseWords(String s) {
        String[] words = s.trim().split(" +");
        Collections.reverse(Arrays.asList(words));
        return String.join(" ", words);
    }
</p>


