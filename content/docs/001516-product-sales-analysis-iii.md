---
title: "Product Sales Analysis III"
weight: 1516
#id: "product-sales-analysis-iii"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Sales</code></p>

<pre>
+-------------+-------+
| Column Name | Type  |
+-------------+-------+
| sale_id     | int   |
| product_id  | int   |
| year        | int   |
| quantity    | int   |
| price       | int   |
+-------------+-------+
sale_id is the primary key of this table.
product_id is a foreign key to <code>Product</code> table.
Note that the price is per unit.
</pre>

<p>Table:&nbsp;<code>Product</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| product_id   | int     |
| product_name | varchar |
+--------------+---------+
product_id is the primary key of this table.
</pre>

<p>&nbsp;</p>

<p style="direction: ltr;">Write an SQL query that selects the <strong>product id</strong>, <strong>year</strong>, <strong>quantity</strong>, and <strong>price</strong> for the <strong>first year</strong> of every product sold.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Sales</code> table:
+---------+------------+------+----------+-------+
| sale_id | product_id | year | quantity | price |
+---------+------------+------+----------+-------+ 
| 1       | 100        | 2008 | 10       | 5000  |
| 2       | 100        | 2009 | 12       | 5000  |
| 7       | 200        | 2011 | 15       | 9000  |
+---------+------------+------+----------+-------+

Product table:
+------------+--------------+
| product_id | product_name |
+------------+--------------+
| 100        | Nokia        |
| 200        | Apple        |
| 300        | Samsung      |
+------------+--------------+

Result table:
+------------+------------+----------+-------+
| product_id | first_year | quantity | price |
+------------+------------+----------+-------+ 
| 100        | 2008       | 10       | 5000  |
| 200        | 2011       | 15       | 9000  |
+------------+------------+----------+-------+
</pre>

</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL Solution
- Author: yizi_winnie
- Creation Date: Thu Jun 06 2019 10:13:56 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 06 2019 10:13:56 GMT+0800 (Singapore Standard Time)

<p>
SELECT product_id, year AS first_year, quantity, price
FROM Sales
WHERE (product_id, year) IN (
SELECT product_id, MIN(year) as year
FROM Sales
GROUP BY product_id) ;
</p>


### Is the expected solution correct?
- Author: hstangnatsh
- Creation Date: Sun Jun 02 2019 22:03:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 22:09:21 GMT+0800 (Singapore Standard Time)

<p>
Shortened the second test case for displayability. Below is the table screenshot. 
Expected output of OJ is actually the **minimum** year of a product_id, with quantity and price of the **first** row. That\'s obviously an outcome from query similar with 
```
select product_id, min(year), quantity, price 
from Sales 
group by product_id 
``` 
in which those ungrouped fields with mutiple values only shows the first row.
It hardly has anything to do with the discreption "product id, year, quantity, and price **for the first year** of every product sold."
![image](https://assets.leetcode.com/users/hstangnatsh/image_1559483193.png)
![image](https://assets.leetcode.com/users/hstangnatsh/image_1559483221.png)
My failed code was:
```
select Sales.product_id as product_id, Sales.year as first_year, quantity, price
from Sales, (select product_id as id, min(year) as my
from Sales
group by product_id) t
where Sales.product_id=t.id and Sales.year=t.my
order by product_id
```
</p>


### Help! Why is my solution wrong? Did I misunderstand the problem?
- Author: Swallow_Liu
- Creation Date: Sat Aug 31 2019 07:46:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 31 2019 07:46:05 GMT+0800 (Singapore Standard Time)

<p>
From my understanding, this question is the same as this [game analysis 1](https://leetcode.com/problems/game-play-analysis-i/)
so I wrote similar code as below
```
# select product_id, min(year), quantity, price  from Sales  
# group by product_id
```

which does not pass, I saw people wrote below that passed 

```
SELECT product_id, year as first_year, quantity, price
from Sales 
WHERE (product_id, year) IN (SELECT product_id, min(year) as year
FROM Sales 
group by product_id)

```

Am I misunderstanding the problem?

</p>


