---
title: "Fraction Addition and Subtraction"
weight: 544
#id: "fraction-addition-and-subtraction"
---
## Description
<div class="description">
<p>Given a string representing an expression of fraction addition and subtraction, you need to return the calculation result in string format. The final result should be <a href = "https://en.wikipedia.org/wiki/Irreducible_fraction">irreducible fraction</a>. If your final result is an integer, say <code>2</code>, you need to change it to the format of fraction that has denominator <code>1</code>. So in this case, <code>2</code> should be converted to <code>2/1</code>.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b>"-1/2+1/2"
<b>Output:</b> "0/1"
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b>"-1/2+1/2+1/3"
<b>Output:</b> "1/3"
</pre>
</p>

<p><b>Example 3:</b><br />
<pre>
<b>Input:</b>"1/3-1/2"
<b>Output:</b> "-1/6"
</pre>
</p>

<p><b>Example 4:</b><br />
<pre>
<b>Input:</b>"5/3+1/3"
<b>Output:</b> "2/1"
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The input string only contains <code>'0'</code> to <code>'9'</code>, <code>'/'</code>, <code>'+'</code> and <code>'-'</code>. So does the output.</li>
<li>Each fraction (input and output) has format <code>±numerator/denominator</code>. If the first input fraction or the output is positive, then <code>'+'</code> will be omitted.</li>
<li>The input only contains valid <b>irreducible fractions</b>, where the <b>numerator</b> and <b>denominator</b> of each fraction will always be in the range [1,10]. If the denominator is 1, it means this fraction is actually an integer in a fraction format defined above.</li> 
<li>The number of given fractions will be in the range [1,10].</li>
<li>The numerator and denominator of the <b>final result</b> are guaranteed to be valid and in the range of 32-bit int.</li>
</ol>
</p>
</div>

## Tags
- Math (math)

## Companies
- IXL - 6 (taggedByAdmin: true)
- Goldman Sachs - 3 (taggedByAdmin: false)

## Official Solution
[TOC]


## Solution

---
#### Approach 1: Using LCM

The first obvious step to be undertaken is to split the given string into individual fractions. We split the string based on `+` and `-` sign. We store the signs in the order in which they appear in the string in $$sign$$ array. Further, after getting the individual fractions, we further split the fractions based on `/` sign. Thus, we obtain the individual numerator and denominator parts. We store the same in $$num$$ and $$den$$ arrays respectively.

Now, we've got the data ready to be worked upon. In order to see the method we've used in this implementation, we'll take an example and understand the way we work on it.

Let's say, the given fraction is:

$$
\frac{3}{2} + \frac{5}{3} -\frac{7}{6}
$$

We need to equalize all the denominators so as to be able to add and subtract the numerators easily. The nearest value the denominators can be scaled upto is the LCM of all the denominators. Thus, we need to find the LCM of all the denominators and then multiply all the denominators with appropriate integer factors to make them equal to the LCM. But, in order to keep the individual fraction values unchanged, we need to multiply the individual numerators also with the same factors. 

In order to find the LCM, we can go as follows. We use the method $$lcm(a,b,c) = lcm( lcm(a,b), c)$$. Thus, if we can compute the lcm of two denominators, we can keep on repeating the process iteratively over the denominators to get the overall lcm. To find the lcm of two numbers $$a$$ and $$b$$, we use $$lcm(a,b) = (a*b)/gcd(a,b)$$. For the above example, the $$lcm$$ turns out to be 6.

Thus, we scale up the denominators to 6 as follows:

$$
\frac{3*3}{2*3} + \frac{5*2}{3*2} -\frac{7}{6}
$$

Thus, we can observe that, the scaling factor for a fraction $$\frac{num}{den}$$ is given by: $${num*x}/{den*x}$$, where $$x$$ is the corresponding scaling factor. Note that, $$den*x=lcm$$. Thus, $$x=lcm/den$$. Thus, we find out the corresponding scaling factor $$x_i$$ for each fraction.

After this, we can directly add or subtract the new scaled numerators.

In the current example, we obtain $$\frac{12}{6}$$ as the result. Now, we need to convert this into an irreducible fraction. Thus, if we obtain $$\frac{num_i}{den_i}$$ as the final result, we need to find a largest factor $$y$$, which divides both $$num_i$$ and $$den_i$$. Such a number, as we know, is the gcd of $$num_i$$ and $$den_i$$.

Thus, to convert the result $$\frac{num_i}{den_i}$$, we divide both the numerator and denominator by the gcd of the two numbers $$y$$ to obtain the final irreducible $$\frac{num_i/y}{den_i/y}$$.

Note: A problem with this approach is that we find the lcm of all the denominators in a single go and then reduce the overall fraction at the end. Thus, the lcm value could become very large and could lead to an overflow. But, this solution suffices for the current range of numbers.

<iframe src="https://leetcode.com/playground/JiKTFxbd/shared" frameBorder="0" width="100%" height="500" name="JiKTFxbd"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n \log x)$$. Euclidean GCD algorithm takes $$O(\log(ab))$$ time for finding gcd of two numbers $$a$$ and $$b$$. Here $$n$$ refers to the number of fractions in the input string and $$x$$ is the maximum possible value of denominator.

* Space complexity : $$O(n)$$. Size of $$num$$, $$den$$ and $$sign$$ list grows upto $$n$$.

---
#### Approach 2: Using GCD

**Algorithm**

We know that we can continue the process of evaluating the given fractions by considering pairs of fractions at a time and continue the process considering the result obtained and the new fraction to be evaluated this time. We make use of this observation, and thus, instead of segregating the signs, numerators and denominators first, we directly start scanning the given strings and operate on the fractions obtained till now whenever a new sign is encountered.

We operate on the pairs of fractions, and keep on reducing the result obtained to irreducible fractions on the way. By doing this, we can reduce the chances of the problem of potential overflow possible in case the denominators lead to a large value of lcm.

We also observed from the last approach, that we need to equalize the denominators of a pair of fractions say:

$$\frac{a}{b} + \frac{c}{d}$$

We used a scaling factor of $$x$$ for the first fraction (both numerator and denominator). Here, $$x=\frac{\text{lcm}(b,d)}{b}$$. For the second fraction, the scaling factor $$y$$ is given by $$y= \frac{\text{lcm(b,d)}}{d}$$. Here, $$\text{lcm}(b,d)=\frac{b \cdot d}{\gcd(b,d)}$$. Thus, instead of finding the lcm and then again determining the scaling factor, we can directly use: $$x=\frac{b \cdot d}{\gcd(b,d) \cdot b} = \frac{d}{\gcd(b,d)}$$, and $$y=\frac{b \cdot d}{\gcd(b,d) \cdot d}$$. Thus, we need to scale the numerators appropriately and add/subtract them in terms of pairs. The denominators are scaled in the same manner to the lcm of the two denominators involved.

After evaluting every pair of fractions, we again reduce them to irreducible fractions by diving both the numerator and denominator of the resultant fraction by the gcd of the two.


<iframe src="https://leetcode.com/playground/sDp78cmU/shared" frameBorder="0" width="100%" height="500" name="sDp78cmU"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n \log x)$$. Euclidean GCD algorithm takes $$O(\log(ab))$$ time for finding gcd of two numbers $$a$$ and $$b$$. Here $$n$$ refers to the number of fractions in the input string and $$x$$ is the maximum possible value of denominator.

* Space complexity : $$O(n)$$. Size of $$sign$$ list grows upto $$n$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Small simple C++/Java/Python
- Author: StefanPochmann
- Creation Date: Sun May 21 2017 17:17:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 16 2018 06:35:46 GMT+0800 (Singapore Standard Time)

<p>
Keep the overall result in `A / B`, read the next fraction into `a / b`. Their sum is `(Ab + aB) / Bb` (but cancel their greatest common divisor).

**C++:**

    string fractionAddition(string expression) {
        istringstream in(expression);
        int A = 0, B = 1, a, b;
        char _;
        while (in >> a >> _ >> b) {
            A = A * b + a * B;
            B *= b;
            int g = abs(__gcd(A, B));
            A /= g;
            B /= g;
        }
        return to_string(A) + '/' + to_string(B);
    }

**Java:**

    public String fractionAddition(String expression) {
        Scanner sc = new Scanner(expression).useDelimiter("/|(?=[-+])");
        int A = 0, B = 1;
        while (sc.hasNext()) {
            int a = sc.nextInt(), b = sc.nextInt();
            A = A * b + a * B;
            B *= b;
            int g = gcd(A, B);
            A /= g;
            B /= g;
        }
        return A + "/" + B;
    }

    private int gcd(int a, int b) {
        return a != 0 ? gcd(b % a, a) : Math.abs(b);
    }

**Python 3:**
Added this after @lee215 reminded me about Python 3's `math.gcd` with his solution in the comments.

    def fractionAddition(self, expression):
        ints = map(int, re.findall('[+-]?\d+', expression))
        A, B = 0, 1
        for a in ints:
            b = next(ints)
            A = A * b + a * B
            B *= b
            g = math.gcd(A, B)
            A //= g
            B //= g
        return '%d/%d' % (A, B)
</p>


### Simple Python solution, no Fraction, and for people who are not familiar with Regular Expression.
- Author: Xuan__Yu
- Creation Date: Mon Dec 17 2018 02:55:38 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 17 2018 02:55:38 GMT+0800 (Singapore Standard Time)

<p>
If you like it, please give me an upvote.
```
    def fractionAddition(self, expression):
        def gcd(i, j):
            while j: i, j = j, i%j
            return i
        lst = expression.replace("+", " +").replace("-", " -").split()
        A, B = 0, 1
        for num in lst:
            a, b = num.split("/")
            a, b = int(a), int(b)
            A = A*b+B*a
            B *= b
            devisor = gcd(A, B)
            A /= devisor; B /= devisor
        return str(A) + "/" + str(B)
```
</p>


### Concise Java Solution
- Author: compton_scatter
- Creation Date: Sun May 21 2017 11:02:26 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 31 2018 08:29:22 GMT+0800 (Singapore Standard Time)

<p>
```
public String fractionAddition(String expression) {
    String[] fracs = expression.split("(?=[-+])"); // splits input string into individual fractions
    String res = "0/1";
    for (String frac : fracs) res = add(res, frac); // add all fractions together
    return res;
}

public String add(String frac1, String frac2) {
    int[] f1 = Stream.of(frac1.split("/")).mapToInt(Integer::parseInt).toArray(), 
          f2 = Stream.of(frac2.split("/")).mapToInt(Integer::parseInt).toArray();
    int numer = f1[0]*f2[1] + f1[1]*f2[0], denom = f1[1]*f2[1];
    String sign = "";
    if (numer < 0) {sign = "-"; numer *= -1;}
    return sign + numer/gcd(numer, denom) + "/" + denom/gcd(numer, denom); // construct reduced fraction
}

// Computes gcd using Euclidean algorithm
public int gcd(int x, int y) { return x == 0 || y == 0 ? x + y : gcd(y, x % y); }
```
</p>


