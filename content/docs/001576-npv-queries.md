---
title: "NPV Queries"
weight: 1576
#id: "npv-queries"
---
## Description
<div class="description">
<p>Table: <code>NPV</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| year          | int     |
| npv           | int     |
+---------------+---------+
(id, year) is the primary key of this table.
The table has information about the id and the year of each inventory and the corresponding net present value.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Queries</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| year          | int     |
+---------------+---------+
(id, year) is the primary key of this table.
The table has information about the id and the year of each inventory query.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the npv of all each query of queries table.</p>

<p>Return the result table in any order.</p>

<p>The query result format is in the following example:</p>

<pre>
NPV table:
+------+--------+--------+
| id   | year   | npv    |
+------+--------+--------+
| 1    | 2018   | 100    |
| 7    | 2020   | 30     |
| 13   | 2019   | 40     |
| 1    | 2019   | 113    |
| 2    | 2008   | 121    |
| 3    | 2009   | 12     |
| 11   | 2020   | 99     |
| 7    | 2019   | 0      |
+------+--------+--------+

Queries table:
+------+--------+
| id   | year   |
+------+--------+
| 1    | 2019   |
| 2    | 2008   |
| 3    | 2009   |
| 7    | 2018   |
| 7    | 2019   |
| 7    | 2020   |
| 13   | 2019   |
+------+--------+

Result table:
+------+--------+--------+
| id   | year   | npv    |
+------+--------+--------+
| 1    | 2019   | 113    |
| 2    | 2008   | 121    |
| 3    | 2009   | 12     |
| 7    | 2018   | 0      |
| 7    | 2019   | 0      |
| 7    | 2020   | 30     |
| 13   | 2019   | 40     |
+------+--------+--------+

The npv value of (7, 2018) is not present in the NPV table, we consider it 0.
The npv values of all other queries can be found in the NPV table.
</pre>
</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Should have been tagged as easy ( Just 1 left/right join )
- Author: saketgarodia_UC
- Creation Date: Thu May 07 2020 08:12:40 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 07 2020 08:12:40 GMT+0800 (Singapore Standard Time)

<p>
select q.id, q.year, coalesce(n.npv, 0) as npv
from npv n right join queries q
on n.id = q.id and n.year = q.year
</p>


### why is this question categorized as 'medium'
- Author: Xiaomeng-fan
- Creation Date: Sat Jun 06 2020 16:34:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 06 2020 16:37:25 GMT+0800 (Singapore Standard Time)

<p>
this question is way more easier than some easy questions 
pls fix it, leetcode
</p>


### My SQL easy solution
- Author: dihengliu
- Creation Date: Wed Apr 29 2020 09:43:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Apr 29 2020 09:43:19 GMT+0800 (Singapore Standard Time)

<p>
```
select t1.id, t1.year, ifnull(npv, 0) as npv
from queries as t1
left join NPV as t2
on t1.id = t2.id and t1.year = t2.year;
```
</p>


