---
title: "Guess Number Higher or Lower II"
weight: 358
#id: "guess-number-higher-or-lower-ii"
---
## Description
<div class="description">
<p>We are playing the Guessing Game. The game will work as follows:</p>

<ol>
	<li>I pick a number between&nbsp;<code>1</code>&nbsp;and&nbsp;<code>n</code>.</li>
	<li>You guess a number.</li>
	<li>If you guess the right number, <strong>you win the game</strong>.</li>
	<li>If you guess the wrong number, then I will tell you whether the number I picked is <strong>higher or lower</strong>, and you will continue guessing.</li>
	<li>Every time you guess a wrong number&nbsp;<code>x</code>, you will pay&nbsp;<code>x</code>&nbsp;dollars. If you run out of money, <strong>you lose the game</strong>.</li>
</ol>

<p>Given a particular&nbsp;<code>n</code>, return&nbsp;<em>the minimum amount of money you need to&nbsp;<strong>guarantee a win regardless of what number I pick</strong></em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/10/graph.png" style="width: 505px; height: 388px;" />
<pre>
<strong>Input:</strong> n = 10
<strong>Output:</strong> 16
<strong>Explanation:</strong> The winning strategy is as follows:
- The range is [1,10]. Guess 7.
&nbsp;   - If this is my number, your total is $0. Otherwise, you pay $7.
&nbsp;   - If my number is higher, the range is [8,10]. Guess 9.
&nbsp;       - If this is my number, your total is $7. Otherwise, you pay $9.
&nbsp;       - If my number is higher, it must be 10. Guess 10. Your total is $7 + $9 = $16.
&nbsp;       - If my number is lower, it must be 8. Guess 8. Your total is $7 + $9 = $16.
&nbsp;   - If my number is lower, the range is [1,6]. Guess 3.
&nbsp;       - If this is my number, your total is $7. Otherwise, you pay $3.
&nbsp;       - If my number is higher, the range is [4,6]. Guess 5.
&nbsp;           - If this is my number, your total is $7 + $3 = $10. Otherwise, you pay $5.
&nbsp;           - If my number is higher, it must be 6. Guess 6. Your total is $7 + $3 + $5 = $15.
&nbsp;           - If my number is lower, it must be 4. Guess 4. Your total is $7 + $3 + $5 = $15.
&nbsp;       - If my number is lower, the range is [1,2]. Guess 1.
&nbsp;           - If this is my number, your total is $7 + $3 = $10. Otherwise, you pay $1.
&nbsp;           - If my number is higher, it must be 2. Guess 2. Your total is $7 + $3 + $1 = $11.
The worst case in all these scenarios is that you pay $16. Hence, you only need $16 to guarantee a win.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> 0
<strong>Explanation:</strong>&nbsp;There is only one possible number, so you can guess 1 and not have to pay anything.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 2
<strong>Output:</strong> 1
<strong>Explanation:</strong>&nbsp;There are two possible numbers, 1 and 2.
- Guess 1.
&nbsp;   - If this is my number, your total is $0. Otherwise, you pay $1.
&nbsp;   - If my number is higher, it must be 2. Guess 2. Your total is $1.
The worst case is that you pay $1.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 200</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Minimax (minimax)

## Companies
- Google - 3 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]
## Summary

Given a number $$n$$, we have to find the worst case cost of guessing a number chosen from the range $$(1, n)$$, assuming that the guesses are made intelligently(minimize the total cost). The cost is incremented by $$i$$ for every wrong guess $$i$$.

For example:
```
n=5
1 2 3 4 5
```
If we start with 3 as the initial guess, the next guess would certainly be 4 as in the worst case required number is 5. Total Cost $$= 4+3=7$$.

But if we start with 4 as the initial guess, our next guess would be 2 as in the worst case required number is 3 or 1. Total Cost $$=4+2=6$$ which is the minimum cost.

```
n=8
1 2 3 4 5 6 7 8
```
In this case we have to guess 5 followed by 7. Total Cost $$=5+7=12$$.
If we choose 4 as our intial guess. Total Cost $$=4+5+7=16$$.

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

Firstly, we need to be aware of the fact that out of the range $$(1, n)$$, we have to guess the numbers intelligently in order to minimize the cost. But, along with that we have to take into account the worst case scenario possible, that is we have to assume that the original number chosen is such that it will try to maximize the overall cost.

In Brute Force, we can pick up any number $$i$$ in the range $$(1, n)$$. Assuming it is a wrong guess(worst case scenario), we have to minimize the cost of reaching the required number. Now, the required number could be lying either to the right or left of the number picked($$i$$). But to cover the possibility of the worst case number chosen, we need to take the maximum cost out of the cost of reaching the worst number out of the right and left segments of $$i$$. Thus, if we pick up $$i$$ as the pivot, the overall minimum cost for the worst required number will be:

$$
\mathrm{cost}(1, n)=i + \max\big(\mathrm{cost}(1,i-1), \mathrm{cost}(i+1,n)\big)
$$

For every segment, we can further choose another pivot and repeat the same process for calculating the minimum cost.

By using the above procedure, we found out the cost of reaching the required number starting with $$i$$ as the pivot. In the same way, we iterate over all the numbers in the range $$(1, n)$$, choosing them as the pivot, calculating the cost of every pivot chosen and thus, we can find the minimum cost out of those.


<iframe src="https://leetcode.com/playground/QW3ndyqL/shared" frameBorder="0" name="QW3ndyqL" width="100%" height="326"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n!)$$. We choose a number as pivot and repeat the pivoting process further $$n$$ times $$O(n!)$$. We repeat the same process for $$n$$ pivots.
* Space complexity : $$O(n)$$. Recursion of depth $$n$$ is used.

---
#### Approach #2 Modified Brute Force [Time Limit Exceeded]

**Algorithm**

In Brute Force, for numbers in the range $$(i, j)$$, we picked up every number from $$i$$ to $$j$$ as the pivot and found the maximum cost out of its left and right segments. But an important point to observe is that if we choose any number from the range $$\big( i,\frac{i+j}{2} \big)$$ as the pivot, the right segment(consisting of numbers larger than the picked up pivot) will be longer than the left segment(consisting of numbers smaller than it). Thus, we will always get the maximum cost from its right segment and it will be larger than the minimum cost achievable by choosing some other pivot. Therefore, our objective here is to reduce the larger cost which is coming from the right segment. Thus, it is wise to choose the pivot from the range $$\big(\frac{i+j}{2}, j\big)$$. In this way the costs of the two segments will be nearer to each other and this will minimize the overall cost.

Thus, while choosing the pivot instead of iterating from $$i$$ to $$j$$, we iterate from $$\frac{i+j}{2}$$ to $$j$$ and find the minimum achievable cost similar to brute force.

<iframe src="https://leetcode.com/playground/juXrfQvD/shared" frameBorder="0" name="juXrfQvD" width="100%" height="309"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n!)$$. We choose a number as pivot and repeat the pivoting process further $$n$$ times $$O(n!)$$. We repeat the same process for $$n$$ pivots.
* Space complexity : $$O(n)$$. Recursion of depth $$n$$ is used.

---
#### Approach #3 Using DP [Accepted]

**Algorithm**

The problem of finding the minimum cost of reaching the destination number choosing $$i$$ as a pivot can be divided into the subproblem of finding the maximum out of the minimum costs of its left and right segments as explained above. For each segment, we can continue the process leading to smaller and smaller subproblems. This leads us to the conclusion that we can use DP for this problem.

We need to use a $$dp$$ matrix, where $$dp(i, j)$$ refers to the minimum cost of finding the worst number given only the numbers in the range $$(i, j)$$. Now, we need to know how to fill in the entries of this $$dp$$. If we are given only a single number $$k$$, no matter what the number is the cost of finding that number is always 0 since we always hit the number directly without any wrong guess. Thus, firstly, we fill in all the entries of the $$dp$$ which correspond to segments of length 1 i.e. all entries $$dp(k, k)$$ are initialized to 0. Then, in order to find the entries for segments of length 2, we need all the entries for segments of length 1. Thus, in general, to fill in the entries corresponding to segments of length $$len$$, we need all the entries of length $$len-1$$ and below to be already filled. Thus, we need to fill the entries in the order of their segment lengths. Thus, we fill the entries of $$dp$$ diagonally.

Now, what criteria do we need to fill up the $$dp$$ matrix? For any entry  $$dp(i, j)$$, given the current segment length of interest is $$len$$ i.e. if $$len=j-i+1$$, we assume as if we are available only with the numbers in the range $$(i, j)$$. To fill in its current entry, we follow the same process as Approach 1, choosing every number as the pivot and finding the minimum cost as:

$$
\mathrm{cost}(i, j)=\mathrm{pivot} + \max\big(\mathrm{cost}(i,\mathrm{pivot}-1), \mathrm{cost}(\mathrm{pivot}+1,j)\big)
$$

But, we have an advantage in terms of calculating the cost here, since we already know the costs for the segments of length smaller than $$len$$ from $$dp$$. Thus, the dp equation becomes:

$$
\mathrm{dp}(i, j) = \min_{\mathrm{pivot} \in (i, j)} \big[ \mathrm{pivot} + \max \big( \mathrm{dp}(i,\mathrm{pivot}-1) , \mathrm{dp}(\mathrm{pivot}+1,j) \big) \big]
$$

  where $$\min_{\mathrm{pivot} \in (i, j)}$$ indicates the minimum obtained by considering every number in the range $$(i, j)$$ as the pivot.

The following animation will make the process more clear for n=5:
<!--![Guess Number Higher or Lower](https://leetcode.com/media/original_images/375_Guess_Number_Higher_or_Lower.gif)-->
!?!../Documents/375_Guess.json:791,552!?!


<iframe src="https://leetcode.com/playground/X99KiHYD/shared" frameBorder="0" name="X99KiHYD" width="100%" height="326"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n^3)$$. We traverse the complete $$dp$$ matrix once $$(O(n^2))$$. For every entry we take atmost $$n$$ numbers as pivot.

* Space complexity : $$O(n^2)$$. $$dp$$ matrix of size $$n^2$$ is used.

---

#### Approach #4 Better Approach using DP [Accepted]

**Algorithm**

In the last approach, we chose every possible pivot from the range $$(i, j)$$. But, as per the argument given in Approach 2, we can choose pivots only from the range $$\big(i+(len-1)/2,j\big)$$, where $$len$$ is the current segment length of interest.
Thus the governing equation is:

$$
\mathrm{dp}(i, j)=\min_{\mathrm{pivot} \in \big(i+\frac{len-1}{2}, j\big)}\big[\mathrm{pivot} + \max\big(\mathrm{dp}(i,\mathrm{pivot}-1), \mathrm{dp}(\mathrm{pivot}+1,j)\big)\big]
$$

 Thus, we can optimize the Approach 3 to some extent.


<iframe src="https://leetcode.com/playground/vxpg2Chd/shared" frameBorder="0" name="vxpg2Chd" width="100%" height="360"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n^3)$$. We traverse the complete $$dp$$ matrix once $$(O(n^2))$$. For every entry we take at most $$n$$ numbers as pivot.

* Space complexity : $$O(n^2)$$. $$dp$$ matrix of size $$n^2$$ is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Improve the Question and Example
- Author: grodrigues3
- Creation Date: Mon Jul 18 2016 08:05:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 05:59:15 GMT+0800 (Singapore Standard Time)

<p>
Can you guys define the problem a little more and improve the test case. 

The example provided for n=10 does not actually give the solution to the problem (it shows 21 as the result of the provided guessing pattern); however, if you run a custom testcase the leetcode server say the answer is 16.  Can you explain how we arrive at 16.  If the number is 8 and we choose 5 7 9, we pay $21.  How does one do better than that?

Additionally, the question states:  Given a particular n \u2265 1, find out how much money you need to have to guarantee a win.  Well, this wording is ambiguous, as I should be able to always return n(n+1)/2 to give the sum of all numbers from 1 to n.  That would certainly guarantee a win, right?  I think you are looking for the **minimum** amount of money you need to guarantee a win.  If so, can you specify that and improve the example to reflect it (and show how the answer 16 is achieved).
</p>


### Simple DP solution with explanation~~
- Author: bbccyy1
- Creation Date: Sat Jul 16 2016 11:03:34 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 03:38:18 GMT+0800 (Singapore Standard Time)

<p>
For each number x in range[i~j]
we do:  result_when_pick_x = x + **max**{DP([i~x-1]),  DP([x+1, j])}    
                 --> *// the max means whenever you choose a number, the feedback is always bad and therefore leads you to a worse branch.*
then we get  DP([i~j]) = **min**{xi, ... ,xj}    
                 --> *// this min makes sure that you are minimizing your cost.*

```
public class Solution {
    public int getMoneyAmount(int n) {
        int[][] table = new int[n+1][n+1];
        return DP(table, 1, n);
    }
    
    int DP(int[][] t, int s, int e){
        if(s >= e) return 0;
        if(t[s][e] != 0) return t[s][e];
        int res = Integer.MAX_VALUE;
        for(int x=s; x<=e; x++){
            int tmp = x + Math.max(DP(t, s, x-1), DP(t, x+1, e));
            res = Math.min(res, tmp);
        }
        t[s][e] = res;
        return res;
    }
}
```

Here is a bottom up solution.

```
public class Solution {
    public int getMoneyAmount(int n) {
        int[][] table = new int[n+1][n+1];
        for(int j=2; j<=n; j++){
            for(int i=j-1; i>0; i--){
                int globalMin = Integer.MAX_VALUE;
                for(int k=i+1; k<j; k++){
                    int localMax = k + Math.max(table[i][k-1], table[k+1][j]);
                    globalMin = Math.min(globalMin, localMax);
                }
                table[i][j] = i+1==j?i:globalMin;
            }
        }
        return table[1][n];
    }
}
```
</p>


### Clarification on the problem description. [Problem description need to be updated !!! ]
- Author: 10000tb
- Creation Date: Thu Nov 17 2016 06:52:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 14:48:49 GMT+0800 (Singapore Standard Time)

<p>
It is actually confusing that the example shown in the problem description is not the best stragety to guess the final target number, and the problem itself is asking for the lowest cost achieved by best guessing strategy.
The example description should be updated.


```---POSSIBLY, it can also add some example about the BEST Strategy---```
The example description should be:

first introducebest strategyto guess:

1. ```for one number```, like 1, best strategy is 0$
2. ```for two number```, like 3,4, best strategy is 3$, which can be understood in this way: you have two way to guess: a) start by guess 4 is the target, (the worst case is) if wrong, you get charged $4, then immediately you know 3 is the target number, get get charged $0 by guessing that, and finally you get charged $4. b) similarly, if you start by 3, (the worst case is) if wrong, you get charged $3, then you immediately know that 4 is the target number, and get charged $0 for guessing this, and finally you get charged $3. In summary:
range ---------> best strategy cost
3, 4 ---------> $3
5, 6 ---------> $5
...
3. ```for three number```, the best strategy is guess the middle number first, and (worst case is) if wrong, you get charged that middle number money, and then you immediately know what target number is by using "lower" or "higher" response, so in summary:
range ---------> best strategy cost
3, 4, 5 ---------> $4
7, 8, 9 ---------> $8
...
4. ```for more numbers```, it can simply be reduced them into smaller ranges, and here is why DP solution make more sense in solving this.
suppose the range is [start, end]
the strategy here is to iterate through all number possible and select it as the starting point, say for any k between start and end, the worst cost for this is: k+DP( start, k-1 ) + DP(k+1, end ), and the goal is minimize the cost, so you need the minimum one among all those k between start and end
</p>


