---
title: "Bag of Tokens"
weight: 898
#id: "bag-of-tokens"
---
## Description
<div class="description">
<p>You have an initial power <code>P</code>, an initial score of <code>0</code> points, and a bag of tokens.</p>

<p>Each token can be used at most once, has a value <code>token[i]</code>, and has potentially two ways to use it.</p>

<ul>
	<li>If we have at least <code>token[i]</code> power, we may play the token face up, losing <code>token[i]</code> power, and gaining <code>1</code> point.</li>
	<li>If we have at least <code>1</code> point, we may play the token face down, gaining <code>token[i]</code> power, and losing <code>1</code> point.</li>
</ul>

<p>Return the largest number of points we can have after playing any number of tokens.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>tokens = <span id="example-input-1-1">[100]</span>, P = <span id="example-input-1-2">50</span>
<strong>Output: </strong><span id="example-output-1">0</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>tokens = <span id="example-input-2-1">[100,200]</span>, P = <span id="example-input-2-2">150</span>
<strong>Output: </strong><span id="example-output-2">1</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>tokens = <span id="example-input-3-1">[100,200,300,400]</span>, P = <span id="example-input-3-2">200</span>
<strong>Output: </strong><span id="example-output-3">2</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>tokens.length &lt;= 1000</code></li>
	<li><code>0 &lt;= tokens[i] &lt; 10000</code></li>
	<li><code>0 &lt;= P &lt; 10000</code></li>
</ol>
</div>
</div>
</div>

</div>

## Tags
- Greedy (greedy)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Greedy

**Intuition**

If we play a token face up, we might as well play the one with the smallest value.  Similarly, if we play a token face down, we might as well play the one with the largest value.

**Algorithm**

We don't need to play anything until absolutely necessary.  Let's play tokens face up until we can't, then play a token face down and continue.

We must be careful, as it is easy for our implementation to be incorrect if we do not handle corner cases correctly.  We should always play tokens face up until exhaustion, then play one token face down and continue.

Our loop must be constructed with the right termination condition: we can either play a token face up or face down.

Our final answer could be any of the intermediate answers we got after playing tokens face up (but before playing them face down.)

<iframe src="https://leetcode.com/playground/NXpdv9Np/shared" frameBorder="0" width="100%" height="412" name="NXpdv9Np"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the length of `tokens`.

* Space Complexity:  $$O(N)$$. 
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Bad descriptions!
- Author: lMiaoj
- Creation Date: Sun Nov 25 2018 16:26:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 25 2018 16:26:39 GMT+0800 (Singapore Standard Time)

<p>
The description of the problem is not very clear.
1. At first I thought `losing power / point` means **not getting them**, but it turned out to be **minus** the `power / point` we got before (`losing` is not as clear as `minus`, a little misleading);
2. Bad example 3: I really didn\'t understand the third example at first, because I thought we should take the token `in order`. But it turned out that we can take whichever `tokens[i]` we want -- which really should be **emphasized**. And  I really think that this kind of examples should go with the process of getting the result, otherwise it will be misleading or confusing.
3. The descriptions didn\'t include the situation when `power<tokens[i] && point<1`. And when it described the other two situations, it used the word **may**, which is a little misleading.

PS: the order of taking tokens in example 3 is as below:
```
         Current power         point
		200               0
		100               1       (minus 100 powers, plus 1 point)
		500               0       (plus 400 powers, minus 1 point)
		300               1       (minus 200 powers, plus 1 point)
		0                 2       (minus 300 powers, plus 1 point) -------  max
```

Someone asked me to translate the problem description. So I add my understanding here. Hope it helps.
```
Tranlations:
You have a bag of tokens, from which you can take whichever token you want, and after you take one, you can\'t put it back to the bag, meaning you can use every token at most once.

You start the game with P power and 0 point.

For every tokens[i], you can use it in either way:
- plus tokens[i] powers, and minus 1 point;
- or, minus tokens[i] powers, and plus 1 point.
(meaning you exchange your powers to get 1 point, or exchange your point to get more powers)

But you have to make sure that during the process, both your powers>=0 and points>=0, otherwise you would have to stop playing the game.

And you can use just some of the tokens (don\'t have to use all of them).

Your target is to get the maximum points possible.
```
</p>


### [C++/Java/Python] Greedy + Two Pointers
- Author: lee215
- Creation Date: Sun Nov 25 2018 12:10:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 25 2018 12:10:05 GMT+0800 (Singapore Standard Time)

<p>
Sort tokens.
Buy at the cheapest and sell at the most expensive.

**C++**
```
    int bagOfTokensScore(vector<int>& tokens, int P) {
        sort(tokens.begin(), tokens.end());
        int res = 0, points = 0, i = 0, j = tokens.size() - 1;
        while (i <= j) {
            if (P >= tokens[i]) {
                P -= tokens[i++];
                res = max(res, ++points);
            } else if (points > 0) {
                points--;
                P += tokens[j--];
            } else {
                break;
            }
        }
        return res;
    }
```

**Java:**
```
    public int bagOfTokensScore(int[] tokens, int P) {
        Arrays.sort(tokens);
        int res = 0, points = 0, i = 0, j = tokens.length - 1;
        while (i <= j) {
            if (P >= tokens[i]) {
                P -= tokens[i++];
                res = Math.max(res, ++points);
            } else if (points > 0) {
                points--;
                P += tokens[j--];
            } else {
                break;
            }
        }
        return res;
    }
```
**Python:**
```
    def bagOfTokensScore(self, tokens, P):
        res = cur = 0
        d = collections.deque(sorted(tokens))
        while d and (d[0] <= P or cur):
            if d[0] <= P:
                P -= d.popleft()
                cur += 1
            else:
                P += d.pop()
                cur -= 1
            res = max(res, cur)
        return res
```

</p>


### Python two pointers O(N*logN) time O(1) space
- Author: cenkay
- Creation Date: Sun Nov 25 2018 12:03:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 25 2018 12:03:12 GMT+0800 (Singapore Standard Time)

<p>
* Solution is straightforward.
* Always aim for highest point
* Sort the tokens so we can buy from lowest & sell from highest which means =>
	* If we have enough power, no worries. Just lose token[l(eft)], and increase score by 1.
	* If we have at least 1 score and we are not in the last processed token, gain token[r(ight)] and decrease score by 1.
	* Otherwise, we are finished.
```
class Solution:
    def bagOfTokensScore(self, tokens, P):
        tokens.sort()
        l, r, score = 0, len(tokens) - 1, 0
        while l <= r:
            if P >= tokens[l]:
                P -= tokens[l]
                score += 1
                l += 1
            elif score and l != r:
                P += tokens[r]
                score -= 1
                r -= 1
            else:
                break
        return score
```
</p>


