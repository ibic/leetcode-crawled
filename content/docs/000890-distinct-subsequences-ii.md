---
title: "Distinct Subsequences II"
weight: 890
#id: "distinct-subsequences-ii"
---
## Description
<div class="description">
<p>Given a string <code>S</code>, count the number of distinct, non-empty subsequences of <code>S</code> .</p>

<p>Since the result may be large, <strong>return the answer modulo <code>10^9 + 7</code></strong>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;abc&quot;</span>
<strong>Output: </strong><span id="example-output-1">7</span>
<span><strong>Explanation</strong>: The 7 distinct subsequences are &quot;a&quot;, &quot;b&quot;, &quot;c&quot;, &quot;ab&quot;, &quot;ac&quot;, &quot;bc&quot;, and &quot;abc&quot;.</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;aba&quot;</span>
<strong>Output: </strong><span id="example-output-2">6
</span><strong>Explanation</strong>: The 6 distinct subsequences are &quot;a&quot;, &quot;b&quot;, &quot;ab&quot;, &quot;ba&quot;, &quot;aa&quot; and &quot;aba&quot;.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">&quot;aaa&quot;</span>
<strong>Output: </strong><span id="example-output-3">3
</span><strong>Explanation</strong>: The 3 distinct subsequences are &quot;a&quot;, &quot;aa&quot; and &quot;aaa&quot;.
</pre>
</div>
</div>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>S</code> contains only lowercase letters.</li>
	<li><code>1 &lt;= S.length &lt;= 2000</code></li>
</ol>

<div>
<p>&nbsp;</p>

<div>
<div>&nbsp;</div>
</div>
</div>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Dynamic Programming

**Intuition and Algorithm**

Even though the final code for this problem is very short, it is not very intuitive to find the answer.  In the solution below, we'll focus on finding all subsequences (including empty ones), and subtract the empty subsequence at the end.

Let's try for a dynamic programming solution.  In order to not repeat work, our goal is to phrase the current problem in terms of the answer to previous problems.  A typical idea will be to try to count the number of states `dp[k]` (distinct subsequences) that use letters `S[0], S[1], ..., S[k]`.

Naively, for say, `S = "abcx"`, we have `dp[k] = dp[k-1] * 2`.  This is because for `dp[2]` which counts `("", "a", "b", "c", "ab", "ac", "bc", "abc")`, `dp[3]` counts all of those, plus all of those with the `x` ending, like `("x", "ax", "bx", "cx", "abx", "acx", "bcx", "abcx")`.

However, for something like `S = "abab"`, let's play around with it.  We have:

* `dp[0] = 2`, as it counts `("", "a")`
* `dp[1] = 4`, as it counts `("", "a", "b", "ab")`;
* `dp[2] = 7` as it counts `("", "a", "b", "aa", "ab", "ba", "aba")`;
* `dp[3] = 12`, as it counts `("", "a", "b", "aa", "ab", "ba", "bb", "aab", "aba", "abb", "bab", "abab")`.

We have that dp[3]` counts `dp[2]`, plus `("b", "aa", "ab", "ba", "aba")` with `"b"` added to it.  Notice that `("", "a")` are missing from this list, as they get double counted.  In general, the sequences that resulted from putting `"b"` the last time (ie. `"b", "ab"`) will get double counted.

This insight leads to the recurrence:

`dp[k] = 2 * dp[k-1] - dp[last[S[k]] - 1]`

The number of distinct subsequences ending at `S[k]`, is twice the distinct subsequences counted by `dp[k-1]` (all of them, plus all of them with S[k] appended), minus the amount we double counted, which is `dp[last[S[k]] - 1]`.

<iframe src="https://leetcode.com/playground/e4U39tC7/shared" frameBorder="0" width="100%" height="463" name="e4U39tC7"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.

* Space Complexity:  $$O(N)$$.  It is possible to adapt this solution to take $$O(1)$$ space.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] 4 lines O(N) Time, O(1) Space
- Author: lee215
- Creation Date: Sun Nov 11 2018 12:02:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 11 2018 12:02:04 GMT+0800 (Singapore Standard Time)

<p>
More detailed chinese solution [here](https://www.jianshu.com/p/02501f516437) explaining how I come up with it.

## Explanation
Init an array `endswith[26]`
`endswith[i]` to count how many sub sequence that ends with `i`th character.

Now we have `N = sum(endswith)` different sub sequence,
add a new character `c` to each of them,
then we have `N` different sub sequence that ends with `c`.

With this idea, we loop on the whole string `S`,
and we update `end[c] = sum(end) + 1` for each character.

We need to plus one here, because `"c"` itself is also a sub sequence.

## Example
Input: `"aba"`
Current parsed: `"ab"`

endswith `\'a\'`: `["a"]`
endswith `\'b\'`: `["ab","b"]`

`"a"` -> `"aa"`
`"ab"` -> `"aba"`
`"b"` -> `"ba"`
`""` -> `"a"`

endswith `\'a\'`: `["aa","aba","ba","a"]`
endswith `\'b\'`: `["ab","b"]`
result: 6

## Complexity
Time `O(26N)`, Space `O(1)`.

**C++:**
```
    int distinctSubseqII(string S) {
        long endsWith[26] = {}, mod = 1e9 + 7;
        for (char c : S)
            endsWith[c - \'a\'] = accumulate(begin(endsWith), end(endsWith), 1L) % mod;
        return accumulate(begin(endsWith), end(endsWith), 0L) % mod;
    }
```

**Java:**
```
    public int distinctSubseqII(String S) {
        long end[] = new long[26], mod = (long)1e9 + 7;
        for (char c : S.toCharArray())
            end[c - \'a\'] = Arrays.stream(end).sum()%mod + 1;
        return (int)(Arrays.stream(end).sum() % mod);
    }
```
**Python:**
```
    def distinctSubseqII(self, S):
        end = [0] * 26
        for c in S:
            end[ord(c) - ord(\'a\')] = sum(end) + 1
        return sum(end) % (10**9 + 7)
```

Use a variable to count the `sum(end)` can avoid repeatly count it.
Improve time complexity from `O(26N)` to `O(N)`, if you want.

**C++:**
```
    int distinctSubseqII(string S) {
        int res = 0, added = 0, mod = 1e9 + 7, endsWith[26] = {};
        for (char c : S) {
            added = (res - endsWith[c - \'a\'] + 1) % mod;
            res = (res + added) % mod;
            endsWith[c - \'a\'] = (endsWith[c - \'a\'] + added) % mod;
        }
        return (res + mod) % mod;
    }
```

**Java:**
```
    public int distinctSubseqII(String S) {
        int end[] = new int[26], res = 0, added = 0, mod = (int)1e9 + 7;
        for (char c : S.toCharArray()) {
            added = (res + 1 - end[c - \'a\']) % mod;
            res = (res + added) % mod;
            end[c - \'a\'] = (end[c - \'a\'] + added) % mod;
        }
        return (res + mod) % mod;
    }
```
**Python:**
```
    def distinctSubseqII(self, S):
        res, end = 0, collections.Counter()
        for c in S:
            res, end[c] = res * 2 + 1 - end[c], res + 1
        return res % (10**9 + 7)
```
</p>


### Java DP O(N^2) time -> O(N) time -> O(1) space
- Author: wangzi6147
- Creation Date: Sun Nov 11 2018 12:04:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 11 2018 12:04:38 GMT+0800 (Singapore Standard Time)

<p>
`dp[i]` represents the count of unique subsequence ends with S[i].
`dp[i]` is initialized to `1` for `S[0 ... i]`
For each `dp[i]`, we define `j` from `0` to `i - 1`, we have:

1. if `s[j] != s[i]`, `dp[i] += dp[j]`
2. if `s[j] == s[i]`, do nothing to avoid duplicates.

Then `result = sum(dp[0], ... dp[n - 1])`
Time complexity: `O(n^2)`

```
class Solution {
    public int distinctSubseqII(String S) {
        int n = S.length(), M = (int)1e9 + 7, result = 0;
        int[] dp = new int[n];
        Arrays.fill(dp, 1);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (S.charAt(j) != S.charAt(i)) {
                    dp[i] += dp[j];
                    dp[i] %= M;
                }
            }
            result += dp[i];
            result %= M;
        }
        return result;
    }
}
```

Furthermore, we can use a `sum` to represent `sum(dp[0], ..., dp[i - 1])`.
And also a `count` array, in which `count[S.charAt(i) - \'a\']` represents the count of presented subsequence ends with `S.charAt(i)`.
Then `dp[i] = sum - count[S.charAt(i) - \'a\']`.
Time complexity: `O(n)`

```
class Solution {
    public int distinctSubseqII(String S) {
        int n = S.length(), M = (int)1e9 + 7;
        int[] dp = new int[n];
        Arrays.fill(dp, 1);
        int[] count = new int[26];
        int sum = 0;
        for (int i = 0; i < n; i++) {
            int index = S.charAt(i) - \'a\';
            dp[i] += sum - count[index];
            dp[i] = (dp[i] + M) % M;
            sum = (sum + dp[i]) % M;
            count[index] = (count[index] + dp[i]) % M;
        }
        return sum;
    }
}
 
```

Optimize to `O(1)` space:

```
class Solution {
    public int distinctSubseqII(String S) {
        int n = S.length(), M = (int)1e9 + 7;
        int[] count = new int[26];
        int sum = 0;
        for (int i = 0; i < n; i++) {
            int index = S.charAt(i) - \'a\';
            int cur = (1 + sum - count[index] + M) % M;
            sum = (sum + cur) % M;
            count[index] = (count[index] + cur) % M;
        }
        return sum;
    }
}
 
 
```
</p>


### Concise solution with well explanation
- Author: meng789987
- Creation Date: Sun Feb 24 2019 08:09:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 24 2019 08:09:48 GMT+0800 (Singapore Standard Time)

<p>
`dp[i]` is the number of disctinct subsequences for substring s[0..i]. It includes the empty string "" to make things easy. We\'ll exclude this one in the end by simply minus 1, so the answer is` dp[n-1] - 1`.

If all characters are distinct, then `dp[i] = dp[i-1]*2`, that is all previous subsequeuences without s[i], plus all previous subsequeuences appended with s[i].

If there are duplicate characters, We use `end[c]` to denote the number of distinct subsequences ending with char c so far. So number of all previous subsequences with s[i] should be subtracted by previous end[s[i]]. That is:
`dp[i] = dp[i-1] * 2 - end[s[i]]`

It is easy to compact dp from O(n) to O(1) as d[i] only depends on dp[i-1]. The code is quite simple actually:
```
    public int DistinctSubseqII(string s) {
        int res = 1, MOD = (int)1e9 + 7;
        var end = new int[26];
        
        foreach (var c in s) {
            var pre = res;
            res = (res * 2 % MOD - end[c - \'a\'] + MOD) % MOD;
            end[c - \'a\'] = pre;
        }
        
        return res - 1; // exclude ""
    }
```

Please vote if you like. :)
</p>


