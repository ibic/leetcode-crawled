---
title: "Apply Discount Every n Orders"
weight: 1251
#id: "apply-discount-every-n-orders"
---
## Description
<div class="description">
<p>There is&nbsp;a sale in a supermarket, there will be a <code>discount</code> every <code>n</code> customer.<br />
There are some products in the supermarket where the id of the <code>i-th</code> product is <code>products[i]</code> and the price per unit of this product is&nbsp;<code>prices[i]</code>.<br />
The system will count the number of customers and when the <code>n-th</code> customer arrive he/she will have a <code>discount</code> on the bill. (i.e if the cost is <code>x</code> the new cost is <code>x - (discount * x) / 100</code>). Then the system will start counting customers again.<br />
The customer orders a certain amount of each product where <code>product[i]</code> is the id of the <code>i-th</code> product the customer ordered and <code>amount[i]</code> is the number of units the customer ordered of that product.</p>

<p>Implement the <code>Cashier</code> class:</p>

<ul>
	<li><code>Cashier(int n, int discount, int[] products, int[] prices)</code> Initializes the object with <code>n</code>, the <code>discount</code>, the <code>products</code>&nbsp;and their <code>prices</code>.</li>
	<li><code>double&nbsp;getBill(int[] product, int[] amount)</code>&nbsp;returns the value of the bill and apply the discount if needed. Answers within <code>10^-5</code> of the actual value will be accepted as correct.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;Cashier&quot;,&quot;getBill&quot;,&quot;getBill&quot;,&quot;getBill&quot;,&quot;getBill&quot;,&quot;getBill&quot;,&quot;getBill&quot;,&quot;getBill&quot;]
[[3,50,[1,2,3,4,5,6,7],[100,200,300,400,300,200,100]],[[1,2],[1,2]],[[3,7],[10,10]],[[1,2,3,4,5,6,7],[1,1,1,1,1,1,1]],[[4],[10]],[[7,3],[10,10]],[[7,5,3,1,6,4,2],[10,10,10,9,9,9,7]],[[2,3,5],[5,3,2]]]
<strong>Output</strong>
[null,500.0,4000.0,800.0,4000.0,4000.0,7350.0,2500.0]
<strong>Explanation</strong>
Cashier cashier = new Cashier(3,50,[1,2,3,4,5,6,7],[100,200,300,400,300,200,100]);
cashier.getBill([1,2],[1,2]);                        // return 500.0, bill = 1 * 100 + 2 * 200 = 500.
cashier.getBill([3,7],[10,10]);                      // return 4000.0
cashier.getBill([1,2,3,4,5,6,7],[1,1,1,1,1,1,1]);    // return 800.0, The bill was 1600.0 but as this is the third customer, he has a discount of 50% which means his bill is only 1600 - 1600 * (50 / 100) = 800.
cashier.getBill([4],[10]);                           // return 4000.0
cashier.getBill([7,3],[10,10]);                      // return 4000.0
cashier.getBill([7,5,3,1,6,4,2],[10,10,10,9,9,9,7]); // return 7350.0, Bill was 14700.0 but as the system counted three more customers, he will have a 50% discount and the bill becomes 7350.0
cashier.getBill([2,3,5],[5,3,2]);                    // return 2500.0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^4</code></li>
	<li><code>0 &lt;= discount &lt;= 100</code></li>
	<li><code>1 &lt;= products.length &lt;= 200</code></li>
	<li><code>1 &lt;= products[i] &lt;= 200</code></li>
	<li>There are <strong>not</strong> repeated elements in the array <code>products</code>.</li>
	<li><code>prices.length == products.length</code></li>
	<li><code>1 &lt;= prices[i] &lt;= 1000</code></li>
	<li><code>1 &lt;= product.length &lt;= products.length</code></li>
	<li><code>product[i]</code> exists in <code>products</code>.</li>
	<li><code>amount.length == product.length</code></li>
	<li><code>1 &lt;= amount[i] &lt;= 1000</code></li>
	<li>At most <code>1000</code> calls will be made to <code>getBill</code>.</li>
	<li>Answers within&nbsp;<code>10^-5</code>&nbsp;of the actual value will be accepted as correct.</li>
</ul>
</div>

## Tags
- Design (design)

## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] HashMap/Dictionary
- Author: rock
- Creation Date: Sun Feb 23 2020 00:05:46 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 14 2020 16:56:56 GMT+0800 (Singapore Standard Time)

<p>
```java
    private int cnt = 0, n, discount;
    private Map<Integer, Integer> price = new HashMap<>();
    public Cashier(int n, int discount, int[] products, int[] prices) {
        this.n = n;
        this.discount = discount;
        for (int i = 0; i < products.length; ++i)
            price.put(products[i], prices[i]);
    }
    
    public double getBill(int[] product, int[] amount) {
        double total = 0.0d;
        for (int i = 0; i < product.length; ++i)
            total += price.get(product[i]) * amount[i];
        return total * (++cnt % n == 0 ? 1 - discount / 100d : 1);
    }
```
```python
    def __init__(self, n: int, discount: int, products: List[int], prices: List[int]):
        self.price = dict(zip(products, prices)) # here credit to @danielmittereder for making code clean.
        self.discount = discount
        self.n = n
        self.cnt = 0

    def getBill(self, product: List[int], amount: List[int]) -> float:
        self.cnt += 1
        total = 0.0
        for i, p in enumerate(product):
            total += self.price[p] * amount[i]
        return total * (1 - self.discount / 100 if self.cnt % self.n == 0 else 1)    
```
</p>


### [C++] Hashmap
- Author: orangezeit
- Creation Date: Sun Feb 23 2020 00:02:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 00:02:48 GMT+0800 (Singapore Standard Time)

<p>
```
class Cashier {
public:
    unordered_map<int, int> tags;
    int cnt, mod;
    double pct;
    
    Cashier(int n, int discount, vector<int>& products, vector<int>& prices) {
        cnt = 0;
        mod = n;
        pct = (100.0 - discount) / 100.0;
        for (int i = 0; i < products.size(); ++i)
            tags[products[i]] = prices[i];
    }
    
    double getBill(vector<int> product, vector<int> amount) {
        cnt++;
        double ans(0.0);
        for (int i = 0; i < product.size(); ++i)
            ans += tags[product[i]] * amount[i];
        return cnt % mod ? ans : ans * pct;
    }
};
```
</p>


### [Java] Straight-forward Store price in Hashmap
- Author: manrajsingh007
- Creation Date: Sun Feb 23 2020 00:04:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 00:28:21 GMT+0800 (Singapore Standard Time)

<p>
```
class Cashier {
    static int n;
    static int discount;
    static HashMap<Integer, Integer> prices;
    static int counter;
    public Cashier(int n, int discount, int[] products, int[] prices) {
        this.prices = new HashMap<>();
        this.n = n;
        counter = n;
        this.discount = discount;
        for(int i = 0; i < prices.length; i++) this.prices.put(products[i], prices[i]);
    }
    public double getBill(int[] product, int[] amount) {
        counter--;
        double ans = 0;
        for(int i = 0; i < product.length; i++) {
            ans += prices.get(product[i]) * amount[i];
        }
        if(counter == 0) {
            counter = n;
            ans -= (discount * ans) / (100.0);
        }
        return ans;
    }
}
</p>


