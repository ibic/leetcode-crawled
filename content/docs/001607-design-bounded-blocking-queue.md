---
title: "Design Bounded Blocking Queue"
weight: 1607
#id: "design-bounded-blocking-queue"
---
## Description
<div class="description">
<p>Implement a thread safe bounded&nbsp;blocking queue that has&nbsp;the following&nbsp;methods:</p>

<ul>
	<li><code>BoundedBlockingQueue(int capacity)</code> The constructor initializes the queue with a maximum <code>capacity</code>.</li>
	<li><code>void enqueue(int element)</code> Adds an <code>element</code> to the front of the queue. If the queue is full, the calling thread is blocked until the queue is no longer full.</li>
	<li><code>int dequeue()</code> Returns the element at the rear of the queue and removes it. If the queue is empty, the calling thread is blocked until the queue is no longer empty.</li>
	<li><code>int size()</code>&nbsp;Returns the number of elements currently in the queue.</li>
</ul>

<p>Your implementation will be tested using multiple threads at the same time. Each thread will either be a producer thread that only makes calls to the&nbsp;<code>enqueue</code>&nbsp;method or a consumer thread that only makes calls to the&nbsp;<code>dequeue</code>&nbsp;method. The&nbsp;<code>size</code>&nbsp;method will be called after every test case.</p>

<p>Please do not use built-in implementations of bounded&nbsp;blocking queue as this will not be accepted in an interview.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong>
1
1
[&quot;BoundedBlockingQueue&quot;,&quot;enqueue&quot;,&quot;dequeue&quot;,&quot;dequeue&quot;,&quot;enqueue&quot;,&quot;enqueue&quot;,&quot;enqueue&quot;,&quot;enqueue&quot;,&quot;dequeue&quot;]
[[2],[1],[],[],[0],[2],[3],[4],[]]

<strong>Output:</strong>
[1,0,2,2]

<strong>Explanation:
</strong>Number of producer threads = 1
Number of consumer threads = 1

BoundedBlockingQueue queue = new BoundedBlockingQueue(2);   // initialize the queue with capacity = 2.

queue.enqueue(1);   // The producer thread enqueues 1 to the queue.
queue.dequeue();    // The consumer thread calls dequeue and returns 1 from the queue.
queue.dequeue();    // Since the queue is empty, the consumer thread is blocked.
queue.enqueue(0);   // The producer thread enqueues 0 to the queue. The consumer thread is unblocked and returns 0 from the queue.
queue.enqueue(2);   // The producer thread enqueues 2 to the queue.
queue.enqueue(3);   // The producer thread enqueues 3 to the queue.
queue.enqueue(4);   // The producer thread is blocked because the queue&#39;s capacity (2) is reached.
queue.dequeue();    // The consumer thread returns 2 from the queue. The producer thread is unblocked and enqueues 4 to the queue.
queue.size();       // 2 elements remaining in the queue. size() is always called at the end of each test case.
</pre>

<p>&nbsp;</p>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong>
3
4
[&quot;BoundedBlockingQueue&quot;,&quot;enqueue&quot;,&quot;enqueue&quot;,&quot;enqueue&quot;,&quot;dequeue&quot;,&quot;dequeue&quot;,&quot;dequeue&quot;,&quot;enqueue&quot;]
[[3],[1],[0],[2],[],[],[],[3]]

<strong>Output:</strong>
[1,0,2,1]

<strong>Explanation:
</strong>Number of producer threads = 3
Number of consumer threads = 4

BoundedBlockingQueue queue = new BoundedBlockingQueue(3);   // initialize the queue with capacity = 3.

queue.enqueue(1);   // Producer thread P1 enqueues 1 to the queue.
queue.enqueue(0);   // Producer thread P2 enqueues 0 to the queue.
queue.enqueue(2);   // Producer thread P3 enqueues 2 to the queue.
queue.dequeue();    // Consumer thread C1 calls dequeue.
queue.dequeue();    // Consumer thread C2 calls dequeue.
queue.dequeue();    // Consumer thread C3 calls dequeue.
queue.enqueue(3);   // One of the producer threads enqueues 3 to the queue.
queue.size();       // 1 element remaining in the queue.

Since the number of threads for producer/consumer is greater than 1, we do not know how the threads will be scheduled in the operating system, even though the input seems to imply the ordering. Therefore, any of the output [1,0,2] or [1,2,0] or [0,1,2] or [0,2,1] or [2,0,1] or [2,1,0] will be accepted.</pre>

</div>

## Tags


## Companies
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: true)
- Cloudera - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java ReentrantLock + Condition Solution
- Author: yrq
- Creation Date: Thu Sep 12 2019 03:24:40 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 01 2019 01:50:29 GMT+0800 (Singapore Standard Time)

<p>
ReentrantLock = Voltaile + CAS + Condition
Condition = LinkedNode which contains Waiting Thread
In same ReentrantLock, we can create multiple Conditions
For this question, blockingqueue need 2 waiting list ( full / empty)
When queue is empty, we block dequeue thread, and add thread to empty waiting list
When queue is full, block enqueue thread, add thread to full waiting list

Lock  --- --- >   empty condition ->  blocked Thread1 -> blocked Thread2
		    |
	    	| --- >   full condition  -> blocked Thread 3 -> blocked Thread4

Signal = Notify
SignalAll = NotifyAll
Await = Wait

Same as BlockingQueue implmentation in Java source code.

```
import java.util.concurrent.locks.ReentrantLock; 
import java.util.concurrent.locks.Condition; 

class BoundedBlockingQueue {
    private ReentrantLock lock = new ReentrantLock();
    private Condition full = lock.newCondition();
    private Condition empty = lock.newCondition();
    private int[] queue;
    private int tail = 0;
    private int head = 0;
    private int size = 0;
    public BoundedBlockingQueue(int capacity) {
        queue = new int[capacity];
    }
    
    public void enqueue(int element) throws InterruptedException {
		lock.lock();
        try {
            while(size == queue.length) {
                full.await();
            }
            queue[tail++] = element;
            tail %= queue.length;
            size++;
            empty.signal();
        } finally {
            lock.unlock();
        }
    }
    
    public int dequeue() throws InterruptedException {
		lock.lock();
        try {
            while(size == 0) {
                empty.await();
            }
            int res = queue[head++];
            head %= queue.length;
            size--;
            full.signal();
            return res;
        } finally {
            lock.unlock();
        }
    }
    
    public int size() throws InterruptedException {
        lock.lock();
		try {
			return this.size;
		} finally {
			lock.unlock();
		}
    }
}
```
</p>


### Java 2 semaphores and thread safe queue
- Author: petitout
- Creation Date: Fri Oct 11 2019 12:28:10 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 11 2019 12:28:10 GMT+0800 (Singapore Standard Time)

<p>
I found usage of Semaphore way more elegant than lock or, worst, wait and notifyAll.

Of course you have to use a collection that is thread safe here because, up to capacity number of concurrent threads would are able to add values into the queue.


```
class BoundedBlockingQueue {

    private Semaphore enq;

    private Semaphore deq;

    ConcurrentLinkedDeque<Integer> q;

    public BoundedBlockingQueue(int capacity) {
        q =  new ConcurrentLinkedDeque<>();
        enq = new Semaphore(capacity);
        deq = new Semaphore(0);
    }

    public void enqueue(int element) throws InterruptedException {
        enq.acquire();
        q.add(element);
        deq.release();
    }

    public int dequeue() throws InterruptedException {
        deq.acquire();
        int val = q.poll();
        enq.release();
        return val;
    }

    public int size() {
        return q.size();
    }
}
```
</p>


### Python3 Semaphore Solution
- Author: kuznecpl
- Creation Date: Thu Sep 12 2019 04:51:35 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 12 2019 05:01:20 GMT+0800 (Singapore Standard Time)

<p>
The idea is to use two semaphores, pushing and pulling, to maintain the invariants of the queue.
Initially, no thread can dequeue (self.pulling.acquire()) until a thread has enqueued (self.pulling.release()).
When capacity elements have been enqueued, self.pushing.acquire() will block the thread until a dequeue releases the semaphore again.
Additionally, use a Lock so only a single thread can modify the actual queue at once.

```
import threading

class BoundedBlockingQueue(object):
    def __init__(self, capacity: int):
        self.capacity = capacity
        
        self.pushing = threading.Semaphore(capacity)
        self.pulling = threading.Semaphore(0)
        self.editing = threading.Lock()
      
        self.queue = collections.deque()

    def enqueue(self, element: int) -> None:
      self.pushing.acquire()
      self.editing.acquire()
      
      self.queue.append(element)
      
      self.editing.release()
      self.pulling.release()

    def dequeue(self) -> int:
        self.pulling.acquire()
        self.editing.acquire()
        
        res = self.queue.popleft()
        
        self.editing.release()
        self.pushing.release()
        return res

    def size(self) -> int:
      return len(self.queue)
```
</p>


