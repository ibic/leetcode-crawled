---
title: "Solve the Equation"
weight: 573
#id: "solve-the-equation"
---
## Description
<div class="description">
<p>
Solve a given equation and return the value of <code>x</code> in the form of string "x=#value". The equation contains only '+', '-' operation, the variable <code>x</code> and its coefficient.
</p>

<p>
If there is no solution for the equation, return "No solution".
</p>
<p>
If there are infinite solutions for the equation, return "Infinite solutions".
</p>
<p>
If there is exactly one solution for the equation, we ensure that the value of <code>x</code> is an integer.
</p>

<p><b>Example 1:</b><br/>
<pre>
<b>Input:</b> "x+5-3+x=6+x-2"
<b>Output:</b> "x=2"
</pre>
</p>

<p><b>Example 2:</b><br/>
<pre>
<b>Input:</b> "x=x"
<b>Output:</b> "Infinite solutions"
</pre>
</p>

<p><b>Example 3:</b><br/>
<pre>
<b>Input:</b> "2x=x"
<b>Output:</b> "x=0"
</pre>
</p>

<p><b>Example 4:</b><br/>
<pre>
<b>Input:</b> "2x+3x-6x=x+2"
<b>Output:</b> "x=-1"
</pre>
</p>

<p><b>Example 5:</b><br/>
<pre>
<b>Input:</b> "x=x+2"
<b>Output:</b> "No solution"
</pre>
</p>
</div>

## Tags
- Math (math)

## Companies
- Amazon - 3 (taggedByAdmin: true)
- Google - 5 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Partioning Coefficients [Accepted]

In the current approach, we start by splitting the given $$equation$$ based on `=` sign. This way, we've separated the left and right hand side of this equation. Once this is done, we need to extract the individual elements(i.e. `x`'s and the numbers) from both sides of the equation. To do so, we make use of `breakIt` function, in which we traverse over the given equation(either left hand side or right hand side), and put the separated parts into an array. 

Now, the idea is as follows. We treat the given equation as if we're bringing all the `x`'s on the left hand side and all the rest of the numbers on the right hand side as done below for an example.

`x+5-3+x=6+x-2`

`x+x-x=6-2-5+3`

Thus, every `x` in the left hand side of the given equation is treated as positive, while that on the right hand side is treated as negative, in the current implementation. 

Likewise, every number on the left hand side is treated as negative, while that on the right hand side is treated as positive. Thus, by doing so, we obtain all the `x`'s in the new $$lhs$$ and all the numbers in the new $$rhs$$ of the original equation. 

Further, in case of an `x`, we also need to find its corresponding coefficients in order to evaluate the final effective coefficient of `x` on the left hand side. We also evaluate the final effective number on the right hand side as well.

Now, in case of a unique solution, the ratio of the effective $$rhs$$ and $$lhs$$ gives the required result. In case of infinite solutions, both the effective $$lhs$$ and $$rhs$$ turns out to be zero e.g. `x+1=x+1`. In case of no solution, the coefficient of `x`($$lhs$$) turns out to be zero, but the effective number on the $$rhs$$ is non-zero.


<iframe src="https://leetcode.com/playground/5qsPscf9/shared" frameBorder="0" name="5qsPscf9" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Generating coefficients and findinn $$lhs$$ and $$rhs$$ will take $$O(n)$$.

* Space complexity : $$O(n)$$. ArrayList $$res$$ size can grow upto $$n$$.

---
#### Approach #2 Using regex for spliting [Accepted]

**Algorithm**

In the last approach, we made use of a new function `breakIt` to obtain the individual components of either the left hand side or the right hand side. Instead of doing so, we can also make use of splitting based on `+` or `-` sign, to obtain the individual elements. The rest of the process remains the same as in the last approach. 

In order to do the splitting, we make use of an expression derived from regular expressions(regex). Simply speaking, regex is a functionality used to match a target string based on some given criteria. The ?=n quantifier, in regex, matches any string that is followed by a specific string $$n$$. What it's saying is that the captured match must be followed by $$n$$ but the $$n$$ itself isn't captured.

By making use of this kind of expression in the `split` functionality, we make sure that the partitions are obtained such that the `+` or `-` sign remains along with the parts(numbers or coefficients) even after the splitting.

<iframe src="https://leetcode.com/playground/9JbHjYgz/shared" frameBorder="0" name="9JbHjYgz" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Generating coefficients and finding $$lhs$$ and $$rhs$$ will take $$O(n)$$.

* Space complexity : $$O(n)$$. ArrayList $$res$$ size can grow upto $$n$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise Java Solution
- Author: compton_scatter
- Creation Date: Sun Jul 09 2017 11:02:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 04:41:54 GMT+0800 (Singapore Standard Time)

<p>
```
public String solveEquation(String equation) {
    int[] res = evaluateExpression(equation.split("=")[0]),  
  	  res2 = evaluateExpression(equation.split("=")[1]);
    res[0] -= res2[0];
    res[1] = res2[1] - res[1];
    if (res[0] == 0 && res[1] == 0) return "Infinite solutions";
    if (res[0] == 0) return "No solution";
    return "x=" + res[1]/res[0];
}  

public int[] evaluateExpression(String exp) {
    String[] tokens = exp.split("(?=[-+])"); 
    int[] res =  new int[2];
    for (String token : tokens) {
        if (token.equals("+x") || token.equals("x")) res[0] += 1;
	else if (token.equals("-x")) res[0] -= 1;
	else if (token.contains("x")) res[0] += Integer.parseInt(token.substring(0, token.indexOf("x")));
	else res[1] += Integer.parseInt(token);
    }
    return res;
}
```
</p>


### Simple 2-liner (and more)
- Author: StefanPochmann
- Creation Date: Sun Jul 09 2017 18:20:18 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 12 2018 22:23:56 GMT+0800 (Singapore Standard Time)

<p>
```
def solve_equation(equation)
  a, x = eval('i = 1i;' + equation.gsub('x', 'i').sub('=', '-(') + ')').rect
  "x=#{-a/x}" rescue a != 0 ? 'No solution' : 'Infinite solutions'
end
```
It's easy to change an equation like `2+3x=5x-7` to an expression for a complex number like `2+3i-(5i-7)`. Then I let Ruby evaluate that and it gives me the total of "x numbers" and the total of "non-x numbers" as the imaginary and real part of the complex number.

Same in Python:

    def solveEquation(self, equation):
        z = eval(equation.replace('x', 'j').replace('=', '-(') + ')', {'j': 1j})
        a, x = z.real, -z.imag
        return 'x=%d' % (a / x) if x else 'No solution' if a else 'Infinite solutions'

And here's a completely different one using a regular expression to parse the tokens:

    def solveEquation(self, equation):
        x = a = 0
        side = 1
        for eq, sign, num, isx in re.findall('(=)|([-+]?)(\d*)(x?)', equation):
            if eq:
                side = -1
            elif isx:
                x += side * int(sign + '1') * int(num or 1)
            elif num:
                a -= side * int(sign + num)
        return 'x=%d' % (a / x) if x else 'No solution' if a else 'Infinite solutions'
</p>


### Clear Java Code with Detailed Example
- Author: GraceMeng
- Creation Date: Sun Jul 15 2018 16:40:31 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 19 2018 10:53:33 GMT+0800 (Singapore Standard Time)

<p>
Thanks @compton_scatter for the original post.

**Example**
e.g. x+5-3+x=6+x-2
* Firstly, we split the equation by "=":
`leftPart` is x+5-3+x;
`rightPart` is 6+x-2;
* Secondly, we sum up coefficient and the rest numbers separately, i.e.
`leftVals` is 2x + 2, i.e., [2, 2];
`rightVals` is x + 4, i.e., [1, 4];
* Thirdly, we solve the simplified equation by moving all elements to the left of "=",
`cntX = leftVals[0] - rightVals[0]; `, i.e., 2 - 1 = 1,
`cntNum = leftVals[1] - rightVals[1]; `, i.e., 2 - 4 = -2,
`cntX * x + cntNum = 0`, i.e., 1 * x + (-2) = 0,
`x = (-cntNum) / cntX`, i.e., x = 2

**Please note that**
`exp.split("");` split exp by 0-length string ("a+b-c" to "a", "+", "b", "-", "c")
`exp.split("(?=[-+])");`split exp by 0-length string only if they are follow by "-" or "+" ("a+b-c" to "a", "+b", "-c")
 
**Clear Java Code**
```
    public String solveEquation(String equation) {

        String[] parts = equation.split("=");
        String leftPart = parts[0], rightPart = parts[1];
        int[] leftVals = evaluate(leftPart), rightVals = evaluate(rightPart);
        int cntX = leftVals[0] - rightVals[0]; 
        int cntNum = leftVals[1] - rightVals[1]; 
        if (cntX == 0) {
            if (cntNum != 0)
                return "No solution";
            return "Infinite solutions";
        }
        int valX = (-cntNum) / cntX;
        StringBuilder result = new StringBuilder();
        result.append("x=").append(valX);

        return result.toString();
    }

    private int[] evaluate(String exp) {
        
        int[] result = new int[2];
        String[] expElements = exp.split("(?=[-+])");
        
        for (String ele : expElements) {
            if (ele.equals("+x") || ele.equals("x")) {
                result[0]++;
            } else if (ele.equals("-x")) {
                result[0]--;
            } else if (ele.contains("x")) {
                result[0] += Integer.valueOf(ele.substring(0, ele.indexOf("x")));
            } else {
                result[1] += Integer.valueOf(ele);
            }
        }
        
        return result;
    }

```
**I would appreciate your VOTE UP ;)**
</p>


