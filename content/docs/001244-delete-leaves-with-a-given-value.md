---
title: "Delete Leaves With a Given Value"
weight: 1244
#id: "delete-leaves-with-a-given-value"
---
## Description
<div class="description">
<p>Given a binary tree&nbsp;<code>root</code>&nbsp;and an integer&nbsp;<code>target</code>, delete all the&nbsp;<strong>leaf nodes</strong>&nbsp;with value <code>target</code>.</p>

<p>Note&nbsp;that once you delete a leaf node with value <code>target</code><strong>,&nbsp;</strong>if it&#39;s parent node becomes a leaf node and has the value <code><font face="monospace">target</font></code>, it should also be deleted (you need to continue doing that until you can&#39;t).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/01/09/sample_1_1684.png" style="width: 550px; height: 120px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [1,2,3,2,null,2,4], target = 2
<strong>Output:</strong> [1,null,3,null,4]
<strong>Explanation:</strong> Leaf nodes in green with value (target = 2) are removed (Picture in left). 
After removing, new nodes become leaf nodes with value (target = 2) (Picture in center).
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/01/09/sample_2_1684.png" style="width: 300px; height: 120px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [1,3,3,3,2], target = 3
<strong>Output:</strong> [1,3,null,null,2]
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/01/15/sample_3_1684.png" style="width: 420px; height: 150px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [1,2,null,2,null,2], target = 2
<strong>Output:</strong> [1]
<strong>Explanation:</strong> Leaf nodes in green with value (target = 2) are removed at each step.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root = [1,1,1], target = 1
<strong>Output:</strong> []
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> root = [1,2,3], target = 1
<strong>Output:</strong> [1,2,3]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= target&nbsp;&lt;= 1000</code></li>
	<li>The&nbsp;given binary tree will have between&nbsp;<code>1</code>&nbsp;and&nbsp;<code>3000</code>&nbsp;nodes.</li>
	<li>Each node&#39;s value is between <code>[1, 1000]</code>.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 2 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Recursion Solution
- Author: lee215
- Creation Date: Sun Jan 19 2020 12:02:08 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 21 2020 14:48:44 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Recursively call removeLeafNodes on the left and right.
If `root.left == root.right == null` and `root.val == target`,
the `root` node is now a leaf with value = target, we return `null`.
Otherwise return `root` node itself.

Can be compressed into 1-line but too long.
<br>


## **Note**
Leetcode tree problem usually states it as
"the number of nodes in the given tree is between 1 and 10^4"
in order to avoid any unclear null case.
Well after @awice,
leetcode don\'t stay consistent with the same good old format for constrains.
And I didn\'t check nullity of the root.
<br>

## **Complexity**
Time `O(N)`
Space `O(height)` for recursion.
<br>

**Java:**
```java
    public TreeNode removeLeafNodes(TreeNode root, int target) {
        if (root.left != null) root.left = removeLeafNodes(root.left, target);
        if (root.right != null) root.right = removeLeafNodes(root.right, target);
        return root.left == root.right && root.val == target ? null : root;
    }
```

**C++:**
```cpp
    TreeNode* removeLeafNodes(TreeNode* root, int target) {
        if (root->left) root->left = removeLeafNodes(root->left, target);
        if (root->right) root->right = removeLeafNodes(root->right, target);
        return root->left == root->right && root->val == target ? nullptr : root;
    }
```

**Python:**
```python
    def removeLeafNodes(self, root, target):
        if root.left: root.left = self.removeLeafNodes(root.left, target)
        if root.right: root.right = self.removeLeafNodes(root.right, target)
        return None if root.left == root.right and root.val == target else root
```

</p>


### [Python] Simple recursion
- Author: kaiwensun
- Creation Date: Sun Jan 19 2020 12:03:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 19 2020 12:03:14 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def removeLeafNodes(self, root, target):
        if root:
            root.left = self.removeLeafNodes(root.left, target)
            root.right = self.removeLeafNodes(root.right, target)
            if root.val != target or root.left or root.right:
                return root
```
</p>


### Python simple
- Author: StefanPochmann
- Creation Date: Sun Jan 19 2020 12:03:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 19 2020 13:55:03 GMT+0800 (Singapore Standard Time)

<p>
First do it in the subtrees, then check the current root itself. Can handle the empty tree as well. Although LeetCode doesn\'t test that, despite not ruling it out. But my it also makes my code shorter.

One little trick is detecting leaves with `left is right` (which is shorter than `not left and not right`). It can only be true if both are `None` (they can\'t be nodes, as they\'d have to be the same node, and that\'s not allowed in a tree). Edit: For the "*not* leaf" check I first had `is not`, changed to shorter and clearer `or` (thanks [@kaiwensun\'s solution](https://leetcode.com/problems/delete-leaves-with-a-given-value/discuss/484286/Python-Simple-recursion)).

O(n) time, O(height) space (for call stack)
```
def removeLeafNodes(self, root, target):
    if root:
        root.left = self.removeLeafNodes(root.left, target)
        root.right = self.removeLeafNodes(root.right, target)
        if root.val != target or root.left or root.right:
            return root
```
Maybe slightly clearer ending:
```
def removeLeafNodes(self, root, target):
    if root:
        root.left = self.removeLeafNodes(root.left, target)
        root.right = self.removeLeafNodes(root.right, target)
        if root.val == target and root.left is root.right:
            return None
        return root
```
</p>


