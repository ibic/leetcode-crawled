---
title: "Freedom Trail"
weight: 485
#id: "freedom-trail"
---
## Description
<div class="description">
<p>In the video game Fallout 4, the quest &quot;Road to Freedom&quot; requires players to reach a metal dial called the &quot;Freedom Trail Ring&quot;, and use the dial to spell a specific keyword in order to open the door.</p>

<p>Given a string <b>ring</b>, which represents the code engraved on the outer ring and another string <b>key</b>, which represents the keyword needs to be spelled. You need to find the <b>minimum</b> number of steps in order to spell all the characters in the keyword.</p>

<p>Initially, the first character of the <b>ring</b> is aligned at 12:00 direction. You need to spell all the characters in the string <b>key</b> one by one by rotating the ring clockwise or anticlockwise to make each character of the string <b>key</b> aligned at 12:00 direction and then by pressing the center button.</p>

<p>At the stage of rotating the ring to spell the key character <b>key[i]</b>:</p>

<ol>
	<li>You can rotate the <b>ring</b> clockwise or anticlockwise <b>one place</b>, which counts as 1 step. The final purpose of the rotation is to align one of the string <b>ring&#39;s</b> characters at the 12:00 direction, where this character must equal to the character <b>key[i]</b>.</li>
	<li>If the character <b>key[i]</b> has been aligned at the 12:00 direction, you need to press the center button to spell, which also counts as 1 step. After the pressing, you could begin to spell the next character in the key (next stage), otherwise, you&#39;ve finished all the spelling.</li>
</ol>

<p><b>Example:</b></p>

<center><img src="https://assets.leetcode.com/uploads/2018/10/22/ring.jpg" style="width: 26%;" /></center>
&nbsp;

<pre>
<b>Input:</b> ring = &quot;godding&quot;, key = &quot;gd&quot;
<b>Output:</b> 4
<b>Explanation:</b>
For the first key character &#39;g&#39;, since it is already in place, we just need 1 step to spell this character. 
For the second key character &#39;d&#39;, we need to rotate the ring &quot;godding&quot; anticlockwise by two steps to make it become &quot;ddinggo&quot;.
Also, we need 1 more step for spelling.
So the final output is 4.
</pre>

<p><b>Note:</b></p>

<ol>
	<li>Length of both ring and <b>key</b> will be in range 1 to 100.</li>
	<li>There are only lowercase letters in both strings and might be some duplcate characters in both strings.</li>
	<li>It&#39;s guaranteed that string <b>key</b> could always be spelled by rotating the string <b>ring</b>.</li>
</ol>

</div>

## Tags
- Divide and Conquer (divide-and-conquer)
- Dynamic Programming (dynamic-programming)
- Depth-first Search (depth-first-search)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise Java DP Solution
- Author: shawngao
- Creation Date: Sun Mar 05 2017 12:08:38 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 01:15:57 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public int findRotateSteps(String ring, String key) {
        int n = ring.length();
        int m = key.length();
        int[][] dp = new int[m + 1][n];
        
        for (int i = m - 1; i >= 0; i--) {
            for (int j = 0; j < n; j++) {
                dp[i][j] = Integer.MAX_VALUE;
                for (int k = 0; k < n; k++) {
                    if (ring.charAt(k) == key.charAt(i)) {
                        int diff = Math.abs(j - k);
                        int step = Math.min(diff, n - diff);
                        dp[i][j] = Math.min(dp[i][j], step + dp[i + 1][k]);
                    }
                }
            }
        }
        
        return dp[0][0] + m;
    }
}
```
</p>


### Evolve from brute force to dp
- Author: yu6
- Creation Date: Mon Mar 13 2017 05:37:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 10 2018 00:17:08 GMT+0800 (Singapore Standard Time)

<p>
1. brute force, try all next steps.
```
    int findRotateSteps(string ring, string key) {
        vector<int> pos[26];
        for(int i=0;i<ring.size();i++) pos[ring[i]-'a'].push_back(i);
        return findSteps(0, 0, ring, key, pos);    
    }
    int findSteps(int p1, int p2, string &ring, string &key, vector<int> pos[26]) {
        if(p2==key.size()) return 0;
        int r = ring.size(), ms=INT_MAX;
        for(int nxt:pos[key[p2]-'a']) {
            int dist = abs(p1-nxt);
            ms = min(ms,min(dist, r-dist)+findSteps(nxt,p2+1,ring,key,pos));    
        }
        return ms+1;
    }
```
2. O(kr^2) Memoization. There are overlapping sub-problems in #1. We only need to process a substring of key once.
```
    int findRotateSteps(string ring, string key) {
        vector<int> pos[26];
        int r = ring.size();
        for(int i=0;i<r;i++) pos[ring[i]-'a'].push_back(i);
        vector<vector<int>> mem(r,vector<int>(key.size()));
        return findSteps(0, 0, ring, key, pos,mem);    
    }
    int findSteps(int p1, int p2, string &ring, string &key, vector<int> pos[26],vector<vector<int>>& mem) {
        if(p2==key.size()) return 0;
        if(mem[p1][p2]) return mem[p1][p2];
        int r = ring.size(), ms=INT_MAX;
        for(int nxt:pos[key[p2]-'a']) {
            int dist = abs(p1-nxt);
            ms = min(ms,min(dist, r-dist)+findSteps(nxt,p2+1,ring,key,pos,mem));    
        }
        return mem[p1][p2]=ms+1;
    }
```
3. O(kr^2) dp
```
    int findRotateSteps(string ring, string key) {
        vector<int> pos[26];
        int r = ring.size(), k = key.size();
        for(int i=0;i<r;i++) pos[ring[i]-'a'].push_back(i);
        vector<vector<int>> dp(k+1,vector<int>(r,INT_MAX));
        dp[k].assign(r,0);
        for(int i=k-1;i>=0;i--) 
            for(int j=0;j<r;j++)
                for(int nxt:pos[key[i]-'a']) {
                    int dist = abs(j-nxt);
                    dp[i][j]=min(dp[i][j],min(dist,r-dist)+dp[i+1][nxt]);
                }
        return dp[0][0]+k;
    }
```
4. linear space dp
```
    int findRotateSteps(string ring, string key) {
        vector<int> pos[26];
        int r = ring.size(), k = key.size();
        for(int i=0;i<r;i++) pos[ring[i]-'a'].push_back(i);
        vector<int> pre(r), cur(r,INT_MAX), *p_pre = &pre, *p_cur = &cur;
        for(int i=k-1;i>=0;i--) {
            for(int j=0;j<r;j++)
                for(int nxt:pos[key[i]-'a']) {
                    int dist = abs(j-nxt);
                    (*p_cur)[j]=min((*p_cur)[j],min(dist,r-dist)+(*p_pre)[nxt]);
                }
            swap(p_pre,p_cur);
            p_cur->assign(r,INT_MAX);
        }
        return (*p_pre)[0]+k;
    }
```
</p>


### Super clear DFS + memorization solution
- Author: wzx520zm
- Creation Date: Thu Jun 29 2017 03:23:20 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 11:23:45 GMT+0800 (Singapore Standard Time)

<p>

```
public class Solution {
    public int findRotateSteps(String ring, String key) {
           Map<String,Integer> map = new HashMap();
           return dfs(ring, key, 0, map);
    }
    
    public int dfs(String ring, String key, int index, Map<String,Integer> map){
        if(index == key.length()){
            return 0;
        }
    
        char c = key.charAt(index);
        String hashKey = ring + index;
        if(map.containsKey(hashKey)) return map.get(hashKey);
        
        int minSteps = Integer.MAX_VALUE;
        for(int i = 0; i < ring.length(); i ++){
            if(ring.charAt(i) == c){
                String s = ring.substring(i, ring.length()) + ring.substring(0, i);
                int steps = 1 + Math.min(i, ring.length() - i);
                steps += dfs(s, key, index + 1, map);
                minSteps = Math.min(minSteps, steps);
            }
        }
        
        map.put(hashKey, minSteps);
        
        return minSteps;
    }
}
````
</p>


