---
title: "Add Binary"
weight: 67
#id: "add-binary"
---
## Description
<div class="description">
<p>Given two binary strings, return their sum (also a binary string).</p>

<p>The input strings are both <strong>non-empty</strong> and contains only characters <code>1</code> or&nbsp;<code>0</code>.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> a = &quot;11&quot;, b = &quot;1&quot;
<strong>Output:</strong> &quot;100&quot;</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> a = &quot;1010&quot;, b = &quot;1011&quot;
<strong>Output:</strong> &quot;10101&quot;</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>Each string consists only of <code>&#39;0&#39;</code> or <code>&#39;1&#39;</code> characters.</li>
	<li><code>1 &lt;= a.length, b.length &lt;= 10^4</code></li>
	<li>Each string is either <code>&quot;0&quot;</code> or doesn&#39;t contain any leading zero.</li>
</ul>

</div>

## Tags
- Math (math)
- String (string)

## Companies
- Facebook - 64 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

There is a simple way using built-in functions:

- Convert a and b into integers.

- Compute the sum.

- Convert the sum back into binary form.

<iframe src="https://leetcode.com/playground/csdNVNdg/shared" frameBorder="0" width="100%" height="140" name="csdNVNdg"></iframe>

The overall algorithm has $$\mathcal{O}(N + M)$$ time complexity 
and has two drawbacks which could be used against you during the interview.

> 1 . In Java this approach is limited by the length of the input strings a and b.
Once the string is long enough, the result of conversion into 
integers will not fit into Integer, Long or BigInteger:

- 33 1-bits - and b doesn't fit into Integer.

- 65 1-bits - and b doesn't fit into Long.

- [500000001](https://docs.oracle.com/javase/8/docs/api/java/math/BigInteger.html) 1-bits - and b doesn't fit into BigInteger.

To fix the issue, one could use standard Bit-by-Bit Computation approach
which is suitable for quite long input strings.

> 2 . This method has quite low performance in the case of large input 
numbers. 

One could use Bit Manipulation approach to speed up the solution. 
<br /> 
<br />


---
#### Approach 1: Bit-by-Bit Computation

**Algorithm**

That's a good old classical algorithm, and there is no
conversion from binary string to decimal and back here. Let's consider the
numbers bit by bit starting from the lowest one and 
compute the carry this bit will add. 

Start from carry = 0. 
If number a has 1-bit in this lowest bit, add 1 to the carry.
The same for number b: if number b has 1-bit in the lowest bit, 
add 1 to the carry. At this point the carry for the lowest bit could
be equal to $$(00)_2$$, $$(01)_2$$, or $$(10)_2$$.

Now append the lowest bit of the carry to the answer, and 
move the highest bit of the carry to the next order bit.  

Repeat the same steps again, and again, till all bits in a and b
are used up. If there is still nonzero carry to add, add it. 
Now reverse the answer string and the job is done.

!?!../Documents/67_LIS.json:1000,387!?!

**Implementation**

<iframe src="https://leetcode.com/playground/9eeUaMno/shared" frameBorder="0" width="100%" height="480" name="9eeUaMno"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(\max(N, M))$$, 
where $$N$$ and $$M$$ are lengths of the input strings a and b.

* Space complexity: $$\mathcal{O}(\max(N, M))$$ to keep the answer.
<br /> 
<br />


---
#### Approach 2: Bit Manipulation
 
**Intuition**
 
Here the input is more adapted to push towards Approach 1,
but there is popular Facebook variation of this problem 
when interviewer provides you two numbers and 
asks to sum them up without using addition operation.  

> No addition? OK, bit manipulation then.

How to start? There is an interview tip for bit manipulation problems:
if you don't know how to start, start from computing XOR for your input data.
Strangely, that helps to go out for quite a lot of problems,
[Single Number II](https://leetcode.com/articles/single-number-ii/),
[Single Number III](https://leetcode.com/articles/single-number-iii/),
[Maximum XOR of Two Numbers in an Array](https://leetcode.com/articles/maximum-xor-of-two-numbers-in-an-array/),
[Repeated DNA Sequences](https://leetcode.com/articles/repeated-dna-sequences/),
[Maximum Product of Word Lengths](https://leetcode.com/articles/maximum-product-of-word-lengths/),
etc.

Here XOR is a key as well, because it's a sum of two binaries 
without taking carry into account.

![fig](../Figures/67/xor4.png) 

To find current carry is quite easy as well, it's AND of two input numbers,
shifted one bit to the left. 

![fig](../Figures/67/carry2.png)

Now the problem is reduced: one has to find the sum of 
answer without carry and carry. It's the same problem - to sum two numbers,
and hence one could solve it in a loop with the condition statement 
"while carry is not equal to zero". 

**Algorithm**

- Convert a and b into integers x and y, 
x will be used to keep an answer, and y for the carry.

- While carry is nonzero: `y != 0`:

    - Current answer without carry is XOR of x and y: `answer = x^y`.
    
    - Current carry is left-shifted AND of x and y: `carry = (x & y) << 1`.
    
    - Job is done, prepare the next loop: `x = answer`, `y = carry`.
    
- Return x in the binary form.

**Implementation**

<iframe src="https://leetcode.com/playground/yL3qxhji/shared" frameBorder="0" width="100%" height="327" name="yL3qxhji"></iframe>

This solution could be written as 4-liner in Python

<iframe src="https://leetcode.com/playground/FmSbY53h/shared" frameBorder="0" width="100%" height="157" name="FmSbY53h"></iframe>

**Performance Discussion**

Here we deal with input numbers which are greater than $$2^{100}$$. 
That forces to use slow [BigInteger](https://docs.oracle.com/javase/8/docs/api/java/math/BigInteger.html) 
in Java, and hence the performance gain will be present for the Python solution only.
Provided here Java solution could make its best with Integers or Longs, but not
with BigIntegers.

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N + M)$$, 
where $$N$$ and $$M$$ are lengths of the input strings a and b.

* Space complexity : $$\mathcal{O}(\max(N, M))$$ to keep the answer.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short AC solution in Java with explanation
- Author: lx223
- Creation Date: Tue May 12 2015 03:32:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 01:06:51 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public String addBinary(String a, String b) {
            StringBuilder sb = new StringBuilder();
            int i = a.length() - 1, j = b.length() -1, carry = 0;
            while (i >= 0 || j >= 0) {
                int sum = carry;
                if (j >= 0) sum += b.charAt(j--) - '0';
                if (i >= 0) sum += a.charAt(i--) - '0';
                sb.append(sum % 2);
                carry = sum / 2;
            }
            if (carry != 0) sb.append(carry);
            return sb.reverse().toString();
        }
    }

Computation from string usually can be simplified by using a carry as such.
</p>


### Short code by c++
- Author: makuiyu
- Creation Date: Tue Feb 17 2015 19:16:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 23:59:11 GMT+0800 (Singapore Standard Time)

<p>
    class Solution
    {
    public:
        string addBinary(string a, string b)
        {
            string s = "";
            
            int c = 0, i = a.size() - 1, j = b.size() - 1;
            while(i >= 0 || j >= 0 || c == 1)
            {
                c += i >= 0 ? a[i --] - '0' : 0;
                c += j >= 0 ? b[j --] - '0' : 0;
                s = char(c % 2 + '0') + s;
                c /= 2;
            }
            
            return s;
        }
    };
</p>


### An accepted concise Python recursive solution 10 lines
- Author: lariven
- Creation Date: Sat Dec 20 2014 18:58:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 13 2019 14:03:10 GMT+0800 (Singapore Standard Time)

<p>
#add two binary from back to front, I think it is very self explained, when 1+1 we need a carry.
	#The time complex is O(m+n+c)\uFF0Cit\'s linear, where m=len(a)\uFF0Cn=len(b) and c="count of carries, which is less than min(m,n)".


       class Solution:
            def addBinary(self, a, b):
                if len(a)==0: return b
                if len(b)==0: return a
                if a[-1] == \'1\' and b[-1] == \'1\':
                    return self.addBinary(self.addBinary(a[0:-1],b[0:-1]),\'1\')+\'0\'
                if a[-1] == \'0\' and b[-1] == \'0\':
                    return self.addBinary(a[0:-1],b[0:-1])+\'0\'
                else:
                    return self.addBinary(a[0:-1],b[0:-1])+\'1\'
</p>


