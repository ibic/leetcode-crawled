---
title: "Unique Orders and Customers Per Month"
weight: 1594
#id: "unique-orders-and-customers-per-month"
---
## Description
<div class="description">
<p>Table: <code>Orders</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| order_id      | int     |
| order_date    | date    |
| customer_id   | int     |
| invoice       | int     |
+---------------+---------+
order_id is the primary key for this table.
This table contains information about the orders made by customer_id.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the number of <strong>unique orders</strong> and the number of <strong>unique customers</strong> with invoices <strong>&gt; $20</strong> for each <strong>different month</strong>.</p>

<p>Return the result table sorted in <strong>any order</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Orders</code>
+----------+------------+-------------+------------+
| order_id | order_date | customer_id | invoice    |
+----------+------------+-------------+------------+
| 1        | 2020-09-15 | 1           | 30         |
| 2        | 2020-09-17 | 2           | 90         |
| 3        | 2020-10-06 | 3           | 20         |
| 4        | 2020-10-20 | 3           | 21         |
| 5        | 2020-11-10 | 1           | 10         |
| 6        | 2020-11-21 | 2           | 15         |
| 7        | 2020-12-01 | 4           | 55         |
| 8        | 2020-12-03 | 4           | 77         |
| 9        | 2021-01-07 | 3           | 31         |
| 10       | 2021-01-15 | 2           | 20         |
+----------+------------+-------------+------------+

Result table:
+---------+-------------+----------------+
| month   | order_count | customer_count |
+---------+-------------+----------------+
| 2020-09 | 2           | 2              |
| 2020-10 | 1           | 1              |
| 2020-12 | 2           | 1              |
| 2021-01 | 1           | 1              |
+---------+-------------+----------------+
In September 2020 we have two orders from 2 different customers with invoices &gt; $20.
In October 2020 we have two orders from 1 customer, and only one of the two orders has invoice &gt; $20.
In November 2020 we have two orders from 2 different customers but invoices &lt; $20, so we don&#39;t include that month.
In December 2020 we have two orders from 1 customer both with invoices &gt; $20.
In January 2021 we have two orders from 2 different customers, but only one of them with invoice &gt; $20.
</pre>

</div>

## Tags


## Companies
- Whole Foods Market - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MS SQL simple solution
- Author: arav_kab
- Creation Date: Tue Sep 01 2020 20:52:12 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 01 2020 20:52:12 GMT+0800 (Singapore Standard Time)

<p>
```
select cast(order_date as varchar(7)) as month,
count(distinct order_id) as order_count,
count(distinct customer_id)  as customer_count
from orders
where invoice > 20
group by cast(order_date as varchar(7)) 
```
</p>


### Simeple mysql solution
- Author: ratul_kumar
- Creation Date: Sun Oct 04 2020 15:35:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 15:35:56 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below

select 
 SUBSTRING(order_date, 1, 7) as month,
 count(distinct order_id) as order_count,
 count(distinct customer_id) as customer_count
from
Orders
where invoice > 20
group by
SUBSTRING(order_date, 1, 7)

</p>


### MySQL solution using LEFT
- Author: Arthur-Zhang
- Creation Date: Fri Sep 11 2020 13:25:39 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 11 2020 13:25:39 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT left(order_date, 7) AS month, 
		COUNT(DISTINCT order_id) AS order_count,
        COUNT(DISTINCT customer_id) AS customer_count
FROM Orders
WHERE invoice > 20
GROUP BY month
```
</p>


