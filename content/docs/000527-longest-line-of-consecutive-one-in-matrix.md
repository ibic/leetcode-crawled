---
title: "Longest Line of Consecutive One in Matrix"
weight: 527
#id: "longest-line-of-consecutive-one-in-matrix"
---
## Description
<div class="description">
Given a 01 matrix <b>M</b>, find the longest line of consecutive one in the matrix. The line could be horizontal, vertical, diagonal or anti-diagonal.

<p><b>Example:</b><br />
<pre>
<b>Input:</b>
[[0,1,1,0],
 [0,1,1,0],
 [0,0,0,1]]
<b>Output:</b> 3
</pre>
</p>

<p>
<b>Hint:</b>
The number of elements in the given matrix will not exceed 10,000.
</p>
</div>

## Tags
- Array (array)

## Companies
- Google - 5 (taggedByAdmin: true)

## Official Solution
[TOC]


## Solution

---
#### Approach 1: Brute Force

**Algorithm**

The brute force approach is really simple. We directly traverse along every valid line in the given matrix: i.e. Horizontal, Vertical, Diagonal aline above and below the middle diagonal, Anti-diagonal line above and below the middle anti-diagonal. Each time during the traversal, we keep on incrementing the $$count$$ if we encounter continuous 1's. We reset the $$count$$ for any discontinuity encountered. While doing this, we also keep a track of the maximum $$count$$ found so far.


<iframe src="https://leetcode.com/playground/NXCJghgq/shared" frameBorder="0" width="100%" height="500" name="NXCJghgq"></iframe>

**Complexity Analysis**

Let $$m$$ be the length of the matrix and $$n$$ be the width of the matrix. As a result, $$mn$$ would be the total number of cells in the matrix.

* Time complexity : $$O(mn)$$. We traverse along the entire matrix 4 times.
* Space complexity : $$O(1)$$. Constant space is used.

---
#### Approach 2: Using 3D Dynamic Programming

**Algorithm**

Instead of traversing over the same matrix multiple times, we can keep a track of the 1' along all the lines possible while traversing the matrix once only. In order to do so, we make use of a $$4mn$$ sized $$dp$$ array. Here, $$dp[0]$$, $$dp[1]$$, $$dp[2]$$ ,$$dp[3]$$ are used to store the maximum number of continuous 1's found so far along the Horizontal, Vertical, Diagonal and Anti-diagonal lines respectively. e.g. $$dp[i][j][0]$$ is used to store the number of continuous 1's found so far(till we reach the element $$M[i][j]$$), along the horizontal lines only.

Thus, we traverse the matrix $$M$$ in a row-wise fashion only but, keep updating the entries for every $$dp$$ appropriately. 

The following image shows the filled $$dp$$ values for this matrix:
```
 0 1 1 0

 0 1 1 0
   
 0 0 1 1
   
```

![Longest_Line](../Figures/562_Longest_Line.PNG)

While filling up the $$dp$$, we can keep a track of the length of the longest consecutive line of 1's.

Watch this animation for complete process:

!?!../Documents/562_Longest_Line.json:1000,563!?!

<iframe src="https://leetcode.com/playground/CzZ37JMu/shared" frameBorder="0" width="100%" height="429" name="CzZ37JMu"></iframe>

**Complexity Analysis**

* Time complexity : $$O(mn)$$. We traverse the entire matrix once only.

* Space complexity : $$O(mn)$$. $$dp$$ array of size $$4mn$$ is used, where $$m$$ and $$n$$ are the number of rows ans coloumns of the matrix.

---

#### Approach 3: Using 2D Dynamic Programming

**Algorithm**

In the previous approach, we can observe that the current $$dp$$ entry is dependent only on the entries of the just previous corresponding $$dp$$ row. Thus, instead of maintaining a 2-D $$dp$$ matrix for each kind of line of 1's possible, we can use a 1-d array for each one of them, and update the corresponding entries in the same row during each row's traversal. Taking this into account, the previous 3-D $$dp$$ matrix shrinks to a 2-D $$dp$$ matrix now. The rest of the procedure remains same as the previous approach.


<iframe src="https://leetcode.com/playground/jEPgEY9G/shared" frameBorder="0" width="100%" height="497" name="jEPgEY9G"></iframe>

**Complexity Analysis**

* Time complexity : $$O(mn)$$. The entire matrix is traversed once only.

* Space complexity : $$O(n)$$. $$dp$$ array of size $$4n$$ is used, where $$n$$ is the number of columns of the matrix.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(nm) Time DP Solution
- Author: compton_scatter
- Creation Date: Sun Apr 23 2017 11:01:15 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 13:16:29 GMT+0800 (Singapore Standard Time)

<p>
```
public int longestLine(int[][] M) {
    int n = M.length, max = 0;
    if (n == 0) return max;
    int m = M[0].length;
    int[][][] dp = new int[n][m][4];
    for (int i=0;i<n;i++) 
        for (int j=0;j<m;j++) {
            if (M[i][j] == 0) continue;
            for (int k=0;k<4;k++) dp[i][j][k] = 1;
            if (j > 0) dp[i][j][0] += dp[i][j-1][0]; // horizontal line
            if (j > 0 && i > 0) dp[i][j][1] += dp[i-1][j-1][1]; // anti-diagonal line
            if (i > 0) dp[i][j][2] += dp[i-1][j][2]; // vertical line
            if (j < m-1 && i > 0) dp[i][j][3] += dp[i-1][j+1][3]; // diagonal line
            max = Math.max(max, Math.max(dp[i][j][0], dp[i][j][1]));
            max = Math.max(max, Math.max(dp[i][j][2], dp[i][j][3]));
        }
    return max;
}
```

Note that each cell of the DP table only depends on the current row or previous row so you can easily optimize the above algorithm to use only O(m) space.
</p>


### Simple and Concise Java Solution (Easy to Understand O(m+n) space)
- Author: ihaveayaya
- Creation Date: Mon Apr 24 2017 12:54:07 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 13:11:22 GMT+0800 (Singapore Standard Time)

<p>

    public int longestLine(int[][] M) {
        if (M.length == 0 || M[0].length == 0) {
            return 0;
        }
        int max = 0;
        int[] col = new int[M[0].length];
        int[] diag = new int[M.length + M[0].length];
        int[] antiD = new int[M.length + M[0].length];
        for (int i = 0; i < M.length; i++) {
            int row = 0;
            for (int j = 0; j < M[0].length; j++) {
                if (M[i][j] == 1) {
                    row++;
                    col[j]++;
                    diag[j + i]++;
                    antiD[j - i + M.length]++;
                    max = Math.max(max, row);
                    max = Math.max(max, col[j]);
                    max = Math.max(max, diag[j + i]);
                    max = Math.max(max, antiD[j - i + M.length]);
                } else {
                    row = 0;
                    col[j] = 0;
                    diag[j + i] = 0;
                    antiD[j - i + M.length] = 0;
                }
            }
        }
        return max;
}

Btw, I realized this is very similar to N-Queens. You can use the above approach and DFS to easily tackle down N-Queens problems.
</p>


### Python, Simple with Explanation
- Author: awice
- Creation Date: Sun Apr 23 2017 11:20:29 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 15 2018 23:32:23 GMT+0800 (Singapore Standard Time)

<p>
We can separate the problem into two subproblems.  The first subproblem is, given a 1 dimensional list of 0's and 1's, what is the longest chain of consecutive 1s?  The second subproblem is to generate every line (row, column, diagonal, and anti-diagonal).

The first problem is common.  We keep track of the number of 1's we've seen before.  If we see a 1, we add to our count and update our answer.  If we see a 0, we reset.  Alternatively, we can also use ```itertools.groupby```.  Straightforward code for the first part looks like this:
```
def score(line):
  ans = count = 0
  for x in line:
    if x:
      count += 1
      ans = max(ans, count)
    else:
      count = 0
  return ans
```

The second part is more complex.  We can try to manipulate indices of the grid, but there is a trick.  Each element in the grid belongs to exactly 4 lines: the r-th row, c-th column, (r+c)-th diagonal, and (r-c)-th anti-diagonal.  We scan from left to right, top to bottom, adding each element's value to it's respective 4 groups.  As we visited in reading order, our lines will be appended to in that order, which is suitable for our purposes.

```
def longestLine(self, A):
    if not A: return 0
    
    def score(line):
        return max(len(list(v)) if k else 0 
                   for k, v in itertools.groupby(line))
    
    groups = collections.defaultdict(list)
    for r, row in enumerate(A):
        for c, val in enumerate(row):
            groups[0, r] += [val]
            groups[1, c] += [val]
            groups[2, r+c] += [val]
            groups[3, r-c] += [val]
    
    return max(map(score, groups.itervalues()))
```
</p>


