---
title: "Construct Target Array With Multiple Sums"
weight: 1269
#id: "construct-target-array-with-multiple-sums"
---
## Description
<div class="description">
<p>Given an array of integers&nbsp;<code>target</code>. From a starting array, <code>A</code>&nbsp;consisting of all 1&#39;s, you may perform the following procedure :</p>

<ul>
	<li>let <code>x</code> be the sum of all elements currently in your array.</li>
	<li>choose index <code>i</code>, such that&nbsp;<code>0 &lt;= i &lt; target.size</code> and set the value of <code>A</code> at index <code>i</code> to <code>x</code>.</li>
	<li>You may repeat this procedure&nbsp;as many times as needed.</li>
</ul>

<p>Return True if it is possible to construct the <code>target</code> array from <code>A</code> otherwise&nbsp;return False.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> target = [9,3,5]
<strong>Output:</strong> true
<strong>Explanation:</strong> Start with [1, 1, 1] 
[1, 1, 1], sum = 3 choose index 1
[1, 3, 1], sum = 5 choose index 2
[1, 3, 5], sum = 9 choose index 0
[9, 3, 5] Done
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> target = [1,1,1,2]
<strong>Output:</strong> false
<strong>Explanation:</strong> Impossible to create target array from [1,1,1,1].
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> target = [8,5]
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>N == target.length</code></li>
	<li><code>1 &lt;= target.length&nbsp;&lt;= 5 * 10^4</code></li>
	<li><code>1 &lt;= target[i] &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Quora - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

#### Approach 1: Working Backward

**Intuition**

Let $$n$$ be the *length of the target array*.

Firstly, we know that if $$n = 1$$, then the sum of the array is always `1`, and therefore it is impossible for it to ever change from `[1]`. This means that the only target array with $$n = 1$$ that is possible to solve is `[1]` itself. Any others, for example, `[2]`, `[12]`, and `[18329832]` must be impossible. Depending on how you code your algorithm, you might or might not have to deal with this *edge case* explicitly.  

Now, how about when $$n ≥ 2$$?

A natural place to start might be to begin with an array of length $$n$$, containing all `1`s, and to attempt to get to the target array.

Let's have a go at doing that. Say our target array is `[13, 7, 1, 43]`.

We would start with an array of `[1, 1, 1, 1]`. At each step, we compute the sum of all elements in the array, and choose one of the numbers to replace with the sum, as per the problem statement.

For example, for the first step, the options we have are `[4, 1, 1, 1]`, `[1, 4, 1, 1]`, `[1, 1, 4, 1]`, and `[1, 1, 1, 4]`.

For each of these, we then have two options: replacing the `4` with a `7 ` (`1 + 1 + 1 + 4 = 7`) or replacing a `1` with a `7`. 

For example, the next possible steps for `[4, 1, 1, 1]` are `[7, 1, 1, 1]`, `[7, 4, 1, 1]`, `[7, 1, 4, 1]` and `[7, 1, 1, 4]`. 

As you can probably see, this branches out really, really fast.

![The exponential explosion of working forwards.](../Figures/1354/exponential_explosion.png)

It's not obvious which path might lead us to the target array.

How about the strategy of trying to get each number one-by-one, starting with the smallest? For example, when we had `[1, 7, 1, 1]`, perhaps we could say that two slots are correct, and we just need to get the other two without changing the two that are correct?

The only option now is to replace one of the others with `10` (`1 + 7 + 1 + 1 = 10`). That gives us `[10, 7, 1, 1]`.

But what happens next? `10 + 7 + 1 + 1 = 19`. This is higher than `13`.

It is now impossible to get a `13`. The sums are *strictly increasing* (as long as $$n  ≥ 2$$), i.e. each step's sum will always be larger than the last. Therefore, we definitely won't get the target array following this approach. The reason we know it's *strictly increasing* is because at each step, we're always making one number *larger*.

You may now be tempted to conclude that the target `[13, 7, 1, 43]` is *impossible* to reach. However, this is not the case. Here's the way that you can construct it!

```
[1,  1, 1, 1]
[4,  1, 1, 1]
[4,  7, 1, 1]
[13, 7, 1, 1]
[13, 7, 1, 22]
[13, 7, 1, 43]
```

As a side note (not needed for interviews, but aimed at the math lovers here), I'm not aware of a *greedy* approach that could go from the array of `1`s **up to** the target. It seems any such approach would be intractable. However, I haven't been able to *prove* this, so it's very possible there is indeed a way. If you know of one, please do let us know in the comments because I really am curious!

*We will, therefore, need a better strategy!*

If trying to calculate the result working forward doesn't work, sometimes working backward does. Let's have a go at working backward, i.e. going from `[13, 7, 1, 43]` to `[1, 1, 1, 1]`. 

Let's think carefully about what number had to be put into the target array *last*. Could it have, for example, possibly been `13`? If it was indeed the `13`, then our previous array went from `[x, 7, 1, 43]` to `[13, 7, 1, 43]`. We would then just need to figure out what `x` is, which should be simple, as we know `13 = x + 7 + 1 + 43`. Rearranging, we get `x = 13 - 7 - 1 - 43`. Therefore, `x = -38`. 

This can't be right though! We know that the array *always* contains positive numbers only (refer back to where we determined that the sums for each step are strictly increasing). Therefore, it could not have been `13`.

In fact, the only valid possibility is that the last was the largest number: `43`. Otherwise, `43` would have to be part of the sum, forcing the resulting sum to be greater than`43` (assuming $$n ≥ 2$$).

So now that we know it has to have been the `43`, we know that `43 = 13 + 7 + 1 + x`. Therefore, `x = 22`.

So we have `[13, 7, 1, 22] → [13, 7, 1, 43]` (look at the list a bit above, this is indeed the last transition!)

Seeing as we know we can go from `[13, 7, 1, 22]` directly to the target, and because there is only one valid way of going up to the target (recall that we *had* to change the largest), we now only need to consider how we can get from `[1, 1, 1, 1]` up to `[13, 7, 1, 22]`. In other words, we can now treat `[13, 7, 1, 22]` as the *target*.

And therefore, we can repeat the same process again! Here is an animation of that.

!?!../Documents/1354_valid_case_animation.json:960,180!?!

Something quite interesting we can infer here is that the steps to get from an array of `1`s to a target *has to be unique*. In other words, there is only *one* way to get to the target (as there's only one way of changing the largest each step).

Therefore, we can conclude that *the target array is reachable (from an array of `1`s) **if and only if** this unique chain can be derived by working backward.*

If a target array is not reachable, then the above approach has to *not* reduce it to an array of `1`s. So, what would it do?

As long as $$n ≥ 2$$, it can't get stuck in an infinite loop. We know this because the sums *strictly increase*. That only leaves the possibility of it going to numbers *below* `1`.

So our algorithm should check at each step that the array is *valid*, i.e. contains only numbers that are greater than or equal to `1`.

For reference, here is an animation of a target that is *not* possible to construct.

!?!../Documents/1354_invalid_case_animation.json:960,180!?!

**Algorithm**

A key thing to recognise is that order doesn't matter. In other words, if we know that `[13, 7, 1, 43]` has a solution, then we also know that `[43, 13, 7, 1]` has a solution.

Therefore, the problem reduces to converting all of the input numbers to `1`. Because at each step we always needed to replace the maximum, we should use a data structure that allows us to efficiently find the maximum after each change. Hopefully you've realised that the perfect data structure for the job is a `max-heap`!

We need to keep track of the current sum. This can be done by calculating the sum at the start, and then updating it as we change values (push and pop from heap).

Then, while the top of the `max-heap` is greater than 1, we "solve for x" (see intuition above), and then replace the maximum with `x`, as long as that `x` is not negative. If it is less than 1, then we return `false`. Once the max is equal to 1, this means everything else has to be 1 too, and so we can return `true`.

<iframe src="https://leetcode.com/playground/znrdm4V5/shared" frameBorder="0" width="100%" height="497" name="znrdm4V5"></iframe>

**Complexity Analysis**

Let $$n$$ be the length of the target array. Let $$k$$ be the *maximum* of the target array.

- Time Complexity : $$O(n + k \, \log \, n)$$.

    Making the initial heap is $$O(n)$$. 

    Then, each heap operation (add and remove) is $$O(\log \, n)$$. 

    The $$k$$ comes from the cost of reducing the largest item. In the worst case, it is massive. For example, consider a target of `[1, 1_000_000_000]`. One step of the algorithm will reduce it to `[1, 999_999_999]`. And the next is `[1, 999_999_998]`. You probably see where this is going. 

    Because we don't know whether $$n$$ or $$k \, \log \, n$$ is larger, we add them.

    While for this problem, we're told the maximum possible value in the array is `1,000,000,000`, we don't consider these limits for big-oh notation. Because the time taken varies with the maximum number, we consider this time complexity to be [**pseudo-polynomial**](https://en.wikipedia.org/wiki/Pseudo-polynomial_time). For the `[1, 1_000_000_000]` case, it is unworkably slow on any typical computer. 

- Space Complexity : $$O(n)$$.

    The heap requires $$O(n)$$ extra space. There are always $$n$$ or $$n-1$$ items on the heap at any one time.

    $$O(1)$$ space would be possible, by converting the target array directly into a heap. The Python code does this, although the line we used to make the target array negative is "technically" $$O(n)$$ (some Python interpreters might delete and replace this with an $$O(1)$$ operation behind the scenes). If space was a big issue (it isn't generally), then it's useful to know that these optimizations are at least *possible*.
    
The fact that this algorithm is **pseudo-polynomial**, and has to cope with large $$n$$ values, and extremely large $$k$$ values is a big limitation. Luckily there are a few tweaks we can make to the algorithm that will make it **polynomial**.

</br>

---

#### Approach 2: Working Backward with Optimizations

**Intuition**

If you got Approach 1 working, but failed on large test cases, you're still doing really well to have gotten that far. A lot of people find these final optimizations really difficult, so always remember during interviews that your performance is *compared to other candidates*. This is one of the harder "hard" questions, especially for those with a limited background in mathematics.

Recall that the worst case is where the largest number is substantially larger than the sum of the rest. For example, `[1, 5, 998]`. In these cases, we are repeatedly subtracting from the largest number and putting it back in. A good way to see what we need to do is to just compute the next few transitions for this target.

```
[1, 4, 998]
→ [1, 4, 993]
→ [1, 4, 988]
→ [1, 4, 983]
```

See the pattern? Each time, the largest number is being reduced by `5`. Where does the `5` come from? It is the sum of the other numbers.

Remembering that this number will be the top of the heap until it is smaller than the next largest (i.e. the `4`), we can see that this is going to continue until it is *less than `5`*. From the pattern we can see that eventually it gets to `[1, 4, 8]`, and finally `[1, 4, 3]`. 

Subtracting `5` from `998` repeatedly, until it is below `5`, is modular arithmetic. Effectively, all we've done is `998 % 5 = 3`, following a *really* inefficient process!

Generalising a bit, the `998` was the current heap maximum, and `5` was the sum of the rest (obtained in code with `total_sum - largest`).

So, instead of doing the following line:

```python
x = largest - (total_sum - largest)
```

We should do:

```python
x = largest % (total_sum - largest)
```

This solves the problem, although it might not be at all obvious why. The key ideas are:

1. That `largest` is always at least half of total_sum. 
2. That `largest` is always replaced with a value at most half of itself.

Effectively, we are always removing at least $$\frac{1}{4}$$ of the total sum in the array. 

To simplify the explanation, we'll define `rest = total_sum - largest`. i.e. it is simply the sum of the array, excluding the largest.

To prove the first point, we know that `largest` is always bigger than `rest`, because if it wasn't, `x` would go negative (and therefore we would immediately return `false` and exist.). This can be seen from the formula above. 

```
x = largest - (total_sum - largest)
→ 
x = largest - rest
```

To prove the second point, we need to think carefully what the following is doing:

```
x = largest % (total_sum - largest)
→ 
x = largest % rest
```

Because `largest > rest`, we know that `x` is at most `largest - rest`. i.e. the modulus will cause `rest` to be subtracted at least once.

If `rest` is at least half the size of `largest`, then this will clearly chop `largest` in half.

If instead `rest` is less than half the size of `largest`, then `largest % rest` must be less than half of `largest`.

Removing $$\frac{1}{4}$$ each time is logarithmic.

One edge case we need to be cautious of is where `rest` is `1`. When we take numbers modulo `1`, they always become `0`. The only case this can occur is where $$n = 2$$. In fact though, we know that this case is always doable, because `largest` is simply decremented by `1` repeatedly until it reaches `1` itself.


**Algorithm**

<iframe src="https://leetcode.com/playground/7Jqjehej/shared" frameBorder="0" width="100%" height="500" name="7Jqjehej"></iframe>

**Complexity Analysis**

- Time Complexity : $$O(n + \log \, k \cdot \log \, n)$$.

    Creating a heap is $$O(n)$$. 
    
    At each step, we were removing at least $$\frac{1}{4}$$ of the total sum. The original total sum is $$2 \cdot k$$, because $$k$$ is the largest element, and we know that if the algorithm continues, then the rest can't add up to more than $$k$$. So, we need to take $$O(\log \, k)$$ steps to reduce the array down. Each of these steps is the cost of a heap add and remove, i.e. $$O(\log \, n)$$. In total, this is $$O(\log \, k \cdot \log \, n)$$.

- Space Complexity : $$O(n)$$.

    Same as above.

</br>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Backtrack, OJ is wrong
- Author: lee215
- Creation Date: Sun Feb 16 2020 12:03:46 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Feb 26 2020 16:05:50 GMT+0800 (Singapore Standard Time)

<p>
# Foreword
By the way, I planed to update this post ealier.
I didn\'t do that, not because of lazy.
I understand that you have expectation on me or the top voted post,
but I also have expectation of LC.
But lc doesn\'t do its work at all (in my mind),
Someone would rather believe the misleading "Accepted",
Honestly, I cannot do better in this case like that.
<br>

# Weak test case
Problem is good, tests cases are trash.
Missing test cases:
[1]
[2]
[1, 2]
[1,1,2]
[1, 1000000000]
[5, 50]
<br>

# Wrong OJ
Yes, OJ needs a solution to run the test cases.
But the OJ for this problem fail the case `[1, 1000000000]`, which expects true
<br>

# Explanation
The total sum always bigger than all elements.
We can decompose the biggest number.
Each round we get the current maximum value,
delete it by the sum of other elements.

Time `O(N)` to build up the priority queue.
Time `O(logMaxAlogN))` for the reducing value part.
We have `O(maxA)` loops, which is similar to `gcd`
Space `O(N)`


`%` operation is totally much more important than using heap.
If brute force solution is accepted,
then the solutions without `%` are right and good.

But the truth is that, solution without % should be TLE.
So I afraid that, without `%` is **wrong** to me.
<br>

**Java**
O(nlogn) to build up the priority queue.
```java
    public boolean isPossible(int[] A) {
        PriorityQueue<Integer> pq = new PriorityQueue<Integer>((a, b) -> (b - a));
        long total = 0;
        for (int a : A) {
            total += a;
            pq.add(a);
        }
        while (true) {
            int a = pq.poll();
            total -= a;
            if (a == 1 || total == 1)
                return true;
            if (a < total || total == 0 || a % total == 0)
                return false;
            a %= total;
            total += a;
            pq.add(a);
        }
    }
```
**C++**
```cpp
    bool isPossible(vector<int>& A) {
        long total = 0;
        int n = A.size(), a;
        priority_queue<int> pq(A.begin(), A.end());
        for (int a : A)
            total += a;
        while (true) {
            a = pq.top(); pq.pop();
            total -= a;
            if (a == 1 || total == 1)
                return true;
            if (a < total || total == 0 || a % total == 0)
                return false;
            a %= total;
            total += a;
            pq.push(a);
        }
    }
```
**Python**
```py
    def isPossible(self, A):
        total = sum(A)
        A = [-a for a in A]
        heapq.heapify(A)
        while True:
            a = -heapq.heappop(A)
            total -= a
            if a == 1 or total == 1: return True
            if a < total or total == 0 or a % total == 0:
                return False
            a %= total
            total += a
            heapq.heappush(A, -a)
```
</p>


### [Java/C++] O(n) Solution (Reaching Points)
- Author: votrubac
- Creation Date: Sun Feb 16 2020 12:00:51 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 03 2020 10:23:44 GMT+0800 (Singapore Standard Time)

<p>
The approach is similar to [780. Reaching Points](https://leetcode.com/problems/reaching-points/).

The first solution uses a heap and has O(n log n) time complexity. The second solution is optimized per the complexity analysis to run in O(n) time.

If we work backwards, we always know which element - the biggest one - is the sum of all elements after the last turn. A max heap `q` would be a good data structure to keep track of the biggest element.  We also need to track the sum `s` of all elements.

Every turn, we remove the largest element `cur` from the heap and determine its previous value: `prev = cur - (s - cur)`. Then, we put the previous value back to the heap and update the sum.

Common pitfalls:
- The sum of all elements can exceed `INT_MAX`. We need to use a 64-bit integer for `s`.
- If the largest element is smaller than `s / 2`, we cannot continue with our turns.
- In case the largest element is significantly bigger than the sum of other elements, we can get TLE. To counter that, we can use modulo operation (as in  [780. Reaching Points](https://leetcode.com/problems/reaching-points/)). Instead `cur - (s - cur)`, we will do `cur % (s - cur)`.

> Note that we need to handle the case when (`s - cur == 1`) and return true (cases like `[100, 1]`). Otherwise, the result of the modulo operation will be zero.

In the end, if the sum is equals the number of elements, we reached the starting array (all ones).

**C++**
```CPP
bool isPossible(vector<int>& target) {
    auto s = accumulate(begin(target), end(target), (long long)0);
    priority_queue<int> q(begin(target), end(target));
    while (s > 1 && q.top() > s / 2) {
        auto cur = q.top(); q.pop();
        s -= cur;
        if (s <= 1)
            return s;
        q.push(cur % s);
        s += cur % s;
    }
    return s == target.size();
}
```

**Java**
```java
public boolean isPossible(int[] target) {
    long s = 0;
    PriorityQueue<Integer> q = new PriorityQueue<>(Collections.reverseOrder());
    for (int n : target) {
        s += n;
        q.add(n);
    }        
    while (s > 1 && q.peek() > s / 2) {
        int cur = q.poll();
        s -= cur;
        if (s <= 1) 
            return s == 0 ? false : true;
        q.add(cur % (int)s);
        s += cur % s;
    }
    return s == target.length;
}
```

**Complexity Analysis**
- Time: O(n log n) to put elements into the heap. It takes no more than 44 operations to check if it\'s possible, so we can consider it as a constant. We update different element each time, so the sum of the array roughly doubles. As numbers are integers, we cannot go above `INT_MAX`.
**Update:** the priority queue\'s constructor in C++ takes O(n) to heapify the input vector. In Java, the standard implementation seems to be O(n log n).
> The worst case is `[1,1]`, and it takes 44 operations before it starts to overflow. The resulting array will be `[1836311903, 1134903170]`. It\'ll take less for larger arrays; e.g. 16 operations for the array of size `5 * 10^4`.

> If we update the same element each time, we can perform a lot of operations before we max out, but the modulo operator will slash it in one go.
- Memory: O(n) for the heap.

#### O(n) Solution
As we can see from the complexity analysis above, we perform no more than 44 operations. So, instead puting the input array into the heap, which takes O(n log n), we can do up to 44 linear scans to find the next maximum element.

**C++**
```CPP
bool isPossible(vector<int>& t) {
    auto s = accumulate(begin(t), end(t), (long long)0);
    auto i = max_element(begin(t), end(t)) - begin(t);
    while (s > 1 && t[i] > s / 2) {
        s -= t[i];
        if (s <= 1)
            return s;
        t[i] = t[i] % s;
        s += t[i];
        i = max_element(begin(t), end(t)) - begin(t);
    }
    return s == t.size();
}
```

**Java**
```java
private int maxIdx(int[] arr) {
    int mi = 0;
    for (int i = 1; i < arr.length; ++i) {
        mi = arr[mi] < arr[i] ? i : mi;
    }
    return mi;
}
public boolean isPossible(int[] t) {
    long s = 0;
    for (int n : t) s += n;
    for (int i = maxIdx(t); s > 1 && t[i] > s / 2; i = maxIdx(t)) {
        s -= t[i];
        if (s <= 1)
            return s == 1 ? true : false;
        t[i] = t[i] % (int)s;
        s += t[i];
    }
    return s == t.length;
}
```
**Complexity Analysis**
- Time: O(n). We are doing up to 44 linear scans (16 for array of size `5 * 10^4`).
- Memory: O(1).
</p>


### [Java] Think the problem from the end to start.
- Author: LeetcodeEveryDay
- Creation Date: Sun Feb 16 2020 12:13:26 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 07 2020 23:42:07 GMT+0800 (Singapore Standard Time)

<p>
Now we can consider it from the other side :
We first find the max one, then we subtract the other nums. Then we will get the last round num in the current position. Then we change it to the last round\'s num, and recursively call the function.
eg.1:  [9,3,5] ->[1,3,5]->[1,3,1]->[1,1,1]
1. [9,3,5]: max = 9, index = 0, subtract the other nums, 9-5-3=1 , 1>0, so we then change target[0] to 1.
2. [1,3,5]: max = 5, index = 2, subtract the other nums, 5-1-3=1 , 1>0, so we then change target[2] to 1.
3. [1,3,1]: max = 3, index = 1, subtract the other nums, 3-1-1=1 , 1>0, so we then change target[1] to 1.
4. [1,1,1]: max = 1 ,then return true;

eg.2 : [8,5] ->[3,5]->[3,2]->[1,2]->[1,1]

1. [8,5]: max = 8, index = 0, subtract the other nums, 8-5=3 , 3>0, so we then change target[0] to 3
2. [3,5]: max = 5, index = 1, subtract the other nums, 5-3=2 , 2>0, so we then change target[1] to 2
3. [3,2]: max = 3, index = 0, subtract the other nums, 3-2=1 , 1>0, so we then change target[0] to 1
4. [1,2]: max = 2, index = 1,subtract the other nums, 2-1=1 , 1>0, so we then change target[1] to 1
5. [1,1]: max = 1 ,then return true;

```java
class Solution {
    public boolean isPossible(int[] target) {
        int max = 0;
        int index = 0;
        for(int i=0; i<target.length; i++){
            if(max < target[i]){
                max = target[i];  // find the max value
                index = i; // find the index of the max
            }
        }
        if(max == 1)return true;    // it means we finally get an array full of 1, then we return true;
		
        for(int i=0; i<target.length; i++){
            if(i != index){   // skip the max one\'s index
                max-=target[i];     // subtract the other num in the array.
				if(max <= 0)return false;// max must be one more than the sum of rest of the target [i].
            }
        }
        target[index] = max;  // change the current one to the new max.
        return isPossible(target);  //recursively call the function
    }
}
```

**Update:**
2020/2/16  : Thanks to [**@harshlynn**](https://leetcode.com/harshlynn/) and [**@elleryqueenhomels**](https://leetcode.com/elleryqueenhomels/)
Change the way to check the sum of other nums and the current max. Now we subtract the max  by other nums, because the max must be one more than the sum of rest of the target [i]. Otherwise, the current array is impossible to reach so we return false.
</p>


