---
title: "Reducing Dishes"
weight: 1289
#id: "reducing-dishes"
---
## Description
<div class="description">
<p>A chef&nbsp;has collected data on the <code>satisfaction</code> level of his&nbsp;<code>n</code> dishes.&nbsp;Chef can cook any dish in 1 unit of time.</p>

<p><em>Like-time coefficient</em>&nbsp;of a dish is defined as&nbsp;the time taken to cook that dish including previous dishes multiplied by its satisfaction level &nbsp;i.e.&nbsp; <code>time[i]</code>*<code>satisfaction[i]</code></p>

<p>Return&nbsp;the maximum sum of&nbsp;<em>Like-time coefficient </em>that the chef can obtain after dishes preparation.</p>

<p>Dishes can be prepared in <strong>any </strong>order and the chef can discard some dishes to get this maximum value.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> satisfaction = [-1,-8,0,5,-9]
<strong>Output:</strong> 14
<strong>Explanation: </strong>After Removing the second and last dish, the maximum total <em>Like-time coefficient</em> will be equal to (-1*1 + 0*2 + 5*3 = 14). Each dish is prepared in one unit of time.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> satisfaction = [4,3,2]
<strong>Output:</strong> 20
<strong>Explanation:</strong> Dishes can be prepared in any order, (2*1 + 3*2 + 4*3 = 20)
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> satisfaction = [-1,-4,-5]
<strong>Output:</strong> 0
<strong>Explanation:</strong> People don&#39;t like the dishes. No dish is prepared.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> satisfaction = [-2,5,-1,0,3,-3]
<strong>Output:</strong> 35
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == satisfaction.length</code></li>
	<li><code>1 &lt;= n &lt;= 500</code></li>
	<li><code>-10^3 &lt;=&nbsp;satisfaction[i] &lt;= 10^3</code></li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- OT - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy and Concise
- Author: lee215
- Creation Date: Sun Apr 05 2020 00:10:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 05 2020 00:20:08 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
If we cook some dishes,
they must be the most satisfied among all choices.

Another important observation is that,
we will cook the dish with small satisfication,
and leave the most satisfied dish in the end.

## **Explanation**
We choose dishes from most satisfied.
Everytime we add a new dish to the menu list,
all dishes on the menu list will be cooked one time unit later,
so the `result += total satisfaction on the list`.
We\'ll keep doing this as long as `A[i] + total > 0`.
<br>

## **Complexity**
Time `O(NlogN)`
Space `O(1)`
<br>

**Java**
```java
    public int maxSatisfaction(int[] A) {
        Arrays.sort(A);
        int res = 0, total = 0, n = A.length;
        for (int i = n - 1; i >= 0 && A[i] > -total; --i) {
            total += A[i];
            res += total;
        }
        return res;
    }
```
**C++**
```cpp
    int maxSatisfaction(vector<int>& A) {
        sort(A.begin(), A.end());
        int res = 0, total = 0, n = A.size();
        for (int i = n - 1; i >= 0 && A[i] > -total; --i) {
            total += A[i];
            res += total;
        }
        return res;
    }
```
**Python**
```py
    def maxSatisfaction(self, A):
        res = total = 0
        A.sort()
        while A and A[-1] + total > 0:
            total += A.pop()
            res += total
        return res
```

</p>


### C++ 0 ms beats 100% /All solutions in both time and space. With proper explaination
- Author: jiteshgupta490
- Creation Date: Sun Apr 05 2020 13:52:56 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Apr 08 2020 16:07:50 GMT+0800 (Singapore Standard Time)

<p>
```
// After sorting the array becomes - -9,-8,-1,0,5
// As we have sorted the array the bigger element will be at the end , so we start from the end of the array
// at i = n-1 i.e 4 -> Cumulative_sum = 0, and cur = 5 (5*1) = 5
// at i = 3 -> Cumulative-sum = 5, and cur = 5 + 5 + 0 (0*1 + 5*2) = 10
// at i = 2 -> Cumulative-sum = 5 (5 + 0), and cur = cur + Cumlative_sum + satis[i] = (10 + 5 + -1) = -1*1 + 0*2 + 5*3 = 14
// at i = 1 -> Cumulative-sum = 4 (5 + 0 + -1), and cur = cur + Cumlative_sum + satis[i] = (14 + 4 + -8) = -8*1 + -1*2 + 0*3 + 5*5= 14
// And similarly for i = 0 and and max of all currents is printed

class Solution {
public:
    int maxSatisfaction(vector<int>& satisfaction) {
        
        sort(satisfaction.begin(),satisfaction.end());
        int n = satisfaction.size();
        
        int Cumulative_sum = 0; // From back
        
        int ans = 0;
        int cur = 0;
        for(int i = n-1;i>=0;i--)
        {
            cur += Cumulative_sum + satisfaction[i];
            Cumulative_sum += satisfaction[i];
            
            ans = max(ans,cur);
        }
        
        return ans;
        
    }
};
```
</p>


### This shouldn't be rated as hard
- Author: dansilveira
- Creation Date: Sun Apr 05 2020 02:33:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 05 2020 02:33:00 GMT+0800 (Singapore Standard Time)

<p>
We have some way more elaborated (and by elaborated I mean hard) problems rated Medium in here that will take you way more time to even come up with an initial idea.


</p>


