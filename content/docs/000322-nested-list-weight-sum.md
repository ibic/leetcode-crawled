---
title: "Nested List Weight Sum"
weight: 322
#id: "nested-list-weight-sum"
---
## Description
<div class="description">
<p>Given a nested list of integers, return the sum of all integers in the list weighted by their depth.</p>

<p>Each element is either an integer, or a list -- whose elements may also be integers or other lists.</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[1,1],2,[1,1]]</span>
<strong>Output: </strong><span id="example-output-1">10 </span>
<strong>Explanation: </strong>Four 1&#39;s at depth 2, one 2 at depth 1.</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[1,[4,[6]]]</span>
<strong>Output: </strong><span id="example-output-2">27 </span>
<strong>Explanation: </strong>One 1 at depth 1, one 4 at depth 2, and one 6 at depth 3; 1 + 4*2 + 6*3 = 27.</pre>
</div>
</div>

</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Facebook - 14 (taggedByAdmin: false)
- LinkedIn - 10 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Cloudera - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary

This is a very simple recursion problem and is a nice introduction to Depth-first Search (DFS).

## Solution
---
#### Depth-first Traversal [Accepted]

**Algorithm**

Because the input is nested, it is natural to think about the problem in a recursive way. We go through the list of nested integers one by one, keeping track of the current depth $$d$$. If a nested integer is an integer $$n$$, we calculate its sum as $$n\times d$$. If the nested integer is a list, we calculate the sum of this list recursively using the same process but with depth $$d+1$$.

**Java**

```java
/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * public interface NestedInteger {
 *
 *     // @return true if this NestedInteger holds a single integer,
 *     // rather than a nested list.
 *     public boolean isInteger();
 *
 *     // @return the single integer that this NestedInteger holds,
 *     // if it holds a single integer
 *     // Return null if this NestedInteger holds a nested list
 *     public Integer getInteger();
 *
 *     // @return the nested list that this NestedInteger holds,
 *     // if it holds a nested list
 *     // Return null if this NestedInteger holds a single integer
 *     public List<NestedInteger> getList();
 * }
 */
public int depthSum(List<NestedInteger> nestedList) {
    return depthSum(nestedList, 1);
}

public int depthSum(List<NestedInteger> list, int depth) {
    int sum = 0;
    for (NestedInteger n : list) {
        if (n.isInteger()) {
            sum += n.getInteger() * depth;
        } else {
            sum += depthSum(n.getList(), depth + 1);
        }
    }
    return sum;
}
```

**Complexity Analysis**

The algorithm takes $$O(N)$$ time, where $$N$$ is the total number of nested elements in the input list. For example, the list `````[ [[[[1]]]], 2 ]````` contains $$4$$ nested lists and $$2$$ nested integers ($$1$$ and $$2$$), so $$N=6$$.

In terms of space, at most $$O(D)$$ recursive calls are placed on the stack, where $$D$$ is the maximum level of nesting in the input. For example, $$D=2$$ for the input `````[[1,1],2,[1,1]]`````, and $$D=3$$ for the input `````[1,[4,[6]]]`````.

Analysis written by: @noran

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution: similar to tree level order traversal
- Author: lop
- Creation Date: Fri Apr 01 2016 09:07:39 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 06:14:58 GMT+0800 (Singapore Standard Time)

<p>
    public int depthSum(List<NestedInteger> nestedList) {
        if(nestedList == null){
            return 0;
        }
        
        int sum = 0;
        int level = 1;
        
        Queue<NestedInteger> queue = new LinkedList<NestedInteger>(nestedList);
        while(queue.size() > 0){
            int size = queue.size();
            
            for(int i = 0; i < size; i++){
                NestedInteger ni = queue.poll();
                
                if(ni.isInteger()){
                    sum += ni.getInteger() * level;
                }else{
                    queue.addAll(ni.getList());
                }
            }
            
            level++;
        }
        
        return sum;
    }
</p>


### 2ms easy to understand java solution
- Author: larrywang2014
- Creation Date: Wed Mar 30 2016 22:59:03 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 28 2018 14:03:59 GMT+0800 (Singapore Standard Time)

<p>
    public int depthSum(List<NestedInteger> nestedList) {
        return helper(nestedList, 1);
    }

    private int helper(List<NestedInteger> list, int depth)
    {
        int ret = 0;
        for (NestedInteger e: list)
        {
            ret += e.isInteger()? e.getInteger() * depth: helper(e.getList(), depth + 1);
        }
        return ret;
    }
</p>


### Short Python BFS
- Author: xuewei4d
- Creation Date: Mon Aug 29 2016 05:36:59 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 06:29:05 GMT+0800 (Singapore Standard Time)

<p>
```python
class Solution(object):
    def depthSum(self, nestedList):
        """
        :type nestedList: List[NestedInteger]
        :rtype: int
        """
        depth, ret = 1, 0
        while nestedList:
            ret += depth * sum([x.getInteger() for x in nestedList if x.isInteger()])
            nestedList = sum([x.getList() for x in nestedList if not x.isInteger()], [])
            depth += 1
        return ret
```
</p>


