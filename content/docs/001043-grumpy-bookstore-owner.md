---
title: "Grumpy Bookstore Owner"
weight: 1043
#id: "grumpy-bookstore-owner"
---
## Description
<div class="description">
<p>Today, the bookstore owner has a store open for <code>customers.length</code> minutes.&nbsp; Every minute, some number of customers (<code>customers[i]</code>) enter the store, and all those customers leave after the end of that minute.</p>

<p>On some minutes, the bookstore owner is grumpy.&nbsp; If the bookstore owner is grumpy on the i-th minute, <code>grumpy[i] = 1</code>, otherwise <code>grumpy[i] = 0</code>.&nbsp; When the bookstore owner is grumpy, the customers of that minute are not satisfied, otherwise they are satisfied.</p>

<p>The bookstore owner knows a secret technique to keep themselves&nbsp;not grumpy for <code>X</code>&nbsp;minutes straight, but can only use it once.</p>

<p>Return the maximum number of customers that can be satisfied throughout the day.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>customers = [1,0,1,2,1,1,7,5], grumpy = [0,1,0,1,0,1,0,1], X = 3
<strong>Output: </strong>16
<strong>Explanation:</strong>&nbsp;The bookstore owner keeps themselves&nbsp;not grumpy for the last 3 minutes. 
The maximum number of customers that can be satisfied = 1 + 1 + 1 + 1 + 7 + 5 = 16.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>1 &lt;= X &lt;=&nbsp;customers.length ==&nbsp;grumpy.length &lt;= 20000</code></li>
	<li><code>0 &lt;=&nbsp;customers[i] &lt;= 1000</code></li>
	<li><code>0 &lt;=&nbsp;grumpy[i] &lt;= 1</code></li>
</ul>
</div>

## Tags
- Array (array)
- Sliding Window (sliding-window)

## Companies
- Robinhood - 2 (taggedByAdmin: false)
- Nutanix - 28 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Sliding window.
- Author: rock
- Creation Date: Sun May 26 2019 12:03:47 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Feb 18 2020 02:45:46 GMT+0800 (Singapore Standard Time)

<p>
1. Use a sliding window `winOfMakeSatisfied` to record the number of unsatisfied customers for `X` minutes. Deduct the unsatisfied customers from left end of the sliding window when it is wider than `X`:
 `winOfMakeSatisfied -= grumpy[i - X] * customers[i - X];`.
2. Use `satisfied` to record the number of satistified customers without grumpy technique.
3. by the end of iterations, `satisfied` + `max(winOfMakeSatisfied)` is the answer.

```java
    public int maxSatisfied(int[] customers, int[] grumpy, int X) {
        int satisfied = 0, maxMakeSatisfied = 0;
        for (int i = 0, winOfMakeSatisfied = 0; i < grumpy.length; ++i) {
            if (grumpy[i] == 0) { satisfied += customers[i]; }
            else { winOfMakeSatisfied += customers[i]; }
            if (i >= X) {
                winOfMakeSatisfied -= grumpy[i - X] * customers[i - X];
            }
            maxMakeSatisfied = Math.max(winOfMakeSatisfied, maxMakeSatisfied);
        }
        return satisfied + maxMakeSatisfied;        
    }
```
```python
    def maxSatisfied(self, customers: List[int], grumpy: List[int], X: int) -> int:
        i = win_of_make_satisfied = satisfied = max_make_satisfied = 0
        for c, g in zip(customers, grumpy):
            satisfied += (1 - g) * c
            win_of_make_satisfied += g * c
            if i >= X:
                win_of_make_satisfied -= grumpy[i - X] * customers[i - X]
            max_make_satisfied = max(win_of_make_satisfied, max_make_satisfied)  
            i += 1    
        return satisfied + max_make_satisfied
```

**Analysis:**

Time: O(n), space: O(1), where n = grumpy.length;
</p>


### Python with explanation. Rolling sum.
- Author: Hai_dee
- Creation Date: Sun May 26 2019 12:15:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 26 2019 12:30:59 GMT+0800 (Singapore Standard Time)

<p>
The way I approached this problem was to split it into 2 smaller problems. 

The first involves counting how many customers are already satisfied, i.e. those where the shopkeeper is not grumpy. I also set any slots with already satisfied customers to 0, so that we will be left with only the currently unsatisfied customers in the list.

For the second part, the array now only contains customers who will not be satisfied. We are able to make X adjacent times \u201Chappy\u201D, so we want to find the subarray of length X that has the most customers. We can just keep a rolling sum of the last X customers in the array, and then the best solution is the max the rolling sum ever was.

Finally we return the sum of the 2 parts: the customers who were already satisfied, and the maximum number who can be made satisfied by stopping the shop keeper from being grumpy for X time.

Note that both parts can be combined into a single loop, but I felt this way was probably easier for understanding, and both are still ```O(n)``` time. I\'ll shortly include code for how to merge it all together.

```
class Solution:
    def maxSatisfied(self, customers: List[int], grumpy: List[int], X: int) -> int:
        
        # Part 1 requires counting how many customers
        # are already satisfied, and removing them
        # from the customer list.
        already_satisfied = 0
        for i in range(len(grumpy)):
            if grumpy[i] == 0: #He\'s happy
                already_satisfied += customers[i]
                customers[i] = 0
        
        # Part 2 requires finding the optinal number
        # of unhappy customers we can make happy.
        best_we_can_make_satisfied = 0
        current_satisfied = 0
        for i, customers_at_time in enumerate(customers):
            current_satisfied += customers_at_time # Add current to rolling total
            if i >= X: # We need to remove some from the rolling total
                current_satisfied -= customers[i - X]
            best_we_can_make_satisfied = max(best_we_can_make_satisfied, current_satisfied)
        
        # The answer is the sum of the solutions for the 2 parts.
        return already_satisfied + best_we_can_make_satisfied
```
</p>


### C++ Sliding Window
- Author: votrubac
- Creation Date: Sun May 26 2019 12:06:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 26 2019 13:37:34 GMT+0800 (Singapore Standard Time)

<p>
When the owner is not grumpy, we count all customers as ```satisfied```. 

We then use the sliding window to count additionally satisfied customers (```add_satisfied```) if the owner start \'behaving\' at minute ```i```. We track the maximum additional satisfied customers in ```m_add_satisfied```.

Finally, return ```satisfied + m_add_satisfied``` as the result.

```
int maxSatisfied(vector<int>& cs, vector<int>& grumpy, int X) {
    auto satisfied = 0, m_add_satisfied = 0, add_satisfied = 0;
    for (auto i = 0; i < cs.size(); ++i) {
        satisfied += grumpy[i] ? 0 : cs[i];
        add_satisfied += grumpy[i] ? cs[i] : 0;
        if (i >= X) add_satisfied -= grumpy[i - X] ? cs[i - X] : 0;
        m_add_satisfied = max(m_add_satisfied, add_satisfied);
    }
    return satisfied + m_add_satisfied;
}
```
# Complexity Analysis
Runtime: O(n)
Memory: O(1)
</p>


