---
title: "Sort Array By Parity II"
weight: 872
#id: "sort-array-by-parity-ii"
---
## Description
<div class="description">
<p>Given an array <code>A</code>&nbsp;of non-negative integers, half of the integers in A are odd, and half of the integers are even.</p>

<p>Sort the array so that whenever <code>A[i]</code> is odd, <code>i</code> is odd; and whenever <code>A[i]</code> is even, <code>i</code> is even.</p>

<p>You may return any answer array that satisfies this condition.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[4,2,5,7]</span>
<strong>Output: </strong><span id="example-output-1">[4,5,2,7]</span>
<strong>Explanation: </strong>[4,7,2,5], [2,5,4,7], [2,7,4,5] would also have been accepted.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>2 &lt;= A.length &lt;= 20000</code></li>
	<li><code>A.length % 2 == 0</code></li>
	<li><code>0 &lt;= A[i] &lt;= 1000</code></li>
</ol>

<div>
<p>&nbsp;</p>
</div>
</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Two Pass

**Intuition and Algorithm**

Read all the even integers and put them into places `ans[0]`, `ans[2]`, `ans[4]`, and so on.

Then, read all the odd integers and put them into places `ans[1]`, `ans[3]`, `ans[5]`, etc.

<iframe src="https://leetcode.com/playground/c7feme3x/shared" frameBorder="0" width="100%" height="429" name="c7feme3x"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Read / Write Heads

**Intuition**

We are motivated (perhaps by the interviewer) to pursue a solution where we modify the original array `A` in place.

First, it is enough to put all even elements in the correct place, since all odd elements will be in the correct place too.  So let's only focus on `A[0], A[2], A[4], ...`

Ideally, we would like to have some partition where everything to the left is already correct, and everything to the right is undecided.

Indeed, this idea works if we separate it into two slices `even = A[0], A[2], A[4], ...` and `odd = A[1], A[3], A[5], ...`.  Our invariant will be that everything less than `i` in the even slice is correct, and everything less than `j` in the odd slice is correct.

**Algorithm**

For each even `i`, let's make `A[i]` even.  To do it, we will draft an element from the odd slice.  We pass `j` through the odd slice until we find an even element, then swap.  Our invariant is maintained, so the algorithm is correct.

<iframe src="https://leetcode.com/playground/JZoBkrjA/shared" frameBorder="0" width="100%" height="344" name="JZoBkrjA"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java two pointer one pass inplace
- Author: wangzi6147
- Creation Date: Sun Oct 14 2018 11:40:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 02:01:19 GMT+0800 (Singapore Standard Time)

<p>
```

class Solution {
    public int[] sortArrayByParityII(int[] A) {
        int i = 0, j = 1, n = A.length;
        while (i < n && j < n) {
            while (i < n && A[i] % 2 == 0) {
                i += 2;
            }
            while (j < n && A[j] % 2 == 1) {
                j += 2;
            }
            if (i < n && j < n) {
                swap(A, i, j);
            }
        }
        return A;
    }
    private void swap(int[] A, int i, int j) {
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }
}

```
</p>


### Python one-pass, O(1) memory, simple code, beats 90%
- Author: Peregrin
- Creation Date: Sun Dec 16 2018 23:32:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 16 2018 23:32:56 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def sortArrayByParityII(self, a):
        i = 0 # pointer for even misplaced
        j = 1 # pointer for odd misplaced
        sz = len(a)
        
        # invariant: for every misplaced odd there is misplaced even
        # since there is just enough space for odds and evens

        while i < sz and j < sz:
            if a[i] % 2 == 0:
                i += 2
            elif a[j] % 2 == 1:
                j += 2
            else:
                # a[i] % 2 == 1 AND a[j] % 2 == 0
                a[i],a[j] = a[j],a[i]
                i += 2
                j += 2

        return a
```
</p>


### C++ 5 lines, two pointers + 2-liner bonus
- Author: votrubac
- Creation Date: Sun Oct 14 2018 11:39:51 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 06 2019 02:07:08 GMT+0800 (Singapore Standard Time)

<p>
Use two pointers to search for missplaced odd and even elements, and swap them.
```
vector<int> sortArrayByParityII(vector<int>& A) {
    for (int i = 0, j = 1; i < A.size(); i += 2, j += 2) {
        while (i < A.size() && A[i] % 2 == 0) i += 2;
        while (j < A.size() && A[j] % 2 == 1) j += 2;
        if (i < A.size()) swap(A[i], A[j]);
    }
    return A;
}
```
Now, some fun for for my minimalistic functional friends. It\'s techically a two-liner, though I split ```swap``` into 3 lines for readability :) It actually may even look a bit cleaner, as you do not have to do "plus 2".
```
vector<int> sortArrayByParityII(vector<int>& A) {
  for (int i = 0, j = 0; i < A.size() && j < A.size(); ) swap(
      *find_if(begin(A) + i, end(A), [&] (int v) { return (i++ % 2 == 0 && v % 2 != 0) || i == A.size(); }),
      *find_if(begin(A) + j, end(A), [&] (int v) { return (j++ % 2 != 0 && v % 2 == 0) || j == A.size(); }));
  return A;
}
```
</p>


