---
title: "Construct Binary Tree from Preorder and Postorder Traversal"
weight: 839
#id: "construct-binary-tree-from-preorder-and-postorder-traversal"
---
## Description
<div class="description">
<p>Return any binary tree that matches the given preorder and postorder traversals.</p>

<p>Values in the traversals&nbsp;<code>pre</code> and <code>post</code>&nbsp;are distinct&nbsp;positive integers.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>pre = <span id="example-input-1-1">[1,2,4,5,3,6,7]</span>, post = <span id="example-input-1-2">[4,5,2,6,7,3,1]</span>
<strong>Output: </strong><span id="example-output-1">[1,2,3,4,5,6,7]</span>
</pre>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ul>
	<li><code>1 &lt;= pre.length == post.length &lt;= 30</code></li>
	<li><code>pre[]</code> and <code>post[]</code>&nbsp;are both permutations of <code>1, 2, ..., pre.length</code>.</li>
	<li>It is guaranteed an answer exists. If there exists multiple answers, you can return any of them.</li>
</ul>
</div>

</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Recursion

**Intuition**

A preorder traversal is:

* `(root node) (preorder of left branch) (preorder of right branch)`

While a postorder traversal is:

* `(postorder of left branch) (postorder of right branch) (root node)`

For example, if the final binary tree is `[1, 2, 3, 4, 5, 6, 7]` (serialized), then the preorder traversal is `[1] + [2, 4, 5] + [3, 6, 7]`, while the postorder traversal is `[4, 5, 2] + [6, 7, 3] + [1]`.

If we knew how many nodes the left branch had, we could partition these arrays as such, and use recursion to generate each branch of the tree.

**Algorithm**

Let's say the left branch has $$L$$ nodes.  We know the head node of that left branch is `pre[1]`, but it also occurs last in the postorder representation of the left branch.  So `pre[1] = post[L-1]` (because of uniqueness of the node values.)  Hence, `L = post.indexOf(pre[1]) + 1`.

Now in our recursion step, the left branch is represnted by `pre[1 : L+1]` and `post[0 : L]`, while the right branch is represented by `pre[L+1 : N]` and `post[L : N-1]`.

<iframe src="https://leetcode.com/playground/jtdpR5eG/shared" frameBorder="0" width="100%" height="378" name="jtdpR5eG"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the number of nodes.

* Space Complexity:  $$O(N^2)$$.
<br />
<br />


---
#### Approach 2: Recursion (Space Saving Variant)

**Explanation**

We present a variation of *Approach 1* that uses indexes to refer to the subarrays of `pre` and `post`, instead of passing copies of those subarrays.  Here, `(i0, i1, N)` refers to `pre[i0:i0+N], post[i1:i1+N]`.

<iframe src="https://leetcode.com/playground/JuxBYmFS/shared" frameBorder="0" width="100%" height="446" name="JuxBYmFS"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the number of nodes.

* Space Complexity:  $$O(N)$$, the space used by the answer.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] One Pass, Real O(N)
- Author: lee215
- Creation Date: Sun Aug 19 2018 11:02:52 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 05 2019 20:59:00 GMT+0800 (Singapore Standard Time)

<p>
## **Foreword**
I saw some solutions saying `O(N)` time, but actually they are not.
If it takes already `O(N)` time to find left part and right part, it could not be `O(N)`.
<br>

## **Complexity**:
Time `O(N)`, as we iterate both pre index and post index only once.
Space `O(height)`, depending on the height of constructed tree.
<br>

## **Recursive Solution**
Create a node `TreeNode(pre[preIndex])` as the root.

Becasue root node will be lastly iterated in post order,
`if root.val == post[posIndex]`,
it means we have constructed the whole tree,

If we haven\'t completed constructed the whole tree,
So we recursively `constructFromPrePost` for left sub tree and right sub tree.

And finally, we\'ll reach the `posIndex` that `root.val == post[posIndex]`.
We increment `posIndex` and return our `root` node.

**C++:**
```cpp
    int preIndex = 0, posIndex = 0;
    TreeNode* constructFromPrePost(vector<int>& pre, vector<int>& post) {
        TreeNode* root = new TreeNode(pre[preIndex++]);
        if (root->val != post[posIndex])
            root->left = constructFromPrePost(pre, post);
        if (root->val != post[posIndex])
            root->right = constructFromPrePost(pre, post);
        posIndex++;
        return root;
    }
```

**Java:**
inspired by @duhber
```java
    int preIndex = 0, posIndex = 0;
    public TreeNode constructFromPrePost(int[]pre, int[]post) {
        TreeNode root = new TreeNode(pre[preIndex++]);
        if (root.val != post[posIndex])
            root.left = constructFromPrePost(pre, post);
        if (root.val != post[posIndex])
            root.right = constructFromPrePost(pre, post);
        posIndex++;
        return root;
    }
```
**Python:**
```py
    preIndex, posIndex = 0, 0
    def constructFromPrePost(self, pre, post):
        root = TreeNode(pre[self.preIndex])
        self.preIndex += 1
        if (root.val != post[self.posIndex]):
            root.left = self.constructFromPrePost(pre, post)
        if (root.val != post[self.posIndex]):
            root.right = self.constructFromPrePost(pre, post)
        self.posIndex += 1
        return root
```
<br>

## **Iterative Solution**
We will **preorder** generate TreeNodes, push them to `stack` and **postorder** pop them out.
1. Iterate on `pre` array and construct node one by one.
2. `stack` save the current path of tree.
3. `node = new TreeNode(pre[i])`, if not left child, add node to the left. otherwise add it to the right.
4. If we meet a same value in the pre and post, it means we complete the construction for current subtree. We pop it from `stack`.

**C++:**
```cpp
    TreeNode* constructFromPrePost(vector<int> pre, vector<int> post) {
        vector<TreeNode*> s;
        s.push_back(new TreeNode(pre[0]));
        for (int i = 1, j = 0; i < pre.size(); ++i) {
            TreeNode* node = new TreeNode(pre[i]);
            while (s.back()->val == post[j])
                s.pop_back(), j++;
            if (s.back()->left == NULL) s.back()->left = node;
            else s.back()->right = node;
            s.push_back(node);
        }
        return s[0];
    }
```

**Java:**
```java
    public TreeNode constructFromPrePost(int[] pre, int[] post) {
        Deque<TreeNode> s = new ArrayDeque<>();
        s.offer(new TreeNode(pre[0]));
        for (int i = 1, j = 0; i < pre.length; ++i) {
            TreeNode node = new TreeNode(pre[i]);
            while (s.getLast().val == post[j]) {
                s.pollLast(); j++;
            }
            if (s.getLast().left == null) s.getLast().left = node;
            else s.getLast().right = node;
            s.offer(node);
        }
        return s.getFirst();
    }
```
**Python:**
```py
    def constructFromPrePost(self, pre, post):
        stack = [TreeNode(pre[0])]
        j = 0
        for v in pre[1:]:
            node = TreeNode(v)
            while stack[-1].val == post[j]:
                stack.pop()
                j += 1
            if not stack[-1].left:
                stack[-1].left = node
            else:
                stack[-1].right = node
            stack.append(node)
        return stack[0]
```
<br>

</p>


### Java / Python Divide and Conquer with Explanation
- Author: GraceMeng
- Creation Date: Sun Aug 19 2018 14:32:47 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 14:10:26 GMT+0800 (Singapore Standard Time)

<p>
The problem is easier to solve if we decrease it into subproblems using **Divide and Conquer**.
```
e.g.   Given preorder : 1 2 4 5 3 6;     postorder: 4 5 2 6 3 1.
We see it as preorder : 1 (2 4 5) (3 6); postorder: (4 5 2) (6 3) 1 [to be explained afterwards]
That can be decreased to subproblems A, B, C: 
A. preorder : 1; postorder: 1 =>
 1
B. preorder : (2 4 5); postorder: (4 5 2) => 
   2
  / \
 4   5
C. preorder : (3 6); postorder: (6 3) => 
   3
  / 
 6     or
   3
    \
     6
* Then we conquer the subproblems => A.left = B; A.right = C;
   1
  / \
 2   3
/ \  /
4  5 6
```
If we observe parameters in each recursion above:
```
preStart: 0, preEnd: 5, postStart: 0, postEnd: 5
preStart: 1, preEnd: 3, postStart: 0, postEnd: 2
preStart: 4, preEnd: 5, postStart: 3, postEnd: 4
```
For the commented, `[to be explained afterwards]`, how do we decrease a problem?
That is, 1 is root and 2 is its left child. Since 2 is the root of the left subtree, all elements in front of 2 in `post[]` must be in the left subtree also. 
We recursively follow the above approach.
Please note that `pre[preStart + 1] may also be the root of the right subtree` if there is no left subtree at all in the orginal tree. Since we are asked to generate one possible original tree, I assume `pre[preStart + 1]` to be the left subtree root always.

****
**Java**
```
    public TreeNode constructFromPrePost(int[] pre, int[] post) {
        return constructFromPrePost(pre, 0, pre.length - 1, post, 0, pre.length - 1);
    }
    
    private TreeNode constructFromPrePost(int[] pre, int preStart, int preEnd, int[] post, int postStart, int postEnd) {
        // Base cases.
        if (preStart > preEnd) {
            return null;
        }
        if (preStart == preEnd) {
            return new TreeNode(pre[preStart]);
        }
        
        // Build root.
        TreeNode root = new TreeNode(pre[preStart]);
        
        // Locate left subtree.
        int leftSubRootInPre = preStart + 1; 
        int leftSubRootInPost = findLeftSubRootInPost(pre[leftSubRootInPre], post, postStart, postEnd);
        int leftSubEndInPre = leftSubRootInPre + (leftSubRootInPost - postStart);
        
        // Divide.
        TreeNode leftSubRoot = constructFromPrePost(pre, leftSubRootInPre, leftSubEndInPre, 
                                                    post, postStart, leftSubRootInPost);  
        TreeNode rightSubRoot = constructFromPrePost(pre, leftSubEndInPre + 1, preEnd, 
                                                     post, leftSubRootInPost + 1, postEnd - 1);
        
        // Conquer.      
        root.left = leftSubRoot;
        root.right = rightSubRoot;
        
        return root;
    }
    
    private int findLeftSubRootInPost(int leftSubRootVal, int[] post, int postStart, int postEnd) {
        for (int i = postStart; i <= postEnd; i++) {
            if (post[i] == leftSubRootVal) {
                return i;
            }
        }
        
        throw new IllegalArgumentException();
    }

```
**Python**
```
    def constructFromPrePost(self, pre, post):
        return self.constructFromPrePostRecurUtil(
            pre, 0, len(pre) - 1, post, 0, len(post) - 1)
        
    def constructFromPrePostRecurUtil(
            self, 
            pre, 
            preStart, 
            preEnd, 
            post, 
            postStart, 
            postEnd):
        # Base case.
        if (preStart > preEnd):
            return None
        if (preStart == preEnd):
            return TreeNode(pre[preStart])
        # Recursive case.
        root = TreeNode(pre[preStart])
        leftRootIndexInPre = preStart + 1
        leftRootIndexInPost = self.getIndexInPost(
            post, pre[leftRootIndexInPre])
        leftEndIndexInPre = leftRootIndexInPre + \
            (leftRootIndexInPost - postStart)
        root.left = self.constructFromPrePostRecurUtil(
            pre, 
            leftRootIndexInPre, 
            leftEndIndexInPre, 
            post, 
            postStart, 
            leftRootIndexInPost)
        root.right = self.constructFromPrePostRecurUtil(
            pre, 
            leftEndIndexInPre + 1, 
            preEnd, 
            post, 
            leftRootIndexInPost + 1, 
            postEnd - 1)
        return root
        
    def getIndexInPost(self, post, target):
        for i, v in enumerate(post):
            if v == target:
                return i
        return -1   # to optimize
```
**(\u4EBA \u2022\u0348\u1D17\u2022\u0348)** Thanks for voting!
</p>


### C++ O(N) recursive solution
- Author: zhoubowei
- Creation Date: Sun Aug 19 2018 11:12:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 23 2018 01:29:01 GMT+0800 (Singapore Standard Time)

<p>
For two subarrays `pre[a,b]` and `post[c,d]`, if we want to reconstruct a tree from them, we know that pre[a]==post[d] is the root node.

```
[root][......left......][...right..]  ---pre
[......left......][...right..][root]  ---post
```

`pre[a+1]` is the root node of the left subtree.
Find the index of `pre[a+1]` in `post`, then we know the left subtree should be constructed from `pre[a+1, a+idx-c+1]` and `post[c, idx]`.

Here is my code:

```cpp
class Solution {
public:
    unordered_map<int, int> m; // value->index
    TreeNode* constructFromPrePost(vector<int>& pre, vector<int>& post) {
        int len = post.size();
        for (int i = 0; i < len; i++) m[post[i]] = i;
        return construct(pre, post, 0, len - 1, 0, len - 1);
    }
    
    TreeNode* construct(vector<int>& pre, vector<int>& post, int a, int b, int c, int d) {
        TreeNode* n = new TreeNode(pre[a]);
        if (a == b) return n;
        int t = pre[a + 1];
        int idx = m[t];
        int len = idx - c + 1;
        n->left = construct(pre, post, a + 1, a + len, c, c + len - 1);
        if (idx + 1 == d) return n;
        n->right = construct(pre, post, a + len + 1, b, idx + 1, d - 1);
        return n;
    }
};
```
</p>


