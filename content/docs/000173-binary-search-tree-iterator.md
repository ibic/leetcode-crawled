---
title: "Binary Search Tree Iterator"
weight: 173
#id: "binary-search-tree-iterator"
---
## Description
<div class="description">
<p>Implement an iterator over a binary search tree (BST). Your iterator will be initialized with the root node of a BST.</p>

<p>Calling <code>next()</code> will return the next smallest number in the BST.</p>

<p>&nbsp;</p>

<ul>
</ul>

<p><strong>Example:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2018/12/25/bst-tree.png" style="width: 189px; height: 178px;" /></strong></p>

<pre>
BSTIterator iterator = new BSTIterator(root);
iterator.next();    // return 3
iterator.next();    // return 7
iterator.hasNext(); // return true
iterator.next();    // return 9
iterator.hasNext(); // return true
iterator.next();    // return 15
iterator.hasNext(); // return true
iterator.next();    // return 20
iterator.hasNext(); // return false
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ul>
	<li><code>next()</code> and <code>hasNext()</code> should run in average O(1) time and uses O(<i>h</i>) memory, where <i>h</i> is the height of the tree.</li>
	<li>You may assume that&nbsp;<code>next()</code>&nbsp;call&nbsp;will always be valid, that is, there will be at least a next smallest number in the BST when <code>next()</code> is called.</li>
</ul>

</div>

## Tags
- Stack (stack)
- Tree (tree)
- Design (design)

## Companies
- Facebook - 31 (taggedByAdmin: true)
- Amazon - 8 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: true)
- Microsoft - 3 (taggedByAdmin: true)
- Bloomberg - 3 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Oracle - 5 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Qualtrics - 3 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: true)
- eBay - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Cloudera - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- Redfin - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Splunk - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

Before looking at the solutions for this problem, let's try and boil down what the problem statement essentially asks us to do. So, we need to implement an iterator class with two functions namely `next()` and `hasNext()`. The `hasNext()` function returns a boolean value indicating whether there are any more elements left in the binary search tree or not. The `next()` function returns the next smallest element in the BST. Therefore, the first time we call the `next()` function, it should return the smallest element in the BST and likewise, when we call `next()` for the very last time, it should return the largest element in the BST. 

You might be wondering as to what could be the use case for an iterator. Essentially, an iterator can be used to *iterate over* any container object. For our purpose, the container object is a binary search tree. If such an iterator is defined, then the traversal logic can be abstracted out and we can simply make use of the iterator to process the elements in a certain order.

```python
1. new_iterator = BSTIterator(root);
2. while (new_iterator.hasNext())
3.     process(new_iterator.next());
```

Now that we know the motivation behind designing a good iterator class for a data structure, let's take a look at another interesting aspect about the iterator that we have to build for this problem. Usually, an iterator simply goes over each of the elements of the container one by one. For the BST, we want the iterator to return elements in an ascending order.

> An important property of the binary search tree is that the inorder traversal of a BST gives us the elements in a sorted order. Thus, the inorder traversal will be the core of the solutions that we will look ahead.

!?!../Documents/173_anim.json:770,405!?!

---

#### Approach 1: Flattening the BST

**Intuition**

In computer programming, an iterator is an object that enables a programmer to traverse a container, particularly lists. This is the Wikipedia definition of an iterator. Naturally, the easiest way to implement an iterator would be on an array like container interface. So, if we had an array, all we would need is a pointer or an index and we could easily implement the two required functions `next()` and `hasNext()`.

Hence, the first approach that we will look at is based on this idea. We will be using additional memory and we will flatten the binary search tree into an array. Since we need the elements to be in a sorted order, we will do an inorder traversal over the tree and store the elements in a new array and then build the iterator functions using this new array.

**Algorithm**

1. Initialize an empty array that will contain the nodes of the binary search tree in the sorted order. 
2. We traverse the binary search tree in the inorder fashion and for each node that we process, we add it to our array `nodes`. Note that before processing a node, its left subtree has to be processed (or recursed upon) and after processing a node, its right subtree has to be recursed upon.
3. Once we have all the nodes in an array, we simply need a pointer or an index in that array to implement the two functions `next` and `hasNext`. Whenever there's a call to `hasNext`, we simply check if the index has reached the end of the array or not. For the call to `next` function, we simply return the element pointed by the index. Also, after a the `next` function call is made, we have to move the index one step forward to simulate the progress of our iterator. 

<center>
<img src="../Figures/173/appr_1.png"></center>

<iframe src="https://leetcode.com/playground/N47ccN8t/shared" frameBorder="0" width="100%" height="500" name="N47ccN8t"></iframe>

**Complexity analysis**

* Time complexity : $$O(N)$$ is the time taken by the constructor for the iterator. The problem statement only asks us to analyze the complexity of the two functions, however, when implementing a class, it's important to also note the time it takes to initialize a new object of the class and in this case it would be linear in terms of the number of nodes in the BST. In addition to the space occupied by the new array we initialized, the recursion stack for the inorder traversal also occupies space but that is limited to $$O(h)$$ where $$h$$ is the height of the tree.
    - `next()` would take $$O(1)$$ 
    - `hasNext()` would take $$O(1)$$
* Space complexity : $$O(N)$$ since we create a new array to contain all the nodes of the BST. This doesn't comply with the requirement specified in the problem statement that the maximum space complexity of either of the functions should be $$O(h)$$ where $$h$$ is the height of the tree and for a well balanced BST, the height is usually $$logN$$. So, we get great time complexities but we had to compromise on the space. Note that the new array is used for both the function calls and hence the space complexity for both the calls is $$O(N)$$.
<br/>
<br/>

---

#### Approach 2: Controlled Recursion

**Intuition**

The approach we saw earlier uses space which is linear in the number of nodes in the binary search tree. However, the reason we had to resort to such an approach was because we can control the iteration over the array. We can't really *pause* a recursion in between and then start it off sometime later. 

>However, if we could simulate a controlled recursion for an inorder traversal, we wouldn't really need to use any additional space other than the space used by the stack for our recursion simulation. 

So, this approach essentially uses a custom stack to simulate the inorder traversal i.e. we will be taking an iterative approach to inorder traversal rather than going with the recursive approach and in doing so, we will be able to easily implement the two function calls without any other additional space. 

Things however, do get a bit complicated as far as the time complexity of the two operations is concerned and that is where we will spend a little bit of time to understand if this approach complies with all the asymptotic complexity requirements of the question. Let's move on to the algorithm for now to look at this idea more concretely.

**Algorithm**

1. Initialize an empty stack `S` which will be used to simulate the inorder traversal for our binary search tree. Note that we will be following the same approach for inorder traversal as before except that now we will be using our own stack rather than the system stack. Since we are using a custom data structure, we can *pause* and *resume* the recursion at will.
2. Let's also consider a helper function that we will be calling again and again in the implementation. This function, called `_inorder_left` will essentially add all the nodes in the leftmost branch of the tree rooted at the given node `root` to the stack and it will keep on doing so until there is no `left` child of the `root` node. Something like the following code:
                
      <pre>def inorder_left(root):
      while (root):
        S.append(root)
        root = root.left</pre>
        
      For a given node `root`, the next smallest element will *always* be the leftmost element in its tree. So, for a given root node, we keep on following the leftmost branch until we reach a node which doesn't have a left child and that will be the next smallest element. For the root of our BST, this leftmost node would be the smallest node in the tree. Rest of the nodes are added to the stack because they are pending processing. Try and relate this with a dry run of a simple recursive inorder traversal and things will make a bit more sense. 
      
3. The first time `next()` function call is made, the smallest element of the BST has to be returned and then our simulated recursion has to move one step forward i.e. move onto the next smallest element in the BST. The invariant that will be maintained in this algorithm is that the stack top always contains the element to be returned for the `next()` function call. However, there is additional work that needs to be done to maintain that invariant. It's very easy to implement the `hasNext()` function since all we need to check is if the stack is empty or not. So, we will only focus on the `next()` call from now. 

4. Initially, given the root node of the BST, we call the function `_inorder_left` and that ensures our invariant holds. Let's see this first step with an example.
    
    <center>
    <img src="../Figures/173/approach_2-1.png" width="700"></center>
    
5. Suppose we get a call to the `next()` function. The node which we have to return i.e. the next smallest element in the binary search tree iterator is the one sitting at the top of our stack. So, for the example above, that node would be `2` which is the correct value. Now, there are two possibilities that we have to deal with:
      * One is where the node at the top of the stack is actually a leaf node. This is the best case and here we don't have to do anything. Simply pop the node off the stack and return its value. So, this would be a constant time operation.
      * Second is where the node has a `right` child. We don't need to check for the left child because of the way we have added nodes onto the stack. The topmost node either won't have a `left` child or would already have the `left` subtree processed. If it has a `right` child, then we call our helper function on the node's right child. This would comparatively be a costly operation depending upon the structure of the tree.
      
        <center>
        <img src="../Figures/173/approach_2-2.png" width="700"></center>
        
6. We keep on maintaining the invariant this way in the function call for `next` and this way we will always be able to return the next smallest element in the BST from the top of the stack. Again, it's important to understand that obtaining the next smallest element doesn't take much time. However, some time is spent in maintaining the invariant that the stack top will *always* have the node we are looking for.       

<iframe src="https://leetcode.com/playground/8JX6wb33/shared" frameBorder="0" width="100%" height="500" name="8JX6wb33"></iframe>

**Complexity analysis**        

* Time complexity : The time complexity for this approach is very interesting to analyze. Let's look at the complexities for both the functions in the class:
    - `hasNext` is the easier of the lot since all we do in this is to return true if there are any elements left in the stack. Otherwise, we return false. So clearly, this is an $$O(1)$$ operation every time. Let's look at the more complicated function now to see if we satisfy all the requirements in the problem statement
    - `next` involves two major operations. One is where we pop an element from the stack which becomes the next smallest element to return. This is a $$O(1)$$ operation. However, we then make a  call to our helper function `_inorder_left` which iterates over a bunch of nodes. This is clearly a linear time operation i.e. $$O(N)$$ in the worst case. This is true. 
    
        >However, the important thing to note here is that we only make such a call for nodes which have a right child. Otherwise, we simply return. Also, even if we end up calling the helper function, it won't always process N nodes. They will be much lesser. Only if we have a skewed tree would there be N nodes for the root. But that is the only node for which we would call the helper function. 
        
        Thus, the amortized (average) time complexity for this function would still be $$O(1)$$ which is what the question asks for. We don't need to have a solution which gives constant time operations for *every* call. We need that complexity on average and that is what we get. 
      
* Space complexity: The space complexity is $$O(h)$$ which is occupied by our custom stack for simulating the inorder traversal. Again, we satisfy the space requirements as well as specified in the problem statement.
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My solutions in 3 languages with Stack
- Author: xcv58
- Creation Date: Wed Dec 31 2014 13:44:17 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 01:16:55 GMT+0800 (Singapore Standard Time)

<p>
I use Stack to store directed left children from root.
When next() be called, I just pop one element and process its right child as new root.
The code is pretty straightforward.

So this can satisfy O(h) memory, hasNext() in O(1) time,
But next() is O(h) time.

I can't find a solution that can satisfy both next() in O(1) time, space in O(h).

Java:

    public class BSTIterator {
        private Stack<TreeNode> stack = new Stack<TreeNode>();
        
        public BSTIterator(TreeNode root) {
            pushAll(root);
        }
    
        /** @return whether we have a next smallest number */
        public boolean hasNext() {
            return !stack.isEmpty();
        }
    
        /** @return the next smallest number */
        public int next() {
            TreeNode tmpNode = stack.pop();
            pushAll(tmpNode.right);
            return tmpNode.val;
        }
        
        private void pushAll(TreeNode node) {
            for (; node != null; stack.push(node), node = node.left);
        }
    }

C++:


    class BSTIterator {
        stack<TreeNode *> myStack;
    public:
        BSTIterator(TreeNode *root) {
            pushAll(root);
        }
    
        /** @return whether we have a next smallest number */
        bool hasNext() {
            return !myStack.empty();
        }
    
        /** @return the next smallest number */
        int next() {
            TreeNode *tmpNode = myStack.top();
            myStack.pop();
            pushAll(tmpNode->right);
            return tmpNode->val;
        }
    
    private:
        void pushAll(TreeNode *node) {
            for (; node != NULL; myStack.push(node), node = node->left);
        }
    };


Python:

    class BSTIterator:
        # @param root, a binary search tree's root node
        def __init__(self, root):
            self.stack = list()
            self.pushAll(root)
    
        # @return a boolean, whether we have a next smallest number
        def hasNext(self):
            return self.stack
    
        # @return an integer, the next smallest number
        def next(self):
            tmpNode = self.stack.pop()
            self.pushAll(tmpNode.right)
            return tmpNode.val
            
        def pushAll(self, node):
            while node is not None:
                self.stack.append(node)
                node = node.left
</p>


### Ideal Solution using Stack (Java)
- Author: siyang2
- Creation Date: Thu Jan 01 2015 16:10:24 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 03:03:10 GMT+0800 (Singapore Standard Time)

<p>
My idea comes from this: My first thought was to use inorder traversal to put every node into an array, and then make an index pointer for the next() and hasNext(). That meets the O(1) run time but not the O(h) memory. O(h) is really much more less than O(n) when the tree is huge.

This means I cannot use a lot of memory, which suggests that I need to make use of the tree structure itself. And also, one thing to notice is the "average O(1) run time". It's weird to say average O(1), because there's nothing below O(1) in run time, which suggests in most cases, I solve it in O(1), while in some cases, I need to solve it in O(n) or O(h). These two limitations are big hints.

Before I come up with this solution, I really draw a lot binary trees and try inorder traversal on them. We all know that, once you get to a TreeNode, in order to get the smallest, you need to go all the way down its left branch. So our first step is to point to pointer to the left most TreeNode. The problem is how to do back trace. Since the TreeNode doesn't have father pointer, we cannot get a TreeNode's father node in O(1) without store it beforehand. Back to the first step, when we are traversal to the left most TreeNode, we store each TreeNode we met ( They are all father nodes for back trace). 

After that, I try an example, for next(), I directly return where the pointer pointing at, which should be the left most TreeNode I previously found. What to do next? After returning the smallest TreeNode, I need to point the pointer to the next smallest TreeNode. When the current TreeNode has a right branch (It cannot have left branch, remember we traversal to the left most), we need to jump to its right child first and then traversal to its right child's left most TreeNode. When the current TreeNode doesn't have a right branch, it means there cannot be a node with value smaller than itself father node, point the pointer at its father node.

The overall thinking leads to the structure Stack, which fits my requirement so well.

    /**
     * Definition for binary tree
     * public class TreeNode {
     *     int val;
     *     TreeNode left;
     *     TreeNode right;
     *     TreeNode(int x) { val = x; }
     * }
     */
    
    public class BSTIterator {
        
        private Stack<TreeNode> stack;
        public BSTIterator(TreeNode root) {
            stack = new Stack<>();
            TreeNode cur = root;
            while(cur != null){
                stack.push(cur);
                if(cur.left != null)
                    cur = cur.left;
                else
                    break;
            }
        }
    
        /** @return whether we have a next smallest number */
        public boolean hasNext() {
            return !stack.isEmpty();
        }
    
        /** @return the next smallest number */
        public int next() {
            TreeNode node = stack.pop();
            TreeNode cur = node;
            // traversal right branch
            if(cur.right != null){
                cur = cur.right;
                while(cur != null){
                    stack.push(cur);
                    if(cur.left != null)
                        cur = cur.left;
                    else
                        break;
                }
            }
            return node.val;
        }
    }
    
    /**
     * Your BSTIterator will be called like this:
     * BSTIterator i = new BSTIterator(root);
     * while (i.hasNext()) v[f()] = i.next();
     */
</p>


### Nice Comparison (and short Solution)
- Author: StefanPochmann
- Creation Date: Sun Jul 26 2015 18:30:46 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 12:24:17 GMT+0800 (Singapore Standard Time)

<p>
Compare this typical iterative inorder traversal

    1.    TreeNode visit = root;
          Stack<TreeNode> stack = new Stack();
    2.    while (visit != null || !stack.empty()) {
    3.        while (visit != null) {
                  stack.push(visit);
                  visit = visit.left;
              }
              TreeNode next = stack.pop();
              visit = next.right;
              doSomethingWith(next.val);
          }

with what we're supposed to support here:

    1.    BSTIterator i = new BSTIterator(root);
    2.    while (i.hasNext())
    3.        doSomethingWith(i.next());

You can see they already have the exact same structure:

1. Some **initialization**.
2. A while-loop with **a condition that tells whether there is more**.
3. The loop body **gets the next value** and does something with it.

So simply put the three parts of that iterative solution into our three iterator methods:

    public class BSTIterator {
    
        private TreeNode visit;
        private Stack<TreeNode> stack;
        
        public BSTIterator(TreeNode root) {
            visit = root;
            stack = new Stack();
        }
    
        public boolean hasNext() {
            return visit != null || !stack.empty();
        }
    
        public int next() {
            while (visit != null) {
                stack.push(visit);
                visit = visit.left;
            }
            TreeNode next = stack.pop();
            visit = next.right;
            return next.val;
        }
    }
</p>


