---
title: "Bulb Switcher IV"
weight: 1400
#id: "bulb-switcher-iv"
---
## Description
<div class="description">
<p>There is a room with <code>n</code>&nbsp;bulbs, numbered from <code>0</code> to&nbsp;<code>n-1</code>,&nbsp;arranged in a row from left to right. Initially all the bulbs are <strong>turned off</strong>.</p>

<p>Your task is to obtain the configuration represented by <code>target</code> where&nbsp;<code>target[i]</code> is &#39;1&#39; if the i-th bulb is turned on and is &#39;0&#39; if it is turned off.</p>

<p>You have a switch&nbsp;to flip the state of the bulb,&nbsp;a flip operation is defined as follows:</p>

<ul>
	<li>Choose <strong>any</strong> bulb (index&nbsp;<code>i</code>)&nbsp;of your current configuration.</li>
	<li>Flip each bulb from index&nbsp;<code>i</code> to&nbsp;<code>n-1</code>.</li>
</ul>

<p>When any bulb is flipped it means that if it is 0 it changes to 1 and if it is 1 it changes to 0.</p>

<p>Return the <strong>minimum</strong> number of flips required to form <code>target</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> target = &quot;10111&quot;
<strong>Output:</strong> 3
<strong>Explanation: </strong>Initial configuration &quot;00000&quot;.
flip from the third bulb:  &quot;00000&quot; -&gt; &quot;00111&quot;
flip from the first bulb:  &quot;00111&quot; -&gt; &quot;11000&quot;
flip from the second bulb:  &quot;11000&quot; -&gt; &quot;10111&quot;
We need at least 3 flip operations to form target.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> target = &quot;101&quot;
<strong>Output:</strong> 3
<strong>Explanation: </strong>&quot;000&quot; -&gt; &quot;111&quot; -&gt; &quot;100&quot; -&gt; &quot;101&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> target = &quot;00000&quot;
<strong>Output:</strong> 0
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> target = &quot;001011101&quot;
<strong>Output:</strong> 5
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= target.length &lt;= 10^5</code></li>
	<li><code>target[i] == &#39;0&#39;</code>&nbsp;or <code>target[i] == &#39;1&#39;</code></li>
</ul>

</div>

## Tags
- String (string)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++, Python, Java: Readable easy code with explanation & code comments
- Author: interviewrecipes
- Creation Date: Sun Jul 26 2020 12:15:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 08 2020 13:08:56 GMT+0800 (Singapore Standard Time)

<p>
**Key idea**
Flip is a must when current status of the bulbs and target status are not same.

Initially all bulbs are at 0 and when a bulb at index i is flipped, everything that comes after that gets flipped. So it only makes sense to keep getting the status of the bulbs correct from left to right. 

We will keep track of the status of the bulbs as we make flips.

Initially all bulbs are 0, so say status = 0.
Bulb 0 is 0 initially. 
1. If we want it to be 0, we don\'t have to make a flip.
2. If we want it to be 1, we must make a flip. This will change the status of remaining bulbs and they will be ON i.e. 1, so we will also make status as 1.
Whenever we see the status to be different from what the target is, we must make a flip and this will also change the status of remaining bulbs.

**Thanks**
Please upvote if you like it -
1. It encourages me to write on Leetcode and on my blog/website. 
2. Learners get to see the post at the top.
2. It stays at the top so even if people downvote it, it carries little effect. Its so annoying really.

Thank you :)

**More**
Well, if you find this helpful, you will likely find my blog helpful. 

**C++**
```
class Solution {
public:
    int minFlips(string target) {
        int n = target.size();                          // Total bulbs.
        int flips = 0;                                  // Final answer.
        char status = \'0\';                              // This stores the status of bulbs that are
                                                        // ahead of current index `i`.
        for (int i=0; i<n; i++) {                       // For each bulb =
            if (status != target[i]) {                  // If what we want is different from what
                                                        // it is at this moment, make a flip.
                flips++;                                // We made a flip.
                status = status == \'0\' ?  \'1\' : \'0\';    // Now status of remaining
                                                        // bulbs have changed.
            }
        }
        return flips; // Awesome, return the answer now.
    }
};
```

**Python**
```
class Solution(object):
    def minFlips(self, target):
        flips = 0
        status = \'0\'
        for c in target:
            if c != status:
                flips += 1
                status = \'0\' if status == \'1\' else \'1\'
        return flips
        
```
**Java**
```
class Solution {
    public int minFlips(String target) {
        int n = target.length();
        int flips = 0;
        char status = \'0\';
        for (int i = 0; i < n; i++) {
            if (status != target.charAt(i)) {
                flips++;
            }
            status = flips % 2 == 1 ? \'1\' : \'0\'; 
        }
        return flips;
    }
}
```
</p>


### C++/Java O(n)
- Author: votrubac
- Creation Date: Sun Jul 26 2020 12:04:05 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 27 2020 16:06:10 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**
The order of bit operations does not matter. Go from left to right, switching bulbs greedily.

Similar question: [995. Minimum Number of K Consecutive Bit Flips](https://leetcode.com/problems/minimum-number-of-k-consecutive-bit-flips/). I provided more detailed explanations in [my solution there](https://leetcode.com/problems/minimum-number-of-k-consecutive-bit-flips/discuss/239284/C%2B%2B-greedy-stack-and-O(1)-memory).

**Algorithm**
Go from left to right, tracking `state` of the remaining bulbs. When a bulb does not match the state, we change the state and increment the result.

Example:
```
Bulbs: [0, 1, 1, 0, 1, 1]
State:  0 !1  1 !0 !1  1
Result: 0  1  1  2  3  3
```
This corresponds to these underlying three transitions: 
```
[0, 0, 0, 0, 0, 0] => [0, 1, 1, 1, 1, 1] => [0, 1, 1, 0, 0, 0] => [0, 1, 1, 0, 1, 1]
```

**C++**
```cpp
int minFlips(string target) {
    int cnt = 0, state = 0;
    for (auto b : target)
        if (b - \'0\' != state) {
            state = b - \'0\';
            ++cnt;
        }
    return cnt;
}
```

**Java**
```java
public int minFlips(String target) {
    int cnt = 0, state = 0;
    for (var b : target.toCharArray())
        if (b - \'0\' != state) {
            state = b - \'0\';
            ++cnt;
        }
    return cnt;
}
```
**Complexity Analysis**
- Time: O(n)
- Memory: O(1)
</p>


### Java 1 loop O(N)
- Author: hobiter
- Creation Date: Sun Jul 26 2020 12:01:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 12:01:32 GMT+0800 (Singapore Standard Time)

<p>
switch from head of the array, every time next value is different with prev value, you need to flip again.
init prev as \u20180\u2019.
```
    public int minFlips(String target) {
        char prev = \'0\';
        int res = 0;
        for (char c : target.toCharArray()) {
            if (c != prev) {
                prev = c;
                res++;
            }
        }
        return res;
    }
```

</p>


