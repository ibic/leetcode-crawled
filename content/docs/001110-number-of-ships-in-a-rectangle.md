---
title: "Number of Ships in a Rectangle"
weight: 1110
#id: "number-of-ships-in-a-rectangle"
---
## Description
<div class="description">
<p><em>(This problem is an&nbsp;<strong>interactive problem</strong>.)</em></p>

<p>On the sea represented by a cartesian plane, each ship is located at an integer point, and each integer point may contain at most 1 ship.</p>

<p>You have a function <code>Sea.hasShips(topRight, bottomLeft)</code> which takes two points&nbsp;as arguments and returns <code>true</code>&nbsp;if and only if there is at least one ship in the rectangle represented by the two points, including on the boundary.</p>

<p>Given two points, which are the top right and bottom left corners of a rectangle, return the number of ships present in that rectangle.&nbsp;&nbsp;It is guaranteed that there are <strong>at most 10 ships</strong> in that rectangle.</p>

<p>Submissions making <strong>more than 400 calls</strong> to&nbsp;<code>hasShips</code>&nbsp;will be judged <em>Wrong Answer</em>.&nbsp; Also, any solutions that attempt to circumvent the judge&nbsp;will result in disqualification.</p>

<p>&nbsp;</p>
<p><strong>Example :</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/07/26/1445_example_1.PNG" style="width: 400px; height: 404px;" /></p>

<pre>
<strong>Input:</strong> 
ships = [[1,1],[2,2],[3,3],[5,5]], topRight = [4,4], bottomLeft = [0,0]
<strong>Output:</strong> 3
<strong>Explanation:</strong> From [0,0] to [4,4] we can count 3 ships within the range.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>On the input <code>ships</code> is only given to initialize the map internally.&nbsp;You must solve this problem &quot;blindfolded&quot;. In other words, you must find the answer using the given <code>hasShips</code> API, without knowing the <code>ships</code>&nbsp;position.</li>
	<li><code>0 &lt;=&nbsp;bottomLeft[0]&nbsp;&lt;= topRight[0]&nbsp;&lt;= 1000</code></li>
	<li><code>0 &lt;=&nbsp;bottomLeft[1]&nbsp;&lt;= topRight[1]&nbsp;&lt;= 1000</code></li>
</ul>

</div>

## Tags
- Divide and Conquer (divide-and-conquer)

## Companies
- Bloomberg - 14 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Simple divide and conquer solution with explanation
- Author: Sun_WuKong
- Creation Date: Sun Dec 01 2019 00:02:30 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 02 2019 20:49:08 GMT+0800 (Singapore Standard Time)

<p>
We divide the current rectangle into 4 pieces in the middle.

Base case: when topRight == bottomLeft, meaning our rectangle reduces into a point in the map. We return 1 if hasShips(topRight, bottomLeft)

Time complexity: O(n) where n is total number of points inside the rectangle
T(n) = 4xT(n/4) + O(1)
Apply master theorem: n^(log(4)4) = n is O(O(1)). So T(n) = O(n)

```
class Solution {
    public int countShips(Sea sea, int[] topRight, int[] bottomLeft) {
        if (!sea.hasShips(topRight, bottomLeft)) return 0;
        if (topRight[0] == bottomLeft[0] && topRight[1] == bottomLeft[1]) return 1;
        
        int midX = (topRight[0] + bottomLeft[0])/2;
        int midY = (topRight[1] + bottomLeft[1])/2;
        return countShips(sea, new int[]{midX, midY}, bottomLeft) +
            countShips(sea, topRight, new int[]{midX+1, midY+1}) +
            countShips(sea, new int[]{midX, topRight[1]}, new int[]{bottomLeft[0], midY+1}) +
            countShips(sea, new int[]{topRight[0], midY}, new int[]{midX+1, bottomLeft[1]});
    }
}
```
</p>


### Python
- Author: StefanPochmann
- Creation Date: Sun Dec 01 2019 01:01:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 01 2019 01:52:16 GMT+0800 (Singapore Standard Time)

<p>
Return 0 or 1 for trivial cases, otherwise split into four quadrants.
```
def countShips(self, sea, topRight, bottomLeft):
    def count((x, X), (y, Y)):
        if x > X or y > Y or not sea.hasShips(Point(X, Y), Point(x, y)):
            return 0
        if (x, y) == (X, Y):
            return 1
        xm = (x + X) / 2
        ym = (y + Y) / 2
        xRanges = (x, xm), (xm+1, X)
        yRanges = (y, ym), (ym+1, Y)
        return sum(count(xr, yr) for xr in xRanges for yr in yRanges)
    return count((bottomLeft.x, topRight.x), (bottomLeft.y, topRight.y))
```
</p>


### [Python] Quartered Search and O(1) hack for fun
- Author: lee215
- Creation Date: Sun Dec 01 2019 12:27:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 01 2019 12:27:39 GMT+0800 (Singapore Standard Time)

<p>
# Solution 1: Quartered Search
Time `O(10logMN)`
<br>

**Python:**
```python
    def countShips(self, sea, P, Q):
        res = 0
        if P.x >= Q.x and P.y >= Q.y and sea.hasShips(P, Q):
            if P.x == Q.x and P.y == Q.y: return 1
            mx, my = (P.x + Q.x) / 2, (P.y + Q.y) / 2
            res += self.countShips(sea, P, Point(mx + 1, my + 1))
            res += self.countShips(sea, Point(mx, P.y), Point(Q.x, my + 1))
            res += self.countShips(sea, Point(mx, my), Q)
            res += self.countShips(sea, Point(P.x, my), Point(mx + 1, Q.y))
        return res
```
<br>

# Solution 2: 1-line Hack
Just for staying fun, to welcome Ste-fan.
Time `O(1)`
<br>

```
    def countShips(self, sea, P, Q):
        return sum(Q.x <= x <= P.x and Q.y <= y <= P.y for x, y in sea._Sea__ans)
```
<br>
</p>


