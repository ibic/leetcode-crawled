---
title: "Sort Integers by The Power Value"
weight: 1276
#id: "sort-integers-by-the-power-value"
---
## Description
<div class="description">
<p>The power of an integer <code>x</code> is defined as the number of steps needed to transform&nbsp;<code>x</code> into <code>1</code> using the following steps:</p>

<ul>
	<li>if <code>x</code> is even then <code>x = x / 2</code></li>
	<li>if <code>x</code> is odd then <code>x = 3 * x + 1</code></li>
</ul>

<p>For example, the power of x = 3 is 7 because 3 needs 7 steps to become 1 (3 --&gt; 10 --&gt; 5 --&gt; 16 --&gt; 8 --&gt; 4 --&gt; 2 --&gt; 1).</p>

<p>Given three integers <code>lo</code>, <code>hi</code> and <code>k</code>. The task is to sort all integers in the interval <code>[lo, hi]</code> by the power value in <strong>ascending order</strong>, if two or more integers have <strong>the same</strong> power value sort them by <strong>ascending order</strong>.</p>

<p>Return the <code>k-th</code> integer in the range <code>[lo, hi]</code> sorted by the power value.</p>

<p>Notice that for any&nbsp;integer <code>x</code> <code>(lo &lt;= x &lt;= hi)</code> it is <strong>guaranteed</strong> that <code>x</code> will transform into <code>1</code> using these steps and that the power of <code>x</code> is will <strong>fit</strong> in 32 bit signed integer.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> lo = 12, hi = 15, k = 2
<strong>Output:</strong> 13
<strong>Explanation:</strong> The power of 12 is 9 (12 --&gt; 6 --&gt; 3 --&gt; 10 --&gt; 5 --&gt; 16 --&gt; 8 --&gt; 4 --&gt; 2 --&gt; 1)
The power of 13 is 9
The power of 14 is 17
The power of 15 is 17
The interval sorted by the power value [12,13,14,15]. For k = 2 answer is the second element which is 13.
Notice that 12 and 13 have the same power value and we sorted them in ascending order. Same for 14 and 15.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> lo = 1, hi = 1, k = 1
<strong>Output:</strong> 1
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> lo = 7, hi = 11, k = 4
<strong>Output:</strong> 7
<strong>Explanation:</strong> The power array corresponding to the interval [7, 8, 9, 10, 11] is [16, 3, 19, 6, 14].
The interval sorted by power is [8, 10, 11, 7, 9].
The fourth number in the sorted array is 7.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> lo = 10, hi = 20, k = 5
<strong>Output:</strong> 13
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> lo = 1, hi = 1000, k = 777
<strong>Output:</strong> 570
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= lo &lt;= hi &lt;= 1000</code></li>
	<li><code>1 &lt;= k &lt;= hi - lo + 1</code></li>
</ul>
</div>

## Tags
- Sort (sort)
- Graph (graph)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ 4 ms - precompute and partial sort
- Author: votrubac
- Creation Date: Sun Mar 22 2020 09:52:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 01 2020 03:15:47 GMT+0800 (Singapore Standard Time)

<p>
We can use a static array to precompute powers for numbers [1..1000]. To speed-up pre-computation, I initially used memosiation, but it did not make a difference in OJ (perhaps, it might if the range is wider).

Also, we do not need to sort the entre subrange; we can use a partial sort (like `nth_element` in C++).

> Note: I need to put the static array outside of the solution class; you\'ll need to define it if you want to put it inside.
> Another note: I provide only the first 3 elements of `comp` to cut the initialization time. We\'ll populate the rest in the `compute` function. The `comp[2]` will be set to `1` afterwards, so we\'ll know that `comp` is ready to use.
```cpp
int comp[1001] = { 0, 0, 0 };
class Solution {
    int compute(int i) {
    return i < 2 ? 0 :
        1 + (i % 2 ? compute(i * 3 + 1) : compute(i / 2));
    }
public:
    int getKth(int lo, int hi, int k) {
        if (comp[2] == 0)
            for (auto i = 2; i <= 1000; ++i)
                comp[i] = compute(i);
        vector<int> sorted(hi - lo + 1);
        iota(begin(sorted), end(sorted), lo);
        nth_element(begin(sorted), begin(sorted) + k - 1, end(sorted), [](int i, int j) {
            return comp[i] == comp[j] ? i < j : comp[i] < comp[j]; });
        return sorted[k - 1];
    }
};
```
</p>


### Java PQ
- Author: Poorvank
- Creation Date: Mon May 04 2020 22:26:37 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 04 2020 22:26:37 GMT+0800 (Singapore Standard Time)

<p>
```
Map<Integer,Integer> map; // Store steps for each value to reach 1. So that it can be reused.

    public int getKth(int lo, int hi, int k) {
        map = new HashMap<>();
        PriorityQueue<int[]> pq =
                new PriorityQueue<>((a,b)-> b[1]!=a[1] ?a[1]-b[1]:a[0]-b[0]);
        for (int i=lo;i<=hi;i++) {
            pq.add(new int[]{i,util(i)});
        }
        while (!pq.isEmpty() && k-->1) { 
            pq.poll();
        }
        return pq.poll()[0];
    }

    private int util(int val) {
        if(val==1) {
            return 0;
        }
        if(map.containsKey(val)) {
            return map.get(val);
        }
        if((val&1)==0) {
            map.put(val,util(val/2)+1);
        } else {
            map.put(val,util(3*val+1)+1);
        }
        return map.get(val);
    }
```
</p>


### [C++] Share concise solution
- Author: windclip
- Creation Date: Sun Mar 22 2020 00:34:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2020 00:34:02 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    int getKth(int lo, int hi, int k) {
        vector<pair<int,int>> v;
        for(int i=lo;i<=hi;i++){
            v.push_back({power(i),i});
        }
        sort(v.begin(),v.end());
        return v[k-1].second;
    }
    int power(int num){
        int cnt=0;
        while(num!=1){
            if(num%2==0){
                num=num/2;
            }
            else{
                num=3*num+1;
            }
            cnt++;
        }
        return cnt;
    }
};
```
</p>


