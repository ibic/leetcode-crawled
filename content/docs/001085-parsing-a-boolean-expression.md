---
title: "Parsing A Boolean Expression"
weight: 1085
#id: "parsing-a-boolean-expression"
---
## Description
<div class="description">
<p>Return the result of evaluating a given boolean <code>expression</code>, represented as a string.</p>

<p>An expression can either be:</p>

<ul>
	<li><code>&quot;t&quot;</code>, evaluating to <code>True</code>;</li>
	<li><code>&quot;f&quot;</code>, evaluating to <code>False</code>;</li>
	<li><code>&quot;!(expr)&quot;</code>, evaluating to the logical NOT of the inner expression <code>expr</code>;</li>
	<li><code>&quot;&amp;(expr1,expr2,...)&quot;</code>, evaluating to the logical AND of 2 or more inner expressions <code>expr1, expr2, ...</code>;</li>
	<li><code>&quot;|(expr1,expr2,...)&quot;</code>, evaluating to the logical OR of 2 or more inner expressions <code>expr1, expr2, ...</code></li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> expression = &quot;!(f)&quot;
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> expression = &quot;|(f,t)&quot;
<strong>Output:</strong> true
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> expression = &quot;&amp;(t,f)&quot;
<strong>Output:</strong> false
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> expression = &quot;|(&amp;(t,f,t),!(t))&quot;
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= expression.length &lt;= 20000</code></li>
	<li><code>expression[i]</code>&nbsp;consists of characters in <code>{&#39;(&#39;, &#39;)&#39;, &#39;&amp;&#39;, &#39;|&#39;, &#39;!&#39;, &#39;t&#39;, &#39;f&#39;, &#39;,&#39;}</code>.</li>
	<li><code>expression</code> is a valid expression representing a boolean, as given in the description.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Affinity - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Easy 1-line Cheat
- Author: lee215
- Creation Date: Sun Jun 30 2019 12:03:55 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 04 2019 11:04:27 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Well, we can see that `&`, `|` and `!` are just three functions.
And in python, they are function `all`, `any` and keyword `not`.

## **Explanation**
Following the description,
it demands us to evaluate the expression.
So no recursion and no stack, I just `eval` the expression.
<br>

## **Complexity**
Time `O(N)`
Sapce `O(N)`
I guess it\'s not fast compared with string parse, but wow it\'s `O(N)`.
<br>

**Python:**
```
    def parseBoolExpr(self, S, t=True, f=False):
        return eval(S.replace(\'!\', \'not |\').replace(\'&(\', \'all([\').replace(\'|(\', \'any([\').replace(\')\', \'])\'))
```
</p>


### [Java/Python 3] Iterative and recursive solutions w/ explanation and analysis.
- Author: rock
- Creation Date: Sun Jun 30 2019 14:59:28 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 05 2019 14:39:45 GMT+0800 (Singapore Standard Time)

<p>
**Method 1:** Iterative version - Use Stack and Set.

Loop through the input String:
1. Use a stack to store chars except `\',\'` and `\')\'`;
2. If we find a `\')\'`, keep popping out the chars from the stack till find a `\'(\'`; add the popped-out into a Set.
3. Pop out the `operator` after popping `\')\'`  out, push into stack the corresponding result according to the `operator`.
4. repeat the above till the end, and the remaining is the result.

**[Java]**

```
    public boolean parseBoolExpr(String expression) {
        Deque<Character> stk = new ArrayDeque<>();
        for (int i = 0; i < expression.length(); ++i) {
            char c = expression.charAt(i);
            if (c == \')\') {
                Set<Character> seen = new HashSet<>();
                while (stk.peek() != \'(\')
                    seen.add(stk.pop());
                stk.pop();// pop out \'(\'.
                char operator = stk.pop(); // get operator for current expression.
                if (operator == \'&\') {
                    stk.push(seen.contains(\'f\') ? \'f\' : \'t\'); // if there is any \'f\', & expression results to \'f\'
                }else if (operator == \'|\') {
                    stk.push(seen.contains(\'t\') ? \'t\' : \'f\'); // if there is any \'t\', | expression results to \'t\'
                }else { // ! expression.
                    stk.push(seen.contains(\'t\') ? \'f\' : \'t\'); // Logical NOT flips the expression.
                }
            }else if (c != \',\') {
                stk.push(c);
            }
        }
        return stk.pop() == \'t\';
    }
```
**[Python 3]** credit to: **@zengfei216 and @cenkay**

```
    def parseBoolExpr(self, expression: str) -> bool:
        stack = []
        for c in expression:
            if c == \')\':
                seen = set()
                while stack[-1] != \'(\':
                    seen.add(stack.pop())
                stack.pop()
                operator = stack.pop()
                stack.append(all(seen) if operator == \'&\' else any(seen) if operator == \'|\' else not seen.pop())
            elif c != \',\':
                stack.append(True if c == \'t\' else False if c == \'f\' else c)
        return stack.pop()
```
----
**Method 2:** Recursive version.

Use `level` to count the non-matched `(` and `)`, together with `,`, we can delimit valid sub-expression and hence recurse to sub-problem.


**[Java]**


```
    public boolean parseBoolExpr(String expression) {
        return parse(expression, 0, expression.length());
    }
    private boolean parse(String s, int lo, int hi) {
        char c = s.charAt(lo);
        if (hi - lo == 1) return c == \'t\'; // base case.
        boolean ans = c == \'&\'; // only when c is &, set ans to true; otherwise false.
        for (int i = lo + 2, start = i, level = 0; i < hi; ++i) {
            char d = s.charAt(i);
            if (level == 0 && (d == \',\' || d == \')\')) { // locate a valid sub-expression. 
                boolean cur = parse(s, start, i); // recurse to sub-problem.
                start = i + 1; // next sub-expression start index.
                if (c == \'&\') ans &= cur;
                else if (c == \'|\') ans |= cur;
                else ans = !cur; // c == \'!\'.
            }
            if (d == \'(\') ++level;
            if (d == \')\') --level;
        }
        return ans;
    }
```
**[Python 3]**

```
    def parseBoolExpr(self, expression: str) -> bool:
        
        def parse(e: str, lo: int, hi: int) -> bool:
            if hi - lo == 1: # base case
                return e[lo] == \'t\'               
            ans = e[lo] == \'&\' # only when the first char is \'&\', ans assigned True.
            level, start = 0, lo + 2 # e[lo + 1] must be \'(\', so start from lo + 2 to delimit sub-expression.
            for i in range(lo + 2, hi):
                if level == 0  and e[i] in [\',\', \')\']: # found a sub-expression.
                    cur = parse(e, start, i) # recurse to sub-problem.
                    start = i + 1 # start point of next sub-expression.
                    if e[lo] == \'&\':
                        ans &= cur
                    elif e[lo] == \'|\':
                        ans |= cur
                    else: # e[lo] is \'!\'.
                        ans = not cur
                if e[i] == \'(\':
                    level = level + 1
                elif e[i] == \')\':
                    level = level - 1
            return ans;        
        
        return parse(expression, 0, len(expression))
```
**Analysis:**
Time & space: O(n), n = expression.length().


</p>


### c++ recursive descent  O(n) solution
- Author: hanzhoutang
- Creation Date: Sun Jun 30 2019 12:20:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 30 2019 12:20:31 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    bool f_not(const string& s,int& i){
        i+=2;
        auto ret = f(s,i);
        i++;
        return !ret;
    }
    
    bool f_and(const string& s,int& i){
        i+=2;
        bool ret = true;
        ret &= f(s,i);
        while(s[i]!=\')\'){
            i++;
            ret &= f(s,i);
        }
        i++;
        return ret;
    }
    
    bool f_or(const string& s,int& i){
        i+=2;
        bool ret = false;
        ret |= f(s,i);
        while(s[i]!=\')\'){
            i++;
            ret |= f(s,i);
        }
        i++;
        return ret;
    }
    
    bool f(const string& s, int& i){
        if(s[i] == \'t\'){
            i++;
            return true;
        } else if(s[i] == \'f\'){
            i++;
            return false;
        } else if(s[i] == \'!\'){
            return f_not(s,i);
        } else if(s[i] == \'&\'){
            return f_and(s,i);
        } return f_or(s,i);
    }
    bool parseBoolExpr(string expression) {
        int i = 0;
        return f(expression,i);
    }
};
```
</p>


