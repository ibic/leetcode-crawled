---
title: "Decoded String at Index"
weight: 830
#id: "decoded-string-at-index"
---
## Description
<div class="description">
<p>An encoded string <code>S</code> is given.&nbsp; To find and write the <em>decoded</em> string to a tape, the encoded string is read <strong>one character at a time</strong>&nbsp;and the following steps are taken:</p>

<ul>
	<li>If the character read is a letter, that letter is written onto the tape.</li>
	<li>If the character read is a digit (say <code>d</code>), the entire current tape is repeatedly written&nbsp;<code>d-1</code>&nbsp;more times in total.</li>
</ul>

<p>Now for some encoded string <code>S</code>, and an index <code>K</code>, find and return the <code>K</code>-th letter (1 indexed) in the decoded string.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-1-1">&quot;leet2code3&quot;</span>, K = <span id="example-input-1-2">10</span>
<strong>Output: </strong><span id="example-output-1">&quot;o&quot;</span>
<strong>Explanation: </strong>
The decoded string is &quot;leetleetcodeleetleetcodeleetleetcode&quot;.
The 10th letter in the string is &quot;o&quot;.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-2-1">&quot;ha22&quot;</span>, K = <span id="example-input-2-2">5</span>
<strong>Output: </strong><span id="example-output-2">&quot;h&quot;</span>
<strong>Explanation: </strong>
The decoded string is &quot;hahahaha&quot;.  The 5th letter is &quot;h&quot;.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-3-1">&quot;a2345678999999999999999&quot;</span>, K = <span id="example-input-3-2">1</span>
<strong>Output: </strong><span id="example-output-3">&quot;a&quot;</span>
<strong>Explanation: </strong>
The decoded string is &quot;a&quot; repeated 8301530446056247680 times.  The 1st letter is &quot;a&quot;.
</pre>
</div>
</div>
</div>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= S.length &lt;= 100</code></li>
	<li><code>S</code>&nbsp;will only contain lowercase letters and digits <code>2</code> through <code>9</code>.</li>
	<li><code>S</code>&nbsp;starts with a letter.</li>
	<li><code>1 &lt;= K &lt;= 10^9</code></li>
	<li>It&#39;s guaranteed that <code>K</code>&nbsp;is less than or equal to the length of the decoded string.</li>
	<li>The decoded string is guaranteed to have less than <code>2^63</code> letters.</li>
</ul>

</div>

## Tags
- Stack (stack)

## Companies
- National Instruments - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Work Backwards

**Intuition**

If we have a decoded string like `appleappleappleappleappleapple` and an index like `K = 24`, the answer is the same if `K = 4`.

In general, when a decoded string is equal to some word with `size` length repeated some number of times (such as `apple` with `size = 5` repeated 6 times), the answer is the same for the index `K` as it is for the index `K % size`.

We can use this insight by working backwards, keeping track of the size of the decoded string.  Whenever the decoded string would equal some `word` repeated `d` times, we can reduce `K` to `K % (word.length)`.

**Algorithm**

First, find the length of the decoded string.  After, we'll work backwards, keeping track of `size`: the length of the decoded string after parsing symbols `S[0], S[1], ..., S[i]`.

If we see a digit `S[i]`, it means the size of the decoded string after parsing `S[0], S[1], ..., S[i-1]` will be `size / Integer(S[i])`.  Otherwise, it will be `size - 1`.


<iframe src="https://leetcode.com/playground/S7Ty5GF3/shared" frameBorder="0" width="100%" height="500" name="S7Ty5GF3"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Python] O(N) Time O(1) Space
- Author: lee215
- Creation Date: Sun Aug 05 2018 11:07:14 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 17:55:45 GMT+0800 (Singapore Standard Time)

<p>
We decode the string and `N` keeps the length of decoded string, until `N >= K`.
Then we go back from the decoding position.
If it\'s `S[i] = d` is a digit, then `N = N / d` before repeat and `K  = K % N` is what we want.
If it\'s `S[i] = c` is a character, we return `c` if `K == 0` or `K == N`

**C++:**
```
    string decodeAtIndex(string S, int K) {
        long N = 0, i;
        for (i = 0; N < K; ++i)
            N = isdigit(S[i]) ? N * (S[i] - \'0\') : N + 1;
        while (i--)
            if (isdigit(S[i]))
                N /= S[i] - \'0\', K %= N;
            else if (K % N-- == 0)
                return string(1, S[i]);
        return "lee215";
    }
```

**Python:**
```
    def decodeAtIndex(self, S, K):
        N = 0
        for i, c in enumerate(S):
            N = N * int(c) if c.isdigit() else N + 1
            if K <= N: break
        for j in range(i, -1, -1):
            c = S[j]
            if c.isdigit():
                N /= int(c)
                K %= N
            else:
                if K == N or K == 0: return c
                N -= 1
```
</p>


### Logical Thinking with Clear Code
- Author: GraceMeng
- Creation Date: Tue Aug 07 2018 08:34:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 09:51:25 GMT+0800 (Singapore Standard Time)

<p>
**Logical Thinking**
Only the character in the Kth position matters rather than the whole string decoded, so we only keep track of `curLength` (`long` data type to avoid integer overflow).
Let\'s see the code commented, first `a1, a2`, then `b1, b2`. They are matching except that we manage K in `a2, b2`.
Since the result character can only be a letter rather than digit, in `b2` we ought to check K. Whenever `K % curLength == 0`, we figure out the result.

**Clear Code**
```
    public String decodeAtIndex(String S, int K) {
        
        long curLength = 0;
        char c = 0;
        
        for (int i = 0; i < S.length(); i++) {
            c = S.charAt(i);
            if (Character.isDigit(c)) { \\ a1
                curLength *= c - \'0\';
            }
            else { \\ b1
                curLength++;
            }
        }
        
        for (int i = S.length() - 1; i >= 0; i--) {
            c = S.charAt(i);
            if (Character.isDigit(c)) { \\ a2
                curLength /= c - \'0\';
                K %= curLength;
            }
            else { \\ b2
                if (K == 0 || K == curLength) {
                    return "" + c;
                }
                curLength--;
            }
        }
        
        throw null;
    }
```
**I would appreciate your VOTE UP ;)**
</p>


### C++ simple recursion
- Author: votrubac
- Creation Date: Sun Aug 05 2018 11:04:27 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 10 2018 04:05:54 GMT+0800 (Singapore Standard Time)

<p>
As we go through the string, we increment ```len``` as we encounter a letter, and multiply it if we see a number. 

- If, after the increment, ```len``` is equal ```K```, we return the current letter. 
- If ```len``` after the multiply is equal or greater than ```K```, we call our function recursively for ```K % len```.
```
string decodeAtIndex(string &S, int K, long long len = 0) {
  for (auto i = 0; i < S.size(); ++i) {
    if (isalpha(S[i])) {
      if (++len == K) return string(1, S[i]);
    }
    else {
      if (len * (S[i] - \'0\') >= K) return decodeAtIndex(S, K % len == 0 ? len : K % len);
      len *= S[i] - \'0\';
    }
  }
}
```
</p>


