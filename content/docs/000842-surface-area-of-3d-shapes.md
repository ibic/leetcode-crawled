---
title: "Surface Area of 3D Shapes"
weight: 842
#id: "surface-area-of-3d-shapes"
---
## Description
<div class="description">
<p>On a&nbsp;<code>N&nbsp;*&nbsp;N</code>&nbsp;grid, we place some&nbsp;<code>1 * 1 * 1&nbsp;</code>cubes.</p>

<p>Each value&nbsp;<code>v = grid[i][j]</code>&nbsp;represents a tower of&nbsp;<code>v</code>&nbsp;cubes placed on top of grid cell&nbsp;<code>(i, j)</code>.</p>

<p>Return the total surface area of the resulting shapes.</p>

<p>&nbsp;</p>

<div>
<div>
<div>
<ul>
</ul>
</div>
</div>
</div>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[2]]</span>
<strong>Output: </strong><span id="example-output-1">10</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[[1,2],[3,4]]</span>
<strong>Output: </strong><span id="example-output-2">34</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[[1,0],[0,2]]</span>
<strong>Output: </strong><span id="example-output-3">16</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">[[1,1,1],[1,0,1],[1,1,1]]</span>
<strong>Output: </strong><span id="example-output-4">32</span>
</pre>

<div>
<p><strong>Example 5:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-5-1">[[2,2,2],[2,1,2],[2,2,2]]</span>
<strong>Output: </strong><span id="example-output-5">46</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>1 &lt;= N &lt;= 50</code></li>
	<li><code>0 &lt;= grid[i][j] &lt;= 50</code></li>
</ul>
</div>
</div>
</div>
</div>
</div>

</div>

## Tags
- Math (math)
- Geometry (geometry)

## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach 1: Square by Square

**Intuition**

Let's try to count the surface area contributed by `v = grid[i][j]`.

When `v > 0`, the top and bottom surface contributes an area of 2.

Then, for each side (west side, north side, east side, south side) of the column at `grid[i][j]`, the neighboring cell with value `nv` means our square contributes `max(v - nv, 0)`.

For example, for `grid = [[1, 5]]`, the contribution at `grid[0][1]` is 2 + 5 + 5 + 5 + 4.  The 2 comes from the top and bottom side, the 5 comes from the north, east, and south side; and the 4 comes from the west side, of which 1 unit is covered by the adjacent column.

**Algorithm**

For each `v = grid[r][c] > 0`, count `ans += 2`, plus `ans += max(v - nv, 0)` for each neighboring value `nv` adjacent to `grid[r][c]`.

<iframe src="https://leetcode.com/playground/HDzCyHVN/shared" frameBorder="0" width="100%" height="497" name="HDzCyHVN"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the number of rows (and columns) in the `grid`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/1-line Python] Minus Hidden Area
- Author: lee215
- Creation Date: Sun Aug 26 2018 11:01:27 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 06:24:45 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**:
For each tower, its surface area is `4 * v + 2`
However, 2 adjacent tower will hide the area of connected part.
The hidden part is `min(v1, v2)` and we need just minus this area * 2

**Time Complexity**:
O(N^2)

**C++:**
```
    int surfaceArea(vector<vector<int>> grid) {
        int res = 0, n = grid.size();
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (grid[i][j]) res += grid[i][j] * 4 + 2;
                if (i) res -= min(grid[i][j], grid[i - 1][j]) * 2;
                if (j) res -= min(grid[i][j], grid[i][j - 1]) * 2;
            }
        }
        return res;
    }
```

**Java:**
```
    public int surfaceArea(int[][] grid) {
        int res = 0, n = grid.length;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (grid[i][j] > 0) res += grid[i][j] * 4 + 2;
                if (i > 0) res -= Math.min(grid[i][j], grid[i - 1][j]) * 2;
                if (j > 0) res -= Math.min(grid[i][j], grid[i][j - 1]) * 2;
            }
        }
        return res;
    }
```
**Python:**
```
    def surfaceArea(self, grid):
        n, res = len(grid), 0
        for i in range(n):
            for j in range(n):
                if grid[i][j]: res += 2 + grid[i][j] * 4
                if i: res -= min(grid[i][j], grid[i - 1][j]) * 2
                if j: res -= min(grid[i][j], grid[i][j - 1]) * 2
        return res
```

**1-line Python:**
```
    def surfaceArea(self, grid):
        return sum(v * 4 + 2 for row in grid for v in row if v) - sum(min(a, b) * 2 for row in grid + zip(*grid) for a, b in zip(row, row[1:]))
```
</p>


### 892. Surface Area of 3D Shapes. C++ solution with visual presentation
- Author: langrenn
- Creation Date: Tue Jan 07 2020 17:55:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2020 00:37:23 GMT+0800 (Singapore Standard Time)

<p>
Surface area of 1 cube is equal to 6 according to description (1 * 1 * 1) * 6, cube have 6 sides.
grid[i][j] is a count of cubes placed on top of grid cell. For instance, if we have a grid[i][j] = 2, it means we placed two cubes one on other and they have a common area is equal to 2, because we have two connected sides. If the grid[i][j] = 3, then common area is equal to 4. 
It turns out when grid[i][j] is equal to:
2 cubes - 2
3 cubes - 4
4 cubes - 6
5 cubes - 8 => **2 * (grid[i][j] - 1)**
We also have to substract common area on **y** and **x** axis. We only compute up to **n - 1** for both axis by calculating minimum of two connected **V**s and multiply by 2.
Red squares is a common areas which we have to subtract from resulting total surface area.
The following depicted picture shows example: [[2, 3, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]. The answer is: 5 * 6 - 2 - 4 = 20.
![image](https://assets.leetcode.com/users/langrenn/image_1578395952.png)


```
int surfaceArea(vector<vector<int>>& grid) {
	int n = grid.size();
	int res = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (grid[i][j] == 0) continue;
			res += grid[i][j] * 6 - 2 * (grid[i][j] - 1); 
			if (i < n - 1) res -= 2 * min(grid[i][j], grid[i+1][j]);
			if (j < n - 1) res -= 2 * min(grid[i][j], grid[i][j+1]); 
		}
	}
	return res;
}
```
**Complexity Analysis**:

Time Complexity: **O(n\xB2)**, where N is the number of rows (and columns) in the grid.

Space Complexity: **O(1)**.
</p>


### Difficulty should be Medium not Easy
- Author: martinvelez
- Creation Date: Sun Aug 26 2018 12:25:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 26 2018 12:25:31 GMT+0800 (Singapore Standard Time)

<p>
I think this problem should be labeled as Medium.  (This is under the assumption that problems are manually labeled.) . I took a long time to solve this problem. I may have been misled by initial intuition which was to find the maximum of each row and the maximum of each column.  That gives me *part* of the surface area visible from x and y.  I developed a complex way of counting the surface area not visible from x and y.  The surface area of z is just the count of nonzero values * 2.

I have read others\' solution and, in hindsight, they are much more straight forward.  Essentially, compute the surface area of each grid but substract the overlapping areas.


</p>


