---
title: "Merge Two Sorted Lists"
weight: 21
#id: "merge-two-sorted-lists"
---
## Description
<div class="description">
<p>Merge two sorted linked lists and return it as a new <strong>sorted</strong> list. The new list should be made by splicing together the nodes of the first two lists.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/10/03/merge_ex1.jpg" style="width: 662px; height: 302px;" />
<pre>
<strong>Input:</strong> l1 = [1,2,4], l2 = [1,3,4]
<strong>Output:</strong> [1,1,2,3,4,4]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> l1 = [], l2 = []
<strong>Output:</strong> []
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> l1 = [], l2 = [0]
<strong>Output:</strong> [0]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in both lists is in the range <code>[0, 50]</code>.</li>
	<li><code>-100 &lt;= Node.val &lt;= 100</code></li>
	<li>Both <code>l1</code> and <code>l2</code> are sorted in <strong>non-decreasing</strong> order.</li>
</ul>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Amazon - 61 (taggedByAdmin: true)
- Adobe - 13 (taggedByAdmin: false)
- Google - 10 (taggedByAdmin: false)
- Facebook - 7 (taggedByAdmin: false)
- Microsoft - 6 (taggedByAdmin: true)
- Capital One - 6 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: true)
- Bloomberg - 4 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- IBM - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: true)
- Yandex - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Intel - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Tencent - 6 (taggedByAdmin: false)
- Arista Networks - 5 (taggedByAdmin: false)
- Indeed - 4 (taggedByAdmin: false)
- Samsung - 2 (taggedByAdmin: false)
- Huawei - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach 1: Recursion

**Intuition**

We can recursively define the result of a `merge` operation on two lists as
the following (avoiding the corner case logic surrounding empty lists):

$$
\left\{
\begin{array}{ll}
      list1[0] + merge(list1[1:], list2) & list1[0] < list2[0] \\
      list2[0] + merge(list1, list2[1:]) & otherwise
\end{array}
\right.
$$

Namely, the smaller of the two lists' heads plus the result of a `merge` on
the rest of the elements.

**Algorithm**

We model the above recurrence directly, first accounting for edge cases.
Specifically, if either of `l1` or `l2` is initially `null`, there is no
merge to perform, so we simply return the non-`null` list. Otherwise, we
determine which of `l1` and `l2` has a smaller head, and recursively set the
`next` value for that head to the next merge result. Given that both lists
are `null`-terminated, the recursion will eventually terminate.

<iframe src="https://leetcode.com/playground/S3FpZm9s/shared" frameBorder="0" width="100%" height="378" name="S3FpZm9s"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n + m)$$

    Because each recursive call increments the pointer to `l1` or `l2` by one (approaching the dangling `null` at the end of each list), there will be exactly one call to `mergeTwoLists` per element in each list. Therefore, the time complexity is linear in the combined size of the lists.

* Space complexity : $$O(n + m)$$

    The first call to `mergeTwoLists` does not return until the ends of both `l1` and `l2` have been reached, so $$n + m$$ stack frames consume $$O(n + m)$$ space.

<br />

---

#### Approach 2: Iteration

**Intuition**

We can achieve the same idea via iteration by assuming that `l1` is entirely
less than `l2` and processing the elements one-by-one, inserting elements of
`l2` in the necessary places in `l1`.

**Algorithm**

First, we set up a false "`prehead`" node that allows us to easily return the
head of the merged list later. We also maintain a `prev` pointer, which
points to the current node for which we are considering adjusting its `next`
pointer. Then, we do the following until at least one of `l1` and `l2` points
to `null`: if the value at `l1` is less than or equal to the value at `l2`,
then we connect `l1` to the previous node and increment `l1`. Otherwise, we
do the same, but for `l2`. Then, regardless of which list we connected, we
increment `prev` to keep it one step behind one of our list heads.

After the loop terminates, at most one of `l1` and `l2` is non-`null`.
Therefore (because the input lists were in sorted order), if either list is
non-`null`, it contains only elements greater than all of the
previously-merged elements. This means that we can simply connect the
non-`null` list to the merged list and return it.

To see this in action on an example, check out the animation below:

!?!../Documents/21_Merge_Two_Sorted_Lists.json:1280,720!?!

<iframe src="https://leetcode.com/playground/dssukVFP/shared" frameBorder="0" width="100%" height="463" name="dssukVFP"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n + m)$$

    Because exactly one of `l1` and `l2` is incremented on each loop
    iteration, the `while` loop runs for a number of iterations equal to the
    sum of the lengths of the two lists. All other work is constant, so the
    overall complexity is linear.

* Space complexity : $$O(1)$$

    The iterative approach only allocates a few pointers, so it has a
    constant overall memory footprint.

## Accepted Submission (python3)
```python3
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        ph = ListNode()
        p = ph
        p1 = l1
        p2 = l2
        while p1 != None and p2 != None:
            if p1.val < p2.val:
                p.next = p1
                p1 = p1.next
            else:
                p.next = p2
                p2 = p2.next
            p = p.next
        if p1 == None:
            p.next = p2
        else:
            p.next = p1
        return ph.next
```

## Top Discussions
### Java, 1 ms, 4 lines codes, using recursion
- Author: yangliguang
- Creation Date: Tue May 10 2016 23:43:49 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 15:11:35 GMT+0800 (Singapore Standard Time)

<p>
    public ListNode mergeTwoLists(ListNode l1, ListNode l2){
    		if(l1 == null) return l2;
    		if(l2 == null) return l1;
    		if(l1.val < l2.val){
    			l1.next = mergeTwoLists(l1.next, l2);
    			return l1;
    		} else{
    			l2.next = mergeTwoLists(l1, l2.next);
    			return l2;
    		}
    }
</p>


### Python solutions (iteratively, recursively, iteratively in-place).
- Author: OldCodingFarmer
- Creation Date: Fri Aug 14 2015 04:11:27 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 09:21:06 GMT+0800 (Singapore Standard Time)

<p>
        
    
    # iteratively
    def mergeTwoLists1(self, l1, l2):
        dummy = cur = ListNode(0)
        while l1 and l2:
            if l1.val < l2.val:
                cur.next = l1
                l1 = l1.next
            else:
                cur.next = l2
                l2 = l2.next
            cur = cur.next
        cur.next = l1 or l2
        return dummy.next
        
    # recursively    
    def mergeTwoLists2(self, l1, l2):
        if not l1 or not l2:
            return l1 or l2
        if l1.val < l2.val:
            l1.next = self.mergeTwoLists(l1.next, l2)
            return l1
        else:
            l2.next = self.mergeTwoLists(l1, l2.next)
            return l2
            
    # in-place, iteratively        
    def mergeTwoLists(self, l1, l2):
        if None in (l1, l2):
            return l1 or l2
        dummy = cur = ListNode(0)
        dummy.next = l1
        while l1 and l2:
            if l1.val < l2.val:
                l1 = l1.next
            else:
                nxt = cur.next
                cur.next = l2
                tmp = l2.next
                l2.next = nxt
                l2 = tmp
            cur = cur.next
        cur.next = l1 or l2
        return dummy.next
</p>


### A recursive solution
- Author: GZShi
- Creation Date: Thu Jul 31 2014 10:19:16 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 15:14:30 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
            if(l1 == NULL) return l2;
            if(l2 == NULL) return l1;
            
            if(l1->val < l2->val) {
                l1->next = mergeTwoLists(l1->next, l2);
                return l1;
            } else {
                l2->next = mergeTwoLists(l2->next, l1);
                return l2;
            }
        }
    };


This solution is not a tail-recursive, the stack will overflow while the list is too long :)
http://en.wikipedia.org/wiki/Tail_call
</p>


