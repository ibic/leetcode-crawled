---
title: "Add Two Numbers"
weight: 2
#id: "add-two-numbers"
---
## Description
<div class="description">
<p>You are given two <b>non-empty</b> linked lists representing two non-negative integers. The digits are stored in <b>reverse order</b>, and each of their nodes contains a single digit. Add the two numbers and return the sum&nbsp;as a linked list.</p>

<p>You may assume the two numbers do not contain any leading zero, except the number 0 itself.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/10/02/addtwonumber1.jpg" style="width: 483px; height: 342px;" />
<pre>
<strong>Input:</strong> l1 = [2,4,3], l2 = [5,6,4]
<strong>Output:</strong> [7,0,8]
<strong>Explanation:</strong> 342 + 465 = 807.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> l1 = [0], l2 = [0]
<strong>Output:</strong> [0]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
<strong>Output:</strong> [8,9,9,9,0,0,0,1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in each linked list is in the range <code>[1, 100]</code>.</li>
	<li><code>0 &lt;= Node.val &lt;= 9</code></li>
	<li>It is guaranteed that the list represents a number that does not have leading zeros.</li>
</ul>

</div>

## Tags
- Linked List (linked-list)
- Math (math)

## Companies
- Amazon - 39 (taggedByAdmin: true)
- Bloomberg - 22 (taggedByAdmin: true)
- Apple - 20 (taggedByAdmin: false)
- Adobe - 19 (taggedByAdmin: true)
- Google - 14 (taggedByAdmin: false)
- Microsoft - 12 (taggedByAdmin: true)
- Facebook - 11 (taggedByAdmin: false)
- Uber - 8 (taggedByAdmin: false)
- Yahoo - 4 (taggedByAdmin: false)
- Docusign - 3 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- ServiceNow - 4 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Paypal - 3 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Capital One - 2 (taggedByAdmin: false)
- Qualcomm - 2 (taggedByAdmin: false)
- Zoho - 2 (taggedByAdmin: false)
- Huawei - 2 (taggedByAdmin: false)
- Cisco - 7 (taggedByAdmin: false)
- Yandex - 6 (taggedByAdmin: false)
- Grab - 4 (taggedByAdmin: false)
- Baidu - 3 (taggedByAdmin: false)
- Intel - 3 (taggedByAdmin: false)
- IBM - 3 (taggedByAdmin: false)
- Redfin - 3 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- Aetion - 2 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- Dell - 2 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
## Solution
---
#### Approach 1: Elementary Math

**Intuition**

Keep track of the carry using a variable and simulate digits-by-digits sum starting from the head of list, which contains the least-significant digit.

![Illustration of Adding two numbers](../Figures/2_add_two_numbers.svg){:width="539px"}
{:align="center"}

*Figure 1. Visualization of the addition of two numbers: $$342 + 465 = 807$$.  
Each node contains a single digit and the digits are stored in reverse order.*
{:align="center"}

**Algorithm**

Just like how you would sum two numbers on a piece of paper, we begin by summing the least-significant digits, which is the head of $$l1$$ and $$l2$$. Since each digit is in the range of $$0 \ldots 9$$, summing two digits may "overflow". For example $$5 + 7 = 12$$. In this case, we set the current digit to $$2$$ and bring over the $$carry = 1$$ to the next iteration. $$carry$$ must be either $$0$$ or $$1$$ because the largest possible sum of two digits (including the carry) is $$9 + 9 + 1 = 19$$.

The pseudocode is as following:

* Initialize current node to dummy head of the returning list.
* Initialize carry to $$0$$.
* Initialize $$p$$ and $$q$$ to head of $$l1$$ and $$l2$$ respectively.
* Loop through lists $$l1$$ and $$l2$$ until you reach both ends.
    * Set $$x$$ to node $$p$$'s value. If $$p$$ has reached the end of $$l1$$, set to $$0$$.
    * Set $$y$$ to node $$q$$'s value. If $$q$$ has reached the end of $$l2$$, set to $$0$$.
    * Set $$sum = x + y + carry$$.
    * Update $$carry = sum / 10$$.
    * Create a new node with the digit value of $$(sum \bmod 10)$$ and set it to current node's next, then advance current node to next.
    * Advance both $$p$$ and $$q$$.
* Check if $$carry = 1$$, if so append a new node with digit $$1$$ to the returning list.
* Return dummy head's next node.

Note that we use a dummy head to simplify the code. Without a dummy head, you would have to write extra conditional statements to initialize the head's value.

Take extra caution of the following cases:

| Test case | Explanation |
| ------------- | ---------------- |
| $$l1=[0,1]$$<br>$$l2=[0,1,2]$$ | When one list is longer than the other. |
| $$l1=[]$$<br>$$l2=[0,1]$$ | When one list is null, which means an empty list. |
| $$l1=[9,9]$$<br>$$l2=[1]$$ | The sum could have an extra carry of one at the end, which is easy to forget. |

<iframe src="https://leetcode.com/playground/5onAHA8v/shared" frameBorder="0" width="100%" height="378" name="5onAHA8v"></iframe>

**Complexity Analysis**

* Time complexity : $$O(\max(m, n))$$. Assume that $$m$$ and $$n$$ represents the length of $$l1$$ and $$l2$$ respectively, the algorithm above iterates at most $$\max(m, n)$$ times.

* Space complexity : $$O(\max(m, n))$$. The length of the new list is at most $$\max(m,n) + 1$$.

**Follow up**

What if the the digits in the linked list are stored in non-reversed order? For example:

$$
(3 \to 4 \to 2) + (4 \to 6 \to 5) = 8 \to 0 \to 7
$$

## Accepted Submission (java)
```java
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    private ListNode addCarry(ListNode l, int carry) {
        if (carry == 0) {
            return l;
        }

        ListNode p = l;
        while (p != null) {
            int val = p.val + carry;
            if (val >= 10) {
                p.val = val - 10;
                if (p.next == null) {
                    break;
                } else {
                    p = p.next;
                }
            } else {
                p.val = val;
                carry = 0;
                break;
            }
        }
        if (carry > 0) {
            if (p == null) {
                return new ListNode(carry);
            }
            p.next = new ListNode(carry);
        }
        return l;
    }
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int carry = 0;
        ListNode r = new ListNode(0);
        ListNode headp = r;
        while (true) {
            if (l1 == null) {
                r.next = addCarry(l2, carry);
                carry = 0;
                break;
            }
            if (l2 == null) {
                r.next = addCarry(l1, carry);
                carry = 0;
                break;
            }
            r.next = new ListNode(0);
            r = r.next;
            int val = l1.val + l2.val + carry;
            if (val >= 10) {
                r.val = val - 10;
                carry = 1;
            } else {
                r.val = val;
                carry = 0;
            }
            l1 = l1.next;
            l2 = l2.next;
        }
        if (carry > 0) {
            r.next = new ListNode(carry);
        }
        return headp.next;
    }
}

```

## Top Discussions
### Is this Algorithm optimal or what?
- Author: potpie
- Creation Date: Fri Jan 24 2014 08:31:17 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 20:38:54 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
            ListNode c1 = l1;
            ListNode c2 = l2;
            ListNode sentinel = new ListNode(0);
            ListNode d = sentinel;
            int sum = 0;
            while (c1 != null || c2 != null) {
                sum /= 10;
                if (c1 != null) {
                    sum += c1.val;
                    c1 = c1.next;
                }
                if (c2 != null) {
                    sum += c2.val;
                    c2 = c2.next;
                }
                d.next = new ListNode(sum % 10);
                d = d.next;
            }
            if (sum / 10 == 1)
                d.next = new ListNode(1);
            return sentinel.next;
        }
    }
</p>


### Clear python code, straight forward
- Author: tusizi
- Creation Date: Sun Feb 15 2015 20:27:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 12:17:14 GMT+0800 (Singapore Standard Time)

<p>
    class Solution:
    # @return a ListNode
    def addTwoNumbers(self, l1, l2):
        carry = 0
        root = n = ListNode(0)
        while l1 or l2 or carry:
            v1 = v2 = 0
            if l1:
                v1 = l1.val
                l1 = l1.next
            if l2:
                v2 = l2.val
                l2 = l2.next
            carry, val = divmod(v1+v2+carry, 10)
            n.next = ListNode(val)
            n = n.next
        return root.next
</p>


### [c++] Sharing my 11-line c++ solution, can someone make it even more concise?
- Author: ce2
- Creation Date: Thu Dec 11 2014 16:22:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 05:52:30 GMT+0800 (Singapore Standard Time)

<p>
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
        ListNode preHead(0), *p = &preHead;
        int extra = 0;
        while (l1 || l2 || extra) {
            int sum = (l1 ? l1->val : 0) + (l2 ? l2->val : 0) + extra;
            extra = sum / 10;
            p->next = new ListNode(sum % 10);
            p = p->next;
            l1 = l1 ? l1->next : l1;
            l2 = l2 ? l2->next : l2;
        }
        return preHead.next;
    }
</p>


