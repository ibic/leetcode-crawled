---
title: "Array Partition I"
weight: 526
#id: "array-partition-i"
---
## Description
<div class="description">
<p>
Given an array of <b>2n</b> integers, your task is to group these integers into <b>n</b> pairs of integer, say (a<sub>1</sub>, b<sub>1</sub>), (a<sub>2</sub>, b<sub>2</sub>), ..., (a<sub>n</sub>, b<sub>n</sub>) which makes sum of min(a<sub>i</sub>, b<sub>i</sub>) for all i from 1 to n as large as possible.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [1,4,3,2]

<b>Output:</b> 4
<b>Explanation:</b> n is 2, and the maximum sum of pairs is 4 = min(1, 2) + min(3, 4).
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li><b>n</b> is a positive integer, which is in the range of [1, 10000].</li>
<li>All the integers in the array will be in the range of [-10000, 10000].</li>
</ol>
</p>
</div>

## Tags
- Array (array)

## Companies
- Apple - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)

## Official Solution
[TOC]


## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

**Algorithm**

The simplest solution is to consider every possible set of pairings possible by using the elements of the $$nums$$ array. For generating all the possible pairings, we make use of a function `permute(nums, current_index)`. This function creates all the possible permutations of the elements of the given array.

To do so, `permute` takes the index of the current element $$current_index$$ as one of the arguments. Then, it swaps the current element with every other element in the array, lying towards its right, so as to generate a new ordering of the array elements. After the swapping has been done, it makes another call to  `permute` but this time with the index of the next element in the array. While returning back, we reverse the swapping done in the current function call.

Thus, when we reach the end of the array, a new ordering of the array's elements is generated. We consider the elements to be taken for the pairings such that the first element of every pair comes from the first half of the new array and the second element comes from the last half of the array. Thus, we sum up the minimum elements out of all these possible pairings and find out the maximum sum out of them.

The animation below depicts the ways the permutations are generated.

!?!../Documents/561_Array.json:1000,563!?!


<iframe src="https://leetcode.com/playground/yuWmjysn/shared" frameBorder="0" name="yuWmjysn" width="100%" height="513"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n!)$$. A total of $$n!$$ permutations are possible for $$n$$ elements in the array.
* Space complexity : $$O(1)$$. Constant extra space is used.

---
#### Approach #2 Using Sorting [Accepted]

**Algorithm**

In order to understand this approach, let us look at the problem from a different perspective. We need to form the pairings of the array's elements such that the overall sum of the minimum out of such pairings is maximum. Thus, we can look at the operation of choosing the minimum out of the pairing, say $$(a, b)$$ as incurring a loss of $$a - b$$(if $$a> b$$), in the maximum sum possible.

The total sum will now be maximum if the overall loss incurred from such pairings is minimized. This minimization of loss in every pairing is possible only if the numbers chosen for the pairings lie closer to each other than to the other elements of the array.

Taking this into consideration, we can sort the elements of the given array and form the pairings of the elements directly in the sorted order. This will lead to the pairings of elements with minimum difference between them leading to the maximization of the required sum.

<iframe src="https://leetcode.com/playground/d5X9zosv/shared" frameBorder="0" name="d5X9zosv" width="100%" height="224"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(nlog(n)\big)$$. Sorting takes $$O\big(nlog(n)\big)$$ time. We iterate over the array only once.

* Space complexity : $$O(1)$$. Constant extra space is used.

---
#### Approach #3 Using Extra Array [Accepted] 

**Algorithm**

This approach is somewhat related to the sorting approach. Since the range of elements in the given array is limited, we can make use of a hashmap $$arr$$, such that $$arr[i]$$ stores the frequency of occurence of $$(i-10000)^{th}$$ element. This subtraction is done so as to be able to map the numbers in the range $$-10000 &leq; i &leq; -1$$ onto the hashmap.

Thus, now instead of sorting the array's elements, we can directly traverse the hashmap in an ascending order. But, any element could also occur multiple times in the given array. We need to take this factor into account. 

For this, consider an example: `nums: [a, b, a, b, b, a]`. The sorted order of this array will be `nums_sorted: [a, a, a, b, b, b]`. (We aren't actually sorting the array in this approach, but the sorted array is taken just for demonstration). From the previous approach, we know that the required set of pairings is $$(a,a), (a,b), (b,b)$$. Now, we can see that while choosing the minimum elements, $$a$$ will be chosen twice and $$b$$ will be chosen once only. This happens because the number of $$a$$'s to be chosen has already been determined by the frequency of $$a$$, leaving the rest of the places to be filled by $$b$$. This is because, for the correct result we need to consider the elements in the ascending order. Thus, the lower number always gets priority to be added to the end result.

But, if the sorted elements take the form: `nums_sorted: [a, a, b, b, b, b]`, the correct pairing will be $$(a,a), (b,b), (b,b)$$. Again, in this case the number of $$a$$'s chosen is already predetermined, but since the number of $$a$$'s is odd, it doesn't impact the choice of $$b$$ in the final sum.

Thus, based on the above discussion, we traverse the hashmap $$arr$$. If the current element is occuring $$freq_i$$ number of times, and one of the elements is left to be paired with other elements in the right region(considering a virtual sorted array), we consider the current element $$\left \lceil\frac{freq_i}{2}\right \rceil$$ number of times and the next element occuring in the array $$\left \lfloor\frac{freq_j}{2}\right \rfloor$$ number of times for the final sum. To propagate the impact of this left over chosen number, we make use of a flag $$d$$. This flag is set to 1 if there is a leftover element from the current set which will be considered one more time. The same extra element already considered is taken into account while choosing an element from the next set.

While traversing the hashmap, we determine the correct number of times each element needs to be considered as discussed above. Note that the flag $$d$$ and the $$sum$$ remains unchanged if the current element of the hashmap doesn't exist in the array.

Below code is inspired by [@fallcreek](https://leetcode.com/fallcreek)

<iframe src="https://leetcode.com/playground/tzs4t7Hu/shared" frameBorder="0" name="tzs4t7Hu" width="100%" height="309"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. The whole hashmap $$arr$$ of size $$n$$ is traversed only once.

* Space complexity : $$O(n)$$. A hashmap $$arr$$ of size $$n$$ is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution, Sorting. And rough proof of algorithm.
- Author: shawngao
- Creation Date: Sun Apr 23 2017 11:06:44 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 08:17:08 GMT+0800 (Singapore Standard Time)

<p>
The algorithm is first sort the input array and then the sum of 1st, 3rd, 5th..., is the answer. 
```
public class Solution {
    public int arrayPairSum(int[] nums) {
        Arrays.sort(nums);
        int result = 0;
        for (int i = 0; i < nums.length; i += 2) {
            result += nums[i];
        }
        return result;
    }
}
```
Let me try to prove the algorithm...
1. Assume in each pair ```i```, ```bi >= ai```. 
2. Denote ```Sm = min(a1, b1) + min(a2, b2) + ... + min(an, bn)```. The biggest ```Sm``` is the answer of this problem. Given ```1```, ```Sm = a1 + a2 + ... + an```.
3. Denote ```Sa = a1 + b1 + a2 + b2 + ... + an + bn```. ```Sa``` is constant for a given input.
4. Denote ```di = |ai - bi|```. Given ```1```, ```di = bi - ai```. Denote ```Sd = d1 + d2 + ... + dn```.
5. So ```Sa = a1 + a1 + d1 + a2 + a2 + d2 + ... + an + an + dn = 2Sm + Sd``` => ```Sm = (Sa - Sd) / 2```. To get the max ```Sm```, given ```Sa``` is constant, we need to make ```Sd``` as small as possible.
6. So this problem becomes finding pairs in an array that makes sum of ```di``` (distance between ```ai``` and ```bi```) as small as possible. Apparently, sum of these distances of adjacent elements is the smallest. If that's not intuitive enough, see attached picture. Case 1 has the smallest ```Sd```.
![0_1492961937328_leetcode561.jpg](/uploads/files/1492961944408-leetcode561.jpg)
</p>


### Please explain: The question doesn't make sense.
- Author: djordan
- Creation Date: Fri Apr 28 2017 05:18:59 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 08:35:50 GMT+0800 (Singapore Standard Time)

<p>
So if we are given [1.2.3.4], is this a possibility?

(1,4) and (2,3)

In that case, won't the correct answer be 5. Our goal is to get the largest possible sum from the above 4 integers. So, how can 4 be the answer?

Thanks
</p>


### Java O(n) beats 100%
- Author: Wang-Zehao
- Creation Date: Wed May 03 2017 00:45:31 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 03:53:56 GMT+0800 (Singapore Standard Time)

<p>
inspired by [c++ code O(n),beats 100%](https://discuss.leetcode.com/topic/87483/c-code-o-n-beats-100)
```
public class Solution {

	public int arrayPairSum(int[] nums) {
		int[] exist = new int[20001];
		for (int i = 0; i < nums.length; i++) {
			exist[nums[i] + 10000]++;
		}
		int sum = 0;
		boolean odd = true;
		for (int i = 0; i < exist.length; i++) {
			while (exist[i] > 0) {
				if (odd) {
					sum += i - 10000;
				}
				odd = !odd;
				exist[i]--;
			}
		}
		return sum;
	}
	
}
```
</p>


