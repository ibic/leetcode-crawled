---
title: "Matchsticks to Square"
weight: 449
#id: "matchsticks-to-square"
---
## Description
<div class="description">
<p>Remember the story of Little Match Girl? By now, you know exactly what matchsticks the little match girl has, please find out a way you can make one square by using up all those matchsticks. You should not break any stick, but you can link them up, and each matchstick must be used <b>exactly</b> one time.</P>

<p> Your input will be several matchsticks the girl has, represented with their stick length. Your output will either be true or false, to represent whether you could make one square using all the matchsticks the little match girl has.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [1,1,2,2,2]
<b>Output:</b> true

<b>Explanation:</b> You can form a square with length 2, one side of the square came two sticks with length 1.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> [3,3,3,3,4]
<b>Output:</b> false

<b>Explanation:</b> You cannot find a way to form a square with all the matchsticks.
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The length sum of the given matchsticks is in the range of <code>0</code> to <code>10^9</code>.
<li>The length of the given matchstick array will not exceed <code>15</code>.</li>
</ol>
</p>
</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Rackspace - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
<br />
**Intuition**

Suppose we have `1,1,1,1,2,2,2,2,3,3,3,3` as our set of matchsticks. In this case a square of side $$6$$ can be formed and we have 4 matchsticks each of 1, 2 and 3 and so we can have each square side formed by `3 + 2 + 1 = 6`.

<center>
<img src="../Figures/473/473_Matchsticks-In-Square-Diag-1.png" height="400"></center>

We can clearly see in the diagram above that the 3 matchsticks of sizes `1`, `2` and `3` combine to give one side of our resulting square.

This problem boils down to splitting an array of integers into $$4$$ subsets where all of these subsets are:
* mutually exclusive i.e. no specific element of the array is shared by any two of these subsets, and
* have the same sum which is equal to the side of our square.

We know that we will have $$4$$ different subsets. The sum of elements of these subsets would be $$\frac{1}{4}\sum_{}^{} arr$$. If the sum if not divisible by $$4$$, that implies that $$4$$ subsets of equal value are not possible and we don't need to do any further processing on this.

The only question that remains now for us to solve is:
> what subset a particular element belongs to?

If we are able to figure that out, then there's nothing else left to do. But, since we can't say which of the $$4$$ subsets would contain a particular element, we try out all the options.
<br/>
<br/>

---

#### Approach 1: Depth First Search

It is possible that a matchstick ***can*** be a part of any of the 4 sides of the resulting square, but which one of these choices leads to an actual square is something we don't know.

This means that for every matchstick in our given array, we have $$4$$ different options each representing the side of the square or subset that this matchstick can be a part of.

We try out all of them and keep on doing this recursively until we exhaust all of the possibilities or until we find an arrangement of our matchsticks such that they form the square.

**Algorithm**

1. As discussed previously, we will follow a recursive, depth first approach to solve this problem. So, we have a function that takes the current matchstick index we are to process and also the number of sides of the square that are completely formed till now.

2. If all of the matchsticks have been used up and 4 sides have been completely formed, that implies our square is completely formed. This is the base case for the recursion.

3. For the current matchstick we have 4 different options. This matchstick at $$index$$ can be a part of any of the sides of the square. We try out the 4 options by recursing on them.
    - If any of these recursive calls returns $$True$$, then we return from there, else we return $$False$$

<iframe src="https://leetcode.com/playground/YYsyQn9m/shared" frameBorder="0" width="100%" height="500" name="YYsyQn9m"></iframe>

**Implementation Details**

This solution is very slow as is. However, we can speed it up considerably by a small trick and that is to `sort our matchsticks sizes in reverse order before processing them recursively`.

The reason for this is that if there is no solution, trying a longer matchstick first will get to negative conclusion earlier.

e.g. $$[8,4,4,4]$$. In this case we can have a square of size 5 but the largest side 8 doesn't fit in anywhere i.e. cannot be a part of any of the sides (because we can't break matchsticks according to the question) and hence we can simply return $$False$$ without even considering the remaining matchsticks.

**Complexity Analysis**

* Time Complexity : $$O(4^N)$$ because we have a total of $$N$$ sticks and for each one of those matchsticks, we have $$4$$ different possibilities for the subsets they might belong to or the side of the square they might be a part of.

* Space Complexity : $$O(N)$$. For recursive solutions, the space complexity is the stack space occupied by all the recursive calls. The deepest recursive call here would be of size $$N$$ and hence the space complexity is $$O(N)$$. There is no additional space other than the recursion stack in this solution.
<br/>
<br/>
---

#### Approach 2: Dynamic Programming

In any dynamic programming problem, what's important is that our problem must be breakable into smaller subproblems and also, these subproblems show some sort of overlap which we can save upon by caching or memoization.

Suppose we have `3,3,4,4,5,5` as our matchsticks that have been used already to construct some of the sides of our square (**Note:** not all the sides may be completely constructed at all times.)

If the square side is $$8$$, then there are many possibilities for how the sides can be constructed using the matchsticks above. We can have

<pre>
  (4, 4), (3, 5), (3, 5) -----------> 3 sides fully constructed.
  (3, 4), (3, 5), (4), (5) ---------> 0 sides completely constructed.
  (3, 3), (4, 4), (5), (5) ---------> 1 side completely constructed.
</pre>

As we can see above, there are multiple ways to use the same set of matchsticks and land up in completely different recursion states.

This means that if we just keep track of what all matchsticks have been used and which all are remaining, it won't properly define the state of recursion we are in or what subproblem we are solving.

A single set of used matchsticks can represent multiple different unrelated subproblems and that is just not right.

We also need to keep track of number of sides of the square that have been **completely** formed till now.

Also, an important thing to note in the example we just considered was that if the matchsticks being used are $$[3,3,4,4,5,5]$$ and the side of the square is `8`, then we will always consider that arrangement that forms the most number of complete sides over that arrangement that leads to incomplete sides. Hence, the optimal arrangement here is $$(4, 4), (3, 5), (3, 5)$$ with 3 complete sides of the square.

Let us take a look at the following recursion tree to see if in-fact we can get overlapping subproblems.

<center>
<img src="../Figures/473/473_Matchsticks-In-Square-Diag-2.png" width="500"></center>

**Note:** Not all subproblems have been shown in this figure. The thing we wanted to point out was overlapping subproblems.

We know that the overall sum of these matchsticks can be split equally into 4 halves. The only thing we don't know is if 4 **equal** halves can be carved out of the given set of matchsticks. For that also we need to keep track of the number of sides completely formed at any point in time. ***If we end up forming 4 equal sides successfully then naturally we would have used up all of the matchsticks each being used exactly once and we would have formed a square***.

Let us first look at the pseudo-code for this problem before looking at the exact implementation details for the same.

<pre>
let square_side = sum(matchsticks) / 4
func recurse(matchsticks_used, sides_formed) {
    if sides_formed == 4, then {
        Square Formed!!
    }
    for match in matchsticks available, do {
          add match to matchsticks_used
          let result = recurse(matchsticks_used, sides_formed)
          if result == True, then {
              return True
          }
          remove match from matchsticks_used
    }
    return False
}
</pre>

This is the overall structure of our dynamic programming solution. Of-course, a lot of implementation details are missing here that we will address now.

<br />

**Implementation Details**

It is very clear from the pseudo-code above that the state of a recursion is defined by two variables `matchsticks_used` and `sides_formed`. Hence, these are the two variables that will be used to **memoize** or cache the results for that specific subproblem.

The question however is how do we actually store all the matchsticks that have been used? We want a memory efficient solution for this.

If we look at the question's constraints, we find that the max number of matchsticks we can have are $$15$$. That's a pretty small number and we can make use of this constraint.

All we need to store is which of the matchsticks from the original list have been used. `We can use a Bit-Map for this`

We will use $$N$$ number of bits, one for each of the matchsticks ($$N$$ is at max 15 according to the question's constraints). Initially we will start with a bit mask of `all 1s` and then as we keep on using the matchsticks, we will keep on setting their corresponding bits to `0`.

This way, we just have to hash an integer value which represents our bit-map and the max value for this mask would be $$2^{15}$$.

<br />

**Do we really need to see if all 4 sides have been completely formed ?**

Another implementation trick that helps optimize this solution is that we don't really need to see if 4 sides have been completely formed.

This is because, we already know that the sum of all the matchsticks is divisible by 4. So, *if 3 equal sides have been formed by using some of the matchsticks, then the remaining matchsticks would definitely form the remaining side of our square.*

Hence, we only need to check if 3 sides of our square can be formed or not.


<iframe src="https://leetcode.com/playground/DGgcAAdf/shared" frameBorder="0" width="100%" height="500" name="DGgcAAdf"></iframe>

**Complexity Analysis**

* Time Complexity : $$O(N \times 2^N)$$. At max $$2^N$$ unique bit masks are possible and during every recursive call, we iterate our original matchsticks array to sum up the values of matchsticks used to update the `sides_formed` variable.

* Space Complexity : $$O(N + 2^N)$$ because $$N$$ is the stack space taken up by recursion and $$4 \times 2^N$$ = $$O(2^N)$$ is the max possible size of our cache for memoization.
    - The size of the cache is defined by the two variables `sides_formed` and `mask`. The number of different values that `sides_formed` can take = 4 and number of unique values of `mask` = $$2^N$$.

  <br />
  <br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java DFS Solution with Explanation
- Author: shawngao
- Creation Date: Sun Dec 18 2016 13:21:14 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 05:53:29 GMT+0800 (Singapore Standard Time)

<p>
According to https://en.wikipedia.org/wiki/Partition_problem, the partition problem (or number partitioning) is the task of deciding whether a given multiset ```S``` of positive integers can be partitioned into two subsets ```S1``` and ```S2``` such that the sum of the numbers in ```S1``` equals the sum of the numbers in ```S2```. The partition problem is ```NP-complete```.

When I trying to think how to apply dynamic programming solution of above problem to this one (difference is divid ```S``` into 4 subsets), I took another look at the constraints of the problem:
The length sum of the given matchsticks is in the range of ```0``` to ```10^9```.
The length of the given matchstick array will not exceed ```15```.

Sounds like the input will not be very large... Then why not just do DFS? In fact, DFS solution passed judges.

Anyone solved this problem by using DP? Please let me know :)

```
public class Solution {
    public boolean makesquare(int[] nums) {
    	if (nums == null || nums.length < 4) return false;
        int sum = 0;
        for (int num : nums) sum += num;
        if (sum % 4 != 0) return false;
        
    	return dfs(nums, new int[4], 0, sum / 4);
    }
    
    private boolean dfs(int[] nums, int[] sums, int index, int target) {
    	if (index == nums.length) {
    	    if (sums[0] == target && sums[1] == target && sums[2] == target) {
    		return true;
    	    }
    	    return false;
    	}
    	
    	for (int i = 0; i < 4; i++) {
    	    if (sums[i] + nums[index] > target) continue;
    	    sums[i] += nums[index];
            if (dfs(nums, sums, index + 1, target)) return true;
    	    sums[i] -= nums[index];
    	}
    	
    	return false;
    }
}
```
```Updates on 12/19/2016``` Thanks @benjamin19890721 for pointing out a very good optimization: Sorting the input array ```DESC``` will make the DFS process run much faster. Reason behind this is we always try to put the next matchstick in the first subset. If there is no solution, trying a longer matchstick first will get to negative conclusion earlier. Following is the updated code. Runtime is improved from more than 1000ms to around 40ms. A big improvement.
```
public class Solution {
    public boolean makesquare(int[] nums) {
    	if (nums == null || nums.length < 4) return false;
        int sum = 0;
        for (int num : nums) sum += num;
        if (sum % 4 != 0) return false;
        
        Arrays.sort(nums);
        reverse(nums);
        
    	return dfs(nums, new int[4], 0, sum / 4);
    }
    
    private boolean dfs(int[] nums, int[] sums, int index, int target) {
    	if (index == nums.length) {
    	    if (sums[0] == target && sums[1] == target && sums[2] == target) {
    		return true;
    	    }
    	    return false;
    	}
    	
    	for (int i = 0; i < 4; i++) {
    	    if (sums[i] + nums[index] > target) continue;
    	    sums[i] += nums[index];
            if (dfs(nums, sums, index + 1, target)) return true;
    	    sums[i] -= nums[index];
    	}
    	
    	return false;
    }
    
    private void reverse(int[] nums) {
        int i = 0, j = nums.length - 1;
        while (i < j) {
            int temp = nums[i];
            nums[i] = nums[j];
            nums[j] = temp;
            i++; j--;
        }
    }
}
```
</p>


### cpp 6ms solution with DFS
- Author: withacup
- Creation Date: Wed Jan 04 2017 04:23:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 17:27:39 GMT+0800 (Singapore Standard Time)

<p>
This is a NP problem. Time complexity should be O(4 ^ n), n is the length of array.
#### Trivial Solution:
```cpp
class Solution {
    bool dfs(vector<int> &sidesLength,const vector<int> &matches, int index) {
        if (index == matches.size())
            return sidesLength[0] == sidesLength[1] && sidesLength[1] == sidesLength[2] && sidesLength[2] == sidesLength[3];
        for (int i = 0; i < 4; ++i) {
            sidesLength[i] += matches[index];
            if (dfs(sidesLength, matches, index + 1))
                return true;
            sidesLength[i] -= matches[index];
        }
        return false;
    }
public:
    bool makesquare(vector<int>& nums) {
        if (nums.empty()) return false;
        vector<int> sidesLength(4, 0);
        return dfs(sidesLength, nums, 0);
    }
};
```
Without pruning, this solution TLEs at 11th test case.
#### First Optimization:
>each matchstick must be used exactly one time.

The description says we need to use every single match exactly **once**, so we can get the **length of each side of the square** if there is one.
if the current length is larger than target length, we don't need to go any further.
```if (sidesLength[i] + matches[index] > target) continue;``` by adding this line of code into dfs function, solution get TLE at 147th test case.
#### Second Optimization:
After reading the description again, I realize that the length of a single match can be very long. If they put long ones after short ones, it will take a long time before my algorithm return false.
So I sort all the matches to avoid the worst case:```sort(nums.begin(), nums.end(), [](const int &l, const int &r){return l > r;});```. After putting this line before my algorithm, solution passed all cases and beats 50% solutions. I saw someone's algorithm is amazingly fast, they even make me reconsider wether the problem is NP or not.
#### Third Optimization:
Because their solutions is so fast, I need think about if I can use DP to solve the problem. It turns out that it's still a NP problem, which makes happy again. But I can actually use the concept of DP in optimization: **if I have checked the same length before, why do I need to bother checking again?**
Although we only have 4 sides in a square, we can still check if we have encountered the same length with the current match. After I add this to my code:
```cpp
int j = i;
while (--j >= 0)
    if (sidesLength[i] == sidesLength[j]) 
        break;
if (j != -1) continue;
```

It passed all test case in 6ms.
```cpp
class Solution {
    bool dfs(vector<int> &sidesLength,const vector<int> &matches, int index, const int target) {
        if (index == matches.size())
            return sidesLength[0] == sidesLength[1] && sidesLength[1] == sidesLength[2] && sidesLength[2] == sidesLength[3];
        for (int i = 0; i < 4; ++i) {
            if (sidesLength[i] + matches[index] > target) // first
                continue;
            int j = i;
            while (--j >= 0) // third
                if (sidesLength[i] == sidesLength[j]) 
                    break;
            if (j != -1) continue;
            sidesLength[i] += matches[index];
            if (dfs(sidesLength, matches, index + 1, target))
                return true;
            sidesLength[i] -= matches[index];
        }
        return false;
    }
public:
    bool makesquare(vector<int>& nums) {
        if (nums.size() < 4) return false;
        int sum = 0;
        for (const int val: nums) {
            sum += val;
        }
        if (sum % 4 != 0) return false;
        sort(nums.begin(), nums.end(), [](const int &l, const int &r){return l > r;}); // second
        vector<int> sidesLength(4, 0);
        return dfs(sidesLength, nums, 0, sum / 4);
    }
};
```
</p>


### Java DFS solution with various optimizations (sorting, sequential-partition, DP)
- Author: fun4LeetCode
- Creation Date: Thu Dec 22 2016 05:48:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 16 2018 04:08:47 GMT+0800 (Singapore Standard Time)

<p>
As others pointed out, after proper preprocessing, this problem boiled down to a number partitioning problem which is NP-hard. Nevertheless, if the total number of elements is small, a naive DFS solution is possible.

---
**`I -- Naive DFS`**

For better description of the problem, let's reformulate it in the following symbolic way: 

**Given an array `nums` with `n` elements, let `T(i, s1, s2, s3, s4)` denote whether we can partition the subarray `nums[0, i]`(both inclusive) into four disjoint groups such that the sum of elements in the `j-th`  group is `sj`, with `j = 1, 2, 3, 4`.**

With this definition, our original problem will be `T(n - 1, side, side, side, side)` where `side` is the side length of the square.

To solve for `T(i, s1, s2, s3, s4)`, note that the last element of the subarray `nums[0, i]` (which is `nums[i]`) must belong to one of the disjoint groups, therefore we have the following recurrence relation:
```
T(i, s1, s2, s3, s4) = T(i - 1, s1 - nums[i], s2, s3, s4) ||
                       T(i - 1, s1, s2 - nums[i], s3, s4) ||
                       T(i - 1, s1, s2, s3 - nums[i], s4) ||
                       T(i - 1, s1, s2, s3, s4 - nums[i])
```
The interpretation is as follows: if `nums[i]` belongs to the `j-th` group, we subtract it from `sj`, then recursively solve for the subproblem with reduced array size and modified group sum. However, we do not know which group it belongs to beforehand, therefore each of the groups will be examined until we either hit the right group or determine that no partition is possible. Also note that if all elements in the input array are positive, an element cannot fall into a group with a group sum smaller than the element itself, i.e., `nums[i]` cannot belong to the `j-th` group if `nums[i] > sj`.

The termination condition for the recursion is when the subarray is empty, i.e., `i = -1`, at which time we will check whether the sum of each group is zero. If so, a partition is found and return true; otherwise no partition is possible and return false.

Here is the java program based on the above ideas:

```
public boolean makesquare(int[] nums) {
    if (nums.length < 4) return false;
        
    int perimeter = 0;
    for (int ele : nums) perimeter += ele;
    if (perimeter % 4 != 0) return false;
    
    int side = perimeter / 4;

    return makesquareSub(nums, nums.length - 1, new int[] {side, side, side, side});
}

private boolean makesquareSub(int[] nums, int i, int[] s) {
    if (i < 0) return s[0] == 0 && s[1] == 0 && s[2] == 0 && s[3] == 0;
        
    for (int j = 0; j < s.length; j++) {
        if (nums[i] > s[j]) continue;
        s[j] -= nums[i];
        if (makesquareSub(nums, i - 1, s)) return true;
        s[j] += nums[i];
    }
        
    return false;
}
```
While accepted, this solution runs rather slowly (~`450ms`). So let's see what optimizations can be done to enhance the time complexity.

---
**`II -- DFS with sorting`**

In the recurrence relation above, we concluded that if `nums[i] > sj`, it cannot belong to the `j-th` group, which implies we needn't even bother to try that case. This condition is most likely to be met if we always choose the maximum element that is currently available in the subarray. Also note that the order of elements does not matter in the partition of the array. Therefore we can sort the input array in ascending order before rushing into DFS. This reduced the running time sharply to ~`40ms`.
```
public boolean makesquare(int[] nums) {
    if (nums.length < 4) return false;
        
    int perimeter = 0;
    for (int ele : nums) perimeter += ele;
    if (perimeter % 4 != 0) return false;
        
    Arrays.sort(nums);
    int side = perimeter / 4;

    return makesquareSub(nums, nums.length - 1, new int[] {side, side, side, side});
}
    
private boolean makesquareSub(int[] nums, int i, int[] s) {
    if (i < 0) return s[0] == 0 && s[1] == 0 && s[2] == 0 && s[3] == 0;
        
    for (int j = 0; j < s.length; j++) {
        if (nums[i] > s[j]) continue;
        s[j] -= nums[i];
        if (makesquareSub(nums, i - 1, s)) return true;
        s[j] += nums[i];
    }
        
    return false;
}
```
---
***Note: It looks like the following solutions based on sequential ideas are not correct. See [oliver_feng's](https://discuss.leetcode.com/topic/72569/java-dfs-solution-with-various-optimizations-sorting-sequential-partition-dp/3) comment below.***

~~**`III -- DFS with sequential-partition`**~~

So far, the partitioning of the array is done simultaneously for the four disjoint groups. With the array sorted, it is also possible to break the partitioning process into four sequential parts such that each part will find each of the disjoint groups. (Note: sorting in ascending order is **necessary** now. We have to choose greedily (maximum element available) for each group,  otherwise the total number of valid groups may decrease. Check out the case `[3,3,3,3,1,1,1,1]`.)

To this end, let's define `T(i, sum)` which denotes whether `sum` can be written as the summation of some elements in the subarray `nums[0, i]`, with each element used at most once. If so, a group is found such that the sum of its elements will be `sum`. Without much efforts, we can obtain the following recurrence relation:
```
T(i, sum) = T(i - 1, sum - nums[i]) || T(i - 1, sum) 
```
The two cases on the right hand side correspond to whether we choose the last element `nums[i]` to form the sum or not. And again, it cannot be chosen if `nums[i] > sum`.

The termination condition is either `sum = 0` in which case a group is found or `i = -1` in which case no such group exists.

So far everything looks pretty similar to what we have discussed in part `I`. However, a key difference here is now we need to explicitly mark the elements that are chosen to form previous groups since they are no longer available. Some straightforward ways would be using a `boolean array` or an `integer`(and do bit manipulations). Since the elements in the input array are initially positive, it is also possible to mark visited elements by negating their values. 

Here is the java program for sequential partition, with running time further reduced to ~`15ms`.

```
public boolean makesquare(int[] nums) {
    if (nums.length < 4) return false;
        
    int perimeter = 0;
    for (int ele : nums) perimeter += ele;
    if (perimeter % 4 != 0) return false;
        
    Arrays.sort(nums);
    int side = perimeter / 4;

    for (int i = 0; i < 3; i++) {
        if (!makesquareSub(nums, nums.length - 1, side)) return false;
    }
        
    return true;
}
    
private boolean makesquareSub(int[] nums, int i, int sum) {
    if (sum == 0) return true;
    if (i < 0) return false;
        
    if (nums[i] > 0 && nums[i] <= sum) {
        nums[i] = -nums[i];
        if (makesquareSub(nums, i - 1, sum + nums[i])) return true;
        nums[i] = -nums[i];
    }
        
    return makesquareSub(nums, i - 1, sum);
}
```

---
~~**`IV -- DFS with DP`**~~

For all solutions above (`T(i, s1, s2, s3, s4)` or `T(i, sum)`), we did not take into account the possibility that there is overlapping among the subproblems.

For `T(i, s1, s2, s3, s4)`, each subproblem is characterized by **five** integers while for `T(i, sum)`, it is **two**. I would say the probability of overlapping subproblems for the former is relatively lower than the latter. So I only implemented DP for the sequential-partition case. However the running time (~`25ms`) did not get further improved, possibly due to the fact that the probability for overlapping subproblems is already rather low (if any) for this case with small number of elements. Not sure if DP will prevail for larger input size though.

Last word about the following DP solution: naively we would use an array of HashMap (let's denote it as `map`) to memorize intermediate results, with `i` indexed into the array and `sum` as the key of the corresponding HashMap. However, due to our greedy strategy for finding each group, if two subproblems `T1(i1, sum1)` and `T2(i2, sum2)` have the same sum, i.e., `sum1 = sum2`, then the one with larger index `i` will always be solved before the other. Since `T(i, sum) = true` implies `T(j, sum) = true` with `j >= i`, and `T(i, sum) = false` implies `T(j, sum) = false` with `j <= i`, it is sufficient to use only the `sum` factor to characterize subproblems that yield `false` solution (if any of the subproblem yields true, the recursion will rewind and eventually terminate). Therefore you will see only a HashSet is used in the following implementation.

```
public boolean makesquare(int[] nums) {
    if (nums.length < 4) return false;
        
    int perimeter = 0;
    for (int ele : nums) perimeter += ele;
    if (perimeter % 4 != 0) return false;
        
    Arrays.sort(nums);
    int side = perimeter / 4;
        
    for (int i = 0; i < 3; i++) {
        if (!makesquareSub(nums, nums.length - 1, side, new HashSet<>())) return false;
    }
    
    return true;
}
    
private boolean makesquareSub(int[] nums, int i, int sum, Set<Integer> set) {
    if (sum == 0) return true;
    if (set.contains(sum) || i < 0) return false;
    
    if (nums[i] > 0 && nums[i] <= sum) {
        nums[i] = -nums[i];
        if (makesquareSub(nums, i - 1, sum + nums[i], set)) return true;
        nums[i] = -nums[i];
    }
        
    if (makesquareSub(nums, i - 1, sum, set)) return true;
        
    set.add(sum);
    return false;
}
```
</p>


