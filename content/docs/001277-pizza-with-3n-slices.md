---
title: "Pizza With 3n Slices"
weight: 1277
#id: "pizza-with-3n-slices"
---
## Description
<div class="description">
<p>There is a pizza with 3n slices of varying size, you and your friends will take slices of pizza as follows:</p>

<ul>
	<li>You will pick <strong>any</strong> pizza slice.</li>
	<li>Your friend Alice&nbsp;will pick&nbsp;next slice in anti clockwise direction of your pick.&nbsp;</li>
	<li>Your friend Bob&nbsp;will&nbsp;pick&nbsp;next slice in clockwise direction of your pick.</li>
	<li>Repeat&nbsp;until&nbsp;there are no more slices of pizzas.</li>
</ul>

<p>Sizes of Pizza slices is represented by circular array <code>slices</code> in clockwise direction.</p>

<p>Return the maximum possible sum of slice sizes which you can have.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/02/18/sample_3_1723.png" style="width: 475px; height: 240px;" /></p>

<pre>
<strong>Input:</strong> slices = [1,2,3,4,5,6]
<strong>Output:</strong> 10
<strong>Explanation:</strong> Pick pizza slice of size 4, Alice and Bob will pick slices with size 3 and 5 respectively. Then Pick slices with size 6, finally Alice and Bob will pick slice of size 2 and 1 respectively. Total = 4 + 6.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/02/18/sample_4_1723.png" style="width: 475px; height: 250px;" /></strong></p>

<pre>
<strong>Input:</strong> slices = [8,9,8,6,1,1]
<strong>Output:</strong> 16
<strong>Output:</strong> Pick pizza slice of size 8 in each turn. If you pick slice with size 9 your partners will pick slices of size 8.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> slices = [4,1,2,5,8,3,1,9,7]
<strong>Output:</strong> 21
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> slices = [3,1,2]
<strong>Output:</strong> 3
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= slices.length &lt;= 500</code></li>
	<li><code>slices.length % 3 == 0</code></li>
	<li><code>1 &lt;= slices[i] &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] DP Solution - Clear Explanation - Clean code
- Author: hiepit
- Creation Date: Sun Mar 29 2020 00:06:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 29 2020 01:07:23 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
- We can understand the problem: pick `n` **non-adjacent** elements from **circular array** `m=3n` elements so that the sum of the elements is maximum.
- We can\'t pick `0th` and `(m-1)th` elements at the same time, since it\'s adjacent in circular array.
- So there are total 2 cases, then we can return the case with maximum sum:
	- Case 1: Don\'t pick `(m-1)th` element 
=> Solve problem: pick `n` **non-adjacent** elements from **linear array** elements in ranges `[0..m-2]` so that the sum of the elements is maximum.
	- Case 2: Don\'t pick `0th` element 
=> Solve problem: pick `n` **non-adjacent** elements from **linear array** elements in ranges `[1..m-1]` so that the sum of the elements is maximum.
```
public int maxSizeSlices(int[] slices) {
	int m = slices.length, n = m / 3;
	int[] slices1 = Arrays.copyOfRange(slices, 0, m-1);
	int[] slices2 = Arrays.copyOfRange(slices, 1, m);
	return Math.max(maxSum(slices1, n), maxSum(slices2, n));
}

int maxSum(int[] arr, int n) { // max sum when pick `n` non-adjacent elements from `arr`
	int m = arr.length;
	int[][] dp = new int[m+1][n+1]; // dp[i][j] is maximum sum which we pick `j` elements from linear array `i` elements
	// Case j = 0 (pick 0 elements): dp[i][0] = 0
	// Case i = 0 (array is empty): dp[0][j] = 0
	for (int i = 1; i <= m; ++i) {
		for (int j = 1; j <= n; ++j) {
			if (i == 1) { // array has only 1 element
				dp[i][j] = arr[0]; // pick that element
			} else {
				dp[i][j] = Math.max(
					dp[i-1][j],             // don\'t pick element `ith`
					dp[i-2][j-1] + arr[i-1] // pick element `ith` -> dp[i-2][j-1] means choose `j-1` elements from array `i-2` elements
											//   because we exclude adjacent element `(i-1)th`
				);
			}
		}
	}
	return dp[m][n];
}
```
Complexity
- Time & Space: `O(m*n) ~ O(3n*n) ~ O(n^2)`, n is the number of slices in pizza need to pick

If it helps you easy to understand, please help to **UPVOTE**.
If you have any questions, feel free to comment below.
Happy coding!
</p>


### [Python] Robber n/3 Houses in Cycle
- Author: lee215
- Creation Date: Sun Mar 22 2020 00:04:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 23 2020 22:23:11 GMT+0800 (Singapore Standard Time)

<p>
# Preword
Someone happened to ask me this problem ealier.
I told him to do this [213. House Robber II](https://leetcode.com/problems/house-robber-ii/description/)
Almost the same.
<br>

# Intuition
The interruption of this question is:
get n elements from an an array of sizes `3*n`.
None of the selected n elements could be neighbor to each others.
And because its in cycle,
`A[0]` and `A[n-1]` cannot be chosen together.
After understanding this, the question become much easier.
<br>

# Explanation
Each round, we will decide if we take the last slice in `A[i]~A[j]`.
If we take it, we will take the rest `k-1` slices in `A[i]~A[j-2]`
If we don\'t take it, we will take the rest `k` slices in `A[i]~A[j-1]`

Why do we check `j - i + 1 < k * 2 - 1`,
instead of something like `(k-1) < j - i +1 <= 3 * k`

@caohuicn help explain this part:
Because j - i + 1 is not the number of all the slices that are left.
You might have chosen not to take a slice in a previous step,
then there would be leftovers outside of [i, j].
Now if you take i or j (the start or end slice),
one of the slices your friends take will be outside of [i, j],
so the length of [i, j] is reduced by 2, not 3.
Therefore the minimum number is 2 * k - 1
(the last step only requires one slice).

Note that `dfs` can be compressed to one line but too long.
<br>

**Java**
```java
    public final int maxSizeSlices(final int[] slices) {
        final Map<String, Integer> dp = new HashMap<>();
        return maxSizeSlices(slices, 0, slices.length - 1, slices.length / 3, 1, dp);
    }

    private final int maxSizeSlices(final int[] slices, final int start, final int end,
                                    final int n, final int cycle, final Map<String, Integer> dp) {
        final String serialized = start + "$" + end + "$" + n + "$" + cycle;
        if (dp.containsKey(serialized)) {
            return dp.get(serialized);
        }
        if (n == 1) {
            int max = Integer.MIN_VALUE;
            for (int i = start; i <= end; i++) {
                max = Math.max(max, slices[i]);
            }
            dp.put(serialized, max);
            return dp.get(serialized);
        }
        if (end - start + 1 < n * 2 - 1) {
            dp.put(serialized, Integer.MIN_VALUE);
            return dp.get(serialized);
        }
        final int res = Math.max(maxSizeSlices(slices, start + cycle, end - 2, n - 1, 0, dp) + slices[end],
                                maxSizeSlices(slices, start, end - 1, n, 0, dp));
        dp.put(serialized, res);
        return dp.get(serialized);
    }
```
**Python:**
```py
    def maxSizeSlices(self, A):
        @functools.lru_cache(None)
        def dp(i, j, k, cycle=0):
            if k == 1: return max(A[i:j + 1])
            if j - i + 1 < k * 2 - 1: return -float(\'inf\')
            return max(dp(i + cycle, j - 2, k - 1) + A[j], dp(i, j - 1, k))
        return dp(0, len(A) - 1, len(A) // 3, 1)
```

</p>


### [Python/C++] O(n) space, Easy DP with explanation
- Author: yanrucheng
- Creation Date: Sun Mar 22 2020 00:01:00 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 21 2020 11:29:06 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
We first divide the problem into 2 sub-problem. (since we cannot have slice 0 and slice n at the same time)
1. we choose from pizza [0, ..., n-1] as a linear pizza instead of a circular pizza
2. we choose from pizza [1, ..., n] as a linear pizza instead of a circular pizza

We then track the maximum amount we ate after eating the ith pizza we have in `eat`. There are only 2 possibilities as follows.
1. largest value when encountering the previous slice
2. current slice + largest value when encountering last second slice

**Similar Questoins**
- [188. Best Time to Buy and Sell Stock IV](http://https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iv/)
- [213. House Robber II](http://https://leetcode.com/problems/house-robber-ii/)

**Complexity**
Time: `O(n^2)`
Space: `O(n)`

**Python 3, O(n^2) space and time**
```
class Solution:
    def maxSizeSlices(self, slices: List[int]) -> int:
        n = len(slices) // 3
        def linear(arr):
            eat = [[0] + [-math.inf]*n] * 2
            for x in arr:
                eat.append([i and max(eat[-1][i], eat[-2][i-1]+x) for i in range(n+1)])
            return max(l[n] for l in eat)
        return max(linear(slices[1:]), linear(slices[:-1]))
```

**Python 3, O(n) space**
```
class Solution:
    def maxSizeSlices(self, slices: List[int]) -> int:
        n = len(slices) // 3
        def linear(arr):
            eat = collections.deque([[0] + [-math.inf]*n] * 2)
            res = 0
            for x in arr:
                eat += [i and max(eat[-1][i], eat[-2][i-1]+x) for i in range(n+1)],
                eat.popleft()
                res = max(res, eat[-1][n])
            return res
        return max(linear(slices[1:]), linear(slices[:-1]))
```

**C++**
```
class Solution {
public:
    int maxSizeSlices(vector<int>& slices) {
        int n = (int)slices.size() / 3;
        auto l1 = vector<int>(slices.begin(), slices.end()-1);
        auto l2 = vector<int>(slices.begin()+1, slices.end());
        return max(linear(l1, n), linear(l2, n));
    }
    
private:
    int linear(vector<int>& slices, int n) {
        vector<vector<int>> eat((int)slices.size()+2, vector<int>(n+1, INT_MIN));
        int res = INT_MIN;
        for (int i=0; i<eat.size(); ++i) eat[i][0] = 0;
        for (int i=2; i<eat.size(); ++i) {
            for (int j=1; j<n+1; ++j)
                eat[i][j] = max(eat[i-1][j], eat[i-2][j-1] + slices[i-2]);
            res = max(eat[i][n], res);
        }
        return res;
    }
};
```

</p>


