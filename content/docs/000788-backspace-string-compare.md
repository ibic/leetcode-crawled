---
title: "Backspace String Compare"
weight: 788
#id: "backspace-string-compare"
---
## Description
<div class="description">
<p>Given two&nbsp;strings&nbsp;<code>S</code>&nbsp;and <code>T</code>,&nbsp;return if they are equal when both are typed into empty text editors. <code>#</code> means a backspace character.</p>

<p>Note that after&nbsp;backspacing an empty text, the text will continue empty.</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-1-1">&quot;ab#c&quot;</span>, T = <span id="example-input-1-2">&quot;ad#c&quot;</span>
<strong>Output: </strong><span id="example-output-1">true
</span><span><strong>Explanation</strong>: Both S and T become &quot;ac&quot;.</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-2-1">&quot;ab##&quot;</span>, T = <span id="example-input-2-2">&quot;c#d#&quot;</span>
<strong>Output: </strong><span id="example-output-2">true
</span><span><strong>Explanation</strong>: Both S and T become &quot;&quot;.</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-3-1">&quot;a##c&quot;</span>, T = <span id="example-input-3-2">&quot;#a#c&quot;</span>
<strong>Output: </strong><span id="example-output-3">true
</span><span><strong>Explanation</strong>: Both S and T become &quot;c&quot;.</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-4-1">&quot;a#c&quot;</span>, T = <span id="example-input-4-2">&quot;b&quot;</span>
<strong>Output: </strong><span id="example-output-4">false
</span><span><strong>Explanation</strong>: S becomes &quot;c&quot; while T becomes &quot;b&quot;.</span>
</pre>

<p><span><strong>Note</strong>:</span></p>

<ul>
	<li><code><span>1 &lt;= S.length &lt;= 200</span></code></li>
	<li><code><span>1 &lt;= T.length &lt;= 200</span></code></li>
	<li><span><code>S</code>&nbsp;and <code>T</code> only contain&nbsp;lowercase letters and <code>&#39;#&#39;</code> characters.</span></li>
</ul>

<p><strong>Follow up:</strong></p>

<ul>
	<li>Can you solve it in <code>O(N)</code> time and <code>O(1)</code> space?</li>
</ul>
</div>
</div>
</div>
</div>

</div>

## Tags
- Two Pointers (two-pointers)
- Stack (stack)

## Companies
- Facebook - 8 (taggedByAdmin: false)
- Amazon - 8 (taggedByAdmin: false)
- Google - 7 (taggedByAdmin: true)
- Microsoft - 4 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Atlassian - 4 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Build String [Accepted]

**Intuition**

Let's individually build the result of each string (`build(S)` and `build(T)`), then compare if they are equal.

**Algorithm**

To build the result of a string `build(S)`, we'll use a stack based approach, simulating the result of each keystroke.

<iframe src="https://leetcode.com/playground/vDnvYd8P/shared" frameBorder="0" width="100%" height="327" name="vDnvYd8P"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(M + N)$$, where $$M, N$$ are the lengths of `S` and `T` respectively.

* Space Complexity:  $$O(M + N)$$.


---
#### Approach #2: Two Pointer [Accepted]

**Intuition**

When writing a character, it may or may not be part of the final string depending on how many backspace keystrokes occur in the future.

If instead we iterate through the string in reverse, then we will know how many backspace characters we have seen, and therefore whether the result includes our character.

**Algorithm**

Iterate through the string in reverse.  If we see a backspace character, the next non-backspace character is skipped.  If a character isn't skipped, it is part of the final answer.

See the comments in the code for more details.

<iframe src="https://leetcode.com/playground/Sv67s6bn/shared" frameBorder="0" width="100%" height="500" name="Sv67s6bn"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(M + N)$$, where $$M, N$$ are the lengths of `S` and `T` respectively.

* Space Complexity:  $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] O(N) time and O(1) space
- Author: lee215
- Creation Date: Sun Jun 03 2018 11:04:52 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 09 2020 23:07:25 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
The intuition and quick methode is to find the final text result.
You can just use a string if you don\'t care cost on string modification.
Or you can use a stack or string builder to do it in O(N).

Use stack to avoid string modification. 
Time O(N) and space O(N).
```py
    def backspaceCompare(self, S, T):
        def back(res, c):
            if c != \'#\': res.append(c)
            elif res: res.pop()
            return res
        return reduce(back, S, []) == reduce(back, T, [])
```
<br>

# Follow up: O(1) Space
Can you do it in O(N) time and O(1) space?
I believe you have one difficulty here:
When we meet a char, we are not sure if it will be still there or be deleted.

However, we can do a back string compare (just like the title of problem).
If we do it backward, we meet a char and we can be sure this char won\'t be deleted.
If we meet a \'#\', it tell us we need to skip next lowercase char.

The idea is that, read next letter from end to start.
If we meet `#`, we increase the number we need to step back, until `back = 0`
<br>

**C++:**
```cpp
    bool backspaceCompare(string S, string T) {
        int i = S.length() - 1, j = T.length() - 1, back;
        while (true) {
            back = 0;
            while (i >= 0 && (back > 0 || S[i] == \'#\')) {
                back += S[i] == \'#\' ? 1 : -1;
                i--;
            }
            back = 0;
            while (j >= 0 && (back > 0 || T[j] == \'#\')) {
                back += T[j] == \'#\' ? 1 : -1;
                j--;
            }
            if (i >= 0 && j >= 0 && S[i] == T[j]) {
                i--;
                j--;
            } else {
                break;
            }
        }
        return i == -1 && j == -1;
    }
```

**Java:**
```java
    public boolean backspaceCompare(String S, String T) {
        int i = S.length() - 1, j = T.length() - 1, back;
        while (true) {
            back = 0;
            while (i >= 0 && (back > 0 || S.charAt(i) == \'#\')) {
                back += S.charAt(i) == \'#\' ? 1 : -1;
                i--;
            }
            back = 0;
            while (j >= 0 && (back > 0 || T.charAt(j) == \'#\')) {
                back += T.charAt(j) == \'#\' ? 1 : -1;
                j--;
            }
            if (i >= 0 && j >= 0 && S.charAt(i) == T.charAt(j)) {
                i--;
                j--;
            } else {
                break;
            }
        }
        return i == -1 && j == -1;
    }
```
**Python:**
```py
    def backspaceCompare(self, S, T):
        i, j = len(S) - 1, len(T) - 1
        backS = backT = 0
        while True:
            while i >= 0 and (backS or S[i] == \'#\'):
                backS += 1 if S[i] == \'#\' else -1
                i -= 1
            while j >= 0 and (backT or T[j] == \'#\'):
                backT += 1 if T[j] == \'#\' else -1
                j -= 1
            if not (i >= 0 and j >= 0 and S[i] == T[j]):
                return i == j == -1
            i, j = i - 1, j - 1
```
<br>

# Original version
**C++:**
```cpp
    bool backspaceCompare(string S, string T) {
        for (int i = S.length() - 1, j = T.length() - 1;; i--, j--) {
            for (int back = 0; i >= 0 && (back || S[i] == \'#\'); --i)
                back += S[i] == \'#\' ? 1 : -1;
            for (int back = 0; j >= 0 && (back || T[j] == \'#\'); --j)
                back += T[j] == \'#\' ? 1 : -1;
            if (i < 0 || j < 0 || S[i] != T[j]) return i == -1 && j == -1;
        }
    }
```
**Java:**
```java
    public boolean backspaceCompare(String S, String T) {
        for (int i = S.length() - 1, j = T.length() - 1;; i--, j--) {
            for (int b = 0; i >= 0 && (b > 0 || S.charAt(i) == \'#\'); --i)
                b += S.charAt(i) == \'#\' ? 1 : -1;
            for (int b = 0; j >= 0 && (b > 0 || T.charAt(j) == \'#\'); --j)
                b += T.charAt(j) == \'#\' ? 1 : -1;
            if (i < 0 || j < 0 || S.charAt(i) != T.charAt(j)) return i == -1 && j == -1;
        }
    }
```
**Python:**
```py
    def backspaceCompare(self, S, T):
        back = lambda res, c: res[:-1] if c == \'#\' else res + c
        return reduce(back, S, "") == reduce(back, T, "")
```
</p>


### [Java/C++] Efficient and Simple Solution WITHOUT Stack etc
- Author: karthickm
- Creation Date: Sun Jun 03 2018 12:43:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 01:34:28 GMT+0800 (Singapore Standard Time)

<p>
Scan the String from right to left. Maintain the "#" count. Increment \'count\' variable if the character encountered is \'#\'. Otherwise decrement the count and if the count is 0, append that character to the result.
Perform above for both the strings, compare the results and return true/false accordingly.

JAVA: 

```
class Solution {
    
    private String getString(String str) {
        int n=str.length(), count=0;
        String result="";
        for(int i=n-1; i>=0; i--) {
            char ch=str.charAt(i);
            if(ch==\'#\') 
                count++;
            else {
                if(count>0)
                    count--;
                else {
                    result+=ch;
                }                     
            }
        }
        return result;
    }
    
    public boolean backspaceCompare(String S, String T) {
        return getString(S).equals(getString(T));
    }
}
```

C++
```
class Solution {
public:    
    string getString(string &str) {
        int n=str.length(), count=0;
        string result="";
        for(int i=n-1; i>=0; i--) {
            char ch=str[i];
            if(ch==\'#\') 
                count++;
            else {
                if(count>0)
                    count--;
                else {
                    result+=ch;
                }                     
            }
        }
        return result;        
    }
    
    bool backspaceCompare(string S, string T) {
        return getString(S)==getString(T);
    }
};
```
</p>


### 👏🏻 Python - 公瑾™
- Author: yuzhoujr
- Creation Date: Fri Jul 06 2018 08:57:39 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 10:26:46 GMT+0800 (Singapore Standard Time)

<p>
# **844. Backspace String Compare**

<br>

## **Solution 1: Stack**

```
Time Complexity O(m + n)
Space Complexity O(m + n)
```

\u521A\u5F00\u59CB\u8BD5\u7740\u601D\u8003\u8FD9\u9898\u7684Two Pointers\u7B97\u6CD5\uFF0C\u6CA1\u6709\u601D\u8DEF\uFF0C\u5374\u53D1\u73B0\u8FD9\u4E2A\u9898\u7684\u6027\u8D28\u7279\u522B\u7B26\u5408Stack\uFF0C\u5C31\u7528Stack\u5148\u89E3\u51B3\u4E86\u3002\u5F53`char is not \'#\'`\u7684\u65F6\u5019\u8981\u7F29\u51CF\uFF0C\u8FD9\u548CStack\u7684`pop`\u4E00\u4E2A\u6027\u8D28\u3002

```
class Solution:
    def backspaceCompare(self, S, T):
        l1 = self.stack(S, [])
        l2 = self.stack(T, [])
        return l1 == l2
        
    
    def stack(self, S, stack):
        for char in S:
            if char is not "#":
                stack.append(char)
            else:
                if not stack:
                    continue
                stack.pop()
        return stack
```
<br><br>

##  **Solution 2: Two Pointers**
```
\u7C7B\u578B\uFF1A\u4ECE\u53F3\u5F80\u5DE6Index
Time Complexity O(n)
Space Complexity O(1)
```

\u89E3\u9898\u601D\u8DEF\uFF1A
\u5BF9\u4E24\u4E2A\u5B57\u7B26\u4E32\u4ECE\u53F3\u5F80\u5DE6\u904D\u5386\uFF0C\u7528\u5B50\u65B9\u7A0B`getChar()` \u4ECE\u4E24\u4E2A\u5B57\u7B26\u4E32\u5206\u522B\u53D6\u503C\uFF0C\u5982\u53D6\u503C\u4E0D\u7B49\u5219\u8FD4\u56DE`False`\uFF0C\u76F8\u7B49\u5C31\u7EE7\u7EED\u8FED\u4EE3\uFF0C\u77E5\u9053\u8FED\u4EE3\u6280\u672F\u8FD4\u56DE`True`

\u5B50\u65B9\u7A0B`getChar()`\u7684\u53D6\u503C\u89C4\u5219\uFF1A

```
While\u5FAA\u73AF\u9000\u51FA\u6761\u4EF6\uFF1A\u4E0B\u6807\u51FA\u754C\u6216\u8005\u8FD4\u56DE\u503C\u4E0D\u4E3A\u7A7A\u3002
Case 1: \u5F53\u503C\u7B49\u4E8E`#`, `count`\u589E\u503C
Case 2: \u5982\u679C`count == 0` \u8BF4\u660E\u8FD9\u4E2A\u503C\u6CA1\u6709\u88AB`#`\u7ED9\u62B5\u6D88\uFF0C\u8FD4\u56DE
Case 3: \u5982\u679C`count != 0` \u5207\u8FD9\u4E2A\u503C\u4E0D\u4E3A`#`\uFF0C\u8FD9\u4E2A\u503C\u8981\u88AB`#`\u62B5\u6D88\u6389
\u4E09\u4E2ACase\u8FD0\u884C\u5B8C\u540E\uFF0C\u8BB0\u5F97\u9501\u7D27\u4E0B\u6807`r`
```
```
class Solution(object):
    def backspaceCompare(self, S1, S2):
        r1 = len(S1) - 1 
        r2 = len (S2) - 1
        
        while r1 >= 0 or r2 >= 0:
            char1 = char2 = ""
            if r1 >= 0:
                char1, r1 = self.getChar(S1, r1)
            if r2 >= 0:
                char2, r2 = self.getChar(S2, r2)
            if char1 != char2:
                return False
        return True
        
    
    def getChar(self, s , r):
        char, count = \'\', 0
        while r >= 0 and not char:
            if s[r] == \'#\':
                count += 1
            elif count == 0:
                char = s[r]
            else:
                count -= 1
            r -= 1
        return char, r
```

<br><br>
</p>


