---
title: "Palindrome Permutation"
weight: 249
#id: "palindrome-permutation"
---
## Description
<div class="description">
<p>Given a string, determine if a permutation of the string could form a palindrome.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> <code>&quot;code&quot;</code>
<strong>Output:</strong> false</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> <code>&quot;aab&quot;</code>
<strong>Output:</strong> true</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> <code>&quot;carerac&quot;</code>
<strong>Output:</strong> true</pre>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Microsoft - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Uber - 0 (taggedByAdmin: true)
- Bloomberg - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force [Accepted]

If a string with an even length is a palindrome, every character in the string must always occur an even number of times. If the string with an odd length is a palindrome, every character except one of the characters must always occur an even number of times. Thus, in case of a palindrome, the number of characters with odd number of occurrences can't exceed 1(1 in case of odd length and 0 in case of even length).

Based on the above observation, we can find the solution for the given problem. The given string could contain almost all the ASCII characters from 0 to 127. Thus, we iterate over all the characters from 0 to 127. For every character chosen, we again iterate over the given string $$s$$ and find the number of occurrences, $$ch$$, of the current character in $$s$$. We also keep a track of the number of characters in the given string $$s$$ with odd number of occurrences in a variable $$\text{count}$$.

If, for any character currently considered, its corresponding count, $$ch$$, happens to be odd, we increment the value of $$\text{count}$$, to reflect the same. In case of even value of $$ch$$ for any character, the $$\text{count}$$ remains unchanged.

If, for any character, the $$count$$ becomes greater than 1, it indicates that the given string $$s$$ can't lead to the formation of a palindromic permutation based on the reasoning discussed above. But, if the value of $$\text{count}$$ remains lesser than 2 even when all the possible characters have been considered, it indicates that a palindromic permutation can be formed from the given string $$s$$.

<iframe src="https://leetcode.com/playground/2GTcGjDw/shared" frameBorder="0" name="2GTcGjDw" width="100%" height="309"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We iterate constant number of times(128) over the string $$s$$ of length $$n$$, _i.e._ $$O(128 \cdot n) = O(n)$$.

* Space complexity : $$O(1)$$. Constant extra space is used.

---
#### Approach #2 Using HashMap [Accepted]

**Algorithm**

From the discussion above, we know that to solve the given problem, we need to count the number of characters with odd number of occurrences in the given string $$s$$. To do so, we can also make use of a hashmap, $$\text{map}$$. This $$\text{map}$$ takes the form $$(\text{character}, \text{number of occurrences of character})$$.

We traverse over the given string $$s$$. For every new character found in $$s$$, we create a new entry in the $$\text{map}$$ for this character with the number of occurrences as 1. Whenever we find the same character again, we update the number of occurrences appropriately. 

At the end, we traverse over the $$\text{map}$$ created and find the number of characters with odd number of occurrences. If this $$\text{count}$$ happens to exceed 1 at any step,  we conclude that a palindromic permutation isn't possible for the string $$s$$. But, if we can reach the end of the string with $$\text{count}$$ lesser than 2, we conclude that a palindromic permutation is possible for $$s$$.

The following animation illustrates the process.

!?!../Documents/266_Palindrome_Permutation.json:1000,563!?!

<iframe src="https://leetcode.com/playground/QedxyvpM/shared" frameBorder="0" name="QedxyvpM" width="100%" height="292"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We traverse over the given string $$s$$ with $$n$$ characters once. We also traverse over the $$\text{map}$$ which can grow up to a size of $$n$$ in case all characters in $$s$$ are distinct.

* Space complexity : $$O(1)$$. The $$\text{map}$$ can grow up to a maximum number of all distinct elements. However, the number of distinct characters are bounded, so as the space complexity.


---
#### Approach #3 Using Array [Accepted]

**Algorithm**

Instead of making use of the inbuilt Hashmap, we can make use of an array as a hashmap. For this, we make use of an array $$\text{map}$$ with length 128. Each index of this $$\text{map}$$ corresponds to one of the 128 ASCII characters possible.

We traverse over the string $$s$$ and put in the number of occurrences of each character in this $$\text{map}$$ appropriately as done in the last case. Later on, we find the number of characters with odd number of occurrences to determine if a palindromic permutation is possible for the string $$s$$ or not as done in previous approaches.

<iframe src="https://leetcode.com/playground/pvhRBZgk/shared" frameBorder="0" name="pvhRBZgk" width="100%" height="275"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n)$$. We traverse once over the string $$s$$ of length $$n$$. Then, we traverse over the $$\text{map}$$ of length 128(constant).

* Space complexity : $$O(1)$$. Constant extra space is used for $$\text{map}$$ of size 128.

---
#### Approach #4 Single Pass [Accepted]:

**Algorithm**

Instead of first traversing over the string $$s$$ for finding the number of occurrences of each element and then determining the $$\text{count}$$ of characters with odd number of occurrences in $$s$$, we can determine the value of $$\text{count}$$ on the fly while traversing over $$s$$.

For this, we traverse over $$s$$ and update the number of occurrences of the character just encountered in the $$\text{map}$$. But, whevenever we update any entry in $$\text{map}$$, we also check if its value becomes even or odd. We start of with a $$\text{count}$$ value of 0. If the value  of the entry just updated in $$map$$ happens to be odd, we increment the value of $$\text{count}$$ to indicate that one more character with odd number of occurrences has been found. But, if this entry happens to be even, we decrement the value of $$\text{count}$$ to indicate that the number of characters with odd number of occurrences has reduced by one. 

But, in this case, we need to traverse till the end of the string to determine the final result, unlike the last approaches, where we could stop the traversal over $$\text{map}$$ as soon as the $$\text{count}$$ exceeded 1. This is because, even if the number of elements with odd number of occurrences may seem very large at the current moment, but their occurrences could turn out to be even when we traverse further in the string $$s$$.

At the end, we again check if the value of $$\text{count}$$ is lesser than 2 to conclude that a palindromic permutation is possible for the string $$s$$.

<iframe src="https://leetcode.com/playground/5AuTxAxU/shared" frameBorder="0" name="5AuTxAxU" width="100%" height="292"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We traverse over the string $$s$$ of length $$n$$ once only.

* Space complexity : $$O(1)$$. A $$map$$ of constant size(128) is used.

---
#### Approach #5 Using Set [Accepted]:

**Algorithm**

Another modification of the last approach could be by making use of a $$\text{set}$$ for keeping track of the number of elements with odd number of occurrences in $$s$$. For doing this, we traverse over the characters of the string $$s$$. Whenever the number of occurrences of a character becomes odd, we put its entry in the $$\text{set}$$. Later on, if we find the same element again, lead to its number of occurrences as even, we remove its entry from the $$\text{set}$$. Thus, if the element occurs again(indicating an odd number of occurrences), its entry won't exist in the $$\text{set}$$.

Based on this idea, when we find a character in the string $$s$$ that isn't present in the $$\text{set}$$(indicating an odd number of occurrences currently for this character), we put its corresponding entry in the $$\text{set}$$. If we find a character that is already present in the $$\text{set}$$(indicating an even number of occurrences currently for this character), we remove its corresponding entry from the $$\text{set}$$.

At the end, the size of $$\text{set}$$ indicates the number of elements with odd number of occurrences in $$s$$. If it is lesser than 2, a palindromic permutation of the string $$s$$ is possible, otherwise not.

Below code is inspired by [@StefanPochmann](http://leetcode.com/stefanpochmann)

<iframe src="https://leetcode.com/playground/PwqWXcwG/shared" frameBorder="0" name="PwqWXcwG" width="100%" height="241"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We traverse over the string $$s$$ of length $$n$$ once only.

* Space complexity : $$O(1)$$. The $$\text{set}$$ can grow up to a maximum number of all distinct elements. However, the number of distinct characters are bounded, so as the space complexity.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution w/Set, one pass, without counters.
- Author: ammv
- Creation Date: Fri Aug 21 2015 19:21:28 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 07:16:03 GMT+0800 (Singapore Standard Time)

<p>
The idea is to iterate over string, adding current character to `set` if `set` doesn't contain that character, or removing current character from `set` if `set` contains it.
When the iteration is finished, just return `set.size()==0 || set.size()==1`.

`set.size()==0` corresponds to the situation when there are even number of any character in the string, and
`set.size()==1` corresponsds to the fact that there are even number of any character except one.

    public class Solution {
        public boolean canPermutePalindrome(String s) {
            Set<Character> set=new HashSet<Character>();
            for(int i=0; i<s.length(); ++i){
                if (!set.contains(s.charAt(i)))
                    set.add(s.charAt(i));
                else 
                    set.remove(s.charAt(i));
            }
            return set.size()==0 || set.size()==1;
        }
    }
</p>


### 1-4 lines Python, Ruby, C++, C, Java
- Author: StefanPochmann
- Creation Date: Fri Aug 21 2015 09:04:28 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 21 2015 09:04:28 GMT+0800 (Singapore Standard Time)

<p>
Just check that no more than one character appears an odd number of times. Because if there is one, then it must be in the middle of the palindrome. So we can't have two of them.

**Python**

First count all characters in a `Counter`, then count the odd ones.

    def canPermutePalindrome(self, s):
        return sum(v % 2 for v in collections.Counter(s).values()) < 2

**Ruby**

Using an integer as a bitset (Ruby has arbitrarily large integers).

    def can_permute_palindrome(s)
      x = s.chars.map { |c| 1 << c.ord }.reduce(0, :^)
      x & x-1 == 0
    end

**C++**

Using a bitset.

    bool canPermutePalindrome(string s) {
        bitset<256> b;
        for (char c : s)
            b.flip(c);
        return b.count() < 2;
    }

**C**

Tricky one. Increase `odds` when the increased counter is odd, decrease it otherwise.

    bool canPermutePalindrome(char* s) {
        int ctr[256] = {}, odds = 0;
        while (*s)
            odds += ++ctr[*s++] & 1 ? 1 : -1;
        return odds < 2;
    }

Thanks to jianchao.li.fighter for pointing out a nicer way in the comments to which I switched now because it's clearer and faster. Some speed test results (see comments for details):

            odds += ++ctr[*s++] % 2 * 2 - 1;       // 1499 ms mean-of-five (my original)
            odds += (ctr[*s++] ^= 1) * 2 - 1;      // 1196 ms mean-of-five
            odds += ++ctr[*s++] % 2 ? 1 : -1;      // 1108 ms mean-of-five
            odds += ((++ctr[*s++] & 1) << 1) - 1;  // 1217 ms mean-of-five
            odds += ++ctr[*s++] & 1 ? 1 : -1;      // 1132 ms mean-of-five

**Java**

Using a BitSet.

    public boolean canPermutePalindrome(String s) {
        BitSet bs = new BitSet();
        for (byte b : s.getBytes())
            bs.flip(b);
        return bs.cardinality() < 2;
    }
</p>


### 5-lines simple JAVA solution with explanation
- Author: Cheng_Zhang
- Creation Date: Mon Nov 23 2015 13:04:47 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 23 2015 13:04:47 GMT+0800 (Singapore Standard Time)

<p>
**Explanation**

The basic idea is using HashSet to find the number of single characters, which should be at most 1.

    public boolean canPermutePalindrome(String s) {
    	Set<Character>set = new HashSet<Character>();
    	for (char c : s.toCharArray())  
    		if (set.contains(c)) set.remove(c);// If char already exists in set, then remove it from set
    		else set.add(c);// If char doesn't exists in set, then add it to set
    	return set.size() <= 1;
    }
</p>


