---
title: "Maximum Distance in Arrays"
weight: 559
#id: "maximum-distance-in-arrays"
---
## Description
<div class="description">
<p>You are given&nbsp;<code>m</code>&nbsp;<code>arrays</code>, where each array is sorted in <strong>ascending order</strong>. Now you can pick up two integers from two different arrays (each array picks one) and calculate the distance. We define the distance between two integers <code>a</code> and <code>b</code> to be their absolute difference <code>|a - b|</code>. Your task is to find the maximum distance.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arrays = [[1,2,3],[4,5],[1,2,3]]
<strong>Output:</strong> 4
<strong>Explanation:</strong> One way to reach the maximum distance 4 is to pick 1 in the first or third array and pick 5 in the second array.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arrays = [[1],[1]]
<strong>Output:</strong> 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arrays = [[1],[2]]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arrays = [[1,4],[0,5]]
<strong>Output:</strong> 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m == arrays.length</code></li>
	<li><code>2&nbsp;&lt;= m &lt;= 10<sup>4</sup></code></li>
	<li><code>1 &lt;= arrays[i].length &lt;= 500</code></li>
	<li><code>-10<sup>4</sup> &lt;= arrays[i][j] &lt;= 10<sup>4</sup></code></li>
	<li><code>arrays[i]</code> is sorted in <strong>ascending order</strong>.</li>
	<li>There will be at most <code>10<sup>5</sup></code> integers in all the arrays.</li>
</ul>

</div>

## Tags
- Array (array)
- Hash Table (hash-table)

## Companies
- Yahoo - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

The simplest solution is to pick up every element of every array from the $$list$$ and find its distance from every element in all the other arrays except itself and find the largest distance from out of those.

<iframe src="https://leetcode.com/playground/rge5K69S/shared" frameBorder="0" name="rge5K69S" width="100%" height="326"></iframe>

**Complexity Analysis**

* Time complexity : $$O((n*x)^2)$$. We traverse over all the arrays in $$list$$ for every element of every array considered. Here, $$n$$ refers to the number of arrays in the $$list$$ and $$x$$ refers to the average number of elements in each array in the $$list$$.

* Space complexity : $$O(1)$$. Constant extra space is used.

---
#### Approach #2 Better Brute Force [Time Limit Exceeded]

**Algorithm**

In the last approach, we didn't make use of the fact that every array in the $$list$$ is sorted. Thus, instead of considering the distances among all the elements of all the arrays(except intra-array elements), we can consider only the distances between the first(minimum element) element of an array and the last(maximum element) element of the other arrays and find out the maximum distance from among all such distances. 


<iframe src="https://leetcode.com/playground/QdXWERJK/shared" frameBorder="0" name="QdXWERJK" width="100%" height="275"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. We consider only max and min values directly for every array currenty considered. Here, $$n$$ refers to the number of arrays in the $$list$$.

* Space complexity : $$O(1)$$. Constant extra space is used.

---
#### Approach #3 Single Scan [Accepted]

**Algorithm**

As discussed already, in order to find out the maximum distance between any two arrays, we need not compare every element of the arrays, since the arrays are already sorted. Thus, we can consider only the extreme points in the arrays to do the distance calculations.

Further, the two points being considered for the distance calculation should not both belong to the same array. Thus, for arrays $$a$$ and $$b$$ currently chosen, we can just find the maximum out of $$a[n-1]-b[0]$$ and $$b[m-1]-a[0]$$ to find the larger distance. Here, $$n$$ and $$m$$ refer to the lengths of arrays $$a$$ and $$b$$ respectively. 

But, we need not compare all the array pairs possible to find the maximum distance. Instead, we can keep on traversing over the arrays in the $$list$$ and keep a track of the maximum distance found so far. 

To do so, we keep a track of the element with minimum value($$min\_val$$) and the one with maximum value($$max\_val$$) found so far. Thus, now these extreme values can be treated as if they represent the extreme points of a cumulative array of all the arrays that have been considered till now. 

For every new array, $$a$$ considered, we find the distance $$a[n-1]-min\_val$$ and $$max\_val - a[0]$$ to compete with the maximum distance found so far. Here, $$n$$ refers to the number of elements in the current array, $$a$$. Further, we need to note that the maximum distance found till now needs not always be contributed by the end points of the distance being $$max\_val$$ and $$min\_val$$. 

But, such points could help in maximizing the distance in the future. Thus, we need to keep track of these maximum and minimum values along with the maximum distance found so far for future calculations. But, in general, the final maximum distance found will always be determined by one of these extreme values, $$max\_val$$ and $$min\_val$$, or in some cases, by both of them.

The following animation illustrates the process.

!?!../Documents/624_Maximum_Distance.json:1000,563!?!

From the above illustration, we can clearly see that although the $$max\_val$$ or $$min\_val$$ could not contribute to the local maximum distance values, they could later on contribute to the maximum distance.

<iframe src="https://leetcode.com/playground/tiaZo26H/shared" frameBorder="0" name="tiaZo26H" width="100%" height="241"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n)$$. We traverse over the $$list$$ of length $$n$$ once only.

* Space complexity : $$O(1)$$. Constant extra space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution, Min and Max
- Author: shawngao
- Creation Date: Sun Jun 18 2017 11:13:46 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 06:11:56 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public int maxDistance(int[][] arrays) {
        int result = Integer.MIN_VALUE;
        int max = arrays[0][arrays[0].length - 1];
        int min = arrays[0][0];
        
        for (int i = 1; i < arrays.length; i++) {
            result = Math.max(result, Math.abs(arrays[i][0] - max));
            result = Math.max(result, Math.abs(arrays[i][arrays[i].length - 1] - min));
            max = Math.max(max, arrays[i][arrays[i].length - 1]);
            min = Math.min(min, arrays[i][0]);
        }
        
        return result;
    }
}
```
LeetCode updated the input to List.
```
public class Solution {
    public int maxDistance(List<List<Integer>> arrays) {
        int result = Integer.MIN_VALUE;
        int max = arrays.get(0).get(arrays.get(0).size() - 1);
        int min = arrays.get(0).get(0);
        
        for (int i = 1; i < arrays.size(); i++) {
            result = Math.max(result, Math.abs(arrays.get(i).get(0) - max));
            result = Math.max(result, Math.abs(arrays.get(i).get(arrays.get(i).size() - 1) - min));
            max = Math.max(max, arrays.get(i).get(arrays.get(i).size() - 1));
            min = Math.min(min, arrays.get(i).get(0));
        }
        
        return result;
    }
}
```
</p>


### 5-liner C++ and Python O(m) simple solution, O(1) space
- Author: zzg_zzm
- Creation Date: Tue Jun 20 2017 10:57:38 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 12 2018 18:57:57 GMT+0800 (Singapore Standard Time)

<p>
Because each array is sorted, just need to consider the first and last elements in each array.

When you scan another array `a`, the answer `maxDif` is simply given by
```cpp
maxDif = max(maxDif, max(a.back()-curMin, curMax-a.front()));
```
where `curMin` and `curMax` are current min and max values in all previous arrays.
```cpp
    int maxDistance(vector<vector<int>>& arrays) {
        int maxDif = 0, curMin = 10000, curMax = -10000;
        for (auto& a : arrays) {
            maxDif = max(maxDif, max(a.back()-curMin, curMax-a.front()));
            curMin = min(curMin, a.front()), curMax = max(curMax, a.back());
        }
        return maxDif;
    }
```
Python:
```py
    def maxDistance(self, arrays):
        res, curMin, curMax = 0, 10000, -10000
        for a in arrays :
            res = max(res, max(a[-1]-curMin, curMax-a[0]))
            curMin, curMax = min(curMin, a[0]), max(curMax, a[-1])
        return res
```
</p>


