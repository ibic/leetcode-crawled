---
title: "Matrix Cells in Distance Order"
weight: 1003
#id: "matrix-cells-in-distance-order"
---
## Description
<div class="description">
<p>We are given a matrix with <code>R</code> rows and <code>C</code> columns has cells with integer coordinates&nbsp;<code>(r, c)</code>, where <code>0 &lt;= r &lt; R</code> and <code>0 &lt;= c &lt; C</code>.</p>

<p>Additionally, we are given a cell in that matrix with coordinates&nbsp;<code>(r0, c0)</code>.</p>

<p>Return the coordinates of&nbsp;all cells in the matrix, sorted by their distance from <code>(r0, c0)</code>&nbsp;from smallest distance to largest distance.&nbsp; Here,&nbsp;the distance between two cells <code>(r1, c1)</code> and <code>(r2, c2)</code> is the Manhattan distance,&nbsp;<code>|r1 - r2| + |c1 - c2|</code>.&nbsp; (You may return the answer in any order that satisfies this condition.)</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>R = <span id="example-input-1-1">1</span>, C = <span id="example-input-1-2">2</span>, r0 = <span id="example-input-1-3">0</span>, c0 = <span id="example-input-1-4">0</span>
<strong>Output: </strong><span id="example-output-1">[[0,0],[0,1]]
<strong>Explanation:</strong> The distances from (r0, c0) to other cells are: [0,1]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>R = <span id="example-input-2-1">2</span>, C = <span id="example-input-2-2">2</span>, r0 = <span id="example-input-2-3">0</span>, c0 = <span id="example-input-2-4">1</span>
<strong>Output: </strong><span id="example-output-2">[[0,1],[0,0],[1,1],[1,0]]
</span><span id="example-output-1"><strong>Explanation:</strong> The distances from (r0, c0) to other cells are:</span><span> [0,1,1,2]</span>
The answer [[0,1],[1,1],[0,0],[1,0]] would also be accepted as correct.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>R = <span id="example-input-3-1">2</span>, C = <span id="example-input-3-2">3</span>, r0 = <span id="example-input-3-3">1</span>, c0 = <span id="example-input-3-4">2</span>
<strong>Output: </strong><span id="example-output-3">[[1,2],[0,2],[1,1],[0,1],[1,0],[0,0]]</span>
<span id="example-output-1"><strong>Explanation:</strong> The distances from (r0, c0) to other cells are:</span><span> [0,1,1,2,2,3]</span>
There are other answers that would also be accepted as correct, such as [[1,2],[1,1],[0,2],[1,0],[0,1],[0,0]].
</pre>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ol>
	<li><code>1 &lt;= R &lt;= 100</code></li>
	<li><code>1 &lt;= C &lt;= 100</code></li>
	<li><code>0 &lt;= r0 &lt; R</code></li>
	<li><code>0 &lt;= c0 &lt; C</code></li>
</ol>
</div>
</div>
</div>

</div>

## Tags
- Sort (sort)

## Companies
- Yahoo - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### O(N) Java BFS
- Author: _topspeed_
- Creation Date: Sun Apr 21 2019 12:50:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 21 2019 12:50:26 GMT+0800 (Singapore Standard Time)

<p>
```
public int[][] allCellsDistOrder(int R, int C, int r0, int c0) {
    boolean[][] visited = new boolean[R][C];
    int[][] result = new int[R * C][2];
    int i = 0;
    Queue<int[]> queue = new LinkedList<int[]>();
    queue.offer(new int[]{r0, c0});
    while (!queue.isEmpty()) {
      int[] cell = queue.poll();
      int r = cell[0];
      int c = cell[1];

      if (r < 0 || r >= R || c < 0 || c >= C) {
        continue;
      }
      if (visited[r][c]) {
        continue;
      }

      result[i] = cell;
      i++;
      visited[r][c] = true;

      queue.offer(new int[]{r, c - 1});
      queue.offer(new int[]{r, c + 1});
      queue.offer(new int[]{r - 1, c});
      queue.offer(new int[]{r + 1, c});
    }
    return result;
  }
  ```
</p>


### Python 1-line sorting based solution
- Author: Twohu
- Creation Date: Sun Apr 21 2019 12:18:01 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 07 2019 10:09:45 GMT+0800 (Singapore Standard Time)

<p>
```
def allCellsDistOrder(self, R: int, C: int, r0: int, c0: int) -> List[List[int]]:
    return sorted([(i, j) for i in range(R) for j in range(C)], key=lambda p: abs(p[0] - r0) + abs(p[1] - c0))
```

The same code, more readable:
```
def allCellsDistOrder(self, R: int, C: int, r0: int, c0: int) -> List[List[int]]:
    def dist(point):
        pi, pj = point
        return abs(pi - r0) + abs(pj - c0)

    points = [(i, j) for i in range(R) for j in range(C)]
    return sorted(points, key=dist)
```
</p>


### 4ms O(n) Java Counting Sort (計數排序) Solution
- Author: nullpointer01
- Creation Date: Sun Apr 21 2019 15:54:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 21 2019 15:54:31 GMT+0800 (Singapore Standard Time)

<p>
The max distance is `R + C`, and the result array\'s length is `R * C`. Since the distance is limited (generally, compared with the cell count), we can use Counting Sort (\u8A08\u6578\u6392\u5E8F) to solve it efficiently.

Time complexity is `O(R * C)`, i.e. `O(n)`.

> **66 / 66** test cases passed.
> **Status**: Accepted
> **Runtime**: 4 ms
> **Memory Usage**: 38.5 MB

```java
class Solution {
    public int[][] allCellsDistOrder(int R, int C, int r0, int c0) {
        int[] counter = new int[R + C + 1];
        for (int r = 0; r < R; r++) {
            for (int c = 0; c < C; c++) {
                int dist = Math.abs(r - r0) + Math.abs(c - c0);
                counter[dist + 1] += 1;
            }
        }
        
        for (int i = 1; i < counter.length; i++) {
            counter[i] += counter[i - 1];
        }
        
        int[][] ans = new int[R * C][];
        for (int r = 0; r < R; r++) {
            for (int c = 0; c < C; c++) {
                int dist = Math.abs(r - r0) + Math.abs(c - c0);
                ans[counter[dist]] = new int[] { r, c };
                counter[dist]++;
            }
        }
        
        return ans;
    }
}
```
</p>


