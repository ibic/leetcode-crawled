---
title: "Restore IP Addresses"
weight: 93
#id: "restore-ip-addresses"
---
## Description
<div class="description">
<p>Given a string <code>s</code> containing only digits, return all possible valid IP addresses that can be obtained from <code>s</code>. You can return them in <strong>any</strong> order.</p>

<p>A <strong>valid IP address</strong> consists of exactly four integers, each integer is between <code>0</code> and <code>255</code>, separated by single dots and cannot have leading zeros. For example, &quot;0.1.2.201&quot; and &quot;192.168.1.1&quot; are <strong>valid</strong> IP addresses and &quot;0.011.255.245&quot;, &quot;192.168.1.312&quot; and &quot;192.168@1.1&quot; are <strong>invalid</strong> IP addresses.&nbsp;</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> s = "25525511135"
<strong>Output:</strong> ["255.255.11.135","255.255.111.35"]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> s = "0000"
<strong>Output:</strong> ["0.0.0.0"]
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> s = "1111"
<strong>Output:</strong> ["1.1.1.1"]
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> s = "010010"
<strong>Output:</strong> ["0.10.0.10","0.100.1.0"]
</pre><p><strong>Example 5:</strong></p>
<pre><strong>Input:</strong> s = "101023"
<strong>Output:</strong> ["1.0.10.23","1.0.102.3","10.1.0.23","10.10.2.3","101.0.2.3"]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= s.length &lt;= 3000</code></li>
	<li><code>s</code> consists of digits only.</li>
</ul>

</div>

## Tags
- String (string)
- Backtracking (backtracking)

## Companies
- Facebook - 8 (taggedByAdmin: false)
- Amazon - 6 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- VMware - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Splunk - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Intuition

The naive solution would be to brute-force, 
_i.e._ to check all possible positions for the dots 
and keep only the valid ones. In the worst case that 
means `11` possible positions and hence $$11 \times 10 \times 9
= 990$$ validations.

That could be optimized with the help of two conceptions.

> The first one is called _constrained programming_. 

That basically means
to put restrictions after each dot placement. 

If one already put a dot that leaves only `3` possibilities
for the next dot to be placed : after one digit, 
after two digits, or after three digits. The first dot has 
only `3` available slots as well.

That propagates _constraints_ 
and helps to reduce a number of combinations to consider.
Instead of $$990$$ combinations it's enough to 
check just $$3 \times 3 \times 3 = 27$$.

> The second one called _backtracking_. 

Let's imagine that one put one or two dots already and that left
no way to place the others to create a valid IP address. 
What to do? _To backtrack_. That means to come back,
to change the position of the previously placed dot and try 
to proceed again. If that would not work either, _backtrack_ again.
<br />
<br />


---
#### Approach 1: Backtracking (DFS)

Here is an algorithm for the backtrack function
`backtrack(prev_pos = -1, dots = 3)` which takes 
position of the previously placed dot `prev_pos`
and number of dots to place `dots` as arguments :

* Iterate over three available slots `curr_pos` to place a dot. 
   * Check if the segment from the previous dot to the current one is valid :
      * Yes :
        * Place the dot.
        * Check if all `3` dots are placed :
            * Yes :
                * Add the solution into the output list.
            * No :
                * Proceed to place next dots `backtrack(curr_pos, dots - 1)`.
        * Remove the last dot to backtrack. 

!?!../Documents/93_LIS.json:1000,612!?!

<iframe src="https://leetcode.com/playground/Jmzi5Ugk/shared" frameBorder="0" width="100%" height="500" name="Jmzi5Ugk"></iframe>

**Complexity Analysis**

* Time complexity : as discussed above, there is not more than `27`
combinations to check. 
* Space complexity : constant space to keep the solutions, not more 
than `19` valid IP addresses. 

<img src="../Figures/93/93_max_number.png" width="500">

## Accepted Submission (python3)
```python3
class Solution:
    def restoreIpAddresses(self, s: str) -> List[str]:
        def dfs(sLeft: str, segs: List[str]):
            lens = len(sLeft)
            lenSeg = len(segs)
            if lens == 0 and lenSeg == 4:
                result.append('.'.join(segs))
                return
            if lens == 0 or lenSeg == 4:
                return
            if lens >= 1:
                segs.append(sLeft[:1])
                dfs(sLeft[1:], segs)
                del segs[lenSeg]
            if lens >= 2 and sLeft[0] != '0':
                segs.append(sLeft[:2])
                dfs(sLeft[2:], segs)
                del segs[lenSeg]
            if lens >= 3 and sLeft[0] != '0' and int(sLeft[:3]) < 256:
                segs.append(sLeft[:3])
                dfs(sLeft[3:], segs)
                del segs[lenSeg]
        result = []
        dfs(s, [])
        return result 
```

## Top Discussions
### My code in Java
- Author: fiona_mao
- Creation Date: Thu Oct 09 2014 10:45:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 15:09:49 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public List<String> restoreIpAddresses(String s) {
            List<String> res = new ArrayList<String>();
            int len = s.length();
            for(int i = 1; i<4 && i<len-2; i++){
                for(int j = i+1; j<i+4 && j<len-1; j++){
                    for(int k = j+1; k<j+4 && k<len; k++){
                        String s1 = s.substring(0,i), s2 = s.substring(i,j), s3 = s.substring(j,k), s4 = s.substring(k,len);
                        if(isValid(s1) && isValid(s2) && isValid(s3) && isValid(s4)){
                            res.add(s1+"."+s2+"."+s3+"."+s4);
                        }
                    }
                }
            }
            return res;
        }
        public boolean isValid(String s){
            if(s.length()>3 || s.length()==0 || (s.charAt(0)=='0' && s.length()>1) || Integer.parseInt(s)>255)
                return false;
            return true;
        }
    }

3-loop divides the string s into 4 substring: s1, s2, s3, s4. Check if each substring is valid.
In isValid, strings whose length greater than 3 or equals to 0 is not valid; or if the string's length is longer than 1 and the first letter is '0' then it's invalid; or the string whose integer representation greater than 255 is invalid.
</p>


### WHO CAN BEAT THIS CODE ?
- Author: mitbbs8080
- Creation Date: Fri Feb 26 2016 13:55:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 04:50:22 GMT+0800 (Singapore Standard Time)

<p>

        // c++  code
        vector<string> restoreIpAddresses(string s) {
            vector<string> ret;
            string ans;
            
            for (int a=1; a<=3; a++)
            for (int b=1; b<=3; b++)
            for (int c=1; c<=3; c++)
            for (int d=1; d<=3; d++)
                if (a+b+c+d == s.length()) {
                    int A = stoi(s.substr(0, a));
                    int B = stoi(s.substr(a, b));
                    int C = stoi(s.substr(a+b, c));
                    int D = stoi(s.substr(a+b+c, d));
                    if (A<=255 && B<=255 && C<=255 && D<=255)
                        if ( (ans=to_string(A)+"."+to_string(B)+"."+to_string(C)+"."+to_string(D)).length() == s.length()+3)
                            ret.push_back(ans);
                }    
            
            return ret;
        }
</p>


### Very simple DFS solution
- Author: greatgrahambini
- Creation Date: Tue Nov 04 2014 11:59:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 10:15:58 GMT+0800 (Singapore Standard Time)

<p>
    public List<String> restoreIpAddresses(String s) {
        List<String> solutions = new ArrayList<String>();
        restoreIp(s, solutions, 0, "", 0);
        return solutions;
    }
    
    private void restoreIp(String ip, List<String> solutions, int idx, String restored, int count) {
        if (count > 4) return;
        if (count == 4 && idx == ip.length()) solutions.add(restored);
        
        for (int i=1; i<4; i++) {
            if (idx+i > ip.length()) break;
            String s = ip.substring(idx,idx+i);
            if ((s.startsWith("0") && s.length()>1) || (i==3 && Integer.parseInt(s) >= 256)) continue;
            restoreIp(ip, solutions, idx+i, restored+s+(count==3?"" : "."), count+1);
        }
    }
</p>


