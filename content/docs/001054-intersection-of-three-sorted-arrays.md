---
title: "Intersection of Three Sorted Arrays"
weight: 1054
#id: "intersection-of-three-sorted-arrays"
---
## Description
<div class="description">
<p>Given three integer arrays <code>arr1</code>, <code>arr2</code> and <code>arr3</code>&nbsp;<strong>sorted</strong> in <strong>strictly increasing</strong> order, return a sorted array of <strong>only</strong>&nbsp;the&nbsp;integers that appeared in <strong>all</strong> three arrays.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr1 = [1,2,3,4,5], arr2 = [1,2,5,7,9], arr3 = [1,3,4,5,8]
<strong>Output:</strong> [1,5]
<strong>Explanation: </strong>Only 1 and 5 appeared in the three arrays.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr1.length, arr2.length, arr3.length &lt;= 1000</code></li>
	<li><code>1 &lt;= arr1[i], arr2[i], arr3[i] &lt;= 2000</code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Two Pointers (two-pointers)

## Companies
- Facebook - 4 (taggedByAdmin: true)
- TripAdvisor - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java solution - beats 100%
- Author: harsh_vardan
- Creation Date: Sun Oct 06 2019 00:02:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 03:05:05 GMT+0800 (Singapore Standard Time)

<p>
```
   public List<Integer> arraysIntersection(int[] arr1, int[] arr2, int[] arr3) {

        List<Integer> result = new ArrayList<>();

        int i = 0;
        int j = 0;
        int k = 0;
		
        while (i < arr1.length && j < arr2.length && k < arr3.length) {
            if (arr1[i] == arr2[j] && arr2[j] == arr3[k]) {
                result.add(arr1[i]);
                i++;
                j++;
                k++;
            } else if (arr1[i] < arr2[j]) {
                i++;
            } else if (arr2[j] < arr3[k]) {
                j++;
            } else k++;
        }

        return result;

    }
</p>


### Java O(n) solution using 3 pointers
- Author: Sun_WuKong
- Creation Date: Sun Oct 06 2019 00:01:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 00:01:15 GMT+0800 (Singapore Standard Time)

<p>
Using 3 pointers in 3 arrays starting at 0. Iterate untill one of them reaches the end of its array.
At any given time, if 3 number pointed by 3 pointers are the same, add it to the list and move 3 pointers to the right.
If they aren\'t the same, move the pointers that point to smaller number to the right.

```
class Solution {
    public List<Integer> arraysIntersection(int[] arr1, int[] arr2, int[] arr3) {
        List<Integer> list = new ArrayList();
        int p1 = 0, p2 = 0, p3 = 0;
        while (p1 < arr1.length && p2 < arr2.length && p3 < arr3.length) {
            int min = Math.min(arr1[p1], Math.min(arr2[p2], arr3[p3]));
            if (arr1[p1] == arr2[p2] && arr1[p1] == arr3[p3]) list.add(arr1[p1]);
            if (arr1[p1] == min) p1++;
            if (arr2[p2] == min) p2++;
            if (arr3[p3] == min) p3++;
        }
        return list;
    }
}
```
</p>


### [Python] 3 pointers
- Author: eric496
- Creation Date: Wed Oct 09 2019 04:29:47 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 24 2019 06:21:46 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def arraysIntersection(self, arr1: List[int], arr2: List[int], arr3: List[int]) -> List[int]:
        i = j = k = 0
        res = []
        
        while i < len(arr1) and j < len(arr2) and k < len(arr3):
            if arr1[i] == arr2[j] == arr3[k]:
                res.append(arr1[i])
                i += 1 
                j += 1
                k += 1
                continue
            
            max_ = max(arr1[i], arr2[j], arr3[k])
            
            if arr1[i] < max_:
                i += 1
            
            if arr2[j] < max_:
                j += 1
                
            if arr3[k] < max_:
                k += 1
                
        return res
```

More syntactically concise:

```
class Solution:
    def arraysIntersection(self, arr1: List[int], arr2: List[int], arr3: List[int]) -> List[int]:
        i = j = k = 0
        res = []
        
        while i<len(arr1) and j<len(arr2) and k<len(arr3):
            if arr1[i] == arr2[j] == arr3[k]:
                res.append(arr1[i])
                i, j, k = i+1, j+1, k+1
                continue
            
            max_ = max(arr1[i], arr2[j], arr3[k])
            i = i+1 if arr1[i]<max_ else i
            j = j+1 if arr2[j]<max_ else j
            k = k+1 if arr3[k]<max_ else k
                
        return res
```
</p>


