---
title: "Assign Cookies"
weight: 432
#id: "assign-cookies"
---
## Description
<div class="description">
<p>Assume you are an awesome parent and want to give your children some cookies. But, you should give each child at most one cookie.</p>

<p>Each child <code>i</code> has&nbsp;a greed factor <code>g[i]</code>, which is the minimum size of a cookie that the child will be content with; and each cookie <code>j</code> has a size <code>s[j]</code>. If <code>s[j]&nbsp;&gt;= g[i]</code>, we can assign the cookie <code>j</code> to the child <code>i</code>, and the child <code>i</code> will be content. Your goal is to maximize the number of your content children and output the maximum number.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> g = [1,2,3], s = [1,1]
<strong>Output:</strong> 1
<strong>Explanation:</strong> You have 3 children and 2 cookies. The greed factors of 3 children are 1, 2, 3. 
And even though you have 2 cookies, since their size is both 1, you could only make the child whose greed factor is 1 content.
You need to output 1.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> g = [1,2], s = [1,2,3]
<strong>Output:</strong> 2
<strong>Explanation:</strong> You have 2 children and 3 cookies. The greed factors of 2 children are 1, 2. 
You have 3 cookies and their sizes are big enough to gratify all of the children, 
You need to output 2.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= g.length &lt;= 3 * 10<sup>4</sup></code></li>
	<li><code>0 &lt;= s.length &lt;= 3 * 10<sup>4</sup></code></li>
	<li><code>1 &lt;= g[i], s[j] &lt;=&nbsp;2<sup>31</sup> - 1</code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Greedy Java Solution
- Author: fabrizio3
- Creation Date: Sun Nov 13 2016 20:41:07 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 15:43:20 GMT+0800 (Singapore Standard Time)

<p>
```
Arrays.sort(g);
Arrays.sort(s);
int i = 0;
for(int j=0;i<g.length && j<s.length;j++) {
	if(g[i]<=s[j]) i++;
}
return i;
```

Just assign the cookies starting from the child with less greediness to maximize the number of happy children .
</p>


### Simple PYTHON O(nlogn)
- Author: moonstone1011
- Creation Date: Wed Nov 16 2016 14:17:04 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 16 2016 14:17:04 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def findContentChildren(self, g, s):
        """
        :type g: List[int]
        :type s: List[int]
        :rtype: int
        """
        g.sort()
        s.sort()
        
        childi = 0
        cookiei = 0
        
        while cookiei < len(s) and childi < len(g):
            if s[cookiei] >= g[childi]:
                childi += 1
            cookiei += 1
        
        return childi
                
```
</p>


### Array sort + Two pointer greedy solution O(nlogn)
- Author: amanchik
- Creation Date: Fri Nov 18 2016 10:40:42 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 08:31:07 GMT+0800 (Singapore Standard Time)

<p>
Two assign cookies to children optimaly we should give for each child the closest higher cookie. By using this greedy approach overall sum of wasted cookies will be minimum amoung all. To use this greedy solution in effective way we can sort both arrays and use two pointers. We should move pointer of children only if there is enough cookies to make that child content. In each step we will try to make content child at position pointerG by searching the closes higher cookie value.
```
public class Solution {
    public int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        
        int pointG = 0;
        int pointS = 0;
        
        while (pointG<g.length && pointS<s.length) {
            if (g[pointG]<=s[pointS]) {
                pointG++;
                pointS++;
            } else {
                pointS++;
            }
        }
        
        return pointG;
    }
}
```
</p>


