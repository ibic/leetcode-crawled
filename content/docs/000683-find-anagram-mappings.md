---
title: "Find Anagram Mappings"
weight: 683
#id: "find-anagram-mappings"
---
## Description
<div class="description">
<p>
Given two lists <code>A</code>and <code>B</code>, and <code>B</code> is an anagram of <code>A</code>. <code>B</code> is an anagram of <code>A</code> means <code>B</code> is made by randomizing the order of the elements in <code>A</code>.
</p><p>
We want to find an <i>index mapping</i> <code>P</code>, from <code>A</code> to <code>B</code>. A mapping <code>P[i] = j</code> means the <code>i</code>th element in <code>A</code> appears in <code>B</code> at index <code>j</code>.
</p><p>
These lists <code>A</code> and <code>B</code> may contain duplicates.  If there are multiple answers, output any of them.
</p>

<p>
For example, given
<pre>
A = [12, 28, 46, 32, 50]
B = [50, 12, 32, 46, 28]
</pre>
</p>
We should return
<pre>
[1, 4, 3, 2, 0]
</pre>
as <code>P[0] = 1</code> because the <code>0</code>th element of <code>A</code> appears at <code>B[1]</code>,
and <code>P[1] = 4</code> because the <code>1</code>st element of <code>A</code> appears at <code>B[4]</code>,
and so on.
</p>

<p><b>Note:</b><ol>
<li><code>A, B</code> have equal lengths in range <code>[1, 100]</code>.</li>
<li><code>A[i], B[i]</code> are integers in range <code>[0, 10^5]</code>.</li>
</ol></p>
</div>

## Tags
- Hash Table (hash-table)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Hash Table [Accepted]

**Intuition**

Take the example `A = [12, 28, 46]`, `B = [46, 12, 28]`.  We want to know where the `12` occurs in `B`, say at position `1`; then where the `28` occurs in `B`, which is position `2`; then where the `46` occurs in `B`, which is position `0`.

If we had a dictionary (hash table) `D = {46: 0, 12: 1, 28: 2}`, then this question could be handled easily.

**Algorithm**

Create the hash table `D` as described above.  Then, the answer is a list of `D[A[i]]` for `i = 0, 1, ...`.

<iframe src="https://leetcode.com/playground/6uKzPtyG/shared" frameBorder="0" width="100%" height="276" name="6uKzPtyG"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of $$A$$.

* Space Complexity: $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution Using HashMap - Leetcode test case missing??
- Author: diddit
- Creation Date: Sun Jan 07 2018 12:20:48 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 20 2018 16:38:27 GMT+0800 (Singapore Standard Time)

<p>
Other solution does not handle duplicates. 
Like [12,12] and [12,12]. (This test case missing?) Description says duplicates are possible
So we should use ArrayList in the HashMap. 

To clarify my confusion again. 
if A[10,20,10], B[10,10,20], here multiple answer is [1,2,0] or [0,2,1].
I don't think we should do like [1,2,1] or [0,2,0]. But some people think is okay. But i think of this "B is formed by randomizing the A", which means all indices has to be used?

Let me know what you think? 

    public int[] anagramMappings(int[] A, int[] B) {
        int[] result = new int [A.length];
        Map<Integer, List<Integer>> map = new HashMap<>();
        for(int i = 0; i < B.length; i++) {
            map.computeIfAbsent(B[i], k -> new ArrayList<>()).add(i);
        }
        for(int i = 0; i < A.length; i++) {
            result[i] = map.get(A[i]).remove(map.get(A[i]).size()-1);
        }
        return result;
    }
</p>


### Easy and Concise Python Solution
- Author: lee215
- Creation Date: Sun Jan 07 2018 12:56:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 07 2018 12:56:01 GMT+0800 (Singapore Standard Time)

<p>
O(N) solution using hashmap:
```
def anagramMappings(self, A, B):
        d = {}
        for i, b in enumerate(B):
            if b not in d:
                d[b] = []
            d[b].append(i)
        return [d[a].pop() for a in A]
```
2 lines accepted solution, but it can't return all index in case of duplicates.
```
def anagramMappings(self, A, B):
        d = {b:i for i,b in enumerate(B)}
        return [d[a] for a in A]
```
O(N^2) solution in 1 line:
```
def anagramMappings(self, A, B):
        return [B.index(a) for a in A]
```
</p>


### [JAVA] use sorting - 5ms
- Author: tyuan73
- Creation Date: Sun Jan 07 2018 12:59:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 01:26:03 GMT+0800 (Singapore Standard Time)

<p>
    public int[] anagramMappings(int[] A, int[] B) {
        int n = A.length;
        for(int i = 0; i < n; i++) {
            A[i] = (A[i] << 8) + i;
            B[i] = (B[i] << 8) + i;
        }
        Arrays.sort(A);
        Arrays.sort(B);
        int[] ans = new int[n];
        for(int i = 0; i < n; i++)
            ans[A[i] & 0xFF] = B[i] & 0xFF;
        return ans;
    }
</p>


