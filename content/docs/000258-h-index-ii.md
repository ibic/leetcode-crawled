---
title: "H-Index II"
weight: 258
#id: "h-index-ii"
---
## Description
<div class="description">
<p>Given an array of citations <strong>sorted&nbsp;in ascending order </strong>(each citation is a non-negative integer) of a researcher, write a function to compute the researcher&#39;s h-index.</p>

<p>According to the&nbsp;<a href="https://en.wikipedia.org/wiki/H-index" target="_blank">definition of h-index on Wikipedia</a>: &quot;A scientist has index&nbsp;<i>h</i>&nbsp;if&nbsp;<i>h</i>&nbsp;of his/her&nbsp;<i>N</i>&nbsp;papers have&nbsp;<b>at least</b>&nbsp;<i>h</i>&nbsp;citations each, and the other&nbsp;<i>N &minus; h</i>&nbsp;papers have&nbsp;<b>no more than</b>&nbsp;<i>h&nbsp;</i>citations each.&quot;</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b> <code>citations = [0,1,3,5,6]</code>
<b>Output:</b> 3 
<strong>Explanation: </strong><code>[0,1,3,5,6] </code>means the researcher has <code>5</code> papers in total and each of them had 
             received 0<code>, 1, 3, 5, 6</code> citations respectively. 
&nbsp;            Since the researcher has <code>3</code> papers with <b>at least</b> <code>3</code> citations each and the remaining 
&nbsp;            two with <b>no more than</b> <code>3</code> citations each, her h-index is <code>3</code>.</pre>

<p><strong>Note:</strong></p>

<p>If there are several possible values for&nbsp;<em>h</em>, the maximum one is taken as the h-index.</p>

<p><strong>Follow up:</strong></p>

<ul>
	<li>This is a follow up problem to&nbsp;<a href="/problems/h-index/description/">H-Index</a>, where <code>citations</code> is now guaranteed to be sorted in ascending order.</li>
	<li>Could you solve it in logarithmic time complexity?</li>
</ul>

</div>

## Tags
- Binary Search (binary-search)

## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Linear search, O(k) time

**Intuition**

Thanks to the fact that the list of citation numbers is sorted in the ascending order,
one could solve the problem in a single pass of iteration.

Let's consider an article whose citation number `c` is index at `i`,
_i.e_ `c = citations[i]`. We would know that the number of articles 
whose citation number is higher than `c` would be `n - i - 1`.  
And together with the current article, there are `n - i` articles that are cited 
at least `c` times.

Given the definition of H-Index, we just need to find the first article at `i` 
whose citation number `c = citation[i]` is greater or equal to `n - i `,
_i.e._ `c >= n - i`.
As we know that all the articles following `i` would be cited at least `c` times,
so in total _there are `n - i` articles that are cited at least `c` times_.
As a result, according to the definition, the H-Index of the author should be `n - i`.

![pic](../Figures/275/dia.png)

Following the above intuition, it is straightforward to implement the algorithm.
We give some examples in the following.

**Implementation**

<iframe src="https://leetcode.com/playground/uLFEWQPr/shared" frameBorder="0" width="100%" height="242" name="uLFEWQPr"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ where N is the length of the input list,
since in the worse case we would iterate the entire list.

* Space complexity : $$\mathcal{O}(1)$$, it's a constant space solution.
<br /> 
<br />


---
#### Approach 2: Binary Search, O(log N) time

**Intuition**

Following in the intuition we elaborated in the Approach 1,
the problem is actually boiled down to the following task:

> Given a sorted list `citations` of size `n` in ascending order, 
one is asked to find the *first* number `citations[i]`
which meets the constraint of `citations[i] >= n - i`. 

With the above formulation of the problem, 
it becomes clear that one could apply the *binary search* 
algorithm to solve the problem.
In binary search algorithm, we recursively 
reduce the searching scope into half, 
which leads to a more optimal $$\mathcal{O}(\log N)$$ 
time complexity comparing to the $$\mathcal{O}(N)$$ of the linear search.

![pic](../Figures/275/binary.png)

**Algorithm**

First we pick a pivot element that is in the middle of the list, 
_i.e._ `citations[pivot]`,
which would divide the original list into two sublists:
`citations[0 : pivot - 1]` and `citations[pivot + 1 : n]`.

Then comparing between the values of `n - pivot` 
and `citations[pivot]` element,
our binary search algorithm breaks down to the following 3 cases:

- `citations[pivot] == n - pivot`: We found the desired element !

- `citations[pivot] < n - pivot`: 
Since the desired element should be greater or equal to `n - pivot`,
we then further look into the sublist on the right hand side, 
_i.e._ `citations[pivot + 1 : n]`.

- `citations[pivot] > n - pivot`: 
We should look into the sublist on the left hand side, 
_i.e._ `citations[0 : pivot-1]`.

A minor difference to the typical binary search algorithm, 
is that in this case we return the value of `n - pivot` as the result, 
rather than the value of the desired element.

**Implementation**

<iframe src="https://leetcode.com/playground/iUDZukg8/shared" frameBorder="0" width="100%" height="361" name="iUDZukg8"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(\log N)$$ since we apply binary search
algorithm here. 

* Space complexity : $$\mathcal{O}(1)$$, it's a constant space solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Standard binary search
- Author: LeJas
- Creation Date: Fri Sep 04 2015 22:10:36 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 23:14:34 GMT+0800 (Singapore Standard Time)

<p>
Just binary search, each time check citations[mid]
case 1: citations[mid] == len-mid, then it means there are citations[mid] papers that have at least citations[mid] citations.
case 2: citations[mid] > len-mid, then it means there are citations[mid] papers that have moret than citations[mid] citations, so we should continue searching in the left half
case 3:  citations[mid] < len-mid, we should continue searching in the right side
After iteration, it is guaranteed that right+1 is the one we need to find (i.e. len-(right+1) papars have at least len-(righ+1) citations)


    class Solution {
    public:
        int hIndex(vector<int>& citations) {
            int left=0, len = citations.size(), right= len-1,  mid;
            while(left<=right)
            {
                mid=(left+right)>>1;
                if(citations[mid]== (len-mid)) return citations[mid];
                else if(citations[mid] > (len-mid)) right = mid - 1;
                else left = mid + 1;
            }
            return len - (right+1);
        }
    };

or simplified version

    class Solution {
    public:
        int hIndex(vector<int>& citations) {
            int left=0, len = citations.size(), right= len-1,  mid;
            while(left<=right)
            {
                mid=left+ (right-left)/2;
                if(citations[mid] >= (len-mid)) right = mid - 1;
                else left = mid + 1;
            }
            return len - left;
        }
    };
</p>


### Java binary search, simple and clean
- Author: jinwu
- Creation Date: Sat Sep 05 2015 04:31:05 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 13 2018 09:47:23 GMT+0800 (Singapore Standard Time)

<p>
The idea is to search for the first index from the sorted array so that :
<br> <i>citations[index] >= length(citations) - index.</I> <br>
And return (length - index) as the result.
Here is the code:

    public int hIndex(int[] citations) {
		int len = citations.length;
		int lo = 0, hi = len - 1;
		while (lo <= hi) {
			int med = (hi + lo) / 2;
			if (citations[med] == len - med) {
				return len - med;
			} else if (citations[med] < len - med) {
				lo = med + 1;
			} else { 
				//(citations[med] > len-med), med qualified as a hIndex,
			    // but we have to continue to search for a higher one.
				hi = med - 1;
			}
		}
		return len - lo;
	}
</p>


### It takes me 20 mins to understand the description
- Author: qjhdtc001
- Creation Date: Thu Jul 12 2018 06:26:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 11 2018 12:45:27 GMT+0800 (Singapore Standard Time)

<p>
apeuiofadfhfaoefwapfiejfoijfopdjfjopj
</p>


