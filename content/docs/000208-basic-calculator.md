---
title: "Basic Calculator"
weight: 208
#id: "basic-calculator"
---
## Description
<div class="description">
<p>Implement a basic calculator to evaluate a simple expression string.</p>

<p>The expression string may contain open <code>(</code> and closing parentheses <code>)</code>, the plus <code>+</code> or minus sign <code>-</code>, <b>non-negative</b> integers and empty spaces <code> </code>.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> &quot;1 + 1&quot;
<strong>Output:</strong> 2
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> &quot; 2-1 + 2 &quot;
<strong>Output:</strong> 3</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> &quot;(1+(4+5+2)-3)+(6+8)&quot;
<strong>Output:</strong> 23</pre>
<b>Note:</b>

<ul>
	<li>You may assume that the given expression is always valid.</li>
	<li><b>Do not</b> use the <code>eval</code> built-in library function.</li>
</ul>

</div>

## Tags
- Math (math)
- Stack (stack)

## Companies
- Amazon - 11 (taggedByAdmin: false)
- Roblox - 9 (taggedByAdmin: false)
- Indeed - 6 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: true)
- Facebook - 5 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Karat - 4 (taggedByAdmin: false)
- Robinhood - 2 (taggedByAdmin: false)
- Wayfair - 2 (taggedByAdmin: false)
- Snapchat - 3 (taggedByAdmin: false)
- Uber - 11 (taggedByAdmin: false)
- Intuit - 5 (taggedByAdmin: false)
- Pinterest - 4 (taggedByAdmin: false)
- Paypal - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

This problem is all about understanding the following:

* Input always contains valid strings
* The rules of addition and subtraction
* Implication of precedence by parenthesis
* Spaces do not affect the evaluation of the input expression

<center>
<img src="../Figures/224/Basic_Calculator_0.png" width="600"/>
</center>

#### Approach 1: Stack and String Reversal

**Intuition**

This question qualifies really well for a stack question. Since the expression might have parenthesis, we can use a stack to find the value for each sub-expression within a parenthesis. Essentially, we need to *delay* processing the main expression until we are done evaluating the interim sub-expressions within parenthesis and to introduce this delay, we use a stack.

We push the elements of the expression one by one onto the stack until we get a closing bracket `)`. Then we pop the elements from the stack one by one and evaluate the expression on-the-go. This is done till we find the corresponding `(` opening bracket. This kind of evaluation is very common when using the stack data structure. However, if you notice the way we calculate the final answer, you will realize that we actually process the values from right to left whereas it should be the other way around.

<center>
<img src="../Figures/224/Basic_Calculator_1.png" width="600"/>
</center>

From the above example we realize that following the simple stack push and pop methodology will not help us here. We need to understand how `+` and `-` work. `+` follows the associative property. For the expression $$A+B+C$$,  we have $$(A+B)+C = A+(B+C)$$. However,  `-` does not follow this rule which is the root cause of all the problems in this approach.

If we use a stack and read the elements of the expression from left to right, we end up evaluating the expression from right-to-left. This means we are expecting $$(A-B)-C$$ to be equal to $$(C-B)-A$$ which is not correct. Subtraction is neither associative nor commutative.

This problem could be solved very easily by reversing the string and then using basic drill using a stack. Reversing a string helps since we now put the elements of the expression into the stack from right to left and evaluation for the expression is done correctly from left to right.

<center>
<img src="../Figures/224/Basic_Calculator_2.png" width="600"/>
</center>

**Algorithm**

1. Iterate the expression string in reverse order one character at a time. Since we are reading the expression character by character, we need to be careful when we are reading digits and non-digits.
2. The operands could be formed by multiple characters. A string "123" would mean a numeric 123, which could be formed as: `123` >> `120 + 3` >> `100 + 20 + 3`. Thus, if the character read is a digit we need to form the operand by multiplying a power of `10` to the current digit and adding it to the overall operand. We do this since we are processing the string in the reverse order.
3. The operands could be formed by multiple characters. We need to keep track of an on-going operand. This part is a bit tricky since in this case the string is reversed. Once we encounter a character which is not a digit, we push the operand onto the stack.
4. When we encounter an opening parenthesis `(`, this means an expression just ended. Recall we have reversed the expression. So an opening bracket would signify the end of the an expression. This calls for evaluation of the expression by popping operands and operators off the stack till we pop corresponding closing parenthesis. The final result of the expression is pushed back onto the stack.

    >Note: We are evaluating all the sub-expressions within the main expression. The sub-expressions on the right get evaluated first but the main expression itself is evaluated from left to right when all its components are resolved, which is very important for correct results.

    For eg. For expression $$A - (B+C) + (D+E-F)$$, $$D+E-F$$ is evaluated before $$B+C$$. While evaluating $$D+E-F$$ the order is from left to right. Similarly for the parent expression, all the child components are evaluated and stored on the stack so that final evaluation is left to right.

5. Push the other non-digits onto to the stack.
6. Do this until we get the final result. It's possible that we don't have any more characters left to process but the stack is still non-empty. This would happen when the main expression is not enclosed by parenthesis. So, once we are done evaluating the entire expression, we check if the stack is non-empty. If it is, we treat the elements in it as one final expression and evaluate it the same way we would if we had encountered an opening bracket.

    We can also cover the original expression with a set of parenthesis to avoid this extra call.

<iframe src="https://leetcode.com/playground/VzyUtLtT/shared" frameBorder="0" width="100%" height="500" name="VzyUtLtT"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where N is the length of the string.

* Space Complexity: $$O(N)$$, where N is the length of the string.
<br/>
<br/>

---

#### Approach 2: Stack and No String Reversal

**Intuition**

A very easy way to solve the problem of associativity for `-` we tackled in the previous approach, is to use `-` operator as the magnitude for the operand to the right of the operator. Once we start using `-` as a magnitude for the operands, we just have one operator left which is addition and `+` is associative.

for e.g. $$A - B - C$$ could be re-written as $$A + (-B) + (-C)$$.

> The re-written expression would follow associativity rule. Thus evaluating the expression from right or left, won't change the result.

What we need to keep in mind is that the expressions given would be complicated, i.e. there would be expressions nested within other expressions. Even if we have something like `(A - (B - C))` we need to associate the `negative sign` outside of `B-C` with the result of `B-C` instead of just with `B`.

We can solve this problem by following the basic drill before and associating the sign with the expression to the right of it. However, the approach that we will instead take has a small twist to it in that we will be evaluating most of the expression on-the-go. This reduces the number of push and pop operations.

<center>
<img src="../Figures/224/Basic_Calculator_3.png" width="600"/>
</center>

Follow the below steps closely. This algorithm is inspired from [discussion post](https://leetcode.com/problems/basic-calculator/discuss/62361/southpenguin) by [southpenguin](https://leetcode.com/southpenguin/).

**Algorithm**

1. Iterate the expression string one character at a time. Since we are reading the expression character by character, we need to be careful when we are reading digits and non-digits.
2. The operands could be formed by multiple characters. A string "123" would mean a numeric 123, which could be formed as: `123` >> `120 + 3` >> `100 + 20 + 3`. Thus, if the character read is a digit we need to form the operand by multiplying `10` to the previously formed continuing operand and adding the digit to it.
3. Whenever we encounter an operator such as `+` or `-` we first evaluate the expression to the left and then save this `sign` for the next evaluation.
    <center>
    <img src="../Figures/224/Basic_Calculator_4.png" width="600"/>
    </center>
4. If the character is an opening parenthesis `(`, we just push the result calculated so far and the `sign` on to the stack (the sign and the magnitude) and start a fresh as if we are calculating a new expression.
5. If the character is a closing parenthesis `)`, we first calculate the expression to the left. The result from this would be the result of the expression within the set of parenthesis that just concluded. This result is then multiplied with the sign, if there is any on top of the stack.
Remember we saved the `sign` on top of the stack when we had encountered an open parenthesis? This sign is associated with the parenthesis that started then, thus when the expression ends or concludes, we pop the `sign` and multiply it with result of the expression. It is then just added to the next element on top of the stack.

<iframe src="https://leetcode.com/playground/iXCu2REA/shared" frameBorder="0" width="100%" height="500" name="iXCu2REA"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where N is the length of the string. The difference in time complexity between this approach and the previous one is that every character in this approach will get processed exactly once. However, in the previous approach, each character can potentially get processed twice, once when it's pushed onto the stack and once when it's popped for processing of the final result (or a subexpression). That's why this approach is faster.

* Space Complexity: $$O(N)$$, where N is the length of the string.
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Iterative Java solution with stack
- Author: southpenguin
- Creation Date: Wed Jun 10 2015 05:09:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 00:20:45 GMT+0800 (Singapore Standard Time)

<p>
Simple iterative solution by identifying characters one by one. One important thing is that the input is valid, which means the parentheses are always paired and in order.
Only 5 possible input we need to pay attention:

 1. digit: it should be one digit from the current number
 2. '+': number is over, we can add the previous number and start a new number
 3. '-': same as above
 4. '(': push the previous result and the sign into the stack, set result to 0, just calculate the new result within the parenthesis.
 5. ')': pop out the top two numbers from stack, first one is the sign before this pair of parenthesis, second is the temporary result before this pair of parenthesis. We add them together.
 
 
Finally if there is only one number, from the above solution, we haven't add the number to the result, so we do a check see if the number is zero.

----------


    public int calculate(String s) {
        Stack<Integer> stack = new Stack<Integer>();
        int result = 0;
        int number = 0;
        int sign = 1;
        for(int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if(Character.isDigit(c)){
                number = 10 * number + (int)(c - '0');
            }else if(c == '+'){
                result += sign * number;
                number = 0;
                sign = 1;
            }else if(c == '-'){
                result += sign * number;
                number = 0;
                sign = -1;
            }else if(c == '('){
                //we push the result first, then sign;
                stack.push(result);
                stack.push(sign);
                //reset the sign and result for the value in the parenthesis
                sign = 1;   
                result = 0;
            }else if(c == ')'){
                result += sign * number;  
                number = 0;
                result *= stack.pop();    //stack.pop() is the sign before the parenthesis
                result += stack.pop();   //stack.pop() now is the result calculated before the parenthesis
                
            }
        }
        if(number != 0) result += sign * number;
        return result;
    }
</p>


### JAVA-----------Easy Version To Understand!!!!!
- Author: HelloWorld123456
- Creation Date: Sun Jan 03 2016 11:20:31 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 22:25:12 GMT+0800 (Singapore Standard Time)

<p>
    public static int calculate(String s) {
		int len = s.length(), sign = 1, result = 0;
		Stack<Integer> stack = new Stack<Integer>();
		for (int i = 0; i < len; i++) {
			if (Character.isDigit(s.charAt(i))) {
				int sum = s.charAt(i) - '0';
				while (i + 1 < len && Character.isDigit(s.charAt(i + 1))) {
					sum = sum * 10 + s.charAt(i + 1) - '0';
					i++;
				}
				result += sum * sign;
			} else if (s.charAt(i) == '+')
				sign = 1;
			else if (s.charAt(i) == '-')
				sign = -1;
			else if (s.charAt(i) == '(') {
				stack.push(result);
				stack.push(sign);
				result = 0;
				sign = 1;
			} else if (s.charAt(i) == ')') {
				result = result * stack.pop() + stack.pop();
			}

		}
		return result;
	}
</p>


### Easy 18 lines C++, 16 lines Python
- Author: StefanPochmann
- Creation Date: Wed Jun 10 2015 00:54:15 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 03:25:39 GMT+0800 (Singapore Standard Time)

<p>
Keep a global running total and a stack of signs (+1 or -1), one for each open scope. The "global" outermost sign is +1.

- Each number consumes a sign.
- Each `+` and `-` causes a new sign.
- Each `(` duplicates the current sign so it can be used for the first term inside the new scope. That's also why I start with `[1, 1]` - the global sign 1 and a duplicate to be used for the first term, since expressions start like `3...` or `(...`, not like `+3...` or `+(...`.
- Each `)` closes the current scope and thus drops the current sign.

Also see the example trace below my programs.

**C++:**

    int calculate(string s) {
        int total = 0;
        vector<int> signs(2, 1);
        for (int i=0; i<s.size(); i++) {
            char c = s[i];
            if (c >= '0') {
                int number = 0;
                while (i < s.size()  &&  s[i] >= '0')
                    number = 10 * number + s[i++] - '0';
                total += signs.back() * number;
                signs.pop_back();
                i--;
            }
            else if (c == ')')
                signs.pop_back();
            else if (c != ' ')
                signs.push_back(signs.back() * (c == '-' ? -1 : 1));
        }
        return total;
    }

**Python:**

    def calculate(self, s):
        total = 0
        i, signs = 0, [1, 1]
        while i < len(s):
            c = s[i]
            if c.isdigit():
                start = i
                while i < len(s) and s[i].isdigit():
                    i += 1
                total += signs.pop() * int(s[start:i])
                continue
            if c in '+-(':
                signs += signs[-1] * (1, -1)[c == '-'],
            elif c == ')':
                signs.pop()
            i += 1
        return total

**Example trace:**

Here's an example trace for input `3-(2+(9-4))`.

      remaining   sign stack      total
    3-(2+(9-4))   [1, 1]            0
     -(2+(9-4))   [1]               3
      (2+(9-4))   [1, -1]           3
       2+(9-4))   [1, -1, -1]       3
        +(9-4))   [1, -1]           1
         (9-4))   [1, -1, -1]       1
          9-4))   [1, -1, -1, -1]   1
           -4))   [1, -1, -1]      -8
            4))   [1, -1, -1, 1]   -8
             ))   [1, -1, -1]      -4
              )   [1, -1]          -4
                  [1]              -4

If you want to see traces for other examples, you can add this at the start inside the loop and after the loop (that's for the Python solution, where it's all easier):

    print '%11s   %-16s %2d' % (s[i:], signs, total)
</p>


