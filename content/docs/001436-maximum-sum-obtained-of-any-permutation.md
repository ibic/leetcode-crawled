---
title: "Maximum Sum Obtained of Any Permutation"
weight: 1436
#id: "maximum-sum-obtained-of-any-permutation"
---
## Description
<div class="description">
<p>We have an array of integers, <code>nums</code>, and an array of <code>requests</code> where <code>requests[i] = [start<sub>i</sub>, end<sub>i</sub>]</code>. The <code>i<sup>th</sup></code> request asks for the sum of <code>nums[start<sub>i</sub>] + nums[start<sub>i</sub> + 1] + ... + nums[end<sub>i</sub> - 1] + nums[end<sub>i</sub>]</code>. Both <code>start<sub>i</sub></code> and <code>end<sub>i</sub></code> are <em>0-indexed</em>.</p>

<p>Return <em>the maximum total sum of all requests <strong>among all permutations</strong> of</em> <code>nums</code>.</p>

<p>Since the answer may be too large, return it <strong>modulo</strong> <code>10<sup>9</sup> + 7</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4,5], requests = [[1,3],[0,1]]
<strong>Output:</strong> 19
<strong>Explanation:</strong> One permutation of nums is [2,1,3,4,5] with the following result: 
requests[0] -&gt; nums[1] + nums[2] + nums[3] = 1 + 3 + 4 = 8
requests[1] -&gt; nums[0] + nums[1] = 2 + 1 = 3
Total sum: 8 + 3 = 11.
A permutation with a higher total sum is [3,5,4,2,1] with the following result:
requests[0] -&gt; nums[1] + nums[2] + nums[3] = 5 + 4 + 2 = 11
requests[1] -&gt; nums[0] + nums[1] = 3 + 5  = 8
Total sum: 11 + 8 = 19, which is the best that you can do.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4,5,6], requests = [[0,1]]
<strong>Output:</strong> 11
<strong>Explanation:</strong> A permutation with the max total sum is [6,5,4,3,2,1] with request sums [11].</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4,5,10], requests = [[0,2],[1,3],[1,1]]
<strong>Output:</strong> 47
<strong>Explanation:</strong> A permutation with the max total sum is [4,10,5,3,2,1] with request sums [19,18,10].</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == nums.length</code></li>
	<li><code>1 &lt;= n &lt;= 10<sup>5</sup></code></li>
	<li><code>0 &lt;= nums[i]&nbsp;&lt;= 10<sup>5</sup></code></li>
	<li><code>1 &lt;= requests.length &lt;=&nbsp;10<sup>5</sup></code></li>
	<li><code>requests[i].length == 2</code></li>
	<li><code>0 &lt;= start<sub>i</sub>&nbsp;&lt;= end<sub>i</sub>&nbsp;&lt;&nbsp;n</code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Paypal - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Sweep Line
- Author: lee215
- Creation Date: Sun Sep 20 2020 00:07:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 27 2020 16:52:05 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
We want to calculate the frequency for `A[i]`.
Assign the big element `A[i]` to the position queried more frequently.
<br>

# **Explanation**
For each request `[i,j]`,
we set `count[i]++` and `count[j + 1]--`,

Then we sweep once the whole `count`,
we can find the frequency for `count[i]`.

Note that for all intervals inputs,
this method should be the first intuition you come up with.
<br>

# **Complexity**
Time `O(NlogN)` for sorting
Space `O(N)`
<br>

**Java:**
```java
    public int maxSumRangeQuery(int[] A, int[][] req) {
        long res = 0, mod = (long)1e9 + 7;
        int n = A.length, count[] = new int[n];
        for (int[] r: req) {
            count[r[0]] += 1;
            if (r[1] + 1 < n)
                count[r[1] + 1] -= 1;
        }
        for (int i = 1; i < n; ++i)
            count[i] += count[i - 1];
        Arrays.sort(A);
        Arrays.sort(count);
        for (int i = 0; i < n; ++i)
            res += (long)A[i] * count[i];
        return (int)(res % mod);
    }
```

**C++:**
```cpp
    int maxSumRangeQuery(vector<int>& A, vector<vector<int>>& req) {
        long res = 0, mod = 1e9 + 7, n = A.size();
        vector<int> count(n);
        for (auto &r: req) {
            count[r[0]] += 1;
            if (r[1] + 1 < n)
                count[r[1] + 1] -= 1;
        }
        for (int i = 1; i < n; ++i)
            count[i] += count[i - 1];
        sort(begin(count), end(count));
        sort(begin(A), end(A));
        for (int i = 0; i < n; ++i)
            res += (long)A[i] * count[i];
        return res % mod;
    }
```

**Python:**
```py
    def maxSumRangeQuery(self, A, req):
        n = len(A)
        count = [0] * (n + 1)
        for i, j in req:
            count[i] += 1
            count[j + 1] -= 1
        for i in xrange(1, n + 1):
            count[i] += count[i - 1]
        res = 0
        for v, c in zip(sorted(count[:-1]), sorted(A)):
            res += v * c
        return res % (10**9 + 7)
```
<br>

# More Sweep Line Problems
Here are some sweep line problem that I solved.
I\'ll make a summary for this method.
Let me know other related problems or my other related posts.
Good luck and have fun.

- [253. Meeting Rooms II](https://leetcode.com/problems/meeting-rooms-ii/)
- [1094. Car Pooling](https://leetcode.com/problems/car-pooling/discuss/317610/JavaC++Python-Meeting-Rooms-III) 
- [1109. Corporate Flight Bookings](https://leetcode.com/problems/corporate-flight-bookings/discuss/328856/JavaC%2B%2BPython-Straight-Forward-Solution)
<br>
</p>


### C++/Java O(n log n)
- Author: votrubac
- Creation Date: Sun Sep 20 2020 00:01:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 27 2020 03:34:08 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
We should put the largest number into the most queried position.

#### Solution
We can count how many times each position was queried using another array `cnt`. To do so in O(n), we mark the start and end of each query with `+ 1` and `- 1`, and then calculate counts for each position in one swipe.

> You can see the picture that demonstrates this here: [1109. Corporate Flight Bookings](https://leetcode.com/problems/corporate-flight-bookings/discuss/328871/C%2B%2BJava-with-picture-O(n)).

Then, we can sort the input array and the count array, so that larger numbers and higher count are in the matching positions. Finally, we compute the maximum sum in one last swipe.

**C++**
```cpp
int maxSumRangeQuery(vector<int>& nums, vector<vector<int>>& requests) {
    vector<int> cnt(nums.size());
    for (auto &r : requests) {
        cnt[r[0]] += 1;
        if (r[1] + 1 < nums.size())
            cnt[r[1] + 1] -= 1;
    }
    for (auto i = 1; i < cnt.size(); ++i)
        cnt[i] += cnt[i - 1];
    sort(begin(nums), end(nums));
    sort(begin(cnt), end(cnt));
    long res = 0;
    for (auto i = 0; i < nums.size(); ++i)
        res = (res + (long)nums[i] * cnt[i]) % 1000000007;
    return res;
}
```
**Java**
```java
public int maxSumRangeQuery(int[] nums, int[][] requests) {
    int cnt[] = new int[nums.length];
    for (var r : requests) {
        cnt[r[0]] += 1;
        if (r[1] + 1 < nums.length)
            cnt[r[1] + 1] -= 1;
    }
    for (int i = 1; i < cnt.length; ++i)
        cnt[i] += cnt[i - 1];
    Arrays.sort(nums);
    Arrays.sort(cnt);
    long res = 0;
    for (int i = 0; i < nums.length; ++i)
        res = (res + (long)nums[i] * cnt[i]) % 1000000007;
    return (int)res;
}
```
**Complexity Analysis**
- Time: O(n log n)
- Memory: O(n) for storing counts.

</p>


### [Java] Count Frequencies of indexes and then sort with Explanation
- Author: manrajsingh007
- Creation Date: Sun Sep 20 2020 00:01:33 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 21 2020 14:28:45 GMT+0800 (Singapore Standard Time)

<p>
Iterate through the requests array. 
Take an aux array of size equal to the size of the array arr.
For each request {st, end}, mark aux[st] = aux[st] + 1 and aux[end + 1] = aux[end + 1] - 1.

Now when you iterate over this array aux, keep a running sum. At curr index i, the running sum will tell you as to how many times that current index i would have been visited.

Now you have frequencies for all the indexes. Sort them.

Now apply greedy approach, it is pretty obvious that you would like to keep the maximum element in the array at the index where you had the maximum frequency. 

Sort arr.
Sort aux.
Just iterate over aux and array and sum the products of the frequencies in sorted aux and elements in sorted arr.


Hapy Coding !!


```
class Solution {
    public int maxSumRangeQuery(int[] nums, int[][] requests) {
        int n = nums.length;
        int[] aux = new int[n];
        for(int i = 0; i < requests.length; i++) {
            int[] curr = requests[i];
            int st = curr[0], end = curr[1];
            aux[st]++;
            if(end != n - 1) aux[end + 1]--;
        }
        int sum = 0;
        for(int i = 0; i < aux.length; i++) {
            sum += aux[i];
            aux[i] = sum;
        }
        Arrays.sort(nums);
        Arrays.sort(aux);
        long ans = 0;
        int mod = (int)Math.pow(10, 9) + 7;
        for(int i = 0; i < n; i++) {
            long temp = 1;
            ans  = (ans + (((temp * aux[i]) % mod) * nums[i]) % mod) % mod;
        }
        return (int)ans;
    }
}
</p>


