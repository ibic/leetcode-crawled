---
title: "Longest Palindromic Substring"
weight: 5
#id: "longest-palindromic-substring"
---
## Description
<div class="description">
<p>Given a string <code>s</code>, return&nbsp;<em>the longest palindromic substring</em> in <code>s</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;babad&quot;
<strong>Output:</strong> &quot;bab&quot;
<strong>Note:</strong> &quot;aba&quot; is also a valid answer.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;cbbd&quot;
<strong>Output:</strong> &quot;bb&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;a&quot;
<strong>Output:</strong> &quot;a&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;ac&quot;
<strong>Output:</strong> &quot;a&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 1000</code></li>
	<li><code>s</code> consist of only digits and English letters (lower-case and/or upper-case),</li>
</ul>

</div>

## Tags
- String (string)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 43 (taggedByAdmin: true)
- Microsoft - 12 (taggedByAdmin: true)
- Apple - 9 (taggedByAdmin: false)
- Facebook - 8 (taggedByAdmin: false)
- Google - 8 (taggedByAdmin: false)
- Bloomberg - 7 (taggedByAdmin: true)
- Goldman Sachs - 7 (taggedByAdmin: false)
- Cisco - 4 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Nutanix - 3 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Morgan Stanley - 2 (taggedByAdmin: false)
- Oracle - 6 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Huawei - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- Samsung - 2 (taggedByAdmin: false)
- Yahoo - 9 (taggedByAdmin: false)
- Wayfair - 6 (taggedByAdmin: false)
- Airbnb - 4 (taggedByAdmin: false)
- SAP - 4 (taggedByAdmin: false)
- Roblox - 4 (taggedByAdmin: false)
- Pure Storage - 3 (taggedByAdmin: false)
- GoDaddy - 3 (taggedByAdmin: false)
- Tencent - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- JPMorgan - 2 (taggedByAdmin: false)
- AppDynamics - 2 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)
- Robinhood - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary
This article is for intermediate readers. It introduces the following ideas:
Palindrome, Dynamic Programming and String Manipulation. Make sure you understand what a palindrome means. A palindrome is a string which reads the same in both directions. For example, $$S$$ = "aba" is a palindrome, $$S$$ = "abc" is not.

## Solution
---
#### Approach 1: Longest Common Substring

**Common mistake**

Some people will be tempted to come up with a quick solution, which is unfortunately flawed (however can be corrected easily):

> Reverse $$S$$ and become $$S'$$. Find the longest common substring between $$S$$ and $$S'$$, which must also be the longest palindromic substring.

This seemed to work, let’s see some examples below.

For example, $$S$$ = "caba", $$S'$$ = "abac".

The longest common substring between $$S$$ and $$S'$$ is "aba", which is the answer.

Let’s try another example: $$S$$ = "abacdfgdcaba", $$S'$$ = "abacdgfdcaba".

The longest common substring between $$S$$ and $$S'$$ is "abacd". Clearly, this is not a valid palindrome.

**Algorithm**

We could see that the longest common substring method fails when there exists a reversed copy of a non-palindromic substring in some other part of $$S$$. To rectify this, each time we find a longest common substring candidate, we check if the substring’s indices are the same as the reversed substring’s original indices. If it is, then we attempt to update the longest palindrome found so far; if not, we skip this and find the next candidate.

This gives us an $$O(n^2)$$ Dynamic Programming solution which uses $$O(n^2)$$ space (could be improved to use $$O(n)$$ space). Please read more about Longest Common Substring [here](http://en.wikipedia.org/wiki/Longest_common_substring).
<br />
<br />

---
#### Approach 2: Brute Force

The obvious brute force solution is to pick all possible starting and ending positions for a substring, and verify if it is a palindrome.

**Complexity Analysis**

* Time complexity : $$O(n^3)$$.
Assume that $$n$$ is the length of the input string, there are a total of $$\binom{n}{2} = \frac{n(n-1)}{2}$$ such substrings (excluding the trivial solution where a character itself is a palindrome). Since verifying each substring takes $$O(n)$$ time, the run time complexity is $$O(n^3)$$.

* Space complexity : $$O(1)$$.
<br />
<br />

---
#### Approach 3: Dynamic Programming

To improve over the brute force solution, we first observe how we can avoid unnecessary re-computation while validating palindromes. Consider the case "ababa". If we already knew that "bab" is a palindrome, it is obvious that "ababa" must be a palindrome since the two left and right end letters are the same.

We define $$P(i,j)$$ as following:

$$
P(i,j) =
     \begin{cases}
       \text{true,} &\quad\text{if the substring } S_i \dots S_j \text{ is a palindrome}\\
       \text{false,} &\quad\text{otherwise.} \
     \end{cases}
$$

Therefore,

$$
P(i, j) = ( P(i+1, j-1) \text{ and } S_i == S_j )
$$

The base cases are:

$$
P(i, i) = true
$$

$$
P(i, i+1) = ( S_i == S_{i+1} )
$$

This yields a straight forward DP solution, which we first initialize the one and two letters palindromes, and work our way up finding all three letters palindromes, and so on...


**Complexity Analysis**

* Time complexity : $$O(n^2)$$.
This gives us a runtime complexity of $$O(n^2)$$.

* Space complexity : $$O(n^2)$$.
It uses $$O(n^2)$$ space to store the table.

**Additional Exercise**

Could you improve the above space complexity further and how?
<br />
<br />

---
#### Approach 4: Expand Around Center

In fact, we could solve it in $$O(n^2)$$ time using only constant space.

We observe that a palindrome mirrors around its center. Therefore, a palindrome can be expanded from its center, and there are only $$2n - 1$$ such centers.

You might be asking why there are $$2n - 1$$ but not $$n$$ centers? The reason is the center of a palindrome can be in between two letters. Such palindromes have even number of letters (such as "abba") and its center are between the two 'b's.

<iframe src="https://leetcode.com/playground/D5gn4CJn/shared" frameBorder="0" width="100%" height="446" name="D5gn4CJn"></iframe>


**Complexity Analysis**

* Time complexity : $$O(n^2)$$.
Since expanding a palindrome around its center could take $$O(n)$$ time, the overall complexity is $$O(n^2)$$.

* Space complexity : $$O(1)$$.
<br />
<br />
---

#### Approach 5: Manacher's Algorithm

There is even an $$O(n)$$ algorithm called Manacher's algorithm, explained [here in detail](https://en.wikipedia.org/wiki/Longest_palindromic_substring#Manacher's_algorithm).
However, it is a non-trivial algorithm, and no one expects you to come up with this algorithm in a 45 minutes coding session. But, please go ahead and understand it, I promise it will be a lot of fun.

## Accepted Submission (java)
```java
class Solution {
    private boolean isPali(String s, int start, int stop) {
        while (start < stop) {
            if(s.charAt(start) != s.charAt(stop)) {
                return false;
            }
            start++;
            stop--;
        }
        return true;
    }
    public String longestPalindrome(String s) {
        int len = s.length();
        if (len == 0) {
            return s;
        }
        int longestPaliStart = 0;
        int longestPaliLength = 0;
        for (int i = 0; i < len; i++) {
            for (int j = 0; j <= i - longestPaliLength; j++) {
                if (isPali(s, j, i)) {
                    longestPaliStart = j;
                    longestPaliLength = 1 + i -j;
                    break;
                }
            } 
        }
        return s.substring(longestPaliStart, longestPaliStart + longestPaliLength);
    }
}
```

## Top Discussions
### Very simple clean java solution
- Author: jinwu
- Creation Date: Sun Sep 06 2015 09:46:36 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 05:17:19 GMT+0800 (Singapore Standard Time)

<p>
The performance is pretty good, surprisingly.

    public class Solution {
	private int lo, maxLen;
	
    public String longestPalindrome(String s) {
    	int len = s.length();
    	if (len < 2)
    		return s;
    	
        for (int i = 0; i < len-1; i++) {
         	extendPalindrome(s, i, i);  //assume odd length, try to extend Palindrome as possible
         	extendPalindrome(s, i, i+1); //assume even length.
        }
        return s.substring(lo, lo + maxLen);
    }

	private void extendPalindrome(String s, int j, int k) {
		while (j >= 0 && k < s.length() && s.charAt(j) == s.charAt(k)) {
			j--;
			k++;
		}
		if (maxLen < k - j - 1) {
			lo = j + 1;
			maxLen = k - j - 1;
		}
	}}
</p>


### Share my Java solution using dynamic programming
- Author: jeantimex
- Creation Date: Sun Sep 27 2015 09:06:12 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 08:21:10 GMT+0800 (Singapore Standard Time)

<p>
`dp(i, j)` represents whether `s(i ... j)` can form a palindromic substring, `dp(i, j)` is true when `s(i)` equals to `s(j)` and `s(i+1 ... j-1)` is a palindromic substring. When we found a palindrome, check if it's the longest one. Time complexity O(n^2).

    public String longestPalindrome(String s) {
      int n = s.length();
      String res = null;
        
      boolean[][] dp = new boolean[n][n];
        
      for (int i = n - 1; i >= 0; i--) {
        for (int j = i; j < n; j++) {
          dp[i][j] = s.charAt(i) == s.charAt(j) && (j - i < 3 || dp[i + 1][j - 1]);
                
          if (dp[i][j] && (res == null || j - i + 1 > res.length())) {
            res = s.substring(i, j + 1);
          }
        }
      }
        
      return res;
    }
</p>


### Python easy to understand solution with comments (from middle to two ends).
- Author: OldCodingFarmer
- Creation Date: Fri Aug 07 2015 03:45:58 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 08:18:52 GMT+0800 (Singapore Standard Time)

<p>
        
    def longestPalindrome(self, s):
        res = ""
        for i in xrange(len(s)):
            # odd case, like "aba"
            tmp = self.helper(s, i, i)
            if len(tmp) > len(res):
                res = tmp
            # even case, like "abba"
            tmp = self.helper(s, i, i+1)
            if len(tmp) > len(res):
                res = tmp
        return res
     
    # get the longest palindrome, l, r are the middle indexes   
    # from inner to outer
    def helper(self, s, l, r):
        while l >= 0 and r < len(s) and s[l] == s[r]:
            l -= 1; r += 1
        return s[l+1:r]
</p>


