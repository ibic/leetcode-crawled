---
title: "Find Winner on a Tic Tac Toe Game"
weight: 1209
#id: "find-winner-on-a-tic-tac-toe-game"
---
## Description
<div class="description">
<p>Tic-tac-toe is played&nbsp;by&nbsp;two players <em>A</em> and <em>B</em> on a&nbsp;<i>3</i>&nbsp;x&nbsp;<i>3</i>&nbsp;grid.</p>

<p>Here are the rules of Tic-Tac-Toe:</p>

<ul>
	<li>Players take turns placing characters into empty squares (&quot; &quot;).</li>
	<li>The first player <em>A</em> always places &quot;X&quot; characters, while the second player <em>B</em>&nbsp;always places &quot;O&quot; characters.</li>
	<li>&quot;X&quot; and &quot;O&quot; characters are always placed into empty squares, never on filled ones.</li>
	<li>The game ends when there are 3 of the same (non-empty) character filling any row, column, or diagonal.</li>
	<li>The game also ends if all squares are non-empty.</li>
	<li>No more moves can be played if the game is over.</li>
</ul>

<p>Given an array <code>moves</code> where each element&nbsp;is another array of size 2 corresponding to the row and column of the grid where they mark their respective character in the order in which <em>A</em> and <em>B</em> play.</p>

<p>Return the winner of the game if it exists (<em>A</em> or <em>B</em>), in case the game ends in a draw return &quot;Draw&quot;, if there are still movements to play return &quot;Pending&quot;.</p>

<p>You can assume that&nbsp;<code>moves</code> is&nbsp;<strong>valid</strong> (It follows the rules of Tic-Tac-Toe),&nbsp;the grid is initially empty and <em>A</em> will play <strong>first</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> moves = [[0,0],[2,0],[1,1],[2,1],[2,2]]
<strong>Output:</strong> &quot;A&quot;
<strong>Explanation:</strong> &quot;A&quot; wins, he always plays first.
&quot;X  &quot;    &quot;X  &quot;    &quot;X  &quot;    &quot;X  &quot;    &quot;<strong>X</strong>  &quot;
&quot;   &quot; -&gt; &quot;   &quot; -&gt; &quot; X &quot; -&gt; &quot; X &quot; -&gt; &quot; <strong>X</strong> &quot;
&quot;   &quot;    &quot;O  &quot;    &quot;O  &quot;    &quot;OO &quot;    &quot;OO<strong>X</strong>&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> moves = [[0,0],[1,1],[0,1],[0,2],[1,0],[2,0]]
<strong>Output:</strong> &quot;B&quot;
<strong>Explanation:</strong> &quot;B&quot; wins.
&quot;X  &quot;    &quot;X  &quot;    &quot;XX &quot;    &quot;XXO&quot;    &quot;XXO&quot;    &quot;XX<strong>O</strong>&quot;
&quot;   &quot; -&gt; &quot; O &quot; -&gt; &quot; O &quot; -&gt; &quot; O &quot; -&gt; &quot;XO &quot; -&gt; &quot;X<strong>O</strong> &quot; 
&quot;   &quot;    &quot;   &quot;    &quot;   &quot;    &quot;   &quot;    &quot;   &quot;    &quot;<strong>O</strong>  &quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> moves = [[0,0],[1,1],[2,0],[1,0],[1,2],[2,1],[0,1],[0,2],[2,2]]
<strong>Output:</strong> &quot;Draw&quot;
<strong>Explanation:</strong> The game ends in a draw since there are no moves to make.
&quot;XXO&quot;
&quot;OOX&quot;
&quot;XOX&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> moves = [[0,0],[1,1]]
<strong>Output:</strong> &quot;Pending&quot;
<strong>Explanation:</strong> The game has not finished yet.
&quot;X  &quot;
&quot; O &quot;
&quot;   &quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= moves.length &lt;= 9</code></li>
	<li><code>moves[i].length == 2</code></li>
	<li><code>0 &lt;= moves[i][j] &lt;= 2</code></li>
	<li>There are no repeated elements on <code>moves</code>.</li>
	<li><code>moves</code> follow the rules of tic tac toe.</li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Zoho - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Check rows, columns and two diagonals w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Dec 01 2019 12:03:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 06 2020 01:00:38 GMT+0800 (Singapore Standard Time)

<p>
1. `D1` and `D2` refer to main and skew diagonals respectively;
2. Use `r == c` and `r + c == 2` to judge main and skew diagonals.

**Java**
```java
    public String tictactoe(int[][] moves) {
        int[] aRow = new int[3], aCol = new int[3], bRow = new int[3], bCol = new int[3];
        int  aD1 = 0, aD2 = 0, bD1 = 0, bD2 = 0;
        for (int i = 0; i < moves.length; ++i) {
            int r = moves[i][0], c = moves[i][1];
            if (i % 2 == 1) {
                if (++bRow[r] == 3 || ++bCol[c] == 3 || r == c && ++bD1 == 3 || r + c == 2 && ++bD2 == 3) return "B";
            }else {
                if (++aRow[r] == 3 || ++aCol[c] == 3 || r == c && ++aD1 == 3 || r + c == 2 && ++aD2 == 3) return "A";
            }
        }
        return moves.length == 9 ? "Draw" : "Pending";        
    }
```
Make it shorter:

id: 0 for A, 1 for B;
row[id][r]: the current number of characters that player `id` places on row `r`;
col[id][c]: the current number of characters that player `id` places on column `c`;
d1[id]: the current number of characters that player `id` places on main diagonal;
d2[id]: the current number of characters that player `id` places on skew diagonal.

```java
    public String tictactoe(int[][] moves) {
        int[][] row = new int[2][3], col = new int[2][3];
        int[] d1 = new int[2], d2 = new int[2];
        for (int i = 0; i < moves.length; ++i) {
            int r = moves[i][0], c = moves[i][1], id = i % 2;
            if (++row[id][r] == 3 || ++col[id][c] == 3 || r == c && ++d1[id] == 3 || r + c == 2 && ++d2[id] == 3) 
                return id == 0 ? "A" : "B";
        }
        return moves.length == 9 ? "Draw" : "Pending";        
    }
```

----

**Python 3**
```python
    def tictactoe(self, moves: List[List[int]]) -> str:
        row, col = [[0] * 3 for _ in range(2)], [[0] * 3 for _ in range(2)]
        d1, d2, id = [0] * 2, [0] * 2, 0
        for r, c in moves:
            row[id][r] += 1
            col[id][c] += 1
            d1[id] += r == c
            d2[id] += r + c == 2
            if 3 in (row[id][r], col[id][c], d1[id], d2[id]):
                return \'AB\'[id]
            id ^= 1
        return \'Draw\' if len(moves) == 9 else \'Pending\'
```
**Analysis**
Time: O(moves.length), space: O(R + C), where R and C are the number of rows and columns respectively.
</p>


### [Java/Python/C++] 0ms, short and simple, all 8 ways to win in one array
- Author: savvadia
- Creation Date: Sun Dec 01 2019 12:42:36 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 02 2020 16:10:39 GMT+0800 (Singapore Standard Time)

<p>
There are 8 ways to win for each player:
 - 3 columns
 - 3 rows
 - 2 diagonals
 
Players make moves one by one so all odd moves are for player A, even for B.
Now we just need to track if we reach 3 in any line for any of the players.
One array keeps all ways to win for each player:
  - 0,1,2 - for rows
  - 3,4,5 - for cols
  - 6 - for diagonal top left - bottom right
  - 7 - for diagonal top right - bottom left

Java, 0 ms
```
    public String tictactoe(int[][] moves) {
        int[] A = new int[8], B = new int[8];  // 3 rows, 3 cols, 2 diagonals
        for(int i=0;i<moves.length;i++) {
            int r=moves[i][0], c=moves[i][1];
            int[] player = (i%2==0)?A:B;
            player[r]++;
            player[c+3]++;
            if(r==c) player[6]++;
            if(r==2-c) player[7]++;
        }
        for(int i=0;i<8;i++) {
            if(A[i]==3) return "A";
            if(B[i]==3) return "B";
        }
        return moves.length==9 ? "Draw":"Pending";
    }
```

C++, 0 ms
 ```
    string tictactoe(vector<vector<int>>& moves) {
       vector<int> A(8,0), B(8,0); // 3 rows, 3 cols, 2 diagonals
       for(int i=0; i<moves.size(); i++) {
           int r=moves[i][0], c=moves[i][1];
           vector<int>& player = (i%2==0)?A:B;
           player[r]++;
           player[c+3]++; 
           if(r==c) player[6]++;
           if(r==2-c) player[7]++;
       }
       for(int i=0; i<8; i++) {
           if(A[i]==3) return "A";
           if(B[i]==3) return "B";
       }
       return moves.size()==9 ? "Draw":"Pending";
   }
```

Python, 24 ms, beats 100%
```
    def tictactoe(self, moves: List[List[int]]) -> str:
        A=[0]*8
        B=[0]*8
        for i in range(len(moves)):
            r,c=moves[i]
            player = A if i%2==0 else B
            player[r] += 1
            player[c+3] += 1
            if r==c:
                player[6] += 1
            if r==2-c:
                player[7] += 1
        for i in range(8):
            if A[i]==3:
                return "A"
            if B[i]==3:
                return "B"
        
        return "Draw" if len(moves) == 9 else "Pending"
```
</p>


### Java 100% with comments and Interview tip
- Author: PiyushSaravagi
- Creation Date: Sun Jan 26 2020 07:40:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 26 2020 07:45:47 GMT+0800 (Singapore Standard Time)

<p>
I recently had an interview with Amazon and one of the rounds was code reusability. They wanted to see how easy it was to adapt my code to changing needs and maintain it. If you just think of tic-tac toe as a 3x3 grid you are doing it wrong. The interviewer would ask you to build a tic-tac-toe for 3x3 grid, you write you code and check for all conditions and it works great. Then the interviewer takes his turn to play :) He\'d tell you that the requirements have changed and now the game is a 4x4 grid instead. How would you do it?
1. Erase the code you\'ve written, scratch it out, change all the 3\'s to 4, modify the conditional checks, introduce bugs, then fix it
2. Make a single change since you started off by writing reusable code

Here\'s my version of tic-tac-toe that works for n x n grid
```
class Solution {
    int n = 3;
    public String tictactoe(int[][] moves) {
        char[][] board = new char[n][n];
        for(int i = 0; i < moves.length; i++){
            int row = moves[i][0];
            int col = moves[i][1];
            if((i & 1) == 0){
                //even, X\'s move
                board[row][col] = \'X\';
                if(didWin(board, row, col, \'X\')) return "A";
            }else{
                //odd, O\'s move
                board[row][col] = \'O\';
                if(didWin(board, row, col, \'O\')) return "B";
            }
        }
        return moves.length == n * n ? "Draw" : "Pending";
    }
    
    private boolean didWin(char[][] board, int row, int col, char player){
        //check the current row
        boolean didPlayerWin = true;
        for(int i = 0; i < n; i++){
            if(board[row][i] != player){
                didPlayerWin = false;
            }                
        }
        if(didPlayerWin) return true;   //player has won the game
        
        //check the current col
        didPlayerWin = true;
        for(int i = 0; i < n; i++){
            if(board[i][col] != player){
                didPlayerWin = false;
            }                
        }
        if(didPlayerWin) return true;   //player has won the game
        
        //check the diagonal
        if(row == col){
            didPlayerWin = true;
            for(int i = 0; i < n; i++){
                if(board[i][i] != player){
                    didPlayerWin = false;
                }                
            }
            if(didPlayerWin) return true;   //player has won the game    
        }
        
        //check reverse diagonal
        if(col == n - row - 1){
            didPlayerWin = true;
            for(int i = 0; i < n; i++){
                if(board[i][n-i-1] != player){
                    didPlayerWin = false;
                }                
            }
            if(didPlayerWin) return true;   //player has won the game    
        }
        
        //player did not win
        return false;
    }
}
```
</p>


