---
title: "Last Moment Before All Ants Fall Out of a Plank"
weight: 1377
#id: "last-moment-before-all-ants-fall-out-of-a-plank"
---
## Description
<div class="description">
<p>We have a wooden&nbsp;plank of the length <code>n</code> <strong>units</strong>. Some ants are walking on the&nbsp;plank, each ant moves with speed <strong>1 unit per second</strong>. Some of the ants move to the <strong>left</strong>, the other move to the <strong>right</strong>.</p>

<p>When two ants moving in two <strong>different</strong> directions meet at some point, they change their directions and continue moving again. Assume changing directions doesn&#39;t take any additional time.</p>

<p>When an ant reaches <strong>one end</strong> of the plank at a time <code>t</code>, it falls out of the plank imediately.</p>

<p>Given an integer <code>n</code> and two integer arrays <code>left</code> and <code>right</code>, the positions of the ants moving to the left and the right.&nbsp;Return <em>the&nbsp;moment</em> when the last ant(s) fall out of the plank.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/06/17/ants.jpg" style="width: 450px; height: 610px;" />
<pre>
<strong>Input:</strong> n = 4, left = [4,3], right = [0,1]
<strong>Output:</strong> 4
<strong>Explanation:</strong> In the image above:
-The ant at index 0 is named A and going to the right.
-The ant at index 1 is named B and going to the right.
-The ant at index 3 is named C and going to the left.
-The ant at index 4 is named D and going to the left.
Note that the last moment when an ant was on the plank is t = 4 second, after that it falls imediately out of the plank. (i.e. We can say that at t = 4.0000000001, there is no ants on the plank).
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/06/17/ants2.jpg" style="width: 639px; height: 101px;" />
<pre>
<strong>Input:</strong> n = 7, left = [], right = [0,1,2,3,4,5,6,7]
<strong>Output:</strong> 7
<strong>Explanation:</strong> All ants are going to the right, the ant at index 0 needs 7 seconds to fall.
</pre>

<p><strong>Example 3:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/06/17/ants3.jpg" style="width: 639px; height: 100px;" />
<pre>
<strong>Input:</strong> n = 7, left = [0,1,2,3,4,5,6,7], right = []
<strong>Output:</strong> 7
<strong>Explanation:</strong> All ants are going to the left, the ant at index 7 needs 7 seconds to fall.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 9, left = [5], right = [4]
<strong>Output:</strong> 5
<strong>Explanation:</strong> At t = 1 second, both ants will be at the same intial position but with different direction.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 6, left = [6], right = [0]
<strong>Output:</strong> 6
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^4</code></li>
	<li><code>0 &lt;= left.length &lt;= n + 1</code></li>
	<li><code>0 &lt;= left[i] &lt;= n</code></li>
	<li><code>0 &lt;= right.length &lt;= n + 1</code></li>
	<li><code>0 &lt;= right[i] &lt;= n</code></li>
	<li><code>1 &lt;= left.length + right.length &lt;= n + 1</code></li>
	<li>All values of <code>left</code> and <code>right</code> are unique, and each value can appear <strong>only in one</strong> of the two arrays.</li>
</ul>
</div>

## Tags
- Array (array)
- Brainteaser (brainteaser)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Ants Keep Walking, O(N)
- Author: lee215
- Creation Date: Sun Jul 05 2020 12:09:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 05 2020 23:43:12 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
When two ants meet at some point,
they change their directions and continue moving again.
But you can assume they don\'t change direction and keep moving.

(You don\'t really know the difference of ants between one and the other, do you?)
<br>

# Explanation
For ants in direction of left, the leaving time is `left[i]`
For ants in direction of right, the leaving time is `n - right[i]`
<br>

# **Complexity**
Time `O(N)`
Space `O(1)`

<br>

**Java:**
```java
    public int getLastMoment(int n, int[] left, int[] right) {
        int res = 0;
        for (int i: left)
            res = Math.max(res, i);
        for (int i: right)
            res = Math.max(res, n - i);
        return res;
    }
```

**C++:**
```cpp
    int getLastMoment(int n, vector<int>& left, vector<int>& right) {
        int res = 0;
        for (int& i: left)
            res = max(res, i);
        for (int& i: right)
            res = max(res, n - i);
        return res;
    }
```

**Python:**
```py
    def getLastMoment(self, n, left, right):
        return max(max(left or [0]), n - min(right or [n]))
```

</p>


### C++/Java: two-way street O(n)
- Author: votrubac
- Creation Date: Sun Jul 05 2020 12:03:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 05 2020 13:08:01 GMT+0800 (Singapore Standard Time)

<p>
If two ants bump into each other and change directions, it\'s the same as if these ants continue as nothing happens.

So, we can think about that plank as a two-way street. So, find the maximum units that any ant needs to travel.

>  I got this intuition by manually solving few test cases. My first idea was simulation, but the implementation seemed too complex. 
>  So, I was already looking for ways to simplify it.
>  
>  For me, during the contest and the interview, finding few good test cases and solving them manually is the way to unstuck.

**C++**
```cpp
int getLastMoment(int n, vector<int>& left, vector<int>& right) {
    return max(left.empty() ? 0 : *max_element(begin(left), end(left)),
        n - (right.empty() ? n : *min_element(begin(right), end(right))));
}
```
If you do not like one-liners, here is an easier to read Java solution.

**Java**
```java
public int getLastMoment(int n, int[] left, int[] right) {
    int units = 0;
    for (var u : left)
        units = Math.max(units, u);
    for (var u : right)
        units = Math.max(units, n - u);        
    return units;
}
```
</p>


### [C++, Python, Java] Beautiful Visual Explanation
- Author: interviewrecipes
- Creation Date: Sun Jul 05 2020 12:56:17 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 17 2020 02:39:17 GMT+0800 (Singapore Standard Time)

<p>
# Introduction
First of all, it\'s a too tricky for a coding problem. In interviews, generally nobody would ask questions like this, or at least most won\'t prefer asking this. So some sigh of relief there. As for solving the problem goes - hope that the diagram makes visualization clear & hence assist in getting the intuition. 

# It\'s a lot about observation
The problem, I think, is about testing the observation skills. Observing and analysing the ant behavior will reveal that the problem is way to tricky to understand, but way to easy to implement.

# Key idea / Intuition
The idea is to think of the bigger picture and focus only on the paths of the ants. At any collision, think of it as ants transferring their responsibility of going to the other end, to each other. So one ant covers the path for the other.

# Explanation
As you see this, the implementation is trivial. Many learners suggested that I explain with code, so please check the code below which also has the explanation.

![image](https://assets.leetcode.com/users/images/7d5d2ab8-1fb5-4a59-bc8c-d82e1e947670_1593995093.9210796.png)


## Thank you. 

Please upvote if it helps -

1. It encourages me to write and share my solutions if they are easy to understand.
2. It reaches to more learners, larger audience. :)

Thanks.

# C++
```
class Solution {
public:
    int getLastMoment(int n, vector<int>& left, vector<int>& right) {
        // The one that is farthest from the left end, but desires to go in the left
        // direction, will be the last one to go off of the plank from the left side.
        int maxLeft = left.empty() ? 0 : *max_element(left.begin(), left.end());
        
        // Similarly,
        // The one that is farthest from the right end, but desires to go in the right
        // direction, will be the last one to go off of the plank from the right side.
        int minRight = right.empty() ? n : *min_element(right.begin(), right.end());
        
        // The one among above two would be the last one to off of the plank among all.
        return max(maxLeft, n - minRight);
    }
};
```

Other languages -
# Python
```
class Solution(object):
    def getLastMoment(self, n, left, right):
        maxLeft  = max(left)  if len(left) > 0 else 0
        minRight = min(right) if (len(right)) > 0 else n
        return max(maxLeft, n - minRight)
```

# Java
```
class Solution {
    public int getLastMoment(int n, int[] left, int[] right) {
        int maxLeft = 0;
        for(int x : left) {
            maxLeft=Math.max(maxLeft, x);
        }
        int minRight = n;
        for(int x : right) {
            minRight=Math.min(minRight, x);
        }
        return Math.max(maxLeft, n - minRight);
    }
}
```

</p>


