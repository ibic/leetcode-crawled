---
title: "Redundant Connection II"
weight: 617
#id: "redundant-connection-ii"
---
## Description
<div class="description">
<p>
In this problem, a rooted tree is a <b>directed</b> graph such that, there is exactly one node (the root) for which all other nodes are descendants of this node, plus every node has exactly one parent, except for the root node which has no parents.
</p><p>
The given input is a directed graph that started as a rooted tree with N nodes (with distinct values 1, 2, ..., N), with one additional directed edge added.  The added edge has two different vertices chosen from 1 to N, and was not an edge that already existed.
</p><p>
The resulting graph is given as a 2D-array of <code>edges</code>.  Each element of <code>edges</code> is a pair <code>[u, v]</code> that represents a <b>directed</b> edge connecting nodes <code>u</code> and <code>v</code>, where <code>u</code> is a parent of child <code>v</code>.
</p><p>
Return an edge that can be removed so that the resulting graph is a rooted tree of N nodes.  If there are multiple answers, return the answer that occurs last in the given 2D-array.
</p><p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [[1,2], [1,3], [2,3]]
<b>Output:</b> [2,3]
<b>Explanation:</b> The given directed graph will be like this:
  1
 / \
v   v
2-->3
</pre>
</p>
<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> [[1,2], [2,3], [3,4], [4,1], [1,5]]
<b>Output:</b> [4,1]
<b>Explanation:</b> The given directed graph will be like this:
5 <- 1 -> 2
     ^    |
     |    v
     4 <- 3
</pre>
</p>
<p><b>Note:</b><br />
<li>The size of the input 2D-array will be between 3 and 1000.</li>
<li>Every integer represented in the 2D-array will be between 1 and N, where N is the size of the input array.</li>
</p>
</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)
- Union Find (union-find)
- Graph (graph)

## Companies
- Google - 2 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Depth-First Search [Accepted]

**Intuition**

Starting from a rooted tree with `N-1` edges and `N` vertices, let's enumerate the possibilities for the added "redundant" edge.  If there is no loop, then either one vertex must have two parents (or no edge is redundant.)  If there is a loop, then either one vertex has two parents, or every vertex has one parent.

In the first two cases, there are only two candidates for deleting an edge, and we can try removing the last one and seeing if that works.  In the last case, the last edge of the cycle can be removed: for example, when `1->2->3->4->1->5`, we want the last edge (by order of occurrence) in the cycle `1->2->3->4->1` (but not necessarily `1->5`).

**Algorithm**

We'll first construct the underlying graph, keeping track of edges coming from nodes with multiple parents.  After, we either have 2 or 0 `candidates`.

If there are no candidates, then every vertex has one parent, such as in the case `1->2->3->4->1->5`.  From any node, we walk towards it's parent until we revisit a node - then we must be inside the cycle, and any future seen nodes are part of that cycle.  Now we take the last edge that occurs in the cycle.

Otherwise, we'll see if the graph induced by `parent` is a rooted tree.  We again take the `root` by walking from any node towards the parent until we can't, then we perform a depth-first search on this `root`.  If we visit every node, then removing the last of the two edge candidates is acceptable, and we should.  Otherwise, we should remove the first of the two edge candidates.

In our solution, we use `orbit` to find the result upon walking from a node `x` towards it's parent repeatedly until you revisit a node or can't walk anymore.  `orbit(x).node` (or `orbit(x)[0]` in Python) will be the resulting node, while `orbit(x).seen` (or `orbit(x)[1]`) will be all the nodes visited.

<iframe src="https://leetcode.com/playground/cvUmads5/shared" frameBorder="0" width="100%" height="500" name="cvUmads5"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$ where $$N$$ is the number of vertices (and also the number of edges) in the graph.  We perform a depth-first search.

* Space Complexity:  $$O(N)$$, the size of the graph.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java, Union Find with explanation, O(n)
- Author: zestypanda
- Creation Date: Wed Sep 27 2017 08:13:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 05:27:28 GMT+0800 (Singapore Standard Time)

<p>
This problem is very similar to "Redundant Connection". But the description on the parent/child relationships is much better clarified. 
```
There are two cases for the tree structure to be invalid.
1) A node having two parents;
   including corner case: e.g. [[4,2],[1,5],[5,2],[5,3],[2,4]]
2) A circle exists
```
If we can remove exactly 1 edge to achieve the tree structure, a single node can have at most two parents. So my solution works in two steps.
```
1) Check whether there is a node having two parents. 
    If so, store them as candidates A and B, and set the second edge invalid. 
2) Perform normal union find. 
    If the tree is now valid 
           simply return candidate B
    else if candidates not existing 
           we find a circle, return current edge; 
    else 
           remove candidate A instead of B.
```
If you like this solution, please help upvote so more people can see.
```
class Solution {
public:
    vector<int> findRedundantDirectedConnection(vector<vector<int>>& edges) {
        int n = edges.size();
        vector<int> parent(n+1, 0), candA, candB;
        // step 1, check whether there is a node with two parents
        for (auto &edge:edges) {
            if (parent[edge[1]] == 0)
                parent[edge[1]] = edge[0]; 
            else {
                candA = {parent[edge[1]], edge[1]};
                candB = edge;
                edge[1] = 0;
            }
        } 
        // step 2, union find
        for (int i = 1; i <= n; i++) parent[i] = i;
        for (auto &edge:edges) {
            if (edge[1] == 0) continue;
            int u = edge[0], v = edge[1], pu = root(parent, u);
            // Now every node only has 1 parent, so root of v is implicitly v
            if (pu == v) {
                if (candA.empty()) return edge;
                return candA;
            }
            parent[v] = pu;
        }
        return candB;
    }
private:
    int root(vector<int>& parent, int k) {
        if (parent[k] != k) 
            parent[k] = root(parent, parent[k]);
        return parent[k];
    }
};
```
Java version by MichaelLeo 
```
class Solution {
    public int[] findRedundantDirectedConnection(int[][] edges) {
        int[] can1 = {-1, -1};
        int[] can2 = {-1, -1};
        int[] parent = new int[edges.length + 1];
        for (int i = 0; i < edges.length; i++) {
            if (parent[edges[i][1]] == 0) {
                parent[edges[i][1]] = edges[i][0];
            } else {
                can2 = new int[] {edges[i][0], edges[i][1]};
                can1 = new int[] {parent[edges[i][1]], edges[i][1]};
                edges[i][1] = 0;
            }
        }
        for (int i = 0; i < edges.length; i++) {
            parent[i] = i;
        }
        for (int i = 0; i < edges.length; i++) {
            if (edges[i][1] == 0) {
                continue;
            }
            int child = edges[i][1], father = edges[i][0];
            if (root(parent, father) == child) {
                if (can1[0] == -1) {
                    return edges[i];
                }
                return can1;
            }
            parent[child] = father;
        }
        return can2;
    }
    
    int root(int[] parent, int i) {
        while (i != parent[i]) {
            parent[i] = parent[parent[i]];
            i = parent[i];
        }   
        return i;
    }
}
```
</p>


### one pass disjoint set solution with explain
- Author: tyuan73
- Creation Date: Thu Oct 05 2017 13:08:34 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 22:05:08 GMT+0800 (Singapore Standard Time)

<p>
This problem is tricky and interesting. It took me quite a few hours to figure it out. My first working solution was based on DFS, but I feel there might be better solutions. By spending whole night thinking deeply, I finally cracked it using Disjoint Set (also known as Union Find?). I want to share what I got here.

Assumption before we start: input "**edges**" contains a directed tree with one and only one extra edge. If we remove the extra edge, the remaining graph should make a directed tree - a tree which has one root and from the root you can visit all other nodes by following directed edges. It has features:
1. one and only one root, and root does not have parent;
2. each non-root node has exactly one parent;
3. there is no cycle, which means any path will reach the end by moving at most (n-1) steps along the path.

By adding one edge ***(parent->child)*** to the tree:
1. every node including root has exactly one parent, if ***child*** is root;
2. root does not have parent, one node (***child***) has 2 parents, and all other nodes have exactly 1 parent, if ***child*** is not root.

Let's check cycles. By adding one edge ***(a->b)*** to the tree, the tree will have:
1. a cycle, if there exists a path from ***(b->...->a)***; in particularly, if ***b == root***, (in other word, add an edge from a node to root) it will make a cycle since there must be a path ***(root->...->a)***.
2. no cycle, if there is no such a path ***(b->...->a)***.

After adding the extra edge, the graph can be generalized in 3 different cases:
![0_1507232871672_Screen Shot 2017-10-05 at 2.25.34 PM.png](/assets/uploads/files/1507232873325-screen-shot-2017-10-05-at-2.25.34-pm-resized.png) 

```Case 1```: "c" is the only node which has 2 parents and there is not path (c->...->b) which means no cycle. In this case, removing either "e1" or "e2" will make the tree valid. According to the description of the problem, whichever edge added later is the answer.

```Case 2```: "c" is the only node which has 2 parents and there is a path(c->...->b) which means there is a cycle. In this case, "e2" is the only edge that should be removed. Removing "e1" will make the tree in 2 separated groups. Note, in input `edges`, "e1" may come after "e2".

```Case 3```: this is how it looks like if edge ***(a->root)*** is added to the tree. Removing any of the edges along the cycle will make the tree valid. But according to the description of the problem, the last edge added to complete the cycle is the answer. Note: edge "e2" (an edge pointing from a node outside of the cycle to a node on the cycle) can never happen in this case, because every node including root has exactly one parent. If "e2" happens, that make a node on cycle have 2 parents. That is impossible.

As we can see from the pictures, the answer must be:
1. one of the 2 edges that pointing to the same node in ``case 1`` and ``case 2``; there is one and only one such node which has 2 parents.
2. the last edge added to complete the cycle in ``case 3``.

Note: both ``case 2`` and ``case 3`` have cycle, but in ``case 2``, "e2" may not be the last edge added to complete the cycle.

Now, we can apply Disjoint Set (DS) to build the tree in the order the edges are given. We define ``ds[i]`` as the parent or ancestor of node ``i``. It will become the root of the whole tree eventually if `edges` does not have extra edge. When given an edge (a->b), we find node ``a``'s ancestor and assign it to `ds[b]`. Note, in typical DS, we also need to find node `b`'s ancestor and assign `a`'s ancestor as the ancestor of `b`'s ancestor. But in this case, we don't have to, since we skip the second parent edge (see below), it is guaranteed `a` is the only parent of `b`.

If we find an edge pointing to a node that already has a parent, we simply skip it. The edge skipped can be "e1" or "e2" in ``case 1`` and ``case 2``. In ``case 1``, removing either "e1" or "e2" will make the tree valid. In ``case 3``, removing "e2" will make the tree valid, but removing "e1" will make the tree in 2 separated groups and one of the groups has a cycle. In ```case 3```, none of the edges will be skipped because there is no 2 edges pointing to the same node. The result is a graph with cycle and "n" edges.

**How to detect cycle by using Disjoint Set (Union Find)?**
When we join 2 nodes by edge (a->b), we check `a`'s ancestor, if it is b, we find a cycle! When we find a cycle, we don't assign `a`'s ancestor as `b`'s ancestor. That will trap our code in endless loop. We need to save the edge though since it might be the answer in `case 3`.

Now the code. We define two variables (`first` and `second`) to store the 2 edges that point to the same node if there is any (there may not be such edges, see `case 3`). We skip adding `second` to tree. `first` and `second` hold the values of the original index in input `edges` of the 2 edges respectively. Variable `last` is the edge added to complete a cycle if there is any (there may not be a cycle, see `case 1` and removing "e2" in `case 2`). And it too hold the original index in input `edges`.

After adding all except at most one edges to the tree, we end up with 4 different scenario:
1. `case 1` with either "e1" or "e2" removed. Either way, the result tree is valid. The answer is the edge being removed or skipped (a.k.a. `second`)
2. `case 2` with "e2" removed. The result tree is valid. The answer is the edge being removed or skipped (a.k.a. `second`)
3. `case 2` with "e1" removed. The result tree is invalid with a cycle in one of the groups. The answer is the other edge (`first`) that points to the same node as `second`. 
4. `case 3` with no edge removed. The result tree is invalid with a cycle. The answer is the `last` edge added to complete the cycle.

In the following code,
`last == -1` means "no cycle found" which is scenario 1 or 2
`second != -1 && last != -1` means "one edge removed and the result tree has cycle" which is scenario 3
`second == -1` means "no edge skipped or removed" which is scenario 4

``` 
   public int[] findRedundantDirectedConnection(int[][] edges) {
        int n = edges.length;
        int[] parent = new int[n+1], ds = new int[n+1];
        Arrays.fill(parent, -1);
        int first = -1, second = -1, last = -1;
        for(int i = 0; i < n; i++) {
            int p = edges[i][0], c = edges[i][1];
            if (parent[c] != -1) {
                first = parent[c];
                second = i;
                continue;
            }
            parent[c] = i;
            
            int p1 = find(ds, p);
            if (p1 == c) last = i;
            else ds[c] = p1;
        }

        if (last == -1) return edges[second]; // no cycle found by removing second
        if (second == -1) return edges[last]; // no edge removed
        return edges[first];
    }
    
    private int find(int[] ds, int i) {
        return ds[i] == 0 ? i : (ds[i] = find(ds, ds[i]));
    }
```
This solution past all these test cases and ACed.
Test case:
[[1,2],[1,3],[2,3]]
[[1,2], [2,3], [3,4], [4,1], [1,5]]
[[4,2],[1,5],[5,2],[5,3],[2,4]]
[[2,1],[3,1],[4,2],[1,4]]
[[4,1],[1,2],[1,3],[4,5],[5,6],[6,5]]
[[2,3], [3,4], [4,1], [1,5], [1,2]]
[[3,1],[1,4],[3,5],[1,2],[1,5]]
[[1,2],[2,3],[3,1]]

expected output:
[2,3]
[4,1]
[4,2]
[2,1]
[6,5]
[1,2]
[1,5]
[3,1]
</p>


### [中文]清晰易懂的思路
- Author: rampaging9
- Creation Date: Sat Apr 20 2019 11:22:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Apr 20 2019 11:22:08 GMT+0800 (Singapore Standard Time)

<p>

\u5199\u5728\u6700\u524D\u9762\uFF0C\u89C9\u5F97\u601D\u8DEF\u5F88\u4E71\u7684\u65F6\u5019\u62FF\u8D77\u7EB8\u7B14\u8D70\u4E00\u8D70\u6837\u4F8B\uFF0C\u6A21\u62DF\u6A21\u62DF\u6D41\u7A0B\uFF0C\u4F1A\u6E05\u6670\u8BB8\u591A\u3002
\u9898\u76EE\u8BF4\u4E86\u53EA\u9700\u8981\u53BB\u9664\u4E00\u6761\u8FB9\uFF0C\u5C31\u80FD\u53D8\u6210\u4E00\u4E2A\u6839\u6811\uFF08\u6709\u5411\uFF09\uFF0C\u53CD\u8FC7\u6765\u8BF4\u4E00\u5B9A\u5B58\u5728\uFF08\u53EF\u4EE5\u5B58\u5728\u5F88\u591A\uFF09\u67D0\u4E2A\u6839\u6811\u52A0\u4E86\u4E00\u6761\u8FB9\u53D8\u6210\u4E86\u73B0\u5728\u7684\u56FE\u3002
\u8FD9\u6761\u591A\u4F59\u7684\u8FB9\uFF0C\u4F1A\u5206\u4E3A\u4EE5\u4E0B\u60C5\u51B5\uFF1A 
1. \u8FD9\u6761\u8FB9\u4ECE\u4EFB\u610F\u4E00\u4E2A\u8282\u70B9\u51FA\u53D1\uFF0C\u6307\u5411\u6811\u6839\u3002

	
	![image](https://assets.leetcode.com/users/rampaging9/image_1555729612.png)

	\u8FD9\u79CD\u60C5\u51B5\u4E0B\uFF0C\u6811\u4E0D\u5408\u6CD5\u7684\u539F\u56E0\u662F\uFF0C\u6811\u6CA1\u4E86\u6839\uFF0C\u56E0\u4E3A\u6240\u6709\u70B9\u7684\u5165\u5EA6\u90FD\u4E3A1\u3002\u663E\u800C\u6613\u89C1\u7684\u662F\u5728\u73AF\u4E0A\u7684\u4EFB\u610F\u4E00\u6761\u8FB9\u90FD\u53EF\u4EE5\u5220\u9664\uFF0C\u5220\u9664\u4E4B\u540E\u5C31\u53EF\u4EE5\u5F97\u5230\u4E00\u4E2A\u5408\u6CD5\u7684\u6839\u6811\u3002
	**\u5373\u8FD9\u6761\u6A59\u8272\u7684\u591A\u4F59\u7684\u8FB9\uFF0C\u53EA\u662F\u6211\u4EEC\u4E0A\u9762\u63D0\u5230\u7684\u5B58\u5728\u7684\u53EF\u80FD\u6027\u7684\u67D0\u4E00\u79CD\uFF0C\u5982\u4F60\u6240\u89C1\uFF0C\u5176\u5B9E\u73AF\u4E0A\u7684\u6BCF\u4E00\u6761\u8FB9\u90FD\u53EF\u4EE5\u662F\u201C\u591A\u4F59\u8FB9\u201D\uFF0C\u8BF7\u7406\u89E3\u8FD9\u53E5\u8BDD**\uFF0C\u65E2\u7136\u6BCF\u4E00\u6761\u90FD\u53EF\u4EE5\u5220\u9664\uFF0C\u90A3\u6309\u7167\u9898\u76EE\u8981\u6C42\u5220\u6389\u73AF\u4E2D\u6700\u540E\u51FA\u73B0\u7684\u90A3\u6761\u8FB9\u5C31\u597D\u3002

2. \u8FD9\u6761\u8FB9\u4ECE\u4EFB\u610F\u8282\u70B9\u51FA\u53D1\uFF0C\u6307\u5411\u7956\u5148\u94FE\u4E0A\u4EFB\u610F\u4E00\u4E2A\u975E\u6839\u8282\u70B9\u3002
	![image](https://assets.leetcode.com/users/rampaging9/image_1555729798.png)
	\u8FD9\u79CD\u60C5\u51B5\u4E0B\uFF0C\u6811\u4E0D\u5408\u6CD5\u7684\u539F\u56E0\u662F\u51FA\u73B0\u4E86\u4E00\u4E2A\u6709\u4E24\u4E2A\u5165\u5EA6\u7684\u8282\u70B9\uFF0C**\u5E76\u4E14\u5176\u4E2D\u4E00\u4E2A\u5165\u5EA6\u6765\u81EA\u73AF\u4E2D\u7684\u4E00\u6761\u8FB9**\u3002\u53EF\u4EE5\u89C2\u5BDF\u5230\uFF0C\u8FD9\u4E24\u4E2A\u5165\u5EA6\u91CC\uFF0C\u53EA\u80FD\u5220\u9664\u6765\u81EA\u73AF\u91CC\u7684\u90A3\u6761\u8FB9\u3002

3. \u8FD9\u6761\u8FB9\u4ECE\u4EFB\u610F\u8282\u70B9\u51FA\u53D1\uFF0C\u6307\u5411\u975E\u7956\u5148\u94FE\u4E0A\u4EFB\u610F\u4E00\u4E2A\u8282\u70B9\u3002
	
	![image](https://assets.leetcode.com/users/rampaging9/image_1555730504.png)


	\u8FD9\u79CD\u60C5\u51B5\u4E0B\uFF0C\u6811\u4E0D\u5408\u6CD5\u7684\u539F\u56E0\u4E5F\u662F\u51FA\u73B0\u4E86\u4E00\u4E2A\u6709\u4E24\u4E2A\u5165\u5EA6\u7684\u8282\u70B9\uFF0C**\u4F46\u4E24\u4E2A\u5165\u5EA6\u90FD\u5728\u73AF\u4E0A**\u3002\u53EF\u4EE5\u89C2\u5BDF\u5230\uFF0C\u8FD9\u4E24\u4E2A\u5165\u5EA6\u90FD\u80FD\u5220\u6389\uFF0C\u6240\u4EE5\u6309\u7167\u9898\u76EE\u8981\u6C42\u5220\u6389\u6392\u5E8F\u5728\u540E\u9762\u7684\u90A3\u6761\u3002
	
	
**\u4EE5\u4E0A\u63D0\u5230\u7684\u73AF\uFF0C\u90FD\u662F\u6307\u65E0\u5411\u73AF\uFF0C\u5373\u5047\u88C5\u8FD9\u4E2A\u56FE\u662F\u4E2A\u65E0\u5411\u56FE\u7684\u73AF**


\u60C5\u51B5\u5206\u6790\u6E05\u695A\u4E86\uFF0C\u600E\u4E48\u505A\u5462\u3002\u6211\u4EEC\u53EF\u4EE5\u4ECE\u524D\u5230\u540E\uFF0C\u5E76\u67E5\u96C6\u5408\u5E76\u6BCF\u4E00\u6761\u8FB9\uFF0C\u5BF9\u4E8E\u60C5\u51B52\u30013\uFF0C\u6211\u4EEC\u4F1A\u5728\u8FD9\u4E2A\u8FC7\u7A0B\u4E2D\u78B0\u5230\u4E00\u6761\u8FB9\u5BFC\u81F4\u67D0\u4E2A\u70B9\u7684\u5165\u5EA6\u4ECE1\u53D8\u4E3A2\uFF0C\u4E5F\u5C31\u662F\u90A3\u4E2A\u6709\u4E24\u4E2A\u5165\u5EA6\u7684\u70B9\u7684\u6392\u5728\u540E\u9762\u7684\u90A3\u4E2A\u5165\u5EA6\u3002\u6211\u4EEC\u5148\u628A\u8FD9\u4E24\u6761\u8FB9edge1\uFF0Cedge2\u8BB0\u4E0B\u6765\uFF0C**\u5982\u679C\u6211\u4EEC\u8DF3\u8FC7\u5E76\u67E5\u96C6\u5408\u5E76edge2**\uFF0C\u90A3\u4E48\u5BF9\u4E8E\u60C5\u51B52\u30013\u4F1A\u51FA\u73B0\u4E0D\u540C\u7684\u7ED3\u679C\u3002
* \u60C5\u51B53\u4F1A\u53D1\u73B0\u5408\u5E76\u5230\u6700\u540E\u4E00\u8DEF\u7545\u901A\uFF0C\u4EC0\u4E48\u4E8B\u60C5\u90FD\u4E0D\u4F1A\u53D1\u751F\uFF0C**\u56E0\u6B64\u8DF3\u8FC7\u8FD9\u6761edge2\u5C31\u662F\u7B54\u6848**\u3002 
* \u60C5\u51B52\u4E2D\uFF0C**\u5982\u679C\u8DF3\u8FC7\u7684\u8FD9\u6761edge2\u662F\u5728\u73AF\u91CC\u7684\u90A3\u6761\u8FB9\uFF0C\u5219\u4E5F\u4F1A\u4E00\u8DEF\u7545\u901A\uFF0C\u90A3\u7B54\u6848\u5C31\u662F edge2**, \u5982\u679C\u8DF3\u8FC7\u7684\u8FD9\u6761edge2\u662F\u4E0D\u5728\u73AF\u91CC\u7684\u90A3\u6761\u8FB9\uFF0C**\u90A3\u4E48\u63A5\u4E0B\u6765\u5E76\u67E5\u96C6\u5408\u5E76\u7684\u65F6\u5019\u4E00\u5B9A\u4F1A\u78B0\u5230\u73AF\uFF01\u90A3\u4E48\u7B54\u6848\u5C31\u662F edge1\u3002
* **\u5982\u679C\u5408\u5E76\u7684\u8FC7\u7A0B\u4E2D\u538B\u6839\u6CA1\u78B0\u5230\u8FC7\u4E00\u6761\u8FB9\u4F1A\u5BFC\u81F4\u53CC\u5165\u5EA6\u70B9\uFF0C\u90A3\u4E48\u5C31\u662F\u60C5\u51B51\uFF0C\u8FD9\u79CD\u60C5\u51B5\u4E0B\uFF0C**\u5408\u5E76\u8FC7\u7A0B\u4E2D\u4F1A\u78B0\u5230\u4E00\u6761\u8FB9\u5BFC\u81F4\u73AF\u7684\u51FA\u73B0\uFF0C\uFF08\u4E00\u4E2A\u73AF\uFF0C\u5728\u6700\u540E\u4E00\u6761\u8FB9\u51FA\u73B0\u4E4B\u524D\u90FD\u4E0D\u662F\u4E2A\u73AF\uFF09\u56E0\u6B64\u7684\u8FD9\u6761\u8FB9\u5C31\u662F\u60C5\u51B51\u7684\u7B54\u6848\u3002**

\u8FD9\u91CC\u662F\u6211\u7684\u7B54\u6848\uFF0C\u5E76\u67E5\u96C6\u8DEF\u5F84\u538B\u7F29\u4E86\uFF0C\u4E0D\u8FC7\u6CA1\u6709\u6309\u79E9\u5408\u5E76\uFF0C\u65F6\u95F4\u662F100%\u3002
```
class Solution {  
    int[] anc;//\u5E76\u67E5\u96C6
    int[] parent;// record the father of every node to find the one with 2 fathers,\u8BB0\u5F55\u6BCF\u4E2A\u70B9\u7684\u7236\u4EB2\uFF0C\u4E3A\u4E86\u627E\u5230\u53CC\u5165\u5EA6\u70B9
    public int[] findRedundantDirectedConnection(int[][] edges) {
        anc=new int[edges.length+1];
        parent=new int[edges.length+1];
        int[] edge1=null;
        int[] edge2=null;
        int[] lastEdgeCauseCircle=null;
        for (int[] pair:edges){
            int u=pair[0];
            int v=pair[1];
            
            if(anc[u]==0) anc[u]=u;
            if(anc[v]==0) anc[v]=v;//init the union-find set  \u521D\u59CB\u5316\u5E76\u67E5\u96C6
                
            if (parent[v]!=0){// node v already has a father, so we just skip the union of this edge and check if there will be a circle \uFF0C\u8DF3\u8FC7 edge2,\u5E76\u8BB0\u4E0B edge1,edge2
                edge1=new int[]{parent[v],v};
                edge2=pair;
            } else {
                parent[v]=u;
                int ancU=find(u);
                int ancV=find(v);
                if(ancU!=ancV){
                    anc[ancV]=ancU;
                } else { //meet a circle , \u78B0\u5230\u4E86\u73AF
                    lastEdgeCauseCircle=pair;
                }
            }
        }
        if (edge1!=null&&edge2!=null) return lastEdgeCauseCircle==null?edge2:edge1; //\u5982\u679C\u662F\u60C5\u51B52\u30013\uFF0C\u5219\u6839\u636E\u6709\u6CA1\u6709\u78B0\u5230\u73AF\u8FD4\u56DE edge1 \u6216 edge2
        else return lastEdgeCauseCircle; //\u5426\u5219\u5C31\u662F\u60C5\u51B51\uFF0C\u8FD4\u56DE\u90A3\u4E2A\u5BFC\u81F4\u73AF\u7684\u6700\u540E\u51FA\u73B0\u7684\u8FB9\u3002
    }
     
    private int find(int node){
        if (anc[node]==node) return node;
        anc[node]=find(anc[node]);
        return anc[node];
    }
}
```
</p>


