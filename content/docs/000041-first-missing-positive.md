---
title: "First Missing Positive"
weight: 41
#id: "first-missing-positive"
---
## Description
<div class="description">
<p>Given an unsorted integer array, find the smallest missing&nbsp;positive integer.</p>

<p><strong>Example 1:</strong></p>

<pre>
Input: [1,2,0]
Output: 3
</pre>

<p><strong>Example 2:</strong></p>

<pre>
Input: [3,4,-1,1]
Output: 2
</pre>

<p><strong>Example 3:</strong></p>

<pre>
Input: [7,8,9,11,12]
Output: 1
</pre>

<p><strong>Follow up:</strong></p>

<p>Your algorithm should run in <em>O</em>(<em>n</em>) time and uses constant extra space.</p>

</div>

## Tags
- Array (array)

## Companies
- Amazon - 11 (taggedByAdmin: false)
- Microsoft - 8 (taggedByAdmin: false)
- Databricks - 6 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Tesla - 3 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Oracle - 4 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Salesforce - 3 (taggedByAdmin: false)
- Morgan Stanley - 2 (taggedByAdmin: false)
- Walmart Labs - 3 (taggedByAdmin: false)
- Wish - 3 (taggedByAdmin: false)
- Pocket Gems - 2 (taggedByAdmin: false)
- Grab - 2 (taggedByAdmin: false)
- Twitch - 2 (taggedByAdmin: false)
- Wayfair - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Index as a hash key.

**Data clean up**

First of all let's get rid of negative numbers and zeros since there is no
need of them. One could get rid of all numbers larger than `n` as well,
since the first missing positive is for sure smaller or equal to `n + 1`.
The case when the first missing positive is equal to `n + 1` will be
treated separately.  

![max_first](../Figures/41/41_max_possible_first.png)

What does it mean - to get rid of, if one has to keep $$\mathcal{O}(N)$$
time complexity and hence could not pop unwanted elements out? 
Let's just replace all these by `1`s. 

![max_first](../Figures/41/41_replace.png)

To ensure that the first missing positive is not `1`, one has to verify 
the presence of `1` before proceeding to this operation.

**How to solve in-place**

Now there we have an array which contains only positive numbers
in a range from `1` to `n`,
and the problem is to find a first missing positive in 
$$\mathcal{O}(N)$$ time and constant space. 

That would be simple, if one would be allowed to 
have a hash-map `positive number -> its presence` for the array.

![max_first](../Figures/41/41_missing.png)

Sort of "dirty workaround" solution would be to allocate a string `hash_str` 
with `n` zeros, and use it as a sort of hash map by changing 
`hash_str[i]` to `1` each time one meets number `i` in the array. 

![max_first](../Figures/41/41_string.png)

Let's not use this solution, but just take away a pretty nice idea _to use 
index as a hash-key_ for a positive number.

The final idea is to _use index in nums as a hash key_ and _sign of 
the element as a hash value_ which is presence detector.

> For example, negative sign of `nums[2]` element means that 
number `2` is present in `nums`. The positive sign of `nums[3]` element
means that number `3` is not present (missing) in `nums`.

To achieve that let's walk along the array (which after clean up contains 
only positive numbers), check each element value `elem` 
and change the sign of element `nums[elem]` to negative to mark 
that number `elem` is present in `nums`. Be careful 
with duplicates and ensure that the sign was changed only once.

![max_first](../Figures/41/41_true_solution.png)

**Algorithm**

Now everything is ready to write down the algorithm.

* Check if `1` is present in the array. If not, you're done and `1`
is the answer.
* If `nums = [1]`, the answer is `2`.
* Replace negative numbers, zeros, and numbers larger than `n` by `1`s.
* Walk along the array. Change the sign of a-th element if you meet number `a`. 
Be careful with duplicates : do sign change only once.
Use index `0` to save an information about presence of number `n` since 
index `n` is not available.
* Walk again along the array. Return the index of the first positive element.
* If `nums[0] > 0` return `n`.
* If on the previous step you didn't find the positive element in nums, that means
that the answer is `n + 1`.

**Implementation**

!?!../Documents/41_LIS.json:1000,589!?!

<iframe src="https://leetcode.com/playground/ZX67Hqs4/shared" frameBorder="0" width="100%" height="500" name="ZX67Hqs4"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since all we do here is four walks 
along the array of length `N`.
* Space complexity : $$\mathcal{O}(1)$$ since this is a constant space solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My short c++ solution, O(1) space, and O(n) time
- Author: makuiyu
- Creation Date: Mon Feb 02 2015 10:26:32 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 03:46:49 GMT+0800 (Singapore Standard Time)

<p>

Put each number in its right place.

For example:

When we find 5, then swap it with A[4].

At last, the first place where its number is not right, return the place + 1.

    class Solution
    {
    public:
        int firstMissingPositive(int A[], int n)
        {
            for(int i = 0; i < n; ++ i)
                while(A[i] > 0 && A[i] <= n && A[A[i] - 1] != A[i])
                    swap(A[i], A[A[i] - 1]);
            
            for(int i = 0; i < n; ++ i)
                if(A[i] != i + 1)
                    return i + 1;
            
            return n + 1;
        }
    };
</p>


### Python O(1) space,  O(n) time solution with explanation
- Author: asones
- Creation Date: Tue May 17 2016 22:20:58 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 02:50:15 GMT+0800 (Singapore Standard Time)

<p>
     def firstMissingPositive(self, nums):
        """
        :type nums: List[int]
        :rtype: int
         Basic idea:
        1. for any array whose length is l, the first missing positive must be in range [1,...,l+1], 
            so we only have to care about those elements in this range and remove the rest.
        2. we can use the array index as the hash to restore the frequency of each number within 
             the range [1,...,l+1] 
        """
        nums.append(0)
        n = len(nums)
        for i in range(len(nums)): #delete those useless elements
            if nums[i]<0 or nums[i]>=n:
                nums[i]=0
        for i in range(len(nums)): #use the index as the hash to record the frequency of each number
            nums[nums[i]%n]+=n
        for i in range(1,len(nums)):
            if nums[i]/n==0:
                return i
        return n
</p>


### Java - simple solution - with documentation
- Author: mor3
- Creation Date: Mon May 30 2016 04:40:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 22 2018 16:56:27 GMT+0800 (Singapore Standard Time)

<p>
This code takes advantage of two insights: 

 1. Numbers greater then n can be ignored because the missing integer must be in the range 1..n+1
 2. If each cell in the array were to contain positive integers only, we can use the negative of the stored number as a flag to mark something (in this case the flag indicates this index was found in some cell of the array)

        public class Solution {
        public int firstMissingPositive(int[] nums) {
            int n = nums.length;
            
            // 1. mark numbers (num < 0) and (num > n) with a special marker number (n+1) 
            // (we can ignore those because if all number are > n then we'll simply return 1)
            for (int i = 0; i < n; i++) {
                if (nums[i] <= 0 || nums[i] > n) {
                    nums[i] = n + 1;
                }
            }
            // note: all number in the array are now positive, and on the range 1..n+1
            
            // 2. mark each cell appearing in the array, by converting the index for that number to negative
            for (int i = 0; i < n; i++) {
                int num = Math.abs(nums[i]);
                if (num > n) {
                    continue;
                }
                num--; // -1 for zero index based array (so the number 1 will be at pos 0)
                if (nums[num] > 0) { // prevents double negative operations
                    nums[num] = -1 * nums[num];
                }
            }
            
            // 3. find the first cell which isn't negative (doesn't appear in the array)
            for (int i = 0; i < n; i++) {
                if (nums[i] >= 0) {
                    return i + 1;
                }
            }
            
            // 4. no positive numbers were found, which means the array contains all numbers 1..n
            return n + 1;
        }
        }
</p>


