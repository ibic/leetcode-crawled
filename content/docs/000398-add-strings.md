---
title: "Add Strings"
weight: 398
#id: "add-strings"
---
## Description
<div class="description">
<p>Given two non-negative integers <code>num1</code> and <code>num2</code> represented as string, return the sum of <code>num1</code> and <code>num2</code>.</p>

<p><b>Note:</b>
<ol>
<li>The length of both <code>num1</code> and <code>num2</code> is < 5100.</li>
<li>Both <code>num1</code> and <code>num2</code> contains only digits <code>0-9</code>.</li>
<li>Both <code>num1</code> and <code>num2</code> does not contain any leading zero.</li>
<li>You <b>must not use any built-in BigInteger library</b> or <b>convert the inputs to integer</b> directly.</li>
</ol>
</p>
</div>

## Tags
- String (string)

## Companies
- Facebook - 65 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)
- Square - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Oracle - 10 (taggedByAdmin: false)
- Snapchat - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---

#### Overview

Facebook interviewers like this question and propose it in four main variations.
The choice of algorithm should be based on the input format:

1. Strings (the current problem). 
Use schoolbook digit-by-digit addition. 
Note, that to fit into constant space is not possible for 
languages with immutable strings, for example, for Java and Python.
Here are two examples:

    - [Add Binary](https://leetcode.com/articles/add-binary/): sum
    two binary strings.  
    
    - [Add Strings](https://leetcode.com/problems/add-strings/): sum two non-negative numbers 
    in a string representation without converting them to integers directly.

2. Integers. 
Usually, the interviewer would ask you to implement a sum without 
using `+` and `-` operators.
Use bit manipulation approach. 
Here is an example: 

    - [Sum of Two Integers](https://leetcode.com/articles/sum-of-two-integers/): 
    Sum two integers without using `+` and `-` operators.
        
3. Arrays. 
The same textbook addition.
Here is an example: 

    - [Add to Array Form of Integer](https://leetcode.com/articles/add-to-array-form-of-integer/).

4. Linked Lists. 
Sentinel Head + Textbook Addition. 
Here are some examples:
    
    - [Plus One](https://leetcode.com/articles/plus-one/).
    
    - [Add Two Numbers](https://leetcode.com/articles/add-two-numbers/).
    
    - [Add Two Numbers II](https://leetcode.com/problems/add-two-numbers-ii/).
    
<br />
<br />


---
#### Approach 1: Elementary Math

Here we have two strings as input and asked not to convert them to
integers. Digit-by-digit addition is the only option here.

!?!../Documents/415_LIS.json:1000,402!?!

**Algorithm**

- Initialize an empty `res` structure. Once could use array in Python and 
StringBuilder in Java.

- Start from `carry = 0`. 

- Set a pointer at the end of each string:
`p1 = num1.length() - 1`, `p2 = num2.length() - 1`.

- Loop over the strings from the end to the beginning using `p1` and `p2`. 
Stop when both strings are used entirely.

    - Set `x1` to be equal to a digit from string `nums1` at index `p1`. If 
    `p1` has reached the beginning of `nums1`, set `x1` to `0`.
    
    - Do the same for `x2`. Set `x2` to be equal to digit from string 
    `nums2` at index `p2`. If `p2` has reached the beginning of `nums2`, set `x2` to `0`.

    - Compute the current value: `value = (x1 + x2 + carry) % 10`, and update the carry:
    `carry = (x1 + x2 + carry) / 10`.
    
    - Append the current value to the result: `res.append(value)`.
    
- Now both strings are done. If the carry is still non-zero, update the result:
`res.append(carry)`. 
    
- Reverse the result, convert it to a string, and return that string.

**Implementation**

<iframe src="https://leetcode.com/playground/WEFSCAgo/shared" frameBorder="0" width="100%" height="446" name="WEFSCAgo"></iframe>

**Complexity Analysis**

* Time Complexity: $$\mathcal{O}(\max(N_1, N_2))$$, where $$N_1$$ and $$N_2$$
are length of `nums1` and `nums2`. Here we do $$\max(N_1, N_2)$$
iterations at most.

* Space Complexity: $$\mathcal{O}(\max(N_1, N_2))$$, because the length
of the new string is at most $$\max(N_1, N_2) + 1$$.

<br />
<br />


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Straightforward Java 8 main lines 25ms
- Author: ZachC
- Creation Date: Sun Oct 09 2016 14:16:06 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 01:09:02 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public String addStrings(String num1, String num2) {
        StringBuilder sb = new StringBuilder();
        int carry = 0;
        for(int i = num1.length() - 1, j = num2.length() - 1; i >= 0 || j >= 0 || carry == 1; i--, j--){
            int x = i < 0 ? 0 : num1.charAt(i) - '0';
            int y = j < 0 ? 0 : num2.charAt(j) - '0';
            sb.append((x + y + carry) % 10);
            carry = (x + y + carry) / 10;
        }
        return sb.reverse().toString();
    }
}
```
</p>


### straightforward python solution
- Author: rarezhang
- Creation Date: Sat Mar 04 2017 00:48:51 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 10:34:09 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def addStrings(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        num1, num2 = list(num1), list(num2)
        carry, res = 0, []
        while len(num2) > 0 or len(num1) > 0:
            n1 = ord(num1.pop())-ord('0') if len(num1) > 0 else 0
            n2 = ord(num2.pop())-ord('0') if len(num2) > 0 else 0
            
            temp = n1 + n2 + carry 
            res.append(temp % 10)
            carry = temp // 10
        if carry: res.append(carry)
        return ''.join([str(i) for i in res])[::-1]
```
</p>


### C++_Accepted_13ms
- Author: jasonshieh
- Creation Date: Sun Oct 09 2016 14:00:55 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 12:20:49 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
    string addStrings(string num1, string num2) {
        int i = num1.size() - 1;
        int j = num2.size() - 1;
        int carry = 0;
        string res = "";
        while(i>=0 || j>=0 || carry){
            long sum = 0;
            if(i >= 0){sum += (num1[i] - '0');i--;}
            if(j >= 0){sum += (num2[j] - '0');j--;}
            sum += carry; 
            carry = sum / 10;
            sum = sum % 10;
            res =  res + to_string(sum);
        }
        reverse(res.begin(), res.end());
        return res;
    }
    };
</p>


