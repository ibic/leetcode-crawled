---
title: "Rectangles Area"
weight: 1581
#id: "rectangles-area"
---
## Description
<div class="description">
<p>Table: <code>Points</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| x_value       | int     |
| y_value       | int     |
+---------------+---------+
id is the primary key for this table.
Each point is represented as a 2D Dimensional (x_value, y_value).</pre>

<p>Write an SQL query to&nbsp;report&nbsp;of all possible rectangles which can be formed by any two points of the table.&nbsp;</p>

<p>Each row in the result contains three columns (p1, p2, area) where:</p>

<ul>
	<li><strong>p1</strong> and <strong>p2</strong> are the id of&nbsp;two opposite corners of a rectangle and p1 &lt; p2.</li>
	<li>Area of this rectangle is represented by the column <strong>area</strong>.</li>
</ul>

<p>Report the query in&nbsp;descending&nbsp;order by area in case of tie in ascending order by p1 and p2.</p>

<pre>
Points table:
+----------+-------------+-------------+
| id       | x_value     | y_value     |
+----------+-------------+-------------+
| 1        | 2           | 8           |
| 2        | 4           | 7           |
| 3        | 2           | 10          |
+----------+-------------+-------------+

Result table:
+----------+-------------+-------------+
| p1       | p2          | area        |
+----------+-------------+-------------+
| 2        | 3           | 6           |
| 1        | 2           | 2           |
+----------+-------------+-------------+

p1 should be less than p2 and area greater than 0.
p1 = 1 and p2 = 2, has an area equal to |2-4| * |8-7| = 2.
p1 = 2 and p2 = 3, has an area equal to |4-2| * |7-10| = 6.
p1 = 1 and p2 = 3 It&#39;s not possible because the rectangle has an area equal to 0.
</pre>

</div>

## Tags


## Companies
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple MySQL solution
- Author: Caspar-Chen-hku
- Creation Date: Thu Jun 04 2020 14:03:13 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 04 2020 14:21:05 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT  pt1.id as P1, pt2.id as P2,
		ABS(pt2.x_value - pt1.x_value)*ABS(pt2.y_value-pt1.y_value) as AREA
FROM Points pt1 JOIN Points pt2 
ON pt1.id<pt2.id
AND pt1.x_value!=pt2.x_value 
AND pt2.y_value!=pt1.y_value
ORDER BY AREA DESC, p1 ASC, p2 ASC;
```
</p>


### [MySQL] The simplest solution with explanation
- Author: aleksey12345
- Creation Date: Wed Jun 24 2020 20:40:14 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 24 2020 20:40:14 GMT+0800 (Singapore Standard Time)

<p>
The idea of this solution is to join table on itself using condition t1.id < t2.id

```
select * from(
select t1.id as P1, t2.id as P2, abs((t1.x_value - t2.x_value) * (t1.y_value - t2.y_value)) as AREA
from points t1 join points t2 on t1.id < t2.id) t where AREA <> 0
order by AREA desc, P1 asc, P2 asc 
```
</p>


### Simple 4 Line MySQL Faster than 100%
- Author: quintile
- Creation Date: Wed Sep 02 2020 14:12:29 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 02 2020 14:12:29 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT t1.id p1, t2.id p2, ABS(t1.x_value-t2.x_value)*ABS(t1.y_value-t2.y_value) area
FROM Points t1, Points t2
WHERE t1.id <> t2.id AND t1.x_value <> t2.x_value AND t1.y_value <> t2.y_value AND t1.id < t2.id
ORDER BY 3 DESC, p1, p2
```
</p>


