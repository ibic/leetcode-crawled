---
title: "Nth Magical Number"
weight: 824
#id: "nth-magical-number"
---
## Description
<div class="description">
<p>A positive integer&nbsp;is <em>magical</em>&nbsp;if it is divisible by either <font face="monospace">A</font>&nbsp;or <font face="monospace">B</font>.</p>

<p>Return the <font face="monospace">N</font>-th magical number.&nbsp; Since the answer may be very large, <strong>return it modulo </strong><code>10^9 + 7</code>.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>N = <span id="example-input-1-1">1</span>, A = <span id="example-input-1-2">2</span>, B = <span id="example-input-1-3">3</span>
<strong>Output: </strong><span id="example-output-1">2</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>N = <span id="example-input-2-1">4</span>, A = <span id="example-input-2-2">2</span>, B = <span id="example-input-2-3">3</span>
<strong>Output: </strong><span id="example-output-2">6</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>N = <span id="example-input-3-1">5</span>, A = <span id="example-input-3-2">2</span>, B = <span id="example-input-3-3">4</span>
<strong>Output: </strong><span id="example-output-3">10</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong>N = <span id="example-input-4-1">3</span>, A = <span id="example-input-4-2">6</span>, B = <span id="example-input-4-3">4</span>
<strong>Output: </strong><span id="example-output-4">8</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= N&nbsp;&lt;= 10^9</code></li>
	<li><code>2 &lt;= A&nbsp;&lt;= 40000</code></li>
	<li><code>2 &lt;= B&nbsp;&lt;= 40000</code></li>
</ol>
</div>
</div>
</div>
</div>

</div>

## Tags
- Math (math)
- Binary Search (binary-search)

## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach 1: Mathematical

**Intuition and Algorithm**

Let's try to count to the $$N$$-th magical number mathematically.

First, the pattern of magical numbers repeats.  Let $$L$$ be the least common multiple of $$A$$ and $$B$$.  If $$X \leq L$$ is magical, then $$X + L$$ is magical, because (for example) $$A \| X$$ and $$A \| L$$ implies $$A \| (X + L)$$, and similarly if $$B$$ were the divisor.

There are $$M = \frac{L}{A} + \frac{L}{B} - 1$$ magical numbers less than or equal to $$L$$: $$\frac{L}{A}$$ of them are divisible by $$A$$, $$\frac{L}{B}$$ of them are divisible by $$B$$, and $$1$$ of them is divisible by both.  So instead of counting one at a time, we can count by $$M$$ at a time.

Now, suppose $$N = M*q + r$$ (with $$r < M$$).  The first $$L*q$$ numbers contain $$M*q$$ magical numbers, and within the next numbers $$(L*q + 1, L*q + 2, \cdots)$$ we want to find $$r$$ more magical ones.

For this task, we can use brute force.  The next magical number (less $$L*q$$) will either be $$A$$ or $$B$$.  If for example it is $$A$$, then the next number will either be $$2*A$$ or $$B$$, and so on.

If the $$r$$-th such magical number is $$Y$$, then the final answer is $$L*q + Y$$.  Care must also be taken in the case that $$r$$ is $$0$$.

<iframe src="https://leetcode.com/playground/PDpwCrrN/shared" frameBorder="0" width="100%" height="500" name="PDpwCrrN"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(A+B)$$, assuming a model where integer math operations are $$O(1)$$.  The calculation of `q * L` is $$O(1)$$.  The calculation of the $$r$$-th magical number after $$q*M$$ is $$O(M)$$ which is $$O(A+B)$$.

* Space Complexity:  $$O(1)$$.
<br />
<br />


---
#### Approach 2: Binary Search

**Intuition**

The number of magical numbers less than or equal to $$x$$ is a monotone increasing function in $$x$$, so we can binary search for the answer.

**Algorithm**

Say $$L = \text{lcm}(A, B)$$, the *least common multiple* of $$A$$ and $$B$$; and let $$f(x)$$ be the number of magical numbers less than or equal to $$x$$.  A well known result says that $$L = \frac{A * B}{\text{gcd}(A, B)}$$, and that we can calculate the function $$\gcd$$.  For more information on least common multiples and greatest common divisors, please visit [Wikipedia - Lowest Common Multiple](https://en.wikipedia.org/wiki/Least_common_multiple).

Then $$f(x) = \lfloor \frac{x}{A} \rfloor + \lfloor \frac{x}{B} \rfloor - \lfloor \frac{x}{L} \rfloor$$.  Why?  There are $$\lfloor \frac{x}{A} \rfloor$$ numbers $$A,  2A,  3A,  \cdots$$ that are divisible by $$A$$, there are $$\lfloor \frac{x}{B} \rfloor$$ numbers divisible by $$B$$, and we need to subtract the $$\lfloor \frac{x}{L} \rfloor$$ numbers divisible by $$A$$ and $$B$$ that we double counted.

Finally, the answer must be between $$0$$ and $$N * \min(A, B)$$.  
Without loss of generality, suppose $$A \geq B$$, so that it remains to show

$$
\lfloor \frac{N * \min(A, B)}{A} \rfloor + \lfloor \frac{N * \min(A, B)}{B} \rfloor - \lfloor \frac{N * \min(A, B)}{\text{lcm}(A, B)} \rfloor \geq N
$$

$$
\Leftrightarrow \lfloor \frac{N*A}{A} \rfloor + \lfloor \frac{N*A}{B} \rfloor - \lfloor \frac{N*A*\gcd(A, B)}{A*B} \rfloor \geq N
$$

$$
\Leftrightarrow \lfloor \frac{N*A}{B} \rfloor \geq \lfloor \frac{N*\gcd(A, B)}{B} \rfloor
$$

$$
\Leftrightarrow A \geq \gcd(A, B)
$$

as desired.

Afterwards, the binary search on $$f$$ is straightforward.  For more information on binary search, please visit [[LeetCode Explore - Binary Search]](https://leetcode.com/explore/learn/card/binary-search/).

<iframe src="https://leetcode.com/playground/bSyf8JE2/shared" frameBorder="0" width="100%" height="480" name="bSyf8JE2"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\log (N * \min(A, B)))$$.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Binary Search
- Author: lee215
- Creation Date: Sun Jul 29 2018 11:03:25 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 22:58:53 GMT+0800 (Singapore Standard Time)

<p>
4 points to figure out:

1. Get gcd (greatest common divisor) and lcm (least common multiple) of (A, B).
`
(a, b) = (A, B)
while b > 0:
    (a, b) = (b, a % b)
`
then, gcd = a and lcm = A * B / a

2. How many magic numbers `<= x` ?
By inclusion exclusion principle, we have:
`x / A + x / B - x / lcm`

3. Set our binary search range
Lower bound is `min(A, B)`, I **just** set `left = 2`.
Upper bound is `N * min(A, B)`, I **just** set `right = 10 ^ 14`.

4. binary search, find the smallest `x` that `x / A + x / B - x / lcm = N`
```
while (l < r) {
    m = (l + r) / 2;
    if (m / A + m / B - m / (A * B / a) < N) // m too small
        l = m + 1;
    else // m may be too big
        r = m;
}
```

**Time Complexity**:
`O(log(10**14))`


**C++:**
```
    int nthMagicalNumber(int N, int A, int B) {
        long lcm = A * B / __gcd(A, B), l = 2, r = 1e14, mod = 1e9 + 7;
        while (l < r) {
            long m = (l + r) / 2;
            if (m / A + m / B - m / lcm < N) l = m + 1;
            else r = m;
        }
        return l % mod;
    }
```

**Java:**
```
    public int nthMagicalNumber(int N, int A, int B) {
        long a = A, b = B, tmp, l = 2, r = (long)1e14, mod = (long)1e9 + 7;
        while (b > 0) {
            tmp = a;
            a = b;
            b = tmp % b;
        }
        while (l < r) {
            long m = (l + r) / 2;
            if (m / A + m / B - m / (A * B / a) < N) l = m + 1;
            else r = m;
        }
        return (int)(l % mod);
    }
```
**Python:**
```
    def nthMagicalNumber(self, N, A, B):
        a, b = A, B
        while b: a, b = b, a % b
        l, r, lcm = 2, 10**14, A * B / a
        while l < r:
            m = (l + r) / 2
            if m / A + m / B - m / lcm < N: l = m + 1
            else: r = m
        return l % (10**9 + 7)
```

</p>


### Python using gcd and lcm, no search needed
- Author: wnmmxy
- Creation Date: Sun Jul 29 2018 13:41:15 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 22:47:36 GMT+0800 (Singapore Standard Time)

<p>
Suppose A =2 and B = 3, then the lcm is 6. The list of magical number less or equal to 6 is [2,3,4,6]. Then, the 1st to 4th magical number to [2,3,4,6], the 5th to 8th number is 6 added to [2,3,4,6] respectively, the 9th to 12nd number is 6*2 added to [2,3,4,6] respectively, and so on.

So, the key here is to get all the magical number less or equal to the lcm of A and B. Then, the Nth number can be obtained immediately.

```python
class Solution(object):
	def gcd(self, x, y):
		while y > 0:
			x, y = y, x % y
		return x

	def lcm(self, x, y):
		return x*y//self.gcd(x,y)

	def nthMagicalNumber(self, N, A, B):
		temp = self.lcm(A,B)
		seq = {}
		for i in range(1,temp//A+1):
			seq[i*A] = 1
		for i in range(1,temp//B+1):
			seq[i*B] = 1
		cand = sorted([key for key, value in seq.items()])
		ans = ((N-1)//len(cand))*cand[-1] + cand[N%len(cand)-1]
		return ans % (10**9+7)
```
</p>


### o(1) Mathematical Solution without binary or brute force search
- Author: jianwu
- Creation Date: Mon Jul 30 2018 11:47:24 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 20 2018 03:36:42 GMT+0800 (Singapore Standard Time)

<p>
I have seen most of the solutions contain binary search or brute force search. Actually we can solve it with pure math solution. 
First we need to find count of the LCM blocks as each LCM block contains fixed number of magic numbers. For the remain part, instead of using brute force or binary search. Actually there\'s linear relationship between count of magic number (F(x)) and the number (x) in following formular: 
`f(x) = floor(x/A) + floor(x/B) `
As within an LCD block, there\'s no overlapping between x/A and x/B. 

If we plot this in a chart, it\'s very close to linear furmular:
`f(x) = x/A + x/B. `
But it\'s always below this line.
following is the chart exmple for A=3, B=5
![image](https://s3-lc-upload.s3.amazonaws.com/users/jianwu/image_1532925395.png)


So solution to get the number within an LCM block is very simple: 
The minimum integer number great than:   N / ( 1/A + 1 /B ), that can be divided either by A or B.

Following is the code passes the test:
```
import static java.lang.Math.ceil;
import static java.lang.Math.min;

class Solution {
 public int nthMagicalNumber(int N, int A, int B) {
    int MOD = 1_000_000_007;
    long lcm = A * B / gcd(A, B);
    long cntPerLcm = lcm / A + lcm / B - 1;
    long cntLcm = N / cntPerLcm;
    long remain = N % cntPerLcm;
    
    double nearest = remain / (1./A + 1./B);
    int remainIdx = (int)min(ceil(nearest / A) * A, ceil(nearest / B) * B);
    return (int)((cntLcm * lcm + remainIdx) % MOD);
  }
  
  public static int gcd(int A, int B) {
    return B == 0 ? A : gcd(B, A % B);
  }
}
```



</p>


