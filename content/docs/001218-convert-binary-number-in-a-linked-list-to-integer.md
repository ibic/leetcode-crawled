---
title: "Convert Binary Number in a Linked List to Integer"
weight: 1218
#id: "convert-binary-number-in-a-linked-list-to-integer"
---
## Description
<div class="description">
<p>Given <code>head</code> which is a reference node to&nbsp;a singly-linked list. The value of each node in the linked list is either 0 or 1. The linked list holds the binary representation of a number.</p>

<p>Return the <em>decimal value</em> of the number in the linked list.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/12/05/graph-1.png" style="width: 426px; height: 108px;" />
<pre>
<strong>Input:</strong> head = [1,0,1]
<strong>Output:</strong> 5
<strong>Explanation:</strong> (101) in base 2 = (5) in base 10
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> head = [0]
<strong>Output:</strong> 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> head = [1]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> head = [1,0,0,1,0,0,1,1,1,0,0,0,0,0,0]
<strong>Output:</strong> 18880
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> head = [0,0]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The Linked List is not empty.</li>
	<li>Number of nodes&nbsp;will not exceed <code>30</code>.</li>
	<li>Each node&#39;s value is either&nbsp;<code>0</code> or <code>1</code>.</li>
</ul>
</div>

## Tags
- Linked List (linked-list)
- Bit Manipulation (bit-manipulation)

## Companies
- Mathworks - 9 (taggedByAdmin: false)
- Cisco - 4 (taggedByAdmin: false)
- Roblox - 3 (taggedByAdmin: true)
- ServiceNow - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Overview

Here we have two subproblems:

- To parse non-empty linked list and to retrieve the digit sequence which represents 
a binary number.

- To convert this sequence into the number in decimal representation. 

The first subproblem is easy because the linked list is guaranteed to be 
non-empty.

<iframe src="https://leetcode.com/playground/UioTC7Vw/shared" frameBorder="0" width="100%" height="191" name="UioTC7Vw"></iframe>

The second subproblem is to convert $$(101)_2$$ into 
$$1 \times 2^2 + 0 \times 2^1 + 1 \times 2^0 = 5$$.
It could be solved in two ways. To use classical arithmetic
is more straightforward

![img](../Figures/1290/try1.png)
*Figure 1. Approach 1: num = num * 2 + x*
{:align="center"}

and to use bitwise operators is faster

![img](../Figures/1290/try2.png)
*Figure 2. Approach 2: num = (num << 1) | x*
{:align="center"}

<br />
<br />


---
#### Approach 1: Binary Representation

![img](../Figures/1290/try1.png)
*Figure 3. Approach 1: num = num * 2 + x.*
{:align="center"}

- Initialize result number to be equal to head value: `num = head.val`. 
This operation is safe because the list is guaranteed to be non-empty.

- Parse linked list starting from the head: `while head.next`:

    - The current value is `head.next.val`. Update the result by shifting
    it by one to the left and adding the current value:
    `num = num * 2 + head.next.val`.
    
- Return `num`.

**Implementation**

<iframe src="https://leetcode.com/playground/f36TfDa4/shared" frameBorder="0" width="100%" height="225" name="f36TfDa4"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$. 

* Space complexity: $$\mathcal{O}(1)$$. 
<br />
<br />


---
#### Approach 2: Bit Manipulation

![img](../Figures/1290/try2.png)
*Figure 4. Approach 2: num = (num << 1) | x*
{:align="center"}

- Initialize result number to be equal to head value: `num = head.val`. 
This operation is safe because the list is guaranteed to be non-empty.

- Parse linked list starting from the head: `while head.next`:

    - The current value is `head.next.val`. Update the result by shifting
    it by one to the left and adding the current value using logical OR:
    `num = (num << 1) | head.next.val`.
    
- Return `num`.

**Implementation**

<iframe src="https://leetcode.com/playground/TV9mffKX/shared" frameBorder="0" width="100%" height="225" name="TV9mffKX"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$. 

* Space complexity: $$\mathcal{O}(1)$$. 
<br />
<br />

---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Simulate binary operations.
- Author: rock
- Creation Date: Sun Dec 15 2019 12:04:12 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Mar 19 2020 00:42:50 GMT+0800 (Singapore Standard Time)

<p>
Please refer to the following excellent picture from **@YaoFrankie**.
<img src="https://yyc-images.oss-cn-beijing.aliyuncs.com/leetcode_1290.png" alt="">

----
**Q & A**
Q1.  Can u explain what this line does: `ans = (ans << 1) | head.val`?
A1. `ans` starts with `0`, left shift `ans` for `1` bit, then bitwise `or` with `head.val`. 
e.g., for test case: [0,1,0,1], the value of head is 0 -> 1 -> 0 -> 1. 

You can compare it with the following, which has same result:
```
ans = ans * 2 + head.val;
```

Google "bitwise operations" if still confused.

**end of Q & A**

----

```java
    public int getDecimalValue(ListNode head) {
        int ans = 0;
        while (head != null) {
            ans = (ans << 1) | head.val;
            head = head.next;
        }
        return ans;
    }
```
```python
    def getDecimalValue(self, head: ListNode) -> int:
        ans = 0
        while head:
            ans = (ans << 1) | head.val
            head = head.next
        return ans
```
Use `walrus` operator in Python 3.8:
```python
    def getDecimalValue(self, head: ListNode) -> int:
        ans = head.val
        while head := head.next:
            ans = ans << 1 | head.val
        return ans
```
**Analysis:**
Time: O(n), space: O(1), where n is the number of nodes.

----

For more bit shift operations practice and solution, refer to [1018. Binary Prefix Divisible By 5](https://leetcode.com/problems/binary-prefix-divisible-by-5/discuss/265554/Java-7-liner-left-shift-bitwise-or-and-mod.)
</p>


### JAVA  - EASY Solution with EXPLANATION! (MUST READ)
- Author: anubhavjindal
- Creation Date: Mon Dec 16 2019 14:31:21 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 10 2020 04:09:20 GMT+0800 (Singapore Standard Time)

<p>
We initialise an integer variable num to 0. For each node in the linked list, we will left shift num by 1 position to make way for the val in the next node in linked list. This is same as multiplying num by 2, but using the left shift operator makes it easier to visualise how the binary number is being made. For eg. if num in binary is 10, after left shift it would become 100. 

```
public int getDecimalValue(ListNode head) {
	int num = 0;                // Initialise num to 0
	while(head!=null) {         // Iteratore over the linked list until head is null    
		num <<= 1;              // Left shift num by 1 position to make way for next bit
		num += head.val;        // Add next bit to num at least significant position
		head = head.next;       // Update head
	}
	return num;
}
```
</p>


### [Python] Simple. 20ms.
- Author: rohin7
- Creation Date: Thu Dec 19 2019 09:04:42 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Dec 19 2019 09:08:25 GMT+0800 (Singapore Standard Time)

<p>
Read the binary number from MSB to LSB
```
class Solution:
    def getDecimalValue(self, head: ListNode) -> int:
        answer = 0
        while head: 
            answer = 2*answer + head.val 
            head = head.next 
        return answer 
```
</p>


