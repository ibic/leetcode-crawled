---
title: "Min Cost to Connect All Points"
weight: 1446
#id: "min-cost-to-connect-all-points"
---
## Description
<div class="description">
<p>You are given an array&nbsp;<code>points</code>&nbsp;representing integer coordinates of some points on a 2D-plane, where <code>points[i] = [x<sub>i</sub>, y<sub>i</sub>]</code>.</p>

<p>The cost of connecting two points <code>[x<sub>i</sub>, y<sub>i</sub>]</code> and <code>[x<sub>j</sub>, y<sub>j</sub>]</code> is the <strong>manhattan distance</strong> between them:&nbsp;<code>|x<sub>i</sub> - x<sub>j</sub>| + |y<sub>i</sub> - y<sub>j</sub>|</code>, where <code>|val|</code> denotes the absolute value of&nbsp;<code>val</code>.</p>

<p>Return&nbsp;<em>the minimum cost to make all points connected.</em> All points are connected if there is <strong>exactly one</strong> simple path between any two points.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/08/26/d.png" style="width: 214px; height: 268px;" /></p>

<pre>
<strong>Input:</strong> points = [[0,0],[2,2],[3,10],[5,2],[7,0]]
<strong>Output:</strong> 20
<strong>Explanation:
</strong><img alt="" src="https://assets.leetcode.com/uploads/2020/08/26/c.png" style="width: 214px; height: 268px;" />
We can connect the points as shown above to get the minimum cost of 20.
Notice that there is a unique path between every pair of points.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> points = [[3,12],[-2,5],[-4,1]]
<strong>Output:</strong> 18
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> points = [[0,0],[1,1],[1,0],[-1,1]]
<strong>Output:</strong> 4
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> points = [[-1000000,-1000000],[1000000,1000000]]
<strong>Output:</strong> 4000000
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> points = [[0,0]]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= points.length &lt;= 1000</code></li>
	<li><code>-10<sup>6</sup>&nbsp;&lt;= x<sub>i</sub>, y<sub>i</sub> &lt;= 10<sup>6</sup></code></li>
	<li>All pairs <code>(x<sub>i</sub>, y<sub>i</sub>)</code> are distinct.</li>
</ul>

</div>

## Tags
- Union Find (union-find)

## Companies
- Directi - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ MST: Kruskal + Prim's + Complete Graph
- Author: votrubac
- Creation Date: Sun Sep 13 2020 12:02:04 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 15 2020 04:08:41 GMT+0800 (Singapore Standard Time)

<p>
We can imagine that every point is a node of the graph, connected to all other points, and the lenght of the edge is the manhattan distance between two points.

To find the min cost, we therefore need to find the minimum spanning tree.

**Kruskal vs. Prim\'s**
Based on the OJ test cases, the performance of both algorithms is similar (accepted in ~500 ms), and Kruskal seems to have a small advantage (~50 ms) in avarage.

> Note: I tried to sort all edges first, but I got TLE during the contest. I think that limits for C++ are too tight. I then used a min heap, and it worked. 
> 
> For Kruskal, the complexity when using sort is O(n * n log (n * n)) - we have n * n edges. Using a min heap is O(k log (n * n)), where k is the number of edges we need to pull to complete the tree. It\'s much smaller than n * n in the average case.
> 

At the same time, Prim\'s implementation is simpler/shorter. Also, we can do an optimization for a complete graph, as suggested by [mithuntm](https://leetcode.com/mithuntm/) below. 

Check out the optiized Prim\'s impementation below- it has a blasting 80 ms runtime!!

**Kruskal**
Kruskal involves min heap to pick the smallest edge, and union-find to check if the edge is redundant. We exit when all points are connected.

```cpp
int find(vector<int> &ds, int i) {
    return ds[i] < 0 ? i : ds[i] = find(ds, ds[i]);
}
int minCostConnectPoints(vector<vector<int>>& ps) {
    int n = ps.size(), res = 0;
    vector<int> ds(n, -1);
    vector<array<int, 3>> arr;
    for (auto i = 0; i < n; ++i)
        for (auto j = i + 1; j < n; ++j) {
            arr.push_back({abs(ps[i][0] - ps[j][0]) + abs(ps[i][1] - ps[j][1]), i, j});
        }
    make_heap(begin(arr), end(arr), greater<array<int, 3>>());
    while (!arr.empty()) {
        pop_heap(begin(arr), end(arr), greater<array<int, 3>>());
        auto [dist, i, j] = arr.back();
        arr.pop_back();
        i = find(ds, i), j = find(ds, j);
        if (i != j) {
            res += dist;
            ds[i] += ds[j];
            ds[j] = i;
            if (ds[i] == -n)
                break;
        }
    }
    return res;
}
```
**Prim\'s**
In the Prim\'s algorithm, we are building a tree starting from some initial point. We track all connected points in `visited`. For the current point, we add its edges to the min heap. Then, we pick a smallest edge that connects to a point that is not `visited`. Repeat till all points are `visited`.

```cpp
int minCostConnectPoints(vector<vector<int>>& ps) {
    int n = ps.size(), res = 0, i = 0, connected = 0;
    vector<bool> visited(n);
    priority_queue<pair<int, int>> pq;
    while (++connected < n) {
        visited[i] = true;
        for (int j = 0; j < n; ++j)
            if (!visited[j])
                pq.push({-(abs(ps[i][0] - ps[j][0]) + abs(ps[i][1] - ps[j][1])), j});
        while (visited[pq.top().second])
            pq.pop();
        res -= pq.top().first;
        i = pq.top().second;
        pq.pop();
    }
    return res;
}
```
**Prim\'s for Complete Graph**
For this problem, any two points can be connected, so we are dealing with the complete graph. Thus, the number of edges is much larger (a square of) the number of points.

As suggested by [mithuntm](https://leetcode.com/mithuntm/), we can keep track of the minimal distance to each point, and update that distance as we connect more points. For each round, we need to scan all points to find the next point to connect to.

```cpp
int minCostConnectPoints(vector<vector<int>>& ps) {
    int n = ps.size(), res = 0, i = 0, connected = 0;
    vector<int> min_d(n, 10000000);
    while (++connected < n) {
        min_d[i] = INT_MAX;
        int min_j = i;
        for (int j = 0; j < n; ++j)
            if (min_d[j] != INT_MAX) {
                min_d[j] = min(min_d[j], abs(ps[i][0] - ps[j][0]) + abs(ps[i][1] - ps[j][1]));
                min_j = min_d[j] < min_d[min_j] ? j : min_j;
            }
        res += min_d[min_j];
        i = min_j;
    }
    return res;
}
```
</p>


### [Python/Golang] Just add points greedily
- Author: yanrucheng
- Creation Date: Sun Sep 13 2020 12:00:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 13 2020 12:00:49 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
The ultimate goal is to construct a tree using all the points. 
Select a random point as the base tree `T`. We greedily add the closest point to `T`.

**Complexity**
- Time: O(n ^ 2)
- Space: O(n)

**Python 3**
```
class Solution:
    def minCostConnectPoints(self, points: List[List[int]]) -> int:
        n = len(points)
        if len(points) == 1: return 0
        res = 0
        curr = 0 # select a random point as the starting point
        dis = [math.inf] * n
        explored = set()
        
        for i in range(n - 1):
            x0, y0 = points[curr]
            explored.add(curr)
            for j, (x, y) in enumerate(points):
                if j in explored: continue
                dis[j] = min(dis[j], abs(x - x0) + abs(y - y0))
                
            delta, curr = min((d, j) for j, d in enumerate(dis)) 
            dis[curr] = math.inf
            res += delta
            
        return res
```


**Golang**
```
func minCostConnectPoints(points [][]int) int {
    n := len(points)
    dis := make([]int, n)
    curr, res := 0, 0
    set := make(map[int]bool)
    
    for i := range dis {
        dis[i] = math.MaxInt64
    }
    
    for i := 0; i < n - 1; i++ {
        set[curr] = true
        dis[curr] = math.MaxInt64
        x, y := points[curr][0], points[curr][1]
        
        for j := 0; j < n; j++ {
            if _, ok := set[j]; ok {
                continue
            }
            x_, y_ := points[j][0], points[j][1]
            new_d := abs(x - x_) + abs(y - y_)
            if dis[j] > new_d {
                dis[j] = new_d
            }
        }
        
        min_d, nearest_ele := math.MaxInt64, -1
        for i, d := range dis {
            if d < min_d {
                min_d = d
                nearest_ele = i
            }
        }
        res += min_d
        curr = nearest_ele
    }
    return res
}

func abs(a int) int {
    if a < 0 {
        return -a
    }
    return a
}
```
</p>


### Stupid OJ?
- Author: richarddia
- Creation Date: Sun Sep 13 2020 12:15:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 13 2020 12:15:05 GMT+0800 (Singapore Standard Time)

<p>
I got TLE for MST (kruskal) in c++...

</p>


