---
title: "Super Egg Drop"
weight: 837
#id: "super-egg-drop"
---
## Description
<div class="description">
<p>You are given <code>K</code> eggs, and you have access to a building with <code>N</code> floors from <code>1</code> to <code>N</code>.&nbsp;</p>

<p>Each egg is identical in function, and if an egg breaks, you cannot drop it&nbsp;again.</p>

<p>You know that there exists a floor <code>F</code> with <code>0 &lt;= F &lt;= N</code> such that any egg dropped at a floor higher than <code>F</code> will break, and any egg dropped at or below floor <code>F</code> will not break.</p>

<p>Each <em>move</em>, you may take an egg (if you have an unbroken one) and drop it from any floor <code>X</code> (with&nbsp;<code>1 &lt;= X &lt;= N</code>).&nbsp;</p>

<p>Your goal is to know&nbsp;<strong>with certainty</strong>&nbsp;what the value of <code>F</code> is.</p>

<p>What is the minimum number of moves that you need to know with certainty&nbsp;what <code>F</code> is, regardless of the initial value of <code>F</code>?</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>K = <span id="example-input-1-1">1</span>, N = <span id="example-input-1-2">2</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>
Drop the egg from floor 1.  If it breaks, we know with certainty that F = 0.
Otherwise, drop the egg from floor 2.  If it breaks, we know with certainty that F = 1.
If it didn&#39;t break, then we know with certainty F = 2.
Hence, we needed 2 moves in the worst case to know what F is with certainty.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>K = <span id="example-input-2-1">2</span>, N = 6
<strong>Output: </strong><span id="example-output-2">3</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>K = <span id="example-input-3-1">3</span>, N = <span id="example-input-3-2">14</span>
<strong>Output: </strong><span id="example-output-3">4</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= K &lt;= 100</code></li>
	<li><code>1 &lt;= N &lt;= 10000</code></li>
</ol>
</div>
</div>
</div>

</div>

## Tags
- Math (math)
- Binary Search (binary-search)
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 2 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Dynamic Programming with Binary Search

**Intuition**

It's natural to attempt dynamic programming, as we encounter similar subproblems.  Our state is `(K, N)`: `K` eggs and `N` floors left.  When we drop an egg from floor `X`, it either survives and we have state `(K, N-X)`, or it breaks and we have state `(K-1, X-1)`.

This approach would lead to a $$O(K N^2)$$ algorithm, but this is not efficient enough for the given constraints.  However, we can try to speed it up.  Let `dp(K, N)` be the maximum number of moves needed to solve the problem in state `(K, N)`.  Then, by our reasoning above, we have:

$$
\text{dp}(K, N) = \min\limits_{1 \leq X \leq N} \Big( \max(\text{dp}(K-1, X-1), \text{dp}(K, N-X)) \Big)
$$

Now for the key insight:  Because $$\text{dp}(K, N)$$ is a function that is increasing on $$N$$, the first term $$\mathcal{T_1} = \text{dp}(K-1, X-1)$$ in our $$\max$$ expression is an increasing function on $$X$$, and the second term $$\mathcal{T_2} = \text{dp}(K, N-X)$$ is a decreasing function on $$X$$.  This means that we do not need to check every $$X$$ to find the minimum -- instead, we can binary search for the best $$X$$.


**Algorithm**

<p align="center">
    <img src="../Figures/891/sketch.png" alt="T1, T2 diagram" style="height: 300px;"/>
</p>

Continuing our discussion, if $$\mathcal{T_1} < \mathcal{T_2}$$, then the $$X$$ value chosen is too small; and if $$\mathcal{T_1} > \mathcal{T_2}$$, then $$X$$ is too big.  However, this argument is not quite correct: when there are only two possible values of $$X$$, we need to check both.

Using the above fact, we can use a binary search to find the correct value of $$X$$ more efficiently than checking all $$N$$ of them.

<iframe src="https://leetcode.com/playground/KfdcVYz9/shared" frameBorder="0" width="100%" height="500" name="KfdcVYz9"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(K * N \log N)$$.

* Space Complexity:  $$O(K * N)$$.
<br />
<br />


---
#### Approach 2: Dynamic Programming with Optimality Criterion

**Intuition**

As in *Approach 1*, we try to speed up our $$O(K N^2)$$ algorithm.  Again, for a state of $$K$$ eggs and $$N$$ floors, where $$\text{dp}(K, N)$$ is the answer for that state, we have:

$$
\text{dp}(K, N) = \min\limits_{1 \leq X \leq N} \Big( \max(\text{dp}(K-1, X-1), \text{dp}(K, N-X)) \Big)
$$

Now, suppose $$X_{\emptyset} = \text{opt}(K, N)$$ is the smallest $$X$$ for which that minimum is attained: that is, the smallest value for which

$$
\text{dp}(K, N) = \Big( \max(\text{dp}(K-1, X_{\emptyset}-1), \text{dp}(K, N-X_{\emptyset})) \Big)
$$

The key insight that we will develop below, is that $$\text{opt}(K, N)$$ is an increasing function in $$N$$.

<p align="center">
    <img src="../Figures/891/sketch2.png" alt="T1, T2 diagram" style="height: 300px;"/>
</p>

The first term of our $$\max$$ expression, $$\mathcal{T_1} = \text{dp}(K-1, X-1)$$, is increasing with respect to $$X$$, but constant with respect to $$N$$.  The second term, $$\mathcal{T_2} = \text{dp}(K, N-X)$$, is decreasing with respect to $$X$$, but increasing with respect to $$N$$.

This means that as $$N$$ increases, the intersection point $$X_{\emptyset} = \text{opt}(K, N)$$ of these two lines is increasing, as we can see in the diagram.


**Algorithm**

Perform "bottom up" dynamic programming based on the recurrence below, keeping track of $$X_{\emptyset} = \text{opt}(K, N)$$.  Again:

$$
\text{dp}(K, N) = \min\limits_{1 \leq X \leq N} \Big( \max(\text{dp}(K-1, X-1), \text{dp}(K, N-X)) \Big)
$$

When we want to find $$\text{dp}(K, N+1)$$, instead of searching for $$X$$ from $$1 \leq X \leq N$$, we only have to search through $$X_{\emptyset} \leq X \leq N$$.

Actually, (as illustrated by the diagram,) if ever the next $$X+1$$ is worse than the current $$X$$, then we've searched too far, and we know our current $$X$$ is best for this $$N$$.

<iframe src="https://leetcode.com/playground/jM5NUax9/shared" frameBorder="0" width="100%" height="500" name="jM5NUax9"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(K * N)$$.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 3: Mathematical

**Intuition**

Let's ask the question in reverse: given $$T$$ moves (and $$K$$ eggs), what is the most number of floors $$f(T, K)$$ that we can still "solve" (find $$0 \leq F \leq f(T, K)$$ with certainty)?  Then, the problem is to find the least $$T$$ for which $$f(T, K) \geq N$$.  Because more tries is always at least as good, $$f$$ is increasing on $$T$$, which means we could binary search for the answer.

Now, we find a similar recurrence for $$f$$ as in the other approaches.  If in an optimal strategy we drop the egg from floor $$X_{\emptyset}$$, then either it breaks and we can solve $$f(T-1, K-1)$$ lower floors (floors $$< X_{\emptyset}$$); or it doesn't break and we can solve $$f(T-1, K)$$ higher floors (floors $$\geq X_{\emptyset}$$).  In total,

$$
f(T, K) = 1 + f(T-1, K-1) + f(T-1, K)
$$

Also, it is easily seen that $$f(t, 1) = t$$ when $$t \geq 1$$, and $$f(1, k) = 1$$ when $$k \geq 1$$.


<p align="center">
    <img src="../Figures/891/sketch3.png" alt="T1, T2 diagram" style="height: 300px;""/>
</p>

From here, we don't need to solve the recurrence mathematically - we could simply use it to generate all $$O(K * \max(T))$$ possible values of $$f(T, K)$$.

However, there is a mathematical solution to this recurrence.  If $$g(t, k) = f(t, k) - f(t, k-1)$$, [the difference between the $$k-1$$th and $$k$$th term,] then subtracting the two equations:

$$
f(T, K) = 1 + f(T-1, K-1) + f(T-1, K)
$$

$$
f(T, K-1) = 1 + f(T-1, K-2) + f(T-1, K-1)
$$

we get:

$$
g(t, k) = g(t-1, k) + g(t-1, k-1)
$$

This is a binomial recurrence with solution $$g(t, k) = \binom{t}{k+1}$$, so that indeed,

$$
f(t, k) = \sum\limits_{1 \leq x \leq K} g(t, x) = \sum \binom{t}{x}
$$


**Alternative Mathematical Derivation**

Alternatively, when we have $$t$$ tries and $$K$$ eggs, the result of our $$t$$ throws must be a $$t$$-length sequence of successful and failed throws, with at most K failed throws.  The number of sequences with $$0$$ failed throws is $$\binom{t}{0}$$, the number of sequences with $$1$$ failed throw is $$\binom{t}{1}$$ etc., so that the number of such sequences is $$\sum\limits_{0 \leq x \leq K} \binom{t}{x}$$.

Hence, we can only distinguish at most these many floors in $$t$$ tries (as each sequence can only map to 1 answer per sequence.)  This process includes distinguishing $$F = 0$$, so that the corresponding value of $$N$$ is one less than this sum.

However, this is also a lower bound for the number of floors that can be distinguished, as the result of a throw on floor $$X$$ will bound the answer to be either at most $$X$$ or greater than $$X$$.  Hence, in an optimal throwing strategy, each such sequence actually maps to a unique answer.

**Algorithm**

Recapping our algorithm, we have the increasing [wrt $$t$$] function $$f(t, K) = \sum\limits_{1 \leq x \leq K} \binom{t}{x}$$, and we want the least $$t$$ so that $$f(t, K) \geq N$$.  We binary search for the correct $$t$$.

To evaluate $$f(t, K)$$ quickly, we can transform the previous binomial coefficient to the next (in the summand) by the formula $$\binom{n}{k} * \frac{n-k}{k+1} = \binom{n}{k+1}$$.


<iframe src="https://leetcode.com/playground/EjAh85br/shared" frameBorder="0" width="100%" height="480" name="EjAh85br"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(K * \log N)$$.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] 2D and 1D DP, O(KlogN)
- Author: lee215
- Creation Date: Sun Aug 12 2018 11:07:12 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Mar 11 2020 10:54:02 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
Drop eggs is a very classical problem.
Some people may come up with idea `O(KN^2)`
where `dp[K][N] = 1 + max(dp[K - 1][i - 1], dp[K][N - i])` for i in 1...N.
However this idea is very brute force, for the reason that you check all possiblity.

So I consider this problem in a different way:
`dp[M][K]`means that, given `K` eggs and `M` moves,
what is the maximum number of floor that we can check.

The dp equation is:
`dp[m][k] = dp[m - 1][k - 1] + dp[m - 1][k] + 1`,
which means we take 1 move to a floor,
if egg breaks, then we can check `dp[m - 1][k - 1]` floors.
if egg doesn\'t breaks, then we can check `dp[m - 1][k]` floors.

`dp[m][k]` is the number of combinations and it increase exponentially to `N`
<br>

# Complexity
For time, `O(NK)` decalre the space, `O(KlogN)` running,
For space, `O(NK)`.
<br>

**C++:**
```cpp
    int superEggDrop(int K, int N) {
        vector<vector<int>> dp(N + 1, vector<int>(K + 1, 0));
        int m = 0;
        while (dp[m][K] < N) {
            m++;
            for (int k = 1; k <= K; ++k)
                dp[m][k] = dp[m - 1][k - 1] + dp[m - 1][k] + 1;
        }
        return m;
    }
```

**Java:**
```java
    public int superEggDrop(int K, int N) {
        int[][] dp = new int[N + 1][K + 1];
        int m = 0;
        while (dp[m][K] < N) {
            ++m;
            for (int k = 1; k <= K; ++k)
                dp[m][k] = dp[m - 1][k - 1] + dp[m - 1][k] + 1;
        }
        return m;
    }
```
**Python:**
```py
    def superEggDrop(self, K, N):
        dp = [[0] * (K + 1) for i in range(N + 1)]
        for m in range(1, N + 1):
            for k in range(1, K + 1):
                dp[m][k] = dp[m - 1][k - 1] + dp[m - 1][k] + 1
            if dp[m][K] >= N: return m
```


# Optimized to 1D DP

Complexity:
C++/Java `O(KlogN)` Time,  `O(K)` Space
Python `O(min(K, logN)^2)` Time,  `O(min(K, logN))` Space
<br>

**C++:**
```cpp
    int superEggDrop(int K, int N) {
        vector<int> dp(K + 1, 0);
        int m;
        for (m = 0; dp[K] < N; m++)
            for (int k = K; k > 0; --k)
                dp[k] += dp[k - 1] + 1;
        return m;
    }
```

**Java:**
```java
    public int superEggDrop(int K, int N) {
        int dp[] = new int[K + 1], m = 0;
        for (m = 0; dp[K] < N; ++m)
            for (int k = K; k > 0; --k)
                dp[k] += dp[k - 1] + 1;
        return m;
    }
```
**Python:**
```py
    def superEggDrop(self, K, N):
        dp = [0, 0]
        m = 0
        while dp[-1] < N:
            for i in range(len(dp) - 1, 0, - 1):
                dp[i] += dp[i - 1] + 1
            if len(dp) < K + 1:
                dp.append(dp[-1])
            m += 1
        return m
```

</p>


### Python DP from kn^2 to knlogn to kn
- Author: Greatjian
- Creation Date: Sun Aug 12 2018 15:13:08 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 04 2018 23:41:10 GMT+0800 (Singapore Standard Time)

<p>
# Basic idea
First of all, we can see to get the answer of larger floors and eggs, results of smaller cases are useful for analysis, which leads to dynamic programming.

Next, how to define a state for each drop of getting the optimal floor? Here we have two variables:

- The number of eggs left to throw `i`, (0 <= i <= K)
- The number of floors left to test `j`, (1 <= j <= N)

The answer (smallest times to get the optimal floor) can be the value of each dp state.

Therefore, we define `dp[i][j]` as smallest number of drop to get the optimal floor with `i` eggs and `j` floors left.

# DP formula
For the implementation of dp, we need two information:
- base case
- recursive relation

Base case is rather intuitive to come up with. Think of cases with smallers eggs and floors:
```python
dp[1][j] = j, j = 1...N # one egg, check each floor from 1 to j
dp[i][0] = 0, i = 1...K # no floor, no drop needed to get the optimal floor
dp[i][1] = 1, i = 1...K # one floor, only check once
```
To get recursive relation, let\'s consider a test case: 3 eggs and 100 floors.
For the next drop, I can choose floor from 1 to 100, say I choose 25.
There are 2 possible results for this drop:
- the egg brokes, I now have 2 eggs, and the floor to choose becomes 1~24.
- the egg remains safe, I still have 3 eggs, and the floor to choose becomes 26~100.

Think of the worst case senerio and use the dp defination above, we can describe the situation of getting the optical floor with choosing floor 25 for the next drop as:
`dp[3][100] = 1 + max(dp[2][24], dp[3][75])`

Besides floor 25, in term of next drop, we can also choose floor from 1 to 100. Each drop would be similar to the case of 25 above. The final result would be the minimum of all possible choices of next floors to drop:

`dp[3][100] = min(..., 1 + max(dp[2][23], dp[3][76]), 1 + max(dp[2][24], dp[3][75]), 1 + max(dp[2][25], dp[3][74]), ...)` (take floor 24, 25, 26 as example)

To generilize the above example, the dp formula would be:
`dp[i][j] = min(1 + max(dp[i-1][k-1], dp[i][j-k])), k = 1,2,...j`

# The brute force solution

With dp formula above, the brute force solution would be `O(kn^2)` as below:
```python
class Solution(object):
    def superEggDrop(self, K, N):
        """
        :type K: int
        :type N: int
        :rtype: int
        """
        dp=[[float(\'inf\')]*(N+1) for _ in range(K+1)]
        for i in range(1, K+1):
            dp[i][0] = 0
            dp[i][1] = 1
        for j in range(1, N+1):
            dp[1][j] = j
        
        for i in range(2, K+1):
            for j in range(2, N+1):
                for k in range(1, j+1):
                    dp[i][j] = min(dp[i][j], 1 + max(dp[i-1][k-1], dp[i][j-k]))
        return dp[K][N]
```

# Optimization1: choosing k for each dp[i][j]
The brute force solution gets TLE. To optimize, we should check for the unnecessary iteration in the for-loops. More specifically, to get the `k` that best fits each drop, we don\'t need to go over all floors from `1` to `j`. As for a fixed `k`, `dp[i][k]` goes up as `k` increases. This means `dp[i-1][k-1]` will increase and `dp[i][j-k]` will decrease as k goes from `1` to `j`. The optimal value of `k` will be the middle point where the two meet. So to get the optimal `k` value for `dp[i][j]`, we can do a binary search for `k` from `1` to `j`. 

This will save the third for-loop of `k` from `O(n)` to `O(logn)`. Total complexity is `O(knlogn)`. Using binary seach, we can only do bottom up dp, as shown below:

```python
class Solution(object):
    def superEggDrop(self, K, N):
        """
        :type K: int
        :type N: int
        :rtype: int
        """
        def dfs(i, j):
            if i==1:
                return j
            if j==0:
                return 0
            if j==1:
                return 1
            if (i, j) in d:
                return d[i, j]
            lo, hi = 0, j
            while lo < hi:
                mid = (lo+hi)/2
                left, right = dfs(i-1, mid-1), dfs(i, j-mid)
                if left < right:
                    lo = mid + 1
                else:
                    hi = mid
            res = 1 + max(dfs(i-1, lo-1), dfs(i, j-lo))
            d[i, j]=res
            return res
        
        d={}
        return dfs(K, N)
```

# Optimization2: choosing k_1...k_N for each dp[i][1...N]
To go one step further, till now, we are still finding the optimal floor `k` from 1 to `j` for each `dp[i][j]`. But is this really the smallest range we can narrow? In fact, we can see that the optimal floor `k` for each `dp[i][j]` increases as `j` increases. This means that once we get the optimal `k` for `dp[i][j]`, we can save current `k` value and start the next round of for-loop directly, instead of initiating `k` from 0 again. In this way, in the third for-loop, `k` will go from 1 to N only once as `j` in the second for-loop goes from 1 to N. The total time complexity will be `O(kn)`. The code is shown below:

```python
class Solution(object):
    def superEggDrop(self, K, N):
        """
        :type K: int
        :type N: int
        :rtype: int
        """
        dp=[[float(\'inf\')]*(N+1) for _ in range(K+1)]
        for i in range(1, K+1):
            dp[i][0] = 0
            dp[i][1] = 1
        for j in range(1, N+1):
            dp[1][j] = j
            
        for i in range(2, K+1):
            k = 1
            for j in range(2, N+1):
                while k < j+1 and dp[i][j-k] > dp[i-1][k-1]:
                    k += 1
                dp[i][j] = 1 + dp[i-1][k-1]
        return dp[K][N]
```
# Optimization3: space complexity
Usually, we can save space complexity by one dimension if the recursive relation of dp is based the constant length of previous states. Here, the current line dp[i] is getting updated based on the current line and previous line, so we can only record those two lines and update them in the iteration process to save the space space complexity by one dimension. Final time complexity `O(kn)`, space complexity `O(n)`. The code is shown below:
```python
class Solution(object):
    def superEggDrop(self, K, N):
        """
        :type K: int
        :type N: int
        :rtype: int
        """ 
        dp = range(N+1)
        for i in range(2, K+1):
            k = 1
            ndp = [0, 1] + [float(\'inf\')]*(N-1)
            for j in range(2, N+1):
                while k < j+1 and ndp[j-k] > dp[k-1]:
                    k += 1
                ndp[j] = 1 + dp[k-1]
            dp = ndp
        return dp[N]
```
# Summery
Personally, I\'d recommend thinking this problem through the following logic:
1. Why dp? Under what circumstances should we use dp to solve a problem?
2. What\'s the base case and recursive relation to describe a state in the problem?
3. What\'s the dp formula based on the above analysis?
4. Implement the brute force solution.
5. Optimization: find the pattern of dp array, what iteration is unneccessary and can be saved?
6. What\'s the time and space complexity? Any way to save the space complexity by one dimension?
</p>


### Java DP solution from O(KN^2) to O(KNlogN)
- Author: wangzi6147
- Creation Date: Sun Aug 12 2018 14:05:21 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 10 2018 15:50:12 GMT+0800 (Singapore Standard Time)

<p>
We can easily come up with an `O(KN^2)` DP solution, where `dp[k][n] = min(1 + max(dp[k - 1][i - 1], dp[k][n - i])) i = 1...n`
In this implementation, we use recursion to simulate each move.
But it runs **TLE**.
```
class Solution {
    public int superEggDrop(int K, int N) {
        int[][] memo = new int[K + 1][N + 1];
        return helper(K, N, memo);
    }
    private int helper(int K, int N, int[][] memo) {
        if (N <= 1) {
            return N;
        }
        if (K == 1) {
            return N;
        }
        if (memo[K][N] > 0) {
            return memo[K][N];
        }
        int min = N;
        for (int i = 1; i <= N; i++) {
            int left = helper(K - 1, i - 1, memo);
            int right = helper(K, N - i, memo);
            min = Math.min(min, Math.max(left, right) + 1);
        }
        memo[K][N] = min;
        return min;
    }
}
```

Notice that for the same `K` when `N` goes up, `dp[K][N]` goes up.
Then for `int left = helper(K - 1, i - 1, memo);` `int right = helper(K, N - i, memo);` when `i` goes up, `left` goes up and `right` goes down.
We can use `Binary Search` here to get the minimum `Math.max(left, right) + 1`, when `left` and `right` are as close as possible.
We come to this `O(KNlogN)` solution:
```
class Solution {
    public int superEggDrop(int K, int N) {
        int[][] memo = new int[K + 1][N + 1];
        return helper(K, N, memo);
    }
    private int helper(int K, int N, int[][] memo) {
        if (N <= 1) {
            return N;
        }
        if (K == 1) {
            return N;
        }
        if (memo[K][N] > 0) {
            return memo[K][N];
        }
        
        int low = 1, high = N, result = N;
        while (low < high) {
            int mid = low + (high - low) / 2;
            int left = helper(K - 1, mid - 1, memo);
            int right = helper(K, N - mid, memo);
            result = Math.min(result, Math.max(left, right) + 1);
            if (left == right) {
                break;
            } else if (left < right) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }
        memo[K][N] = result;
        return result;
    }
}
```
</p>


