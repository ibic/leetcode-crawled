---
title: "Gas Station"
weight: 134
#id: "gas-station"
---
## Description
<div class="description">
<p>There are <em>N</em> gas stations along a circular route, where the amount of gas at station <em>i</em> is <code>gas[i]</code>.</p>

<p>You have a car with an unlimited gas tank and it costs <code>cost[i]</code> of gas to travel from station <em>i</em> to its next station (<em>i</em>+1). You begin the journey with an empty tank at one of the gas stations.</p>

<p>Return the starting gas station&#39;s index if you can travel around the circuit once in the clockwise direction, otherwise return -1.</p>

<p><strong>Note:</strong></p>

<ul>
	<li>If there exists a&nbsp;solution, it is guaranteed to be unique.</li>
	<li>Both input arrays are non-empty and have the same length.</li>
	<li>Each element in the input arrays is a non-negative integer.</li>
</ul>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 
gas  = [1,2,3,4,5]
cost = [3,4,5,1,2]

<strong>Output:</strong> 3

<strong>Explanation:
</strong>Start at station 3 (index 3) and fill up with 4 unit of gas. Your tank = 0 + 4 = 4
Travel to station 4. Your tank = 4 - 1 + 5 = 8
Travel to station 0. Your tank = 8 - 2 + 1 = 7
Travel to station 1. Your tank = 7 - 3 + 2 = 6
Travel to station 2. Your tank = 6 - 4 + 3 = 5
Travel to station 3. The cost is 5. Your gas is just enough to travel back to station 3.
Therefore, return 3 as the starting index.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> 
gas  = [2,3,4]
cost = [3,4,3]

<strong>Output:</strong> -1

<strong>Explanation:
</strong>You can&#39;t start at station 0 or 1, as there is not enough gas to travel to the next station.
Let&#39;s start at station 2 and fill up with 4 unit of gas. Your tank = 0 + 4 = 4
Travel to station 0. Your tank = 4 - 3 + 2 = 3
Travel to station 1. Your tank = 3 - 3 + 3 = 3
You cannot travel back to station 2, as it requires 4 unit of gas but you only have 3.
Therefore, you can&#39;t travel around the circuit once no matter where you start.
</pre>

</div>

## Tags
- Greedy (greedy)

## Companies
- Amazon - 7 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- IBM - 4 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: One pass.

**Intuition**

The first idea is to check every single station :

- Choose the station as starting point.

- Perform the road trip and check how much gas we have in tank
at each station.

That means $$\mathcal{O}(N^2)$$ time complexity, and for sure one could
do better.

Let's notice two things.

> It's impossible to perform the road trip if `sum(gas) < sum(cost)`. 
In this situation the answer is `-1`. 

<img src="../Figures/134/round_trip.png" width="500">

One could compute total amount of gas in the tank 
`total_tank = sum(gas) - sum(cost)` 
during the round trip, 
and then return `-1` if `total_tank < 0`.

> It's impossible to start at a station `i` if `gas[i] - cost[i] < 0`,
because then there is not enough gas in the tank to travel to 
`i + 1` station.
 
<img src="../Figures/134/no_start1.png" width="500">

The second fact could be generalized. Let's introduce
`curr_tank` variable to track the current amount of gas 
in the tank. If at some station `curr_tank` is less
than `0`, that means that one couldn't reach this station.

Next step is to mark this station as a new starting point,
and reset `curr_tank` to zero since one starts with 
no gas in the tank.

**Algorithm**

Now the algorithm is straightforward :

1. Initiate `total_tank` and `curr_tank` as zero, and
choose station `0` as a starting station.

2. Iterate over all stations :
    
    - Update `total_tank` and `curr_tank` at each step,
    by adding `gas[i]` and subtracting `cost[i]`. 
    
    - If `curr_tank < 0` at `i + 1` station,
    make `i + 1` station a new starting point and
    reset `curr_tank = 0` to start with an empty tank.

3. Return `-1` if `total_tank < 0` and `starting station`
otherwise.

**Why this works**

Let's imagine the situation when `total_tank >= 0` and
the above algorithm returns $$N_s$$ as a starting station.

Algorithm directly ensures 
that it's possible to go from $$N_s$$ to the station $$0$$.
But what about the last part of the round trip
from the station $$0$$ to the station $$N_s$$ ?

> How one could ensure that it's possible to loop around to $$N_s$$ ?

Let's use here the [proof by contradiction](https://en.wikipedia.org/wiki/Proof_by_contradiction)
and assume that there is a station $$0 < k < N_s$$ such that 
one couldn't reach this station starting from $$N_s$$. 
 
The condition `total_tank >= 0` could be written as

$$
\sum_{i = 0}^{i = N}{\alpha_i} \ge 0    \qquad (1)
$$
where $$\alpha_i = \textrm{gas[i]} - \textrm{cost[i]}$$.

Let's split the sum on the right side 
by the starting station $$N_s$$
and unreachable station `k` : 

$$
\sum_{i = 0}^{i = k}{\alpha_i} + 
\sum_{i = k + 1}^{i = N_s - 1}{\alpha_i} +
\sum_{i = N_s}^{i = N}{\alpha_i} \ge 0  \qquad (2)
$$

The second term is negative by the algorithm definition - 
otherwise the starting station would be before $$N_s$$.
It could be equal to zero only in the case of $$k = N_s - 1$$.

$$
\sum_{i = k + 1}^{i = N_s - 1}{\alpha_i} \le 0  \qquad (3)
$$

Equations `(2)` and `(3)` together results in 

$$
\sum_{i = 0}^{i = k}{\alpha_i} +
\sum_{i = N_s}^{i = N}{\alpha_i} \ge 0  \qquad (4)
$$

At the same time the station $$k$$ is supposed to be unreachable
from $$N_s$$ that means 

$$
\sum_{i = N_s}^{i = N}{\alpha_i} +
\sum_{i = 0}^{i = k}{\alpha_i} < 0  \qquad (5)
$$

Eqs. `(4)` and `(5)` together result in a contradiction. 
Therefore, the initial assumption — 
that there is a station $$0 < k < N_s$$ such that 
one couldn't reach this station starting from $$N_s$$
— must be false.

Hence, one could do a round trip starting from $$N_s$$,
that makes $$N_s$$ to be an answer. 
The answer is unique according to the problem definition.

**Implementation**

!?!../Documents/134_LIS.json:1000,746!?!

<iframe src="https://leetcode.com/playground/a457Z8gb/shared" frameBorder="0" width="100%" height="429" name="a457Z8gb"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since there is only
one loop over all stations here.
 
* Space complexity : $$\mathcal{O}(1)$$ since it's a constant 
space solution. 

**Further reading**

There are numerous variations of gas problem, here are some examples :

[Find the cheapest path between two stations 
if at most Δ stops are allowed.](https://www.sciencedirect.com/science/article/pii/S002001901730203X) 

[Find the cheapest path between two stations 
if the vehicle has a given tank capacity.](https://link.springer.com/chapter/10.1007/978-3-540-75520-3_48)

## Accepted Submission (scala)
```scala
object Solution {
    def canCompleteCircuit(gas: Array[Int], cost: Array[Int]): Int = {
        var tank = 0
        var start = 0
        while (start < gas.size) {
            var i = start
            while (tank >= 0) {
                if (i == start + gas.size) {
                    return start
                }
                tank += gas(i % gas.size)
                tank -= cost(i % gas.size)
                i += 1
            }
            start = i
            tank = 0
        }
        return -1
    }
}
```

## Top Discussions
### Share some of my ideas.
- Author: daxianji007
- Creation Date: Fri Apr 04 2014 17:10:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 04:32:32 GMT+0800 (Singapore Standard Time)

<p>
I have thought for a long time and got two ideas:

 - If car starts at A and can not reach B. Any station between A and B 
    can not reach B.(B is the first station that A can not reach.)
 - If the total number of gas is bigger than the total number of cost. There must be a solution. 
 - (Should I prove them?)

Here is my solution based on those ideas:

    class Solution {
    public:
        int canCompleteCircuit(vector<int> &gas, vector<int> &cost) {
            int start(0),total(0),tank(0);
            //if car fails at 'start', record the next station
            for(int i=0;i<gas.size();i++) if((tank=tank+gas[i]-cost[i])<0) {start=i+1;total+=tank;tank=0;}
            return (total+tank<0)? -1:start;
        }
    };
</p>


### My AC is O(1) space O(n) running time solution. Does anybody have posted this solution?
- Author: xuewuxiao
- Creation Date: Fri Nov 14 2014 10:20:43 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 15:09:53 GMT+0800 (Singapore Standard Time)

<p>
I have got one solution to this problem. I am not sure whether somebody has already posted this solution.

    class Solution {
    public:
        int canCompleteCircuit(vector<int> &gas, vector<int> &cost) {
    
           int start = gas.size()-1;
           int end = 0;
           int sum = gas[start] - cost[start];
           while (start > end) {
              if (sum >= 0) {
                 sum += gas[end] - cost[end];
                 ++end;
              }
              else {
                 --start;
                 sum += gas[start] - cost[start];
              }
           }
           return sum >= 0 ? start : -1;
        }
    };
</p>


### Proof of "if total gas is greater than total cost, there is a solution". C++
- Author: XBOX360555
- Creation Date: Sun Mar 13 2016 04:02:50 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 10:13:28 GMT+0800 (Singapore Standard Time)

<p>
We prove the following statement. 
If sum of all `gas[i]-cost[i]` is greater than or equal to `0`, then there is a start position you can travel the whole circle. 
Let `i` be the index such that the the partial sum 

    gas[0]-cost[0]+gas[1]-cost[1]+...+gas[i]-cost[i]

is the smallest, then the start position should be `start=i+1` ( `start=0` if `i=n-1`). Consider any other partial sum, for example,

    gas[0]-cost[0]+gas[1]-cost[1]+...+gas[i]-cost[i]+gas[i+1]-cost[i+1]

Since `gas[0]-cost[0]+gas[1]-cost[1]+...+gas[i]-cost[i]` is the smallest, we must have 

    gas[i+1]-cost[i+1]>=0

in order for `gas[0]-cost[0]+gas[1]-cost[1]+...+gas[i]-cost[i]+gas[i+1]-cost[i+1]` to be greater.
The same reasoning gives that 

     gas[i+1]-cost[i+1]>=0
     gas[i+1]-cost[i+1]+gas[i+2]-cost[i+2]>=0
     .......
     gas[i+1]-cost[i+1]+gas[i+2]-cost[i+2]+...+gas[n-1]-cost[n-1]>=0
What about for the partial sums that wraps around?

    gas[0]-cost[0]+gas[1]-cost[1]+...+gas[j]-cost[j] + gas[i+1]-cost[i+1]+...+gas[n-1]-cost[n-1]
    >=
    gas[0]-cost[0]+gas[1]-cost[1]+...+gas[i]-cost[i] + gas[i+1]-cost[i+1]+...+gas[n-1]-cost[n-1]
    >=0
The last inequality is due to the assumption that the entire sum of `gas[k]-cost[k]` is greater than or equal to 0.
So we have that all the partial sums 

    gas[i+1]-cost[i+1]>=0,
    gas[i+1]-cost[i+1]+gas[i+2]-cost[i+2]>=0,
    gas[i+1]-cost[i+1]+gas[i+2]-cost[i+2]+...+gas[n-1]-cost[n-1]>=0,
    ...
    gas[i+1]-cost[i+1]+...+gas[n-1]-cost[n-1] + gas[0]-cost[0]+gas[1]-cost[1]+...+gas[j]-cost[j]>=0,
    ...
Thus `i+1` is the position to start. Coding using this reasoning is as follows:

    class Solution {
    public:
        int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
            int n = gas.size();
            int total(0), subsum(INT_MAX), start(0);
            for(int i = 0; i < n; ++i){
                total += gas[i] - cost[i];
                if(total < subsum) {
                    subsum = total;
                    start = i + 1;
                }
            }
            return (total < 0) ?  -1 : (start%n); 
        }
    };
</p>


