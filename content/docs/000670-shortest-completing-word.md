---
title: "Shortest Completing Word"
weight: 670
#id: "shortest-completing-word"
---
## Description
<div class="description">
<p>Given a string <code>licensePlate</code> and an array of strings <code>words</code>, find the <strong>shortest completing</strong> word in <code>words</code>.</p>

<p>A <strong>completing</strong> word is a word that <strong>contains all the letters</strong> in <code>licensePlate</code>. <strong>Ignore numbers and spaces</strong> in <code>licensePlate</code>, and treat letters as <strong>case insensitive</strong>. If a letter appears more than once in <code>licensePlate</code>, then it must appear in the word the same number of times or more.</p>

<p>For example, if <code>licensePlate</code><code> = &quot;aBc 12c&quot;</code>, then it contains letters <code>&#39;a&#39;</code>, <code>&#39;b&#39;</code> (ignoring case), and <code>&#39;c&#39;</code> twice. Possible <strong>completing</strong> words are <code>&quot;abccdef&quot;</code>, <code>&quot;caaacab&quot;</code>, and <code>&quot;cbca&quot;</code>.</p>

<p>Return <em>the shortest <strong>completing</strong> word in </em><code>words</code><em>.</em> It is guaranteed an answer exists. If there are multiple shortest <strong>completing</strong> words, return the <strong>first</strong> one that occurs in <code>words</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> licensePlate = &quot;1s3 PSt&quot;, words = [&quot;step&quot;,&quot;steps&quot;,&quot;stripe&quot;,&quot;stepple&quot;]
<strong>Output:</strong> &quot;steps&quot;
<strong>Explanation:</strong> licensePlate contains letters &#39;s&#39;, &#39;p&#39;, &#39;s&#39; (ignoring case), and &#39;t&#39;.
&quot;step&quot; contains &#39;t&#39; and &#39;p&#39;, but only contains 1 &#39;s&#39;.
&quot;steps&quot; contains &#39;t&#39;, &#39;p&#39;, and both &#39;s&#39; characters.
&quot;stripe&quot; is missing an &#39;s&#39;.
&quot;stepple&quot; is missing an &#39;s&#39;.
Since &quot;steps&quot; is the only word containing all the letters, that is the answer.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> licensePlate = &quot;1s3 456&quot;, words = [&quot;looks&quot;,&quot;pest&quot;,&quot;stew&quot;,&quot;show&quot;]
<strong>Output:</strong> &quot;pest&quot;
<strong>Explanation:</strong> licensePlate only contains the letter &#39;s&#39;. All the words contain &#39;s&#39;, but among these &quot;pest&quot;, &quot;stew&quot;, and &quot;show&quot; are shortest. The answer is &quot;pest&quot; because it is the word that appears earliest of the 3.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> licensePlate = &quot;Ah71752&quot;, words = [&quot;suggest&quot;,&quot;letter&quot;,&quot;of&quot;,&quot;husband&quot;,&quot;easy&quot;,&quot;education&quot;,&quot;drug&quot;,&quot;prevent&quot;,&quot;writer&quot;,&quot;old&quot;]
<strong>Output:</strong> &quot;husband&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> licensePlate = &quot;OgEu755&quot;, words = [&quot;enough&quot;,&quot;these&quot;,&quot;play&quot;,&quot;wide&quot;,&quot;wonder&quot;,&quot;box&quot;,&quot;arrive&quot;,&quot;money&quot;,&quot;tax&quot;,&quot;thus&quot;]
<strong>Output:</strong> &quot;enough&quot;
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> licensePlate = &quot;iMSlpe4&quot;, words = [&quot;claim&quot;,&quot;consumer&quot;,&quot;student&quot;,&quot;camera&quot;,&quot;public&quot;,&quot;never&quot;,&quot;wonder&quot;,&quot;simple&quot;,&quot;thought&quot;,&quot;use&quot;]
<strong>Output:</strong> &quot;simple&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= licensePlate.length &lt;= 7</code></li>
	<li><code>licensePlate</code> contains digits, letters (uppercase or lowercase), or space <code>&#39; &#39;</code>.</li>
	<li><code>1 &lt;= words.length &lt;= 1000</code></li>
	<li><code>1 &lt;= words[i].length &lt;= 15</code></li>
	<li><code>words[i]</code> consists of lower case English letters.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Compare Counts [Accepted]

**Intuition and Algorithm**

A natural question is, how to tell whether a `word` like `"steps"` completes a `licensePlate` like `"12s pst"`?

We count the number of letters in both `word` and `licensePlate`, converting to lowercase and ignoring non-letter characters.  If the count of each letter is greater or equal in the word, then that word completes the `licensePlate`.

From the words that complete `licensePlate`, we should keep the one with the shortest length (with ties broken by whether it occurs first.)

<iframe src="https://leetcode.com/playground/zcwwe5Vf/shared" frameBorder="0" width="100%" height="500" name="zcwwe5Vf"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ where $$N$$ is the length of `words`, and assuming the lengths of `licensePlate` and `words[i]` are bounded by $$O(1)$$.

* Space Complexity: $$O(1)$$ in additional space.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 6ms beats 100% PRIME NUMBERS
- Author: jmcelvenny
- Creation Date: Thu Aug 09 2018 12:50:49 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 16:35:52 GMT+0800 (Singapore Standard Time)

<p>
Idea: assign each letter a prime number and compute the product for the license plate. Then, compute the product for each word in wordlist. We know if the char product for a word is divisible by the license plate char product, it contains all the characters. 

```
class Solution {
    private static final int[] primes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103}; 
    
    public String shortestCompletingWord(String licensePlate, String[] words) {
        long charProduct = getCharProduct(licensePlate.toLowerCase());
        String shortest = "aaaaaaaaaaaaaaaaaaaa"; // 16 a\'s
        for(String word : words)
            if (word.length() < shortest.length() && getCharProduct(word) % charProduct == 0)
                    shortest = word;
        return shortest;
    }
    
    private long getCharProduct(String plate) {
        long product = 1L;
        for(char c : plate.toCharArray()) {
            int index = c - \'a\';
            if (0 <= index && index <= 25) 
                product *= primes[index];
        }
        return product;
    }
}
```
</p>


### Python 2-liner with explanation
- Author: rostam
- Creation Date: Wed Apr 17 2019 14:19:45 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 07 2019 08:42:20 GMT+0800 (Singapore Standard Time)

<p>
1) In first line, just filter out all none letters from the plate and make sure all letters are lower case.
2) In second line, iterate through all words and use `&` to extract the count of shared letters between the word and the plate. If all the counts are equal, this returns true. Then,  just extract the word that satisfies this condition and has the shortest length.

```
    def shortestCompletingWord(self, licensePlate: str, words: List[str]) -> str:
        pc = Counter(filter(lambda x : x.isalpha(), licensePlate.lower()))
        return min([w for w in words if Counter(w) & pc == pc], key=len) 
```
</p>


### Java Solution using character Array
- Author: HanburuGakusei
- Creation Date: Sun Dec 17 2017 12:03:21 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 02:30:13 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public String shortestCompletingWord(String licensePlate, String[] words) {
        String target = licensePlate.toLowerCase();
        int [] charMap = new int[26];
        // Construct the character map
        for(int i = 0 ; i < target.length(); i++){
            if(Character.isLetter(target.charAt(i))) charMap[target.charAt(i) - 'a']++;
        }
        int minLength = Integer.MAX_VALUE;
        String result = null;
        for (int i = 0; i < words.length; i++){
            String word = words[i].toLowerCase();
            if(matches(word, charMap) && word.length() < minLength) {
                minLength = word.length();
                result  = words[i];
            }
        }
        return result;
    }
    private boolean matches(String word, int[] charMap){
        int [] targetMap = new int[26];
        for(int i = 0; i < word.length(); i++){
            if(Character.isLetter(word.charAt(i))) targetMap[word.charAt(i) - 'a']++;
        }
        
        for(int i = 0; i < 26; i++){
            if(charMap[i]!=0 && targetMap[i]<charMap[i]) return false;
        }
        return true;
    }
}
```
</p>


