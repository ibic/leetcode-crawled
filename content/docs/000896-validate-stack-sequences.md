---
title: "Validate Stack Sequences"
weight: 896
#id: "validate-stack-sequences"
---
## Description
<div class="description">
<p>Given two sequences <code>pushed</code> and <code>popped</code>&nbsp;<strong>with distinct values</strong>,&nbsp;return <code>true</code> if and only if this could have been the result of a sequence of push and pop operations on an initially empty stack.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>pushed = <span id="example-input-1-1">[1,2,3,4,5]</span>, popped = <span id="example-input-1-2">[4,5,3,2,1]</span>
<strong>Output: </strong><span id="example-output-1">true</span>
<strong>Explanation: </strong>We might do the following sequence:
push(1), push(2), push(3), push(4), pop() -&gt; 4,
push(5), pop() -&gt; 5, pop() -&gt; 3, pop() -&gt; 2, pop() -&gt; 1
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>pushed = <span id="example-input-2-1">[1,2,3,4,5]</span>, popped = <span id="example-input-2-2">[4,3,5,1,2]</span>
<strong>Output: </strong><span id="example-output-2">false</span>
<strong>Explanation: </strong>1 cannot be popped before 2.
</pre>
</div>
</div>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= pushed.length == popped.length &lt;= 1000</code></li>
	<li><code>0 &lt;= pushed[i], popped[i] &lt; 1000</code></li>
	<li><code>pushed</code> is a permutation of <code>popped</code>.</li>
	<li><code>pushed</code> and <code>popped</code> have distinct values.</li>
</ul>

</div>

## Tags
- Stack (stack)

## Companies
- Google - 25 (taggedByAdmin: false)
- LinkedIn - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Greedy

**Intuition**

We have to push the items in order, so when do we pop them?

If the stack has say, `2` at the top, then if we have to pop that value next, we must do it now.  That's because any subsequent push will make the top of the stack different from `2`, and we will never be able to pop again.

**Algorithm**

For each value, push it to the stack.

Then, greedily pop values from the stack if they are the next values to pop.

At the end, we check if we have popped all the values successfully.

<iframe src="https://leetcode.com/playground/dK26WPnL/shared" frameBorder="0" width="100%" height="344" name="dK26WPnL"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `pushed` and `popped`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Simulation, O(1) Space
- Author: lee215
- Creation Date: Sun Nov 25 2018 12:05:21 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Feb 18 2020 20:19:29 GMT+0800 (Singapore Standard Time)

<p>
# Solution1: Simulating

Initialize am empty `stack`,
iterate and push elements from `pushed` one by one.
Each time, we\'ll try to pop the elements from as many as possibile `popped`.
In the end, we we\'ll check if `stack` is empty.

Time `O(N)`
Space `O(N)`

**C++:**
```cpp
    bool validateStackSequences(vector<int>& pushed, vector<int>& popped) {
        stack<int> stack;
        int i = 0;
        for (int x : pushed) {
            stack.push(x);
            while (stack.size() && stack.top() == popped[i]) {
                stack.pop();
                i++;
            }
        }
        return stack.size() == 0;
    }
```

**Java:**
```java
    public boolean validateStackSequences(int[] pushed, int[] popped) {
        Stack<Integer> stack = new Stack<>();
        int i = 0;
        for (int x : pushed) {
            stack.push(x);
            while (!stack.empty() && stack.peek() == popped[i]) {
                stack.pop();
                i++;
            }
        }
        return stack.empty();
    }
```
**Python:**
```py
    def validateStackSequences(self, pushed, popped):
        stack = []
        i = 0
        for x in pushed:
            stack.append(x)
            while stack and stack[-1] == popped[i]:
                i += 1
                stack.pop()
        return len(stack) == 0
```
<br><br>


# Solution 2: Using `pushed` as Stack

Based on the solution 1,
@aravind-kumar suggested using `pushed` as the stack.
This solution will take `O(1)` extra space,
though it also changed the input.

Time `O(N)`
Space `O(1)`

**C++:**
```cpp
    bool validateStackSequences(vector<int>& pushed, vector<int>& popped) {
        int i = 0, j = 0;
        for (int x : pushed) {
            pushed[i++] = x;
            while (i > 0 && pushed[i - 1] == popped[j])
                --i, ++j;
        }
        return i == 0;
    }
```

**Java:**
```java
    public boolean validateStackSequences(int[] pushed, int[] popped) {
        int i = 0, j = 0;
        for (int x : pushed) {
            pushed[i++] = x;
            while (i > 0 && pushed[i - 1] == popped[j]) {
                --i; ++j;
            }
        }
        return i == 0;
    }
```
**Python:**
```py
    def validateStackSequences(self, pushed, popped):
        i = j = 0
        for x in pushed:
            pushed[i] = x
            while i >= 0 and pushed[i] == popped[j]:
                i, j = i - 1, j + 1
            i += 1
        return i == 0
```
</p>


### [Java/Python 3] straight forward stack solution.
- Author: rock
- Creation Date: Sun Nov 25 2018 12:03:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 26 2019 19:30:18 GMT+0800 (Singapore Standard Time)

<p>
Simulate stack operations:
Loop through the `pushed` array,
1. Keep pushing `pushed` elements into stack if the top element on the stack is different from the current one of `popped`;
2. Keep poping out of the top element from stack if it is same as the current one of `popped`;
3. Check if the stack is empty after loop.

**Analysis:** 
Let `n` be `pushed.length`
the while loop at most runs `n` times since the stack at most pops out `n` times.
**Time & space: O(n).**
```
    public boolean validateStackSequences(int[] pushed, int[] popped) {
        Deque<Integer> stack = new ArrayDeque<>();
        int index = 0;
        for(int p : pushed){
            stack.push(p);
            while (!stack.isEmpty() && stack.peek() == popped[index]){
                stack.pop();
                index++;
            }
        }
        return stack.isEmpty();
    }
```	
```
    def validateStackSequences(self, pushed: List[int], popped: List[int]) -> bool:
        index, stack = 0, []
        for p in pushed:
            stack.append(p)
            while stack and stack[-1] == popped[index]:
                stack.pop()
                index += 1
        return not stack
```
</p>


### C++ 5 lines O(n) stack and two pointers
- Author: votrubac
- Creation Date: Sun Nov 25 2018 15:40:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 25 2018 15:40:38 GMT+0800 (Singapore Standard Time)

<p>
We push elements from ```push[i]``` into the stack, and pop them while the top of the stack equals ```pop[j]```. In the end, if the stack is empty then the sequence is valid.
```
bool validateStackSequences(vector<int> &push, vector<int> &pop) {
  stack<int> s;
  for (auto i = 0, j = 0; i < push.size(); ++i) {
    s.push(push[i]);
    while (!s.empty() && s.top() == pop[j]) s.pop(), ++j;
  }
  return s.empty();
}
```
</p>


