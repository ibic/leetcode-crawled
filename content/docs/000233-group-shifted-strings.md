---
title: "Group Shifted Strings"
weight: 233
#id: "group-shifted-strings"
---
## Description
<div class="description">
<p>Given a string, we can &quot;shift&quot; each of its letter to its successive letter, for example: <code>&quot;abc&quot; -&gt; &quot;bcd&quot;</code>. We can keep &quot;shifting&quot; which forms the sequence:</p>

<pre>
&quot;abc&quot; -&gt; &quot;bcd&quot; -&gt; ... -&gt; &quot;xyz&quot;</pre>

<p>Given a list of <strong>non-empty</strong> strings which contains only lowercase alphabets, group all strings that belong to the same shifting sequence.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b> <code>[&quot;abc&quot;, &quot;bcd&quot;, &quot;acef&quot;, &quot;xyz&quot;, &quot;az&quot;, &quot;ba&quot;, &quot;a&quot;, &quot;z&quot;],</code>
<b>Output:</b> 
[
  [&quot;abc&quot;,&quot;bcd&quot;,&quot;xyz&quot;],
  [&quot;az&quot;,&quot;ba&quot;],
  [&quot;acef&quot;],
  [&quot;a&quot;,&quot;z&quot;]
]
</pre>

</div>

## Tags
- Hash Table (hash-table)
- String (string)

## Companies
- Facebook - 16 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Uber - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My Concise JAVA Solution
- Author: stevenye
- Creation Date: Wed Aug 05 2015 23:43:07 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 12:08:49 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public List<List<String>> groupStrings(String[] strings) {
            List<List<String>> result = new ArrayList<List<String>>();
            Map<String, List<String>> map = new HashMap<String, List<String>>();
            for (String str : strings) {
                int offset = str.charAt(0) - 'a';
                String key = "";
                for (int i = 0; i < str.length(); i++) {
                    char c = (char) (str.charAt(i) - offset);
                    if (c < 'a') {
                        c += 26;
                    }
                    key += c;
                }
                if (!map.containsKey(key)) {
                    List<String> list = new ArrayList<String>();
                    map.put(key, list);
                }
                map.get(key).add(str);
            }
            for (String key : map.keySet()) {
                List<String> list = map.get(key);
                Collections.sort(list);
                result.add(list);
            }
            return result;
        }
    }
</p>


### Python Solution with Explanation (44ms, 84%)
- Author: AnthonyChao
- Creation Date: Sat Apr 27 2019 10:02:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Apr 27 2019 10:02:33 GMT+0800 (Singapore Standard Time)

<p>
We can solve this problem by mapping each `string` in `strings` to a `key` in a `hashmap`. We then return `hashmap.values()`.
```
{
     (1, 1): [\'abc\', \'bcd\', \'xyz\'],  
  (2, 2, 1): [\'acef\'],   
      (25,): [\'az\', \'ba\'],   
         (): [\'a\', \'z\']
}
```
* The key can be represented as a tuple of the "differences" between adjacent characters. Characters map to integers (e.g. `ord(\'a\') = 97`). For example,  \'abc\' maps to `(1,1)` because `ord(\'b\') - ord(\'a\') = 1` and `ord(\'c\') - ord(\'b\') = 1`
* We need to watch out for the "wraparound" case - for example, `\'az\'` and `\'ba\'` should map to the same "shift group" as `a + 1 = b` and `z + 1 = a`. Given the above point, the respective tuples would be `(25,)` (122 - 97) and `(-1,)` (79 - 80) and `az` and `ba` would map to different groups. This is incorrect.
* To account for this case, we add 26 to the difference between letters (smallest difference possible is -25, `za`) and mod by 26. So, (__26__ + 122 - 97) __% 26__ and (__26__ + 79 - 80) __% 26__ both equal `(25,)`
```
def groupStrings(self, strings: List[str]) -> List[List[str]]:
	hashmap = {}
	for s in strings:
		key = ()
		for i in range(len(s) - 1):
			circular_difference = 26 + ord(s[i+1]) - ord(s[i])
			key += (circular_difference % 26,)
		hashmap[key] = hashmap.get(key, []) + [s]
	return list(hashmap.values())
```
* Time complexity would be `O(ab)` where `a` is the total number of strings and `b` is the length of the longest string in strings.
* Space complexity would be `O(n)`, as the most space we would use is the space required for `strings` and the keys of our hashmap.

23 / 23 test cases passed.
Status: Accepted
Runtime: 44 ms
Memory Usage: 13.1 MB

</p>


### 4ms Easy C++ Solution with Explanations
- Author: jianchao-li
- Creation Date: Thu Aug 06 2015 21:18:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 04:26:41 GMT+0800 (Singapore Standard Time)

<p>
The key to this problem is how to identify strings that are in the same shifting sequence. There are different ways to encode this.

In the following code, this manner is adopted: for a string `s` of length `n`, we encode its shifting feature as `"s[1] - s[0], s[2] - s[1], ..., s[n - 1] - s[n - 2],"`.

Then we build an `unordered_map`, using the above shifting feature string as key and strings that share the shifting feature as value. We store all the strings that share the same shifting feature in a `vector`. Well, remember to `sort` the `vector` since the problem requires them to be in lexicographic order :-)

A final note, since the problem statement has given that `"az"` and `"ba"` belong to the same shifting sequence. So if `s[i] - s[i - 1]` is negative, we need to add `26` to it to make it positive and give the correct result. BTW, taking the suggestion of @StefanPochmann, we change the difference from numbers to lower-case alphabets using `'a' + diff`. 

The code is as follows. 

    class Solution {
    public:
        vector<vector<string>> groupStrings(vector<string>& strings) {
            unordered_map<string, vector<string> > mp;
            for (string  s : strings)
                mp[shift(s)].push_back(s);
            vector<vector<string> > groups;
            for (auto m : mp) {
                vector<string> group = m.second;
                sort(group.begin(), group.end());
                groups.push_back(group);
            }
            return groups;
        }
    private:
        string shift(string& s) {
            string t;
            int n = s.length();
            for (int i = 1; i < n; i++) {
                int diff = s[i] - s[i - 1];
                if (diff < 0) diff += 26;
                t += 'a' + diff + ',';
            }
            return t;
        }
    };
</p>


