---
title: "Maximum Number of Darts Inside of a Circular Dartboard"
weight: 1338
#id: "maximum-number-of-darts-inside-of-a-circular-dartboard"
---
## Description
<div class="description">
<p>You have a very large square wall and a circular dartboard placed on the wall.&nbsp;You have been challenged to throw darts into the board blindfolded.&nbsp;Darts thrown at the wall are represented as an array of&nbsp;<code>points</code> on a 2D plane.&nbsp;</p>

<p>Return&nbsp;the maximum number of points that are within or&nbsp;lie&nbsp;on&nbsp;<strong>any</strong> circular dartboard of radius&nbsp;<code>r</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/04/29/sample_1_1806.png" style="width: 186px; height: 159px;" /></p>

<pre>
<strong>Input:</strong> points = [[-2,0],[2,0],[0,2],[0,-2]], r = 2
<strong>Output:</strong> 4
<strong>Explanation:</strong> Circle dartboard with center in (0,0) and radius = 2 contain all points.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/04/29/sample_2_1806.png" style="width: 224px; height: 183px;" /></strong></p>

<pre>
<strong>Input:</strong> points = [[-3,0],[3,0],[2,6],[5,4],[0,9],[7,8]], r = 5
<strong>Output:</strong> 5
<strong>Explanation:</strong> Circle dartboard with center in (0,4) and radius = 5 contain all points except the point (7,8).
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> points = [[-2,0],[2,0],[0,2],[0,-2]], r = 1
<strong>Output:</strong> 1
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> points = [[1,2],[3,5],[1,-1],[2,3],[4,1],[1,3]], r = 2
<strong>Output:</strong> 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= points.length &lt;= 100</code></li>
	<li><code>points[i].length == 2</code></li>
	<li><code>-10^4 &lt;= points[i][0], points[i][1] &lt;= 10^4</code></li>
	<li><code>1 &lt;= r &lt;= 5000</code></li>
</ul>
</div>

## Tags
- Geometry (geometry)

## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] POJ 1981
- Author: lee215
- Creation Date: Sun May 17 2020 12:05:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 22 2020 11:59:50 GMT+0800 (Singapore Standard Time)

<p>
# Reference
POJ: http://poj.org/problem?id=1981
StackOverflow: https://stackoverflow.com/questions/3229459/algorithm-to-cover-maximal-number-of-points-with-one-circle-of-given-radius/3229582#3229582
<br>

# Plan
Well, I doubt if this problem is appropriate for Leetcode or interview.

If you meet this prolem during interview,
you can ask the interviewer:
"Could you give me some APIs?"
<br>

# Explanation
The basic idea is that,
imagine that we are shrink the final circle,
until there are at least 2 points on the circle.

We can enumerate all possible circles based on the given points.
<br>

# Solution 1: O(N^4) brute force

Enumerate all combinations of 3 points,
find the circumcenter of these 3 points.

Use this circumcenter as the center of circle,
and count how many points inside.

If the final result >=3, we won\'t miss it during this search.
But if we still have `res = 1`
we still need to check each two pair distance,
to find out if `res` can be 2.

Time `O(N^4)`
Space `O(1)`
<br>

# Solution 2:
Enumerate all combinations of 2 points,
find the circle going through them with radius = r.

Use this circumcenter as the center of circle,
and count how many points inside.

Also explained by Alexandre C:
Basic observations :
1. I assume the radius is one,
since it doesn\'t change anything.
2. given any two points,
there exists at most two unit circles on which they lie.
3. given a solution circle to your problem,
you can move it until it contains two points of your set
while keeping the same number of points of your set inside it.

The algorithm is then:

1. For each pair of points,
if their distance is < 2,
compute the two unit circles C1 and C2 that pass through them.
2. Compute the number of points of your set inside C1 and C2
3. Take the max.


Time `O(N^3)`
Space `O(1)`

**Python**
```py
    def numPoints(self, A, r):
        res = 1
        for (x1, y1), (x2, y2) in itertools.combinations(A, 2):
            d = ((x1 - x2)**2 + (y1 - y2)**2) / 4.0
            if d > r * r: continue
            x0 = (x1 + x2) / 2.0 + (y2 - y1) * (r * r - d)**0.5 / (d * 4) ** 0.5
            y0 = (y1 + y2) / 2.0 - (x2 - x1) * (r * r - d)**0.5 / (d * 4) ** 0.5
            res = max(res, sum((x - x0)**2 + (y - y0)**2 <= r * r + 0.00001 for x, y in A))
        return res
```
<br>

# Solution 3
Enumerate points,
and draw a circle with radius = r * 2
Find the maximum overlapped circle arc.
(Don\'t really know how to explain it,
please refer to the original problem)

Time `O(N^2logN)`
Space `O(1)`

Credits to @TwiStar
**Python3**
```python
    def numPoints(self, points: List[List[int]], r: int) -> int:
        max_pts = 500
        dis = [[0 for _ in range(max_pts)] for _ in range(max_pts)]

        def getPointsInside(i, r, n):
            # This vector stores alpha and beta and flag
            # is marked true for alpha and false for beta
            angles = []

            for j in range(n):
                if i != j and dis[i][j] <= 2 * r:
                    # acos returns the arc cosine of the complex
                    # used for cosine inverse
                    B = math.acos(dis[i][j] / (2 * r))

                    # arg returns the phase angle of the complex
                    x1, y1 = points[i]
                    x2, y2 = points[j]
                    A = math.atan2(y1 - y2, x1 - x2)
                    alpha = A - B
                    beta = A + B
                    angles.append((alpha, False))
                    angles.append((beta, True))

            # angles vector is sorted and traversed
            angles.sort()
            # count maintains the number of points inside
            # the circle at certain value of theta
            # res maintains the maximum of all count
            cnt, res = 1, 1
            for angle in angles:
                # entry angle
                if angle[1] == 0: cnt += 1
                # exit angle
                else: cnt -= 1

                res = max(cnt, res)

            return res

        # Returns count of maximum points that can lie
        # in a circle of radius r.
        # dis array stores the distance between every
        # pair of points
        n = len(points)
        for i in range(n - 1):
            for j in range(i + 1, n):
                # abs gives the magnitude of the complex
                # number and hence the distance between
                # i and j
                x1, y1 = points[i]
                x2, y2 = points[j]
                dis[i][j] = dis[j][i] = sqrt((x1 - x2)**2 + (y1 - y2)**2)

        # This loop picks a point p
        ans = 0
        # maximum number of points for point arr[i]
        for i in range(n):
            ans = max(ans, getPointsInside(i, r, n))

        return ans
```
</p>


### Python O(n3) and O(n2logn) solution explained in detail with pictures
- Author: lichuan199010
- Creation Date: Sun May 17 2020 12:03:56 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 20 2020 07:17:36 GMT+0800 (Singapore Standard Time)

<p>
We need to think about what defines a circle. Actually, if we know the radius and two points that it passes through, we can find two circles that satisfy the criteria, as shown below with radius r, passing through A and B.
![image](https://assets.leetcode.com/users/lichuan199010/image_1589688960.png)

There are our candidate circles for each pair of points (n^2). For each candidate circle, go through the points list, and count how many points are in there (n). So the total time complexity is O(n^3). The space complexity is O(1).

**How did I come up with this solution during the contest?**
Look at the range of the data, the algorithm has to be n^3 (100^4 will be too slow). 
We at least need to iterate through all points to see if it is inside a circle or not. Therefore, probably we need O(n^2) to determine a circle, which means we use two points to determine a circle. 
Now it becomes obvious that if you have two points and the radius, you can uniquely find the circle.

Please thumb up if you like the solution! Thank you.

**My code is as follows.**

	class Solution:
    def numPoints(self, points: List[List[int]], r: int) -> int:
        # a sub function that calculates the center of the circle
        def circles_from_p1p2r(p1, p2, r):
            (x1, y1), (x2, y2) = p1, p2

            # delta x, delta y between points
            dx, dy = x2 - x1, y2 - y1

            # dist between points
            q = math.sqrt(dx**2 + dy**2)

            # if two points are too far away, there is no such circle
            if q > 2.0*r:
                return []

            # find the halfway point
            x3, y3 = (x1+x2)/2, (y1+y2)/2

            # distance along the mirror line
            d = math.sqrt(r**2-(q/2)**2)

            # One circle
            c1 = [x3 - d*dy/q, y3 + d*dx/q]

            # The other circle
            c2 = [x3 + d*dy/q, y3 - d*dx/q]
            return [c1, c2]

        # now the main function
        res = 0
        n = len(points)

        for p in range(n):
            for q in range(p+1, n):

                # Find the two candidate circle for each pair of points
                TwoCirs = circles_from_p1p2r(points[p], points[q], r)

                # count how many dots are inside the circle
                for center in TwoCirs: # center = TwoCirs[1]
                    cnt = 0

                    for dots in points: # dots =points[0]
                        if (dots[0]-center[0])**2 + (dots[1]-center[1])**2 <= r**2+10**-6:
                            cnt += 1
                    res = max(res, cnt)
                        
        return res if res else 1
As a reference, here is more code on how to find these two circles.
https://rosettacode.org/wiki/Circles_of_given_radius_through_two_points

**Intuition and Proof**
As for why passing through two points is the best candidate circle? You can try to draw it slightly off. You will find that if one of the dots are outside the circle, you loose one point (Worse). If both one of the points are inside the circle, I don\'t have a rigorous proof, but I feel it is intuitive that putting both dots on the line will making the circle better. Hope someone can give a proof.

My friend just told me a good proof. For the best circle, you can always move it until two points are on the circle. I think that is a great proof so that we are listing all valid candidate circles.

**Floating point calculation**
Many people asked about why 10**-6, actually any number > 10^-8, but not too big will do.
The floating point error is 10^-16.
Since we used sqrt, so sqrt(10^-16) = 10^-8.

===================================================

**Another better N2logN solution from ye15**
I really like https://leetcode.com/problems/maximum-number-of-darts-inside-of-a-circular-dartboard/discuss/636439/Python3-angular-sweep-O(N2-logN). Credit goes to @ye15. Please upvote that post if you like the solution.

For each point P, we can use a circle with radius 2r to see what points are included by these circles. For all circles corresponding to a point P, we can use angular sweep to find the maximum number of points in a circle.

Below I attached a few plots for explanation
![image](https://assets.leetcode.com/users/lichuan199010/image_1589730444.png)

An overview of how point Q enters and exits the angular sweep
![image](https://assets.leetcode.com/users/lichuan199010/image_1589730899.png)

Angle-delta and angle+delta defines the entry and exit point
![image](https://assets.leetcode.com/users/lichuan199010/image_1589730906.png)

The process of scanning through each points (similar to a skyline problem.)
![image](https://assets.leetcode.com/users/lichuan199010/image_1589730918.png)

Below are the code from @ye15 (https://leetcode.com/problems/maximum-number-of-darts-inside-of-a-circular-dartboard/discuss/636439/Python3-angular-sweep-O(N2-logN)) with some extra annotations added by me.

	class Solution:
		def numPoints(self, points: List[List[int]], r: int) -> int:
			ans = 1 # at least 1 point can be included by the circle
			for x, y in points: 
				#angles records the entry and exit position for all other points
				angles = []
				for x1, y1 in points: 
				    # for all points that are within 2r radius, it is reachable from point P
					if (x1 != x or y1 != y) and (d:=sqrt((x1-x)**2 + (y1-y)**2)) <= 2*r: 
					
					    # angle is angle between line P-Q and the positive x axis.
						angle = atan2(y1-y, x1-x)
						
						# delta is half of the span between entry and exit
						delta = acos(d/(2*r))
						
						# 1 for entry and -1 for exit so that we can count the number of points in the circle in the next step
						angles.append((angle-delta, +1)) #entry
						angles.append((angle+delta, -1)) #exit
				
				# when we sort all exit and entry, make sure we enter first before exit.
				angles.sort(key=lambda x: (x[0], -x[1]))
				
				# point P will be included, so we start with val=1
				val = 1
				# scan through all other points and count what is the maximum number of points covered.
				for _, entry in angles: 
				
				    # update the global answer
					ans = max(ans, val := val+entry)
			return ans 

What a brilliant and beautiful O(N2logN) solution!
</p>


### [cpp] O(N^3) solution with pictures.
- Author: insomniacat
- Creation Date: Sun May 17 2020 12:03:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 12:05:24 GMT+0800 (Singapore Standard Time)

<p>
The problem can be rephrased: 

Given a set of points and a radius r, how many points a circle with r can cover at most.


Basically, for any two points on the plane, we find the circle with radius r (autually there are 2 such circles) that goes through these two points and check other points one by one.

![image](https://assets.leetcode.com/users/insomniacat/image_1589687936.png)
![image](https://assets.leetcode.com/users/insomniacat/image_1589688171.png)


The distance `tmp`, `d` are straightforward to compute. And in order to compute C1(or C2) , we need to know the theta1. 
Since `theta1 + alpha = theta2 + alpha = 90 degree.`
So `theta1 = theta2 = arctan((A.y - B.y)/(B.x - A.x));`
And
```
C1.x = mid.x - d * sin(theta1), C1.y = mid.y - d * sin(theta1)
C2.x = mid.x + d * sin(theta1), C2.y = mid.y + d * sin(theta1)
```

Time complexity: O(N^3)
Space complexity: O(N) (in face we only need O(1))

Code:
```
const double tol = 1e-6;
    double R;

    struct Point {
        double x, y;
    };
    
    vector<Point> point;
    
    double dis(const Point& a, const Point& b) {
        return sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
    }

	pair<Point, Point> getCenter(const Point& a, const Point& b) {
		Point mid;
		pair<Point, Point> res;
		mid.x = (a.x + b.x) / 2, mid.y = (a.y + b.y) / 2;
		double theta = atan2(a.y - b.y, b.x - a.x);
		double tmp = dis(a, b) / 2;
		double d = sqrt(R * R - tmp*tmp);
		res.first.x = mid.x - d*sin(theta);
		res.first.y = mid.y - d*cos(theta);
		res.second.x = mid.x + d*sin(theta);
		res.second.y = mid.y + d*cos(theta);
		return res;
	}

	int numPoints(vector<vector<int>>& points, int r) {
		int n = points.size();
		R = (double)r;
		point.resize(n);
		for (int i = 0; i < n; ++i) {
			point[i].x = points[i][0];
			point[i].y = points[i][1];
		}
		int ans = 1;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i == j || dis(point[i], point[j]) - 2 * R > tol) continue;
				int cnt = 0;
				auto p = getCenter(point[i], point[j]);
				for (int k = 0; k<n; k++) {
					if (dis(point[k], p.first) - R <= tol) 
						cnt++;
				}
				ans = max(ans, cnt);
				cnt = 0;
				for (int k = 0; k<n; k++) {
					if (dis(point[k], p.second) - R <= tol) 
						cnt++;
				}
				ans = max(ans, cnt);
			}
		}
		return ans;
	}
```
</p>


