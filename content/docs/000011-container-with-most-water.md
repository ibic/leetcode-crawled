---
title: "Container With Most Water"
weight: 11
#id: "container-with-most-water"
---
## Description
<div class="description">
<p>Given <code>n</code> non-negative integers <code>a<sub>1</sub>, a<sub>2</sub>, ..., a<sub>n</sub></code><sub>&nbsp;</sub>, where each represents a point at coordinate <code>(i, a<sub>i</sub>)</code>. <code>n</code> vertical lines are drawn such that the two endpoints of the line <code>i</code> is at <code>(i, a<sub>i</sub>)</code> and <code>(i, 0)</code>. Find two lines, which, together with the x-axis forms a container, such that the container contains the most water.</p>

<p><strong>Notice</strong>&nbsp;that you may not slant the container.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/07/17/question_11.jpg" style="width: 600px; height: 287px;" />
<pre>
<strong>Input:</strong> height = [1,8,6,2,5,4,8,3,7]
<strong>Output:</strong> 49
<strong>Explanation:</strong> The above vertical lines are represented by array [1,8,6,2,5,4,8,3,7]. In this case, the max area of water (blue section) the container can contain&nbsp;is 49.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> height = [1,1]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> height = [4,3,2,1,4]
<strong>Output:</strong> 16
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> height = [1,2,1]
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;=&nbsp;height.length &lt;= 3 * 10<sup>4</sup></code></li>
	<li><code>0 &lt;=&nbsp;height[i] &lt;= 3 * 10<sup>4</sup></code></li>
</ul>

</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)

## Companies
- Amazon - 12 (taggedByAdmin: false)
- Facebook - 8 (taggedByAdmin: false)
- Adobe - 5 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Walmart Labs - 3 (taggedByAdmin: false)
- Flipkart - 2 (taggedByAdmin: false)
- Goldman Sachs - 9 (taggedByAdmin: false)
- Yahoo - 6 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Bloomberg - 4 (taggedByAdmin: true)
- Microsoft - 3 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Lyft - 3 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Citrix - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary
We have to maximize the Area that can be formed between the vertical lines using the shorter line as length and the distance between the lines as the width of the rectangle forming the area.

## Solution
---

#### Approach 1: Brute Force

**Algorithm**

In this case, we will simply consider the area for every possible pair of the lines and find out the maximum area out of those.

<iframe src="https://leetcode.com/playground/gL3JYnab/shared" frameBorder="0" width="100%" height="208" name="gL3JYnab"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Calculating area for all $$\dfrac{n(n-1)}{2}$$ height pairs.
* Space complexity : $$O(1)$$. Constant extra space is used.
<br />
<br />
---
#### Approach 2: Two Pointer Approach

**Algorithm**

The intuition behind this approach is that the area formed between the lines will always be limited by the height of the shorter line. Further, the farther the lines, the more will be the area obtained.

We take two pointers, one at the beginning and one at the end of the array constituting the length of the lines. Futher, we maintain a variable $$\text{maxarea}$$ to store the maximum area obtained till now. At every step, we find out the area formed between them, update $$\text{maxarea}$$ and move the pointer pointing to the shorter line towards the other end by one step.

The algorithm can be better understood by looking at the example below:
```
1 8 6 2 5 4 8 3 7
```

<!--![Water_Continer](https://leetcode.com/media/original_images/11_Container_Water.gif)-->
!?!../Documents/11_Container_Water.json:1000,563!?!

How this approach works?

Initially we consider the area constituting the exterior most lines. Now, to maximize the area, we need to consider the area between the lines of larger lengths. If we try to move the pointer at the longer line inwards, we won't gain any increase in area, since it is limited by the shorter line. But moving the shorter line's pointer could turn out to be beneficial, as per the same argument, despite the reduction in the width. This is done since a relatively longer line obtained by moving the shorter line's pointer might overcome the reduction in area caused by the width reduction.

For further clarification click [here](https://leetcode.com/problems/container-with-most-water/discuss/6099/yet-another-way-to-see-what-happens-in-the-on-algorithm) and for the proof click [here](https://leetcode.com/problems/container-with-most-water/discuss/6089/Anyone-who-has-a-O(N)-algorithm/7268).

<iframe src="https://leetcode.com/playground/f9MCyxXg/shared" frameBorder="0" width="100%" height="276" name="f9MCyxXg"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Single pass.

* Space complexity : $$O(1)$$. Constant space is used.

## Accepted Submission (java)
```java
class Solution {
    public int maxArea(int[] height) {
        if (height.length == 2) {
            return Math.min(height[0], height[1]);
        }
        int left = 0, right = height.length - 1;
        int max = 0;
        while (left <= right) {
            int newMax = Math.min(height[left], height[right]) * (right - left);
            if (newMax > max) {
                max = newMax;
            }

            if (height[left] < height[right]) {
                left++;
            } else {
                right--;
            }
        }
        return max;
    }
}
```

## Top Discussions
### Yet another way to see what happens in the O(n) algorithm
- Author: kongweihan
- Creation Date: Fri Sep 19 2014 15:14:58 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 31 2019 00:18:14 GMT+0800 (Singapore Standard Time)

<p>
The O(n) solution with proof by contradiction doesn\'t look intuitive enough to me. Before moving on, read any [example](https://leetcode.com/problems/container-with-most-water/discuss/6100/Simple-and-clear-proofexplanation) of the algorithm first if you don\'t know it yet.

Here\'s another way to see what happens in a matrix representation:

Draw a matrix where rows correspond to the position of the left line, and columns corresponds to the position of the right line.

For example, say `n=6`. Element at `(2,4)` would corresponds to the case where the left line is at position `2` and the right line is at position `4`. The value of the element is the volume for the case.

In the figures below, `x` means we don\'t need to compute the volume for that case, because:
1. on the diagonal, the two lines are overlapped;
2. the lower left triangle area of the matrix, the two lines are switched and the case is symmetric to the upper right area.

We start by computing the volume at `(1,6)`, denoted by `o`. Now if the left line is shorter than the right line, then moving the right line towards left would only decrease the volume, so all the elements left to `(1,6)` on the first row have smaller volume. Therefore, we don\'t need to compute those cases (crossed by `---`).
 

      1 2 3 4 5 6
    1 x ------- o
    2 x x
    3 x x x 
    4 x x x x
    5 x x x x x
    6 x x x x x x

So we can only move the left line towards right to `2` and compute `(2,6)`. Now if the right line is shorter, all cases below `(2,6)` are eliminated.

      1 2 3 4 5 6
    1 x ------- o
    2 x x       o
    3 x x x     |
    4 x x x x   |
    5 x x x x x |
    6 x x x x x x
And no matter how this `o` path goes, we end up only need to find the max value on this path, which contains `n-1` cases.

      1 2 3 4 5 6
    1 x ------- o
    2 x x - o o o
    3 x x x o | |
    4 x x x x | |
    5 x x x x x |
    6 x x x x x x
Hope this helps. I feel more comfortable seeing things this way.
</p>


### Simple and clear proof/explanation
- Author: StefanPochmann
- Creation Date: Thu May 28 2015 08:49:56 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 21:49:55 GMT+0800 (Singapore Standard Time)

<p>
I've seen some "proofs" for the common O(n) solution, but I found them very confusing and lacking. Some even didn't explain anything but just used lots of variables and equations and were like "Tada! See?". I think mine makes more sense:

**Idea / Proof:**

 1. The widest container (using first and last line) is a good candidate, because of its width. Its water level is the height of the smaller one of first and last line.
 2. All other containers are less wide and thus would need a higher water level in order to hold more water.
 3. The smaller one of first and last line doesn't support a higher water level and can thus be safely removed from further consideration.

**Implementation:** (Python)

    class Solution:
        def maxArea(self, height):
            i, j = 0, len(height) - 1
            water = 0
            while i < j:
                water = max(water, (j - i) * min(height[i], height[j]))
                if height[i] < height[j]:
                    i += 1
                else:
                    j -= 1
            return water

**Further explanation:**

Variables `i` and `j` define the container under consideration. We initialize them to first and last line, meaning the widest container. Variable `water` will keep track of the highest amount of water we managed so far. We compute `j - i`, the width of the current container, and `min(height[i], height[j])`, the water level that this container can support. Multiply them to get how much water this container can hold, and update `water` accordingly. Next remove the smaller one of the two lines from consideration, as justified above in "Idea / Proof". Continue until there is nothing left to consider, then return the result.
</p>


### Simple and fast C++/C with explanation
- Author: StefanPochmann
- Creation Date: Mon Jun 22 2015 08:10:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 19:48:15 GMT+0800 (Singapore Standard Time)

<p>
Start by evaluating the widest container, using the first and the last line. All other possible containers are less wide, so to hold more water, they need to be higher. Thus, after evaluating that widest container, skip lines at both ends that don't support a higher height. Then evaluate that new container we arrived at. Repeat until there are no more possible containers left.

**C++**

    int maxArea(vector<int>& height) {
        int water = 0;
        int i = 0, j = height.size() - 1;
        while (i < j) {
            int h = min(height[i], height[j]);
            water = max(water, (j - i) * h);
            while (height[i] <= h && i < j) i++;
            while (height[j] <= h && i < j) j--;
        }
        return water;
    }

**C**

A bit shorter and perhaps faster because I can use raw int pointers, but a bit longer because I don't have `min` and `max`.

    int maxArea(int* heights, int n) {
        int water = 0, *i = heights, *j = i + n - 1;
        while (i < j) {
            int h = *i < *j ? *i : *j;
            int w = (j - i) * h;
            if (w > water) water = w;
            while (*i <= h && i < j) i++;
            while (*j <= h && i < j) j--;
        }
        return water;
    }
</p>


