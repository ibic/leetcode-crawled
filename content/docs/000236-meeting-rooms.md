---
title: "Meeting Rooms"
weight: 236
#id: "meeting-rooms"
---
## Description
<div class="description">
<p>Given an array of meeting time intervals consisting of start and end times <code>[[s1,e1],[s2,e2],...]</code> (s<sub>i</sub> &lt; e<sub>i</sub>), determine if a person could attend all meetings.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> <code>[[0,30],[5,10],[15,20]]</code>
<b>Output:</b> false
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> [[7,10],[2,4]]
<b>Output:</b> true
</pre>

<p><strong>NOTE:</strong>&nbsp;input types have been changed on April 15, 2019. Please reset to default code definition to get new method signature.</p>

</div>

## Tags
- Sort (sort)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Facebook - 5 (taggedByAdmin: true)
- Bloomberg - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---

#### Approach 1: Brute Force
The straight-forward solution is to compare every two meetings in the array, and see if they conflict with each other (i.e. if they overlap). Two meetings overlap if one of them starts while the other is still taking place.


<iframe src="https://leetcode.com/playground/KV8gJPfL/shared" frameBorder="0" width="100%" height="327" name="KV8gJPfL"></iframe>

**Overlap Condition**

The overlap condition in the code above can be written in a more concise way. Consider two non-overlapping meetings. The earlier meeting ends before the later meeting begins. Therefore, the *minimum* end time of the two meetings (which is the end time of the earlier meeting) is smaller than or equal the *maximum* start time of the two meetings (which is the start time of the later meeting).

![Two non-overlapping intervals](https://leetcode.com/media/original_images/252_NonOverlappingIntervals.png "Odd Even Linked List"){:width="300px"}
{:align="center"}

*Figure 1. Two non-overlapping intervals.*
{:align="center"}

![Two overlapping intervals](https://leetcode.com/media/original_images/252_OverlappingIntervals.png "Odd Even Linked List"){:width="280px"}
{:align="center"}

*Figure 2. Two overlapping intervals.*
{:align="center"}

So the condition can be rewritten as follows.

<iframe src="https://leetcode.com/playground/zsgKxLBb/shared" frameBorder="0" width="100%" height="123" name="zsgKxLBb"></iframe>

**Complexity Analysis**

Because we have two check every meeting with every other meeting, the total run time is $$O(n^2)$$. No additional space is used, so the space complexity is $$O(1)$$.
<br/>
<br/>

---
#### Approach 2: Sorting

The idea here is to sort the meetings by starting time. Then, go through the meetings one by one and make sure that each meeting ends before the next one starts.

<iframe src="https://leetcode.com/playground/QXBskyni/shared" frameBorder="0" width="100%" height="327" name="QXBskyni"></iframe>


**Complexity Analysis**

* Time complexity : $$O(n \log n)$$.
The time complexity is dominated by sorting. Once the array has been sorted, only $$O(n)$$ time is taken to go through the array and determine if there is any overlap.

* Space complexity : $$O(1)$$.
Since no additional space is allocated.
<br/>
</br>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### AC clean Java solution
- Author: jeantimex
- Creation Date: Sun Aug 09 2015 01:19:35 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 05:12:49 GMT+0800 (Singapore Standard Time)

<p>
    public boolean canAttendMeetings(Interval[] intervals) {
      if (intervals == null)
        return false;

      // Sort the intervals by start time
      Arrays.sort(intervals, new Comparator<Interval>() {
        public int compare(Interval a, Interval b) { return a.start - b.start; }
      });
      
      for (int i = 1; i < intervals.length; i++)
        if (intervals[i].start < intervals[i - 1].end)
          return false;
      
      return true;
    }
</p>


### Easy JAVA solution beat 98%
- Author: printf_ll_
- Creation Date: Tue Dec 08 2015 05:29:50 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 08 2015 05:29:50 GMT+0800 (Singapore Standard Time)

<p>
    public boolean canAttendMeetings(Interval[] intervals) {
            int len=intervals.length;
            if(len==0){
                return true;
            }
            int[]begin=new int[len];
            int[]stop=new int[len];
            for(int i=0;i<len;i++){
                begin[i]=intervals[i].start;
                stop[i]=intervals[i].end;
            }
            Arrays.sort(begin);
            Arrays.sort(stop);
            int endT=0;
            for(int i=1;i<len;i++){
                if(begin[i]<stop[i-1]){
                    return false;
                }
            }
            return true;
    }
</p>


### My Python Solution
- Author: lc_cl_aaa
- Creation Date: Sat Aug 08 2015 08:24:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 23 2018 06:47:08 GMT+0800 (Singapore Standard Time)

<p>
    def canAttendMeetings(self, intervals):
        intervals.sort(key=lambda x: x.start)
        
        for i in range(1, len(intervals)):
            if intervals[i].start < intervals[i-1].end:
                return False
            
        return True
</p>


