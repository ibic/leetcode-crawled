---
title: "Check If a String Contains All Binary Codes of Size K"
weight: 1332
#id: "check-if-a-string-contains-all-binary-codes-of-size-k"
---
## Description
<div class="description">
<p>Given a binary string <code>s</code> and an integer <code>k</code>.</p>

<p>Return <em>True</em> if every&nbsp;binary code&nbsp;of length <code>k</code> is a substring of <code>s</code>. Otherwise, return <em>False</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;00110110&quot;, k = 2
<strong>Output:</strong> true
<strong>Explanation:</strong> The binary codes of length 2 are &quot;00&quot;, &quot;01&quot;, &quot;10&quot; and &quot;11&quot;. They can be all found as substrings at indicies 0, 1, 3 and 2 respectively.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;00110&quot;, k = 2
<strong>Output:</strong> true
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;0110&quot;, k = 1
<strong>Output:</strong> true
<strong>Explanation:</strong> The binary codes of length 1 are &quot;0&quot; and &quot;1&quot;, it is clear that both exist as a substring. 
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;0110&quot;, k = 2
<strong>Output:</strong> false
<strong>Explanation:</strong> The binary code &quot;00&quot; is of length 2 and doesn&#39;t exist in the array.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;0000000001011100&quot;, k = 4
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 5 * 10^5</code></li>
	<li><code>s</code> consists of 0&#39;s and 1&#39;s only.</li>
	<li><code>1 &lt;= k &lt;= 20</code></li>
</ul>

</div>

## Tags
- String (string)
- Bit Manipulation (bit-manipulation)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

### Overview

We introduce two different approaches here. The first one will use a built-in set to solve the problem, while in the second one we will write our hash function to get faster performance.

---

#### Approach 1: Set

Recall (part of) the problem:
> Return True if every binary code of length k is a substring of s. Otherwise, return False.

We just need to check every substring with length `k` until we get all the possible binary codes. Since there are two possible char for each place: `0` or `1`, there will be $$2^k$$ possible binary code.

We can continue until we count up to $$2^k$$. We will return `false` if did not get $$2^k$$ counts after iteration. To prevent repeated counting, a set can be used to store the result we previously counted.

Note that `1 << k` is the same as $$2^k$$, and we use the previous one here.

<iframe src="https://leetcode.com/playground/Y2nA4mcm/shared" frameBorder="0" width="100%" height="378" name="Y2nA4mcm"></iframe>

If you like short code, a shorter version in Python presented below. Also, when you shorten codes, it is important that the shortened version still keeps readability.

<iframe src="https://leetcode.com/playground/YSKnPstv/shared" frameBorder="0" width="100%" height="123" name="YSKnPstv"></iframe>


**Complexity Analysis**

Let $$N$$ be the length of `s`.

- Time complexity: $$\mathcal{O}(NK)$$. We need to iterate the string, and use $$\mathcal{O}(K)$$ to calculate the hash of each substring.

- Space complexity: $$\mathcal{O}(NK)$$. There are at most $$N$$ strings with length $$K$$ in the set.

<br/>

---

#### Approach 2: Hash

Maybe you think the approach above is not fast enough. Let's write the hash function ourselves to improve the speed.

Note that we will have at most $$2^k$$ string, can we map each string to a number in [$$0$$, $$2^k-1$$]?

We can. Recall the binary number, we can treat the string as a binary number, and take its decimal form as the hash value. In this case, each binary number has a unique hash value. Moreover, the minimum is all `0`, which is zero, while the maximum is all `1`, which is exactly $$2^k-1$$.

Because we can directly apply bitwise operations to decimal numbers, it is not even necessary to convert the binary number to a decimal number explicitly.

What's more, we can get the current hash from the last one. This method is called [Rolling Hash](https://en.wikipedia.org/wiki/Rolling_hash). All we need to do is to remove the most significant digit and to add a new least significant digit with bitwise operations.

> For example, say `s="11010110"`, and `k=3`, and we just finish calculating the hash of the first substring: `"110"` (`hash` is 4+2=6, or `110`). Now we want to know the next hash, which is the hash of `"101"`. 
>
> We can start from the binary form of our hash, which is `110`. First, we shift left, resulting `1100`. We do not need the first digit, so it is a good idea to do `1100 & 111 = 100`. The all-one `111` helps us to align the digits. Now we need to apply the lowest digit of `"101"`, which is `1`, to our hash, and by using `|`, we get `100 | last_digit = 100 | 1 = 101`.

Write them together, we have: `new_hash = ((old_hash << 1) & all_one) | last_digit_of_new_hash`.

With rolling hash method, we only need $$\mathcal{O}(1)$$ to calculate the next hash, because bitwise operations (`&`, `<<`, `|`, etc.) are only cost $$\mathcal{O}(1)$$.

This time, we can use a simple list to store our hashs, and we will not have hash collision. Those advantages make this approach faster.

<iframe src="https://leetcode.com/playground/LSrcfYSg/shared" frameBorder="0" width="100%" height="429" name="LSrcfYSg"></iframe>

Let $$N$$ be the length of `s`.

- Time complexity: $$\mathcal{O}(N)$$. We need to iterate the string, and use $$\mathcal{O}(1)$$ to calculate the hash of each substring. 

- Space complexity: $$\mathcal{O}(2^k)$$. There are $$2^k$$ elements in the list.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Incorrect problem statement!!!
- Author: claytonjwong
- Creation Date: Sun May 31 2020 00:08:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 31 2020 08:37:44 GMT+0800 (Singapore Standard Time)

<p>
**Incorrect:** Return True if **any** binary code of length k is a substring of s. Otherwise, return False.

**Correct:** Return True if **ALL** binary code of length k is a substring of s. Otherwise, return False.

---

I figured this out during the contest by comparing example 2 and example 4:

**Example 2:**
Input: s = "00110", k = 2
Output: true

**Example 4:**
Input: s = "0110", k = 2
Output: false
Explanation: **The binary code "00" is of length 2 and doesn\'t exist in the array.**

---

**Screenshare:** [Biweekly Contest 27](https://www.youtube.com/watch?v=LR0XYJo8gE8)

---

**Solution #1: Sliding Window + Hash Table**

Use a sliding window of size `K` to add **ALL** strings of size `K` into a set.  Return true if and only if the size of **ALL** strings is equal to 2<sup>K</sup> (ie. all permutations of 0,1 of length `K`).

*Javascript*
```
let hasAllCodes = (S, K, q = [], all = new Set()) => {
    S.split(\'\').forEach(c => {
        q.push(c);
        if (q.length == K) all.add(q.join(\'\')), q.shift();
    })
    return all.size == 1 << K;
};
```

*C++*
```
class Solution {
public:
    using Queue = deque<char>;
    using Set = unordered_set<string>;
    bool hasAllCodes(string S, int K, Queue q = {}, Set all = {}) {
        for (auto c: S) {
            q.push_back(c);
            if (q.size() == K) all.insert({ q.begin(), q.end() }), q.pop_front();
        }
        return all.size() == 1 << K;
    }
};
```

--- 

**Solution #2: Brute-Force:**

Checks all values exist in the input string `S`.

**Note:** this solution is AC in JS, but TLE in C++

*Javascript*
```
let hasAllCodes = (S, K) => {
    let N = 1 << K;
    for (let i = 0; i < N; ++i) {
        let t = _.padStart((i).toString(2), K, \'0\');
        if (S.indexOf(t) == -1)
            return false;
    }
    return true;
};
```

*C++*
```
class Solution {
public:
    bool hasAllCodes(string S, int K) {
        int N = 1 << K;
        for (auto i{ 0 }; i < N; ++i) {
            auto t = bitset<32>(i).to_string();
            t = t.substr(t.size() - K);
            if (S.find(t) == string::npos)
                return false;
        }
        return true;
    }
};
```
</p>


### [Java/Python 3] 4 and 1 liners clean codes using set w brief explanation and analysis.
- Author: rock
- Creation Date: Sun May 31 2020 00:02:44 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 05 2020 00:11:59 GMT+0800 (Singapore Standard Time)

<p>
Using sliding window to traverse all possible binary codes of size k, put them into a set, then check if its size is 2 ^ k.

```java
    public boolean hasAllCodes(String s, int k) {
        Set<String> seen = new HashSet<>();
        for (int i = k; i <= s.length() && seen.size() < 1 << k; ++i) {
            seen.add(s.substring(i - k, i));
        }
        return seen.size() == 1 << k;
    }
```


```python
    def hasAllCodes(self, s: str, k: int) -> bool:
        st = set()
        for i in range(k, len(s) + 1):
            st.add(s[i - k : i])
            if len(st) == 1 << k:
                break
        return len(st) == 1 << k
```
```python
    def hasAllCodes(self, s: str, k: int) -> bool:
        return len({s[i - k : i] for i in range(k, len(s) + 1)}) == 1 << k
```

Please **upvote** if the codes are helpful.

**Analysis:**
Each string cost k space and time. Therefore,

Time: O((s.length() - k) * k), space: O(2 ^ k * k).
</p>


### [Python] 1-line, Follow Up
- Author: lee215
- Creation Date: Sun May 31 2020 01:14:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 31 2020 23:05:30 GMT+0800 (Singapore Standard Time)

<p>
The problem should be:

Given a binary string s and an integer k.
Return True if **all** binary code of length k is a **substring** of s. Otherwise, return False.

**Python**
```
    def hasAllCodes(self, s, k):
        return len({s[i:i + k] for i in xrange(len(s) - k + 1)}) == 2 ** k
```

That\'s easy warm-up.
Then here come the my question.

Given a binary string s and an integer k.
Return True if **all** binary code of length k is a **subsequence** of s. Otherwise, return False.

The difficult will be doubled,
so it 2 lines solution is acceptable to solve in python.


</p>


