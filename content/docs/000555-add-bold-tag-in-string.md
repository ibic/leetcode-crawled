---
title: "Add Bold Tag in String"
weight: 555
#id: "add-bold-tag-in-string"
---
## Description
<div class="description">
Given a string <b>s</b> and a list of strings <b>dict</b>, you need to add a closed pair of bold tag <code>&lt;b&gt;</code> and <code>&lt;/b&gt;</code> to wrap the substrings in s that exist in dict. If two such substrings overlap, you need to wrap them together by only one pair of closed bold tag. Also, if two substrings wrapped by bold tags are consecutive, you need to combine them.
<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> 
s = &quot;abcxyz123&quot;
dict = [&quot;abc&quot;,&quot;123&quot;]
<b>Output:</b>
&quot;&lt;b&gt;abc&lt;/b&gt;xyz&lt;b&gt;123&lt;/b&gt;&quot;
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> 
s = &quot;aaabbcc&quot;
dict = [&quot;aaa&quot;,&quot;aab&quot;,&quot;bc&quot;]
<b>Output:</b>
&quot;&lt;b&gt;aaabbc&lt;/b&gt;c&quot;
</pre>

<p>&nbsp;</p>

<p><b>Constraints:</b></p>

<ul>
	<li>The given dict won&#39;t contain duplicates, and its length won&#39;t exceed 100.</li>
	<li>All the strings in input have length in range [1, 1000].</li>
</ul>

<p><strong>Note:</strong> This question is the same as 758:&nbsp;<a href="https://leetcode.com/problems/bold-words-in-string/">https://leetcode.com/problems/bold-words-in-string/</a></p>

</div>

## Tags
- String (string)

## Companies
- Facebook - 6 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Google - 6 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Brute Force [Accepted]

**Intuition**

Let's try to learn which letters end up bold, since the resulting answer will just be the canonical one - we put bold tags around each group of bold letters.

To do this, we'll check for all occurrences of each word and mark the corresponding letters bold.

**Algorithm**

Let's work on first setting `mask[i] = true` if and only if the `i`-th letter is bold.  For each starting position `i` in `S`, for each `word`, if `S[i]` starts with `word`, we'll set the appropriate letters bold.

Now armed with the correct `mask`, let's try to output the answer.  A letter in position `i` is the first bold letter of the group if `mask[i] && (i == 0 || !mask[i-1])`, and is the last bold letter if `mask[i] && (i == N-1 || !mask[i+1])`.  Alternatively, we could use `itertools.groupby` in Python.

Once we know which letters are the first and last bold letters of a group, we know where to put the `"<b>"` and `"</b>"` tags.

<iframe src="https://leetcode.com/playground/MALzQsdc/shared" frameBorder="0" width="100%" height="500" name="MALzQsdc"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N\sum w_i)$$, where $$N$$ is the length of `S` and $$w_i$$ is the sum of `W`.

* Space Complexity: $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution, boolean array
- Author: shawngao
- Creation Date: Sun Jun 11 2017 11:19:36 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 13:58:31 GMT+0800 (Singapore Standard Time)

<p>
Use a boolean array to mark if character at each position is bold or not. After that, things will become simple.

```
public class Solution {
    public String addBoldTag(String s, String[] dict) {
        boolean[] bold = new boolean[s.length()];
        for (int i = 0, end = 0; i < s.length(); i++) {
            for (String word : dict) {
                if (s.startsWith(word, i)) {
                    end = Math.max(end, i + word.length());
                }
            }
            bold[i] = end > i;
        }
        
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (!bold[i]) {
                result.append(s.charAt(i));
                continue;
            }
            int j = i;
            while (j < s.length() && bold[j]) j++;
            result.append("<b>" + s.substring(i, j) + "</b>");
            i = j - 1;
        }
        
        return result.toString();
    }
}
```
</p>


### Java solution, Same as Merge Interval.
- Author: helloworldzt
- Creation Date: Sun Jun 11 2017 11:20:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 04:59:19 GMT+0800 (Singapore Standard Time)

<p>
 Consider you have string  
	   s = "aaabbcc"
       dict = ["aaa","aab","bc"]
       
       you find the index of each string in dict, conver to an interval, you will get
       
       [[0, 3], [1, 4], [4, 6]]
         aaa     aab      bc
       then combine these intervals
       
       Ater merged, we got [0,6], so we know "aaabbc" needs to be surrounded by tag. 



```
public String addBoldTag(String s, String[] dict) {
        List<Interval> intervals = new ArrayList<>();
        for (String str : dict) {
        	int index = -1;
        	index = s.indexOf(str, index);
        	while (index != -1) {
        		intervals.add(new Interval(index, index + str.length()));
        		index +=1;
        		index = s.indexOf(str, index);
        	}
        }
        System.out.println(Arrays.toString(intervals.toArray()));
        intervals = merge(intervals);
        System.out.println(Arrays.toString(intervals.toArray()));
        int prev = 0;
        StringBuilder sb = new StringBuilder();
        for (Interval interval : intervals) {
        	sb.append(s.substring(prev, interval.start));
        	sb.append("<b>");
        	sb.append(s.substring(interval.start, interval.end));
        	sb.append("</b>");
        	prev = interval.end;
        }
        if (prev < s.length()) {
        	sb.append(s.substring(prev));
        }
        return sb.toString();
    }
	
	class Interval {
		int start, end;
		public Interval(int s, int e) {
			start = s;
			end = e;
		}
		
		public String toString() {
			return "[" + start + ", " + end + "]" ;
		}
	}
	
	public List<Interval> merge(List<Interval> intervals) {
        if (intervals == null || intervals.size() <= 1) {
            return intervals;
        }
        Collections.sort(intervals, new Comparator<Interval>(){
            public int compare(Interval a, Interval b) {
                return a.start - b.start;
            }
        });
        
        int start = intervals.get(0).start;
        int end = intervals.get(0).end;
        List<Interval> res = new ArrayList<>();
        for (Interval i : intervals) {
            if (i.start <= end) {
                end = Math.max(end, i.end);
            } else {
                res.add(new Interval(start, end));
                start = i.start;
                end = i.end;
            }
        }
        res.add(new Interval(start, end));
        return res;
    }
```
</p>


### Python solution beats 100%
- Author: abhishek02
- Creation Date: Wed Jan 24 2018 11:51:33 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 12:22:54 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def addBoldTag(self, s, dict):
        """
        :type s: str
        :type dict: List[str]
        :rtype: str
        """
        status = [False]*len(s)
        final = ""
        for word in dict:
            start = s.find(word)
            last = len(word)
            while start != -1:
                for i in range(start, last+start):
                    status[i] = True
                start = s.find(word,start+1)
        i = 0
        i = 0
        while i < len(s):
            if status[i]:
                final += "<b>"
                while i < len(s) and status[i]:
                    final += s[i]
                    i += 1
                final += "</b>"
            else:
                final += s[i]
                i += 1
        return final
                
```
</p>


