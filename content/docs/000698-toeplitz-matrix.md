---
title: "Toeplitz Matrix"
weight: 698
#id: "toeplitz-matrix"
---
## Description
<div class="description">
<p>A matrix is <em>Toeplitz</em> if every diagonal from top-left to bottom-right has the same element.</p>

<p>Now given an <code>M x N</code> matrix, return&nbsp;<code>True</code>&nbsp;if and only if the matrix is <em>Toeplitz</em>.<br />
&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:
</strong>matrix = [
&nbsp; [1,2,3,4],
&nbsp; [5,1,2,3],
&nbsp; [9,5,1,2]
]
<strong>Output:</strong> True
<strong>Explanation:</strong>
In the above grid, the&nbsp;diagonals are:
&quot;[9]&quot;, &quot;[5, 5]&quot;, &quot;[1, 1, 1]&quot;, &quot;[2, 2, 2]&quot;, &quot;[3, 3]&quot;, &quot;[4]&quot;.
In each diagonal all elements are the same, so the answer is True.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:
</strong>matrix = [
&nbsp; [1,2],
&nbsp; [2,2]
]
<strong>Output:</strong> False
<strong>Explanation:</strong>
The diagonal &quot;[1, 2]&quot; has different elements.
</pre>

<p><br />
<strong>Note:</strong></p>

<ol>
	<li><code>matrix</code> will be a 2D array of integers.</li>
	<li><code>matrix</code> will have a number of rows and columns in range <code>[1, 20]</code>.</li>
	<li><code>matrix[i][j]</code> will be integers in range <code>[0, 99]</code>.</li>
</ol>

<p><br />
<strong>Follow up:</strong></p>

<ol>
	<li>What if the matrix is stored on disk, and the memory is limited such that you can only load at most one row of the matrix into the memory at once?</li>
	<li>What if the matrix is so large that you can only load up a partial row into the memory at once?</li>
</ol>

</div>

## Tags
- Array (array)

## Companies
- Facebook - 13 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]


---
#### Approach #1: Group by Category [Accepted]

**Intuition and Algorithm**

We ask what feature makes two coordinates `(r1, c1)` and `(r2, c2)` belong to the same diagonal?

It turns out two coordinates are on the same diagonal if and only if `r1 - c1 == r2 - c2`.

This leads to the following idea: remember the value of that diagonal as `groups[r-c]`.  If we see a mismatch, the matrix is not Toeplitz; otherwise it is.

<iframe src="https://leetcode.com/playground/DW7Y93pt/shared" frameBorder="0" width="100%" height="293" name="DW7Y93pt"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(M*N)$$.  (Recall in the problem statement that $$M, N$$ are the number of rows and columns in `matrix`.)

* Space Complexity: $$O(M+N)$$.

---
#### Approach #2: Compare With Top-Left Neighbor [Accepted]

**Intuition and Algorithm**

For each diagonal with elements in order $$a_1, a_2, a_3, \dots, a_k$$, we can check $$a_1 = a_2, a_2 = a_3, \dots, a_{k-1} = a_k$$.  The matrix is *Toeplitz* if and only if all of these conditions are true for all (top-left to bottom-right) diagonals.

Every element belongs to some diagonal, and it's previous element (if it exists) is it's top-left neighbor.  Thus, for the square `(r, c)`, we only need to check `r == 0 OR c == 0 OR matrix[r-1][c-1] == matrix[r][c]`.

<iframe src="https://leetcode.com/playground/fkw7aBg5/shared" frameBorder="0" width="100%" height="208" name="fkw7aBg5"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(M*N)$$, as defined in the problem statement.

* Space Complexity: $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution, 4 liner.
- Author: shawngao
- Creation Date: Sun Jan 21 2018 12:01:29 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 22:35:57 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public boolean isToeplitzMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length - 1; i++) {
            for (int j = 0; j < matrix[i].length - 1; j++) {
                if (matrix[i][j] != matrix[i + 1][j + 1]) return false;
            }
        }
        return true;
    }
}
```
</p>


### Python Easy and Concise Solution
- Author: lee215
- Creation Date: Sun Jan 21 2018 12:15:50 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 20 2018 19:12:12 GMT+0800 (Singapore Standard Time)

<p>
```
def isToeplitzMatrix(self, m):
        for i in range(len(m) - 1):
            for j in range(len(m[0]) - 1):
                if m[i][j] != m[i + 1][j + 1]:
                    return False
        return True
```
Make it 1 line:
```
    def isToeplitzMatrix(self, m):
        return all(m[i][j] == m[i+1][j+1] for i in range(len(m)-1) for j in range(len(m[0])-1))
```
Or shorter and more pythonic.
```
    def isToeplitzMatrix(self, m):
        return all(r1[:-1] == r2[1:] for r1,r2 in zip(m, m[1:]))
```
</p>


### Java Solution for Follow-Up 1 & 2
- Author: zyuan18
- Creation Date: Tue Apr 09 2019 11:15:43 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 09 2019 11:15:43 GMT+0800 (Singapore Standard Time)

<p>
Here are what I\'ve thought so far, please let me know if you have any better ideas!

For the follow-up 1, we are only able to load one row at one time, so we have to use a buffer (1D data structure) to store the row info. When next row comes as a streaming flow, we can compare each value (say, matrix[i][j], i>=1, j>=1) to the "upper-left" value in the buffer (buffer[j - 1]); meanwhile, we update the buffer by inserting the 1st element of the current row (matrix[i][0]) to buffer at position 0 (buffer[0]), and removing the last element in the buffer. 
```
class Solution {
    public boolean isToeplitzMatrix(int[][] matrix) {
        if(matrix == null || matrix.length == 0 || matrix[0] == null || matrix[0].length == 0) return true;
        int row = matrix.length;
        int col = matrix[0].length;
        List<Integer> buffer = new LinkedList<>();
        for(int j = 0; j < col; j++) buffer.add(matrix[0][j]);
        for(int i = 1; i < row; i++){
            for(int j = 1; j < col; j++){
                if(buffer.get(j - 1) != matrix[i][j]) return false;
            }
            buffer.remove(buffer.size() - 1);
            buffer.add(0, matrix[i][0]);
        }
        return true;
    }
}
```

For the follow-up 2, we can only load a partial row at one time. We could split the whole matrix vertically into several sub-matrices, while each sub-matrix should have one column overlapped. We repeat the same method in follow-up 1 for each sub-matrix. 

For example:
```
[1 2 3 4 5 6 7 8 9 0]              [1 2 3 4]              [4 5 6 7]              [7 8 9 0]
[0 1 2 3 4 5 6 7 8 9]              [0 1 2 3]              [3 4 5 6]              [6 7 8 9]
[1 0 1 2 3 4 5 6 7 8]     -->      [1 0 1 2]       +      [2 3 4 5]       +      [5 6 7 8]
[2 1 0 1 2 3 4 5 6 7]              [2 1 0 1]              [1 2 3 4]              [4 5 6 7]
[3 2 1 0 1 2 3 4 5 6]              [3 2 1 0]              [0 1 2 3]              [3 4 5 6]
```
</p>


