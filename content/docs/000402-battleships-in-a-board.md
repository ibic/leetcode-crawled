---
title: "Battleships in a Board"
weight: 402
#id: "battleships-in-a-board"
---
## Description
<div class="description">
Given an 2D board, count how many battleships are in it. The battleships are represented with <code>'X'</code>s, empty slots are represented with <code>'.'</code>s. You may assume the following rules:

<ul>
<li>You receive a valid board, made of only battleships or empty slots.</li>
<li>Battleships can only be placed horizontally or vertically. In other words, they can only be made of the shape <code>1xN</code> (1 row, N columns) or <code>Nx1</code> (N rows, 1 column), where N can be of any size.</li>
<li>At least one horizontal or vertical cell separates between two battleships - there are no adjacent battleships.</li>
</ul>

<p><b>Example:</b><br />
<pre>X..X
...X
...X
</pre>
In the above board there are 2 battleships.

<p><b>Invalid Example:</b><br />
<pre>...X
XXXX
...X
</pre>
This is an invalid board that you will not receive - as battleships will always have a cell separating between them.
<p></p>
<p><b>Follow up:</b><br>Could you do it in <b>one-pass</b>, using only <b>O(1) extra memory</b> and <b>without modifying</b> the value of the board?</p>
</div>

## Tags


## Companies
- Facebook - 5 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)
- Amazon - 5 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java Solution
- Author: ben65
- Creation Date: Thu Oct 13 2016 13:37:22 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 01:33:28 GMT+0800 (Singapore Standard Time)

<p>
Going over all cells, we can count only those that are the "first" cell of the battleship. First cell will be defined as the most top-left cell. We can check for first cells by only counting cells that do not have an 'X' to the left _and_ do not have an 'X' above them.

```

    public int countBattleships(char[][] board) {
        int m = board.length;
        if (m==0) return 0;
        int n = board[0].length;
        
        int count=0;
        
        for (int i=0; i<m; i++) {
            for (int j=0; j<n; j++) {
                if (board[i][j] == '.') continue;
                if (i > 0 && board[i-1][j] == 'X') continue;
                if (j > 0 && board[i][j-1] == 'X') continue;
                count++;
            }
        }
        
        return count;
    }
```
</p>


### Share my 7-line code, 1-line core code, 3ms, super easy
- Author: YutingLiu
- Creation Date: Fri Oct 21 2016 04:33:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 10:34:42 GMT+0800 (Singapore Standard Time)

<p>
No need to modify the input matrix.


```
public int countBattleships(char[][] board) {
    int count = 0;
    for(int i=0;i<board.length;i++)
        for(int j=0;j<board[0].length;j++)
            if(board[i][j]=='X' && (i==0 || board[i-1][j]!='X') && (j==0 || board[i][j-1]!='X')) count++;
    return count;
}
```
</p>


### Python solution
- Author: simkieu
- Creation Date: Wed Oct 26 2016 02:14:50 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 02:26:23 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def countBattleships(self, board):
        if len(board) == 0: return 0
        m, n = len(board), len(board[0])
        count = 0
        for i in range(m):
            for j in range(n):
                if board[i][j] == 'X' and (i == 0 or board[i-1][j] == '.') and (j == 0 or board[i][j-1] == '.'):
                    count += 1
        return count
```
</p>


