---
title: "Partition Array Into Three Parts With Equal Sum"
weight: 971
#id: "partition-array-into-three-parts-with-equal-sum"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of integers, return <code>true</code> if and only if we can partition the array into three <strong>non-empty</strong> parts with equal sums.</p>

<p>Formally, we can partition the array if we can find indexes <code>i+1 &lt; j</code> with <code>(A[0] + A[1] + ... + A[i] == A[i+1] + A[i+2] + ... + A[j-1] == A[j] + A[j-1] + ... + A[A.length - 1])</code></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> A = [0,2,1,-6,6,-7,9,1,2,0,1]
<strong>Output:</strong> true
<strong>Explanation: </strong>0 + 2 + 1 = -6 + 6 - 7 + 9 + 1 = 2 + 0 + 1
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> A = [0,2,1,-6,6,7,9,-1,2,0,1]
<strong>Output:</strong> false
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> A = [3,3,6,5,-2,2,5,1,-9,4]
<strong>Output:</strong> true
<strong>Explanation: </strong>3 + 3 = 6 = 5 - 2 + 2 + 5 + 1 - 9 + 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>3 &lt;= A.length &lt;= 50000</code></li>
	<li><code>-10^4&nbsp;&lt;= A[i] &lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] two O(n) time O(1) space codes w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Mar 24 2019 12:15:46 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Mar 12 2020 21:58:35 GMT+0800 (Singapore Standard Time)

<p>
**Method 1: Count valid parts**
1. Loop through the array A, and compute part of sum; if the average value is found, reset the part to 0, and increase the counter;
2. By the end if the average can be seen for at least 3 times and if the total sum is divisible by 3;, return true; otherwise return false.

**note: if the average (sum / 3) found 2 times before the end of the array, then the remaining part is also equals to the average. Therefore, no need to continue after the counter reaches 3.**

```java
    public boolean canThreePartsEqualSum(int[] A) {
        int sum = Arrays.stream(A).sum(), part = 0, average = sum / 3, cnt = 0;
        for (int a : A) {
            part += a;
            if (part == average) { // find the average: sum / 3
                ++cnt; // find an average, increase the counter.
                part = 0; // reset part.
            }
        }
        return cnt >= 3 && sum % 3 == 0;
    } 
```
```python
    def canThreePartsEqualSum(self, A: List[int]) -> bool:
        average, remainder, part, cnt = sum(A) // 3, sum(A) % 3, 0, 0
        for a in A:
            part += a
            if part == average:
                cnt += 1
                part = 0
        return not remainder and cnt >= 3
```

----

**Method 2: 2 pointers greedy**, credit to **@D9night**

1. Staring from the two ends of the input array, accumulate left and right parts till getting the average values;
2. If having found average values on both parts before the two pointers meet, return `true`; otherwise, return `false`.
```java
    public boolean canThreePartsEqualSum(int[] A) {
        int sum = Arrays.stream(A).sum(), average = sum / 3;
        int l = 0, r = A.length - 1; 
        int leftSum = A[l++], rightSum = A[r--];
        do {
            if (leftSum != average)
                leftSum += A[l++];
            if (l < r && rightSum != average)
                rightSum += A[r--];
            if (l <= r && sum % 3 == 0 && leftSum == average && rightSum == average)
                return true;
        } while (l < r);
        return false;
    }
```
```python
    def canThreePartsEqualSum(self, A: List[int]) -> bool:
        l, r, s = 1, len(A) - 2, sum(A)
        leftSum, rightSum, average = A[0], A[-1], s // 3
        while l <= r:
            if l < r and leftSum != average:
                leftSum += A[l]
                l += 1
            if l < r and rightSum != average:
                rightSum += A[r]
                r -= 1    
            if leftSum == average == rightSum and s % 3 == 0:
                return True    
            if l == r:
                break
        return False
```

**Analysis:**
Time: O(n), space: O(1), where n = A.length.
</p>


### C++ 6 lines O(n), target sum
- Author: votrubac
- Creation Date: Sun Mar 24 2019 12:06:47 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 07 2019 12:09:57 GMT+0800 (Singapore Standard Time)

<p>
## Intuition
We can find our target sum by aggregating all numbers and dividing the result by 3.
Then we sum numbers again and track how many times we get the target sum. 
### Solution
> Note: if the target sum is zero, we can reach it more than 3 times (thanks [@prateek_123](https://leetcode.com/prateek_123/) for the find!)
```
bool canThreePartsEqualSum(vector<int>& A, int parts = 0) {
  auto total = accumulate(begin(A), end(A), 0);
  if (total % 3 != 0) return false;
  for (auto i = 0, sum = 0; i < A.size(); ++i) {
    sum += A[i];
    if (sum == (parts + 1) * total / 3) ++parts;
  }
  return parts >= 3;
}
```
### Optimized Solution
It\'s enough to find the target sum two times (thanks [@abdalonimbus](https://leetcode.com/abdalonimbus/) for the optimization!) However, it\'s only true if the target sum is not zero (good catch, [@DYR90](https://leetcode.com/dyr90/)!)

So, we can exit our loop earlier, as soon as we find two (or three) parts (```parts == (total != 0 ? 2 : 3)```
```).
bool canThreePartsEqualSum(vector<int>& A, int parts = 0) {
  auto total = accumulate(begin(A), end(A), 0);
  if (total % 3 != 0) return false;
  for (auto i = 0, sum = 0; i < A.size() && parts < (total != 0 ? 2 : 3); ++i) {
    sum += A[i];
    if (sum == total / 3) ++parts, sum = 0;
  }
  return parts == (total != 0 ? 2 : 3);
}
```
## Complexity Analysis
Runtime: *O(n)*. We process each element in A twice.
Memory: *O(1)*.
</p>


### Detailed Explanation C++ [O(n) Time |  O(1) Space]
- Author: Just__a__Visitor
- Creation Date: Sun Mar 24 2019 12:02:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 24 2019 12:02:10 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**
* Suppose the array was indeed breakable into 3 parts with equal sum. Then the array would look like `S S S`, where `S` represents the sum 
of each segment of the array. Hence, the entire sum would be `S+S+S`=`3S`. 
* Assume that the array is breakable into such segments. Let us compute the prefix sum of the array. Since the array resembles `S S S`, 
therefore the prefix sum would resemble `S 2S 3S`. 
* So we just  need to check if the prefix sum contains `S` `2S` & `3S` (in the same order). Since the sum is already `3S`, we do not need to 
worry about `3S`. All that remains is to check whether the prefix sum contains `S` and `2S` such that `2S` is to the right of `S`. 
[**Update** ---- We do have to check for `3S`.]
---
**Observation** 
* If the accumulated sum is not a multiple of 3, then the array cannot be partitioned in such a way. This is fairly obvious from the above remark.
---
**Algorithm**
* First, calculate the entire sum. If it is not a multiple of 3, return false.
* Intialise 3 variable, `firstFound`, `secondFound`  and `thirdFound` which capture if `S`, `2S` and `3S` have appeared in the desired order or not.
* Start calculating the prefix sum. As soon as you see `S`, set `firstFound` to true and start searching for `2S`. As soon as you see `2S`, set `secondFound` to true and start searching for `3S`.
* In the end, if all are true, it means that `S` was found before `2S` (and `2S` was found before `3S`). Therefore, we can cut our array at the point `S` and `2S` to give us 3 parts with equal sum.
---

**Update**

* Looks like I was wrong. You do have to check the for `3S` as the subarrays may overlap if `S==2S==3S` which happens if `S==0`. So, we need to check if there exists 3 disjoint sets or not. Thanks [rock](https://leetcode.com/rock/) for pointing this out.
---
```
class Solution
{
public:
    bool canThreePartsEqualSum(vector<int>& A);
};

bool Solution :: canThreePartsEqualSum(vector<int>& a)
{
    int sum = accumulate(a.begin(), a.end(), 0);
    
    if(sum%3!=0) return false;
    
    int subSum = sum/3;
    
    bool firstFound=false, secondFound = false, thirdFound = false;
    
    int prefixSum=0;
    for(auto ele : a)
    {
        prefixSum += ele;
        if(!firstFound && prefixSum==subSum) firstFound = true;
        else if(firstFound && !secondFound && prefixSum==subSum*2) secondFound = true;
        else if(firstFound && secondFound && prefixSum==subSum*3) thirdFound = true;
    }
    
    return (firstFound && secondFound && thirdFound);
        
}
```
</p>


