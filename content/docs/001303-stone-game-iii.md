---
title: "Stone Game III"
weight: 1303
#id: "stone-game-iii"
---
## Description
<div class="description">
<p>Alice and Bob continue their&nbsp;games with piles of stones. There are several stones&nbsp;<strong>arranged in a row</strong>, and each stone has an associated&nbsp;value which is an integer given in the array&nbsp;<code>stoneValue</code>.</p>

<p>Alice and Bob take turns, with <strong>Alice</strong> starting first. On each player&#39;s turn, that player&nbsp;can take <strong>1, 2 or 3 stones</strong>&nbsp;from&nbsp;the <strong>first</strong> remaining stones in the row.</p>

<p>The score of each player is the sum of values of the stones taken. The score of each player is <strong>0</strong>&nbsp;initially.</p>

<p>The objective of the game is to end with the highest score, and the winner is the player with the highest score and there could be a tie. The game continues until all the stones have been taken.</p>

<p>Assume&nbsp;Alice&nbsp;and Bob&nbsp;<strong>play optimally</strong>.</p>

<p>Return <em>&quot;Alice&quot;</em> if&nbsp;Alice will win, <em>&quot;Bob&quot;</em> if Bob will win or <em>&quot;Tie&quot;</em> if they end the game with the same score.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> values = [1,2,3,7]
<strong>Output:</strong> &quot;Bob&quot;
<strong>Explanation:</strong> Alice will always lose. Her best move will be to take three piles and the score become 6. Now the score of Bob is 7 and Bob wins.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> values = [1,2,3,-9]
<strong>Output:</strong> &quot;Alice&quot;
<strong>Explanation:</strong> Alice must choose all the three piles at the first move to win and leave Bob with negative score.
If Alice chooses one pile her score will be 1 and the next move Bob&#39;s score becomes 5. The next move Alice will take the pile with value = -9 and lose.
If Alice chooses two piles her score will be 3 and the next move Bob&#39;s score becomes 3. The next move Alice will take the pile with value = -9 and also lose.
Remember that both play optimally so here Alice will choose the scenario that makes her win.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> values = [1,2,3,6]
<strong>Output:</strong> &quot;Tie&quot;
<strong>Explanation:</strong> Alice cannot win this game. She can end the game in a draw if she decided to choose all the first three piles, otherwise she will lose.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> values = [1,2,3,-1,-2,-3,7]
<strong>Output:</strong> &quot;Alice&quot;
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> values = [-1,-2,-3]
<strong>Output:</strong> &quot;Tie&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= values.length &lt;= 50000</code></li>
	<li><code>-1000&nbsp;&lt;= values[i] &lt;= 1000</code></li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] DP, O(1) Space
- Author: lee215
- Creation Date: Sun Apr 05 2020 12:03:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 05 2020 12:37:49 GMT+0800 (Singapore Standard Time)

<p>
# Solution 1: DP
`dp[i]` means, if we ignore before `A[i]`,
what\'s the highest score that Alex can win over the Bob\uFF1F

There are three option for Alice to choose.
Take `A[i]`, win `take - dp[i+1]`
Take `A[i] + A[i+1]`, win `take - dp[i+2]`
Take `A[i] + A[i+1] + A[i+2]`, win `take - dp[i+3]`
dp[i] equals the best outcome of these three solutions.
<br>

# Complexity
Time `O(N^)`
Space `O(N)`
<br>

**Java:**
```java
    public String stoneGameIII(int[] A) {
        int n = A.length, dp[] = new int[n+1];
        for (int i = n - 1; i >= 0; --i) {
            dp[i] = Integer.MIN_VALUE;
            for (int k = 0, take = 0; k < 3 && i + k < n; ++k) {
                take += A[i + k];
                dp[i] = Math.max(dp[i], take - dp[i + k + 1]);
            }
        }
        if (dp[0] > 0) return "Alice";
        if (dp[0] < 0) return "Bob";
        return "Tie";
    }
```

**C++:**
```cpp
    string stoneGameIII(vector<int>& A) {
        int n = A.size();
        vector<int> dp(n, -1e9);
        for (int i = n - 1; i >= 0; --i) {
            for (int k = 0, take = 0; k < 3 && i + k < n; ++k) {
                take += A[i + k];
                dp[i] = max(dp[i], take - (i + k + 1 < n ? dp[i + k + 1] : 0));
            }
        }
        if (dp[0] > 0) return "Alice";
        if (dp[0] < 0) return "Bob";
        return "Tie";
    }
```
<br>

# Solution 2: O(1) Space
We don\'t need `O(N)` space.
We only need last 3 results: dp[i+1],dp[i+2] and dp[i+3].
Time `O(N)`, space `O(1)`

**Java**
```java
    public String stoneGameIII(int[] A) {
        int n = A.length, dp[] = new int[4];
        for (int i = n - 1; i >= 0; --i) {
            dp[i % 4] = Integer.MIN_VALUE;
            for (int k = 0, take = 0; k < 3 && i + k < n; ++k) {
                take += A[i + k];
                dp[i % 4] = Math.max(dp[i % 4], take - dp[(i + k + 1) % 4]);
            }
        }
        if (dp[0] > 0) return "Alice";
        if (dp[0] < 0) return "Bob";
        return "Tie";
    }
```
**C++**
```cpp
    string stoneGameIII(vector<int>& A) {
        vector<int> dp(4);
        for (int i = A.size() - 1; i >= 0; --i) {
            dp[i % 4] = -1e9;
            for (int k = 0, take = 0; k < 3 && i + k < A.size(); ++k)
                dp[i % 4] = max(dp[i % 4], (take += A[i + k]) - dp[(i + k + 1) % 4]);
        }
        return dp[0] == 0 ? "Tie" : (dp[0] > 0 ? "Alice" : "Bob");
    }
```
**Python:**
```py
    def stoneGameIII(self, A):
        dp = [0] * 3
        for i in xrange(len(A) - 1, -1, -1):
            dp[i % 3] = max(sum(A[i:i + k]) - dp[(i + k) % 3] for k in (1, 2, 3))
        return ["Tie", "Alice", "Bob"][cmp(dp[0], 0)]
```

</p>


### [Java/C++/Python] Dynamic Programming
- Author: insane_reckless
- Creation Date: Sun Apr 05 2020 12:13:56 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 06 2020 17:24:22 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**
The problem is typical of dynamic programming. Let dp[i] denote the maximum score one can get if he/she takes stones first at the position i. We only need to compare dp[0]*2 with the total sum of stone values to determine the game result.

**Algorithm**
**Step1**. Calculate the suffix sum of stone values.
**Step2**. For i = n-1, n-2, \u2026, 0, calculate dp[i]. We need to enumerate three possible scenarios which correspond to taking 1, 2, 3 stones at this round.
***state transition***: dp[i] = max(dp[i], suffixSum[i]-suffixSum[k+1] + suffixSum[k+1] - dp[k+1]) = max(dp[i], suffixSum[i] - dp[k+1])\uFF0Cfor k = i, i+1, i+2,  where (suffixSum[k+1] - dp[k+1]) means the score one can get when he/she takes stones secondly at the position k+1.
**Step3**. Compare suffixSum[0] with dp[0]*2.  (Alice score: dp[0], Bob score: suffixSum[0]-dp[0])

**Complexity**
Time: O(n)
Space: O(n)
In fact, we could **reduce the space complexity to O(1)** since we only need suffixSum[i:i+3] and dp[i:i+3] in each iteration of Step2. To make the code clear to understand, the space optimization is not implemented in the code below.

**Java**
```
class Solution {
    public String stoneGameIII(int[] stoneValue) {
        int n = stoneValue.length;
        int[] suffixSum = new int[n+1];
        int[] dp = new int[n+1];
        suffixSum[n] = 0;
        dp[n] = 0;
        for (int i = n - 1; i >= 0; i--)
            suffixSum[i] = suffixSum[i + 1] + stoneValue[i];
        for (int i = n-1; i >= 0; i--) {
            dp[i] = stoneValue[i] + suffixSum[i+1] - dp[i+1];
            for (int k = i+1; k < i+3 && k < n; k++) {
                dp[i] = Math.max(dp[i], suffixSum[i]-dp[k+1]);
            }
        }
        if (dp[0]*2 == suffixSum[0])
            return "Tie";
        else if (dp[0]*2 > suffixSum[0])
            return "Alice";
        else
            return "Bob";
    }
}
```

**C++**
```
class Solution {
public:
    string stoneGameIII(vector<int>& stoneValue) {
        int n = stoneValue.size();
        vector<int> suffixSum(n+1, 0);
        vector<int> dp(n+1, 0);
        for (int i = n - 1; i >= 0; i--)
            suffixSum[i] = suffixSum[i + 1] + stoneValue[i];
        for (int i = n-1; i >= 0; i--) {
            dp[i] = stoneValue[i] + suffixSum[i+1] - dp[i+1];
            for (int k = i+1; k < i+3 && k < n; k++) {
                dp[i] = max(dp[i],  suffixSum[i]-dp[k+1]);
            }
        }
        if (dp[0]*2 == suffixSum[0])
            return "Tie";
        else if (dp[0]*2 > suffixSum[0])
            return "Alice";
        else
            return "Bob";
    }
};
```

**Python**
```
class Solution(object):
    def stoneGameIII(self, stoneValue):
        """
        :type stoneValue: List[int]
        :rtype: str
        """
        n = len(stoneValue)
        suffixSum = [0 for _ in range(n+1)]
        dp = [0 for _ in range(n+1)]
        for i in range(n-1, -1, -1):
            suffixSum[i] = suffixSum[i+1] + stoneValue[i]
        for i in range(n-1, -1, -1):
            dp[i] = stoneValue[i] + suffixSum[i+1] - dp[i+1]
            for k in range(i+1, min(n, i+3)):
                dp[i] = max(dp[i], suffixSum[i] - dp[k+1])
        if dp[0]*2 == suffixSum[0]:
            return "Tie"
        elif dp[0]*2 > suffixSum[0]:
            return "Alice"
        else:
            return "Bob"
```
</p>


### [Java] Minimax & DP Bottom-Up Solutions - Clean code - O(N)
- Author: hiepit
- Creation Date: Sun Apr 05 2020 18:24:53 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 14 2020 09:36:25 GMT+0800 (Singapore Standard Time)

<p>
**Approach 1: Minimax Solution**

Let\'s `Alice` be a `maxPlayer` and `Bob` be a `minPlayer`. On each turn, we need to maximize score of the `maxPlayer` and minimize score of the `minPlayer`. After all, we will check the final score. If `score>0` Alice wins, `score<0` Bob wins else they tie.
More about detail Minimax: https://en.wikipedia.org/wiki/Minimax
```java
class Solution {
    public String stoneGameIII(int[] arr) {
        int score = minimax(arr, 0, 1, new Integer[arr.length][2]);
        if (score > 0) return "Alice";
        if (score < 0) return "Bob";
        return "Tie";
    }
    int minimax(int[] arr, int i, int maxPlayer, Integer[][] dp) {
        if (i >= arr.length) return 0;
        if (dp[i][maxPlayer] != null) return dp[i][maxPlayer];
        int ans = maxPlayer == 1 ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        int score = 0;
        for (int j = i; j < Math.min(arr.length, i + 3); j++) {
            if (maxPlayer == 1) {
                score += arr[j];
                ans = Math.max(ans, score + minimax(arr, j + 1, 0, dp));
            } else {
                score -= arr[j];
                ans = Math.min(ans, score + minimax(arr, j + 1, 1, dp));
            }
        }
        return dp[i][maxPlayer] = ans;
    }
}
```
Time & Space: `O(n)`

**Approach 2: DP Bottom Up ~ 8ms**

`dp[i]` is the maximum difference in the score of the current player against the opponent in the `ith` turn.

There are 3 options for the current player to choose:
- Take `A[i]`, diff1 = take - dp[i+1]
- Take `A[i] + A[i+1]`, diff2 = take - dp[i+2]
- Take `A[i] + A[i+1] + A[i+2]`, diff3 = take - dp[i+3]

We want to maximize difference in the score of the current player against the opponent, so `dp[i] = max(diff1, diff2, diff3)`

```java
class Solution {
    public String stoneGameIII(int[] stoneValue) {
        int n = stoneValue.length;
        int[] dp = new int[n + 1];
        for (int i = n - 1; i >= 0; i--) {
            int take = 0;
            dp[i] = Integer.MIN_VALUE;
            for (int j = i; j < Math.min(n, i + 3); j++) {
                take += stoneValue[j];
                dp[i] = Math.max(dp[i], take - dp[j + 1]);
            }
        }
        int diff = dp[0]; // Alice goes first, starting from the first stone
        if (diff > 0) return "Alice";
        if (diff < 0) return "Bob";
        return "Tie";
    }
}
```
Time & Space: `O(n)`
Reference: @lee215 on this [post](https://leetcode.com/problems/stone-game-iii/discuss/564260/JavaC%2B%2BPython-DP-O(1)-Space)
</p>


