---
title: "Bricks Falling When Hit"
weight: 742
#id: "bricks-falling-when-hit"
---
## Description
<div class="description">
<p>We have a grid of 1s and 0s; the 1s in a cell represent bricks.&nbsp; A brick will not drop if and only if it is directly connected to the top of the grid, or at least one of its (4-way) adjacent bricks will not drop.</p>

<p>We will do some erasures&nbsp;sequentially. Each time we want to do the erasure at the location (i, j), the brick (if it exists) on that location will disappear, and then some other bricks may&nbsp;drop because of that&nbsp;erasure.</p>

<p>Return an array representing the number of bricks that will drop after each erasure in sequence.</p>

<pre>
<strong>Example 1:</strong>
<strong>Input:</strong> 
grid = [[1,0,0,0],[1,1,1,0]]
hits = [[1,0]]
<strong>Output:</strong> [2]
<strong>Explanation: </strong>
If we erase the brick at (1, 0), the brick at (1, 1) and (1, 2) will drop. So we should return 2.</pre>

<pre>
<strong>Example 2:</strong>
<strong>Input:</strong> 
grid = [[1,0,0,0],[1,1,0,0]]
hits = [[1,1],[1,0]]
<strong>Output:</strong> [0,0]
<strong>Explanation: </strong>
When we erase the brick at (1, 0), the brick at (1, 1) has already disappeared due to the last move. So each erasure will cause no bricks dropping.  Note that the erased brick (1, 0) will not be counted as a dropped brick.</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li>The number of rows and columns in the grid will be in the range&nbsp;[1, 200].</li>
	<li>The number of erasures will not exceed the area of the grid.</li>
	<li>It is guaranteed that each erasure will be different from any other erasure, and located inside the grid.</li>
	<li>An erasure may refer to a location with no brick - if it does, no bricks drop.</li>
</ul>

</div>

## Tags
- Union Find (union-find)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Reverse Time and Union-Find [Accepted]

**Intuition**

The problem is about knowing information about the connected components of a graph as we cut vertices.  In particular, we'll like to know the size of the "roof" (component touching the top edge) between each cut.  Here, a cut refers to the erasure of a vertex.

As we may know, a useful data structure for joining connected components is a disjoint set union structure.  The key idea in this problem is that we can use this structure if we work in reverse: instead of looking at the graph as a series of sequential cuts, we'll look at the graph after all the cuts, and reverse each cut.

**Algorithm**

We'll modify our typical disjoint-set-union structure to include a `dsu.size` operation, that tells us the size of this component.  The way we do this is whenever we make a component point to a new parent, we'll also send it's size to that parent.

We'll also include `dsu.top`, which tells us the size of the "roof", or the component connected to the top edge.  We use an *ephemeral* "source" node with label `R * C` where all nodes on the top edge (with row number `0`) are connected to the source node.

For more information on DSU, please look at *Approach #2* in the [article here](https://leetcode.com/articles/redundant-connection/).

Next, we'll introduce `A`, the grid after all the cuts have happened, and initialize our disjoint union structure on the graph induced by `A` (nodes are grid squares with a brick; edges between 4-directionally adjacent nodes).

After, if we get an cut at `(r, c)` but the original `grid[r][c]` was always `0`, then we couldn't have had a meaningful cut - the number of dropped bricks is `0`.

Otherwise, we'll look at the size of the new roof after adding this brick at `(r, c)`, and compare them to find the number of dropped bricks.

Since we were working in reverse time order, we should reverse our working answer to arrive at our final answer.

<iframe src="https://leetcode.com/playground/ieDCWdNy/shared" frameBorder="0" width="100%" height="500" name="ieDCWdNy"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N*Q*\alpha(N * Q))$$, where $$N = R*C$$ is the number of grid squares, $$Q$$ is the length of `hits`, and $$\alpha$$ is the *Inverse-Ackermann function*.

* Space Complexity: $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python Solution by reversely adding hits bricks back
- Author: LuckyPants
- Creation Date: Mon Mar 19 2018 11:42:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 00:47:29 GMT+0800 (Singapore Standard Time)

<p>
We can reverse the problem and count how many new no-dropping bricks are added when we add the bricks reversely. It\'s just the same of counting dropping bricks when erase one brick.

Let m, n = len(grid), len(grid[0]).

Here is the detailed solution:

1. For each hit (i, j), if grid[i][j]==0, set grid[i][j]=-1 otherwise set grid[i][j]=0. Since a hit may happen at an empty position, we need to seperate emptys from bricks.
2. For i in [0, n], do dfs at grid[i][0] and mark no-dropping bricks. Here we get the grid after all hits.
3. Then for each hit (i,j) (reversely), first we check grid[i][j]==-1, if yes, it\'s empty, skip this hit. Then we check whether it\'s connected to any no-dropping bricks or it\'s at the top, if not, it can\'t add any no-dropping bricks, skip this hit. Otherwise we do dfs at grid[i][j], mark new added no-dropping bricks and record amount of them.
4. Return the amounts of new added no-dropping bricks at each hits.

Here is a example, you can walk from the last step to the first step to see how we transfer the question:

![image](https://s3-lc-upload.s3.amazonaws.com/users/luckypants/image_1521450349.png)
![image](https://s3-lc-upload.s3.amazonaws.com/users/luckypants/image_1521450376.png)
![image](https://s3-lc-upload.s3.amazonaws.com/users/luckypants/image_1521450387.png)
![image](https://s3-lc-upload.s3.amazonaws.com/users/luckypants/image_1521450393.png)

Using this method, we only do $O(n)+O(len(hits))$ dfs.



Here is my Python code:

**EDIT**:  Many thanks to @lee215 for pointing mistake out and improving the code:
* I used h[0], h[1] in is_connected, although it works, it\'s a mistake
* Use grid[i][j]-=1 to execute hits and grid[i][j]+=1 to add bricks (So when we have repeating hits, only when we add from 0 to 1, it\'s the true time we hit the brick, following hits are done on empty), it makes the code concise and deals with repeating hits (Although the problem guarantees no repeating hits)

I also simplified some for loops when checking adjacent positions.

```
class Solution:
    def hitBricks(self, grid, hits):
        """
        :type grid: List[List[int]]
        :type hits: List[List[int]]
        :rtype: List[int]
        """

        m, n = len(grid), len(grid[0])
        
        # Connect unconnected bricks and 
        def dfs(i, j):
            if not (0<=i<m and 0<=j<n) or grid[i][j]!=1:
                return 0
            ret = 1
            grid[i][j] = 2
            ret += sum(dfs(x, y) for x, y in [(i-1, j), (i+1, j), (i, j-1), (i, j+1)])
            return ret
        
        # Check whether (i, j) is connected to Not Falling Bricks
        def is_connected(i, j):
            return i==0 or any([0<=x<m and 0<=y<n and grid[x][y]==2 for x, y in [(i-1, j), (i+1, j), (i, j-1), (i, j+1)]])
        
        # Mark whether there is a brick at the each hit
        for i, j in hits:
            grid[i][j] -= 1
                
        # Get grid after all hits
        for i in range(n):
            dfs(0, i)
        
        # Reversely add the block of each hits and get count of newly add bricks
        ret = [0]*len(hits)
        for k in reversed(range(len(hits))):
            i, j = hits[k]
            grid[i][j] += 1
            if grid[i][j]==1 and is_connected(i, j):
                ret[k] = dfs(i, j)-1
            
        return ret
```

</p>


### Union-find Logical Thinking
- Author: GraceMeng
- Creation Date: Tue Nov 20 2018 08:40:24 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 20 2018 08:40:24 GMT+0800 (Singapore Standard Time)

<p>
> When won\'t a brick drop?
> A brick will not drop if it connects to top or its adjacent bricks will not drop.
> That is, the brick will not drop if it belongs to the same connected component with top.
> Problems related to **connect** can be solved by **Disjoint Set**

> We represent the top as 0, and any `grid[x][y] as (x * cols + y + 1)`.
> We union 1-cells on the first row with 0, and any two adjacent 1-cells.

> There are n hits. 
> Instead of checking all cells after each hit, we start from the last hit to the first hit, restoring it and observing the change of `bricksLeft` -  that is actually the corresponding bricks dropped. `change of bricksLeft = change of size(find(0))` 
****
```
class Solution {
    private static final int[][] directions = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
    private int[][] grid;
    private int rows, cols;

    public int[] hitBricks(int[][] grid, int[][] hits) {
        rows = grid.length;
        cols = grid[0].length;
        this.grid = grid;
        
        DisjointSet ds = new DisjointSet(rows * cols + 1);
        
        /** Mark cells to hit as 2. */
        for (int[] hit : hits) {
            if (grid[hit[0]][hit[1]] == 1) grid[hit[0]][hit[1]] = 2;
        }
        
        /** Union around 1 cells. */
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j ++) {
                if (grid[i][j] == 1) unionAround(i, j, ds);
            }
        }
        
        int numBricksLeft = ds.size[ds.find(0)]; // numBricksLeft after the last erasure.
        int i = hits.length - 1; // Index of erasure.
        int[] numBricksDropped = new int[hits.length]; // Number of bricks that will drop after each erasure.
        
        while (i >= 0) {
            int x = hits[i][0];
            int y = hits[i][1];
            if (grid[x][y] == 2) {
                grid[x][y] = 1; // Restore to last erasure.
                unionAround(x, y, ds);
                int newNumBricksLeft = ds.size[ds.find(0)];
                numBricksDropped[i] = Math.max(newNumBricksLeft - numBricksLeft - 1, 0); // Excluding the brick to erase.
                numBricksLeft = newNumBricksLeft;
            }
            i--;
        }
        
        return numBricksDropped;
    }
    
    private void unionAround(int x, int y, DisjointSet ds) {   
        int curMark = mark(x, y);
        
        for (int[] direction : directions) {
            int nx = x + direction[0];
            int ny = y + direction[1];
            if (nx >= 0 && nx < rows && ny >= 0 && ny < cols && grid[nx][ny] == 1) {
                ds.union(curMark, mark(nx, ny));
            }
        }
        
        if(x == 0) ds.union(0, curMark); // Connect to the top of the grid.
    }
    
    private int mark(int x, int y) {
        return x * cols + y + 1;
    }
    
    class DisjointSet {
        int[] parent, size;
        
        public DisjointSet(int n) {
            parent = new int[n];
            size = new int[n];
            Arrays.fill(size, 1);
            for (int i = 0; i < n; i++) { // 0 indicates top of the grid.
                parent[i] = i;
            }
        }
        
        public int find(int x) {
            if (x == parent[x]) return x;
            return parent[x] = find(parent[x]);
        }
        
        public void union(int x, int y) {
            int rootX = find(x);
            int rootY = find(y);
            if (rootX != rootY) {
                parent[rootX] = rootY;
                size[rootY] += size[rootX];
            }
        }
    }
}
```
**(\uFF89>\u03C9<)\uFF89 Vote up, please!**
</p>


### Tricky problem that reverses LC305
- Author: wanglu
- Creation Date: Sun Apr 08 2018 11:49:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 02:34:45 GMT+0800 (Singapore Standard Time)

<p>
I just want to share my thoughts here. 

If no bircks drop, then after all operations. The grid will be look like a pool with multi islands.
for example:
0010000100
0111001110
1111111111
after operations: [0,2], [2,4], [1,2], [0,7]
0000000000
0101001110
1111011111
so total 2 islands. 

Then add bricks back reversely.
[0,7]
0000000100
0101001110
1111011111
the right island attaches top, and its size is 9, which means 8 bricks drop in this operation.

[1,2]
0000000100
0111001110
1111011111
the left island does not reach the top, so no brick drops. 

[2,4]
0000000100
0111001110
1111111111
the left island connects to right island and acttaches top, and left island is original 7, which means 7 bricks drop in this operation.

[0,2]
0010000100
0111001110
1111111111
the island size is just enlarged by 1, which means no brick drops. 

Hope this helps:)






</p>


