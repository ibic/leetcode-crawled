---
title: "Poor Pigs"
weight: 435
#id: "poor-pigs"
---
## Description
<div class="description">
<p>There are 1000 buckets, one and only one of them is poisonous, while the rest are filled with water. They all look identical. If a pig drinks the poison it will die within 15 minutes. What is the minimum amount of pigs you need to figure out which bucket is poisonous within one hour?</p>

<p>Answer this question, and write an algorithm for the general case.</p>

<p>&nbsp;</p>

<p><b>General case: </b></p>

<p>If there are <code>n</code> buckets and a pig drinking poison will die within <code>m</code> minutes, how many pigs (<code>x</code>) you need to figure out the <strong>poisonous</strong>&nbsp;bucket within <code>p</code> minutes? There is exactly one bucket with poison.</p>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>A pig can be allowed to drink simultaneously on as many buckets as one would like, and the feeding takes no time.</li>
	<li>After a pig has instantly finished drinking buckets, there has to be a <strong>cool down time</strong> of <em>m&nbsp;</em>minutes. During this time, only observation is allowed and no feedings at all.</li>
	<li>Any given bucket can be sampled an infinite number of times (by an unlimited number of pigs).</li>
</ol>
</div>

## Tags
- Math (math)

## Companies


## Official Solution
[TOC]

## Solution

---
   
#### Approach 1: Pig as a [qubit](https://en.wikipedia.org/wiki/Qubit) 

**Intuition**

These "strange" questions are now asked by Google, Baidu and IBM 
because of their interest in quantum computing. 
[Quantum bit]((https://en.wikipedia.org/wiki/Qubit)) 
(or _qubit_) is the basic unit of quantum information, it's 
the quantum version of the classical binary bit.
Binary bit has only two states : `0` and `1`, 
and on a very basic level the qubit has more. 
In such questions we deal with an object (here is a pig) which has more than two states.

**How many states does a pig have**

If there is no time to test, i.e. `minutesToTest / minutesToDie = 0`, 
the pig has only one state - `alive`.

If `minutesToTest / minutesToDie = 1` then the pig has a time to die from the poison,
that means that now there are two states available for the pig : `alive` or `dead`.  

One more step. If `minutesToTest / minutesToDie = 2` then there are three available 
states for the pig : `alive` / `dead after the first test` / `dead after the second test`.

![bla](../Figures/458/pigs.png)

> The number of available states for the pig is `states = minutesToTest / minutesToDie + 1`.
        
**How many buckets could test `x` pigs with 2 available states**

One pig could test 2 buckets - let's make him drink from the bucket 
number `1` and then wait `minutesToDie` time. If he is alive - the 
poison is in the bucket number `2`. If he is dead - the poison 
is in the bucket number `1`. 

![bla](../Figures/458/pigs_bucket.png)

The same way two pigs could test $$2^2 = 4$$ buckets

![bla](../Figures/458/2_pigs.png)

![bla](../Figures/458/2_pigs_results.png)

> Hence if one pig has two available states, `x` pigs could
test $$2^x$$ buckets.

**How many buckets could test `x` pigs with `s` available states**

> After the discussion above, the answer is quite obvious : $$s^x$$ buckets.

Let's consider as an example one pig with 3 states, _i.e._ 
`s = minutesToTest / minutesToDie + 1 = 2 + 1 = 3`, and show that
he could test `3` buckets.

![bla](../Figures/458/1_pig_2_tries.png)

![bla](../Figures/458/1_pig_results.png)

**Solution**

Hence the problem is to find `x` such that $$\textrm{states}^x \ge \textrm{buckets}$$,
where `x` is a number of pigs, `states = minutesToTest / minutesToDie + 1`
is a number of states available for each pig, and 
$$\textrm{buckets}$$ is number of buckets.

[The solution is well known](https://en.wikipedia.org/wiki/Exponential_function) :
$$x \ge \log_{\textrm{states}}{\textrm{buckets}}$$. 
To simplify the code let's rewrite the equation with the help of 
[natural logarithms](https://en.wikipedia.org/wiki/List_of_logarithmic_identities#Changing_the_base) :

$$
x \ge \frac{\log \textrm{buckets}}{\log \textrm{states}}
$$

**Implementation**

<iframe src="https://leetcode.com/playground/VZhuCRSn/shared" frameBorder="0" width="100%" height="174" name="VZhuCRSn"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$ since it's a [constant time
solution](https://stackoverflow.com/a/7317571/7775414). 
* Space complexity : $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Another explanation and solution
- Author: StefanPochmann
- Creation Date: Sun Nov 13 2016 19:22:30 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 15:31:43 GMT+0800 (Singapore Standard Time)

<p>
With 2 pigs, poison killing in 15 minutes, and having 60 minutes, we can find the poison in up to 25 buckets in the following way. Arrange the buckets in a 5&times;5 square:

```
 1  2  3  4  5
 6  7  8  9 10
11 12 13 14 15
16 17 18 19 20
21 22 23 24 25
```
Now **use one pig to find the row** (make it drink from buckets 1, 2, 3, 4, 5, wait 15 minutes, make it drink from buckets 6, 7, 8, 9, 10, wait 15 minutes, etc). **Use the second pig to find the column** (make it drink 1, 6, 11, 16, 21, then 2, 7, 12, 17, 22, etc).

Having 60 minutes and tests taking 15 minutes means we can run four tests. If the row pig dies in the third test, the poison is in the third row. If the column pig doesn't die at all, the poison is in the fifth column (this is why we can cover five rows/columns even though we can only run four tests).

With 3 pigs, we can similarly use a 5&times;5&times;5 cube instead of a 5&times;5 square and again use one pig to determine the coordinate of one dimension (one pig drinks layers from top to bottom, one drinks layers from left to right, one drinks layers from front to back). So 3 pigs can solve up to 125 buckets.

In general, we can solve up to (&lfloor;minutesToTest / minutesToDie&rfloor; + 1)<sup>pigs</sup> buckets this way, so just find the smallest sufficient number of pigs for example like this:

    def poorPigs(self, buckets, minutesToDie, minutesToTest):
        pigs = 0
        while (minutesToTest / minutesToDie + 1) ** pigs < buckets:
            pigs += 1
        return pigs

Or with logarithm like I've seen other people do it. That's also where I got the idea from (I didn't really try solving this problem on my own because the judge's solution originally [was wrong](https://discuss.leetcode.com/topic/66856/major-flaw-in-current-algorithm-fixed) and I was more interested in possibly helping to make it right quickly).
</p>


### Solution with detailed explanation
- Author: ifyouseewendy
- Creation Date: Sat Nov 12 2016 22:24:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 21:01:19 GMT+0800 (Singapore Standard Time)

<p>
To be honest, I've spent several days thinking about this question. It's not only fun, but also confusing. it drives me up the wall, especially for its **easy** difficulty. \U0001f629

### Thinking Process

***1. What if you only have one shot?***

> Eg. **4** buckets, **15** mins to die, and **15** mins to test.

The answer is **2**. Suppose we use A and B to represent pigs, we could have

![0_1478958363340_upload-5c96756b-ca9f-41f2-b597-ff57a040ebe8](/uploads/files/1478958363472-upload-5c96756b-ca9f-41f2-b597-ff57a040ebe8.png)

Obviously we could use the binary form to represent the solution.

![0_1478958322597_upload-0812d848-8375-4422-813e-b97bdd37c653](/uploads/files/1478958322907-upload-0812d848-8375-4422-813e-b97bdd37c653.png) 

**Conclusion**: If we have `x` pigs,  we could use them to represent (encode) `2^x` buckets.

***2. What if we could have more than one attempts?***

> Eg. **4** buckets, **15** mins to die, and **30** mins to test.

At the moment, I consider the problem as an encoding problem: *With more attempts, how to use fewer pigs to represent all the buckets?*

I got lost at this step by keep thinking the binary way. After hanging around the forum, I got the idea to change my views. Let's go back to the one shot situation. What does the binary form mean? It's much easier if we regard it as:

+ `0` means the pig does not drink and die.
+ `1` means the pig drinks in the first (and only) round.

We could generalise with:

+ `0` means the pig does not drink and die.
+ `1` means the pig drinks in the first round and die.
+ `2` means the pig drinks in the second round and die.
...
+ `t` means the pig drinks in the t-th round and die.

**Conclusion**: If we have `t` attempts, we could use `t+1`-based number to represent (encode) the buckets. (That's also why the first conclusion uses the `2`-based number)

### Example

> Eg. **8** buckets, **15** mins to die, and **40** mins to test.

We have **2** (`= (40/15).floor`) attempts, as a result we'll use **3**-based number to encode the buckets. 

How many pigs do we need? Answer is 2 (`= Math.log(8, 3).ceil`)

![0_1478960257723_upload-0da5c126-bf43-4183-935d-c2c1a5b5df15](/uploads/files/1478960260176-upload-0da5c126-bf43-4183-935d-c2c1a5b5df15.png) 

For example 3-based number `02` means: the pig **A** does not drink and die, and the pig **B** drinks in the second round and die.

### Ruby Code

```ruby
class FooTest < Minitest::Test
  def poor_pigs(buckets, minutes_to_die, minutes_to_test)
    states = minutes_to_test / minutes_to_die + 1

    Math.log(buckets, states).ceil
  end

  def test_run
    assert_equal 1, poor_pigs(2, 10, 10)
    assert_equal 2, poor_pigs(4, 10, 10)
    assert_equal 3, poor_pigs(8, 10, 10)
    assert_equal 4, poor_pigs(16, 10, 10)

    assert_equal 1, poor_pigs(2, 10, 20)
    assert_equal 2, poor_pigs(4, 10, 20)
    assert_equal 2, poor_pigs(8, 10, 20)
    assert_equal 3, poor_pigs(16, 10, 20)

    assert_equal 5, poor_pigs(1000, 15, 60)
  end
end

```
</p>


### Why should pig die?
- Author: dotasheepliu
- Creation Date: Fri Jun 16 2017 05:11:28 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 23:19:09 GMT+0800 (Singapore Standard Time)

<p>
Pig is our friend. Do not feed pig with poison!
</p>


