---
title: "Max Chunks To Make Sorted II"
weight: 700
#id: "max-chunks-to-make-sorted-ii"
---
## Description
<div class="description">
<p><em>This question is the same as &quot;Max Chunks to Make Sorted&quot; except the integers of the given array are not necessarily distinct, the input array could be up to length <code>2000</code>, and the elements could be up to <code>10**8</code>.</em></p>

<hr />

<p>Given an array <code>arr</code> of integers (<strong>not necessarily distinct</strong>), we split the array into some number of &quot;chunks&quot; (partitions), and individually sort each chunk.&nbsp; After concatenating them,&nbsp;the result equals the sorted array.</p>

<p>What is the most number of chunks we could have made?</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [5,4,3,2,1]
<strong>Output:</strong> 1
<strong>Explanation:</strong>
Splitting into two or more chunks will not return the required result.
For example, splitting into [5, 4], [3, 2, 1] will result in [4, 5, 1, 2, 3], which isn&#39;t sorted.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,1,3,4,4]
<strong>Output:</strong> 4
<strong>Explanation:</strong>
We can split into two chunks, such as [2, 1], [3, 4, 4].
However, splitting into [2, 1], [3], [4], [4] is the highest number of chunks possible.
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li><code>arr</code> will have length in range <code>[1, 2000]</code>.</li>
	<li><code>arr[i]</code> will be an integer in range <code>[0, 10**8]</code>.</li>
</ul>

<p>&nbsp;</p>

</div>

## Tags
- Array (array)

## Companies
- Microsoft - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Sliding Window [Accepted]

**Intuition**

Let's try to find the smallest left-most chunk.

**Algorithm**

Notice that if $$a_1, a_2, \dots, a_m$$ is a chunk, and $$a_1, a_2, \dots, a_n$$ is a chunk ($$m < n$$), then $$a_{m+1}, a_{m+2}, \dots, a_n$$ is a chunk too.  This shows that a greedy approach produces the highest number of chunks.

We know the array `arr` should end up like `expect = sorted(arr)`.  If the count of the first `k` elements minus the count what those elements should be is zero everywhere, then the first `k` elements form a valid chunk.  We repeatedly perform this process.

We can use a variable `nonzero` to count the number of letters where the current count is non-zero.

<iframe src="https://leetcode.com/playground/PpNBmB7e/shared" frameBorder="0" width="100%" height="480" name="PpNBmB7e"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N \log N)$$, where $$N$$ is the length of `arr`

* Space Complexity: $$O(N)$$.

---
#### Approach #2: Sorted Count Pairs [Accepted]

**Intuition**

As in *Approach #1*, let's try to find the smallest left-most chunk, where we have some expectation `expect = sorted(arr)`

If the elements were distinct, then it is enough to find the smallest `k` with `max(arr[:k+1]) == expect[k]`, as this must mean the elements of `arr[:k+1]` are some permutation of `expect[:k+1]`.

Since the elements are not distinct, this fails; but we can amend the cumulative multiplicity of each element to itself to make the elements distinct.

**Algorithm**

Instead of elements `x`, have counted elements `(x, count)` where `count` ranges from `1` to the total number of `x` present in `arr`.

Now `cur` will be the cumulative maximum of `counted[:k+1]`, where we expect a result of `Y = expect[k]`.  We count the number of times they are equal.

<iframe src="https://leetcode.com/playground/mxi9Uze7/shared" frameBorder="0" width="100%" height="500" name="mxi9Uze7"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N \log N)$$, where $$N$$ is the length of `arr`

* Space Complexity: $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution, left max and right min.
- Author: shawngao
- Creation Date: Sun Jan 21 2018 12:16:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 11:08:30 GMT+0800 (Singapore Standard Time)

<p>
Algorithm: Iterate through the array, each time all elements to the left are smaller (or equal) to all elements to the right, there is a new chunck.
Use two arrays to store the left max and right min to achieve O(n) time complexity. Space complexity is O(n) too.
This algorithm can be used to solve ver1 too.
```
class Solution {
    public int maxChunksToSorted(int[] arr) {
        int n = arr.length;
        int[] maxOfLeft = new int[n];
        int[] minOfRight = new int[n];

        maxOfLeft[0] = arr[0];
        for (int i = 1; i < n; i++) {
            maxOfLeft[i] = Math.max(maxOfLeft[i-1], arr[i]);
        }

        minOfRight[n - 1] = arr[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            minOfRight[i] = Math.min(minOfRight[i + 1], arr[i]);
        }

        int res = 0;
        for (int i = 0; i < n - 1; i++) {
            if (maxOfLeft[i] <= minOfRight[i + 1]) res++;
        }

        return res + 1;
    }
}
```
</p>


### [C++] 9 lines, 15ms
- Author: liuchuo
- Creation Date: Sun Jan 28 2018 16:30:40 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 07:15:25 GMT+0800 (Singapore Standard Time)

<p>
```
int maxChunksToSorted(vector<int>& arr) {
        int sum1 = 0, sum2 = 0, ans = 0;
        vector<int> t = arr;
        sort(t.begin(), t.end());
        for(int i = 0; i < arr.size(); i++) {
            sum1 += t[i];
            sum2 += arr[i];
            if(sum1 == sum2) ans++;
        }
	return ans;
    }
```
</p>


### [Java/Python] Easy and Straight Froward
- Author: lee215
- Creation Date: Sun Jan 21 2018 12:19:46 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 11 2020 10:04:58 GMT+0800 (Singapore Standard Time)

<p>
# Soluton 1
Count numbers.
O(nlogn) for sorting
O(n^2) for check
**Python**
```py
    def maxChunksToSorted(self, arr):
        res, c1, c2 = 0, collections.Counter(), collections.Counter()
        for a, b in zip(arr, sorted(arr)):
            c1[a] += 1
            c2[b] += 1
            res += c1 == c2
        return res
```
<br>

# Soluton 2
Count prefix sum.
O(nlogn) for sorting
O(n) for check

**Java**
by @Chengyou0421
```java
    public int maxChunksToSorted(int[] arr) {
        int[] sorted = arr.clone();
        Arrays.sort(sorted);
        int res = 0, sum1 = 0, sum2 = 0;
        for (int i = 0; i < arr.length; i++) {
            sum1 += arr[i];
            sum2 += sorted[i];
            if (sum1 == sum2) res += 1;
        }
        return res;
    }
```
**Python**
Suggested by @here0007
```py
    def maxChunksToSorted(self, A):
        res, s1, s2 = 0, 0, 0
        for a, b in zip(A, sorted(A)):
            s1 += a
            s2 += b
            res += s1 == s2
        return res
```
<br>
</p>


