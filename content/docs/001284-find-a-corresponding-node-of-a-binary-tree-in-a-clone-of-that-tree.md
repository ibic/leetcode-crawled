---
title: "Find a Corresponding Node of a Binary Tree in a Clone of That Tree"
weight: 1284
#id: "find-a-corresponding-node-of-a-binary-tree-in-a-clone-of-that-tree"
---
## Description
<div class="description">
<p>Given two binary trees <code>original</code> and <code>cloned</code> and given a reference to a node <code>target</code> in the original tree.</p>

<p>The <code>cloned</code> tree is a <strong>copy of</strong> the <code>original</code> tree.</p>

<p>Return <em>a reference to the same node</em> in the <code>cloned</code> tree.</p>

<p><strong>Note</strong> that you are <strong>not allowed</strong> to change any of the two trees or the <code>target</code> node and the answer <strong>must be</strong> a reference to a node in the <code>cloned</code> tree.</p>

<p><strong>Follow up:</strong>&nbsp;Solve the problem if repeated values on the tree are allowed.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/02/21/e1.png" style="width: 544px; height: 426px;" />
<pre>
<strong>Input:</strong> tree = [7,4,3,null,null,6,19], target = 3
<strong>Output:</strong> 3
<strong>Explanation:</strong> In all examples the original and cloned trees are shown. The target node is a green node from the original tree. The answer is the yellow node from the cloned tree.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/02/21/e2.png" style="width: 221px; height: 159px;" />
<pre>
<strong>Input:</strong> tree = [7], target =  7
<strong>Output:</strong> 7
</pre>

<p><strong>Example 3:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/02/21/e3.png" style="width: 459px; height: 486px;" />
<pre>
<strong>Input:</strong> tree = [8,null,6,null,5,null,4,null,3,null,2,null,1], target = 4
<strong>Output:</strong> 4
</pre>

<p><strong>Example 4:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/02/21/e4.png" style="width: 555px; height: 239px;" />
<pre>
<strong>Input:</strong> tree = [1,2,3,4,5,6,7,8,9,10], target = 5
<strong>Output:</strong> 5
</pre>

<p><strong>Example 5:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/02/21/e5.png" style="width: 427px; height: 345px;" />
<pre>
<strong>Input:</strong> tree = [1,2,null,3], target = 2
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the <code>tree</code> is in the range <code>[1, 10^4]</code>.</li>
	<li>The values of the nodes of the <code>tree</code> are unique.</li>
	<li><code>target</code> node is a&nbsp;node from the <code>original</code> tree and is not <code>null</code>.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Facebook - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Simple Solution
- Author: aywq2008
- Creation Date: Fri Mar 13 2020 10:13:40 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 13 2020 10:13:40 GMT+0800 (Singapore Standard Time)

<p>
```
	public final TreeNode getTargetCopy(final TreeNode original, final TreeNode cloned, final TreeNode target) {
		if (original == null || original == target)
			return cloned;
		TreeNode res = getTargetCopy(original.left, cloned.left, target);
		if (res != null)
			return res;
		return getTargetCopy(original.right, cloned.right, target);
	}
```
</p>


### [Python] Clean and Pythonic way using iterator(generator) solving followup too
- Author: K_kkkyle
- Creation Date: Fri Mar 13 2020 09:02:24 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 13 2020 09:49:14 GMT+0800 (Singapore Standard Time)

<p>
IMP only if with the followup this problem could be tagged as medium...

``` python
class Solution:
    def getTargetCopy(self, original: TreeNode, cloned: TreeNode, target: TreeNode) -> TreeNode:
        def it(node):
            if node:
				yield node
				yield from it(node.left)
				yield from it(node.right)
            
        for n1, n2 in zip(it(original), it(cloned)):
            if n1 == target:
                return n2
```
</p>


### C++ #minimalizm
- Author: votrubac
- Creation Date: Fri Mar 13 2020 16:42:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 14 2020 08:16:52 GMT+0800 (Singapore Standard Time)

<p>
Searh in the original tree, navigating in the both original and cloned trees in the same direction.

```cpp
TreeNode* getTargetCopy(TreeNode* orig, TreeNode* clone, TreeNode* targ) {
  return orig == nullptr ? nullptr :
    orig == targ ? clone : 
      getTargetCopy(orig->left, clone->left, targ) ?: getTargetCopy(orig->right, clone->right, targ);
}
```
</p>


