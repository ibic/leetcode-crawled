---
title: "Maximum Average Subtree"
weight: 1000
#id: "maximum-average-subtree"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a binary tree, find the maximum average value of any subtree of that tree.</p>

<p>(A subtree of a tree is any node of that tree plus all its descendants. The average value of a tree is the sum of its values, divided by the number of nodes.)</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/04/09/1308_example_1.png" style="width: 132px; height: 123px;" /></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[5,6,1]</span>
<strong>Output: </strong><span id="example-output-1">6.00000</span>
<strong>Explanation: </strong>
For the node with value = 5 we have an average of (5 + 6 + 1) / 3 = 4.
For the node with value = 6 we have an average of 6 / 1 = 6.
For the node with value = 1 we have an average of 1 / 1 = 1.
So the answer is 6 which is the maximum.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>The number of nodes in the tree is between <code>1</code> and <code>5000</code>.</li>
	<li>Each node will have a value between <code>0</code> and <code>100000</code>.</li>
	<li>Answers will be accepted as correct if they are within <code>10^-5</code> of the correct answer.</li>
</ol>

</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 10 (taggedByAdmin: true)
- Atlassian - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] 8 line clean recursive code w/ brief comment and analysis.
- Author: rock
- Creation Date: Sun Jul 14 2019 01:06:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 14 2019 02:12:03 GMT+0800 (Singapore Standard Time)

<p>
```
    public double maximumAverageSubtree(TreeNode root) {
        return helper(root)[2];
    }
    private double[] helper(TreeNode n) {
        if (n == null)  // base case.
            return new double[]{0, 0, 0}; // sum, count  & average of nodes
        double[] l = helper(n.left), r = helper(n.right); // recurse to children.
        double child = Math.max(l[2], r[2]); // larger of the children.
        double sum = n.val + l[0] + r[0], cnt = 1 + l[1] + r[1]; // sum & count of subtree rooted at n.
        double maxOfThree = Math.max(child, sum  / cnt); // largest out of n and its children.
        return new double[]{sum, cnt, maxOfThree}; 
    }
```
**Analysis:**

Time & space: O(n), n is the `#` of nodes in the tree.
</p>


### [Python] Recursion
- Author: lee215
- Creation Date: Sun Jul 14 2019 00:02:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 14 2019 00:39:50 GMT+0800 (Singapore Standard Time)

<p>
## **Complexity**
Time `O(N)`
Space `O(H)`

**Python:**
```python
    def maximumAverageSubtree(self, root):
        self.res = 0
        def helper(root):
            if not root: return [0, 0.0]
            n1, s1 = helper(root.left)
            n2, s2 = helper(root.right)
            n = n1 + n2 + 1
            s = s1 + s2 + root.val
            self.res = max(self.res, s / n)
            return [n, s]
        helper(root)
        return self.res
```

</p>


### [Java] Easy post order recursive O(N)
- Author: yuhwu
- Creation Date: Sun Jul 14 2019 00:02:18 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 27 2019 04:07:55 GMT+0800 (Singapore Standard Time)

<p>
```
    double res;
    public double maximumAverageSubtree(TreeNode root) {
        res = 0;
        sumAndNum(root);
        return res;
    }
    
    //{num,sum}
    public int[] sumAndNum(TreeNode node){
        if(node==null) return new int[]{0,0};
        int sum = node.val;
        int num = 1;
        int[] left = sumAndNum(node.left);
        int[] right = sumAndNum(node.right);
        num+=left[0]+right[0];
        sum+=left[1]+right[1];
        res = Math.max(res, (double)sum/(double)num);
        return new int[]{num,sum};
    }
```
</p>


