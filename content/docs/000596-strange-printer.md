---
title: "Strange Printer"
weight: 596
#id: "strange-printer"
---
## Description
<div class="description">
<p>
There is a strange printer with the following two special requirements:

<ol>
<li>The printer can only print a sequence of the same character each time.</li>
<li>At each turn, the printer can print new characters starting from and ending at any places, and will cover the original existing characters.</li>
</ol>

</p>

<p>
Given a string consists of lower English letters only, your job is to count the minimum number of turns the printer needed in order to print it.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> "aaabbb"
<b>Output:</b> 2
<b>Explanation:</b> Print "aaa" first and then print "bbb".
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> "aba"
<b>Output:</b> 2
<b>Explanation:</b> Print "aaa" first and then print "b" from the second place of the string, which will cover the existing character 'a'.
</pre>
</p>

<p><b>Hint</b>: Length of the given string will not exceed 100.</p>
</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Depth-first Search (depth-first-search)

## Companies
- NetEase - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Dynamic Programming [Accepted]

**Intuition**

It is natural to consider letting `dp(i, j)` be the answer for printing `S[i], S[i+1], ..., S[j]`, but proceeding from here is difficult.  We need the following sequence of deductions:

* Whatever turn creates the final print of `S[i]` might as well be the first turn, and also there might as well only be one print, since any later prints on interval `[i, k]` could just be on `[i+1, k]`.

* Say the first print is on `[i, k]`.  We can assume `S[i] == S[k]`, because if it wasn't, we could print up to the last occurrence of `S[i]` in `[i, k]` for the same result.

* When correctly printing everything in `[i, k]` (with `S[i] == S[k]`), it will take the same amount of steps as correctly printing everything in `[i, k-1]`.  This is because if `S[i]` and `S[k]` get completed in separate steps, we might as well print them first in one step instead.

**Algorithm**

With the above deductions, the algorithm is straightforward.

To compute a recursion for `dp(i, j)`, for every `i <= k <= j` with `S[i] == S[k]`, we have some candidate answer `dp(i, k-1) + dp(k+1, j)`, and we take the minimum of these candidates.  Of course, when `k = i`, the candidate is just `1 + dp(i+1, j)`.

To avoid repeating work, we memoize our intermediate answers `dp(i, j)`.

<iframe src="https://leetcode.com/playground/kmhxPfoW/shared" frameBorder="0" width="100%" height="378" name="kmhxPfoW"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^3)$$ where $$N$$ is the length of `s`.  For each of $$O(N^2)$$ possible states representing a subarray of `s`, we perform $$O(N)$$ work iterating through `k`.

* Space Complexity: $$O(N^2)$$, the size of our `memo`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n^3) DP Solution with Explanation and Simple Optimization
- Author: zhuolikevin
- Creation Date: Sun Aug 20 2017 23:05:16 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 21:54:07 GMT+0800 (Singapore Standard Time)

<p>
The problem wants us to find the number of ways to do something without giving specific steps like how to achieve it. This can be a typical signal that dynamic programming may come to help.

`dp[i][j]` stands for the minimal turns we need for string from index `i` to index `j`.
So we have
- `dp[i][i] = 1`: we need 1 turn to paint a single character.
- `dp[i][i + 1] `
  - `dp[i][i + 1] = 1` if `s.chartAt(i) == s.charAt(i + 1)`
  - `dp[i][i + 1] = 2` if `s.chartAt(i) != s.charAt(i + 1)`

Then we can iteration `len` from 2 to possibly n. For each iteration, we iteration `start` index from 0 to the farthest possible.

- The maximum turns for `dp[start][start + len]` is `len + 1`, i.e. print one character each time.
- We can further divide the substring to two parts: **start -> start+k** and **start+k+1 -> start+len**. It is something as following:
  ```
  index |start  ...  start + k| |start + k + 1 ... start + len|
  char  |  a    ...       b   | |      c       ...      b     |
  ```
    - As shown above, if we have `s.charAt(start + k) == s.charAt(start + len)`, we can **make it in one turn when we print this character (i.e. `b` here)**
    - This case we can reduce our turns to `dp[start][start + k] + dp[start + k + 1][start + len] - 1`

Complete codes are here
```java
class Solution {
    public int strangePrinter(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        
        int n = s.length();
        int[][] dp = new int[n][n];
        for (int i = 0; i < n; i++) {
            dp[i][i] = 1;
            if (i < n - 1) {
                dp[i][i + 1] = s.charAt(i) == s.charAt(i + 1) ? 1 : 2;
            }
        }
        
        for (int len = 2; len < n; len++) {
            for (int start = 0; start + len < n; start++) {
                dp[start][start + len] = len + 1;
                for (int k = 0; k < len; k++) {
                    int temp = dp[start][start + k] + dp[start + k + 1][start + len];
                    dp[start][start + len] = Math.min(
                        dp[start][start + len],
                        s.charAt(start + k) == s.charAt(start + len) ? temp - 1 : temp
                    );
                }
            }
        }
        
        return dp[0][n - 1];
    }
}
```
Time complexity is `O(n^3)`

Some simple optimization. Consecutive repeating characters do not affect our printing as we can always print them together. i.e `aaabbb` is equivalent with `ab`. So we can reduce the string first which somehow reduce `n`

```java
StringBuilder sb = new StringBuilder();
for (int i = 0; i < s.length(); i++) {
    if (i > 0 && s.charAt(i) == s.charAt(i - 1)) {
        continue;
    }
    sb.append(s.charAt(i));
}
s = sb.toString();
```
This helps reduce running time from 60ms to 53ms.
</p>


### Clear Logical Thinking with Code
- Author: GraceMeng
- Creation Date: Mon Jul 23 2018 11:20:35 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 24 2018 05:41:31 GMT+0800 (Singapore Standard Time)

<p>
**Logical Thinking**

The problem is easier to solve if we decrease it to subproblems using **Divide and Conquer**.
**base case**: 
if s contains only 1 character, 1 turn is needed
if s contains 2 characters, 1 turn is needed if they equal to each other, or else, 2 turns are needed
**divide the problem**:
we keep dividing s until the substring contains 1 or 2 characters (as the **base case**)
Take s = "abc" for example,
```
  abc
 /    \
a,bc ab,c (base case here)
```
**conquer the subproblems**:
`turns s needed = min(turns one substring needed + turns the other needed)` (since there are many ways to divide s, we pick the one needs minimum turns)
Please note that, if s = "aba", and we divide it into "a,ba", turns "aba" needed = turns "a" needed + turns "ba" needed `- 1` (aaa => aba rather than a => ab => aba).

To avoid duplicate calculations, we implement the idea above using **Bottom-up Dynamic Programming**.
**state**: `state[i][j]` turns needed to print s[i .. j] (both inclusive)
**aim state**: `state[0][n - 1]` (n = s.length() - 1)
**state transition**: 
```
state[i][i] = 1;
state[i][i + 1] = 1 if s[i] == s[i + 1]
state[i][i + 1] = 2 if s[i] != s[i + 1]
state[i][j] = min(state[i][k] + state[k + 1][j]) for i <= k <= j - 1
	please note that, if s[i] equals to s[j] , state[i][j] should -1
```
**Note**
We observe that states defined on small sections contribute to the states defined on the larger one.
We define `j = i + dist` to represent sections easily.
Take i = 2, dist = 3 for example,
`state[2][5] = state[2][3] + state[4][5]`,
state[4][5] should be calculated before `state[2][5]`, so **i should keep decreasing**.
`dist = |5 - 4|` should be counted before `dist = |5 - 2|`, so **dist should be increasing**.

**Clear Java Code**
```
    public int strangePrinter(String s) {

        if (s == null || s.length() == 0) {
            return 0;
        }

        int n = s.length();
        int[][] state = new int[n][n];

        for (int i = 0; i < n; i++) {
            state[i][i] = 1;
        }

        for (int i = n - 1; i >= 0; i--) {
            for (int dist = 1; dist + i < n; dist++) {
                int j = dist + i;
                if (dist == 1) {
                    state[i][j] = (s.charAt(i) == s.charAt(j)) ? 1 : 2;
                    continue;
                }
                state[i][j] = Integer.MAX_VALUE;
                for (int k = i; k + 1 <= j; k++) {
                    int tmp = state[i][k] + state[k + 1][j];
                    state[i][j] = Math.min(state[i][j], tmp);
                }
                if (s.charAt(i) == s.charAt(j)) {
                    state[i][j]--;
                }
            }
        }

        return state[0][n - 1];
    }
```
**I would appreciate your VOTE UP ;)**
</p>


### One suggestion for all solutions
- Author: lee215
- Creation Date: Mon Sep 18 2017 22:43:22 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 12 2018 06:23:49 GMT+0800 (Singapore Standard Time)

<p>
I suggest to do this treatment, before go directly DP.

Shorten the original string, like reduce ```aaabbb``` to ```ab```.

The same consecutive characters won't change the result and this really help improve the efficiency.
 
Besides, in python, it takes only 1 line to do it:
```
s = ''.join(a for a, b in zip(s, '#' + s) if a != b)
````
or use regex
````
s = re.sub(r'(.)\1*', r'\1', s)
`````

Edited after stefan's suggestion.
</p>


