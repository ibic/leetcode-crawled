---
title: "Apples & Oranges"
weight: 1579
#id: "apples-oranges"
---
## Description
<div class="description">
<p>Table: <code>Sales</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| sale_date     | date    |
| fruit         | enum    | 
| sold_num      | int     | 
+---------------+---------+
(sale_date,fruit) is the primary key for this table.
This table contains the sales of &quot;apples&quot; and &quot;oranges&quot; sold each day.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to&nbsp;report the&nbsp;difference between number of <strong>apples</strong> and <strong>oranges</strong> sold each day.</p>

<p>Return the result table <strong>ordered</strong> by sale_date in format (&#39;YYYY-MM-DD&#39;).</p>

<p>The query result format is in the following example:</p>

<p>&nbsp;</p>

<pre>
<code>Sales</code> table:
+------------+------------+-------------+
| sale_date  | fruit      | sold_num    |
+------------+------------+-------------+
| 2020-05-01 | apples     | 10          |
| 2020-05-01 | oranges    | 8           |
| 2020-05-02 | apples     | 15          |
| 2020-05-02 | oranges    | 15          |
| 2020-05-03 | apples     | 20          |
| 2020-05-03 | oranges    | 0           |
| 2020-05-04 | apples     | 15          |
| 2020-05-04 | oranges    | 16          |
+------------+------------+-------------+

Result table:
+------------+--------------+
| sale_date  | diff         |
+------------+--------------+
| 2020-05-01 | 2            |
| 2020-05-02 | 0            |
| 2020-05-03 | 20           |
| 2020-05-04 | -1           |
+------------+--------------+

Day 2020-05-01, 10 apples and 8 oranges were sold (Difference  10 - 8 = 2).
Day 2020-05-02, 15 apples and 15 oranges were sold (Difference 15 - 15 = 0).
Day 2020-05-03, 20 apples and 0 oranges were sold (Difference 20 - 0 = 20).
Day 2020-05-04, 15 apples and 16 oranges were sold (Difference 15 - 16 = -1).
</pre>

</div>

## Tags


## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Clear MySQL solution with Case When
- Author: ShooL_ds
- Creation Date: Mon Jun 08 2020 01:20:50 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 08 2020 01:20:50 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below

select sale_date, sum(case when fruit=\'apples\' then sold_num else -sold_num end) as diff
from sales
group by sale_date
```
</p>


### Clean MySQL, SUM(IF()), GROUP BY
- Author: weiweiutd
- Creation Date: Fri Jul 24 2020 04:07:43 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 24 2020 04:07:43 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below
SELECT
    sale_date,
    SUM(IF(fruit = \'apples\', sold_num, -sold_num)) AS diff
FROM Sales
GROUP BY sale_date
```
</p>


### Simple solution
- Author: yuang5
- Creation Date: Tue May 19 2020 05:34:13 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 19 2020 05:34:13 GMT+0800 (Singapore Standard Time)

<p>
SELECT a.sale_date, (a.sold_num- b.sold_num) as diff  
from Sales a, Sales b
where a.sale_date = b.sale_date and a.fruit=\'apples\' and a.fruit!=b.fruit
order by a.sale_date
</p>


