---
title: "Similar RGB Color"
weight: 739
#id: "similar-rgb-color"
---
## Description
<div class="description">
<p>In the following, every capital letter represents some hexadecimal digit from <code>0</code> to <code>f</code>.</p>

<p>The red-green-blue color <code>&quot;#AABBCC&quot;</code>&nbsp;can be written&nbsp;as&nbsp;<code>&quot;#ABC&quot;</code> in&nbsp;shorthand.&nbsp; For example, <code>&quot;#15c&quot;</code> is shorthand for the color <code>&quot;#1155cc&quot;</code>.</p>

<p>Now, say the similarity between two colors <code>&quot;#ABCDEF&quot;</code> and <code>&quot;#UVWXYZ&quot;</code> is <code>-(AB - UV)^2 -&nbsp;(CD - WX)^2 -&nbsp;(EF - YZ)^2</code>.</p>

<p>Given the color <code>&quot;#ABCDEF&quot;</code>, return a 7 character color&nbsp;that is most similar to <code>#ABCDEF</code>, and has a shorthand (that is, it can be represented as some <code>&quot;#XYZ&quot;</code></p>

<pre>
<strong>Example 1:</strong>
<strong>Input:</strong> color = &quot;#09f166&quot;
<strong>Output:</strong> &quot;#11ee66&quot;
<strong>Explanation: </strong> 
The similarity is -(0x09 - 0x11)^2 -(0xf1 - 0xee)^2 - (0x66 - 0x66)^2 = -64 -9 -0 = -73.
This is the highest among any shorthand color.
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li><code>color</code> is a string of length <code>7</code>.</li>
	<li><code>color</code> is a valid RGB color: for <code>i &gt; 0</code>, <code>color[i]</code> is a hexadecimal digit from <code>0</code> to <code>f</code></li>
	<li>Any answer which has the same (highest)&nbsp;similarity as the best answer will be accepted.</li>
	<li>All inputs and outputs should use lowercase letters, and the output is 7 characters.</li>
</ul>

</div>

## Tags
- Math (math)
- String (string)

## Companies
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Brute Force [Accepted]

**Intuition**

For each possible shorthand-RGB color from `"#000"` to `"#fff"`, let's find it's similarity to the given color.  We'll take the best one.

**Algorithm**

This problem is straightforward, but there are a few tricky implementation details.

To iterate over each shorthand color, we'll use an integer based approach, (though other ones exist.)  Each digit in the shorthand `"#RGB"` could be from 0 to 15.  This leads to a color of 17 * R * (1 << 16) + 17 * G * (1 << 8) + 17 * B.  The reason for the 17 is because a hexadecimal value of `0x22` is equal to `2 * 16 + 2 * 1` which is `2 * (17)`.  The other values for red and green work similarly, just shifted up by 8 or 16 bits.

To determine the similarity between two colors represented as integers, we'll sum the similarity of each of their colored components separately.  For a color like `hex1`, it has 3 colored components `r1 = (hex1 >> 16) % 256, g1 = (hex1 >> 8) % 256, b1 = (hex1 >> 0) % 256`.  Then, the first addend in the similarity is `-(r1 - r2) * (r1 - r2)`, etc.

To convert an integer back to a hex string, we'll use `String.format`.  The `06` refers to a zero padded string of length 6, while `x` refers to lowercase hexadecimal.

Finally, it should be noted that the answer is always unique.  Indeed, for two numbers that differ by 17, every integer is closer to one than the other.  For example, with `0` and `17`, `8` is closer to `0` and `9` is closer to `17` - there is no number that is tied in closeness.

<iframe src="https://leetcode.com/playground/vG2WvzuB/shared" frameBorder="0" width="100%" height="480" name="vG2WvzuB"></iframe>

**Complexity Analysis**

* Time and Space Complexity:  $$O(1)$$.

---
#### Approach #2: Rounding By Component [Accepted]

**Intuition and Algorithm**

Because color similarity is a sum of the similarity of individual color components, we can treat each colored component separately and combine the answer.

As in the previous approach, we want the colored component to be the closest to a multiple of 17.  We can just round it to the closest such value.

<iframe src="https://leetcode.com/playground/SgMHcuU2/shared" frameBorder="0" width="100%" height="242" name="SgMHcuU2"></iframe>

**Complexity Analysis**

* Time and Space Complexity:  $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple python solution
- Author: li-_-il
- Creation Date: Mon Mar 19 2018 12:57:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 26 2018 10:31:25 GMT+0800 (Singapore Standard Time)

<p>
```
    def similarRGB(self, color):
        def geClosest(s):
            return min([\'00\', \'11\', \'22\', \'33\', \'44\', \'55\', \'66\', \'77\', \'88\', \'99\', \'aa\', \'bb\', \'cc\', \'dd\', \'ee\', \'ff\'],
                key=lambda x: abs(int(s, 16) - int(x, 16)))

        res = [geClosest(color[i:i+2]) for i in range(1, len(color), 2)]
        return \'#\' + \'\'.join(res)

```
</p>


### Concise java solution
- Author: larrywang2014
- Creation Date: Mon Mar 19 2018 12:56:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 22:58:40 GMT+0800 (Singapore Standard Time)

<p>
    public String similarRGB(String color) {
        StringBuilder sb = new StringBuilder(color.length());
        sb.append("#");
        for (int i = 1; i < color.length(); i += 2){
            sb.append(getHexDigits(color.charAt(i), color.charAt(i + 1)));
        }
        return sb.toString();
    }
    
    private String getHexDigits(char c1, char c2){
        int d1 = Character.isDigit(c1)? c1 - \'0\': 10 + c1 - \'a\';
        int d2 = Character.isDigit(c2)? c2 - \'0\': 10 + c2 - \'a\';
        
        int sum       = d1 * 16 + d2;
        int index     = sum / 17; // [ 0x00(0) , 0x11(17), 0x22(34),  0x33(51), ........., 0xff(255) ]
        int remainder = sum % 17;
        if (remainder > 17 / 2){
            index++;
        }
        
        char c = 0 <= index && index <= 9? (char)(\'0\' + index): (char)(\'a\' + index - 10);
        return String.valueOf(c) + String.valueOf(c);
    }
</p>


### Simple java sol
- Author: lakshl
- Creation Date: Tue Jun 12 2018 02:29:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 15 2018 11:55:14 GMT+0800 (Singapore Standard Time)

<p>
\'\'\'
private String getCloset(String color){
	        
        String[] li = {"00", "11", "22", "33", "44", "55", "66", "77", "88", "99", "aa", "bb", "cc", "dd", "ee", "ff"};
        StringBuilder sb = new StringBuilder();
        sb.append("#");
        
        for (int c = 1 ;c < color.length(); c=c+2) {
            
            String hex = color.substring(c, c+2);
            
            int num = Integer.MAX_VALUE;
            String rgb = "";
            
            for (String i : li){
                int res  = Math.abs(Integer.parseInt(hex, 16) - Integer.parseInt(i, 16));
                if (res < num){
                    num = res;
                    rgb = i;
                }
            }
            sb.append(rgb);
        }
        
        
        return sb.toString();
    }
		\'\'\'
</p>


