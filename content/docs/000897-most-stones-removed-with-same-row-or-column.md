---
title: "Most Stones Removed with Same Row or Column"
weight: 897
#id: "most-stones-removed-with-same-row-or-column"
---
## Description
<div class="description">
<p>On a 2D plane, we place stones at some integer coordinate points.&nbsp; Each coordinate point may have at most one stone.</p>

<p>Now, a <em>move</em> consists of removing a stone&nbsp;that shares a column or row with another stone on the grid.</p>

<p>What is the largest possible number of moves we can make?</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>stones = <span id="example-input-1-2">[[0,0],[0,1],[1,0],[1,2],[2,1],[2,2]]</span>
<strong>Output: </strong>5
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>stones = <span id="example-input-2-2">[[0,0],[0,2],[1,1],[2,0],[2,2]]</span>
<strong>Output: </strong>3
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>stones = <span id="example-input-3-2">[[0,0]]</span>
<strong>Output: </strong>0
</pre>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ol>
	<li><code>1 &lt;= stones.length &lt;= 1000</code></li>
	<li><code>0 &lt;= stones[i][j] &lt; 10000</code></li>
</ol>
</div>
</div>
</div>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Union Find (union-find)

## Companies
- Google - 5 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Depth-First Search

**Intuition**

Let's say two stones are connected by an edge if they share a row or column, and define a connected component in the usual way for graphs: a subset of stones so that there doesn't exist an edge from a stone in the subset to a stone not in the subset.  For convenience, we refer to a *component* as meaning a connected component.

The main insight is that we can always make moves that reduce the number of stones in each component to 1.

Firstly, every stone belongs to exactly one component, and moves in one component do not affect another component.

Now, consider a spanning tree of our component.  We can make moves repeatedly from the leaves of this tree until there is one stone left.

**Algorithm**

To count connected components of the above graph, we will use depth-first search.

For every stone not yet visited, we will visit it and any stone in the same connected component.  Our depth-first search traverses each node in the component.

For each component, the answer changes by `-1 + component.size`.

<iframe src="https://leetcode.com/playground/ZKzgofLv/shared" frameBorder="0" width="100%" height="500" name="ZKzgofLv"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `stones`.

* Space Complexity:  $$O(N^2)$$.
<br />
<br />


---
#### Approach 2: Union-Find

**Intuition**

As in *Approach 1*, we will need to consider components of an underlying graph.  A "Disjoint Set Union" (DSU) data structure is ideal for this.

We will skip the explanation of how a DSU structure is implemented.  Please refer to [https://leetcode.com/problems/redundant-connection/solution/](https://leetcode.com/problems/redundant-connection/solution/) for a tutorial on DSU.

**Algorithm**

Let's connect row `i` to column `j`, which will be represented by `j+10000`.  The answer is the number of components after making all the connections.

Note that for brevity, our `DSU` implementation does not use union-by-rank.  This makes the asymptotic time complexity larger.

<iframe src="https://leetcode.com/playground/osUoEhyA/shared" frameBorder="0" width="100%" height="500" name="osUoEhyA"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the length of `stones`.  (If we used union-by-rank, this can be $$O(N * \alpha(N))$$, where $$\alpha$$ is the Inverse-Ackermann function.)

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Count the Number of Islands, O(N)
- Author: lee215
- Creation Date: Sun Nov 25 2018 12:03:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 04:16:25 GMT+0800 (Singapore Standard Time)

<p>
## **Problem:**
we can remove a stone if and only if,
there is another stone in the same column OR row.
We try to remove as many as stones as possible.
<br>

## **One sentence to solve:**
Connected stones can be reduced to 1 stone,
the maximum stones can be removed = stones number - islands number.
so just count the number of "islands".
<br>

## **1. Connected stones**
Two stones are connected if they are in the same row or same col.
Connected stones will build a connected graph.
It\'s obvious that in one connected graph,
we can\'t remove all stones.

We have to have one stone left.
An intuition is that, in the best strategy, we can remove until 1 stone.

I guess you may reach this step when solving the problem.
But the important question is, how?
<br>

## **2. A failed strategy**
Try to remove the least degree stone
Like a tree, we try to remove leaves first.
Some new leaf generated.
We continue this process until the root node left.


However, there can be no leaf.
When you try to remove the least in-degree stone,
it won\'t work on this "8" like graph:
[[1, 1, 0, 0, 0],
 [1, 1, 0, 0, 0],
 [0, 1, 1, 0, 0],
 [0, 0, 1, 1, 1],
 [0, 0, 0, 1, 1]]

The stone in the center has least degree = 2.
But if you remove this stone first,
the whole connected stones split into 2 parts,
and you will finish with 2 stones left.
<br>

## **3. A good strategy**

In fact, the proof is really straightforward.
You probably apply a `DFS`, from one stone to next connected stone.
You can remove stones in reversed order.
In this way, all stones can be removed but the stone that you start your DFS.

One more step of explanation:
In the view of DFS, a graph is explored in the structure of a tree.
As we discussed previously,
a tree can be removed in topological order,
from leaves to root.
<br>

## **4. Count the number of islands**
We call a connected graph as an island.
One island must have at least one stone left.
The maximum stones can be removed = stones number - islands number

The whole problem is transferred to:
What is the number of islands?

You can show all your skills on a DFS implementation,
and solve this problem as a normal one.
<br>

## **5. Unify index**

Struggle between rows and cols?
You may duplicate your codes when you try to the same thing on rows and cols.
In fact, no logical difference between col index and rows index.

An easy trick is that, add 10000 to col index.
So we use 0 ~ 9999 for row index and 10000 ~ 19999 for col.
<br>

## **6. Search on the index, not the points**
When we search on points,
we alternately change our view on a row and on a col.

We think:
a row index, connect two stones on this row
a col index, connect two stones on this col.

In another view\uFF1A
A stone, connect a row index and col.

Have this idea in mind, the solution can be much simpler.
The number of islands of *points*,
is the same as the number of islands of *indexes*.
<br>

## **7. Union-Find**

I use union find to solve this problem.
As I mentioned, the elements are not the points, but the indexes.
1. for each point, union two indexes.
2. return points number - union number

Copy a template of union-find,
write 2 lines above,
you can solve this problem in several minutes.
<br>

## **Complexity**
union and find functions have worst case `O(N)`, amortize `O(1)`
The whole union-find solution with path compression,
has `O(N)` Time, `O(N)` Space

If you have any doubts on time complexity,
please refer to [wikipedia](https://en.wikipedia.org/wiki/Disjoint-set_data_structure) first.
<br>

**C++:**
```
    int removeStones(vector<vector<int>>& stones) {
        for (int i = 0; i < stones.size(); ++i)
            uni(stones[i][0], ~stones[i][1]);
        return stones.size() - islands;
    }

    unordered_map<int, int> f;
    int islands = 0;

    int find(int x) {
        if (!f.count(x)) f[x] = x, islands++;
        if (x != f[x]) f[x] = find(f[x]);
        return f[x];
    }

    void uni(int x, int y) {
        x = find(x), y = find(y);
        if (x != y) f[x] = y, islands--;
    }
```

**Java:**
```
    Map<Integer, Integer> f = new HashMap<>();
    int islands = 0;

    public int removeStones(int[][] stones) {
        for (int i = 0; i < stones.length; ++i)
            union(stones[i][0], ~stones[i][1]);
        return stones.length - islands;
    }

    public int find(int x) {
        if (f.putIfAbsent(x, x) == null)
            islands++;
        if (x != f.get(x))
            f.put(x, find(f.get(x)));
        return f.get(x);
    }

    public void union(int x, int y) {
        x = find(x);
        y = find(y);
        if (x != y) {
            f.put(x, y);
            islands--;
        }
    }
```

**Python:**
```
    def removeStones(self, points):
        UF = {}
        def find(x):
            if x != UF[x]:
                UF[x] = find(UF[x])
            return UF[x]
        def union(x, y):
            UF.setdefault(x, x)
            UF.setdefault(y, y)
            UF[find(x)] = find(y)

        for i, j in points:
            union(i, ~j)
        return len(points) - len({find(x) for x in UF})
```
<br>

## **Update About Union Find Complexity**
I have 3 main reasons\xA0that always insist `O(N)`, on all my union find solutions.

1. The most important, union find is really a common knowledge for algorithm.
Using both\xA0path\xA0compression,\xA0splitting, or\xA0halving\xA0and\xA0union by\xA0rank\xA0or\xA0size\xA0ensures
that the\xA0amortized\xA0time per operation is only\xA0O(1).
So it\'s fair enough to apply this conclusion.

2. \xA0It\'s really not my job to discuss how union find works or the definition of big O.
I bet everyone can find better resource than my post on this part.
You can see the core of my solution is to transform the problem as a union find problem.
The essence is the thinking process behind.
People can have their own template and solve this problem with 2-3 more lines.
But not all the people get the point.\xA0\xA0

3. I personally\xA0manually\xA0write this version of union find every time.
It is really not worth a long template.
The version with path compression can well handle all cases on leetcode.
What\u2018s the benefit here to add more lines?\xA0

4. In this problem, there is `N` union operation, at most `2 * sqrt(N)` node.
When `N` get bigger, the most operation of union operation is amortize `O(1)`.

5. I knew there were three good resourse of union find:
    * [top down analusis of path compression](http://www.cs.tau.ac.il/~michas/ufind.pdf)
    * [wiki](https://en.wikipedia.org/wiki/Disjoint-set_data_structure#cite_note-Cormen2009-10)
    * [stackexchange](https://cs.stackexchange.com/questions/50294/why-is-the-path-compression-no-rank-for-disjoint-sets-o-log-n-amortized-fo)

    But they most likely give a upper bound time complexity of union find,
    not a supreme.
    If anyone has a clear example of union find operation sequence,
    to make it larger than O(N), I am so glad to know it.



</p>


### Java recursive DFS - Short and easy to understand
- Author: ztcztc1994
- Creation Date: Wed Dec 26 2018 10:27:00 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Dec 26 2018 10:27:00 GMT+0800 (Singapore Standard Time)

<p>
Time : O(N^2), N = # of stones
Space: O(N)
```
class Solution {
    // Ans = # of stones \u2013 # of islands
    public int removeStones(int[][] stones) {
        Set<int[]> visited = new HashSet();
        int numOfIslands = 0;
        for (int[] s1:stones){
            if (!visited.contains(s1)){
               dfs(s1, visited, stones); 
               numOfIslands++;
            }
        }
        return stones.length - numOfIslands;
    }
    
    private void dfs(int[] s1, Set<int[]> visited, int[][] stones){
        visited.add(s1);
        for (int[] s2: stones){
            if (!visited.contains(s2)){
				// stone with same row or column. group them into island
                if (s1[0] == s2[0] || s1[1] == s2[1])
                    dfs(s2, visited, stones);
            }
        }
    }
}
```
</p>


### Someone please help, I just don't get this
- Author: Dacredible
- Creation Date: Sat Dec 01 2018 07:13:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Dec 01 2018 07:13:51 GMT+0800 (Singapore Standard Time)

<p>
What is the inputs stands for? Is it stands for the cordinate\'s of the stone? Also What does remove and moves means here? And what\'s the goal?
</p>


