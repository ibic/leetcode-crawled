---
title: "Subdomain Visit Count"
weight: 750
#id: "subdomain-visit-count"
---
## Description
<div class="description">
<p>A website domain like &quot;discuss.leetcode.com&quot; consists of various subdomains. At the top level, we have &quot;com&quot;, at the next level, we have &quot;leetcode.com&quot;, and at the lowest level, &quot;discuss.leetcode.com&quot;. When we visit a domain like &quot;discuss.leetcode.com&quot;, we will also visit the parent domains &quot;leetcode.com&quot; and &quot;com&quot; implicitly.</p>

<p>Now, call a &quot;count-paired domain&quot; to be a count (representing the number of visits this domain received), followed by a space, followed by the address. An example of a count-paired domain might be &quot;9001 discuss.leetcode.com&quot;.</p>

<p>We are given a list <code>cpdomains</code> of count-paired domains. We would like a list of count-paired domains, (in the same format as the input, and in any order), that explicitly counts the number of visits to each subdomain.</p>

<pre>
<strong>Example 1:</strong>
<strong>Input:</strong> 
[&quot;9001 discuss.leetcode.com&quot;]
<strong>Output:</strong> 
[&quot;9001 discuss.leetcode.com&quot;, &quot;9001 leetcode.com&quot;, &quot;9001 com&quot;]
<strong>Explanation:</strong> 
We only have one website domain: &quot;discuss.leetcode.com&quot;. As discussed above, the subdomain &quot;leetcode.com&quot; and &quot;com&quot; will also be visited. So they will all be visited 9001 times.

</pre>

<pre>
<strong>Example 2:</strong>
<strong>Input:</strong> 
[&quot;900 google.mail.com&quot;, &quot;50 yahoo.com&quot;, &quot;1 intel.mail.com&quot;, &quot;5 wiki.org&quot;]
<strong>Output:</strong> 
[&quot;901 mail.com&quot;,&quot;50 yahoo.com&quot;,&quot;900 google.mail.com&quot;,&quot;5 wiki.org&quot;,&quot;5 org&quot;,&quot;1 intel.mail.com&quot;,&quot;951 com&quot;]
<strong>Explanation:</strong> 
We will visit &quot;google.mail.com&quot; 900 times, &quot;yahoo.com&quot; 50 times, &quot;intel.mail.com&quot; once and &quot;wiki.org&quot; 5 times. For the subdomains, we will visit &quot;mail.com&quot; 900 + 1 = 901 times, &quot;com&quot; 900 + 50 + 1 = 951 times, and &quot;org&quot; 5 times.

</pre>

<p><strong>Notes: </strong></p>

<ul>
	<li>The length of <code>cpdomains</code> will not exceed&nbsp;<code>100</code>.&nbsp;</li>
	<li>The length of each domain name will not exceed <code>100</code>.</li>
	<li>Each address will have either 1 or 2 &quot;.&quot; characters.</li>
	<li>The input count&nbsp;in any count-paired domain will not exceed <code>10000</code>.</li>
	<li>The answer output can be returned in any order.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Roblox - 12 (taggedByAdmin: true)
- Karat - 9 (taggedByAdmin: false)
- Intuit - 3 (taggedByAdmin: false)
- Indeed - 2 (taggedByAdmin: false)
- Pinterest - 8 (taggedByAdmin: false)
- Wayfair - 7 (taggedByAdmin: false)
- Citrix - 10 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Hash Map [Accepted]

**Intuition and Algorithm**

The algorithm is straightforward: we just do what the problem statement tells us to do.

For an address like `a.b.c`, we will count `a.b.c`, `b.c`, and `c`.  For an address like `x.y`, we will count `x.y` and `y`.

To count these strings, we will use a hash map.  To split the strings into the required pieces, we will use library `split` functions.

<iframe src="https://leetcode.com/playground/pnxZrtmK/shared" frameBorder="0" width="100%" height="395" name="pnxZrtmK"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `cpdomains`, and assuming the length of `cpdomains[i]` is fixed.

* Space Complexity: $$O(N)$$, the space used in our count.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Easy Understood Solution
- Author: lee215
- Creation Date: Sun Apr 01 2018 13:17:22 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 12:58:43 GMT+0800 (Singapore Standard Time)

<p>
**C++**
```
    vector<string> subdomainVisits(vector<string>& cpdomains) {
        unordered_map<string, int> c;
        for (auto cd : cpdomains) {
            int i = cd.find(" ");
            int n = stoi(cd.substr (0, i));
            string s = cd.substr (i + 1);
            for (int i = 0; i < s.size(); ++i)
                if (s[i] == \'.\')
                    c[s.substr(i + 1)] += n;
            c[s] += n;
        }
        vector<string> res;
        for (auto k : c)
            res.push_back (to_string(k.second) + " " + k.first);
        return res;
    }
```
**Java:**
```
    public List<String> subdomainVisits(String[] cpdomains) {
        Map<String, Integer> map = new HashMap();
        for (String cd : cpdomains) {
            int i = cd.indexOf(\' \');
            int n = Integer.valueOf(cd.substring(0, i));
            String s = cd.substring(i + 1);
            for (i = 0; i < s.length(); ++i) {
                if (s.charAt(i) == \'.\') {
                    String d = s.substring(i + 1);
                    map.put(d, map.getOrDefault(d, 0) + n);
                }
            }
            map.put(s, map.getOrDefault(s, 0) + n);
        }

        List<String> res = new ArrayList();
        for (String d : map.keySet()) res.add(map.get(d) + " " + d);
        return res;
    }
```
**python:**
```
    def subdomainVisits(self, cpdomains):
        c = collections.Counter()
        for cd in cpdomains:
            n, d = cd.split()
            c[d] += int(n)
            for i in range(len(d)):
                if d[i] == \'.\': c[d[i + 1:]] += int(n)
        return ["%d %s" % (c[k], k) for k in c]
</p>


### Java: Do not use String.split() to handle the string in this case and the code will run faster
- Author: dreamjjy
- Creation Date: Fri May 11 2018 17:48:46 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 04 2018 11:59:44 GMT+0800 (Singapore Standard Time)

<p>
for one input like "9001 leetcode.com", we can get visit count and domain like this:
```
String[] arr = s.split(" ");
int visit = Integer.parseInt(arr[0]);
String domain = arr[1];
```
however, the code below may run faster
```
int index = s.indexOf(\' \');
int visit = Integer.parseInt(s.substring(0, index));
String domain = s.substring(index + 1);
```

the second one is 3 times faster than the first one!
</p>


### Python short & understandable solution [68 ms]
- Author: cenkay
- Creation Date: Sun Apr 01 2018 16:40:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 19 2019 00:29:34 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def subdomainVisits(self, cpdomains):
        counter = collections.Counter()
        for cpdomain in cpdomains:
            count, *domains = cpdomain.replace(" ",".").split(".")
            for i in range(len(domains)):
                counter[".".join(domains[i:])] += int(count)
        return [" ".join((str(v), k)) for k, v in counter.items()]
```
</p>


