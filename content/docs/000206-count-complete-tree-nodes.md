---
title: "Count Complete Tree Nodes"
weight: 206
#id: "count-complete-tree-nodes"
---
## Description
<div class="description">
<p>Given a <b>complete</b> binary tree, count the number of nodes.</p>

<p><b>Note: </b></p>

<p><b><u>Definition of a complete binary tree from <a href="http://en.wikipedia.org/wiki/Binary_tree#Types_of_binary_trees" target="_blank">Wikipedia</a>:</u></b><br />
In a complete binary tree every level, except possibly the last, is completely filled, and all nodes in the last level are as far left as possible. It can have between 1 and 2<sup>h</sup> nodes inclusive at the last level h.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> 
    1
   / \
  2   3
 / \  /
4  5 6

<strong>Output:</strong> 6</pre>

</div>

## Tags
- Binary Search (binary-search)
- Tree (tree)

## Companies
- Google - 3 (taggedByAdmin: false)
- Amazon - 5 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Linear Time

**Intuition**

This problem is quite popular at Google during the last year.
The naive solution here is a linear time one-liner which counts nodes
recursively one by one.

**Implementation**

<iframe src="https://leetcode.com/playground/YGNy7Ge5/shared" frameBorder="0" width="100%" height="140" name="YGNy7Ge5"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$.
* Space complexity : $$\mathcal{O}(d) = \mathcal{O}(\log N)$$ to keep
the recursion stack, where d is a tree depth.
<br /> 
<br />


---
#### Approach 2: Binary search

**Intuition**

Approach 1 doesn't profit from the fact that the tree is a complete one.

> In a complete binary tree every level, 
except possibly the last, is completely filled, 
and all nodes in the last level are as far left as possible.

That means that complete tree has $$2^k$$ nodes in the kth level 
if the kth level is not the last one. 
The last level may be not filled completely,
and hence in the last level the number of nodes 
could vary from 1 to $$2^d$$, where d is a tree
depth.

![fig](../Figures/222/tree.png)

Now one could compute the number of nodes in all levels
but the last one: $$\sum_{k = 0}^{k = d - 1}{2^k} = 2^d - 1$$. 
That reduces the problem to the simple check of how many nodes 
the tree has in the last level.

![fic](../Figures/222/level.png)

Now there are two questions:

1. How many nodes in the last level have to be checked?

2. What is the best time performance for such a check?

Let's start from the first question. It's a complete tree, and hence
all nodes in the last level are _as far left as possible_.
That means that instead of checking the existence of all 
$$2^d$$ possible leafs, one could use binary search and check 
$$\log(2^d) = d$$ leafs only.

![pic](../Figures/222/exist.png) 

Let's move to the second question, and enumerate potential nodes
in the last level from 0 to $$2^d - 1$$. 
How to check if the node number idx exists?
Let's use binary search again to reconstruct the sequence of moves 
from root to idx node. For example, idx = 4. idx is in the 
second half of nodes `0,1,2,3,4,5,6,7` and hence the first move is to the
right. Then idx is in the first half of nodes `4,5,6,7` and hence 
the second move is to the left. The idx is in the first half of nodes
`4,5` and hence the next move is to the left. The time complexity
for one check is $$\mathcal{O}(d)$$.

![pif](../Figures/222/check.png)

1 and 2 together result in $$\mathcal{O}(d)$$ checks, each check at
a price of $$\mathcal{O}(d)$$. That means that the overall time complexity
would be $$\mathcal{O}(d^2)$$.
 
**Algorithm**

- Return 0 if the tree is empty.

- Compute the tree depth `d`.

- Return 1 if `d == 0`.

- The number of nodes in all levels but the last one is $$2^d - 1$$.
The number of nodes in the last level could vary from 1 to $$2^d$$.
Enumerate potential nodes from 0 to $$2^d - 1$$ 
and perform the binary search by the node index to check how many 
nodes are in the last level. Use the function `exists(idx, d, root)` to
check if the node with index idx exists.

- Use binary search to implement `exists(idx, d, root)` as well.

- Return $$2^d - 1$$ + the number of nodes in the last level.

**Implementation**

<iframe src="https://leetcode.com/playground/c7C4n4GJ/shared" frameBorder="0" width="100%" height="500" name="c7C4n4GJ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(d^2) = \mathcal{O}(\log^2 N)$$,
where $$d$$ is a tree depth.
* Space complexity : $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise Java solutions O(log(n)^2)
- Author: StefanPochmann
- Creation Date: Sat Jun 06 2015 09:18:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 08:54:09 GMT+0800 (Singapore Standard Time)

<p>
**Main Solution** - 572 ms

    class Solution {
        int height(TreeNode root) {
            return root == null ? -1 : 1 + height(root.left);
        }
        public int countNodes(TreeNode root) {
            int h = height(root);
            return h < 0 ? 0 :
                   height(root.right) == h-1 ? (1 << h) + countNodes(root.right)
                                             : (1 << h-1) + countNodes(root.left);
        }
    }

---

**Explanation**

The height of a tree can be found by just going left. Let a single node tree have height 0. Find the height `h` of the whole tree. If the whole tree is empty, i.e., has height -1, there are 0 nodes.

Otherwise check whether the height of the right subtree is just one less than that of the whole tree, meaning left and right subtree have the same height.

- If yes, then the last node on the last tree row is in the right subtree and the left subtree is a full tree of height h-1. So we take the 2^h-1 nodes of the left subtree plus the 1 root node plus recursively the number of nodes in the right subtree.
- If no, then the last node on the last tree row is in the left subtree and the right subtree is a full tree of height h-2. So we take the 2^(h-1)-1 nodes of the right subtree plus the 1 root node plus recursively the number of nodes in the left subtree.

Since I halve the tree in every recursive step, I have O(log(n)) steps. Finding a height costs O(log(n)). So overall O(log(n)^2).

---

**Iterative Version** - 508 ms

Here's an iterative version as well, with the benefit that I don't recompute `h` in every step.

    class Solution {
        int height(TreeNode root) {
            return root == null ? -1 : 1 + height(root.left);
        }
        public int countNodes(TreeNode root) {
            int nodes = 0, h = height(root);
            while (root != null) {
                if (height(root.right) == h - 1) {
                    nodes += 1 << h;
                    root = root.right;
                } else {
                    nodes += 1 << h-1;
                    root = root.left;
                }
                h--;
            }
            return nodes;
        }
    }

---

**A Different Solution** - 544 ms

Here's one based on [victorlee's C++ solution](https://leetcode.com/discuss/38899/easy-short-c-recursive-solution).

    class Solution {
        public int countNodes(TreeNode root) {
            if (root == null)
                return 0;
            TreeNode left = root, right = root;
            int height = 0;
            while (right != null) {
                left = left.left;
                right = right.right;
                height++;
            }
            if (left == null)
                return (1 << height) - 1;
            return 1 + countNodes(root.left) + countNodes(root.right);
        }
    }

Note that that's basically this:

    public int countNodes(TreeNode root) {
        if (root == null)
            return 0;
        return 1 + countNodes(root.left) + countNodes(root.right)

That would be O(n). But... the actual solution has a gigantic optimization. It first walks all the way left and right to determine the height and whether it's a full tree, meaning the last row is full. If so, then the answer is just 2^height-1. And since always at least one of the two recursive calls is such a full tree, at least one of the two calls immediately stops. Again we have runtime O(log(n)^2).
</p>


### Easy short c++ recursive solution
- Author: victorlee
- Creation Date: Sat Jun 06 2015 03:19:54 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 09:21:08 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    
    public:
    
        int countNodes(TreeNode* root) {
    
            if(!root) return 0;
    
            int hl=0, hr=0;
    
            TreeNode *l=root, *r=root;
    
            while(l) {hl++;l=l->left;}
    
            while(r) {hr++;r=r->right;}
    
            if(hl==hr) return pow(2,hl)-1;
    
            return 1+countNodes(root->left)+countNodes(root->right);
    
        }
    
    };
</p>


### Accepted Easy Understand Java Solution
- Author: mo10
- Creation Date: Fri Aug 14 2015 12:17:58 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 08:06:48 GMT+0800 (Singapore Standard Time)

<p>

public class Solution {

    public int countNodes(TreeNode root) {

        int leftDepth = leftDepth(root);
		int rightDepth = rightDepth(root);

		if (leftDepth == rightDepth)
			return (1 << leftDepth) - 1;
		else
			return 1+countNodes(root.left) + countNodes(root.right);

	}

	private int rightDepth(TreeNode root) {
		// TODO Auto-generated method stub
		int dep = 0;
		while (root != null) {
			root = root.right;
			dep++;
		}
		return dep;
	}

	private int leftDepth(TreeNode root) {
		// TODO Auto-generated method stub
		int dep = 0;
		while (root != null) {
			root = root.left;
			dep++;
		}
		return dep;
    }
}
</p>


