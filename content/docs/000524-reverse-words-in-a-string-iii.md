---
title: "Reverse Words in a String III"
weight: 524
#id: "reverse-words-in-a-string-iii"
---
## Description
<div class="description">
<p>Given a string, you need to reverse the order of characters in each word within a sentence while still preserving whitespace and initial word order.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> "Let's take LeetCode contest"
<b>Output:</b> "s'teL ekat edoCteeL tsetnoc"
</pre>
</p>

<p><b>Note:</b>
In the string, each word is separated by single space and there will not be any extra space in the string.
</p>
</div>

## Tags
- String (string)

## Companies
- Microsoft - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Zappos - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Simple Solution[Accepted]

The first method is really simple. We simply split up the given string based on whitespaces and put the individual words in an array of strings. Then, we reverse each individual string and concatenate the result. We return the result after removing the additional whitespaces at the end.


<iframe src="https://leetcode.com/playground/DjRyo9vA/shared" frameBorder="0" name="DjRyo9vA" width="100%" height="207"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. where $$n$$ is the length of the string.

* Space complexity : $$O(n)$$. $$res$$ of size $$n$$ is used.

---
#### Approach #2 Without using pre-defined split and reverse function [Accepted]

**Algorithm**

We can create our own split and reverse function. Split function splits the string based on the delimiter " "(space) and returns the array of words. Reverse function returns the string after reversing the characters.



<iframe src="https://leetcode.com/playground/oxasWgHd/shared" frameBorder="0" name="oxasWgHd" width="100%" height="515"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n)$$. where $$n$$ is the length of the string.
* Space complexity : $$O(n)$$. $$res$$ of size $$n$$ is used.

---
#### Approach #3 Using StringBuilder and reverse method [Accepted]

**Algorithm**

Instead of using split method, we can use temporary string $$word$$ to store the word. We simply append the characters to the $$word$$ until `' '` character is not found. On getting `' '` we append the reverse of the $$word$$ to the resultant string $$result$$. Also after completion of loop , we still have to append the $$reverse$$ of the $$word$$(last word) to the $$result$$ string. 

Below code is inspired by [@ApolloX](http://leetcode.com/apolloX).


<iframe src="https://leetcode.com/playground/Xt8eMTKv/shared" frameBorder="0" name="Xt8eMTKv" width="100%" height="343"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n)$$. Single loop upto $$n$$ is there, where $$n$$ is the length of the string.
* Space complexity : $$O(n)$$. $$result$$ and $$word$$ size will grow upto $$n$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 1 line Ruby / Python
- Author: StefanPochmann
- Creation Date: Mon Apr 10 2017 04:59:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 18:56:57 GMT+0800 (Singapore Standard Time)

<p>
**Ruby:**

Once again Ruby is really nice, it's super short and you can just write the steps from left to right. Split the string into words, reverse each word, then join them back together.
```
def reverse_words(s)
  s.split.map(&:reverse).join(" ")
end
```

**Python:**

Here I first reverse the order of the words and then reverse the entire string.

    def reverseWords(self, s):
        return ' '.join(s.split()[::-1])[::-1]

That's a bit shorter than the more obvious one:

    def reverseWords(self, s):
        return ' '.join(x[::-1] for x in s.split())

**Ruby again:**

That double reversal in Ruby:
```
def reverse_words(s)
  s.split.reverse.join(" ").reverse
end
```
**Python again:**

The double reversal is not just shorter but also faster. Trying both versions as well as the optimized obvious solution (using a list comprehension instead of a generator expression), five attempts each:
```
>>> from timeit import timeit
>>> setup = 's = "Let\'s take LeetCode contest"'
>>> statements = ("' '.join(s.split()[::-1])[::-1]",
	          "' '.join(x[::-1] for x in s.split())",
	          "' '.join([x[::-1] for x in s.split()])")
>>> for stmt in statements:
        print ' '.join('%.2f' % timeit(stmt, setup) for _ in range(5)), 'seconds for:', stmt

0.79 0.78 0.80 0.82 0.79 seconds for: ' '.join(s.split()[::-1])[::-1]
2.10 2.14 2.08 2.06 2.13 seconds for: ' '.join(x[::-1] for x in s.split())
1.27 1.26 1.28 1.28 1.26 seconds for: ' '.join([x[::-1] for x in s.split()])
```
With many more words, the double reversal's advantage gets even bigger:
```
>>> setup = 's = "Let\'s take LeetCode contest" * 1000'
>>> for stmt in statements:
        print ' '.join('%.2f' % timeit(stmt, setup, number=1000) for _ in range(5)), 'seconds for:', stmt

0.16 0.14 0.13 0.14 0.14 seconds for: ' '.join(s.split()[::-1])[::-1]
0.69 0.71 0.69 0.70 0.70 seconds for: ' '.join(x[::-1] for x in s.split())
0.63 0.68 0.63 0.64 0.64 seconds for: ' '.join([x[::-1] for x in s.split()])
```
</p>


### [C++] [Java] Clean Code
- Author: alexander
- Creation Date: Sun Apr 09 2017 12:03:32 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 18:56:50 GMT+0800 (Singapore Standard Time)

<p>
**C++**
```
class Solution {
public:
    string reverseWords(string s) {
        for (int i = 0; i < s.length(); i++) {
            if (s[i] != ' ') {   // when i is a non-space
                int j = i;
                for (; j < s.length() && s[j] != ' '; j++) { } // move j to the next space
                reverse(s.begin() + i, s.begin() + j);
                i = j - 1;
            }
        }
        
        return s;
    }
};
```
**Java**
```
public class Solution {
    public String reverseWords(String s) {
        char[] ca = s.toCharArray();
        for (int i = 0; i < ca.length; i++) {
            if (ca[i] != ' ') {   // when i is a non-space
                int j = i;
                while (j + 1 < ca.length && ca[j + 1] != ' ') { j++; } // move j to the end of the word
                reverse(ca, i, j);
                i = j;
            }
        }
        return new String(ca);
    }

    private void reverse(char[] ca, int i, int j) {
        for (; i < j; i++, j--) {
            char tmp = ca[i];
            ca[i] = ca[j];
            ca[j] = tmp;
        }
    }
}
```
</p>


### short java code without explanation
- Author: MSG_Yang
- Creation Date: Mon Jul 31 2017 23:17:33 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 12:11:03 GMT+0800 (Singapore Standard Time)

<p>
```
    public String reverseWords(String s) {
        String[] str = s.split(" ");
        for (int i = 0; i < str.length; i++) str[i] = new StringBuilder(str[i]).reverse().toString();
        StringBuilder result = new StringBuilder();
        for (String st : str) result.append(st + " ");
        return result.toString().trim();
    } 
```
</p>


