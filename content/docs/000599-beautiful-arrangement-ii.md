---
title: "Beautiful Arrangement II"
weight: 599
#id: "beautiful-arrangement-ii"
---
## Description
<div class="description">
<p>
Given two integers <code>n</code> and <code>k</code>, you need to construct a list which contains <code>n</code> different positive integers ranging from <code>1</code> to <code>n</code> and obeys the following requirement: <br/>

Suppose this list is [a<sub>1</sub>, a<sub>2</sub>, a<sub>3</sub>, ... , a<sub>n</sub>], then the list [|a<sub>1</sub> - a<sub>2</sub>|, |a<sub>2</sub> - a<sub>3</sub>|, |a<sub>3</sub> - a<sub>4</sub>|, ... , |a<sub>n-1</sub> - a<sub>n</sub>|] has exactly <code>k</code> distinct integers.
</p>

<p>
If there are multiple answers, print any of them.
</p>

<p><b>Example 1:</b><br/>
<pre>
<b>Input:</b> n = 3, k = 1
<b>Output:</b> [1, 2, 3]
<b>Explanation:</b> The [1, 2, 3] has three different positive integers ranging from 1 to 3, and the [1, 1] has exactly 1 distinct integer: 1.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> n = 3, k = 2
<b>Output:</b> [1, 3, 2]
<b>Explanation:</b> The [1, 3, 2] has three different positive integers ranging from 1 to 3, and the [2, 1] has exactly 2 distinct integers: 1 and 2.
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The <code>n</code> and <code>k</code> are in the range 1 <= k < n <= 10<sup>4</sup>.</li>
</ol>
</p>
</div>

## Tags
- Array (array)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1: Brute Force [Time Limit Exceeded]

**Intuition**

For each permutation of $$\text{[1, 2, ..., n]}$$, let's look at the set of differences of the adjacent elements.

**Algorithm**

For each permutation, we find the number of unique differences of adjacent elements.  If it is the desired number, we'll return that permutation.

To enumerate each permutation without using library functions, we use a recursive algorithm, where `permute` is responsible for permuting the indexes of $$\text{nums}$$ in the interval $$\text{[start, nums.length)}$$.

<iframe src="https://leetcode.com/playground/JvKeuMXb/shared" frameBorder="0" name="JvKeuMXb" width="100%" height="515"></iframe>
**Complexity Analysis**

* Time Complexity: $$O(n!)$$ to generate every permutation in the outer loop, then $$O(n)$$ work to check differences.  In total taking $$O(n* n!)$$ time.

* Space Complexity:  $$O(n)$$.  We use $$\text{seen}$$ to store whether we've seen the differences, and each generated permutation has a length equal to $$\text{n}$$.

---
#### Approach #2: Construction [Accepted]

**Intuition**

When $$\text{k = n-1}$$, a valid construction is $$\text{[1, n, 2, n-1, 3, n-2, ....]}$$. One way to see this is, we need to have a difference of $$\text{n-1}$$, which means we need $$\text{1}$$ and $$\text{n}$$ adjacent; then, we need a difference of $$\text{n-2}$$, etc.

Also, when $$\text{k = 1}$$, a valid construction is $$\text{[1, 2, 3, ..., n]}$$. So we have a construction when $$\text{n-k}$$ is tiny, and when it is large.  This leads to the idea that we can stitch together these two constructions:  we can put $$\text{[1, 2, ..., n-k-1]}$$ first so that $$\text{n}$$ is effectively $$\text{k+1}$$, and then finish the construction with the first $$\text{"k = n-1"}$$ method.

For example, when $$\text{n = 6}$$ and $$\text{k = 3}$$, we will construct the array as $$\text{[1, 2, 3, 6, 4, 5]}$$.  This consists of two parts: a construction of $$\text{[1, 2]}$$ and a construction of $$\text{[1, 4, 2, 3]}$$ where every element had $$\text{2}$$ added to it (i.e. $$\text{[3, 6, 4, 5]}$$).

**Algorithm**

As before, write $$\text{[1, 2, ..., n-k-1]}$$ first.  The remaining $$\text{k+1}$$ elements to be written are $$\text{[n-k, n-k+1, ..., n]}$$, and we'll write them in alternating head and tail order.

When we are writing the $$i^{th}$$ element from the remaining $$\text{k+1}$$, every even $$i$$ is going to be chosen from the head, and will have value $$\text{n-k + i//2}$$.  Every odd $$i$$ is going to be chosen from the tail, and will have value $$\text{n - i//2}$$.

<iframe src="https://leetcode.com/playground/knXdznYV/shared" frameBorder="0" name="knXdznYV" width="100%" height="275"></iframe>
**Complexity Analysis**

* Time Complexity: $$O(n)$$.  We are making a list of size $$\text{n}$$.

* Space Complexity:  $$O(n)$$.  Our answer has a length equal to $$\text{n}$$.

## Accepted Submission (java)
```java
class Solution {
    public int[] constructArray(int n, int k) {
        int[] r = new int[n];
        int a = 1, b = n;
        int i = 0;
        boolean endBig = false;
        for (;;) {
            r[i] = a;
            i++;
            a++;
            if (i >= k) {
                break;
            }
            r[i] = b;
            i++;
            b--;
            if (i >= k) {
                endBig = true;
                break;
            }
        }

        if (endBig) {
            for (int j = i; j < n; j++) {
                r[j] = b;
                b--;                
            }
        } else {
            for (int j = i; j < n; j++) {
                r[j] = a;
                a++;                
            }
        }
        return r;
    }
}
```

## Top Discussions
### [C++] [Java] Clean Code 4-liner
- Author: alexander
- Creation Date: Sun Aug 27 2017 11:02:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 08:34:26 GMT+0800 (Singapore Standard Time)

<p>
if you have `n` number, the maximum `k` can be `n - 1`;
if `n` is 9, max `k` is 8.
This can be done by picking numbers interleavingly from head and tail, 
```
// start from i = 1, j = n;
// i++, j--, i++, j--, i++, j--

i: 1   2   3   4   5
j:   9   8   7   6
out: 1 9 2 8 3 7 4 6 5
dif:  8 7 6 5 4 3 2 1
```
Above is a case where `k` is exactly `n - 1`
When k is less than that, simply lay out the rest `(i, j)` in incremental
 order(all diff is 1). Say if k is 5:
```
     i++ j-- i++ j--  i++ i++ i++ ...
out: 1   9   2   8    3   4   5   6   7
dif:   8   7   6   5    1   1   1   1 
```
**C++**
```
class Solution {
public:
    vector<int> constructArray(int n, int k) {
        vector<int> res;
        for (int i = 1, j = n; i <= j; ) {
            if (k > 1) {
                res.push_back(k-- % 2 ? i++ : j--);
            }
            else {
                res.push_back(i++);
            }
        }

        return res;
    }
};
```

**C++ Compact**
```
class Solution {
public:
    vector<int> constructArray(int n, int k) {
        vector<int> res;
        for (int i = 1, j = n; i <= j; )
            res.push_back(k > 1 ? (k-- % 2 ? i++ : j--) : i++;
        return res;
    }
};
```
**Java**
```
class Solution {
    public int[] constructArray(int n, int k) {
        int[] res = new int[n];
        for (int i = 0, l = 1, r = n; l <= r; i++)
            res[i] = k > 1 ? (k-- % 2 != 0 ? l++ : r--) : l++;
        return res;
    }
}
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Aug 27 2017 15:21:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 26 2018 00:02:11 GMT+0800 (Singapore Standard Time)

<p>
When `k = n-1`, a valid construction is `[1, n, 2, n-1, 3, n-2, ....]`.  One way to see this is, we need to have a difference of `n-1`, which means we need `1` and `n` adjacent; then, we need a difference of `n-2`, etc.

This leads to the following idea:  we will put `[1, 2, ...., n-k-1]` first, and then we have `N = k+1` adjacent numbers left, of which we want `k` different differences.  This is just the answer above translated by `n-k-1`: we'll put `[n-k, n, n-k+1, n-1, ....]` after.

```
def constructArray(self, n, k):
    ans = range(1, n - k)
    for d in xrange(k+1):
        if d % 2 == 0:
            ans.append(n-k + d/2)
        else:
            ans.append(n - d/2)

    return ans
```
</p>


### Java, easy to understand with explanation
- Author: ydeng365
- Creation Date: Sun Aug 27 2017 12:29:31 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 10:06:01 GMT+0800 (Singapore Standard Time)

<p>
1,n,2,n-1,3,n-2,4... ==> Diff:  n-1, n-2, n-3, n-4, n-5...
By following this pattern, k numbers will have k-1 distinct difference values; 
and all the rest numbers should have |ai - a_i-1| = 1; 
In total, we will have k-1+1 = k distinct values.

```
class Solution {
    public int[] constructArray(int n, int k) {
        if(k>=n) return null;
        int[] arr = new int[n];
        int i = 0, small = 1, large = n;        
        while(i<k){ 
            arr[i++] = small++;
            if(i<k) arr[i++] = large--;
        }        
        if(k%2 == 0){ // k==2 ==> 1, 6, 5,4,3,2
            while(i<arr.length) arr[i++] = large--;
        } else { // k==3 ==> 1,6,2,3,4,5
            while(i<arr.length) arr[i++] = small++;
        }
        return arr;
    }
}
```
</p>


