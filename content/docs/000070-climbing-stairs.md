---
title: "Climbing Stairs"
weight: 70
#id: "climbing-stairs"
---
## Description
<div class="description">
<p>You are climbing a stair case. It takes <em>n</em> steps to reach to the top.</p>

<p>Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 2
<strong>Output:</strong> 2
<strong>Explanation:</strong> There are two ways to climb to the top.
1. 1 step + 1 step
2. 2 steps
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> 3
<strong>Output:</strong> 3
<strong>Explanation:</strong> There are three ways to climb to the top.
1. 1 step + 1 step + 1 step
2. 1 step + 2 steps
3. 2 steps + 1 step
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 45</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 5 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: true)
- Facebook - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: true)
- Microsoft - 6 (taggedByAdmin: false)
- Uber - 5 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Huawei - 2 (taggedByAdmin: false)
- TripAdvisor - 2 (taggedByAdmin: false)
- Goldman Sachs - 5 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)
- Baidu - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Zulily - 2 (taggedByAdmin: false)

## Official Solution
[TOC]
## Summary

You are climbing a stair case. It takes n steps to reach to the top.

Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

## Solution

---
#### Approach 1: Brute Force

**Algorithm**

In this brute force approach we take all possible step combinations i.e. 1 and 2, at every step. At every step we are calling the function $$climbStairs$$ for step $$1$$ and $$2$$, and return the sum of returned values of both functions.

$$
climbStairs(i,n)=(i + 1, n) + climbStairs(i + 2, n)
$$

where $$i$$ defines the current step and $$n$$ defines the destination step.

<iframe src="https://leetcode.com/playground/vSGtWB6q/shared" frameBorder="0" width="100%" height="293" name="vSGtWB6q"></iframe>

**Complexity Analysis**

* Time complexity : $$O(2^n)$$. Size of recursion tree will be $$2^n$$.

    Recursion tree for n=5 would be like this:

    ![Climbing_Stairs](../Figures/70_Climbing_Stairs_rt.jpg)

* Space complexity : $$O(n)$$. The depth of the recursion tree can go upto $$n$$.
<br />
<br />
---
#### Approach 2: Recursion with Memoization

**Algorithm**

In the previous approach we are redundantly calculating the result for every step. Instead, we can store the result at each step in $$memo$$ array and directly returning the result from the memo array whenever that function is called again.

In this way we are pruning recursion tree with the help of $$memo$$ array and reducing the size of recursion tree upto $$n$$.

<iframe src="https://leetcode.com/playground/5fLezqKW/shared" frameBorder="0" width="100%" height="378" name="5fLezqKW"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Size of recursion tree can go upto $$n$$.

* Space complexity : $$O(n)$$. The depth of recursion tree can go upto $$n$$.
<br />
<br />
---
#### Approach 3: Dynamic Programming

**Algorithm**

As we can see this problem can be broken into subproblems, and it contains the optimal substructure property i.e. its optimal solution can be constructed efficiently from optimal solutions of its subproblems, we can use dynamic programming to solve this problem.


One can reach $$i^{th}$$ step in one of the two ways:

1. Taking a single step from $$(i-1)^{th}$$ step.

2. Taking a step of $$2$$ from $$(i-2)^{th}$$ step.

So, the total number of ways to reach $$i^{th}$$ is equal to sum of ways of reaching $$(i-1)^{th}$$ step and ways of reaching $$(i-2)^{th}$$ step.

Let $$dp[i]$$ denotes the number of ways to reach on $$i^{th}$$ step:

$$
dp[i]=dp[i-1]+dp[i-2]
$$

Example:

<!--![Climbing_Stairs](../Figures/70_Climbing_Stairs.gif)-->
!?!../Documents/70_Climbing_Stairs.json:1000,563!?!


<iframe src="https://leetcode.com/playground/bJT3YiVD/shared" frameBorder="0" width="100%" height="293" name="bJT3YiVD"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Single loop upto $$n$$.

* Space complexity : $$O(n)$$. $$dp$$ array of size $$n$$ is used.
<br />
<br />
---
#### Approach 4: Fibonacci Number

**Algorithm**

In the above approach we have used $$dp$$ array where $$dp[i]=dp[i-1]+dp[i-2]$$. It can be easily analysed that $$dp[i]$$ is nothing but $$i^{th}$$ fibonacci number.

$$
Fib(n)=Fib(n-1)+Fib(n-2)
$$

Now we just have to find $$n^{th}$$ number of the fibonacci series having $$1$$ and $$2$$ their first and second term respectively, i.e. $$Fib(1)=1$$ and $$Fib(2)=2$$.

<iframe src="https://leetcode.com/playground/jXMy3r3P/shared" frameBorder="0" width="100%" height="310" name="jXMy3r3P"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Single loop upto $$n$$ is required to calculate $$n^{th}$$ fibonacci number.

* Space complexity : $$O(1)$$. Constant space is used.
<br />
<br />
---
#### Approach 5: Binets Method

**Algorithm**

This is an interesting solution which uses matrix multiplication to obtain the $$n^{th}$$ Fibonacci Number. The matrix takes the following form:

$$
\left[ {\begin{array}{cc} F_{n+1} & F_n \\  F_n & F_{n-1}     \end{array} } \right] = \left[ {\begin{array}{cc} 1 & 1 \\  1 & 0     \end{array} } \right]
$$

Let's say $$Q=\left[ {\begin{array}{cc} F_{n+1} & F_n \\  F_n & F_{n-1}     \end{array} } \right]$$. As per the method, the $$n^{th}$$ Fibonacci Number is given by $$Q^{n-1}[0,0]$$.

Let's look at the proof of this method.

We can prove this method using Mathematical Induction. We know, this matrix gives the correct result for the $$3^{rd}$$ term(base case). Since $$Q^2 = \left[ {\begin{array}{cc} 2 & 1 \\  1 & 1     \end{array} } \right]$$. This proves that the base case holds.

Assume that this method holds for finding the $$n^{th}$$ Fibonacci Number, i.e. $$F_n=Q^{n-1}[0,0]$$, where
$$
Q^{n-1}=\left[ {\begin{array}{cc} F_{n} & F_{n-1} \\  F_{n-1} & F_{n-2}     \end{array} } \right]
$$

Now, we need to prove that with the above two conditions holding true, the method is valid for finding the $$(n+1)^{th}$$ Fibonacci Number, i.e. $$F_{n+1}=Q^{n}[0,0]$$.

Proof: $$Q^{n} = \left[ {\begin{array}{cc} F_{n} & F_{n-1} \\  F_{n-1} & F_{n-2}     \end{array} } \right]\left[ {\begin{array}{cc} 1 & 1 \\  1 & 0     \end{array} } \right]$$.
 $$Q^{n} = \left[ {\begin{array}{cc} F_{n}+F_{n-1} & F_n \\  F_{n-1}+F_{n-2} & F_{n-1}    \end{array} } \right]$$
 $$Q^{n} = \left[ {\begin{array}{cc} F_{n+1} & F_n \\  F_n & F_{n-1}     \end{array} } \right]$$

 Thus, $$F_{n+1}=Q^{n}[0,0]$$. This completes the proof of this method.

 The only variation we need to do for our problem is that we need to modify the initial terms to 2 and 1 instead of 1 and 0 in the Fibonacci series. Or, another way is to use the same initial $$Q$$ matrix and use $$result = Q^{n}[0,0]$$ to get the final result. This happens because the initial terms we have to use are the 2nd and 3rd terms of the otherwise normal Fibonacci Series.


 <iframe src="https://leetcode.com/playground/DTs7qYKy/shared" frameBorder="0" width="100%" height="500" name="DTs7qYKy"></iframe>

**Complexity Analysis**

* Time complexity : $$O(\log n)$$. Traversing on $$\log n$$ bits.

* Space complexity : $$O(1)$$. Constant space is used.

Proof of Time Complexity:

Let's say there is a  matrix $$M$$ to be raised to  power $$n$$. Suppose, $$n$$ is the power of 2. Thus, $$n = 2^i$$, $$i\in\mathbb{N}$$, where $$\mathbb{N}$$ represents the set of natural numbers(including 0). We can represent  in the form of a tree:

![Climbing Stairs](../Figures/70_Climbing_Stairs.PNG)
{:align="center"}

Meaning that: $$M^n = M^{n/2}.M^{n/2} = .... = \prod_{1}^{n} M^{1}$$

So, to calculate  $$M^{n}$$ matrix, we should calculate $$M^{n/2}$$  matrix and multiply it by itself. To calculate $$M^{n/2}$$ we would have to do the same with $$M^{n/4}$$ and so on.

Obviously, the tree height is $$\log_{2} n$$.

Let’s estimate $$M^{n}$$ calculation time. $$M$$ matrix is of the same size in any power . Therefore, we can perform the multiplication of two matrices in any power in $$O(1)$$. We should perform $$\log_2 n$$ of such multiplications. So, $$M^{n}$$ calculation complexity is $$O(\log_{2} n)$$.

In case, the number $$n$$ is not a power of two, we can break it in terms of powers of 2 using its binary representation:

$$
n= \sum_{p\in P} 2^{p}, \text{where }P\subset\mathbb{N}
$$

Thus, we can obtain the final result using:

$$
M^{n} = \prod_{p\in P} M^{2^{p}}
$$

This is the method we've used in our implementation. Again, the complexity remains $$O(\log_{2} n)$$ as we have limited the number of multiplications to $$O(\log_{2} n)$$.<br /><br />

---
#### Approach 6: Fibonacci Formula

**Algorithm**

We can find $$n^{th}$$ fibonacci number using this formula:

$$
F_n = 1/\sqrt{5}\left[ \left(\frac{1+\sqrt{5}}{2}\right)^{n} - \left(\frac{1-\sqrt{5}}{2}\right)^{n} \right]
$$

For the given problem, the Fibonacci sequence is defined by $$F_0 = 1$$, $$F_1= 1$$,  $$F_1= 2$$, $$F_{n+2}= F_{n+1} + F_n$$. A standard method of trying to solve such recursion formulas is assume $$F_n$$ of the form $$F_n= a^n$$. Then, of course, $$F_{n+1} = a^{n+1}$$ and $$F_{n+2}= a^{n+2}$$ so the equation becomes $$a^{n+2}= a^{n+1}+ a^n$$. If we divide the entire equation by an we arrive at $$a^2= a + 1$$ or the quadratic equation $$a^2 - a- 1= 0$$.

Solving this by the quadratic formula, we get:

$$
a=1/\sqrt{5}\left(\left(\frac{1\pm \sqrt{5}}{2}\right)\right)
$$

The general solution, thus takes the form:

$$
F_n = A\left(\frac{1+\sqrt{5}}{2}\right)^{n} + B\left(\frac{1-\sqrt{5}}{2}\right)^{n}
$$

For $$n=0$$, we get $$A + B = 1$$

For $$n=1$$, we get $$A\left(\frac{1+\sqrt{5}}{2}\right) + B\left(\frac{1-\sqrt{5}}{2}\right) = 1$$

Solving the above equations, we get:

$$
A = \left(\frac{1+\sqrt{5}}{2\sqrt{5}}\right), B = \left(\frac{1-\sqrt{5}}{2\sqrt{5}}\right)
$$

Putting these values of $$A$$ and $$B$$ in the above general solution equation, we get:

$$
F_n = 1/\sqrt{5}\left( \left(\frac{1+\sqrt{5}}{2}\right)^{n+1} - \left(\frac{1-\sqrt{5}}{2}\right)^{n+1} \right)
$$

<iframe src="https://leetcode.com/playground/nGLhVjXP/shared" frameBorder="0" width="100%" height="174" name="nGLhVjXP"></iframe>

**Complexity Analysis**

* Time complexity : $$O(\log n)$$. $$pow$$ method takes $$\log n$$ time.

* Space complexity : $$O(1)$$. Constant space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Basically it's a fibonacci.
- Author: liaison
- Creation Date: Sun Nov 23 2014 12:57:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 11:15:49 GMT+0800 (Singapore Standard Time)

<p>
The problem seems to be a *dynamic programming* one. **Hint**: the tag also suggests that! 
Here are the steps to get the solution incrementally. 

- Base cases:  
if n <= 0, then the number of ways should be zero. 
if n == 1, then there is only way to climb the stair. 
if n == 2, then there are two ways to climb the stairs. One solution is one step by another; the other one is two steps at one time. 

- The key intuition to solve the problem is that given a number of stairs n, if we know the number ways to get to the points `[n-1]` and `[n-2]` respectively, denoted as `n1` and `n2` , then the total ways to get to the point `[n]` is `n1 + n2`. Because from the `[n-1]` point, we can take one single step to reach `[n]`. And from the `[n-2]` point, we could take two steps to get there.

- The solutions calculated by the above approach are ***complete*** and ***non-redundant***. The two solution sets (`n1`  and `n2`) cover all the possible cases on how the final step is taken. And there would be NO overlapping among the final solutions constructed from these two solution sets, because they differ in the final step. 

Now given the above intuition, one can construct an array where each node stores the solution for each number n. Or if we look at it closer, it is clear that this is basically a fibonacci number, with the starting numbers as 1 and 2, instead of 1 and 1. 

The implementation in Java as follows: 

    public int climbStairs(int n) {
        // base cases
        if(n <= 0) return 0;
        if(n == 1) return 1;
        if(n == 2) return 2;
        
        int one_step_before = 2;
        int two_steps_before = 1;
        int all_ways = 0;
        
        for(int i=2; i<n; i++){
        	all_ways = one_step_before + two_steps_before;
        	two_steps_before = one_step_before;
	        one_step_before = all_ways;
        }
        return all_ways;
    }
</p>


### 3-4 short lines in every language
- Author: StefanPochmann
- Creation Date: Thu Jun 25 2015 07:34:21 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 10:17:41 GMT+0800 (Singapore Standard Time)

<p>
Same simple algorithm written in every offered language. Variable `a` tells you the number of ways to reach the current step, and `b` tells you the number of ways to reach the next step. So for the situation one step further up, the old `b` becomes the new `a`, and the new `b` is the old `a+b`, since that new step can be reached by climbing 1 step from what `b` represented or 2 steps from what `a` represented.

Ruby wins, and *"the C languages"* all look the same.

**Ruby** (60 ms)

    def climb_stairs(n)
        a = b = 1
        n.times { a, b = b, a+b }
        a
    end

**C++** (0 ms)

    int climbStairs(int n) {
        int a = 1, b = 1;
        while (n--)
            a = (b += a) - a;
        return a;
    }

**Java** (208 ms)

    public int climbStairs(int n) {
        int a = 1, b = 1;
        while (n-- > 0)
            a = (b += a) - a;
        return a;
    }

**Python** (52 ms)

    def climbStairs(self, n):
        a = b = 1
        for _ in range(n):
            a, b = b, a + b
        return a

**C** (0 ms)

    int climbStairs(int n) {
        int a = 1, b = 1;
        while (n--)
            a = (b += a) - a;
        return a;
    }

**C#** (48 ms)

    public int ClimbStairs(int n) {
        int a = 1, b = 1;
        while (n-- > 0)
            a = (b += a) - a;
        return a;
    }

**Javascript** (116 ms)

    var climbStairs = function(n) {
        a = b = 1
        while (n--)
            a = (b += a) - a
        return a
    };
</p>


### Python different solutions (bottom up, top down).
- Author: OldCodingFarmer
- Creation Date: Fri Jul 24 2015 23:48:43 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 15:01:26 GMT+0800 (Singapore Standard Time)

<p>
    
    # Top down - TLE
    def climbStairs1(self, n):
        if n == 1:
            return 1
        if n == 2:
            return 2
        return self.climbStairs(n-1)+self.climbStairs(n-2)
     
    # Bottom up, O(n) space
    def climbStairs2(self, n):
        if n == 1:
            return 1
        res = [0 for i in xrange(n)]
        res[0], res[1] = 1, 2
        for i in xrange(2, n):
            res[i] = res[i-1] + res[i-2]
        return res[-1]
    
    # Bottom up, constant space
    def climbStairs3(self, n):
        if n == 1:
            return 1
        a, b = 1, 2
        for i in xrange(2, n):
            tmp = b
            b = a+b
            a = tmp
        return b
        
    # Top down + memorization (list)
    def climbStairs4(self, n):
        if n == 1:
            return 1
        dic = [-1 for i in xrange(n)]
        dic[0], dic[1] = 1, 2
        return self.helper(n-1, dic)
        
    def helper(self, n, dic):
        if dic[n] < 0:
            dic[n] = self.helper(n-1, dic)+self.helper(n-2, dic)
        return dic[n]
        
    # Top down + memorization (dictionary)  
    def __init__(self):
        self.dic = {1:1, 2:2}
        
    def climbStairs(self, n):
        if n not in self.dic:
            self.dic[n] = self.climbStairs(n-1) + self.climbStairs(n-2)
        return self.dic[n]
</p>


