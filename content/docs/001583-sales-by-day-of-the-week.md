---
title: "Sales by Day of the Week"
weight: 1583
#id: "sales-by-day-of-the-week"
---
## Description
<div class="description">
<p>Table: <code>Orders</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| order_id      | int     |
| customer_id   | int     |
| order_date    | date    | 
| item_id       | varchar |
| quantity      | int     |
+---------------+---------+
(ordered_id, item_id) is the primary key for this table.
This table contains information of the orders placed.
order_date is the date when item_id was ordered by the customer with id customer_id.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Items</code></p>

<pre>
+---------------------+---------+
| Column Name         | Type    |
+---------------------+---------+
| item_id             | varchar |
| item_name           | varchar |
| item_category       | varchar |
+---------------------+---------+
item_id is the primary key for this table.
item_name is the name of the item.
item_category&nbsp;is the category of the item.
</pre>

<p>&nbsp;</p>

<p>You are the business owner and would like to obtain a sales report for category items and day of the week.</p>

<p>Write an SQL query to&nbsp;report how many units in each category have been ordered on each <strong>day of the week</strong>.</p>

<p>Return the result table&nbsp;<strong>ordered</strong>&nbsp;by category.</p>

<p>The query result format is in the following example:</p>

<p>&nbsp;</p>

<pre>
<code>Orders</code> table:
+------------+--------------+-------------+--------------+-------------+
| order_id   | customer_id  | order_date  | item_id      | quantity    |
+------------+--------------+-------------+--------------+-------------+
| 1          | 1            | 2020-06-01  | 1            | 10          |
| 2          | 1            | 2020-06-08  | 2            | 10          |
| 3          | 2            | 2020-06-02  | 1            | 5           |
| 4          | 3            | 2020-06-03  | 3            | 5           |
| 5          | 4            | 2020-06-04  | 4            | 1           |
| 6          | 4            | 2020-06-05  | 5            | 5           |
| 7          | 5            | 2020-06-05  | 1            | 10          |
| 8          | 5            | 2020-06-14  | 4            | 5           |
| 9          | 5            | 2020-06-21  | 3            | 5           |
+------------+--------------+-------------+--------------+-------------+

<code>Items</code> table:
+------------+----------------+---------------+
| item_id    | item_name      | item_category |
+------------+----------------+---------------+
| 1          | LC Alg. Book   | Book          |
| 2          | LC DB. Book    | Book          |
| 3          | LC SmarthPhone | Phone         |
| 4          | LC Phone 2020  | Phone         |
| 5          | LC SmartGlass  | Glasses       |
| 6          | LC T-Shirt XL  | T-Shirt       |
+------------+----------------+---------------+

Result table:
+------------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+
| Category   | Monday    | Tuesday   | Wednesday | Thursday  | Friday    | Saturday  | Sunday    |
+------------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+
| Book       | 20        | 5         | 0         | 0         | 10        | 0         | 0         |
| Glasses    | 0         | 0         | 0         | 0         | 5         | 0         | 0         |
| Phone      | 0         | 0         | 5         | 1         | 0         | 0         | 10        |
| T-Shirt    | 0         | 0         | 0         | 0         | 0         | 0         | 0         |
+------------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+
On Monday (2020-06-01, 2020-06-08) were sold a total of 20 units (10 + 10) in the category Book (ids: 1, 2).
On Tuesday (2020-06-02) were sold a total of 5 units  in the category Book (ids: 1, 2).
On Wednesday (2020-06-03) were sold a total of 5 units in the category Phone (ids: 3, 4).
On Thursday (2020-06-04) were sold a total of 1 unit in the category Phone (ids: 3, 4).
On Friday (2020-06-05) were sold 10 units in the category Book (ids: 1, 2) and 5 units in Glasses (ids: 5).
On Saturday there are no items sold.
On Sunday (2020-06-14, 2020-06-21) were sold a total of 10 units (5 +5) in the category Phone (ids: 3, 4).
There are no sales of T-Shirt.
</pre>
</div>

## Tags


## Companies
- Amazon - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySql solution
- Author: lxhnt
- Creation Date: Sun Jun 14 2020 10:06:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 14 2020 10:06:49 GMT+0800 (Singapore Standard Time)

<p>
select b.item_category as \'CATEGORY\', sum(case when weekday(a.order_date) = 0 then a.quantity else 0 end) as \'MONDAY\',
                        sum(case when weekday(a.order_date) = 1 then a.quantity else 0 end) as \'TUESDAY\',
                        sum(case when weekday(a.order_date) = 2 then a.quantity else 0 end) as \'WEDNESDAY\',
                        sum(case when weekday(a.order_date) = 3 then a.quantity else 0 end) as \'THURSDAY\',
                        sum(case when weekday(a.order_date) = 4 then a.quantity else 0 end) as \'FRIDAY\',
                        sum(case when weekday(a.order_date) = 5 then a.quantity else 0 end) as \'SATURDAY\',
                        sum(case when weekday(a.order_date) = 6 then a.quantity else 0 end) as \'SUNDAY\'
from orders a right join items b on a.item_id = b.item_id
group by b.item_category
order by b.item_category
</p>


### MySQL Solution with Detailed Explanation
- Author: chinhau
- Creation Date: Mon Jul 20 2020 22:55:00 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 20 2020 22:59:25 GMT+0800 (Singapore Standard Time)

<p>
Please upvote this solution if you find it useful!

Explanation: 
* cte - transform order_date into day, aggregate SUM() based on item category & day
* final - basically a pivot table

	
		WITH cte AS 
		(
			SELECT i.item_category, WEEKDAY(o.order_date) AS "day", SUM(o.quantity) AS "total"
			FROM Orders o LEFT JOIN Items i ON i.item_id = o.item_id
			GROUP BY 1, 2 ORDER BY 1, 2
		),
		final AS 
		(
			SELECT i.item_category AS "Category",
			IFNULL(MAX(CASE WHEN c.day = 0 THEN c.total END), 0) AS "Monday",
			IFNULL(MAX(CASE WHEN c.day = 1 THEN c.total END), 0) AS "Tuesday",
			IFNULL(MAX(CASE WHEN c.day = 2 THEN c.total END), 0) AS "Wednesday",
			IFNULL(MAX(CASE WHEN c.day = 3 THEN c.total END), 0) AS "Thursday",
			IFNULL(MAX(CASE WHEN c.day = 4 THEN c.total END), 0) AS "Friday",
			IFNULL(MAX(CASE WHEN c.day = 5 THEN c.total END), 0) AS "Saturday",
			IFNULL(MAX(CASE WHEN c.day = 6 THEN c.total END), 0) AS "Sunday"
			FROM Items i LEFT JOIN cte c ON i.item_category = c.item_category
			GROUP BY 1 ORDER BY 1
		)

		SELECT * FROM final
</p>


### Super easy & understandable MySQL solution
- Author: FarTaFar
- Creation Date: Wed Jun 17 2020 01:17:05 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 17 2020 01:17:05 GMT+0800 (Singapore Standard Time)

<p>
```
with cte as (select item_category, ifnull(sum(quantity),0) total , dayname(order_date) weekday
from orders
right join items
on orders.item_id=items.item_id
group by 1,3)

select item_category as Category 
,ifnull(sum(case when weekday=\'Monday\' then total end),0) as Monday  
,ifnull(sum(case when weekday=\'Tuesday\' then total end),0) as Tuesday
,ifnull(sum(case when weekday=\'Wednesday\' then total end),0) as Wednesday
,ifnull(sum(case when weekday=\'Thursday\' then total end),0) as Thursday
,ifnull(sum(case when weekday=\'Friday\' then total end),0) as Friday
,ifnull(sum(case when weekday=\'Saturday\' then total end),0) as Saturday
,ifnull(sum(case when weekday=\'Sunday\' then total end),0) as Sunday

from cte
group by 1
order by category

</p>


