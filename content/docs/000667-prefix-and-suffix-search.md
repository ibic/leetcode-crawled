---
title: "Prefix and Suffix Search"
weight: 667
#id: "prefix-and-suffix-search"
---
## Description
<div class="description">
<p>Design a special dictionary which has some words and allows you to search the words in it by a prefix and a suffix.</p>

<p>Implement the <code>WordFilter</code> class:</p>

<ul>
	<li><code>WordFilter(string[] words)</code> Initializes the object with the <code>words</code> in the dictionary.</li>
	<li><code>f(string prefix, string suffix)</code> Returns <em>the index of the word in the dictionary</em> which has the prefix <code>prefix</code> and the suffix <code>suffix</code>. If there is&nbsp;more than one valid index, return <strong>the largest</strong> of them. If there is no such word in the dictionary, return <code>-1</code>.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;WordFilter&quot;, &quot;f&quot;]
[[[&quot;apple&quot;]], [&quot;a&quot;, &quot;e&quot;]]
<strong>Output</strong>
[null, 0]

<strong>Explanation</strong>
WordFilter wordFilter = new WordFilter([&quot;apple&quot;]);
wordFilter.f(&quot;a&quot;, &quot;e&quot;); // return 0, because the word at index 0 has prefix = &quot;a&quot; and suffix = &#39;e&quot;.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= words.length &lt;= 15000</code></li>
	<li><code>1 &lt;= words[i].length &lt;= 10</code></li>
	<li><code>1 &lt;= prefix.length, suffix.length&nbsp;&lt;= 10</code></li>
	<li><code>words[i]</code>, <code>prefix</code> and <code>suffix</code> consist of lower-case English letters only.</li>
	<li>At most <code>15000</code> calls will be made to the function <code>f</code>.</li>
</ul>

</div>

## Tags
- Trie (trie)

## Companies
- Google - 2 (taggedByAdmin: false)
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Trie + Set Intersection [Time Limit Exceeded]

**Intuition and Algorithm**

We use two tries to separately find all words that match the prefix, plus all words that match the suffix.  Then, we try to find the highest weight element in the intersection of these sets.

Of course, these sets could still be large, so we might TLE if we aren't careful.

<iframe src="https://leetcode.com/playground/Amgydcwk/shared" frameBorder="0" width="100%" height="500" name="Amgydcwk"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(NK + Q(N+K))$$ where $$N$$ is the number of words, $$K$$ is the maximum length of a word, and $$Q$$ is the number of queries.  If we use memoization in our solution, we could produce tighter bounds for this complexity, as the complex queries are somewhat disjoint.

* Space Complexity: $$O(NK)$$, the size of the tries.

---
#### Approach #2: Paired Trie [Accepted]

**Intuition and Algorithm**

Say we are inserting the word `apple`.  We could insert `('a', 'e'), ('p', 'l'), ('p', 'p'), ('l', 'p'), ('e', 'a')` into our trie.  Then, if we had equal length queries like `prefix = "ap", suffix = "le"`, we could find the node `trie['a', 'e']['p', 'l']` in our trie.  This seems promising.

What about queries that aren't equal?  We should just insert them like normal.  For example, to capture a case like `prefix = "app", suffix = "e"`, we could create nodes `trie['a', 'e']['p', None]['p', None]`.

After inserting these pairs into our trie, our searches are straightforward.

<iframe src="https://leetcode.com/playground/hf4KN24N/shared" frameBorder="0" width="100%" height="500" name="hf4KN24N"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(NK^2 + QK)$$ where $$N$$ is the number of words, $$K$$ is the maximum length of a word, and $$Q$$ is the number of queries.

* Space Complexity: $$O(NK^2)$$, the size of the trie.

---
#### Approach #3: Trie of Suffix Wrapped Words [Accepted]

**Intuition and Algorithm**

Consider the word `'apple'`.  For each suffix of the word, we could insert that suffix, followed by `'#'`, followed by the word, all into the trie.

For example, we will insert `'#apple', 'e#apple', 'le#apple', 'ple#apple', 'pple#apple', 'apple#apple'` into the trie.  Then for a query like `prefix = "ap", suffix = "le"`, we can find it by querying our trie for `le#ap`.

<iframe src="https://leetcode.com/playground/P62QwBFc/shared" frameBorder="0" width="100%" height="500" name="P62QwBFc"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(NK^2 + QK)$$ where $$N$$ is the number of words, $$K$$ is the maximum length of a word, and $$Q$$ is the number of queries.

* Space Complexity: $$O(NK^2)$$, the size of the trie.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Three ways to solve this problem in Java
- Author: motorix
- Creation Date: Mon Dec 11 2017 03:10:51 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 02:01:39 GMT+0800 (Singapore Standard Time)

<p>
Before solving this problem, we need to know which operation is called the most.

If ```f()``` is more frequently than ```WordFilter()```, use method 1. 
If **space complexity** is concerned, use method 2. 
If the input string array changes frequently, i.e., ```WordFilter()``` is more frequently than ```f()```, use method 3.

**< Method 1 >**
WordFilter: Time = O(NL^2) 
f: Time = O(1)
Space = O(NL^2)
Note: N is the size of input array and L is the max length of input strings. 
```
class WordFilter {
    HashMap<String, Integer> map = new HashMap<>();

    public WordFilter(String[] words) {
        for(int w = 0; w < words.length; w++){
            for(int i = 0; i <= 10 && i <= words[w].length(); i++){
                for(int j = 0; j <= 10 && j <= words[w].length(); j++){
                    map.put(words[w].substring(0, i) + "#" + words[w].substring(words[w].length()-j), w);
                }
            }
        }
    }

    public int f(String prefix, String suffix) {
        return (map.containsKey(prefix + "#" + suffix))? map.get(prefix + "#" + suffix) : -1;
    }
}

```

**< Method 2 >**
WordFilter: Time = O(NL)
f: Time = O(N)
Space = O(NL)
```
class WordFilter {
    HashMap<String, List<Integer>> mapPrefix = new HashMap<>();
    HashMap<String, List<Integer>> mapSuffix = new HashMap<>();
    
    public WordFilter(String[] words) {
        
        for(int w = 0; w < words.length; w++){
            for(int i = 0; i <= 10 && i <= words[w].length(); i++){
                String s = words[w].substring(0, i);
                if(!mapPrefix.containsKey(s)) mapPrefix.put(s, new ArrayList<>());
                mapPrefix.get(s).add(w);
            }
            for(int i = 0; i <= 10 && i <= words[w].length(); i++){
                String s = words[w].substring(words[w].length() - i);
                if(!mapSuffix.containsKey(s)) mapSuffix.put(s, new ArrayList<>());
                mapSuffix.get(s).add(w);
            }
        }

    public int f(String prefix, String suffix) {
        
        if(!mapPrefix.containsKey(prefix) || !mapSuffix.containsKey(suffix)) return -1;
        List<Integer> p = mapPrefix.get(prefix);
        List<Integer> s = mapSuffix.get(suffix);
        int i = p.size()-1, j = s.size()-1;
        while(i >= 0 && j >= 0){
            if(p.get(i) < s.get(j)) j--;
            else if(p.get(i) > s.get(j)) i--;
            else return p.get(i);
        }
        return -1;
```

**< Method 3 >** 
WordFilter: Time = O(1)
f: Time = O(NL)
Space = O(1)

```
class WordFilter {
    String[] input;
    public WordFilter(String[] words) {
        input = words;
    }
    public int f(String prefix, String suffix) {
        for(int i = input.length-1; i >= 0; i--){
            if(input[i].startsWith(prefix) && input[i].endsWith(suffix)) return i;
        }
        return -1;
    }
}

```

Note: The format of input data:
**First line**: ```["WordFilter", "f", ..., "f"]```, depends on how many time ```f()``` is called.
**Second line**: First item is the input string array, following by the pairs of ```[prefix, suffix]```.
ex:
```
["WordFilter","f", "f"]
[[["apple"]],["a","e"], ["ap","ple"]]
```
</p>


### Java Beat 95%, just small modifications in implementing Trie.
- Author: ytyrain
- Creation Date: Tue Jul 03 2018 15:21:44 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 11:58:08 GMT+0800 (Singapore Standard Time)

<p>
Take "apple" as an example, we will insert add "apple{apple", "pple{apple", "ple{apple", "le{apple", "e{apple", "{apple" into the Trie Tree. 
If the query is: prefix = "app", suffix = "le", we can find it by querying our trie for 
"le { app".
We use \'{\' because in ASCii Table, \'{\' is next to \'z\', so we just need to create new TrieNode[27] instead of 26. Also, compared with tradition Trie, we add the attribute weight in class TrieNode.
```
class TrieNode {
    TrieNode[] children;
    int weight;
    public TrieNode() {
        children = new TrieNode[27]; // \'a\' - \'z\' and \'{\'. \'z\' and \'{\' are neighbours in ASCII table
        weight = 0;
    }
}

public class WordFilter {
    TrieNode root;
    public WordFilter(String[] words) {
        root = new TrieNode();
        for (int weight = 0; weight < words.length; weight++) {
            String word = words[weight] + "{";
            for (int i = 0; i < word.length(); i++) {
                TrieNode cur = root;
                cur.weight = weight;
    // add "apple{apple", "pple{apple", "ple{apple", "le{apple", "e{apple", "{apple" into the Trie Tree
                for (int j = i; j < 2 * word.length() - 1; j++) {
                    int k = word.charAt(j % word.length()) - \'a\';
                    if (cur.children[k] == null)
                        cur.children[k] = new TrieNode();
                    cur = cur.children[k];
                    cur.weight = weight;
                }
            }
        }
    }
    public int f(String prefix, String suffix) {
        TrieNode cur = root;
        for (char c: (suffix + \'{\' + prefix).toCharArray()) {
            if (cur.children[c - \'a\'] == null) {
                return -1;
            }
            cur = cur.children[c - \'a\'];
        }
        return cur.weight;
    }
}
```
</p>


### Python few ways to do it, with EXPLANATIONS! \U0001f389
- Author: yangshun
- Creation Date: Sun Dec 10 2017 16:40:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 11 2018 12:11:21 GMT+0800 (Singapore Standard Time)

<p>
*By Yangshun*

#### Method 1

Store all the words corresponding to its prefixes and suffixes. For example, for two words `bat` and `bar`, the `prefixes` and `suffixes` dictionary will look as such:

```
# prefixes
{
  '': {'bat', 'bar'},
  'b': {'bat', 'bar'},
  'ba': {'bat', 'bar'},
  'bar': {'bar'},
  'bat': {'bat'},
}

# suffixes
{
  't': {'bat'},
  'at': {'bat'},
  'bat': {'bat'},
  'r': {'bar'},
  'ar': {'bar'},
  'bar': {'bar'},
}
```

`f('b', 'at')` => set intersection of `{'bat', 'bar'}` and `{'bat'}` => `'bat'`.

You can use a Trie to make it more space efficient as well.

```
class WordFilter(object):

    def __init__(self, words):
        from collections import defaultdict
        self.prefixes = defaultdict(set)
        self.suffixes = defaultdict(set)
        self.weights = {}
        for index, word in enumerate(words):
            prefix, suffix = '', ''
            for char in [''] + list(word):
                prefix += char
                self.prefixes[prefix].add(word)
            for char in [''] + list(word[::-1]):
                suffix += char
                self.suffixes[suffix[::-1]].add(word)
            self.weights[word] = index

    def f(self, prefix, suffix):
        weight = -1
        for word in self.prefixes[prefix] & self.suffixes[suffix]:
            if self.weights[word] > weight:
                weight = self.weights[word]
        return weight
```

#### Method 2

Directly save the prefix and suffix combinations for a word, where the value is the weight. For a word such as `'bat'`, store all the prefix + suffix combinations in a dictionary, delimited by a non-alphabet character such as `'.'`. The delimiter is important so as to distinguish between prefix/suffix pairs that would have been concatenated to give the same result if without - `ab` + `c` and `a` + `bc` would both give `abc` if there wasn't a delimiter present.

```
{
  '.': 0,
  '.t': 0,
  '.at': 0,
  '.bat': 0,
  'b.': 0,
  'b.t': 0,
  'b.at': 0,
  'b.bat': 0,
  'ba.': 0,
  'ba.t': 0,
  'ba.at': 0,
  'ba.bat': 0,
  'bat.': 0,
  'bat.t': 0,
  'bat.at': 0,
  'bat.bat': 0,
}
```

Later occurrences of the prefix + suffix combi will have its weight overwritten, so we can simply look up the dictionary and return the answer.

```
class WordFilter(object):

    def __init__(self, words):
        self.inputs = {}
        for index, word in enumerate(words):
            prefix = ''
            for char in [''] + list(word):
                prefix += char
                suffix = ''
                for char in [''] + list(word[::-1]):
                    suffix += char
                    self.inputs[prefix + '.' + suffix[::-1]] = index

    def f(self, prefix, suffix):
        return self.inputs.get(prefix + '.' + suffix, -1)
```
</p>


