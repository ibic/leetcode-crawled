---
title: "Missing Element in Sorted Array"
weight: 968
#id: "missing-element-in-sorted-array"
---
## Description
<div class="description">
<p>Given a sorted array <code>A</code> of <strong>unique</strong> numbers, find the <code><em>K</em>-th</code> missing number starting from the leftmost number of the array.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[4,7,9,10]</span>, K = 1
<strong>Output: </strong><span id="example-output-1">5</span>
<strong>Explanation: </strong>
The first missing number is 5.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">[4,7,9,10]</span>, K = 3
<strong>Output: </strong><span id="example-output-2">8</span>
<strong>Explanation: </strong>
The missing numbers are [5,6,8,...], hence the third missing number is 8.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-3-1">[1,2,4]</span>, K = 3
<strong>Output: </strong><span id="example-output-3">6</span>
<strong>Explanation: </strong>
The missing numbers are [3,5,6,7,...], hence the third missing number is 6.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 50000</code></li>
	<li><code>1 &lt;= A[i] &lt;= 1e7</code></li>
	<li><code>1 &lt;= K &lt;= 1e8</code></li>
</ol>
</div>

## Tags
- Binary Search (binary-search)

## Companies
- Facebook - 13 (taggedByAdmin: false)
- Google - 11 (taggedByAdmin: false)
- Amazon - 5 (taggedByAdmin: true)
- Bloomberg - 4 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: One Pass

**Intuition**

The problem is similar to [First Missing Positive](https://leetcode.com/articles/first-missing-positive/)
and the naive idea would be to solve it in a similar way 
by one pass approach.

Let's first assume that one has a function `missing(idx)`
that returns how many numbers are missing until the element
at index `idx`. 

![fig](../Figures/1060/function.png)

With the help of such a function the solution is
straightforward :

- Find an index such that `missing(idx - 1) < k <= missing(idx)`.
In other words, that means that kth missing number is in-between
`nums[idx - 1]` and `nums[idx]`. 

    One even could 
    compute a difference between kth missing number and 
    `nums[idx - 1]`. First, there are `missing(idx - 1)`
    missing numbers until `nums[idx - 1]`. 
    Second, all `k - missing(idx - 1)` missing numbers from
    `nums[idx - 1]` to kth missing are _consecutive ones_,
    because all of them are less than `nums[idx]` and hence
    there is nothing to separate them.
    Together that means that kth smallest is
    larger than `nums[idx - 1]` by `k - missing(idx - 1)`.

- Return kth smallest `nums[idx - 1] + k - missing(idx - 1)`.

![fic](../Figures/1060/algor.png)

> The last thing to discuss is how to implement `missing(idx)` function.

Let's consider an array element at index `idx`. If there is no numbers
missing, the element should be equal to `nums[idx] = nums[0] + idx`.
If k numbers are missing, the element should be equal to
`nums[idx] = nums[0] + idx + k`. 
Hence the number of missing elements is equal to
`nums[idx] - nums[0] - idx`.

![pic](../Figures/1060/missing.png)

**Algorithm**

- Implement `missing(idx)` function that returns how many numbers
are missing until array element with index `idx`.
Function returns `nums[idx] - nums[0] - idx`.

- Find an index such that `missing(idx - 1) < k <= missing(idx)`
by a linear search.

- Return kth smallest `nums[idx - 1] + k - missing(idx - 1)`.

**Implementation**

<iframe src="https://leetcode.com/playground/BhRaH34k/shared" frameBorder="0" width="100%" height="463" name="BhRaH34k"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since in the worst case 
it's one pass along the array.

* Space complexity: $$\mathcal{O}(1)$$ since it's a constant space solution.
<br /> 
<br />


---
#### Approach 2: Binary Search

**Intuition**

Approach 1 uses the linear search and 
doesn't profit from the fact that array is _sorted_.
One could replace the linear search by a [binary one](https://leetcode.com/articles/binary-search/) 
and 
reduce the time complexity from $$\mathcal{O}(N)$$ 
down to $$\mathcal{O}(\log N)$$.

> The idea is to find the leftmost element such that 
the number of missing numbers until this element 
is less or equal to k.

![fif](../Figures/1060/inary.png)

**Algorithm**

- Implement `missing(idx)` function that returns how many numbers
are missing until array element with index `idx`.
Function returns `nums[idx] - nums[0] - idx`.

- Find an index such that `missing(idx - 1) < k <= missing(idx)`
by a _binary search_.

- Return kth smallest `nums[idx - 1] + k - missing(idx - 1)`.

**Implementation**

<iframe src="https://leetcode.com/playground/xMQHrhUF/shared" frameBorder="0" width="100%" height="500" name="xMQHrhUF"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(\log N)$$ since it's a binary search algorithm
in the worst case when the missing
number is less than the last element of the array.

* Space complexity : $$\mathcal{O}(1)$$ since it's a constant space solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(logN) solution - Binary Search
- Author: Blink_fw
- Creation Date: Sun Jun 02 2019 01:56:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 01:57:45 GMT+0800 (Singapore Standard Time)

<p>
Let **missingNum** be amount of missing number in the array. Two cases that need to be handled:

1. missingNum < k, then return nums[n - 1] + k - missingNum
2. missingNum >= k, then use binary search(**during the search k will be updated**) to find the **index** in the array, where the kth missing number will be located in (nums[**index**], nums[**index** + 1]), return nums[**index**] + k 

```
class Solution {
    public int missingElement(int[] nums, int k) {
        int n = nums.length;
        int l = 0;
        int h = n - 1;
        int missingNum = nums[n - 1] - nums[0] + 1 - n;
        
        if (missingNum < k) {
            return nums[n - 1] + k - missingNum;
        }
        
        while (l < h - 1) {
            int m = l + (h - l) / 2;
            int missing = nums[m] - nums[l] - (m - l);
            
            if (missing >= k) {
			    // when the number is larger than k, then the index won\'t be located in (m, h]
                h = m;
            } else {
			    // when the number is smaller than k, then the index won\'t be located in [l, m), update k -= missing
                k -= missing;
                l = m;
            }
        }
        
        return nums[l] + k;
    }
}
```
</p>


### Python3 with detailed explanation - Binary Search O(longN)
- Author: allen791210
- Creation Date: Wed Nov 13 2019 12:50:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Dec 20 2019 23:37:38 GMT+0800 (Singapore Standard Time)

<p>
```python
class Solution:
    def missingElement(self, nums: List[int], k: int) -> int:
        if not nums or k == 0:
            return 0
        
        diff = nums[-1] - nums[0] + 1 # complete length
        missing = diff - len(nums) # complete length - real length
        if k > missing: # if k is larger than the number of mssing words in sequence
            return nums[-1] + k - missing
        
        left, right = 0, len(nums) - 1
        while left + 1 < right:
            mid = (left + right) // 2
            missing = nums[mid] - nums[left] - (mid - left)
            if missing < k:
                left = mid
                k -= missing # KEY: move left forward, we need to minus the missing words of this range
            else:
                right = mid
                
        return nums[left] + k # k should be between left and right index in the end
```
</p>


### C++ binary search
- Author: tangshuo123321
- Creation Date: Wed Jun 05 2019 00:09:28 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 05 2019 00:09:28 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    int missingElement(vector<int>& nums, int k) {
        int l = 0, h = nums.size();
        while(l < h) {
            int m = l + (h - l) / 2;
            nums[m] - m - k >= nums[0] ? h = m : l = m + 1;
        }
        return nums[0] + l + k - 1;
    }
};
```
</p>


