---
title: "Guess the Word"
weight: 787
#id: "guess-the-word"
---
## Description
<div class="description">
<p>This problem is an&nbsp;<strong><em>interactive problem</em></strong>&nbsp;new to the LeetCode platform.</p>

<p>We are given a word list of unique words, each word is 6 letters long, and one word in this list is chosen as <strong>secret</strong>.</p>

<p>You may call <code>master.guess(word)</code>&nbsp;to guess a word.&nbsp; The guessed word should have&nbsp;type <code>string</code>&nbsp;and must be from the original list&nbsp;with 6 lowercase letters.</p>

<p>This function returns an&nbsp;<code>integer</code>&nbsp;type, representing&nbsp;the number of exact matches (value and position) of your guess to the <strong>secret word</strong>.&nbsp; Also, if your guess is not in the given wordlist, it will return <code>-1</code> instead.</p>

<p>For each test case, you have 10 guesses to guess the word. At the end of any number of calls, if you have made 10 or less calls to <code>master.guess</code>&nbsp;and at least one of these guesses was the <strong>secret</strong>, you pass the testcase.</p>

<p>Besides the example test case below, there will be 5&nbsp;additional test cases, each with 100 words in the word list.&nbsp; The letters of each word in those testcases were chosen&nbsp;independently at random from <code>&#39;a&#39;</code> to <code>&#39;z&#39;</code>, such that every word in the given word lists is unique.</p>

<pre>
<strong>Example 1:</strong>
<strong>Input:</strong>&nbsp;secret = &quot;acckzz&quot;, wordlist = [&quot;acckzz&quot;,&quot;ccbazz&quot;,&quot;eiowzz&quot;,&quot;abcczz&quot;]

<strong>Explanation:</strong>

<code>master.guess(&quot;aaaaaa&quot;)</code> returns -1, because&nbsp;<code>&quot;aaaaaa&quot;</code>&nbsp;is not in wordlist.
<code>master.guess(&quot;acckzz&quot;) </code>returns 6, because&nbsp;<code>&quot;acckzz&quot;</code> is secret and has all 6&nbsp;matches.
<code>master.guess(&quot;ccbazz&quot;)</code> returns 3, because<code>&nbsp;&quot;ccbazz&quot;</code>&nbsp;has 3 matches.
<code>master.guess(&quot;eiowzz&quot;)</code> returns 2, because&nbsp;<code>&quot;eiowzz&quot;</code>&nbsp;has 2&nbsp;matches.
<code>master.guess(&quot;abcczz&quot;)</code> returns 4, because&nbsp;<code>&quot;abcczz&quot;</code> has 4 matches.

We made 5 calls to&nbsp;master.guess and one of them was the secret, so we pass the test case.
</pre>

<p><strong>Note:</strong>&nbsp; Any solutions that attempt to circumvent the judge&nbsp;will result in disqualification.</p>

</div>

## Tags
- Minimax (minimax)

## Companies
- Google - 18 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Minimax with Heuristic [Accepted]

**Intuition**

We can guess that having less words in the word list is generally better.  If the data is random, we can reason this is often the case.

Now let's use the strategy of making the guess that minimizes the maximum possible size of the resulting word list.  If we started with $$N$$ words in our word list, we can iterate through all possibilities for what the secret could be.

**Algorithm**

Store `H[i][j]` as the number of matches of `wordlist[i]` and `wordlist[j]`.  For each guess that hasn't been guessed before, do a minimax as described above, taking the guess that gives us the smallest group that might occur.

<iframe src="https://leetcode.com/playground/nDyYcugf/shared" frameBorder="0" width="100%" height="500" name="nDyYcugf"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2 \log N)$$, where $$N$$ is the number of words, and assuming their length is $$O(1)$$.  Each call to `solve` is $$O(N^2)$$, and the number of calls is bounded by $$O(\log N)$$.

* Space Complexity:  $$O(N^2)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Random Guess and Minimax Guess with Comparison
- Author: lee215
- Creation Date: Sun May 27 2018 11:05:11 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 13 2020 11:40:15 GMT+0800 (Singapore Standard Time)

<p>
# **Discuss**:
Random Guess and Minimax Guess with Comparison

# Foreword
Great! Finally we have an interactive problem on LeetCode platform.

The description emphasizes that,
the wordlist is generated randomly and it\'s does have a reason.

There is no solution that can guarantee to find a secret word in 10 tries.
If I make up a test case with wordlist like `["aaaaaa", "bbbbbb" ...., "zzzzzz"]`,
it needs 26 tries to find the secret.

So 10 tries is just a constrain to check a reasonable solution.
And instead of just finding right output from given input, it\'s more about a strategy.

**So it doesn\'t make any sense to do any hand-made "extra challendge".**
<br><br>


# Intuition
Take a word from wordlist and guess it.
Get the matches of this word
Update our wordlist and keep only the same matches to our guess.

This process is straight forward.
However, the key point is, which word should we guess from all of the wordlist?
<br><br>


# Prepare
For example we guess "aaaaaa" and get matches `x = 3`,
we keep the words with exactly 3 `a` .

Also we need to know the matches between two words,
so a helper function `match` as following will be useful.

**C++:**
```cpp
    int match(string a, string b) {
        int matches = 0;
        for (int i = 0; i < a.length(); ++i)
            if (a[i] == b[i])
                matches ++;
        return matches;
    }
```

**Java:**
```java
    public int match(String a, String b) {
        int matches = 0;
        for (int i = 0; i < a.length(); ++i)
            if (a.charAt(i) == b.charAt(i))
                matches ++;
        return matches;
    }
```

**Python**
```py
    def match(self, w1, w2):
        return sum(i == j for i, j in zip(w1, w2))
```
<br><br>





# Solution 1: Always Guess the First One
First of all, we just guessed the first word in the wordlist.
Unfortunately, it didn\'t get a lucky pass.
This problem has only 5 test cases but they are good.

Time complexity `O(N)`
Space complexity `O(N)`
<br><br>

# Solution 2.1: Shuffle the Wordlist
I didn\'t give up the previous idea, it\'s not that bad.
So I decided to try my luck by shuffling `wordlist` at the beginning.

Note that it may sound some unreliable,
but actully randomicity is very useful trick in both competition and reality problem.


This method can get accepted but not for sure.
After manualy testing locally,
on Leetcode\'s test cases set and random test cases set,
it has roughly 70% rate to get accepted.

However, C++ may set random seed in the OJ.
I keep submitting but doesn\'t fail.

Time complexity `O(N)`
Space complexity `O(N)`
<br>

**C++**
```cpp
    void findSecretWord(vector<string>& wordlist, Master& master) {
        random_shuffle(wordlist.begin(), wordlist.end());
        for (int i = 0, x = 0; i < 10 && x < 6; ++i) {
            string guess = wordlist[0];
            x = master.guess(guess);
            vector<string> wordlist2;
            for (string w : wordlist)
                if (match(guess, w) == x)
                    wordlist2.push_back(w);
            wordlist = wordlist2;
        }
    }
```

**Python:**
```py
    def findSecretWord(self, wordlist, master):
        random.shuffle(wordlist)
        for i in range(10):
            guess = random.choice(0)
            x = master.guess(guess)
            wordlist = [w for w in wordlist if sum(i == j for i, j in zip(guess, w)) == x]
```
<br><br>



# Solution 2.2: Guess a Random Word
All words are generated randomly.
So why not we also guess a random word and let it be whatever will be.
This is actually the same as the previous solution.
Though we don\'t need one more `O(N)` operation to shuffle the `wordlist` at first.

Time complexity `O(N)`
Space complexity `O(N)`

**C++:**
```cpp
    void findSecretWord(vector<string>& wordlist, Master& master) {
        for (int i = 0, x = 0; i < 10 && x < 6; ++i) {
            string guess = wordlist[rand() % (wordlist.size())];
            x = master.guess(guess);
            vector<string> wordlist2;
            for (string w : wordlist)
                if (match(guess, w) == x)
                    wordlist2.push_back(w);
            wordlist = wordlist2;
        }
    }
```

**Java:**
```java
    public void findSecretWord(String[] wordlist, Master master) {
        for (int i = 0, x = 0; i < 10 && x < 6; ++i) {
            String guess = wordlist[new Random().nextInt(wordlist.length)];
            x = master.guess(guess);
            List<String> wordlist2 = new ArrayList<>();
            for (String w : wordlist)
                if (match(guess, w) == x)
                    wordlist2.add(w);
            wordlist = wordlist2.toArray(new String[wordlist2.size()]);
        }
    }
```
**Python:**
```py
    def findSecretWord(self, wordlist, master):
        for i in range(10):
            guess = random.choice(wordlist)
            x = master.guess(guess)
            wordlist = [w for w in wordlist if sum(i == j for i, j in zip(guess, w)) == x]
```
<br><br>


# Solution 3: Minimax

Now we want to try a better solution.
Generally, we will get 0 matches from the `master.guess`.
As a result, the size of `wordlist` reduces slowly.

Recall some math here, the possiblity that get 0 matched is:
(25/26) ^ 6 = 79.03%

That is to say, if we make a blind guess,
we have about 80% chance to get 0 matched with the secret word.

To simplify the model,
we\'re going to assume that,
we will always run into the worst case (get 0 matched).

In this case,
we have 80% chance to eliminate the candidate word
as well as its close words which have at least 1 match.

Additionally, in order to delete a max part of words,
we select a candidate who has a big "family",
(that is, the fewest 0 matched with other words.)
**We want to guess a word that can minimum our worst outcome.**

So we compare each two words and count their matches.
For each word, we note how many word of 0 matches it gets.
Then we guess the word with minimum words of 0 matches.

In this solution, we apply a minimax idea.
We minimize our worst case,
The worst case is `max(the number of words with x matches)`,
and we assume it equal to "the number of words with 0 matches"

Time complexity `O(N^2)`
Space complexity `O(N)`

**C++:**
```cpp
    void findSecretWord(vector<string>& wordlist, Master& master) {
        for (int i = 0, x = 0; i < 10 && x < 6; ++i) {
            unordered_map<string, int> count;
            for (string w1 : wordlist)
                for (string w2 : wordlist)
                    if (match(w1, w2) == 0)
                        count[w1]++;
            pair<string, int> minimax = {wordlist[0], 1000};
            for (string w : wordlist)
                if (count[w] <= minimax.second)
                    minimax = make_pair(w, count[w]);
            x = master.guess(minimax.first);
            vector<string> wordlist2;
            for (string w : wordlist)
                if (match(minimax.first, w) == x)
                    wordlist2.push_back(w);
            wordlist = wordlist2;
        }
    }
```

**Java:**
```java
    public void findSecretWord(String[] wordlist, Master master) {
        for (int i = 0, x = 0; i < 10 && x < 6; ++i) {
            HashMap<String, Integer> count = new HashMap<>();
            for (String w1 : wordlist)
                for (String w2 : wordlist)
                    if (match(w1, w2) == 0)
                        count.put(w1, count.getOrDefault(w1 , 0) + 1);
            String guess = "";
            int min0 = 100;
            for (String w : wordlist)
                if (count.getOrDefault(w, 0) < min0) {
                    guess = w;
                    min0 = count.getOrDefault(w, 0);
                }
            x = master.guess(guess);
            List<String> wordlist2 = new ArrayList<String>();
            for (String w : wordlist)
                if (match(guess, w) == x)
                    wordlist2.add(w);
            wordlist = wordlist2.toArray(new String[0]);
        }
    }
```
**Python:**
```py
    def findSecretWord(self, wordlist, master):
        x = 0
        while x < 6:
            count = collections.Counter(w1 for w1, w2 in itertools.permutations(wordlist, 2) if self.match(w1, w2) == 0)
            guess = min(wordlist, key=lambda w: count[w])
            x = master.guess(guess)
            wordlist = [w for w in wordlist if self.match(w, guess) == x]
```
<br><br>

# **Result Analyse**
To be more convincing, I test each approach with 1000 test cases.
For the random approach, average 6.5 guess, worst case 14 guess.
For the minimax  approach, average 5.5 guess, worst case 10 guess.
I draw this diagram to visualize the result:
![image](https://s3-lc-upload.s3.amazonaws.com/users/lee215/image_1527391162.png)
<br><br>

# Solution 4: Count the Occurrence of Characters
In the previous solution, we compaired each two words.
This make the complexity `O(N^2)` for each turn.

But actually we don\'t have to do that.
We just need to count the occurrence for each character on each position.

If we can guess the word that not in the wordlist,
we can guess the word based on the most frequent character on the position.

Here we have to guess a word from the list,
we still can calculate a score of similarity for each word,
and guess the word with highest score.

Time complexity `O(N)`
Space complexity `O(N)`
<br>

**Java**
```java
    public void findSecretWord(String[] wordlist, Master master) {
        for (int t = 0, x = 0; t < 10 && x < 6; ++t) {
            int count[][] = new int[6][26], best = 0;
            for (String w : wordlist)
                for (int i = 0; i < 6; ++i)
                    count[i][w.charAt(i) - \'a\']++;
            String guess = wordlist[0];
            for (String w: wordlist) {
                int score = 0;
                for (int i = 0; i < 6; ++i)
                    score += count[i][w.charAt(i) - \'a\'];
                if (score > best) {
                    guess = w;
                    best = score;
                }
            }
            x = master.guess(guess);
            List<String> wordlist2 = new ArrayList<String>();
            for (String w : wordlist)
                if (match(guess, w) == x)
                    wordlist2.add(w);
            wordlist = wordlist2.toArray(new String[0]);
        }
    }
```
**C++**
```cpp
    void findSecretWord(vector<string>& wordlist, Master& master) {
        int count[6][26], x = 0, best;
        for (int t = 0; t < 10 && x < 6; ++t) {
            memset(count, 0, 156 * sizeof(int));
            for (string w : wordlist)
                for (int i = 0; i < 6; ++i)
                    count[i][w[i] - \'a\']++;
            best = 0;
            string guess = wordlist[0];
            for (string w: wordlist) {
                int score = 0;
                for (int i = 0; i < 6; ++i)
                    score += count[i][w[i] - \'a\'];
                if (score > best) {
                    guess = w;
                    best = score;
                }
            }
            x = master.guess(guess);
            vector<string> wordlist2;
            for (string w : wordlist)
                if (match(guess, w) == x)
                    wordlist2.push_back(w);
            wordlist = wordlist2;
        }
    }
```
**Python**
```py
    def findSecretWord(self, wordlist, master):
        def match(w1, w2):
            return sum(i == j for i, j in zip(w1, w2))

        n = 0
        while n < 6:
            count = [collections.Counter(w[i] for w in wordlist) for i in xrange(6)]
            guess = max(wordlist, key=lambda w: sum(count[i][c] for i, c in enumerate(w)))
            n = master.guess(guess)
            wordlist = [w for w in wordlist if match(w, guess) == n]
```


</p>


### Python O(n) with maximum overlap heuristic
- Author: yorkshire
- Creation Date: Sat Aug 18 2018 08:08:33 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 23:49:40 GMT+0800 (Singapore Standard Time)

<p>
Same as other solutions - repeatedly choose a word to guess and eliminate all words that do not have the same number of matches as the
chosen word.

To choose a word, calculate a matrix counting the number of words where each char is in each position. Then score each word by adding the number of words that have the same chars in the same positions.

So this is O(n) time and not O(n^2) because I don\'t check all pairs.

```
class Solution(object):
    def findSecretWord(self, wordlist, master):
		
        def pair_matches(a, b):         # count the number of matching characters
            return sum(c1 == c2 for c1, c2 in zip(a, b))

        def most_overlap_word():
            counts = [[0 for _ in range(26)] for _ in range(6)]     # counts[i][j] is nb of words with char j at index i
            for word in candidates:
                for i, c in enumerate(word):
                    counts[i][ord(c) - ord("a")] += 1

            best_score = 0
            for word in candidates:
                score = 0
                for i, c in enumerate(word):
                    score += counts[i][ord(c) - ord("a")]           # all words with same chars in same positions
                if score > best_score:
                    best_score = score
                    best_word = word

            return best_word

        candidates = wordlist[:]        # all remaining candidates, initially all words
        while candidates:

            s = most_overlap_word()     # guess the word that overlaps with most others
            matches = master.guess(s)

            if matches == 6:
                return

            candidates = [w for w in candidates if pair_matches(s, w) == matches]   # filter words with same matches
```
</p>


### Optimal MinMax Solution (+ extra challenging test cases)
- Author: _pzh
- Creation Date: Mon May 28 2018 18:18:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 10:36:36 GMT+0800 (Singapore Standard Time)

<p>
**Intuition:**
We start by taking all words as potential candidates. If we guess a word, we\'re given its distance from the secret, which allows us to eliminate words whose distance from the guess is different. That is, if we know that the secret is 4 characters away from the guess, then we can eliminate all words whose distance from the guess is not 4, because they can\'t be the secret.

In order to maximize the number of words we eliminate at each guess, we choose a guess that partitions the potential candidate set roughly equally by all possible distances. That is, if we choose a guess that\'s roughly equally at distance 0 from 1/6 of all words, distance 1 from 1/6 of all words, etc., we know that whatever distance the secret happens to be from the guess, we can eliminate a substantial number of words. This is where we use a MinMax heuristic. For each word, we compute a histogram of its distance from every word in the candidate set, and then select the word whose histogram peak (max) is smallest (min). That property guarantees that the selected word partitions the candidate set well by distance and that it has the potential to eliminate the maximum number of elements. That\'s the word we choose as a guess. Once we offer it and obtain its distance from the secret, then we can eliminate all candidates that don\'t have the same distance to it.

We repeat the same process iteratively, taking words from the full word set, but compute the histogram of their distances to the reduced candidate set only. The reason is that a word that is bad at partitioning the full word set may be good at partitioning the reduced candidate set at a later iteration. Choosing guess words from the full set also allows us to avoid falling into a local optimum valley where the reduced candidate set contains only words that partition it very poorly.


**Solution:**

```
class Solution {

    int dist(const string& a, const string &b) 
    {
        // Maybe this can be memoized if too slow.
        int dist = 0;
        for (int idx = 0; idx < a.size(); ++idx) {
            dist += a[idx] == b[idx];
        }
        return dist;
    }
    
    int maxEquidistantSetSize(const string& word, const unordered_set<string>& guessSet) 
    {
        vector<int> hist(word.size() + 1, 0);    
        for (auto& guess : guessSet) {
            ++hist[dist(word, guess)];
        }
        return *max_element(hist.cbegin(), hist.cend());
    }
    
    string maxPartitioningGuess(const vector<string>& wordlist, const unordered_set<string>& guessSet)
    {
        auto maxGuessIt = wordlist.cend();
        int minMax = wordlist.size();
        for (auto it = wordlist.cbegin(); it != wordlist.cend(); ++it) {
            int curMax = maxEquidistantSetSize(*it, guessSet);
            if (curMax < minMax) {
                minMax = curMax;
                maxGuessIt = it;
            }
        }
        return *maxGuessIt;
    }
    
public:
    void findSecretWord(vector<string>& wordlist, Master& master) 
    {
        // Start guessing
        unordered_set<string> guessSet(wordlist.cbegin(), wordlist.cend());
        while (guessSet.size() > 1) {
            // Calculate max partitioning elem taken from full word list
            string guessWord = maxPartitioningGuess(wordlist, guessSet);
        
            // Try the guess
            int d = master.guess(guessWord);
            if (d == guessWord.size()) return; // Got lucky!

            // Eliminate words whose distance != d
            for (auto it = guessSet.begin(); it != guessSet.end();) {
                if (dist(guessWord, *it) != d) {
                    it = guessSet.erase(it);
                } else {
                    ++it;
                }
            }
        }
        if (!guessSet.empty()) {
            master.guess(*guessSet.cbegin());
        }
    }
};
```

**Testing:**

Most of the other posted solutions so far can\'t seem to handle the following test cases:

```
"aaponm"
["aazyxw","aayxwv","aaxwvu","aawvut","aavuts","aautsr","aatsrq","aasrqp","aarqpo","aaqpon","aaponm","aaonml","aanmlk","aamlkj","aalkji","aakjih","aajihg","aaihgf","aahgfe","aagfed","aafedc","ccwwww","ccssss","ccoooo","cckkkk","ccgggg","cccccc","ccyyyy","ccuuuu","ccqqqq","ccmmmm","ddwwww","ddssss","ddoooo","ddkkkk","ddgggg","ddcccc","ddyyyy","dduuuu","ddqqqq","ddmmmm","eewwww","eessss","eeoooo","eekkkk","eegggg","eecccc","eeyyyy","eeuuuu","eeqqqq","eemmmm","ffwwww","ffssss","ffoooo","ffkkkk","ffgggg","ffcccc","ffyyyy","ffuuuu","ffqqqq","ffmmmm","ggwwww","ggssss","ggoooo","ggkkkk","gggggg","ggcccc","ggyyyy","gguuuu","ggqqqq","ggmmmm","hhwwww","hhssss","hhoooo","hhkkkk","hhgggg","hhcccc","hhyyyy","hhuuuu","hhqqqq","hhmmmm","iiwwww","iissss","iioooo","iikkkk","iigggg","iicccc","iiyyyy","iiuuuu","iiqqqq","iimmmm","jjwwww","jjssss","jjoooo","jjkkkk","jjgggg","jjcccc","jjyyyy","jjuuuu","jjqqqq"]
10
```

Here are some less strong ones:

```
"aiaaaa"
["azbbbb","aybbbb","axcccc","awcccc","avaaaa","auaaaa","ataaaa","asaaaa","araaaa","aqaaaa","agaaaa","ahaaaa","aiaaaa","ajaaaa","akaaaa","alaaaa","amaaaa","anaaaa","aoaaaa","apaaaa","afaaaa","aeaaaa","cxcccc","cwcccc","cvcccc","cucccc","ctcccc","cscccc","crcccc","cqcccc","dzdddd","dydddd","dxdddd","dwdddd","dvdddd","dudddd","dtdddd","dsdddd","drdddd","dqdddd","ezeeee","eyeeee","exeeee","eweeee","eveeee","eueeee","eteeee","eseeee","ereeee","eqeeee","fzffff","fyffff","fxffff","fwffff","fvffff","fuffff","ftffff","fsffff","frffff","fqffff","gzgggg","gygggg","gxgggg","gwgggg","gvgggg","gugggg","gtgggg","gsgggg","grgggg","gqgggg","hzhhhh","hyhhhh","hxhhhh","hwhhhh","hvhhhh","huhhhh","hthhhh","hshhhh","hrhhhh","hqhhhh","iziiii","iyiiii","ixiiii","iwiiii","iviiii","iuiiii","itiiii","isiiii","iriiii","iqiiii","jzjjjj","jyjjjj","jxjjjj","jwjjjj","jvjjjj","jujjjj","jtjjjj","jsjjjj","jrjjjj","jqjjjj"]
10

"azaaaa"
["azaaaa","ayaaaa","axaaaa","awaaaa","avaaaa","auaaaa","ataaaa","asaaaa","araaaa","aqaaaa","bzbbbb","bybbbb","bxbbbb","bwbbbb","bvbbbb","bubbbb","btbbbb","bsbbbb","brbbbb","apaaaa","czcccc","cycccc","cxcccc","cwcccc","cvcccc","cucccc","ctcccc","cscccc","crcccc","cqcccc","dzdddd","dydddd","dxdddd","dwdddd","dvdddd","dudddd","dtdddd","dsdddd","drdddd","dqdddd","ezeeee","eyeeee","exeeee","eweeee","eveeee","eueeee","eteeee","eseeee","ereeee","eqeeee","fzffff","fyffff","fxffff","fwffff","fvffff","fuffff","ftffff","fsffff","frffff","fqffff","gzgggg","gygggg","gxgggg","gwgggg","gvgggg","gugggg","gtgggg","gsgggg","grgggg","gqgggg","hzhhhh","hyhhhh","hxhhhh","hwhhhh","hvhhhh","huhhhh","hthhhh","hshhhh","hrhhhh","hqhhhh","iziiii","iyiiii","ixiiii","iwiiii","iviiii","iuiiii","itiiii","isiiii","iriiii","iqiiii","jzjjjj","jyjjjj","jxjjjj","jwjjjj","jvjjjj","jujjjj","jtjjjj","jsjjjj","jrjjjj","jqjjjj"]
10

"apaaaa"
["azbbbb","aybbbb","axcccc","awcccc","avdddd","audddd","ateeee","aseeee","arffff","aqffff","bzbbbb","bybbbb","bxbbbb","bwbbbb","akaaaa","alaaaa","amaaaa","anaaaa","aoaaaa","apaaaa","czcccc","cycccc","cxcccc","cwcccc","cvcccc","cucccc","ctcccc","cscccc","crcccc","cqcccc","dzdddd","dydddd","dxdddd","dwdddd","dvdddd","dudddd","dtdddd","dsdddd","drdddd","dqdddd","ezeeee","eyeeee","exeeee","eweeee","eveeee","eueeee","eteeee","eseeee","ereeee","eqeeee","fzffff","fyffff","fxffff","fwffff","fvffff","fuffff","ftffff","fsffff","frffff","fqffff","gzgggg","gygggg","gxgggg","gwgggg","gvgggg","gugggg","gtgggg","gsgggg","grgggg","gqgggg","hzhhhh","hyhhhh","hxhhhh","hwhhhh","hvhhhh","huhhhh","hthhhh","hshhhh","hrhhhh","hqhhhh","iziiii","iyiiii","ixiiii","iwiiii","iviiii","iuiiii","itiiii","isiiii","iriiii","iqiiii","jzjjjj","jyjjjj","jxjjjj","jwjjjj","jvjjjj","jujjjj","jtjjjj","jsjjjj","jrjjjj","jqjjjj"]
10
```

The point is that most solutions start from the minmax set and then proceed to use guesses from within the same reduced set. The following test case
```
"aaponm"
["aazyxw","aayxwv","aaxwvu","aawvut","aavuts","aautsr","aatsrq","aasrqp","aarqpo","aaqpon","aaponm","aaonml","aanmlk","aamlkj","aalkji","aakjih","aajihg","aaihgf","aahgfe","aagfed","aafedc","ccwwww","ccssss","ccoooo","cckkkk","ccgggg","cccccc","ccyyyy","ccuuuu","ccqqqq","ccmmmm","ddwwww","ddssss","ddoooo","ddkkkk","ddgggg","ddcccc","ddyyyy","dduuuu","ddqqqq","ddmmmm","eewwww","eessss","eeoooo","eekkkk","eegggg","eecccc","eeyyyy","eeuuuu","eeqqqq","eemmmm","ffwwww","ffssss","ffoooo","ffkkkk","ffgggg","ffcccc","ffyyyy","ffuuuu","ffqqqq","ffmmmm","ggwwww","ggssss","ggoooo","ggkkkk","gggggg","ggcccc","ggyyyy","gguuuu","ggqqqq","ggmmmm","hhwwww","hhssss","hhoooo","hhkkkk","hhgggg","hhcccc","hhyyyy","hhuuuu","hhqqqq","hhmmmm","iiwwww","iissss","iioooo","iikkkk","iigggg","iicccc","iiyyyy","iiuuuu","iiqqqq","iimmmm","jjwwww","jjssss","jjoooo","jjkkkk","jjgggg","jjcccc","jjyyyy","jjuuuu","jjqqqq"]
10
```
is constructed in such a way that the initial minmax set has 21 words at the same distance from one another. After the first guess, you reduce your candidates to this group:
```
"aazyxw","aayxwv","aaxwvu","aawvut","aavuts","aautsr","aatsrq","aasrqp","aarqpo","aaqpon","aaponm","aaonml","aanmlk","aamlkj","aalkji","aakjih","aajihg","aaihgf","aahgfe","aagfed","aafedc"
```
But since each word in this set is at equal distance, if you only use guesses from within that set, you can eliminate at most one word for each guess. 

The trick is that you should continue to use the complete word list to generate guesses. I.e. take words from outside of your current reduced set of potential guesses, and use them to calculate the minmax that would best partition the guess set . That way you can reduce the guess set signifcantly.

For example, if your second guess is "aazyxw", you eliminate only 1 word, but if you use an outside word like "ccwwww", you can eliminate 4 words ("aazyxw","aayxwv","aaxwvu","aawvut"), etc.

</p>


