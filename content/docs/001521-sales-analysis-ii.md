---
title: "Sales Analysis II"
weight: 1521
#id: "sales-analysis-ii"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Product</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| product_id   | int     |
| product_name | varchar |
| unit_price   | int     |
+--------------+---------+
product_id is the primary key of this table.
</pre>

<p>Table:&nbsp;<code>Sales</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| seller_id   | int     |
| product_id  | int     |
| buyer_id    | int     |
| sale_date   | date    |
| quantity    | int     |
| price       | int     |
+------ ------+---------+
This table has no primary key, it can have repeated rows.
product_id is a foreign key to Product table.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query that reports the <strong>buyers</strong> who have bought <em>S8</em> but not <em>iPhone</em>. Note that <em>S8</em> and <em>iPhone</em> are products present in the <code>Product</code> table.</p>

<p>The query result format is in the following example:</p>

<pre>
Product table:
+------------+--------------+------------+
| product_id | product_name | unit_price |
+------------+--------------+------------+
| 1          | S8           | 1000       |
| 2          | G4           | 800        |
| 3          | iPhone       | 1400       |
+------------+--------------+------------+

<code>Sales </code>table:
+-----------+------------+----------+------------+----------+-------+
| seller_id | product_id | buyer_id | sale_date  | quantity | price |
+-----------+------------+----------+------------+----------+-------+
| 1         | 1          | 1        | 2019-01-21 | 2        | 2000  |
| 1         | 2          | 2        | 2019-02-17 | 1        | 800   |
| 2         | 1          | 3        | 2019-06-02 | 1        | 800   |
| 3         | 3          | 3        | 2019-05-13 | 2        | 2800  |
+-----------+------------+----------+------------+----------+-------+

Result table:
+-------------+
| buyer_id    |
+-------------+
| 1           |
+-------------+
The buyer with id 1 bought an S8 but didn&#39;t buy an iPhone. The buyer with id 3 bought both.</pre>

</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple MySQL solution
- Author: tschijnmo
- Creation Date: Fri Jun 14 2019 10:21:57 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 14 2019 10:21:57 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT s.buyer_id
FROM Sales AS s INNER JOIN Product AS p
ON s.product_id = p.product_id
GROUP BY s.buyer_id
HAVING SUM(CASE WHEN p.product_name = \'S8\' THEN 1 ELSE 0 END) > 0
AND SUM(CASE WHEN p.product_name = \'iPhone\' THEN 1 ELSE 0 END) = 0;
```
</p>


### Another simple MySQL solution
- Author: celticrocks
- Creation Date: Fri Jun 14 2019 16:37:53 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 14 2019 16:37:53 GMT+0800 (Singapore Standard Time)

<p>
```
select distinct buyer_id
from sales join product using(product_id)
where product_name = \'S8\' 
and buyer_id not in (select distinct buyer_id
                    from sales join product using(product_id)
                    where product_name = \'iPhone\' )
```
</p>


### 99%, 100%. straightforward mysql sol
- Author: xionghq803
- Creation Date: Sat Mar 21 2020 21:50:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 21 2020 21:50:06 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT DISTINCT(buyer_id)
FROM Sales
WHERE buyer_id IN (
    SELECT buyer_id
    FROM Sales AS S LEFT JOIN Product AS P
    ON S.product_id = P.product_id
    WHERE product_name = \'S8\'
) AND buyer_id NOT IN (
    SELECT buyer_id
    FROM Sales AS S LEFT JOIN Product AS P
    ON S.product_id = P.product_id
    WHERE product_name = \'iPhone\'
)
```
</p>


