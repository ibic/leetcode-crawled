---
title: "Running Total for Different Genders"
weight: 1560
#id: "running-total-for-different-genders"
---
## Description
<div class="description">
<p>Table: <code>Scores</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| player_name   | varchar |
| gender        | varchar |
| day           | date    |
| score_points  | int     |
+---------------+---------+
(gender, day) is the primary key for this table.
A competition is held between females team and males team.
Each row of this table indicates that a player_name and with gender has scored score_point in someday.
Gender is &#39;F&#39; if the player is in females team and &#39;M&#39; if the player is in males team.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the total score for each gender at each day.</p>

<p>Order the result table by gender and day</p>

<p>The query result format is in the following example:</p>

<pre>
Scores table:
+-------------+--------+------------+--------------+
| player_name | gender | day        | score_points |
+-------------+--------+------------+--------------+
| Aron        | F      | 2020-01-01 | 17           |
| Alice       | F      | 2020-01-07 | 23           |
| Bajrang     | M      | 2020-01-07 | 7            |
| Khali       | M      | 2019-12-25 | 11           |
| Slaman      | M      | 2019-12-30 | 13           |
| Joe         | M      | 2019-12-31 | 3            |
| Jose        | M      | 2019-12-18 | 2            |
| Priya       | F      | 2019-12-31 | 23           |
| Priyanka    | F      | 2019-12-30 | 17           |
+-------------+--------+------------+--------------+
Result table:
+--------+------------+-------+
| gender | day        | total |
+--------+------------+-------+
| F      | 2019-12-30 | 17    |
| F      | 2019-12-31 | 40    |
| F      | 2020-01-01 | 57    |
| F      | 2020-01-07 | 80    |
| M      | 2019-12-18 | 2     |
| M      | 2019-12-25 | 13    |
| M      | 2019-12-30 | 26    |
| M      | 2019-12-31 | 29    |
| M      | 2020-01-07 | 36    |
+--------+------------+-------+
For females team:
First day is 2019-12-30, Priyanka scored 17 points and the total score for the team is 17.
Second day is 2019-12-31, Priya scored 23 points and the total score for the team is 40.
Third day is 2020-01-01, Aron scored 17 points and the total score for the team is 57.
Fourth day is 2020-01-07, Alice scored 23 points and the total score for the team is 80.
For males team:
First day is 2019-12-18, Jose scored 2 points and the total score for the team is 2.
Second day is 2019-12-25, Khali scored 11 points and the total score for the team is 13.
Third day is 2019-12-30, Slaman scored 13 points and the total score for the team is 26.
Fourth day is 2019-12-31, Joe scored 3 points and the total score for the team is 29.
Fifth day is 2020-01-07, Bajrang scored 7 points and the total score for the team is 36.
</pre>

</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 2 Simple Solutions (Window Function and Self-join)
- Author: yangmexi
- Creation Date: Thu Jan 02 2020 05:22:49 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 02 2020 11:23:20 GMT+0800 (Singapore Standard Time)

<p>
### Window Function
I guess the code explains itself. Pretty simple.
```
SELECT gender, day, 
       SUM(score_points) OVER(PARTITION BY gender ORDER BY day) AS total
FROM Scores
```
### Self-join
A typical problem of using self-join to calculate the running aggregated stats.
```
SELECT s1.gender, s1.day, SUM(s2.score_points) AS total
FROM Scores AS s1,
     Scores AS s2
WHERE s1.gender = s2.gender AND s2.day <= s1.day
GROUP BY s1.gender, s1.day
ORDER BY s1.gender, s1.day
```
</p>


### self join solution MYSQL, beats 97%
- Author: mc2011
- Creation Date: Mon Mar 02 2020 03:35:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 02 2020 03:35:49 GMT+0800 (Singapore Standard Time)

<p>
select a.gender, a.day, 
        sum(b.score_points) as total
from Scores a
left join Scores b on a.day >= b.day and a.gender = b.gender
group by a.gender, a.day
order by a.gender, a.day
;
</p>


### [MySQL] Cumulative sum using variable
- Author: ye15
- Creation Date: Wed Feb 12 2020 04:36:30 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 02 2020 08:14:13 GMT+0800 (Singapore Standard Time)

<p>
Define `@g` for gender and `@s` for score. When gender changes, score is reset. 

Implementation (506ms, 99.87%): 
```
SELECT 
    gender, 
    day, 
    CASE WHEN @g <> @g:=gender THEN @s:=score
    ELSE @s:=@s+score END AS total 
FROM (SELECT gender, day, score_points AS score 
      FROM Scores 
      ORDER BY gender, day) a, (SELECT @s:=0, @g:="F") init; 
```

A less efficient but more straightforward solution is to use self-join 

(1075ms, 88.12%): 
```
SELECT 
    a.gender, 
    a.day, 
    SUM(b.score_points) AS total 
FROM 
    Scores a 
    JOIN Scores b ON a.gender = b.gender AND a.day >= b.day
GROUP BY 1, 2
ORDER BY 1, 2; 
```
</p>


