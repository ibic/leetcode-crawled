---
title: "Avoid Flood in The City"
weight: 1365
#id: "avoid-flood-in-the-city"
---
## Description
<div class="description">
<p>Your country has an infinite number of lakes. Initially, all the lakes are empty, but when it rains over the <code>nth</code> lake, the <code>nth</code> lake becomes full of water. If it rains over a lake which is <strong>full of water</strong>, there will be a <strong>flood</strong>. Your goal is to avoid the flood in any lake.</p>

<p>Given an integer array <code>rains</code> where:</p>

<ul>
	<li><code>rains[i] &gt; 0</code> means there will be rains over the <code>rains[i]</code> lake.</li>
	<li><code>rains[i] == 0</code> means there are no rains this day and you can choose <strong>one lake</strong> this day and <strong>dry it</strong>.</li>
</ul>

<p>Return <em>an array <code>ans</code></em> where:</p>

<ul>
	<li><code>ans.length == rains.length</code></li>
	<li><code>ans[i] == -1</code> if <code>rains[i] &gt; 0</code>.</li>
	<li><code>ans[i]</code> is the lake you choose to dry in the <code>ith</code> day&nbsp;if <code>rains[i] == 0</code>.</li>
</ul>

<p>If there are multiple valid answers return <strong>any</strong> of them. If it is impossible to avoid flood return <strong>an empty array</strong>.</p>

<p>Notice that if you chose to dry a full lake, it becomes empty, but if you chose to dry an empty lake, nothing changes. (see example 4)</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> rains = [1,2,3,4]
<strong>Output:</strong> [-1,-1,-1,-1]
<strong>Explanation:</strong> After the first day full lakes are [1]
After the second day full lakes are [1,2]
After the third day full lakes are [1,2,3]
After the fourth day full lakes are [1,2,3,4]
There&#39;s no day to dry any lake and there is no flood in any lake.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> rains = [1,2,0,0,2,1]
<strong>Output:</strong> [-1,-1,2,1,-1,-1]
<strong>Explanation:</strong> After the first day full lakes are [1]
After the second day full lakes are [1,2]
After the third day, we dry lake 2. Full lakes are [1]
After the fourth day, we dry lake 1. There is no full lakes.
After the fifth day, full lakes are [2].
After the sixth day, full lakes are [1,2].
It is easy that this scenario is flood-free. [-1,-1,1,2,-1,-1] is another acceptable scenario.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> rains = [1,2,0,1,2]
<strong>Output:</strong> []
<strong>Explanation:</strong> After the second day, full lakes are  [1,2]. We have to dry one lake in the third day.
After that, it will rain over lakes [1,2]. It&#39;s easy to prove that no matter which lake you choose to dry in the 3rd day, the other one will flood.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> rains = [69,0,0,0,69]
<strong>Output:</strong> [-1,69,1,1,-1]
<strong>Explanation:</strong> Any solution on one of the forms [-1,69,x,y,-1], [-1,x,69,y,-1] or [-1,x,y,69,-1] is acceptable where 1 &lt;= x,y &lt;= 10^9
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> rains = [10,20,20]
<strong>Output:</strong> []
<strong>Explanation:</strong> It will rain over lake 20 two consecutive days. There is no chance to dry any lake.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= rains.length &lt;= 10^5</code></li>
	<li><code>0 &lt;= rains[i] &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Array (array)
- Hash Table (hash-table)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A Set and A Map - Lucid code with documented comments.
- Author: interviewrecipes
- Creation Date: Sun Jun 21 2020 12:00:59 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 17 2020 02:40:37 GMT+0800 (Singapore Standard Time)

<p>

**Two key things to note-**
1. When drying a lake #L, it is only useful to dry it if it is FULL already. Otherwise its of no use. (Explained this in the code with a small example as well.)
2. Which lake to dry on a day when there is no rain, can not be determined without knowing the rain sequence that is coming next.
For example - If you have rains = [1, 2, 0, ??]. Without knowing what the \'??\' is, you can not determine which lake to dry on the 3rd day (rains[2]), this is because if \'??\' = 1, then you must dry the lake #1 to avoid flooding. Similarly, if \'??\' =2,  then you must dry lake #2 to avoid flooding.

![image](https://assets.leetcode.com/users/images/e775a319-756a-4e1c-8a61-2ef448137900_1593072251.1421912.png)

Detailed Comments in the code.

Please upvote if it helps - 
1. It encourages me to write and share my solutions if they are easy to understand. 
2. It reaches to more learners, larger audience. :)

Thanks.

C++

```
class Solution {
public:
    vector<int> avoidFlood(vector<int>& rains) {
        vector<int> ans; // Store the final answer here.
        int n = rains.size(); 
        unordered_map<int, int> fulllakes; // Lake number -> day on which it became full.
        set<int> drydays;     // Set of available days that can be used for drying a full lake.
        for (int i=0; i<n; i++) {  // For each day - 
            if (rains[i] == 0) {  // No rain on this day.
                drydays.insert(i); // This day can be used as a day to dry some lake.
                                   // We don\'t know which lake to prioritize for drying yet.
                ans.push_back(1);  // Any number would be ok. This will get overwritten eventually.
                                   // If it doesn\'t get overwritten, its totally ok to dry a lake
                                   // irrespective of whether it is full or empty.
            } else { // Rained in rains[i]-th lake.
                int lake = rains[i]; 
                if (fulllakes.find(lake) != fulllakes.end()) { // If it is already full -
                    // We must dry this lake before it rains in this lake.
                    // So find a day in "drydays" to dry this lake. Obviously, that day must be
                    // a day that is after the day on which the lake was full.
                    // i.e. if the lake got full on 7th day, we must find a dry day that is 
                    // greater than 7.
                    auto it = drydays.lower_bound(fulllakes[lake]);
                    if (it == drydays.end()) { // If there is no available dry day to dry the lake,
                                               // flooding is inevitable.
                        return {}; // Sorry, couldn\'t stop flooding.
                    }
                    int dryday = *it; // Great, we found a day which we can use to dry this lake.
                    ans[dryday] = lake; // Overwrite the "1" and dry "lake"-th lake instead.
                    drydays.erase(dryday); // We dried "lake"-th lake on "dryday", and we can\'t use
                                           // the same day to dry any other lake, so remove the day
                                           // from the set of available drydays.
                }
                fulllakes[lake] = i; // Update that the "lake" became full on "i"-th day.
                ans.push_back(-1); // As the problem statement expects.
            }
        }
        return ans; // Congratualtions, you avoided flooding.
    }
};
```



</p>


### [Java] nlog(n) find zero that can be used to empty the fullfilled lake
- Author: dreamyjpl
- Creation Date: Sun Jun 21 2020 12:02:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 21 2020 12:26:15 GMT+0800 (Singapore Standard Time)

<p>
```java
class Solution {
    public int[] avoidFlood(int[] rains) {
        // the previous appeared idx of rains[i]
        Map<Integer, Integer> map = new HashMap<>();
        TreeSet<Integer> zeros = new TreeSet<>();
        int[] res = new int[rains.length];
        for (int i = 0; i < rains.length; i++) {
            if (rains[i] == 0) {
                zeros.add(i);
            } else {
                if (map.containsKey(rains[i])) {
                    // find the location of zero that can be used to empty rains[i]
                    Integer next = zeros.ceiling(map.get(rains[i]));
                    if (next == null) return new int[0];
                    res[next] = rains[i];
                    zeros.remove(next);
                }
                res[i] = -1;
				map.put(rains[i], i);
            }
        }
        for (int i : zeros) res[i] = 1;
        return res;
    }
}
```
</p>


### [python] greedy with a heap
- Author: _-owo-_
- Creation Date: Sun Jun 21 2020 12:02:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 21 2020 12:43:24 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def avoidFlood(self, rains: List[int]) -> List[int]:
        seen = set()
        closest = []
        locs = collections.defaultdict(collections.deque)
        for i, lake in enumerate(rains):
            locs[lake].append(i)
        ret = []
        for lake in rains:
            if lake in seen:
                return []
            if not lake:
                # get closest that\'s already seen
                if not closest:
                    # there\'s nothing filled that will be filled again later
                    ret.append(1) 
                    continue
                nxt = heapq.heappop(closest)
                ret.append(rains[nxt])
                seen.remove(rains[nxt])
            else:
                seen.add(lake)
                l = locs[lake]
                l.popleft()
                if l:
                    nxt = l[0]
                    heapq.heappush(closest, nxt)
                ret.append(-1)
        return ret
```

start by getting all the days that each river is rained on, in chronological order.

then, go through each day. if a lake is rained on during a day, we get the next day that that river is going to be rained on in the future. we put this day in our "closest" minheap so that the next time we have a dry day, we can just pop from the minheap and fill up the lake that will be re-rained on nearest in the future.

edit: solution without seen, thanks @yaroslav-repeta

```
class Solution:
    def avoidFlood(self, rains: List[int]) -> List[int]:
        closest = []
        locs = collections.defaultdict(collections.deque)
        for i, lake in enumerate(rains):
            locs[lake].append(i)
        ret = []
        for i, lake in enumerate(rains):
            if closest and closest[0] == i:
                return []
            if not lake:
                if not closest:
                    ret.append(1) 
                    continue
                nxt = heapq.heappop(closest)
                ret.append(rains[nxt])
            else:
                l = locs[lake]
                l.popleft()
                if l:
                    nxt = l[0]
                    heapq.heappush(closest, nxt)
                ret.append(-1)
        return ret
```
</p>


