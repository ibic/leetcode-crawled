---
title: "Find Bottom Left Tree Value"
weight: 484
#id: "find-bottom-left-tree-value"
---
## Description
<div class="description">
<p>
Given a binary tree, find the leftmost value in the last row of the tree. 
</p>

<p><b>Example 1:</b><br />
<pre>
Input:

    2
   / \
  1   3

Output:
1
</pre>
</p>

<p> <b> Example 2: </b><br>
<pre>
Input:

        1
       / \
      2   3
     /   / \
    4   5   6
       /
      7

Output:
7
</pre>
</p>

<p><b>Note:</b>
You may assume the tree (i.e., the given root node) is not <b>NULL</b>.
</p>
</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Atlassian - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Right-to-Left BFS (Python + Java)
- Author: StefanPochmann
- Creation Date: Sun Feb 12 2017 15:25:48 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 01:52:25 GMT+0800 (Singapore Standard Time)

<p>
Doing BFS right-to-left means we can simply return the **last** node's value and don't have to keep track of the first node in the current row or even care about rows at all. Inspired by @fallcreek's solution (not published) which uses two nested loops to go row by row but already had the right-to-left idea making it easier. I just took that further.

**Python:**

    def findLeftMostNode(self, root):
        queue = [root]
        for node in queue:
            queue += filter(None, (node.right, node.left))
        return node.val

**Java:**

    public int findLeftMostNode(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            root = queue.poll();
            if (root.right != null)
                queue.add(root.right);
            if (root.left != null)
                queue.add(root.left);
        }
        return root.val;
    }
</p>


### Simple Java Solution, beats 100.0%!
- Author: ckcz123
- Creation Date: Sun Feb 12 2017 12:59:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 01:15:47 GMT+0800 (Singapore Standard Time)

<p>
``` java
public class Solution {
    int ans=0, h=0;
    public int findBottomLeftValue(TreeNode root) {
        findBottomLeftValue(root, 1);
        return ans;
    }
    public void findBottomLeftValue(TreeNode root, int depth) {
        if (h<depth) {ans=root.val;h=depth;}
        if (root.left!=null) findBottomLeftValue(root.left, depth+1);
        if (root.right!=null) findBottomLeftValue(root.right, depth+1);
    }
}
```

No global variables, 6ms (faster):
```
public class Solution {
    public int findBottomLeftValue(TreeNode root) {
        return findBottomLeftValue(root, 1, new int[]{0,0});
    }
    public int findBottomLeftValue(TreeNode root, int depth, int[] res) {
        if (res[1]<depth) {res[0]=root.val;res[1]=depth;}
        if (root.left!=null) findBottomLeftValue(root.left, depth+1, res);
        if (root.right!=null) findBottomLeftValue(root.right, depth+1, res);
        return res[0];
    }
}
```
</p>


### Verbose Java Solution, Binary tree level order traversal
- Author: shawngao
- Creation Date: Sun Feb 12 2017 12:35:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 12 2017 12:35:59 GMT+0800 (Singapore Standard Time)

<p>
Typical way to do binary tree level order traversal. Only additional step is to remember the ```first``` element of each level.
```
public class Solution {
    public int findLeftMostNode(TreeNode root) {
        if (root == null) return 0;
        
        int result = 0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.poll();
                if (i == 0) result = node.val;
                if (node.left != null) queue.add(node.left);
                if (node.right != null) queue.add(node.right);
            }
        }
        
        return result;
    }
}
```
</p>


