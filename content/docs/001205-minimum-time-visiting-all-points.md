---
title: "Minimum Time Visiting All Points"
weight: 1205
#id: "minimum-time-visiting-all-points"
---
## Description
<div class="description">
<p>On a plane there are <code>n</code> points with integer coordinates <code>points[i] = [xi, yi]</code>. Your task is to find the minimum time in seconds to visit all points.</p>

<p>You can move according to the next rules:</p>

<ul>
	<li>In one second always you can either move vertically, horizontally by one unit or diagonally (it means to move one unit vertically and one unit horizontally in one second).</li>
	<li>You have to visit the points in the same order as they appear in the array.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/11/14/1626_example_1.PNG" style="width: 500px; height: 428px;" />
<pre>
<strong>Input:</strong> points = [[1,1],[3,4],[-1,0]]
<strong>Output:</strong> 7
<strong>Explanation: </strong>One optimal path is <strong>[1,1]</strong> -&gt; [2,2] -&gt; [3,3] -&gt; <strong>[3,4] </strong>-&gt; [2,3] -&gt; [1,2] -&gt; [0,1] -&gt; <strong>[-1,0]</strong>   
Time from [1,1] to [3,4] = 3 seconds 
Time from [3,4] to [-1,0] = 4 seconds
Total time = 7 seconds</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> points = [[3,2],[-2,2]]
<strong>Output:</strong> 5
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>points.length == n</code></li>
	<li><code>1 &lt;= n&nbsp;&lt;= 100</code></li>
	<li><code>points[i].length == 2</code></li>
	<li><code>-1000&nbsp;&lt;= points[i][0], points[i][1]&nbsp;&lt;= 1000</code></li>
</ul>

</div>

## Tags
- Array (array)
- Geometry (geometry)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Media.net - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] 6 liner and 1 liner w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Nov 24 2019 12:27:22 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 14 2020 21:06:47 GMT+0800 (Singapore Standard Time)

<p>
**Proof: the time cost to travel between 2 neighboring points equals the larger value between the absolute values of the difference of respective x and y coordinates of the 2 points.**

a) Consider 2 points `(x1, y1)` and `(x2, y2)`, let `dx = |x1 - x2|` and `dy = |y1 - y2|`; According to the constraints of the problem, each step at most moves `1` unit along `x` and/or `y` coordinate. Therefore, `min time >= max(dx, dy)`;
b) On the other hand, each step **can** move `1` unit along `x`/`y` coordinate to cover the distance `dx`/`dy`, whichever is greater; Therefore, `min time <= max(dx, dy)`;
Combine the above a) and b), we have `max(dx, dy) <= min time <= max(dx, dy)` to complete the proof.

**End of Proof**

----
**Q & A:**
Q: Can you use an example to show `max(dx, dy) <= min time <= max(dx, dy)`?
A: Let us use the example 2 in problem description: `points = [[3,2],[-2,2]]`.
dx = |3 - (-2)| = 5, dy = |2 - 2| = 0.
a) Since each step at most moves `1` unit along `x` and/or `y` coordinate, `min time >= max(dx, dy) = 5`; 
b) On the other hand, each step **can** move `1` unit along both `x` and `y` coordinate, `min time <= max(dx, dy) = 5`;
Combine a) and b), `min time = max(dx, dy) = 5`.
**End of Q & A**

----
1. Traverse the input array, for each pair of neighboring points, comparing the absolute difference in `x` and `y` coordinates, the larger value is the minimum time need to travel between them;
2. Sum these time.

**Java**
```java
    public int minTimeToVisitAllPoints(int[][] points) {
        int ans = 0;
        for (int i = 1; i < points.length; ++i) {
            int[] cur = points[i], prev = points[i - 1];
            ans += Math.max(Math.abs(cur[0] - prev[0]), Math.abs(cur[1] - prev[1]));
        }
        return ans;        
    }
```
**Python 3**
```python
    def minTimeToVisitAllPoints(self, points: List[List[int]]) -> int:
        ans = 0
        for i in range(1, len(points)):
            prev, cur = points[i - 1 : i + 1]
            ans += max(map(abs, (prev[0] - cur[0], prev[1] - cur[1])))
        return ans    
```
1 liner:
```python
    def minTimeToVisitAllPoints(self, points: List[List[int]]) -> int:
        return sum(max(abs(p[i][0] - p[i - 1][0]), abs(p[i][1] - p[i - 1][1])) for i in range(1, len(p)))
```
or better readability at the cost of O(n) space:
```python
    def minTimeToVisitAllPoints(self, points: List[List[int]]) -> int:
        return sum(max(abs(cur[0] - prev[0]), abs(cur[1] - prev[1])) for cur, prev in zip(points, points[1 :]))
```
or use `zip` once again to make it clearer - credit to **@infmount**:
```python
    def minTimeToVisitAllPoints(self, points: List[List[int]]) -> int:
        return sum(max(abs(c - p) for c, p in zip(cur, prev)) for cur, prev in zip(points, points[1 :]))
```

**Analysis:**

Time: O(n), space: O(1), where n = points.length.
</p>


### C++ Straightforward Approach Greedy
- Author: gagandeepahuja09
- Creation Date: Sun Nov 24 2019 12:10:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 24 2019 13:29:15 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**
*  In order to minimize steps, we should walk most points diagonally. If we cannot make a move then those points should be covered either horizontally or vertically.
*  Example : Lets say |x2 - x1| = 5 and |y2 - y1| = 3, then we would walk 3 diagonally and remaining two have to be walked horizontally.

```
class Solution {
public:
    int minTimeToVisitAllPoints(vector<vector<int>>& points) {
        int ans = 0;
        for(int i = 1; i < points.size(); i++) {
            ans += max(abs(points[i][1] - points[i - 1][1]), abs(points[i][0] - points[i - 1][0]));
        }
        return ans;
    }
};
```
</p>


### Python 3 Easy Peasy Lemon Squeezy
- Author: mcclay
- Creation Date: Sun Nov 24 2019 13:15:51 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 31 2019 11:11:05 GMT+0800 (Singapore Standard Time)

<p>
**The Secret to the Problem:**

*point1 -> point 2*
*(x1, y1) -> (x2, y2)*

##### Take the greater change from x1, x2 and y1 ,y2
*max( |x2-x1|, |y2-y1| )*

Add this to your running total 

Rinse & Repeat
WINNING!!!!

**WHY?**
Going diagonally lets you move twice 
Going horizontal or vertical lets you move once
Diagonal is twice as fast as horizontal or vertical (in this problem)

Prioritize diagonal movement until x1 == x2 or y1 == y2 
Then you move horizontal or vertical

**Example:**
*x1,y1 = [0,0] 
x2,y2 =[10,20]*

*[0,0] -> [10,10]*
move 10 diagonal

*[10,10] ->[10,20]*
move 10 vertically  

the change is always going to be the greatest difference in either x\'s or y\'s


```python
# Python 3
class Solution:
    def minTimeToVisitAllPoints(self, points: List[List[int]]) -> int:
		res = 0
        x1, y1 = points.pop()
        while points:
            x2, y2 = points.pop()
            res += max(abs(y2 - y1), abs(x2-x1))
            x1, y1 = x2, y2
        return res
```
</p>


