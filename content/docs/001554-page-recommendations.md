---
title: "Page Recommendations"
weight: 1554
#id: "page-recommendations"
---
## Description
<div class="description">
<p>Table: <code>Friendship</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| user1_id      | int     |
| user2_id      | int     |
+---------------+---------+
(user1_id, user2_id) is the primary key for this table.
Each row of this table indicates that there is a friendship relation between user1_id and user2_id.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Likes</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| user_id     | int     |
| page_id     | int     |
+-------------+---------+
(user_id, page_id) is the primary key for this table.
Each row of this table indicates that user_id likes page_id.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to recommend pages to the user with <code>user_id</code> = 1 using the pages that your friends liked.&nbsp;It should not recommend pages you already liked.</p>

<p>Return result table in any order without duplicates.</p>

<p>The query result format is in the following example:</p>

<pre>
Friendship table:
+----------+----------+
| user1_id | user2_id |
+----------+----------+
| 1        | 2        |
| 1        | 3        |
| 1        | 4        |
| 2        | 3        |
| 2        | 4        |
| 2        | 5        |
| 6        | 1        |
+----------+----------+
 
Likes table:
+---------+---------+
| user_id | page_id |
+---------+---------+
| 1       | 88      |
| 2       | 23      |
| 3       | 24      |
| 4       | 56      |
| 5       | 11      |
| 6       | 33      |
| 2       | 77      |
| 3       | 77      |
| 6       | 88      |
+---------+---------+

Result table:
+------------------+
| recommended_page |
+------------------+
| 23               |
| 24               |
| 56               |
| 33               |
| 77               |
+------------------+
User one is friend with users 2, 3, 4 and 6.
Suggested pages are 23 from user 2, 24 from user 3, 56 from user 3 and 33 from user 6.
Page 77 is suggested from both user 2 and user 3.
Page 88 is not suggested because user 1 already likes it.
</pre>
</div>

## Tags


## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### mysql solution
- Author: kunting
- Creation Date: Fri Nov 22 2019 22:50:12 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Nov 22 2019 22:50:12 GMT+0800 (Singapore Standard Time)

<p>
select distinct page_id as recommended_page
from
(select 
 case
 when user1_id=1 then user2_id
 when user2_id=1 then user1_id
 end as user_id
from Friendship) a
join Likes
on a.user_id=Likes.user_id
where page_id not in (select page_id from Likes where user_id=1)
</p>


### MySQL solution using subquery
- Author: Shirleyxxy
- Creation Date: Sat Nov 23 2019 05:57:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Nov 23 2019 05:57:02 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT DISTINCT page_id AS recommended_page
FROM Likes
WHERE user_id IN (
    SELECT user2_id AS friend_id FROM Friendship WHERE user1_id = 1
    UNION
    SELECT user1_id AS friend_id FROM Friendship WHERE user2_id = 1) AND
    page_id NOT IN (
      SELECT page_id FROM Likes WHERE user_id = 1
    )
```
</p>


### MySQL, No Join, No Union, Only Where Clause.
- Author: sophiesu0827
- Creation Date: Wed Dec 18 2019 04:56:12 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Dec 18 2019 05:05:35 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT DISTINCT page_id AS recommended_page FROM Likes
WHERE user_id IN 
(SELECT CASE WHEN user1_id=1 THEN user2_id WHEN user2_id=1 THEN user1_id ELSE \'\' END
FROM Friendship WHERE user1_id=1 or user2_id=1)
AND page_id NOT IN 
(SELECT DISTINCT page_id FROM Likes WHERE user_id=1);
```
</p>


