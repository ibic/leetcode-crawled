---
title: "Rotate List"
weight: 61
#id: "rotate-list"
---
## Description
<div class="description">
<p>Given a linked&nbsp;list, rotate the list to the right by <em>k</em> places, where <em>k</em> is non-negative.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 1-&gt;2-&gt;3-&gt;4-&gt;5-&gt;NULL, k = 2
<strong>Output:</strong> 4-&gt;5-&gt;1-&gt;2-&gt;3-&gt;NULL
<strong>Explanation:</strong>
rotate 1 steps to the right: 5-&gt;1-&gt;2-&gt;3-&gt;4-&gt;NULL
rotate 2 steps to the right: 4-&gt;5-&gt;1-&gt;2-&gt;3-&gt;NULL
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> 0-&gt;1-&gt;2-&gt;NULL, k = 4
<strong>Output:</strong> <code>2-&gt;0-&gt;1-&gt;NULL</code>
<strong>Explanation:</strong>
rotate 1 steps to the right: 2-&gt;0-&gt;1-&gt;NULL
rotate 2 steps to the right: 1-&gt;2-&gt;0-&gt;NULL
rotate 3 steps to the right:&nbsp;<code>0-&gt;1-&gt;2-&gt;NULL</code>
rotate 4 steps to the right:&nbsp;<code>2-&gt;0-&gt;1-&gt;NULL</code></pre>

</div>

## Tags
- Linked List (linked-list)
- Two Pointers (two-pointers)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- LinkedIn - 4 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: 

**Intuition**

The nodes in the list are already linked, 
and hence the rotation basically means 

- To close the linked list into the ring.

- To break the ring after the new tail and just in front of 
the new head.

![rotate](../Figures/61/rotate.png)

> Where is the new head?

In the position `n - k`, where `n` is a number of nodes
in the list. The new tail is just before, in the position
`n - k - 1`. 

> We were assuming that `k < n`. 
What about the case of `k >= n`?

`k` could be rewritten as a sum `k = (k // n) * n + k % n`,
where the first term doesn't result in any rotation. 
Hence one could simply replace `k` by `k % n` to always
have number of rotation places smaller than `n`.

**Algorithm**

The algorithm is quite straightforward :

* Find the old tail and connect it with the head 
`old_tail.next = head` to close the ring. Compute
the length of the list `n` at the same time.

* Find the new tail, which is `(n - k % n - 1)`th node from the `head`
and the new head, which is `(n - k % n)`th node.

* Break the ring `new_tail.next = None` and return `new_head`.

**Implementation**

!?!../Documents/61_LIS.json:1000,583!?!

<iframe src="https://leetcode.com/playground/knkfoosp/shared" frameBorder="0" width="100%" height="500" name="knkfoosp"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ where $$N$$ is a number
of elements in the list.
 
* Space complexity : $$\mathcal{O}(1)$$ since it's a
constant space solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My clean C++ code, quite standard (find tail and reconnect the list)
- Author: LeJas
- Creation Date: Fri May 22 2015 00:22:38 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 04:11:18 GMT+0800 (Singapore Standard Time)

<p>
There is no trick for this problem. Some people used slow/fast pointers to find the tail node but I don't see the benefit (in the sense that it doesn't reduce the pointer move op) to do so. So I just used one loop to find the length first.

    class Solution {
    public:
        ListNode* rotateRight(ListNode* head, int k) {
            if(!head) return head;
            
            int len=1; // number of nodes
            ListNode *newH, *tail;
            newH=tail=head;
            
            while(tail->next)  // get the number of nodes in the list
            {
                tail = tail->next;
                len++;
            }
            tail->next = head; // circle the link

            if(k %= len) 
            {
                for(auto i=0; i<len-k; i++) tail = tail->next; // the tail node is the (len-k)-th node (1st node is head)
            }
            newH = tail->next; 
            tail->next = NULL;
            return newH;
        }
    };
</p>


### Share my java solution with explanation
- Author: reeclapple
- Creation Date: Thu Aug 21 2014 04:28:47 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 00:16:53 GMT+0800 (Singapore Standard Time)

<p>
Since n may be  a large number compared to the length of list. So we need to know the length of linked list.After that, move the list after the (l-n%l )th node to the front to finish the rotation.

Ex: {1,2,3} k=2 Move the list after the 1st node to the front


Ex: {1,2,3} k=5, In this case Move the list after (3-5%3=1)st node to the front.

So  the code has three parts. 

1) Get the length

2) Move to the (l-n%l)th node

3)Do the rotation



    public ListNode rotateRight(ListNode head, int n) {
        if (head==null||head.next==null) return head;
        ListNode dummy=new ListNode(0);
        dummy.next=head;
        ListNode fast=dummy,slow=dummy;

        int i;
        for (i=0;fast.next!=null;i++)//Get the total length 
        	fast=fast.next;
        
        for (int j=i-n%i;j>0;j--) //Get the i-n%i th node
        	slow=slow.next;
        
        fast.next=dummy.next; //Do the rotation
        dummy.next=slow.next;
        slow.next=null;
        
        return dummy.next;
    }
</p>


### Anyone solve the problem without counting the length of List?
- Author: erichsueh
- Creation Date: Thu Jul 17 2014 10:35:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 22:14:21 GMT+0800 (Singapore Standard Time)

<p>
My solution has O(n) time complexity and O(1) memory.
The basic idea is to connect the list into a circle. First, count the length of list while going through the list to find the end of it. Connect the tail to head. The problem asked to rotate k nodes, however, now the tail is at the end of the list and its difficult to move backward, so move *(k - len)* nodes along the list instead. *"k = k % len"* saves the unnecessary moves because rotate a list with length = *len* by *len* times doesn't change the list at all.

    ListNode *rotateRight(ListNode *head, int k) {
            if (head == NULL || head->next == NULL || k == 0) return head;
            int len = 1;
            ListNode *tail = head;

            /* find the end of list */
            while (tail->next != NULL) {
                tail = tail->next;
                len++;
            }

            /* form a circle */
            tail->next = head;
            k = k % len;
            for (int i = 0; i < len - k; i++) {
                tail = tail->next;
            }
            head = tail->next;
            tail->next = NULL;
            return head;
        }
</p>


