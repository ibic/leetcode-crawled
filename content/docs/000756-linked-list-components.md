---
title: "Linked List Components"
weight: 756
#id: "linked-list-components"
---
## Description
<div class="description">
<p>We are given&nbsp;<code>head</code>,&nbsp;the head node of a linked list containing&nbsp;<strong>unique integer values</strong>.</p>

<p>We are also given the list&nbsp;<code>G</code>, a subset of the values in the linked list.</p>

<p>Return the number of connected components in <code>G</code>, where two values are connected if they appear consecutively in the linked list.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 
head: 0-&gt;1-&gt;2-&gt;3
G = [0, 1, 3]
<strong>Output:</strong> 2
<strong>Explanation:</strong> 
0 and 1 are connected, so [0, 1] and [3] are the two connected components.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> 
head: 0-&gt;1-&gt;2-&gt;3-&gt;4
G = [0, 3, 1, 4]
<strong>Output:</strong> 2
<strong>Explanation:</strong> 
0 and 1 are connected, 3 and 4 are connected, so [0, 1] and [3, 4] are the two connected components.
</pre>

<p><strong>Note: </strong></p>

<ul>
	<li>If&nbsp;<code>N</code>&nbsp;is the&nbsp;length of the linked list given by&nbsp;<code>head</code>,&nbsp;<code>1 &lt;= N &lt;= 10000</code>.</li>
	<li>The value of each node in the linked list will be in the range<code> [0, N - 1]</code>.</li>
	<li><code>1 &lt;= G.length &lt;= 10000</code>.</li>
	<li><code>G</code> is a subset of all values in the linked list.</li>
</ul>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Google - 4 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Grouping [Accepted]

**Intuition**

Instead of thinking about connected components in `G`, think about them in the linked list.  Connected components in `G` must occur consecutively in the linked list.

**Algorithm**

Scanning through the list, if `node.val` is in `G` and `node.next.val` isn't (including if `node.next` is `null`), then this must be the end of a connected component.

For example, if the list is `0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7`, and `G = [0, 2, 3, 5, 7]`, then when scanning through the list, we fulfill the above condition at `0, 3, 5, 7`, for a total answer of `4`.

<iframe src="https://leetcode.com/playground/xpHKsABv/shared" frameBorder="0" width="100%" height="361" name="xpHKsABv"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N + G\text{.length})$$, where $$N$$ is the length of the linked list with root node `head`.

* Space Complexity: $$O(G\text{.length})$$, to store `Gset`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Easy and Concise Solution with Explanation
- Author: lee215
- Creation Date: Sun Apr 15 2018 11:06:27 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 05:14:30 GMT+0800 (Singapore Standard Time)

<p>
Take second example in the description:
liked list: `0->1`->2->`3->4`
I highlighed the subset G in linked list with color red.
The problem is just to count how many red part there are.
One red part is one connected components.
To do this, we just need to count tails of red parts.

**C++:**
```
    int numComponents(ListNode* head, vector<int>& G) {
        unordered_set<int> setG (G.begin(), G.end());
        int res = 0;
        while (head != NULL) {
            if (setG.count(head->val) && (head->next == NULL || !setG.count(head->next->val))) res++;
            head = head->next;
        }
        return res;
    }
```
**Java:**
```
    public int numComponents(ListNode head, int[] G) {
        Set<Integer> setG = new HashSet<>();
        for (int i: G) setG.add(i);
        int res = 0;
        while (head != null) {
            if (setG.contains(head.val) && (head.next == null || !setG.contains(head.next.val))) res++;
            head = head.next;
        }
        return res;
    }
```
**Python:**
```
    def numComponents(self, head, G):
        setG = set(G)
        res = 0
        while head:
            if head.val in setG and (head.next == None or head.next.val not in setG):
                res += 1
            head = head.next
        return res
```


</p>


### Can someone explain the test case
- Author: abhishes
- Creation Date: Fri May 18 2018 02:41:10 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 15:10:50 GMT+0800 (Singapore Standard Time)

<p>
In this problem https://leetcode.com/problems/linked-list-components/description/

The explanation says

Input: 
head: 0->1->2->3
G = [0, 1, 3]
Output: 2
Explanation: 
0 and 1 are connected, so [0, 1] and [3] are the two connected components.

But how is 3 a connected component?? there is no link between 0 -> 3 or 1 -> 3. So the right answer is 1 because 3 is not connected to anything inside G.
</p>


### How is 3  a connected component in this example statement
- Author: kushalmahajan
- Creation Date: Tue Aug 07 2018 19:04:08 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 05:47:06 GMT+0800 (Singapore Standard Time)

<p>
head: 0->1->2->3
G = [0, 1, 3]
Output: 2
Explanation: 
0 and 1 are connected, so [0, 1] and [3] are the two connected components.


</p>


