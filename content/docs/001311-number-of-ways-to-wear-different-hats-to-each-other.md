---
title: "Number of Ways to Wear Different Hats to Each Other"
weight: 1311
#id: "number-of-ways-to-wear-different-hats-to-each-other"
---
## Description
<div class="description">
<p>There are&nbsp;<code>n</code> people&nbsp;and 40 types of hats labeled from 1 to 40.</p>

<p>Given a list of list of integers <code>hats</code>, where <code>hats[i]</code>&nbsp;is a list of all hats preferred&nbsp;by the <code data-stringify-type="code">i-th</code> person.</p>

<p>Return the number of ways that the n people wear different hats to each other.</p>

<p>Since the answer&nbsp;may be too large,&nbsp;return it modulo&nbsp;<code>10^9 + 7</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> hats = [[3,4],[4,5],[5]]
<strong>Output:</strong> 1
<strong>Explanation: </strong>There is only one way to choose hats given the conditions. 
First person choose hat 3, Second person choose hat 4 and last one hat 5.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> hats = [[3,5,1],[3,5]]
<strong>Output:</strong> 4
<strong>Explanation: </strong>There are 4 ways to choose hats
(3,5), (5,3), (1,3) and (1,5)
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> hats = [[1,2,3,4],[1,2,3,4],[1,2,3,4],[1,2,3,4]]
<strong>Output:</strong> 24
<strong>Explanation: </strong>Each person can choose hats labeled from 1 to 4.
Number of Permutations of (1,2,3,4) = 24.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> hats = [[1,2,3],[2,3,5,6],[1,3,7,9],[1,8,9],[2,5,7]]
<strong>Output:</strong> 111
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == hats.length</code></li>
	<li><code>1 &lt;= n &lt;= 10</code></li>
	<li><code>1 &lt;= hats[i].length &lt;= 40</code></li>
	<li><code>1 &lt;= hats[i][j] &lt;= 40</code></li>
	<li><code>hats[i]</code> contains a list of <strong>unique</strong> integers.</li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Bit Manipulation (bit-manipulation)

## Companies
- MindTickle - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Top down DP + Bitmask - Clean code
- Author: hiepit
- Creation Date: Sun May 03 2020 00:26:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 10 2020 00:36:45 GMT+0800 (Singapore Standard Time)

<p>
**Approach 1: Assign n people with different hats - Time Limit Exceeded**
The first idea come to our mind is that we will do straightforward like the problem said: `Return the number of ways that the n people wear different hats to each other`. We will assign **n people** with **different hats** and use dp to calculate number of ways.
```java
    public int numberWays(List<List<Integer>> hats) {
        int n = hats.size();
        Integer[][] dp = new Integer[n][1L << 41];
        return dfs(hats, n, 0, 0, dp);
    }
    // dfs(...i, assignedHats...) number of ways to assign different hats for `i` people keep in mind that `assignedHats` is
    //    the mask of list of hats that we already assigned
    int dfs(List<List<Integer>> hats, int n, int i, long assignedHats, Integer[][] dp) {
        if (i == n) return 1; // assigned different hats to n people
        if (dp[i][assignedHats] != null) return dp[i][assignedHats];
        int ans = 0;
        for (int hat : hats.get(i)) {
            if (((assignedHats >> hat) & 1) == 1) continue; // Skip if this `hat` was already assigned
            ans += dfs(hats, n, i + 1, assignedHats | (1L << hat), dp);
            ans %= 1_000_000_007;
        }
        return dp[i][assignedHats] = ans;
    }
```
**Complexity**
- Time: `O(n * 2^40 * 40)`
   Explain: There are total `i * assignedHats` - `10 * 2^40` states in `dfs(..i, assignedHats)` function, each state needs a loop up to `40` times (`for (int hat : hats.get(i))`) to calculate the result.
- Space: `O(n * 2^40)`

**Approach 2: Assign different hats to n people - Accepted**
The time complexity in Approach 1 is so big. Since `n <= 10` is less then number of different hats `<= 40`. We can **assign up to 40 different hats** to **n people** and use dp to calculate number of ways.
```java
    public int numberWays(List<List<Integer>> hats) {
        int n = hats.size();
        List<Integer>[] h2p = new List[41]; // h2p[i] indicates the list of people who can wear i_th hat
        for (int i = 1; i <= 40; i++) h2p[i] = new ArrayList<>();
        for (int i = 0; i < n; i++)
            for (int hat : hats.get(i))
                h2p[hat].add(i);
        Integer[][] dp = new Integer[41][1024];
        return dfs(h2p, (1 << n) - 1, 1, 0, dp);
    }
    // dfs(...hat, assignedPeople...) number of ways to assign up to `hat` different hats to n people keep in mind that `assignedPeople` is
    //     the mask of list of people were assigned hat
    int dfs(List<Integer>[] h2p, int allMask, int hat, int assignedPeople, Integer[][] dp) {
        if (assignedPeople == allMask) return 1; // Check if assigned different hats to all people
        if (hat > 40) return 0; // no more hats to process
        if (dp[hat][assignedPeople] != null) return dp[hat][assignedPeople];
        int ans = dfs(h2p, allMask, hat + 1, assignedPeople, dp); // Don\'t wear this hat
        for (int p : h2p[hat]) {
            if (((assignedPeople >> p) & 1) == 1) continue; // Skip if person `p` was assigned hat
            ans += dfs(h2p, allMask, hat + 1, assignedPeople | (1 << p), dp); // Wear this hat for p_th person
            ans %= 1_000_000_007;
        }
        return dp[hat][assignedPeople] = ans;
    }
```
**Complexity**
- Time: `O(40 * 2^n * n)`, where `n` is the number of people, `n <= 10`
Explain: There are total `hat*assignedPeople` = `40*2^n` states in `dfs(..hat, assignedPeople...)` function, each state needs a loop up to `n` times (`int p : h2p[hat]`) to calculate the result.
- Space: `O(40 * 2^n)`

Please help to **UPVOTE** if this post is useful for you. 
If you have any questions, feel free to comment below.
Happy coding!
</p>


### [C++] Bit-masks and Bottom-Up DP
- Author: orangezeit
- Creation Date: Sun May 03 2020 00:10:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 20:42:57 GMT+0800 (Singapore Standard Time)

<p>
Since we have less persons and more hats, it would be better to re-organize the information structure. Instead of recording the hat labels for each person, recording the person labels for each hat will benefit us later in the bit-masking.

```cpp
class Solution {
public:
    
    int numberWays(vector<vector<int>>& hats) {
		// n: number of persons, 10 at most
		// h: number of hats, 40 at most
		// Time Complexity: O(2^n * h * n)
		// Space Complexity: O(2^n)
		// from the complexity analysis, you can also see why re-organization is critical in passing OJ
		
        vector<vector<int>> persons(40);
        const int n(hats.size()), mod(1e9 + 7);
		
		// masks range from 00...0 to 11...1 (n-digit binary number)
		// i-th digit represents whether i-th person has already had a hat
		// Base case: no one has a hat at first
        vector<int> masks(1 << n);
        masks[0] = 1;
        
		// re-organize, hats -> persons
        for (int i = 0; i < n; ++i)
            for (const int& h: hats[i])
                persons[h - 1].emplace_back(i);
        
        for (int i = 0; i < 40; ++i)                    // hats
            for (int j = (1 << n) - 1; j >= 0; --j)     // masks
                for (const int& p: persons[i])          // persons
					// if the current state j implies that p-th person hasn\'t had a hat yet
					// we can give the i-th hat to that person, and update the state
                    if ((j & (1 << p)) == 0) {
                        masks[j | (1 << p)] += masks[j];
                        masks[j | (1 << p)] %= mod;
                    }
        // return the state that each person has a hat
        return masks[(1 << n) - 1];
    }
};
```
</p>


### Assign hats to people, don't assign people with hats
- Author: bethoven
- Creation Date: Sun May 03 2020 00:11:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 04:50:08 GMT+0800 (Singapore Standard Time)

<p>
I started with normal dp to assign people with hats, and got TLE all the time, even though I tried different ways to optimize. Then I realized the number of hats is much bigger than the number of people, so I switched to assign hats to people, and of course it worked.

The difference betwen the two is that when assigning people with hats, since hat number is much bigger, we won\'t hit many duplicate assignments, hence less pruning, baiscally we have 40 bit mask and only 10 bit will be set at most; when we assign hats to people, the mask is only 10 bit and at most 40 recursion to set the bit, it\'s much more efficient.
```
class Solution:
    def numberWays(self, hats: List[List[int]]) -> int:
        htop = [[] for i in range(41)] # htop: hat to people list
        for p, prefer_hats in enumerate(hats):
            for h in prefer_hats:
                htop[h].append(p)
        htop = list(filter(lambda h: h, htop)) # filter out hats no one wants
        
        num_hats, num_people = len(htop), len(hats)
        if num_hats < num_people:
            return 0
        
        MOD = 10**9+7
        @functools.lru_cache(None)
        def dp(i, mask):
            if bin(mask).count(\'1\') == num_people:
                return 1
            if i == num_hats:
                return 0
            res = dp(i+1, mask) # not using the current hat
            for p in htop[i]:
                if mask & (1<<p) == 0:
                    mask |= 1<<p
                    res += dp(i+1, mask)
                    mask ^= 1<<p
            return res%MOD
        return dp(0, 0)
```

</p>


