---
title: "Decrease Elements To Make Array Zigzag"
weight: 1120
#id: "decrease-elements-to-make-array-zigzag"
---
## Description
<div class="description">
<p>Given an array <code>nums</code> of integers, a <em>move</em>&nbsp;consists of choosing any element and <strong>decreasing it by 1</strong>.</p>

<p>An array <code>A</code> is a&nbsp;<em>zigzag array</em>&nbsp;if either:</p>

<ul>
	<li>Every even-indexed element is greater than adjacent elements, ie.&nbsp;<code>A[0] &gt; A[1] &lt; A[2] &gt; A[3] &lt; A[4] &gt; ...</code></li>
	<li>OR, every odd-indexed element is greater than adjacent elements, ie.&nbsp;<code>A[0] &lt; A[1] &gt; A[2] &lt; A[3] &gt; A[4] &lt; ...</code></li>
</ul>

<p>Return the minimum number of moves to transform the given array <code>nums</code> into a zigzag array.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3]
<strong>Output:</strong> 2
<strong>Explanation:</strong> We can decrease 2 to 0 or 3 to 1.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [9,6,1,6,2]
<strong>Output:</strong> 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 1000</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy and concise
- Author: lee215
- Creation Date: Sun Aug 04 2019 12:03:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 04 2019 12:37:56 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Two options, either make `A[even]` smaller or make `A[odd]` smaller.
Loop on the whole array `A`,
find the `min(A[i - 1],A[i + 1])`,
calculate that the moves need to make smaller than both side.
If it\'s negative, it means it\'s already smaller than both side, no moved needed.
Add the moves need to `res[i%i]`.
In the end return the smaller option.
<br>

## **Complexity**
Time `O(N)` for one pass
Space `O(2)` for two options
<br>

**Java:**
```java
    public int movesToMakeZigzag(int[] A) {
        int res[] = new int[2],  n = A.length, left, right;
        for (int i = 0; i < n; ++i) {
            left = i > 0 ? A[i - 1] : 1001;
            right = i + 1 < n ? A[i + 1] : 1001;
            res[i % 2] += Math.max(0, A[i] - Math.min(left, right) + 1);
        }
        return Math.min(res[0], res[1]);
    }
```

**C++:**
```cpp
    int movesToMakeZigzag(vector<int>& A) {
        int res[2] = {0, 0},  n = A.size(), left, right;
        for (int i = 0; i < n; ++i) {
            left = i > 0 ? A[i - 1] : 1001;
            right = i + 1 < n ? A[i + 1] : 1001;
            res[i % 2] += max(0, A[i] - min(left, right) + 1);
        }
        return min(res[0], res[1]);
    }
```

**Python:**
```python
    def movesToMakeZigzag(self, A):
        A = [float(\'inf\')] + A + [float(\'inf\')]
        res = [0, 0]
        for i in xrange(1, len(A) - 1):
            res[i % 2] += max(0, A[i] - min(A[i - 1], A[i + 1]) + 1)
        return min(res)
```

</p>


### Read my short proof if you feel confused.
- Author: jiah
- Creation Date: Sun Aug 04 2019 13:02:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 04 2019 15:32:46 GMT+0800 (Singapore Standard Time)

<p>
Statement:
The optimal solution makes 0 or more moves at `even` indexes only, or at `odd` indexes only.

Brief Proof:
Consider any solution that makes some moves at `even` positions and also some moves at `odd` positions:
1. If this solution makes number at `odd` positions larger than their neighbors. We can discard these moves at `odd` positions, and this new solution still ensures `odd`-indexed numbers larger than their neighbors.
2. If this solution makes number at `even` positions larger than their neighbors. We can discard these moves at `even` positions, and this new solution still ensures `even`-indexed numbers larger than neighbors.
3. From the above analysis, we can conclude that for any solution with both odd and even moves, there exists better solutions. In other words, optimal solution makes odd moves only or even moves only.




</p>


### I feel frustrated for every Saturday night :(
- Author: dionwang
- Creation Date: Sun Aug 04 2019 12:57:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 04 2019 12:57:19 GMT+0800 (Singapore Standard Time)

<p>
I feel frustrated for every Saturday night :(
</p>


