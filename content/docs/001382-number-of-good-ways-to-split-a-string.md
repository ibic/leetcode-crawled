---
title: "Number of Good Ways to Split a String"
weight: 1382
#id: "number-of-good-ways-to-split-a-string"
---
## Description
<div class="description">
<p>You are given a string <code>s</code>, a&nbsp;split is called <em>good</em>&nbsp;if you can split&nbsp;<code>s</code> into 2&nbsp;non-empty strings <code>p</code> and <code>q</code> where its concatenation is equal to <code>s</code> and the number of distinct letters in <code>p</code> and <code>q</code> are the same.</p>

<p>Return the number of <em>good</em> splits you can make in <code>s</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aacaba&quot;
<strong>Output:</strong> 2
<strong>Explanation:</strong> There are 5 ways to split <code>&quot;aacaba&quot;</code> and 2 of them are good. 
(&quot;a&quot;, &quot;acaba&quot;) Left string and right string contains 1 and 3 different letters respectively.
(&quot;aa&quot;, &quot;caba&quot;) Left string and right string contains 1 and 3 different letters respectively.
(&quot;aac&quot;, &quot;aba&quot;) Left string and right string contains 2 and 2 different letters respectively (good split).
(&quot;aaca&quot;, &quot;ba&quot;) Left string and right string contains 2 and 2 different letters respectively (good split).
(&quot;aacab&quot;, &quot;a&quot;) Left string and right string contains 3 and 1 different letters respectively.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abcd&quot;
<strong>Output:</strong> 1
<strong>Explanation: </strong>Split the string as follows (&quot;ab&quot;, &quot;cd&quot;).
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aaaaa&quot;
<strong>Output:</strong> 4
<strong>Explanation: </strong>All possible splits are good.</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;acbadbaada&quot;
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>s</code> contains only lowercase English letters.</li>
	<li><code>1 &lt;= s.length &lt;= 10^5</code></li>
</ul>
</div>

## Tags
- String (string)
- Bit Manipulation (bit-manipulation)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Some hints to help you solve this problem on your own
- Author: Just__a__Visitor
- Creation Date: Sun Jul 26 2020 00:03:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 01:19:18 GMT+0800 (Singapore Standard Time)

<p>
<details>
<summary>Time Complexity </summary>

`O(26*n)`
</details>

<details>
<summary>Hint 1</summary>

Define `pref[i]` as the number of unique characters in `str[0...i]`. Can you fill this in a single forward pass? (Use Maps).
</details>

<details>
<summary>Hint 2</summary>

Define `suff[i]` as the number of unique characters in `str[i....]`. Can you fill this in a single backward pass? (Use Maps).
</details>

<details>
<summary>Final Algorithm</summary>

1. Fill `pref` and `suff` array
2. Fix the ending point of the first string as `i` and check if `pref[i] == suff[i +1]`. If yes, you have one instance.
</details>

<details>
<summary>Code</summary>

```
class Solution {
public:
    int numSplits(string str) {
        int n = str.length();
        
        vector<int> pref(n), suff(n);
        
        map<char, int> freq;
        
        for(int i = 0; i < n; i++)
        {
            freq[str[i]]++;
            pref[i] = freq.size();
        }
        
        freq.clear();
        
        for(int i = n -1; i >= 0; i--)
        {
            freq[str[i]]++;
            suff[i] = freq.size();
            
        }
       
        int ans = 0;
        for(int i = 1; i < (n); i++)
        {
            if(pref[i - 1] == suff[i])
                ans++;
        }
        
        return ans;
        
    }
};
```
</details>
</p>


### C++/Java Sliding Window
- Author: votrubac
- Creation Date: Sun Jul 26 2020 00:02:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 02:05:41 GMT+0800 (Singapore Standard Time)

<p>
We first count each character (`r[ch]`), and number of distinct characters (`d_r`). These are initial numbers for our right substring (thus, indicated as `r`).  

As we move our split point from left to right, we "move" the current character to the left substring, and update count and distinct characters in left and right substrings.

If the number of distict characters is equal, we increment the result.

**C++**
```cpp
int numSplits(string s) {
    int l[26] = {}, r[26] = {}, d_l = 0, d_r = 0, res = 0;
    for (auto ch : s)
        d_r += ++r[ch - \'a\'] == 1;
    for (int i = 0; i < s.size(); ++i) {
        d_l += ++l[s[i] - \'a\'] == 1;
        d_r -= --r[s[i] - \'a\'] == 0;
        res += d_l == d_r;
    }
    return res;
}
```
**Java**
```java
public int numSplits(String str) {
    int l[] = new int[26], r[] = new int[26], d_l = 0, d_r = 0, res = 0;
    var s = str.toCharArray();
    for (char ch : s)
        d_r += ++r[ch - \'a\'] == 1 ? 1 : 0;
    for (int i = 0; i < s.length; ++i) {
        d_l += ++l[s[i] - \'a\'] == 1 ? 1 : 0;
        d_r -= --r[s[i] - \'a\'] == 0 ? 1 : 0;
        res += d_l == d_r ? 1 : 0;
    }
    return res;
}
```
</p>


### 2-Solution | Linear Time Constant Space Using Map | Detailed Steps
- Author: ngytlcdsalcp1
- Creation Date: Sun Jul 26 2020 00:24:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 09 2020 01:39:37 GMT+0800 (Singapore Standard Time)

<p>
**Detailed Video Explanation** https://www.youtube.com/watch?v=UVfpK3dNGKE

**With  Set & Map**

1. Idea is first create the frequency map of full string `map`.
2. Now iterate from index i from 1 to  n - 1 which is our possible split cuts in string.
3. Create a `Set<Character>` set for tracking the distinct characters in left part of string.
4. Now we need to update the frequency of that character from initial frequency map how?
5. Check if that character present in  map and check if frequency 1 then remove from map else reduce frequency by 1.
6. Check if set.size() == map.sizer()  count++;

`TC - O(n)`

```
class Solution {
    public int numSplits(String s) {
        Map<Character, Integer> map = new HashMap();
        for(int i = 0; i < s.length(); i++) 
            map.put(s.charAt(i), map.getOrDefault(s.charAt(i), 0) + 1);
        
        int count = 0;
        Set<Character> set = new HashSet();
        for(int i = 1; i < s.length(); i++) {
            char c = s.charAt(i - 1);
            set.add(c);
            if(map.containsKey(c)) 
                if(map.get(c) == 1) map.remove(c); else map.put(c, map.get(c) - 1);
            if(set.size() == map.size()) count++;
        }
        
        return count;
    }
}
```

********
**Prefix and Suffix Array**

```
class Solution {
    public int numSplits(String s) {
        int n = s.length();
        int[] prefix = new int[n];
        int[] suffix = new int[n];
        Set<Character> pset = new HashSet();
        Set<Character> sset = new HashSet();
        for(int i = 0; i < n; i++) {
            pset.add(s.charAt(i));
            sset.add(s.charAt(n - 1 - i));
            prefix[i] = pset.size();
            suffix[n - 1 - i] = sset.size();
        }
        
        int goodWays = 0;
        for(int i = 1; i < n; i++) {
            if(prefix[i - 1] == suffix[i])
                goodWays++;
        }
        
        return goodWays;
    }
}
```
</p>


