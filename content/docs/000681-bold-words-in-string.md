---
title: "Bold Words in String"
weight: 681
#id: "bold-words-in-string"
---
## Description
<div class="description">
<p>Given a set of keywords <code>words</code> and a string <code>S</code>, make all appearances of all keywords in <code>S</code> bold. Any letters between <code>&lt;b&gt;</code> and <code>&lt;/b&gt;</code> tags become bold.</p>

<p>The returned string should use the least number of tags possible, and of course the tags should form a valid combination.</p>

<p>For example, given that <code>words = [&quot;ab&quot;, &quot;bc&quot;]</code> and <code>S = &quot;aabcd&quot;</code>, we should return <code>&quot;a&lt;b&gt;abc&lt;/b&gt;d&quot;</code>. Note that returning <code>&quot;a&lt;b&gt;a&lt;b&gt;b&lt;/b&gt;c&lt;/b&gt;d&quot;</code> would use more tags, so it is incorrect.</p>

<p><b>Constraints:</b></p>

<ul>
	<li><code>words</code> has length in range <code>[0, 50]</code>.</li>
	<li><code>words[i]</code> has length in range <code>[1, 10]</code>.</li>
	<li><code>S</code> has length in range <code>[0, 500]</code>.</li>
	<li>All characters in <code>words[i]</code> and <code>S</code> are lowercase letters.</li>
</ul>

<p><strong>Note:</strong> This question is the same as 616:&nbsp;<a href="https://leetcode.com/problems/add-bold-tag-in-string/">https://leetcode.com/problems/add-bold-tag-in-string/</a></p>

</div>

## Tags
- String (string)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Brute Force [Accepted]

**Intuition**

Let's try to learn which letters end up bold, since the resulting answer will just be the canonical one - we put bold tags around each group of bold letters.

To do this, we'll check for all occurrences of each word and mark the corresponding letters bold.

**Algorithm**

Let's work on first setting `mask[i] = true` if and only if the `i`-th letter is bold.  For each starting position `i` in `S`, for each `word`, if `S[i]` starts with `word`, we'll set the appropriate letters bold.

Now armed with the correct `mask`, let's try to output the answer.  A letter in position `i` is the first bold letter of the group if `mask[i] && (i == 0 || !mask[i-1])`, and is the last bold letter if `mask[i] && (i == N-1 || !mask[i+1])`.  Alternatively, we could use `itertools.groupby` in Python.

Once we know which letters are the first and last bold letters of a group, we know where to put the `"<b>"` and `"</b>"` tags.

<iframe src="https://leetcode.com/playground/Eg4ivU8y/shared" frameBorder="0" width="100%" height="500" name="Eg4ivU8y"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N\sum w_i)$$, where $$N$$ is the length of `S` and $$w_i$$ is the sum of `W`.

* Space Complexity: $$O(N)$$.

---

Analysis written by: [@awice](https://leetcode.com/awice).

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java, Very Easy, 15 lines, 14ms
- Author: climberig
- Creation Date: Wed Feb 28 2018 09:40:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 10 2018 12:16:33 GMT+0800 (Singapore Standard Time)

<p>
```
public String boldWords(String[] words, String S) {
        boolean[] bold = new boolean[S.length() + 1];
        for (String w : words) {
            int start = S.indexOf(w, 0);
            while (start != -1) {
                Arrays.fill(bold, start, start + w.length(), true);
                start = S.indexOf(w, start + 1);
            }
        }
        StringBuilder r = new StringBuilder().append(bold[0] ? "<b>" : "");
        for (int i = 0; i < bold.length - 1; i++) {
            r.append(S.charAt(i));
            if (!bold[i] && bold[i + 1]) r.append("<b>");
            else if (bold[i] && !bold[i + 1]) r.append("</b>");
        }
        return r.toString();
    }
</p>


### Isn't this question exact the same as 616?
- Author: ccwei
- Creation Date: Mon Jan 08 2018 06:50:40 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 14 2018 15:07:22 GMT+0800 (Singapore Standard Time)

<p>
Look the same to me, am I missing something?
</p>


### Clean Java solution using only boolean array and StringBuilder
- Author: lily4ever
- Creation Date: Sun Jan 07 2018 12:15:23 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 01:32:28 GMT+0800 (Singapore Standard Time)

<p>
The main idea is to use a boolean array to mark the words at the corresponding positions in S. Then build the final string based on the marked positions.

```
    public String boldWords(String[] words, String S) {
        if (words == null || words.length == 0) return "";
        
        boolean[] marked = new boolean[S.length()];
        for (String word : words) {
            markWords(S, word, marked);
        }
        
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < S.length(); i++) {
            if (marked[i] && (i == 0 || !marked[i - 1])) {
                sb.append("<b>");
            }
            sb.append(S.charAt(i));
            if (marked[i] && (i == S.length() - 1 || !marked[i + 1])) {
                sb.append("</b>");
            }
        }
        return sb.toString();
    }
    
    void markWords(String S, String word, boolean[] marked) {
        for (int i = 0; i <= S.length() - word.length(); i++) {
            String substr = S.substring(i, i + word.length());
            if (substr.equals(word)) {
                for (int j = i; j < i + word.length(); j++) {
                    marked[j] = true;
                }
            }
        }
    }
```
</p>


