---
title: "Linked List in Binary Tree"
weight: 1272
#id: "linked-list-in-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree <code>root</code> and a&nbsp;linked list with&nbsp;<code>head</code>&nbsp;as the first node.&nbsp;</p>

<p>Return True if all the elements in the linked list starting from the <code>head</code> correspond to some <em>downward path</em> connected in the binary tree&nbsp;otherwise return False.</p>

<p>In this context downward path means a path that starts at some node and goes downwards.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/02/12/sample_1_1720.png" style="width: 220px; height: 280px;" /></strong></p>

<pre>
<strong>Input:</strong> head = [4,2,8], root = [1,4,4,null,2,2,null,1,null,6,8,null,null,null,null,1,3]
<strong>Output:</strong> true
<strong>Explanation:</strong> Nodes in blue form a subpath in the binary Tree.  
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/02/12/sample_2_1720.png" style="width: 220px; height: 280px;" /></strong></p>

<pre>
<strong>Input:</strong> head = [1,4,2,6], root = [1,4,4,null,2,2,null,1,null,6,8,null,null,null,null,1,3]
<strong>Output:</strong> true
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> head = [1,4,2,6,8], root = [1,4,4,null,2,2,null,1,null,6,8,null,null,null,null,1,3]
<strong>Output:</strong> false
<strong>Explanation:</strong> There is no path in the binary tree that contains all the elements of the linked list from <code>head</code>.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= node.val&nbsp;&lt;= 100</code>&nbsp;for each node in the linked list and binary tree.</li>
	<li>The given linked list will contain between&nbsp;<code>1</code>&nbsp;and <code>100</code>&nbsp;nodes.</li>
	<li>The given binary tree will contain between&nbsp;<code>1</code>&nbsp;and&nbsp;<code>2500</code>&nbsp;nodes.</li>
</ul>

</div>

## Tags
- Linked List (linked-list)
- Dynamic Programming (dynamic-programming)
- Tree (tree)

## Companies
- SoundHound - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Recursive Solution, O(N) Time
- Author: lee215
- Creation Date: Sun Mar 01 2020 12:05:16 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 27 2020 10:55:51 GMT+0800 (Singapore Standard Time)

<p>
# Solution 1: Brute DFS
Time `O(N * min(L,H))`
Space `O(H)`
where N = tree size, H = tree height, L = list length.
<br>

**Java**
```java
    public boolean isSubPath(ListNode head, TreeNode root) {
        if (head == null) return true;
        if (root == null) return false;
        return dfs(head, root) || isSubPath(head, root.left) || isSubPath(head, root.right);
    }

    private boolean dfs(ListNode head, TreeNode root) {
        if (head == null) return true;
        if (root == null) return false;
        return head.val == root.val && (dfs(head.next, root.left) || dfs(head.next, root.right));
    }
```
**C++**
```cpp
    bool isSubPath(ListNode* head, TreeNode* root) {
        if (!head) return true;
        if (!root) return false;
        return dfs(head, root) || isSubPath(head, root->left) || isSubPath(head, root->right);
    }

    bool dfs(ListNode* head, TreeNode* root) {
        if (!head) return true;
        if (!root) return false;
        return head->val == root->val && (dfs(head->next, root->left) || dfs(head->next, root->right));
    }
```
**Python:**
```py
    def isSubPath(self, head, root):
        def dfs(head, root):
            if not head: return True
            if not root: return False
            return root.val == head.val and (dfs(head.next, root.left) or dfs(head.next, root.right))
        if not head: return True
        if not root: return False
        return dfs(head, root) or self.isSubPath(head, root.left) or self.isSubPath(head, root.right)
```
<br>

# Solution 2: DP
1. Iterate the whole link, find the maximum matched length of prefix.
2. Iterate the whole tree, find the maximum matched length of prefix.

About this dp, @fukuzawa_yumi gave a link of reference:
https://en.wikipedia.org/wiki/Knuth\u2013Morris\u2013Pratt_algorithm

Time `O(N)`
Space `O(L + H)`
where N = tree size, H = tree height, L = list length.
<br>

**Java**
```java
    public boolean isSubPath(ListNode head, TreeNode root) {
        List<Integer> A = new ArrayList(), dp = new ArrayList();
        A.add(head.val);
        dp.add(0);
        int i = 0;
        head = head.next;
        while (head != null) {
            while (i > 0 && head.val != A.get(i))
                i = dp.get(i - 1);
            if (head.val == A.get(i)) ++i;
            A.add(head.val);
            dp.add(i);
            head = head.next;
        }
        return dfs(root, 0, A, dp);
    }

    private boolean dfs(TreeNode root, int i, List<Integer> A, List<Integer> dp) {
        if (root == null) return false;
        while (i > 0 && root.val != A.get(i))
            i = dp.get(i - 1);
        if (root.val == A.get(i)) ++i;
        return i == dp.size() || dfs(root.left, i, A, dp) || dfs(root.right, i, A, dp);
    }
```
**C++**
```cpp
    bool isSubPath(ListNode* head, TreeNode* root) {
        vector<int> A = {head->val}, dp = {0};
        int i = 0;
        head = head->next;
        while (head) {
            while (i && head->val != A[i])
                i = dp[i - 1];
            i += head->val == A[i];
            A.push_back(head->val);
            dp.push_back(i);
            head = head->next;
        }
        return dfs(root, 0, A, dp);
    }

    bool dfs(TreeNode* root, int i, vector<int>& A, vector<int>& dp) {
        if (!root) return false;
        while (i && root->val != A[i])
            i = dp[i - 1];
        i += root->val == A[i];
        return i == dp.size() || dfs(root->left, i, A, dp) || dfs(root->right, i, A, dp);
    }
```
**Python**
```py
class Solution(object):

    def isSubPath(self, head, root):
        A, dp = [head.val], [0]
        i = 0
        node = head.next
        while node:
            while i and node.val != A[i]:
                i = dp[i - 1]
            i += node.val == A[i]
            A.append(node.val)
            dp.append(i)
            node = node.next

        def dfs(root, i):
            if not root: return False
            while i and root.val != A[i]:
                i = dp[i - 1]
            i += root.val == A[i]
            return i == len(dp) or dfs(root.left, i) or dfs(root.right, i)
        return dfs(root, 0)
```
</p>


### [C++] Simple Recursion
- Author: orangezeit
- Creation Date: Sun Mar 01 2020 12:00:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 02 2020 01:37:44 GMT+0800 (Singapore Standard Time)

<p>
```cpp
class Solution {
public:
    bool findSubPath(ListNode* head, TreeNode* root) {
        if (!head) return true;
        if (!root) return false;
        return head->val == root->val && (findSubPath(head->next, root->left) || findSubPath(head->next, root->right));
    }
    
    bool isSubPath(ListNode* head, TreeNode* root) {
        if (!root) return false;
        return findSubPath(head, root) || isSubPath(head, root->left) || isSubPath(head, root->right);
    }
};
```
</p>


### Possible reason for failing 61st Test Case [Accepted]
- Author: ashishsirohi
- Creation Date: Mon Mar 02 2020 04:23:13 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 02 2020 04:25:03 GMT+0800 (Singapore Standard Time)

<p>
Try checking your code against:
```python
[1,2,1,2,3]
[1,null,2,null,1,2,null,1,null,null,2,null,3]
```

![image](https://assets.leetcode.com/users/ashishsirohi/image_1583094177.png)


And the reason being, the new matching path starts somwhere from already matching path. For above example,
`1->2->1->2` will match the linked list but matching 3 will fail as the next node in the tree is 1. So, I stopped and checked whether current node matches the head of my linked list (in this case, it does) and I started my next path matching from here and I reached the last node without completely exhausting my linked list and hence return `False` but the correct answer is `True`. The mistake I made was not checking already exhausted path again for the possible start for my next path matching in case of previous failure. Hope it makes sense.

Here is my code which was failing for 61st test case:
```python
class Solution:
    def isSubPath(self, head: ListNode, root: TreeNode) -> bool:
        
        def dfs(tnode, orig, lnode):
            if not lnode:
                return True
            
            if not tnode:
                return
            
            if tnode.val == lnode.val:
                lnode = lnode.next
            else:
                lnode = orig if tnode.val != orig.val else orig.next
                
            if dfs(tnode.left, orig, lnode):
                return True
            elif dfs(tnode.right, orig, lnode):
                return True
        
        return dfs(root, head, head)
```

And Here is my code for accepted solution:
```python
class Solution:
    def isSubPath(self, head: ListNode, root: TreeNode) -> bool:
        
        def dfs(tnode, orig, lnode):
            if not lnode: return True
            
            if not tnode: return
            
            if tnode.val == lnode.val:
                lnode = lnode.next
            elif tnode.val == orig.val:
                lnode = orig.next
            else:
                return False
                
            if dfs(tnode.left, orig, lnode):
                return True
            elif dfs(tnode.right, orig, lnode):
                return True
        
        if not head: return True
        if not root: return False
        
        return (dfs(root, head, head) or self.isSubPath(head, root.left)
                or self.isSubPath(head, root.right))
```

</p>


