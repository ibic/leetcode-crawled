---
title: "Next Permutation"
weight: 31
#id: "next-permutation"
---
## Description
<div class="description">
<p>Implement <strong>next permutation</strong>, which rearranges numbers into the lexicographically next greater permutation of numbers.</p>

<p>If such an arrangement is not possible, it must rearrange it as the lowest possible order (i.e., sorted in ascending order).</p>

<p>The replacement must be <strong><a href="http://en.wikipedia.org/wiki/In-place_algorithm" target="_blank">in place</a></strong> and use only constant&nbsp;extra memory.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> nums = [1,2,3]
<strong>Output:</strong> [1,3,2]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> nums = [3,2,1]
<strong>Output:</strong> [1,2,3]
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> nums = [1,1,5]
<strong>Output:</strong> [1,5,1]
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> nums = [1]
<strong>Output:</strong> [1]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 100</code></li>
	<li><code>0 &lt;= nums[i] &lt;= 100</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Facebook - 20 (taggedByAdmin: false)
- Google - 7 (taggedByAdmin: true)
- ByteDance - 4 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Adobe - 4 (taggedByAdmin: false)
- Bloomberg - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Flipkart - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Atlassian - 4 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Sumologic - 9 (taggedByAdmin: false)
- Quora - 3 (taggedByAdmin: false)
- JPMorgan - 3 (taggedByAdmin: false)
- Houzz - 2 (taggedByAdmin: false)
- Samsung - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary

We need to find the next lexicographic permutation of the given list of numbers than the number formed by the given array.

## Solution

---
#### Approach 1: Brute Force

**Algorithm**

In this approach, we find out every possible permutation of list formed by the elements of the given array and find out the permutation which is
just larger than the given one. But this one will be a very naive approach, since it requires us to find out every possible permutation
 which will take really long time and the implementation is complex.
 Thus, this approach is not acceptable at all. Hence, we move on directly to the correct approach.

**Complexity Analysis**

* Time complexity : $$O(n!)$$. Total possible permutations is $$n!$$.
* Space complexity : $$O(n)$$. Since an array will be used to store the permutations.
<br />
<br />
---
#### Approach 2: Single Pass Approach

**Algorithm**

First, we observe that for any given sequence that is in descending order, no next larger permutation is possible.
 For example, no next permutation is possible for the following array:
 ```
 [9, 5, 4, 3, 1]
 ```

We need to find the first pair of two successive numbers $$a[i]$$ and $$a[i-1]$$, from the right, which satisfy
 $$a[i] > a[i-1]$$. Now, no rearrangements to the right of $$a[i-1]$$ can create a larger permutation since that subarray consists of numbers in descending order.
 Thus, we need to rearrange the numbers to the right of $$a[i-1]$$ including itself.

Now, what kind of rearrangement will produce the next larger number? We want to create the permutation just larger than the current one. Therefore, we need to replace the number $$a[i-1]$$ with the number which is just larger than itself among the numbers lying to its right section, say $$a[j]$$.

![ Next Permutation ](https://leetcode.com/media/original_images/31_nums_graph.png)

We swap the numbers $$a[i-1]$$ and $$a[j]$$. We now have the correct number at index $$i-1$$. But still the current permutation isn't the permutation
    that we are looking for. We need the smallest permutation that can be formed by using the numbers only to the right of $$a[i-1]$$. Therefore, we need to place those
     numbers in ascending order to get their smallest permutation.

But, recall that while scanning the numbers from the right, we simply kept decrementing the index
      until we found the pair $$a[i]$$ and $$a[i-1]$$ where,  $$a[i] > a[i-1]$$. Thus, all numbers to the right of $$a[i-1]$$ were already sorted in descending order.
      Furthermore, swapping $$a[i-1]$$ and $$a[j]$$ didn't change that order.
      Therefore, we simply need to reverse the numbers following $$a[i-1]$$ to get the next smallest lexicographic permutation.

The following animation will make things clearer:

![Next Permutation](https://leetcode.com/media/original_images/31_Next_Permutation.gif)

<iframe src="https://leetcode.com/playground/tJPs3ERV/shared" frameBorder="0" width="100%" height="500" name="tJPs3ERV"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. In worst case, only two scans of the whole array are needed.

* Space complexity : $$O(1)$$. No extra space is used. In place replacements are done.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ from Wikipedia
- Author: jianchao-li
- Creation Date: Tue Jun 02 2015 01:01:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 11:43:55 GMT+0800 (Singapore Standard Time)

<p>
According to [Wikipedia](https://en.wikipedia.org/wiki/Permutation#Generation_in_lexicographic_order), a man named Narayana Pandita presented the following simple algorithm to solve this problem in the 14th century.

1. Find the largest index `k` such that `nums[k] < nums[k + 1]`. If no such index exists, just reverse `nums` and done.
2. Find the largest index `l > k` such that `nums[k] < nums[l]`.
3. Swap `nums[k]` and `nums[l]`.
4. Reverse the sub-array `nums[k + 1:]`.

```cpp
class Solution {
public:
    void nextPermutation(vector<int>& nums) {
    	int n = nums.size(), k, l;
    	for (k = n - 2; k >= 0; k--) {
            if (nums[k] < nums[k + 1]) {
                break;
            }
        }
    	if (k < 0) {
    	    reverse(nums.begin(), nums.end());
    	} else {
    	    for (l = n - 1; l > k; l--) {
                if (nums[l] > nums[k]) {
                    break;
                }
            } 
    	    swap(nums[k], nums[l]);
    	    reverse(nums.begin() + k + 1, nums.end());
        }
    }
}; 
```

The above algorithm can also handle duplicates and thus can be further used to solve [Permutations](https://leetcode.com/problems/permutations/) and [Permutations II](https://leetcode.com/problems/permutations-ii/).
</p>


### Share my O(n) time solution
- Author: yuyibestman
- Creation Date: Sat Aug 02 2014 08:00:53 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 14:39:58 GMT+0800 (Singapore Standard Time)

<p>
My idea is for an array:

 1. Start from its last element, traverse backward to find the first one with index i that satisfy  num[i-1] < num[i]. So, elements from num[i] to num[n-1] is reversely sorted. 
 2. To find the next permutation, we have to swap some numbers at different positions, to minimize the increased amount, we have to make the highest changed position as high as possible. Notice that index larger than or equal to i is not possible as num[i,n-1] is reversely sorted. So, we want to increase the number at index i-1, clearly, swap it with the smallest number between num[i,n-1] that is larger than num[i-1]. For example, original number is 121543321, we want to swap the '1' at position 2 with '2' at position 7. 
 3. The last step is to make the remaining higher position part as small as possible, we just have to reversely sort the num[i,n-1]

The following is my code:

        
    public void nextPermutation(int[] num) {
        int n=num.length;
        if(n<2)
            return;
        int index=n-1;        
        while(index>0){
            if(num[index-1]<num[index])
                break;
            index--;
        }
        if(index==0){
            reverseSort(num,0,n-1);
            return;
        }
        else{
            int val=num[index-1];
            int j=n-1;
            while(j>=index){
                if(num[j]>val)
                    break;
                j--;
            }
            swap(num,j,index-1);
            reverseSort(num,index,n-1);
            return;
        }
    }
    
    public void swap(int[] num, int i, int j){
        int temp=0;
        temp=num[i];
        num[i]=num[j];
        num[j]=temp;
    }
    
    public void reverseSort(int[] num, int start, int end){   
        if(start>end)
            return;
        for(int i=start;i<=(end+start)/2;i++)
            swap(num,i,start+end-i);
    }
</p>


### Easiest JAVA Solution
- Author: yavinci
- Creation Date: Sun Nov 22 2015 13:36:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 02:47:57 GMT+0800 (Singapore Standard Time)

<p>
Using a simple example, we can derive the following steps:

    public void nextPermutation(int[] A) {
        if(A == null || A.length <= 1) return;
        int i = A.length - 2;
        while(i >= 0 && A[i] >= A[i + 1]) i--; // Find 1st id i that breaks descending order
        if(i >= 0) {                           // If not entirely descending
            int j = A.length - 1;              // Start from the end
            while(A[j] <= A[i]) j--;           // Find rightmost first larger id j
            swap(A, i, j);                     // Switch i and j
        }
        reverse(A, i + 1, A.length - 1);       // Reverse the descending sequence
    }

    public void swap(int[] A, int i, int j) {
        int tmp = A[i];
        A[i] = A[j];
        A[j] = tmp;
    }

    public void reverse(int[] A, int i, int j) {
        while(i < j) swap(A, i++, j--);
    }
</p>


