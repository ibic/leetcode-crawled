---
title: "Subarray Sum Equals K"
weight: 525
#id: "subarray-sum-equals-k"
---
## Description
<div class="description">
<p>Given an array of integers and an integer <b>k</b>, you need to find the total number of continuous subarrays whose sum equals to <b>k</b>.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b>nums = [1,1,1], k = 2
<b>Output:</b> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The length of the array is in range [1, 20,000].</li>
	<li>The range of numbers in the array is [-1000, 1000] and the range of the integer <b>k</b> is [-1e7, 1e7].</li>
</ul>

</div>

## Tags
- Array (array)
- Hash Table (hash-table)

## Companies
- Facebook - 102 (taggedByAdmin: false)
- Google - 14 (taggedByAdmin: true)
- Amazon - 8 (taggedByAdmin: false)
- Goldman Sachs - 4 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Twilio - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Quora - 2 (taggedByAdmin: false)
- Oracle - 4 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force

**Algorithm**

The simplest method is to consider every possible subarray of the given $$nums$$ array, find the sum of the elements of each of those subarrays and check for the equality of the sum obtained with the given $$k$$. Whenver the sum equals $$k$$, we can increment the $$count$$ used to store the required result.

<iframe src="https://leetcode.com/playground/uzdLhWrz/shared" frameBorder="0" name="uzdLhWrz" width="100%" height="309"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^3)$$. Considering every possible subarray takes $$O(n^2)$$ time. For each of the subarray we calculate the sum taking $$O(n)$$ time in the worst case, taking a total of $$O(n^3)$$ time.

* Space complexity : $$O(1)$$. Constant space is used.

<br/>

---

#### Approach 2: Using Cumulative Sum

**Algorithm**

Instead of determining the sum of elements everytime for every new subarray considered, we can make use of a cumulative sum array , $$sum$$. Then, in order to calculate the sum of elements lying between two indices, we can subtract the cumulative sum corresponding to the two indices to obtain the sum directly, instead of iterating over the subarray to obtain the sum.

In this implementation, we make use of a cumulative sum array, $$sum$$, such that $$sum[i]$$ is used to store the cumulative sum of $$nums$$ array upto the element corresponding to the $$(i-1)^{th}$$ index. Thus, to determine the sum of elements for the subarray $$nums[i:j]$$, we can directly use $$sum[j+1] - sum[i]$$.

<iframe src="https://leetcode.com/playground/YnknRnC6/shared" frameBorder="0" name="YnknRnC6" width="100%" height="326"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Considering every possible subarray takes $$O(n^2)$$ time. Finding out the sum of any subarray takes $$O(1)$$ time after the initial processing of $$O(n)$$ for creating the cumulative sum array.

* Space complexity : $$O(n)$$. Cumulative sum array $$sum$$ of size $$n+1$$ is used.

<br/>

---

#### Approach 3: Without Space

**Algorithm**

Instead of considering all the $$start$$ and $$end$$ points and then finding the sum for each subarray corresponding to those points, we can directly find the sum on the go while considering different $$end$$ points. i.e. We can choose a particular $$start$$ point and while iterating over the $$end$$ points, we can add the element corresponding to the $$end$$ point to the sum formed till now. Whenver the $$sum$$ equals the required $$k$$ value, we can update the $$count$$ value. We do so while iterating over all the $$end$$ indices possible for every $$start$$ index. Whenver, we update the $$start$$ index, we need to reset the $$sum$$ value to 0.

<iframe src="https://leetcode.com/playground/MGuUEEUy/shared" frameBorder="0" name="MGuUEEUy" width="100%" height="292"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n^2)$$. We need to consider every subarray possible.

* Space complexity : $$O(1)$$. Constant space is used.

<br/>

---

#### Approach 4: Using Hashmap

**Algorithm**

The idea behind this approach is as follows: If the cumulative sum(repreesnted by $$sum[i]$$ for sum upto $$i^{th}$$ index) upto two indices is the same, the sum of the elements lying in between those indices is zero. Extending the same thought further, if the cumulative sum upto two indices, say $$i$$ and $$j$$ is at a difference of $$k$$ i.e. if $$sum[i] - sum[j] = k$$, the sum of elements lying between indices $$i$$ and $$j$$ is $$k$$.

Based on these thoughts, we make use of a hashmap $$map$$ which is used to store the cumulative sum upto all the indices possible along with the number of times the same sum occurs. We store the data in the form: $$(sum_i, no. of occurences of sum_i)$$. We traverse over the array $$nums$$ and keep on finding the cumulative sum. Every time we encounter a new sum, we make a new entry in the hashmap corresponding to that sum. If the same sum occurs again, we increment the count corresponding to that sum in the hashmap. Further, for every sum encountered, we also determine the number of times the sum $$sum-k$$ has occured already, since it will determine the number of times a subarray with sum $$k$$ has occured upto the current index. We increment the $$count$$ by the same amount. 

After the complete array has been traversed, the $$count$$ gives the required result.

The animation below depicts the process.

!?!../Documents/560_Subarray.json:1000,563!?!

<iframe src="https://leetcode.com/playground/S6xciAtN/shared" frameBorder="0" name="S6xciAtN" width="100%" height="292"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n)$$. The entire $$nums$$ array is traversed only once.

* Space complexity : $$O(n)$$. Hashmap $$map$$ can contain upto $$n$$ distinct entries in the worst case.

<br/>

## Accepted Submission (python3)
```python3
class Solution:
    def subarraySum(self, nums: List[int], k: int) -> int:
        r, sum = 0, 0
        map = {0: 1}
        for n in nums:
            sum += n
            comp = sum - k
            if comp in map:
                r += map[comp]
            if sum not in map:
                map[sum] = 0
            map[sum] += 1
        return r

```

## Top Discussions
### Java Solution, PreSum + HashMap
- Author: shawngao
- Creation Date: Sun Apr 30 2017 11:04:19 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 19:44:34 GMT+0800 (Singapore Standard Time)

<p>
Solution 1. Brute force. We just need two loops (i, j) and test if ```SUM[i, j]``` = k. Time complexity O(n^2), Space complexity O(1). I bet this solution will TLE.

Solution 2. From solution 1, we know the key to solve this problem is ```SUM[i, j]```. So if we know ```SUM[0, i - 1]``` and ```SUM[0, j]```, then we can easily get ```SUM[i, j]```. To achieve this, we just need to go through the array, calculate the current sum and save number of all seen ```PreSum``` to a HashMap. Time complexity O(n), Space complexity O(n).
```
public class Solution {
    public int subarraySum(int[] nums, int k) {
        int sum = 0, result = 0;
        Map<Integer, Integer> preSum = new HashMap<>();
        preSum.put(0, 1);
        
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            if (preSum.containsKey(sum - k)) {
                result += preSum.get(sum - k);
            }
            preSum.put(sum, preSum.getOrDefault(sum, 0) + 1);
        }
        
        return result;
    }
}
```
</p>


### General summary of what kind of problem can/ cannot solved by Two Pointers
- Author: a2232189
- Creation Date: Wed May 29 2019 10:38:29 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 30 2019 10:06:36 GMT+0800 (Singapore Standard Time)

<p>
You may come up with two pointers/ sliding window technique at the very begin thought, which is not right after you have tried. Here is the general summary to expalin why we can not apply two pointers to solve this problem.

Let go through the process of explanation by introducing what kind of problem can be solved with two pointers, such that if this problem doesn\'t match the criteria, it cannot be solved with two pointers.

A problem can be solved by two pointers when two pointers come into place to help us reduce the total cases we need to consider, such that the corresponding time complexity will reduce too. 

Let\'s  trying to solve  [longest-substring-without-repeating-characters](https://leetcode.com/problems/longest-substring-without-repeating-characters/) firstly, which is a problem that can be solved by two pointers. After that, I will generalize the characteristics of that problem, such that we can know what kind of problem can be solved by two pointers.


```
Given a string, find the length of the longest substring without repeating characters.

Example 1:

Input: "abcabcbb"
Output: 3 
Explanation: The answer is "abc", with the length of 3. 
```

The very begin thought to solve problem above will be applying a brute-force solution, in which we maintain two index, `i` and `j`, such that for every `i` we test whether any possible `j` that form a substring `s.substring(i,j)(both inclusive)` doesn\'t contains repeating characters. This cost `O(n^3)`.

Now we need to find a better solution to reduce time complexity. The brute-force solution is not efficient, because it tests some cases that doesn\'t need to consider.

1. case that are never valid: e.g., for` Input: "abcabcbb"`, and `i == 0, j ==3` such that we are testing substring`abca` right now. we found a repeating character `a` when seeing `s.charAt(j) ==\'a\'`, so we know that for a certain `i == 0`, the rightmost `j` will be `j == 2` , because when `j == 3` it contains repeating characters(`s.charAt(3) ==\'a\'`), and  when `j == 4` it contains repeating characters(`s.charAt(3) ==\'a\'`) and  when `j == 5` it contains repeating characters(`s.charAt(3) ==\'a\'`), ... and  when `j == n - 1` it still contains repeating characters(`s.charAt(3) ==\'a\'`).
After oberserving that, we no long need to test every j such that` j >= 3` when `i == 0`, so we can increase `i` by `1` . 

2.  case that are included in the result already:
Wait, after `i = i + 1`, what about `j`? Should `j` move left or move right or not move? Let go back to brute-force solution to get some intuition. In brute-force solution, whenever `i++`, the `j` should be `j = i + 1`. However if we apply `j = i + 1`, we still test some cases that should not being tested because they  must be valid and they are included in the result alreay. For isntance, in case `i == 0 and j == 3`, we do `i = i + 1` such that `i` becomes `1` now. we do not need to test case where `i == 1` and `j == 2` becuase we have tested case where `i == 0` and `j == 2`, and it have been included in the result already. `i == 1` and `j == 2`  is a sub case of  `i == 0` and `j == 2`, since result of  `i == 0` and `j == 2` have been put in result, we no longer need to test  `i == 1` and `j == 2`. Consider if I tell you that I have 3 cats, will you doubt that I have at least 2 cats?

So the result is we will make `i++` and `j` stay at the same index before next round. (though in next round `j++`)

Above is the specific problem that can be solved by two pinters. Now let\'s generalize the characteristics of the problems that can be solved by two pinters. The summary is simple: 
1. `If a wider scope of the sliding window is valid, the narrower scope of that wider scope is valid` mush hold.
2.  `If a narrower scope of the sliding window is invalid, the wider scope of that narrower  scope is invalid` mush hold.

With 2 rules above hold, we are able to optimize the brute-force solution to two pointers solution.

I just show you what kind of question can be solved by two pointers by using some very simple Induction Reasoning. Now let me show you why this problem cannot be solved by two pointers. Like I said, If this problem doesn\'t meet the creteria that two pointer technique, it cannot be solved with two pointers.

In this specific problem, rule 1 doesn\'t hold, because I can find a specific case such that it doesn\'t hold, e.g., if `K` is `3`,    `1,1,1` sum is 3, so `1,1,1,` is valid,  but `1,1` sum is 2 which means `1,1` is invalid, so rule 1 breaks.

Rule2 doesn\'t hold, either, because I can find a specific case such that it doesn\'t hold, e.g., if `K` is `2`,    `1,1,1` sum is 3, so `1,1,1,` is invalid,  but `1,1,1,-1` sum is 2 which means `1,1,1,-1` is valid, so rule 2 breaks.
</p>


### Python clear explanation with code and example
- Author: vijayzuzu
- Creation Date: Tue Jul 23 2019 06:29:42 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 23 2019 06:29:42 GMT+0800 (Singapore Standard Time)

<p>
Just wanted to share a clear explanation that helped me.

First of all, the basic idea behind this code is that, **whenever the sums has increased by a value of k, we\'ve found a subarray of sums=k.**
I\'ll also explain why we need to initialise a 0 in the hashmap.
Example: Let\'s say our elements are [1,2,1,3] and k = 3.
and our corresponding running sums = [1,3,4,7]
Now, **if you notice the running sums array, from 1->4, there is increase of k and from 4->7, there is an increase of k. So, we\'ve found 2 subarrays of sums=k.**

But, if you look at the original array, there are 3 subarrays of sums==k. Now, you\'ll understand why 0 comes in the picture.

In the above example, 4-1=k and 7-4=k. Hence, we concluded that there are 2 subarrays.
**However, if sums==k, it should\'ve been 3-0=k. But 0 is not present in the array. To account for this case, we include the 0.**
Now the modified sums array will look like [0,1,3,4,7]. Now, try to see for the increase of k.
1) 0->3
2) 1->4
3) 4->7
Hence, 3 sub arrays of sums=k

This clarified some confusions I had while doing this problem.
```
class Solution(object):
    def subarraySum(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        count = 0
        sums = 0
        d = dict()
        d[0] = 1
        
        for i in range(len(nums)):
            sums += nums[i]
            count += d.get(sums-k,0)
            d[sums] = d.get(sums,0) + 1
        
        return(count)
```
</p>


