---
title: "Best Time to Buy and Sell Stock III"
weight: 123
#id: "best-time-to-buy-and-sell-stock-iii"
---
## Description
<div class="description">
<p>Say you have an array for which the <em>i</em><sup>th</sup> element is the price of a given stock on day <em>i</em>.</p>

<p>Design an algorithm to find the maximum profit. You may complete at most <em>two</em> transactions.</p>

<p><strong>Note:&nbsp;</strong>You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> prices = [3,3,5,0,0,3,1,4]
<strong>Output:</strong> 6
<strong>Explanation:</strong> Buy on day 4 (price = 0) and sell on day 6 (price = 3), profit = 3-0 = 3.
Then buy on day 7 (price = 1) and sell on day 8 (price = 4), profit = 4-1 = 3.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> prices = [1,2,3,4,5]
<strong>Output:</strong> 4
<strong>Explanation:</strong> Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are engaging multiple transactions at the same time. You must sell before buying again.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> prices = [7,6,4,3,1]
<strong>Output:</strong> 0
<strong>Explanation:</strong> In this case, no transaction is done, i.e. max profit = 0.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> prices = [1]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;=&nbsp;prices.length &lt;= 10<sup>5</sup></code></li>
	<li><code>0 &lt;=&nbsp;prices[i] &lt;=&nbsp;10<sup>5</sup></code></li>
</ul>

</div>

## Tags
- Array (array)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 7 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Two Sigma - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Rubrik - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
### Overview
First of all, as one should know, this is one of the problems from the series of [Best Time to Buy and Sell Stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/) problem. One could start from the first problem in the series and progress one by one from easy to hard.

If there is ever a God of the stock market, who knows the price of stock at any moment, then the strategies to gain the maximum profits from the stock market is actually surprisingly intuitive, which also depends on the number of transactions that one can make.

>If one can only make one transaction (_i.e._ buy and sell once), then better to make this one _bet_ count. The best strategy would be to _buy_ at the **_lowest_** price and then _sell_ at the **_highest_** price. To put it simple, _buy low sell high._

![example of price sequence](../Figures/123/123_example.png)

Let us look at a concrete example as shown in the above graph, given a list of prices, the task becomes to find maximal _difference_ between a **_latter_** stock price and an **_earlier_** one, which would be the maximal profits that we could gain, if only one transaction is allowed.

In the above example, the best moment to buy the stock would be the timestamp `t1`, and the best moment to sell the stock would be the timestamp `t9`.

The above strategy is actually the solution to the first problem of the series, _i.e._ [Best Time to Buy and Sell Stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/).

>On the other hand, if one can make as many transactions as one would like, then in order to gain the maximal profits, one must _capture_ each **augmentation** and _avoid_ each **plunging** of stock price.

Specifically, given a list of prices, for any two adjacent time points with stock prices `p1` and `p2`, the above best strategies can be broke down into the following two cases:

- If later the price augments, _i.e._ `p2 > p1`, then a good trader should buy at `p1` and then sell at `p2`, seizing this moment to make profits.
<br/>

- If later the price stays the same or even plunges, _i.e._ `p2 <= p1`, then a good trader should just hold the money in the pocket, neither to buy nor sell any stock.

With the above strategies, as one can see, we would _perfectly_ capitalize at each right moment, meanwhile avoiding any loss. At the end, the accumulative profits that we gain over the time would reach the maximum.

![example of price sequence](../Figures/123/123_example.png)

With the same example above, we would buy at the moment of `t1` and sell it at the moment of `t2`. Similarly, we would also buy at the moment of `t2` and sell the moment of `t3`. As one might notice, the profits we gain from these two transactions are equivalent to the single transaction of buying at the moment of `t1` and selling at the moment of `t3`. 

The above strategy would be the solution for the second problem of series, _i.e._ [Best Time to Buy and Sell Stock II](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/) where there is no limit on the number of transactions.
<br/>
<br/>

---
#### Approach 1: Bidirectional Dynamic Programming

**Intuition**

The only difference between this problem and the previous two problems is that in this problem we are allowed to make **at most two** transactions.

Additionally, there is a constraint on the order of transactions stated in the problem description as follows:

>You may not engage in multiple transactions at the same time, (_i.e._ you must sell the stock before you buy again).

We could interpret this constraint as that there would be **no overlapping** in the sequence of transactions. 

![order of transaction actions](../Figures/123/123_division.png)

In other words, the two transactions that we should make would situate in two different subsequences of the stock prices, without any overlapping, which we illustrate in the above graph.

>That being said, we can solve the problem in a **divide-and-conquer** manner, where we divide the original sequence of prices into two subsequences and then we calculate the maximum profit that we could gain from making a single transaction in each subsequence.

The total profits would be the sum of profits from each subsequence. If we enumerate all possible divisions (or we could consider them as combinations of subsequences), we could find the maximum total profits among them, which is also the desired result of the problem.

![visual of partitions on sequence](../Figures/123/123_partition.png)

So we divide this problem into two subproblems, and each subproblem is actually of the same problem of [Best Time to Buy and Sell Stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/) as we discussed in the overview section.

**Algorithm**

A naive implementation of the above idea would be to divide the sequences into two and then enumerate each of the subsequences, though this is definitely not the most optimized solution.

For a sequence of length $$N$$, we would have $$N$$ possible divisions (including no division), each of the elements would be visited once in each division. As a result, the overall time complexity of this naive implementation would be $$\mathcal{O}(N^2)$$.

We could do better than the naive $$\mathcal{O}(N^2)$$ implementation. Regarding the algorithms of divide-and-conquer,
one common technique that we can apply in order to optimize the time complexity is called **dynamic programming** (DP) where we trade less repetitive calculation with some extra space.

In dynamic programming algorithms, normally we create an array of one or two dimensions to keep the intermediate optimal results. In this problem though, we would use two arrays, with one array keeping the results of sequence from left to right and the other array keeping the results of sequence from right to left. For the sake of name, we could call it **_bidirectional dynamic programming_**.

![visualization of dp arrays](../Figures/123/123_dp.png)

First, we denote a sequence of prices as `Prices[i]`, with index starting from `0` to `N-1`. Then we define two arrays, namely `left_profits[i]` and `right_profits[i]`.

- As suggested by the name, each element in the `left_profits[i]` array would hold the maximum profits that one can gain from doing one single transaction on the left subsequence of prices from the index zero to `i`, (_i.e._ `Prices[0], Prices[1], ... Prices[i]`).
For instance, for the subsequences of `[7, 1, 5]`, the corresponding `left_profits[2]` would be 4, which is to buy the price of 1 and sell it at the price of 5.
<br/>

- And each element in the `right_profits[i]` array would hold the maximum profits that one can gain from doing one single transaction on the right subsequence of the prices from the index `i` up to `N-1`, (_i.e._ `Prices[i], Prices[i+1], ... Prices[N-1]`).
For example, for the right subsequence of `[3, 6, 4]`, the corresponding `right_profits[3]` would be 3, which is to buy at the price of 3 and then sell it at the price of 6.

Now, if we divide the sequence of prices around the element at the index `i`, into two subsequences, with left subsequences as `Prices[0], Prices[1], ... Prices[i]` and the right subsequence as `Prices[i+1], ... Prices[N-1]`,
then the total maximum profits that we obtain from this division (denoted as `max_profits[i]`) can be expressed as follows:
$$
    \text{max\_profits[i]} = \text{left\_profits[i]} + \text{right\_profits[i+1]}
$$

>Then if we exhaust all possible divisions, _i.e._ we place the two transactions in all possible combinations of subsequences, we would then obtain the global maximum profits that we could gain from this sequence of stock prices, which can be expressed as follows:
$$
    \max_{i=[0, N)}{(\text{max\_profits[i]})}
$$

We demonstrate how the DP arrays are calculated in the following animation.

!?!../Documents/123_LIS.json:1000,418!?!

Following the above idea, Here are some sample implementations.

<iframe src="https://leetcode.com/playground/xWK3YtAf/shared" frameBorder="0" width="100%" height="500" name="xWK3YtAf"></iframe>

In the above implementations, we refined the code a bit to make it a bit more concise and hopefully more intuitive. Here are some tweaks that we applied.

- Rather than constructing the two DP arrays in two separate loops, we do the calculation in a single loop (two birds with one stone).
<br>

- We pad the `right_profits[i]` array with an additional zero, which indicates the maximum profits that we can gain from an empty right subsequence, so that we can compare the result of having only one transaction (_i.e._ `left_profits[N-1]`) with the profits gained from doing two transactions. 

By the way, one can try the above algorithm on another problem called [Sliding Window Maximum](https://leetcode.com/articles/sliding-window-maximum/).


**Complexity**

- Time Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the length of the input sequence, since we have two iterations of length $$N$$. 
<br/>

- Space Complexity: $$\mathcal{O}(N)$$ for the two arrays that we keep in the algorithm.
<br/>
<br/>

---
#### Approach 2: One-pass Simulation

**Intuition**

Just when we think that the space complexity of $$\mathcal{O}(N)$$ is the best we can get for this problem, many users in the Discussion forum proposed a more optimized solution that reduced the space complexity to $$O(1)$$, (just to name a few of them [@weijiac](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/discuss/39611/Is-it-Best-Solution-with-O(n)-O(1).), [@shetty4l](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/discuss/149383/Easy-DP-solution-using-state-machine-O(n)-time-complexity-O(1)-space-complexity)).
The idea is quite brilliant, and requires only a single iteration without the additional DP arrays.

The intuition is that we can consider the problem as a game, and we as agent could make at most two transactions in order to gain the maximum points (profits) from the game.

The two transactions be decomposed into 4 actions: _"buy of transaction #1"_, _"sell of transaction #1"_, _"buy of transaction #2"_ and _"sell of transaction #2"_.

>To solve the game, we simply run a simulation along the sequence of prices, at each time step, we calculate the potential outcomes for each of our actions. At the end of the simulation, the outcome of the final action _"sell of transaction #2"_ would be the desired output of the problem.

![game simulation](../Figures/123/123_game.png)

**Algorithm**

Overall, we run an iteration over the sequence of prices.

Over the iteration, we calculate 4 variables which correspond to the costs or the profits of each action respectively, as follows:

- `t1_cost`: the _minimal_ cost of buying the stock in transaction #1. The minimal cost to acquire a stock would be the minimal price value that we have seen so far at each step.
<br/>

- `t1_profit`: the _maximal_ profit of selling the stock in transaction #1. Actually, at the end of the iteration, this value would be the answer for the first problem in the series, _i.e._ [Best Time to Buy and Sell Stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/).
<br/>

- `t2_cost`: the _minimal_ cost of buying the stock in transaction #2, while taking into account the profit gained from the previous transaction #1. One can consider this as the cost of reinvestment. Similar with `t1_cost`, we try to find the lowest price so far, which in addition would be partially compensated by the profits gained from the first transaction.
<br>

- `t2_profit`: the _maximal_ profit of selling the stock in transaction #2. With the help of `t2_cost` as we prepared so far, we would find out the maximal profits with at most two transactions at each step.

<iframe src="https://leetcode.com/playground/LdcNxUY6/shared" frameBorder="0" width="100%" height="395" name="LdcNxUY6"></iframe>


**Complexity**

- Time Complexity: $$\mathcal{O}(N)$$, where $$N$$ is the length of the input sequence.
<br/>

- Space Complexity: $$\mathcal{O}(1)$$, only constant memory is required, which is invariant from the input sequence.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Detail explanation of DP solution
- Author: meng789987
- Creation Date: Sun Jun 03 2018 13:59:44 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 14:24:58 GMT+0800 (Singapore Standard Time)

<p>
It\'s not difficult to get the DP recursive formula:
```
dp[k, i] = max(dp[k, i-1], prices[i] - prices[j] + dp[k-1, j-1]), j=[0..i-1]
```
For k transactions, on i-th day, 
if we don\'t trade then the profit is same as previous day dp[k, i-1]; 
and if we bought the share on j-th day where j=[0..i-1], then sell the share on i-th day then the profit is prices[i] - prices[j] + dp[k-1, j-1] .
Actually j can be i as well. When j is i, the one more extra item prices[i] - prices[j] + dp[k-1, j] = dp[k-1, i] looks like we just lose one chance of transaction.

I see someone else use the formula dp[k, i] = max(dp[k, i-1], prices[i] - prices[j] + dp[k-1, j]), where the last one is dp[k-1, j] instead of dp[k-1, j-1]. It\'s not the direct sense, as if the share was bought on j-th day, then the total profit of previous transactions should be done on (j-1)th day. However, the result based on that formula is also correct, because if the share was sold on j-th day and then bought again, it is the same if we didn\'t trade on that day.

So the straigtforward implementation is:
```
        public int MaxProfitDp(int[] prices) {
            if (prices.Length == 0) return 0;
            var dp = new int[3, prices.Length];
            for (int k = 1; k <= 2; k++)  {
                for (int i = 1; i < prices.Length; i++) {
                    int min = prices[0];
                    for (int j = 1; j <= i; j++)
                        min = Math.Min(min, prices[j] - dp[k-1, j-1]);
                    dp[k, i] = Math.Max(dp[k, i-1], prices[i] - min);
                }
            }

            return dp[2, prices.Length - 1];
        }
```
Time complexity is O(kn^2), space complexity is O(kn).
In the above code, min is repeated calculated. It can be easily improved as:
```
        public int MaxProfitDpCompact1(int[] prices) {
            if (prices.Length == 0) return 0;
            var dp = new int[3, prices.Length];
            for (int k = 1; k <= 2; k++) {
                int min = prices[0];
                for (int i = 1; i < prices.Length; i++) {
                    min = Math.Min(min, prices[i] - dp[k-1, i-1]);
                    dp[k, i] = Math.Max(dp[k, i-1], prices[i] - min);
                }
            }

            return dp[2, prices.Length - 1];
        }
```
Time complexity is O(kn), space complexity is O(kn).
If we slight swap the two \'for\' loops:
```
        public int MaxProfitDpCompact1T(int[] prices) {
            if (prices.Length == 0) return 0;
            var dp = new int[3, prices.Length];
            var min = new int[3];
            Array.Fill(min, prices[0]);
            for (int i = 1; i < prices.Length; i++) {
                for (int k = 1; k <= 2; k++) {
                    min[k] = Math.Min(min[k], prices[i] - dp[k-1, i-1]);
                    dp[k, i] = Math.Max(dp[k, i-1], prices[i] - min[k]);
                }
            }

            return dp[2, prices.Length - 1];
        }
```				
We need to save min for each transaction, so there are k \'min\'.
We can find the second dimension (variable i) is only dependent on the previous one (i-1), so we can compact this dimension. (We can choose the first dimension (variable k) as well since it is also only dependent on its previous one k-1, but can\'t compact both.)
```
        public int MaxProfitDpCompact2(int[] prices) {
            if (prices.Length == 0) return 0;
            var dp = new int[3];
            var min = new int[3];
            Array.Fill(min, prices[0]);
            for (int i = 1; i < prices.Length; i++)  {
                for (int k = 1; k <= 2; k++) {
                    min[k] = Math.Min(min[k], prices[i] - dp[k-1]);
                    dp[k] = Math.Max(dp[k], prices[i] - min[k]);
                }
            }

            return dp[2];
        }
```
So time complexity is O(kn), space complexity becomes O(k).
In this case, K is 2. We can expand the array to all named variables:
```
        public int MaxProfitDpCompactFinal(int[] prices)  {
            int buy1 = int.MaxValue, buy2 = int.MaxValue;
            int sell1 = 0, sell2 = 0;

            for (int i = 0; i < prices.Length; i++) {
                buy1 = Math.Min(buy1, prices[i]);
                sell1 = Math.Max(sell1, prices[i] - buy1);
                buy2 = Math.Min(buy2, prices[i] - sell1);
                sell2 = Math.Max(sell2, prices[i] - buy2);
            }

            return sell2;
        }
```
We can also explain the above codes in other words. On every day, we buy the share with the price as low as we can, and sell the share with price as high as we can. For the second transaction, we integrate the profit of first transaction into the cost of the second buy, then the profit of the second sell will be the total profit of two transactions.

</p>


### Is it Best Solution with O(n), O(1).
- Author: weijiac
- Creation Date: Fri Dec 12 2014 11:45:56 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 21:53:46 GMT+0800 (Singapore Standard Time)

<p>
The thinking is simple and is inspired by the best solution from Single Number II (I read through the discussion after I use DP). 
Assume we only have 0 money at first;
4 Variables to maintain some interested 'ceilings' so far:
The maximum of if we've just buy 1st stock, if we've just sold 1nd stock, if we've just buy  2nd stock, if we've just sold 2nd stock.
Very simple code too and work well. I have to say the logic is simple than those in Single Number II.

    public class Solution {
        public int maxProfit(int[] prices) {
            int hold1 = Integer.MIN_VALUE, hold2 = Integer.MIN_VALUE;
            int release1 = 0, release2 = 0;
            for(int i:prices){                              // Assume we only have 0 money at first
                release2 = Math.max(release2, hold2+i);     // The maximum if we've just sold 2nd stock so far.
                hold2    = Math.max(hold2,    release1-i);  // The maximum if we've just buy  2nd stock so far.
                release1 = Math.max(release1, hold1+i);     // The maximum if we've just sold 1nd stock so far.
                hold1    = Math.max(hold1,    -i);          // The maximum if we've just buy  1st stock so far. 
            }
            return release2; ///Since release1 is initiated as 0, so release2 will always higher than release1.
        }
    }
</p>


### A clean DP solution which generalizes to k transactions
- Author: peterleetcode
- Creation Date: Wed Nov 05 2014 00:45:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 07:36:22 GMT+0800 (Singapore Standard Time)

<p>
Solution is commented in the code. Time complexity is O(k*n), space complexity can be O(n) because this DP only uses the result from last step. But for cleaness this solution still used O(k*n) space complexity to preserve similarity to the equations in the comments.

    class Solution {
    public:
        int maxProfit(vector<int> &prices) {
            // f[k, ii] represents the max profit up until prices[ii] (Note: NOT ending with prices[ii]) using at most k transactions. 
            // f[k, ii] = max(f[k, ii-1], prices[ii] - prices[jj] + f[k-1, jj]) { jj in range of [0, ii-1] }
            //          = max(f[k, ii-1], prices[ii] + max(f[k-1, jj] - prices[jj]))
            // f[0, ii] = 0; 0 times transation makes 0 profit
            // f[k, 0] = 0; if there is only one price data point you can't make any money no matter how many times you can trade
            if (prices.size() <= 1) return 0;
            else {
                int K = 2; // number of max transation allowed
                int maxProf = 0;
                vector<vector<int>> f(K+1, vector<int>(prices.size(), 0));
                for (int kk = 1; kk <= K; kk++) {
                    int tmpMax = f[kk-1][0] - prices[0];
                    for (int ii = 1; ii < prices.size(); ii++) {
                        f[kk][ii] = max(f[kk][ii-1], prices[ii] + tmpMax);
                        tmpMax = max(tmpMax, f[kk-1][ii] - prices[ii]);
                        maxProf = max(f[kk][ii], maxProf);
                    }
                }
                return maxProf;
            }
        }
    };
</p>


