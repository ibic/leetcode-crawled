---
title: "Find in Mountain Array"
weight: 1076
#id: "find-in-mountain-array"
---
## Description
<div class="description">
<p><em>(This problem is an&nbsp;<strong>interactive problem</strong>.)</em></p>

<p>You may recall that an array&nbsp;<code>A</code> is a <em>mountain array</em> if and only if:</p>

<ul>
	<li><code>A.length &gt;= 3</code></li>
	<li>There exists some&nbsp;<code>i</code>&nbsp;with&nbsp;<code>0 &lt; i&nbsp;&lt; A.length - 1</code>&nbsp;such that:
	<ul>
		<li><code>A[0] &lt; A[1] &lt; ... A[i-1] &lt; A[i]</code></li>
		<li><code>A[i] &gt; A[i+1] &gt; ... &gt; A[A.length - 1]</code></li>
	</ul>
	</li>
</ul>

<p>Given a mountain&nbsp;array <code>mountainArr</code>, return the <strong>minimum</strong>&nbsp;<code>index</code> such that <code>mountainArr.get(index) == target</code>.&nbsp; If such an <code>index</code>&nbsp;doesn&#39;t exist, return <code>-1</code>.</p>

<p><strong>You can&#39;t access the mountain array directly.</strong>&nbsp; You may only access the array using a&nbsp;<code>MountainArray</code>&nbsp;interface:</p>

<ul>
	<li><code>MountainArray.get(k)</code> returns the element of the array at index <code>k</code>&nbsp;(0-indexed).</li>
	<li><code>MountainArray.length()</code>&nbsp;returns the length of the array.</li>
</ul>

<p>Submissions making more than <code>100</code> calls to&nbsp;<code>MountainArray.get</code>&nbsp;will be judged <em>Wrong Answer</em>.&nbsp; Also, any solutions that attempt to circumvent the judge&nbsp;will result in disqualification.</p>

<ol>
</ol>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> array = [1,2,3,4,5,3,1], target = 3
<strong>Output:</strong> 2
<strong>Explanation:</strong> 3 exists in the array, at index=2 and index=5. Return the minimum index, which is 2.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> array = [0,1,2,4,2,1], target = 3
<strong>Output:</strong> -1
<strong>Explanation:</strong> 3 does not exist in <code>the array,</code> so we return -1.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>3 &lt;= mountain_arr.length() &lt;= 10000</code></li>
	<li><code>0 &lt;= target &lt;= 10^9</code></li>
	<li><code>0 &lt;= mountain_arr.get(index) &lt;=&nbsp;10^9</code></li>
</ul>

</div>

## Tags
- Binary Search (binary-search)

## Companies
- Apple - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Triple Binary Search
- Author: lee215
- Creation Date: Sun Jun 23 2019 12:07:10 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 02 2020 14:07:12 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Triple Binary Search, Triple Happiness. Visualizations from @lionkingeatapple
![image](https://assets.leetcode.com/users/images/8af035a7-04e3-401a-a8fc-75d4b527d4df_1599026759.8462024.png)

1. Binary find peak in the mountain.
[852. Peak Index in a Mountain Array](https://leetcode.com/problems/peak-index-in-a-mountain-array/discuss/139848/C%2B%2BJavaPython-Better-than-Binary-Search/294082)
2. Binary find the target in strict increasing array
3. Binary find the target in strict decreasing array
<br>

# Tip
Personally,
If I want find the index, I always use `while (left < right)`
If I may return the index during the search, I\'ll use `while (left <= right)`
<br>

# Complexity
Time `O(logN)` Space `O(1)`
<br>

# Some Improvement
1. Cache the result of `get`, in case we make the same calls.
In sacrifice of `O(logN)` space for the benefit of less calls.
2. Binary search of peak is unnecessary, just easy to write.
<br>

**C++/Java**
```
    int findInMountainArray(int target, MountainArray A) {
        int n = A.length(), l, r, m, peak = 0;
        // find index of peak
        l  = 0;
        r = n - 1;
        while (l < r) {
            m = (l + r) / 2;
            if (A.get(m) < A.get(m + 1))
                l = peak = m + 1;
            else
                r = m;
        }
        // find target in the left of peak
        l = 0;
        r = peak;
        while (l <= r) {
            m = (l + r) / 2;
            if (A.get(m) < target)
                l = m + 1;
            else if (A.get(m) > target)
                r = m - 1;
            else
                return m;
        }
        // find target in the right of peak
        l = peak;
        r = n - 1;
        while (l <= r) {
            m = (l + r) / 2;
            if (A.get(m) > target)
                l = m + 1;
            else if (A.get(m) < target)
                r = m - 1;
            else
                return m;
        }
        return -1;
    }
```

**Python:**
```
    def findInMountainArray(self, target, A):
        n = len(A)
        # find index of peak
        l, r = 0, n - 1
        while l < r:
            m = (l + r) / 2
            if A.get(m) < A.get(m + 1):
                l = peak = m + 1
            else:
                r = m
        # find target in the left of peak
        l, r = 0, peak
        while l <= r:
            m = (l + r) / 2
            if A.get(m) < target:
                l = m + 1
            elif A.get(m) > target:
                r = m - 1
            else:
                return m
        # find target in the right of peak
        l, r = peak, n - 1
        while l <= r:
            m = (l + r) / 2
            if A.get(m) > target:
                l = m + 1
            elif A.get(m) < target:
                r = m - 1
            else:
                return m
        return -1
```

</p>


### C++ Find Peak (#162) + Binary Search
- Author: votrubac
- Creation Date: Sun Jun 23 2019 12:07:02 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 27 2019 03:51:49 GMT+0800 (Singapore Standard Time)

<p>
First, find the peak as described in [162. Find Peak Element](https://leetcode.com/problems/find-peak-element/).

Then, do the binary search for the first range. If the element is not found, do the binary search again for the second range (note that the second range is descending).
```
int findInMountainArray(int t, MountainArray& arr) {
  auto peak = 0, r = arr.length() - 1;
  while (peak < r) {
    int m = (peak + r) / 2;
    if (arr.get(m) > arr.get(m + 1)) r = m;
    else peak = m + 1;
  }
  auto i = bSearch(arr, t, 0, peak);
  return i != -1 ? i : bSearch(arr, t, peak + 1, arr.length() - 1, false);
}
int bSearch(MountainArray& arr, int t, int l, int r, bool asc = true) {
  while (l <= r) {
    int m = (l + r) / 2;
    auto val = arr.get(m);
    if (val == t) return m;
    if (asc == val < t) l = m + 1;
    else r = m - 1;
  }
  return -1;
}
```
</p>


### Don't over-engineer a trinary search. Two binary search functions give the result.
- Author: kaiwensun
- Creation Date: Sun Jun 23 2019 12:36:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 23 2019 17:43:59 GMT+0800 (Singapore Standard Time)

<p>
I initially did a "trinary search" ([my another solution](https://leetcode.com/problems/find-in-mountain-array/discuss/317619/Python-trinary-%2B-binary-search)). Then I realized that the limit of queries is sufficient for quite a few times of binary searches.

Each binary search requies about `log(10000) / log(2) = 13` times of query.
With 100 as the upper limit, we can do at least `100 / 13 = 7` rounds of binary searches. That is way more than what we need.

First use binary search to find the peak.
Then use bianry search to find the target left to the peak; if not found, search the target right to the peak.

Python 2:

```
class Solution(object):
    def findInMountainArray(self, target, mountain_arr):
        cache = {}
        LEN = mountain_arr.length()

        def get(i):
			if i not in cache:
				cache[i] = mountain_arr.get(i)
			return cache[i]
        
        def getPeakIndex(start, end):
            while start < end:
                mid = (end - start) // 2 + start
                v = get(mid)
                if get(mid - 1) < v > get(mid + 1):
                    return mid
                if get(mid - 1) < v < get(mid + 1):
                    start = mid + 1
                else:
                    end = mid
        
        def binarySearch(start, end, is_inc):
            while start < end:
                mid = (end - start) // 2 + start
                v = get(mid)
                if v == target:
                    return mid
                if (v < target) == is_inc:
                    start = mid + 1
                else:
                    end = mid
            return -1
        peak_ind = getPeakIndex(0, LEN)
        if get(peak_ind) == target:
            return peak_ind
        res = binarySearch(0, peak_ind, is_inc=True)
        if res != -1:
            return res
        return binarySearch(peak_ind + 1, LEN, is_inc=False)
```

Python 3 with `@functools.lru_cache`:
```
import functools
class Solution:
    def findInMountainArray(self, target: int, mountain_arr: \'MountainArray\') -> int:
        LEN = mountain_arr.length()
        @functools.lru_cache(LEN)
        def get(i):
            return mountain_arr.get(i)
        
        def getPeakIndex(start, end):
            while start < end:
                mid = (end - start) // 2 + start
                v = get(mid)
                if get(mid - 1) < v > get(mid + 1):
                    return mid
                if get(mid - 1) < v < get(mid + 1):
                    start = mid + 1
                else:
                    end = mid
        
        def binarySearch(start, end, is_inc):
            while start < end:
                mid = (end - start) // 2 + start
                v = get(mid)
                if v == target:
                    return mid
                if (v < target) == is_inc:
                    start = mid + 1
                else:
                    end = mid
            return -1
        peak_ind = getPeakIndex(0, LEN)
        if get(peak_ind) == target:
            return peak_ind
        res = binarySearch(0, peak_ind, is_inc=True)
        if res != -1:
            return res
        return binarySearch(peak_ind + 1, LEN, is_inc=False)
```
</p>


