---
title: "Integer to English Words"
weight: 256
#id: "integer-to-english-words"
---
## Description
<div class="description">
<p>Convert a non-negative integer to its english words representation. Given input is guaranteed to be less than 2<sup>31</sup> - 1.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> 123
<b>Output:</b> &quot;One Hundred Twenty Three&quot;
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> 12345
<b>Output:</b> &quot;Twelve Thousand Three Hundred Forty Five&quot;</pre>

<p><b>Example 3:</b></p>

<pre>
<b>Input:</b> 1234567
<b>Output:</b> &quot;One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven&quot;
</pre>

<p><b>Example 4:</b></p>

<pre>
<b>Input:</b> 1234567891
<b>Output:</b> &quot;One Billion Two Hundred Thirty Four Million Five Hundred Sixty Seven Thousand Eight Hundred Ninety One&quot;
</pre>

</div>

## Tags
- Math (math)
- String (string)

## Companies
- Facebook - 49 (taggedByAdmin: true)
- Amazon - 19 (taggedByAdmin: false)
- Microsoft - 9 (taggedByAdmin: true)
- Apple - 5 (taggedByAdmin: false)
- Square - 4 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Netflix - 2 (taggedByAdmin: false)
- Nutanix - 4 (taggedByAdmin: false)
- Walmart Labs - 3 (taggedByAdmin: false)
- Palantir Technologies - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Zillow - 2 (taggedByAdmin: false)
- Tesla - 2 (taggedByAdmin: false)
- LinkedIn - 11 (taggedByAdmin: false)
- Splunk - 3 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Capital One - 2 (taggedByAdmin: false)
- Tableau - 2 (taggedByAdmin: false)
- JPMorgan - 2 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Citrix - 2 (taggedByAdmin: false)
- Barclays - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Divide and conquer 

Let's simplify the problem by representing it as a set of simple sub-problems.
One could split the initial integer `1234567890` on the groups 
containing not more than three digits `1.234.567.890`.
That results in representation `1 Billion 234 Million 567 Thousand 890` and 
reduces the initial problem to how to convert 3-digit integer to English word. 
One could split further `234` -> `2 Hundred 34` into two sub-problems :
convert 1-digit integer and convert 2-digit integer. The first one is trivial. 
The second one could be reduced to the first one for all 2-digit integers 
but the ones from `10` to `19` which should be considered separately.

<!--![LIS](../Figures/273/273_tr.gif)-->
!?!../Documents/273_LIS.json:1000,632!?!

<iframe src="https://leetcode.com/playground/hme62cME/shared" frameBorder="0" width="100%" height="500" name="hme62cME"></iframe>

**Complexity Analysis**

* Time complexity :  $$\mathcal{O}(N)$$. 
Intuitively the output is proportional
to the number `N` of digits in the input. 
* Space complexity : $$\mathcal{O}(1)$$ since the output is just a string.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My clean Java solution, very easy to understand
- Author: hwy_2015
- Creation Date: Tue Sep 01 2015 05:02:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 22:28:31 GMT+0800 (Singapore Standard Time)

<p>
    private final String[] LESS_THAN_20 = {"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
	private final String[] TENS = {"", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
	private final String[] THOUSANDS = {"", "Thousand", "Million", "Billion"};
	
    public String numberToWords(int num) {
        if (num == 0) return "Zero";

        int i = 0;
        String words = "";
        
        while (num > 0) {
            if (num % 1000 != 0)
        	    words = helper(num % 1000) +THOUSANDS[i] + " " + words;
        	num /= 1000;
        	i++;
        }
        
        return words.trim();
    }
    
    private String helper(int num) {
        if (num == 0)
            return "";
        else if (num < 20)
            return LESS_THAN_20[num] + " ";
        else if (num < 100)
            return TENS[num / 10] + " " + helper(num % 10);
        else
            return LESS_THAN_20[num / 100] + " Hundred " + helper(num % 100);
    }
</p>


### Short clean Java solution
- Author: iurisrb
- Creation Date: Wed Nov 25 2015 21:22:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 22:47:53 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        private final String[] belowTen = new String[] {"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
        private final String[] belowTwenty = new String[] {"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
        private final String[] belowHundred = new String[] {"", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
        
        public String numberToWords(int num) {
            if (num == 0) return "Zero";
            return helper(num); 
        }
        
        private String helper(int num) {
            String result = new String();
            if (num < 10) result = belowTen[num];
            else if (num < 20) result = belowTwenty[num -10];
            else if (num < 100) result = belowHundred[num/10] + " " + helper(num % 10);
            else if (num < 1000) result = helper(num/100) + " Hundred " +  helper(num % 100);
            else if (num < 1000000) result = helper(num/1000) + " Thousand " +  helper(num % 1000);
            else if (num < 1000000000) result = helper(num/1000000) + " Million " +  helper(num % 1000000);
            else result = helper(num/1000000000) + " Billion " + helper(num % 1000000000);
            return result.trim();
        }
    }
</p>


### Recursive Python
- Author: StefanPochmann
- Creation Date: Tue Sep 01 2015 06:50:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 00:41:40 GMT+0800 (Singapore Standard Time)

<p>
    def numberToWords(self, num):
        to19 = 'One Two Three Four Five Six Seven Eight Nine Ten Eleven Twelve ' \
               'Thirteen Fourteen Fifteen Sixteen Seventeen Eighteen Nineteen'.split()
        tens = 'Twenty Thirty Forty Fifty Sixty Seventy Eighty Ninety'.split()
        def words(n):
            if n < 20:
                return to19[n-1:n]
            if n < 100:
                return [tens[n/10-2]] + words(n%10)
            if n < 1000:
                return [to19[n/100-1]] + ['Hundred'] + words(n%100)
            for p, w in enumerate(('Thousand', 'Million', 'Billion'), 1):
                if n < 1000**(p+1):
                    return words(n/1000**p) + [w] + words(n%1000**p)
        return ' '.join(words(num)) or 'Zero'
</p>


