---
title: "Convert to Base -2"
weight: 979
#id: "convert-to-base-2"
---
## Description
<div class="description">
<p>Given a number <code>N</code>, return a string consisting of <code>&quot;0&quot;</code>s and <code>&quot;1&quot;</code>s&nbsp;that represents its value in base <code><strong>-2</strong></code>&nbsp;(negative two).</p>

<p>The returned string must have no leading zeroes, unless the string is <code>&quot;0&quot;</code>.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">2</span>
<strong>Output: </strong><span id="example-output-1">&quot;110&quot;
<strong>Explantion:</strong> (-2) ^ 2 + (-2) ^ 1 = 2</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">3</span>
<strong>Output: </strong><span id="example-output-2">&quot;111&quot;
</span><span id="example-output-1"><strong>Explantion:</strong> (-2) ^ 2 + (-2) ^ 1 + (-2) ^ 0</span><span> = 3</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">4</span>
<strong>Output: </strong><span id="example-output-3">&quot;100&quot;
</span><span id="example-output-1"><strong>Explantion:</strong> (-2) ^ 2 = 4</span>
</pre>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ol>
	<li><span><code>0 &lt;= N &lt;= 10^9</code></span></li>
</ol>
</div>
</div>
</div>
</div>

## Tags
- Math (math)

## Companies
- Grab - 3 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] 2 lines, Exactly Same as Base 2
- Author: lee215
- Creation Date: Sun Mar 31 2019 12:02:26 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 30 2020 15:05:58 GMT+0800 (Singapore Standard Time)

<p>
Please follow more detailed explanation in my channel on youtube.

# **Intuition**
1. Maybe write a base2 function first?
2. How about add minus `-`?
3. Done.
<br>

# **Explanation**
base2 function is quite basis of basis.
1. check last digit by `N%2` or `N&1`.
If it\'s 1, you get 1.
If it\'s 0, you get 0.

2. shift to right `N >> 1`.
This actually do two things:
2.1 Minus 1 if last digit is 1.
2.2 Divide `N` by 2.

base -2 is no difference,
except that we need divide `N` by -2.

So we do the same thing,
just add a sign `-` after division.

The explanation seems not enough.
But if you **really understand** how we do the base2,
you will find just literally the same process.
<br>

# More about `N = -(N >> 1)` 
Explained by @yren2:
It\'s mostly implementation defined. and most compilers choose to preserve the sign by setting the most significant bits after the shift.
for example, -3 in 8-bit is 11111101 and -3>>1 is 11111110, which is -2. (round towards -inf)

this is **different** from -3/2, which is -1. (round towards 0)
same goes -3>>2 == -1 and -3/4 == 0.
<br>

# **Complexity**
Time `O(logN)`
Space `O(logN)`

Note that I didn\'t deal with string concatenation,
and just take this part as `O(1)`.

Instead of create a new string each time,
we can improve this process using some operations join/reverse or data structure list/vector .
Like Java we may need `StringBuilder`,
but those are not what I want to discuss deeply here.
<br>


# **Solution 1: Iterative Solution**
**Java**
```java
    public String base2(int N) {
        StringBuilder res = new StringBuilder();
        while (N != 0) {
            res.append(N & 1);
            N = N >> 1;
        }
        return res.length() > 0 ? res.reverse().toString() : "0";
    }

    public String baseNeg2(int N) {
        StringBuilder res = new StringBuilder();
        while (N != 0) {
            res.append(N & 1);
            N = -(N >> 1);
        }
        return res.length() > 0 ? res.reverse().toString() : "0";
    }
```
**C++**
```cpp
    string base2(int N) {
        string res = "";
        while (N) {
            res = to_string(N & 1) + res;
            N = N >> 1;
        }
        return res == ""  ? "0" : res;
    }

    string baseNeg2(int N) {
        string res = "";
        while (N) {
            res = to_string(N & 1) + res;
            N = -(N >> 1);
        }
        return res == "" ? "0" : res;
    }
```
**Python**
```python
    def base2(self, x):
        res = []
        while x:
            res.append(x & 1)
            x = x >> 1
        return "".join(map(str, res[::-1] or [0]))

    def baseNeg2(self, x):
        res = []
        while x:
            res.append(x & 1)
            x = -(x >> 1)
        return "".join(map(str, res[::-1] or [0]))
```

<br>

# **Solution 2: Recursive Solution**
**Java:**
```java
    public String base2(int N) {
        if (N == 0 || N == 1) return Integer.toString(N);
        return base2(N >> 1) + Integer.toString(N & 1);
    }

    public String baseNeg2(int N) {
        if (N == 0 || N == 1) return Integer.toString(N);
        return baseNeg2(-(N >> 1)) + Integer.toString(N & 1);
    }
```
**C++:**
```cpp
    string base2(int N) {
        if (N == 0 || N == 1) return to_string(N);
        return base2(N >> 1) + to_string(N & 1);
    }

    string baseNeg2(int N) {
        if (N == 0 || N == 1) return to_string(N);
        return baseNeg2(-(N >> 1)) + to_string(N & 1);
    }
```

**Python:**
```py
    def base2(self, N):
        if N == 0 or N == 1: return str(N)
        return self.base2(N >> 1) + str(N & 1)

    def baseNeg2(self, N):
        if N == 0 or N == 1: return str(N)
        return self.baseNeg2(-(N >> 1)) + str(N & 1)
```
<br>

# More Solutions
[Easy One Line](https://leetcode.com/problems/convert-to-base-2/discuss/287017/JavaPython-Easy-One-Line)
</p>


### 4-line python clear solution with explanation
- Author: bupt_wc
- Creation Date: Sun Mar 31 2019 15:03:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 31 2019 15:03:37 GMT+0800 (Singapore Standard Time)

<p>
for each number, we first consider its binary mode, then we check 10,1000,100000,... one by one
for example, 6 = \'110\', so for the second \'1\',  in base -2, it makes the initial number decrease `4(+2 -> -2)`
so we jusr make the initial number add 4, then 6 -> 10 = \'1010\', later we consider the first \'1\', it makes the initial number decrease `16(+8 -> -8)`, then we add 16, 10 -> 26 = \'11010\', now we get the answer.

```python
class Solution:
    def baseNeg2(self, N: int) -> str:
        neg = [1 << i for i in range(1, 33, 2)]
        for mask in neg:
            if N & mask: N += mask*2
        return bin(N)[2:]
```
</p>


### C++ Geeks4Geeks
- Author: votrubac
- Creation Date: Sun Mar 31 2019 12:28:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 31 2019 12:28:28 GMT+0800 (Singapore Standard Time)

<p>
See this Geeks4Geeks article: [Convert a number into negative base representation](https://www.geeksforgeeks.org/convert-number-negative-base-representation/).

Below is the specialized solution for the ```-2``` base.
```
string baseNeg2(int N, string res = "") {
  while (N != 0) {
    int rem = N % -2;
    N /= -2;
    if (rem < 0) rem += 2, N += 1;
    res = to_string(rem) + res;
  }
  return max(string("0"), res);
}
```
</p>


