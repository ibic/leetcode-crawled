---
title: "Check if There is a Valid Path in a Grid"
weight: 1291
#id: "check-if-there-is-a-valid-path-in-a-grid"
---
## Description
<div class="description">
Given a <em>m</em> x <em>n</em> <code>grid</code>. Each cell of the <code>grid</code> represents a street. The street of&nbsp;<code>grid[i][j]</code> can be:
<ul>
	<li><strong>1</strong> which means a street connecting the left cell and the right cell.</li>
	<li><strong>2</strong> which means a street connecting the upper cell and the lower cell.</li>
	<li><b>3</b>&nbsp;which means a street connecting the left cell and the lower cell.</li>
	<li><b>4</b> which means a street connecting the right cell and the lower cell.</li>
	<li><b>5</b> which means a street connecting the left cell and the upper cell.</li>
	<li><b>6</b> which means a street connecting the right cell and the upper cell.</li>
</ul>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/03/05/main.png" style="width: 450px; height: 708px;" /></p>

<p>You will initially start at the street of the&nbsp;upper-left cell <code>(0,0)</code>. A valid path in the grid is a path which starts from the upper left&nbsp;cell <code>(0,0)</code> and ends at the bottom-right&nbsp;cell <code>(m - 1, n - 1)</code>. <strong>The path should only follow the streets</strong>.</p>

<p><strong>Notice</strong> that you are <strong>not allowed</strong> to change any street.</p>

<p>Return <i>true</i>&nbsp;if there is a valid path in the grid or <em>false</em> otherwise.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/03/05/e1.png" style="width: 455px; height: 311px;" />
<pre>
<strong>Input:</strong> grid = [[2,4,3],[6,5,2]]
<strong>Output:</strong> true
<strong>Explanation:</strong> As shown you can start at cell (0, 0) and visit all the cells of the grid to reach (m - 1, n - 1).
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/03/05/e2.png" style="width: 455px; height: 293px;" />
<pre>
<strong>Input:</strong> grid = [[1,2,1],[1,2,1]]
<strong>Output:</strong> false
<strong>Explanation:</strong> As shown you the street at cell (0, 0) is not connected with any street of any other cell and you will get stuck at cell (0, 0)
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,1,2]]
<strong>Output:</strong> false
<strong>Explanation:</strong> You will get stuck at cell (0, 1) and you cannot reach cell (0, 2).
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,1,1,1,1,1,3]]
<strong>Output:</strong> true
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> grid = [[2],[2],[2],[2],[2],[2],[6]]
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m == grid.length</code></li>
	<li><code>n == grid[i].length</code></li>
	<li><code>1 &lt;= m, n &lt;= 300</code></li>
	<li><code>1 &lt;= grid[i][j] &lt;= 6</code></li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Robinhood - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java clean BFS
- Author: rexue70
- Creation Date: Sun Mar 22 2020 12:47:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2020 15:08:27 GMT+0800 (Singapore Standard Time)

<p>
credit to @uwi, some modification
```
class Solution {
    int[][][] dirs = {
                {{0, -1}, {0, 1}},
                {{-1, 0}, {1, 0}},
                {{0, -1}, {1, 0}},
                {{0, 1}, {1, 0}},
                {{0, -1}, {-1, 0}},
                {{0, 1}, {-1, 0}}
    };
    //the idea is you need to check port direction match, you can go to next cell and check whether you can come back.
    public boolean hasValidPath(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        boolean[][] visited = new boolean[m][n];
        Queue<int[]> q = new LinkedList<>();
        q.add(new int[]{0, 0});
        visited[0][0] = true;
        while (!q.isEmpty()) {
            int[] cur = q.poll();
            int x = cur[0], y = cur[1];
            int num = grid[x][y] - 1;
            for (int[] dir : dirs[num]) {
                int nx = x + dir[0], ny = y + dir[1];
                if (nx < 0 || nx >= m || ny < 0 || ny >= n || visited[nx][ny]) continue;
                //go to the next cell and come back to orign to see if port directions are same
                for (int[] backDir : dirs[grid[nx][ny] - 1])
                    if (nx + backDir[0] == x && ny + backDir[1] == y) {
                        visited[nx][ny] = true;
                        q.add(new int[]{nx, ny});
                    }
            }
        }
        return visited[m - 1][n - 1];
    }
}
```
</p>


### [Python] Union Find
- Author: lee215
- Creation Date: Sun Mar 22 2020 12:06:03 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 27 2020 22:42:42 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
Assume we have input A[2][2]. (Thanks @Gad2017 for this diagram)
<img src="https://assets.leetcode.com/users/lee215/image_1584946037.png" alt="diagram" width="600"/>

The center of `A[0][0]` has coordinates `[0, 0]`
The center of `A[i][j]` has coordinates `[2i, 2j]`
The top edge of `A[i][j]` has coordinates `[2i-1, 2j]`
The left edge of `A[i][j]` has coordinates `[2i, 2j-1]`
The bottom edge of `A[i][j]` has coordinates `[2i+1, 2j]`
The right edge of `A[i][j]` has coordinates `[2i, 2j+1]`

Then we apply **Union Find**:
if `A[i][j]` in [2, 5, 6]: connect center and top
if `A[i][j]` in [1, 3, 5]: connect center and left
if `A[i][j]` in [2, 3, 4]: connect center and bottom
if `A[i][j]` in [1, 4, 6]: connect center and right
<br>


# Complexity
Time `O(MN) * O(UF)`
Space `O(MN)`
<br>

**Python:**
```py
    def hasValidPath(self, A):
        m, n = len(A), len(A[0])
        uf = {(i, j): (i, j) for i in xrange(-1, m * 2) for j in xrange(-1, n * 2)}

        def find(x):
            if uf[x] != x:
                uf[x] = find(uf[x])
            return uf[x]

        def merge(i, j, di, dj):
            uf[find((i, j))] = find((i + di, j + dj))

        for i in xrange(m):
            for j in xrange(n):
                if A[i][j] in [2, 5, 6]: merge(i * 2, j * 2, -1, 0)
                if A[i][j] in [1, 3, 5]: merge(i * 2, j * 2, 0, -1)
                if A[i][j] in [2, 3, 4]: merge(i * 2, j * 2, 1, 0)
                if A[i][j] in [1, 4, 6]: merge(i * 2, j * 2, 0, 1)
        return find((0, 0)) == find((m * 2 - 2, n * 2 - 2))
```

</p>


### Python easy DFS
- Author: alviniac
- Creation Date: Sun Mar 22 2020 12:09:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2020 12:34:03 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def hasValidPath(self, grid: List[List[int]]) -> bool:
        if not grid:
            return true
        directions = {1: [(0,-1),(0,1)],
                      2: [(-1,0),(1,0)],
                      3: [(0,-1),(1,0)],
                      4: [(0,1),(1,0)],
                      5: [(0,-1),(-1,0)],
                      6: [(0,1),(-1,0)]}
        visited = set()
        goal = (len(grid)-1, len(grid[0]) - 1)
        def dfs(i, j):
            visited.add((i,j))
            if (i,j) == goal:
                return True
            for d in directions[grid[i][j]]:
                ni, nj = i+d[0], j+d[1]
                if 0 <= ni < len(grid) and 0 <= nj < len(grid[0]) and (ni, nj) not in visited and (-d[0], -d[1]) in directions[grid[ni][nj]]:
                    if dfs(ni, nj):
                        return True
            return False
        return dfs(0,0)
```
Why ```(-d[0], -d[1]) in directions[grid[ni][nj]]```:?

When traversing from one cell to the next. the next cell must have a direction that is the opposite of the direction we are moving in for the cells to be connected. For example, if we are moving one unit to the right, then from the next cell it must be possible to go one unit to the left, otherwise it\'s not actually connected.
</p>


