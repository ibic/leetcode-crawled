---
title: "Kth Ancestor of a Tree Node"
weight: 1156
#id: "kth-ancestor-of-a-tree-node"
---
## Description
<div class="description">
<p>You are given a tree with&nbsp;<code>n</code>&nbsp;nodes numbered from&nbsp;<code>0</code>&nbsp;to&nbsp;<code>n-1</code>&nbsp;in the form of a parent array where <code>parent[i]</code>&nbsp;is the parent of node <code>i</code>. The root of the tree is node <code>0</code>.</p>

<p>Implement the function&nbsp;<code>getKthAncestor</code><code>(int node, int k)</code>&nbsp;to return the <code>k</code>-th ancestor of the given&nbsp;<code>node</code>. If there is no such ancestor, return&nbsp;<code>-1</code>.</p>

<p>The&nbsp;<em>k-th&nbsp;</em><em>ancestor</em>&nbsp;of a tree node is the <code>k</code>-th node&nbsp;in the path&nbsp;from that node to the root.</p>

<p>&nbsp;</p>

<p><strong>Example:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/08/28/1528_ex1.png" style="width: 396px; height: 262px;" /></strong></p>

<pre>
<b>Input:</b>
[&quot;TreeAncestor&quot;,&quot;getKthAncestor&quot;,&quot;getKthAncestor&quot;,&quot;getKthAncestor&quot;]
[[7,[-1,0,0,1,1,2,2]],[3,1],[5,2],[6,3]]

<b>Output:</b>
[null,1,0,-1]

<b>Explanation:</b>
TreeAncestor treeAncestor = new TreeAncestor(7, [-1, 0, 0, 1, 1, 2, 2]);

treeAncestor.getKthAncestor(3, 1);  // returns 1 which is the parent of 3
treeAncestor.getKthAncestor(5, 2);  // returns 0 which is the grandparent of 5
treeAncestor.getKthAncestor(6, 3);  // returns -1 because there is no such ancestor
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= k &lt;=&nbsp;n &lt;= 5*10^4</code></li>
	<li><code>parent[0] == -1</code>&nbsp;indicating that&nbsp;<code>0</code>&nbsp;is the root node.</li>
	<li><code>0 &lt;= parent[i] &lt; n</code>&nbsp;for all&nbsp;<code>0 &lt;&nbsp;i &lt; n</code></li>
	<li><code>0 &lt;= node &lt; n</code></li>
	<li>There will be at most <code>5*10^4</code> queries.</li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Explanation [with c++ sample code]
- Author: ukohank517
- Creation Date: Sun Jun 14 2020 12:02:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 30 2020 10:23:35 GMT+0800 (Singapore Standard Time)

<p>
# method
this is a question about `doubling`

And here is what I thought: 


### first, brute force

What I believe is : **Every algorithm starts from the brute force method**, 
So when I have no idea about a question, I always start from brute force algorithm.

If we do nothing for the initial data, 
basicly, since we have parent[NODE] represents the parent of NODE.
So, we can run `getKthAncestor(node, k)` by a for statement, 

Time complexity:: 
O(1)  for initializing 
O(K) for getKthAncestor


### second and also the last, tricky algorithm

Since There will be at most `5*10^4` queries for method `getKthAncestor`,
Let\'s set queries called time as N, 
It means, the brute force\'s complexity will become O(NK)......-> TLE!

So we have to reduce eighter N or K.
here, N is the number of queries, the element we cannot influence, so we know we have to reduce the time complexity for getKthAncestor.

When you want to get [K]th ancestor of [node], if you have already known [K/2]th ancestor, as what we talk in the first part, we can use [K/2]th parent twice to get the answer. If you notice this, then everything becomes simple.

But what if we remember the whole kth (1st, 2nd, 3th, 4th.....) ansestor?
in that case, we only use O(1) for getKthAncestor, but to prepare the data, we need to take O(K^2)  -> TLE!!!.

So we need a much more efficient way to prepare & get data...
Let\'s see the parent again, and remember the [2^i]th parent: 
 - we have 1st parent for node. 
 - use this, we can know 2nd parent, by searching [1st parent]\'s [2^0 = 1st parent],
 - to know the 3rd parent, trace [2nd parent]\'s [1st parent] 
 - to know the 4th parent, trace [2nd parent]\'s [2nd parent]  -> we memory it as [2^2 = 4th parent]
 - to know the 5th parent, trace [4th parent]\'s [1st parent]
 - .....



### conclution

Here, we can make a P[i][node], it means  [node]\'s [2^i]th parent.
and when we want to take the Kth parent, we just turn the number \'k\' to  binary number, and see the bits to trace parent.

for example
``` txt
let\'s set K = 5, 
5 (in 10 base ) = 101(in 2 base), 
```
so we find the (2^0) parent firstly, then find the (2^2) parent, 

Time complexity:
**O(log(max(K)) * number of node)** for initilizing(prepare)
**O(log(K))**   for getKthAncestor (N times query).



### references:

There are a lot of references in the internet, the keywords are 
**doubling , algorithm , tree**


# sample code

```
class TreeAncestor {
public:
    vector<vector<int> > P; // P[i][node] :::: [node] \'s [2^i]th parent
    TreeAncestor(int n, vector<int>& parent) {
        // initialize
        P.resize(20, vector<int>(parent.size(), -1));
        
        // 2^0
        for(int i = 0; i < parent.size(); i++){
            P[0][i] = parent[i];
        }
        
        // 2^i
        for(int i = 1; i < 20; i++){
            for(int node = 0; node < parent.size(); node ++){
                int nodep = P[i-1][node];
                if(nodep != -1) P[i][node] = P[i-1][nodep];
            }
        }
    }
    
    int getKthAncestor(int node, int k) {
        for(int i = 0; i < 20; i++){
            if(k & (1 << i)){
                node = P[i][node];
                if(node == -1) return -1;
            }
        }

        return node;
    }
};
```

#### other way for getting ancestor
You know, it\'s quite hard for me to understand the bit calculation in the begining, 
so here is another way to get the kth ancestor without bit calculation : 
```
    int getKthAncestor(int node, int k) {
        for(int i = 19; i >= 0; i--){ // remember to check the bit from TOP!!!!!!!!
		    int num = pow(2, i); // we don\'t think bit, just see if we can jump to [num th] ancestor
            if(k >= num){        // if we can
                node = P[i][node];
				k -= num;         // we jump back, so the rest step is [k - num]
                if(node == -1) return -1;				
            }
        }

        return node;
    }
```

</p>


### Why does brute force work in golang / Java / JS?
- Author: manparvesh
- Creation Date: Sun Jun 14 2020 12:17:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 14 2020 13:24:14 GMT+0800 (Singapore Standard Time)

<p>
The person who ranked 5 in this contest wrote brute force in golang. And there are other people who did brute force in Java and it worked. This did not work in Python, so either it should work in all languages or in none at all.

</p>


### [Java/C++/Python] Binary Lifting
- Author: lee215
- Creation Date: Sun Jun 14 2020 12:09:02 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 15 2020 14:21:01 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
`A` is the parent in 1 step
Based on this, we can find the parent in 2 steps.
Again, based on this, we can find the parent in 4 steps.
And so on.

This will help you jump fast on the ancestor tree.
<br>

**Python:**
```py
    step = 15
    def __init__(self, n, A):
        A = dict(enumerate(A))
        jump = [A]
        for s in xrange(self.step):
            B = {}
            for i in A:
                if A[i] in A:
                    B[i] = A[A[i]]
            jump.append(B)
            A = B
        self.jump = jump

    def getKthAncestor(self, x, k):
        step = self.step
        while k > 0 and x > -1:
            if k >= 1 << step:
                x = self.jump[step].get(x, -1)
                k -= 1 << step
            else:
                step -= 1
        return x
```

**Java**
@changeme
```java
    int[][] jump;
    int maxPow;

    public TreeAncestor(int n, int[] parent) {
        // log_base_2(n)
        maxPow = (int) (Math.log(n) / Math.log(2)) + 1;
        jump = new int[maxPow][n];
        jump[0] = parent;
        for (int p = 1; p < maxPow; p++) {
            for (int j = 0; j < n; j++) {
                int pre = jump[p - 1][j];
                jump[p][j] = pre == -1 ? -1 : jump[p - 1][pre];
            }
        }
    }

    public int getKthAncestor(int node, int k) {
        int maxPow = this.maxPow;
        while (k > 0 && node > -1) {
            if (k >= 1 << maxPow) {
                node = jump[maxPow][node];
                k -= 1 << maxPow;
            } else
                maxPow -= 1;
        }
        return node;
    }
```
**C++**
all from @user4745

ok so there is concept of binary lifting,
what is binary lifting ??

So any number can be expressed power of 2,
and we can greedily find that ,
by taking highest powers and taking the modulo,
or just utilising the inbuilt binary format that
how numbers are stored in computer,
so we know how to represent k in binary format,
so now using the same concept we will jump on k height using binary powers,
and remove the highest power ,
but here we must know the 2^i th height parent from every node,
so we will preprocess the tree as shown in the code,
so u could see as 2^i = 2^(i-1) + 2^(i-1),
so if we calculated 2^(i-1) th parent of every node beforehand
we could calculate 2^i th parent from that,
like 2^(i-1) th parent of 2^(i-1) th parent of the node,
right like 4th parent is 2nd parent of 2nd parent of node,
this is called binary lifting.

so now utilise the binary version of k and jump accordingly.
I have also made parent of 0 to -1,
so that I could stop if I went above the node.

```cpp
    vector<vector<int>>v;
    TreeAncestor(int n, vector<int>& parent) {
        vector<vector<int>> par(n, vector<int>(20));
        for (int i = 0; i < n; i++) par[i][0] = parent[i];
        for (int j = 1; j < 20; j++) {
            for (int i = 0; i < n; i++) {
                if (par[i][j - 1] == -1) par[i][j] = -1;
                else par[i][j] = par[par[i][j - 1]][j - 1];
            }
        }
        swap(v, par);
    }
    int getKthAncestor(int node, int k) {
        for (int i = 0; i < 20; i++) {
            if ((k >> i) & 1) {
                node = v[node][i];
                if (node == -1) return -1;
            }
        }
        return node;
    }
```


</p>


