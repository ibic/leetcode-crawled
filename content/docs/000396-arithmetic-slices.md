---
title: "Arithmetic Slices"
weight: 396
#id: "arithmetic-slices"
---
## Description
<div class="description">
<p>A sequence of numbers is called arithmetic if it consists of at least three elements and if the difference between any two consecutive elements is the same.</p>

<p>For example, these are arithmetic sequences:</p>

<pre>
1, 3, 5, 7, 9
7, 7, 7, 7
3, -1, -5, -9</pre>

<p>The following sequence is not arithmetic.</p>

<pre>
1, 1, 2, 5, 7</pre>
&nbsp;

<p>A zero-indexed array A consisting of N numbers is given. A slice of that array is any pair of integers (P, Q) such that 0 &lt;= P &lt; Q &lt; N.</p>

<p>A slice (P, Q) of the array A is called arithmetic if the sequence:<br />
A[P], A[P&nbsp;+ 1], ..., A[Q - 1], A[Q] is arithmetic. In particular, this means that P + 1 &lt; Q.</p>

<p>The function should return the number of arithmetic slices in the array A.</p>
&nbsp;

<p><b>Example:</b></p>

<pre>
A = [1, 2, 3, 4]

return: 3, for 3 arithmetic slices in A: [1, 2, 3], [2, 3, 4] and [1, 2, 3, 4] itself.
</pre>

</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Aetion - 0 (taggedByAdmin: true)
- Baidu - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Brute Force 

The most naive solution is to consider every pair of elements(with atleast 1 element between them), so that the range of elements lying between these two elements acts as a slice. Then, we can iterate over every such slice(range) to check if all the consecutive elements within this range have the same difference. For every such range found, we can increment the $$count$$ that is used to keep a track of the required result.

<iframe src="https://leetcode.com/playground/TzyaGSVh/shared" frameBorder="0" width="100%" height="344" name="TzyaGSVh"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^3)$$. We iterate over the range formed by every pair of elements. Here, $$n$$ refers to the number of elements in the given array $$A$$.

* Space complexity : $$O(1)$$. Constant extra space is used.

<br />
<br />


---
#### Approach 2: Better Brute Force

**Algorithm**

In the last approach, we considered every possible range and then iterated over the range to check if the difference between every consercutive element in this range is the same. We can optimize this approach to some extent, by making a small observation. 

We can see, that if we are currently considering the range bound by the elements, let's say, $$A[s]$$(start) and $$A[e]$$(end), we have checked the consecutive elements in this range to have the same difference. Now, when we move on to the next range between the indices $$s$$ and $$e+1$$, we again perform a check on all the elements in the range $$s:e$$, along with one additional pair $$A[e+1]$$ and $$A[e]$$. We can remove this redundant check in the range $$s:e$$ and just check the last pair to have the same difference as the one used for the previous range(same $$s$$, incremented $$e$$).

Note that if the last range didn't constitute an arithmetic slice, the same elements will be a part of the updated range as well. Thus, we can omit the rest of the ranges consisting of the same starting index. The rest of the process remains the same as in the last approach.

<iframe src="https://leetcode.com/playground/h67hCevJ/shared" frameBorder="0" width="100%" height="310" name="h67hCevJ"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Two for loops are used.

* Space complexity : $$O(1)$$. Constant extra space is used.

<br />
<br />


---
#### Approach 3: Using Recursion

**Algorithm**

By making use of the observation discussed in the last approach, we know, that if a range of elements between the indices $$(i,j)$$ constitute an Arithmetic Slice, and another element $$A[j+1]$$ is included such that $$A[j+1]$$ and $$A[j]$$ have the same difference as that of the previous common difference, the ranges between $$(i,j+1)$$ will constitutes an arithmetic slice. Further, if the original range $$(i,j)$$ doesn't form an arithmetic slice, adding new elements to this range won't do us any good. Thus, no more arithmetic slices can be obtained by adding new elements to it.

By making use of this observation, we can develop a recursive solution for the given problem as well. Assume that a $$sum$$ variable is used to store the total number of arithmetic slices in the given array $$A$$. We make use of a recursive function `slices(A,i)` which returns the number of Arithmetic Slices in the range $$(k,i)$$, but which are not a part of any range $$(k,j)$$ such that $$j<i$$. It also updates $$sum$$ with the number of arithmetic slices(total) in the current range. Thus, $$k$$ refers to  the minimum index such that the range $$(k,i)$$ constitutes a valid arithmetic slice.

Now, suppose we know the number of arithmetic slices in the range $$(0,i-1)$$ constituted by the elements $$[a_0,a_1,a_2,...a_(i-1)]$$, to be say $$x$$. If this range itself is an arithmetic slice, all the consecutive elements have the same difference(equal to say, $$a_(i-1)-a_(i-2)$$). Now, adding a new element $$a_i$$ to it to extend the range to $$(0,i)$$ will constitute an arithmetic slice only if this new element satisfies $$a_i-a_(i-1)=a_(i-1)-a_(i-2)$$. Thus, now, the addition of this new element, will lead to an addition of $$ap$$ number of arithmetic slices to the ones obtained in the range $$(0,i-1)$$. The new arithmetic slices will be the ones constituting the ranges $$(0,i), (1,i), ... (i-2,i)$$, which are a total of  $$x+1$$ additional arithmetic slices. This is because, apart from the range $$(0,i)$$ the rest of the ranges $$(1,i), (2,i),...(i-2,i)$$ can be mapped to $$(0,i-1), (1,i-1),...(i-3,i-1)$$, with count equal to $$x$$. 

Thus, in every call to `slices`, if the $$i^{th}$$ element has the same common difference with the last element as the previous common difference, we can find the number of new arithmetic slices added by the use of this element, $$ap$$ and also update the $$sum$$ to include this $$ap$$ into it, apart from the count obtained by the smaller ranges. But, if the new element doesn't have the same common difference, extra arithmetic slices can't be contributed by it and hence, no addition is done to $$sum$$ for the current element. But, of course $$sum$$ will be updated as per the count obtained from the smaller ranges.

<iframe src="https://leetcode.com/playground/NQkYdx3b/shared" frameBorder="0" width="100%" height="361" name="NQkYdx3b"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. The recursive function is called at most $$n-2$$ times.

* Space complexity : $$O(n)$$. The depth of the recursion tree goes upto $$n-2$$.

<br />
<br />


---
#### Approach 4: Dynamic Programming

**Algorithm**

In the last approach, we start with the full range $$(0,n-1)$$, where $$n$$ is the number of elements in the given $$A$$ array. We can observe that the result for the range $$(0,i)$$ only depends on the elements in the range $$(0,i)$$ and not on any element beyond this range. Thus, we can make use of Dynamic Programming to solve the given problem.

We can make use of a 1-D $$dp$$ with number of elements equal to $$n$$. $$dp[i]$$ is used to store the number of arithmetic slices possible in the range $$(k,i)$$ and not in any range $$(k,j)$$ such that $$j<i$$. Again, $$k$$ refers to the minimum index possible such that $$(k,j)$$ constitutes a valid Arithmetic Slice.

Instead of going in the reverse order as in the recursive approach, we can start filling the $$dp$$ in a forward manner. The intuition remains the same as in the last approach. For the $$i^{th}$$ element being considered, we check if this element satsfies the common difference criteria with the previous element. If so, we know the number of new arithmetic slices added will be $$1+dp[i-1]$$ as discussed in the last approach. The $$sum$$ is also updated by the same count to reflect the new arithmetic slices added.  

The following animation illustrates the $$dp$$ filling process.

!?!../Documents/413_Arithmetic_Slices.json:1000,563!?!

<iframe src="https://leetcode.com/playground/TzzPyBLN/shared" frameBorder="0" width="100%" height="276" name="TzzPyBLN"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We traverse over the given $$A$$ array with $$n$$ elements once only.

* Space complexity : $$O(n)$$. 1-D $$dp$$ of size $$n$$ is used.

<br />
<br />


---
#### Approach 5: Constant Space Dynamic Programming

**Algorithm**

In the last approach, we can observe that we only require the element $$dp[i-1]$$ to determine the value to be entered at $$dp[i]$$. Thus, instead of making use of a 1-D array to store the required data, we can simply keep a track of just the last element. 

<iframe src="https://leetcode.com/playground/cTjs9bAZ/shared" frameBorder="0" width="100%" height="293" name="cTjs9bAZ"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We traverse over the given $$A$$ array with $$n$$ elements once only.

* Space complexity : $$O(1)$$. Constant extra space is used.

---
#### Approach 6: Using Formula

**Algorithm**

From the $$dp$$ solution, we can observe that for $$k$$ consecutive elements sastisfying the common difference criteria, we update the $$sum$$ for each such element by $$1, 2, 3, ..., k$$ counts in that order. Thus, instead of updating the $$sum$$ at the same time, we can just keep a track of the number of consecutive elements satisfying the common differnce criteria in a $$count$$ variable and just update the $$sum$$ directly as $$count*(count+1)/2$$ whenver an element not satisfying this criteria is found. At the same time, we also need to reset the $$count$$ value. 

<iframe src="https://leetcode.com/playground/4d8UKupp/shared" frameBorder="0" width="100%" height="310" name="4d8UKupp"></iframe>


**Complexity Analysis**

* Time complexity : $$O(n)$$. We iterate over $$A$$ with $$n$$ elements exactly once.

* Space complexity : $$O(1)$$. Constant extra space is used.

## Accepted Submission (python3)
```python3
class Solution:
    def numberOfArithmeticSlices(self, A: List[int]) -> int:
        la = len(A)
        if la < 3:
            return 0
        # now la is at least 3
        li = A + [2 * A[-1] - A[-2] + 1]
        diff = A[1] - A[0]
        curCount = 2
        r = 0
        for i in range(2, la + 1):
            curDiff = li[i] - li[i - 1]
            if curDiff == diff:
                curCount += 1
            else:
                if curCount > 2:
                    r += (curCount - 1) * (curCount - 2) // 2
                diff = curDiff
                curCount = 2
        return r
```

## Top Discussions
### Simple Java solution 9 lines, 2ms
- Author: lcl7722
- Creation Date: Sun Oct 16 2016 03:16:54 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 01:55:50 GMT+0800 (Singapore Standard Time)

<p>
    public int numberOfArithmeticSlices(int[] A) {
        int curr = 0, sum = 0;
        for (int i=2; i<A.length; i++)
            if (A[i]-A[i-1] == A[i-1]-A[i-2]) {
                curr += 1;
                sum += curr;
            } else {
                curr = 0;
            }
        return sum;
    }
</p>


### 3ms C++ Standard DP Solution with Very Detailed Explanation
- Author: vesion
- Creation Date: Thu Oct 13 2016 17:44:36 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 23:23:43 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& A) {
        int n = A.size();
        if (n < 3) return 0;
        vector<int> dp(n, 0); // dp[i] means the number of arithmetic slices ending with A[i]
        if (A[2]-A[1] == A[1]-A[0]) dp[2] = 1; // if the first three numbers are arithmetic or not
        int result = dp[2];
        for (int i = 3; i < n; ++i) {
            // if A[i-2], A[i-1], A[i] are arithmetic, then the number of arithmetic slices ending with A[i] (dp[i])
            // equals to:
            //      the number of arithmetic slices ending with A[i-1] (dp[i-1], all these arithmetic slices appending A[i] are also arithmetic)
            //      +
            //      A[i-2], A[i-1], A[i] (a brand new arithmetic slice)
            // it is how dp[i] = dp[i-1] + 1 comes
            if (A[i]-A[i-1] == A[i-1]-A[i-2]) 
                dp[i] = dp[i-1] + 1;
            result += dp[i]; // accumulate all valid slices
        }
        return result;
    }
};
```
</p>


### Detailed Explanation: Two DP Solutions
- Author: hf_z
- Creation Date: Wed Jan 09 2019 11:15:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jan 09 2019 11:15:39 GMT+0800 (Singapore Standard Time)

<p>
### 1st DP Solution: time O(n^2), space: O(n^2)
We find the sub problem: 
Assume `A[i:j]` (both include `A[i]` and `A[j]`) is an  arithmetic slice, then we have:
1) if `A[i]-A[i-1] = = A[i+1]-A[i]`, then `A[i-1:j]` is  an arithmetic slice;
2) if `A[j+1]-A[j] = = A[j]-A[j-1]`, then `A[i:j+1]` is  an arithmetic slice.

use `dp[i][j]` to memorize whether `A[i:j]` is  an  arithmetic slice,  and `count` to count the  num of arithmetic slices:

    public int numberOfArithmeticSlices(int[] A) {
        int n=A.length;
        if(n<3){return 0;}
        boolean[][] dp=new boolean[n][n]; //initial value is false
        int count=0;
        for(int i=0;i<n-3+1;i++){
            if((A[i+1]-A[i])==(A[i+2]-A[i+1])){
                dp[i][i+3-1]=true;
                count++;
            }
        }
        for(int k=4;k<=n;k++){
            for (int i=0;i<n-k+1;i++){
                int j=i+k-1;
                if(dp[i+1][j]==true&&(A[i+1]-A[i]==A[i+2]-A[i+1])){
                    dp[i][j]=true;
                    count++;
                }else if(dp[i][j-1]==true&&(A[j]-A[j-1]==A[j-1]-A[j-2])){
                    dp[i][j]=true;
                    count++;
                }
            }
        }
        return count;
    }
### 2nd DP Solution: time O(n), space O(n)
We can find another sub problem: assume `dp[i]` is the number of arithmetic slices which are end with `A[i]`. then we have:
`dp[i]=(A[i]-A[i-1] = = A[i-1]-A[i-2])? 1+dp[i-1] : 0`, the code:

    public int numberOfArithmeticSlices(int[] A) {
        int n=A.length;
        if(n<3){return 0;}
        int[] dp=new int[n];
        dp[0]=0;
        dp[1]=0;
        int sum=0;
        for(int i=2;i<n;i++){
            if((A[i]-A[i-1])==(A[i-1]-A[i-2])){
                dp[i]=dp[i-1]+1;
            }else{
                dp[i]=0;
            }
            sum+=dp[i];
        }
        return sum;
    }
up to now, time complexity is O(n), but space complexity is also O(n). In fact, we only need a `curr` to memorize the num of arithmetic slices which end with current `A[i]` and a `sum` to memorize num of all `curr`. that is [@icl7722\'s solution](https://leetcode.com/problems/arithmetic-slices/discuss/90058/Simple-Java-solution-9-lines-2ms), which time complexity is O(n) and space complexity is O(1).

	public int numberOfArithmeticSlices(int[] A) {
		int curr = 0, sum = 0;
		for (int i=2; i<A.length; i++)
			if (A[i]-A[i-1] == A[i-1]-A[i-2]) {
				curr += 1;
				sum += curr;
			} else {
				curr = 0;
			}
		return sum;
	}
</p>


