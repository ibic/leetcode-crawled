---
title: "Remove Boxes"
weight: 514
#id: "remove-boxes"
---
## Description
<div class="description">
<p>Given several boxes with different colors represented by different positive numbers.<br />
You may experience several rounds to remove boxes until there is no box left. Each time you can choose some continuous boxes with the same color (composed of k boxes, k &gt;= 1), remove them and get <code>k*k</code> points.<br />
Find the maximum points you can get.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> boxes = [1,3,2,2,2,3,4,3,1]
<strong>Output:</strong> 23
<strong>Explanation:</strong>
[1, 3, 2, 2, 2, 3, 4, 3, 1] 
----&gt; [1, 3, 3, 4, 3, 1] (3*3=9 points) 
----&gt; [1, 3, 3, 3, 1] (1*1=1 points) 
----&gt; [1, 1] (3*3=9 points) 
----&gt; [] (2*2=4 points)
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= boxes.length &lt;= 100</code></li>
	<li><code>1 &lt;= boxes[i]&nbsp;&lt;= 100</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Depth-first Search (depth-first-search)

## Companies
- Apple - 2 (taggedByAdmin: false)
- Tencent - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force Approach[Time Limit Exceeded]

**Algorithm**

The brute force approach is very obvious. We try removing every possible element of the given array and calculate the points obtained for the rest of the array in a recursive manner.


<iframe src="https://leetcode.com/playground/7oDx7spV/shared" frameBorder="0" name="7oDx7spV" width="100%" height="513"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n!)$$. $$f(n)$$ be the time to find the solution of n boxes with n different colors, then obviously $$f(n) = n * f(n-1)$$ which results in the $$n!$$ time complexity.

* Space complexity : $$O(n^2)$$. The recursive tree goes upto a depth of $$n$$, with every level consisting of upto $$n$$ $$\text{newBoxes}$$ elements.

---

#### Approach #2 Using DP with Memorization[Accepted]

**Algorithm**

The problem with the previous approach is that it involves a lot of recomputations. e.g. Consider the array `[3, 2, 1, 4, 4, 4]`. In this case, we try to remove 3 and calculate the cost for the remaining array, in which we try removing 2 first leading to the point calculation for the subarray `[1, 4, 4, 4]`. The same happens in the second iteration in which we try to remove 2 first and then remove 3. We can prune the depth of the recursion tree a lot by using memorization.

But the problem of memorization isn't simple in this case. We can't simply use the start and end index of the array to determine the maximum number of points which that subarray will eventually lead to. This is because the points obtained by using the subarray depend not only on the subarray but also on the removals done prior to reaching the current subarray, which aren't even a part of the subarray. e.g. Consider the array `[3, 2, 1, 4, 4, 2, 4, 4]`. The points obtained for the subarray `[3, 2, 1, 4]` depend on whether the element 2(index 5) has been already removed or not, since it eventually determines the number of 4's which will be combined together to determine the potential points obtained for the currently considered subarray.

Thus, in order to preserve this information, we need to add another dimension to the memorization array, which tells us how many similar elements are combined together from the end of the current subarray. We make use of a $$\text{dp}$$ array, which is used to store the maximum number of points that can be obtained for a given subarray with a specific number of similar elements at the end. For an entry in $$\text{dp[l][r][k]}$$, $$l$$ represents the starting index of the subarray, $$r$$ represents the ending index of the subarray and $$k$$ represents the number of elements similar to the $$r^{th}$$ element following it which can be combined to obtain the point information to be stored in $$\text{dp[l][r][k]}$$. e.g.

This can be better understood with the following example. Consider a subarray $$[x_l, x_{l+1},.., x_i,.., x_r, 6, 6, 6]$$. For this subarray, if x_r=6, the entry at $$\text{dp[l][r][3]}$$ represents the maximum points that can be obtained using the subarray $$boxes[l:r]$$ if three 6's are appended with the trailing $$x_r$$.

Now, let us look at how to fill in the $$dp$$. Consider the same suabrray as mentioned above. For filling in the entry, $$\text{dp[l][r][k]}$$, we firstly make an initial entry in $$\text{dp[l][r][k]}$$, which considers the assumption that we will firstly combine the last $$k+1$$ similar elements and then proceed with the remaining subarray. Thus, the initial entry becomes:

$$\text{dp[l][r][k]} = dp[l][r-1][0] + (k+1)*(k+1)$$. Here, we combined all the trailing similar elements, so the value 0 is passed as the k value for the recursive function, since no similar elements to the $$(r-1)^{th}$$ element exist at its end.

But, the above situation isn't the only possible solution. We could obtain a better solution for the same subarray $$boxes[l:r]$$ for making the entry into $$\text{dp[l][r][k]}$$, if we could somehow combine the trailing similar elements with some extra similar elements lying between $$boxes[l:r]$$.

Thus, we look for the elements within $$boxes[l:r]$$, which could be similar to the trailing $$k$$ elements, which in turn are similar to the $$r^{th}$$ element. Whenever such an element $$boxes[i]$$ is found, we check if the new solution could lead to more points by using the same array. If so, we update the entry at $$\text{dp[l][r][k]}$$.

To get a clearer understanding of the above statment, consider the same subarray again: $$[x_l, x_{l+1},.., x_i,.., x_r, 6, 6, 6]$$. If $$x_i = x_r = 6$$, we could eventually be benefitted by combining $$x_i$$ and $$x_r$$ by removing the elements lying between them, since now we can bring $$k+2$$ similar elements together. By removing the in-between lying elements($$[x_{i+1}, x_{i+2},..., x_{r-1}]$$, the maximum points we can obtain are given by: $$\text{dp[i+1][r-1][0]}$$. Now, the points obtained from the remaining array $$[x_l, x_{l+1},.., x_i,x_r, 6, 6, 6]$$ are given by: $$\text{dp[l][i][k+1]}$$, which is quite clear now.

Thus equation for $$dp$$ updation becomes:

$$\text{dp[l][r][k]} = max(\text{dp[l][r][k]}, \text{dp[l][i][k+1]} + \text{dp[i+1][r-1][0]})$$.

At the end, the entry for dp[0][n-1][0] gives the required result. In the implementation below, we've made use of `calculatePoints` function which is simply a recursive function used to obtain the $$\text{dp}$$ values.

<iframe src="https://leetcode.com/playground/56ydatBv/shared" frameBorder="0" name="56ydatBv" width="100%" height="445"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^4)$$  $$dp$$ array of size $$n^3$$ is filled, and linear
time is taken to process each element.

* Space complexity : $$O(n^3)$$  $$dp$$ array is of size $$n^3$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java top-down and bottom-up DP solutions
- Author: fun4LeetCode
- Creation Date: Thu Mar 30 2017 02:49:28 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 01:40:53 GMT+0800 (Singapore Standard Time)

<p>
Since the input is an array, let\'s begin with the usual approach by breaking it down with the original problem applied to each of the subarrays. 

Let the input array be `boxes` with length `n`. Define `T(i, j)` as the maximum points one can get by removing boxes of the subarray `boxes[i, j]` (both inclusive). The original problem is identified as `T(0, n - 1)` and the termination condition is as follows:
1. `T(i, i - 1) = 0`: no boxes so no points.
2. `T(i, i) = 1`: only one box left so the maximum point is `1`.

Next let\'s try to work out the recurrence relation for `T(i, j)`. Take the first box `boxes[i]`(i.e., the box at index `i`) as an example. What are the possible ways of removing it? (Note: we can also look at the last box and the analyses turn out to be the same.)

If it happens to have a color that you dislike, you\'ll probably say "I don\'t like this box so let\'s get rid of it now". In this case, you will first get `1` point for removing this poor box. But still you want maximum points for the remaining boxes, which by definition is `T(i + 1, j)`. In total your points will be `1 + T(i + 1, j)`.

But later after reading the rules more carefully, you realize that you might get more points if this box (`boxes[i]`) can be removed together with other boxes of the same color. For example, if there are two such boxes, you get `4` points by removing them simultaneously, instead of `2` by removing them one by one. So you decide to let it stick around a little bit longer until another box of the same color (whose index is `m`) becomes its neighbor. Note up to this moment all boxes from index `i + 1` to index `m - 1` would have been removed. So if we again aim for maximum points, the points gathered so far will be `T(i + 1, m - 1)`.  What about the remaining boxes?

At this moment, the boxes we left behind consist of two parts: the one at index `i` (`boxes[i]`) and those of the subarray `boxes[m, j]`, with the former bordering the latter from the left. Apparently there is no way applying the definition of the subproblem to the subarray `boxes[m, j]`, since we have some extra piece of information that is not included in the definition. **In this case, I shall call that the definition of the subproblem is not self-contained and its solution relies on information external to the subproblem itself**.

Another example of problem that does not have self-contained subproblems is [leetcode 312. Burst Balloons](https://leetcode.com/problems/burst-balloons/#/description), where the maximum coins of subarray `nums[i, j]` depend on the two numbers adjacent to `nums[i]` on the left and to `nums[j]` on the right. So you may find some similarities between these two problems. 

Problems without self-contained subproblems usually don\'t have well-defined recurrence relations, which renders it impossible to be solved recursively. The cure to this issue can sound simple and straightforward: **modify the definition of the problem to absorb the external information so that the new one is self-contained**.

So let\'s see how we can redefine `T(i, j)` to make it self-contained. First let\'s identify the external information. On the one hand, from the point of view of the subarray `boxes[m, j]`, it knows nothing about the number (denoted by `k`) of boxes of the same color as `boxes[m]`to its left. On the other hand, given this number `k`, the maximum points can be obtained from removing all these boxes is fixed. Therefore the external information to `T(i, j)` is this `k`. Next let\'s absorb this extra piece of information into the definition of `T(i, j)` and redefine it as `T(i, j, k)` which denotes the maximum points possible by removing the boxes of subarray `boxes[i, j]` with `k` boxes attached to its left of the same color as `boxes[i]`. Lastly let\'s reexamine some of the statements above:

1. Our original problem now becomes `T(0, n - 1, 0)`, since there is no boxes attached to the left of the input array at the beginning.

2. The termination conditions now will be:
**a**. `T(i, i - 1, k) = 0`: no boxes so no points, and this is true for any `k` (you can interpret it as nowhere to attach the boxes).
**b**. `T(i, i, k) = (k + 1) * (k + 1)`: only one box left in the subarray but we\'ve already got `k` boxes of the same color attached to its left, so the total number of boxes of the same color is `(k + 1)` and the maximum point is `(k + 1) * (k + 1)`.

3. The recurrence relation is as follows and the maximum points will be the larger of the two cases:
**a**. If we remove `boxes[i]` first, we get `(k + 1) * (k + 1) + T(i + 1, j, 0)` points, where for the first term, instead of `1` we again get `(k + 1) * (k + 1)` points for removing `boxes[i]` due to the attached boxes to its left; and for the second term there will be no attached boxes so we have the `0` in this term.
**b**. If we decide to attach `boxes[i]` to some other box of the same color, say `boxes[m]`, then from our analyses above, the total points will be `T(i + 1, m - 1, 0) + T(m, j, k + 1)`, where for the first term, since there is no attached boxes for subarray `boxes[i + 1, m - 1]`, we have `k = 0` for this part; while for the second term, the total number of attached boxes for subarray `boxes[m, j]` will increase by `1` because apart from the original `k` boxes, we have to account for `boxes[i]`now, so we have `k + 1` for this term. But we are not done yet. What if there are multiple boxes of the same color as `boxes[i]` within subarray `boxes[i + 1, j]`? We have to try each of them and choose the one that yields the maximum points. Therefore the final answer for this case will be: `max(T(i + 1, m - 1, 0) + T(m, j, k + 1))` where `i < m <= j && boxes[i] == boxes[m]`.

Before we get to the actual code, it\'s not hard to discover that there is overlapping among the subproblems `T(i, j, k)`, therefore it\'s qualified as a DP problem and its intermediate results should be cached for future lookup. Here each subproblem is characterized by three integers `(i, j, k)`, all of which are bounded, i.e, `0 <= i, j, k < n`, so a three-dimensional array (`n x n x n`) will be good enough for the cache.

Finally here are the two solutions, one for top-down DP and the other for bottom-up DP. From the bottom-up solution, the time complexity will be `O(n^4)` and the space complexity will be `O(n^3)`.

**`Top-down DP:`**

```
public int removeBoxes(int[] boxes) {
    int n = boxes.length;
    int[][][] dp = new int[n][n][n];
    return removeBoxesSub(boxes, 0, n - 1, 0, dp);
}
    
private int removeBoxesSub(int[] boxes, int i, int j, int k, int[][][] dp) {
    if (i > j) return 0;
    if (dp[i][j][k] > 0) return dp[i][j][k];
    
    for (; i + 1 <= j && boxes[i + 1] == boxes[i]; i++, k++); // optimization: all boxes of the same color counted continuously from the first box should be grouped together
    int res = (k + 1) * (k + 1) + removeBoxesSub(boxes, i + 1, j, 0, dp);
    
    for (int m = i + 1; m <= j; m++) {
        if (boxes[i] == boxes[m]) {
            res = Math.max(res, removeBoxesSub(boxes, i + 1, m - 1, 0, dp) + removeBoxesSub(boxes, m, j, k + 1, dp));
        }
    }
        
    dp[i][j][k] = res;
    return res;
}
```

**`Bottom-up DP:`**

```
public int removeBoxes(int[] boxes) {
    int n = boxes.length;
    int[][][] dp = new int[n][n][n];
    	
    for (int j = 0; j < n; j++) {
    	for (int k = 0; k <= j; k++) {
    	    dp[j][j][k] = (k + 1) * (k + 1);
    	}
    }
    	
    for (int l = 1; l < n; l++) {
    	for (int j = l; j < n; j++) {
    	    int i = j - l;
    	        
    	    for (int k = 0; k <= i; k++) {
    	        int res = (k + 1) * (k + 1) + dp[i + 1][j][0];
    	            
    	        for (int m = i + 1; m <= j; m++) {
    	            if (boxes[m] == boxes[i]) {
    	                res = Math.max(res, dp[i + 1][m - 1][0] + dp[m][j][k + 1]);
    	            }
    	        }
    	            
    	        dp[i][j][k] = res;
    	    }
    	}
    }
    
    return (n == 0 ? 0 : dp[0][n - 1][0]);
}
```

**`Side notes`**: In case you are curious, for the problem "**leetcode 312. Burst Balloons**", the external information to subarray `nums[i, j]` is the two numbers (denoted as `left` and `right`) adjacent to `nums[i]` and `nums[j]`, respectively. If we absorb this extra piece of information into the definition of `T(i, j)`, we have `T(i, j, left, right)` which represents the maximum coins obtained by bursting balloons of subarray `nums[i, j]` whose two adjacent numbers are `left` and `right`. The original problem will be `T(0, n - 1, 1, 1)` and the termination condition is `T(i, i, left, right) = left * right * nums[i]`. The recurrence relations will be: `T(i, j, left, right) = max(left * nums[k] * right + T(i, k - 1, left, nums[k]) + T(k + 1, j, nums[k], right))` where `i <= k <= j` (here we interpret it as that the balloon at index `k` is the last to be burst. Since all balloons can be the last one so we try each case and choose one that yields the maximum coins). For more details, refer to [dietpepsi \'s post](https://discuss.leetcode.com/topic/30746/share-some-analysis-and-explanations).
</p>


### Memoization DFS C++
- Author: waterbucket
- Creation Date: Sun Mar 26 2017 11:31:13 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 00:21:43 GMT+0800 (Singapore Standard Time)

<p>
Getting memory limit errors for the last input, so sad. I read some of the top submissions and found out the reason: I was using STL vector instead of a C array....

Thanks to one of the top submission, which used the same idea as me, I have cleaned my code.

=======================  Explanation ===========================
**First Attempt**
The initial thought is straightforward, try every possible removal and recursively search the rest. No doubt it will be a TLE answer. Obviously there are a lot of recomputations involved here. Memoization is the key then. But how to design the memory is tricky. I tried to use a string of 0s and 1s to indicate whether the box is removed or not, but still getting TLE. 

**One step further**
I think the problem of the approach above is that there are a lot of *unnecessary* computations (not recomputations). For example, if there is a formation of `ABCDAA`, we know the optimal way is `B->C->D->AAA`. On the other hand, if the formation is `BCDAA`, meaning that we couldn't find an `A` before `D`, we will simply remove `AA`, which will be the optimal solution for removing them. Note this is true only if `AA` is at the end of the array. With naive memoization approach, the program will search a lot of unnecessary paths, such as `C->B->D->AA`, `D->B->C->AA`.

Therefore, I designed the memoization matrix to be `memo[l][r][k]`, the largest number we can get using `l`th to `r`th (inclusive) boxes with k same colored boxes as `r`th box appended at the end. Example, `memo[l][r][3]` represents the solution for this setting: `[b_l, ..., b_r, A,A,A]` with `b_r==A`.

The transition function is to find the maximum among all `b_i==b_r` for `i=l,...,r-1`:

`memo[l][r][k] = max(memo[l][r][k], memo[l][i][k+1] + memo[i+1][r-1][0])`

Basically, if there is one `i` such that `b_i==b_r`, we partition the array into two: `[b_l, ..., b_i, b_r, A, ..., A]`, and `[b_{i+1}, ..., b_{r-1}]`. The solution for first one will be `memo[l][i][k+1]`, and the second will be `memo[i+1][r-1][0]`. Otherwise, we just remove the last k+1 boxes (including `b_r`) and search the best solution for `l`th to `r-1`th boxes. (One optimization here: make `r` as left as possible, this improved the running time from **250ms** to **35ms**)

The final solution is stored in `memo[0][n-1][0]` for sure. 

I didn't think about this question for a long time in the contest because the time is up. There will be a lot of room for time and space optimization as well. Thus, if you find any flaws or any improvements, please correct me.

```
class Solution {
public:
    int removeBoxes(vector<int>& boxes) {
        int n=boxes.size();
        int memo[100][100][100] = {0};
        return dfs(boxes,memo,0,n-1,0);
    }
    
    int dfs(vector<int>& boxes,int memo[100][100][100], int l,int r,int k){
        if (l>r) return 0;
        if (memo[l][r][k]!=0) return memo[l][r][k];

        while (r>l && boxes[r]==boxes[r-1]) {r--;k++;}
        memo[l][r][k] = dfs(boxes,memo,l,r-1,0) + (k+1)*(k+1);
        for (int i=l; i<r; i++){
            if (boxes[i]==boxes[r]){
                memo[l][r][k] = max(memo[l][r][k], dfs(boxes,memo,l,i,k+1) + dfs(boxes,memo,i+1,r-1,0));
            }
        }
        return memo[l][r][k];
    }
};
```
</p>


### Python, Fast DP with Explanation
- Author: awice
- Creation Date: Sun Mar 26 2017 13:14:34 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 23:44:07 GMT+0800 (Singapore Standard Time)

<p>
Let **A** be the array of boxes.  

One natural approach is to consider dp(i, j) = the answer for A[i: j+1].  But this isn't flexible enough for divide and conquer style strategies.  For example, with [1,2,2,2,1], we don't have enough information when investigating things like [1,2,2,2] and [1] separately.

Let dp(i, j, k) = the maximum value of removing boxes if we have k extra boxes of color A[i] to the left of A[i: j+1].  (We would have at most k < len(A) extra boxes.)  Let m <= j be the largest value so that A[i], A[i+1], ... A[m] are all the same color.  Because a^2 + b^2 < (a+b)^2, any block of contiguous boxes of the same color must be removed at the same time, so in fact dp(i, j, k) = dp(m, j, k+(m-i)).

Now, we could remove the k boxes we were carrying plus box A[i] (value: (k+1)**2), then remove the rest (value: dp(i+1, j, 0)).  Or, for any point m in [i+1, j] with A[i] == A[m], we could remove dp(i+1, m-1) first, then [m, j] would have k+1 extra boxes of color A[m] behind, which has value dp(m, j, k+1).

The "i, k = m, k + m - i" part skips order (m-i)*(j-i) calls to dp, and is necessary to get this kind of solution to pass in Python.
```
def removeBoxes(self, A):
    N = len(A)
    memo = [[[0]*N for _ in xrange(N) ] for _ in xrange(N) ]
    
    def dp(i, j, k):
        if i > j: return 0
        if not memo[i][j][k]:
            m = i
            while m+1 <= j and A[m+1] == A[i]:
                m += 1
            i, k = m, k + m - i
            ans = dp(i+1, j, 0) + (k+1) ** 2
            for m in xrange(i+1, j+1):
                if A[i] == A[m]:
                    ans = max(ans, dp(i+1, m-1, 0) + dp(m, j, k+1))
            memo[i][j][k] = ans
        return memo[i][j][k]
    
    return dp(0, N-1, 0)
```
</p>


