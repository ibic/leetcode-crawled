---
title: "Combination Sum III"
weight: 200
#id: "combination-sum-iii"
---
## Description
<div class="description">
<p>Find all valid combinations of <code>k</code> numbers that sum up to <code>n</code> such that the following conditions are true:</p>

<ul>
	<li>Only numbers <code>1</code> through <code>9</code> are used.</li>
	<li>Each number is used <strong>at most once</strong>.</li>
</ul>

<p>Return <em>a list of all possible valid combinations</em>. The list must not contain the same combination twice, and the combinations may be returned in any order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> k = 3, n = 7
<strong>Output:</strong> [[1,2,4]]
<strong>Explanation:</strong>
1 + 2 + 4 = 7
There are no other valid combinations.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> k = 3, n = 9
<strong>Output:</strong> [[1,2,6],[1,3,5],[2,3,4]]
<strong>Explanation:</strong>
1 + 2 + 6 = 9
1 + 3 + 5 = 9
2 + 3 + 4 = 9
There are no other valid combinations.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> k = 4, n = 1
<strong>Output:</strong> []
<strong>Explanation:</strong> There are no valid combinations. [1,2,1] is not valid because 1 is used twice.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> k = 3, n = 2
<strong>Output:</strong> []
<strong>Explanation:</strong> There are no valid combinations.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> k = 9, n = 45
<strong>Output:</strong> [[1,2,3,4,5,6,7,8,9]]
<strong>Explanation:</strong>
1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 = 45
​​​​​​​There are no other valid combinations.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= k &lt;= 9</code></li>
	<li><code>1 &lt;= n &lt;= 60</code></li>
</ul>

</div>

## Tags
- Array (array)
- Backtracking (backtracking)

## Companies
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Backtracking

**Intuition**

The problem asks us to come up with some fixed-length combinations that meet certain conditions.

To solve the problem, it would be beneficial to build a combination by hand.

If we represent the combination as an array, we then could fill the array **_one element at a time_**.

For example, given the input $$k=3$$ and $$n=9$$, _i.e._ the size of the combination is 3, and the sum of the digits in the combination should be 9.
Here are a few steps that we could do:

- 1). We could pick a digit for the **_first_** element in the combination.
Initially, the list of candidates is `[1, 2, 3, 4, 5, 6, 7, 8. 9]` for any element in the combination, as stated in the problem. 
Let us pick `1` as the first element. The current combination is `[1]`.

![first element](../Figures/216/216_element_I.png)

- 2). Now that we picked the first element, we have two more elements to fill in the final combination.
Before we proceed, let us review the conditions that we should fullfil for the next steps.

    - Since we've already picked the digit `1`, we should exclude the digit from the original candidate list for the remaining elements, in order to ensure that the combination does not contain any **_duplicate_** digits, as required in the problem.

    - In addition, the sum of the remaining two elements should be $$9 - 1 = 8$$.

- 3). Based on the above conditions, for the second element, we could have several choices.
Let us pick the digit `2`, which is not a duplicate of the first element, plus it does not exceed the desired sum (_i.e._ $$8$$) neither.
The combination now becomes `[1, 2]`.

![second element](../Figures/216/216_element_II.png)

- 4). Now for the third element, with all the constraints, it leaves us no choice but to pick the digit `6` as the final element in the combination of `[1, 2, 6]`.

![third element](../Figures/216/216_element_III.png)

- 5). As we mentioned before, for the second element, we could have several choices.
For instance, we could have picked the digit `3`, instead of the digit `2`. Eventually, it could _lead_ us to another solution as `[1, 3, 5]`.

- 6). As one can see, for each element in the combination, we could **_revisit_** our choices, and **_try out_** other possibilities to see if it leads to a valid solution.


If you have followed the above steps, it should become _evident_ that **_backtracking_** would be the technique that we could use to come up an algorithm for this problem.

![backtrack](../Figures/216/216_backtrack.png)

>Indeed, we could resort to _backtracking_, where we try to fill the combination **one element at a step**. Each choice we make at certain step might lead us to a final solution. If not, we simply revisit the choice and try out other choices, _i.e._ backtrack. 


**Algorithm**

There are many ways to implement a backtracking algorithm.
One could also refer to our [Explore card](https://leetcode.com/explore/learn/card/recursion-ii/472/backtracking/) where we give some examples of backtracking algorithms.

To implement the algorithm, one could literally follow the steps in the Intuition section.
However, we would like to highlight a key **_trick_** that we employed, in order to ensure the **_non-redundancy_** among the digits within a single combination, as well as the **_non-redundancy_** among the combinations. 

>The trick is that we pick the candidates **_in order_**.
We treat the candidate digits as a list with order, _i.e._ `[1, 2, 3, 4, 5, 6, 7, 8. 9]`.
At any given step, once we pick a digit, _e.g._ `6`, we will not consider any digits before the chosen digit for the following steps, _e.g._ the candidates are reduced down to `[7, 8, 9]`. 

With the above strategy, we could ensure that a digit will never be picked twice for the same combination.
Also, all the combinations that we come up with would be unique.

Here are some sample implementations based on the above ideas.

<iframe src="https://leetcode.com/playground/PBwLF7ik/shared" frameBorder="0" width="100%" height="500" name="PBwLF7ik"></iframe>



**Complexity Analysis**

Let $$K$$ be the number of digits in a combination.

- Time Complexity: $$\mathcal{O}(\frac{9! \cdot K}{(9-K)!})$$

    - In a worst scenario, we have to explore all potential combinations to the very end, _i.e._ the sum $$n$$ is a large number ($$n > 9 * 9$$). At the first step, we have $$9$$ choices, while at the second step, we have $$8$$ choices, so on and so forth.

    - The number of exploration we need to make in the worst case would be $$P(9, K) = \frac{9!}{(9-K)!}$$, assuming that $$K <= 9$$. By the way, $$K$$ cannot be greater than 9, otherwise we cannot have a combination whose digits are all unique.

    - Each exploration takes a constant time to process, except the last step where it takes $$\mathcal{O}(K)$$ time to make a copy of combination.

    - To sum up, the overall time complexity of the algorithm would be $$ \frac{9!}{(9-K)!} \cdot \mathcal{O}(K) = \mathcal{O}(\frac{9! \cdot K}{(9-K)!})$$.

- Space Complexity: $$\mathcal{O}(K)$$

    - During the backtracking, we used a list to keep the current combination, which holds up to $$K$$ elements, _i.e._ $$\mathcal{O}(K)$$.

    - Since we employed recursion in the backtracking, we would need some additional space for the function call stack, which could pile up to $$K$$ consecutive invocations, _i.e._ $$\mathcal{O}(K)$$.

    - Hence, to sum up, the overall space complexity would be $$\mathcal{O}(K)$$.

    - **Note that**, we did not take into account the space for the final results in the space complexity.


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple and clean Java code, backtracking.
- Author: jinwu
- Creation Date: Tue Oct 06 2015 03:49:25 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 23 2018 16:06:49 GMT+0800 (Singapore Standard Time)

<p>
     public List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> ans = new ArrayList<>();
        combination(ans, new ArrayList<Integer>(), k, 1, n);
        return ans;
    }

	private void combination(List<List<Integer>> ans, List<Integer> comb, int k,  int start, int n) {
		if (comb.size() == k && n == 0) {
			List<Integer> li = new ArrayList<Integer>(comb);
			ans.add(li);
			return;
		}
		for (int i = start; i <= 9; i++) {
			comb.add(i);
			combination(ans, comb, k, i+1, n-i);
			comb.remove(comb.size() - 1);
		}
	}
</p>


### My C++ solution, backtracking.
- Author: yushu
- Creation Date: Sun May 24 2015 14:36:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 03:46:45 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
      void combination(vector<vector<int>>& result, vector<int> sol, int k, int n) {
        if (sol.size() == k && n == 0) { result.push_back(sol); return ; }
        if (sol.size() < k) {
          for (int i = sol.empty() ? 1 : sol.back() + 1; i <= 9; ++i) {
            if (n - i < 0) break;
            sol.push_back(i);
            combination(result, sol, k, n - i);
            sol.pop_back();
          }
        }
      }
    
      vector<vector<int>> combinationSum3(int k, int n) {
        vector<vector<int>> result;
        vector<int> sol;
        combination(result, sol, k, n);
        return result;
      }
    };
</p>


### Easy to understand Python solution (backtracking).
- Author: OldCodingFarmer
- Creation Date: Sat Jul 18 2015 22:00:03 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 10 2020 22:48:33 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def combinationSum3(self, k, n):
        ret = []
        self.dfs(list(range(1, 10)), k, n, [], ret)
        return ret
    
    def dfs(self, nums, k, n, path, ret):
        if k < 0 or n < 0:
            return 
        if k == 0 and n == 0:
            ret.append(path)
        for i in range(len(nums)):
            self.dfs(nums[i+1:], k-1, n-nums[i], path+[nums[i]], ret)
```
</p>


