---
title: "Project Employees II"
weight: 1518
#id: "project-employees-ii"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Project</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| project_id  | int     |
| employee_id | int     |
+-------------+---------+
(project_id, employee_id) is the primary key of this table.
employee_id is a foreign key to <code>Employee</code> table.
</pre>

<p>Table:&nbsp;<code>Employee</code></p>

<pre>
+------------------+---------+
| Column Name      | Type    |
+------------------+---------+
| employee_id      | int     |
| name             | varchar |
| experience_years | int     |
+------------------+---------+
employee_id is the primary key of this table.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query that reports all the <strong>projects</strong>&nbsp;that have the most employees.</p>

<p>The query result format is in the following example:</p>

<pre>
Project table:
+-------------+-------------+
| project_id  | employee_id |
+-------------+-------------+
| 1           | 1           |
| 1           | 2           |
| 1           | 3           |
| 2           | 1           |
| 2           | 4           |
+-------------+-------------+

Employee table:
+-------------+--------+------------------+
| employee_id | name   | experience_years |
+-------------+--------+------------------+
| 1           | Khaled | 3                |
| 2           | Ali    | 2                |
| 3           | John   | 1                |
| 4           | Doe    | 2                |
+-------------+--------+------------------+

Result table:
+-------------+
| project_id  |
+-------------+
| 1           |
+-------------+
The first project has 3 employees while the second one has 2.</pre>

</div>

## Tags


## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple SQL solution
- Author: Merciless
- Creation Date: Sat Jun 08 2019 15:09:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 08 2019 15:09:16 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below
SELECT project_id
FROM project
GROUP BY project_id
HAVING COUNT(employee_id) =
(
    SELECT count(employee_id)
    FROM project
    GROUP BY project_id
    ORDER BY count(employee_id) desc
    LIMIT 1
)

</p>


### Easy Understand MySQL Solution
- Author: thatismyhouse
- Creation Date: Thu Aug 08 2019 06:09:22 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 08 2019 06:09:22 GMT+0800 (Singapore Standard Time)

<p>
```
select project_id
from project 
group by project_id
having count(employee_id) = 
     (select max(cnt) 
      from (select project_id, count(distinct employee_id) as cnt
            from project
            group by project_id) as t1)
;
```
</p>


### Simple MSSQL Solution using TOP WITH TIES
- Author: Negmat
- Creation Date: Fri Aug 09 2019 01:05:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 09 2019 01:05:22 GMT+0800 (Singapore Standard Time)

<p>
\'\'\'
SELECT TOP 1 WITH TIES project_id
FROM Project
GROUP BY project_id
ORDER BY COUNT(employee_id) DESC
\'\'\'
</p>


