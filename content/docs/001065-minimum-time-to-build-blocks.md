---
title: "Minimum Time to Build Blocks"
weight: 1065
#id: "minimum-time-to-build-blocks"
---
## Description
<div class="description">
<p>You are given a list of blocks, where <code>blocks[i] = t</code> means that the&nbsp;<code>i</code>-th block needs&nbsp;<code>t</code>&nbsp;units of time to be built. A block can only be built by exactly one worker.</p>

<p>A worker can either split into two workers (number of workers increases by one) or build a block then go home. Both decisions cost some time.</p>

<p>The time cost of spliting one worker into two workers is&nbsp;given as an integer <code>split</code>. Note that if two workers split at the same time, they split in parallel so the cost would be&nbsp;<code>split</code>.</p>

<p>Output the minimum time needed to build all blocks.</p>

<p>Initially, there is only <strong>one</strong> worker.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> blocks = [1], split = 1
<strong>Output:</strong> 1
<strong>Explanation: </strong>We use 1 worker to build 1 block in 1 time unit.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> blocks = [1,2], split = 5
<strong>Output:</strong> 7
<strong>Explanation: </strong>We split the worker into 2 workers in 5 time units then assign each of them to a block so the cost is 5 + max(1, 2) = 7.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> blocks = [1,2,3], split = 1
<strong>Output:</strong> 4
<strong>Explanation: </strong>Split 1 worker into 2, then assign the first worker to the last block and split the second worker into 2.
Then, use the two unassigned workers to build the first two blocks.
The cost is 1 + max(3, 1 + max(1, 2)) = 4.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= blocks.length &lt;= 1000</code></li>
	<li><code>1 &lt;= blocks[i] &lt;= 10^5</code></li>
	<li><code>1 &lt;= split &lt;= 100</code></li>
</ul>

</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python: O(n-log-n), using Huffman's Algorithm (priority queue) with explanation.
- Author: Hai_dee
- Creation Date: Sun Sep 22 2019 00:11:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 22 2019 01:02:57 GMT+0800 (Singapore Standard Time)

<p>
Code at end. Sorry in advance for typos, it\'s 2 am here in Australia, lol.

I initially though this was a dynamic programming question, and was even assuming I was aiming for O(n^2) because the max number of blocks is so low at 1000. Normally this means O(n^2). But after a little thinking, I realised this is not the best way to approach it at all.  We can do it greedily!

We can model this entire question as a binary tree that we need to construct with a minimum max depth cost. Each of the blocks is a leaf node, with a cost of its face value. And then each inner node will be of cost ```split```. nodes that are sitting at the same level represent work that is done in parallel. We know there will be len(blocks) - 1 of these inner nodes, so the question now is how can we construct the tree such that it has the minimum depth.

For example, a possible (not optimal) tree for the data set
[1, 2, 4, 7, 10] with split cost 3 is:
(Sorry for ascii art, 2 AM is too late at night to do this properly :) )

```
........3
...../....\
.....3.....\
../.....\....\
 3.......3....\
/..\..../..\...\
1...2...4...7...10

```

This tree has a maximum depth of 16 (3 -> 3 -> 3 -> 7).

So, how can we optimise the construction of this tree? Huffman\'s algorithm!

I\'m going to assume you\'re familiar with Huffman\'s algorithm. If not, google it or read this. Note that it\'s traditionally used for building compression codes, but there is no reason we can\'t also use it here.
https://www.geeksforgeeks.org/huffman-coding-greedy-algo-3/

Instead of actually building the whole tree, we can just keep track of the nodes the huffman algorithm still needs to consider, as the maximum depth below them. So put all the leaf node (blocks) onto a priority queue, and then repeatedly take the 2 smallest off and add ```split``` onto the biggest of the 2 (this is Huffman\'s algorithm, it\'s greedy) and put the result back onto the priority queue.

Once there is only one left, this is the depth of the root node, and we return it.

It is O(n-log-n) because we are making 2n - 1 insertions into a heap and n removals, and both heap insertion and removal have a cost of O(log-n). We drop the constants, giving a final cost of O(n-log-n).

And here is the code.

```
class Solution:
    def minBuildTime(self, blocks: List[int], split: int) -> int:
        heapq.heapify(blocks)
        while len(blocks) > 1:
            block_1 = heapq.heappop(blocks)
            block_2 = heapq.heappop(blocks)
            new_block = max(block_1, block_2) + split
            heapq.heappush(blocks, new_block)
        return blocks[0]
```

**Small improvement**: As kimS pointed out, we know that block_2 is the biggest, as the smallest came off the heap first. So we don\'t need that call to max, and we don\'t need to put block_1 into a variable. Thanks!
</p>


### [Java/C++/Python] Huffman's Algorithm
- Author: lee215
- Creation Date: Sun Sep 22 2019 00:19:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 22 2019 00:19:43 GMT+0800 (Singapore Standard Time)

<p>
I feel that I have seen Huffman\'s algorithm 3 times this month on Leetcode.
What is happening?

Time `O(NlogN)`, Space `O(N)`
<br>

**Java:**
```java
class Solution {
    public int minBuildTime(int[] blocks, int split) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        for (int b : blocks) pq.add(b);
        while (pq.size() > 1) {
            int x = pq.poll(), y = pq.poll();
            pq.add(y + split);
        }
        return pq.poll();
    }
}
```

**C++:**
```cpp
    int minBuildTime(vector<int>& blocks, int split) {
        priority_queue<int> pq;
        for (int b : blocks) pq.push(-b);
        while (pq.size() > 1) {
            int a = -pq.top(); pq.pop();
            int b = -pq.top(); pq.pop();
            pq.push(-(split + b));
        }
        return -pq.top();
    }
```

**Python:**
```python
    def minBuildTime(self, A, split):
        heapq.heapify(A)
        while len(A) > 1:
            x, y = heapq.heappop(A), heapq.heappop(A)
            heapq.heappush(A, y + split)
        return heapq.heappop(A)
```

</p>


### [Java] Huffman's Algorithm with Explanation Video
- Author: kelvinchandra1024
- Creation Date: Sun Sep 22 2019 01:54:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 22 2019 01:54:15 GMT+0800 (Singapore Standard Time)

<p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/pSW8wxvcEcM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>


