---
title: "Cells with Odd Values in a Matrix"
weight: 1196
#id: "cells-with-odd-values-in-a-matrix"
---
## Description
<div class="description">
<p>Given&nbsp;<code>n</code>&nbsp;and&nbsp;<code>m</code>&nbsp;which are the dimensions of a matrix initialized by zeros and given an array <code>indices</code>&nbsp;where <code>indices[i] = [ri, ci]</code>. For each pair of <code>[ri, ci]</code>&nbsp;you have to increment all cells in row <code>ri</code> and column <code>ci</code>&nbsp;by 1.</p>

<p>Return <em>the number of cells with odd values</em> in the matrix after applying the increment to all <code>indices</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/10/30/e1.png" style="width: 600px; height: 118px;" />
<pre>
<strong>Input:</strong> n = 2, m = 3, indices = [[0,1],[1,1]]
<strong>Output:</strong> 6
<strong>Explanation:</strong> Initial matrix = [[0,0,0],[0,0,0]].
After applying first increment it becomes [[1,2,1],[0,1,0]].
The final matrix will be [[1,3,1],[1,3,1]] which contains 6 odd numbers.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/10/30/e2.png" style="width: 600px; height: 150px;" />
<pre>
<strong>Input:</strong> n = 2, m = 2, indices = [[1,1],[0,0]]
<strong>Output:</strong> 0
<strong>Explanation:</strong> Final matrix = [[2,2],[2,2]]. There is no odd number in the final matrix.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 50</code></li>
	<li><code>1 &lt;= m &lt;= 50</code></li>
	<li><code>1 &lt;= indices.length &lt;= 100</code></li>
	<li><code>0 &lt;= indices[i][0] &lt;&nbsp;n</code></li>
	<li><code>0 &lt;= indices[i][1] &lt;&nbsp;m</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] 2 methods: time O(m * n + L)  and O(L) codes w/ comment and analysis.
- Author: rock
- Creation Date: Sun Nov 10 2019 12:01:44 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 14 2020 18:12:28 GMT+0800 (Singapore Standard Time)

<p>
**Method 1:**
1. Count the rows and columns that appear odd times;
2. Traverse all cells to get the answer.
```java
    public int oddCells(int n, int m, int[][] indices) {
        boolean[] oddRows = new boolean[n], oddCols = new boolean[m];
        for (int[] idx : indices) {
            oddRows[idx[0]] ^= true; // if row idx[0] appears odd times, it will correspond to true.
            oddCols[idx[1]] ^= true; // if column idx[1] appears odd times, it will correspond to true.
        }
        int cnt = 0;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                cnt += oddRows[i] ^ oddCols[j] ? 1 : 0; // only cell (i, j) with odd times count of row + column would get odd values.
            }
        }
        return cnt;
    }
```
```python
    def oddCells(self, n: int, m: int, indices: List[List[int]]) -> int:
        odd_rows, odd_cols = [False] * n, [False] * m
        for r, c in indices:
            odd_rows[r] ^= True
            odd_cols[c] ^= True
        return sum(ro ^ cl for ro in odd_rows for cl in odd_cols)
```

**Analysis:**

Time: O(m * n + L), space: O(m + n), where L = indices.length.

----

**Method 2:**
We actually only care about the number of rows and columns with odd times of increments respectively.
1. Count the rows and columns that appear odd times;
2. Compute the number of cells in aforementioned rows and columns respectively, then both deduct the cells located on odd rows and odd columns (they become evens, because odd + odd results even).
**Note:** Easier alternative way for 2 is `odd_cols * even_rows + even_cols * odd_rows` -- credit to **@lenchen1112**.
```java
    public int oddCells(int n, int m, int[][] indices) {
        boolean[] oddRows = new boolean[n], oddCols = new boolean[m];
        int cntRow = 0, cntCol = 0;
        for (int[] idx : indices) {
            oddRows[idx[0]] ^= true;
            oddCols[idx[1]] ^= true;
        }
        for (boolean r : oddRows)
            cntRow += r ? 1 : 0;
        for (boolean c : oddCols)
            cntCol += c ? 1 : 0;
        // return m * cntRow + n * cntCol - 2 * cntRow * cntCol;
        return (m - cntCol) * cntRow + (n - cntRow) * cntCol;
    }
```

```python
    def oddCells(self, n: int, m: int, indices: List[List[int]]) -> int:
        odd_rows, odd_cols = [False] * n, [False] * m
        for r, c in indices:
            odd_rows[r] ^= True
            odd_cols[c] ^= True
        # return m * sum(odd_rows) + n * sum(odd_cols) - 2 * sum(odd_rows) * sum(odd_cols)
        return (m - sum(odd_cols)) * sum(odd_rows) + (n - sum(odd_rows))* sum(odd_cols)
```

**Analysis:**

Time: O(L + m + n), space: O(m + n), where L = indices.length.

or optimize the above further as follows:
```java
    public int oddCells(int n, int m, int[][] indices) {
        boolean[] oddRows = new boolean[n], oddCols = new boolean[m];
        int cntRow = 0, cntCol = 0;
        for (int[] idx : indices) {
            oddRows[idx[0]] ^= true;
            oddCols[idx[1]] ^= true;
            cntRow += oddRows[idx[0]] ? 1 : -1;
            cntCol += oddCols[idx[1]] ? 1 : -1;
        }
        // return m * cntRow + n * cntCol - 2 * cntRow * cntCol;
        return (m - cntCol) * cntRow + (n - cntRow) * cntCol;
    }
```
```python
    def oddCells(self, n: int, m: int, indices: List[List[int]]) -> int:
        odd_rows, odd_cols, cntRow, cntCol = [False] * n, [False] * m, 0, 0
        for r, c in indices:
            odd_rows[r] ^= True
            odd_cols[c] ^= True
            cntRow += 1 if odd_rows[r] else -1 
            cntCol += 1 if odd_cols[c] else -1 
        # return m * cntRow + n * cntCol - 2 * cntRow * cntCol
        return (m - cntCol) * cntRow + (n - cntRow) * cntCol
```

**Analysis:**

Time: O(L), space: O(m + n), where L = indices.length.

----

Use `BitSet`, credit to **@mishak**.
```java
    public int oddCells(int n, int m, int[][] indices) {
        BitSet oddRows = new BitSet(n), oddCols = new BitSet(m);
        int cntRow = 0, cntCol = 0;
        for (int[] idx : indices) {
            oddRows.flip(idx[0]);
            oddCols.flip(idx[1]);
            cntRow += oddRows.get(idx[0]) ? 1 : -1;
            cntCol += oddCols.get(idx[1]) ? 1 : -1;
        }
        return (m - cntCol) * cntRow + (n - cntRow) * cntCol;   
    }
```

</p>


### [C++] 0ms, 9.2MB. Faster than 100% and uses lesser memory than 100% submissions
- Author: 97amarnathk
- Creation Date: Tue Nov 12 2019 03:09:07 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 12 2019 03:10:41 GMT+0800 (Singapore Standard Time)

<p>
This can be done in **O(m+n)** time and **O(m+n)** space 
Firstly notice that the order in which we add 1 does not matter. You could add 1 to all rows and then proceed to adding 1 to all columns.

Now suppose you add 1 to the same row twice, the resultant elements in that row are even. So applying the operation to the same row/col is as good as not applying anything there. 

Applying operation to a same row/col any odd number of times is as good as applying the operation only once.

So now suppose there are `r` such rows where you apply the operation odd number of times. And `c` such cols where you apply operation odd number of times.

Then

1. All `m` elements in each row are odd => `r*m`
2. All `n` elements in each row are odd => `c*n`
3. You double count `r*c` elements => `-1 * r * c`
4. Also, these `r*c` elements are even => `-1 * r * c`

So total odd elements is `r*m + c*n - 2*r*c`


```
class Solution {
public:
    int oddCells(int n, int m, vector<vector<int>>& indices) {
        vector<bool> N(n, false);
        vector<bool> M(m, false);
        
        for(int i=0; i<indices.size(); i++) {
            N[indices[i][0]] = N[indices[i][0]]^true;
            M[indices[i][1]] = M[indices[i][1]]^true;
        }
        
        int r = 0;
        int c = 0;
        
        for(int i=0; i<n; i++) {
            if(N[i])
                r++;
        }
        
        for(int j=0; j<m; j++) {
            if(M[j])
                c++;
        }
        
        return  r*m + c*n - 2*r*c;
    }
};
```
</p>


### Simple Fast Java Solution (with explanation)
- Author: vakulb
- Creation Date: Sun Nov 17 2019 10:33:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 10:40:31 GMT+0800 (Singapore Standard Time)

<p>
Steps:
1) Count how many times each row and column is incremented using row and column array.
2) Add row and col value to get corresponding array element value.
3) If it\'s odd increment counter


```
class Solution {
    public int oddCells(int n, int m, int[][] indices) {
        int count = 0;
        int row[] = new int [n];
        int col[] = new int [m];
        for(int x[] : indices)
        {
            row[x[0]]++;
            col[x[1]]++;
        }    
        for(int i=0;i<n;i++)
            for(int j=0;j<m;j++)
            {
                if((row[i]+col[j])%2!=0)
                    count++;
            }        
        return count;
    }
}
```
</p>


