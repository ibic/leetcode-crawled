---
title: "Wiggle Sort"
weight: 263
#id: "wiggle-sort"
---
## Description
<div class="description">
<p>Given an unsorted array <code>nums</code>, reorder it <b>in-place</b> such that <code>nums[0] &lt;= nums[1] &gt;= nums[2] &lt;= nums[3]...</code>.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b> <code>nums = [3,5,2,1,6,4]</code>
<b>Output:</b> One possible answer is [3,5,1,6,2,4]</pre>

</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Airbnb - 2 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach #1 (Sorting) [Accepted]

The obvious solution is to just sort the array first, then swap elements pair-wise starting from the second element. For example:

       [1, 2, 3, 4, 5, 6]
           ↑  ↑  ↑  ↑
           swap  swap

    => [1, 3, 2, 5, 4, 6]

```java
public void wiggleSort(int[] nums) {
    Arrays.sort(nums);
    for (int i = 1; i < nums.length - 1; i += 2) {
        swap(nums, i, i + 1);
    }
}

private void swap(int[] nums, int i, int j) {
    int temp = nums[i];
    nums[i] = nums[j];
    nums[j] = temp;
}
```

**Complexity analysis**

* Time complexity : $$O(n \log n)$$.
The entire algorithm is dominated by the sorting step, which costs $$O(n \log n)$$ time to sort $$n$$ elements.

* Space complexity : $$O(1)$$. Space depends on the sorting implementation which, usually, costs $$O(1)$$ auxiliary space if `heapsort` is used.

---
#### Approach #2 (One-pass Swap) [Accepted]

Intuitively, we should be able to reorder it in one-pass. As we iterate through the array, we compare the current element to its next element and if the order is incorrect, we swap them.

```java
public void wiggleSort(int[] nums) {
    boolean less = true;
    for (int i = 0; i < nums.length - 1; i++) {
        if (less) {
            if (nums[i] > nums[i + 1]) {
                swap(nums, i, i + 1);
            }
        } else {
            if (nums[i] < nums[i + 1]) {
                swap(nums, i, i + 1);
            }
        }
        less = !less;
    }
}
```

We could shorten the code further by compacting the condition to a single line. Also observe the boolean value of `less` actually depends on whether the index is even or odd.

```java
public void wiggleSort(int[] nums) {
    for (int i = 0; i < nums.length - 1; i++) {
        if (((i % 2 == 0) && nums[i] > nums[i + 1])
                || ((i % 2 == 1) && nums[i] < nums[i + 1])) {
            swap(nums, i, i + 1);
        }
    }
}
```

Here is another amazing solution by @StefanPochmann who came up with [originally here](https://leetcode.com/discuss/57113/java-o-n-solution?show=57192#a57192).

```java
public void wiggleSort(int[] nums) {
    for (int i = 0; i < nums.length - 1; i++) {
        if ((i % 2 == 0) == (nums[i] > nums[i + 1])) {
            swap(nums, i, i + 1);
        }
    }
}
```

**Complexity analysis**

* Time complexity : $$O(n)$$.
In the worst case we swap at most $$n \over 2$$ times. An example input is `[2,1,3,1,4,1]`.

* Space complexity : $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(N) solution
- Author: WalkerIX
- Creation Date: Thu Sep 10 2015 12:35:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 02:06:36 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public void wiggleSort(int[] nums) {
            for(int i=0;i<nums.length;i++)
                if(i%2==1){
                   if(nums[i-1]>nums[i]) swap(nums, i);
                }else if(i!=0 && nums[i-1]<nums[i]) swap(nums, i);
        }
        public void swap(int[] nums, int i){
              int tmp=nums[i];
              nums[i]=nums[i-1];
              nums[i-1]=tmp;
        }
    }
</p>


### My explanations of the best voted Algo
- Author: ofLucas
- Creation Date: Thu Apr 14 2016 19:35:09 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 02:26:12 GMT+0800 (Singapore Standard Time)

<p>
I have to say nobody explains the sufficiency of the following algo:

> The final sorted nums needs to satisfy two conditions:
> 
> If i is odd, then nums[i] >= nums[i - 1];
> 
> If i is even, then nums[i] <= nums[i - 1].
> 
> The code is just to fix the orderings of nums that do not satisfy 1
> and 2.

(from https://leetcode.com/discuss/57120/4-lines-o-n-c)

why is this greedy solution can ensure previous sequences and coming sequences W.R.T position i wiggled? 

My explanation is recursive, 

suppose nums[0 .. i - 1] is wiggled, for position i:

if i is odd, we already have, nums[i - 2] >= nums[i - 1],

if nums[i - 1] <= nums[i], then we does not need to do anything, its already wiggled.

if nums[i - 1] > nums[i], then we swap element at i -1 and i. Due to previous wiggled elements (nums[i - 2] >= nums[i - 1]), we know after swap the sequence is ensured to be nums[i - 2] > nums[i - 1] < nums[i], which is wiggled.

similarly,

if i is even, we already have, nums[i - 2] <= nums[i - 1],

if nums[i - 1] >= nums[i], pass

if nums[i - 1] <  nums[i], after swap, we are sure to have wiggled nums[i - 2] < nums[i - 1] > nums[i].

The same recursive solution applies to all the elements in the sequence, ensuring the algo success.

    public void wiggleSort(int[] nums) {
        for (int i = 1; i < nums.length; i++)
            if (((i & 1) == 0) == (nums[i - 1] < nums[i])) xwap(nums, i);
    }
    
    private void xwap(int[] a, int i) {
        int t = a[i]; a[i] = a[i - 1]; a[i - 1] = t;
    }
</p>


### 4-lines O(n) C++
- Author: jianchao-li
- Creation Date: Thu Sep 10 2015 12:56:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 13:00:36 GMT+0800 (Singapore Standard Time)

<p>
The final sorted `nums` needs to satisfy two conditions:

 1. If `i` is odd, then `nums[i] >= nums[i - 1]`;
 2. If `i` is even, then `nums[i] <= nums[i - 1]`.

The code is just to fix the orderings of `nums` that do not satisfy 1 and 2.

    class Solution {
    public: 
        void wiggleSort(vector<int>& nums) {
            int n = nums.size();
            for (int i = 1; i < n; i++)
                if (((i & 1) && nums[i] < nums[i - 1]) || (!(i & 1) && nums[i] > nums[i - 1]))
                    swap(nums[i], nums[i - 1]);
        } 
    };
</p>


