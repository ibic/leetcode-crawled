---
title: "Power of Three"
weight: 309
#id: "power-of-three"
---
## Description
<div class="description">
<p>Given an integer, write a function to determine if it is a power of three.</p>

<p><b>Example 1:</b></p>

<pre>
<strong>Input:</strong> 27
<strong>Output:</strong> true
</pre>

<p><b>Example 2:</b></p>

<pre>
<strong>Input:</strong> 0
<strong>Output:</strong> false</pre>

<p><b>Example 3:</b></p>

<pre>
<strong>Input:</strong> 9
<strong>Output:</strong> true</pre>

<p><b>Example 4:</b></p>

<pre>
<strong>Input:</strong> 45
<strong>Output:</strong> false</pre>

<p><b>Follow up:</b><br />
Could you do it without using any loop / recursion?</p>
</div>

## Tags
- Math (math)

## Companies
- Goldman Sachs - 2 (taggedByAdmin: false)
- Hulu - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)

## Official Solution
[TOC]


## Solution

In this article we will look into ways of speeding up simple computations and why that is useful in practice.
<br>
<br>

---
#### Approach 1: Loop Iteration

One simple way of finding out if a number `n` is a power of a number `b` is to keep dividing `n` by `b` as long as the remainder is **0**. This is because we can write

$$
\begin{aligned}
n &= b^x \\
n &= b \times b \times \ldots \times b
\end{aligned}
$$

Hence it should be possible to divide `n` by `b` `x` times, every time with a remainder of **0** and the end result to be **1**.

<iframe src="https://leetcode.com/playground/ojoAnJXy/shared" frameBorder="0" width="100%" height="276" name="ojoAnJXy"></iframe>

Notice that we need a guard to check that `n != 0`, otherwise the while loop will never finish. For negative numbers, the algorithm does not make sense, so we will include this guard as well.

**Complexity Analysis**

* Time complexity : $$O(\log_b(n))$$. In our case that is $$O(\log_3n)$$. The number of divisions is given by that logarithm.

* Space complexity : $$O(1)$$. We are not using any additional memory.
<br>
<br>

---
#### Approach 2: Base Conversion

In Base 10, all powers of 10 start with the digit **1** and then are followed only by **0** (e.g. 10, 100, 1000). This is true for other bases and their respective powers. For instance in *base 2*, the representations of $$10_2$$, $$100_2$$ and $$1000_2$$ are $$2_{10}$$, $$4_{10}$$ and $$8_{10}$$ respectively. Therefore if we convert our number to base 3 and the representation is of the form 100...0, then the number is a power of 3.

**Proof**

Given the base 3 representation of a number as the array `s`, with the least significant digit on index 0, the formula for converting from base **3** to base **10** is:

$$
\sum_{i=0}^{len(s) - 1} s[i] * 3^{i}
$$

Therefore, having just one digit of **1** and everything else **0** means the number is a power of 3.

**Implementation**

All we need to do is convert [^note-4] the number to *base 3* and check if it is written as a leading **1** followed by all **0**.

A couple of built-in Java functions will help us along the way.

<iframe src="https://leetcode.com/playground/mswCj3De/shared" frameBorder="0" width="100%" height="72" name="mswCj3De"></iframe>

The code above converts `number` into base `base` and returns the result as a `String`. For example, `Integer.toString(5, 2) == "101"` and `Integer.toString(5, 3) == "12"`.

<iframe src="https://leetcode.com/playground/T6CmNK28/shared" frameBorder="0" width="100%" height="72" name="T6CmNK28"></iframe>

The code above checks if a certain **Regular Expression** [^note-2] pattern exists inside a string. For instance the above will return true if the substring "123" exists inside the string `myString`.

<iframe src="https://leetcode.com/playground/mujLkBaw/shared" frameBorder="0" width="100%" height="72" name="mujLkBaw"></iframe>

We will use the regular expression above for checking if the string starts with **1** `^1`, is followed by zero or more **0**s `0*` and contains nothing else `＄`.

<iframe src="https://leetcode.com/playground/Vg5V7RYp/shared" frameBorder="0" width="100%" height="140" name="Vg5V7RYp"></iframe>

**Complexity Analysis**

* Time complexity : $$O(\log_3n)$$.

    Assumptions:

    * `Integer.toString()` - Base conversion is generally implemented as a repeated division. The complexity of  should be similar to our Approach 1: $$O(\log_3n)$$.
    * `String.matches()` - Method iterates over the entire string. The number of digits in the base 3 representation of `n` is $$O(\log_3n)$$.

* Space complexity : $$O(\log_3n)$$.

    We are using two additional variables,

    * The string of the base 3 representation of the number (size $$\log_3n$$)
    * The string of the regular expression (constant size)
<br>
<br>

---
#### Approach 3: Mathematics

We can use mathematics as follows

$$
n = 3^i \\
i = \log_3(n) \\
i = \frac{\log_b(n)}{\log_b(3)}
$$

`n` is a power of three if and only if `i` is an integer. In Java, we check if a number is an integer by taking the decimal part (using `% 1`) and checking if it is 0.

<iframe src="https://leetcode.com/playground/rGU5MG2p/shared" frameBorder="0" width="100%" height="140" name="rGU5MG2p"></iframe>

**Common pitfalls**

This solution is problematic because we start using `double`s, which means we are subject to precision errors. This means, we should never use `==` when comparing `double`s. That is because the result of `Math.log10(n) / Math.log10(3)` could be `5.0000001` or `4.9999999`. This effect can be observed by using the function `Math.log()` instead of `Math.log10()`.

In order to fix that, we need to compare the result against an `epsilon`.

<iframe src="https://leetcode.com/playground/eVP3xfwb/shared" frameBorder="0" width="100%" height="72" name="eVP3xfwb"></iframe>

**Complexity Analysis**

* Time complexity : $$Unknown$$ The expensive operation here is `Math.log`, which upper bounds the time complexity of our algorithm. The implementation is dependent on the language we are using and the compiler [^note-3]

* Space complexity : $$O(1)$$. We are not using any additional memory. The `epsilon` variable can be inlined.
<br>
<br>

---
#### Approach 4: Integer Limitations

An important piece of information can be deduced from the function signature

<iframe src="https://leetcode.com/playground/2s2CCD3x/shared" frameBorder="0" width="100%" height="0" name="2s2CCD3x"></iframe>

In particular, `n` is of type `int`. In Java, this means it is a 4 byte, signed integer [ref]. The maximum value of this data type is **2147483647**. Three ways of calculating this value are

- [Google](http://stackoverflow.com/questions/15004944/max-value-of-integer)
- ```System.out.println(Integer.MAX_VALUE);```
- MaxInt = $$\frac{ 2^{32} }{2} - 1$$ since we use 32 bits to represent the number, half of the range is used for negative numbers and 0 is part of the positive numbers

Knowing the limitation of `n`, we can now deduce that the maximum value of `n` that is also a power of three is **1162261467**. We calculate this as:

$$
3^{\lfloor{}\log_3{MaxInt}\rfloor{}} = 3^{\lfloor{}19.56\rfloor{}} = 3^{19} = 1162261467
$$

Therefore, the possible values of `n` where we should return `true` are $$3^0$$, $$3^1$$ ... $$3^{19}$$. Since 3 is a prime number, the only divisors of $$3^{19}$$ are $$3^0$$, $$3^1$$ ... $$3^{19}$$, therefore all we need to do is divide $$3^{19}$$ by `n`. A remainder of **0** means `n` is a divisor of $$3^{19}$$ and therefore a power of three.

<iframe src="https://leetcode.com/playground/P5BpBmpB/shared" frameBorder="0" width="100%" height="140" name="P5BpBmpB"></iframe>

**Complexity Analysis**

* Time complexity : $$O(1)$$. We are only doing one operation.

* Space complexity : $$O(1)$$. We are not using any additional memory.
<br>
<br>

---
## Performance Measurements

Single runs of the function make it is hard to accurately measure the difference of the two solutions. On LeetCode, on the *Accepted Solutions Runtime Distribution* page, all solutions being between `15 ms` and `20 ms`. For completeness, we have proposed the following benchmark to see how the two solutions differ.

**Java Benchmark Code**

<iframe src="https://leetcode.com/playground/7bpZrVLY/shared" frameBorder="0" width="100%" height="174" name="7bpZrVLY"></iframe>

In the table below, the values are in seconds.

| Iterations | $$10^6$$ | $$10^7$$ | $$10^8$$ | $$10^9$$ | $$Maxint$$ |
|:---:|:---:|:---:|:---:|:---:|:---:|
| Java Approach 1: (Naive) | 0.04 | 0.07 | 0.30 | 2.47 | 5.26 |
| Java Approach 2: (Strings) | 0.68 | 4.02 | 38.90 | 409.16 | 893.89 |
| Java Approach 3: (Logarithms) | 0.09 | 0.50 | 4.59 | 45.53 | 97.50 |
| Java Approach 4: (Fast) | 0.04 | 0.06 | 0.08 | 0.41 | 0.78 |

As we can see, for small values of N, the difference is not noticeable, but as we do more iterations and the values of `n` passed to `isPowerOfThree()` grow, we see significant boosts in performance for Approach 4.
<br>
<br>

---
## Conclusion

Simple optimizations like this might seem negligible, but historically, when computation power was an issue, it allowed certain computer programs (such as Quake 3 [^note-1]) possible.
<br>
<br>

---
## References

[^note-1]: [https://en.wikipedia.org/wiki/Fast_inverse_square_root](https://en.wikipedia.org/wiki/Fast_inverse_square_root)
[^note-2]: [https://en.wikipedia.org/wiki/Regular_expression](https://en.wikipedia.org/wiki/Regular_expression)
[^note-3]: [http://developer.classpath.org/doc/java/lang/StrictMath-source.html](http://developer.classpath.org/doc/java/lang/StrictMath-source.html)
[^note-4]: [http://www.cut-the-knot.org/recurrence/conversion.shtml](http://www.cut-the-knot.org/recurrence/conversion.shtml)

Analysis written by: [@aicioara](http://andrei.cioara.me)

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 1 line java solution without loop / recursion
- Author: Nkeys
- Creation Date: Tue Feb 02 2016 18:47:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 10:44:36 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
    public boolean isPowerOfThree(int n) {
        // 1162261467 is 3^19,  3^20 is bigger than int  
        return ( n>0 &&  1162261467%n==0);
    }
}
</p>


### ** A summary of `all` solutions (new method included at 15:30pm Jan-8th)
- Author: ElementNotFoundException
- Creation Date: Fri Jan 08 2016 12:29:50 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 05:19:04 GMT+0800 (Singapore Standard Time)

<p>
Well, this problem doesn't seem to be quite interesting or worthwhile to think about at a first glance. I had the same feeling at the beginning. However, after seeing a couple of posts, I saw a couple of interesting ways. So here is a summary post and hope you learn something from others' solutions.

Two trivial solutions first: 
#Recursive Solution#

    public boolean isPowerOfThree(int n) {
        return n>0 && (n==1 || (n%3==0 && isPowerOfThree(n/3)));
    }

#Iterative Solution#

**update following Stefan's answer below:**

    public boolean isPowerOfThree(int n) {
        if(n>1)
            while(n%3==0) n /= 3;
        return n==1;
    }

**my original code:**
    public boolean isPowerOfThree(int n) {
        while(n>1) {
            if(n%3!=0) return false;
            n /= 3;
        }
        return n<=0 ? false : true;
    }

#It's all about MATH...#

**Method 1**

Find the maximum integer that is a power of 3 and check if it is a multiple of the given input. ([related post][1])

    public boolean isPowerOfThree(int n) {
        int maxPowerOfThree = (int)Math.pow(3, (int)(Math.log(0x7fffffff) / Math.log(3)));
        return n>0 && maxPowerOfThree%n==0;
    }

Or simply hard code it since we know `maxPowerOfThree = 1162261467`:

    public boolean isPowerOfThree(int n) {
        return n > 0 && (1162261467 % n == 0);
    }

It is worthwhile to mention that Method 1 works only when the base is prime. For example, we cannot use this algorithm to check if a number is a power of 4 or 6 or any other composite number.

**Method 2**

 If `log10(n) / log10(3)` returns an int (more precisely, a double but has 0 after decimal point), then n is a power of 3. ([original post][2]). But **be careful here**, you cannot use `log` (natural log) here, because it will generate round off error for `n=243`. This is more like a coincidence. I mean when `n=243`, we have the following results:

    log(243) = 5.493061443340548    log(3) = 1.0986122886681098
       ==> log(243)/log(3) = 4.999999999999999
   
    log10(243) = 2.385606273598312    log10(3) = 0.47712125471966244
       ==> log10(243)/log10(3) = 5.0

This happens because `log(3)` is actually slightly larger than its true value due to round off, which makes the ratio smaller. 

    public boolean isPowerOfThree(int n) {
        return (Math.log10(n) / Math.log10(3)) % 1 == 0;
    }

**Method 3** [related post][3]

    public boolean isPowerOfThree(int n) {
        return n==0 ? false : n==Math.pow(3, Math.round(Math.log(n) / Math.log(3)));
    }

**Method 4** [related post][4]

    public boolean isPowerOfThree(int n) {
        return n>0 && Math.abs(Math.log10(n)/Math.log10(3)-Math.ceil(Math.log10(n)/Math.log10(3))) < Double.MIN_VALUE;
    }

**`Cheating` Method**

This is not really a good idea in general. But for such kind of `power` questions, if we need to check many times, it might be a good idea to store the desired powers into an array first. ([related post][5])

    public boolean isPowerOfThree(int n) {
        int[] allPowerOfThree = new int[]{1, 3, 9, 27, 81, 243, 729, 2187, 6561, 19683, 59049, 177147, 531441, 1594323, 4782969, 14348907, 43046721, 129140163, 387420489, 1162261467};
        return Arrays.binarySearch(allPowerOfThree, n) >= 0;
    }

or even better with HashSet:

    public boolean isPowerOfThree(int n) {
        HashSet<Integer> set = new HashSet<>(Arrays.asList(1, 3, 9, 27, 81, 243, 729, 2187, 6561, 19683, 59049, 177147, 531441, 1594323, 4782969, 14348907, 43046721, 129140163, 387420489, 1162261467));
        return set.contains(n);
    }

#New Method Included at 15:30pm Jan-8th#

**Radix-3** [original post][6]

The idea is to convert the original number into radix-3 format and check if it is of format `10*` where `0*` means `k` zeros with `k>=0`.

    public boolean isPowerOfThree(int n) {
        return Integer.toString(n, 3).matches("10*");
    }


----------


Any other interesting solutions?

If you are interested in my other posts, please feel free to check my Github page here: [https://github.com/F-L-A-G/Algorithms-in-Java][7]


  [1]: https://leetcode.com/discuss/78500/math-solution
  [2]: https://leetcode.com/discuss/78495/my-one-line-java-solution
  [3]: https://leetcode.com/discuss/78481/java-one-line-solution-using-math-knowledge
  [4]: https://leetcode.com/discuss/78531/one-line-math-solution-python-code
  [5]: https://leetcode.com/discuss/78492/one-lined-python-solution-without-using-any-loop-recursion
  [6]: https://leetcode.com/discuss/78708/ternary-number-solution
  [7]: https://github.com/F-L-A-G/Algorithms-in-Java
</p>


### Without log and O(1).
- Author: notbad123456
- Creation Date: Mon Mar 07 2016 12:51:05 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 21:44:56 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        int const Max3PowerInt = 1162261467; // 3^19, 3^20 = 3486784401 > MaxInt32
        int const MaxInt32 = 2147483647; // 2^31 - 1
        bool isPowerOfThree(int n) {
            if (n <= 0 || n > Max3PowerInt) return false;
            return Max3PowerInt % n == 0;
        }
    };
 
Typically, Log(x, y) is not O(1), it should be O(ln(N)), which just hides the loop/recursion .
</p>


