---
title: "Path with Maximum Gold"
weight: 1173
#id: "path-with-maximum-gold"
---
## Description
<div class="description">
<p>In a gold mine <code>grid</code>&nbsp;of size <code>m * n</code>,&nbsp;each cell in this mine has an integer representing the amount of gold&nbsp;in that cell,&nbsp;<code>0</code> if it is empty.</p>

<p>Return the maximum amount of gold you&nbsp;can collect under the conditions:</p>

<ul>
	<li>Every time you are located in a cell you will collect all the gold in that cell.</li>
	<li>From your position you can walk one step to the left, right, up or down.</li>
	<li>You can&#39;t visit the same cell more than once.</li>
	<li>Never visit a cell with&nbsp;<code>0</code> gold.</li>
	<li>You can start and stop collecting gold from&nbsp;<strong>any </strong>position in the grid that has some gold.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> grid = [[0,6,0],[5,8,7],[0,9,0]]
<strong>Output:</strong> 24
<strong>Explanation:</strong>
[[0,6,0],
 [5,8,7],
 [0,9,0]]
Path to get the maximum gold, 9 -&gt; 8 -&gt; 7.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,0,7],[2,0,6],[3,4,5],[0,3,0],[9,0,20]]
<strong>Output:</strong> 28
<strong>Explanation:</strong>
[[1,0,7],
 [2,0,6],
 [3,4,5],
 [0,3,0],
 [9,0,20]]
Path to get the maximum gold, 1 -&gt; 2 -&gt; 3 -&gt; 4 -&gt; 5 -&gt; 6 -&gt; 7.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= grid.length,&nbsp;grid[i].length &lt;= 15</code></li>
	<li><code>0 &lt;= grid[i][j] &lt;= 100</code></li>
	<li>There are at most <strong>25&nbsp;</strong>cells containing gold.</li>
</ul>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Goldman Sachs - 5 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Google - 7 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Short DFS
- Author: PhoenixDD
- Creation Date: Sun Oct 06 2019 12:01:29 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 16 2019 10:51:07 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
Since the count of cells is at maximum 15x15 and we know at maximum there can be 25 cells with gold, we can perform a simple DFS starting with each cell with gold and find the maximum possible gold considering each step as a new start. We can then return the maximum gold that can we aquired of all starting points.

**Solution**
```c++
static vector<int> directions={0,1,0,-1,0};
class Solution {
public:
    int dfs(vector<vector<int>> &grid,int i,int j)
    {
        int x,y,temp=grid[i][j],result=0;
        grid[i][j]=0;
        for(int d=0;d<4;d++)                    //Try all 4 possible directions
        {
            x=i+directions[d],y=j+directions[d+1];
            if(x>=0&&y>=0&&x<grid.size()&&y<grid[x].size()&&grid[x][y]!=0)
                result=max(result,grid[x][y]+dfs(grid,x,y));  //Set grid[i][j] to 0 to mark as visited before next dfs and reset the values after the dfs ends
        }
        grid[i][j]=temp;
        return result;
    }
    int getMaximumGold(vector<vector<int>>& grid) 
    {
        int result=0;
        for(int i=0;i<grid.size();i++)
            for(int j=0;j<grid[i].size();j++)
                if(grid[i][j]>0)
                    result=max(grid[i][j]+dfs(grid,i,j),result);   //Set grid[i][j] to 0 to mark this cell as visited and reset after the dfs ends.
        return result;
    }
};
```

**Complexity**
`n` being the number of elements in matrix.
Space: `O(n)` for the algorithm. O(1) for this question. Since the depth of recursion can go upto max `25` cells
Time: `O(4^n)` for the algorithm  (Although I would argue that complexity is O(3^n) as each cell after the first would just have 3 choices at max.), O(1) for this question. Since the question states that there are at most `25` cells with gold.
</p>


### [Java/C++/Python] DFS Backtracking - Clean code - O(4*3^k)
- Author: hiepit
- Creation Date: Sun Oct 06 2019 12:50:05 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 14 2020 23:52:12 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
The idea is simple, using backtracking to try all possible paths and return the path with maximum gold.

**Complexity**
- Time: `O(4 * 3^k)`, where `k <= 25` is the number of cells that have gold.
Because the first cell has up to 4 choices, the (k-1) remain cells have up to 3 choices. And we make k different gold cells as first cell. So k * 4\*3^(k-1) = 4 * 3^k
- Test Number of Operations:
I tested on worst case 5x5 matrix full 25 golds like below:
`{{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}}`
 And it takes **12.241.693 times findMaxGold()** function is called 
Check IDE Online: https://ideone.com/10n6TI
- Space: `O(k)`, it\'s the DFS stack depth.

**Java**
```java
class Solution {
    public int getMaximumGold(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        int maxGold = 0;
        for (int r = 0; r < m; r++)
            for (int c = 0; c < n; c++)
                maxGold = Math.max(maxGold, findMaxGold(grid, m, n, r, c));
        return maxGold;
    }
    int[] DIR = new int[]{0, 1, 0, -1, 0};
    int findMaxGold(int[][] grid, int m, int n, int r, int c) {
        if (r < 0 || r == m || c < 0 || c == n || grid[r][c] == 0) return 0;
        int origin = grid[r][c];
        grid[r][c] = 0; // mark as visited
        int maxGold = 0;
        for (int i = 0; i < 4; i++)
            maxGold = Math.max(maxGold, findMaxGold(grid, m, n, DIR[i] + r, DIR[i+1] + c));
        grid[r][c] = origin; // backtrack
        return maxGold + origin;
    }
}
```

**C++**
```c++
class Solution {
public:
    int getMaximumGold(vector<vector<int>>& grid) {
        int m = grid.size(), n = grid[0].size();
        int maxGold = 0;
        for (int r = 0; r < m; r++)
            for (int c = 0; c < n; c++)
                maxGold = max(maxGold, findMaxGold(grid, m, n, r, c));
        return maxGold;
    }
    int DIR[5] = {0, 1, 0, -1, 0};
    int findMaxGold(vector<vector<int>>& grid, int m, int n, int r, int c) {
        if (r < 0 || r == m || c < 0 || c == n || grid[r][c] == 0) return 0;
        int origin = grid[r][c];
        grid[r][c] = 0; // mark as visited
        int maxGold = 0;
        for (int i = 0; i < 4; i++)
            maxGold = max(maxGold, findMaxGold(grid, m, n, DIR[i] + r, DIR[i + 1] + c));
        grid[r][c] = origin; // backtrack
        return maxGold + origin;
    }
};
```

**Python 3**
```python
class Solution:
    def getMaximumGold(self, grid: List[List[int]]) -> int:
        def findMaxGold(r: int, c: int) -> int:
            if r < 0 or r == m or c < 0 or c == n or grid[r][c] == 0: return 0
            origin = grid[r][c]
            grid[r][c] = 0  # mark as visited
            maxGold = 0
            for nr, nc in ((r, c + 1), (r, c - 1), (r + 1, c), (r - 1, c)):
                maxGold = max(findMaxGold(nr, nc), maxGold)
            grid[r][c] = origin  # backtrack
            return maxGold + origin

        m, n = len(grid), len(grid[0])
        return max(findMaxGold(r, c) for c in range(n) for r in range(m))
```
</p>


### [Java/Python 3] DFS and BFS w/ comment, brief explanation and analysis.
- Author: rock
- Creation Date: Sun Oct 06 2019 12:10:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 15 2020 02:44:16 GMT+0800 (Singapore Standard Time)

<p>
**DFS**

Since `1 <= grid.length, grid[i].length <= 15` is a very small magnititude, we can use DFS to explore the `grid`.
1. Loop through the `grid`, for each element at `(i, j)` perform DFS to get the max value;
2. if `(i, j)` is out of the bound, or `grid[i][j] == 0`, or visited already, return current `sum`;
3. Otherwise, recurse to the neighbors and accumulate the `sum`.
<iframe src="https://leetcode.com/playground/jVZFR8yR/shared" frameBorder="0" width="900" height="430"></iframe>


----
 **DFS** code **`without modification of input`**:
 
In case you are NOT comfortable with modification of the input `grid`, we can use a `set` to prune duplicates, do NOT forget remove it after each DFS exploration.

**Java:** Encode `(i, j)` to `i * n + j`, where `n = grid[0].length` and put it into a HashSet.

<iframe src="https://leetcode.com/playground/gUrik4ZW/shared" frameBorder="0" width="900" height="410"></iframe>


**Analysis:**

Each of the k gold cells can at most have 4 neighbors. Therefore,
Time: O(k * 4 ^ k + m * n), space: O(m * n), where k = number of gold cells, m = grid.length, n = grid[0].length.

----

**BFS**
Some said we can NOT use BFS to solve this problem, I do NOT believe and write a BFS code.:P

Assume there are `k` cells containing gold. Use `0 ~ k - 1` to identify the gold cells; Since `k <= 25`, we can set `0th ~ (k - 1)th` bit of an int to record the trace of the correspoinding cell in a path; e.g., in example 1, 
```
[[0,6,0],
 [5,8,7],
 [0,9,0]]
````
the id of gold cell `(0, 1)` and `(1, 1)` are `0` and `2` respectively. The one cell trace of them are `1 (1 << 0)` and `4 (1 << 2)`; The trace of of the path including only the two cells is `1 | 4 = 5`, and the sum of the path is `6 + 8 = 14`.
1. Loop through the grid, for each gold cell put into a Queue the coordinate `(i, j)`, gold value `grid[i][j]`, and the trace `(1 << goldCellId)`, then perform BFS to get the max value;
2. For any gold cell neighbor `(r, c)` during BFS, ignore it if it is already in the trace ((trace & oneCellTrace[r][c]) != 0); otherwise, add its one cell trace to the current trace (trace | oneCellTrace[r][c]).

<iframe src="https://leetcode.com/playground/btoJPBAg/shared" frameBorder="0" width="900" height="480"></iframe>

**Analysis:**

Time & space: O(k * 4 ^ k + m * n), where k = number of gold cells, m = grid.length, n = grid[0].length.



</p>


