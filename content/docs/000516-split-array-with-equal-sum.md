---
title: "Split Array with Equal Sum"
weight: 516
#id: "split-array-with-equal-sum"
---
## Description
<div class="description">
<p>
Given an array with n integers, you need to find if there are triplets  (i, j, k) which satisfies following conditions:
<ol>
<li> 0 < i, i + 1 < j, j + 1 < k < n - 1 </li>
<li> Sum of subarrays (0, i - 1), (i + 1, j - 1), (j + 1, k - 1) and (k + 1, n - 1) should be equal. </li>
</ol>
where we define that subarray (L, R) represents a slice of the original array starting from the element indexed L to the element indexed R.
</p>

<p><b>Example:</b><br />
<pre>
<b>Input:</b> [1,2,1,2,1,2,1]
<b>Output:</b> True
<b>Explanation:</b>
i = 1, j = 3, k = 5. 
sum(0, i - 1) = sum(0, 0) = 1
sum(i + 1, j - 1) = sum(2, 2) = 1
sum(j + 1, k - 1) = sum(4, 4) = 1
sum(k + 1, n - 1) = sum(6, 6) = 1
</pre>
</p>

<b>Note:</b>
<ol>
<li> 1 <= n <= 2000. </li>
<li> Elements in the given array will be in range [-1,000,000, 1,000,000]. </li>
</ol>
</div>

## Tags
- Array (array)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Alibaba - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

**Algorithm**

Before we start looking at any of the approaches for solving this problem, firstly we need to look at the limits imposed on $$i$$, $$j$$ and $$k$$ by the given set of constraints. The figure below shows the maximum and minimum values that $$i$$, $$j$$ and $$k$$ can assume.

![Split_Array](../Figures/638_Split_Array.PNG)

Thus, the limits based on the length of the array $$n$$ can now be rewritten as:

$$1 &le; i &le; n-6$$

$$i+2 &le; j &le; n-4$$

$$j+2 &le; k &le; n-2$$

Having discussed the limits imposed on the cuts $$i$$, $$j$$, $$k$$ that we will apply on the given array $$nums$$, let's look at the first solution that comes to our mind.

We simply traverse over all the elements of the array. We consider all the possible subarrays taking care of the constraints imposed on the cuts, and check if any such cuts exist which satisfy the given equal sum quadruples criteria.


<iframe src="https://leetcode.com/playground/VVnTPo9k/shared" frameBorder="0" name="VVnTPo9k" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^4)$$. Four for loops inside each other each with a worst case run of length $$n$$.
* Space complexity : $$O(1)$$. Constant Space required.

---
#### Approach #2 Cumulative Sum [Time Limit Exceeded]

**Algorithm**

In the brute force approach, we traversed over the subarrays for every triplet of cuts considered. Rather than doing this, we can save some calculation effort if we make use of a cumulative sum array $$sum$$, where $$sum[i]$$ stores the cumulative sum of the array $$nums$$ upto the $$i^{th}$$ element. Thus, now in order to find the $$sum\big(subarray(i:j)\big)$$, we can simply use $$sum[j]-sum[i]$$. Rest of the process remains the same.

<iframe src="https://leetcode.com/playground/rnHnJfu2/shared" frameBorder="0" name="rnHnJfu2" width="100%" height="462"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^3)$$. Three for loops are there, one within the other.

* Space complexity : $$O(n)$$. $$sum$$ array of size $$n$$ is used for storing cumulative sum.

---
#### Approach #3 Slightly Better Approach [Time Limit Exceeded]

**Algorithm**

We can improve the previous implementation to some extent if we stop checking for further quadruples if the first and second parts formed till now don't have equal sums. This idea is used in the current implementation.

<iframe src="https://leetcode.com/playground/4ou9KNuS/shared" frameBorder="0" name="4ou9KNuS" width="100%" height="513"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^3)$$. Three loops are there.

* Space complexity : $$O(n)$$. $$sum$$ array of size $$n$$ is used for storing the cumulative sum.

---
#### Approach #4 Using HashMap [Time limit Exceeded]

**Algorithm**

In this approach, we create a data structure called $$map$$ which is simply a HashMap, with data arranged in the format:

$$\big\{csum(i):[i_1,i_2,i_3,....]\big\}$$, here $$csum(i)$$ represents the cumulative sum in the given array $$nums$$ upto the $$i^{th}$$ index and its corresponding value represents indices upto which cumulative sum=csum(i).

Once we create this $$map$$, the solutions gets simplified a lot. Consider only the first two cuts formed by $$i$$ and $$j$$. Then, the cumulative sum upto the $$(j-1)^{th}$$ index will be given by: $$csum(j-1)=sum(part1) + nums[i] + sum(part2)$$. Now, if we want the first two parts to have the same sum, the same cumulative sum can be rewritten as:

$$csum'(j-1) = csum(i-1) + nums[i] + csum(i-1) = 2csum(i-1) + nums[i]$$.

Thus, we traverse over the given array, changing the value of the index $$i$$ forming the first cut, and look if the $$map$$ formed initially contains a cumulative sum equal to $$csum'(j-1)$$. If $$map$$ contains such a cumulative sum, we consider every possible index $$j$$ satisfying the given constraints and look for the equalities of the first cumulative sum with the third and the fourth parts.

Following the similar lines as the discussion above, the cumulative sum upto the third cut by $$k^{th}$$ index is given by 

$$csum(k-1) = sum(part1) + nums[i] + sum(part2) + nums[j] + sum(part3)$$. 

For equality of sum, the condition becomes: 

$$csum'(k-1) = 3*csum(i-1) + nums[i] + nums[j]$$. 

Similarly, the cumulative sum upto the last index becomes:

$$csum(end) = sum(part1) + nums[i] + sum(part2) + nums[j] + sum(part3) + nums[k] + sum(part4)$$. 

Again, for equality, the condition becomes:

$$csum'(end) = 4*csum(i-1) + nums[i] + nums[j] + nums[k]$$.

For every cut chosen, we look if the required cumulative sum exists in $$map$$. Thus, we need not calculate sums again and again or traverse over the array for all the triplets $$(i, j, k)$$ possible. Rather, now, we directly know, what cumulative sum to look for in the $$map$$, which reduces a lot of computations.

<iframe src="https://leetcode.com/playground/bnMtNeme/shared" frameBorder="0" name="bnMtNeme" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^3)$$. Three nested loops are there and every loop runs $$n$$ times in the worst case. Consider the worstcase [0,0,0....,1,1,1,1,1,1,1].

* Space complexity : $$O(n)$$. HashMap size can go upto $$n$$.

---
#### Approach #5 Using Cumulative Sum and HashSet [Accepted]

**Algorithm**

In this approach, firstly we form a cumulative sum array $$sum$$, where $$sum[i]$$ stores the cumulative sum of the array $$nums$$ upto the $$i^{th}$$ index. Then, we start by traversing over the possible positions for the middle cut formed by $$j$$. For every $$j$$, firstly, we find all the left cut's positions, $$i$$,  that lead to equalizing the sum of the first and the second part (i.e. $$sum[i-1] = sum [j-1] - sum[i]$$) and store such sums in the $$set$$ (a new HashSet is formed for every $$j$$ chosen). Thus, the presence of a sum in $$set$$ implies that such a sum is possible for having equal sum of the first and second part for the current position of the middle cut($$j$$).

Then, we go for the right cut and find the position of the right cut that leads to equal sum of the third and the fourth part ($$sum[n-1]-sum[k]=sum[k-1] - sum[j]$$), for the same middle cut as chosen earlier. We also, look if the same sum exists in the $$set$$. If so, such a triplet $$(i, j, k)$$ exists which satisfies the required criteria, otherwise not.

Look at the animation below for a visual representation of the process:

<!--![Split_Array](../Figures/638_Split_Array.gif)-->
!?!../Documents/548_Split_Array.json:1000,563!?!

<iframe src="https://leetcode.com/playground/rBcQvyon/shared" frameBorder="0" name="rBcQvyon" width="100%" height="445"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. One outer loop and two inner loops are used.

* Space complexity : $$O(n)$$. HashSet size can go upto $$n$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java solution O(n^2)
- Author: vinod23
- Creation Date: Sun Apr 02 2017 11:15:24 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 06:39:17 GMT+0800 (Singapore Standard Time)

<p>
Here j is used for middle cut, i for left cut and k for right cut. 
Iterate middle cuts and then find left cuts which divides the first half into two equal quarters, store that quarter sums in  the hashset. Then find right cuts which divides the second half into two equal quarters and check if quarter sum is present in the hashset. If yes return true.
```
public class Solution {
    public boolean splitArray(int[] nums) {
        if (nums.length < 7)
            return false;
        int[] sum = new int[nums.length];
        sum[0] = nums[0];
        for (int i = 1; i < nums.length; i++) {
            sum[i] = sum[i - 1] + nums[i];
        }
        for (int j = 3; j < nums.length - 3; j++) {
            HashSet < Integer > set = new HashSet < > ();
            for (int i = 1; i < j - 1; i++) {
                if (sum[i - 1] == sum[j - 1] - sum[i])
                    set.add(sum[i - 1]);
            }
            for (int k = j + 2; k < nums.length - 1; k++) {
                if (sum[nums.length - 1] - sum[k] == sum[k - 1] - sum[j] && set.contains(sum[k - 1] - sum[j]))
                    return true;
            }
        }
        return false;
    }
}
</p>


### Java solution, DFS
- Author: shawngao
- Creation Date: Sun Apr 02 2017 11:26:31 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 06:32:50 GMT+0800 (Singapore Standard Time)

<p>
Just think this problem as a DFS. What we need is to search for ```3``` positions (i, j, k) and see if they divide the array to ```4``` parts with same summary. Some tricks:
1. Calculate ```left``` on the fly. Thus at last we don't need to calc summary of the ```4th``` part.
2. Skip ```0``` during calculate ```target``` because adding zero won't change it.

```
public class Solution {
    public boolean splitArray(int[] nums) {
        int sum = 0, target = 0;
        for (int num : nums) sum += num;
        for (int i = 1; i + 5 < nums.length; i++) {
            if (i != 1 && nums[i - 1] == 0  && nums[i] == 0) continue;
            target += nums[i - 1];
            if (dfs(nums, i + 1, target, sum - target - nums[i], 1)) return true;
        }
        return false;
    }
    
    private boolean dfs(int[] nums, int start, int target, int left, int depth) {
        if (depth == 3) {
            if (left == target) return true;
            return false;
        }
        
        int sub = 0;
        for (int j = start + 1; j + 5 - depth * 2 < nums.length; j++) {
            sub += nums[j - 1];
            if (sub == target) {
                if (dfs(nums, j + 1, target, left - sub - nums[j], depth + 1)) {
                    return true;
                }
            }
        }
        
        return false;
    }
}
```
</p>


### 5 lines simple Python
- Author: realisking
- Creation Date: Sun Apr 02 2017 13:08:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 16 2018 05:55:08 GMT+0800 (Singapore Standard Time)

<p>
The key idea is kind like divide and conquer (only twice though).

First, for every middle point ```j```, we split ```nums``` into two subarray ```nums[:j]``` and ```nums[j+1:]```. In the helper function ```split```, try to remove one element from the subarray, if the the sums of two remaining left and right sub-subarray are equal, we keep the ```sum``` of sub-subarray in the set we return. Once we have any intersection between the two sets, we know we can make it. 

Keep in mind ```len(nums) > 6``` is a must since we need to split original array into four parts.
```
class Solution(object):
    def splitArray(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        def split(A):
            total = sum(A)
            for i in range(1, len(A)): A[i] += A[i-1]
            return {A[i-1] for i in range(1, len(A)-1) if A[i-1] == total - A[i]}
            
        return len(nums) > 6 and any(split(nums[:j]) & split(nums[j+1:]) \
                             for j in range(3, len(nums)-3))
```
</p>


