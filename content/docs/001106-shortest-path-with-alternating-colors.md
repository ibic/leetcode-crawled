---
title: "Shortest Path with Alternating Colors"
weight: 1106
#id: "shortest-path-with-alternating-colors"
---
## Description
<div class="description">
<p>Consider a directed graph, with nodes labelled <code>0, 1, ..., n-1</code>.&nbsp; In this graph, each edge is either red or blue, and there could&nbsp;be self-edges or parallel edges.</p>

<p>Each <code>[i, j]</code> in <code>red_edges</code> denotes a red directed edge from node <code>i</code> to node <code>j</code>.&nbsp; Similarly, each <code>[i, j]</code> in <code>blue_edges</code> denotes a blue directed edge from node <code>i</code> to node <code>j</code>.</p>

<p>Return an array <code>answer</code>&nbsp;of length <code>n</code>,&nbsp;where each&nbsp;<code>answer[X]</code>&nbsp;is&nbsp;the length of the shortest path from node <code>0</code>&nbsp;to node <code>X</code>&nbsp;such that the edge colors alternate along the path (or <code>-1</code> if such a path doesn&#39;t exist).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> n = 3, red_edges = [[0,1],[1,2]], blue_edges = []
<strong>Output:</strong> [0,1,-1]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> n = 3, red_edges = [[0,1]], blue_edges = [[2,1]]
<strong>Output:</strong> [0,1,-1]
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> n = 3, red_edges = [[1,0]], blue_edges = [[2,1]]
<strong>Output:</strong> [0,-1,-1]
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> n = 3, red_edges = [[0,1]], blue_edges = [[1,2]]
<strong>Output:</strong> [0,1,2]
</pre><p><strong>Example 5:</strong></p>
<pre><strong>Input:</strong> n = 3, red_edges = [[0,1],[0,2]], blue_edges = [[1,0]]
<strong>Output:</strong> [0,1,1]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 100</code></li>
	<li><code>red_edges.length &lt;= 400</code></li>
	<li><code>blue_edges.length &lt;= 400</code></li>
	<li><code>red_edges[i].length == blue_edges[i].length == 2</code></li>
	<li><code>0 &lt;= red_edges[i][j], blue_edges[i][j] &lt; n</code></li>
</ul>
</div>

## Tags
- Breadth-first Search (breadth-first-search)
- Graph (graph)

## Companies
- Wish - 2 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python] BFS
- Author: lee215
- Creation Date: Sun Jul 21 2019 12:01:58 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 27 2019 23:38:35 GMT+0800 (Singapore Standard Time)

<p>
Just need to be noticed that, result can to bigger than `n`.
To be more specific, the maximum result can be `n * 2 - 3`.
So in my solution I initial the result as `n * 2`

**Some note:**
`G` = graph
`i` = index of Node
`c` = color
<br>

**Python:**
```python
    def shortestAlternatingPaths(self, n, red_edges, blue_edges):
        G = [[[], []] for i in xrange(n)]
        for i, j in red_edges: G[i][0].append(j)
        for i, j in blue_edges: G[i][1].append(j)
        res = [[0, 0]] + [[n * 2, n * 2] for i in xrange(n - 1)]
        bfs = [[0, 0], [0, 1]]
        for i, c in bfs:
            for j in G[i][c]:
                if res[j][c] == n * 2:
                    res[j][c] = res[i][1 - c] + 1
                    bfs.append([j, 1 - c])
        return [x if x < n * 2 else -1 for x in map(min, res)]
```

**Java**
Credit to @venkim
```java
    public int[] shortestAlternatingPaths(int n, int[][] red_edges, int[][] blue_edges) {
        // Two sets one for blu and another for red
        Set<Integer>[][] graph = new HashSet[2][n];
        for (int i = 0; i < n; i++) {
            graph[0][i] = new HashSet<>();
            graph[1][i] = new HashSet<>();
        }
        // red edges in 0 - col
        for (int[] re : red_edges) {
            graph[0][ re[0] ].add(re[1]);
        }
        // blu edges in 1 - col
        for (int[] blu : blue_edges) {
            graph[1][ blu[0] ].add(blu[1]);
        }
        int[][] res = new int[2][n];
        // Zero edge is always accessible to itself - leave it as 0
        for (int i = 1; i < n; i++) {
            res[0][i] = 2 * n;
            res[1][i] = 2 * n;
        }
        // Q entries are vert with a color (up to that point)
        Queue<int[]> q = new LinkedList<>();
        q.offer(new int[] {0, 0}); // either with red
        q.offer(new int[] {0, 1}); // or with blue
        while (!q.isEmpty()) {
            int[] cur = q.poll();
            int vert = cur[0];
            int colr = cur[1];
            // No need to keep track of level up to now
            // only need to keep what color - and the length
            // is automatically derived from previous node
            for (int nxt : graph[1 - colr][vert]) {
                if (res[1 - colr][nxt] == 2 * n) {
                    res[1 - colr][nxt] = 1 + res[colr][vert];
                    q.offer(new int[] {nxt, 1 - colr});
                }
            }
        }
        int[] ans = new int[n];
        for (int i = 0; i < n; i++) {
            int t = Math.min(res[0][i], res[1][i]);
            ans[i] = (t == 2 * n) ? -1 : t;
        }
        return ans;
    }
```
</p>


### Java BFS Solution with Video Explanation
- Author: LifeIsAGame
- Creation Date: Sun Jul 21 2019 18:44:47 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 06 2019 20:43:37 GMT+0800 (Singapore Standard Time)

<p>
Chinese Version\u4E2D\u6587\u89E3\u91CA

<iframe width="560" height="315" src="https://www.youtube.com/embed/Y44KhDTSOMQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

English Version

<iframe width="560" height="315" src="https://www.youtube.com/embed/bP-HEu8ghDk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

```
class Solution {
    public int[] shortestAlternatingPaths(int n, int[][] red_edges, int[][] blue_edges) {
        int[][] g = new int[n][n];
        buildGraph(g, n, red_edges, blue_edges);
        
        Queue<int[]> q = new LinkedList<>();
        q.offer(new int[]{0, 1});
        q.offer(new int[]{0, -1});
        int len = 0;
        int[] res = new int[n];
        Arrays.fill(res, Integer.MAX_VALUE);
        res[0] = 0;
        
        Set<String> visited = new HashSet<>();
        while (!q.isEmpty()) {
            int size = q.size();
            len++;
            for (int i = 0; i < size; i++) {
                int[] cur = q.poll();
                int node = cur[0];
                int color = cur[1];
                int oppoColor = -color;
                
                for (int j = 1; j < n; j++) {
                    if (g[node][j] == oppoColor ||
                       g[node][j] == 0) {
                        if (!visited.add(j + "" + oppoColor)) continue;
                        q.offer(new int[]{j, oppoColor});
                        res[j] = Math.min(res[j], len);
                    }
                }
            }
        }
        
        for (int i = 1; i < n; i++) {
            if (res[i] == Integer.MAX_VALUE) {
                res[i] = -1;
            }
        }
        
        return res;
    }
    
    private void buildGraph(int[][] g, int n, int[][] red_edges, int[][] blue_edges) {
        for (int i = 0; i < n; i++) {
            Arrays.fill(g[i], -n);
        }
        
        for (int[] e : red_edges) {
            int from = e[0];
            int to = e[1];
            g[from][to] = 1;
        }
        
        for (int[] e : blue_edges) {
            int from = e[0];
            int to = e[1];
            if (g[from][to] == 1) {
                g[from][to] = 0;
            } else {
                g[from][to] = -1;
            }
        }
    }
}
```
</p>


### Intuitive Java Solution With Explanation
- Author: naveen_kothamasu
- Creation Date: Sun Jul 21 2019 12:09:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 21 2019 14:10:18 GMT+0800 (Singapore Standard Time)

<p>
**Idea**

The basic idea is to do BFS and adding children from the opposite color for the `curr` explored node. This will ensure only the valid paths are added to `q` (we add `<node#, incoming-edge-color-to-node>` to `q`). So there is not need to keep track of valid paths or their lengths.

The tricky part is to figure out avoiding loops in the BFS. Simply, skipping the visited nodes will also skip other valid paths. But if we do not skip visited nodes, obviously cycles will be a problem.

That is why, `seen` maintains visited nodes with an edge color that it came from. That way, we can avoid cycles and still explore red edge adding a node and blue edge adding the same node.


```
//1=red, 2=blue, 0=root-edge (special case)
public int[] shortestAlternatingPaths(int n, int[][] red_edges, int[][] blue_edges) {
        List<Integer>[] reds = new ArrayList[n], blues = new ArrayList[n];
        for(int[] e : red_edges){
            if(reds[e[0]] == null)
                reds[e[0]] = new ArrayList<>();
            reds[e[0]].add(e[1]);
        }
        for(int[] e : blue_edges){
            if(blues[e[0]] == null)
                blues[e[0]] = new ArrayList<>();
            blues[e[0]].add(e[1]);
        }
        Queue<int[]> q = new LinkedList<>();
        int[] res = new int[n];
        Arrays.fill(res, -1);
        q.add(new int[]{0, 0});
        int moves = 0;
        Set<String> seen = new HashSet<>();
        while(!q.isEmpty()){
            int size = q.size();
            for(int i = 0; i < size; i++){
                int[] curr = q.remove();
                String key = curr[0]+" "+curr[1];
                if(seen.contains(key)) continue;
                seen.add(key);
                if(res[curr[0]] == -1)
                    res[curr[0]] = moves;
                if(curr[1] == 2 || curr[1] == 0)
                    if(reds[curr[0]] != null)
                        for(int child : reds[curr[0]])
                            q.add(new int[]{child, 1});
                if(curr[1] == 1 || curr[1] == 0)
                    if(blues[curr[0]] != null)
                        for(int child : blues[curr[0]])
                            q.add(new int[]{child, 2});
            }
            ++moves;
        }
        return res;
    }
```

Thanks to @muma2, we can just initilize all nodes as unreachable (`-1`).
</p>


