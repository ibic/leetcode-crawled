---
title: "Queries Quality and Percentage"
weight: 1549
#id: "queries-quality-and-percentage"
---
## Description
<div class="description">
<p>Table: <code>Queries</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| query_name  | varchar |
| result      | varchar |
| position    | int     |
| rating      | int     |
+-------------+---------+
There is no primary key for this table, it may have duplicate rows.
This table contains information collected from some queries on a database.
The <code>position</code> column has a value from <strong>1</strong> to <strong>500</strong>.
The <code>rating</code> column has a value from <strong>1</strong> to <strong>5</strong>. Query with <code>rating</code> less than 3 is a poor query.
</pre>

<p>&nbsp;</p>

<p>We define query <code>quality</code> as:</p>

<blockquote>
<p>The average of the ratio between query rating and its position.</p>
</blockquote>

<p>We also define <code>poor query percentage</code> as:</p>

<blockquote>
<p>The percentage of all queries with rating less than 3.</p>
</blockquote>

<p>Write an SQL query to find each&nbsp;<code>query_name</code>, the <code>quality</code> and <code>poor_query_percentage</code>.</p>

<p>Both <code>quality</code> and <code>poor_query_percentage</code> should be <strong>rounded to 2 decimal places</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
Queries table:
+------------+-------------------+----------+--------+
| query_name | result            | position | rating |
+------------+-------------------+----------+--------+
| Dog        | Golden Retriever  | 1        | 5      |
| Dog        | German Shepherd   | 2        | 5      |
| Dog        | Mule              | 200      | 1      |
| Cat        | Shirazi           | 5        | 2      |
| Cat        | Siamese           | 3        | 3      |
| Cat        | Sphynx            | 7        | 4      |
+------------+-------------------+----------+--------+

Result table:
+------------+---------+-----------------------+
| query_name | quality | poor_query_percentage |
+------------+---------+-----------------------+
| Dog        | 2.50    | 33.33                 |
| Cat        | 0.66    | 33.33                 |
+------------+---------+-----------------------+

Dog queries quality is ((5 / 1) + (5 / 2) + (1 / 200)) / 3 = 2.50
Dog queries poor_ query_percentage is (1 / 3) * 100 = 33.33

Cat queries quality equals ((2 / 5) + (3 / 3) + (4 / 7)) / 3 = 0.66
Cat queries poor_ query_percentage is (1 / 3) * 100 = 33.33
</pre>

</div>

## Tags


## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [MYSQL] beats 100%
- Author: Roger_Q
- Creation Date: Thu Oct 03 2019 10:43:52 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 30 2019 17:05:23 GMT+0800 (Singapore Standard Time)

<p>
Edited:
```
SELECT query_name 
,  ROUND(AVG(rating*100 / position), 0)/100 AS quality
,  ROUND(AVG(case 
             when rating < 3 then 1 
             else 0 end
            )*100, 2) as poor_query_percentage
FROM Queries
GROUP BY query_name
```

Original:
```
SELECT query_name 
,  ROUND(AVG(rating*100 / position), 0)/100 AS quality
,  ROUND(SUM(case 
             when rating < 3 then 1 
             else 0 end
            )*100 / count(*), 2) as poor_query_percentage
FROM Queries
GROUP BY query_name
```
</p>


### Use AVG() for both column calculations
- Author: josmoor98
- Creation Date: Fri Oct 18 2019 16:27:07 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 18 2019 16:27:26 GMT+0800 (Singapore Standard Time)

<p>
	SELECT 
		query_name,
		ROUND(AVG(rating / position), 2) AS quality,
		ROUND(AVG(rating < 3) * 100, 2) AS poor_query_percentage 
	FROM 
		Queries
	GROUP BY 
		query_name
</p>


### Straightforward MySQL
- Author: ddoudle
- Creation Date: Sat Mar 07 2020 07:52:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 07 2020 07:54:13 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT
    query_name, 
    ROUND(AVG(rating/position), 2) AS quality,
    ROUND(AVG(rating<3)*100, 2) AS poor_query_percentage 
FROM Queries 
GROUP BY query_name 
```
</p>


