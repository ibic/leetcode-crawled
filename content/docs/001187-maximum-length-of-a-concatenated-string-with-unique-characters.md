---
title: "Maximum Length of a Concatenated String with Unique Characters"
weight: 1187
#id: "maximum-length-of-a-concatenated-string-with-unique-characters"
---
## Description
<div class="description">
<p>Given an array of strings <code>arr</code>. String <code>s</code> is a concatenation of a sub-sequence of <code>arr</code> which have <strong>unique characters</strong>.</p>

<p>Return <em>the maximum possible length</em> of <code>s</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [&quot;un&quot;,&quot;iq&quot;,&quot;ue&quot;]
<strong>Output:</strong> 4
<strong>Explanation:</strong> All possible concatenations are &quot;&quot;,&quot;un&quot;,&quot;iq&quot;,&quot;ue&quot;,&quot;uniq&quot; and &quot;ique&quot;.
Maximum length is 4.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [&quot;cha&quot;,&quot;r&quot;,&quot;act&quot;,&quot;ers&quot;]
<strong>Output:</strong> 6
<strong>Explanation:</strong> Possible solutions are &quot;chaers&quot; and &quot;acters&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [&quot;abcdefghijklmnopqrstuvwxyz&quot;]
<strong>Output:</strong> 26
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 16</code></li>
	<li><code>1 &lt;= arr[i].length &lt;= 26</code></li>
	<li><code>arr[i]</code> contains only lower case English letters.</li>
</ul>

</div>

## Tags
- Backtracking (backtracking)
- Bit Manipulation (bit-manipulation)

## Companies
- Microsoft - 9 (taggedByAdmin: false)
- Tesla - 5 (taggedByAdmin: false)
- honey - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python/C++/Java] Set Solution
- Author: lee215
- Creation Date: Sun Oct 27 2019 12:06:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Dec 21 2019 23:05:07 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**

0. Initial the result `res` to include the case of empty string `""`.
`res` include all possible combinations we find during we iterate the input.

1. Itearte the the input strings,
but skip the word that have duplicate characters.
The examples is kind of misleading,
but the input string can have duplicate characters,
no need to considerate these strings.

3. For each string,
we check if it\'s conflit with the combination that we found.
If they have intersection of characters, we skip it.
If not, we append this new combination to result.

4. return the maximum length from all combinations.
<br>

**Python:**
```python
    def maxLength(self, A):
        dp = [set()]
        for a in A:
            if len(set(a)) < len(a): continue
            a = set(a)
            for c in dp[:]:
                if a & c: continue
                dp.append(a | c)
        return max(len(a) for a in dp)
```
**C++**
based on @savvadia
```C++
    int maxLength(vector<string>& A) {
        vector<bitset<26>> dp = {bitset<26>()};
        int res = 0;
        for (auto& s : A) {
            bitset<26> a;
            for (char c : s)
                a.set(c - \'a\');
            int n = a.count();
            if (n < s.size()) continue;

            for (int i = dp.size() - 1; i >= 0; --i) {
                bitset c = dp[i];
                if ((c & a).any()) continue;
                dp.push_back(c | a);
                res = max(res, (int)c.count() + n);
            }
        }
        return res;
    }
```
**Java**
```java
    public int maxLength(List<String> A) {
        List<Integer> dp = new ArrayList<>();
        dp.add(0);
        int res = 0;
        for (String s : A) {
            int a = 0, dup = 0;
            for (char c : s.toCharArray()) {
                dup |= a & (1 << (c - \'a\'));
                a |= 1 << (c - \'a\');
            }
            if (dup > 0) continue;
            for (int i = dp.size() - 1; i >= 0; --i) {
                if ((dp.get(i) & a) > 0) continue;
                dp.add(dp.get(i) | a);
                res = Math.max(res, Integer.bitCount(dp.get(i) | a));
            }
        }
        return res;
    }
```
</p>


### Clean Java/JavaScript DFS solution
- Author: misters
- Creation Date: Sun Oct 27 2019 12:08:31 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 30 2019 05:13:18 GMT+0800 (Singapore Standard Time)

<p>
I think you can optimize a bit by adding memory to it. Time was limited during the contest, I didn\'t have time to optimized it.

```
class Solution {
    private int result = 0;

    public int maxLength(List<String> arr) {
        if (arr == null || arr.size() == 0) {
            return 0;
        }

        dfs(arr, "", 0);

        return result;
    }

    private void dfs(List<String> arr, String path, int idx) {
        boolean isUniqueChar = isUniqueChars(path);
        
        if (isUniqueChar) {
            result = Math.max(path.length(), result);
        }

        if (idx == arr.size() || !isUniqueChar) {
            return;
        }
        
        for (int i = idx; i < arr.size(); i++) {
            dfs(arr, path + arr.get(i), i + 1);
        }
    }

    private boolean isUniqueChars(String s) {
        Set<Character> set = new HashSet<>();

        for (char c : s.toCharArray()) {
            if (set.contains(c)) {
                return false;
            }
            set.add(c);
        }
        return true;
    }
}

```

JavaScript solution using memorization DFS from @jitendra8911 in the comment.

```
var maxLength = function(arr) {
    let maxLen = 0;
    let listOfStrs = [];
    arr = arr.filter(isUnique);
    let mem = {};
    maxLen = dfs(arr, "", 0, maxLen, mem);
    
    return maxLen;
};

function dfs(arr, path, i, maxLen, mem) {
    if (mem[path]) return mem[path];
    let pathIsUnique = isUnique(path);
    if (pathIsUnique) {
        maxLen = Math.max(path.length, maxLen);
    } 
    if (i === arr.length || !pathIsUnique) {
        mem[path] = maxLen;
        return maxLen;
    }
    for (let j = i; j < arr.length; j++) {
        maxLen = dfs(arr, path + arr[j], j + 1, maxLen, mem);
    }


    mem[path] = maxLen;
    return maxLen;
}

function isUnique(str) {
    const map = {}
    for (let i = 0; i < str.length; i++) {
        if (map[str[i]]) return false;
        map[str[i]] = 1;
    }
    
    return true;
}


```
</p>


### Python 3, Easy Code , Intutive Solution
- Author: reigns7677
- Creation Date: Thu May 14 2020 14:32:59 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 26 2020 12:42:17 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def maxLength(self, arr: List[str]) -> int:
        
        uniqELements = [\'\']
        maximum = 0
        for i in range(len(arr)):
            sz = len(uniqELements)
            for j in range(sz):
                x=arr[i]+uniqELements[j]
                if (len(x)==len(set(x))):
                    uniqELements.append(x)
                    maximum = max(maximum,len(x))
        #print(uniqELements)
        return maximum
		
</p>


