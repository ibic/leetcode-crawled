---
title: "Valid Palindrome"
weight: 125
#id: "valid-palindrome"
---
## Description
<div class="description">
<p>Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.</p>

<p><strong>Note:</strong>&nbsp;For the purpose of this problem, we define empty string as valid palindrome.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> &quot;A man, a plan, a canal: Panama&quot;
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> &quot;race a car&quot;
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>s</code> consists only of printable ASCII characters.</li>
</ul>

</div>

## Tags
- Two Pointers (two-pointers)
- String (string)

## Companies
- Facebook - 46 (taggedByAdmin: true)
- Apple - 4 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)
- Yandex - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Wayfair - 4 (taggedByAdmin: false)
- Cisco - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Uber - 0 (taggedByAdmin: true)
- Zenefits - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Compare with Reverse

**Intuition**

A palindrome is a word, phrase, or sequence that reads the same backwards as forwards. e.g. `madam`

A palindrome, and its reverse, are identical to each other.

**Algorithm**

We'll reverse the given string and compare it with the original. If those are equivalent, it's a palindrome.

Since only alphanumeric characters are considered, we'll filter out all other types of characters before we apply our algorithm.

Additionally, because we're treating letters as case-insensitive, we'll convert the remaining letters to lower case. The digits will be left the same.

<iframe src="https://leetcode.com/playground/XtvkGPok/shared" frameBorder="0" width="100%" height="500" name="XtvkGPok"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$, in length $$n$$ of the string.

  We need to iterate thrice through the string:
    1. When we filter out non-alphanumeric characters, and convert the remaining characters to lower-case.
    2. When we reverse the string.
    3. When we compare the original and the reversed strings.

  Each iteration runs linear in time (since each character operation completes in constant time). Thus, the effective run-time complexity is linear.

* Space complexity : $$O(n)$$, in length $$n$$ of the string. We need $$O(n)$$ additional space to stored the filtered string and the reversed string.

<br />

---

#### Approach 2: Two Pointers

**Intuition**

If you take any ordinary string, and concatenate its reverse to it, you'll get a palindrome. This leads to an interesting insight about the converse: every palindrome half is reverse of the other half.

Simply speaking, if one were to start in the middle of a palindrome, and traverse outwards, they'd encounter the same characters, in the exact same order, in both halves!

!?!../Documents/125_valid_palindrome.json:800,600!?!

**Algorithm**

Since the input string contains characters that we need to ignore in our palindromic check, it becomes tedious to figure out the real middle point of our palindromic input.

> Instead of going outwards from the middle, we could just go inwards towards the middle!

So, if we start traversing inwards, from both ends of the input string, we can expect to _see_ the same characters, in the same order.

The resulting algorithm is simple:
+ Set two pointers, one at each end of the input string
+ If the input is palindromic, both the pointers should point to equivalent characters, _at all times_. [^note-1]
  + If this condition is not met at any point of time, we break and return early.  [^note-2]
+ We can simply ignore non-alphanumeric characters by continuing to traverse further.
+ Continue traversing inwards until the pointers meet in the middle.

<iframe src="https://leetcode.com/playground/VhRBMQi8/shared" frameBorder="0" width="100%" height="378" name="VhRBMQi8"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$, in length $$n$$ of the string. We traverse over each character at-most once, until the two pointers meet in the middle, or when we break and return early.

* Space complexity : $$O(1)$$. No extra space required, at all.



[^note-1]: Such a property is formally known as a [loop invariant](https://en.wikipedia.org/wiki/Loop_invariant).

[^note-2]: Such a property is often called a _loop termination condition_. It is one of several used in this solution. Can you identify the others?

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted pretty Java solution(271ms)
- Author: odanylevskyi
- Creation Date: Mon Feb 02 2015 06:57:02 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 21:42:00 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public boolean isPalindrome(String s) {
            if (s.isEmpty()) {
            	return true;
            }
            int head = 0, tail = s.length() - 1;
            char cHead, cTail;
            while(head <= tail) {
            	cHead = s.charAt(head);
            	cTail = s.charAt(tail);
            	if (!Character.isLetterOrDigit(cHead)) {
            		head++;
            	} else if(!Character.isLetterOrDigit(cTail)) {
            		tail--;
            	} else {
            		if (Character.toLowerCase(cHead) != Character.toLowerCase(cTail)) {
            			return false;
            		}
            		head++;
            		tail--;
            	}
            }
            
            return true;
        }
    }
</p>


### Python in-place two-pointer solution.
- Author: OldCodingFarmer
- Creation Date: Wed Aug 26 2015 01:44:33 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 11:22:12 GMT+0800 (Singapore Standard Time)

<p>
        
    def isPalindrome(self, s):
        l, r = 0, len(s)-1
        while l < r:
            while l < r and not s[l].isalnum():
                l += 1
            while l <r and not s[r].isalnum():
                r -= 1
            if s[l].lower() != s[r].lower():
                return False
            l +=1; r -= 1
        return True
</p>


### My three line java solution
- Author: arunav2
- Creation Date: Sat Sep 26 2015 06:31:04 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 12:55:26 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public boolean isPalindrome(String s) {
            String actual = s.replaceAll("[^A-Za-z0-9]", "").toLowerCase();
            String rev = new StringBuffer(actual).reverse().toString();
            return actual.equals(rev);
        }
    }
</p>


