---
title: "Binary Tree Postorder Traversal"
weight: 145
#id: "binary-tree-postorder-traversal"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a&nbsp;binary tree, return <em>the postorder traversal of its nodes&#39; values</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/28/pre1.jpg" style="width: 202px; height: 317px;" />
<pre>
<strong>Input:</strong> root = [1,null,2,3]
<strong>Output:</strong> [3,2,1]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = []
<strong>Output:</strong> []
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [1]
<strong>Output:</strong> [1]
</pre>

<p><strong>Example 4:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/28/pre3.jpg" style="width: 202px; height: 197px;" />
<pre>
<strong>Input:</strong> root = [1,2]
<strong>Output:</strong> [2,1]
</pre>

<p><strong>Example 5:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/28/pre2.jpg" style="width: 202px; height: 197px;" />
<pre>
<strong>Input:</strong> root = [1,null,2]
<strong>Output:</strong> [2,1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of the nodes in the tree is in the range <code>[0, 100]</code>.</li>
	<li><code>-100 &lt;= Node.val &lt;= 100</code></li>
</ul>

<p>&nbsp;</p>

<p><strong>Follow up:</strong></p>

<p>Recursive solution is trivial, could you do it iteratively?</p>

<p>&nbsp;</p>

</div>

## Tags
- Stack (stack)
- Tree (tree)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### How to traverse the tree

There are two general strategies to traverse a tree:

- *Breadth First Search* (`BFS`)

    We scan through the tree level by level, following the order of height,
    from top to bottom. The nodes on a higher level would be visited before the ones on the lower levels.
     
- *Depth First Search* (`DFS`)

    In this strategy, we adopt the `depth` as the priority, so that one
    would start from a root and reach down to a leaf,
    and then back to root to reach another branch.

    The DFS strategy can further be distinguished as
    `preorder`, `inorder`, and `postorder` depending on the relative order
    among the root node, left node, and right node.
    

![img](../Figures/145/traverse2.png)
*Figure 1. The nodes are numerated in the order you visit them,
please follow ```1-2-3-4-5``` to compare different strategies.*
{:align="center"}

Here the problem is to implement postorder traversal using iterations.
<br />
<br />


---
#### Approach 1: Recursive Postorder Traversal

![recursion](../Figures/145/recursion.png)
*Figure 2. Recursive DFS traversals.*
{:align="center"}

The most straightforward way is to implement recursion using 
Left -> Right -> Node traversal strategy.

<iframe src="https://leetcode.com/playground/ExYJjL2V/shared" frameBorder="0" width="100%" height="293" name="ExYJjL2V"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, where $$N$$ is the number of nodes. 
We visit each node exactly once, 
thus the time complexity is $$\mathcal{O}(N)$$.

* Space complexity: up to $$\mathcal{O}(H)$$ to keep the recursion stack,
where $$H$$ is a tree height.
<br />
<br />


---
#### Approach 2: Iterative Preorder Traversal: Tweak the Order of the Output

Let's start from the root, and at each iteration, 
pop the current node out of the stack and
push its child nodes. In the implemented strategy, we push nodes into stack following the order Top->Bottom and Left->Right. 
Since DFS postorder transversal is Bottom->Top and Left->Right 
the output list should be reverted after the end of the loop.

<iframe src="https://leetcode.com/playground/DesckNad/shared" frameBorder="0" width="100%" height="361" name="DesckNad"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, where $$N$$ is the number of nodes. 
We visit each node exactly once, 
thus the time complexity is $$\mathcal{O}(N)$$.

* Space complexity: up to $$\mathcal{O}(H)$$ to keep the stack,
where $$H$$ is a tree height.
<br />
<br />


---
#### Approach 3: Iterative Postorder Traversal

**Algorithm**

The idea is to fulfill the stack following right->node->left strategy. 
One could pop the last node out of the stack and check if it's the leftmost leaf.
If yes, it's time to update the output. Otherwise, one should 
swap the last two nodes in the stack
and repeat all these steps. 

!?!../Documents/145_LIS.json:1000,489!?!

**Implementation**

<iframe src="https://leetcode.com/playground/atdcf8JQ/shared" frameBorder="0" width="100%" height="500" name="atdcf8JQ"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, where $$N$$ is the number of nodes. 
We visit each node exactly once, 
thus the time complexity is $$\mathcal{O}(N)$$.

* Space complexity: up to $$\mathcal{O}(H)$$ to keep the stack,
where $$H$$ is a tree height.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Preorder, Inorder, and Postorder Iteratively Summarization
- Author: yavinci
- Creation Date: Sat Nov 28 2015 07:49:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 09:38:35 GMT+0800 (Singapore Standard Time)

<p>
Here I summarize the iterative implementation for preorder, inorder, and postorder traverse.

<hr>
<h3>Pre Order Traverse</h3>
<hr>

    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        Deque<TreeNode> stack = new ArrayDeque<>();
        TreeNode p = root;
        while(!stack.isEmpty() || p != null) {
            if(p != null) {
                stack.push(p);
                result.add(p.val);  // Add before going to children
                p = p.left;
            } else {
                TreeNode node = stack.pop();
                p = node.right;   
            }
        }
        return result;
    }

<hr>
<h3>In Order Traverse</h3>
<hr>

    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        Deque<TreeNode> stack = new ArrayDeque<>();
        TreeNode p = root;
        while(!stack.isEmpty() || p != null) {
            if(p != null) {
                stack.push(p);
                p = p.left;
            } else {
                TreeNode node = stack.pop();
                result.add(node.val);  // Add after all left children
                p = node.right;   
            }
        }
        return result;
    }

<hr>
<h3>Post Order Traverse</h3>
<hr>

    public List<Integer> postorderTraversal(TreeNode root) {
        LinkedList<Integer> result = new LinkedList<>();
        Deque<TreeNode> stack = new ArrayDeque<>();
        TreeNode p = root;
        while(!stack.isEmpty() || p != null) {
            if(p != null) {
                stack.push(p);
                result.addFirst(p.val);  // Reverse the process of preorder
                p = p.right;             // Reverse the process of preorder
            } else {
                TreeNode node = stack.pop();
                p = node.left;           // Reverse the process of preorder
            }
        }
        return result;
    }
</p>


### My Accepted code with explaination. Does anyone have a better idea?
- Author: Deepalaxmi
- Creation Date: Sun Aug 24 2014 05:33:10 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 15:55:13 GMT+0800 (Singapore Standard Time)

<p>
pre-order traversal is **root-left-right**, and post order is **left-right-root**. modify the code for pre-order to make it root-right-left, and then  **reverse** the output so that we can get left-right-root .


 1. Create an empty stack, Push root node to the stack.
 2. Do following while stack is not empty.

 2.1. pop an item from the stack and print it.
 
 2.2. push the left child of popped item to stack.

 2.3. push the right child of popped item to stack.

 3. reverse the ouput.

        class Solution {
        public:
            vector<int> postorderTraversal(TreeNode *root) {
                stack<TreeNode*> nodeStack;
                vector<int> result;
                //base case
                if(root==NULL)
                return result;
                nodeStack.push(root);
            while(!nodeStack.empty())
            {
                TreeNode* node= nodeStack.top();  
                result.push_back(node->val);
                nodeStack.pop();
                if(node->left)
                nodeStack.push(node->left);
                if(node->right)
                nodeStack.push(node->right);
            }
             reverse(result.begin(),result.end());
             return result;
            
        }
    };
</p>


### Java simple and clean
- Author: jinwu
- Creation Date: Sat Sep 05 2015 00:15:11 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 05:10:33 GMT+0800 (Singapore Standard Time)

<p>
    public List<Integer> postorderTraversal(TreeNode root) {
    	LinkedList<Integer> ans = new LinkedList<>();
    	Stack<TreeNode> stack = new Stack<>();
    	if (root == null) return ans;
    	
    	stack.push(root);
    	while (!stack.isEmpty()) {
    		TreeNode cur = stack.pop();
    		ans.addFirst(cur.val);
    		if (cur.left != null) {
    			stack.push(cur.left);
    		}
    		if (cur.right != null) {
    			stack.push(cur.right);
    		} 
    	}
    	return ans;
    }
</p>


