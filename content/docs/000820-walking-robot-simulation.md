---
title: "Walking Robot Simulation"
weight: 820
#id: "walking-robot-simulation"
---
## Description
<div class="description">
<p>A robot on an infinite grid starts at point (0, 0) and faces north.&nbsp; The robot can receive one of three possible types of commands:</p>

<ul>
	<li><code>-2</code>: turn left 90 degrees</li>
	<li><code>-1</code>: turn right 90 degrees</li>
	<li><code>1 &lt;= x &lt;= 9</code>: move forward <code>x</code> units</li>
</ul>

<p>Some of the grid squares are obstacles.&nbsp;</p>

<p>The <code>i</code>-th obstacle is at grid point <code>(obstacles[i][0], obstacles[i][1])</code></p>

<p>If the robot would try to move onto them, the robot stays on the previous grid square instead (but still continues following the rest of the route.)</p>

<p>Return the <strong>square</strong> of the maximum Euclidean distance that the robot will be from the origin.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>commands = <span id="example-input-1-1">[4,-1,3]</span>, obstacles = <span id="example-input-1-2">[]</span>
<strong>Output: </strong><span id="example-output-1">25</span>
<span>Explanation: </span>robot will go to (3, 4)
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>commands = <span id="example-input-2-1">[4,-1,4,-2,4]</span>, obstacles = <span id="example-input-2-2">[[2,4]]</span>
<strong>Output: </strong><span id="example-output-2">65</span>
<strong>Explanation</strong>: robot will be stuck at (1, 4) before turning left and going to (1, 8)
</pre>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= commands.length &lt;= 10000</code></li>
	<li><code>0 &lt;= obstacles.length &lt;= 10000</code></li>
	<li><code>-30000 &lt;= obstacle[i][0] &lt;= 30000</code></li>
	<li><code>-30000 &lt;= obstacle[i][1] &lt;= 30000</code></li>
	<li>The answer is guaranteed to be less than <code>2 ^ 31</code>.</li>
</ol>

</div>

## Tags
- Greedy (greedy)

## Companies
- Jane Street - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Simulation

**Intuition**

We simulate the path of the robot step by step.  Since there are at most 90000 steps, this is efficient enough to pass the given input limits.

**Algorithm**

We store the robot's position and direction.  If we get a turning command, we update the direction; otherwise we walk the specified number of steps in the given direction.

Care must be made to use a `Set` data structure for the obstacles, so that we can check efficiently if our next step is obstructed.  If we don't, our check `is point in obstacles` could be ~10,000 times slower.

In some languages, we need to encode the coordinates of each obstacle as a `long` integer so that it is a hashable key that we can put into a `Set` data structure.  Alternatively, we could also encode the coordinates as a `string`.

<iframe src="https://leetcode.com/playground/rvqXGcEw/shared" frameBorder="0" width="100%" height="500" name="rvqXGcEw"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N + K)$$, where $$N, K$$ are the lengths of `commands` and `obstacles` respectively.

* Space Complexity:  $$O(K)$$, the space used in storing the `obstacleSet`.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Maximum?! This is crazy!
- Author: wangzi6147
- Creation Date: Sun Jul 22 2018 11:04:37 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 14:07:58 GMT+0800 (Singapore Standard Time)

<p>
I spent nearly one hour to find this word! looooool

```
class Solution {
    public int robotSim(int[] commands, int[][] obstacles) {
        Set<String> set = new HashSet<>();
        for (int[] obs : obstacles) {
            set.add(obs[0] + " " + obs[1]);
        }
        int[][] dirs = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
        int d = 0, x = 0, y = 0, result = 0;
        for (int c : commands) {
            if (c == -1) {
                d++;
                if (d == 4) {
                    d = 0;
                }
            } else if (c == -2) {
                d--;
                if (d == -1) {
                    d = 3;
                }
            } else {
                while (c-- > 0 && !set.contains((x + dirs[d][0]) + " " + (y + dirs[d][1]))) {
                    x += dirs[d][0];
                    y += dirs[d][1];
                }
            }
            result = Math.max(result, x * x + y * y);
        }
        return result;
    }
}
```
</p>


### Logical Thinking with Clear Code
- Author: GraceMeng
- Creation Date: Wed Aug 01 2018 20:34:37 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 27 2020 00:34:35 GMT+0800 (Singapore Standard Time)

<p>
>The robot starts at point (0, 0) and faces north. **Which edge of grid is to the north?**
Since it will go to point (3, 4) with `commands = [4,-1,3], obstacles = []`, we know that the right edge is to the `North`.
```
     W
  S -|- N
     E
```

>**How do we represent absolute orientations given only relative turning directions(i.e., left or right)?** We define `direction` indicates the absolute orientation as below:
```
North, direction = 0, directions[direction] = {0, 1}
East,  direction = 1, directions[direction] = {1, 0}
South, direction = 2, directions[direction] = {0, -1}
West,  direction = 3, directions[direction] = {-1, 0}

direction will increase by one when we turn right, 
and will decrease by one (or increase by three) when we turn left.
```
****
```
    public int robotSim(int[] commands, int[][] obstacles) {
        int[][] directions = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
        
        // Set of obstacles indexes in the format of : obstacle[0] + " " + obstacle[1]
        Set<String> obstaclesSet = new HashSet<>();
        for (int[] obstacle : obstacles) {
            obstaclesSet.add(obstacle[0] + " " + obstacle[1]);
        }

        int x = 0, y = 0, direction = 0, maxDistSquare = 0;
        for (int i = 0; i < commands.length; i++) {
            if (commands[i] == -2) { // Turns left
                direction = (direction + 3) % 4;
            } else if (commands[i] == -1) { // Turns right
                direction = (direction + 1) % 4;
            } else { // Moves forward commands[i] steps
                int step = 0;
                while (step < commands[i] 
                       && (!obstaclesSet.contains(
                           (x + directions[direction][0]) + " " + (y + directions[direction][1]))
                          )
                      ) {
                    x += directions[direction][0];
                    y += directions[direction][1];
                    step++;
                }
            }
            maxDistSquare = Math.max(maxDistSquare, x * x + y * y);
        }

        return maxDistSquare;
    }
```

</p>


### Python short & straightforward solution w/ explanation & statement is wrong in the question !!!
- Author: cenkay
- Creation Date: Sun Jul 22 2018 11:28:23 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 21:27:31 GMT+0800 (Singapore Standard Time)

<p>
* Firstly, robot doesn\'t face **north** as stated in the explanation. When i consider north, i imagine robot faces upwards but its initial direction is **to right** actually.
* Note: This is a similar question to **Robot Room cleaner (No: 489)**.
*  **Code explanation**:
	*  At first, we should make obstacles as **set** for **O(1)** check for obstacles and we simply move according to command if there is no obstacle.
	*  **Critical part** is the **move array** in my opinion. It is a simplified move to **next i and j** in array form for **"right", "up", "left", "down" respectively.**
	*  We also change **d** variable, which is **direction** variable as move index, when we get -2 or -1 command.
	*  If command is -2, we should turn **left**, which is to **increment direction index.**
	*  If command is -1, we should turn **right**, which is to **decrement direction index.**
	*  Else, we can walk through untill face an obstacle or not.
	*  And we update **mx value** in each move also.
```
class Solution:
    def robotSim(self, commands, obstacles):
        i = j = mx = d = 0
        move, obstacles = [(0, 1), (-1, 0), (0, -1), (1, 0), ], set(map(tuple, obstacles))
        for command in commands:
            if command == -2: d = (d + 1) % 4
            elif command == -1: d = (d - 1) % 4
            else:
                x, y = move[d]
                while command and (i + x, j + y) not in obstacles:
                    i += x
                    j += y
                    command -= 1
            mx = max(mx, i ** 2 + j ** 2)
        return mx
```
</p>


