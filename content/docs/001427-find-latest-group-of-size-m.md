---
title: "Find Latest Group of Size M"
weight: 1427
#id: "find-latest-group-of-size-m"
---
## Description
<div class="description">
<p>Given an array <code>arr</code>&nbsp;that represents a permutation of numbers from <code>1</code>&nbsp;to <code>n</code>. You have a binary string of size&nbsp;<code>n</code>&nbsp;that initially has all its bits set to zero.</p>

<p>At each step <code>i</code>&nbsp;(assuming both the binary string and <code>arr</code> are 1-indexed) from <code>1</code> to&nbsp;<code>n</code>, the bit at position&nbsp;<code>arr[i]</code>&nbsp;is set to&nbsp;<code>1</code>. You are given an integer&nbsp;<code>m</code>&nbsp;and you need to find the latest step at which there exists a group of ones of length&nbsp;<code>m</code>. A group of ones is a contiguous substring of 1s such that it cannot be extended in either direction.</p>

<p>Return <em>the latest step at which there exists a group of ones of length <strong>exactly</strong></em>&nbsp;<code>m</code>. <em>If no such group exists, return</em>&nbsp;<code>-1</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [3,5,1,2,4], m = 1
<strong>Output:</strong> 4
<strong>Explanation:
</strong>Step 1: &quot;00<u>1</u>00&quot;, groups: [&quot;1&quot;]
Step 2: &quot;0010<u>1</u>&quot;, groups: [&quot;1&quot;, &quot;1&quot;]
Step 3: &quot;<u>1</u>0101&quot;, groups: [&quot;1&quot;, &quot;1&quot;, &quot;1&quot;]
Step 4: &quot;1<u>1</u>101&quot;, groups: [&quot;111&quot;, &quot;1&quot;]
Step 5: &quot;111<u>1</u>1&quot;, groups: [&quot;11111&quot;]
The latest step at which there exists a group of size 1 is step 4.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [3,1,5,4,2], m = 2
<strong>Output:</strong> -1
<strong>Explanation:
</strong>Step 1: &quot;00<u>1</u>00&quot;, groups: [&quot;1&quot;]
Step 2: &quot;<u>1</u>0100&quot;, groups: [&quot;1&quot;, &quot;1&quot;]
Step 3: &quot;1010<u>1</u>&quot;, groups: [&quot;1&quot;, &quot;1&quot;, &quot;1&quot;]
Step 4: &quot;101<u>1</u>1&quot;, groups: [&quot;1&quot;, &quot;111&quot;]
Step 5: &quot;1<u>1</u>111&quot;, groups: [&quot;11111&quot;]
No group of size 2 exists during any step.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [1], m = 1
<strong>Output:</strong> 1
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,1], m = 2
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == arr.length</code></li>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
	<li><code>1 &lt;= arr[i] &lt;= n</code></li>
	<li>All integers in&nbsp;<code>arr</code>&nbsp;are&nbsp;<strong>distinct</strong>.</li>
	<li><code>1 &lt;= m&nbsp;&lt;= arr.length</code></li>
</ul>

</div>

## Tags
- Binary Search (binary-search)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Count the Length of Groups, O(N)
- Author: lee215
- Creation Date: Sun Aug 23 2020 12:04:01 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 15 2020 10:49:05 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
When we set bit `a`, where `a = A[i]`,
we check the length of group on the left `length[a - 1]`
also the length of group on the right `length[a + 1]`.
Then we update `length[a - left], length[a + right]` to `left + right + 1`.

Note that the length value is updated on the leftmost and the rightmost bit of the group.
The length value inside the group may be out dated.

As we do this, we also update the `count` of length.
If `count[m] > 0`, we update `res` to current step index `i + 1`.
<br>

# **Complexity**
Time `O(N)`
Space `O(N)`
<br>


# Solution 1: Count all lengths
**Java:**
```java
    public int findLatestStep(int[] A, int m) {
        int res = -1, n = A.length;
        int[] length = new int[n + 2], count = new int[n + 1];
        for (int i = 0; i < n; ++i) {
            int a = A[i], left = length[a - 1], right = length[a + 1];
            length[a] = length[a - left] = length[a + right] = left + right + 1;
            count[left]--;
            count[right]--;
            count[length[a]]++;
            if (count[m] > 0)
                res = i + 1;
        }
        return res;
    }
```

**C++:**
```cpp
    int findLatestStep(vector<int>& A, int m) {
        int res = -1, n = A.size();
        vector<int> length(n + 2), count(n + 1);
        for (int i = 0; i < n; ++i) {
            int a = A[i], left = length[a - 1], right = length[a + 1];
            length[a] = length[a - left] = length[a + right] = left + right + 1;
            count[left]--;
            count[right]--;
            count[length[a]]++;
            if (count[m])
                res = i + 1;
        }
        return res;
    }
```
<br>

# Solution 2
@hCaulfield suggests removing the counter.
**Java:**
```java
    public int findLatestStep(int[] A, int m) {
        int res = -1, n = A.length;
        if (n == m) return n;
        int[] length = new int[n + 2];
        for (int i = 0; i < n; ++i) {
            int a = A[i], left = length[a - 1], right = length[a + 1];
            length[a - left] = length[a + right] = left + right + 1;
            if (left == m || right == m)
                res = i;
        }
        return res;
    }
```

**C++:**
```cpp
    int findLatestStep(vector<int>& A, int m) {
        int res = -1, n = A.size();
        if (n == m) return n;
        vector<int> length(n + 2);
        for (int i = 0; i < n; ++i) {
            int a = A[i], left = length[a - 1], right = length[a + 1];
            length[a - left] = length[a + right] = left + right + 1;
            if (left == m || right == m)
                res = i;
        }
        return res;
    }
```

**Python:**
```py
    def findLatestStep(self, A, m):
        if m == len(A): return m
        length = [0] * (len(A) + 2)
        res = -1
        for i, a in enumerate(A):
            left, right = length[a - 1], length[a + 1]
            if left == m or right == m:
                res = i
            length[a - left] = length[a + right] = left + right + 1
        return res
```
</p>


### [C++] Union Find (Count groups of size - Reverse mapping)
- Author: PhoenixDD
- Creation Date: Sun Aug 23 2020 12:00:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 23 2020 13:33:11 GMT+0800 (Singapore Standard Time)

<p>
### Observation

**Thought process:**

1. On first thought it seems like a union find data structure might help to union two groups into one.
1. We can also note that using a union find data structure we can also store the counts of each group and update them when union\'ing.
1. We need to somehow reverse map these counts, as in what\'s required in the question is the count(`m`) of these groups (where group size = `m`), thus let\'s also try and maintain the count of groups of size `x`
1. Now all we need to do is keep a track of the reverse map and record the last step where the `groupCounts[m]>0`.

### Solution
```c++
class Solution {
public:
    vector<int> parent,Rank,count,groupCounts;	//Basic union find related storages (groupCounts stores # of groups of size `i`- our reverse mapping)
    int Find(int x)
    {
        return parent[x]==x?x:parent[x]=Find(parent[x]);
    }
    void Union(int x,int y)			//Modified union to update groupCounts
    {
        int xset=Find(x),yset=Find(y);
        if(xset!=yset)
        {
            groupCounts[count[xset]]--,groupCounts[count[yset]]--;//Union\'ing would change the groupSize of current x and y groups.
            count[xset]=count[yset]=count[xset]+count[yset];		//Size of groups of x and y now are now size(x)+size(y)
            groupCounts[count[xset]]++;
            Rank[xset]<Rank[yset]?parent[xset]=yset:parent[yset]=xset;
            if(Rank[xset]==Rank[yset])
                Rank[xset]++;
        }
    }
    int findLatestStep(vector<int>& arr, int m)
    {
        parent.resize(arr.size()+1),Rank.resize(arr.size()+1,0);			//Few lines for basic initialization.
        for(int i=1;i<=arr.size();i++)
            parent[i]=i;
        vector<bool> visited(arr.size()+1,false);
        count.resize(arr.size()+1,1);			//Currently all groups are of size 1
        groupCounts.resize(arr.size()+1,0);
        int last=-1,step=1;						//Last stores the result, step is the current step (We can move this to a for loop)
        for(int &i:arr)
        {
            groupCounts[1]++;					//We always chose a new number whose current groupSize is 1 so we increase the groupCount[1] (Reverse Map)
            if(i+1<=arr.size()&&visited[i+1])	//Union i and i+1 if i+1\'th bit was previously set
                Union(i,i+1);
            if(i-1>0&&visited[i-1])				//Union i and i-1 if i-1\'th bit was previously set
                Union(i,i-1);
            visited[i]=true;
            if(groupCounts[m])					//If counts of group of size m is > 0 (This means we have a group of size m at current step, update `last`)
                last=step;
            step++;
        }
        return last;
    }
};
```
</p>


### [C++]-No other algorithm, We just iterativly do it from back to front --Very intuitive and clean !!
- Author: Aincrad-Lyu
- Creation Date: Sun Aug 23 2020 12:16:35 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 24 2020 11:50:26 GMT+0800 (Singapore Standard Time)

<p>
Hi, We can do from back to front. They asked the **lastest**, and when we do it reversely, we transfer it to the **first**. Here is the process:

We use a **queue** to record all the ones intervals. 
As we do it reversely, we set the initial state to all ones, and we iterate the **arr** to turn *one* to *zero*.

**Example**:
```
arr = [3,5,1,2,4], m = 1
```
The first interval in queue:
```
[1,5]
```
We iterate arr from back to front:
```
// arr[4] = 4, so we split intervals in queue in position 4
[1,3] [5,5]  // step: 1

// we check the length of intervals, and we find [5, 5] have satisfied m = 1
// so we just return 
n - step
```

**Complexity**
After each splitting, we add at most one interval at a time, so the worst case is $O(n^2)$.

Yes, it is slow compared to other method, but when I came up with it, I just code it within 10 minutes,
So it is like the first unoptimized method we may come up with in an interview.
Hope it may be helpful in some sort of ways.

**Complete Code**:
```
class Solution {
public:
    int findLatestStep(vector<int>& arr, int m) {
        int n = arr.size();
        queue<pair<int, int>> que;
        que.push({1, n}); // initial state
		
        int step = 0;
		// do it reversely
        for(int i = n - 1; i >= 0; --i){
			// we split the intervals in que each step
            int t = que.size();
            for(int j = 0; j < t; ++j){
                auto p = que.front();
                que.pop();
                int len = p.second - p.first + 1;
                if(len == m) return n - step; // we find the first (i.e. the lastest in terms of this question)
				
                if(len < m) continue; // we can accelerate by discarding the intervals with length less than m
				
				// split
                if(arr[i] >= p.first && arr[i] <= p.second){
					// same accelaration
                    if(arr[i] - p.first >= m) que.push({p.first, arr[i] - 1});
                    if(p.second - arr[i] >= m) que.push({arr[i] + 1, p.second});
                }else{
                    que.push(p);
                }
            }
            step++;
        }
        return -1;
    }
};
```
**Update**
Thanks for @aryan29, we can optimize the solution to $O(nlog(n))$ by using **set** instead of **queue** and perform binary search
```
class Solution {
public:
    int findLatestStep(vector<int>& arr, int m) {
        int n = arr.size();
        if(n == m) return n;
        
        set<pair<int, int>> st;
        st.insert({1, n});
        int step = 0;
        for(int i = n - 1; i >= 0; --i){
            if(st.empty()) break;
            
            auto it = st.upper_bound({arr[i], INT_MAX});
            --it;
            int l = it->first, r = it->second;
            st.erase(it);

            int len1 = arr[i] - l, len2 = r - arr[i];
            if(len1 == m || len2 == m) return n - step - 1;
            if(len1 >= 1) st.insert({l, arr[i] - 1});
            if(len2 >= 1) st.insert({arr[i] + 1, r});
            
            step++;
        }
        return -1;
    }
};
```
</p>


