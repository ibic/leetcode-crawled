---
title: "Fruit Into Baskets"
weight: 854
#id: "fruit-into-baskets"
---
## Description
<div class="description">
<p>In a row of trees, the <code>i</code>-th tree&nbsp;produces&nbsp;fruit with type&nbsp;<code>tree[i]</code>.</p>

<p>You <strong>start at any tree&nbsp;of your choice</strong>, then repeatedly perform the following steps:</p>

<ol>
	<li>Add one piece of fruit from this tree to your baskets.&nbsp; If you cannot, stop.</li>
	<li>Move to the next tree to the right of the current tree.&nbsp; If there is no tree to the right, stop.</li>
</ol>

<p>Note that you do not have any choice after the initial choice of starting tree:&nbsp;you must perform step 1, then step 2, then back to step 1, then step 2, and so on until you stop.</p>

<p>You have two baskets, and each basket can carry any quantity of fruit, but you want each basket to only carry one type of fruit each.</p>

<p>What is the total amount of fruit you can collect with this procedure?</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,2,1]</span>
<strong>Output: </strong><span id="example-output-1">3</span>
<strong><span>Explanation: </span></strong><span>We can collect [1,2,1].</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[0,1,2,2]</span>
<strong>Output: </strong><span id="example-output-2">3
</span><strong><span>Explanation: </span></strong><span>We can collect [1,2,2].
If we started at the first tree, we would only collect [0, 1].</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[1,2,3,2,2]</span>
<strong>Output: </strong><span id="example-output-3">4
</span><strong><span>Explanation: </span></strong><span>We can collect [2,3,2,2].</span>
<span>If we started at the first tree, we would only collect [1, 2].</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">[3,3,3,1,2,1,1,2,3,3,4]</span>
<strong>Output: </strong>5<span id="example-output-4">
</span><strong><span>Explanation: </span></strong><span>We can collect [1,2,1,1,2].</span>
<span>If we started at the first tree or the eighth tree, we would only collect 4 fruits.</span>
</pre>

<p>&nbsp;</p>
</div>
</div>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= tree.length &lt;= 40000</code></li>
	<li><code>0 &lt;= tree[i] &lt; tree.length</code></li>
</ol>

</div>

## Tags
- Two Pointers (two-pointers)

## Companies
- Google - 4 (taggedByAdmin: true)
- Amazon - 4 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Scan Through Blocks

**Intuition**

Equivalently, we want the longest subarray with at most two "types" (values of `tree[i]`).

Instead of considering each element individually, we can consider blocks of adjacent elements of the same type.

For example, instead of `tree = [1, 1, 1, 1, 2, 2, 3, 3, 3]`, we can say this is `blocks = [(1, weight = 4), (2, weight = 2), (3, weight = 3)]`.

Now say we brute forced, scanning from left to right.  We'll have something like `blocks = [1, _2_, 1, 2, 1, 2, _1_, 3, ...]` (with various weights).

The key insight is that when we encounter a `3`, we do not need to start from the second element `2` (marked `_2_` for convenience); we can start from the first element (`_1_`) before the `3`.  This is because if we started two or more elements before, the sequence must have types `1` and `2`, and that sequence is going to end at the `3`, and thus be shorter than anything we've already considered.

Since every starting point (that is the left-most index of a block) was considered, this solution is correct.

**Algorithm**

As the notation and strategy around implementing this differs between Python and Java, please see the inline comments for more details.

<iframe src="https://leetcode.com/playground/fPnRCQ5W/shared" frameBorder="0" width="100%" height="500" name="fPnRCQ5W"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `tree`.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Sliding Window

**Intuition**

As in *Approach 1*, we want the longest subarray with at most two different "types" (values of `tree[i]`).  Call these subarrays *valid*.

Say we consider all valid subarrays that end at index `j`.  There must be one with the smallest possible starting index `i`: lets say `opt(j) = i`.

Now the key idea is that `opt(j)` is a monotone increasing function.  This is because any subarray of a valid subarray is valid.

**Algorithm**

Let's perform a sliding window, keeping the loop invariant that `i` will be the smallest index for which `[i, j]` is a valid subarray.

We'll maintain `count`, the count of all the elements in the subarray.  This allows us to quickly query whether there are 3 types in the subarray or not.

<iframe src="https://leetcode.com/playground/ZPcT5ZbF/shared" frameBorder="0" width="100%" height="500" name="ZPcT5ZbF"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `tree`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Another poorly worded question
- Author: rootswoods
- Creation Date: Sun Sep 16 2018 11:11:59 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 05:31:19 GMT+0800 (Singapore Standard Time)

<p>
If I start from a tree I can\'t stop and have to put the fruit in a basket, but I want basket to have only one type of fruit. It is not clear if one needs to stop after a 3rd type of fruit is encountered.

Since about 4 contests there is at least one question that is harder to understand than to solve. Is it too hard to have someone proofread it before posting?
</p>


### Problem: Longest Subarray With 2 Elements
- Author: lee215
- Creation Date: Sun Sep 16 2018 11:05:10 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 18:05:24 GMT+0800 (Singapore Standard Time)

<p>
## **Problem**
"Start from any index, we can collect at most two types of fruits. What is the maximum amount"

##  **Translation**
Find out the longest length of subarrays with at most 2 different numbers?

Solution of sliding window will be easier to understand.
Here I share another solution wihtout hash map.
Hope it\'s not damn hard to understand.


##  **Explanation**:

Loop all fruit `c` in `tree`,
Note that `a` and `b` are the last two different types of fruit that we met,
`c` is the current fruit type,
so it\'s something like "....aaabbbc..."


**Case 1** `c == b`:
fruit `c` already in the basket,
and it\'s same as the last type of fruit
`cur += 1`
`count_b += 1`


**Case 2** `c == a`:
fruit `c` already in the basket,
but it\'s not same as the last type of fruit
`cur +=  1`
`count_b = 1`
`a = b, b = c`

**Case 3** `c != b && c!= a`:
fruit `c` not in the basket,
`cur = count_b + 1`
`count_b = 1`
`a = b, b = c`

Of course, in each turn we need to update `res = max(res, cur)`

## **Complexity**:
`O(N)` time, `O(1)` space

**C++:**
```
    int totalFruit(vector<int> tree) {
        int res = 0, cur = 0, count_b = 0, a = 0, b = 0;
        for (int c :  tree) {
            cur = c == a || c == b ? cur + 1 : count_b + 1;
            count_b = c == b ? count_b + 1 : 1;
            if (b != c) a = b, b = c;
            res = max(res, cur);
        }
        return res;
    }
```

**Java:**
```
    public int totalFruit(int[] tree) {
        int res = 0, cur = 0, count_b = 0, a = 0, b = 0;
        for (int c :  tree) {
            cur = c == a || c == b ? cur + 1 : count_b + 1;
            count_b = c == b ? count_b + 1 : 1;
            if (b != c) {a = b; b = c;}
            res = Math.max(res, cur);
        }
        return res;
    }
```
**Python:**
```
    def totalFruit(self, tree):
        res = cur = count_b = a = b = 0
        for c in tree:
            cur = cur + 1 if c in (a, b) else count_b + 1
            count_b = count_b + 1 if c == b else 1
            if b != c: a, b = b, c
            res = max(res, cur)
        return res
```

</p>


### [Java/C++/Python] Sliding Window for K Elements
- Author: lee215
- Creation Date: Sun Sep 16 2018 11:04:22 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 03 2019 22:17:12 GMT+0800 (Singapore Standard Time)

<p>
# **Problem**
"Start from any index, we can collect at most two types of fruits. What is the maximum amount"
<br>

# **Translation**
Find out the longest length of subarrays with at most 2 different numbers?
<br>

# **Explanation**
Solve with sliding window,
and maintain a hashmap `counter`,
which count the number of element between the two pointers.
Find more infinite similar prolems in the end.
<br>

**Java:**
Time `O(N)`, Space `O(N)`
```java
    public int totalFruit(int[] tree) {
        Map<Integer, Integer> count = new HashMap<>();
        int i = 0, j;
        for (j = 0; j < tree.length; ++j) {
            count.put(tree[j], count.getOrDefault(tree[j], 0) + 1);
            if (count.size() > 2) {
                count.put(tree[i], count.get(tree[i]) - 1);
                count.remove(tree[i++], 0);
            }
        }
        return j - i;
    }
```


**Java**
Time `O(N)`, Space `O(2)`
```java
    public int totalFruit2(int[] tree) {
        Map<Integer, Integer> count = new HashMap<Integer, Integer>();
        int res = 0, i = 0;
        for (int j = 0; j < tree.length; ++j) {
            count.put(tree[j], count.getOrDefault(tree[j], 0) + 1);
            while (count.size() > 2) {
                count.put(tree[i], count.get(tree[i]) - 1);
                if (count.get(tree[i]) == 0) count.remove(tree[i]);
                i++;
            }
            res = Math.max(res, j - i + 1);
        }
        return res;
    }
```

**C++:**
Time `O(N)`, Space `O(N)`
```cpp
    int totalFruit(vector<int> &tree) {
        unordered_map<int, int> count;
        int i, j;
        for (i = 0, j = 0; j < tree.size(); ++j) {
            count[tree[j]]++;
            if (count.size() > 2) {
                if (--count[tree[i]] == 0)count.erase(tree[i]);
                i++;
            }
        }
        return j - i;
    }
```

**Python:**
Time `O(N)`, Space `O(N)`
```python
    def totalFruit(self, tree):
        count, i = {}, 0
        for j, v in enumerate(tree):
            count[v] = count.get(v, 0) + 1
            if len(count) > 2:
                count[tree[i]] -= 1
                if count[tree[i]] == 0: del count[tree[i]]
                i += 1
        return j - i + 1
```
<br>

# More Similar Sliding Window Problems
Here are some similar sliding window problems.
Also find more explanations.
Good luck and have fun.

- 1248. [Count Number of Nice Subarrays](https://leetcode.com/problems/count-number-of-nice-subarrays/discuss/419378/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
- 1234. [Replace the Substring for Balanced String](https://leetcode.com/problems/replace-the-substring-for-balanced-string/discuss/408978/javacpython-sliding-window/367697)
- 1004. [Max Consecutive Ones III](https://leetcode.com/problems/max-consecutive-ones-iii/discuss/247564/javacpython-sliding-window/379427?page=3)
-  930. [Binary Subarrays With Sum](https://leetcode.com/problems/binary-subarrays-with-sum/discuss/186683/)
-  992. [Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/discuss/234482/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
-  904. [Fruit Into Baskets](https://leetcode.com/problems/fruit-into-baskets/discuss/170740/Sliding-Window-for-K-Elements)
-  862. [Shortest Subarray with Sum at Least K](https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/discuss/143726/C%2B%2BJavaPython-O(N)-Using-Deque)
-  209. [Minimum Size Subarray Sum](https://leetcode.com/problems/minimum-size-subarray-sum/discuss/433123/JavaC++Python-Sliding-Window)
<br>
</p>


