---
title: "Counting Elements"
weight: 1204
#id: "counting-elements"
---
## Description
<div class="description">
<p>Given an integer array <code>arr</code>, count how many elements&nbsp;<code>x</code>&nbsp;there are, such that <code>x + 1</code> is also in <code>arr</code>.</p>

<p>If there&#39;re duplicates in&nbsp;<code>arr</code>, count them seperately.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,3]
<strong>Output:</strong> 2
<strong>Explanation:</strong>&nbsp;1 and 2 are counted cause 2 and 3 are in arr.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,1,3,3,5,5,7,7]
<strong>Output:</strong> 0
<strong>Explanation:</strong>&nbsp;No numbers are counted, cause there&#39;s no 2, 4, 6, or 8 in arr.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,3,2,3,5,0]
<strong>Output:</strong> 3
<strong>Explanation:</strong>&nbsp;0, 1 and 2 are counted cause 1, 2 and 3 are in arr.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,1,2,2]
<strong>Output:</strong> 2
<strong>Explanation:</strong>&nbsp;Two 1s are counted cause 2 is in arr.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,1,2]
<strong>Output:</strong> 2
<strong>Explanation:</strong>&nbsp;Both 1s are counted because 2 is in the array.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 1000</code></li>
	<li><code>0 &lt;= arr[i] &lt;= 1000</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- DRW - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Search with Array

**Intuition**

The simplest way of solving this problem is to loop through each integer, `x`, checking whether or not it should be counted. This requires checking whether or not `x + 1` is in `arr`.

```text
define function count_elements(arr):
    count = 0
    for each x in arr:
        if integer_in_array(arr, x + 1):
            count = count + 1
    return count
```

To implement the `integer_in_array` function in the above algorithm, we can use **linear search**. To do a linear search, we need to loop through each integer of `arr`. If we find the integer that we're looking for, then return `true`. If we get to the end of `arr`, then we know the integer is not there, and so should return `false`.

```
define function integer_in_array(arr, target):
    for each x in arr:
        if target is equal to x:
            return true
    return false
```

Many programming languages have a built in function for checking whether or not an integer is in `arr`, e.g. Python.

**Algorithm**

<iframe src="https://leetcode.com/playground/m4jbby7y/shared" frameBorder="0" width="100%" height="395" name="m4jbby7y"></iframe>


**Complexity Analysis**

Let $$N$$ be the length of the input array, `arr`.

- Time complexity : $$O(N^2)$$.

    We loop through each of the $$N$$ integers `x`, checking whether or not `x + 1` is also in `arr`. Checking whether or not `x + 1` is in `arr` is done using linear search, which requires checking through all $$N$$ integers in `arr`. Because we're doing $$N$$ operations $$N$$ times, we get a time complexity of $$O(N^2)$$.

- Space complexity : $$O(1)$$.

    We are only using a constant number of single-value variables (e.g. `count`), giving us a space complexity of $$O(1)$$.

<br/>

---

#### Approach 2: Search with HashSet

**Intuition**

If you're not familiar with the `HashSet` data structure, check out our [Hash Tables Explore Card](https://leetcode.com/explore/learn/card/hash-table/) to get up to speed.

The above algorithm will work fine for the maximum array length we're given here. However, we can do a lot better than $$O(N^2)$$, and an interviewer will no doubt expect you to come up with a better way.

The reason why the algorithm above was so inefficient is because we're performing $$N$$ linear searches, each with a cost of $$O(N)$$. When we have an algorithm that is performing many linear searches to check for item existence, we should instead be looking to change the way the data is stored so that the time complexity of doing each search is less.

Recall that looking up items in a `HashSet` has a cost of $$O(1)$$. Creating a `HashSet` from an array of $$N$$ items has a cost of $$O(N)$$. We only need to create the `HashSet` *once*. After that, we can then replace all $$O(N)$$ linear searches with $$O(1)$$ `HashSet` lookups.

Before we go any further, here is an algorithm that is *incorrect*. Try to spot what the problem is; we'll discuss it just below.

```text
define function count_elements(arr):
    hash_set = a new HashSet
    add all integers of arr to hash_set
    count = 0
    for each x in hash_set:
        if hash_set contains x + 1:
            count = count + 1
    return count
```

Did you spot the bug? If there were duplicates in `arr`, then the `count` returned will be too low! 

Recall that a `HashSet` removes duplicates. Consider a case like `arr = [1, 1, 2]`. The `HashSet` will be `{1, 2}`. Therefore, the above code will loop over each integer in the `HashSet`, which is only *one* copy of `1`. Yet `arr` had *two* copies of `1`. 

To fix it, we need to loop over `arr`, but do the existence checks using the `HashSet`.

```text
define function count_elements(arr):
    hash_set = a new HashSet
    add all integers of arr to hash_set
    count = 0
    for each x in arr:
        if hash_set contains x + 1:
            count = count + 1
    return count
```

**Algorithm**

<iframe src="https://leetcode.com/playground/ksH3u6gu/shared" frameBorder="0" width="100%" height="276" name="ksH3u6gu"></iframe>

**Complexity Analysis**

Let $$N$$ be the length of the input array, `arr`.

- Time complexity : $$O(N)$$.

    Creating a `HashSet` from $$N$$ integers takes $$O(N)$$ time. We then need to loop over each of the $$N$$ integers like before, except this time we check for `x + 1` by seeing if it is in the `HashSet`; an $$O(1)$$ operation. This gives us a total time complexity of $$O(N) + N \cdot O(1) = O(N) + O(N) = O(N)$$.

- Space complexity : $$O(N)$$.

    The `HashSet` needs to store each unique integer from `arr`. In the worst case, all the integers in `arr` will be unique, meaning that the `HashSet` has a space complexity of $$O(N)$$.

It's interesting to note that $$O(N)$$ is an *upper bound* on the space complexity. If $$U$$ is the number of unique integers in `arr`, then the space complexity could more accurately be represented as $$O(U)$$.

<br/>

---

#### Approach 3: Search with Sorted Array

**Intuition**

Another way of changing the data storage to allow for more efficient searching is to sort it. Sorting has a time complexity of $$O(N \, \log \, N)$$, and searching for integers in a sorted array, using binary search, has a cost of $$O(\log \, N)$$. This will give us a total time complexity of $$O(N \, \log \, N)$$.

```text
define function countElements(arr):
    sort arr
    count = 0
    for each x in arr:
        binary search for x + 1 in arr
        if x + 1 is in arr:
            count = count + 1
    return count
```

The main challenge of this approach would be needing to implement your own binary search.

However, we don't actually need to use binary search! If we iterate over the sorted `arr`, then we know that if `x + 1` exists, it will be after all the copies of `x`. 

![Searching for x + 1 in sorted arr.](../Figures/10003/sorting.png)

Each copy of `x` should be counted if at least one copy of `x + 1` exists. Therefore, we can iterate down the sorted `arr`, keeping track of how many times the current `x` has appeared. When we get to a different integer, we can check if it's `x + 1`, and if it is, then the number of `x` we saw should be added to `count`.

```text
define function countElements(arr):
    sort arr
    count = 0
    run_length = 1
    for each i in range 1 to arr.length - 1:
        if arr[i - 1] is not equal to arr[i]:
            if arr[i - 1] + 1 is equal to arr[i]:
                count = count + run_length
            run_length = 0
        run_length = run_length + 1
    return count
```

Here is an animation of this approach.

!?!../Documents/10003_sorting_approach.json:960,183!?!

**Algorithm**

<iframe src="https://leetcode.com/playground/8shUpyzg/shared" frameBorder="0" width="100%" height="310" name="8shUpyzg"></iframe>

**Complexity Analysis**

- Time complexity : $$O(N \, \log \, N)$$.

    Sorting using a built-in sorting algorithm has a cost of $$O(N \, \log \, N)$$. After that, we do a single pass through `arr`, which has a cost of $$O(N)$$, giving us a total time complexity of $$O(N \, \log \, N) + O(N) = O(N \, \log \, N)$$.

- Space complexity : varies from $$O(N)$$ to $$O(1)$$.

    The space complexity of this approach is dependent on the space complexity of the sorting algorithm you're using. The space complexity of sorting algorithms built into programming languages is generally anywhere from $$O(N)$$ to $$O(1)$$.

    Notice that you could implement your own $$O(N \, \log \, N)$$ time complexity, $$O(1)$$ space complexity, sorting algorithm if needed. In practice, $$O(N \, \log \, N)$$ is not much worse than $$O(N)$$, and so this approach provides an interesting contrast to Approach 2 (which had a space complexity of $$O(N)$$).

<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### why this problem. This one is way easier than any interview problem.
- Author: li-_-il
- Creation Date: Tue Apr 07 2020 15:11:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 07 2020 15:19:52 GMT+0800 (Singapore Standard Time)

<p>
This problem is way too easy. Not sure about the reasoning to include this in the challenge.

I was so excited, because a mail was sent from Leetcode that on 7th 12 am pst, a brand new problem is released. So I was wide awake, anticipating this one.
</p>


### Python in 2 lines
- Author: BQP
- Creation Date: Tue Apr 07 2020 15:04:54 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 07 2020 15:04:54 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def countElements(self, arr: List[int]) -> int:
        C = collections.Counter(arr)
        return sum(C[x] for x in C if x+1 in C)
```
</p>


### [C++ STL] O(n), easy 4 lines just for fun)
- Author: manavrion
- Creation Date: Tue Apr 07 2020 16:11:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 07 2020 16:35:46 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
  int countElements(vector<int>& as) {
    auto exist = unordered_set<int>(as.begin(), as.end());
    return count_if(as.begin(), as.end(), [&](auto a){
      return exist.count(a + 1);
    });
  }
};

```
</p>


