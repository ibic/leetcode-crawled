---
title: "Valid Number"
weight: 65
#id: "valid-number"
---
## Description
<div class="description">
<p>Validate if a given string can be interpreted as&nbsp;a decimal number.</p>

<p>Some examples:<br />
<code>&quot;0&quot;</code> =&gt; <code>true</code><br />
<code>&quot; 0.1 &quot;</code> =&gt; <code>true</code><br />
<code>&quot;abc&quot;</code> =&gt; <code>false</code><br />
<code>&quot;1 a&quot;</code> =&gt; <code>false</code><br />
<code>&quot;2e10&quot;</code> =&gt; <code>true</code><br />
<code>&quot; -90e3&nbsp; &nbsp;&quot;</code> =&gt; <code>true</code><br />
<code>&quot; 1e&quot;</code> =&gt; <code>false</code><br />
<code>&quot;e3&quot;</code> =&gt; <code>false</code><br />
<code>&quot; 6e-1&quot;</code> =&gt; <code>true</code><br />
<code>&quot; 99e2.5&nbsp;&quot;</code> =&gt; <code>false</code><br />
<code>&quot;53.5e93&quot;</code> =&gt; <code>true</code><br />
<code>&quot; --6 &quot;</code> =&gt; <code>false</code><br />
<code>&quot;-+3&quot;</code> =&gt; <code>false</code><br />
<code>&quot;95a54e53&quot;</code> =&gt; <code>false</code></p>

<p><strong>Note:</strong> It is intended for the problem statement to be ambiguous. You should gather all requirements up front before implementing one. However, here is a list of characters that can be in a valid decimal number:</p>

<ul>
	<li>Numbers 0-9</li>
	<li>Exponent - &quot;e&quot;</li>
	<li>Positive/negative sign - &quot;+&quot;/&quot;-&quot;</li>
	<li>Decimal point - &quot;.&quot;</li>
</ul>

<p>Of course, the context of these characters also matters in the input.</p>

<p><strong>Update (2015-02-10):</strong><br />
The signature of the <code>C++</code> function had been updated. If you still see your function signature accepts a <code>const char *</code> argument, please click the reload button to reset your code definition.</p>

</div>

## Tags
- Math (math)
- String (string)

## Companies
- Facebook - 18 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- TripAdvisor - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### The worst problem i have ever met in this oj
- Author: aqin
- Creation Date: Mon Mar 03 2014 19:04:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 23:23:07 GMT+0800 (Singapore Standard Time)

<p>
The description do not give a clear explantion of the definition of a valid Number,  we just use more and more trick to get the right solution. It's too bad, it's waste of my time
</p>


### Clear Java solution with ifs
- Author: balint
- Creation Date: Sun Mar 01 2015 18:25:46 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 08:01:54 GMT+0800 (Singapore Standard Time)

<p>
All we need is to have a couple of flags so we can process the string in linear time:

    public boolean isNumber(String s) {
        s = s.trim();
        
        boolean pointSeen = false;
        boolean eSeen = false;
        boolean numberSeen = false;
        boolean numberAfterE = true;
        for(int i=0; i<s.length(); i++) {
            if('0' <= s.charAt(i) && s.charAt(i) <= '9') {
                numberSeen = true;
                numberAfterE = true;
            } else if(s.charAt(i) == '.') {
                if(eSeen || pointSeen) {
                    return false;
                }
                pointSeen = true;
            } else if(s.charAt(i) == 'e') {
                if(eSeen || !numberSeen) {
                    return false;
                }
                numberAfterE = false;
                eSeen = true;
            } else if(s.charAt(i) == '-' || s.charAt(i) == '+') {
                if(i != 0 && s.charAt(i-1) != 'e') {
                    return false;
                }
            } else {
                return false;
            }
        }
        
        return numberSeen && numberAfterE;
    }

We start with trimming.

 - If we see `[0-9]` we reset the number flags.
 - We can only see `.` if we didn't see `e` or `.`. 
 - We can only see `e` if we didn't see `e` but we did see a number. We reset numberAfterE flag.
 - We can only see `+` and `-` in the beginning and after an `e`
 - any other character break the validation.

At the and it is only valid if there was at least 1 number and if we did see an `e` then a number after it as well.

So basically the number should match this regular expression:

`[-+]?(([0-9]+(.[0-9]*)?)|.[0-9]+)(e[-+]?[0-9]+)?`
</p>


### A simple solution in Python based on DFA
- Author: aynamron
- Creation Date: Fri Nov 20 2015 06:26:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 08:35:22 GMT+0800 (Singapore Standard Time)

<p>
I was asked in the interview of linkedIn, writing it directly can be extremely complicated, for there are many special cases we have to deal with, and the code I wrote was messy. Then I failed to pass the interview. 

Here's a clear solution. With DFA we can easily get our idea into shape and then debug, and the source code is clear and simple.

```
class Solution(object):
  def isNumber(self, s):
      """
      :type s: str
      :rtype: bool
      """
      #define a DFA
      state = [{}, 
              {'blank': 1, 'sign': 2, 'digit':3, '.':4}, 
              {'digit':3, '.':4},
              {'digit':3, '.':5, 'e':6, 'blank':9},
              {'digit':5},
              {'digit':5, 'e':6, 'blank':9},
              {'sign':7, 'digit':8},
              {'digit':8},
              {'digit':8, 'blank':9},
              {'blank':9}]
      currentState = 1
      for c in s:
          if c >= '0' and c <= '9':
              c = 'digit'
          if c == ' ':
              c = 'blank'
          if c in ['+', '-']:
              c = 'sign'
          if c not in state[currentState].keys():
              return False
          currentState = state[currentState][c]
      if currentState not in [3,5,8,9]:
          return False
      return True
```


![enter image description here][1]


  [1]: http://normanyahq.github.io/static/files/valid_number_dfa.svg
</p>


