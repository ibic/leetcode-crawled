---
title: "Moving Stones Until Consecutive"
weight: 1012
#id: "moving-stones-until-consecutive"
---
## Description
<div class="description">
<p>Three stones are on a number line at positions <code>a</code>, <code>b</code>, and <code>c</code>.</p>

<p>Each turn, you pick up a stone at an endpoint (ie., either the lowest or highest position stone), and move it to an unoccupied position between those&nbsp;endpoints.&nbsp; Formally, let&#39;s say the stones are currently at positions <code>x, y, z</code> with <code>x &lt; y &lt; z</code>.&nbsp; You pick up the stone at either position <code>x</code> or position <code>z</code>, and move that stone to an integer position <code>k</code>, with <code>x &lt; k &lt; z</code> and <code>k != y</code>.</p>

<p>The game ends when you cannot make any more moves, ie. the stones are in consecutive positions.</p>

<p>When the game ends, what is the minimum and maximum number of moves that you could have made?&nbsp; Return the answer as an length 2 array: <code>answer = [minimum_moves, maximum_moves]</code></p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>a = <span id="example-input-1-1">1</span>, b = <span id="example-input-1-2">2</span>, c = <span id="example-input-1-3">5</span>
<strong>Output: </strong><span id="example-output-1">[1,2]</span>
<strong>Explanation: </strong>Move the stone from 5 to 3, or move the stone from 5 to 4 to 3.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>a = <span id="example-input-2-1">4</span>, b = <span id="example-input-2-2">3</span>, c = <span id="example-input-2-3">2</span>
<strong>Output: </strong><span id="example-output-2">[0,0]</span>
<strong>Explanation: </strong>We cannot make any moves.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>a = <span id="example-input-3-1">3</span>, b = <span id="example-input-3-2">5</span>, c = <span id="example-input-3-3">1</span>
<strong>Output: </strong><span id="example-output-3">[1,2]</span>
<strong>Explanation: </strong>Move the stone from 1 to 4; or move the stone from 1 to 2 to 4.
</pre>

<p>&nbsp;</p>
</div>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= a &lt;= 100</code></li>
	<li><code>1 &lt;= b &lt;= 100</code></li>
	<li><code>1 &lt;= c &lt;= 100</code></li>
	<li><code>a != b, b != c, c != a</code></li>
</ol>

<div>
<p>&nbsp;</p>

<div>
<div>&nbsp;</div>
</div>
</div>

</div>

## Tags
- Brainteaser (brainteaser)

## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java 4 lines
- Author: votrubac
- Creation Date: Sun Apr 28 2019 12:01:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 28 2019 12:01:43 GMT+0800 (Singapore Standard Time)

<p>
Edge case 1: all three stones are next to each other (```z - x == 2```). Return ```{0, 0}```.
Edge case 2: two stones are next to each other, or there is only one space in between. Minimum moves is ```1```.

Otherwise; minimum moves are ```2```, maximum - ```z - x - 2```.

So the position of the middle stone (```y```) only matters for the minimum moves. 
```
vector<int> numMovesStones(int a, int b, int c) {
  vector<int> s = { a, b, c };
  sort(begin(s), end(s));
  if (s[2] - s[0] == 2) return { 0, 0 };
  return { min(s[1] - s[0], s[2] - s[1]) <= 2 ? 1 : 2, s[2] - s[0] - 2 };
}
```
Java version:
```
public int[] numMovesStones(int a, int b, int c) {
  int[] s = { a, b, c };
  Arrays.sort(s);
  if (s[2] - s[0] == 2) return new int[] { 0, 0 };
  return new int[] { Math.min(s[1] - s[0], s[2] - s[1]) <= 2 ? 1 : 2, s[2] - s[0] - 2 };
}
```
</p>


### Simple Python O(1) code with explanation
- Author: hobiter
- Creation Date: Sun Apr 28 2019 12:02:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 28 2019 12:02:41 GMT+0800 (Singapore Standard Time)

<p>
Max is simple, just calculate the num of vacuum spots between the head - min(a,b,c) and tail -  max(a,b,c)
Min is max 2, since you can always move the head and tail to the neigbors of mid, with 2 exceptions:
	 a, there is only 1 vacuum btw either of the 2 numbers, then you just need to move the third number 1 step to the vacuum. for example, 1,3,8, then min is 1
	 b, abc is origially connective. min is 0
 
```
       def numMovesStones(self, a, b, c):
        maxi = max(a,b,c) - min(a,b,c) - 2
        mini = max((min(abs(a-b),abs(b-c),abs(a-c), 3) - 1), 1)
        if maxi == 0: mini = 0            
        return [mini,maxi]
</p>


### Clean Python, beats 100%
- Author: aquafie
- Creation Date: Mon Apr 29 2019 10:55:00 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 29 2019 10:55:00 GMT+0800 (Singapore Standard Time)

<p>
Good code should focus on readability instead of being as short as possible.

```
class Solution:
    def numMovesStones(self, a: int, b: int, c: int) -> List[int]:
        x, y, z = sorted([a, b, c])
        if x + 1 == y == z - 1:
            min_steps = 0
        elif y - x > 2 and z - y > 2:
            min_steps = 2
        else:
            min_steps = 1
        max_steps = z - x - 2
        return [min_steps, max_steps]
```
</p>


