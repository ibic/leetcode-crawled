---
title: "Rabbits in Forest"
weight: 718
#id: "rabbits-in-forest"
---
## Description
<div class="description">
<p>In a forest, each rabbit has some color. Some subset of rabbits (possibly all of them) tell you how many other rabbits have the same color as them. Those <code>answers</code> are placed in an array.</p>

<p>Return the minimum number of rabbits that could be in the forest.</p>

<pre>
<strong>Examples:</strong>
<strong>Input:</strong> answers = [1, 1, 2]
<strong>Output:</strong> 5
<strong>Explanation:</strong>
The two rabbits that answered &quot;1&quot; could both be the same color, say red.
The rabbit than answered &quot;2&quot; can&#39;t be red or the answers would be inconsistent.
Say the rabbit that answered &quot;2&quot; was blue.
Then there should be 2 other blue rabbits in the forest that didn&#39;t answer into the array.
The smallest possible number of rabbits in the forest is therefore 5: 3 that answered plus 2 that didn&#39;t.

<strong>Input:</strong> answers = [10, 10, 10]
<strong>Output:</strong> 11

<strong>Input:</strong> answers = []
<strong>Output:</strong> 0
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li><code>answers</code> will have length at most <code>1000</code>.</li>
	<li>Each <code>answers[i]</code> will be an integer in the range <code>[0, 999]</code>.</li>
</ol>

</div>

## Tags
- Hash Table (hash-table)
- Math (math)

## Companies
- Wish - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Count [Accepted]

**Intuition**

Each rabbit that says a different number must be a different color, and a rabbit only tells us information about rabbits of its color.  We can count the number of rabbits of each color separately.

Now, say `13` rabbits answer `5`.  What does this imply?  Say one of the rabbits is red.  We could have 5 other red rabbits in this group answering `5`, but not more.  So, say another rabbit in this group is a different color (blue).  We could have 5 other blue rabbits in this group answering `5`, but not more.  Finally, another rabbit must be a different color (green), and there are 5 other green rabbits (in the forest).

Notice there must be a minimum of 18 rabbits saying 5 (answering or in the forest), since there must be at least 3 unique colors among rabbits that answered or would have answered `5`, so it didn't matter that we chose the rabbits to be grouped by color or not when answering.

**Algorithm**

In general, if `v = count[k]` rabbits answered `k`, there must be at least `a` rabbits that answered or would have answered `k`, where `a` is the smallest multiple of `k+1` such that `a >= count[k]`.

We add all these `a`s together.

<iframe src="https://leetcode.com/playground/aDvqS6CG/shared" frameBorder="0" width="100%" height="242" name="aDvqS6CG"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of rabbits that answered.  In Java, our implementation using `int[]` instead of a `Map` would be $$O(N + W)$$, where $$W$$ is the number of possible different answers that rabbits could say.

* Space Complexity:  $$O(N)$$.  In Java, our implementation is $$O(W)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Easy and Concise Solution
- Author: lee215
- Creation Date: Sun Feb 11 2018 17:02:06 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 14:17:19 GMT+0800 (Singapore Standard Time)

<p>
## Solution 1
If ```x+1``` rabbits have same color, then we get ```x+1``` rabbits who all answer ```x```.
now n rabbits answer x.
If ```n % (x + 1) == 0```, we need ```n / (x + 1)``` groups of ```x + 1``` rabbits.
If ```n % (x + 1) != 0```, we need ```n / (x + 1) + 1``` groups of ```x + 1``` rabbits.

the number of groups is ```math.ceil(n / (x + 1))``` and it equals to ```(n + x) / (x + 1)``` , which is more elegant.

**Java**
```
    public int numRabbits(int[] answers) {
        Map<Integer, Integer> m = new HashMap<>();
        for (int i : answers)
            m.put(i, m.getOrDefault(i, 0) + 1);
        int res = 0;
        for (int i : m.keySet())
            res += (m.get(i) + i) / (i + 1) * (i + 1);
        return res;
    }
```
**C++**
```
    int numRabbits(vector<int>& answers) {
        unordered_map<int, int> c;
        for (int i : answers) c[i]++;
        int res = 0;
        for (auto i : c) res += (i.second + i.first) / (i.first + 1) * (i.first + 1);
        return res;
    }
```
**Python**
```
    def numRabbits(self, answers):
        c = collections.Counter(answers)
        return sum((c[i] + i) / (i + 1) * (i + 1) for i in c)
```

## Solution 2

Inspried by @sggkjihua,
We can count the `res` as we loop on the answers.

**Java**
```
    public int numRabbits(int[] answers) {
        int c[] = new int[1000], res = 0;
        for (int i : answers)
            if (c[i]++ % (i + 1) == 0)
                res += i + 1;
        return res;
    }
```
**C++**
```
    public int numRabbits(int[] answers) {
        int c[] = new int[1000], res = 0;
        for (int i : answers)
            if (c[i]++ % (i + 1) == 0)
                res += i + 1;
        return res;
    }
```
**Python**
```
    def numRabbits(self, answers):
        c = collections.Counter()
        res = 0
        for i in answers:
            if c[i] % (i + 1) == 0:
                res += i + 1
            c[i] += 1
        return res
```
</p>


### Java Solution with HashMap O(N) and comments
- Author: michellebecerra94
- Creation Date: Sun Feb 11 2018 12:21:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 11 2018 12:21:26 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int numRabbits(int[] answers) {
        if(answers.length == 0) return 0;
        
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        int sum = 0;
        
        //For each rabbits answer
        for(int i : answers){
            if(i == 0 ){
                sum += 1;
                continue;
            }
            if(!map.containsKey(i)){
                //If we haven't accounted for this rabbit color then account for the one telling us
                // as well as the one that rabbit says is that color.
                map.put(i, 0);
                sum += (i + 1);
                
            }else{
                map.put(i, map.get(i) + 1);
                //if there are k of each color then they are all present, remove them to allow the change to account for others.
                if(map.get(i) == i){ 
                    map.remove(i);
                }
            }
            
        }
        return sum;
    }
}

```
</p>


### My easy Java HashMap solution
- Author: bzdjsm
- Creation Date: Sun Feb 11 2018 13:01:26 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 04 2018 05:29:50 GMT+0800 (Singapore Standard Time)

<p>
```java
class Solution {
    public int numRabbits(int[] answers) {
        int res = 0;
        Map<Integer,Integer> map = new HashMap<>();
        for(int answer : answers){
            map.put(answer,map.getOrDefault(answer,0)+1);
        }
        for(Integer n : map.keySet()){
            int group = map.get(n)/(n+1);
            res += map.get(n)%(n+1) != 0 ? (group+1)*(n+1) : group*(n+1);
        }
        return res;
    }
}
```
</p>


