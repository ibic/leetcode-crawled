---
title: "Confusing Number"
weight: 978
#id: "confusing-number"
---
## Description
<div class="description">
<p>Given a number <code>N</code>, return <code>true</code> if and only if it is a <em>confusing number</em>, which satisfies the following condition:</p>

<p>We can rotate digits by 180 degrees to form new digits. When 0, 1, 6, 8, 9 are rotated 180 degrees, they become 0, 1, 9, 8, 6 respectively. When 2, 3, 4, 5 and 7 are rotated 180 degrees, they become invalid. A <em>confusing number</em> is a number that when rotated 180 degrees becomes a <strong>different</strong> number with each digit valid.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/03/23/1268_1.png" style="width: 180px; height: 90px;" /></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">6</span>
<strong>Output: </strong><span id="example-output-1">true</span>
<strong>Explanation: </strong>
We get <code>9</code> after rotating <code>6</code>, <code>9</code> is a valid number and <code>9!=6</code>.
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/03/23/1268_2.png" style="width: 180px; height: 90px;" /></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">89</span>
<strong>Output: </strong><span id="example-output-2">true</span>
<strong>Explanation: </strong>
We get <code>68</code> after rotating <code>89</code>, <code>86</code> is a valid number and <code>86!=89</code>.
</pre>

<p><strong>Example 3:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/03/26/1268_3.png" style="width: 301px; height: 121px;" /></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">11</span>
<strong>Output: </strong><span id="example-output-3">false</span>
<strong>Explanation: </strong>
We get <code>11</code> after rotating <code>11</code>, <code>11</code> is a valid number but the value remains the same, thus <code>11</code> is not a confusing number.
</pre>

<p><strong>Example 4:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/03/23/1268_4.png" style="width: 180px; height: 90px;" /></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">25</span>
<strong>Output: </strong><span id="example-output-4">false</span>
<strong>Explanation: </strong>
We get an invalid number after rotating <code>25</code>.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= N &lt;= 10^9</code></li>
	<li>After the rotation we can ignore leading zeros, for example if after rotation we have <code>0008</code>&nbsp;then this number is considered as just <code>8</code>.</li>
</ol>
</div>

## Tags
- Math (math)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution using HashMap - Similar to 246. Strobogrammatic Number
- Author: Makubex74
- Creation Date: Sun Jun 02 2019 12:47:45 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 24 2019 08:49:49 GMT+0800 (Singapore Standard Time)

<p>
* Store <digit, inverted digit> as <K, V> in HashMap.
* Keep dividing current number by 10 and check individual digit to see if it exists in the HashMap or return false if not found.
* Construct new number by multiplying new number by 10 each time and adding the inverted digit for the current remainder value.
* Check if original number and new number are the same.
```
class Solution {
    public boolean confusingNumber(int N) {
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        map.put(6, 9);
        map.put(9, 6);
        map.put(0, 0);
        map.put(1, 1);
        map.put(8, 8); 
        int newNum = 0;
        int x = N;
        while (x != 0) {
            int remainder = x % 10;
            if (!map.containsKey(remainder)) 
                return false;
            if(newNum > Integer.MAX_VALUE/10)
                return false;
            newNum = newNum * 10 + map.get(remainder);
            x /= 10;
        }    
        return N == newNum? false: true;
    }
}
```

</p>


### Python - map each digit to its rotation
- Author: yorkshire
- Creation Date: Sun Jun 02 2019 08:17:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 08:17:36 GMT+0800 (Singapore Standard Time)

<p>
Convert N to a string.
Map each digit of N to its rotation, returning False if no rotation.
Compare the reversed mapping to the original string.

```
	def confusingNumber(self, N):
        S = str(N)
        rotation = {"0" : "0", "1" : "1", "6" : "9", "8" : "8", "9" : "6"}
        result = []
        
        for c in S[::-1]:           # iterate in reverse
            if c not in rotation:
                return False
            result.append(rotation[c])
                
        return "".join(result) != S
```
</p>


### Why is 916 not a confusing number?
- Author: mmcclarty
- Creation Date: Sun Jun 02 2019 05:59:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 05:59:23 GMT+0800 (Singapore Standard Time)

<p>
916 says it should return False; but it seems to fit the criteria of confusing number (619 on rotation)?
</p>


