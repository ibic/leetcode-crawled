---
title: "Lemonade Change"
weight: 804
#id: "lemonade-change"
---
## Description
<div class="description">
<p>At a lemonade stand, each lemonade costs <code>$5</code>.&nbsp;</p>

<p>Customers are standing in a queue to buy from you, and order one at a time (in the order specified by <code>bills</code>).</p>

<p>Each customer will only buy one lemonade and&nbsp;pay with either a <code>$5</code>, <code>$10</code>, or <code>$20</code> bill.&nbsp; You must provide the correct change to each customer, so that the net transaction is that the customer pays $5.</p>

<p>Note that you don&#39;t have any change&nbsp;in hand at first.</p>

<p>Return <code>true</code>&nbsp;if and only if you can provide every customer with correct change.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[5,5,5,10,20]</span>
<strong>Output: </strong><span id="example-output-1">true</span>
<strong>Explanation: </strong>
From the first 3 customers, we collect three $5 bills in order.
From the fourth customer, we collect a $10 bill and give back a $5.
From the fifth customer, we give a $10 bill and a $5 bill.
Since all customers got correct change, we output true.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[5,5,10]</span>
<strong>Output: </strong><span id="example-output-2">true</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[10,10]</span>
<strong>Output: </strong><span id="example-output-3">false</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">[5,5,10,10,20]</span>
<strong>Output: </strong><span id="example-output-4">false</span>
<strong>Explanation: </strong>
From the first two customers in order, we collect two $5 bills.
For the next two customers in order, we collect a $10 bill and give back a $5 bill.
For the last customer, we can't give change of $15 back because we only have two $10 bills.
Since not every customer received correct change, the answer is false.
</pre>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ul>
	<li><code>0 &lt;= bills.length &lt;= 10000</code></li>
	<li><code>bills[i]</code>&nbsp;will be either&nbsp;<code>5</code>, <code>10</code>, or <code>20</code>.</li>
</ul>
</div>
</div>
</div>
</div>

</div>

## Tags
- Greedy (greedy)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Atlassian - 5 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Simulation

**Intuition and Algorithm**

Let's try to simulate giving change to each customer buying lemonade.  Initially, we start with no `five` dollar bills, and no `ten` dollar bills.

* If a customer brings a ＄5 bill, then we take it.

* If a customer brings a ＄10 bill, we must return a five dollar bill.  If we don't have a five dollar bill, the answer is `False`, since we can't make correct change.

* If a customer brings a ＄20 bill, we must return ＄15.

    * If we have a ＄10 and a ＄5, then we always prefer giving change in that, because it is strictly worse for making change than three ＄5 bills.

    * Otherwise, if we have three ＄5 bills, then we'll give that.

    * Otherwise, we won't be able to give ＄15 in change, and the answer is `False`.


<iframe src="https://leetcode.com/playground/pdYf9ahw/shared" frameBorder="0" width="100%" height="480" name="pdYf9ahw"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `bills`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Jul 01 2018 11:04:55 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 18 2019 13:43:28 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**:
When the customer gives us `$20`, we have two options:
1. To give three `$5` in return
2. To give one `$5` and one `$10`.

On insight is that the second option (if possible) is always better than the first one.
Because two `$5` in hand is always better than one `$10`
<br>

## **Explanation**:
Count the number of `$5` and `$10` in hand.

if (customer pays with `$5`) `five++`;
if (customer pays with `$10`) `ten++, five--`;
if (customer pays with `$20`) `ten--, five--` or `five -= 3`;

Check if five is positive, otherwise return false.
<br>

## **Time Complexity**
Time `O(N)` for one iteration
Space `O(1)`
<br>

**C++:**
```cpp
    int lemonadeChange(vector<int> bills) {
        int five = 0, ten = 0;
        for (int i : bills) {
            if (i == 5) five++;
            else if (i == 10) five--, ten++;
            else if (ten > 0) ten--, five--;
            else five -= 3;
            if (five < 0) return false;
        }
        return true;
    }
```

**Java:**
```java
    public boolean lemonadeChange(int[] bills) {
        int five = 0, ten = 0;
        for (int i : bills) {
            if (i == 5) five++;
            else if (i == 10) {five--; ten++;}
            else if (ten > 0) {ten--; five--;}
            else five -= 3;
            if (five < 0) return false;
        }
        return true;
    }
```
**Python:**
```py
    def lemonadeChange(self, bills):
        five = ten = 0
        for i in bills:
            if i == 5: five += 1
            elif i == 10: five, ten = five - 1, ten + 1
            elif ten > 0: five, ten = five - 1, ten - 1
            else: five -= 3
            if five < 0: return False
        return True
```

</p>


### Python simple & readable
- Author: cenkay
- Creation Date: Sun Jul 01 2018 11:34:21 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 21:34:32 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def lemonadeChange(self, bills):
        five = ten = 0
        for num in bills:
            if num == 5:
                five += 1
            elif num == 10 and five:
                ten += 1
                five -= 1
            elif num == 20 and five and ten:
                five -= 1
                ten -= 1
            elif num == 20 and five >= 3:
                five -= 3
            else:
                return False
        return True
```
</p>


### The answer why greedy is correct here
- Author: stoneMaster
- Creation Date: Mon May 20 2019 15:07:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 20 2019 15:07:53 GMT+0800 (Singapore Standard Time)

<p>
The key idea is "why greedy is correct  here" ?
When you receive a bill of 5 or 10, you have only one option: either return nothing(when receive 5) or return a bill of 5 (when receive 10).
But when you receive a bill of 20, you have two choices: return a 5 bill and a 10 bill, or just return 3 5 bill . The right idea is the former is better\uFF0C but Why? The answer is the 10 bill can only be returned if you receive a 20 bill, but you can returned 5 bill both when you receive 20 bill and 10 bill.  So the 5 bill is somehow more precious than the 10 bill . 

\u5173\u952E\u7684\u60F3\u6CD5\u662F\u201C\u4E3A\u4EC0\u4E48\u8D2A\u5A6A\u5728\u8FD9\u91CC\u662F\u6B63\u786E\u7684\u201D\uFF1F
\u5F53\u60A8\u6536\u52305\u621610\u7684\u8D26\u5355\u65F6\uFF0C\u60A8\u53EA\u6709\u4E00\u4E2A\u9009\u9879\uFF1A\u8981\u4E48\u4E0D\u8FD4\u56DE\uFF08\u5F53\u6536\u52305\uFF09\u8981\u4E48\u8FD4\u56DE5\uFF08\u5F53\u6536\u523010\uFF09\u7684\u8D26\u5355\u3002
\u4F46\u662F\u5F53\u4F60\u6536\u523020\u7684\u8D26\u5355\u65F6\uFF0C\u4F60\u6709\u4E24\u4E2A\u9009\u62E9\uFF1A\u9000\u8FD8\u4E00\u4E2A5\u5143\u8D26\u5355\u548C\u4E00\u4E2A10\u5143\u8D26\u5355\uFF0C\u6216\u8005\u53EA\u8FD4\u56DE3\u4E2A 5\u5143\u8D26\u5355\u3002 \u6B63\u786E\u7684\u60F3\u6CD5\u662F\u524D\u8005\u66F4\u597D\uFF0C\u4F46\u4E3A\u4EC0\u4E48\u5462\uFF1F \u7B54\u6848\u662F10\u5143\u8D26\u5355\u53EA\u6709\u5728\u4F60\u6536\u523020\u5143\u8D26\u5355\u65F6\u624D\u80FD\u5728\u9000\u8FD8\u65F6\u4F7F\u7528\uFF0C\u4F46\u662F\u5F53\u4F60\u6536\u523020\u5F20\u8D26\u5355\u548C10\u5F20\u8D26\u5355\u65F6\uFF0C\u4F60\u90FD\u53EF\u4EE5\u9000\u8FD85\u5143\u8D26\u5355\u3002\u6240\u4EE5\u8BF4\uFF0C \u5728\u67D0\u79CD\u7A0B\u5EA6\u4E0A\uFF0C 5\u5143\u8D26\u5355\u6BD415\u5143\u8D26\u5355\u66F4\u201C\u73CD\u8D35\u201D \u3002
</p>


