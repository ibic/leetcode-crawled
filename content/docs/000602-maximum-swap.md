---
title: "Maximum Swap"
weight: 602
#id: "maximum-swap"
---
## Description
<div class="description">
<p>
Given a non-negative integer, you could swap two digits <b>at most</b> once to get the maximum valued number. Return the maximum valued number you could get.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> 2736
<b>Output:</b> 7236
<b>Explanation:</b> Swap the number 2 and the number 7.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> 9973
<b>Output:</b> 9973
<b>Explanation:</b> No swap.
</pre>
</p>


<p><b>Note:</b><br>
<ol>
<li>The given number is in the range [0, 10<sup>8</sup>]</li>
</ol>
</p>
</div>

## Tags
- Array (array)
- Math (math)

## Companies
- Facebook - 17 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1: Brute Force [Accepted]

**Intuition**

The number only has at most 8 digits, so there are only $${}^{8}\text{C}_{2}$$ = 28 available swaps.  We can easily brute force them all.

**Algorithm**

We will store the candidates as lists of length $$\text{len(num)}$$.  For each candidate swap with positions $$\text{(i, j)}$$, we swap the number and record if the candidate is larger than the current answer, then swap back to restore the original number.

The only detail is possibly to check that we didn't introduce a leading zero.  We don't actually need to check it, because our original number doesn't have one.

<iframe src="https://leetcode.com/playground/9BbnzEUC/shared" frameBorder="0" name="9BbnzEUC" width="100%" height="462"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^3)$$, where $$N$$ is the total number of digits in the input number.  For each pair of digits, we spend up to $$O(N)$$ time to compare the final sequences.

* Space Complexity: $$O(N)$$, we keep each digit in the array $$\text{A}$$.

---

#### Approach #2: Greedy [Accepted]

**Intuition**

At each digit of the input number in order, if there is a larger digit that occurs later, we know that the best swap must occur with the digit we are currently considering.

**Algorithm**

We will compute $$\text{last[d] = i}$$, the index $$\text{i}$$ of the last occurrence of digit $$\text{d}$$ (if it exists).

Afterwards, when scanning the number from left to right, if there is a larger digit in the future, we will swap it with the largest such digit; if there are multiple such digits, we will swap it with the one that occurs the latest.

<iframe src="https://leetcode.com/playground/c2u3L78L/shared" frameBorder="0" name="c2u3L78L" width="100%" height="411"></iframe>

**Complexity Analysis**

Let $$N$$ be the total number of digits in the input number.

* Time Complexity:  $$O(N)$$.

    - Every digit is considered at most once.


* Space Complexity: $$O(N)$$.  
    
    - We keep each digit in the array of $$A$$, as in the approach #1.

    - The additional space used by the hashtable of `last` only has up to 10 values. Therefore, on this part, the space complexity is $$O(1)$$.

    - In total, the overall space complexity is $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java simple solution, O(n) time
- Author: joe53211
- Creation Date: Mon Sep 04 2017 01:18:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 05:32:13 GMT+0800 (Singapore Standard Time)

<p>
Use buckets to record the last position of digit 0 ~ 9 in this num.

Loop through the num array from left to right. For each position, we check whether there exists a larger digit in this num (start from 9 to current digit). We also need to make sure the position of this larger digit is behind the current one. If we find it, simply swap these two digits and return the result.

```
class Solution {
    public int maximumSwap(int num) {
        char[] digits = Integer.toString(num).toCharArray();
        
        int[] buckets = new int[10];
        for (int i = 0; i < digits.length; i++) {
            buckets[digits[i] - '0'] = i;
        }
        
        for (int i = 0; i < digits.length; i++) {
            for (int k = 9; k > digits[i] - '0'; k--) {
                if (buckets[k] > i) {
                    char tmp = digits[i];
                    digits[i] = digits[buckets[k]];
                    digits[buckets[k]] = tmp;
                    return Integer.valueOf(new String(digits));
                }
            }
        }
        
        return num;
    }
}
```
</p>


### C++ one-pass simple & fast solution: 1-3ms, O(n) time
- Author: TimmyLu926
- Creation Date: Thu Sep 28 2017 05:31:16 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 04 2018 05:57:15 GMT+0800 (Singapore Standard Time)

<p>
Actually this problem can be easily solved by only one pass from backward. During the scan, we only need to do 2 things:
1. record the largest digit (maxdigit) and its corresponding index (maxidx);
2. if the current digit is smaller than the largest digit, this digit and the largest digit are the best candidate for max swap so far. In this case, this digit pair is recorded (leftidx and rightidx).

```

    int maximumSwap(int num) {
        string numstr = std::to_string(num);

        int maxidx = -1; int maxdigit = -1;
        int leftidx = -1; int rightidx = -1;        

        for (int i = numstr.size() - 1; i >= 0; --i) {
            // current digit is the largest by far
            if (numstr[i] > maxdigit) {
                maxdigit = numstr[i];
                maxidx = i;
                continue;
            }

            // best candidate for max swap if there is no more 
            // such situation on the left side
            if (numstr[i] < maxdigit) {
                leftidx = i;
                rightidx = maxidx;
            }
        }

        // num is already in order
        if (leftidx == -1) return num;

        std::swap(numstr[leftidx], numstr[rightidx]);

        return std::stoi(numstr);
    }
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Sep 03 2017 11:39:13 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 26 2018 19:30:41 GMT+0800 (Singapore Standard Time)

<p>
The number only has 8 digits, so there are only 8 choose 2 = 28 available swaps.  Brute force them all for an `O(N^2)` solution which passes.

We will store the candidates as lists of length `len(num)`.  For each candidate swap with positions `(i, j)`, we swap the number and record if the candidate is larger than the current answer, then swap back to restore the original number.  The only detail is possibly to check that we didn't introduce a leading zero.  We don't actually need to check it, because our original number doesn't have one.

```python
def maximumSwap(self, num):
    A = list(str(num))
    ans = A[:]
    for i in xrange(len(A)):
        for j in xrange(i+1, len(A)):
            A[i], A[j] = A[j], A[i]
            if A > ans: ans = A[:]
            A[i], A[j] = A[j], A[i]

    return int("".join(ans))
```

---

We can also get an `O(N)` solution.  At each digit, if there is a larger digit that occurs later, we want the swap it with the largest such digit that occurs the latest.

```python
def maximumSwap(self, num):
    A = map(int, str(num))
    last = {x: i for i, x in enumerate(A)}
    for i, x in enumerate(A):
        for d in xrange(9, x, -1):
            if last.get(d, None) > i:
                A[i], A[last[d]] = A[last[d]], A[i]
                return int("".join(map(str, A)))
    return num
```
</p>


