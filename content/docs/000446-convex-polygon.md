---
title: "Convex Polygon"
weight: 446
#id: "convex-polygon"
---
## Description
<div class="description">
<p>Given a list of points that form a polygon when joined sequentially, find if this polygon is convex <a href="https://en.wikipedia.org/wiki/Convex_polygon" target="_blank">(Convex polygon definition)</a>.</p>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>There are at least 3 and at most 10,000 points.</li>
	<li>Coordinates are in the range -10,000 to 10,000.</li>
	<li>You may assume the polygon formed by given points is always a simple polygon<a href="https://en.wikipedia.org/wiki/Simple_polygon" target="_blank"> (Simple polygon definition)</a>. In other words, we ensure that exactly two edges intersect at each vertex, and that edges otherwise <b>don&#39;t intersect each other</b>.</li>
</ol>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
[[0,0],[0,1],[1,1],[1,0]]

Answer: True

Explanation:<img src="https://assets.leetcode.com/uploads/2018/10/13/polygon_convex.png" style="width: 100px; height: 100px;" />
</pre>

<p><b>Example 2:</b></p>

<pre>
[[0,0],[0,10],[10,10],[10,0],[5,5]]

Answer: False

Explanation:<img src="https://assets.leetcode.com/uploads/2018/10/13/polygon_not_convex.png" style="width: 100px; height: 100px;" />
</pre>

</div>

## Tags
- Math (math)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Beyond my knowledge... Java solution with in-line explanation
- Author: shawngao
- Creation Date: Sun Dec 04 2016 23:51:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 26 2018 06:36:26 GMT+0800 (Singapore Standard Time)

<p>
Well, I have to say that this problem is beyond my knowledge. @Ipeq1 and @zzg_zzm have explained how to solve this problem in their posts 
https://discuss.leetcode.com/topic/70643/i-believe-this-time-it-s-far-beyond-my-ability-to-get-a-good-grade-of-the-contest
https://discuss.leetcode.com/topic/70664/c-7-line-o-n-solution-to-check-convexity-with-cross-product-of-adajcent-vectors-detailed-explanation
The algorithm itself is not hard but I have no idea there exists such a way to determine if a polygon is convex or not. Laugh at me for my ignorance... I believe 90% of programmers can solve this problem if they were given the formula.
Anyway, following is the Java solution with in-line explanation. Accepted, 32ms. Reference: http://csharphelper.com/blog/2014/07/determine-whether-a-polygon-is-convex-in-c/

```
public class Solution {
    public boolean isConvex(List<List<Integer>> points) {
        // For each set of three adjacent points A, B, C, find the cross product AB \xb7 BC. If the sign of
        // all the cross products is the same, the angles are all positive or negative (depending on the
        // order in which we visit them) so the polygon is convex.
        boolean gotNegative = false;
        boolean gotPositive = false;
        int numPoints = points.size();
        int B, C;
        for (int A = 0; A < numPoints; A++) {
            // Trick to calc the last 3 points: n - 1, 0 and 1.
            B = (A + 1) % numPoints;
            C = (B + 1) % numPoints;
    
            int crossProduct =
                crossProductLength(
                    points.get(A).get(0), points.get(A).get(1),
                    points.get(B).get(0), points.get(B).get(1),
                    points.get(C).get(0), points.get(C).get(1));
            if (crossProduct < 0) {
                gotNegative = true;
            }
            else if (crossProduct > 0) {
                gotPositive = true;
            }
            if (gotNegative && gotPositive) return false;
        }
    
        // If we got this far, the polygon is convex.
        return true;
    }
    
    // Return the cross product AB x BC.
    // The cross product is a vector perpendicular to AB and BC having length |AB| * |BC| * Sin(theta) and
    // with direction given by the right-hand rule. For two vectors in the X-Y plane, the result is a
    // vector with X and Y components 0 so the Z component gives the vector's length and direction.
    private int crossProductLength(int Ax, int Ay, int Bx, int By, int Cx, int Cy)
    {
        // Get the vectors' coordinates.
        int BAx = Ax - Bx;
        int BAy = Ay - By;
        int BCx = Cx - Bx;
        int BCy = Cy - By;
    
        // Calculate the Z coordinate of the cross product.
        return (BAx * BCy - BAy * BCx);
    }
}
```
</p>


### java easy solution
- Author: yzj1212
- Creation Date: Sun Dec 25 2016 14:44:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 25 2016 14:44:31 GMT+0800 (Singapore Standard Time)

<p>
The solution is based on http://www.geeksforgeeks.org/orientation-3-ordered-points/
```
public boolean isConvex(List<List<Integer>> points) {
        int flag = 0;
        for (int i = 0; i < points.size(); i++) {
            int angle = getAngle(points, i);
            if (angle == 0) continue;
            if (flag == 0) flag = angle > 0 ? 1 : -1;
            else if (flag > 0 != angle > 0) return false;
        }
        return true;
    }
    
    private int getAngle(List<List<Integer>> points, int i) {
        List<Integer> c = points.get(i % points.size());
        List<Integer> b = points.get((i + 1) % points.size());
        List<Integer> a = points.get((i + 2) % points.size());
        return (a.get(1) - b.get(1)) * (b.get(0) - c.get(0)) - (b.get(1) - c.get(1)) * (a.get(0) - b.get(0));
    }
```
</p>


### C++ 5-liner O(N) checking convexity with cross product of adjacent vectors (detailed explanation)
- Author: zzg_zzm
- Creation Date: Sun Dec 04 2016 14:21:51 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 13 2018 13:14:38 GMT+0800 (Singapore Standard Time)

<p>
Great solution inspired by @Ipeq1! Here is a C++ version with extracted determinant calculation.

The key observation for convexity is that **vector p<sub>i+1</sub>-p<sub>i</sub> always turns to the same direction to p<sub>i+2</sub>-p<sub>i</sub> formed by any 3 sequentially adjacent vertices, i.e., cross product (p<sub>i+1</sub>-p<sub>i</sub>) x (p<sub>i+2</sub>-p<sub>i</sub>) does not change sign when traversing sequentially along polygon vertices**. 

Note that for any 2D vectors **v<sub>1</sub>**, **v<sub>2</sub>**,
* **v<sub>1</sub> x v<sub>2</sub> = det([v<sub>1</sub>, v<sub>2</sub>])**

which is the determinant of 2x2 matrix **[v<sub>1</sub>, v<sub>2</sub>]**. And the sign of **det([v<sub>1</sub>, v<sub>2</sub>])** represents the positive z-direction of right-hand system from **v<sub>1</sub>** to **v<sub>2</sub>**. So **det([v<sub>1</sub>, v<sub>2</sub>]) &ge; 0** if and only if **v<sub>1</sub>** turns at most 180 degrees **counterclockwise** to **v<sub>2</sub>**.
![0_1480993864673_Right_hand_rule_cross_product.png](/uploads/files/1480993854384-right_hand_rule_cross_product.png) 

**Version 1:** use `pos, neg` as `0-1` flags to store if positive or negative determinant has ever been encountered:
```
    bool isConvex(vector<vector<int>>& p) {
      for (int i=0, pos=0, neg=0, n=p.size(); i < n; ++i) {
        long det = det2({p[i], p[(i+1)%n], p[(i+2)%n]});
        if ((pos|=(det>0))*(neg|=(det<0))) return false;
      }    
      return true;
    }
    // determinant of 2x2 matrix [A1-A0, A2-A0]
    long det2(const vector<vector<int>>& A) {
    	return (A[1][0]-A[0][0])*(A[2][1]-A[0][1]) - (A[1][1]-A[0][1])*(A[2][0]-A[0][0]);
    }
```
**Version 2:** use `prev` and `cur` to store previous and current non-zero determinants:
```
    bool isConvex(vector<vector<int>>& p) {
      for (long i = 0, n = p.size(), prev = 0, cur; i < n; ++i)
        if (cur = det2({p[i], p[(i+1)%n], p[(i+2)%n]})) 
          if (cur*prev < 0) return false; 
          else prev = cur;

      return true;
    }
```
</p>


