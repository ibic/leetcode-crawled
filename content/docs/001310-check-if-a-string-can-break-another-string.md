---
title: "Check If a String Can Break Another String"
weight: 1310
#id: "check-if-a-string-can-break-another-string"
---
## Description
<div class="description">
<p>Given two strings: <code>s1</code> and <code>s2</code> with the same&nbsp;size, check if some&nbsp;permutation of string <code>s1</code> can break&nbsp;some&nbsp;permutation of string <code>s2</code> or vice-versa (in other words <code>s2</code> can break <code>s1</code>).</p>

<p>A string <code>x</code>&nbsp;can break&nbsp;string <code>y</code>&nbsp;(both of size <code>n</code>) if <code>x[i] &gt;= y[i]</code>&nbsp;(in alphabetical order)&nbsp;for all <code>i</code>&nbsp;between <code>0</code> and <code>n-1</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s1 = &quot;abc&quot;, s2 = &quot;xya&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong> &quot;ayx&quot; is a permutation of s2=&quot;xya&quot; which can break to string &quot;abc&quot; which is a permutation of s1=&quot;abc&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s1 = &quot;abe&quot;, s2 = &quot;acd&quot;
<strong>Output:</strong> false 
<strong>Explanation:</strong> All permutations for s1=&quot;abe&quot; are: &quot;abe&quot;, &quot;aeb&quot;, &quot;bae&quot;, &quot;bea&quot;, &quot;eab&quot; and &quot;eba&quot; and all permutation for s2=&quot;acd&quot; are: &quot;acd&quot;, &quot;adc&quot;, &quot;cad&quot;, &quot;cda&quot;, &quot;dac&quot; and &quot;dca&quot;. However, there is not any permutation from s1 which can break some permutation from s2 and vice-versa.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s1 = &quot;leetcodee&quot;, s2 = &quot;interview&quot;
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>s1.length == n</code></li>
	<li><code>s2.length == n</code></li>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
	<li>All strings consist of lowercase English letters.</li>
</ul>
</div>

## Tags
- String (string)
- Greedy (greedy)

## Companies
- endurance - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] O(n).
- Author: Chanchal_Maji
- Creation Date: Sun May 03 2020 00:08:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 00:23:47 GMT+0800 (Singapore Standard Time)

<p>
We just have to check, for all characters if count of all characters in `s1` is >= `s2` or vice versa.

```python
class Solution:
    def check(self, d1, d2):
        s = 0
        for c in \'abcdefghijklmnopqrstuvwxyz\':
            s += d1[c] - d2[c]
            if s < 0:
                return False
        return True
    
    def checkIfCanBreak(self, s1: str, s2: str) -> bool:
        d1 = collections.Counter(s1)
        d2 = collections.Counter(s2)
        return self.check(d1, d2) | self.check(d2, d1)
```

Time : `O(n)`.
Space : `O(1)`.
</p>


### [Java] Maintain a count array O(n)
- Author: manrajsingh007
- Creation Date: Sun May 03 2020 00:16:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 00:46:48 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public boolean checkIfCanBreak(String s1, String s2) {
        int n = s1.length();
        int[] arr = new int[26], brr = new int[26];
        for(int i = 0; i < n; i++) arr[s1.charAt(i) - 97]++;
        for(int i = 0; i < n; i++) brr[s2.charAt(i) - 97]++;
        int count1 = 0, count2 = 0;
        boolean f1 = false, f2 = false;
        for(int i = 0; i < 26; i++) {
            count1 += arr[i];
            count2 += brr[i];
            if(count1 > count2) {
                if(f2) return false;
                f1 = true;
            } else if(count2 > count1) {
                if(f1) return false;
                f2 = true;
            }
        }
        return true;
    }
}
</p>


### [Python] 1-line for fun
- Author: lee215
- Creation Date: Sun May 03 2020 00:06:13 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 26 2020 14:06:31 GMT+0800 (Singapore Standard Time)

<p>
Check if `sorted(s1) >= sorted(s2)` or `sorted(s1) <= sorted(s2)` one by one.
```
    def checkIfCanBreak(self, s1, s2):
        return not {1, -1}.issubset(set(cmp(a, b) for a, b in zip(sorted(s1), sorted(s2))))
```

@vaibhavD143
```
    def checkIfCanBreak(self, s1, s2):
        return all(x <= y for x, y in zip(*sorted([sorted(s1), sorted(s2)])))
```
</p>


