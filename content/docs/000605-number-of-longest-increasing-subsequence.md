---
title: "Number of Longest Increasing Subsequence"
weight: 605
#id: "number-of-longest-increasing-subsequence"
---
## Description
<div class="description">
<p>
Given an unsorted array of integers, find the number of longest increasing subsequence.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [1,3,5,4,7]
<b>Output:</b> 2
<b>Explanation:</b> The two longest increasing subsequence are [1, 3, 4, 7] and [1, 3, 5, 7].
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> [2,2,2,2,2]
<b>Output:</b> 5
<b>Explanation:</b> The length of longest continuous increasing subsequence is 1, and there are 5 subsequences' length is 1, so output 5.
</pre>
</p>

<p><b>Note:</b>
Length of the given array will be not exceed 2000 and the answer is guaranteed to be fit in 32-bit signed int.
</p>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Bloomberg - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Dynamic Programming

**Intuition and Algorithm**

Suppose for sequences ending at `nums[i]`, we knew the length `length[i]` of the longest sequence, and the number `count[i]` of such sequences with that length.

For every `i < j` with `A[i] < A[j]`, we might append `A[j]` to a longest subsequence ending at `A[i]`.  It means that we have demonstrated `count[i]` subsequences of length `length[i] + 1`.  

Now, if those sequences are longer than `length[j]`, then we know we have `count[i]` sequences of this length.  If these sequences are equal in length to `length[j]`, then we know that there are now `count[i]` additional sequences to be counted of that length (ie. `count[j] += count[i]`).

<iframe src="https://leetcode.com/playground/PhGwUhQn/shared" frameBorder="0" width="100%" height="500" name="PhGwUhQn"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$ where $$N$$ is the length of `nums`.  There are two for-loops and the work inside is $$O(1)$$.

* Space Complexity: $$O(N)$$, the space used by `lengths` and `counts`.
<br>
<br>

---
#### Approach 2: Segment Tree

**Intuition**

Suppose we knew for each length `L`, the number of sequences with length `L` ending in `x`.  Then when considering the next element of `nums`, updating our knowledge hinges on knowing the number of sequences with length `L-1` ending in `y < x`.  This type of query over an interval is a natural fit for using some sort of tree.

We could try using Fenwick trees, but we would have to store $$N$$ of them, which naively might be $$O(N^2)$$ space.  Here, we focus on an implementation of a Segment Tree.

**Algorithm**

Implementing Segment Trees is discussed in more detail [here](https://leetcode.com/articles/a-recursive-approach-to-segment-trees-range-sum-queries-lazy-propagation/).  In this approach, we will attempt a variant of segment tree that doesn't use lazy propagation.

First, let us call the "value" of an interval, the longest length of an increasing subsequence, and the number of such subsequences in that interval.

Each node knows about the interval of `nums` values it is considering `[node.range_left, node.range_right]`, and it knows about `node.val`, which contains information on the value of interval.  Nodes also have `node.left` and `node.right` children that represents the left and right half of the interval `node` considers.  These child nodes are created on demand as appropriate.

Now, `query(node, key)` will tell us the value of the interval specified by `node`, except we'll exclude anything above `key`.  When key is outside the interval specified by `node`, we return the answer.  Otherwise, we'll divide the interval into two and query both intervals, then `merge` the result.

What does `merge(v1, v2)` do?  Suppose two nodes specify adjacent intervals, and have corresponding values `v1 = node1.val, v2 = node2.val`.  What should the aggregate value, `v = merge(v1, v2)` be?  If there are longer subsequences in one node, then `v` will just be that.  If both nodes have longest subsequences of equal length, then we should count subsequences in both nodes.  Note that we did not have to consider cases where larger subsequences were made, since these were handled by `insert`.

What does `insert(node, key, val)` do?  We repeatedly insert the `key` into the correct half of the interval that `node` specifies (possibly a point), and after such insertion this node's value could change, so we merge the values together again.

Finally, in our main algorithm, for each `num in nums` we `query` for the value `length, count` of the interval below `num`, and we know it will lead to `count` additional sequences of length `length + 1`.  We then update our tree with that knowledge.

<iframe src="https://leetcode.com/playground/QtZndJQh/shared" frameBorder="0" width="100%" height="500" name="QtZndJQh"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N\log {N})$$ where $$N$$ is the length of `nums`.  In our main for loop, we do $$O(\log{N})$$ work to `query` and `insert`.

* Space Complexity: $$O(N)$$, the space used by the segment tree.
<br>
<br>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] Simple dp solution with explanation
- Author: caihao0727mail
- Creation Date: Sun Sep 10 2017 11:46:34 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 13:09:48 GMT+0800 (Singapore Standard Time)

<p>
The idea is to use two arrays ```len[n]``` and ```cnt[n]``` to record the maximum length of Increasing Subsequence and the coresponding number of these sequence which ends with ```nums[i]```, respectively. That is:

```len[i]```: the length of the Longest Increasing Subsequence which ends with ```nums[i]```.
```cnt[i]```: the number of the Longest Increasing Subsequence which ends with ```nums[i]```.

Then, the result is the sum of each ```cnt[i]``` while its corresponding ```len[i]``` is the maximum length.

Java version:
```
public int findNumberOfLIS(int[] nums) {
        int n = nums.length, res = 0, max_len = 0;
        int[] len =  new int[n], cnt = new int[n];
        for(int i = 0; i<n; i++){
            len[i] = cnt[i] = 1;
            for(int j = 0; j <i ; j++){
                if(nums[i] > nums[j]){
                    if(len[i] == len[j] + 1)cnt[i] += cnt[j];
                    if(len[i] < len[j] + 1){
                        len[i] = len[j] + 1;
                        cnt[i] = cnt[j];
                    }
                }
            }
            if(max_len == len[i])res += cnt[i];
            if(max_len < len[i]){
                max_len = len[i];
                res = cnt[i];
            }
        }
        return res;
    }
```

C++ version: (use ```vector<pair<int, int>> dp``` to combine ```len[]``` and ```cnt[]```)
```
    int findNumberOfLIS(vector<int>& nums) {
        int n = nums.size(), res = 0, max_len = 0;
        vector<pair<int,int>> dp(n,{1,1});            //dp[i]: {length, number of LIS which ends with nums[i]}
        for(int i = 0; i<n; i++){
            for(int j = 0; j <i ; j++){
                if(nums[i] > nums[j]){
                    if(dp[i].first == dp[j].first + 1)dp[i].second += dp[j].second;
                    if(dp[i].first < dp[j].first + 1)dp[i] = {dp[j].first + 1, dp[j].second};
                }
            }
            if(max_len == dp[i].first)res += dp[i].second;
            if(max_len < dp[i].first){
                max_len = dp[i].first;
                res = dp[i].second;
            }
        }
        return res;
    }
```
</p>


### 9ms [C++] Explanation: DP + Binary search + prefix sums O(NlogN) time; O(N) space
- Author: sobkuliak
- Creation Date: Wed Nov 29 2017 09:03:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 23:09:11 GMT+0800 (Singapore Standard Time)

<p>
The idea is to modify classic [LIS solution which uses binary search]( https://en.wikipedia.org/wiki/Longest_increasing_subsequence) to find the "insertion point" of a currently processed value. At ```dyn[k]``` we **don\'t store** a single number representing the smallest value such that there exists a LIS of length ```k+1``` as in classic LIS solution. Instead, at ```dyn[k]``` we store all such values that were once endings of a ```k+1``` LIS (so we keep the history as well). 
These values are held in the first part of the pairs in  ```vector<pair<int,int>>``` which we get by indexing ```dyn``` vector. So for example in a pair ```x = {a, b}``` the first part -- ```a```, indicates that there exists a LIS of length ```k+1``` such that it ends with a value ```a```. The second part -- ```b```, represents the number of possible options for which LIS of length ```k+1``` ends with a value equal to or greater than ```a```. This is the place where we use prefix sums. 
If we want to know how many options do we have to end a LIS of length ```m``` with value ```y```, we just binary search for the index ```i``` of a pair with first part strictly less than ```y``` in ```dyn[m-2]```. Then the number of options is ```dyn[m-2].back().second - dyn[m-2][i-1].second``` or just ```dyn[m-2].back()``` if ```i``` is ```0```.
That is the basic idea, the running time is O(NlogN), because we just do 2 binary searches for every element of the input. Space complexity is O(N), as every element of the input will be contained in the ```dyn``` vector exactly once.
Feel free to post any corrections or simpler explanations :)
```
class Solution {
public:
    int findNumberOfLIS(vector<int>& nums) {
        if (nums.empty())
            return 0;
        
        vector<vector<pair<int, int>>> dyn(nums.size() + 1);
        int max_so_far = 0;
        for (int i = 0; i < nums.size(); ++i) {
            // bsearch insertion point
            int l = 0, r = max_so_far;
            while (l < r) {
                int mid = l + (r - l) / 2;
                if (dyn[mid].back().first < nums[i]) {
                    l = mid + 1;
                } else {
                    r = mid;
                }
            }
            
            // bsearch number of options
            int options = 1;
            int row = l - 1;
            if (row >= 0) {
                int l1 = 0, r1 = dyn[row].size();
                while (l1 < r1) {
                    int mid = l1 + (r1 - l1) / 2;
                    if (dyn[row][mid].first < nums[i]) {
                        r1 = mid;
                    } else {
                        l1 = mid + 1;
                    }
                }
                
                options = dyn[row].back().second;
                options -= (l1 == 0) ? 0 : dyn[row][l1 - 1].second;
            }
            
            
            dyn[l].push_back({nums[i], (dyn[l].empty() ? options : dyn[l].back().second + options)});
            if (l == max_so_far) {
                max_so_far++;
            }
        }
        
        return dyn[max_so_far-1].back().second;
        
    }
};
```
</p>


### C++, DP with explanation, O(n^2)
- Author: zestypanda
- Creation Date: Sun Sep 10 2017 11:03:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 22 2018 05:40:02 GMT+0800 (Singapore Standard Time)

<p>
The solution is based on DP. 
```
For a sequence of numbers,
cnt[k] is total number of longest subsequence ending with nums[k];
len[k] is the length of longest subsequence ending with nums[k];
```
Then we have following equations 
```
len[k+1] = max(len[k+1], len[i]+1) for all i <= k and nums[i] < nums[k+1];
cnt[k+1] = sum(cnt[i]) for all i <= k and nums[i] < nums[k+1] and len[i] = len[k+1]-1;
```
Starting case and default case: cnt[0] = len[0] = 1;
```
class Solution {
public:
    int findNumberOfLIS(vector<int>& nums) {
        int n = nums.size(), maxlen = 1, ans = 0;
        vector<int> cnt(n, 1), len(n, 1);
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) {
                    if (len[j]+1 > len[i]) {
                        len[i] = len[j]+1;
                        cnt[i] = cnt[j];
                    }
                    else if (len[j]+1 == len[i]) 
                        cnt[i] += cnt[j];
                }
            }
            maxlen = max(maxlen, len[i]);
        }
        // find the longest increasing subsequence of the whole sequence
       // sum valid counts
        for (int i = 0; i < n; i++) 
            if (len[i] == maxlen) ans += cnt[i];
        return ans;
    }
};

```
</p>


