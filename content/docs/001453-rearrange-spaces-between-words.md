---
title: "Rearrange Spaces Between Words"
weight: 1453
#id: "rearrange-spaces-between-words"
---
## Description
<div class="description">
<p>You are given a string <code>text</code> of words that are placed among some number of spaces. Each word consists of one or more lowercase English letters and are separated by at least one space. It&#39;s guaranteed that <code>text</code> <strong>contains at least one word</strong>.</p>

<p>Rearrange the spaces so that there is an <strong>equal</strong> number of spaces between every pair of adjacent words and that number is <strong>maximized</strong>. If you cannot redistribute all the spaces equally, place the <strong>extra spaces at the end</strong>, meaning the returned string should be the same length as <code>text</code>.</p>

<p>Return <em>the string after rearranging the spaces</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;  this   is  a sentence &quot;
<strong>Output:</strong> &quot;this   is   a   sentence&quot;
<strong>Explanation: </strong>There are a total of 9 spaces and 4 words. We can evenly divide the 9 spaces between the words: 9 / (4-1) = 3 spaces.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> text = &quot; practice   makes   perfect&quot;
<strong>Output:</strong> &quot;practice   makes   perfect &quot;
<strong>Explanation:</strong>&nbsp;There are a total of 7 spaces and 3 words. 7 / (3-1) = 3 spaces plus 1 extra space. We place this extra space at the end of the string.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;hello   world&quot;
<strong>Output:</strong> &quot;hello   world&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;  walks  udp package   into  bar a&quot;
<strong>Output:</strong> &quot;walks  udp  package  into  bar  a &quot;
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;a&quot;
<strong>Output:</strong> &quot;a&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= text.length &lt;= 100</code></li>
	<li><code>text</code>&nbsp;consists of lowercase English letters and&nbsp;<code>&#39; &#39;</code>.</li>
	<li><code>text</code>&nbsp;contains at least one word.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C#/C++ Count Spaces
- Author: votrubac
- Creation Date: Sun Sep 20 2020 12:15:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 20 2020 14:36:27 GMT+0800 (Singapore Standard Time)

<p>
I feel like C# is the best language to solve such problems. Not sure how to make the C++ solution less hideous...

**C#**
```csharp
public string ReorderSpaces(string text) {
    var spaces = text.Count(Char.IsWhiteSpace);
    var words = text.Split(\' \', StringSplitOptions.RemoveEmptyEntries);
    int gap = words.Count() == 1 ? 0 : spaces / (words.Count() - 1);
    return string.Join(new String(\' \', gap), words) + new String(\' \', spaces - gap * (words.Count() - 1));
}
```

**C++**
```cpp
string reorderSpaces(string text) {
    int spaces = 0;
    string word, s;
    vector<string> words;
    for (auto ch : text) {
        if (ch == \' \') {
            ++spaces;
            if (!word.empty())
                words.push_back(word);
            word = "";
        }
        else
            word += ch;
    }
    if (!word.empty())
        words.push_back(word);
    if (words.size() == 1)
        return words.back() + string(spaces, \' \');
    int gap = spaces / (words.size() - 1), suff = spaces % (words.size() - 1);
    for (auto w : words)
        s += w + string(gap, \' \');
    return s.substr(0, s.size() - gap) + string(suff, \' \');
}
```
</p>


### [Java/Python 3] 6 liners w/ analysis.
- Author: rock
- Creation Date: Sun Sep 20 2020 13:18:08 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 08 2020 18:54:54 GMT+0800 (Singapore Standard Time)

<p>

Similar solution:

[929. Unique Email Addresses](https://leetcode.com/problems/unique-email-addresses/discuss/186798/JavaPython-3-7-and-6-liners-with-comment-and-analysis.)

----


```java
    public String reorderSpaces(String text) {
     // var words = text.trim().split("\\s+");
        String[] words = text.trim().split("\\s+");
        int cnt = words.length;
        int spaces = text.chars().map(c -> c == \' \' ? 1 : 0).sum();
        int gap = cnt <= 1 ? 0 : spaces / (cnt - 1); 
     // int trailingSpaces = gap == 0 ? spaces : spaces % (cnt - 1);
        int trailingSpaces = spaces - gap * (cnt - 1); // credit to @madno
        return String.join(" ".repeat(gap), words) + " ".repeat(trailingSpaces);
    }
```
```python
    def reorderSpaces(self, text: str) -> str:
        words = text.split() # split(sep=None) will discard empty strings.
        cnt = len(words)
        spaces = text.count(\' \')
        gap = 0 if cnt == 1 else spaces // (cnt - 1)
      # trailing_spaces = spaces if gap == 0 else spaces % (cnt - 1)
        trailing_spaces = spaces - gap * (cnt - 1) # credit to @madno
        return (\' \' * gap).join(words) + \' \' * trailing_spaces        
```
**Analysis:**

Time & space: O(n), where n = text.length().

----


**Q & A**

**Q1**: What is Java metacharacter?
**A1**: A metacharacter \u2014 a character with special meaning interpreted by the Java regular expression engine / matcher.
https://en.wikipedia.org/wiki/Metacharacter
https://docs.oracle.com/javase/tutorial/essential/regex/literals.html. 

**Q2**: Why does Java regular expression use `\\`, instead of `\`, to escape metacharacter such as `+, ., *`, etc?

**A2**: I guess the reason is that the backslash character is an escape character in Java String literals already. 

**Update:**

Credit to **@helengu1996** for her [link](https://stackoverflow.com/a/25145826/4150692):

*"There are two "escapings" going on here. The first backslash is to escape the second backslash for the Java language, to create an actual backslash character. The backslash character is what escapes the + or the s for interpretation by the regular expression engine. That\'s why you need two backslashes -- one for Java, one for the regular expression engine. With only one backslash, Java reports \s and \+ as illegal escape characters -- not for regular expressions, but for an actual character in the Java language."*

**Q3**: What is the key word `var`?
**A3**: Java 10 introduce **local variable type inference** with the key word `var`, see [here](https://docs.oracle.com/en/java/javase/13/language/local-variable-type-inference.html).

**Q4**: What is `"\\s+"`? What does `var words = text.trim().split("\\s+");` mean?
**A4**:`"\\s+"`  is Java regular expression (regex); `"\\s"` stands for a single space and `+` is a quantifier in a regex, and it indicates **one or more** `[the char or expression]` right before it;
The whole statement is used to trim the leading and trailing spaces of `text`, then to split it into words by space(s).

**End of Q & A.**

</p>


### Python simple 7 lines solution
- Author: michaelyoussrysaeed
- Creation Date: Sun Sep 20 2020 12:05:32 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 21 2020 05:08:43 GMT+0800 (Singapore Standard Time)

<p>
```python
class Solution:
    def reorderSpaces(self, text: str) -> str:
        spaces = text.count(\' \')
        words = text.split()
        if len(words) == 1:
            return words[0] + (\' \' * spaces)
		
		sep_count = len(words) - 1
        spaces_between_words = spaces // sep_count
        spaces_in_the_end = spaces % sep_count
        return (\' \' * spaces_between_words).join(words) + (\' \' * spaces_in_the_end)
```
</p>


