---
title: "One Edit Distance"
weight: 161
#id: "one-edit-distance"
---
## Description
<div class="description">
<p>Given two strings <code>s</code>&nbsp;and <code>t</code>, return <code>true</code> if they are both one edit distance apart, otherwise return <code>false</code>.</p>

<p>A string <code>s</code> is said to be one distance apart from a string <code>t</code> if you can:</p>

<ul>
	<li>Insert <strong>exactly one</strong> character into <code>s</code>&nbsp;to get&nbsp;<code>t</code>.</li>
	<li>Delete <strong>exactly one</strong> character from&nbsp;<code>s</code>&nbsp;to get&nbsp;<code>t</code>.</li>
	<li>Replace <strong>exactly one</strong> character of&nbsp;<code>s</code>&nbsp;with <strong>a different character</strong> to get&nbsp;<code>t</code>.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;ab&quot;, t = &quot;acb&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong> We can insert &#39;c&#39; into s&nbsp;to get&nbsp;t.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;&quot;, t = &quot;&quot;
<strong>Output:</strong> false
<strong>Explanation:</strong> We cannot get t from s by only one step.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;a&quot;, t = &quot;&quot;
<strong>Output:</strong> true
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;&quot;, t = &quot;A&quot;
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= s.length &lt;= 10<sup>4</sup></code></li>
	<li><code>0 &lt;= t.length &lt;= 10<sup>4</sup></code></li>
	<li><code>s</code> and <code>t</code> consist of lower-case letters, upper-case letters <strong>and/or</strong> digits.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Facebook - 3 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Twitter - 3 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- Snapchat - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: One pass algorithm

**Intuition**

First of all, let's ensure that the string lengths are not too far from each 
other. If the length difference is 2 or more characters, then `s` and `t` couldn't be 
one edit away strings.

![compute](../Figures/161/one_away.png)

For the next let's assume that `s` is always shorter or the same length as `t`.
If not, one could always call `isOneEditDistance(t, s)` to inverse the string order.

Now it's time to pass along the strings and to look for the first different character.

If there is no differences between the first `len(s)` characters, 
only two situations are possible :

- The strings are equal.

- The strings are one edit away distance.

![compute](../Figures/161/161_pic2.png)

Now what if there _is_ a different character so that `s[i] != t[i]`. 

- If the strings are of the same length, _all_ next 
characters should be equal to keep one edit away distance. To verify it,
one has to compare the substrings of `s` and `t` 
both starting from the `i + 1`th character. 

- If `t` is one character longer than `s`, 
the additional character `t[i]` should be the only difference
between both strings. To verify it, one has to compare
a substring of `s` starting from the `i`th character and 
a substring of `t` starting from the `i + 1`th character.

![compute](../Figures/161/161_pic3.png)

**Implementation**

<iframe src="https://leetcode.com/playground/Hd4cXKBY/shared" frameBorder="0" width="100%" height="500" name="Hd4cXKBY"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ in the worst case when
string lengths are close enough `abs(ns - nt) <= 1`, 
where `N` is a number of characters in the longest string. 
$$\mathcal{O}(1)$$ in the best case when `abs(ns - nt) > 1`.
 
* Space complexity : $$\mathcal{O}(N)$$ because strings are immutable
in Python and Java and to create substring costs $$\mathcal{O}(N)$$ space. 

**Problem generalization : Edit distance**

[Given two words word1 and word2, find the minimum number of operations required to convert word1 to word2.](https://leetcode.com/articles/edit-distance/)

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My CLEAR JAVA solution with explanation
- Author: Cheng_Zhang
- Creation Date: Mon Nov 23 2015 12:41:54 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 14:35:18 GMT+0800 (Singapore Standard Time)

<p>
    /*
     * There're 3 possibilities to satisfy one edit distance apart: 
     * 
     * 1) Replace 1 char:
     	  s: a B c
     	  t: a D c
     * 2) Delete 1 char from s: 
    	  s: a D  b c
    	  t: a    b c
     * 3) Delete 1 char from t
    	  s: a   b c
    	  t: a D b c
     */
    public boolean isOneEditDistance(String s, String t) {
        for (int i = 0; i < Math.min(s.length(), t.length()); i++) { 
        	if (s.charAt(i) != t.charAt(i)) {
        		if (s.length() == t.length()) // s has the same length as t, so the only possibility is replacing one char in s and t
        			return s.substring(i + 1).equals(t.substring(i + 1));
    			else if (s.length() < t.length()) // t is longer than s, so the only possibility is deleting one char from t
    				return s.substring(i).equals(t.substring(i + 1));	        	
    			else // s is longer than t, so the only possibility is deleting one char from s
    				return t.substring(i).equals(s.substring(i + 1));
        	}
        }       
        //All previous chars are the same, the only possibility is deleting the end char in the longer one of s and t 
        return Math.abs(s.length() - t.length()) == 1;        
    }
</p>


### Easy understood Java solution
- Author: zq670067
- Creation Date: Tue Aug 25 2015 15:45:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 13:19:10 GMT+0800 (Singapore Standard Time)

<p>
    public boolean isOneEditDistance(String s, String t) {
        if(Math.abs(s.length()-t.length()) > 1) return false;
        if(s.length() == t.length()) return isOneModify(s,t);
        if(s.length() > t.length()) return isOneDel(s,t);
        return isOneDel(t,s);
    }
    public boolean isOneDel(String s,String t){
        for(int i=0,j=0;i<s.length() && j<t.length();i++,j++){
            if(s.charAt(i) != t.charAt(j)){
                return s.substring(i+1).equals(t.substring(j));
            }
        }
        return true;
    }
    public boolean isOneModify(String s,String t){
        int diff =0;
        for(int i=0;i<s.length();i++){
            if(s.charAt(i) != t.charAt(i)) diff++;
        }
        return diff==1;
    }
</p>


### C++ DP
- Author: jianchao-li
- Creation Date: Fri Jun 26 2015 23:15:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 08:44:56 GMT+0800 (Singapore Standard Time)

<p>
The edit distance in this problem is the *Levenshtein distance*: *insertion*, *deletion* and *substitution* are all of the same cost. Thus, the edit distance is commutative.

For two strings ***s*** and ***t*** to be one edit distance apart, if they are of the same length, we can substitute one character in one string to get the other; if they are of length one apart, we can delete one character from the longer one to get the shorter one. These are all the possibilities.

We traverse the two strings, once we find a mismatch, we either do a substituion (nothing happens) or a deletion in the longer one (the pointer to the longer string is moved forward by one step). Then we check whether the two remaining substrings are equal. For edge cases like `s = "ab"` and `t = "abc"` where the mismatch happens at the last character of the longer one and thus we will not find a mismatch during the loop, we just check whether they are of one apart in lengths.

```cpp
class Solution {
public:
    bool isOneEditDistance(string s, string t) {
        int m = s.size(), n = t.size();
        if (m > n) {
            return isOneEditDistance(t, s);
        }
        for (int i = 0; i < m; i++) {
            if (s[i] != t[i]) {
                if (m == n) {
                    return s.substr(i + 1) == t.substr(i + 1);
                }
                return s.substr(i) == t.substr(i + 1);
            }
        }
        return m + 1 == n;
    }
};
```
</p>


