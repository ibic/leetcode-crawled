---
title: "Active Businesses"
weight: 1532
#id: "active-businesses"
---
## Description
<div class="description">
<p>Table: <code>Events</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| business_id   | int     |
| event_type    | varchar |
| occurences    | int     | 
+---------------+---------+
(business_id, event_type) is the primary key of this table.
Each row in the table logs the info that an event of some type occured at some business for a number of times.</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find all <em>active businesses</em>.</p>

<p>An active business is a business that has more than one event type&nbsp;with occurences greater than the average occurences of that event type&nbsp;among all businesses.</p>

<p>The query result format is in the following example:</p>

<pre>
Events table:
+-------------+------------+------------+
| business_id | event_type | occurences |
+-------------+------------+------------+
| 1           | reviews    | 7          |
| 3           | reviews    | 3          |
| 1           | ads        | 11         |
| 2           | ads        | 7          |
| 3           | ads        | 6          |
| 1           | page views | 3          |
| 2           | page views | 12         |
+-------------+------------+------------+

Result table:
+-------------+
| business_id |
+-------------+
| 1           |
+-------------+ 
Average for &#39;reviews&#39;, &#39;ads&#39; and &#39;page views&#39; are (7+3)/2=5, (11+7+6)/3=8, (3+12)/2=7.5 respectively.
Business with id 1 has 7 &#39;reviews&#39; events (more than 5) and 11 &#39;ads&#39; events (more than 8) so it is an active business.</pre>

</div>

## Tags


## Companies
- Yelp - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL Solution with Explanation
- Author: quabouquet
- Creation Date: Thu Sep 12 2019 05:47:46 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 12 2019 05:51:08 GMT+0800 (Singapore Standard Time)

<p>
```
select business_id                                      # Finally, select \'business_id\'
from
(select event_type, avg(occurences) as ave_occurences   # First, take the average of \'occurences\' group by \'event_type\'
 from events as e1
 group by event_type
) as temp1
join events as e2 on temp1.event_type = e2.event_type   # Second, join Events table on \'event_type\'
where e2.occurences > temp1.ave_occurences              # Third, the \'occurences\' should be greater than the average of \'occurences\'
group by business_id
having count(distinct temp1.event_type) > 1             # (More than one event type with \'occurences\' greater than 1)

```
</p>


### Window function + CTE
- Author: EvelynDong
- Creation Date: Fri Oct 18 2019 05:36:13 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 18 2019 05:36:13 GMT+0800 (Singapore Standard Time)

<p>
WITH r2 AS(
SELECT *, CASE WHEN occurences > AVG(occurences) OVER (PARTITION BY event_type) THEN 1 ELSE 0 END AS chosen
FROM Events)
SELECT business_id
FROM r2
GROUP BY business_id
HAVING SUM(chosen) >1;
</p>


### MySQL beats 100%
- Author: tx2180
- Creation Date: Mon Jul 15 2019 15:01:25 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 15 2019 15:01:25 GMT+0800 (Singapore Standard Time)

<p>
```
select 
business_id

from events as a
left join
    (
    select event_type, avg(occurences) as av
    from events
    group by event_type
    ) as b
on a.event_type = b.event_type
where a.occurences > b.av
group by business_id
having count(*)>1;
```
</p>


