---
title: "Sort Colors"
weight: 75
#id: "sort-colors"
---
## Description
<div class="description">
<p>Given an array <code>nums</code> with <code>n</code> objects colored red, white, or blue, sort them <strong><a href="https://en.wikipedia.org/wiki/In-place_algorithm" target="_blank">in-place</a>&nbsp;</strong>so that objects of the same color are adjacent, with the colors in the order red, white, and blue.</p>

<p>Here, we will use the integers <code>0</code>, <code>1</code>, and <code>2</code> to represent the color red, white, and blue respectively.</p>

<p><strong>Follow up:</strong></p>

<ul>
	<li>Could you&nbsp;solve this problem without using&nbsp;the library&#39;s sort function?</li>
	<li>Could you come up with a&nbsp;one-pass algorithm using only <code>O(1)</code> constant space?</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> nums = [2,0,2,1,1,0]
<strong>Output:</strong> [0,0,1,1,2,2]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> nums = [2,0,1]
<strong>Output:</strong> [0,1,2]
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> nums = [0]
<strong>Output:</strong> [0]
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> nums = [1]
<strong>Output:</strong> [1]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == nums.length</code></li>
	<li><code>1 &lt;= n &lt;= 300</code></li>
	<li><code>nums[i]</code> is <code>0</code>, <code>1</code>, or <code>2</code>.</li>
</ul>

</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)
- Sort (sort)

## Companies
- Amazon - 10 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: true)
- Oracle - 4 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: true)
- Pure Storage - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- LinkedIn - 5 (taggedByAdmin: false)
- Apple - 5 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Pocket Gems - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: One Pass 

**Intuition**

The problem is known as [Dutch National Flag Problem](https://en.wikipedia.org/wiki/Dutch_national_flag_problem)
and first was proposed by [Edsger W. Dijkstra](https://en.wikipedia.org/wiki/Edsger_W._Dijkstra).
The idea is to attribute a color to each number and then to
arrange them following the order of colors on the Dutch flag.

![bla](../Figures/75/first_pic2.png)

Let's use here three pointers to track the 
rightmost boundary of zeros, the leftmost boundary of twos
and the current element under the consideration.

![bla](../Figures/75/idx.png) 

The idea of solution is to move `curr` pointer along the
array, if `nums[curr] = 0` - swap it with `nums[p0]`,
if `nums[curr] = 2` - swap it with `nums[p2]`.

**Algorithm**

- Initialise the rightmost boundary of zeros : `p0 = 0`.
During the algorithm execution `nums[idx < p0] = 0`.

- Initialise the leftmost boundary of twos : `p2 = n - 1`.
During the algorithm execution `nums[idx > p2] = 2`.

- Initialise the index of current element to consider : `curr = 0`.

- While `curr <= p2` :

    - If `nums[curr] = 0` : swap `curr`th and `p0`th elements and 
    move both pointers to the right.
    
    - If `nums[curr] = 2` : swap `curr`th and `p2`th elements. Move
    pointer `p2` to the left. 
    
    - If `nums[curr] = 1` : move pointer `curr` to the right. 

**Implementation**

!?!../Documents/75_LIS.json:1000,511!?!

<iframe src="https://leetcode.com/playground/DKXFbfqc/shared" frameBorder="0" width="100%" height="500" name="DKXFbfqc"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since it's one pass
along the array of length $$N$$.
 
* Space complexity : $$\mathcal{O}(1)$$ since it's a constant space
solution.

## Accepted Submission (java)
```java
class Solution {
    private void swap(int[] nums, int pa, int pb) {
        int tmp = nums[pa];
        nums[pa] = nums[pb];
        nums[pb] = tmp;
    }

    public void sortColors(int[] nums) {
        int i = 0;
        int j = 0;
        int k = nums.length;
        while (j < k) {
            if (nums[j] == 0) {
                swap(nums, i, j);
                i++;
                j++;
            } else if (nums[j] == 2) {
                k--;
                swap(nums, j, k);
            } else {
                j++;
            }
        }
    }
}

```

## Top Discussions
### Share my at most two-pass constant space 10-line solution
- Author: yidawang
- Creation Date: Tue Nov 25 2014 10:28:24 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 02 2018 10:58:57 GMT+0800 (Singapore Standard Time)

<p>
The idea is to sweep all 0s to the left and all 2s to the right, then all 1s are left in the middle.

It is hard to define what is a "one-pass" solution but this algorithm is bounded by O(2n), meaning that at most each element will be seen and operated twice (in the case of all 0s). You may be able to write an algorithm which goes through the list only once, but each step requires multiple operations, leading the total operations larger than O(2n).

        class Solution {
        public:
            void sortColors(int A[], int n) {
                int second=n-1, zero=0;
                for (int i=0; i<=second; i++) {
                    while (A[i]==2 && i<second) swap(A[i], A[second--]);
                    while (A[i]==0 && i>zero) swap(A[i], A[zero++]);
                }
            }
        };
</p>


### Python O(n) 1 pass in-place solution with explanation
- Author: girikuncoro
- Creation Date: Tue Feb 02 2016 20:33:46 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 04:00:48 GMT+0800 (Singapore Standard Time)

<p>
This is a [dutch partitioning problem][1]. We are classifying the array into four groups: red, white, unclassified, and blue. Initially we group all elements into unclassified. We iterate from the beginning as long as the white pointer is less than the blue pointer. 

If the white pointer is red (nums[white] == 0), we swap with the red pointer and move both white and red pointer forward. If the pointer is white (nums[white] == 1), the element is already in correct place, so we don't have to swap, just move the white pointer forward. If the white pointer is blue, we swap with the latest unclassified element.


    def sortColors(self, nums):
        red, white, blue = 0, 0, len(nums)-1
        
        while white <= blue:
            if nums[white] == 0:
                nums[red], nums[white] = nums[white], nums[red]
                white += 1
                red += 1
            elif nums[white] == 1:
                white += 1
            else:
                nums[white], nums[blue] = nums[blue], nums[white]
                blue -= 1
                
                


  [1]: https://en.wikipedia.org/wiki/Dutch_national_flag_problem
</p>


### Four different solutions
- Author: shichaotan
- Creation Date: Thu Jan 08 2015 14:40:07 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 18:51:01 GMT+0800 (Singapore Standard Time)

<p>
    // two pass O(m+n) space
    void sortColors(int A[], int n) {
        int num0 = 0, num1 = 0, num2 = 0;
        
        for(int i = 0; i < n; i++) {
            if (A[i] == 0) ++num0;
            else if (A[i] == 1) ++num1;
            else if (A[i] == 2) ++num2;
        }
        
        for(int i = 0; i < num0; ++i) A[i] = 0;
        for(int i = 0; i < num1; ++i) A[num0+i] = 1;
        for(int i = 0; i < num2; ++i) A[num0+num1+i] = 2;
    }
    
    // one pass in place solution
    void sortColors(int A[], int n) {
        int n0 = -1, n1 = -1, n2 = -1;
        for (int i = 0; i < n; ++i) {
            if (A[i] == 0) 
            {
                A[++n2] = 2; A[++n1] = 1; A[++n0] = 0;
            }
            else if (A[i] == 1) 
            {
                A[++n2] = 2; A[++n1] = 1;
            }
            else if (A[i] == 2) 
            {
                A[++n2] = 2;
            }
        }
    }
    
    // one pass in place solution
    void sortColors(int A[], int n) {
        int j = 0, k = n - 1;
        for (int i = 0; i <= k; ++i){
            if (A[i] == 0 && i != j)
                swap(A[i--], A[j++]);
            else if (A[i] == 2 && i != k)
                swap(A[i--], A[k--]);
        }
    }
    
    // one pass in place solution
    void sortColors(int A[], int n) {
        int j = 0, k = n-1;
        for (int i=0; i <= k; i++) {
            if (A[i] == 0)
                swap(A[i], A[j++]);
            else if (A[i] == 2)
                swap(A[i--], A[k--]);
        }
    }
</p>


