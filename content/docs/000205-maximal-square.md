---
title: "Maximal Square"
weight: 205
#id: "maximal-square"
---
## Description
<div class="description">
<p>Given a 2D binary matrix filled with 0&#39;s and 1&#39;s, find the largest square containing only 1&#39;s and return its area.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input: 
</strong>
1 0 1 0 0
1 0 <font color="red">1</font> <font color="red">1</font> 1
1 1 <font color="red">1</font> <font color="red">1</font> 1
1 0 0 1 0

<strong>Output: </strong>4
</pre>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 14 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- IBM - 4 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Paypal - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: true)
- Citadel - 2 (taggedByAdmin: false)
- AppDynamics - 2 (taggedByAdmin: false)
- Uber - 6 (taggedByAdmin: false)
- Huawei - 4 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- FactSet - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Twilio - 2 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Summary

We need to find the largest square comprising of all ones in the given $$m \times n$$ matrix. In other words we need to find the largest set of connected ones in the given matrix that forms a square.

## Solution

---
#### Approach #1 Brute Force [Accepted]

The simplest approach consists of trying to find out every possible square of 1’s that can be formed from within the matrix. The question now is – how to go for it?

We use a variable to contain the size of the largest square found so far and another variable to store the size of the current, both initialized to 0. Starting from the left uppermost point in the matrix, we search for a 1. No operation needs to be done for a 0. Whenever a 1 is found, we try to find out the largest square that can be formed including that 1. For this, we move diagonally (right and downwards), i.e. we increment the row index and column index temporarily and then check whether all the elements of that row and column are 1 or not. If all the elements happen to be 1, we move diagonally further as previously. If even one element turns out to be 0, we stop this diagonal movement and update the size of the largest square. Now we, continue the traversal of the matrix from the element next to the initial 1 found, till all the elements of the matrix have been traversed.


**Java**

```java
public class Solution {
    public int maximalSquare(char[][] matrix) {
        int rows = matrix.length, cols = rows > 0 ? matrix[0].length : 0;
        int maxsqlen = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (matrix[i][j] == '1') {
                    int sqlen = 1;
                    boolean flag = true;
                    while (sqlen + i < rows && sqlen + j < cols && flag) {
                        for (int k = j; k <= sqlen + j; k++) {
                            if (matrix[i + sqlen][k] == '0') {
                                flag = false;
                                break;
                            }
                        }
                        for (int k = i; k <= sqlen + i; k++) {
                            if (matrix[k][j + sqlen] == '0') {
                                flag = false;
                                break;
                            }
                        }
                        if (flag)
                            sqlen++;
                    }
                    if (maxsqlen < sqlen) {
                        maxsqlen = sqlen;
                    }
                }
            }
        }
        return maxsqlen * maxsqlen;
    }
}
```

**Complexity Analysis**

* Time complexity : $$O\big((mn)^2\big)$$. In worst case, we need to traverse the complete matrix for every 1.
* Space complexity : $$O(1)$$. No extra space is used.

---
#### Approach #2 (Dynamic Programming) [Accepted]

**Algorithm**

We will explain this approach with the help of an example.
```
0 1 1 1 0
1 1 1 1 1
0 1 1 1 1
0 1 1 1 1
0 0 1 1 1
```
We initialize another matrix (dp) with the same dimensions as the original one initialized with all 0’s.

dp(i,j) represents the side length of the maximum square whose bottom right corner is the cell with index (i,j) in the original matrix. 

Starting from index (0,0), for every 1 found in the original matrix, we update the value of the current element as 

$$
\text{dp}(i,\  j) = \min \big( \text{dp}(i-1,\  j),\  \text{dp}(i-1,\  j-1),\  \text{dp}(i,\  j-1) \big) + 1.
$$

We also remember the size of the largest square found so far. In this way, we traverse the original matrix once and find out the required maximum size. This gives the side length of the square (say $$maxsqlen$$). The required result is the area $$maxsqlen^2$$.

To understand how this solution works, see the figure below.

![Max Square](https://leetcode.com/media/original_images/221_Maximal_Square.PNG?raw=true)

An entry 2 at $$(1, 3)$$ implies that we have a square of side 2 up to that index in the original matrix. Similarly, a 2 at $$(1, 2)$$ and $$(2, 2)$$ implies that a square of side 2 exists up to that index in the original matrix. Now to make a square of side 3, only a single entry of 1 is pending at $$(2, 3)$$. So, we enter a 3 corresponding to that position in the dp array.

Now consider the case for the index $$(3, 4)$$. Here, the entries at index $$(3, 3)$$ and $$(2, 3)$$ imply that a square of side 3 is possible up to their indices. But, the entry 1 at index $$(2, 4)$$ indicates that a square of side 1 only can be formed up to its index. Therefore, while making an entry at the index $$(3, 4)$$, this element obstructs the formation of a square having a side larger than 2. Thus, the maximum sized square that can be formed up to this index is of size $$2\times2$$.


**Java**
```java

public class Solution {
    public int maximalSquare(char[][] matrix) {
        int rows = matrix.length, cols = rows > 0 ? matrix[0].length : 0;
        int[][] dp = new int[rows + 1][cols + 1];
        int maxsqlen = 0;
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= cols; j++) {
                if (matrix[i-1][j-1] == '1'){
                    dp[i][j] = Math.min(Math.min(dp[i][j - 1], dp[i - 1][j]), dp[i - 1][j - 1]) + 1;
                    maxsqlen = Math.max(maxsqlen, dp[i][j]);
                }
            }
        }
        return maxsqlen * maxsqlen;
    }
}
```

**Complexity Analysis**

* Time complexity : $$O(mn)$$. Single pass.

* Space complexity : $$O(mn)$$. Another matrix of same size is used for dp.

---
#### Approach #3 (Better Dynamic Programming) [Accepted]

**Algorithm**

In the previous approach for calculating dp of $$i^{th}$$ row we are using only the previous element and the $$(i-1)^{th}$$ row. Therefore, we don't need 2D dp matrix as 1D dp array will be sufficient for this.

Initially the dp array contains all 0's. As we scan the elements of the original matrix across a row, we keep on updating the dp array as per the equation $$dp[j]=min(dp[j-1],dp[j],prev)$$, where prev refers to the old $$dp[j-1]$$. For every row, we repeat the same process and update in the same dp array.


![ Max Square ](https://leetcode.com/media/original_images/221_Maximal_Square1.png?raw=true)

**java**
```java
public class Solution {
    public int maximalSquare(char[][] matrix) {
        int rows = matrix.length, cols = rows > 0 ? matrix[0].length : 0;
        int[] dp = new int[cols + 1];
        int maxsqlen = 0, prev = 0;
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= cols; j++) {
                int temp = dp[j];
                if (matrix[i - 1][j - 1] == '1') {
                    dp[j] = Math.min(Math.min(dp[j - 1], prev), dp[j]) + 1;
                    maxsqlen = Math.max(maxsqlen, dp[j]);
                } else {
                    dp[j] = 0;
                }
                prev = temp;
            }
        }
        return maxsqlen * maxsqlen;
    }
}
```
**Complexity Analysis**

* Time complexity : $$O(mn)$$. Single pass.

* Space complexity : $$O(n)$$. Another array which stores elements in a row is used for dp.

Analysis written by: [@vinod23](https://leetcode.com/vinod23)

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ space-optimized DP
- Author: jianchao-li
- Creation Date: Wed Jun 03 2015 11:27:27 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 21:43:42 GMT+0800 (Singapore Standard Time)

<p>
To appy DP, we define the state as the maximal **size** (square = size * size) of the square that can be formed till point `(i, j)`, denoted as `dp[i][j]`.

For the topmost row (`i = 0`) and the leftmost column (`j = 0`), we have `dp[i][j] = matrix[i][j] - \'0\'`, meaning that it can at most form a square of size 1 when the matrix has a `\'1\'` in that cell.

When `i > 0` and `j > 0`, if `matrix[i][j] = \'0\'`, then `dp[i][j] = 0` since no square will be able to contain the `\'0\'` at that cell. If `matrix[i][j] = \'1\'`, we will have `dp[i][j] = min(dp[i-1][j-1], dp[i-1][j], dp[i][j-1]) + 1`, which means that the square will be limited by its left, upper and upper-left neighbors.

```cpp
class Solution {
public:
    int maximalSquare(vector<vector<char>>& matrix) {
        if (matrix.empty()) {
            return 0;
        }
        int m = matrix.size(), n = matrix[0].size(), sz = 0;
        vector<vector<int>> dp(m, vector<int>(n, 0));
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (!i || !j || matrix[i][j] == \'0\') {
                    dp[i][j] = matrix[i][j] - \'0\';
                } else {
                    dp[i][j] = min(dp[i - 1][j - 1], min(dp[i - 1][j], dp[i][j - 1])) + 1;
                }
                sz = max(dp[i][j], sz);
            }
        }
        return sz * sz;
    }
};
```

In the above code, it uses `O(mn)` space. Actually each time when we update `dp[i][j]`, we only need `dp[i-1][j-1]`, `dp[i-1][j]` (the previous row) and `dp[i][j-1]` (the current row). So we may just keep two rows.

```cpp
class Solution {
public:
    int maximalSquare(vector<vector<char>>& matrix) {
        if (matrix.empty()) {
            return 0;
        }
        int m = matrix.size(), n = matrix[0].size(), sz = 0;
        vector<int> pre(n, 0), cur(n, 0);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (!i || !j || matrix[i][j] == \'0\') {
                    cur[j] = matrix[i][j] - \'0\';
                } else {
                    cur[j] = min(pre[j - 1], min(pre[j], cur[j - 1])) + 1;
                }
                sz = max(cur[j], sz);
            }
            fill(pre.begin(), pre.end(), 0);
            swap(pre, cur);
        }
        return sz * sz;
    }
};
```

Furthermore, we may only use just one `vector` (thanks to @stellari for sharing the idea).

```cpp
class Solution {
public:
    int maximalSquare(vector<vector<char>>& matrix) {
        if (matrix.empty()) {
            return 0;
        }
        int m = matrix.size(), n = matrix[0].size(), sz = 0, pre;
        vector<int> cur(n, 0);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int temp = cur[j];
                if (!i || !j || matrix[i][j] == \'0\') {
                    cur[j] = matrix[i][j] - \'0\';
                } else {
                    cur[j] = min(pre, min(cur[j], cur[j - 1])) + 1;
                }
                sz = max(cur[j], sz);
                pre = temp;
            }
        }
        return sz * sz;
    }
};
```
</p>


### Extremely Simple Java Solution :)
- Author: earlme
- Creation Date: Thu Aug 06 2015 15:31:12 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 02 2018 12:22:08 GMT+0800 (Singapore Standard Time)

<p>
    public int maximalSquare(char[][] a) {
        if(a.length == 0) return 0;
        int m = a.length, n = a[0].length, result = 0;
        int[][] b = new int[m+1][n+1];
        for (int i = 1 ; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if(a[i-1][j-1] == '1') {
                    b[i][j] = Math.min(Math.min(b[i][j-1] , b[i-1][j-1]), b[i-1][j]) + 1;
                    result = Math.max(b[i][j], result); // update result
                }
            }
        }
        return result*result;
    }
</p>


### [Python] Thinking Process Diagrams - DP Approach
- Author: arkaung
- Creation Date: Mon Apr 27 2020 22:21:06 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 28 2020 10:46:08 GMT+0800 (Singapore Standard Time)

<p>
**Understanding basics**
![image](https://assets.leetcode.com/users/arkaung/image_1587997244.png)

* Here I want to mention that we are drawing squares from top left corner to bottom right corner. Therefore, when I mention, "surrounding elements", I am saying cells above the corner cell and the cells on the left of the corner cell.

**Building DP grid to memoize**
* We are going to create a `dp` grid with initial values of 0.
* We are going to update `dp` as described in the following figure. 

![image](https://assets.leetcode.com/users/arkaung/image_1587997873.png)

**Bigger Example**
* Let\'s try to see a bigger example.
* We go over one cell at a time row by row in the `matrix` and then update our `dp` grid accordingly. 
* Update `max_side` with the maximum `dp` cell value as you update.

![image](https://assets.leetcode.com/users/arkaung/image_1588005144.png)



In the code, I create a `dp` grid which has one additional column and one additional row. The reason is to facilitate the index dp[r-1][c] dp[r][c-1] and dp[r-1][c-1] for cells in first row and first column in `matrix`.

``` python
class Solution:
    def maximalSquare(self, matrix: List[List[str]]) -> int:
        if matrix is None or len(matrix) < 1:
            return 0
        
        rows = len(matrix)
        cols = len(matrix[0])
        
        dp = [[0]*(cols+1) for _ in range(rows+1)]
        max_side = 0
        
        for r in range(rows):
            for c in range(cols):
                if matrix[r][c] == \'1\':
                    dp[r+1][c+1] = min(dp[r][c], dp[r+1][c], dp[r][c+1]) + 1 # Be careful of the indexing since dp grid has additional row and column
                    max_side = max(max_side, dp[r+1][c+1])
                
        return max_side * max_side
                
```

**Complexity Analysis**

Time complexity : `O(mn)`. Single pass - `row x col (m=row; n=col)`
Space complexity : `O(mn)`. Additional space for `dp` grid (don\'t need to worry about additional 1 row and col).

**Follow up**
Space can be optimized as we don\'t need to keep the whole `dp` grid as we progress down the rows in `matrix`.

Aren\'t Dynamic Programming problems much like this joke? :D
![image](https://assets.leetcode.com/users/arkaung/image_1587998641.png)


</p>


