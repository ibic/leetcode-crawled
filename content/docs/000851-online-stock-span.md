---
title: "Online Stock Span"
weight: 851
#id: "online-stock-span"
---
## Description
<div class="description">
<p>Write a class <code>StockSpanner</code> which collects daily price quotes for some stock, and returns the <em>span</em>&nbsp;of that stock&#39;s price for the current day.</p>

<p>The span of the stock&#39;s price today&nbsp;is defined as the maximum number of consecutive days (starting from today and going backwards)&nbsp;for which the price of the stock was less than or equal to today&#39;s price.</p>

<p>For example, if the price of a stock over the next 7 days were <code>[100, 80, 60, 70, 60, 75, 85]</code>, then the stock spans would be <code>[1, 1, 1, 2, 1, 4, 6]</code>.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;StockSpanner&quot;,&quot;next&quot;,&quot;next&quot;,&quot;next&quot;,&quot;next&quot;,&quot;next&quot;,&quot;next&quot;,&quot;next&quot;]</span>, <span id="example-input-1-2">[[],[100],[80],[60],[70],[60],[75],[85]]</span>
<strong>Output: </strong><span id="example-output-1">[null,1,1,1,2,1,4,6]</span>
<strong>Explanation: </strong>
First, S = StockSpanner() is initialized.  Then:
S.next(100) is called and returns 1,
S.next(80) is called and returns 1,
S.next(60) is called and returns 1,
S.next(70) is called and returns 2,
S.next(60) is called and returns 1,
S.next(75) is called and returns 4,
S.next(85) is called and returns 6.

Note that (for example) S.next(75) returned 4, because the last 4 prices
(including today&#39;s price of 75) were less than or equal to today&#39;s price.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>Calls to <code>StockSpanner.next(int price)</code> will have <code>1 &lt;= price &lt;= 10^5</code>.</li>
	<li>There will be at most <code>10000</code> calls to <code>StockSpanner.next</code>&nbsp;per test case.</li>
	<li>There will be at most <code>150000</code> calls to <code>StockSpanner.next</code> across all test cases.</li>
	<li>The total&nbsp;time limit for this problem has been reduced by 75% for&nbsp;C++, and 50% for all other languages.</li>
</ol>
</div>

</div>

## Tags
- Stack (stack)

## Companies
- Amazon - 10 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Stack

**Intuition**

Clearly, we need to focus on how to make each query faster than a linear scan.  In a typical case, we get a new element like `7`, and there are some previous elements like `11, 3, 9, 5, 6, 4`.  Let's try to create some relationship between this query and the next query.

If (after getting `7`) we get an element like `2`, then the answer is `1`.  So in general, whenever we get a smaller element, the answer is 1.

If we get an element like `8`, the answer is 1 plus the previous answer (for `7`), as the `8` "stops" on the same value that `7` does (namely, `9`).

If we get an element like `10`, the answer is 1 plus the previous answer, plus the answer for `9`.

Notice throughout this evaluation, we only care about elements that occur in increasing order - we "shortcut" to them.  That is, from adding an element like `10`, we cut to `7` [with "weight" 4], then to `9` [with weight 2], then cut to `11` [with weight 1].

A stack is the ideal data structure to maintain what we care about efficiently.

**Algorithm**

Let's maintain a weighted stack of decreasing elements.  The size of the weight will be the total number of elements skipped.  For example, `11, 3, 9, 5, 6, 4, 7` will be `(11, weight=1), (9, weight=2), (7, weight=4)`.

When we get a new element like `10`, this helps us count the previous values faster by popping weighted elements off the stack.  The new stack at the end will look like `(11, weight=1), (10, weight=7)`.

<iframe src="https://leetcode.com/playground/ggrXtaXy/shared" frameBorder="0" width="100%" height="395" name="ggrXtaXy"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(Q)$$, where $$Q$$ is the number of calls to `StockSpanner.next`.  In total, there are $$Q$$ pushes to the stack, and at most $$Q$$ pops.

* Space Complexity:  $$O(Q)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] O(1)
- Author: lee215
- Creation Date: Sun Sep 09 2018 11:31:28 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 13 2020 00:23:29 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
You can refer to the same problem 739. Daily Temperatures.

Push every pair of `<price, result>` to a stack.
Pop lower price from the stack and accumulate the count.

One price will be pushed once and popped once.
So `2 * N` times stack operations and `N` times calls.
I\'ll say time complexity is amortized `O(1)`
</br>

**C++:**
```cpp
    stack<pair<int, int>> s;
    int next(int price) {
        int res = 1;
        while (!s.empty() && s.top().first <= price) {
            res += s.top().second;
            s.pop();
        }
        s.push({price, res});
        return res;
    }
```

**Java:**
```java
    Stack<int[]> stack = new Stack<>();
    public int next(int price) {
        int res = 1;
        while (!stack.isEmpty() && stack.peek()[0] <= price)
            res += stack.pop()[1];
        stack.push(new int[]{price, res});
        return res;
    }
```

**Python:**
```py
    def __init__(self):
        self.stack = []

    def next(self, price):
        res = 1
        while self.stack and self.stack[-1][0] <= price:
            res += self.stack.pop()[1]
        self.stack.append([price, res])
        return res
```
</br>

# More Good Stack Problems
Here are some problems that impressed me.
Good luck and have fun.

[1130. Minimum Cost Tree From Leaf Values](https://leetcode.com/problems/minimum-cost-tree-from-leaf-values/discuss/339959/One-Pass-O(N)-Time-and-Space)
[907. Sum of Subarray Minimums](https://leetcode.com/problems/sum-of-subarray-minimums/discuss/170750/C++JavaPython-Stack-Solution)
[901. Online Stock Span](https://leetcode.com/problems/online-stock-span/discuss/168311/C++JavaPython-O(1))
[856. Score of Parentheses](https://leetcode.com/problems/score-of-parentheses/discuss/141777/C++JavaPython-O(1)-Space)
[503. Next Greater Element II](https://leetcode.com/problems/next-greater-element-ii/discuss/98270/JavaC++Python-Loop-Twice)
496. Next Greater Element I
84. Largest Rectangle in Histogram
42. Trapping Rain Water

</p>


### [JAVA Solution] With visualization and easy explained!
- Author: el-seoudy
- Creation Date: Tue May 19 2020 17:15:41 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 19 2020 17:17:39 GMT+0800 (Singapore Standard Time)

<p>
**Let\'s start with code, then we move to visualization!**
```
class StockSpanner {
    
    /*
        We should have a stack of a pair of (current  price, maximum number of consecutive days)
        Since we don\'t have an access to the indicies.
    */
    Stack<int[]> s;
    
    public StockSpanner() {
        s = new Stack<>();
    }
    
   /*
   Let\'s trace the algorithm together on [100, 80, 60, 70, 60, 75, 85]
   1. calling StockSpanner.next(100) should result in first element in our stack will be (100, 1) (s.size() == 1)
   2. calling StockSpanner.next(80) should result in second element in our stack will be (80, 1) (s.size() == 2)
   3. calling StockSpanner.next(60) should result in third element in our stack will be (60, 1) (s.size() == 3)
   4. Now on calling StockSpanner.next(70) we should add span of (60) + 1 {the current day}
   and remove it from stack (70, 2) (s.size() == 3)
   5. Now on calling StockSpanner.next(60) result in fourth element in our stack will be (60, 1) (s.size() == 4)
   6. Now on calling StockSpanner.next(75) we should add span of (60) and (70) + 1 {the current day}
   and remove it from stack : (75, 4) (s.size() == 3)
   7. Now on calling StockSpanner.next(85) we should add span of (75) and (80) + 1 {the current day}
   and remove it from stack : (85, 6) (s.size() == 2)
   */
    
    public int next(int price) {
        int span = 1;
        while (!s.isEmpty() && price >= s.peek()[0]) { // If the current price is greater than stack peek.
            span += s.peek()[1];
            s.pop();
        }
        s.push(new int[]{price, span});
        return span;
    }
}

```

![image](https://assets.leetcode.com/users/el-seoudy/image_1589879595.png)

**Please upvote if you liked it.** <3
</p>


### Single stack explanation with Diagram
- Author: RohanPrakash
- Creation Date: Sun May 17 2020 16:20:46 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 04 2020 22:45:15 GMT+0800 (Singapore Standard Time)

<p>
stack of _pair<Price , Count>
![image](https://assets.leetcode.com/users/_ro/image_1589907199.png)


```
class StockSpanner {
public:
    stack<pair<int,int>> sk;
    StockSpanner() {    }
    
    int next(int price) {
	
        int ct = 1;
        while(sk.size() and sk.top().first <= price)    // as shown in during Dry run on sample input
				ct+=sk.top().second , sk.pop();            // we add count of poped element\'s count
				
        sk.push({price , ct});
        return ct;
    }
};
```
**Keep Coding \uD83C\uDF24 \uD83C\uDF1C **
</p>


