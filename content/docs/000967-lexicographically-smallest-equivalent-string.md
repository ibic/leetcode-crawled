---
title: "Lexicographically Smallest Equivalent String"
weight: 967
#id: "lexicographically-smallest-equivalent-string"
---
## Description
<div class="description">
<p>Given strings <code>A</code> and <code>B</code> of the same length, we say A[i] and B[i] are equivalent characters. For example, if <code>A = &quot;abc&quot;</code> and <code>B = &quot;cde&quot;</code>, then we have <code>&#39;a&#39; == &#39;c&#39;, &#39;b&#39; == &#39;d&#39;, &#39;c&#39; == &#39;e&#39;</code>.</p>

<p>Equivalent characters follow the usual rules of any equivalence relation:</p>

<ul>
	<li>Reflexivity: &#39;a&#39; == &#39;a&#39;</li>
	<li>Symmetry: &#39;a&#39; == &#39;b&#39; implies &#39;b&#39; == &#39;a&#39;</li>
	<li>Transitivity: &#39;a&#39; == &#39;b&#39; and &#39;b&#39; == &#39;c&#39; implies &#39;a&#39; == &#39;c&#39;</li>
</ul>

<p>For example, given the equivalency information from <code>A</code> and <code>B</code> above, <code>S = &quot;eed&quot;</code>, <code>&quot;acd&quot;</code>, and <code>&quot;aab&quot;</code> are equivalent strings, and <code>&quot;aab&quot;</code> is the lexicographically smallest equivalent string of <code>S</code>.</p>

<p>Return the lexicographically smallest equivalent string of <code>S</code> by using the equivalency information from <code>A</code> and <code>B</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">&quot;parker&quot;</span>, B = <span id="example-input-1-2">&quot;morris&quot;</span>, S = <span id="example-input-1-3">&quot;parser&quot;</span>
<strong>Output: </strong><span id="example-output-1">&quot;makkek&quot;</span>
<strong>Explanation:</strong> Based on the equivalency information in <code>A</code> and <code>B</code>, we can group their characters as <code>[m,p]</code>, <code>[a,o]</code>, <code>[k,r,s]</code>, <code>[e,i]</code>. The characters in each group are equivalent and sorted in lexicographical order. So the answer is <code>&quot;makkek&quot;</code>.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">&quot;hello&quot;</span>, B = <span id="example-input-2-2">&quot;world&quot;</span>, S = <span id="example-input-2-3">&quot;hold&quot;</span>
<strong>Output: </strong><span id="example-output-2">&quot;hdld&quot;</span>
<strong>Explanation: </strong> Based on the equivalency information in <code>A</code> and <code>B</code>, we can group their characters as <code>[h,w]</code>, <code>[d,e,o]</code>, <code>[l,r]</code>. So only the second letter <code>&#39;o&#39;</code> in <code>S</code> is changed to <code>&#39;d&#39;</code>, the answer is <code>&quot;hdld&quot;</code>.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-3-1">&quot;leetcode&quot;</span>, B = <span id="example-input-3-2">&quot;programs&quot;</span>, S = <span id="example-input-3-3">&quot;sourcecode&quot;</span>
<strong>Output: </strong><span id="example-output-3">&quot;aauaaaaada&quot;</span>
<strong>Explanation: </strong> We group the equivalent characters in <code>A</code> and <code>B</code> as <code>[a,o,e,r,s,c]</code>, <code>[l,p]</code>, <code>[g,t]</code> and <code>[d,m]</code>, thus all letters in <code>S</code> except <code>&#39;u&#39;</code> and <code>&#39;d&#39;</code> are transformed to <code>&#39;a&#39;</code>, the answer is <code>&quot;aauaaaaada&quot;</code>.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>String <code>A</code>, <code>B</code> and <code>S</code> consist of only lowercase English letters from <code>&#39;a&#39;</code> - <code>&#39;z&#39;</code>.</li>
	<li>The lengths of string <code>A</code>, <code>B</code> and <code>S</code> are between <code>1</code> and <code>1000</code>.</li>
	<li>String <code>A</code> and <code>B</code> are of the same length.</li>
</ol>
</div>

## Tags
- Depth-first Search (depth-first-search)
- Union Find (union-find)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Union Find Solution Java
- Author: yrq
- Creation Date: Fri Jun 07 2019 01:13:59 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 07 2019 01:13:59 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public String smallestEquivalentString(String A, String B, String S) {
        int[] graph = new int[26];
        for(int i = 0; i < 26; i++) {
            graph[i] = i;
        }
        for(int i = 0; i < A.length(); i++) {
            int a = A.charAt(i) - \'a\';
            int b = B.charAt(i) - \'a\';
            int end1 = find(graph, b);
            int end2 = find(graph, a);
            if(end1 < end2) {
                graph[end2] = end1;
            } else {
                graph[end1] = end2;
            }
        }
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < S.length(); i++) {
            char c = S.charAt(i);
            sb.append((char)(\'a\' + find(graph, c - \'a\')));
        }
        return sb.toString();
    }
    
    private int find(int[] graph, int idx) {
        while(graph[idx] != idx) {
            idx = graph[idx];
        }
        return idx;
    }
}
```
</p>


### C++ UF
- Author: votrubac
- Creation Date: Mon Jun 24 2019 09:29:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 24 2019 09:29:55 GMT+0800 (Singapore Standard Time)

<p>
```
int ds_find(vector<int>& ds, int p) {
  return ds[p] == -1 ? p : ds[p] = ds_find(ds, ds[p]);
}
void ds_merge(vector<int>& ds, int p1, int p2) {
  p1 = ds_find(ds, p1), p2 = ds_find(ds, p2);
  if (p1 != p2) ds[max(p1, p2)] = min(p1, p2);
}
string smallestEquivalentString(string A, string B, string S) {
  vector<int> ds(26, -1);
  for (auto i = 0; i < A.size(); ++i) ds_merge(ds, A[i] - \'a\', B[i] - \'a\');
  for (auto i = 0; i < S.size(); ++i) S[i] = ds_find(ds, S[i] - \'a\') + \'a\';
  return S;
}
```
</p>


### python, bfs/dfs/union find, complexity analysis
- Author: Olala_michelle
- Creation Date: Fri Oct 25 2019 08:48:49 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 25 2019 11:50:22 GMT+0800 (Singapore Standard Time)

<p>
**BFS w/o memo: beats 5.81%**

```
class Solution:
    def smallestEquivalentString(self, A, B, S):
		\'\'\'step1: Create a mapping from each char to its direct equivalents.\'\'\'
        neighbors = collections.defaultdict(set)
        for a, b in zip(A, B):
            neighbors[a].add(b)
            neighbors[b].add(a)
		
		\'\'\'step2: For each char of S, explore the map of all equivalents (marke as seen) 
		and memoize the minimum equivalents.\'\'\'
        d = {i:i for i in S}

        def bfs(ch):
            res = ch
            seen = set()
            queue = {ch}

            while queue:
                c = queue.pop()
                if c in seen: continue
                seen.add(c)
                res = min(res, c)
                queue |= neighbors[c]

            for v in seen:
                d[v] = res

            return res
        return \'\'.join(bfs(c) for c  in S)
```

**BFS w/ memo: beats 99.35%**

```
class Solution:
    def smallestEquivalentString(self, A, B, S):
        neighbors = collections.defaultdict(set)
        for a, b in zip(A, B):
            neighbors[a].add(b)
            neighbors[b].add(a)

        memo = {}

        def bfs(ch):
            if ch in memo: return memo[ch]
            res = ch
            seen = set()
            queue = {ch}

            while queue:
                c = queue.pop()
                if c in seen: continue
                seen.add(c)
                res = min(res, c)
                queue |= neighbors[c]

            for v in seen:
                memo[v] = res

            return res
        return \'\'.join(bfs(c) for c  in S)
        
```

**DFS: beats 5.8%**

```
class Solution(object):   
    def smallestEquivalentString(self, A, B, S):
        neighbors = collections.defaultdict(set)
        for a, b in zip(A, B):
            neighbors[a].add(b)
            neighbors[b].add(a)
        
        visited = set()
        def dfs(ch, minChar, visited):
            visited.add(ch)
            res = minChar
            
            for nei in neighbors[ch]:
                if nei not in visited:
                    res = min(res, dfs(nei, min(minChar, nei), visited))
            return res


        return \'\'.join([dfs(c, c, set()) for c in S])
```



**Union Find: beats 97.42%**

```
class Solution:
    def smallestEquivalentString(self, A: str, B: str, S: str) -> str:
	    \'\'\'
        step1: model these equalities as edges in a graph
        step2: compute connected components of the graph ==> {node: commID}
        step3: convert \'s\'
        \'\'\'
        # step 1 & 2
        d = {i:i for i in string.ascii_lowercase}
        
        def find(x):
            if d[x] != x:
                d[x] = find(d[x])
            return d[x]
        
        def union(x, y):
            rx, ry = find(x), find(y)
            if d[rx] < d[ry]:
                d[ry] = rx
            else:
                d[rx] = ry
        
        for a, b in zip(A, B):
            union(a, b)
			
        # step3
        ans = \'\'
        for s in S:
            ans += find(s)
        return ans
```

**Complexity:**
* Time: O(max(n, m)) where n is length of A, and m is length of S.
* space: O(1).  Since we only need to store the 26 English characters.
</p>


