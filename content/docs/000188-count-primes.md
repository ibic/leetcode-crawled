---
title: "Count Primes"
weight: 188
#id: "count-primes"
---
## Description
<div class="description">
<p>Count the number of prime numbers less than a non-negative number, <code>n</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 10
<strong>Output:</strong> 4
<strong>Explanation:</strong> There are 4 prime numbers less than 10, they are 2, 3, 5, 7.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 0
<strong>Output:</strong> 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= n &lt;= 5 * 10<sup>6</sup></code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Math (math)

## Companies
- Capital One - 5 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: true)
- Google - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Microsoft - 7 (taggedByAdmin: true)
- Oracle - 4 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Intel - 2 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (python3)
```python3
class Solution:
    def countPrimes(self, n: int) -> int:
        count = 0
        # False - not prime, True - prime
        sieve = [True] * (n - 1)
        for i in range(2, n):
            if sieve[i - 2]:
                count += 1
                for j in range(i, n, i):
                    sieve[j - 2] = False
        return count

```

## Top Discussions
### My simple Java solution
- Author: FunnyThingHH
- Creation Date: Mon May 11 2015 11:57:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 13:01:27 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public int countPrimes(int n) {
            boolean[] notPrime = new boolean[n];
            int count = 0;
            for (int i = 2; i < n; i++) {
                if (notPrime[i] == false) {
                    count++;
                    for (int j = 2; i*j < n; j++) {
                        notPrime[i*j] = true;
                    }
                }
            }
            
            return count;
        }
    }
</p>


### Fast Python Solution
- Author: tusizi
- Creation Date: Sat May 16 2015 16:32:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 05:56:12 GMT+0800 (Singapore Standard Time)

<p>
    class Solution:
    # @param {integer} n
    # @return {integer}
    def countPrimes(self, n):
        if n < 3:
            return 0
        primes = [True] * n
        primes[0] = primes[1] = False
        for i in range(2, int(n ** 0.5) + 1):
            if primes[i]:
                primes[i * i: n: i] = [False] * len(primes[i * i: n: i])
        return sum(primes)
</p>


### Python3, 99%, 112 ms, Explained: The Sieve of Eratosthenes with optimizations
- Author: dearborn
- Creation Date: Wed Jul 25 2018 19:35:45 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 27 2018 09:40:40 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def countPrimes(self, n):
        """
        :type n: int
        :rtype: int
        """
        # Sieve of Eratosthenes

        # We are only interested in numbers LESS than the input number
        # exit early for numbers LESS than 2; (two is prime)
        if n < 2:
            return 0
        
        # create strike list for the input range, initializing all indices to
        # prime (1).
        strikes = [1] * n

        # we know that 0 and 2 are not prime
        strikes[0] = 0
        strikes[1] = 0
        
        # Now set multiples of remaining numbers that are marked as prime to
        # not prime.  It is safe ignore numbers alreay marked as not prime
        # because there are factor(s) that divide evenly into this number and
        # all its multiples.  Use upper limit of (n**0.5)+1, because:
        #  (a) the smallest factor of a non-prime number will not be > sqrt(n).
        #      Ex. non-prime = 100, 
        #           5*20
        #           10*10, 
        #           20*5   # !! we have seen 5 before.
        for i in range(2, int(n**0.5)+1):
            if  strikes[i] != 0:
                # slow:
                #for j in range(i*i, n, i):
                #    strikes[j] = 0

                # 3x faster:
                # strikes[i*i:n:i] = [0] * ((n-1-i*i)//i + 1)
                # n = 11
                # i = 2
                # (n-1-i*i)//i + 1
                # (n-1)               # get total # of indicies for n (non-inclusive)
                #     -i*i            # shift to get # of slots in range of interest
                #          //i        # get number of groups
                #              + 1    # get number of slots
                # strikes[2*2:11:2]  = [0] * ((11-1-2*2)//2 + 1
                # strikes[4:11:2]    = [0] * 4
                # s[4], s[6], s[8], s10] = 0, 0, 0, 0
                strikes[i*i:n:i] = [0] * ((n-1-i*i)//i + 1)

        return sum(strikes)
```
</p>


