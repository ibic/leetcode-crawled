---
title: "Strings Differ by One Character"
weight: 1438
#id: "strings-differ-by-one-character"
---
## Description
<div class="description">
<p>Given a list&nbsp;of strings <code>dict</code> where all the strings are of the same length.</p>

<p>Return <code>True</code> if there are 2 strings that only differ by 1 character in the same index, otherwise&nbsp;return <code>False</code>.</p>

<p><strong>Follow up:&nbsp;</strong>Could you solve this problem in O(n*m) where n is the length of <code>dict</code> and m is the length of each string.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> dict = [&quot;abcd&quot;,&quot;acbd&quot;, &quot;aacd&quot;]
<strong>Output:</strong> true
<strong>Explanation:</strong> Strings &quot;a<strong>b</strong>cd&quot; and &quot;a<strong>a</strong>cd&quot; differ only by one character in the index 1.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> dict = [&quot;ab&quot;,&quot;cd&quot;,&quot;yz&quot;]
<strong>Output:</strong> false
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> dict = [&quot;abcd&quot;,&quot;cccc&quot;,&quot;abyd&quot;,&quot;abab&quot;]
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>Number of characters in <code>dict &lt;= 10^5</code></li>
	<li><code>dict[i].length == dict[j].length</code></li>
	<li><code>dict[i]</code> should be unique.</li>
	<li><code>dict[i]</code> contains only lowercase English letters.</li>
</ul>

</div>

## Tags


## Companies
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ Rabin-Karp O(nm)
- Author: votrubac
- Creation Date: Fri Aug 21 2020 04:23:53 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 21 2020 04:52:43 GMT+0800 (Singapore Standard Time)

<p>
First, we compute a `hash` for each string `i` in `[0, n)` as `hash[i]  = a[0] * 26 ^ (m - 1) + a[1] * 26 ^ (m - 2) + ... + a[m - 2] * 26 + a[m - 1]`, where `n` is the number of strings, and `m` - the number of characters in a string. 

Then, we go through each character position `j` in `[0, m)`, and compute a hash without that character: `h = hash[i] - a[j] * 26 ^ (m - j - 1)`.  We track `h` in a hash set so we can detect if there is another string with the same hash. Ideally, we should check for collisions as we are using `mod` to keep hashes within the integer range. Since the solution below is accepted, I am keeping it simple here.

Note that in the second loop we are going right-to-left so it\'s easier to compute `26 ^ (m - j - 1)`.

```cpp
bool differByOne(vector<string>& d) {
    int mod = 1000000007, n = d.size(), m = d[0].size();
    vector<int> hash(n);
    for (auto i = 0; i < n; ++i)
        for (auto j = 0; j < m; ++j)
            hash[i] = (26l * hash[i] + (d[i][j] - \'a\')) % mod;
    for (int j = m - 1, mult = 1; j >= 0; --j) {
        unordered_set<int> s;
        for (auto i = 0; i < n; ++i) {
            int h = (mod + hash[i] - (long)mult * (d[i][j] - \'a\') % mod) % mod;
            if (!s.insert(h).second)
                return true;
        }
        mult = 26l * mult % mod;
    }
    return false;
}
```
</p>


### [Python] Clean set + string hashing solution - from O(NM^2) to O(NM)
- Author: alanlzl
- Creation Date: Thu Aug 20 2020 15:46:33 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 21 2020 10:40:39 GMT+0800 (Singapore Standard Time)

<p>
**Idea**

The brute force solution iterates through all the words and use a wildcard character (e.g. `*`) to replace each character. Then we throw all these wildcard strings into a hash set and check if there\'s any duplication.

This solution takes `O(N*M^2)`, where `N = len(dict)` and `M = len(dict[i])`. It takes `O(N)` to iterate through the word list, `O(M)` to iterate through each character in the word, and another `O(M)` to create all the wildcard words.

We can use string hashing to bring down the time complexity to `O(N*M)`. Instead of storing all the wildcard words, we store their hash value. And a wildcard word\'s hash value can be calculated in `O(1)` time. Specifically, we substract the wildcard position hash value from the word\'s original hash value.

Example: 
- hash(\'ba\') = 2\*26^1 + 1\*26^0 = 53
- hash(\'*a\') = 53 - 2\*26^1 = 1
- hash(\'b*\') = 53 - 1\*26^0 = 52

Note that we may face hash collision problem. Here we use a large prime number (`10*11 + 7`) to avoid it, but there\'s still a chance that the algorithm fails. If we add checks for hash collision, the worst-case complexity will go back to `O(N*M^2)`.

<br />

**Complexity**

Brute force:
- Time complexity: `O(NM^2)`
- Space complexity: `O(NM^2)`
	
String hashing:
- Time complexity: `O(NM)`
- Space complexity: `O(NM)`

<br />

**Python**

Brute force:
```Python
class Solution:
    def differByOne(self, dict: List[str]) -> bool:
        n, m = len(dict), len(dict[0])
        for j in range(m):
            seen = set()
            for i in range(n):
                new_w = dict[i][:j] + \'*\' +dict[i][j+1:]
                if new_w in seen:
                    return True
                seen.add(new_w)
        return False
```


String hashing:
```Python
class Solution:
    def differByOne(self, dict: List[str]) -> bool:
        n, m = len(dict), len(dict[0])
        hashes = [0] * n
        MOD = 10**11 + 7
        
        for i in range(n):
            for j in range(m):
                hashes[i] = (26 * hashes[i] + (ord(dict[i][j]) - ord(\'a\'))) % MOD
        
        base = 1
        for j in range(m - 1, -1, -1):        
            seen = set()
            for i in range(n):
                new_h = (hashes[i] - base * (ord(dict[i][j]) - ord(\'a\'))) % MOD
                if new_h in seen:
                    return True
                seen.add(new_h)
            base = 26 * base % MOD
        return False
```

<br />

**Edit**
Inspired by @votrubac, I changed the order of for loop from `i-j` to `j-i`. Looking char by char is a more natural way of thinking and it runs faster. Complexity stays the same.
</p>


### Easy Java Hashset O(Nm) solution beats 83%
- Author: mildSpicySauce
- Creation Date: Sat Aug 22 2020 13:35:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 22 2020 13:36:17 GMT+0800 (Singapore Standard Time)

<p>
The idea is to loop in each index of dict[i]. In every loop, we remove the specific index i character of each string in dict, and add the new string to a hashset. To check if the return is true or false, we only need to check before add to hashset if there is already a same string there.
```
class Solution {
    public boolean differByOne(String[] dict) {
        Set<String> set = new HashSet<>();
        int len = dict[0].length();
        for(int i = 0; i < len; i++){
            set.clear();
            for(String str: dict){
                String t = str.substring(0, i) + str.substring(i + 1, len);
                if(set.contains(t)){
                    return true;
                }
                set.add(t);
            }
        }
        return false;
    }
}
```
</p>


