---
title: "Next Greater Element III"
weight: 523
#id: "next-greater-element-iii"
---
## Description
<div class="description">
<p>Given a positive <strong>32-bit</strong> integer <strong>n</strong>, you need to find the smallest <strong>32-bit</strong> integer which has exactly the same digits existing in the integer <strong>n</strong> and is greater in value than n. If no such positive <strong>32-bit</strong> integer exists, you need to return -1.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 12
<strong>Output:</strong> 21
</pre>

<p>&nbsp;</p>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> 21
<strong>Output:</strong> -1
</pre>

<p>&nbsp;</p>

</div>

## Tags
- String (string)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Houzz - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force

To solve the given problem, we treat the given number as a string, $$s$$. In this approach, we find out every possible permutation of list formed by the elements of the string $$s$$ formed. We form a list of strings, $$list$$, containing all the permutations possible. Then, we sort the given $$list$$ to find out the permutation which is just larger than the given one. But this one will be a very naive approach, since it requires us to find out every possible permutation which will take really long time.

<!---iframe src="https://leetcode.com/playground/UBNhHzjo/shared" frameBorder="0" name="UBNhHzjo" width="100%" height="515"></iframe>--->

<iframe src="https://leetcode.com/playground/CVwJC9NM/shared" frameBorder="0" width="100%" height="500" name="CVwJC9NM"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n!)$$. A total of $$n!$$ permutations are possible for a number consisting of $$n$$ digits.

* Space complexity : $$O(n!)$$. A total of $$n!$$ permutations are possible for a number consisting of $$n$$ digits, with each permutation consisting of $$n$$ digits.


---
#### Approach #2 Linear Solution

**Algorithm**

In this case as well, we consider the given number $$n$$ as a character array $$a$$.
First, we observe that for any given sequence that is in descending order, no next larger permutation is possible.
 For example, no next permutation is possible for the following array:
 ```
 [9, 5, 4, 3, 1]
 ```

We need to find the first pair of two successive numbers $$a[i]$$ and $$a[i-1]$$, from the right, which satisfy
 $$a[i] > a[i-1]$$. Now, no rearrangements to the right of $$a[i-1]$$ can create a larger permutation since that subarray consists of numbers in descending order.
 Thus, we need to rearrange the numbers to the right of $$a[i-1]$$ including itself.

Now, what kind of rearrangement will produce the next larger number? We want to create the permutation just larger than the current one. Therefore, we need to replace the number $$a[i-1]$$ with the number which is just larger than itself among the numbers lying to its right section, say $$a[j]$$.

![Next Greater Element ](https://leetcode.com/media/original_images/31_nums_graph.png)


We swap the numbers $$a[i-1]$$ and $$a[j]$$. We now have the correct number at index $$i-1$$. But still the current permutation isn't the permutation
    that we are looking for. We need the smallest permutation that can be formed by using the numbers only to the right of $$a[i-1]$$. Therefore, we need to place those
     numbers in ascending order to get their smallest permutation.

But, recall that while scanning the numbers from the right, we simply kept decrementing the index
      until we found the pair $$a[i]$$ and $$a[i-1]$$ where,  $$a[i] > a[i-1]$$. Thus, all numbers to the right of $$a[i-1]$$ were already sorted in descending order.
      Furthermore, swapping $$a[i-1]$$ and $$a[j]$$ didn't change that order.
      Therefore, we simply need to reverse the numbers following $$a[i-1]$$ to get the next smallest lexicographic permutation.

The following animation will make things clearer:

<!--![Next Permutation](https://leetcode.com/media/original_images/31_Next_Permutation.gif)-->
!?!../Documents/556_Next_Greater_Element_III.json!?!

<!--iframe src="https://leetcode.com/playground/uSrWDrPW/shared" frameBorder="0" name="uSrWDrPW" width="100%" height="515"></iframe>-->

<iframe src="https://leetcode.com/playground/G2HHYoTa/shared" frameBorder="0" width="100%" height="500" name="G2HHYoTa"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. In worst case, only two scans of the whole array are needed. Here, $$n$$ refers to the number of digits in the given number.

* Space complexity : $$O(n)$$. An array $$a$$ of size $$n$$ is used, where $$n$$ is the number of digits in the given number.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java solution (4ms) with explanation.
- Author: sanketdige268
- Creation Date: Tue Apr 11 2017 09:51:26 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 01:00:38 GMT+0800 (Singapore Standard Time)

<p>
This solution is just a java version derived from this [post](http://www.geeksforgeeks.org/find-next-greater-number-set-digits/).

At first, lets look at the edge cases -
1. If all digits sorted in descending order, then output is always \u201cNot Possible\u201d. For example, 4321.
2) If all digits are sorted in ascending order, then we need to swap last two digits. For example, 1234.
3) For other cases, we need to process the number from rightmost side (why? because we need to find the smallest of all greater numbers)

Now the main algorithm works in following steps -

I) Traverse the given number from rightmost digit, keep traversing till you find a digit which is smaller than the previously traversed digit. For example, if the input number is \u201c534976\u201d, we stop at 4 because 4 is smaller than next digit 9. If we do not find such a digit, then output is \u201cNot Possible\u201d.

II) Now search the right side of above found digit \u2018d\u2019 for the smallest digit greater than \u2018d\u2019. For \u201c53**4**976\u2033, the right side of 4 contains \u201c976\u201d. The smallest digit greater than 4 is **6**.

III) Swap the above found two digits, we get 53**6**97**4** in above example.

IV) Now sort all digits from position next to \u2018d\u2019 to the end of number. The number that we get after sorting is the output. For above example, we sort digits in bold 536**974**. We get \u201c536**479**\u201d which is the next greater number for input 534976.

```
public class Solution {
    public int nextGreaterElement(int n) {
        char[] number = (n + "").toCharArray();
        
        int i, j;
        // I) Start from the right most digit and 
        // find the first digit that is
        // smaller than the digit next to it.
        for (i = number.length-1; i > 0; i--)
            if (number[i-1] < number[i])
               break;

        // If no such digit is found, its the edge case 1.
        if (i == 0)
            return -1;
            
         // II) Find the smallest digit on right side of (i-1)'th 
         // digit that is greater than number[i-1]
        int x = number[i-1], smallest = i;
        for (j = i+1; j < number.length; j++)
            if (number[j] > x && number[j] <= number[smallest])
                smallest = j;
        
        // III) Swap the above found smallest digit with 
        // number[i-1]
        char temp = number[i-1];
        number[i-1] = number[smallest];
        number[smallest] = temp;
        
        // IV) Sort the digits after (i-1) in ascending order
        Arrays.sort(number, i, number.length);
        
        long val = Long.parseLong(new String(number));
        return (val <= Integer.MAX_VALUE) ? (int) val : -1;
    }
}
```
</p>


### C++ 4 lines (next_permutation)
- Author: votrubac
- Creation Date: Sun Apr 09 2017 11:01:14 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 02 2018 14:43:24 GMT+0800 (Singapore Standard Time)

<p>
```
int nextGreaterElement(int n) {
    auto digits = to_string(n);
    next_permutation(begin(digits), end(digits));
    auto res = stoll(digits);
    return (res > INT_MAX || res <= n) ? -1 : res;
}
```
</p>


### This problem is the same to Next Permutation, algorithm only.
- Author: Nakanu
- Creation Date: Sun Apr 09 2017 11:07:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 13 2018 07:16:42 GMT+0800 (Singapore Standard Time)

<p>
Here is one simple example.
index:  012345
given:  124651
ans :      125146
procedure:
Starting from the rightmost digit, going to left. Find the first digit which is smaller than the previous digit. 
In this example, 4 is smaller than 6.  Remember 4 and its index 2. 
Going from rightmost again. This time, find the first digit which is bigger than 4. It is 5 here.
Swap 4 and 5. The number becomes 125641.
Reverse all the digits which are right to 4's original index (That is 2), 641 should be reversed to 146 here.  
And the answer is reached which is 125146.
</p>


