---
title: "Form Largest Integer With Digits That Add up to Target"
weight: 1322
#id: "form-largest-integer-with-digits-that-add-up-to-target"
---
## Description
<div class="description">
<p>Given an array of integers <code>cost</code> and an integer <code>target</code>. Return the <strong>maximum</strong> integer you can paint&nbsp;under the following rules:</p>

<ul>
	<li>The cost of painting a&nbsp;digit (i+1) is given by&nbsp;<code>cost[i]</code>&nbsp;(0 indexed).</li>
	<li>The total cost used must&nbsp;be equal to <code>target</code>.</li>
	<li>Integer does not have digits 0.</li>
</ul>

<p>Since the answer may be too large, return it as string.</p>

<p>If there is no way to paint any integer given the condition, return &quot;0&quot;.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> cost = [4,3,2,5,6,7,2,5,5], target = 9
<strong>Output:</strong> &quot;7772&quot;
<strong>Explanation: </strong> The cost to paint the digit &#39;7&#39; is 2, and the digit &#39;2&#39; is 3. Then cost(&quot;7772&quot;) = 2*3+ 3*1 = 9. You could also paint &quot;977&quot;, but &quot;7772&quot; is the largest number.
<strong>Digit    cost</strong>
  1  -&gt;   4
  2  -&gt;   3
  3  -&gt;   2
  4  -&gt;   5
  5  -&gt;   6
  6  -&gt;   7
  7  -&gt;   2
  8  -&gt;   5
  9  -&gt;   5
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> cost = [7,6,5,5,5,6,8,7,8], target = 12
<strong>Output:</strong> &quot;85&quot;
<strong>Explanation:</strong> The cost to paint the digit &#39;8&#39; is 7, and the digit &#39;5&#39; is 5. Then cost(&quot;85&quot;) = 7 + 5 = 12.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> cost = [2,4,6,2,4,6,4,4,4], target = 5
<strong>Output:</strong> &quot;0&quot;
<strong>Explanation:</strong> It&#39;s not possible to paint any integer with total cost equal to target.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> cost = [6,10,15,40,40,40,40,40,40], target = 47
<strong>Output:</strong> &quot;32211&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>cost.length == 9</code></li>
	<li><code>1 &lt;= cost[i] &lt;= 5000</code></li>
	<li><code>1 &lt;= target &lt;= 5000</code></li>
</ul>

</div>

## Tags
- String (string)
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Strict O(Target)
- Author: lee215
- Creation Date: Sun May 17 2020 00:04:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 00:57:07 GMT+0800 (Singapore Standard Time)

<p>
I was asked if any solution in linear time.
Well, I didn\'t see any strict linear time solutions.
So I\'ll add this one myself.


# Solution 1
Take all value as 1 first.
We find out how many digits we can get.
Time strict `O(target)`
Space `O(target)`

**Java**
```java
    public String largestNumber(int[] cost, int target) {
        int[] dp = new int[target + 1];
        for (int t = 1; t <= target; ++t) {
            dp[t] = -10000;
            for (int i = 0; i < 9; ++i) {
                if (t >= cost[i])
                    dp[t] = Math.max(dp[t], 1 + dp[t - cost[i]]);
            }
        }
        if (dp[target] < 0) return "0";
        StringBuilder res = new StringBuilder();
        for (int i = 8; i >= 0; --i) {
            while (target >= cost[i] && dp[target] == dp[target - cost[i]] + 1) {
                res.append(1 + i);
                target -= cost[i];
            }
        }
        return res.toString();
    }
```
**C++**
```cpp
    string largestNumber(vector<int>& cost, int target) {
        vector<int> dp(target + 1, -10000);
        dp[0] = 0;
        for (int t = 1; t <= target; ++t) {
            for (int i = 0; i < 9; ++i) {
                dp[t] = max(dp[t], t >= cost[i] ? 1 + dp[t - cost[i]] : -10000);
            }
        }
        if (dp[target] < 0) return "0";
        string res = "";
        for (int i = 8; i >= 0; --i) {
            while (target >= cost[i] && dp[target] == dp[target - cost[i]] + 1) {
                res.push_back(\'1\' + i);
                target -= cost[i];
            }
        }
        return res;
    }
```

# Solution 2
Very standard Knapsack problem.
Some black magic here:
1. I initial the impossible number with -1,
so that the all imporssible negative value will be impossible.
2. We can always add the digit to tail.
Because we already add the bigger digits first.

**Python:**
```py
    def largestNumber(self, cost, target):
        dp = [0] + [-1] * (target + 5000)
        for t in xrange(1, target + 1):
            dp[t] = max(dp[t - c] * 10 + i + 1 for i, c in enumerate(cost))
        return str(max(dp[t], 0))
```
<br>
</p>


### [DP is Easy!] 5 Step DP THINKING process EXPLAINED!
- Author: teampark
- Creation Date: Wed May 27 2020 11:40:12 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 27 2020 12:07:05 GMT+0800 (Singapore Standard Time)

<p>
# DP is Easy!
*Disclaimer: I\'m not writing optimal bottom up, tabulated DP here. For simplicity, I didn\'t even memoize the code in this tutorial. My goal is to teach you how to THINK through these problems - not memorize.*

Today I\'ll show you in 4 simple steps why, DP is Easy! 

**DP Framework:**
```
def dp(state variables):
	# base case
	# decision1, decision2 
	# return max(decision1, decision2)
```

**Step 1. What type of DP problem can I frame this as?**
First I thought of optimization, because the problem asks us to find min or max. Next, I thought of knapsack because "target" or "remain" means knapsack to me. So 0/1 or unbounded? Well since we can choose element more than once, we know it\'s unbounded. 

So we know this problem is an optimization, unbounded knapsack problem. 

**Step 2. What are our state variables?**
Think about what we need to keep track of at every function call? Easy! The current number (aka `index`) and how much of the target value we have left (aka `remain`).

Notes:
1. **Index:** We need a way to iterate over the numbers we are going to choose. Plus we can use this to check how much this number costs us.
2. **Remain:** We know that it\'s important to keep track of how much cost we have incurred so far. We\'ll use remaining to track how much cost we have left at this point in time. 

Honestly, as soon I realized this was 0/1 or unbounded knapsack I knew the state variables would be index and remain. This is just from repeated practice. Always remember, PRACTICE makes perfect.

Great. Now we know our function will look like
```
def dp(index, remain):
	# base case
	# decision1,  decision2 
	# return max(decision1, decision2)
```

**Step 3. What are our base cases?**
For DP problems, we\'ll need a case to catch a success AND failure. 

1. Success Case: `if remain == 0`
2. Failure Case: `if remain < 0 or index == len(costs)+1`

Notes:
- **Success:** Our problem states our maximum optimal number MUST equal target. We keep track of that target state by using `remain`. So anytime remain == 0, we know our number is valid aka successful. 
- **Fail #1:** Similarly, If the remaining target value is less than 0, we know FORSURE that the answer will never be valid because we can never "gain" remain. So we failed.
- **Fail #2:** Lastly, if we run out of numbers to see aka index ever becomes equal to 10, we know we have failed because our earlier success case of  `remain==0` didn\'t return. 

Sweet. So we know that our function will look like

```
def dp(index, remain):
	# base cases
	if target == 0:
		return Success 
	elif target < 0 or index == len(costs) + 1:
		return Failure 
	# decision1, decision2 
	# return max(decision1, decision2) 
```

**Step 4. What are our decisions?**
1. Take this Number: `str(index) + dp(1, remain-cost[index-1])`
2. Continue to the next Number:  `dp(index+1, remain)`
 
Notes:
- **Eliminate Infinite Recurision:** Notice, both of our decisions take us closer to either a fail/success base case.
- **Take:** There are a few things at play here. (1) We add the current number to our answer aka we choose it, (2) We subtract the cost of the number we just took from our remaining value, (3) We reset the index back to 1 in order to choose from the whole slate of 1-9 numbers again. 
- **Continue:** This is a standard case. We simply increment the index aka try the next number. Because we didn\'t "take" a number, we don\'t edit our remaining value. 

Again, practice makes perfect here. It\'s standard knapsack framework to increment index and subtract from remain. 

Now our framework is done. 
```
def dp(index, remain):
	# base cases
	if target == 0:
		return Success 
	elif target < 0 or index == len(costs) + 1:
		return Failure 
		
	# decision1, decision2 
	takeThisNumber = str(index) + dp(1, remain-cost[index-1])
	skipThisNumber = dp(index+1, remain)
	
	# return max(decision1, decision2) 
	return max(takeThisNumber, skipThisNumber) 
```

**Step 5. Code It**
You\'ve done all the work. It\'s trivial to actually code the solution now. You can add a `self.memo={}` to optimize it. I\'m keeping it simple and ignoring the memoization. It\'s trivial as well. 

Note on the failure and success base cases. If there is a \'0\' in my answer, I know the answer is not valid. So any failure cases, I add \'0\' to the answer we are building. And on any success cases, I just add an empty string. It\'s a bit convoluted but it\'s the best I came up with after forgetting we can simply multiple the answer and add the current number. Oh well. Hope you enjoyed this! 
```
class Solution(object):
    def largestNumber(self, cost, target):
        ans = self.dfs(cost, 1, target)
        return ans if \'0\' not in ans else \'0\'
    
    def dfs(self, cost, index, remain):
		# base cases
        if remain == 0:
            return \'\'
        elif remain < 0 or index == len(cost)+1:
            return \'0\'
        
		# decisions 
        take = str(index) + self.dfs(cost, 1, remain-cost[index-1]) 
        skip = self.dfs(cost, index+1, remain) 
		
		# memoize and return the max of decisions
        return self.getBigger(take, skip) 
    
    def getBigger(self, num1, num2):
        if \'0\' in num2:
            return num1
        elif \'0\' in num1:
            return num2
        elif int(num1)>int(num2):
            return num1 
        else:
            return num2

```
</p>


### [Java] Top down DP + Greedy - Clean code - O(9 * target)
- Author: hiepit
- Creation Date: Sun May 17 2020 00:24:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 12:16:42 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
- Use `dp` approach to find possible answers with `maxLength`
- In our `dp`, we greedy from `d = 9..1`, so the first `maxLength` number will be the bigger number overall.

**Solution 1: Create New Strings (Straightforward)**
```java
class Solution {
    public String largestNumber(int[] cost, int target) {
        String[] dp = new String[target + 1];
        return dfs(cost, target, dp);
    }
    String dfs(int[] cost, int target, String[] dp) {
        if (target < 0) return "0"; // not found
        if (target == 0) return "";
        if (dp[target] != null) return dp[target];
        String ans = "0";
        for(int d = 9; d >= 1; d--) {
            String curr = dfs(cost, target - cost[d - 1], dp);
            if (curr.equals("0")) continue; // skip if can\'t solve sub-problem
            curr = d + curr;
            if (ans.equals("0") || curr.length() > ans.length()) {
                ans = curr;
            }
        }
        return dp[target] = ans;
    }
}
```
**Complexity**
- Time: `O(9 * target^2)`, the time for creating new string in line `curr = d + curr;` can cost up to `O(target)`
- Space: `O(target^2)`, because we store total `target` strings, each string up to `target` chars.

**Solution 2: Use Trace Array (Optimized) ~ 4ms**
```java
class Solution {
    public String largestNumber(int[] cost, int target) {
        Integer[] dp = new Integer[target + 1];
        Integer[] trace = new Integer[target + 1];
        int ans = dfs(cost, target, dp, trace);
        if (ans <= 0) return "0";
        StringBuilder sb = new StringBuilder();
        while (target > 0) { // trace back to get the result
            sb.append(trace[target]);
            target -= cost[trace[target] - 1];
        }
        return sb.toString();
    }
    int dfs(int[] cost, int target, Integer[] dp, Integer[] trace) {
        if (target < 0) return -1; // not found
        if (target == 0) return 0;
        if (dp[target] != null) return dp[target];
        int ans = -1;
        for(int d = 9; d >= 1; d--) {
            int curr = dfs(cost, target - cost[d - 1], dp, trace);
            if (curr == -1) continue; // skip if can\'t solve sub-problem
            if (curr + 1 > ans) {
                ans = curr + 1;
                trace[target] = d;
            }
        }
        return dp[target] = ans;
    }
}
```
**Complexity**
- Time: `O(9 * target)`
- Space: `O(target)`
</p>


