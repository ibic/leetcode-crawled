---
title: "Toss Strange Coins"
weight: 1064
#id: "toss-strange-coins"
---
## Description
<div class="description">
<p>You have some coins.&nbsp; The <code>i</code>-th&nbsp;coin has a probability&nbsp;<code>prob[i]</code> of facing heads when tossed.</p>

<p>Return the probability that the number of coins facing heads equals <code>target</code> if you toss every coin exactly once.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> prob = [0.4], target = 1
<strong>Output:</strong> 0.40000
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> prob = [0.5,0.5,0.5,0.5,0.5], target = 0
<strong>Output:</strong> 0.03125
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= prob.length &lt;= 1000</code></li>
	<li><code>0 &lt;= prob[i] &lt;= 1</code></li>
	<li><code>0 &lt;= target&nbsp;</code><code>&lt;= prob.length</code></li>
	<li>Answers will be accepted as correct if they are within <code>10^-5</code> of the correct answer.</li>
</ul>

</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- Twitch - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] DP
- Author: lee215
- Creation Date: Sun Oct 20 2019 00:04:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 00:34:02 GMT+0800 (Singapore Standard Time)

<p>
Please reply and upvote rather than later.
Don\'t have prime membership.
**Can\'t** even read and modify my own post later, when it\'s locked.
<br>

## **Explanation**
`dp[c][k]` is the prob of tossing `c` first coins and get `k` faced up.
`dp[c][k] = dp[c - 1][k] * (1 - p) + dp[c - 1][k - 1] * p)`
where `p` is the prob for `c`-th coin.
<br>

## **Complexity**
Time `O(N^2)`
Space `O(N)`
<br>

**Java:**
```java
    public double probabilityOfHeads(double[] prob, int target) {
        double[] dp = new double[target + 1];
        dp[0] = 1.0;
        for (int i = 0; i < prob.length; ++i)
            for (int k = Math.min(i + 1, target); k >= 0; --k)
                dp[k] = (k > 0 ? dp[k - 1] : 0) * prob[i] + dp[k] * (1 - prob[i]);
        return dp[target];
    }
```

**C++:**
```cpp
    double probabilityOfHeads(vector<double>& prob, int target) {
        vector<double> dp(target + 1);
        dp[0] = 1.0;
        for (int i = 0; i < prob.size(); ++i)
            for (int k = min(i + 1, target); k >= 0; --k)
                dp[k] = (k ? dp[k - 1] : 0) * prob[i] + dp[k] * (1 - prob[i]);
        return dp[target];
    }
```

**Python:**
```python
    def probabilityOfHeads(self, prob, target):
        dp = [1] + [0] * target
        for p in prob:
            for k in xrange(target, -1, -1):
                dp[k] = (dp[k - 1] if k else 0) * p + dp[k] * (1 - p)
        return dp[target]
```
</p>


### C++ DP solution with intuitive explanation
- Author: LeetcodeKZ
- Creation Date: Sun Oct 20 2019 00:16:47 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 21 2019 19:45:47 GMT+0800 (Singapore Standard Time)

<p>
This problem can be solved using a __dynamic programming__ approach. 
Our DP array is *d[i][j]* which answers the question => **What is the probability of having j items till i-th coin inclusively?**
**1**.
```c++
The base case is when we have a zero target.
This can be computed using the recurrence relation as the cumulative product of prob[i] negations. 
```
**2**.
For all other variations of filling *d[i][j]*, we have two options.
* **i-th coin has a tail**
	```c++
	P1 = (proability of having j coins till i) * (probability of having a tail)
	```
* **i-th coin has a head**
	```c++
	P2 = (probability of having j - 1 coins till i) * (probability of having a head)
	```
Therefore, the probability of having *j heads till i-th coin inclusively will be*
```c++
P = P1 + P2;
```



```c++
class Solution {
public:
    double probabilityOfHeads(vector<double>& prob, int target) {
        vector<vector<double> > d(prob.size() + 1, vector<double>(target + 1, 0));
        d[0][0] = 1.0;
        for (int i = 1; i <= prob.size(); ++i)
            for (int j = 0; j <= target; ++j)
                if (j == 0) d[i][j] = d[i - 1][j] * (1 - prob[i - 1]);
                else  d[i][j] =  d[i - 1][j - 1] * prob[i - 1] + d[i - 1][j] * (1.0 - prob[i - 1]);
        return d[prob.size()][target];
    }
};
```

**Complexity:**
**Time complexity:** O(target * n) => **O(n^2)**, because the target is in range [0,n]
**Space complexity:**  **O(n^2)**. 
However, the space complexity might be reduced to **O(n)**
**Hint:** for filling results of *d[i][j]* we only need to have an access to the vector with j-1 coins.

P.S. If you like this solution, please upvote so more people can see this post.
</p>


### C++ clean dp with explanation
- Author: BarOrz
- Creation Date: Sun Oct 20 2019 00:12:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 00:39:45 GMT+0800 (Singapore Standard Time)

<p>
**define dp[i][j] : until i-th coins, the probability to get j heads.**

```
example1 dp[3][1] : only 1 head when tossing first 3 coins
only 1 head when tossing first 3 coins = 
   only 1 head when tossing first 2 coins and third coins is not head + 
   no head when tossing first 2 coins and third coins is head
Thus, dp[3][1] =  dp[2][0] * prob[3]  +  dp[2][1] * (1-prob[3])

```
```
class Solution {
public:
    double probabilityOfHeads(vector<double>& prob, int target) {
        vector<vector<double>> dp(prob.size()+1,vector<double>(target+1,0.0));
        dp[0][0] = 1;
		/* initialize dp[i][0] */
        for(int i = 1 ; i <= prob.size() ; i++)
            dp[i][0] = dp[i-1][0] * (1-prob[i-1]);
			
		/* DP body */	
        for(int i = 1 ; i <= prob.size() ; i++)
            for(int j = 1 ; j <= min(i,target) ; j++)
                dp[i][j] = dp[i-1][j-1] * prob[i-1] + dp[i-1][j] * (1-prob[i-1]);
				
        return dp[prob.size()][target];
    }
};
```
</p>


