---
title: "Projection Area of 3D Shapes"
weight: 833
#id: "projection-area-of-3d-shapes"
---
## Description
<div class="description">
<p>On a&nbsp;<code>N&nbsp;*&nbsp;N</code> grid, we place some&nbsp;<code>1 * 1 * 1&nbsp;</code>cubes that are axis-aligned with the x, y, and z axes.</p>

<p>Each value&nbsp;<code>v = grid[i][j]</code>&nbsp;represents a tower of&nbsp;<code>v</code>&nbsp;cubes placed on top of grid cell <code>(i, j)</code>.</p>

<p>Now we view the&nbsp;<em>projection</em>&nbsp;of these cubes&nbsp;onto the xy, yz, and zx planes.</p>

<p>A projection is like a shadow, that&nbsp;maps our 3 dimensional figure to a 2 dimensional plane.&nbsp;</p>

<p>Here, we are viewing the &quot;shadow&quot; when looking at the cubes from the top, the front, and the side.</p>

<p>Return the total area of all three projections.</p>

<p>&nbsp;</p>

<div>
<ul>
</ul>
</div>

<div>
<div>
<ul>
</ul>
</div>
</div>

<div>
<div>
<div>
<div>
<ul>
</ul>
</div>
</div>
</div>
</div>

<div>
<div>
<div>
<div>
<div>
<div>
<div>
<div>
<ul>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[2]]</span>
<strong>Output: </strong><span id="example-output-1">5</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[[1,2],[3,4]]</span>
<strong>Output: </strong><span id="example-output-2">17</span>
<strong>Explanation: </strong>
Here are the three projections (&quot;shadows&quot;) of the shape made with each axis-aligned plane.
<img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/08/02/shadow.png" style="width: 749px; height: 200px;" />
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[[1,0],[0,2]]</span>
<strong>Output: </strong><span id="example-output-3">8</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">[[1,1,1],[1,0,1],[1,1,1]]</span>
<strong>Output: </strong><span id="example-output-4">14</span>
</pre>

<div>
<p><strong>Example 5:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-5-1">[[2,2,2],[2,1,2],[2,2,2]]</span>
<strong>Output: </strong><span id="example-output-5">21</span>
</pre>

<p>&nbsp;</p>

<div>
<div>
<div>
<p><span><strong>Note:</strong></span></p>

<ul>
	<li><code>1 &lt;= grid.length = grid[0].length&nbsp;&lt;= 50</code></li>
	<li><code>0 &lt;= grid[i][j] &lt;= 50</code></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

</div>

## Tags
- Math (math)

## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach 1: Mathematical

**Intuition and Algorithm**

From the top, the shadow made by the shape will be 1 square for each non-zero value.

From the side, the shadow made by the shape will be the largest value for each row in the grid.

From the front, the shadow made by the shape will be the largest value for each column in the grid.


**Example**

With the example `[[1,2],[3,4]]`:

* The shadow from the top will be 4, since there are four non-zero values in the grid;

* The shadow from the side will be `2 + 4`, since the maximum value of the first row is `2`, and the maximum value of the second row is `4`;

* The shadow from the front will be `3 + 4`, since the maximum value of the first column is `3`, and the maximum value of the second column is `4`.

<iframe src="https://leetcode.com/playground/wSjoDJhL/shared" frameBorder="0" width="100%" height="429" name="wSjoDJhL"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `grid`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Please change the description of the problem
- Author: Happy4892
- Creation Date: Thu Aug 16 2018 00:48:00 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 15:44:55 GMT+0800 (Singapore Standard Time)

<p>
The problem description is very ambiguous and confusing. I actually spent 90% of my time to understand what does the input imply when I tried to solve this problem. If you want to make problems complicated to solve, you should increase the complexity by making the algorithm itself hard to implement or think of, not making bad wordings and trying to confuse people. If I met this type of problem description in an actual interview, I might not even proceed with it.
</p>


### i don't even understand the description
- Author: xd386
- Creation Date: Sun Aug 05 2018 13:44:34 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 15:45:21 GMT+0800 (Singapore Standard Time)

<p>
can any body explain me the means of the first input?
</p>


### [C++/Java/Python] Straight Forward One Pass
- Author: lee215
- Creation Date: Sun Aug 05 2018 11:03:35 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 17 2019 11:29:54 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
front-back projection area on xz = `sum(max value for every col)`
right-left projection area on yz = `sum(max value for every row)`
top-down projection area on xy = `sum(1 for every v > 0)`
<br>
## **Diagram**
I drawn this diagram to help understand.
<img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/08/02/shadow.png" style="width: 749px; height: 200px;" />
<br>

## **Complexity**
Time `O(N^2)` for one pass.
Space `O(1)` extra space.
<br>

**C++:**
```
    int projectionArea(const vector<vector<int>>& grid) {
        int res = 0, n = grid.size(), x, y;
        for (int i = 0; i < n; ++i) {
            x = 0, y = 0;
            for (int j = 0; j < n; ++j) {
                x = max(x, grid[i][j]);
                y = max(y, grid[j][i]);
                if (grid[i][j]) ++res;
            }
            res += x + y;
        }
        return res;
    }
```

**Java:**
```
    public int projectionArea(int[][] grid) {
        int res = 0, n = grid.length;
        for (int i = 0; i < n; ++i) {
            int x = 0, y = 0;
            for (int j = 0; j < n; ++j) {
                x = Math.max(x, grid[i][j]);
                y = Math.max(y, grid[j][i]);
                if (grid[i][j] > 0) ++res;
            }
            res += x + y;
        }
        return res;
    }
```

**Python**
```
    def projectionArea(self, grid):
        hor = sum(map(max, grid))
        ver = sum(map(max, zip(*grid)))
        top = sum(v > 0 for row in grid for v in row)
        return ver + hor + top
```

**Python, Make it 1-line:**
```
    def projectionArea(self, grid):
        return sum(map(max, grid + zip(*grid))) + sum(v > 0 for row in grid for v in row)
```
</p>


