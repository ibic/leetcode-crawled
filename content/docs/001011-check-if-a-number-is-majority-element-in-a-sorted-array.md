---
title: "Check If a Number Is Majority Element in a Sorted Array"
weight: 1011
#id: "check-if-a-number-is-majority-element-in-a-sorted-array"
---
## Description
<div class="description">
<p>Given an array <code>nums</code> sorted in <strong>non-decreasing</strong> order, and a number <code>target</code>, return <code>True</code> if and only if <code>target</code> is a majority element.</p>

<p>A <em>majority element</em> is an element that appears <strong>more than <code>N/2</code></strong> times in an array of length <code>N</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>nums = <span id="example-input-1-1">[2,4,5,5,5,5,5,6,6]</span>, target = <span id="example-input-1-2">5</span>
<strong>Output: </strong><span id="example-output-1">true</span>
<strong>Explanation: </strong>
The value 5 appears 5 times and the length of the array is 9.
Thus, 5 is a majority element because 5 &gt; 9/2 is true.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>nums = <span id="example-input-2-1">[10,100,101,101]</span>, target = <span id="example-input-2-2">101</span>
<strong>Output: </strong><span id="example-output-2">false</span>
<strong>Explanation: </strong>
The value 101 appears 2 times and the length of the array is 4.
Thus, 101 is not a majority element because 2 &gt; 4/2 is false.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 1000</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 10^9</code></li>
	<li><code>1 &lt;= target &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Salesforce - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java just one binary search O(logN)), 0ms, beats 100%
- Author: kmw
- Creation Date: Wed Aug 14 2019 08:38:10 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 02 2020 10:56:00 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public boolean isMajorityElement(int[] nums, int target) {
        int firstIndex = firstOccur(nums, target);
        int plusNBy2Index = firstIndex + nums.length/2;
        
        if (plusNBy2Index<nums.length 
            && nums[plusNBy2Index] == target) {
            return true;
        }
        
        return false;
    }
    
    private int firstOccur(int[] nums, int target) {
        int low = 0;
        int high = nums.length;
        while (low < high) {
            int mid = low + (high-low)/2;
            if (nums[mid] < target) low = mid + 1;
            else if (nums[mid] >= target) high = mid;
        }
        return low;
    }
    
}
```
</p>


### Python: Two Ways, O(N) and O(logN)
- Author: dibdidib
- Creation Date: Sun Aug 11 2019 01:32:23 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 30 2019 07:23:05 GMT+0800 (Singapore Standard Time)

<p>
During a contest, we see that we have only a small number of elements, so we can take the quick route with time complexity `O(N)`:


```python
def isMajorityElement(self, nums, target):
    return nums.count(target) > len(nums) // 2
```

During an interview, we would think a bit more. Letting `x` be our majority element, a sorted array containing a majority element would look like one of the following:

    [ x x x x x x x . . . . . . ]        # majority at the beginning
    [ . . . x x x x x x x . . . ]        # majority at the middle
    [ . . . . . . x x x x x x x ]        # majority at the ending

If there is a majority element then when we examine the middle index of the array, we are guaranteed to find the majority element. So we can binary search for the beginning and end of the streak:

```python
def isMajorityElement(self, nums, target):
    N = len(nums)
    if nums[N // 2] != target:
        return False
    lo = bisect.bisect_left(nums, target)
    hi = bisect.bisect_right(nums, target)
    return hi - lo > N // 2
```

This cuts our time complexity from `O(N)` to `O(logN)`. If an interviewer asks us to do it without `bisect`, we can spell out the logic of `bisect_left()` and use a helper to find the required indices. This will likely run a bit slower because it is pure Python, and the `bisect` module typically uses a faster underlying implementation in C:

```python
def isMajorityElement(self, nums, target):

        def search(a, x):
            lo, hi = 0, len(a)
            while lo < hi:
                mid = (lo + hi) // 2
                if a[mid] < x:
                    lo = mid + 1
                else:
                    hi = mid
            return lo
            
        N = len(nums)
        if nums[N // 2] != target:
            return False
        lo = search(nums, target)
        hi = search(nums, target + 1)
        return hi - lo > N // 2
```

Thanks to [@ye15](https://leetcode.com/ye15) for pointing out that we can apply a similar approach to use only one binary search:

```python
def isMajorityElement(self, nums, target):
    k = len(nums) // 2
    if nums[k] != target:
        return False
    lo = bisect.bisect_left(nums, target, 0, k)
    hi = lo + k
    return hi < len(nums) and nums[hi] == target
```

</p>


### Clean Java O(log(n)) Solution (Binary Search)
- Author: anshu4intvcom
- Creation Date: Sun Aug 11 2019 02:13:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 14 2019 02:48:09 GMT+0800 (Singapore Standard Time)

<p>
```
    public boolean isMajorityElement(int[] nums, int target) {
        int n = nums.length;
        int first = search(nums, target);
        int last = search(nums, target + 1);
        return last - first > n / 2;
    }
	
    private int search(int[] nums, int target) {
        int lo = 0, hi = nums.length;
        while (lo < hi) {
            int mid = lo + (hi - lo) / 2;
            if (nums[mid] < target) {
                lo = mid + 1;
            } else {
                hi = mid;
            }
        }
        return lo;
    }

```
</p>


