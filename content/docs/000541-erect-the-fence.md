---
title: "Erect the Fence"
weight: 541
#id: "erect-the-fence"
---
## Description
<div class="description">
<p>There are some trees, where each tree is represented by (x,y) coordinate in a two-dimensional garden. Your job is to fence the entire garden using the <b>minimum length</b> of rope as it is expensive. The garden is well fenced only if all the trees are enclosed. Your task is to help find the coordinates of trees which are exactly located on the fence perimeter.</p>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> [[1,1],[2,2],[2,0],[2,4],[3,3],[4,2]]
<b>Output:</b> [[1,1],[2,0],[4,2],[3,3],[2,4]]
<b>Explanation:</b>
<img src="https://assets.leetcode.com/uploads/2018/10/12/erect_the_fence_1.png" style="width: 100%; max-width: 320px" />
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> [[1,2],[2,2],[4,2]]
<b>Output:</b> [[1,2],[2,2],[4,2]]
<b>Explanation:</b>
<img src="https://assets.leetcode.com/uploads/2018/10/12/erect_the_fence_2.png" style="width: 100%; max-width: 320px" />
Even you only have trees in a line, you need to use rope to enclose them. 
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>All trees should be enclosed together. You cannot cut the rope to enclose trees that will separate them in more than one group.</li>
	<li>All input integers will range from 0 to 100.</li>
	<li>The garden has at least one tree.</li>
	<li>All coordinates are distinct.</li>
	<li>Input points have <b>NO</b> order. No order required for output.</li>
	<li>input types have been changed on April 15, 2019. Please reset to default code definition to get new method signature.</li>
</ol>

</div>

## Tags
- Geometry (geometry)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]
## Summary

## Solution

---
#### Approach 1: Jarvis Algorithm

**Algorithm**

The idea behind Jarvis Algorithm is really simple. We start with the leftmost point among the given set of points and try to wrap up all the given points considering the boundary points in counterclockwise direction. 

This means that for every point $$p$$ considered, we try to find out a point $$q$$, such that this point $$q$$ is the most counterclockwise relative to $$p$$ than all the other points. For checking this, we make use of `orientation()` function in the current implementation. This function takes three arguments $$p$$, the current point added in the hull; $$q$$, the next point being considered to be added in the hull; $$r$$, any other point in the given point space. This function returns a negative value if the point $$q$$ is more counterclockwise to $$p$$ than the point $$r$$. 

The following figure shows the concept. The point $$q$$ is more counterclockwise to $$p$$ than the point $$r$$. 

![Erect_Fence](../Figures/587_Erect_Fence_Jarvis.PNG)

From the above figure, we can observe that in order for the points $$p$$, $$q$$ and $$r$$ need to be traversed in the same order in a counterclockwise direction, the cross product of the vectors $$\vec{pq}$$ and $$\vec{qr}$$ should be in a direction out of the plane of the screen i.e. it should be positive.

$$\vec{pq} $$x$$ \vec{qr} > 0$$

$$\begin{vmatrix} (q_x-p_x) & (q_y-p_y) \\ (r_x-q_x) & (r_y-p_y) \end{vmatrix} > 0$$

$$(q_x - p_x)*(r_y - q_y) - (q_y - p_y)*(r_x - q_x) > 0$$

$$(q_y - p_y)*(r_x - q_x) - (r_y - q_y)*(q_x - p_x) < 0$$

The above result is being calculated by the `orientation()` function.

Thus, we scan over all the points $$r$$ and find out the point $$q$$ which is the most counterclockwise relative to $$p$$ and add it to the convex hull. Further, if there exist two points(say $$i$$ and $$j$$) with the same relative orientation to $$p$$, i.e. if the points $$i$$ and $$j$$ are collinear relative to $$p$$, we need to consider the point $$i$$ which lies in between the two points $$p$$ and $$j$$. For considering such a situation, we've made use of a function `inBetween()` in the current implementation. Even after finding out a point $$q$$, we need to consider all the other points which are collinear to $$q$$ relative to $$p$$ so as to be able to consider all the points lying on the boundary.

Thus, we keep on including the points in the hull till we reach the beginning point. 

The following animation depicts the process for a clearer understanding.

!?!../Documents/587_Erect_Fence_1.json:1000,563!?!

<iframe src="https://leetcode.com/playground/K7vpW5JW/shared" frameBorder="0" width="100%" height="500" name="K7vpW5JW"></iframe>

**Complexity Analysis**

* Time complexity : $$O(m*n)$$. For every point on the hull we examine all the other points to determine the next point. Here n is number of input points and m is number of output or hull points ($$m \leq n$$). 

* Space complexity : $$O(m)$$. List $$hull$$ grows upto size $$m$$.

---
#### Approach 2: Graham Scan

**Algorithm**

Graham Scan Algorithm is also a standard algorithm for finding the convex hull of a given set of points. Consider the animation below to follow along with the discussion. 

!?!../Documents/587_Erect_Fence_2.json:1000,563!?!

The method works as follows. Firsly we select an initial point($$bm$$) to start the hull with. This point is chosen as the point with the lowest y-coordinate. In case of a tie, we need to choose the point with the lowest x-coordinate, from among all the given set of points. This point is indicated as point 0 in the animation. Then, we sort the given set of points based on their polar angles formed w.r.t. a vertical line drawn throught the intial point. 

This sorting of the points gives us a rough idea of the way in which we should consider the points to be included in the hull while considering the boundary in counter-clockwise order. In order to sort the points, we make use of `orientation` function which is the same as discussed in the last approach. The points with a lower polar angle relative to the vertical line come first in the sorted array. In case, if the orientation of two points happens to be the same, the points are sorted based on their distance from the beginning point($$bm$$). Later on we'll be considering the points in the sorted array in the same order. Because of this, we need to do the sorting based on distance for points collinear relative to $$bm$$, so that all the collinear points lying on the hull are included in the boundary.

But, we need to consider another important case. In case, the collinear points lie on the closing(last) edge of the hull, we need to consider the points such that the points which lie farther from the initial point $$bm$$ are considered first. Thus, after sorting the array, we traverse the sorted array from the end and reverse the order of the points which are collinear and lie towards the end of the sorted array, since these will be the points which will be considered at the end while forming the hull and thus, will be considered at the end. Thus, after these preprocessing steps, we've got the points correctly arranged in the way that they need to be considered while forming the hull.

Now, as per the algorithm, we start off by considering the line formed by the first two points(0 and 1 in the animation) in the sorted array. We push the points on this line onto a $$stack$$. After this, we start traversing over the sorted $$points$$ array from the third point onwards. If the current point being considered appears after taking a left turn(or straight path) relative to the previous line(line's direction), we push the point onto the stack, indicating that the point has been temporarily added to the hull boundary.

This checking of left or right turn is done by making use of `orientation` again. An orientation greater than 0, considering the points on the line and the current point, indicates a counterclockwise direction or a right turn. A negative orientation indicates a left turn similarly.

If the current point happens to be occuring by taking a right turn from the previous line's direction, it means that the last point included in the hull was incorrect, since it needs to lie inside the boundary and not on the boundary(as is indicated by point 4 in the animation). Thus, we pop off the last point from the stack and consider the second last line's direction with the current point. 

Thus, the same process continues, and the popping keeps on continuing till we reach a state where the current point can be included in the hull by taking a right turn. Thus, in this way, we ensure that the hull includes only the boundary points and not the points inside the boundary. After all the points have been traversed, the points lying in the stack constitute the boundary of the convex hull. 

The below code is inspired by [@yuxiangmusic](http://leetcode.com/yuxiangmusic) solution.

<iframe src="https://leetcode.com/playground/YbhLCxAT/shared" frameBorder="0" width="100%" height="500" name="YbhLCxAT"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(n \log n\big)$$. Sorting the given points takes $$O\big(n \log n\big)$$ time. Further, after sorting the points can be considered in two cases, while being pushed onto the $$stack$$ or while popping from the $$stack$$. Atmost, every point is touched twice(both push and pop) taking $$2n$$($$O(n)$$) time in the worst case.

* Space complexity : $$O(n)$$. Stack size grows upto $$n$$ in worst case.

---
#### Approach 3: Monotone Chain

**Algorithm**

The idea behing Monotone Chain Algorithm is somewhat similar to Graham Scan Algorithm. It mainly differs in the order in which the points are considered while being included in the hull. Instead of sorting the points based on their polar angles as in Graham Scan, we sort the points on the basis of their x-coordinate values. If two points have the same x-coordinate values, the points are sorted based on their y-coordinate values. The reasoning behind this will be explained soon.

In this algorithm, we consider the hull as being comprised of two sub-boundaries- The upper hull and the lower hull. We form the two portions in a slightly different manner. 

We traverse over the sorted $$points$$ array after adding the initial two points in the hull temporarily(which are pushed over the stack $$hull$$). For every new point considered, we check if the current point lies in the counter-clockwise direction relative to the last two points. If so, the current point is staightaway pushed onto $$hull$$. If not(indicated by a positive `orientation`), we again get the inference that the last point on the $$hull$$ needs to lie inside the boundary and not on the boundary. Thus, we keep on popping the points from $$hull$$ till the current point lies in a counterclockwise direction relative to the top two points on the $$hull$$. 

Note that this time, we need not consider the case of collinear points explicitly, since the points have already been sorted based on their x-coordinate values. So, the collinear points, if any, will implicitly be considered in the correct order.

Doing so, we reach a state such that we reach the point with the largest x-coordinate. But, the hull isn't complete yet. The portion of the hull formed till now constitutes the lower poriton of the hull. Now, we need to form the upper portion of the hull.

Thus, we continue the process of finding the next counterclockwise points and popping in case of a conflict, but this time we consider the points in the reverse order of their x-coordinate values. For this, we can simply traverse over the sorted $$points$$ array in the reverse order. We append the new upper hull values obtained to the previous $$hull$$ itself. At the end, $$hull$$ gives the points on the required boundary.

The following animation depicts the process for a better understanding of the process:

!?!../Documents/587_Erect_Fence_3.json:1000,563!?!

<iframe src="https://leetcode.com/playground/W9BRhz8d/shared" frameBorder="0" width="100%" height="500" name="W9BRhz8d"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(n \log n\big)$$. Sorting the given points takes $$O\big(n \log n\big)$$ time. Further, after sorting the points can be considered in two cases, while being pushed onto the $$hull$$ or while popping from the $$hull$$. Atmost, every point is touched twice(both push and pop) taking $$2n$$($$O(n)$$) time in the worst case.

* Space complexity : $$O(n)$$. $$hull$$ stack can grow upto size $$n$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ and Python easy wiki solution
- Author: lee215
- Creation Date: Thu May 18 2017 17:16:29 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 23 2018 18:26:51 GMT+0800 (Singapore Standard Time)

<p>
C++ version:
`````
// Ref: http://www.algorithmist.com/index.php/Monotone_Chain_Convex_Hull.cpp
class Solution {
 public:
  typedef int coord_t;  // coordinate type
  typedef long long coord2_t;  // must be big enough to hold 2*max(|coordinate|)^2
  // 2D cross product of OA and OB vectors, i.e. z-component of their 3D cross
  // product. Returns a positive value, if OAB makes a counter-clockwise turn,
  // negative for clockwise turn, and zero if the points are collinear.
  coord2_t cross(const Point &O, const Point &A, const Point &B) {
    return (A.x - O.x) * (coord2_t)(B.y - O.y) -
           (A.y - O.y) * (coord2_t)(B.x - O.x);
  }

  static bool cmp(Point &p1, Point &p2) {
    return p1.x < p2.x || (p1.x == p2.x && p1.y < p2.y);
  }

  static bool equ(Point &p1, Point &p2) { return p1.x == p2.x && p1.y == p2.y; }
  // Returns a list of points on the convex hull in counter-clockwise order.
  // Note: the last point in the returned list is the same as the first one.
  vector<Point> outerTrees(vector<Point> &P) {
    int n = P.size(), k = 0;
    vector<Point> H(2 * n);

    // Sort points lexicographically
    sort(P.begin(), P.end(), cmp);

    // Build lower hull
    for (int i = 0; i < n; i++) {
      while (k >= 2 && cross(H[k - 2], H[k - 1], P[i]) < 0) k--;
      H[k++] = P[i];
    }

    // Build upper hull
    for (int i = n - 2, t = k + 1; i >= 0; i--) {
      while (k >= t && cross(H[k - 2], H[k - 1], P[i]) < 0) k--;
      H[k++] = P[i];
    }

    // Remove duplicates
    H.resize(k);
    sort(H.begin(), H.end(), cmp);
    H.erase(unique(H.begin(), H.end(), equ), H.end());
    return H;
  }
};
`````

Python version:
````
# http://www.algorithmist.com/index.php/Monotone_Chain_Convex_Hull.py


class Solution(object):

    def outerTrees(self, points):
        """Computes the convex hull of a set of 2D points.

        Input: an iterable sequence of (x, y) pairs representing the points.
        Output: a list of vertices of the convex hull in counter-clockwise order,
          starting from the vertex with the lexicographically smallest coordinates.
        Implements Andrew's monotone chain algorithm. O(n log n) complexity.
        """

        # Sort the points lexicographically (tuples are compared lexicographically).
        # Remove duplicates to detect the case we have just one unique point.
        # points = sorted(set(points))
        points = sorted(points, key=lambda p: (p.x, p.y))

        # Boring case: no points or a single point, possibly repeated multiple times.
        if len(points) <= 1:
            return points

        # 2D cross product of OA and OB vectors, i.e. z-component of their 3D cross product.
        # Returns a positive value, if OAB makes a counter-clockwise turn,
        # negative for clockwise turn, and zero if the points are collinear.
        def cross(o, a, b):
            # return (a[0] - o[0]) * (b[1] - o[1]) - (a[1] - o[1]) * (b[0] - o[0])
            return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x)

        # Build lower hull
        lower = []
        for p in points:
            while len(lower) >= 2 and cross(lower[-2], lower[-1], p) < 0:
                lower.pop()
            lower.append(p)

        # Build upper hull
        upper = []
        for p in reversed(points):
            while len(upper) >= 2 and cross(upper[-2], upper[-1], p) < 0:
                upper.pop()
            upper.append(p)

        # Concatenation of the lower and upper hulls gives the convex hull.
        # Last point of each list is omitted because it is repeated at the
        # beginning of the other list.
        # return lower[:-1] + upper[:-1]
        return list(set(lower[:-1] + upper[:-1]))
````
</p>


### Java Graham scan with adapted sorting to deal with collinear points
- Author: yuxiangmusic
- Creation Date: Sun May 14 2017 12:30:41 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 12 2018 03:50:48 GMT+0800 (Singapore Standard Time)

<p>
The trick is that once all points are sorted by polar angle with respect to the reference point:
* For collinear points in the begin positions, make sure they are sorted by distance to reference point in **ascending** order.
* For collinear points in the end positions, make sure they are sorted by distance to reference point in **descending** order.

For example:
```(0, 0), (2, 0), (3, 0), (3, 1), (3, 2), (2, 2), (1, 2), (0, 2), (0, 1)```
These points are sorted by polar angle
The reference point (bottom left point) is ```(0, 0)```
* In the begin positions ```(0, 0)``` collinear with ```(2, 0), (3, 0)``` sorted by distance to reference point in **ascending** order.
* In the end positions ```(0, 0)``` collinear with ```(0, 2), (0, 1)``` sorted by distance to reference point in **descending** order.

Now we can run the standard Graham scan to give us the desired result.
```
public class Solution {

    public List<Point> outerTrees(Point[] points) {
        if (points.length <= 1)
            return Arrays.asList(points);
        sortByPolar(points, bottomLeft(points));
        Stack<Point> stack = new Stack<>(); 
        stack.push(points[0]);                      
        stack.push(points[1]);                              
        for (int i = 2; i < points.length; i++) {
            Point top = stack.pop();                                
            while (ccw(stack.peek(), top, points[i]) < 0)
                top = stack.pop();
            stack.push(top);
            stack.push(points[i]);
        }       
        return new ArrayList<>(stack);
    }                               

    private static Point bottomLeft(Point[] points) {
        Point bottomLeft = points[0];
        for (Point p : points)          
            if (p.y < bottomLeft.y || p.y == bottomLeft.y && p.x < bottomLeft.x)
                bottomLeft = p;                 
        return bottomLeft;                                                  
    }

    /**
     * @return positive if counter-clockwise, negative if clockwise, 0 if collinear
     */
    private static int ccw(Point a, Point b, Point c) {
        return a.x * b.y - a.y * b.x + b.x * c.y - b.y * c.x + c.x * a.y - c.y * a.x;       
    }

    /**
     * @return distance square of |p - q|
     */
    private static int dist(Point p, Point q) {
        return (p.x - q.x) * (p.x - q.x) + (p.y - q.y) * (p.y - q.y);
    }
                              
    private static void sortByPolar(Point[] points, Point r) {
        Arrays.sort(points, (p, q) -> {
            int compPolar = ccw(p, r, q);
            int compDist = dist(p, r) - dist(q, r); 
            return compPolar == 0 ? compDist : compPolar;
        });     
        // find collinear points in the end positions
        Point p = points[0], q = points[points.length - 1];
        int i = points.length - 2;
        while (i >= 0 && ccw(p, q, points[i]) == 0)
            i--;    
        // reverse sort order of collinear points in the end positions
        for (int l = i + 1, h = points.length - 1; l < h; l++, h--) {
            Point tmp = points[l];
            points[l] = points[h];
            points[h] = tmp;
        }
    }
}
```
</p>


### Java Solution, Convex Hull Algorithm - Gift wrapping aka Jarvis march
- Author: shawngao
- Creation Date: Sun May 14 2017 11:43:49 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 10:25:33 GMT+0800 (Singapore Standard Time)

<p>
There are couple of ways to solve Convex Hull problem. https://en.wikipedia.org/wiki/Convex_hull_algorithms
The following code implements ```Gift wrapping aka Jarvis march``` algorithm https://en.wikipedia.org/wiki/Gift_wrapping_algorithm and also added logic to handle case of ```multiple Points in a line``` because original Jarvis march algorithm assumes ```no three points are collinear```.
It also uses knowledge in this problem https://leetcode.com/problems/convex-polygon . Disscussion: https://discuss.leetcode.com/topic/70706/beyond-my-knowledge-java-solution-with-in-line-explanation
```
public class Solution {
    public List<Point> outerTrees(Point[] points) {
        Set<Point> result = new HashSet<>();
        
        // Find the leftmost point
        Point first = points[0];
        int firstIndex = 0;
        for (int i = 1; i < points.length; i++) {
            if (points[i].x < first.x) {
                first = points[i];
                firstIndex = i;
            }
        }
        result.add(first);
        
        Point cur = first;
        int curIndex = firstIndex;
        do {
            Point next = points[0];
            int nextIndex = 0;
            for (int i = 1; i < points.length; i++) {
                if (i == curIndex) continue;
                int cross = crossProductLength(cur, points[i], next);
                if (nextIndex == curIndex || cross > 0 ||
                        // Handle collinear points
                        (cross == 0 && distance(points[i], cur) > distance(next, cur))) {
                    next = points[i];
                    nextIndex = i;
                }
            }
            // Handle collinear points
            for (int i = 0; i < points.length; i++) {
                if (i == curIndex) continue;
                int cross = crossProductLength(cur, points[i], next);
                if (cross == 0) {
                    result.add(points[i]);
                }
            }

            cur = next;
            curIndex = nextIndex;
            
        } while (curIndex != firstIndex);
        
        return new ArrayList<Point>(result);
    }
    
    private int crossProductLength(Point A, Point B, Point C) {
        // Get the vectors' coordinates.
        int BAx = A.x - B.x;
        int BAy = A.y - B.y;
        int BCx = C.x - B.x;
        int BCy = C.y - B.y;
    
        // Calculate the Z coordinate of the cross product.
        return (BAx * BCy - BAy * BCx);
    }

    private int distance(Point p1, Point p2) {
        return (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
    }
}
```
</p>


