---
title: "Split Two Strings to Make Palindrome"
weight: 1616
#id: "split-two-strings-to-make-palindrome"
---
## Description
<div class="description">
<p>You are given two strings <code>a</code> and <code>b</code> of the same length. Choose an index and split both strings <strong>at the same index</strong>, splitting <code>a</code> into two strings: <code>a<sub>prefix</sub></code> and <code>a<sub>suffix</sub></code> where <code>a = a<sub>prefix</sub> + a<sub>suffix</sub></code>, and splitting <code>b</code> into two strings: <code>b<sub>prefix</sub></code> and <code>b<sub>suffix</sub></code> where <code>b = b<sub>prefix</sub> + b<sub>suffix</sub></code>. Check if <code>a<sub>prefix</sub> + b<sub>suffix</sub></code> or <code>b<sub>prefix</sub> + a<sub>suffix</sub></code> forms a palindrome.</p>

<p>When you split a string <code>s</code> into <code>s<sub>prefix</sub></code> and <code>s<sub>suffix</sub></code>, either <code>s<sub>suffix</sub></code> or <code>s<sub>prefix</sub></code> is allowed to be empty. For example, if <code>s = &quot;abc&quot;</code>, then <code>&quot;&quot; + &quot;abc&quot;</code>, <code>&quot;a&quot; + &quot;bc&quot;</code>, <code>&quot;ab&quot; + &quot;c&quot;</code> , and <code>&quot;abc&quot; + &quot;&quot;</code> are valid splits.</p>

<p>Return <code>true</code><em> if it is possible to form</em><em> a palindrome string, otherwise return </em><code>false</code>.</p>

<p><strong>Notice</strong> that&nbsp;<code>x + y</code> denotes the concatenation of strings <code>x</code> and <code>y</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> a = &quot;x&quot;, b = &quot;y&quot;
<strong>Output:</strong> true
<strong>Explaination:</strong> If either a or b are palindromes the answer is true since you can split in the following way:
a<sub>prefix</sub> = &quot;&quot;, a<sub>suffix</sub> = &quot;x&quot;
b<sub>prefix</sub> = &quot;&quot;, b<sub>suffix</sub> = &quot;y&quot;
Then, a<sub>prefix</sub> + b<sub>suffix</sub> = &quot;&quot; + &quot;y&quot; = &quot;y&quot;, which is a palindrome.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> a = &quot;abdef&quot;, b = &quot;fecab&quot;
<strong>Output:</strong> true
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> a = &quot;ulacfd&quot;, b = &quot;jizalu&quot;
<strong>Output:</strong> true
<strong>Explaination:</strong> Split them at index 3:
a<sub>prefix</sub> = &quot;ula&quot;, a<sub>suffix</sub> = &quot;cfd&quot;
b<sub>prefix</sub> = &quot;jiz&quot;, b<sub>suffix</sub> = &quot;alu&quot;
Then, a<sub>prefix</sub> + b<sub>suffix</sub> = &quot;ula&quot; + &quot;alu&quot; = &quot;ulaalu&quot;, which is a palindrome.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> a = &quot;xbdef&quot;, b = &quot;xecab&quot;
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= a.length, b.length &lt;= 10<sup>5</sup></code></li>
	<li><code>a.length == b.length</code></li>
	<li><code>a</code> and <code>b</code> consist of lowercase English letters</li>
</ul>
</div>

## Tags
- Two Pointers (two-pointers)
- String (string)
- Greedy (greedy)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java Greedy O(n) | O(1)
- Author: votrubac
- Creation Date: Sun Oct 11 2020 12:00:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 13:19:31 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
Greedily use all matching letters from suffix and prefix. Then check if the middle of either string is a palindrome. This logic can be easily proven by a contradiction.

> Another way to think about this: using more characters from the both strings reduces the size of the middle string, so it increases the chance that the middle part is a palindrome.

Note that we do this twice - once for `a] + [b`, and once for `b] + [a`.

> with [lee215](https://leetcode.com/lee215/)\'s suggestions to avoid copying the string the code below achieves the fastest runtime.

**C++**
```cpp
bool isPalindrome(string &s, int i, int j) {
    while (i < j && s[i] == s[j])
        ++i, --j;
    return i >= j;
}
bool check(string &a, string &b) {
    int i = 0, j = a.size() - 1;
    while (i < j && a[i] == b[j])
        ++i, --j;
    return isPalindrome(a, i, j) || isPalindrome(b, i, j);
}
bool checkPalindromeFormation(string a, string b) {
    return check(a, b) || check(b, a);
}
```

**Java**
```java
boolean isPalindrome(String s, int i, int j) {
    while (i < j && s.charAt(i) == s.charAt(j)) {
        ++i;
        --j;
    }
    return i >= j;
}
boolean check(String a, String b) {
    int i = 0, j = a.length() - 1;
    while (i < j && a.charAt(i) == b.charAt(j)) {
        ++i;
        --j;
    }
    return isPalindrome(a, i, j) || isPalindrome(b, i, j);
}    
public boolean checkPalindromeFormation(String a, String b) {
    return check(a, b) || check(b, a);
}
```
**Complexity Analysis**
- Time: O(n)
- Memory: O(1)
</p>


### [Java/C++/Python] Greedy Solution, O(1) Space
- Author: lee215
- Creation Date: Sun Oct 11 2020 12:04:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 12:39:11 GMT+0800 (Singapore Standard Time)

<p>

## **Explanation**
Greedily take the `a_suffix` and `b_prefix` as long as they are palindrome,
that is, `a_suffix = reversed(b_prefix)`.

The the middle part of `a` is s1,
The the middle part of `b` is s2.

If either `s1` or `s2` is palindrome, then return `true`.

Then we do the same thing for `b_suffix` and `a_prefix`
<br>


# Solution 1:
Time `O(N)`, Space `O(N)`
**Python:**
```py
    def checkPalindromeFormation(self, a, b):
        i, j = 0, len(a) - 1
        while i < j and a[i] == b[j]:
            i, j = i + 1, j - 1
        s1, s2 = a[i:j + 1], b[i:j + 1]

        i, j = 0, len(a) - 1
        while i < j and b[i] == a[j]:
            i, j = i + 1, j - 1
        s3, s4 = a[i:j + 1], b[i:j + 1]

        return any(s == s[::-1] for s in (s1,s2,s3,s4))
```
<br>

# Solution 2
Same idea, Time `O(N)`, Space `O(1)`.
**Java:**
```java
    public boolean isPa(String s, int i, int j) {
        for (; i < j; ++i, --j)
            if (s.charAt(i) != s.charAt(j))
                return false;
        return true;
    }

    public boolean check(String a, String b) {
        for (int i = 0, j = a.length() - 1; i < j; ++i, --j)
            if (a.charAt(i) != b.charAt(j))
                return isPa(a, i, j) || isPa(b, i, j);
        return true;
    }



    public boolean checkPalindromeFormation(String a, String b) {
        return check(a, b) || check(b, a);
    }
```

**C++:**
```cpp
    bool isPa(string& s, int i, int j) {
        for (; i < j; ++i, --j)
            if (s[i] != s[j])
                return false;
        return true;
    }

    bool check(string& a, string& b) {
        for (int i = 0, j = a.size() - 1; i < j; ++i, --j)
            if (a[i] != b[j])
                return isPa(a, i, j) || isPa(b, i, j);
        return true;
    }

    bool checkPalindromeFormation(string a, string b) {
        return check(a, b) || check(b, a);
    }
```
</p>


