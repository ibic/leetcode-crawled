---
title: "Spiral Matrix II"
weight: 59
#id: "spiral-matrix-ii"
---
## Description
<div class="description">
<p>Given a positive integer <em>n</em>, generate a square matrix filled with elements from 1 to <em>n</em><sup>2</sup> in spiral order.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> 3
<strong>Output:</strong>
[
 [ 1, 2, 3 ],
 [ 8, 9, 4 ],
 [ 7, 6, 5 ]
]
</pre>

</div>

## Tags
- Array (array)

## Companies
- Microsoft - 5 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Robinhood - 3 (taggedByAdmin: false)
- Yandex - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Epic Systems - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Overview

There are various problems in spiral matrix series with some variations like [Spiral Matrix](https://leetcode.com/problems/spiral-matrix/) and [Spiral Matrix III](https://leetcode.com/problems/spiral-matrix-iii/).

In order to solve such questions, the core idea is to decode the underlying pattern. This can be done by simulating the pattern and finding a generic representation that would work for any given $$n$$.
Let's discuss a few approaches.

---
#### Approach 1: Traverse Layer by Layer in Spiral Form

**Intuition**

If we try to build a pattern for a given $$n$$, we observe that the pattern repeats after completing one circular traversal around the matrix. Let's call this one circular traversal as  _layer_. We start traversing from the outer layer and move towards inner layers on every iteration.

![img](../Figures/59/spiral_layers.png)

**Algorithm**

Let's devise an algorithm for the spiral traversal:

-  We can observe that, for any given $$n$$, the total number of layers is given by :
$$\lfloor \frac{n+1}{2} \rfloor$$
This works for both even and odd $$n$$.

_Example_

For `$$n = 3$$`, `$$layers = 2$$`

For `$$n = 6$$`, total `$$layers = 3$$`

- Also, for each layer, we traverse in _at most_ 4 directions :


![img](../Figures/59/spiral_traverse.png)


In every direction, either row or column remains constant and other parameter changes (increments/decrements).

_Direction 1: From top left corner to top right corner._

The row remains constant as $$\text{layer}$$ and column increments from $$\text{layer}$$ to  $$n-\text{layer}-1$$

_Direction 2: From top right corner to the bottom right corner._

The column remains constant as $$n-layer-1$$ and row increments from
$$\text{layer}+1$$ to $$n-\text{layer}$$.

_Direction 3: From bottom right corner to bottom left corner._

The row remains constant as $$n-\text{layer}-1$$ and column decrements from $$n-\text{layer}-2$$ to $$\text{layer}$$.

_Direction 4: From bottom right corner to top left corner._

The column remains constant as $$\text{layer}$$ and column decrements from $$n-\text{layer}-2$$ to $$\text{layer}+1$$.

This process repeats $$(n+1)/2$$ times until all layers are traversed.

![img](../Figures/59/spiral_detailed.png)


<iframe src="https://leetcode.com/playground/hUpa4EqX/shared" frameBorder="0" width="100%" height="500" name="hUpa4EqX"></iframe>

**Complexity Analysis**

* Time Complexity: $$\mathcal{O}(n^2)$$. Here, $$n$$ is given input and we are iterating over $$n\cdot n$$ matrix in spiral form.
* Space Complexity: $$\mathcal{O}(1)$$  We use constant extra space for storing $$cnt$$.

---
#### Approach 2: Optimized spiral traversal

**Intuition**

Our main aim is to walk in a spiral form and fill the array in a particular pattern. In the previous approach, we used a separate loop for each direction. Here, we discuss another optimized to achieve the same result.

**Algorithm**

- We have to walk in 4 directions forming a layer. We use an array $$dir$$ that stores the changes in $$x$$ and $$y$$ co-ordinates in each direction.

_Example_

In left to right walk ( _direction #1_ ), $$x$$ co-ordinates remains same and $$y$$ increments ($$x = 0$$, $$y = 1$$).

In right to left walk ( _direction #3_ ), $$x$$ remains same and $$y$$ decrements ($$x = 0$$, $$y = -1$$).

Using this intuition, we pre-define an array $$dir$$ having $$x$$ and $$y$$ co-ordinate changes for each direction. There are a total of 4 directions as discussed in the previous approach.

- The $$\text{row}$$ and $$col$$ variables represent the current $$x$$ and $$y$$ co-ordinates respectively. It updates based on the direction in which we are moving.

_How do we know when we have to change the direction?_

When we find the next row or column in a particular direction has a non-zero value, we are sure it is already traversed and we change the direction.

Let $$d$$ be the current direction index. We go to next direction in array $$dir$$ using $$(d+ 1) \% 4$$. Using this we could go back to direction 1 after completing one circular traversal from direction 1 to direction 4 .

> It must be noted that we use `floorMod` in Java instead of modulo $$\%$$ to handle mod of negative numbers. This is required because row and column values might go negative and using $$\%$$ won't give desired results in such cases.  

<iframe src="https://leetcode.com/playground/SnzJBfsc/shared" frameBorder="0" width="100%" height="497" name="SnzJBfsc"></iframe>

**Complexity Analysis**

* Time Complexity: $$\mathcal{O}(n^2)$$. Here, $$n$$ is given input and we are iterating over $$n\cdot n$$ matrix in spiral form.
* Space Complexity: $$\mathcal{O}(1)$$  We use constant extra space for storing $$cnt$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 4-9 lines Python solutions
- Author: StefanPochmann
- Creation Date: Sun Jul 19 2015 06:51:09 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 23:58:31 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1: *Build it inside-out*** - 44 ms, 5 lines

Start with the empty matrix, add the numbers in reverse order until we added the number 1. Always rotate the matrix clockwise and add a top row:

        ||  =>  |9|  =>  |8|      |6 7|      |4 5|      |1 2 3|
                         |9|  =>  |9 8|  =>  |9 6|  =>  |8 9 4|
                                             |8 7|      |7 6 5|

The code:

    def generateMatrix(self, n):
        A, lo = [], n*n+1
        while lo > 1:
            lo, hi = lo - len(A), lo
            A = [range(lo, hi)] + zip(*A[::-1])
        return A

While this isn't O(n^2), it's actually quite fast, presumably due to me not doing much in Python but relying on `zip` and `range` and `+` being fast. I got it accepted in 44 ms, matching the fastest time for recent Python submissions (according to the submission detail page).

---

**Solution 2: *Ugly inside-out*** - 48 ms, 4 lines

Same as solution 1, but without helper variables. Saves a line, but makes it ugly. Also, because I access A[0][0], I had to handle the n=0 case differently.

    def generateMatrix(self, n):
        A = [[n*n]]
        while A[0][0] > 1:
            A = [range(A[0][0] - len(A), A[0][0])] + zip(*A[::-1])
        return A * (n>0)

---

**Solution 3: *Walk the spiral*** - 52 ms, 9 lines

Initialize the matrix with zeros, then walk the spiral path and write the numbers 1 to n*n. Make a right turn when the cell ahead is already non-zero.

    def generateMatrix(self, n):
        A = [[0] * n for _ in range(n)]
        i, j, di, dj = 0, 0, 0, 1
        for k in xrange(n*n):
            A[i][j] = k + 1
            if A[(i+di)%n][(j+dj)%n]:
                di, dj = dj, -di
            i += di
            j += dj
        return A
</p>


### My Super Simple Solution. Can be used for both Spiral Matrix I and II
- Author: qwl5004
- Creation Date: Fri Oct 24 2014 12:14:11 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 05:10:35 GMT+0800 (Singapore Standard Time)

<p>
This is my solution for Spiral Matrix I, [https://oj.leetcode.com/discuss/12228/super-simple-and-easy-to-understand-solution][1]. If you can understand that, this one is a no brainer :)

Guess what? I just made several lines of change (with comment "//change") from that and I have the following AC code:

    public class Solution {
        public int[][] generateMatrix(int n) {
            // Declaration
            int[][] matrix = new int[n][n];
            
            // Edge Case
            if (n == 0) {
                return matrix;
            }
            
            // Normal Case
            int rowStart = 0;
            int rowEnd = n-1;
            int colStart = 0;
            int colEnd = n-1;
            int num = 1; //change
            
            while (rowStart <= rowEnd && colStart <= colEnd) {
                for (int i = colStart; i <= colEnd; i ++) {
                    matrix[rowStart][i] = num ++; //change
                }
                rowStart ++;
                
                for (int i = rowStart; i <= rowEnd; i ++) {
                    matrix[i][colEnd] = num ++; //change
                }
                colEnd --;
                
                for (int i = colEnd; i >= colStart; i --) {
                    if (rowStart <= rowEnd)
                        matrix[rowEnd][i] = num ++; //change
                }
                rowEnd --;
                
                for (int i = rowEnd; i >= rowStart; i --) {
                    if (colStart <= colEnd)
                        matrix[i][colStart] = num ++; //change
                }
                colStart ++;
            }
            
            return matrix;
        }
    }

Obviously, you could merge colStart and colEnd into rowStart and rowEnd because it is a square matrix. But this is easily extensible to matrices that are m*n.

Hope this helps :)


  [1]: https://oj.leetcode.com/discuss/12228/super-simple-and-easy-to-understand-solution
</p>


### Simple C++ solution(with explaination)
- Author: AllenYick
- Creation Date: Wed Jan 14 2015 09:55:35 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 13:31:12 GMT+0800 (Singapore Standard Time)

<p>
   
    class Solution {
        public:
            vector<vector<int> > generateMatrix(int n) {
                vector<vector<int> > ret( n, vector<int>(n) );
            	int k = 1, i = 0;
            	while( k <= n * n )
            	{
            		int j = i;
                        // four steps
            		while( j < n - i )             // 1. horizonal, left to right
            			ret[i][j++] = k++;
            		j = i + 1;
            		while( j < n - i )             // 2. vertical, top to bottom
            			ret[j++][n-i-1] = k++;
            		j = n - i - 2;
            		while( j > i )                  // 3. horizonal, right to left 
            			ret[n-i-1][j--] = k++;
            		j = n - i - 1;
            		while( j > i )                  // 4. vertical, bottom to  top 
            			ret[j--][i] = k++;
            		i++;      // next loop
            	}
            	return ret;
            }
        };
</p>


