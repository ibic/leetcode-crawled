---
title: "Min Cost Climbing Stairs"
weight: 668
#id: "min-cost-climbing-stairs"
---
## Description
<div class="description">
<p>
On a staircase, the <code>i</code>-th step has some non-negative cost <code>cost[i]</code> assigned (0 indexed).
</p><p>
Once you pay the cost, you can either climb one or two steps. You need to find minimum cost to reach the top of the floor, and you can either start from the step with index 0, or the step with index 1.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> cost = [10, 15, 20]
<b>Output:</b> 15
<b>Explanation:</b> Cheapest is start on cost[1], pay that cost and go to the top.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> cost = [1, 100, 1, 1, 1, 100, 1, 1, 100, 1]
<b>Output:</b> 6
<b>Explanation:</b> Cheapest is start on cost[0], and only step on 1s, skipping cost[3].
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li><code>cost</code> will have a length in the range <code>[2, 1000]</code>.</li>
<li>Every <code>cost[i]</code> will be an integer in the range <code>[0, 999]</code>.</li>
</ol>
</p>
</div>

## Tags
- Array (array)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 2 (taggedByAdmin: true)
- Nvidia - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Dynamic Programming [Accepted]

**Intuition**

There is a clear recursion available: the final cost `f[i]` to climb the staircase from some step `i` is `f[i] = cost[i] + min(f[i+1], f[i+2])`.  This motivates *dynamic programming*.

**Algorithm**

Let's evaluate `f` backwards in order.  That way, when we are deciding what `f[i]` will be, we've already figured out `f[i+1]` and `f[i+2]`.

We can do even better than that.  At the `i`-th step, let `f1, f2` be the old value of `f[i+1]`, `f[i+2]`, and update them to be the new values `f[i], f[i+1]`.  We keep these updated as we iterate through `i` backwards.  At the end, we want `min(f1, f2)`.

<iframe src="https://leetcode.com/playground/HvuQ9AXf/shared" frameBorder="0" width="100%" height="242" name="HvuQ9AXf"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ where $$N$$ is the length of `cost`.

* Space Complexity: $$O(1)$$, the space used by `f1, f2`.

## Accepted Submission (python3)
```python3
from typing import List
#
# @lc app=leetcode id=746 lang=python3
#
# [746] Min Cost Climbing Stairs
#
# https://leetcode.com/problems/min-cost-climbing-stairs/description/
#
# algorithms
# Easy (46.20%)
# Total Accepted:    69.3K
# Total Submissions: 149.7K
# Testcase Example:  '[0,0,0,0]'
#
#
# On a staircase, the i-th step has some non-negative cost cost[i] assigned (0
# indexed).
#
# Once you pay the cost, you can either climb one or two steps. You need to
# find minimum cost to reach the top of the floor, and you can either start
# from the step with index 0, or the step with index 1.
#
#
# Example 1:
#
# Input: cost = [10, 15, 20]
# Output: 15
# Explanation: Cheapest is start on cost[1], pay that cost and go to the
# top.
#
#
#
# Example 2:
#
# Input: cost = [1, 100, 1, 1, 1, 100, 1, 1, 100, 1]
# Output: 6
# Explanation: Cheapest is start on cost[0], and only step on 1s, skipping
# cost[3].
#
#
#
# Note:
#
# cost will have a length in the range [2, 1000].
# Every cost[i] will be an integer in the range [0, 999].
#
#
#
class Solution:
    def minCostClimbingStairsDp(self, cost: List[int]) -> int:
        lenc = len(cost)
        memo = [0] * lenc
        memo[0] = cost[0]
        memo[1] = cost[1]
        for i in range(2, lenc):
            memo[i] = cost[i] + min(memo[i-1], memo[i-2])
        # print(memo)
        return min(memo[lenc - 1], memo[lenc - 2])
    def minCostClimbingStairs(self, cost: List[int]) -> int:
        lenc = len(cost)
        tc1 = cost[0]
        tc2 = cost[1]
        for i in range(2, lenc):
            # t = min(tc1, tc2) + cost[i]
            # tc1 = tc2
            # tc2 = t
            tc1, tc2 = tc2, min(tc1, tc2) + cost[i]
        return min(tc1, tc2)

# cost = [1,2,3,4]
# so = Solution()
# print(so.minCostClimbingStairs(cost))

```

## Top Discussions
### 4 ways | Step by step from Recursion -> top down DP -> bottom up DP -> fine tuning
- Author: avval
- Creation Date: Sat Jan 11 2020 04:48:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jan 11 2020 19:27:17 GMT+0800 (Singapore Standard Time)

<p>
We start at either step 0 or step 1. The target is to reach either last or second last step, whichever is minimum.

**Step 1 - Identify a recurrence relation between subproblems.** In this problem, 
	Recurrence Relation:
	`mincost(i) = cost[i]+min(mincost(i-1), mincost(i-2))`
    Base cases:
	`mincost(0) = cost[0]`
	`mincost(1) = cost[1]`
	
**Step 2 - Covert the recurrence relation to recursion**
```
// Recursive Top Down - O(2^n) Time Limit Exceeded
public int minCostClimbingStairs(int[] cost) {
	int n = cost.length;
	return Math.min(minCost(cost, n-1), minCost(cost, n-2));
}
private int minCost(int[] cost, int n) {
	if (n < 0) return 0;
	if (n==0 || n==1) return cost[n];
	return cost[n] + Math.min(minCost(cost, n-1), minCost(cost, n-2));
}
```


**Step 3 - Optimization 1 - Top Down DP - Add memoization to recursion** - From exponential to linear.
```
// Top Down Memoization - O(n) 1ms
int[] dp;
public int minCostClimbingStairs(int[] cost) {
	int n = cost.length;
	dp = new int[n];
	return Math.min(minCost(cost, n-1), minCost(cost, n-2));
}
private int minCost(int[] cost, int n) {
	if (n < 0) return 0;
	if (n==0 || n==1) return cost[n];
	if (dp[n] != 0) return dp[n];
	dp[n] = cost[n] + Math.min(minCost(cost, n-1), minCost(cost, n-2));
	return dp[n];
}
```
**Step 4 - Optimization 2 -Bottom Up DP - Convert recursion to iteration** - Getting rid of recursive stack
```
// Bottom up tabulation - O(n) 1ms
public int minCostClimbingStairs(int[] cost) {
	int n = cost.length;
	int[] dp = new int[n];
	for (int i=0; i<n; i++) {
		if (i<2) dp[i] = cost[i];
		else dp[i] = cost[i] + Math.min(dp[i-1], dp[i-2]);
	}
	return Math.min(dp[n-1], dp[n-2]);
}
```
**Step 5 - Optimization 3 - Fine Tuning - Reduce O(n) space to O(1)**.
```
// Bottom up computation - O(n) time, O(1) space
public int minCostClimbingStairs(int[] cost) {
	int n = cost.length;
	int first = cost[0];
	int second = cost[1];
	if (n<=2) return Math.min(first, second);
	for (int i=2; i<n; i++) {
		int curr = cost[i] + Math.min(first, second);
		first = second;
		second = curr;
	}
	return Math.min(first, second);
}
```
</p>


### 3 Lines Java Solution - O(1) space
- Author: VVtina
- Creation Date: Wed Jul 04 2018 11:17:04 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 16:09:38 GMT+0800 (Singapore Standard Time)

<p>
A bit DP and a bit Greedy
```
public int minCostClimbingStairs(int[] cost) {
        for (int i = 2; i < cost.length; i++) {
            cost[i] += Math.min(cost[i-1], cost[i-2]);
        }
        return Math.min(cost[cost.length-1], cost[cost.length-2]);
    }
```
</p>


### Doubt ??
- Author: mankadjyot
- Creation Date: Fri Jul 13 2018 02:43:53 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 08:53:23 GMT+0800 (Singapore Standard Time)

<p>
This is the example provided in the Question

```
Input: cost = [10, 15, 20]
Output: 15
Explanation: Cheapest is start on cost[1], pay that cost and go to the top.
```

Won\'t it be cheap to start at cost[0] and since you paid 10, Climb two steps to reach 20 which is top. Hence you only pay 10.

Or is my understanding of question is not corrrect ?
</p>


