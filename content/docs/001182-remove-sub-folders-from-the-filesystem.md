---
title: "Remove Sub-Folders from the Filesystem"
weight: 1182
#id: "remove-sub-folders-from-the-filesystem"
---
## Description
<div class="description">
<p>Given a list of folders, remove all sub-folders in those folders and return in <strong>any order</strong> the folders after removing.</p>

<p>If a <code>folder[i]</code> is located within&nbsp;another <code>folder[j]</code>, it is called a&nbsp;sub-folder&nbsp;of it.</p>

<p>The format of a path is&nbsp;one or more concatenated strings of the form:&nbsp;<code>/</code>&nbsp;followed by one or more lowercase English letters. For example,&nbsp;<code>/leetcode</code>&nbsp;and&nbsp;<code>/leetcode/problems</code>&nbsp;are valid paths while an empty string and&nbsp;<code>/</code>&nbsp;are not.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> folder = [&quot;/a&quot;,&quot;/a/b&quot;,&quot;/c/d&quot;,&quot;/c/d/e&quot;,&quot;/c/f&quot;]
<strong>Output:</strong> [&quot;/a&quot;,&quot;/c/d&quot;,&quot;/c/f&quot;]
<strong>Explanation:</strong> Folders &quot;/a/b/&quot; is a subfolder of &quot;/a&quot; and &quot;/c/d/e&quot; is inside of folder &quot;/c/d&quot; in our filesystem.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> folder = [&quot;/a&quot;,&quot;/a/b/c&quot;,&quot;/a/b/d&quot;]
<strong>Output:</strong> [&quot;/a&quot;]
<strong>Explanation:</strong> Folders &quot;/a/b/c&quot; and &quot;/a/b/d/&quot; will be removed because they are subfolders of &quot;/a&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> folder = [&quot;/a/b/c&quot;,&quot;/a/b/ca&quot;,&quot;/a/b/d&quot;]
<strong>Output:</strong> [&quot;/a/b/c&quot;,&quot;/a/b/ca&quot;,&quot;/a/b/d&quot;]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= folder.length&nbsp;&lt;= 4 * 10^4</code></li>
	<li><code>2 &lt;= folder[i].length &lt;= 100</code></li>
	<li><code>folder[i]</code> contains only&nbsp;lowercase letters and &#39;/&#39;</li>
	<li><code>folder[i]</code> always starts with character &#39;/&#39;</li>
	<li>Each folder name is unique.</li>
</ul>

</div>

## Tags
- Array (array)
- String (string)

## Companies
- Facebook - 5 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] 3 methods from O(n * (logn + m ^ 2)) to O(n * m), w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Oct 20 2019 12:08:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 29 2020 22:28:55 GMT+0800 (Singapore Standard Time)

<p>
**Method 1: sort by length then put only parent into HashSet**
1. Sort `folder` by length;
2. Check if the floder\'s parent fold in HashSet before adding it into the HashSet.
Note: the part before any `/` is a parent.

**Java**
```
    public List<String> removeSubfolders(String[] folder) {
        Arrays.sort(folder, Comparator.comparing(s -> s.length()));
        Set<String> seen = new HashSet<>();
        outer:
        for (String f : folder) {
            for (int i = 2; i < f.length(); ++i)
                if (f.charAt(i) == \'/\' && seen.contains(f.substring(0, i))) 
                    continue outer;
            seen.add(f);
        }
        return new ArrayList<>(seen);
    }
```
**Python 3**
```
    def removeSubfolders(self, folder: List[str]) -> List[str]:
        folder.sort(key=lambda f: len(f))
        seen = set()
        for f in folder:
            for i in range(2, len(f)):
                if f[i] == \'/\' and f[: i] in seen:
                    break
            else:
                seen.add(f)
        return list(seen)
````
A simpler version, learn from **@Sarmon**:
```
    def removeSubfolders(self, folder: List[str]) -> List[str]:
        folder.sort(key=len)
        seen = set()
        for f in folder:
            if not any(f[i] == \'/\' and f[: i] in seen for i in range(2, len(f))):
                seen.add(f)
        return list(seen)
```
**Analysis**
Sort cost n * logn;
Outer for loop run `n` times; inner for loop cost `i` in each iteration due to `substring(0, i)`, that is, `2 + ... + m`, which is `O(m ^ 2)`;
Therefore,

Time: `O(n * (logn + m ^ 2))`, space: `(n * m)`, where `n = folder.length, m = average size of the strings in folder`.

----

**Method 2: sort folders**
1. Sort the folders;
2. For each folder check if the followings are child folders; if yes, ignore; otherwise, count it in. 

```
    public List<String> removeSubfolders(String[] folder) {
        LinkedList<String> ans = new LinkedList<>();
        Arrays.sort(folder);
        for (String f : folder)
            if (ans.isEmpty() || !f.startsWith(ans.peekLast() + \'/\')) //  need \'/\' to ensure a parent.
                ans.offer(f);
        return ans;
    }
```
```
    def removeSubfolders(self, folder: List[str]) -> List[str]:
        ans = []
        for f in sorted(folder):
            if not ans or not f.startswith(ans[-1] + \'/\'):	#  need \'/\' to ensure a parent.
                ans.append(f)
        return ans
```

**Analysis**

credit to **@alanwaked** for his excellent comment:

*I think the time complexity for method 2 is actually O(n * m * logn).
Because the sort is based on merge sort for Object and time complexity of merge sort is O(n * logn). That means n * logn times comparing happened.
For this question, it just makes the comparing time be O(m). Thus it won\'t increase the number of "layers" of merge sort to log(n * m).*

Time: `O(n * m * log(n))`, space: `O(1) `(excluding space cost of sorting part), where `n = folder.length, m = average size of the strings in folder`.

----

**Method 3: Trie**

Credit to **@Phil_IV-XIII** for correction.

Use `index` to save each `folder` index in a trie node; when search the trie, if we find a folder (index >= 0) and the next char is `/`, then we get all parent folders on the current trie branch. 
```
    class Trie {
        Trie[] sub = new Trie[27];
        int index = -1;
    }
    public List<String> removeSubfolders(String[] folder) {
        Trie root = new Trie();
        for (int i = 0; i < folder.length; ++i) {
            Trie t = root;
            for (char c : folder[i].toCharArray()) {
                int idx = c == \'/\' ? 26 : c - \'a\'; // correspond \'/\' to index 26.
                if (t.sub[idx] == null)
                    t.sub[idx] = new Trie();
                t = t.sub[idx];
            }
            t.index = i;
        }
        return bfs(root, folder);
    }
    private List<String> bfs(Trie t, String[] folder) {
        List<String> ans = new ArrayList<>();
        Queue<Trie> q = new LinkedList<>();
        q.offer(t);
        while (!q.isEmpty()) { // BFS search.
            t = q.poll();
            if (t.index >= 0) { // found a parent folder, but there might be more.
                ans.add(folder[t.index]);
            }
            for (int i = 0; i < 27; ++i)
                if (t.sub[i] != null && !(i == 26 && t.index >= 0)) // not yet found all parent folders in current trie branch.
                    q.offer(t.sub[i]);
        }
        return ans;
    }
```
```
class Trie:
    def __init__(self):
        self.sub = collections.defaultdict(Trie)
        self.index = -1

class Solution:
    def removeSubfolders(self, folder: List[str]) -> List[str]:
        self.root = Trie()
        for i in range(len(folder)):
            cur = self.root
            for c in folder[i]:
                cur = cur.sub[c]
            cur.index = i
        return self.bfs(self.root, folder)
    def bfs(self, trie: Trie, folder: List[str]) -> List[str]:
        q, ans = [trie], []
        for t in q:
            if t.index >= 0:
                ans.append(folder[t.index])
            for c in t.sub.keys():
                if \'/\' != c or t.index < 0:
                    q.append(t.sub.get(c))
        return ans
```

**Analysis:**

Time & space: O(n * m), where `n = folder.length, m = average size of the strings in folder`.

----


**Compare the first two methods**

Generally speaking, `m > logn`,

For method 2: 
`O(n * m * logn)`

For method 1:
`O(n * (logn + m ^ 2)) = O(n * m ^ 2) > O(n * m * logn)` - time complexity of method 1.

Conclusion:
1. Method 2 is more space efficent and is, generally speaking, faster than method 1; 
2. Method 1 is faster if folders\' average size `m < logn`.
</p>


### [C++] Short, sorting O(nlogn)
- Author: PhoenixDD
- Creation Date: Sun Oct 20 2019 12:01:03 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 27 2020 10:14:55 GMT+0800 (Singapore Standard Time)

<p>
**Observation**

* Strings with subfolders will always be of larger length than the ones that include their parents.
* When input is sorted, all we need to do iterate through all the input strings and look at the last found parent which will be the only viable candidate to be a parent, if it\'s not then the current string is a parent.
* Since the question states that the folders are unique, we don\'t need to check the if there are multiple same parents
We use these observations to form the solution.

 **Solution**
```c++
class Solution {
public:
    vector<string> removeSubfolders(vector<string>& folder) 
    {
        sort(folder.begin(),folder.end());
        vector<string> result;
        for(string &s:folder)
            if(result.empty()||result.back().compare(0,result.back().length(),s,0,result.back().length())!=0||s[result.back().length()]!=\'/\') //Check if 1. This is the first string, 2.parent at back is not the parent of `s` by comparing.
                result.push_back(s);                                   //3. If the entire parent matches `s` check if last folder name in parent does not match the folder of same depth in `s`. for cases like `/a/b, /a/bc`.
        return result;
    }
};
```
**Complexity**
Space: `O(1)`. Although I would say using output vector for calculations in not space saving.
Time: `O(nlogn)` (considering the size of input) else `O(avg(s.length())*nlogn)` where s are the strings in folder.
</p>


### Java/C# Easy and Simple Solution with Space O(1)
- Author: venendroid
- Creation Date: Sun Oct 20 2019 12:05:13 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 24 2019 22:31:58 GMT+0800 (Singapore Standard Time)

<p>
Time Complexity: O(nlogn)
Space: O(1)

```
public IList<string> RemoveSubfolders(string[] folder)
    {        
	 //Sort the list	
      Array.Sort(folder);
      
       string temp = folder[0];	   
      var res = new List<string>(){temp};
      for(int i = 1; i < folder.Length; i++)
      {
	  // Check if the string StartsWith or not. If not, add to the result list
          if(!(folder[i]).StartsWith(temp + "/"))
          {
              res.Add(folder[i]);
              temp = folder[i];
          }
      }
         
      return res;  
    }
```

//*** Thanks to all the comments on the improvments. Changed from Contains to StartsWith. 

//PS:// Please vote if you like this solution.
</p>


