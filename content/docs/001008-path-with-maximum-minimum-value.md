---
title: "Path With Maximum Minimum Value"
weight: 1008
#id: "path-with-maximum-minimum-value"
---
## Description
<div class="description">
<p>Given a&nbsp;matrix of integers <code>A</code>&nbsp;with&nbsp;<font face="monospace">R</font>&nbsp;rows and <font face="monospace">C</font>&nbsp;columns, find&nbsp;the <strong>maximum</strong>&nbsp;score&nbsp;of a path starting at&nbsp;<code>[0,0]</code>&nbsp;and ending at <code>[R-1,C-1]</code>.</p>

<p>The <em>score</em> of a path is the <strong>minimum</strong> value in that path.&nbsp; For example, the value of the path 8 &rarr;&nbsp; 4 &rarr;&nbsp; 5 &rarr;&nbsp; 9 is 4.</p>

<p>A <em>path</em> moves some number of times from one visited cell to any neighbouring unvisited cell in one of the 4 cardinal directions (north, east, west, south).</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/04/23/1313_ex1.JPG" style="width: 70px; height: 59px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[5,4,5],[1,2,6],[7,4,6]]</span>
<strong>Output: </strong><span id="example-output-1">4</span>
<strong>Explanation: </strong>
The path with the maximum score is highlighted in yellow. 
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/04/23/1313_ex2.JPG" style="width: 134px; height: 39px;" /></strong></p>

<pre>
<strong>Input: </strong><span>[[2,2,1,2,2,2],[1,2,2,2,1,2]]</span>
<strong>Output: 2</strong></pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/04/23/1313_ex3.JPG" /></strong></p>

<pre>
<strong>Input: </strong><span>[[3,4,6,3,4],[0,2,1,1,7],[8,8,3,2,7],[3,2,4,9,8],[4,1,2,0,0],[4,6,5,4,3]]</span>
<strong>Output: 3</strong></pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= R, C&nbsp;&lt;= 100</code></li>
	<li><code>0 &lt;= A[i][j] &lt;= 10^9</code></li>
</ol>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Union Find (union-find)
- Graph (graph)

## Companies
- Amazon - 3 (taggedByAdmin: true)
- Google - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Dijkstra / Binary Search + DFS / Union Find, complexity analysis
- Author: Olala_michelle
- Creation Date: Wed Oct 30 2019 03:55:07 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 31 2019 06:17:25 GMT+0800 (Singapore Standard Time)

<p>
* Time: O(MN log MN), since for each element in matrix we have to do a heap push, which cost O(log # of element in the heap) times. The size of the heap can grow up to # of elemnts in the matrix.
* Space: O(MN). We need to keep track of the elements we have seen so far. Finally the size of seen will grow up to # of items in the matrix.

```
class Solution:
    def maximumMinimumPath(self, A: List[List[int]]) -> int:
        dire = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        R, C = len(A), len(A[0])
        
        maxHeap = [(-A[0][0], 0, 0)]
        seen = [[0 for _ in range(C)] for _ in range(R)]
        while maxHeap:
            val, x, y = heapq.heappop(maxHeap)
            # seen[x][y] = 1 # got TLE
            if x == R - 1 and y == C - 1: return -val
            for dx, dy in dire:
                nx, ny = x + dx, y + dy
                if 0 <= nx < R and 0 <= ny < C and not seen[nx][ny]:
                    seen[nx][ny] = 1 # passed
                    heapq.heappush(maxHeap, (max(val, -A[nx][ny]), nx, ny))
        return -1
```

**Binary Search + DFS:**
* Time: O(MN log MN)
* Space:  O(MN)

**Intuition:**
* remove all the cells with value >= `min(begin,end)`
* sort all remaining unique values
* enumerate all remaining values to find a maximum value that is the minimum value in a path from the begin to the end; for each value, we use DFS to check weather there exists a path from begin to end such that this value is the the minimum among the values in that path.
	*  if we find that value, we keep Binary Search to try to find a bigger value
	*  we loose our search criatria and see if there is path from begin to end that all the values in that path >= a smaller value by moving the right pointer to the left

**Why use DFS?**
we use DFS to check if there exist a path from the being to the end 

**Why use Binary Search?**
we use binary search to find the upper boundary. So when we find a valid value, we move left pointer to mid + 1 to keep finding a larger value. Just as what we did in finding the first bad version: if we find a bad version, we move right pointer to the mid - 1 to find a earlier bad product. 

**What are in the sorted array? / What are we binary search for?**
Since we don\'t know weather a value \'val\'. in this sorted array is the minimum, so we deliberately make it be, by only considering the cells with values that are larger than this value. And if this arrangement doesn\'t work (we cannot construct a path from begin to the end with \'val\' being the minimum value seen) when BinarySearch(\'val\') returns False.

```
class Solution:
    def maximumMinimumPath(self, A: List[List[int]]) -> int:
        dire = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        R, C = len(A), len(A[0])
        
        def check(val):
            memo = [[0 for _ in range(C)] for _ in range(R)]
            
            def dfs(x,y):
                if x == R - 1 and y == C - 1:
                    return True
                memo[x][y] = 1
                for d in dire:
                    nx = x + d[0]
                    ny = y + d[1]
                    if 0 <= nx < R and 0 <= ny < C and not memo[nx][ny] and A[nx][ny] >= val and dfs(nx,ny):
                        return True
                return False
            
            return dfs(0,0)   
        
        unique = set()
        ceiling = min(A[0][0], A[-1][-1])
        for r in range(R):
            for c in range(C):
                if A[r][c] <= ceiling:
                    unique.add(A[r][c])
                
        arr = sorted(unique)
        l, r = 0, len(arr) - 1
        while l <= r:
            m = l + (r - l) // 2
            # if check(m):
            if check(arr[m]):
                # cause we\'re trying to find the MAXIMUM of \'minimum\'
                l = m + 1
            else:
                r = m - 1
        return arr[r]

```

**Union Find:**
Idea is similar to LC788 Swim in Rising Water or percolation.

We want to find a path from (0,0) to (n-1, m-1) w/ max lower bound. So we just visit the cell in the order from largest to smallest, and use UF to connect all the **visited** cells. Once we make (0,0) and (n-1, m-1) connected, we know we get a path with max lower bound, which is just the value of the last visited cell.

1. sort all the points in a descending order
2. union the point with the explored points until `start` and `end` has the same parent

* Time: O(MN log MN)
* Space: O(MN)
```
class Solution:
    def maximumMinimumPath(self, A: List[List[int]]) -> int:
        R, C = len(A), len(A[0])
        parent = [i for i in range(R * C)]
        dire = [(0, 1), (0, -1), (1, 0), (-1, 0)]
        seen = [[0 for _ in range(C)] for _ in range(R)]
        
        def find(x):
            if parent[x] != x:
                parent[x] = find(parent[x])
            return parent[x]
        
        def union(x, y):
            rx, ry = find(x), find(y)
            if rx != ry:
                parent[ry] = rx
        
        points = [(x, y) for x in range(R) for y in range(C)]
        points.sort(key = lambda x: A[x[0]][x[1]], reverse = True)
        
        for x, y in points:
            seen[x][y] = 1
            for dx, dy in dire:
                nx, ny = x + dx, y + dy
                if 0 <= nx < R and 0 <= ny < C and seen[nx][ny]:
                    union(x * C + y, nx * C + ny)
            if find(0) == find(R * C - 1):
                return A[x][y]
        return -1
        
```
</p>


### Clear Code Dijkstra Algorithm (C++/Java/Python/Go/PHP)
- Author: Cosmopolitan
- Creation Date: Tue Jul 02 2019 08:44:36 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 30 2020 01:57:27 GMT+0800 (Singapore Standard Time)

<p>
BFS Dijkstra algorithm: use a priority queue to choose the next step with the maximum value. Keep track of the mininum value along the path.

**C++:**
```
    int maximumMinimumPath(vector<vector<int>>& A) {
        static constexpr int DIRS[][2] {{0,1},{1,0},{0,-1},{-1,0}};
        priority_queue<tuple<int,int,int>> pq;
        pq.emplace(A[0][0], 0, 0);
        int n = A.size(), m = A[0].size(), maxscore = A[0][0];
        A[0][0] = -1; // visited
        while(!pq.empty()) {
            auto [a, i, j] = pq.top();
            pq.pop();
            maxscore = min(maxscore, a);
            if(i == n - 1 && j == m - 1)
                break;
            for(const auto& d : DIRS)
                if(int newi = d[0] + i, newj = d[1] + j;
                   newi >= 0 && newi < n && newj >= 0 && newj < m && A[newi][newj]>=0){
                    pq.emplace(A[newi][newj], newi, newj);
                    A[newi][newj] = -1;
                }
        }
        return maxscore;
    }
```
**Java:**
```
    public int maximumMinimumPath(int[][] A) {
        final int[][] DIRS = {{0,1},{1,0},{0,-1},{-1,0}};
        Queue<int[]> pq = new PriorityQueue<>((a, b) -> Integer.compare(b[0], a[0]));
        pq.add(new int[] {A[0][0], 0, 0});
        int maxscore = A[0][0];
        A[0][0] = -1; // visited
        while(!pq.isEmpty()) {
            int[] top = pq.poll();
            int i = top[1], j = top[2], n = A.length, m = A[0].length;
            maxscore = Math.min(maxscore, top[0]);
            if(i == n - 1 && j == m - 1)
                break;
            for(int[] d : DIRS) {
                int newi = d[0] + i, newj = d[1] + j;
                if(newi >= 0 && newi < n && newj >= 0 && newj < m && A[newi][newj]>=0){
                    pq.add(new int[] {A[newi][newj], newi, newj});
                    A[newi][newj] = -1;
                }
            }
        }
        return maxscore;
    }
```
**Python:**
```
class Solution(object):
    def maximumMinimumPath(self, A):
        """
        :type A: List[List[int]]
        :rtype: int
        """
        DIRS = [[0,1], [1,0], [0,-1], [-1,0]]
        pq = []
        # negate element to simulate max heap
        heappush(pq, (-A[0][0], 0, 0))
        n, m, maxscore = len(A), len(A[0]), A[0][0]
        while len(pq) != 0:
            (a, i, j) = heappop(pq)
            maxscore = min(maxscore, -a)
            if i == n - 1 and j == m - 1:
                break
            for d in DIRS:
                newi, newj = d[0] + i, d[1] + j
                if newi >= 0 and newi < n and newj >= 0 and newj < m and A[newi][newj] >= 0:
                    heappush(pq, (-A[newi][newj], newi, newj))
                    A[newi][newj] = -1;
        return maxscore
```
**Go:**
```
// The minimal implementation of the Priority Queue
type Item struct {
	value int   // The value of the cell.
	row int
	column int
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	// Pop returns the greatest
	return pq[i].value > pq[j].value
}

func (pq *PriorityQueue) Push(x interface{}) {
	item := x.(*Item)
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil  // avoid memory leak
	*pq = old[0 : n-1]
	return item
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func Min(x, y int) int {
    if x > y {
        return y
    }
    return x
}

func maximumMinimumPath(A [][]int) int {
    DIRS := [][2]int{{0,1},{1,0},{0,-1},{-1,0}}
    pq := PriorityQueue{}
    heap.Push(&pq, &Item{A[0][0], 0, 0})
    n := len(A)
    m := len(A[0])
    maxscore := A[0][0];
    A[0][0] = -1; // visited
    for pq.Len() > 0 {
        top := heap.Pop(&pq).(*Item)
        maxscore = Min(maxscore, top.value)
        if top.row == n - 1 && top.column == m - 1 {
            break
        }
        for _, d := range DIRS {
            newi := d[0] + top.row
            newj := d[1] + top.column
            if newi >= 0 && newi < n && newj >= 0 && newj < m && A[newi][newj]>=0 {
                heap.Push(&pq, &Item{A[newi][newj], newi, newj});
                A[newi][newj] = -1;
            }
        }
    }
    return maxscore;
}
```
**PHP:**
```
    function maximumMinimumPath($A) {
        $DIRS = [[0,1],[1,0],[0,-1],[-1,0]];
        $pq = new SplPriorityQueue;
        $pq->insert([$A[0][0], 0, 0], $A[0][0]);
        $n = count($A);
        $m = count($A[0]);
        $maxscore = $A[0][0];
        $A[0][0] = -1; // visited
        while (!$pq->isEmpty()) {
            [$a, $i, $j] = $pq->extract();
            $maxscore = min($maxscore, $a);
            if($i == $n - 1 && $j == $m - 1)
                break;
            foreach ($DIRS as $d) {
                $newi = $d[0] + $i;
                $newj = $d[1] + $j;
                if ($newi >= 0 && $newi < $n && $newj >= 0 && $newj < $m && $A[$newi][$newj] >= 0) {
                    $pq->insert([$A[$newi][$newj], $newi, $newj], $A[$newi][$newj]);
                    $A[$newi][$newj] = -1;
                }
            }
        }
        return $maxscore;
    }
</p>


### Java BFS + PQ
- Author: daniel_stoian
- Creation Date: Mon Jul 01 2019 01:40:47 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 01 2019 01:40:47 GMT+0800 (Singapore Standard Time)

<p>
Thank you @venkim for posting the original solution. 

Rewrote the code a little bit and added some comments for clarity.

Please see the original solution here: https://leetcode.com/problems/path-with-maximum-minimum-value/discuss/323118/Java-simple-solution-using-PQ

```
class Solution {
  int[][] directions = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
  
  public int maximumMinimumPath(int[][] A) {
    int n = A.length;
    int m = A[0].length;
    boolean[][] visited = new boolean[n][m];
    
    // in the BFS approach, for each step, we are interested in getting the maximum min that we have seen so far, thus we reverse the ordering in the pq
    Queue<int[]> pq = new PriorityQueue<>((a,b) -> b[2] - a[2]);
    
    pq.offer(new int[]{0, 0, A[0][0]});
    
    // BFS
    while (!pq.isEmpty()) {
      int[] cell = pq.poll();
      int row = cell[0];
      int col = cell[1];
      
      if (row == n - 1 && col == m - 1) {
        return cell[2];
      }
      
      visited[row][col] = true;
      
      for (int[] dir : directions) {
        int nextRow = row + dir[0];
        int nextCol = col + dir[1];
        
        if (nextRow < 0 || nextRow >= n || nextCol < 0 || nextCol >= m || visited[nextRow][nextCol]) continue;
        
        // we are keeping track of the min element that we have seen until now
        pq.offer(new int[]{nextRow, nextCol, Math.min(cell[2], A[nextRow][nextCol])});
      }
    }
    return -1;
  }
}
```
</p>


