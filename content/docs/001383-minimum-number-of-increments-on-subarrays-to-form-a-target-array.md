---
title: "Minimum Number of Increments on Subarrays to Form a Target Array"
weight: 1383
#id: "minimum-number-of-increments-on-subarrays-to-form-a-target-array"
---
## Description
<div class="description">
<p>Given an array of positive integers <code>target</code> and an array <code>initial</code> of same size with all zeros.</p>

<p>Return the minimum number of operations to form a <code>target</code> array from <code>initial</code>&nbsp;if you are allowed to do the following operation:</p>

<ul>
	<li>Choose <strong>any</strong> subarray from <code>initial</code>&nbsp;and increment each value by one.</li>
</ul>
The answer is guaranteed to fit within the range of a 32-bit signed integer.
<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> target = [1,2,3,2,1]
<strong>Output:</strong> 3
<strong>Explanation: </strong>We need at least 3 operations to form the target array from the initial array.
[0,0,0,0,0] increment 1 from index 0 to 4&nbsp;(inclusive).
[1,1,1,1,1] increment 1 from index 1 to 3&nbsp;(inclusive).
[1,2,2,2,1] increment 1 at index 2.
[1,2,3,2,1] target array is formed.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> target = [3,1,1,2]
<strong>Output:</strong> 4
<strong>Explanation: </strong>(initial)[0,0,0,0] -&gt; [1,1,1,1] -&gt; [1,1,1,2] -&gt; [2,1,1,2] -&gt; [3,1,1,2] (target).
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> target = [3,1,5,4,2]
<strong>Output:</strong> 7
<strong>Explanation: </strong>(initial)[0,0,0,0,0] -&gt; [1,1,1,1,1] -&gt; [2,1,1,1,1] -&gt; [3,1,1,1,1] 
                                  -&gt; [3,1,2,2,2] -&gt; [3,1,3,3,2] -&gt; [3,1,4,4,2] -&gt; [3,1,5,4,2] (target).
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> target = [1,1,1,1]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= target.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= target[i] &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Segment Tree (segment-tree)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Detailed Explanation
- Author: khaufnak
- Creation Date: Sun Jul 26 2020 00:00:51 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 27 2020 17:49:19 GMT+0800 (Singapore Standard Time)

<p>
Start with first position `a[0]`. We know that it will take overall `a[0]` operations to reach `a[0]` from 0. 

Now, 2 things can happen from here:

1. We encounter a number less than `a[0]` ```(a[1] < a[0])```: In this case we can simply reuse the same operations that we did for `a[0]`. i.e If array was (3, 2), we can first perform 3 operations and then use 2 of the same operations in next term. However, going forward, we will only have `a[1]` operations available for reuse.

2. We encounter a number greater than `a[0]` ```(a[1] > a[0])```: In this case we  can simply reuse the same operations that we did for `a[0]`. And additionally, we will perform `a[1] - a[0]` more operation to reach `a[1]`. Again, going forward, we will have `a[1]` operations available for reuse.

Casewise, easy to understand solution:

```
class Solution {
    public int minNumberOperations(int[] target) {
        int totalOperations = target[0];
        int operationsWeCanReuse = target[0];
        
        for (int i = 1; i < target.length; ++i) {
            if (target[i] <= operationsWeCanReuse) { // Case #1
                operationsWeCanReuse = target[i];
            } else { // Case #2
                totalOperations += target[i] - operationsWeCanReuse; 
                operationsWeCanReuse = target[i];
            }
        }
        return totalOperations;
    }
}

```

Refactored, clean solution: 

```
class Solution {
    public int minNumberOperations(int[] target) {
        int totalOperations = target[0];
        for (int i = 1; i < target.length; ++i) {
            if (target[i] > target[i-1]) {
                totalOperations += target[i] - target[i-1];
            }
        }
        return totalOperations;
    }
}
```

Complexity: ```O(n)```

Still can\'t visualize? This might help:

Consider a chain made up of multiple links, each with different strengths. 

![image](https://assets.leetcode.com/users/images/71067d23-ebb4-46c5-9bc4-ff706c67fd3e_1595729068.1669052.png)

By applying force *f* at both the ends you weaken all links in the chain by *f*.

Now, let\'s try to destroy the chain completely. 

As soon as we apply *f* = 1, we will break the chain into two pieces. Result:

![image](https://assets.leetcode.com/users/images/97303927-e78e-400b-9956-2c86ffe5548a_1595729460.8027384.png)

Now, we have to break these two chain individually. We have to tell sum of all forces that we applied.

> Observation: A link will only let a force equal to its strength pass through it.
</p>


### [Java/C++/Python] Comparison of Consecutive Elements
- Author: lee215
- Creation Date: Sun Jul 26 2020 00:02:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 00:20:21 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
Whenever the current element `a` is bigger than the previous element,
we need at lease `a - pre` operations to make this difference.

We accumulate the total number of the operations,
this result is a lower bound and it\'s feasible.
<br>

# **Complexity**
Time `O(N)`
Space `O(1)`
<br>

**Java**
```java
    public int minNumberOperations(int[] A) {
        int res = 0, pre = 0;
        for (int a: A) {
            res += Math.max(a - pre, 0);
            pre = a;
        }
        return res;
    }
```
**C++**
```cpp
    int minNumberOperations(vector<int>& A) {
        int res = 0, pre = 0;
        for (int a: A) {
            res += max(a - pre, 0);
            pre = a;
        }
        return res;
    }
```
**Python**
```py
    def minNumberOperations(self, A):
        res = pre = 0
        for a in A:
            res += max(a - pre, 0)
            pre = a
        return res
```
<br>

# Shorter versions
**Java**
```java
    public int minNumberOperations(int[] A) {
        int res = 0, pre = 0;
        for (int a : A) {
            res += Math.max(a - pre, 0);
            pre = a;
        }
        return res;
    }
```
**C++**
```cpp
    int minNumberOperations(vector<int>& A) {
        int res = A[0];
        for (int i = 1; i < A.size(); ++i)
            res += max(A[i] - A[i - 1], 0);
        return res;
    }
```
**Python**
```py
    def minNumberOperations(self, A):
        return sum(max(b - a, 0) for b, a in zip(A, [0] + A))
```

</p>


### Wall of bricks
- Author: coder206
- Creation Date: Sun Jul 26 2020 00:02:17 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 27 2020 17:14:37 GMT+0800 (Singapore Standard Time)

<p>
Let us assume that target represents height of columns on a square grid. One operation corresponds to laying out continuous row of bricks. What is the number of these rows? To find this number we count the number of left edges of these rows.

Example: [3,1,5,4,2,3,4,2]. Left edges are marked by red color. Total number of left edges is 3 + 4 + 1 + 1.
![image](https://assets.leetcode.com/users/images/3f930d4e-1eea-442d-b27e-31a394ec301d_1595841158.565036.png)

Applying this approach we get
```
int minNumberOperations(vector<int>& target)
{
    int count = target[0];
    for (int i = 1; i < target.size(); i++)
        count += max(target[i] - target[i - 1], 0);
    return count;
}
```
</p>


