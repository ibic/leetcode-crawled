---
title: "Day of the Week"
weight: 1153
#id: "day-of-the-week"
---
## Description
<div class="description">
<p>Given a date, return the corresponding day of the week for that date.</p>

<p>The input is given as three integers representing the <code>day</code>, <code>month</code> and <code>year</code> respectively.</p>

<p>Return the answer as one of the following values&nbsp;<code>{&quot;Sunday&quot;, &quot;Monday&quot;, &quot;Tuesday&quot;, &quot;Wednesday&quot;, &quot;Thursday&quot;, &quot;Friday&quot;, &quot;Saturday&quot;}</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> day = 31, month = 8, year = 2019
<strong>Output:</strong> &quot;Saturday&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> day = 18, month = 7, year = 1999
<strong>Output:</strong> &quot;Sunday&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> day = 15, month = 8, year = 1993
<strong>Output:</strong> &quot;Sunday&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The given dates are valid&nbsp;dates between the years <code>1971</code> and <code>2100</code>.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Microsoft - 6 (taggedByAdmin: false)
- United Health Group - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python3 Solution With No Knowledge (Without knowing formulas or week day of 1/1/1971 beforehand)
- Author: magks
- Creation Date: Sun Sep 15 2019 06:01:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 15 2019 18:21:51 GMT+0800 (Singapore Standard Time)

<p>
In an interview situation where perhaps it is only you and a whiteboard, it is helpful to be able to find the solution without 
needing extra information than what you walked in with. This does assume knowledge of the number of days each month has.

Other solutions require knowing which day of the week fell on 1971/1/1  or one of the closed formulas like the Zeller or Sakamoto formulas.

This solution uses the current date (literally the day you sit down to solve the problem) to find the correct offset to a list of strings representings the days of the week, i.e. [ "Sunday", "Monday", ... ].

```
        def unknownStartDay(day, month, year):
		    def hasLeapDay(year):
			    return 1 if year % 4 == 0 and year % 100 != 0 or year % 400 == 0 else 0
		    
			dayNames = ["Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
            daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
            # days since 31, 12, 1970
            def daysSinceStart(day, month, year):
                numDays = 0
                for y in range(year - 1, 1970, -1):
                    numDays += 365 + hasLeapDay(y)
                numDays += sum(daysInMonth[:month-1])
                numDays += day 
                if month > 2:    
                    numDays += hasLeapDay(year)
                return numDays
			
            knownStart = daysSinceStart(14,9,2019)
            d = daysSinceStart(day, month, year) 
            return dayNames[ (d - knownStart) % 7]
```

In general, you can start the list of days with a known date. For example, if today when writing the solution is Day = 14, Month = 9, and Year = 2019 and you know the day name is "Saturday" then +1 day must be "Sunday" and likewise -1 day must be "Friday".

So start your list of day names with the known date of "today", e.g. "Saturday". Then, find the number of days since the start of the valid range for  both your known date and the input date. Note, the code considers 1, 1, 1971 to be 1 day after the start and so on.

The difference will be the  +1, -1, .. etc. offset that you are looking for, just use this difference mod 7 to correctly index the list of days. 

This is not the most efficient but allows for a correctly working algorithm on a whiteboard without having extra knowledge (except the number of days in each month and the names of the days).
</p>


### [Java/C++/Python] Zeller Formula
- Author: lee215
- Creation Date: Sun Sep 08 2019 12:05:15 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 17 2020 10:08:39 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Is this a real interview problem?
The formula for this problem is Zelle formula
Another name: Zeller\'s congruence or Kim larsen calculation formula.
<br>

## **Complexity**
Time `O(1)`
Space `O(1)`
<br>

**Java:**
```java
    String[] days = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    public String dayOfTheWeek(int d, int m, int y) {
        if (m < 3) {
            m += 12;
            y -= 1;
        }
        int c = y / 100;
        y = y % 100;
        int w = (c / 4 - 2 * c + y + y / 4 + 13 * (m + 1) / 5 + d - 1) % 7;
        return days[(w + 7) % 7];
    }
```

**C++:**
```cpp
    vector<string> days= {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    string dayOfTheWeek(int d, int m, int y, int c = 0) {
        if (m < 3) m += 12, y -= 1;
        c = y / 100, y = y % 100;
        int w = (c / 4 - 2 * c + y + y / 4 + 13 * (m + 1) / 5 + d - 1) % 7;
        return days[(w + 7) % 7];
    }
```

**Python:**
```python
    days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", ]
    def dayOfTheWeek(self, d, m, y):
        if m < 3:
            m += 12
            y -= 1
        c, y = y / 100, y % 100
        w = (c / 4 - 2 * c + y + y / 4 + 13 * (m + 1) / 5 + d - 1) % 7
        return self.days[w]
```

## Python Cheat
As I mentioned the usage of `datetime` in [1154. Day of the Year](https://leetcode.com/problems/day-of-the-year/discuss/355854/Python-Cheat)
I google the doc of package and find a method called `weekday`
<br>

```python
    def dayOfTheWeek(self, d, m, y):
        days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
        from datetime import datetime
        return days[datetime(y, m, d).weekday()]
```

</p>


### cpp solution easy to understand, you don't need to know what day of the week 1970.1.1 is
- Author: zhwenwan
- Creation Date: Sun Sep 08 2019 14:54:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 14:54:24 GMT+0800 (Singapore Standard Time)

<p>
```cpp
class Solution {
public:
    int daysOfMonth[2][12] = {
        31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };
    string weekName[7] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    bool isleapyear(int year)
    {
        return ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0));
    }
    int daystill1971(int year, int month, int day)
    {
        int count = 0;
        int year1 = 1970, month1 = 1, day1 = 1;
        while (year1 != year)
        {
            bool b = isleapyear(year1);
            if (b) count += 366;
            else count += 365;
            year1++;
        }
        int b = isleapyear(year1) ? 0 : 1;
        for (int i = 0; i < month - 1; i++) count += daysOfMonth[b][i];
        count += day - 1;
        return count;
    }
    string dayOfTheWeek(int day, int month, int year) {
        int days1 = daystill1971(2019, 9, 8);//today(2019,9,8) is Sunday
        int days2 = daystill1971(year, month, day);
        int days = (((days2 - days1) % 7) + 7) % 7;//Number of days off
        return weekName[days];
    }
};
```
</p>


