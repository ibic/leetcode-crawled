---
title: "Unique Binary Search Trees"
weight: 96
#id: "unique-binary-search-trees"
---
## Description
<div class="description">
<p>Given <em>n</em>, how many structurally unique <strong>BST&#39;s</strong> (binary search trees) that store values 1 ...&nbsp;<em>n</em>?</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> 3
<strong>Output:</strong> 5
<strong>Explanation:
</strong>Given <em>n</em> = 3, there are a total of 5 unique BST&#39;s:

   1         3     3      2      1
    \       /     /      / \      \
     3     2     1      1   3      2
    /     /       \                 \
   2     1         2                 3
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 19</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Tree (tree)

## Companies
- Amazon - 5 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Wish - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Snapchat - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Dynamic Programming

**Intuition**

The problem can be solved in a dynamic programming way.

Given a sorted sequence `1 ... n`, to construct a Binary Search Tree (BST) out of the sequence,
we could enumerate each number `i` in the sequence, and use the number as the root,
then, the subsequence `1 ... (i-1)` on its left side would lay on the left branch of the root,
and similarly the right subsequence `(i+1) ... n` lay on the right branch of the root.
We then can construct the subtree from the subsequence recursively.
Through the above approach, we could be assured that the BST that we construct are all unique,
since they start from unique roots.

As we can see, the problem can be reduced into problems with smaller sizes,
instead of recursively (also repeatedly) solve the subproblems,
we can store the solution of subproblems and reuse them later,
*i.e.* the dynamic programming way.

**Algorithm**

The problem is to calculate the number of unique BST.
To do so, we can define two functions:

1. $$G(n)$$: the number of unique BST for a sequence of length `n`.

2. $$F(i, n)$$: the number of unique BST,
where the number `i` is served as the root of BST ($$1 \leq i \leq n$$).

As we can see,
>$$G(n)$$ is actually the desired function we need in order to solve the problem.


*Later we would see that $$G(n)$$ can be deducted from $$F(i, n)$$, which at the end, would recursively refers to $$G(n)$$.*

First of all, following the idea in the section of intuition,
we can see that the total number of unique BST $$G(n)$$,
is the sum of BST $$F(i, n)$$ enumerating each number `i` (`1 <= i <= n`) as a root.
*i.e.*

$$
G(n) = \sum_{i=1}^{n} F(i, n) \qquad  \qquad (1)
$$

Particularly, for the bottom cases, there is only one combination to construct a BST
out of a sequence of length 1 (only a root) or nothing (empty tree).
*i.e.*

$$
G(0) = 1, \qquad G(1) = 1
$$


Given a sequence `1 ... n`, we pick a number `i` out of the sequence as the root,
then the number of unique BST with the specified root defined as $$F(i, n)$$,
is the **cartesian product** of the number of BST for its left and right subtrees, as illustrated below:

<center><img src="../Figures/96_BST.png" width="550px" /></center>

For example, $$F(3, 7)$$, the number of unique BST tree with the number `3` as its root.
To construct an unique BST out of the entire sequence `[1, 2, 3, 4, 5, 6, 7]` with `3` as the root,
which is to say, we need to construct a subtree out of its left subsequence `[1, 2]` and
another subtree out of the right subsequence `[4, 5, 6, 7]`,
and then combine them together (*i.e.* cartesian product).
Now the tricky part is that we could consider the number of unique BST out of sequence `[1,2]` as $$G(2)$$,
and the number of of unique BST out of sequence `[4, 5, 6, 7]` as $$G(4)$$. For $$G(n)$$,
it does not matter the content of the sequence, but the length of the sequence.
Therefore, $$F(3,7) = G(2) \cdot G(4)$$. To generalise from the example, we could derive the following formula:

$$
F(i, n) = G(i-1) \cdot G(n-i) \qquad  \qquad (2)
$$

By combining the formulas (1), (2), we obtain a recursive formula for $$G(n)$$, *i.e.*

$$
G(n) = \sum_{i=1}^{n}G(i-1) \cdot G(n-i) \qquad  \qquad (3)
$$

To calculate the result of function, we start with the lower number, since the value of $$G(n)$$ depends on the values of $$G(0) … G(n-1)$$.

With the above explanation and formulas, one can easily implement an algorithm to calculate the $$G(n)$$. Here are some examples:

<iframe src="https://leetcode.com/playground/JiyWwdB2/shared" frameBorder="0" width="100%" height="293" name="JiyWwdB2"></iframe>

**Complexity Analysis**

* Time complexity : the main computation of the algorithm is done at the statement with `G[i]`.
So the time complexity is essentially the number of iterations for the statement,
which is $$\sum_{i=2}^{n} i = \frac{(2+n)(n-1)}{2}$$, to be exact, therefore the time complexity is $$O(N^2)$$

* Space complexity : The space complexity of the above algorithm is mainly the storage to
keep all the intermediate solutions, therefore $$O(N)$$.

<br/>

---
 

#### Approach 2: Mathematical Deduction

**Intuition**

Actually, as it turns out, the sequence of  $$G(n)$$ function results is known as [Catalan number](https://en.wikipedia.org/wiki/Catalan_number) $$C_n$$. And one of the more convenient forms for calculation is defined as follows:

$$
C_0 = 1, \qquad C_{n+1} = \frac{2(2n+1)}{n+2}C_n \qquad  \qquad (4)
$$

We skip the proof here, which one can find following the above reference.

**Algorithm**

Given the formula (3), it becomes rather easy to calculate $$G_n$$ which is actually $$C_n$$. Here are some examples:
<iframe src="https://leetcode.com/playground/ZmqfEiPu/shared" frameBorder="0" width="100%" height="225" name="ZmqfEiPu"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N)$$, as one can see, there is one single loop in the algorithm.
* Space complexity : $$O(1)$$, we use only one variable to store all the intermediate results and the final one.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### DP Solution in 6 lines with explanation. F(i, n) = G(i-1) * G(n-i)
- Author: liaison
- Creation Date: Wed Feb 04 2015 17:50:31 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 26 2019 15:51:54 GMT+0800 (Singapore Standard Time)

<p>
The problem can be solved in a dynamic programming way. I\u2019ll explain the intuition and formulas in the following. 

Given a sequence 1\u2026n, to construct a Binary Search Tree (BST) out of the sequence, we could enumerate each number i in the sequence, and use the number as the root, naturally, the subsequence 1\u2026(i-1) on its left side would lay on the left branch of the root, and similarly the right subsequence (i+1)\u2026n lay on the right branch of the root. We then can construct the subtree from the subsequence recursively. Through the above approach, we could ensure that the BST that we construct are all unique, since they have unique roots.

The problem is to calculate the number of unique BST. To do so, we need to define two functions: 

`G(n)`: the number of unique BST for a sequence of length n. 

`F(i, n), 1 <= i <= n`: the number of unique BST, where the number i is the root of BST, and the sequence ranges from 1 to n. 

As one can see, `G(n)` is the actual function we need to calculate in order to solve the problem. And `G(n)` can be derived from `F(i, n)`, which at the end, would recursively refer to `G(n)`.

First of all, given the above definitions, we can see that the total number of unique BST `G(n)`, is the sum of BST `F(i)` using each number i as a root. 
*i.e.* 

    G(n) = F(1, n) + F(2, n) + ... + F(n, n). 

Particularly, the bottom cases, there is only one combination to construct a BST out of a sequence of length 1 (only a root) or 0 (empty tree). 
*i.e.*

    G(0)=1, G(1)=1. 

Given a sequence 1\u2026n, we pick a number i out of the sequence as the root, then the number of unique BST with the specified root `F(i)`, is the cartesian product of the number of BST for its left and right subtrees. For example, `F(3, 7)`: the number of unique BST tree with number 3 as its root. To construct an unique BST out of the entire sequence [1, 2, 3, 4, 5, 6, 7] with 3 as the root, which is to say, we need to construct an unique BST out of its left subsequence [1, 2] and another BST out of the right subsequence [4, 5, 6, 7], and then combine them together (*i.e.* cartesian product). The tricky part is that we could consider the number of unique BST out of sequence [1,2] as `G(2)`, and the number of of unique BST out of sequence [4, 5, 6, 7] as `G(4)`. Therefore, `F(3,7) = G(2) * G(4)`.

*i.e.*

    F(i, n) = G(i-1) * G(n-i)	1 <= i <= n 


Combining the above two formulas, we obtain the recursive formula for `G(n)`. *i.e.*

    G(n) = G(0) * G(n-1) + G(1) * G(n-2) + \u2026 + G(n-1) * G(0) 

In terms of calculation, we need to start with the lower number, since the value of `G(n)` depends on the values of `G(0) \u2026 G(n-1)`. 

With the above explanation and formulas, here is the implementation in Java. 

    public int numTrees(int n) {
      int [] G = new int[n+1];
      G[0] = G[1] = 1;
        
      for(int i=2; i<=n; ++i) {
        for(int j=1; j<=i; ++j) {
          G[i] += G[j-1] * G[i-j];
        }
	  }
      return G[n];
    }
</p>


### Fantastic Clean Java DP Solution with Detail Explaination
- Author: mo10
- Creation Date: Tue Feb 16 2016 16:38:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 05:09:00 GMT+0800 (Singapore Standard Time)

<p>
First note that dp[k] represents the number of BST trees built from 1....k;

Then assume we have the number of the first 4 trees: dp[1] = 1 ,dp[2] =2 ,dp[3] = 5, dp[4] =14 , how do we get dp[5] based on these four numbers is the core problem here.

The essential process is: to build a tree, we need to pick a root node, then we need to know how many possible left sub trees and right sub trees can be held under that node, finally multiply them.

To build a tree contains {1,2,3,4,5}. First we pick 1 as root, for the left sub tree, there are none; for the right sub tree, we need count how many possible trees are there constructed from {2,3,4,5}, apparently it's the same number as {1,2,3,4}. So the total number of trees under "1" picked as root is dp[0] * dp[4] = 14. (assume dp[0] =1). Similarly, root 2 has dp[1]*dp[3] = 5 trees. root 3 has dp[2]*dp[2] = 4, root 4 has dp[3]*dp[1]= 5 and root  5 has dp[0]*dp[4] = 14. Finally sum the up and it's done.

Now, we may have a better understanding of the dp[k], which essentially represents the number of BST trees with k consecutive nodes. It is used as database when we need to know how many left sub trees are possible for k nodes when picking (k+1) as root. 

     public int numTrees(int n) {
        int [] dp = new int[n+1];
        dp[0]= 1;
        dp[1] = 1;
        for(int level = 2; level <=n; level++)
            for(int root = 1; root<=level; root++)
                dp[level] += dp[level-root]*dp[root-1];
        return dp[n];
    }
</p>


### Dp problem. 10+ lines with comments
- Author: leo_mao
- Creation Date: Thu Dec 04 2014 15:12:39 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 21:39:08 GMT+0800 (Singapore Standard Time)

<p>
    /**
     * Taking 1~n as root respectively:
     *      1 as root: # of trees = F(0) * F(n-1)  // F(0) == 1
     *      2 as root: # of trees = F(1) * F(n-2) 
     *      3 as root: # of trees = F(2) * F(n-3)
     *      ...
     *      n-1 as root: # of trees = F(n-2) * F(1)
     *      n as root:   # of trees = F(n-1) * F(0)
     *
     * So, the formulation is:
     *      F(n) = F(0) * F(n-1) + F(1) * F(n-2) + F(2) * F(n-3) + ... + F(n-2) * F(1) + F(n-1) * F(0)
     */

    int numTrees(int n) {
        int dp[n+1];
        dp[0] = dp[1] = 1;
        for (int i=2; i<=n; i++) {
            dp[i] = 0;
            for (int j=1; j<=i; j++) {
                dp[i] += dp[j-1] * dp[i-j];
            }
        }
        return dp[n];
    }
</p>


