---
title: "Reverse Bits"
weight: 180
#id: "reverse-bits"
---
## Description
<div class="description">
<p>Reverse bits of a given 32 bits unsigned integer.</p>

<p><strong>Note:</strong></p>

<ul>
	<li>Note that in some languages such as Java, there is no unsigned integer type. In this case, both input and output will be given as a signed integer type. They should not affect your implementation, as the integer&#39;s internal binary representation is the same, whether it is signed or unsigned.</li>
	<li>In Java,&nbsp;the compiler represents the signed integers using <a href="https://en.wikipedia.org/wiki/Two%27s_complement" target="_blank">2&#39;s complement notation</a>. Therefore, in <strong>Example 2</strong>&nbsp;above, the input represents the signed integer <code>-3</code>&nbsp;and the output represents the signed integer <code>-1073741825</code>.</li>
</ul>

<p><b>Follow up</b>:</p>

<p>If this function is called many times, how would you optimize it?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 00000010100101000001111010011100
<strong>Output:</strong>    964176192 (00111001011110000010100101000000)
<strong>Explanation: </strong>The input binary string <strong>00000010100101000001111010011100</strong> represents the unsigned integer 43261596, so return 964176192 which its binary representation is <strong>00111001011110000010100101000000</strong>.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 11111111111111111111111111111101
<strong>Output:</strong>   3221225471 (10111111111111111111111111111111)
<strong>Explanation: </strong>The input binary string <strong>11111111111111111111111111111101</strong> represents the unsigned integer 4294967293, so return 3221225471 which its binary representation is <strong>10111111111111111111111111111111</strong>.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The input must be a <strong>binary string</strong> of length <code>32</code></li>
</ul>

</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Apple - 2 (taggedByAdmin: true)
- Google - 3 (taggedByAdmin: false)
- Samsung - 2 (taggedByAdmin: false)
- Tencent - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Bit by Bit

**Intuition**

Though the question is not difficult, it often serves as a warm-up question to kick off the interview. The point is to test one's basic knowledge on data type and bit operations.

>As one of the most intuitive solutions that one could come up during an interview, one could reverse the bits **one by one**.

![pic](../Figures/190/190_mapping.png)

As easy as it sounds, the above intuition could lead to quite some variants of implementation. For instance, to retrieve the _right-most_ bit in an integer `n`, one could either apply the modulo operation (_i.e._ `n % 2`) or the bit AND operation (_i.e._ `n & 1`). Another example would be that in order to combine the results of reversed bits (_e.g._ $$2^a, 2^b$$), one could either use the addition operation (_i.e._ $$2^a + 2^b$$) or again the bit OR operation (_i.e._ $$2^a | 2^b$$).

**Algorithm**

Here we show on example of implementation based on the above intuition.

![pic](../Figures/190/190_reverse_bits.png)

>The key idea is that for a bit that is situated at the index `i`, after the reversion, its position should be `31-i` (note: the index starts from zero).

- We iterate through the bit string of the input integer, from right to left (_i.e._ `n = n >> 1`). To retrieve the right-most bit of an integer, we apply the bit AND operation (`n & 1`).
<br/>

- For each bit, we reverse it to the correct position (_i.e._ `(n & 1) << power`). Then we accumulate this reversed bit to the final result.
<br/>

- When there is no more bits of one left (_i.e._ `n == 0`), we terminate the iteration.
<br>


<iframe src="https://leetcode.com/playground/bzLogJhj/shared" frameBorder="0" width="100%" height="259" name="bzLogJhj"></iframe>


**Complexity**

- Time Complexity: $$\mathcal{O}(1)$$.  Though we have a loop in the algorithm, the number of iteration is fixed regardless the input, since the integer is of fixed-size (32-bits) in our problem.
<br/>

- Space Complexity: $$\mathcal{O}(1)$$, since the consumption of memory is constant regardless the input.
<br/>
<br/>

---
#### Approach 2: Byte by Byte with Memoization

**Intuition**

Someone might argument it might be more efficient to reverse the bits, **per byte**, which is an unit of 8 bits. Though it is not necessarily true in our case, since the input is of fixed-size 32-bit integer, it could become more advantageous when dealing with the input of long bit stream. 

![pic](../Figures/190/190_reverse_bytes.png)

Another implicit advantage of using **byte** as the unit of iteration is that we could apply the technique of **[memoization](https://leetcode.com/explore/learn/card/recursion-i/255/recursion-memoization/)**, which caches the previously calculated values to avoid the re-calculation.

The application of memoization can be considered as a response to the **follow-up** question posed in the description of the problem, which is stated as following:

>_If this function is called many times, how would you optimize it?_

To reverse bits for a byte, one could apply the same algorithm as we show in the above approach. Here we would like to show a different algorithm which is solely based on the arithmetic and bit operations without resorting to any loop statement, as following:

```python
def reverseByte(byte):
    return (byte * 0x0202020202 & 0x010884422010) % 1023
```

The algorithm is documented as ["reverse the bits in a byte with 3 operations"](http://graphics.stanford.edu/~seander/bithacks.html#ReverseByteWith64BitsDiv) on the online book called **Bit Twiddling Hacks** by Sean Eron Anderson, where one can find more details.

**Algorithm**

- We iterate over the bytes of an integer. To retrieve the right-most byte in an integer, again we apply the bit AND operation (_i.e._ `n & 0xff`) with the bit mask of `11111111`. 
<br/>

- For each byte, first we reverse the bits within the byte, via a function called `reverseByte(byte)`. Then we shift the reversed bits to their final positions.
<br/>

- With the function `reverseByte(byte)`, we apply the technique of memoization, which caches the result of the function and returns the result directly for the future invocations of the same input.

Note that, one could opt for a smaller unit rather than byte, _e.g._ a unit of 4 bits, which would require a bit more calculation in exchange of less space for cache. It goes without saying that, the technique of memoization is a trade-off between the space and the computation.

<iframe src="https://leetcode.com/playground/AmtFPJJA/shared" frameBorder="0" width="100%" height="429" name="AmtFPJJA"></iframe>


**Complexity**

- Time Complexity: $$\mathcal{O}(1)$$. Though we have a loop in the algorithm, the number of iteration is fixed regardless the input, since the integer is of fixed-size (32-bits) in our problem.
<br/>

- Space Complexity: $$\mathcal{O}(1)$$. Again, though we used a cache keep the results of reversed bytes, the total number of items in the cache is bounded to $$2^8 = 256$$.
<br/>
<br/>

---
#### Approach 3: Mask and Shift

**Intuition**

We have shown in Approach #2 an example on how to reverse the bits in a byte without resorting to the loop statement. During the interview, one might be asked to reverse the entire 32 bits without using loop. Here we propose one solution that utilizes only the bit operations.

>The idea can be considered as a strategy of **_divide and conquer_**, where we divide the original 32-bits into blocks with fewer bits via **bit masking**, then we reverse each block via **bit shifting**, and at the end we merge the result of each block to obtain the final result.

In the following graph, we demonstrate how to reverse two bits with the above-mentioned idea. As one can see, the idea could be applied to **blocks** of bits.

![pic](../Figures/190/190_mask_shift.png)

**Algorithm**

We can implement the algorithm in the following steps:

- 1). First, we break the original 32-bit into 2 blocks of 16 bits, and switch them.
<br/>

- 2). We then break the 16-bits block into 2 blocks of 8 bits. Similarly, we switch the position of the 8-bits blocks
<br/>

- 3). We then continue to break the blocks into smaller blocks, until we reach the level with the block of 1 bit.
<br>

- 4). At each of the above steps, we merge the intermediate results into a single integer which serves as the input for the next step.

The credit of this solution goes to @tworuler and @bhch3n for their [post and comment](https://leetcode.com/problems/reverse-bits/discuss/54741/O(1)-bit-operation-C%2B%2B-solution-(8ms)) in the discussion forum.

<iframe src="https://leetcode.com/playground/V57cngHF/shared" frameBorder="0" width="100%" height="242" name="V57cngHF"></iframe>


**Complexity**

- Time Complexity: $$\mathcal{O}(1)$$, no loop is used in the algorithm.
<br/>

- Space Complexity: $$\mathcal{O}(1)$$. Actually, we did not even create any new variable in the function.
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### O(1) bit operation C++ solution (8ms)
- Author: tworuler
- Creation Date: Sun Mar 08 2015 12:25:26 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 02:41:57 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        uint32_t reverseBits(uint32_t n) {
            n = (n >> 16) | (n << 16);
            n = ((n & 0xff00ff00) >> 8) | ((n & 0x00ff00ff) << 8);
            n = ((n & 0xf0f0f0f0) >> 4) | ((n & 0x0f0f0f0f) << 4);
            n = ((n & 0xcccccccc) >> 2) | ((n & 0x33333333) << 2);
            n = ((n & 0xaaaaaaaa) >> 1) | ((n & 0x55555555) << 1);
            return n;
        }
    };

for 8 bit binary number *abcdefgh*, the process is as follow:

*abcdefgh -> efghabcd -> ghefcdab -> hgfedcba*
</p>


### Sharing my 2ms Java Solution with Explanation
- Author: Amadou
- Creation Date: Thu Apr 14 2016 10:27:43 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 06:53:29 GMT+0800 (Singapore Standard Time)

<p>

"
We first intitialize result to 0. We then iterate from
0 to 31 (an integer has 32 bits).  In each iteration:  
  We first shift result to the left by 1 bit.
  Then, if the last digit of input n is 1, we add 1 to result. To
  find the last digit of n, we just do: (n & 1)
    Example, if n=5 (101), n&1 = 101 & 001 = 001 = 1;
    however, if n = 2 (10), n&1 = 10 & 01 = 00 = 0).

  Finally, we update n by shifting it to the right by 1 (n >>= 1). This is because the last digit is already taken care of, so we need to drop it by shifting n to the right by 1.

At the end of the iteration, we return result.

Example, if input n = 13 (represented in binary as
0000_0000_0000_0000_0000_0000_0000_1101, the "_" is for readability),
calling reverseBits(13) should return:
1011_0000_0000_0000_0000_0000_0000_0000

Here is how our algorithm would work for input n = 13:

Initially, result = 0 = 0000_0000_0000_0000_0000_0000_0000_0000,
n = 13 = 0000_0000_0000_0000_0000_0000_0000_1101

Starting for loop:
  i = 0:
    result = result << 1 = 0000_0000_0000_0000_0000_0000_0000_0000.
    n&1 = 0000_0000_0000_0000_0000_0000_0000_1101 
           & 0000_0000_0000_0000_0000_0000_0000_0001 
           = 0000_0000_0000_0000_0000_0000_0000_0001 = 1
          therefore result = result + 1 =
          0000_0000_0000_0000_0000_0000_0000_0000 
       + 0000_0000_0000_0000_0000_0000_0000_0001 
       = 0000_0000_0000_0000_0000_0000_0000_0001 = 1
    
Next, we right shift n by 1 (n >>= 1) (i.e. we drop the least significant bit) to get:
    n = 0000_0000_0000_0000_0000_0000_0000_0110.
    We then go to the next iteration.

  i = 1:
    result = result << 1 = 0000_0000_0000_0000_0000_0000_0000_0010;
    n&1 = 0000_0000_0000_0000_0000_0000_0000_0110 &
          0000_0000_0000_0000_0000_0000_0000_0001
        = 0000_0000_0000_0000_0000_0000_0000_0000 = 0;
    therefore we don't increment result.
    We right shift n by 1 (n >>= 1) to get:
    n = 0000_0000_0000_0000_0000_0000_0000_0011.
    We then go to the next iteration.

  i = 2:
    result = result << 1 = 0000_0000_0000_0000_0000_0000_0000_0100.
    n&1 = 0000_0000_0000_0000_0000_0000_0000_0011 &
          0000_0000_0000_0000_0000_0000_0000_0001 =
          0000_0000_0000_0000_0000_0000_0000_0001 = 1
          therefore result = result + 1 =
          0000_0000_0000_0000_0000_0000_0000_0100 +
          0000_0000_0000_0000_0000_0000_0000_0001 =
          result = 0000_0000_0000_0000_0000_0000_0000_0101
    We right shift n by 1 to get:
    n = 0000_0000_0000_0000_0000_0000_0000_0001.
    We then go to the next iteration.

  i = 3:
    result = result << 1 = 0000_0000_0000_0000_0000_0000_0000_1010. 
    n&1 = 0000_0000_0000_0000_0000_0000_0000_0001 &
              0000_0000_0000_0000_0000_0000_0000_0001 =
              0000_0000_0000_0000_0000_0000_0000_0001 = 1
          therefore result = result + 1 =
                           = 0000_0000_0000_0000_0000_0000_0000_1011
    We right shift n by 1 to get:
    n = 0000_0000_0000_0000_0000_0000_0000_0000 = 0.

  Now, from here to the end of the iteration, n is 0, so (n&1)
  will always be 0 and and n >>=1 will not change n. The only change
  will be for result <<=1, i.e. shifting result to the left by 1 digit.
  Since there we have i=4 to i = 31 iterations left, this will result
  in padding 28 0's to the right of result. i.e at the end, we get
  result = 1011_0000_0000_0000_0000_0000_0000_0000

  This is exactly what we expected to get
"

    
    public int reverseBits(int n) {
        if (n == 0) return 0;
        
        int result = 0;
        for (int i = 0; i < 32; i++) {
            result <<= 1;
            if ((n & 1) == 1) result++;
            n >>= 1;
        }
        return result;
    }
</p>


### Java Solution and Optimization
- Author: AlexTheGreat
- Creation Date: Sat Mar 07 2015 20:43:22 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 04:16:32 GMT+0800 (Singapore Standard Time)

<p>
The Java solution is straightforward, just bitwise operation:

    public int reverseBits(int n) {
        int result = 0;
        for (int i = 0; i < 32; i++) {
            result += n & 1;
            n >>>= 1;   // CATCH: must do unsigned shift
            if (i < 31) // CATCH: for last digit, don't shift!
                result <<= 1;
        }
        return result;
    }

How to optimize if this function is called multiple times? We can divide an int into 4 bytes, and reverse each byte then combine into an int. For each byte, we can use cache to improve performance.

    // cache
    private final Map<Byte, Integer> cache = new HashMap<Byte, Integer>();
    public int reverseBits(int n) {
        byte[] bytes = new byte[4];
        for (int i = 0; i < 4; i++) // convert int into 4 bytes
            bytes[i] = (byte)((n >>> 8*i) & 0xFF);
        int result = 0;
        for (int i = 0; i < 4; i++) {
            result += reverseByte(bytes[i]); // reverse per byte
            if (i < 3)
                result <<= 8;
        }
        return result;
    }
    
    private int reverseByte(byte b) {
        Integer value = cache.get(b); // first look up from cache
        if (value != null)
            return value;
        value = 0;
        // reverse by bit
        for (int i = 0; i < 8; i++) {
            value += ((b >>> i) & 1);
            if (i < 7)
                value <<= 1;
        }
        cache.put(b, value);
        return value;
    }
</p>


