---
title: "Word Frequency"
weight: 1600
#id: "word-frequency"
---
## Description
<div class="description">
<p>Write a bash script to calculate the frequency of each word in a text file <code>words.txt</code>.</p>

<p>For simplicity sake, you may assume:</p>

<ul>
	<li><code>words.txt</code> contains only lowercase characters and space <code>&#39; &#39;</code> characters.</li>
	<li>Each word must consist of lowercase characters only.</li>
	<li>Words are separated by one or more whitespace characters.</li>
</ul>

<p><strong>Example:</strong></p>

<p>Assume that <code>words.txt</code> has the following content:</p>

<pre>
the day is sunny the the
the sunny is is
</pre>

<p>Your script should output the following, sorted by descending frequency:</p>

<pre>
the 4
is 3
sunny 2
day 1
</pre>

<p><b>Note:</b></p>

<ul>
	<li>Don&#39;t worry about handling ties, it is guaranteed that each word&#39;s frequency count is unique.</li>
	<li>Could you write it in one-line using <a href="http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO-4.html">Unix pipes</a>?</li>
</ul>

</div>

## Tags


## Companies
- Amazon - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Yelp - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My simple solution (one line with pipe)
- Author: Swordmctster
- Creation Date: Mon Mar 23 2015 03:49:34 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 20:36:37 GMT+0800 (Singapore Standard Time)

<p>
```
cat words.txt | tr -s ' ' '\
' | sort | uniq -c | sort -r | awk '{ print $2, $1 }'
```

**tr -s**: truncate the string with target string, but only remaining one instance (e.g. multiple whitespaces)

**sort**: To make the same string successive so that `uniq` could count the same string fully and correctly.

**uniq -c**: uniq is used to filter out the repeated lines which are successive, -c means counting

**sort -r**: -r means sorting in descending order

**awk '{ print $2, $1 }'**: To format the output, see [here][1].


  [1]: http://linux.cn/article-3945-1.html
</p>


### Solution using awk and pipes with explaination
- Author: illuz
- Creation Date: Sun Mar 22 2015 18:32:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2015 18:32:12 GMT+0800 (Singapore Standard Time)

<p>

1. I should count the words. So I chose the `awk` command.
  - I use a dictionary in `awk`. For every line I count every word in the dictionary.
  - After deal with all lines. At the `END`, use `for (item in Dict) { #do someting# } ` to print every words and its frequency.
2. Now the printed words are unsorted. Then I use a `|` pipes and sort it by `sort`
  - `sort -n` means "compare according to string numerical value".
  - `sort -r` means "reverse the result of comparisons".
  - `sort -k 2` means "sort by the second word"

---


    awk '\
    { for (i=1; i<=NF; i++) { ++D[$i]; } }\
    END { for (i in D) { print i, D[i] } }\
    ' words.txt | sort -nr -k 2

---

Are there any other solutions without `awk`?  
Such as using `sed` or `grep`.
</p>


### Extremely simple solution (with detailed explanation)!
- Author: somethinganonymous
- Creation Date: Tue Jun 23 2020 23:59:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 22 2020 22:00:27 GMT+0800 (Singapore Standard Time)

<p>
`cat words.txt | xargs printf "%s\
" | sort | uniq -c | sort -nr | awk \'{print $2,$1}\'`

**cat** - Displays the contents of the file
**xargs** - Applies the printf function to every line in the output of cat
**sort** - Sorts the current buffer piped to it
**uniq -c** - Displays the counts of each line in the buffer
**sort -nr** - Reverse sorts the output via the counts in the previous operation
**awk** - Prints the 2nd column and then the 1st column based on the problem requirement.

Please upvote if you like the solution!
</p>


