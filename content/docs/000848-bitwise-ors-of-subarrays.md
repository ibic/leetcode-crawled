---
title: "Bitwise ORs of Subarrays"
weight: 848
#id: "bitwise-ors-of-subarrays"
---
## Description
<div class="description">
<p>We have an array <code>A</code> of non-negative integers.</p>

<p>For every (contiguous) subarray <code>B =&nbsp;[A[i], A[i+1], ..., A[j]]</code> (with <code>i &lt;= j</code>), we take the bitwise OR of all the elements in <code>B</code>, obtaining a result <font face="monospace"><code>A[i] | A[i+1] | ... | A[j]</code>.</font></p>

<p>Return the number of possible&nbsp;results.&nbsp; (Results that occur more than once are only counted once in the final answer.)</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[0]</span>
<strong>Output: </strong><span id="example-output-1">1</span>
<strong>Explanation: </strong>
There is only one possible result: 0.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[1,1,2]</span>
<strong>Output: </strong><span id="example-output-2">3</span>
<strong>Explanation: </strong>
The possible subarrays are [1], [1], [2], [1, 1], [1, 2], [1, 1, 2].
These yield the results 1, 1, 2, 1, 3, 3.
There are 3 unique values, so the answer is 3.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[1,2,4]</span>
<strong>Output: </strong><span id="example-output-3">6</span>
<strong>Explanation: </strong>
The possible results are 1, 2, 3, 4, 6, and 7.
</pre>
</div>
</div>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 50000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 10^9</code></li>
</ol>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Bit Manipulation (bit-manipulation)

## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach 1: Frontier Set

**Intuition**

Let's try to speed up a brute force answer.  Evidently, the brute force approach is to calculate every result `result(i, j) = A[i] | A[i+1] | ... | A[j]`.  We can speed this up by taking note of the fact that `result(i, j+1) = result(i, j) | A[j+1]`.  Naively, this approach has time complexity $$O(N^2)$$, where $$N$$ is the length of the array.

Actually, this approach can be better than that.  At the `k`th step, say we have all the `result(i, k)` in some set `cur`.  Then we can find the next `cur` set (for `k -> k+1`) by using `result(i, k+1) = result(i, k) | A[k+1]`.

However, the number of unique values in this set `cur` is at most 32, since the list `result(k, k), result(k-1, k), result(k-2, k), ...` is monotone increasing, and any subsequent values that are different must have more 1s in it's binary representation (to a maximum of 32 ones).

**Algorithm**

In the `k`th step, we'll maintain `cur`: the set of results `A[i] | ... | A[k]` for all `i`.  These results will be included in our final answer set.

<iframe src="https://leetcode.com/playground/WxU3dvHe/shared" frameBorder="0" width="100%" height="344" name="WxU3dvHe"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log W)$$, where $$N$$ is the length of `A`, and $$W$$ is the maximum size of elements in `A`.

* Space Complexity:  $$O(N \log W)$$, the size of the answer.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] O(30N)
- Author: lee215
- Creation Date: Sun Sep 02 2018 11:09:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 09 2020 15:51:58 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**:
Assume `B[i][j] = A[i] | A[i+1] | ... | A[j]`
Hash set `cur` stores all wise `B[0][i]`, `B[1][i]`, `B[2][i]`, `B[i][i]`.

When we handle the `A[i+1]`, we want to update `cur`
So we need operate bitwise OR on all elements in `cur`.
Also we need to add `A[i+1]` to `cur`.

In each turn, we add all elements in `cur` to `res`.
<br>

# **Complexity**:
Time `O(30N)`

Normally this part is easy.
But for this problem, time complexity matters a lot.

The solution is straight forward,
while you may worry about the time complexity up to `O(N^2)`
However, it\'s not the fact.
This solution has only `O(30N)`

The reason is that, `B[0][i] >= B[1][i] >= ... >= B[i][i]`.
`B[0][i] covers all bits of B[1][i]`
`B[1][i] covers all bits of B[2][i]`
....

There are at most 30 bits for a positive number `0 <= A[i] <= 10^9`.
So there are at most 30 different values for `B[0][i]`, `B[1][i]`, `B[2][i]`, ..., `B[i][i]`.
Finally `cur.size() <= 30` and `res.size() <= 30 * A.length()`

In a worst case, `A = {1,2,4,8,16,..., 2 ^ 29}`
And all `B[i][j]` are different and `res.size() == 30 * A.length()`
<br>

# Solution 1: Use HashSet 
**C++:**
new test cases added to this problem,
this cpp solution may TLE.
(It doesn\'t make any sense to me).
`unordered_set` is actually very slow when size is big.
Refer to solution 2 for optimisation.
```cpp
    int subarrayBitwiseORs(vector<int> A) {
        unordered_set<int> res, cur, cur2;
        for (int i: A) {
            cur2 = {i};
            for (int j: cur) cur2.insert(i|j);
            for (int j: cur = cur2) res.insert(j);
        }
        return res.size();
    }
```

**Java:**
```java
    public int subarrayBitwiseORs(int[] A) {
        Set<Integer> res = new HashSet<>(), cur = new HashSet<>(), cur2;
        for (Integer i: A) {
            cur2 = new HashSet<>();
            cur2.add(i);
            for (Integer j: cur) cur2.add(i|j);
            res.addAll(cur = cur2);
        }
        return res.size();
    }
```
**Python:**
```py
    def subarrayBitwiseORs(self, A):
        res, cur = set(), set()
        for i in A:
            cur = {i | j for j in cur} | {i}
            res |= cur
        return len(res)
```
<br>

# Solution2: Use Array List
The elements between `res[left]` and `res[right]` are same as the `cur` in solution 1.
**C++**
```cpp
    int subarrayBitwiseORs(vector<int> A) {
        vector<int> res;
        int left = 0, right;
        for (int a: A) {
            right = res.size();
            res.push_back(a);
            for (int i = left; i < right; ++i) {
                if (res.back() != (res[i] | a)) {
                    res.push_back(res[i] | a);
                }
            }
            left = right;
        }
        return unordered_set(res.begin(), res.end()).size();
    }
```

</p>


### C++ O(kN) solution
- Author: zhoubowei
- Creation Date: Sun Sep 02 2018 11:02:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 10:40:20 GMT+0800 (Singapore Standard Time)

<p>


For example, an array is [001, 011, 100, 110, 101] (in binary).

All the subarrays are:

```
[001]
[001 011] [011]
[001 011 100] [011 100] [100]
[001 011 100 110] [011 100 110] [100 110] [110]
[001 011 100 110 101] [011 100 110 101] [100 110 101] [110 101] [101]
```

In each line, we add a new number to each array of the previous line. 

Calculate the OR operations for each subarray:

```
001
011 011
111 111 100
111 111 110 110
111 111 111 111 101
```

There are O(N^2) numbers in total, which is not acceptable.

After removing duplicate numbers in each line, it becomes:

```
001
011
111 100
111 110
111 101
```

In each line `t`, for any two numbers `t[i]` and `t[j]` (i < j), `t[i]` must have more `1`s than `t[j]`. So the length of each line will not exceed 32. So if we remove duplicate numbers, the complexity would not be very large.



The complexity is O(kN), where k is a constant depends on the bitwise length of input numbers. (32 in this problem)

Here is my code:

```cpp
class Solution {
public:
    int subarrayBitwiseORs(vector<int>& A) {
        unordered_set<int> s;
        set<int> t;
        for (int i : A) {
            set<int> r;
            r.insert(i);
            for (int j : t) r.insert(i | j);
            t = r;
            for (int j : t) s.insert(j);
        }
        return s.size();
    }
};
```


</p>


### [Python] Dynamic programming solution with indepth explanation of intuition.
- Author: Hai_dee
- Creation Date: Sun Sep 02 2018 12:10:23 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 14:46:35 GMT+0800 (Singapore Standard Time)

<p>
There are **O(n\xB2)** possible sets. A brute force algorithm that checks each of these sets would be **O(n\xB3)**, or **O(n\xB2)** with a few optimisations. However, this isn\'t fast enough. The question is then... how can we do less doubling up of work? 

One way is to go through the array once, attempting to OR the current number with all subsets that included the number before it. The number of subsets in the previous number isn\'t **n**, it\'s actually never going to be bigger than the number of bits in the largest numbers of the array. This is because each step we go back from the previous number could add at least one more bit in, or none at all. It\'s impossible to subtract them.

For example, let\'s say we have the following (5 bit) numbers:
```[4, 5, 10, 2, 16, 4, 5, 2, 1, 3]```
In binary, these are:
```[00100, 00101, 01010, 00010, 10000, 00100, 00101, 00010, 00001, 00011].```

The last number in the list is ```00011```. Let\'s look at what happens when we make increasingly larger sublists with the numbers before it and get the result of OR\'ing all those numbers.

```
[00011] ==> 00011
[00001, 00011] ==>  00011
[00010, 00001, 00011]  ==>  00011
[00101, 00010, 00001, 00011] ==>  00111
[00100, 00101, 00010, 00001, 00011] ==>  00111
[10000, 00100, 00101, 00010, 00001, 00011] ==> 10111
[00010, 10000, 00100, 00101, 00010, 00001, 00011] ==> 10111
[01010, 00010, 10000, 00100, 00101, 00010, 00001, 00011] ==> 11111
[00101, 01010, 00010, 10000, 00100, 00101, 00010, 00001, 00011] ==> 11111
[00100, 00101, 01010, 00010, 10000, 00100, 00101, 00010, 00001, 00011] ==> 11111
```

Notice how a lot of these are the same? Once a bit appears in a particular position, it\'s impossible for it to disappear! What this means, is that we only need to OR the last number with previous ones that are actually unique. This is proportional to the number of bits in the largest number rather than n. That is, it\'s a lot smaller!

So, for each number in the array, we start with a set containing just that number (because that\'s what we\'d get OR\'ing it with itself). We then add all the possibilities we can get by OR\'ing the number with results that include the previous number.

And here\'s an algorithm that uses that idea.

```py
class Solution:
    def subarrayBitwiseORs(self, A):
        # Tabulation is a list of sets, one for each number in A. 
        # Each set, at position i, is initialised to containing the element at A[i]
        tabulation = [set([A[i]]) for i in range(len(A))]
        
        # And now we need to go through, updating the sets based on the previous set.
        for i in range(1, len(A)):
            for previous_result in tabulation[i - 1]: 
                tabulation[i].add(A[i] | previous_result)  
        
        # Return the number of unique numbers in the tabulation list.
        return len(set.union(*tabulation)) if len(A) > 0 else 0
```
</p>


