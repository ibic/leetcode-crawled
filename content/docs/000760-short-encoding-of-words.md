---
title: "Short Encoding of Words"
weight: 760
#id: "short-encoding-of-words"
---
## Description
<div class="description">
<p>Given a list of words, we may encode it by writing a reference string <code>S</code> and a list of indexes <code>A</code>.</p>

<p>For example, if the list of words is <code>[&quot;time&quot;, &quot;me&quot;, &quot;bell&quot;]</code>, we can write it as <code>S = &quot;time#bell#&quot;</code>&nbsp;and <code>indexes = [0, 2, 5]</code>.</p>

<p>Then for each index, we will recover the word by reading from the reference string from that index until we reach a <code>&quot;#&quot;</code> character.</p>

<p>What is the length of the shortest reference string S possible that encodes the given words?</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> words = <code>[&quot;time&quot;, &quot;me&quot;, &quot;bell&quot;]</code>
<strong>Output:</strong> 10
<strong>Explanation:</strong> S = <code>&quot;time#bell#&quot; and indexes = [0, 2, 5</code>].
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= words.length&nbsp;&lt;= 2000</code>.</li>
	<li><code>1 &lt;=&nbsp;words[i].length&nbsp;&lt;= 7</code>.</li>
	<li>Each word&nbsp;has only&nbsp;lowercase letters.</li>
</ol>

</div>

## Tags


## Companies


## Official Solution
[TOC]

---
#### Approach #1: Store Prefixes [Accepted]

**Intuition**

If a word `X` is a suffix of `Y`, then it does not need to be considered, as the encoding of `Y` in the reference string will also encode `X`.  For example, if `"me"` and `"time"` is in `words`, we can throw out `"me"` without changing the answer.

If a word `Y` does not have any other word `X` (in the list of `words`) that is a suffix of `Y`, then `Y` must be part of the reference string.

Thus, the goal is to remove words from the list such that no word is a suffix of another.  The final answer would be `sum(word.length + 1 for word in words)`.

**Algorithm**

Since a word only has up to 7 suffixes (as `words[i].length <= 7`), let's iterate over all of them.  For each suffix, we'll try to remove it from our `words` list.  For efficiency, we'll make `words` a set.

<iframe src="https://leetcode.com/playground/cpeEtpcc/shared" frameBorder="0" width="100%" height="293" name="cpeEtpcc"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\sum w_i^2)$$, where $$w_i$$ is the length of `words[i]`.

* Space Complexity: $$O(\sum w_i)$$, the space used in storing suffixes.

---
#### Approach #2: Trie [Accepted]

**Intuition**

As in *Approach #1*, the goal is to remove words that are suffixes of another word in the list.

**Algorithm**

To find whether different words have the same suffix, let's put them backwards into a trie (prefix tree).  For example, if we have `"time"` and `"me"`, we will put `"emit"` and `"em"` into our trie.

After, the leaves of this trie (nodes with no children) represent words that have no suffix, and we will count `sum(word.length + 1 for word in words)`.

<iframe src="https://leetcode.com/playground/dTqKWkUd/shared" frameBorder="0" width="100%" height="500" name="dTqKWkUd"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\sum w_i)$$, where $$w_i$$ is the length of `words[i]`.

* Space Complexity: $$O(\sum w_i)$$, the space used by the trie.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Easy Understood Solution with Explanation
- Author: lee215
- Creation Date: Sun Apr 22 2018 11:09:12 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 13:31:01 GMT+0800 (Singapore Standard Time)

<p>
**Deacription analyse**
Let\u2019s get the deacription clear:
Given string list L1, construct a another string list L2, making every word in L1 be a suffix of a word in L2.
Return the minimum possible total length of words in L2
Input L1: [\u201Ctime\u201D,\u201Cme\u201D,\u201Cbell\u201D]
L2: [\u201Ctime\u201D,\u201Cbell\u201D]

**Explanation of solution**
Base on @awice\'s idea. This solution is not my intuition but it is really simple to write, compared with Trie solution.

1. Build a set of words.
2. Iterate on all words and remove all suffixes of every word from the set.
3. Finally the set will the set of all encoding words.
4. Iterate on the set and return `sum(word\'s length + 1 for every word in the set)`

**Complexity**
 `O(NK^2)` for time and \'O(NK)\' for space.
It is really kind of `K` with `K <= 7`, almost ignorable.
I should have suggested for bigger \'K\' cases.
I believe it will take more time for most people to solve this problem if we have a big `K`.


**C++:**
```
    int minimumLengthEncoding(vector<string>& words) {
        unordered_set<string> s(words.begin(), words.end());
        for (string w : s)
            for (int i = 1; i < w.size(); ++i)
                s.erase(w.substr(i));
        int res = 0;
        for (string w : s) res += w.size() + 1;
        return res;
    }
```

**Java:**
```
    public int minimumLengthEncoding(String[] words) {
        Set<String> s = new HashSet<>(Arrays.asList(words));
        for (String w : words)
            for (int i = 1; i < w.length(); ++i)
                s.remove(w.substring(i));
        int res = 0;
        for (String w : s) res += w.length() + 1;
        return res;
    }

```
**Python:**
```
    def minimumLengthEncoding(self, words):
        s = set(words)
        for w in words:
            for i in range(1, len(w)):
                s.discard(w[i:])
        return sum(len(w) + 1 for w in s)

```
</p>


### Trie Solution
- Author: lee215
- Creation Date: Sun Apr 22 2018 11:01:37 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 28 2018 13:20:36 GMT+0800 (Singapore Standard Time)

<p>
Let\'s get the deacription clear:  
Given string list L1, construct a another string list L2, making every word in L1 be a suffix of a word in L2. 
Return the minimum possible total length of words in L2
Input L1: ["time","me","bell"]
L2: ["time","bell"]

So the idea of trie is really straightforward. It will be even more obvious if it is sufix.
What we should do here is insert all reversed words to a trie.
Here are three steps:

**1. Define a trie.**

**C++:**
```
class TrieNode {
public:
    unordered_map<char, TrieNode *> next;
};

```

**Java:**
```
class TrieNode {
    HashMap<Character, TrieNode> next = new HashMap<>();
    int depth;
}
```

For python we can use `defaultdict` to realize a trie, however I just used `dict`.
Then we initialize a trie root.
We can take the root node as the stop character `#` and it has depth 1.


**2. Insert all reversed words to the trie.**
For example, for word "time", we insert \'e\', \'m\', \'i\', \'t\' successively.

**3. Return the sum of depth of all leaves.**
One way is to write a simple recursive function to do it.
Here I save all last nodes of words and its depth to a list and all leaves should be in this list.
I iterate this list and all nodes without children are leaved.


**C++:**
```
class Solution {
public:
    int minimumLengthEncoding(vector<string>& words) {
        TrieNode *root = new TrieNode;
        vector<pair<TrieNode *, int>> leaves;
        for (auto & w : unordered_set<string> (words.begin(), words.end())) {
            TrieNode *cur = root;
            for (int i = w.length() - 1; i >= 0; --i) {
                if (cur->next.count(w[i]) == 0) cur->next[w[i]] = new TrieNode;
                cur = cur->next[w[i]];
            }
            leaves.push_back(make_pair(cur, w.length() + 1));
        }
        int res = 0;
        for (auto leaf : leaves) if ((leaf.first->next).size() == 0) res += leaf.second;
        return res;
    }
};
```

**Java:**
```
class Solution {
    public int minimumLengthEncoding(String[] words) {
        TrieNode root = new TrieNode();
        List<TrieNode> leaves = new  ArrayList<TrieNode>();
        for (String w : new HashSet<>(Arrays.asList(words))) {
            TrieNode cur = root;
            for (int i = w.length() - 1; i >= 0; --i) {
                char j = w.charAt(i);
                if (!cur.next.containsKey(j)) cur.next.put(j, new TrieNode());
                cur = cur.next.get(j);
            }
            cur.depth = w.length() + 1;
            leaves.add(cur);
        }
        int res = 0;
        for (TrieNode leaf : leaves) if (leaf.next.isEmpty()) res += leaf.depth;
        return res;
    }
}

```
**Python:**
```
    def minimumLengthEncoding(self, words):
        root = dict()
        leaves = []
        for word in set(words):
            cur = root
            for i in word[::-1]:
                cur[i] = cur = cur.get(i, dict())
            leaves.append((cur, len(word) + 1))
        return sum(depth for node, depth in leaves if len(node) == 0)

```

</p>


### The problem description should be improved
- Author: accessw
- Creation Date: Mon Apr 23 2018 03:11:09 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 23 2018 03:11:09 GMT+0800 (Singapore Standard Time)

<p>
Came up with a solution that failed one case. After studying the solution, I realized that you do not need to keep the order of the words. Please add necessary clarifications!
</p>


