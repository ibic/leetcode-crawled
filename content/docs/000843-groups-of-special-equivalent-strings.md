---
title: "Groups of Special-Equivalent Strings"
weight: 843
#id: "groups-of-special-equivalent-strings"
---
## Description
<div class="description">
<p>You are given an array <code>A</code> of strings.</p>

<p>A <em>move&nbsp;onto <code>S</code></em> consists of swapping any two even indexed characters of <code>S</code>, or any two odd indexed characters of <code>S</code>.</p>

<p>Two strings <code>S</code> and <code>T</code> are&nbsp;<em>special-equivalent</em>&nbsp;if after any number of <em>moves onto <code>S</code></em>, <code>S == T</code>.</p>

<p>For example, <code>S = &quot;zzxy&quot;</code> and <code>T = &quot;xyzz&quot;</code> are special-equivalent because we may make the moves <code>&quot;zzxy&quot; -&gt; &quot;xzzy&quot; -&gt; &quot;xyzz&quot;</code>&nbsp;that swap <code>S[0]</code> and <code>S[2]</code>, then <code>S[1]</code> and <code>S[3]</code>.</p>

<p>Now, a <em>group of special-equivalent strings from <code>A</code></em>&nbsp;is a non-empty subset of&nbsp;A such that:</p>

<ol>
	<li>Every pair of strings in the group are special equivalent, and;</li>
	<li>The group is the largest size possible (ie., there isn&#39;t a string S not in the group such that S is special equivalent to every string in the group)</li>
</ol>

<p>Return the number of groups of special-equivalent strings from <code>A</code>.</p>

<div>&nbsp;</div>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;abcd&quot;,&quot;cdab&quot;,&quot;cbad&quot;,&quot;xyzz&quot;,&quot;zzxy&quot;,&quot;zzyx&quot;]</span>
<strong>Output: </strong><span id="example-output-1">3</span>
<strong>Explanation: </strong>
One group is [&quot;abcd&quot;, &quot;cdab&quot;, &quot;cbad&quot;], since they are all pairwise special equivalent, and none of the other strings are all pairwise special equivalent to these.

The other two groups are [&quot;xyzz&quot;, &quot;zzxy&quot;] and [&quot;zzyx&quot;].  Note that in particular, &quot;zzxy&quot; is not special equivalent to &quot;zzyx&quot;.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[&quot;abc&quot;,&quot;acb&quot;,&quot;bac&quot;,&quot;bca&quot;,&quot;cab&quot;,&quot;cba&quot;]</span>
<strong>Output: </strong><span id="example-output-2">3</span></pre>

<p>&nbsp;</p>
</div>
</div>

<div>
<div>
<div>
<div>
<p><strong>Note:</strong></p>

<ul>
	<li><code>1 &lt;= A.length &lt;= 1000</code></li>
	<li><code>1 &lt;= A[i].length &lt;= 20</code></li>
	<li>All <code>A[i]</code> have the same length.</li>
	<li>All <code>A[i]</code> consist of only lowercase letters.</li>
</ul>
</div>
</div>
</div>
</div>

</div>

## Tags
- String (string)

## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Counting

**Intuition and Algorithm**

Let's try to characterize a special-equivalent string $$S$$, by finding a function $$\mathcal{C}$$ so that $$S \equiv T \iff \mathcal{C}(S) = \mathcal{C}(T)$$.

Through swapping, we can permute the even indexed letters, and the odd indexed letters.  What characterizes these permutations is the count of the letters: all such permutations have the same count, and different counts have different permutations.

Thus, the function $$\mathcal{C}(S) =$$ (the count of the even indexed letters in S, followed by the count of the odd indexed letters in S) successfully characterizes the equivalence relation.

Afterwards, we count the number of unique $$\mathcal{C}(S)$$ for $$S \in A$$.

<iframe src="https://leetcode.com/playground/MELaBnuZ/shared" frameBorder="0" width="100%" height="259" name="MELaBnuZ"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\sum\limits_{i} (A_i)\text{.length})$$

* Space Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### No Idea Whats Being Asked
- Author: sjw214
- Creation Date: Thu Nov 29 2018 09:58:10 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Nov 29 2018 09:58:10 GMT+0800 (Singapore Standard Time)

<p>
I\'m sure this question is probably easy once you know what the hell is being asked, but after reading the description several times, I\'m giving up on trying to make sense of what is even being accomplished here.
</p>


### Java Concise Set Solution
- Author: caraxin
- Creation Date: Sun Aug 26 2018 11:01:07 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 21:36:01 GMT+0800 (Singapore Standard Time)

<p>
For each String, we generate it\'s corresponding signature, and add it to the set.
In the end, we return the size of the set.
```
class Solution {
    public int numSpecialEquivGroups(String[] A) {
        Set<String> set= new HashSet<>();
        for (String s: A){
            int[] odd= new int[26];
            int[] even= new int[26];
            for (int i=0; i<s.length(); i++){
                if (i%2==1) odd[s.charAt(i)-\'a\']++;
                else even[s.charAt(i)-\'a\']++;
            }
            String sig= Arrays.toString(odd)+Arrays.toString(even);
            set.add(sig);
        }
        return set.size();
    }
}
```
</p>


### Python 1-liner
- Author: cenkay
- Creation Date: Sun Aug 26 2018 15:38:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 23 2018 12:15:55 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def numSpecialEquivGroups(self, A):
        return len(set("".join(sorted(s[0::2])) + "".join(sorted(s[1::2])) for s in A))
```
</p>


