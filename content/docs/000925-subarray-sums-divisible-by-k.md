---
title: "Subarray Sums Divisible by K"
weight: 925
#id: "subarray-sums-divisible-by-k"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of integers, return the number of (contiguous, non-empty) subarrays that have a sum divisible by <code>K</code>.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[4,5,0,-2,-3,1]</span>, K = <span id="example-input-1-2">5</span>
<strong>Output: </strong><span id="example-output-1">7</span>
<strong>Explanation: </strong>There are 7 subarrays with a sum divisible by K = 5:
[4, 5, 0, -2, -3, 1], [5], [5, 0], [5, 0, -2, -3], [0], [0, -2, -3], [-2, -3]
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 30000</code></li>
	<li><code>-10000 &lt;= A[i] &lt;= 10000</code></li>
	<li><code>2 &lt;= K &lt;= 10000</code></li>
</ol>
</div>
</div>

## Tags
- Array (array)
- Hash Table (hash-table)

## Companies
- Twilio - 14 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Cloudera - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Prefix Sums and Counting

**Intuition**

As is typical with problems involving subarrays, we use prefix sums to add each subarray.  Let `P[i+1] = A[0] + A[1] + ... + A[i]`.  Then, each subarray can be written as `P[j] - P[i]` (for `j > i`).  Thus, we have `P[j] - P[i]` equal to `0` modulo `K`, or equivalently `P[i]` and `P[j]` are the same value modulo `K`.

**Algorithm**

Count all the `P[i]`'s modulo `K`.  Let's say there are $$C_x$$ values $$P[i] \equiv x \pmod{K}$$.  Then, there are $$\sum_x \binom{C_x}{2}$$ possible subarrays.

For example, take `A = [4,5,0,-2,-3,1]` and `K = 5`.  Then `P = [0,4,9,9,7,4,5]`, and $$C_0 = 2, C_2 = 1, C_4 = 4$$:

* With $$C_0 = 2$$ (at $$P[0]$$, $$P[6]$$), it indicates $$\binom{2}{2} = 1$$ subarray with sum divisible by $$K$$, namely $$A[0:6] = [4, 5, 0, -2, -3, 1]$$.
* With $$C_4 = 4$$ (at $$P[1]$$, $$P[2]$$, $$P[3]$$, $$P[5]$$), it indicates $$\binom{4}{2} = 6$$ subarrays with sum divisible by $$K$$, namely $$A[1:2]$$, $$A[1:3]$$, $$A[1:5]$$, $$A[2:3]$$, $$A[2:5]$$, $$A[3:5]$$.

<iframe src="https://leetcode.com/playground/KscHVJwf/shared" frameBorder="0" width="100%" height="344" name="KscHVJwf"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$.  (However, the solution can be modified to use $$O(K)$$ space by storing only `count`.)
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(N) with HashMap and prefix Sum
- Author: CanDong
- Creation Date: Sun Jan 13 2019 12:05:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 13 2019 12:05:20 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int subarraysDivByK(int[] A, int K) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, 1);
        int count = 0, sum = 0;
        for(int a : A) {
            sum = (sum + a) % K;
            if(sum < 0) sum += K;  // Because -1 % 5 = -1, but we need the positive mod 4
            count += map.getOrDefault(sum, 0);
            map.put(sum, map.getOrDefault(sum, 0) + 1);
        }
        return count;
    }
}
```

About the problems - sum of contiguous subarray , prefix sum is a common technique. 
Another thing is if sum[0, i] % K == sum[0, j] % K, sum[i + 1, j] is divisible by by K.
So for current index j, we need to find out how many index i (i < j) exit that has the same mod of K.
Now it easy to come up with HashMap <mod, frequency>

Time Complexity: O(N)
Space Complexity: O(K)

------------------------------------
As @davidluoyes Comments, we can just use array, which is faster than HashMap.
```
class Solution {
    public int subarraysDivByK(int[] A, int K) {
        int[] map = new int[K];
		map[0] = 1;
        int count = 0, sum = 0;
        for(int a : A) {
            sum = (sum + a) % K;
            if(sum < 0) sum += K;  // Because -1 % 5 = -1, but we need the positive mod 4
            count += map[sum];
            map[sum]++;
        }
        return count;
    }
}
```
</p>


### [Java/C++/Python] Prefix Sum
- Author: lee215
- Creation Date: Sun Jan 13 2019 12:06:04 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 18 2019 22:58:08 GMT+0800 (Singapore Standard Time)

<p>
Calculate the prefix sum and count it.
In c++ and java, `a % K + K` takes care of the cases where `a < 0`.

**Java, use HashMap**
```
    public int subarraysDivByK(int[] A, int K) {
        Map<Integer, Integer> count = new HashMap<>();
        count.put(0, 1);
        int prefix = 0, res = 0;
        for (int a : A) {
            prefix = (prefix + a % K + K) % K;
            res += count.getOrDefault(prefix, 0);
            count.put(prefix, count.getOrDefault(prefix, 0) + 1);
        }
        return res;
    }
```

**Java, Use Array**
```
    public int subarraysDivByK(int[] A, int K) {
        int[] count = new int[K];
        count[0] = 1;
        int prefix = 0, res = 0;
        for (int a : A) {
            prefix = (prefix + a % K + K) % K;
            res += count[prefix]++;
        }
        return res;
    }
```

**C++\uFF0CUsing vecotor:**
```
    int subarraysDivByK(vector<int>& A, int K) {
        vector<int> count(K);
        count[0] = 1;
        int prefix = 0, res = 0;
        for (int a : A) {
            prefix = (prefix + a % K + K) % K;
            res += count[prefix]++;
        }
        return res;
    }
```

**Python**
```
    def subarraysDivByK(self, A, K):
        res = 0
        prefix = 0
        count = [1] + [0] * K
        for a in A:
            prefix = (prefix + a) % K
            res += count[prefix]
            count[prefix] += 1
        return res
```
</p>


### [Java] Clean O(n) Number Theory + Prefix Sums
- Author: EddieCarrillo
- Creation Date: Sun Jan 13 2019 12:01:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 13 2019 12:01:23 GMT+0800 (Singapore Standard Time)

<p>
**Logic:**
I am already going to assume that you know about prefix sums before you read this.
We can all agree that for an array int[] A, where N = len(A), that there are N prefix sums.
prefix[0] = A[0], prefix[1] = A[0] + A[1],  ... prefix[i] = A[0] + ... + A[i].

Then to calculate how many subarrays are divisible by K is logically equivalent to saying, how many ways can we pair up all prefix sum pairs (i,j) where i < j 
such that (prefix[j] - prefix[i]) % K == 0.

Just from that information alone we easily get a O(n^2) solution.
Compute all prefix sums, then check all pair to see if k divides the  difference between them.

However, if we just exploit some information w.r.t to the remainder of each prefix sum we can manipulate this into a linear algorithm. Here\'s how.

**Number Theory Part**
I noted above that we need to find all prefix sum pairs (i,j) such tha (p[j] - p[i]) % K == 0.
But this is only true, **if and only if** ```p[j] % K == p[i] % K``` 
Why is this?

According the the division algorithm we can express p[j] and p[i] in the following way.
p[j] = b*K + r0 where 0 <= r0 < K
p[i] = a*K + r1 where 0<= r1 < K

Then ```p[j] - p[i] = (b*K + r0) - (a*K + r1) ``` 
```= b*K - a*K + r0 - r1 = K*(b-a) + r0 - r1```
Again: ```p[j] - p[i] = K*(b-a) + (r0-r1)```, in other words
```K``` only ```divides p[j] - p[i]``` iff ```r0-r1 = 0 <-> r0 = r1``` QED

But we should not forget about elements in the array that do not need a pairing, namely those that are are divisible by K. That\'s why I add mod[0] at the end.

Also counting pairs => N choose 2 = > n*(n-1) / 2. 



```
class Solution {
    public int subarraysDivByK(int[] A, int K) {
        //There K mod groups 0...K-1
        //For each prefix sum that does not have remainder 0 it needs to be paired with 1 with the same remainder
        //this is so the remainders cancel out.
        int N = A.length;
        int[] modGroups = new int[K];
        int sum = 0;
        for (int i = 0; i < N; i++){
            sum += A[i];
            int group = sum % K;
            
            if (group < 0) group += K; //Java has negative modulus so correct it
            modGroups[group]++;
        }
        
        int total = 0;
        for (int x : modGroups){
             if (x > 1) total += (x*(x-1)) / 2;
        }
        //Dont forget all numbers that divide K
        return total + modGroups[0];
    }
}
```
</p>


