---
title: "Bulb Switcher III"
weight: 1279
#id: "bulb-switcher-iii"
---
## Description
<div class="description">
<p>There is a room with <code>n</code> bulbs, numbered from <code>1</code> to <code>n</code>, arranged in a row from left to right. Initially, all the bulbs are turned off.</p>

<p>At moment <em>k</em> (for <em>k</em> from <code>0</code> to <code>n - 1</code>), we turn on the <code>light[k]</code> bulb. A bulb <strong>change&nbsp;color to blue</strong> only if it is on and all the previous bulbs (to the left)&nbsp;are turned on too.</p>

<p>Return the number of moments in&nbsp;which <strong>all&nbsp;turned on</strong> bulbs&nbsp;<strong>are blue.</strong></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/02/29/sample_2_1725.png" style="width: 575px; height: 300px;" /></p>

<pre>
<strong>Input:</strong> light = [2,1,3,5,4]
<strong>Output:</strong> 3
<strong>Explanation:</strong> All bulbs turned on, are blue at the moment 1, 2 and 4.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> light = [3,2,4,1,5]
<strong>Output:</strong> 2
<strong>Explanation:</strong> All bulbs turned on, are blue at the moment 3, and 4 (index-0).
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> light = [4,1,2,3]
<strong>Output:</strong> 1
<strong>Explanation:</strong> All bulbs turned on, are blue at the moment 3 (index-0).
Bulb 4th changes to blue at the moment 3.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> light = [2,1,4,3,6,5]
<strong>Output:</strong> 3
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> light = [1,2,3,4,5,6]
<strong>Output:</strong> 6
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n ==&nbsp;light.length</code></li>
	<li><code>1 &lt;= n &lt;= 5 * 10^4</code></li>
	<li><code>light</code> is a permutation of&nbsp;&nbsp;<code>[1, 2, ..., n]</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Microsoft - 4 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Straight Forward, O(1) Space
- Author: lee215
- Creation Date: Sun Mar 08 2020 12:04:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2020 12:10:49 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
`right` is the number of the right most lighted bulb.

Iterate the input light `A`,
update `right = max(right, A[i])`.

Now we have lighted up `i + 1` bulbs,
if `right == i + 1`,
it means that all the previous bulbs (to the left) are turned on too.
Then we increment `res`
<br>

## **Complexity**
Time `O(N)`
Space `O(1)`
<br>

**Java:**
```java
    public int numTimesAllBlue(int[] A) {
        int right = 0, res = 0, n = A.length;
        for (int i = 0; i < n; ++i) {
            right = Math.max(right, A[i]);
            if (right == i + 1) ++res;
        }
        return res;
    }
```

**C++:**
```cpp
    int numTimesAllBlue(vector<int>& A) {
        int right = 0, res = 0, n = A.size();
        for (int i = 0; i < n; ++i)
            res += (right = max(right, A[i])) == i + 1;
        return res;
    }
```

**Python:**
```py
    def numTimesAllBlue(self, A):
        right = res = 0
        for i, a in enumerate(A, 1):
            right = max(right, a)
            res += right == i
        return res
```

**1-line Python3**
By @ManuelP
```py
def numTimesAllBlue(self, A):
    return sum(map(operator.eq, itertools.accumulate(A, max), itertools.count(1)))
```
```py
def numTimesAllBlue(self, A):
    return sum(i == m for i, m in enumerate(itertools.accumulate(A, max), 1))
```
</p>


### [C++] Checking for contiguous sequence
- Author: PhoenixDD
- Creation Date: Sun Mar 08 2020 12:01:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 12:26:24 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
We can keep track of maximum `blue` bulbs that are switched on from the first bulb.
Everytime we switch on a bulb we can check if it forms a contiguous sequence of switched on bulbs/blue bulbs.
Whenever the number of blue bulbs is equal to the number of bulbs switched on we can add 1 to the result.
The key here is to always start the check of contiguous sequence from the previous found end of the blue bulbs sequence.

**Solution**
```c++
class Solution {
public:
    int numTimesAllBlue(vector<int>& light) 
    {
        vector<bool> on(light.size(),false);
        int maxBlue=0,result=0;
        for(int i=0;i<light.size();i++)
        {
            on[light[i]-1]=true;
            while(maxBlue<light.size()&&on[maxBlue])   //Increase the end of blue bulbs sequence starting from 0,
                maxBlue++;
            result+=maxBlue==i+1;                  		//If number of blue bulbs is equal to number of switched on bulbs.
        }
        return result;
    }
};
```
**Complexity**
Space: `O(n)`.
Time `O(n)`,
</p>


### [Java] Beautiful math to Rescue (Sum of first n natural numbers) O(n) time O(1) space
- Author: manrajsingh007
- Creation Date: Sun Mar 08 2020 12:03:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 12:06:16 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int numTimesAllBlue(int[] light) {
        int n = light.length, ans = 0, currMax = 0;
        long currSum = 0;
        for(int i = 0; i < n; i++) {
            currMax = Math.max(currMax, light[i]);
            currSum += (long)light[i];
            if(currSum == ((long)currMax * (currMax + 1)) / 2) ans++;
        }
        return ans;
    }
}
</p>


