---
title: "Burst Balloons"
weight: 295
#id: "burst-balloons"
---
## Description
<div class="description">
<p>Given <code>n</code> balloons, indexed from <code>0</code> to <code>n-1</code>. Each balloon is painted with a number on it represented by array <code>nums</code>. You are asked to burst all the balloons. If the you burst balloon <code>i</code> you will get <code>nums[left] * nums[i] * nums[right]</code> coins. Here <code>left</code> and <code>right</code> are adjacent indices of <code>i</code>. After the burst, the <code>left</code> and <code>right</code> then becomes adjacent.</p>

<p>Find the maximum coins you can collect by bursting the balloons wisely.</p>

<p><b>Note:</b></p>

<ul>
	<li>You may imagine <code>nums[-1] = nums[n] = 1</code>. They are not real therefore you can not burst them.</li>
	<li>0 &le; <code>n</code> &le; 500, 0 &le; <code>nums[i]</code> &le; 100</li>
</ul>

<p><b>Example:</b></p>

<pre>
<b>Input:</b> <code>[3,1,5,8]</code>
<b>Output:</b> <code>167 
<strong>Explanation: </strong></code>nums = [3,1,5,8] --&gt; [3,5,8] --&gt;   [3,8]   --&gt;  [8]  --&gt; []
&nbsp;            coins =  3*1*5      +  3*5*8    +  1*3*8      + 1*8*1   = 167
</pre>
</div>

## Tags
- Divide and Conquer (divide-and-conquer)
- Dynamic Programming (dynamic-programming)

## Companies
- Samsung - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Snapchat - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Intuition

At first glance, the obvious solution is to find every possible order in which balloons can be burst. Since at each step we burst one balloon, the number of possibilities we get at each step are $$N \times (N - 1) \times (N - 2) \times ... \times 1$$, giving us a time complexity of $$O(N!)$$. We can make a small improvement to this by caching the set of existing balloons. Since each balloon can be burst or not burst, and we are incurring extra time creating a set of balloons each time, we are still looking at a solution worse than $$O(2^N)$$.

There are two techniques we will use to optimize our solution:

1. Divide and Conquer

    * After bursting balloon `i`, we can divide the problem into the balloons to the left of `i` (`nums[0:i]`) and to the right of `i` (`nums[i+1:]`).

    * To find the optimal solution we check every optimal solution after bursting each balloon.

    * Since we will find the optimal solution for every range in nums, and we burst every balloon in every range to find the optimal solution, we have an $$O(N^2) * O(N) = O(N^3)$$ solution

    * However, if we try to divide our problem in the order where we burst balloons first, we run into an issue. As balloons burst, the adjacency of other balloons changes. We are unable to keep track of what balloons the endpoints of our intervals are adjacent to. This is where the second technique comes in.

2. Working Backwards

    * Above, we start with all the balloons and try to burst them. This causes adjacency issues. Instead, we can start with no balloons and add them in reverse order of how they were popped. Each time we add a balloon, we can divide and conquer on its left and right sides, letting us add new balloons in between.

    * This gets rid of adjacency issues. For the left interval, we know that the left boundary stays the same, and we know that our right boundary is the element we just added. The opposite goes for the right interval. We compute the coins added from adding balloon `i` with `nums[left] * nums[i] * nums[right]`.




---
#### Approach 1: Dynamic Programming (Top-Down)

**Algorithm**

To deal with the edges of our array we can reframe the problem into only bursting the non-edge balloons and adding ones on the sides.

We define a function `dp` to return the maximum number of coins obtainable on the open interval (left, right). Our base case is if there are no integers on our interval (`left + 1 == right`), and therefore there are no more balloons that can be added. We add each balloon on the interval, divide and conquer the left and right sides, and find the maximum score.

The best score after adding the `i`th balloon is given by:

    nums[left] * nums[i] * nums[right] + dp(left, i) + dp(i, right)

`nums[left] * nums[i] * nums[right]` is the number of coins obtained from adding the `i`th balloon, and `dp(left, i) + dp(i, right)` are the maximum number of coins obtained from solving the left and right sides of that balloon respectively.

An illustration of how we divide and conquer the left and right sides:

!?!../Documents/312_burst_balloons.json:960,540!?!

**Implementation**
<iframe src="https://leetcode.com/playground/ynhVKc9x/shared" frameBorder="0" width="100%" height="500" name="ynhVKc9x"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N^3)$$. We determine the maximum score from all (left, right) pairs. Determining the maximum score requires adding all balloons in (left, right), giving $$O(N^2) * O(N) = O(N^3)$$

* Space complexity : $$O(N^2)$$ to store our cache


---
#### Approach 2: Dynamic Programming (Bottom-Up)

**Algorithm**

Instead of caching our results in recursive calls we can build our way up to `dp(0, len(nums)-1)` in a bottom-up manner.

**Implementation**
<iframe src="https://leetcode.com/playground/JtpoxvUX/shared" frameBorder="0" width="100%" height="500" name="JtpoxvUX"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N^3)$$. There are $$N^2$$ (left, right) pairs and it takes `O(N)` to find the value of one of them.

* Space complexity : $$O(N^2)$$. This is the size of `dp`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Share some analysis and explanations
- Author: dietpepsi
- Creation Date: Mon Nov 30 2015 04:51:32 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 05:35:22 GMT+0800 (Singapore Standard Time)

<p>
**See [here for a better view](http://algobox.org/burst-balloons/)**

**Be Naive First**

When I first get this problem, it is far from dynamic programming to me. I started with the most naive idea the backtracking.

We have n balloons to burst, which mean we have n steps in the game. In the i th step we have n-i balloons to burst, i = 0~n-1. Therefore we are looking at an algorithm of O(n!). Well, it is slow, probably works for n < 12 only.

Of course this is not the point to implement it. We need to identify the redundant works we did in it and try to optimize.

Well, we can find that for any balloons left the maxCoins does not depends on the balloons already bursted. This indicate that we can use memorization (top down) or dynamic programming (bottom up) for all the cases from small numbers of balloon until n balloons. How many cases are there?  For k balloons there are C(n, k) cases and for each case it need to scan the k balloons to compare. The sum is quite big still. It is better than O(n!) but worse than O(2^n).


**Better idea**

We then think can we apply the divide and conquer technique? After all there seems to be many self similar sub problems from the previous analysis.

Well,  the nature way to divide the problem is burst one balloon and separate the balloons into 2 sub sections one on the left and one one the right. However, in this problem the left and right become adjacent and have effects on the maxCoins in the future.

Then another interesting idea come up. Which is quite often seen in dp problem analysis. That is reverse thinking. Like I said the coins you get for a balloon does not depend on the balloons already burst. Therefore
instead of divide the problem by the first balloon to burst, we divide the problem by the last balloon to burst. 

Why is that? Because only the first and last balloons we are sure of their adjacent balloons before hand!

For the first we have `nums[i-1]*nums[i]*nums[i+1]` for the last we have `nums[-1]*nums[i]*nums[n]`.


OK. Think about n balloons if i is the last one to burst, what now?

We can see that the balloons is again separated into 2 sections. But this time since the balloon i is the last balloon of all to burst, the left and right section now has well defined boundary and do not affect each other! Therefore we can do either recursive method with memoization or dp.

**Final**

Here comes the final solutions. Note that we put 2 balloons with 1 as boundaries and also burst all the zero balloons in the first round since they won't give any coins.
The algorithm runs in O(n^3) which can be easily seen from the 3 loops in dp solution.

**Java D&C with Memoization**

    public int maxCoins(int[] iNums) {
        int[] nums = new int[iNums.length + 2];
        int n = 1;
        for (int x : iNums) if (x > 0) nums[n++] = x;
        nums[0] = nums[n++] = 1;


        int[][] memo = new int[n][n];
        return burst(memo, nums, 0, n - 1);
    }

    public int burst(int[][] memo, int[] nums, int left, int right) {
        if (left + 1 == right) return 0;
        if (memo[left][right] > 0) return memo[left][right];
        int ans = 0;
        for (int i = left + 1; i < right; ++i)
            ans = Math.max(ans, nums[left] * nums[i] * nums[right] 
            + burst(memo, nums, left, i) + burst(memo, nums, i, right));
        memo[left][right] = ans;
        return ans;
    }
    // 12 ms

**Java DP**

    public int maxCoins(int[] iNums) {
        int[] nums = new int[iNums.length + 2];
        int n = 1;
        for (int x : iNums) if (x > 0) nums[n++] = x;
        nums[0] = nums[n++] = 1;


        int[][] dp = new int[n][n];
        for (int k = 2; k < n; ++k)
            for (int left = 0; left < n - k; ++left) {
                int right = left + k;
                for (int i = left + 1; i < right; ++i)
                    dp[left][right] = Math.max(dp[left][right], 
                    nums[left] * nums[i] * nums[right] + dp[left][i] + dp[i][right]);
            }

        return dp[0][n - 1];
    }
    // 17 ms

**C++ DP**

    int maxCoinsDP(vector<int> &iNums) {
        int nums[iNums.size() + 2];
        int n = 1;
        for (int x : iNums) if (x > 0) nums[n++] = x;
        nums[0] = nums[n++] = 1;


        int dp[n][n] = {};
        for (int k = 2; k < n; ++k) {
            for (int left = 0; left < n - k; ++left)
                int right = left + k;
                for (int i = left + 1; i < right; ++i)
                    dp[left][right] = max(dp[left][right],
                         nums[left] * nums[i] * nums[right] + dp[left][i] + dp[i][right]);
            }

        return dp[0][n - 1];
    }
    // 16 ms

**Python DP**

    def maxCoins(self, iNums):
        nums = [1] + [i for i in iNums if i > 0] + [1]
        n = len(nums)
        dp = [[0]*n for _ in xrange(n)]

        for k in xrange(2, n):
            for left in xrange(0, n - k):
                right = left + k
                for i in xrange(left + 1,right):
                    dp[left][right] = max(dp[left][right],
                           nums[left] * nums[i] * nums[right] + dp[left][i] + dp[i][right])
        return dp[0][n - 1]

    # 528ms
</p>


### For anyone that is still confused after reading all kinds of explanations...
- Author: jienan2
- Creation Date: Fri Sep 15 2017 09:41:53 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 16:35:46 GMT+0800 (Singapore Standard Time)

<p>
I think the most upvoted post didn't talk about what dp[i][j] represent and what exactly does the transition function : 

**for (int k = left; k <= right; ++k)
                    dp[left][right] = max(dp[left][right], nums[left-1] * nums[k] * nums[right+1] + dp[left][k-1] + dp[k+1][right])****
 means.  

Or maybe it did talk about it but I miss it. But anyway here is my understand of this problem after reading countless of posts and comments : 

First of all, dp[i][j] in here means, the maximum coins we get after we burst **all** the balloons between i and j in the original array. 

For example with input [3,1,5,8] :

dp[0][0] means we burst ballons between [0,0], which means we only burst the first balloon in original array. So dp[0][0] is 1 * 3 * 1 = 3.

dp[1][1] means we burst balloons between [1][1], which means we only burst the second ballon in the original array. So dp[1][1] is 3 * 1 * 5 = 15.

So in the end for this problem we want to find out dp[0][ arr.length - 1 ], which is the maximum value we can get when we burst all the balloon between [0 , length -1]

To get that we need the transition function :

**for (int k = left; k <= right; ++k)
                    dp[left][right] = max(dp[left][right], nums[left-1] * nums[k] * nums[right+1] + dp[left][k-1] + dp[k+1][right])****

This transition function basically says in order to get the maximum value we can get for bursting all the balloons between  [ i , j] , we just loop through each balloon between these two indexes and make them to be the last balloon to be burst, 

why we pick it as the last balloon to burst ?

For example when calculating dp[0,3] and picking index 2 as the last balloon to burst, 

 [ 3 , 1 , 5 , 8] ,  that means 5 is the last balloon to burst between [0,3] , to get the maximum value when picking 5 as the last balloon to burst :

max = maximum value of bursting all the balloon on the left side of 5 + maximum value of bursting all the balloon on the right side of 5 + bursting balloon 5 when left side and right side are gone. 

That is dp[0, 1]  + nums[0 - 1] * nums[2] * nums[3 + 1] + + dp[3,3];  
 
That is dp[left, k - 1]  + nums[left - 1] * nums[k] * nums[right + 1] + dp[k+1, right] ; 

to get the maximum dp[left, right] we just loop through all the possible value of k to get the maximum.


Hope it helps!
</p>


### Another way to think of this problem (Matrix-chain multiplication)
- Author: GREEsDY
- Creation Date: Wed Jan 11 2017 11:42:42 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jan 11 2017 11:42:42 GMT+0800 (Singapore Standard Time)

<p>
If you think of bursting a balloon as multiplying two adjacent matrices, then this problem is exactly the classical DP problem **Matrix-chain multiplication** found in section 15.2 in the book Introduction to Algorithms (2nd edition). 

For example, given [3,5,8] and bursting 5, the number of coins you get is the number of scalar multiplications you need to do to multiply two matrices A[3\*5] and B[5\*8]. So in this example, the original problem is actually the same as given a matrix chain A[1\*3]\*B[3\*5]\*C[5\*8]*D[8\*1], fully parenthesize it so that the total number of scalar multiplications is maximized, although the orignal matrix-chain multiplication problem in the book asks to minimize it. Then you can see it clearly as a classical DP problem.
</p>


