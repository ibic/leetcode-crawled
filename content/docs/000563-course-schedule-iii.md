---
title: "Course Schedule III"
weight: 563
#id: "course-schedule-iii"
---
## Description
<div class="description">
<p>There are <code>n</code> different online courses numbered from <code>1</code> to <code>n</code>. Each course has some duration(course length) <code>t</code> and closed on <code>d<sub>th</sub></code> day. A course should be taken <b>continuously</b> for <code>t</code> days and must be finished before or on the <code>d<sub>th</sub></code> day. You will start at the <code>1<sub>st</sub></code> day.</p>

<p>Given <code>n</code> online courses represented by pairs <code>(t,d)</code>, your task is to find the maximal number of courses that can be taken.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b> [[100, 200], [200, 1300], [1000, 1250], [2000, 3200]]
<b>Output:</b> 3
<b>Explanation:</b> 
There&#39;re totally 4 courses, but you can take 3 courses at most:
First, take the 1st course, it costs 100 days so you will finish it on the 100th day, and ready to take the next course on the 101st day.
Second, take the 3rd course, it costs 1000 days so you will finish it on the 1100th day, and ready to take the next course on the 1101st day. 
Third, take the 2nd course, it costs 200 days so you will finish it on the 1300th day. 
The 4th course cannot be taken now, since you will finish it on the 3300th day, which exceeds the closed date.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>The integer 1 &lt;= d, t, n &lt;= 10,000.</li>
	<li>You can&#39;t take two courses simultaneously.</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags
- Greedy (greedy)

## Companies
- Google - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Works Applications - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force

**Algorithm**

The most naive solution will be to consider every possible permutation of the given courses and to try to take as much courses as possible by  taking the courses in a serial order in every permutation. We can find out the maximum number of courses that can be taken from out of values obtained from these permutations.

**Complexity Analysis**

* Time complexity : $$O\big((n+1)!\big)$$. A total of $$n!$$ permutations are possible for the $$courses$$ array of length $$n$$. For every permutation, we scan over the $$n$$ elements of the permutation to find the number of courses that can be taken in each case.

* Space complexity : $$O(n)$$. Each permutation needs $$n$$ space.
<br />
<br />

---
#### Approach 2: Recursion with Memoization

**Algorithm**

Before we move on to the better approaches, let's discuss one basic idea to solve the given problem. Suppose, we are considering only two courses $$(a,x)$$ and $$(b,y)$$. Let's assume $$y>x$$. Now, we'll look at the various relative values which $$a$$ and $$b$$ can take, and which course should be taken first in each of these cases. In all the cases, we assume that the course's duration is always lesser than its end day i.e. $$a<x$$ and $$b<y$$.

1. $$(a+b) &le; x$$: In this case, we can take the courses in any order. Both the courses can be taken irrespective of the order in which the courses are taken.

![Courses](../Figures/630/630_Course_Schedule_III_1.PNG)
{:align="center"}

2. $$(a+b)>x$$, $$a>b$$, $$(a+b) &leq; y$$: In this case, as is evident from the figure, both the courses can be taken only by taking course $$a$$ before $$b$$.

![Courses](../Figures/630/630_Course_Schedule_III_2.PNG)
{:align="center"}

3. $$(a+b)>x$$, $$b>a$$, $$(a+b) &leq; y$$: In this case also, both the courses can be taken only by taking course $$a$$ before $$b$$.

![Courses](../Figures/630/630_Course_Schedule_III_3.PNG)
{:align="center"}

4. $$(a+b)>y$$: In this case, irrespective of the order in which we take the courses, only one course can be taken.

![Courses](../Figures/630/630_Course_Schedule_III_4.PNG)
{:align="center"}

From the above example, we can conclude that it is always profitable to take the course with a smaller end day prior to a course with a larger end day. This is because, the course with a smaller duration, if can be taken, can surely be taken only if it is taken prior to a course with a larger end day. 

Based on this idea, firstly, we sort the given $$courses$$ array based on their end days. Then, we try to take the courses in a serial order from this sorted $$courses$$ array. 

In order to solve the given problem, we make use of a recursive function `schedule(courses, i, time)` which returns the maximum number of courses that can be taken starting from the $$i^{th}$$ course(starting from 0), given the time aleady consumed by the other courses is $$time$$, i.e. the current time is $$time$$, given a $$courses$$ array as the schedule.

Now, in each function call to `schedule(courses, i, time)`, we try to include the current course in the taken courses. But, this can be done only if $$time + duration_i < end\_day_i$$. Here, $$duration_i$$ refers to the duration of the $$i^{th}$$ course and $$end\_day_i$$ refers to the end day of the $$i^{th}$$ course. 

If the course can be taken, we increment the number of courses taken and obtain the number of courses that can be taken by passing the updated time and courses' index. i.e. we make the function call `schedule(courses, i + 1, time + duration_i)`. Let's say, we store the number of courses that can be taken by taking the current course in $$taken$$ variable.

Further, for every current course, we also leave the current course, and find the number of courses that can be taken thereof. Now, we need not update the time, but we need to update the courses' index. Thus, we make the function call, `schedule(courses, i + 1, time)`. Let's say, we store the count obtained in $$not\_taken$$ variable. 

While returning the number of courses at the end of each function call, we return the maximum value out of $$taken$$ and $$not\_taken$$.

Thus, the function call `schedule(courses, 0, 0)` gives the required result.


In order to remove this redundancy, we make use of a memoization array $$memo$$, such that $$memo[i][j]$$ is used to store the result of the function call `schedule(courses, i, time)`. Thus, whenever the same function call is made again, we can return the result directly from the $$memo$$ array. This helps to prune the search space to a great extent.

<iframe src="https://leetcode.com/playground/eFfe6FVz/shared" frameBorder="0" width="100%" height="378" name="eFfe6FVz"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n*d)$$. $$memo$$ array of size $$n$$x$$d$$ is filled once. Here, $$n$$ refers to the number of courses in the given $$courses$$ array and $$d$$ refers to the maximum value of the end day from all the end days in the $$courses$$ array.

* Space complexity : $$O(n*d)$$. $$memo$$ array of size $$n$$x$$d$$ is used.
<br />
<br />

---
#### Approach 3: Iterative Solution

For the current approach, the idea goes as follows. As discussed in the previous approaches, we need to sort the given $$courses$$ array based on the end days. Thus, we consider the courses in the ascending order of their end days. We keep a track of the current time in a $$time$$ variable. Along with this, we also keep a track of the number of courses taken till now in $$count$$ variable.

For each course being considered currently(let's say $$i^{th}$$ course), we try to take this course. But, to be able to do so, the current course should end before its corresponding end day i.e. $$time + duration_i &leq; end\day_i$$. Here, $$duration_i$$ refers to the duration of the $$i^{th}$$ course and $$end\_day_i$$ refers to the end day of the $$i^{th}$$ course. 

If this course can be taken, we update the current time to $$time + duration_i$$ and also increment the current $$count$$ value to indicate that one more course has been taken. 

But, if we aren't able to take the current course i.e. $$time + duration_i > end\_day_i$$, we can try to take this course by removing some other course from amongst the courses that have already been taken. But, the current course can fit in by removing some other course, only if the duration of the course($$j^{th}$$) being removed $$duration_j$$ is larger than the current course's duration, $$duration_i$$ i.e. $$duration_j > duration_i$$. 

We are sure of the fact that by removing the $$j^{th}$$ course, we can fit in the current course, because, $$course_j$$ was already fitting in the duration available till now. Since, $$duration_i < duration_j$$, the current course can surely take its place. Thus, we look for a course from amongst the taken courses having a duration larger than the current course.

But why are we doing this replacement? The answer to this question is as follows. By replacing the $$j^{th}$$ course, with the $$i^{th}$$ course of a relatively smaller duration, we can increase the time available for upcoming courses to be taken. An extra $$duration_j - duration_i$$ time can be made available by doing so. 

Now, for this saving in time to be maximum, the course taken for the replacement should be the one with the maximum duration. Thus, from amongst the courses that have been taken till now, we find the course having the maximum duration which should be more than the duration of the current course(which can't be taken). 

Let's say, this course be called as $$max\_i$$. Thus, now, a saving of $$duration_{max\_i} - duration_i$$ can be achived, which could help later in fitting in more courses to be taken.

If such a course, $$max\_i$$, is found, we remove this course from the taken courses and consider the current course as taekn. We also mark this course with $$\text{-1}$$ to indicate that this course has not been taken and should not be considered in the future again for replacement. 

But, if such a course isn't found, we can't take the current course at any cost. Thus, we mark the current course with $$\text{-1}$$ to indicate that the current course has not been taken.

At the end, the value of $$count$$ gives the required result.

The following animation illustrates the process.

!?!../Documents/630_Course_Schedule_III.json:1000,563!?!

<iframe src="https://leetcode.com/playground/QV3ZZCDt/shared" frameBorder="0" width="100%" height="463" name="QV3ZZCDt"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$.  We iterate over the $$count$$ array of size $$n$$ once. For every element currently considered, we could scan backwards till the first element, giving $$O(n^2)$$ complexity. Sorting the $$count$$ array takes $$O\big(n \log n\big)$$ time for $$count$$ array.

* Space complexity : $$O(1)$$. Constant extra space is used.
<br />
<br />

---
#### Approach 4: Optimized Iterative

In the last approach, we've seen that, in the case of current course which can't be taken direclty, i.e. for $$time + duration_i > end\_day_i$$, we need to traverse back in the $$courses$$ array till the beginning to find a course with the maximum duration which is larger than the current course's duration. This backward traversal also goes through the courses which aren't  taken and thus, can't be replaced, and have been marked as $$\text{-1}$$. 

We can bring in some optimization here. For this, we should search among only those courses which have been taken(and not the ones which haven't been taken). 

To do so, as we iterate over the $$courses$$ array, we also keep on updating it, such that the first $$count$$ number of elements in this array now correspond to only those $$count$$ number of courses which have been taken till now. 

Thus, whenever we update the $$count$$ to indicate that one more course has been taken, we also update the $$courses[count]$$ entry to 
reflect the current course that has just been taken. 

Whenever, we find a course for which $$time + duration_i > end\_day_i$$, we find a $$max_i$$ course from only amongst these first $$count$$ number of courses in the $$courses$$ array, which indicate the courses that have been taken till now. 

Also, instead of marking this $$max_i^{th}$$ course with a $$\text{-1}$$, we can simply replace this course with the current course. Thus, the first $$count$$ courses still reflect the courses that have been taken till now.

<iframe src="https://leetcode.com/playground/n6dHjzhG/shared" frameBorder="0" width="100%" height="463" name="n6dHjzhG"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n*count)$$. We iterate over a total of $$n$$ elements of the $$courses$$ array. For every element, we can traverse backwards upto atmost $$count$$(final value) number of elements.

* Space complexity : $$O(1)$$. Constant extra space is used.
<br />
<br />

---
#### Approach 5: Extra List

**Algorithm**

In the last approach, we updated the $$course$$ array itself so that the first $$count$$ elements indicate the $$count$$ number of courses that have been taken till now. If it is required to retain the $$courses$$ array as such, we can do the same job by maintaining a separate list $$valid\_list$$ which is the list of those courses that have been taken till now. 

Thus, to find the $$max_i$$ course, we need to search in this list only. Further, when replacing this $$max_i^{th}$$ course with the current course, we can replace this $$max_i$$ course in the list with current course directly. The rest of the method remains the same as the last approach.

<iframe src="https://leetcode.com/playground/pPmcFbfG/shared" frameBorder="0" width="100%" height="463" name="pPmcFbfG"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n*m)$$. We iterate over a total of $$n$$ elements of the $$courses$$ array. For every element, we can traverse over atmost $$m$$ number of elements. Here, $$m$$ refers to the final length of the $$valid\_list$$.

* Space complexity : $$O(n)$$. The $$valid\_list$$ can contain atmost $$n$$ courses.
<br />
<br />

---
#### Approach 6: Priority Queue

**Algorithm**

This approach is inspired by [@stomach_ache](http://leetcode.com/stomach_ache)

In the last few approaches, we've seen that we needed to traverse over the courses which have been taken to find the course(with the maximum duration) which can be replaced by the current course(if it can't be taken directly). These traversals can be saved, if we make use of a Priority Queue, $$queue$$(which is implemented as a Max-Heap) which contains the durations of all the courses that have been taken till now. 

The iteration over the sorted $$courses$$ remains the same as in the last approaches. Whenver the current course($$i^{th}$$ course) can be taken($$time + duration_i &leq; end\_day_i$$), it is added to the $$queue$$ and the value of the current time is updated to $$time + duration_i$$. 

If the current course can't be taken directly, as in the previous appraoches, we need to find a course whose duration $$duration_j$$ is maximum from amongst the courses taken till now. Now, since we are maintaing a Max-Heap, $$queue$$, we can obtain this duration directly from this $$queue$$. If the duration $$duration_j > duration_i$$, we can replace the $$j^{th}$$ course, with the current one. 

Thus, we remove the $$duration_j$$ from the $$queue$$ and add the current course's duration $$duration_i$$ to the $$queue$$. We also need to make proper adjustments to the $$time$$ to account for this replacement done.

At the end, the number of elements in the $$queue$$ represent the number of courses that have been taken till now.

<iframe src="https://leetcode.com/playground/ybA2xeHz/shared" frameBorder="0" width="100%" height="344" name="ybA2xeHz"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(n \log n\big)$$. At most $$n$$ elements are added to the $$queue$$. Adding each element is followed by heapification, which takes $$O\big(\log n\big)$$ time.

* Space complexity : $$O(n)$$. The $$queue$$ containing the durations of the  courses taken can have atmost $$n$$ elements

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short Java code using PriorityQueue
- Author: KakaHiguain
- Creation Date: Sun Jun 25 2017 21:51:33 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 21:32:51 GMT+0800 (Singapore Standard Time)

<p>

```
public class Solution {
    public int scheduleCourse(int[][] courses) {
        Arrays.sort(courses,(a,b)->a[1]-b[1]); //Sort the courses by their deadlines (Greedy! We have to deal with courses with early deadlines first)
        PriorityQueue<Integer> pq=new PriorityQueue<>((a,b)->b-a);
        int time=0;
        for (int[] c:courses) 
        {
            time+=c[0]; // add current course to a priority queue
            pq.add(c[0]);
            if (time>c[1]) time-=pq.poll(); //If time exceeds, drop the previous course which costs the most time. (That must be the best choice!)
        }        
        return pq.size();
    }
}
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Jun 25 2017 11:22:38 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 01:51:49 GMT+0800 (Singapore Standard Time)

<p>
Sort all the courses by their ending time.  When considering the first K courses, they all end before ```end```.  A necessary and sufficient condition for our schedule to be valid, is that (for all K), the courses we choose to take within the first K of them, have total duration less than ```end```.

For each K, we will greedily remove the largest-length course until the total duration ```start``` is ```<= end```.  To select these largest-length courses, we will use a max heap.  ```start``` will maintain the loop invariant that it is the sum of the lengths of the courses we have currently taken.

Clearly, this greedy choice makes the number of courses used maximal for each K.  When considering potential future K, there's never a case where we preferred having a longer course to a shorter one, so indeed our greedy choice dominates all other candidates.

```
def scheduleCourse(self, A):
    pq = []
    start = 0
    for t, end in sorted(A, key = lambda (t, end): end):
        start += t
        heapq.heappush(pq, -t)
        while start > end:
            start += heapq.heappop(pq)
    return len(pq)
```

(With thanks to @uwi - see his solution [here](https://leetcode.com/contest/leetcode-weekly-contest-38/ranking/))
</p>


### Recursion -> DP->Greedy.
- Author: traceroute
- Creation Date: Fri Mar 15 2019 12:54:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 29 2019 10:48:23 GMT+0800 (Singapore Standard Time)

<p>
When I initially think about this problem, I will connect the logic to Knapsack problem. Since they share the same pattern, time is bag size, max course number is value, course time is the weight of each stone. It\'s a DP problem.
Starting with basic recusion
Time: O(2^n) Space:O(n) 
```
//DFS no cache O(2^n)
    public static int scheduleCourseDfsRecursive(int[][] courses) {
        Arrays.sort(courses, (i,j)->i[1]-j[1]);
        return helper(courses, 0, 0);
    }
    private static int helper(int[][] course, int day, int index){
        //base
        if(index == course.length){
            return 0;
        }
        int[] cur = course[index];
        int taken = 0;
        if(day+cur[0] <= cur[1]){
            taken = 1+helper(course, day+cur[0], index+1);
        }
        int not_taken = helper(course, day, index+1);
        return Math.max(taken, not_taken);
    }
```

Then optimize it with top down DP, surely this can be transformed to bottom up DP, but I\'d like to modify code slightly here.
```
//Dfs recursive with cache O(n*M) where M is max course ending time, n is # of courses
//Space  o(n)(M) -> can be optimized to O(M) if using bottom-up DP
    public static int scheduleDfsCacheDP(int[][] courses){
        if(courses == null || courses.length ==0) return 0;
        int m = courses.length; 
        Arrays.sort(courses, (i, j) -> i[1] - j[1]);
        int[][] cache = new int[m][courses[m-1][1]+1];
        return schedule(courses, 0, 0,cache);
    }
    public static int schedule(int[][] courses, int day, int index, int[][] cache){
        //base
        if(index == courses.length){
            return 0;
        }
        if(cache[index][day]!=0) return cache[index][day];
        int taken = 0;
        int[] cur = courses[index];
        if(day + cur[0] <= cur[1]){
            taken = 1 + schedule(courses, day + cur[0], index+1, cache); 
        }
        int not_taken = schedule(courses, day, index+1, cache);
        cache[index][day] = Math.max(taken, not_taken);
        return cache[index][day];
    }
	//TLE of memory, which is expected. Think about the Salesman problem. 
```
Then the last step, think about Greedy( actually just cutting unnessary recursion with an algorithm)
```
//greedy O(nlogn) + o(nlogn) space:O( n)
    /*
    Take the ealiest course that can be finished, otherwise, remove the longest course, and replaced it with multiple shorter courses. since the max course number is the goal.
    */
public static int ScheduleGreedy(int[][] courses){
        Arrays.sort(courses, (i,j)->i[1]-j[1]);
        PriorityQueue<Integer> q = new PriorityQueue<>((i,j)->j-i); //max heap
        int time =0;
        for(int i =0;i<courses.length;i++){
            int[] cur = courses[i];
            time+=cur[0];
            q.add(cur[0]);
            if(time>cur[1]) time-=q.poll();
        }
        return q.size();
    }
	//I can\'t provide the formula proof here, since I\'m bad at that. I hope someone can post how this Greedy is proven. That\'ll be super helpful.
```
</p>


