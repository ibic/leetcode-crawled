---
title: "Split a String in Balanced Strings"
weight: 1176
#id: "split-a-string-in-balanced-strings"
---
## Description
<div class="description">
<p><i data-stringify-type="italic">Balanced</i>&nbsp;strings are those who have equal quantity of &#39;L&#39; and &#39;R&#39; characters.</p>

<p>Given a balanced string&nbsp;<code data-stringify-type="code">s</code>&nbsp;split it in the maximum amount of balanced strings.</p>

<p>Return the maximum amount of splitted balanced strings.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;RLRRLLRLRL&quot;
<strong>Output:</strong> 4
<strong>Explanation: </strong>s can be split into &quot;RL&quot;, &quot;RRLL&quot;, &quot;RL&quot;, &quot;RL&quot;, each substring contains same number of &#39;L&#39; and &#39;R&#39;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;RLLLLRRRLR&quot;
<strong>Output:</strong> 3
<strong>Explanation: </strong>s can be split into &quot;RL&quot;, &quot;LLLRRR&quot;, &quot;LR&quot;, each substring contains same number of &#39;L&#39; and &#39;R&#39;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;LLLLRRRR&quot;
<strong>Output:</strong> 1
<strong>Explanation: </strong>s can be split into &quot;LLLLRRRR&quot;.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;RLRRRLLRLL&quot;
<strong>Output:</strong> 2
<strong>Explanation: </strong>s can be split into &quot;RL&quot;, &quot;RRRLLRLL&quot;, since each substring contains an equal number of &#39;L&#39; and &#39;R&#39;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 1000</code></li>
	<li><code>s[i] = &#39;L&#39; or &#39;R&#39;</code></li>
</ul>

</div>

## Tags
- String (string)
- Greedy (greedy)

## Companies
- Apple - 6 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Easy Solution
- Author: dnuang
- Creation Date: Sun Oct 13 2019 13:39:20 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 14 2019 00:44:41 GMT+0800 (Singapore Standard Time)

<p>
Greedily split the string, and with the counting
L +1
R -1

when the counter is reset to 0, we get one balanced string.


**C++**
```
int balancedStringSplit(string s) {
    int res = 0, cnt = 0;
    for (const auto& c : s) {
        cnt += c == \'L\' ? 1 : -1;
        if (cnt == 0) ++res;
    }
    return res;        
}
```


**Java**
```
public int balancedStringSplit(String s) {
    int res = 0, cnt = 0;
    for (int i = 0; i < s.length(); ++i) {
        cnt += s.charAt(i) == \'L\' ? 1 : -1;
        if (cnt == 0) ++res;
    }
    return res;             
}    
```

**Python3**
```
def balancedStringSplit(self, s: str) -> int:
    res = cnt = 0         
    for c in s:
        cnt += 1 if c == \'L\' else -1            
        if cnt == 0:
            res += 1
    return res  
```

</p>


### Confusing Examples Provided
- Author: idiotPlayer
- Creation Date: Thu Oct 17 2019 07:57:21 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 17 2019 07:57:21 GMT+0800 (Singapore Standard Time)

<p>
The problem statement is correct but the examples listed are confusing. This should be one of the examples so that people don\'t mistake the question as asking us to match with \'L\'s on one side and \'R\'s on the other side. The \'R\'s and \'L\'s can be in any order as long as there is an equal number of them in the substring.


<code>
<b>Input:</b> s = "RLRRRLLRLL"
<br>
<b>Output:</b> 2
<br>
<b>Explanation:</b> s can be split into "RL", "RRRLLRLL", since each substring contains an equal number of \'L\' and \'R\'

</code>

</p>


### [Python3] 1-liner
- Author: cenkay
- Creation Date: Sun Oct 13 2019 16:51:19 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 14 2019 00:44:38 GMT+0800 (Singapore Standard Time)

<p>
```
from itertools import accumulate as acc
class Solution:
    def balancedStringSplit(self, s: str) -> int:
        return list(acc(1 if c == \'L\' else -1 for c in s)).count(0)
```
* Easy one
```
class Solution:
    def balancedStringSplit(self, s: str) -> int:
        res = cnt = 0         
        for c in s:
            cnt += c == \'L\'
            cnt -= c == \'R\'
            res += cnt == 0
        return res  
```
</p>


