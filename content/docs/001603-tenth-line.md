---
title: "Tenth Line"
weight: 1603
#id: "tenth-line"
---
## Description
<div class="description">
<p>Given a text file&nbsp;<code>file.txt</code>, print&nbsp;just the 10th line of the&nbsp;file.</p>

<p><strong>Example:</strong></p>

<p>Assume that <code>file.txt</code> has the following content:</p>

<pre>
Line 1
Line 2
Line 3
Line 4
Line 5
Line 6
Line 7
Line 8
Line 9
Line 10
</pre>

<p>Your script should output the tenth line, which is:</p>

<pre>
Line 10
</pre>

<div class="spoilers"><b>Note:</b><br />
1. If the file contains less than 10 lines, what should you output?<br />
2. There&#39;s at least three different solutions. Try to explore all possibilities.</div>

</div>

## Tags


## Companies
- Apple - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Adobe - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (bash)
```bash
# Read from the file file.txt and output the tenth line to stdout.
i=0
while read -r line
do
    #i=$((i+1))
    let "i=i+1"
    [[ $i -eq 10 ]] && echo "$line"
done < file.txt

```

## Top Discussions
### Share four different solutions
- Author: think.hy@gmail.com
- Creation Date: Fri Apr 10 2015 15:53:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 06:50:26 GMT+0800 (Singapore Standard Time)

<p>

    # Solution 1
    cnt=0
    while read line && [ $cnt -le 10 ]; do
      let 'cnt = cnt + 1'
      if [ $cnt -eq 10 ]; then
        echo $line
        exit 0
      fi
    done < file.txt

    # Solution 2
    awk 'FNR == 10 {print }'  file.txt
    # OR
    awk 'NR == 10' file.txt

    # Solution 3
    sed -n 10p file.txt

    # Solution 4
    tail -n+10 file.txt|head -1
</p>


### Super simple solution
- Author: andyhansmall1001
- Creation Date: Fri Mar 27 2015 05:34:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 12:43:21 GMT+0800 (Singapore Standard Time)

<p>
    awk 'NR == 10' file.txt

NR: the current row number (start from 1).
Because the default action of awk is {print $0}, we can ignore the action.
</p>


### My three simple solutions
- Author: tryHarder
- Creation Date: Thu Dec 03 2015 04:54:21 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 12:43:24 GMT+0800 (Singapore Standard Time)

<p>
    # Read from the file file.txt and output the tenth line to stdout.
    
    #Solution One:
    #head -n 10 file.txt | tail -n +10
    
    #Solution Two:
    #awk 'NR==10' file.txt
    
    #Solution Three:
    sed -n 10p file.txt
</p>


