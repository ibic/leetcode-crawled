---
title: "Invert Binary Tree"
weight: 210
#id: "invert-binary-tree"
---
## Description
<div class="description">
<p>Invert a binary tree.</p>

<p><strong>Example:</strong></p>

<p>Input:</p>

<pre>
     4
   /   \
  2     7
 / \   / \
1   3 6   9</pre>

<p>Output:</p>

<pre>
     4
   /   \
  7     2
 / \   / \
9   6 3   1</pre>

<p><strong>Trivia:</strong><br />
This problem was inspired by <a href="https://twitter.com/mxcl/status/608682016205344768" target="_blank">this original tweet</a> by <a href="https://twitter.com/mxcl" target="_blank">Max Howell</a>:</p>

<blockquote>Google: 90% of our engineers use the software you wrote (Homebrew), but you can&rsquo;t invert a binary tree on a whiteboard so f*** off.</blockquote>

</div>

## Tags
- Tree (tree)

## Companies
- Google - 8 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- VMware - 4 (taggedByAdmin: false)
- Bloomberg - 4 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach #1 (Recursive) [Accepted]


This is a classic tree problem that is best-suited for a recursive approach.


**Algorithm**

The inverse of an empty tree is the empty tree. The inverse of a tree with root $$r$$, and subtrees $$\mbox{right}$$ and $$\mbox{left}$$, is a tree with root $$r$$, whose right subtree is the inverse of $$\mbox{left}$$, and whose left subtree is the inverse of $$\mbox{right}$$.

**Java**

```java
public TreeNode invertTree(TreeNode root) {
    if (root == null) {
        return null;
    }
    TreeNode right = invertTree(root.right);
    TreeNode left = invertTree(root.left);
    root.left = right;
    root.right = left;
    return root;
}
```

**Complexity Analysis**

Since each node in the tree is visited only once, the time complexity is $$O(n)$$, where $$n$$ is the number of nodes in the tree. We cannot do better than that, since at the very least we have to visit each node to invert it.

Because of recursion, $$O(h)$$ function calls will be placed on the stack in the worst case, where $$h$$ is the height of the tree. Because $$h\in O(n)$$, the space complexity is $$O(n)$$.

---
#### Approach #2 (Iterative) [Accepted]

Alternatively, we can solve the problem iteratively, in a manner similar to breadth-first search.

**Algorithm**

The idea is that we need to swap the left and right child of all nodes in the tree. So we create a queue to store nodes whose left and right child have not been swapped yet. Initially, only the root is in the queue. As long as the queue is not empty, remove the next node from the queue, swap its children, and add the children to the queue. Null nodes are not added to the queue. Eventually, the queue will be empty and all the children swapped, and we return the original root.

**Java**
```java
public TreeNode invertTree(TreeNode root) {
    if (root == null) return null;
    Queue<TreeNode> queue = new LinkedList<TreeNode>();
    queue.add(root);
    while (!queue.isEmpty()) {
        TreeNode current = queue.poll();
        TreeNode temp = current.left;
        current.left = current.right;
        current.right = temp;
        if (current.left != null) queue.add(current.left);
        if (current.right != null) queue.add(current.right);
    }
    return root;
}
```

**Complexity Analysis**

Since each node in the tree is visited / added to the queue only once, the time complexity is $$O(n)$$, where $$n$$ is the number of nodes in the tree.

Space complexity is $$O(n)$$, since in the worst case, the queue will contain all nodes in one level of the binary tree. For a full binary tree, the leaf level has $$\lceil \frac{n}{2}\rceil=O(n)$$ leaves.

Analysis written by: @noran

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Straightforward DFS recursive, iterative, BFS solutions
- Author: jmnarloch
- Creation Date: Fri Jun 12 2015 16:43:43 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 12:53:43 GMT+0800 (Singapore Standard Time)

<p>
As in many other cases this problem has more than one possible solutions:

----------

Lets start with straightforward - recursive DFS - it's easy to write and pretty much concise.

----------

    public class Solution {
        public TreeNode invertTree(TreeNode root) {
            
            if (root == null) {
                return null;
            }
    
            final TreeNode left = root.left,
                    right = root.right;
            root.left = invertTree(right);
            root.right = invertTree(left);
            return root;
        }
    }

----------

The above solution is correct, but it is also bound to the application stack, which means that it's no so much scalable - (you can find the problem size that will overflow the stack and crash your application), so more robust solution would be to use stack data structure.

----------


    public class Solution {
        public TreeNode invertTree(TreeNode root) {
            
            if (root == null) {
                return null;
            }
    
            final Deque<TreeNode> stack = new LinkedList<>();
            stack.push(root);
            
            while(!stack.isEmpty()) {
                final TreeNode node = stack.pop();
                final TreeNode left = node.left;
                node.left = node.right;
                node.right = left;
                
                if(node.left != null) {
                    stack.push(node.left);
                }
                if(node.right != null) {
                    stack.push(node.right);
                }
            }
            return root;
        }
    }

----------

Finally we can easly convert the above solution to BFS - or so called level order traversal.

----------

    public class Solution {
        public TreeNode invertTree(TreeNode root) {
            
            if (root == null) {
                return null;
            }
    
            final Queue<TreeNode> queue = new LinkedList<>();
            queue.offer(root);
    
            while(!queue.isEmpty()) {
                final TreeNode node = queue.poll();
                final TreeNode left = node.left;
                node.left = node.right;
                node.right = left;
    
                if(node.left != null) {
                    queue.offer(node.left);
                }
                if(node.right != null) {
                    queue.offer(node.right);
                }
            }
            return root;
        }
    }

----------

If I can write this code, does it mean I can get job at Google? ;)
</p>


### 3-4 lines Python
- Author: StefanPochmann
- Creation Date: Fri Jun 12 2015 21:36:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 07:33:47 GMT+0800 (Singapore Standard Time)

<p>
    def invertTree(self, root):
        if root:
            root.left, root.right = self.invertTree(root.right), self.invertTree(root.left)
            return root

Maybe make it four lines for better readability:

    def invertTree(self, root):
        if root:
            invert = self.invertTree
            root.left, root.right = invert(root.right), invert(root.left)
            return root

---

And an iterative version using my own stack:

    def invertTree(self, root):
        stack = [root]
        while stack:
            node = stack.pop()
            if node:
                node.left, node.right = node.right, node.left
                stack += node.left, node.right
        return root
</p>


### Recursive and non-recursive C++ both 4ms
- Author: chammika
- Creation Date: Sat Jun 13 2015 16:42:06 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 29 2018 16:11:14 GMT+0800 (Singapore Standard Time)

<p>
Recursive 

    TreeNode* invertTree(TreeNode* root) {
        if (root) {
            invertTree(root->left);
            invertTree(root->right);
            std::swap(root->left, root->right);
        }
        return root;
    }

Non-Recursive

    TreeNode* invertTree(TreeNode* root) {
        std::stack<TreeNode*> stk;
        stk.push(root);
        
        while (!stk.empty()) {
            TreeNode* p = stk.top();
            stk.pop();
            if (p) {
                stk.push(p->left);
                stk.push(p->right);
                std::swap(p->left, p->right);
            }
        }
        return root;
    }
</p>


