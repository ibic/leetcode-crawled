---
title: "Longest Uncommon Subsequence II"
weight: 492
#id: "longest-uncommon-subsequence-ii"
---
## Description
<div class="description">
<p>
Given a list of strings, you need to find the longest uncommon subsequence among them. The longest uncommon subsequence is defined as the longest subsequence of one of these strings and this subsequence should not be <b>any</b> subsequence of the other strings.
</p>

<p>
A <b>subsequence</b> is a sequence that can be derived from one sequence by deleting some characters without changing the order of the remaining elements. Trivially, any string is a subsequence of itself and an empty string is a subsequence of any string.
</p>

<p>
The input will be a list of strings, and the output needs to be the length of the longest uncommon subsequence. If the longest uncommon subsequence doesn't exist, return -1.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> "aba", "cdc", "eae"
<b>Output:</b> 3
</pre>
</p>

<p><b>Note:</b>
<ol>
<li>All the given strings' lengths will not exceed 10.</li>
<li>The length of the given list will be in the range of [2, 50].</li>
</ol>
</p>
</div>

## Tags
- String (string)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force[Accepted]

In the brute force approach we will generate all the possible $$2^n$$ subsequences of all the strings and store their number of occurences in a hashmap. Longest subsequence whose frequency is equal to $$1$$ will be the required subsequence. And, if it is not found we will return $$-1$$.


<iframe src="https://leetcode.com/playground/HSf9Ggnx/shared" frameBorder="0" name="HSf9Ggnx" width="100%" height="479"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n*2^x)$$. where $$x$$ is the average length of the strings and $$n$$ is the total number of given strings.
* Space complexity : $$O(n*2^x)$$. Hashmap of size $$n*2^x$$ is used.

---
#### Approach #2 Checking Subsequence [Accepted]

**Algorithm**

By some analysis, we can note that if longest uncommon subsequence is there, then it will always be one of the string from the given list of strings.
Using this idea, we can check each string that whether it is a subsequence of any other string. If a string is not a subsequence of any other string i.e. it is uncommon , we will return maximum length string out of them. If no string found, we will return $$-1$$.

To understand the method, look at the example given below:

<!--![Checking_Subsequence](../Figures/595_Longest_Uncommon_Subsequence.gif)-->
!?!../Documents/595_Longest_Uncommon.json:1000,563!?!

<iframe src="https://leetcode.com/playground/8u5yUq7G/shared" frameBorder="0" name="8u5yUq7G" width="100%" height="462"></iframe>

**Complexity Analysis**

* Time complexity : $$O(x*n^2)$$. where $$n$$ is the number of strings and $$x$$ is the average length of the strings.

* Space complexity : $$O(1)$$. Constant space required.

---
#### Approach #3 Sorting and Checking Subsequence [Accepted]

**Algorithm**

In the last approach, we needed to compare all the given strings and compare them for the subsequence criteria. We can save some computations if we sort the given set of strings based on their lengths initially.

In this approach, firstly we sort the given strings in decreasing order of their lengths. Then, we start off by comparing the longest string with all the other strings. If none of the other strings happens to be the subsequence of the longest string, we return the length of the longest string as the result without any need of further comparisons. If some string happens to be a subsequence of the longest string, we continue the same process by choosing the second largest string as the first string and repeat the process, and so on.

<iframe src="https://leetcode.com/playground/88P8AEzt/shared" frameBorder="0" name="88P8AEzt" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(x*n^2)$$. where $$n$$ is the number of strings and $$x$$ is the average length of the strings.

* Space complexity : $$O(1)$$. Constant space required.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java(15ms) - Sort + check subsequence
- Author: earlme
- Creation Date: Mon Apr 03 2017 05:43:13 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 04:12:49 GMT+0800 (Singapore Standard Time)

<p>
Sort the strings in the reverse order. If there is not duplicates in the array, then the longest string is the answer. 

But if there are duplicates, and if the longest string is not the answer, then we need to check other strings. But the smaller strings can be subsequence of the bigger strings. 
For this reason, we need to check if the string is a subsequence of all the strings bigger than itself. If it's not, that is the answer. 
```java
    public int findLUSlength(String[] strs) {
        Arrays.sort(strs, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return o2.length() - o1.length();
            }
        });
        
        Set<String> duplicates = getDuplicates(strs);
        for(int i = 0; i < strs.length; i++) {
            if(!duplicates.contains(strs[i])) {
                if(i == 0) return strs[0].length();
                for(int j = 0; j < i; j++) {
                    if(isSubsequence(strs[j], strs[i])) break;
                    if(j == i-1) return strs[i].length();
                }
            }
        }
        return -1;
    }
    
    public boolean isSubsequence(String a, String b) {
        int i = 0, j = 0;
        while(i < a.length() && j < b.length()) {
            if(a.charAt(i) == b.charAt(j)) j++;
            i++;
        }
        return j == b.length();
    }
    
    private Set<String> getDuplicates(String[] strs) {
        Set<String> set = new HashSet<String>();
        Set<String> duplicates = new HashSet<String>();
        for(String s : strs) {
            if(set.contains(s)) duplicates.add(s);
            set.add(s);
        }
        return duplicates;
    }
```
</p>


### Python, Simple Explanation
- Author: awice
- Creation Date: Sun Apr 02 2017 11:29:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 14:22:10 GMT+0800 (Singapore Standard Time)

<p>
When we add a letter Y to our candidate longest uncommon subsequence answer of X, it only makes it strictly harder to find a common subsequence.  Thus our candidate longest uncommon subsequences will be chosen from the group of words itself.

Suppose we have some candidate X.  We only need to check whether X is not a subsequence of any of the other words Y.  To save some time, we could have quickly ruled out Y when len(Y) < len(X), either by adding "if len(w1) > len(w2): return False" or enumerating over A[:i] (and checking neighbors for equality.)  However, the problem has such small input constraints that this is not required.

We want the max length of all candidates with the desired property, so we check candidates in descending order of length.  When we find a suitable one, we know it must be the best global answer.

```
def subseq(w1, w2):
    #True iff word1 is a subsequence of word2.
    i = 0
    for c in w2:
        if i < len(w1) and w1[i] == c:
            i += 1
    return i == len(w1)
    
A.sort(key = len, reverse = True)
for i, word1 in enumerate(A):
    if all(not subseq(word1, word2) 
            for j, word2 in enumerate(A) if i != j):
        return len(word1)
return -1
```
</p>


### JAVA 13 lines 5ms beats 100% with explaination
- Author: caraxin
- Creation Date: Thu Jul 05 2018 02:41:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 02 2018 12:16:13 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int findLUSlength(String[] strs) {
        int res=-1, n=strs.length;
        for (int i=0; i<n; i++){
            if (strs[i].length()<res) continue;
            int j=-1;
            while(++j<n) if (i!=j && isSubsequence(strs[i], strs[j])) break;
            if (j==n) res=Math.max(res, strs[i].length());
        }
        return res;
    }
    public boolean isSubsequence(String a, String b){
        int i=0, j=0;
        while(i<a.length() && j<b.length()) if (a.charAt(i)==b.charAt(j++)) i++;
        return i==a.length();
    }
}
```
![image](https://s3-lc-upload.s3.amazonaws.com/users/caraxin/image_1530730044.png)
</p>


