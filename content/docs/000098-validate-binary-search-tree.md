---
title: "Validate Binary Search Tree"
weight: 98
#id: "validate-binary-search-tree"
---
## Description
<div class="description">
<p>Given a binary tree, determine if it is a valid binary search tree (BST).</p>

<p>Assume a BST is defined as follows:</p>

<ul>
	<li>The left subtree of a node contains only nodes with keys <strong>less than</strong> the node&#39;s key.</li>
	<li>The right subtree of a node contains only nodes with keys <strong>greater than</strong> the node&#39;s key.</li>
	<li>Both the left and right subtrees must also be binary search trees.</li>
</ul>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
    2
   / \
  1   3

<strong>Input:</strong>&nbsp;[2,1,3]
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<pre>
    5
   / \
  1   4
&nbsp;    / \
&nbsp;   3   6

<strong>Input:</strong> [5,1,4,null,null,3,6]
<strong>Output:</strong> false
<strong>Explanation:</strong> The root node&#39;s value is 5 but its right child&#39;s value is 4.
</pre>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Bloomberg - 23 (taggedByAdmin: true)
- Facebook - 12 (taggedByAdmin: true)
- Asana - 11 (taggedByAdmin: false)
- Microsoft - 10 (taggedByAdmin: true)
- Amazon - 9 (taggedByAdmin: true)
- Atlassian - 6 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Qualtrics - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Yandex - 6 (taggedByAdmin: false)
- Oracle - 5 (taggedByAdmin: false)
- Visa - 3 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Tree definition

First of all, here is the definition of the ```TreeNode``` which we would use.

<iframe src="https://leetcode.com/playground/GJg6BP2J/shared" frameBorder="0" width="100%" height="225" name="GJg6BP2J"></iframe>
<br />
<br />


---
#### Intuition

On the first sight, the problem is trivial. Let's traverse the tree
and check at each step if `node.right.val > node.val` and 
`node.left.val < node.val`. This approach would even work for some
trees 
![compute](../Figures/98/98_not_bst.png)

The problem is this approach will not work for all cases. 
Not only the right child should be larger than the node 
but all the 
elements in the right subtree. Here is an example :

![compute](../Figures/98/98_not_bst_3.png)

That means one should keep both upper 
and lower limits for each node while traversing the tree, 
and compare the node value not
with children values but with these limits.
<br />
<br />


---
#### Approach 1: Recursion

The idea above could be implemented as a recursion.
One compares the node value with its upper and lower limits
if they are available. Then one repeats the same 
step recursively for left and right subtrees. 

!?!../Documents/98_LIS.json:1000,462!?!

<iframe src="https://leetcode.com/playground/sUgNuQnw/shared" frameBorder="0" width="100%" height="412" name="sUgNuQnw"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since we visit each node exactly once. 
* Space complexity : $$\mathcal{O}(N)$$ since we keep up to the entire tree.

<br />
<br />


---
#### Approach 2: Iteration

The above recursion could be converted into iteration, 
with the help of stack. DFS would be better than BFS since 
it works faster here.

<iframe src="https://leetcode.com/playground/qsxLtFeG/shared" frameBorder="0" width="100%" height="500" name="qsxLtFeG"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since we visit each node exactly once. 
* Space complexity : $$\mathcal{O}(N)$$ since we keep up to the entire tree.
<br />
<br />


---
#### Approach 3: Inorder traversal

**Algorithm**

Let's use the order of nodes in the 
[inorder traversal](https://leetcode.com/articles/binary-tree-inorder-traversal/) 
`Left -> Node -> Right`.

![postorder](../Figures/145_transverse.png)

Here the nodes are enumerated in the order you visit them, 
and you could follow `1-2-3-4-5` to compare different strategies.

`Left -> Node -> Right` order of inorder traversal 
means for BST that each element should be smaller 
than the next one.

Hence the algorithm with $$\mathcal{O}(N)$$ time complexity 
and $$\mathcal{O}(N)$$ space complexity could be simple:

- Compute inorder traversal list `inorder`.

- Check if each element in `inorder` is smaller than the next one.

![postorder](../Figures/98/98_bst_inorder.png)

> Do we need to keep the whole `inorder` traversal list? 

Actually, no. The last added inorder element is enough 
to ensure at each step that the tree is BST (or not).
Hence one could merge both steps into one and
reduce the used space.

**Implementation**

<iframe src="https://leetcode.com/playground/ohsrFTua/shared" frameBorder="0" width="100%" height="429" name="ohsrFTua"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ in the worst case
when the tree is BST or the "bad" element is a rightmost leaf.
 
* Space complexity : $$\mathcal{O}(N)$$ to keep `stack`.

## Accepted Submission (java)
```java
import java.util.*;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *	   int val;
 *	   TreeNode left;
 *	   TreeNode right;
 *	   TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
	private void inorder(TreeNode root, Queue<Integer> q) {
		if (root != null) {
			inorder(root.left, q);
			q.offer(root.val);
			inorder(root.right, q);
		}
	}

	public boolean isValidBST(TreeNode root) {
		Queue<Integer> q = new ArrayDeque<Integer>();
		inorder(root, q);
		boolean bl = true;
		Integer pi = q.poll();
		if (pi != null) {
			while (true) {
				Integer i = q.poll();
				if (i == null) {
					break;
				}
				if (i <= pi) {
					bl = false;
					break;
				}
				pi = i;
			}
		}
		return bl;
	}
}

```

## Top Discussions
### Learn one iterative inorder traversal, apply it to multiple tree questions (Java Solution)
- Author: issac3
- Creation Date: Sun May 22 2016 11:38:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 03:20:07 GMT+0800 (Singapore Standard Time)

<p>
I will show you all how to tackle various tree questions using iterative inorder traversal. First one is the standard iterative inorder traversal using stack. Hope everyone agrees with this solution. 

Question : [Binary Tree Inorder Traversal][1]

    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if(root == null) return list;
        Stack<TreeNode> stack = new Stack<>();
        while(root != null || !stack.empty()){
            while(root != null){
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            list.add(root.val);
            root = root.right;
            
        }
        return list;
    }

Now, we can use this structure to find the Kth smallest element in BST.

Question : [Kth Smallest Element in a BST][2]

     public int kthSmallest(TreeNode root, int k) {
         Stack<TreeNode> stack = new Stack<>();
         while(root != null || !stack.isEmpty()) {
             while(root != null) {
                 stack.push(root);    
                 root = root.left;   
             } 
             root = stack.pop();
             if(--k == 0) break;
             root = root.right;
         }
         return root.val;
     }

We can also use this structure to solve BST validation question. 

Question : [Validate Binary Search Tree][3]

    public boolean isValidBST(TreeNode root) {
       if (root == null) return true;
       Stack<TreeNode> stack = new Stack<>();
       TreeNode pre = null;
       while (root != null || !stack.isEmpty()) {
          while (root != null) {
             stack.push(root);
             root = root.left;
          }
          root = stack.pop();
          if(pre != null && root.val <= pre.val) return false;
          pre = root;
          root = root.right;
       }
       return true;
    }



  [1]: https://leetcode.com/problems/binary-tree-inorder-traversal/
  [2]: https://leetcode.com/problems/kth-smallest-element-in-a-bst/
  [3]: https://leetcode.com/problems/validate-binary-search-tree/
</p>


### My simple Java solution in 3 lines
- Author: sruzic
- Creation Date: Mon Jan 12 2015 19:29:13 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 14:13:34 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public boolean isValidBST(TreeNode root) {
            return isValidBST(root, Long.MIN_VALUE, Long.MAX_VALUE);
        }
        
        public boolean isValidBST(TreeNode root, long minVal, long maxVal) {
            if (root == null) return true;
            if (root.val >= maxVal || root.val <= minVal) return false;
            return isValidBST(root.left, minVal, root.val) && isValidBST(root.right, root.val, maxVal);
        }
    }

Basically what I am doing is recursively iterating over the tree while defining interval `<minVal, maxVal>` for each node which value must fit in.
</p>


### C++ in-order traversal, and please do not rely on buggy INT_MAX, INT_MIN solutions any more
- Author: jakwings
- Creation Date: Sun Nov 02 2014 06:27:41 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 18:34:48 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        bool isValidBST(TreeNode* root) {
            TreeNode* prev = NULL;
            return validate(root, prev);
        }
        bool validate(TreeNode* node, TreeNode* &prev) {
            if (node == NULL) return true;
            if (!validate(node->left, prev)) return false;
            if (prev != NULL && prev->val >= node->val) return false;
            prev = node;
            return validate(node->right, prev);
        }
    };

> Update:
> 
> If we use in-order traversal to serialize a binary search tree, we can
> get a list of values in ascending order. It can be proved with the
> definition of BST. And here I use the reference of TreeNode
> pointer `prev` as a global variable to mark the address of previous node in the
> list.
> 
> \u201cIn-order Traversal\u201d:
> [https://en.wikipedia.org/wiki/Tree_traversal#In-order]()

If you know what `INT_MAX` or `INT_MIN` is, then it is no excuse for your carelessness.
</p>


