---
title: "Sum of Two Integers"
weight: 354
#id: "sum-of-two-integers"
---
## Description
<div class="description">
<p>Calculate the sum of two integers <i>a</i> and <i>b</i>, but you are <b>not allowed</b> to use the operator <code>+</code> and <code>-</code>.</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>a = <span id="example-input-1-1">1</span>, b = <span id="example-input-1-2">2</span>
<strong>Output: </strong><span id="example-output-1">3</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>a = -<span id="example-input-2-1">2</span>, b = <span id="example-input-2-2">3</span>
<strong>Output: </strong>1
</pre>
</div>
</div>

</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Apple - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Hulu - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

Approach 1 is a detailed explanation of bit manipulation basics.
Approach 2 is a language-specific discussion of a possible follow-up 
and mainly written for fun.  

#### Approach 1: Bit Manipulation: Easy and Language-Independent

That's an extremely popular Facebook problem 
designed to check your knowledge of [bitwise operators](https://wiki.python.org/moin/BitwiseOperators):

$$
x \oplus y \qquad \textrm{that means} \qquad \textrm{bitwise XOR}  
$$

$$
x \& y \qquad \textrm{that means} \qquad \textrm{bitwise AND} 
$$

$$
\sim x \qquad \textrm{that means} \qquad \textrm{bitwise NOT} 
$$

**Reduce the Number of Use Cases**

First of all, there are too many use cases here:
both $$a$$ and $$b$$ could be positive or negative, 
`abs(a)` could be greater or less than `abs(b)`.
In total, that results in $$2 \times 2 \times 2 = 8$$ use cases.

Let's start by reducing the problem down to two simple cases:

- Sum of two positive integers: $$x + y$$, where $$x > y$$.

- Difference of two positive integers: $$x - y$$, where $$x > y$$.

<iframe src="https://leetcode.com/playground/Lzn2yfuu/shared" frameBorder="0" width="100%" height="500" name="Lzn2yfuu"></iframe>

**Interview Tip for Bit Manipulation Problems: Use XOR** 

How to start? There is an interview tip for bit manipulation problems:
if you don't know how to start, start from computing XOR for your input data.
Strangely, that helps out for quite a lot of problems,
[Single Number II](https://leetcode.com/articles/single-number-ii/),
[Single Number III](https://leetcode.com/articles/single-number-iii/),
[Maximum XOR of Two Numbers in an Array](https://leetcode.com/articles/maximum-xor-of-two-numbers-in-an-array/),
[Repeated DNA Sequences](https://leetcode.com/articles/repeated-dna-sequences/),
[Maximum Product of Word Lengths](https://leetcode.com/articles/maximum-product-of-word-lengths/),
etc.

> What is XOR?

XOR of zero and a bit results in that bit.

$$ 
0 \oplus x = x
$$

XOR of two equal bits (even if they are zeros) results in a zero.

$$ 
x \oplus x = 0
$$

**Sum of Two Positive Integers**

Now let's use this tip for the first use case: the sum of two positive integers. Here XOR is a key as well because it's a sum of two integers
in the binary form without taking carry into account.
In other words, XOR is a sum of bits of x and y where at 
least one of the bits is not set.

![fig](../Figures/371/answer_sum2.png) 

The next step is to find the carry. 
It contains the common set bits of x and y, shifted one bit to the left. 
_I.e._ it's logical AND of two input numbers, shifted one bit to the left:
$$\text{carry} = (x \& y) << 1$$. 

![fig](../Figures/371/carry.png)

The problem is reduced down to find the sum of the answer without carry and 
the carry.   

Technically, it's the same problem: to sum two numbers,
and hence one could solve it in a loop with the condition statement 
"while carry is not equal to zero". 

![fig](../Figures/371/sum.png)

**Difference of Two Positive Integers**

As for addition, XOR is a difference of two integers without taking borrow 
into account.

![fig](../Figures/371/answer_sub2.png)

The next step is to find the borrow. It contains common
set bits of $$y$$ and unset bits of $$x$$, 
_i.e._ $$\text{borrow} = ((\sim x) \& y) << 1$$.  

![fig](../Figures/371/borrow2.png)

The problem is reduced down to the subtraction of the borrow
from the answer without borrow. As for the sum, it could be solved recursively
or in a loop with the condition statement "while borrow is not equal to zero". 

![fig](../Figures/371/sub2.png)

**Algorithm**

- Simplify problem down to two cases: sum or subtraction of two 
positive integers: $$x \pm y$$, where $$x > y$$. Save down the sign of the result.

- If one has to compute the sum:

    - While carry is nonzero: `y != 0`:
    
        - Current answer without carry is XOR of x and y: `answer = x^y`.
        
        - Current carry is left-shifted AND of x and y: `carry = (x & y) << 1`.
        
        - Job is done, prepare the next loop: `x = answer`, `y = carry`.
        
    - Return `x * sign`.
    
- If one has to compute the difference:

    - While borrow is nonzero: `y != 0`:
    
        - Current answer without borrow is XOR of x and y: `answer = x^y`.
        
        - Current borrow is left-shifted AND of NOT x and y: `borrow = ((~x) & y) << 1`.
        
        - Job is done, prepare the next loop: `x = answer`, `y = borrow`.
        
    - Return `x * sign`.

**Implementation**

<iframe src="https://leetcode.com/playground/WndKZTN5/shared" frameBorder="0" width="100%" height="500" name="WndKZTN5"></iframe>

This solution could be written a bit shorter in Python:

<iframe src="https://leetcode.com/playground/7qJMqJqN/shared" frameBorder="0" width="100%" height="361" name="7qJMqJqN"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(1)$$ because each integer contains $$32$$ bits.

* Space complexity: $$\mathcal{O}(1)$$ because we don't use any additional
data structures.

<br />
<br />


---
#### Approach 2: Bit Manipulation: Short Language-Specific Solution

Approach 1 is easy to attack during the follow-up:

> Please don't use multiplication to manage negative numbers and make a clean bitwise solution.

Let's be honest, it's a trap. 
Once you start to manage negative numbers using bit manipulation, your solution becomes
_language-specific_. 

**Different languages represent negative numbers differently.** 

**Java**

For example, Java integer is a number of 32 bits. 
31 bits are used for the value. 
The first bit is used for the sign: if it's equal to 1, the number is negative,
if it's equal to 0, the number is positive. 

And now the fun starts. Does it mean that 

$$1 = (\underbrace{0}_\text{positive}\underbrace{00000..0}_\text{30 times}1)_2$$ 

and 

$$ -1 = (\underbrace{1}_\text{negative}\underbrace{00000..0}_\text{30 times}1)_2$$? 

No!

For the representation of a negative number Java uses the so-called "two's complement":

$$ -1 = (\underbrace{1}_\text{negative}\underbrace{11111..1}_\text{30 times}1)_2$$

The idea is simple:

$$
(- 1 + 1) \& \underbrace{(111111..1)_2}_\text{32 1-bits} = 0
$$

$$
(- x + x) \& \underbrace{(111111..1)_2}_\text{32 1-bits} = 0 
$$

The main goal of "two's complement" is to decrease the complexity of bit manipulations.
How does Java compute "two's complement" and manage 32-bits limit? Here is how:

- After each operation we have an invisible `& mask`, where `mask = 0xFFFFFFFF`, _i.e._
bitmask of 32 1-bits.

- The overflow, _i.e._ the situation of `x > 0x7FFFFFFF` (bitmask of 31 1-bits), is managed  as `x --> ~(x ^ 0xFFFFFFFF)`.

At this point, we could come back to approach 1 and, surprisingly, 
all management of negative numbers, signs, and subtractions Java already 
does for us. That simplifies the solution to the 
computation of a sum of two positive integers. That's how the
magic of "two's complement" works! 

<iframe src="https://leetcode.com/playground/J4PEDChj/shared" frameBorder="0" width="100%" height="259" name="J4PEDChj"></iframe>

**Python**

Now let's go back to real life. 
Python has no 32-bits limit, and hence its representation of negative
integers is entirely different. 

There is no Java magic by default, and if you need a magic - 
just do it:

- After each operation we have an invisible `& mask`, where `mask = 0xFFFFFFFF`, _i.e._
bitmask of 32 1-bits.

- The overflow, _i.e._ the situation of `x > 0x7FFFFFFF` (bitmask of 31 1-bits), is managed  as `x --> ~(x ^ 0xFFFFFFFF)`.

<iframe src="https://leetcode.com/playground/Luqs4gN8/shared" frameBorder="0" width="100%" height="208" name="Luqs4gN8"></iframe>

**Implementation**
 
Each language has its beauty.

<iframe src="https://leetcode.com/playground/B7ks9HXR/shared" frameBorder="0" width="100%" height="310" name="B7ks9HXR"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(1)$$. 

* Space complexity: $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A summary: how to use bit manipulation to solve problems easily and efficiently
- Author: LHearen
- Creation Date: Wed Jul 06 2016 14:33:10 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 14:51:20 GMT+0800 (Singapore Standard Time)

<p>
### Wiki
Bit manipulation is the act of algorithmically manipulating bits or other pieces of data shorter than a word. Computer programming tasks that require bit manipulation include low-level device control, error detection and correction algorithms, data compression, encryption algorithms, and optimization. For most other tasks, modern programming languages allow the programmer to work directly with abstractions instead of bits that represent those abstractions. Source code that does bit manipulation makes use of the bitwise operations: AND, OR, XOR, NOT, and bit shifts.

Bit manipulation, in some cases, can obviate or reduce the need to loop over a data structure and can give many-fold speed ups, as bit manipulations are processed in parallel, but the code can become more difficult to write and maintain. 

### Details

#### Basics
At the heart of bit manipulation are the bit-wise operators & (and), | (or), ~ (not) and ^ (exclusive-or, xor) and shift operators a << b and a >> b. 

> There is no boolean operator counterpart to bitwise exclusive-or, but there is a simple explanation. The exclusive-or operation takes two inputs and returns a 1 if either one or the other of the inputs is a 1, but not if both are. That is, if both inputs are 1 or both inputs are 0, it returns 0. Bitwise exclusive-or, with the operator of a caret, ^, performs the exclusive-or operation on each pair of bits. Exclusive-or is commonly abbreviated XOR. 

- Set union A | B
- Set intersection A & B
- Set subtraction A & ~B
- Set negation ALL_BITS ^ A or ~A
- Set bit A |= 1 << bit
- Clear bit A &= ~(1 << bit)
- Test bit (A & 1 << bit) != 0
- Extract last bit A&-A or A&~(A-1) or x^(x&(x-1)) 
- Remove last bit A&(A-1)
- Get all 1-bits ~0

#### Examples
Count the number of ones in the binary representation of the given number
```
int count_one(int n) {
    while(n) {
        n = n&(n-1);
        count++;
    }
    return count;
}
```

Is power of four (actually map-checking, iterative and recursive methods can do the same)
```
bool isPowerOfFour(int n) {
    return !(n&(n-1)) && (n&0x55555555);
    //check the 1-bit location;
}
```

#### `^` tricks
Use `^` to remove even exactly same numbers and save the odd, or save the distinct bits and remove the same.
##### Sum of Two Integers
Use `^` and `&` to add two integers
```
int getSum(int a, int b) {
    return b==0? a:getSum(a^b, (a&b)<<1); //be careful about the terminating condition;
}
```
##### Missing Number
Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, find the one that is missing from the array.  For example, Given nums = [0, 1, 3] return 2. (Of course, you can do this by math.)
```
int missingNumber(vector<int>& nums) {
    int ret = 0;
    for(int i = 0; i < nums.size(); ++i) {
        ret ^= i;
        ret ^= nums[i];
    }
    return ret^=nums.size();
}
```

#### `|` tricks
Keep as many 1-bits as possible 

Find the largest power of 2 (most significant bit in binary form), which is less than or equal to the given number N.
```
long largest_power(long N) {
    //changing all right side bits to 1.
    N = N | (N>>1);
    N = N | (N>>2);
    N = N | (N>>4);
    N = N | (N>>8);
    N = N | (N>>16);
    return (N+1)>>1;
}
```

##### Reverse Bits
Reverse bits of a given 32 bits unsigned integer.
###### Solution
```
uint32_t reverseBits(uint32_t n) {
    unsigned int mask = 1<<31, res = 0;
    for(int i = 0; i < 32; ++i) {
        if(n & 1) res |= mask;
        mask >>= 1;
        n >>= 1;
    }
    return res;
}
```
```
uint32_t reverseBits(uint32_t n) {
	uint32_t mask = 1, ret = 0;
	for(int i = 0; i < 32; ++i){
		ret <<= 1;
		if(mask & n) ret |= 1;
		mask <<= 1;
	}
	return ret;
}
```
#### `&` tricks
Just selecting certain bits

Reversing the bits in integer
```
x = ((x & 0xaaaaaaaa) >> 1) | ((x & 0x55555555) << 1);
x = ((x & 0xcccccccc) >> 2) | ((x & 0x33333333) << 2);
x = ((x & 0xf0f0f0f0) >> 4) | ((x & 0x0f0f0f0f) << 4);
x = ((x & 0xff00ff00) >> 8) | ((x & 0x00ff00ff) << 8);
x = ((x & 0xffff0000) >> 16) | ((x & 0x0000ffff) << 16);
```
##### Bitwise AND of Numbers Range
Given a range [m, n] where 0 <= m <= n <= 2147483647, return the bitwise AND of all numbers in this range, inclusive.  For example, given the range [5, 7], you should return 4.
###### Solution
```
int rangeBitwiseAnd(int m, int n) {
    int a = 0;
    while(m != n) {
        m >>= 1;
        n >>= 1;
        a++;
    }
    return m<<a; 
}
```
##### Number of 1 Bits
Write a function that takes an unsigned integer and returns the number of \u20191' bits it has (also known as the Hamming weight).
###### Solution
```
int hammingWeight(uint32_t n) {
	int count = 0;
	while(n) {
		n = n&(n-1);
		count++;
	}
	return count;
}
```

```
int hammingWeight(uint32_t n) {
    ulong mask = 1;
    int count = 0;
    for(int i = 0; i < 32; ++i){ //31 will not do, delicate;
        if(mask & n) count++;
        mask <<= 1;
    }
    return count;
}
```

#### Application
##### Repeated DNA Sequences
All DNA is composed of a series of nucleotides abbreviated as A, C, G, and T, for example: "ACGAATTCCG". When studying DNA, it is sometimes useful to identify repeated sequences within the DNA.  Write a function to find all the 10-letter-long sequences (substrings) that occur more than once in a DNA molecule.  
For example, 
Given s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT", 
Return: ["AAAAACCCCC", "CCCCCAAAAA"].
###### Solution
```
class Solution {
public:
    vector<string> findRepeatedDnaSequences(string s) {
        int sLen = s.length();
        vector<string> v;
        if(sLen < 11) return v;
        char keyMap[1<<21]{0};
        int hashKey = 0;
        for(int i = 0; i < 9; ++i) hashKey = (hashKey<<2) | (s[i]-'A'+1)%5;
        for(int i = 9; i < sLen; ++i) {
            if(keyMap[hashKey = ((hashKey<<2)|(s[i]-'A'+1)%5)&0xfffff]++ == 1)
                v.push_back(s.substr(i-9, 10));
        }
        return v;
    }
};
```
> But the above solution can be invalid when repeated sequence appears too many times, in which case we should use `unordered_map<int, int> keyMap` to replace `char keyMap[1<<21]{0}`here.
##### Majority Element
Given an array of size n, find the majority element. The majority element is the element that appears more than \u230a n/2 \u230b times. (bit-counting as a usual way, but here we actually also can adopt sorting and Moore Voting Algorithm)
###### Solution
```
int majorityElement(vector<int>& nums) {
    int len = sizeof(int)*8, size = nums.size();
    int count = 0, mask = 1, ret = 0;
    for(int i = 0; i < len; ++i) {
        count = 0;
        for(int j = 0; j < size; ++j)
            if(mask & nums[j]) count++;
        if(count > size/2) ret |= mask;
        mask <<= 1;
    }
    return ret;
}
```
##### Single Number III
Given an array of integers, every element appears three times except for one. Find that single one. (Still this type can be solved by bit-counting easily.) But we are going to solve it by `digital logic design`
###### Solution
```
//inspired by logical circuit design and boolean algebra;
//counter - unit of 3;
//current   incoming  next
//a b            c    a b
//0 0            0    0 0
//0 1            0    0 1
//1 0            0    1 0
//0 0            1    0 1
//0 1            1    1 0
//1 0            1    0 0
//a = a&~b&~c + ~a&b&c;
//b = ~a&b&~c + ~a&~b&c;
//return a|b since the single number can appear once or twice;
int singleNumber(vector<int>& nums) {
    int t = 0, a = 0, b = 0;
    for(int i = 0; i < nums.size(); ++i) {
        t = (a&~b&~nums[i]) | (~a&b&nums[i]);
        b = (~a&b&~nums[i]) | (~a&~b&nums[i]);
        a = t;
    }
    return a | b;
}
;
```
##### Maximum Product of Word Lengths
Given a string array words, find the maximum value of length(word[i]) * length(word[j]) where the two words do not share common letters. You may assume that each word will contain only lower case letters. If no such two words exist, return 0.

> Example 1:
Given ["abcw", "baz", "foo", "bar", "xtfn", "abcdef"]
Return 16
The two words can be "abcw", "xtfn".

> Example 2:
Given ["a", "ab", "abc", "d", "cd", "bcd", "abcd"]
Return 4
The two words can be "ab", "cd".

> Example 3:
Given ["a", "aa", "aaa", "aaaa"]
Return 0
No such pair of words.

###### Solution
Since we are going to use the length of the word very frequently and we are to compare the letters of two words checking whether they have some letters in common:
- using an array of int to pre-store the length of each word reducing the frequently *measuring* process; 
- since int has 4 bytes, a 32-bit type, and there are only 26 different letters, so we can just use one bit to indicate the existence of the letter in a word.
```
int maxProduct(vector<string>& words) {
    vector<int> mask(words.size());
    vector<int> lens(words.size());
    for(int i = 0; i < words.size(); ++i) lens[i] = words[i].length();
    int result = 0;
    for (int i=0; i<words.size(); ++i) {
        for (char c : words[i])
            mask[i] |= 1 << (c - 'a');
        for (int j=0; j<i; ++j)
            if (!(mask[i] & mask[j]))
                result = max(result, lens[i]*lens[j]);
    }
    return result;
}
```

#### Attention

- result after shifting left(or right) too much is undefined
- right shifting operations on negative values are undefined
- right operand in shifting should be non-negative, otherwise the result is undefined
- The & and | operators have lower precedence than comparison operators

### Sets
All the subsets
A big advantage of bit manipulation is that it is trivial to iterate over all the subsets of an N-element set: every N-bit value represents some subset. Even better, `if A is a subset of B then the number representing A is less than that representing B`, which is convenient for some dynamic programming solutions.

It is also possible to iterate over all the subsets of a particular subset (represented by a bit pattern), provided that you don\u2019t mind visiting them in reverse order (if this is problematic, put them in a list as they\u2019re generated, then walk the list backwards). The trick is similar to that for finding the lowest bit in a number. If we subtract 1 from a subset, then the lowest set element is cleared, and every lower element is set. However, we only want to set those lower elements that are in the superset. So the iteration step is just `i = (i - 1) & superset`.

```
vector<vector<int>> subsets(vector<int>& nums) {
    vector<vector<int>> vv;
    int size = nums.size(); 
    if(size == 0) return vv;
    int num = 1 << size;
    vv.resize(num);
    for(int i = 0; i < num; ++i) {
        for(int j = 0; j < size; ++j)
            if((1<<j) & i) vv[i].push_back(nums[j]);   
    }
    return vv;
}
```
Actually there are two more methods to handle this using `recursion` and `iteration` respectively.

### Bitset
A [bitset](http://www.cplusplus.com/reference/bitset/bitset/?kw=bitset) stores bits (elements with only two possible values: 0 or 1, true or false, ...).
The class emulates an array of bool elements, but optimized for space allocation: generally, each element occupies only one bit (which, on most systems, is eight times less than the smallest elemental type: char).
```
// bitset::count
#include <iostream>       // std::cout
#include <string>         // std::string
#include <bitset>         // std::bitset

int main () {
  std::bitset<8> foo (std::string("10110011"));
  std::cout << foo << " has ";
  std::cout << foo.count() << " ones and ";
  std::cout << (foo.size()-foo.count()) << " zeros.\
";
  return 0;
}
```

Always welcom new ideas and `practical` tricks, just leave them in the comments!
</p>


### Simple explanation on how to arrive at the solution 
- Author: kishynivas10
- Creation Date: Sun May 20 2018 23:17:43 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 13:49:21 GMT+0800 (Singapore Standard Time)

<p>
There\'s lot of answers here, but none of them shows how they arrived at the answer, here\'s my simple try to explain. 

Eg: Let\'s try this with our hand  3 + 2 = 5 , the carry will be with in the brackets i.e "()" 
```
3 => 011 
2=>  010
     ____
     1(1)01
```

Here we will forward the carry  at the second bit to get the result.
So which bitwise operator can do this ? A simple observation says that XOR can do that,but it just falls short in dealing with the carry properly, but correctly adds when there is no need to deal with carry.
For Eg: 
```
1   =>  001 
2   =>  010 
1^2 =>  011 (2+1 = 3) 
```

So now when we have carry, to deal with, we can see the result as : 
```
3  => 011 
2  => 010 
3^2=> 001  
```
Here we can see  XOR just fell  short with the carry  generated at the second bit. 
So how can we find the carry ? The carry is generated when both the bits are set, i.e (1,1) will generate carry but (0,1 or 1,0 or 0,0) won\'t generate a carry, so which bitwise operator can do that ?  AND gate ofcourse. 

To find the carry we can do 
```
3    =>  011 
2    =>  010 
3&2  =>  010
```
now we need to add it to the previous value we generated i.e ( 3 ^ 2), but the carry should be added to the left bit of the one which genereated it. 
so we left shift it by one so that it gets added at the right spot. 

Hence  (3&2)<<1  => 100 
so we can now do 
```
3 ^2        =>  001 
(3&2)<<1    =>  100 

Now xor them, which will give 101(5) , we can continue this until the carry becomes zero.

```
A Java program which implements the above logic :

```
class Solution {
    public int getSum(int a, int b) {
      int c; 
      while(b !=0 ) {
        c = (a&b);
        a = a ^ b;
        b = (c)<<1;
      }
      return a;
        
    }
}
```
Hope I made some sense, If not I am really sorry for wasting your time.
</p>


### Java simple easy understand solution with explanation
- Author: zhaolz
- Creation Date: Thu Jun 30 2016 09:29:26 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 05:09:40 GMT+0800 (Singapore Standard Time)

<p>
I have been confused about bit manipulation for a very long time. So I decide to do a summary about it here.

"&" AND operation, for example, 2 (0010) & 7 (0111) => 2 (0010)

"^" XOR operation, for example, 2 (0010) ^ 7 (0111) => 5 (0101)

"~" NOT operation, for example, ~2(0010) => -3 (1101) what??? Don't get frustrated here. It's called two's complement.

1111 is -1, in two's complement

1110 is -2, which is ~2 + 1, ~0010 => 1101, 1101 + 1 = 1110 => 2 

1101 is -3, which is ~3 + 1

so if you want to get a negative number, you can simply do ~x + 1

Reference:

[https://en.wikipedia.org/wiki/Two%27s_complement][1]

[https://www.cs.cornell.edu/~tomf/notes/cps104/twoscomp.html][2]

For this, problem, for example, we have a = 1, b = 3,

In bit representation, a = 0001, b = 0011,

First, we can use "and"("&") operation between a and b to find a carry.

carry = a & b, then carry = 0001

Second, we can use "xor" ("^") operation between a and b to find the different bit, and assign it to a, 

Then, we shift carry one position left and assign it to b, b = 0010.

Iterate until there is no carry (or b == 0)

    // Iterative
    public int getSum(int a, int b) {
		if (a == 0) return b;
		if (b == 0) return a;

		while (b != 0) {
			int carry = a & b;
			a = a ^ b;
			b = carry << 1;
		}
		
		return a;
    }

    // Iterative
    public int getSubtract(int a, int b) {
		while (b != 0) {
			int borrow = (~a) & b;
			a = a ^ b;
			b = borrow << 1;
		}
		
		return a;
	}

    // Recursive
	public int getSum(int a, int b) {
		return (b == 0) ? a : getSum(a ^ b, (a & b) << 1);
	}

	// Recursive
	public int getSubtract(int a, int b) {
		return (b == 0) ? a : getSubtract(a ^ b, (~a & b) << 1);
	}
	
	// Get negative number
	public int negate(int x) {
		return ~x + 1;
	}


  [1]: https://en.wikipedia.org/wiki/Two%27s_complement
  [2]: https://www.cs.cornell.edu/~tomf/notes/cps104/twoscomp.html
</p>


