---
title: "Minimum Number of Vertices to Reach All Nodes"
weight: 1408
#id: "minimum-number-of-vertices-to-reach-all-nodes"
---
## Description
<div class="description">
<p>Given a<strong>&nbsp;directed acyclic graph</strong>,&nbsp;with&nbsp;<code>n</code>&nbsp;vertices numbered from&nbsp;<code>0</code>&nbsp;to&nbsp;<code>n-1</code>,&nbsp;and an array&nbsp;<code>edges</code>&nbsp;where&nbsp;<code>edges[i] = [from<sub>i</sub>, to<sub>i</sub>]</code>&nbsp;represents a directed edge from node&nbsp;<code>from<sub>i</sub></code>&nbsp;to node&nbsp;<code>to<sub>i</sub></code>.</p>

<p>Find <em>the smallest set of vertices from which all nodes in the graph are reachable</em>. It&#39;s guaranteed that a unique solution exists.</p>

<p>Notice that you can return the vertices in any order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/07/07/untitled22.png" style="width: 231px; height: 181px;" /></p>

<pre>
<strong>Input:</strong> n = 6, edges = [[0,1],[0,2],[2,5],[3,4],[4,2]]
<strong>Output:</strong> [0,3]
<b>Explanation: </b>It&#39;s not possible to reach all the nodes from a single vertex. From 0 we can reach [0,1,2,5]. From 3 we can reach [3,4,2,5]. So we output [0,3].</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/07/07/untitled.png" style="width: 201px; height: 201px;" /></p>

<pre>
<strong>Input:</strong> n = 5, edges = [[0,1],[2,1],[3,1],[1,4],[2,4]]
<strong>Output:</strong> [0,2,3]
<strong>Explanation: </strong>Notice that vertices 0, 3 and 2 are not reachable from any other node, so we must include them. Also any of these vertices can reach nodes 1 and 4.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 10^5</code></li>
	<li><code>1 &lt;= edges.length &lt;= min(10^5, n * (n - 1) / 2)</code></li>
	<li><code>edges[i].length == 2</code></li>
	<li><code>0 &lt;= from<sub>i,</sub>&nbsp;to<sub>i</sub> &lt; n</code></li>
	<li>All pairs <code>(from<sub>i</sub>, to<sub>i</sub>)</code> are distinct.</li>
</ul>
</div>

## Tags
- Graph (graph)

## Companies
- Google - 0 (taggedByAdmin: true)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Nodes with no In-Degree
- Author: lee215
- Creation Date: Sun Aug 23 2020 00:01:09 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 14 2020 15:05:39 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Just return the nodes with no in-degres.
<br>

# **Explanation**
Quick prove:
1. All nodes with no in-degree must in the final result,
because they can not be reached from any other nodes.
2. All other nodes can be reached from some other nodes.
<br>

# **Complexity**
Time `O(E)`
Space `O(N)`
<br>

**Java:**
```java
    public List<Integer> findSmallestSetOfVertices(int n, List<List<Integer>> edges) {
        List<Integer> res = new ArrayList<>();
        int[] seen = new int[n];
        for (List<Integer> e: edges)
            seen[e.get(1)] = 1;
        for (int i = 0; i < n; ++i)
            if (seen[i] == 0)
                res.add(i);
        return res;
    }
```

**C++:**
```cpp
    vector<int> findSmallestSetOfVertices(int n, vector<vector<int>>& edges) {
        vector<int> res, seen(n);
        for (auto& e: edges)
            seen[e[1]] = 1;
        for (int i = 0; i < n; ++i)
            if (seen[i] == 0)
                res.push_back(i);
        return res;
    }
```

**Python:**
```py
    def findSmallestSetOfVertices(self, n, edges):
        return list(set(range(n)) - set(j for i, j in edges))
```

</p>


### Vertices with 0 In-Degree - Easy to Understand
- Author: karprabin1
- Creation Date: Sun Aug 23 2020 00:10:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 23 2020 13:44:10 GMT+0800 (Singapore Standard Time)

<p>
Idea:

If any vertices don\'t have incident edge from any other vertex so for covering that vertex we have to cosider that vertex.
Just create `inDegree` array of all vertices and choose all vertex  inDegree =  0.

```
class Solution {
    public List<Integer> findSmallestSetOfVertices(int n, List<List<Integer>> edges) {
        int[] inDegree = new int[n];
        for(List<Integer> e : edges) {
            inDegree[e.get(1)]++;
        }
        
        List<Integer> result = new ArrayList();
        for(int i = 0; i < n; i++)
            if(inDegree[i] == 0)
                result.add(i);
        
        return result;
    }
}
```

`TC - O(E), E - edges.length`
`SC- O(n)`

**Detailed Video Explanation** https://www.youtube.com/watch?v=wpRvIzZA2Gs

If you like post **upvote**.
</p>


### c++ super simple 6-liner
- Author: apluscs
- Creation Date: Sun Aug 23 2020 00:02:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 23 2020 00:02:24 GMT+0800 (Singapore Standard Time)

<p>
Basically find all the nodes with in-degree = 0. Because you can\'t get to these nodes from anywhere, and once you have all of these you can visit the next level of nodes, and so on.


```
class Solution {
public:
  vector<int> findSmallestSetOfVertices(int n, vector<vector<int>>& edges) {
    vector<int> res, in(n);
    for(auto edge: edges)
      in[edge[1]]++;
    for(int i=0; i!=n; ++i)
      if(in[i]==0) res.push_back(i);
    return res;
  }
};
```
</p>


