---
title: "Number of Days Between Two Dates"
weight: 1139
#id: "number-of-days-between-two-dates"
---
## Description
<div class="description">
<p>Write a program to count the number of days between two dates.</p>

<p>The two dates are given as strings, their format is <code>YYYY-MM-DD</code>&nbsp;as shown in the examples.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> date1 = "2019-06-29", date2 = "2019-06-30"
<strong>Output:</strong> 1
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> date1 = "2020-01-15", date2 = "2019-12-31"
<strong>Output:</strong> 15
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The given dates are valid&nbsp;dates between the years <code>1971</code> and <code>2100</code>.</li>
</ul>

</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Calling API, or Not, That is the Question
- Author: Admin007
- Creation Date: Sun Feb 23 2020 12:05:14 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 24 2020 16:17:17 GMT+0800 (Singapore Standard Time)

<p>
I am really really curious is this kind of question related to any algorithm technique??? Or just to check whether we know and call API to calculate the result...

If you choose not to call the API, then you will lose your time during the interview...

I think this problem is even worse than the Remove Palindromic Subsequence problem, which the a kind of BRAINTEASER but it is fine to me.

Anyways, I give my solutions by both ways to make this post more useful after you read :)

**Solution 1 with Time-API:**
```
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

class Solution {
    public int daysBetweenDates(String date1, String date2) {
	    return Math.abs((int)ChronoUnit.DAYS.between(LocalDate.parse(date1), LocalDate.parse(date2)));
    }
}
```

**Solution 2 with No Time-API:**
```
    static int monthDays[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public int daysBetweenDates(String date1, String date2) {
        int[] d1 = stringDateConvertor(date1);
        int[] d2 = stringDateConvertor(date2);
        
        int date1Days = d1[0] * 365 + d1[2]; 
  
        // Add number of days for the given month  
        for (int i = 0; i < d1[1] - 1; i++)  
        { 
            date1Days += monthDays[i]; 
        } 
  
        // Since each leap year has 366 days, 
        // Add an extra day for every leap year we meet. 
        date1Days += countLeapYear(d1[0], d1[1]); 
  
        int date2Days = d2[0] * 365 + d2[2]; 
        for (int i = 0; i < d2[1] - 1; i++) 
        { 
            date2Days += monthDays[i]; 
        } 
        date2Days += countLeapYear(d2[0], d2[1]); 
  
        // Return number of days between Two dates. 
        return Math.abs(date1Days - date2Days); 
    }
    
    public int[] stringDateConvertor(String date) {
        int[] dateTrans = new int[3];
        
        String[] d = date.split("-");
        dateTrans[0] = Integer.valueOf(d[0]);
        dateTrans[1] = Integer.valueOf(d[1]);
        dateTrans[2] = Integer.valueOf(d[2]);
        
        return dateTrans;
    }
    
    public int countLeapYear(int year, int month) {
        // If the current year not reach to Feb., then we do not need to consider it
        // for the count of leap years.
        if (month <= 2)  
        { 
            year--; 
        } 
  
        // A leap year is a multiple of 4, multiple of 400 BUT not a multiple of 100. 
        return year / 4 - year / 100 + year / 400; 
    }
```

**Solution 2.5: Write a Function to Count the day from year of 1971**
```
	public int daysBetweenDates(String date1, String date2) {    
			return Math.abs(countSince1971(date1) - countSince1971(date2));
    }

    public int countSince1971(String date) {
        int[] monthDays = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        String[] data = date.split("-");
        
        int year = Integer.parseInt(data[0]);
        int month = Integer.parseInt(data[1]);
        int day = Integer.parseInt(data[2]);
        
        for (int i = 1971; i < year; i++) {
            day += isALeapYear(i) ? 366 : 365;
        }
        for (int i = 1; i < month; i++) {
            if (isALeapYear(year) && i == 2) {
                day += 1;
            } 
            day += monthDays[i];
        }
        return day;
    }

    public boolean isALeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    }
```
</p>


### Similar to day of the year
- Author: votrubac
- Creation Date: Sun Feb 23 2020 12:08:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 13:31:19 GMT+0800 (Singapore Standard Time)

<p>
This solution is based on [1154. Day of the Year](https://leetcode.com/problems/day-of-the-year/discuss/355916/C%2B%2B-Number-of-Days-in-a-Month).

Here, we also need to add days from year 1971 till the previous year. In the end, we return the absolute difference in number of days between two input dates.
```
int days[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
int daysBetweenDates(string d1, string d2) {
    return abs(daysFrom1971(d1) - daysFrom1971(d2));
}
bool isLeap(int y) { 
    return y % 4 == 0 && (y % 100 != 0 || y % 400 == 0); 
}
int daysFrom1971(string dt) {
    int y = stoi(dt.substr(0, 4)), m = stoi(dt.substr(5, 2)), d = stoi(dt.substr(8));
    for (int iy = 1971; iy < y; ++iy) 
        d += isLeap(iy) ? 366 : 365;
    return d + (m > 2 && isLeap(y)) + accumulate(begin(days), begin(days) + m - 1, 0);
}  
```
</p>


### [Python] Magical Formula
- Author: orangezeit
- Creation Date: Sun Feb 23 2020 12:02:53 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 24 2020 06:38:28 GMT+0800 (Singapore Standard Time)

<p>
When ```m=1``` or ```m=2``` (January or February), we let ```m=13``` or ```m=14``` and let ```y``` decreased by 1. Imagine it is 13th or 14th month of the last year. By doing that, we let the magical formula also work for those two months. ```(153 * m + 8) // 5``` is just a carefully designed way to record the days of each month. More specifically, it is designed to record the difference of days between two months. Suppose we have March 1st and April 1st, ```(153 * 3 + 8) // 5 = 93``` while ```(153 * 4 + 8) // 5 = 124```, the difference is 31 which is the number of days in March. Suppose we have April 1st to May 1st, ```(153 * 4 + 8) // 5 = 124``` and ```(153 * 5 + 8) // 5 = 154```, the difference is now 30 which is the number of days in April. You can also check other months.

I learned this formula somewhere else before. It is not something to come up with in minutes.

```python
class Solution:
    
    def daysBetweenDates(self, date1: str, date2: str) -> int:
        def f(date):
            y, m, d = map(int, date.split(\'-\'))
            if m < 3:
                m += 12
                y -= 1
            return 365 * y + y // 4 + y // 400 - y // 100 + d + (153 * m + 8) // 5
        
        return abs(f(date1) - f(date2))
```
</p>


