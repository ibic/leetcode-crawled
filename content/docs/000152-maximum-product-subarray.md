---
title: "Maximum Product Subarray"
weight: 152
#id: "maximum-product-subarray"
---
## Description
<div class="description">
<p>Given an integer array&nbsp;<code>nums</code>, find the contiguous subarray within an array (containing at least one number) which has the largest product.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> [2,3,-2,4]
<strong>Output:</strong> <code>6</code>
<strong>Explanation:</strong>&nbsp;[2,3] has the largest product 6.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> [-2,0,-1]
<strong>Output:</strong> 0
<strong>Explanation:</strong>&nbsp;The result cannot be 2, because [-2,-1] is not a subarray.</pre>

</div>

## Tags
- Array (array)
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 8 (taggedByAdmin: false)
- Amazon - 7 (taggedByAdmin: false)
- Facebook - 7 (taggedByAdmin: false)
- Bloomberg - 6 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- LinkedIn - 7 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Akuna Capital - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

It is advisable to approach [Maximum Subarray](https://leetcode.com/problems/maximum-subarray/) problem first before approaching this problem. The intuition acquired from that problem will help a lot with this problem.

---

#### Approach 1: Brute Force

**Intuition**

The most naive way to tackle this problem is to go through each element in `nums`, and for each element, consider the product of every a contiguous subarray starting from that element. This will result in a cubic runtime. 

    for i in [0...nums-1]: 
        for j in [i..nums-1]: 
            accumulator = 1
            for k in [i..j]:
                accumulator = accumulator * nums[k]
            result = max(result, accumulator)

We can improve the runtime from cubic to quadratic by removing the innermost `for` loop in the above pseudo code. Rather than calculating the product of every contiguous subarray over and over again, for each element in `nums` (the outermost `for` loop), we accumulate the products of contiguous subarrays starting from that element to subsequent elements as we go through them (the second `for` loop). By doing so, we only need to multiply the current number with accumulated product to get the product of numbers up to the current number.

**Implementation**

<iframe src="https://leetcode.com/playground/TxvynHcJ/shared" frameBorder="0" width="100%" height="344" name="TxvynHcJ"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N^2)$$ where $$N$$ is the size of `nums`. Since we are checking every possible contiguous subarray following every element in `nums` we have quadratic runtime.

* Space complexity : $$O(1)$$ since we are not consuming additional space other than two variables: `result` to hold the final result and `accu` to accumulate product of preceding contiguous subarrays.

---

#### Approach 2: Dynamic Programming

**Intuition**

Rather than looking for every possible subarray to get the largest product, we can scan the array and solve smaller subproblems.

Let's see this problem as a problem of getting the highest combo chain. The way combo chains work is that they build on top of the previous combo chains that you have acquired. The simplest case is when the numbers in `nums` are all positive numbers. In that case, you would only need to keep on multiplying the accumulated result to get a bigger and bigger combo chain as you progress.

However, two things can disrupt your combo chain:

- Zeros
- Negative numbers

**Zeros** will reset your combo chain. A high score which you have achieved will be recorded in placeholder `result`. You will have to *restart* your combo chain after zero. If you encounter another combo chain which is higher than the recorded high score in `result`, you just need to update the `result`.

**Negative numbers** are a little bit tricky. A single negative number can flip the largest combo chain to a very small number. This may sound like your combo chain has been completely disrupted but if you encounter another negative number, your combo chain can be saved. Unlike zero, you still have a hope of saving your combo chain as long as you have another negative number in `nums` (Think of this second negative number as an antidote for the poison that you just consumed). However, if you encounter a zero while you are looking your another negative number to save your combo chain, you lose the hope of saving that combo chain.

While going through numbers in `nums`, we will have to keep track of the maximum product up to that number (we will call `max_so_far`) and minimum product up to that number (we will call `min_so_far`). The reason behind keeping track of `max_so_far` is to keep track of the accumulated product of positive numbers. The reason behind keeping track of `min_so_far` is to properly handle negative numbers.

`max_so_far` is updated by taking the **maximum** value among:
    
1. Current number.
    * This value will be picked if the accumulated product has been really bad (even compared to the current number). This can happen when the current number has a preceding zero (e.g. `[0,4]`) or is preceded by a single negative number (e.g. `[-3,5]`).
2. Product of last `max_so_far` and current number.
    * This value will be picked if the accumulated product has been steadily increasing (all positive numbers).
3. Product of last `min_so_far` and current number.
    * This value will be picked if the current number is a negative number and the combo chain has been disrupted by a single negative number before (In a sense, this value is like an antidote to an already poisoned combo chain).

`min_so_far` is updated in using the same three numbers except that we are taking **minimum**  among the above three numbers.

In the animation below, you will observe a negative number `-5` disrupting a combo chain but that combo chain is later saved by another negative number `-4`. The only reason this can be saved is because of `min_so_far`. You will also observe a zero disrupting a combo chain. 

!?!../Documents/152_maximum_product_subarray.json:1200,600!?!

**Implementation**


<iframe src="https://leetcode.com/playground/gQYpEw94/shared" frameBorder="0" width="100%" height="412" name="gQYpEw94"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N)$$ where $$N$$ is the size of `nums`. The algorithm achieves linear runtime since we are going through `nums` only once. 

* Space complexity : $$O(1)$$ since no additional space is consumed rather than variables which keep track of the maximum product so far, the minimum product so far, current variable, temp variable, and placeholder variable for the result.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Possibly simplest solution with O(n) time complexity
- Author: mzchen
- Creation Date: Sun Oct 26 2014 01:13:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 10:57:06 GMT+0800 (Singapore Standard Time)

<p>
```
int maxProduct(int A[], int n) {
    // store the result that is the max we have found so far
    int r = A[0];

    // imax/imin stores the max/min product of
    // subarray that ends with the current number A[i]
    for (int i = 1, imax = r, imin = r; i < n; i++) {
        // multiplied by a negative makes big number smaller, small number bigger
        // so we redefine the extremums by swapping them
        if (A[i] < 0)
            swap(imax, imin);

        // max/min product for the current number is either the current number itself
        // or the max/min by the previous number times the current one
        imax = max(A[i], imax * A[i]);
        imin = min(A[i], imin * A[i]);

        // the newly computed max value is a candidate for our global result
        r = max(r, imax);
    }
    return r;
}
```
</p>


### [Java/C++/Python] it can be more simple
- Author: lee215
- Creation Date: Sat Oct 20 2018 10:24:24 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 03 2020 16:07:17 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Seem to be a problem of 2014.
Is it too late to write one in 2018?
<br>

# **Explanation**
Calculate prefix product in `A`.
Calculate suffix product in `A`.
Return the max.
<br>

**Python**
```python
    def maxProduct(self, A):
        B = A[::-1]
        for i in range(1, len(A)):
            A[i] *= A[i - 1] or 1
            B[i] *= B[i - 1] or 1
        return max(A + B)
```

**Also wrote in C++ version**
O(N) time O(1) space
```cpp
    int maxProduct(vector<int> A) {
        int n = A.size(), res = A[0], l = 0, r = 0;
        for (int i = 0; i < n; i++) {
            l =  (l ? l : 1) * A[i];
            r =  (r ? r : 1) * A[n - 1 - i];
            res = max(res, max(l, r));
        }
        return res;
    }
```

**Also wrote in Java version**
O(N) time O(1) space
```java
    public int maxProduct(int[] A) {
        int n = A.length, res = A[0], l = 0, r = 0;
        for (int i = 0; i < n; i++) {
            l =  (l == 0 ? 1 : l) * A[i];
            r =  (r == 0 ? 1 : r) * A[n - 1 - i];
            res = Math.max(res, Math.max(l, r));
        }
        return res;
    }
```
</p>


### Sharing my solution: O(1) space, O(n) running time
- Author: grow
- Creation Date: Thu Sep 25 2014 21:39:11 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 22:22:45 GMT+0800 (Singapore Standard Time)

<p>
    public int maxProduct(int[] A) {
        if (A.length == 0) {
            return 0;
        }
        
        int maxherepre = A[0];
        int minherepre = A[0];
        int maxsofar = A[0];
        int maxhere, minhere;
        
        for (int i = 1; i < A.length; i++) {
            maxhere = Math.max(Math.max(maxherepre * A[i], minherepre * A[i]), A[i]);
            minhere = Math.min(Math.min(maxherepre * A[i], minherepre * A[i]), A[i]);
            maxsofar = Math.max(maxhere, maxsofar);
            maxherepre = maxhere;
            minherepre = minhere;
        }
        return maxsofar;
    }



Note:
There's no need to use O(n) space, as all that you need is a minhere and maxhere. (local max and local min), then you can get maxsofar (which is global max) from them.

There's a chapter in Programming Pearls 2 that discussed the MaxSubArray problem, the idea is similar.
</p>


