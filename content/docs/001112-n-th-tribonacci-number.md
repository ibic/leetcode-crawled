---
title: "N-th Tribonacci Number"
weight: 1112
#id: "n-th-tribonacci-number"
---
## Description
<div class="description">
<p>The Tribonacci sequence T<sub>n</sub> is defined as follows:&nbsp;</p>

<p>T<sub>0</sub> = 0, T<sub>1</sub> = 1, T<sub>2</sub> = 1, and T<sub>n+3</sub> = T<sub>n</sub> + T<sub>n+1</sub> + T<sub>n+2</sub> for n &gt;= 0.</p>

<p>Given <code>n</code>, return the value of T<sub>n</sub>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 4
<strong>Output:</strong> 4
<strong>Explanation:</strong>
T_3 = 0 + 1 + 1 = 2
T_4 = 1 + 1 + 2 = 4
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 25
<strong>Output:</strong> 1389537
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= n &lt;= 37</code></li>
	<li>The answer is guaranteed to fit within a 32-bit integer, ie. <code>answer &lt;= 2^31 - 1</code>.</li>
</ul>
</div>

## Tags
- Recursion (recursion)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Coursera - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Possible Solutions: Space vs Performance Optimisation

There could be two approaches here. 
The first one is to optimise the performance, and the second one is to minimize the space used.

> Let's start from the performance optimisation. 

Since n is known to be less then 38, it's enough 
to precompute all 38 Tribonacci numbers once, 
store them in a static variable of the class Solution, 
and then just retrieve a needed number 
in a constant time during the testcase execution. 

> How to make the preliminary computations?

Two ideas could work here pretty well : recursion with memoization and dynamic programming. 
They both need N operations to compute N Tribonacci numbers. 
Simple recursion like `tribonacci(k) = tribonacci(k - 1) + tribonacci(k - 2) + tribonacci(k - 3)`
is out of consideration here because it will result in exponential time complexity $$3^N$$.

> Approach with preliminary computations has perfect O(1) 
runtime performance but
needs in O(N) space to keep N Tribonacci numbers. 
In some quite rare cases it's crucial
to optimise the space used above the performance.

In such a situation no more space-consuming static variables
are allowed and one could use dynamic programming approach 
keeping not more than 3 Tribonacci numbers in memory.

![fig](../Figures/1137/methods.png)
<br /> 
<br />


---
#### Approach 1: Space Optimisation : Dynamic Programming

- If n < 3 the answer is obvious.

- Otherwise initiate the first three numbers `x = 0, y = z = 1` 
and proceed to the loop of `n - 2` steps. At each step:

    - Replace x by y.
    
    - Replace y by z.
    
    - Replace z by x + y + z.
    
- Return z.

**Implementation**

<iframe src="https://leetcode.com/playground/9tqTWR93/shared" frameBorder="0" width="100%" height="293" name="9tqTWR93"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$.

* Space complexity : constant space to keep the last three 
Fibonacci numbers.
<br /> 
<br />


---
#### Approach 2: Performance Optimisation : Recursion with Memoization

- Precompute 38 Tribonacci numbers:

    - Initiate array of precomputed Tribonacci numbers `nums` by zeros
    and initiate the first three numbers. 
    
    - Return `helper(n - 1)`.
    
- Recursive function `helper(k)`:

    - If `k == 0`, return 0.
    
    - If `nums[k] != 0`, return `nums[k]`.
    
    - Otherwise, `nums[k] = helper(k - 1) + helper(k - 2) + helper(k - 3)`. 
    Return `nums[k]`.    
        
- Retrieve needed Tribonacci number from the array of precomputed numbers.

**Implementation**

<iframe src="https://leetcode.com/playground/xfMaTQBX/shared" frameBorder="0" width="100%" height="480" name="xfMaTQBX"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$ to retrieve preliminary 
computed Tribonacci number, and 38 operations for the preliminary computations. 

* Space complexity : constant space to keep an array of 
38 Tribonacci numbers.
<br /> 
<br />


---
#### Approach 3: Performance Optimisation : Dynamic Programming

- Precompute 38 Tribonacci numbers:

    - Initiate an array of precomputed Tribonacci numbers `nums` by zeros
    and initiate the first three numbers. 
    
    - Perform the loop for i in a range from 3 to 38.
    Compute at each step the new Tribonacci number:
    `nums[i] = helper(i - 1) + helper(i - 2) + helper(i - 3)`. 
        
- Retrieve needed Tribonacci number from the array of precomputed numbers.

**Implementation**

<iframe src="https://leetcode.com/playground/DaEevCFL/shared" frameBorder="0" width="100%" height="344" name="DaEevCFL"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$ to retrieve preliminary 
computed Tribonacci number, and 38 operations for the preliminary computations. 

* Space complexity : constant space to keep an array of 
38 Tribonacci numbers.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Jul 28 2019 12:03:53 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 31 2019 10:28:11 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Calculate next element `d = a + b + c`,
let `(a,b,c) = (b,c,d)`.
Repeat this process `n - 2` times;

We can loop `n` times and return `i0`.
It can remove the special cases for `n < 2`.
But I did `n - 2` loop on purpose.
`i1` and `i2` will get overflow.
Though it won\'t throw an error in Java. Hardly say it\'s a right answer.

A possibly better solution is to start with the number before i0,i1,i2.
As I did in python,
`i[-2] = 1`
`i[-1] = 1`
`i[0] = 0`
Then it won\'t have this problem.

## **Complexity**
Time `O(N)`
Space `O(1)`

<br>

**Java:**
```java
    public int tribonacci(int n) {
        if (n < 2) return n;
        int a = 0, b = 1, c = 1, d;
        while (n-- > 2) {
            d = a + b + c;
            a = b;
            b = c;
            c = d;
        }
        return c;
    }
```

**C++:**
```cpp
    int tribonacci(int n) {
        if (n < 2) return n;
        int a = 0, b = 1, c = 1, d = a + b + c;
        while (n-- > 2) {
            d = a + b + c, a = b, b = c, c = d;
        }
        return c;
    }
```

**Python:**
```python
    def tribonacci(self, n):
        a, b, c = 1, 0, 0
        for _ in xrange(n): a, b, c = b, c, a + b + c
        return c
```

</p>


### [C++]O(k^3log(n)) Solution (k=3), Matrix Exponentiation
- Author: dangledangkhoa
- Creation Date: Wed Jul 31 2019 11:10:34 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 31 2019 11:18:59 GMT+0800 (Singapore Standard Time)

<p>
```
We have 3 equations:
        f(n)   = f(n-1) + f(n-2) + f(n-3)
        f(n-1) = f(n-1)
        f(n-2) =          f(n-2)

By turning them into matrix realtion. we get:
        | f(n)   |     | 1 1 1 |     | f(n-1) |
        | f(n-1) |  =  | 1 0 0 |  *  | f(n-2) |
        | f(n-2) |     | 0 1 0 |     | f(n-3) |

Since we can compute an matrix exponent by O(log(n)), Simplify the relation into exponents
        | f(n)   |     | 1 1 1 |^(n-2)     | f(2) |
        | f(n-1) |  =  | 1 0 0 |       *   | f(1) |
        | f(n-2) |     | 0 1 0 |           | f(0) |
		
The matrix multiplication cost is k^3, k=3. So the total cost is O(k^3log(n))
```

```C++
class Solution {
private:
    vector<vector<int>> matrixMul(const vector<vector<int>> &A, const vector<vector<int>> &B) {
        vector<vector<int>> C(A.size(), vector<int>(B[0].size(), 0));
        for(int i=0; i<A.size(); ++i)
            for(int k=0; k<B.size(); ++k)
                for(int j=0; j<B[0].size();++j) {
                    C[i][j] += (A[i][k] * B[k][j]); 
                }
        return C;
    }
    vector<vector<int>> matrixPow(const vector<vector<int>> &A, int k) {
        if(k == 0) {
            vector<vector<int>> C(A.size(), vector<int>(A.size(), 0));
            for(int i=0; i < A.size(); ++i) C[i][i] = 1;
            return C;
        }
        if(k == 1) return A;
        
        vector<vector<int>> C = matrixPow(A, k/2);
        C = matrixMul(C, C);
        if(k%2 == 1) return matrixMul(C,A);
        return C;
    }
public:
    int tribonacci(int n) {
        if(n == 0) return 0;
        if(n==1 || n==2) return 1;

        int f_0 = 0;
        int f_1 = 1;
        int f_2 = 1;

        vector<vector<int>> C = matrixPow({
            {1,1,1},
            {1,0,0},
            {0,1,0}
        }, n-2);
        return matrixMul(C, {
            {f_2},
            {f_1},
            {f_0}
        })[0][0];
    }
};
```
</p>


### Java easy solution with comments
- Author: ab_rai
- Creation Date: Sun Jul 28 2019 13:32:54 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 30 2019 20:04:10 GMT+0800 (Singapore Standard Time)

<p>
We run a for loop in which newElement is calculated.
newElement is calculated by adding all the previous three elements
newElement= first+second+third

**Complexity:-**
time= O(n)
space=O(1)

```
class Solution {
    public int tribonacci(int n) {
        int first=0,second=1,third=1;
        // here first,second and third are the previous three elements
        if(n<2)
            return n;
        // if n=0 or n=1 then the tribonacci number is n itself 
        // else for loop will be executed
        for(int i=3;i<=n;i++)
        {
            int newElement=first+second+third;
            first=second;
            second=third;
            third=newElement;
        }
		//If n=2 then the for loop is not executed and the initial value of the
		//variable third is returned which is 1 
        return third;
        // returning the last element
    }
}
```
</p>


