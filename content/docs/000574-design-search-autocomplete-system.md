---
title: "Design Search Autocomplete System"
weight: 574
#id: "design-search-autocomplete-system"
---
## Description
<div class="description">
<p>Design a search autocomplete system for a search engine. Users may input a sentence (at least one word and end with a special character <code>&#39;#&#39;</code>). For <b>each character</b> they type <b>except &#39;#&#39;</b>, you need to return the <b>top 3</b> historical hot sentences that have prefix the same as the part of sentence already typed. Here are the specific rules:</p>

<ol>
	<li>The hot degree for a sentence is defined as the number of times a user typed the exactly same sentence before.</li>
	<li>The returned top 3 hot sentences should be sorted by hot degree (The first is the hottest one). If several sentences have the same degree of hot, you need to use ASCII-code order (smaller one appears first).</li>
	<li>If less than 3 hot sentences exist, then just return as many as you can.</li>
	<li>When the input is a special character, it means the sentence ends, and in this case, you need to return an empty list.</li>
</ol>

<p>Your job is to implement the following functions:</p>

<p>The constructor function:</p>

<p><code>AutocompleteSystem(String[] sentences, int[] times):</code> This is the constructor. The input is <b>historical data</b>. <code>Sentences</code> is a string array consists of previously typed sentences. <code>Times</code> is the corresponding times a sentence has been typed. Your system should record these historical data.</p>

<p>Now, the user wants to input a new sentence. The following function will provide the next character the user types:</p>

<p><code>List&lt;String&gt; input(char c):</code> The input <code>c</code> is the next character typed by the user. The character will only be lower-case letters (<code>&#39;a&#39;</code> to <code>&#39;z&#39;</code>), blank space (<code>&#39; &#39;</code>) or a special character (<code>&#39;#&#39;</code>). Also, the previously typed sentence should be recorded in your system. The output will be the <b>top 3</b> historical hot sentences that have prefix the same as the part of sentence already typed.</p>
&nbsp;

<p><b>Example:</b><br />
<b>Operation:</b> AutocompleteSystem([&quot;i love you&quot;, &quot;island&quot;,&quot;ironman&quot;, &quot;i love leetcode&quot;], [5,3,2,2])<br />
The system have already tracked down the following sentences and their corresponding times:<br />
<code>&quot;i love you&quot;</code> : <code>5</code> times<br />
<code>&quot;island&quot;</code> : <code>3</code> times<br />
<code>&quot;ironman&quot;</code> : <code>2</code> times<br />
<code>&quot;i love leetcode&quot;</code> : <code>2</code> times<br />
Now, the user begins another search:<br />
<br />
<b>Operation:</b> input(&#39;i&#39;)<br />
<b>Output:</b> [&quot;i love you&quot;, &quot;island&quot;,&quot;i love leetcode&quot;]<br />
<b>Explanation:</b><br />
There are four sentences that have prefix <code>&quot;i&quot;</code>. Among them, &quot;ironman&quot; and &quot;i love leetcode&quot; have same hot degree. Since <code>&#39; &#39;</code> has ASCII code 32 and <code>&#39;r&#39;</code> has ASCII code 114, &quot;i love leetcode&quot; should be in front of &quot;ironman&quot;. Also we only need to output top 3 hot sentences, so &quot;ironman&quot; will be ignored.<br />
<br />
<b>Operation:</b> input(&#39; &#39;)<br />
<b>Output:</b> [&quot;i love you&quot;,&quot;i love leetcode&quot;]<br />
<b>Explanation:</b><br />
There are only two sentences that have prefix <code>&quot;i &quot;</code>.<br />
<br />
<b>Operation:</b> input(&#39;a&#39;)<br />
<b>Output:</b> []<br />
<b>Explanation:</b><br />
There are no sentences that have prefix <code>&quot;i a&quot;</code>.<br />
<br />
<b>Operation:</b> input(&#39;#&#39;)<br />
<b>Output:</b> []<br />
<b>Explanation:</b><br />
The user finished the input, the sentence <code>&quot;i a&quot;</code> should be saved as a historical sentence in system. And the following input will be counted as a new search.</p>
&nbsp;

<p><b>Note:</b></p>

<ol>
	<li>The input sentence will always start with a letter and end with &#39;#&#39;, and only one blank space will exist between two words.</li>
	<li>The number of <b>complete sentences</b> that to be searched won&#39;t exceed 100. The length of each sentence including those in the historical data won&#39;t exceed 100.</li>
	<li>Please use double-quote instead of single-quote when you write test cases even for a character input.</li>
	<li>Please remember to <b>RESET</b> your class variables declared in class AutocompleteSystem, as static/class variables are <b>persisted across multiple test cases</b>. Please see <a href="https://leetcode.com/faq/#different-output">here</a> for more details.</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags
- Design (design)
- Trie (trie)

## Companies
- Google - 7 (taggedByAdmin: false)
- Amazon - 7 (taggedByAdmin: false)
- Lyft - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: true)
- Uber - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Dropbox - 7 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: true)
- Salesforce - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)

## Official Solution
[TOC]


## Solution

---
#### Approach 1: Brute Force

In this solution, we make use of a HashMap $$map$$ which stores entries in the form $$(sentence_i, times_i)$$. Here, $$times_i$$ refers to the number of times the $$sentence_i$$ has been typed earlier.

`AutocompleteSystem`: We pick up each sentence from $$sentences$$ and their corresponding times from the $$times$$, and make their entries in the $$map$$ appropriately.

`input(c)`: We make use of a current sentence tracker variable, $$\text{cur\_sent}$$, which is used to store the sentence entered till now as the input. For $$c$$ as the current input, firstly, we append this $$c$$ to $$\text{cur\_sent}$$ and then iterate over all the keys of $$map$$ to check if a key exists whose initial characters match with $$\text{cur\_sent}$$. We add all such keys to a $$list$$. Then, we sort this $$list$$ as per our requirements, and obtain the first three values from this $$list$$.

<iframe src="https://leetcode.com/playground/kqdPsxTH/shared" frameBorder="0" width="100%" height="500" name="kqdPsxTH"></iframe>

**Performance Analysis**

* `AutocompleteSystem()` takes $$O(k*l)$$ time. This is because, putting an entry in a hashMap takes $$O(1)$$ time. But, to create a hash value for a sentence of average length $$k$$, it will be scanned atleast once. We need to put $$l$$ such entries in the $$map$$.

* `input()` takes $$O\big(n+m \log m\big)$$ time. We need to iterate over the list of sentences, in $$map$$, entered till now(say with a count $$n$$), taking $$O(n)$$ time, to populate the $$list$$ used for finding the hot sentences. Then, we need to sort the $$list$$ of length $$m$$, taking $$O\big(m \log m\big)$$ time.

---

#### Approach 2: Using One level Indexing

This method is almost the same as that of the last approach except that instead of making use of simply a HashMap to store the sentences along with their number of occurences, we make use of a Two level HashMap. 

Thus, we make use of an array $$arr$$ of HashMapsEach element of this array, $$arr$$, is used to refer to one of the alphabets possible. Each element is a HashMap itself, which stores the sentences and their number of occurences similar to the last approach. e.g. $$arr[0]$$ is used to refer to a HashMap which stores the sentences starting with an 'a'. 

The process of adding the data in `AutocompleteSystem` and retrieving the data remains the same as in the last approach, except the one level indexing using $$arr$$ which needs to be done prior to accessing the required HashMap.

<iframe src="https://leetcode.com/playground/MCcmvnZz/shared" frameBorder="0" width="100%" height="500" name="MCcmvnZz"></iframe>

**Performance Analysis**

* `AutocompleteSystem()` takes $$O(k*l+26)$$ time. Putting an entry in a hashMap takes $$O(1)$$ time. But, to create a hash value for a sentence of average length $$k$$, it will be scanned atleast once. We need to put $$l$$ such entries in the $$map$$.

* `input()` takes $$O\big(s+m \log m\big)$$ time. We need to iterate only over one hashmap corresponding to the sentences starting with the first character of the current sentence, to populate the $$list$$ for finding the hot sentences. Here, $$s$$ refers to the size of this corresponding hashmap. Then, we need to sort the $$list$$ of length $$m$$, taking $$O\big(m \log m\big)$$ time.

---
#### Approach 3: Using Trie

A Trie is a special data structure used to store strings that can be visualized like a tree. It consists of nodes and edges. Each node consists of at max 26 children and edges connect each parent node to its children. These 26 pointers are nothing but pointers for each of the 26 letters of the English alphabet A separate edge is maintained for every edge.

Strings are stored in a top to bottom manner on the basis of their prefix in a trie. All prefixes of length 1 are stored at until level 1, all prefixes of length 2 are sorted at until level 2 and so on. 

A Trie data structure is very commonly used for representing the words stored in a dictionary. Each level represents one character of the word being formed. A word available in the dictionary can be read off from the Trie by starting from the root and going till the leaf. 

By doing a small modification to this structure, we can also include an entry, $$times$$, for the number of times the current word has been previously typed. This entry can be stored in the leaf node corresponding to the particular word.

Now, for implementing the `AutoComplete` function, we need to consider each character of the every word given in $$sentences$$ array, and add an entry corresponding to each such character at one level of the trie. At the leaf node of every word, we can update the $$times$$ section of the node with the corresponding number of times this word has been typed. 

The following figure shows a trie structure for the words  "A","to", "tea", "ted", "ten", "i", "in", and "inn", occuring 15, 7, 3, 4, 12, 11, 5 and 9 times respectively.

![Trie](../Figures/642/642_Trie.PNG)

Similarly, to implement the `input(c)` function, for every input character $$c$$, we need to add this character to the word being formed currently, i.e. to $$\text{cur\_sent}$$. Then, we need to traverse in the current trie till all the characters in the current word, $$\text{cur\_sent}$$, have been exhausted. 

From this point onwards, we traverse all the branches possible in the Trie, put the sentences/words formed by these branches to a $$list$$ along with their corresponding number of occurences, and find the best 3 out of them similar to the last approach. The following animation shows a typical illustration.

!?!../Documents/642_Design_Autocomplete.json:1000,563!?!


<iframe src="https://leetcode.com/playground/8wGSXZtL/shared" frameBorder="0" width="100%" height="500" name="8wGSXZtL"></iframe>

**Performance Analysis**

* `AutocompleteSystem()` takes $$O(k*l)$$ time. We need to iterate over $$l$$ sentences each of average length $$k$$, to create the trie for the given set of $$sentences$$.

* `input()` takes $$O\big(p+q+m \log m\big)$$ time. Here, $$p$$ refers to the length of the sentence formed till now, $$\text{cur\_sent}$$. $$q$$ refers to the number of nodes in the trie considering the sentence formed till now as the root node. Again, we need to sort the $$list$$ of length $$m$$ indicating the options available for the hot sentences, which takes $$O\big(m \log m\big)$$ time.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution, Trie and PriorityQueue
- Author: shawngao
- Creation Date: Sun Jul 16 2017 22:47:37 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 06:25:22 GMT+0800 (Singapore Standard Time)

<p>
Only thing more than a normal ```Trie``` is added a map of ```sentence``` to ```count``` in each of the ```Trie``` node to facilitate process of getting top 3 results.

```
public class AutocompleteSystem {
    class TrieNode {
        Map<Character, TrieNode> children;
        Map<String, Integer> counts;
        boolean isWord;
        public TrieNode() {
            children = new HashMap<Character, TrieNode>();
            counts = new HashMap<String, Integer>();
            isWord = false;
        }
    }
    
    class Pair {
        String s;
        int c;
        public Pair(String s, int c) {
            this.s = s; this.c = c;
        }
    }
    
    TrieNode root;
    String prefix;
    
    
    public AutocompleteSystem(String[] sentences, int[] times) {
        root = new TrieNode();
        prefix = "";
        
        for (int i = 0; i < sentences.length; i++) {
            add(sentences[i], times[i]);
        }
    }
    
    private void add(String s, int count) {
        TrieNode curr = root;
        for (char c : s.toCharArray()) {
            TrieNode next = curr.children.get(c);
            if (next == null) {
                next = new TrieNode();
                curr.children.put(c, next);
            }
            curr = next;
            curr.counts.put(s, curr.counts.getOrDefault(s, 0) + count);
        }
        curr.isWord = true;
    }
    
    public List<String> input(char c) {
        if (c == '#') {
            add(prefix, 1);
            prefix = "";
            return new ArrayList<String>();
        }
        
        prefix = prefix + c;
        TrieNode curr = root;
        for (char cc : prefix.toCharArray()) {
            TrieNode next = curr.children.get(cc);
            if (next == null) {
                return new ArrayList<String>();
            }
            curr = next;
        }
        
        PriorityQueue<Pair> pq = new PriorityQueue<>((a, b) -> (a.c == b.c ? a.s.compareTo(b.s) : b.c - a.c));
        for (String s : curr.counts.keySet()) {
            pq.add(new Pair(s, curr.counts.get(s)));
        }

        List<String> res = new ArrayList<String>();
        for (int i = 0; i < 3 && !pq.isEmpty(); i++) {
            res.add(pq.poll().s);
        }
        return res;
    }
}
```
</p>


### Python Clean Solution Using Trie
- Author: jedihy
- Creation Date: Wed Sep 27 2017 05:53:12 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 11:19:29 GMT+0800 (Singapore Standard Time)

<p>
Just using Trie.
```
class TrieNode(object):
    def __init__(self):
        self.children = {}
        self.isEnd = False
        self.data = None
        self.rank = 0
        
class AutocompleteSystem(object):
    def __init__(self, sentences, times):
        self.root = TrieNode()
        self.keyword = ""
        for i, sentence in enumerate(sentences):
            self.addRecord(sentence, times[i])

    def addRecord(self, sentence, hot):
        p = self.root
        for c in sentence:
            if c not in p.children:
                p.children[c] = TrieNode()
            p = p.children[c]
        p.isEnd = True
        p.data = sentence
        p.rank -= hot
    
    def dfs(self, root):
        ret = []
        if root:
            if root.isEnd:
                ret.append((root.rank, root.data))
            for child in root.children:
                ret.extend(self.dfs(root.children[child]))
        return ret
        
    def search(self, sentence):
        p = self.root
        for c in sentence:
            if c not in p.children:
                return []
            p = p.children[c]
        return self.dfs(p)
    
    def input(self, c):
        results = []
        if c != "#":
            self.keyword += c
            results = self.search(self.keyword)
        else:
            self.addRecord(self.keyword, 1)
            self.keyword = ""
        return [item[1] for item in sorted(results)[:3]]
```
</p>


### Java Solution using Trie with Video Explanation (English and Chinese)
- Author: LifeIsAGame
- Creation Date: Sat Aug 31 2019 22:04:20 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 06 2019 20:51:20 GMT+0800 (Singapore Standard Time)

<p>
# English:
<iframe width="560" height="315" src="https://www.youtube.com/embed/Zm3-cGKKFLg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Chinese
<iframe width="560" height="315" src="https://www.youtube.com/embed/NX68_rf_gxE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Code
```java
class AutocompleteSystem {
    class TrieNode implements Comparable<TrieNode> {
        TrieNode[] children;
        String s;
        int times;
        List<TrieNode> hot;
        
        public TrieNode() {
            children = new TrieNode[128];
            s = null;
            times = 0;
            hot = new ArrayList<>();
        }
        
        public int compareTo(TrieNode o) {
            if (this.times == o.times) {
                return this.s.compareTo(o.s);
            }
            
            return o.times - this.times;
        }
        
        public void update(TrieNode node) {
            if (!this.hot.contains(node)) {
                this.hot.add(node);
            }
            
            Collections.sort(hot);
            
            if (hot.size() > 3) {
                hot.remove(hot.size() - 1);
            }
        }
    }
    
    TrieNode root;
    TrieNode cur;
    StringBuilder sb;
    public AutocompleteSystem(String[] sentences, int[] times) {
        root = new TrieNode();
        cur = root;
        sb = new StringBuilder();
        
        for (int i = 0; i < times.length; i++) {
            add(sentences[i], times[i]);
        }
    }
    
    
    public void add(String sentence, int t) {
        TrieNode tmp = root;
        
        List<TrieNode> visited = new ArrayList<>();
        for (char c : sentence.toCharArray()) {
            if (tmp.children[c] == null) {
                tmp.children[c] = new TrieNode();
            }
            
            tmp = tmp.children[c];
            visited.add(tmp);
        }
        
        tmp.s = sentence;
        tmp.times += t;
        
        for (TrieNode node : visited) {
            node.update(tmp);
        }
    }
    
    public List<String> input(char c) {
        List<String> res = new ArrayList<>();
        if (c == \'#\') {
            add(sb.toString(), 1);
            sb = new StringBuilder();
            cur = root;
            return res;
        }
        
        sb.append(c);
        if (cur != null) {
            cur = cur.children[c];
        }
        
        if (cur == null) return res;
        for (TrieNode node : cur.hot) {
            res.add(node.s);
        }
        
        return res;
    }
}

/**
 * Your AutocompleteSystem object will be instantiated and called as such:
 * AutocompleteSystem obj = new AutocompleteSystem(sentences, times);
 * List<String> param_1 = obj.input(c);
 */
```
</p>


