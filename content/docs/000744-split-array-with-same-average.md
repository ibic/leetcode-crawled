---
title: "Split Array With Same Average"
weight: 744
#id: "split-array-with-same-average"
---
## Description
<div class="description">
<p>In a given integer array A, we must move every element of A to either list B or list C. (B and C initially start empty.)</p>

<p>Return true if and only if after such a move, it is possible that the average value of B is equal to the average value of C, and B and C are both non-empty.</p>

<pre>
<strong>Example :</strong>
<strong>Input:</strong> 
[1,2,3,4,5,6,7,8]
<strong>Output:</strong> true
<strong>Explanation: </strong>We can split the array into [1,4,5,8] and [2,3,6,7], and both of them have the average of 4.5.
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li>The length of <code>A</code> will be in the range&nbsp;[1, 30].</li>
	<li><code>A[i]</code> will be in the range of <code>[0, 10000]</code>.</li>
</ul>

<p>&nbsp;</p>

</div>

## Tags
- Math (math)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: true)
- Uber - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Meet in the Middle [Accepted]

**Intuition and Algorithm**

First, let's get a sense of the condition that `average(B) = average(C)`, where `B, C` are defined in the problem statement.

Say `A` (the input array) has `N` elements which sum to `S`, and `B` (one of the splitting sets) has `K` elements which sum to `X`.  Then the equation for `average(B) = average(C)` becomes $$\frac{X}{K} = \frac{S-X}{N-K}$$.  This reduces to $$X(N-K) = (S-X)K$$ which is $$\frac{X}{K} = \frac{S}{N}$$.  That is, `average(B) = average(A)`.

Now, we could delete `average(A)` from each element `A[i]` without changing our choice for `B`.  (`A[i] -= mu`, where `mu = average(A)`).  This means we just want to choose a set `B` that sums to `0`.

Trying all $$2^N$$ sets is still too many choices, so we will create sets of sums `left, right` of the approximately $$2^{N/2}$$ choices on the left and on the right separately.  (That is, `left` is a set of sums of every powerset in the first half of A, and `right` is the set of sums of every powerset in the second half of A).  Then, it is true if we find $$0$$ in these powersets, or if two sums in different halves cancel out (`-x in right for x in left`), except for one minor detail below.

Care must be taken that we do not specify sets that would make the original `B` or `C` empty.  If `sleft = A[0] + A[1] + ... + A[N/2 - 1]`, and `sright = A[N/2] + ... + A[N-1]`, (where `A[i]` was transformed to the new `A[i] - average(A)`) then we cannot choose both (`sleft, sright`).  This is correct because if for example `sleft` was a sum reached by a strictly smaller powerset than `{A[0], A[1], ..., A[N/2 - 1]}`, then the difference between these sets would be non-empty and have sum `0`.

<iframe src="https://leetcode.com/playground/zgrsweR2/shared" frameBorder="0" width="100%" height="500" name="zgrsweR2"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(2^{N/2})$$, where $$N$$ is the length of `A`.

* Space Complexity: $$O(2^{N/2})$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ Solution with explanation, early termination (Updated for new test case)
- Author: zestypanda
- Creation Date: Sun Mar 25 2018 11:09:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 02:40:47 GMT+0800 (Singapore Standard Time)

<p>
First, this problem is NP, and the worst case runtime is exponential. But the expected runtime for random input could be very fast.

If the array of size n can be splitted into group A and B with same mean, assuming A is the smaller group, then
```
  totalSum/n = Asum/k = Bsum/(n-k), where k = A.size() and 1 <= k <= n/2;
  Asum = totalSum*k/n, which is an integer. So we have totalSum*k%n == 0;
```
In general, not many k are valid. 

**Solution 2: early pruning + knapsack DP, O(n^3 * M) 33 ms**
If there are still some k valid after early pruning by checking ```totalSum*k%n == 0```,
we can generate all possible combination sum of k numbers from the array using DP, like knapsack problem. (Note: 1 <= k <= n/2)
Next, for each valid k, simply check whether the group sum, i.e. totalSum * k / n, exists in the kth combination sum hashset.
```
vector<vector<unordered_set<int>>> sums(n, vector<unordered_set<int>>(n/2+1));
sums[i][j] is all possible combination sum of j numbers from the subarray A[0, i];
```
**Goal:** sums[n-1][k], for all k in range [1, n/2]
**Initial condition:** sums[i][0] = {0}, 0 <= i <= n-1; sums[0][1] = {all numbers in the array}; 
**Deduction:** sums[i+1][j] = sums[i][j]  "join"  (sums[i][j-1] + A[i+1])
The following code uses less space but the same DP formula.
**Runtime analysis:**
All numbers in the array are in range [0, 10000]. Let M = 10000.
So the size of kth combination sum hashset, i.e. sums[...][k], is <= k * M;
For each number in the array, the code need loop through all combination sum hashsets, so 
the total runtime is n * (1 * M + 2 * M + ... + (n/2) * M) = **O(n^3 * M)**
```
class Solution {
public:
    bool splitArraySameAverage(vector<int>& A) {
        int n = A.size(), m = n/2, totalSum = accumulate(A.begin(), A.end(), 0);
        // early pruning
        bool isPossible = false;
        for (int i = 1; i <= m && !isPossible; ++i) 
            if (totalSum*i%n == 0) isPossible = true;
        if (!isPossible) return false;
        // DP like knapsack
        vector<unordered_set<int>> sums(m+1);
        sums[0].insert(0);
        for (int num: A) {
            for (int i = m; i >= 1; --i) 
                for (const int t: sums[i-1]) 
                    sums[i].insert(t + num);
        }
        for (int i = 1; i <= m; ++i) 
            if (totalSum*i%n == 0 && sums[i].find(totalSum*i/n) != sums[i].end()) return true;
        return false;
    }
};
```

**Solution 1: early termination + combination sum. 5 ms Now TLE (Update)**
For such k, the problem transforms to "Find k sum = Asum, i.e. totalSum * k/n, from an array of size n". This subproblem is similar to LC39 combination sum, which can be solved by backtracking.
```
class Solution {
public:
    bool splitArraySameAverage(vector<int>& A) {
        int n = A.size(), m = n/2, totalSum = accumulate(A.begin(), A.end(), 0);
        sort(A.rbegin(), A.rend()); // Optimization
        for (int i = 1; i <= m; ++i) 
            if (totalSum*i%n == 0 && combinationSum(A, 0, i, totalSum*i/n)) return true;
        return false;
    }
    bool combinationSum(vector<int>& nums, int idx, int k, int tar) {
        if (tar > k * nums[idx]) return false; // Optimization, A is sorted from large to small
        if (k == 0) return tar == 0;
        for (int i = idx; i <= nums.size()-k; ++i) 
            if (nums[i] <= tar && combinationSum(nums, i+1, k-1, tar-nums[i])) return true;
        return false;
    }
};
```
</p>


### DP with bitset over *sum* (fast Python/Ruby, decent C++)
- Author: StefanPochmann
- Creation Date: Mon Mar 26 2018 04:29:27 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 09:54:35 GMT+0800 (Singapore Standard Time)

<p>
**Python version**

My fastest Python solution so far, gets accepted in about 44 ms (tried five times, took 44, 48, 44, 40 and 44 ms). After using [bitset over size N](https://leetcode.com/problems/split-array-with-same-average/discuss/120830/DP-with-bitset) I realized I could do bitset over sum S instead, using Python\'s big integers. Then almost all action is in Python\'s C code (for big integers). Which is fast and saves me code.

    def splitArraySameAverage(self, A):
        N, S, P = len(A), sum(A), [1]
        for a in A:
            P[1:] = [(p << a) | q for p, q in zip(P, P[1:] + [0])]
        return any(S * n % N == 0 and P[n] & (1 << (S * n // N))
                   for n in range(1, N))

A nice side effect of using big integers for the bitsets is that they don\'t always store S bits but only as many bits as needed to include the largest 1-bit. We can even take further advantage of this by using `for a in sorted(A)`, which keeps the bitsets short as long as it can. Though that doesn\'t make it faster here, as 44 ms is already pretty much as fast as possible. Even the below cheating solution takes about 40 ms:
```
answers = iter(\'TFFTFTFFFFTTFFFFFFFFFFFFFTTFFFFFTFFFFFTFFFFTFFFFTFFFFFFFFTFFFTFTTFFFTFTFFFFFFFFFFFFTFFF\')
class Solution:
    def splitArraySameAverage(self, A):
        return next(answers) == \'T\'
```

<br>

**Ruby version**

Ruby also has big integers that we can use as bitsets. Takes about 50 ms (five attempts took 52, 52, 48, 52 and 48 ms):

```
def split_array_same_average(a)
  n, s, p = a.size, a.sum, [1]
  a.each { |x| p[1..-1] = (p << 0).each_cons(2).map { |q, r| (q << x) | r } }
  (1..n/2).any? { |m| s * m % n == 0 && (p[m] & (1 << (s * m / n))) > 0 }
end
```

<br>

**C++ version** using `bitset<300001>`

Sadly `bitset` doesn\'t support dynamic sizes. So I must make it as large as the largest *possible* sum, not just as large as the sum of the given input and not just as large as the current prefix sum of the given input. It takes about 190 ms, much slower than my [bitset over size](https://leetcode.com/problems/split-array-with-same-average/discuss/120830/Simple-DP) version (which takes about 35 ms). I could make it about twice as fast by not going the full N-range for every `a` but only going as far as how many `a` values I\'ve used so far, but meh... what\'s the point if it\'s gonna be slower anyway.

    bool splitArraySameAverage(vector<int>& A) {        
        int N = A.size(), S = 0;
        for (int a : A) S += a;
        bitset<300001> p[N] = {1};
        for (int a : A)
            for (int n = N - 2; n >= 0; n--)
                p[n+1] |= p[n] << a;
        for (int n = 1; n < N; n++)
            if (S*n%N == 0  &&  p[n][S*n/N])
                return true;
        return false;
    }

</p>


### [Python] Easy and Concise Solution
- Author: lee215
- Creation Date: Sun Mar 25 2018 15:54:47 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 01 2020 11:13:21 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
Change the quesiton change to a N-sum problem:
To find if
`1` element with `sum = 1 * avg`  or
`2` elements with  `sum = 2 * avg` or
`k` elements with  `sum = k * avg`

The size of smaller list between `B` and `C` will be  less than `N/2+1`, so `0 < i < N/2+1`

Recursive funciton `find` try to find a subset of `n` elements from `A` with `sum = target`
<br>

**Python3**
```py
    def splitArraySameAverage(self, A):
        @functools.lru_cache()
        def find(target, k, i):
            if k == 0: return target == 0
            if target < 0 or k + i > n: return False
            return find(target - A[i], k - 1, i + 1) or find(target, k, i + 1)
        n, s = len(A), sum(A)
        return any(find(s * k // n, k, 0) for k in range(1, n // 2 + 1) if s * k % n == 0)
```
<br>

Commented [version](https://leetcode.com/problems/split-array-with-same-average/discuss/663594) from @lichuan199010
**Python3**
```py
    def splitArraySameAverage(self, A: List[int]) -> bool:

        # A subfunction that see if total k elements sums to target
        # target is the goal, k is the number of elements in set B, i is the index we have traversed through so far
        mem = {}

        def find(target, k, i):
            # if we are down searching for k elements in the array, see if the target is 0 or not. This is a basecase
            if k == 0: return target == 0

            # if the to-be selected elements in B (k) + elements we have traversed so far is larger than total length of A
            # even if we choose all elements, we don\'t have enough elements left, there should be no valid answer.
            if k + i > len(A): return False

            if (target, k, i) in mem: return mem[(target, k, i)]

            # if we choose the ith element, the target becomes target - A[i] for total sum
            # if we don\'t choose the ith element, the target doesn\'t change
            mem[(target - A[i], k - 1, i + 1)] = find(target - A[i], k - 1, i + 1) or find(target, k, i + 1)

            return mem[(target - A[i], k - 1, i + 1)]

        n, s = len(A), sum(A)
        # Note that the smaller set has length j ranging from 1 to n//2+1
        # we iterate for each possible length j of array B from length 1 to length n//2+1
        # if s*j%n, which is the sum of the subset, it should be an integer, so we only proceed to check if s * j % n == 0
        # we check if we can find target sum s*j//n (total sum of j elements that sums to s*j//n)
        return any(find(s * j // n, j, 0) for j in range(1, n // 2 + 1) if s * j % n == 0)
```
</p>


