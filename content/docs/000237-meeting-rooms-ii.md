---
title: "Meeting Rooms II"
weight: 237
#id: "meeting-rooms-ii"
---
## Description
<div class="description">
<p>Given an array of meeting time intervals consisting of start and end times <code>[[s1,e1],[s2,e2],...]</code> (s<sub>i</sub> &lt; e<sub>i</sub>), find the minimum number of conference rooms required.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> <code>[[0, 30],[5, 10],[15, 20]]</code>
<strong>Output:</strong> 2</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> [[7,10],[2,4]]
<b>Output:</b> 1</pre>

<p><strong>NOTE:</strong>&nbsp;input types have been changed on April 15, 2019. Please reset to default code definition to get new method signature.</p>

</div>

## Tags
- Heap (heap)
- Greedy (greedy)
- Sort (sort)

## Companies
- Amazon - 24 (taggedByAdmin: false)
- Bloomberg - 16 (taggedByAdmin: false)
- Facebook - 15 (taggedByAdmin: true)
- Google - 7 (taggedByAdmin: true)
- eBay - 7 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Roblox - 2 (taggedByAdmin: false)
- Uber - 6 (taggedByAdmin: false)
- Quora - 3 (taggedByAdmin: false)
- Lyft - 3 (taggedByAdmin: false)
- Nutanix - 3 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: true)
- Visa - 2 (taggedByAdmin: false)
- GoDaddy - 2 (taggedByAdmin: false)
- Citrix - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Yelp - 5 (taggedByAdmin: false)
- Paypal - 3 (taggedByAdmin: false)
- Atlassian - 3 (taggedByAdmin: false)
- Postmates - 3 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Booking.com - 2 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- Drawbridge - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
<br />

**Intuition**

This problem is very similar to something that employees of a company can face potentially on daily basis.

Suppose you work at a company and you belong to the IT department and one of your job responsibilities is securing rooms for meetings that are to happen throughout the day in the office.

You have multiple meeting rooms in the office and you want to make judicious use of them. You don't really want to keep people waiting and want to give a group of employees a room to hold the meeting right on time.

At the same time, you don't really want to use too many rooms unless absolutely necessary. It would make sense to hold meetings in different rooms provided that the meetings are colliding with each other, otherwise you want to make use of as less rooms as possible to hold all of the meetings. How do you go about it ?

I just represented a common scenario at an office where given the start and end times for meetings to happen throughout the day, you, as an IT guy need to setup and allocate the room numbers to different teams.

Let's approach this problem from the perspective of a group of people who want to hold a meeting and have not been allocated a room yet. What would they do?

>This group would essentially go from one room to another and check if any meeting room is free. If they find a room that is indeed free, they would start their meeting in that room. Otherwise, they would wait for a room to be free. As soon as the room frees up, they would occupy it.

This is the basic approach that we will follow in this question. So, it is a kind of simulation but not exactly. In the worst case we can assign a new room to all of the meetings but that is not really optimal right? Unless of course they all collide with each other.

> We need to be able to find out efficiently if a room is available or not for the current meeting and assign a new room only if none of the assigned rooms is currently free.

Let's look at the first approach based on the idea we just discussed.
<br/>
<br/>

---

#### Approach 1: Priority Queues

We can't really process the given meetings in any random order. The most basic way of processing the meetings is in increasing order of their `start times` and this is the order we will follow. After all if you're an IT guy, you should allocate a room to the meeting that is scheduled for 9 a.m. in the morning before you worry about the 5 p.m. meeting, right?

Let's do a dry run of an example problem with sample meeting times and see what our algorithm should be able to do efficiently.

We will consider the following meeting times for our example `(1, 10), (2, 7), (3, 19), (8, 12), (10, 20), (11, 30)`. The first part of the tuple is the start time for the meeting and the second value represents the ending time. We are considering the meetings in a sorted order of their start times. The first diagram depicts the first three meetings where each of them requires a new room because of collisions.

<center>
<img src="../Figures/253/253_Meeting_Rooms_II_Diag_1.png" width="700"></center>

The next 3 meetings start to occupy some of the existing rooms. However, the last one requires a new room altogether and overall we have to use 4 different rooms to accommodate all the meetings.

<center>
<img src="../Figures/253/253_Meeting_Rooms_II_Diag_2.png" width="700"></center>

Sorting part is easy, but for every meeting how do we find out efficiently if a room is available or not? At any point in time we have multiple rooms that can be occupied and we don't really care which room is free as long as we find one when required for a new meeting.

A naive way to check if a room is available or not is to iterate on all the rooms and see if one is available when we have a new meeting at hand.

> However, we can do better than this by making use of Priority Queues or the Min-Heap data structure.

Instead of manually iterating on every room that's been allocated and checking if the room is available or not, we can keep all the rooms in a min heap where the key for the min heap would be the ending time of meeting.

So, every time we want to check if **any** room is free or not, simply check the topmost element of the min heap as that would be the room that would get free the earliest out of all the other rooms currently occupied.

If the room we extracted from the top of the min heap isn't free, then `no other room is`. So, we can save time here and simply allocate a new room.

Let us look at the algorithm before moving onto the implementation.

**Algorithm**

1. Sort the given meetings by their `start time`.
2. Initialize a new `min-heap` and add the first meeting's ending time to the heap. We simply need to keep track of the ending times as that tells us when a meeting room will get free.
3. For every meeting room check if the minimum element of the heap i.e. the room at the top of the heap is free or not.
  1. If the room is free, then we extract the topmost element and add it back with the ending time of the current meeting we are processing.
  2. If not, then we allocate a new room and add it to the heap.
4. After processing all the meetings, the size of the heap will tell us the number of rooms allocated. This will be the minimum number of rooms needed to accommodate all the meetings.

Let us not look at the implementation for this algorithm.

<iframe src="https://leetcode.com/playground/jCt4Ru8D/shared" frameBorder="0" width="100%" height="500" name="jCt4Ru8D"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N\log N)$$.
    - There are two major portions that take up time here. One is `sorting` of the array that takes $$O(N\log N)$$ considering that the array consists of $$N$$ elements.
    - Then we have the `min-heap`. In the worst case, all $$N$$ meetings will collide with each other. In any case we have $$N$$ add operations on the heap. In the worst case we will have $$N$$ extract-min operations as well. Overall complexity being $$(NlogN)$$ since extract-min operation on a heap takes $$O(\log N)$$.

* Space Complexity: $$O(N)$$ because we construct the `min-heap` and that can contain $$N$$ elements in the worst case as described above in the time complexity section. Hence, the space complexity is $$O(N)$$.
<br/>
<br/>

---

#### Approach 2: Chronological Ordering

**Intuition**

The meeting timings given to us define a chronological order of events throughout the day. We are given the start and end timings for the meetings which can help us define this ordering.

Arranging the meetings according to their start times helps us know the natural order of meetings throughout the day. However, simply knowing when a meeting starts doesn't tell us much about its duration.

We also need the meetings sorted by their ending times because an ending event essentially tells us that there must have been a corresponding starting event and more importantly, an ending event tell us that a previously occupied room has now become free.

A meeting is defined by its start and end times. However, for this specific algorithm, we need to treat the start and end times `individually`. This might not make sense right away because a meeting is defined by its start and end times. If we separate the two and treat them individually, then the identity of a meeting goes away. This is fine because:

> When we encounter an ending event, that means that some meeting that started earlier has ended now. We are not really concerned with which meeting has ended. All we need is that **some** meeting ended thus making a room available.

Let us consider the same example as we did in the last approach. We have the following meetings to be scheduled: `(1, 10), (2, 7), (3, 19), (8, 12), (10, 20), (11, 30)`. As before, the first diagram show us that the first three meetings are colliding with each other and they have to be allocated separate rooms.

<center>
<img src="../Figures/253/253_Meeting_Rooms_II_Diag_3.png"></center>

The next two diagrams process the remaining meetings and we see that we can now reuse some of the existing meeting rooms. The final result is the same, we need 4 different meeting rooms to process all the meetings. That's the best we can do here.

<center>
<img src="../Figures/253/253_Meeting_Rooms_II_Diag_4.png"></center>

<center>
<img src="../Figures/253/253_Meeting_Rooms_II_Diag_5.png"></center>


**Algorithm**

1. Separate out the start times and the end times in their separate arrays.
2. Sort the start times and the end times separately. Note that this will mess up the original correspondence of start times and end times. They will be treated individually now.
3. We consider two pointers: `s_ptr` and `e_ptr` which refer to start pointer and end pointer. The start pointer simply iterates over all the meetings and the end pointer helps us track if a meeting has ended and if we can reuse a room.
4. When considering a specific meeting pointed to by `s_ptr`, we check if this start timing is greater than the meeting pointed to by `e_ptr`. If this is the case then that would mean some meeting has ended by the time the meeting at `s_ptr` had to start. So we can reuse one of the rooms. Otherwise, we have to allocate a new room.
5. If a meeting has indeed ended i.e. if `start[s_ptr] >= end[e_ptr]`, then we increment `e_ptr`.
6. Repeat this process until `s_ptr` processes all of the meetings.

Let us not look at the implementation for this algorithm.

<iframe src="https://leetcode.com/playground/wXd7WCgG/shared" frameBorder="0" width="100%" height="500" name="wXd7WCgG"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N\log N)$$ because all we are doing is sorting the two arrays for `start timings` and `end timings` individually and each of them would contain $$N$$ elements considering there are $$N$$ intervals.

* Space Complexity: $$O(N)$$ because we create two separate arrays of size $$N$$, one for recording the start times and one for the end times.



<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Explanation of "Super Easy Java Solution Beats 98.8%" from @pinkfloyda
- Author: magicyuli
- Creation Date: Mon Jan 25 2016 08:12:22 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 22:00:49 GMT+0800 (Singapore Standard Time)

<p>
The solution is proposed by @pinkfloyda at ["Super Easy Java Solution Beats 98.8%"][1] , which is amazing.

Here I would like to explain why it works a little bit.

The code from @pinkfloyda:

    public class Solution {
        public int minMeetingRooms(Interval[] intervals) {
            int[] starts = new int[intervals.length];
            int[] ends = new int[intervals.length];
            for(int i=0; i<intervals.length; i++) {
                starts[i] = intervals[i].start;
                ends[i] = intervals[i].end;
            }
            Arrays.sort(starts);
            Arrays.sort(ends);
            int rooms = 0;
            int endsItr = 0;
            for(int i=0; i<starts.length; i++) {
                if(starts[i]<ends[endsItr])
                    rooms++;
                else
                    endsItr++;
            }
            return rooms;
        }
    }

To understand why it works, first let\u2019s define two events:
Meeting Starts
Meeting Ends

Next, we acknowledge three facts:
The numbers of the intervals give chronological orders
When an ending event occurs, there must be a starting event has happened before that, where \u201chappen before\u201d is defined by the chronological orders given by the intervals
Meetings that started which haven\u2019t ended yet have to be put into different meeting rooms, and the number of rooms needed is the number of such meetings

So, what this algorithm works as follows:

for example, we have meetings that span along time as follows:

    |_____|
          |______|
    |________|
            |_______|

Then, the start time array and end time array after sorting appear like follows:

    ||    ||
         |   |   |  |

Initially, `endsItr` points to the first end event, and we move `i` which is the start event pointer. As we examine the start events, we\u2019ll find the first two start events happen before the end event that `endsItr` points to, so we need two rooms (we magically created two rooms), as shown by the variable rooms. Then, as `i` points to the third start event, we\u2019ll find that this event happens after the end event pointed by `endsItr`, then we increment `endsItr` so that it points to the next end event. What happens here can be thought of as one of the two previous meetings ended, and we moved the newly started meeting into that vacant room, thus we don\u2019t need to increment rooms at this time and move both of the pointers forward.
Next, because `endsItr` moves to the next end event, we\u2019ll find that the start event pointed by `i` happens before the end event pointed by `endsItr`. Thus, now we have 4 meetings started but only one ended, so we need one more room. And it goes on as this.


  [1]: https://leetcode.com/discuss/71846/super-easy-java-solution-beats-98-8%25
</p>


### AC Java solution using min heap
- Author: jeantimex
- Creation Date: Sun Aug 09 2015 00:58:47 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 07:50:34 GMT+0800 (Singapore Standard Time)

<p>
Just want to share another idea that uses min heap, average time complexity is O(nlogn).

    public int minMeetingRooms(Interval[] intervals) {
        if (intervals == null || intervals.length == 0)
            return 0;
            
        // Sort the intervals by start time
        Arrays.sort(intervals, new Comparator<Interval>() {
            public int compare(Interval a, Interval b) { return a.start - b.start; }
        });
        
        // Use a min heap to track the minimum end time of merged intervals
        PriorityQueue<Interval> heap = new PriorityQueue<Interval>(intervals.length, new Comparator<Interval>() {
            public int compare(Interval a, Interval b) { return a.end - b.end; }
        });
        
        // start with the first meeting, put it to a meeting room
        heap.offer(intervals[0]);
        
        for (int i = 1; i < intervals.length; i++) {
            // get the meeting room that finishes earliest
            Interval interval = heap.poll();
            
            if (intervals[i].start >= interval.end) {
                // if the current meeting starts right after 
                // there's no need for a new room, merge the interval
                interval.end = intervals[i].end;
            } else {
                // otherwise, this meeting needs a new room
                heap.offer(intervals[i]);
            }
            
            // don't forget to put the meeting room back
            heap.offer(interval);
        }
        
        return heap.size();
    }
</p>


### My Python Solution With Explanation
- Author: lc_cl_aaa
- Creation Date: Sat Aug 08 2015 08:19:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 15:17:37 GMT+0800 (Singapore Standard Time)

<p>
     # Very similar with what we do in real life. Whenever you want to start a meeting, 
     # you go and check if any empty room available (available > 0) and
     # if so take one of them ( available -=1 ). Otherwise,
     # you need to find a new room someplace else ( numRooms += 1 ).  
     # After you finish the meeting, the room becomes available again ( available += 1 ).
     
     def minMeetingRooms(self, intervals):
            starts = []
            ends = []
            for i in intervals:
                starts.append(i.start)
                ends.append(i.end)
            
            starts.sort()
            ends.sort()
            s = e = 0
            numRooms = available = 0
            while s < len(starts):
                if starts[s] < ends[e]:
                    if available == 0:
                        numRooms += 1
                    else:
                        available -= 1
                        
                    s += 1
                else:
                    available += 1
                    e += 1
            
            return numRooms
</p>


