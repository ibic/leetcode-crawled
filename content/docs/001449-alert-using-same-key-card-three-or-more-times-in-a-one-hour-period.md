---
title: "Alert Using Same Key-Card Three or More Times in a One Hour Period"
weight: 1449
#id: "alert-using-same-key-card-three-or-more-times-in-a-one-hour-period"
---
## Description
<div class="description">
<p>LeetCode company workers use key-cards to unlock office doors. Each time a worker uses their key-card, the security system saves the worker&#39;s name and the time when it was used. The system emits an <strong>alert</strong> if any worker uses the key-card <strong>three or more times</strong> in a one-hour period.</p>

<p>You are given a list of strings <code>keyName</code> and <code>keyTime</code> where <code>[keyName[i], keyTime[i]]</code> corresponds to a person&#39;s name and the time when their key-card was used <strong>in a</strong> <strong>single day</strong>.</p>

<p>Access times are given in the <strong>24-hour time format &quot;HH:MM&quot;</strong>, such as <code>&quot;23:51&quot;</code> and <code>&quot;09:49&quot;</code>.</p>

<p>Return a <em>list of unique worker names who received an alert for frequent keycard use</em>. Sort the names in <strong>ascending order alphabetically</strong>.</p>

<p>Notice that <code>&quot;10:00&quot;</code> - <code>&quot;11:00&quot;</code> is considered to be within a one-hour period, while <code>&quot;22:51&quot;</code> - <code>&quot;23:52&quot;</code> is not considered to be within a one-hour period.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> keyName = [&quot;daniel&quot;,&quot;daniel&quot;,&quot;daniel&quot;,&quot;luis&quot;,&quot;luis&quot;,&quot;luis&quot;,&quot;luis&quot;], keyTime = [&quot;10:00&quot;,&quot;10:40&quot;,&quot;11:00&quot;,&quot;09:00&quot;,&quot;11:00&quot;,&quot;13:00&quot;,&quot;15:00&quot;]
<strong>Output:</strong> [&quot;daniel&quot;]
<strong>Explanation:</strong> &quot;daniel&quot; used the keycard 3 times in a one-hour period (&quot;10:00&quot;,&quot;10:40&quot;, &quot;11:00&quot;).
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> keyName = [&quot;alice&quot;,&quot;alice&quot;,&quot;alice&quot;,&quot;bob&quot;,&quot;bob&quot;,&quot;bob&quot;,&quot;bob&quot;], keyTime = [&quot;12:01&quot;,&quot;12:00&quot;,&quot;18:00&quot;,&quot;21:00&quot;,&quot;21:20&quot;,&quot;21:30&quot;,&quot;23:00&quot;]
<strong>Output:</strong> [&quot;bob&quot;]
<strong>Explanation:</strong> &quot;bob&quot; used the keycard 3 times in a one-hour period (&quot;21:00&quot;,&quot;21:20&quot;, &quot;21:30&quot;).
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> keyName = [&quot;john&quot;,&quot;john&quot;,&quot;john&quot;], keyTime = [&quot;23:58&quot;,&quot;23:59&quot;,&quot;00:01&quot;]
<strong>Output:</strong> []
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> keyName = [&quot;leslie&quot;,&quot;leslie&quot;,&quot;leslie&quot;,&quot;clare&quot;,&quot;clare&quot;,&quot;clare&quot;,&quot;clare&quot;], keyTime = [&quot;13:00&quot;,&quot;13:20&quot;,&quot;14:00&quot;,&quot;18:00&quot;,&quot;18:51&quot;,&quot;19:30&quot;,&quot;19:49&quot;]
<strong>Output:</strong> [&quot;clare&quot;,&quot;leslie&quot;]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= keyName.length, keyTime.length &lt;= 10<sup>5</sup></code></li>
	<li><code>keyName.length == keyTime.length</code></li>
	<li><code>keyTime</code> are in the format <strong>&quot;HH:MM&quot;</strong>.</li>
	<li><code>[keyName[i], keyTime[i]]</code> is <strong>unique</strong>.</li>
	<li><code>1 &lt;= keyName[i].length &lt;= 10</code></li>
	<li><code>keyName[i] contains only lowercase English letters.</code></li>
</ul>

</div>

## Tags
- String (string)
- Ordered Map (ordered-map)

## Companies
- Wayfair - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] HashMap/dict + Sliding Window.
- Author: rock
- Creation Date: Sun Oct 04 2020 00:06:59 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 06 2020 09:10:34 GMT+0800 (Singapore Standard Time)

<p>
Two similar codes either language.

----

**Method 1: Scan 3-consecutive-elements window**
```java
    public List<String> alertNames(String[] keyName, String[] keyTime) {
        Map<String, TreeSet<Integer>> nameToTime = new HashMap<>();
        for (int i = 0; i < keyName.length; ++i) {
            int time = Integer.parseInt(keyTime[i].substring(0, 2)) * 60 + Integer.parseInt(keyTime[i].substring(3));
            nameToTime.computeIfAbsent(keyName[i], s -> new TreeSet<>()).add(time);
        }
        TreeSet<String> names = new TreeSet<>();
        for (Map.Entry<String, TreeSet<Integer>> e : nameToTime.entrySet()) {
            List<Integer> list = new ArrayList<>(e.getValue());
            for (int i = 2; i < list.size(); ++i) {
                if (list.get(i) - list.get(i - 2) <= 60 ) {
                    names.add(e.getKey());
                    break;
                }
            }
        }
        return new ArrayList<>(names);
    }
```
```python
    def alertNames(self, keyName: List[str], keyTime: List[str]) -> List[str]:
        name_to_time = collections.defaultdict(list)
        for name, hour_minute in zip(keyName, keyTime):
            hour, minute = map(int, hour_minute.split(\':\'))
            time = hour * 60 + minute
            name_to_time[name].append(time)
        names = []    
        for name, time_list in name_to_time.items():
            time_list.sort()
            for i, time in enumerate(time_list):
                if i >= 2 and time - time_list[i - 2] <= 60:
                    names.append(name)
                    break
        return sorted(names)
```

----

**Method 2: Deque** -- credit to **@grokus**

The only difference from method 1 is to use `Deque` instead of `3-consecutive-elements` to maintain the sliding window in which all elements are within `60` minutes;  The trade off is sacrifice a bit of memory of `Deque` for better readability.
```java
    public List<String> alertNames(String[] keyName, String[] keyTime) {
        Map<String, TreeSet<Integer>> nameToTime = new HashMap<>();
        for (int i = 0; i < keyName.length; ++i) {
            int time = Integer.parseInt(keyTime[i].substring(0, 2)) * 60 + Integer.parseInt(keyTime[i].substring(3));
            nameToTime.computeIfAbsent(keyName[i], s -> new TreeSet<>()).add(time);
        }
        TreeSet<String> names = new TreeSet<>();
        for (Map.Entry<String, TreeSet<Integer>> e : nameToTime.entrySet()) {
            Deque<Integer> dq = new ArrayDeque<>();
            for (int time : new ArrayList<>(e.getValue())) {
                dq.offer(time);
                if (dq.peekLast() - dq.peek() > 60) {
                    dq.poll();
                }
                if (dq.size() >= 3) {
                    names.add(e.getKey());
                    break;
                }
            }
        }
        return new ArrayList<>(names);
    }
```
```python
    def alertNames(self, keyName: List[str], keyTime: List[str]) -> List[str]:
        name_to_time = collections.defaultdict(list)
        for name, hour_minute in zip(keyName, keyTime):
            hour, minute = map(int, hour_minute.split(\':\'))
            time = hour * 60 + minute
            name_to_time[name].append(time)
        names = []    
        for name, time_list in name_to_time.items():
            time_list.sort()
            dq = collections.deque()
            for time in time_list:
                dq.append(time)
                if dq[-1] - dq[0] > 60:
                    dq.popleft()
                if len(dq) >= 3:
                    names.append(name)
                    break
        return sorted(names)
```
</p>


### C++ Sorting
- Author: votrubac
- Creation Date: Sun Oct 04 2020 01:25:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 01:27:56 GMT+0800 (Singapore Standard Time)

<p>
Collect times for each person, sort them, and then use a sliding window to check for alarms.

```cpp
vector<string> alertNames(vector<string>& names, vector<string>& times) {
    unordered_map<string, vector<int>> log;
    vector<string> res;
    for (int i = 0; i < names.size(); ++i)
        log[names[i]].push_back(stoi(times[i].substr(0, 2)) * 60 + stoi(times[i].substr(3)));
    for (auto &p : log) {
        sort(begin(p.second), end(p.second));
        for (int i = 0, j = 0; i < p.second.size(); ++i) {
            while(p.second[i] - p.second[j] > 60)
                ++j;
            if (i - j >= 2) {
                res.push_back(p.first);
                break;
            }
        }
    }
    sort(begin(res), end(res));
    return res;
}
```
</p>


### [SOLVED] Potentially wrong test cases?
- Author: rchaves
- Creation Date: Sun Oct 04 2020 00:00:58 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 08 2020 05:55:27 GMT+0800 (Singapore Standard Time)

<p>
Input: ["a","a","a","a","a","a","b","b","b","b","b"]
["23:27","03:14","12:57","13:35","13:18","21:58","22:39","10:49","19:37","14:14","10:41"]
Output: []
Expected: ["a"]

The input seems to be a time series. So every time an hour is lower than the previous one, you would expect to have a change in the current day.

I would expect "13:35" -> "13:18" to be in different days. What do you think?

(I was able to get Accepted by sorting all timestamps, but it doesn\'t seem intuitive)
</p>


