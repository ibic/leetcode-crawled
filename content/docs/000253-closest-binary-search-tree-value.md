---
title: "Closest Binary Search Tree Value"
weight: 253
#id: "closest-binary-search-tree-value"
---
## Description
<div class="description">
<p>Given a non-empty binary search tree and a target value, find the value in the BST that is closest to the target.</p>

<p><b>Note:</b></p>

<ul>
	<li>Given target value is a floating point.</li>
	<li>You are guaranteed to have only one unique value in the BST that is closest to the target.</li>
</ul>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> root = [4,2,5,1,3], target = 3.714286

    4
   / \
  2   5
 / \
1   3

<strong>Output:</strong> 4
</pre>

</div>

## Tags
- Binary Search (binary-search)
- Tree (tree)

## Companies
- Facebook - 18 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)
- Amazon - 5 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Snapchat - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Recursive Inorder + Linear search, O(N) time

**Intuition**

The simplest approach (3 lines in Python) is to build inorder traversal
and then find the closest element in a sorted array with built-in 
function `min`.  

![pic](../Figures/270/dummy.png)

This approach is simple stupid, and serves to identify the subproblems.

**Algorithm**

- Build an inorder traversal array.

- Find the closest to target element in that array.

**Implementation**

<iframe src="https://leetcode.com/playground/2FAkaVd3/shared" frameBorder="0" width="100%" height="378" name="2FAkaVd3"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ because to build inorder traversal
and then to perform linear search takes linear time.
* Space complexity : $$\mathcal{O}(N)$$ to keep inorder traversal.
<br /> 
<br />


---
#### Approach 2: Iterative Inorder, O(k) time

**Intuition**

Let's optimise Approach 1 in the case when index k of the closest 
element is much smaller than the tree heigh H.

First, one could merge both steps by traversing the tree and 
searching the closest value at the same time.  

Second, one could stop just after identifying the closest value, there 
is no need to traverse the whole tree. 
The closest value is found if the target value is 
in-between of two inorder array elements 
`nums[i] <= target < nums[i + 1]`. Then the closest value
is one of these elements.

![pic](../Figures/270/iteration.png)

**Algorithm**

- Initiate stack as an empty array and predecessor value as a very small 
number.

- While root is not null:

    - To build an inorder traversal iteratively, 
    go left as far as you can and add all nodes on the way into stack.
    
    - Pop the last element from stack `root = stack.pop()`. 

    - If target is in-between of `pred` and `root.val`, return 
    the closest between these two elements.
    
    - Set predecessor value to be equal to `root.val` and go one
    step right: `root = root.right`.
    
- We're here because during the loop one couldn't 
identify the closest value. That means that the closest value is
the last value in the inorder traversal, 
i.e. current predecessor value. Return it. 

**Implementation**

<iframe src="https://leetcode.com/playground/DP4RW2aP/shared" frameBorder="0" width="100%" height="412" name="DP4RW2aP"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(k)$$ in the average case and 
$$\mathcal{O}(H + k)$$ in the worst case, 
where k is an index of closest element. 
It's known that [average case is a balanced tree](https://pages.cpsc.ucalgary.ca/~jacobs/Courses/cpsc331/F08/notes/lecture17.pdf),
in that case stack always contains a few elements, and hence 
one does $$2k$$ operations to go to kth element in 
inorder traversal (k times to push into stack and 
then k times to pop out of stack). That results in
$$\mathcal{O}(k)$$ time complexity.
The worst case is a completely unbalanced tree, then you 
first push H elements into stack and then pop out k elements,
that results in $$\mathcal{O}(H + k)$$ time complexity.

![pic](../Figures/270/unbalanced.png)

* Space complexity : up to $$\mathcal{O}(H)$$ to keep the stack
in the case of non-balanced tree.
<br /> 
<br />


---
#### Approach 3: Binary Search, O(H) time 

**Intuition**

Approach 2 works fine when index k of closest element 
is much smaller than the tree height H.  

Let's now consider another limit and optimise Approach 1 in the 
case of relatively large k, comparable with N. 

Then it makes sense to use a binary search: 
go left if target is smaller than current root value,
and go right otherwise. Choose the closest to target value at each step.

![pic](../Figures/270/binary.png)

Kudos for this solution go to @[stefanpochmann](https://leetcode.com/stefanpochmann/).  

**Implementation**

<iframe src="https://leetcode.com/playground/DtzChTTs/shared" frameBorder="0" width="100%" height="242" name="DtzChTTs"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(H)$$ since here one goes from 
root down to a leaf.

* Space complexity : $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Clean and concise java solution
- Author: larrywang2014
- Creation Date: Thu Sep 24 2015 11:34:14 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 09:06:07 GMT+0800 (Singapore Standard Time)

<p>
    public int closestValue(TreeNode root, double target) {
        int ret = root.val;   
        while(root != null){
            if(Math.abs(target - root.val) < Math.abs(target - ret)){
                ret = root.val;
            }      
            root = root.val > target? root.left: root.right;
        }     
        return ret;
    }
</p>


### 4-7 lines recursive/iterative Ruby/C++/Java/Python
- Author: StefanPochmann
- Creation Date: Thu Aug 27 2015 04:04:39 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 09:11:32 GMT+0800 (Singapore Standard Time)

<p>
Same recursive/iterative solution in different languages.

---

Recursive
===

Closest is either the root's value (`a`) or the closest in the appropriate subtree (`b`).

**Ruby**

    def closest_value(root, target)
      a = root.val
      kid = target < a ? root.left : root.right or return a
      b = closest_value(kid, target)
      [b, a].min_by { |x| (x - target).abs }
    end

**C++**

    int closestValue(TreeNode* root, double target) {
        int a = root->val;
        auto kid = target < a ? root->left : root->right;
        if (!kid) return a;
        int b = closestValue(kid, target);
        return abs(a - target) < abs(b - target) ? a : b;
    }

**Java**

    public int closestValue(TreeNode root, double target) {
        int a = root.val;
        TreeNode kid = target < a ? root.left : root.right;
        if (kid == null) return a;
        int b = closestValue(kid, target);
        return Math.abs(a - target) < Math.abs(b - target) ? a : b;
    }

**Python**

    def closestValue(self, root, target):
        a = root.val
        kid = root.left if target < a else root.right
        if not kid: return a
        b = self.closestValue(kid, target)
        return min((b, a), key=lambda x: abs(target - x))

Alternative endings:

        return (b, a)[abs(a - target) < abs(b - target)]
        return a if abs(a - target) < abs(b - target) else b

---

Iterative
===

Walk the path down the tree close to the target, return the closest value on the path. Inspired by [yd](https://leetcode.com/discuss/54436/java-iterative-solution), I wrote these after reading "while loop".

**Ruby**

    def closest_value(root, target)
      path = []
      while root
        path << root.val
        root = target < root.val ? root.left : root.right
      end
      path.reverse.min_by { |x| (x - target).abs }
    end

The `.reverse` is only for handling targets much larger than 32-bit integer range, where different path values x have the same "distance" `(x - target).abs`. In such cases, the leaf value is the correct answer. If such large targets aren't asked, then it's unnecessary.

Or with O(1) space:

    def closest_value(root, target)
      closest = root.val
      while root
        closest = [root.val, closest].min_by { |x| (x - target).abs }
        root = target < root.val ? root.left : root.right
      end
      closest
    end

**C++**

    int closestValue(TreeNode* root, double target) {
        int closest = root->val;
        while (root) {
            if (abs(closest - target) >= abs(root->val - target))
                closest = root->val;
            root = target < root->val ? root->left : root->right;
        }
        return closest;
    }

**Python**

    def closestValue(self, root, target):
        path = []
        while root:
            path += root.val,
            root = root.left if target < root.val else root.right
        return min(path[::-1], key=lambda x: abs(target - x))

The `[::-1]` is only for handling targets much larger than 32-bit integer range, where different path values x have the same "distance" `(x - target).abs`. In such cases, the leaf value is the correct answer. If such large targets aren't asked, then it's unnecessary.

Or with O(1) space:

    def closestValue(self, root, target):
        closest = root.val
        while root:
            closest = min((root.val, closest), key=lambda x: abs(target - x))
            root = root.left if target < root.val else root.right
        return closest
</p>


### Clean python code
- Author: lime66
- Creation Date: Thu Feb 18 2016 16:27:08 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 04:33:57 GMT+0800 (Singapore Standard Time)

<p>
    class Solution(object):
        def closestValue(self, root, target):
            r = root.val
            while root:
                if abs(root.val - target) < abs(r - target):
                    r = root.val
                root = root.left if target < root.val else root.right
            return r
</p>


