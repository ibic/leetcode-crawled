---
title: "Replace Employee ID With The Unique Identifier"
weight: 1570
#id: "replace-employee-id-with-the-unique-identifier"
---
## Description
<div class="description">
<p>Table: <code>Employees</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| name          | varchar |
+---------------+---------+
id is the primary key for this table.
Each row of this table contains the id and the name of an employee in a company.
</pre>

<p>&nbsp;</p>

<p>Table: <code>EmployeeUNI</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| unique_id     | int     |
+---------------+---------+
(id, unique_id) is the primary key for this table.
Each row of this table contains the id and the corresponding unique id of an employee in the company.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to&nbsp;show&nbsp;the <strong>unique ID&nbsp;</strong>of each user, If a user doesn&#39;t have a unique ID replace just show null.</p>

<p>Return the result table in <strong>any</strong> order.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Employees</code> table:
+----+----------+
| id | name     |
+----+----------+
| 1  | Alice    |
| 7  | Bob      |
| 11 | Meir     |
| 90 | Winston  |
| 3  | Jonathan |
+----+----------+

<code>EmployeeUNI</code> table:
+----+-----------+
| id | unique_id |
+----+-----------+
| 3  | 1         |
| 11 | 2         |
| 90 | 3         |
+----+-----------+

<code>EmployeeUNI</code> table:
+-----------+----------+
| unique_id | name     |
+-----------+----------+
| null      | Alice    |
| null      | Bob      |
| 2         | Meir     |
| 3         | Winston  |
| 1         | Jonathan |
+-----------+----------+

Alice and Bob don&#39;t have a unique ID, We will show null instead.
The unique ID of Meir is 2.
The unique ID of Winston is 3.
The unique ID of Jonathan is 1.
</pre>
</div>

## Tags


## Companies
- Point72 - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL Simple Solution
- Author: ruddysimonpour
- Creation Date: Sat Mar 28 2020 03:17:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 28 2020 03:17:14 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT eu.unique_id,
        e.name
FROM Employees as e
LEFT JOIN EmployeeUNI as eu
ON e.id = eu.id;
```
</p>


### Extremely Easy Solution
- Author: joyjiang
- Creation Date: Mon Sep 14 2020 04:52:45 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 14 2020 04:52:45 GMT+0800 (Singapore Standard Time)

<p>
SELECT unique_id,name
FROM Employees e1 Left JOIN EmployeeUNI e2
ON e1.id = e2.id

</p>


### Any db, left join, 100%
- Author: nikopol86
- Creation Date: Fri Sep 11 2020 14:10:42 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 11 2020 14:10:42 GMT+0800 (Singapore Standard Time)

<p>
```
select euni.unique_id, e.name
from employees e left join employeeuni euni on euni.id = e.id
```
</p>


