---
title: "Remove Duplicates from Sorted Array"
weight: 26
#id: "remove-duplicates-from-sorted-array"
---
## Description
<div class="description">
<p>Given a sorted array <em>nums</em>, remove the duplicates <a href="https://en.wikipedia.org/wiki/In-place_algorithm" target="_blank"><strong>in-place</strong></a> such that each element appears only <em>once</em> and returns the new length.</p>

<p>Do not allocate extra space for another array, you must do this by <strong>modifying the input array <a href="https://en.wikipedia.org/wiki/In-place_algorithm" target="_blank">in-place</a></strong> with O(1) extra memory.</p>

<p><strong>Example 1:</strong></p>

<pre>
Given <em>nums</em> = <strong>[1,1,2]</strong>,

Your function should return length = <strong><code>2</code></strong>, with the first two elements of <em><code>nums</code></em> being <strong><code>1</code></strong> and <strong><code>2</code></strong> respectively.

It doesn&#39;t matter what you leave beyond the returned length.</pre>

<p><strong>Example 2:</strong></p>

<pre>
Given <em>nums</em> = <strong>[0,0,1,1,1,2,2,3,3,4]</strong>,

Your function should return length = <strong><code>5</code></strong>, with the first five elements of <em><code>nums</code></em> being modified to&nbsp;<strong><code>0</code></strong>, <strong><code>1</code></strong>, <strong><code>2</code></strong>, <strong><code>3</code></strong>, and&nbsp;<strong><code>4</code></strong> respectively.

It doesn&#39;t matter what values are set beyond&nbsp;the returned length.
</pre>

<p><strong>Clarification:</strong></p>

<p>Confused why the returned value is an integer but your answer is an array?</p>

<p>Note that the input array is passed in by <strong>reference</strong>, which means a modification to the input array will be known to the caller as well.</p>

<p>Internally you can think of this:</p>

<pre>
// <strong>nums</strong> is passed in by reference. (i.e., without making a copy)
int len = removeDuplicates(nums);

// any modification to <strong>nums</strong> in your function would be known by the caller.
// using the length returned by your function, it prints the first <strong>len</strong> elements.
for (int i = 0; i &lt; len; i++) {
&nbsp; &nbsp; print(nums[i]);
}</pre>

</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)

## Companies
- Facebook - 4 (taggedByAdmin: true)
- Amazon - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: true)
- ServiceNow - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Oracle - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Hulu - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Two Pointers

**Algorithm**

Since the array is already sorted, we can keep two pointers $$i$$ and $$j$$, where $$i$$ is the slow-runner while $$j$$ is the fast-runner. As long as $$nums[i] = nums[j]$$, we increment $$j$$ to skip the duplicate.

When we encounter $$nums[j] \neq nums[i]$$, the duplicate run has ended so we must copy its value to $$nums[i + 1]$$. $$i$$ is then incremented and we repeat the same process again until $$j$$ reaches the end of array.

<iframe src="https://leetcode.com/playground/p8jPfpcx/shared" frameBorder="0" width="100%" height="242" name="p8jPfpcx"></iframe>

**Complexity analysis**

* Time complextiy : $$O(n)$$.
Assume that $$n$$ is the length of array. Each of $$i$$ and $$j$$ traverses at most $$n$$ steps.

* Space complexity : $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Share my clean C++ code
- Author: jasusy
- Creation Date: Sun Feb 15 2015 16:14:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 09:00:56 GMT+0800 (Singapore Standard Time)

<p>
    int count = 0;
    for(int i = 1; i < n; i++){
        if(A[i] == A[i-1]) count++;
        else A[i-count] = A[i];
    }
    return n-count;
</p>


### My Solution : Time O(n), Space O(1)
- Author: liyangguang1988
- Creation Date: Wed Sep 03 2014 17:22:21 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 20:33:21 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
        public:
        int removeDuplicates(int A[], int n) {
            if(n < 2) return n;
            int id = 1;
            for(int i = 1; i < n; ++i) 
                if(A[i] != A[i-1]) A[id++] = A[i];
            return id;
        }
    };
</p>


### 5 lines C++/Java, nicer loops
- Author: StefanPochmann
- Creation Date: Sat Jun 27 2015 20:29:00 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 15:31:24 GMT+0800 (Singapore Standard Time)

<p>
I don't like old-style indexed looping. I much prefer the "enhanced" (Java) / "range-based" (C++) loops, they make things much cleaner.

---

**C++**

    int removeDuplicates(vector<int>& nums) {
        int i = 0;
        for (int n : nums)
            if (!i || n > nums[i-1])
                nums[i++] = n;
        return i;
    }

And to not need the `!i` check in the loop:

    int removeDuplicates(vector<int>& nums) {
        int i = !nums.empty();
        for (int n : nums)
            if (n > nums[i-1])
                nums[i++] = n;
        return i;
    }

---

**Java**

    public int removeDuplicates(int[] nums) {
        int i = 0;
        for (int n : nums)
            if (i == 0 || n > nums[i-1])
                nums[i++] = n;
        return i;
    }

And to not need the `i == 0` check in the loop:

    public int removeDuplicates(int[] nums) {
        int i = nums.length > 0 ? 1 : 0;
        for (int n : nums)
            if (n > nums[i-1])
                nums[i++] = n;
        return i;
    }
</p>


