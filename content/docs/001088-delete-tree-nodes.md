---
title: "Delete Tree Nodes"
weight: 1088
#id: "delete-tree-nodes"
---
## Description
<div class="description">
<p>A tree rooted at node 0 is given as follows:</p>

<ul>
	<li>The number of nodes is <code>nodes</code>;</li>
	<li>The value of the <code>i</code>-th node is <code>value[i]</code>;</li>
	<li>The parent of the <code>i</code>-th node is <code>parent[i]</code>.</li>
</ul>

<p>Remove every subtree whose sum of values of nodes is zero.</p>

<p>After doing so, return the number of nodes remaining in the tree.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/07/02/1421_sample_1.PNG" style="width: 403px; height: 347px;" />
<pre>
<strong>Input:</strong> nodes = 7, parent = [-1,0,0,1,2,2,2], value = [1,-2,4,0,-2,-1,-1]
<strong>Output:</strong> 2
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nodes = 7, parent = [-1,0,0,1,2,2,2], value = [1,-2,4,0,-2,-1,-2]
<strong>Output:</strong> 6
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nodes = 5, parent = [-1,0,1,0,0], value = [-672,441,18,728,378]
<strong>Output:</strong> 5
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nodes = 5, parent = [-1,0,0,1,1], value = [-686,-842,616,-739,-746]
<strong>Output:</strong> 5
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nodes &lt;= 10^4</code></li>
	<li><code>parent.length == nodes</code></li>
	<li><code>0 &lt;= parent[i] &lt;= nodes - 1</code></li>
	<li><code>parent[0] == -1</code>&nbsp;which indicates that <code>0</code> is the root.</li>
	<li><code>value.length == nodes</code></li>
	<li><code>-10^5 &lt;= value[i] &lt;= 10^5</code></li>
	<li>The given input is <strong>guaranteed</strong> to represent a <strong>valid tree</strong>.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Depth-first Search (depth-first-search)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] One pass
- Author: lee215
- Creation Date: Sun Dec 01 2019 00:20:42 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 02 2019 21:11:47 GMT+0800 (Singapore Standard Time)

<p>
# Solution 1: DFS
Build up the mapping of parent and its children.
Recursively find the sum and the count of subtree.
Time `O(N)`, Space `O(N)`
<br>

**Python:**
```python
    def deleteTreeNodes(self, n, parent, value):
        sons = {i: set() for i in xrange(n)}
        for i, p in enumerate(parent):
            if i: sons[p].add(i)

        def dfs(x):
            total, count = value[x], 1
            for y in sons[x]:
                t, c = dfs(y)
                total += t
                count += c
            return total, count if total else 0
        return dfs(0)[1]
```
<br>

# Solution 2: One Pass
Wrote this solution in 2019-11-30,
don\'t have the premium so I\'ll lose the access to my this post.

Hidden condition:
`parernt[i] < i` for all `i > 0`
a parent always has have smaller index of its children.
<br>

# Intuition
Don\'t ask me why, my friend told me.
He said it was an intuition.
We hardly see a tree problem represented by array.
Another observation is tha, no test case contains a tree with `sum = 0`.

The problem writer was lasy and made some random cases.
I personally think taht he didn\'t actually do the work.
<br>

# **Complexity**
Time `O(N)`
Space `O(N)`
<br>

**C++**
```cpp
    int deleteTreeNodes(int n, vector<int>& parent, vector<int>& value) {
        vector<int> res(n);
        for (int i = n - 1; i > 0; --i) {
            value[parent[i]] += value[i];
            res[parent[i]] += value[i] ? res[i] + 1 : 0;
        }
        return value[0] ? res[0] + 1 : 0;
    }
```

**Java**
```java
    public int deleteTreeNodes(int n, int[] parent, int[] value) {
        int[] res = new int[n];
        for (int i = n - 1; i > 0; --i) {
            value[parent[i]] += value[i];
            res[parent[i]] += value[i] != 0 ? res[i] + 1 : 0;
        }
        return value[0] != 0 ? res[0] + 1 : 0;
    }
```
**Python**
```py
    def deleteTreeNodes(self, n, parent, value):
        res = [1] * n
        for i in xrange(n - 1, 0, -1):
            value[parent[i]] += value[i]
            res[parent[i]] += res[i] if value[i] else 0
        return res[0] if value[0] else 0
```
</p>


### [Java] DFS - Time O(N) - Clean code
- Author: hiepit
- Creation Date: Sun Dec 01 2019 01:47:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 01 2019 01:58:25 GMT+0800 (Singapore Standard Time)

<p>
**Explain:** Comments in the source code
**Time complexity**: O(N), N is the number of nodes in the tree, N <= 10^4
```java
class Solution {
    public int deleteTreeNodes(int n, int[] parent, int[] value) {
        List<List<Integer>> graph = new ArrayList<>(n); // Create graph for the tree
        for (int i = 0; i < n; i++) graph.add(new ArrayList<>());
        for (int i = 0; i < n; i++) {
            if (parent[i] != -1) {
                graph.get(parent[i]).add(i);
            }
        }
        return dfs(graph, value, 0)[1];
    }

    private int[] dfs(List<List<Integer>> graph, int[] value, int root) {
        int sum = value[root];
        int cntRemainNodes = 1;
        for (int child : graph.get(root)) {
            int[] temp = dfs(graph, value, child);
            sum += temp[0];
            cntRemainNodes += temp[1];
        }
        if (sum == 0) cntRemainNodes = 0; // Don\'t count nodes of this subtree
        return new int[]{sum, cntRemainNodes};
    }
}
```
</p>


### Um, how are we supposed to remove subtrees?
- Author: StefanPochmann
- Creation Date: Sun Dec 01 2019 00:21:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 01 2019 00:23:26 GMT+0800 (Singapore Standard Time)

<p>
The problem says *"Remove every subtree [...]"* and it\'s not clear to me how to do that. It\'s not like we\'re given node objects with pointers that we can set to `null` or so.

Shall we remove their entries from the given lists and update the remaining list values accordingly? What about `nodes`? That\'s a local variable and even if I change it, this won\'t be visible to the caller.
</p>


