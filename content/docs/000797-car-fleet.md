---
title: "Car Fleet"
weight: 797
#id: "car-fleet"
---
## Description
<div class="description">
<p><code>N</code> cars are going to the same destination along a one lane road.&nbsp; The destination is <code>target</code>&nbsp;miles away.</p>

<p>Each car <code>i</code>&nbsp;has a constant speed <code>speed[i]</code>&nbsp;(in miles per hour), and initial position <code>position[i]</code>&nbsp;miles towards the target along the road.</p>

<p>A car can never pass another car ahead of it, but it can catch up to it, and drive bumper to bumper at the same speed.</p>

<p>The distance between these two cars is ignored - they are assumed to have the same position.</p>

<p>A <em>car fleet</em> is some non-empty set of cars driving&nbsp;at the same position and same speed.&nbsp; Note that a single car is also a car fleet.</p>

<p>If a car catches up to a car fleet right at the destination point, it will&nbsp;still be&nbsp;considered as one car fleet.</p>

<p><br />
How many car fleets will arrive at the destination?</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>target = <span id="example-input-1-1">12</span>, position = <span id="example-input-1-2">[10,8,0,5,3]</span>, speed = <span id="example-input-1-3">[2,4,1,1,3]</span>
<strong>Output: </strong><span id="example-output-1">3</span>
<strong>Explanation</strong>:
The cars starting at 10 and 8 become a fleet, meeting each other at 12.
The car starting at 0 doesn&#39;t catch up to any other car, so it is a fleet by itself.
The cars starting at 5 and 3 become a fleet, meeting each other at 6.
Note that no other cars meet these fleets before the destination, so the answer is 3.
</pre>

<p><br />
<strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= N &lt;= 10 ^ 4</code></li>
	<li><code>0 &lt; target&nbsp;&lt;= 10 ^ 6</code></li>
	<li><code>0 &lt;&nbsp;speed[i] &lt;= 10 ^ 6</code></li>
	<li><code>0 &lt;= position[i] &lt; target</code></li>
	<li>All initial positions are different.</li>
</ol>
</div>

## Tags
- Sort (sort)

## Companies
- Nutanix - 5 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach 1: Sort

**Intuition**

Call the "lead fleet" the fleet furthest in position.

If the car `S` (Second) behind the lead car `F` (First) would arrive earlier, then `S` forms a fleet with the lead car `F`.  Otherwise, fleet `F` is final as no car can catch up to it - cars behind `S` would form fleets with `S`, never `F`.

**Algorithm**

A car is a `(position, speed)` which implies some arrival time `(target - position) / speed`.  Sort the cars by position.

Now apply the above reasoning - if the lead fleet drives away, then count it and continue.  Otherwise, merge the fleets and continue.

<iframe src="https://leetcode.com/playground/V9ZbFCSw/shared" frameBorder="0" width="100%" height="497" name="V9ZbFCSw"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the number of cars.  The complexity is dominated by the sorting operation.

* Space Complexity:  $$O(N)$$, the space used to store information about the cars.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Jun 17 2018 11:05:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Apr 18 2020 11:50:34 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
Sort cars by the start positions `pos`
Loop on each car from the end to the beginning
Calculate its `time` needed to arrive the `target`
`cur` records the current biggest time (the slowest)

If another car needs less or equal time than `cur`,
it can catch up this car fleet.

If another car needs more time,
it will be the new slowest car,
and becomes the new lead of a car fleet.
<br>

# **Complexity**:
`O(NlogN)` Quick sort the cars by position. (Other sort can be applied)
`O(N)` One pass for all cars from the end to start (another direction also works).

`O(N)` Space for sorted cars.
`O(1)` space is possible if we sort `pos` inplace.
<br>


**Java, use Array**
```java
    public int carFleet(int target, int[] pos, int[] speed) {
        int N = pos.length, res = 0;
        double[][] cars = new double[N][2];
        for (int i = 0; i < N; ++i)
            cars[i] = new double[] {pos[i], (double)(target - pos[i]) / speed[i]};
        Arrays.sort(cars, (a, b) -> Double.compare(a[0], b[0]));
        double cur = 0;
        for (int i = N - 1; i >= 0 ; --i) {
            if (cars[i][1] > cur) {
                cur = cars[i][1];
                res++;
            }
        }
        return res;
    }
```

**Java, use TreeMap:**
```java
    public int carFleet(int target, int[] pos, int[] speed) {
        Map<Integer, Double> m = new TreeMap<>(Collections.reverseOrder());
        for (int i = 0; i < pos.length; ++i)
            m.put(pos[i], (double)(target - pos[i]) / speed[i]);
        int res = 0; double cur = 0;
        for (double time : m.values()) {
            if (time > cur) {
                cur = time;
                res++;
            }
        }
        return res;
    }
```

**C++, use map:**
```cpp
    int carFleet(int target, vector<int>& pos, vector<int>& speed) {
        map<int, double> m;
        for (int i = 0; i < pos.size(); i++) m[-pos[i]] = (double)(target - pos[i]) / speed[i];
        int res = 0; double cur = 0;
        for (auto it : m) if (it.second > cur) cur = it.second, res++;
        return res;
    }
```

**Python:**
```py
    def carFleet(self, target, pos, speed):
        time = [float(target - p) / s for p, s in sorted(zip(pos, speed))]
        res = cur = 0
        for t in time[::-1]:
            if t > cur:
                res += 1
                cur = t
        return res
```

</p>


### Easy understanding JAVA TreeMap Solution with explanation & comment
- Author: tysu
- Creation Date: Sun Jun 17 2018 18:50:26 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 08:45:39 GMT+0800 (Singapore Standard Time)

<p>
If one car catch up the one before it, it means the time it takes to reach the target is shorter than the one in front(if no blocking).
For example:
car A at pos a with speed sa
car B at pos b with speed sb
(b < a < target)
Their distances to the target are (target-a) and (target-b).
If (target-a)/sa > (target-b)/sb it means car A take more time to reach target so car B will catch up car A and form a single group.

So we use the distance to target as key and speed as value, iterate through all cars in order of their distances to the target.
keep track of currently slowest one(which might block the car behind), if a car can catch up current slowest one, it will not cause a new group.
Otherwise, we count a new group and update the info of slowest

```
class Solution {
    public int carFleet(int target, int[] position, int[] speed) {
        TreeMap<Integer, Integer> map = new TreeMap<>();
        int n = position.length;
        for(int i=0; i<n; ++i){
            map.put(target - position[i], speed[i]);
        }
        int count = 0;
        double r = -1.0;
	/*for all car this value must > 0, so we can count for the car closeset to target*/
        for(Map.Entry<Integer, Integer> entry: map.entrySet()){
            int d = entry.getKey(); // distance
            int s = entry.getValue(); // speed
            double t = 1.0*d/s; // time to target
            if(t>r){ // this car is unable to catch up previous one, form a new group and update the value
                ++count;
                r = t;
            }
        }
        return count;
    }
}
```
Time: O(nlogn)
Space: O(n)
Runtime: 40 ms
</p>


### [C++] 97% | O(NlogN), O(N) | <position, arrival time> + sort
- Author: dhdnr1220
- Creation Date: Sat Jan 11 2020 23:04:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jan 11 2020 23:04:52 GMT+0800 (Singapore Standard Time)

<p>
```
int Solution::carFleet(int target, vector<int>& position, vector<int>& speed)
{
	if (position.empty() || speed.empty()) return 0;
	
	vector<pair<int, double>> intervals; // start position, arrival time
    for (int i = 0; i < position.size(); i++)
		intervals.push_back(make_pair(position[i], (double)(target - position[i])/(double)(speed[i])));
    sort(intervals.begin(), intervals.end());
	
	int fleetCnt = 0; pair<int,double> p;
	p = intervals[intervals.size()-1];
	for (int i = intervals.size()-2; i>=0; i--) {
        if (intervals[i].second > p.second) {
			fleetCnt++; p = intervals[i];
		}
	}	
	return fleetCnt+1;
}
```
</p>


