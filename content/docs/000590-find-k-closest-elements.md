---
title: "Find K Closest Elements"
weight: 590
#id: "find-k-closest-elements"
---
## Description
<div class="description">
<p>Given a sorted array <code>arr</code>, two integers <code>k</code> and <code>x</code>, find the <code>k</code> closest elements to <code>x</code> in the array. The result should also be sorted in ascending order. If there is a tie, the smaller elements are always preferred.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> arr = [1,2,3,4,5], k = 4, x = 3
<strong>Output:</strong> [1,2,3,4]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> arr = [1,2,3,4,5], k = 4, x = -1
<strong>Output:</strong> [1,2,3,4]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= k &lt;= arr.length</code></li>
	<li><code>1 &lt;= arr.length&nbsp;&lt;= 10^4</code></li>
	<li>Absolute value of elements in the array and <code>x</code> will not exceed 10<sup>4</sup></li>
</ul>

</div>

## Tags
- Binary Search (binary-search)

## Companies
- Facebook - 12 (taggedByAdmin: false)
- Apple - 5 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Snapchat - 3 (taggedByAdmin: false)

## Official Solution
[TOC]


## Solution

---
#### Approach 1: Using Collection.sort()

**Algorithm**

Intuitively, we can sort the elements in list `arr` by their absolute difference values to the target `x`. Then the sublist of the first k elements is the result after sorting the elements by the natural order.

<iframe src="https://leetcode.com/playground/pPUrT4oY/shared" frameBorder="0" width="100%" height="157" name="pPUrT4oY"></iframe>

Note: This solution is inspired by [@compton_scatter](https://discuss.leetcode.com/user/compton_scatter).

**Complexity Analysis**

* Time complexity : $$O(n\log n)$$. Collections.sort() uses binary sort so it has a $$O(n\log n)$$ complexity.

* Space complexity : $$O(k)$$. The in-place sorting does not consume any extra space. However, generating a k length sublist will take some space.
<br>
<br>

---
#### Approach 2: Binary Search and Two Pointers

**Algorithm**

The original array has been sorted so we can take this advantage by the following steps.
1. If the target `x` is less or equal than the first element in the sorted array, the first `k` elements are the result.
2. Similarly, if the target `x` is more or equal than the last element in the sorted array, the last `k` elements are the result.
3. Otherwise, we can use binary search to find the `index` of the element, which is equal (when this list has `x`) or a little bit larger than `x` (when this list does not have it). Then set `low` to its left `k-1` position, and `high` to the right `k-1` position of this `index` as a start. The desired k numbers must in this rang [index-k-1, index+k-1]. So we can shrink this range to get the result using the following rules.
    * If `low` reaches the lowest index `0` or the `low` element is closer to `x` than the `high` element, decrease the `high` index.
    * If `high` reaches to the highest index `arr.size()-1` or it is nearer to `x` than the `low` element, increase the `low` index.
    * The looping ends when there are exactly k elements in [low, high], the subList of which is the result.

<iframe src="https://leetcode.com/playground/kS5bGpg6/shared" frameBorder="0" width="100%" height="480" name="kS5bGpg6"></iframe>

**Complexity Analysis**

* Time complexity : $$O(\log n +k)$$. $$O(\log n)$$ is for the time of binary search, while $$O(k)$$ is for shrinking the index range to k elements.

* Space complexity : $$O(k)$$. It is to generate the required sublist.


Analysis written by: [@Mr.Bin](https://discuss.leetcode.com/user/mr-bin)

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Binary Search, O(log(N-K) + K)
- Author: lee215
- Creation Date: Sun Aug 13 2017 12:19:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Apr 29 2020 14:11:04 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
The array is sorted.
If we want find the one number closest to `x`,
we don\'t have to check one by one.
it\'s straightforward to use binary research.

Now we want the `k` closest,
the logic should be similar.
<br>

# Explanation
Assume we are taking `A[i] ~ A[i + k -1]`.
We can binary research `i`
We compare the distance between `x - A[mid]` and `A[mid + k] - x`


@vincent_gui listed the following cases:
Assume `A[mid] ~ A[mid + k]` is sliding window

case 1: x - A[mid] < A[mid + k] - x, need to move window go left
-------x----A[mid]-----------------A[mid + k]----------

case 2: x - A[mid] < A[mid + k] - x, need to move window go left again
-------A[mid]----x-----------------A[mid + k]----------

case 3: x - A[mid] > A[mid + k] - x, need to move window go right
-------A[mid]------------------x---A[mid + k]----------

case 4: x - A[mid] > A[mid + k] - x, need to move window go right
-------A[mid]---------------------A[mid + k]----x------


If `x - A[mid] > A[mid + k] - x`,
it means `A[mid + 1] ~ A[mid + k]` is better than `A[mid] ~ A[mid + k - 1]`,
and we have `mid` smaller than the right `i`.
So assign `left = mid + 1`.


# Important
Note that, you **SHOULD NOT** compare the **absolute value** of `abs(x - A[mid])` and `abs(A[mid + k] - x)`.
It fails at cases like `A = [1,1,2,2,2,2,2,3,3]`, `x = 3`, `k = 3`

The problem is interesting and good.
Unfortunately the test cases is terrible.
The worst part of Leetcode test cases is that,
you submit a wrong solution but get accepted.

You didn\'t read my post and up-vote carefully,
then you miss this key point.
<br>

# Complexity
Time `O(log(N - K))` to binary research and find result
Space `O(K)` to create the returned list.

<br>

**Java:**
```java
    public List<Integer> findClosestElements(int[] A, int k, int x) {
        int left = 0, right = A.length - k;
        while (left < right) {
            int mid = (left + right) / 2;
            if (x - A[mid] > A[mid + k] - x)
                left = mid + 1;
            else
                right = mid;
        }
        return Arrays.stream(A, left, left + k).boxed().collect(Collectors.toList());
    }
```
<br>

**C++:**
```cpp
    vector<int> findClosestElements(vector<int>& A, int k, int x) {
        int left = 0, right = A.size() - k;
        while (left < right) {
            int mid = (left + right) / 2;
            if (x - A[mid] > A[mid + k] - x)
                left = mid + 1;
            else
                right = mid;
        }
        return vector<int>(A.begin() + left, A.begin() + left + k);
    }
```

<br>

**Python:**
```python
    def findClosestElements(self, A, k, x):
        left, right = 0, len(A) - k
        while left < right:
            mid = (left + right) / 2
            if x - A[mid] > A[mid + k] - x:
                left = mid + 1
            else:
                right = mid
        return A[left:left + k]
```
</p>


### Very simple Java O(n) solution using two pointers
- Author: makovkastar
- Creation Date: Sat Dec 08 2018 05:33:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Dec 08 2018 05:33:55 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public List<Integer> findClosestElements(int[] arr, int k, int x) {
        int lo = 0;
		int hi = arr.length - 1;
		while (hi - lo >= k) {
			if (Math.abs(arr[lo] - x) > Math.abs(arr[hi] - x)) {
				lo++;
			} else {
				hi--;
			}
		}
		List<Integer> result = new ArrayList<>(k);
		for (int i = lo; i <= hi; i++) {
			result.add(arr[i]);
		}
		return result;
    }
}
```
</p>


### O(log n) Java, 1 line O(log(n) + k) Ruby
- Author: StefanPochmann
- Creation Date: Sun Aug 13 2017 21:41:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 20:43:28 GMT+0800 (Singapore Standard Time)

<p>
I binary-search for where the resulting elements start in the array. It's the first index `i` so that `arr[i]` is better than `arr[i+k]` (with "better" meaning closer to or equally close to `x`). Then I just return the `k` elements starting there.
```
def find_closest_elements(arr, k, x)
  arr[(0..arr.size).bsearch { |i| x - arr[i] <= (arr[i+k] || 1/0.0) - x }, k]
end
```
I think that's simpler than binary-searching for `x` and then expanding to the left and to the right like I've seen in other binary search solutions.

---

Here's a Java version without using the library's binary search:

    public List<Integer> findClosestElements(List<Integer> arr, int k, int x) {
        int lo = 0, hi = arr.size() - k;
        while (lo < hi) {
            int mid = (lo + hi) / 2;
            if (x - arr.get(mid) > arr.get(mid+k) - x)
                lo = mid + 1;
            else
                hi = mid;
        }
        return arr.subList(lo, lo + k);
    }
The binary search costs O(log n) (actually also just O(log (n-k)) and the `subList` call probably only takes O(1). As @sagimann [pointed out](https://discuss.leetcode.com/post/211008), `subList` [returns a view](https://docs.oracle.com/javase/8/docs/api/java/util/List.html#subList-int-int-), not a separate copy. So it should only take O(1). Can be seen for example in `ArrayList`'s [subList](http://hg.openjdk.java.net/jdk8/jdk8/jdk/file/687fd7c7986d/src/share/classes/java/util/ArrayList.java#l980) and the [`SubList` constructor](http://hg.openjdk.java.net/jdk8/jdk8/jdk/file/687fd7c7986d/src/share/classes/java/util/ArrayList.java#l1001) it calls. I also checked `LinkedList`, it gets its `subList` method inherited from `AbstractList`, where it also takes only O(1). And `AbstractList` is a basis for most lists.

**Edit:** I also [implemented it in Go](https://discuss.leetcode.com/topic/99831/o-log-n) now, to make it O(log n). **Edit 2:** Ha, didn't have to do that, as the Java version apparently already was O(log n) (I didn't originally know Java returns a view, only added that now). **Edit 3:** Lol, I had mistakenly written "Python" in the title instead of "Ruby" but apparently nobody noticed (and it's at 1800 views). Fixed that now.
</p>


