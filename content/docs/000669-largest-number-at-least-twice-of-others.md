---
title: "Largest Number At Least Twice of Others"
weight: 669
#id: "largest-number-at-least-twice-of-others"
---
## Description
<div class="description">
<p>In a given integer array <code>nums</code>, there is always exactly one largest element.</p>

<p>Find whether the largest element in the array is at least twice as much as every other number in the array.</p>

<p>If it is, return the <strong>index</strong> of the largest element, otherwise return -1.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [3, 6, 1, 0]
<strong>Output:</strong> 1
<strong>Explanation:</strong> 6 is the largest integer, and for every other number in the array x,
6 is more than twice as big as x.  The index of value 6 is 1, so we return 1.
</pre>

<p>&nbsp;</p>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1, 2, 3, 4]
<strong>Output:</strong> -1
<strong>Explanation:</strong> 4 isn&#39;t at least as big as twice the value of 3, so we return -1.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>nums</code> will have a length in the range <code>[1, 50]</code>.</li>
	<li>Every <code>nums[i]</code> will be an integer in the range <code>[0, 99]</code>.</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags
- Array (array)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Linear Scan [Accepted]

**Intuition and Algorithm**

Scan through the array to find the unique largest element `m`, keeping track of it's index `maxIndex`.

Scan through the array again.  If we find some `x != m` with `m < 2*x`, we should return `-1`.

Otherwise, we should return `maxIndex`.

<iframe src="https://leetcode.com/playground/ppgATcK2/shared" frameBorder="0" width="100%" height="293" name="ppgATcK2"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ where $$N$$ is the length of `nums`.

* Space Complexity: $$O(1)$$, the space used by our `int` variables.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### One Pass O(N) Java Solution 9ms 100%
- Author: Cique
- Creation Date: Sun Jul 22 2018 00:49:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 29 2018 09:33:36 GMT+0800 (Singapore Standard Time)

<p>

```
class Solution {
    public int dominantIndex(int[] nums) {
        int max = -1, index = -1, second = -1;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > max) {
                second = max;
                max = nums[i];
                index = i;
            } else if (nums[i] > second)
                second = nums[i];
        }
        return second * 2 <= max ? index : -1;
    }
}
```
</p>


### Python O(n) time and O(1) space without fancy builtins
- Author: SunnyvaleCA
- Creation Date: Mon Dec 25 2017 06:43:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 06:14:32 GMT+0800 (Singapore Standard Time)

<p>
Just iterate through the array and note the highest and second highest numbers. Might as well take note of the index at the same time.

One slightly clever idea was to shuffle the highest to the second-highest whenever a new highest was found. That was a way to handle the case where there are two (or more) value tied for highest.

```
class Solution:
    def dominantIndex(self, nums):
        if len(nums) == 0: return -1

        highest = -1
        secondHighest = -1
        highestIndex = 0
        
        for i,n in enumerate(nums):
            if n >= highest:
                secondHighest = highest
                highest = n
                highestIndex = i
            elif n > secondHighest:
                secondHighest = n

        if highest < secondHighest*2:
            highestIndex = -1
        
        return highestIndex

```
</p>


### Wrong return with [1] and with lists that have zeroes
- Author: michaelkareev
- Creation Date: Sun Sep 30 2018 23:59:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 00:35:37 GMT+0800 (Singapore Standard Time)

<p>
How come the test shows that when the input is [1], the output should be 0, not -1?
1 is not at least twice as large as any element on the list.

Anyway, here is a 2-pass solution:

```
def dominantIndex(nums):
    if len(nums) == 0:
        return -1
    if len(nums) == 1:
        return 0
    largest_ix = nums.index(max(nums))
    for n in nums[:largest_ix] + nums[largest_ix + 1:]:
        if nums[largest_ix] < 2 * n:
            return -1
    return largest_ix
```
Passes all tests.

And here is a 1-pass solution that fails a [0, 0, 3, 2] case. This problem is discussed by other users: you cannot say that any number is twice or more as large as zero. So, it\'s another problem of the question.

```
import math
def dominantIndex_1pass(nums): 
    if len(nums) == 0:
        return -1
    if len(nums) == 1:
        return 0
    m_ix, glob_max = -1, -math.inf
    for i in range(len(nums)):
        if nums[i] > glob_max:
            if nums[i] >= 2 * glob_max:
                m_ix = i
                glob_max = nums[i]
            else:
                m_ix = -1
                glob_max = nums[i]
    return m_ix
```
</p>


