---
title: "Minimum Absolute Difference in BST"
weight: 499
#id: "minimum-absolute-difference-in-bst"
---
## Description
<div class="description">
<p>Given a binary search tree with non-negative values, find the minimum <a href="https://en.wikipedia.org/wiki/Absolute_difference">absolute difference</a> between values of any two nodes.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b>

   1
    \
     3
    /
   2

<b>Output:</b>
1

<b>Explanation:</b>
The minimum absolute difference is 1, which is the difference between 2 and 1 (or between 2 and 3).
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ul>
	<li>There are at least two nodes in this BST.</li>
	<li>This question is the same as 783:&nbsp;<a href="https://leetcode.com/problems/minimum-distance-between-bst-nodes/">https://leetcode.com/problems/minimum-distance-between-bst-nodes/</a></li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Apple - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Two Solutions, in-order traversal and a more general way using TreeSet
- Author: shawngao
- Creation Date: Sun Feb 26 2017 12:26:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 16:40:16 GMT+0800 (Singapore Standard Time)

<p>
The most common idea is to first ```inOrder``` traverse the tree and compare the delta between each of the adjacent values. It's guaranteed to have the correct answer because it is a ```BST``` thus ```inOrder``` traversal values are ```sorted```.

Solution 1 - In-Order traverse, time complexity O(N), space complexity O(1).
```
public class Solution {
    int min = Integer.MAX_VALUE;
    Integer prev = null;
    
    public int getMinimumDifference(TreeNode root) {
        if (root == null) return min;
        
        getMinimumDifference(root.left);
        
        if (prev != null) {
            min = Math.min(min, root.val - prev);
        }
        prev = root.val;
        
        getMinimumDifference(root.right);
        
        return min;
    }
    
}
```
What  if it is not a ```BST```? (Follow up of the problem) The idea is to put values in a TreeSet and then every time we can use ```O(lgN)``` time to lookup for the nearest values.

Solution 2 - Pre-Order traverse, time complexity O(NlgN), space complexity O(N).
```
public class Solution {
    TreeSet<Integer> set = new TreeSet<>();
    int min = Integer.MAX_VALUE;
    
    public int getMinimumDifference(TreeNode root) {
        if (root == null) return min;
        
        if (!set.isEmpty()) {
            if (set.floor(root.val) != null) {
                min = Math.min(min, root.val - set.floor(root.val));
            }
            if (set.ceiling(root.val) != null) {
                min = Math.min(min, set.ceiling(root.val) - root.val);
            }
        }
        
        set.add(root.val);
        
        getMinimumDifference(root.left);
        getMinimumDifference(root.right);
        
        return min;
    }
}
```
</p>


### Java O(n) Time Inorder Traversal Solution
- Author: compton_scatter
- Creation Date: Sun Feb 26 2017 12:01:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 26 2017 12:01:07 GMT+0800 (Singapore Standard Time)

<p>
Since this is a BST, the inorder traversal of its nodes results in a sorted list of values. Thus, the minimum absolute difference must occur in any adjacently traversed nodes. I use the global variable "prev" to keep track of each node's inorder predecessor.

```
public class Solution {
    
    int minDiff = Integer.MAX_VALUE;
    TreeNode prev;
    
    public int getMinimumDifference(TreeNode root) {
        inorder(root);
        return minDiff;
    }
    
    public void inorder(TreeNode root) {
        if (root == null) return;
        inorder(root.left);
        if (prev != null) minDiff = Math.min(minDiff, root.val - prev.val);
        prev = root;
        inorder(root.right);
    }

}
```
</p>


### C++ 7 lines, O(n) run-time O(h) memory
- Author: votrubac
- Creation Date: Sun Feb 26 2017 12:55:39 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 18 2018 22:35:25 GMT+0800 (Singapore Standard Time)

<p>
In-order traversal of BST yields sorted sequence. So, we just need to subtract the previous element from the current one, and keep track of the minimum. We need O(1) memory as we only store the previous element, but we still need O(h) for the stack.
```
void inorderTraverse(TreeNode* root, int& val, int& min_dif) {
    if (root->left != NULL) inorderTraverse(root->left, val, min_dif);
    if (val >= 0) min_dif = min(min_dif, root->val - val);
    val = root->val;
    if (root->right != NULL) inorderTraverse(root->right, val, min_dif);
}
int getMinimumDifference(TreeNode* root) {
    auto min_dif = INT_MAX, val = -1;
    inorderTraverse(root, val, min_dif);
    return min_dif;
}
```
Another solution with the member variables (6 lines):
```
class Solution {
    int min_dif = INT_MAX, val = -1;
public:
int getMinimumDifference(TreeNode* root) {
    if (root->left != NULL) getMinimumDifference(root->left);
    if (val >= 0) min_dif = min(min_dif, root->val - val);
    val = root->val;
    if (root->right != NULL) getMinimumDifference(root->right);
    return min_dif;
}
```
</p>


