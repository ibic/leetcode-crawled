---
title: "Encode String with Shortest Length"
weight: 447
#id: "encode-string-with-shortest-length"
---
## Description
<div class="description">
<p>Given a <b>non-empty</b> string, encode the string such that its encoded length is the shortest.</p>

<p>The encoding rule is: <code>k[encoded_string]</code>, where the <i>encoded_string</i> inside the square brackets is being repeated exactly <i>k</i> times.</p>

<p><b>Note:</b></p>

<ol>
	<li><i>k</i> will be a positive integer and encoded string will not be empty or have extra space.</li>
	<li>You may assume that the input string contains only lowercase English letters. The string&#39;s length is at most 160.</li>
	<li>If an encoding process does not make the string shorter, then do not encode it. If there are several solutions, return any of them is fine.</li>
</ol>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
Input: &quot;aaa&quot;
Output: &quot;aaa&quot;
Explanation: There is no way to encode it such that it is shorter than the input string, so we do not encode it.
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
Input: &quot;aaaaa&quot;
Output: &quot;5[a]&quot;
Explanation: &quot;5[a]&quot; is shorter than &quot;aaaaa&quot; by 1 character.
</pre>

<p>&nbsp;</p>

<p><b>Example 3:</b></p>

<pre>
Input: &quot;aaaaaaaaaa&quot;
Output: &quot;10[a]&quot;
Explanation: &quot;a9[a]&quot; or &quot;9[a]a&quot; are also valid solutions, both of them have the same length = 5, which is the same as &quot;10[a]&quot;.
</pre>

<p>&nbsp;</p>

<p><b>Example 4:</b></p>

<pre>
Input: &quot;aabcaabcd&quot;
Output: &quot;2[aabc]d&quot;
Explanation: &quot;aabc&quot; occurs twice, so one answer can be &quot;2[aabc]d&quot;.
</pre>

<p>&nbsp;</p>

<p><b>Example 5:</b></p>

<pre>
Input: &quot;abbbabbbcabbbabbbc&quot;
Output: &quot;2[2[abbb]c]&quot;
Explanation: &quot;abbbabbbc&quot; occurs twice, but &quot;abbbabbbc&quot; can also be encoded to &quot;2[abbb]c&quot;, so one answer can be &quot;2[2[abbb]c]&quot;.
</pre>

<p>&nbsp;</p>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 3 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted Solution in Java
- Author: dsaddasdasfsa
- Creation Date: Fri Dec 16 2016 15:16:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 06:10:28 GMT+0800 (Singapore Standard Time)

<p>
This is the first question I have answered in Leetcode. I hope you guys will like my solution. The approach here is simple. We will form 2-D array of Strings. 
dp[i][j] = string from index i to index j in encoded form.

We can write the following formula as:-
dp[i][j] = min(dp[i][j], dp[i][k] + dp[k+1][j]) or if we can find some pattern in string from i to j which will result in more less length.

Time Complexity = O(n^3)


public class Solution {

     public String encode(String s) {
        String[][] dp = new String[s.length()][s.length()];
        
        for(int l=0;l<s.length();l++) {
            for(int i=0;i<s.length()-l;i++) {
                int j = i+l;
                String substr = s.substring(i, j+1);
                // Checking if string length < 5. In that case, we know that encoding will not help.
                if(j - i < 4) {
                    dp[i][j] = substr;
                } else {
                    dp[i][j] = substr;
                    // Loop for trying all results that we get after dividing the strings into 2 and combine the   results of 2 substrings
                    for(int k = i; k<j;k++) {
                        if((dp[i][k] + dp[k+1][j]).length() < dp[i][j].length()){
                            dp[i][j] = dp[i][k] + dp[k+1][j];
                        }
                    }
                    
                    // Loop for checking if string can itself found some pattern in it which could be repeated.
                    for(int k=0;k<substr.length();k++) {
                        String repeatStr = substr.substring(0, k+1);
                        if(repeatStr != null 
                           && substr.length()%repeatStr.length() == 0 
                           && substr.replaceAll(repeatStr, "").length() == 0) {
                              String ss = substr.length()/repeatStr.length() + "[" + dp[i][i+k] + "]";
                              if(ss.length() < dp[i][j].length()) {
                                dp[i][j] = ss;
                              }
                         }
                    }
                }
            }
        }
        
        return dp[0][s.length()-1];
    }
    }
</p>


### Short Python
- Author: StefanPochmann
- Creation Date: Sun Dec 11 2016 19:02:38 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 06:42:03 GMT+0800 (Singapore Standard Time)

<p>
Either don't encode `s` at all, or encode it as **one** part `k[...]` or encode it as **multiple** parts (in which case we can somewhere split it into two subproblems). Whatever is shortest. Uses @rsrs3's nice [trick](https://discuss.leetcode.com/topic/68206/easy-python-solution-with-explaination) of searching `s` in `s + s`.

    def encode(self, s, memo={}):
        if s not in memo:
            n = len(s)
            i = (s + s).find(s, 1)
            one = '%d[%s]' % (n / i, self.encode(s[:i])) if i < n else s
            multi = [self.encode(s[:i]) + self.encode(s[i:]) for i in xrange(1, n)]
            memo[s] = min([s, one] + multi, key=len)
        return memo[s]
</p>


### Easy to understand C++ O(n^3) solution
- Author: yanzhan2
- Creation Date: Mon Dec 12 2016 14:53:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 19:25:30 GMT+0800 (Singapore Standard Time)

<p>
3 for loop, so complexity O(n^3), bottom up DP with step goes from 1 to n, and for each step calculate all start and end locations. Use the collapse idea from [another solution](https://discuss.leetcode.com/topic/71442/short-python).
```
class Solution {
private:
	vector<vector<string>> dp;
public:
	string collapse(string& s, int i, int j) {
	    string temp = s.substr(i, j - i + 1);
		auto pos = (temp+temp).find(temp, 1);
		if (pos >= temp.size()) {
		    return temp;
		}
		return to_string(temp.size()/pos) + '['+ dp[i][i+pos-1]+']';
	}

	string encode(string s) {
		int n = s.size();
		dp = vector<vector<string>>(n, vector<string>(n, ""));
		for (int step = 1; step <= n; step++) {
			for (int i = 0; i + step - 1 < n; i++) {
				int j = i + step - 1;
				dp[i][j] = s.substr(i, step);
				for (int k = i; k < j; k++) {
					auto left = dp[i][k];
					auto right = dp[k + 1][j];
					if (left.size() + right.size() < dp[i][j].size()) {
						dp[i][j] = left + right;
					}
				}
				string replace = collapse(s, i, j);
				if (replace.size() < dp[i][j].size()) {
					dp[i][j] = replace;
				}
			}
		}
		return dp[0][n - 1];
	}
};
```
</p>


