---
title: "Sliding Puzzle"
weight: 708
#id: "sliding-puzzle"
---
## Description
<div class="description">
<p>On a 2x3 <code>board</code>, there are 5 tiles represented by the integers 1 through 5, and an empty square represented by 0.</p>

<p>A move consists of choosing <code>0</code>&nbsp;and a 4-directionally adjacent number and swapping it.</p>

<p>The state of the board is <em>solved</em> if and only if the <code>board</code> is <code>[[1,2,3],[4,5,0]].</code></p>

<p>Given a puzzle board, return the least number of moves required so that the state of the board is solved. If it is impossible for the state of the board to be solved, return -1.</p>

<p><strong>Examples:</strong></p>

<pre>
<strong>Input:</strong> board = [[1,2,3],[4,0,5]]
<strong>Output:</strong> 1
<strong>Explanation:</strong> Swap the 0 and the 5 in one move.
</pre>

<pre>
<strong>Input:</strong> board = [[1,2,3],[5,4,0]]
<strong>Output:</strong> -1
<strong>Explanation:</strong> No number of moves will make the board solved.
</pre>

<pre>
<strong>Input:</strong> board = [[4,1,2],[5,0,3]]
<strong>Output:</strong> 5
<strong>Explanation:</strong> 5 is the smallest number of moves that solves the board.
An example path:
After move 0: [[4,1,2],[5,0,3]]
After move 1: [[4,1,2],[0,5,3]]
After move 2: [[0,1,2],[4,5,3]]
After move 3: [[1,0,2],[4,5,3]]
After move 4: [[1,2,0],[4,5,3]]
After move 5: [[1,2,3],[4,5,0]]
</pre>

<pre>
<strong>Input:</strong> board = [[3,2,4],[1,5,0]]
<strong>Output:</strong> 14
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li><code>board</code> will be a 2 x 3 array as described above.</li>
	<li><code>board[i][j]</code> will be a permutation of <code>[0, 1, 2, 3, 4, 5]</code>.</li>
</ul>

</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies
- Microsoft - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Uber - 9 (taggedByAdmin: false)
- Airbnb - 7 (taggedByAdmin: true)
- Google - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Breadth-First Search [Accepted]

**Intuition**

We can think of this problem as a shortest path problem on a graph.  Each node is a different board state, and we connect two boards by an edge if they can be transformed into one another in one move.  We can solve shortest path problems with *breadth first search*.

**Algorithm**

For our breadth first search, we will need to be able to represent the nodes as something hashable, and we'll need to enumerate the neighbors of a board.  Afterwards, we can use a typical template for breadth-first search as shown below:

```python
queue = collections.deque([(start, 0)])
seen = {start}
while queue:
    node, depth = queue.popleft()
    if node == target: return depth
    for nei in neighbors(node):
        if nei not in seen:
            seen.add(nei)
            queue.append((nei, depth+1))
```
To represent the nodes as something hashable, in Python, we convert the board to 1 dimension and use a tuple; in Java we can either encode the board as an integer, or more generally use `Arrays.deepToString`.

To enumerate the neighbors of a board, we'll remember where the zero is.  Then, there are only 4 possible neighbors.  If the board is flattened to 1 dimension, these neighbors occur at distances `(1, -1, C, -C)` from the zero, where `C` is the number of columns in the board.

<iframe src="https://leetcode.com/playground/HtkByHNd/shared" frameBorder="0" width="100%" height="500" name="HtkByHNd"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(R * C * (R * C)!)$$, where $$R, C$$ are the number of rows and columns in `board`.  There are $$O((R * C)!)$$ possible board states.

* Space Complexity: $$O(R * C * (R * C)!)$$.

---
#### Approach #2: A* Search [Accepted]

**Intuition**

As in *Approach #1*, this is a problem about searching on a graph.

We can use the "A* Star Search Algorithm" to search promising nodes in our graph first, with guarantees that it will find the best answer.

For every node, we have some estimated cost `node.priority = node.depth + node.heuristic`, where `node.depth` is the actual distance travelled, and `node.heuristic` is our *heuristic* (guess) of the remaining distance to travel.  If the heuristic is *admissible* (it never overestimates the distance to the goal), then the following algorithm is guaranteed to terminate at the best answer.

For solvers familiar with *Dijkstra's Algorithm*, *A\* Search* is a special case of *Dijkstra's* with `node.heuristic = 0` always.  On certain types of graphs and with good heuristics, this approach is substantially faster then a breadth-first search.

**Algorithm**

Let's keep a priority queue that sorts by `node.depth + node.heuristic`.  As before, each node represents a puzzle board.

The heuristic we use is the sum of the taxicab distance of each (non-zero) number to it's final destination.  This heuristic is admissible as we need at least this many moves.

To speed up our algorithm, we use `targetWrong`, which has a near zero heuristic distance to the target (meaning our search will aim for it quickly).  If it finds it, we don't have to search all the boards.

We could prove that the set of boards can be split in half, with one half transformable to `target`, and the other half transformable to `targetWrong`.  One way to convince yourself of this is to see that every piece except the last 2 can be placed in the correct position, but a formal proof analyzing the parity of inversions of the underlying permutation is outside the scope of this article.  For more information see [link](http://kevingong.com/Math/SixteenPuzzle.html).

<iframe src="https://leetcode.com/playground/RzEmtPdV/shared" frameBorder="0" width="100%" height="500" name="RzEmtPdV"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(R * C * (R * C)!)$$, where $$R, C$$ are the number of rows and columns in `board`.  Tighter bounds are possible, but difficult to prove.  (*In testing with random permutations of a 3x3 board, about 50 times less nodes were searched compared to breadth-first-search.*)

* Space Complexity: $$O(R * C * (R * C)!)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 8ms BFS with algorithm explained
- Author: ShinozakiAi
- Creation Date: Sun Jul 08 2018 06:24:34 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 14:10:30 GMT+0800 (Singapore Standard Time)

<p>
Algorithm:

Consider each state in the board as a graph node, we just need to find out the min distance between start node and final target node "123450". Since it\'s a single point to single point questions, Dijkstra is not needed here. We can simply use BFS, and also count the level we passed. Every time we swap 0 position in the String to find the next state. Use a hashTable to store the visited states.

```
public int slidingPuzzle(int[][] board) {
        String target = "123450";
        String start = "";
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                start += board[i][j];
            }
        }
        HashSet<String> visited = new HashSet<>();
        // all the positions 0 can be swapped to
        int[][] dirs = new int[][] { { 1, 3 }, { 0, 2, 4 },
                { 1, 5 }, { 0, 4 }, { 1, 3, 5 }, { 2, 4 } };
        Queue<String> queue = new LinkedList<>();
        queue.offer(start);
        visited.add(start);
        int res = 0;
        while (!queue.isEmpty()) {
            // level count, has to use size control here, otherwise not needed
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                String cur = queue.poll();
                if (cur.equals(target)) {
                    return res;
                }
                int zero = cur.indexOf(\'0\');
                // swap if possible
                for (int dir : dirs[zero]) {
                    String next = swap(cur, zero, dir);
                    if (visited.contains(next)) {
                        continue;
                    }
                    visited.add(next);
                    queue.offer(next);

                }
            }
            res++;
        }
        return -1;
    }

    private String swap(String str, int i, int j) {
        StringBuilder sb = new StringBuilder(str);
        sb.setCharAt(i, str.charAt(j));
        sb.setCharAt(j, str.charAt(i));
        return sb.toString();
    }
```
</p>


### [Java/Python 3] BFS clean codes w/ comment, Time & space: O(m * n * (m * n)!).
- Author: rock
- Creation Date: Sun Jan 28 2018 22:13:01 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 22 2020 23:15:10 GMT+0800 (Singapore Standard Time)

<p>
# Update 2: 
Please refer to more **readable** and generic versions of Java and Python 3, which adapt to various dimensions of input `board`. Feel free to let me know if you have any suggestions to make them better.
**Java**
```
    private static final int[] d = {0, 1, 0, -1, 0}; //  relative displacements of neighbors in board.
    public int slidingPuzzle(int[][] board) {
        int height = board.length, width = height == 0 ? 0 : board[0].length;
        // convert board to string - initial state.
        String s = Arrays.deepToString(board).replaceAll("\\[|\\]|,|\\s", ""); // e.g., [[1,2,3],[4,0,5]] -> "123405"
        Queue<String> q = new LinkedList<>(Arrays.asList(s)); // add initial state to queue.
        Set<String> seen = new HashSet<>(q); // used to avoid duplicates
        for (int steps = 0; !q.isEmpty(); ++steps) { // Not traverse all states yet?
            // loop used to control search breadth.
            for (int sz = q.size(); sz > 0; --sz) {
                String str = q.poll();
                if (str.equals("123450")) { return steps; } // found target.
                int i = str.indexOf("0"), x = i / width, y = i % width; // board[x][y] is \'0\'.
                for (int k = 0; k < 4; ++k) { // traverse all options.
                    int r = x + d[k], c = y + d[k + 1]; // board[r][c] is the neighbor of \'0\'.
                    if (r >= 0 && r < height && c >= 0 && c < width) {
                        char[] ch = str.toCharArray();
                        ch[i] = ch[r * width + c]; // r * width + c is the string index of board[r][c].
                        ch[r * width + c] = \'0\'; // assign \'0\' to the neighbor of board[x][y].
                        s = String.valueOf(ch);
                        if (seen.add(s)) q.offer(s); // if not duplicate, add s to the queue.
                    }
                }
            }
        }
        return -1;
    }
```
**Python 3**
```
    def slidingPuzzle(self, board: List[List[int]]) -> int:
        s = \'\'.join(str(d) for row in board for d in row)
        dq, seen = collections.deque(), {s}
        dq.append((s, s.index(\'0\')))
        steps, height, width = 0, len(board), len(board[0]) 
        while dq:
            for _ in range(len(dq)):
                t, i= dq.popleft()
                if t == \'123450\':
                    return steps
                x, y = i // width, i % width
                for r, c in (x, y + 1), (x, y - 1), (x + 1, y), (x - 1, y):
                    if height > r >= 0 <= c < width:
                        ch = [d for d in t]
                        ch[i], ch[r * width + c] = ch[r * width + c], \'0\' # swap \'0\' and its neighbor.
                        s = \'\'.join(ch)
                        if s not in seen:
                            seen.add(s)
                            dq.append((s, r * width + c))
            steps += 1              
        return -1
```
# Update 1: 
1. **distance** in the follows changed to **displacement** to avoid confusion:
2. see after the code.

Convert array to string, e.g., [[1,2,3],[4,0,5]] -> "123405", hence the corresponding potential swap displacements are: -1, 1, -3, 3. Also note, charAt(2) and charAt(3) are not adjacent in original 2 dimensional int array and therefore are not valid swaps.

```    
    private static final int[] d = { 1, -1, 3, -3 }; // potential swap displacements.
    public int slidingPuzzle(int[][] board) {
        // convert board to string - initial state.
        String s = Arrays.deepToString(board).replaceAll("\\[|\\]|,|\\s", "");
        Queue<String> q = new LinkedList<>(Arrays.asList(s)); // add initial state to queue.
        Set<String> seen = new HashSet<>(q); // used to avoid duplicates
        int ans = 0; // record the # of rounds of Breadth Search
        while (!q.isEmpty()) { // Not traverse all states yet?
            // loop used to control search breadth.
            for (int sz = q.size(); sz > 0; --sz) { 
                String str = q.poll();
                if (str.equals("123450")) { return ans; } // found target.
                int i = str.indexOf(\'0\'); // locate \'0\'
                for (int k = 0; k < 4; ++k) { // traverse all options.
                    int j = i + d[k]; // potential swap index.
                    // conditional used to avoid invalid swaps.
                    if (j < 0 || j > 5 || i == 2 && j == 3 || i == 3 && j == 2) { continue; } 
                    char[] ch = str.toCharArray();
                    // swap ch[i] and ch[j]:  ch[i] = ch[j], ch[j] = \'0\'. Updated per @caowang888\'s suggestion. 
                    ch[i] = ch[j];
                    ch[j] = \'0\';
                    s = String.valueOf(ch); // a new candidate state.
                    if (seen.add(s)) { q.offer(s); } //Avoid duplicate.
                }
            }
            ++ans; // finished a round of Breadth Search, plus 1.
        }
        return -1;
    }
```
**Analysis:**
There are at most 6! permutation of the 6 numbers: 0~5. For each permustion, cost spaceO(6); String.indexOf() and String.equals() cost time O(6). Therefore, space and time both cost 6 * 6! = 4320. 
Replace the above `6` with `m * n`, we have:

**Time & space: O(m * n * (m * n)!)**.

Feel free to let me know if you can find a tighter bound.

**Update**: 
2
**Question: Can you explain how did u determine the potential swap distance?**

**Answer:**

When you map a 2 dimensional ( row * column ) matrix to 1 dimensional array,  the swap to left and right are still -1 and 1 respectively in terms of index change in 1-D array. In contrast, the swap to up and down are -column and column as of index change.  

**That is, for any given index (i, j) in the matrix, it will be mapped to (i * column + j) in the array. Since in the matrix (i, j) could swap with (i, j - 1), (i, j + 1), (i - 1, j), and (i + 1, j), in the array (i * column + j) would swap with (i * column + j - 1), (i * column + j + 1), ((i - 1) * column + j) and ((i + 1) * column + j) accordingly.** 

**We can easily conclude the swap displacement are -1, 1, -column, and column correspondingly.**

e.g.: Let\'s consider (1, 2) in the following 3 * 5 matrix
```
column # -->               0     1     2     3     4
                      0  (0,0) (0,1) (0,2) (0,3) (0,4)
                      1  (1,0) (1,1) (1,2) (1,3) (1,4)
                      2  (2,0) (2,1) (2,2) (2,3) (2,4)
```
we can swap (1, 2) with (1, 1), (1, 3), (0, 2), and (2, 2) 

When mapped to 1 dimensional array, it can be obtained that (1 * 5 + 2) swap with (1 * 5 + 2 - 1), (1 * 5 + 2 + 1), ((1 - 1) * 5 + 2) and ((1 + 1) * 5 + 2):
```
  0    1    2    3    4    5    6    7    8    9   10   11   12   13   
(0,0)(0,1)(0,2)(0,3)(0,4)(1,0)(1,1)(1,2)(1,3)(1,4)(2,0)(2,1)(2,2)(2,3)

 14
(2,4)
```
Obviously, we can swap 7 with, 6 (= 7 - 1), 8 (= 7 + 1), 2 (= 7 - 5), 12 (= 7 + 5), where 5 is matrix column (width).                                 

**The displacements in the above case are -1, 1, -5, and 5.**

Hope the above answers your question.


</p>


### Python very easy to understand  // BFS w/ backtracking concise solution // beats 98 %
- Author: cenkay
- Creation Date: Wed Jun 27 2018 20:21:11 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 18 2018 12:29:40 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def slidingPuzzle(self, board):
        moves, used, cnt = {0: {1, 3}, 1:{0, 2, 4}, 2:{1, 5}, 3:{0, 4}, 4:{1, 3, 5}, 5:{2, 4}}, set(), 0
        s = "".join(str(c) for row in board for c in row)
        q = [(s, s.index("0"))]
        while q:
            new = []
            for s, i in q:
                used.add(s)
                if s == "123450":
                    return cnt
                arr = [c for c in s]
                for move in moves[i]:
                    new_arr = arr[:]
                    new_arr[i], new_arr[move] = new_arr[move], new_arr[i]
                    new_s = "".join(new_arr)
                    if new_s not in used:
                        new.append((new_s, move))
            cnt += 1
            q = new
        return -1
```
</p>


