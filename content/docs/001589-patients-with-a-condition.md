---
title: "Patients With a Condition"
weight: 1589
#id: "patients-with-a-condition"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Patients</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| patient_id   | int     |
| patient_name | varchar |
| conditions   | varchar |
+--------------+---------+
patient_id is the primary key for this table.
&#39;conditions&#39; contains 0 or more code separated by spaces. 
This table contains information of the patients in the hospital.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to report the patient_id, patient_name all conditions of patients who have Type I Diabetes. Type I Diabetes always starts with <code>DIAB1</code> prefix</p>

<p>Return the result table in any order.</p>

<p>The query result format is in the following example.</p>

<p>&nbsp;</p>

<pre>
<code>Patients</code>
+------------+--------------+--------------+
| patient_id | patient_name | conditions   |
+------------+--------------+--------------+
| 1          | Daniel      &nbsp;| YFEV COUGH   |
| 2    &nbsp;     | Alice        |            &nbsp; |
| 3    &nbsp;     | Bob         &nbsp;| DIAB100 MYOP&nbsp;|
| 4 &nbsp;        | George      &nbsp;| ACNE DIAB100&nbsp;|
| 5 &nbsp;        | Alain       &nbsp;| DIAB201     &nbsp;|
+------------+--------------+--------------+

Result table:
+------------+--------------+--------------+
| patient_id | patient_name | conditions   |
+------------+--------------+--------------+
| 3    &nbsp;     | Bob         &nbsp;| DIAB100 MYOP&nbsp;|
| 4 &nbsp;        | George   &nbsp;   | ACNE DIAB100&nbsp;| 
+------------+--------------+--------------+
Bob and George both have a condition that starts with DIAB1.
</pre>
</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple MySQL Solution, faster than 100%
- Author: anshulkapoor018
- Creation Date: Sun Jul 26 2020 17:46:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 17:46:01 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT * FROM PATIENTS WHERE
CONDITIONS LIKE \'% DIAB1%\' OR
CONDITIONS LIKE \'DIAB1%\';
```
</p>


### MySQL: 2 solutions
- Author: bunxi
- Creation Date: Sun Jul 26 2020 08:54:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 21:55:32 GMT+0800 (Singapore Standard Time)

<p>
Using simple string matching:
```
SELECT * FROM Patients
WHERE conditions LIKE \'%DIAB1%\';
```

Using regular expression match:
```
SELECT * FROM Patients
WHERE conditions REGEXP \'^DIAB1| DIAB1\';
```

The REGEXP version is more robust.
</p>


### Use LIKE to solve
- Author: vikisingh33
- Creation Date: Sun Sep 06 2020 12:15:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 12:15:47 GMT+0800 (Singapore Standard Time)

<p>
SELECT patient_id, patient_name, conditions
FROM Patients
WHERE conditions LIKE \'%DIAB1%\';
</p>


