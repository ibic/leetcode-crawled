---
title: "Predict the Winner"
weight: 461
#id: "predict-the-winner"
---
## Description
<div class="description">
<p>Given an array of scores that are non-negative integers. Player 1 picks one of the numbers from either end of the array followed by the player 2 and then player 1 and so on. Each time a player picks a number, that number will not be available for the next player. This continues until all the scores have been chosen. The player with the maximum score wins.</p>

<p>Given an array of scores, predict whether player 1 is the winner. You can assume each player plays to maximize his score.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> [1, 5, 2]
<b>Output:</b> False
<b>Explanation:</b> Initially, player 1 can choose between 1 and 2. 
If he chooses 2 (or 1), then player 2 can choose from 1 (or 2) and 5. If player 2 chooses 5, then player 1 will be left with 1 (or 2). 
So, final score of player 1 is 1 + 2 = 3, and player 2 is 5. 
Hence, player 1 will never be the winner and you need to return False.
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> [1, 5, 233, 7]
<b>Output:</b> True
<b>Explanation:</b> Player 1 first chooses 1. Then player 2 have to choose between 5 and 7. No matter which number player 2 choose, player 1 can choose 233.
Finally, player 1 has more score (234) than player 2 (12), so you need to return True representing player1 can win.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>1 &lt;= length of the array &lt;= 20.</li>
	<li>Any scores in the given array are non-negative integers and will not exceed 10,000,000.</li>
	<li>If the scores of both players are equal, then player 1 is still the winner.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Minimax (minimax)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Using Recursion [Accepted]

The idea behind the recursive approach is simple. The two players Player 1 and Player 2 will be taking turns alternately. For the Player 1 to be the winner, we need $$score_{Player\_1} &geq; score_{Player\_2}$$. Or in other terms, $$score_{Player\_1} - score_{Player\_2} &geq; 0$$. 

Thus, for the turn of Player 1, we can add its score obtained to the total score and for Player 2's turn, we can substract its score from the total score. At the end, we can check if the total score is greater than or equal to zero(equal score of both players), to predict that Player 1 will be the winner.

Thus, by making use of a recursive function `winner(nums,s,e,turn)` which predicts the winner for the $$nums$$ array as the score array with the elements in the range of indices $$[s,e]$$ currently being considered, given a particular player's turn, indicated by $$turn=1$$ being Player 1's turn and $$turn=-1$$ being the Player 2's turn, we can predict the winner of the given problem by making the function call `winner(nums,0,n-1,1)`. Here, $$n$$ refers to the length of $$nums$$ array.

In every turn, we can either pick up the first($$nums[s]$$) or the last($$nums[e]$$) element of the current subarray. Since both the players are assumed to be playing smartly and making the best move at every step, both will tend to maximize their scores. Thus, we can make use of the same function `winner` to determine the maximum score possible for any of the players. 

Now, at every step of the recursive process, we determine the maximum score possible for the current player. It will be the maximum one possible out of the scores obtained by picking the first or the last element of the current subarray. 

To obtain the score possible from the remaining subarray, we can again make use of the same `winner` function and add the score corresponding to the point picked in the current function call. But, we need to take care of whether to add or subtract this score to the total score available. If it is Player 1's turn, we add the current number's score to the total score, otherwise, we need to subtract the same. 

Thus, at every step, we need update the search space appropriately based on the element chosen and also invert the $$turn$$'s value to indicate the turn change among the players and either add or subtract the current player's score from the total score available to determine the end result.

Further, note that the value returned at every step is given by $$turn *\text{max}(turn * a, turn * b)$$. This is equivalent to the statement $$max(a,b)$$ for Player 1's turn and $$min(a,b)$$ for Player 2's turn. 

This is done because, looking from Player 1's perspective, for any move made by Player 1, it tends to leave the remaining subarray in a situation which minimizes the best score possible for Player 2, even if it plays in the best possible manner. But, when the turn passes to Player 1 again, for Player 1 to win, the remaining subarray should be left in a state such that the score obtained from this subarrray is maximum(for Player 1). 

This is a general criteria for any arbitrary two player game and is commonly known as the 
[Min-Max algorithm](https://en.wikipedia.org/wiki/Minimax).

The following image shows how the scores are passed to determine the end result for a simple example.

![Recursive_Tree](../Figures/486/486_Predict_the_winner_new.PNG)
{:align="center"}

<iframe src="https://leetcode.com/playground/3SDSCR7V/shared" frameBorder="0" name="3SDSCR7V" width="100%" height="275"></iframe>

**Complexity Analysis**

* Time complexity : $$O(2^n)$$. Size of recursion tree will be $$2^n$$. Here, $$n$$ refers to the length of $$nums$$ array.

* Space complexity : $$O(n)$$. The depth of the recursion tree can go upto $$n$$.

---
#### Approach #2 Similar Approach [Accepted]

**Algorithm**

We can omit the use of $$turn$$ to keep a track of the player for the current turn. To do so, we can make use of a simple observation. If the current turn belongs to, say Player 1, we pick up an element, say $$x$$, from either end, and give the turn to Player 2. Thus, if we obtain the score for the remaining elements(leaving $$x$$), this score, now belongs to Player 2. Thus, since Player 2 is competing against Player 1, this score should be subtracted from Player 1's current(local) score($$x$$) to obtain the effective score of Player 1 at the current instant.

Similar argument holds true for Player 2's turn as well i.e. we can subtract Player 1's score for the remaining subarray from Player 2's current score to obtain its effective score. By making use of this observation, we can omit the use of $$turn$$ from `winner` to find the required result by making the slight change discussed above in the `winner`'s implementation.

While returning the result from `winner` for the current function call, we return the larger of the effective scores possible by choosing either the first or the last element from the currently available subarray. Rest of the process remains the same as the last approach.

Now, in order to remove the duplicate function calls, we can make use of a 2-D memoization array, $$memo$$, such that we can store the result obtained for the function call `winner` for a subarray with starting and ending indices being $$s$$ and $$e$$ ] at $$memo[s][e]$$. This helps to prune the search space to a great extent.

 This approach is inspired by [@chidong](http://leetcode.com/chidong)

<iframe src="https://leetcode.com/playground/RGPbqsDC/shared" frameBorder="0" name="RGPbqsDC" width="100%" height="326"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n^2)$$. The entire $$memo$$ array of size $$n$$x$$n$$ is filled only once. Here, $$n$$ refers to the size of $$nums$$ array.

* Space complexity : $$O(n^2)$$. $$memo$$ array of size $$n$$x$$n$$ is used for memoization.

---
#### Approach #3 Dynamic Programming [Accepted]:

**Algorithm**

We can observe that the effective score for the current player for any given subarray $$nums[x:y]$$ only depends on the elements within the range $$[x,y]$$ in the array $$nums$$. It mainly depends on whether the element $$nums[x]$$ or $$nums[y]$$ is chosen in the current turn and also on the maximum score possible for the other player from the remaining subarray left after choosing the current element. Thus, it is certain that the current effective score isn't dependent on the elements outside the range $$[x,y]$$. 

Based on the above observation, we can say that if know the maximum effective score possible for the subarray $$nums[x+1,y]$$ and $$nums[x,y-1]$$, we can easily determine the maximum effective score possible for the subarray $$nums[x,y]$$ as $$\text{max}(nums[x]-score_{[x+1,y]}, nums[y]-score_{[x,y-1]})$$. These equations are deduced based on the last approach. 

From this,  we conclude that we can make use of Dynamic Programming to determine the required maximum effective score for the array $$nums$$. We can make use of a 2-D $$dp$$ array, such that $$dp[i][j]$$ is used to store the maximum effective score possible for the subarray $$nums[i,j]$$. The $$dp$$ updation equation becomes: 

$$dp[i,j] = nums[i] - dp[i + 1][j], nums[j] - dp[i][j - 1]$$.

We can fill in the $$dp$$ array starting from the last row. At the end, the value for $$dp[0][n-1]$$ gives the required result. Here, $$n$$ refers to the length of $$nums$$ array.

Look at the animation below to clearly understand the $$dp$$ filling process.

!?!../Documents/486_Predict_the_winner.json:1000,563!?!



<iframe src="https://leetcode.com/playground/EFGjsVXp/shared" frameBorder="0" name="EFGjsVXp" width="100%" height="275"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. $$((n+1)$$x$$n)/2$$ entries in $$dp$$ array of size $$(n+1)$$x$$n$$ is filled once. Here, $$n$$ refers to the length of $$nums$$ array.

* Space complexity : $$O(n^2)$$. $$dp$$ array of size $$(n+1)$$x$$n$$ is used.

---
#### Approach #4 1-D Dynamic Programming [Accepted]:

**Algorithm**

From the last approach, we see that the $$dp$$ updation equation is: 

$$dp[i,j] = nums[i] - dp[i + 1][j], nums[j] - dp[i][j - 1]$$. 

Thus, for filling in any entry in $$dp$$ array, only the entries in the next row(same column) and the previous column(same row) are needed.

Instead of making use of a new row in $$dp$$ array for the current $$dp$$ row's updations, we can overwrite the values in the previous row itself and consider the values as belonging to the new row's entries, since the older values won't be needed ever in the future again. Thus, instead of making use of a 2-D $$dp$$ array, we can make use of a 1-D $$dp$$ array and make the updations appropriately.

<iframe src="https://leetcode.com/playground/k9vrYN2X/shared" frameBorder="0" name="k9vrYN2X" width="100%" height="292"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. The elements of $$dp$$ array are updated $$1+2+3+...+n$$ times. Here, $$n$$ refers to the length of $$nums$$ array.

* Space complexity : $$O(n)$$. 1-D $$dp$$ array of size $$n$$ is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### JAVA 9 lines DP solution, easy to understand with improvement to O(N) space complexity.
- Author: odingg
- Creation Date: Thu Jan 26 2017 13:38:32 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 11:28:09 GMT+0800 (Singapore Standard Time)

<p>
The dp[i][j] saves how much ***more*** scores that the first-in-action player will get from i to j than the second player. First-in-action means whomever moves first. You can still make the code even shorter but I think it looks clean in this way.

    public boolean PredictTheWinner(int[] nums) {
        int n = nums.length;
        int[][] dp = new int[n][n];
        for (int i = 0; i < n; i++) { dp[i][i] = nums[i]; }
        for (int len = 1; len < n; len++) {
            for (int i = 0; i < n - len; i++) {
                int j = i + len;
                dp[i][j] = Math.max(nums[i] - dp[i + 1][j], nums[j] - dp[i][j - 1]);
            }
        }
        return dp[0][n - 1] >= 0;
    }

Here is the code for O(N) space complexity:
```

public boolean PredictTheWinner(int[] nums) {
    if (nums == null) { return true; }
    int n = nums.length;
    if ((n & 1) == 0) { return true; } // Improved with hot13399's comment.
    int[] dp = new int[n];
    for (int i = n - 1; i >= 0; i--) {
        for (int j = i; j < n; j++) {
            if (i == j) {
                dp[i] = nums[i];
            } else {
                dp[j] = Math.max(nums[i] - dp[j], nums[j] - dp[j - 1]);
            }
        }
    }
    return dp[n - 1] >= 0;
}

```

Edit : Since I have some time now, I will explain how I come up with this solution step by step:

1, The first step is to break the question into the sub-problems that we can program. From the question, the winning goal is that "The player with the maximum score wins". So one way to approach it is that we may want to find a way to maximize player 1's sum and check if it is greater than player 2's sum (or more than half of the sum of all numbers). Another way, after noting that the sum of all numbers is fixed, I realized that it doesn't matter how much player 1's total sum is as long as the sum is no less than player 2's sum. No matter how, I think we can easily recognize that it is a recursive problem where we may use the status on one step to calculate the answer for the next step. It is a common way to solve game problems. So we may start with using a brutal force recursive method to solve this one.

2, However, we always want to do better than brutal force. We may easily notice that there will be lots of redundant calculation. For example, "player 1 picks left, then player 2 picks left, then player 1 picks right, then player 2 picks right" will end up the same as "player 1 picks right, then player 2 picks right, then player 1 picks left, then player 2 picks left". So, we may want to use dynamic programming to save intermediate states.

3, I think it will be easy to think about using a two dimensional array dp[i][j] to save all the intermediate states. From step 1, we may see at least two ways of doing it. It just turned out that if we choose to save how much more scores that the first-in-action player will earn from position i to j in the array (as I did), the code will be better in a couple of ways.

4, After we decide that dp[i][j] saves how much more scores that the first-in-action player will get from i to j than the second player, the next step is how we update the dp table from one state to the next. Going back to the question, each player can pick one number either from the left or the right end of the array. Suppose they are picking up numbers from position i to j in the array and it is player A's turn to pick the number now. If player A picks position i, player A will earn nums[i] score instantly. Then player B will choose from i + 1 to j. Please note that dp[i + 1][j] already saves how much more score that the first-in-action player will get from i + 1 to j than the second player. So it means that player B will eventually earn dp[i + 1][j] more score from i + 1 to j than player A. So if player A picks position i, eventually player A will get nums[i] - dp[i + 1][j] more score than player B after they pick up all numbers. Similarly, if player A picks position j, player A will earn nums[j] - dp[i][j - 1] more score than player B after they pick up all numbers. Since A is smart, A will always choose the max in those two options, so:
dp[i][j] = Math.max(nums[i] - dp[i + 1][j], nums[j] - dp[i][j - 1]);

5, Now we have the recursive formula, the next step is to decide where it all starts. This step is easy because we can easily recognize that we can start from dp[i][i], where dp[i][i] = nums[i]. Then the process becomes a very commonly seen process to update the dp table. I promise that this is a very useful process. Everyone who is preparing for interviews should get comfortable with this process:
Using a 5 x 5 dp table as an example, where i is the row number and j is the column number. Each dp[i][j] corresponds to a block at row i, column j on the table. We may start from filling dp[i][i], which are all the diagonal blocks. I marked them as 1. Then we can see that each dp[i][j] depends only on dp[i + 1][j] and dp[i][j - 1]. On the table, it means each block (i, j) only depends on the block to its left (i, j - 1) and to its down (i + 1, j). So after filling all the blocks marked as 1, we can start to calculate those blocks marked as 2. After that, all blocks marked as 3 and so on.
![0_1488092542752_dp.jpg](/uploads/files/1488092557587-dp.jpg) 
So in my code, I always use len to denote how far the block is away from the diagonal. So len ranges from 1 to n - 1. Remember this is the outer loop. The inner loop is all valid i positions. After filling all the upper side of the table, we will get our answer at dp[0][n - 1] (marked as 5). This is the end of my code.

6. However, if you are interviewing with a good company, they may challenge you to further improve your code, probably in the aspect of space complexity. So far, we are using a n x n matrix so the space complexity is O(n^2). It actually can be improved to O(n). That can be done by changing our way of filling the table. We may use only one dimensional dp[i] and we start to fill the table at the bottom right corner where dp[4] = nums[4]. On the next step, we start to fill the second to the last line, where it starts from dp[3] = nums[3]. Then dp[4] = Math.max(nums[4] - dp[3], nums[3] - dp[4]). Then we fill the third to the last line where dp[2] = nums[2] and so on... Eventually after we fill the first line and after the filling, dp[4] will be the answer.

7. On a related note, whenever we do sum, subtract, multiply or divide of integers, we might need to think about overflow. It doesn't seem to be a point to check for this question. However, we may want to consider using long instead of int for some cases. Further, in my way of code dp[i][j] roughly varies around zero or at least it doesn't always increases with approaching the upper right corner. So it will be less likely to overflow.
</p>


### Java '1 Line' Recursive Solution O(n^2) Time and O(n) Space
- Author: chidong
- Creation Date: Sun Jan 22 2017 12:00:42 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 06:00:26 GMT+0800 (Singapore Standard Time)

<p>
Not the most readable solution, but just for fun and for the sake for being able to put all the logic in one line. ;) (I actually think the logic is very clear and not that unreadable..) 
```
public class Solution {
    public boolean PredictTheWinner(int[] nums) {
        return helper(nums, 0, nums.length-1)>=0;
    }
    private int helper(int[] nums, int s, int e){        
        return s==e ? nums[e] : Math.max(nums[e] - helper(nums, s, e-1), nums[s] - helper(nums, s+1, e));
    }
}
```

Inspired by @sameer13, add a cache: 
```
public class Solution {
    public boolean PredictTheWinner(int[] nums) {
        return helper(nums, 0, nums.length-1, new Integer[nums.length][nums.length])>=0;
    }
    private int helper(int[] nums, int s, int e, Integer[][] mem){    
        if(mem[s][e]==null)
            mem[s][e] = s==e ? nums[e] : Math.max(nums[e]-helper(nums,s,e-1,mem),nums[s]-helper(nums,s+1,e,mem));
        return mem[s][e];
    }
}
```

**Explanation**
So assuming the sum of the array it SUM, so eventually player1 and player2 will split the SUM between themselves. For player1 to win, he/she has to get more than what player2 gets. If we think from the prospective of one player, then what he/she gains each time is a **plus**, while, what the other player gains each time is a **minus**. Eventually if player1 can have a >0 total, player1 can win. 

Helper function simulate this process. In each round: 
if e==s, there is no choice but have to select nums[s]
otherwise, this current player has 2 options: 
 --> nums[s]-helper(nums,s+1,e): this player select the front item, leaving the other player a choice from s+1 to e
 --> nums[e]-helper(nums,s,e-1): this player select the tail item, leaving the other player a choice from s to e-1
Then take the max of these two options as this player\'s selection, return it.

**complexity**
Space: O(n), the recursive can go as deep as n. 
Time: Without cache/mem, it is O(2^n), because each node can have 2 children. With cache/mem, it is O(n^2), same as DP, because at most, we will calculate all index * index combinations and others are just read from cache. There are n ^ 2 of possible combinations. 
</p>


### From Brute Force to Top-down DP
- Author: GraceMeng
- Creation Date: Tue Jul 31 2018 08:31:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 14 2020 00:25:04 GMT+0800 (Singapore Standard Time)

<p>
### Brute Force

Let\'s build the search tree first, take `nums = [3, 2, 4]` for example,
```
 [3,2,4]
   3/\4---------- 1st player\'s decision
[2,4] [3,2]
2/ \4  3/ \2----- 2nd player\'s decision
[4][2] [2][3]

currently 1st with choosable i, j,
        1.if 1st picks nums[i], 2nd can pick either ends of nums[i + 1, j]
            a.if 2nd picks nums[i + 1], 1st can pick either ends of nums[i + 2, j]
            b.if 2nd picks nums[j], 1st can pick either ends of nums[i + 1, j - 1]
            since 2nd plays to maximize his score, 1st can get nums[i] + min(1.a, 1.b)
						
        2.if 1st picks nums[j], 2nd can pick either ends of nums[i, j - 1]
            a.if 2nd picks nums[i], 1st can pick either ends of nums[i + 1, j - 1];
            b.if 2nd picks nums[j - 1], 1st can pick either ends of nums[i, j - 2];
            since 2nd plays to maximize his score, 1st can get nums[j] + min(2.a, 2.b)
        
        since the 1st plays to maximize his score, 1st can get max(nums[i] + min(1.a, 1.b), nums[j] + min(2.a, 2.b));
```

```
class Solution {
    public boolean PredictTheWinner(int[] nums) {
        
        int scoreFirst = predictTheWinnerFrom(nums, 0, nums.length - 1);
        int scoreTotal = getTotalScores(nums);
        
        // Compare final scores of two players.
        return scoreFirst >= scoreTotal - scoreFirst;
    }
    
    private int predictTheWinnerFrom(int[] nums, int i, int j) {
        if (i > j) {
            return 0;
        }
        if (i == j) {
            return nums[i];
        }
        
        int curScore = Math.max(
            nums[i] + Math.min(
                predictTheWinnerFrom(nums, i + 2, j), 
                predictTheWinnerFrom(nums, i + 1, j - 1)
            ),
            nums[j] + Math.min(
                predictTheWinnerFrom(nums, i, j - 2), 
                predictTheWinnerFrom(nums, i + 1, j - 1)
            )
        );   
        return curScore;
    }
    
    private int getTotalScores (int[] nums) {
        int scoreTotal = 0;
        for (int num : nums) {
            scoreTotal += num;
        }
        
        return scoreTotal;
    }
}
```

### Top-down DP
To overcome overlapping subproblems, we use memoization.
```
class Solution {
    public boolean PredictTheWinner(int[] nums) {
        int[][] memo = buildMemo(nums.length);
        
        int scoreFirst = predictTheWinnerFrom(nums, 0, nums.length - 1, memo);
        
        int scoreTotal = getTotalScores(nums);
        
        return scoreFirst >= scoreTotal - scoreFirst;
    }
    
    private int predictTheWinnerFrom(int[] nums, int i, int j, int[][] memo) {
        if (i > j) {
            return 0;
        }
        
        if (i == j) {
            return nums[i];
        }
        
        if (memo[i][j] != -1) {
            return memo[i][j];
        }
        
        int curScore = Math.max(
            nums[i] + Math.min(
                predictTheWinnerFrom(nums, i + 2, j, memo), 
                predictTheWinnerFrom(nums, i + 1, j - 1, memo)
            ),
            nums[j] + Math.min(
                predictTheWinnerFrom(nums, i, j - 2, memo), 
                predictTheWinnerFrom(nums, i + 1, j - 1, memo)
            )
        );   
        
        return memo[i][j] = curScore;
    }
    
    private int getTotalScores (int[] nums) {
        int scoreTotal = 0;
        for (int num : nums) {
            scoreTotal += num;
        }
        
        return scoreTotal;
    }
    
    private int[][] buildMemo(int n) {
        int[][] memo = new int[n][n];
        
        for (int[] memoRow : memo)
            Arrays.fill(memoRow, -1);
        return memo;
    }
}
```

**Python**
```
    def PredictTheWinner(self, nums):
        n = len(nums)
        memo = [[-1 for x in range(n)] for y in range(n)]      
        scoreFirst = self.PredictTheWinnerInSituation(nums, 0, n - 1, memo)
        scoreTotal = sum(nums)
        return scoreFirst >= scoreTotal - scoreFirst
    
    def PredictTheWinnerInSituation(self, nums, i, j, memo):
        # Base case.
        if i > j:
            return 0
        if i == j:
            return nums[i]
        if memo[i][j] != -1:
            return memo[i][j]
        # Recursive case.
        curScore = max(nums[i] + min(self.PredictTheWinnerInSituation(nums, i+2, j, memo), self.PredictTheWinnerInSituation(nums, i+1, j-1, memo)), 
                       nums[j] + min(self.PredictTheWinnerInSituation(nums, i, j-2, memo), self.PredictTheWinnerInSituation(nums, i+1, j-1, memo)))
        memo[i][j] = curScore        
        return curScore
```
**(\u4EBA \u2022\u0348\u1D17\u2022\u0348)** Thanks for voting!
</p>


