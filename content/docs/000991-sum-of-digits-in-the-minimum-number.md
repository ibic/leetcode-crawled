---
title: "Sum of Digits in the Minimum Number"
weight: 991
#id: "sum-of-digits-in-the-minimum-number"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of positive integers, let <code>S</code> be the sum of the digits of the minimal element of <code>A</code>.</p>

<p>Return 0 if <code>S</code> is odd, otherwise return 1.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[34,23,1,24,75,33,54,8]</span>
<strong>Output: </strong><span id="example-output-1">0</span>
<strong>Explanation: </strong>
The minimal element is 1, and the sum of those digits is S = 1 which is odd, so the answer is 0.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[99,77,33,66,55]</span>
<strong>Output: </strong><span id="example-output-2">1</span>
<strong>Explanation: </strong>
The minimal element is 33, and the sum of those digits is S = 3 + 3 = 6 which is even, so the answer is 1.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= A.length &lt;= 100</code></li>
	<li><code>1 &lt;= A[i] &lt;= 100</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Straight forward code.
- Author: rock
- Creation Date: Sun Jun 16 2019 00:05:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 16 2019 00:14:05 GMT+0800 (Singapore Standard Time)

<p>
Find the minimum then add its digits, get mod 2 of the result, use 1 to subtract it.

```
    public int sumOfDigits(int[] A) {
        int min = Arrays.stream(A).min().getAsInt();
        int ans = 0;
        while (min > 0) {
            ans += min % 10;
            min /= 10;
        }
        return 1 - ans % 2;
   }
```
</p>


### Python3 one-line XOR
- Author: ashwingo
- Creation Date: Fri Sep 13 2019 18:42:42 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 13 2019 18:42:42 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def sumOfDigits(self, A: List[int]) -> int:
        return (sum(int(x) for x in str(min(A))) % 2) ^ 1
```
</p>


### Python Beginner
- Author: dxbKing
- Creation Date: Sat Nov 02 2019 03:47:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Nov 02 2019 03:47:46 GMT+0800 (Singapore Standard Time)

<p>
```
def sumOfDigits(self, A):
    res = 0
    for a in str(min(A)):
        res += int(a)
    if res%2 == 0:
        return 1
    else: return 0
```
</p>


