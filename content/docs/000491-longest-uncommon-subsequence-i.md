---
title: "Longest Uncommon Subsequence I"
weight: 491
#id: "longest-uncommon-subsequence-i"
---
## Description
<div class="description">
<p>Given two strings <code>a</code>&nbsp;and <code>b</code>, find the length of the&nbsp;<strong>longest uncommon subsequence</strong>&nbsp;between them.</p>

<p>A&nbsp;<b>subsequence</b>&nbsp;of&nbsp;a string&nbsp;<code>s</code>&nbsp;is a string that can be obtained after deleting any number of characters from <code>s</code>. For example, <code>&quot;abc&quot;</code>&nbsp;is a subsequence of <code>&quot;aebdc&quot;</code>&nbsp;because you can delete the underlined characters in&nbsp;<code>&quot;a<u>e</u>b<u>d</u>c&quot;</code>&nbsp;to get <code>&quot;abc&quot;</code>. Other subsequences of&nbsp;<code>&quot;aebdc&quot;</code>&nbsp;include&nbsp;<code>&quot;aebdc&quot;</code>,&nbsp;<code>&quot;aeb&quot;</code>,&nbsp;and&nbsp;<code>&quot;&quot;</code>&nbsp;(empty string).</p>

<p>An&nbsp;<strong>uncommon subsequence</strong>&nbsp;between two strings&nbsp;is a string that is a <strong>subsequence of one&nbsp;but not the other</strong>.</p>

<p>Return <em>the length of the <strong>longest uncommon subsequence</strong>&nbsp;between <code>a</code>&nbsp;and <code>b</code></em>. If the longest uncommon subsequence doesn&#39;t exist, return <code>-1</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> a = &quot;aba&quot;, b = &quot;cdc&quot;
<strong>Output:</strong> 3
<strong>Explanation:</strong> One longest uncommon subsequence is &quot;aba&quot; because &quot;aba&quot; is a subsequence of &quot;aba&quot; but not &quot;cdc&quot;.
Note that &quot;cdc&quot; is also a longest uncommon subsequence.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> a = &quot;aaa&quot;, b = &quot;bbb&quot;
<strong>Output:</strong> 3
<strong>Explanation:</strong>&nbsp;The longest uncommon subsequences are &quot;aaa&quot; and &quot;bbb&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> a = &quot;aaa&quot;, b = &quot;aaa&quot;
<strong>Output:</strong> -1
<strong>Explanation:</strong>&nbsp;Every subsequence of string a is also a subsequence of string b. Similarly, every subsequence of string b is also a subsequence of string a.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= a.length, b.length &lt;= 100</code></li>
	<li><code>a</code> and <code>b</code> consist of lower-case English letters.</li>
</ul>

</div>

## Tags
- String (string)
- Brainteaser (brainteaser)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

In the brute force approach we will generate all the possible $$2^n$$ subsequences of both the strings and store their number of occurences in a hashmap.
Longest subsequence whose frequency is equal to $$1$$ will be the required subsequence.
And, if it is not found we will return $$-1$$.


<iframe src="https://leetcode.com/playground/tSXGPoqU/shared" frameBorder="0" name="tSXGPoqU" width="100%" height="479"></iframe>

**Complexity Analysis**

* Time complexity : $$O(2^x+2^y)$$. where $$x$$ and $$y$$ are the lengths of strings $$a$$ and $$b$$ respectively . Number of subsequences will be $$2^x+2^y$$.
* Space complexity : $$O(2^x+2^y)$$. $$2^x+2^y$$ subsequences will be generated.

---
#### Approach #2 Simple Solution[Accepted]

**Algorithm**

Simple analysis of this problem can lead to an easy solution.

These three cases are possible with string $$a$$ and $$b$$:-

* $$a=b$$. If both the strings are identical, it is obvious that no subsequence will be uncommon. Hence, return -1.

* $$length(a)=length(b)$$ and $$a \ne b$$. Example: $$abc$$ and $$abd$$. In this case we can consider any string i.e. $$abc$$ or $$abd$$ as a required subsequence, as out of these two strings one string will never be a subsequence of other string. Hence, return $$length(a)$$ or $$length(b)$$.

* $$length(a) \ne length(b)$$. Example $$abcd$$ and $$abc$$. In this case we can consider bigger string as a required subsequence because bigger string can't be a subsequence of smaller string. Hence, return $$max(length(a),length(b))$$.


<iframe src="https://leetcode.com/playground/YdNcPgTE/shared" frameBorder="0" name="YdNcPgTE" width="100%" height="173"></iframe>

**Complexity Analysis**

* Time complexity : $$O(min(x,y))$$. where $$x$$ and $$y$$ are the lengths of strings $$a$$ and $$b$$ respectively. Here equals method will take $$min(x,y)$$ time .

* Space complexity : $$O(1)$$. No extra space required.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### I feel this problem is just perfect for April Fools' day
- Author: zhengkai2001
- Creation Date: Sun Apr 02 2017 11:26:00 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 14:39:15 GMT+0800 (Singapore Standard Time)

<p>
I know this problem may seem obviously trivial for many programming masters, but not for me; actually I was really over-thinking into it and wondering why it's only a 3-point problem.

To the problem contributor: you really got me this time! (if the baffling problem description is intentional)

Anyone has the same feeling?
</p>


### Java 1-liner
- Author: compton_scatter
- Creation Date: Sun Apr 02 2017 11:12:23 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 23:21:31 GMT+0800 (Singapore Standard Time)

<p>
```
public int findLUSlength(String a, String b) {
    return a.equals(b) ? -1 : Math.max(a.length(), b.length());
}
```
</p>


### Just a suggestion for the LeetCode administrators
- Author: aslgomes
- Creation Date: Sun Feb 03 2019 21:10:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 03 2019 21:10:30 GMT+0800 (Singapore Standard Time)

<p>
Please listen to the users. A question like this has, at the time of this message, 188 upvotes and 2997 downvotes.

It would be amazing if some review process could happen to check why certain questions are so much downvoted by the users. They can be fixed either by rephrasing the problem statement, providing better test examples or by removing the questions completely.

I\'m sure we all enjoy learning and practising on this amazing platform so let\'s try to make it better.

We all want good, challenging and well phrased problems.
</p>


