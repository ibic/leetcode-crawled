---
title: "Increasing Subsequences"
weight: 465
#id: "increasing-subsequences"
---
## Description
<div class="description">
<p>Given an integer array, your task is to find all the different possible increasing subsequences of the given array, and the length of an increasing subsequence should be at least 2.</p>

<p>&nbsp;</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b> [4, 6, 7, 7]
<b>Output:</b> [[4, 6], [4, 7], [4, 6, 7], [4, 6, 7, 7], [6, 7], [6, 7, 7], [7,7], [4,7,7]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The length of the given array will not exceed 15.</li>
	<li>The range of integer in the given array is [-100,100].</li>
	<li>The given array may contain duplicates, and two equal integers should also be considered as a special case of increasing sequence.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Yahoo - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution beats 100%
- Author: chidong
- Creation Date: Sun Jan 22 2017 06:58:08 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 00:12:50 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public List<List<Integer>> findSubsequences(int[] nums) {
        List<List<Integer>> res = new LinkedList<>();
        helper(new LinkedList<Integer>(), 0, nums, res);
        return res; 
    }
    private void helper(LinkedList<Integer> list, int index, int[] nums, List<List<Integer>> res){
        if(list.size()>1) res.add(new LinkedList<Integer>(list));
        Set<Integer> used = new HashSet<>();
        for(int i = index; i<nums.length; i++){
            if(used.contains(nums[i])) continue;
            if(list.size()==0 || nums[i]>=list.peekLast()){
                used.add(nums[i]);
                list.add(nums[i]); 
                helper(list, i+1, nums, res);
                list.remove(list.size()-1);
            }
        }
    }
}
```

Pretty straightforward. Maybe one thing is: while nums is not necessarily sorted but we have to skip duplicates in each recursion, so we use a hash set to record what we have used in this particular recursion.
</p>


### Java 20 lines backtracking solution using set, beats 100%.
- Author: Adarsh_Kashyap
- Creation Date: Sun Jan 22 2017 04:39:36 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 03:28:25 GMT+0800 (Singapore Standard Time)

<p>

    public class Solution {

         public List<List<Integer>> findSubsequences(int[] nums) {
             Set<List<Integer>> res= new HashSet<List<Integer>>();
             List<Integer> holder = new ArrayList<Integer>();
             findSequence(res, holder, 0, nums);
             List result = new ArrayList(res);
             return result;
         }
    
        public void findSequence(Set<List<Integer>> res, List<Integer> holder, int index, int[] nums) {
            if (holder.size() >= 2) {
                res.add(new ArrayList(holder));
            }
            for (int i = index; i < nums.length; i++) {
                if(holder.size() == 0 || holder.get(holder.size() - 1) <= nums[i]) {
                    holder.add(nums[i]);
                    findSequence(res, holder, i + 1, nums);
                    holder.remove(holder.size() - 1);
                }
            }
        }
    }
</p>


### C++ dfs solution using unordered_set
- Author: emmm_
- Creation Date: Sun Jan 22 2017 06:58:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 22 2018 08:12:47 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    vector<vector<int>> findSubsequences(vector<int>& nums) {
        vector<vector<int>> res;
        vector<int> seq;
        dfs(res, seq, nums, 0);
        return res;
    }
    
    void dfs(vector<vector<int>>& res, vector<int>& seq, vector<int>& nums, int pos) {
        if(seq.size() > 1) res.push_back(seq);
        unordered_set<int> hash;
        for(int i = pos; i < nums.size(); ++i) {
            if((seq.empty() || nums[i] >= seq.back()) && hash.find(nums[i]) == hash.end()) {
                seq.push_back(nums[i]);
                dfs(res, seq, nums, i + 1);
                seq.pop_back();
                hash.insert(nums[i]);
            }
        }
    }
};
```
</p>


