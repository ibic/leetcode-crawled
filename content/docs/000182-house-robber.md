---
title: "House Robber"
weight: 182
#id: "house-robber"
---
## Description
<div class="description">
<p>You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed, the only constraint stopping you from robbing each of them is that adjacent houses have security system connected and <b>it will automatically contact the police if two adjacent houses were broken into on the same night</b>.</p>

<p>Given a list of non-negative integers representing the amount of money of each house, determine the maximum amount of money you can rob tonight <b>without alerting the police</b>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,1]
<strong>Output:</strong> 4
<strong>Explanation:</strong> Rob house 1 (money = 1) and then rob house 3 (money = 3).
&nbsp;            Total amount you can rob = 1 + 3 = 4.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [2,7,9,3,1]
<strong>Output:</strong> 12
<strong>Explanation:</strong> Rob house 1 (money = 2), rob house 3 (money = 9) and rob house 5 (money = 1).
&nbsp;            Total amount you can rob = 2 + 9 + 1 = 12.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= nums.length &lt;= 100</code></li>
	<li><code>0 &lt;= nums[i] &lt;= 400</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Cisco - 16 (taggedByAdmin: false)
- Google - 8 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Expedia - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- HBO - 4 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: true)
- Walmart Labs - 2 (taggedByAdmin: false)
- Quora - 2 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 (Dynamic Programming) [Accepted]

**Algorithm**

It could be overwhelming thinking of all possibilities on which houses to rob.

A natural way to approach this problem is to work on the simplest case first.

Let us denote that:

>*f*(*k*) = Largest amount that you can rob from the first *k* houses.  
A<sub>i</sub> = Amount of money at the i<sup>th</sup> house.

Let us look at the case `n = 1`, clearly *f*(1) = A<sub>1</sub>.

Now, let us look at `n = 2`, which *f*(2) = max(A<sub>1</sub>, A<sub>2</sub>).

For `n = 3`, you have basically the following two options:

1. Rob the third house, and add its amount to the first house's amount.

2. Do not rob the third house, and stick with the maximum amount of the first two houses.

Clearly, you would want to choose the larger of the two options at each step.

Therefore, we could summarize the formula as following:

>*f*(*k*) = max(*f*(*k* – 2) + A<sub>k</sub>, *f*(*k* – 1))

We choose the base case as *f*(–1) = *f*(0) = 0, which will greatly simplify our code as you can see.

The answer will be calculated as *f*(*n*). We could use an array to store and calculate the result, but since at each step you only need the previous two maximum values, two variables are suffice.

```java
public int rob(int[] num) {
    int prevMax = 0;
    int currMax = 0;
    for (int x : num) {
        int temp = currMax;
        currMax = Math.max(prevMax + x, currMax);
        prevMax = temp;
    }
    return currMax;
}
```

**Complexity analysis**

* Time complexity : $$O(n)$$.
Assume that $$n$$ is the number of houses, the time complexity is $$O(n)$$.

* Space complexity : $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### From good to great. How to approach most of DP problems.
- Author: heroes3001
- Creation Date: Sat Aug 04 2018 18:43:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 11:58:18 GMT+0800 (Singapore Standard Time)

<p>
There is some frustration when people publish their perfect fine-grained algorithms without sharing any information abut how they were derived. This is an attempt to change the situation. There is not much more explanation but it\'s rather an example of higher level improvements. Converting a solution to the next step shouldn\'t be as hard as attempting to come up with perfect algorithm at first attempt.

This particular problem and most of others can be approached using the following sequence:
1. Find recursive relation
2. Recursive (top-down)
3. Recursive + memo (top-down) 
4. Iterative + memo (bottom-up) 
5. Iterative + N variables (bottom-up)

**Step 1.** Figure out recursive relation. 
A robber has 2 options: a) rob current house `i`; b) don\'t rob current house. 
If an option "a" is selected it means she can\'t rob previous `i-1` house but can safely proceed to the one before previous `i-2` and gets all cumulative loot that follows.
If an option "b" is selected the robber gets all the possible loot from robbery of `i-1` and all the following buildings.
So it boils down to calculating what is more profitable: 
* robbery of current house + loot from houses before the previous
* loot from the previous house robbery and any loot captured before that

`rob(i) = Math.max( rob(i - 2) + currentHouseValue, rob(i - 1) )`

**Step 2.** Recursive (top-down)
Converting the recurrent relation from Step 1 shound\'t be very hard.
```
public int rob(int[] nums) {
    return rob(nums, nums.length - 1);
}
private int rob(int[] nums, int i) {
    if (i < 0) {
        return 0;
    }
    return Math.max(rob(nums, i - 2) + nums[i], rob(nums, i - 1));
}
```
This algorithm will process the same `i` multiple times and it needs improvement. Time complexity: [to fill]

**Step 3.** Recursive + memo (top-down).
```
int[] memo;
public int rob(int[] nums) {
    memo = new int[nums.length + 1];
    Arrays.fill(memo, -1);
    return rob(nums, nums.length - 1);
}

private int rob(int[] nums, int i) {
    if (i < 0) {
        return 0;
    }
    if (memo[i] >= 0) {
        return memo[i];
    }
    int result = Math.max(rob(nums, i - 2) + nums[i], rob(nums, i - 1));
    memo[i] = result;
    return result;
}
```
Much better, this should run in `O(n)` time. Space complexity is `O(n)` as well, because of the recursion stack, let\'s try to get rid of it.

**Step 4.** Iterative + memo (bottom-up) 
```
public int rob(int[] nums) {
    if (nums.length == 0) return 0;
    int[] memo = new int[nums.length + 1];
    memo[0] = 0;
    memo[1] = nums[0];
    for (int i = 1; i < nums.length; i++) {
        int val = nums[i];
        memo[i+1] = Math.max(memo[i], memo[i-1] + val);
    }
    return memo[nums.length];
}
```

**Step 5.** Iterative + 2 variables (bottom-up)
We can notice that in the previous step we use only `memo[i]` and `memo[i-1]`, so going just 2 steps back. We can hold them in 2 variables instead. This optimization is met in Fibonacci sequence creation and some other problems [to paste links].

```
/* the order is: prev2, prev1, num  */
public int rob(int[] nums) {
    if (nums.length == 0) return 0;
    int prev1 = 0;
    int prev2 = 0;
    for (int num : nums) {
        int tmp = prev1;
        prev1 = Math.max(prev2 + num, prev1);
        prev2 = tmp;
    }
    return prev1;
}
```


</p>


### C 1ms, O(1)space,  very simple solution
- Author: Jasonly
- Creation Date: Wed Apr 01 2015 00:13:34 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 21:18:34 GMT+0800 (Singapore Standard Time)

<p>
    #define max(a, b) ((a)>(b)?(a):(b))
    int rob(int num[], int n) {
        int a = 0;
        int b = 0;
        
        for (int i=0; i<n; i++)
        {
            if (i%2==0)
            {
                a = max(a+num[i], b);
            }
            else
            {
                b = max(a, b+num[i]);
            }
        }
        
        return max(a, b);
    }
</p>


### Java O(n) solution, space O(1)
- Author: tusizi
- Creation Date: Tue Mar 31 2015 17:10:10 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 21:50:12 GMT+0800 (Singapore Standard Time)

<p>
    public int rob(int[] num) {
        int[][] dp = new int[num.length + 1][2];
        for (int i = 1; i <= num.length; i++) {
            dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1]);
            dp[i][1] = num[i - 1] + dp[i - 1][0];
        }
        return Math.max(dp[num.length][0], dp[num.length][1]);
    }

dp[i][1] means we rob the current house and dp[i][0] means we don't,

so it is easy to convert this to O(1) space

    public int rob(int[] num) {
        int prevNo = 0;
        int prevYes = 0;
        for (int n : num) {
            int temp = prevNo;
            prevNo = Math.max(prevNo, prevYes);
            prevYes = n + temp;
        }
        return Math.max(prevNo, prevYes);
    }
</p>


