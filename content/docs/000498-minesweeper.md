---
title: "Minesweeper"
weight: 498
#id: "minesweeper"
---
## Description
<div class="description">
<p>Let&#39;s play the minesweeper game (<a href="https://en.wikipedia.org/wiki/Minesweeper_(video_game)">Wikipedia</a>, <a href="http://minesweeperonline.com">online game</a>)!</p>

<p>You are given a 2D char matrix representing the game board. <b>&#39;M&#39;</b> represents an <b>unrevealed</b> mine, <b>&#39;E&#39;</b> represents an <b>unrevealed</b> empty square, <b>&#39;B&#39;</b> represents a <b>revealed</b> blank square that has no adjacent (above, below, left, right, and all 4 diagonals) mines, <b>digit</b> (&#39;1&#39; to &#39;8&#39;) represents how many mines are adjacent to this <b>revealed</b> square, and finally <b>&#39;X&#39;</b> represents a <b>revealed</b> mine.</p>

<p>Now given the next click position (row and column indices) among all the <b>unrevealed</b> squares (&#39;M&#39; or &#39;E&#39;), return the board after revealing this position according to the following rules:</p>

<ol>
	<li>If a mine (&#39;M&#39;) is revealed, then the game is over - change it to <b>&#39;X&#39;</b>.</li>
	<li>If an empty square (&#39;E&#39;) with <b>no adjacent mines</b> is revealed, then change it to revealed blank (&#39;B&#39;) and all of its adjacent <b>unrevealed</b> squares should be revealed recursively.</li>
	<li>If an empty square (&#39;E&#39;) with <b>at least one adjacent mine</b> is revealed, then change it to a digit (&#39;1&#39; to &#39;8&#39;) representing the number of adjacent mines.</li>
	<li>Return the board when no more squares will be revealed.</li>
</ol>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> 

[[&#39;E&#39;, &#39;E&#39;, &#39;E&#39;, &#39;E&#39;, &#39;E&#39;],
 [&#39;E&#39;, &#39;E&#39;, &#39;M&#39;, &#39;E&#39;, &#39;E&#39;],
 [&#39;E&#39;, &#39;E&#39;, &#39;E&#39;, &#39;E&#39;, &#39;E&#39;],
 [&#39;E&#39;, &#39;E&#39;, &#39;E&#39;, &#39;E&#39;, &#39;E&#39;]]

Click : [3,0]

<b>Output:</b> 

[[&#39;B&#39;, &#39;1&#39;, &#39;E&#39;, &#39;1&#39;, &#39;B&#39;],
 [&#39;B&#39;, &#39;1&#39;, &#39;M&#39;, &#39;1&#39;, &#39;B&#39;],
 [&#39;B&#39;, &#39;1&#39;, &#39;1&#39;, &#39;1&#39;, &#39;B&#39;],
 [&#39;B&#39;, &#39;B&#39;, &#39;B&#39;, &#39;B&#39;, &#39;B&#39;]]

<b>Explanation:</b>
<img src="https://assets.leetcode.com/uploads/2018/10/12/minesweeper_example_1.png" style="width: 100%; max-width: 400px" />
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> 

[[&#39;B&#39;, &#39;1&#39;, &#39;E&#39;, &#39;1&#39;, &#39;B&#39;],
 [&#39;B&#39;, &#39;1&#39;, &#39;M&#39;, &#39;1&#39;, &#39;B&#39;],
 [&#39;B&#39;, &#39;1&#39;, &#39;1&#39;, &#39;1&#39;, &#39;B&#39;],
 [&#39;B&#39;, &#39;B&#39;, &#39;B&#39;, &#39;B&#39;, &#39;B&#39;]]

Click : [1,2]

<b>Output:</b> 

[[&#39;B&#39;, &#39;1&#39;, &#39;E&#39;, &#39;1&#39;, &#39;B&#39;],
 [&#39;B&#39;, &#39;1&#39;, &#39;X&#39;, &#39;1&#39;, &#39;B&#39;],
 [&#39;B&#39;, &#39;1&#39;, &#39;1&#39;, &#39;1&#39;, &#39;B&#39;],
 [&#39;B&#39;, &#39;B&#39;, &#39;B&#39;, &#39;B&#39;, &#39;B&#39;]]

<b>Explanation:</b>
<img src="https://assets.leetcode.com/uploads/2018/10/12/minesweeper_example_2.png" style="width: 100%; max-width: 400px" />
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>The range of the input matrix&#39;s height and width is [1,50].</li>
	<li>The click position will only be an unrevealed square (&#39;M&#39; or &#39;E&#39;), which also means the input board contains at least one clickable square.</li>
	<li>The input board won&#39;t be a stage when game is over (some mines have been revealed).</li>
	<li>For simplicity, not mentioned rules should be ignored in this problem. For example, you <b>don&#39;t</b> need to reveal all the unrevealed mines when the game is over, consider any cases that you will win the game or flag any squares.</li>
</ol>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 4 (taggedByAdmin: true)
- Facebook - 4 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- LiveRamp - 4 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Cruise Automation - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (java)
```java
class Solution {
    private boolean[][] visited;
    private char digitToChar(int digit) {
        return (char)((int)'0' + digit);
    }

    private void updateSurroundings(char[][] board, int y, int x, int height, int width) {
        List<int[]> neighbors = Arrays.stream(new int[][] {
            {y - 1, x - 1},
            {y - 1, x},
            {y - 1, x + 1},
            {y, x - 1},
            {y, x + 1},
            {y + 1, x - 1},
            {y + 1, x},
            {y + 1, x + 1},
        })
        .filter((int[] pos) -> pos[0] >= 0 && pos[0] < height && pos[1] >= 0 && pos[1] < width
            && !this.visited[pos[0]][pos[1]])

        .collect(Collectors.toList());

        int mines = neighbors.stream()
        .mapToInt(
            (int[] a) -> board[a[0]][a[1]] == 'M' ? 1 : 0
        ).sum();
        if (mines == 0) {
            board[y][x] = 'B';
            this.visited[y][x] = true;
            for (int[] neighbor: neighbors) {
                updateSurroundings(board, neighbor[0], neighbor[1], height, width);
            }
        } else {
            board[y][x] = digitToChar(mines);
        }
    }

    public char[][] updateBoard(char[][] board, int[] click) {
        int height = board.length;
        int width = board[0].length;
        this.visited = new boolean[height][width];
        // for (int i = 0; i < height; i++) {
        //     for (int j = 0; i < width; j++) {
        //         this.visited[i][j] = false;
        //     }
        // }
        int y = click[0];
        int x = click[1];
        if (board[y][x] == 'M') {
            board[y][x] = 'X';
        } else if (board[y][x] == 'E') { // unnecessary comparison, but just keep for clarity's seek
            updateSurroundings(board, y, x, height, width);
        }
        return board;
    }
}
```

## Top Discussions
### Java Solution, DFS + BFS
- Author: shawngao
- Creation Date: Sun Feb 26 2017 12:03:27 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 20:34:45 GMT+0800 (Singapore Standard Time)

<p>
This is a typical ```Search``` problem, either by using ```DFS``` or ```BFS```. Search rules:
1. If click on a mine ('```M```'), mark it as '```X```', stop further search.
2. If click on an empty cell ('```E```'), depends on how many surrounding mine:
2.1 Has surrounding mine(s), mark it with number of surrounding mine(s), stop further search.
2.2 No surrounding mine, mark it as '```B```', continue search its ```8``` neighbors. 

DFS solution. 

```
public class Solution {
    public char[][] updateBoard(char[][] board, int[] click) {
        int m = board.length, n = board[0].length;
        int row = click[0], col = click[1];
        
        if (board[row][col] == 'M') { // Mine
            board[row][col] = 'X';
        }
        else { // Empty
            // Get number of mines first.
            int count = 0;
            for (int i = -1; i < 2; i++) {
                for (int j = -1; j < 2; j++) {
                    if (i == 0 && j == 0) continue;
                    int r = row + i, c = col + j;
                    if (r < 0 || r >= m || c < 0 || c < 0 || c >= n) continue;
                    if (board[r][c] == 'M' || board[r][c] == 'X') count++;
                }
            }
            
            if (count > 0) { // If it is not a 'B', stop further DFS.
                board[row][col] = (char)(count + '0');
            }
            else { // Continue DFS to adjacent cells.
                board[row][col] = 'B';
                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        if (i == 0 && j == 0) continue;
                        int r = row + i, c = col + j;
                        if (r < 0 || r >= m || c < 0 || c < 0 || c >= n) continue;
                        if (board[r][c] == 'E') updateBoard(board, new int[] {r, c});
                    }
                }
            }
        }
        
        return board;
    }
}
```

BFS solution. As you can see the basic logic is almost the same as DFS. Only added a queue to facilitate BFS.
```
public class Solution {
    public char[][] updateBoard(char[][] board, int[] click) {
        int m = board.length, n = board[0].length;
        Queue<int[]> queue = new LinkedList<>();
        queue.add(click);
        
        while (!queue.isEmpty()) {
            int[] cell = queue.poll();
            int row = cell[0], col = cell[1];
            
            if (board[row][col] == 'M') { // Mine
                board[row][col] = 'X';
            }
            else { // Empty
                // Get number of mines first.
                int count = 0;
                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        if (i == 0 && j == 0) continue;
                        int r = row + i, c = col + j;
                        if (r < 0 || r >= m || c < 0 || c < 0 || c >= n) continue;
                        if (board[r][c] == 'M' || board[r][c] == 'X') count++;
                    }
                }
                
                if (count > 0) { // If it is not a 'B', stop further BFS.
                    board[row][col] = (char)(count + '0');
                }
                else { // Continue BFS to adjacent cells.
                    board[row][col] = 'B';
                    for (int i = -1; i < 2; i++) {
                        for (int j = -1; j < 2; j++) {
                            if (i == 0 && j == 0) continue;
                            int r = row + i, c = col + j;
                            if (r < 0 || r >= m || c < 0 || c < 0 || c >= n) continue;
                            if (board[r][c] == 'E') {
                                queue.add(new int[] {r, c});
                                board[r][c] = 'B'; // Avoid to be added again.
                            }
                        }
                    }
                }
            }
        }
        
        return board;
    }
}
```
</p>


### Straight forward Java solution
- Author: tankztc
- Creation Date: Sun Feb 26 2017 12:04:25 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 27 2019 05:08:53 GMT+0800 (Singapore Standard Time)

<p>
DFS solution:
```
public class Solution {
    public char[][] updateBoard(char[][] board, int[] click) {
        int x = click[0], y = click[1];
        if (board[x][y] == \'M\') {
            board[x][y] = \'X\';
            return board;
        }
        
        dfs(board, x, y);
        return board;
    }
    
    int[] dx = {-1, 0, 1, -1, 1, 0, 1, -1};
    int[] dy = {-1, 1, 1, 0, -1, -1, 0, 1};
    private void dfs(char[][] board, int x, int y) {
        if (x < 0 || x >= board.length || y < 0 || y >= board[0].length || board[x][y] != \'E\')  return;
        
        int num = getNumsOfBombs(board, x, y);
    
        if (num == 0) {
            board[x][y] = \'B\';
            for (int i = 0; i < 8; i++) {
                int nx = x + dx[i], ny = y + dy[i];
                dfs(board, nx, ny);
            }
        } else {
            board[x][y] = (char)(\'0\' + num);
        }
        
    }
    
    private int getNumsOfBombs(char[][] board, int x, int y) {
        int num = 0;
        for (int i = 0; i < 8; i++) {
            int nx = x + dx[i], ny = y + dy[i];
            if (nx < 0 || nx >= board.length || ny < 0 || ny >= board[0].length)    continue;
            if (board[nx][ny] == \'M\' || board[nx][ny] == \'X\') {
                num++;
            }
        }
        return num;
    }
}
```

BFS solution:
```
class Solution {
    int[] dx = new int[]{0, -1, 0, 1, 1, -1, 1, -1};
    int[] dy = new int[]{1, 0, -1, 0, 1, -1, -1, 1};
    
    public char[][] updateBoard(char[][] board, int[] click) {
        if (board[click[0]][click[1]] == \'M\') {
            board[click[0]][click[1]] = \'X\';
            return board;
        }
        
        int m = board.length, n = board[0].length;
        
        Queue<int[]> queue = new LinkedList();
        boolean[][] visited = new boolean[m][n];
        
        queue.offer(click);
        visited[click[0]][click[1]] = true;
        
        while (!queue.isEmpty()) {
            int[] cur = queue.poll();
            int x = cur[0], y = cur[1];
            int mines = getNumOfBombs(board, x, y);
            
            if (mines == 0) {
                board[x][y] = \'B\';
                for (int i = 0; i < 8; ++i) {
                    int nx = x + dx[i], ny = y + dy[i];
                
                    if (nx >= 0 && ny >= 0 && nx < board.length && ny < board[0].length && !visited[nx][ny] && board[nx][ny] == \'E\') {
                        queue.offer(new int[]{nx, ny});
                        visited[nx][ny] = true;
                    }
                }
            } else {
                board[x][y] = (char)(mines + \'0\');
            }
        }
        
        return board;
    }
    
    int getNumOfBombs(char[][] board, int x, int y) {
        int count = 0;
        for (int i = 0; i < 8; ++i) {
            int nx = x + dx[i], ny = y + dy[i];
            
            if (nx == x && ny == y) continue;

            if (nx >= 0 && ny >= 0 && nx < board.length && ny < board[0].length) {
                count += board[nx][ny] == \'M\' ? 1 : 0;
            }
          
        }
        return count;
    }
}
```
</p>


### Python DFS solution
- Author: RogerFederer
- Creation Date: Sun Jun 10 2018 03:51:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 10 2018 03:51:04 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def updateBoard(self, board, click):
        """
        :type board: List[List[str]]
        :type click: List[int]
        :rtype: List[List[str]]
        """
        if not board:
            return []

        m, n = len(board), len(board[0])
        i, j = click[0], click[1]

        # If a mine (\'M\') is revealed, then the game is over - change it to \'X\'.
        if board[i][j] == \'M\':
            board[i][j] = \'X\'
            return board

        # run dfs to reveal the board
        self.dfs(board, i, j)
        return board

    def dfs(self, board, i, j):
        if board[i][j] != \'E\':
            return

        m, n = len(board), len(board[0])       
        directions = [(-1,-1), (0,-1), (1,-1), (1,0), (1,1), (0,1), (-1,1), (-1,0)]

        mine_count = 0

        for d in directions:
            ni, nj = i + d[0], j + d[1]
            if 0 <= ni < m and 0 <= nj < n and board[ni][nj] == \'M\':        
                mine_count += 1

        if mine_count == 0:
            board[i][j] = \'B\'
        else:
            board[i][j] = str(mine_count)
            return

        for d in directions:
            ni, nj = i + d[0], j + d[1]
            if 0 <= ni < m and 0 <= nj < n:
                self.dfs(board, ni, nj)
```
</p>


