---
title: "Check If It Is a Straight Line"
weight: 1181
#id: "check-if-it-is-a-straight-line"
---
## Description
<div class="description">
<p>You are given an array&nbsp;<code>coordinates</code>, <code>coordinates[i] = [x, y]</code>, where <code>[x, y]</code> represents the coordinate of a point. Check if these points&nbsp;make a straight line in the XY plane.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/10/15/untitled-diagram-2.jpg" style="width: 336px; height: 336px;" /></p>

<pre>
<strong>Input:</strong> coordinates = [[1,2],[2,3],[3,4],[4,5],[5,6],[6,7]]
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/10/09/untitled-diagram-1.jpg" style="width: 348px; height: 336px;" /></strong></p>

<pre>
<strong>Input:</strong> coordinates = [[1,1],[2,2],[3,4],[4,5],[5,6],[7,7]]
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;=&nbsp;coordinates.length &lt;= 1000</code></li>
	<li><code>coordinates[i].length == 2</code></li>
	<li><code>-10^4 &lt;=&nbsp;coordinates[i][0],&nbsp;coordinates[i][1] &lt;= 10^4</code></li>
	<li><code>coordinates</code>&nbsp;contains no duplicate point.</li>
</ul>

</div>

## Tags
- Array (array)
- Math (math)
- Geometry (geometry)

## Companies
- Palantir - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] check slopes, short code w/ explanation and analysis.
- Author: rock
- Creation Date: Sun Oct 20 2019 12:03:21 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 08 2020 22:16:56 GMT+0800 (Singapore Standard Time)

<p>
The slope for a line through any 2 points `(x0, y0)` and `(x1, y1)` is `(y1 - y0) / (x1 - x0)`; Therefore, for any given 3 points (denote the 3rd point as `(x, y)`), if they are in a straight line, the slopes of the lines from the 3rd point to the 2nd point and the 2nd point to the 1st point must be equal:
```
(y - y1) / (x - x1) = (y1 - y0) / (x1 - x0)
```
In order to avoid being divided by 0, use multiplication form:
```
(x1 - x0) * (y - y1) = (x - x1) * (y1 - y0) =>
dx * (y - y1) = dy * (x - x1), where dx = x1 - x0 and dy = y1 - y0
```

Now imagine connecting the 2nd points respectively with others one by one, Check if all of the slopes are equal.

```
    public boolean checkStraightLine(int[][] coordinates) {
        int x0 = coordinates[0][0], y0 = coordinates[0][1], 
            x1 = coordinates[1][0], y1 = coordinates[1][1];
        int dx = x1 - x0, dy = y1 - y0;
        for (int[] co : coordinates) {
            int x = co[0], y = co[1];
            if (dx * (y - y1) != dy * (x - x1))
                return false;
        }
        return true;
    }
```
```
    def checkStraightLine(self, coordinates: List[List[int]]) -> bool:
        (x0, y0), (x1, y1) = coordinates[: 2]
        for x, y in coordinates:
            if (x1 - x0) * (y - y1) != (x - x1) * (y1 - y0):
                return False
        return True
```
or simpler:
```
    def checkStraightLine(self, coordinates: List[List[int]]) -> bool:
        (x0, y0), (x1, y1) = coordinates[: 2]
        return all((x1 - x0) * (y - y1) == (x - x1) * (y1 - y0) for x, y in coordinates)
```

**Analysis**

Time: O(n), space: O(1), where n = coordinates.length.
</p>


### [Java] [Python3] [CPP] | Simple code with explanation | 100% fast | O(1) space
- Author: logan138
- Creation Date: Fri May 08 2020 16:08:49 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 08 2020 23:56:08 GMT+0800 (Singapore Standard Time)

<p>

**The point is  if we take points p1(x, y), p2(x1, y1), p3(x3, y3),  slopes of any two pairs is same then p1, p2, p3 lies on same line.
     slope from p1 and p2 is  y - y1 / x - x1
     slope from p2 and p3 is  y2 - y1 / x2 - x1
if these two slopes equal, then p1, p2, p3 lies on same line.**

**IF YOU HAVE ANY DOUBTS, FEEL FREE TO ASK**
**IF YOU UNDERSTAND, DON\'T FORGET TO UPVOTE**

![image](https://assets.leetcode.com/users/logan138/image_1588922156.png)

```
JAVA:-  

class Solution {
    public boolean onLine(int[] p1, int[] p2, int[] p3){
        int x = p1[0], y = p1[1], x1 = p2[0], y1 = p2[1], x2 = p3[0], y2 = p3[1];
        return ((y - y1) * (x2 - x1) == (y2 - y1) * (x - x1));
    }
    public boolean checkStraightLine(int[][] coordinates) {
        // base case:- there are only two points, return true
        // otherwise, check each point lies on line using above equation.
		
        for(int i=2;i<coordinates.length;i++){
            if(!onLine(coordinates[i], coordinates[0], coordinates[1]))
                return false;
        }
        return true;
    }
}


Python3:-

class Solution:
    def checkStraightLine(self, coordinates: List[List[int]]) -> bool:
        (x1, y1), (x2, y2) = coordinates[:2]
        for i in range(2, len(coordinates)):
            (x, y) = coordinates[i]
            if((y2 - y1) * (x1 - x) != (y1 - y) * (x2 - x1)):
                return False
        return True		

CPP:-
\u200Bclass Solution {
public:
    bool checkStraightLine(vector<vector<int>>& coordinates) {
        if(coordinates.size() <= 2) return true;
        int x1 = coordinates[0][0];
        int y1 = coordinates[0][1];
        int x2 = coordinates[1][0];
        int y2 = coordinates[1][1];
        for(int i=2;i<coordinates.size();i++){
            int x = coordinates[i][0];
            int y = coordinates[i][1];
            if((y2 - y1) * (x1 - x) != (y1 - y) * (x2 - x1))
                return false;
        }
        return true;
    }
};
```

</p>


### Check collinearity
- Author: Poorvank
- Creation Date: Sun Oct 20 2019 12:02:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 12:02:13 GMT+0800 (Singapore Standard Time)

<p>
Three points lie on the straight line if the area formed by the triangle of these three points is zero.
Formula for area of triangle is :
**0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2))**


```
 public boolean checkStraightLine(int[][] coordinates) {
        int n = coordinates.length;
        for(int end=0;end<=n-3;end++) {
            int[] c1 = coordinates[end];
            int[] c2 = coordinates[end+1];
            int[] c3 = coordinates[end+2];
            if(!isCollinear(c1[0],c1[1],c2[0],c2[1],c3[0],c3[1])) {
                return false;
            }
        }
        return true;
    }

    private boolean isCollinear(int x1, int y1, int x2,int y2, int x3, int y3) {
        int result = x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2);
        return result==0;
    }
```
</p>


