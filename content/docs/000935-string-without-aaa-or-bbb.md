---
title: "String Without AAA or BBB"
weight: 935
#id: "string-without-aaa-or-bbb"
---
## Description
<div class="description">
<p>Given two integers <code>A</code> and <code>B</code>, return <strong>any</strong> string <code>S</code> such that:</p>

<ul>
	<li><code>S</code> has length <code>A + B</code> and contains exactly <code>A</code> <code>&#39;a&#39;</code> letters, and exactly <code>B</code> <code>&#39;b&#39;</code> letters;</li>
	<li>The substring&nbsp;<code>&#39;aaa&#39;</code>&nbsp;does not occur in <code>S</code>;</li>
	<li>The substring <code>&#39;bbb&#39;</code> does not occur in <code>S</code>.</li>
</ul>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">1</span>, B = <span id="example-input-1-2">2</span>
<strong>Output: </strong><span id="example-output-1">&quot;abb&quot;
</span><strong>Explanation:</strong> &quot;abb&quot;, &quot;bab&quot; and &quot;bba&quot; are all correct answers.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">4</span>, B = <span id="example-input-2-2">1</span>
<strong>Output: </strong><span id="example-output-2">&quot;aabaa&quot;</span></pre>

<p>&nbsp;</p>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= A &lt;= 100</code></li>
	<li><code>0 &lt;= B &lt;= 100</code></li>
	<li>It is guaranteed such an <code>S</code> exists for the given <code>A</code> and <code>B</code>.</li>
</ol>

</div>

## Tags
- Greedy (greedy)

## Companies
- Grab - 4 (taggedByAdmin: false)
- zalando - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Greedy

**Intuition**

Intuitively, we should write the most common letter first.  For example, if we have `A = 6, B = 2`, we want to write `'aabaabaa'`.  The only time we don't write the most common letter is if the last two letters we have written are also the most common letter

**Algorithm**

Let's maintain `A, B`: the number of `'a'` and `'b'`'s left to write.

If we have already written the most common letter twice, we'll write the other letter.  Otherwise, we'll write the most common letter.

<iframe src="https://leetcode.com/playground/y4AwjqMA/shared" frameBorder="0" width="100%" height="500" name="y4AwjqMA"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(A+B)$$.

* Space Complexity:  $$O(A+B)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java/C++ (and Python) simple greedy
- Author: votrubac
- Creation Date: Sun Jan 27 2019 12:01:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 27 2019 12:01:24 GMT+0800 (Singapore Standard Time)

<p>
First, I must note that this problem does not feel like \'Easy\'. The implementation is easy, but the intuition is not. In fact, I spent more time on this problem then on other \'Medium\' problems in the contest.

- If the initial number of As is greater than Bs, we swap A and B.
- For each turn, we add \'a\' to our string.
- If the number of remaining As is greater than Bs, we add one more \'a\'.
- Finally, we add \'b\'. 
<iframe src="https://leetcode.com/playground/VkpuNFz6/shared" frameBorder="0" width="100%" height="210"></iframe>

If you feel rather functional, here is a 3-liner (on par with the Python solution, courtesy of [@zengxinhai](https://leetcode.com/zengxinhai)):
<iframe src="https://leetcode.com/playground/wkpugsHY/shared" frameBorder="0" width="100%" height="150"></iframe>
</p>


### Clean C++/python solution
- Author: lbjlc
- Creation Date: Sun Jan 27 2019 12:33:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 27 2019 12:33:45 GMT+0800 (Singapore Standard Time)

<p>
C++:
```
class Solution {
public:
    string strWithout3a3b(int A, int B) {
        if(A == 0) return string(B, \'b\');
        else if(B == 0) return string(A, \'a\');
        else if(A == B) return "ab" + strWithout3a3b(A - 1, B - 1);
        else if(A > B) return "aab" + strWithout3a3b(A - 2, B - 1);
        else return strWithout3a3b(A - 1, B - 2) + "abb";
    }
};
```

Python:
```
class Solution(object):
    def strWithout3a3b(self, A, B):
        if A == 0:      
			return \'b\' * B
        elif B == 0:    
			return \'a\' * A
        elif A == B:    
			return \'ab\' * A
        elif A > B:     
			return \'aab\' + self.strWithout3a3b(A - 2, B - 1)
        else:           
			return self.strWithout3a3b(A - 1, B - 2) + \'abb\'
```
</p>


### [Java] Two simple logic readable codes, iterative and recursive versions.
- Author: rock
- Creation Date: Sun Jan 27 2019 12:22:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 27 2019 12:22:33 GMT+0800 (Singapore Standard Time)

<p>
1. If current result ends with `aa`, next char is `b`; if ends with `bb`, next char must be `a`.
2. Other cases, if A > B, next char is `a`; otherwise, next char is `b`.

```
    public String strWithout3a3b(int A, int B) {
        StringBuilder sb = new StringBuilder(A + B);
        while (A + B > 0) {
            String s = sb.toString();
            if (s.endsWith("aa")) {
                sb.append("b");
                --B;       
            }else if (s.endsWith("bb")){
                sb.append("a");
                --A;
            }else if (A > B) {
                sb.append("a");
                --A;
            }else {
                sb.append("b");
                --B;
            }
        }
        return sb.toString();
    }
```

Recursive Version, inspired by @lbjlc
 
```
    public String strWithout3a3b(int A, int B) {
        if (A == 0 || B == 0) { return String.join("", Collections.nCopies(A + B, A == 0 ? "b" : "a")); } // if A or B is 0, return A \'a\'s or B \'b\'s.
        if (A == B) { return "ab" + strWithout3a3b(A - 1, B - 1); }
        if (A > B) { return "aab" + strWithout3a3b(A - 2, B - 1); }
        return "bba" + strWithout3a3b(A - 1, B - 2);
    }
```
</p>


