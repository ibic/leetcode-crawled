---
title: "Dinner Plate Stacks"
weight: 1135
#id: "dinner-plate-stacks"
---
## Description
<div class="description">
<p>You have an infinite number of stacks arranged in a row and numbered (left to right) from 0, each of the stacks has the same&nbsp;maximum <code>capacity</code>.</p>

<p>Implement the <code>DinnerPlates</code> class:</p>

<ul>
	<li><code>DinnerPlates(int capacity)</code> Initializes the object with the maximum <code>capacity</code> of the stacks.</li>
	<li><code>void push(int val)</code>&nbsp;Pushes the given positive integer <code>val</code> into the leftmost stack with size less than <code>capacity</code>.</li>
	<li><code>int pop()</code>&nbsp;Returns the value at the top of the rightmost non-empty stack and removes it from that stack, and returns <code>-1</code> if all stacks are empty.</li>
	<li><code>int popAtStack(int index)</code>&nbsp;Returns the value at the top of the stack with the given <code>index</code> and removes it from that stack, and returns -1 if the stack with that&nbsp;given <code>index</code> is empty.</li>
</ul>

<p><strong>Example:</strong></p>

<pre>
<b>Input: </b>
[&quot;DinnerPlates&quot;,&quot;push&quot;,&quot;push&quot;,&quot;push&quot;,&quot;push&quot;,&quot;push&quot;,&quot;popAtStack&quot;,&quot;push&quot;,&quot;push&quot;,&quot;popAtStack&quot;,&quot;popAtStack&quot;,&quot;pop&quot;,&quot;pop&quot;,&quot;pop&quot;,&quot;pop&quot;,&quot;pop&quot;]
[[2],[1],[2],[3],[4],[5],[0],[20],[21],[0],[2],[],[],[],[],[]]
<b>Output: </b>
[null,null,null,null,null,null,2,null,null,20,21,5,4,3,1,-1]

<b>Explanation: </b>
DinnerPlates D = DinnerPlates(2);  // Initialize with capacity = 2
D.push(1);
D.push(2);
D.push(3);
D.push(4);
D.push(5);         // The stacks are now:  2 &nbsp;4
&nbsp;                                          1 &nbsp;3 &nbsp;5
                                           ﹈ ﹈ ﹈
D.popAtStack(0);   // Returns 2.  The stacks are now:    &nbsp;4
            &nbsp;                                          1 &nbsp;3 &nbsp;5
                                                       ﹈ ﹈ ﹈
D.push(20);        // The stacks are now: 20  4
&nbsp;                                          1 &nbsp;3 &nbsp;5
                                           ﹈ ﹈ ﹈
D.push(21);        // The stacks are now: 20  4 21
&nbsp;                                          1 &nbsp;3 &nbsp;5
                                           ﹈ ﹈ ﹈
D.popAtStack(0);   // Returns 20.  The stacks are now:     4 21
             &nbsp;                                          1 &nbsp;3 &nbsp;5
                                                        ﹈ ﹈ ﹈
D.popAtStack(2);   // Returns 21.  The stacks are now:     4
             &nbsp;                                          1 &nbsp;3 &nbsp;5
                                                        ﹈ ﹈ ﹈ 
D.pop()            // Returns 5.  The stacks are now:      4
             &nbsp;                                          1 &nbsp;3 
                                                        ﹈ ﹈  
D.pop()            // Returns 4.  The stacks are now:   1 &nbsp;3 
                                                        ﹈ ﹈   
D.pop()            // Returns 3.  The stacks are now:   1 
                                                        ﹈   
D.pop()            // Returns 1.  There are no stacks.
D.pop()            // Returns -1.  There are still no stacks.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= capacity&nbsp;&lt;= 20000</code></li>
	<li><code>1 &lt;= val&nbsp;&lt;= 20000</code></li>
	<li><code>0 &lt;= index&nbsp;&lt;= 100000</code></li>
	<li>At most <code>200000</code>&nbsp;calls will be made to <code>push</code>, <code>pop</code>, and <code>popAtStack</code>.</li>
</ul>

</div>

## Tags
- Design (design)

## Companies
- ByteDance - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Python] Two Solutions
- Author: lee215
- Creation Date: Sun Aug 25 2019 12:02:59 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 14 2020 00:54:34 GMT+0800 (Singapore Standard Time)

<p>
## Solution 1
Use a list `stacks` to save all stacks.
Use a heap queue `q` to find the leftmost available stack.

push, `O(logN)` time
pop, amortized `O(1)` time
popAtStack, `O(logN)` time

**Python**
commented by @lichuan199010
```python
class DinnerPlates:
    def __init__(self, capacity):
        self.c = capacity
        self.q = [] # record the available stack, will use heap to quickly find the smallest available stack
        # if you are Java or C++ users, tree map is another good option.
        self.stacks = [] # record values of all stack of plates, its last nonempty stack are the rightmost position

    def push(self, val):
        # To push, we need to find the leftmost available position
        # first, let\'s remove any stacks on the left that are full
        # 1) self.q: if there is still available stack to insert plate
        # 2) self.q[0] < len(self.stacks): the leftmost available index self.q[0] is smaller than the current size of the stacks
        # 3) len(self.stacks[self.q[0]]) == self.c: the stack has reached full capacity
        while self.q and self.q[0] < len(self.stacks) and len(self.stacks[self.q[0]]) == self.c:
            # we remove the filled stack from the queue of available stacks
            heapq.heappop(self.q)

        # now we reach the leftmost available stack to insert

        # if the q is empty, meaning there are no more available stacks
        if not self.q:
            # open up a new stack to insert
            heapq.heappush(self.q, len(self.stacks))

        # for the newly added stack, add a new stack to self.stacks accordingly
        if self.q[0] == len(self.stacks):
            self.stacks.append([])

        # append the value to the leftmost available stack
        self.stacks[self.q[0]].append(val)

    def pop(self):
        # To pop, we need to find the rightmost nonempty stack
        # 1) stacks is not empty (self.stacks) and
        # 2) the last stack is empty
        while self.stacks and not self.stacks[-1]:
            # we throw away the last empty stack, because we can\'t pop from it
            self.stacks.pop()

        # now we reach the rightmost nonempty stack

        # we pop the plate from the last nonempty stack of self.stacks by using popAtStack function
        return self.popAtStack(len(self.stacks) - 1)

    def popAtStack(self, index):
        # To pop from an stack of given index, we need to make sure that it is not empty
        # 1) the index for inserting is valid and\uFF0C
        # 2) the stack of interest is not empty
        if 0 <= index < len(self.stacks) and self.stacks[index]:
            # we add the index into the available stack
            heapq.heappush(self.q, index)
            # take the top plate, pop it and return its value
            return self.stacks[index].pop()

        # otherwise, return -1 because we can\'t pop any plate
        return -1
```


## Solution 2
Use a map `m` to keep the mapping between index and stack
Use a set `available` to keep indices of all no full stacks.


**C++:**
```cpp
    int c;
    map<int, vector<int>> m;
    set<int> available;

    DinnerPlates(int capacity) {
        c = capacity;
    }

    void push(int val) {
        if (available.empty())
            available.insert(m.size());
        m[*available.begin()].push_back(val);
        if (m[*available.begin()].size() == c)
            available.erase(available.begin());
    }

    int pop() {
        while (m.size() && m.rbegin()->second.empty()) {
            m.erase(m.rbegin()->first);
        }
        if (m.empty()) return -1;
        return popAtStack(m.rbegin()->first);
    }

    int popAtStack(int index) {
        if (m[index].empty())
            return -1;
        int val = m[index].back();
        m[index].pop_back();
        available.insert(index);
        if (m[index].empty())
            m.erase(index);
        return val;
    }
```

</p>


### Java straightforward, HashMap + 2 Pointers
- Author: xiaogugu
- Creation Date: Sun Aug 25 2019 12:10:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 25 2019 12:38:30 GMT+0800 (Singapore Standard Time)

<p>
Primary idea is to maintain 2 pointers(indexes) to keep track of where to push or pop a element. And whenever push/pop is invoked, update the pointers accordingly. 

```
class DinnerPlates {

    Map<Integer, Stack<Integer>> map;
    int cap;
    int curr;
    int last;
    int count;
    
    public DinnerPlates(int capacity) {
        cap = capacity;
        curr = 0; //where to push element
        last = 0; //where to pop element
        count = 0; //number of elements
        map = new HashMap<>();
        map.put(curr, new Stack<>());
    }
    
    public void push(int val) {
        //do some preprocessing to update current index
        while(map.containsKey(curr) && map.get(curr).size()==cap){
            curr++;
        }
        if(!map.containsKey(curr)){
            map.put(curr, new Stack<>());
        }
        map.get(curr).push(val);
        last = Math.max(last, curr);
        count++;
    }
    
    public int pop() {
        if(count==0) return -1;
        while(last>=0 && map.get(last).isEmpty()){
            last--;
        }
        count--;
        curr=Math.min(curr, last);
        return map.get(last).pop();
    }
    
    public int popAtStack(int index) {
        if(!map.containsKey(index) || map.get(index).isEmpty()){
            return -1;
        }
        count--;
        curr=Math.min(curr, index);
        return map.get(index).pop();
    }
}
```
</p>


### Python, Use Heap to memorize the non-full stacks
- Author: davyjing
- Creation Date: Sun Aug 25 2019 12:18:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 25 2019 23:27:45 GMT+0800 (Singapore Standard Time)

<p>
Use a heap to record all the popAtStack- index.  Insert at these locations first. If the heap is empty, (that means all the stacks except the last one are full) then insert at the end of the stacks.
```
class DinnerPlates:

    def __init__(self, capacity: int):
        self.queue = []
        self.c = capacity
        self.emp = [] #non-full stacks, if the same index appears twice, that means it has two empty positions in this stack.

    def push(self, val: int) -> None:
        print(self.queue,self.emp)
        if self.emp:
            l = heapq.heappop(self.emp)
            if l < len(self.queue): #in some cases, the pop is called too many times, and the queue is shorter than the index
                self.queue[l] += [val]
                return
            else: self.emp = []
        if len(self.queue)>0 and len(self.queue[-1]) < self.c:
            self.queue[-1] += [val]
            return
        self.queue += [[val]]
        #print(self.queue)

    def pop(self) -> int:
        while len(self.queue) > 0 and not self.queue[-1]:
            self.queue.pop()
        if self.queue:
            res = self.queue[-1][-1]
            self.queue[-1].pop()
            return res
        return -1

    def popAtStack(self, index: int) -> int:
        if index < len(self.queue) and len(self.queue[index]) > 0:
            res = self.queue[index][-1]
            self.queue[index].pop()
            heapq.heappush(self.emp,index)
            return res
        return -1
```
</p>


