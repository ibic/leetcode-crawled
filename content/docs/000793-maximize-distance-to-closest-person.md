---
title: "Maximize Distance to Closest Person"
weight: 793
#id: "maximize-distance-to-closest-person"
---
## Description
<div class="description">
<p>You are given an array representing a row of <code>seats</code> where <code>seats[i] = 1</code> represents a person sitting in the <code>i<sup>th</sup></code> seat, and <code>seats[i] = 0</code> represents that the <code>i<sup>th</sup></code> seat is empty <strong>(0-indexed)</strong>.</p>

<p>There is at least one empty seat, and at least one person sitting.</p>

<p>Alex wants to sit in the seat such that the distance between him and the closest person to him is maximized.&nbsp;</p>

<p>Return <em>that maximum distance to the closest person</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/10/distance.jpg" style="width: 650px; height: 257px;" />
<pre>
<strong>Input:</strong> seats = [1,0,0,0,1,0,1]
<strong>Output:</strong> 2
<strong>Explanation: </strong>
If Alex sits in the second open seat (i.e. seats[2]), then the closest person has distance 2.
If Alex sits in any other open seat, the closest person has distance 1.
Thus, the maximum distance to the closest person is 2.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> seats = [1,0,0,0]
<strong>Output:</strong> 3
<strong>Explanation: </strong>
If Alex sits in the last seat (i.e. seats[3]), the closest person is 3 seats away.
This is the maximum distance possible, so the answer is 3.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> seats = [0,1]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= seats.length &lt;= 2 * 10<sup>4</sup></code></li>
	<li><code>seats[i]</code>&nbsp;is <code>0</code> or&nbsp;<code>1</code>.</li>
	<li>At least one seat is <strong>empty</strong>.</li>
	<li>At least one seat is <strong>occupied</strong>.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Audible - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Google - 18 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Next Array [Accepted]

**Intuition**

Let `left[i]` be the distance from seat `i` to the closest person sitting to the left of `i`.  Similarly, let `right[i]` be the distance to the closest person sitting to the right of `i`.  This is motivated by the idea that the closest person in seat `i` sits a distance `min(left[i], right[i])` away.

**Algorithm**

To construct `left[i]`, notice it is either `left[i-1] + 1` if the seat is empty, or `0` if it is full.  `right[i]` is constructed in a similar way.

<iframe src="https://leetcode.com/playground/VdzbADFi/shared" frameBorder="0" width="100%" height="480" name="VdzbADFi"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `seats`.

* Space Complexity:  $$O(N)$$, the space used by `left` and `right`.


---
#### Approach #2: Two Pointer [Accepted]

**Intuition**

As we iterate through seats, we'll update the closest person sitting to our left, and closest person sitting to our right.

**Algorithm**

Keep track of `prev`, the filled seat at or to the left of `i`, and `future`, the filled seat at or to the right of `i`.

Then at seat `i`, the closest person is `min(i - prev, future - i)`, with one exception.  `i - prev` should be considered infinite if there is no person to the left of seat `i`, and similarly `future - i` is infinite if there is no one to the right of seat `i`.

<iframe src="https://leetcode.com/playground/Bz4fKrBR/shared" frameBorder="0" width="100%" height="429" name="Bz4fKrBR"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `seats`.

* Space Complexity:  $$O(1)$$.



---
#### Approach #3: Group by Zero [Accepted]

**Intuition**

In a group of `K` adjacent empty seats between two people, the answer is `(K+1) / 2`.

**Algorithm**

For each group of `K` empty seats between two people, we can take into account the candidate answer `(K+1) / 2`.

For groups of empty seats between the edge of the row and one other person, the answer is `K`, and we should take into account those answers too.

<iframe src="https://leetcode.com/playground/oeWGx2at/shared" frameBorder="0" width="100%" height="500" name="oeWGx2at"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `seats`.

* Space Complexity:  $$O(1)$$.  (In Python, `seats[::-1]` uses $$O(N)$$ space, but this can be remedied.)

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] One pass, Easy Understood
- Author: lee215
- Creation Date: Sun Jun 10 2018 11:05:01 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 02 2019 10:51:31 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
`last` is the index of last seated seat.
Loop on all seats, when we met a people, we count the distance from the `last`.
The final result = max(distance at the beginning, distance in the middle / 2, distance at the end).

## **Explanation**
Time O(N) Space O(1)

**Java:**
```java
    public int maxDistToClosest(int[] seats) {
        int res = 0, n = seats.length, last = -1;
        for (int i = 0; i < n; ++i) {
            if (seats[i] == 1) {
                res = last < 0 ? i : Math.max(res, (i - last) / 2);
                last = i;
            }
        }
        res = Math.max(res, n - last - 1);
        return res;
    }
```

**C++:**
```cpp
    int maxDistToClosest(vector<int> seats) {
        int res = 0, n = seats.size(), last = -1;
        for (int i = 0; i < n; ++i) {
            if (seats[i] == 1) {
                res = last < 0 ? i : max(res, (i - last) / 2);
                last = i;
            }
        }
        res = max(res, n - last - 1);
        return res;
    }
```

**Python:**
```python
    def maxDistToClosest(self, seats):
        res, last, n = 0, -1, len(seats)
        for i in range(n):
            if seats[i]:
                res = max(res, i if last < 0 else (i - last) / 2)
                last = i
        return max(res, n - last - 1)
```
</p>


### Clean / One Pass / Two Pointers / Java Solution
- Author: FLAGbigoffer
- Creation Date: Wed Aug 01 2018 11:48:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 02:34:14 GMT+0800 (Singapore Standard Time)

<p>
Idea is simple. Use two pointers. 
1. If the current value is "0", keep going forward.
2. Left pointer points to the position of left "1" and right pointer points to the position of right "1". Keep updating two pointers and calculate the max distance.
2. Be careful of two situations: seats[0] is 0 and seats[len - 1] is 0. Just check them and get the final answer. Ex: 00101000.

```
class Solution {
    public int maxDistToClosest(int[] seats) {
        int left = -1, maxDis = 0;
        int len = seats.length;
        
        for (int i = 0; i < len; i++) {
            if (seats[i] == 0) continue;

            if (left == -1) {
                maxDis = Math.max(maxDis, i);
            } else {
                maxDis = Math.max(maxDis, (i - left) / 2);
            }
            left = i;
        }
        
        if (seats[len - 1] == 0) {
            maxDis = Math.max(maxDis, len - 1 - left);
        }
        
        return maxDis;
    }
}
```
</p>


### java one pass easy understand
- Author: iamwds
- Creation Date: Sun Jun 10 2018 12:48:37 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 13:28:16 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {// 0 0 0 1
    public int maxDistToClosest(int[] nums) {
        int n = nums.length;
        int max = 0;
        int i = 0;
        while(i < n){
            while(i < n && nums[i] == 1){
                i++;
            }
            int j = i;//start
            while(i < n && nums[i] == 0){
                i++;
            }
            if(j == 0 || i == n){
                max = Math.max(max, i - j);
            }else{
                max = Math.max(max, (i - j + 1) / 2) ;
            }
        }
        return max;
    }
}
```
</p>


