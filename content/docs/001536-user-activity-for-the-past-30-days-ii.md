---
title: "User Activity for the Past 30 Days II"
weight: 1536
#id: "user-activity-for-the-past-30-days-ii"
---
## Description
<div class="description">
<p>Table: <code>Activity</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| user_id       | int     |
| session_id    | int     |
| activity_date | date    |
| activity_type | enum    |
+---------------+---------+
There is no primary key for this table, it may have duplicate rows.
The activity_type column is an ENUM of type (&#39;open_session&#39;, &#39;end_session&#39;, &#39;scroll_down&#39;, &#39;send_message&#39;).
The table shows the user activities for a social media website.
Note that each session belongs to exactly one user.</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the average number of sessions per user for a period of 30 days ending <strong>2019-07-27</strong>&nbsp;inclusively, <strong>rounded to 2 decimal places</strong>. The sessions we want to count for a user are those with at least one activity in that time period.</p>

<p>The query result format is in the following example:</p>

<pre>
Activity table:
+---------+------------+---------------+---------------+
| user_id | session_id | activity_date | activity_type |
+---------+------------+---------------+---------------+
| 1       | 1          | 2019-07-20    | open_session  |
| 1       | 1          | 2019-07-20    | scroll_down   |
| 1       | 1          | 2019-07-20    | end_session   |
| 2       | 4          | 2019-07-20    | open_session  |
| 2       | 4          | 2019-07-21    | send_message  |
| 2       | 4          | 2019-07-21    | end_session   |
| 3       | 2          | 2019-07-21    | open_session  |
| 3       | 2          | 2019-07-21    | send_message  |
| 3       | 2          | 2019-07-21    | end_session   |
| 3       | 5          | 2019-07-21    | open_session  |
| 3       | 5          | 2019-07-21    | scroll_down   |
| 3       | 5          | 2019-07-21    | end_session   |
| 4       | 3          | 2019-06-25    | open_session  |
| 4       | 3          | 2019-06-25    | end_session   |
+---------+------------+---------------+---------------+

Result table:
+---------------------------+ 
| average_sessions_per_user |
+---------------------------+ 
| 1.33                      |
+---------------------------+ 
User 1 and 2 each had 1 session in the past 30 days while user 3 had 2 sessions so the average is (1 + 1 + 2) / 3 = 1.33.</pre>

</div>

## Tags


## Companies
- Facebook - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MYSQL Simple and readable, no Subqueries
- Author: buddyeorl
- Creation Date: Mon Aug 19 2019 12:21:33 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 19 2019 12:21:59 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT ifnull(ROUND(COUNT(DISTINCT session_id)/COUNT(DISTINCT user_id), 2),0.00) 
AS average_sessions_per_user
FROM Activity 
WHERE activity_date >= \'2019-06-28\' and activity_date <= \'2019-07-27\';  
```
</p>


### MySQL Solution (Sub query)
- Author: mbodke41
- Creation Date: Mon Sep 16 2019 08:48:01 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 16 2019 08:48:01 GMT+0800 (Singapore Standard Time)

<p>
```
select ifnull(round(sum(x)/count(user_id),2),0.00) as average_sessions_per_user 
from 
(
select user_id, count(distinct session_id) as x
from activity
where 
activity_date between date_sub(\'2019-07-27\',interval 29 day) and \'2019-07-27\'
group by user_id
) as t 
```
</p>


### MySQL answer
- Author: Brianpan_true
- Creation Date: Thu Jan 09 2020 09:16:54 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 09 2020 09:16:54 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT IFNULL(ROUND(COUNT(DISTINCT user_id,session_id)/COUNT(DISTINCT user_id),2),0) AS average_sessions_per_user
FROM Activity
WHERE activity_date BETWEEN \'2019-06-28\' AND \'2019-07-27\'
```
</p>


