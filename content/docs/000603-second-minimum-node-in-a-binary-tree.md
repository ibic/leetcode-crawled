---
title: "Second Minimum Node In a Binary Tree"
weight: 603
#id: "second-minimum-node-in-a-binary-tree"
---
## Description
<div class="description">
<p>Given a non-empty special binary tree consisting of nodes with the non-negative value, where each node in this tree has exactly <code>two</code> or <code>zero</code> sub-node. If the node has two sub-nodes, then this node&#39;s value is the smaller value among its two sub-nodes. More formally, the property&nbsp;<code>root.val = min(root.left.val, root.right.val)</code>&nbsp;always holds.</p>

<p>Given such a binary tree, you need to output the <b>second minimum</b> value in the set made of all the nodes&#39; value in the whole tree.</p>

<p>If no such second minimum value exists, output -1 instead.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> 
    2
   / \
  2   5
     / \
    5   7

<b>Output:</b> 5
<b>Explanation:</b> The smallest value is 2, the second smallest value is 5.
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> 
    2
   / \
  2   2

<b>Output:</b> -1
<b>Explanation:</b> The smallest value is 2, but there isn&#39;t any second smallest value.
</pre>

<p>&nbsp;</p>

</div>

## Tags
- Tree (tree)

## Companies
- LinkedIn - 7 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1: Brute Force [Accepted]

**Intuition and Algorithm**

Traverse the tree with a depth-first search, and record every unique value in the tree using a Set structure `uniques`.

Then, we'll look through the recorded values for the second minimum.  The first minimum must be $$\text{root.val}$$.

<iframe src="https://leetcode.com/playground/rVYM4qCQ/shared" frameBorder="0" name="rVYM4qCQ" width="100%" height="394"></iframe>


**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the total number of nodes in the given tree.  We visit each node exactly once, and scan through the $$O(N)$$ values in `unique` once.

* Space Complexity: $$O(N)$$, the information stored in `uniques`.

---
#### Approach #2: Ad-Hoc [Accepted]

**Intuition and Algorithm**

Let $$\text{min1 = root.val}$$.  When traversing the tree at some node, $$\text{node}$$, if $$\text{node.val > min1}$$, we know all values in the subtree at $$\text{node}$$ are at least $$\text{node.val}$$, so there cannot be a better candidate for the second minimum in this subtree.  Thus, we do not need to search this subtree.

Also, as we only care about the second minimum $$\text{ans}$$, we do not need to record any values that are larger than our current candidate for the second minimum, so unlike Approach #1 we can skip maintaining a Set of values(`uniques`) entirely.


<iframe src="https://leetcode.com/playground/btTLPkjK/shared" frameBorder="0" name="btTLPkjK" width="100%" height="394"></iframe>


**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the total number of nodes in the given tree.  We visit each node at most once.

* Space Complexity: $$O(N)$$.  The information stored in $$\text{ans}$$ and $$\text{min1}$$ is $$O(1)$$, but our depth-first search may store up to $$O(h) = O(N)$$ information in the call stack, where $$h$$ is the height of the tree.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java divide and conquer solution
- Author: tuo
- Creation Date: Mon Sep 04 2017 21:35:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 02:25:56 GMT+0800 (Singapore Standard Time)

<p>
for left and right sub-node, if their value is the same with the parent node value, need to using recursion to find the next candidate, otherwise use their node value as the candidate.
```  
public int findSecondMinimumValue(TreeNode root) {
    if (root == null) {
        return -1;
    }
    if (root.left == null && root.right == null) {
        return -1;
    }
    
    int left = root.left.val;
    int right = root.right.val;
    
    // if value same as root value, need to find the next candidate
    if (root.left.val == root.val) {
        left = findSecondMinimumValue(root.left);
    }
    if (root.right.val == root.val) {
        right = findSecondMinimumValue(root.right);
    }
    
    if (left != -1 && right != -1) {
        return Math.min(left, right);
    } else if (left != -1) {
        return left;
    } else {
        return right;
    }
}
```
</p>


### Java 4 lines
- Author: octavila
- Creation Date: Mon Sep 04 2017 11:45:50 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 15:02:54 GMT+0800 (Singapore Standard Time)

<p>
```
public int findSecondMinimumValue(TreeNode root) {
        if(root.left == null) return -1;
        
        int l = root.left.val == root.val ? findSecondMinimumValue(root.left) : root.left.val;
        int r = root.right.val == root.val ? findSecondMinimumValue(root.right) : root.right.val;
        
        return l == -1 || r == -1 ? Math.max(l, r) : Math.min(l, r);
    }
```
</p>


### Python Extremely Easy To Understand (Beats 91%)
- Author: yangshun
- Creation Date: Sun Sep 03 2017 12:36:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 13:49:53 GMT+0800 (Singapore Standard Time)

<p>
Based on the special property of the tree, we can guarantee that the root node is the smallest node in the tree. We just have to recursively traverse the tree and find a node that is bigger than the root node but smaller than any existing node we have come across.

*- Yangshun*

```
class Solution(object):
    def findSecondMinimumValue(self, root):
        res = [float('inf')]
        def traverse(node):
            if not node:
                return
            if root.val < node.val < res[0]:
                res[0] = node.val
            traverse(node.left)
            traverse(node.right)
        traverse(root)
        return -1 if res[0] == float('inf') else res[0]
```
</p>


