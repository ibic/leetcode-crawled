---
title: "License Key Formatting"
weight: 457
#id: "license-key-formatting"
---
## Description
<div class="description">
<p>You are given a license key represented as a string S which consists only alphanumeric character and dashes. The string is separated into N+1 groups by N dashes.</p>

<p>Given a number K, we would want to reformat the strings such that each group contains <i>exactly</i> K characters, except for the first group which could be shorter than K, but still must contain at least one character. Furthermore, there must be a dash inserted between two groups and all lowercase letters should be converted to uppercase.</p>

<p>Given a non-empty string S and a number K, format the string according to the rules described above.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> S = "5F3Z-2e-9-w", K = 4

<b>Output:</b> "5F3Z-2E9W"

<b>Explanation:</b> The string S has been split into two parts, each part has 4 characters.
Note that the two extra dashes are not needed and can be removed.
</pre>
</p>


<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> S = "2-5g-3-J", K = 2

<b>Output:</b> "2-5G-3J"

<b>Explanation:</b> The string S has been split into three parts, each part has 2 characters except the first part as it could be shorter as mentioned above.
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The length of string S will not exceed 12,000, and K is a positive integer.</li>
<li>String S consists only of alphanumerical characters (a-z and/or A-Z and/or 0-9) and dashes(-).</li>
<li>String S is non-empty.</li>
</ol>
</p>
</div>

## Tags


## Companies
- Capital One - 3 (taggedByAdmin: false)
- Google - 6 (taggedByAdmin: true)
- Twitter - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (java)
```java
public class Solution {
    public String licenseKeyFormatting(String S, int K) {
        String clean = S.toUpperCase().replaceAll("\\-", "");
        if (clean.isEmpty()) {
            return "";
        }
        int firstLen = clean.length() % K;
        StringBuilder sb = new StringBuilder();
        if (firstLen != 0) {
            sb.append(clean.substring(0, firstLen));
            sb.append("-");
        }
        for (int i = firstLen; i < clean.length(); i += K) {
            sb.append(clean.substring(i, i + K));
            sb.append("-");
        }
        sb.delete(sb.length() - 1, sb.length());
        return sb.toString();
    }
}

```

## Top Discussions
### Java 5 lines clean solution
- Author: yuxiangmusic
- Creation Date: Wed Jan 11 2017 14:42:41 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 11:03:03 GMT+0800 (Singapore Standard Time)

<p>
```
    public String licenseKeyFormatting(String s, int k) {
        StringBuilder sb = new StringBuilder();
        for (int i = s.length() - 1; i >= 0; i--)
            if (s.charAt(i) != '-')
                sb.append(sb.length() % (k + 1) == k ? '-' : "").append(s.charAt(i));
        return sb.reverse().toString().toUpperCase();
    } 
```
</p>


### beats 100% python3 submission
- Author: shibasisp
- Creation Date: Fri May 18 2018 14:52:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 00:58:28 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def licenseKeyFormatting(self, S, K):
        """
        :type S: str
        :type K: int
        :rtype: str
        """
        S = S.replace("-", "").upper()[::-1]
        return \'-\'.join(S[i:i+K] for i in range(0, len(S), K))[::-1]
```
</p>


### Python solution
- Author: yujiao03@gmail.com
- Creation Date: Tue Jan 10 2017 23:31:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 29 2018 20:06:34 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def licenseKeyFormatting(self, S, K):
        """
        :type S: str
        :type K: int
        :rtype: str
        """
        S = S.upper().replace('-','')
        size = len(S)
        s1 = K if size%K==0 else size%K
        res = S[:s1]
        while s1<size:
            res += '-'+S[s1:s1+K]
            s1 += K
        return res
```
</p>


