---
title: "Remove Duplicates from Sorted Array II"
weight: 80
#id: "remove-duplicates-from-sorted-array-ii"
---
## Description
<div class="description">
<p>Given a sorted array <em>nums</em>, remove the duplicates <a href="https://en.wikipedia.org/wiki/In-place_algorithm" target="_blank"><strong>in-place</strong></a> such that duplicates appeared at most&nbsp;<em>twice</em> and return the new length.</p>

<p>Do not allocate extra space for another array, you must do this by <strong>modifying the input array <a href="https://en.wikipedia.org/wiki/In-place_algorithm" target="_blank">in-place</a></strong> with O(1) extra memory.</p>

<p><strong>Example 1:</strong></p>

<pre>
Given <em>nums</em> = <strong>[1,1,1,2,2,3]</strong>,

Your function should return length = <strong><code>5</code></strong>, with the first five elements of <em><code>nums</code></em> being <strong><code>1, 1, 2, 2</code></strong> and <strong>3</strong> respectively.

It doesn&#39;t matter what you leave beyond the returned length.</pre>

<p><strong>Example 2:</strong></p>

<pre>
Given <em>nums</em> = <strong>[0,0,1,1,1,1,2,3,3]</strong>,

Your function should return length = <strong><code>7</code></strong>, with the first seven elements of <em><code>nums</code></em> being modified to&nbsp;<strong><code>0</code></strong>, <strong>0</strong>, <strong>1</strong>, <strong>1</strong>, <strong>2</strong>, <strong>3</strong> and&nbsp;<strong>3</strong> respectively.

It doesn&#39;t matter what values are set beyond&nbsp;the returned length.
</pre>

<p><strong>Clarification:</strong></p>

<p>Confused why the returned value is an integer but your answer is an array?</p>

<p>Note that the input array is passed in by <strong>reference</strong>, which means modification to the input array will be known to the caller as well.</p>

<p>Internally you can think of this:</p>

<pre>
// <strong>nums</strong> is passed in by reference. (i.e., without making a copy)
int len = removeDuplicates(nums);

// any modification to <strong>nums</strong> in your function would be known by the caller.
// using the length returned by your function, it prints the first <strong>len</strong> elements.
for (int i = 0; i &lt; len; i++) {
&nbsp; &nbsp; print(nums[i]);
}
</pre>

</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)

## Companies
- VMware - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

#### Approach 1: Popping Unwanted Duplicates

**Intuition**

The input array is already sorted and hence, all the duplicates appear next to each other. The problem statement mentions that we are not allowed to use any additional space and we have to modify the array in-place. The easiest approach for in-place modifications would be to get rid of all the unwanted duplicates. For every number in the array, if we detect `> 2` duplicates, we simply remove them from the list of elements and we do this for all the elements in the array.

<center>
<img src="../Figures/80/img1.png" width="600"/>
</center>

**Algorithm**

1. The implementation is slightly tricky so to say since we will be removing elements from the array and iterating over it at the same time. So, we need to keep updating the array's indexes as and when we pop an element else we'll be accessing invalid indexes.
2. Say we have two variables, `i` which is the array pointer and `count` which keeps track of the count of a particular element in the array. Note that the minimum count would always be 1. 

    <center>
    <img src="../Figures/80/img2.png" width="600"/>
    </center>

3. We start with index `1` and process one element at a time in the array.
4. If we find that the current element is the *same* as the previous element i.e. `nums[i] == nums[i - 1]`, then we increment the `count`. If the value of `count > 2`, then we have encountered an unwanted duplicate element and we can remove it from the array. Since we know the index of this element, we can use the `del` or `pop` or `remove` operation (or whatever corresponding operation is supported in your language of choice) to delete the element at index `i` from the array. Since we popped an element, we decrement the index by 1 as well.

    <center>
    <img src="../Figures/80/img3.png" width="600"/>
    </center>


5. If we encounter that the current element is *not* the same as the previous element i.e. `nums[i] != nums[i - 1]`, then it means we have a new element at hand and so accordingly, we update `count = 1`.

    <center>
    <img src="../Figures/80/img4.png" width="600"/>
    </center>

6. Since we are removing all the unwanted duplicates from the original array, the final array that remains after process all the elements will only contain the valid elements and hence we simply return the length of this array.

<iframe src="https://leetcode.com/playground/zTxSEYe9/shared" frameBorder="0" width="100%" height="500" name="zTxSEYe9"></iframe>

**Complexity Analysis**

* Time Complexity: Let's see what the costly operations in our array are:
    - We have to iterate over all the elements in the array. Suppose that the original array contains `N` elements, the time taken here would be $$O(N)$$.
    - Next, for every unwanted duplicate element, we will have to perform a delete operation and deletions in arrays are also $$O(N)$$.
    - The worst case would be when all the elements in the array are the same. In that case, we would be performing $$N - 2$$ deletions thus giving us $$O(N^2)$$ complexity for deletions
    - Overall complexity = $$O(N) + O(N^2) \equiv O(N^2)$$.
* Space Complexity: $$O(1)$$ since we are modifying the array in-place. 
<br>
<br>

---
#### Approach 2: Overwriting unwanted duplicates

**Intuition**

The second approach is really inspired by the fact that the problem statement asks us to return the *new length of the array* from the function. If all we had to do was *remove elements*, the function would not really ask us to return the updated length. However, in our scenario, this is really an indication that we don't need to actually remove elements from the array. Instead, we can do something better and simply overwrite the duplicate elements that are unwanted. 

> We won't be able to achieve this using a single pointer. We will be using a two-pointer approach where one pointer iterates over the original set of elements and another one that keeps track of the next "empty" location in the array or the next location that can be overwritten in the array.

**Algorithm**

1. We define two pointers, `i` and `j` for our algorithm. The pointer `i` iterates of the array processing one element at a time and `j` keeps track of the next location in the array where we can overwrite an element. 
2. We also keep a variable `count` which keeps track of the count of a particular element in the array. Note that the minimum count would always be 1. 
3. We start with index `1` and process one element at a time in the array.
4. If we find that the current element is the *same* as the previous element i.e. `nums[i] == nums[i - 1]`, then we increment the `count`. If the value of `count > 2`, then we have encountered an unwanted duplicate element. In this case, we simply move forward i.e. we increment `i` but not `j`.
5. However, if the count is `<= 2`, then we can move the element from index `i` to index `j`. 

    <center>
    <img src="../Figures/80/img5.png" width="600"/>
    </center>

6. If we encounter that the current element is *not* the same as the previous element i.e. `nums[i] != nums[i - 1]`, then it means we have a new element at hand and so accordingly, we update `count = 1` and also move this element to index `j`.

    <center>
    <img src="../Figures/80/img6.png" width="600"/>
    </center>

7. It goes without saying that whenever we copy a new element to `nums[j]`, we have to update the value of `j` as well since `j` always points to the location where the next element can be copied to in the array.

    <center>
    <img src="../Figures/80/img7.png" width="600"/>
    </center>

<iframe src="https://leetcode.com/playground/6DLdQmfK/shared" frameBorder="0" width="100%" height="500" name="6DLdQmfK"></iframe>
        
**Complexity Analysis**

* Time Complexity: $$O(N)$$ since we process each element exactly once.
* Space Complexity: $$O(1)$$.
<br>
<br>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 3-6 easy lines, C++, Java, Python, Ruby
- Author: StefanPochmann
- Creation Date: Fri Jun 26 2015 20:15:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 01:53:20 GMT+0800 (Singapore Standard Time)

<p>
Same simple solution written in several languages. Just go through the numbers and include those in the result that haven't been included twice already.

**C++**

    int removeDuplicates(vector<int>& nums) {
        int i = 0;
        for (int n : nums)
            if (i < 2 || n > nums[i-2])
                nums[i++] = n;
        return i;
    }

**Java**

    public int removeDuplicates(int[] nums) {
        int i = 0;
        for (int n : nums)
            if (i < 2 || n > nums[i-2])
                nums[i++] = n;
        return i;
    }

**Python**

    def removeDuplicates(self, nums):
        i = 0
        for n in nums:
            if i < 2 or n > nums[i-2]:
                nums[i] = n
                i += 1
        return i

**Ruby**

    def remove_duplicates(nums)
        i = 0
        nums.each { |n| nums[(i+=1)-1] = n if i < 2 || n > nums[i-2] }
        i
    end
</p>


### Short and Simple Java solution (easy to understand)
- Author: issac3
- Creation Date: Fri May 27 2016 22:53:03 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 14:49:59 GMT+0800 (Singapore Standard Time)

<p>
Question wants us to return the length of new array after removing duplicates and that we don't care about what we leave beyond new length , hence we can use `i` to keep track of the position and update the array. 

 
----------


Remove Duplicates from Sorted Array(no duplicates) :

    public int removeDuplicates(int[] nums) {
        int i = 0;
        for(int n : nums)
            if(i < 1 || n > nums[i - 1]) 
                nums[i++] = n;
        return i;
    }


Remove Duplicates from Sorted Array II (allow duplicates up to 2):

    public int removeDuplicates(int[] nums) {
       int i = 0;
       for (int n : nums)
          if (i < 2 || n > nums[i - 2])
             nums[i++] = n;
       return i;
    }
</p>


### Share my O(N) time and O(1) solution when duplicates are allowed at most K times
- Author: tech-wonderland.net
- Creation Date: Wed Jan 21 2015 06:39:43 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 04:11:09 GMT+0800 (Singapore Standard Time)

<p>
I think both Remove Duplicates from Sorted Array I and II could be solved in a consistent and more general way by allowing the duplicates to appear k times (k = 1 for problem I and k = 2 for problem II). Here is my way: we need a count variable to keep how many times the duplicated element appears, if we encounter a different element, just set counter to 1, if we encounter a duplicated one, we need to check this count, if it is already k, then we need to skip it, otherwise, we can keep this element. The following is the implementation and can pass both OJ:

    int removeDuplicates(int A[], int n, int k) {
    
                if (n <= k) return n;
    
                int i = 1, j = 1;
                int cnt = 1;
                while (j < n) {
                    if (A[j] != A[j-1]) {
                        cnt = 1;
                        A[i++] = A[j];
                    }
                    else {
                        if (cnt < k) {
                            A[i++] = A[j];
                            cnt++;
                        }
                    }
                    ++j;
                }
                return i;
    }


For more details, you can also see this post: [LeetCode Remove Duplicates from Sorted Array I and II: O(N) Time and O(1) Space][1]


  [1]: http://tech-wonderland.net/blog/leetcode-remove-duplicates-from-sorted-array-i-and-ii.html
</p>


