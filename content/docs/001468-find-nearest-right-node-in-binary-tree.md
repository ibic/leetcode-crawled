---
title: "Find Nearest Right Node in Binary Tree"
weight: 1468
#id: "find-nearest-right-node-in-binary-tree"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a binary tree and a node <code>u</code> in the tree, return <em>the <strong>nearest</strong> node on the <strong>same level</strong> that is to the <strong>right</strong> of</em> <code>u</code><em>, or return</em> <code>null</code> <em>if </em><code>u</code> <em>is the rightmost node in its level</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/09/24/p3.png" style="width: 241px; height: 161px;" /></p>

<pre>
<strong>Input:</strong> root = [1,2,3,null,4,5,6], u = 4
<strong>Output:</strong> 5
<strong>Explanation: </strong>The nearest node on the same level to the right of node 4 is node 5.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/09/23/p2.png" style="width: 101px; height: 161px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [3,null,4,2], u = 2
<strong>Output:</strong> null
<strong>Explanation: </strong>There are no nodes to the right of 2.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [1], u = 1
<strong>Output:</strong> null
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root = [3,4,2,null,null,null,1], u = 4
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is in the range <code>[1, 10<sup>5</sup>]</code>.</li>
	<li><code>1 &lt;= Node.val &lt;= 10<sup>5</sup></code></li>
	<li>All values in the tree are <strong>distinct</strong>.</li>
	<li><code>u</code> is a node in the binary tree rooted at <code>root</code>.</li>
</ul>

</div>

## Tags
- Tree (tree)
- Breadth-first Search (breadth-first-search)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] 2 solutions - BFS and DFS
- Author: alanlzl
- Creation Date: Fri Oct 02 2020 09:34:40 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 02 2020 09:37:51 GMT+0800 (Singapore Standard Time)

<p>
We can use either BFS or DFS (preorder) to solve this problem.

Both have time complexity of `O(N)`. BFS has space complexity of `O(N)` while DFS has space complextiy of `O(height)`. The DFS solution can be further improved to `O(1)` space using Morris.

**BFS solution**
```Python
class Solution:
    def findNeartestRightNode(self, root: TreeNode, u: TreeNode) -> TreeNode:
        arr = [root]
        while arr:
            nxt_arr, is_found = [], False
            for cur in arr:
                if is_found:
                    return cur
                if cur == u:
                    is_found = True
                if cur.left:
                    nxt_arr.append(cur.left)
                if cur.right:
                    nxt_arr.append(cur.right)
            if is_found:
                return None
            arr = nxt_arr
        return None
```

**DFS solution**
```Python
class Solution:
    def findNeartestRightNode(self, root: TreeNode, u: TreeNode) -> TreeNode:
        lvl_found = ans = None
        
        def preorder(node, lvl):
            nonlocal lvl_found, ans
            if not node:
                return
            if node == u:
                lvl_found = lvl
            elif lvl == lvl_found and ans is None:
                ans = node
            else:
                preorder(node.left, lvl + 1)
                preorder(node.right, lvl + 1)
            
        preorder(root, 0)
        return ans
```
</p>


### Java Morris Preorder Time O(n) Space O(1)
- Author: REED_W
- Creation Date: Fri Oct 02 2020 09:01:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 02 2020 09:01:18 GMT+0800 (Singapore Standard Time)

<p>
```
    private TreeNode sln1(TreeNode root, TreeNode u){
        TreeNode curr =root;
        TreeNode res = null;
        int dep = 0;
        int targetDep = -1;
        
        while(curr!=null){
            
            if(curr.left == null){
                if(curr == u) targetDep = dep;
                else if(targetDep == dep) return curr;
                curr = curr.right;
                dep++;
            }else{
                TreeNode prev = curr.left;
                int delta = 1;
                while(prev.right != null && prev.right!=curr){
                    prev = prev.right;
                    delta++;
                }
                
                if(prev.right == curr){
                    prev.right = null;
                    curr = curr.right;
                    dep-=delta;
                }else{
                    prev.right = curr;
                    if(curr == u) targetDep = dep;
                    else if(targetDep == dep) return curr;
                    curr = curr.left;
                    dep++;
                }
            }
        }
        
        return res;
    }
</p>


### [Java] - 100%
- Author: philip2207
- Creation Date: Thu Oct 01 2020 16:31:48 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 01 2020 16:31:48 GMT+0800 (Singapore Standard Time)

<p>
```java
public TreeNode findNeartestRightNode(TreeNode root, TreeNode u) {
	Deque<TreeNode> queue = new ArrayDeque<>();
	queue.add(root);

	while (!queue.isEmpty()) {
		for (int i = queue.size(); i > 0; i--) {
			TreeNode node = queue.poll();

			if (node == u) {
				if (i == 1) {
					return null;
				}
				return queue.poll();
			}

			if (node.left != null) {
				queue.add(node.left);
			}
			if (node.right != null) {
				queue.add(node.right);
			}
		}
	}

	throw new IllegalStateException("Invalid state, could not find U within root");
}
```
</p>


