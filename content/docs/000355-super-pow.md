---
title: "Super Pow"
weight: 355
#id: "super-pow"
---
## Description
<div class="description">
<p>Your task is to calculate <code>a<sup>b</sup></code> mod <code>1337</code> where <code>a</code> is a positive integer and <code>b</code> is an extremely large positive integer given in the form of an array.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> a = 2, b = [3]
<strong>Output:</strong> 8
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> a = 2, b = [1,0]
<strong>Output:</strong> 1024
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> a = 1, b = [4,3,3,8,5,2]
<strong>Output:</strong> 1
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> a = 2147483647, b = [2,0,0]
<strong>Output:</strong> 1198
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= a &lt;= 2<sup>31</sup> - 1</code></li>
	<li><code>1 &lt;= b.length &lt;= 2000</code></li>
	<li><code>0 &lt;= b[i] &lt;= 9</code></li>
	<li><code>b</code> doesn&#39;t contain leading zeros.</li>
</ul>

</div>

## Tags
- Math (math)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ Clean and Short Solution
- Author: fentoyal
- Creation Date: Thu Jul 07 2016 23:34:17 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 22:21:34 GMT+0800 (Singapore Standard Time)

<p>
One knowledge:  ab % k = (a%k)(b%k)%k
Since the power here is an array, we'd better handle it digit by digit.
One observation:
a^1234567 % k = (a^1234560 % k) * (a^7 % k) % k = (a^123456 % k)^10 % k * (a^7 % k) % k
Looks complicated? Let me put it  other way:
Suppose f(a, b) calculates a^b % k; Then translate above formula to using f :
f(a,1234567) = f(a, 1234560) * f(a, 7) % k = f(f(a, 123456),10) * f(a,7)%k;
Implementation of this idea:
```
class Solution {
    const int base = 1337;
    int powmod(int a, int k) //a^k mod 1337 where 0 <= k <= 10
    {
        a %= base;
        int result = 1;
        for (int i = 0; i < k; ++i)
            result = (result * a) % base;
        return result;
    }
public:
    int superPow(int a, vector<int>& b) {
        if (b.empty()) return 1;
        int last_digit = b.back();
        b.pop_back();
        return powmod(superPow(a, b), 10) * powmod(a, last_digit) % base;
    }
};
```
**Note:** This approach is definitely not the fastest, but it is something one can quickly understand and write out when asked in an interview. 
And this approach is not using any built-in "pow" functions, I think this is also what the interviewer would expect you to do.
Hope it helps!
</p>


### Math solusion based on Euler's theorem, power called only ONCE, C++/Java/1-line-Python
- Author: ShuangquanLi
- Creation Date: Fri Jul 08 2016 16:32:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 07:38:04 GMT+0800 (Singapore Standard Time)

<p>
1337 only has two divisors 7 and 191 exclusive 1 and itself, so judge if `a` has a divisor of 7 or 191, and note that 7 and 191 are prime numbers, `phi` of them is itself - 1, then we can use the Euler's theorem, see it on wiki https://en.wikipedia.org/wiki/Euler%27s_theorem, it's just [Fermat's little theorem](https://en.wikipedia.org/wiki/Fermat%27s_little_theorem) if the mod `n` is prime.

see how 1140 is calculated out:
phi(1337) = phi(7) * phi(191) = 6 * 190 = 1140


**optimized solution update at 2016-7-12**
--------------------------------
Today, seeing @myanonymos 's comments, and I find several days ago I AC it just by fortunate coincidence, it's not the best solution. now I get a better idea.

(1)  Firstly,  if `a` has both divisor 7 and 191, that's `a % 1337 == 0`, answer is 0.
(2)  Then if `a` has neither divisor 7 nor 191, that's a and 1337 are coprime, so **a<sup>b</sup> % 1337 = a<sup>b % phi(1337) </sup> % 1337 = a<sup>b % 1140 </sup> % 1337**.

(3)  Finally,  `a` could has either divisor 7 or 191, that's similar.
Let it be 7 for example.
Let **a =  7<sup>n</sup>x**
and let **b = 1140p + q**, where **0 < q <= 1140**
then:

**a<sup>b</sup> % 1337**
**= ((7<sup>n</sup>x)<sup>b</sup>) % 1337**
**= (7<sup>nb</sup>x<sup>b</sup>) % 1337**
**= ( (7<sup>nb</sup> % 1337) * (x<sup>b</sup> % 1337) ) % 1337**
**= ( (7<sup>1140np + nq</sup> % 1337) * (x<sup>1140p + q</sup> % 1337) ) % 1337**

now note x and 1337 are coprime, so

**= ( (7<sup>1140np + nq</sup> % 1337) * (x<sup>q</sup> % 1337) ) % 1337**
**= ( 7 * (7<sup>1140np + nq - 1</sup> % 191) * (x<sup>q</sup> % 1337) ) % 1337**

note 7 and 191 are coprime too, and 1140 is a multiple of 190, where 190 = phi(191). What's more we should assure that q != 0, if b % 1140== 0, then let b = 1140. so

**= ( 7 * (7<sup>nq - 1</sup> % 191) * (x<sup>q</sup> % 1337) ) % 1337**
**= ( (7<sup>nq</sup> % 1337) * (x<sup>q</sup> % 1337) ) % 1337**
**= (7<sup>nq</sup>x<sup>q</sup>) % 1337**
**= ((7<sup>n</sup>x)<sup>q</sup>) % 1337**
**= (a<sup>q</sup>) % 1337**

now you see condition (2) and (3) can be merged as one solution, if you take care of when `b % 1440 == 0`, and let `b += 1140`. Actually (1) can be merged too, but not efficient.


new code:
C++:

    int superPow(int a, vector<int>& b) {
        if (a % 1337 == 0) return 0; // this line could also be removed
        int p = 0;
        for (int i : b) p = (p * 10 + i) % 1140;
        if (p == 0) p += 1140;
        return power(a, p, 1337);
    }
    int power(int x, int n, int mod) {
        int ret = 1;
        for (x %= mod; n; x = x * x % mod, n >>= 1) if (n & 1) ret = ret * x % mod;
        return ret;
    }

java:

    public int superPow(int a, int[] b) {
        if (a % 1337 == 0) return 0;
        int p = 0;
        for (int i : b) p = (p * 10 + i) % 1140;
        if (p == 0) p += 1440;
        return power(a, p, 1337);
    }
    public int power(int a, int n, int mod) {
        a %= mod;
        int ret = 1;
        while (n != 0) {
            if ((n & 1) != 0) ret = ret * a % mod;
            a = a * a % mod;
            n >>= 1;
        }
        return ret;
    }

Actually, if p == 0 or not, we can always let p += 1140, it doesn't matter.
one line python:

    def superPow(self, a, b):
        return 0 if a % 1337 == 0 else pow(a, reduce(lambda x, y: (x * 10 + y) % 1140, b) + 1140, 1337)

**will this be the best solution?**

p.s.
I have testcases that the system missed
574
[1,1,4,0]

764
[1,1,4,0]
in this case if I remove this line of code `if (p == 0) p += 1140;`, it will get wrong answer, but also can get AC on OJ.

and I found thar 574 * 574 % 1337 = 574, 764 * 764 % 1337 = 764, how interesting!
</p>


### What's the point of this kind of question?
- Author: coder2
- Creation Date: Sun Jul 24 2016 01:42:01 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 05:12:08 GMT+0800 (Singapore Standard Time)

<p>
Just to test whether the candidate knows https://en.wikipedia.org/wiki/Modular_exponentiation?

Which company has asked this question? Curious.
</p>


