---
title: "Delete N Nodes After M Nodes of a Linked List"
weight: 1370
#id: "delete-n-nodes-after-m-nodes-of-a-linked-list"
---
## Description
<div class="description">
<p>Given the&nbsp;<code>head</code>&nbsp;of a linked list and two integers <code>m</code> and <code>n</code>. Traverse the linked list and remove some nodes&nbsp;in the following way:</p>

<ul>
	<li>Start with the head as the current node.</li>
	<li>Keep the first <code>m</code> nodes starting with the current node.</li>
	<li>Remove the next <code>n</code> nodes</li>
	<li>Keep repeating steps 2 and 3 until you reach the end of the list.</li>
</ul>

<p>Return the head of the modified list after removing the mentioned nodes.</p>

<p><strong>Follow up question:</strong> How can you solve this problem by modifying the list in-place?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/06/06/sample_1_1848.png" style="width: 620px; height: 95px;" /></strong></p>

<pre>
<strong>Input:</strong> head = [1,2,3,4,5,6,7,8,9,10,11,12,13], m = 2, n = 3
<strong>Output:</strong> [1,2,6,7,11,12]
<strong>Explanation: </strong>Keep the first (m = 2) nodes starting from the head of the linked List  (1 -&gt;2) show in black nodes.
Delete the next (n = 3) nodes (3 -&gt; 4 -&gt; 5) show in read nodes.
Continue with the same procedure until reaching the tail of the Linked List.
Head of linked list after removing nodes is returned.</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/06/06/sample_2_1848.png" style="width: 620px; height: 123px;" /></strong></p>

<pre>
<strong>Input:</strong> head = [1,2,3,4,5,6,7,8,9,10,11], m = 1, n = 3
<strong>Output:</strong> [1,5,9]
<strong>Explanation:</strong> Head of linked list after removing nodes is returned.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> head = [1,2,3,4,5,6,7,8,9,10,11], m = 3, n = 1
<strong>Output:</strong> [1,2,3,5,6,7,9,10,11]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> head = [9,3,7,7,9,10,8,2], m = 1, n = 2
<strong>Output:</strong> [9,7,8]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The given linked list will contain between&nbsp;<code>1</code>&nbsp;and&nbsp;<code>10^4</code>&nbsp;nodes.</li>
	<li>The value of each node in the linked list will be in the range<code>&nbsp;[1, 10^6]</code>.</li>
	<li><code>1 &lt;= m,n &lt;=&nbsp;1000</code></li>
</ul>
</div>

## Tags
- Linked List (linked-list)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python One-Pass In-Place with O(n) time and O(1) space with explanation
- Author: gdkou90
- Creation Date: Thu Jun 11 2020 11:27:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 19 2020 02:47:19 GMT+0800 (Singapore Standard Time)

<p>
Methodology based on a dummy head:
1. Use an indicator ```i``` to count the number of already-passed list nodes
2. Keep moving head node forward as long as ```i < m-1``` 
3. Remove the next ```n``` nodes and reset ```i``` to 0 when ```i == m-1```
```
class Solution:
    def deleteNodes(self, head: ListNode, m: int, n: int) -> ListNode:
        dummy = ListNode(None)
        dummy.next = head
        i = 0
        while head:
            if i < m-1:
                i += 1
            else:
                j = 0
                while j < n and head.next:
                    head.next = head.next.next
                    j += 1
                i = 0
            head = head.next
        return dummy.next
```
</p>


### Java Easy and Concise
- Author: AsStone
- Creation Date: Mon Jun 15 2020 10:56:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 15 2020 10:56:03 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public ListNode deleteNodes(ListNode head, int m, int n) {
        ListNode cur = head, pre = null;    // pre.next == cur
        while (cur != null) {
            int i = m, j = n;
            
            // find the m-th node as pre
            while (cur != null && i-- > 0) {
                pre = cur;
                cur = cur.next;
            }
            
            while (cur != null && j-- > 0) {
                cur = cur.next;
            }
            pre.next = cur;     // delete n nodes
            
        }
        return head;
    }
}
```
</p>


### Easy to understand concise Java solution with minimized number of assignment operations
- Author: Jaltair
- Creation Date: Fri Jul 03 2020 20:43:16 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 13 2020 05:40:03 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public ListNode deleteNodes(ListNode head, int m, int n) {
        ListNode prev = null; // pointer to previous node
        ListNode cur = head; // pointer to current node
        while (cur != null) {
		    // skip up to m nodes
            for (int i = 0; i < m && cur != null; ++i) {
                prev = cur;
                cur = cur.next;
            }
			// remove up to n nodes
            for (int i = 0; i < n && cur != null; ++i) {
                cur = cur.next;
            }
            prev.next = cur;
        }
        return head;
    }
}
```
</p>


