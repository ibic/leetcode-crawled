---
title: "Last Person to Fit in the Elevator"
weight: 1547
#id: "last-person-to-fit-in-the-elevator"
---
## Description
<div class="description">
<p>Table: <code>Queue</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| person_id   | int     |
| person_name | varchar |
| weight      | int     |
| turn        | int     |
+-------------+---------+
person_id is the primary key column for this table.
This table has the information about all people waiting for an elevator.
The <code>person_id</code>&nbsp;and <code>turn</code> columns will contain all numbers from 1 to n, where n is the number of rows in the table.
</pre>

<p>&nbsp;</p>

<p>The maximum weight the elevator can hold is <strong>1000</strong>.</p>

<p>Write an SQL query to find the&nbsp;<code>person_name</code> of the last person who will fit in the elevator without exceeding the weight limit. It is guaranteed that the person who is&nbsp;first in the queue can fit in the elevator.</p>

<p>The query result format is in the following example:</p>

<pre>
Queue table
+-----------+-------------------+--------+------+
| person_id | person_name       | weight | turn |
+-----------+-------------------+--------+------+
| 5         | George Washington | 250    | 1    |
| 3         | John Adams        | 350    | 2    |
| 6         | Thomas Jefferson  | 400    | 3    |
| 2         | Will Johnliams    | 200    | 4    |
| 4         | Thomas Jefferson  | 175    | 5    |
| 1         | James Elephant    | 500    | 6    |
+-----------+-------------------+--------+------+

Result table
+-------------------+
| person_name       |
+-------------------+
| Thomas Jefferson  |
+-------------------+

Queue table is ordered by turn in the example for simplicity.
In the example George Washington(id 5), John Adams(id 3) and Thomas Jefferson(id 6) will enter the elevator as their weight sum is 250 + 350 + 400 = 1000.
Thomas Jefferson(id 6) is the last person to fit in the elevator because he has the last turn in these three people.
</pre>

</div>

## Tags


## Companies
- Wayfair - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL beat 100%, lol. Of course, since I'm the first one to do this problem.
- Author: LijianChen
- Creation Date: Wed Sep 25 2019 12:58:38 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 25 2019 12:59:40 GMT+0800 (Singapore Standard Time)

<p>
The steps:
(1) Get cumulative sum weight using ```Join``` with condition ```q1.turn >= q2.turn``` and ```Group By q1.turn```  
(2) Filter the groups with  cum sum ```<=1000``` 
(3) Order by cum sum with ```Desc``` order, select the 1st.
```
SELECT q1.person_name
FROM Queue q1 JOIN Queue q2 ON q1.turn >= q2.turn
GROUP BY q1.turn
HAVING SUM(q2.weight) <= 1000
ORDER BY SUM(q2.weight) DESC
LIMIT 1
```
</p>


### ms sql solution based on windows function
- Author: user7867e
- Creation Date: Tue Nov 19 2019 04:23:15 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 19 2019 04:23:15 GMT+0800 (Singapore Standard Time)

<p>
select top 1 sub.person_name
from 
(select person_name,
       SUM(weight) OVER (ORDER BY turn ASC) AS running_total
from Queue) sub
where sub.running_total <= 1000
order by sub.running_total DESC
</p>


### Simple MySQL solution beat 100%
- Author: eminem18753
- Creation Date: Wed Sep 25 2019 23:07:12 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 25 2019 23:07:12 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below
SELECT person_name FROM Queue as a
WHERE
(
    SELECT SUM(weight) FROM Queue as b
    WHERE b.turn<=a.turn
    ORDER By turn
)<=1000
ORDER BY a.turn DESC limit 1;
```
</p>


