---
title: "Digit Count in Range"
weight: 977
#id: "digit-count-in-range"
---
## Description
<div class="description">
Given an integer <code>d</code> between <code>0</code> and <code>9</code>, and two positive integers <code>low</code> and <code>high</code> as lower and upper bounds, respectively. Return the number of times that <code>d</code> occurs as a digit in all integers between <code>low</code> and <code>high</code>, including the bounds <code>low</code> and <code>high</code>.
<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>d = <span id="example-input-1-1">1</span>, low = <span id="example-input-1-2">1</span>, high = <span id="example-input-1-3">13</span>
<strong>Output: </strong><span id="example-output-1">6</span>
<strong>Explanation: </strong>
The digit <code>d=1</code> occurs <code>6</code> times in <code>1,10,11,12,13</code>. Note that the digit <code>d=1</code> occurs twice in the number <code>11</code>.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>d = <span id="example-input-2-1">3</span>, low = <span id="example-input-2-2">100</span>, high = <span id="example-input-2-3">250</span>
<strong>Output: </strong><span id="example-output-2">35</span>
<strong>Explanation: </strong>
The digit <code>d=3</code> occurs <code>35</code> times in <code>103,113,123,130,131,...,238,239,243</code>.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= d &lt;= 9</code></li>
	<li><code>1 &lt;= low &lt;= high &lt;= 2&times;10^8</code></li>
</ol>
</div>
</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- eBay - 2 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple iterative Java solution extending [number of digits one]
- Author: liyun1988
- Creation Date: Sun Jun 02 2019 02:37:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 06:06:37 GMT+0800 (Singapore Standard Time)

<p>
The solution is based on https://leetcode.com/articles/number-of-digit-one/. Huge Kudos to the origional author of that article. 

My solution extends that solution in two places.
1 when d > 0, the remainder should be larger than i * d instead of i;
2 when d == 0, when analyzing the remainder, we need avoid taking numbers with "heading zero" like 0xxxx into the total count.

```
class Solution {
    public int digitsCount(int d, int low, int high) {
        return countDigit(high, d) - countDigit(low-1, d);
    }
    
    int countDigit(int n, int d) {
        if(n < 0 || n < d) {
            return 0;
        }
        
        int count = 0;
        for(long i = 1; i <= n; i*= 10) {
            long divider = i * 10;
            count += (n / divider) * i;
            
            if (d > 0) {
                count += Math.min(Math.max(n % divider - d * i + 1, 0), i); // comment1: tailing number need to be large than d *  i to qualify.
            } else {
                if(n / divider > 0) {
                    if(i > 1) {  // comment2: when d == 0, we need avoid to take numbers like 0xxxx into account.
                        count -= i;
                        count += Math.min(n % divider + 1, i);  
                    }
                }
            }
        }
        
        return count;
    }
}
```
</p>


### [Python] O(logN)
- Author: lee215
- Creation Date: Sun Jun 02 2019 00:51:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 00:51:06 GMT+0800 (Singapore Standard Time)

<p>
**Python:**
```
    def digitsCount(self, d, low, high):
        def count(N):
            if N == 0: return 0
            if d == 0 and N <= 10: return 0
            res = 0
            if N % 10 > d: res += 1
            if N / 10 > 0: res += str(N / 10).count(str(d)) * (N % 10)
            res += N / 10 if d else N / 10 - 1
            res += count(N / 10) * 10
            return res
        return count(high + 1) - count(low)
```

</p>


### Java Very Concise log10(N) Solution
- Author: Seayahh
- Creation Date: Mon Aug 26 2019 11:40:38 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 26 2019 11:40:38 GMT+0800 (Singapore Standard Time)

<p>
```
public int digitsCount(int d, int low, int high) {
	return digitsCount(d, high) - digitsCount(d, low - 1);
}

private int digitsCount(int d, int n) {
	int cnt = 0;
	for (long m = 1; m <= n; m *= 10) {
		long a = n / m, b = n % m;
		cnt += (a + 9 - d) / 10 * m + (a % 10 == d ? b + 1 : 0);
		if (d == 0) cnt -= m;
	}

	return cnt;
}
```
</p>


