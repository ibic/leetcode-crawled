---
title: "Number of Paths with Max Score"
weight: 1111
#id: "number-of-paths-with-max-score"
---
## Description
<div class="description">
<p>You are given a square <code>board</code>&nbsp;of characters. You can move on the board starting at the bottom right square marked with the character&nbsp;<code>&#39;S&#39;</code>.</p>

<p>You need&nbsp;to reach the top left square marked with the character <code>&#39;E&#39;</code>. The rest of the squares are labeled either with a numeric character&nbsp;<code>1, 2, ..., 9</code> or with an obstacle <code>&#39;X&#39;</code>. In one move you can go up, left or up-left (diagonally) only if there is no obstacle there.</p>

<p>Return a list of two integers: the first integer is the maximum sum of numeric characters you can collect, and the second is the number of such paths that you can take to get that maximum sum, <strong>taken modulo <code>10^9 + 7</code></strong>.</p>

<p>In case there is no path, return&nbsp;<code>[0, 0]</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> board = ["E23","2X2","12S"]
<strong>Output:</strong> [7,1]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> board = ["E12","1X1","21S"]
<strong>Output:</strong> [4,2]
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> board = ["E11","XXX","11S"]
<strong>Output:</strong> [0,0]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= board.length == board[i].length &lt;= 100</code></li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Samsung - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] DP Solution
- Author: lee215
- Creation Date: Sun Dec 29 2019 00:14:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 29 2019 00:14:02 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
We create 3D `dp[x][y]`, where
`dp[x][y][0]` is the maximum value to this cell,
`dp[x][y][1]` is the number of paths.
<br>

## **Complexity**
Time `O(N^2)`
Space `O(N^2)`
<br>


**Python:**
```python
    def pathsWithMaxScore(self, A):
        n, mod = len(A), 10**9 + 7
        dp = [[[-10**5, 0] for j in xrange(n + 1)] for i in xrange(n + 1)]
        dp[n - 1][n - 1] = [0, 1]
        for x in range(n)[::-1]:
            for y in range(n)[::-1]:
                if A[x][y] in \'XS\': continue
                for i, j in [[0, 1], [1, 0], [1, 1]]:
                    if dp[x][y][0] < dp[x + i][y + j][0]:
                        dp[x][y] = [dp[x + i][y + j][0], 0]
                    if dp[x][y][0] == dp[x + i][y + j][0]:
                        dp[x][y][1] += dp[x + i][y + j][1]
                dp[x][y][0] += int(A[x][y]) if x or y else 0
        return [dp[0][0][0] if dp[0][0][1] else 0, dp[0][0][1] % mod]
```

</p>


### [Java] DP Solution - Clean code - O(m*n)
- Author: hiepit
- Creation Date: Sun Dec 29 2019 00:03:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 29 2019 22:10:25 GMT+0800 (Singapore Standard Time)

<p>
**Complexity**
- Time: `O(m*n)`, `m` is the number of rows, `n` is the number of columns of the `board`, (m, n <= 100)
- Space: `O(m*n)`

**Java**
```java
class Solution {
    private static final int[][] DIRS = new int[][]{{0, -1}, {-1, 0}, {-1, -1}};
    public int[] pathsWithMaxScore(List<String> board) {
        int m = board.size(), n = board.get(0).length();
        int[][] dpSum = new int[m][n];
        int[][] dpCnt = new int[m][n];
        dpCnt[m - 1][n - 1] = 1; // start at the bottom right square
        for (int r = m - 1; r >= 0; r--) {
            for (int c = n - 1; c >= 0; c--) {
                if (dpCnt[r][c] == 0) continue; // can\'t reach to this square
                for (int[] dir : DIRS) {
                    int nr = r + dir[0], nc = c + dir[1];
                    if (nr >= 0 && nc >= 0 && board.get(nr).charAt(nc) != \'X\') {
                        int nsum = dpSum[r][c];
                        if (board.get(nr).charAt(nc) != \'E\')
                            nsum += board.get(nr).charAt(nc) - \'0\';
                        if (nsum > dpSum[nr][nc]) {
                            dpCnt[nr][nc] = dpCnt[r][c];
                            dpSum[nr][nc] = nsum;
                        } else if (nsum == dpSum[nr][nc]) {
                            dpCnt[nr][nc] = (dpCnt[nr][nc] + dpCnt[r][c]) % 1000000007;
                        }
                    }
                }
            }
        }
        return new int[]{dpSum[0][0], dpCnt[0][0]};
    }
}
```
</p>


### C++ DP
- Author: votrubac
- Creation Date: Sun Dec 29 2019 06:01:32 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 14 2020 03:34:06 GMT+0800 (Singapore Standard Time)

<p>
Track the maximum score and paths for each cell from 3 directions:
- The starting cell has score `0` and `1` paths.
- If the current score for a cell is smaller, update it and **overwrite** the number of paths. 
- If the score in the same, **add** the number of paths.
```CPP
vector<vector<int>> dirs {{1, 0}, {0, 1}, {1, 1}};
vector<int> pathsWithMaxScore(vector<string>& board) {
    auto sz = board.size();
    vector<vector<int>> score(sz + 1, vector<int>(sz + 1)), paths(sz + 1, vector<int>(sz + 1));
    board[0][0] = board[sz - 1][sz - 1] = \'0\';
    paths[0][0] = 1;
    for (int i = 1; i <= sz; ++i) {
        for (int j = 1; j <= sz; ++j) {
            if (board[i - 1][j - 1] == \'X\') 
                continue;
            for (auto d : dirs) {
                auto i1 = i - d[0], j1 = j - d[1];
                auto val = score[i1][j1] + (board[i - 1][j - 1] - \'0\');
                if (score[i][j] <= val && paths[i1][j1] > 0) {
                    paths[i][j] = ((score[i][j] == val ? paths[i][j] : 0) + paths[i1][j1]) % 1000000007; 
                    score[i][j] = val;
                }
            }
        }
    }
    return {paths[sz][sz] ? score[sz][sz] : 0, paths[sz][sz]};
} 
```
</p>


