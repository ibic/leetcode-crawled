---
title: "Trapping Rain Water"
weight: 42
#id: "trapping-rain-water"
---
## Description
<div class="description">
<p>Given <em>n</em> non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap after raining.</p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/22/rainwatertrap.png" style="width: 412px; height: 161px;" /><br />
<small>The above elevation map is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In this case, 6 units of rain water (blue section) are being trapped. <strong>Thanks Marcos</strong> for contributing this image!</small></p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> [0,1,0,2,1,0,1,3,2,1,2,1]
<strong>Output:</strong> 6</pre>

</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)
- Stack (stack)

## Companies
- Goldman Sachs - 46 (taggedByAdmin: false)
- Amazon - 43 (taggedByAdmin: true)
- Facebook - 32 (taggedByAdmin: false)
- Bloomberg - 10 (taggedByAdmin: true)
- Apple - 10 (taggedByAdmin: true)
- Microsoft - 8 (taggedByAdmin: false)
- ByteDance - 7 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Citadel - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Walmart Labs - 2 (taggedByAdmin: false)
- Flipkart - 2 (taggedByAdmin: false)
- Databricks - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- Qualtrics - 5 (taggedByAdmin: false)
- Visa - 5 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Airbnb - 2 (taggedByAdmin: true)
- eBay - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- Audible - 2 (taggedByAdmin: false)
- Twitch - 2 (taggedByAdmin: false)
- Lyft - 14 (taggedByAdmin: false)
- Wish - 4 (taggedByAdmin: false)
- Palantir Technologies - 3 (taggedByAdmin: false)
- Dataminr - 2 (taggedByAdmin: false)
- Intel - 2 (taggedByAdmin: false)
- Affirm - 2 (taggedByAdmin: false)
- Huawei - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Electronic Arts - 2 (taggedByAdmin: false)
- Tableau - 2 (taggedByAdmin: false)
- Zenefits - 0 (taggedByAdmin: true)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Brute force

**Intuition**

Do as directed in question. For each element in the array, we find the maximum level of water it can trap after the rain, which is equal to the minimum of maximum height of bars on both the sides minus its own height.

**Algorithm**

* Initialize $$ans=0$$
* Iterate the array from left to right:
  + Initialize $$\text{left\_max}=0$$ and $$\text{right\_max}=0$$
  + Iterate from the current element to the beginning of array updating:
    * $$\text{left\_max}=\max(\text{left\_max},\text{height}[j])$$
  + Iterate from the current element to the end of array updating:
    * $$\text{right\_max}=\max(\text{right\_max},\text{height}[j])$$
  + Add $$\min(\text{left\_max},\text{right\_max}) - \text{height}[i]$$ to $$\text{ans}$$

<iframe src="https://leetcode.com/playground/n34EdmKB/shared" frameBorder="0" width="100%" height="0" name="n34EdmKB"></iframe>

**Complexity Analysis**

* Time complexity: $$O(n^2)$$. For each element of array, we iterate the left and right parts.

* Space complexity: $$O(1)$$ extra space.
<br />
<br />
---
#### Approach 2: Dynamic Programming

**Intuition**

In brute force, we iterate over the left and right parts again and again just to find the highest bar size upto that index. But, this could be stored. Voila, dynamic programming.

The concept is illustrated as shown:

![Dynamic programming](../Figures/42/trapping_rain_water.png){:width="500px"}
{:align="center"}

**Algorithm**

* Find maximum height of bar from the left end upto an index i in the array $$\text{left\_max}$$.
* Find maximum height of bar from the right end upto an index i in the array $$\text{right\_max}$$.
* Iterate over the $$\text{height}$$ array and update ans:
	+ Add $$\min(\text{left\_max}[i],\text{right\_max}[i]) - \text{height}[i]$$ to $$\text{ans}$$

<iframe src="https://leetcode.com/playground/TjV5y9eB/shared" frameBorder="0" width="100%" height="395" name="TjV5y9eB"></iframe>

**Complexity analysis**

* Time complexity: $$O(n)$$.
	+ We store the maximum heights upto a point using 2 iterations of $$O(n)$$ each.
	+ We finally update $$\text{ans}$$ using the stored values in $$O(n)$$.

* Space complexity: $$O(n)$$ extra space.
	+ Additional $$O(n)$$ space for $$\text{left\_max}$$ and $$\text{right\_max}$$ arrays than in [Approach 1](#approach-1-brute-force).
<br />
<br />
---
#### Approach 3: Using stacks

**Intuition**

Instead of storing the largest bar upto an index as in [Approach 2](#approach-2-dynamic-programming), we can use stack to keep track of the bars that are bounded by longer bars and hence, may store water. Using the stack, we can do the calculations in only one iteration.

We keep a stack and iterate over the array. We add the index of the bar to the stack if bar is smaller than or equal to the bar at top of stack, which means that the current bar is bounded by the previous bar in the stack. If we found a bar longer than that at the top, we are sure that the bar at the top of the stack is bounded by the current bar and a previous bar in the stack, hence, we can pop it and add resulting trapped water to $$\text{ans}$$.

**Algorithm**

* Use stack to store the indices of the bars.
* Iterate the array:
	+ While stack is not empty and $$\text{height[current]}>\text{height[st.top()]}$$
		* It means that the stack element can be popped. Pop the top element as $$\text{top}$$.
		* Find the distance between the current element and the element at top of stack, which is to be filled.
		$$\text{distance} = \text{current} - \text{st.top}() - 1$$
		* Find the bounded height
		$$\text{bounded\_height} = \min(\text{height[current]}, \text{height[st.top()]}) - \text{height[top]}$$
		* Add resulting trapped water to answer $$\text{ans} \mathrel{+}= \text{distance} \times \text{bounded\_height}$$
	+ Push current index to top of the stack
	+ Move $$\text{current}$$ to the next position


<iframe src="https://leetcode.com/playground/8QAzB3yt/shared" frameBorder="0" width="100%" height="361" name="8QAzB3yt"></iframe>

**Complexity analysis**

* Time complexity: $$O(n)$$.
	+ Single iteration of $$O(n)$$ in which each bar can be touched at most twice(due to  insertion and deletion from stack) and insertion and deletion from stack takes $$O(1)$$ time.
* Space complexity: $$O(n)$$. Stack can take upto $$O(n)$$ space in case of stairs-like or flat structure.
<br />
<br />

---
#### Approach 4: Using 2 pointers

**Intuition**

As in [Approach 2](#approach-2-dynamic-programming), instead of computing the left and right parts seperately, we may think of some way to do it in one iteration.
From the figure in dynamic programming approach, notice that as long as $$\text{right\_max}[i]>\text{left\_max}[i]$$ (from element 0 to 6), the water trapped depends upon the left_max, and similar is the case when $$\text{left\_max}[i]>\text{right\_max}[i]$$ (from element 8 to 11).
So, we can say that if there is a larger bar at one end (say right), we are assured that the water trapped would be dependant on height of bar in current direction (from left to right). As soon as we find the bar at other end (right) is smaller, we start iterating in opposite direction (from right to left).
We must maintain $$\text{left\_max}$$ and $$\text{right\_max}$$ during the iteration, but now we can do it in one iteration using 2 pointers, switching between the two.

**Algorithm**

* Initialize $$\text{left}$$ pointer to 0 and $$\text{right}$$ pointer to size-1
* While $$\text{left}< \text{right}$$, do:
	+ If $$\text{height[left]}$$ is smaller than $$\text{height[right]}$$
		* If $$\text{height[left]} \geq \text{left\_max}$$, update $$\text{left\_max}$$
		* Else add $$\text{left\_max}-\text{height[left]}$$ to $$\text{ans}$$
		* Add 1 to $$\text{left}$$.
	+ Else
		* If $$\text{height[right]} \geq \text{right\_max}$$, update $$\text{right\_max}$$
		* Else add $$\text{right\_max}-\text{height[right]}$$ to $$\text{ans}$$
		* Subtract 1 from $$\text{right}$$.

Refer the example for better understanding:
!?!../Documents/42/42_trapping_rain_water.json:1000,662!?!

<iframe src="https://leetcode.com/playground/Q6RYdFA5/shared" frameBorder="0" width="100%" height="344" name="Q6RYdFA5"></iframe>

**Complexity analysis**

* Time complexity: $$O(n)$$. Single iteration of $$O(n)$$.
* Space complexity: $$O(1)$$ extra space. Only constant space required for $$\text{left}$$, $$\text{right}$$, $$\text{left\_max}$$ and $$\text{right\_max}$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Sharing my simple c++ code: O(n) time, O(1) space
- Author: mcrystal
- Creation Date: Sat Nov 15 2014 04:03:29 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 07:14:47 GMT+0800 (Singapore Standard Time)

<p>
    
Here is my idea: instead of calculating area by height*width, we can think it in a cumulative way. In other words, sum water amount of each bin(width=1). 
Search from left to right and maintain a max height of left and right separately, which is like a one-side wall of  partial container. Fix the higher one and flow water from the lower part. For example, if current height of left is lower, we fill water in the left bin. Until left meets right, we filled the whole container.

    class Solution {
    public:
        int trap(int A[], int n) {
            int left=0; int right=n-1;
            int res=0;
            int maxleft=0, maxright=0;
            while(left<=right){
                if(A[left]<=A[right]){
                    if(A[left]>=maxleft) maxleft=A[left];
                    else res+=maxleft-A[left];
                    left++;
                }
                else{
                    if(A[right]>=maxright) maxright= A[right];
                    else res+=maxright-A[right];
                    right--;
                }
            }
            return res;
        }
    };
</p>


### Share my short solution.
- Author: yuyibestman
- Creation Date: Fri Aug 29 2014 01:06:17 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 22:10:26 GMT+0800 (Singapore Standard Time)

<p>
Keep track of the maximum height from both forward directions backward directions, call them leftmax and rightmax. 

----------

    public int trap(int[] A){
        int a=0;
        int b=A.length-1;
        int max=0;
        int leftmax=0;
        int rightmax=0;
        while(a<=b){
            leftmax=Math.max(leftmax,A[a]);
            rightmax=Math.max(rightmax,A[b]);
            if(leftmax<rightmax){
                max+=(leftmax-A[a]);       // leftmax is smaller than rightmax, so the (leftmax-A[a]) water can be stored
                a++;
            }
            else{
                max+=(rightmax-A[b]);
                b--;
            }
        }
        return max;
    }
</p>


### 7 lines C / C++
- Author: StefanPochmann
- Creation Date: Wed Jul 15 2015 03:40:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 05:19:01 GMT+0800 (Singapore Standard Time)

<p>
Keep track of the already safe `level` and the total `water` so far. In each step, process and discard the `lower` one of the leftmost or rightmost elevation.

---

**C**

Changing the given parameters to discard the lower border. I'm quite fond of this one.

    int trap(int* height, int n) {
        int level = 0, water = 0;
        while (n--) {
            int lower = *height < height[n] ? *height++ : height[n];
            if (lower > level) level = lower;
            water += level - lower;
        }
        return water;
    }

Slight variation with two pointers (left and right).

    int trap(int* height, int n) {
        int *L = height, *R = L+n-1, level = 0, water = 0;
        while (L < R) {
            int lower = *L < *R ? *L++ : *R--;
            if (lower > level) level = lower;
            water += level - lower;
        }
        return water;
    }

---

**C++**

With left and right index.

    int trap(vector<int>& height) {
        int l = 0, r = height.size()-1, level = 0, water = 0;
        while (l < r) {
            int lower = height[height[l] < height[r] ? l++ : r--];
            level = max(level, lower);
            water += level - lower;
        }
        return water;
    }

With left and right iterator.

    int trap(vector<int>& height) {
        auto l = height.begin(), r = height.end() - 1;
        int level = 0, water = 0;
        while (l != r + 1) {
            int lower = *l < *r ? *l++ : *r--;
            level = max(level, lower);
            water += level - lower;
        }
        return water;
    }
</p>


