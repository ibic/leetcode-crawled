---
title: "Rotate Array"
weight: 179
#id: "rotate-array"
---
## Description
<div class="description">
<p>Given an array, rotate the array to the right by <em>k</em> steps, where&nbsp;<em>k</em>&nbsp;is non-negative.</p>

<p><strong>Follow up:</strong></p>

<ul>
	<li>Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.</li>
	<li>Could you do it in-place with O(1) extra space?</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4,5,6,7], k = 3
<strong>Output:</strong> [5,6,7,1,2,3,4]
<strong>Explanation:</strong>
rotate 1 steps to the right: [7,1,2,3,4,5,6]
rotate 2 steps to the right: [6,7,1,2,3,4,5]
rotate 3 steps to the right: [5,6,7,1,2,3,4]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [-1,-100,3,99], k = 2
<strong>Output:</strong> [3,99,-1,-100]
<strong>Explanation:</strong> 
rotate 1 steps to the right: [99,-1,-100,3]
rotate 2 steps to the right: [3,99,-1,-100]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 2 * 10<sup>4</sup></code></li>
	<li><code>-2<sup>31</sup> &lt;= nums[i] &lt;= 2<sup>31</sup> - 1</code></li>
	<li><code>0 &lt;= k &lt;= 10<sup>5</sup></code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Facebook - 5 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Cisco - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary

We have to rotate the elements of the given array k times to the right.

## Solution

---

#### Approach 1: Brute Force

The simplest approach is to rotate all the elements of the array in $$k$$ steps
by rotating the elements by 1 unit in each step.

<iframe src="https://leetcode.com/playground/SyXheKcY/shared" frameBorder="0" width="100%" height="310" name="SyXheKcY"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(n \times k)$$. 
All the numbers are shifted by one step($$\mathcal{O}(n)$$) 
k times($$\mathcal{O}(n)$$).

* Space complexity: $$\mathcal{O}(1)$$. No extra space is used.

<br /> 
<br />


---
#### Approach 2: Using Extra Array

**Algorithm**

We use an extra array in which we place every element of the array at its correct
position i.e. the number at index $$i$$ in the original array is placed at the
index $$(i + k) \% \text{ length of array}$$. 
Then, we copy the new array to the original one.

<iframe src="https://leetcode.com/playground/ZCcvd6Yy/shared" frameBorder="0" width="100%" height="242" name="ZCcvd6Yy"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(n)$$. 
One pass is used to put the numbers in the new array.
And another pass to copy the new array to the original one.

* Space complexity: $$\mathcal{O}(n)$$. Another array of the same size is used.

---
#### Approach 3: Using Cyclic Replacements

**Algorithm**

We can directly place every number of the array at its required correct position.
But if we do that, we will destroy the original element. Thus, we need to store
the number being replaced in a $$temp$$ variable. Then, we can place the replaced
number($$\text{temp}$$) at its correct position and so on, $$n$$ times, where $$n$$ is
the length of array. We have chosen $$n$$ to be the number of replacements since we have
to shift all the elements of the array(which is $$n$$). 
But, there could be a problem with this method, if $$n \% k = 0$$
where $$k = k \% n$$ (since a value of $$k$$ larger than $$n$$ eventually 
leads to a $$k$$ equivalent to $$k \% n$$). 
In this case, while picking up numbers to be placed at the
correct position, we will eventually reach the number from 
which we originally started. Thus, in such a case, when
we hit the original number's index again, we start the same process 
with the number following it.

Now let's look at the proof of how the above method works. 
Suppose, we have $$n$$ as the number of elements in the array and
$$k$$ is the number of shifts required. Further, assume $$n \%k = 0$$. 
Now, when we start placing the elements at their correct position, 
in the first cycle all the numbers with their index $$i$$ satisfying 
$$i \%k = 0$$ get placed at their required position. 
This happens because when we jump k steps every time, 
we will only hit the numbers k steps apart. 
We start with index $$i = 0$$, having $$i \% k = 0$$. 
Thus, we hit all the numbers satisfying the above condition in the first cycle. 
When we reach back the original index, we have placed $$\frac{n}{k}$$ 
elements at their correct position, 
since we hit only that many elements in the first cycle. 
Now, we increment the index for replacing the numbers. 
This time, we place other $$\frac{n}{k}$$ elements at their correct position, 
different from the ones placed correctly in the first cycle, 
because this time we hit all the numbers satisfy the condition $$i \% k = 1$$. 
When we hit the starting number again, 
we increment the index and repeat the same process from $$i = 1$$ 
for all the indices satisfying $$i \% k == 1$$. 
This happens till we reach the number with the index $$i \% k = 0$$ 
again, which occurs for $$i=k$$. 
We will reach such a number after a total of $$k$$ cycles. 
Now, the total count of numbers exclusive numbers placed at their correct 
position will be $$k \times \frac{n}{k} = n$$. 
Thus, all the numbers will be placed at their correct position.

Look at the following example to clarify the process:
 
```
nums: [1, 2, 3, 4, 5, 6]
k: 2
```

![Rotate Array](https://leetcode.com/media/original_images/189_Rotate_Array.png)

<iframe src="https://leetcode.com/playground/RSPKZuqR/shared" frameBorder="0" width="100%" height="361" name="RSPKZuqR"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(n)$$. Only one pass is used.

* Space complexity: $$\mathcal{O}(1)$$. Constant extra space is used.

---
#### Approach 4: Using Reverse

**Algorithm**

This approach is based on the fact that when we rotate the array k times, $$k%n$$ elements from the back end of the array come to the front and the rest of the elements from the front shift backwards.

In this approach, we firstly reverse all the elements of the array. Then, reversing the first k elements followed by reversing the rest $$n-k$$ elements gives us the required result.

Let $$n = 7$$ and $$k = 3$$.
```
Original List                   : 1 2 3 4 5 6 7
After reversing all numbers     : 7 6 5 4 3 2 1
After reversing first k numbers : 5 6 7 4 3 2 1
After revering last n-k numbers : 5 6 7 1 2 3 4 --> Result
```

<iframe src="https://leetcode.com/playground/8fZRNLsB/shared" frameBorder="0" width="100%" height="344" name="8fZRNLsB"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(n)$$. $$n$$ elements are reversed a total of three times.

* Space complexity: $$\mathcal{O}(1)$$. No extra space is used.
  
<br /> 
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy to read Java solution
- Author: danny6514
- Creation Date: Wed May 20 2015 13:14:13 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 04:08:17 GMT+0800 (Singapore Standard Time)

<p>
I really don't like those _*something little*_ line solutions as they are incredibly hard to read. Below is my solution.

    public void rotate(int[] nums, int k) {
        k %= nums.length;
        reverse(nums, 0, nums.length - 1);
        reverse(nums, 0, k - 1);
        reverse(nums, k, nums.length - 1);
    }
    
    public void reverse(int[] nums, int start, int end) {
        while (start < end) {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
    }
</p>


### Summary of C++ solutions
- Author: zhukov
- Creation Date: Sun Mar 08 2015 08:52:17 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 13:41:18 GMT+0800 (Singapore Standard Time)

<p>
#1. Make an extra copy and then rotate. 
Time complexity: O(n). Space complexity: O(n).
    
        class Solution 
        {
        public:
            void rotate(int nums[], int n, int k) 
            {
                if ((n == 0) || (k <= 0))
                {
                    return;
                }
                
                // Make a copy of nums
                vector<int> numsCopy(n);
                for (int i = 0; i < n; i++)
                {
                    numsCopy[i] = nums[i];
                }
                
                // Rotate the elements.
                for (int i = 0; i < n; i++)
                {
                    nums[(i + k)%n] = numsCopy[i];
                }
            }
        };

#2. Start from one element and keep rotating until we have rotated n different elements.
Time complexity: O(n). Space complexity: O(1).
    
        class Solution 
        {
        public:
            void rotate(int nums[], int n, int k) 
            {
                if ((n == 0) || (k <= 0))
                {
                    return;
                }
                
                int cntRotated = 0;
                int start = 0;
                int curr = 0;
                int numToBeRotated = nums[0];
                int tmp = 0;
                // Keep rotating the elements until we have rotated n 
                // different elements.
                while (cntRotated < n)
                {
                    do
                    {
                        tmp = nums[(curr + k)%n];
                        nums[(curr+k)%n] = numToBeRotated;
                        numToBeRotated = tmp;
                        curr = (curr + k)%n;
                        cntRotated++;
                    } while (curr != start);
                    // Stop rotating the elements when we finish one cycle, 
                    // i.e., we return to start.
                    
                    // Move to next element to start a new cycle.
                    start++;
                    curr = start;
                    numToBeRotated = nums[curr];
                }
            }
        };
    
#3. Reverse the first n - k elements, the last k elements, and then all the n elements.
Time complexity: O(n). Space complexity: O(1).
        
        class Solution 
        {
        public:
            void rotate(int nums[], int n, int k) 
            {
                k = k%n;
        
                // Reverse the first n - k numbers.
                // Index i (0 <= i < n - k) becomes n - k - i.
                reverse(nums, nums + n - k);
                
                // Reverse tha last k numbers.
                // Index n - k + i (0 <= i < k) becomes n - i.
                reverse(nums + n - k, nums + n);
                
                // Reverse all the numbers.
                // Index i (0 <= i < n - k) becomes n - (n - k - i) = i + k.
                // Index n - k + i (0 <= i < k) becomes n - (n - i) = i.
                reverse(nums, nums + n);
            }
        };

#4. Swap the last k elements with the first k elements.
Time complexity: O(n). Space complexity: O(1).

    class Solution 
    {
    public:
        void rotate(int nums[], int n, int k) 
        {
            for (; k = k%n; n -= k, nums += k)
            {
                // Swap the last k elements with the first k elements. 
                // The last k elements will be in the correct positions
                // but we need to rotate the remaining (n - k) elements 
                // to the right by k steps.
                for (int i = 0; i < k; i++)
                {
                    swap(nums[i], nums[n - k + i]);
                }
            }
        }
    };

#5. Keep swapping two subarrays.
Time complexity: O(n). Space complexity: O(1).
    
    class Solution 
    {
    public:
        void rotate(int nums[], int n, int k) 
        {
            if ((n == 0) || (k <= 0) || (k%n == 0))
            {
                return;
            }
            
            k = k%n;
            // Rotation to the right by k steps is equivalent to swapping 
            // the two subarrays nums[0,...,n - k - 1] and nums[n - k,...,n - 1].
            int start = 0;
            int tmp = 0;
            while (k > 0)
            {
                if (n - k >= k)
                {
                    // The left subarray with size n - k is longer than 
                    // the right subarray with size k. Exchange 
                    // nums[n - 2*k,...,n - k - 1] with nums[n - k,...,n - 1].
                    for (int i = 0; i < k; i++)
                    {
                        tmp = nums[start + n - 2*k + i];
                        nums[start + n - 2*k + i] = nums[start + n - k + i];
                        nums[start + n - k + i] = tmp;
                    }
                    
                    // nums[n - 2*k,...,n - k - 1] are in their correct positions now.
                    // Need to rotate the elements of nums[0,...,n - k - 1] to the right 
                    // by k%n steps.
                    n = n - k;
                    k = k%n;
                }
                else
                {
                    // The left subarray with size n - k is shorter than 
                    // the right subarray with size k. Exchange 
                    // nums[0,...,n - k - 1] with nums[n - k,...,2*(n - k) - 1].
                    for (int i = 0; i < n - k; i++)
                    {
                        tmp = nums[start + i];
                        nums[start + i] = nums[start + n - k + i];
                        nums[start + n - k + i] = tmp;
                    }
                    
                    // nums[n - k,...,2*(n - k) - 1] are in their correct positions now.
                    // Need to rotate the elements of nums[n - k,...,n - 1] to the right 
                    // by k - (n - k) steps.
                    tmp = n - k;
                    n = k;
                    k -= tmp;
                    start += tmp;
                }
            }
        }
    };
</p>


### My solution by using Python
- Author: Maples7
- Creation Date: Wed Mar 18 2015 23:39:38 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 23:20:16 GMT+0800 (Singapore Standard Time)

<p>
    class Solution:
        # @param nums, a list of integer
        # @param k, num of steps
        # @return nothing, please modify the nums list in-place.
        def rotate(self, nums, k):
            n = len(nums)
            k = k % n
            nums[:] = nums[n-k:] + nums[:n-k]
        

A little important thing to be cautious:  

    nums[:] = nums[n-k:] + nums[:n-k] 
can't be written as:

    nums = nums[n-k:] + nums[:n-k]

on the OJ. 
    
The previous one can truly change the value of **old** *nums*, but the following one just changes its reference to a **new** *nums* not the value of **old** *nums*.
</p>


