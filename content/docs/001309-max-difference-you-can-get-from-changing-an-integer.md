---
title: "Max Difference You Can Get From Changing an Integer"
weight: 1309
#id: "max-difference-you-can-get-from-changing-an-integer"
---
## Description
<div class="description">
<p>You are given an integer <code>num</code>. You will apply the following steps exactly <strong>two</strong> times:</p>

<ul>
	<li>Pick a digit <code>x (0&nbsp;&lt;= x &lt;= 9)</code>.</li>
	<li>Pick another digit <code>y (0&nbsp;&lt;= y &lt;= 9)</code>. The digit <code>y</code> can be equal to <code>x</code>.</li>
	<li>Replace all the occurrences of <code>x</code> in the decimal representation of <code>num</code> by <code>y</code>.</li>
	<li>The new integer <strong>cannot</strong> have any leading zeros, also the new integer <strong>cannot</strong> be 0.</li>
</ul>

<p>Let <code>a</code>&nbsp;and <code>b</code>&nbsp;be the results of applying the operations to <code>num</code> the first and second times, respectively.</p>

<p>Return <em>the max difference</em> between <code>a</code> and <code>b</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> num = 555
<strong>Output:</strong> 888
<strong>Explanation:</strong> The first time pick x = 5 and y = 9 and store the new integer in a.
The second time pick x = 5 and y = 1 and store the new integer in b.
We have now a = 999 and b = 111 and max difference = 888
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> num = 9
<strong>Output:</strong> 8
<strong>Explanation:</strong> The first time pick x = 9 and y = 9 and store the new integer in a.
The second time pick x = 9 and y = 1 and store the new integer in b.
We have now a = 9 and b = 1 and max difference = 8
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> num = 123456
<strong>Output:</strong> 820000
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> num = 10000
<strong>Output:</strong> 80000
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> num = 9288
<strong>Output:</strong> 8700
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= num &lt;= 10^8</code></li>
</ul>

</div>

## Tags
- String (string)

## Companies
- mercari - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python3] Easy Solution
- Author: localhostghost
- Creation Date: Sun May 03 2020 00:12:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 00:30:02 GMT+0800 (Singapore Standard Time)

<p>
Basically just brute force.

```
class Solution:
    def maxDiff(self, num: int) -> int:
        num = str(num)
        maxNum, minNum = float(\'-inf\'), float(\'inf\')
        for i in \'0123456789\':
            for j in \'0123456789\':
                nextNum = num.replace(i, j)
                if nextNum[0] == \'0\' or int(nextNum) == 0:
                    continue
                maxNum = max(maxNum, int(nextNum))    
                minNum = min(minNum, int(nextNum))    
        return maxNum - minNum 
```
</p>


### [Python] Clean greedy solution with explanation
- Author: C0R3
- Creation Date: Sun May 03 2020 00:31:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 00:32:07 GMT+0800 (Singapore Standard Time)

<p>
We need to maximize the difference of two numbers ```a``` and ```b```. Therefore we should maximize ```a``` and minimize ```b```.

To maximize ```a``` we need to find the left-most digit in it that is not a 9 and replace all occurences of that digit with 9.
9 is the maximum possible digit and the more left we find a candidate for replacement the bigger the result gets.

To minimize ```b``` we need to find the left-most digit in it that is not a 0 and replace all occurences of that digit with 0.
But we have to watch out: We are not allowed to convert the first digit to a 0 as we should not create a number with trailing zeroes.
Therefore we can only replace the first digit with a 1. All other digits can be replaced with a 0 if they are not a 1 as that would also replace the trailing 1 with a 0.

```python
class Solution:
    def maxDiff(self, num: int) -> int:
        a = b = str(num)

        for digit in a:
            if digit != "9":
                a = a.replace(digit, "9")
                break
        
        if b[0] != "1":
            b = b.replace(b[0], "1")
        else:
            for digit in b[1:]:
                if digit not in "01":
                    b = b.replace(digit, "0")
                    break
        
        return int(a) - int(b)
```
</p>


### [Java/Python 3] O(digits) w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun May 03 2020 00:03:42 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 06 2020 23:22:32 GMT+0800 (Singapore Standard Time)

<p>
Convert to char array/list then process.

The following explains the algorithm of the code: -- credit to **@leetcodeCR97 and @martin20**.

If your first character `x` is 
1. neither `1` nor `9` then you will be replacing `a` with `9 `and `b` with `1 `if it matched the first character of the given number. 
2. `1` then you already have `b` starting with `1` which is least and you will have one more chance to decrease `b` to `0` which should be in MSB(Left most side). 
3. `9` then you will have one more chance to change `a` to `9` which should be in MSB(Left most side). 

```java
    public int maxDiff(int num) {
        char[] a = Integer.toString(num).toCharArray(), b = a.clone();
        char x = a[0], y = 0;
        for (int i = 0; i < a.length; ++i) {
            if (a[i] == x) {
                a[i] = \'9\';
                b[i] = \'1\';
            }else if (x == \'1\' && a[i] > \'0\' || x == \'9\' && a[i] < \'9\') {
                if (y == 0) {
                    y = a[i];
                } 
                if (y == a[i]) {
                    if (x == \'1\')
                        b[i] = \'0\';
                    else
                        a[i] = \'9\';
                }
            }
        }
        return Integer.parseInt(String.valueOf(a)) - Integer.parseInt(String.valueOf(b));
    }
```
```python
    def maxDiff(self, num: int) -> int:
        a, b = ([d for d in str(num)] for _ in range(2))
        x, y = a[0], \' \' 
        for i, ch in enumerate(a):
            if ch == x:
                a[i] = \'9\'
                b[i] = \'1\'
            else:
                if x == \'1\' and ch != \'0\' or x == \'9\' != ch:
                    if y == \' \':
                        y = ch
                    if y == ch:
                        if x == \'1\':
                            b[i] = \'0\'
                        else:
                            a[i] = \'9\'
        return int(\'\'.join(a)) - int(\'\'.join(b))
```

**Analysis:**

Time & space: `O(n)`, where `n` is the number of the digits in `num`
</p>


