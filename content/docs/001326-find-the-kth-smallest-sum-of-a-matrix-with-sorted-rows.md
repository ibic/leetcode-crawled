---
title: "Find the Kth Smallest Sum of a Matrix With Sorted Rows"
weight: 1326
#id: "find-the-kth-smallest-sum-of-a-matrix-with-sorted-rows"
---
## Description
<div class="description">
<p>You are given an&nbsp;<code>m&nbsp;* n</code>&nbsp;matrix,&nbsp;<code>mat</code>, and an integer <code>k</code>,&nbsp;which&nbsp;has its rows sorted in non-decreasing&nbsp;order.</p>

<p>You are allowed to choose exactly 1 element from each row to form an array.&nbsp;Return the Kth <strong>smallest</strong> array sum among all possible arrays.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> mat = [[1,3,11],[2,4,6]], k = 5
<strong>Output:</strong> 7
<strong>Explanation: </strong>Choosing one element from each row, the first k smallest sum are:
[1,2], [1,4], [3,2], [3,4], [1,6]. Where the 5th sum is 7.  </pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> mat = [[1,3,11],[2,4,6]], k = 9
<strong>Output:</strong> 17
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> mat = [[1,10,10],[1,4,5],[2,3,6]], k = 7
<strong>Output:</strong> 9
<strong>Explanation:</strong> Choosing one element from each row, the first k smallest sum are:
[1,1,2], [1,1,3], [1,4,2], [1,4,3], [1,1,6], [1,5,2], [1,5,3]. Where the 7th sum is 9.  
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> mat = [[1,1,10],[2,2,9]], k = 7
<strong>Output:</strong> 12
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m == mat.length</code></li>
	<li><code>n == mat.length[i]</code></li>
	<li><code>1 &lt;= m, n &lt;= 40</code></li>
	<li><code>1 &lt;= k &lt;= min(200, n ^&nbsp;m)</code></li>
	<li><code>1 &lt;= mat[i][j] &lt;= 5000</code></li>
	<li><code>mat[i]</code> is a non decreasing array.</li>
</ul>

</div>

## Tags
- Heap (heap)

## Companies
- Facebook - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] O(k * logk * len(mat)) with detailed explanations + [4 lines python].
- Author: Chanchal_Maji
- Creation Date: Sun May 03 2020 12:00:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 13:26:34 GMT+0800 (Singapore Standard Time)

<p>
To understand this problem, first please look at - [373. Find K Pairs with Smallest Sums](https://leetcode.com/problems/find-k-pairs-with-smallest-sums/). The function `kSmallestPairs` can be taken directly from Problem - 373. So we start merging two rows at a time by using the function `kSmallestPairs`,  `m-1` times, keeping each time the size of the `res` at most `k = 200`. 

The time complexity of `kSmallestPairs` each time it is called is `k * logk`. Since it is called `m-1` times, so we have total time complexity = `k * logk * (m-1)`.

Note: `k * logk * (m-1)` <= `200 * log(200) * 40`.

Time : `O(k * logk * (m-1))`.
Space : `O(200)`.

```python
class Solution:
    def kSmallestPairs(self, nums1: List[int], nums2: List[int], k: int = 200) -> List[List[int]]:
        res = []
        h = []
        if len(nums1) == 0 or len(nums2) == 0 or k == 0:
            return res
        i = 0
        while i < len(nums1) and i < k:
            heapq.heappush(h, (nums1[i]+nums2[0], nums1[i], nums2[0], 0))
            i += 1
        while k and h:
            cur = heappop(h)
            res.append(cur[0])
            if cur[3] == len(nums2) - 1:
                continue
            heapq.heappush(h, (cur[1]+nums2[cur[3]+1], cur[1], nums2[cur[3]+1], cur[3]+1))
            k -= 1
        return res
    
    def kthSmallest(self, mat: List[List[int]], k: int) -> int:
        m, n = len(mat), len(mat[0])
        res = mat[0]
        for i in range(1, m):
            res = self.kSmallestPairs(res, mat[i])
        return res[k-1]
```

**Another Solution :** 
```python
class Solution:
    def kthSmallest(self, mat: List[List[int]], k: int) -> int:
        h = mat[0][:]
        for row in mat[1:]:
            h = sorted([i+j for i in row for j in h])[:k]
        return h[k-1]
```

Please `upvote` if u find this useful. ^_^ .
</p>


### [Java] Max Priority Queue O(m * n * k * log(k))
- Author: changeme
- Creation Date: Sun May 03 2020 12:08:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 06:43:27 GMT+0800 (Singapore Standard Time)

<p>
Intuition:
We need to keep at most 200 smallest sums. `1 <= k <= min(200, n ^ m)`
For the input with one row the answer is the smallest k-th element or top of the max priority queue of size k.

Algorithm:
Calculate max priority queue of sizr k for the first row.

Add the rest rows one by one to the max priority queue and make sure that max priority queue size is less than or equal to k.


Time: <code>O(m * n * k * log(k))</code>
Space: <code>O(k)</code>
```
public int kthSmallest(int[][] mat, int k) {
	int col = Math.min(mat[0].length, k);

	PriorityQueue<Integer> pq = new PriorityQueue<>(Collections.reverseOrder());
	pq.add(0);
	for (int[] row : mat) {
		// max priority queue for the i-th row
		PriorityQueue<Integer> nextPq = new PriorityQueue<>(Collections.reverseOrder());
		for (int i : pq) {
			for (int c = 0; c < col; c++) {
				nextPq.add(i + row[c]);
				// keep pq size <= k
				if (nextPq.size() > k) {
					nextPq.poll();
				}
			}
		}
		pq = nextPq;
	}
	return pq.poll();
}
```

</p>


### [Java/C++/Python] Binary Search & Prune Backtracking - O(m*k*log(m*5000)) ~ 4ms
- Author: hiepit
- Creation Date: Sun May 03 2020 19:38:39 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 05 2020 02:05:06 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
- Since `1 <= mat[i][j] <= 5000, m <= 40`, we can do **binary search** for our target kth smallest `sum` in range `[m, 5000*m]`
- For each `sum`, we use **backtracking** to **count the number of array** whose sum is **less than or equal to** `sum`, **prune** when `count > k`
- If we found a `sum` that `count >= k`, we record current `ans = sum`, try to find better answer in the left side.
- Else try to find in the right side.

**Complexity**
- Time: `O(m * k * log(5000*m))`, where `m <= 40` is the number of rows, `k <= min(200, n^m)`
   + `countArraysHaveSumLessOrEqual()` can run up to `(m-i+1) * min(k,n^i), 1<=i<=m` times.  And `n^i` can go up to `k` very quickly while `i` is small, so time complexity will be `O(m*k)`
Can check the following picture to understand more about complexity:
   ![image](https://assets.leetcode.com/users/hiepit/image_1588612855.png)
   Credit @yuhan-wang for provide the example
   + binary search to find `targetSum` in range `[m, 5000*m]` costs `log(5000*m)`
   
- Space: `O(m)`, it\'s the depth of recursion

**Java ~ 4ms**
```java
    public int kthSmallest(int[][] mat, int k) {
        int m = mat.length, n = mat[0].length;
        int left = m, right = m * 5000, ans = -1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            int cnt = countArraysHaveSumLessOrEqual(mat, m, n, mid, 0, 0, k);
            if (cnt >= k) {
                ans = mid;
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return ans;
    }
    int countArraysHaveSumLessOrEqual(int[][] mat, int m, int n, int targetSum, int r, int sum, int k) {
        if (sum > targetSum) return 0;
        if (r == m) return 1;
        int ans = 0;
        for (int c = 0; c < n; ++c) {
            int cnt = countArraysHaveSumLessOrEqual(mat, m, n, targetSum, r + 1, sum + mat[r][c], k - ans);
            if (cnt == 0) break;
            ans += cnt;
            if (ans > k) break; // prune when count > k
        }
        return ans;
    }
```


**C++ ~ 16ms**
```c++
    int kthSmallest(vector<vector<int>>& mat, int k) {
        int m = mat.size(), n = mat[0].size();
        int left = m, right = m * 5000, ans = -1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            int cnt = countArraysHaveSumLessOrEqual(mat, m, n, mid, 0, 0, k);
            if (cnt >= k) {
                ans = mid;
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return ans;
    }
    int countArraysHaveSumLessOrEqual(vector<vector<int>>& mat, int m, int n, int targetSum, int r, int sum, int k) {
        if (sum > targetSum) return 0;
        if (r == m) return 1;
        int ans = 0;
        for (int c = 0; c < n; ++c) {
            int cnt = countArraysHaveSumLessOrEqual(mat, m, n, targetSum, r + 1, sum + mat[r][c], k - ans);
            if (cnt == 0) break;
            ans += cnt;
            if (ans > k) break; // prune when count > k
        }
        return ans;
    }
```

**Python ~ 360ms**
```python
    def kthSmallest(self, mat, k):
        m, n = len(mat), len(mat[0])
        
        def countArraysHaveSumLessOrEqual(targetSum, r, sum, k):
            if sum > targetSum: return 0
            if r == m: return 1
            ans = 0
            for c in xrange(0, n):
                cnt = countArraysHaveSumLessOrEqual(targetSum, r + 1, sum + mat[r][c], k - ans)
                if cnt == 0: break
                ans += cnt
                if ans > k: break # prune when count > k
            return ans
        
        left, right, ans = m, m * 5000, -1
        while left <= right:
            mid = left + (right - left) // 2
            cnt = countArraysHaveSumLessOrEqual(mid, 0, 0, k)
            if cnt >= k:
                ans = mid
                right = mid - 1
            else:
                left = mid + 1
        return ans
```

Inspired from [This Post](https://leetcode.com/problems/find-the-kth-smallest-sum-of-a-matrix-with-sorted-rows/discuss/609953) by @MrGhasita
</p>


