---
title: "Data Stream as Disjoint Intervals"
weight: 335
#id: "data-stream-as-disjoint-intervals"
---
## Description
<div class="description">
<p>Given a data stream input of non-negative integers a<sub>1</sub>, a<sub>2</sub>, ..., a<sub>n</sub>, ..., summarize the numbers seen so far as a list of disjoint intervals.</p>

<p>For example, suppose the integers from the data stream are 1, 3, 7, 2, 6, ..., then the summary will be:</p>

<pre>
[1, 1]
[1, 1], [3, 3]
[1, 1], [3, 3], [7, 7]
[1, 3], [7, 7]
[1, 3], [6, 7]
</pre>

<p>&nbsp;</p>

<p><b>Follow up:</b></p>

<p>What if there are lots of merges and the number of disjoint intervals are small compared to the data stream&#39;s size?</p>

</div>

## Tags
- Binary Search (binary-search)
- Ordered Map (ordered-map)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Oracle - 7 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution using TreeMap, real O(logN) per adding.
- Author: qianzhige
- Creation Date: Wed Jun 01 2016 03:29:06 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 09:21:17 GMT+0800 (Singapore Standard Time)

<p>
Use TreeMap to easily find the lower and higher keys, the key is the start of the interval. 
Merge the lower and higher intervals when necessary. The time complexity for adding is O(logN) since lowerKey(), higherKey(), put() and remove() are all O(logN). It would be O(N) if you use an ArrayList and remove an interval from it. 

    public class SummaryRanges {
        TreeMap<Integer, Interval> tree;
    
        public SummaryRanges() {
            tree = new TreeMap<>();
        }
    
        public void addNum(int val) {
            if(tree.containsKey(val)) return;
            Integer l = tree.lowerKey(val);
            Integer h = tree.higherKey(val);
            if(l != null && h != null && tree.get(l).end + 1 == val && h == val + 1) {
                tree.get(l).end = tree.get(h).end;
                tree.remove(h);
            } else if(l != null && tree.get(l).end + 1 >= val) {
                tree.get(l).end = Math.max(tree.get(l).end, val);
            } else if(h != null && h == val + 1) {
                tree.put(val, new Interval(val, tree.get(h).end));
                tree.remove(h);
            } else {
                tree.put(val, new Interval(val, val));
            }
        }
    
        public List<Interval> getIntervals() {
            return new ArrayList<>(tree.values());
        }
    }
</p>


### I can not understand the question description, can any one explain it?
- Author: lirongbin
- Creation Date: Wed Jun 01 2016 14:06:48 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 21 2018 23:51:03 GMT+0800 (Singapore Standard Time)

<p>
I can not understand the question description, can any one explain it?
</p>


### Very concise c++ solution.
- Author: goodliar
- Creation Date: Wed Jun 01 2016 07:13:17 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 01 2016 07:13:17 GMT+0800 (Singapore Standard Time)

<p>
In general case, vector is OK, it will take O(n) time in each add, and O(1) in get result. But if there are lots of merges and the number of disjoint intervals are small compared to the data stream's size, we'd better use another data structure "set", because the insert operation in vector will cost O(n) time, but it only cost O(log n) in binary search tree, but it will cost O(n) time in getInterval. So use which data structure will depends. 

first one is the solution use vector

    class SummaryRanges {
    public:
        void addNum(int val) {
            auto Cmp = [](Interval a, Interval b) { return a.start < b.start; };
            auto it = lower_bound(vec.begin(), vec.end(), Interval(val, val), Cmp);
            int start = val, end = val;
            if(it != vec.begin() && (it-1)->end+1 >= val) it--;
            while(it != vec.end() && val+1 >= it->start && val-1 <= it->end)
            {
                start = min(start, it->start);
                end = max(end, it->end);
                it = vec.erase(it);
            }
            vec.insert(it,Interval(start, end));
        }
        
        vector<Interval> getIntervals() {
            return vec;
        }
    private:
        vector<Interval> vec;
    };

and below is another solution use binary search tree.

    class SummaryRanges {
    public:
        /** Initialize your data structure here. */
        void addNum(int val) {
            auto it = st.lower_bound(Interval(val, val));
            int start = val, end = val;
            if(it != st.begin() && (--it)->end+1 < val) it++;
            while(it != st.end() && val+1 >= it->start && val-1 <= it->end)
            {
                start = min(start, it->start);
                end = max(end, it->end);
                it = st.erase(it);
            }
            st.insert(it,Interval(start, end));
        }
        
        vector<Interval> getIntervals() {
            vector<Interval> result;
            for(auto val: st) result.push_back(val);
            return result;
        }
    private:
        struct Cmp{
            bool operator()(Interval a, Interval b){ return a.start < b.start; }
        };
        set<Interval, Cmp> st;
    };
</p>


