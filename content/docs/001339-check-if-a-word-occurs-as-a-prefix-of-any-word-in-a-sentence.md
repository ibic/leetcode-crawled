---
title: "Check If a Word Occurs As a Prefix of Any Word in a Sentence"
weight: 1339
#id: "check-if-a-word-occurs-as-a-prefix-of-any-word-in-a-sentence"
---
## Description
<div class="description">
<p>Given a <code>sentence</code>&nbsp;that consists of some words separated by a&nbsp;<strong>single space</strong>, and a <code>searchWord</code>.</p>

<p>You have to check if <code>searchWord</code> is a prefix of any word in <code>sentence</code>.</p>

<p>Return <em>the index of the word</em> in <code>sentence</code> where <code>searchWord</code> is a prefix of this word (<strong>1-indexed</strong>).</p>

<p>If <code>searchWord</code> is&nbsp;a prefix of more than one word, return the index of the first word <strong>(minimum index)</strong>. If there is no such word return <strong>-1</strong>.</p>

<p>A <strong>prefix</strong> of a string&nbsp;<code>S</code> is any leading contiguous substring of <code>S</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> sentence = &quot;i love eating burger&quot;, searchWord = &quot;burg&quot;
<strong>Output:</strong> 4
<strong>Explanation:</strong> &quot;burg&quot; is prefix of &quot;burger&quot; which is the 4th word in the sentence.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> sentence = &quot;this problem is an easy problem&quot;, searchWord = &quot;pro&quot;
<strong>Output:</strong> 2
<strong>Explanation:</strong> &quot;pro&quot; is prefix of &quot;problem&quot; which is the 2nd and the 6th word in the sentence, but we return 2 as it&#39;s the minimal index.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> sentence = &quot;i am tired&quot;, searchWord = &quot;you&quot;
<strong>Output:</strong> -1
<strong>Explanation:</strong> &quot;you&quot; is not a prefix of any word in the sentence.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> sentence = &quot;i use triple pillow&quot;, searchWord = &quot;pill&quot;
<strong>Output:</strong> 4
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> sentence = &quot;hello from the other side&quot;, searchWord = &quot;they&quot;
<strong>Output:</strong> -1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= sentence.length &lt;= 100</code></li>
	<li><code>1 &lt;= searchWord.length &lt;= 10</code></li>
	<li><code>sentence</code> consists of lowercase English letters and spaces.</li>
	<li><code>searchWord</code>&nbsp;consists of lowercase English letters.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Yelp - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Straight forward codes.
- Author: rock
- Creation Date: Sun May 24 2020 12:35:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 24 2020 12:35:42 GMT+0800 (Singapore Standard Time)

<p>
```java
    public int isPrefixOfWord(String sentence, String searchWord) {
        String[] words = sentence.split(" ");
        for (int i = 1; i <= words.length; ++i) {
            if (words[i - 1].startsWith(searchWord)) {
                return i;
            }
        }
        return -1;
    }
```

```python
    def isPrefixOfWord(self, sentence: str, searchWord: str) -> int:
        for i, w in enumerate(sentence.split(\' \'), 1):
            if w.startswith(searchWord):
                return i
        return -1    
```
</p>


### C++ Add Leading Space
- Author: votrubac
- Creation Date: Sun May 24 2020 12:02:57 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 26 2020 06:43:57 GMT+0800 (Singapore Standard Time)

<p>
To make it simple, we add a leading space to our sentence and word. Then, we can do a string search, and cout the number of spaces before the found word.

```cpp
int isPrefixOfWord(string sentence, string searchWord) {
    auto sent = " " + sentence, word = " " + searchWord;
    auto pos = sent.find(word);
    if (pos != string::npos)
        return count(begin(sent), begin(sent) + pos + 1, \' \');
    return -1;
}
```
**Complexity Analysis**
- Time: O(n), where n is the characters in the sentence. 
	
	We go through the sentence twice. `string.find` in C++ is KPM, so it\'s complexity is O(n + m), where m is the characters in the word. But, since our string does not have repeated parts (due to a leading space), the actual complexity is O(n).
	
- Memory: O(m), used internally by `string.find` (KMP).

**Optimized KMP-ish Solution**
We can use two pointers to solve this problem in a single pass. We do not need KMP - our word has exactly one space in the beginning, thus no repeating substrings.

**Update:** we do need one check from KMP, but just for the first letter (space). Thanks [vitrant](https://leetcode.com/vikrant_pc/) for the find!

```cpp
int isPrefixOfWord(string sentence, string searchWord) {
    auto sent = " " + sentence, word = " " + searchWord;
    int word_cnt = 0, j = 0;
    for (auto i = 0; i < sent.size() && j < word.size(); ++i) {
        word_cnt += sent[i] == \' \';
        if (sent[i] == word[j])
            ++j;
        else
            j = sent[i] == word[0] ? 1 : 0;
    }
    return j == word.size() ? word_cnt : -1;
}
```
**If you do not like adding a space**
Well, just check if the previous character is a space. Here, we do not need the KMP check as space will re-start `j` naturally. 
```cpp
int isPrefixOfWord(string sent, string word) {
    int word_cnt = 1, j = 0;
    for (auto i = 0; i < sent.size() && j < word.size(); ++i) {
        word_cnt += sent[i] == \' \';
        if (sent[i] == word[j])
            j = j > 0 ? j + 1 : (i == 0 || sent[i -1] == \' \');
        else
            j = 0;
    }
    return j == word.size() ? word_cnt : -1;
}
```
**Complexity Analysis**
- Time: O(n), where n is the number of characters in the sentence. We do through the sentence exactly once.
- Memory: O(1).
</p>


### [C++/Python] Simple simulation and 2-lines
- Author: codedayday
- Creation Date: Sun May 24 2020 12:22:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 24 2020 22:58:06 GMT+0800 (Singapore Standard Time)

<p>
Version 1: simple logic 
```
class Solution {
public:
    //int isPrefixOfWord(string& sentence, string& searchWord) { // also ok
    int isPrefixOfWord(string sentence, string_view searchWord) {
        stringstream ss(sentence);
        string word;
        int i=1;
        while(ss>>word){
            if(word.find(searchWord) == 0 ) return i;
            i++;
        }
        return -1;
    }
};
```

Version 2: fewer lines but harder to read
```
class Solution {
public:
    int isPrefixOfWord(string sentence, string_view searchWord) {
        stringstream ss(sentence);
        string word;
        int i=0;
        while(ss>>word) 
		    if(++i && word.find(searchWord) == 0 ) return i;
        return -1;
    }
};
```

Version 3: bonus solution. Only for fun
```
class Solution {
public:
    int isPrefixOfWord(string sentence, string searchWord) {                        
        auto it = (\' \' + sentence).find(\' \' + searchWord);        
        return it ==string::npos ? -1: count(begin(sentence), begin(sentence) + it, \' \') + 1; 
    }
};
```

Python: Solution 1
```
class Solution:
    def isPrefixOfWord(self, sentence: str, searchWord: str) -> int:        
        it = (\' \' + sentence).find(\' \' + searchWord);        
        return -1 if it == -1 else sentence[:it].count(\' \') + 1;
```

Python: Solution 2
https://stackoverflow.com/questions/3862010/is-there-a-generator-version-of-string-split-in-python
With re.finditer, we cau use minimal memory overhead.
```
class Solution:
    def split_iter(self, sentence: str):
        return (x.group(0) for x in re.finditer(r"[a-z]+", sentence))

    def isPrefixOfWord(self, sentence: str, searchWord: str) -> int:        
        for i,w in enumerate(self.split_iter(sentence)):
            if w.startswith(searchWord):
                return i + 1
        return -1
```
Credit: Thanks for sharing from @zhanghuimeng: 
https://leetcode.com/problems/check-if-a-word-occurs-as-a-prefix-of-any-word-in-a-sentence/discuss/648361/Python3-Split-and-check
</p>


