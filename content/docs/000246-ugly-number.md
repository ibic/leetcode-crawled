---
title: "Ugly Number"
weight: 246
#id: "ugly-number"
---
## Description
<div class="description">
<p>Write a program to check whether a given number is an ugly number.</p>

<p>Ugly numbers are <strong>positive numbers</strong> whose prime factors only include <code>2, 3, 5</code>.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 6
<strong>Output:</strong> true
<strong>Explanation: </strong>6 = 2 &times;&nbsp;3</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> 8
<strong>Output:</strong> true
<strong>Explanation: </strong>8 = 2 &times; 2 &times;&nbsp;2
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> 14
<strong>Output:</strong> false 
<strong>Explanation: </strong><code>14</code> is not ugly since it includes another prime factor <code>7</code>.
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1</code> is typically treated as an ugly number.</li>
	<li>Input is within the 32-bit signed integer range:&nbsp;[&minus;2<sup>31</sup>,&nbsp; 2<sup>31&nbsp;</sup>&minus; 1].</li>
</ol>
</div>

## Tags
- Math (math)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 2-4 lines, every language
- Author: StefanPochmann
- Creation Date: Wed Aug 19 2015 03:37:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 16:07:27 GMT+0800 (Singapore Standard Time)

<p>
Just divide by 2, 3 and 5 as often as possible and then check whether we arrived at 1. Also try divisor 4 if that makes the code simpler / less repetitive.

**C++ / C**

    for (int i=2; i<6 && num; i++)
        while (num % i == 0)
            num /= i;
    return num == 1;

**Ruby**

    (2..5).each { |i| num /= i while num % i == 0 } if num > 0
    num == 1

Or:

    require 'prime'
    num > 0 && num.prime_division.all? { |p, _| p < 6 }

**Python**

    for p in 2, 3, 5:
        while num % p == 0 < num:
            num /= p
    return num == 1

**Java / C#**

    for (int i=2; i<6 && num>0; i++)
        while (num % i == 0)
            num /= i;
    return num == 1;

**Javascript**

    for (var p of [2, 3, 5])
        while (num && num % p == 0)
            num /= p;
    return num == 1;

---

**General**

Would be a bit cleaner if I did the zero-test outside, and discarding negative numbers right away can speed things up a little, but meh... I don't want to add another line and indentation level :-)

    if (num > 0)
        for (int i=2; i<6; i++)
            while (num % i == 0)
                num /= i;
    return num == 1;
</p>


### My 2ms java solution
- Author: kittyfeng
- Creation Date: Mon Sep 28 2015 15:45:44 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 03:36:20 GMT+0800 (Singapore Standard Time)

<p>
    public boolean isUgly(int num) {
        if(num==1) return true;
        if(num==0) return false;
    	while(num%2==0) num=num>>1;
    	while(num%3==0) num=num/3;
    	while(num%5==0) num=num/5;
        return num==1;
    }
</p>


### My python solution
- Author: chaowyc
- Creation Date: Sun Oct 18 2015 20:54:07 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 18:08:58 GMT+0800 (Singapore Standard Time)

<p>

    def isUgly(self, num):
        """
        :type num: int
        :rtype: bool
        """
        if num <= 0:
            return False
        for x in [2, 3, 5]:
            while num % x == 0:
                num = num / x
        return num == 1
</p>


