---
title: "Number of Days in a Month"
weight: 997
#id: "number-of-days-in-a-month"
---
## Description
<div class="description">
<p>Given a year <code>Y</code> and a month <code>M</code>, return how many days there are in that month.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>Y = <span id="example-input-1-1">1992</span>, M = <span id="example-input-1-2">7</span>
<strong>Output: </strong><span id="example-output-1">31</span>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>Y = <span id="example-input-2-1">2000</span>, M = <span id="example-input-2-2">2</span>
<strong>Output: </strong><span id="example-output-2">29</span>
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>Y = <span id="example-input-3-1">1900</span>, M = <span id="example-input-3-2">2</span>
<strong>Output: </strong><span id="example-output-3">28</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1583 &lt;= Y &lt;= 2100</code></li>
	<li><code>1 &lt;= M &lt;= 12</code></li>
</ol>

</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] 1 liner & 2 liner.
- Author: rock
- Creation Date: Sun Jul 14 2019 00:02:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 14 2019 00:30:12 GMT+0800 (Singapore Standard Time)

<p>
**Method 1:**
Use API.

```
    public int numberOfDays(int Y, int M) {
        return java.time.YearMonth.of(Y, M).lengthOfMonth();
    }
```
**Method 2:**
Use Year and Month rules.
**Note: days[0] is a dummy value.**
```
   private static final int[] days = {0,31,28,31,30,31,30,31,31,30,31,30,31};
   public int numberOfDays(int Y, int M) {
        return M == 2 && (Y % 4 == 0 && Y % 100 != 0 || Y % 400 == 0) ? 29 : days[M];
    }  
```
</p>


### Python easy understand solution
- Author: formatmemory
- Creation Date: Wed Jul 24 2019 15:36:36 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 24 2019 15:36:36 GMT+0800 (Singapore Standard Time)

<p>
The key is to determin the target year is leap year or not. For those who are not familliar with the concept of leap year: https://en.wikipedia.org/wiki/Leap_year. 
So there is one more day in February for leap year. 

```
    def numberOfDays(self, Y: int, M: int) -> int:
        up = [1,3,5,7,8,10,12]
        
        if M < 1 or M > 12:
            return 0
        else:
            if M == 2:
                return 29 if self.is_leap_year(Y) else 28
            else:
                return 31 if M in up else 30

    def is_leap_year(self, y):
		"""
        Leap Year:
            1.The year can be evenly divided by 4;
            2.If the year can be evenly divided by 100, it is NOT a leap year, unless;
            3.The year is also evenly divisible by 400. Then it is a leap year.
		"""
        if y%4:
            return False
        elif y%100 != 0:
            return True
        elif y%400 != 0:
            return False
        else:
            return True
```

Analysis: 
	time: O(1), space: O(1)
</p>


### Python 3 self-explained solution
- Author: ye15
- Creation Date: Wed Oct 30 2019 10:54:30 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 30 2019 10:54:30 GMT+0800 (Singapore Standard Time)

<p>
The algorithm to determine leap year is below:
* if (year is divisible by 4) and (year is not divisible by 100) then (it is a leap year);
* if (year is divisible by 400) then (it is a leap year).

```
class Solution:
    def numberOfDays(self, Y: int, M: int) -> int:
        if M in {1,3,5,7,8,10,12}: return 31
        elif M != 2: return 30
        else: return 28 + (Y % 4 == 0 and Y % 100 != 0 or Y % 400 == 0)
```
</p>


