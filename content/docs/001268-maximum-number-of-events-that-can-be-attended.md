---
title: "Maximum Number of Events That Can Be Attended"
weight: 1268
#id: "maximum-number-of-events-that-can-be-attended"
---
## Description
<div class="description">
<p>Given an array of <code>events</code> where <code>events[i] = [startDay<sub>i</sub>, endDay<sub>i</sub>]</code>. Every event <code>i</code> starts at&nbsp;<code>startDay<sub>i</sub></code><sub>&nbsp;</sub>and ends at&nbsp;<code>endDay<sub>i</sub></code>.</p>

<p>You can attend an event <code>i</code>&nbsp;at any day&nbsp;<code>d</code> where&nbsp;<code>startTime<sub>i</sub>&nbsp;&lt;= d &lt;= endTime<sub>i</sub></code>. Notice that you can only attend one event at any time <code>d</code>.</p>

<p>Return <em>the maximum number of events&nbsp;</em>you can attend.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/02/05/e1.png" style="width: 660px; height: 440px;" />
<pre>
<strong>Input:</strong> events = [[1,2],[2,3],[3,4]]
<strong>Output:</strong> 3
<strong>Explanation:</strong> You can attend all the three events.
One way to attend them all is as shown.
Attend the first event on day 1.
Attend the second event on day 2.
Attend the third event on day 3.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> events= [[1,2],[2,3],[3,4],[1,2]]
<strong>Output:</strong> 4
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> events = [[1,4],[4,4],[2,2],[3,4],[1,1]]
<strong>Output:</strong> 4
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> events = [[1,100000]]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> events = [[1,1],[1,2],[1,3],[1,4],[1,5],[1,6],[1,7]]
<strong>Output:</strong> 7
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= events.length &lt;= 10<sup>5</sup></code></li>
	<li><code>events[i].length == 2</code></li>
	<li><code>1 &lt;= startDay<sub>i</sub> &lt;= endDay<sub>i</sub> &lt;= 10<sup>5</sup></code></li>
</ul>

</div>

## Tags
- Greedy (greedy)
- Sort (sort)
- Segment Tree (segment-tree)

## Companies
- Visa - 3 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Priority Queue
- Author: lee215
- Creation Date: Sun Feb 16 2020 12:04:32 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 15 2020 19:09:50 GMT+0800 (Singapore Standard Time)

<p>
# Solution 1
Sort events increased by start time.
Priority queue `pq` keeps the current open events.

Iterate from the day 1 to day 100000,
Each day, we add new events starting on day `d` to the queue `pq`.
Also we remove the events that are already closed.

Then we greedily attend the event that ends soonest.
If we can attend a meeting, we increment the result `res`.
<br>

# **Complexity**
Time `O(d + nlogn)`, where `D` is the range of `A[i][1]`
Space `O(N)`
<br>

**Java:**
```java
    public int maxEvents(int[][] A) {
        PriorityQueue<Integer> pq = new PriorityQueue<Integer>();
        Arrays.sort(A, (a, b) -> Integer.compare(a[0], b[0]));
        int i = 0, res = 0, n = A.length;
        for (int d = 1; d <= 100000; ++d) {
            while (!pq.isEmpty() && pq.peek() < d)
                pq.poll();
            while (i < n && A[i][0] == d)
                pq.offer(A[i++][1]);
            if (!pq.isEmpty()) {
                pq.poll();
                ++res;
            }
        }
        return res;
    }
```

**C++:**
```cpp
    int maxEvents(vector<vector<int>>& A) {
        priority_queue <int, vector<int>, greater<int>> pq;
        sort(A.begin(), A.end());
        int i = 0, res = 0, n = A.size();
        for (int d = 1; d <= 100000; ++d) {
            while (pq.size() && pq.top() < d)
                pq.pop();
            while (i < n && A[i][0] == d)
                pq.push(A[i++][1]);
            if (pq.size()) {
                pq.pop();
                ++res;
            }
        }
        return res;
    }
```
<br>

# Solution 2
Same idea, though people worry about the time complexity of iteration all days.
We can easily improve it to strict `O(nlogn)`

**Java:**
```java
    public int maxEvents(int[][] A) {
        PriorityQueue<Integer> pq = new PriorityQueue<Integer>();
        Arrays.sort(A, (a, b) -> Integer.compare(a[0], b[0]));
        int i = 0, res = 0, d = 0, n = A.length;
        while (!pq.isEmpty() || i < n) {
            if (pq.isEmpty())
                d = A[i][0];
            while (i < n && A[i][0] <= d)
                pq.offer(A[i++][1]);
            pq.poll();
            ++res; ++d;
            while (!pq.isEmpty() && pq.peek() < d)
                pq.poll();
        }
        return res;
    }
```

**C++:**
```cpp
    int maxEvents(vector<vector<int>>& A) {
        priority_queue <int, vector<int>, greater<int>> pq;
        sort(A.begin(), A.end());
        int i = 0, res = 0, d = 0, n = A.size();
        while (pq.size() > 0 || i < n) {
            if (pq.size() == 0)
                d = A[i][0];
            while (i < n && A[i][0] <= d)
                pq.push(A[i++][1]);
            pq.pop();
            ++res, ++d;
            while (pq.size() > 0 && pq.top() < d)
                pq.pop();

        }
        return res;
    }
```
**Python:**
```python
    def maxEvents(self, A):
        A.sort(reverse=1)
        h = []
        res = d = 0
        while A or h:
            if not h: d = A[-1][0]
            while A and A[-1][0] <= d:
                heapq.heappush(h, A.pop()[1])
            heapq.heappop(h)
            res += 1
            d += 1
            while h and h[0] < d:
                heapq.heappop(h)
        return res
```

</p>


### 【Detailed analysis】Let me lead you to the solution step by step
- Author: kaiwensun
- Creation Date: Sun Feb 16 2020 12:04:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 16 2020 17:24:36 GMT+0800 (Singapore Standard Time)

<p>
## **Where to start your thinking**

Always keep in mind: interviewers rarely expect you to invent new algorithms. They almost always test your understanding and skills to apply algorithms you\'ve learned at school.
So, what algorithms have you learned at schools that are usually used to solve questions involving an array? DP, search/sort, divide and conquer, greedy.... Hmm... this question reminds me of the question about scheduling meetings with limited meeting rooms, which is solved by greedy algorithm. Even if you don\'t know the scheduling meeting question, you can swiftly attempt with DP and divide-and-conquer, and will find it is not very straight forward to define the subproblem of DB, or to find the split point of divide-and-conquer. Hmm... so greedy algorithm looks like the right one. Let\'s try that.

---

## **Greedy algorithm intuition**

Greedy algorithms are usually very intuitive (but not necessarily correct. it requires proof). What would you do, if you have multiple equally important meetings to run, but can only make some of them? Most people probably would choose to go to the one that is going to end soon. And after that meeting, pick the next meeting from those that are still available.

---

## **Greedy algorithm proof**

At some day, suppose both events `E1` and `E2` are available to choose to attend. For contradictory purpose, suppose the event `E1` that is going to end sooner is not the best choice for now. Instead, `E2` that ends later is the best choice for now. By choosing `E2` now, you come up with a schedule `S1`.

I claim that I can always construct another schedule `S2` in which we choose `E1` instead of `E2` for now, and `S2` is not worse than `S1`.
In `S1`, from now on, if `E1` is picked some time after, then I can always swap `E1` and `E2` in `S1`, so I construct a `S2` which is not worse than `S1`.
In `S1`, from now on, if `E1` is **not** picked some time after, then I can aways replace `E2` in `S1` with `E1`, so I construct a `S2` which is not worse than `S1`.

So it is always better (at least not worse) to always choose the event that ends sooner.

---

## **Greedy algorithm implementation**

As we go through each days to figure out the availability of each events, it is very intuitive to first sort the `events` by the starting day of the events. Then the question is, how to find out which (still available) event ends the earliest? It seems that we need to sort the **currently available** events according to the ending day of the events. How to do that? Again, the interviewers don\'t expect you to invent something realy new! What data structures / algorithm have you learned that can efficiently keep track of the biggest value, while you can dynamically add and remove elements? ...... Yes! Binary search/insert and min/max heap! Obviously, heap is more efficient than binary search, because adding/removing an elements after doing binary search can potentionally cause linear time complexity.

---

## **Python code**

```
import heapq
class Solution(object):
    def maxEvents(self, events):
        # sort according to start time
        events = sorted(events)
        total_days = max(event[1] for event in events)
        min_heap = []
        day, cnt, event_id = 1, 0, 0
        while day <= total_days:
		    # if no events are available to attend today, let time flies to the next available event.
            if event_id < len(events) and not min_heap:
                day = events[event_id][0]
			
			# all events starting from today are newly available. add them to the heap.
            while event_id < len(events) and events[event_id][0] <= day:
                heapq.heappush(min_heap, events[event_id][1])
                event_id += 1

			# if the event at heap top already ended, then discard it.
            while min_heap and min_heap[0] < day:
                heapq.heappop(min_heap)

			# attend the event that will end the earliest
            if min_heap:
                heapq.heappop(min_heap)
                cnt += 1
			elif event_id >= len(events):
                break  # no more events to attend. so stop early to save time.
            day += 1
        return cnt
```
</p>


### Java nlgn solution
- Author: wangjian4814
- Creation Date: Sun Feb 16 2020 12:01:17 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 17 2020 00:06:05 GMT+0800 (Singapore Standard Time)

<p>
Thank you guys for correcting my time complexity, I was thinking it was impossible an event can last n days....
However, it should be O(n * m) at worst case, here m is the largest (endDay - startDay + 1) among all events.
So, we can just think my solution\'s time complexity is O(n^2).

```
class Solution {
    public int maxEvents(int[][] events) {
        
        // greedy thinking: sort the events based on finish time, then grab the first finish event to do in 1 day
        // if there are multiple days can be used, use the earliest.
        // forgive my english
        
        Arrays.sort(events, (a, b) -> a[1] != b[1]? a[1] - b[1]: a[0] - b[0]);
        
        Set<Integer> hset = new HashSet<>();
        
        for(int[] e: events) {
            if(e[1] == e[0]) {
                hset.add(e[0]);
            } else {
                // if there are many days can be used grab the earliest day
                for(int i = e[0]; i <= e[1]; i++) {
                    if(!hset.contains(i)) {
                        hset.add(i);
                        break;
                    }
                }
            }
        }
        return hset.size();
    }
}
</p>


