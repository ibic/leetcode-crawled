---
title: "Word Search"
weight: 79
#id: "word-search"
---
## Description
<div class="description">
<p>Given a 2D board and a word, find if the word exists in the grid.</p>

<p>The word can be constructed from letters of sequentially adjacent cell, where &quot;adjacent&quot; cells are those horizontally or vertically neighboring. The same letter cell may not be used more than once.</p>

<p><strong>Example:</strong></p>

<pre>
board =
[
  [&#39;A&#39;,&#39;B&#39;,&#39;C&#39;,&#39;E&#39;],
  [&#39;S&#39;,&#39;F&#39;,&#39;C&#39;,&#39;S&#39;],
  [&#39;A&#39;,&#39;D&#39;,&#39;E&#39;,&#39;E&#39;]
]

Given word = &quot;<strong>ABCCED</strong>&quot;, return <strong>true</strong>.
Given word = &quot;<strong>SEE</strong>&quot;, return <strong>true</strong>.
Given word = &quot;<strong>ABCB</strong>&quot;, return <strong>false</strong>.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>board</code>&nbsp;and <code>word</code> consists only of lowercase and uppercase English letters.</li>
	<li><code>1 &lt;= board.length &lt;= 200</code></li>
	<li><code>1 &lt;= board[i].length &lt;= 200</code></li>
	<li><code>1 &lt;= word.length &lt;= 10^3</code></li>
</ul>

</div>

## Tags
- Array (array)
- Backtracking (backtracking)

## Companies
- Amazon - 22 (taggedByAdmin: false)
- Bloomberg - 18 (taggedByAdmin: true)
- ByteDance - 9 (taggedByAdmin: false)
- Cisco - 7 (taggedByAdmin: false)
- Microsoft - 6 (taggedByAdmin: true)
- Intuit - 5 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: true)
- Apple - 3 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Cruise Automation - 2 (taggedByAdmin: false)
- Coupang - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- Pinterest - 3 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Docusign - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: false)
- Zillow - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- Houzz - 2 (taggedByAdmin: false)
- Quantcast - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Backtracking

**Intuition**

This problem is yet another 2D grid traversal problem, which is similar with another problem called [489. Robot Room Cleaner](https://leetcode.com/articles/robot-room-cleaner/).

Many people in the [discussion forum](https://leetcode.com/problems/word-search/discuss/) claimed that the solution is of _**DFS**_ (Depth-First Search). Although it is true that we would explore the 2D grid with the DFS strategy for this problem, it does not capture the entire nature of the solution.

>We argue that a more accurate term to summarize the solution would be _**backtracking**_, which is a methodology where we mark the current path of exploration, if the path does not lead to a solution, we then revert the change (_i.e._ backtracking) and try another path.

As the general idea for the solution, we would walk around the 2D grid, at each step we _mark_ our choice before jumping into the next step. And at the end of each step, we would also revert our marking, so that we could have a _clean slate_ to try another _direction_. In addition, the exploration is done via the _DFS_ strategy, where we go as further as possible before we try the next direction.

![pic](../Figures/79/79_example.png)

**Algorithm**

There is a certain code pattern for all the algorithms of backtracking. For example, one can find one template in our [Explore card of Recursion II](https://leetcode.com/explore/learn/card/recursion-ii/472/backtracking/2793/).

The skeleton of the algorithm is a loop that iterates through each cell in the grid. For each cell, we invoke the _backtracking_ function (_i.e._ `backtrack()`) to check if we would obtain a solution, starting from this very cell.

For the backtracking function `backtrack(row, col, suffix)`, as a DFS algorithm, it is often implemented as a _recursive_ function. The function can be broke down into the following four steps:

- Step 1). At the beginning, first we check if we reach the bottom case of the recursion, where the word to be matched is empty, _i.e._ we have already found the match for each prefix of the word.

- Step 2). We then check if the current state is invalid, either the position of the cell is out of the boundary of the board or the letter in the current cell does not match with the first letter of the word.

- Step 3). If the current step is valid, we then start the exploration of backtracking with the strategy of DFS. First, we mark the current cell as _visited_, _e.g._ any non-alphabetic letter will do. Then we iterate through the four possible directions, namely _up_, _right_, _down_ and _left_. The order of the directions can be altered, to one's preference.

- Step 4). At the end of the exploration, we revert the cell back to its original state. Finally we return the result of the exploration.

We demonstrate how it works with an example in the following animation.

!?!../Documents/79_LIS.json:1000,476!?!

<iframe src="https://leetcode.com/playground/UKfoouJt/shared" frameBorder="0" width="100%" height="500" name="UKfoouJt"></iframe>


**Notes**

There are a few choices that we made for our backtracking algorithm, here we elaborate some thoughts that are behind those choices.

>Instead of returning directly once we find a match, we simply _break_ out of the loop and do the cleanup before returning.

Here is what the alternative solution might look like.

<iframe src="https://leetcode.com/playground/RwThKoWG/shared" frameBorder="0" width="100%" height="500" name="RwThKoWG"></iframe>

As once notices, we simply `return True` if the result of recursive call to `backtrack()` is positive. Though this minor modification would have no impact on the time or space complexity, it would however leave with some "side-effect", _i.e._ the matched letters in the original board would be altered to `#`.

>Instead of doing the boundary checks before the recursive call on the `backtrack()` function, we do it within the function.

This is an important choice though. Doing the boundary check within the function would allow us to reach the bottom case, for the test case where the board contains only a single cell, since either of neighbor indices would not be valid.

**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(N \cdot 3 ^ L)$$ where $$N$$ is the number of cells in the board and $$L$$ is the length of the word to be matched.

    - For the backtracking function, initially we could have at most 4 directions to explore, but further the choices are reduced into 3 (since we won't go back to where we come from).
    As a result, the execution trace after the first step could be visualized as a 3-ary tree, each of the branches represent a potential exploration in the corresponding direction. Therefore, in the worst case, the total number of invocation would be the number of nodes in a full 3-nary tree, which is about $$3^L$$.

    - We iterate through the board for backtracking, _i.e._ there could be $$N$$ times invocation for the backtracking function in the worst case.

    - As a result, overall the time complexity of the algorithm would be $$\mathcal{O}(N \cdot 3 ^ L)$$.

- Space Complexity: $$\mathcal{O}(L)$$ where $$L$$ is the length of the word to be matched.

    - The main consumption of the memory lies in the recursion call of the backtracking function. The maximum length of the call stack would be the length of the word. Therefore, the space complexity of the algorithm is $$\mathcal{O}(L)$$.
<br/>
<br/>

## Accepted Submission (python)
```python
class Solution(object):
    def getNexts(self, board, width, height, sol, lens, ch):
        if lens == 0:
            return self.starts
        x, y = sol[-1]
        neighbours = [(x-1,y), (x,y-1), (x+1,y), (x,y+1)]
        points = filter(
            lambda e: e[0] >= 0 and e[0] < width and e[1] >= 0 and e[1] < height,
            neighbours)
        return filter(lambda e: board[e[1]][e[0]] == ch and (e[0], e[1]) not in sol, points)
            
    def bt(self, board, width, height, sol, word, lenw):
        #print('--', sol)
        lens = len(sol)
        if lens == lenw:
            return True
        nexts = self.getNexts(board, width, height, sol, lens, word[lens])
        for n in nexts:
            sol.append(n)
            if self.bt(board, width, height, sol, word, lenw):
                return True
            sol.pop()
        return False

    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """
        sol = []
        lenw = len(word)
        height = len(board)
        if height == 0:
            return lenw == 0
        width = len(board[0])
        if width == 0:
            return lenw == 0
        if lenw == 0:
            return width == 0 or height == 0
        self.starts = []
        for y in range(height):
            for x in range(width):
                if board[y][x] == word[0]:
                    self.starts.append((x,y))
        return self.bt(board, width, height, sol, word, lenw)
```

## Top Discussions
### Accepted very short Java solution. No additional space.
- Author: pavel-shlyk
- Creation Date: Mon Jan 26 2015 01:11:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 18 2019 13:46:31 GMT+0800 (Singapore Standard Time)

<p>
Here accepted solution based on recursion. To save memory I decuded to apply bit mask for every visited cell. Please check board[y][x] ^= 256;

    public boolean exist(char[][] board, String word) {
        char[] w = word.toCharArray();
        for (int y=0; y<board.length; y++) {
        	for (int x=0; x<board[y].length; x++) {
        		if (exist(board, y, x, w, 0)) return true;
        	}
        }
        return false;
    }
	
	private boolean exist(char[][] board, int y, int x, char[] word, int i) {
		if (i == word.length) return true;
		if (y<0 || x<0 || y == board.length || x == board[y].length) return false;
		if (board[y][x] != word[i]) return false;
		board[y][x] ^= 256;
		boolean exist = exist(board, y, x+1, word, i+1)
			|| exist(board, y, x-1, word, i+1)
			|| exist(board, y+1, x, word, i+1)
			|| exist(board, y-1, x, word, i+1);
		board[y][x] ^= 256;
		return exist;
	}
</p>


### Python dfs solution with comments.
- Author: OldCodingFarmer
- Creation Date: Sat Aug 29 2015 04:39:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 14:28:58 GMT+0800 (Singapore Standard Time)

<p>
        
    def exist(self, board, word):
        if not board:
            return False
        for i in xrange(len(board)):
            for j in xrange(len(board[0])):
                if self.dfs(board, i, j, word):
                    return True
        return False
    
    # check whether can find word, start at (i,j) position    
    def dfs(self, board, i, j, word):
        if len(word) == 0: # all the characters are checked
            return True
        if i<0 or i>=len(board) or j<0 or j>=len(board[0]) or word[0]!=board[i][j]:
            return False
        tmp = board[i][j]  # first character is found, check the remaining part
        board[i][j] = "#"  # avoid visit agian 
        # check whether can find "word" along one direction
        res = self.dfs(board, i+1, j, word[1:]) or self.dfs(board, i-1, j, word[1:]) \
        or self.dfs(board, i, j+1, word[1:]) or self.dfs(board, i, j-1, word[1:])
        board[i][j] = tmp
        return res
</p>


### My Java solution
- Author: harish-v
- Creation Date: Wed Aug 12 2015 03:39:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 12:54:53 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        static boolean[][] visited;
        public boolean exist(char[][] board, String word) {
            visited = new boolean[board.length][board[0].length];
            
            for(int i = 0; i < board.length; i++){
                for(int j = 0; j < board[i].length; j++){
                    if((word.charAt(0) == board[i][j]) && search(board, word, i, j, 0)){
                        return true;
                    }
                }
            }
            
            return false;
        }
        
        private boolean search(char[][]board, String word, int i, int j, int index){
            if(index == word.length()){
                return true;
            }
            
            if(i >= board.length || i < 0 || j >= board[i].length || j < 0 || board[i][j] != word.charAt(index) || visited[i][j]){
                return false;
            }
            
            visited[i][j] = true;
            if(search(board, word, i-1, j, index+1) || 
               search(board, word, i+1, j, index+1) ||
               search(board, word, i, j-1, index+1) || 
               search(board, word, i, j+1, index+1)){
                return true;
            }
            
            visited[i][j] = false;
            return false;
        }
    }
</p>


