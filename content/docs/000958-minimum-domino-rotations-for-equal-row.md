---
title: "Minimum Domino Rotations For Equal Row"
weight: 958
#id: "minimum-domino-rotations-for-equal-row"
---
## Description
<div class="description">
<p>In a row of dominoes, <code>A[i]</code> and <code>B[i]</code> represent the top and bottom halves of the <code>i</code>-th domino.&nbsp; (A domino is a tile with two numbers from 1 to 6 - one on each half of the tile.)</p>

<p>We may rotate the <code>i</code>-th domino, so that <code>A[i]</code> and <code>B[i]</code> swap values.</p>

<p>Return the minimum number of rotations so that all the values in <code>A</code> are the same, or all the values in <code>B</code>&nbsp;are the same.</p>

<p>If it cannot be done, return <code>-1</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/03/08/domino.png" style="height: 161px; width: 200px;" /></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[2,1,2,4,2,2]</span>, B = <span id="example-input-1-2">[5,2,6,2,3,2]</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>
The first figure represents the dominoes as given by A and B: before we do any rotations.
If we rotate the second and fourth dominoes, we can make every value in the top row equal to 2, as indicated by the second figure.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">[3,5,1,2,3]</span>, B = <span id="example-input-2-2">[3,6,3,3,4]</span>
<strong>Output: </strong><span id="example-output-2">-1</span>
<strong>Explanation: </strong>
In this case, it is not possible to rotate the dominoes to make one row of values equal.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A[i], B[i] &lt;= 6</code></li>
	<li><code>2 &lt;= A.length == B.length &lt;= 20000</code></li>
</ol>

</div>

## Tags
- Array (array)
- Greedy (greedy)

## Companies
- Google - 3 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Greedy.

**Intuition**

Let's pick up an arbitrary `i`th domino element in the configuration. 
The element has two sides, `A[i]` is an upper side 
and `B[i]` is a lower side.

![bla](../Figures/1007/config.png)    

There could be three possible situations here

1 . One could make all elements of `A` row or `B` row 
to be the same and equal to `A[i]` value. 
For example, if one picks up the `0`th element, 
it's possible to make all elements of `A` row to be equal to `2`.

![bla](../Figures/1007/s1.png) 

2 . One could make all elements of `A` row or `B` row 
to be the same and equal to `B[i]` value. 
For example, if one picks up the `1`th element, 
it's possible to make all elements of `B` row to be equal to `2`.

![bla](../Figures/1007/s2.png)

3 . It's impossible to make all elements of `A` row or `B` row 
to have the same `A[i]` or `B[i]` value. 

![bla](../Figures/1007/s3.png) 

> The third situation means that it's impossible to make all
elements in `A` row or `B` row to be equal. 

Yes, the only one domino element was checked here, and still it's
enough because the rotation is the only allowed operation here.

**Algorithm**

- Pick up the first element. It has two sides: `A[0]` and `B[0]`.

- Check if one could make all elements in `A` row or `B` row 
to be equal to `A[0]`. 
If yes, return the minimum number of rotations needed.

- Check if one could make all elements in `A` row or `B` row 
to be equal to `B[0]`.
If yes, return the minimum number of rotations needed.

- Otherwise return `-1`.


**Implementation**

<iframe src="https://leetcode.com/playground/bQ3se2Rp/shared" frameBorder="0" width="100%" height="500" name="bQ3se2Rp"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since here one iterates over the
arrays not more than two times.
 
* Space complexity : $$\mathcal{O}(1)$$ since it's a constant 
space solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Different Ideas
- Author: lee215
- Creation Date: Sun Mar 10 2019 12:04:35 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Mar 04 2020 10:46:06 GMT+0800 (Singapore Standard Time)

<p>
## Intuition
One observation is that, if `A[0]` works, no need to check `B[0]`.
Because if both `A[0]` and `B[0]` exist in all dominoes,
when you swap `A[0]` in a whole row,
you will swap  `B[0]` in a whole at the same time.
The result of trying  `A[0]` and `B[0]` will be the same.
<br>

## Solution 1:

Count the occurrence of all numbers in `A` and `B`,
and also the number of domino with two same numbers.

Try all possibilities from 1 to 6.
If we can make number `i` in a whole row,
it should satisfy that `countA[i] + countB[i] - same[i] = n`

Take example of
`A = [2,1,2,4,2,2]`
`B = [5,2,6,2,3,2]`

`countA[2] = 4`, as `A[0] = A[2] = A[4] = A[5] = 2`
`countB[2] = 3`, as `B[1] = B[3] = B[5] = 2`
`same[2] = 1`, as `A[5] = B[5] = 2`

We have `countA[2] + countB[2] - same[2] =  6`,
so we can make 2 in a whole row.

Time `O(N)`, Space `O(1)`
<br>

**Java**
```java
    public int minDominoRotations(int[] A, int[] B) {
        int[] countA = new int[7], countB = new int[7], same = new int[7];
        int n = A.length;
        for (int i = 0; i < n; ++i) {
            countA[A[i]]++;
            countB[B[i]]++;
            if (A[i] == B[i])
                same[A[i]]++;
        }
        for (int i  = 1; i < 7; ++i)
            if (countA[i] + countB[i] - same[i] == n)
                return n - Math.max(countA[i], countB[i]);
        return -1;
    }
```
**C++**
```cpp
    int minDominoRotations(vector<int>& A, vector<int>& B) {
        vector<int> countA(7), countB(7), same(7);
        int n = A.size();
        for (int i = 0; i < n; ++i) {
            countA[A[i]]++;
            countB[B[i]]++;
            if (A[i] == B[i])
                same[A[i]]++;
        }
        for (int i  = 1; i < 7; ++i)
            if (countA[i] + countB[i] - same[i] == n)
                return n - max(countA[i], countB[i]);
        return -1;
    }
```
**python**
```python
    def minDominoRotations(self, A, B):
        for x in [A[0],B[0]]:
            if all(x in d for d in zip(A, B)):
                return len(A) - max(A.count(x), B.count(x))
        return -1
```
<br>

## Solution 2
1. Try make `A[0]` in a whole row,
the condition is that `A[i] == A[0] || B[i] == A[0]`
`a` and `b` are the number of swap to make a whole row `A[0]`

2. Try `B[0]`
the condition is that `A[i] == B[0] || B[i] == B[0]`
`a` and `b` are the number of swap to make a whole row `B[0]`

3. Return -1

**Java**
```java
    public int minDominoRotations(int[] A, int[] B) {
        int n = A.length;
        for (int i = 0, a = 0, b = 0; i < n && (A[i] == A[0] || B[i] == A[0]); ++i) {
            if (A[i] != A[0]) a++;
            if (B[i] != A[0]) b++;
            if (i == n - 1) return Math.min(a, b);
        }
        for (int i = 0, a = 0, b = 0; i < n && (A[i] == B[0] || B[i] == B[0]); ++i) {
            if (A[i] != B[0]) a++;
            if (B[i] != B[0]) b++;
            if (i == n - 1) return Math.min(a, b);
        }
        return -1;
    }
```

**C++**
```cpp
    int minDominoRotations(vector<int>& A, vector<int>& B) {
        int n = A.size();
        for (int i = 0, a = 0, b = 0; i < n && (A[i] == A[0] || B[i] == A[0]); ++i) {
            if (A[i] != A[0]) a++;
            if (B[i] != A[0]) b++;
            if (i == n - 1) return min(a, b);
        }
        for (int i = 0, a = 0, b = 0; i < n && (A[i] == B[0] || B[i] == B[0]); ++i) {
            if (A[i] != B[0]) a++;
            if (B[i] != B[0]) b++;
            if (i == n - 1) return min(a, b);
        }
        return -1;
    }
```
<br>

## Solution 3
Find intersection set `s` of all `{A[i], B[i]}`
`s.size = 0`, no possible result.
`s.size = 1`, one and only one result.
`s.size = 2`, it means all dominoes are `[a,b]` or `[b,a]`, try either one.
`s.size > 2`, impossible.

**Java:**
```java
    public int minDominoRotations(int[] A, int[] B) {
        HashSet<Integer> s = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        int[] countA = new int[7], countB = new int[7];
        for (int i = 0; i < A.length; ++i) {
            s.retainAll(new HashSet<>(Arrays.asList(A[i], B[i])));
            countA[A[i]]++;
            countB[B[i]]++;
        }
        for (int i : s)
            return A.length - Math.max(countA[i], countB[i]);
        return -1;
    }
```

**Python:**
```py
    def minDominoRotations(self, A, B):
        s = reduce(set.__and__, (set(d) for d in zip(A, B)))
        if not s: return -1
        x = s.pop()
        return min(len(A) - A.count(x), len(B) - B.count(x))
```

</p>


### [Java/Python 3] one pass counting, O(A + B) w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Mar 10 2019 19:06:34 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 09 2020 11:50:04 GMT+0800 (Singapore Standard Time)

<p>
**Q & A:**

**Q1**: How do you know that `min(countA[i], countB[i]) - same[i]` is the smallest number?
**A1**: `countA[i] + countB[i] - same[i]` is like finding the union of two set `A` and set `B` <=> `A + B - (A & B)`. We have only 2 options of minimum rotations to make all `A` and `B` are respectively same:
rotate
1. `countA[i] - same[i]`;
2. `countB[i] - same[i]`;
Obviously, we should choose the smaller one: `min(countA[i] - same[i], countB[i] - same[i])`, which is `min(countA[i], countB[i]) - same[i]`.

**Q2**: The following code:
```
        for i in range(1, 7):
            if countA[i] + countB[i] - same[i] == len(A):
                return min(countA[i], countB[i]) - same[i]  
```
means that when we found i = `p` that satisfy countA[p] + countB[p] - same[p] == len(A) then we use `p` to find the min(countA[p], countB[p]) - same[p]. But how about another i = `q` that satisfy countA[q] + countB[q] - same[q] == len(A)? Could it be smaller than the value corresponding to `p`?

**A2**: There are 2 cases, if 
1.) for all 1 <= i <= 6, same[i] == 0, then 
countA[p] + countB[p] == len(A),
Since 
sum(countA) + sum(countB) = 2 * len(A),
it is possible that there exists `q` such that 
`q != p` and countA[q] + countB[q] == len(A), in which case,
countA[p] == countB[q] and countA[q] == countB[p]
Therefore, min(countA[p], countB[p]) == min(CountA[q], countB[q])

2.) same[p] > 0, 
=> countA[p] + countB[p] > len(A), 
=> for any i != p, countA[i] + countB[i] - same[i] < countA[i] + countB[i] < len(A)
Therefore, 
min(countA[p], countB[p]) - same[p] is the only option.

**Q3:** Doesn\'t using Counter make the space complexity O(N) ?
`same, countA, countB = Counter(), Counter(A), Counter(B)`

And Time complexity O(A) since you checked the sizes here:
`if len(A) != len(B): return -1`
**A3:** It\'s actually `O(1)` space. There are at most `3 X 7 = 21` int used in Java code; and similarly in Py 3. Also, `len()` cost time `O(1)`.
**End of Q & A**

----
1. Count the frequency of each number in A and B, respectively; 
2. Count the frequency of A[i] if A[i] == B[i];
3. If countA[i] + countB[i] - same[i] == A.length, we find a solution; otherwise, return -1;
4. min(countA[i], countB[i]) - same[i] is the answer. `countA[i] + countB[i] - same[i]` is like finding the union of two set `A` and set `B` <=> `A + B - (A & B)` -- credit to **@StarInTheNight**


```
class Solution {
    public int minDominoRotations(int[] A, int[] B) {
        if (A.length != B.length) { return -1; }
        int[] countA = new int[7]; // countA[i] records the occurrence of i in A.
        int[] countB = new int[7]; // countB[i] records the occurrence of i in B.
        int[] same = new int[7]; // same[k] records the occurrence of k, where k == A[i] == B[i].
        for (int i = 0; i < A.length; ++i) {
            ++countA[A[i]];
            ++countB[B[i]];
            if (A[i] == B[i]) { ++same[A[i]]; }
        }
        for (int i = 1; i < 7; ++i) {
            if (countA[i] + countB[i] - same[i] >= A.length) {
                return Math.min(countA[i], countB[i]) - same[i];
            }
        }
        return -1;
    }
}
```
```
from collections import Counter

class Solution:
    def minDominoRotations(self, A: List[int], B: List[int]) -> int:
        if len(A) != len(B): return -1
        same, countA, countB = Counter(), Counter(A), Counter(B)
        for a, b in zip(A, B):
            if a == b:
                same[a] += 1
        for i in range(1, 7):
            if countA[i] + countB[i] - same[i] == len(A):
                return min(countA[i], countB[i]) - same[i]        
        return -1
```

**Analysis:**

Time: O(A + B), space: O(1).
</p>


### Thinking Process
- Author: GraceMeng
- Creation Date: Mon Mar 11 2019 02:29:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 23 2019 22:27:16 GMT+0800 (Singapore Standard Time)

<p>
> We find all possible number of rotations to make `all the values in A are the same, or all the values in B are the same`, and then get the minimum among them.

>  A[i] = A[i] if not swap OR B[i] if swap, B[i] = B[i] if not swap OR A[i] if swap.
When i = 0, A[0] can be either A[0] or B[0], B[0] can be either B[0] or A[0].
>  So the `value` must be either `A[0]` or `B[0]` if can be done.

> There are 4 possible cases:
> make values in A equal to A[0]
> make values in B equal to A[0]
> make values in A equal to B[0]
> make values in B equal to B[0]
> 
> For each case we count rotations and we get the min rotations among them.
****
```
    public int minDominoRotations(int[] A, int[] B) {
        int ans = min(
                f(A[0], A, B),
                f(B[0], A, B),
                f(A[0], B, A),
                f(B[0], B, A));
        return ans == Integer.MAX_VALUE ? -1 : ans;
    }
    
    private int min(int a, int b, int c, int d) {
        return Math.min(Math.min(Math.min(a, b), c), d);
    }

    /* Count number of rotations to make values in A equal to target. */
    private int f(int target, int[] A, int[] B) {
        int swap = 0;
        for (int i = 0; i < A.length; i++) {
            if (A[i] != target) {
                if (B[i] != target) {
                    return Integer.MAX_VALUE;
                } else {
                    swap++;
                }
            }
        }
        return swap;
    }
```

</p>


