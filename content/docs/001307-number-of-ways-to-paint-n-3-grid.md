---
title: "Number of Ways to Paint N × 3 Grid"
weight: 1307
#id: "number-of-ways-to-paint-n-3-grid"
---
## Description
<div class="description">
<p>You have a <code>grid</code> of size <code>n x 3</code> and you want to paint each cell of the grid with exactly&nbsp;one of the three colours: <strong>Red</strong>, <strong>Yellow</strong> or <strong>Green</strong>&nbsp;while making sure that no two adjacent cells have&nbsp;the same colour (i.e no two cells that share vertical or horizontal sides have the same colour).</p>

<p>You are given <code>n</code> the number of rows of the grid.</p>

<p>Return <em>the number of ways</em> you can paint this <code>grid</code>. As the answer may grow large, the answer <strong>must be</strong> computed modulo&nbsp;<code>10^9 + 7</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> 12
<strong>Explanation:</strong> There are 12 possible way to paint the grid as shown:
<img alt="" src="https://assets.leetcode.com/uploads/2020/03/26/e1.png" style="width: 450px; height: 289px;" />
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2
<strong>Output:</strong> 54
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 3
<strong>Output:</strong> 246
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 7
<strong>Output:</strong> 106494
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 5000
<strong>Output:</strong> 30228214
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == grid.length</code></li>
	<li><code>grid[i].length == 3</code></li>
	<li><code>1 &lt;= n &lt;= 5000</code></li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Akuna Capital - 5 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Paypal - 3 (taggedByAdmin: false)
- Hackerrank - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] DP, O(1) Space
- Author: lee215
- Creation Date: Sun Apr 12 2020 12:03:21 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 13 2020 01:37:18 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Classic dynamic programming problem.
<br>

# **Explanation**
Two pattern for each row, 121 and 123.
121, the first color same as the third in a row.
123, one row has three different colors.

We consider the state of the first row,
pattern 121: 121, 131, 212, 232, 313, 323.
pattern 123: 123, 132, 213, 231, 312, 321.
So we initialize `a121 = 6, a123 = 6`.

We consider the next possible for each pattern:
Patter 121 can be followed by: 212, 213, 232, 312, 313
Patter 123 can be followed by: 212, 231, 312, 232

121 => three 121, two 123
123 => two 121, two 123

So we can write this dynamic programming transform equation:
`b121 = a121 * 3 + a123 * 2`
`b123 = a121 * 2 + a123 * 2`

We calculate the result iteratively
and finally return the sum of both pattern `a121 + a123`
<br>

# Complexity
Time `O(N)`, Space `O(1)`
Of course, we can do better.
[O(logN) Time Solution](https://leetcode.com/problems/number-of-ways-to-paint-n-3-grid/discuss/575485/C++Python-O(logN)-Time)

Well, I don\'t think it\'s required,
But still you can mention during interview,
(as a ciper code hinting that you are my follower).
<br>

**Java:**
```java
    public int numOfWays(int n) {
        long a121 = 6, a123 = 6, b121, b123, mod = (long)1e9 + 7;
        for (int i = 1; i < n; ++i) {
            b121 = a121 * 3 + a123 * 2;
            b123 = a121 * 2 + a123 * 2;
            a121 = b121 % mod;
            a123 = b123 % mod;
        }
        return (int)((a121 + a123) % mod);
    }
```

**C++:**
```cpp
    int numOfWays(int n) {
        long a121 = 6, a123 = 6, b121, b123, mod = 1e9 + 7;
        for (int i = 1; i < n; ++i) {
            b121 = a121 * 3 + a123 * 2;
            b123 = a121 * 2 + a123 * 2;
            a121 = b121 % mod;
            a123 = b123 % mod;
        }
        return (a121 + a123) % mod;
    }
```

**Python:**
```py
    def numOfWays(self, n):
        a121, a123, mod = 6, 6, 10**9 + 7
        for i in xrange(n - 1):
            a121, a123 = a121 * 3 + a123 * 2, a121 * 2 + a123 * 2
        return (a121 + a123) % mod
```
<br>

</p>


### [Java] Detailed Explanation with Graph Demo - DP - Easy Understand
- Author: frankkkkk
- Creation Date: Sun Apr 12 2020 12:04:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 12 2020 14:09:07 GMT+0800 (Singapore Standard Time)

<p>
**Key Note**:
1. There are only two possibilities to form a non-adjacent row: **3 colors combination** (use all three colors, e.g., RYG) and **2 color combination** (use only two of three colors, e.g., RYR).
2. We add the new row one by one. Apart from its inner adjacent relation, **every new added row only relies on the previous one row** (new added row is only adjacent with the row above it).
3. **Every color combination follows the same pattern indicated below**. 3-color combination can generate **two** 3-color combination, and **two** 2-color combination for the next round. 2-color combination can generate **two** 3-color combination, and **three** 2-color combination for the next round.

![image](https://assets.leetcode.com/users/frankkkkk/image_1586662749.png)

Let\'s try to have a math eqution to show the change above. 
The number of 3-color combination for round n is: **S(n)**. The number of 2-color combination for round n is **T(n**).
Thus, we can have two simple math equations reflecting the relation between current round (n) with next round (n + 1) for both 3-color-combination and 2-color-combination.

**S(n + 1) = 2 * S(n) + 2 * T(n)**
**T(n + 1) = 2 * S(n) + 3 * T(n)**


```java
int MOD = (int) (1e9 + 7);
/*
init: for the single row, there are C(3, 3) * 3! = 6 3-color combinations, and C(3, 2) * 2 = 6 2-color combinations.
C(3, 3) * 3! means: choose 3 colors out of 3, and, first cell: 3 possibilities, second cell: 2 possibilities, third cell: only 1 possibility.
C(3, 2) * 2 means: choose 2 colors out of 3, and, there are only two ways: ABA or BAB.
*/
long color3 = 6;
long color2 = 6;

for (int i = 2; i <= n; ++i) {
	long tempColor3 = color3;
	color3 = (2 * color3 + 2 * color2) % MOD;
	color2 = (2 * tempColor3 + 3 * color2) % MOD;
}

return (int) (color3 + color2) % MOD;
```

</p>


### [Java/C++] DFS Memoization with Picture - Clean code
- Author: hiepit
- Creation Date: Sun Apr 12 2020 12:02:52 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 14 2020 00:10:38 GMT+0800 (Singapore Standard Time)

<p>
**Example Picture**
![image](https://assets.leetcode.com/users/hiepit/image_1586703072.png)

**Java**
```java
    public int numOfWays(int n) {
        int[][][][] dp = new int[n+1][4][4][4]; 
        return dfs(n, 0, 0, 0, dp);
    }
    // dfs(n, a0, b0, c0) is the number of ways to color the first n rows of the grid 
    //      keeping in mind that the previous row (n + 1) has colors a0, b0 and c0
    int dfs(int n, int a0, int b0, int c0, int[][][][] dp) {
        if (n == 0) return 1;
        if (dp[n][a0][b0][c0] != 0) return dp[n][a0][b0][c0];
        int ans = 0;
        int[] colors = new int[]{1, 2, 3}; // Red: 1, Yellow: 2, Green: 3
        for (int a : colors) {
            if (a == a0) continue; // Check if the same color with the below neighbor
            for (int b : colors) {
                if (b == b0 || b == a) continue; // Check if the same color with the below neighbor or the left neighbor
                for (int c : colors) {
                    if (c == c0 || c == b) continue; // Check if the same color with the below neighbor or the left neighbor
                    ans += dfs(n - 1, a, b, c, dp);
                    ans %= 1000_000_007;
                }
            }
        }
        return dp[n][a0][b0][c0] = ans;
    }
```
**C++**
```c++
    int dp[5001][4][4][4] = {};
    int numOfWays(int n) {
        return dfs(n, 0, 0, 0);
    }
    // dfs(n, a0, b0, c0) is the number of ways to color the first n rows of the grid 
    //      keeping in mind that the previous row (n + 1) has colors a0, b0 and c0
    int dfs(int n, int a0, int b0, int c0) {
        if (n == 0) return 1;
        if (dp[n][a0][b0][c0] != 0) return dp[n][a0][b0][c0];
        int ans = 0;
        vector<int> colors = {1, 2, 3}; // Red: 1, Yellow: 2, Green: 3
        for (int a : colors) {
            if (a == a0) continue; // Check if the same color with the below neighbor
            for (int b : colors) {
                if (b == b0 || b == a) continue; // Check if the same color with the below neighbor or the left neighbor
                for (int c : colors) {
                    if (c == c0 || c == b) continue; // Check if the same color with the below neighbor or the left neighbor
                    ans += dfs(n - 1, a, b, c);
                    ans %= 1000000007;
                }
            }
        }
        return dp[n][a0][b0][c0] = ans;
    }
```
**Complexity**
- Time: `O(n * 27^2)`, where `n` is the number of rows of the grid, `n <= 5000`
Explain: There are total `n*a0*b0*c0` = `n*3*3*3` states in `dfs(n,a0,b0,c0)` function (don\'t count a one-time call `dfs(n,0,0,0)`), each state needs maximum `3^3` (3 for loops) to calculate the result. So time complexity = `O(n * 3^3 * 3^3)` = `O(n * 27^2)`
- Space: `O(n * 4^3)`


Please help to **UPVOTE** if this post is useful for you. 
If you have any questions, feel free to comment below.
Happy coding!
</p>


