---
title: "Reported Posts"
weight: 1531
#id: "reported-posts"
---
## Description
<div class="description">
<p>Table: <code>Actions</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| user_id       | int     |
| post_id       | int     |
| action_date   | date    | 
| action        | enum    |
| extra         | varchar |
+---------------+---------+
There is no primary key for this table, it may have duplicate rows.
The action column is an ENUM type of (&#39;view&#39;, &#39;like&#39;, &#39;reaction&#39;, &#39;comment&#39;, &#39;report&#39;, &#39;share&#39;).
The extra column has optional information about the action such as a reason for report or a type of reaction. </pre>

<p>&nbsp;</p>

<p>Write an SQL query that reports the number of posts reported yesterday for each report reason. Assume today is <strong>2019-07-05</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
Actions table:
+---------+---------+-------------+--------+--------+
| user_id | post_id | action_date | action | extra  |
+---------+---------+-------------+--------+--------+
| 1       | 1       | 2019-07-01  | view   | null   |
| 1       | 1       | 2019-07-01  | like   | null   |
| 1       | 1       | 2019-07-01  | share  | null   |
| 2       | 4       | 2019-07-04  | view   | null   |
| 2       | 4       | 2019-07-04  | report | spam   |
| 3       | 4       | 2019-07-04  | view   | null   |
| 3       | 4       | 2019-07-04  | report | spam   |
| 4       | 3       | 2019-07-02  | view   | null   |
| 4       | 3       | 2019-07-02  | report | spam   |
| 5       | 2       | 2019-07-04  | view   | null   |
| 5       | 2       | 2019-07-04  | report | racism |
| 5       | 5       | 2019-07-04  | view   | null   |
| 5       | 5       | 2019-07-04  | report | racism |
+---------+---------+-------------+--------+--------+

Result table:
+---------------+--------------+
| report_reason | report_count |
+---------------+--------------+
| spam          | 1            |
| racism        | 2            |
+---------------+--------------+ 
Note that we only care about report reasons with non zero number of reports.</pre>

</div>

## Tags


## Companies
- Facebook - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simplest MySQL solution
- Author: shawlu
- Creation Date: Fri Aug 30 2019 14:15:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 30 2019 14:30:17 GMT+0800 (Singapore Standard Time)

<p>
Do not \u753B\u86C7\u6DFB\u8DB3:
* No need to filter `extra is not null`. When `action = \'report\'`, `extra` is guaranteed not null.
* No need to filter count > 0. If a group exists, it **must** have at least one row.

I\'m maintaining a GitHub repository that documents useful SQL techniques and common pitfalls in interview. Please star/fork if you find it helpful: https://github.com/shawlu95/Beyond-LeetCode-SQL

```
select 
  extra as report_reason
  ,count(distinct post_id) as report_count
from Actions
where action_date = \'2019-07-04\'
  and action = \'report\'
group by extra
```
</p>


### MySQL Solution
- Author: ellie7947
- Creation Date: Tue Jul 09 2019 09:05:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 09 2019 09:05:56 GMT+0800 (Singapore Standard Time)

<p>
SELECT extra AS report_reason, COUNT(DISTINCT post_id) AS report_count
FROM Actions
WHERE action = \'report\' AND action_date = \'2019-07-04\'
GROUP BY extra
</p>


### Elegant MySQL Solution
- Author: Chulong_Li
- Creation Date: Sun Aug 25 2019 02:24:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 25 2019 02:25:41 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT extra AS report_reason, COUNT(*) AS report_count
FROM (
    SELECT DISTINCT extra, post_id
    FROM Actions
    WHERE action=\'report\' AND DATEDIFF(\'2019-07-05\', action_date) = 1
    ) AS tab
GROUP BY extra;
```
</p>


