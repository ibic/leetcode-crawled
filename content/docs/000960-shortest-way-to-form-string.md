---
title: "Shortest Way to Form String"
weight: 960
#id: "shortest-way-to-form-string"
---
## Description
<div class="description">
<p>From any string, we can form a <i>subsequence</i> of that string by deleting some number of characters (possibly no deletions).</p>

<p>Given two strings <code>source</code> and <code>target</code>, return the minimum number of subsequences of <code>source</code> such that their concatenation equals <code>target</code>. If the task is impossible, return <code>-1</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>source = <span id="example-input-1-1">&quot;abc&quot;</span>, target = <span id="example-input-1-2">&quot;abcbc&quot;</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>The target &quot;abcbc&quot; can be formed by &quot;abc&quot; and &quot;bc&quot;, which are subsequences of source &quot;abc&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>source = <span id="example-input-2-1">&quot;abc&quot;</span>, target = <span id="example-input-2-2">&quot;acdbc&quot;</span>
<strong>Output: </strong><span id="example-output-2">-1</span>
<strong>Explanation: </strong>The target string cannot be constructed from the subsequences of source string due to the character &quot;d&quot; in target string.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>source = <span id="example-input-3-1">&quot;xyz&quot;</span>, target = <span id="example-input-3-2">&quot;xzyxz&quot;</span>
<strong>Output: </strong><span id="example-output-3">3</span>
<strong>Explanation: </strong>The target string can be constructed as follows &quot;xz&quot; + &quot;y&quot; + &quot;xz&quot;.
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>Both the <code>source</code> and <code>target</code> strings consist of only lowercase English letters from &quot;a&quot;-&quot;z&quot;.</li>
	<li>The lengths of <code>source</code> and <code>target</code> string are between <code>1</code> and <code>1000</code>.</li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Greedy (greedy)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Google - 10 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accept is not enough to get a hire. Interviewee 4 follow up
- Author: happyleetcode
- Creation Date: Tue Jul 09 2019 21:02:35 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 09 2019 21:02:35 GMT+0800 (Singapore Standard Time)

<p>
first opinion is that we can use two pointer , one iterate src, another iterate tar. 
for each tar char, we move j until src[j] == tar[i], if j == src.length, res++, j = 0;
in this solution, we greedy match as many chars from src to tar as possible which can lead mininum use of  src.
and we can build a set to save all the char in src, if there exists a char from tar which not exists in set, return -1.
```
public int shortestWay(String source, String target) {
	char[] cs = source.toCharArray(), ts = target.toCharArray();
	boolean[] map = new boolean[26];
	for (int i = 0; i < cs.length; i++) 
		map[cs[i] - \'a\'] = true;
	int j = 0, res = 1;
	for (int i = 0; i < ts.length; i++,j++) {
		if (!map[ts[i] - \'a\']) return -1;
		while (j < cs.length && cs[j] != ts[i]) {
			j++;
		}
		if (j == cs.length) {
			j = -1;
			res++;
			i--;
		}
	}
	return res;
}
```
### follow up 1: yes, correct. could u implement it with O 1 space, which mean without set.
okay. without set, we need a way to make sure there is a char which not in src. we can iterate src completely. if the j not move, then we can return -1.
```
public int shortestWay(String source, String target) {
	char[] cs = source.toCharArray(), ts = target.toCharArray();
	int res = 0;
	for (int i = 0; i < ts.length; ) {
		int oriI = i;
		for (int j = 0; j < cs.length; j++) {
			if (i < ts.length && cs[j] == ts[i])
				i++;
		}
		if (i == oriI) return -1;
		res++;
	}
	return res;
}
```
### follow up 2: fine. what\'s the time complexity for above solutions. O(MN). could u make it better?
the time complexity is better than O (MN), should be O(logM * N) or O (N)
to find a logM way, it is easy to think of binary search. for each char in tar, we need loop from j to end, to find a char same as tar[i].
we can build a map which key is from \'a\' -> \'z\', the value is idx for this char in src. because idx is add from small to big. when we iterate tar[i], we can easily to find the tar[i]\'s idx list. to search is there a idx is larger or equal than j+1. it is logM. and we have N char in tar, so the time complexity is N * logM
the time is to build the map is O(M);
```
public int shortestWay(String source, String target) {
	char[] cs = source.toCharArray(), ts = target.toCharArray();
	int res = 1;
	List<Integer>[] idx = new List[26];
	for (int i = 0; i < 26; i++) idx[i] = new ArrayList<>();
	for (int i = 0; i < cs.length; i++) idx[cs[i] - \'a\'].add(i);
	int j = 0;
	for (int i = 0; i < ts.length; ) {
		List<Integer> tar = idx[ts[i] - \'a\'];
		if (tar.isEmpty()) return -1;
		int k = Collections.binarySearch(tar,j);
		if (k < 0) k = -k - 1;
		if (k == tar.size()) {
			res++;
			j = 0;
		} else {
			j = tar.get(k) + 1;
			i++;
		}

	}
	return res;
}
```
### follow up 3: great.  could u improve it more?
so we have to think a solution which is O(N), how should we use O(1) to know the next J pos?
maybe we can use more to save time.
in binary search solution we will have a map like a ->{1,3,7,16} (total src length is 20), so we need binary search.
if we can flatten them, i mean for each pos in 20 length, we just save the next idx, we can use O 1 to find the next J.
a -> {1,1,3,3,7,7,7,7,16,16,16,16,16,16,16,16,16,0,0,0}
for example if now j is 4, we can just check map[4] = 7; we know 7 pos have an \'a\', so next j will be 7 + 1.
if now j is 17, we get map[17] = 0, we know there is no more j after. so j = 0, res++;
the time complexity is O (N) ,  and build the map cost 26 * M
```
public int shortestWay(String source, String target) {
	char[] cs = source.toCharArray(), ts = target.toCharArray();
	int[][] idx = new int[26][cs.length];
	for (int i = 0; i < cs.length; i++) idx[cs[i] - \'a\'][i] = i + 1;
	for (int i = 0; i < 26; i++) {
		for (int j = cs.length - 1, pre = 0; j >= 0; j--) {
			if (idx[i][j] == 0) idx[i][j] = pre;
			else pre = idx[i][j];
		}
	}
	int res = 1, j = 0;
	for (int i = 0; i < ts.length; i++) {
		if (j == cs.length) {
			j = 0;
			res++;
		}
		if (idx[ts[i] - \'a\'][0] == 0) return -1;
		j = idx[ts[i] - \'a\'][j];
		if (j == 0 ) {
			res++;
			i--;
		}
	}
	return  res;
}
```
### follow up 4: cool, if we assume which can copy a array to another array with 26 length in constant time. could u implement it with O(M + N)
it sounds like we need switch the map from [26][src.length] to [src.length][26].
and we also need to use O 1 time to know what\'s next j position.
now we are in the 2rd idx (j = 1), so tar[i] = \'a\' we should save the map[1][\'a\'] the next position of j.
if we are in the last idx, i think the map should be all 0, except the last tar char. for example the char is \'a\'
so the map for it is [last+1,0,0,...,0]
how about last -1 idx, if the tar[last - 1] is same as tar[last], we just update it , [last - 1 + 1, 0....0]
if is not same. we can update a 0 with last - 1 + 1
```
public int shortestWay(String source, String target) {
	char[] cs = source.toCharArray(), ts = target.toCharArray();
	int[][] idx = new int[cs.length][26];
	idx[cs.length - 1][cs[cs.length - 1] - \'a\'] = cs.length; 
	for (int i = cs.length - 2; i >= 0; i--) {
		idx[i] = Arrays.copyOf(idx[i + 1],26);
		idx[i][cs[i] - \'a\'] = i + 1; 
	}
	int j = 0, res = 1;
	for (int i = 0; i < ts.length; i++) {
		if (j == cs.length) {
			j = 0;
			res++;
		}
		j = idx[j][ts[i] - \'a\'];
		if (idx[0][ts[i] - \'a\'] == 0) return -1;
		if (j == 0) {
			res++;
			i--;
		}
	}
	return res;
}
```

</p>


### Python O(M + N*logM) using inverted index + binary search (Similar to LC 792)
- Author: Twohu
- Creation Date: Mon Jun 03 2019 13:53:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 30 2019 03:23:28 GMT+0800 (Singapore Standard Time)

<p>
This is a google phone screen question. It\'s the same idea as [792. Number of Matching Subsequences](https://leetcode.com/problems/number-of-matching-subsequences/description/). I recommend solving that question first.

The idea is to create an [inverted index](https://en.wikipedia.org/wiki/Inverted_index) that saves the offsets of where each character occurs in `source`. The index data structure is represented as a hashmap, where the Key is the character, and the Value is the (sorted) list of offsets where this character appears. To run the algorithm, for each character in `target`, use the index to get the list of possible offsets for this character. Then search this list for next offset which appears after the offset of the previous character. We can use binary search to efficiently search for the next offset in our index.

Example with `source` = "abcab", `target` = "aabbaac"
The inverted index data structure for this example would be:
inverted_index  = {
a: [0, 3]   # \'a\' appears at index 0, 3 in source
b: [1, 4],  # \'b\' appears at index 1, 4 in source
c: [2],      # \'c\' appears at index 2 in source
}
Initialize `i` = -1 (i represents the smallest valid next offset) and `loop_cnt` = 1 (number of passes through source). 
Iterate through the target string "aabbaac"
a => get the offsets of character \'a\' which is [0, 3]. Set `i` to 1.
a => get the offsets of character \'a\' which is [0, 3]. Set `i` to 4.
b => get the offsets of character \'b\' which is [1, 4]. Set `i` to 5.
b => get the offsets of character \'b\' which is [1, 4]. Increment `loop_cnt` to 2, and Set `i` to 2.
a => get the offsets of character \'a\' which is [0, 3]. Set `i` to 4.
a => get the offsets of character \'a\' which is [0, 3]. Increment `loop_cnt` to 3, and Set `i` to 1.
c => get the offsets of character \'c\' which is [2]. Set `i` to 3.
We\'re done iterating through target so return the number of loops (3).

The runtime is O(M) to build the index, and O(logM) for each query. There are N queries, so the total runtime is O(M + N\*logM). M is the length of source and N is the length of target. The space complexity is O(M), which is the space needed to store the index.

```
:::python
def shortestWay(self, source: str, target: str) -> int:
    inverted_index = collections.defaultdict(list)
    for i, ch in enumerate(source):
        inverted_index[ch].append(i)

    loop_cnt = 1
    i = -1
    for ch in target:
        if ch not in inverted_index:
            return -1
        offset_list_for_ch = inverted_index[ch]
        # bisect_left(A, x) returns the smallest index j s.t. A[j] >= x. If no such index j exists, it returns len(A).
        j = bisect.bisect_left(offset_list_for_ch, i)
        if j == len(offset_list_for_ch):
            loop_cnt += 1
            i = offset_list_for_ch[0] + 1
        else:
            i = offset_list_for_ch[j] + 1

    return loop_cnt
```

Closing thoughts: As usual, there are multiple solutions to this problem. One way other way to solve is  the naive O(M\*N) solution using two pointers. This solution won\'t be enough to pass a phone screen. There is also a dynamic programming O(|\u03A3|\*M + N) solution (where |\u03A3| is the size of the alphabet, or 26 in this question). xiangmo posted an [example solution here](https://leetcode.com/problems/shortest-way-to-form-string/discuss/309632/Java-O(M-+-N)-solution), which I encourage you to check out.
</p>


### Java Two Pointers Solution With Explanation
- Author: yuanb10
- Creation Date: Mon Jun 03 2019 00:56:52 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 03 2019 00:56:52 GMT+0800 (Singapore Standard Time)

<p>
We traverse the target string while matching source string multiple times. 
t: index for target string
s: index for source string
ans: results

Each time, we want to ensure that t index moves forward instead of staying steady. 

```
class Solution {
    public int shortestWay(String source, String target) {
        int t = 0;
        int ans = 0;
        while (t < target.length()) {
            int pt = t;
            
            for (int s = 0; s < source.length(); s++) {
                if (t < target.length() && source.charAt(s) == target.charAt(t)) {
                    t++;
                }
            }
            
            if (t == pt) {
                return -1;
            }
            ans++;
        }
        
        return ans;
    }
}
```
</p>


