---
title: "Second Degree Follower"
weight: 1505
#id: "second-degree-follower"
---
## Description
<div class="description">
<p>In facebook, there is a <code>follow</code> table with two columns: <b>followee</b>, <b>follower</b>.</p>

<p>Please write a sql query to get the amount of each follower&rsquo;s follower if he/she has one.</p>

<p>For example:</p>

<pre>
+-------------+------------+
| followee    | follower   |
+-------------+------------+
|     A       |     B      |
|     B       |     C      |
|     B       |     D      |
|     D       |     E      |
+-------------+------------+
</pre>
should output:

<pre>
+-------------+------------+
| follower    | num        |
+-------------+------------+
|     B       |  2         |
|     D       |  1         |
+-------------+------------+
</pre>
<b>Explaination:</b><br />
Both B and D exist in the follower list, when as a followee, B&#39;s follower is C and D, and D&#39;s follower is E. A does not exist in follower list.
<p>&nbsp;</p>

<p>&nbsp;</p>
<b>Note:</b><br />
Followee would not follow himself/herself in all cases.<br />
Please display the result in follower&#39;s alphabet order.
<p>&nbsp;</p>

</div>

## Tags


## Companies
- Facebook - 4 (taggedByAdmin: false)
- Databricks - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### The OJ has a bug! upper and lower case
- Author: zestypanda
- Creation Date: Wed Sep 06 2017 11:36:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 08:55:10 GMT+0800 (Singapore Standard Time)

<p>
The table and OJ code use upper and lower case, such as 'B' and 'b' interchangeably. But it requires specific upper or lower case in the output, to be consistent with follower column only!

Here is my answer to apply this approach.
```
select distinct follower, num
from follow, 
(select followee, count(distinct follower) as num from follow 
group by followee) as t
where follower = t.followee
order by follower;
```
</p>


### Accept without subquery
- Author: xiaxin
- Creation Date: Sat Jun 10 2017 11:13:13 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 18:24:05 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below
Select f1.follower, count(distinct f2.follower) as num
from follow f1
inner join follow f2 on f1.follower = f2.followee
Group by f1.follower
```
</p>


### AC code with Clear Explanation for all corner cases
- Author: LionKing
- Creation Date: Mon Oct 16 2017 01:19:44 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 18:24:32 GMT+0800 (Singapore Standard Time)

<p>
The first thing we should do is understand the question correctly.

in the example
```
+-------------+------------+
| followee    | follower   |
+-------------+------------+
|     A       |     B      |
|     B       |     C      |
|     B       |     D      |
|     D       |     E      |
+-------------+------------+

+-------------+------------+
| follower    | num        |
+-------------+------------+
|     B       |  2         |
|     D       |  1         |
+-------------+------------+
```
For A, B, D, they have 1, 2, 1 follower correspondingly.
While we don't output **A-1**  because A is not in the column follower, which means it is **not** a **Second Degree Follower**

So I came up with code like these(which is **WRONG**)
```
select f1.followee as follower, count(f1.follower) as num 
from follow f1 where f1.followee in (select distinct follower from follow) 
group by f1.followee 
order by follower;
```
The reason is that there may be duplicate followee-follower as input
```
+-------------+------------+
| followee    | follower   |
+-------------+------------+
|     A       |     B      |
|     B       |     C      |
|     B       |     C      |
|     D       |     E      |
+-------------+------------+
```
The code above output B-2 while we should output B-1
While it didn't pass when I change the first line into 
```
select f1.followee as follower, count(distinct f1.follower) as num 
```

After a while, I realize that there may be something wrong with the alphabet policy. All my output values were correct, but the followee may change from lower case to upper case (actually I have no idea and MySQL seems to be case insensitive)

So finally I try another answer credit to @xiaxin and get AC.
```
select f1.follower, count(distinct f2.follower) as num
from follow f1
join follow f2 on f1.follower = f2.followee
group by f1.follower
order by f1.follower;
```
</p>


