---
title: "Range Addition II"
weight: 547
#id: "range-addition-ii"
---
## Description
<div class="description">
<p>You are given an <code>m x n</code> matrix <code>M</code> initialized with all <code>0</code>&#39;s and an array of operations <code>ops</code>, where <code>ops[i] = [a<sub>i</sub>, b<sub>i</sub>]</code> means <code>M[x][y]</code> should be incremented by one for all <code>0 &lt;= x &lt; a<sub>i</sub></code> and <code>0 &lt;= y &lt; b<sub>i</sub></code>.</p>

<p>Count and return <em>the number of maximum integers in the matrix after performing all the operations</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/10/02/ex1.jpg" style="width: 750px; height: 176px;" />
<pre>
<strong>Input:</strong> m = 3, n = 3, ops = [[2,2],[3,3]]
<strong>Output:</strong> 4
<strong>Explanation:</strong> The maximum integer in M is 2, and there are four of it in M. So return 4.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> m = 3, n = 3, ops = [[2,2],[3,3],[3,3],[3,3],[2,2],[3,3],[3,3],[3,3],[2,2],[3,3],[3,3],[3,3]]
<strong>Output:</strong> 4
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> m = 3, n = 3, ops = []
<strong>Output:</strong> 9
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= m, n &lt;= 4 * 10<sup>4</sup></code></li>
	<li><code>1 &lt;= ops.length &lt;= 10<sup>4</sup></code></li>
	<li><code>ops[i].length == 2</code></li>
	<li><code>1 &lt;= a<sub>i</sub> &lt;= m</code></li>
	<li><code>1 &lt;= b<sub>i</sub> &lt;= n</code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- IXL - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

The simplest method is to create a actual 2-D array of size $$m$$x$$n$$($$arr$$), perform all the operations one by one on the given range of elements, and then count the number of maximum elements. Now, we know that all the operations performed always include the element at index $$(0,0)$$. Thus, the element $$arr[0][0]$$ will always be the maximum. After performing all the operations, we can count the number of elements equal to $$arr[0][0]$$ to get the required count of the maximum elements.

<iframe src="https://leetcode.com/playground/LA3PzFef/shared" frameBorder="0" width="100%" height="395" name="LA3PzFef"></iframe>


**Complexity Analysis**

* Time complexity : $$O(x*m*n)$$. Array is updated $$x$$ times, where $$x$$ represents number of times operation is preformed i.e. $$ops.length$$.

* Space complexity : $$O(m*n)$$. Array of size $$m*n$$ is used.


---
#### Approach #2 Single Pass [Accepted]

**Algorithm**

As per the given problem statement, all the operations are performed on a rectangular sub-matrix of the initial all 0's $$M$$ matrix. The upper left corner of each such rectangle is given by the index $$(0, 0)$$ and the lower right corner for an operation $$[i, j]$$ is given by the index $$(i, j)$$. 

The maximum element will be the one on which all the operations have been performed. The figure below shows an example of two operations being performed on the initial $$M$$ array. 

![Range_Addition](../Figures/598_Range_Addition2.PNG)

From this figure, we can observe that the maximum elements will be the ones which lie in the intersection region of the rectangles representing the operations. Further, we can observe that to count the number of elements lying in this intersection region, we don't actually need to perform the operations, but we need to determine the lower right corner of the intersecting region only. This corner is given by $$\big(x, y\big) = \big(\text{min}(\text{op[0]}), \text{min}(op[1])\big)$$, where $$\text{min}(\text{op[i]})$$ reprsents the minimum value of $$\text{op[i]}$$ from among all the $$\text{op[i]}$$'s in the given set of operations.

Thus, the resultant count of elements lying in the intersection is given by: $$x$$x$$y$$.

<iframe src="https://leetcode.com/playground/7SvQSZKS/shared" frameBorder="0" width="100%" height="208" name="7SvQSZKS"></iframe>


**Complexity Analysis**

* Time complexity : $$O(x)$$. Single traversal of all operations is done. $$x$$ refers to the number of operations.

* Space complexity : $$O(1)$$. No extra space is used.


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python solution , beat 100%
- Author: vroyibg
- Creation Date: Mon May 29 2017 17:35:14 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 14:00:43 GMT+0800 (Singapore Standard Time)

<p>
An operation [a,b] add by one for all 0 <= i < a and 0 <= j < b.
So the number of maximum integers in the matrix after performing all the operations or the integers in matrix that get added by 1 by all operations are the integers that in *0 <=i<min_a* and *0<=i<min_b* or **min_a * min_b**
```class Solution(object):
    def maxCount(self, m, n, ops):
        """
        :type m: int
        :type n: int
        :type ops: List[List[int]]
        :rtype: int
        """
        if not ops:
            return m*n
        return min(op[0] for op in ops)*min(op[1] for op in ops)
</p>


### Java Solution, find Min
- Author: shawngao
- Creation Date: Sun May 28 2017 11:07:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 01 2018 04:45:17 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public int maxCount(int m, int n, int[][] ops) {
        if (ops == null || ops.length == 0) {
            return m * n;
        }
        
        int row = Integer.MAX_VALUE, col = Integer.MAX_VALUE;
        for(int[] op : ops) {
            row = Math.min(row, op[0]);
            col = Math.min(col, op[1]);
        }
        
        return row * col;
    }
}
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun May 28 2017 11:11:11 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 14:00:32 GMT+0800 (Singapore Standard Time)

<p>
Say the operations are ```[(x_1, y_1), (x_2, y_2), ..., (x_n, y_n)]```.  The top left square is clearly incremented by every operation.  If some square ```(x, y)``` has ```x >= x_i```, then it will not be marked by operation ```i```.  So all squares ```(x, y)``` with ```x >= min_i(x_i)``` do not get marked.  

Thus, when there is atleast one operation, all squares ```(x, y)``` with ```0 <= x < min(x_1, x_2, ..., x_n) and 0 <= y < min(y_1, y_2, ..., y_n)``` get marked; and there are ```min_i(x_i) * min_i(y_i)``` of them.  If there are no operations, then what is marked is the entire board.

```
def maxCount(self, R, C, ops):
    if not ops: return R * C
    X, Y = zip(*ops)
    return min(X) * min(Y)
```
</p>


