---
title: "Two Sum Less Than K"
weight: 992
#id: "two-sum-less-than-k"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of integers and&nbsp;integer <code>K</code>, return the maximum <code>S</code> such that there exists <code>i &lt; j</code> with <code>A[i] + A[j] = S</code> and <code>S &lt; K</code>. If no <code>i, j</code> exist satisfying this equation, return -1.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[34,23,1,24,75,33,54,8]</span>, K = <span id="example-input-1-2">60</span>
<strong>Output: </strong><span id="example-output-1">58</span>
<strong>Explanation: </strong>
We can use 34 and 24 to sum 58 which is less than 60.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">[10,20,30]</span>, K = <span id="example-input-2-2">15</span>
<strong>Output: </strong><span id="example-output-2">-1</span>
<strong>Explanation: </strong>
In this case it&#39;s not possible to get a pair sum less that 15.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 100</code></li>
	<li><code>1 &lt;= A[i] &lt;= 1000</code></li>
	<li><code>1 &lt;= K &lt;= 2000</code></li>
</ol>

</div>

## Tags
- Array (array)

## Companies
- Amazon - 3 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

This problem is a variation of [Two Sum](https://leetcode.com/articles/two-sum/). The main difference is that we are not searching for the exact target here. Instead, our sum is in some *relation* with the target. For this problem, we are looking for a maximum sum that is *smaller* than the target.

First, let's check solutions for the similar problems:

1. [Two Sum](https://leetcode.com/articles/two-sum/) uses a hashmap to find complement values, and therefore achieves $$\mathcal{O}(N)$$ time complexity.
2. [Two Sum II](https://leetcode.com/articles/two-sum-ii-input-array-is-sorted/) uses the two pointers pattern and also has $$\mathcal{O}(N)$$ time complexity for a sorted array. We can use this approach for any array if we sort it first, which bumps the time complexity to $$\mathcal{O}(n\log{n})$$.

Since our sum can be any value smaller than the target, we cannot use a hashmap. We do not know which value to look up! Instead, we need to sort the array and use a binary search or the two pointers pattern, like in [Two Sum II](https://leetcode.com/articles/two-sum-ii-input-array-is-sorted/). In a sorted array, it is easy to find elements that are close to a given value.

---

#### Approach 1: Brute Force

It is important to understand the input constraints to choose the most appropriate approach. For this problem, the size of our array is limited to `100`. So, a brute force solution could be a reasonable option. It's simple and does not require any additional memory.

**Algorithm**

1. For each index `i` in `A`:
    - For each index `j > i` in `A`:
        - If `A[i] + A[j]` is less than `k`:
            - Track maximum `A[i] + A[j]` in the result `S`.

2. Return the result `S`.

<iframe src="https://leetcode.com/playground/Zp3hpwLg/shared" frameBorder="0" width="100%" height="191" name="Zp3hpwLg"></iframe>

**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(n^2)$$. We have 2 nested loops.

- Space Complexity: $$\mathcal{O}(1)$$.

---

#### Approach 2: Two Pointers

We will follow the same two pointers approach as in [Two Sum II](https://leetcode.com/articles/two-sum-ii-input-array-is-sorted/). It requires the array to be sorted, so we'll do that first.

As a quick refresher, the pointers are initially set to the first and the last element respectively. We compare the sum of these two elements with the target. If it is smaller than the target, we increment the lower pointer `lo`. Otherwise, we decrement the higher pointer `hi`. Thus, the sum always moves toward the target, and we "prune" pairs that would move it further away. Again, this works only if the array is sorted. Head to the [Two Sum II](https://leetcode.com/articles/two-sum-ii-input-array-is-sorted/) solution for the detailed explanation.

Since the sum here must be smaller than the target, we don't stop when we find a pair that sums exactly to the target. We decrement the higher pointer and continue until our pointers collide. For each iteration, we track the maximum sum - if it's smaller than the target.

!?!../Documents/1099_Two_Sum_Less_K.json:1200,470!?!

**Algorithm**

1. Sort the array.

2. Set the `lo` pointer to zero, and `hi` - to the last index.

3. While `lo` is smaller than `hi`:
    - If `A[lo] + A[hi]` is less than `K`:
        - Track maximum `A[hi] + A[lo]` in the result `S`.
        - Increment `lo`.
    - Else:
        - Decrement `hi`.

4. Return the result `S`.

<iframe src="https://leetcode.com/playground/Fkk9j8mD/shared" frameBorder="0" width="100%" height="293" name="Fkk9j8mD"></iframe>

**Optimizations**

We can break from the loop as soon as `A[lo] > K / 2`. In the sorted array, `A[lo]` is the smallest of the remaining elements, so `A[hi] > K / 2` for any `hi`. Therefore, `A[lo] + A[hi]` will be equal or greater than `K` for the remaining elements.

**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(n\log{n})$$ to sort the array. The two pointers approach itself is $$\mathcal{O}(n)$$, so the time complexity would be linear if the input is sorted.

- Space Complexity: from $$\mathcal{O}(\log{n})$$ to $$\mathcal{O}(n)$$, depending on the implementation of the sorting algorithm.

---

#### Approach 3: Binary Search

Instead of moving two pointers towards the target, we can iterate through each element `A[i]`, and binary-search for a complement value `K - A[i]`. This approach is less efficient than the two pointers one, however, it can be more intuitive to come up with. Same as above, we need to sort the array first for this to work.

Note that the binary search returns the "insertion point" for the searched value, i.e. the position where that value would be inserted to keep the array sorted. So, the binary search result points to the first element that is equal or greater than the complement value. Since our sum must be smaller than `K`, we consider the element immediately *before* the found element.

**Algorithm**

1. Sort the array.

2. For each index `i` in `A`:
    - Binary search for `K - A[i]` starting from `i + 1`.
    - Set `j` to the position before the found element.
    - If `j` is less than `i`:
        - Track maximum `A[i] + A[j]` in the result `S`.

3. Return the result `S`.

> Note that the binary search function in Java works a bit differently. If there are multiple elements that match the search value, it does not guarantee to point to the first one. That's why in the Java solution below we search for `K - A[i] - 1`. Note that we decrement the pointer only if the value we found is greater than `K - A[i] - 1`.

<iframe src="https://leetcode.com/playground/4ZzbXjhi/shared" frameBorder="0" width="100%" height="293" name="4ZzbXjhi"></iframe>

**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(n\log{n})$$ to sort the array and do the binary search for each element.

- Space Complexity: from $$\mathcal{O}(\log{n})$$ to $$\mathcal{O}(n)$$, depending on the implementation of the sorting algorithm.

---

#### Approach 4: Counting Sort

We can leverage the fact that the input number range is limited to `[1..1000]` and use a counting sort. Then, we can use the two pointers pattern to enumerate pairs in the `[1..1000]` range.

> Note that the result can be a sum of two identical numbers, and that means that `lo` can be equal to `hi`. In this case, we need to check if the count for that number is greater than one.

**Algorithm**

1. Count each element using the array `count`.

2. Set the `lo` number to zero, and `hi` - to 1000.

3. While `lo` is smaller than, or **equals** `hi`:
    - If `lo + hi` is greater than `K`, or `count[hi] == 0`:
        - Decrement `hi`.
    - Else:
        - If `count[lo]` is greater than `0` (when `lo < hi`), or `1` (when `lo == hi`):
            - Track maximum `lo + hi` in the result `S`.
        - Increment `lo`.

4. Return the result `S`.

<iframe src="https://leetcode.com/playground/jqRbMKfE/shared" frameBorder="0" width="100%" height="344" name="jqRbMKfE"></iframe>

**Optimizations**

1. We can set `hi` to either maximum number, or `K - 1`, whichever is smaller.
2. We can ignore numbers greater than `K - 1`.
3. We can use a boolean array (e.g. `seen`) instead of `count`. In the first loop, we will check if `i` is a duplicate (`seen[i]` is already true) and set `S` to the highest `i + i < K`. Note that the two pointers loop will run while `lo < hi`, not while `lo <= hi`.
4. We can break from the two pointers loop as soon as `A[lo] > K / 2`.

**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(n + m)$$, where $$m$$ corresponds to the range of values in the input array.

- Space Complexity: $$\mathcal{O}(m)$$ to count each value.

---

#### Further Thoughts

Always clarify the problem constraints and inputs during an interview. This would help you choose the right approach.

The Two Pointers approach is a good choice when the number of elements is large, and the range of possible values is not constrained. Also, if the input array is already sorted, this approach provides a linear time complexity and does not require additional memory.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Sort then push from two ends.
- Author: rock
- Creation Date: Sun Jun 30 2019 00:02:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 01 2019 00:13:16 GMT+0800 (Singapore Standard Time)

<p>
# Note: 1) I have no premium subscription and can NO longer read comments under locked problems, though I can still edit my post now. 2) Sorry that I can NOT answer your questions.
---

1. Sort the input `A`;
2. Push from the two ends and attempt to find any addition of the two elements < K; if the addition >= K, then decrease the high bound and hence tentatively get a smaller addition; otherwise, increase low bound to find a bigger addition;
3. repeat 2 till the end.

```
    public int twoSumLessThanK(int[] A, int K) {
        Arrays.sort(A); // Time cost O(nlogn).
        int max = -1, i = 0, j = A.length - 1; 
        while (i < j) {
            int sum = A[i] + A[j];
            if (sum < K) { // find one candidate.
                max = Math.max(max, sum);
                ++i; // increase the smaller element.
            }else { // >= sum.
                --j; // decrease the bigger element.
            }
        }
        return max;
    }
```	

**Analysis:**

Time: O(nlogn), space: O(1), where n = A.length.
</p>


### C++ 3 solutions
- Author: votrubac
- Creation Date: Thu Jul 04 2019 06:55:43 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 26 2020 09:31:20 GMT+0800 (Singapore Standard Time)

<p>
# Brute-Force
Since ```1 <= A.length <= 100```, a brute-force should do.
```
int twoSumLessThanK(const vector<int>& A, int K, int S = -1) {
  for (auto i = 0; i < A.size(); ++i)
    for (auto j = i + 1; j < A.size(); ++j)
      if (A[i] + A[j] < K) S = max(S, A[i] + A[j]);
  return S;
}
```
## Complexity Analysis
Runtime: *O(n * n)*
Memory: *O(1)*
# Two Pointers
Sort and use two pointers to try all reasonable combinations. This is a generic solution for an unconstrained problem.
```
int twoSumLessThanK(vector<int>& A, int K, int S = -1) {
  sort(begin(A), end(A));
  for (int i = 0, j = A.size() - 1; i < j; ) {
    if (A[i] + A[j] < K) S = max(S, A[i++] + A[j]);
    else --j;
  }
  return S;
}
```
Runtime: *O(n log n)*
Memory: *O(n)* for the sorted array (assuming we cannot modify the input)
# Counting Sort
Since ```1 <= A[i] <= 1000```, we can apply the counting sort, which takes linear time. Then, we use two indexes to search for a pair, like in the solution above.
```
int twoSumLessThanK(vector<int>& A, int K, int S = -1) {
  int nums[1001] = {};
  for (auto i : A) 
      ++nums[i];
  for (int i = 1, j = 1000; i <= j;) {
    if (!nums[j] || i + j >= K)
        --j;
    else {
        if (nums[i] > 0 + (i == j))
            S = max(S, i + j);
        ++i;
    } 
  }
  return S;
}
```
If you want to use less memory, we can track our numbers in a boolean array. We just need to add an extra check if there is a duplicate.
```cpp
int twoSumLessThanK(vector<int>& A, int K, int S = -1) {
  bool nums[1001] = {};
  for (auto i : A) 
      if (nums[i] && i + i < K)
          S = max(S, i + i);
      else
          nums[i] = true;
  for (int i = 1, j = 1000; i < j;) {
    if (!nums[j] || i + j >= K)
        --j;
    else {
        if (nums[i])
            S = max(S, i + j);
        ++i;
    } 
  }
  return S;
}
```
Runtime: *O(n + m)*, where m is the maximum value (1000 for this problem).
Memory: *O(m)*
</p>


### Python simple solution
- Author: hanhan015
- Creation Date: Sun Jun 30 2019 14:42:17 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 18 2019 13:11:08 GMT+0800 (Singapore Standard Time)

<p>
First we sort the array. Then we initialize two cursors i, j pointing at the beginning and the end of the sorted array. Start checking if the current combination satisfys the contraint.
case 1. yes, then we move i one step further : ==> i <- i+1, update the ans:  ans <- max(ans, current)
case 2 no, move j one step to the left : j <- j-1
Utill they meet, return the ans
```
def towSumLessThanK(A,K): 
		a = sorted(A) 
		i,j = 0,len(a)-1
        ans = -1
        while i<j:
            if a[i]+a[j]<K:
                ans = max(ans,a[i]+a[j])
                i += 1
            else:
                j -= 1
        return ans
		```
</p>


