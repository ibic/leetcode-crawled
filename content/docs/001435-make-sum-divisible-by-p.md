---
title: "Make Sum Divisible by P"
weight: 1435
#id: "make-sum-divisible-by-p"
---
## Description
<div class="description">
<p>Given an array of positive integers <code>nums</code>, remove the <strong>smallest</strong> subarray (possibly <strong>empty</strong>) such that the <strong>sum</strong> of the remaining elements is divisible by <code>p</code>. It is <strong>not</strong> allowed to remove the whole array.</p>

<p>Return <em>the length of the smallest subarray that you need to remove, or </em><code>-1</code><em> if it&#39;s impossible</em>.</p>

<p>A <strong>subarray</strong> is defined as a contiguous block of elements in the array.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,1,4,2], p = 6
<strong>Output:</strong> 1
<strong>Explanation:</strong> The sum of the elements in nums is 10, which is not divisible by 6. We can remove the subarray [4], and the sum of the remaining elements is 6, which is divisible by 6.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [6,3,5,2], p = 9
<strong>Output:</strong> 2
<strong>Explanation:</strong> We cannot remove a single element to get a sum divisible by 9. The best way is to remove the subarray [5,2], leaving us with [6,3] with sum 9.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3], p = 3
<strong>Output:</strong> 0
<strong>Explanation:</strong> Here the sum is 6. which is already divisible by 3. Thus we do not need to remove anything.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3], p = 7
<strong>Output:</strong> -1
<strong>Explanation:</strong> There is no way to remove a subarray in order to get a sum divisible by 7.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> nums = [1000000000,1000000000,1000000000], p = 3
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10<sup>5</sup></code></li>
	<li><code>1 &lt;= nums[i] &lt;= 10<sup>9</sup></code></li>
	<li><code>1 &lt;= p &lt;= 10<sup>9</sup></code></li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Prefix Sum
- Author: lee215
- Creation Date: Sun Sep 20 2020 00:06:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 20 2020 01:26:30 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Calculate the `need = sum(A) % p`.
Then one pass, record the prefix sum in a hashmap.
<br>

# **Explanation**
Then the question become:
Find the shortest array with sum % p = need.

`last[remainder] = index` records the last `index ` that
`(A[0] + A[1] + .. + A[i]) % p = remainder`
<br>

# **Complexity**
Time `O(N)`
Space `O(N)`
<br>

**Java:**
```java
class Solution {
    public int minSubarray(int[] A, int p) {
        int n = A.length, res = n, need = 0, cur = 0;
        for (int a : A)
            need = (need + a) % p;
        Map<Integer, Integer> last = new HashMap<>();
        last.put(0, -1);
        for (int i = 0; i < n; ++i) {
            cur = (cur + A[i]) % p;
            last.put(cur, i);
            int want = (cur - need + p) % p;
            res = Math.min(res, i - last.getOrDefault(want, -n));
        }
        return res < n ? res : -1;
    }
}
```

**C++:**
```cpp
    int minSubarray(vector<int>& A, int p) {
        int n = A.size(), res = n, need = 0, cur = 0;
        for (auto a : A)
            need = (need + a) % p;
        unordered_map<int, int> last = {{0, -1}};
        for (int i = 0; i < n; ++i) {
            cur = (cur + A[i]) % p;
            last[cur] = i;
            int want = (cur - need + p) % p;
            if (last.count(want))
                res = min(res, i - last[want]);
        }
        return res < n ? res : -1;
    }
```

**Python:**
```py
    def minSubarray(self, A, p):
        need = sum(A) % p
        dp = {0: -1}
        cur = 0
        res = n = len(A)
        for i, a in enumerate(A):
            cur = (cur + a) % p
            dp[cur] = i
            if (cur - need) % p in dp:
                res = min(res, i - dp[(cur - need) % p])
        return res if res < n else -1
```

</p>


### C++/Java O(n)
- Author: votrubac
- Creation Date: Sun Sep 20 2020 00:02:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 20 2020 00:30:23 GMT+0800 (Singapore Standard Time)

<p>
This is a variation of [974. Subarray Sums Divisible by K](https://leetcode.com/problems/subarray-sums-divisible-by-k/).

We first compute modulo `mod` of the sum for the entire array. This modulo should be zero for the array to be divisible by `p`. To make it so, we need to remove a subarray, which sum modulo is a compliment `comp` to `mod`.

We use a hash map, where we track the running sum modulo, and the most recent position for that modulo. As we go through the array, we look up for a `comp` modulo in that hash map, and track the minimum window size.

**C++**
```cpp
int minSubarray(vector<int>& nums, int p) {
    int mod = 0, r_mod = 0, min_w = nums.size();
    for (auto n : nums)
        mod = (mod + n) % p;
    if (mod == 0)
        return 0;
    unordered_map<int, int> pos = {{0, -1}};
    for (int i = 0; i < nums.size(); ++i) {
        r_mod = (r_mod + nums[i]) % p;
        int comp = (p - mod + r_mod) % p;
        if (pos.count(comp))
            min_w = min(min_w, i - pos[comp]);
        pos[r_mod] = i;
    }
    return min_w >= nums.size() ? -1 : min_w;
}
```
**Java**
```java
public int minSubarray(int[] nums, int p) {
    int mod = 0, r_mod = 0, min_w = nums.length;
    for (var n : nums)
        mod = (mod + n) % p;
    if (mod == 0)
        return 0;
    Map<Integer, Integer> pos = new HashMap<>();
    pos.put(0, -1);
    for (int i = 0; i < nums.length; ++i) {
        r_mod = (r_mod + nums[i]) % p;
        int comp = (p - mod + r_mod) % p;
        if (pos.containsKey(comp))
            min_w = Math.min(min_w, i - pos.get(comp));
        pos.put(r_mod, i);
    }    
    return min_w >= nums.length ? -1 : min_w;
}
```
</p>


### [Java/Python 3] O(n) code w/ brief explanation, analysis and similar problems.
- Author: rock
- Creation Date: Sun Sep 20 2020 00:02:23 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 01 2020 08:48:36 GMT+0800 (Singapore Standard Time)

<p>
Similar problems:

[1. Two Sum](https://leetcode.com/problems/two-sum/description/)
[325. Maximum Size Subarray Sum Equals k: Premium](https://leetcode.com/problems/maximum-size-subarray-sum-equals-k) 
[525. Contiguous Array](https://leetcode.com/problems/contiguous-array)
[560. Subarray Sum Equals K](https://leetcode.com/problems/subarray-sum-equals-k/description/)
[974. Subarray Sums Divisible by K](https://leetcode.com/problems/subarray-sums-divisible-by-k/description/)
[1010. Pairs of Songs With Total Durations Divisible by 60](https://leetcode.com/problems/pairs-of-songs-with-total-durations-divisible-by-60/discuss/256726/JavaPython-3-O(n)-code-w-comment-similar-to-Two-Sum)
[1497. Check If Array Pairs Are Divisible by k](https://leetcode.com/problems/check-if-array-pairs-are-divisible-by-k/discuss/709226/JavaPython-3-Count-positive-remainder-and-cancel-the-other-whenever-locate-a-pair.)

----
1. Find the remainder of the `sum(nums) % p`;
2. Use a HashMap to store the `(prefix-sum % p == last index)` bindings;
3. Traverse the input array to compute the prefix sum and seach its congruence in the afore-mentioned HashMap; Update the minimum length of the subarray if found;
4. If the minimum length is still the initial value after traversal, return `-1`; otherwise, return the minimun.
```java
    public int minSubarray(int[] nums, int p) {
        int remainder = 0, prefixSum = 0; 
        int n = nums.length, minLen = n;
        for (int num : nums) {
            remainder = (remainder + num) % p; 
        }
        var prefixSumToIndex = new HashMap<Integer, Integer>();
        prefixSumToIndex.put(prefixSum, -1);
        for (int i = 0; i < n; ++i) {
            prefixSum = (prefixSum + nums[i]) % p;
            prefixSumToIndex.put(prefixSum, i);
            int key = (prefixSum - remainder + p) % p;
            if (prefixSumToIndex.containsKey(key)) {
                minLen = Math.min(minLen, i - prefixSumToIndex.get(key));
            }
        }
        return minLen == n ? -1 : minLen;        
    }
```
```python
    def minSubarray(self, nums: List[int], p: int) -> int:
        remainder, min_len, prefix_sum = sum(nums) % p, len(nums), 0
        prefix_sum_to_index = {prefix_sum : -1}
        for i, num in enumerate(nums):
            prefix_sum = (prefix_sum + num) % p
            key = (prefix_sum - remainder) % p
            prefix_sum_to_index[prefix_sum] = i
            if key in prefix_sum_to_index:
                min_len = min(min_len, i - prefix_sum_to_index[key])
        return min_len if min_len < len(nums) else -1   
```
**Analysis:**
Time & space: O(n).
</p>


