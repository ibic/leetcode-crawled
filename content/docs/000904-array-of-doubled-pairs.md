---
title: "Array of Doubled Pairs"
weight: 904
#id: "array-of-doubled-pairs"
---
## Description
<div class="description">
<p>Given an array of integers <code>A</code>&nbsp;with even length, return <code>true</code> if and only if it is possible to reorder it such that <code>A[2 * i + 1] = 2 * A[2 * i]</code> for every <code>0 &lt;=&nbsp;i &lt; len(A) / 2</code>.</p>

<div>
<div>
<div>
<ol>
</ol>
</div>
</div>
</div>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> A = [3,1,3,6]
<strong>Output:</strong> false
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> A = [2,1,2,6]
<strong>Output:</strong> false
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> A = [4,-2,2,-4]
<strong>Output:</strong> true
<strong>Explanation:</strong> We can take two groups, [-2,-4] and [2,4] to form [-2,-4,2,4] or [2,4,-2,-4].
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> A = [1,2,4,16,8,4]
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= A.length &lt;= 3 *&nbsp;10<sup>4</sup></code></li>
	<li><code>A.length</code> is even.</li>
	<li><code>-10<sup>5</sup> &lt;= A[i] &lt;= 10<sup>5</sup></code></li>
</ul>

</div>

## Tags
- Array (array)
- Hash Table (hash-table)

## Companies
- Uber - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Greedy

**Intuition**

If `x` is currently the array element with the least absolute value, it must pair with `2*x`, as there does not exist any other `x/2` to pair with it.

**Algorithm**

Let's try to (virtually) "write" the final reordered array.

Let's check elements in order of absolute value.  When we check an element `x` and it isn't used, it must pair with `2*x`.  We will attempt to write `x, 2x` - if we can't, then the answer is `false`.  If we write everything, the answer is `true`.

To keep track of what we have not yet written, we will store it in a `count`.

<iframe src="https://leetcode.com/playground/tGjuksWY/shared" frameBorder="0" width="100%" height="500" name="tGjuksWY"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Match from the Smallest or Biggest, 100%
- Author: lee215
- Creation Date: Sun Dec 09 2018 12:08:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 09 2018 12:08:13 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
**Let\'s see a simple case**
Assume all interger are positive, for example `[2,4,4,8]`.
We have one `x = 2`,  we need to match it with one `2x = 4`.
Then one `4` is gone, we have the other `x = 4`.
We need to match it with one `2x = 8`.
Finaly no number left.

**Why we start from `2`?**
Because it\'s the smallest and we no there is no `x/2` left.
So we know we need to find `2x`


**What if the case negative?**
One way is that start from the biggest (with abosolute value smallest),
and we aplly same logic.

Another way is that start from the smallest (with abosolute value biggest),
and we try to find `x/2` each turn.
<br>

## **Explanation**
1. Count all numbers.
2. Loop all numbers on the order of its absolute.
    We have `counter[x]` of `x`, so we need the same amount of `2x` wo match them.
    If `c[x] > c[2 * x]`, then we return false
    If `c[x] <= c[2 * x]`, then we do `c[2 * x] -= c[x]` to remove matched `2x`.


Don\'t worry about `0`, it doesn\'t fit the logic above but it won\'t break our algorithme.

In case count[0] is odd, it won\'t get matched in the end.
(Anyway you can return false earlier here)

In case count[0] is even, we still have `c[0] <= c[2 * 0]`.
And we still need to check all other numbers.
<br>

**Java, O(NlogK):**
```
    public boolean canReorderDoubled(int[] A) {
        Map<Integer, Integer> count = new TreeMap<>();
        for (int a : A)
            count.put(a, count.getOrDefault(a, 0) + 1);
        for (int x : count.keySet()) {
            if (count.get(x) == 0) continue;
            int want = x < 0 ? x / 2 : x * 2;
            if (x < 0 && x % 2 != 0 || count.get(x) > count.getOrDefault(want, 0))
                return false;
            count.put(want, count.get(want) - count.get(x));
        }
        return true;
    }
```

**C++, O(N + KlogK)**
```
    bool canReorderDoubled(vector<int>& A) {
        unordered_map<int, int> c;
        for (int a : A) c[a]++;
        vector<int> keys;
        for (auto it : c)
            keys.push_back(it.first);
        sort(keys.begin(), keys.end(), [](int i, int j) {return abs(i) < abs(j);});
        for (int x : keys) {
            if (c[x] > c[2 * x])
                return false;
            c[2 * x] -= c[x];
        }
        return true;
    }
```

**Python, O(N + KlogK), 100~200ms**
```
    def canReorderDoubled(self, A):
        c = collections.Counter(A)
        for x in sorted(c, key=abs):
            if c[x] > c[2 * x]:
                return False
            c[2 * x] -= c[x]
        return True
```

</p>


### Python O(1) space, two pointers
- Author: LambdaS
- Creation Date: Mon Dec 10 2018 05:54:49 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 10 2018 05:54:49 GMT+0800 (Singapore Standard Time)

<p>
1. We sort the list (ascending) by absolute value and make sure the negative ones come before the positive ones. 
2. Then we use two pointers. Since the values are ascending (by abs), the fast pointer never looks back and will only reach the end of the list if it falis to find a 2*x and returns false. When it does find a match, it marks the item with a dummy value so that the slow pointer can skip it.
3. Time is O(NlogN), Space is O(1).
```
class Solution:
    def canReorderDoubled(self, A):
        """
        :type A: List[int]
        :rtype: bool
        """
        dummy = 1000000
        A.sort(key=lambda x:(x>0,abs(x)))
        size =len(A)
        fast=0
        for i in range(size):
            if A[i]==dummy: continue
            if fast<=i: fast = i+1
            while fast<size and A[fast]!=2*A[i]: fast+=1
            if fast==size: return False
            A[fast] = dummy
        return True
```
</p>


### Java Heap, Concise
- Author: cheng_coding_attack
- Creation Date: Thu Dec 27 2018 00:25:59 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Dec 27 2018 00:25:59 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    
    public boolean canReorderDoubled(int[] A) {
        PriorityQueue<Integer> pq = new PriorityQueue<>((a, b) -> {
            return Math.abs(a) - Math.abs(b);
        });
        for (int num : A) {
            pq.offer(num);
        }
        while (!pq.isEmpty()) {
            Integer num = pq.poll();
            if (!pq.remove(num * 2)) return false;
        }
        return true;
    }
    
}
```
</p>


