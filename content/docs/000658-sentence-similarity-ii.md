---
title: "Sentence Similarity II"
weight: 658
#id: "sentence-similarity-ii"
---
## Description
<div class="description">
<p>Given two sentences <code>words1, words2</code> (each represented as an array of strings), and a list of similar word pairs <code>pairs</code>, determine if two sentences are similar.</p>

<p>For example, <code>words1 = [&quot;great&quot;, &quot;acting&quot;, &quot;skills&quot;]</code> and <code>words2 = [&quot;fine&quot;, &quot;drama&quot;, &quot;talent&quot;]</code> are similar, if the similar word pairs are <code>pairs = [[&quot;great&quot;, &quot;good&quot;], [&quot;fine&quot;, &quot;good&quot;], [&quot;acting&quot;,&quot;drama&quot;], [&quot;skills&quot;,&quot;talent&quot;]]</code>.</p>

<p>Note that the similarity relation <b>is</b> transitive. For example, if &quot;great&quot; and &quot;good&quot; are similar, and &quot;fine&quot; and &quot;good&quot; are similar, then &quot;great&quot; and &quot;fine&quot; <b>are similar</b>.</p>

<p>Similarity is also symmetric. For example, &quot;great&quot; and &quot;fine&quot; being similar is the same as &quot;fine&quot; and &quot;great&quot; being similar.</p>

<p>Also, a word is always similar with itself. For example, the sentences <code>words1 = [&quot;great&quot;], words2 = [&quot;great&quot;], pairs = []</code> are similar, even though there are no specified similar word pairs.</p>

<p>Finally, sentences can only be similar if they have the same number of words. So a sentence like <code>words1 = [&quot;great&quot;]</code> can never be similar to <code>words2 = [&quot;doubleplus&quot;,&quot;good&quot;]</code>.</p>

<p><b>Note:</b></p>

<ul>
	<li>The length of <code>words1</code> and <code>words2</code> will not exceed <code>1000</code>.</li>
	<li>The length of <code>pairs</code> will not exceed <code>2000</code>.</li>
	<li>The length of each <code>pairs[i]</code> will be <code>2</code>.</li>
	<li>The length of each <code>words[i]</code> and <code>pairs[i][j]</code> will be in the range <code>[1, 20]</code>.</li>
</ul>

<p>&nbsp;</p>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Union Find (union-find)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Depth-First Search [Accepted]

**Intuition**

Two words are similar if they are the same, or there is a path connecting them from edges represented by `pairs`.

We can check whether this path exists by performing a depth-first search from a word and seeing if we reach the other word.  The search is performed on the underlying graph specified by the edges in `pairs`.

**Algorithm**

We start by building our `graph` from the edges in `pairs`.  

The specific algorithm we go for is an iterative depth-first search.  The implementation we go for is a typical "visitor pattern": when searching whether there is a path from `w1 = words1[i]` to `w2 = words2[i]`, `stack` will contain all the nodes that are queued up for processing, while `seen` will be all the nodes that have been queued for processing (whether they have been processed or not).

<iframe src="https://leetcode.com/playground/TL6SiGPy/shared" frameBorder="0" width="100%" height="500" name="TL6SiGPy"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(NP)$$, where $$N$$ is the maximum length of `words1` and `words2`, and $$P$$ is the length of `pairs`.  Each of $$N$$ searches could search the entire graph.

* Space Complexity: $$O(P)$$, the size of `pairs`.

---
#### Approach #2: Union-Find [Accepted]

**Intuition**

As in *Approach #1*, we want to know if there is path connecting two words from edges represented by `pairs`.

Our problem comes down to finding the connected components of a graph.  This is a natural fit for a *Disjoint Set Union* (DSU) structure.

**Algorithm**

Draw edges between words if they are similar.  For easier interoperability between our DSU template, we will map each `word` to some integer `ix = index[word]`.  Then, `dsu.find(ix)` will tell us a unique id representing what component that word is in.

For more information on DSU, please look at *Approach #2* in the [article here](https://leetcode.com/articles/redundant-connection/).  For brevity, the solutions showcased below do not use *union-by-rank*.

After putting each word in `pairs` into our DSU template, we check successive pairs of words `w1, w2 = words1[i], words2[i]`.  We require that `w1 == w2`, or `w1` and `w2` are in the same component.  This is easily checked using `dsu.find`.

<iframe src="https://leetcode.com/playground/b2FAdLmt/shared" frameBorder="0" width="100%" height="500" name="b2FAdLmt"></iframe>


**Complexity Analysis**

* Time Complexity: $$O(N \log P + P)$$, where $$N$$ is the maximum length of `words1` and `words2`, and $$P$$ is the length of `pairs`.  If we used union-by-rank, this complexity improves to $$O(N * \alpha(P) + P) \approx O(N + P)$$, where $$\alpha$$ is the *Inverse-Ackermann* function.

* Space Complexity: $$O(P)$$, the size of `pairs`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] Clean Code with Explanation
- Author: alexander
- Creation Date: Mon Nov 27 2017 04:04:36 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 09:50:21 GMT+0800 (Singapore Standard Time)

<p>
[description](https://leetcode.com/problems/sentence-similarity-ii/description/)

- This is a good use case for `Union-Find`, compare to [Sentence Similarity I](https://leetcode.com/problems/sentence-similarity/description/), here the similarity between words are `transitive`, so all the connected(`similar`) words should be group into an `union` represented by their `ultimate parent`(or family holder, you name it).
- The connections can be represented by an parent map `Map<String, String> m`, which record the `direct parent-ship` we learned in each pair, but not the `ultimate-parent`. To build it, go through the input `pairs`, for each `pair<w1, w2>`, use the recursive `find()` method to find the `ultimate-parent` for both word - `parent1`, `parent2`, if they are different, assign `parent1` as parent of `parent2`(or the other way around), so that the to families are `merged`.
- The classic `find(x)` method will find the `ultimate-parent` of `x`. I modified it a little bit, make it do a little of extra initialization work - `assign x itself as its parent when it is not initialize` - so that we don't have to explicitly initialize the map at the beginning.
**Java**
```
class Solution {
    public boolean areSentencesSimilarTwo(String[] a, String[] b, String[][] pairs) {
        if (a.length != b.length) return false;
        Map<String, String> m = new HashMap<>();
        for (String[] p : pairs) {
            String parent1 = find(m, p[0]), parent2 = find(m, p[1]);
            if (!parent1.equals(parent2)) m.put(parent1, parent2);
        }

        for (int i = 0; i < a.length; i++)
            if (!a[i].equals(b[i]) && !find(m, a[i]).equals(find(m, b[i]))) return false;

        return true;
    }

    private String find(Map<String, String> m, String s) {
        if (!m.containsKey(s)) m.put(s, s);
        return s.equals(m.get(s)) ? s : find(m, m.get(s));
    }
}
```
**C++**
```
class Solution {
public:
    bool areSentencesSimilarTwo(vector<string>& a, vector<string>& b, vector<pair<string, string>> pairs) {
        if (a.size() != b.size()) return false;
        map<string, string> m;
        for (pair<string, string> p : pairs) {
            string parent1 = find(m, p.first), parent2 = find(m, p.second);
            if (parent1 != parent2) m[parent1] = parent2;
        }

        for (int i = 0; i < a.size(); i++)
            if (a[i] != b[i] && find(m, a[i]) != find(m, b[i])) return false;

        return true;
    }

private:
    string find(map<string, string>& m, string s) {
        return !m.count(s) ? m[s] = s : (m[s] == s ? s : find(m, m[s]));
    }
};
```
</p>


### Java Easy DFS solution with Explanation
- Author: FLAGbigoffer
- Creation Date: Mon Nov 27 2017 01:56:45 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 12:45:48 GMT+0800 (Singapore Standard Time)

<p>
Notice there\'s no java DFS solution posted by others. I love DFS, what about you?
The idea is simple:
1. Build the graph according to the similar word pairs. Each word is a graph node.
2. For each word in words1, we do DFS search to see if the corresponding word is existing in words2.

See the clean code below. Happy coding!
```
class Solution {
    public boolean areSentencesSimilarTwo(String[] words1, String[] words2, String[][] pairs) {
        if (words1.length != words2.length) {
            return false;
        }
        
        Map<String, Set<String>> graph = new HashMap<>();
        for (String[] p : pairs) {
            graph.putIfAbsent(p[0], new HashSet<>());
            graph.putIfAbsent(p[1], new HashSet<>());
            graph.get(p[0]).add(p[1]);
            graph.get(p[1]).add(p[0]);
        }
        
        for (int i = 0; i < words1.length; i++) {
            if (words1[i].equals(words2[i])) continue;           
            if (!graph.containsKey(words1[i])) return false;            
            if (!dfs(graph, words1[i], words2[i], new HashSet<>())) return false;
        }
        
        return true;
    }
    
    private boolean dfs(Map<String, Set<String>> graph, String source, String target, Set<String> visited) {
        if (graph.get(source).contains(target)) return true;
        
        if (visited.add(source)) {
            for (String next : graph.get(source)) {
                if (!visited.contains(next) && dfs(graph, next, target, visited)) 
                    return true;
            }
        }
        return false;
    }
}
```
</p>


### SHORT Python DFS with explanation
- Author: yangshun
- Creation Date: Sun Nov 26 2017 15:58:56 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 00:59:48 GMT+0800 (Singapore Standard Time)

<p>
Whenever we see a list of pairs as input, one probable approach will be to treat that as a list of edges and model the question as a graph. In this question, the idea here is to connect words to their similar words, and all connected words are similar. In each connected component of a graph, select any word to be the root word and then generate a mapping of word to root word. If two words are similar, they have the same root word.

First build a graph from `pairs`. An input of `[["great","good"],["fine","good"],["drama","acting"],["skills","talent"]]` will have a graph that looks like:

```
# words
{
  "great": set(["good"]),
  "good": set(["great", "fine"]),
  "talent": set(["skills"]),
  "skills": set(["talent"]),
  "drama": set(["acting"]),
  "acting": set(["drama"]),
  "fine": set(["good"]),
}
```

Next, we do a DFS on each word to try to group the connected words together by assigning each word to a root word. The `similar_words` dict maps every word to a root word so that we can immediately know whether two words are similar just by looking up this dict and seeing if they have the same root word:

```
# similar_words
{
  "great": "great",
  "good": "great",
  "talent": "talent",
  "skills": "talent",
  "drama": "drama",
  "acting": "drama",
  "fine": "great",
}
```

*- Yangshun*

```
class Solution(object):
    def areSentencesSimilarTwo(self, words1, words2, pairs):
        from collections import defaultdict
        if len(words1) != len(words2): return False
        words, similar_words = defaultdict(set), {}
        [(words[w1].add(w2), words[w2].add(w1)) for w1, w2 in pairs]
        def dfs(word, root_word):
            if word in similar_words: return
            similar_words[word] = root_word
            [dfs(synonym, root_word) for synonym in words[word]]
        [dfs(word, word) for word in words]
        return all(similar_words.get(w1, w1) == similar_words.get(w2, w2) for w1, w2 in zip(words1, words2))
```

A longer version with inline comments can be found below:

```
class Solution(object):
    def areSentencesSimilarTwo(self, words1, words2, pairs):
        from collections import defaultdict
        if len(words1) != len(words2):
            return False
        words = defaultdict(set)
        # Build the graph from pairs.
        for w1, w2 in pairs:
            words[w1].add(w2)
            words[w2].add(w1)

        similar_words = {}
        def dfs(word, root_word):
            if word in similar_words:
                return
            similar_words[word] = root_word
            [dfs(synonym, root_word) for synonym in words[word]]

        # Assign root words.
        [dfs(word, word) for word in words]

        # Compare words.
        return all(similar_words.get(w1, w1) == similar_words.get(w2, w2) for w1, w2 in zip(words1, words2))
```
</p>


