---
title: "Single Number III"
weight: 244
#id: "single-number-iii"
---
## Description
<div class="description">
<p>Given an integer array <code>nums</code>, in which exactly two elements appear only once and all the other elements appear exactly twice. Find the two elements that appear only once. You can return the answer in <strong>any order</strong>.</p>

<p><strong>Follow up:&nbsp;</strong>Your algorithm should run in linear runtime complexity. Could you implement it using only constant space complexity?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,1,3,2,5]
<strong>Output:</strong> [3,5]
<strong>Explanation: </strong> [5, 3] is also a valid answer.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [-1,0]
<strong>Output:</strong> [-1,0]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,1]
<strong>Output:</strong> [1,0]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 30000</code></li>
	<li>&nbsp;Each integer in <code>nums</code> will appear twice, only two integers will appear once.</li>
</ul>

</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Apple - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

The problem could be solved in $$\mathcal{O}(N)$$ time 
and $$\mathcal{O}(N)$$ space by using hashmap. 

To solve the problem in a constant space is a bit tricky 
but could be done with the help of two bitmasks. 

![fig](../Figures/260/two2.png)
<br /> 
<br />


---
#### Approach 1: Hashmap

Build a hashmap : element -> its frequency. Return only the 
elements with the frequency equal to 1.

**Implementation**

<iframe src="https://leetcode.com/playground/atBQ6ERZ/shared" frameBorder="0" width="100%" height="293" name="atBQ6ERZ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ to iterate over the input array. 

* Space complexity : $$\mathcal{O}(N)$$ to keep the hashmap of $$N$$ elements.
<br /> 
<br />


---
#### Approach 2: Two bitmasks 

**Prerequisites**

This article will use two bitwise
tricks, discussed in details last week :

- If one builds an array bitmask with the help of XOR operator,
following `bitmask ^= x` strategy,
the bitmask would keep only the bits which appear odd number of times. 
That was discussed in details in the article 
[Single Number II](https://leetcode.com/articles/single-number-ii/).

![fig](../Figures/260/xor3.png)

- `x & (-x)` is a way to isolate the rightmost 1-bit, 
i.e. to keep the rightmost 1-bit and to set all the others bits to zero.
Please refer to the article [Power of Two](https://leetcode.com/articles/power-of-two/)
for the detailed explanation. 

![fig](../Figures/260/isolate3.png)

**Intuition**

> An interview tip. Imagine, you have a problem to indentify an array
element (or elements), which appears exactly given number of times.
Probably, the key is to build first an array bitmask using XOR operator.
Examples: [In-Place Swap](leetcode.com/articles/single-number-ii/356460/Single-Number-II/324042), 
[Single Number](https://leetcode.com/articles/single-number/), [Single Number II](leetcode.com/articles/single-number-ii/356460/Single-Number-II/324042).
  
So let's create an array bitmask : `bitmask ^= x`. 
This bitmask will _not_ keep any number which appears twice 
because XOR of two equal bits results in a zero bit
`a^a = 0`.

Instead, the bitmask would keep only the difference
between two numbers (let's call them x and y) which appear just once.
The difference here it's the bits which are different for x and y. 

![fig](../Figures/260/diff_new.png)

> Could we extract x and y directly from this bitmask? No. 
Though we could use this bitmask as a marker to separate x and y.

Let's do `bitmask & (-bitmask)` to isolate the rightmost 1-bit,
which is different between x and y. Let's say this is 1-bit for x, 
and 0-bit for y. 

![fig](../Figures/260/isolate2_new.png)

Now let's use XOR as before, but for the new bitmask `x_bitmask`, 
which will contain only the numbers which have
1-bit in the position of `bitmask & (-bitmask)`. 
This way, this new bitmask will 
contain only number x `x_bitmask = x`, because of two reasons:

- y has 0-bit in the position `bitmask & (-bitmask)` and hence will
not enter this new bitmask. 

- All numbers but x will not be visible in this new bitmask because
they appear two times. 

![fig](../Figures/260/x_bitmask2.png)

Voila, x is identified. Now to identify y is simple: `y = bitmask^x`.

**Implementation**

<iframe src="https://leetcode.com/playground/BHEyFUxt/shared" frameBorder="0" width="100%" height="344" name="BHEyFUxt"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ to iterate over the input array. 

* Space complexity : $$\mathcal{O}(1)$$, it's a constant space solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted C++/Java O(n)-time O(1)-space Easy Solution with Detail Explanations
- Author: zhiqing_xiao
- Creation Date: Mon Aug 17 2015 15:20:30 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 10:36:13 GMT+0800 (Singapore Standard Time)

<p>
Once again, we need to use XOR to solve this problem. But this time, we need to do it in two passes:

- In the first pass, we XOR all elements in the array, and get the XOR of the two numbers we need to find. Note that since the two numbers are distinct, so there must be a set bit (that is, the bit with value '1') in the XOR result. Find
out an arbitrary set bit (for example, the rightmost set bit).

- In the second pass, we divide all numbers into two groups, one with the aforementioned bit set, another with the aforementinoed bit unset. Two different numbers we need to find must fall into thte two distrinct groups. XOR numbers in each group, we can find a number in either group.

**Complexity:**

- Time: *O* (*n*)

- Space: *O* (1)

**A Corner Case:**

- When `diff == numeric_limits<int>::min()`, `-diff` is also `numeric_limits<int>::min()`. Therefore, the value of `diff` after executing `diff &= -diff` is still `numeric_limits<int>::min()`. The answer is still correct.


C++:

    class Solution
    {
    public:
        vector<int> singleNumber(vector<int>& nums) 
        {
            // Pass 1 : 
            // Get the XOR of the two numbers we need to find
            int diff = accumulate(nums.begin(), nums.end(), 0, bit_xor<int>());
            // Get its last set bit
            diff &= -diff;

            // Pass 2 :
            vector<int> rets = {0, 0}; // this vector stores the two numbers we will return
            for (int num : nums)
            {
                if ((num & diff) == 0) // the bit is not set
                {
                    rets[0] ^= num;
                }
                else // the bit is set
                {
                    rets[1] ^= num;
                }
            }
            return rets;
        }
    };


Java:

    public class Solution {
        public int[] singleNumber(int[] nums) {
            // Pass 1 : 
            // Get the XOR of the two numbers we need to find
            int diff = 0;
            for (int num : nums) {
                diff ^= num;
            }
            // Get its last set bit
            diff &= -diff;
            
            // Pass 2 :
            int[] rets = {0, 0}; // this array stores the two numbers we will return
            for (int num : nums)
            {
                if ((num & diff) == 0) // the bit is not set
                {
                    rets[0] ^= num;
                }
                else // the bit is set
                {
                    rets[1] ^= num;
                }
            }
            return rets;
        }
    }

Thanks for reading :)


----------


Acknowledgements:

- Thank **@jianchao.li.fighter** for introducing this problem and for your encouragement.

- Thank **@StefanPochmann** for your valuable suggestions and comments. Your idea of `diff &= -diff` is very elegent! And yes, it does not need to XOR for both group in the second pass. XOR for one group suffices. I revise my code accordingly. 

- Thank **@Nakagawa_Kanon** for posting this question and presenting the same idea in a previous thread (prior to this thread).

- Thank **@caijun** for providing an interesting test case.
</p>


### Sharing explanation of the solution
- Author: douglasleer
- Creation Date: Sat Sep 26 2015 00:13:01 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 11:14:59 GMT+0800 (Singapore Standard Time)

<p>
If you were stuck by this problem, it's easy to find a solution in the discussion. However, usually, the solution lacks some explanations.

I'm sharing my understanding here:

The two numbers that appear only once must differ at some bit, this is how we can distinguish between them. Otherwise, they will be one of the duplicate numbers. 

One important point is that by XORing all the numbers, we actually get the XOR of the two target numbers (because XORing two duplicate numbers always results in 0). Consider the XOR result of the two target numbers; if some bit of the XOR result is 1, it means that the two target numbers differ at that location. 

Let\u2019s say the at the ith bit, the two desired numbers differ from each other. which means one number has bit i equaling: 0, the other number has bit i equaling 1.

Thus, all the numbers can be partitioned into two groups according to their bits at location i. 
the first group consists of all numbers whose bits at i is 0.
the second group consists of all numbers whose bits at i is 1. 

Notice that, if a duplicate number has bit i as 0, then, two copies of it will belong to the first group. Similarly, if a duplicate number has bit i as 1, then, two copies of it will belong to the second group.

by XoRing all numbers in the first group, we can get the first number.
by XoRing all numbers in the second group, we can get the second number.
</p>


### C++ solution O(n) time and O(1) space, easy-understaning with simple explanation
- Author: lchen77
- Creation Date: Fri Aug 21 2015 05:00:21 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 05:29:11 GMT+0800 (Singapore Standard Time)

<p>
    vector<int> singleNumber(vector<int>& nums) {
        int aXorb = 0;  // the result of a xor b;
        for (auto item : nums) aXorb ^= item;
        int lastBit = (aXorb & (aXorb - 1)) ^ aXorb;  // the last bit that a diffs b
        int intA = 0, intB = 0;
        for (auto item : nums) {
            // based on the last bit, group the items into groupA(include a) and groupB
            if (item & lastBit) intA = intA ^ item;
            else intB = intB ^ item;
        }
        return vector<int>{intA, intB};   
    }
</p>


