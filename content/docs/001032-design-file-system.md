---
title: "Design File System"
weight: 1032
#id: "design-file-system"
---
## Description
<div class="description">
<p>You are asked to design a file system&nbsp;that allows you to create new paths and associate them with different values.</p>

<p>The format of a path is&nbsp;one or more concatenated strings of the form:&nbsp;<code>/</code> followed by one or more lowercase English letters. For example, &quot;<code>/leetcode&quot;</code>&nbsp;and &quot;<code>/leetcode/problems&quot;</code>&nbsp;are valid paths while an empty&nbsp;string <code>&quot;&quot;</code> and <code>&quot;/&quot;</code>&nbsp;are not.</p>

<p>Implement the&nbsp;<code>FileSystem</code> class:</p>

<ul>
	<li><code>bool createPath(string path, int value)</code>&nbsp;Creates a new <code>path</code> and associates a <code>value</code> to it if possible and returns <code>true</code>.&nbsp;Returns <code>false</code>&nbsp;if the path <strong>already exists</strong> or its parent path <strong>doesn&#39;t exist</strong>.</li>
	<li><code>int get(string path)</code>&nbsp;Returns the value associated with <code>path</code> or returns&nbsp;<code>-1</code>&nbsp;if the path doesn&#39;t exist.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 
[&quot;FileSystem&quot;,&quot;createPath&quot;,&quot;get&quot;]
[[],[&quot;/a&quot;,1],[&quot;/a&quot;]]
<strong>Output:</strong> 
[null,true,1]
<strong>Explanation:</strong> 
FileSystem fileSystem = new FileSystem();

fileSystem.createPath(&quot;/a&quot;, 1); // return true
fileSystem.get(&quot;/a&quot;); // return 1
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> 
[&quot;FileSystem&quot;,&quot;createPath&quot;,&quot;createPath&quot;,&quot;get&quot;,&quot;createPath&quot;,&quot;get&quot;]
[[],[&quot;/leet&quot;,1],[&quot;/leet/code&quot;,2],[&quot;/leet/code&quot;],[&quot;/c/d&quot;,1],[&quot;/c&quot;]]
<strong>Output:</strong> 
[null,true,true,2,false,-1]
<strong>Explanation:</strong> 
FileSystem fileSystem = new FileSystem();

fileSystem.createPath(&quot;/leet&quot;, 1); // return true
fileSystem.createPath(&quot;/leet/code&quot;, 2); // return true
fileSystem.get(&quot;/leet/code&quot;); // return 2
fileSystem.createPath(&quot;/c/d&quot;, 1); // return false because the parent path &quot;/c&quot; doesn&#39;t exist.
fileSystem.get(&quot;/c&quot;); // return -1 because this path doesn&#39;t exist.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of&nbsp;calls to the two functions&nbsp;is less than or equal to <code>10<sup>4</sup></code> in total.</li>
	<li><code>2 &lt;= path.length &lt;= 100</code></li>
	<li><code>1 &lt;= value &lt;= 10<sup>9</sup></code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Design (design)

## Companies
- Airbnb - 6 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] 7 line simple HashMap code w/ analysis.
- Author: rock
- Creation Date: Sun Aug 25 2019 00:40:41 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 27 2019 11:48:24 GMT+0800 (Singapore Standard Time)

<p>
If `path` already exist or its parent path non-exist, return false; Otherwise, create a new path and value.

**Java**
```
    Map<String, Integer> file = new HashMap<>(); 
    
    public FileSystem() {
        file.put("", -1);
    }
    
    public boolean create(String path, int value) {
        int idx = path.lastIndexOf("/");
        String parent = path.substring(0, idx);
        if (!file.containsKey(parent)) { return false; }
        return file.putIfAbsent(path, value) == null;   
    }
    
    public int get(String path) {
        return file.getOrDefault(path, -1);
    }

```

----

**Python 3**
```
    def __init__(self):
        self.d = {"" : -1}

    def create(self, path: str, value: int) -> bool:
        parent = path[:path.rfind(\'/\')]
        if parent in self.d and path not in self.d:
            self.d[path] = value
            return True
        return False

    def get(self, path: str) -> int:
        return self.d.get(path, -1)
```

**Analysis:**

Time & Space:
create(): `O(path.length())`, get(): `O(1)`.
 
 space:
 file(): 
 In worst case, e.g., `path = "/a/b/c/d/e/f/g/..."`, all the path family cost `2 + 4 + 6 + ... + 2n = n * (n + 1)`. So the space cost `O(path.length() ^ 2)`.
</p>


### Java/Python TrieTree solution
- Author: haiou0987654321
- Creation Date: Sun Aug 25 2019 05:10:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 25 2019 05:16:49 GMT+0800 (Singapore Standard Time)

<p>
Java:
```
class FileSystem {
    class File{
        String name;
        int val = -1;
        Map<String, File> map = new HashMap<>();
        
        File(String name){
            this.name = name;
        }
    }
    File root;
    public FileSystem() {
        root = new File("");
    }
    
    public boolean create(String path, int value) {
        String[] array = path.split("/");
        File cur = root;
        
        for(int i=1;i<array.length;i++){
            String cur_name = array[i];
            if(cur.map.containsKey(cur_name)==false){
                if(i==array.length-1){
                    cur.map.put(cur_name, new File(cur_name));
                }else{
                    return false;
                }
            }
            cur = cur.map.get(cur_name);
        }
        
        if(cur.val!=-1){
            return false;
        }
        
        cur.val = value;
        return true;
    }
    
    public int get(String path) {
        String[] array = path.split("/");
        File cur = root;
        for(int i=1;i<array.length;i++){
            String cur_name = array[i];
            if(cur.map.containsKey(cur_name)==false){
                return -1;
            }
            cur = cur.map.get(cur_name);
        }
        
        return cur.val;
        
        
    }
}

/**
 * Your FileSystem object will be instantiated and called as such:
 * FileSystem obj = new FileSystem();
 * boolean param_1 = obj.create(path,value);
 * int param_2 = obj.get(path);
 */
```

Python:
```
from collections import defaultdict
class File(object):
    def __init__(self, name):
        self.map = defaultdict(File)
        self.name = name
        self.value = -1
        
class FileSystem(object):

    def __init__(self):
        self.root = File("")

    def create(self, path, value):
        """
        :type path: str
        :type value: int
        :rtype: bool
        """
        array = path.split("/")
        cur = self.root
        for i in range(1, len(array)):
            name = array[i]
            if name not in cur.map:
                if i==len(array)-1:
                    cur.map[name] = File(name)
                else:
                    return False
            cur = cur.map[name]
        
        if cur.value!=-1:
            return False
        cur.value = value
        
        return True
        

    def get(self, path):
        """
        :type path: str
        :rtype: int
        """
        cur = self.root
        array = path.split("/")
        for i in range(1, len(array)):
            name = array[i]
            if name not in cur.map:
                return -1
            cur = cur.map[name]
        return cur.value
        


# Your FileSystem object will be instantiated and called as such:
# obj = FileSystem()
# param_1 = obj.create(path,value)
# param_2 = obj.get(path)
```
</p>


### [C++] Hash map lookup and parent-check
- Author: mhelvens
- Creation Date: Sun Aug 25 2019 00:02:20 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 09 2019 02:46:04 GMT+0800 (Singapore Standard Time)

<p>
This is just a straight-forward application of a hash-map, with an additional check for the existence of the parent path.

_Note:_ Though I opted to keep this code simple, more efficient data-structures are possible. A hash map lookup is still _O(n)_ by the length of the string. So we could actually store this as a kind of [Trie](https://en.wikipedia.org/wiki/Trie), with a node for every directory (segment). That way, the parent lookup would already bring us partway towards the main path lookup.

```C++
class FileSystem {
public:
  bool createPath(const string& path, int value) {
    auto parent = path.substr(0, path.rfind(\'/\'));
    if (!parent.empty() && !paths.count(parent))
      return false;
    return paths.emplace(path, value).second;
  }
    
  int get(const string& path) {
    auto it = paths.find(path);
    if (it == paths.end())
      return -1;
    return it->second;
  }
  
private:
  unordered_map<string, int> paths;
};
```
</p>


