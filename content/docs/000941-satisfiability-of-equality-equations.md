---
title: "Satisfiability of Equality Equations"
weight: 941
#id: "satisfiability-of-equality-equations"
---
## Description
<div class="description">
<p>Given an array <font face="monospace">equations</font>&nbsp;of strings that represent relationships between variables, each string <code>equations[i]</code>&nbsp;has length <code>4</code> and takes one of two different forms: <code>&quot;a==b&quot;</code> or <code>&quot;a!=b&quot;</code>.&nbsp; Here, <code>a</code> and <code>b</code> are lowercase letters (not necessarily different) that represent one-letter variable names.</p>

<p>Return <code>true</code>&nbsp;if and only if it is possible to assign integers to variable names&nbsp;so as to satisfy all the given equations.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;a==b&quot;,&quot;b!=a&quot;]</span>
<strong>Output: </strong><span id="example-output-1">false</span>
<strong>Explanation: </strong>If we assign say, a = 1 and b = 1, then the first equation is satisfied, but not the second.  There is no way to assign the variables to satisfy both equations.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[&quot;b==a&quot;,&quot;a==b&quot;]</span>
<strong>Output: </strong><span id="example-output-2">true</span>
<strong>Explanation: </strong>We could assign a = 1 and b = 1 to satisfy both equations.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[&quot;a==b&quot;,&quot;b==c&quot;,&quot;a==c&quot;]</span>
<strong>Output: </strong><span id="example-output-3">true</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">[&quot;a==b&quot;,&quot;b!=c&quot;,&quot;c==a&quot;]</span>
<strong>Output: </strong><span id="example-output-4">false</span>
</pre>

<div>
<p><strong>Example 5:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-5-1">[&quot;c==c&quot;,&quot;b==d&quot;,&quot;x!=z&quot;]</span>
<strong>Output: </strong><span id="example-output-5">true</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= equations.length &lt;= 500</code></li>
	<li><code>equations[i].length == 4</code></li>
	<li><code>equations[i][0]</code> and <code>equations[i][3]</code> are lowercase letters</li>
	<li><code>equations[i][1]</code> is either <code>&#39;=&#39;</code> or <code>&#39;!&#39;</code></li>
	<li><code>equations[i][2]</code> is&nbsp;<code>&#39;=&#39;</code></li>
</ol>
</div>
</div>
</div>
</div>
</div>

</div>

## Tags
- Union Find (union-find)
- Graph (graph)

## Companies
- Sumologic - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Connected Components

**Intuition**

All variables that are equal to each other form connected components.  For example, if `a=b, b=c, c=d` then `a, b, c, d` are in the same connected component as they all must be equal to each other.

**Algorithm**

First, we use a depth first search to color each variable by connected component based on these equality equations.

After coloring these components, we can parse statements of the form `a != b`.  If two components have the same color, then they must be equal, so if we say they can't be equal then it is impossible to satisfy the equations.

Otherwise, our coloring demonstrates a way to satisfy the equations, and thus the result is true.

<iframe src="https://leetcode.com/playground/NFGZK5mi/shared" frameBorder="0" width="100%" height="500" name="NFGZK5mi"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$ where $$N$$ is the length of `equations`.

* Space Complexity:  $$O(1)$$, assuming the size of the alphabet is $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy Union Find
- Author: lee215
- Creation Date: Sun Feb 10 2019 12:03:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 10 2019 12:03:41 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**:
We have 26 nodes in the graph.
All "==" equations actually represent the connection in the graph.
The connected nodes should be in the same color/union/set.

Then we check all inequations. 
Two inequal nodes should be in the different color/union/set.


**Explanation**
We can solve this problem by DFS or Union Find.

Firt pass all "==" equations.
Union equal letters together
Now we know which letters must equal to the others.

Second pass all "!=" inequations,
Check if there are any contradict happens.

**Time Complexity**:
Union Find Operation, amortized `O(1)`
First pass all equations, `O(N)`
Second pass all inequations, `O(N)`

Overall `O(N)`

<br>

**Java:**
```
    int[] uf = new int[26];
    public boolean equationsPossible(String[] equations) {
        for (int i = 0; i < 26; ++i) uf[i] = i;
        for (String e : equations)
            if (e.charAt(1) == \'=\')
                uf[find(e.charAt(0) - \'a\')] = find(e.charAt(3) - \'a\');
        for (String e : equations)
            if (e.charAt(1) == \'!\' && find(e.charAt(0) - \'a\') == find(e.charAt(3) - \'a\'))
                return false;
        return true;
    }

    public int find(int x) {
        if (x != uf[x]) uf[x] = find(uf[x]);
        return uf[x];
    }
```

**C++:**
```
    int uf[26];
    bool equationsPossible(vector<string>& equations) {
        for (int i = 0; i < 26; ++i) uf[i] = i;
        for (string e : equations)
            if (e[1] == \'=\')
                uf[find(e[0] - \'a\')] = find(e[3] - \'a\');
        for (string e : equations)
            if (e[1] == \'!\' && find(e[0] - \'a\') == find(e[3] - \'a\'))
                return false;
        return true;
    }

    int find(int x) {
        if (x != uf[x]) uf[x] = find(uf[x]);
        return uf[x];
    }
```

**Python:**
```
    def equationsPossible(self, equations):
        def find(x):
            if x != uf[x]: uf[x] = find(uf[x])
            return uf[x]
        uf = {a: a for a in string.lowercase}
        for a, e, _, b in equations:
            if e == "=":
                uf[find(a)] = find(b)
        return not any(e == "!" and find(a) == find(b) for a, e, _, b in equations)
```

</p>


### C++ 7 lines with picture, union-find
- Author: votrubac
- Creation Date: Sun Feb 10 2019 12:01:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 10 2019 12:01:22 GMT+0800 (Singapore Standard Time)

<p>
First, going through equality equations and use the union operation to join the variables in one set.

Second, going through inequality equations and verify that the variables are not in the same set using the find operation.

As a refresher, the picture below demonstrates how the disjoined set structure changes based on the equality equations.
![image](https://assets.leetcode.com/users/votrubac/image_1549773815.png)
In the end, there are two sets ```[a, c, d, f]``` and ```[b, e, g]```.
```
int uf_find(vector<int> &uf, int i) {
  return uf[i] == -1 || uf[i] == i ? i : uf_find(uf, uf[i]);
}
bool equationsPossible(vector<string>& equations) {
  vector<int> uf(\'z\' + 1, -1);
  for (auto s : equations)
    if (s[1] == \'=\') uf[uf_find(uf, s[0])] = uf_find(uf, s[3]);
  for (auto s : equations)
    if (s[1] == \'!\' && uf_find(uf, s[0]) == uf_find(uf, s[3])) return false;
  return true;
}
```
</p>


### [Python3] Easy DFS
- Author: localhostghost
- Creation Date: Sun Feb 10 2019 12:05:01 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 02 2020 23:18:17 GMT+0800 (Singapore Standard Time)

<p>
Bascially we are making a graph. 
If  `a == b` we will have two edges: `a->b` and `b->a`.
After we construct the graph, we check all the `x != y`
and make sure they are not able to visit each other.

```
class Solution:
    def equationsPossible(self, equations: \'List[str]\') -> \'bool\':
        graph = collections.defaultdict(set)
        notEquals = []
        
        def canMeet(u, target, visited):
            if u == target:
                return True 
            visited.add(u)
            for v in graph[u]:
                if v in visited:
                    continue
                if canMeet(v, target, visited):
                    return True 
            return False    
        
        for eq in equations:
            if eq[1:3] == \'!=\':
                a, b = eq.split(\'!=\')
                notEquals.append((a, b))
                continue
            u, v = eq.split(\'==\')    
            graph[u].add(v)
            graph[v].add(u)
                
        for u, v in notEquals:
            if canMeet(u, v, set()):
                return False
        return True  
```
</p>


