---
title: "Largest Number"
weight: 175
#id: "largest-number"
---
## Description
<div class="description">
<p>Given a list of non-negative integers <code>nums</code>, arrange them such that they form the largest number.</p>

<p><strong>Note:</strong> The result may be very large, so you need to return a string instead of an integer.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [10,2]
<strong>Output:</strong> &quot;210&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,30,34,5,9]
<strong>Output:</strong> &quot;9534330&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1]
<strong>Output:</strong> &quot;1&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [10]
<strong>Output:</strong> &quot;10&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 100</code></li>
	<li><code>0 &lt;= nums[i] &lt;= 10<sup>9</sup></code></li>
</ul>

</div>

## Tags
- Sort (sort)

## Companies
- Microsoft - 4 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Works Applications - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1 Sorting via Custom Comparator [Accepted]

**Intuition**

To construct the largest number, we want to ensure that the most significant
digits are occupied by the largest digits.

**Algorithm**

First, we convert each integer to a string. Then, we sort the array of strings.

While it might be tempting to simply sort the numbers in descending order,
this causes problems for sets of numbers with the same leading digit. For
example, sorting the problem example in descending order would produce the
number $$9534303$$, while the correct answer can be achieved by transposing
the $$3$$ and the $$30$$. Therefore, for each pairwise comparison during the
sort, we compare the numbers achieved by concatenating the pair in both
orders. We can prove that this sorts into the proper order as follows: 

Assume that (without loss of generality), for some pair of integers $$a$$ and
$$b$$, our comparator dictates that $$a$$ should precede $$b$$ in sorted
order. This means that $$a\frown b > b\frown a$$ (where $$\frown$$ represents
concatenation). For the sort to produce an incorrect ordering, there must be
some $$c$$ for which $$b$$ precedes $$c$$ and $$c$$ precedes $$a$$. This is a
contradiction because $$a\frown b > b\frown a$$ and $$b\frown c > c\frown b$$
implies $$a\frown c > c\frown a$$. In other words, our custom comparator
preserves transitivity, so the sort is correct.

Once the array is sorted, the most "signficant" number will be at the front.
There is a minor edge case that comes up when the array consists of only
zeroes, so if the most significant number is $$0$$, we can simply return
$$0$$. Otherwise, we build a string out of the sorted array and return it.

<iframe src="https://leetcode.com/playground/wVZb2DmS/shared" frameBorder="0" width="100%" height="500" name="wVZb2DmS"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(nlgn)$$

    Although we are doing extra work in our comparator, it is only by a
    constant factor. Therefore, the overall runtime is dominated by the
    complexity of `sort`, which is $$\mathcal{O}(nlgn)$$ in Python and Java.

* Space complexity : $$\mathcal{O}(n)$$

    Here, we allocate $$\mathcal{O}(n)$$ additional space to store the copy of `nums`.
    Although we could do that work in place (if we decide that it is okay to
    modify `nums`), we must allocate $$\mathcal{O}(n)$$ space for the final return
    string. Therefore, the overall memory footprint is linear in the length
    of `nums`.

---

Analysis and solutions written by: [@emptyset](https://leetcode.com/emptyset)

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My Java Solution to share
- Author: ran3
- Creation Date: Wed Jan 28 2015 07:33:29 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 20 2020 06:14:25 GMT+0800 (Singapore Standard Time)

<p>
The idea here is basically implement a String comparator to decide which String  should come first during concatenation. Because when you have 2 numbers (let\'s convert them into String), you\'ll face only 2 cases:
For example:
```
String s1 = "9";
String s2 = "31";

String case1 =  s1 + s2; // 931
String case2 = s2 + s1; // 319

```
Apparently, case1 is greater than case2 in terms of value.
So, we should always put s1 in front of s2.

I have received many good suggestions from you in this discussion. Below is the modified version of codes based on your suggestions:

```
public class Solution {
     public String largestNumber(int[] num) {
		if(num == null || num.length == 0)
		    return "";
		
		// Convert int array to String array, so we can sort later on
		String[] s_num = new String[num.length];
		for(int i = 0; i < num.length; i++)
		    s_num[i] = String.valueOf(num[i]);
			
		// Comparator to decide which string should come first in concatenation
		Comparator<String> comp = new Comparator<String>(){
		    @Override
		    public int compare(String str1, String str2){
		        String s1 = str1 + str2;
				String s2 = str2 + str1;
				return s2.compareTo(s1); // reverse order here, so we can do append() later
		    }
	     };
		
		Arrays.sort(s_num, comp);
		// An extreme edge case by lc, say you have only a bunch of 0 in your int array
		if(s_num[0].charAt(0) == \'0\')
			return "0";
            
		StringBuilder sb = new StringBuilder();
		for(String s: s_num)
	        sb.append(s);
		
		return sb.toString();
		
	}
}
```

In terms of Time and Space Complexity:
Let\'s assume:
the length of input array is `n`,
average length of Strings in s_num is `k`,
Then, compare 2 strings will take `O(k)`.
Sorting will take `O(nlgn)`
Appending to StringBuilder takes `O(n)`.

*Updates:* according to how merge sort time complexity is calculated from this [post](https://cs.stackexchange.com/questions/54525/how-to-calculate-the-mergesort-time-complexity)
We still need `lg(n)` times of operations, but the comparison at the leaf node takes `O(k)` instead of `O(1)`
Thus, the total will be `O(n*k*lgn) + O(n) = O(nklgn).`

Space is pretty straight forward: `O(n)`.
</p>


### A simple C++ solution
- Author: isaac7
- Creation Date: Wed Jan 14 2015 10:31:06 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 22:36:16 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        string largestNumber(vector<int> &num) {
            vector<string> arr;
            for(auto i:num)
                arr.push_back(to_string(i));
            sort(begin(arr), end(arr), [](string &s1, string &s2){ return s1+s2>s2+s1; });
            string res;
            for(auto s:arr)
                res+=s;
            while(res[0]=='0' && res.length()>1)
                res.erase(0,1);
            return  res;
        }
    };
</p>


### Python different solutions (bubble, insertion, selection, merge, quick sorts).
- Author: OldCodingFarmer
- Creation Date: Sun Aug 23 2015 00:03:39 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 23:21:02 GMT+0800 (Singapore Standard Time)

<p>
    
    # build-in function
    def largestNumber1(self, nums):
        if not any(nums):
            return "0"
        return "".join(sorted(map(str, nums), cmp=lambda n1, n2: -1 if n1+n2>n2+n1 else (1 if n1+n2<n2+n1 else 0)))
        
    # bubble sort
    def largestNumber2(self, nums):
        for i in xrange(len(nums), 0, -1):
            for j in xrange(i-1):
                if not self.compare(nums[j], nums[j+1]):
                    nums[j], nums[j+1] = nums[j+1], nums[j]
        return str(int("".join(map(str, nums))))
        
    def compare(self, n1, n2):
        return str(n1) + str(n2) > str(n2) + str(n1)
        
    # selection sort
    def largestNumber3(self, nums):
        for i in xrange(len(nums), 0, -1):
            tmp = 0
            for j in xrange(i):
                if not self.compare(nums[j], nums[tmp]):
                    tmp = j
            nums[tmp], nums[i-1] = nums[i-1], nums[tmp]
        return str(int("".join(map(str, nums))))
        
    # insertion sort
    def largestNumber4(self, nums):
        for i in xrange(len(nums)):
            pos, cur = i, nums[i]
            while pos > 0 and not self.compare(nums[pos-1], cur):
                nums[pos] = nums[pos-1]  # move one-step forward
                pos -= 1
            nums[pos] = cur
        return str(int("".join(map(str, nums))))
    
    # merge sort        
    def largestNumber5(self, nums):
        nums = self.mergeSort(nums, 0, len(nums)-1)
        return str(int("".join(map(str, nums))))
        
    def mergeSort(self, nums, l, r):
        if l > r:
            return 
        if l == r:
            return [nums[l]]
        mid = l + (r-l)//2
        left = self.mergeSort(nums, l, mid)
        right = self.mergeSort(nums, mid+1, r)
        return self.merge(left, right)
        
    def merge(self, l1, l2):
        res, i, j = [], 0, 0
        while i < len(l1) and j < len(l2):
            if not self.compare(l1[i], l2[j]):
                res.append(l2[j])
                j += 1
            else:
                res.append(l1[i])
                i += 1
        res.extend(l1[i:] or l2[j:])
        return res
        
    # quick sort, in-place
    def largestNumber(self, nums):
        self.quickSort(nums, 0, len(nums)-1)
        return str(int("".join(map(str, nums)))) 
    
    def quickSort(self, nums, l, r):
        if l >= r:
            return 
        pos = self.partition(nums, l, r)
        self.quickSort(nums, l, pos-1)
        self.quickSort(nums, pos+1, r)
        
    def partition(self, nums, l, r):
        low = l
        while l < r:
            if self.compare(nums[l], nums[r]):
                nums[l], nums[low] = nums[low], nums[l]
                low += 1
            l += 1
        nums[low], nums[r] = nums[r], nums[low]
        return low
</p>


