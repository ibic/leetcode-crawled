---
title: "Smallest Sufficient Team"
weight: 1100
#id: "smallest-sufficient-team"
---
## Description
<div class="description">
<p>In a project, you have a list of required skills <code>req_skills</code>,&nbsp;and a list of <code>people</code>.&nbsp; The i-th person <code>people[i]</code>&nbsp;contains a list of skills that person has.</p>

<p>Consider a <em>sufficient team</em>: a set of people such that for every required skill in <code>req_skills</code>, there is at least one person in the team who has that skill.&nbsp; We can represent these teams by the index of each person: for example, <code>team = [0, 1, 3]</code> represents the people with skills <code>people[0]</code>, <code>people[1]</code>, and <code>people[3]</code>.</p>

<p>Return <strong>any</strong>&nbsp;sufficient team of the smallest possible size, represented by the index of each person.</p>

<p>You may return the answer in any order.&nbsp; It is guaranteed an answer exists.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> req_skills = ["java","nodejs","reactjs"], people = [["java"],["nodejs"],["nodejs","reactjs"]]
<strong>Output:</strong> [0,2]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> req_skills = ["algorithms","math","java","reactjs","csharp","aws"], people = [["algorithms","math","java"],["algorithms","math","reactjs"],["java","csharp","aws"],["reactjs","csharp"],["csharp","math"],["aws","java"]]
<strong>Output:</strong> [1,2]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= req_skills.length &lt;= 16</code></li>
	<li><code>1 &lt;= people.length &lt;= 60</code></li>
	<li><code>1 &lt;= people[i].length, req_skills[i].length, people[i][j].length&nbsp;&lt;= 16</code></li>
	<li>Elements of <code>req_skills</code> and <code>people[i]</code> are (respectively) distinct.</li>
	<li><code>req_skills[i][j], people[i][j][k]</code> are&nbsp;lowercase English letters.</li>
	<li>Every skill in <code>people[i]</code>&nbsp;is a skill in <code>req_skills</code>.</li>
	<li>It is guaranteed a sufficient team exists.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Bit Manipulation (bit-manipulation)

## Companies
- Google - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] DP Solution
- Author: lee215
- Creation Date: Sun Jul 14 2019 12:02:30 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 13 2020 00:27:01 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
`dp[skill_set]` is a sufficient team to cover the `skill_set`.
For each `people`,
update `his_skill` with all current combinations of `skill_set` in `dp`.
<br>

## **Complexity**
Time `O(people * 2^skill)`
Space `O(2^skill)`
<br>

**Python:**
```python
    def smallestSufficientTeam(self, req_skills, people):
        n, m = len(req_skills), len(people)
        key = {v: i for i, v in enumerate(req_skills)}
        dp = {0: []}
        for i, p in enumerate(people):
            his_skill = 0
            for skill in p:
                if skill in key:
                    his_skill |= 1 << key[skill]
            for skill_set, need in dp.items():
                with_him = skill_set | his_skill
                if with_him == skill_set: continue
                if with_him not in dp or len(dp[with_him]) > len(need) + 1:
                    dp[with_him] = need + [i]
        return dp[(1 << n) - 1]
```

</p>


### c++ dp bitmask solution, with algorithm
- Author: goelrishabh5
- Creation Date: Sun Jul 14 2019 16:09:33 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 16 2019 18:08:56 GMT+0800 (Singapore Standard Time)

<p>
1. Enumerate given skills. Their index will act as index of bit when we do bitmasking. Also create a set to store diff combination of skills (bitwise).
2. for each people :
2.1  calculate bitmask of all its skill
	for each prev bitmask in our set:
	2.1.1 calculate combo of prev and curr skill
	2.1.2  if  combo doesn\'t exist already , add it with this people id
	2.1.3  if combo bitmask exists but with a larger group of people, replace it with current people and group of people with prev bitmask

3. return group of people with bitmask (1<<(skills.size())) -1.
```
		int n = req_skills.size();
		unordered_map<int,vector<int>> res;  // using unordered_map, we improve on time
        res.reserve(1 << n);    // using reserved space, we avoid rehash
        //map<int,vector<int>> res;
        res[0]={};
        unordered_map<string,int> skill_map;
        for(int i=0;i< req_skills.size();i++)
            skill_map[req_skills[i]]=i;
        
        for(int i=0;i<people.size();i++)
        {
            int curr_skill = 0;
            for(int j=0;j<people[i].size();j++)
                curr_skill |= 1<<skill_map[people[i][j]];
            
            for(auto it = res.begin();it!=res.end();it++)
            {
                int comb = it->first | curr_skill;
                if(res.find(comb)==res.end() || res[comb].size()>1+res[it->first].size())
                {
                    res[comb]=it->second;
                    res[comb].push_back(i);
                }       
            }
        }
        return res[(1<<n) -1];
```
</p>


### Python - Optimized backtracking with explanation and code comments [88 ms]
- Author: Hai_dee
- Creation Date: Sun Jul 14 2019 12:15:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 14 2019 12:41:43 GMT+0800 (Singapore Standard Time)

<p>
The first important observation to make is that this problem has that NP-Complete feeling around it. With that, we know that we\'re probably looking at a pseudo-polynomial algorithm at best (e.g. dynamic programming), or a well optimised non-polynomial algorithm. Given how small the number of skills is, and the number of people, the latter should seem more likely. Therefore, we should be focussing on the best way of doing that, rather than trying to find a true polynomial approach.

The approach I took was a backtracking one. Using a map of skills to people with them and a set of currently unmet skills, go through each skill, considering each person that could perform the skill. Remove all skills that person can perform from the unmet skills set, recursively call on the next skill, and then afterwards return the person\'s skills to the unmet set (backtracking).

In order to speed it up a bit, I made one other improvement. Before doing the backtracking, the algorithm clears the skill sets of all people who are a subset of another. The sets are replaced with an empty set so that indexes aren\'t messed up (which would lead to a lot of bother and hassle having to maintain a map, for no efficiency improvement)

Looking at Wikipedia afterwards, this is indeed the [Set Cover Problem](https://en.wikipedia.org/wiki/Set_cover_problem), an NP Complete problem.

In terms of run time, this algorithm completed in 88 ms, which is probably comparable to the DP approaches. The optimizations at the start make a big difference! (Without them, it\'s TLE)

Comments are in the code. Reading through that is probably the easiest way to understand. Enjoy!

```
class Solution:
    def smallestSufficientTeam(self, req_skills: List[str], people: List[List[str]]) -> List[int]:
        
        # Firstly, convert all the sublists in people into sets for easier processing.
        for i, skills in enumerate(people):
            people[i] = set(skills)
        
        # Remove all skill sets that are subset of another skillset, by replacing the subset with an
        # empty set. We do this rather than completely removing, so that indexes aren\'t 
        # disrupted (which is a pain to have to sort out later).
        for i, i_skills in enumerate(people):
            for j, j_skills in enumerate(people):
                if i != j and i_skills.issubset(j_skills):
                    people[i] = set()
        
        # Now build up a dictionary of skills to the people who can perform them. The backtracking algorithm
        # will use this.
        skills_to_people = collections.defaultdict(set)
        for i, skills in enumerate(people):
            for skill in skills:
                skills_to_people[skill].add(i)
            people[i] = set(skills)
        
        # Keep track of some data used by the backtracking algorithm.
        self.unmet_skills = set(req_skills) # Backtracking will remove and readd skills here as needed.
        self.smallest_length = math.inf # Smallest team length so far.
        self.current_team = [] # Current team members.
        self.best_team = [] # Best team we\'ve found, i,e, shortest team that covers skills/
        
		# Here is the backtracking algorithm.
        def meet_skill(skill=0):
			# Base case: All skills are met.
            if not self.unmet_skills:
				# If the current team is smaller than the previous we found, update it.
                if self.smallest_length > len(self.current_team):
                    self.smallest_length = len(self.current_team)
                    self.best_team = self.current_team[::] # In Python, this makes a copy of a list.
                return # So that we don\'t carry out the rest of the algorithm.
                        
            # If this skill is already met, move onto the next one.
            if req_skills[skill] not in self.unmet_skills:
                return meet_skill(skill + 1)
				# Note return is just to stop rest of code here running. Return values
				# are not caught and used.
            
            # Otherwise, consider all who could meet the current skill.
            for i in skills_to_people[req_skills[skill]]:
                
				# Add this person onto the team by updating the backtrading data.
                skills_added_by_person = people[i].intersection(self.unmet_skills)
                self.unmet_skills = self.unmet_skills - skills_added_by_person
                self.current_team.append(i)
                
				# Do the recursive call to further build the team.
                meet_skill(skill + 1)
                
                # Backtrack by removing the person from the team again.
                self.current_team.pop()
                self.unmet_skills = self.unmet_skills.union(skills_added_by_person)
        
		# Kick off the algorithm.
        meet_skill()        
        return self.best_team 
```
</p>


