---
title: "Rotate Function"
weight: 379
#id: "rotate-function"
---
## Description
<div class="description">
<p>
Given an array of integers <code>A</code> and let <i>n</i> to be its length.
</p>

<p>
Assume <code>B<sub>k</sub></code> to be an array obtained by rotating the array <code>A</code> <i>k</i> positions clock-wise, we define a "rotation function" <code>F</code> on <code>A</code> as follow:
</p>

<p>
<code>F(k) = 0 * B<sub>k</sub>[0] + 1 * B<sub>k</sub>[1] + ... + (n-1) * B<sub>k</sub>[n-1]</code>.</p>

<p>Calculate the maximum value of <code>F(0), F(1), ..., F(n-1)</code>. 
</p>

<p><b>Note:</b><br />
<i>n</i> is guaranteed to be less than 10<sup>5</sup>.
</p>

<p><b>Example:</b>
<pre>
A = [4, 3, 2, 6]

F(0) = (0 * 4) + (1 * 3) + (2 * 2) + (3 * 6) = 0 + 3 + 4 + 18 = 25
F(1) = (0 * 6) + (1 * 4) + (2 * 3) + (3 * 2) = 0 + 4 + 6 + 6 = 16
F(2) = (0 * 2) + (1 * 6) + (2 * 4) + (3 * 3) = 0 + 6 + 8 + 9 = 23
F(3) = (0 * 3) + (1 * 2) + (2 * 6) + (3 * 4) = 0 + 2 + 12 + 12 = 26

So the maximum value of F(0), F(1), F(2), F(3) is F(3) = 26.
</pre>
</p>
</div>

## Tags
- Math (math)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n) solution with explanation
- Author: oreomilkshake
- Creation Date: Mon Sep 12 2016 07:25:24 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 21:47:50 GMT+0800 (Singapore Standard Time)

<p>
```
F(k) = 0 * Bk[0] + 1 * Bk[1] + ... + (n-1) * Bk[n-1]
F(k-1) = 0 * Bk-1[0] + 1 * Bk-1[1] + ... + (n-1) * Bk-1[n-1]
       = 0 * Bk[1] + 1 * Bk[2] + ... + (n-2) * Bk[n-1] + (n-1) * Bk[0]
```
Then,
```
F(k) - F(k-1) = Bk[1] + Bk[2] + ... + Bk[n-1] + (1-n)Bk[0]
              = (Bk[0] + ... + Bk[n-1]) - nBk[0]
              = sum - nBk[0]
```
Thus,
```
F(k) = F(k-1) + sum - nBk[0]
```

What is Bk[0]?
```
k = 0; B[0] = A[0];
k = 1; B[0] = A[len-1];
k = 2; B[0] = A[len-2];
...
```
```java
int allSum = 0;
int len = A.length;
int F = 0;
for (int i = 0; i < len; i++) {
    F += i * A[i];
    allSum += A[i];
}
int max = F;
for (int i = len - 1; i >= 1; i--) {
    F = F + allSum - len * A[i];
    max = Math.max(F, max);
}
return max;   
```
</p>


### Java Solution O(n) with non mathametical explaination
- Author: chiranjeeb2
- Creation Date: Tue Sep 13 2016 09:39:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 09:37:37 GMT+0800 (Singapore Standard Time)

<p>
Consider we have 5 coins A,B,C,D,E

According to the problem statement 
F(0) = (0*A)  +  (1*B) +  (2*C) + (3*D) + (4*E)
F(1) = (4*A)  +  (0*B) +  (1*C) + (2*D) + (3*E)
F(2) = (3*A)  +  (4*B) +  (0*C) + (1*D) + (2*E)

This problem at a glance seem like a difficult problem. I am not very strong in mathematics, so this is how I visualize this problem 

We can construct F(1) from F(0) by two step:
Step 1. taking away one count of each coin from F(0), this is done by subtracting "sum" from "iteration" in the code below
after step 1 F(0) = (-1*A)  +  (0*B) +  (1*C) + (2*D) + (3*E)

Step 2. Add n times the element which didn't contributed in F(0), which is A. This is done by adding "A[j-1]*len" in the code below.
after step 2 F(0) = (4*A)  +  (0*B) +  (1*C) + (2*D) + (3*E)  

At this point F(0) can be considered as F(1) and F(2) to F(4) can be constructed by repeating the above steps. 

Hope this explanation helps, cheers!

  
```
    public int maxRotateFunction(int[] A) {
        if(A.length == 0){
            return 0;
        }
        
        int sum =0, iteration = 0, len = A.length;
        
        for(int i=0; i<len; i++){
            sum += A[i];
            iteration += (A[i] * i);
        }
        
        int max = iteration;
        for(int j=1; j<len; j++){
            // for next iteration lets remove one entry value of each entry and the prev 0 * k
            iteration = iteration - sum + A[j-1]*len;
            max = Math.max(max, iteration);
        }
        
        return max;
    }
```
</p>


### 6 - lines Python O(N) time O(1) space with explanation
- Author: kien_the_sun
- Creation Date: Mon Nov 19 2018 21:31:52 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 19 2018 21:31:52 GMT+0800 (Singapore Standard Time)

<p>
suppose at a point i the array is A[0], A[1], A[2], A[3],..., A[k-1], A[k] then we have : 
```
  f(i)          = 0 * A[0] + 1 * A[1] + 2 * A[2] + .... +  (k-1) * A[k-1] + k * A[k]
  f(i+1)        = 1 * A[0] + 2 * A[1] + 3 * A[2] + ...  +     k  * A[k-1] + 0 * A[k] 
=>f(i+1) - f(i) =     A[0]   +   A[1]   +   A[2] + ...  +       A[k-1]    - k * A[k] 
   = (A[0]   +   A[1]   +   A[2] + ...  +       A[k-1] + k * A[k]) - (k+1) * A[k]
   = sum(Array) - A[k] * array.length
=> f(i+1) = f(i) + sum(Array) -  last element * array.length
``` 
so at a point i if we know f(i) then we could easily caculate f(i+1) by the above formula so just go through the array and update the maximum value.
here is  my code:
```
class Solution(object):
    def maxRotateFunction(self, A):
        r = curr = sum(i * j for i,j in enumerate(A))
        s = sum(A)
        k = len(A)
        while A:
            curr += s - A.pop() * k
            r = max(curr, r)
        return r
```


</p>


