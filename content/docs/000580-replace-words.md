---
title: "Replace Words"
weight: 580
#id: "replace-words"
---
## Description
<div class="description">
<p>In English, we have a concept called <strong>root</strong>, which can be followed by some other word&nbsp;to form another longer word - let&#39;s call this word <strong>successor</strong>. For example, when the <strong>root</strong> <code>&quot;an&quot;</code> is&nbsp;followed by the <strong>successor</strong>&nbsp;word&nbsp;<code>&quot;other&quot;</code>, we&nbsp;can form a new word <code>&quot;another&quot;</code>.</p>

<p>Given a <code>dictionary</code> consisting of many <strong>roots</strong> and a <code>sentence</code>&nbsp;consisting of words separated by spaces, replace all the <strong>successors</strong> in the sentence with the <strong>root</strong> forming it. If a <strong>successor</strong> can be replaced by more than one <strong>root</strong>,&nbsp;replace it with the <strong>root</strong> that has&nbsp;<strong>the shortest length</strong>.</p>

<p>Return <em>the <code>sentence</code></em> after the replacement.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> dictionary = ["cat","bat","rat"], sentence = "the cattle was rattled by the battery"
<strong>Output:</strong> "the cat was rat by the bat"
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> dictionary = ["a","b","c"], sentence = "aadsfasf absbs bbab cadsfafs"
<strong>Output:</strong> "a a b c"
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> dictionary = ["a", "aa", "aaa", "aaaa"], sentence = "a aa a aaaa aaa aaa aaa aaaaaa bbb baba ababa"
<strong>Output:</strong> "a a a a a a a a bbb baba a"
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> dictionary = ["catt","cat","bat","rat"], sentence = "the cattle was rattled by the battery"
<strong>Output:</strong> "the cat was rat by the bat"
</pre><p><strong>Example 5:</strong></p>
<pre><strong>Input:</strong> dictionary = ["ac","ab"], sentence = "it is abnormal that this solution is accepted"
<strong>Output:</strong> "it is ab that this solution is ac"
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= dictionary.length&nbsp;&lt;= 1000</code></li>
	<li><code>1 &lt;= dictionary[i].length &lt;= 100</code></li>
	<li><code>dictionary[i]</code>&nbsp;consists of only lower-case letters.</li>
	<li><code>1 &lt;= sentence.length &lt;= 10^6</code></li>
	<li><code>sentence</code>&nbsp;consists of only lower-case letters and spaces.</li>
	<li>The number of words in&nbsp;<code>sentence</code>&nbsp;is in the range <code>[1, 1000]</code></li>
	<li>The length of each word in&nbsp;<code>sentence</code>&nbsp;is in the range <code>[1, 1000]</code></li>
	<li>Each two consecutive words in&nbsp;<code>sentence</code>&nbsp;will be separated by exactly one space.</li>
	<li><code>sentence</code>&nbsp;does not have leading or trailing spaces.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Trie (trie)

## Companies
- Uber - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Prefix Hash [Accepted]

**Intuition**

For each word in the sentence, we'll look at successive prefixes and see if we saw them before.

**Algorithm**

Store all the `roots` in a *Set* structure.  Then for each word, look at successive prefixes of that word.  If you find a prefix that is a root, replace the word with that prefix.  Otherwise, the prefix will just be the word itself, and we should add that to the final sentence answer.

<iframe src="https://leetcode.com/playground/uDtKQPAS/shared" frameBorder="0" width="100%" height="361" name="uDtKQPAS"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(\sum w_i^2)$$ where $$w_i$$ is the length of the $$i$$-th word.  We might check every prefix, the $$i$$-th of which is $$O(w_i^2)$$ work.

* Space Complexity: $$O(N)$$ where $$N$$ is the length of our sentence; the space used by `rootset`.

---
#### Approach #2: Trie [Accepted]

**Intuition and Algorithm**

Put all the roots in a trie (prefix tree).  Then for any query word, we can find the smallest root that was a prefix in linear time.

<iframe src="https://leetcode.com/playground/HziYkFQF/shared" frameBorder="0" width="100%" height="500" name="HziYkFQF"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ where $$N$$ is the length of the `sentence`.  Every query of a word is in linear time.

* Space Complexity: $$O(N)$$, the size of our trie.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Simple/Classical Trie question/solution (Beat 96%)
- Author: fishercoder
- Creation Date: Tue Jul 25 2017 05:58:17 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 11:11:32 GMT+0800 (Singapore Standard Time)

<p>
```
public String replaceWords(List<String> dict, String sentence) {
        String[] tokens = sentence.split(" ");
        TrieNode trie = buildTrie(dict);
        return replaceWords(tokens, trie);
    }

    private String replaceWords(String[] tokens, TrieNode root) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String token : tokens) {
            stringBuilder.append(getShortestReplacement(token, root));
            stringBuilder.append(" ");
        }
        return stringBuilder.substring(0, stringBuilder.length()-1);
    }

    private String getShortestReplacement(String token, final TrieNode root) {
        TrieNode temp = root;
        StringBuilder stringBuilder = new StringBuilder();
        for (char c : token.toCharArray()) {
            stringBuilder.append(c);
            if (temp.children[c - 'a'] != null) {
                if (temp.children[c - 'a'].isWord) {
                    return stringBuilder.toString();
                }
                temp = temp.children[c - 'a'];
            } else {
                return token;
            }
        }
        return token;
    }

    private TrieNode buildTrie(List<String> dict) {
        TrieNode root = new TrieNode(' ');
        for (String word : dict) {
            TrieNode temp = root;
            for (char c : word.toCharArray()) {
                if (temp.children[c - 'a'] == null) {
                    temp.children[c - 'a'] = new TrieNode(c);
                }
                temp = temp.children[c - 'a'];
            }
            temp.isWord = true;
        }
        return root;
    }

    public class TrieNode {
        char val;
        TrieNode[] children;
        boolean isWord;

        public TrieNode(char val) {
            this.val = val;
            this.children = new TrieNode[26];
            this.isWord = false;
        }
    }
```

Also viewable on Github [here](https://github.com/fishercoder1534/Leetcode).
</p>


### Python, Straightforward with Explanation (Prefix hash, Trie solutions)
- Author: awice
- Creation Date: Sun Jul 23 2017 11:35:53 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 27 2018 01:38:46 GMT+0800 (Singapore Standard Time)

<p>
We can check the prefixes directly.  For each word in the sentence, we'll look at successive prefixes and see if we saw them before.

```
def replaceWords(self, roots, sentence):
    rootset = set(roots)

    def replace(word):
        for i in xrange(1, len(word)):
            if word[:i] in rootset:
                return word[:i]
        return word

    return " ".join(map(replace, sentence.split()))
```


We could also use a trie.  We'll insert each root in the trie.  Then, for each word in the sentence, we'll replace it with the first root we encounter upon traversal of the trie.

```
def replaceWords(self, roots, sentence):
    _trie = lambda: collections.defaultdict(_trie)
    trie = _trie()
    END = True
    for root in roots:
        cur = trie
        for letter in root:
            cur = cur[letter]
        cur[END] = root

    def replace(word):
        cur = trie
        for letter in word:
            if letter not in cur: break
            cur = cur[letter]
            if END in cur:
                return cur[END]
        return word

    return " ".join(map(replace, sentence.split()))
```
</p>


### Java solution, 12 lines, HashSet
- Author: shawngao
- Creation Date: Sun Jul 23 2017 11:33:12 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 10:07:17 GMT+0800 (Singapore Standard Time)

<p>
Why it is a hard problem? Anyway...

```
public class Solution {
    public String replaceWords(List<String> dict, String sentence) {
        if (dict == null || dict.size() == 0) return sentence;
        
        Set<String> set = new HashSet<>();
        for (String s : dict) set.add(s);
        
        StringBuilder sb = new StringBuilder();
        String[] words = sentence.split("\\s+");
        
        for (String word : words) {
            String prefix = "";
            for (int i = 1; i <= word.length(); i++) {
                prefix = word.substring(0, i);
                if (set.contains(prefix)) break;
            }
            sb.append(" " + prefix);
        }
        
        return sb.deleteCharAt(0).toString();
    }
}
```
</p>


