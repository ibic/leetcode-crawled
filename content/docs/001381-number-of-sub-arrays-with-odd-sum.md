---
title: "Number of Sub-arrays With Odd Sum"
weight: 1381
#id: "number-of-sub-arrays-with-odd-sum"
---
## Description
<div class="description">
<p>Given an array of integers <code>arr</code>. Return <em>the number of sub-arrays</em> with <strong>odd</strong> sum.</p>

<p>As the answer may grow large, the answer&nbsp;<strong>must be</strong>&nbsp;computed modulo&nbsp;<code>10^9 + 7</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,3,5]
<strong>Output:</strong> 4
<strong>Explanation:</strong> All sub-arrays are [[1],[1,3],[1,3,5],[3],[3,5],[5]]
All sub-arrays sum are [1,4,9,3,8,5].
Odd sums are [1,9,3,5] so the answer is 4.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,4,6]
<strong>Output:</strong> 0
<strong>Explanation:</strong> All sub-arrays are [[2],[2,4],[2,4,6],[4],[4,6],[6]]
All sub-arrays sum are [2,6,12,4,10,6].
All sub-arrays have even sum and the answer is 0.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,3,4,5,6,7]
<strong>Output:</strong> 16
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [100,100,99,99]
<strong>Output:</strong> 4
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> arr = [7]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 100</code></li>
</ul>
</div>

## Tags
- Array (array)
- Math (math)

## Companies
- Directi - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Some hints to help you solve this problem on your own
- Author: Just__a__Visitor
- Creation Date: Sun Jul 26 2020 00:02:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 01:19:37 GMT+0800 (Singapore Standard Time)

<p>
<details>
<summary>Time Complexity </summary>
O(n)
</details>

<details>
<summary>Hint 1</summary>
Let\'s perform all the calculations in modulo 2. So, just assume that all elements are zero or one.
</details>

<details>
<summary>Modified Question </summary>
Given an array consisting of zero and one, how many subarrays sum to 1, such that sum is performed modulo 2.
</details>

<details>
<summary>Hint 2</summary>

Define `dp_one[i]` as the number of subarrays starting at `i` with sum one.
Define `dp_zero[i]` as the number of subarrays starting at `i` with sum zero.

Clearly, answer is sum of all `dp_one[i]`.
</details>

<details>
<summary>Hint 3</summary>
To perform the transition : 

1. If current element is zero, then to update `dp_zero[i]` we can either use it alone, or borrow the subarrays starting at `i + 1` with sum 0.
2. If current element is zero, then to update `dp_one[i]` we will have to borrow the subarrays starting at `i + 1` with sum 1.

Do the same when the current element is zero.
</details>

<details>
<summary>Final Algorithm</summary>

1. Convert to modulo 2.
2. Perform transitions for `dp_zero` and `dp_one`.
3. Sum up `dp_one`.
</details>

<details>
<summary>Code</summary>

```
const int mod = 1e9 + 7;

class Solution {
public:
    int numOfSubarrays(vector<int>& a) {
        int n = a.size();
        
        for(auto &ele : a)
            ele %= 2;
        
        vector<int> dp_zero(n), dp_one(n);
        
        if(a[n - 1] == 0)
            dp_zero[n - 1] = 1;
        else
            dp_one[n - 1] = 1;
        

        for(int i = n - 2; i >= 0; i--)
        {
            if(a[i] == 1)
            {
                dp_one[i] = (1 + dp_zero[i + 1])%mod;
                
                dp_zero[i] = dp_one[i + 1];
            }
            
            if(a[i] == 0)
            {
                dp_zero[i] = (1 + dp_zero[i + 1])%mod;
                
                dp_one[i] = dp_one[i + 1];
            }
            
        }
        
        int sum = 0;
        for(auto ele : dp_one)
            sum += ele, sum %= mod;
        
        return sum;
        
        
    }
};
```

</details>

</p>


### [C++/Python] 7-line Intuitive Constant Space DP
- Author: yanrucheng
- Creation Date: Sun Jul 26 2020 00:03:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 00:11:57 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
This is an elementary dynamic programming problem.\u2028`odd[i]` records the number of subarray ending at `arr[i]` that has odd sum.\u2028`even[i]` records the number of subarray ending at `arr[i]` that has even sum.
if arr[i + 1] is odd, `odd[i + 1] = even[i] + 1` and `even[i + 1] = odd[i]`
if arr[i + 1] is even, `odd[i + 1] = odd[i]` and `even[i + 1] = even[i] + 1`
Since we only required the previous value in `odd` and `even`, we only need `O(1)` space.

Please upvote if you find this post helpful or interesting. It means a lot to me. Thx~

**Complexity**
- Time: `O(n)`
- Space: `O(1)`

**Python**
```
class Solution:
    def numOfSubarrays(self, arr: List[int]) -> int:
        res = odd = even = 0
        for x in arr:
            even += 1
            if x % 2:
                odd, even = even, odd
            res = (res + odd) % 1000000007             
        return res            
```

**C++**
```
class Solution {
public:
    int numOfSubarrays(vector<int>& arr) {
        int res = 0, odd = 0, even = 0;
        for (auto x: arr) {
            even += 1;
            if (x % 2)
                swap(odd, even);
            res = (res + odd) % 1000000007;
        }
        return res;
    }
};
```
</p>


### C++/Java O(n)
- Author: votrubac
- Creation Date: Sun Jul 26 2020 00:03:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 01:03:37 GMT+0800 (Singapore Standard Time)

<p>
> Update: added simplified solution below.

If we know the number of even and odd subarrays that end at the previous element, we can figure out how many even and odd subarrays we have for element `n`:
- If `n` is even, we increase the number of even subarrays; the number of odd subarrays does not change.
- If `n` is odd, the number of odd subarrays is the previous number of even subarrays + 1. The number of even subarrays is the previous number of odd subarrays.

Looking at this example:
```
Array: [1, 1, 2, 1]  Total
Odd:    1  1  1  3     6
Even:   0  1  2  1
```

**C++**
```cpp
int numOfSubarrays(vector<int>& arr) {
    int odd = 0, even = 0, sum = 0;
    for (int n : arr) {
        if (n % 2) {
            swap(odd, even);
            ++odd;
        }
        else
            ++even;
        sum = (sum + odd) % 1000000007;
    }
    return sum;
}
```

**Java**
```java
public int numOfSubarrays(int[] arr) {
    int odd = 0, even = 0, sum = 0;
    for (int n : arr) {
        if (n % 2 == 1) {
            int temp = odd;
            odd = even + 1;
            even = temp;
        }
        else
            ++even;
        sum = (sum + odd) % 1000000007;
    }
    return sum;
}
```
#### Simplified Solution
Since `odd + even` equals the number of elements so far, we can simplify our solution by only tracking `odd`.

**C++**
```cpp
int numOfSubarrays(vector<int>& arr) {
    int sum = 0;
    for (int i = 0, odd = 0; i < arr.size(); ++i) {
        if (arr[i] % 2)
            odd = (i - odd) + 1;
        sum = (sum + odd)  % 1000000007;
    }
    return sum;
}
```
**Java**
```java
public int numOfSubarrays(int[] arr) {
    int sum = 0;
    for (int i = 0, odd = 0; i < arr.length; ++i) {
        if (arr[i] % 2 == 1)
            odd = (i - odd) + 1;
        sum = (sum + odd)  % 1000000007;
    }
    return sum;
}
```

#### Complexity Analysis
- Time: O(n)
- Memory: O(1)
</p>


