---
title: "Rotate Image"
weight: 48
#id: "rotate-image"
---
## Description
<div class="description">
<p>You are given an <em>n</em> x <em>n</em> 2D <code>matrix</code> representing an image, rotate the image by 90 degrees (clockwise).</p>

<p>You have to rotate the image <a href="https://en.wikipedia.org/wiki/In-place_algorithm" target="_blank"><strong>in-place</strong></a>, which means you have to modify the input 2D matrix directly. <strong>DO NOT</strong> allocate another 2D matrix and do the rotation.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/28/mat1.jpg" style="width: 642px; height: 242px;" />
<pre>
<strong>Input:</strong> matrix = [[1,2,3],[4,5,6],[7,8,9]]
<strong>Output:</strong> [[7,4,1],[8,5,2],[9,6,3]]
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/28/mat2.jpg" style="width: 800px; height: 321px;" />
<pre>
<strong>Input:</strong> matrix = [[5,1,9,11],[2,4,8,10],[13,3,6,7],[15,14,12,16]]
<strong>Output:</strong> [[15,13,2,5],[14,3,4,1],[12,6,8,9],[16,7,10,11]]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> matrix = [[1]]
<strong>Output:</strong> [[1]]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> matrix = [[1,2],[3,4]]
<strong>Output:</strong> [[3,1],[4,2]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>matrix.length == n</code></li>
	<li><code>matrix[i].length == n</code></li>
	<li><code>1 &lt;= n &lt;= 20</code></li>
	<li><code>-1000 &lt;= matrix[i][j] &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Cisco - 13 (taggedByAdmin: false)
- Apple - 6 (taggedByAdmin: true)
- Microsoft - 3 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Akuna Capital - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- Quora - 2 (taggedByAdmin: false)
- Wish - 3 (taggedByAdmin: false)
- Nvidia - 3 (taggedByAdmin: false)
- Yandex - 3 (taggedByAdmin: false)
- Houzz - 2 (taggedByAdmin: false)
- Groupon - 2 (taggedByAdmin: false)
- Cohesity - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Palantir Technologies - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1 : Transpose and then reverse

The obvious idea would be to transpose the matrix first and then
reverse each row. This simple approach already demonstrates the best
possible time complexity $$\mathcal{O}(N^2)$$.

<iframe src="https://leetcode.com/playground/QUyv3UmS/shared" frameBorder="0" width="100%" height="429" name="QUyv3UmS"></iframe>

* Time complexity : $$\mathcal{O}(N^2)$$. 
* Space complexity : $$\mathcal{O}(1)$$ since we do a rotation *in place*.
<br />
<br />

---
#### Approach 2 : Rotate four rectangles

**Intuition**

Approach 1 makes two passes through the matrix, 
though it's possible to make a rotation in one pass.

To figure out how let's check 
how each element in the angle moves during the rotation. 
 
![compute](../Figures/48/48_angles.png)

That gives us an idea to split a given matrix in four rectangles and
reduce the initial problem to the rotation of these rectangles.

![compute](../Figures/48/48_rectangles.png) 

Now the solution is quite straightforward - 
one could move across the elements 
in the first rectangle and rotate them using a temp list of `4` elements.

**Implementation**

!?!../Documents/48_LIS.json:1000,513!?!

<iframe src="https://leetcode.com/playground/th7ZgLrF/shared" frameBorder="0" width="100%" height="463" name="th7ZgLrF"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N^2)$$ is a complexity given by two inserted loops. 
* Space complexity : $$\mathcal{O}(1)$$ since we do a rotation *in place* 
and allocate only the list of `4` elements as a temporary helper.
<br />
<br />

---
#### Approach 3 : Rotate four rectangles in one single loop

The idea is the same as in the approach 2,
but everything is done in one single loop 
and hence it's a way more elegant
(kudos go to @[gxldragon](https://leetcode.com/gxldragon/)).

<iframe src="https://leetcode.com/playground/7gxVTE5D/shared" frameBorder="0" width="100%" height="293" name="7gxVTE5D"></iframe>

* Time complexity : $$\mathcal{O}(N^2)$$ is a complexity given by two inserted loops. 
* Space complexity : $$\mathcal{O}(1)$$ since we do a rotation *in place*.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A common method to rotate the image
- Author: shichaotan
- Creation Date: Tue Jan 06 2015 02:30:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 07:26:06 GMT+0800 (Singapore Standard Time)

<p>
here give a common method to solve the image rotation problems.
    
    /*
     * clockwise rotate
     * first reverse up to down, then swap the symmetry 
     * 1 2 3     7 8 9     7 4 1
     * 4 5 6  => 4 5 6  => 8 5 2
     * 7 8 9     1 2 3     9 6 3
    */
    void rotate(vector<vector<int> > &matrix) {
        reverse(matrix.begin(), matrix.end());
        for (int i = 0; i < matrix.size(); ++i) {
            for (int j = i + 1; j < matrix[i].size(); ++j)
                swap(matrix[i][j], matrix[j][i]);
        }
    }
    
    /*
     * anticlockwise rotate
     * first reverse left to right, then swap the symmetry
     * 1 2 3     3 2 1     3 6 9
     * 4 5 6  => 6 5 4  => 2 5 8
     * 7 8 9     9 8 7     1 4 7
    */
    void anti_rotate(vector<vector<int> > &matrix) {
        for (auto vi : matrix) reverse(vi.begin(), vi.end());
        for (int i = 0; i < matrix.size(); ++i) {
            for (int j = i + 1; j < matrix[i].size(); ++j)
                swap(matrix[i][j], matrix[j][i]);
        }
    }
</p>


### Seven Short Solutions (1 to 7 lines)
- Author: StefanPochmann
- Creation Date: Wed Jun 03 2015 02:23:56 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 00:27:03 GMT+0800 (Singapore Standard Time)

<p>
While these solutions are Python, I think they're understandable/interesting for non-Python coders as well. But before I begin: No mathematician would call a matrix `matrix`, so I'll use the usual `A`. Also, btw, the 40 ms reached by two of the solutions is I think the fastest achieved by Python solutions so far.

---

**Most Pythonic - `[::-1]` and `zip`** - 44 ms

The most pythonic solution is a simple one-liner using `[::-1]` to flip the matrix upside down and then `zip` to transpose it. It assigns the result back into `A`, so it's "in-place" in a sense and the OJ accepts it as such, though some people might not.

    class Solution:
        def rotate(self, A):
            A[:] = zip(*A[::-1])

---

**Most Direct** - 52 ms

A 100% in-place solution. It even reads and writes each matrix element only once and doesn't even use an extra temporary variable to hold them. It walks over the *"top-left quadrant"* of the matrix and directly rotates each element with the three corresponding elements in the other three quadrants. Note that I'm moving the four elements in parallel and that `[~i]` is way nicer than `[n-1-i]`.

    class Solution:
        def rotate(self, A):
            n = len(A)
            for i in range(n/2):
                for j in range(n-n/2):
                    A[i][j], A[~j][i], A[~i][~j], A[j][~i] = \
                             A[~j][i], A[~i][~j], A[j][~i], A[i][j]

---

**Clean Most Pythonic** - 56 ms

While the OJ accepts the above solution, the the result rows are actually tuples, not lists, so it's a bit dirty. To fix this, we can just apply `list` to every row:

    class Solution:
        def rotate(self, A):
            A[:] = map(list, zip(*A[::-1]))

---

**List Comprehension** - 60 ms

If you don't like `zip`, you can use a nested list comprehension instead:

    class Solution:
        def rotate(self, A):
            A[:] = [[row[i] for row in A[::-1]] for i in range(len(A))]

---

**Almost as Direct** - 40 ms

If you don't like the little repetitive code of the above "Most Direct" solution, we can instead do each four-cycle of elements by using three swaps of just two elements.

    class Solution:
        def rotate(self, A):
            n = len(A)
            for i in range(n/2):
                for j in range(n-n/2):
                    for _ in '123':
                        A[i][j], A[~j][i], i, j = A[~j][i], A[i][j], ~j, ~i
                    i = ~j

---

**Flip Flip** - 40 ms

Basically the same as the first solution, but using `reverse` instead of `[::-1]` and transposing the matrix with loops instead of `zip`. It's 100% in-place, just instead of only moving elements around, it also moves the rows around.

    class Solution:
        def rotate(self, A):
            A.reverse()
            for i in range(len(A)):
                for j in range(i):
                    A[i][j], A[j][i] = A[j][i], A[i][j]

---

**Flip Flip, all by myself** - 48 ms

Similar again, but I first transpose and then flip left-right instead of upside-down, and do it all by myself in loops. This one is 100% in-place again in the sense of just moving the elements.

    class Solution:
        def rotate(self, A):
            n = len(A)
            for i in range(n):
                for j in range(i):
                    A[i][j], A[j][i] = A[j][i], A[i][j]
            for row in A:
                for j in range(n/2):
                    row[j], row[~j] = row[~j], row[j]
</p>


### AC Java in place solution with explanation Easy to understand.
- Author: LuckyIdiot
- Creation Date: Sat Mar 07 2015 03:14:44 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 09:37:33 GMT+0800 (Singapore Standard Time)

<p>
The idea was firstly transpose the matrix and then flip it symmetrically. For instance, 

    1  2  3             
    4  5  6
    7  8  9

after transpose, it will be swap(matrix[i][j], matrix[j][i])

    1  4  7
    2  5  8
    3  6  9

Then flip the matrix horizontally.  (swap(matrix[i][j], matrix[i][matrix.length-1-j])

    7  4  1
    8  5  2
    9  6  3

Hope this helps.

    public class Solution {
        public void rotate(int[][] matrix) {
            for(int i = 0; i<matrix.length; i++){
                for(int j = i; j<matrix[0].length; j++){
                    int temp = 0;
                    temp = matrix[i][j];
                    matrix[i][j] = matrix[j][i];
                    matrix[j][i] = temp;
                }
            }
            for(int i =0 ; i<matrix.length; i++){
                for(int j = 0; j<matrix.length/2; j++){
                    int temp = 0;
                    temp = matrix[i][j];
                    matrix[i][j] = matrix[i][matrix.length-1-j];
                    matrix[i][matrix.length-1-j] = temp;
                }
            }
        }
    }
</p>


