---
title: "Tiling a Rectangle with the Fewest Squares"
weight: 1188
#id: "tiling-a-rectangle-with-the-fewest-squares"
---
## Description
<div class="description">
<p>Given a rectangle of size&nbsp;<code>n</code>&nbsp;x <code><font face="monospace">m</font></code>, find the minimum number of integer-sided squares that tile the rectangle.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/10/17/sample_11_1592.png" style="width: 154px; height: 106px;" /></p>

<pre>
<strong>Input:</strong> n = 2, m = 3
<strong>Output:</strong> 3
<strong>Explanation:</strong> <code>3</code> squares are necessary to cover the rectangle.
<code>2</code> (squares of <code>1x1</code>)
<code>1</code> (square of <code>2x2</code>)</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/10/17/sample_22_1592.png" style="width: 224px; height: 126px;" /></p>

<pre>
<strong>Input:</strong> n = 5, m = 8
<strong>Output:</strong> 5
</pre>

<p><strong>Example 3:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/10/17/sample_33_1592.png" style="width: 224px; height: 189px;" /></p>

<pre>
<strong>Input:</strong> n = 11, m = 13
<strong>Output:</strong> 6
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 13</code></li>
	<li><code>1 &lt;= m&nbsp;&lt;=&nbsp;13</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Backtracking (backtracking)

## Companies
- Google - 4 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 8ms Memorized Backtrack Solution, without special case!
- Author: Charles000
- Creation Date: Sun Oct 27 2019 12:55:56 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 28 2019 09:56:10 GMT+0800 (Singapore Standard Time)

<p>
The basic idea is to fill the entire block bottom up. In every step, find the lowest left unfilled square first, and select a square with different possible sizes to fill it. We maintain a height array (skyline) with length n while dfs. This skyline is the identity of the state. The final result we ask for is the minimum number of squares for the state [m, m, m, m, m, m, m] (The length of this array is n). Of course, backtrack without optimization will have a huge time complexity, but it can be pruned or optimized by the following three methods.
1. When the current cnt (that is, the number of squares) of this skyline has exceeded the value of the current global optimal solution, then return directly.
2. When the current skyline has been traversed, and the previous cnt is smaller than the current cnt, then return directly.
3. When we find the empty square in the lowest left corner, we pick larger size for the next square first. This can make the program converge quickly. (It is a very important optimization)

```
class Solution {
    Map<Long, Integer> set = new HashMap<>();
    int res = Integer.MAX_VALUE;
    public int tilingRectangle(int n, int m) {
        if (n == m) return 1;
        if (n > m) {
            int temp = n;
            n = m;
            m = temp;
        }
        dfs(n, m, new int[n + 1], 0);
        return res;
    }
    
    private void dfs(int n, int m, int[] h, int cnt) {
        if (cnt >= res) return;
        boolean isFull = true;
        int pos = -1, minH = Integer.MAX_VALUE;
        for (int i = 1; i <= n; i++) {
            if (h[i] < m) isFull = false;
            if (h[i] < minH) {
                pos = i;
                minH = h[i];
            }
        }
        if (isFull) {
            res = Math.min(cnt, res);
            return;
        }
        
        long key = 0, base = m + 1;
        for (int i = 1; i <= n; i++) {
            key += h[i] * base;
            base *= m + 1;
        }
        if (set.containsKey(key) && set.get(key) <= cnt) return;
        set.put(key, cnt);
        
        int end = pos;
        while (end + 1 <= n && h[end + 1] == h[pos] && (end + 1 - pos + 1 + minH) <= m) end++;
        for (int j = end; j >= pos; j--) {
            int curH = j - pos + 1;
            
            int[] next  = Arrays.copyOf(h, n + 1);
            for (int k = pos; k <= j; k++) {
                next[k] += curH;
            }
            dfs(n, m, next, cnt + 1);
        }
        
    }
}
```
</p>


### A Review: Why This Problem Is a Tip of the Iceberg
- Author: orangezeit
- Creation Date: Mon Oct 28 2019 05:31:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 20 2020 08:26:18 GMT+0800 (Singapore Standard Time)

<p>
**Algorithm Review**

* Credits to all leetcoders who discussed here. \uD83D\uDE0A
* Denote the solution of (n, m) as s(n, m). Obviously, s(n, m) = s(m, n).
* (Conjecture 1) Let F_n be the Fibonacci sequence(1, 1, 2, 3, 5, 8...). We purpose that s(F_k, F_(k+1)) = k where k is a positive integer. If that is true, the greedy algorithm does output the correct answer in this special case. (Currently we have checked the rectangles up to 380 * 380 and no counterexamples have been found.)
	* s(5, 8) = s(F_5, F_6) = 5
	![image](https://assets.leetcode.com/users/orangezeit/image_1572212430.png)
* Too-specific solution: Hard-code a 13 * 13 table of results pre-calculated by others
* Specific (and essentially wrong) solutions: Due to the input scale, we can adapt DP or greedy by handling special cases.
	* If we adapt (naive) DP (i.e., only storing information of tiling squares in the sub-problems), we only need to deal with one special case 11 * 13 (or equivalently 13 * 11) where the DP answer does not agree with the correct one.
	* If we adapt (naive) greedy (i.e. always cutting off the largest possible square), we need to deal with much more special cases compared with DP. I will argue the pros and cons of two methods in the following code block.
	
	```cpp
    // (Wrong!) DP, must handle special cases and cannot generalize to any (n, m)
    // Credit: [@xiaogugu](https://leetcode.com/problems/tiling-a-rectangle-with-the-fewest-squares/
	// discuss/414246/Java-Straightforward-DP%3A-Top-Down-and-Bottom-up), with modifications
	// Time Complexity: O(mn * max(m, n))
	// Space Complexity: O(mn)
	
    int tilingRectangle(int n, int m) {
        if ((n == 11 && m == 13) || (m == 11 && n == 13))
			return 6; // DP answer is 8
        int dp[n + 1][m + 1] = {};
        
        for (int i = 1; i <= n; i++)
            for (int j = 1; j <= m; j++){
                if (i == j) {
                    dp[i][j] = 1;
                    continue;
                }
                dp[i][j] = i * j;
                for (int k = 1; k <= i / 2; k++)
                    dp[i][j] = min(dp[i][j], dp[i - k][j] + dp[k][j]);
                for (int k = 1; k <= j / 2; k++)
                    dp[i][j] = min(dp[i][j], dp[i][j - k] + dp[i][k]);
            }
        
        return dp[n][m];
    }
	
	// (Wrong!) greedy, must handle much more special cases and cannot generalize to any (n, m)
	// You may wonder why I still apply seemingly less efficient algorithm.
	// Since naive DP and greedy are both wrong, we can only
	// use them as the upper bounds of the true answers in pruning DFS later.
	// DP generates more accurate upper bounds in a slower rate, and
	// greedy generates less accurate upper bounds in a faster rate. Here is the trade-off.
	
	// Hint: The Euclidean Algorithm
	// Time Complexity: O(log(min(m, n)))
	// Space Complexity: O(log(min(m, n)))
	
    int tilingRectangle(int n, int m) {
		if (n > m) swap(m, n);
		if (n == 5 && m == 6)   return 5;   // greedy answer is 6
		if (n == 5 && m == 11)  return 6;   // greedy answer is 7
		if (n == 6 && m == 7)   return 5;   // greedy answer is 7
		if (n == 6 && m == 11)  return 6;   // greedy answer is 7
		if (n == 6 && m == 13)              // ... You get the point.
	
        return m == n ? 1 : tilingRectangle(n, m - n) + 1;
	}
	```
	* (Conjecture 2) For any positive integers m and n, we further denote the result generated by DP as dp(m, n) and the result generated by greedy as gd(m, n). We purpose that s(m, n) <= dp(m, n) <= gd(m, n) always holds. It is pretty intuitive but I don\'t know how to prove.
	* **[Updated]** It might be tempted to construct a **more sophisticated DP / greedy** (i.e.  consider rectangles and L-shapes besides squares). This method will **eventually fail** if you test larger m and n. (Chanllenge me if I am wrong.)
		* Take case 16 * 17 for example. If you draw the tiles, you will realize that there is a "double-L-shape" (Your method might still work with the detailed categories of L-shapes but this is the signal). Needless to say, as m and n increase, there would be more complex shapes that cannot be filled into any category you designed at first (and that\'s usually when DP and greedy fail), but that is just my intuition.
		![image](https://assets.leetcode.com/users/orangezeit/image_1572481565.png)
		
		* If you are lucky and your codes passed case 16 * 17 (the correct answer is 8), it probabily won\'t pass 82 * 83 (the correct answer is 10). The **lowest-order simple squared square figure** below also visualizes the complex relations of the squares.
		* (Conjecture 3) This problem cannot be solved by DP or greedy alone.
* General (true) solution: use DFS / backtracking, start stacking squares from any of the four corners, could be improved by A* search.
	* Pruning 1: Try largest possible squares first to prune DFS.
	* Pruning 2: Use results generated by (naive) DP or greedy discussed above as smaller upper bounds of true answers.
	* After actual testing up to 30 * 30, DP upper bounds work better.
	* Visualization of heights
	* ![image](https://assets.leetcode.com/users/orangezeit/image_1577235083.png)

	```cpp
	class Solution {
	public:
		int tilingRectangle(int n, int m) {
			if (n == m) return 1;
			if (n > m) swap(n, m);
			int heights[n] = {};

			int dp[n + 1][m + 1] = {};

			for (int i = 1; i <= n; i++)
				for (int j = 1; j <= m; j++){
					if (i == j) {
						dp[i][j] = 1;
						continue;
					}
					dp[i][j] = i * j;
					for (int k = 1; k <= i / 2; k++)
						dp[i][j] = min(dp[i][j], dp[i - k][j] + dp[k][j]);
					for (int k = 1; k <= j / 2; k++)
						dp[i][j] = min(dp[i][j], dp[i][j - k] + dp[i][k]);
				}

			int res(dp[n][m]);
			dfs(n, m, heights, 0, res);
			return res;
		}
		
		// Credit: [@Charles000](https://leetcode.com/problems/tiling-a-rectangle-with-the-fewest-squares/
		// discuss/414260/8ms-Memorized-Backtrack-Solution-without-special-case!), with modifications
		// (a) add DP upper bound
		// (b) use backtracking and pass array by reference to be memory efficient
		// (c) other minor changes such as removing redundant variables & rewriting conditions
		// (d) does not include the skyline record since it does not reduce time complexity significantly in tests
		// Time Complexity: O(m^n)
		// Space Complexity: O(n)
		
		void dfs(const int& n, const int& m, int heights[], int cnt, int& res) {
			if (cnt >= res) return;

			int left(0), min_height(m);
			for (int i = 0; i < n; i++)
				if (heights[i] < min_height)
					min_height = heights[left = i];

			if (min_height == m) {
				res = min(cnt, res);
				return;
			}

			int right(left);
			while (right < n && heights[left] == heights[right] && right - left + min_height < m) right++;
			/*
			for (int i = right; i > left; --i) {
				for (int j = left; j < i; ++j)
					heights[j] += i - left;
				dfs(n, m, heights, cnt + 1, res);
				for (int j = left; j < i; ++j)
					heights[j] -= i - left;
			}
			*/
			// Credit: @revaluation
			// plot the biggest possible square to the rectangle
			for (int i = left; i < right; i++)
				heights[i] += right - left;

			for (int size = right - left; size >= 1; size--) {
				dfs(n, m, heights, cnt + 1, res);

				// update the rectangle to contain the next smaller square
				for (int i = left; i < left + size - 1; i++)
					heights[i]--;
				heights[left + size - 1] -= size;
			}
		}
	};
	```

**Suggestions on Code Review**
* [Cheat Table](http://int-e.eu/~bf3/squares/young.txt) from [@lee215](https://leetcode.com/problems/tiling-a-rectangle-with-the-fewest-squares/discuss/414160/Cheat)\'s post

* Focus on **basic solutions** in the cheat table

**Mathematical Review**

*WARNING: LOTS OF DEFINITIONS!*

This problem is just a special case of a group of problems studied by mathematicians. The first (most classic one) is **squaring the square**, which means we want to partition a square into several (possibly unique, i.e. use every kind of square only once) squares. If we require the uniqueness, it is a **simple (or perfect) squared square**, otherwise it is a **compound squared square**. As for this programming problem, we are **squaring the rectangle**. Since the uniqueness is not required, we try to make a **compound squared rectangle**. Since we can always make one, we are interested in finding the minimum number of squares.

Watch the following video for a nice visualization of squaring squares.

https://www.youtube.com/watch?v=NoRjwZomUK0

**Transformation**

Between 1936 to 1938 at Cambridge University, Brooks, Smith, Stone and Tutte transformed the square tiling into an **equivalent electrical circuit** (also known as **Smith diagram**). From the perspective of computer science, it is equivalent to a **directed acyclic graph with more requirements**. (Maybe there is a jargon that I don\'t know to describe that graph, but I just call it a flowchart.) There are three types of nodes. The first type only has the outflows (denoted as +). The second type only has the inflows (denoted as -). The third type has both outflows and inflows.

![image](https://assets.leetcode.com/users/orangezeit/image_1572211864.png)

For the examples in this programming problem, we can draw similar graphs / flowcharts.

![image](https://assets.leetcode.com/users/orangezeit/image_1572271808.png)

**Several key observations in the graph**

* The edges represent the squares. (@Prezes) The nodes represent the contiguities of the squares.
* The sum of the outflows of the node (+) / the sum of the inflows of the node (-) is equal to one side of the rectangle.
* For each of the nodes in between, the sum of the inflows is equal to the sum of the outflows.
* For each path from node (+) to node (-), the sum of them is equal to the other side of the rectangle.

Maybe someone who aces in graph theory can apply those properties in actual coding, but that is beyond my programming ability.

**HELL-LEVEL Follow-Ups**

* Could be used as contest problems (in professional ones such as CODE JAM, NOT in Leetcode weekly contests)
* Solving them may not be impossible because DFS always works. The hardness comes from how we improve the efficiency.
* Extension 0: (Compound tilting - Count & Display) The compound tilting always exists given any positive integers m and n. More specifically, the number of squares ranges from s(n, m) to (m * n) inclusively. How many unique solutions are there? Return int. Also display them as list[list[int]] where int is the size length of squares. (Hint: count at the end of DFS + skip duplicated ones after rotations)
* Extension 1: What if we require uniqueness (i.e. each kind can be only used once) , or some degree of uniqueness (i.e. each kind can be used at most k times, we refer that as **semi-simple squaring / cubing / hypercubing** in the following text informally) ?
	 - Q1.1: (Existence) Given m, n, does there exist a solution? Return true or false. As you are probably aware, unlike the compound tilting, the simple or semi-simple one does not always have a solution. (Hint: DFS + hashset if simple or hashmap if semi-simple)
		 - The (only) lowest-order simple squared square

	![image](https://assets.leetcode.com/users/orangezeit/image_1572213053.png)

	- Q1.2: (Count & Display) Given m, n and suppose at least one solution exists, how many unique solutions are there? Return int. Also display them as list[list[int]] where int is the size length of squares. ([OEIS A006983](https://oeis.org/A006983)) (Hint: count at the end of DFS + skip duplicated ones after rotations)
* Extension 2: What if we generalize the problem to 3 dimensions (**Cubing the cube** or **Cubing the cuboid**) or even higher dimensions (**Hypercubing the hypercube** or **Hypercubing the hypercuboid** \u2013 not sure if that is a legitimate word)? It has been proved that simple cubing cubes and simple hypercubing hypercubes are impossible. What about other situations? Show whether they exist. Count and display them if they exist.

| Existence  | simple  |  semi-simple  | compound |
|:-:|:-:|:-:|:-:|
| (hyper)cube  | Impossible  |  Indeterminate  | Definite |
| (hyper)cuboid  | Indeterminate  | Indeterminate  | Definite |

| Count & Display  | simple  |  semi-simple  | compound |
|:-:|:-:|:-:|:-:|
| (hyper)cube  | NA  |  ?  | ? |
| (hyper)cuboid  | ?  | ?  | ? |

**[Updated] Sample Solutions to Follow-Ups**

Feel free to run the program at [here](https://repl.it/@YunfeiLuo/tilingrectanglescuboids)

```cpp
#include <algorithm>
#include <cstring>
#include <iostream>
#include <map>
#include <numeric>
#include <set>
#include <vector>


class Tiles {
private:
    std::map<int, int> record;
    std::set<std::vector<std::pair<int, int>>> rect_collections;
    int res;
  
    const int dpUpperBound2D(const int& x, const int& y) {
        int dp[x + 1][y + 1];
        memset(dp, 0, (x + 1) * (y + 1) * sizeof(int));

        for (int i = 1; i <= x; ++i)
            for (int j = 1; j <= y; ++j) {
                if (i == j) {
                    dp[i][j] = 1;
                    continue;
                }
                dp[i][j] = i * j;
                for (int k = 1; k <= i / 2; ++k)
                    dp[i][j] = std::min(dp[i][j], dp[i - k][j] + dp[k][j]);
                for (int k = 1; k <= j / 2; ++k)
                    dp[i][j] = std::min(dp[i][j], dp[i][j - k] + dp[i][k]);
            }

        return dp[x][y];
    }

    const int dpUpperBound3D(const int& x, const int& y, const int& z) {
        int dp[x + 1][y + 1][z + 1];
        memset(dp, 0, (x + 1) * (y + 1) * (z + 1) * sizeof(int));

        for (int i = 1; i <= x; ++i)
            for (int j = 1; j <= y; ++j)
                for (int k = 1; k <= z; ++k) {
                    if (i == j && j == k) {
                        dp[i][j][k] = 1;
                        continue;
                    }
                    dp[i][j][k] = i * j * k;
                    for (int n = 1; n <= i / 2; ++n)
                        dp[i][j][k] = std::min(dp[i][j][k], dp[i - n][j][k] + dp[n][j][k]);
                    for (int n = 1; n <= j / 2; ++n)
                        dp[i][j][k] = std::min(dp[i][j][k], dp[i][j - n][k] + dp[i][n][k]);
                    for (int n = 1; n <= k / 2; ++n)
                        dp[i][j][k] = std::min(dp[i][j][k], dp[i][j][k - n] + dp[i][j][n]);
                }

        return dp[x][y][z];
    }

    void dfs2D(const int& x, const int& y, const int& n, int *heights, int cnt, int& ub, const bool& minTiles) {
		
        if (cnt > ub) return;

		int left(0), min_height(y);
		for (int i = 0; i < x; i++)
			if (heights[i] < min_height)
				min_height = *(heights + (left = i));

		if (min_height == y) {
            if (cnt <= ub) {
                int i(0);
                std::vector< std::pair<int, int> > squares(record.size());
                for (const auto& p: record)
                    squares[i++] = std::make_pair(p.first, p.second);
                if (minTiles) rect_collections.clear();
                rect_collections.insert(squares);
            }
            res = std::min(res, cnt);
            if (minTiles) ub = std::min(ub, cnt);
            return;
		}

		int right(left);
		while (right < x && *(heights + left) == *(heights + right) && right - left + min_height < y) right++;

		for (int i = right; i > left; --i) {
            if (n && record.count(i - left) && record[i - left] == n)
                continue;
            for (int j = left; j < i; ++j)
                *(heights + j) += i - left;
            record[i - left]++;
            dfs2D(x, y, n, heights, cnt + 1, ub, minTiles);
            for (int j = left; j < i; ++j)
                *(heights + j) -= i - left;
            if (!(--record[i - left]))
                record.erase(i - left);
		}
	}

    void dfs3D(const int& x, const int& y, const int& z, const int& n, int *heights, int cnt, int& ub, const bool& minTiles) {

        if (cnt > ub) return;

        int left(0), top(0), min_height(z);
        for (int i = 0; i < x; ++i)
            for (int j = 0; j < y; ++j)
                if (*(heights + i * y + j) < min_height)
                    min_height = *(heights + (left = i) * y + (top = j));

        if (min_height == z) {
            if (cnt <= ub) {
                int m(0);
                std::vector< std::pair<int, int> > cubes(record.size());
                for (const auto& p: record)
                    cubes[m++] = std::make_pair(p.first, p.second);
                if (minTiles) rect_collections.clear();
                rect_collections.insert(cubes);
            }
            res = std::min(res, cnt);
            if (minTiles) ub = std::min(ub, cnt);
            return;
        }

        int right(left), bottom(top);
        while (right < x && bottom < y &&
               *(heights + left * y + top) == *(heights + right * y + top) && *(heights + left * y + top) == *(heights + left * y + bottom) &&
               right - left + min_height < z && bottom - top + min_height < z) {
            
            right++;
            bottom++;
            bool notSame(false);
            for (int i = left + 1; i < right; ++i) {
                for (int j = top + 1; j < bottom; ++j)
                    if (*(heights + i * y + j) != *(heights + left * y + top)) {
                        notSame = true;
                        break;
                    }
                if (notSame) break;
            }

            if (notSame) {
                right--;
                bottom--;
                break;
            }
        }
        
        for (int i = right; i > left; --i) {
            if (n && record.count(i - left) && record[i - left] == n)
                continue;
            for (int j = left; j < i; ++j)
                for (int k = top; k < top + i - left; ++k)
                    *(heights + j * y + k) += i - left;
            record[i - left]++;
            dfs3D(x, y, z, n, heights, cnt + 1, ub, minTiles);
            for (int j = left; j < i; ++j)
                for (int k = top; k < top + i - left; ++k)
                    *(heights + j * y + k) -= i - left;
            if (!(--record[i - left]))
                record.erase(i - left);
        }
    }

public:
    // d: dimension
    // arr[]: lengths of dimensions
    // n: the maximum number of each kind of square / cube to be used
    //    if n == 0: compound tiling
    //    if n > 0: semi-simple tiling (simple / perfect tiling if n == 1)
    // minTile: true if to find the tiles whose number is minimum, false if to find all tiles
	void tilingRectangleCuboid(int arr[], size_t d, const int n, const bool minTiles) {
        res = std::accumulate(arr, arr + d, 1, std::multiplies<int>());
		std::sort(arr, arr + d);

        if (d == 2) {
            int ub(minTiles ? dpUpperBound2D(arr[0], arr[1]) : res), heights[arr[0]];
            memset(heights, 0, arr[0] * sizeof(int));
            dfs2D(arr[0], arr[1], n, (int *)heights, 0, ub, minTiles);
        } else {
            int ub(minTiles ? dpUpperBound3D(arr[0], arr[1], arr[2]) : res), heights[arr[0]][arr[1]];
            memset(heights, 0, arr[0] * arr[1] * sizeof(int));
            dfs3D(arr[0], arr[1], arr[2], n, (int *)heights, 0, ub, minTiles);
        }

        if (rect_collections.empty()) {
            std::cout << "No solutions with such requirements.\
";
        } else {
            
            for (const auto& rect: rect_collections) {
                for (const auto& p: rect)
                    std::cout << p.first << ": " << p.second << "\
";
                std::cout << "--------\
";
            }
            std::cout << "The minimum number of " << (d == 2 ? "squares" : "cubes") << " is " << res << "\
";
        }
	}
};

int main() {
    Tiles t;
    int d[2] = {112, 112};
    t.tilingRectangleCuboid(d, 2, 1, false);
}
```

**References**

[Wikipedia - Squaring the squares](https://en.wikipedia.org/wiki/Squaring_the_square)
[Wolfram Math World - Perfect Square Dissection](http://mathworld.wolfram.com/PerfectSquareDissection.html)
[Squaring the Plane](http://math.smith.edu/~jhenle/stp/stp.pdf)
</p>


### Java back tracking solution
- Author: motorix
- Creation Date: Mon Oct 28 2019 09:45:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Nov 02 2019 13:32:44 GMT+0800 (Singapore Standard Time)

<p>
Based on @uwi\'s solution in the contest, I add some explantion here.
1. Go through every point in the rectangle, starting from (0,0), (0,1), ..., (n, m).
2. If ```rect[r,..., r+k][c, ..., c+k]``` is an available area, then cover a ```k*k``` square starting at point ```(r,c)```.
3. Try every possible size of square ```k * k```, where ```k = min(n-r, m-c)```.
4. Optimzation: If ```cnt``` >= ```ans```, then stop.
![image](https://assets.leetcode.com/users/motorix/image_1572226905.png)

Java:
```
class Solution {
    int ans = Integer.MAX_VALUE;
    public int tilingRectangle(int n, int m) {
        dfs(0, 0, new boolean[n][m], 0);
        return ans;
    }
    // (r, c) is the starting point for selecting a square
    // rect records the status of current rectangle
    // cnt is the number of squares we have covered
    private void dfs(int r, int c, boolean[][] rect, int cnt) {
        int n = rect.length, m = rect[0].length;
        // optimization 1: The current cnt >= the current answer
        if (cnt >= ans) return;
        
        // Successfully cover the whole rectangle
        if (r >= n) {
            ans = cnt; 
            return;
        }
        
        // Successfully cover the area [0, ..., n][0, ..., c] => Move to next row
        if (c >= m) {
            dfs(r+1, 0, rect, cnt); 
            return;
        }
        
        // If (r, c) is already covered => move to next point (r, c+1)
        if (rect[r][c]) {
            dfs(r, c+1, rect, cnt);
            return;
        }
        
        // Try all the possible size of squares starting from (r, c)
        for (int k = Math.min(n-r, m-c); k >= 1 && isAvailable(rect, r, c, k); k--) {
            cover(rect, r, c, k);
            dfs(r, c+1, rect, cnt+1);
            uncover(rect, r, c, k);
        }
    }
    // Check if the area [r, ..., r+k][c, ..., c+k] is alreadc covered
    private boolean isAvailable(boolean[][] rect, int r, int c, int k) {
        for (int i = 0; i < k; i++){
            for(int j = 0; j < k; j++){
                if(rect[r+i][c+j]) return false;
            }
        }
        return true;
    }
    // Cover the area [r, ..., r+k][c, ..., c+k] with a k * k square
    private void cover(boolean[][] rect, int r, int c, int k) {
        for(int i = 0; i < k; i++){
            for(int j = 0; j < k; j++){
                rect[r+i][c+j] = true;
            }
        }
    }
    // Uncover the area [r, ..., r+k][c, ..., c+k]
    private void uncover(boolean[][] rect, int r, int c, int k) {
        for(int i = 0; i < k; i++){
            for(int j = 0; j < k; j++){
                rect[r+i][c+j] = false;
            }
        }
    }
}
```
</p>


