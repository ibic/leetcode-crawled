---
title: "Keys and Rooms"
weight: 785
#id: "keys-and-rooms"
---
## Description
<div class="description">
<p>There are <code>N</code> rooms and you start in room <code>0</code>.&nbsp; Each room has a distinct number in <code>0, 1, 2, ..., N-1</code>, and each room may have&nbsp;some keys to access the next room.&nbsp;</p>

<p>Formally, each room <code>i</code>&nbsp;has a list of keys <code>rooms[i]</code>, and each key <code>rooms[i][j]</code> is an integer in <code>[0, 1, ..., N-1]</code> where <code>N = rooms.length</code>.&nbsp; A key <code>rooms[i][j] = v</code>&nbsp;opens the room with number <code>v</code>.</p>

<p>Initially, all the rooms start locked (except for room <code>0</code>).&nbsp;</p>

<p>You can walk back and forth between rooms freely.</p>

<p>Return <code>true</code>&nbsp;if and only if you can enter&nbsp;every room.</p>

<ol>
</ol>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>[[1],[2],[3],[]]
<strong>Output: </strong>true
<strong>Explanation:  </strong>
We start in room 0, and pick up key 1.
We then go to room 1, and pick up key 2.
We then go to room 2, and pick up key 3.
We then go to room 3.  Since we were able to go to every room, we return true.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>[[1,3],[3,0,1],[2],[0]]
<strong>Output: </strong>false
<strong>Explanation: </strong>We can&#39;t enter the room with number 2.
</pre>

<p><b>Note:</b></p>

<ol>
	<li><code>1 &lt;= rooms.length &lt;=&nbsp;1000</code></li>
	<li><code>0 &lt;= rooms[i].length &lt;= 1000</code></li>
	<li>The number of keys in all rooms combined is at most&nbsp;<code>3000</code>.</li>
</ol>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Graph (graph)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Depth-First Search [Accepted]

**Intuition and Algorithm**

When visiting a room for the first time, look at all the keys in that room.  For any key that hasn't been used yet, add it to the todo list (`stack`) for it to be used.

See the comments of the code for more details.

<iframe src="https://leetcode.com/playground/gB4GZBww/shared" frameBorder="0" width="100%" height="446" name="gB4GZBww"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N + E)$$, where $$N$$ is the number of rooms, and $$E$$ is the total number of keys.

* Space Complexity:  $$O(N)$$ in additional space complexity, to store `stack` and `seen`.

## Accepted Submission (python3)
```python3
class Solution:
    # def dfs(self, rooms, room, unlocked):
    #     unlocked.add(room)
    #     for nr in rooms[room]:
    #         if not nr in unlocked:
    #             self.dfs(rooms, nr, unlocked)

    def canVisitAllRooms(self, rooms):
        """
        :type rooms: List[List[int]]
        :rtype: bool
        """
        # unlocked = set()
        # self.dfs(rooms, 0, unlocked)
        # return len(unlocked) == len(rooms)
        unlocked = set()
        stack = []
        stack.append(0)
        while stack:
            room = stack.pop()
            if room not in unlocked:
                unlocked.add(room)
                stack.extend(rooms[room])
        return len(unlocked) == len(rooms)

```

## Top Discussions
### Straight Forward
- Author: lee215
- Creation Date: Sun May 27 2018 11:02:16 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 03:37:06 GMT+0800 (Singapore Standard Time)

<p>
**C++:**
```
    bool canVisitAllRooms(vector<vector<int>>& rooms) {
        stack<int> dfs; dfs.push(0);
        unordered_set<int> seen = {0};
        while (!dfs.empty()) {
            int i = dfs.top(); dfs.pop();
            for (int j : rooms[i])
                if (seen.count(j) == 0) {
                    dfs.push(j);
                    seen.insert(j);
                    if (rooms.size() == seen.size()) return true;
                }
        }
        return rooms.size() == seen.size();
    }
```

**Java:**
```
    public boolean canVisitAllRooms(List<List<Integer>> rooms) {
        Stack<Integer> dfs = new Stack<>(); dfs.add(0);
        HashSet<Integer> seen = new HashSet<Integer>(); seen.add(0);
        while (!dfs.isEmpty()) {
            int i = dfs.pop();
            for (int j : rooms.get(i))
                if (!seen.contains(j)) {
                    dfs.add(j);
                    seen.add(j);
                    if (rooms.size() == seen.size()) return true;
                }
        }
        return rooms.size() == seen.size();
    }
```
**Python:**
```
    def canVisitAllRooms(self, rooms):
        dfs = [0]
        seen = set(dfs)
        while dfs:
            i = dfs.pop()
            for j in rooms[i]:
                if j not in seen:
                    dfs.append(j)
                    seen.add(j)
                    if len(seen) == len(rooms): return True
        return len(seen) == len(rooms)
```
</p>


### Clean Code
- Author: navid
- Creation Date: Sun May 27 2018 11:24:33 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 10:13:26 GMT+0800 (Singapore Standard Time)

<p>
```
HashSet<Integer> enteredRooms = new HashSet<>();

public boolean canVisitAllRooms(List<List<Integer>> rooms) {
    enterRoom(0, rooms);
    return enteredRooms.size() == rooms.size();
}

private void enterRoom(int roomId, List<List<Integer>> rooms) {
    enteredRooms.add(roomId);
    List<Integer> keysInRoom = rooms.get(roomId);
    for (int key: keysInRoom)
        if (!enteredRooms.contains(key))
            enterRoom(key, rooms);
}
</p>


### BFS (9 lines, 10ms) and DFS (7 lines, 18ms) in C++ w/ beginner friendly explanation
- Author: zackchen95
- Creation Date: Sat Jun 02 2018 06:16:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 02 2018 06:16:17 GMT+0800 (Singapore Standard Time)

<p>
**BFS:**
We use an unordered_set to record the rooms visited, and a queue for BFS. Push room 0 to queue first.
While the queue is not empty, meaning we have more rooms to visit, we check all keys in the current room, if we haven\'t visit all of these rooms, push it to the queue.
```
class Solution {
public:
    bool canVisitAllRooms(vector<vector<int>>& rooms) {
        unordered_set<int> visited;
        queue<int> to_visit;
        to_visit.push(0);
        while(!to_visit.empty()) {
            int curr = to_visit.front();
            to_visit.pop();
            visited.insert(curr);
            for (int k : rooms[curr]) if (visited.find(k) == visited.end()) to_visit.push(k);
        }
        return visited.size() == rooms.size();
    }
};
```

**DFS:**
When we enter a room, mark it as visited.
Then we put the keys in the current room to our unordered_set keys.
Check all the keys we have, if we haven\'t visited all corresponding room, go DFS.
If we have visited all rooms, number of visited rooms should be the same as number of rooms.
```
class Solution {
    void dfs(vector<vector<int>>& rooms, unordered_set<int> & keys, unordered_set<int> & visited, int curr) {
        visited.insert(curr);
        for (int k : rooms[curr]) keys.insert(k);
        for (int k : keys) if (visited.find(k) == visited.end()) dfs(rooms, keys, visited, k);
    }
    
public:
    bool canVisitAllRooms(vector<vector<int>>& rooms) {
        unordered_set<int> keys;
        unordered_set<int> visited;
        dfs(rooms, keys, visited, 0);
        return visited.size() == rooms.size();
    }
};
```

**Possible followup**: Why would you use BFS over DFS in this solution (except that DFS takes longer here)?
Ans: If input is too large, DFS might cause stack overflow.

**Some general ideas on how to tackle a whatever-first search problem:**
BFS:
General Idea of BFS is that we need to use a queue to record which rooms / nodes / blocks that we want to visit in the future, usually we use an unordered_set to record the places that we have visited, so that we don\'t visit them anymore. 
First we want to push the starting point to the queue, we use a while (!queue.empty()) to make sure there are still work to do. Take the front element from queue, pop queue, then do what you gotta do, for instance insert it into visited, do some calculation, return something if this guy met certain conditions, blah blah. Then we get its neighbors, push them to the queue. 
In general, we want to expand one step at a time. 

DFS:
The general idea of DFS is that we recursively call itself with changing parameters. When we enter DFS, we normally want to check if certain conditions are met, for instance we want to visit all nodes: we first check if our unordered_set visited.size() is the same as node size. If so, return true. We also want to check if current DFS is viable, like is i or j out of boundary? If they are, you should return / return false. 
Then for each possible ways to go, we try DFS on them: go left, go right, go up, go down, you name it! So unlike BFS, DFS is more like going to one direction straight, if it works that\'s great, if it doesn\'t, we come back to the previous recursive call and try another way.
</p>


