---
title: "Recover a Tree From Preorder Traversal"
weight: 1002
#id: "recover-a-tree-from-preorder-traversal"
---
## Description
<div class="description">
<p>We run a&nbsp;preorder&nbsp;depth first search on the <code>root</code> of a binary tree.</p>

<p>At each node in this traversal, we output <code>D</code> dashes (where <code>D</code> is the <em>depth</em> of this node), then we output the value of this node.&nbsp;&nbsp;<em>(If the depth of a node is <code>D</code>, the depth of its immediate child is <code>D+1</code>.&nbsp; The depth of the root node is <code>0</code>.)</em></p>

<p>If a node has only one child, that child is guaranteed to be the left child.</p>

<p>Given the output <code>S</code> of this traversal, recover the tree and return its <code>root</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/04/08/recover-a-tree-from-preorder-traversal.png" style="width: 320px; height: 200px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;1-2--3--4-5--6--7&quot;</span>
<strong>Output: </strong><span id="example-output-1">[1,2,5,3,4,6,7]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/04/11/screen-shot-2019-04-10-at-114101-pm.png" style="width: 256px; height: 250px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;1-2--3---4-5--6---7&quot;</span>
<strong>Output: </strong><span id="example-output-2">[1,2,5,3,null,6,null,4,null,7]</span></pre>
</div>

<div>
<p>&nbsp;</p>

<div>
<p><strong>Example 3:</strong></p>

<p><span><img alt="" src="https://assets.leetcode.com/uploads/2019/04/11/screen-shot-2019-04-10-at-114955-pm.png" style="width: 276px; height: 250px;" /></span></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">&quot;1-401--349---90--88&quot;</span>
<strong>Output: </strong><span id="example-output-3">[1,401,null,349,88,90]</span>
</pre>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li>The number of nodes in the original tree is between <code>1</code> and <code>1000</code>.</li>
	<li>Each node will have a value between <code>1</code> and <code>10^9</code>.</li>
</ul>
</div>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- LinkedIn - 2 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Iterative Stack Solution
- Author: lee215
- Creation Date: Sun Apr 14 2019 12:08:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 14 2019 12:08:08 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
We save the construction path in a `stack`.
In each loop,
we get the number `level` of `\'-\'`
we get the value `val` of `node` to add.

If the size of stack is bigger than the level of node,
we pop the stack until it\'s not.

Finally we return the first element in the stack, as it\'s root of our tree.

## **Complexity**

Time `O(S)`, Space `O(N)`


**Java**
```
    public TreeNode recoverFromPreorder(String S) {
        int level, val;
        Stack<TreeNode> stack = new Stack<>();
        for (int i = 0; i < S.length();) {
            for (level = 0; S.charAt(i) == \'-\'; i++) {
                level++;
            }
            for (val = 0; i < S.length() && S.charAt(i) != \'-\'; i++) {
                val = val * 10 + (S.charAt(i) - \'0\');
            }
            while (stack.size() > level) {
                stack.pop();
            }
            TreeNode node = new TreeNode(val);
            if (!stack.isEmpty()) {
                if (stack.peek().left == null) {
                    stack.peek().left = node;
                } else {
                    stack.peek().right = node;
                }
            }
            stack.add(node);
        }
        while (stack.size() > 1) {
            stack.pop();
        }
        return stack.pop();
    }
```

**C++**
```
    TreeNode* recoverFromPreorder(string S) {
        vector<TreeNode*> stack;
        for (int i = 0, level, val; i < S.length();) {
            for (level = 0; S[i] == \'-\'; i++)
                level++;
            for (val = 0; i < S.length() && S[i] != \'-\'; i++)
                val = val * 10 + S[i] - \'0\';
            TreeNode* node = new TreeNode(val);
            while (stack.size() > level) stack.pop_back();
            if (!stack.empty())
                if (!stack.back()->left) stack.back()->left = node;
                else stack.back()->right = node;
            stack.push_back(node);
        }
        return stack[0];
    }
```

**Python:**
```
    def recoverFromPreorder(self, S):
        stack, i = [], 0
        while i < len(S):
            level, val = 0, ""
            while i < len(S) and S[i] == \'-\':
                level, i = level + 1, i + 1
            while i < len(S) and S[i] != \'-\':
                val, i = val + S[i], i + 1
            while len(stack) > level:
                stack.pop()
            node = TreeNode(val)
            if stack and stack[-1].left is None:
                stack[-1].left = node
            elif stack:
                stack[-1].right = node
            stack.append(node)
        return stack[0]
```


</p>


### Java recursive solution.
- Author: Honiess
- Creation Date: Sun Apr 14 2019 12:20:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 14 2019 12:20:04 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    int index = 0;
    public TreeNode recoverFromPreorder(String S) {
        return helper(S, 0);
    }
    
    public TreeNode helper(String s, int depth) {
        int numDash = 0;
        while (index + numDash < s.length() && s.charAt(index + numDash) == \'-\') {
            numDash++;
        }
        if (numDash != depth) return null;
        int next = index + numDash;
        while (next < s.length() && s.charAt(next) != \'-\') next++;
        int val = Integer.parseInt(s.substring(index + numDash, next));
        index = next;
        TreeNode root = new TreeNode(val);
        root.left = helper(s, depth + 1);
        root.right = helper(s, depth + 1);
        return root;
    }
}
```
</p>


### [C++] Simple Recursive Preorder
- Author: PhoenixDD
- Creation Date: Sun Apr 14 2019 12:13:04 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 05 2019 15:02:48 GMT+0800 (Singapore Standard Time)

<p>
Similar to solving `#331 Verify Preorder Serialization of a Binary Tree` and `#297 Serialize and Deserialize Binary Tree` and a few other similar questions.

The idea is pretty simple, at each depth make sure that the number of `\'-\'` is same as the depth, if it\'s not return `NULL` else continue the recursive preorder traversal.

In general the preorder traversal through recursion is:
```c++
void preorder(Treenode* root)
{
	if(!root)
	{
		cout<<"NULL";
		return;
	}
	cout<<root->val;
	preorder(root->left);
	preorder(root->right);
}
```

What we are doing here is the same, just imagine the string is in form of a tree and make sure that the boundaries for print(case above)/set root->val(this case) or returning/printing NULL are set right (according to the question).

```c++
class Solution {
public:
    TreeNode* recoverFromPreorder(string &S,int &i,int d)
    {
        TreeNode* root=new TreeNode(-1);
        int start=S.find_first_of("1234567890",i);                              //Get index of the first number after i
        if(start-i==d)                                           //If number of \'-\' in between = depth create root with the integer after it
        {
            i=start;
            start=S.find("-",i);
        }
        else                                                    //Since depth is not correct to add the integer return NULL;
            return NULL;
        root->val=stoi(S.substr(i,start-i));                    //Set the correct root value
        i=start;                                                //Move index forward
        root->left=recoverFromPreorder(S,i,d+1);                //Create left subtree
        root->right=recoverFromPreorder(S,i,d+1);               //Create right subtree
        return root;
    }
    TreeNode* recoverFromPreorder(string S)
    {
        int i=S.find("-");
        TreeNode* root=new TreeNode(stoi(S.substr(0,i)));       //Get first integer and store it as root
        root->left=recoverFromPreorder(S,i,1);                  //Create left subtree
        root->right=recoverFromPreorder(S,i,1);                 //Create right subtree
        return root;
    }
};
```
**Complexity**
Space: `O(h)` where `h` is the height of tree. This is due to recursion stack.
Time: `O(n)`.
</p>


