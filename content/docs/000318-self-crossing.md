---
title: "Self Crossing"
weight: 318
#id: "self-crossing"
---
## Description
<div class="description">
<p>You are given an array <i>x</i> of <code>n</code> positive numbers. You start at point <code>(0,0)</code> and moves <code>x[0]</code> metres to the north, then <code>x[1]</code> metres to the west, <code>x[2]</code> metres to the south, <code>x[3]</code> metres to the east and so on. In other words, after each move your direction changes counter-clockwise.</p>

<p>Write a one-pass algorithm with <code>O(1)</code> extra space to determine, if your path crosses itself, or not.</p>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<strong>┌───┐
│ &nbsp; │
└───┼──&gt;
&nbsp; &nbsp; │

Input: </strong><code>[2,1,1,2]</code>
<strong>Output: </strong>true
</pre>

<p><b>Example 2:</b></p>

<pre>
<strong>┌──────┐
│ &nbsp; &nbsp; &nbsp;│
│
│
└────────────&gt;

Input:</strong> <code>[1,2,3,4]</code>
<strong>Output: </strong>false 
</pre>

<p><b>Example 3:</b></p>

<pre>
<strong>┌───┐
│ &nbsp; │
└───┼&gt;

Input:</strong> <code>[1,1,1,1]</code>
<strong>Output:</strong> true 
</pre>

</div>

## Tags
- Math (math)

## Companies
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Oms with explanation
- Author: KuangYuan
- Creation Date: Tue Feb 23 2016 14:38:48 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 17:30:36 GMT+0800 (Singapore Standard Time)

<p>
    // Categorize the self-crossing scenarios, there are 3 of them: 
    // 1. Fourth line crosses first line and works for fifth line crosses second line and so on...
    // 2. Fifth line meets first line and works for the lines after
    // 3. Sixth line crosses first line and works for the lines after
    public class Solution {
        public boolean isSelfCrossing(int[] x) {
            int l = x.length;
            if(l <= 3) return false;
            
            for(int i = 3; i < l; i++){
                if(x[i] >= x[i-2] && x[i-1] <= x[i-3]) return true;  //Fourth line crosses first line and onward
                if(i >=4)
                {
                    if(x[i-1] == x[i-3] && x[i] + x[i-4] >= x[i-2]) return true; // Fifth line meets first line and onward
                }
                if(i >=5)
                {
                    if(x[i-2] - x[i-4] >= 0 && x[i] >= x[i-2] - x[i-4] && x[i-1] >= x[i-3] - x[i-5] && x[i-1] <= x[i-3]) return true;  // Sixth line crosses first line and onward
                }
            }
            return false;
        }
    }
</p>


### Another python...
- Author: StefanPochmann
- Creation Date: Wed Feb 24 2016 02:43:16 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 04 2018 12:34:20 GMT+0800 (Singapore Standard Time)

<p>
Checking out every six pack.

**Solution 1**

    def isSelfCrossing(self, x):
        return any(d >= b > 0 and (a >= c or a >= c-e >= 0 and f >= d-b)
                   for a, b, c, d, e, f in ((x[i:i+6] + [0] * 6)[:6]
                                            for i in xrange(len(x))))

**Solution 2**

    def isSelfCrossing(self, x):
        b = c = d = e = 0
        for a in x:
            if d >= b > 0 and (a >= c or a >= c-e >= 0 and f >= d-b):
                return True
            b, c, d, e, f = a, b, c, d, e
        return False

**Explanation**

                b                              b
       +----------------+             +----------------+
       |                |             |                |
       |                |             |                | a
     c |                |           c |                |
       |                | a           |                |    f
       +----------->    |             |                | <----+
                d       |             |                |      | e
                        |             |                       |
                                      +-----------------------+
                                                   d

Draw a line of length `a`. Then draw further lines of lengths `b`, `c`, etc. How does the `a`-line get crossed? From the left by the `d`-line or from the right by the `f`-line, see the above picture. I just encoded the criteria for actually crossing it.

Two details:

- In both cases, `d` needs to be at least `b`. In the first case to cross the `a`-line directly, and in the second case to get behind it so that the `f`-line can cross it. So I factored out `d >= b`.
- The "special case" of the `e`-line stabbing the `a`-line from below is covered because in that case, the `f`-line "crosses" it (note that even if there is no actual `f`-line, my code uses `f = 0` and thus still finds that "crossing").
</p>


### Simple Java Solution
- Author: munsteur
- Creation Date: Wed Mar 09 2016 06:21:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 15 2018 15:15:12 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public boolean isSelfCrossing(int[] x) {
            if (x.length <= 3) {
                return false;
            }
            int i = 2;
            // keep spiraling outward
            while (i < x.length && x[i] > x[i - 2]) {
                i++;
            }
            if (i >= x.length) {
                return false;
            }
            // transition from spiraling outward to spiraling inward
            if ((i >= 4 && x[i] >= x[i - 2] - x[i - 4]) ||
                    (i == 3 && x[i] == x[i - 2])) {
                x[i - 1] -= x[i - 3];
            }
            i++;
            // keep spiraling inward
            while (i < x.length) {
                if (x[i] >= x[i - 2]) {
                    return true;
                }
                i++;
            }
            return false;
        }
    }
</p>


