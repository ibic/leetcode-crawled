---
title: "Binary Tree Zigzag Level Order Traversal"
weight: 103
#id: "binary-tree-zigzag-level-order-traversal"
---
## Description
<div class="description">
<p>Given a binary tree, return the <i>zigzag level order</i> traversal of its nodes' values. (ie, from left to right, then right to left for the next level and alternate between).</p>

<p>
For example:<br />
Given binary tree <code>[3,9,20,null,null,15,7]</code>,<br />
<pre>
    3
   / \
  9  20
    /  \
   15   7
</pre>
</p>
<p>
return its zigzag level order traversal as:<br />
<pre>
[
  [3],
  [20,9],
  [15,7]
]
</pre>
</p>
</div>

## Tags
- Stack (stack)
- Tree (tree)
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 19 (taggedByAdmin: false)
- Microsoft - 8 (taggedByAdmin: true)
- ByteDance - 4 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)
- Qualtrics - 2 (taggedByAdmin: false)
- Flipkart - 2 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: true)
- Samsung - 2 (taggedByAdmin: false)
- ServiceNow - 4 (taggedByAdmin: false)
- Oracle - 4 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: BFS (Breadth-First Search)

**Intuition**

Following the description of the problem, the most intuitive solution would be the *BFS* (Breadth-First Search) approach through which we traverse the tree level-by-level.

The default ordering of BFS within a single level is from left to right. As a result, we should adjust the BFS algorithm a bit to generate the desired zigzag ordering.

>One of the keys here is to store the values that are of the same level with the `deque` (double-ended queue) data structure, where we could add new values on either end of a queue.

So if we want to have the ordering of **FIFO** (first-in-first-out), we simply append the new elements to the *tail* of the queue, _i.e._ the late comers stand last in the queue. While if we want to have the ordering of **FILO** (first-in-last-out), we insert the new elements to the *head* of the queue, _i.e._ the late comers jump the queue.

![pic](../Figures/103/103_BFS.png)


**Algorithm**

There are several ways to implement the BFS algorithm.

- One way would be that we run a two-level nested loop, with the *outer loop* iterating each level on the tree, and with the *inner loop* iterating each node within a single level.
<br/>
- We could also implement BFS with a single loop though. The trick is that we append the nodes to be visited into a queue and we separate nodes of different levels with a sort of **delimiter** (_e.g._ an empty node). The delimiter marks the end of a level, as well as the beginning of a new level.

Here we adopt the *second* approach above. One can start with the normal BFS algorithm, upon which we add a touch of *zigzag* order with the help of `deque`. For each level, we start from an empty deque container to hold all the values of the same level. Depending on the ordering of each level, _i.e._ either from-left-to-right or from-right-to-left, we decide at which end of the deque to add the new element:

![pic](../Figures/103/103_deque.png)

- For the ordering of from-left-to-right (FIFO), we _append_ the new element to the **_tail_** of the queue, so that the element that comes late would get out late as well. As we can see from the above graph, given an input sequence of `[1, 2, 3, 4, 5]`, with FIFO ordering, we would have an output sequence of `[1, 2, 3, 4, 5]`.

- For the ordering of from-right-to-left (FILO), we _insert_ the new element to the **_head_** of the queue, so that the element that comes late would get out first. With the same input sequence of `[1, 2, 3, 4, 5]`, with FILO ordering, we would obtain an output sequence of `[5, 4, 3, 2, 1]`.

<iframe src="https://leetcode.com/playground/CuzksuMQ/shared" frameBorder="0" width="100%" height="500" name="CuzksuMQ"></iframe>

Note: as an alternative approach, one can also implement the normal BFS algorithm first, which would generate the ordering of from-left-to-right for each of the levels. Then, at the end of the algorithm, we can simply **_reverse_** the ordering of certain levels, following the zigzag steps.

**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(N)$$, where $$N$$ is the number of nodes in the tree.

    - We visit each node once and only once.

    - In addition, the insertion operation on either end of the deque takes a constant time, rather than using the array/list data structure where the inserting at the head could take the $$\mathcal{O}(K)$$ time where $$K$$ is the length of the list.

- Space Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the number of nodes in the tree.

    - The main memory consumption of the algorithm is the `node_queue` that we use for the loop, apart from the array that we use to keep the final output.

    - As one can see, at any given moment, the `node_queue` would hold the nodes that are _at most_ across two levels. Therefore, at most, the size of the queue would be no more than $$2 \cdot L$$, assuming $$L$$ is the maximum number of nodes that might reside on the same level. Since we have a binary tree, the level that contains the most nodes could occur to consist all the leave nodes in a full binary tree, which is roughly $$L = \frac{N}{2}$$. As a result, we have the space complexity of $$2 \cdot \frac{N}{2} = N$$ in the worst case.   
<br/>
<br/>

---
#### Approach 2: DFS (Depth-First Search)

**Intuition**

Though not intuitive, we could also obtain the _BFS_ traversal ordering via the _DFS_ (Depth-First Search) traversal in the tree.

>The trick is that during the DFS traversal, we maintain the results in a _global_ array that is indexed by the level, _i.e._ the element `array[level]` would contain all the nodes that are at the same level. The global array would then be referred and updated at each step of DFS.

![pic](../Figures/103/103_DFS.png)

Similar with the above modified BFS algorithm, we employ the `deque` data structure to hold the nodes that are of the same level, and we alternate the insertion direction (_i.e._ either to the head or to the tail) to generate the desired output ordering.

**Algorithm**

Here we implement the DFS algorithm via _recursion_. We define a recursive function called `DFS(node, level)` which only takes care of the current `node` which is located at the specified `level`. Within the function, here are three steps that we would perform:

- If this is the first time that we visit any node at the `level`, _i.e._ the deque for the level does not exist, then we simply create the deque with the current node value as the initial element.

- If the deque for this level exists, then depending on the ordering, we insert the current node value either to the head or to the tail of the queue.

- At the end, we _recursively_ call the function for each of its child nodes.


<iframe src="https://leetcode.com/playground/5kZDYMsP/shared" frameBorder="0" width="100%" height="500" name="5kZDYMsP"></iframe>

It might go without saying that, one can also implement the DFS traversal via **_iteration_** rather than recursion, which could be one of the followup questions by an interviewer.

**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(N)$$, where $$N$$ is the number of nodes in the tree.

    - Same as the previous BFS approach, we visit each node once and only once.

- Space Complexity: $$\mathcal{O}(H)$$, where $$H$$ is the height of the tree, _i.e._ the number of levels in the tree, which would be roughly $$\log_2{N}$$.

    - Unlike the BFS approach, in the DFS approach, we do not need to maintain the `node_queue` data structure for the traversal.

    - However, the function recursion would incur additional memory consumption on the _function call stack_. As we can see, the size of the call stack for any invocation of `DFS(node, level)` would be exactly the number of `level` that the current node resides on. Therefore, the space complexity of our DFS algorithm is $$\mathcal{O}(\log_2{N})$$ which is much better than the BFS approach.
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My accepted JAVA solution
- Author: awaylu
- Creation Date: Wed Sep 17 2014 14:17:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 12:13:36 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public List<List<Integer>> zigzagLevelOrder(TreeNode root) 
        {
            List<List<Integer>> sol = new ArrayList<>();
            travel(root, sol, 0);
            return sol;
        }
        
        private void travel(TreeNode curr, List<List<Integer>> sol, int level)
        {
            if(curr == null) return;
            
            if(sol.size() <= level)
            {
                List<Integer> newLevel = new LinkedList<>();
                sol.add(newLevel);
            }
            
            List<Integer> collection  = sol.get(level);
            if(level % 2 == 0) collection.add(curr.val);
            else collection.add(0, curr.val);
            
            travel(curr.left, sol, level + 1);
            travel(curr.right, sol, level + 1);
        }
    }

1.  O(n) solution by using LinkedList along with ArrayList.  So insertion in the inner list and outer list are both O(1),
2.  Using DFS and creating new lists when needed.

should be quite straightforward.  any better answer?
</p>


### [c++] 5ms version: one queue and without reverse operation by using size of each level
- Author: StevenCooks
- Creation Date: Sat Mar 14 2015 04:50:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 22:45:07 GMT+0800 (Singapore Standard Time)

<p>
  
Assuming after traversing the 1st level, nodes in queue are {9, 20, 8}, And we are going to traverse 2nd level, which is even line and should print value from right to left [8, 20, 9]. 

We know there are 3 nodes in current queue, so the vector for this level in final result should be of size 3. 
Then,     queue [i] -> goes to ->  vector[queue.size() - 1 - i]
i.e. the ith node in current queue should be placed in (queue.size() - 1 - i) position in vector for that line.
 
For example, for node(9), it's index in queue is 0, so its index in vector should be (3-1-0) = 2. 


    vector<vector<int> > zigzagLevelOrder(TreeNode* root) {
        if (root == NULL) {
            return vector<vector<int> > ();
        }
        vector<vector<int> > result;
    
        queue<TreeNode*> nodesQueue;
        nodesQueue.push(root);
        bool leftToRight = true;
    
        while ( !nodesQueue.empty()) {
            int size = nodesQueue.size();
            vector<int> row(size);
            for (int i = 0; i < size; i++) {
                TreeNode* node = nodesQueue.front();
                nodesQueue.pop();

                // find position to fill node's value
                int index = (leftToRight) ? i : (size - 1 - i);

                row[index] = node->val;
                if (node->left) {
                    nodesQueue.push(node->left);
                }
                if (node->right) {
                    nodesQueue.push(node->right);
                }
            }
            // after this level
            leftToRight = !leftToRight;
            result.push_back(row);
        }
        return result;
    }
</p>


### JAVA Double Stack Solution
- Author: tjcd
- Creation Date: Sat Dec 26 2015 22:24:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 03:00:53 GMT+0800 (Singapore Standard Time)

<p>
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
       TreeNode c=root;
       List<List<Integer>> ans =new ArrayList<List<Integer>>();
       if(c==null) return ans;
       Stack<TreeNode> s1=new Stack<TreeNode>();
       Stack<TreeNode> s2=new Stack<TreeNode>();
       s1.push(root);
       while(!s1.isEmpty()||!s2.isEmpty())
       {
           List<Integer> tmp=new ArrayList<Integer>();
            while(!s1.isEmpty())
            {
                c=s1.pop();
                tmp.add(c.val);
                if(c.left!=null) s2.push(c.left);
                if(c.right!=null) s2.push(c.right);
            }
            ans.add(tmp);
            tmp=new ArrayList<Integer>();
            while(!s2.isEmpty())
            {
                c=s2.pop();
                tmp.add(c.val);
                if(c.right!=null)s1.push(c.right);
                if(c.left!=null)s1.push(c.left);
            }
            if(!tmp.isEmpty()) ans.add(tmp);
       }
       return ans;
    }
</p>


