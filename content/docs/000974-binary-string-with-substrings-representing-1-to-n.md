---
title: "Binary String With Substrings Representing 1 To N"
weight: 974
#id: "binary-string-with-substrings-representing-1-to-n"
---
## Description
<div class="description">
<p>Given a binary string <code>S</code> (a string consisting only of &#39;0&#39; and &#39;1&#39;s) and a positive integer <code>N</code>, return true if and only if for every integer X from 1 to N, the binary representation of X is a substring of S.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-1-1">&quot;0110&quot;</span>, N = <span id="example-input-1-2">3</span>
<strong>Output: </strong><span id="example-output-1">true</span>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-2-1">&quot;0110&quot;</span>, N = <span id="example-input-2-2">4</span>
<strong>Output: </strong><span id="example-output-2">false</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= S.length &lt;= 1000</code></li>
	<li><code>1 &lt;= N &lt;= 10^9</code></li>
</ol>

</div>

## Tags
- String (string)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] O(S)
- Author: lee215
- Creation Date: Sun Mar 24 2019 11:03:16 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 15 2019 17:42:58 GMT+0800 (Singapore Standard Time)

<p>
# Solution 1
## **Intuition**:
The construction of `S` is a NP problem,
it\'s time consuming to construct a short `S`.
I notice `S.length <= 1000`, which is too small to make a big `N`.

This intuition lead me to the following 1-line python solution,
which can be implemented very fast.

<br>

## **Explanation**:
Check if `S` contains binary format of `N`,
If so, continue to check `N - 1`.
<br>

## **Time Complexity**

Have a look at the number 1001 ~ 2000 and their values in binary.

1001 0b1111101001
1002 0b1111101010
1003 0b1111101011
...
1997 0b11111001101
1998 0b11111001110
1999 0b11111001111
2000 0b11111010000


The number 1001 ~ 2000 have 1000 different continuous 10 digits.
The string of length `S` has at most `S - 9` different continuous 10 digits.
So `S <= 1000`, the achievable `N <= 2000`.
So `S * 2` is a upper bound for achievable `N`.
If `N > S * 2`, we can return `false` directly in `O(1)`

Note that it\'s the same to prove with the numbers 512 ~ 1511, or even smaller range.

`N/2` times check, `O(S)` to check a number in string `S`.
The overall time complexity has upper bound `O(S^2)`.


**Java:**
```
    public boolean queryString(String S, int N) {
        for (int i = N; i > N / 2; --i)
            if (!S.contains(Integer.toBinaryString(i)))
                return false;
        return true;
    }
```
**C++**
```
    bool queryString(string S, int N) {
        for (int i = N; i > N / 2;  --i) {
            string b = bitset<32>(i).to_string();
            if (S.find(b.substr(b.find("1"))) == string::npos)
                return false;
        }
        return true;
    }
```
**Python:**
```
    def queryString(self, S, N):
        return all(bin(i)[2:] in S for i in xrange(N, N / 2, -1))
```
<br>

# Solution2

Continue to improve the above solution to `O(S)`

**Python:**
```
    def queryString(self, S, N):
        if N > len(S) * 2: return False
        L = len(bin(N)) - 2
        seen = {S[i:i + L] for i in xrange(len(S))} | {S[i:i + L - 1] for i in xrange(len(S))}
        return all(bin(i)[2:] in seen for i in xrange(N, N / 2, -1))
```
`seen` can be calculated by two passes of rolling hash.

</p>


### C++ O(S log N) vs. O(N * (S + log N))
- Author: votrubac
- Creation Date: Sun Mar 24 2019 12:05:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 24 2019 12:05:00 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
We can process the entire string and track all numbers [1..N] that we can build.

OR

We can generate binary string representation for ```[1..N]```, and search for it in the string.
## Solution 1
- For each non-zero position ```i``` in S, gradually build ```num``` while ```num <= N```.
- Track each ```num``` in ```seen```, increment ```X``` for new ```num```.
- Return true if we built all numbers from 1 to N (```X == N```).
```
bool queryString(string S, int N, int X = 0) {
  vector<bool> seen(N);
  for (auto i = 0; i < S.size() && X < N; ++i) {
    if (S[i] == \'0\') continue;
    for (auto j = i, num = 0; num <= N && j < S.size(); ++j) {
      num = (num << 1) + S[j] - \'0\';
      if (num > 0 && num <= N && !seen[num - 1]) {
        ++X;
        seen[num - 1] = true;
      }
    }
  }
  return X == N;
}
```
## Complexity Analysis
Runtime: *O(S log N)*, where ```S``` is the string size. For every position, we analyze ```log N``` digits.
Memory: *O(N)* as we need to track all numbers from 1 to ```N```.
# Solution 2
Iterate from N to 1. Build binary string and search.
```
bool queryString(string S, int N) {
  while (N > 0) {
    auto s = bitset<32>(N--).to_string();
    if (S.find(s.substr(s.find("1"))) == string::npos) return false;
  }
  return true;
}
```
## Complexity Analysis
Runtime: *O(N * (S + log N))*, where ```S``` is the string size. We search N times, and every search is *O(S + log N)*.
Memory: *O(1)*.
</p>


### [Java] O(31 * 31 * S) Solution with Explanation Included
- Author: simonzhu91
- Creation Date: Mon Mar 25 2019 07:26:38 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 25 2019 07:26:38 GMT+0800 (Singapore Standard Time)

<p>
**Small Rant -- If not interested, please skip to idea section below**

I noticed that this problem can AC with many solutions. Some solution seem to have `O(N*S)`, `O(S^3)`, `O(S^2)`, `O(S log N)` runtimes. Based on some ideas about continuous digits, post show `O(N*S)` solution can be modified slightly to `O(S^2)` . I feel a little unsatisfied with that idea because in interview or pressure situation, without clever intuition about continuous bits, it is hard to come up with this kind of solution. 

Also, worth mentioning problem input states that `N <= 10^9` and `S <= 10^3`, so a little hard to think of looping on N or 3 nested loops on S since TLE. But apparently test cases for this problem doesn\'t seem to TLE in these case.

Code for these ideas are below:

```
// O(S^3)
class Solution {
    public boolean queryString(String S, int N) {
        Set<Integer> set = new HashSet<>();
        for(int i = 0; i < S.length(); i++){
            for(int j = i + 1; j <= S.length(); j++){
                int v = Integer.parseInt(S.substring(i, j), 2);
                
                if(v > 0 && v <= N){
                    set.add(v);
                } else {
                    break;
                }
            }
        }
        
        return set.size() == N;
    }
}
```

```
// O(N * S)
class Solution {
     public boolean queryString(String S, int N) {
        for (int i = 1; i <= N; ++i)
            if (!S.contains(Integer.toBinaryString(i)))
                return false;
        return true;
    }
}
```

Other approach with runtimes `O(S^2)` and `O(S log N)` work well and makes sense for this problem. But I just want to share a different approach to problem that uses idea that no matter string length, Max Integer only use 31 bits. Writing the code is simple and not too many tricks.

**Idea:**
We want to find every possible substring in S and check if it\'s integer value covers all values from `[1, N]`, we are done. Since string can be 1000 in length, maybe too slow to loop over every index for start and then rest of index for end, and then call substring to get content (takes `S^3` time). Of course, you can use `StringBuilder` and build substring while you are looping for the end (optimizing extra substring call), `S^2` and would work. But for every index i, you really only need to consider `[Math.max(i-30, 0), i]` because max int only use 31 bits.

Hence solution below:

```
// O(S * 31 * 31)
class Solution {
    public boolean queryString(String S, int N) {
    	Set<Integer> set = new HashSet<>();

    	for (int i = 0; i < S.length(); i++){
    		for (int j = Math.max(i - 30, 0); j <= i; j++){
    			int v = 0;
                
    			for (int bit = j;bit <= i; bit++) {
    				v = v * 2 + (S.charAt(bit) - \'0\');    				
    			}    	
                
    			if (v > 0 && v <= N) {
    				set.add(v);
    			}    			
    		}
    	}
    	return set.size() == N;    	
    }
}
```

</p>


