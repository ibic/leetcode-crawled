---
title: "Find the Shortest Superstring"
weight: 893
#id: "find-the-shortest-superstring"
---
## Description
<div class="description">
<p>Given an array A of strings, find any&nbsp;smallest string that contains each string in <code>A</code> as a&nbsp;substring.</p>

<p>We may assume that no string in <code>A</code> is substring of another string in <code>A</code>.</p>

<div>&nbsp;</div>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;alex&quot;,&quot;loves&quot;,&quot;leetcode&quot;]</span>
<strong>Output: </strong><span id="example-output-1">&quot;alexlovesleetcode&quot;</span>
<strong>Explanation: </strong>All permutations of &quot;alex&quot;,&quot;loves&quot;,&quot;leetcode&quot; would also be accepted.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[&quot;catg&quot;,&quot;ctaagt&quot;,&quot;gcta&quot;,&quot;ttca&quot;,&quot;atgcatc&quot;]</span>
<strong>Output: </strong><span id="example-output-2">&quot;gctaagttcatgcatc&quot;</span></pre>

<p>&nbsp;</p>
</div>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 12</code></li>
	<li><code>1 &lt;= A[i].length &lt;= 20</code></li>
</ol>

<div>
<div>&nbsp;</div>
</div>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 6 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Dynamic Programming

**Intuition**

We have to put the words into a row, where each word may overlap the previous word.  This is because no word is contained in any word.

Also, it is sufficient to try to maximize the total overlap of the words.

Say we have put some words down in our row, ending with word `A[i]`.  Now say we put down word `A[j]` as the next word, where word `j` hasn't been put down yet.  The overlap increases by `overlap(A[i], A[j])`.

We can use dynamic programming to leverage this recursion.  Let `dp(mask, i)` be the total overlap after putting some words down (represented by a bitmask `mask`), for which `A[i]` was the last word put down.  Then, the key recursion is `dp(mask ^ (1<<j), j) = max(overlap(A[i], A[j]) + dp(mask, i))`, where the `j`th bit is not set in mask, and `i` ranges over all bits set in `mask`.

Of course, this only tells us what the maximum overlap is for each set of words.  We also need to remember each choice along the way (ie. the specific `i` that made `dp(mask ^ (1<<j), j)` achieve a minimum) so that we can reconstruct the answer.

**Algorithm**

Our algorithm has 3 main components:

* Precompute `overlap(A[i], A[j])` for all possible `i, j`.
* Calculate `dp[mask][i]`, keeping track of the "`parent`" `i` for each `j` as described above.
* Reconstruct the answer using `parent` information.

Please see the implementation for more details about each section.

<iframe src="https://leetcode.com/playground/HJGrVYEU/shared" frameBorder="0" width="100%" height="500" name="HJGrVYEU"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2 (2^N + W))$$, where $$N$$ is the number of words, and $$W$$ is the maximum length of each word.

* Space Complexity:  $$O(N (2^N + W))$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Travelling Salesman Problem
- Author: wangzi6147
- Creation Date: Sun Nov 18 2018 12:17:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 18 2018 12:17:52 GMT+0800 (Singapore Standard Time)

<p>
[Travelling Salesman Problem](https://en.wikipedia.org/wiki/Travelling_salesman_problem)

1. `graph[i][j]` means the length of string to append when `A[i]` followed by `A[j]`. eg. `A[i] = abcd`, `A[j] = bcde`, then `graph[i][j] = 1`
2. Then the problem becomes to: find the shortest path in this graph which visits every node exactly once. This is a Travelling Salesman Problem.
3. Apply TSP DP solution. Remember to record the path.

Time complexity: `O(n^2 * 2^n)`

```
class Solution {
    public String shortestSuperstring(String[] A) {
        int n = A.length;
        int[][] graph = new int[n][n];
        // build the graph
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                graph[i][j] = calc(A[i], A[j]);
                graph[j][i] = calc(A[j], A[i]);
            }
        }
        int[][] dp = new int[1 << n][n];
        int[][] path = new int[1 << n][n];
        int last = -1, min = Integer.MAX_VALUE;
		
        // start TSP DP
        for (int i = 1; i < (1 << n); i++) {
            Arrays.fill(dp[i], Integer.MAX_VALUE);
            for (int j = 0; j < n; j++) {
                if ((i & (1 << j)) > 0) {
                    int prev = i - (1 << j);
                    if (prev == 0) {
                        dp[i][j] = A[j].length();
                    } else {
                        for (int k = 0; k < n; k++) {
                            if (dp[prev][k] < Integer.MAX_VALUE && dp[prev][k] + graph[k][j] < dp[i][j]) {
                                dp[i][j] = dp[prev][k] + graph[k][j];
                                path[i][j] = k;
                            }
                        }
                    }
                }
                if (i == (1 << n) - 1 && dp[i][j] < min) {
                    min = dp[i][j];
                    last = j;
                }
            }
        }
		
        // build the path
        StringBuilder sb = new StringBuilder();
        int cur = (1 << n) - 1;
        Stack<Integer> stack = new Stack<>();
        while (cur > 0) {
            stack.push(last);
            int temp = cur;
            cur -= (1 << last);
            last = path[temp][last];
        }
		
        // build the result
        int i = stack.pop();
        sb.append(A[i]);
        while (!stack.isEmpty()) {
            int j = stack.pop();
            sb.append(A[j].substring(A[j].length() - graph[i][j]));
            i = j;
        }
        return sb.toString();
    }
    private int calc(String a, String b) {
        for (int i = 1; i < a.length(); i++) {
            if (b.startsWith(a.substring(i))) {
                return b.length() - a.length() + i;
            }
        }
        return b.length();
    }
}
```
</p>


### Greedy solution is WRONG. If your greedy solution gets AC it only means you are LUCKY enough.
- Author: zhengkai2001
- Creation Date: Sun Nov 18 2018 15:09:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 18 2018 15:09:32 GMT+0800 (Singapore Standard Time)

<p>
By "greedy approach" I mean: each time we merge the two strings with maximum length of overlap, remove them from the string array, and put the merged string into the string array.

You might also have read the [geeksforgeeks article for this problem](https://www.geeksforgeeks.org/shortest-superstring-problem/). It features a greedy approach and it explicitly says it is "approximate".

Consider this test case:
["ift", "tef", "efd", "fdn", "dnete"]

The correct output is "iftefdnete" (concatenate them from left to right), and its length is 10

However, if we apply greedy approach in this order:
1. merge "tef" and "dnete" (overlapping length = 2) -> "dnetef" (["ift", "efd", "fdn", "dnetef"])
2. merge "dnetef" and "efd" (overlapping length = 2) -> "dnetefd" (["ift", "fdn", "dnetefd"])
3. merge "dnetefd" and "fdn" (overlapping length = 2) -> "dnetefdn" (["ift", "dnetefdn"])
4. merge "ift" and "dnetefdn" (overlapping length = 0) -> "iftdnetefdn" (["iftdnetefdn"])

The output is "iftdnetefdn". Its length is 11, which makes it a wrong answer.

So I\'d say the test cases are a little bit weak that so many greedy solutions actually got AC, including the 3rd-place contender\'s solution for this week\'s contest (well, I just want to use it as an example, nothing personal here:))
</p>


### python bfs solution with detailed explanation(with extra Chinese explanation)
- Author: bupt_wc
- Creation Date: Mon Nov 19 2018 13:50:57 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 19 2018 13:50:57 GMT+0800 (Singapore Standard Time)

<p>
you can get Chinese explanation [here](https://buptwc.github.io/2018/11/19/Leetcode-943-Find-the-Shortest-Superstring/)

First, we convert this problem into a graph problem.
for instance, `A = ["catg","ctaagt","gcta","ttca","atgcatc"]`
we regard each string in A as a node, and regard the repeat_length of two string as edge weight. then ,we get this graph `G`:
![image](https://assets.leetcode.com/users/2017111303/image_1542604947.png)

for `G[2][1] = 3`, it means that we can `save` 3 characters length by place `node 1` behind `node 2`, etc `gctaagt`.

In the end, the more we `save`, the shorter string length we will get.

So, the question now is:
In a given graph, we can start in any node, and traverse all node in this graph excatly once, to get the maximum `save` length.
(Note that although there is no connection between 1,3, you can still go from node 1 to node 3.)

first we construct the graph:
```python
def getDistance(s1, s2):
        for i in range(1, len(s1)):
            if s2.startswith(s1[i:]):
                return len(s1) - i
        return 0

n = len(A)
G = [[0]*n for _ in xrange(n)]
for i in range(n):
    for j in range(i+1, n):
        G[i][j] = getDistance(A[i], A[j])
        G[j][i] = getDistance(A[j], A[i])
```

if we consider all possible case, it will be `n*(n-1)*(n-2)*...*1 = n!` possible cases. Obviously it won\u2019t work.
We need to make some optimizations, look at these two cases\uFF1A
`2->1->3->...` and `1->2->3->...`
Both cases traverse three nodes 1, 2, and 3, and is currently at the `node 3`.
for `2->1->3->...`, we assume the `save length` now is `L1`
for `1->2->3->...`, we assume the `save length` now is `L2`
if `L2 < L1`, regardless of how the strategy is adopted afterwards, the second case cannot be better than the first case. So we can discard the second case.

Based on this idea, we difine `d[mask][node]` represent the now `save length` in the status `(mask, node)`.
`mask` represent the node we have traversed, `10011` means traversed `node 0,1,4`
`node` represent the current node.

In BFS solution, such a data structure is stored in the queue `(node, mask, path, s_len)`
`path` means the order of traversed nodes.
`s_len` means the `save length`.

Once `mask` == `(1<<n)-1`, it means that all nodes have been traversed. Now we find the maximum `s_len` and get the corresponding `path`.
Finally, we construct the answer via `path`.

python bfs code:
```python
class Solution(object):
    def shortestSuperstring(self, A):
        def getDistance(s1, s2):
            for i in range(1, len(s1)):
                if s2.startswith(s1[i:]):
                    return len(s1) - i
            return 0

        def pathtoStr(A, G, path):
            res = A[path[0]]
            for i in range(1, len(path)):
                res += A[path[i]][G[path[i-1]][path[i]]:]
            return res

        n = len(A)
        G = [[0]*n for _ in xrange(n)]
        for i in range(n):
            for j in range(i+1, n):
                G[i][j] = getDistance(A[i], A[j])
                G[j][i] = getDistance(A[j], A[i])

        d = [[0]*n for _ in xrange(1<<n)]
        Q = collections.deque([(i, 1<<i, [i], 0) for i in xrange(n)])
        l = -1 # record the maximum s_len
        P = [] # record the path corresponding to maximum s_len
        while Q:
            node, mask, path, dis = Q.popleft()
            if dis < d[mask][node]: continue
            if mask == (1<<n) - 1 and dis > l:
                P,l = path,dis
                continue
            for i in xrange(n):
                nex_mask = mask | (1<<i)
                # case1: make sure that each node is only traversed once
                # case2: only if we can get larger save length, we consider it.
                if nex_mask != mask and d[mask][node] + G[node][i] >= d[nex_mask][i]:
                    d[nex_mask][i] = d[mask][node] + G[node][i]
                    Q.append((i, nex_mask, path+[i], d[nex_mask][i]))

        return pathtoStr(A,G,P)
```
</p>


