---
title: "Minimum Difficulty of a Job Schedule"
weight: 1249
#id: "minimum-difficulty-of-a-job-schedule"
---
## Description
<div class="description">
<p>You want to schedule a list of jobs in <code>d</code> days. Jobs are dependent (i.e To work on the <code>i-th</code> job, you have to finish all the jobs <code>j</code> where <code>0 &lt;= j &lt; i</code>).</p>

<p>You have to finish <strong>at least</strong> one task every day. The difficulty of a job schedule is the sum of difficulties of each day of the <code>d</code> days. The difficulty of a day is the maximum difficulty of a job done in that day.</p>

<p>Given an array of integers <code>jobDifficulty</code> and an integer <code>d</code>. The difficulty of the <code>i-th</code>&nbsp;job is&nbsp;<code>jobDifficulty[i]</code>.</p>

<p>Return <em>the minimum difficulty</em> of a job schedule. If you cannot find a schedule for the jobs return <strong>-1</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/01/16/untitled.png" style="width: 365px; height: 230px;" />
<pre>
<strong>Input:</strong> jobDifficulty = [6,5,4,3,2,1], d = 2
<strong>Output:</strong> 7
<strong>Explanation:</strong> First day you can finish the first 5 jobs, total difficulty = 6.
Second day you can finish the last job, total difficulty = 1.
The difficulty of the schedule = 6 + 1 = 7 
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> jobDifficulty = [9,9,9], d = 4
<strong>Output:</strong> -1
<strong>Explanation:</strong> If you finish a job per day you will still have a free day. you cannot find a schedule for the given jobs.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> jobDifficulty = [1,1,1], d = 3
<strong>Output:</strong> 3
<strong>Explanation:</strong> The schedule is one job per day. total difficulty will be 3.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> jobDifficulty = [7,1,7,1,7,1], d = 3
<strong>Output:</strong> 15
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> jobDifficulty = [11,111,22,222,33,333,44,444], d = 6
<strong>Output:</strong> 843
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= jobDifficulty.length &lt;= 300</code></li>
	<li><code>0 &lt;=&nbsp;jobDifficulty[i] &lt;= 1000</code></li>
	<li><code>1 &lt;= d &lt;= 10</code></li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 10 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Turvo - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python3] DP, O(nd) Solution
- Author: lee215
- Creation Date: Sun Jan 26 2020 12:06:10 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Feb 06 2020 23:23:33 GMT+0800 (Singapore Standard Time)

<p>
# Solution1: Top-down DP with cache
`dfs` help find the the minimum difficulty
if start work at `i`th job with `d` days left.

If `d = 1`, only one day left, we have to do all jobs,
return the maximum difficulty of jobs.

Time complexity `O(nnd)`
Space complexity `O(nd)`
<br>

**Python3:**
```python
import functools
class Solution:

    def minDifficulty(self, A, d):
        n = len(A)
        if n < d: return -1

        @functools.lru_cache(None)
        def dfs(i, d):
            if d == 1:
                return max(A[i:])
            res, maxd = float(\'inf\'), 0
            for j in range(i, n - d + 1):
                maxd = max(maxd, A[j])
                res = min(res, maxd + dfs(j + 1, d - 1))
            return res
        return dfs(0, d)
```
<br>

# Solution2: Bottom-up 2D DP
Time complexity `O(nnd)`
Space complexity `O(nd)`
<br>

**Python2**
```py
    def minDifficulty(self, A, d):
        n, inf = len(A), float(\'inf\')
        dp = [[inf] * n + [0] for i in xrange(d + 1)]
        for d in xrange(1, d + 1):
            for i in xrange(n - d + 1):
                maxd = 0
                for j in xrange(i, n - d + 1):
                    maxd = max(maxd, A[j])
                    dp[d][i] = min(dp[d][i], maxd + dp[d - 1][j + 1])
        return dp[d][0] if dp[d][0] < inf else -1
```
<br>

# Solution3: Bottom-up 1D DP
Time complexity `O(nnd)`
Space complexity `O(n)`
<br>

**Java**
```java
    public int minDifficulty(int[] A, int D) {
        int n = A.length, inf = Integer.MAX_VALUE, maxd;
        if (n < D) return -1;
        int[] dp = new int[n + 1];
        for (int i = n - 1; i >= 0; --i)
            dp[i] = Math.max(dp[i + 1], A[i]);
        for (int d = 2; d <= D; ++d) {
            for (int i = 0; i <= n - d; ++i) {
                maxd = 0;
                dp[i] = inf;
                for (int j = i; j <= n - d; ++j) {
                    maxd = Math.max(maxd, A[j]);
                    dp[i] = Math.min(dp[i], maxd + dp[j + 1]);
                }
            }
        }
        return dp[0];
    }
```
**C++**
```cpp
    int minDifficulty(vector<int>& A, int D) {
        int n = A.size(), inf = 1e9, maxd;
        if (n < D) return -1;
        vector<int> dp(n + 1, 1e9);
        dp[n] = 0;
        for (int d = 1; d <= D; ++d) {
            for (int i = 0; i <= n - d; ++i) {
                maxd = 0, dp[i] = inf;
                for (int j = i; j <= n - d; ++j) {
                    maxd = max(maxd, A[j]);
                    dp[i] = min(dp[i], maxd + dp[j + 1]);
                }
            }
        }
        return dp[0];
    }
```
**Python2**
```py
    def minDifficulty(self, A, d):
        n, inf = len(A), float(\'inf\')
        if n < d: return -1
        dp = [inf] * n + [0]
        for d in xrange(1, d + 1):
            for i in xrange(n - d + 1):
                maxd, dp[i] = 0, inf
                for j in xrange(i, n - d + 1):
                    maxd = max(maxd, A[j])
                    dp[i] = min(dp[i], maxd + dp[j + 1])
        return dp[0]
```
<br>

# Solution 4: Stack

**C++**
```cpp
    int minDifficulty(vector<int>& A, int D) {
        int n = A.size();
        if (n < d) return -1;
        vector<int> dp(n, 1000), dp2(n), stack;
        for (int d = 0; d < D; ++d) {
            stack.clear();
            for (int i = d; i < n; i++) {
                dp2[i] = i ? dp[i - 1] + A[i] : A[i];
                while (stack.size() && A[stack.back()] <= A[i]) {
                    int j = stack.back(); stack.pop_back();
                    dp2[i] = min(dp2[i], dp2[j] - A[j] + A[i])
                }
                if (stack.size()) {
                    dp2[i] = min(dp2[i], dp2[stack.back()]);
                }
                stack.push_back(i);
            }
            swap(dp, dp2);
        }
        return dp[n - 1];
    }
```

**Python**
```py
    def minDifficulty(self, A, d):
        n = len(A)
        dp, dp2 = [float(\'inf\')] * n, [0] * n
        if n < d: return -1
        for d in xrange(d):
            stack = []
            for i in xrange(d, n):
                dp2[i] = dp[i - 1] + A[i] if i else A[i]
                while stack and A[stack[-1]] <= A[i]:
                    j = stack.pop()
                    dp2[i] = min(dp2[i], dp2[j] - A[j] + A[i])
                if stack:
                    dp2[i] = min(dp2[i], dp2[stack[-1]])
                stack.append(i)
            dp, dp2 = dp2, [0] * n
        return dp[-1]
```

</p>


### [Java] Bottom-Up DP
- Author: nihalanim9
- Creation Date: Sun Jan 26 2020 12:03:41 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 27 2020 22:35:46 GMT+0800 (Singapore Standard Time)

<p>
Time Complexity(n<sup>2</sup>d)
Space Complexity(nd)
Where n is number of jobs
```
class Solution {
    public int minDifficulty(int[] jobDifficulty, int d) {
        
		int n = jobDifficulty.length; 
        if(n < d) return -1;
        int[][] dp = new int[d][n];
        
        dp[0][0] = jobDifficulty[0];
        for(int i = 1; i < n; i++){
            dp[0][i] = Math.max(jobDifficulty[i],dp[0][i-1]);
        }
        
        for(int i = 1; i < d; i++){
            for(int j = i; j < n; j++){
                int localMax = jobDifficulty[j];
                dp[i][j] = Integer.MAX_VALUE;
                for(int r = j; r >= i; r--){
                    localMax = Math.max(localMax,jobDifficulty[r]);
                    dp[i][j] =  Math.min(dp[i][j],dp[i-1][r-1] + localMax);
                }
            }
        }
        
        return dp[d-1][n-1];
    }
}
```

**Similar Logic:**
[Split Array Largest Sum](https://leetcode.com/problems/split-array-largest-sum/) (Binary Search Solution is also there)
[Pallindrome Partitioning III](https://leetcode.com/problems/palindrome-partitioning-iii/)
**Exact Question**
https://www.geeksforgeeks.org/divide-the-array-in-k-segments-such-that-the-sum-of-minimums-is-maximized/
</p>


### O(n*d) time and O(n) space solution
- Author: 617280219
- Creation Date: Thu Jan 30 2020 21:21:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 02 2020 14:47:14 GMT+0800 (Singapore Standard Time)

<p>
Firstly we have the basic O(nnd) DP solution:
f[i][j]=min{f[i-1][k]+max(val[k+1..j])},i-2<=k<j
where f[i][j] is the minimum difficulty for first j jobs done in i days, and val=jobDifficulty
Define t=l[j] to be the largest t such that t<j and val[t]>=val[j]
Then we know that for t<=k<j, max(val[k+1..j])=val[j]; and that for k<t, max(val[k+1..j])=max(val[k+1..t])
So for k<t case we have min{f[i-1][k]+max(val[k+1..j])}=f[i][t]
For t<=k<j case we have max(val[k+1..j])=val[j], so we only need to calculate min(f[i-1][t..j-1])
The DP equation becomes:
f[i][j]=min(f[i][l[j]],val[j]+min(f[i-1][l[j]..j-1]))
Here we notice that if a>b and val[a]>val[b] then for any c>a, l[c]!=b
Therefore for any l[a]<b<a<c, we have l[c]!=b, so we can suppress the interval l[a]..a-1 since they will always be included together
Finally we use a monotonic stack to maintain the array f[i-1][l[a]..a-1] where a={...,l[l[j]],l[j],j} is the representatives for the intervals
The time complexity is O(nd) since in each of the d rounds every j will be popped only once, and the space is O(n) because f[i][...] only depends on f[i-1][...]
```
class Solution:
    def minDifficulty(self, val: List[int], d: int) -> int:
        if len(val)<d:
            return -1
        n,f=len(val),val[:]
        for i in range(1,n):
            f[i]=max(f[i-1],f[i])
        for t in range(1,d):
            temp=[10**9]*t
            st=[(10**9,0,0)]
            for i in range(t,n):
                m,j=f[i-1],i-1
                while st[-1][0]<val[i]:
                    m,j=min(m,st[-1][1]),st[-1][2]
                    st.pop()
                if len(st)>1:
                    temp.append(min(temp[j],m+val[i]))
                else:
                    temp.append(m+val[i])
                st.append((val[i],m,j))
            f=temp
        return f[n-1]
```
</p>


