---
title: "Path Sum IV"
weight: 598
#id: "path-sum-iv"
---
## Description
<div class="description">
<p>If the depth of a tree is smaller than <code>5</code>, then this tree can be represented by a list of three-digits integers.</p>

<p>For each integer in this list:</p>

<ol>
	<li>The hundreds digit represents the depth <code>D</code> of this node, <code>1 &lt;= D &lt;= 4.</code></li>
	<li>The tens digit represents the position <code>P</code> of this node in the level it belongs to, <code>1 &lt;= P &lt;= 8</code>. The position is the same as that in a full binary tree.</li>
	<li>The units digit represents the value <code>V</code> of this node, <code>0 &lt;= V &lt;= 9.</code></li>
</ol>

<p>Given a list of <code>ascending</code> three-digits integers representing a binary tree with the depth smaller than 5, you need to return the sum of all paths from the root towards the leaves.</p>

<p>It&#39;s guaranteed that the given list represents a valid connected binary tree.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> [113, 215, 221]
<b>Output:</b> 12
<b>Explanation:</b> 
The tree that the list represents is:
    3
   / \
  5   1

The path sum is (3 + 5) + (3 + 1) = 12.
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> [113, 221]
<b>Output:</b> 4
<b>Explanation:</b> 
The tree that the list represents is: 
    3
     \
      1

The path sum is (3 + 1) = 4.
</pre>

</div>

## Tags
- Tree (tree)

## Companies
- Alibaba - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Convert to Tree [Accepted]

**Intuition**

Convert the given array into a tree using Node objects.  Afterwards, for each path from root to leaf, we can add the sum of that path to our answer.

**Algorithm**

There are two steps, the tree construction, and the traversal.

In the tree construction, we have some depth, position, and value, and we want to know where the new node goes.  With some effort, we can see the relevant condition for whether a node should be left or right is `pos - 1 < 2**(depth - 2)`.  For example, when `depth = 4`, the positions are `1, 2, 3, 4, 5, 6, 7, 8`, and it's left when `pos <= 4`.

In the traversal, we perform a depth-first search from root to leaf, keeping track of the current sum along the path we have travelled.  Every time we reach a leaf `(node.left == null && node.right == null)`, we have to add that running sum to our answer.

<iframe src="https://leetcode.com/playground/ne38nVFY/shared" frameBorder="0" width="100%" height="500" name="ne38nVFY"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ where $$N$$ is the length of `nums`.  We construct the graph and traverse it in this time.

* Space Complexity: $$O(N)$$, the size of the implicit call stack in our depth-first search.

---
#### Approach #2: Direct Traversal [Accepted]

**Intuition and Algorithm**

As in *Approach #1*, we will depth-first search on the tree.  One time-saving idea is that we can use `num / 10 = 10 * depth + pos` as a unique identifier for that node.  The left child of such a node would have identifier `10 * (depth + 1) + 2 * pos - 1`, and the right child would be one greater.

<iframe src="https://leetcode.com/playground/pamheDYq/shared" frameBorder="0" width="100%" height="500" name="pamheDYq"></iframe>

**Complexity Analysis**

* Time and Space Complexity: $$O(N)$$.  The analysis is the same as in *Approach #1*.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution, Represent tree using HashMap
- Author: shawngao
- Creation Date: Sun Aug 27 2017 11:01:28 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 05:22:06 GMT+0800 (Singapore Standard Time)

<p>
How do we solve problem like this if we were given a normal tree? Yes, traverse it, keep a root to leaf running sum. If we see a leaf node (node.left == null && node.right == null), we add the running sum to the final result.

Now each tree node is represented by a number. 1st digits is the ```level```, 2nd is the ```position``` in that ```level``` (note that it starts from ```1``` instead of ```0```). 3rd digit is the value. We need to find a way to traverse this ```tree``` and get the sum.

The idea is, we can form a ```tree``` using a HashMap. The ```key``` is first two digits which marks the position of a node in the tree. The ```value``` is value of that node. Thus, we can easily find a node's left and right children using math. 
Formula: For node ```xy?``` its left child is ```(x+1)(y*2-1)?``` and right child is ```(x+1)(y*2)?```

Given above HashMap and formula, we can traverse the ```tree```. Problem is solved!
```
class Solution {
    int sum = 0;
    Map<Integer, Integer> tree = new HashMap<>();
    
    public int pathSum(int[] nums) {
        if (nums == null || nums.length == 0) return 0;
        
        for (int num : nums) {
            int key = num / 10;
            int value = num % 10;
            tree.put(key, value);
        }
        
        traverse(nums[0] / 10, 0);
        
        return sum;
    }
    
    private void traverse(int root, int preSum) {
        int level = root / 10;
        int pos = root % 10;
        int left = (level + 1) * 10 + pos * 2 - 1;
        int right = (level + 1) * 10 + pos * 2;
        
        int curSum = preSum + tree.get(root);
        
        if (!tree.containsKey(left) && !tree.containsKey(right)) {
            sum += curSum;
            return;
        }
        
        if (tree.containsKey(left)) traverse(left, curSum);
        if (tree.containsKey(right)) traverse(right, curSum);
    }
}
```
</p>


### [C++] [Java] Clean Code
- Author: alexander
- Creation Date: Sun Aug 27 2017 11:04:17 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 18 2018 11:01:40 GMT+0800 (Singapore Standard Time)

<p>
```
         0
     0       1
  0   1     2   3
0 1  2 3   4 5  6 7
```
Regardless whether these nodes exist:

the position of left child is always `parent_pos * 2`;
the position of right child is always` parent_pos * 2 + 1`;
the position of parent is always `child_pos / 2`;

**Solution C++ Array**
```
class Solution {
public:
    int pathSum(vector<int>& nums) {
        int m[5][8] = {};
        for (int n : nums) {
            int i = n / 100; // i is 1 based index;
            int j = (n % 100) / 10 - 1; // j used 0 based index;
            int v = n % 10;
            m[i][j] = m[i - 1][j / 2] + v;
        }

        int sum = 0;
        for (int i = 1; i < 5; i++) {
            for (int j = 0; j < 8; j++) {
                if (i == 4 || m[i][j] && !m[i + 1][j * 2] && !m[i + 1][j * 2 + 1]){
                    sum += m[i][j];
                }
            }
        }
        return sum;
    }
};
```
**Solution C++ map**
If we use map, we don't need to do the boundary check at little extra cost of memory.
```
class Solution {
public:
    int pathSum(vector<int>& nums) {
        map<int, map<int, int>> m;
        for (int n : nums) {
            int i = n / 100 - 1; // i is 0 based index;
            int j = (n % 100) / 10 - 1; // j used 0 based index;
            int v = n % 10;
            m[i][j] = m[i - 1][j / 2] + v;
        }

        int sum = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 8; j++) {
                sum += m[i][j] && !m[i + 1][j * 2] && !m[i + 1][j * 2 + 1] ? m[i][j] : 0;
            }
        }
        return sum;
    }
};
```
**Solution C++ - queue**
```
class Solution {
public:
    int pathSum(vector<int>& nums) {
        if (nums.empty()) return 0;
        int sum = 0;
        queue<info> q;
        info dummy(0);
        info* p = &dummy; // parent start with dummy info, root have no real parent;
        for (int n : nums) {
            info c(n); // child;
            while (!p->isparent(c) && !q.empty()) {
                sum += p->leaf ? p->v : 0;
                p = &q.front();
                q.pop();
            }
            p->leaf = false;
            c.v += p->v;
            q.push(c);
        }
        while (!q.empty()) {
            sum += q.front().v;
            q.pop();
        }
        return sum;
    }
private:
    struct info {
        int i, j, v;
        bool leaf;
        info(int n) : i(n / 100 - 1), j((n % 100) / 10 - 1), v(n % 10), leaf(true) {};
        bool isparent(info other) { return i == other.i - 1 && j == other.j / 2;};
    };
};
```
**Solution Java**
```
class Solution {
    public int pathSum(int[] nums) {
        int[][] m = new int[5][8];
        for (int n : nums) {
            int i = n / 100; // i is 1 based index;
            int j = (n % 100) / 10 - 1; // j used 0 based index;
            int v = n % 10;
            m[i][j] = m[i - 1][j / 2] + v;
        }

        int sum = 0;
        for (int i = 1; i < 5; i++) {
            for (int j = 0; j < 8; j++) {
                if (i == 4 || m[i][j] != 0 && m[i + 1][j * 2] == 0 && m[i + 1][j * 2 + 1] == 0){
                    sum += m[i][j];
                }
            }
        }
        return sum;        
    }
}
```
</p>


### Python Straight Forward Solution
- Author: lee215
- Creation Date: Sun Aug 27 2017 11:25:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 07:23:11 GMT+0800 (Singapore Standard Time)

<p>
v is path sum, nc is the number of leaves
````
def pathSum(self, nums):
        s = {}
        l = {}
        for i in nums[::-1]:
            a, b, c = i / 100, i / 10 % 10, i % 10
            l[a, b] = max(1, l.get((a + 1, b * 2 - 1), 0) + l.get((a + 1, b * 2), 0))
            s[a, b] = s.get((a + 1, b * 2 - 1), 0) + s.get((a + 1, b * 2), 0) + l[a, b] * c
        return s.get((1, 1), 0)
</p>


