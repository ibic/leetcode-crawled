---
title: "Tournament Winners"
weight: 1546
#id: "tournament-winners"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Players</code></p>

<pre>
+-------------+-------+
| Column Name | Type  |
+-------------+-------+
| player_id   | int   |
| group_id    | int   |
+-------------+-------+
player_id is the primary key of this table.
Each row of this table indicates the group of each player.
</pre>

<p>Table:&nbsp;<code>Matches</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| match_id      | int     |
| first_player  | int     |
| second_player | int     | 
| first_score   | int     |
| second_score  | int     |
+---------------+---------+
match_id is the primary key of this table.
Each row is a record of a match, first_player and second_player contain the player_id of each match.
first_score and second_score contain the number of points of the first_player and second_player respectively.
You may assume that, in each match, players belongs to the same group.
</pre>

<p>&nbsp;</p>

<p>The winner in each group is the player who scored the maximum total points within the group.&nbsp;In the case of a tie, the <strong>lowest</strong> player_id&nbsp;wins.</p>

<p>Write an SQL query to find the winner in each group.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Players </code>table:
+-----------+------------+
| player_id | group_id   |
+-----------+------------+
| 15        | 1          |
| 25        | 1          |
| 30        | 1          |
| 45        | 1          |
| 10        | 2          |
| 35        | 2          |
| 50        | 2          |
| 20        | 3          |
| 40        | 3          |
+-----------+------------+

<code>Matches </code>table:
+------------+--------------+---------------+-------------+--------------+
| match_id   | first_player | second_player | first_score | second_score |
+------------+--------------+---------------+-------------+--------------+
| 1          | 15           | 45            | 3           | 0            |
| 2          | 30           | 25            | 1           | 2            |
| 3          | 30           | 15            | 2           | 0            |
| 4          | 40           | 20            | 5           | 2            |
| 5          | 35           | 50            | 1           | 1            |
+------------+--------------+---------------+-------------+--------------+

Result table:
+-----------+------------+
| group_id  | player_id  |
+-----------+------------+ 
| 1         | 15         |
| 2         | 35         |
| 3         | 40         |
+-----------+------------+
</pre>

</div>

## Tags


## Companies
- Wayfair - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A MySQL Solution (Explained)
- Author: rye_raptor
- Creation Date: Wed Sep 18 2019 00:42:02 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 19 2019 17:51:52 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1:**
This solution works on most databases (i.e. Oracle, PostgreSQL, MySQL etc.) and on the leetcode versions.

First, we can retrieve a set of all the players along with their scores per match using "union all" i.e. first player and their scores (first score) along with second players and their scores (second score):
```
select first_player as player, first_score as score from Matches
union all
select second_player, second_score from Matches
```
From this set, we can then add the scores for each player in all matches:
```
select player, sum(score) as score from
	(select first_player as player, first_score as score from Matches
	union all
	select second_player, second_score from Matches) s
group by player
```
We can refer to this as the players\' scores. To get the group of each player we can simple join this "players\' scores" set to Players set based on the player_id.

However, what we really need is the maximum score in each group, therefore we can use an aggregate function here along with the join:
```
select group_id, max(score)
from Players,
	(select player, sum(score) as score from
		(select first_player as player, first_score as score from Matches
		union all
		select second_player, second_score from Matches) s
	group by player) PlayerScores
where Players.player_id = PlayerScores.player
group by group_id
```
Now that we have the maximum score in each group, it is simply a matter of retrieving all players in the "players\' score" set with that score
```
select group_id, player_id
from Players,
    (select player, sum(score) as score from
        (select first_player as player, first_score as score from Matches
        union all
        select second_player, second_score from Matches) s
    group by player) PlayerScores
    where Players.player_id = PlayerScores.player
        and (group_id, score) in                     # check against maximum score in each group
            (select group_id, max(score)
            from Players,
                (select player, sum(score) as score from
                    (select first_player as player, first_score as score from Matches
                    union all
                    select second_player, second_score from Matches) s
                group by player) PlayerScores
            where Players.player_id = PlayerScores.player
            group by group_id)
```

**Final code for Solution 1:**
Finally, we take care of players with the same score by retrieving the player with the id:
```
select group_id as GROUP_ID, min(player_id) as PLAYER_ID
from Players,
    (select player, sum(score) as score from
        (select first_player as player, first_score as score from Matches
        union all
        select second_player, second_score from Matches) s
    group by player) PlayerScores
where Players.player_id = PlayerScores.player and (group_id, score) in
	(select group_id, max(score)
	from Players,
		(select player, sum(score) as score from
			(select first_player as player, first_score as score from Matches
			union all
			select second_player, second_score from Matches) s
		group by player) PlayerScores
	where Players.player_id = PlayerScores.player
	group by group_id)
group by group_id
```


**Solution 2:**
In Solution 1, the PlayerScores set had to be created twice (first to get the scores and a second time to compare the scores with the maximum score in each group). One way to remove the repeating code segment would be to use Common Table Expression (however, I do not think this can be done in leetcode). Another way might be to use variables. However, we can also leverage a trick based on how MySQL handles aggregates when **sql_mode=only_full_group_by** is disabled (which is the case in leetcode). MySQL simple picks the first item in a row when a column is in the **SELECT** clause but not the **GROUP BY** clause. Other databases would throw an error. Therefore we can simply get the players\' scores (ps) like we did in Solution 1 along with the group_id of each player by joining the subquery with the Players table. Then order them to make sure the first item of each group is what we are looking for:
```
select p.group_id, ps.player_id, sum(ps.score) as score from Players p,
	(select first_player as player_id, first_score as score from Matches
	union all
	select second_player, second_score from Matches) ps
where p.player_id = ps.player_id
group by ps.player_id order by group_id, score desc, player_id
```
**Final code for Solution 2:**
Aggregating this over the group_id will then give us our final result:
```
select group_id, player_id from (
	select p.group_id, ps.player_id, sum(ps.score) as score from Players p,
	    (select first_player as player_id, first_score as score from Matches
	    union all
	    select second_player, second_score from Matches) ps
	where p.player_id = ps.player_id
	group by ps.player_id order by group_id, score desc, player_id) top_scores
group by group_id
```

</p>


### Clear solution using union all and window function
- Author: ruanang
- Creation Date: Fri Oct 04 2019 08:34:08 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 04 2019 08:34:08 GMT+0800 (Singapore Standard Time)

<p>
```
select group_id, player_id from 
(
    select p.player_id, p.group_id, 
    row_number() over(partition by p.group_id order by player_total.score desc, p.player_id asc) rank
    from players p join
        (
            select player_id, sum(player_score.score) as score
            from (
                    (select first_player as player_id, first_score as score from Matches)
                        union all
                    (select second_player as player_id, second_score as score from Matches)
                 ) player_score 
            group by player_id
        ) player_total
    on p.player_id = player_total.player_id
) rank_table 
where rank_table.rank = 1
```
</p>


### MS SQL
- Author: hij1356
- Creation Date: Thu Oct 03 2019 11:38:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 03 2019 11:38:28 GMT+0800 (Singapore Standard Time)

<p>
```
with cte as(
select player, sum(score) as ind_score
from
(select first_player as player, first_score as score
from Matches m1
union all
select second_player as player, second_score as score
from Matches m2) m
group by player),

cte2 as(
select group_id, player, row_number() over(partition by group_id order by ind_score desc, player) as rn
from players left join cte on
players.player_id=cte.player)
   
select group_id, player as player_id
from cte2
where rn=1
```
</p>


