---
title: "Meeting Scheduler"
weight: 1063
#id: "meeting-scheduler"
---
## Description
<div class="description">
<p>Given the availability time slots arrays&nbsp;<code>slots1</code> and <code>slots2</code>&nbsp;of two people and a meeting duration <code>duration</code>, return the <strong>earliest time slot</strong> that works for both of them and is of duration <code>duration</code>.</p>

<p>If there is no common time slot that satisfies the requirements, return an <strong>empty array</strong>.</p>

<p>The format of a time slot is an array of two elements&nbsp;<code>[start, end]</code>&nbsp;representing an inclusive time range from <code>start</code>&nbsp;to <code>end</code>.&nbsp;&nbsp;</p>

<p>It is guaranteed that no two availability slots of the same person intersect with each other. That is, for any two time slots&nbsp;<code>[start1, end1]</code>&nbsp;and&nbsp;<code>[start2, end2]</code>&nbsp;of the same person, either&nbsp;<code>start1 &gt; end2</code>&nbsp;or&nbsp;<code>start2 &gt; end1</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> slots1 = [[10,50],[60,120],[140,210]], slots2 = [[0,15],[60,70]], duration = 8
<strong>Output:</strong> [60,68]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> slots1 = [[10,50],[60,120],[140,210]], slots2 = [[0,15],[60,70]], duration = 12
<strong>Output:</strong> []
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= slots1.length, slots2.length &lt;= 10^4</code></li>
	<li><code>slots1[i].length, slots2[i].length == 2</code></li>
	<li><code>slots1[i][0] &lt; slots1[i][1]</code></li>
	<li><code>slots2[i][0] &lt; slots2[i][1]</code></li>
	<li><code>0 &lt;= slots1[i][j], slots2[i][j] &lt;= 10^9</code></li>
	<li><code>1 &lt;= duration &lt;= 10^6&nbsp;</code></li>
</ul>

</div>

## Tags
- Line Sweep (line-sweep)

## Companies
- Apple - 6 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- DoorDash - 7 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Paypal - 0 (taggedByAdmin: true)
- pramp - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] Two Pointer - Clean code - O(NlogN) for Sorting Beat 100%
- Author: hiepit
- Creation Date: Sun Oct 20 2019 00:05:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 01:35:55 GMT+0800 (Singapore Standard Time)

<p>
**Java**
```java
class Solution {
    public List<Integer> minAvailableDuration(int[][] slots1, int[][] slots2, int duration) {
        Arrays.sort(slots1, (a,b)->a[0]-b[0]); // sort increasing by start time
        Arrays.sort(slots2, (a,b)->a[0]-b[0]); // sort increasing by start time

        int i = 0, j = 0;
        int n1 = slots1.length, n2 = slots2.length;
        while (i < n1 && j < n2) {
            // Find intersect between slots1[i] and slots2[j]
            int intersectStart = Math.max(slots1[i][0], slots2[j][0]);
            int intersectEnd = Math.min(slots1[i][1], slots2[j][1]);

            if (intersectStart + duration <= intersectEnd) // Found the result
                return Arrays.asList(intersectStart, intersectStart + duration);
            else if (slots1[i][1] < slots2[j][1])
                i++;
            else
                j++;
        }
        return new ArrayList<>();
    }
}
```
**C++**
```c++
class Solution {
public:
    vector<int> minAvailableDuration(vector<vector<int>>& slots1, vector<vector<int>>& slots2, int duration) {
        sort(slots1.begin(), slots1.end()); // sort increasing by start time (default sorting by first value)
        sort(slots2.begin(), slots2.end()); // sort increasing by start time (default sorting by first value)
        
        int i = 0, j = 0;
        int n1 = slots1.size(), n2 = slots2.size();
        while (i < n1 && j < n2) {
            // Find intersect between slots1[i] and slots2[j]
            int intersectStart = max(slots1[i][0], slots2[j][0]);
            int intersectEnd = min(slots1[i][1], slots2[j][1]);

            if (intersectStart + duration <= intersectEnd) // Found the result
                return {intersectStart, intersectStart + duration};
            else if (slots1[i][1] < slots2[j][1])
                i++;
            else
                j++;
        }
        return {};
    }
};
```
**Complexity:**
- Time: O(NlogN) for Sorting, Two pointer only take O(M+N)
- Space: O(1)
</p>


### [Java/Python 3] simple code using PriorityQueue/heapq w/ brief explanation and analysis
- Author: rock
- Creation Date: Sun Oct 20 2019 00:10:26 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 24 2019 14:55:17 GMT+0800 (Singapore Standard Time)

<p>
1. Put both `slots1` and `slots2` into PriorityQueue/heapq (first filter slots shorter than `duration`, credit to **@SunnyvaleCA**), sort by starting time;
2. Pop out the slots one by one, comparing every consective two to check if having `duration` time in common.
```
    public List<Integer> minAvailableDuration(int[][] slots1, int[][] slots2, int duration) {
        PriorityQueue<int[]> pq = new PriorityQueue<>(Comparator.comparing(a -> a[0]));
        for (int[] s : slots1) 
            if (s[1] - s[0] >= duration) 
                pq.offer(s);
        for (int[] s : slots2) 
            if (s[1] - s[0] >= duration) 
                pq.offer(s);
        while (pq.size() > 1)
            if (pq.poll()[1] >= pq.peek()[0] + duration)
                return Arrays.asList(pq.peek()[0], pq.peek()[0] + duration);
        return Arrays.asList();
    }
```
```
    def minAvailableDuration(self, slots1: List[List[int]], slots2: List[List[int]], duration: int) -> List[int]:
        s = list(filter(lambda slot: slot[1] - slot[0] >= duration, slots1 + slots2))
        heapq.heapify(s)
        while len(s) > 1:
            if heapq.heappop(s)[1] >= s[0][0] + duration:
                return [s[0][0], s[0][0] + duration] 
        return []   
```

**Analysis**

Time: O(nlog(n)), space: O(n), where n = slots1.length + slots2.length.
</p>


### Python solution based on 986. Interval List Intersections
- Author: Yumi_C
- Creation Date: Sun Oct 20 2019 05:56:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 05:56:08 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def minAvailableDuration(self, slots1, slots2, duration):

        slots1.sort(key = lambda x: x[0])
        slots2.sort(key = lambda x: x[0])
        i = 0
        j = 0
        
        while i < len(slots1) and j < len(slots2):
            head = max(slots1[i][0], slots2[j][0])
            tail = min(slots1[i][1], slots2[j][1])
            if head + duration <= tail:
                return [head, head+duration]
            
            if slots1[i][1] < slots2[j][1]:
                i += 1
            else:
                j += 1
        
        return []
```
</p>


