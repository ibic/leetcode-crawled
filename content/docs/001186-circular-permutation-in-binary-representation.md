---
title: "Circular Permutation in Binary Representation"
weight: 1186
#id: "circular-permutation-in-binary-representation"
---
## Description
<div class="description">
<p>Given 2 integers <code>n</code> and <code>start</code>. Your task is return <strong>any</strong> permutation <code>p</code>&nbsp;of <code>(0,1,2.....,2^n -1) </code>such that :</p>

<ul>
	<li><code>p[0] = start</code></li>
	<li><code>p[i]</code> and <code>p[i+1]</code>&nbsp;differ by only one bit in their binary representation.</li>
	<li><code>p[0]</code> and <code>p[2^n -1]</code>&nbsp;must also differ by only one bit in their binary representation.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 2, start = 3
<strong>Output:</strong> [3,2,0,1]
<strong>Explanation:</strong> The binary representation of the permutation is (11,10,00,01). 
All the adjacent element differ by one bit. Another valid permutation is [3,1,0,2]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 3, start = 2
<strong>Output:</strong> [2,6,7,5,4,0,1,3]
<strong>Explanation:</strong> The binary representation of the permutation is (010,110,111,101,100,000,001,011).
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 16</code></li>
	<li><code>0 &lt;= start&nbsp;&lt;&nbsp;2 ^ n</code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Walmart - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] 4-line Gray Code
- Author: lee215
- Creation Date: Sun Oct 27 2019 12:17:38 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 03 2019 21:57:16 GMT+0800 (Singapore Standard Time)

<p>
Find more explanation here about the used [formula](https://cp-algorithms.com/algebra/gray-code.html)
Time `O(2^N)` Space `O(2^N)`

**Java:**
```java
    public List<Integer> circularPermutation(int n, int start) {
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < 1 << n; ++i)
            res.add(start ^ i ^ i >> 1);
        return res;
    }
```

**C++:**
```cpp
    vector<int> circularPermutation(int n, int start) {
        vector<int> res;
        for (int i = 0; i < 1 << n; ++i)
            res.push_back(start ^ i ^ i >> 1);
        return res;
    }
```

**1-line Python:**
```python
    def circularPermutation(self, n, start):
        return [start ^ i ^ i >> 1 for i in range(1 << n)]
```

</p>


### Java AC solution: generate "one bit diff" list, then make it start from "start"
- Author: jeremyasm
- Creation Date: Sun Oct 27 2019 12:02:12 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 28 2019 11:13:45 GMT+0800 (Singapore Standard Time)

<p>
1. Generate one bit diff permutation list.
2. Rotate the list to make it start from "start".

Note: If you exercised 89. Gray Code (https://leetcode.com/problems/gray-code/) before, you can easily come up with the way to generate one bit diff permutation list.
And here\'s a good explanation to help you understand: https://leetcode.com/problems/gray-code/discuss/29881/An-accepted-three-line-solution-in-JAVA/172544

```
    public List<Integer> circularPermutation(int n, int start) {
        List<Integer> res = new ArrayList<>();
        List<Integer> tmp = oneBitDiffPermutation(n); // generate one bit diff permutations
        
        // rotate the tmp list to make it starts from "start"
        // before: {0, 1, ..., start, ..., end}
        // after:  {start, ..., end, 0, 1, ...}
        int i = 0;
        for( ; i < tmp.size(); i ++){
            if(tmp.get(i) == start){
                break;
            }
        }
        
        for(int k = i; k < tmp.size(); k ++){
            res.add(tmp.get(k));
        }
        
        for(int q = 0; q < i; q ++){
            res.add(tmp.get(q));
        }
        
        return res;
    }
    
    // generate one bit diff permutations
    // 0, 1
    // 0, 1, 11, 10 
    // 000, 001, 011, 010, 110, 111, 101, 100 
    public List<Integer> oneBitDiffPermutation(int n){
        List<Integer> tmp = new ArrayList<>();
        tmp.add(0);
        if(n == 0){
            return tmp;
        }
        
        for(int i = 0; i < n; i ++){
            for(int j = tmp.size() - 1; j >= 0; j --){
                tmp.add(tmp.get(j) + (1 << i));
            }
        }
        
        return tmp;
    }
```


Details about ***oneBitDiffPermutation(int n)***

* n = 0 : {0}
* n = 1: {0, **1**}
* n = 2: {00, 01, **11, 10** }
* n = 3: {000, 001, 011, 010, **110, 111, 101, 100** } 

Let\'s say n = i (i > 0), how to generate the list of "n = i" from the list of "n = i - 1" ?

Take n = 3 for example, the first half in the list is the same as the elements in list of n = 2, right ?
* n = 2: {00, 01, 11, 10 }
* n = 3: {000, 001, 011, 010, ...}    // the first half of list is same as the list for n = 2

Then, what about the second half ? how to generate the second half from the first half ?
* n = 3: {000, 001, 011, 010, **110, 111, 101, 100** } 

The elements in the first half all begin with **\'0\'**.
The elements in the second half all begin with **\'1\'**, 
**Ignore the \'0\'/\'1\', then the two parts have a mirror symmetry.**

* n = 3: {00, 01, 11, 10, **10, 11, 01, 00** }  // for each element, ignore the first bit \'0\'/\'1\'

Then add \'0\' to each element in first half, add \'1\' to each element in second half.
* n = 3: {000, 001, 011, 010, **110, 111, 101, 100** } 

</p>


### Java DFS + Bit
- Author: wushangzhen
- Creation Date: Sun Oct 27 2019 12:01:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 27 2019 12:13:27 GMT+0800 (Singapore Standard Time)

<p>
1. add start
2. dfs backtracking: use set to avoid duplicates and find last number\'s neighbors. Pay attention to the return condition(check with the first number)

```
class Solution {
    public List<Integer> circularPermutation(int n, int start) {
        List<Integer> res = new ArrayList<>();
        Set<Integer> set = new HashSet<>();
        res.add(start);
        set.add(start);
        dfs(n, set, res, start);
        return res;
    }
    boolean dfs(int n, Set<Integer> set, List<Integer> res, int start) {
        if (set.size() == (int)Math.pow(2, n)) {
            int x = res.get(res.size() - 1) ^ start;
            return (x & (x - 1)) == 0;
        }
        int last = res.get(res.size() - 1);
        for (int i = 0; i < 16; i++) {
            int next = (last ^ (1 << i));
            if (next <= Math.pow(2, n) - 1 && !set.contains(next)) {
                set.add(next);
                res.add(next);
                if (dfs(n, set, res, start)) {
                    return true;
                }
                set.remove(next);
                res.remove(res.size() - 1);
            }
        }
        return false;
    }
}
```
</p>


