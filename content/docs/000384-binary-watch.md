---
title: "Binary Watch"
weight: 384
#id: "binary-watch"
---
## Description
<div class="description">
<p>A binary watch has 4 LEDs on the top which represent the <b>hours</b> (<b>0-11</b>), and the 6 LEDs on the bottom represent the <b>minutes</b> (<b>0-59</b>).</p>
<p>Each LED represents a zero or one, with the least significant bit on the right.</p>
<img src="https://upload.wikimedia.org/wikipedia/commons/8/8b/Binary_clock_samui_moon.jpg" height="300" />
<p>For example, the above binary watch reads "3:25".</p>

<p>Given a non-negative integer <i>n</i> which represents the number of LEDs that are currently on, return all possible times the watch could represent.</p>

<p><b>Example:</b>
<pre>Input: n = 1<br>Return: ["1:00", "2:00", "4:00", "8:00", "0:01", "0:02", "0:04", "0:08", "0:16", "0:32"]</pre>
</p>

<p><b>Note:</b><br />
<ul>
<li>The order of output does not matter.</li>
<li>The hour must not contain a leading zero, for example "01:00" is not valid, it should be "1:00".</li>
<li>The minute must be consist of two digits and may contain a leading zero, for example "10:2" is not valid, it should be "10:02".</li>
</ul>
</p>
</div>

## Tags
- Backtracking (backtracking)
- Bit Manipulation (bit-manipulation)

## Companies
- Apple - 3 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Python+Java
- Author: StefanPochmann
- Creation Date: Sun Sep 18 2016 17:32:48 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 09:06:25 GMT+0800 (Singapore Standard Time)

<p>
Just go through the possible times and collect those with the correct number of one-bits.

Python:

    def readBinaryWatch(self, num):
        return ['%d:%02d' % (h, m)
                for h in range(12) for m in range(60)
                if (bin(h) + bin(m)).count('1') == num]

Java:

    public List<String> readBinaryWatch(int num) {
        List<String> times = new ArrayList<>();
        for (int h=0; h<12; h++)
            for (int m=0; m<60; m++)
                if (Integer.bitCount(h * 64 + m) == num)
                    times.add(String.format("%d:%02d", h, m));
        return times;        
    }
</p>


### Straight-forward 6-line c++ solution, no need to explain
- Author: mzchen
- Creation Date: Sun Sep 18 2016 22:08:18 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 02:02:34 GMT+0800 (Singapore Standard Time)

<p>
```
vector<string> readBinaryWatch(int num) {
    vector<string> rs;
    for (int h = 0; h < 12; h++)
    for (int m = 0; m < 60; m++)
        if (bitset<10>(h << 6 | m).count() == num)
            rs.emplace_back(to_string(h) + (m < 10 ? ":0" : ":") + to_string(m));
    return rs;
}
```
</p>


### 3ms Java Solution Using Backtracking and Idea of "Permutation and Combination"
- Author: xietao0221
- Creation Date: Mon Sep 19 2016 07:46:14 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 01:39:33 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public List<String> readBinaryWatch(int num) {
        List<String> res = new ArrayList<>();
        int[] nums1 = new int[]{8, 4, 2, 1}, nums2 = new int[]{32, 16, 8, 4, 2, 1};
        for(int i = 0; i <= num; i++) {
            List<Integer> list1 = generateDigit(nums1, i);
            List<Integer> list2 = generateDigit(nums2, num - i);
            for(int num1: list1) {
                if(num1 >= 12) continue;
                for(int num2: list2) {
                    if(num2 >= 60) continue;
                    res.add(num1 + ":" + (num2 < 10 ? "0" + num2 : num2));
                }
            }
        }
        return res;
    }

    private List<Integer> generateDigit(int[] nums, int count) {
        List<Integer> res = new ArrayList<>();
        generateDigitHelper(nums, count, 0, 0, res);
        return res;
    }

    private void generateDigitHelper(int[] nums, int count, int pos, int sum, List<Integer> res) {
        if(count == 0) {
            res.add(sum);
            return;
        }
        
        for(int i = pos; i < nums.length; i++) {
            generateDigitHelper(nums, count - 1, i + 1, sum + nums[i], res);    
        }
    }
}
```
</p>


