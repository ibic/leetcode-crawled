---
title: "Create a Session Bar Chart"
weight: 1577
#id: "create-a-session-bar-chart"
---
## Description
<div class="description">
<p>Table: <code>Sessions</code></p>

<pre>
+---------------------+---------+
| Column Name         | Type    |
+---------------------+---------+
| session_id          | int     |
| duration            | int     |
+---------------------+---------+
session_id is the primary key for this table.
duration is the time in seconds that a user has visited the application.
</pre>

<p>&nbsp;</p>

<p>You want to know how long a user visits your application. You decided to create bins of &quot;[0-5&gt;&quot;, &quot;[5-10&gt;&quot;, &quot;[10-15&gt;&quot; and &quot;15 minutes or more&quot; and count the number of sessions on it.</p>

<p>Write an SQL query to report the (bin, total) in <strong>any</strong> order.</p>

<p>The query result format is in the following example.</p>

<pre>
Sessions table:
+-------------+---------------+
| session_id  | duration      |
+-------------+---------------+
| 1           | 30            |
| 2           | 199           |
| 3           | 299           |
| 4           | 580           |
| 5           | 1000          |
+-------------+---------------+

Result table:
+--------------+--------------+
| bin          | total        |
+--------------+--------------+
| [0-5&gt;        | 3            |
| [5-10&gt;       | 1            |
| [10-15&gt;      | 0            |
| 15 or more   | 1            |
+--------------+--------------+

For session_id 1, 2 and 3 have a duration greater or equal than 0 minutes and less than 5 minutes.
For session_id 4 has a duration greater or equal than 5 minutes and less than 10 minutes.
There are no session with a duration greater or equial than 10 minutes and less than 15 minutes.
For session_id 5 has a duration greater or equal than 15 minutes.
</pre>

</div>

## Tags


## Companies
- Twitch - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### two solutions, both beat 100%
- Author: CodeSunny
- Creation Date: Sun May 03 2020 22:29:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 22:29:21 GMT+0800 (Singapore Standard Time)

<p>
### method 1: 
First, create a \'frame\' with `UNION ALL`, then `JOIN` the aggregated results to the frame, so that even if there is no record for some categories, we can still use `IFNULL` to fill with 0. 
```
SELECT b.bin, IFNULL(COUNT(s.session_id),0) AS total
FROM 
    (SELECT session_id, 
           CASE WHEN duration/60<5 THEN \'[0-5>\'
                WHEN duration/60>=5 AND duration/60<10 THEN \'[5-10>\'
                WHEN duration/60>=10 AND duration/60<15 THEN \'[10-15>\'
                ELSE \'15 or more\' END AS bin
    FROM sessions) s
RIGHT JOIN 
    (SELECT \'[0-5>\' AS bin
     UNION ALL
     SELECT \'[5-10>\' AS bin
     UNION ALL
     SELECT \'[10-15>\' AS bin
     UNION ALL
     SELECT \'15 or more\' AS bin) b
        ON b.bin=s.bin
GROUP BY bin
```

### method 2: 
Aggregate first, then `UNION ALL`

```
WITH cte AS (
    SELECT duration/60 AS mins
    FROM sessions
)

SELECT \'[0-5>\' AS bin,
       SUM(CASE WHEN mins<5 THEN 1 ELSE 0 END) AS total
FROM cte
UNION ALL
SELECT \'[5-10>\' AS bin,
       SUM(CASE WHEN mins>=5 AND mins<10 THEN 1 ELSE 0 END) AS total
FROM cte
UNION ALL
SELECT \'[10-15>\' AS bin,
       SUM(CASE WHEN mins>=10 AND mins<15 THEN 1 ELSE 0 END) AS total
FROM cte
UNION ALL
SELECT \'15 or more\' AS bin,
       SUM(CASE WHEN mins>=15 THEN 1 ELSE 0 END) AS total
FROM cte
```




















</p>


### MySQL, no CASE WHEN
- Author: OutOfMemory123
- Creation Date: Wed Jun 24 2020 06:02:07 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 24 2020 06:02:07 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT \'[0-5>\' AS \'bin\', SUM(duration/60 < 5) AS \'total\'
FROM Sessions
UNION
SELECT \'[5-10>\' AS \'bin\', SUM(duration/60 >= 5 AND duration/60 < 10) AS \'total\'
FROM Sessions
UNION
SELECT \'[10-15>\' AS \'bin\', SUM(duration/60 >= 10 AND duration/60 < 15) AS \'total\'
FROM Sessions
UNION
SELECT \'15 or more\' AS \'bin\', SUM(duration/60 >= 15) AS \'total\'
FROM Sessions
```
</p>


### Union 4 times ( better than 92% )
- Author: saketgarodia_UC
- Creation Date: Fri May 08 2020 06:47:40 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 08 2020 06:48:24 GMT+0800 (Singapore Standard Time)

<p>
/* Write your T-SQL query statement below */

(select \'[0-5>\' as bin, sum(case when duration/60 < 5 then 1 else 0 end) as total from sessions)
union
(select \'[5-10>\' as bin, sum(case when ((duration/60 >= 5) and (duration/60 < 10)) then 1 else 0 end) as total from sessions)
union
(select \'[10-15>\' as bin, sum(case when ((duration/60 >= 10) and (duration/60 < 15)) then 1 else 0 end) as total from sessions)
union
(select \'15 or more\' as bin, sum(case when duration/60 >= 15 then 1 else 0 end) as total from sessions)



</p>


