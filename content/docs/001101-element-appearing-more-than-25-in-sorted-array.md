---
title: "Element Appearing More Than 25% In Sorted Array"
weight: 1101
#id: "element-appearing-more-than-25-in-sorted-array"
---
## Description
<div class="description">
<p>Given an&nbsp;integer array&nbsp;<strong>sorted</strong> in non-decreasing order, there is exactly one integer in the array that occurs more than 25% of the time.</p>

<p>Return that integer.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> arr = [1,2,2,6,6,6,6,7,10]
<strong>Output:</strong> 6
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 10^4</code></li>
	<li><code>0 &lt;= arr[i] &lt;= 10^5</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Binary Search
- Author: Poorvank
- Creation Date: Sun Dec 15 2019 00:04:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 15 2019 06:58:54 GMT+0800 (Singapore Standard Time)

<p>
Since its a sorted array so i wanted to avoid linear search

Find the element at position n/4
Perform a binary search to find the first occurrence of that item.
Perform a binary search to find the last occurrence of that item.
If last-first+1 > n/4, you have your answer.

Repeat that process for n/2 and 3(n/4)

Updated to a more readable solution:
```
public int findSpecialInteger(int[] arr) {
        int n = arr.length;
        if(n==1) {
            return arr[0];
        }
        List<Integer> list = new ArrayList<>(Arrays.asList(arr[n/4],arr[n/2],arr[(3*n)/4]));
        for (int element : list) {
            int f = firstOccurrence(arr,element);
            int l = lastOccurrence(arr,element);
            if(l-f+1>n/4) {
                return element;
            }
        }
        return -1;
    }

    private int firstOccurrence(int[] nums, int target) {
        int start=0;
        int end = nums.length-1;
        while(start < end){
            int middle = start + (end - start)/2;
            if(nums[middle]==target && (middle==start || nums[middle-1]<target)) {
                return middle;
            }
            if(target > nums[middle])
                start = middle + 1;
            else
                end = middle;
        }
        return start;

    }

    private int lastOccurrence(int[] nums,int target) {
        int start=0;
        int end = nums.length-1;
        while(start < end){
            int middle = start + (end - start)/2;
            if(nums[middle]==target && (middle==end || nums[middle+1]>target)) {
                return middle;
            }
            if(nums[middle] > target)
                end = middle;
            else
                start = middle + 1;
        }
        return start;

    }
```
</p>


### Simple Java Solution - O(n) time, O(1) space
- Author: anshu4intvcom
- Creation Date: Sun Dec 15 2019 00:01:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 15 2019 00:11:03 GMT+0800 (Singapore Standard Time)

<p>
```
    public int findSpecialInteger(int[] arr) {
        int n = arr.length, t = n / 4;

        for (int i = 0; i < n - t; i++) {
            if (arr[i] == arr[i + t]) {
                return arr[i];
            }
        }
        return -1;
    }
```
</p>


### Python3 faster over98%
- Author: zhangjunxu3
- Creation Date: Mon Jan 06 2020 12:07:12 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 06 2020 12:07:12 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:

def findSpecialInteger(self, arr: List[int]) -> int:
n = len(arr) // 4
for i in range(len(arr)):
if arr[i] == arr[i + n]:
return arr[i]
```

</p>


