---
title: "Number of Distinct Islands"
weight: 626
#id: "number-of-distinct-islands"
---
## Description
<div class="description">
<p>Given a non-empty 2D array <code>grid</code> of 0's and 1's, an <b>island</b> is a group of <code>1</code>'s (representing land) connected 4-directionally (horizontal or vertical.)  You may assume all four edges of the grid are surrounded by water.</p>

<p>Count the number of <b>distinct</b> islands.  An island is considered to be the same as another if and only if one island can be translated (and not rotated or reflected) to equal the other.</p>

<p><b>Example 1:</b><br />
<pre>
11000
11000
00011
00011
</pre>
Given the above grid map, return <code>1</code>.
</p>

<p><b>Example 2:</b><br />
<pre>11011
10000
00001
11011</pre>
Given the above grid map, return <code>3</code>.<br /><br />
Notice that:
<pre>
11
1
</pre>
and
<pre>
 1
11
</pre>
are considered different island shapes, because we do not consider reflection / rotation.
</p>

<p><b>Note:</b>
The length of each dimension in the given <code>grid</code> does not exceed 50.
</p>
</div>

## Tags
- Hash Table (hash-table)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 7 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: false)
- Palantir Technologies - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Hash By Local Coordinates [Accepted]

**Intuition and Algorithm**

At the beginning, we need to find every island, which we can do using a straightforward depth-first search.  The hard part is deciding whether two islands are the same.

Since two islands are the same if one can be translated to match another, let's translate every island so the top-left corner is `(0, 0)`  For example, if an island is made from squares `[(2, 3), (2, 4), (3, 4)]`, we can think of this shape as `[(0, 0), (0, 1), (1, 1)]` when anchored at the top-left corner.

From there, we only need to check how many unique shapes there ignoring permutations (so `[(0, 0), (0, 1)]` is the same as `[(0, 1), (1, 0)]`).  We use sets directly as we have showcased below, but we could have also sorted each list and put those sorted lists in our set structure `shapes`.

In the Java solution, we converted our tuples `(r - r0, c - c0)` to integers.  We multiplied the number of rows by `2 * grid[0].length` instead of `grid[0].length` because our local row-coordinate could be negative.

<iframe src="https://leetcode.com/playground/nxmaQmqz/shared" frameBorder="0" name="nxmaQmqz" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(R*C)$$, where $$R$$ is the number of rows in the given `grid`, and $$C$$ is the number of columns.  We visit every square once.

* Space complexity: $$O(R*C)$$, the space used by `seen` to keep track of visited squares, and `shapes`.

---
#### Approach #2: Hash By Path Signature [Accepted]

**Intuition and Algorithm**

When we start a depth-first search on the top-left square of some island, the path taken by our depth-first search will be the same if and only if the shape is the same.  We can exploit this by recording the path we take as our shape - keeping in mind to record both when we enter and when we exit the function.  The rest of the code remains as in *Approach #1*.

<iframe src="https://leetcode.com/playground/XrUoq6EL/shared" frameBorder="0" name="XrUoq6EL" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time and Space Complexity: $$O(R*C)$$.  The analysis is the same as in *Approach #1*.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java very Elegant and concise DFS Solution(Beats 100%)
- Author: kay_deep
- Creation Date: Sun Oct 08 2017 11:05:48 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 15:25:13 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
   
    int[][] dirs= new int[][]{{1,0},{0,1},{-1,0},{0,-1}};
    public int numDistinctIslands(int[][] grid) {
         Set<String> set= new HashSet<>();
        int res=0;
        
        for(int i=0;i<grid.length;i++){
            for(int j=0;j<grid[0].length;j++){
                if(grid[i][j]==1) {
                    StringBuilder sb= new StringBuilder();
                    helper(grid,i,j,0,0, sb);
                    String s=sb.toString();
                    if(!set.contains(s)){
                    res++;
                    set.add(s);
}
                }
            }
        }
            return res;
    }
    
    public  void helper(int[][] grid,int i,int j, int xpos, int ypos,StringBuilder sb){
        grid[i][j]=0;
        sb.append(xpos+""+ypos);
        for(int[] dir : dirs){
            int x=i+dir[0];
            int y=j+dir[1];
            if(x<0 || y<0 || x>=grid.length || y>=grid[0].length || grid[x][y]==0) continue;
            helper(grid,x,y,xpos+dir[0],ypos+dir[1],sb);
        }
    }
}
```


UPDATE: We can use direction string instead of using number string in set.
Below is @wavy  code using direction string.


```
public int numDistinctIslands(int[][] grid) {
    Set<String> set = new HashSet<>();
    for(int i = 0; i < grid.length; i++) {
        for(int j = 0; j < grid[i].length; j++) {
            if(grid[i][j] != 0) {
                StringBuilder sb = new StringBuilder();
                dfs(grid, i, j, sb, "o"); // origin
                grid[i][j] = 0;
                set.add(sb.toString());
            }
        }
    }
    return set.size();
}
private void dfs(int[][] grid, int i, int j, StringBuilder sb, String dir) {
    if(i < 0 || i == grid.length || j < 0 || j == grid[i].length 
       || grid[i][j] == 0) return;
    sb.append(dir);
    grid[i][j] = 0;
    dfs(grid, i-1, j, sb, "u");
    dfs(grid, i+1, j, sb, "d");
    dfs(grid, i, j-1, sb, "l");
    dfs(grid, i, j+1, sb, "r");
    sb.append("b"); // back
}
```
</p>


### DFS with Explanations
- Author: GraceMeng
- Creation Date: Sun Jul 15 2018 18:06:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 08:55:52 GMT+0800 (Singapore Standard Time)

<p>
**Logical Thought**
The key to the solution is to find a way to represent a **distinct shape**. To describe the shape, in fact, is to describe its moving directions assuming we start at the first 1 we meet `o - start`, and move `0 - right, 1 - down, 2 - left, 3 - up`. 
We need to count backtracking as a moving direction by `shape.append("_")`.Take the gird below as an example,
```
                {1, 1, 0},
                {1, 0, 0},
                {0, 0, 0},
                {1, 1, 0},
                {0, 1, 0}
```
With `shape.append("_");` one with shape `o0_1__`, another with shape `o01___`, and they will be regarded as different shapes. Or else, they will be regarded as the same shape `o01`.

**Code**
```
    private static int rows, cols;
    private static int[][] directions = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
    
    public int numDistinctIslands(int[][] grid) {
        
        cols = grid[0].length;
        rows = grid.length;   
        Set<String> uniqueShapes = new HashSet<>(); // Unique shpes.        
        StringBuilder shape;
        
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {               
                if (grid[i][j] == 1) {
                    grid[i][j] = 0;
                    shape = new StringBuilder("o");
                    dfsTraversal(i, j, grid, shape);      
                    uniqueShapes.add(shape.toString());    
                }
            }
        }
        
        return uniqueShapes.size();
    }
    
    
    private static void dfsTraversal(int x, int y, int[][] matrix, StringBuilder shape) {        
        
        for (int i = 0; i < directions.length; i++) {
            int nx = x + directions[i][0];
            int ny = y + directions[i][1];   
            if (nx >= 0 && ny >= 0 && nx < rows && ny < cols) {
                if (matrix[nx][ny] == 1) {
                    matrix[nx][ny] = 0;
                    shape.append(i);
                    dfsTraversal(nx, ny, matrix, shape);
                }
            }
        }
        shape.append("_");
        
    }
```
Thank you (❁ᴗ͈ˬᴗ͈)◞
</p>


### Simple Python 169ms
- Author: yangshun
- Creation Date: Sun Oct 08 2017 22:07:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 08 2017 22:07:23 GMT+0800 (Singapore Standard Time)

<p>
This question is very similar to the [Max Area of Island](https://leetcode.com/problems/max-area-of-island/description/) question but here instead of counting the area for each island, we find out the shape of each island. 

The shape of the island can be represented by taking the relative position of the connected cells from the leftmost cell on the top row of the island (the first cell of each island we will visit). For each island we visit, we are guaranteed to visit the top row's leftmost cell first if we iterate the matrix row by row, left to right direction. We will get the same order of cells for islands of the same shape if we perform the search in a consistent manner.

Here are some examples of how to represent the shape of each island by using cell positions relative to the top left cell.

```
# First coordinate is row difference, 
# Second coordinate is column difference.
11 -> ((0, 1)) # One cell to the right

11 -> ((0, 1), (1, 1)) # One cell to the right, one cell to the right and bottom
01
```
*- Yangshun*

```
class Solution(object):
    def numDistinctIslands(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        island_shapes = set()
        rows, cols = len(grid), len(grid[0])
        def dfs(i, j, positions, rel_pos):
            grid[i][j] = -1
            for direction in ((0, 1), (1, 0), (-1, 0), (0, -1)):
                next_i, next_j = i + direction[0], j + direction[1]
                if (0 <= next_i < rows and 0 <= next_j < cols) and grid[next_i][next_j] == 1:
                    new_rel_pos = (rel_pos[0] + direction[0], rel_pos[1] + direction[1])
                    positions.append(new_rel_pos)
                    dfs(next_i, next_j, positions, new_rel_pos)
        for i in range(rows):
            for j in range(cols):
                if grid[i][j] == 1:
                    positions = []
                    dfs(i, j, positions, (0, 0))
                    island_shapes.add(tuple(positions))
        return len(island_shapes)
```
</p>


