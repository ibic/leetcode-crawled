---
title: "Maximum 69 Number"
weight: 1242
#id: "maximum-69-number"
---
## Description
<div class="description">
<p>Given a positive integer <code>num</code> consisting only of digits 6 and 9.</p>

<p>Return the maximum number you can get by changing <strong>at most</strong> one digit (6 becomes 9, and 9 becomes 6).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> num = 9669
<strong>Output:</strong> 9969
<strong>Explanation:</strong> 
Changing the first digit results in 6669.
Changing the second digit results in 9969.
Changing the third digit results in 9699.
Changing the fourth digit results in 9666.&nbsp;
The maximum number is 9969.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> num = 9996
<strong>Output:</strong> 9999
<strong>Explanation:</strong> Changing the last digit 6 to 9 results in the maximum number.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> num = 9999
<strong>Output:</strong> 9999
<strong>Explanation:</strong> It is better not to apply any change.</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= num &lt;= 10^4</code></li>
	<li><code>num</code>&#39;s digits are 6 or 9.</li>
</ul>
</div>

## Tags
- Math (math)

## Companies
- HRT - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Replace Once
- Author: lee215
- Creation Date: Sun Jan 19 2020 12:03:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 19 2020 12:03:38 GMT+0800 (Singapore Standard Time)

<p>
**Python:**
```python
    def maximum69Number(self, num):
        return int(str(num).replace(\'6\', \'9\', 1))
```

</p>


### Solution for Interview. One pass without converting to string
- Author: CodingHu
- Creation Date: Mon Jan 20 2020 18:39:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 02 2020 23:22:28 GMT+0800 (Singapore Standard Time)

<p>
Most solutions are based on changing the integer to string, then find the first \'6\' and replace it to \'9\' and thirdly, change the string back to integer. So what do we actually do?  This solution involves 3 passes, the first pass is changing the integer to string, the second pass is to scan string and find \'6\' and the third pass is to change integer to string, we also need a O(log(n)) space to store a length lg(n) string.

I think optimal solution should not do these steps.  Best performance is just using integer and do it with one pass and constant memory.

Here is the solution.

Algorithm:
1. we use a \'sixindex = -1\' to store the first occurance of 6 in the integer. tem as the orignal value num, i = 0 as the first digit of 6.
2.  begin a while loop,  tem greater than 0
	3.  Everytime we take "tem % 10" to get current digit. if current digit is 6, we store it to sixindex. During the loop, the larger digit will reflesh the sixindex. So when the loop ends, we got the biggest 6 digit.
	4.  we shift tem by 1 digit. tem = tem//10.
	5.  increase the digit index by 1, i += 1
	6.  continue loop 2
7. if no 6 exists, sixindex will not get updated and is "-1" we just return the num it self. if there is 6, we just add 3 to that digit, this is done by "pow(10,sixindex) * 3 + num"





```
class Solution:
    def maximum69Number (self, num: int) -> int:
        i = 0 
        tem = num
        sixidx = -1 
        while tem > 0:
            if tem % 10 == 6:
                sixidx = i  #refresh sixidx when found 6 at large digit.
            tem = tem//10
            i += 1
        return (num + 3 *(10**sixidx)) if sixidx != -1 else num
```
</p>


### Easy One In Java
- Author: JatinYadav96
- Creation Date: Sun Jan 19 2020 12:10:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 19 2020 12:10:54 GMT+0800 (Singapore Standard Time)

<p>
```
public int maximum69Number(int num) {
	char[] chars = Integer.toString(num).toCharArray();
	for (int i = 0; i < chars.length; i++) {
		if (chars[i] == \'6\') {
			chars[i] = \'9\';
			break;
		}
	}
	return Integer.parseInt(new String(chars));
}
```
</p>


