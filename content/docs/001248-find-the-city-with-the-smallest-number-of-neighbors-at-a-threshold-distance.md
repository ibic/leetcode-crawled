---
title: "Find the City With the Smallest Number of Neighbors at a Threshold Distance"
weight: 1248
#id: "find-the-city-with-the-smallest-number-of-neighbors-at-a-threshold-distance"
---
## Description
<div class="description">
<p>There are <code>n</code> cities numbered from <code>0</code> to <code>n-1</code>. Given the array <code>edges</code>&nbsp;where <code>edges[i] = [from<sub>i</sub>, to<sub>i</sub>, weight<sub>i</sub>]</code> represents a bidirectional and weighted edge between cities <code>from<sub>i</sub></code>&nbsp;and <code>to<sub>i</sub></code>, and given the integer <code>distanceThreshold</code>.</p>

<p>Return the city with the smallest number<strong> </strong>of&nbsp;cities that are reachable through some path and whose distance is <strong>at most</strong> <code>distanceThreshold</code>, If there are multiple such cities, return the city with the greatest number.</p>

<p>Notice that the distance of a path connecting cities <em><strong>i</strong></em> and <em><strong>j</strong></em> is equal to the sum of the edges&#39; weights along that path.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/01/16/find_the_city_01.png" style="width: 300px; height: 225px;" /></p>

<pre>
<strong>Input:</strong> n = 4, edges = [[0,1,3],[1,2,1],[1,3,4],[2,3,1]], distanceThreshold = 4
<strong>Output:</strong> 3
<strong>Explanation: </strong>The figure above describes the graph.&nbsp;
The neighboring cities at a distanceThreshold = 4 for each city are:
City 0 -&gt; [City 1, City 2]&nbsp;
City 1 -&gt; [City 0, City 2, City 3]&nbsp;
City 2 -&gt; [City 0, City 1, City 3]&nbsp;
City 3 -&gt; [City 1, City 2]&nbsp;
Cities 0 and 3 have 2 neighboring cities at a distanceThreshold = 4, but we have to return city 3 since it has the greatest number.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/01/16/find_the_city_02.png" style="width: 300px; height: 225px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 5, edges = [[0,1,2],[0,4,8],[1,2,3],[1,4,2],[2,3,1],[3,4,1]], distanceThreshold = 2
<strong>Output:</strong> 0
<strong>Explanation: </strong>The figure above describes the graph.&nbsp;
The neighboring cities at a distanceThreshold = 2 for each city are:
City 0 -&gt; [City 1]&nbsp;
City 1 -&gt; [City 0, City 4]&nbsp;
City 2 -&gt; [City 3, City 4]&nbsp;
City 3 -&gt; [City 2, City 4]
City 4 -&gt; [City 1, City 2, City 3]&nbsp;
The city 0 has 1 neighboring city at a distanceThreshold = 2.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 100</code></li>
	<li><code>1 &lt;= edges.length &lt;= n * (n - 1) / 2</code></li>
	<li><code>edges[i].length == 3</code></li>
	<li><code>0 &lt;= from<sub>i</sub> &lt; to<sub>i</sub> &lt; n</code></li>
	<li><code>1 &lt;= weight<sub>i</sub>,&nbsp;distanceThreshold &lt;= 10^4</code></li>
	<li>All pairs <code>(from<sub>i</sub>, to<sub>i</sub>)</code> are distinct.</li>
</ul>

</div>

## Tags
- Graph (graph)

## Companies
- Uber - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy Floyd Algorithm
- Author: lee215
- Creation Date: Sun Jan 26 2020 12:05:36 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 30 2020 18:08:56 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Becasue `O(N^3)` is accepted in this problem, we don\'t need a very fast solution.
we can simply use Floyd algorithm to find the minium distance any two cities.

Reference [Floyd\u2013Warshall algorithm](https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm)

I first saw @awice using it long time ago.
It\'s really easy and makes a lot sense.

Iterate all point middle point `k`,
iterate all pairs (i,j).
If it go through the middle point `k`,
`dis[i][j] = dis[i][k] + dis[k][j]`.
<br>

## **Complexity**
Time `O(N^3)`
Space `O(N^2)`
<br>

**Java**
```java
    public int findTheCity(int n, int[][] edges, int distanceThreshold) {
        int[][] dis = new int[n][n];
        int res = 0, smallest = n;
        for (int[] row : dis)
            Arrays.fill(row, 10001);
        for (int[] e : edges)
            dis[e[0]][e[1]] = dis[e[1]][e[0]] = e[2];
        for (int i = 0; i < n; ++i)
            dis[i][i] = 0;
        for (int k = 0; k < n; ++k)
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    dis[i][j] = Math.min(dis[i][j], dis[i][k] + dis[k][j]);
        for (int i = 0; i < n; i++) {
            int count = 0;
            for (int j = 0; j < n; ++j)
                if (dis[i][j] <= distanceThreshold)
                    ++count;
            if (count <= smallest) {
                res = i;
                smallest = count;
            }
        }
        return res;
    }
```
**C++**
```cpp
    int findTheCity(int n, vector<vector<int>>& edges, int distanceThreshold) {
        vector<vector<int>> dis(n, vector(n, 10001));
        int res = 0, smallest = n;
        for (auto& e : edges)
            dis[e[0]][e[1]] = dis[e[1]][e[0]] = e[2];
        for (int i = 0; i < n; ++i)
            dis[i][i] = 0;
        for (int k = 0; k < n; ++k)
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    dis[i][j] = min(dis[i][j], dis[i][k] + dis[k][j]);
        for (int i = 0; i < n; i++) {
            int count = 0;
            for (int j = 0; j < n; ++j)
                if (dis[i][j] <= distanceThreshold)
                    ++count;
            if (count <= smallest) {
                res = i;
                smallest = count;
            }
        }
        return res;
    }
```
**Python:**
```python
    def findTheCity(self, n, edges, maxd):
        dis = [[float(\'inf\')] * n for _ in xrange(n)]
        for i, j, w in edges:
            dis[i][j] = dis[j][i] = w
        for i in xrange(n):
            dis[i][i] = 0
        for k in xrange(n):
            for i in xrange(n):
                for j in xrange(n):
                    dis[i][j] = min(dis[i][j], dis[i][k] + dis[k][j])
        res = {sum(d <= maxd for d in dis[i]): i for i in xrange(n)}
        return res[min(res)]
```

</p>


### The Reason of DFS Not Working (Explain Graph and Example)
- Author: czyang
- Creation Date: Sun Jan 26 2020 15:41:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 26 2020 15:41:08 GMT+0800 (Singapore Standard Time)

<p>
Many people solve this problem with a DFS algorithm and have a problem with some very big test cases. I found a short test case will fail the DFS solutions and explain the reason.

This test case will fail a DFS solution(I already contributed this test case):
```
6
[[0,1,10],[0,2,1],[2,3,1],[1,3,1],[1,4,1],[4,5,10]]
20
```

The graph looks like this, and The **distanceThreshold = 20**
![image](https://assets.leetcode.com/users/czyang/image_1580023655.png)

If your DFS go though the **0->3** before the **0->1** the error will encounter. Note the visited list.
![image](https://assets.leetcode.com/users/czyang/image_1580023776.png)

The next time your DFS goes **0->1** it will find the 3 already visited and the 5 will never visit. So that\'s why those case will have fewer city visit than expect.
![image](https://assets.leetcode.com/users/czyang/image_1580023814.png)

By the way, this question is basiclly a trap. If you never heard of **Floyd-Warshall algorithm** or noticed those cases. A lot people will directly use DFS. This question\'s contest AC rate only: **0.259**

</p>


### [Java] Floyd, SPFA, Dijkstra, and Bellman
- Author: renjunyao
- Creation Date: Thu Jan 30 2020 04:35:20 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 30 2020 04:35:20 GMT+0800 (Singapore Standard Time)

<p>
All the sp algorithms works: 
Floyd: 14ms
Dijkstra: 32ms
SPFA: 64ms
Bellman: 251ms


```
class Solution {
    public int findTheCity(int n, int[][] edges, int distanceThreshold) {
        int INF = (int) 1e9 + 7;
        List<int[]>[] adj = new List[n];
        int[][] dist = new int[n][n];
        for (int i = 0; i < n; i++) {
            Arrays.fill(dist[i], INF);
            dist[i][i] = 0;
        }
        for (int i = 0; i < n; i++) {adj[i] = new ArrayList<>();}
        for (int[] e : edges) {
            int u = e[0];
            int v = e[1];
            int d = e[2];
            
            adj[u].add(new int[]{v, d});
            adj[v].add(new int[]{u, d});
            // dist[u][v] = d;
            // dist[v][u] = d;
        }
        
        // floyd(n, adj, dist);
        for (int i = 0; i < n; i++) {
            // dijkstra(n, adj, dist[i], i);
            // bellman(n, edges, dist[i], i);
            spfa(n, adj, dist[i], i);
        }
        
        int minCity = -1;
        int minCount = n;
        
        for (int i = 0; i < n; i++) {
            int curCount = 0;
            for (int j = 0; j < n; j++) {
                if (i == j) {continue;}
                if (dist[i][j] <= distanceThreshold) {curCount++;}
            }
            if (curCount <= minCount) {
                minCount = curCount;
                minCity = i;
            }
        }
        
        return minCity;
    }
    
    void spfa(int n, List<int[]>[] adj, int[] dist, int src) {
        Deque<Integer> q = new ArrayDeque<>();
        int[] updateTimes = new int[n];
        q.add(src);
        
        while (!q.isEmpty()) {
            int u = q.removeFirst();
            for (int[] next : adj[u]) {
                int v = next[0];
                int duv = next[1];
                
                if (dist[v] > dist[u] + duv) {
                    dist[v] = dist[u] + duv;
                    updateTimes[v]++;
                    q.add(v);
                    if (updateTimes[v] > n) {System.out.println("wrong");}
                }
            }
        }
    }
    
    void bellman(int n, int[][] edges, int[] dist, int src) {
        for (int k = 1; k < n; k++) {
            for (int[] e : edges) {
                int u = e[0];
                int v = e[1];
                int duv = e[2];
                
                if (dist[u] > dist[v] + duv) {
                    dist[u] = dist[v] + duv;
                }
                
                if (dist[v] > dist[u] + duv) {
                    dist[v] = dist[u] + duv;
                }
            }
        }
    }
    
    void dijkstra(int n, List<int[]>[] adj, int[] dist, int src) {
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> (a[1] - b[1]));
        pq.add(new int[]{src, 0});
        while (!pq.isEmpty()) {
            int[] cur = pq.remove();
            int u = cur[0];
            int du = cur[1];
            if (du > dist[u]) {continue;}
            
            for (int[] nb : adj[u]) {
                int v = nb[0];
                int duv = nb[1];
                if (dist[v] > du + duv) {
                    dist[v] = du + duv;
                    pq.add(new int[]{v, dist[v]});
                }
            }
        }
    }
    
    void floyd(int n, List<int[]>[] adj, int[][] dist) {
        for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    dist[i][j] = Math.min(dist[i][j], dist[i][k] + dist[k][j]);
                }
            }
        } 
    }
}
```
</p>


