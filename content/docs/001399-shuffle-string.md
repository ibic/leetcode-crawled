---
title: "Shuffle String"
weight: 1399
#id: "shuffle-string"
---
## Description
<div class="description">
<p>Given a string <code>s</code>&nbsp;and an integer array <code>indices</code> of the <strong>same length</strong>.</p>

<p>The string <code>s</code> will be shuffled such that the character at the <code>i<sup>th</sup></code> position moves to&nbsp;<code>indices[i]</code> in the shuffled string.</p>

<p>Return <em>the shuffled string</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/09/q1.jpg" style="width: 321px; height: 243px;" />
<pre>
<strong>Input:</strong> s = &quot;codeleet&quot;, <code>indices</code> = [4,5,6,7,0,2,1,3]
<strong>Output:</strong> &quot;leetcode&quot;
<strong>Explanation:</strong> As shown, &quot;codeleet&quot; becomes &quot;leetcode&quot; after shuffling.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abc&quot;, <code>indices</code> = [0,1,2]
<strong>Output:</strong> &quot;abc&quot;
<strong>Explanation:</strong> After shuffling, each character remains in its position.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aiohn&quot;, <code>indices</code> = [3,1,4,2,0]
<strong>Output:</strong> &quot;nihao&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aaiougrt&quot;, <code>indices</code> = [4,0,2,6,7,3,1,5]
<strong>Output:</strong> &quot;arigatou&quot;
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;art&quot;, <code>indices</code> = [1,0,2]
<strong>Output:</strong> &quot;rat&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>s.length == indices.length == n</code></li>
	<li><code>1 &lt;= n &lt;= 100</code></li>
	<li><code>s</code> contains only lower-case English letters.</li>
	<li><code>0 &lt;= indices[i] &lt;&nbsp;n</code></li>
	<li>All values of <code>indices</code> are unique (i.e. <code>indices</code> is a permutation of the integers from <code>0</code> to <code>n - 1</code>).</li>
</ul>
</div>

## Tags
- Sort (sort)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Used Cyclic Sort with O(1) SPACE and O(N) Time complexity
- Author: reddy_vishnuvardhan
- Creation Date: Sun Jul 26 2020 12:12:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 16:01:34 GMT+0800 (Singapore Standard Time)

<p>
**1. This Problem can best solved with cyclic sort because all indices lies between 0 to n-1 and the swaps also follows the same pattern.
2. This will help you achieve O(1) Space complexity and with minimum no of swaps you can achieve your target
**

```
class Solution {
public:
    string restoreString(string s, vector<int>& indices) {
        
 
        
        for(int i=0;i<indices.size();i++)
        {
            
              while(indices[i]!=i)
              {
                  swap(s[i],s[indices[i]]);
                  swap(indices[i],indices[indices[i]]);
                       
              }
        }
        
       
        return s;
    }
};
```
</p>


### Java Simple Easy to Understand Do as Question says O(n)
- Author: ngytlcdsalcp1
- Creation Date: Sun Jul 26 2020 12:11:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 12:29:37 GMT+0800 (Singapore Standard Time)

<p>
`i`th character needs to shift `in[i]`the index.

```
class Solution {
    public String restoreString(String s, int[] in) {
        char[] c = new char[in.length];
        for(int i = 0; i < in.length; i++)
            c[in[i]] = s.charAt(i);
        return new String(c);
    }
}
```
</p>


### C++ | Easy to understand | O(n)
- Author: di_b
- Creation Date: Sun Jul 26 2020 12:06:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 12:12:12 GMT+0800 (Singapore Standard Time)

<p>
```
    string restoreString(string s, vector<int>& indices) {
        string res = s;
        for(int i =0; i < indices.size(); ++i)
            res[indices[i]] = s[i];
        return res;
    }
</p>


