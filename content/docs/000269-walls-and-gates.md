---
title: "Walls and Gates"
weight: 269
#id: "walls-and-gates"
---
## Description
<div class="description">
<p>You are given a <i>m x n</i> 2D grid initialized with these three possible values.</p>

<ol>
	<li><code>-1</code> - A wall or an obstacle.</li>
	<li><code>0</code> - A gate.</li>
	<li><code>INF</code> - Infinity means an empty room. We use the value <code>2<sup>31</sup> - 1 = 2147483647</code> to represent <code>INF</code> as you may assume that the distance to a gate is less than <code>2147483647</code>.</li>
</ol>

<p>Fill each empty room with the distance to its <i>nearest</i> gate. If it is impossible to reach a gate, it should be filled with <code>INF</code>.</p>

<p><strong>Example:&nbsp;</strong></p>

<p>Given the 2D grid:</p>

<pre>
INF  -1  0  INF
INF INF INF  -1
INF  -1 INF  -1
  0  -1 INF INF
</pre>

<p>After running your function, the 2D grid should be:</p>

<pre>
  3  -1   0   1
  2   2   1  -1
  1  -1   2  -1
  0  -1   3   4
</pre>

</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 6 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Facebook - 3 (taggedByAdmin: true)
- Spotify - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Uber - 7 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 (Brute Force) [Time Limit Exceeded]

The brute force approach is simple, we just implement a breadth-first search from each empty room to its nearest gate.

While we are doing the search, we use a 2D array called `distance` to keep track of the distance from the starting point. It also implicitly tell us whether a position had been visited so it won't be inserted into the queue again.

```java
private static final int EMPTY = Integer.MAX_VALUE;
private static final int GATE = 0;
private static final int WALL = -1;
private static final List<int[]> DIRECTIONS = Arrays.asList(
        new int[] { 1,  0},
        new int[] {-1,  0},
        new int[] { 0,  1},
        new int[] { 0, -1}
);

public void wallsAndGates(int[][] rooms) {
    if (rooms.length == 0) return;
    for (int row = 0; row < rooms.length; row++) {
        for (int col = 0; col < rooms[0].length; col++) {
            if (rooms[row][col] == EMPTY) {
                rooms[row][col] = distanceToNearestGate(rooms, row, col);
            }
        }
    }
}

private int distanceToNearestGate(int[][] rooms, int startRow, int startCol) {
    int m = rooms.length;
    int n = rooms[0].length;
    int[][] distance = new int[m][n];
    Queue<int[]> q = new LinkedList<>();
    q.add(new int[] { startRow, startCol });
    while (!q.isEmpty()) {
        int[] point = q.poll();
        int row = point[0];
        int col = point[1];
        for (int[] direction : DIRECTIONS) {
            int r = row + direction[0];
            int c = col + direction[1];
            if (r < 0 || c < 0 || r >= m || c >= n || rooms[r][c] == WALL
                    || distance[r][c] != 0) {
                continue;
            }
            distance[r][c] = distance[row][col] + 1;
            if (rooms[r][c] == GATE) {
                return distance[r][c];
            }
            q.add(new int[] { r, c });
        }
    }
    return Integer.MAX_VALUE;
}
```

**Complexity analysis**

* Time complexity : $$O(m^2n^2)$$.
For each point in the $$m \times n$$ size grid, the gate could be at most $$m \times n$$ steps away.

* Space complexity : $$O(mn)$$.
The space complexity depends on the queue's size. Since we won't insert points that have been visited before into the queue, we insert at most $$m \times n$$ points into the queue.

---
#### Approach #2 (Breadth-first Search) [Accepted]

Instead of searching from an empty room to the gates, how about searching the other way round? In other words, we initiate breadth-first search (BFS) from all gates at the same time. Since BFS guarantees that we search all rooms of distance *d* before searching rooms of distance *d* + 1, the distance to an empty room must be the shortest.

```java
private static final int EMPTY = Integer.MAX_VALUE;
private static final int GATE = 0;
private static final List<int[]> DIRECTIONS = Arrays.asList(
        new int[] { 1,  0},
        new int[] {-1,  0},
        new int[] { 0,  1},
        new int[] { 0, -1}
);

public void wallsAndGates(int[][] rooms) {
    int m = rooms.length;
    if (m == 0) return;
    int n = rooms[0].length;
    Queue<int[]> q = new LinkedList<>();
    for (int row = 0; row < m; row++) {
        for (int col = 0; col < n; col++) {
            if (rooms[row][col] == GATE) {
                q.add(new int[] { row, col });
            }
        }
    }
    while (!q.isEmpty()) {
        int[] point = q.poll();
        int row = point[0];
        int col = point[1];
        for (int[] direction : DIRECTIONS) {
            int r = row + direction[0];
            int c = col + direction[1];
            if (r < 0 || c < 0 || r >= m || c >= n || rooms[r][c] != EMPTY) {
                continue;
            }
            rooms[r][c] = rooms[row][col] + 1;
            q.add(new int[] { r, c });
        }
    }
}
```

**Complexity analysis**

* Time complexity : $$O(mn)$$.

    If you are having difficulty to derive the time complexity, start simple.

    Let us start with the case with only one gate. The breadth-first search takes at most $$m \times n$$ steps to reach all rooms, therefore the time complexity is $$O(mn)$$. But what if you are doing breadth-first search from $$k$$ gates?

    Once we set a room's distance, we are basically marking it as visited, which means each room is visited at most once. Therefore, the time complexity does not depend on the number of gates and is $$O(mn)$$.

* Space complexity : $$O(mn)$$.
The space complexity depends on the queue's size. We insert at most $$m \times n$$ points into the queue.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java BFS Solution-O(mn) Time
- Author: chase1991
- Creation Date: Thu Sep 24 2015 22:13:43 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 21:06:18 GMT+0800 (Singapore Standard Time)

<p>
Push all gates into queue first. Then for each gate update its neighbor cells and push them to the queue. 

Repeating above steps until there is nothing left in the queue.

    public class Solution {
        public void wallsAndGates(int[][] rooms) {
            if (rooms.length == 0 || rooms[0].length == 0) return;
            Queue<int[]> queue = new LinkedList<>();
            for (int i = 0; i < rooms.length; i++) {
                for (int j = 0; j < rooms[0].length; j++) {
                    if (rooms[i][j] == 0) queue.add(new int[]{i, j});
                }
            }
            while (!queue.isEmpty()) {
                int[] top = queue.remove();
                int row = top[0], col = top[1];
                if (row > 0 && rooms[row - 1][col] == Integer.MAX_VALUE) {
                    rooms[row - 1][col] = rooms[row][col] + 1;
                    queue.add(new int[]{row - 1, col});
                }
                if (row < rooms.length - 1 && rooms[row + 1][col] == Integer.MAX_VALUE) {
                    rooms[row + 1][col] = rooms[row][col] + 1;
                    queue.add(new int[]{row + 1, col});
                }
                if (col > 0 && rooms[row][col - 1] == Integer.MAX_VALUE) {
                    rooms[row][col - 1] = rooms[row][col] + 1;
                    queue.add(new int[]{row, col - 1});
                }
                if (col < rooms[0].length - 1 && rooms[row][col + 1] == Integer.MAX_VALUE) {
                    rooms[row][col + 1] = rooms[row][col] + 1;
                    queue.add(new int[]{row, col + 1});
                }
            }
        }
    }
</p>


### Benchmarks of DFS and BFS
- Author: dietpepsi
- Creation Date: Mon Jan 25 2016 05:24:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 14:38:54 GMT+0800 (Singapore Standard Time)

<p>
Better view [here](http://algobox.org/walls-and-gates/)
### The Solutions

The Multi End BFS solution used is this

    public static final int[] d = {0, 1, 0, -1, 0};

    public void wallsAndGates(int[][] rooms) {
        if (rooms.length == 0) return;
        int m = rooms.length, n = rooms[0].length;

        Deque<Integer> queue = new ArrayDeque<>();
        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j)
                if (rooms[i][j] == 0) queue.offer(i * n + j); // Put gates in the queue

        while (!queue.isEmpty()) {
            int x = queue.poll();
            int i = x / n, j = x % n;
            for (int k = 0; k < 4; ++k) {
                int p = i + d[k], q = j + d[k + 1]; // empty room
                if (0 <= p && p < m && 0 <= q && q < n && rooms[p][q] == Integer.MAX_VALUE) {
                    rooms[p][q] = rooms[i][j] + 1;
                    queue.offer(p * n + q);
                }
            }
        }
    }

The Naive BFS solution used is this

    public static final int[] d = {0, 1, 0, -1, 0};

    public void wallsAndGates(int[][] rooms) {
        if (rooms.length == 0) return;
        for (int i = 0; i < rooms.length; ++i)
            for (int j = 0; j < rooms[0].length; ++j)
                if (rooms[i][j] == 0) bfs(rooms, i, j);
    }

    private void bfs(int[][] rooms, int i, int j) {
        int m = rooms.length, n = rooms[0].length;
        Deque<Integer> queue = new ArrayDeque<>();
        queue.offer(i * n + j); // Put gate in the queue
        while (!queue.isEmpty()) {
            int x = queue.poll();
            i = x / n; j = x % n;
            for (int k = 0; k < 4; ++k) {
                int p = i + d[k], q = j + d[k + 1];
                if (0 <= p && p < m && 0 <= q && q < n && rooms[p][q] > rooms[i][j] + 1) {
                    rooms[p][q] = rooms[i][j] + 1;
                    queue.offer(p * n + q);
                }
            }
        }
    }

And the DFS solution used is this

    private static int[] d = {0, 1, 0, -1, 0};

    public void wallsAndGates(int[][] rooms) {
        for (int i = 0; i < rooms.length; i++)
            for (int j = 0; j < rooms[0].length; j++)
                if (rooms[i][j] == 0) dfs(rooms, i, j);
    }

    public void dfs(int[][] rooms, int i, int j) {
        for (int k = 0; k < 4; ++k) {
            int p = i + d[k], q = j + d[k + 1];
            if (0<= p && p < rooms.length && 0<= q && q < rooms[0].length && rooms[p][q] > rooms[i][j] + 1) {
                rooms[p][q] = rooms[i][j] + 1;
                dfs(rooms, p, q);
            }
        }
    }



## Some benchmark:

### CASE 1: n by n matrix with all empty rooms except one gate at upper left corner.
The case generator is like this:

    public static int[][] generateSingleGate(int n) {
        int[][] rooms = new int[n][n];
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                rooms[i][j] = Integer.MAX_VALUE;
        rooms[0][0] = 0;
        return rooms;
    }

The results are

     n	MEBFS		NaiveBFS	DFS
     10	0.161 ms	0.157 ms	0.715 ms
     20	0.848 ms	0.482 ms	3.913 ms  
     40	2.672 ms	1.009 ms	25.429 ms
     80	5.974 ms	3.879 ms	241.825 ms
    160	9.282 ms	9.687 ms	StackOverflowError

For this case due to the deep recursion, DFS is much slower than BFS. DFS is repeatedly updating the cell distance. Since there is only one gate in this case, NaiveBFS is expected to perform just like the MultiEndBFS.


### CASE 2: n by n matrix with a lot of gates.
The case generator is like this

    public static int[][] generateMassiveGates(int n) {
        int[][] rooms = new int[n][n];
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                if (i % 2 != 0 || j % 2 != 0)
                    rooms[i][j] = Integer.MAX_VALUE;
        return rooms;
    }


The results are:

     n	MEBFS		NaiveBFS	DFS
     10	0.244 ms	0.783 ms	0.471 ms
     20	0.611 ms	3.064 ms	1.941 ms
     40	1.616 ms	10.370 ms	7.248 ms
     80	6.220 ms	26.910 ms	68.338 ms
    160	12.291 ms	95.291 ms	stackoverflow/915.517 ms
	320 27.790 ms	435.643 ms	stackoverflow/12719.976 ms
    640	85.793 ms	3502.662 ms

Like expected, the DFS is much slower, and it is again overflowed.
The Naive BFS out performs DFS starting from n = 80.
If we change the vm options to -Xss20m, DFS will run in 915 ms for n=160.
Fitting the time vs n, I found that the MEBFS is O(n^2), NaiveBFS is O(n^3), DFS is O(n^4) for these cases.


### CASE 3: n by n matrix with a lot of gates but rooms are isolated by walls and gates

The case generator is this:

    public static int[][] generateIsolateRooms(int n) {
        int[][] rooms = new int[n][n];
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                if (i % 2 != 0 && j % 2 != 0)
                    rooms[i][j] = Integer.MAX_VALUE;
        return rooms;
    }

The results are

     n	MEBFS		NaiveBFS	DFS
     10	0.167 ms	0.268 ms	0.049 ms
     20	0.593 ms	0.865 ms	0.196 ms  
     40	2.073 ms	2.096 ms	1.117 ms
     80	7.347 ms	4.471 ms	2.598 ms
    160	7.223 ms	4.232 ms	2.730 ms

Only in this case DFS wins. Since rooms are isolated, there will be limited recalculation and very shallow recursion.
The currently testcases in the OJ must somewhat in this catergory which results in a bias of DFS solutions.

### Conclusions

The performances of MultiEnd is very stable and have time complexities of O(n^2) for a `n x n` square matrix.

The time complexity for NaiveBFS should be O(n^4) in the worst case. However is not shown in our tests.

The performance of recursive DFS is very unstable. It is much slower than BFS if the rooms are interconnected. It is only faster than BFS when small groups of rooms are isolated. In the worst case the time complexity is also O(n^4).

Thus, for this problem we should prefer BFS over DFS. And the best Solution is Multi End BFS.

And I suggest admin to add more test cases to emphasize this preference.
</p>


### My short java solution, very easy to understand
- Author: hwy_2015
- Creation Date: Thu Jan 07 2016 16:49:12 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 17 2018 07:39:57 GMT+0800 (Singapore Standard Time)

<p>
    public void wallsAndGates(int[][] rooms) {
        for (int i = 0; i < rooms.length; i++)
            for (int j = 0; j < rooms[0].length; j++)
                if (rooms[i][j] == 0) dfs(rooms, i, j, 0);
    }
    
    private void dfs(int[][] rooms, int i, int j, int d) {
        if (i < 0 || i >= rooms.length || j < 0 || j >= rooms[0].length || rooms[i][j] < d) return;
        rooms[i][j] = d;
        dfs(rooms, i - 1, j, d + 1);
        dfs(rooms, i + 1, j, d + 1);
        dfs(rooms, i, j - 1, d + 1);
        dfs(rooms, i, j + 1, d + 1);
    }
</p>


