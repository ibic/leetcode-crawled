---
title: "Kth Largest Element in an Array"
weight: 199
#id: "kth-largest-element-in-an-array"
---
## Description
<div class="description">
<p>Find the <strong>k</strong>th largest element in an unsorted array. Note that it is the kth largest element in the sorted order, not the kth distinct element.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> <code>[3,2,1,5,6,4] </code>and k = 2
<strong>Output:</strong> 5
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> <code>[3,2,3,1,2,4,5,5,6] </code>and k = 4
<strong>Output:</strong> 4</pre>

<p><strong>Note: </strong><br />
You may assume k is always valid, 1 &le; k &le; array&#39;s length.</p>

</div>

## Tags
- Divide and Conquer (divide-and-conquer)
- Heap (heap)

## Companies
- Facebook - 51 (taggedByAdmin: true)
- Amazon - 11 (taggedByAdmin: true)
- ByteDance - 7 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: true)
- eBay - 4 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)
- LinkedIn - 8 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)
- Spotify - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- JPMorgan - 2 (taggedByAdmin: false)
- Alibaba - 3 (taggedByAdmin: false)
- VMware - 3 (taggedByAdmin: false)
- Expedia - 3 (taggedByAdmin: false)
- Tencent - 2 (taggedByAdmin: false)
- Airbnb - 2 (taggedByAdmin: false)
- Baidu - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Roblox - 2 (taggedByAdmin: false)
- Grab - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Pocket Gems - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 0: Sort

The naive solution would be to sort an array first and then return kth 
element from the end, something like `sorted(nums)[-k]` on Python. 
That would be an algorithm of $$\mathcal{O}(N \log N)$$ time complexity
and $$\mathcal{O}(1)$$ space complexity.
This time complexity is not really exciting so 
let's check how to improve it by using some additional space.
<br />
<br />


---     
#### Approach 1: Heap

The idea is to init a heap "the smallest element first",
and add all elements from the array into this heap one by one
keeping the size of the heap always less or equal to `k`. 
That would results in a heap containing `k` largest elements of the array.

The head of this heap is the answer, 
i.e. the kth largest element of the array. 

The time complexity of adding an element in a heap of size `k`
is $$\mathcal{O}(\log k)$$, and we do it `N` times that means 
$$\mathcal{O}(N \log k)$$ time complexity for the algorithm.

In Python there is a method `nlargest` in `heapq` library 
which has the same $$\mathcal{O}(N \log k)$$
time complexity and reduces the code to one line.

This algorithm improves time complexity, but one pays with 
$$\mathcal{O}(k)$$ space complexity.
        
<!--![LIS](../Figures/72/72_tr.gif)-->
!?!../Documents/215_LIS.json:1000,530!?!

<iframe src="https://leetcode.com/playground/3fPxq2Yv/shared" frameBorder="0" width="100%" height="344" name="3fPxq2Yv"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N \log k)$$. 
* Space complexity : $$\mathcal{O}(k)$$ to store the heap elements. 
<br />
<br />


---
#### Approach 2: Quickselect 

This [textbook algorthm](https://en.wikipedia.org/wiki/Quickselect) 
has $$\mathcal{O}(N)$$ average time complexity.
Like quicksort, it was developed by Tony Hoare, 
and is also known as _Hoare's selection algorithm_.

The approach is basically the same as for quicksort. 
For simplicity let's notice that `k`th largest element is the same as
`N - k`th smallest element, hence one could implement 
`k`th smallest algorithm for this problem.

First one chooses a pivot, 
and defines its position in a sorted array in a linear time.
This could be done with the help of _partition algorithm_.

> To implement partition one moves along an array,
compares each element with a pivot, 
and moves all elements smaller than pivot to the left of the pivot.
 
As an output we have an array where pivot is on its perfect position
in the ascending sorted array, 
all elements on the left of the pivot are smaller than pivot,
and all elements on the right of the pivot are larger or equal to pivot.

Hence the array is now split into two parts.
If that would be a quicksort algorithm, one would proceed recursively 
to use quicksort for the both parts that would result in 
$$\mathcal{O}(N \log N)$$ time complexity.
Here there is no need to deal with both parts since now one knows 
in which part to search for `N - k`th smallest element, and that
reduces average time complexity to $$\mathcal{O}(N)$$.

Finally the overall algorithm is quite straightforward :

* Choose a random pivot.

* Use a partition algorithm to place the pivot 
into its perfect position `pos` in the sorted array,
move smaller elements to the left of pivot, and larger or equal ones - to the right.

* Compare `pos` and `N - k` to choose the side of array to proceed recursively.

> ! Please notice that this algorithm works well even for arrays with duplicates.

![quickselect](../Figures/215/215_quickselect.png)

<iframe src="https://leetcode.com/playground/QsycSKCS/shared" frameBorder="0" width="100%" height="500" name="QsycSKCS"></iframe>

* Time complexity : $$\mathcal{O}(N)$$ in the average case, $$\mathcal{O}(N^2)$$ in the worst case. 
* Space complexity : $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Solution explained
- Author: jmnarloch
- Creation Date: Sat May 23 2015 21:13:12 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 08:04:32 GMT+0800 (Singapore Standard Time)

<p>
This problem is well known and quite often can be found in various text books.

You can take a couple of approaches to actually solve it:

- O(N lg N) running time + O(1) memory

The simplest approach is to sort the entire input array and then access the element by it's index (which is O(1)) operation:

----------

    public int findKthLargest(int[] nums, int k) {
            final int N = nums.length;
            Arrays.sort(nums);
            return nums[N - k];
    }

----------

- O(N lg K) running time + O(K) memory

Other possibility is to use a min oriented priority queue that will store the K-th largest values. The algorithm iterates over the whole input and maintains the size of priority queue.

----------

    public int findKthLargest(int[] nums, int k) {

        final PriorityQueue<Integer> pq = new PriorityQueue<>();
        for(int val : nums) {
            pq.offer(val);

            if(pq.size() > k) {
                pq.poll();
            }
        }
        return pq.peek();
    }

----------

- O(N) best case / O(N^2) worst case running time + O(1) memory

The smart approach for this problem is to use the selection algorithm (based on the partion method - the same one as used in quicksort).


----------

    public int findKthLargest(int[] nums, int k) {
    
            k = nums.length - k;
            int lo = 0;
            int hi = nums.length - 1;
            while (lo < hi) {
                final int j = partition(nums, lo, hi);
                if(j < k) {
                    lo = j + 1;
                } else if (j > k) {
                    hi = j - 1;
                } else {
                    break;
                }
            }
            return nums[k];
        }
    
        private int partition(int[] a, int lo, int hi) {
    
            int i = lo;
            int j = hi + 1;
            while(true) {
                while(i < hi && less(a[++i], a[lo]));
                while(j > lo && less(a[lo], a[--j]));
                if(i >= j) {
                    break;
                }
                exch(a, i, j);
            }
            exch(a, lo, j);
            return j;
        }
    
        private void exch(int[] a, int i, int j) {
            final int tmp = a[i];
            a[i] = a[j];
            a[j] = tmp;
        }
    
        private boolean less(int v, int w) {
            return v < w;
        }

----------

O(N) guaranteed running time + O(1) space

So how can we improve the above solution and make it O(N) guaranteed? The answer is quite simple, we can randomize the input, so that even when the worst case input would be provided the algorithm wouldn't be affected. So all what it is needed to be done is to shuffle the input.

----------

    public int findKthLargest(int[] nums, int k) {
    
            shuffle(nums);
            k = nums.length - k;
            int lo = 0;
            int hi = nums.length - 1;
            while (lo < hi) {
                final int j = partition(nums, lo, hi);
                if(j < k) {
                    lo = j + 1;
                } else if (j > k) {
                    hi = j - 1;
                } else {
                    break;
                }
            }
            return nums[k];
        }
    
    private void shuffle(int a[]) {
    
            final Random random = new Random();
            for(int ind = 1; ind < a.length; ind++) {
                final int r = random.nextInt(ind + 1);
                exch(a, ind, r);
            }
        }

----------

There is also worth mentioning the Blum-Floyd-Pratt-Rivest-Tarjan algorithm that has a guaranteed O(N) running time.
</p>


### C++ STL, partition and heapsort
- Author: jianchao-li
- Creation Date: Tue Jun 02 2015 12:22:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 04:58:50 GMT+0800 (Singapore Standard Time)

<p>
#### STL

This problem needs to use partial sorting. In STL, there are two built-in functions (`nth_element` and `partial_sort`) for this.

```cpp
class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        nth_element(nums.begin(), nums.begin() + k - 1, nums.end(), greater<int>());
        return nums[k - 1];
    }
};
```

```cpp
class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        partial_sort(nums.begin(), nums.begin() + k, nums.end(), greater<int>());
        return nums[k - 1];
    }
};
```

Note the off-by-1 difference in the second argument of the two built-in functions.

We may also use a heap to solve this problem. We can maintain the largest `k` elements in a heap with the smallest among them at the top. Or we can add all the elements to a heap, with the largest at the top, and then pop the heap for `k - 1` times, then the one on the top is our target. The first one is min-heap and the second one is max-heap. In STL, both `priority_queue` and `multiset` can be used as a min/max-heap.

**min-heap using `priority_queue`**

```cpp
class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        priority_queue<int, vector<int>, greater<int>> pq;
        for (int num : nums) {
            pq.push(num);
            if (pq.size() > k) {
                pq.pop();
            }
        }
        return pq.top();
    }
};
```

**max-heap using `priority_queue`**

```cpp
class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        priority_queue<int> pq(nums.begin(), nums.end());
        for (int i = 0; i < k - 1; i++) {
            pq.pop();
        }
        return pq.top();
    }
};
```

**min-heap using `multiset`**

```cpp
class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        multiset<int> mset;
        for (int num : nums) {
            mset.insert(num);
            if (mset.size() > k) {
                mset.erase(mset.begin());
            }
        }
        return *mset.begin();
    }
};
```

**max-heap using `multiset`**

```cpp
class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        multiset<int, greater<int>> mset(nums.begin(), nums.end());
        for (int i = 0; i < k - 1; i++) {
            mset.erase(mset.begin());
        }
        return *mset.begin();
    }
};
```

#### Partition

The partition subroutine of quicksort can also be used to solve this problem. In `partition`, we divide the array into

```
elements>=pivot pivot elements<=pivot
```

Then, according to the index of `pivot`, we will know whther the `k`th largest element is to the left or right of `pivot` or just itself.

In average, this algorithm reduces the size of the problem by approximately one half after each partition, giving the recurrence `T(n) = T(n/2) + O(n)` with `O(n)` being the time for partition. The solution is `T(n) = O(n)`, which means we have found an average linear-time solution. However, in the worst case, the recurrence will become `T(n) = T(n - 1) + O(n)` and `T(n) = O(n^2)`.

```cpp
class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        int left = 0, right = nums.size() - 1, kth;
        while (true) {
            int idx = partition(nums, left, right);
            if (idx == k - 1) {
                kth = nums[idx];
                break;
            }
            if (idx < k - 1) {
                left = idx + 1;
            } else {
                right = idx - 1;
            }
        }
        return kth;
    }
private:
    int partition(vector<int>& nums, int left, int right) {
        int pivot = nums[left], l = left + 1, r = right;
        while (l <= r) {
            if (nums[l] < pivot && nums[r] > pivot) {
                swap(nums[l++], nums[r--]);
            }
            if (nums[l] >= pivot) {
                l++;
            }
            if (nums[r] <= pivot) {
                r--;
            }
        }
        swap(nums[left], nums[r]);
        return r;
    }
};
```

#### Heapsort

In the above we have presented heap solutions using STL. You may also implement your own heap if you are interested. I suggest you to read the Heapsort chapter of Introduction to Algorithms if you are not familiar with it. The following code implements a max-heap.

```cpp
class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        buildMaxHeap(nums);
        for (int i = 0; i < k - 1; i++) {
            swap(nums[0], nums[--heapSize]);
            maxHeapify(nums, 0);
        }
        return nums[0];
    }
private:
    int heapSize;
    
    int left(int i) {
        return (i << 1) + 1;
    }
    
    int right(int i) {
        return (i << 1) + 2;
    }
    
    void maxHeapify(vector<int>& nums, int i) {
        int largest = i, l = left(i), r = right(i);
        if (l < heapSize && nums[l] > nums[largest]) {
            largest = l;
        }
        if (r < heapSize && nums[r] > nums[largest]) {
            largest = r;
        }
        if (largest != i) {
            swap(nums[i], nums[largest]);
            maxHeapify(nums, largest);
        }
    }
    
    void buildMaxHeap(vector<int>& nums) {
        heapSize = nums.size();
        for (int i = (heapSize >> 1) - 1; i >= 0; i--) {
            maxHeapify(nums, i);
        }
    }
};
```
</p>


### AC Clean QuickSelect Java solution avg. O(n) time
- Author: jeantimex
- Creation Date: Tue Jul 14 2015 08:36:33 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 14 2019 13:47:17 GMT+0800 (Singapore Standard Time)

<p>
https://en.wikipedia.org/wiki/Quickselect

```
public int findKthLargest(int[] nums, int k) {
  return quickSelect(nums, 0, nums.length - 1, k);
}

int quickSelect(int[] nums, int low, int high, int k) {
  int pivot = low;

  // use quick sort\'s idea
  // put nums that are <= pivot to the left
  // put nums that are  > pivot to the right
  for (int j = low; j < high; j++) {
    if (nums[j] <= nums[high]) {
      swap(nums, pivot++, j);
    }
  }
  swap(nums, pivot, high);
  
  // count the nums that are > pivot from high
  int count = high - pivot + 1;
  // pivot is the one!
  if (count == k) return nums[pivot];
  // pivot is too small, so it must be on the right
  if (count > k) return quickSelect(nums, pivot + 1, high, k);
  // pivot is too big, so it must be on the left
  return quickSelect(nums, low, pivot - 1, k - count);
}
```
</p>


