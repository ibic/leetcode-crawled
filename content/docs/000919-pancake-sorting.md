---
title: "Pancake Sorting"
weight: 919
#id: "pancake-sorting"
---
## Description
<div class="description">
<p>Given an array of integers&nbsp;<code>arr</code>, sort the array by performing a series of <strong>pancake flips</strong>.</p>

<p>In one pancake flip we do the following steps:</p>

<ul>
	<li>Choose an integer <code>k</code> where <code>1 &lt;= k &lt;= arr.length</code>.</li>
	<li>Reverse the sub-array <code>arr[1...k]</code>.</li>
</ul>

<p>For example, if <code>arr = [3,2,1,4]</code> and we performed a pancake flip choosing <code>k = 3</code>, we reverse the sub-array <code>[3,2,1]</code>, so <code>arr = [<strong>1,2,3</strong>,4]</code> after the pancake flip at <code>k = 3</code>.</p>

<p>Return <em>the <code>k</code>-values corresponding to a sequence of pancake flips that sort <code>arr</code></em>.&nbsp;Any valid answer that sorts the array within <code>10 * arr.length</code> flips will be judged as correct.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [3,2,4,1]
<strong>Output:</strong> [4,2,4,3]
<strong>Explanation: </strong>
We perform 4 pancake flips, with k values 4, 2, 4, and 3.
Starting state: arr = [3, 2, 4, 1]
After 1st flip (k = 4): arr = [<strong>1, 4, 2, 3</strong>]
After 2nd flip (k = 2): arr = [<strong>4, 1</strong>, 2, 3]
After 3rd flip (k = 4): arr = [<strong>3, 2, 1, 4</strong>]
After 4th flip (k = 3): arr = [<strong>1, 2, 3</strong>, 4], which is sorted.
Notice that we return an array of the chosen k values of the pancake flips.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,3]
<strong>Output:</strong> []
<strong>Explanation: </strong>The input is already sorted, so there is no need to flip anything.
Note that other answers, such as [3, 3], would also be accepted.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 100</code></li>
	<li><code>1 &lt;= arr[i] &lt;= arr.length</code></li>
	<li>All integers in <code>arr</code> are unique (i.e. <code>arr</code> is a permutation of the integers from <code>1</code> to <code>arr.length</code>).</li>
</ul>

</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Square - 2 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Sort like Bubble-Sort

**Intuition**

One might argue that this is an awkward question to do things.
Indeed, it is not the most practical operation that one can have with the _pancake flipping_, in order to sort a list.

However awkward the problem might be, it is the game that we play with. And in order to win the game, we have to play by the rules.
Actually, from this perspective, this problem does share some similarity with the **_[Rubik's cube](https://en.wikipedia.org/wiki/Rubik%27s_Cube)_**, _i.e._ one cannot move one tile without moving other tiles along with.
Let us get on with it, by playing a few rounds ourselves to get the hang of the problem.

Given the input of `[3, 2, 4, 1]`, the desired sorted output would be `[1, 2, 3, 4]`. 

As a reminder, the only operation that we could perform in order to move the elements in the list, is the so-called _pancake flip_, which is to reverse a _prefix_ of the list.

Starting from the largest value in the list, _i.e._ `4` in the example, its desired position would be the tail of the list.
While in the input, it is located at the third of the list, if we look at the list from left to right.

In order to move the value of `4` to its desired position, we could perform the following two steps:

- Firstly, we do the pancake flip on the prefix of `[3, 2, 4]`. With this operation, we then move the value `4` to the _**head**_ of the updated list as `[4, 2, 3, 1]`.

![flip to head](../Figures/969/969_flip_head.png)

- Now that, the value `4` is located at the head of the list, we could now perform another pancake flip on the entire list, which would get us the list of `[1, 3, 2, 4]`.

![flip to tail](../Figures/969/969_flip_tail.png)

Voila. With the obtained list of `[1, 3, 2, 4]`, we are now one step closer to our final goal, with the value `4` now at its proper place.
For the following steps, we only need to focus on the sublist of `[1, 3, 2]`.

>If one looks over the above steps again, it might ring a bell to a well-known algorithm called _**[bubble sort](https://en.wikipedia.org/wiki/Bubble_sort)**_.

<p align="center">
<img src="https://upload.wikimedia.org/wikipedia/commons/3/37/Bubble_sort_animation.gif">
</p>


Indeed, we share the same strategy as the bubble sort, by _sinking_ the numbers to the bottom one by one.

>Here we can make a statement that for any given number, in order to move it to any desired position, it takes **_at most_** two pancake flips to do so.

The idea is simple. First we move the number to the head of the list, then we can switch it with any other element by performing another pancake flip.


**Algorithm**

One can inspire from the bubble sort to implement the algorithm.

- First of all, we implement a function called `flip(list, k)`, which performs the pancake flip on the prefix of `list[0:k]` (in Python).

- The main algorithm runs a loop over the values of the list, starting from the largest one.

    - At each round, we identify the value to sort (named as `value_to_sort`), which is the number we would put in place at this round.

    - We then locate the index of the `value_to_sort`.

    - If the `value_to_sort` is not at its place already, we can then perform _at most_ two pancake flips as we explained in the intuition.

    - At the end of the round, the `value_to_sort` would be put in place.


<iframe src="https://leetcode.com/playground/EFVjQPCm/shared" frameBorder="0" width="100%" height="500" name="EFVjQPCm"></iframe>



**Complexity Analysis**

Let $$N$$ be the length of the input list.

- Time Complexity: $$\mathcal{O}(N^2)$$

    - In the algorithm, we run a loop with $$N$$ iterations.

    - Within each iteration, we are dealing with the corresponding prefix of the list.
    Here we denote the length of the prefix as $$k$$, _e.g._ in the first iteration, the length of the prefix is $$N$$. While in the second iteration, the length of the prefix is $$N-1$$. 

    - Within each iteration, we have operations whose time complexity is linear to the length of the prefix, such as iterating through the prefix to find the index, or flipping the entire prefix _etc._ Hence, for each iteration, its time complexity would be $$\mathcal{O}(k)$$

    - To sum up all iterations, we have the overall time complexity of the algorithm as $$\sum_{k=1}^{N} \mathcal{O}(k) = \mathcal{O}(N^2)$$.


- Space Complexity: $$\mathcal{O}(N)$$

    - Within the algorithm, we use a list to maintain the final results, which is proportional to the number of pancake flips.

    - For each round of iteration, at most we would add two pancake flips. Therefore, the maximal number of pancake flips needed would be $$2\cdot N$$.

    - As a result, the space complexity of the algorithm is $$\mathcal{O}(N)$$. If one does not take into account the space required to hold the result of the function, then one could consider the above algorithm as a constant space solution.


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Jan 06 2019 12:09:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 06 2019 12:09:21 GMT+0800 (Singapore Standard Time)

<p>
**Explanation**
Find the index `i` of the next maximum number `x`.
Reverse `i + 1` numbers, so that the `x` will be at `A[0]`
Reverse `x` numbers, so that `x` will be at `A[x - 1]`.
Repeat this process `N` times.

**Update:**
Actually, I didn\'t use the condition permutation of `[1,2,..., A.length]`.
I searched in the descending order of `A`.

**Time Complexity**:
O(N^2)

<br>

**Java:**
```
    public List<Integer> pancakeSort(int[] A) {
        List<Integer> res = new ArrayList<>();
        for (int x = A.length, i; x > 0; --x) {
            for (i = 0; A[i] != x; ++i);
            reverse(A, i + 1);
            res.add(i + 1);
            reverse(A, x);
            res.add(x);
        }
        return res;
    }

    public void reverse(int[] A, int k) {
        for (int i = 0, j = k - 1; i < j; i++, j--) {
            int tmp = A[i];
            A[i] = A[j];
            A[j] = tmp;
        }
    }
```

**C++:**
```
    vector<int> pancakeSort(vector<int> A) {
        vector<int> res;
        int x,i;
        for (x = A.size(); x > 0; --x) {
            for (i = 0; A[i] != x; ++i);
            reverse(A.begin(), A.begin() + i + 1);
            res.push_back(i + 1);
            reverse(A.begin(), A.begin() + x);
            res.push_back(x);
        }
        return res;
    }
```

**Python:**
```
    def pancakeSort(self, A):
        res = []
        for x in range(len(A), 1, -1):
            i = A.index(x)
            res.extend([i + 1, x])
            A = A[:i:-1] + A[:i]
        return res
```

</p>


### Error in question specification
- Author: gomorycut
- Creation Date: Sat Aug 29 2020 16:16:38 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 02 2020 12:39:44 GMT+0800 (Singapore Standard Time)

<p>
The question states:
```
For example, if A = [3,2,1,4] and we performed a pancake flip choosing k = 2, we reverse the sub-array [3,2,1], so A = [1,2,3,4] after the pancake flip at k = 2.

Example 1:

Input: A = [3,2,4,1]
Output: [4,2,4,3]
```

In the description is says that k=2 flips the subarray [0,1,2] but in the Example1 given, k=4 would be out of bounds. The k-values it expects in the answer checker are for a 1-indexed array, or equivalently, k= the number of values (from the left side) to flip. 

The specs would say that k=1 would flip the values at indices [0,1] but in the example, k=1 would not flip anything (one element flip).

Kind of poor to have an error in a question when used in the August challenge.
</p>


### Java flip the largest number to the tail
- Author: wangzi6147
- Creation Date: Sun Jan 06 2019 12:05:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 06 2019 12:05:13 GMT+0800 (Singapore Standard Time)

<p>
1. Find the largest number
2. Flip twice to the tail

Time: O(N^2)
Flips: 2*N

```
class Solution {
    public List<Integer> pancakeSort(int[] A) {
        List<Integer> result = new ArrayList<>();
        int n = A.length, largest = n;
        for (int i = 0; i < n; i++) {
            int index = find(A, largest);
            flip(A, index);
            flip(A, largest - 1);
            result.add(index + 1);
            result.add(largest--);
        }
        return result;
    }
    private int find(int[] A, int target) {
        for (int i = 0; i < A.length; i++) {
            if (A[i] == target) {
                return i;
            }
        }
        return -1;
    }
    private void flip(int[] A, int index) {
        int i = 0, j = index;
        while (i < j) {
            int temp = A[i];
            A[i++] = A[j];
            A[j--] = temp;
        }
    }
}
```
</p>


