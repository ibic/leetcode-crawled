---
title: "Card Flipping Game"
weight: 762
#id: "card-flipping-game"
---
## Description
<div class="description">
<p>On a table are <code>N</code> cards, with a positive integer printed on the front and back of each card (possibly different).</p>

<p>We flip any number of cards, and after we choose one&nbsp;card.&nbsp;</p>

<p>If the number <code>X</code> on the back of the chosen&nbsp;card is not on the front of any card, then this number X is good.</p>

<p>What is the smallest number that is good?&nbsp; If no number is good, output <code>0</code>.</p>

<p>Here, <code>fronts[i]</code> and <code>backs[i]</code> represent the number on the front and back of card <code>i</code>.&nbsp;</p>

<p>A&nbsp;flip swaps the front and back numbers, so the value on the front is now on the back and vice versa.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> fronts = [1,2,4,4,7], backs = [1,3,4,1,3]
<strong>Output:</strong> <code>2</code>
<strong>Explanation:</strong> If we flip the second card, the fronts are <code>[1,3,4,4,7]</code> and the backs are <code>[1,2,4,1,3]</code>.
We choose the second card, which has number 2 on the back, and it isn&#39;t on the front of any card, so <code>2</code> is good.</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= fronts.length == backs.length&nbsp;&lt;=&nbsp;1000</code>.</li>
	<li><code>1 &lt;=&nbsp;fronts[i]&nbsp;&lt;= 2000</code>.</li>
	<li><code>1 &lt;= backs[i]&nbsp;&lt;= 2000</code>.</li>
</ol>

</div>

## Tags


## Companies


## Official Solution
[TOC]

---
#### Approach #1: Hash Set [Accepted]

**Intuition**

If a card has the same value `x` on the front and back, it is impossible to win with `x`.  Otherwise, it has two different values, and if we win with `x`, we can put `x` face down on the rest of the cards.

**Algorithm**

Remember all values `same` that occur twice on a single card.  Then for every value `x` on any card that isn't in `same`, `x` is a candidate answer.  If we have no candidate answers, the final answer is zero.

<iframe src="https://leetcode.com/playground/p7LpC6py/shared" frameBorder="0" width="100%" height="378" name="p7LpC6py"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `fronts` (and `backs`).  We scan through the arrays.

* Space Complexity: $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Problem statement is so confusing!
- Author: shankark
- Creation Date: Tue May 01 2018 14:31:49 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 10:43:45 GMT+0800 (Singapore Standard Time)

<p>
I dont know if its just me - but the problem statement is so confusing. Especially  "is not on the front of any card,"

Even after looking at this forum and accepted solutions - the problem statement is still not making sense.

+1 for down vote.
</p>


### [C++/Java/Python] Easy and Concise with Explanation
- Author: lee215
- Creation Date: Sun Apr 22 2018 11:02:31 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 11:49:44 GMT+0800 (Singapore Standard Time)

<p>
If ```fronts[i] == backs[i]```, it means that `fronts[i]` is sure to appear on the table, no matter how we flap this card.
In case that `fronts[i]` and `backs[i]` are same, `fronts[i]` become impossible to be good number, so I add it to a set `same`.

If ```fronts[i] != backs[i]```,  we can always hide either number by flapping it to back.

Then loop on all numbers and return the minimum.

C++
```
    int flipgame(vector<int>& f, vector<int>& b) {
        unordered_set<int> same;
        for (int i = 0; i < f.size(); ++i) if (f[i] == b[i]) same.insert(f[i]);
        int res = 3000;
        for (int & i : f) if (same.count(i) == 0) res = min(res, i);
        for (int & i : b) if (same.count(i) == 0) res = min(res, i);
        return res % 3000;
    }
```
Java
```
    public int flipgame(int[] f, int[] b) {
        HashSet<Integer> same = new HashSet<>();
        for (int i = 0; i < f.length; ++i) if (f[i] == b[i]) same.add(f[i]);
        int res = 3000;
        for (int i : f) if (!same.contains(i)) res = Math.min(res, i);
        for (int i : b) if (!same.contains(i)) res = Math.min(res, i);
        return res % 3000;
    }
```
Python
```
    def flipgame(self, f, b):
        same = {x for x, y in zip(f, b) if x == y}
        return min([i for i in f + b if i not in same] or [0])
```
</p>


### Java solution using HashSet (The description in the problem really confuses me...)
- Author: wangzi6147
- Creation Date: Sun Apr 22 2018 11:08:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 11:30:35 GMT+0800 (Singapore Standard Time)

<p>
At the first glance I even don\'t know what is this game going to do. It takes me a lot of time to figure out what does the description mean. Finally I realize it is a simple problem using HashSet.

The intuition behind my solution is: If a card has the same number both on the front side and the back side, then this number is fixed, which means this number will never be a good number. On the other hand, for other numbers which are not fixed, after several flips, we could always find a way to flip this number to the front when currently this number is not showing.
**The problem\'s meaning is if you could find a way to flip it up when currently this number is not showing on any of the card(including the chosen one), then this number could be considered as a good number.**

We check whether the number is in the hashset because if a number is in the hashset, this number will always face to us. Make sense?

Consider an example: `fronts = [1,2,2,4,2], backs = [1,3,4,1,3]`, in this case, hashset will only contains a number `1`.
After we build up our hashset, we check the fronts number and backs number one by one. When we encounter `1`, it has been in the hashset, which means it will never be a good number because there will always be a `1` faces to us.
However, for other numbers, which are not presented in the hashset, we could always find a way to filp it up as a good number. i.e. for number `2`, firstly we flip the `second`, `third`, `fifth` card so that all the `2`s are in the back side. Now the cards become `[1, 3, 4, 4, 3] and [1, 2, 2, 1, 2]` Then we flip any of `second`, `third`, `fifth`. At this time, `2` will be considered as a good number. Since the 2 is the minimum valid good number. It will be the result.

```
class Solution {
    public int flipgame(int[] fronts, int[] backs) {
        Set<Integer> set = new HashSet<>();
        int n = fronts.length;
        for (int i = 0; i < n; i++) {
            if (fronts[i] == backs[i]) {
                set.add(fronts[i]);
            }
        }
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            if (!set.contains(backs[i])) {
                min = Math.min(min, backs[i]);
            }
            if (!set.contains(fronts[i])) {
                min = Math.min(min, fronts[i]);
            }
        }
        return min == Integer.MAX_VALUE ? 0 : min;
    }
}
```
</p>


