---
title: "Get Highest Answer Rate Question"
weight: 1488
#id: "get-highest-answer-rate-question"
---
## Description
<div class="description">
<p>Get the highest answer rate question from a table <code>survey_log</code> with these columns: <b>id</b>, <b>action</b>, <b>question_id</b>, <b>answer_id</b>, <b>q_num</b>, <b>timestamp</b>.</p>

<p>id means user id; action has these kind of values: &quot;show&quot;, &quot;answer&quot;, &quot;skip&quot;; answer_id is not null when action column is &quot;answer&quot;, while is null for &quot;show&quot; and &quot;skip&quot;; q_num is the numeral order of the question in current session.</p>

<p>Write a sql query to identify the question which has the highest answer rate.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b>
+------+-----------+--------------+------------+-----------+------------+
| id   | action    | question_id  | answer_id  | q_num     | timestamp  |
+------+-----------+--------------+------------+-----------+------------+
| 5    | show      | 285          | null       | 1         | 123        |
| 5    | answer    | 285          | 124124     | 1         | 124        |
| 5    | show      | 369          | null       | 2         | 125        |
| 5    | skip      | 369          | null       | 2         | 126        |
+------+-----------+--------------+------------+-----------+------------+
<b>Output:</b>
+-------------+
| survey_log  |
+-------------+
|    285      |
+-------------+
<b>Explanation:</b>
question 285 has answer rate 1/1, while question 369 has 0/1 answer rate, so output 285.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b> The highest answer rate meaning is: answer number&#39;s ratio in show number in the same question.</p>

</div>

## Tags


## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach I: Using sub-query and `SUM()` function [Accepted]

**Intuition**

Calculate the answered times / show times for each question.

**Algorithm**

First, we can use `SUM()` to get the total number of answered times as well as the show times for each question using a sub-query as below.

```sql
SELECT
    question_id,
    SUM(CASE
        WHEN action = 'answer' THEN 1
        ELSE 0
    END) AS num_answer,
    SUM(CASE
        WHEN action = 'show' THEN 1
        ELSE 0
    END) AS num_show
FROM
    survey_log
GROUP BY question_id
;
```

```
| question_id | num_answer | num_show |
|-------------|------------|----------|
| 285         | 1          | 1        |
| 369         | 0          | 1        |
```

Then we can calculate the answer rate by its definition.

**MySQL**

```sql
SELECT question_id as survey_log
FROM
(
	SELECT question_id,
         SUM(case when action="answer" THEN 1 ELSE 0 END) as num_answer,
        SUM(case when action="show" THEN 1 ELSE 0 END) as num_show,    
	FROM survey_log
	GROUP BY question_id
) as tbl
ORDER BY (num_answer / num_show) DESC
LIMIT 1
```

#### Approach II: Using sub-query and `COUNT(IF...)` function [Accepted]

**Algorithm**

This solution is very straight forward: use the `COUNT()` function to sum the answer and show time combining with the `IF()` function.

**MySQL**
```sql
SELECT 
    question_id AS 'survey_log'
FROM
    survey_log
GROUP BY question_id
ORDER BY COUNT(answer_id) / COUNT(IF(action = 'show', 1, 0)) DESC
LIMIT 1;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My AC solution
- Author: Douglas1612
- Creation Date: Tue May 09 2017 08:49:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 08:48:52 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT question_id as survey_log
FROM
(
	SELECT question_id, SUM(case when action="show" THEN 1 ELSE 0 END) as num_show,    SUM(case when action="answer" THEN 1 ELSE 0 END) as num_answer
	FROM survey_log
	GROUP BY question_id
) as tbl
ORDER BY (num_answer / num_show) DESC LIMIT 1
```
</p>


### what if there are more than 1 questions with highest answer rate?
- Author: zhizirachel
- Creation Date: Thu Nov 21 2019 12:29:36 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Nov 21 2019 12:29:36 GMT+0800 (Singapore Standard Time)

<p>
I\'ve found many of the approach use limit 1
what if there are more than 1 questions with highest answer rate?
</p>


### Tips to ace all test cases
- Author: 495801738
- Creation Date: Mon Dec 04 2017 03:45:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 15 2018 02:07:21 GMT+0800 (Singapore Standard Time)

<p>
As many of you may know that, the test case is generated in random, meaning there could be cases that are not logical. Here are some tips and explanations to ace these test cases.
First, count answered question by "answer_id" column, not by "action" column. According to my tests, counting action = "answer" is not reliable.
Second, never try to return multiple results even there is a tie. The judge only accept one answer.
Here are some sample AC codes:
1. Using a subquery
```
SELECT question_id as survey_log
FROM
(
SELECT question_id,
       count(answer_id) as num_answer,
       SUM(case when action="show" THEN 1 ELSE 0 END) as num_show    
FROM survey_log
GROUP BY question_id
) as tbl
ORDER BY (num_answer / num_show) DESC
LIMIT 1
```
2. Using a view
```
create or replace view tbl as (
select question_id, count(answer_id) as num_answer, sum(if(action="show", 1, 0)) as num_show
from survey_log
group by question_id
);
    
select question_id as survey_log
from tbl
order by num_answer / num_show desc
limit 1
# where num_answer / num_show = (
#     select num_answer / num_show 
#     from tbl 
#     order by num_answer / num_show desc
#     limit 1
# )
```
You may notice I commented out some parts in the end because I was trying to return multiple results if there is a tie. It could most likely be a follow-up. But unfortunately, this question does not accept this even if there is a tie.
3. Another concise version
```
select question_id as survey_log
from survey_log
group by question_id
order by count(answer_id) / count(case when action="show" then action else null end) desc 
limit 1
```
The common parts in these three solutions is that they all count answer_id instead of action, and only return one answer. Hope they help.
</p>


