---
title: "Insert Interval"
weight: 57
#id: "insert-interval"
---
## Description
<div class="description">
<p>Given a set of <em>non-overlapping</em> intervals, insert a new interval into the intervals (merge if necessary).</p>

<p>You may assume that the intervals were initially sorted according to their start times.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[1,3],[6,9]], newInterval = [2,5]
<strong>Output:</strong> [[1,5],[6,9]]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
<strong>Output:</strong> [[1,2],[3,10],[12,16]]
<strong>Explanation:</strong> Because the new interval <code>[4,8]</code> overlaps with <code>[3,5],[6,7],[8,10]</code>.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> intervals = [], newInterval = [5,7]
<strong>Output:</strong> [[5,7]]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[1,5]], newInterval = [2,3]
<strong>Output:</strong> [[1,5]]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[1,5]], newInterval = [2,7]
<strong>Output:</strong> [[1,7]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= intervals.length &lt;= 10<sup>4</sup></code></li>
	<li><code>intervals[i].length == 2</code></li>
	<li><code>0 &lt;=&nbsp;intervals[i][0] &lt;=&nbsp;intervals[i][1] &lt;= 10<sup>5</sup></code></li>
	<li><code>intervals</code>&nbsp;is sorted by <code>intervals[i][0]</code> in <strong>ascending</strong>&nbsp;order.</li>
	<li><code>newInterval.length == 2</code></li>
	<li><code>0 &lt;=&nbsp;newInterval[0] &lt;=&nbsp;newInterval[1] &lt;= 10<sup>5</sup></code></li>
</ul>

</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Amazon - 9 (taggedByAdmin: false)
- Google - 8 (taggedByAdmin: true)
- Facebook - 6 (taggedByAdmin: true)
- Twitter - 3 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- LinkedIn - 6 (taggedByAdmin: true)
- Uber - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Dataminr - 2 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Greedy.

**Greedy algorithms**

Greedy problems usually look like 
"Find minimum number of _something_ to do _something_" or 
"Find maximum number of _something_ to fit in _some conditions_", 
and typically propose an unsorted input.

> The idea of greedy algorithm is to pick the _locally_
optimal move at each step, that will lead to the _globally_ optimal solution.

The standard solution has $$\mathcal{O}(N \log N)$$ time complexity and consists of two parts:

- Figure out how to sort the input data ($$\mathcal{O}(N \log N)$$ time).
That could be done directly by a sorting or indirectly by a heap usage. 
Typically sort is better than the heap usage because of gain in space.

- Parse the sorted input to have a solution ($$\mathcal{O}(N)$$ time). 

Please notice that in case of well-sorted input one doesn't need the first 
part and the greedy solution could have $$\mathcal{O}(N)$$ time complexity,
[here is an example](https://leetcode.com/articles/gas-station/).

> How to prove that your greedy algorithm provides globally optimal solution?

Usually you could use the [proof by contradiction](https://en.wikipedia.org/wiki/Proof_by_contradiction). 

**Intuition**

Here we have an example of a greedy problem with a well-sorted input,
and hence the algorithm time complexity should be 
$$\mathcal{O}(N)$$.

Let's consider the following intervals

![bla](../Figures/57/intervals.png)

The straightforward one-pass strategy could be implemented in three steps.

1 . Add to the output all the intervals starting before `newInterval`.

![bla](../Figures/57/step1_new.png)

2 . Add to the output `newInterval`, merge it with the last added
interval if needed.

![bla](../Figures/57/step2_new.png)

3 . Add the next intervals one by one, merge if needed.

![bla](../Figures/57/step33.png)

Basically, the same strategy [as here](https://leetcode.com/articles/merge-intervals/),
with an additional care to add the new interval in its proper
position in order not to destroy the well-sorted input. 

**Algorithm**

Here is the algorithm :

- Add to the output all the intervals starting before `newInterval`.

- Add to the output `newInterval`. Merge it with the last added
interval if `newInterval` starts before the
last added interval.

- Add the next intervals one by one. Merge with the last added
interval if the current interval starts before the
last added interval.

**Implementation**

<iframe src="https://leetcode.com/playground/r6ABqTx8/shared" frameBorder="0" width="100%" height="500" name="r6ABqTx8"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since it's one pass along
the input array.
 
* Space complexity : $$\mathcal{O}(N)$$ to keep the output.

## Accepted Submission (java)
```java
class Solution {
//  private int findPos(List<Integer> list, int num) {
//    int l = 0;
//    int r = list.size() - 1;
//    while (l <= r) {
//      int mid = (l + r) / 2;
//      if (num >= list.get(mid) && num <= list.get(mid + 1)) {
//        return mid;
//      } else if (num < list.get(mid)) {
//        r = mid - 1;
//      } else {
//        l = mid + 1;
//      }
//    }
//    // shouldn't reach
//    return 0;
//  }
//
//  public int[][] insert(int[][] intervals, int[] newInterval) {
//    List<Integer> intervalPoints = new ArrayList<>();
//    intervalPoints.add(Integer.MIN_VALUE);
//    for (int[] interval : intervals) {
//      intervalPoints.add(interval[0]);
//      intervalPoints.add(interval[1]);
//    }
//    intervalPoints.add(Integer.MAX_VALUE);
//    int start = newInterval[0];
//    int stop = newInterval[1];
//    int startPos = findPos(intervalPoints, start);
//    int stopPos = findPos(intervalPoints, stop);
//    List<Integer> merged = new ArrayList<>();
//    if (startPos == 0) {
//      startPos = 1;
//    } else if (startPos % 2 == 0) { // gap
//      if (start == intervalPoints.get(startPos)) {
//        startPos--;
//        start = intervalPoints.get(startPos);
//      } else {
//        startPos++;
//      }
//    } else { // interval
//      start = intervalPoints.get(startPos);
//    }
//
//    stopPos++;
//    if (stopPos % 2 == 1) { // gap
//      if (stop == intervalPoints.get(stopPos)) {
//        stop = intervalPoints.get(stopPos + 1);
//        stopPos += 2;
//      }
//    } else { // interval
//      stop = intervalPoints.get(stopPos);
//      stopPos++;
//    }
//
//    List<Integer> listOfPoints = new ArrayList<>();
//    listOfPoints.addAll(intervalPoints.subList(0, startPos));
//
//    List<Integer> newIntervalList = new ArrayList<>();
//    newIntervalList.add(start);
//    newIntervalList.add(stop);
//    listOfPoints.addAll(newIntervalList);
//
//    List<Integer> tail = intervalPoints.subList(stopPos, intervalPoints.size());
//    listOfPoints.addAll(tail);
//    //        MyUtil.printList(intervalPoints);
//    //        System.out.println("startPos: " + startPos + ", stopPos: " + stopPos);
//    //        System.out.println("start: " + start + ", stop: " + stop);
//    //        MyUtil.printList(listOfPoints);
//
//    List<int[]> rList = new ArrayList<>();
//    for (int i = 1; i < listOfPoints.size() - 1; i += 2) {
//      rList.add(new int[] {listOfPoints.get(i), listOfPoints.get(i + 1)});
//    }
//    return rList.toArray(new int[][]{});
//  }
  public int[][] insert(int[][] intervals, int[] newInterval) {
    List<int[]> rList = new ArrayList<>();
    int i = 0;
    for (; i < intervals.length && intervals[i][1] < newInterval[0]; i++) {
      rList.add(intervals[i]);
    }
    if (i == intervals.length) {
      rList.add(newInterval);
    } else {
      if (intervals[i][0] < newInterval[0]) {
        newInterval[0] = intervals[i][0];
      }
      for (; i < intervals.length && intervals[i][0] <= newInterval[1]; i++) {
        newInterval[1] = Math.max(intervals[i][1], newInterval[1]);
      }
      rList.add(newInterval);
      for (; i < intervals.length; i++) {
        rList.add(intervals[i]);
      }
    }
    return rList.toArray(new int[][]{});
  }
}

```

## Top Discussions
### Short and straight-forward Java solution
- Author: shpolsky
- Creation Date: Sat Jan 24 2015 01:47:54 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 13:20:02 GMT+0800 (Singapore Standard Time)

<p>
Hi guys!

Here's a pretty straight-forward and concise solution below.

    public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
        List<Interval> result = new LinkedList<>();
        int i = 0;
        // add all the intervals ending before newInterval starts
        while (i < intervals.size() && intervals.get(i).end < newInterval.start)
            result.add(intervals.get(i++));
        // merge all overlapping intervals to one considering newInterval
        while (i < intervals.size() && intervals.get(i).start <= newInterval.end) {
            newInterval = new Interval( // we could mutate newInterval here also
                    Math.min(newInterval.start, intervals.get(i).start),
                    Math.max(newInterval.end, intervals.get(i).end));
            i++;
        }
        result.add(newInterval); // add the union of intervals we got
        // add all the rest
        while (i < intervals.size()) result.add(intervals.get(i++)); 
        return result;
    }

Hope it helps.
</p>


### 7+ lines, 3 easy solutions
- Author: StefanPochmann
- Creation Date: Thu Jun 25 2015 03:05:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 02:17:42 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1:** (7 lines, 88 ms)

Collect the intervals strictly left or right of the new interval, then merge the new one with the middle ones (if any) before inserting it between left and right ones.

    def insert(self, intervals, newInterval):
        s, e = newInterval.start, newInterval.end
        left = [i for i in intervals if i.end < s]
        right = [i for i in intervals if i.start > e]
        if left + right != intervals:
            s = min(s, intervals[len(left)].start)
            e = max(e, intervals[~len(right)].end)
        return left + [Interval(s, e)] + right

---

**Solution 2:** (8 lines, 84 ms)

Same algorithm as solution 1, but different implementation with only one pass and explicitly collecting the to-be-merged intervals.

    def insert(self, intervals, newInterval):
        s, e = newInterval.start, newInterval.end
        parts = merge, left, right = [], [], []
        for i in intervals:
            parts[(i.end < s) - (i.start > e)].append(i)
        if merge:
            s = min(s, merge[0].start)
            e = max(e, merge[-1].end)
        return left + [Interval(s, e)] + right

---

**Solution 3:** (11 lines, 80 ms)

Same again, but collect and merge while going over the intervals once.

    def insert(self, intervals, newInterval):
        s, e = newInterval.start, newInterval.end
        left, right = [], []
        for i in intervals:
            if i.end < s:
                left += i,
            elif i.start > e:
                right += i,
            else:
                s = min(s, i.start)
                e = max(e, i.end)
        return left + [Interval(s, e)] + right
</p>


### Easy and clean O(n) C++ solution
- Author: forestsong
- Creation Date: Wed Sep 16 2015 09:57:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 07:09:03 GMT+0800 (Singapore Standard Time)

<p>
Very easy to understand code as follows.

First, put all intervals that are to the left of the inserted interval. 
Second, merge all intervals that intersect with the inserted interval. 
Finally, put all intervals that are to the right of the inserted interval.

That's it! You are done!

    class Solution {
    public:
        vector<Interval> insert(vector<Interval>& intervals, Interval newInterval) {
            vector<Interval> res;
            int index = 0;
            while(index < intervals.size() && intervals[index].end < newInterval.start){
                res.push_back(intervals[index++]);
            }
            while(index < intervals.size() && intervals[index].start <= newInterval.end){
                newInterval.start = min(newInterval.start, intervals[index].start);
                newInterval.end = max(newInterval.end, intervals[index].end);
                index++;
            }
            res.push_back(newInterval);
            while(index < intervals.size()){
                res.push_back(intervals[index++]);
            }
            return res;
        }
    };
</p>


