---
title: "Sparse Matrix Multiplication"
weight: 294
#id: "sparse-matrix-multiplication"
---
## Description
<div class="description">
<p>Given two <a href="https://en.wikipedia.org/wiki/Sparse_matrix" target="_blank">sparse matrices</a> <b>A</b> and <b>B</b>, return the result of <b>AB</b>.</p>

<p>You may assume that <b>A</b>&#39;s column number is equal to <b>B</b>&#39;s row number.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:

</b><strong>A</strong> = [
  [ 1, 0, 0],
  [-1, 0, 3]
]

<strong>B</strong> = [
  [ 7, 0, 0 ],
  [ 0, 0, 0 ],
  [ 0, 0, 1 ]
]

<strong>Output:</strong>

     |  1 0 0 |   | 7 0 0 |   |  7 0 0 |
<b>AB</b> = | -1 0 3 | x | 0 0 0 | = | -7 0 3 |
                  | 0 0 1 |
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= A.length, B.length &lt;= 100</code></li>
	<li><code>1 &lt;= A[i].length, B[i].length &lt;= 100</code></li>
	<li><code>-100 &lt;= A[i][j], B[i][j]&nbsp;&lt;= 100</code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Facebook - 13 (taggedByAdmin: true)
- Apple - 4 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- LinkedIn - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easiest JAVA solution
- Author: yavinci
- Creation Date: Sat Nov 28 2015 03:47:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 19:34:03 GMT+0800 (Singapore Standard Time)

<p>

UPDATE: Thanks to @stpeterh we have this `70ms` concise solution:
<hr>


    public class Solution {
        public int[][] multiply(int[][] A, int[][] B) {
            int m = A.length, n = A[0].length, nB = B[0].length;
            int[][] C = new int[m][nB];
    
            for(int i = 0; i < m; i++) {
                for(int k = 0; k < n; k++) {
                    if (A[i][k] != 0) {
                        for (int j = 0; j < nB; j++) {
                            if (B[k][j] != 0) C[i][j] += A[i][k] * B[k][j];
                        }
                    }
                }
            }
            return C;   
        }
    }

<hr>

The followings is the original `75ms` solution:

The idea is derived from [a CMU lecture.][1]

>A sparse matrix can be represented as a sequence of rows, each of which is a sequence of (column-number, value) pairs of the nonzero values in the row.

So let's create a non-zero array for A, and do multiplication on B. 

Hope it helps!

<hr>


    public int[][] multiply(int[][] A, int[][] B) {
        int m = A.length, n = A[0].length, nB = B[0].length;
        int[][] result = new int[m][nB];
    
        List[] indexA = new List[m];
        for(int i = 0; i < m; i++) {
            List<Integer> numsA = new ArrayList<>();
            for(int j = 0; j < n; j++) {
                if(A[i][j] != 0){
                    numsA.add(j); 
                    numsA.add(A[i][j]);
                }
            }
            indexA[i] = numsA;
        }
    
        for(int i = 0; i < m; i++) {
            List<Integer> numsA = indexA[i];
            for(int p = 0; p < numsA.size() - 1; p += 2) {
                int colA = numsA.get(p);
                int valA = numsA.get(p + 1);
                for(int j = 0; j < nB; j ++) {
                    int valB = B[colA][j];
                    result[i][j] += valA * valB;
                }
            }
        }
    
        return result;   
    }


  [1]: http://www.cs.cmu.edu/~scandal/cacm/node9.html
</p>


### [54ms ] Detailed Summary of Easiest JAVA solutions Beating 99.9%
- Author: 10000tb
- Creation Date: Sat Dec 24 2016 14:13:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 23 2018 23:59:52 GMT+0800 (Singapore Standard Time)

<p>
@yavinci  Talked about the "smart"/Easiest solution in his thread page ([here](https://discuss.leetcode.com/topic/30625/easiest-java-solution/12)), and most explanation down that page is not really touching why the smart solution is smart, So i am making this post to 1)explain in detail why the smart solution is smart and 2)make some improvements/tweaks on the smart solution code to show you which part is essential, 3) also i will briefly mention why [Sparse Matrix Manipulation](http://www.cs.cmu.edu/~scandal/cacm/node9.html) can help make some improvements on top of the smart solution.

a) Originally, the normal way to calculate the multiplication of two metrics A, and B is as follow:
        We take the the all values from the first line of A, and all values from the first column of B, and multiply the corresponding values and sum them up, the final sum is the value for the location of first column and first row in final result matrix.  Similarly, the value at [ i ][ j ] of result matrix C, which is C[ i ][ j ] is calculated as:
     
C[ i ][ j ] = A[ i ][0]*B[0][j]  +  A[i][1]*B[1][j] + A[i][2]*B[2][j] + ... A[i][K]*B[K][j]
( which is the sum of each multiplication of corresponding K values from row i of A and K values from column j of B )
```The Key is: if we calculate it this way, we finishing calculating the final value for the result matrix at once ```
Then the brute force solution is as follow:


    public class Solution {
            public int[][] multiply(int[][] A, int[][] B) {
                int m = A.length, n = A[0].length, nB = B[0].length;
                int[][] C = new int[m][nB];

                for(int i = 0; i < m; i++) {
                        for (int j = 0; j < nB; j++) {
                            for(int k = 0; k < n; k++) {
                                 C[i][j] += A[i][k] * B[k][j];
                            }
                        }
                }
                return C;  
            }
    }

b) ****The smart solution****, the key part of smart solution is that: it does not calculate the final result at once, and it takes each value from A, and calculate and partial sum and accumulate it into the final spot:
For example, for each value A[i][k], if it is not zero, it will be used at most nB times ( n is B[0].length ), which can be illustrated as follow:
Generally for the following equation:
C[ i ][ j ] = A[ i ][0]*B[0][j]  +  A[i][1]*B[1][j] + A[i][2]*B[2][j] + ... A[i][k]*B[k][j] ....  A[i][K]*B[K][j]
j can be from 0 to nB, if we write all of them down, it will like following:
[For i from 0 to nB 
C[ i ][ 0 ]=A[ i ][0]*B[0][0]  +  A[i][1]*B[1][0] + A[i][2]*B[2][0] + ... A[i][k]B[k][0] ....  A[i][K]*B[K][0]
C[ i ][ 1 ]=A[ i ][0]*B[0][1]  +  A[i][1]*B[1][1] + A[i][2]*B[2][1] + ... A[i][k]B[k][0] ....  A[i][K]*B[K][1]
...
C[ i ][ nB ]=A[ i ][0]*B[0][nB]  +  A[i][1]*B[1][nB] + A[i][2]*B[2][nB] + ... A[i][k]B[k][nB] ....  A[i][K]*B[K][nB]

As you can see from above: for the same value A[i][k] from the first matrix, it will be used at most nB times if A[i][k] is not zero.  And the smart solution is taking advantage of that!!!, the smart solution can be described as:

For each value A[i][k] in matrix A, if it is not zero, we calculate A[i][k] * B[k][j] and accumulate it into C[ i ][ j ] (```Key part: the C[ i ][ j ] by now is not the final value in the result matrix !! Remember, in the brute force solution, the final value of C[i][j], takes sum of all multiplication values of K corresponding values from A and B? here C[ i ][ j ] is only sum of some multiplication values, NOT ALL until the program is done``` )

**BY NOW, it is very clear that, if the value A[ i ][ k ] from matrix is zero, we skip a For-loop- calculation, which is a loop iterating nB times, and this is the key part of why the smart solution is smart!!!**
 
The smart Solution Code is as follow:


    public class Solution {
        public int[][] multiply(int[][] A, int[][] B) {
            int m = A.length, n = A[0].length, nB = B[0].length;
            int[][] C = new int[m][nB];

            for(int i = 0; i < m; i++) {
                for(int k = 0; k < n; k++) {
                    if (A[i][k] != 0) {
                        for (int j = 0; j < nB; j++) {
                            if (B[k][j] != 0) C[i][j] += A[i][k] * B[k][j];
                        }
                    }
                }
            }
            return C;   
        }
    }

( Credit:@yavinci , I am having a different version, so I am directly referencing the original version )

Based on the discussion above, the inner checking ( ```if (B[k][j] != 0) ```  )  is actually not necessary, because whether or not we have that check, we still iterate nB times, ( since the operation ```C[i][j] += A[i][k] * B[k][j];``` inside the if-check is O(1) time)

So the smart solution can also be written as follow by removing the check ( which is my version ):


    public class Solution {
        public int[][] multiply(int[][] A, int[][] B) {
            int m = A.length, n = A[0].length, nB = B[0].length;
            int[][] C = new int[m][nB];

            for(int i = 0; i < m; i++) {
                for(int k = 0; k < n; k++) {
                    if (A[i][k] != 0) {
                        for (int j = 0; j < nB; j++) {
                            if (B[k][j] != 0) C[i][j] += A[i][k] * B[k][j];
                        }
                    }
                }
            }
            return C;   
        }
    }


c) "Sparse matrix manipultion" helps, if we compress the first sparse matrix into rows of lists( in each row list, it contains ( value, index ) pair ), we actually don't need to go over all values in a row in matrix A when are calculating the final result matrix. But Overall, it does not help improve run time algorithmatically!!
</p>


### Java and Python Solutions with and without Tables
- Author: stpeterh
- Creation Date: Sat Nov 28 2015 04:03:10 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 25 2018 11:27:01 GMT+0800 (Singapore Standard Time)

<p>
Given A and B are sparse matrices, we could use lookup tables to speed up.  At the beginining I thought two lookup tables would be necessary.  After [discussing with @yavinci][1], I think one lookup table for B would be enough.  Surprisingly, it seems like detecting non-zero elements for both A and B on the fly without additional data structures provided the fastest performance on current test set.

However, I think such fastest performance could due to an imperfect test set we have for OJ right now: there are only 12 test cases.  And, for an element `B[k, j]`, it would be detected for non-zero elements several times if we detecting both A and B on the fly, depending on how many `i`'s make elements `A[i, k]` non-zero.  With this point, the additional data structures, like lookup tables, should save our time by focusing on only non-zero elements.  If it is not, I am worried the set of OJ test cases probably is not good enough.

Anyway, I am posting my respective solutions below.  Comments are welcome. Thanks @yavinci again for discussing with me.

Python solution with only one table for B (~196ms):

    class Solution(object):
        def multiply(self, A, B):
            """
            :type A: List[List[int]]
            :type B: List[List[int]]
            :rtype: List[List[int]]
            """
            if A is None or B is None: return None
            m, n, l = len(A), len(A[0]), len(B[0])
            if len(B) != n:
                raise Exception("A's column number must be equal to B's row number.")
            C = [[0 for _ in range(l)] for _ in range(m)]
            tableB = {}
            for k, row in enumerate(B):
                tableB[k] = {}
                for j, eleB in enumerate(row):
                    if eleB: tableB[k][j] = eleB
            for i, row in enumerate(A):
                for k, eleA in enumerate(row):
                    if eleA:
                        for j, eleB in tableB[k].iteritems():
                            C[i][j] += eleA * eleB
            return C

Java solution with only one table for B (~150ms):

    public class Solution {
        public int[][] multiply(int[][] A, int[][] B) {
            if (A == null || A[0] == null || B == null || B[0] == null) return null;
            int m = A.length, n = A[0].length, l = B[0].length;
            int[][] C = new int[m][l];
            Map<Integer, HashMap<Integer, Integer>> tableB = new HashMap<>();
            
            for(int k = 0; k < n; k++) {
                tableB.put(k, new HashMap<Integer, Integer>());
                for(int j = 0; j < l; j++) {
                    if (B[k][j] != 0){
                        tableB.get(k).put(j, B[k][j]);
                    }
                }
            }
    
            for(int i = 0; i < m; i++) {
                for(int k = 0; k < n; k++) {
                    if (A[i][k] != 0){
                        for (Integer j: tableB.get(k).keySet()) {
                            C[i][j] += A[i][k] * tableB.get(k).get(j);
                        }
                    }
                }
            }
            return C;   
        }
    }


Python solution without table (~156ms):

    class Solution(object):
        def multiply(self, A, B):
            """
            :type A: List[List[int]]
            :type B: List[List[int]]
            :rtype: List[List[int]]
            """
            if A is None or B is None: return None
            m, n, l = len(A), len(A[0]), len(B[0])
            if len(B) != n:
                raise Exception("A's column number must be equal to B's row number.")
            C = [[0 for _ in range(l)] for _ in range(m)]
            for i, row in enumerate(A):
                for k, eleA in enumerate(row):
                    if eleA:
                        for j, eleB in enumerate(B[k]):
                            if eleB: C[i][j] += eleA * eleB
            return C

Java solution without table (~70ms):

    public class Solution {
        public int[][] multiply(int[][] A, int[][] B) {
            int m = A.length, n = A[0].length, l = B[0].length;
            int[][] C = new int[m][l];
    
            for(int i = 0; i < m; i++) {
                for(int k = 0; k < n; k++) {
                    if (A[i][k] != 0){
                        for (int j = 0; j < l; j++) {
                            if (B[k][j] != 0) C[i][j] += A[i][k] * B[k][j];
                        }
                    }
                }
            }
            return C;   
        }
    }

Python solution with two tables (~196ms):

    class Solution(object):
        def multiply(self, A, B):
            """
            :type A: List[List[int]]
            :type B: List[List[int]]
            :rtype: List[List[int]]
            """
            if A is None or B is None: return None
            m, n = len(A), len(A[0])
            if len(B) != n:
                raise Exception("A's column number must be equal to B's row number.")
            l = len(B[0])
            table_A, table_B = {}, {}
            for i, row in enumerate(A):
                for j, ele in enumerate(row):
                    if ele:
                        if i not in table_A: table_A[i] = {}
                        table_A[i][j] = ele
            for i, row in enumerate(B):
                for j, ele in enumerate(row):
                    if ele:
                        if i not in table_B: table_B[i] = {}
                        table_B[i][j] = ele
            C = [[0 for j in range(l)] for i in range(m)]
            for i in table_A:
                for k in table_A[i]:
                    if k not in table_B: continue
                    for j in table_B[k]:
                        C[i][j] += table_A[i][k] * table_B[k][j]
            return C
          
Java solution with two tables (~160ms):

    public class Solution {
        public int[][] multiply(int[][] A, int[][] B) {
            if (A == null || B == null) return null;
            if (A[0].length != B.length) 
                throw new IllegalArgumentException("A's column number must be equal to B's row number.");
            Map<Integer, HashMap<Integer, Integer>> tableA = new HashMap<>();
            Map<Integer, HashMap<Integer, Integer>> tableB = new HashMap<>();
            int[][] C = new int[A.length][B[0].length];
            for (int i = 0; i < A.length; i++) {
                for (int j = 0; j < A[i].length; j++) {
                    if (A[i][j] != 0) {
                        if(tableA.get(i) == null) tableA.put(i, new HashMap<Integer, Integer>());
                        tableA.get(i).put(j, A[i][j]);
                    }
                }
            }
            
            for (int i = 0; i < B.length; i++) {
                for (int j = 0; j < B[i].length; j++) {
                    if (B[i][j] != 0) {
                        if(tableB.get(i) == null) tableB.put(i, new HashMap<Integer, Integer>());
                        tableB.get(i).put(j, B[i][j]);
                    }
                }
            }
            
            for (Integer i: tableA.keySet()) {
                for (Integer k: tableA.get(i).keySet()) {
                    if (!tableB.containsKey(k)) continue;
                    for (Integer j: tableB.get(k).keySet()) {
                        C[i][j] += tableA.get(i).get(k) * tableB.get(k).get(j);
                    }
                }
            }
            return C;
        }
    }


  [1]: https://leetcode.com/discuss/71912/easiest-76ms-java-solution
</p>


