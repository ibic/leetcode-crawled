---
title: "Active Users"
weight: 1580
#id: "active-users"
---
## Description
<div class="description">
<p>Table <code>Accounts</code>:</p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| name          | varchar |
+---------------+---------+
the id is the primary key for this table.
This table contains the account id and the user name of each account.
</pre>

<p>&nbsp;</p>

<p>Table <code>Logins</code>:</p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| login_date    | date    |
+---------------+---------+
There is no primary key for this table, it may contain duplicates.
This table contains the account id of the user who logged in and the login date. A user may log in multiple times in the day.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the id and the name of active users.</p>

<p>Active users are those who logged in to their accounts for 5 or more consecutive days.</p>

<p>Return the result table <strong>ordered</strong> by the id.</p>

<p>The query result format is in the following example:</p>

<pre>
Accounts table:
+----+----------+
| id | name     |
+----+----------+
| 1  | Winston  |
| 7  | Jonathan |
+----+----------+

Logins table:
+----+------------+
| id | login_date |
+----+------------+
| 7  | 2020-05-30 |
| 1  | 2020-05-30 |
| 7  | 2020-05-31 |
| 7  | 2020-06-01 |
| 7  | 2020-06-02 |
| 7  | 2020-06-02 |
| 7  | 2020-06-03 |
| 1  | 2020-06-07 |
| 7  | 2020-06-10 |
+----+------------+

Result table:
+----+----------+
| id | name     |
+----+----------+
| 7  | Jonathan |
+----+----------+
User Winston with id = 1 logged in 2 times only in 2 different days, so, Winston is not an active user.
User Jonathan with id = 7 logged in 7 times in 6 different days, five of them were consecutive days, so, Jonathan is an active user.
</pre>

<p><strong>Follow up question:</strong><br />
Can you write a general solution if the active&nbsp;users are those who logged in to their accounts for <code>n</code> or more consecutive days?</p>

</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple MySQL solution without window function
- Author: coderxrise
- Creation Date: Thu May 21 2020 10:51:30 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 21 2020 10:51:30 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT DISTINCT a.id, (SELECT name FROM Accounts WHERE id=a.id) name
FROM Logins a, Logins b
WHERE a.id=b.id AND
DATEDIFF(a.login_date,b.login_date) BETWEEN 1 AND 4
GROUP BY a.id, a.login_date
HAVING COUNT(DISTINCT b.login_date)=4
;
</p>


### 100% Faster than Submissions, Window Functions, Documentation Linked
- Author: adrianlievano1993
- Creation Date: Fri May 22 2020 01:35:58 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 22 2020 01:36:10 GMT+0800 (Singapore Standard Time)

<p>
This is a problem I keep running into as a professional data scientist. There are a ton of inefficient ways to do it, but I found these blog posts to be super helpful: 

1. https://stackoverflow.com/questions/26117179/sql-count-consecutive-days
2. https://mattboegner.com/improve-your-sql-skills-master-the-gaps-islands-problem/

This is what is known as a \'Gap & Islands\' Problem. It\'s fairly common to run into it, and knowing how to do it opens up the door to many important analysis: calculating churn, retention, ressurrected users, etc. 


```# # Write your MySQL query statement below

with temp0 AS
(SELECT  id,
            login_date,
            dense_rank() OVER(PARTITION BY id ORDER BY login_date) as row_num
    FROM Logins),

temp1 as (
    select id, login_date, row_num,
        DATE_ADD(login_date, INTERVAL -row_num DAY) as Groupings
    from temp0),

answer_table as (SELECT  id,
         MIN(login_date) as startDate,
         MAX(login_date) as EndDate,
         row_num,
         Groupings, 
         count(id),
        datediff(MAX(login_date), MIN(login_date)) as duration
 FROM temp1
 GROUP BY id, Groupings
 HAVING datediff(MAX(login_date), MIN(login_date)) >= 4
 ORDER BY id, StartDate)
 
select distinct a.id, name
from answer_table a
join Accounts acc on acc.id = a.id
order by a.id\'\'\'
</p>


### (^_^) Mysql Solutions: 3 easy ways + 1 RECURSIVE CTE way to solve N
- Author: 120668385
- Creation Date: Fri May 22 2020 13:15:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 07 2020 04:34:00 GMT+0800 (Singapore Standard Time)

<p>
Related Questions: 
1321. Restaurant Growth
601. Human Traffic of Stadium
********************************
**Choice 1(I like this one):**

```
SELECT DISTINCT l1.id,
(SELECT name FROM Accounts WHERE id = l1.id) AS name
FROM Logins l1
JOIN Logins l2 ON l1.id = l2.id AND DATEDIFF(l2.login_date, l1.login_date) BETWEEN 1 AND 4
GROUP BY l1.id, l1.login_date
HAVING COUNT(DISTINCT l2.login_date) = 4
```
_________________

**Choice 2:**

```
SELECT *
FROM Accounts
WHERE id IN
    (SELECT DISTINCT t1.id 
     FROM Logins t1 INNER JOIN Logins t2 on t1.id = t2.id AND DATEDIFF(t1.login_date, t2.login_date) BETWEEN 1 AND 4
     GROUP BY t1.id, t1.login_date
     HAVING COUNT(DISTINCT(t2.login_date)) = 4)
ORDER BY id
```

_________________

**Choice 3:**

```
WITH temp AS (SELECT l1.id, l1.login_date, COUNT(DISTINCT l2.login_date) AS cnt
FROM Logins l1
LEFT JOIN Logins l2 ON l1.id = l2.id AND DATEDIFF(l2.login_date, l1.login_date) BETWEEN 1 AND 4
GROUP BY 1,2
HAVING cnt>=4)

SELECT DISTINCT temp.id, name
FROM temp
JOIN Accounts ON temp.id = Accounts.id
ORDER BY 1;
```

_________________


**Recursive CTE solution** to solve the follow up question(just for test):

```
WITH RECURSIVE
rec_t AS
(SELECT id, login_date, 1 AS days FROM Logins 
 UNION ALL
 SELECT l.id, l.login_date, rec_t.days+1 FROM rec_t
 INNER JOIN Logins l 
 ON rec_t.id = l.id AND DATE_ADD(rec_t.login_date, INTERVAL 1 DAY) = l.login_date
)

SELECT * FROM Accounts
WHERE id IN
(SELECT DISTINCT id FROM rec_t WHERE days = 5)
ORDER BY id
```

_________________

**Explanation** for RECURSIVE CTE solution:

First, you need a recursive cte query to get the data as the following:

*-TEST CODE:*

```
WITH RECURSIVE
rec_t AS
(SELECT id, login_date, 1 AS days FROM Logins 
 UNION ALL
 SELECT l.id, l.login_date, rec_t.days+1 FROM rec_t
 INNER JOIN Logins l 
 ON rec_t.id = l.id AND DATE_ADD(rec_t.login_date, INTERVAL 1 DAY) = l.login_date
)

SELECT * FROM rec_t
```

*-RESULT:*

{"headers": 
["id", "login_date", "days"], "values": [
[7, "2020-05-30", 1], 
[1, "2020-05-30", 1], 
[7, "2020-05-31", 1], 
[7, "2020-06-01", 1], 
[7, "2020-06-02", 1], 
[7, "2020-06-02", 1], 
[7, "2020-06-03", 1], 
[1, "2020-06-07", 1], 
[7, "2020-06-10", 1],
Above rows are all the login records, which is same as the original Logins Table.  1 is what we add for recording which level it belongs to.
[7, "2020-05-31", 2], 
[7, "2020-06-01", 2], 
[7, "2020-06-02", 2], 
[7, "2020-06-02", 2], 
[7, "2020-06-03", 2], 
[7, "2020-06-03", 2], 
Above rows are those have a second day login
[7, "2020-06-01", 3], 
[7, "2020-06-02", 3], 
[7, "2020-06-02", 3], 
[7, "2020-06-03", 3], 
[7, "2020-06-03", 3], 
Above rows are those have a third day login
[7, "2020-06-02", 4], 
[7, "2020-06-02", 4], 
[7, "2020-06-03", 4], 
[7, "2020-06-03", 4], 
Above rows are those have a fourth day login
[7, "2020-06-03", 5], 
[7, "2020-06-03", 5]]}
Above rows are those have a fifth day login

Second, if your N is 5, then you add this number to your query like this:

*-TEST CODE(clear the previous one):*

```
WITH RECURSIVE
rec_t AS
(SELECT id, login_date, 1 AS days FROM Logins 
 UNION ALL
 SELECT l.id, l.login_date, rec_t.days+1 FROM rec_t
 INNER JOIN Logins l 
 ON rec_t.id = l.id AND DATE_ADD(rec_t.login_date, INTERVAL 1 DAY) = l.login_date
)

SELECT DISTINCT id FROM rect WHERE days = 5
```

*-RESULT:*

{"headers": ["id"], "values": [[7]]}

Finally,  you can get the id and name from the Accounts table:

*-TEST CODE(clear the previous one):*

```
WITH RECURSIVE
rec_t AS
(SELECT id, login_date, 1 AS days FROM Logins 
 UNION ALL
 SELECT l.id, l.login_date, rec_t.days+1 FROM rec_t
 INNER JOIN Logins l 
 ON rec_t.id = l.id AND DATE_ADD(rec_t.login_date, INTERVAL 1 DAY) = l.login_date
)

SELECT * FROM Accounts
WHERE id IN
(SELECT DISTINCT id FROM rec_t WHERE days = 5)
ORDER BY id
```

*-RESULT:*

And you will get the final result shown below (if you submit it, it may run timeout):

{"headers": ["id", "name"], "values": [[7, "Jonathan"]]}

</p>


