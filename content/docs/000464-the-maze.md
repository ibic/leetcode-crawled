---
title: "The Maze"
weight: 464
#id: "the-maze"
---
## Description
<div class="description">
<p>There is a ball in a <code>maze</code> with empty spaces (represented as <code>0</code>) and walls (represented as <code>1</code>). The ball can go through the empty spaces by rolling <strong>up, down, left or right</strong>, but it won&#39;t stop rolling until hitting a wall. When the ball stops, it could choose the next direction.</p>

<p>Given the <code>maze</code>, the ball&#39;s <code>start</code> position and&nbsp;the <code>destination</code>, where <code>start = [start<sub>row</sub>, start<sub>col</sub>]</code> and <code>destination&nbsp;= [destination<sub>row</sub>, destination<sub>col</sub>]</code>, return <code>true</code> if the ball can stop at the destination, otherwise return <code>false</code>.</p>

<p>You may assume that <strong>the borders of the maze are all walls</strong> (see examples).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/01/maze1.png" style="width: 500px; height: 277px;" />
<pre>
<strong>Input:</strong> maze = [[0,0,1,0,0],[0,0,0,0,0],[0,0,0,1,0],[1,1,0,1,1],[0,0,0,0,0]], start = [0,4], destination = [4,4]
<strong>Output:</strong> true
<strong>Explanation:</strong> One possible way is : left -&gt; down -&gt; left -&gt; down -&gt; right -&gt; down -&gt; right.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/01/maze2.png" style="width: 500px; height: 277px;" />
<pre>
<strong>Input:</strong> maze = [[0,0,1,0,0],[0,0,0,0,0],[0,0,0,1,0],[1,1,0,1,1],[0,0,0,0,0]], start = [0,4], destination = [3,2]
<strong>Output:</strong> false
<strong>Explanation:</strong> There is no way for the ball to stop at the destination. Notice that you can pass through the destination but you cannot stop there.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> maze = [[0,0,0,0,0],[1,1,0,0,1],[0,0,0,0,0],[0,1,0,0,1],[0,1,0,0,0]], start = [4,3], destination = [0,1]
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= maze.length, maze[i].length &lt;= 100</code></li>
	<li><code>maze[i][j]</code> is <code>0</code> or <code>1</code>.</li>
	<li><code>start.length == 2</code></li>
	<li><code>destination.length == 2</code></li>
	<li><code>0 &lt;= start<sub>row</sub>,&nbsp;destination<sub>row</sub> &lt;= maze.length</code></li>
	<li><code>0 &lt;= start<sub>col</sub>,&nbsp;destination<sub>col</sub> &lt;= maze[i].length</code></li>
	<li>Both the ball and the destination exist on an empty space, and they will not be at the same position initially.</li>
	<li>The maze contains <strong>at least 2 empty spaces</strong>.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Depth First Search

We can view the given search space in the form of a tree. The root node of the tree represents the starting position. Four different routes are possible from each position i.e. left, right, up or down. These four options can be represented by 4 branches of each node in the given tree. Thus, the new node reached from the root traversing over the branch represents the new position occupied by the ball after choosing the corresponding direction of travel.

![Maze_Tree](../Figures/490_Maze_Tree.PNG)

In order to do this traversal, one of the simplest schemes is to undergo depth first search. In this case, we choose one path at a time and try to go as deep as possible into the levels of the tree before going for the next path. In order to implement this, we make use of a recursive function `dfs(maze, start, desination, visited)`. This function takes the given $$maze$$ array, the $$start$$ position and the $$destination$$ position as its arguments along with a $$visited$$ array. $$visited$$ array is a 2-D boolean array of the same size as that of $$maze$$. A True value at $$visited[i][j]$$ represents that the current position has already been reached earlier during the path traversal. We make use of this array so as to keep track of the same paths being repeated over and over. We mark a True at the current position in the $$visited$$ array once we reach that particular positon in the $$maze$$.

From every $$start$$ position, we can move continuously in either left, right, upward or downward direction till we reach the boundary or a wall. Thus, from the $$start$$ position, we determine all the end points which can be reached by choosing the four directions. For each of the cases, the new endpoint will now act as the new start point for the traversals. The destination, obviously remains unchanged. Thus, now we call the same function four times for the four directions, each time with a new start point obtained previously. 

If any of the function call returns a True value, it means we can reach the desination. 

The following animation depicts the process:

!?!../Documents/490_Maze_DFS.json:1000,563!?!

<iframe src="https://leetcode.com/playground/bpeFBMaU/shared" frameBorder="0" width="100%" height="500" name="bpeFBMaU"></iframe>

**Complexity Analysis**

* Time complexity : $$O(mn)$$. Complete traversal of maze will be done in the worst case. Here, $$m$$ and $$n$$ refers to the number of rows and coloumns of the maze.

* Space complexity : $$O(mn)$$. $$visited$$ array of size $$m*n$$ is used.

---
#### Approach 2: Breadth First Search

**Algorithm**

The same search space tree can also be explored in a Depth First Search manner. In this case, we try to explore the search space on a level by level basis. i.e. We try to move in all the directions at every step. When all the directions have been explored and we still don't reach the destination, then only we proceed to the new set of traversals from the new positions obtained. 

In order to implement this, we make use of a $$queue$$. We start with the ball at the $$start$$ position. For every current position, we add all the new positions possible by traversing in all the four directions(till reaching the wall or boundary) into the $$queue$$ to act as the new start positions and mark these positions as True in the $$visited$$ array. When all the directions have been covered up, we remove a position value, $$s$$, from the front of the $$queue$$ and again continue the same process with $$s$$ acting as the new $$start$$ position. 

Further, in order to choose the direction of travel, we make use of a $$dir$$ array, which contains 4 entries. Each entry represents a  one-dimensional direction of travel. To travel in a particular direction, we keep on adding the particular entry of the $$dirs$$ array till we hit a wall or a boundary. For a particular start position, we do this process of $$dir$$ addition for all all the four directions possible.

If we hit the destination position at any moment, we return a True directly indicating that the $$destination$$ position can be reached starting from the $$start$$ position. 

The following animation depicts the process:

!?!../Documents/490_Maze_BFS.json:1000,563!?!

<iframe src="https://leetcode.com/playground/eK3vzmye/shared" frameBorder="0" width="100%" height="500" name="eK3vzmye"></iframe>

**Complexity Analysis**

* Time complexity : $$O(mn)$$. Complete traversal of maze will be done in the worst case. Here, $$m$$ and $$n$$ refers to the number of rows and coloumns of the maze.

* Space complexity : $$O(mn)$$. $$visited$$ array of size $$m*n$$ is used and $$queue$$ size can grow upto $$m*n$$ in worst case.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy-understanding Java bfs solution.
- Author: ckcz123
- Creation Date: Wed Feb 01 2017 21:29:39 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 00:53:45 GMT+0800 (Singapore Standard Time)

<p>
Solution of *The Maze II*: https://discuss.leetcode.com/topic/77472/similar-to-the-maze-easy-understanding-java-bfs-solution
Solution of *The Maze III*: https://discuss.leetcode.com/topic/77474/similar-to-the-maze-ii-easy-understanding-java-bfs-solution

A standart bfs solution.
``` java
public class Solution {
    class Point {
        int x,y;
        public Point(int _x, int _y) {x=_x;y=_y;}
    }
    public boolean hasPath(int[][] maze, int[] start, int[] destination) {
        int m=maze.length, n=maze[0].length;
        if (start[0]==destination[0] && start[1]==destination[1]) return true;
        int[][] dir=new int[][] {{-1,0},{0,1},{1,0},{0,-1}};
        boolean[][] visited=new boolean[m][n];
        LinkedList<Point> list=new LinkedList<>();
        visited[start[0]][start[1]]=true;
        list.offer(new Point(start[0], start[1]));
        while (!list.isEmpty()) {
            Point p=list.poll();
            int x=p.x, y=p.y;
            for (int i=0;i<4;i++) {
                int xx=x, yy=y;
                while (xx>=0 && xx<m && yy>=0 && yy<n && maze[xx][yy]==0) {
                    xx+=dir[i][0];
                    yy+=dir[i][1];
                }
                xx-=dir[i][0];
                yy-=dir[i][1];
                if (visited[xx][yy]) continue;
                visited[xx][yy]=true;
                if (xx==destination[0] && yy==destination[1]) return true;
                list.offer(new Point(xx, yy));
            }
        }
        return false;
        
    }
}
```
</p>


### Python BFS solution
- Author: SanjarAhmadov
- Creation Date: Thu Feb 02 2017 02:47:11 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 04 2018 07:09:07 GMT+0800 (Singapore Standard Time)

<p>
```
def hasPath(self, maze, start, destination):

        Q = [start]
        n = len(maze)
        m = len(maze[0])
        dirs = ((0, 1), (0, -1), (1, 0), (-1, 0))
        
        while Q:
            # Use Q.pop() as DFS or Q.popleft() with deque from collections library for better performance. Kudos to @whglamrock
            i, j = Q.pop(0)
            maze[i][j] = 2

            if i == destination[0] and j == destination[1]:
                return True
            
            for x, y in dirs:
                row = i + x
                col = j + y
                while 0 <= row < n and 0 <= col < m and maze[row][col] != 1:
                    row += x
                    col += y
                row -= x
                col -= y
                if maze[row][col] == 0:
                    Q.append([row, col])
        
        return False
```
</p>


### Simple Java DFS with comments
- Author: KidOptimo
- Creation Date: Thu Feb 02 2017 06:48:53 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 12:18:59 GMT+0800 (Singapore Standard Time)

<p>
- Search in the four possible directions when coming to a stopping point (i.e. a new starting point).
- Keep track of places that you already started at in case you roll back to that point.

```
public class Solution {
    public boolean hasPath(int[][] maze, int[] start, int[] destination) {
        boolean[][] startedHere = new boolean[maze.length][maze[0].length]; // mark visited starting points
        return dfs(maze, startedHere, start, destination);
    }
    
    private boolean dfs(int[][] maze, boolean[][] startedHere, int[] start, int[] destination) {
        if (startedHere[start[0]][start[1]]) return false;
        if (Arrays.equals(start, destination)) return true;
        
        startedHere[start[0]][start[1]] = true; // in case we roll back to a point we already started at
        
        BiPredicate<Integer, Integer> roll = (rowInc, colInc) -> {
            int row = start[0], col = start[1]; // init new start row and col
            while (canRoll(maze, row + rowInc, col + colInc)) {
                row += rowInc;
                col += colInc;
            }
            return dfs(maze, startedHere, new int[]{row, col}, destination); // pass in new start to dfs
        };
        
        if (roll.test(1, 0)) return true; // roll up
        if (roll.test(0, 1)) return true; // roll right
        if (roll.test(-1, 0)) return true; // roll down
        if (roll.test(0, -1)) return true; // roll left
        
        return false; // return false if no paths led to destination
    }
    
    private boolean canRoll(int[][] maze, int row, int col) {
        if (row >= maze.length || row < 0 || col >= maze[0].length || col < 0) return false; // stop at borders
        return maze[row][col] != 1; // stop at walls (1 -> wall)
    }
}
```

UPDATE: Also including one without using BiPredicate on every recursive call since it runs faster

```
public class Solution {
    
    private static final int[] DIRECTIONS = { 0, 1, 0, -1, 0 };
    
    public boolean hasPath(int[][] maze, int[] start, int[] destination) {
        boolean[][] startedHere = new boolean[maze.length][maze[0].length];
        return dfs(maze, startedHere, start, destination);
    }
    
    private boolean dfs(int[][] maze, boolean[][] startedHere, int[] start, int[] destination) {
        if (startedHere[start[0]][start[1]]) return false;
        if (Arrays.equals(start, destination)) return true;
        
        startedHere[start[0]][start[1]] = true;
        
        for (int i = 0; i < DIRECTIONS.length - 1; i++) {
            int[] newStart = roll(maze, start[0], start[1], DIRECTIONS[i], DIRECTIONS[i + 1]);
            if (dfs(maze, startedHere, newStart, destination)) return true;
        }
        
        return false;
    }
    
    private int[] roll(int[][] maze, int row, int col, int rowInc, int colInc) {
        while (canRoll(maze, row + rowInc, col + colInc)) {
            row += rowInc;
            col += colInc;
        }
        
        return new int[]{row, col};
    }
    
    private boolean canRoll(int[][] maze, int row, int col) {
        if (row >= maze.length || row < 0 || col >= maze[0].length || col < 0) return false;
        return maze[row][col] != 1; // 1 is a wall
    }
}
```
</p>


