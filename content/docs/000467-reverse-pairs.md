---
title: "Reverse Pairs"
weight: 467
#id: "reverse-pairs"
---
## Description
<div class="description">
<p>Given an array <code>nums</code>, we call <code>(i, j)</code> an <b><i>important reverse pair</i></b> if <code>i &lt; j</code> and <code>nums[i] &gt; 2*nums[j]</code>.</p>

<p>You need to return the number of important reverse pairs in the given array.</p>

<p><b>Example1:</b>
<pre>
<b>Input</b>: [1,3,2,3,1]
<b>Output</b>: 2
</pre></p>

<p><b>Example2:</b>
<pre>
<b>Input</b>: [2,4,3,5,1]
<b>Output</b>: 3
</pre></p>

<p><b>Note:</b><br>
<ol>
<li>The length of the given array will not exceed <code>50,000</code>.</li>
<li>All the numbers in the input array are in the range of 32-bit integer.</li>
</ol>
</p>
</div>

## Tags
- Binary Search (binary-search)
- Divide and Conquer (divide-and-conquer)
- Sort (sort)
- Binary Indexed Tree (binary-indexed-tree)
- Segment Tree (segment-tree)

## Companies
- ByteDance - 6 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: true)
- Uber - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Brute Force

**Intuition**

Do as directed in the question. We can simply check all the pairs if they are important reverse pairs or not.

**Algorithm**

* Iterate over $$i$$ from $$0$$ to $$\text{size} - 1$$
    * Iterate over $$j$$ from $$0$$ to $$i - 1$$
        * If $$\text{nums[j]} > 2 * \text{nums[i]}$$, increment $$\text{count}$$


<iframe src="https://leetcode.com/playground/je5UoW73/shared" frameBorder="0" width="100%" height="259" name="je5UoW73"></iframe>

**Complexity Analysis**

* Time complexity: $$O(n^2)$$
    * We iterate over all the possible pairs wherein ($$i<j$$) in the array which is $$O(n^2)$$

* Space complexity: $$O(1)$$ only constant extra space is required for $$n$$, $$count$$ etc.

**Trivia**

The above code can be expressed as one-liner in Python:

<iframe src="https://leetcode.com/playground/6jgFrttV/shared" frameBorder="0" width="100%" height="89" name="6jgFrttV"></iframe>

Herein, we iterate over all the pairs and store the boolean values for $$\text{nums[i]}>2*\text{nums[j]}$$. Finally, we count the number of $$\text{true}$$ in the array and return the result.
<br>
<br>

---
#### Approach 2: Binary Search Tree

**Intuition**

In Approach 1, for each element $$i$$, we searched the subarray $$[0,i)$$ for elements such that their value is greater than $$2*\text{nums[i]}$$. In the previous approach, the search is linear. However, we need to make the process efficient. Maybe, memoization can help, but since, we need to compare the elements, we cannot find a linear DP solution.

Observe that the indices of the elements in subarray $$[0,i)$$ don't matter as we only require the count. So, we can sort the elements and perform binary search on the subarray. But, since the subarray keeps growing as we iterate to the next element, we need a data structure to store the previous result as well as to allow efficient searching(preferably $$O(\log n)$$) - Binary Search Tree(BST) could be a good bet.   

*Refreshing BST*

BST is a rooted binary tree, wherein each node is associated with a value and has 2 distinguishable sub-trees namely $$left$$ and $$right$$ subtree. The left subtree contains only the nodes with lower values than the parent's value, while the right subtree conatins only the nodes with greater values than the parent's value.

*Voila!*

This is exactly what is required. So, if we store our elements in BST, then we can search the larger elements thus eliminating the search on smaller elements altogether.

**Algorithm**

Define the $$\text{Node}$$ of BST that stores the $$\text{val}$$ and pointers to the $$\text{left}$$ and $$\text{right}$$. We also need a count of elements(say $$\text{count\_ge}$$) in the subtree rooted at the current node that are greater than or equal to the current node's $$\text{val}$$. $$\text{count\_ge}$$ is initialized to 1 for each node and $$\text{left}$$, $$\text{right}$$ pointers are set to $$\text{NULL}$$.


We define the $$\text{insert}$$ routine that recursively adds the given $$\text{val}$$ as an appropriate leaf node based on comparisons with the $$Node.val$$. Each time, the given $$val$$ is smaller than $$Node.val$$, we increment the $$\text{count\_ge}$$ and move the $$val$$ to the right subtree. While, if the $$val$$ is equal to the current $$Node$$, we simply increment the $$\text{count\_ge}$$ and exit. While, we move to the left subtree in case $$(\text{val}<\text{Node.val})$$.

We also require the $$search$$ routine that gives the count of number of elements greater than or equal to the $$\text{target}$$. In the $$\text{search}$$ routine, if the $$head$$ is NULL, return 0. Otherwise, if $$\text{target}==\text{head.val}$$, we know the count of values greater than or equal to the $$\text{target}$$, hence simply return $$\text{head.count\_ge}$$. In case, $$\text{target}<\text{head.val}$$, the ans is calculated by adding $$\text{Node.count\_ge}$$ and recursively calling the $$\text{search}$$ routine with $$\text{head.left}$$. And if $$\text{target}>\text{head.val}$$, ans is obtained by recursively calling the $$\text{search}$$ routine with $$\text{head.right}$$.

Now, we can get to our main logic:

* Iterate over $$i$$ from $$0$$ to $$(size-1)$$ of $$\text{nums}$$ :
    * Search the existing BST for $$\text{nums[i]} * 2 + 1$$ and add the result to $$\text{count}$$
    * Insert $$\text{nums[i]}$$ to the BST, hence updating the $$\text{count\_ge}$$ of the previous nodes

The algorithm can be better understood using the example below:
!?!../Documents/493_reverse_pairs.json:1000,662!?!

<iframe src="https://leetcode.com/playground/7pmviQhS/shared" frameBorder="0" width="100%" height="500" name="7pmviQhS"></iframe>

**Complexity analysis**

* Time complexity: $$O(n^2)$$
    * The best case complexity for BST is $$O(\log n)$$ for search as well as insertion, wherein, the tree formed is complete binary tree
    * Whereas, in case like [1,2,3,4,5,6,7,8,...], insertion as well as search for an element becomes $$O(n)$$ in time, since, the tree is skewed in only one direction, and hence, is no better than the array
    * So, in worst case, for searching and insertion over n items, the complexity is $$O(n*n)$$
* Space complexity: $$O(n)$$ extra space for storing the BST in $$\text{Node}$$ class.
<br>
<br>

---
#### Approach 3: BIT

**Intuition**

The problem with BST is that the tree can be skewed hence, making it $$O(n^2)$$ in complexity. So, need a data structure that remains balanced. We could either use a Red-black or AVL tree to make a balanced BST, but the implementation would be an overkill for the solution. We can use BIT (Binary Indexed Tree, also called Fenwick Tree) to ensure that the complexity is $$O(n\log n)$$ with only 12-15 lines of code.

*BIT Overview:*

Fenwick Tree or BIT provides a way to represent an array of numbers in an array(can be visualized as tree), allowing prefix/suffix sums to be calculated efficiently($$O(\log n)$$). BIT allows to update an element in $$O(\log n)$$ time.

We recommend having a look at BIT from the following link before getting into details:

* [https://www.topcoder.com/community/data-science/data-science-tutorials/binary-indexed-trees/](https://www.topcoder.com/community/data-science/data-science-tutorials/binary-indexed-trees/)

So, BIT is very useful to accumulate information from front/back and hence, we can use it in the same way we used BST to get the count of elements that are greater than or equal to $$2 * \text{nums[i]} + 1$$ in the existing tree and then adding the current element to the tree.

**Algorithm**

First, lets review the BIT $$\text{query}$$ and $$\text{update}$$ routines of BIT. According to the convention, $$\text{query}$$ routine goes from $$\text{index}$$ to $$0$$, i.e., $$\text{BIT[i]}$$ gives the sum for the range $$[0,index]$$, and $$\text{update}$$ updates the values from current $$\text{index}$$ to the end of array. But, since, we require to find the numbers greater than the given index, as and when we update an index, we update all the ancestors of the node in the tree, and for $$\text{search}$$, we go from the node to the end.   

The modified $$\text{update}$$ algorithm is:

```python
update(BIT, index, val):
  while(index > 0):
    BIT[index] += val
    index -= (index & (-index))
```

Herein, we find get the next index using: $$\text{index -= index \& (-index)}$$, which is essentially subtracting the rightmost 1 from the $$\text{index}$$ binary representation. We update the previous indices since, if an element is greater than the index

And the modified $$\text{query}$$ algorithm is:

```python
query(BIT,index):
  sum=0
  while(index<BIT.size):
    sum+=BIT[index]
    index+=(index&(-index))
```

Herein, we find get the next index using: $$\text{index += index \& (-index)}$$. This gives the suffix sum from $$index$$ to the end.

So, the main idea is to count the number of elements greater than $$2*\text{nums[i]}$$ in range $$[0,i)$$ as we iterate from $$0$$ to $$\text{size-1}$$. The steps are as follows:

* Create a copy of $$\text{nums}$$, say $$\text{nums\_copy}$$ ans sort $$\text{nums\_copy}$$. This array is actually used for creating the Binary indexed tree
* Initialize $$\text{count}=0$$ and $$\text{BIT}$$ array of size $$\text{size(nums)} + 1$$ to store the BIT
* Iterate over $$i$$ from $$0$$ to $$\text{size(nums)}-1$$ :
    * Search the index of element not less than $$2*\text{nums[i]}+1$$ in $$\text{nums\_copy}$$ array. $$\text{query}$$ the obtained index+1 in the $$\text{BIT}$$, and add the result to $$\text{count}$$
    * Search for the index of element not less than $$nums[i]$$ in $$\text{nums\_copy}$$. We need to $$\text{update}$$ the BIT for this index by 1. This essentially means that 1 is added to this index(or number of elements greater than this index is incremented). The effect of adding $$1$$ to the index is passed to the ancestors as shown in $$\text{update}$$ algorithm


<iframe src="https://leetcode.com/playground/BgYC3F2a/shared" frameBorder="0" width="100%" height="500" name="BgYC3F2a"></iframe>

**Complexity analysis**

* Time complexity: $$O(n\log n)$$
    * In $$\text{query}$$ and $$\text{update}$$ operations, we see that the loop iterates at most the number of bits in $$\text{index}$$ which can be at most $$n$$. Hence, the complexity of both the operations is $$O(\log n)$$(Number of bits in $$n$$ is $$\log n$$)
    * The in-built operation $$\text{lower\_bound}$$ is binary search hence $$O(\log n)$$
    * We perform the operations for $$n$$ elements, hence the total complexity is $$O(n\log n)$$
* Space complexity: $$O(n)$$. Additional space for $$\text{BITS}$$ array
<br>
<br>

---
#### Approach 4: Modified Merge Sort

**Intuition**

In BIT and BST, we iterate over the array, dividing the array into 3 sections: already visited and hence added to the tree, current node and section to be visited. Another approach could be divide the problem into smaller subproblems, solving them and combining these problems to get the final result - Divide and conquer. We see that the problem has a great resemblance to the merge sort routine. The question is to find the inversions such that $$\text{nums[i]}>2 * \text{nums[j]}$$ and $$i<j$$. So, we can easily modify the merge sort to count the inversions as required.

*Mergesort*

Mergesort is a divide-and-conquer based sorting technique that operates in $$O(n\log n)$$ time. The basic idea to divide the array into several sub-arrays until each sub-array is single element long and merging these sublists recursively that results in the final sorted array.

**Algorithm**

We define $$\text{mergesort\_and\_count}$$ routine that takes parameters an array say $$A$$ and $$\text{start}$$ and $$\text{end}$$ indices:

* If $$\text{start}$$>=$$\text{end}$$ this implies that elements can no longer be broken further and hence we return 0
* Otherwise, set $$\text{mid}=(\text{start} + \text{end})/2$$
* Store $$count$$ by recursively calling $$\text{mergesort\_and\_count}$$ on range $$\text{[start,mid]}$$ and $$\text{[mid+1,end]}$$ and adding the results. This is the divide step on our routine, breaking it into the 2 ranges, and finding the results for each range separately
* Now, we that we have separately calculated the results for ranges $$\text{[start,mid]}$$ and $$\text{[mid+1,end]}$$, but we still have to count the elements in $$\text{[start,mid]}$$ that are greater than 2 * elements in $$\text{[mid+1,end]}$$. Count all such elements and add the result to $$\text{count}$$
* Finally, $$\text{merge}$$ the array from $$\text{start}$$ to $$\text{end}$$
    * Make 2 array : $$L$$ from elements in range $$\text{[start,mid]}$$ and $$R$$ from elements in range $$\text{R[mid+1,end]}$$
    * Keep pointers $$i$$ and $$j$$ to $$L$$ and $$R$$ respectively both initialized to start to the arrays
    * Iterate over $$k$$ from $$\text{start}$$ to $$\text{end}$$ and set $$\text{A[k]}$$ to the smaller of $$\text{L[i]}$$ or $$\text{R[j]}$$ and increment the respective index


<iframe src="https://leetcode.com/playground/8NrGXeXT/shared" frameBorder="0" width="100%" height="500" name="8NrGXeXT"></iframe>

**Complexity analysis**

* Time complexity: $$O(n\log n)$$

    * In each step we divide the array into 2 sub-arrays, and hence, the maximum times we need to divide is equal to $$O(\log n)$$
    * Additional $$O(n)$$ work needs to be done to count the inversions and to merge the 2 sub-arrays after sorting. Hence total time complexity is $$O(n\log n)$$

* Space complexity: $$O(n)$$. Additional space for storing $$L$$ and $$R$$ arrays

---
Shoutout to [@FUN4LEETCODE](https://discuss.leetcode.com/user/fun4leetcode) for the brilliant post!

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### General principles behind problems similar to "Reverse Pairs"
- Author: fun4LeetCode
- Creation Date: Tue Feb 14 2017 05:13:10 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 15:00:06 GMT+0800 (Singapore Standard Time)

<p>
It looks like a host of solutions are out there (`BST`-based, `BIT`-based, `Merge-sort`-based). Here I\'d like to focus on the general principles behind these solutions and its possible application to a number of similar problems.

The fundamental idea is very simple: **break down the array and solve for the subproblems**.

A breakdown of an array naturally reminds us of subarrays. To smoothen our following discussion, let\'s assume the input array is `nums`, with a total of `n` elements. Let `nums[i, j]` denote the subarray starting from index `i` to index `j` (both inclusive), `T(i, j)` as the same problem applied to this subarray (for example, for [**Reverse Pairs**](https://leetcode.com/problems/reverse-pairs/?tab=Description),  `T(i, j)` will represent the total number of important reverse pairs for subarray `nums[i, j]`). 

With the definition above, it\'s straightforward to identify our original problem as `T(0, n - 1)`. Now the key point is how to construct solutions to the original problem from its subproblems. This is essentially equivalent to building recurrence relations for `T(i, j)`. Since if we can find solutions to `T(i, j)` from its subproblems, we surely can build solutions to larger subarrays until eventually the whole array is spanned.

While there may be many ways for establishing recurrence relations for `T(i, j)`, here I will only introduce the following two common ones:

1. `T(i, j) = T(i, j - 1) + C`, i.e., elements will be processed sequentially and `C` denotes the subproblem for processing the last element of subarray `nums[i, j]`. We will call this sequential recurrence relation.

2. `T(i, j) = T(i, m) + T(m + 1, j) + C` where `m = (i+j)/2`, i.e., subarray `nums[i, j]` will be further partitioned into two parts and `C` denotes the subproblem for combining the two parts. We will call this partition recurrence relation.

For either case, the nature of the subproblem `C` will depend on the problem under consideration, and it will determine the overall time complexity of the original problem. So usually it\'s crucial to find efficient algorithm for solving this subproblem in order to have better time performance. Also pay attention to possibilities of overlapping subproblems, in which case a dynamic programming (DP) approach would be preferred.

Next, I will apply these two recurrence relations to this problem "Reverse Pairs" and list some solutions for your reference.

---
**`I -- Sequential recurrence relation`**

Again we assume the input array is `nums` with `n` elements and `T(i, j)` denotes the total number of important reverse pairs for subarray `nums[i, j]`. For sequential recurrence relation, we can set `i = 0`, i.e., the subarray always starts from the beginning. Therefore we end up with:

`T(0, j) = T(0, j - 1) + C`

where the subproblem `C` now becomes **"find the number of important reverse pairs with the first element of the pair coming from subarray `nums[0, j - 1]`  while the second element of the pair being `nums[j]`"**.

Note that for a pair `(p, q)` to be an important reverse pair, it has to satisfy the following two conditions:
1. `p < q`: the first element must come before the second element;
2. `nums[p] > 2 * nums[q]`: the first element has to be greater than twice of the second element.

For subproblem `C`, the first condition is met automatically; so we only need to consider the second condition, which is equivalent to searching for all elements within subarray `nums[0, j - 1]` that are greater than twice of `nums[j]`.

The straightforward way of searching would be a linear scan of the subarray, which runs at the order of `O(j)`. From the sequential recurrence relation, this leads to the naive `O(n^2)` solution.

To improve the searching efficiency, a key observation is that the order of elements in the subarray does not matter, since we are only interested in the total number of important reverse pairs. This suggests we may sort those elements and do a binary search instead of a plain linear scan.

If the searching space (formed by elements over which the search will be done) is "static" (it does not vary from run to run), placing the elements into an array would be perfect for us to do the binary search. However, this is not the case here. After the `j-th` element is processed, we need to add it to the searching space so that it becomes searchable for later elements, which renders the searching space expanding as more and more elements are processed.

Therefore we\'d like to strike a balance between searching and insertion operations. This is where data structures like binary search tree (`BST`) or binary indexed tree (`BIT`) prevail, which offers relatively fast performance for both operations.

**1. `BST`-based solution**

we will define the tree node as follows, where `val` is the node value and `cnt` is the total number of elements in the subtree rooted at current node that are greater than or equal to `val`:
```
class Node {
    int val, cnt;
    Node left, right;
        
    Node(int val) {
        this.val = val;
        this.cnt = 1;
    }
}
```
The searching and insertion operations can be done as follows:
```
private int search(Node root, long val) {
    if (root == null) {
    	return 0;
    } else if (val == root.val) {
    	return root.cnt;
    } else if (val < root.val) {
    	return root.cnt + search(root.left, val);
    } else {
    	return search(root.right, val);
    }
}

private Node insert(Node root, int val) {
    if (root == null) {
        root = new Node(val);
    } else if (val == root.val) {
        root.cnt++;
    } else if (val < root.val) {
        root.left = insert(root.left, val);
    } else {
        root.cnt++;
        root.right = insert(root.right, val);
    }
    
    return root;
}
```
And finally the main program, in which we will search for all elements no less than twice of current element plus `1` (converted to `long` type to avoid overflow) while insert the element itself into the BST. 

**Note:** this homemade BST is not self-balanced and the time complexity can go as bad as `O(n^2)` (in fact you will get `TLE` if you copy and paste the solution here). To guarantee `O(nlogn)` performance, use one of the self-balanced BST\'s (e.g. `Red-black` tree, `AVL` tree, etc.).
```
public int reversePairs(int[] nums) {
    int res = 0;
    Node root = null;
    	
    for (int ele : nums) {
        res += search(root, 2L * ele + 1);
        root = insert(root, ele);
    }
    
    return res;
}
```

**2. `BIT`-based solution**

For `BIT`, the searching and insertion operations are:
```
private int search(int[] bit, int i) {
    int sum = 0;
    
    while (i < bit.length) {
        sum += bit[i];
        i += i & -i;
    }

    return sum;
}

private void insert(int[] bit, int i) {
    while (i > 0) {
        bit[i] += 1;
        i -= i & -i;
    }
}
```
And the main program, where again we will search for all elements greater than twice of current element while insert the element itself into the BIT. For each element, the `"index"` function will return its index in the `BIT`. Unlike the BST-based solution, this is guaranteed to run at `O(nlogn)`.
```
public int reversePairs(int[] nums) {
    int res = 0;
    int[] copy = Arrays.copyOf(nums, nums.length);
    int[] bit = new int[copy.length + 1];
    
    Arrays.sort(copy);
    
    for (int ele : nums) {
        res += search(bit, index(copy, 2L * ele + 1));
        insert(bit, index(copy, ele));
    }
    
    return res;
}

private int index(int[] arr, long val) {
    int l = 0, r = arr.length - 1, m = 0;
    	
    while (l <= r) {
    	m = l + ((r - l) >> 1);
    		
    	if (arr[m] >= val) {
    	    r = m - 1;
    	} else {
    	    l = m + 1;
    	}
    }
    
    return l + 1;
}
```

More explanation for the BIT-based solution:
1. We want the elements to be sorted so there is a sorted version of the input array which is `copy`.

2. The `bit` is built upon this sorted array. Its length is one greater than that of the `copy` array to account for the root.

3. Initially the `bit` is empty and we start doing a sequential scan of the input array. For each element being scanned, we first search the `bit` to find all elements greater than twice of it and add the result to `res`. We then insert the element itself into the `bit` for future search.

4. Note that conventionally searching of the `bit` involves traversing towards the root from some index of the `bit`, which will yield a predefined running total of the `copy` array up to the corresponding index. For insertion, the traversing direction will be opposite and go from some index towards the end of the `bit` array.

5. For each scanned element of the input array, its searching index will be given by the index of the first element in the `copy` array that is greater than twice of it (shifted up by `1` to account for the root), while its insertion index will be the index of the first element in the `copy` array that is no less than itself (again shifted up by `1`). This is what the `index` function is for.

6. For our case, the running total is simply the number of elements encountered during the traversal process. If we stick to the convention above, the running total will be the number of elements smaller than the one at the given index, since the `copy` array is sorted in ascending order. However, we\'d actually like to find the number of elements greater than some value (i.e., twice of the element being scanned), therefore we need to flip the convention. This is what you see inside the `search` and `insert` functions: the former traversing towards the end of the `bit` while the latter towards the root.

---
**`II -- Partition recurrence relation`**

For partition recurrence relation, setting `i = 0, j = n - 1, m = (n-1)/2`, we have:

`T(0, n - 1) = T(0, m) + T(m + 1, n - 1) + C`

where the subproblem `C` now reads **"find the number of important reverse pairs with the first element of the pair coming from the left subarray `nums[0, m]`  while the second element of the pair coming from the right subarray `nums[m + 1, n - 1]`"**.

Again for this subproblem, the first of the two aforementioned conditions is met automatically. As for the second condition, we have as usual this plain linear scan algorithm, applied for each element in the left (or right) subarray. This, to no surprise, leads to the `O(n^2)` naive solution.

Fortunately the observation holds true here that the order of elements in the left or right subarray does not matter, which prompts sorting of elements in both subarrays. With both subarrays sorted, the number of important reverse pairs can be found in linear time by employing the so-called two-pointer technique: one pointing to elements in the left subarray while the other to those in the right subarray and both pointers will go only in one direction due to the ordering of the elements.

The last question is which algorithm is best here to sort the subarrays. Since we need to partition the array into halves anyway, it is most natural to adapt it into a `Merge-sort`. Another point in favor of `Merge-sort` is that the searching process above can be embedded seamlessly into its merging stage.

So here is the `Merge-sort`-based solution, where the function `"reversePairsSub"` will return the total number of important reverse pairs within subarray `nums[l, r]`. The two-pointer searching process is represented by the nested `while` loop involving variable `p`, while the rest is the standard merging algorithm.
```
public int reversePairs(int[] nums) {
    return reversePairsSub(nums, 0, nums.length - 1);
}
    
private int reversePairsSub(int[] nums, int l, int r) {
    if (l >= r) return 0;
        
    int m = l + ((r - l) >> 1);
    int res = reversePairsSub(nums, l, m) + reversePairsSub(nums, m + 1, r);
        
    int i = l, j = m + 1, k = 0, p = m + 1;
    int[] merge = new int[r - l + 1];
        
    while (i <= m) {
        while (p <= r && nums[i] > 2 L * nums[p]) p++;
        res += p - (m + 1);
				
        while (j <= r && nums[i] >= nums[j]) merge[k++] = nums[j++];
        merge[k++] = nums[i++];
    }
        
    while (j <= r) merge[k++] = nums[j++];
        
    System.arraycopy(merge, 0, nums, l, merge.length);
        
    return res;
}
```

---
**`III -- Summary`**

Many problems involving arrays can be solved by breaking down the problem into subproblems applied on subarrays and then link the solution to the original problem with those of the subproblems, to which we have sequential recurrence relation and partition recurrence relation. For either case, it\'s crucial to identify the subproblem `C` and find efficient algorithm for approaching it.

If the subproblem `C` involves searching on "dynamic searching space", try to consider data structures that support relatively fast operations on both searching and updating (such as `self-balanced BST`, `BIT`, `Segment tree`, `...`).

If the subproblem `C` of partition recurrence relation involves sorting, `Merge-sort` would be a nice sorting algorithm to use. Also, the code could be made more elegant if the solution to the subproblem can be embedded into the merging process.

If there are overlapping among the subproblems `T(i, j)`, it\'s preferable to cache the intermediate results for future lookup.

Lastly let me name a few leetcode problems that fall into the patterns described above and thus can be solved with similar ideas.

[315. Count of Smaller Numbers After Self](https://leetcode.com/problems/count-of-smaller-numbers-after-self/)
[327. Count of Range Sum](https://leetcode.com/problems/count-of-range-sum/)

For `leetcode 315`, applying the sequential recurrence relation (with `j` fixed), the subproblem `C` reads: **find the number of elements out of visited ones that are smaller than current element**, which involves searching on "dynamic searching space"; applying the partition recurrence relation, we have a subproblem `C`: **for each element in the left half, find the number of elements in the right half that are smaller than it**, which can be embedded into the merging process by noting that these elements are exactly those swapped to its left during the merging process.

For `leetcode 327`, applying the sequential recurrence relation (with `j` fixed) on the pre-sum array, the subproblem `C` reads: **find the number of elements out of visited ones that are within the given range**, which again involves searching on "dynamic searching space"; applying the partition recurrence relation, we have a subproblem `C`: **for each element in the left half, find the number of elements in the right half that are within the given range**, which can be embedded into the merging process using the two-pointer technique.

Anyway, hope these ideas can sharpen your skills for solving array-related problems.
</p>


### Very Short and Clear MergeSort & BST Java Solutions
- Author: chidong
- Creation Date: Sun Feb 12 2017 12:10:30 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 03:33:33 GMT+0800 (Singapore Standard Time)

<p>
**MergeSort**

**Explanation:**  In each round, we divide our array into two parts and sort them. So after "int cnt = mergeSort(nums, s, mid) + mergeSort(nums, mid+1, e); ", the left part and the right part are sorted and now our only job is to count how many pairs of number (leftPart[i], rightPart[j]) satisfies leftPart[i] <= 2*rightPart[j]. 
For example, 
left: 4 6 8   right: 1 2 3
so we use two pointers to travel left and right parts. For each leftPart[i], if j<=e && nums[i]/2.0 > nums[j], we just continue to move j to the end, to increase rightPart[j], until it is valid. Like in our example, left's 4 can match 1 and 2; left's 6 can match 1, 2, 3, and left's 8 can match 1, 2, 3. So in this particular round, there are 8 pairs found, so we increases our total by 8. 

```
public class Solution {
    public int reversePairs(int[] nums) {
        return mergeSort(nums, 0, nums.length-1);
    }
    private int mergeSort(int[] nums, int s, int e){
        if(s>=e) return 0; 
        int mid = s + (e-s)/2; 
        int cnt = mergeSort(nums, s, mid) + mergeSort(nums, mid+1, e); 
        for(int i = s, j = mid+1; i<=mid; i++){
            while(j<=e && nums[i]/2.0 > nums[j]) j++; 
            cnt += j-(mid+1); 
        }
        Arrays.sort(nums, s, e+1); 
        return cnt; 
    }
}
```
**Or:** 
Because left part and right part are sorted, you can replace the Arrays.sort() part with a actual merge sort process. The previous version is easy to write, while this one is faster. 

```
public class Solution {
    int[] helper;
    public int reversePairs(int[] nums) {
        this.helper = new int[nums.length];
        return mergeSort(nums, 0, nums.length-1);
    }
    private int mergeSort(int[] nums, int s, int e){
        if(s>=e) return 0; 
        int mid = s + (e-s)/2; 
        int cnt = mergeSort(nums, s, mid) + mergeSort(nums, mid+1, e); 
        for(int i = s, j = mid+1; i<=mid; i++){
            while(j<=e && nums[i]/2.0 > nums[j]) j++; 
            cnt += j-(mid+1); 
        }
        //Arrays.sort(nums, s, e+1); 
        myMerge(nums, s, mid, e);
        return cnt; 
    }
    
    private void myMerge(int[] nums, int s, int mid, int e){
        for(int i = s; i<=e; i++) helper[i] = nums[i];
        int p1 = s;//pointer for left part
        int p2 = mid+1;//pointer for rigth part
        int i = s;//pointer for sorted array
        while(p1<=mid || p2<=e){
            if(p1>mid || (p2<=e && helper[p1] >= helper[p2])){
                nums[i++] = helper[p2++];
            }else{
                nums[i++] = helper[p1++];
            }
        }
    }
}
```


------------------------------------------------------------------------

**BST**
BST solution is no longer acceptable, because it's performance can be very bad, O(n^2) actually, for extreme cases like [1,2,3,4......49999], due to the its unbalance, but I am still providing it below just FYI. 
We build the Binary Search Tree from right to left, and at the same time, search the partially built tree with nums[i]/2.0. The code below should be clear enough. 
```
public class Solution {
    public int reversePairs(int[] nums) {
        Node root = null;
        int[] cnt = new int[1];
        for(int i = nums.length-1; i>=0; i--){
            search(cnt, root, nums[i]/2.0);//search and count the partially built tree
            root = build(nums[i], root);//add nums[i] to BST
        }
        return cnt[0];
    }
    
    private void search(int[] cnt, Node node, double target){
        if(node==null) return; 
        else if(target == node.val) cnt[0] += node.less;
        else if(target < node.val) search(cnt, node.left, target);
        else{
            cnt[0]+=node.less + node.same; 
            search(cnt, node.right, target);
        }
    }
    
    private Node build(int val, Node n){
        if(n==null) return new Node(val);
        else if(val == n.val) n.same+=1;
        else if(val > n.val) n.right = build(val, n.right);
        else{
            n.less += 1;
            n.left = build(val, n.left);
        }
        return n;
    }
    
    class Node{
        int val, less = 0, same = 1;//less: number of nodes that less than this node.val
        Node left, right;
        public Node(int v){
            this.val = v;
        }
    }
}
```
Similar to this https://leetcode.com/problems/count-of-smaller-numbers-after-self/. But the main difference is: here, the number to add and the number to search are different (add nums[i], but search nums[i]/2.0), so not a good idea to combine build and search together.
</p>


### C++ with iterators
- Author: StefanPochmann
- Creation Date: Sun Feb 12 2017 16:35:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 12 2017 16:35:06 GMT+0800 (Singapore Standard Time)

<p>
Just a mergesort solution, but using iterators (instead of indexes) and `inplace_merge`.

```
class Solution {
public:
    int sort_and_count(vector<int>::iterator begin, vector<int>::iterator end) {
        if (end - begin <= 1)
            return 0;
        auto mid = begin + (end - begin) / 2;
        int count = sort_and_count(begin, mid) + sort_and_count(mid, end);
        for (auto i = begin, j = mid; i != mid; ++i) {
            while (j != end and *i > 2L * *j)
                ++j;
            count += j - mid;
        }
        inplace_merge(begin, mid, end);
        return count;
    }

    int reversePairs(vector<int>& nums) {
        return sort_and_count(nums.begin(), nums.end());
    }
};
```
</p>


