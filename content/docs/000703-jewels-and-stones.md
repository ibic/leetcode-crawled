---
title: "Jewels and Stones"
weight: 703
#id: "jewels-and-stones"
---
## Description
<div class="description">
<p>You&#39;re given strings <code>J</code> representing the types of stones that are jewels, and <code>S</code> representing the stones you have.&nbsp; Each character in <code>S</code> is a type of stone you have.&nbsp; You want to know how many of the stones you have are also jewels.</p>

<p>The letters in <code>J</code> are guaranteed distinct, and all characters in <code>J</code> and <code>S</code> are letters. Letters are case sensitive, so <code>&quot;a&quot;</code> is considered a different type of stone from <code>&quot;A&quot;</code>.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> J = &quot;aA&quot;, S = &quot;aAAbbbb&quot;
<strong>Output:</strong> 3
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> J = &quot;z&quot;, S = &quot;ZZ&quot;
<strong>Output:</strong> 0
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li><code>S</code> and <code>J</code> will consist of letters and have length at most 50.</li>
	<li>The characters in <code>J</code> are distinct.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Amazon - 4 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Brute Force [Accepted]

**Intuition and Algorithm**

For each stone, check whether it matches any of the jewels.  We can check with a linear scan.

<iframe src="https://leetcode.com/playground/RDBuKEvs/shared" frameBorder="0" width="100%" height="259" name="RDBuKEvs"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(J\text{.length} * S\text{.length}))$$.

* Space Complexity: $$O(1)$$ additional space complexity in Python.  In Java, this can be $$O(J\text{.length} * S\text{.length}))$$ because of the creation of new arrays.

---
#### Approach #2: Hash Set [Accepted]

**Intuition and Algorithm**

For each stone, check whether it matches any of the jewels.  We can check efficiently with a *Hash Set*.

<iframe src="https://leetcode.com/playground/oKA7PWkd/shared" frameBorder="0" width="100%" height="276" name="oKA7PWkd"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(J\text{.length} + S\text{.length}))$$.  The $$O(J\text{.length})$$ part comes from creating `J`.  The $$O(S\text{.length})$$ part comes from searching `S`.

* Space Complexity: $$O(J\text{.length})$$.

## Accepted Submission (java)
```java
class Solution {
    public int numJewelsInStones(String J, String S) {
        char[] ja = J.toCharArray();
        char[] sa = S.toCharArray();
        Map<Character, Integer> sm = new HashMap<Character, Integer>();
        for (char s: sa) {
            if (sm.containsKey(s)) {
                sm.put(s, sm.get(s) + 1);
            } else {
                sm.put(s, 1);
            }
        }
        int result = 0;
        for (char j: ja) {
            if (sm.containsKey(j)) {
                result += sm.get(j);
            }
        }
        return result;
    }
}
```

## Top Discussions
### 1-liners Python/Java/Ruby
- Author: StefanPochmann
- Creation Date: Sun Jan 28 2018 19:26:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 14:34:22 GMT+0800 (Singapore Standard Time)

<p>
### Ruby
```
def num_jewels_in_stones(j, s)
  s.count(j)
end
```
```
def num_jewels_in_stones(j, s)
  s.scan(/[#{j}]/).size
end
```
```
def num_jewels_in_stones(j, s)
  s.chars.count { |c| j.include?(c) }
end
```

### Python
```
def numJewelsInStones(self, J, S):
    return sum(map(J.count, S))
```
```
def numJewelsInStones(self, J, S):
    return sum(map(S.count, J))               # this one after seeing https://discuss.leetcode.com/post/244105
```
```
def numJewelsInStones(self, J, S):
    return sum(s in J for s in S)
```
### Java

    public int numJewelsInStones(String J, String S) {
        return S.replaceAll("[^" + J + "]", "").length();
    }
</p>


### [C++/Java/Python] Set Solution O(J + S)
- Author: lee215
- Creation Date: Sun Jan 28 2018 12:31:45 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 27 2019 19:58:03 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
1. read `J` and build jewels hash set.
2. read `S` and count jewels.

## **Complexity**
I used hash set and it\'s `O(1)` time to check if it contains an element.
So the total time complexity will be **O(J+S)**, instead of O(JS)
Space is `O(J)`


**Java**
```java
    public int numJewelsInStones(String J, String S) {
        int res = 0;
        Set setJ = new HashSet();
        for (char j: J.toCharArray())
            setJ.add(j);
        for (char s: S.toCharArray())
            if (setJ.contains(s)) res++;
        return res;
    }
```


**C++**
```cpp
    int numJewelsInStones(string J, string S) {
        int res = 0;
        unordered_set<char> setJ(J.begin(), J.end());
        for (char s : S)
            if (setJ.count(s)) res++;
        return res;
    }
```

**Python**
```python
def numJewelsInStones(self, J, S):
        setJ = set(J)
        return sum(s in setJ for s in S)
```
</p>


### [Java] Count - Clean code - O(j+s)
- Author: hiepit
- Creation Date: Sat May 02 2020 15:41:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 02 2020 15:44:59 GMT+0800 (Singapore Standard Time)

<p>
```java
class Solution {
    public int numJewelsInStones(String J, String S) {
        int[] cnt = new int[128];
        for (char c : S.toCharArray())
            cnt[c]++;
        int ans = 0;
        for (char c : J.toCharArray())
            ans += cnt[c];
        return ans;
    }
}
```
**Complexity**
- Time: `O(j+s)`, where `j` is length of string `J` and `s` is the length of string `S`
- Space: `O(1)`
</p>


