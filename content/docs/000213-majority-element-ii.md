---
title: "Majority Element II"
weight: 213
#id: "majority-element-ii"
---
## Description
<div class="description">
<p>Given an integer array of size <code>n</code>, find all elements that appear more than <code>&lfloor; n/3 &rfloor;</code> times.</p>

<p><strong>Follow-up: </strong>Could you solve the problem&nbsp;in linear time and in O(1) space?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,2,3]
<strong>Output:</strong> [3]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1]
<strong>Output:</strong> [1]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2]
<strong>Output:</strong> [1,2]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 5 * 10<sup>4</sup></code></li>
	<li><code>-10<sup>9</sup> &lt;= nums[i] &lt;= 10<sup>9</sup></code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Amazon - 6 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Zenefits - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

This problem can be approached similarly to [Majority Element](https://leetcode.com/problems/majority-element/). For this problem, two constraints we have to satisfy are linear runtime and constant space. In this article, we will focus on the solution which satisfies both constraints.

---

#### Approach 1: Boyer-Moore Voting Algorithm

**Intuition**

To figure out a $$O(1)$$ space requirement, we would need to get this simple intuition first. For an array of length `n`:

* There can be at most **one** majority element which is **more than** `⌊n/2⌋` times.
* There can be at most **two** majority elements which are **more than** `⌊n/3⌋` times.
* There can be at most **three** majority elements which are **more than** `⌊n/4⌋` times.

and so on.

Knowing this can help us understand how we can keep track of majority elements which satisfies $$O(1)$$ space requirement.

Let's try to get an intuition for the case where we would like to find a majority element which is more than `⌊n/2⌋` times in an array of length `n`.

The idea is to have two variables, one holding a potential candidate for majority element and a counter to keep track of whether to swap a potential candidate or not. Why can we get away with only two variables? Because *there can be at most **one** majority element which is more than `⌊n/2⌋` times*. Therefore, having only one variable to hold the only potential candidate and one counter is enough.

While scanning the array, the counter is incremented if you encounter an element which is exactly same as the potential candidate but decremented otherwise. When the counter reaches zero, the element which will be encountered next will become the potential candidate. Keep doing this procedure while scanning the array. However, when you have exhausted the array, you have to make sure that the element recorded in the potential candidate variable is the majority element by checking whether it occurs more than `⌊n/2⌋` times in the array. In the original [Majority Element](https://leetcode.com/problems/majority-element/) problem, it is guaranteed that there is a majority element in the array so your implementation can omit the second pass. However, in a general case, you need this second pass since your array can have no majority elements at all!

The counter is initialized as `0` and the potential candidate as `None` at the start of the array.

!?!../Documents/229_majority_element_ii_first.json:1200,600!?!

If an element is truly a majority element, it will stick in the potential candidate variable, no matter how it shows up in the array (i.e. all clustered in the beginning of the array, all clustered near the end of the array, or showing up anywhere in the array), after the whole array has been scanned. Of course, while you are scanning the array, the element might be replaced by another element in the process, but the true majority element will definitely remain as the potential candidate in the end.

Now figuring out the majority elements which show up more than `⌊n/3⌋` times is not that hard anymore. Using the intuition presented in the beginning, we only need four variables: two for holding two potential candidates and two for holding two corresponding counters. Similar to the above case, both candidates are initialized as `None` in the beginning with their corresponding counters being 0. While going through the array:

* If the current element is equal to one of the potential candidate, the count for that candidate is increased while leaving the count of the other candidate as it is.
* If the counter reaches zero, the candidate associated with that counter will be replaced with the next element **if** the next element is not equal to the other candidate as well. 
* Both counters are decremented **only when** the current element is different from both candidates. 

!?!../Documents/229_majority_element_ii_second.json:1200,600!?!

**Implementation**

<iframe src="https://leetcode.com/playground/cgXiFr5G/shared" frameBorder="0" width="100%" height="500" name="cgXiFr5G"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N)$$ where $$N$$ is the size of `nums`. We first go through `nums` looking for first and second potential candidates. We then count the number of occurrences for these two potential candidates in `nums`. Therefore, our runtime is $$O(N) + O(N) = O(2N) \approx O(N)$$.

* Space complexity : $$O(1)$$ since we only have four variables for holding two potential candidates and two counters. Even the returning array is at most 2 elements.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Boyer-Moore Majority Vote algorithm and my elaboration
- Author: orbuluh
- Creation Date: Thu Jul 02 2015 01:58:34 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 20:57:34 GMT+0800 (Singapore Standard Time)

<p>
For those who aren't familiar with Boyer-Moore Majority Vote algorithm, 
I found a great article (http://goo.gl/64Nams) that helps me to understand this fantastic algorithm!!
Please check it out!

The essential concepts is you keep a counter for the majority number **X**. If you find a number **Y** that is not **X**, the current counter should deduce 1. The reason is that if there is 5 **X** and 4 **Y**, there would be one (5-4) more **X** than **Y**. This could be explained as "4 **X** being paired out by 4 **Y**".

And since the requirement is finding the majority for more than ceiling of [n/3], the answer would be less than or equal to two numbers. 
So we can modify the algorithm to maintain two counters for two majorities.

Followings are my sample Python code:

    class Solution:
    # @param {integer[]} nums
    # @return {integer[]}
    def majorityElement(self, nums):
        if not nums:
            return []
        count1, count2, candidate1, candidate2 = 0, 0, 0, 1
        for n in nums:
            if n == candidate1:
                count1 += 1
            elif n == candidate2:
                count2 += 1
            elif count1 == 0:
                candidate1, count1 = n, 1
            elif count2 == 0:
                candidate2, count2 = n, 1
            else:
                count1, count2 = count1 - 1, count2 - 1
        return [n for n in (candidate1, candidate2)
                        if nums.count(n) > len(nums) // 3]
</p>


### 6 lines, general case O(N) time and O(k) space
- Author: StefanPochmann
- Creation Date: Mon Jun 29 2015 18:53:41 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 01:31:14 GMT+0800 (Singapore Standard Time)

<p>
**Solution**

I keep up to two candidates in my counter, so this fulfills the O(N) time and O(1) space requirements.

    def majorityElement(self, nums):
        ctr = collections.Counter()
        for n in nums:
            ctr[n] += 1
            if len(ctr) == 3:
                ctr -= collections.Counter(set(ctr))
        return [n for n in ctr if nums.count(n) > len(nums)/3]

---

**Explanation**

Think of it this way: Find three different votes and hide them. Repeat until there aren't three different votes left. A number that originally had more than one third of the votes now still has at least one vote, because to hide *all* of its votes you would've had to hide more than three times one third of the votes - more votes than there were. You can easily have false positives, though, so in the end check whether the remaining up to two candidates actually had more than one third of the votes.

My code does just that: Collect (count) the votes for every number, but remove triples of three different votes on the fly, as soon as we have such a triple.

---

**Generalization to \u230aN/k\u230b, still O(N) time but O(k) space**

For the general problem, looking for elements appearing more than \u230aN/k\u230b times for some positive integer k, I just have to change my `3` to `k`. Then it already works and takes takes O(k) space and O(kN) time.

The O(kN) time does **not** come from the main loop, though. Yes, each `ctr -= ...` does cost k, but I only have to do it at most N/k times. To put it in terms of the above explanation, I can't hide a vote more than once.

No, the culprit is my last line, counting each remaining candidate separately. If I count them at the same time, I get O(N) again. Here's the full generalized code:

    def majorityElement(self, nums, k):
        ctr = collections.Counter()
        for n in nums:
            ctr[n] += 1
            if len(ctr) == k:
                ctr -= collections.Counter(set(ctr))
        ctr = collections.Counter(n for n in nums if n in ctr)
        return [n for n in ctr if ctr[n] > len(nums)/k]
</p>


### My understanding of Boyer-Moore Majority Vote
- Author: ifyouseewendy
- Creation Date: Thu Oct 27 2016 11:54:09 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 02:55:36 GMT+0800 (Singapore Standard Time)

<p>
This problem is an extension to [169. Majority Element](https://leetcode.com/problems/majority-element/), which needs Boyer-Moore Majority Vote Algorithm to find the element, whose count is over `n/2`.

When I was learning about Boyer-Moore, I was always thinking about **the pair**. I drew a picture to get myself understandable.

Suppose there are nine elements in array **A**, and the round one is the majority.

![0_1477537808895_upload-f2ddd14f-9954-4025-b77a-40137c5abf06](/uploads/files/1477537810177-upload-f2ddd14f-9954-4025-b77a-40137c5abf06.png) 

No matter in what order we select element from the array, we can only get two results

![0_1477537956098_upload-e3d23d8b-0d43-4f8f-ace1-065bd0928493](/uploads/files/1477537957428-upload-e3d23d8b-0d43-4f8f-ace1-065bd0928493.png) 

Compared to fully pairing, it is a little wasting of the partially pairing as there are some round ones are not paired (definitely it would be left). So, under the condition that the majority element exists, we could only think about **the fully pairing situation**. (It's useful when dealing with `n/3` situation)

We can consider either column as the candidate, and it's intuitive for me to get understand that the code means found a pair.

```
if candidate != element
  count -= 1
end
```

![0_1477539703014_upload-2186f2ff-dc3d-4324-a3ce-5f7ade11a2da](/uploads/files/1477539704324-upload-2186f2ff-dc3d-4324-a3ce-5f7ade11a2da.png) 

So here comes the `n/3` problem, we would only think about the fully pairing situation. If the over one third majority exists, it should be left after pairing. 

![0_1477539890642_upload-1c838025-3ff3-4fa9-ae23-abd8b7e10be9](/uploads/files/1477539893194-upload-1c838025-3ff3-4fa9-ae23-abd8b7e10be9.png)
Why would we use three elements as a pair? Because it makes sure that in fully pairing the count of majority element equals `n/3`.

That's my understanding about Boyer-Moore. Maybe it's not so clear, but it helps me think about it.

*Code*

```ruby
# Modified Boyer-Moore Majority Voting Algorithm
def majority_element(nums)
  candidate1, candidate2 = 0, 0
  count1, count2 = 0, 0

  # first round to find candidates
  nums.each do |num|
    if candidate1 == num
      count1 += 1

    elsif candidate2 == num
      count2 += 1

    elsif count1 == 0
      candidate1 = num
      count1 += 1

    elsif count2 == 0
      candidate2 = num
      count2 += 1

    else
      # This condition is important, which means a pair out,
      # filtering a set of three elements out

      count1 -= 1
      count2 -= 1
    end
  end

  # second round to confirm
  result = []
  [candidate1, candidate2].uniq.each do |candidate|
    result << candidate if nums.count(candidate) > (nums.count/3)
  end

  result
end
```

*Reference*

+ [Boyer-Moore Majority Vote algorithm and my elaboration](https://discuss.leetcode.com/topic/17564/boyer-moore-majority-vote-algorithm-and-my-elaboration) by [orbuluh](https://discuss.leetcode.com/user/orbuluh)
+ [Majority Voting Algorithm - Blog of Greg Grothaus](https://gregable.com/2013/10/majority-vote-algorithm-find-majority.html)
+ [Boyer-Moore Majority Vote Algorithm](http://www.cs.rug.nl/~wim/pub/whh348.pdf)
</p>


