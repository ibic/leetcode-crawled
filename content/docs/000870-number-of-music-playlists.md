---
title: "Number of Music Playlists"
weight: 870
#id: "number-of-music-playlists"
---
## Description
<div class="description">
<p>Your music player contains <code>N</code>&nbsp;different songs and she wants to listen to <code>L</code><strong> </strong>(not necessarily different) songs during your trip. &nbsp;You&nbsp;create&nbsp;a playlist so&nbsp;that:</p>

<ul>
	<li>Every song is played at least once</li>
	<li>A song can only be played again only if&nbsp;<code>K</code>&nbsp;other songs have been played</li>
</ul>

<p>Return the number of possible playlists.&nbsp; <strong>As the answer can be very large, return it modulo <code>10^9 + 7</code></strong>.</p>

<p>&nbsp;</p>

<div>
<div>
<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>N = <span id="example-input-1-1">3</span>, L = <span id="example-input-1-2">3</span>, K = <span id="example-input-1-3">1</span>
<strong>Output: </strong><span id="example-output-1">6
<strong>Explanation</strong>: </span><span>There are 6 possible playlists. [1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1].</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>N = <span id="example-input-2-1">2</span>, L = <span id="example-input-2-2">3</span>, K = <span id="example-input-2-3">0</span>
<strong>Output: </strong><span id="example-output-2">6
</span><span id="example-output-1"><strong>Explanation</strong>: </span><span>There are 6 possible playlists. [1, 1, 2], [1, 2, 1], [2, 1, 1], [2, 2, 1], [2, 1, 2], [1, 2, 2]</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>N = <span id="example-input-3-1">2</span>, L = <span id="example-input-3-2">3</span>, K = <span id="example-input-3-3">1</span>
<strong>Output: </strong><span id="example-output-3">2
<strong>Explanation</strong>: </span><span>There are 2 possible playlists. [1, 2, 1], [2, 1, 2]</span>
</pre>
</div>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= K &lt; N &lt;= L &lt;= 100</code></li>
</ol>
</div>
</div>
</div>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Salesforce - 4 (taggedByAdmin: false)
- Coursera - 10 (taggedByAdmin: true)
- Google - 4 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Dynamic Programming

**Intuition**

Let `dp[i][j]` be the number of playlists of length `i` that have exactly `j` unique songs.  We want `dp[L][N]`, and it seems likely we can develop a recurrence for `dp`.

**Algorithm**

Consider `dp[i][j]`.  Last song, we either played a song for the first time or we didn't.  If we did, then we had `dp[i-1][j-1] * (N-j)` ways to choose it.  If we didn't, then we repeated a previous song in `dp[i-1][j] * max(j-K, 0)` ways (`j` of them, except the last `K` ones played are banned.)

<iframe src="https://leetcode.com/playground/NFFotEX5/shared" frameBorder="0" width="100%" height="327" name="NFFotEX5"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(NL)$$.

* Space Complexity:  $$O(NL)$$.  (However, we can adapt this algorithm to only store the last row of `dp` to easily get $$O(L)$$ space complexity.)
<br />
<br />


---
#### Approach 2: Partitions + Dynamic Programming

(*Note: This solution is extremely challenging, but is a natural consequence of trying to enumerate the playlists in this manner.*)

**Intuition**

Since we are interested in playing every song at least once, let's keep track of what times $$x = (x_1, x_2, \cdots)$$ a song was played that wasn't yet played before.  For example, if we have 5 songs `abcde`, and we play `abacabdcbaeacbd`, then $$x = (1, 2, 4, 7, 11)$$ as these are the first occurrences of a unique song.  For convenience, we'll also put $$x_{N+1} = L+1$$.  Our strategy is to count the number of playlists $$\#_x$$ that satisfy this $$x$$, so that our final answer will be $$\sum \#_x$$.  

Doing a direct count,

$$
\#_x = N * (N-1) * \cdots * (N-K+1) 1^{x_{K+1} - x_K - 1} * (N-K+2)  2^{x_{K+2} - x_{K+1}} * \cdots
$$

$$
\Rightarrow \#_x = N! \prod_{j=1}^{N-K+1} j^{x_{K+j} - x_{K+j-1} - 1}
$$

Now, let $$\delta_i = x_{K+i} - x_{K+i-1} - 1$$, so that $$\sum \delta_i = L-N$$.  To recap, the final answer will be (for $$S = L-N, P = N-K+1$$):

$$
N! \Big(\sum\limits_{\delta : \sum\limits_{0 \leq i \leq P} \delta_i = S} \prod\limits_{j=1}^P j^{\delta_j} \Big)
$$

For convenience, let's denote the stuff in the large brackets as $$\langle S, P\rangle$$.

**Algorithm**

We can develop a recurrence for $$\langle S, P\rangle$$ mathematically, by factoring out the $$P^{\delta_P}$$ term.

$$
\langle S, P\rangle = \sum_{\delta_P = 0}^S P^{\delta_P} \sum_{\sum\limits_{0\leq i < P} \delta_i = S - \delta_P} \prod\limits_{j=1}^{P-1} j^{\delta_j}
$$

$$
\Rightarrow \langle S, P\rangle = \sum_{\delta_P = 0}^S P^{\delta_P} \langle S - \delta_P, P-1\rangle
$$

so that it can be shown through algebraic manipulation that:
$$
\langle S, P \rangle = P \langle S-1, P-1 \rangle + \langle S, P-1 \rangle
$$

With this recurrence, we can perform dynamic programming similar to Approach 1.  The final answer is $$N! \langle L-N, N-K+1 \rangle$$.

<iframe src="https://leetcode.com/playground/FV3S6hzG/shared" frameBorder="0" width="100%" height="395" name="FV3S6hzG"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(NL)$$.

* Space Complexity:  $$O(L)$$.
<br />
<br />


---
#### Approach 3: Generating Functions

(*Note: This solution is extremely challenging and not recommended for interviews, but is included here for completeness.*)

**Analysis**

Following the terminology of Approach 2, we would like to compute $$\langle S, P \rangle$$ quickly.  We can use generating functions.

For a fixed $$P$$, consider the function:

$$
f(x) = (1^0x^0 + 1^1x^1 + 1^2x^2 + 1^3x^3 + \cdots) * (2^0x^0 + 2^1x^1 + 2^2x^2 + 2^3x^3 + \cdots)
$$
$$
\cdots * (P^0x^0 + P^1x^1 + P^2x^2 + P^3x^3 + \cdots)
$$

$$
\Leftrightarrow f(x) = \prod_{k=1}^{P} (\sum_{j \geq 0} k^j x^j) = \prod_{k=1}^P \frac{1}{1-kx}
$$

The coefficient of $$x^S$$ in $$f$$ (denoted $$[x^S]f$$) is the desired $$\langle S, P \rangle$$.

By the Chinese Remainder theorem on polynomials, this product can be written as a partial fraction decomposition:

$$
\prod_{k=1}^P \frac{1}{1-kx} = \sum_{k=1}^P \frac{A_k}{1-kx}
$$

for some rational coefficients $$A_k$$.  We can solve for these coefficients by clearing denominators and setting $$x = 1/m$$ for $$1 \leq m \leq P$$.  Then for a given $$m$$, all the terms except the $$m$$-th vanish, and:

$$
A_m = \frac{1}{\prod\limits_{\substack{1 \leq j \leq P\\j \neq m}} 1 - j/m} = \prod_{j \neq m} \frac{m}{m-j}
$$

Since a geometric series has sum $$\sum_{j \geq 0} (kx)^j = \frac{1}{1-kx}$$, altogether it implies:

$$
[x^S]f = \sum_{k=1}^P A_k * k^S
$$

so that the final answer is

$$
\text{answer} = N! \sum_{k=1}^{N-K} k^{L-N} \prod_{\substack{1 \leq j \leq N-K\\j \neq k}} \frac{k}{k-j}
$$

$$
\Rightarrow \text{answer} = N! \sum_k k^{L-K-1} \prod_{j \neq k} \frac{1}{k-j}
$$

We only need a quick way to compute $$C_k = \prod\limits_{j \neq k} \frac{1}{k-j}$$.  Indeed,

$$
C_{k+1} = C_k * \frac{k - (N-K)}{k}
$$

so that we now have everything we need to compute the answer quickly.


<iframe src="https://leetcode.com/playground/dGeFkUPJ/shared" frameBorder="0" width="100%" height="395" name="dGeFkUPJ"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log L)$$.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### DP solution that is Easy to understand
- Author: optimisea
- Creation Date: Fri Oct 12 2018 07:23:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 10:44:19 GMT+0800 (Singapore Standard Time)

<p>
dp[i][j] denotes the solution of  i songs with  j different songs. So the final answer should be dp[L][N]

Think one step before the last one, there are only cases for the answer of dp[i][j]
case 1 (the last added one is new song): listen i - 1 songs with j - 1 different songs, then the last one is definitely new song with the choices of N - (j - 1). 
Case 2 (the last added one is old song): listen i - 1 songs with j different songs, then the last one is definitely old song with the choices of j
if without the constraint of K, the status equation will be
dp[i][j] = dp[i-1][j-1] * (N - (j-1)) + dp[i-1][j] * j

If with the constaint of K, there are also two cases
Case 1: no changes since the last added one is new song. Hence, there is no conflict
Case 2: now we don\'t have choices of j for the last added old song. It should be updated j - k because k songs can\'t be chosed from j - 1 to j - k. However, if  j <= K,  this case will be 0 because only after choosing K different other songs, old song can be chosen.

if (j > k)
dp[i][j] = dp[i-1][j-1] * (N- (j-1)) + dp[i-1][j] * (j-k)
else
dp[i][j] = dp[i-1][j-1] * (N- (j-1))

```
class Solution {
    public int numMusicPlaylists(int N, int L, int K) {
        int mod = (int)Math.pow(10, 9) + 7;
        long[][] dp = new long[L+1][N+1];
        dp[0][0] = 1;
        for (int i = 1; i <= L; i++){
            for (int j = 1; j <= N; j++){
                dp[i][j] = (dp[i-1][j-1] * (N - (j-1)))%mod; 
                if (j > K){
                    dp[i][j] = (dp[i][j] + (dp[i-1][j] * (j-K))%mod)%mod;
                }
            }
        }
        return (int)dp[L][N];
    }
}
```
</p>


### [C++/Java/Python] DP Solution
- Author: lee215
- Creation Date: Sun Oct 07 2018 11:03:30 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 23:18:25 GMT+0800 (Singapore Standard Time)

<p>
`F(N,L,K) = F(N - 1, L - 1, K) * N + F(N, L - 1, K) * (N - K)`

`F(N - 1, L - 1, K)`
If only `N - 1` in the `L - 1` first songs.
We need to put the rest one at the end of music list.
Any song can be this last song, so there are `N` possible combinations.


`F(N, L - 1, K)`
If already `N` in the `L - 1` first songs.
We can put any song at the end of music list,
but it should be different from `K` last song.
We have `N - K` choices.


**Time Complexity**:
`O((L-K)(N-K))`

**C++:**
```
    int numMusicPlaylists(int N, int L, int K) {
        long dp[N + 1][L + 1], mod = 1e9 + 7;
        for (int i = K + 1; i <= N; ++i)
            for (int j = i; j <= L; ++j)
                if ((i == j) || (i == K + 1))
                    dp[i][j] = factorial(i);
                else
                    dp[i][j] = (dp[i - 1][j - 1] * i + dp[i][j - 1] * (i - K))  % mod;
        return (int) dp[N][L];
    }

    long factorial(int n) {
        return n ? factorial(n - 1) * n % (long)(1e9 + 7) : 1;
    }
```

**Java:**
```
    long mod = (long)1e9 + 7;
    public int numMusicPlaylists(int N, int L, int K) {
        long[][] dp = new long[N + 1][L + 1];
        for (int i = K + 1; i <= N; ++i)
            for (int j = i; j <= L; ++j)
                if ((i == j) || (i == K + 1))
                    dp[i][j] = factorial(i);
                else
                    dp[i][j] = (dp[i - 1][j - 1] * i + dp[i][j - 1] * (i - K))  % mod;
        return (int) dp[N][L];
    }

    long factorial(int n) {
        return n > 0 ? factorial(n - 1) * n % mod : 1;
    }
```
**Python:**
```
    def numMusicPlaylists(self, N, L, K):
        dp = [[0 for i in range(L + 1)] for j in range(N + 1)]
        for i in range(K + 1, N + 1):
            for j in range(i, L + 1):
                if i == j or i == K + 1:
                    dp[i][j] = math.factorial(i)
                else:
                    dp[i][j] = dp[i - 1][j - 1] * i + dp[i][j - 1] * (i - K)
        return dp[N][L] % (10**9 + 7)
```

</p>


### [C++] Straight forward DP, with explanation
- Author: goddice
- Creation Date: Sun Oct 07 2018 11:06:14 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 14:25:43 GMT+0800 (Singapore Standard Time)

<p>
The solution is dp[N][L].
The state transfer function is :

    dp[N][L] = dp[N][L-1] * max(0, N-K) + dp[N-1][L-1] * N 
Boundary condition: 

    dp[N][N] = N!, dp[0][L] = 0 
 
Explanation:

The first term means we use N numbers in L-1 position, when we open a new position, the possible inserted value can be only selected from (N-K) numbers, because the nearest K numbers cannot be used.
The second term means we use N-1 numbers in L-1 position, and the inserted number must be the remaining number. Since we can use any N-1 numbers in N numbers, we have totally N different combination,

**C++ code**:
```
    int numMusicPlaylists(int N, int L, int K) {
        const int mod = 1000000007;
        
        vector<vector<long long>> dp(1 + N, vector<long long>(1 + L, 0LL));
        dp[1][1] = 1;
        for(int i = 2; i <= N; ++i) {
            dp[i][i] = (dp[i-1][i-1] * i) % mod;
        }
        for(int n = 1; n <= N; ++n) {
            for(int l = n+1; l <= L; ++l) {
                dp[n][l] = ((dp[n][l-1] * ((n-K)>0? (n-K) : 0)) % mod + (dp[n-1][l-1] * n) % mod) % mod;
            }
        }
        
        return dp[N][L];
    }
```
</p>


