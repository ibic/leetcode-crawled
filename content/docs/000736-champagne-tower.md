---
title: "Champagne Tower"
weight: 736
#id: "champagne-tower"
---
## Description
<div class="description">
<p>We stack glasses in a pyramid, where the <strong>first</strong> row has <code>1</code> glass, the <strong>second</strong> row has <code>2</code> glasses, and so on until the 100<sup>th</sup> row.&nbsp; Each glass holds one cup (<code>250</code>ml) of champagne.</p>

<p>Then, some champagne is poured in the first glass at the top.&nbsp; When the topmost glass is full, any excess liquid poured will fall equally to the glass immediately to the left and right of it.&nbsp; When those glasses become full, any excess champagne will fall equally to the left and right of those glasses, and so on.&nbsp; (A glass at the bottom row has its excess champagne fall on the floor.)</p>

<p>For example, after one cup of champagne is poured, the top most glass is full.&nbsp; After two cups of champagne are poured, the two glasses on the second row are half full.&nbsp; After three cups of champagne are poured, those two cups become full - there are 3 full glasses total now.&nbsp; After four cups of champagne are poured, the third row has the middle glass half full, and the two outside glasses are a quarter full, as pictured below.</p>

<p><img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/03/09/tower.png" style="height: 241px; width: 350px;" /></p>

<p>Now after pouring some non-negative integer cups of champagne, return how full the <code>j<sup>th</sup></code> glass in the <code>i<sup>th</sup></code> row is (both <code>i</code> and <code>j</code> are 0-indexed.)</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> poured = 1, query_row = 1, query_glass = 1
<strong>Output:</strong> 0.00000
<strong>Explanation:</strong> We poured 1 cup of champange to the top glass of the tower (which is indexed as (0, 0)). There will be no excess liquid so all the glasses under the top glass will remain empty.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> poured = 2, query_row = 1, query_glass = 1
<strong>Output:</strong> 0.50000
<strong>Explanation:</strong> We poured 2 cups of champange to the top glass of the tower (which is indexed as (0, 0)). There is one cup of excess liquid. The glass indexed as (1, 0) and the glass indexed as (1, 1) will share the excess liquid equally, and each will get half cup of champange.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> poured = 100000009, query_row = 33, query_glass = 17
<strong>Output:</strong> 1.00000
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;=&nbsp;poured &lt;= 10<sup>9</sup></code></li>
	<li><code>0 &lt;= query_glass &lt;= query_row&nbsp;&lt; 100</code></li>
</ul>

</div>

## Tags


## Companies
- Google - 4 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Simulation [Accepted]

**Intuition**

Instead of keeping track of how much champagne should end up in a glass, keep track of the total amount of champagne that flows through a glass.  For example, if `poured = 10` cups are poured at the top, then the total flow-through of the top glass is `10`; the total flow-through of each glass in the second row is `4.5`, and so on.

**Algorithm**

In general, if a glass has flow-through `X`, then `Q = (X - 1.0) / 2.0` quantity of champagne will equally flow left and right.  We can simulate the entire pour for 100 rows of glasses.  A glass at `(r, c)` will have excess champagne flow towards `(r+1, c)` and `(r+1, c+1)`.

<iframe src="https://leetcode.com/playground/tBELYQkj/shared" frameBorder="0" width="100%" height="344" name="tBELYQkj"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(R^2)$$, where $$R$$ is the number of rows.  As this is fixed, we can consider this complexity to be $$O(1)$$.

* Space Complexity: $$O(R^2)$$, or $$O(1)$$ by the reasoning above.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [20ms] C++, Easy understand solution
- Author: suilan0602
- Creation Date: Sun Mar 11 2018 12:30:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 13:37:08 GMT+0800 (Singapore Standard Time)

<p>
We use a table to record the result.
Simple idea:
If the glass >=1, we should split the diff (glass - 1) into next level.

```
double champagneTower(int poured, int query_row, int query_glass) {
        double result[101][101] = {0.0};
        result[0][0] = poured;
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j <= i; j++) {
                if (result[i][j] >= 1) {
                    result[i + 1][j] += (result[i][j] - 1) / 2.0;
                    result[i + 1][j + 1] += (result[i][j] - 1) / 2.0;
                    result[i][j] = 1;
                }
            }
        }
        return result[query_row][query_glass];
    }
```
</p>


### [Java/C++/Python] 1D DP, O(R) space
- Author: lee215
- Creation Date: Sun Mar 11 2018 15:45:47 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 27 2019 11:18:45 GMT+0800 (Singapore Standard Time)

<p>
Time `O(R^2)` Time, Space `O(R)`, where `R = query_row`

**Java:**
```
    public double champagneTower(int poured, int query_row, int query_glass) {
        double[] res = new double[query_row + 2];
        res[0] = poured;
        for (int row = 1; row <= query_row; row++)
            for (int i = row; i >= 0; i--)
                res[i + 1] += res[i] = Math.max(0.0, (res[i] - 1) / 2);
        return Math.min(res[query_glass], 1.0);
    }
```

**C++:**
```
    double champagneTower(int poured, int query_row, int query_glass) {
        vector<double> res(102);
        res[0] = poured;
        for (int row = 1; row <= query_row; row++)
            for (int i = row; i >= 0; i--)
                res[i + 1] += res[i] = max(0.0, (res[i] - 1) / 2);
        return min(res[query_glass], 1.0);
    }
```

**Python:**
```
    def champagneTower(self, poured, query_row, query_glass):
        res = [poured] + [0] * query_row
        for row in range(1, query_row + 1):
            for i in range(row, -1, -1):
                res[i] = max(res[i] - 1, 0) / 2.0 + max(res[i - 1] - 1, 0) / 2.0
        return min(res[query_glass], 1)
```

</p>


### [9ms] 5 Lines Code [ C++/Java ]
- Author: DDev
- Creation Date: Sun Mar 11 2018 14:16:32 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 23:46:48 GMT+0800 (Singapore Standard Time)

<p>
  C++:
	
	double champagneTower(int poured, int query_row, int query_glass) {
        vector<double> dp(101, 0); dp[0] = poured;
        for(int row=1; row<=query_row; row++)
            for(int i=row; i>=0; i--)
                dp[i+1] += dp[i] = max(0.0, (dp[i]-1)/2);
        return min(dp[query_glass], 1.0);
    }


Java :

	public double champagneTower(int poured, int query_row, int query_glass) {
        double[] dp = new double[101]; dp[0] = poured;
        for(int row=1; row<=query_row; row++)
            for(int i=row; i>=0; i--)
                dp[i+1] += dp[i] = Math.max(0.0, (dp[i]-1)/2);
        return Math.min(dp[query_glass], 1.0);
    }
</p>


