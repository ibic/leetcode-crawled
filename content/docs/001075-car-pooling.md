---
title: "Car Pooling"
weight: 1075
#id: "car-pooling"
---
## Description
<div class="description">
<p>You are driving a vehicle that&nbsp;has <code>capacity</code> empty seats initially available for passengers.&nbsp; The vehicle <strong>only</strong> drives east (ie. it <strong>cannot</strong> turn around and drive west.)</p>

<p>Given a list of <code>trips</code>, <code>trip[i] = [num_passengers, start_location, end_location]</code>&nbsp;contains information about the <code>i</code>-th trip: the number of passengers that must be picked up, and the locations to pick them up and drop them off.&nbsp; The locations are given as the number of kilometers&nbsp;due east from your vehicle&#39;s initial location.</p>

<p>Return <code>true</code> if and only if&nbsp;it is possible to pick up and drop off all passengers for all the given trips.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>trips = <span id="example-input-1-1">[[2,1,5],[3,3,7]]</span>, capacity = <span id="example-input-1-2">4</span>
<strong>Output: </strong><span id="example-output-1">false</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>trips = <span id="example-input-2-1">[[2,1,5],[3,3,7]]</span>, capacity = <span id="example-input-2-2">5</span>
<strong>Output: </strong><span id="example-output-2">true</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>trips = <span id="example-input-3-1">[[2,1,5],[3,5,7]]</span>, capacity = <span id="example-input-3-2">3</span>
<strong>Output: </strong><span id="example-output-3">true</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong>trips = <span id="example-input-4-1">[[3,2,7],[3,7,9],[8,3,9]]</span>, capacity = <span id="example-input-4-2">11</span>
<strong>Output: </strong><span id="example-output-4">true</span>
</pre>
</div>
</div>
</div>

<div>
<div>
<div>
<div>&nbsp;</div>
</div>
</div>
</div>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ol>
	<li><code>trips.length &lt;= 1000</code></li>
	<li><code>trips[i].length == 3</code></li>
	<li><code>1 &lt;= trips[i][0] &lt;= 100</code></li>
	<li><code>0 &lt;= trips[i][1] &lt; trips[i][2] &lt;= 1000</code></li>
	<li><code>1 &lt;=&nbsp;capacity &lt;= 100000</code></li>
</ol>

</div>

## Tags
- Greedy (greedy)

## Companies
- Facebook - 4 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Lyft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

### Overview

It is one of the classical problems related to intervals, and we have some similar problems such as [Meeting Rooms II](https://leetcode.com/problems/meeting-rooms-ii/) at LeetCode. Below, two approaches are introduced: the simple *Time Stamp* approach, and the *Bucket Sort* approach.

---

### Approach 1: Time Stamp

**Intuition**

A simple idea is to go through from the start to end, and check if the actual capacity exceeds `capacity`.

To know the actual capacity, we just need the number of passengers changed at each timestamp.

We can save the number of passengers changed at each time, sort it by timestamp, and finally iterate it to check the actual capacity.

**Algorithm**

We will initialize a list to store the number of passengers changed and the corresponding timestamp and then sort it.

Note that in Java, we do not have a nice API to do this. However, we can use a `TreeMap`, which can help us to sort during insertion. You can use a `PriorityQueue` instead.

Finally, we just need to iterate from the start timestamp to the end timestamp and check if the actual capacity meets the condition.

<iframe src="https://leetcode.com/playground/K8fmqVtL/shared" frameBorder="0" width="100%" height="395" name="K8fmqVtL"></iframe>

**Complexity Analysis**

Assume $$N$$ is the length of `trips`.

* Time complexity: $$\mathcal{O}(N\log(N))$$ since we need to iterate over `trips` and sort our `timestamp`. Iterating costs $$\mathcal{O}(N)$$, and sorting costs $$\mathcal{O}(N\log(N))$$, and adding together we have $$\mathcal{O}(N) + \mathcal{O}(N\log(N)) = \mathcal{O}(N\log(N))$$.
 
* Space complexity: $$\mathcal{O}(N)$$ since in the worst case we need $$\mathcal{O}(N)$$ to store `timestamp`.

---

### Approach 2: Bucket Sort

**Intuition**

Note that in the problem there is a interesting constraint:

> 4. `0 <= trips[i][1] < trips[i][2] <= 1000`

What pops into the mind is [Bucket Sort](https://en.wikipedia.org/wiki/Bucket_sort), which is a sorting algorithm in $$\mathcal{O}(N)$$ time but requires some prior knowledge for the range of the data.

We can use it instead of the normal sorting in this method.

What we do is initial 1001 buckets, and put the number of passengers changed in corresponding buckets, and collect the buckets one by one.

**Algorithm**

We will initial 1001 buckets, iterate `trip`, and save the number of passengers changed at `i` mile in the `i`-th bucket.

<iframe src="https://leetcode.com/playground/NtoWq4z4/shared" frameBorder="0" width="100%" height="344" name="NtoWq4z4"></iframe>

**Complexity Analysis**

Assume $$N$$ is the length of `trip`.

* Time complexity: $$\mathcal{O}(max(N, 1001))$$ since we need to iterate over `trips` and then iterate over our 1001 buckets.
 
* Space complexity : $$\mathcal{O}(1001)=\mathcal{O}(1)$$ since we have 1001 buckets.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java O(n) Thousand and One Stops
- Author: votrubac
- Creation Date: Sun Jun 23 2019 12:07:34 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 24 2019 03:06:47 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Since we only have 1,001 ```stops```, we can just figure out how many people get it and out in each location. 
# Solution
Process all trips, adding passenger count to the start location, and removing it from the end location. After processing all trips, a positive value for the specific location tells that we are getting more passengers; negative - more empty seats. 

Finally, scan all stops and check if we ever exceed our vehicle capacity.
## C++
```
bool carPooling(vector<vector<int>>& trips, int capacity) {
  int stops[1001] = {};
  for (auto t : trips) stops[t[1]] += t[0], stops[t[2]] -= t[0];
  for (auto i = 0; capacity >= 0 && i < 1001; ++i) capacity -= stops[i];
  return capacity >= 0;
}
```
## Java
```
public boolean carPooling(int[][] trips, int capacity) {    
  int stops[] = new int[1001]; 
  for (int t[] : trips) {
      stops[t[1]] += t[0];
      stops[t[2]] -= t[0];
  }
  for (int i = 0; capacity >= 0 && i < 1001; ++i) capacity -= stops[i];
  return capacity >= 0;
}
```
# Complexity Analysis
Runtime: *O(n)*, where *n* is the number of trips. 
Memory: *O(m)*, where *m* is the number of stops.
</p>


### [Java/C++/Python] Meeting Rooms III
- Author: lee215
- Creation Date: Sun Jun 23 2019 12:07:33 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 18 2020 11:35:19 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Same as [253. Meeting Rooms II](https://leetcode.com/problems/meeting-rooms-ii/discuss/278270/Java-Sort-All-Time-Point).
Track the change of capacity in time order.

## **Explanation**

1. Save all time points and the change on current `capacity`
2. Sort all the changes on the key of time points.
3. Track the current `capacity` and return `false` if negative


## **Complexity**
Time `O(NlogN)`
Space `O(N)`

<br>

**Java:**
```java
    public boolean carPooling(int[][] trips, int capacity) {
        Map<Integer, Integer> m = new TreeMap<>();
        for (int[] t : trips) {
            m.put(t[1], m.getOrDefault(t[1], 0) + t[0]);
            m.put(t[2], m.getOrDefault(t[2], 0) - t[0]);
        }
        for (int v : m.values()) {
            capacity -= v;
            if (capacity < 0) {
                return false;
            }
        }
        return true;
    }
```
**C++:**
```cpp
    bool carPooling(vector<vector<int>>& trips, int capacity) {
        map<int, int> m;
        for (auto &t : trips)
            m[t[1]] += t[0], m[t[2]] -= t[0];
        for (auto &v : m)
            if ((capacity -= v.second) < 0)
                return false;
        return true;
    }
```
**Python:**
```py
    def carPooling(self, trips, capacity):
        for i, v in sorted(x for n, i, j in trips for x in [[i, n], [j, - n]]):
            capacity -= v
            if capacity < 0:
                return False
        return True
```

</p>


### Simple Python solution
- Author: otoc
- Creation Date: Tue Jun 25 2019 06:08:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 07 2019 12:33:16 GMT+0800 (Singapore Standard Time)

<p>
Please see and vote for my solutions for similar problems.
[253. Meeting Rooms II](https://leetcode.com/problems/meeting-rooms-ii/discuss/322622/Simple-Python-solutions)
[731. My Calendar II](https://leetcode.com/problems/my-calendar-ii/discuss/323479/Simple-C%2B%2B-Solution-using-built-in-map-(Same-as-253.-Meeting-Rooms-II))
[732. My Calendar III](https://leetcode.com/problems/my-calendar-iii/discuss/302492/Simple-C%2B%2B-Solution-using-built-in-map-(Same-as-253.-Meeting-Rooms-II))
[1094. Car Pooling](https://leetcode.com/problems/car-pooling/discuss/319088/Simple-Python-solution)
[1109. Corporate Flight Bookings](https://leetcode.com/problems/corporate-flight-bookings/discuss/328949/Simple-Python-solution)
[218. The Skyline Problem](https://leetcode.com/problems/the-skyline-problem/discuss/325070/SImple-Python-solutions)


Python solution:
```
    def carPooling(self, trips: List[List[int]], capacity: int) -> bool:
        lst = []
        for n, start, end in trips:
            lst.append((start, n))
            lst.append((end, -n))
        lst.sort()
        pas = 0
        for loc in lst:
            pas += loc[1]
            if pas > capacity:
                return False
        return True
```
</p>


