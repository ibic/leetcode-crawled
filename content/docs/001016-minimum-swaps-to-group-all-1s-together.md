---
title: "Minimum Swaps to Group All 1's Together"
weight: 1016
#id: "minimum-swaps-to-group-all-1s-together"
---
## Description
<div class="description">
<p>Given a&nbsp;binary array <code>data</code>, return&nbsp;the minimum number of swaps required to group all <code>1</code>&rsquo;s present in the array together in <strong>any place</strong> in the array.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> data = [1,0,1,0,1]
<strong>Output:</strong> 1
<strong>Explanation: </strong>
There are 3 ways to group all 1&#39;s together:
[1,1,1,0,0] using 1 swap.
[0,1,1,1,0] using 2 swaps.
[0,0,1,1,1] using 1 swap.
The minimum is 1.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> data = [0,0,0,1,0]
<strong>Output:</strong> 0
<strong>Explanation: </strong>
Since there is only one 1 in the array, no swaps needed.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> data = [1,0,1,0,1,0,0,1,1,0,1]
<strong>Output:</strong> 3
<strong>Explanation: </strong>
One possible solution that uses 3 swaps is [0,0,0,0,0,1,1,1,1,1,1].
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> data = [1,0,1,0,1,0,1,1,1,0,1,0,0,1,1,1,0,0,1,1,1,0,1,0,1,1,0,0,0,1,1,1,1,0,0,1]
<strong>Output:</strong> 8
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= data.length &lt;= 10<sup>5</sup></code></li>
	<li><code>data[i]</code>&nbsp;is <code>0</code> or <code>1</code>.</li>
</ul>

</div>

## Tags
- Array (array)
- Sliding Window (sliding-window)

## Companies
- IBM - 2 (taggedByAdmin: false)
- Expedia - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java]Sliding window O(n) with detailed explanation, very easy to understand
- Author: tumei
- Creation Date: Sun Aug 11 2019 02:16:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 11 2019 02:16:38 GMT+0800 (Singapore Standard Time)

<p>
Thinking about it:  **the final result we want is a window with length of n** (total number of the 1s)
Check all the window with the same length n, **find the maximum one which already contains the most 1s.**
All we need to do is to swap the rest:  n-max.

```
class Solution {
    public int minSwaps(int[] nums) {
        if(nums.length < 3) return 0;
        int n = 0;
        for(int num: nums){
            if(num == 1) n++; // total number of 1s
        }
        int i=0, j=0, c=0, max=0; //sliding window i to j
        while(j < nums.length) {
            while(j < nums.length && j - i < n){ //until i to j == n or search is done
                if(nums[j++] == 1) c++;
            }
            max = Math.max(c, max); // max all the sliding window of which length equals to n
            if(j == nums.length) break;
            
            if(nums[i++] == 1) { //move i forward
                c--;
            }
        }
        return n - max; //this is the minimun swaps  
    }
}
```
</p>


### [Java/Python 3] Sliding Window O(n) code w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Aug 11 2019 00:03:08 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 12 2019 14:21:13 GMT+0800 (Singapore Standard Time)

<p>
1. Maintain a sliding window of `width`, which is the number of `1`\'s in `data`; 
2. Find the max number of `1`\'s, `maxWin`, in that window; 
3. Then the remaining `0`\'s in the window with max number of `1`\'s, `width - maxWin`, is the minimum swaps to get a window of all `1`\'s.

**Java:**
```
    public int minSwaps(int[] data) {
        int maxWin = 0, width = Arrays.stream(data).sum(); // count the number of 1\'s in data.
        for (int l = -1, r = 0, cntWin = 0; r < data.length; ++r) {
            cntWin += data[r];
            if (r - l > width) { cntWin -= data[++l]; } // wider than width, shrink the lower bound to maintain its width.
            maxWin = Math.max(cntWin, maxWin);
        }
        return width - maxWin;
    }
```
**Update:** credit to @venendroid for correction of last statement.

----

**Python 3:**
```
    def minSwaps(self, data: List[int]) -> int:
        width, maxWin, cntWin, l = sum(data), 0, 0, -1 
        for r, d in enumerate(data):
            cntWin += d
            if r - l > width: 
                l += 1
                cntWin -= data[l]
            maxWin = max(maxWin, cntWin)
        return width - maxWin
```
**Analysis:**
Time: O(n), space: O(1).

</p>


### C++ O(n) time O(1) space
- Author: gingerale
- Creation Date: Sun Aug 11 2019 12:15:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 11 2019 12:16:02 GMT+0800 (Singapore Standard Time)

<p>
the final window size is equal to the number of 1 the array has. So find the window has maxium number of 1s already in it. 
Since the window size if fixed and the array is binary, why is everyone using two pointers instead of one ? one pointer i and the other is i-K.

```
    int minSwaps(vector<int>& data) {
        for(int i = 1; i < data.size(); ++i) {
            data[i] += data[i-1];
        }
        int K = data.back();
        if(K == 0)
            return 0;
        // count max number of 1 window size K has
        int max_one = data[K-1];
        for(int i = K; i < data.size(); ++i) {
            max_one = max(max_one, data[i] - data[i-K]);
        }
        return K - max_one;
    }
```
</p>


