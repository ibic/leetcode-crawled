---
title: "Minimum Genetic Mutation"
weight: 410
#id: "minimum-genetic-mutation"
---
## Description
<div class="description">
<p>A gene string can be represented by an 8-character long string, with choices from <code>&quot;A&quot;</code>, <code>&quot;C&quot;</code>, <code>&quot;G&quot;</code>, <code>&quot;T&quot;</code>.</p>

<p>Suppose we need to investigate about a mutation (mutation from &quot;start&quot; to &quot;end&quot;), where ONE mutation is defined as ONE single character changed in the gene string.</p>

<p>For example, <code>&quot;AACCGGTT&quot;</code> -&gt; <code>&quot;AACCGGTA&quot;</code> is 1 mutation.</p>

<p>Also, there is a given gene &quot;bank&quot;, which records all the valid gene mutations. A gene must be in the bank to make it a valid gene string.</p>

<p>Now, given 3 things - start, end, bank, your task is to determine what is the minimum number of mutations needed to mutate from &quot;start&quot; to &quot;end&quot;. If there is no such a mutation, return -1.</p>

<p><b>Note:</b></p>

<ol>
	<li>Starting point is assumed to be valid, so it might not be included in the bank.</li>
	<li>If multiple mutations are needed, all mutations during in the sequence must be valid.</li>
	<li>You may assume start and end string is not the same.</li>
</ol>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
start: &quot;AACCGGTT&quot;
end:   &quot;AACCGGTA&quot;
bank: [&quot;AACCGGTA&quot;]

return: 1
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
start: &quot;AACCGGTT&quot;
end:   &quot;AAACGGTA&quot;
bank: [&quot;AACCGGTA&quot;, &quot;AACCGCTA&quot;, &quot;AAACGGTA&quot;]

return: 2
</pre>

<p>&nbsp;</p>

<p><b>Example 3:</b></p>

<pre>
start: &quot;AAAAACCC&quot;
end:   &quot;AACCCCCC&quot;
bank: [&quot;AAAACCCC&quot;, &quot;AAACCCCC&quot;, &quot;AACCCCCC&quot;]

return: 3
</pre>

<p>&nbsp;</p>

</div>

## Tags


## Companies
- Google - 4 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution using BFS
- Author: xietao0221
- Creation Date: Tue Nov 01 2016 12:12:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 18:51:12 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public int minMutation(String start, String end, String[] bank) {
        if(start.equals(end)) return 0;
        
        Set<String> bankSet = new HashSet<>();
        for(String b: bank) bankSet.add(b);
        
        char[] charSet = new char[]{'A', 'C', 'G', 'T'};
        
        int level = 0;
        Set<String> visited = new HashSet<>();
        Queue<String> queue = new LinkedList<>();
        queue.offer(start);
        visited.add(start);
        
        while(!queue.isEmpty()) {
            int size = queue.size();
            while(size-- > 0) {
                String curr = queue.poll();
                if(curr.equals(end)) return level;
                
                char[] currArray = curr.toCharArray();
                for(int i = 0; i < currArray.length; i++) {
                    char old = currArray[i];
                    for(char c: charSet) {
                        currArray[i] = c;
                        String next = new String(currArray);
                        if(!visited.contains(next) && bankSet.contains(next)) {
                            visited.add(next);
                            queue.offer(next);
                        }
                    }
                    currArray[i] = old;
                }
            }
            level++;
        }
        return -1;
    }
}
```
</p>


### Simple JAVA Solution 
- Author: irodov
- Creation Date: Mon Aug 27 2018 10:20:48 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 03 2018 09:09:03 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int minMutation(String start, String end, String[] bank) {
		if(start == end)
			return 1;
		Set<String> visited = new HashSet<>();
		Set<String> bankSet = new HashSet<>();
		for(String b : bank) {
			bankSet.add(b);
		}
		Queue<GeneHop> queue = new LinkedList<>();
		visited.add(start);
		queue.add(new GeneHop(start , 1));
		while(! queue.isEmpty()) {
			GeneHop next = queue.poll();
			if(next.mutation.equals(end)) {
				return next.step - 1;
			}
			for(int i = 0 ; i < 8 ; i++) {
				for(char c : new char[] {\'A\', \'C\', \'G\', \'T\'}) {
					String temp = next.mutation.substring(0, i) + c + next.mutation.substring(i + 1);
					if(bankSet.contains(temp) && !visited.contains(temp)) {
						visited.add(temp);
						queue.add(new GeneHop(temp , next.step + 1));
					}
				}
			}
		}
        return -1;
    }
}

public class GeneHop {
	String mutation;
	int step;
	GeneHop(String mutation , int step) {
		this.mutation = mutation;
		this.step = step;
	}
}

```
</p>


### Python Solution using BFS
- Author: lee12jin
- Creation Date: Wed Aug 23 2017 16:01:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 18 2018 05:14:57 GMT+0800 (Singapore Standard Time)

<p>
```
import collections

def viableMutation(current_mutation, next_mutation):
    changes = 0
    for i in xrange(len(current_mutation)):
        if current_mutation[i] != next_mutation[i]:
            changes += 1
    return changes == 1

class Solution(object):
    def minMutation(self, start, end, bank):
        """
        :type start: str
        :type end: str
        :type bank: List[str]
        :rtype: int
        """
        queue = collections.deque()
        queue.append([start, start, 0]) # current, previous, num_steps
        while queue:
            current, previous, num_steps = queue.popleft()
            if current == end:  # in BFS, the first instance of current == end will yield the minimum
                return num_steps
            for string in bank:
                if viableMutation(current, string) and string != previous:
                    queue.append([string, current, num_steps+1])
        return -1
```
</p>


