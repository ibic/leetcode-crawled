---
title: "Partition List"
weight: 86
#id: "partition-list"
---
## Description
<div class="description">
<p>Given a linked list and a value <em>x</em>, partition it such that all nodes less than <em>x</em> come before nodes greater than or equal to <em>x</em>.</p>

<p>You should preserve the original relative order of the nodes in each of the two partitions.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> head = 1-&gt;4-&gt;3-&gt;2-&gt;5-&gt;2, <em>x</em> = 3
<strong>Output:</strong> 1-&gt;2-&gt;2-&gt;4-&gt;3-&gt;5
</pre>

</div>

## Tags
- Linked List (linked-list)
- Two Pointers (two-pointers)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

The problem wants us to reform the linked list structure, such that the
elements lesser that a certain value `x`, come before the elements greater or
equal to `x`. This essentially means in this reformed list, there would be a
point in the linked list `before` which all the elements would be smaller than
`x` and `after` which all the elements would be greater or equal to `x`.
Let's call this point as the `JOINT`.

<center>
<img src="../Figures/86/86_Partition_List_1.png" width="700"/>
</center>

Reverse engineering the question tells us that if we break the reformed list
at the `JOINT`, we will get two smaller linked lists, one with lesser elements
and the other with elements greater or equal to `x`. In the solution, our main aim
is to create these two linked lists and join them.

#### Approach 1: Two Pointer Approach

**Intuition**

We can take two pointers `before` and `after` to keep track of the two linked
lists as described above. These two pointers could be
used two create two separate lists and then these lists could be combined to
form the desired reformed list.

**Algorithm**

1. Initialize two pointers `before` and `after`. In the implementation we have
initialized these two with a dummy `ListNode`. This helps to reduce the number
of conditional checks we would need otherwise. You can try an implementation
where you don't initialize with a dummy node and see it yourself!

    <center>
    <img src="../Figures/86/86_Partition_List_2.png" width="400"/>
    </center>
    <center>Dummy Node Initialization</center><br/>

2. Iterate the original linked list, using the `head` pointer.
3. If the node's value pointed by `head` is *lesser* than `x`, the node should
be part of the `before` list. So we move it to `before` list.

    <center>
    <img src="../Figures/86/86_Partition_List_3.png" width="700"/>
    </center>

4. Else, the node should be part of `after` list. So we move it to `after` list.

    <center>
    <img src="../Figures/86/86_Partition_List_4.png" width="700"/>
    </center>

5. Once we are done with all the nodes in the original linked list, we would
have two list `before` and `after`. The original list nodes are either part of
`before` list or `after` list, depending on its value.

    <center>
    <img src="../Figures/86/86_Partition_List_5.png" width="700"/>
    </center>

    *`Note:` Since we traverse the original linked list from left to right,
    at no point would the order of nodes change relatively in the two lists. Another important thing to note here is that we show the original linked list intact in the above diagrams. However, in the implementation, we remove the nodes from the original linked list and attach them in the before or after list. We don't utilize any additional space. We simply move the nodes from the original list around.*

6. Now, these two lists `before` and `after` can be combined to form the reformed list.

    <center>
    <img src="../Figures/86/86_Partition_List_6.png" width="700"/>
    </center>

We did a dummy node initialization at the start to make implementation
easier, you don't want that to be part of the returned list, hence just
move ahead one node in both the lists while combining the two list. Since both
before and after have an extra node at the front.

<iframe src="https://leetcode.com/playground/o2LDnaWG/shared" frameBorder="0" width="100%" height="500" name="o2LDnaWG"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of nodes in the original
linked list and we iterate the original list.
* Space Complexity: $$O(1)$$, we have not utilized any extra space, the point to
note is that we are reforming the original list, by moving the original nodes, we
have not used any extra space as such.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Very concise one pass solution
- Author: shichaotan
- Creation Date: Fri Jan 09 2015 08:30:09 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 16:51:56 GMT+0800 (Singapore Standard Time)

<p>
    ListNode *partition(ListNode *head, int x) {
        ListNode node1(0), node2(0);
        ListNode *p1 = &node1, *p2 = &node2;
        while (head) {
            if (head->val < x)
                p1 = p1->next = head;
            else
                p2 = p2->next = head;
            head = head->next;
        }
        p2->next = NULL;
        p1->next = node2.next;
        return node1.next;
    }
</p>


### Concise java code with explanation, one pass
- Author: cbmbbz
- Creation Date: Fri Jan 23 2015 15:30:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 13:41:51 GMT+0800 (Singapore Standard Time)

<p>
the basic idea is to maintain two queues, the first one stores all nodes with val less than x , and the second queue stores all the rest nodes. Then concat these two queues. Remember to set the tail of second queue a null next, or u will get TLE.

    public ListNode partition(ListNode head, int x) {
        ListNode dummy1 = new ListNode(0), dummy2 = new ListNode(0);  //dummy heads of the 1st and 2nd queues
        ListNode curr1 = dummy1, curr2 = dummy2;      //current tails of the two queues;
        while (head!=null){
            if (head.val<x) {
                curr1.next = head;
                curr1 = head;
            }else {
                curr2.next = head;
                curr2 = head;
            }
            head = head.next;
        }
        curr2.next = null;          //important! avoid cycle in linked list. otherwise u will get TLE.
        curr1.next = dummy2.next;
        return dummy1.next;
    }
</p>


### Python concise solution with dummy nodes.
- Author: OldCodingFarmer
- Creation Date: Sat Aug 15 2015 19:22:13 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 08:19:55 GMT+0800 (Singapore Standard Time)

<p>
        
    def partition(self, head, x):
        h1 = l1 = ListNode(0)
        h2 = l2 = ListNode(0)
        while head:
            if head.val < x:
                l1.next = head
                l1 = l1.next
            else:
                l2.next = head
                l2 = l2.next
            head = head.next
        l2.next = None
        l1.next = h2.next
        return h1.next
</p>


