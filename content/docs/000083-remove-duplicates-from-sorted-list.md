---
title: "Remove Duplicates from Sorted List"
weight: 83
#id: "remove-duplicates-from-sorted-list"
---
## Description
<div class="description">
<p>Given a sorted linked list, delete all duplicates such that each element appear only <em>once</em>.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 1-&gt;1-&gt;2
<strong>Output:</strong> 1-&gt;2
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> 1-&gt;1-&gt;2-&gt;3-&gt;3
<strong>Output:</strong> 1-&gt;2-&gt;3
</pre>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Goldman Sachs - 6 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Salesforce - 6 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)

## Official Solution
## Solution
---

#### Approach 1: Straight-Forward Approach

**Algorithm**

This is a simple problem that merely tests your ability to manipulate list node pointers. Because the input list is sorted, we can determine if a node is a duplicate by comparing its value to the node *after* it in the list. If it is a duplicate, we change the ````next```` pointer of the current node so that it skips the next node and points directly to the one after the next node.

<iframe src="https://leetcode.com/playground/KHvbA6CF/shared" frameBorder="0" width="100%" height="242" name="KHvbA6CF"></iframe>


**Complexity Analysis**

* Time complexity : $$O(n)$$. Because each node in the list is checked exactly once to determine if it is a duplicate or not, the total run time is $$O(n)$$, where $$n$$ is the number of nodes in the list.

* Space complexity : $$O(1)$$. No additional space is used.

**Correctness**

We can prove the correctness of this code by defining a *loop invariant*. A loop invariant is condition that is true before and after every iteration of the loop. In this case, a loop invariant that helps us prove correctness is this:

> All nodes in the list up to the pointer `current` do not contain duplicate elements.

We can prove that this condition is indeed a loop invariant by induction. Before going into the loop, `current` points to the head of the list. Therefore, the part of the list up to `current` contains only the head. And so it can not contain any duplicate elements. Now suppose `current` is now pointing to some node in the list (but not the last element), and the part of the list up to `current` contains no duplicate elements. After another loop iteration, one of two things happen.

1. `current.next` was a duplicate of `current`. In this case, the duplicate node at `current.next` is deleted, and `current` stays pointing to the same node as before. Therefore, the condition still holds; there are still no duplicates up to `current`.

2. `current.next` was not a duplicate of `current` (and, because the list is sorted, `current.next` is also not a duplicate of any other element appearing *before* `current`). In this case, `current` moves forward one step to point to `current.next`. Therefore, the condition still holds; there are no duplicates up to `current`.


At the last iteration of the loop, `current` must point to the last element, because afterwards, `current.next = null`. Therefore, after the loop ends, all elements up to the last element do not contain duplicates.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 3 Line JAVA recursive solution
- Author: wen587sort
- Creation Date: Tue May 26 2015 09:47:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 04:34:28 GMT+0800 (Singapore Standard Time)

<p>
This solution is inspired by renzid https://leetcode.com/discuss/33043/3-line-recursive-solution

    public ListNode deleteDuplicates(ListNode head) {
            if(head == null || head.next == null)return head;
            head.next = deleteDuplicates(head.next);
            return head.val == head.next.val ? head.next : head;
    }

Enjoy!
</p>


### My pretty solution. Java.
- Author: odanylevskyi
- Creation Date: Wed Feb 04 2015 00:30:49 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 27 2018 17:30:56 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public ListNode deleteDuplicates(ListNode head) {
            ListNode list = head;
             
             while(list != null) {
            	 if (list.next == null) {
            		 break;
            	 }
            	 if (list.val == list.next.val) {
            		 list.next = list.next.next;
            	 } else {
            		 list = list.next;
            	 }
             }
             
             return head;
        }
    }
</p>


### Simple iterative Python 6 lines, 60 ms
- Author: zhuyinghua1203
- Creation Date: Sun Dec 13 2015 14:44:26 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 10:33:50 GMT+0800 (Singapore Standard Time)

<p>
    def deleteDuplicates(self, head):
        cur = head
        while cur:
            while cur.next and cur.next.val == cur.val:
                cur.next = cur.next.next     # skip duplicated node
            cur = cur.next     # not duplicate of current node, move to next node
        return head
</p>


