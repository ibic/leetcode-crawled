---
title: "Find Smallest Letter Greater Than Target"
weight: 666
#id: "find-smallest-letter-greater-than-target"
---
## Description
<div class="description">
<p>
Given a list of sorted characters <code>letters</code> containing only lowercase letters, and given a target letter <code>target</code>, find the smallest element in the list that is larger than the given target.
</p><p>
Letters also wrap around.  For example, if the target is <code>target = 'z'</code> and <code>letters = ['a', 'b']</code>, the answer is <code>'a'</code>.
</p>

<p><b>Examples:</b><br />
<pre>
<b>Input:</b>
letters = ["c", "f", "j"]
target = "a"
<b>Output:</b> "c"

<b>Input:</b>
letters = ["c", "f", "j"]
target = "c"
<b>Output:</b> "f"

<b>Input:</b>
letters = ["c", "f", "j"]
target = "d"
<b>Output:</b> "f"

<b>Input:</b>
letters = ["c", "f", "j"]
target = "g"
<b>Output:</b> "j"

<b>Input:</b>
letters = ["c", "f", "j"]
target = "j"
<b>Output:</b> "c"

<b>Input:</b>
letters = ["c", "f", "j"]
target = "k"
<b>Output:</b> "c"
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li><code>letters</code> has a length in range <code>[2, 10000]</code>.</li>
<li><code>letters</code> consists of lowercase letters, and contains at least 2 unique letters.</li>
<li><code>target</code> is a lowercase letter.</li>
</ol>
</p>
</div>

## Tags
- Binary Search (binary-search)

## Companies
- LinkedIn - 4 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Record Letters Seen [Accepted]

**Intuition and Algorithm**

Let's scan through `letters` and record if we see a letter or not.  We could do this with an array of size 26, or with a Set structure.

Then, for every next letter (starting with the letter that is one greater than the target), let's check if we've seen it.  If we have, it must be the answer.

<iframe src="https://leetcode.com/playground/vCkvYV3q/shared" frameBorder="0" width="100%" height="276" name="vCkvYV3q"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `letters`.  We scan every element of the array.

* Space Complexity: $$O(1)$$, the maximum size of `seen`.

---
#### Approach #2: Linear Scan [Accepted]

**Intuition and Algorithm**

Since `letters` are sorted, if we see something larger when scanning form left to right, it must be the answer.  Otherwise, (since `letters` is non-empty), the answer is `letters[0]`.

<iframe src="https://leetcode.com/playground/YKuLPei8/shared" frameBorder="0" width="100%" height="174" name="YKuLPei8"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `letters`.  We scan every element of the array.

* Space Complexity: $$O(1)$$, as we maintain only pointers.

---
#### Approach #3: Binary Search [Accepted]

**Intuition and Algorithm**

Like in *Approach #2*, we want to find something larger than target in a sorted array.  This is ideal for a *binary search*: Let's find the rightmost position to insert `target` into `letters` so that it remains sorted.

Our binary search (a typical one) proceeds in a number of rounds.  At each round, let's maintain the *loop invariant* that the answer must be in the interval `[lo, hi]`.  Let `mi = (lo + hi) / 2`.  If `letters[mi] <= target`, then we must insert it in the interval `[mi + 1, hi]`.  Otherwise, we must insert it in the interval `[lo, mi]`.

At the end, if our insertion position says to insert `target` into the last position `letters.length`, we return `letters[0]` instead.  This is what the modulo operation does.

<iframe src="https://leetcode.com/playground/QpdBm22u/shared" frameBorder="0" width="100%" height="242" name="QpdBm22u"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(\log N)$$, where $$N$$ is the length of `letters`.  We peek only at $$\log N$$ elements in the array.

* Space Complexity: $$O(1)$$, as we maintain only pointers.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy Binary Search in Java - O(log(n)) time
- Author: nrl
- Creation Date: Sun Dec 10 2017 12:02:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 11 2018 07:36:05 GMT+0800 (Singapore Standard Time)

<p>
Binary search for the character which comes immediately after character target in the alphabets, or if the target is greater than or equal to the last character in the input list, then search for the first character in the list.

```
class Solution {
    public char nextGreatestLetter(char[] a, char x) {
        int n = a.length;
       
        if (x >= a[n - 1])   x = a[0];
        else    x++;
        
        int lo = 0, hi = n - 1;
        while (lo < hi) {
            int mid = lo + (hi - lo) / 2;
            if (a[mid] == x)    return a[mid];
            if (a[mid] < x)     lo = mid + 1;
            else    hi = mid;
        }
        return a[hi];
    }
}
```

EDIT: Thanks to @xpfxzxc for a further optimized solution:

```
class Solution {
    public char nextGreatestLetter(char[] a, char x) {
        int n = a.length;

        //hi starts at 'n' rather than the usual 'n - 1'. 
        //It is because the terminal condition is 'lo < hi' and if hi starts from 'n - 1', 
        //we can never consider value at index 'n - 1'
        int lo = 0, hi = n;

        //Terminal condition is 'lo < hi', to avoid infinite loop when target is smaller than the first element
        while (lo < hi) {
            int mid = lo + (hi - lo) / 2;
            if (a[mid] > x)     hi = mid;
            else    lo = mid + 1;                 //a[mid] <= x
        }
 
        //Because lo can end up pointing to index 'n', in which case we return the first element
        return a[lo % n];
    }
}
```
</p>


### Python no brainer! \U0001f921
- Author: yangshun
- Creation Date: Sun Dec 10 2017 13:33:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 29 2018 18:12:38 GMT+0800 (Singapore Standard Time)

<p>
Since the input is sorted, you can use binary search, or simply linear scan and it will still be accepted.

*- Yangshun*

```
class Solution(object):
    def nextGreatestLetter(self, letters, target):
        for letter in letters:
            if letter > target:
                return letter
        return letters[0] # If not found
```

Using `bisect`:

```
class Solution(object):
    def nextGreatestLetter(self, letters, target):
        pos = bisect.bisect_right(letters, target)
        return letters[0] if pos == len(letters) else letters[pos]
```
</p>


### This question is hard to understand in my opinion
- Author: onebigsalmon
- Creation Date: Tue Dec 18 2018 09:35:14 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 18 2018 09:35:14 GMT+0800 (Singapore Standard Time)

<p>
"find the smallest element in the list that is larger than the given target. Letters also wrap around."

I read this 5 times and still didn\'t understand. If my input is:

["a","z"]
target: "y"

Then \'a\' is returned due to the wrap around.

But if the input is:

["c","f"]
target: "d"

Then how come "c" is not returned? "c" is bigger than "d" with wrap around and it is smaller than "f" (which is supposed to be the right answer. How does this make sense?
</p>


