---
title: "Strobogrammatic Number III"
weight: 232
#id: "strobogrammatic-number-iii"
---
## Description
<div class="description">
<p>A strobogrammatic number is a number that looks the same when rotated 180 degrees (looked at upside down).</p>

<p>Write a function to count the total strobogrammatic numbers that exist in the range of low &lt;= num &lt;= high.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b> low = &quot;50&quot;, high = &quot;100&quot;
<b>Output:</b> 3 
<strong>Explanation: </strong>69, 88, and 96 are three strobogrammatic numbers.</pre>

<p><strong>Note:</strong><br />
Because the range might be a large number, the <em>low</em> and <em>high</em> numbers are represented as string.</p>

</div>

## Tags
- Math (math)
- Recursion (recursion)

## Companies
- Google - 4 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise Java Solution
- Author: yavinci
- Creation Date: Wed Dec 09 2015 09:19:48 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 23:39:23 GMT+0800 (Singapore Standard Time)

<p>
* Construct char arrays from `low.length()` to `high.length()` 
* Add stro pairs from outside
* When `left` > `right`, add eligible `count`


    private static final char[][] pairs = {{'0', '0'}, {'1', '1'}, {'6', '9'}, {'8', '8'}, {'9', '6'}};
    
    public int strobogrammaticInRange(String low, String high) {
        int[] count = {0};
        for (int len = low.length(); len <= high.length(); len++) {
            char[] c = new char[len];
            dfs(low, high, c, 0, len - 1, count);
        }
        return count[0];
    }
    
    public void dfs(String low, String high , char[] c, int left, int right, int[] count) {
        if (left > right) {
            String s = new String(c);
            if ((s.length() == low.length() && s.compareTo(low) < 0) || 
                (s.length() == high.length() && s.compareTo(high) > 0)) {
                return;
            }
            count[0]++;
            return;
        }
        for (char[] p : pairs) {
            c[left] = p[0];
            c[right] = p[1];
            if (c.length != 1 && c[0] == '0') {
                continue;
            }
            if (left == right && p[0] != p[1]) {
                continue;
            }
            dfs(low, high, c, left + 1, right - 1, count);
        }
    }
</p>


### Clear Java AC solution using Strobogrammatic Number II method
- Author: czonzhu
- Creation Date: Tue Sep 01 2015 06:12:14 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 00:42:32 GMT+0800 (Singapore Standard Time)

<p>
The basic idea is to find generate a list of strobogrammatic number with the length between the length of lower bound and the length of upper bound. Then we pass the list and ignore the numbers with the same length of lower bound or upper bound but not in the range.

I think it is not a a very optimized method and can any one provide a better one?



    public class Solution{
    
    
    	public int strobogrammaticInRange(String low, String high){
    		int count = 0;
    		List<String> rst = new ArrayList<String>();
    		for(int n = low.length(); n <= high.length(); n++){
    			rst.addAll(helper(n, n));
    		}
    		for(String num : rst){
    		    
    			if((num.length() == low.length()&&num.compareTo(low) < 0 ) ||(num.length() == high.length() && num.compareTo(high) > 0)) continue;
    				count++;
    		}
    		return count;
    	}
    
    	private List<String> helper(int cur, int max){
    		if(cur == 0) return new ArrayList<String>(Arrays.asList(""));
    		if(cur == 1) return new ArrayList<String>(Arrays.asList("1", "8", "0"));
    
    		List<String> rst = new ArrayList<String>();
    		List<String> center = helper(cur - 2, max);
    
    		for(int i = 0; i < center.size(); i++){
    			String tmp = center.get(i);
    			if(cur != max) rst.add("0" + tmp + "0");
    			rst.add("1" + tmp + "1");
    			rst.add("6" + tmp + "9");
    			rst.add("8" + tmp + "8");
    			rst.add("9" + tmp + "6");
    		}
    		return rst;
    	}
    }
</p>


### Python solution 180ms
- Author: yawnzheng
- Creation Date: Tue Aug 18 2015 05:20:29 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 12:40:43 GMT+0800 (Singapore Standard Time)

<p>
The solution is also base on strobogrammatic number II, but with a little optimization.

    class Solution:
    # @param {string} low
    # @param {string} high
    # @return {integer}
    def strobogrammaticInRange(self, low, high):
        a=self.below(high)
        b=self.below(low,include=False)
        return a-b if a>b else 0
    
    '''
    get how many strobogrammatic numbers less than n
    '''
    def below(self,n,include=True):
        res=0
        for i in range(1,len(n)):
            res+=self.number(i)
        l=self.strobogrammatic(len(n))
        '''
        filter num larger than n and start with 0
        '''
        if include:
            l=[num for num in l if (len(num)==1 or num[0]!='0') and num<=n]
        else:
            l=[num for num in l if (len(num)==1 or num[0]!='0') and num<n]
        return res+len(l)
    
    '''
    get strobogrammatic numbers with length l
    number start with 0 would be included
    '''
    def strobogrammatic(self,l):
        res=[]
        if l==1:
            return ['0','1','8']
        if l==2:
            return ['00','11','69','96','88']
        for s in self.strobogrammatic(l-2):
            res.append('0'+s+'0')
            res.append('1'+s+'1')
            res.append('6'+s+'9')
            res.append('8'+s+'8')
            res.append('9'+s+'6')
        return res

    '''
    get number of strobogrammatic numbers of length l
    '''
    def number(self,l):
        if l==0:
            return 0
        '''
        If l is an even number, the first digit has four choices (1,6,8,9). digits 
        at other position have five choices(0,1,6,8,9)
        '''
        if l%2==0:
            return 4*(5**(l/2-1))
        '''
        If l is an odd number, the first digit has four choices (1,6,8,9) and digit 
        at the middle has 3 choices (0,1,8),other digits have 5 choices.
        digit at other position could be 0,1,6,8,9
        '''
        elif l==1:
            return 3
        else:
            return 3*(5**(l/2-1))*4
</p>


