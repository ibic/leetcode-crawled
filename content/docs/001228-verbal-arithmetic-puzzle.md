---
title: "Verbal Arithmetic Puzzle"
weight: 1228
#id: "verbal-arithmetic-puzzle"
---
## Description
<div class="description">
<p>Given an equation, represented by <code>words</code> on left side and the <code>result</code> on right side.</p>

<p>You need to check if the equation is solvable&nbsp;under the following rules:</p>

<ul>
	<li>Each character is decoded as one digit (0 - 9).</li>
	<li>Every pair of different characters they must map to different digits.</li>
	<li>Each <code>words[i]</code> and <code>result</code>&nbsp;are decoded as one number <strong>without</strong> leading zeros.</li>
	<li>Sum of numbers on left side (<code>words</code>) will equal to the number on right side (<code>result</code>).&nbsp;</li>
</ul>

<p>Return <code>True</code>&nbsp;if the equation is solvable otherwise&nbsp;return&nbsp;<code>False</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> words = [&quot;SEND&quot;,&quot;MORE&quot;], result = &quot;MONEY&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong> Map &#39;S&#39;-&gt; 9, &#39;E&#39;-&gt;5, &#39;N&#39;-&gt;6, &#39;D&#39;-&gt;7, &#39;M&#39;-&gt;1, &#39;O&#39;-&gt;0, &#39;R&#39;-&gt;8, &#39;Y&#39;-&gt;&#39;2&#39;
Such that: &quot;SEND&quot; + &quot;MORE&quot; = &quot;MONEY&quot; ,  9567 + 1085 = 10652</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> words = [&quot;SIX&quot;,&quot;SEVEN&quot;,&quot;SEVEN&quot;], result = &quot;TWENTY&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong> Map &#39;S&#39;-&gt; 6, &#39;I&#39;-&gt;5, &#39;X&#39;-&gt;0, &#39;E&#39;-&gt;8, &#39;V&#39;-&gt;7, &#39;N&#39;-&gt;2, &#39;T&#39;-&gt;1, &#39;W&#39;-&gt;&#39;3&#39;, &#39;Y&#39;-&gt;4
Such that: &quot;SIX&quot; + &quot;SEVEN&quot; + &quot;SEVEN&quot; = &quot;TWENTY&quot; ,  650 + 68782 + 68782 = 138214</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> words = [&quot;THIS&quot;,&quot;IS&quot;,&quot;TOO&quot;], result = &quot;FUNNY&quot;
<strong>Output:</strong> true
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> words = [&quot;LEET&quot;,&quot;CODE&quot;], result = &quot;POINT&quot;
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= words.length &lt;= 5</code></li>
	<li><code>1 &lt;= words[i].length,&nbsp;result.length&nbsp;&lt;= 7</code></li>
	<li><code>words[i], result</code>&nbsp;contains only upper case English letters.</li>
	<li>Number of different characters used on the expression is at most&nbsp;10.</li>
</ul>

</div>

## Tags
- Math (math)
- Backtracking (backtracking)

## Companies
- Atlassian - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Please help understand how someone got this accepted
- Author: ritam777
- Creation Date: Sun Dec 29 2019 12:05:39 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 30 2019 07:08:20 GMT+0800 (Singapore Standard Time)

<p>
I was looking at people\'s solutions after contest and found this successful submission by the contestant #22 and #241 in the contest. What is the trick behind this?

**EDIT:** The problem had just 26 test cases, and can only have a `True` or `False` response, hence all the cheaters decided to probe the test cases using multiple accounts, then submit the final solution using a direct map from input to output from their main account.

```
class Solution:
    def isSolvable(self, words: List[str], result: str) -> bool:
        r = result
        rset = set(["MONEY","TWENTY","FUNNY","INDEED","PLANETS","PEOPLE","FALSE","TRUE","NOVENTA","EAII","DHCF","CANCER","TREES","EUROPE","ABCDE","FBCDE","EEIE"])
        if r in rset:
            return True
```
</p>


### [Java] Fast Backtracking - Clean Code - O(n!) ~ 300ms
- Author: hiepit
- Creation Date: Sun Dec 29 2019 12:39:48 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jan 01 2020 12:04:34 GMT+0800 (Singapore Standard Time)

<p>
**Complexity**
- Time: `O(n!)`, `n` is the number of different characters of `words` and `result`, `n <= 10` 
- Space: `O(n)`

**Java**
```java
class Solution {
    private static final int[] POW_10 = new int[]{1, 10, 100, 1000, 10000, 100000, 1000000};
    public boolean isSolvable(String[] words, String result) {
        Set<Character> charSet = new HashSet<>();
        int[] charCount = new int[91];
        boolean[] nonLeadingZero = new boolean[91]; // ASCII of `A..Z` chars are in range `65..90`
        for (String word : words) {
            char[] cs = word.toCharArray();
            for (int i = 0; i < cs.length; i++) {
                if (i == 0 && cs.length > 1) nonLeadingZero[cs[i]] = true;
                charSet.add(cs[i]);
                charCount[cs[i]] += POW_10[cs.length - i - 1]; // charCount is calculated by units
            }
        }
        char[] cs = result.toCharArray();
        for (int i = 0; i < cs.length; i++) {
            if (i == 0 && cs.length > 1) nonLeadingZero[cs[i]] = true;
            charSet.add(cs[i]);
            charCount[cs[i]] -= POW_10[cs.length - i - 1]; // charCount is calculated by units
        }
        boolean[] used = new boolean[10];
        char[] charList = new char[charSet.size()];
        int i = 0;
        for (char c : charSet) charList[i++] = c;
        return backtracking(used, charList, nonLeadingZero, 0, 0, charCount);
    }

    private boolean backtracking(boolean[] used, char[] charList, boolean[] nonLeadingZero, int step, int diff, int[] charCount) {
        if (step == charList.length) return diff == 0; // difference between sum of words and result equal to 0
        for (int d = 0; d <= 9; d++) { // each character is decoded as one digit (0 - 9).
            char c = charList[step];
            if (!used[d] // each different characters must map to different digits
                    && (d > 0 || !nonLeadingZero[c])) {  // decoded as one number without leading zeros.
                used[d] = true;
                if (backtracking(used, charList, nonLeadingZero, step + 1, diff + charCount[c] * d, charCount)) return true;
                used[d] = false;
            }
        }
        return false;
    }
}
```
</p>


### C++ 12ms DFS & Backtracking & Prunning Strategy
- Author: jiah
- Creation Date: Sun Dec 29 2019 12:22:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 05 2020 02:04:37 GMT+0800 (Singapore Standard Time)

<p>
Idea:
- dfs & backtracking, then it is all about early prunning.
- mimic the process we do addition from least significant digit to most significant digit. so that we can compute sum of `words` on each digit position, and compare it with that of `result`, prune if conflict.
- also prune if there are leading zeros in words or `result`.

The following code runs about 236ms. @ZhiwenLuo also shared a polished version which runs 12ms, you can find it in the comments. 
```cpp
class Solution {
    // chracter to digit mapping, and the inverse
	// (if you want better performance: use array instead of unordered_map)
    unordered_map<char, int> c2i;
    unordered_map<int, char> i2c;
	// limit: length of result
    int limit = 0;
	// digit: index of digit in a word, widx: index of a word in word list, sum: summation of all word[digit]  
    bool helper(vector<string>& words, string& result, int digit, int widx, int sum) { 
        if (digit == limit) {
            return sum == 0;
        }
		// if summation at digit position complete, validate it with result[digit].
        if (widx == words.size()) {
            if (c2i.count(result[digit]) == 0 && i2c.count(sum%10) == 0) {
                if (sum%10 == 0 && digit+1 == limit) // Avoid leading zero in result
                    return false;
                c2i[result[digit]] = sum % 10;
                i2c[sum%10] = result[digit];
                bool tmp = helper(words, result, digit+1, 0, sum/10);
                c2i.erase(result[digit]);
                i2c.erase(sum%10);
                return tmp;
            } else if (c2i.count(result[digit]) && c2i[result[digit]] == sum % 10){
                return helper(words, result, digit+1, 0, sum/10);
            } else {
                return false;
            }
        }
		// if word[widx] length less than digit, ignore and go to next word
        if (digit >= words[widx].length()) {
            return helper(words, result, digit, widx+1, sum);
        }
		// if word[widx][digit] already mapped to a value
        if (c2i.count(words[widx][digit])) {
            if (digit+1 == words[widx].length() && words[widx].length() > 1 && c2i[words[widx][digit]] == 0) 
                return false;
            return helper(words, result, digit, widx+1, sum+c2i[words[widx][digit]]);
        }
		// if word[widx][digit] not mapped to a value yet
        for (int i = 0; i < 10; i++) {
            if (digit+1 == words[widx].length() && i == 0 && words[widx].length() > 1) continue;
            if (i2c.count(i)) continue;
            c2i[words[widx][digit]] = i;
            i2c[i] = words[widx][digit];
            bool tmp = helper(words, result, digit, widx+1, sum+i);
            c2i.erase(words[widx][digit]);
            i2c.erase(i);
            if (tmp) return true;
        }
        return false;
    }
public:
    bool isSolvable(vector<string>& words, string result) {
        limit = result.length();
        for (auto &w: words) 
            if (w.length() > limit) 
				return false;
        for (auto&w:words) 
            reverse(w.begin(), w.end());
        reverse(result.begin(), result.end());
        return helper(words, result, 0, 0, 0);
    }
};
```
</p>


