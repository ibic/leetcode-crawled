---
title: "Count Student Number in Departments"
weight: 1490
#id: "count-student-number-in-departments"
---
## Description
<div class="description">
<p>A university uses 2 data tables, <b><i>student</i></b> and <b><i>department</i></b>, to store data about its students and the departments associated with each major.</p>

<p>Write a query to print the respective department name and number of students majoring in each department for all departments in the <b><i>department</i></b> table (even ones with no current students).</p>

<p>Sort your results by descending number of students; if two or more departments have the same number of students, then sort those departments alphabetically by department name.</p>

<p>The <b><i>student</i></b> is described as follow:</p>

<pre>
| Column Name  | Type      |
|--------------|-----------|
| student_id   | Integer   |
| student_name | String    |
| gender       | Character |
| dept_id      | Integer   |
</pre>

<p>where student_id is the student&#39;s ID number, student_name is the student&#39;s name, gender is their gender, and dept_id is the department ID associated with their declared major.</p>

<p>And the <b><i>department</i></b> table is described as below:</p>

<pre>
| Column Name | Type    |
|-------------|---------|
| dept_id     | Integer |
| dept_name   | String  |
</pre>

<p>where dept_id is the department&#39;s ID number and dept_name is the department name.</p>

<p>Here is an example <b>input</b>:<br />
<b><i>student</i></b> table:</p>

<pre>
| student_id | student_name | gender | dept_id |
|------------|--------------|--------|---------|
| 1          | Jack         | M      | 1       |
| 2          | Jane         | F      | 1       |
| 3          | Mark         | M      | 2       |
</pre>

<p><b><i>department</i></b> table:</p>

<pre>
| dept_id | dept_name   |
|---------|-------------|
| 1       | Engineering |
| 2       | Science     |
| 3       | Law         |
</pre>

<p>The <b>Output</b> should be:</p>

<pre>
| dept_name   | student_number |
|-------------|----------------|
| Engineering | 2              |
| Science     | 1              |
| Law         | 0              |
</pre>

</div>

## Tags


## Companies
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `OUTER JOIN` and `COUNT(expression)` [Accepted]

**Intuition**

Use `GROUP BY` function can measure student number in a department, and then use `COUNT` function to count the number of records of each department.

**Algorithm**

We can use `OUTER JOIN` to query all departments. The problem is to display '0' for departments without no current students. Some people will write the following query using `COUNT(*)`.

```sql
SELECT
    dept_name, COUNT(*) AS student_number
FROM
    department
        LEFT OUTER JOIN
    student ON department.dept_id = student.dept_id
GROUP BY department.dept_name
ORDER BY student_number DESC , department.dept_name
;
```

Unfortunately, it wrongly displays '1' for departments like 'Law' without current students for the sample input.
```
| dept_name   | student_number |
|-------------|----------------|
| Engineering | 2              |
| Law         | 1              |
| Science     | 1              |
```
Instead, `COUNT(expression)` could be used because it does not take account if `expression is null`. You can refer to the [MySQL manual](https://dev.mysql.com/doc/refman/5.7/en/counting-rows.html) for the details.

Thus, here is a right solution after fixing the issue above.

**MySQL**

```sql
SELECT
    dept_name, COUNT(student_id) AS student_number
FROM
    department
        LEFT OUTER JOIN
    student ON department.dept_id = student.dept_id
GROUP BY department.dept_name
ORDER BY student_number DESC , department.dept_name
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted Easy Solution Using Right Join
- Author: protocols1919
- Creation Date: Tue Jun 27 2017 13:45:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 08:35:28 GMT+0800 (Singapore Standard Time)

<p>

```
SELECT d.dept_name, COUNT(s.student_id) AS student_number
FROM student s RIGHT JOIN department d ON s.dept_id = d.dept_id
GROUP BY d.dept_name 
ORDER BY student_number DESC, d.dept_name;

```
"ORDER BY d.dept_name" is needed as well (according to the problem description), otherwise you will get a wrong answer.
</p>


### MySQL: Using SUM(CASE WHEN...) instead of COUNT
- Author: BjornCommers
- Creation Date: Tue Jul 14 2020 03:26:16 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 14 2020 03:26:16 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT dept_name, SUM(CASE WHEN s.dept_id THEN 1 ELSE 0 END) as student_number
FROM department d LEFT JOIN student s ON d.dept_id = s.dept_id
GROUP BY 1
ORDER BY 2 desc, 1
```
</p>


### My solution using coalesce
- Author: siwest
- Creation Date: Wed Jul 12 2017 07:57:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 26 2018 00:33:57 GMT+0800 (Singapore Standard Time)

<p>
``` 
SELECT department.dept_name, COALESCE(student_number, 0) AS student_number
FROM department
LEFT JOIN
(SELECT count(student_id) as student_number, dept_id
    FROM student
    GROUP BY dept_id ) t1 
 ON t1.dept_id = department.dept_id
ORDER BY t1.student_number DESC, department.dept_name ```
</p>


