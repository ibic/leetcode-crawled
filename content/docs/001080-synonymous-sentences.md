---
title: "Synonymous Sentences"
weight: 1080
#id: "synonymous-sentences"
---
## Description
<div class="description">
Given a list of pairs of equivalent words&nbsp;<code>synonyms</code> and a sentence <code>text</code>,&nbsp;Return all possible synonymous sentences <strong>sorted lexicographically</strong>.
<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:
</strong>synonyms = [[&quot;happy&quot;,&quot;joy&quot;],[&quot;sad&quot;,&quot;sorrow&quot;],[&quot;joy&quot;,&quot;cheerful&quot;]],
text = &quot;I am happy today but was sad yesterday&quot;
<strong>Output:
</strong>[&quot;I am cheerful today but was sad yesterday&quot;,
&quot;I am cheerful today but was sorrow yesterday&quot;,
&quot;I am happy today but was sad yesterday&quot;,
&quot;I am happy today but was sorrow yesterday&quot;,
&quot;I am joy today but was sad yesterday&quot;,
&quot;I am joy today but was sorrow yesterday&quot;]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> synonyms = [[&quot;happy&quot;,&quot;joy&quot;],[&quot;cheerful&quot;,&quot;glad&quot;]], text = &quot;I am happy today but was sad yesterday&quot;
<strong>Output:</strong> [&quot;I am happy today but was sad yesterday&quot;,&quot;I am joy today but was sad yesterday&quot;]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> synonyms = [[&quot;a&quot;,&quot;b&quot;],[&quot;c&quot;,&quot;d&quot;],[&quot;e&quot;,&quot;f&quot;]], text = &quot;a c e&quot;
<strong>Output:</strong> [&quot;a c e&quot;,&quot;a c f&quot;,&quot;a d e&quot;,&quot;a d f&quot;,&quot;b c e&quot;,&quot;b c f&quot;,&quot;b d e&quot;,&quot;b d f&quot;]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> synonyms = [[&quot;a&quot;,&quot;QrbCl&quot;]], text = &quot;d QrbCl ya ya NjZQ&quot;
<strong>Output:</strong> [&quot;d QrbCl ya ya NjZQ&quot;,&quot;d a ya ya NjZQ&quot;]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;=&nbsp;synonyms.length &lt;= 10</code></li>
	<li><code>synonyms[i].length == 2</code></li>
	<li><code>synonyms[i][0] != synonyms[i][1]</code></li>
	<li>All words consist of at most <code>10</code> English letters only.</li>
	<li><code>text</code>&nbsp;is a single space separated sentence of at most <code>10</code> words.</li>
</ul>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Cruise Automation - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] BFS Solution - Picture  Explain - Clean code
- Author: hiepit
- Creation Date: Sun Nov 17 2019 01:48:35 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Mar 25 2020 09:03:56 GMT+0800 (Singapore Standard Time)

<p>
**Example Input:**
- synonyms = [["happy","joy"],["strong","healthy"],["joy","cheerful"]],
- text = "I am happy and strong"

This solution will work like below picture
![image](https://assets.leetcode.com/users/hiepit/image_1573965953.png)


```java
class Solution {
    public List<String> generateSentences(List<List<String>> synonyms, String text) {
        Map<String, List<String>> graph = new HashMap<>();
        for (List<String> pair : synonyms) {
            String w1 = pair.get(0), w2 = pair.get(1);
            connect(graph, w1, w2);
            connect(graph, w2, w1);
        }
        // BFS
        Set<String> ans = new TreeSet<>();
        Queue<String> q = new LinkedList<>();
        q.add(text);
        while (!q.isEmpty()) {
            String out = q.remove();
            ans.add(out); // Add to result
            String[] words = out.split("\\s");
            for (int i = 0; i < words.length; i++) {
                if (graph.get(words[i]) == null) continue;
                for (String synonym : graph.get(words[i])) { // Replace words[i] with its synonym
                    words[i] = synonym;
                    String newText = String.join(" ", words);
                    if (!ans.contains(newText)) q.add(newText);
                }
            }
        }
        return new ArrayList<>(ans);
    }
    void connect(Map<String, List<String>> graph, String v1, String v2) {
        graph.putIfAbsent(v1, new LinkedList<>());
        graph.get(v1).add(v2);
    }
}
```
</p>


### Python bfs solution
- Author: Luolingwei
- Creation Date: Sun Nov 17 2019 00:23:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 02:39:59 GMT+0800 (Singapore Standard Time)

<p>
```python
class Solution:
    def generateSentences(self, synonyms: List[List[str]], text: str) -> List[str]:
        graph=collections.defaultdict(dict)
        bfs=collections.deque()
        ans=set()
        bfs.append(text)
        for k,v in synonyms:
            graph[k][v]=1
            graph[v][k]=1
        while bfs:
            curT=bfs.popleft()
            ans.add(curT)
            words=curT.split()
            for i,w in enumerate(words):
                if w in graph.keys():
                    for newW in graph[w]:
                        newsent=\' \'.join(words[:i]+[newW]+words[i+1:])
                        if newsent not in ans:
                            bfs.append(newsent)
        return sorted(list(ans))
</p>


### python union find
- Author: liketheflower
- Creation Date: Sun Nov 17 2019 00:02:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 05:27:17 GMT+0800 (Singapore Standard Time)

<p>
1. Using union-find to collect all the synonyms words. After union find, all synonyms words will have a same root which is find(word) as path compression is used in union find.
2. Put all synonyms words in a list, if the word doesn\'t have any synonyms words, put itself in the list. We will have something like this.  [ [\'I\'], [\'am\'],  [\'happy\', \'joy\']]
3.  Use itertools.product to generate all the combinations.
4.  Final results can be generated by " ".join and sorted.
```python
class Solution:
    def generateSentences(self, synonyms: List[List[str]], text: str) -> List[str]:
        uf = {}
        
        def union(x, y):
            uf[find(y)] = find(x)
            
        def find(x):
            uf.setdefault(x, x)
            if uf[x]!=x:
                uf[x] = find(uf[x])
            return uf[x]
        
        for a,b in synonyms:
            union(a, b)
            
        d = collections.defaultdict(set)
        for a, b in synonyms:
            root = find(a)
            d[root] |= set([a, b])
        txt = text.split()
        res = []
        for t in txt:
            if t not in uf:
                res.append([t])
            else:
                r = find(t)
                res.append(list(d[r]))
        fin_res = [" ".join(sentence) for sentence in itertools.product(*res)]
        return sorted(fin_res)
```
</p>


