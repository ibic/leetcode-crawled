---
title: "Product of Array Except Self"
weight: 222
#id: "product-of-array-except-self"
---
## Description
<div class="description">
<p>Given an array <code>nums</code> of <em>n</em> integers where <em>n</em> &gt; 1, &nbsp;return an array <code>output</code> such that <code>output[i]</code> is equal to the product of all the elements of <code>nums</code> except <code>nums[i]</code>.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b>  <code>[1,2,3,4]</code>
<b>Output:</b> <code>[24,12,8,6]</code>
</pre>

<p><strong>Constraint:</strong>&nbsp;It&#39;s guaranteed that the product of the elements of any prefix or suffix of the array (including the whole array) fits in a 32 bit integer.</p>

<p><strong>Note: </strong>Please solve it <strong>without division</strong> and in O(<em>n</em>).</p>

<p><strong>Follow up:</strong><br />
Could you solve it with constant space complexity? (The output array <strong>does not</strong> count as extra space for the purpose of space complexity analysis.)</p>

</div>

## Tags
- Array (array)

## Companies
- Facebook - 103 (taggedByAdmin: true)
- Amazon - 27 (taggedByAdmin: true)
- Asana - 12 (taggedByAdmin: false)
- Lyft - 11 (taggedByAdmin: false)
- Apple - 5 (taggedByAdmin: true)
- Uber - 5 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: true)
- Google - 3 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Goldman Sachs - 15 (taggedByAdmin: false)
- Oracle - 6 (taggedByAdmin: false)
- Adobe - 4 (taggedByAdmin: false)
- Tableau - 4 (taggedByAdmin: false)
- Qualtrics - 4 (taggedByAdmin: false)
- Nutanix - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Splunk - 2 (taggedByAdmin: false)
- Houzz - 2 (taggedByAdmin: false)
- Yahoo - 6 (taggedByAdmin: false)
- VMware - 4 (taggedByAdmin: false)
- Groupon - 3 (taggedByAdmin: false)
- Visa - 3 (taggedByAdmin: false)
- Walmart Labs - 3 (taggedByAdmin: false)
- BlackRock - 2 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Intel - 2 (taggedByAdmin: false)
- Grab - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Evernote - 2 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- LinkedIn - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

From the looks of it, this seems like a simple enough problem to solve in linear time and space. We can simply take the product of all the elements in the given array and then, for each of the elements $$x$$ of the array, we can simply find `product of array except self` value by dividing the product by $$x$$. 

Doing this for each of the elements would solve the problem. However, there's a note in the problem which says that we are not allowed to use division operation. That makes solving this problem a bit harder. 
<br/>
<br/>

---

#### Approach 1: Left and Right product lists

It's much easier to build an intuition for solving this problem without division once you visualize how the different `products except self` look like for each of the elements. So, let's take a look at an example array and the different products.

<center>
<img src="../Figures/238/diag-1.png" width="700"></center>

Looking at the figure about we can figure another way of computing those different product values. 

>Instead of dividing the product of all the numbers in the array by the number at a given index to get the corresponding product, we can make use of the product of all the numbers to the left and all the numbers to the right of the index. Multiplying these two individual products would give us the desired result as well.

For every given index, $$i$$, we will make use of the product of all the numbers to the left of it and multiply it by the product of all the numbers to the right. This will give us the product of all the numbers except the one at the given index $$i$$. Let's look at a formal algorithm describing this idea more concretely.

**Algorithm**

1. Initialize two empty arrays, `L` and `R` where for a given index `i`, `L[i]` would contain the product of all the numbers to the left of `i` and `R[i]` would contain the product of all the numbers to the right of `i`. 
2. We would need two different loops to fill in values for the two arrays. For the array `L`, $$L[0]$$ would be `1` since there are no elements to the left of the first element. For the rest of the elements, we simply use $$L[i] = L[i - 1] * nums[i - 1]$$. Remember that `L[i]` represents product of all the elements *to the left of element at index i*.
3. For the other array, we do the same thing but in reverse i.e. we start with the initial value of `1` in $$R[length - 1]$$ where $$length$$ is the number of elements in the array, and keep updating `R[i]` in reverse. Essentially, $$R[i] = R[i + 1] * nums[i + 1]$$. Remember that `R[i]` represents product of all the elements *to the right of element at index i*.
4. Once we have the two arrays set up properly, we simply iterate over the input array one element at a time, and for each element at index `i`, we find the `product except self` as $$L[i] * R[i]$$. 

Let's go over a simple run of the algorithm that clearly depicts the construction of the two intermediate arrays and finally the answer array.

!?!../Documents/238_anim1.json:770,460!?!

For the given array $$[4,5,1,8,2]$$, the `L` and `R` arrays would finally be:

<center>
<img src="../Figures/238/products.png" width="700"></center>

<iframe src="https://leetcode.com/playground/rxG7teLA/shared" frameBorder="0" width="100%" height="500" name="rxG7teLA"></iframe>

**Complexity analysis**

* Time complexity : $$O(N)$$ where $$N$$ represents the number of elements in the input array. We use one iteration to construct the array $$L$$, one to construct the array $$R$$ and one last to construct the $$answer$$ array using $$L$$ and $$R$$.
* Space complexity : $$O(N)$$ used up by the two intermediate arrays that we constructed to keep track of product of elements to the left and right.
<br/>
<br/>

---

#### Approach 2: O(1) space approach

Although the above solution is good enough to solve the problem since we are not using division anymore, there's a follow-up component as well which asks us to solve this using constant space. Understandably so, the output array *does not* count towards the space complexity. This approach is essentially an extension of the approach above. Basically, we will be using the output array as one of `L` or `R` and we will be constructing the other one on the fly. Let's look at the algorithm based on this idea.

**Algorithm**

1. Initialize the empty `answer` array where for a given index `i`, `answer[i]` would contain the product of all the numbers to the left of `i`. 
2. We construct the `answer` array the same way we constructed the `L` array in the previous approach. These two algorithms are exactly the same except that we are trying to save up on space.
3. The only change in this approach is that we don't explicitly build the `R` array from before. Instead, we simply use a variable to keep track of the running product of elements to the right and we keep updating the `answer` array by doing $$answer[i] = answer[i] * R$$. For a given index `i`, `answer[i]` contains the product of all the elements to the left and `R` would contain product of all the elements to the right. We then update `R` as $$R = R * nums[i]$$

<iframe src="https://leetcode.com/playground/zdLiyQ6e/shared" frameBorder="0" width="100%" height="500" name="zdLiyQ6e"></iframe>

**Complexity analysis**

* Time complexity : $$O(N)$$ where $$N$$ represents the number of elements in the input array. We use one iteration to construct the array $$L$$, one to update the array $$answer$$.
* Space complexity : $$O(1)$$ since don't use any additional array for our computations. The problem statement mentions that using the $$answer$$ array doesn't add to the space complexity.
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java solution in O(n) without extra space
- Author: lycjava3
- Creation Date: Thu Jul 16 2015 07:59:47 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 22:20:31 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
    public int[] productExceptSelf(int[] nums) {
        int n = nums.length;
        int[] res = new int[n];
        res[0] = 1;
        for (int i = 1; i < n; i++) {
            res[i] = res[i - 1] * nums[i - 1];
        }
        int right = 1;
        for (int i = n - 1; i >= 0; i--) {
            res[i] *= right;
            right *= nums[i];
        }
        return res;
    }
}
</p>


### Python solution (Accepted), O(n) time, O(1) space
- Author: hqpdash
- Creation Date: Fri Jul 17 2015 10:48:52 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 10:00:46 GMT+0800 (Singapore Standard Time)

<p>
    class Solution:
        # @param {integer[]} nums
        # @return {integer[]}
        def productExceptSelf(self, nums):
            p = 1
            n = len(nums)
            output = []
            for i in range(0,n):
                output.append(p)
                p = p * nums[i]
            p = 1
            for i in range(n-1,-1,-1):
                output[i] = output[i] * p
                p = p * nums[i]
            return output
</p>


### My simple Java solution
- Author: xcv58
- Creation Date: Sat Jul 18 2015 00:22:49 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 21:53:41 GMT+0800 (Singapore Standard Time)

<p>
Use `tmp` to store temporary multiply result by two directions. Then fill it into `result`. Bingo!

    public int[] productExceptSelf(int[] nums) {
        int[] result = new int[nums.length];
        for (int i = 0, tmp = 1; i < nums.length; i++) {
            result[i] = tmp;
            tmp *= nums[i];
        }
        for (int i = nums.length - 1, tmp = 1; i >= 0; i--) {
            result[i] *= tmp;
            tmp *= nums[i];
        }
        return result;
    }
</p>


