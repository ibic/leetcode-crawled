---
title: "Decode Ways II"
weight: 572
#id: "decode-ways-ii"
---
## Description
<div class="description">
<p>
A message containing letters from <code>A-Z</code> is being encoded to numbers using the following mapping way:
</p>

<pre>
'A' -> 1
'B' -> 2
...
'Z' -> 26
</pre>

<p>
Beyond that, now the encoded string can also contain the character '*', which can be treated as one of the numbers from 1 to 9.
</p>


<p>
Given the encoded message containing digits and the character '*', return the total number of ways to decode it.
</p>

<p>
Also, since the answer may be very large, you should return the output mod 10<sup>9</sup> + 7.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> "*"
<b>Output:</b> 9
<b>Explanation:</b> The encoded message can be decoded to the string: "A", "B", "C", "D", "E", "F", "G", "H", "I".
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> "1*"
<b>Output:</b> 9 + 9 = 18
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The length of the input string will fit in range [1, 10<sup>5</sup>].</li>
<li>The input string will only contain the character '*' and digits '0' - '9'.</li>
</ol>
</p>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: true)

## Official Solution
[TOC]


## Solution

---
#### Approach 1: Recursion with Memoization

**Algorithm**

In order to find the solution to the given problem, we need to consider every case possible(for the arrangement of the input digits/characters)
 and what value needs to be considered for each case. Let's look at each of the possibilites one by one.
 
Firstly, let's assume, we have a function `ways(s,i)` which returns the number of ways to decode the input string $$s$$, if only the characters upto the 
$$i^{th}$$ index in this string are considered. We start off by calling the function `ways(s, s.length()-1)` i.e. by considering the full length of this string $$s$$.

We started by using the last index of the string $$s$$. Suppose, currently, we called the function as `ways(s,i)`. Let's look at how we proceed. At every step, we need 
to look at the current character at the last index ($$i$$) and we need to determine the number of ways of decoding that using this $$i^{th}$$ character could 
add to the total value. There are the following possiblities for the $$i^{th}$$ character.

The $$i^{th}$$ character could be  a `*`. In this case, firstly, we can see that this `*` could be decoded into any of the digits from `1-9`. Thus, for every decoding possible 
upto the index $$i-1$$, this `*` could be replaced by any of these digits(`1-9`). Thus, the total number of decodings is 9 times the number of decodings possible 
for the same string upto the index $$i-1$$. Thus, this `*` initially adds a factor of `9*ways(s,i-1)` to the total value. 

![Decode_Ways](../Figures/639/639_Decode_Ways2.png)
{:align="center"}

Apart from this, this `*` at the $$i^{th}$$ index could also contribute further to the total number of ways depending upon the character/digit at its preceding
 index. If the preceding character happens to be a `1`, by combining this `1` with the current `*`, we could obtain any of the digits from `11-19` which could be decoded
 into any of the characters from `K-S`. We need to note that these decodings are in addition to the ones already obtained above by considering only a single current 
 `*`(`1-9` decoding to `A-J`). Thus, this `1*` pair could be replaced by any of the numbers from `11-19` irrespective of the decodings done for the previous 
 indices(before $$i-1$$). Thus, this `1*` pair leads to 8 times the number of decodings possible with the string $$s$$ upto the index $$i-2$$. Thus, this adds
 a factor of `9 * ways(s, i - 2)` to the total number of decodings. 
 
 Similarly, a `2*` pair obtained by a `2` at the index $$i-1$$ could be considered of the numbers from `21-26`(decoding into `U-Z`), adding a total of 6 times the 
 number of decodings possible upto the index $$i-2$$. 
 
 
 ![Decode_Ways](../Figures/639/639_Decode_Ways3.PNG)
{:align="center"}

On the same basis, if the character at the index $$i-1$$ happens to be another `*`, this `**` pairing could be considered as 
 any of the numbers from `11-19`(9) and `21-26`(6). Thus, the total number of decodings will be 15(9+6) times  the number of decodings possible upto the index $$i-2$$.
 
 Now, if the $$i^{th}$$ character could be a digit from `1-9` as well. In this case, the number of decodings that considering this single digit can 
 contribute to the total number is equal to the number of decodings that can be contributed by the digits upto the index $$i-1$$. But, if the $$i^{th}$$ character is  
 a `0`, this `0` alone can't contribute anything to the total number of decodings(but it can only contribute if the digit preceding it is a `1` or `2`. We'll consider this case below).
 
 Apart from the value obtained(just above) for the digit at the $$i^{th}$$ index being anyone from `0-9`, this digit could also pair with the digit at the 
 preceding index, contributing a value dependent on the previous digit. If the previous digit happens to be a `1`, this `1` can combine with any of the current 
digits forming a valid number in the range `10-19`. Thus, in this case, we can consider a pair formed by the current and the preceding digit, and, the number of 
decodings possible by considering the decoded character to be a one formed using this pair, is equal to the total number of decodings possible by using the digits 
upto the index $$i-2$$ only. 

But, if the previous digit is a `2`, a valid number for decoding could only be a one from the range `20-26`. Thus, if the current digit is lesser than 7, again
this pairing could add decodings with count equal to the ones possible by using the digits upto the $$(i-2)^{th}$$ index only.

Further, if the previous digit happens to be a `*`, the additional number of decodings depend on the current digit again i.e. If the current digit is greater than 
`6`, this `*` could lead to pairings only in the range `17-19`(`*` can't be replaced by `2` leading to `27-29`). Thus, additional decodings with count equal to the
decodings possible upto the index $$i-2$$. 

On the other hand, if the current digit is lesser than 7, this `*` could be replaced by either a `1` or a `2` leading to the 
decodings `10-16` and `20-26` respectively. Thus, the total number of decodings possible by considering this pair is equal to twice the number of decodings possible upto the 
index $$i-2$$(since `*` can now be replaced by two values).

This way, by considering every possible case, we can obtain the required number of decodings by making use of the recursive function `ways` as and where necessary.

By making use of memoization, we can reduce the time complexity owing to duplicate function calls.

<iframe src="https://leetcode.com/playground/YYXTLrAW/shared" frameBorder="0" width="100%" height="500" name="YYXTLrAW"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Size of recursion tree can go upto $$n$$, since $$memo$$ array is filled exactly once. Here, $$n$$ refers to the length of the input 
string.

* Space complexity : $$O(n)$$. The depth of recursion tree can go upto $$n$$.
<br />
<br />
---
#### Approach 2: Dynamic Programming

**Algorithm**

From the solutions discussed above, we can observe that the number of decodings possible upto any index, $$i$$, is dependent only on the characters upto the 
index $$i$$ and not on any of the characters following it. This leads us to the idea that this problem can be solved by making use of Dynamic Programming.

We can also easily observe from the recursive solution that, the number of decodings possible upto the index $$i$$ can be determined easily if we know 
the number of decodings possible upto the index $$i-1$$ and $$i-2$$. Thus, we fill in the $$dp$$ array in a forward manner. $$dp[i]$$ is used to store the 
number of decodings possible by considering the characters in the given string $$s$$ upto the $$(i-1)^{th}$$ index only(including it).

The equations for filling this $$dp$$ at any step again depend on the current character and the just preceding character. These equations are similar 
to the ones used in the recursive solution.

The following animation illustrates the process of filling the $$dp$$ for a simple example.


!?!../Documents/639_Decode_Ways_II.json:1000,563!?!

<iframe src="https://leetcode.com/playground/NEgK3Wqh/shared" frameBorder="0" width="100%" height="500" name="NEgK3Wqh"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. $$dp$$ array of size $$n+1$$ is filled once only. Here, $$n$$ refers to the length of the input string.

* Space complexity : $$O(n)$$. $$dp$$ array of size $$n+1$$ is used.
<br />
<br />
---
#### Approach 3: Constant Space Dynamic Programming

**Algorithm**

In the last approach, we can observe that only the last two values $$dp[i-2]$$ and $$dp[i-1]$$ are used to fill the entry at $$dp[i-1]$$. We can save some 
space in the last approach, if instead of maintaining a whole $$dp$$ array of length $$n$$, we keep a track of only the required last two values. The rest of the 
process remains the same as in the last approach.

<iframe src="https://leetcode.com/playground/g3AXEb8L/shared" frameBorder="0" width="100%" height="500" name="g3AXEb8L"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Single loop upto $$n$$ is required to find the required result. Here, $$n$$ refers to the length of the input string $$s$$.

* Space complexity : $$O(1)$$. Constant space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(N) by General Solution for all DP problems
- Author: Luckman
- Creation Date: Tue Jul 11 2017 06:03:45 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 04:36:22 GMT+0800 (Singapore Standard Time)

<p>
Here, I try to provide not only code but also the steps and thoughts of solving this problem completely which can also be applied to many other DP problems.

(1) Try to solve this problem in Backtracking way, because it is the most straight-forward method. 
```
 E.g '12*3'
                   12*3
              /             \
           12*(3)           12(*3) 
         /     \            /      \
    12(*)(3)  1(2*)(3)  1(2)(*3)   ""
      /    \      \       /
1(2)(*)(3) ""     ""     ""   
    /
   ""
```

(2) There are many nodes visited multiple times, like ```12``` and ```1```. Most importantly, the subtree of those nodes are "exactly" the same. The backtracking solution possibly can be improved by DP. Try to merge the same nodes.
```      
        12*3
        /  \
      12*  |
       | \ |
       |  12
       | / |
       1   |
        \  |
          ""
```

(3) Successfully merge those repeated nodes and find out the **optimal substructure**, which is:
```
      current state = COMBINE(next state ,  next next state)
      e.g
              12* 
              / \    
            COMBINE (few different conditions)
            /     \       
           12      1
```
Therefore, we can solve this problem by DP in **bottom-up** way instead of top-down **memoization**.
    Now, we formulate the **optimal substructure**:
```
    dp[i] = COMBINE dp[i-1] and dp[i-2]
```
where **dp[i]  --> number of all possible decode ways of substring s(0 : i-1)**. Just keep it in mind.
But we need to notice there are some different conditions for this COMBINE operation.

(4) The pseudo code of solution would be:
```
Solution{
    /* initial conditions */
    dp[0] = ??
       :

    /* bottom up method */
    foreach( i ){
        dp[i] = COMBINE dp[i-1] and dp[i-2] ;
    }

    /* Return */
    return dp[last];
}
```

The COMBINE part will be something like:
```
    // for dp[i-1]
    if(condition A)
        dp[i] +=??  dp[i-1];
    else if(condition B)
        dp[i] +=??  dp[i-1];
            :
            :

     // for dp[i-2]
    if(condition C)
        dp[i] +=?? dp[i-2];
    else if(condition D)   
        dp[i] +=?? dp[i-2];
             :
```

(5) The core step of this solution is to find out all possible conditions and their corresponding operation of combination.
```
        For dp[i-1]:

                  /           \
                 /             \
            s[i-1]='*'    s[i-1]>0     
                |               |
          + 9*dp[i-1]        + dp[i-1]

             
        For dp[i-2]:

                   /                                  \
                  /                                    \  
              s[n-2]='1'||'2'                         s[n-2]='*'
              /            \                       /             \     
        s[n-1]='*'         s[n-1]!='*'          s[n-1]='*'       s[n-1]!='*'
         /       \               |                  |              /         \
  s[n-2]='1'  s[n-2]='2'   num(i-2,i-1)<=26         |         s[n-1]<=6    s[n-1]>6
      |            |             |                  |              |            |
 + 9*dp[i-2]   + 6*dp[i-2]     + dp[i-2]       + 15*dp[i-2]    + 2*dp[i-2]   + dp[i-2]
```

(6) Fill up the initial conditions before i = 2. 
Because we need to check if num(i-2,i-1)<=26 for each i, we make the bottom-up process to begin from i=2. Just fill up the dp[0] and dp[1] by observation and by the definition of **dp[i]  --> number of all possible decode ways of substring s(0 : i-1)**.

             dp[0]=1;
             /      \
        s[0]=='*'  s[0]!='*'
            |         |
        dp[1]=9     dp[1]=1

(7) The final Solution:
```
    public int numDecodings(String s) {
        /* initial conditions */
        long[] dp = new long[s.length()+1];
        dp[0] = 1;
        if(s.charAt(0) == '0'){
            return 0;
        }
        dp[1] = (s.charAt(0) == '*') ? 9 : 1;

        /* bottom up method */
        for(int i = 2; i <= s.length(); i++){
            char first = s.charAt(i-2);
            char second = s.charAt(i-1);

            // For dp[i-1]
            if(second == '*'){
                dp[i] += 9*dp[i-1];
            }else if(second > '0'){
                dp[i] += dp[i-1];
            }
            
            // For dp[i-2]
            if(first == '*'){
                if(second == '*'){
                    dp[i] += 15*dp[i-2];
                }else if(second <= '6'){
                    dp[i] += 2*dp[i-2];
                }else{
                    dp[i] += dp[i-2];
                }
            }else if(first == '1' || first == '2'){
                if(second == '*'){
                    if(first == '1'){
                       dp[i] += 9*dp[i-2]; 
                    }else{ // first == '2'
                       dp[i] += 6*dp[i-2]; 
                    }
                }else if( ((first-'0')*10 + (second-'0')) <= 26 ){
                    dp[i] += dp[i-2];    
                }
            }

            dp[i] %= 1000000007;
        }
        /* Return */
        return (int)dp[s.length()];
    }
```


P.S The space complexity can be further improved to O(1) because the current state i is only related to i-1 and i-2 during the bottom-up.
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Jul 09 2017 16:02:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 19 2018 03:20:51 GMT+0800 (Singapore Standard Time)

<p>
```.
class Solution(object):
    def numDecodings(self, S):
        MOD = 10**9 + 7
        e0, e1, e2 = 1, 0, 0
        for c in S:
            if c == \'*\':
                f0 = 9*e0 + 9*e1 + 6*e2
                f1 = e0
                f2 = e0
            else:
                f0 = (c > \'0\') * e0 + e1 + (c < = \'6\') * e2
                f1 = (c == \'1\') * e0
                f2 = (c == \'2\') * e0
            e0, e1, e2 = f0 % MOD, f1, f2
        return e0
```

Let\'s keep track of:
* ```e0 =``` current number of ways we could decode, ending on any number;
* ```e1 =``` current number of ways we could decode, ending on an open 1;
* ```e2 =``` current number of ways we could decode, ending on an open 2;

(Here, an "open 1" means a 1 that may later be used as the first digit of a 2 digit number, because it has not been used in a previous 2 digit number.)

With the right idea of what to keep track of, our dp proceeds straightforwardly.

<hr>

Say we see some character ```c```.  We want to calculate ```f0, f1, f2```, the corresponding versions of ```e0, e1, e2``` after parsing character ```c```.

If ```c == \'*\'```, then the number of ways to finish in total is: we could put * as a single digit number (```9*e0```), or we could pair * as a 2 digit number ```1*``` in ```9*e1``` ways, or we could pair * as a 2 digit number ```2*``` in ```6*e2``` ways.  The number of ways to finish with an open 1 (or 2) is just ```e0```.

If ```c != \'*\'```, then the number of ways to finish in total is: we could put ```c``` as a single digit if it is not zero (```(c>\'0\')*e0```), or we could pair ```c``` with our open 1, or we could pair ```c``` with our open 2 if it is 6 or less (```(c<=\'6\')*e2```).  The number of ways to finish with an open 1 (or 2) is ```e0``` iff ```c == \'1\'``` (or ```c == \'2\'```).
</p>


### Java DP, O(n) time and O(1) space
- Author: li-_-il
- Creation Date: Sun Jul 09 2017 11:42:40 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 04:45:52 GMT+0800 (Singapore Standard Time)

<p>
The idea for DP is simple when using two helper functions
 ways(i) -> that gives the number of ways of decoding a single character
and 
ways(i, j) -> that gives the number of ways of decoding the two character string formed by i and j.
The actual recursion then boils down to :
```
f(i) = (ways(i) * f(i+1)) + (ways(i, i+1) * f(i+2))
```
The solution to a string f(i), where i represents the starting index, 

f(i)  =  no.of ways to decode the character at i, which is ways(i) + solve for remainder of the string using recursion f(i+1) 
and 
no.of ways to decode the characters at i and i+1, which is ways(i, i+1) + solve for remainder of the string using recursion f(i+2) 

The base case is , 
```
return ways(s.charAt(i)) if(i == j) 
```

The above recursion when implemented with a cache, is a viable DP solution, but it leads to stack overflow error, due to the depth of the recursion. So its better to convert to memoized version.

For the memoized version, the equation changes to 

```
f(i) = ( f(i-1) * ways(i) ) + ( f(i-2) *ways(i-1, i) )
```
This is exactly the same as the previous recursive version in reverse,

The solution to a string f(i), where i represents the ending index of the string, 

f(i)  =  solution to the prefix of the string f(i-1) + no.of ways to decode the character at i, which is ways(i) 
and 
solution to the prefix f(i-2) + no.of ways to decode the characters at i - 1 and i, which is ways(i-1, i) 
```
public class Solution {
    public static int numDecodings(String s) {
        long[] res = new long[2];
        res[0] = ways(s.charAt(0));
        if(s.length() < 2) return (int)res[0];

        res[1] = res[0] * ways(s.charAt(1)) + ways(s.charAt(0), s.charAt(1));
        for(int j = 2; j < s.length(); j++) {
            long temp = res[1];
            res[1] = (res[1] * ways(s.charAt(j)) + res[0] * ways(s.charAt(j-1), s.charAt(j))) % 1000000007;
            res[0] = temp;
        }
        return  (int)res[1];
    }
    
    private static int ways(int ch) {
        if(ch == '*') return 9;
        if(ch == '0') return 0;
        return 1;
    }

    private static int ways(char ch1, char ch2) {
        String str = "" + ch1 + "" + ch2;
        if(ch1 != '*' && ch2 != '*') {
            if(Integer.parseInt(str) >= 10 && Integer.parseInt(str) <= 26)
                return 1;
        } else if(ch1 == '*' && ch2 == '*') {
            return 15;
        } else if(ch1 == '*') {
            if(Integer.parseInt(""+ch2) >= 0 && Integer.parseInt(""+ch2) <= 6)
                return 2;
            else
                return 1;
        } else {
            if(Integer.parseInt(""+ch1) == 1 ) {
                return 9;
            } else if(Integer.parseInt(""+ch1) == 2 ) {
                return 6;
            } 
        }
        return 0;
    }
}
```
</p>


