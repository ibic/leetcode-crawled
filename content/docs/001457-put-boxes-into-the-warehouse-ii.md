---
title: "Put Boxes Into the Warehouse II"
weight: 1457
#id: "put-boxes-into-the-warehouse-ii"
---
## Description
<div class="description">
<p>You are given two arrays of positive integers, <code>boxes</code> and <code>warehouse</code>, representing the heights of some boxes of unit width and the heights of <code>n</code> rooms in a warehouse respectively. The warehouse&#39;s rooms are labeled from <code>0</code> to <code>n - 1</code> from left to right where <code>warehouse[i]</code> (0-indexed) is the height of the <code>i<sup>th</sup></code> room.</p>

<p>Boxes are put into the warehouse by the following rules:</p>

<ul>
	<li>Boxes cannot be stacked.</li>
	<li>You can rearrange the insertion order of the boxes.</li>
	<li>Boxes can be pushed into the warehouse from <strong>either side</strong> (left or right)</li>
	<li>If the height of some room in the warehouse is less than the height of a box, then that box and all other boxes behind it will be stopped before that room.</li>
</ul>

<p>Return <em>the maximum number of boxes you can put into the warehouse.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/30/22.png" style="width: 401px; height: 202px;" />
<pre>
<strong>Input:</strong> boxes = [1,2,2,3,4], warehouse = [3,4,1,2]
<strong>Output:</strong> 4
<strong>Explanation:
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/30/22-1.png" style="width: 240px; height: 202px;" />
</strong>We can store the boxes in the following order:
1- Put the yellow box in room 2 from either the left or right side.
2- Put the orange box in room 3 from the right side.
3- Put the green box in room 1 from the left side.
4- Put the red box in room 0 from the left side.
Notice that there are other valid ways to put 4 boxes such as swapping the red and green boxes or the red and orange boxes.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/30/22-2.png" style="width: 401px; height: 242px;" />
<pre>
<strong>Input:</strong> boxes = [3,5,5,2], warehouse = [2,1,3,4,5]
<strong>Output:</strong> 3
<strong>Explanation:
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/30/22-3.png" style="width: 280px; height: 242px;" />
</strong>It&#39;s not possible to put the two boxes of height 5 in the warehouse since there&#39;s only 1 room of height &gt;= 5.
Other valid solutions are to put the green box in room 2 or to put the orange box first in room 2 before putting the green and red boxes.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> boxes = [1,2,3], warehouse = [1,2,3,4]
<strong>Output:</strong> 3
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> boxes = [4,5,6], warehouse = [3,3,3,3,3]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == warehouse.length</code></li>
	<li><code>1 &lt;= boxes.length, warehouse.length &lt;= 10<sup>5</sup></code></li>
	<li><code>1 &lt;= boxes[i], warehouse[i] &lt;= 10<sup>9</sup></code></li>
</ul>
</div>

## Tags
- Greedy (greedy)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### JAVA Short O(NlgN+M) Time/ O(1) Space
- Author: REED_W
- Creation Date: Thu Sep 10 2020 13:42:07 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 10 2020 15:24:19 GMT+0800 (Singapore Standard Time)

<p>

 
    private int sln1(int[] bx, int[] wh){
        int p1 =0; int p2 = wh.length-1;
        Arrays.sort(bx);
        int res = 0;
        for(int i = bx.length-1;i>=0 && p1<=p2;i--){
            if(bx[i] <= wh[p1]){
                p1++;
                res++;
            }else if(bx[i] <= wh[p2]){
                p2--;
                res++;
            }
        }
        return res;
    }
</p>


### Python two pointer + stack
- Author: jy856
- Creation Date: Thu Sep 10 2020 07:16:22 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 10 2020 07:20:07 GMT+0800 (Singapore Standard Time)

<p>
The key is to observe that if we have warehouse [4,5,4], then we never get a chance to add a box to the middle one. From this we can also observe that the valid height in the middle should always be the maximum of left boundary and right boundary. This gives the algorithm 

- start from left and right and calculate the boundary height for each room in the warehouse
- start from the smallest box and add each one in until we cannot add anymore or we add all of them in.

code
```python
def maxBoxesInWarehouse(self, boxes: List[int], warehouse: List[int]) -> int:
        if not warehouse: return 0
        l,r = 0,len(warehouse)-1
        stack = []
        min_l,min_r = sys.maxsize,sys.maxsize
        while l<= r:
            min_l = min(min_l,warehouse[l])
            min_r = min(min_r,warehouse[r])
            if min_l >= min_r:
                stack.append(min_l)
                l+=1
            else:
                stack.append(min_r)
                r-=1

        asr = 0
        boxes.sort(reverse = True)
        while boxes:
            while stack and stack[-1] < boxes[-1]:
                stack.pop()
            if not stack: return asr
            stack.pop()
            boxes.pop()
            asr+=1
        return asr
```

</p>


### Java Clean solution
- Author: Aoi-silent
- Creation Date: Sat Sep 12 2020 08:52:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 12 2020 09:05:24 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int maxBoxesInWarehouse(int[] boxes, int[] warehouse) {
        int n = warehouse.length;
        int[] height = new int[n];
        int min = warehouse[0];
        for (int i = 0; i < warehouse.length; i++) {
            min = Math.min(min, warehouse[i]);
            height[i] = min;            
        }
        min = warehouse[n - 1];
        for (int i = n - 1; i >= 0; i--) {
            min = Math.min(min, warehouse[i]);
            height[i] = Math.max(height[i], min);
        }
        Arrays.sort(height);
        Arrays.sort(boxes);
        int result = 0;
        for (int i = 0, j = 0; i < n && j < boxes.length; i++) {
            if (boxes[j] <= height[i]) {
                j++;
                result++;
            }
        }
        return result;
    }
}
```

Solution 2: Greedy but kinda counter-intuitive
```
class Solution {
    public int maxBoxesInWarehouse(int[] boxes, int[] warehouse) {
        int n = warehouse.length;
        Arrays.sort(boxes);
        int left = 0;
        int right = n - 1;
        int result = 0;
        for (int i = boxes.length - 1; i >= 0 && left <= right; i--) {
            if (boxes[i] <= warehouse[left]) {
                left++;
                result++;
            } else if (boxes[i] <= warehouse[right]) {
                right--;
                result++;
            }
        }
        return result;
    }
}
```
</p>


