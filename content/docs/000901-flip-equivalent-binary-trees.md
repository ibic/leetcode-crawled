---
title: "Flip Equivalent Binary Trees"
weight: 901
#id: "flip-equivalent-binary-trees"
---
## Description
<div class="description">
<p>For a binary tree <strong>T</strong>, we can define a <strong>flip operation</strong> as follows: choose any node, and swap the left and right child subtrees.</p>

<p>A binary tree <strong>X</strong>&nbsp;is <em>flip equivalent</em> to a binary tree <strong>Y</strong> if and only if we can make <strong>X</strong> equal to <strong>Y</strong> after some number of flip operations.</p>

<p>Given the roots of two binary trees <code>root1</code> and <code>root2</code>, return <code>true</code> if the two trees are flip equivelent or <code>false</code> otherwise.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="Flipped Trees Diagram" src="https://assets.leetcode.com/uploads/2018/11/29/tree_ex.png" style="width: 500px; height: 220px;" />
<pre>
<strong>Input:</strong> root1 = [1,2,3,4,5,6,null,null,null,7,8], root2 = [1,3,2,null,6,4,5,null,null,null,null,8,7]
<strong>Output:</strong> true
<strong>Explanation: </strong>We flipped at nodes with values 1, 3, and 5.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root1 = [], root2 = []
<strong>Output:</strong> true
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root1 = [], root2 = [1]
<strong>Output:</strong> false
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root1 = [0,null,1], root2 = []
<strong>Output:</strong> false
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> root1 = [0,null,1], root2 = [0,1]
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in each tree is in the range <code>[0, 100]</code>.</li>
	<li>Each tree will have <strong>unique node values</strong> in the range <code>[0, 99]</code>.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Google - 9 (taggedByAdmin: true)
- eBay - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Recursion

**Intuition**

If `root1` and `root2` have the same root value, then we only need to check if their children are equal (up to ordering.)

**Algorithm**

There are 3 cases:

* If `root1` or `root2` is `null`, then they are equivalent if and only if they are both `null`.

* Else, if `root1` and `root2` have different values, they aren't equivalent.

* Else, let's check whether the children of `root1` are equivalent to the children of `root2`.  There are two different ways to pair these children.

<iframe src="https://leetcode.com/playground/4kxJaBr7/shared" frameBorder="0" width="100%" height="242" name="4kxJaBr7"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(min(N_1, N_2))$$, where $$N_1, N_2$$ are the lengths of `root1` and `root2`.

* Space Complexity:  $$O(min(H_1, H_2))$$, where $$H_1, H_2$$ are the heights of the trees of `root1` and `root2`.
<br />
<br />


---
#### Approach 2: Canonical Traversal

**Intuition**

Flip each node so that the left child is smaller than the right, and call this the *canonical representation*.  All equivalent trees have exactly one canonical representation.

**Algorithm**

We can use a depth-first search to compare the canonical representation of each tree.  If the traversals are the same, the representations are equal.

When traversing, we should be careful to encode both when we enter or leave a node.

<iframe src="https://leetcode.com/playground/4TCtKGvF/shared" frameBorder="0" width="100%" height="500" name="4TCtKGvF"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N_1 + N_2)$$, where $$N_1, N_2$$ are the lengths of `root1` and `root2`.  (In Python, this is $$\min(N_1, N_2)$$.)

* Space Complexity:  $$O(N_1 + N_2)$$.  (In Python, this is $$\min(H_1, H_2)$$, where $$H_1, H_2$$ are the heights of the trees of `root1` and `root2`.)
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] DFS 3 liners and BFS with explanation, time & space: O(n).
- Author: rock
- Creation Date: Sun Dec 02 2018 12:05:34 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 11 2020 12:55:22 GMT+0800 (Singapore Standard Time)

<p>
# Note:
Some complain the iterative java code and DFS python code are wrong. Thanks for your comment.

In fact, **the codes are correct. The 77th test case is not**: 
```
[1,3,3,4,5,6,null,null,null,7,8]
[1,3,3,null,6,4,5,null,null,null,null,8,7]
```
There are duplicate values in both trees. However, according to problem description: 
"...
Constraints:
Each value in each tree will be a **unique integer** in the range [0, 99]."
Duplicate values of 3\'s should not appear in either tree. Therefore, this is an invalid test case.

# End of Note.

----

**Recursive version: DFS**
1. If at least one of the two root nodes is `null`, are they equal? if yes, `true`; if no, `false`;
2. otherwise, neither node is `null`; if the values of the two nodes are: 
	2a) NOT equal, return `false`;
	2b) equal, compare their children recursively.

```java
    public boolean flipEquiv(TreeNode r1, TreeNode r2) {
        if (r1 == null || r2 == null) return r1 == r2;
        return r1.val == r2.val &&
               (flipEquiv(r1.left, r2.left) && flipEquiv(r1.right, r2.right) || 
               flipEquiv(r1.left, r2.right) && flipEquiv(r1.right, r2.left));
    }
```
```python
    def flipEquiv(self, r1: TreeNode, r2: TreeNode) -> bool:
        if not r1 or not r2: return r1 == r2 == None
        return r1.val == r2.val and (self.flipEquiv(r1.left, r2.left) and  self.flipEquiv(r1.right, r2.right) \
                                or self.flipEquiv(r1.left, r2.right) and self.flipEquiv(r1.right, r2.left))
```

----
**Iterative version: BFS**
```java
    public boolean flipEquiv(TreeNode root1, TreeNode root2) {
        Queue<TreeNode> q1 = new LinkedList<>(), q2 = new LinkedList<>();
        q1.offer(root1);
        q2.offer(root2);
        while (!q1.isEmpty() && !q2.isEmpty()) {
            TreeNode n1 = q1.poll(), n2 = q2.poll();
            if (n1 == null || n2 == null) {
                if (n1 != n2) return false;
                else continue;
            }
            if (n1.val != n2.val) {
                return false;
            }
            if (n1.left == n2.left || n1.left != null && n2.left != null && n1.left.val == n2.left.val) {
                q1.addAll(Arrays.asList(n1.left, n1.right));
            }else {
                q1.addAll(Arrays.asList(n1.right, n1.left));
            }
            q2.addAll(Arrays.asList(n2.left, n2.right));
        }
        return q1.isEmpty() && q2.isEmpty();
    }
```
```python
    def flipEquiv(self, r1: TreeNode, r2: TreeNode) -> bool:
        dq1, dq2 = map(collections.deque, ([r1], [r2]))
        while dq1 and dq2:
            node1, node2 = dq1.popleft(), dq2.popleft()
            if node1 == node2 == None: continue 
            elif not node1 or not node2 or node1.val != node2.val: return False

            if node1.left == node2.left == None or node1.left and node2.left and node1.left.val ==  node2.left.val:
                dq1.extend([node1.left, node1.right])
            else:
                dq1.extend([node1.right, node1.left])
            dq2.extend([node2.left, node2.right])
        return not dq1 and not dq2
```
**Iterative version: DFS**
```java
    public boolean flipEquiv(TreeNode root1, TreeNode root2) {
        Stack<TreeNode> stk1 = new Stack<>(), stk2 = new Stack<>();
        stk1.push(root1);
        stk2.push(root2);
        while (!stk1.isEmpty() && !stk2.isEmpty()) {
            TreeNode n1 = stk1.pop(), n2 = stk2.pop();
            if (n1 == null && n2 == null) {
                continue;
            }else if (n1 == null || n2 == null || n1.val != n2.val) {
                return false;
            }

            if (n1.left == n2.left || n1.left != null && n2.left != null && n1.left.val == n2.left.val) {
                stk1.addAll(Arrays.asList(n1.left, n1.right));
            }else {
                stk1.addAll(Arrays.asList(n1.right, n1.left));
            }
            stk2.addAll(Arrays.asList(n2.left, n2.right));
        }
        return stk1.isEmpty() && stk2.isEmpty();
    }
```

```python
    def flipEquiv(self, r1: TreeNode, r2: TreeNode) -> bool:
        stk1, stk2 = [r1], [r2]
        while stk1 and stk2:
            node1, node2 = stk1.pop(), stk2.pop()
            if node1 == node2 == None: continue 
            elif not node1 or not node2 or node1.val != node2.val: return False
            
            if node1.left == node2.left == None or node1.left and node2.left and node1.left.val == node2.left.val:
                stk1.extend([node1.left, node1.right])
            else:
                stk1.extend([node1.right, node1.left])
            stk2.extend([node2.left, node2.right])
        return not stk1 and not stk2
```

----
**Update:** 
For some time, I forgot the following constraint and changed the comlexity from O(n) to O(n ^ 2):
`Each value in each tree will be a unique integer in the range [0, 99]`

The follows are correct **only without the above condition**.
=============================
Complexity analysis corrected from `O(n)` to `O(n ^ 2)`.
**Analysis:**

In worst case, the recursion corresponds to a perfect quaternary (means `4`-nary) tree, which has `4 ^ d = N ^ 2` nodes, and we have to traverse all nodes. `d = logN` is the depth of the binary tree.

One worst case for input:
two perfect binary trees: root1 & root2. 
1. Root1\'s nodes are all `0`s; 
2. Root2\'s nodes are all `0`s, with the exception that left and right bottoms are both `1`s.

**Time & Space: O(n ^ 2).**
Thanks correction from **@heruslu @coder_coder @Everestsky007**

----
Q & A:

Q1:
why it\'s N^2 and how it corresponds to a 4-nary tree? why it was initially considered O(N)?
A1:
Yes, because the return statement has 4 recursive calls.

The problem states `Each value in each tree will be a unique integer in the range [0, 99]`, hence we have the following deduction:
```
ir1.left.val == r2.left.val and r1.left.val == r2.right.val, 

at most 1 of the 2 relations is true; otherwise r2.left.val == r2.right.val, this contradicts the above problem constraint.
```
 Therefore, at least 1 out of flipEquiv(r1.left, r2.left) and flipEquiv(r1.left, r2.right) will terminate; Similiarly,  at least 1 out of flipEquiv(r1.right, r2.right) andflipEquiv(r1.right, r2.leftt) will terminate. 

Obviously at most 2 out of the 4 recursive calls could go all the way down to the bottom.

That is why the time is O(N).

Without the aforementioned constraint, all of the 4 recursive calls could expand the 4-nary tree to the bottom and result time O(N ^ 2).

----

Q2: In iterative Python code, why there needs to be a comparison of `node1.left == node2.left`?
A2: It is a short form of `node1.left == node2.left == None`.
</p>


### Java Iterative BFS - easy to understand
- Author: ursseenu71
- Creation Date: Sun Jan 13 2019 23:14:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 13 2019 23:14:31 GMT+0800 (Singapore Standard Time)

<p>
```
public boolean flipEquivIterative(TreeNode root1, TreeNode root2) {

        Queue<TreeNode> queue = new LinkedList<>();

        queue.offer(root1);
        queue.offer(root2);
        while (!queue.isEmpty()) {
            TreeNode curr1 = queue.poll();
            TreeNode curr2 = queue.poll();

            if (curr1 == null && curr2 == null) {
                continue;
            }
            if (!equals(curr1, curr2)) {
                return false;
            }
            if (isEquals(curr1.left, curr2.left) && isEquals(curr1.right, curr2.right)) {
                queue.offer(curr1.left);
                queue.offer(curr2.left);
                queue.offer(curr1.right);
                queue.offer(curr2.right);
            } else if (isEquals(curr1.left, curr2.right) && isEquals(curr1.right, curr2.left)) {
                queue.offer(curr1.left);
                queue.offer(curr2.right);
                queue.offer(curr1.right);
                queue.offer(curr2.left);
            } else {
                return false;
            }
        }
        return true;
    }

    private boolean isEquals(TreeNode root1, TreeNode root2) {
        if (root1 == null && root2 == null) {
            return true;
        } else if (root1 != null && root2 != null && root1.val == root2.val) {
            return true;
        } else {
            return false;
        }
    }
```
</p>


### C++ Recursive Solution with Explanation
- Author: ElvisTheKing
- Creation Date: Sun Dec 09 2018 01:31:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 09 2018 01:31:14 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    bool flipEquiv(TreeNode* root1, TreeNode* root2) {
        // Two null trees are flip equivalent
        // A non-null and null tree are NOT flip equivalent
        // Two non-null trees with different root values are NOT flip equivalent
        // Two non-null trees are flip equivalent if
        //      The left subtree of tree1 is flip equivalent with the left subtree of tree2 and the right subtree of tree1 is   
        //      flipequivalent with the right subtree of tree2 (no flip case)
        //      OR
        //      The right subtree of tree1 is flip equivalent with the left subtree of tree2 and the left subtree of tree1 is
        //      flipequivalent with the right subtree of tree2 (flip case)
        if ( !root1 && !root2 ) return true;
        if ( !root1 && root2 || root1 &&!root2 || root1->val != root2->val ) return false;
        return flipEquiv( root1->left, root2->left ) && flipEquiv( root1->right, root2->right )
            || flipEquiv( root1->right, root2->left ) && flipEquiv( root1->left, root2->right );
    }
};
```
</p>


