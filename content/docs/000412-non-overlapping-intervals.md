---
title: "Non-overlapping Intervals"
weight: 412
#id: "non-overlapping-intervals"
---
## Description
<div class="description">
<p>Given a collection of intervals, find the minimum number of intervals you need to remove to make the rest of the intervals non-overlapping.</p>

<ol>
</ol>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> [[1,2],[2,3],[3,4],[1,3]]
<b>Output:</b> 1
<b>Explanation:</b> [1,3] can be removed and the rest of intervals are non-overlapping.
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> [[1,2],[1,2],[1,2]]
<b>Output:</b> 2
<b>Explanation:</b> You need to remove two [1,2] to make the rest of intervals non-overlapping.
</pre>

<p><b>Example 3:</b></p>

<pre>
<b>Input:</b> [[1,2],[2,3]]
<b>Output:</b> 0
<b>Explanation:</b> You don&#39;t need to remove any of the intervals since they&#39;re already non-overlapping.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>You may assume the interval&#39;s end point is always bigger than its start point.</li>
	<li>Intervals like [1,2] and [2,3] have borders &quot;touching&quot; but they don&#39;t overlap each other.</li>
</ol>

</div>

## Tags
- Greedy (greedy)

## Companies
- Facebook - 6 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary

Given a collection of intervals, we need to find the minimum number of intervals to be removed to make the rest of the intervals non-overlapping.

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

In the brute force approach, we try to remove the overlapping intervals in different combinations and then check which combination needs the minimum number of removals. To do this, firstly we sort the given intervals based on the starting point. Then, we make use of a recursive function `eraseOverlapIntervals` which takes the index of the previous interval $$prev$$ and the index of the current interval $$curr$$ (which we try to add to the list of intervals not removed), and returns the count of intervals that need to be removed from the current index onwards.

We start by using $$prev=-1$$ and $$curr=0$$. In each recursive call, we check if the current interval overlaps with the previous interval. If not, we need not remove the current interval from the final list and we can call the function `eraseOverlapIntervals` with $$prev=curr$$ and $$curr=curr + 1$$. The result of this function call in which we have included the current element is stored in $$taken$$ variable.

We also make another function call by removing the current interval because this could be overlapping with the upcoming intervals in the next function call and thus, its removal could eventually require lesser total number of removals. Thus, the recursive call takes the arguments $$prev=prev$$ and $$curr=curr + 1$$. Since, we have removed one interval, the result if the current interval isn't included is the sum of the value returned by the function call incremented by 1, which is stored in $$notTaken$$ variable. While returning the count of removals following a particular index, we return the minimum of $$taken$$ and $$notTaken$$.

<iframe src="https://leetcode.com/playground/ZPnCtLgG/shared" frameBorder="0" width="100%" height="429" name="ZPnCtLgG"></iframe>

**Complexity Analysis**

* Time complexity : $$O(2^n)$$. Total possible number of Combinations of subsets are $$2^n$$ .
* Space complexity : $$O(n)$$. Depth of recursion is $$n$$.

---
#### Approach #2 Using DP based on starting point [Accepted]

**Algorithm**

The given problem can be simplified to a great extent if we sort the given interval list based on the starting points. Once it's done, we can make use of a $$dp$$ array, scuh that $$dp[i]$$ stores the maximum number of valid intervals that can be included in the final list if the intervals upto the $$i^{th}$$ interval only are considered, including itself. Now, while finding $$dp[i+1]$$, we can't consider the value of $$dp[i]$$ only, because it could be possible that the $$i^{th}$$ or any previous interval could be overlapping with the $$(i+1)^{th}$$ interval. Thus, we need to consider the maximum of all $$dp[j]$$'s such that $$j \leq i$$ and $$j^{th}$$ interval and $$i^{th}$$ don't overlap, to evaluate $$dp[i+1]$$. Therefore, the equation for $$dp[i+1]$$ becomes:

$$
dp[i+1]= \max(dp[j]) + 1,
$$

such that $$j^{th}$$ interval and $$i^{th}$$ don't overlap, for all $$j \leq i$$.

In the end, to obtain the maximum number of intervals that can be included in the final list($$ans$$) we need to find the maximum value in the $$dp$$ array. The final result will be the total number of intervals given less the result just obtained($$intervals.length-ans$$).

The animation below illustrates the approach more clearly:

!?!../Documents/435_Non_Overlapping_Intervalsdp1.json:866,487!?!

<iframe src="https://leetcode.com/playground/8w5goLop/shared" frameBorder="0" width="100%" height="500" name="8w5goLop"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Two nested loops are required to fill $$dp$$ array.

* Space complexity : $$O(n)$$. $$dp$$ array of size $$n$$ is used.

---
#### Approach #3 Using DP based on the end points [Accepted]

**Algorithm**

In the DP approach just discussed above, for calculating the value of every $$dp[i]$$, we need to traverse the $$dp$$ array back till the starting index. This overhead can be removed, if we use an interval list sorted on the basis of the end points. Thus, now, again we use the same kind of $$dp$$ array, where $$dp[i]$$ is used to store the maximum number of intervals that can be included in the final list if the intervals only upto the $$i^{th}$$ index in the sorted list are considered. Thus, in order to find $$dp[i+1]$$ now, we've to consider two cases:

**Case 1:**

>The interval corresponding to $$(i+1)^{th}$$ interval needs to be included in the final list to obtain the minimum number of removals:

In this case, we need to traverse back in the sorted interval array form the $$(i+1)^{th}$$ index upto the starting index to find the first interval which is non-overlapping. This is because, if we are including the current interval, we need to remove all the intervals which are overlapping with the current interval. But, we need not go back till the starting index everytime. Instead we can stop the traversal as soon as we hit the first non-overlapping interval and use its $$dp[j] + 1$$ to fill in $$dp[i+1]$$, since $$dp[j]$$ will be the element storing the maximum number of intervals that can be included considering elements upto the $$j^{th}$$ index.

**Case 2:**

> The interval corresponding to $$(i+1)^{th}$$ interval needs to be removed from the final list to obtain the minimum number of removals:

In this case, the current element won't be included in the final list. So, the count of intervals to be included upto $$(i+1)^{th}$$ index is the same as the count of intervals upto the $$i^{th}$$ index. Thus, we can use $$dp[i]$$'s value to fill in $$dp[i+1]$$.



The value finally entered in $$dp[i+1]$$ will be the maximum of the above two values.

The final result will again be the total count of intervals less the result obtained at the end from the $$dp$$ array.

The animation below illustrates the approach more clearly:

!?!../Documents/435_Non.json:1000,563!?!

<iframe src="https://leetcode.com/playground/fcPp6mkW/shared" frameBorder="0" width="100%" height="500" name="fcPp6mkW"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Two nested loops are required to fill $$dp$$ array.

* Space complexity : $$O(n)$$. $$dp$$ array of size $$n$$ is used.

---

#### Approach #4 Using Greedy Approach based on starting points [Accepted]

**Algorithm**

If we sort the given intervals based on starting points, the greedy approach works very well. While considering the intervals in the ascending order of starting points, we make use of a pointer $$prev$$ pointer to keep track of the interval just included in the final list. While traversing, we can encounter 3 possibilities as shown in the figure:

![Non_overlapping](../Figures/435_NonOverlapping_greedy1.JPG)


**Case 1:**

> The two intervals currently considered are non-overlapping:

In this case, we need not remove any interval and we can continue by simply assigning the $$prev$$ pointer to the later interval and the count of intervals removed remains unchanged.

**Case 2:**

> The two intervals currently considered are overlapping and the end point of the later interval falls before the end point of the previous interval:

In this case, we can simply take the later interval. The choice is obvious since choosing an interval of smaller width will lead to more available space labelled as $$A$$ and $$B$$, in which more intervals can be accommodated. Hence, the $$prev$$ pointer is updated to current interval and the count of intervals removed is incremented by 1.

**Case 3:**

> The two intervals currently considered are overlapping and the end point of the later interval falls after the end point of the previous interval:

In this case, we can work in a greedy manner and directly remove the later interval. To understand why this greedy approach works, we need to see the figure below, which includes all the subcases possible. It is clear from the figures that we choosing interval 1 always leads to a better solution in the future. Thus, the $$prev$$ pointer remains unchanged and the count of intervals removed is incremented by 1.

![Non_overlapping](../Figures/435_NonOverlapping_greedy2.JPG)

<iframe src="https://leetcode.com/playground/KabnVE3C/shared" frameBorder="0" width="100%" height="497" name="KabnVE3C"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(n \log(n)\big)$$. Sorting takes $$O\big(n \log(n)\big)$$ time.

* Space complexity : $$O(1)$$. No extra space is used.

---

#### Approach #5 Using Greedy Approach based on end points [Accepted]

**Algorithm**

The Greedy approach just discussed was based on choosing intervals greedily based on the starting points. But in this approach, we go for choosing points greedily based on the end points. For this, firstly we sort the given intervals based on the end points. Then, we traverse over the sorted intervals. While traversing, if there is no overlapping between the previous interval and the current interval, we need not remove any interval. But, if an overlap exists between the previous interval and the current interval, we always drop the current interval.

To explain how it works, again we consider every possible arrangement of the intervals.

![Non_overlapping](../Figures/435_NonOverlapping_greedy3.JPG)


**Case 1:**

> The two intervals currently considered are non-overlapping:

In this case, we need not remove any interval and for the next iteration the current interval becomes the previous interval.

**Case 2:**

> The two intervals currently considered are overlapping and the starting point of the later interval falls before the starting point of the previous interval:

In this case, as shown in the figure below, it is obvious that the later interval completely subsumes the previous interval. Hence, it is advantageous to remove the later interval so that we can get more range available to accommodate future intervals. Thus, previous interval remains unchanged and the current interval is updated.

**Case 3:**

> The two intervals currently considered are overlapping and the starting point of the later interval falls before the starting point of the previous interval:

In this case, the only opposition to remove the current interval arises because it seems that more intervals could be accommodated by removing the previous interval in the range marked by $$A$$. But that won't be possible as can be visualized with a case similar to Case 3a and 3b shown above. But, if we remove the current interval, we can save the range $$B$$ to accommodate further intervals. Thus, previous interval remains unchanged and the current interval is updated.

<iframe src="https://leetcode.com/playground/qratMPMS/shared" frameBorder="0" width="100%" height="446" name="qratMPMS"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(n \log(n)\big)$$. Sorting takes $$O\big(n \log(n)\big)$$ time.

* Space complexity : $$O(1)$$. No extra space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java: Least is Most
- Author: crickey180
- Creation Date: Mon Oct 31 2016 11:41:27 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 14:50:32 GMT+0800 (Singapore Standard Time)

<p>
Actually, the problem is the same as "Given a collection of intervals, find the maximum number of intervals that are non-overlapping." (the classic Greedy problem: [Interval Scheduling](https://en.wikipedia.org/wiki/Interval_scheduling#Interval_Scheduling_Maximization)). With the solution to that problem, guess how do we get the minimum number of intervals to remove? : )

Sorting Interval.end in ascending order is O(nlogn), then traverse intervals array to get the maximum number of non-overlapping intervals is O(n). Total is O(nlogn).

```
    public int eraseOverlapIntervals(Interval[] intervals) {
        if (intervals.length == 0)  return 0;

        Arrays.sort(intervals, new myComparator());
        int end = intervals[0].end;
        int count = 1;        

        for (int i = 1; i < intervals.length; i++) {
            if (intervals[i].start >= end) {
                end = intervals[i].end;
                count++;
            }
        }
        return intervals.length - count;
    }
    
    class myComparator implements Comparator<Interval> {
        public int compare(Interval a, Interval b) {
            return a.end - b.end;
        }
    }
```
</p>


### Python Greedy -- Interval Scheduling
- Author: WangQiuc
- Creation Date: Tue Apr 16 2019 14:35:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 16 2020 00:44:36 GMT+0800 (Singapore Standard Time)

<p>
A classic greedy case: interval scheduling problem.

 The heuristic is: always pick the interval with the earliest end time. Then you can get the maximal number of non-overlapping intervals. (or minimal number to remove).
 This is because, the interval with the earliest end time produces the maximal capacity to hold rest intervals. 
 E.g. Suppose current earliest end time of the rest intervals is ```x```. Then available time slot left for other intervals is ```[x:]```. If we choose another interval with end time ```y```, then available time slot would be ```[y:]```. Since ```x \u2264 y```, there is no way ```[y:]``` can hold more intervals then ```[x:]```. Thus, the heuristic holds.
 
 Therefore, we can sort interval by ending time and key track of current earliest end time. Once next interval\'s start time is earlier than current end time, then we have to remove one interval. Otherwise, we update earliest end time.
```
def eraseOverlapIntervals(intervals):
	end, cnt = float(\'-inf\'), 0
	for s, e in sorted(intervals, key=lambda x: x[1]):
		if s >= end: 
			end = e
		else: 
			cnt += 1
	return cnt
```
Time complexity is O(NlogN) as sort overwhelms greedy search.
</p>


### Short Ruby and Python
- Author: StefanPochmann
- Creation Date: Mon Oct 31 2016 22:26:20 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 15:50:59 GMT+0800 (Singapore Standard Time)

<p>
Which interval would be the best **first** (leftmost) interval to keep? One that ends first, as it leaves the most room for the rest. So take one with smallest `end`, remove all the bad ones overlapping it, and repeat (taking the one with smallest `end` of the remaining ones). For the overlap test, just keep track of the current end, initialized with negative infinity.

## Ruby
Take out intervals as described above, so what's left is the bad overlapping ones, so just return their number.
```
def erase_overlap_intervals(intervals)
  end_ = -1.0 / 0
  intervals.sort_by(&:end).reject { |i|
    end_ = i.end if i.start >= end_
  }.size
end
```
Alternatively, `i.start >= end_ and end_ = i.end` works, too.

## Python

    def eraseOverlapIntervals(self, intervals):
        end = float('-inf')
        erased = 0
        for i in sorted(intervals, key=lambda i: i.end):
            if i.start >= end:
                end = i.end
            else:
                erased += 1
        return erased
</p>


