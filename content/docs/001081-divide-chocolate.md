---
title: "Divide Chocolate"
weight: 1081
#id: "divide-chocolate"
---
## Description
<div class="description">
<p>You have one chocolate bar that consists of some chunks. Each chunk has its own sweetness given by the array&nbsp;<code>sweetness</code>.</p>

<p>You want to share the chocolate with your <code>K</code>&nbsp;friends so you start cutting the chocolate bar into <code>K+1</code>&nbsp;pieces using&nbsp;<code>K</code>&nbsp;cuts, each piece consists of some <strong>consecutive</strong> chunks.</p>

<p>Being generous, you will eat the piece with the <strong>minimum total sweetness</strong> and give the other pieces to your friends.</p>

<p>Find the <strong>maximum total sweetness</strong> of the&nbsp;piece you can get by cutting the chocolate bar optimally.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> sweetness = [1,2,3,4,5,6,7,8,9], K = 5
<strong>Output:</strong> 6
<b>Explanation: </b>You can divide the chocolate to [1,2,3], [4,5], [6], [7], [8], [9]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> sweetness = [5,6,7,8,9,1,2,3,4], K = 8
<strong>Output:</strong> 1
<b>Explanation: </b>There is only one way to cut the bar into 9 pieces.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> sweetness = [1,2,2,1,2,2,1,2,2], K = 2
<strong>Output:</strong> 5
<b>Explanation: </b>You can divide the chocolate to [1,2,2], [1,2,2], [1,2,2]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= K &lt;&nbsp;sweetness.length &lt;= 10^4</code></li>
	<li><code>1 &lt;= sweetness[i] &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Binary Search (binary-search)
- Greedy (greedy)

## Companies
- Google - 5 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Binary Search
- Author: lee215
- Creation Date: Sun Oct 20 2019 00:08:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 01:05:29 GMT+0800 (Singapore Standard Time)

<p>
Please reply and upvote now.
Don\'t have prime membership.
Cannot even read and modify my own post later, when it\'s locked.
<br>

## **Explanation**
We want to maximize the minimum sweetness.
Binary search the result between 1 and 10^9.

Don\'e explain binary search too much again and again.
Please find more related explanation in **More**.
Also will explain it more in details on youtube lee215.

Time `O(Nlog(10^9))`
Space `O(1)`
<br>

**Java**
```java
    public int maximizeSweetness(int[] A, int K) {
        int left = 1, right = (int)1e9 / (K + 1);
        while (left < right) {
            int mid = (left + right + 1) / 2;
            int cur = 0, cuts = 0;
            for (int a : A) {
                if ((cur += a) >= mid) {
                    cur = 0;
                    if (++cuts > K) break;
                }
            }
            if (cuts > K)
                left = mid;
            else
                right = mid - 1;
        }
        return left;
    }
```
**C++**
```cpp
    int maximizeSweetness(vector<int>& A, int K) {
        int left = 1, right = 1e9 / (K + 1);
        while (left < right) {
            int mid = (left + right + 1) / 2;
            int cur = 0, cuts = 0;
            for (int a : A) {
                if ((cur += a) >= mid) {
                    cur = 0;
                    if (++cuts > K) break;
                }
            }
            if (cuts > K)
                left = mid;
            else
                right = mid - 1;
        }
        return left;
    }
```
**Python:**
```python
    def maximizeSweetness(self, A, K):
        left, right = 1, sum(A) / (K + 1)
        while left < right:
            mid = (left + right + 1) / 2
            cur = cuts = 0
            for a in A:
                cur += a
                if cur >= mid:
                    cuts += 1
                    cur = 0
            if cuts > K:
                left = mid
            else:
                right = mid - 1
        return right
```
<br>

# More Good Binary Search Problems
Here are some similar binary search problems.
Also find more explanations.
Good luck and have fun.

- 1231. [1231. Divide Chocolate](https://leetcode.com/problems/divide-chocolate/discuss/408503/Python-Binary-Search)
- 1011. [Capacity To Ship Packages In N Days](https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/discuss/256729/javacpython-binary-search/351188?page=3)
- 875. [Koko Eating Bananas](https://leetcode.com/problems/koko-eating-bananas/discuss/152324/C++JavaPython-Binary-Search)
- 774. [Minimize Max Distance to Gas Station](https://leetcode.com/problems/minimize-max-distance-to-gas-station/discuss/113633/Easy-and-Concise-Solution-using-Binary-Search-C++JavaPython)
- 410. [Split Array Largest Sum](https://leetcode.com/problems/split-array-largest-sum/)
<br>
</p>


### Python binary search with inline explanation
- Author: dcvan24
- Creation Date: Wed Oct 23 2019 19:27:00 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 23 2019 19:27:00 GMT+0800 (Singapore Standard Time)

<p>
```python
class Solution:
    def maximizeSweetness(self, sweetness: List[int], K: int) -> int:
        """
        The problem asks us to divide the chocolate in a clever way in order to 
        maximize the sweetness we can get, since our friends are greedy and 
        always take the sweetest chunks. In essence, it is asking for the 
        maximum of the minimum sum of continuous subarrays, since we always get 
        the least sweet piece, which have the least sweet chunks (minimum sum of 
        continuous subarray) as compared to our friend\'s, and we want to find 
        the maximum we can get among all the dividing strategies. 
        """

        def divide(target):
            """
            The function divides the given chocolate into a number of pieces, so 
            that there must be at least one piece with a total sweetness less 
            than or equal to the given target
            """
            # total sweetness of current piece and the number of pieces
            sum_, count = 0, 0
            for l in sweetness:
                # add the sweet level to the total
                sum_ += l
                # if the current piece has a total greater than or equal to the 
                # given target, make a cut
                if sum_ >= target:
                    count += 1
                    sum_ = 0
            return count
        
        # the desired total sweetness must fall in the range from the minimum 
        # sweetness to the average sweetness of the (K + 1) pieces - if we are 
        # dumb enough to cut the least sweet chunk as a piece by itself, its 
        # total sweetness is the sweet level of that single chunk; if we are 
        # lucky enough that the sweetness is evenly distributed throughout the 
        # chocolate bar, we can make even cuts so we have the same amount of 
        # sweetness with our greedy friends. The real life is somewhere in 
        # between, and we always hope for the best, so we move towards the 
        # lucky side as much as we can
        lo, hi = min(sweetness), sum(sweetness)//(K + 1)
        while lo < hi:
            # try a number in the middle tentatively
            target = (lo + hi + 1)//2
            # if it results in less than K + 1 chunks, some of our friends 
            # cannot get a piece and may get mad, so we decrease the sweetness 
            # we get to make our friends happy
            if divide(target) < K + 1:
                hi = target - 1
            # otherwise, we try to increase the sweetness (sneakily) as much as 
            # we can right before being exposed...
            else:
                lo = target
        # this is the most we can get before being exposed...
        return lo
```
</p>


### Similar to problem no : 1101 & 410
- Author: Poorvank
- Creation Date: Sun Oct 20 2019 00:01:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 00:21:16 GMT+0800 (Singapore Standard Time)

<p>
Check : 
https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/description/
https://leetcode.com/problems/split-array-largest-sum/description/

```
class Solution {
    public int maximizeSweetness(int[] sweetness, int K) {
         K=K+1; // Include yourself.
        int lo = getMin(sweetness);
        int hi = getSum(sweetness);
        while (lo < hi) {
            int mid = (lo + hi + 1) >>> 1;
            if (split(sweetness, mid) < K) {
                hi = mid - 1;
            } else {
                lo = mid;
            }
        }
        return lo;
    }

    private int split(int[] arr, int minSweetness) {
        int peopleCount = 0;
        int sweetness = 0;
        for (int val : arr) {
            sweetness += val;
            if (sweetness >= minSweetness) {
                peopleCount++;
                sweetness = 0;
            }
        }
        return peopleCount;
    }

    private int getMin(int[] arr) {
        int min = Integer.MAX_VALUE;
        for (int val : arr) {
            min = Math.min(min, val);
        }
        return min;
    }

    private int getSum(int[] arr) {
        int sum = 0;
        for (int val : arr) {
            sum += val;
        }
        return sum;
    }

}
</p>


