---
title: "Maximum of Absolute Value Expression"
weight: 1107
#id: "maximum-of-absolute-value-expression"
---
## Description
<div class="description">
<p>Given two arrays of integers with equal lengths, return the maximum value of:</p>

<p><code>|arr1[i] - arr1[j]| + |arr2[i] - arr2[j]| + |i - j|</code></p>

<p>where the maximum is taken over all <code>0 &lt;= i, j &lt; arr1.length</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr1 = [1,2,3,4], arr2 = [-1,4,5,6]
<strong>Output:</strong> 13
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr1 = [1,-2,-5,0,10], arr2 = [0,-2,-1,-7,-4]
<strong>Output:</strong> 20
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= arr1.length == arr2.length &lt;= 40000</code></li>
	<li><code>-10^6 &lt;= arr1[i], arr2[i] &lt;= 10^6</code></li>
</ul>

</div>

## Tags
- Math (math)
- Bit Manipulation (bit-manipulation)

## Companies
- Google - 3 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### c++, beats 100% (both time and memory), with algorithm and image
- Author: goelrishabh5
- Creation Date: Sun Jul 21 2019 13:08:24 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 23 2019 20:05:29 GMT+0800 (Singapore Standard Time)

<p>
|arr1[i] - arr1[j]| + |arr2[i] - arr2[j]| + |i - j| can be written as :

![image](https://assets.leetcode.com/users/goelrishabh5/image_1563686004.png)


If we look carefully, 1 & 8 ; 2 & 7; 3 & 6; 4 & 5 are practically the same.
So , problem reduces to finding max of (1,2,3,4).
And in each 1,2,3,4, values in both brackets are same, so we simply find max(value in bracket) - min(value in bracket) for each. 
Then find max of values obtained from (1,2,3,4)

```
int fun(vector<int>& arr, int n) { 
        int max_sum = arr[0], min_sum = arr[0];  
        for (int i = 0; i < n; i++) { // Finding max and min sum value 
            max_sum=max(max_sum,arr[i]);
            min_sum=min(min_sum,arr[i]);
        } 
        return (max_sum-min_sum); 
    } 
    
    int maxAbsValExpr(vector<int>& arr1, vector<int>& arr2) {
        int n = arr1.size();
        vector<int> sum1(n,0),diff1(n,0),sum2(n,0),diff2(n,0);
        for(int i=0;i<n;i++) {
            sum1[i]=arr1[i]+arr2[i]+i;
            diff1[i]=arr1[i]-arr2[i]+i;
            sum2[i]=arr1[i]+arr2[i]-i;
            diff2[i]=arr1[i]-arr2[i]-i;
        }
        return max(max(fun(sum1,n),fun(diff1,n)),max(fun(sum2,n),fun(diff2,n)));        
    }
```
</p>


### [Java/C++/Python] Maximum Manhattan Distance
- Author: lee215
- Creation Date: Sun Jul 21 2019 12:02:38 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 07 2020 14:50:31 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Take `|x[i] - x[j]| + |y[i] - y[j]|` as Manhattan distance of two points.
`x` is the coordinate of points on the x-axis,
`y` is the coordinate of points on the y-axis.
<br>

# **Explanation 1: Math**
Assume `i < j`, there are four possible expression:
`|x[i] - x[j]| + |y[i] - y[j]| = (x[i] - x[j]|) + (y[i] - y[j]) = (x[i] + y[i]|) - (x[j] + y[j])`
`|x[i] - x[j]| + |y[i] - y[j]| = (x[i] - x[j]|) - (y[i] - y[j]) = (x[i] - y[i]|) - (x[j] - y[j])`
`|x[i] - x[j]| + |y[i] - y[j]| = -(x[i] - x[j]|) + (y[i] - y[j]) = (-x[i] + y[i]|) - (-x[j] + y[j])`
`|x[i] - x[j]| + |y[i] - y[j]| = -(x[i] - x[j]|) - (y[i] - y[j]) = (-x[i] - y[i]|) - (-x[j] - y[j])`

So we can see, the expression
`|x[i] - x[j]| + |y[i] - y[j]| + |i - j| = f(j) - f(i)`

where `f(i) = p * x[i] + q * y[i] + i`
with `p = 1 or -1, q = 1 or -1`
<br>


# **Explanation 2: Graph**
For 3 points on the plane, we always have `|AO| - |BO| <= |AB|`.
When `AO` and `BO` are in the same direction, we have `||AO| - |BO|| = |AB|`.

We take 4 points for point `O`, left-top, left-bottom, right-top and right-bottom.
Each time, for each point `B`, and find the `smallest` `A` point to `O`,
the Manhattan distance `|AB| >= |AO| - |BO|`.
<br>

# **Complexity**
Time `O(N)` for 4 passes
Space `O(1)`
<br>

**Java:**
```java
    public int maxAbsValExpr(int[] x, int[] y) {
        int res = 0, n = x.length, P[] = {-1,1};
        for (int p : P) {
            for (int q : P) {
                int smallest = p * x[0] + q * y[0] + 0;
                for (int i = 1; i < n; ++i) {
                    int cur = p * x[i] + q * y[i] + i;
                    res = Math.max(res, cur - smallest);
                    smallest = Math.min(smallest, cur);
                }
            }
        }
        return res;
    }
```
**C++:**
```cpp
    int maxAbsValExpr(vector<int>& x, vector<int>& y) {
        int res = 0, n = x.size(), smallest, cur;
        for (int p : {1, -1}) {
            for (int q : {1, -1}) {
                smallest = p * x[0] + q * y[0] + 0;
                for (int i = 1; i < n; ++i) {
                    cur = p * x[i] + q * y[i] + i;
                    res = max(res, cur - smallest);
                    smallest = min(smallest, cur);
                }
            }
        }
        return res;
    }
```
**Python:**
```python
    def maxAbsValExpr(self, x, y):
        res, n = 0, len(x)
        for p, q in [[1, 1], [1, -1], [-1, 1], [-1, -1]]:
            smallest = p * x[0] + q * y[0] + 0
            for i in xrange(n):
                cur = p * x[i] + q * y[i] + i
                res = max(res, cur - smallest)
                smallest = min(smallest, cur)
        return res
```

</p>


### [Python] Manhattan/Chebyshev Distance
- Author: karutz
- Creation Date: Tue Jul 30 2019 16:17:23 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 30 2019 16:17:23 GMT+0800 (Singapore Standard Time)

<p>
Consider the input as a set of points in 3-dimensions: `[(x[0], y[0], 0), (x[1], y[1], 1), ...]`

The problem is to find the maximum [Manhattan distance](https://en.wikipedia.org/wiki/Manhattan_distance) between any pair of points.

Naively, we could do the following:
```python
class Solution:
    def maxAbsValExpr(self, xs, ys):
        return max(
            abs(x1 - x2) + abs(y1 - y2) + abs(i - j)
            for i, (x1, y1) in enumerate(zip(xs, ys))
            for j, (x2, y2) in enumerate(zip(xs, ys))
        )
```

However, this takes `O(n^2)` time, which is too slow.

We can use a trick to transform manhattan distance in `d` dimensions to [Chebyshev distance](https://en.wikipedia.org/wiki/Chebyshev_distance) in `2^(d-1)` dimensions: https://codeforces.com/blog/entry/57534

eg. In 2D, we replace each point, `(x, y)`, with `(x+y, x-y)`:
```text
Manhattan((x1, y1), (x2, y2))
    = |x1 - x2| + |y1 - y2|
    = max(
        |(x1 - x2) + (y1 - y2)|,
        |(x1 - x2) - (y1 - y2)|
    )
    = max(
        |(x1 + y1) - (x2 + y2)|,
        |(x1 - y1) - (x2 - y2)|
    )
    = Chebyshev((x1+y1, x1-y1), (x2+y2, x2-y2))
```

In 3D, we replace each point, `(x, y, z)`, with `(x+y+z, x+y-z, x-y+z, x-y-z)`:
```text
|x1 - x2| + |y1 - y2| + |z1 - z2| 
    = max(
        |(x1 - x2) + (y1 - y2) + (z1 - z2)|,
	    |(x1 - x2) + (y1 - y2) - (z1 - z2)|,
	    |(x1 - x2) - (y1 - y2) + (z1 - z2)|,
	    |(x1 - x2) - (y1 - y2) - (z1 - z2)|
    )
    = max(
        |(x1 + y1 + z1) - (x2 + y2 + z2)|,
	    |(x1 + y1 - z1) - (x2 + y2 - z2)|,
	    |(x1 - y1 + z1) - (x2 - y2 + z2)|,
	    |(x1 - y1 - z1) - (x2 - y2 - z2)|
    )
```

The max Chebyshev distance can be conveniently found in `O(n)` time:
```python
class Solution:
    def maxAbsValExpr(self, xs, ys):
        points = []
        for i, (x, y) in enumerate(zip(xs, ys)):
            points.append((
                x + y + i, 
                x + y - i,
                x - y + i,
                x - y - i
            ))
        return max(max(dim) - min(dim) for dim in zip(*points))
```
</p>


