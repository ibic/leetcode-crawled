---
title: "Find And Replace in String"
weight: 776
#id: "find-and-replace-in-string"
---
## Description
<div class="description">
<p>To some string <code>S</code>, we will perform some&nbsp;replacement&nbsp;operations that replace groups of letters with new ones (not necessarily the same size).</p>

<p>Each replacement operation has <code>3</code> parameters: a starting index <code>i</code>, a source word&nbsp;<code>x</code>&nbsp;and a target word&nbsp;<code>y</code>.&nbsp; The rule is that if <code><font face="monospace">x</font></code>&nbsp;starts at position <code>i</code>&nbsp;in the <strong>original</strong> <strong>string</strong> <strong><code>S</code></strong>, then we will replace that occurrence of&nbsp;<code>x</code>&nbsp;with&nbsp;<code>y</code>.&nbsp; If not, we do nothing.</p>

<p>For example, if we have&nbsp;<code>S = &quot;abcd&quot;</code>&nbsp;and we have some replacement operation&nbsp;<code>i = 2, x = &quot;cd&quot;, y = &quot;ffff&quot;</code>, then because&nbsp;<code>&quot;cd&quot;</code>&nbsp;starts at position <code><font face="monospace">2</font></code>&nbsp;in the original string <code>S</code>, we will replace it with <code>&quot;ffff&quot;</code>.</p>

<p>Using another example on <code>S = &quot;abcd&quot;</code>, if we have both the replacement operation <code>i = 0, x = &quot;ab&quot;, y = &quot;eee&quot;</code>, as well as another replacement operation&nbsp;<code>i = 2, x = &quot;ec&quot;, y = &quot;ffff&quot;</code>, this second operation does nothing because in the original string&nbsp;<code>S[2] = &#39;c&#39;</code>, which doesn&#39;t match&nbsp;<code>x[0] = &#39;e&#39;</code>.</p>

<p>All these operations occur simultaneously.&nbsp; It&#39;s guaranteed that there won&#39;t be any overlap in replacement: for example,&nbsp;<code>S = &quot;abc&quot;, indexes = [0, 1],&nbsp;sources = [&quot;ab&quot;,&quot;bc&quot;]</code> is not a valid test case.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>S = &quot;abcd&quot;, indexes = [0,2], sources = [&quot;a&quot;,&quot;cd&quot;], targets = [&quot;eee&quot;,&quot;ffff&quot;]
<strong>Output: </strong>&quot;eeebffff&quot;
<strong>Explanation:</strong> &quot;a&quot; starts at index 0 in S, so it&#39;s replaced by &quot;eee&quot;.
&quot;cd&quot; starts at index 2 in S, so it&#39;s replaced by &quot;ffff&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>S = &quot;abcd&quot;, indexes = [0,2], sources = [&quot;ab&quot;,&quot;ec&quot;], targets = [&quot;eee&quot;,&quot;ffff&quot;]
<strong>Output: </strong>&quot;eeecd&quot;
<strong>Explanation:</strong> &quot;ab&quot; starts at index 0 in S, so it&#39;s replaced by &quot;eee&quot;. 
&quot;ec&quot; doesn&#39;t starts at index 2 in the <strong>original</strong> S, so we do nothing.
</pre>

<p>Notes:</p>

<ol>
	<li><code>0 &lt;=&nbsp;indexes.length =&nbsp;sources.length =&nbsp;targets.length &lt;= 100</code></li>
	<li><code>0&nbsp;&lt;&nbsp;indexes[i]&nbsp;&lt; S.length &lt;= 1000</code></li>
	<li>All characters in given inputs are lowercase letters.</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags
- String (string)

## Companies
- Google - 8 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Direct [Accepted]

**Intuition and Algorithm**

We showcase two different approaches.  In both approaches, we build some answer string `ans`, that starts as `S`.  Our main motivation in these approaches is to be able to identify and handle when a given replacement operation does nothing.

In *Java*, the idea is to build an array `match` that tells us `match[ix] = j` whenever `S[ix]` is the head of a successful replacement operation `j`: that is, whenever `S[ix:].startswith(sources[j])`.

After, we build the answer using this match array.  For each index `ix` in `S`, we can use `match` to check whether `S[ix]` is being replaced or not.  We repeatedly either write the next character `S[ix]`, or groups of characters `targets[match[ix]]`, depending on the value of `match[ix]`.

In *Python*, we sort our replacement jobs `(i, x, y)` in reverse order.  If `S[i:].startswith(x)`, then we can replace that section `S[i:i+len(x)]` with the target `y`.  We used a reverse order so that edits to `S` do not interfere with the rest of the queries.

<iframe src="https://leetcode.com/playground/NrWCw5Cb/shared" frameBorder="0" width="100%" height="480" name="NrWCw5Cb"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(NQ)$$, where $$N$$ is the length of `S`, and we have $$Q$$ replacement operations.  (Our complexity could be faster with a more accurate implementation, but it isn't necessary.)

* Space Complexity: $$O(N)$$, if we consider `targets[i].length <= 100` as a constant bound.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Replace S from right to left
- Author: lee215
- Creation Date: Sun May 13 2018 11:08:18 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 10 2019 13:47:38 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**:
Verify equality of string and change it if necessay.
The only thing we need take attention is that the string form sources and targets can be different.
Instead of record the length changement, I sort indexes and do it from right to left.
(Since `indexes.length <= 100` is really small)
In this way, the different length won\'t bother us anymore.
<br>

## **Explanation**:
1. Descending `indexes[]` with tracking its index.
2. Verify equality of subring in `S` source and source.
3. Replace `S` if necessay.
<br>


## **Time Complexity**:
`O(SN)`

Comments from @CanDong:
Since there won\'t be any overlap in replacement,
every character in S will be compared at most once.
If using StringBuilder, it should be O(NlogN + S).
<br>

**C++:**
```
    string findReplaceString(string S, vector<int>& indexes, vector<string>& sources, vector<string>& targets) {
        vector<pair<int, int>> sorted;
        for (int i = 0 ; i < indexes.size(); i++)
            sorted.push_back({indexes[i], i});
        sort(sorted.rbegin(), sorted.rend());
        for (auto ind : sorted) {
            int i = ind.first;
            string s = sources[ind.second], t = targets[ind.second];
            if (S.substr(i, s.length()) == s)
                S = S.substr(0, i) + t + S.substr(i + s.length());
        }
        return S;
    }
```

**Java:**
```
    public String findReplaceString(String S, int[] indexes, String[] sources, String[] targets) {
        List<int[]> sorted = new ArrayList<>();
        for (int i = 0 ; i < indexes.length; i++) sorted.add(new int[]{indexes[i], i});
        Collections.sort(sorted, Comparator.comparing(i -> -i[0]));
        for (int[] ind: sorted) {
            int i = ind[0], j = ind[1];
            String s = sources[j], t = targets[j];
            if (S.substring(i, i + s.length()).equals(s)) S = S.substring(0, i) + t + S.substring(i + s.length());
        }
        return S;
    }
```
**Python:**
```
    def findReplaceString(self, S, indexes, sources, targets):
        for i, s, t in sorted(zip(indexes, sources, targets), reverse=True):
            S = S[:i] + t + S[i + len(s):] if S[i:i + len(s)] == s else S
        return S
```

**1-line Python:**
```
    def findReplaceString(self, S, indexes, sources, targets):
        return reduce(lambda S, (i, s, t): S[:i] + t + S[i + len(s):] if S[i:i + len(s)] == s else S, sorted(zip(indexes, sources, targets))[::-1], S)
```

</p>


### Java O(n) solution
- Author: kevinxw
- Creation Date: Wed May 30 2018 16:03:56 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 19:47:26 GMT+0800 (Singapore Standard Time)

<p>
The technique is like "piece table", which is used in editors.  We record all the valid operations first and put them into a piece table, then iterate the string index to "apply" these operations.
```
    public String findReplaceString(String S, int[] indexes, String[] sources, String[] targets) {
        Map<Integer, Integer> table = new HashMap<>();
        for (int i=0; i<indexes.length; i++) {
            // if a match is found in the original string, record it
            if (S.startsWith(sources[i], indexes[i])) {
                table.put(indexes[i], i);
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<S.length(); ) {
            if (table.containsKey(i)) {
                // if a replacement was recorded before
                sb.append(targets[table.get(i)]);
                i+=sources[table.get(i)].length();
            } else {
                // if no replacement happened at this index
                sb.append(S.charAt(i));
                i++;
            }
        }
        return sb.toString();
    }
```
</p>


### java 10 line 14ms and 13 line 7ms  codes - both use StringBuilder replace from end.
- Author: rock
- Creation Date: Sun May 13 2018 11:26:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 30 2020 00:41:10 GMT+0800 (Singapore Standard Time)

<p>
Method 1: Time O(n ^ 2), space: O(n).

1). Use **TreeMap** to sort *indexes*: put the (indexes[i], i) pair into the map;
2). Iterate the **TreeMap** by reverse order, and hence avoid the change of the indices of the rest part of the original String *S* after each replace.

```
    public String findReplaceString(String S, int[] indexes, String[] sources, String[] targets) {
        StringBuilder sb = new StringBuilder(S);
        TreeMap<Integer, Integer> tm = new TreeMap<>();
        for (int i = 0; i < indexes.length; ++i) { tm.put(indexes[i], i); }
        for (int key : tm.descendingKeySet()) {
            int i = tm.get(key);
            if (S.substring(indexes[i]).startsWith(sources[i])) {
                sb.replace(indexes[i], indexes[i] + sources[i].length(), targets[i]);
            }
        } 
        return sb.toString();
    }
```

Method 2: Time O(n ^ 2), space: O(n). - credit to **@billionaire100**.

No sort, use an array to store *indexes* \'s indices as values, values as indices. 

```
    public String findReplaceString(String S, int[] indexes, String[] sources, String[] targets) {
        StringBuilder sb = new StringBuilder(S);
        int len = S.length();
        int[] indexValInvert = new int[len];
        Arrays.fill(indexValInvert, -1);
        for (int i = 0; i < indexes.length; ++i) { indexValInvert[indexes[i]] = i; }
        for (int i = len - 1; i >= 0; --i) {
            int j = indexValInvert[i];
            if (j < 0) { continue; }
            if (S.substring(indexes[j]).startsWith(sources[j])) {
                sb.replace(indexes[j], indexes[j] + sources[j].length(), targets[j]);
            }
        } 
        return sb.toString();
    }
```
</p>


