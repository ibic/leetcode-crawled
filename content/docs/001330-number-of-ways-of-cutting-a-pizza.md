---
title: "Number of Ways of Cutting a Pizza"
weight: 1330
#id: "number-of-ways-of-cutting-a-pizza"
---
## Description
<div class="description">
<p>Given a rectangular pizza represented as a <code>rows x cols</code>&nbsp;matrix containing the following characters: <code>&#39;A&#39;</code> (an apple) and <code>&#39;.&#39;</code> (empty cell) and given the integer <code>k</code>. You have to cut the pizza into <code>k</code> pieces using <code>k-1</code> cuts.&nbsp;</p>

<p>For each cut you choose the direction: vertical or horizontal, then you choose a cut position at the cell boundary and cut the pizza into two pieces. If you cut the pizza vertically, give the left part of the pizza to a person. If you cut the pizza horizontally, give the upper part of the pizza to a person. Give the last piece of pizza to the last person.</p>

<p><em>Return the number of ways of cutting the pizza such that each piece contains <strong>at least</strong> one apple.&nbsp;</em>Since the answer can be a huge number, return this modulo 10^9 + 7.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/04/23/ways_to_cut_apple_1.png" style="width: 500px; height: 378px;" /></strong></p>

<pre>
<strong>Input:</strong> pizza = [&quot;A..&quot;,&quot;AAA&quot;,&quot;...&quot;], k = 3
<strong>Output:</strong> 3 
<strong>Explanation:</strong> The figure above shows the three ways to cut the pizza. Note that pieces must contain at least one apple.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> pizza = [&quot;A..&quot;,&quot;AA.&quot;,&quot;...&quot;], k = 3
<strong>Output:</strong> 1
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> pizza = [&quot;A..&quot;,&quot;A..&quot;,&quot;...&quot;], k = 1
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= rows, cols &lt;= 50</code></li>
	<li><code>rows ==&nbsp;pizza.length</code></li>
	<li><code>cols ==&nbsp;pizza[i].length</code></li>
	<li><code>1 &lt;= k &lt;= 10</code></li>
	<li><code>pizza</code> consists of characters <code>&#39;A&#39;</code>&nbsp;and <code>&#39;.&#39;</code> only.</li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] DP + PrefixSum in Matrix - Clean code
- Author: hiepit
- Creation Date: Sun May 10 2020 12:04:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 10 2020 18:28:48 GMT+0800 (Singapore Standard Time)

<p>
**Java ~ 6ms**
```java
class Solution {
    public int ways(String[] pizza, int k) {
        int m = pizza.length, n = pizza[0].length();
        Integer[][][] dp = new Integer[k][m][n];
        int[][] preSum = new int[m+1][n+1]; // preSum[r][c] is the total number of apples in pizza[r:][c:]
        for (int r = m - 1; r >= 0; r--)
            for (int c = n - 1; c >= 0; c--)
                preSum[r][c] = preSum[r][c+1] + preSum[r+1][c] - preSum[r+1][c+1] + (pizza[r].charAt(c) == \'A\' ? 1 : 0);
        return dfs(m, n, k-1, 0, 0, dp, preSum);
    }
    int dfs(int m, int n, int k, int r, int c, Integer[][][] dp, int[][] preSum) {
        if (preSum[r][c] == 0) return 0; // if the remain piece has no apple -> invalid
        if (k == 0) return 1; // found valid way after using k-1 cuts
        if (dp[k][r][c] != null) return dp[k][r][c];
        int ans = 0;
        // cut in horizontal
        for (int nr = r + 1; nr < m; nr++) 
            if (preSum[r][c] - preSum[nr][c] > 0) // cut if the upper piece contains at least one apple
                ans = (ans + dfs(m, n, k - 1, nr, c, dp, preSum)) % 1_000_000_007;
        // cut in vertical
        for (int nc = c + 1; nc < n; nc++) 
            if (preSum[r][c] - preSum[r][nc] > 0) // cut if the left piece contains at least one apple
                ans = (ans + dfs(m, n, k - 1, r, nc, dp, preSum)) % 1_000_000_007;
        return dp[k][r][c] = ans;
    }
}
```

**Complexity**
- Time: `O(k*m*n*(m+n))`, where `m<=50` is the number of rows, `n<=50` is the number of columns, `k<=10` is the number of pieces of pizza.
  Explain: There are total `k*m*n` states in `dfs(...k, r, c...)`. Each state needs maximum `(m+n)` times to cut in horizontal or vertical.
- Space: `O(k*m*n)`

**Basic Prefix Sum In matrix:** [304. Range Sum Query 2D - Immutable](https://leetcode.com/problems/range-sum-query-2d-immutable/discuss/572648)


**More versions**
**C++ ~ 20ms**
```c++
class Solution {
public:
    int ways(vector<string>& pizza, int k) {
        int m = pizza.size(), n = pizza[0].size();
        vector<vector<vector<int>>> dp = vector(k, vector(m, vector(n, -1)));
        vector<vector<int>> preSum = vector(m+1, vector(n+1, 0)); // preSum[r][c] is the total number of apples in pizza[r:][c:]
        for (int r = m - 1; r >= 0; r--)
            for (int c = n - 1; c >= 0; c--)
                preSum[r][c] = preSum[r][c+1] + preSum[r+1][c] - preSum[r+1][c+1] + (pizza[r][c] == \'A\');
        return dfs(m, n, k-1, 0, 0, dp, preSum);
    }
    int dfs(int m, int n, int k, int r, int c, vector<vector<vector<int>>>& dp, vector<vector<int>>& preSum) {
        if (preSum[r][c] == 0) return 0; // if the remain piece has no apple -> invalid
        if (k == 0) return 1; // found valid way after using k-1 cuts
        if (dp[k][r][c] != -1) return dp[k][r][c];
        int ans = 0;
        // cut in horizontal
        for (int nr = r + 1; nr < m; nr++) 
            if (preSum[r][c] - preSum[nr][c] > 0) // cut if the upper piece contains at least one apple
                ans = (ans + dfs(m, n, k - 1, nr, c, dp, preSum)) % 1000000007;
        // cut in vertical
        for (int nc = c + 1; nc < n; nc++) 
            if (preSum[r][c] - preSum[r][nc] > 0) // cut if the left piece contains at least one apple
                ans = (ans + dfs(m, n, k - 1, r, nc, dp, preSum)) % 1000000007;
        return dp[k][r][c] = ans;
    }
};
```

**Python3**
```python
class Solution:
    def ways(self, pizza: List[str], K: int) -> int:
        m, n, MOD = len(pizza), len(pizza[0]), 10 ** 9 + 7
        preSum = [[0] * (n + 1) for _ in range(m + 1)]
        for r in range(m - 1, -1, -1):
            for c in range(n - 1, -1, -1):
                preSum[r][c] = preSum[r][c + 1] + preSum[r + 1][c] - preSum[r + 1][c + 1] + (pizza[r][c] == \'A\')

        @lru_cache(None)
        def dp(k, r, c):
            if preSum[r][c] == 0: return 0
            if k == 0: return 1
            ans = 0
            # cut horizontally
            for nr in range(r + 1, m):
                if preSum[r][c] - preSum[nr][c] > 0:
                    ans = (ans + dp(k - 1, nr, c)) % MOD
            # cut vertically                    
            for nc in range(c + 1, n):
                if preSum[r][c] - preSum[r][nc] > 0:
                    ans = (ans + dp(k - 1, r, nc)) % MOD
            return ans

        return dp(K - 1, 0, 0)
```
</p>


### [C++] DP cutting (with picture explanation)
- Author: zhanghuimeng
- Creation Date: Sun May 10 2020 12:07:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 10 2020 12:07:32 GMT+0800 (Singapore Standard Time)

<p>
Given a rectangular grid with apples on the cells, what is the number of ways to cut the grid into `k` pieces so that each piece has at least one apple on them?

# Explanation

![image](https://assets.leetcode.com/users/zhanghuimeng/image_1589083632.png)

This is a DP problem. Let `n`, `m` denote the rows and columns of the matrix, `mat` denote the matrix, `f[r][c][l]` denote the number of ways to cut `mat[r..n-1][c..m-1]` into `l` pieces so that each piece has at least one apple on them, `g[r][c]` denote the number of apples on `mat[r..n-1][c..m-1]`. Because after cutting, we always have the down-right corner of the matrix left, so this setting is valid.

For `mat[r..n-1][c..m-1]`, we can cut the matrix in two directions in a number of ways, as long as the piece that is cut off has at least one apple on it. So

```txt
f[r][c][l] = 
    sum(f[i][c][l-1]) (i=r+1...n-1, g[r][c]-g[i][c]>0) +
    sum(f[r][j][l-1]) (j=c+1...m-1, g[r][c]-g[r][j]>0)
```

The time complexity is `O(n * m * max(n, m) * k)`.

# C++ Solution

Note: in the code, `f[r][c][rest]` means the way to cut `mat[r..n-1][c..m-1]` with `rest` cuts (and cutting into `rest+1` pieces).

```cpp
class Solution {
    int n, m;
    vector<string> pizza;
    typedef long long int LL;
    LL P = 1e9 + 7;
    LL g[55][55];
    LL f[55][55][15];
    
    int dp(int r, int c, int rest) {
        if (f[r][c][rest] != -1) return f[r][c][rest];
        
        if (rest == 0) {
            if (g[r][c] > 0) f[r][c][rest] = 1;
            else f[r][c][rest] = 0;
            return f[r][c][rest];
        }
        
        f[r][c][rest] = 0;
        
        // cut horizontally to [row, i-1] [i, n-1]
        for (int i = r + 1; i < n; i++) {
            if (g[r][c] - g[i][c] > 0 && g[i][c] >= rest) {
                f[r][c][rest] = (f[r][c][rest] + dp(i, c, rest - 1)) % P;
            }
        }
        
        // cut vertically to [col, j-1] [j, m-1]
        for (int j = c + 1; j < m; j++) {
            if (g[r][c] - g[r][j] > 0 && g[r][j] >= rest)
                f[r][c][rest] = (f[r][c][rest] + dp(r, j, rest - 1)) % P;
        }
        
        return f[r][c][rest];
    }
    
public:
    int ways(vector<string>& pizza, int k) {
        n = pizza.size();
        m = pizza[0].length();
        this->pizza = pizza;
        
        memset(g, 0, sizeof(g));
        for (int i = n - 1; i >= 0; i--) {
            for (int j = m - 1; j >= 0; j--) {
                g[i][j] = g[i][j+1];
                for (int l = i; l < n; l++)
                    g[i][j] += (pizza[l][j] == \'A\');
            }
        }
        
        memset(f, -1, sizeof(f));
        
        return dp(0, 0, k-1);
    }
};
```

</p>


### [C++] Solutions w/ Prefix sum + DP and w/o Prefix sum(Smart pruning) +DP
- Author: PhoenixDD
- Creation Date: Sun May 10 2020 13:07:00 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 11 2020 04:30:32 GMT+0800 (Singapore Standard Time)

<p>
**Observation**

Let\'s try cutting the pizza in every way possible recursively, we can easily notice that there can be duplicate calculations for the same piece of pizza. It\'s straight forward to figure out that we can use caching to optimize our recursive solution.

**Solution 1**
This solution is optimized as it uses prefix sum to get if the region we cut has apples or not in O(1), this isn\'t required for the given constraints.
```c++
static int MOD=1e9+7;
class Solution {
public:
    vector<vector<vector<int>>> memo;
    vector<vector<int>> prefixSum;
    int getApples(int row1,int col1,int row2,int col2)  //Get number of apples for the rectangle formed by the provided parameters
    {
        return prefixSum[row2+1][col2+1]-prefixSum[row1][col2+1]-prefixSum[row2+1][col1]+prefixSum[row1][col1];
    }
    int dp(vector<string>& pizza,int left,int top,int k)
    {
        if(memo[top][left][k]!=-1)
            return memo[top][left][k];
        if(k==1)
            return getApples(top,left,pizza.size()-1,pizza[0].size()-1)!=0;     //If this region has apples return 1 else 0
        memo[top][left][k]=0;
        for(int c=left;c<pizza[0].size()-1;c++)     //Move the left boundary till the last column (excluding the last column)
            if(getApples(top,left,pizza.size()-1,c))    //If apples exist in the left partition.
               memo[top][left][k]=(memo[top][left][k]+dp(pizza,c+1,top,k-1))%MOD;  
        for(int r=top;r<pizza.size()-1;r++)         //Move the top boundary till the last row (excluding the last row)
            if(getApples(top,left,r,pizza[0].size()-1)) //If apples exist in the top partition.
                memo[top][left][k]=(memo[top][left][k]+dp(pizza,left,r+1,k-1))%MOD;
        return memo[top][left][k];
    }
    int ways(vector<string>& pizza, int k) 
    {
        memo.resize(pizza.size(),vector<vector<int>>(pizza[0].size(),vector<int>(k+1,-1)));
        prefixSum.resize(pizza.size()+1,vector<int>(pizza[0].size()+1,0));      //Prefix sum to get the number of apples in a region in O(1)
        for(int i=0;i<pizza.size();i++)
            for(int j=0;j<pizza[0].size();j++)
                prefixSum[i+1][j+1]=prefixSum[i+1][j]+prefixSum[i][j+1]-prefixSum[i][j]+(pizza[i][j]==\'A\');
        return dp(pizza,0,0,k);
    }
};
```
**Complexity**
Time: `O(k*m*n*(m+n))`
Space: `O(k*m*n)`.

**Solution 2**
Without using prefix sum by smartly pruning the need to check if apples exists.
```c++
static int MOD=1e9+7;
class Solution {
public:
    vector<vector<vector<int>>> memo;
    bool leftBoundaryContains(vector<string>& pizza,int col,int top)		//Check if left piece contains an apple
    {
        for(int i=top;i<pizza.size();i++)
            if(pizza[i][col]==\'A\')
                return true;
        return false;
    }
    bool topBoundaryContains(vector<string>& pizza,int left,int row)	//Check if top piece contains an apple
    {
        for(int i=left;i<pizza[0].size();i++)
            if(pizza[row][i]==\'A\')
                return true;
        return false;
    }
    int dp(vector<string>& pizza,int left,int top,int k)
    {
        if(memo[top][left][k]!=-1)
            return memo[top][left][k];
        if(k==1)		//Return 1 if the last piece left contains apple else 0.
        {
            for(int i=top;i<pizza.size();i++)
                for(int j=left;j<pizza[0].size();j++)
                    if(pizza[i][j]==\'A\')
                        return 1;
            return 0;
        }
        memo[top][left][k]=0;
        bool leftContains=false,topContains=false;	//These flags make the solution smater by skipping rechecking larger pieces whose smaller ones have appples.
        for(int c=left;c<pizza[0].size()-1;c++)		 //Move the left boundary till the last column (excluding the last column)
            if(leftContains||leftBoundaryContains(pizza,c,top))
            {
                leftContains=true;			//Mark that left piece will always contain apple from now on.
                memo[top][left][k]=(memo[top][left][k]+dp(pizza,c+1,top,k-1))%MOD;
                
            }
        for(int r=top;r<pizza.size()-1;r++)			 //Move the top boundary till the last row (excluding the last row)
            if(topContains||topBoundaryContains(pizza,left,r))
            {
                topContains=true;			//Mark that top piece will always contain apple from now on.
                memo[top][left][k]=(memo[top][left][k]+dp(pizza,left,r+1,k-1))%MOD;
                
            }
        return memo[top][left][k];
    }
    int ways(vector<string>& pizza, int k) 
    {
        memo.resize(pizza.size(),vector<vector<int>>(pizza[0].size(),vector<int>(k+1,-1)));
        return dp(pizza,0,0,k);
    }
};
```
**Complexity**
Time: Worst case - `O(k*(m+n)*(m*n)^2)`, Best case - `O(k*(m+n)*m*n)`
Space: `O(n*m*k)`.
</p>


