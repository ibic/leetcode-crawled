---
title: "Escape The Ghosts"
weight: 726
#id: "escape-the-ghosts"
---
## Description
<div class="description">
<p>You are playing a simplified PAC-MAN game on an infinite 2-D grid. You start at the point <code>[0, 0]</code>, and you are given a destination point <code>target = [x<sub>target</sub>, y<sub>target</sub>]</code>, which you are trying to get to. There are several ghosts on the map with their starting positions given as an array <code>ghosts</code>, where <code>ghosts[i] = [x<sub>i</sub>, y<sub>i</sub>]</code> represents the starting position of the <code>i<sup>th</sup></code> ghost. All inputs are <strong>integral coordinates</strong>.</p>

<p>Each turn, you and all the ghosts may independently choose to either <strong>move 1 unit</strong> in any of the four cardinal directions: north, east, south, or west or <strong>stay still</strong>. All actions happen <strong>simultaneously</strong>.</p>

<p>You escape if and only if you can reach the target <strong>before</strong> any ghost reaches you. If you reach any square (including the target) at the <strong>same time</strong> as a ghost, it <strong>does not</strong> count as an escape.</p>

<p>Return <code>true</code><em> if it is possible to escape, otherwise return </em><code>false</code><em>.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> ghosts = [[1,0],[0,3]], target = [0,1]
<strong>Output:</strong> true
<strong>Explanation:</strong> You can reach the destination (0, 1) after 1 turn, while the ghosts located at (1, 0) and (0, 3) cannot catch up with you.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> ghosts = [[1,0]], target = [2,0]
<strong>Output:</strong> false
<strong>Explanation:</strong> You need to reach the destination (2, 0), but the ghost at (1, 0) lies between you and the destination.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> ghosts = [[2,0]], target = [1,0]
<strong>Output:</strong> false
<strong>Explanation:</strong> The ghost can reach the target at the same time as you.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> ghosts = [[5,0],[-10,-2],[0,-5],[-2,-2],[-7,1]], target = [7,7]
<strong>Output:</strong> false
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> ghosts = [[-1,0],[0,1],[-1,0],[0,1],[-1,0]], target = [0,0]
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= ghosts.length &lt;= 100</code></li>
	<li><code>ghosts[i].length == 2</code></li>
	<li><code>-10<sup>4</sup> &lt;= x<sub>i</sub>, y<sub>i</sub> &lt;= 10<sup>4</sup></code></li>
	<li>There can be <strong>multiple ghosts</strong> in the same location.</li>
	<li><code>target.length == 2</code></li>
	<li><code>-10<sup>4</sup> &lt;= x<sub>target</sub>, y<sub>target</sub> &lt;= 10<sup>4</sup></code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Taxicab Distance [Accepted]

**Intuition**

The *taxicab* distance is the number of moves required to get from point `A` to point `B` in our grid.  It is calculated as `dist(A, B) = abs(A.x - B.x) + abs(A.y - B.y)`.

Let's say we start at `S`, the ghost starts at `G`, the target is `T`, and the ghost first meets us at `X`.  This implies `dist(G, X) <= dist(S, X)`, as the ghost must reach `X` before or at the time that we do.

Now, if the ghost travels from `G` to `X` and then to `T`, it will reach `T` at time `dist(G, T) <= dist(G, X) + dist(X, T) <= dist(S, X) + dist(X, T)`.  The first inequality is because of the *triangle inequality* that all distance metrics satisfy.

The above shows that it is at least as good for the ghost to just travel directly to the target: if it could reach us beforehand (at `X`), it will also reach us if it goes to `X` then to `T`, and then it would reach us if it just went directly to `T`.

Also, if the ghost goes directly to the target, then a necessary condition is clearly that we get to the target before the ghost.

Once we can make the assumption that all parties just go directly to the target in the shortest time possible, the problem is greatly simplified.

**Algorithm**

Check that our (taxicab) distance to the target is smaller than the distance from any ghost to the target.

<iframe src="https://leetcode.com/playground/vqHng6WL/shared" frameBorder="0" width="100%" height="276" name="vqHng6WL"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\text{ghosts}.\text{length})$$.

* Space Complexity:  $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Why interception in the middle is not a good idea for ghosts.
- Author: ctrlcctrlv555
- Creation Date: Mon Feb 26 2018 04:50:55 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 13:44:05 GMT+0800 (Singapore Standard Time)

<p>
![image](https://s3-lc-upload.s3.amazonaws.com/users/ctrlcctrlv555/image_1519591480.png)
Let's say you always take the shortest route to reach the target because if you go a longer or a more tortuous route, I believe the ghost has a better chance of getting you.
Denote your starting point A, ghost's starting point B, the target point C.
For a ghost to intercept you, there has to be some point D on AC such that AD = DB. Fix D. By triangle inequality, AC = AD + DC = DB + DC >= BC. What that means is if the ghost can intercept you in the middle, it can actually reach the target at least as early as you do. So wherever the ghost starts at (and wherever the interception point is), its best chance of getting you is going directly to the target and waiting there rather than intercepting you in the middle.
</p>


### Java 5 liner
- Author: shawngao
- Creation Date: Sun Feb 25 2018 12:01:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 01:01:07 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public boolean escapeGhosts(int[][] ghosts, int[] target) {
        int max = Math.abs(target[0]) + Math.abs(target[1]);
        for (int[] ghost : ghosts) {
            int d = Math.abs(ghost[0] - target[0]) + Math.abs(ghost[1] - target[1]);
            if (d <= max) return false;
        }
        return true;
    }
}
```
</p>


### Short with explanation, python
- Author: yorkshire
- Creation Date: Sun Feb 25 2018 12:02:34 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 15:13:34 GMT+0800 (Singapore Standard Time)

<p>
The best tactic for any ghost is to reach the target before pacman and block the exit.

Note that we do not require that any ghost reaches pacman (which will never happen on an infinite grid for a single ghost and be much harder to determine for multiple ghosts).
We only require that pacman can or cannot reach the target with optimal ghost strategy.
If any ghost has the same or lower distance to the target, then it can get there first and wait. Pacman cannot reach the target, although he would not necessarily be killed by a ghost.
If pacman is closer to the target than any ghost, he goes there along the most direct path.

Since we are working on a 2D grid, distances are measured as Manhattan distance.
```
    def escapeGhosts(self, ghosts, target):
        target_dist = abs(target[0]) + abs(target[1])
        
        for r, c in ghosts:
            ghost_target = abs(target[0] - r) + abs(target[1] - c)
            if ghost_target <= target_dist:
                return False
            
        return True
</p>


