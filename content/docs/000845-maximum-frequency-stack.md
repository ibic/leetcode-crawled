---
title: "Maximum Frequency Stack"
weight: 845
#id: "maximum-frequency-stack"
---
## Description
<div class="description">
<p>Implement <code>FreqStack</code>, a class which simulates the operation of a stack-like data structure.</p>

<p><code>FreqStack</code>&nbsp;has two functions:</p>

<ul>
	<li><code>push(int x)</code>, which pushes an integer <code>x</code> onto the stack.</li>
	<li><code>pop()</code>, which <strong>removes</strong> and returns the most frequent element in the stack.
	<ul>
		<li>If there is a tie for most frequent element, the element closest to the top of the stack is removed and returned.</li>
	</ul>
	</li>
</ul>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>
<span id="example-input-1-1">[&quot;FreqStack&quot;,&quot;push&quot;,&quot;push&quot;,&quot;push&quot;,&quot;push&quot;,&quot;push&quot;,&quot;push&quot;,&quot;pop&quot;,&quot;pop&quot;,&quot;pop&quot;,&quot;pop&quot;]</span>,
<span id="example-input-1-2">[[],[5],[7],[5],[7],[4],[5],[],[],[],[]]</span>
<strong>Output: </strong><span id="example-output-1">[null,null,null,null,null,null,null,5,7,5,4]</span>
<strong>Explanation</strong>:
After making six .push operations, the stack is [5,7,5,7,4,5] from bottom to top.  Then:

pop() -&gt; returns 5, as 5 is the most frequent.
The stack becomes [5,7,5,7,4].

pop() -&gt; returns 7, as 5 and 7 is the most frequent, but 7 is closest to the top.
The stack becomes [5,7,5,4].

pop() -&gt; returns 5.
The stack becomes [5,7,4].

pop() -&gt; returns 4.
The stack becomes [5,7].
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li>Calls to <code>FreqStack.push(int x)</code>&nbsp;will be such that <code>0 &lt;= x &lt;= 10^9</code>.</li>
	<li>It is guaranteed that <code>FreqStack.pop()</code> won&#39;t be called if the stack has zero elements.</li>
	<li>The total number of <code>FreqStack.push</code> calls will not exceed <code>10000</code> in a single test case.</li>
	<li>The total number of <code>FreqStack.pop</code>&nbsp;calls will not exceed <code>10000</code> in a single test case.</li>
	<li>The total number of <code>FreqStack.push</code> and <code>FreqStack.pop</code> calls will not exceed <code>150000</code> across all test cases.</li>
</ul>

<div>
<p>&nbsp;</p>
</div>

</div>

## Tags
- Hash Table (hash-table)
- Stack (stack)

## Companies
- Amazon - 5 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Stack of Stacks

**Intuition**

Evidently, we care about the frequency of an element.  Let `freq` be a `Map` from $$x$$ to the number of occurrences of $$x$$.

Also, we (probably) care about `maxfreq`, the current maximum frequency of any element in the stack.  This is clear because we must pop the element with the maximum frequency.

The main question then becomes: among elements with the same (maximum) frequency, how do we know which element is most recent?  We can use a stack to query this information: the top of the stack is the most recent.

To this end, let `group` be a map from frequency to a stack of elements with that frequency.  We now have all the required components to implement `FreqStack`.

**Algorithm**

Actually, as an implementation level detail, if `x` has frequency `f`, then we'll have `x` in all `group[i] (i <= f)`, not just the top.  This is because each `group[i]` will store information related to the `i`th copy of `x`.

Afterwards, our goal is just to maintain `freq`, `group`, and `maxfreq` as described above.

<iframe src="https://leetcode.com/playground/dEw47wsY/shared" frameBorder="0" width="100%" height="500" name="dEw47wsY"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(1)$$ for both `push` and `pop` operations.

* Space Complexity:  $$O(N)$$, where `N` is the number of elements in the `FreqStack`.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] O(1)
- Author: lee215
- Creation Date: Sun Aug 26 2018 11:01:03 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 01:38:58 GMT+0800 (Singapore Standard Time)

<p>
Hash map `freq` will count the frequence of elements.
Hash map `m` is a map of stack.
If element `x` has n frequence, we will push `x` n times in `m[1], m[2] .. m[n]`
`maxfreq` records the maximum frequence.

`push(x)` will push `x` to`m[++freq[x]]`
`pop()` will pop from the `m[maxfreq]`


**C++:**
```
    unordered_map<int, int> freq;
    unordered_map<int, stack<int>> m;
    int maxfreq = 0;

    void push(int x) {
        maxfreq = max(maxfreq, ++freq[x]);
        m[freq[x]].push(x);
    }

    int pop() {
        int x = m[maxfreq].top();
        m[maxfreq].pop();
        if (!m[freq[x]--].size()) maxfreq--;
        return x;
    }
```

**Java:**
```
class FreqStack {
    HashMap<Integer, Integer> freq = new HashMap<>();
    HashMap<Integer, Stack<Integer>> m = new HashMap<>();
    int maxfreq = 0;

    public void push(int x) {
        int f = freq.getOrDefault(x, 0) + 1;
        freq.put(x, f);
        maxfreq = Math.max(maxfreq, f);
        if (!m.containsKey(f)) m.put(f, new Stack<Integer>());
        m.get(f).add(x);
    }

    public int pop() {
        int x = m.get(maxfreq).pop();
        freq.put(x, maxfreq - 1);
        if (m.get(maxfreq).size() == 0) maxfreq--;
        return x;
    }
}
```
**Python:**
```
    def __init__(self):
        self.freq = collections.Counter()
        self.m = collections.defaultdict(list)
        self.maxf = 0

    def push(self, x):
        freq, m = self.freq, self.m
        freq[x] += 1
        self.maxf = max(self.maxf, freq[x])
        m[freq[x]].append(x)

    def pop(self):
        freq, m, maxf = self.freq, self.m, self.maxf
        x = m[maxf].pop()
        if not m[maxf]: self.maxf = maxf - 1
        freq[x] -= 1
        return x
```

</p>


### JAVA O(1) solution easy understand using bucket sort
- Author: NintendoSwitch
- Creation Date: Sun Aug 26 2018 11:28:43 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 13 2018 01:54:09 GMT+0800 (Singapore Standard Time)

<p>
```
class FreqStack {

    List<Stack<Integer>> bucket;
    HashMap<Integer, Integer> map;
    
    public FreqStack() {
        bucket = new ArrayList<>();
        map = new HashMap<>();
    }
    
    public void push(int x) {
        map.put(x, map.getOrDefault(x, 0) + 1);
        int freq = map.get(x);
        if (freq - 1 >= bucket.size()) {
            bucket.add(new Stack<Integer>());
        }
        bucket.get(freq - 1).add(x);
    }
    
    public int pop() {
        int freq = bucket.size();
        int x = bucket.get(freq - 1).pop();
        if (bucket.get(freq - 1).isEmpty()) bucket.remove(bucket.size() - 1);
        
        map.put(x, map.get(x) - 1);
        if (map.get(x) == 0) map.remove(x);
        
        return x;
    }
}
```
</p>


### Python Simple PriorityQueue
- Author: rikenshah
- Creation Date: Sun Aug 26 2018 11:07:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 02:10:53 GMT+0800 (Singapore Standard Time)

<p>
Storing `(count, index, number)` in min-heap and keeping map of counts. Since its a min-heap, I am negating the `count` and `index` while pushing in the heap. 

The intuition is, heap will always keep the element with max count on top, and if two elements have same count, the second element (`index`) will be considered while doing `pop` operation. Also, the `count` map, is useful when the new occurence of the exisiting element is pushed. 


```python
class FreqStack:

    def __init__(self):
        self.heap = []
        self.m = collections.defaultdict(int)
        self.counter = 0
        
    def push(self, x):
        self.m[x]+=1
        heapq.heappush(self.heap,(-self.m[x], -self.counter, x))
        self.counter+=1
    
    def pop(self):
        toBeRemoved = heapq.heappop(self.heap)
        self.m[toBeRemoved[2]]-=1
        return toBeRemoved[2]
```
</p>


