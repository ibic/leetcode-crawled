---
title: "Fair Candy Swap"
weight: 838
#id: "fair-candy-swap"
---
## Description
<div class="description">
<p>Alice and Bob have candy bars of different sizes: <code>A[i]</code> is the size of the <code>i</code>-th bar of candy that Alice has, and <code>B[j]</code> is the size of the <code>j</code>-th bar of candy that Bob has.</p>

<p>Since they are friends, they would like to exchange one candy bar each so that after the exchange, they both have the same total&nbsp;amount of candy.&nbsp; (<em>The total amount of candy&nbsp;a person has is the sum of the sizes of candy&nbsp;bars they have.</em>)</p>

<p>Return an integer array <code>ans</code>&nbsp;where <code>ans[0]</code> is the size of the candy bar that Alice must exchange, and <code>ans[1]</code> is the size of the candy bar that Bob must exchange.</p>

<p>If there are multiple answers, you may return any one of them.&nbsp; It is guaranteed an answer exists.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[1,1]</span>, B = <span id="example-input-1-2">[2,2]</span>
<strong>Output: </strong><span id="example-output-1">[1,2]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">[1,2]</span>, B = <span id="example-input-2-2">[2,3]</span>
<strong>Output: </strong><span id="example-output-2">[1,2]</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-3-1">[2]</span>, B = <span id="example-input-3-2">[1,3]</span>
<strong>Output: </strong><span id="example-output-3">[2,3]</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-4-1">[1,2,5]</span>, B = <span id="example-input-4-2">[2,4]</span>
<strong>Output: </strong><span id="example-output-4">[5,4]</span>
</pre>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ul>
	<li><span><code>1 &lt;= A.length &lt;= 10000</code></span></li>
	<li><span><code>1 &lt;= B.length &lt;= 10000</code></span></li>
	<li><code><span>1 &lt;= A[i] &lt;= 100000</span></code></li>
	<li><code><span>1 &lt;= B[i] &lt;= 100000</span></code></li>
	<li>It is guaranteed that Alice and Bob have different total amounts of&nbsp;candy.</li>
	<li>It is guaranteed there exists an&nbsp;answer.</li>
</ul>
</div>
</div>
</div>
</div>

</div>

## Tags
- Array (array)

## Companies
- Yahoo - 2 (taggedByAdmin: false)
- Fidessa - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Solve the Equation

**Intuition**

If Alice swaps candy `x`, she expects some specific quantity of candy `y` back.

**Algorithm**

Say Alice and Bob have total candy $$S_A, S_B$$ respectively.

If Alice gives candy $$x$$, and receives candy $$y$$, then Bob receives candy $$x$$ and gives candy $$y$$.  Then, we must have

$$
S_A - x + y = S_B - y + x
$$

for a fair candy swap.  This implies

$$
y = x + \frac{S_B - S_A}{2}
$$

Our strategy is simple.  For every candy $$x$$ that Alice has, if Bob has candy $$y = x + \frac{S_B - S_A}{2}$$, we return $$[x, y]$$.  We use a `Set` structure to check whether Bob has the desired candy $$y$$ in constant time.

<iframe src="https://leetcode.com/playground/UL83WLDo/shared" frameBorder="0" width="100%" height="361" name="UL83WLDo"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(A\text{.length} + B\text{.length})$$.

* Space Complexity:  $$O(B\text{.length})$$, the space used by `setB`.  (We can improve this to $$\min(A\text{.length}, B\text{.length})$$ by using an if statement.)
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Aug 19 2018 11:03:14 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 06:30:38 GMT+0800 (Singapore Standard Time)

<p>
Calculate `dif = (sum(A) - sum(B)) / 2`
We want find a pair `(a, b)` with `a = b + dif`

**Time Complexity**:
O(N)

**C++:**
```
    vector<int> fairCandySwap(vector<int> A, vector<int> B) {
        int dif = (accumulate(A.begin(), A.end(), 0) - accumulate(B.begin(), B.end(), 0)) / 2;
        unordered_set<int> S(A.begin(), A.end());
        for (int b: B)
            if (S.count(b + dif))
                return {b + dif, b};
    }
```

**Java:**
```
    public int[] fairCandySwap(int[] A, int[] B) {
        int dif = (IntStream.of(A).sum() - IntStream.of(B).sum()) / 2;
        HashSet<Integer> S = new HashSet<>();
        for (int a : A) S.add(a);
        for (int b : B) if (S.contains(b + dif)) return new int[] {b + dif, b};
        return new int[0];
    }
```
**Python:**
```
    def fairCandySwap(self, A, B):
        dif = (sum(A) - sum(B)) / 2
        A = set(A)
        for b in set(B):
            if dif + b in A:
                return [dif + b, b]
```

</p>


### Java 2 solutions clear explanation with illustration
- Author: peritan
- Creation Date: Sun Aug 19 2018 11:50:38 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 10:12:26 GMT+0800 (Singapore Standard Time)

<p>
![image](https://s3-lc-upload.s3.amazonaws.com/users/marswork/image_1534651707.png)
Therefore, our target is to find a candy pair whose difference is exactly `x/2`
if B > A, logic is exactly the same
#### Sol1 Brute Force 
time complexity:`O(A + B + A * B)`
space  complexity:`O(1)`
```
class Solution {
    public int[] fairCandySwap(int[] A, int[] B) {
        int sumA = 0, sumB = 0;
        for (int i = 0; i < A.length; i++)
            sumA += A[i];
        for (int i = 0; i < B.length; i++)
            sumB += B[i];
        int dif = (sumA - sumB) / 2;
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < B.length; j++) {
                if (A[i] - B[j] == dif)
                    return new int[]{A[i], B[j]};
            }
        }
        return null;
    }
}
```
#### Sol2 Use Hash Set 
time complexity: `O(A + B)`
space complexity: `O(A)`
```
class Solution {
    public int[] fairCandySwap(int[] A, int[] B) {
        int sumA = 0, sumB = 0;
        Set<Integer> setA = new HashSet<>();
        for (int i = 0; i < A.length; i++) {
            sumA += A[i];
            setA.add(A[i]);
        }
        for (int i = 0; i < B.length; i++)
            sumB += B[i];
        int dif = (sumA - sumB) / 2;
        for (int i = 0; i < B.length; i++) {
            int targetA = B[i] + dif;
            if (setA.contains(targetA))
                return new int[]{targetA, B[i]};
        }
        return null;
    }
}
```
</p>


### C++ 24 ms 99% O(n) solution using bitfield
- Author: gucciGang
- Creation Date: Thu Dec 27 2018 10:48:02 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Dec 27 2018 10:48:02 GMT+0800 (Singapore Standard Time)

<p>
The intuition is based on the equation for solving this problem: sumA - A + B = sumB + A - B.

Simplifying this equation yields: 2*B = sumB - sumA + 2*A = 2*A - (sumA - sumB).

Thus, we need to go through each number in A, and see if there is a corresponding number in B that matches the equality. To do this, we can put numbers of B into a set, and then go through  A and see if there is a nubmer in set B that works.

Since we only need to mark numbers, and the range of values are small, we can use a bitfield instead of a set. This saves space, which makes it more cache friendly. It also saves some overhead from doing hashes. (Using a set requires about 100ms run-time).

The time complexity is O(n + m), where n is size of A and m size of B. The space complexity is 200002 bits, which is about 25 KB, which fits inside most L1 caches.

Note: Not sure if it is problem with my code or with LeetCode, but the first time I submitted this solution I got time limit exceeded, but the second time it is 24 ms.

```
class Solution {
public:
    vector<int> fairCandySwap(vector<int>& A, vector<int>& B) {
        
        bitset<200002> bf;
        
        int sumA = 0, sumB = 0;
        for(auto n: A) {
            sumA += n;
        }
        for(auto n: B) {
            sumB += n;
            bf.set(2*n);
        }
        
        int diff = sumA - sumB;
        
        for(auto n: A) {
            int det = 2*n - diff;
            if(det > 0 && det < 200002 && bf.test(det)) {
                return {n, (2*n-diff)/2};
            }
        }
        return {};
    }
};

auto gucciGang = []() {std::ios::sync_with_stdio(false);cin.tie(nullptr);cout.tie(nullptr);return 0;}();
```
</p>


