---
title: "Search in Rotated Sorted Array"
weight: 33
#id: "search-in-rotated-sorted-array"
---
## Description
<div class="description">
<p>You are given an integer array <code>nums</code> sorted in ascending order, and an integer <code>target</code>.</p>

<p>Suppose that <code>nums</code> is rotated at some pivot unknown to you beforehand (i.e., <code>[0,1,2,4,5,6,7]</code> might become <code>[4,5,6,7,0,1,2]</code>).</p>

<p><em>If <code>target</code> is found in the array return its index, otherwise, return <code>-1</code>.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> nums = [4,5,6,7,0,1,2], target = 0
<strong>Output:</strong> 4
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> nums = [4,5,6,7,0,1,2], target = 3
<strong>Output:</strong> -1
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> nums = [1], target = 0
<strong>Output:</strong> -1
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 5000</code></li>
	<li><code>-10^4 &lt;= nums[i] &lt;= 10^4</code></li>
	<li>All values of <code>nums</code> are <strong>unique</strong>.</li>
	<li><code>nums</code> is guranteed to be rotated at some pivot.</li>
	<li><code>-10^4 &lt;= target &lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Facebook - 19 (taggedByAdmin: true)
- Amazon - 16 (taggedByAdmin: false)
- ByteDance - 8 (taggedByAdmin: false)
- Bloomberg - 6 (taggedByAdmin: true)
- Microsoft - 5 (taggedByAdmin: true)
- Apple - 5 (taggedByAdmin: false)
- eBay - 4 (taggedByAdmin: false)
- VMware - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- LinkedIn - 8 (taggedByAdmin: true)
- Expedia - 6 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- Nvidia - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Nutanix - 6 (taggedByAdmin: false)
- Cisco - 5 (taggedByAdmin: false)
- Tencent - 4 (taggedByAdmin: false)
- Baidu - 3 (taggedByAdmin: false)
- Zillow - 3 (taggedByAdmin: false)
- Visa - 3 (taggedByAdmin: false)
- IXL - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- Samsung - 2 (taggedByAdmin: false)
- NetEase - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- TripAdvisor - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Zulily - 2 (taggedByAdmin: false)
- Twitch - 2 (taggedByAdmin: false)
- JPMorgan - 2 (taggedByAdmin: false)
- Tesla - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
   
#### Approach 1: Binary search

The problem is to implement a search in $$\mathcal{O}(\log{N})$$ time
that gives an idea to use a binary search.

The algorithm is quite straightforward : 

* Find a rotation index `rotation_index`, 
_i.e._ index of the smallest element in the array.
Binary search works just perfect here.

* `rotation_index` splits array in two parts. 
Compare `nums[0]` and `target` 
to identify in which part one has to look for `target`.

* Perform a binary search in the chosen part of the array. 
        
!?!../Documents/33_LIS.json:1000,510!?!

<iframe src="https://leetcode.com/playground/Eihw8NMP/shared" frameBorder="0" width="100%" height="500" name="Eihw8NMP"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(\log{N})$$. 
* Space complexity : $$\mathcal{O}(1)$$. 
<br /> 
<br />


---
#### Approach 2: One-pass Binary Search

Instead of going through the input array in two passes, we could achieve the goal in one pass with an _revised_ binary search.

>The idea is that we add some additional *condition checks* in the normal binary search in order to better _narrow down_ the scope of the search. 

**Algorithm**

As in the normal binary search, we keep two pointers (_i.e._ `start` and `end`) to track the search scope. At each iteration, we reduce the search scope into half, by moving either the `start` or `end` pointer to the middle (_i.e._ `mid`) of the previous search scope.

Here are the detailed breakdowns of the algorithm:

- Initiate the pointer `start` to `0`, and the pointer `end` to `n - 1`.

- Perform standard binary search. While `start <= end`:

    - Take an index in the middle `mid` as a pivot.
    
    - If `nums[mid] == target`, the job is done, return `mid`.
    
    - Now there could be two situations:

        - Pivot element is larger than the first element in the array, _i.e._ the subarray from the first element to the pivot is non-rotated, as shown in the following graph.

        ![pic](../Figures/33/33_small_mid.png)

            - If the target is located in the non-rotated subarray: 
            go left: `end = mid - 1`.
            
            - Otherwise: go right: `start = mid + 1`.
      
        - Pivot element is smaller than the first element of the array, _i.e._ the rotation index is somewhere between `0` and `mid`. It implies that the sub-array from the pivot element to the last one is non-rotated, as shown in the following graph.

        ![pic](../Figures/33/33_big_mid.png)

            - If the target is located in the non-rotated subarray: 
            go right: `start = mid + 1`.
            
            - Otherwise: go left: `end = mid - 1`.
            
- We're here because the target is not found. Return -1.
            

**Implementation**

<iframe src="https://leetcode.com/playground/7jEqQ2MG/shared" frameBorder="0" width="100%" height="361" name="7jEqQ2MG"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(\log{N})$$. 
* Space complexity: $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise O(log N) Binary search solution
- Author: lucastan
- Creation Date: Tue Sep 23 2014 00:52:00 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 11:09:21 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        int search(int A[], int n, int target) {
            int lo=0,hi=n-1;
            // find the index of the smallest value using binary search.
            // Loop will terminate since mid < hi, and lo or hi will shrink by at least 1.
            // Proof by contradiction that mid < hi: if mid==hi, then lo==hi and loop would have been terminated.
            while(lo<hi){
                int mid=(lo+hi)/2;
                if(A[mid]>A[hi]) lo=mid+1;
                else hi=mid;
            }
            // lo==hi is the index of the smallest value and also the number of places rotated.
            int rot=lo;
            lo=0;hi=n-1;
            // The usual binary search and accounting for rotation.
            while(lo<=hi){
                int mid=(lo+hi)/2;
                int realmid=(mid+rot)%n;
                if(A[realmid]==target)return realmid;
                if(A[realmid]<target)lo=mid+1;
                else hi=mid-1;
            }
            return -1;
        }
    };
</p>


### Clever idea making it simple
- Author: StefanPochmann
- Creation Date: Mon Jan 18 2016 05:22:33 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 03:51:39 GMT+0800 (Singapore Standard Time)

<p>
This very nice idea is from [rantos22's solution](https://leetcode.com/discuss/66853/c-4-lines-4ms) who sadly only commented *"You are not expected to understand that :)"*, which I guess is the reason it's now it's hidden among the most downvoted solutions. I present an explanation and a more usual implementation.

---

**Explanation**

Let's say `nums` looks like this: [12, 13, 14, 15, 16, 17, 18, 19, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

Because it's not fully sorted, we can't do normal binary search. But here comes the trick:

- If target is let's say 14, then we adjust `nums` to this, where "inf" means infinity:  
[12, 13, 14, 15, 16, 17, 18, 19, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf]  

- If target is let's say 7, then we adjust `nums` to this:  
[-inf, -inf, -inf, -inf, -inf, -inf, -inf, -inf, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

And then we can simply do ordinary binary search.

Of course we don't actually adjust the whole array but instead adjust only on the fly only the elements we look at. And the adjustment is done by comparing both the target and the actual element against nums[0].

---

**Code**

If `nums[mid]` and `target` are *"on the same side"* of `nums[0]`, we just take `nums[mid]`. Otherwise we use -infinity or +infinity as needed.

    int search(vector<int>& nums, int target) {
        int lo = 0, hi = nums.size();
        while (lo < hi) {
            int mid = (lo + hi) / 2;
            
            double num = (nums[mid] < nums[0]) == (target < nums[0])
                       ? nums[mid]
                       : target < nums[0] ? -INFINITY : INFINITY;
                       
            if (num < target)
                lo = mid + 1;
            else if (num > target)
                hi = mid;
            else
                return mid;
        }
        return -1;
    }
</p>


### Revised Binary Search
- Author: jerry13466
- Creation Date: Wed Jan 21 2015 23:30:52 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 07:26:37 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
    public int search(int[] A, int target) {
        int lo = 0;
        int hi = A.length - 1;
        while (lo < hi) {
            int mid = (lo + hi) / 2;
            if (A[mid] == target) return mid;
            
            if (A[lo] <= A[mid]) {
                if (target >= A[lo] && target < A[mid]) {
                    hi = mid - 1;
                } else {
                    lo = mid + 1;
                }
            } else {
                if (target > A[mid] && target <= A[hi]) {
                    lo = mid + 1;
                } else {
                    hi = mid - 1;
                }
            }
        }
        return A[lo] == target ? lo : -1;
    }
}
</p>


