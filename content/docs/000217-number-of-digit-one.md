---
title: "Number of Digit One"
weight: 217
#id: "number-of-digit-one"
---
## Description
<div class="description">
<p>Given an integer n, count the total number of digit 1 appearing in all non-negative integers less than or equal to n.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> 13
<strong>Output:</strong> 6 
<strong>Explanation: </strong>Digit 1 occurred in the following numbers: 1, 10, 11, 12, 13.
</pre>

</div>

## Tags
- Math (math)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach #1 Brute force [Time Limit Exceeded]

**Intuition**

Do as directed in question.

**Algorithm**

* Iterate over $$i$$ from $$1$$ to $$n$$:
  + Convert $$i$$ to string and count $$\text{'1'}$$ in each integer string
  + Add count of $$\text{'1'}$$ in each string to the sum, say $$countr$$


<iframe src="https://leetcode.com/playground/VwAzPgne/shared" frameBorder="0" name="VwAzPgne" width="100%" height="207"></iframe>

**Complexity Analysis**

* Time complexity: $$O(n*log_{10}(n))$$.
  + We iterate from $$1$$ to $$n$$
  + In each iteration, we convert integer to string and count '1' in string which takes linear time in number of digits in $$i$$, which is $$log_{10}(n)$$.

* Space complexity: $$O(log_{10}(n))$$ Extra space for the countr and the converted string $$\text{str}$$.

---
#### Approach #2 Solve it mathematically [Accepted]

**Intuition**

In Approach #1, we manually calculated the number of all the $$'1'$$s in the digits, but this is very slow. Hence, we need a way to find a pattern in the way $$'1'$$s (or for that matter any digit) appears in the numbers. We could then use the pattern to formulate the answer.

Consider the $$1$$s in $$\text{ones}$$ place , $$\text{tens}$$ place, $$\text{hundreds}$$ place and so on... An analysis
has been performed in the following figure.

![Number of digit one](../Figures/233/number_of_digit_one.png){:width="800px"}
{:align="center"}

From the figure, we can see that from digit '1' at $$\text{ones}$$ place repeat in group of 1 after interval of $$10$$. Similarly, '1' at $$\text{tens}$$ place repeat in group of 10 after interval of $$100$$.
This can be formulated as $$(n/(i*10))*i$$.

Also, notice that if the digit at $$\text{tens}$$ place is $$\text{'1'}$$, then the number of terms with $$\text{'1's}$$  is increased by $$x+1$$, if the number is say $$\text{"ab1x"}$$. As if digits at $$\text{tens}$$ place is greater than $$1$$, then all the $$10$$ occurances of numbers with $$'1'$$ at $$\text{tens}$$ place have taken place, hence, we add $$10$$.
This is formluated as $${\min(\max((\text{n mod (i*10)} )-i+1,0),i)}$$.

Lets take an example, say $$n= 1234$$.

No of $$\text{'1'}$$ in $$\text{ones}$$ place = $$1234/10$$(corresponding to 1,11,21,...1221) + $$\min(4,1)$$(corresponding to 1231) =$$124$$

No of $$\text{'1'}$$ in $$\text{tens}$$ place = $$(1234/100)*10$$(corresponding to 10,11,12,...,110,111,...1919) +$$\min(21,10)$$(corresponding to 1210,1211,...1219)=$$130$$

No of $$\text{'1'}$$ in $$\text{hundreds}$$ place = $$(1234/1000)*100$$(corresponding to 100,101,12,...,199) +$$\min(135,100)$$(corresponding to 1100,1101...1199)=$$200$$

No of $$\text{'1'}$$ in $$\text{thousands}$$ place = $$(1234/10000)*10000$$ +$$\min(235,1000)$$(corresponding to 1000,1001,...1234)=$$235$$

Therefore, Total = $$124+130+200+235 = 689$$.

Herein, one formula has been devised, but many other formulae can be devised for faster implementations, but the essence and complexity remains the same. The users are encouraged to try to devise their own version of solution using the mathematical concepts.

**Algorithm**

* Iterate over $$i$$ from $$1$$ to $$n$$ incrementing by $$10$$ each time:

    - Add  $$(n/(i*10))*i$$ to $$\text{countr}$$ representing the repetition of groups of $$i$$ sizes after each $$(i*10)$$ interval.

    - Add $${\min(\max((\text{n mod (i*10)} )-i+1,0),i)}$$ to $$\text{countr}$$ representing the additional digits dependant on the digit in $$i$$th place as described in intuition.

<iframe src="https://leetcode.com/playground/QVzpgtNB/shared" frameBorder="0" name="QVzpgtNB" width="100%" height="207"></iframe>

**Complexity analysis**

* Time complexity: $$O(log_{10}(n))$$.

  + No of iterations equal to the number of digits in n which is $$log_{10}(n)$$

* Space complexity: $$O(1)$$ space required.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 4+ lines, O(log n), C++/Java/Python
- Author: StefanPochmann
- Creation Date: Tue Jul 07 2015 23:16:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 21:06:59 GMT+0800 (Singapore Standard Time)

<p>
Go through the digit positions one at a time, find out how often a "1" appears at each position, and sum those up.

**C++ solution**

    int countDigitOne(int n) {
        int ones = 0;
        for (long long m = 1; m <= n; m *= 10)
            ones += (n/m + 8) / 10 * m + (n/m % 10 == 1) * (n%m + 1);
        return ones;
    }

**Explanation**

Let me use variables `a` and `b` to make the explanation a bit nicer.

    int countDigitOne(int n) {
        int ones = 0;
        for (long long m = 1; m <= n; m *= 10) {
            int a = n/m, b = n%m;
            ones += (a + 8) / 10 * m + (a % 10 == 1) * (b + 1);
        }
        return ones;
    }

Go through the digit positions by using position multiplier `m` with values 1, 10, 100, 1000, etc.

For each position, split the decimal representation into two parts, for example split n=3141592 into a=31415 and b=92 when we're at m=100 for analyzing the hundreds-digit. And then we know that the hundreds-digit of n is 1 for prefixes "" to "3141", i.e., 3142 times. Each of those times is a streak, though. Because it's the hundreds-digit, each streak is 100 long. So `(a / 10 + 1) * 100` times, the hundreds-digit is 1. 

Consider the thousands-digit, i.e., when m=1000. Then a=3141 and b=592. The thousands-digit is 1 for prefixes "" to "314", so 315 times. And each time is a streak of 1000 numbers. However, since the thousands-digit is a 1, the very last streak isn't 1000 numbers but only 593 numbers, for the suffixes "000" to "592". So `(a / 10 * 1000) + (b + 1)` times, the thousands-digit is 1.

The case distincton between the current digit/position being 0, 1 and >=2 can easily be done in one expression. With `(a + 8) / 10` you get the number of full streaks, and `a % 10 == 1` tells you whether to add a partial streak.

**Java version**

    public int countDigitOne(int n) {
        int ones = 0;
        for (long m = 1; m <= n; m *= 10)
            ones += (n/m + 8) / 10 * m + (n/m % 10 == 1 ? n%m + 1 : 0);
        return ones;
    }

**Python version**

    def countDigitOne(self, n):
        ones, m = 0, 1
        while m <= n:
            ones += (n/m + 8) / 10 * m + (n/m % 10 == 1) * (n%m + 1)
            m *= 10
        return ones

Using `sum` or recursion it can also be a [one-liner](https://leetcode.com/discuss/44302/1-liners-in-python).

---

Old solution
---

Go through the digit positions from back to front. I found it ugly to explain, so I made up that above new solution instead. The `n` here is the new solution's `a`, and the `r` here is the new solution's `b+1`.

**Python**

    def countDigitOne(self, n):
        ones = 0
        m = r = 1
        while n > 0:
            ones += (n + 8) / 10 * m + (n % 10 == 1) * r
            r += n % 10 * m
            m *= 10
            n /= 10
        return ones

**Java**

    public int countDigitOne(int n) {
        int ones = 0, m = 1, r = 1;
        while (n > 0) {
            ones += (n + 8) / 10 * m + (n % 10 == 1 ? r : 0);
            r += n % 10 * m;
            m *= 10;
            n /= 10;
        }
        return ones;
    }

**C++**

    int countDigitOne(int n) {
        int ones = 0, m = 1, r = 1;
        while (n > 0) {
            ones += (n + 8) / 10 * m + (n % 10 == 1) * r;
            r += n % 10 * m;
            m *= 10;
            n /= 10;
        }
        return ones;
    }
</p>


### AC short Java solution
- Author: jeantimex
- Creation Date: Fri Jul 17 2015 07:30:24 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 00:12:10 GMT+0800 (Singapore Standard Time)

<p>
    public int countDigitOne(int n) {
      int count = 0;
        
      for (long k = 1; k <= n; k *= 10) {
        long r = n / k, m = n % k;
        // sum up the count of ones on every place k
        count += (r + 8) / 10 * k + (r % 10 == 1 ? m + 1 : 0);
      }
        
      return count;
    }

Solution explanation:

Let's start by counting the ones for every 10 numbers...

0,  1,  2,  3  ... 9  (1)

**10, 11, 12, 13 ... 19** (1) + **10**

20, 21, 22, 23 ... 29 (1)

...

90, 91, 92, 93 ... 99 (1)

-

100, 101, 102, 103 ... 109 (10 + 1)

**110, 111, 112, 113 ... 119** (10 + 1) + **10**

120, 121, 122, 123 ... 129 (10 + 1)

...

190, 191, 192, 193 ... 199 (10 + 1)

-


**1).** If we don't look at those special rows (start with 10, 110 etc), we know that there's a one at ones' place in every 10 numbers, there are 10 ones at tens' place in every 100 numbers, and 100 ones at hundreds' place in every 1000 numbers, so on and so forth.

Ok, let's start with ones' place and count how many ones at this place, set k = 1, as mentioned above, there's a one at ones' place in every 10 numbers, so how many 10 numbers do we have? 

The answer is (n / k) / 10.

Now let's count the ones in tens' place, set k = 10, as mentioned above, there are 10 ones at tens' place in every 100 numbers, so how many 100 numbers do we have? 

The answer is (n / k) / 10, and the number of ones at ten's place is (n / k) / 10 * k.

Let r = n / k, now we have a formula to count the ones at k's place: **r / 10 * k**

-

**2).** So far, everything looks good, but we need to fix those special rows, how? 

We can use the mod. Take 10, 11, and 12 for example, if n is 10, we get (n / 1) / 10 * 1 = 1 ones at ones's place, perfect, but for tens' place, we get (n / 10) / 10 * 10 = 0, that's not right, there should be a one at tens' place! Calm down, from 10 to 19, we always have a one at tens's place, let m = n % k, the number of ones at this special place is m + 1, so let's fix the formula to be:


**r / 10 * k + (r % 10 == 1 ? m + 1 : 0)**

-


**3).** Wait, how about 20, 21 and 22? 

Let's say 20, use the above formula we get 0 ones at tens' place, but it should be 10! How to fix it? We know that once the digit is larger than 2, we should add 10 more ones to the tens' place, a clever way to fix is to add 8 to r, so our final formula is:

**(r + 8) / 10 * k + (r % 10 == 1 ? m + 1 : 0)**

As you can see, it's all about how we fix the formula. Really hope that makes sense to you.
</p>


### Java/Python one pass solution easy to understand
- Author: dietpepsi
- Creation Date: Tue Oct 20 2015 11:44:57 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 12:41:23 GMT+0800 (Singapore Standard Time)

<p>
The idea is to calculate occurrence of 1 on every digit. There are 3 scenarios, for example 

    if n = xyzdabc

and we are considering the occurrence of one on thousand, it should be:

    (1) xyz * 1000                     if d == 0
    (2) xyz * 1000 + abc + 1           if d == 1
    (3) xyz * 1000 + 1000              if d > 1

iterate through all digits and sum them all will give the final answer


**Java**

    public int countDigitOne(int n) {

        if (n <= 0) return 0;
        int q = n, x = 1, ans = 0;
        do {
            int digit = q % 10;
            q /= 10;
            ans += q * x;
            if (digit == 1) ans += n % x + 1;
            if (digit >  1) ans += x;
            x *= 10;
        } while (q > 0);
        return ans;

    }

    // 40 / 40 test cases passed.
    // Status: Accepted
    // Runtime: 0 ms

**Python**

    def countDigitOne(self, n):
        if n <= 0:
            return 0
        q, x, ans = n, 1, 0
        while q > 0:
            digit = q % 10
            q /= 10
            ans += q * x
            if digit == 1:
                ans += n % x + 1
            elif digit > 1:
                ans += x
            x *= 10
        return ans

    # 40 / 40 test cases passed.
    # Status: Accepted
    # Runtime: 32 ms
    # 97.59%
</p>


