---
title: "Swap Salary"
weight: 1511
#id: "swap-salary"
---
## Description
<div class="description">
<p>Given a table <code>salary</code>, such as the one below, that has m=male and f=female values. Swap all f and m values (i.e., change all f values to m and vice versa) with a <strong>single update statement</strong> and no intermediate temp table.</p>

<p>Note that you must write a single update statement, <strong>DO NOT</strong> write any select statement for this problem.</p>

<p>&nbsp;</p>

<p><strong>Example:</strong></p>

<pre>
| id | name | sex | salary |
|----|------|-----|--------|
| 1  | A    | m   | 2500   |
| 2  | B    | f   | 1500   |
| 3  | C    | m   | 5500   |
| 4  | D    | f   | 500    |
</pre>
After running your <strong>update</strong> statement, the above salary table should have the following rows:

<pre>
| id | name | sex | salary |
|----|------|-----|--------|
| 1  | A    | f   | 2500   |
| 2  | B    | m   | 1500   |
| 3  | C    | f   | 5500   |
| 4  | D    | m   | 500    |
</pre>

</div>

## Tags


## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach: Using `UPDATE` and `CASE...WHEN` [Accepted]

**Algorithm**

To dynamically set a value to a column, we can use [`UPDATE`](https://dev.mysql.com/doc/refman/5.7/en/update.html) statement together when [`CASE...WHEN...`](https://dev.mysql.com/doc/refman/5.7/en/case.html) flow control statement.

**MySQL**

```sql
UPDATE salary
SET
    sex = CASE sex
        WHEN 'm' THEN 'f'
        ELSE 'm'
    END;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accept solution with xor
- Author: newdev
- Creation Date: Fri Jun 23 2017 01:23:37 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 07:30:40 GMT+0800 (Singapore Standard Time)

<p>
```
update salary set sex = CHAR(ASCII('f') ^ ASCII('m') ^ ASCII(sex));
```
</p>


### Short and Simple
- Author: kevin36
- Creation Date: Mon Jun 19 2017 22:50:42 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 06:43:53 GMT+0800 (Singapore Standard Time)

<p>
```
UPDATE salary
    SET sex  = (CASE WHEN sex = 'm' 
        THEN  'f' 
        ELSE 'm' 
        END)
```
</p>


### Simple and short with IF
- Author: Alpher
- Creation Date: Fri Jun 23 2017 13:19:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 01 2018 16:14:01 GMT+0800 (Singapore Standard Time)

<p>
```
UPDATE salary SET sex = IF(sex = 'm', 'f', 'm')
```
</p>


