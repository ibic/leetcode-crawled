---
title: "Pascal's Triangle"
weight: 118
#id: "pascals-triangle"
---
## Description
<div class="description">
<p>Given a non-negative integer&nbsp;<em>numRows</em>, generate the first <em>numRows</em> of Pascal&#39;s triangle.</p>

<p><img alt="" src="https://upload.wikimedia.org/wikipedia/commons/0/0d/PascalTriangleAnimated2.gif" style="height:240px; width:260px" /><br />
<small>In Pascal&#39;s triangle, each number is the sum of the two numbers directly above it.</small></p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> 5
<strong>Output:</strong>
[
     [1],
    [1,1],
   [1,2,1],
  [1,3,3,1],
 [1,4,6,4,1]
]
</pre>

</div>

## Tags
- Array (array)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach 1: Dynamic Programming

**Intuition**

If we have the a row of Pascal's triangle, we can easily compute the next
row by each pair of adjacent values.

**Algorithm**

Although the algorithm is very simple, the iterative approach to constructing
Pascal's triangle can be classified as dynamic programming because we
construct each row based on the previous row.

First, we generate the overall `triangle` list, which will store each row as
a sublist. Then, we check for the special case of $$0$$, as we would otherwise
return $$[1]$$. If $$numRows > 0$$, then we initialize `triangle` with $$[1]$$
as its first row, and proceed to fill the rows as follows:

!?!../Documents/118_Pascals_Triangle.json:1280,720!?!

<iframe src="https://leetcode.com/playground/idrxbCSN/shared" frameBorder="0" width="100%" height="500" name="idrxbCSN"></iframe>

**Complexity Analysis**

* Time complexity : $$O(numRows^2)$$

    Although updating each value of `triangle` happens in constant time, it
    is performed $$O(numRows^2)$$ times. To see why, consider how many
    overall loop iterations there are. The outer loop obviously runs
    $$numRows$$ times, but for each iteration of the outer loop, the inner
    loop runs $$rowNum$$ times. Therefore, the overall number of `triangle` updates
    that occur is $$1 + 2 + 3 + \ldots + numRows$$, which, according to Gauss' formula,
    is

    $$
    \begin{aligned}
        \frac{numRows(numRows+1)}{2} &= \frac{numRows^2 + numRows}{2} \\
        &= \frac{numRows^2}{2} + \frac{numRows}{2} \\
        &= O(numRows^2)
    \end{aligned}
    $$

* Space complexity : $$O(numRows^2)$$

    Because we need to store each number that we update in `triangle`, the
    space requirement is the same as the time complexity.

## Accepted Submission (ruby)
```ruby
# @param {Integer} num_rows
# @return {Integer[][]}
def generate(num_rows)
    r = []
    prevrow = []
    0.upto(num_rows - 1) do |i|
        row = Array.new(i + 1)
        row[0] = 1
        row[i] = 1
        1.upto(i - 1) do |j|
            row[j] = prevrow[j - 1] + prevrow[j]
        end
        r.push(row)
        prevrow = row
    end
    return r  
end
```

## Top Discussions
### My concise solution in Java
- Author: rheaxu
- Creation Date: Tue Jan 06 2015 04:18:57 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 01:07:39 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
    public List<List<Integer>> generate(int numRows)
	{
		List<List<Integer>> allrows = new ArrayList<List<Integer>>();
		ArrayList<Integer> row = new ArrayList<Integer>();
		for(int i=0;i<numRows;i++)
		{
			row.add(0, 1);
			for(int j=1;j<row.size()-1;j++)
				row.set(j, row.get(j)+row.get(j+1));
			allrows.add(new ArrayList<Integer>(row));
		}
		return allrows;
		
	}
}
</p>


### Python 4 lines short solution using map.
- Author: sherlock321
- Creation Date: Thu Aug 27 2015 10:43:44 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 16:18:51 GMT+0800 (Singapore Standard Time)

<p>
    def generate(self, numRows):
            res = [[1]]
            for i in range(1, numRows):
                res += [map(lambda x, y: x+y, res[-1] + [0], [0] + res[-1])]
            return res[:numRows]


explanation: Any row can be constructed using the offset sum of the previous row. Example:

        
        1 3 3 1 0 
     +  0 1 3 3 1
     =  1 4 6 4 1
</p>


### Maybe shortest c++ solution
- Author: mzchen
- Creation Date: Wed Oct 22 2014 23:36:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 13:45:47 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        vector<vector<int> > generate(int numRows) {
            vector<vector<int>> r(numRows);
    
            for (int i = 0; i < numRows; i++) {
                r[i].resize(i + 1);
                r[i][0] = r[i][i] = 1;
      
                for (int j = 1; j < i; j++)
                    r[i][j] = r[i - 1][j - 1] + r[i - 1][j];
            }
            
            return r;
        }
    };
</p>


