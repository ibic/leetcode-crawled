---
title: "Stamping The Sequence"
weight: 886
#id: "stamping-the-sequence"
---
## Description
<div class="description">
<p>You want to form a <code>target</code>&nbsp;string of <strong>lowercase letters</strong>.</p>

<p>At the beginning, your sequence is <code>target.length</code>&nbsp;<code>&#39;?&#39;</code> marks.&nbsp; You also have a <code>stamp</code>&nbsp;of lowercase letters.</p>

<p>On each turn, you may place the stamp over the sequence, and replace every letter in the sequence with the corresponding letter from the stamp.&nbsp; You can make up to <code>10 * target.length</code> turns.</p>

<p>For example, if the initial sequence is <font face="monospace">&quot;?????&quot;</font>, and your stamp is <code>&quot;abc&quot;</code>,&nbsp; then you may make <font face="monospace">&quot;abc??&quot;, &quot;?abc?&quot;, &quot;??abc&quot;&nbsp;</font>in the first turn.&nbsp; (Note that the stamp must be fully contained in the boundaries of the sequence in order to stamp.)</p>

<p>If the sequence is possible to stamp, then return an array of&nbsp;the index of the left-most letter being stamped at each turn.&nbsp; If the sequence is not possible to stamp, return an empty array.</p>

<p>For example, if the sequence is <font face="monospace">&quot;ababc&quot;</font>, and the stamp is <code>&quot;abc&quot;</code>, then we could return the answer <code>[0, 2]</code>, corresponding to the moves <font face="monospace">&quot;?????&quot; -&gt; &quot;abc??&quot; -&gt; &quot;ababc&quot;</font>.</p>

<p>Also, if the sequence is possible to stamp, it is guaranteed it is possible to stamp within <code>10 * target.length</code>&nbsp;moves.&nbsp; Any answers specifying more than this number of moves&nbsp;will not be accepted.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>stamp = <span id="example-input-1-1">&quot;abc&quot;</span>, target = <span id="example-input-1-2">&quot;ababc&quot;</span>
<strong>Output: </strong><span id="example-output-1">[0,2]</span>
([1,0,2] would also be accepted as an answer, as well as some other answers.)
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>stamp = <span id="example-input-2-1">&quot;</span><span id="example-input-2-2">abca</span><span>&quot;</span>, target = <span id="example-input-2-2">&quot;</span><span>aabcaca&quot;</span>
<strong>Output: </strong><span id="example-output-2">[3,0,1]</span>
</pre>

<div>
<p>&nbsp;</p>

<p><strong>Note:</strong></p>
</div>
</div>

<ol>
	<li><code>1 &lt;= stamp.length &lt;= target.length &lt;= 1000</code></li>
	<li><code>stamp</code> and <code>target</code> only contain lowercase letters.</li>
</ol>
</div>

## Tags
- String (string)
- Greedy (greedy)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Work Backwards

**Intuition**

Imagine we stamped the sequence with moves $$m_1, m_2, \cdots$$.  Now, from the final position `target`, we will make those moves in reverse order.  

Let's call the `i`th *window*, a subarray of `target` of length `stamp.length` that starts at `i`.  Each move at position `i` is possible if the `i`th window matches the stamp.  After, every character in the window becomes a wildcard that can match any character in the stamp.

For example, say we have `stamp = "abca"` and `target = "aabcaca"`.  Working backwards, we will reverse stamp at window `1` to get `"a????ca"`, then reverse stamp at window `3` to get `"a??????"`, and finally reverse stamp at position `0` to get `"???????"`.

**Algorithm**

Let's keep track of every window.  We want to know how many cells initially match the stamp (our "`made`" list), and which ones don't (our `"todo"` list).  Any windows that are ready (ie. have no todo list), get enqueued.

Specifically, we enqueue the positions of each character.  (To save time, we enqueue by character, not by window.)  This represents that the character is ready to turn into a `"?"` in our working `target` string.

Now, how to process characters in our queue?  For each character, let's look at all the windows that intersect it, and update their todo lists.  If any todo lists become empty in this manner `(window.todo is empty)`, then we enqueue the characters in `window.made` that we haven't processed yet.

<iframe src="https://leetcode.com/playground/4bQYqsNK/shared" frameBorder="0" width="100%" height="500" name="4bQYqsNK"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N(N-M))$$, where $$M, N$$ are the lengths of `stamp`, `target`.

* Space Complexity:  $$O(N(N-M))$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 12ms Java Solution Beats 100%
- Author: FLAGbigoffer
- Creation Date: Tue Dec 04 2018 14:46:04 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 04 2018 14:46:04 GMT+0800 (Singapore Standard Time)

<p>
The idea is the same as the most upvoted post. Think reversely!
Let\'s take an example to illustrate.
`Stamp` = "abc", `Target` = "ababcbc`
1. Target = `ab***bc`
2. Target = `ab*****`
3. Target = `*******`

We will go through the whole `Target` string to find if there exists any substring equals to `Stamp`.  And then replace the whole substring with `*`. You can see in the step `1`, we replace substring `abc` with `***`. Then we keep doing the same thing. In the step `2`, you will find we replace substring `*bc` to `***`. `*` can match to any character because `*` will be override by the next stamp. Finally we get the result and output the reversed result. When `# of stars` equals to `target.length()`, we will return the result. If during one scan, we are not able to replace even one substring with `*`, directly return empty array.

`Comments` for two helper functions:
`canReplace(char[] T, int p, char[] S)` is used to check if any substring from Target is existing to be replaced with `*`.
`doReplace(char[] T, int p, int len, int count)` is used to replace the substring with `*` and return the total number of `*` we have now.


```
class Solution {
    public int[] movesToStamp(String stamp, String target) {
        char[] S = stamp.toCharArray();
        char[] T = target.toCharArray();
        List<Integer> res = new ArrayList<>();
        boolean[] visited = new boolean[T.length];
        int stars = 0;
        
        while (stars < T.length) {
            boolean doneReplace = false;
            for (int i = 0; i <= T.length - S.length; i++) {
                if (!visited[i] && canReplace(T, i, S)) {
                    stars = doReplace(T, i, S.length, stars);
                    doneReplace = true;
                    visited[i] = true;
                    res.add(i);
                    if (stars == T.length) {
                        break;
                    }
                }
            }
            
            if (!doneReplace) {
                return new int[0];
            }
        }
        
        int[] resArray = new int[res.size()];
        for (int i = 0; i < res.size(); i++) {
            resArray[i] = res.get(res.size() - i - 1);
        }
        return resArray;
    }
    
    private boolean canReplace(char[] T, int p, char[] S) {
        for (int i = 0; i < S.length; i++) {
            if (T[i + p] != \'*\' && T[i + p] != S[i]) {
                return false;
            }
        }
        return true;
    }
    
    private int doReplace(char[] T, int p, int len, int count) {
        for (int i = 0; i < len; i++) {
            if (T[i + p] != \'*\') {
                T[i + p] = \'*\';
                count++;
            }
        }
        return count;
    } 
}
```
</p>


### C++ simple greedy
- Author: votrubac
- Creation Date: Mon Nov 05 2018 04:00:19 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 05 2018 04:00:19 GMT+0800 (Singapore Standard Time)

<p>
It\'s better explained through an example. Let\'s say the target is ```\'aabccbc\'```, and stamp is ```\'abc\'```. We first try to find ```\'abc\'``` and replace it with ```\'***\'```. After there are no more replacements, we will try ```\'*bc\'``` and ```\'ab*\'```, and so on. Now, turn by turn:
```
\'aabccbc\' ? \'abc\' = [1]
\'a***cbc\' ? \'*bc\' = []
\'a***cbc\' ? \'ab*\' = []
\'a***cbc\' ? \'a**\' = [0]
\'****cbc\' ? \'*b*\' = []
\'****cbc\' ? \'**c\' = [2]
\'*****bc\' ? \'*bc\' = [4]
```
The result is: [4, 2, 0, 1]. It feels to me that this greedy solution produces the minumum possible number of stamps, though I cannot provide a strict proof :)
```
vector<int> movesToStamp(string stamp, string target) {
  vector<int> res;
  auto total_stamp = 0, turn_stamp = -1;
  while (turn_stamp) {
      turn_stamp = 0;
      for (int sz = stamp.size(); sz > 0; --sz) 
          for (auto i = 0; i <= stamp.size() - sz; ++i) {
              auto new_stamp = string(i, \'*\') + stamp.substr(i, sz) + string(stamp.size() - sz - i, \'*\');
              auto pos = target.find(new_stamp);
              while (pos != string::npos) {
                  res.push_back(pos);
                  turn_stamp += sz;
                  fill(begin(target) + pos, begin(target) + pos + stamp.size(), \'*\');
                  pos = target.find(new_stamp);
              }
          }
      total_stamp += turn_stamp;
  }
  reverse(begin(res), end(res));
  return total_stamp == target.size() ? res : vector<int>();
}
```
</p>


### [Python] Greedy and DFS
- Author: lee215
- Creation Date: Sun Nov 04 2018 11:05:07 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 23 2019 17:38:05 GMT+0800 (Singapore Standard Time)

<p>

# Solution1 Greedy

Reversely change from target to `????....???` whenever we find a matched stamp substring.

```
    def movesToStamp(self, s, t):
        n, m, t, s, res = len(t), len(s), list(t), list(s), []

        def check(i):
            changed = False
            for j in range(m):
                if t[i + j] == \'?\': continue
                if t[i + j] != s[j]: return False
                changed = True
            if changed:
                t[i:i + m] = [\'?\'] * m
                res.append(i)
            return changed

        changed = True
        while changed:
            changed = False
            for i in range(n - m + 1):
                changed |= check(i)
        return res[::-1] if t == [\'?\'] * n else []
```

<br/>

# Solution 2 DFS
Another idea is backtracking. 


**Step 1:**

Try to find a path of `target`,
where `path[i]` equals to index of `target[i]` in `stamp`

Example 1:
Input: stamp = "abc", target = "ababc"
path = [0,1,0,1,2]


Example 2:
Input:  stamp = "abca", target = "aabcaca"
path = [0,0,1,2,3,2,3]

The rule is that,
rule 0. `path[i + 1]` can equal to `path[i] + 1`
It means target[i] and target[i+1] are on the same stamp.


rule 1. `path[i + 1]` can equal to `0`.
It means `t[i + 1]` is the start of another stamp

rule 2. if `path[i] == stamp.size - 1`, we reach the end of a stamp.
Under this stamp, it\'s another stamp, but not necessary the start.
`path[i + 1]` can equal to `0 ~ stamp.size - 1`.


**Step 2:**
We need to change path to required moves.
This can be a medium problem on the Leetcode.



```
    def movesToStamp(self, s, t):
        if s[0] != t[0] or s[-1] != t[-1]: return []
        n, m = len(s), len(t)
        path = [0] * m
        pos = collections.defaultdict(set)
        for i, c in enumerate(s): pos[c].add(i)

        def dfs(i, index):
            path[i] = index
            if i == m - 1: return index == n - 1
            nxt_index = set()
            if index == n - 1:  # rule 2
                nxt_index |= pos[t[i + 1]]
            elif s[index + 1] == t[i + 1]:  # rule 0
                nxt_index.add(index + 1)
            if s[0] == t[i + 1]:  # rule 1
                nxt_index.add(0)
            return any(dfs(i + 1, j) for j in nxt_index)

        def path2res(path):
            down, up = [], []
            for i in range(len(path)):
                if path[i] == 0:
                    up.append(i)
                elif i and path[i] - 1 != path[i - 1]:
                    down.append(i - path[i])
            return down[::-1] + up

        if not dfs(0, 0): return []
        return path2res(path)
</p>


