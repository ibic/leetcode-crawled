---
title: "Delete Node in a BST"
weight: 427
#id: "delete-node-in-a-bst"
---
## Description
<div class="description">
<p>Given a root node reference of a BST and a key, delete the node with the given key in the BST. Return the root node reference (possibly updated) of the BST.</p>

<p>Basically, the deletion can be divided into two stages:</p>

<ol>
	<li>Search for a node to remove.</li>
	<li>If the node is found, delete the node.</li>
</ol>

<p><b>Follow up:</b>&nbsp;Can you solve it with time complexity <code>O(height of tree)</code>?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/04/del_node_1.jpg" style="width: 800px; height: 214px;" />
<pre>
<strong>Input:</strong> root = [5,3,6,2,4,null,7], key = 3
<strong>Output:</strong> [5,4,6,2,null,null,7]
<strong>Explanation:</strong> Given key to delete is 3. So we find the node with value 3 and delete it.
One valid answer is [5,4,6,2,null,null,7], shown in the above BST.
Please notice that another valid answer is [5,2,6,null,4,null,7] and it&#39;s also accepted.
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/04/del_node_supp.jpg" style="width: 350px; height: 255px;" />
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = [5,3,6,2,4,null,7], key = 0
<strong>Output:</strong> [5,3,6,2,4,null,7]
<strong>Explanation:</strong> The tree does not contain a node with value = 0.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [], key = 0
<strong>Output:</strong> []
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is in the range <code>[0, 10<sup>4</sup>]</code>.</li>
	<li><code>-10<sup>5</sup> &lt;= Node.val &lt;= 10<sup>5</sup></code></li>
	<li>Each node has a <strong>unique</strong> value.</li>
	<li><code>root</code> is a valid binary search tree.</li>
	<li><code>-10<sup>5</sup> &lt;= key &lt;= 10<sup>5</sup></code></li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Microsoft - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Oracle - 6 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Uber - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Three facts to know about BST

Here is list of facts which are better to know before the interview.

> Inorder traversal of BST is an array sorted in the ascending order.

To compute inorder traversal follow the direction `Left -> Node -> Right`.

<iframe src="https://leetcode.com/playground/2WmR9ivr/shared" frameBorder="0" width="100%" height="174" name="2WmR9ivr"></iframe>

![traversal](../Figures/450/450_inorder.png)

> Successor = "after node", i.e. the next node, or the smallest node _after_ the current one.  

It's also the _next_ node in the inorder traversal. To find a successor, go to the right once
and then as many times to the left as you could.

<iframe src="https://leetcode.com/playground/AJKdScMA/shared" frameBorder="0" width="100%" height="140" name="AJKdScMA"></iframe>

> Predecessor = "before node", i.e. the previous node, or the largest node _before_ the current one.  

It's also the _previous_ node in the inorder traversal. To find a predecessor, go to the left once
and then as many times to the right as you could.

<iframe src="https://leetcode.com/playground/Ua5dJLMe/shared" frameBorder="0" width="100%" height="140" name="Ua5dJLMe"></iframe>

![bla](../Figures/450/succ2.png)

<br />
<br />


---
#### Approach 1: Recursion

**Intuition**

There are three possible situations here :

- Node is a leaf, and one could delete it straightforward : `node = null`.

![bla](../Figures/450/del_leaf.png)

- Node is not a leaf and has a right child. Then the node could be replaced by its
_successor_ which is somewhere lower in the right subtree. Then one could proceed down recursively to
delete the successor.

![bla](../Figures/450/del_succ.png)

- Node is not a leaf, has no right child and has a left child. That means that its _successor_ is somewhere
 upper in the tree but we don't want to go back.
 Let's use the _predecessor_ here which is somewhere lower in the left subtree.
 The node could be replaced by its
_predecessor_ and then one could proceed down recursively to delete the predecessor.

![bla](../Figures/450/del_pred.png)

**Algorithm**

- If `key > root.val` then delete the node to delete is in the right subtree
`root.right = deleteNode(root.right, key)`.

- If `key < root.val` then delete the node to delete is in the left subtree
`root.left = deleteNode(root.left, key)`.  

- If `key == root.val` then the node to delete is right here. Let's do it :

    - If the node is a leaf, the delete process is straightforward :
    `root = null`.
    
    - If the node is not a leaf and has the right child, then replace the node 
    value by a successor value `root.val = successor.val`, 
    and then recursively delete
    the successor in the right subtree `root.right = deleteNode(root.right, root.val)`.
    
    - If the node is not a leaf and has only the left child, then replace the node 
    value by a predecessor value `root.val = predecessor.val`, and then recursively delete
    the predecessor in the left subtree `root.left = deleteNode(root.left, root.val)`.
    
- Return `root`.

**Implementation**

![bla](../Figures/450/implem2.png)

<iframe src="https://leetcode.com/playground/NmG3cUrn/shared" frameBorder="0" width="100%" height="500" name="NmG3cUrn"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(\log N)$$. During the algorithm 
execution we go down the tree
all the time - on the left or on the right, 
first to search the node to delete ($$\mathcal{O}(H_1)$$ time
complexity as already 
[discussed](https://leetcode.com/articles/insert-into-a-bst/)) 
and then to actually delete it. $$H_1$$ is a tree height from the root to the node 
to delete.
Delete process takes $$\mathcal{O}(H_2)$$ time, where $$H_2$$ is a tree height
from the root to delete to the leafs.
That in total results in $$\mathcal{O}(H_1 + H_2) = \mathcal{O}(H)$$ time
complexity, where $$H$$ is a tree height, equal to $$\log N$$ in the case of the balanced tree.    
    
* Space complexity : $$\mathcal{O}(H)$$ to keep the recursion stack, where $$H$$ is a tree
height. $$H = \log N$$ for the balanced tree.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Recursive Easy to Understand Java Solution
- Author: booboohsu
- Creation Date: Tue Nov 01 2016 13:55:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 08:05:05 GMT+0800 (Singapore Standard Time)

<p>
Steps:
1. Recursively find the node that has the same value as the key, while setting the left/right nodes equal to the returned subtree
2. Once the node is found, have to handle the below 4 cases
* node doesn't have left or right - return null
* node only has left subtree- return the left subtree
* node only has right subtree- return the right subtree
* node has both left and right - find the minimum value in the right subtree, set that value to the currently found node, then recursively delete the minimum value in the right subtree

```
public TreeNode deleteNode(TreeNode root, int key) {
    if(root == null){
        return null;
    }
    if(key < root.val){
        root.left = deleteNode(root.left, key);
    }else if(key > root.val){
        root.right = deleteNode(root.right, key);
    }else{
        if(root.left == null){
            return root.right;
        }else if(root.right == null){
            return root.left;
        }
        
        TreeNode minNode = findMin(root.right);
        root.val = minNode.val;
        root.right = deleteNode(root.right, root.val);
    }
    return root;
}

private TreeNode findMin(TreeNode node){
    while(node.left != null){
        node = node.left;
    }
    return node;
}
```
</p>


### Iterative solution in Java, O(h) time and O(1) space
- Author: eaglesky
- Creation Date: Tue Nov 15 2016 13:28:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 04:01:13 GMT+0800 (Singapore Standard Time)

<p>
```
    private TreeNode deleteRootNode(TreeNode root) {
        if (root == null) {
            return null;
        }
        if (root.left == null) {
            return root.right;
        }
        if (root.right == null) {
            return root.left;
        }
        TreeNode next = root.right;
        TreeNode pre = null;
        for(; next.left != null; pre = next, next = next.left);
        next.left = root.left;
        if(root.right != next) {
            pre.left = next.right;
            next.right = root.right;
        }
        return next;
    }
    
    public TreeNode deleteNode(TreeNode root, int key) {
        TreeNode cur = root;
        TreeNode pre = null;
        while(cur != null && cur.val != key) {
            pre = cur;
            if (key < cur.val) {
                cur = cur.left;
            } else if (key > cur.val) {
                cur = cur.right;
            }
        }
        if (pre == null) {
            return deleteRootNode(cur);
        }
        if (pre.left == cur) {
            pre.left = deleteRootNode(cur);
        } else {
            pre.right = deleteRootNode(cur);
        }
        return root;
    }
```
Find the node to be removed and its parent using binary search, and then use deleteRootNode to delete the root node of the subtree and return the new root node. This idea is taken from https://discuss.leetcode.com/topic/67309/an-easy-understanding-o-h-time-o-1-space-java-solution.

I'd also like to share my thinkings of the other solutions I've seen. 
1. There are many solutions that got high votes using recursive approach,  including the ones from the Princeton's Algorithm and Data Structure book. Don't you notice that recursive approach always takes extra space? Why not consider the iterative approach first?
2. Some solutions swap the values instead of swapping the nodes. In reality, the value of a node could be more complicated than just a single integer, so copying the contents might take much more time than just copying the reference. 
3. As for the case when both children of the node to be deleted are not null, I transplant the successor to replace the node to be deleted, which is a bit harder to implement than just transplant the left subtree of the node to the left child of its successor.  The former way is used in many text books too. Why? My guess is that transplanting the successor can keep the height of the tree almost unchanged, while transplanting the whole left subtree could increase the height and thus making the tree more unbalanced.
</p>


### Simple Python Solution With Explanation
- Author: Yu_shuxin
- Creation Date: Tue Nov 15 2016 22:50:28 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 19:55:35 GMT+0800 (Singapore Standard Time)

<p>
```
   def deleteNode(root, key):
	if not root: # if root doesn't exist, just return it
		return root
	if root.val > key: # if key value is less than root value, find the node in the left subtree
		root.left = deleteNode(root.left, key)
	elif root.val < key: # if key value is greater than root value, find the node in right subtree
		root.right= deleteNode(root.right, key)
	else: #if we found the node (root.value == key), start to delete it
		if not root.right: # if it doesn't have right children, we delete the node then new root would be root.left
			return root.left
		if not root.left: # if it has no left children, we delete the node then new root would be root.right
			return root.right
               # if the node have both left and right children,  we replace its value with the minmimum value in the right subtree and then delete that minimum node in the right subtree
		temp = root.right
		mini = temp.val
		while temp.left:
			temp = temp.left
			mini = temp.val
		root.val = mini # replace value
		root.right = deleteNode(root.right,root.val) # delete the minimum node in right subtree
	return root

```
</p>


