---
title: "Longest Mountain in Array"
weight: 789
#id: "longest-mountain-in-array"
---
## Description
<div class="description">
<p>Let&#39;s call any (contiguous) subarray B (of A)&nbsp;a <em>mountain</em> if the following properties hold:</p>

<ul>
	<li><code>B.length &gt;= 3</code></li>
	<li>There exists some <code>0 &lt; i&nbsp;&lt; B.length - 1</code> such that <code>B[0] &lt; B[1] &lt; ... B[i-1] &lt; B[i] &gt; B[i+1] &gt; ... &gt; B[B.length - 1]</code></li>
</ul>

<p>(Note that B could be any subarray of A, including the entire array A.)</p>

<p>Given an array <code>A</code>&nbsp;of integers,&nbsp;return the length of the longest&nbsp;<em>mountain</em>.&nbsp;</p>

<p>Return <code>0</code> if there is no mountain.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>[2,1,4,7,3,2,5]
<strong>Output: </strong>5
<strong>Explanation: </strong>The largest mountain is [1,4,7,3,2] which has length 5.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>[2,2,2]
<strong>Output: </strong>0
<strong>Explanation: </strong>There is no mountain.
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= A.length &lt;= 10000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 10000</code></li>
</ol>

<p><strong>Follow up:</strong></p>

<ul>
	<li>Can you solve it using only one pass?</li>
	<li>Can you solve it in <code>O(1)</code> space?</li>
</ul>

</div>

## Tags
- Two Pointers (two-pointers)

## Companies
- Microsoft - 2 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: true)
- Wish - 7 (taggedByAdmin: false)
- Cohesity - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Two Pointer [Accepted]

**Intuition**

Without loss of generality, a mountain can only start after the previous one ends.

This is because if it starts before the peak, it will be smaller than a mountain starting previous; and it is impossible to start after the peak.

**Algorithm**

For a starting index `base`, let's calculate the length of the longest mountain `A[base], A[base+1], ..., A[end]`.

If such a mountain existed, the next possible mountain will start at `base = end`; if it didn't, then either we reached the end, or we have `A[base] > A[base+1]` and we can start at `base + 1`.

**Example**

Here is a worked example on the array `A = [1, 2, 3, 2, 1, 0, 2, 3, 1]`:

<center>
    <img src="../Figures/845/diagram1.png" alt="Worked example of A = [1,2,3,2,1,0,2,3,1]" style="height: 150px"/>
</center>

<br>

`base` starts at `0`, and `end` travels using the first while loop to `end = 2` (`A[end] = 3`), the potential peak of this mountain.  After, it travels to `end = 5` (`A[end] = 0`) during the second while loop, and a candidate answer of 6 `(base = 0, end = 5)` is recorded.

Afterwards, base is set to `5` and the process starts over again, with `end = 7` the peak of the mountain, and `end = 8` the right boundary, and the candidate answer of 4 `(base = 5, end = 8)` being recorded.

<iframe src="https://leetcode.com/playground/hJvxZrng/shared" frameBorder="0" width="100%" height="500" name="hJvxZrng"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] 1-pass and O(1) space
- Author: lee215
- Creation Date: Sun Jun 03 2018 11:02:05 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 22:59:49 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**:
We have already many 2-pass or 3-pass problems, like 821. Shortest Distance to a Character.
They have almost the same idea.
One forward pass and one backward pass.
Maybe another pass to get the final result, or you can merge it in one previous pass.

**Explanation**:
In this problem, we take one forward pass to count up hill length (to every point).
We take another backward pass to count down hill length (from every point).
Finally a pass to find max(up[i] + down[i] + 1) where up[i] and down[i] should be positives.


**Time Complexity**:
O(N)

**C++:**
```
    int longestMountain(vector<int> A) {
        int N = A.size(), res = 0;
        vector<int> up(N, 0), down(N, 0);
        for (int i = N - 2; i >= 0; --i) if (A[i] > A[i + 1]) down[i] = down[i + 1] + 1;
        for (int i = 0; i < N; ++i) {
            if (i > 0 && A[i] > A[i - 1]) up[i] = up[i - 1] + 1;
            if (up[i] && down[i]) res = max(res, up[i] + down[i] + 1);
        }
        return res;
    }
```

**Java:**
```
    public int longestMountain(int[] A) {
        int N = A.length, res = 0;
        int[] up = new int[N], down = new int[N];
        for (int i = N - 2; i >= 0; --i) if (A[i] > A[i + 1]) down[i] = down[i + 1] + 1;
        for (int i = 0; i < N; ++i) {
            if (i > 0 && A[i] > A[i - 1]) up[i] = up[i - 1] + 1;
            if (up[i] > 0 && down[i] > 0) res = Math.max(res, up[i] + down[i] + 1);
        }
        return res;
    }
```
**Python:**
```
    def longestMountain(self, A):
        up, down = [0] * len(A), [0] * len(A)
        for i in range(1, len(A)):
            if A[i] > A[i - 1]: up[i] = up[i - 1] + 1
        for i in range(len(A) - 1)[::-1]:
            if A[i] > A[i + 1]: down[i] = down[i + 1] + 1
        return max([u + d + 1 for u, d in zip(up, down) if u and d] or [0])
```

**Follow up**

Can you solve this problem with only one pass?
Can you solve this problem in O(1) space?

In this solution, I count up length and down length.
Both up and down length are clear to 0 when `A[i - 1] == A[i]` or `down > 0 && A[i - 1] < A[i]`.

**C++:**
```
    int longestMountain(vector<int> A) {
        int res = 0, up = 0, down = 0;
        for (int i = 1; i < A.size(); ++i) {
            if (down && A[i - 1] < A[i] || A[i - 1] == A[i]) up = down = 0;
            up += A[i - 1] < A[i];
            down += A[i - 1] > A[i];
            if (up && down) res = max(res, up + down + 1);
        }
        return res;
    }
```

**Java:**
```
    public int longestMountain(int[] A) {
        int res = 0, up = 0, down = 0;
        for (int i = 1; i < A.length; ++i) {
            if (down > 0 && A[i - 1] < A[i] || A[i - 1] == A[i]) up = down = 0;
            if (A[i - 1] < A[i]) up++;
            if (A[i - 1] > A[i]) down++;
            if (up > 0 && down > 0 && up + down + 1 > res) res = up + down + 1;
        }
        return res;
    }
```
**Python:**
```
    def longestMountain(self, A):
        res = up = down = 0
        for i in range(1, len(A)):
            if down and A[i - 1] < A[i] or A[i - 1] == A[i]: up = down = 0
            up += A[i - 1] < A[i]
            down += A[i - 1] > A[i]
            if up and down: res = max(res, up + down + 1)
        return res
```

</p>


### 1-pass Java Two Point Solution
- Author: vincent_hou
- Creation Date: Sun Sep 02 2018 02:04:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 12:01:20 GMT+0800 (Singapore Standard Time)

<p>
```
public int longestMountain(int[] A) {
        int n=A.length;
        if(n<3)return 0;
        
        int left=0;int right;int max=0;
        
        while(left<n-2){
            //skip decending and equal array
            while(left<n-1 && A[left]>=A[left+1]){
                left++;
            }
            right=left+1;
            //mountain up
            while(right<n-1 && A[right]<A[right+1]){
                right++;
            }
            //mountain down
            while(right<n-1 && A[right]>A[right+1]){
                right++;
                //update the max value
                max=Math.max(max,right-left+1);
            }
            left=right;
        }
        return max;
    }
```
</p>


### Python 7 lines concise O(N) time O(1) space solution
- Author: cenkay
- Creation Date: Sun Jun 03 2018 11:16:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 03 2018 11:16:30 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def longestMountain(self, A, res = 0):
        for i in range(1, len(A) - 1):
            if A[i + 1] < A[i] > A[i - 1]:
                l = r = i
                while l and A[l] > A[l - 1]: l -= 1
                while r + 1 < len(A) and A[r] > A[r + 1]: r += 1
                if r - l + 1 > res: res = r - l + 1
        return res
```
</p>


