---
title: "Number of Islands II"
weight: 288
#id: "number-of-islands-ii"
---
## Description
<div class="description">
<p>A 2d grid map of <code>m</code> rows and <code>n</code> columns is initially filled with water. We may perform an <i>addLand</i> operation which turns the water at position (row, col) into a land. Given a list of positions to operate, <b>count the number of islands after each <i>addLand</i> operation</b>. An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are all surrounded by water.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b> m = 3, n = 3, positions = [[0,0], [0,1], [1,2], [2,1]]
<b>Output:</b> [1,1,2,3]
</pre>

<p><b>Explanation:</b></p>

<p>Initially, the 2d grid <code>grid</code> is filled with water. (Assume 0 represents water and 1 represents land).</p>

<pre>
0 0 0
0 0 0
0 0 0
</pre>

<p>Operation #1: addLand(0, 0) turns the water at grid[0][0] into a land.</p>

<pre>
1 0 0
0 0 0   Number of islands = 1
0 0 0
</pre>

<p>Operation #2: addLand(0, 1) turns the water at grid[0][1] into a land.</p>

<pre>
1 1 0
0 0 0   Number of islands = 1
0 0 0
</pre>

<p>Operation #3: addLand(1, 2) turns the water at grid[1][2] into a land.</p>

<pre>
1 1 0
0 0 1   Number of islands = 2
0 0 0
</pre>

<p>Operation #4: addLand(2, 1) turns the water at grid[2][1] into a land.</p>

<pre>
1 1 0
0 0 1   Number of islands = 3
0 1 0
</pre>

<p><b>Follow up:</b></p>

<p>Can you do it in time complexity O(k log mn), where k is the length of the <code>positions</code>?</p>

</div>

## Tags
- Union Find (union-find)

## Companies
- Uber - 9 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: true)
- Snapchat - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1 (Brute force) [Time Limit Exceeded]

**Algorithm**

Reuse the code for Problem 200: [Number of Islands](https://leetcode.com/problems/number-of-islands/description/), for each _addLand_ operation, just call the `numIslands` function of Problem 200 to get the number of islands after performing that operation.

<iframe src="https://leetcode.com/playground/gD7cM5U3/shared" frameBorder="0" width="100%" height="500" name="gD7cM5U3"></iframe>


**Complexity Analysis**

* Time complexity : $$O(L \times m \times n)$$ where $$L$$ is the number of operations, $$m$$ is the
  number of rows and $$n$$ is the number of columns.

* Space complexity : $$O(m \times n)$$ for the `grid` and `visited` 2D arrays.

---


#### Approach #2: (Ad hoc) [Accepted]

**Algorithm**

Use a `HashMap` to map index of a land to its island_ID (starting from 0).
For each _addLand_ operation at position (row, col), check if its adjacent neighbors are in the `HashMap` or not and put the `island_ID` of identified neighbors into a `set` (where each element is unique):

- if the `set` is empty, then the new land at position (row, col) forms a new island (monotonically increasing island_ID by 1);

- if the `set` contains only one island_ID, then the new land belongs to an existing island and island_ID remains unchanged;

- if the `set` contains more than one island_ID, then the new land bridges these separate islands into one island, we need to iterate through the `HashMap` to update this information (time consuming!) and decrease the number of island appropriately.


<iframe src="https://leetcode.com/playground/YEGgV5UT/shared" frameBorder="0" width="100%" height="500" name="YEGgV5UT"></iframe>

**Complexity Analysis**

* Time complexity : $$O(L^2)$$, for each operation, we have to traverse the entire HashMap to update island id and the number of operations is $$L$$.

* Space complexity : $$O(L)$$ for the `HashMap`.

P.S. C++ solution was accepted with 1409 ms runtime, but Java solution got an TLE (Time Limit Exceeded).

---


#### Approach #3: Union Find (aka Disjoint Set) [Accepted]

**Intuition**

Treat the 2d grid map as an undirected graph (formatted as adjacency matrix) and there is an edge
between two horizontally or vertically adjacent nodes of value `1`, then the problem reduces to finding the number of connected components in the graph after each _addLand_ operation.

**Algorithm**

Make use of a `Union Find` data structure of size `m*n` to store all the nodes in the graph and initially each node's parent value is set to `-1` to represent an empty graph. Our goal is to update `Union Find`  with lands added by _addLand_ operation and union lands belong to the same island.

For each _addLand_ operation at position (row, col), union it with its adjacent neighbors if they belongs to some islands, if none of its neighbors belong to any islands, then initialize the new position as a new island (set parent value to itself) within `Union Find`.

For detailed description of `Union Find` (implemented with path compression and union by rank), you can refer to this [article](https://leetcode.com/articles/redundant-connection/).

The algorithm can be better illustrated by the animation below (including how `Union Find` with _path compression_ and _union by rank_ works):
!?!../Documents/305_number_of_islands_ii_unionfind.json:1024,768!?!

<iframe src="https://leetcode.com/playground/MZ4PxMsE/shared" frameBorder="0" width="100%" height="500" name="MZ4PxMsE"></iframe>


**Complexity Analysis**

* Time complexity : $$O(m \times n + L)$$ where $$L$$ is the number of operations, $$m$$ is the
  number of rows and $$n$$ is the number of columns. it takes $$O(m \times n)$$ to initialize UnionFind, and $$O(L)$$ to process positions. Note that Union operation takes essentially constant
  time[^1] when UnionFind is implemented with both path compression and union by rank.

* Space complexity : $$O(m \times n)$$ as required by UnionFind data structure.



---

**Footnotes**

[^1]: [https://en.wikipedia.org/wiki/Disjoint-set_data_structure](https://en.wikipedia.org/wiki/Disjoint-set_data_structure)

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easiest Java Solution with Explanations
- Author: yavinci
- Creation Date: Sun Nov 15 2015 09:00:57 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 21:29:49 GMT+0800 (Singapore Standard Time)

<p>
This is a basic `union-find` problem. Given a graph with points being added, we can at least solve:

 1. How many islands in total?
 2. Which island is pointA belonging to?
 3. Are pointA and pointB connected?

The idea is simple. To represent a list of islands,  we use **trees**.  i.e., a list of roots. This helps us find the identifier of an island faster. If `roots[c] = p` means the parent of node c is p, we can climb up the parent chain to find out the identifier of an island, i.e., which island this point belongs to:

    Do root[root[roots[c]]]... until root[c] == c;

To transform the two dimension problem into the classic UF, perform a linear mapping:

    int id = n * x + y;

Initially assume every cell are in  non-island set `{-1}`. When point A is added, we create a new root, i.e., a new island. Then, check if any of its 4 neighbors belong to the same island. If not, `union` the neighbor by setting the root to be the same. Remember to skip non-island cells. 

**UNION** operation is only changing the root parent so the running time is `O(1)`.

**FIND** operation is proportional to the depth of the tree. If N is the number of points added, the average running time is `O(logN)`, and a sequence of `4N` operations take `O(NlogN)`. If there is no balancing, the worse case could be `O(N^2)`.

Remember that one island could have different `roots[node]` value for each node. Because `roots[node]` is the parent of the node, not the highest root of the island. To find the actually root, we have to climb up the tree by calling **findIsland** function.

Here I've attached my solution. There can be at least two improvements:  `union by rank` & `path compression`. However I suggest first finish the basis, then discuss the improvements.

Cheers! 

    int[][] dirs = {{0, 1}, {1, 0}, {-1, 0}, {0, -1}};

    public List<Integer> numIslands2(int m, int n, int[][] positions) {
        List<Integer> result = new ArrayList<>();
        if(m <= 0 || n <= 0) return result;

        int count = 0;                      // number of islands
        int[] roots = new int[m * n];       // one island = one tree
        Arrays.fill(roots, -1);            

        for(int[] p : positions) {
            int root = n * p[0] + p[1];     // assume new point is isolated island
            roots[root] = root;             // add new island
            count++;

            for(int[] dir : dirs) {
                int x = p[0] + dir[0]; 
                int y = p[1] + dir[1];
                int nb = n * x + y;
                if(x < 0 || x >= m || y < 0 || y >= n || roots[nb] == -1) continue;
                
                int rootNb = findIsland(roots, nb);
                if(root != rootNb) {        // if neighbor is in another island
                    roots[root] = rootNb;   // union two islands 
                    root = rootNb;          // current tree root = joined tree root
                    count--;               
                }
            }

            result.add(count);
        }
        return result;
    }

    public int findIsland(int[] roots, int id) {
        while(id != roots[id]) id = roots[id];
        return id;
    }

<hr>
<h3> Path Compression (Bonus) </h3>
<hr>

If you have time, add one line to shorten the tree. The new runtime becomes: `19ms (95.94%)`.

    public int findIsland(int[] roots, int id) {
        while(id != roots[id]) {
            roots[id] = roots[roots[id]];   // only one line added
            id = roots[id];
        }
        return id;
    }
</p>


### Java/Python clear solution with UnionFind Class (Weighting and Path compression)
- Author: dietpepsi
- Creation Date: Sat Nov 14 2015 05:47:15 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 04:39:47 GMT+0800 (Singapore Standard Time)

<p>
**Union Find**
is an abstract data structure supporting `find` and `unite` on disjointed sets of objects, typically used to solve the network connectivity problem.

The two operations are defined like this: 

`find(a,b)` : are `a` and `b` belong to the same set?

`unite(a,b)` : if `a` and `b` are not in the same set, unite the sets they belong to.

With this data structure, it is very fast for solving our problem. Every position is an new land, if the new land connect two islands `a` and `b`, we combine them to form a whole. The answer is then the number of the disjointed sets.


The following algorithm is derived from [Princeton's lecture note on Union Find][1] in [Algorithms and Data Structures][2] It is a well organized note with clear illustration describing from the naive QuickFind to the one with Weighting and Path compression.
With Weighting and Path compression, The algorithm runs in `O((M+N) log* N)` where `M` is the number of operations ( unite and find ), `N` is the number of objects, `log*` is [iterated logarithm][3] while the naive runs in `O(MN)`.

For our problem, If there are `N` positions, then there are `O(N)` operations and `N` objects then total is `O(N log*N)`, when we don't consider the `O(mn)` for array initialization.

Note that `log*N` is almost constant (for `N` = 265536, `log*N` = 5) in this universe, so the algorithm is almost linear with `N`.

However, if the map is very big, then the initialization of the arrays can cost a lot of time when `mn` is much larger than `N`. In this case we should consider using a hashmap/dictionary for the underlying data structure to avoid this overhead.

Of course, we can put all the functionality into the Solution class which will make the code a lot shorter. But from a design point of view a separate class dedicated to the data sturcture is more readable and reusable.

I implemented the idea with 2D interface to better fit the problem.

**Java**

    public class Solution {
    
        private int[][] dir = {{0, 1}, {0, -1}, {-1, 0}, {1, 0}};
    
        public List<Integer> numIslands2(int m, int n, int[][] positions) {
            UnionFind2D islands = new UnionFind2D(m, n);
            List<Integer> ans = new ArrayList<>();
            for (int[] position : positions) {
                int x = position[0], y = position[1];
                int p = islands.add(x, y);
                for (int[] d : dir) {
                    int q = islands.getID(x + d[0], y + d[1]);
                    if (q > 0 && !islands.find(p, q))
                        islands.unite(p, q);
                }
                ans.add(islands.size());
            }
            return ans;
        }
    }

    class UnionFind2D {
        private int[] id;
        private int[] sz;
        private int m, n, count;

        public UnionFind2D(int m, int n) {
            this.count = 0;
            this.n = n;
            this.m = m;
            this.id = new int[m * n + 1];
            this.sz = new int[m * n + 1];
        }

        public int index(int x, int y) { return x * n + y + 1; }

        public int size() { return this.count; }

        public int getID(int x, int y) {
            if (0 <= x && x < m && 0<= y && y < n)
                return id[index(x, y)];
            return 0;
        }

        public int add(int x, int y) {
            int i = index(x, y);
            id[i] = i; sz[i] = 1;
            ++count;
            return i;
        }

        public boolean find(int p, int q) {
            return root(p) == root(q);
        }

        public void unite(int p, int q) {
            int i = root(p), j = root(q);
            if (sz[i] < sz[j]) { //weighted quick union
                id[i] = j; sz[j] += sz[i];
            } else {
                id[j] = i; sz[i] += sz[j];
            }
            --count;
        }

        private int root(int i) {
            for (;i != id[i]; i = id[i])
                id[i] = id[id[i]]; //path compression
            return i;
        }
    }
    //Runtime: 20 ms


**Python (using dict)**
    
    class Solution(object):
        def numIslands2(self, m, n, positions):
            ans = []
            islands = Union()
            for p in map(tuple, positions):
                islands.add(p)
                for dp in (0, 1), (0, -1), (1, 0), (-1, 0):
                    q = (p[0] + dp[0], p[1] + dp[1])
                    if q in islands.id:
                        islands.unite(p, q)
                ans += [islands.count]
            return ans
    
    class Union(object):
        def __init__(self):
            self.id = {}
            self.sz = {}
            self.count = 0
    
        def add(self, p):
            self.id[p] = p
            self.sz[p] = 1
            self.count += 1
    
        def root(self, i):
            while i != self.id[i]:
                self.id[i] = self.id[self.id[i]]
                i = self.id[i]
            return i
    
        def unite(self, p, q):
            i, j = self.root(p), self.root(q)
            if i == j:
                return
            if self.sz[i] > self.sz[j]:
                i, j = j, i
            self.id[i] = j
            self.sz[j] += self.sz[i]
            self.count -= 1

    #Runtime: 300 ms


  [1]: https://www.cs.princeton.edu/~rs/AlgsDS07/01UnionFind.pdf
  [2]: https://www.cs.princeton.edu/~rs/AlgsDS07/
  [3]: https://en.wikipedia.org/wiki/Iterated_logarithm
</p>


### Compact Python.
- Author: StefanPochmann
- Creation Date: Sat Nov 14 2015 20:17:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 17:25:34 GMT+0800 (Singapore Standard Time)

<p>
Pretty much just Wikipedia's [Disjoint-set forests](https://en.wikipedia.org/wiki/Disjoint-set_data_structure#Disjoint-set_forests), using *"union by rank"* and *"path compression"*. I don't see the point of `m` and `n`, so I ignore them.

    def numIslands2(self, m, n, positions):
        parent, rank, count = {}, {}, [0]
        def find(x):
            if parent[x] != x:
                parent[x] = find(parent[x])
            return parent[x]
        def union(x, y):
            x, y = find(x), find(y)
            if x != y:
                if rank[x] < rank[y]:
                    x, y = y, x
                parent[y] = x
                rank[x] += rank[x] == rank[y]
                count[0] -= 1
        def add((i, j)):
            x = parent[x] = i, j
            rank[x] = 0
            count[0] += 1
            for y in (i+1, j), (i-1, j), (i, j+1), (i, j-1):
                if y in parent:
                    union(x, y)
            return count[0]
        return map(add, positions)

Too bad Python 2 doesn't have `nonlocal` yet, hence the somewhat ugly `count[0]` "hack". Here's a different way:

    def numIslands2(self, m, n, positions):
        parent, rank = {}, {}
        def find(x):
            if parent[x] != x:
                parent[x] = find(parent[x])
            return parent[x]
        def union(x, y):
            x, y = find(x), find(y)
            if x == y:
                return 0
            if rank[x] < rank[y]:
                x, y = y, x
            parent[y] = x
            rank[x] += rank[x] == rank[y]
            return 1
        counts, count = [], 0
        for i, j in positions:
            x = parent[x] = i, j
            rank[x] = 0
            count += 1
            for y in (i+1, j), (i-1, j), (i, j+1), (i, j-1):
                if y in parent:
                    count -= union(x, y)
            counts.append(count)
        return counts
</p>


