---
title: "Binary Number with Alternating Bits"
weight: 625
#id: "binary-number-with-alternating-bits"
---
## Description
<div class="description">
<p>Given a positive integer, check whether it has alternating bits: namely, if two adjacent bits will always have different values.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 5
<strong>Output:</strong> true
<strong>Explanation:</strong> The binary representation of 5 is: 101
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 7
<strong>Output:</strong> false
<strong>Explanation:</strong> The binary representation of 7 is: 111.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 11
<strong>Output:</strong> false
<strong>Explanation:</strong> The binary representation of 11 is: 1011.</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 10
<strong>Output:</strong> true
<strong>Explanation:</strong> The binary representation of 10 is: 1010.</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 3
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 2<sup>31</sup> - 1</code></li>
</ul>

</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Yahoo - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Convert to String [Accepted]

**Intuition and Algorithm**

Let's convert the given number into a string of binary digits.  Then, we should simply check that no two adjacent digits are the same.

<iframe src="https://leetcode.com/playground/79o5Wvyy/shared" frameBorder="0" name="79o5Wvyy" width="100%" height="241"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(1)$$.  For arbitrary inputs, we do $$O(w)$$ work, where $$w$$ is the number of bits in `n`.  However, $$w \leq 32$$.

* Space complexity: $$O(1)$$, or alternatively $$O(w)$$.

---

#### Approach #2: Divide By Two [Accepted]

**Intuition and Algorithm**

We can get the last bit and the rest of the bits via `n % 2` and `n // 2` operations.  Let's remember `cur`, the last bit of `n`.  If the last bit ever equals the last bit of the remaining, then two adjacent bits have the same value, and the answer is `False`.  Otherwise, the answer is `True`.

Also note that instead of `n % 2` and `n // 2`, we could have used operators `n & 1` and `n >>= 1` instead.

<iframe src="https://leetcode.com/playground/oFAELrSA/shared" frameBorder="0" name="oFAELrSA" width="100%" height="258"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(1)$$.  For arbitrary inputs, we do $$O(w)$$ work, where $$w$$ is the number of bits in `n`.  However, $$w \leq 32$$.

* Space complexity: $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java super simple explanation with inline example
- Author: san89kalp
- Creation Date: Sun Feb 04 2018 07:47:40 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 12:35:42 GMT+0800 (Singapore Standard Time)

<p>
We know that if we shift the number by 1 to the right, all the ones will become zeros and vice versa. Now if we AND those two numbers, we can get the whole thing as zero but that won't work for numbers like 2, 4, 8... So we will take another approach. Instead of AND we will do an XOR . This will make all bits 1. Now we need to check if all the bits are 1. The best way to do that is AND the number by (number+1) . It'll give you zero.
```
    boolean hasAlternatingBits2(int n) {
        /*
        n =         1 0 1 0 1 0 1 0
        n >> 1      0 1 0 1 0 1 0 1
        n ^ n>>1    1 1 1 1 1 1 1 1
        n           1 1 1 1 1 1 1 1
        n + 1     1 0 0 0 0 0 0 0 0
        n & (n+1)   0 0 0 0 0 0 0 0
        */

        n = n ^ (n>>1);
        return (n & n+1) == 0;
    }
```
</p>


### Oneliners (C++, Java, Ruby, Python)
- Author: StefanPochmann
- Creation Date: Sun Oct 08 2017 16:54:35 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 22:11:00 GMT+0800 (Singapore Standard Time)

<p>
### Solution 1 - Cancel Bits

    bool hasAlternatingBits(int n) {
        return !((n ^= n/4) & n-1);
    }

Xor the number with itself shifted right twice and check whether everything after the leading 1-bit became/stayed 0. Xor is 0 iff the bits are equal, so we get 0-bits iff the pair of leading 1-bit and the 0-bit in front of it are repeated until the end.

        000101010
      ^ 000001010
      = 000100000
 
### Solution 2 - Complete Bits

    bool hasAlternatingBits(int n) {
        return !((n ^= n/2) & n+1);
    }

Xor the number with itself shifted right once and check whether everything after the leading 1-bit became/stayed 1. Xor is 1 iff the bits differ, so we get 1-bits iff starting with the leading 1-bit, the bits alternate between 1 and 0.

        000101010
      ^ 000010101
      = 000111111

### Solution 3 - Positive RegEx

    public boolean hasAlternatingBits(int n) {
        return Integer.toBinaryString(n).matches("(10)*1?");
    }

It's simple to describe with a regular expression.
<br>

### Solution 4 - Negative RegEx

    def has_alternating_bits(n)
      n.to_s(2) !~ /00|11/
    end

It's even simpler to describe what we **don't** want: two zeros or ones in a row.
<br>

### Solution 5 - Negative String

    def hasAlternatingBits(self, n):
        return '00' not in bin(n) and '11' not in bin(n)

Same as before, just not using regex.
<br>

### Solution 6 - Golfed

    def has_alternating_bits(n)
      (n^=n/2)&n+1<1
    end

### Solution 7 - Recursion

    def has_alternating_bits(n)
      n < 3 || n%2 != n/2%2 && has_alternating_bits(n/2)
    end

Compare the last two bits and recurse with the last bit shifted out.
<br>

### Solution 8 - Complete Bits + RegEx

    public boolean hasAlternatingBits(int n) {
        return Integer.toBinaryString(n ^ n/2).matches("1+");
    }
</p>


### easy python
- Author: fhqplzj
- Creation Date: Sun Oct 08 2017 20:26:33 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 03:20:08 GMT+0800 (Singapore Standard Time)

<p>
```
    def hasAlternatingBits(self, n):
        s = bin(n)
        return '00' not in s and '11' not in s
</p>


