---
title: "Count Number of Nice Subarrays"
weight: 1192
#id: "count-number-of-nice-subarrays"
---
## Description
<div class="description">
<p>Given an array of integers <code>nums</code> and an integer <code>k</code>. A continuous subarray is called <strong>nice</strong> if there are <code>k</code> odd numbers on it.</p>

<p>Return <em>the number of <strong>nice</strong> sub-arrays</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,1,2,1,1], k = 3
<strong>Output:</strong> 2
<strong>Explanation:</strong> The only sub-arrays with 3 odd numbers are [1,1,2,1] and [1,2,1,1].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [2,4,6], k = 1
<strong>Output:</strong> 0
<strong>Explanation:</strong> There is no odd numbers in the array.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [2,2,2,1,2,2,1,2,2,2], k = 2
<strong>Output:</strong> 16
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 50000</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 10^5</code></li>
	<li><code>1 &lt;= k &lt;= nums.length</code></li>
</ul>

</div>

## Tags
- Two Pointers (two-pointers)

## Companies
- Roblox - 3 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Sliding Window, O(1) Space
- Author: lee215
- Creation Date: Sun Nov 03 2019 12:04:38 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 15 2020 15:40:04 GMT+0800 (Singapore Standard Time)

<p>
# **Solution 1: atMost**
Have you read this? [992. Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/discuss/523136/JavaC%2B%2BPython-Sliding-Window)
Exactly `K` times = at most `K` times - at most `K - 1` times
<br>

# **Complexity**
Time `O(N)` for one pass
Space `O(1)`
<br>

**Java:**
```java
    public int numberOfSubarrays(int[] A, int k) {
        return atMost(A, k) - atMost(A, k - 1);
    }

    public int atMost(int[] A, int k) {
        int res = 0, i = 0, n = A.length;
        for (int j = 0; j < n; j++) {
            k -= A[j] % 2;
            while (k < 0)
                k += A[i++] % 2;
            res += j - i + 1;
        }
        return res;
    }
```

**C++:**
```cpp
    int numberOfSubarrays(vector<int>& A, int k) {
        return atMost(A, k) - atMost(A, k - 1);
    }

    int atMost(vector<int>& A, int k) {
        int res = 0, i = 0, n = A.size();
        for (int j = 0; j < n; j++) {
            k -= A[j] % 2;
            while (k < 0)
                k += A[i++] % 2;
            res += j - i + 1;
        }
        return res;
    }
```

**Python:**
```python
    def numberOfSubarrays(self, A, k):
        def atMost(k):
            res = i = 0
            for j in xrange(len(A)):
                k -= A[j] % 2
                while k < 0:
                    k += A[i] % 2
                    i += 1
                res += j - i + 1
            return res

        return atMost(k) - atMost(k - 1)
```
<br><br>

# Solution II: One pass

Actually it\'s same as three pointers.
Though we use `count` to count the number of even numebers.
Insprired by @yannlecun.

Time `O(N)` for one pass
Space `O(1)`
<br>

**Java:**
```java
    public int numberOfSubarrays(int[] A, int k) {
        int res = 0, i = 0, count = 0, n = A.length;
        for (int j = 0; j < n; j++) {
            if (A[j] % 2 == 1) {
                --k;
                count = 0;
            }
            while (k == 0) {
                k += A[i++] & 1;
                ++count;
            }
            res += count;
        }
        return res;
    }
```

**C++:**
```cpp
    int numberOfSubarrays(vector<int>& A, int k) {
        int res = 0, i = 0, count = 0, n = A.size();
        for (int j = 0; j < n; j++) {
            if (A[j] & 1)
                --k, count = 0;
            while (k == 0)
                k += A[i++] & 1, ++count;
            res += count;
        }
        return res;
    }
```

**Python:**
```python
    def numberOfSubarrays(self, A, k):
        i = count = res = 0
        for j in xrange(len(A)):
            if A[j] & 1:
                k -= 1
                count = 0
            while k == 0:
                k += A[i] & 1
                i += 1
                count += 1
            res += count
        return res
```

# More Similar Sliding Window Problems
Here are some similar sliding window problems.
Also find more explanations.
Good luck and have fun.


- 1358. [Number of Substrings Containing All Three Characters](https://leetcode.com/problems/number-of-substrings-containing-all-three-characters/discuss/516977/JavaC++Python-Easy-and-Concise)
- 1248. [Count Number of Nice Subarrays](https://leetcode.com/problems/count-number-of-nice-subarrays/discuss/419378/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
- 1234. [Replace the Substring for Balanced String](https://leetcode.com/problems/replace-the-substring-for-balanced-string/discuss/408978/javacpython-sliding-window/367697)
- 1004. [Max Consecutive Ones III](https://leetcode.com/problems/max-consecutive-ones-iii/discuss/247564/javacpython-sliding-window/379427?page=3)
-  930. [Binary Subarrays With Sum](https://leetcode.com/problems/binary-subarrays-with-sum/discuss/186683/)
-  992. [Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/discuss/523136/JavaC%2B%2BPython-Sliding-Window)
-  904. [Fruit Into Baskets](https://leetcode.com/problems/fruit-into-baskets/discuss/170740/Sliding-Window-for-K-Elements)
-  862. [Shortest Subarray with Sum at Least K](https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/discuss/143726/C%2B%2BJavaPython-O(N)-Using-Deque)
-  209. [Minimum Size Subarray Sum](https://leetcode.com/problems/minimum-size-subarray-sum/discuss/433123/JavaC++Python-Sliding-Window)
<br>
</p>


### Java/C++ with picture, deque
- Author: votrubac
- Creation Date: Sun Nov 03 2019 13:38:38 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 06 2019 19:44:29 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
This is similar to [992. Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/discuss/235235/C%2B%2BJava-with-picture-prefixed-sliding-window).

When we find `k` odd numbers, we have one nice subarray, plus an additional subarray for each even number preceding the first odd number. This is also true for each even number that follows.

For example, in the picture below we found `k == 2` odd numbers at index `4`. We have 3 nice subarrays therefore: `[2, 4], [1, 4], [0, 4]`. For even number at index `5`, we also have 3 nice subarrays: `[2, 5], [1, 5], [0, 5]`. Plus 3 subarrays for index `6`.

When we encounter `k + 1` odd number at index `7`, we need to get rid of the first odd number (at index `2`) to have exactly `k` odd numbers. So, we count nice subarrays as if the array starts at index `3`. 

![image](https://assets.leetcode.com/users/votrubac/image_1572762880.png)

#### Approach 1: Deque
We do not have to use `deque`, but it makes tracking the window easier (comparing to moving pointers). Note that in Java, the deque interface does not provide a random access, so we\'ll just use `LinkedList` instead.

**Java**
```Java
public int numberOfSubarrays(int[] nums, int k) {
  LinkedList<Integer> deq = new LinkedList();
  deq.add(-1);
  int res = 0;
  for (int i = 0; i < nums.length; ++i) {
    if (nums[i] % 2 == 1) deq.add(i);
    if (deq.size() > k + 1) deq.pop();
    if (deq.size() == k + 1) res += deq.get(1) - deq.get(0);
  }
  return res;
}
```
**C++**
```
int numberOfSubarrays(vector<int>& nums, int k, int res = 0) {
  deque<int> deq = { -1 };
  for (auto i = 0; i < nums.size(); ++i) {
    if (nums[i] % 2) deq.push_back(i);
    if (deq.size() > k + 1) deq.pop_front();
    if (deq.size() == k + 1) res += deq[1] - deq[0];
  }
  return res;
}
```
**Complexity Analysis**
- Time: O(n).
- Memory: O(k) for the deque.
</p>


### C++: Visual explanation. O(1) space. Two pointers
- Author: andnik
- Creation Date: Fri Feb 14 2020 05:38:11 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Feb 19 2020 14:59:46 GMT+0800 (Singapore Standard Time)

<p>
##### Algorithm
The best way to explain this approach is to look at the example:
Let\'s say **k = 2** and we have the following array:

![image](https://assets.leetcode.com/users/andnik/image_1582094442.png)

We only going to work with numbers `1` and `2` because we are only interested in if number is odd or even, we don\'t care what the actual value is.
1. Using `i` iterate over the array counting **only odd** numbers:

![image](https://assets.leetcode.com/users/andnik/image_1582094637.png)

2. When **odd** is equal to **k**, we can analyse our **left part** and count how many subarrays with **odd == k** we can produce.
We are doing it with `j` iterating until we get to **odd number**.

![image](https://assets.leetcode.com/users/andnik/image_1582094795.png)

We can see that there are `3` subarrays we can produce with **odd == k**.

3. Continue with `i` again. Since **odd >= k** then every next **even number** we meet is going to **double previous count**. Let\'s see:

![image](https://assets.leetcode.com/users/andnik/image_1582095218.png)

4. When we meet **odd number** again we need to reset `count=1` and repeat step (2) with `j` again. In our example there\'s going to be only one subarray:

![image](https://assets.leetcode.com/users/andnik/image_1582095418.png)

5. And finish with counting last **even number** as in step (3).

Since we are reseting `count` variable to 0, we are adding sum of all subarrays in `total` variable.

##### Code
```
class Solution {
public:
    int numberOfSubarrays(vector<int>& nums, int k) {
        int j = 0, odd = 0, count = 0, total = 0;
        for (int i = 0; i < nums.size(); i++) {
            if (nums[i] & 1) {
                odd++;
                if (odd >= k) {
                    count = 1;
                    while (!(nums[j++] & 1)) count++;
                    total += count;
                }
            } else if (odd >= k) total += count;
        }
        return total;
    }
};
```
</p>


