---
title: "Least Number of Unique Integers after K Removals"
weight: 1360
#id: "least-number-of-unique-integers-after-k-removals"
---
## Description
<div class="description">
<p>Given an array of integers&nbsp;<code>arr</code>&nbsp;and an integer <code>k</code>.&nbsp;Find the <em>least number of unique integers</em>&nbsp;after removing <strong>exactly</strong> <code>k</code> elements<b>.</b></p>

<ol>
</ol>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>arr = [5,5,4], k = 1
<strong>Output: </strong>1
<strong>Explanation</strong>: Remove the single 4, only 5 is left.
</pre>
<strong>Example 2:</strong>

<pre>
<strong>Input: </strong>arr = [4,3,1,1,3,3,2], k = 3
<strong>Output: </strong>2
<strong>Explanation</strong>: Remove 4, 2 and either one of the two 1s or three 3s. 1 and 3 will be left.</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length&nbsp;&lt;= 10^5</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 10^9</code></li>
	<li><code>0 &lt;= k&nbsp;&lt;= arr.length</code></li>
</ul>
</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Roblox - 2 (taggedByAdmin: false)
- Dosh - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Greedy Alg.: 3 methods from O(nlogn) to O(n) w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Jun 14 2020 12:06:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 15 2020 00:21:42 GMT+0800 (Singapore Standard Time)

<p>
**[Summary]**
Remove  k least frequent elements to make the remaining ones as least unique ints set.

----
**Method 1: HashMap and PriorityQueue** -- credit to **@usamaten**.
Count number then put the keys into a PriorityQueue and sort by their occurrences:

```java
        Map<Integer, Integer> count = new HashMap<>();
        for (int a : arr)
            count.put(a, 1 + count.getOrDefault(a, 0));
        PriorityQueue<Integer> pq = new PriorityQueue<>(Comparator.comparing(count::get));
        pq.addAll(count.keySet());
        while (k > 0)
            k -= count.get(pq.poll());
        return k < 0 ? pq.size() + 1 : pq.size();
```
```python
    def findLeastNumOfUniqueInts(self, arr: List[int], k: int) -> int:
        hp = [(val, key) for key, val in collections.Counter(arr).items()]
        heapq.heapify(hp)
        while k > 0:
            k -= heapq.heappop(hp)[0]
        return len(hp) + (k < 0)   
```

----

**Method 2: HashMap and TreeMap**
Count number then count occurrence:

1. Count the occurrences of each number using HashMap;
2. Using TreeMap to count each occurrence;
3. Poll out currently least frequent elemnets, and check if reaching `k`, deduct the correponding unique count `remaining`.

```java
    public int findLeastNumOfUniqueInts(int[] arr, int k) {
        Map<Integer, Integer> count = new HashMap<>();
        for (int a : arr)
            count.put(a, 1 + count.getOrDefault(a, 0));
        int remaining = count.size();
        TreeMap<Integer, Integer> occurrenceCount = new TreeMap<>();
        for (int v : count.values())
            occurrenceCount.put(v, 1 + occurrenceCount.getOrDefault(v, 0));
        while (k > 0) {
            Map.Entry<Integer, Integer> entry = occurrenceCount.pollFirstEntry();
            if (k - entry.getKey() * entry.getValue() >= 0) {
                k -= entry.getKey() * entry.getValue();
                remaining -= entry.getValue();
            }else {
                return remaining - k / entry.getKey();
            }
        }
        return remaining;        
    }
```
```python
    def findLeastNumOfUniqueInts(self, arr: List[int], k: int) -> int:
        c = Counter(arr)
        cnt, remaining = Counter(c.values()), len(c)
        for key in sorted(cnt): 
            if k >= key * cnt[key]:
                k -= key * cnt[key]
                remaining -= cnt.pop(key)
            else:
                return remaining - k // key
        return remaining
```
**Analysis:**
Time: O(nlogn), space: O(n).

----

**Method 3: HashMap and Array** -- credit to **@krrs**.
Count number then count occurrence:

1. Count the occurrences of each number using HashMap;
2. Using Array to count each occurrence, since max occurrence <= `arr.length`;
3. From small to big, for each unvisited least frequent element, deduct from `k` the multiplication with the number of elements of same occurrence, check if reaching `0`, then deduct the correponding unique count `remaining`.

```java
    public int findLeastNumOfUniqueInts(int[] arr, int k) {
        Map<Integer, Integer> count = new HashMap<>();
        for (int a : arr)
            count.put(a, 1 + count.getOrDefault(a, 0));
        int remaining = count.size(), occur = 1;
        int[] occurrenceCount = new int[arr.length + 1];
        for (int v : count.values())
            ++occurrenceCount[v];
        while (k > 0) {
            if (k - occur * occurrenceCount[occur] >= 0) {
                k -= occur * occurrenceCount[occur];
                remaining -= occurrenceCount[occur++];
            }else {
                return remaining - k / occur;
            }
        }
        return remaining;        
    }
```


```python
    def findLeastNumOfUniqueInts(self, arr: List[int], k: int) -> int:
        c = Counter(arr)
        cnt, remaining = Counter(c.values()), len(c)
        for key in range(1, len(arr) + 1): 
            if k >= key * cnt[key]:
                k -= key * cnt[key]
                remaining -= cnt[key]
            else:
                return remaining - k // key
        return remaining
```
**Analysis:**
Time: O(n), space: O(n).


</p>


### Java Simple HashMap and Lambda Sort
- Author: hobiter
- Creation Date: Sun Jun 14 2020 14:26:12 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 15 2020 11:07:41 GMT+0800 (Singapore Standard Time)

<p>
```
    public int findLeastNumOfUniqueInts(int[] arr, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int n : arr) map.put(n, map.getOrDefault(n, 0) + 1);
        List<Integer> l = new ArrayList<>(map.keySet());
        Collections.sort(l, (a, b) -> map.get(a) - map.get(b));
        int n = map.size(), remove = 0, idx = 0;
        while (k > 0 && idx < n) {
            k -= map.get(l.get(idx++));
            if (k >= 0) remove++;
        }
        return n - remove;
    }
```
</p>


### Python || 3 Line || Shortest, Simplest
- Author: Uttam1728
- Creation Date: Sun Jun 14 2020 12:06:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 15 2020 13:59:08 GMT+0800 (Singapore Standard Time)

<p>
> Idea is to remove \"elements which have least count\" so remaining have least unique char.


```
def findLeastNumOfUniqueInts(self, arr: List[int], k: int) -> int:
        c = Counter(arr)
        s = sorted(arr,key = lambda x:(c[x],x))
        return len(set(s[k:]))
            
```
upvote if you find it good.
> Update -  You can check this [Post](https://leetcode.com/problems/count-good-nodes-in-binary-tree/discuss/635351/BFS-With-MaxValue-or-Template-of-Similar-Problems-Python) also. Hope that will be benificial to new leetcoders for that kind of problems. -> [Post](https://leetcode.com/problems/count-good-nodes-in-binary-tree/discuss/635351/BFS-With-MaxValue-or-Template-of-Similar-Problems-Python)
</p>


