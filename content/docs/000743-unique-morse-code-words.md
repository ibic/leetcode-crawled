---
title: "Unique Morse Code Words"
weight: 743
#id: "unique-morse-code-words"
---
## Description
<div class="description">
<p>International Morse Code defines a standard encoding where each letter is mapped to a series of dots and dashes, as follows: <code>&quot;a&quot;</code> maps to <code>&quot;.-&quot;</code>, <code>&quot;b&quot;</code> maps to <code>&quot;-...&quot;</code>, <code>&quot;c&quot;</code> maps to <code>&quot;-.-.&quot;</code>, and so on.</p>

<p>For convenience, the full table for the 26 letters of the English alphabet is given below:</p>

<pre>
[&quot;.-&quot;,&quot;-...&quot;,&quot;-.-.&quot;,&quot;-..&quot;,&quot;.&quot;,&quot;..-.&quot;,&quot;--.&quot;,&quot;....&quot;,&quot;..&quot;,&quot;.---&quot;,&quot;-.-&quot;,&quot;.-..&quot;,&quot;--&quot;,&quot;-.&quot;,&quot;---&quot;,&quot;.--.&quot;,&quot;--.-&quot;,&quot;.-.&quot;,&quot;...&quot;,&quot;-&quot;,&quot;..-&quot;,&quot;...-&quot;,&quot;.--&quot;,&quot;-..-&quot;,&quot;-.--&quot;,&quot;--..&quot;]</pre>

<p>Now, given a list of words, each word can be written as a concatenation of the Morse code of each letter. For example, &quot;cab&quot; can be written as &quot;-.-..--...&quot;, (which is the concatenation &quot;-.-.&quot; + &quot;.-&quot; + &quot;<code>-...</code>&quot;). We&#39;ll call such a concatenation, the transformation&nbsp;of a word.</p>

<p>Return the number of different transformations among all words we have.</p>

<pre>
<strong>Example:</strong>
<strong>Input:</strong> words = [&quot;gin&quot;, &quot;zen&quot;, &quot;gig&quot;, &quot;msg&quot;]
<strong>Output:</strong> 2
<strong>Explanation: </strong>
The transformation of each word is:
&quot;gin&quot; -&gt; &quot;--...-.&quot;
&quot;zen&quot; -&gt; &quot;--...-.&quot;
&quot;gig&quot; -&gt; &quot;--...--.&quot;
&quot;msg&quot; -&gt; &quot;--...--.&quot;

There are 2 different transformations, &quot;--...-.&quot; and &quot;--...--.&quot;.
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li>The length of <code>words</code> will be at most <code>100</code>.</li>
	<li>Each <code>words[i]</code> will have length in range <code>[1, 12]</code>.</li>
	<li><code>words[i]</code> will only consist of lowercase letters.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach 1: Hash Set

**Intuition and Algorithm**

We can transform each `word` into it's Morse Code representation.

After, we put all transformations into a set `seen`, and return the size of the set.

<iframe src="https://leetcode.com/playground/PLyRVptz/shared" frameBorder="0" width="100%" height="361" name="PLyRVptz"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(S)$$, where $$S$$ is the sum of the lengths of words in `words`.  We iterate through each character of each word in `words`.

* Space Complexity: $$O(S)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### is the description of the problem correct?
- Author: smartgear
- Creation Date: Mon Mar 26 2018 15:42:16 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 22:21:29 GMT+0800 (Singapore Standard Time)

<p>
the description of the problem gives a example.
"a" maps to ".-", "b" maps to "-...", "c" maps to "-.-.", 
"cab" can be written as "-.-.-....-", (which is the concatenation "-.-." + "-..." + ".-").
shouldn\'t "cab" be mapped to "-.-..--...", ("-.-."+".-"+"-...")?!!!!
</p>


### [Java/C++/Python] Easy and Concise Solution
- Author: lee215
- Creation Date: Sun Mar 25 2018 11:14:07 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 27 2019 14:20:29 GMT+0800 (Singapore Standard Time)

<p>

**Java**
```java
    public int uniqueMorseRepresentations(String[] words) {
        String[] d = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
        HashSet<String> s = new HashSet<>();
        for (String w : words) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < w.length(); ++i)
                sb.append(d[w.charAt(i) - \'a\']);
            s.add(sb.toString());
        }
        return s.size();
    }
```

**C++**
```cpp
    int uniqueMorseRepresentations(vector<string>& words) {
        vector<string> d = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
        unordered_set<string> s;
        for (auto w : words) {
            string code;
            for (auto c : w) code += d[c - \'a\'];
            s.insert(code);
        }
        return s.size();
    }
```

**Python**
```python
    def uniqueMorseRepresentations(self, words):
        d = [".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--",
             "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."]
        return len({\'\'.join(d[ord(i) - ord(\'a\')] for i in w) for w in words})
</p>


### C++, Straightforward
- Author: DDev
- Creation Date: Sun Mar 25 2018 11:12:56 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 02:52:15 GMT+0800 (Singapore Standard Time)

<p>
    int uniqueMorseRepresentations(vector<string>& words) {
        vector<string> morse_code = {".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};
        unordered_set<string> gen_codes;
        
        for(auto word : words) {
            string code = "";
            for(auto ch : word)
                code += morse_code[ch - \'a\'];
            gen_codes.insert(code);
        }
        
        return gen_codes.size();
    }
</p>


