---
title: "Count Number of Teams"
weight: 1294
#id: "count-number-of-teams"
---
## Description
<div class="description">
<p>There are&nbsp;<code>n</code>&nbsp;soldiers standing in a line. Each soldier is assigned a <strong>unique</strong> <code>rating</code> value.</p>

<p>You have to form a team of 3 soldiers&nbsp;amongst them under the following rules:</p>

<ul>
	<li>Choose 3 soldiers with index (<code>i</code>, <code>j</code>, <code>k</code>) with&nbsp;rating (<code>rating[i]</code>, <code>rating[j]</code>, <code>rating[k]</code>).</li>
	<li>A team is valid if:&nbsp; (<code>rating[i] &lt; rating[j] &lt; rating[k]</code>) or (<code>rating[i] &gt; rating[j] &gt; rating[k]</code>) where (<code>0&nbsp;&lt;= i &lt;&nbsp;j &lt;&nbsp;k &lt;&nbsp;n</code>).</li>
</ul>

<p>Return the number of teams you can form given the conditions. (soldiers can be part of multiple teams).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> rating = [2,5,3,4,1]
<strong>Output:</strong> 3
<strong>Explanation:</strong> We can form three teams given the conditions. (2,3,4), (5,4,1), (5,3,1). 
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> rating = [2,1,3]
<strong>Output:</strong> 0
<strong>Explanation:</strong> We can&#39;t form any team given the conditions.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> rating = [1,2,3,4]
<strong>Output:</strong> 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == rating.length</code></li>
	<li><code>1 &lt;= n &lt;= 200</code></li>
	<li><code>1 &lt;= rating[i] &lt;= 10^5</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Goldman Sachs - 5 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java O(n * n) and O(n log n)
- Author: votrubac
- Creation Date: Sun Mar 29 2020 12:01:16 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Apr 03 2020 16:12:33 GMT+0800 (Singapore Standard Time)

<p>
For each soldier, count how many soldiers on the left and right have `less` and `greater` ratings.

This soldier can form `less[left] * greater[right] + greater[left] * less[right]` teams.

**Complexity Considerations**

The straightforward approach to count soldiers yields a quadratic solution. You might be tempted to use a set or something to count soldiers in O(log n) time.

You can find a rating in a set in O(log n), however, counting smaller (or larger) elements will still be linear. Unless you implement BST where each node tracks count of left children, a set approach will be quadratic as well.

> Update: see approach 2 below for a simple linearithmic solution below.
 
 #### Approach 1: Count Left and Right
**C++**
```cpp
int numTeams(vector<int>& rating) {
    int res = 0;
    for (auto i = 1; i < rating.size() - 1; ++i) {
        int less[2] = {}, greater[2] = {};
        for (auto j = 0; j < rating.size(); ++j) {
            if (rating[i] < rating[j])
                ++less[j > i];
            if (rating[i] > rating[j])
                ++greater[j > i];
        }
        res += less[0] * greater[1] + greater[0] * less[1];
    }
    return res;
}
```
**Java**
```java
public int numTeams(int[] rating) {
    int res = 0;
    for (int i = 1; i < rating.length - 1; ++i) {
        int less[] = new int[2], greater[] = new int[2];
        for (int j = 0; j < rating.length; ++j) {
            if (rating[i] < rating[j])
                ++less[j > i ? 1 : 0];
            if (rating[i] > rating[j])
                ++greater[j > i ? 1 : 0];
        }
        res += less[0] * greater[1] + greater[0] * less[1];
    }
    return res;
}
```
**Complexity Analysis**
- Time: O(n * n) 
- Memory: O(1)

#### Approach 2: Sort Left and Right
We can store ratings on the left and right in the sorted order. As we process soldiers left-to-right, we move their rating from the right bucket to the left bucket.

We can use BST to store sorted ratings, and insert/remove ratings in O(log n). However, standard containers like `set` or `map` won\'t tell us how many elements are smaller than the given rating. You can locate a rating in O(log n), but counting elements smaller or larger will still br linear.

Therefore, we need to implement our own BST that can tell us how many elements are less than a given rating in O(log n). For that, we will track the number of left children for each node, and accumulate it as we search in the tree. See `count` function below for more details.

**Additional Considerations**
We need to balance BST in order to achieve O(n log n) time complexity in the worst case. Also, the simple implementation below does not handle duplicates.

**C++**
```cpp
struct Tree {
    Tree *left = nullptr, *right = nullptr;
    int val = 0, cnt_left = 0;
    Tree(int val) : val(val) {}
};
int numTeams(vector<int>& rating) {
    Tree left(rating.front()), right(rating.back());
    int res = 0, sz = rating.size();
    for (auto i = 1; i < sz - 1; ++i)
        insert(&right, rating[i]);
    for (auto i = 1; i < sz - 1; ++i) {
        remove(&right, rating[i]);
        int l_sm = count(&left, rating[i]), l_lg = i - l_sm;
        int r_sm = count(&right, rating[i]), r_lg = (sz - 1 - i - r_sm) ;
        res += l_sm * r_lg + l_lg * r_sm;
        insert(&left, rating[i]);
    }
    return res;
}
int count(Tree* root, int val) {
    if (root == nullptr)
        return 0;
    if (val < root->val)
        return count(root->left, val);
    return 1 + root->cnt_left + count(root->right, val);
}
Tree* insert(Tree* root, int val) {
    if (root == nullptr)
        return new Tree(val);
    if (val < root->val) {
        ++root->cnt_left;
        root->left = insert(root->left, val);
    }
    else
        root->right = insert(root->right, val);
    return root;
}
Tree* remove(Tree* root, int val) {
    if (root == nullptr)
        return nullptr;
    if (root->val == val) {
        if (root->left == nullptr)
            return root->right;
        auto rightmost = root->left;
        while (rightmost->right != nullptr)
            rightmost = rightmost->right;
        rightmost->right = root->right;
        return root->left;
    }
    if (val < root->val) {
        --root->cnt_left;
        root->left = remove(root->left, val);
    }
    else
        root->right = remove(root->right, val);
    return root;
}
```
**Complexity Analysis**
- Time: O(n log n) in the average case. We need to implement tree balancing to achieve O(n log n) in the worst case. 
- Memory: O(n) for the trees.
</p>


### Java/C++ 100% O(N^2) Easy To Understand With Explanation
- Author: brustembekov
- Creation Date: Sun Mar 29 2020 20:08:04 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 30 2020 10:33:48 GMT+0800 (Singapore Standard Time)

<p>
Since N = 200, brute force is pretty fine for this problem.
But not sure if interviewer is expecting the same. So, optimizing to O(N^2) is pretty easy once we can see the tripplets:

We need to count tripplets {arr[i] < arr[j] < arr[k]} and {arr[i] > arr[j] > arr[k]} where i<j<k. 
**So, let\'s find for every j count of all i and k, so that it will follow either of above 2 conditons and summarize the counts:**

Example: [13, 3, 4, 10, 7, 8]
13:
  - for {arr[i] < arr[j] < arr[k]} tripplets, Nothing smaller from left side. 
  - for {arr[i] > arr[j] > arr[k]} tripplets, Nothing larger from left side.

3:
  - for {arr[i] < arr[j] < arr[k]} tripplets, Nothing smaller from left side.
  - for {arr[i] > arr[j] > arr[k]} tripplets, Nothing smaller from right side.

4:
  - for {arr[i] < arr[j] < arr[k]} tripplets,1 number is smaller and 3 are larger, total = 1*3 = 3 tripplets => {3, 4, 10}, {3, 4, 7}, {3, 4, 8}
  - for {arr[i] > arr[j] > arr[k]} tripplets, Nothing smaller from right side.

10:
  - for {arr[i] < arr[j] < arr[k]} tripplets,Nothing larger from right side.
  - for {arr[i] > arr[j] > arr[k]} tripplets, 1 number is bigger and 2 numbers are smaller, total = 1*2 = 2 tripplets => {13, 10, 7}, {13, 10, 8}

7:
  - for {arr[i] < arr[j] < arr[k]} tripplets, 2 numbers are smaller and 1 is larger, total = 2*1 = 2 => {3, 7, 8}, {4, 7, 8}
  - for {arr[i] > arr[j] > arr[k]} tripplets, Nothing smaller from right side.

8:
  - for {arr[i] < arr[j] < arr[k]} tripplets, Nothing larger from right side. 
  - for {arr[i] > arr[j] > arr[k]} tripplets, Nothing smaller from right side.

Total = 3 + 2 + 2 = 7 tripplets.

**Time complexity: O(N^2)
Space: O(1)**

**[Java]**
	
	public int numTeams(int[] arr) {
        int count = 0;
        int len = arr.length;
        for (int j = 0; j < len; j++) {
            int leftSmaller = 0, rightLarger = 0;
            int leftLarger = 0, rightSmaller = 0;
            for (int i = 0; i < j; i++) {
                if (arr[i] < arr[j]) {
                    leftSmaller++;
                } else if (arr[i] > arr[j]) {
                    leftLarger++;
                }
            }
            for (int k = j + 1; k < len; k++) {
                if (arr[j] < arr[k]) {
                    rightLarger++;
                } else if (arr[j] > arr[k]) {
                    rightSmaller++;
                }
            }
            count += leftSmaller * rightLarger + leftLarger * rightSmaller;
        }
        
        return count;
    }
	
**[C++]**

	public:
		int numTeams(vector<int>& arr) {
			int count = 0;
			int len = arr.size();
			for (int j = 0; j < len; j++) {
				int leftSmaller = 0, rightLarger = 0;
				int leftLarger = 0, rightSmaller = 0;
				for (int i = 0; i < j; i++) {
					if (arr[i] < arr[j]) {
						leftSmaller++;
					} else if (arr[i] > arr[j]) {
						leftLarger++;
					}
				}
				for (int k = j + 1; k < len; k++) {
					if (arr[j] < arr[k]) {
						rightLarger++;
					} else if (arr[j] > arr[k]) {
						rightSmaller++;
					}
				}
				count += leftSmaller * rightLarger + leftLarger * rightSmaller;
			}

			return count;
		}
</p>


### python, >97.98%, simple logic and very detailed explanation
- Author: rmoskalenko
- Creation Date: Mon Apr 06 2020 03:02:30 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 06 2020 03:11:42 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def numTeams(self, rating: List[int]) -> int:
        asc = dsc = 0
        for i,v in enumerate(rating):
            llc = rgc = lgc = rlc =0
            for l in rating[:i]:
                if l < v:
                    llc += 1
                if l > v:
                    lgc += 1
            for r in rating[i+1:]:
                if r > v:
                    rgc += 1
                if r < v:
                    rlc += 1
            asc += llc * rgc
            dsc += lgc * rlc            
        return asc + dsc
```

The logic is so simple - it actually passed all tests on the first try :)

So here is how it works.

We are going to have a main for loop and we are going to pick one element from the `ratings` list at a time. Then we are going to  have two other loops. One is going to scan all elements before and the other one is going to scan all elements after. Those two loops will count number of elements less and greater than the one we picked. So let\'s say we have 2 elements less than the selected one to the left and 3 elements greater to the right. That means the number of ascending sequences we can build is `2 * 3 = 6` . Now we just need to extend the same logic to all descending sequences and return the total.

It may sound crazy, but if you want a more visual picture - imaging walking accross city and crossing streets. You come to a street, look at your left, count, cross half way, look to the right , count, then finish crossing the street and walk to the next one. While the underlying activity is quite different, when you write the code - that visual helps to implement the proper structure.

Ok, so let\'s write the code now:

First, let\'s define the variables. Technically we can use just one counter, but for clarity, let\'s use two, `asc` for ascending and `dsc` for descendig sequences:

```
        asc = dsc = 0
```

Now let\'s start our main loop to scan all elements:

```
        for i,v in enumerate(rating):
```

Inside every cycle we are going to use 4 local counters: `llc` - stands for `Left, Less, Counter` - count of elements less then the current one to the left. `rgc` - `Right, Greater, Counter` - count of elements greater than the current one to the right.  I think you can guess what the other 2 are?

```
            llc = rgc = lgc = rlc =0
```

Ok, now we picked some element, we are going to look to the left and count element less and greater than the current one:

```
            for l in rating[:i]:
                if l < v:
                    llc += 1
                if l > v:
                    lgc += 1
```

And now we look to the right:

```
            for r in rating[i+1:]:
                if r > v:
                    rgc += 1
                if r < v:
                    rlc += 1
```

And update the total counters:

```
            asc += llc * rgc
            dsc += lgc * rlc            
```

Once the main loop is complete, we just return the total total:
```
        return asc + dsc
```

Ok, all done!

We can add some polishing touches:

1. Since we really need just one counter, we can replace `asc` and `dsc` with a single counter `c`.
2. Since we need to have same elements to the left and to the right, we can skip the the first and the last elements in the main loop scan.
3. Replace 2nd `if` with `elsif` - that would reduce number of logical operations - since if the first condition is `True`, the second won\'t be evaluated.

So here is the improved version:


```
class Solution:
    def numTeams(self, rating: List[int]) -> int:
        c = 0
        for i,v in enumerate(rating[1:-1]):
            llc = rgc = lgc = rlc = 0
            for l in rating[:i+1]:
                if l < v:
                    llc += 1
                elif l > v:
                    lgc += 1
            for r in rating[i+2:]:
                if r > v:
                    rgc += 1
                elif r < v:
                    rlc += 1
            c += llc * rgc + lgc * rlc           
        return c
```

I can a few tests and it looks like the 1st version peaked at 94% and the 2nd at 97.98%, so maybe those improvements added a few per cents.

Thanks for reading!
</p>


