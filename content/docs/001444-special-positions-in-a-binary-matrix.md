---
title: "Special Positions in a Binary Matrix"
weight: 1444
#id: "special-positions-in-a-binary-matrix"
---
## Description
<div class="description">
<p>Given a&nbsp;<code>rows x cols</code>&nbsp;matrix&nbsp;<code>mat</code>,&nbsp;where <code>mat[i][j]</code> is either <code>0</code> or <code>1</code>,&nbsp;return <em>the number of special positions in <code>mat</code>.</em></p>

<p>A position <code>(i,j)</code> is called <strong>special</strong>&nbsp;if&nbsp;<code>mat[i][j] == 1</code> and all other elements in row <code>i</code>&nbsp;and column <code>j</code>&nbsp;are <code>0</code> (rows and columns are <strong>0-indexed</strong>).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> mat = [[1,0,0],
&nbsp;             [0,0,<strong>1</strong>],
&nbsp;             [1,0,0]]
<strong>Output:</strong> 1
<strong>Explanation:</strong> (1,2) is a special position because mat[1][2] == 1 and all other elements in row 1 and column 2 are 0.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> mat = [[<strong>1</strong>,0,0],
&nbsp;             [0,<strong>1</strong>,0],
&nbsp;             [0,0,<strong>1</strong>]]
<strong>Output:</strong> 3
<strong>Explanation:</strong> (0,0), (1,1) and (2,2) are special positions. 
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> mat = [[0,0,0,<strong>1</strong>],
&nbsp;             [<strong>1</strong>,0,0,0],
&nbsp;             [0,1,1,0],
&nbsp;             [0,0,0,0]]
<strong>Output:</strong> 2
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> mat = [[0,0,0,0,0],
&nbsp;             [<strong>1</strong>,0,0,0,0],
&nbsp;             [0,<strong>1</strong>,0,0,0],
&nbsp;             [0,0,<strong>1</strong>,0,0],
&nbsp;             [0,0,0,1,1]]
<strong>Output:</strong> 3
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>rows == mat.length</code></li>
	<li><code>cols == mat[i].length</code></li>
	<li><code>1 &lt;= rows, cols &lt;= 100</code></li>
	<li><code>mat[i][j]</code> is <code>0</code> or <code>1</code>.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ 2 passes
- Author: votrubac
- Creation Date: Sun Sep 13 2020 12:02:31 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 15 2020 02:10:33 GMT+0800 (Singapore Standard Time)

<p>
First pass, count number of ones in `rows` and `cols`. 

Second pass, go through the matrix, and for each one (`mat[i][j] == 1`) check it it\'s alone in the corresponding row (`rows[i] == 1`) and column (`cols[j] == 1`).

This is a copycat question to [531. Lonely Pixel I](https://leetcode.com/problems/lonely-pixel-i/). I am wondering why that question is Medium, and this one - Easy? Does it mean that we getting better at this, as a community \uD83E\uDD14 ?

**C++**

```cpp
int numSpecial(vector<vector<int>>& mat) {
    vector<int> rows(mat.size()), cols(mat[0].size());
    for (int i = 0; i < rows.size(); ++i)
        for (int j = 0; j < cols.size(); ++j) {
            if (mat[i][j])
                ++rows[i], ++cols[j];
        }
    int res = 0;
    for (int i = 0; i < rows.size(); ++i)
        for (int j = 0; j < cols.size(); ++j)
            if (mat[i][j] && rows[i] == 1 && cols[j] == 1)
                ++res;
    return res;
}
```
</p>


### Java Simple Loop
- Author: hobiter
- Creation Date: Sun Sep 13 2020 12:02:48 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 15 2020 13:04:13 GMT+0800 (Singapore Standard Time)

<p>
```
    public int numSpecial(int[][] mat) {
        int m = mat.length, n = mat[0].length, res = 0, col[] = new int[n], row[] = new int[m];
        for (int i = 0; i < m; i++) 
            for (int j = 0; j < n; j++) 
                if (mat[i][j] == 1){
                    col[j]++;
                    row[i]++;
                } 
        for (int i = 0; i < m; i++) 
            for (int j = 0; j < n; j++) 
                if (mat[i][j] == 1 && row[i] == 1 && col[j] == 1) res++;
        return res;
    }
```
</p>


