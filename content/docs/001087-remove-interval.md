---
title: "Remove Interval"
weight: 1087
#id: "remove-interval"
---
## Description
<div class="description">
<p>Given a <strong>sorted</strong> list of disjoint <code>intervals</code>, each interval <code>intervals[i] = [a, b]</code> represents the set of real numbers&nbsp;<code>x</code> such that&nbsp;<code>a &lt;= x &lt; b</code>.</p>

<p>We remove the intersections between any interval in <code>intervals</code> and the interval <code>toBeRemoved</code>.</p>

<p>Return a <strong>sorted</strong>&nbsp;list of <code>intervals</code> after all such removals.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[0,2],[3,4],[5,7]], toBeRemoved = [1,6]
<strong>Output:</strong> [[0,1],[6,7]]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[0,5]], toBeRemoved = [2,3]
<strong>Output:</strong> [[0,2],[3,5]]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[-5,-4],[-3,-2],[1,2],[3,5],[8,9]], toBeRemoved = [-1,4]
<strong>Output:</strong> [[-5,-4],[-3,-2],[4,5],[8,9]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= intervals.length &lt;= 10^4</code></li>
	<li><code>-10^9 &lt;= intervals[i][0] &lt; intervals[i][1] &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Math (math)
- Line Sweep (line-sweep)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Sweep Line, One Pass.

**Best Possible Time Complexity**

> What is the best possible time complexity here?

The input is sorted, that usually means _at least_ linear time complexity.
Is it possible to do $$\mathcal{O}(\log N)$$? 
No, because to copy input elements into output 
still requires $$\mathcal{O}(N)$$ time.

**Sweep Line**

[Sweep Line algorithm](https://en.wikipedia.org/wiki/Sweep_line_algorithm) 
is a sort of geometrical visualisation. 
Let's imagine a vertical line which is swept across the plane, 
stopping at some points. 
That could create various situations, 
and the decision to make depends on the stop point.

![line](../Figures/1272/sweep2.png)

**Algorithm**

Let's sweep the line by iterating over input intervals 
and consider what it could bring to us.

- Current interval has no overlaps with toBeRemoved one.
That means there is nothing to take care about, 
just update the output.

![line](../Figures/1272/no_overlaps.png)

- Second situation is when toBeRemoved interval is inside of the current
interval. Then one has to add two non-overlapping parts of the 
current interval in the output.

![line](../Figures/1272/inside2.png)

- "Left" overlap.

![line](../Figures/1272/left_overlap.png)

- "Right" overlap.

![line](../Figures/1272/right_overlap.png)

And here we are, all situations are covered, the job is done. 

**Implementation**

<iframe src="https://leetcode.com/playground/qgA54Pb8/shared" frameBorder="0" width="100%" height="500" name="qgA54Pb8"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since it's one pass along
the input array.
 
* Space complexity : $$\mathcal{O}(N)$$ to keep the output.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] 12 and 11 liners w/ brief comments and analysis.
- Author: rock
- Creation Date: Sun Dec 01 2019 00:07:46 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 02 2019 01:53:55 GMT+0800 (Singapore Standard Time)

<p>
Check overlap.

```java
    public List<List<Integer>> removeInterval(int[][] intervals, int[] toBeRemoved) {
        List<List<Integer>> ans = new ArrayList<>();
        for (int[] i : intervals) {
            if (i[1] <= toBeRemoved[0] || i[0] >= toBeRemoved[1]) { // no overlap.
                ans.add(Arrays.asList(i[0], i[1]));
            }else { // i[1] > toBeRemoved[0] && i[0] < toBeRemoved[1].
                if(i[0] < toBeRemoved[0]) // left end no overlap.
                    ans.add(Arrays.asList(i[0], toBeRemoved[0]));
                if (i[1] > toBeRemoved[1]) // right end no overlap.
                    ans.add(Arrays.asList(toBeRemoved[1], i[1]));
            }
        }
        return ans;
    }
```
```python
    def removeInterval(self, intervals: List[List[int]], toBeRemoved: List[int]) -> List[List[int]]:
        ans = []
        for lo, hi in intervals:
            remove_lo, remove_hi = toBeRemoved
            if hi <= remove_lo or lo >= remove_hi:
                ans.append([lo, hi])
            else:
                if lo < remove_lo:
                    ans.append([lo, remove_lo])
                if hi > remove_hi:
                    ans.append([remove_hi, hi])
        return ans
```
**Analysis**
Time: O(n), space: O(1) - excluding return list, where n = intervals.length.
</p>


### 1-liner
- Author: StefanPochmann
- Creation Date: Sun Dec 01 2019 05:55:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 01 2019 08:23:04 GMT+0800 (Singapore Standard Time)

<p>
```
def removeInterval(self, intervals, (A, B)):
    return [[x, y] for a, b in intervals for x, y in ((a, min(b, A)), (max(a, B), b)) if x < y]
```

Or without list comprehension:
```
def removeInterval(self, intervals, (A, B)):
    res = []
    for a, b in intervals:
        if a < A:
            res += [a, min(b, A)],
        if b > B:
            res += [max(a, B), b],
    return res
```

[a,b) \ [A,B)
= [a,b) &cap; &not;[A,B)
= [a,b) &cap; ((-&infin;,A) &cup; [B,&infin;))
= ([a,b) &cap; (-&infin;,A)) &cup; ([a,b) &cap; [B,&infin;))
= [max(a,-&infin;), min(b,A)) &cup; [max(a,B), min(b,&infin;))
= [a, min(b,A)) &cup; [max(a,B), b)

Intuitively: If both endpoints of an interval get removed, then also everything in between, so the complete interval gets removed. So in order to *not* be completely removed, at least the left or the right endpoint have to be not removed. If the left endpoint stays, then it extends to either `b` or `A` (whichever comes first). Meaning `[a, min(b, A))`. Similarly for the right endpoint, meaning `[max(a, B), b)`.
</p>


### C++ 3 cases
- Author: votrubac
- Creation Date: Sun Dec 01 2019 05:56:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 01 2019 05:56:26 GMT+0800 (Singapore Standard Time)

<p>
Well, I am not sure if this problem should be *Medium*. Would be more interesing if we have an array of `toBeRemoved`.
```
vector<vector<int>> removeInterval(vector<vector<int>>& intervals, vector<int>& toBeRemoved) {
    vector<vector<int>> res;
    auto start = toBeRemoved[0], end = toBeRemoved[1];
    for (auto &v : intervals) {
        if (v[1] <= start || v[0] >= end) res.push_back(v);
        else {
            if (v[0] < start) res.push_back({v[0], start});
            if (v[1] > end) res.push_back({end, v[1]});
        }
    }
    return res;
}
```
</p>


