---
title: "Masking Personal Information"
weight: 772
#id: "masking-personal-information"
---
## Description
<div class="description">
<p>We are given a&nbsp;personal information string <code>S</code>, which may represent&nbsp;either <strong>an email address</strong> or <strong>a phone number.</strong></p>

<p>We would like to mask this&nbsp;personal information according to the&nbsp;following rules:</p>

<p><br />
<u><strong>1. Email address:</strong></u></p>

<p>We define a&nbsp;<strong>name</strong> to be a string of <code>length &ge; 2</code> consisting&nbsp;of only lowercase letters&nbsp;<code>a-z</code> or uppercase&nbsp;letters&nbsp;<code>A-Z</code>.</p>

<p>An email address starts with a name, followed by the&nbsp;symbol <code>&#39;@&#39;</code>, followed by a name, followed by the&nbsp;dot&nbsp;<code>&#39;.&#39;</code>&nbsp;and&nbsp;followed by a name.&nbsp;</p>

<p>All email addresses are&nbsp;guaranteed to be valid and in the format of&nbsp;<code>&quot;name1@name2.name3&quot;.</code></p>

<p>To mask an email, <strong>all names must be converted to lowercase</strong> and <strong>all letters between the first and last letter of the first name</strong> must be replaced by 5 asterisks <code>&#39;*&#39;</code>.</p>

<p><br />
<u><strong>2. Phone number:</strong></u></p>

<p>A phone number is a string consisting of&nbsp;only the digits <code>0-9</code> or the characters from the set <code>{&#39;+&#39;, &#39;-&#39;, &#39;(&#39;, &#39;)&#39;, &#39;&nbsp;&#39;}.</code>&nbsp;You may assume a phone&nbsp;number contains&nbsp;10 to 13 digits.</p>

<p>The last 10 digits make up the local&nbsp;number, while the digits before those make up the country code. Note that&nbsp;the country code is optional. We want to expose only the last 4 digits&nbsp;and mask all other&nbsp;digits.</p>

<p>The local&nbsp;number&nbsp;should be formatted and masked as <code>&quot;***-***-1111&quot;,&nbsp;</code>where <code>1</code> represents the exposed digits.</p>

<p>To mask a phone number with country code like <code>&quot;+111 111 111 1111&quot;</code>, we write it in the form <code>&quot;+***-***-***-1111&quot;.</code>&nbsp; The <code>&#39;+&#39;</code>&nbsp;sign and the first <code>&#39;-&#39;</code>&nbsp;sign before the local number should only exist if there is a country code.&nbsp; For example, a 12 digit phone number mask&nbsp;should start&nbsp;with <code>&quot;+**-&quot;</code>.</p>

<p>Note that extraneous characters like <code>&quot;(&quot;, &quot;)&quot;, &quot; &quot;</code>, as well as&nbsp;extra dashes or plus signs not part of the above formatting scheme should be removed.</p>

<p>&nbsp;</p>

<p>Return the correct &quot;mask&quot; of the information provided.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>&quot;LeetCode@LeetCode.com&quot;
<strong>Output: </strong>&quot;l*****e@leetcode.com&quot;
<strong>Explanation:&nbsp;</strong>All names are converted to lowercase, and the letters between the
&nbsp;            first and last letter of the first name is replaced by 5 asterisks.
&nbsp;            Therefore, &quot;leetcode&quot; -&gt; &quot;l*****e&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>&quot;AB@qq.com&quot;
<strong>Output: </strong>&quot;a*****b@qq.com&quot;
<strong>Explanation:&nbsp;</strong>There must be 5 asterisks between the first and last letter 
&nbsp;            of the first name &quot;ab&quot;. Therefore, &quot;ab&quot; -&gt; &quot;a*****b&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>&quot;1(234)567-890&quot;
<strong>Output: </strong>&quot;***-***-7890&quot;
<strong>Explanation:</strong>&nbsp;10 digits in the phone number, which means all digits make up the local number.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong>&quot;86-(10)12345678&quot;
<strong>Output: </strong>&quot;+**-***-***-5678&quot;
<strong>Explanation:</strong>&nbsp;12 digits, 2 digits for country code and 10 digits for local number. 
</pre>

<p><strong>Notes:</strong></p>

<ol>
	<li><code>S.length&nbsp;&lt;=&nbsp;40</code>.</li>
	<li>Emails have length at least 8.</li>
	<li>Phone numbers have length at least 10.</li>
</ol>

</div>

## Tags
- String (string)

## Companies
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Direct [Accepted]

**Intuition and Algorithm**

We perform the algorithm as described.

First, to check if information is an email, we check whether it contains a `'@'`.  (There are many different tests: we could check for letters versus digits, for example.)

If we have an email, we should replace the first name with the first letter of that name, followed by 5 asterisks, followed by the last letter of that name.

If we have a phone number, we should collect all the digits and then format it according to the description.

<iframe src="https://leetcode.com/playground/6czi9rvF/shared" frameBorder="0" width="100%" height="327" name="6czi9rvF"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(1)$$, if we consider the length of `S` as bounded by a constant.

* Space Complexity: $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Easy and Concise 
- Author: lee215
- Creation Date: Sun May 06 2018 11:05:37 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 21:20:14 GMT+0800 (Singapore Standard Time)

<p>
**C++:**
```
    vector<string> country = {"", "+*-", "+**-", "+***-"};
    string maskPII(string S) {
        string res;
        int at = S.find("@");
        if (at != string::npos) {
            transform(S.begin(), S.end(), S.begin(), ::tolower);
            return S.substr(0, 1) + "*****" + S.substr(at - 1);
        }
        S = regex_replace(S, regex("[^0-9]"), "");
        return country[S.size() - 10] + "***-***-" + S.substr(S.size() - 4);
    }

```

**Java:**
```
    String[] country = {"", "+*-", "+**-", "+***-"};
    public String maskPII(String S) {
        int at = S.indexOf("@");
        if (at > 0) {
            S = S.toLowerCase();
            return (S.charAt(0) + "*****" + S.substring(at - 1)).toLowerCase();
        }
        S = S.replaceAll("[^0-9]", "");
        return country[S.length() - 10] + "***-***-" + S.substring(S.length() - 4);
    }

```


**Python:**
```
    def maskPII(self, S):
        at = S.find(\'@\')
        if at >= 0:
            return (S[0] + "*" * 5 + S[at - 1:]).lower()
        S = "".join(i for i in S if i.isdigit())
        return ["", "+*-", "+**-", "+***-"][len(S) - 10] + "***-***-" + S[-4:]
```

</p>


### Short python solution
- Author: solomon3
- Creation Date: Sun May 06 2018 11:22:37 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 19:35:28 GMT+0800 (Singapore Standard Time)

<p>
```
        if \'@\' in S:
            name, domain = S.lower().split(\'@\')
            return \'%s*****%s@%s\'%(name[0], name[-1], domain)
        else:
            s=re.sub(\'[\(\)\-\+ ]\',\'\', S)
            coun = \'+%s-\'%(\'*\'*(len(s)-10)) if len(s)>10 else \'\'
            return coun + \'***-***-\'+s[-4:]
```
</p>


### Straightforward C++
- Author: claytonjwong
- Creation Date: Tue May 08 2018 05:09:28 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 13:56:34 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    string maskPII(string S, string ans="", string nums="") {
        auto pos=S.find(\'@\');
        if (pos==string::npos){
            for (auto c: S) if (isdigit(c))
                nums.push_back(c);
            int N=(int)nums.size();
            ans=N==13 ? "+***-" : N==12 ? "+**-" : N==11 ? "+*-" : "";
            ans.append("***-***-"+nums.substr(N-4));
        } else {
            ans=string(1,S[0])+"*****"+S.substr(pos-1);
            transform(ans.begin(),ans.end(),ans.begin(),::tolower);
        }
        return ans;
    }
};
```
</p>


