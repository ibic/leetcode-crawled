---
title: "Design a File Sharing System"
weight: 1389
#id: "design-a-file-sharing-system"
---
## Description
<div class="description">
<p>We will use a file-sharing system to share a very large file which consists of <code>m</code> small <b>chunks</b> with IDs from <code>1</code> to <code>m</code>.</p>

<p>When users join the system, the system should assign&nbsp;<b>a unique</b> ID to them. The unique ID should be used <b>once</b> for each user, but when a user leaves the system, the ID can be <b>reused</b> again.</p>

<p>Users can request a certain chunk of the file, the system should return a list of IDs of all the users who own this chunk. If the user receive a non-empty list of IDs, they receive the requested chunk successfully.</p>

<p><br />
Implement the <code>FileSharing</code> class:</p>

<ul>
	<li><code>FileSharing(int m)</code> Initializes the object with a file of <code>m</code> chunks.</li>
	<li><code>int join(int[] ownedChunks)</code>: A new user joined the system owning some chunks of the file, the system should assign an id to the user which is the <b>smallest positive integer</b> not taken by any other user. Return the assigned id.</li>
	<li><code>void leave(int userID)</code>: The user with <code>userID</code> will leave the system, you cannot take file chunks from them anymore.</li>
	<li><code>int[] request(int userID, int chunkID)</code>: The user&nbsp;<code>userID</code> requested the file chunk with <code>chunkID</code>. Return a list of the IDs of all users that own this chunk sorted in ascending order.</li>
</ul>

<p>&nbsp;</p>

<p><b>Follow-ups:</b></p>

<ul>
	<li>What happens if the system identifies the user by their&nbsp;IP address instead of their unique ID and users disconnect and connect from the system with the same IP?</li>
	<li>If the users in the system join and leave the system frequently without requesting any chunks, will your solution still be efficient?</li>
	<li>If all each user join the system one time, request all files and then leave, will your solution still be efficient?</li>
	<li>If the system will be used to share <code>n</code> files where the <code>ith</code> file consists of <code>m[i]</code>, what are the changes you have to do?</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example:</strong></p>

<pre>
<b>Input:</b>
[&quot;FileSharing&quot;,&quot;join&quot;,&quot;join&quot;,&quot;join&quot;,&quot;request&quot;,&quot;request&quot;,&quot;leave&quot;,&quot;request&quot;,&quot;leave&quot;,&quot;join&quot;]
[[4],[[1,2]],[[2,3]],[[4]],[1,3],[2,2],[1],[2,1],[2],[[]]]
<b>Output:</b>
[null,1,2,3,[2],[1,2],null,[],null,1]
<b>Explanation:</b>
FileSharing fileSharing = new FileSharing(4); // We use the system to share a file of 4 chunks.

fileSharing.join([1, 2]);    // A user who has chunks [1,2] joined the system, assign id = 1 to them and return 1.

fileSharing.join([2, 3]);    // A user who has chunks [2,3] joined the system, assign id = 2 to them and return 2.

fileSharing.join([4]);       // A user who has chunk [4] joined the system, assign id = 3 to them and return 3.

fileSharing.request(1, 3);   // The user with id = 1 requested the third file chunk, as only the user with id = 2 has the file, return [2] . Notice that user 1 now has chunks [1,2,3].

fileSharing.request(2, 2);   // The user with id = 2 requested the second file chunk, users with ids [1,2] have this chunk, thus we return [1,2].

fileSharing.leave(1);        // The user with id = 1 left the system, all the file chunks with them are no longer available for other users.

fileSharing.request(2, 1);   // The user with id = 2 requested the first file chunk, no one in the system has this chunk, we return empty list [].

fileSharing.leave(2);        // The user with id = 2 left the system.

fileSharing.join([]);        // A user who doesn&#39;t have any chunks joined the system, assign id = 1 to them and return 1. Notice that ids 1 and 2 are free and we can reuse them.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= m &lt;= 10^5</code></li>
	<li><code>0 &lt;=&nbsp;ownedChunks.length &lt;= min(100, m)</code></li>
	<li><code>1 &lt;= ownedChunks[i] &lt;= m</code></li>
	<li>Values of&nbsp;<code>ownedChunks</code> are unique.</li>
	<li><code>1 &lt;=&nbsp;chunkID &lt;= m</code></li>
	<li><code>userID</code> is guaranteed to be a user in the system if you <strong>assign</strong> the IDs <strong>correctly</strong>.&nbsp;</li>
	<li>At most <code>10^4</code>&nbsp;calls will be made to&nbsp;<code>join</code>,&nbsp;<code>leave</code>&nbsp;and&nbsp;<code>request</code>.</li>
	<li>Each call to <code>leave</code> will have a matching call for <code>join</code>.</li>
</ul>
</div>

## Tags
- Array (array)
- Design (design)

## Companies
- Twitch - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java PriorityQueue + TreeMap
- Author: compton_scatter
- Creation Date: Thu Jul 02 2020 12:27:44 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 02 2020 12:52:15 GMT+0800 (Singapore Standard Time)

<p>
1) Use a min priority queue to store ids that are available to use
2) Use a TreeMap to store the chunks stored by each user id

```
class FileSharing {

    PriorityQueue<Integer> pq = new PriorityQueue<>();
    Map<Integer, Set<Integer>> userToChunks = new TreeMap<>();

    public FileSharing(int m) {
        pq.add(1);
    }

    public int join(List<Integer> ownedChunks) {
        int id = pq.poll();
        if (pq.isEmpty()) {
            pq.add(id + 1);
        }
        userToChunks.put(id, new HashSet<>(ownedChunks));
        return id;
    }

    public void leave(int userID) {
        pq.add(userID);
        userToChunks.remove(userID);
    }

    public List<Integer> request(int userID, int chunkID) {
        List<Integer> res = new LinkedList<>();
        for (Integer user : userToChunks.keySet()) {
            if (userToChunks.get(user).contains(chunkID)) {
                res.add(user);
            }
        }     
        if (res.size() != 0) userToChunks.get(userID).add(chunkID);
        return res;
    }
    
}
```
</p>


### [Python3] MinHeap + SortedSet
- Author: blackspinner
- Creation Date: Sat Jul 04 2020 05:33:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 04 2020 05:33:02 GMT+0800 (Singapore Standard Time)

<p>
```
from sortedcontainers import SortedSet

class FileSharing:

    def __init__(self, m: int):
        self.users = {}
        self.chunks = {}
        self.cur_id = 1
        self.used_ids = []
        heapify(self.used_ids)
        
    def join(self, ownedChunks: List[int]) -> int:
        if len(self.used_ids) != 0:
            user_id = heappop(self.used_ids)
        else:
            user_id = self.cur_id
            self.cur_id += 1
        self.users[user_id] = ownedChunks
        for chuck in ownedChunks:
            if not chuck in self.chunks:
                self.chunks[chuck] = SortedSet()
            self.chunks[chuck].add(user_id)
        return user_id
        
    def leave(self, userID: int) -> None:
        for chunk in self.users[userID]:
            if chunk in self.chunks and userID in self.chunks[chunk]:
                self.chunks[chunk].remove(userID)
        del self.users[userID]
        heappush(self.used_ids, userID)

    def request(self, userID: int, chunkID: int) -> List[int]:
        res = []
        if chunkID in self.chunks and len(self.chunks[chunkID]) > 0:
            res = list(self.chunks[chunkID])
            self.chunks[chunkID].add(userID)
            self.users[userID].append(chunkID)
        return res
```
</p>


### Java HashMap & PriorityQueue
- Author: varunsjsu
- Creation Date: Fri Jul 03 2020 06:01:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 03 2020 06:01:41 GMT+0800 (Singapore Standard Time)

<p>
```
class FileSharing {
  Map<Integer, Set<Integer>> chunkToUserMap;
  Map<Integer, Set<Integer>> userToChunkMap;
  int id;
  PriorityQueue<Integer> availableIds;
  public FileSharing(int m) {
    id = 1;
    chunkToUserMap = new HashMap<>();
    userToChunkMap = new HashMap<>();
    for (int i = 1; i <= m; i++) {
      chunkToUserMap.put(i, new HashSet<>());
    }
    availableIds = new PriorityQueue<>();
  }

  public int join(List<Integer> ownedChunks) {
    int userId = getUserId();
    userToChunkMap.put(userId, new HashSet<>(ownedChunks));
    for (Integer chunk : ownedChunks) {
      chunkToUserMap.get(chunk).add(userId);
    }
    return userId;
  }
  
  private int getUserId() {
    if (availableIds.isEmpty()) {
      return id++;
    }
    return availableIds.poll();
  }

  public void leave(int userID) {
    Set<Integer> chunksOwned = userToChunkMap.get(userID);
    userToChunkMap.remove(userID);
    for (Integer chunk : chunksOwned) {
      chunkToUserMap.get(chunk).remove(userID);
    }
    availableIds.add(userID);
  }

  public List<Integer> request(int userID, int chunkID) {
    Set<Integer> usersOwningChunk = chunkToUserMap.get(chunkID);
    Iterator<Integer> iterator = usersOwningChunk.iterator();
    List<Integer> users = new ArrayList<>();
    while (iterator.hasNext()) {
      users.add(iterator.next());
    }
    if (!usersOwningChunk.contains(userID) && usersOwningChunk.size() > 0) {
      userToChunkMap.get(userID).add(chunkID);
      chunkToUserMap.get(chunkID).add(userID);
    }
    Collections.sort(users);
    return users;
  }
}

/**
 * Your FileSharing object will be instantiated and called as such:
 * FileSharing obj = new FileSharing(m);
 * int param_1 = obj.join(ownedChunks);
 * obj.leave(userID);
 * List<Integer> param_3 = obj.request(userID,chunkID);
 */
 ```
</p>


