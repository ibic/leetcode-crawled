---
title: "Shortest Word Distance"
weight: 227
#id: "shortest-word-distance"
---
## Description
<div class="description">
<p>Given a list of words and two words <em>word1</em> and <em>word2</em>, return the shortest distance between these two words in the list.</p>

<p><strong>Example:</strong><br />
Assume that words = <code>[&quot;practice&quot;, &quot;makes&quot;, &quot;perfect&quot;, &quot;coding&quot;, &quot;makes&quot;]</code>.</p>

<pre>
<b>Input:</b> <em>word1</em> = <code>&ldquo;coding&rdquo;</code>, <em>word2</em> = <code>&ldquo;practice&rdquo;</code>
<b>Output:</b> 3
</pre>

<pre>
<b>Input:</b> <em>word1</em> = <code>&quot;makes&quot;</code>, <em>word2</em> = <code>&quot;coding&quot;</code>
<b>Output:</b> 1
</pre>

<p><strong>Note:</strong><br />
You may assume that <em>word1</em> <strong>does not equal to</strong> <em>word2</em>, and <em>word1</em> and <em>word2</em> are both in the list.</p>

</div>

## Tags
- Array (array)

## Companies
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- LinkedIn - 8 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)

## Official Solution
## Solution

This is a straight-forward coding problem. The distance between any two positions $$i_1$$ and $$i_2$$ in an array is $$|i_1 - i_2|$$. To find the shortest distance between `````word1````` and `````word2`````, we need to traverse the input array and find all occurrences $$i_1$$ and $$i_2$$ of the two words, and check if $$|i_1 - i_2|$$ is less than the minimum distance computed so far.

---
#### Approach #1 (Brute Force)

**Algorithm**

A naive solution to this problem is to go through the entire array looking for the first word. Every time we find an occurrence of the first word, we search the entire array for the closest occurrence of the second word.

<iframe src="https://leetcode.com/playground/45L9BU7x/shared" frameBorder="0" width="100%" height="310" name="45L9BU7x"></iframe>

**Complexity Analysis**

The time complexity is $$O(n^2)$$, since for every occurrence of `word1`, we traverse the entire array in search for the closest occurrence of `word2`.

Space complexity is $$O(1)$$, since no additional space is used.

---
#### Approach #2 (One-pass)

**Algorithm**

We can greatly improve on the brute-force approach by keeping two indices `i1` and `i2` where we store the *most recent* locations of `word1` and `word2`. Each time we find a new occurrence of one of the words, we do not need to search the entire array for the other word, since we already have the index of its most recent occurrence.

<iframe src="https://leetcode.com/playground/RXQFXoSf/shared" frameBorder="0" width="100%" height="361" name="RXQFXoSf"></iframe>

**Complexity Analysis**

The time complexity is $$O(n)$$. This problem is inherently linear; we cannot do better than $$O(n)$$ because at the very least, we have to read the entire input.

Space complexity is $$O(1)$$, since no additional space is allocated.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### AC Java clean solution
- Author: jeantimex
- Creation Date: Wed Aug 05 2015 12:33:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 07:15:05 GMT+0800 (Singapore Standard Time)

<p>
    public int shortestDistance(String[] words, String word1, String word2) {
        int p1 = -1, p2 = -1, min = Integer.MAX_VALUE;
        
        for (int i = 0; i < words.length; i++) {
            if (words[i].equals(word1)) 
                p1 = i;

            if (words[i].equals(word2)) 
                p2 = i;
                
            if (p1 != -1 && p2 != -1)
                min = Math.min(min, Math.abs(p1 - p2));
        }
        
        return min;
    }
</p>


### Java: only need to keep one index
- Author: BIO2CS
- Creation Date: Sat Oct 03 2015 11:12:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 15 2018 05:44:13 GMT+0800 (Singapore Standard Time)

<p>
    public int shortestDistance(String[] words, String word1, String word2) {
       int index = -1, minDistance = Integer.MAX_VALUE;
       for (int i = 0; i < words.length; i++) {
          if (words[i].equals(word1) || words[i].equals(word2)) {
             if (index != -1 && !words[index].equals(words[i])) {
                minDistance = Math.min(minDistance, i - index);
              }
              index = i;
          }
       }
       return minDistance;
    }
</p>


### Java solution using minimum difference between 2 sorted arrays
- Author: ammv
- Creation Date: Wed Aug 05 2015 08:52:55 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 05 2015 08:52:55 GMT+0800 (Singapore Standard Time)

<p>
   

Creating two lists storing indexes of each occurrence of the `word1` and `word2` accordingly. After that finding minimum difference between two elements from these lists.

    public class Solution {
            public int shortestDistance(String[] words, String word1, String word2) {
                List<Integer> w1occ=new ArrayList<Integer>();
                List<Integer> w2occ=new ArrayList<Integer>();
                
                for (int i=0; i<words.length; ++i){
                    if (words[i].equals(word1)){
                        w1occ.add(i);
                    }
                    if (words[i].equals(word2)){
                        w2occ.add(i);
                    }
                }
                
                int min=words.length;
                int p1=0;
                int p2=0;
                while (p1<w1occ.size() && p2<w2occ.size()){
                    min=Math.min(Math.abs(w1occ.get(p1)-w2occ.get(p2)), min);
                    if (w1occ.get(p1)<w2occ.get(p2)){
                        p1++;
                    } else 
                        p2++;
                }
                return min;
            }
        }
</p>


