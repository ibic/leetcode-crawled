---
title: "Connecting Cities With Minimum Cost"
weight: 1009
#id: "connecting-cities-with-minimum-cost"
---
## Description
<div class="description">
<p>There are <code>N</code> cities numbered from 1 to <code>N</code>.</p>

<p>You are given <code>connections</code>, where each <code>connections[i] = [city1, city2, cost]</code>&nbsp;represents the cost to connect <code>city1</code> and <code>city2</code> together.&nbsp; (A <em>connection</em> is bidirectional: connecting <code>city1</code> and <code>city2</code> is the same as connecting <code>city2</code> and <code>city1</code>.)</p>

<p>Return the minimum cost so that for every pair of cities, there exists a path&nbsp;of connections (possibly of length 1) that connects those two cities together.&nbsp; The cost is the sum of the connection costs used. If the task is impossible, return -1.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/04/20/1314_ex2.png" style="width: 161px; height: 141px;" /></p>

<pre>
<strong>Input: </strong>N = 3, connections = [[1,2,5],[1,3,6],[2,3,1]]
<strong>Output: </strong>6
<strong>Explanation: </strong>
Choosing any 2 edges will connect all cities so we choose the minimum 2.
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/04/20/1314_ex1.png" style="width: 136px; height: 91px;" /></p>

<pre>
<strong>Input: </strong>N = 4, connections = [[1,2,3],[3,4,4]]
<strong>Output: </strong>-1
<strong>Explanation: </strong>
There is no way to connect all cities even if all edges are used.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= N &lt;= 10000</code></li>
	<li><code>1 &lt;= connections.length &lt;= 10000</code></li>
	<li><code>1 &lt;= connections[i][0], connections[i][1] &lt;= N</code></li>
	<li><code>0 &lt;= connections[i][2] &lt;= 10^5</code></li>
	<li><code>connections[i][0] != connections[i][1]</code></li>
</ol>

</div>

## Tags
- Union Find (union-find)
- Graph (graph)

## Companies
- Amazon - 3 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Kruskal’s Minimum Spanning Tree Algorithm with Union Find
- Author: yuanb10
- Creation Date: Sun Jul 28 2019 00:32:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 28 2019 05:24:05 GMT+0800 (Singapore Standard Time)

<p>
We use Kruskal\u2019s algorithm to generate a minimum spanning tree for the graph. Use Union-Find to detect cycle.

Idea is simple:
1. Sort edges to no-descresing order
2. Pick the smallest edge that does not form a cycle
3. Repeat until MST is formed and every node is connected.

Implemented Union-Find with path comression to improve efficiency. 

There are tons of materials online about the proof of correctness and analysis of this algorithm. Feel free to check them around.

Hope this helps.

```
class Solution {
    
    int[] parent;
    int n;
    
    private void union(int x, int y) {
        int px = find(x);
        int py = find(y);
        
        if (px != py) {
            parent[px] = py;
            n--;
        }
    }
    
    private int find(int x) {
        if (parent[x] == x) {
            return parent[x];
        }
        parent[x] = find(parent[x]); // path compression
        return parent[x];
    }
    
    public int minimumCost(int N, int[][] connections) {
        parent = new int[N + 1];
        n = N;
        for (int i = 0; i <= N; i++) {
            parent[i] = i;
        }
        
        Arrays.sort(connections, (a, b) -> (a[2] - b[2]));
        
        int res = 0;
        
        for (int[] c : connections) {
            int x = c[0], y = c[1];
            if (find(x) != find(y)) {
                res += c[2];
                union(x, y);
            }
        }
        
        return n == 1 ? res : -1;
    }
}
```
</p>


### C++ Prim's Algorithm(Heap) and Kruskal's Algorithm(UF)
- Author: ginnyww
- Creation Date: Fri Jan 03 2020 04:14:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 14:36:58 GMT+0800 (Singapore Standard Time)

<p>
**Prim\'s Algorithm**
Time complexity ```O((E+N)logN)``` where E is the number of edges.
Prim\'s algorithm is very similar to Dijkstra algorithm.
The only difference is that instead of keeping a list of minimum total distance and updating it with
```dist[v] = min(dist[v], weight(u,v) + dist[u]);```
we keep a list of minimum cost for the current node and update it with
```minCost[v] = min(minCost[v], cost(u,v));```
to find the light edge(edge that add one more node to the tree with minimum cost) in the priority queue.
```
typedef pair<int,int> pii;
class Solution {
public:
    int minimumCost(int N, vector<vector<int>>& connections) {
        vector<vector<pii>> mp(N+1);
        vector<bool> visited(N+1, false);
        vector<int> minCost(N+1, INT_MAX);
        for (auto& edge : connections) {
            mp[edge[0]].push_back({edge[2], edge[1]});
            mp[edge[1]].push_back({edge[2], edge[0]});
        }
        priority_queue<pii, vector<pii>, greater<pii>> pq;
        pq.push({0,1});
        int numVisited = 0, res = 0;
        while (numVisited < N && !pq.empty()) {
            int cost = pq.top().first, city = pq.top().second;
            pq.pop();
            if (visited[city]) continue;
            visited[city] = true;
            numVisited++;
            res += cost;
            for (auto& n : mp[city]) {
                cost = n.first;
                city = n.second;
                if (!visited[city] && cost < minCost[city]) {
                    minCost[city] = cost;
                    pq.push({cost, city});
                }
            }
        }
        return numVisited == N ? res : -1;
    }
};
```

**Kruskal\'s Algorithm**
Kruskal\'s algorithm is a greedy algorithm using Union Find.
Time complexity: ```O(ElogE)```
```
class Solution {
public:
    vector<int> parent;
    static bool comp(const vector<int>& a, const vector<int>& b) {
        return (a[2] < b[2]);
    }
    
    int find(int i) {
        if (parent[i] != i) parent[i] = find(parent[i]);
        return parent[i];
    }
    
    int minimumCost(int N, vector<vector<int>>& connections) {
        sort(connections.begin(), connections.end(), comp);
        parent.resize(N+1, 0);
        for (int i = 1; i <= N; i++) {
            parent[i] = i;
        }
        int res = 0, count = 1;
        for (auto& c : connections) {
            int rx = find(c[0]), ry = find(c[1]);
            if (rx != ry) {
                res += c[2];
                parent[ry] = rx;
                count++;
            }
            if (count == N) return res;
        }
        return -1;
    }
};
```
</p>


### Python simple Union Find Solution
- Author: sukeyzhou
- Creation Date: Sun Jul 28 2019 00:11:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 28 2019 02:34:07 GMT+0800 (Singapore Standard Time)

<p>
1. Sort `connections` by cost
2. Iterate `connections`, if `union(city1,city2)`, add `cost` to `ans`
3. Check if there is only one group of connected components

* Time Complexity: O(N log N)
Note that Union operation takes constant time when UnionFind is implemented with both path compression and union by rank.
* Space Complexity: O(N)

```
    def minimumCost(self, N: int, connections: List[List[int]]) -> int:
        parents = [x for x in range(N)]
        ranks = [1] * N
        
        def find(u):
            while u != parents[u]:
                parents[u] = parents[parents[u]]
                u = parents[u]
            return u
        
        def union(u, v):
            root_u, root_v = find(u), find(v)
            if root_u == root_v: return False
            if ranks[root_v] > ranks[root_u]:
                root_u, root_v = root_v, root_u
            parents[root_v] = root_u
            ranks[root_u] += ranks[root_v]
            return True
        
        connections.sort(key = lambda x: x[2])
        ans = 0
        for u, v, val in connections:
            if union(u-1, v-1): ans += val
        groups = len({find(x) for x in parents})
        return ans if groups == 1 else -1
```
</p>


