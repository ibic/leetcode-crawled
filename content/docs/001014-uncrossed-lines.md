---
title: "Uncrossed Lines"
weight: 1014
#id: "uncrossed-lines"
---
## Description
<div class="description">
<p>We write the integers of <code>A</code> and <code>B</code>&nbsp;(in the order they are given) on two separate horizontal lines.</p>

<p>Now, we may draw <em>connecting lines</em>: a straight line connecting two numbers <code>A[i]</code> and <code>B[j]</code>&nbsp;such that:</p>

<ul>
	<li><code>A[i] == B[j]</code>;</li>
	<li>The line we draw does not intersect any other connecting (non-horizontal) line.</li>
</ul>

<p>Note that a connecting lines cannot intersect even at the endpoints:&nbsp;each number can only belong to one connecting line.</p>

<p>Return the maximum number of connecting lines we can draw in this way.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/04/26/142.png" style="width: 100px; height: 72px;" />
<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[1,4,2]</span>, B = <span id="example-input-1-2">[1,2,4]</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>We can draw 2 uncrossed lines as in the diagram.
We cannot draw 3 uncrossed lines, because the line from A[1]=4 to B[2]=4 will intersect the line from A[2]=2 to B[1]=2.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">[2,5,1,2,5]</span>, B = <span id="example-input-2-2">[10,5,2,1,5,2]</span>
<strong>Output: </strong><span id="example-output-2">3</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-3-1">[1,3,7,1,7,5]</span>, B = <span id="example-input-3-2">[1,9,2,5,1]</span>
<strong>Output: </strong><span id="example-output-3">2</span></pre>

<p>&nbsp;</p>
</div>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 500</code></li>
	<li><code>1 &lt;= B.length &lt;= 500</code></li>
	<li><code><font face="monospace">1 &lt;= A[i], B[i] &lt;= 2000</font></code></li>
</ol>

</div>

## Tags
- Array (array)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] DP, The Longest Common Subsequence
- Author: lee215
- Creation Date: Sun Apr 28 2019 12:02:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 21 2020 01:03:02 GMT+0800 (Singapore Standard Time)

<p>
## Solution 1: Straight Forward 2D DP
Time `O(MN)`, Space `O(MN)`

**Java:**
```java
    public int maxUncrossedLines(int[] A, int[] B) {
        int m = A.length, n = B.length, dp[][] = new int[m + 1][n + 1];
        for (int i = 1; i <= m; ++i)
            for (int j = 1; j <= n; ++j)
                if (A[i - 1] == B[j - 1])
                    dp[i][j] = 1 + dp[i - 1][j - 1];
                else
                    dp[i][j] = Math.max(dp[i][j - 1], dp[i - 1][j]);
        return dp[m][n];
    }
```

**C++:**
```cpp
    int maxUncrossedLines(vector<int>& A, vector<int>& B) {
        int m = A.size(), n = B.size(), dp[m+1][n+1];
        memset(dp, 0, sizeof(dp));
        for (int i = 1; i <= m; ++i)
            for (int j = 1; j <= n; ++j)
                dp[i][j] = A[i - 1] == B[j - 1] ? dp[i - 1][j - 1] + 1 : max(dp[i][j - 1], dp[i - 1][j]);
        return dp[m][n];
    }
```

**Python:**
```py
    def maxUncrossedLines(self, A, B):
        dp, m, n = collections.defaultdict(int), len(A), len(B)
        for i in xrange(m):
            for j in xrange(n):
                dp[i, j] = max(dp[i - 1, j - 1] + (A[i] == B[j]), dp[i - 1, j], dp[i, j - 1])
        return dp[m - 1, n - 1]
```
<br>

## Solution 2: 1D DP
Time `O(MN)`, Space `O(N)`
**Java**
@chipbk10
```java
     public int maxUncrossedLines(int[] A, int[] B) {
        int m = A.length, n = B.length;
        if (m < n) return maxUncrossedLines(B, A);

        int dp[] = new int[n+1];
        for (int i = 0; i < m; i++) {
            for (int j = 0, prev = 0, cur; j < n; j++) {
                cur = dp[j+1];
                if (A[i] == B[j]) dp[j+1] = 1+prev;
                else dp[j+1] = Math.max(dp[j+1], dp[j]);
                prev = cur;
            }
        }
        return dp[n];
    }
```
**Python:**
```py
    def maxUncrossedLines(self, A, B):
        m, n = len(A), len(B)
        dp = [0] * (n + 1)
        for i in xrange(m):
            for j in range(n)[::-1]:
                if A[i] == B[j]: dp[j + 1] = dp[j] + 1
            for j in range(n):
                dp[j + 1] = max(dp[j + 1], dp[j])
        return dp[n]
```
</p>


### Java DP Explained
- Author: naveen_kothamasu
- Creation Date: Wed Jun 12 2019 16:05:03 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 07 2019 14:26:59 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
if `A[i]` and `B[j]` are same, increment `count` and advance `i` and `j` and work on the remaning arrays.
Otherwise, we advance `i` and check the count on the remaining arrays, in another case we advance `j` and check, choose the max from the two sub-problems.

**Solution1 (TLE)**
```
public int maxUncrossedLines(int[] A, int[] B) {
        return helper(A, 0, B, 0);
    }
    private int helper(int[] A, int i, int[] B, int j){
        if(i == A.length || j == B.length) return 0;
        int count = 0;
        if(A[i] == B[j]) 
            count = 1+helper(A, i+1, B, j+1);
        else
        	count += Math.max(helper(A, i+1, B, j), helper(A, i, B, j+1));
        return count;
    }
```
**Solution2** same idea w/ memo
```
class Solution {
    Integer[][] dp = null;
    public int maxUncrossedLines(int[] A, int[] B) {
        dp = new Integer[A.length][B.length];
        return helper(A, 0, B, 0);
    }
    private int helper(int[] A, int i, int[] B, int j){
        if(i == A.length || j == B.length) return 0;
        if(dp[i][j] != null) return dp[i][j];
        if(A[i] == B[j]) 
            dp[i][j] = 1+helper(A, i+1, B, j+1);
        else
        	dp[i][j] = Math.max(helper(A, i+1, B, j), helper(A, i, B, j+1));
        return dp[i][j];
    }
}
```
**Solution 3 (DP bottom-up)** 
Another way of looking at the problem is keep comparing subarrays of A and B. The logic is same as edit distance @
https://leetcode.com/problems/edit-distance/discuss/170442/Java-DP-with-clear-explanation

Suppose we want to know the max lines in `A[0..i]` and `B[0..j]`, the best situation we expect is if `A[i] == B[j]`, then we will just draw a line connecting these two and add this line to max configuration from `A[0..i-1]` and `B[0..j-1]`. (we can\'t do any better)
If they differ, may be we can connect ending numbers of `A[0..i-1]` and `B[0..j]` **or** ending numbers of `A[0..i]` and `B[0..j-1]`, so we will try both and pick the max.

```
public int maxUncrossedLines(int[] a, int[] b) {
        int m = a.length, n = b.length;
        int[][] dp = new int[m+1][n+1];
        for(int i=1; i <= m; i++){
            for(int j=1; j <= n; j++){
                if(a[i-1] == b[j-1])
                    dp[i][j] = 1+dp[i-1][j-1];
                else
                    dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
            }
        }
        return dp[m][n];
    }
```
</p>


### Python by O( m*n ) DP [w/ Graph]
- Author: brianchiang_tw
- Creation Date: Mon May 25 2020 16:16:08 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 25 2020 22:53:44 GMT+0800 (Singapore Standard Time)

<p>
Python by O( m*n ) DP

This is a numerical version of [**Longest common subsequence** like Leetcode # 1143](https://leetcode.com/problems/longest-common-subsequence/)

---

**Illustration and Visualization**:

![image](https://assets.leetcode.com/users/brianchiang_tw/image_1590396300.png)

---

**Hint**:

First, add one **dummy -1** to A and B to **represent empty list**

Then, we define the notation **DP[ y ][ x ]**.

Let DP[y][x] denote the maximal number of uncrossed lines between **A[ 1 ... y ]** and **B[ 1 ... x ]**

We have optimal substructure as following:

---

**Base case**:
Any sequence with empty list yield no uncrossed lines.

If y = 0 or x = 0:
DP[ y ][ x ] = 0

---

**General case**:
If A[ y ] == B[ x ]:
DP[ y ][ x ] = DP[ y-1 ][ x-1 ] + 1

Current last number is matched, therefore, add one more uncrossed line

---

If A[ y ] =/= B[ x ]:
DP[ y ][ x ] = **Max**( DP[ y ][ x-1 ], DP[ y-1 ][ x ] )

Current last number is not matched, 
backtrack to **A**[ 1...**y** ]**B**[ 1...**x-1** ], **A**[ 1...**y-1** ]**B**[ 1...**x** ]
to find maximal number of uncrossed line

---

**Implementation** by 2D dynamic programming:

```
class Solution:
    def maxUncrossedLines(self, A: List[int], B: List[int]) -> int:
        
        # padding one dummy -1 to represent empty list
        A = [ -1 ] + A
        B = [ -1 ] + B
        
        h, w = len(A), len(B)
        dp_table = [ [ 0 for _ in range(w) ] for _ in range(h) ]
        
        
        
        for y in range(1, h):
            for x in range(1, w):
                
                if A[y] == B[x]:
                    # current number is matched, add one more uncrossed line
                    dp_table[y][x] = dp_table[y-1][x-1] + 1
                    
                else:
                    # cuurent number is not matched, backtracking to find maximal uncrossed line
                    dp_table[y][x] = max( dp_table[y][x-1], dp_table[y-1][x] )

                    
        return dp_table[-1][-1]
```

---


**Another implementation** by recursion with memoization

```
class Solution:
    def maxUncrossedLines(self, A: List[int], B: List[int]) -> int:
        
        A = [ -1 ] + A
        B = [ -1 ] + B
        
        dp_table = {}
        
        def helper( idx_a, idx_b ):
            
            if (idx_a, idx_b) in dp_table:
                return dp_table[(idx_a, idx_b)]
            
            
            if idx_a == 0 or idx_b == 0:
                # Any list compared to empty list give no uncrossed line
                return 0
            
            elif A[idx_a] == B[idx_b]:
                
                dp_table[(idx_a, idx_b)] = helper(idx_a-1, idx_b-1) + 1
                return dp_table[(idx_a, idx_b)]
            else:
                dp_table[(idx_a, idx_b)] = max( helper(idx_a-1, idx_b), helper(idx_a, idx_b-1))
                return dp_table[(idx_a, idx_b)]
        
        # --------------------------------------------
        return helper( len(A)-1, len(B)-1 )
```
</p>


