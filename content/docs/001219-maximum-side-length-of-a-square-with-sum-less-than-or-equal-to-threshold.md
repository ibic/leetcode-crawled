---
title: "Maximum Side Length of a Square with Sum Less than or Equal to Threshold"
weight: 1219
#id: "maximum-side-length-of-a-square-with-sum-less-than-or-equal-to-threshold"
---
## Description
<div class="description">
<p>Given a <code>m x n</code>&nbsp;matrix <code>mat</code> and an integer <code>threshold</code>. Return the maximum side-length of a square with a sum less than or equal to <code>threshold</code> or return <strong>0</strong> if there is no such square.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/12/05/e1.png" style="width: 335px; height: 186px;" />
<pre>
<strong>Input:</strong> mat = [[1,1,3,2,4,3,2],[1,1,3,2,4,3,2],[1,1,3,2,4,3,2]], threshold = 4
<strong>Output:</strong> 2
<strong>Explanation:</strong> The maximum side length of square with sum less than 4 is 2 as shown.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> mat = [[2,2,2,2,2],[2,2,2,2,2],[2,2,2,2,2],[2,2,2,2,2],[2,2,2,2,2]], threshold = 1
<strong>Output:</strong> 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> mat = [[1,1,1,1],[1,0,0,0],[1,0,0,0],[1,0,0,0]], threshold = 6
<strong>Output:</strong> 3
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> mat = [[18,70],[61,1],[25,85],[14,40],[11,96],[97,96],[63,45]], threshold = 40184
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= m, n &lt;= 300</code></li>
	<li><code>m == mat.length</code></li>
	<li><code>n == mat[i].length</code></li>
	<li><code>0 &lt;= mat[i][j] &lt;= 10000</code></li>
	<li><code>0 &lt;= threshold&nbsp;&lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Google - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java sum+binary O(m*n*log(min(m,n))) | sum+sliding window O(m*n)
- Author: BingleLove
- Creation Date: Sun Dec 15 2019 12:17:27 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 17 2019 15:31:02 GMT+0800 (Singapore Standard Time)

<p>
```Java
class Solution {
    
    int m, n;
    
    public int maxSideLength(int[][] mat, int threshold) {
        m = mat.length;
        n = mat[0].length;
        int[][] sum = new int[m + 1][n + 1];
        
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                sum[i][j] = sum[i-1][j] + sum[i][j-1] - sum[i-1][j-1] + mat[i-1][j-1];
            }
        }
        
        int lo = 0, hi = Math.min(m, n);
        
        while (lo <= hi) {
            int mid = (lo + hi) / 2;
            if (isSquareExist(sum, threshold, mid)) {
                lo = mid + 1;    
            } else {
                hi = mid - 1;
            }
        }
        
        return hi;
    }
    
    
    private boolean isSquareExist(int[][] sum, int threshold, int len) {
        for (int i = len; i <= m; i++) {
            for (int j = len; j <= n; j++) {
                if (sum[i][j] - sum[i-len][j] - sum[i][j-len] + sum[i-len][j-len] <= threshold) return true;
            }
        }
        return false;
    }
}
```

-------------------------------
Thanks for idea from [drunkpiano@](https://leetcode.com/problems/maximum-side-length-of-a-square-with-sum-less-than-or-equal-to-threshold/discuss/451871/Java-sum+binary-O(m*n*log(min(mn)))-or-sum+sliding-window-O(m*n)/406905). A tricky way to solve this problem in O(m*n)  is always checking if a square exists which is one size bigger than current maximum sqaure when building prefixsum.  

```Java
class Solution {
        
    public int maxSideLength(int[][] mat, int threshold) {
        int m = mat.length;
        int n = mat[0].length;
        int[][] sum = new int[m + 1][n + 1];
        
        int res = 0;
        int len = 1; // square side length

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                sum[i][j] = sum[i-1][j] + sum[i][j-1] - sum[i-1][j-1] + mat[i-1][j-1];
                
                if (i >= len && j >= len && sum[i][j] - sum[i-len][j] - sum[i][j-len] + sum[i-len][j-len] <= threshold)
                    res = len++;
            }
        }
        
        return res;
    }
    
}
```


</p>


### C++ Two O(MN) Solutions. PrefixSum + SlidingWindow | PrefixSum + Smart Enumeration
- Author: jiah
- Creation Date: Sun Dec 15 2019 12:32:44 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Dec 19 2019 06:57:09 GMT+0800 (Singapore Standard Time)

<p>
My solution has two steps:
1. Compute the `prefixSum` matrix via dynamic-programming method, where `prefixSum[i][j]` is the sum of submatrix `mat[0...i][0...j]`
2. A key observation:`mat` contains only non-negative elements as the problem statement says.
	- recall the 1d version problem: find longest subarray whose sum is less than `thres` in a non-negative size N array. We can do a `O(N)` sliding-window to solve it.
	- apply same idea to all the diagonals of `prefixSum` (m+n-1 diagonals in total). Elements on the same diagonal forms a non-negative array, and we can do the sliding window algorithm on it. (Actually, diagonal is not a must, you can also do sliding-window on rows or columns)

Time complexity:
- compute `prefixSum`: `O(MN)`
- sliding window on each diagonal: `O(sum of length of each diagonal) = O(MN)`
- Total complexity: `O(MN)`

Space complexity:
- prefixSum: `O(MN)`

```cpp
class Solution {
    int squareSum(vector<vector<int>>& prefixSum, int x1, int y1, int x2, int y2) {
        return prefixSum[x2][y2] - prefixSum[x1][y2] - prefixSum[x2][y1] + prefixSum[x1][y1];
    }
public:
    int maxSideLength(vector<vector<int>>& mat, int threshold) {
        // compute prefixSum via dynamic-programming method
        int m = mat.size(), n = mat[0].size();
        vector<vector<int>> prefixSum(m+1, vector<int>(n+1));
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                prefixSum[i][j] = prefixSum[i-1][j] + prefixSum[i][j-1] - prefixSum[i-1][j-1] + mat[i-1][j-1];
            }
        }
        int res = 0;
        
        for (int i = 0; i < m; i ++) {
            // do sliding-window on the diagonal line of (i, 0), (i+1, 1), ...
            int begin = 0, end = 0;
            int x1, x2, y1, y2;
            while (i+end <= m && end <= n) {
                x1 = i+begin, y1 = begin, x2 = i+end, y2 = end;
                while (begin < end && squareSum(prefixSum, x1, y1, x2, y2) > threshold) {
                    begin ++;
                    x1 = i+begin, y1 = begin;
                }
                res = max(res, end - begin);
                end ++;
            }
        }
        
        for (int i = 1; i < n; i ++) {
            // do sliding-window on the diagonal line of (0, i), (1, i+1), ...
            int begin = 0, end = 0;
            int x1, x2, y1, y2;
            while (end <= m && i+end <= n) {
                x1 = begin, y1 = i+begin, x2 = end, y2 = i+end;
                while (begin < end && squareSum(prefixSum, x1, y1, x2, y2) > threshold) {
                    begin ++;
                    x1 = begin, y1 = i+begin;
                }
                res = max(res, end - begin);
                end ++;
            }
        }
        return res;
    }
};
```

You might feel sliding-window an overkill, here is another `O(MN)` solution which uses an easy trick and is much shorter. Thanks to @please_AC
1. Compute `prefixSum`
2. For each point `(i, j)`, enumerate squares who has this point as their left-upper most element, and check the sum of these squares. use a variable `res` to track the longest edge found so far.
	- This trick makes the time complexity `O(MN)`: enumerate length from `res+1` found so far and break if checking fails. This trick helps avoid checking lengths that are not contributing to final answer. As the lengths we check are always increasing( from 1 to `min(m,n)`), we at most do the checking `O(m*n+min(m,n))` times. 


```cpp
class Solution {
    int squareSum(vector<vector<int>>& prefixSum, int x1, int y1, int x2, int y2) {
        return prefixSum[x2][y2] - prefixSum[x1][y2] - prefixSum[x2][y1] + prefixSum[x1][y1];
    }
public:
    int maxSideLength(vector<vector<int>>& mat, int threshold) {
        int m = mat.size(), n = mat[0].size();
        vector<vector<int>> prefixSum(m+1, vector<int>(n+1));
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                prefixSum[i][j] = prefixSum[i-1][j] + prefixSum[i][j-1] - prefixSum[i-1][j-1] + mat[i-1][j-1];
            }
        }
        int res = 0;
        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                int len = res+1;
                while (i+len <= m && j+len <= n && squareSum(prefixSum, i, j, i+len, j+len) <= threshold) {
                    res = len;
                    len++;
                }
            }
        }
        return res;
    }
};
```

</p>


### Java PrefixSum solution
- Author: motorix
- Creation Date: Sun Dec 15 2019 12:06:58 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 16 2019 03:11:57 GMT+0800 (Singapore Standard Time)

<p>
PrefixSum ```prefixsum[i][j]``` is the sum of every element from ```(0,0)``` to ```(i,j)```.
By using prefix sum, we can calculate the sum of every element from ```(i,j)``` to ```(i+k,j+k)``` easily.
```sum = prefixSum[i+k][j+k] - prefixSum[i-1][j+k] - prefixSum[i+k][j-1] + prefixSum[i-1][j-1]```

Java:
```
class Solution {
    public int maxSideLength(int[][] mat, int threshold) {
        int m = mat.length, n = mat[0].length;
        // Find prefix sum
        int[][] prefixSum = new int[m+1][n+1];
        for (int i = 1; i <= m; i++) {
            int sum = 0;
            for (int j = 1; j <= n; j++) {
                sum += mat[i-1][j-1];
                prefixSum[i][j] = prefixSum[i-1][j] + sum;
            }
        }
        // Start from the largest side length
        for(int k = Math.min(m, n)-1; k > 0; k--) {
            for(int i = 1; i+k <= m; i++) {
                for(int j = 1; j+k <= n; j++) {
                    if(prefixSum[i+k][j+k] - prefixSum[i-1][j+k] - prefixSum[i+k][j-1] + prefixSum[i-1][j-1] <= threshold) {
                        return k+1;
                    }
                }
            }
        }
        return 0;
    }
}
```
Time: ```O(m*n*Min(m,n))```
</p>


