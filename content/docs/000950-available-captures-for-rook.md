---
title: "Available Captures for Rook"
weight: 950
#id: "available-captures-for-rook"
---
## Description
<div class="description">
<p>On an 8 x 8 chessboard, there is one white rook.&nbsp; There also may be empty squares, white bishops, and black pawns.&nbsp; These are given as characters &#39;R&#39;, &#39;.&#39;, &#39;B&#39;, and &#39;p&#39; respectively. Uppercase characters represent white pieces, and lowercase characters represent black pieces.</p>

<p>The rook moves as in the rules of Chess: it chooses one of four cardinal directions (north, east, west, and south), then moves in that direction until it chooses to stop, reaches the edge of the board, or captures an opposite colored pawn by moving to the same square it occupies.&nbsp; Also, rooks cannot move into the same square as other friendly bishops.</p>

<p>Return the number of pawns the rook can capture in one move.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/02/20/1253_example_1_improved.PNG" style="width: 300px; height: 305px;" /></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;p&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;R&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;p&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;p&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;]]</span>
<strong>Output: </strong><span id="example-output-1">3</span>
<strong>Explanation: </strong>
In this example the rook is able to capture all the pawns.
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/02/19/1253_example_2_improved.PNG" style="width: 300px; height: 306px;" /></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;p&quot;,&quot;p&quot;,&quot;p&quot;,&quot;p&quot;,&quot;p&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;p&quot;,&quot;p&quot;,&quot;B&quot;,&quot;p&quot;,&quot;p&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;p&quot;,&quot;B&quot;,&quot;R&quot;,&quot;B&quot;,&quot;p&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;p&quot;,&quot;p&quot;,&quot;B&quot;,&quot;p&quot;,&quot;p&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;p&quot;,&quot;p&quot;,&quot;p&quot;,&quot;p&quot;,&quot;p&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;]]</span>
<strong>Output: </strong><span id="example-output-2">0</span>
<strong>Explanation: </strong>
Bishops are blocking the rook to capture any pawn.
</pre>

<p><strong>Example 3:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/02/20/1253_example_3_improved.PNG" style="width: 300px; height: 305px;" /></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;p&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;p&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;p&quot;,&quot;p&quot;,&quot;.&quot;,&quot;R&quot;,&quot;.&quot;,&quot;p&quot;,&quot;B&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;B&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;p&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;]]</span>
<strong>Output: </strong><span id="example-output-3">3</span>
<strong>Explanation: </strong>
The rook can capture the pawns at positions b5, d6 and f5.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>board.length == board[i].length == 8</code></li>
	<li><code>board[i][j]</code> is either <code>&#39;R&#39;</code>, <code>&#39;.&#39;</code>, <code>&#39;B&#39;</code>, or&nbsp;<code>&#39;p&#39;</code></li>
	<li>There is exactly one cell with <code>board[i][j] == &#39;R&#39;</code></li>
</ol>

</div>

## Tags
- Array (array)

## Companies
- Square - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java search and capture
- Author: votrubac
- Creation Date: Sun Feb 24 2019 12:04:22 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 02 2019 00:39:28 GMT+0800 (Singapore Standard Time)

<p>
Search for the rock. From the rock, trace empty spaces in four directions. Return 1 if we hit a pawn, zero otherwise.

Count and return captured pawns.
```
int cap(vector<vector<char>>& b, int x, int y, int dx, int dy) {
  while (x >= 0 && x < b.size() && y >= 0 && y < b[x].size() && b[x][y] != \'B\') {
    if (b[x][y] == \'p\') return 1;
    x += dx, y += dy;
  }
  return 0;
}
int numRookCaptures(vector<vector<char>>& b) {
  for (auto i = 0; i < b.size(); ++i)
    for (auto j = 0; j < b[i].size(); ++j)
      if (b[i][j] == \'R\') return cap(b,i,j,0,1)+cap(b,i,j,0,-1)+cap(b,i,j,1,0)+cap(b,i,j,-1,0);
  return 0;
}
```
Java version:
```
int cap(char[][] b, int x, int y, int dx, int dy) {
  while (x >= 0 && x < b.length && y >= 0 && y < b[x].length && b[x][y] != \'B\') {
    if (b[x][y] == \'p\') return 1;
    x += dx; y += dy;
  }
  return 0;
}
public int numRookCaptures(char[][] b) {
  for (int i = 0; i < b.length; ++i)
    for (int j = 0; j < b[i].length; ++j)
      if (b[i][j] == \'R\') return cap(b,i,j,0,1)+cap(b,i,j,0,-1)+cap(b,i,j,1,0)+cap(b,i,j,-1,0);
  return 0;
}
```
# Complexity Analysis
Runtime: O(n), where n is the total  number of cells. In the worst case, we need to check all cells to find the rock, and then check one row and one column.
Memory: O(1).
</p>


### [Java/C++/Python] Straight Forward Solution
- Author: lee215
- Creation Date: Sun Feb 24 2019 12:05:35 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 13 2020 01:03:58 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
1. Find index of the white rook
2. Search in 4 direction until 8 steps or meet any piece.
<br>

## **Complexity**
Time `O(64)`
Space `O(1)`
<br>

**Java**
```java
    public int numRookCaptures(char[][] board) {
        int x0 = 0, y0 = 0, res = 0;
        for (int i = 0; i < 8; ++i)
            for (int j = 0; j < 8; ++j)
                if (board[i][j] == \'R\') {
                    x0 = i;
                    y0 = j;
                }

        for (int[] d : new int[][] {{1, 0}, {0, 1}, {-1, 0}, {0, -1}}) {
            for (int x = x0 + d[0], y = y0 + d[1]; 0 <= x && x < 8 && 0 <= y && y < 8; x += d[0], y += d[1]) {
                if (board[x][y] == \'p\') res++;
                if (board[x][y] != \'.\') break;
            }
        }
        return res;
    }
```

**C++**
```cpp
    int numRookCaptures(vector<vector<char>>& board) {
        int x0 = 0, y0 = 0, res = 0;
        for (int i = 0; i < 8; ++i)
            for (int j = 0; j < 8; ++j)
                if (board[i][j] == \'R\') {
                    x0 = i;
                    y0 = j;
                }
        vector<vector<int>> direction = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
        for (auto d : direction) {
            for (int x = x0 + d[0], y = y0 + d[1]; 0 <= x && x < 8 && 0 <= y && y < 8; x += d[0], y += d[1]) {
                if (board[x][y] == \'p\') res++;
                if (board[x][y] != \'.\') break;
            }
        }
        return res;
    }
```
**Python:**
```py
    def numRookCaptures(self, board):
        for i in range(8):
            for j in range(8):
                if board[i][j] == \'R\':
                    x0, y0 = i, j
        res = 0
        for i, j in [[1, 0], [0, 1], [-1, 0], [0, -1]]:
            x, y = x0 + i, y0 + j
            while 0 <= x < 8 and 0 <= y < 8:
                if board[x][y] == \'p\': res += 1
                if board[x][y] != \'.\': break
                x, y = x + i, y + j
        return res
```

**Python obligatory 1-line**
```py
    def numRookCaptures(self, A):
        return sum(\'\'.join(r).replace(\'.\', \'\').count(\'Rp\') for r in A + zip(*A) for r in [r, r[::-1]])
```
</p>


### Confusing explanation
- Author: abkosar
- Creation Date: Tue May 21 2019 22:03:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 21 2019 22:03:56 GMT+0800 (Singapore Standard Time)

<p>
The last part of the question `Return the number of pawns the rook can capture in one move.` is confusing because in one move you can only capture one pawn. If you want to capture all the three pawns you have to do three moves. So the question should be `Return the number of pawns the rook can capture`. I don\'t know who assesses these questions but some of these could be written in a much much better way.
</p>


