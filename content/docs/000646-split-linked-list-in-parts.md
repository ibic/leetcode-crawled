---
title: "Split Linked List in Parts"
weight: 646
#id: "split-linked-list-in-parts"
---
## Description
<div class="description">
<p>Given a (singly) linked list with head node <code>root</code>, write a function to split the linked list into <code>k</code> consecutive linked list "parts".
</p><p>
The length of each part should be as equal as possible: no two parts should have a size differing by more than 1.  This may lead to some parts being null.
</p><p>
The parts should be in order of occurrence in the input list, and parts occurring earlier should always have a size greater than or equal parts occurring later.
</p><p>
Return a List of ListNode's representing the linked list parts that are formed.
</p>

Examples
1->2->3->4, k = 5 // 5 equal parts
[ [1], 
[2],
[3],
[4],
null ]

<p><b>Example 1:</b><br />
<pre style="white-space: pre-line">
<b>Input:</b> 
root = [1, 2, 3], k = 5
<b>Output:</b> [[1],[2],[3],[],[]]
<b>Explanation:</b>
The input and each element of the output are ListNodes, not arrays.
For example, the input root has root.val = 1, root.next.val = 2, \root.next.next.val = 3, and root.next.next.next = null.
The first element output[0] has output[0].val = 1, output[0].next = null.
The last element output[4] is null, but it's string representation as a ListNode is [].
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> 
root = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], k = 3
<b>Output:</b> [[1, 2, 3, 4], [5, 6, 7], [8, 9, 10]]
<b>Explanation:</b>
The input has been split into consecutive parts with size difference at most 1, and earlier parts are a larger size than the later parts.
</pre>
</p>

<p><b>Note:</b>
<li>The length of <code>root</code> will be in the range <code>[0, 1000]</code>.</li>
<li>Each value of a node in the input will be an integer in the range <code>[0, 999]</code>.</li>
<li><code>k</code> will be an integer in the range <code>[1, 50]</code>.</li>
</p>
</div>

## Tags
- Linked List (linked-list)

## Companies
- Adobe - 2 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Create New Lists [Accepted]

**Intuition and Algorithm**

If there are $$N$$ nodes in the linked list `root`, then there are $$N / k$$ items in each part, plus the first $$N \% k$$ parts have an extra item.  We can count $$N$$ with a simple loop.

Now for each part, we have calculated how many nodes that part will have: `width + (i < remainder ? 1 : 0)`.  We create a new list and write the part to that list.

Our solution showcases constructs of the form `a = b = c`.  Note that this syntax behaves differently for different languages.

<iframe src="https://leetcode.com/playground/eEgVuXVr/shared" frameBorder="0" width="100%" height="463" name="eEgVuXVr"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N + k)$$, where $$N$$ is the number of nodes in the given list.  If $$k$$ is large, it could still require creating many new empty lists.

* Space Complexity: $$O(max(N, k))$$, the space used in writing the answer.

---
#### Approach #2: Split Input List [Accepted]

**Intuition and Algorithm**

As in *Approach #1*, we know the size of each part.  Instead of creating new lists, we will split the input list directly and return a list of pointers to nodes in the original list as appropriate.

Our solution proceeds similarly.  For a part of size `L = width + (i < remainder ? 1 : 0)`, instead of stepping `L` times, we will step `L-1` times, and our final time will also sever the link between the last node from the previous part and the first node from the next part.

<iframe src="https://leetcode.com/playground/zL6atUfN/shared" frameBorder="0" width="100%" height="500" name="zL6atUfN"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N + k)$$, where $$N$$ is the number of nodes in the given list.  If $$k$$ is large, it could still require creating many new empty lists.

* Space Complexity: $$O(k)$$, the additional space used in writing the answer.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] Clean Code
- Author: alexander
- Creation Date: Sun Nov 12 2017 12:26:14 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 00:10:51 GMT+0800 (Singapore Standard Time)

<p>
**Java**
```
class Solution {
    public ListNode[] splitListToParts(ListNode root, int k) {
        ListNode[] parts = new ListNode[k];
        int len = 0;
        for (ListNode node = root; node != null; node = node.next)
            len++;
        int n = len / k, r = len % k; // n : minimum guaranteed part size; r : extra nodes spread to the first r parts;
        ListNode node = root, prev = null;
        for (int i = 0; node != null && i < k; i++, r--) {
            parts[i] = node;
            for (int j = 0; j < n + (r > 0 ? 1 : 0); j++) {
                prev = node;
                node = node.next;
            }
            prev.next = null;
        }
        return parts;        
    }
}
```
**C++**
```
class Solution {
public:
    vector<ListNode*> splitListToParts(ListNode* root, int k) {
        vector<ListNode*> parts(k, nullptr);
        int len = 0;
        for (ListNode* node = root; node; node = node->next)
            len++;
        int n = len / k, r = len % k; // n : minimum guaranteed part size; r : extra nodes spread to the first r parts;
        ListNode* node = root, *prev = nullptr;
        for (int i = 0; node && i < k; i++, r--) {
            parts[i] = node;
            for (int j = 0; j < n + (r > 0); j++) {
                prev = node;
                node = node->next;
            }
            prev->next = nullptr;
        }
        return parts;
    }
};
```
</p>


### Elegant Python with Explanation - 45ms
- Author: yangshun
- Creation Date: Mon Nov 13 2017 00:52:21 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 12 2018 10:34:58 GMT+0800 (Singapore Standard Time)

<p>
This question can be split into two parts:

1. Count the length of the linked list
2. Determine the length of nodes in each chunk
3. Splitting the linked list up

At the end of step 2, `res` will look something like this, for a list of length 10 and k of 3: `[4, 4, 3]`.

Step 3 iterates through `res` using the values in `res` and replaces the value at each index with each chunk's head node. We have to keep a reference to `prev` in order to slice up the chunks nicely by setting `prev.next = None`.


*- Yangshun*

```
class Solution(object):
    def splitListToParts(self, root, k):
        # Count the length of the linked list
        curr, length = root, 0
        while curr:
            curr, length = curr.next, length + 1
        # Determine the length of each chunk
        chunk_size, longer_chunks = length // k, length % k
        res = [chunk_size + 1] * longer_chunks + [chunk_size] * (k - longer_chunks)
        # Split up the list
        prev, curr = None, root
        for index, num in enumerate(res):
            if prev:
                prev.next = None
            res[index] = curr
            for i in range(num):
                prev, curr = curr, curr.next
        return res
```
</p>


### Intuitive Java Solution With Explanation
- Author: naveen_kothamasu
- Creation Date: Mon Apr 29 2019 03:44:00 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 29 2019 03:44:00 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
For the given length `n`, we first divide it in to `k` equal parts of size `m`, any remaninging nodes `r` must be distributed one by one starting from the first list.
There is a special case If `m==0`, we already distributed one node per list ( with `res[j++] = curr;`) before we come to `r` part, so deduct `k` from reminder `r`.

```
public ListNode[] splitListToParts(ListNode root, int k) {
        int n = 0;
        ListNode curr = root;
        while(curr != null){
            ++n;
            curr = curr.next;
        }
        int m = n/k, r = (n % k), j = 0;
        if(m == 0) r -= k;
        ListNode[] res = new ListNode[k];
        curr = root;
        while(curr != null){
            res[j++] = curr;
            for(int i=1; i < m; i++){
                curr = curr.next;
            }
            if(r > 0){
                curr = curr.next;
                --r;
            }
            ListNode tmp = curr.next;
            curr.next = null;
            curr = tmp;
        }
        return res;
    }
```
</p>


