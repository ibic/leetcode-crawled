---
title: "Arranging Coins"
weight: 418
#id: "arranging-coins"
---
## Description
<div class="description">
<p>You have a total of <i>n</i> coins that you want to form in a staircase shape, where every <i>k</i>-th row must have exactly <i>k</i> coins.</p>
 
<p>Given <i>n</i>, find the total number of <b>full</b> staircase rows that can be formed.</p>

<p><i>n</i> is a non-negative integer and fits within the range of a 32-bit signed integer.</p>

<p><b>Example 1:</b>
<pre>
n = 5

The coins can form the following rows:
¤
¤ ¤
¤ ¤

Because the 3rd row is incomplete, we return 2.
</pre>
</p>

<p><b>Example 2:</b>
<pre>
n = 8

The coins can form the following rows:
¤
¤ ¤
¤ ¤ ¤
¤ ¤

Because the 4th row is incomplete, we return 3.
</pre>
</p>
</div>

## Tags
- Math (math)
- Binary Search (binary-search)

## Companies
- Bloomberg - 2 (taggedByAdmin: false)
- GoDaddy - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Binary Search

This question is easy in a sense that one could run an **exhaustive iteration**
 to obtain the result.
That could work, except that it would run out of time when the input becomes too large.
So let us take a step back to look at the problem, before rushing to the implementation. 

Assume that the answer is $$k$$, _i.e._ we've managed to complete $$k$$ rows of coins.
These completed rows contain in total $$1 + 2 + ... + k = \frac{k (k + 1)}{2}$$ coins.

We could now reformulate the problem as follows:

> Find the maximum $$k$$ such that $$\frac{k (k + 1)}{2} \le N$$.

The problem seems to be one of those **search** problems.
And instead of naive iteration, one could resort to another 
more efficient algorithm called 
[**_binary search_**](https://en.wikipedia.org/wiki/Binary_search_algorithm),
as we can find in another similar problem called 
[search insert position](https://leetcode.com/articles/search-insert-position/). 

**Implementation**

<iframe src="https://leetcode.com/playground/MES8NgB6/shared" frameBorder="0" width="100%" height="378" name="MES8NgB6"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(\log N)$$.
 
* Space complexity : $$\mathcal{O}(1)$$.
<br /> 
<br />


---
#### Approach 2: Math

If we look deeper into the formula of the problem,
we could actually solve it with the help of mathematics,
without using any iteration.

As a reminder, the constraint of the problem can be expressed as follows:

$$
k(k + 1) \le 2N
$$

This could be solved by [completing the square](https://en.wikipedia.org/wiki/Completing_the_square)
technique,

$$
\left(k + \frac{1}{2}\right)^2 - \frac{1}{4} \le 2N
$$

that results in the following answer:

$$
k = \left[\sqrt{2N + \frac{1}{4}} - \frac{1}{2}\right]
$$

**Implementation**

<iframe src="https://leetcode.com/playground/awABUisT/shared" frameBorder="0" width="100%" height="140" name="awABUisT"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$.
 
* Space complexity : $$\mathcal{O}(1)$$. 
<br /> 
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ 1 line code
- Author: zmcx16
- Creation Date: Mon Oct 31 2016 18:37:46 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 08:04:14 GMT+0800 (Singapore Standard Time)

<p>
Concept:

1+2+3+...+x = n
-> (1+x)x/2 = n
-> x^2+x = 2n
-> x^2+x+1/4 = 2n +1/4
-> (x+1/2)^2 = 2n +1/4
-> (x+0.5) = sqrt(2n+0.25)
-> x = -0.5 + sqrt(2n+0.25)

int arrangeCoins(int n) {
return floor(-0.5+sqrt((double)2*n+0.25));
}
</p>


### [JAVA] Clean Code with Explanations and Running Time [2 Solutions]
- Author: ratchapongt
- Creation Date: Mon Oct 31 2016 11:39:53 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 27 2018 07:28:03 GMT+0800 (Singapore Standard Time)

<p>
<h3>[JAVA] Clean Code with Explanations and Running Time [2 Solutions]</h3>
<a href=https://ratchapong.com/algorithm-practice/leetcode/arranging-coins>Full Solutions and Explanations</a>
<h3><b>Solution 1</b></h3>
```
public class Solution {
    public int arrangeCoins(int n) {
        int start = 0;
        int end = n;
        int mid = 0;
        while (start <= end){
            mid = (start + end) >>> 1;
            if ((0.5 * mid * mid + 0.5 * mid ) <= n){
                start = mid + 1;
            }else{
                end = mid - 1;
            }
        }
        return start - 1;
    }
}
```

<div class="margin-bottom-20"><h4 class="text-uppercase margin-bottom-10">Complexity Analysis</h4><p>Uniform cost model is used as Cost Model and `n` is the input number. `b` in this case would be `2`.</p><p><b>Time Complexity:</b><ul><li>Best Case `O(log_b(n))` : With respect to the input, the algorithm will always depend on the value of input.</li><li>Average Case `O(log_b(n))` : With respect to the input, the algorithm will always depend on the value of input.</li><li>Worst Case `O(log_b(n))` : With respect to the input, the algorithm will always depend on the value of input.</li></ul></p><p><b>Auxiliary Space:</b><ul><li>Worst Case `O(1)` : Additional variables are of constant size.</li></ul></p></div><div class="margin-bottom-20"><h4 class="text-uppercase margin-bottom-10">Algorithm</h4><p><b>Approach:</b> Binary Search</p><p>The problem is basically asking the maximum length of consecutive number that has the running sum lesser or equal to `n`. In other word, find `x` that satisfy the following condition:</p><div class="margin-left-50 margin-bottom-10">`1 + 2 + 3 + 4 + 5 + 6 + 7 + ... + x &lt= n`</div><div class="margin-left-50 margin-bottom-10">`sum_{i=1}^x i &lt= n`</div><p>Running sum can be simplified,</p><div class="margin-left-50 margin-bottom-10">`(x * ( x + 1)) / 2 &lt= n`</div><p>Binary search is used in this case to slowly narrow down the `x` that will satisfy the equation. Note that <code>0.5 * mid * mid + 0.5 * mid</code> does not have overflow issue as the intermediate result is implicitly autoboxed to <code>double</code> data type.</p></div>
<hr>

<h3><b>Solution 2</b></h3>
```
public class Solution {
    public int arrangeCoins(int n) {
        return (int) ((Math.sqrt(1 + 8.0 * n) - 1) / 2);
    }
}
```

<div class="margin-bottom-20"><h4 class="text-uppercase margin-bottom-10">Complexity Analysis</h4><p>Uniform cost model is used as Cost Model and `n` is the input number. `b` in this case would be `2`.</p><p><b>Time Complexity:</b><ul><li>Best Case `O(1)` : With respect to the input, the algorithm will always perform basic mathematical operation that run in constant time.</li><li>Average Case `O(1)` : With respect to the input, the algorithm will always perform basic mathematical operation that run in constant time.</li><li>Worst Case `O(1)` : With respect to the input, the algorithm will always perform basic mathematical operation that run in constant time.</li></ul></p><p><b>Auxiliary Space:</b><ul><li>Worst Case `O(1)` : No extra space is used.</li></ul></p></div><div class="margin-bottom-20"><h4 class="text-uppercase margin-bottom-10">Algorithm</h4><p><b>Approach:</b> Mathematics</p><p>The problem is basically asking the maximum length of consecutive number that has the running sum lesser or equal to `n`. In other word, find `x` that satisfy the following condition:</p><div class="margin-left-50 margin-bottom-10">`1 + 2 + 3 + 4 + 5 + 6 + 7 + ... + x &lt= n`</div><div class="margin-left-50 margin-bottom-10">`sum_{i=1}^x i &lt= n`</div><p>Running sum can be simplified,</p><div class="margin-left-50 margin-bottom-10">`(x * ( x + 1)) / 2 &lt= n`</div><p>Using quadratic formula, `x` is evaluated to be,</p><div class="margin-left-50 margin-bottom-10">`x = 1 / 2 * (-sqrt(8 * n + 1)-1)` (Inapplicable) or `x = 1 / 2 * (sqrt(8 * n + 1)-1)`</div><p>Negative root is ignored and positive root is used instead. Note that <code>8.0 * n</code> is very important because it will cause Java to implicitly autoboxed the intermediate result into <code>double</code> data type. The code will not work if it is simply <code>8 * n</code>. Alternatively, an explicit casting can be done <code>8 * (long) n)</code>.</p></div>
<hr>
</p>


### Java O(1) Solution - Math Problem
- Author: mysun
- Creation Date: Mon Oct 31 2016 10:05:28 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 31 2016 10:05:28 GMT+0800 (Singapore Standard Time)

<p>
The idea is about quadratic equation, the formula to get the sum of arithmetic progression is
sum = (x + 1) * x / 2
so for this problem, if we know the the sum, then we can know the x = (-1 + sqrt(8 * n + 1)) / 2

```
public class Solution {
    public int arrangeCoins(int n) {
        return (int)((-1 + Math.sqrt(1 + 8 * (long)n)) / 2);
    }
}
```
</p>


