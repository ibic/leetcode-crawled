---
title: "Customers Who Never Order"
weight: 1477
#id: "customers-who-never-order"
---
## Description
<div class="description">
<p>Suppose that a website contains two tables, the <code>Customers</code> table and the <code>Orders</code> table. Write a SQL query to find all customers who never order anything.</p>

<p>Table: <code>Customers</code>.</p>

<pre>
+----+-------+
| Id | Name  |
+----+-------+
| 1  | Joe   |
| 2  | Henry |
| 3  | Sam   |
| 4  | Max   |
+----+-------+
</pre>

<p>Table: <code>Orders</code>.</p>

<pre>
+----+------------+
| Id | CustomerId |
+----+------------+
| 1  | 3          |
| 2  | 1          |
+----+------------+
</pre>

<p>Using the above tables as example, return the following:</p>

<pre>
+-----------+
| Customers |
+-----------+
| Henry     |
| Max       |
+-----------+
</pre>

</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using sub-query and `NOT IN` clause [Accepted]

**Algorithm**

If we have a list of customers who have ever ordered, it will be easy to know who never ordered.

We can use the following code to get such list.

```sql
select customerid from orders;
```

Then, we can use `NOT IN` to query the customers who are not in this list.

**MySQL**

```sql
select customers.name as 'Customers'
from customers
where customers.id not in
(
    select customerid from orders
);
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Three accepted solutions
- Author: kent-huang
- Creation Date: Wed Jan 21 2015 13:27:52 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 00:07:26 GMT+0800 (Singapore Standard Time)

<p>
    SELECT A.Name from Customers A
    WHERE NOT EXISTS (SELECT 1 FROM Orders B WHERE A.Id = B.CustomerId)

    SELECT A.Name from Customers A
    LEFT JOIN Orders B on  a.Id = B.CustomerId
    WHERE b.CustomerId is NULL

    SELECT A.Name from Customers A
    WHERE A.Id NOT IN (SELECT B.CustomerId from Orders B)
</p>


### A solution using NOT IN and another one using LEFT JOIN
- Author: ohini
- Creation Date: Fri Aug 21 2015 11:51:15 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 21 2015 11:51:15 GMT+0800 (Singapore Standard Time)

<p>
605 ms

    SELECT Name as Customers from Customers
    LEFT JOIN Orders
    ON Customers.Id = Orders.CustomerId
    WHERE Orders.CustomerId IS NULL;

675ms

    SELECT Name as Customers from Customers
    WHERE Id NOT IN (SELECT CustomerId from Orders);
</p>


### Here are 3 solutions
- Author: HarshLoomba
- Creation Date: Fri Mar 18 2016 00:31:13 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 14:04:16 GMT+0800 (Singapore Standard Time)

<p>
select c.Name from Customers c
where c.Id not in (select customerId from Orders)

select c.Name from Customers c
where (select count(*) from Orders o where o.customerId=c.id)=0 

select c.Name from Customers c
where not exists (select * from Orders o where o.customerId=c.id)
</p>


