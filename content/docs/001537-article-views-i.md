---
title: "Article Views I"
weight: 1537
#id: "article-views-i"
---
## Description
<div class="description">
<p>Table: <code>Views</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| article_id    | int     |
| author_id     | int     |
| viewer_id     | int     |
| view_date     | date    |
+---------------+---------+
There is no primary key for this table, it may have duplicate rows.
Each row of this table indicates that some viewer viewed an article (written by some author) on some date. 
Note that equal author_id and viewer_id indicate the same person.</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find all the authors that viewed at least one of their own articles, sorted in ascending order by their id.</p>

<p>The query result format is in the following example:</p>

<pre>
Views table:
+------------+-----------+-----------+------------+
| article_id | author_id | viewer_id | view_date  |
+------------+-----------+-----------+------------+
| 1          | 3         | 5         | 2019-08-01 |
| 1          | 3         | 6         | 2019-08-02 |
| 2          | 7         | 7         | 2019-08-01 |
| 2          | 7         | 6         | 2019-08-02 |
| 4          | 7         | 1         | 2019-07-22 |
| 3          | 4         | 4         | 2019-07-21 |
| 3          | 4         | 4         | 2019-07-21 |
+------------+-----------+-----------+------------+

Result table:
+------+
| id   |
+------+
| 4    |
| 7    |
+------+
</pre>

</div>

## Tags


## Companies
- LinkedIn - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Three approaches (MYSQL)
- Author: treemantan
- Creation Date: Wed Nov 06 2019 22:19:02 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 06 2019 22:20:18 GMT+0800 (Singapore Standard Time)

<p>
1. If with distinct keyword,
```sql
SELECT DISTINCT author_id AS id FROM Views 
where author_id = viewer_id 
ORDER BY id
```
2. If order by first, we need another Select and alias
```sql
SELECT id from (SELECT author_id AS id FROM Views 
where author_id = viewer_id 
ORDER BY id)a
GROUP BY id
```
3. If just with group by (automatically sorted by id)
```sql
SELECT author_id AS id FROM Views 
where author_id = viewer_id 
GROUP BY id
```
</p>


### simple query using 1 where clause
- Author: Sam_kk
- Creation Date: Mon Jul 13 2020 01:55:49 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 13 2020 01:55:49 GMT+0800 (Singapore Standard Time)

<p>
```

select distinct author_id as id from views 
where author_id = viewer_id
order by id
</p>


### Two Mysql Solutions With and Without Distinct
- Author: pavankumarboinapalli
- Creation Date: Mon Jun 15 2020 11:40:42 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 15 2020 11:40:42 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1:**

```
SELECT author_id as id 
FROM Views 
GROUP BY author_id 
HAVING SUM(CASE WHEN author_id=viewer_id THEN 1 ELSE 0 END)>0 
ORDER BY id
```

**Solution 2:**

```
SELECT DISTINCT author_id as id 
FROM views 
WHERE author_id = viewer_id 
ORDER BY id 
```
</p>


