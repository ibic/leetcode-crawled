---
title: "Online Election"
weight: 861
#id: "online-election"
---
## Description
<div class="description">
<p>In an election, the <code>i</code>-th&nbsp;vote was cast for <code>persons[i]</code> at time <code>times[i]</code>.</p>

<p>Now, we would like to implement the following query function: <code>TopVotedCandidate.q(int t)</code> will return the number of the person that was leading the election at time <code>t</code>.&nbsp;&nbsp;</p>

<p>Votes cast at time <code>t</code> will count towards our query.&nbsp; In the case of a tie, the most recent vote (among tied candidates) wins.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;TopVotedCandidate&quot;,&quot;q&quot;,&quot;q&quot;,&quot;q&quot;,&quot;q&quot;,&quot;q&quot;,&quot;q&quot;]</span>, <span id="example-input-1-2">[[[0,1,1,0,0,1,0],[0,5,10,15,20,25,30]],[3],[12],[25],[15],[24],[8]]</span>
<strong>Output: </strong><span id="example-output-1">[null,0,1,1,0,0,1]</span>
<strong>Explanation: </strong>
At time 3, the votes are [0], and 0 is leading.
At time 12, the votes are [0,1,1], and 1 is leading.
At time 25, the votes are [0,1,1,0,0,1], and 1 is leading (as ties go to the most recent vote.)
This continues for 3 more queries at time 15, 24, and 8.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= persons.length = times.length &lt;= 5000</code></li>
	<li><code>0 &lt;= persons[i] &lt;= persons.length</code></li>
	<li><code>times</code>&nbsp;is a strictly increasing array with all elements in <code>[0, 10^9]</code>.</li>
	<li><code>TopVotedCandidate.q</code> is called at most <code>10000</code> times per test case.</li>
	<li><code>TopVotedCandidate.q(int t)</code> is always called with <code>t &gt;= times[0]</code>.</li>
</ol>
</div>

</div>

## Tags
- Binary Search (binary-search)

## Companies
- Google - 3 (taggedByAdmin: true)
- Apple - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: List of Lists + Binary Search

**Intuition and Algorithm**

We can store the votes in a list `A` of lists of votes.  Each vote has a person and a timestamp, and `A[count]` is a list of the `count`-th votes received for that person.

Then, `A[i][0]` and `A[i]` are monotone increasing, so we can binary search on them to find the most recent vote by time.

<iframe src="https://leetcode.com/playground/FWFgarS7/shared" frameBorder="0" width="100%" height="500" name="FWFgarS7"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N + Q \log^2 N)$$, where $$N$$ is the number of votes, and $$Q$$ is the number of queries.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Precomputed Answer + Binary Search

**Intuition and Algorithm**

As the votes come in, we can remember every event `(winner, time)` when the winner changes.  After, we have a sorted list of these events that we can binary search for the answer.

<iframe src="https://leetcode.com/playground/knQ7kc5B/shared" frameBorder="0" width="100%" height="500" name="knQ7kc5B"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N + Q \log N)$$, where $$N$$ is the number of votes, and $$Q$$ is the number of queries.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Anyone else just find this question really confusing?
- Author: benjie
- Creation Date: Sun Oct 14 2018 05:10:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 05:54:15 GMT+0800 (Singapore Standard Time)

<p>
I don\'t understand the example that is given ... why does the input include a bunch of strings? Why are some steps lists of lists of lists and others a list with a single number?

I feel like the description needs to be greatly improved, and is a bit ambiguous at the moment.
</p>


### [C++/Java/Python] Binary Search in Times
- Author: lee215
- Creation Date: Sun Sep 23 2018 11:06:50 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Mar 19 2020 09:45:47 GMT+0800 (Singapore Standard Time)

<p>
## **Initialization part**
In the order of time, we count the number of votes for each person.
Also, we update the current lead of votes for each time point.
` if (count[person] >= count[lead]) lead = person`

Time Complexity: `O(N)`
<br>

## **Query part**
Binary search `t` in `times`,
find out the latest time point no later than `t`.
Return the lead of votes at that time point.

Time Complexity: `O(logN)`

<br>

**C++**
```cpp
    unordered_map<int, int> m;
    vector<int> times;
    TopVotedCandidate(vector<int> persons, vector<int> times) {
        int n = persons.size(), lead = -1;
        this->times = times;
        unordered_map<int, int> count;
        for (int i = 0; i < n; ++i) {
            lead = ++count[persons[i]] >= count[lead] ? persons[i] : lead;
            m[times[i]] = lead;
        }
    }

    int q(int t) {
        return m[*--upper_bound(times.begin(), times.end(), t)];
    }
```

**C++, using map**
```cpp
    map<int, int> m;
    TopVotedCandidate(vector<int> persons, vector<int> times) {
        int n = persons.size(), lead = -1;
        unordered_map<int, int> count;
        for (int i = 0; i < n; ++i) {
            lead = ++count[persons[i]] >= count[lead] ? persons[i] : lead;
            m[times[i]] = lead;
        }
    }

    int q(int t) {
        return (--m.upper_bound(t))-> second;
    }
```

**Java:**
```java
    Map<Integer, Integer> m = new HashMap<>();
    int[] time;
    public TopVotedCandidate(int[] persons, int[] times) {
        int n = persons.length, lead = -1;
        Map<Integer, Integer> count = new HashMap<>();
        time = times;
        for (int i = 0; i < n; ++i) {
            count.put(persons[i], count.getOrDefault(persons[i], 0) + 1);
            if (i == 0 || count.get(persons[i]) >= count.get(lead)) lead = persons[i];
            m.put(times[i], lead);
        }
    }

    public int q(int t) {
        int i = Arrays.binarySearch(time, t);
        return i < 0 ? m.get(time[-i-2]) : m.get(time[i]);
    }
```

**Python:**
```python
    def __init__(self, persons, times):
        self.leads, self.times, count = [], times, {}
        lead = -1
        for p in persons:
            count[p] = count.get(p, 0) + 1
            if count[p] >= count.get(lead, 0): lead = p
            self.leads.append(lead)

    def q(self, t):
        return self.leads[bisect.bisect(self.times, t) - 1]
```

</p>


### Confusion in understanding the question
- Author: mohitfte
- Creation Date: Sun Jan 13 2019 12:43:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 13 2019 12:43:16 GMT+0800 (Singapore Standard Time)

<p>
This Question is not clear in its way!! Does anyone else feels the same?
</p>


