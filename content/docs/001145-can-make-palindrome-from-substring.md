---
title: "Can Make Palindrome from Substring"
weight: 1145
#id: "can-make-palindrome-from-substring"
---
## Description
<div class="description">
<p>Given a string <code>s</code>, we make queries on substrings of <code>s</code>.</p>

<p>For each query <code>queries[i] = [left, right, k]</code>, we may <strong>rearrange</strong>&nbsp;the substring <code>s[left], ..., s[right]</code>, and then choose <strong>up to</strong> <code>k</code> of them to replace with any lowercase English letter.&nbsp;</p>

<p>If the substring&nbsp;is possible to be a&nbsp;palindrome string after the operations above, the result of the query is <code>true</code>.&nbsp;Otherwise, the result&nbsp;is <code>false</code>.</p>

<p>Return an array <code>answer[]</code>, where <code>answer[i]</code> is the result of the <code>i</code>-th query <code>queries[i]</code>.</p>

<p>Note that: Each letter is counted <strong>individually</strong> for replacement so&nbsp;if for example&nbsp;<code>s[left..right] = &quot;aaa&quot;</code>, and <code>k = 2</code>, we can only replace two of the letters.&nbsp; (Also, note that the initial string <code>s</code>&nbsp;is never modified by any query.)</p>

<p>&nbsp;</p>
<p><strong>Example :</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abcda&quot;, queries = [[3,3,0],[1,2,0],[0,3,1],[0,3,2],[0,4,1]]
<strong>Output:</strong> [true,false,false,true,true]
<strong>Explanation:</strong>
queries[0] : substring = &quot;d&quot;, is palidrome.
queries[1] :&nbsp;substring = &quot;bc&quot;, is not palidrome.
queries[2] :&nbsp;substring = &quot;abcd&quot;, is not palidrome after replacing only 1 character.
queries[3] :&nbsp;substring = &quot;abcd&quot;, could be changed to &quot;abba&quot; which is palidrome. Also this can be changed to &quot;baab&quot; first rearrange it &quot;bacd&quot; then replace &quot;cd&quot; with &quot;ab&quot;.
queries[4] :&nbsp;substring = &quot;abcda&quot;,&nbsp;could be changed to &quot;abcba&quot; which is palidrome.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length,&nbsp;queries.length&nbsp;&lt;= 10^5</code></li>
	<li><code>0 &lt;= queries[i][0] &lt;= queries[i][1] &lt;&nbsp;s.length</code></li>
	<li><code>0 &lt;= queries[i][2] &lt;= s.length</code></li>
	<li><code>s</code> only contains lowercase English letters.</li>
</ul>

</div>

## Tags
- Array (array)
- String (string)

## Companies
- SAP - 3 (taggedByAdmin: false)
- Akuna Capital - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python 100% runtime and memory
- Author: yentup
- Creation Date: Sun Sep 01 2019 13:38:51 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 20 2019 14:04:05 GMT+0800 (Singapore Standard Time)

<p>
First, the key is understanding we are allowed to *rearrange*. Knowing this, we can forget about order and only count the occurences of each letter.
Furthermore, we only care if the count is odd. If it\'s even, we can place an equal amount of letter on either side:
`\'aaaac\'` => `\'aacaa\'`
Since `\'a\'`\'s count is even (4), we can ignore it.

In order to convert a string to a palindrome using replace, we need only to replace half of the letters. For example,
`\'abcd\'` => `\'abba\'` (4 // 2 = 2)
`\'abcde\'` => `\'abcba\'` (5 // 2 = 2)
Hence, we only need `k` to be at least half rounded down.

Naively, we can use `collections.Counter()` to count the letters in the substring, check how many are odd, divide that by 2 (rounded down) and check `<= k`.

```
  def canMakePaliQueries(s, queries):
      ans = []
      for l, r, k in queries:
          ss = s[l:r+1]
          rem = 0
          for letter, n in collections.Counter(ss).items():
              rem += n % 2
          need = rem // 2
          ans.append(need <= k)
      return ans
```

However, we are counting letters for **every** substring, which may overlap with previous calculations. This solution gives Time Limit Exceeded.

Therefore, we can optimize by caching previous results. We can say `dp[i]` represents the count of all the letters in `s[:i]`. Then we know that the counts of `dp[l]` subtracted from `dp[r+1]` will give us the count of all the letters in the substring `s[l:r+1]`. We could naively store them all in `collections.Counter()`:

```
  def canMakePaliQueries(s, queries):
      dp = [collections.Counter()]
      for i in range(1, len(s)+1):
          dp.append(dp[i-1] + collections.Counter(s[i-1]))
      ans = []
      for l, r, k in queries:
          c = dp[r+1] - dp[l]
          need = sum(v % 2 for v in c.values()) // 2
          ans.append(need <= k)
      return ans
```

However, the overhead is too great. This solution still gives Time Limit Exceeded. We can simplify by realizing we only care about lowercase letters, 26 in total. We can store our data in an array of size 26 for each substring:

```
  def canMakePaliQueries(s, queries):
      N = 26
      a = ord(\'a\')
      dp = [[0] * N]
      for i in range(1, len(s)+1):
          new = dp[i-1][:]
          j = ord(s[i-1]) - a
          new[j] += 1
          dp.append(new)
      ans = []
      for l, r, k in queries:
          L = dp[l]
          R = dp[r+1]
          ans.append(sum((R[i] - L[i]) & 1 for i in range(N)) // 2 <= k)
      return ans
```

This solution is accepted!

Furthermore, observe that once we subtract the sums we are only looking at `& 1` (the last bit) to see if it\'s odd or even. Therefore, we only need to store binary values of either `0` or `1` in the first place. Thus we make our final optimization by storing the count of the letters in a single 32-bit integer, since we only need to use 26 bits. Then we can XOR the substrings\' counts to get the difference. Then we can count the number of `1` bits to know how many letters need to be changed.

Final solution:

```
def canMakePaliQueries(s, queries):
    N = 26
    S = len(s) + 1
    ints = list(map(lambda c: ord(c) - ord(\'a\'), s))

    dp = [0] * S
    for i in range(1, S):
        dp[i] = dp[i-1] ^ (1 << ints[i-1])

    ones = lambda x: bin(x).count(\'1\')
    return [
        ones(dp[r+1] ^ dp[l]) >> 1 <= k
        for l, r, k in queries
    ]
```
</p>


### [Java/Python 3] 3 codes each: prefix sum, boolean, and xor of characters' frequencies then compare
- Author: rock
- Creation Date: Sun Sep 01 2019 12:05:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 14 2019 23:38:56 GMT+0800 (Singapore Standard Time)

<p>

According to the description and examples, we can deduce that the `rearrange` implies the input string `s` can be changed to any sequence to get `close` to a palindrome. Here `close` means least times of replacement of chars needed to become a palindrome after `rearranging` of `s`.

Obviously, any two same chars can be `rearranged` to be symmetric part of a palindrome, only the odd count of the chars we need to care. Therefore, when looping through the String `s`, we need to know if the count of each char in any prefix of the stirng `s` is odd or not by computing the corrsponding prefix sum, then the same information can be obtained for substring between any two given indices `i` and `j` by `prefixSum[j] - prefixSum[i]`. 
Sum those chars with odd number of occurrences, and we only need to replace half of them to make all in symmetric pairs. 

e.g.,

If we want to make palindrome from `s = "abbcccddddeeeee"`: 
In `"abbcccddddeeeee"`, `a, b, c, d, and e` occur `1, 2, 3, 4, and 5` times, respectively. Among them, `a, c and e` occur `1, 3, and 5` times.

We can easily rearrange `2 b\'s, 2c\'s, 4 d\'s and 4 e\'s` to a palindrome: `bcddeeeeddcb`. Now only odd number occurrence chars `a, c, and e` remaining, and we can place 1 of them, say, `e`, to the center of the palindrome, and replace either `a` to `c`, or `c` to `a` to complete the conversion of `s` to a palindrome: `c -> a: abcddeeeeeddcba` or `a -> c: bccddeeeeddccb`. 

In short, at least need `3 / 2 = 1` replacement: `3` odd number occurrence of chars `a, c, and e` divide by `2` equals to `1`

Here is the brief description of my algorithm:

1. Compute the prefix sum by counting the number of chars in `substring(0,1), substring(0, 2), ..., substring(0,n)`;
2. Use the difference of the prefix sums to get the the number of chars in `substring(queries[i][0], queries[i][1])`, count those do NOT in symmetric pairs, divided by 2, and compare it with `queries[i][2]`.

**Method 1: prefix sum - count each char**

**Java**
```
    public List<Boolean> canMakePaliQueries(String s, int[][] queries) {
        List<Boolean> ans = new ArrayList<>(); 
        int[][] cnt = new int[s.length() + 1][26];
        for (int i = 0; i < s.length(); ++i) {
            cnt[i + 1] = cnt[i].clone(); // copy previous sum.
            ++cnt[i + 1][s.charAt(i) - \'a\'];
        }
        for (int[] q : queries) {
            int sum = 0; 
            for (int i = 0; i < 26; ++i) {
                sum += (cnt[q[1] + 1][i] - cnt[q[0]][i]) % 2;
            }
            ans.add(sum / 2 <= q[2]);
        }
        return ans;
    }
```

----

**Python 3**
```
    def canMakePaliQueries(self, s: str, queries: List[List[int]]) -> List[bool]:
        cnt = [[0] * 26]
        for i, c in enumerate(s):
            cnt.append(cnt[i][:])
            cnt[i + 1][ord(c) - ord(\'a\')] += 1
        return [sum((cnt[hi + 1][i] - cnt[lo][i]) % 2 for i in range(26)) // 2 <= k for lo, hi, k in queries]
```

----

**Method 2: prefix boolean - use true/false to mark odd/even of the count of each char, credit to @mfboulos**

**Java**
```
    public List<Boolean> canMakePaliQueries(String s, int[][] queries) {
        List<Boolean> ans = new ArrayList<>(); 
        boolean[][] odds = new boolean[s.length() + 1][26]; // odds[i][j]: within range [0...i) of s, if the count of (char)(j + \'a) is odd. 
        for (int i = 0; i < s.length(); ++i) {
            odds[i + 1] = odds[i].clone();
            odds[i + 1][s.charAt(i) - \'a\'] ^= true;
        }
        for (int[] q : queries) {
            int sum = 0; 
            for (int i = 0; i < 26; ++i) {
                sum += (odds[q[1] + 1][i] ^ odds[q[0]][i]) ? 1 : 0; // if the count of (char)(i + \'a\') in substring(q[0], q[1] + 1) is odd. 
            }
            ans.add(sum / 2 <= q[2]);
        }
        return ans;
    }
```

----

**Python 3**

```
    def canMakePaliQueries(self, s: str, queries: List[List[int]]) -> List[bool]:
        odds = [[False] * 26]
        for i, c in enumerate(s):
            odds.append(odds[i][:])
            odds[i + 1][ord(c) - ord(\'a\')] ^= True
        return [sum(odds[hi + 1][i] ^ odds[lo][i] for i in range(26)) // 2 <= k for lo, hi, k in queries]  
```

----

**Method 3: prefix xor - use integer bit 0/1 to mark the even/odd of the count of each char**

The `32` bits in a Java `int` are enough to cover the odd/even of the counts of `26` English letters. Use only the 26 least significant bits in an `int`.

**Java**

```
    public List<Boolean> canMakePaliQueries(String s, int[][] queries) {
        List<Boolean> ans = new ArrayList<>(); 
        int[] odds = new int[s.length() + 1]; // odds[i]: within range [0...i) of s, the jth bit of odd[i] indicates even/odd of the count of (char)(j + \'a\'). 
        for (int i = 0; i < s.length(); ++i)
            odds[i + 1] = odds[i] ^ 1 << s.charAt(i) - \'a\';
        for (int[] q : queries)
            ans.add(Integer.bitCount(odds[q[1] + 1] ^ odds[q[0]]) / 2 <= q[2]); // odds[q[1] + 1] ^ odds[q[0]] indicates the count of (char)(i + \'a\') in substring(q[0], q[1] + 1) is even/odd.
        return ans;
    }
```

----

**Python 3**

```
    def canMakePaliQueries(self, s: str, queries: List[List[int]]) -> List[bool]:
        odds = [False]
        for i, c in enumerate(s):
            odds.append(odds[i] ^ 1 << (ord(c) - ord(\'a\')))
        return [bin(odds[hi + 1] ^ odds[lo]).count(\'1\') // 2 <= k for lo, hi, k in queries]   
```

----

**Analysis:**

Time & space: O(S + Q), where S = s.length(), Q = queries.length.
</p>


### Short and fast C++ prefix xor solution, beats 100%
- Author: mzchen
- Creation Date: Sun Sep 01 2019 14:27:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 07 2019 13:04:10 GMT+0800 (Singapore Standard Time)

<p>
We care only about the parity of the character sum in the substring, so there is no need to do the full counting.
```
vector<bool> canMakePaliQueries(string s, vector<vector<int>>& queries) {
    int mask = 0;
    vector<int> ps(1);
    for (char c : s)
        ps.push_back(mask ^= 1 << (c - \'a\'));

    vector<bool> r;
    for (auto &q : queries) {
        int odds = __builtin_popcount(ps[q[1] + 1] ^ ps[q[0]]);
        r.push_back(q[2] >= odds / 2);
    }
    return r;
}
```
PS: The `__builtin_popcount()` function is for g++ only. For C++20 use `popcount()`. For other compilers use `bitset<26>().count()`.
</p>


