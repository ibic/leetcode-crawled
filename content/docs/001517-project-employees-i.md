---
title: "Project Employees I"
weight: 1517
#id: "project-employees-i"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Project</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| project_id  | int     |
| employee_id | int     |
+-------------+---------+
(project_id, employee_id) is the primary key of this table.
employee_id is a foreign key to <code>Employee</code> table.
</pre>

<p>Table:&nbsp;<code>Employee</code></p>

<pre>
+------------------+---------+
| Column Name      | Type    |
+------------------+---------+
| employee_id      | int     |
| name             | varchar |
| experience_years | int     |
+------------------+---------+
employee_id is the primary key of this table.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query that reports the <strong>average</strong>&nbsp;experience years of all the employees for each project, <strong>rounded to 2 digits</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
Project table:
+-------------+-------------+
| project_id  | employee_id |
+-------------+-------------+
| 1           | 1           |
| 1           | 2           |
| 1           | 3           |
| 2           | 1           |
| 2           | 4           |
+-------------+-------------+

Employee table:
+-------------+--------+------------------+
| employee_id | name   | experience_years |
+-------------+--------+------------------+
| 1           | Khaled | 3                |
| 2           | Ali    | 2                |
| 3           | John   | 1                |
| 4           | Doe    | 2                |
+-------------+--------+------------------+

Result table:
+-------------+---------------+
| project_id  | average_years |
+-------------+---------------+
| 1           | 2.00          |
| 2           | 2.50          |
+-------------+---------------+
The average experience years for the first project is (3 + 2 + 1) / 3 = 2.00 and for the second project is (3 + 2) / 2 = 2.50
</pre>

</div>

## Tags


## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Solution
- Author: Slabhead
- Creation Date: Sun Sep 22 2019 12:13:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 22 2019 12:13:40 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT             p.project_id,
                   ROUND(AVG(e.experience_years), 2) AS average_years
FROM               Project AS p
INNER JOIN         Employee AS e
ON                 p.employee_id = e.employee_id
GROUP BY           p.project_id


```
</p>


### Why the expected answer needs 5 decimal places?
- Author: YOGGITA
- Creation Date: Mon Jul 06 2020 12:38:53 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 06 2020 12:38:53 GMT+0800 (Singapore Standard Time)

<p>
My answer:

SELECT P.PROJECT_ID, ROUND((CAST(SUM(E.EXPERIENCE_YEARS) AS FLOAT)/ COUNT(P.EMPLOYEE_ID)), 2) AS AVERAGE_YEARS
FROM PROJECT P
LEFT JOIN EMPLOYEE E 
ON E.EMPLOYEE_ID = P.EMPLOYEE_ID 
GROUP BY P.PROJECT_ID
ORDER BY P.PROJECT_ID;



</p>


### MySQL - beats 88% , not bad though
- Author: jknaveenraj50
- Creation Date: Fri Aug 16 2019 10:43:58 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 16 2019 10:43:58 GMT+0800 (Singapore Standard Time)

<p>
```
select A.project_id, round(avg(A.experience_years), 2) as average_years from
(select p.project_id, p.employee_id, e.experience_years from Project p INNER JOIN Employee e ON
p.employee_id = e.employee_id) as A GROUP BY A.project_id
</p>


