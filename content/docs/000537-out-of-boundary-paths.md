---
title: "Out of Boundary Paths"
weight: 537
#id: "out-of-boundary-paths"
---
## Description
<div class="description">
<p>There is an <b>m</b> by <b>n</b> grid with a ball. Given the start coordinate <b>(i,j)</b> of the ball, you can move the ball to <b>adjacent</b> cell or cross the grid boundary in four directions (up, down, left, right). However, you can <b>at most</b> move <b>N</b> times. Find out the number of paths to move the ball out of grid boundary. The answer may be very large, return it after mod 10<sup>9</sup> + 7.</p>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input: </b>m = 2, n = 2, N = 2, i = 0, j = 0
<b>Output:</b> 6
<b>Explanation:</b>
<img src="https://assets.leetcode.com/uploads/2018/10/13/out_of_boundary_paths_1.png" style="width: 100%; max-width: 400px" />
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input: </b>m = 1, n = 3, N = 3, i = 0, j = 1
<b>Output:</b> 12
<b>Explanation:</b>
<img src="https://assets.leetcode.com/uploads/2018/10/12/out_of_boundary_paths_2.png" style="width: 100%; max-width: 400px" />
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>Once you move the ball out of boundary, you cannot move it back.</li>
	<li>The length and height of the grid is in range [1,50].</li>
	<li>N is in range [0,50].</li>
</ol>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Depth-first Search (depth-first-search)

## Companies
- Baidu - 0 (taggedByAdmin: true)

## Official Solution
[TOC]
## Summary



## Solution

---
#### Approach 1: Brute Force

**Algorithm**

In the brute force approach, we try to take one step in every direction and decrement the number of pending moves for each step taken. Whenever we reach out of the boundary while taking the steps, we deduce that one extra path is available to take the ball out. 

In order to implement the same, we make use of a recursive function `findPaths(m,n,N,i,j)` which takes the current number of moves($$N$$) along with the current position($$(i,j)$$ as some of the parameters and returns the number of moves possible to take the ball out with the current pending moves from the current position. Now, we take a step in every direction and update the corresponding indices involved along with the current number of pending moves. 

Further, if we run out of moves at any moment, we return a 0 indicating that the current set of moves doesn't take the ball out of boundary.

<iframe src="https://leetcode.com/playground/wuQHGNCy/shared" frameBorder="0" width="100%" height="225" name="wuQHGNCy"></iframe>

**Complexity Analysis**

* Time complexity : $$O(4^n)$$. Size of recursion tree will be $$4^n$$. Here, $$n$$ refers to the number of moves allowed.

* Space complexity : $$O(n)$$. The depth of the recursion tree can go upto $$n$$.

---
#### Approach 2: Recursion with Memoization

**Algorithm**

In the brute force approach, while going through the various branches of the recursion tree, we could reach the same position with the same number of moves left. 

Thus, a lot of redundant function calls are made with the same set of parameters leading to a useless increase in runtime. We can remove this redundancy by making use of a memoization array, $$memo$$. $$memo[i][j][k]$$ is used to store the number of possible moves leading to a path out of the boundary if the current position is given by the indices $$(i, j)$$ and number of moves left is $$k$$. 

Thus, now if a function call with some parameters is repeated, the $$memo$$ array will already contain valid values corresponding to that function call resulting in pruning of the search space.

<iframe src="https://leetcode.com/playground/xhGNLVP4/shared" frameBorder="0" width="100%" height="395" name="xhGNLVP4"></iframe>

**Complexity Analysis**

* Time complexity : $$O(mnN)$$. We need to fill the $$memo$$ array once with dimensions $$m \times n \times N$$. Here, $$m$$, $$n$$ refer to the number of rows and columns of the given grid respectively. $$N$$ refers to the total number of allowed moves.

* Space complexity : $$O(mnN)$$. $$memo$$ array of size $$m \times n \times N$$ is used.

---

#### Approach 3: Dynamic Programming

**Algorithm**

The idea behind this approach is that if we can reach some position in $$x$$ moves, we can reach all its adjacent positions in $$x+1$$ moves. Based on this idea, we make use of a 2-D $$dp$$ array to store the number of ways in which a particular position can be reached. $$dp[i][j]$$ refers to the number of ways the position corresponding to the indices $$(i,j)$$ can be reached given some particular number of moves.

Now, if the current $$dp$$ array stores the number of ways the various positions can be reached by making use of $$x-1$$ moves, in order to determine the number of ways the position $$(i,j)$$ can be reached by making use of $$x$$ moves, we need to update the corresponding $$dp$$ entry as $$dp[i][j] = dp[i-1][j] + dp[i+1][j] + dp[i][j-1] + dp[i][j+1]$$ taking care of boundary conditions. This happens because we can reach the index $$(i,j)$$ from any of the four adjacent positions and the total number of ways of reaching the index $$(i,j)$$ in $$x$$ moves is the sum of the ways of reaching the adjacent positions in $$x-1$$ moves. 

But, if we alter the $$dp$$ array, now some of the entries will correspond to $$x-1$$ moves and the updated ones will correspond to $$x$$ moves. Thus, we need to find a way to tackle this issue. So, instead of updating the $$dp$$ array for the current($$x$$) moves, we make use of a temporary 2-D array $$temp$$ to store the updated results for $$x$$ moves, making use of the results obtained for $$dp$$ array corresponding to $$x-1$$ moves. After all the entries for all the positions have been considered for $$x$$ moves, we update the $$dp$$ array based on $$temp$$. Thus, $$dp$$ now contains the entries corresponding to $$x$$ moves.

Thus, we start off by considering zero move available for which we make an initial entry of $$dp[x][y] = 1$$($$(x,y)$$ is the initial position), since we can reach only this position in zero move. Then, we increase the number of moves to 1 and update all the $$dp$$ entries appropriately. We do so for all the moves possible from 1 to N. 

In order to update $$count$$, which indicates the total number of possible moves which lead an out of boundary path, we need to perform the update only when we reach the boundary. We update the count as $$count = count + dp[i][j]$$, where $$(i,j)$$ corresponds to one of the boundaries. But, if $$(i,j)$$ is simultaneously a part of multiple boundaries, we need to add the $$dp[i][j]$$ factor multiple times(same as the number of boundaries to which $$(i,j)$$ belongs).

After we are done with all the $$N$$ moves, $$count$$ gives the required result.

The following animation illustrates the process:

!?!../Documents/576_Boundary_Paths.json:1000,563!?!


<iframe src="https://leetcode.com/playground/s5fQhXWv/shared" frameBorder="0" width="100%" height="480" name="s5fQhXWv"></iframe>

**Complexity Analysis**

* Time complexity : $$O(Nmn)$$. We need to fill the $$dp$$ array with dimensions $$m \times n$$ $$N$$ times. Here $$m \times n$$ refers to the size of the grid and $$N$$ refers to the number of moves available.

* Space complexity : $$O(mn)$$. $$dp$$ and $$temp$$ array of size $$m \times n$$ are used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution, DP with space compression
- Author: shawngao
- Creation Date: Sun May 07 2017 13:35:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 10:51:46 GMT+0800 (Singapore Standard Time)

<p>
```DP[i][j][k]``` stands for how many possible ways to walk into cell ```j,k``` in step ```i```, ```DP[i][j][k]``` only depends on ```DP[i - 1][j][k]```, so we can compress 3 dimensional dp array to 2 dimensional. 

```
public class Solution {
    public int findPaths(int m, int n, int N, int i, int j) {
        if (N <= 0) return 0;
        
        final int MOD = 1000000007;
        int[][] count = new int[m][n];
        count[i][j] = 1;
        int result = 0;
        
        int[][] dirs = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};
        
        for (int step = 0; step < N; step++) {
            int[][] temp = new int[m][n];
            for (int r = 0; r < m; r++) {
                for (int c = 0; c < n; c++) {
                    for (int[] d : dirs) {
                        int nr = r + d[0];
                        int nc = c + d[1];
                        if (nr < 0 || nr >= m || nc < 0 || nc >= n) {
                            result = (result + count[r][c]) % MOD;
                        }
                        else {
                            temp[nr][nc] = (temp[nr][nc] + count[r][c]) % MOD;
                        }
                    }
                }
            }
            count = temp;
        }
        
        return result;
    }
}
```
</p>


### C++ 6 lines DP O(N * m * n), 6 ms
- Author: votrubac
- Creation Date: Sun May 07 2017 11:03:00 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 24 2018 04:35:18 GMT+0800 (Singapore Standard Time)

<p>
The number of paths for N moves is the sum of paths for N - 1 moves from the adjacent cells. If an adjacent cell is out of the border, the number of paths is 1.
```
int findPaths(int m, int n, int N, int i, int j) {
  uint dp[51][50][50] = {};
  for (auto Ni = 1; Ni <= N; ++Ni)
    for (auto mi = 0; mi < m; ++mi)
      for (auto ni = 0; ni < n; ++ni)
        dp[Ni][mi][ni] = ((mi == 0 ? 1 : dp[Ni - 1][mi - 1][ni]) + (mi == m - 1? 1 : dp[Ni - 1][mi + 1][ni])
            + (ni == 0 ? 1 : dp[Ni - 1][mi][ni - 1]) + (ni == n - 1 ? 1 : dp[Ni - 1][mi][ni + 1])) % 1000000007;
  return dp[N][i][j];
}
```
We can also reduce the memory usage by using two grids instead of N, as we only need to look one step back. We can use N % 2 and (N + 1) % 2 to alternate grids so we do not have to copy.
```
int findPaths(int m, int n, int N, int i, int j) {
    unsigned int g[2][50][50] = {};
    while (N-- > 0)
        for (auto k = 0; k < m; ++k)
            for (auto l = 0, nc = (N + 1) % 2, np = N % 2; l < n; ++l)
                g[nc][k][l] = ((k == 0 ? 1 : g[np][k - 1][l]) + (k == m - 1 ? 1 : g[np][k + 1][l])
                    + (l == 0 ? 1 : g[np][k][l - 1]) + (l == n - 1 ? 1 : g[np][k][l + 1])) % 1000000007;
    return g[1][i][j];
}
```
As suggested by @mingthor, we can further decrease the memory usage (2 * m * n >> m * (n + 1)) as we only looking one row up. We will store new values for the current row in an array, and write these values back to the matrix as we process cells in the next row. This approach, however, impacts the runtime as we need extra copying for each step.

I experimented with different n and m (50 - 500), and N (5,000 - 50,000), and the second solution is approximately 10% faster than this one.
```
int findPaths(int m, int n, int N, int i, int j) {
    unsigned int g[50][50] = {}, r[50];
    while (N-- > 0)
        for (auto k = 0; k <= m; ++k)
            for (auto l = 0; l < n; ++l) {
                auto tmp = r[l];
                r[l] = (k == m ? 0 : ((k == 0 ? 1 : g[k - 1][l]) + (k == m - 1 ? 1 : g[k + 1][l])
                    + (l == 0 ? 1 : g[k][l - 1]) + (l == n - 1 ? 1 : g[k][l + 1])) % 1000000007);
                if (k > 0) g[k - 1][l] = tmp;
            }
    return g[i][j];
}
```
</p>


### Java DFS with memorization
- Author: crazy09
- Creation Date: Fri Mar 16 2018 03:31:06 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 04:13:10 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    private int[][] dirs = {{-1, 0}, {1, 0}, {0, 1}, {0, -1}};
    private int mod = 1000000000 + 7;
    
    public int findPaths(int m, int n, int N, int i, int j) {
        // m * n grid
        long[][][] memo = new long[m][n][N+1];
        for (int ii = 0; ii < m; ii++) {
            for (int jj = 0; jj < n; jj++) {
                for (int kk = 0; kk < N+1; kk++) {
                    memo[ii][jj][kk] = -1;
                }
            }
        }
        return (int) (dfs(m, n, N, i, j, memo) % mod);
    }
    
    public long dfs(int m, int n, int N, int i, int j, long[][][] memo) {
        //check if out of boundary, if out could not move back
        if (i < 0 || i >= m || j < 0 || j >= n) {
            return 1;
        }
        if (N == 0) return 0;
        if (memo[i][j][N] != -1) return memo[i][j][N];
        memo[i][j][N] = 0;
        for (int dir[] : dirs) {
            int x = dir[0] + i;
            int y = dir[1] + j;
            memo[i][j][N] = (memo[i][j][N] + dfs(m, n, N - 1, x, y, memo) % mod) % mod;
        }
        return memo[i][j][N];
        
    }
}
```

Java DFS with memorization to store the result. I think both the time and space complexity is O(mnN)
















</p>


