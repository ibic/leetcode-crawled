---
title: "Increasing Order Search Tree"
weight: 847
#id: "increasing-order-search-tree"
---
## Description
<div class="description">
<p>Given a binary search tree, rearrange the tree in <strong>in-order</strong> so that the leftmost node in the tree is now the root of the tree, and every node has no left child and only 1 right child.</p>

<pre>
<strong>Example 1:</strong>
<strong>Input:</strong> [5,3,6,2,4,null,8,1,null,null,null,7,9]

       5
      / \
    3    6
   / \    \
  2   4    8
&nbsp;/        / \ 
1        7   9

<strong>Output:</strong> [1,null,2,null,3,null,4,null,5,null,6,null,7,null,8,null,9]

 1
&nbsp; \
&nbsp;  2
&nbsp;   \
&nbsp;    3
&nbsp;     \
&nbsp;      4
&nbsp;       \
&nbsp;        5
&nbsp;         \
&nbsp;          6
&nbsp;           \
&nbsp;            7
&nbsp;             \
&nbsp;              8
&nbsp;               \
                 9  </pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the given tree will be between <code>1</code> and <code>100</code>.</li>
	<li>Each node will have a unique integer value from <code>0</code> to <code>1000</code>.</li>
</ul>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: In-Order Traversal

**Intuition**

The definition of a binary search tree is that for every node, all the values of the left branch are less than the value at the root, and all the values of the right branch are greater than the value at the root.

Because of this, an *in-order traversal* of the nodes will yield all the values in increasing order.

**Algorithm**

Once we have traversed all the nodes in increasing order, we can construct new nodes using those values to form the answer.

<iframe src="https://leetcode.com/playground/LbNwYipx/shared" frameBorder="0" width="100%" height="378" name="LbNwYipx"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the given tree.

* Space Complexity:  $$O(N)$$, the size of the answer.
<br />
<br />


---
#### Approach 2: Traversal with Relinking

**Intuition and Algorithm**

We can perform the same in-order traversal as in *Approach 1*.  During the traversal, we'll construct the answer on the fly, reusing the nodes of the given tree by cutting their left child and adjoining them to the answer.

<iframe src="https://leetcode.com/playground/5y2E8kL9/shared" frameBorder="0" width="100%" height="361" name="5y2E8kL9"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the given tree.

* Space Complexity:  $$O(H)$$ in *additional* space complexity, where $$H$$ is the height of the given tree, and the size of the implicit call stack in our in-order traversal.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Self-Explained, 5-line, O(N)
- Author: lee215
- Creation Date: Sun Sep 02 2018 11:10:24 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 19 2019 10:05:34 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Don\'t need the condition of BST, just in-order output the whole tree.

Straight forward idea here:
`result = inorder(root.left) + root + inorder(root.right)`
<br>

## **Explanation**

Recursively call function `increasingBST(TreeNode root, TreeNode tail)`
`tail` is its next node in inorder,
\uFF08the word `next` may be easier to understand, but it\u2019s a keyword in python)

If `root == null`, the head will be `tail`, so we return `tail` directly.

we recursively call `increasingBST(root.left, root)`,
change left subtree into the linked list + current node.

we recursively call `increasingBST(root.right, tail)`,
change right subtree into the linked list + tail.

Now the result will be in a format of linked list, with right child is next node.
Since it\'s single linked list, so we set `root.left = null`.
Otherwise it will be TLE for Leetcode judgment to traverse over your tree.

The result now is `increasingBST(root.left) + root + increasingBST(root.right)`.

One tip here, we should arrange the old tree, not create a new tree.
The leetcode judgment comparer only the values,
so it won\'t take it as wrong answer if you return a new tree,
but it is wrong.
<br>

## **Complexity**
`O(N)` time traversal of all nodes
`O(height)` space
<br>

**C++:**
```cpp
    TreeNode* increasingBST(TreeNode* root, TreeNode* tail = NULL) {
        if (!root) return tail;
        TreeNode* res = increasingBST(root->left, root);
        root->left = NULL;
        root->right = increasingBST(root->right, tail);
        return res;
    }
```

**Java:**
```java
    public TreeNode increasingBST(TreeNode root) {
        return increasingBST(root, null);
    }

    public TreeNode increasingBST(TreeNode root, TreeNode tail) {
        if (root == null) return tail;
        TreeNode res = increasingBST(root.left, root);
        root.left = null;
        root.right = increasingBST(root.right, tail);
        return res;
    }
```
**Python:**
```python
    def increasingBST(self, root, tail = None):
        if not root: return tail
        res = self.increasingBST(root.left, root)
        root.left = None
        root.right = self.increasingBST(root.right, tail)
        return res
```

</p>


### Java Simple InOrder Traversal- with Explanation
- Author: bmask
- Creation Date: Sun Sep 02 2018 11:06:15 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 14:53:47 GMT+0800 (Singapore Standard Time)

<p>
```
   TreeNode prev=null, head=null;
    public TreeNode increasingBST(TreeNode root) {
        if(root==null) return null;   
        increasingBST(root.left);  
        if(prev!=null) { 
        	root.left=null; // we no  longer needs the left  side of the node, so set it to null
        	prev.right=root; 
        }
        if(head==null) head=root; // record the most left node as it will be our root
        prev=root; //keep track of the prev node
        increasingBST(root.right); 
        return head;
    }
```
</p>


### Inorder traversal
- Author: wangzi6147
- Creation Date: Sun Sep 02 2018 12:39:28 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 19:10:19 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    private TreeNode result;
    private TreeNode pre;
    public TreeNode increasingBST(TreeNode root) {
        inorder(root);
        return result;
    }
    private void inorder(TreeNode root) {
        if (root == null) {
            return;
        }
        inorder(root.left);
        if (result == null) {
            result = root;
        } else {
            pre.right = root;
        }
        pre = root;
        root.left = null;
        inorder(root.right);
    }
}
```
</p>


