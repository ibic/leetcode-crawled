---
title: "Longest Duplicate Substring"
weight: 1029
#id: "longest-duplicate-substring"
---
## Description
<div class="description">
<p>Given a string <code>S</code>, consider all <em>duplicated substrings</em>: (contiguous) substrings of S that occur 2 or more times.&nbsp; (The occurrences&nbsp;may overlap.)</p>

<p>Return <strong>any</strong> duplicated&nbsp;substring that has the longest possible length.&nbsp; (If <code>S</code> does not have a duplicated substring, the answer is <code>&quot;&quot;</code>.)</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;banana&quot;</span>
<strong>Output: </strong><span id="example-output-1">&quot;ana&quot;</span>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;abcd&quot;</span>
<strong>Output: </strong><span id="example-output-2">&quot;&quot;</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>2 &lt;= S.length &lt;= 10^5</code></li>
	<li><code>S</code> consists of lowercase English letters.</li>
</ol>

</div>

## Tags
- Hash Table (hash-table)
- Binary Search (binary-search)

## Companies
- Amazon - 7 (taggedByAdmin: true)
- Microsoft - 6 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Binary Search + Rabin-Karp

**String Searching Algorithms**

The problem is a follow-up of [Longest Repeating Substring](https://leetcode.com/problems/longest-repeating-substring/),
and typically used to check if you're comfortable with
[string searching algortihms](https://en.wikipedia.org/wiki/String-searching_algorithm#Single-pattern_algorithms).

Best algorithms have a linear execution time in average. 
The most popular ones are
[Aho-Corasick](https://en.wikipedia.org/wiki/Aho%E2%80%93Corasick_algorithm),
[KMP](https://en.wikipedia.org/wiki/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm) and 
[Rabin-Karp](https://en.wikipedia.org/wiki/Rabin%E2%80%93Karp_algorithm):
Aho-Corasick is used by [fgrep](https://en.wikipedia.org/wiki/Grep#Variations),
KMP is used for [chinese string searching](https://www.aclweb.org/anthology/C96-2200),
and Rabin-Karp is used for plagiarism detection and in bioinformatics to look for similarities in two
or more proteins.

The first two are optimised for a single pattern search,
and Rabin-Karp for a multiple pattern search, 
that is exactly the case here.

**Split into two subtasks**

Here we have "two in one" problem :

1. Perform a search by a substring length in the interval from 1 to N.

2. Check if there is a duplicate substring of a given length L.

**Subtask one : Binary search**

A naive solution would be to check all possible string length 
one by one starting from N - 1: 
if there is a duplicate substring of length N - 1, then of length N - 2, etc. 
Note that if there is a duplicate substring of length k, that means
that there is a duplicate substring of length k - 1. 
Hence one could use a binary search by string length here,
and have the first problem solved in $$\mathcal{O}(\log N)$$ time.

![fig](../Figures/1044/binary.png)

**Subtask two : Rabin-Karp**

Subtask two, to check if there is duplicate substring of a given length,
is a multiple pattern search. 
Let's use Rabin-Karp algorithm to solve it in a linear time. 

The idea is very simple : 

- Move a sliding window of length L along the string of length N.
 
- Check if the string in the sliding window
is in the hashset of already seen strings. 

    - If yes, the duplicate substring is right here.
    
    - If not, save the string in the sliding window in the hashset.
    
!?!../Documents/1044_LIS.json:1000,411!?!

The linear time implementation of this idea is a bit
tricky because of two technical problems:

1. [How to implement a string slice in a constant time?](https://stackoverflow.com/questions/35180377/time-complexity-of-string-slice) 

2. Hashset memory consumption could be huge for very long strings. 
One could keep the string hash instead of string itself
but hash generation costs $$\mathcal{O}(L)$$ for the string of length L,
and the complexity of algorithm would be $$\mathcal{O}((N - L)L)$$,
N - L for the slice and L for the hash generation. 
One has to think how to generate hash in a constant time here.

Let's now address these problems.

**String slice in a constant time**

That's a very language-dependent problem. For the moment for 
Java and Python there is no straightforward solution, 
and to move sliding window in a constant time
one has to convert string to another data structure. 

Python is already providing [memoryview](https://docs.python.org/3/library/stdtypes.html#memoryview),
which is known to be surprisingly slow,
and there are a lot of discussion about [strview](https://mail.python.org/pipermail/python-ideas/2011-December/012993.html).

The simplest solution both for Java and Python is to convert string to integer array of ascii-values.

**Rolling hash : hash generation in a constant time**

To generate hash of array of length L, one needs $$\mathcal{O}(L)$$ time.

> How to have constant time of hash generation? Use the advantage of 
slice: only one integer in, and only one - out. 

That's the idea of [rolling hash](https://en.wikipedia.org/wiki/Rolling_hash).
Here we'll implement the simplest one, polynomial rolling hash.
Beware that's polynomial rolling hash is NOT the [Rabin fingerprint](https://en.wikipedia.org/wiki/Rolling_hash#Rabin_fingerprint).

Since one deals here with lowercase English letters, all values 
in the integer array are between 0 and 25 :
`arr[i] = (int)S.charAt(i) - (int)'a'`.  
So one could consider string `abcd` -> `[0, 1, 2, 3]` as a number 
in a [numeral system](https://en.wikipedia.org/wiki/Numeral_system) with the base 26. 
Hence `abcd` -> `[0, 1, 2, 3]` could be hashed as 

$$
h_0 = 0 \times 26^3 + 1 \times 26^2 + 2 \times 26^1 + 3 \times 26^0
$$

Let's write the same formula in a generalised way, where $$c_i$$
is an integer array element and $$a = 26$$ is a system base.

$$
h_0 = c_0 a^{L - 1} + c_1 a^{L - 2} + ... + c_i a^{L - 1 - i} + ... + c_{L - 1} a^1 + c_L a^0
$$

$$
h_0 = \sum_{i = 0}^{L - 1}{c_i a^{L - 1 - i}}
$$

Now let's consider the slice `abcd` -> `bcde`. For int arrays that means
`[0, 1, 2, 3]` -> `[1, 2, 3, 4]`, to remove number 0 and to add number 4.

$$
h_1 = (h_0 - 0 \times 26^3) \times 26 + 4 \times 26^0
$$

In a generalised way

$$
h_1 = (h_0 a - c_0 a^L) + c_{L + 1}
$$

Now hash regeneration is perfect and fits in a constant time. 
There is one more issue to address: possible overflow problem. 

**How to avoid overflow**

$$a^L$$ could be a large number and hence
the idea is to set limits to avoid the overflow. 
To set limits means to limit a hash by a given number called modulus
and use everywhere not hash itself but `h % modulus`.

It's quite obvious that modulus should be large enough, but how 
large? [Here one could read more about the topic](https://en.wikipedia.org/wiki/Linear_congruential_generator#Parameters_in_common_use),
for the problem here the standard int overflow in $$2^{32}$$ is enough.

In a real life, when not all testcases are known in advance, 
one has to check if the strings with equal hashes are truly equal.
Such false-positive strings could happen 
because of a modulus which is too small and strings which are too long.
That leads to Rabin-Karp time complexity $$\mathcal{O}(NL)$$ 
in the worst case then almost all strings are false-positive. 
Here it's not the case because all testcases are known and 
one could adjust the modulus. 

Another one overflow issue here is purely Java related.
While in Python the hash regeneration goes perfectly fine, 
in Java the same thing is better to rewrite to avoid
long overflow. Check [here](https://leetcode.com/problems/longest-duplicate-substring/discuss/292982/Java-version-with-comment) 
the nice explanation by @[hqt](https://leetcode.com/hqt/).

<iframe src="https://leetcode.com/playground/k2CNUSU9/shared" frameBorder="0" width="100%" height="89" name="k2CNUSU9"></iframe>

**Binary search algorithm**

- Use binary search by a substring length to check lengths from 1 to N
`left = 1, right = N`. While left != right:

    - L = left + (right - left) / 2
    
    - If search(L) != -1 (i.e. there is a duplicate substring), left = L + 1
    
    - Otherwise (no duplicate substring), right = L. 
    
- Return duplicate string of length `left - 1`, or an empty string if 
there is no such a string.

**Rabin-Karp algorithm**

search(L) :
    
- Compute the hash of substring `S.substring(0, L)`
    and initiate the hashset of already seen substring with this value.
        
- Iterate over the start position of substring : from 1 to $$N - L$$.
        
    - Compute rolling hash based on the previous hash value.
            
    - Return start position if the hash is in the hashset,
        because that means a duplicate string. 
            
    - Otherwise, add hash in the hashset.
         
- Return -1, that means there is no duplicate string of length L.

**Implementation**

<iframe src="https://leetcode.com/playground/uhTGK5Mq/shared" frameBorder="0" width="100%" height="500" name="uhTGK5Mq"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N \log N)$$. $$\mathcal{O}(\log N)$$
for the binary search and $$\mathcal{O}(N)$$ for Rabin-Karp algorithm.
* Space complexity : $$\mathcal{O}(N)$$ to keep the hashset.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Binary Search
- Author: lee215
- Creation Date: Sun May 12 2019 12:06:27 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 14 2019 16:15:22 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Suffix array is typical solution for this problem.
The fastest way is to copy a template form the Internet.
The code will be quite long.
Here I want to share a binary search solution.
<br>


## **Explanation**
Binary search the length of longest duplicate substring and call the help function `test(L)`.
`test(L)` slide a window of length `L`,
rolling hash the string in this window,
record the `seen` string in a hashset,
and try to find duplicated string.

I give it a big `mod` for rolling hash and it should be enough for this problem.
Actually there could be hash collision.
One solution is to have two different mod for hash.
Or we can use a hashmap to record the index of string.
<br>

## **Complexity**
Binary Search in range 1 and N, so it\'s `O(logN)`
Rolling hash `O(N)`
Overall `O(NlogN)`
Space`O(N)`
<br>

**Python:**
```
    def longestDupSubstring(self, S):
        A = [ord(c) - ord(\'a\') for c in S]
        mod = 2**63 - 1

        def test(L):
            p = pow(26, L, mod)
            cur = reduce(lambda x, y: (x * 26 + y) % mod, A[:L], 0)
            seen = {cur}
            for i in xrange(L, len(S)):
                cur = (cur * 26 + A[i] - A[i - L] * p) % mod
                if cur in seen: return i - L + 1
                seen.add(cur)
        res, lo, hi = 0, 0, len(S)
        while lo < hi:
            mi = (lo + hi + 1) / 2
            pos = test(mi)
            if pos:
                lo = mi
                res = pos
            else:
                hi = mi - 1
        return S[res:res + lo]
```

</p>


### [Python] Binary search O(n log n) average with Rabin-Karp, explained
- Author: DBabichev
- Creation Date: Fri Jun 19 2020 17:34:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 21 2020 03:37:01 GMT+0800 (Singapore Standard Time)

<p>
This is quite difficult problem in fact and you will be very unlucky if you have this on real interview. However if you familiar with **Rabin-Karp** algorighm, it will be not so difficult. 
You can see it in full details on Wikipedia: https://en.wikipedia.org/wiki/Rabin%E2%80%93Karp_algorithm

Here I briefly explain the idea of it. Imagine, that we have string `abcabcddcb`, and we want to check if there are two substrings of size `3`, which are equal. Idea is to **hash** this substrings, using rolling hash, where `d` is our **base** and `q` is used to avoid overflow.
1. for `abc` we evaluate `[ord(a)*d^2 + ord(b)*d^1 + ord(c)*d^0] % q` 
2. for `bca` we evaluate `[ord(b)*d^2 + ord(c)*d^1 + ord(a)*d^0] % q`
  Note, that we can evaluate rolling hash in `O(1)`, for more details please see wikipedia.
...

However, it can happen that we have **collisions**, we can falsely get two substrings with the same hash, which are not equal. So, we need to check our candidates.

### Binary search for help
Note, that we are asked for the longest duplicate substring. If we found duplicate substring of length `10`, it means that there are duplicate substrings of lenths `9,8, ...`. So, we can use **binary search** to find the longest one.

**Complexity** of algorighm is `O(n log n)` if we assume that probability of collision is low.

### How to read the code

I have `RabinKarp(text, M,q)` function, where `text` is the string where we search patterns, `M` is the length we are looking for and `q` is prime modulo for Rabin-Karp algorighm.

1. First, we need to choose `d`, I chose `d = 256`, because it is more than `ord(z)`.
2. Then we need to evaluate auxiliary value `h`, we need it for fast update of rolling hash.
3. Evalute hash for first window
4. Evaluate hashes for all other windows in `O(n)` (that is `O(1)` for each window), using evaluated `h`. 
5. We keep all hashes in **dictionary**: for each hash we keep start indexes of windows.
6. Finally, we iterate over our dictionary and for each unique hash we check all possible combinations and compare not hashes, but original windows to make sure that it was not a collision.

Now, to the `longestDupSubstring(S)` function we run binary search, where we run our `RabinKarp` funcion at most `log n` times.

```
class Solution:
    def RabinKarp(self,text, M, q):
        if M == 0: return True
        h, t, d = (1<<(8*M-8))%q, 0, 256

        dic = defaultdict(list)

        for i in range(M): 
            t = (d * t + ord(text[i]))% q

        dic[t].append(i-M+1)

        for i in range(len(text) - M):
            t = (d*(t-ord(text[i])*h) + ord(text[i + M]))% q
            for j in dic[t]:
                if text[i+1:i+M+1] == text[j:j+M]:
                    return (True, text[j:j+M])
            dic[t].append(i+1)
        return (False, "")

    def longestDupSubstring(self, S):
        beg, end = 0, len(S)
        q = (1<<31) - 1 
        Found = ""
        while beg + 1 < end:
            mid = (beg + end)//2
            isFound, candidate = self.RabinKarp(S, mid, q)
            if isFound:
                beg, Found = mid, candidate
            else:
                end = mid

        return Found
```

**Further discussion** I did some research in this domain, and there are different ways to handle this problem:
1. You can solve it, using **suffix array** in `O(n log n)` (or even `O(n)` with **suffix trees**) complexity, see https://cp-algorithms.com/string/suffix-array.html for suffix array which is a bit easier than suffix trees https://en.wikipedia.org/wiki/Suffix_tree
2. You can solve it with **Tries**, complexity To be checked.
3. I think there is solution where we use KMP, but I have not seen one yet.
4. If you have more ideas, plese let me know!


If you have any questions, feel free to ask. If you like solution and explanations, please **Upvote!**
</p>


### C++ short O(n log(n)) solution with `std::unordered_set<std::string_view>`
- Author: denizevrenci
- Creation Date: Fri Jun 19 2020 19:09:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 20 2020 22:30:31 GMT+0800 (Singapore Standard Time)

<p>
Rabin-Karp algorihm can be implemented in C++17 with `std::unordered_set<std::string_view>` if a custom hasher is supplied. Check the second code block below for the custom hasher.
`std::string_view` does not copy the whole string and `std::unordered_set` handles the logic of hashing and comparing `std::string_view`\'s.

```cpp
class Solution {
public:
    string longestDupSubstring(string S)
    {
        std::string_view longest;
        std::unordered_set<std::string_view> set;
        size_t beg = 1;
        size_t end = S.size() - 1;
        while (beg <= end)
        {
            auto len = beg + (end - beg) / 2;
            bool found = false;
            for (size_t i = 0; i != S.size() - len + 1; ++i)
            {
                const auto [it, inserted] = set.emplace(S.data() + i, len);
                if (!inserted)
                {
                    found = true;
                    longest = *it;
                    break;   
                }
            }
            
            if (found)
                beg = len + 1;
            else
                end = len - 1;
            
            set.clear();      
        }
            
        return {longest.begin(), longest.end()};
    }
};
```

Edit: As @shortenda, @jatinmittal199510 and @Jabro have pointed out, the answer above uses a naive hash algorithm which takes O(len) time per each slice. With minimal change to the function, we can add a hash function that keeps track of the first character of the previously hashed string slice and the previous hash value. This answer runs in around 1000ms and beats about 98% of all C++ answers.

```
class Solution {
    class rabinFingerprint
    {
    public:
        size_t operator()(const std::string_view& s)
        {
    	    if (m_clear)
            {
                m_pow = 1;
                for (size_t i = 1; i != s.size(); ++i)
                    m_pow = (m_pow * base) % p;
                m_cur = 0;
                for (auto c : s)
                    m_cur = ((m_cur * base) % p + (c - \'a\')) % p;
                m_clear = false;
            }
            else
            {
                m_cur = ((ssize_t(m_cur) - ssize_t(m_pow * (m_first - \'a\'))) % ssize_t(p) + p) % p;
                m_cur = (m_cur * base + (s.back() - \'a\')) % p;
            }
            m_first = s.front();
            return m_cur;
        };
        
        void clear() { m_clear = true; }

    private:
        static constexpr size_t p = 19260817;
        static constexpr size_t base = 26;

        bool m_clear = true;
        size_t m_cur;
        size_t m_pow;
        char m_first;
    };
    
    struct wrapper
    {
        size_t operator()(const std::string_view& s) const
        {
            return m_hasher(s);
        }
        
        rabinFingerprint& m_hasher;  
    };

public:    
    string longestDupSubstring(string S)
    {
        std::string_view longest;
        rabinFingerprint hasher;
        std::unordered_set<std::string_view, wrapper> set{10, wrapper{hasher}};
        size_t beg = 1;
        size_t end = S.size() - 1;
        while (beg <= end)
        {
            auto len = beg + (end - beg) / 2;
            bool found = false;
            for (size_t i = 0; i != S.size() - len + 1; ++i)
            {
                const auto [it, inserted] = set.emplace(S.data() + i, len);
                if (!inserted)
                {
                    found = true;
                    longest = *it;
                    break;
                }
            }
            
            if (found)
                beg = len + 1;
            else
                end = len - 1;
            
            set.clear();
            hasher.clear();
        }
            
        return {longest.begin(), longest.end()};
    }
};
```
</p>


