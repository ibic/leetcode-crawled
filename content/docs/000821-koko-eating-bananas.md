---
title: "Koko Eating Bananas"
weight: 821
#id: "koko-eating-bananas"
---
## Description
<div class="description">
<p>Koko loves to eat bananas.&nbsp; There are <code>N</code>&nbsp;piles of bananas, the <code>i</code>-th&nbsp;pile has <code>piles[i]</code> bananas.&nbsp; The guards have gone and will come back in <code>H</code> hours.</p>

<p>Koko can decide her bananas-per-hour eating speed of <code>K</code>.&nbsp; Each hour, she chooses some pile of bananas, and eats K bananas from that pile.&nbsp; If the pile has less than <code>K</code> bananas, she eats all of them instead, and won&#39;t eat any more bananas during this hour.</p>

<p>Koko likes to eat slowly, but still wants to finish eating all the bananas before the guards come back.</p>

<p>Return the minimum integer <code>K</code> such that she can eat all the bananas within <code>H</code> hours.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> piles = [3,6,7,11], H = 8
<strong>Output:</strong> 4
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> piles = [30,11,23,4,20], H = 5
<strong>Output:</strong> 30
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> piles = [30,11,23,4,20], H = 6
<strong>Output:</strong> 23
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= piles.length &lt;= 10^4</code></li>
	<li><code>piles.length &lt;= H &lt;= 10^9</code></li>
	<li><code>1 &lt;= piles[i] &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Binary Search (binary-search)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Airbnb - 8 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Binary Search

**Intuition**

If Koko can finish eating all the bananas (within `H` hours) with an eating speed of `K`, she can finish with a larger speed too.

If we let `possible(K)` be `true` if and only if Koko can finish with an eating speed of `K`, then there is some `X` such that `possible(K) = True` if and only if `K >= X`.

For example, with `piles = [3, 6, 7, 11]` and `H = 8`, there is some `X = 4` so that `possible(1) = possible(2) = possible(3) = False`, and `possible(4) = possible(5) = ... = True`.

**Algorithm**

We can binary search on the values of `possible(K)` to find the first `X` such that `possible(X)` is `True`: that will be our answer.  Our loop invariant will be that `possible(hi)` is always `True`, and `lo` is always less than or equal to the answer.  For more information on binary search, please visit [[LeetCode Explore - Binary Search]](https://leetcode.com/explore/learn/card/binary-search/).

To find the value of `possible(K)`, (ie. whether `Koko` with an eating speed of `K` can eat all bananas in `H` hours), we simulate it.  For each pile of size `p > 0`, we can deduce that Koko finishes it in `Math.ceil(p / K) = ((p-1) // K) + 1` hours, and we add these times across all piles and compare it to `H`.


<iframe src="https://leetcode.com/playground/zbiqQutZ/shared" frameBorder="0" width="100%" height="446" name="zbiqQutZ"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log W)$$, where $$N$$ is the number of piles, and $$W$$ is the maximum size of a pile.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Binary Search Java / Python with Explanations
- Author: GraceMeng
- Creation Date: Sun Jul 22 2018 17:19:58 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 10:12:38 GMT+0800 (Singapore Standard Time)

<p>
Each hour, Koko chooses some pile of bananas, and eats `K` bananas from that pile. 
There is a limited range of `K`\'s to enable her to eat all the bananas within `H` hours. 
We ought to reduce the searching space and to return the minimum valid `K`. 
**Binary Search** is born for that.
Initially, we know that K belongs to [1, the largest element in `piles[]`]. And we follow the pattern of lower-bound Binary Search except that `if (K == target)` is replaced with `if (canEatAll(piles, K, H))`.
****
**Java**
```
    public int minEatingSpeed(int[] piles, int H) {
        int lo = 1, hi = getMaxPile(piles);
        
        // Binary search to find the smallest valid K.
        while (lo <= hi) {
            int K = lo + ((hi - lo) >> 1);
            if (canEatAll(piles, K, H)) {
                hi = K - 1;
            } else {
                lo = K + 1;
            }
        }
        
        return lo;
    }
    
    private boolean canEatAll(int[] piles, int K, int H) {
        int countHour = 0; // Hours take to eat all bananas at speed K.
        
        for (int pile : piles) {
            countHour += pile / K;
            if (pile % K != 0)
                countHour++;
        }
        return countHour <= H;
    }
    
    private int getMaxPile(int[] piles) {
        int maxPile = Integer.MIN_VALUE;
        for (int pile : piles) {
            maxPile = Math.max(pile, maxPile);
        }
        return maxPile;
    }
```
**Python**
```
    def minEatingSpeed(self, piles, H):
        lo, hi = 1, max(piles)
        
        while lo <= hi:
            K = lo + ((hi - lo) >> 1)
            if self.countTimeEatAllAtSpeed(
                    K, piles) <= H:  # count time to eat all bananas at speed K
                hi = K - 1
            else:
                lo = K + 1
        return lo
    
    def countTimeEatAllAtSpeed(self, K, piles):
        countHours = 0  # hours take to eat all bananas
        
        for pile in piles:
            countHours += pile / K
            if pile % K != 0:
                countHours += 1
        return countHours
```
**(\u4EBA \u2022\u0348\u1D17\u2022\u0348)** Thanks for voting!
</p>


### [C++/Java/Python] Binary Search
- Author: lee215
- Creation Date: Sun Jul 22 2018 11:05:26 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 24 2020 14:22:23 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
Binary search between `[1, 10^9]` or `[1, max(piles)]` to find the result.
Time complexity: `O(NlogM)`

`(p + m - 1) / m` equal to `ceil(p / m)` (just personal behavior)

Here you find another similar problem.
[774. Minimize Max Distance to Gas Station](https://leetcode.com/problems/minimize-max-distance-to-gas-station/discuss/113633/C++JavaPython-Binary-Search)
<br>

# Complexity
Time `O(Nlog(MaxP))`
Space `O(1)`
<br>

**C++:**
```cpp
    int minEatingSpeed(vector<int>& piles, int H) {
        int l = 1, r = 1000000000;
        while (l < r) {
            int m = (l + r) / 2, total = 0;
            for (int p : piles) total += (p + m - 1) / m;
            if (total > H) l = m + 1; else r = m;
        }
        return l;
    }
```

**Java:**
```java
    public int minEatingSpeed(int[] piles, int H) {
        int l = 1, r = 1000000000;
        while (l < r) {
            int m = (l + r) / 2, total = 0;
            for (int p : piles) total += (p + m - 1) / m;
            if (total > H) l = m + 1; else r = m;
        }
        return l;
    }
```
**Python:**
```py
    def minEatingSpeed(self, piles, H):
        l, r = 1, max(piles)
        while l < r:
            m = (l + r) / 2
            if sum((p + m - 1) / m for p in piles) > H: l = m + 1
            else: r = m
        return l
```
<br>

# More Good Binary Search Problems
Here are some similar binary search problems.
Also find more explanations.
Good luck and have fun.

- 1283. [Find the Smallest Divisor Given a Threshold](https://leetcode.com/problems/find-the-smallest-divisor-given-a-threshold/discuss/446376/javacpython-bianry-search/401806)
- 1231. [Divide Chocolate](https://leetcode.com/problems/divide-chocolate/discuss/408503/Python-Binary-Search)
- 1011. [Capacity To Ship Packages In N Days](https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/discuss/256729/javacpython-binary-search/351188?page=3)
- 875. [Koko Eating Bananas](https://leetcode.com/problems/koko-eating-bananas/discuss/152324/C++JavaPython-Binary-Search)
- 774. [Minimize Max Distance to Gas Station](https://leetcode.com/problems/minimize-max-distance-to-gas-station/discuss/113633/Easy-and-Concise-Solution-using-Binary-Search-C++JavaPython)
- 410. [Split Array Largest Sum](https://leetcode.com/problems/split-array-largest-sum/)
<br>
</p>


### Highly underrated question in Binary Search topic
- Author: baby_groot
- Creation Date: Thu Jan 17 2019 12:29:12 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 17 2019 12:29:12 GMT+0800 (Singapore Standard Time)

<p>
Do you agree ?
</p>


