---
title: "Thousand Separator"
weight: 1407
#id: "thousand-separator"
---
## Description
<div class="description">
<p>Given an&nbsp;integer <code>n</code>, add a dot (&quot;.&quot;)&nbsp;as the thousands separator and return it in&nbsp;string format.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 987
<strong>Output:</strong> &quot;987&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 1234
<strong>Output:</strong> &quot;1.234&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 123456789
<strong>Output:</strong> &quot;123.456.789&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 0
<strong>Output:</strong> &quot;0&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= n &lt; 2^31</code></li>
</ul>

</div>

## Tags
- String (string)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ Iterative
- Author: votrubac
- Creation Date: Sun Aug 23 2020 00:02:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 23 2020 00:02:06 GMT+0800 (Singapore Standard Time)

<p>
```cpp
string thousandSeparator(int n) {
    string s = to_string(n), res;
    for (int i = 0; i < s.size(); ++i) {
        if (i > 0 && (s.size() - i) % 3 == 0)
            res += ".";
        res += s[i];
    }
    return res;
}
```
</p>


### 2 Solution Brute Force | Bit Optimal
- Author: karprabin1
- Creation Date: Sun Aug 23 2020 01:16:12 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 25 2020 00:02:05 GMT+0800 (Singapore Standard Time)

<p>
**Detailed Video Explanation** https://www.youtube.com/watch?v=ehqOTZLBua8

**Modulo with 3 Approach**

```
class Solution {
    public String thousandSeparator(int n) {
        if(n < 1000) 
            return "" + n;
        String s = "" + n;
        int len = ("" + n).length();
        int i = len % 3;
        StringBuilder sb = new StringBuilder(s.substring(0, i));
        while(i < len) {
            if(i > 0)
                sb.append(".");
            sb.append(s.substring(i, Math.min(i + 3, len)));
            i += 3;
        }
        return sb.toString();
    }
}
```

********
**Divide and Reminder Approach**

```
class Solution {
    public String thousandSeparator(int n) {
        if(n < 1000) return "" + n;
        StringBuilder sb = new StringBuilder();
        
        int count = 0;
        while(n > 0)  {
            if(count == 3) {
                sb.append(".");
                count = 0;
            }
            sb.append(n % 10);
            n /= 10;
            count++;
        }
        
        return sb.reverse().toString();
    }
}
```

If any doubt ask in comments. If you like solution **upvote**.
</p>


### Python 3 by reversing
- Author: srushti22
- Creation Date: Sun Aug 23 2020 01:18:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 30 2020 13:45:19 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def thousandSeparator(self, n: int) -> str:
        s=str(n)
        s=s[::-1]
        res = \'.\'.join(s[i:i + 3] for i in range(0, len(s), 3))
        return res[::-1]
```
</p>


