---
title: "Maximum Score Words Formed by Letters"
weight: 1199
#id: "maximum-score-words-formed-by-letters"
---
## Description
<div class="description">
<p>Given a list of <code>words</code>, list of&nbsp; single&nbsp;<code>letters</code> (might be repeating)&nbsp;and <code>score</code>&nbsp;of every character.</p>

<p>Return the maximum score of <strong>any</strong> valid set of words formed by using the given letters (<code>words[i]</code> cannot be used two&nbsp;or more times).</p>

<p>It is not necessary to use all characters in <code>letters</code> and each letter can only be used once. Score of letters&nbsp;<code>&#39;a&#39;</code>, <code>&#39;b&#39;</code>, <code>&#39;c&#39;</code>, ... ,<code>&#39;z&#39;</code> is given by&nbsp;<code>score[0]</code>, <code>score[1]</code>, ... , <code>score[25]</code> respectively.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> words = [&quot;dog&quot;,&quot;cat&quot;,&quot;dad&quot;,&quot;good&quot;], letters = [&quot;a&quot;,&quot;a&quot;,&quot;c&quot;,&quot;d&quot;,&quot;d&quot;,&quot;d&quot;,&quot;g&quot;,&quot;o&quot;,&quot;o&quot;], score = [1,0,9,5,0,0,3,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0]
<strong>Output:</strong> 23
<strong>Explanation:</strong>
Score  a=1, c=9, d=5, g=3, o=2
Given letters, we can form the words &quot;dad&quot; (5+1+5) and &quot;good&quot; (3+2+2+5) with a score of 23.
Words &quot;dad&quot; and &quot;dog&quot; only get a score of 21.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> words = [&quot;xxxz&quot;,&quot;ax&quot;,&quot;bx&quot;,&quot;cx&quot;], letters = [&quot;z&quot;,&quot;a&quot;,&quot;b&quot;,&quot;c&quot;,&quot;x&quot;,&quot;x&quot;,&quot;x&quot;], score = [4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,10]
<strong>Output:</strong> 27
<strong>Explanation:</strong>
Score  a=4, b=4, c=4, x=5, z=10
Given letters, we can form the words &quot;ax&quot; (4+5), &quot;bx&quot; (4+5) and &quot;cx&quot; (4+5) with a score of 27.
Word &quot;xxxz&quot; only get a score of 25.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> words = [&quot;leetcode&quot;], letters = [&quot;l&quot;,&quot;e&quot;,&quot;t&quot;,&quot;c&quot;,&quot;o&quot;,&quot;d&quot;], score = [0,0,1,1,1,0,0,0,0,0,0,1,0,0,1,0,0,0,0,1,0,0,0,0,0,0]
<strong>Output:</strong> 0
<strong>Explanation:</strong>
Letter &quot;e&quot; can only be used once.</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= words.length &lt;= 14</code></li>
	<li><code>1 &lt;= words[i].length &lt;= 15</code></li>
	<li><code>1 &lt;= letters.length &lt;= 100</code></li>
	<li><code>letters[i].length == 1</code></li>
	<li><code>score.length ==&nbsp;26</code></li>
	<li><code>0 &lt;= score[i] &lt;= 10</code></li>
	<li><code>words[i]</code>, <code>letters[i]</code>&nbsp;contains only lower case English letters.</li>
</ul>

</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Google - 2 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### java backtrack, similar to 78. Subsets, 1ms beats 100%
- Author: dreamyjpl
- Creation Date: Sun Nov 10 2019 12:04:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 10 2019 12:42:14 GMT+0800 (Singapore Standard Time)

<p>
Similar to 78. Subsets, use backtrack to solve the problem.
```java
class Solution {
    public int maxScoreWords(String[] words, char[] letters, int[] score) {
        if (words == null || words.length == 0 || letters == null || letters.length == 0 || score == null || score.length == 0) return 0;
        int[] count = new int[score.length];
        for (char ch : letters) {
            count[ch - \'a\']++;
        }
        int res = backtrack(words, count, score, 0);
        return res;
    }
    int backtrack(String[] words, int[] count, int[] score, int index) {
        int max = 0;
        for (int i = index; i < words.length; i++) {
            int res = 0;
            boolean isValid = true;
            for (char ch : words[i].toCharArray()) {
                count[ch - \'a\']--;
                res += score[ch - \'a\'];
                if (count[ch - \'a\'] < 0) isValid = false;
            }
            if (isValid) {
                res += backtrack(words, count, score, i + 1);
                max = Math.max(res, max);
            }
            for (char ch : words[i].toCharArray()) {
                count[ch - \'a\']++;
                res = 0;
            }
        }
        return max;
    }
}
```
</p>


### Python DFS Pruning
- Author: WangQiuc
- Creation Date: Sun Nov 10 2019 16:12:49 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 03 2020 01:11:37 GMT+0800 (Singapore Standard Time)

<p>
This problem is about finding a combination that the combination of the `words` whose total letter consumptions are restricted by `letters` (covert to a character counter) that achieved the highest score.
So we can use DFS here to search all valid combinations and find out the highest score. To reduce the search space, we can prune the branch and backtrack when current branch is no longer valid or can\'t reach the current highest score.

Each time, we search from index `i` and all the rest node `words[i:]` are potential candidates to DFS.
Suppose in one of DFS branch, we pass `words[i:j]` and check the node `words[j]`. If `words[j]`\'s letter consumption doesn\'t exceed the current `letters` restriction, we are good to add its score and keep DFS from `j+1`.
We can check this by `if all(n <= letters.get(c,0) for c,n in Counter(words[j]).items())`
And when we DFS from `words[j]`, we also need to reduce `letters` resource by `words[j]`\'s letter consumption. Or we can pass a new counter argument to DFS as `new_letter = {c:n-Counter(words[j]).get(c,0) for c,n in letter.items()}`

To improve performance, we can precomputer each word\'s letter counter and its score:
```
words_score = [sum(score[ord(c)-ord(\'a\')] for c in word) for word in words]
words_counter = [collections.Counter(word) for word in words]
```
And we can also prune the branch once `curr_score + sum(words_score[i:]) <= self.max_score` as there is no possibility for current branch exceeding the maximum score so far. We are also precompute a suffix sum array to avoid calculating `sum(words_score[i:]` each time. But I save it here as the `words.length <= 15`.

```
class Solution():
    def maxScoreWords(self, words, letters, score):
        self.max_score = 0
        words_score = [sum(score[ord(c)-ord(\'a\')] for c in word) for word in words]
        words_counter = [collections.Counter(word) for word in words]
        
        def dfs(i, curr_score, counter):
            if curr_score + sum(words_score[i:]) <= self.max_score:
                return
            self.max_score = max(self.max_score, curr_score)
            for j, wcnt in enumerate(words_counter[i:], i):
                if all(n <= counter.get(c,0) for c,n in wcnt.items()):
                    dfs(j+1, curr_score+words_score[j], counter-wcnt)
        
        dfs(0, 0, collections.Counter(letters))
        return self.max_score
```
</p>


### Detailed Explanation using Recursion
- Author: Just__a__Visitor
- Creation Date: Sun Nov 10 2019 12:02:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 10 2019 13:09:44 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
* Since the number of words is quite less (14), we can just generate all the subsets. Let\'s call a subset valid if all the words in that subset can be formed by using the given letters. It is clear that we need to maximize the score of valid subsets. To do this, we generate all subsets, and check if it is valid. If it is, we find its score and update the global maxima.

* To generate the subsets, we create a function call `generate_subset(words, n)`  which generate all the subsets of the vector `words`.  To quickly recap, we have 2 choices for the last element, either to take it or to leave it. We store this choice in an array called `taken` where `taken[i]` represent that the `i-th` element was taken in our journey. We then recurse for the remaining elements.

* When `n` becomes zero, it means we cannot make any more choices. So now, we traverse our `taken` array to find out the elements in this subset. Then we count the frequency of each letter in this subset. If the frequency of each letter is under the provided limit, it means it is a valid subet. Hence, we find the score of this subset and update the maxima.

```cpp
class Solution
{
public:
    vector<bool> taken;
    vector<int> count;
    vector<int> score;
    int max_sum = 0;
    
    void update_score(vector<string>& words);
    void generate_subset(vector<string>& words, int n);
    int maxScoreWords(vector<string>& words, vector<char>& letters, vector<int>& score);
};

void Solution :: generate_subset(vector<string>& words, int n)
{
    if(n == 0)
    {
        update_score(words);
        return;
    }
    
    taken[n-1] = true;
    generate_subset(words, n-1);
    
    taken[n-1] = false;
    generate_subset(words, n-1);
}

void Solution :: update_score(vector<string>& words)
{
    int current_score = 0;
    vector<int> freq(26, 0);
    
    for(int i = 0; i < words.size(); i++)
    {
        if(taken[i])
        {
            for(auto ele : words[i])
            {
                int ind = ele - \'a\';
                current_score += score[ind];
                freq[ind]++;
                
                if(freq[ind] > count[ind])
                      return;
            }
        }
    }
                      
    max_sum = max(max_sum, current_score);
}
                      
int Solution :: maxScoreWords(vector<string>& words, vector<char>& letters, vector<int>& score)
{
    taken.resize(words.size(), false);
    count.resize(26, 0);
    this->score = score;
    
    for(auto ele : letters)
        count[ele - \'a\']++;
    
    int n = words.size();
    generate_subset(words, n);
    
    return max_sum;
}
```
</p>


