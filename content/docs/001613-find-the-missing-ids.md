---
title: "Find the Missing IDs"
weight: 1613
#id: "find-the-missing-ids"
---
## Description
<div class="description">
<p>Table: <code>Customers</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| customer_id   | int     |
| customer_name | varchar |
+---------------+---------+
customer_id is the primary key for this table.
Each row of this table contains the name and the id customer.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the missing customer IDs. The missing IDs are ones that are not in the <code>Customers</code> table but are in the range between <code>1</code> and the <strong>maximum</strong> <code>customer_id</code> present in the table.</p>

<p><strong>Notice</strong> that the maximum <code>customer_id</code> will not exceed <code>100</code>.</p>

<p>Return the result table ordered by <code>ids</code> in <strong>ascending order</strong>.</p>

<p>The query result format is in the following example.</p>

<p>&nbsp;</p>

<pre>
<code>Customer</code> table:
+-------------+---------------+
| customer_id | customer_name |
+-------------+---------------+
| 1           | Alice         |
| 4           | Bob           |
| 5           | Charlie       |
+-------------+---------------+

Result table:
+-----+
| <code>ids </code>|
+-----+
| 2   |
| 3   |
+-----+
The maximum customer_id present in the table is 5, so in the range [1,5], IDs 2 and 3 are missing from the table.</pre>

</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Use cte to generate consecutive id range + NOT IN
- Author: vwang0
- Creation Date: Sun Oct 11 2020 12:19:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 15:02:07 GMT+0800 (Singapore Standard Time)

<p>

```
WITH RECURSIVE id_seq AS (
    SELECT 1 as continued_id
    UNION ALL
    SELECT continued_id + 1
    FROM id_seq
    WHERE continued_id < (SELECT MAX(customer_id) FROM Customers) 
)

SELECT continued_id AS ids
FROM id_seq
WHERE continued_id NOT IN (SELECT customer_id FROM Customers)  



```
</p>


### faster than 100% mysql solution with cte
- Author: user3762K
- Creation Date: Sun Oct 11 2020 11:57:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 11:57:42 GMT+0800 (Singapore Standard Time)

<p>
```
with recursive Numbers as (
    select 1 as Number
    union all
    select Number + 1
    from Numbers
    where Number < (select max(customer_id) from customers) 
)
select Number ids
from Numbers
where Number not in (select customer_id from customers)
```
</p>


### Simple Solution for SQL Server, My SQL, Oracle and PostgreSQL
- Author: mirandanathan
- Creation Date: Sun Oct 11 2020 08:27:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 08:39:18 GMT+0800 (Singapore Standard Time)

<p>
SQL Server or Azure solution is as below:

```
WITH CTE AS (SELECT MAX(customer_id) AS MAX_ID FROM Customers),

CTE1 AS
(
    SELECT 1 AS ids
    UNION ALL
    SELECT ids + 1 FROM CTE1 WHERE ids < (SELECT MAX_ID FROM CTE)
)

SELECT ids
FROM CTE1
WHERE ids NOT IN (SELECT customer_id FROM Customers)
ORDER BY 1 ASC
```

MySQL and PostgreSQL are much easier

```
WITH RECURSIVE CTE (ids) AS
(
    SELECT 1 AS ids
    UNION ALL
    SELECT ids + 1 FROM CTE WHERE ids < (SELECT MAX(customer_id) FROM Customers)
)

SELECT ids
FROM CTE
WHERE ids NOT IN (SELECT customer_id FROM Customers)
ORDER BY 1 ASC
```

Oracle PL/SQL
```
SELECT level "ids"
FROM dual
CONNECT by level <= (SELECT MAX(customer_id) FROM customers)
MINUS
SELECT customer_id
FROM customers
ORDER by 1
```
</p>


