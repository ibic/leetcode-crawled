---
title: "Special Array With X Elements Greater Than or Equal X"
weight: 1463
#id: "special-array-with-x-elements-greater-than-or-equal-x"
---
## Description
<div class="description">
<p>You are given an array <code>nums</code> of non-negative integers. <code>nums</code> is considered <strong>special</strong> if there exists a number <code>x</code> such that there are <strong>exactly</strong> <code>x</code> numbers in <code>nums</code> that are <strong>greater than or equal to</strong> <code>x</code>.</p>

<p>Notice that <code>x</code> <strong>does not</strong> have to be an element in <code>nums</code>.</p>

<p>Return <code>x</code> <em>if the array is <strong>special</strong>, otherwise, return </em><code>-1</code>. It can be proven that if <code>nums</code> is special, the value for <code>x</code> is <strong>unique</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,5]
<strong>Output:</strong> 2
<strong>Explanation:</strong> There are 2 values (3 and 5) that are greater than or equal to 2.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,0]
<strong>Output:</strong> -1
<strong>Explanation:</strong> No numbers fit the criteria for x.
If x = 0, there should be 0 numbers &gt;= x, but there are 2.
If x = 1, there should be 1 number &gt;= x, but there are 0.
If x = 2, there should be 2 numbers &gt;= x, but there are 0.
x cannot be greater since there are only 2 numbers in nums.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,4,3,0,4]
<strong>Output:</strong> 3
<strong>Explanation:</strong> There are 3 values that are greater than or equal to 3.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,6,7,7,0]
<strong>Output:</strong> -1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 100</code></li>
	<li><code>0 &lt;= nums[i] &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Clean Python 3, O(sort)
- Author: lenchen1112
- Creation Date: Sun Oct 04 2020 12:02:07 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 05 2020 19:26:31 GMT+0800 (Singapore Standard Time)

<p>
Concept is similar to H-index
After `while` loop, we can get `i` which indicates there are already `i` items larger or equal to `i`.
Meanwhile, if we found `i == nums[i]`, there will be `i + 1` items larger or equal to `i`, which makes array not special.
Time: `O(sort)`, can achieve `O(N)` if we use counting sort
Space: `O(sort)`
```
class Solution:
    def specialArray(self, nums: List[int]) -> int:
        nums.sort(reverse=True)
        i = 0
        while i < len(nums) and nums[i] > i:
            i += 1
        return -1 if i < len(nums) and i == nums[i] else i
```

it can be faster by using binary search if list is already sorted.
```
class Solution:
    def specialArray(self, nums: List[int]) -> int:
        nums.sort(reverse=True)
        left, right = 0, len(nums)
        while left < right:
            mid = left + (right - left) // 2
            if mid < nums[mid]:
                left = mid + 1
            else:
                right = mid       
        return -1 if left < len(nums) and left == nums[left] else left
```
</p>


### Java O(nlogn) with easy explanation
- Author: renato4
- Creation Date: Sun Oct 04 2020 12:27:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 12:31:41 GMT+0800 (Singapore Standard Time)

<p>
**Explanation**: The number we are searching for is any number from `zero` to `length of array`. If we sort the array in ascending order, we can iterate through it from left to right checking if the number of elements until the end and the current value meet the conditions of success. Those are:
1. The number of elements from the current index until the end is less than or equal the current value in the array
2. The number of elements from the current index until the end is greater than the previous value in the array (if it exists)

We are, essentially, cleverly checking if the current number of elements until the end is the answer.

```
class Solution {
    public int specialArray(int[] nums) {
        Arrays.sort(nums);
        for(int i = 0; i < nums.length; i++) {
            int n = nums.length-i;
            boolean cond1 =  n<=nums[i];
            boolean cond2 = (i-1<0) || (n>nums[i-1]);
            if (cond1 && cond2) return n;
        }
        return -1;
    }

}
```
</p>


### c++ without sorting 0ms
- Author: escrowdis
- Creation Date: Sun Oct 04 2020 12:02:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 13:08:16 GMT+0800 (Singapore Standard Time)

<p>
1. count the amount of numbers
2. add the amount backward so we can know how many numbers are no less than the current index.

Example:
```
input [0, 4, 3, 0, 4]

index  0  1  2  3  4  0  ... 1000 // the range defined in the question
v     [2, 0, 0, 1, 2, 0, ..., 0]
# after accumulation
v     [5, 3, 3, 3, 2, 0, ..., 0]
                ^ // 3 numbers are no less than 3
```


Implementation:
```
class Solution {
public:
    int specialArray(vector<int>& nums) {
        int v[102];
        memset(v, 0, sizeof v);
        for (const auto &n : nums) {
            ++v[n > 100 ? 100 : n];
        }
        for (int i = 100; i > 0; --i) {
            v[i] = v[i + 1] + v[i];
            if (v[i] == i)
                return i;
        }
        return -1;
    }
};
```

[Update 1004]: change size of vector from ~1000 to ~100 based on the constraint that `1 <= nums.length <= 100`.

</p>


