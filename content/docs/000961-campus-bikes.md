---
title: "Campus Bikes"
weight: 961
#id: "campus-bikes"
---
## Description
<div class="description">
<p>On a campus represented as a 2D grid, there are <code>N</code> workers and <code>M</code> bikes, with <code>N &lt;= M</code>. Each worker and bike is a 2D coordinate on this grid.</p>

<p>Our goal is to assign a bike to each worker. Among the available bikes and workers, we choose the (worker, bike) pair with the shortest Manhattan distance between each other, and assign the bike to that worker. (If there are multiple (worker, bike) pairs with the same shortest Manhattan distance, we choose the pair with the smallest worker index; if there are multiple ways to do that, we choose the pair with the smallest bike index). We repeat this process until there are no available workers.</p>

<p>The Manhattan distance between two points <code>p1</code> and <code>p2</code> is <code>Manhattan(p1, p2) = |p1.x - p2.x| + |p1.y - p2.y|</code>.</p>

<p>Return a vector <code>ans</code> of length <code>N</code>, where <code>ans[i]</code> is the index (0-indexed) of the bike that the <code>i</code>-th worker is assigned to.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/03/06/1261_example_1_v2.png" style="width: 264px; height: 264px;" /></p>

<pre>
<strong>Input: </strong>workers = <span id="example-input-1-1">[[0,0],[2,1]]</span>, bikes = <span id="example-input-1-2">[[1,2],[3,3]]</span>
<strong>Output: </strong><span id="example-output-1">[1,0]</span>
<strong>Explanation: </strong>
Worker 1 grabs Bike 0 as they are closest (without ties), and Worker 0 is assigned Bike 1. So the output is [1, 0].
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/03/06/1261_example_2_v2.png" style="width: 264px; height: 264px;" /></p>

<pre>
<strong>Input: </strong>workers = <span id="example-input-2-1">[[0,0],[1,1],[2,0]]</span>, bikes = <span id="example-input-2-2">[[1,0],[2,2],[2,1]]</span>
<strong>Output: </strong><span id="example-output-2">[0,2,1]</span>
<strong>Explanation: </strong>
Worker 0 grabs Bike 0 at first. Worker 1 and Worker 2 share the same distance to Bike 2, thus Worker 1 is assigned to Bike 2, and Worker 2 will take Bike 1. So the output is [0,2,1].
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= workers[i][j], bikes[i][j] &lt; 1000</code></li>
	<li>All worker and bike locations are distinct.</li>
	<li><code>1 &lt;= workers.length &lt;= bikes.length &lt;= 1000</code></li>
</ol>

</div>

## Tags
- Greedy (greedy)
- Sort (sort)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Google - 7 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ bucket sort O(M*N) time and space solution
- Author: mazytes
- Creation Date: Mon Jun 10 2019 02:50:38 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 10 2019 02:50:38 GMT+0800 (Singapore Standard Time)

<p>
Since the range of distance is [0, 2000] which is much lower than the # of pairs, which is 1e6. It\'s a good time to use bucket sort. Basically, it\'s to put each pair into the bucket representing its distance. Eventually, we can loop thru each bucket from lower distance.
Therefore, it\'s O(M * N) time and O(M * N) space. Only takes 60 ms in C++ beat most submissions.
See the code:
```
class Solution {
public:
    // bucket sort
    // O(N*M) time, O(N*M) space
    vector<int> assignBikes(vector<vector<int>>& workers, vector<vector<int>>& bikes) {
        // bucket sort since range of distance is [0, 2000]
        vector<vector<pair<int, int>>> buckets(2001); // buckets[i] is the vector<worker id, bike id> with distance i
        int n = workers.size(), m = bikes.size();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                int dis = abs(workers[i][0] - bikes[j][0]) + abs(workers[i][1] - bikes[j][1]);
                buckets[dis].push_back({i, j});
            }
        }
        vector<int> res(n, -1);
        vector<bool> bikeUsed(m, false);
        for (int d = 0; d <= 2000; d++) {
            for (int k = 0; k < buckets[d].size(); k++) {
                if (res[buckets[d][k].first] == -1 && !bikeUsed[buckets[d][k].second]) {
                    bikeUsed[buckets[d][k].second] = true;
                    res[buckets[d][k].first] = buckets[d][k].second;
                }
            }
        }
        return res;
    }
};
```
</p>


### Java Fully Explained
- Author: kiyayeh
- Creation Date: Wed Jun 05 2019 01:37:49 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 18 2019 11:24:30 GMT+0800 (Singapore Standard Time)

<p>
The solution is inspireed by @wushangzhen

As the question states, there are  ```n``` workers and ```m``` bikes, and ```m > n```.
We are able to solve this question using a greedy approach.

1. initiate a priority queue of bike and worker pairs. The heap order should be ```Distance ASC, WorkerIndex ASC, Bike ASC```
2. Loop through all workers and bikes, calculate their distance, and then throw it to the queue.
3. Initiate a set to keep track of the bikes that have been assigned.
4. initiate a result array and fill it with ```-1```. (unassigned)
5. poll every possible pair from the priority queue and check if the person already got his bike or the bike has been assigned.
6. early exist on every people got their bike.

This is my first post on LeetCode.
Let me know if you got any questions :D

```
   public int[] assignBikes(int[][] workers, int[][] bikes) {
        int n = workers.length;
        
        // order by Distance ASC, WorkerIndex ASC, BikeIndex ASC
        PriorityQueue<int[]> q = new PriorityQueue<int[]>((a, b) -> {
            int comp = Integer.compare(a[0], b[0]);
            if (comp == 0) {
                if (a[1] == b[1]) {
                    return Integer.compare(a[2], b[2]);
                }
                
                return Integer.compare(a[1], b[1]);
            }
            
            return comp;
        });
            
        // loop through every possible pairs of bikes and people,
        // calculate their distance, and then throw it to the pq.
        for (int i = 0; i < workers.length; i++) {
            
            int[] worker = workers[i];
            for (int j = 0; j < bikes.length; j++) {
                int[] bike = bikes[j];
                int dist = Math.abs(bike[0] - worker[0]) + Math.abs(bike[1] - worker[1]);
                q.add(new int[]{dist, i, j}); 
            }
        }
        
        // init the result array with state of \'unvisited\'.
        int[] res = new int[n];
        Arrays.fill(res, -1);
        
        // assign the bikes.
        Set<Integer> bikeAssigned = new HashSet<>();
        while (bikeAssigned.size() < n) {
            int[] workerAndBikePair = q.poll();
            if (res[workerAndBikePair[1]] == -1 
                && !bikeAssigned.contains(workerAndBikePair[2])) {   
                
                res[workerAndBikePair[1]] = workerAndBikePair[2];
                bikeAssigned.add(workerAndBikePair[2]);
            }
        }
        
        return res;
    }
```
</p>


### Python - heap of closest bike to each worker
- Author: yorkshire
- Creation Date: Sun Jun 02 2019 14:30:18 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 03 2019 07:21:09 GMT+0800 (Singapore Standard Time)

<p>
For each worker, create a sorted list of distances to each bike. The elements of the list are tuples (distance, worker, bike).
For each worker, add the tuple with the shortest distance to the heap.
Until each worker has a bike, pop the smallest distance from the heap.
If this bike is not used, update the result for this worker, else add the next closest tuple for this worker to the heap.
```
	def assignBikes(self, workers, bikes):
        distances = []     # distances[worker] is tuple of (distance, worker, bike) for each bike 
        for i, (x, y) in enumerate(workers):
            distances.append([])
            for j, (x_b, y_b) in enumerate(bikes):
                distance = abs(x - x_b) + abs(y - y_b)
                distances[-1].append((distance, i, j))
            distances[-1].sort(reverse = True)  # reverse so we can pop the smallest distance
        
        result = [None] * len(workers)
        used_bikes = set()
        queue = [distances[i].pop() for i in range(len(workers))]   # smallest distance for each worker
        heapq.heapify(queue)
        
        while len(used_bikes) < len(workers):
            _, worker, bike = heapq.heappop(queue)
            if bike not in used_bikes:
                result[worker] = bike
                used_bikes.add(bike)
            else:
                heapq.heappush(queue, distances[worker].pop())  # bike used, add next closest bike
        
        return result
</p>


