---
title: "Reshape the Matrix"
weight: 531
#id: "reshape-the-matrix"
---
## Description
<div class="description">
<p>In MATLAB, there is a very useful function called 'reshape', which can reshape a matrix into a new one with different size but keep its original data.
</p>

<p>
You're given a matrix represented by a two-dimensional array, and two <b>positive</b> integers <b>r</b> and <b>c</b> representing the <b>row</b> number and <b>column</b> number of the wanted reshaped matrix, respectively.</p>

 <p>The reshaped matrix need to be filled with all the elements of the original matrix in the same <b>row-traversing</b> order as they were.
</p>

<p>
If the 'reshape' operation with given parameters is possible and legal, output the new reshaped matrix; Otherwise, output the original matrix.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> 
nums = 
[[1,2],
 [3,4]]
r = 1, c = 4
<b>Output:</b> 
[[1,2,3,4]]
<b>Explanation:</b><br>The <b>row-traversing</b> of nums is [1,2,3,4]. The new reshaped matrix is a 1 * 4 matrix, fill it row by row by using the previous list.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> 
nums = 
[[1,2],
 [3,4]]
r = 2, c = 4
<b>Output:</b> 
[[1,2],
 [3,4]]
<b>Explanation:</b><br>There is no way to reshape a 2 * 2 matrix to a 2 * 4 matrix. So output the original matrix.
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The height and width of the given matrix is in range [1, 100].</li>
<li>The given r and c are all positive.</li>
</ol>
</p>
</div>

## Tags
- Array (array)

## Companies
- Mathworks - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Using Queue 

**Algorithm**

The simplest method is to extract all the elements of the given matrix by reading the elements in a row-wise fashion. In this implementation, we use a queue to put the extracted elements. Then, we can take out the elements of the queue formed in a serial order and arrange the elements in the resultant required matrix in a row-by-row order again.

The formation of the resultant matrix won't be possible if the number of elements in the original matrix isn't equal to the number of elements in the resultant matrix.


<iframe src="https://leetcode.com/playground/Tca3ZP5y/shared" frameBorder="0" width="100%" height="378" name="Tca3ZP5y"></iframe>

**Complexity Analysis**

* Time complexity : $$O(m \cdot n)$$. We traverse over $$m \cdot n$$ elements twice. Here, $$m$$ and $$n$$ refer to the number of rows and columns of the given matrix respectively.

* Space complexity : $$O(m \cdot n)$$. The queue formed will be of size $$m \cdot n$$.
<br />
<br />


---
#### Approach 2: Without Using Extra Space 

**Algorithm**

Instead of unnecessarily using the queue as in the brute force approach, we can keep putting the numbers in the resultant matrix directly while iterating over the given matrix in a row-by-row order. While putting the numbers in the resultant array, we fix a particular row and keep on incrementing the column numbers only till we reach the end of the required columns indicated by $$c$$. At this moment, we update the row index by incrementing it and reset the column index to start from 0 again. Thus, we can save the space consumed by the queue for storing the data that just needs to be copied into a new array.

<iframe src="https://leetcode.com/playground/7oC32KQR/shared" frameBorder="0" width="100%" height="378" name="7oC32KQR"></iframe>

**Complexity Analysis**

* Time complexity : $$O(m \cdot n)$$. We traverse the entire matrix of size $$m \cdot n$$ once only. Here, $$m$$ and $$n$$ refers to the number of rows and columns in the given matrix.

* Space complexity : $$O(m \cdot n)$$. The resultant matrix of size $$m \cdot n$$ is used. 
<br />
<br />


---
#### Approach 3: Using division and modulus

**Algorithm**

In the last approach, we needed to keep a track of when we reached the end of columns for the resultant matrix and needed to update the current row and column number for putting the extracted elements by checking the current indices every time. Instead of doing these limit checks at every step, we can make use of maths to help ease the situation. 

The idea behind this approach is as follows. Do you know how a 2-D array is stored in the main memory(which is 1-D  in nature)? It is internally represented as a 1-D array only. The element $$nums[i][j]$$ of $$nums$$ array is represented in the form of a one dimensional array by using the index in the form: $$nums[n*i + j]$$, where $$m$$ is the number of columns in the given matrix. Looking at the same in the reverse order, while putting the elements in the elements in the resultant matrix, we can make use of a $$count$$ variable which gets incremented for every element traversed as if we are putting the elements in a 1-D resultant array. But, to convert the $$count$$ back into 2-D matrix indices with a column count of $$c$$, we can obtain the indices as $$res[count/c][count\%c]$$ where $$count/c$$ is the row number and $$count\%c$$ is the coloumn number. Thus, we can save the extra checking required at each step.

<iframe src="https://leetcode.com/playground/gGCnaCia/shared" frameBorder="0" width="100%" height="310" name="gGCnaCia"></iframe>

**Complexity Analysis**

* Time complexity : $$O(m \cdot n)$$. We traverse the entire matrix of size $$m \cdot n$$ once only. Here, $$m$$ and $$n$$ refers to the number of rows and columns in the given matrix.

* Space complexity : $$O(m \cdot n)$$. The resultant matrix of size $$m \cdot n$$ is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Concise O(nm) time
- Author: compton_scatter
- Creation Date: Sun Apr 30 2017 11:04:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 02:17:30 GMT+0800 (Singapore Standard Time)

<p>
```
public int[][] matrixReshape(int[][] nums, int r, int c) {
    int n = nums.length, m = nums[0].length;
    if (r*c != n*m) return nums;
    int[][] res = new int[r][c];
    for (int i=0;i<r*c;i++) 
        res[i/c][i%c] = nums[i/m][i%m];
    return res;
}
```
</p>


### One loop
- Author: StefanPochmann
- Creation Date: Sun Apr 30 2017 15:11:21 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 26 2018 12:54:59 GMT+0800 (Singapore Standard Time)

<p>
We can use `matrix[index / width][index % width]` for both the input and the output matrix.

    public int[][] matrixReshape(int[][] nums, int r, int c) {
        int m = nums.length, n = nums[0].length;
        if (r * c != m * n)
            return nums;
        int[][] reshaped = new int[r][c];
        for (int i = 0; i < r * c; i++)
            reshaped[i/c][i%c] = nums[i/n][i%n];
        return reshaped;
    }
</p>


### Python Solutions
- Author: StefanPochmann
- Creation Date: Sun Apr 30 2017 15:01:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 30 2017 15:01:29 GMT+0800 (Singapore Standard Time)

<p>
#### **Solution 1 - `NumPy`**


When I read "MATLAB", I immediately thought "NumPy". Thanks to @fallcreek for pointing out `tolist`, makes converting the result to the correct type easier than what I had originally.
```
import numpy as np

class Solution(object):
    def matrixReshape(self, nums, r, c):
        try:
            return np.reshape(nums, (r, c)).tolist()
        except:
            return nums
```
#### **Solution 2 - Oneliner**

An ugly oneliner :-)

    def matrixReshape(self, nums, r, c):
        return nums if len(sum(nums, [])) != r * c else map(list, zip(*([iter(sum(nums, []))]*c)))

A more readable version of that:

    def matrixReshape(self, nums, r, c):
        flat = sum(nums, [])
        if len(flat) != r * c:
            return nums
        tuples = zip(*([iter(flat)] * c))
        return map(list, tuples)

#### **Solution 3 - `itertools`**

    def matrixReshape(self, nums, r, c):
        if r * c != len(nums) * len(nums[0]):
            return nums
        it = itertools.chain(*nums)
        return [list(itertools.islice(it, c)) for _ in xrange(r)]
</p>


