---
title: "Delete Columns to Make Sorted III"
weight: 910
#id: "delete-columns-to-make-sorted-iii"
---
## Description
<div class="description">
<p>We are given an array&nbsp;<code>A</code> of <code>N</code> lowercase letter strings, all of the same length.</p>

<p>Now, we may choose any set of deletion indices, and for each string, we delete all the characters in those indices.</p>

<p>For example, if we have an array <code>A = [&quot;babca&quot;,&quot;bbazb&quot;]</code> and deletion indices <code>{0, 1, 4}</code>, then the final array after deletions is <code>[&quot;bc&quot;,&quot;az&quot;]</code>.</p>

<p>Suppose we chose a set of deletion indices <code>D</code> such that after deletions, the final array has <strong>every element (row) in&nbsp;lexicographic</strong> order.</p>

<p>For clarity, <code>A[0]</code> is in lexicographic order (ie. <code>A[0][0] &lt;= A[0][1] &lt;= ... &lt;= A[0][A[0].length - 1]</code>), <code>A[1]</code> is in lexicographic order (ie. <code>A[1][0] &lt;= A[1][1] &lt;= ... &lt;= A[1][A[1].length - 1]</code>), and so on.</p>

<p>Return the minimum possible value of <code>D.length</code>.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;babca&quot;,&quot;bbazb&quot;]</span>
<strong>Output: </strong><span id="example-output-1">3</span>
<strong>Explanation: </strong>After deleting columns 0, 1, and 4, the final array is A = [&quot;bc&quot;, &quot;az&quot;].
Both these rows are individually in lexicographic order (ie. A[0][0] &lt;= A[0][1] and A[1][0] &lt;= A[1][1]).
Note that A[0] &gt; A[1] - the array A isn&#39;t necessarily in lexicographic order.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[&quot;edcba&quot;]</span>
<strong>Output: </strong><span id="example-output-2">4</span>
<strong>Explanation: </strong>If we delete less than 4 columns, the only row won&#39;t be lexicographically sorted.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[&quot;ghi&quot;,&quot;def&quot;,&quot;abc&quot;]</span>
<strong>Output: </strong><span id="example-output-3">0</span>
<strong>Explanation: </strong>All rows are already lexicographically sorted.
</pre>

<p>&nbsp;</p>
</div>
</div>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 100</code></li>
	<li><code>1 &lt;= A[i].length &lt;= 100</code></li>
</ol>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Dynamic Programming

**Intuition and Algorithm**

This is a tricky problem that is hard to build an intuition about.

First, lets try to find the number of columns to keep, instead of the number to delete.  At the end, we can subtract to find the desired answer.

Now, let's say we must keep the first column `C`.  The next column `D` we keep must have all rows lexicographically sorted (ie. `C[i] <= D[i]`), and we can say that we have deleted all columns between `C` and `D`.

Now, we can use dynamic programming to solve the problem in this manner.  Let `dp[k]` be the number of columns that are kept in answering the question for input `[row[k:] for row in A]`.  The above gives a simple recursion for `dp[k]`.

<iframe src="https://leetcode.com/playground/qWgCzKBM/shared" frameBorder="0" width="100%" height="395" name="qWgCzKBM"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N * W^2)$$, where $$N$$ is the length of `A`, and $$W$$ is the length of each word in `A`.

* Space Complexity:  $$O(W)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Maximum Increasing Subsequence
- Author: lee215
- Creation Date: Sun Dec 16 2018 12:05:25 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 22 2019 13:06:40 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**

Take `n` cols as `n` elements, so we have an array of `n` elements.
=> The final array has every row in lexicographic order.
=> The elements in final state is in increasing order.
=> The final elements is a sub sequence of initial array.
=> Transform the problem to find the maximum increasing sub sequence.

<br>

## **Explanation**
Now let\'s talking about how to find maximum increasing subsequence.
Here we apply a O(N^2) dp solution.


`dp[i]` means the longest subsequence ends with `i`-th element.
For all `j < i`, if `A[][j] < A[][i]`, then `dp[j] = max(dp[j], dp[i] + 1)`
<br>

## **Time Complexity**:
`O(N^2)` to find increasing subsequence
`O(M)` for comparing elements.
So Overall `O(MN^2)` time.
Space `O(N)` for dp.
<br>

**C++:**
```
    int minDeletionSize(vector<string>& A) {
        int m = A.size(), n = A[0].length(), res = n - 1, k;
        vector<int>dp(n, 1);
        for (int j = 0; j < n; ++j) {
            for (int i = 0; i < j; ++i) {
                for (k = 0; k < m; ++k) {
                    if (A[k][i] > A[k][j]) break;
                }
                if (k == m && dp[i] + 1 > dp[j])
                    dp[j] = dp[i] + 1;
            }
            res = min(res, n - dp[j]);
        }
        return res;
    }
```

**Java:**
```
    public int minDeletionSize(String[] A) {
        int m = A.length, n = A[0].length(), res = n - 1, k;
        int[] dp = new int[n];
        Arrays.fill(dp, 1);
        for (int j = 0; j < n; ++j) {
            for (int i = 0; i < j; ++i) {
                for (k = 0; k < m; ++k) {
                    if (A[k].charAt(i) > A[k].charAt(j)) break;
                }
                if (k == m && dp[i] + 1 > dp[j])
                    dp[j] = dp[i] + 1;
            }
            res = Math.min(res, n - dp[j]);
        }
        return res;
    }
```
**Python:**
```
    def minDeletionSize(self, A):
        n = len(A[0])
        dp = [1] * n
        for j in xrange(1, n):
            for i in xrange(j):
                if all(a[i] <= a[j] for a in A):
                    dp[j] = max(dp[j], dp[i] + 1)
        return n - max(dp)
```

</p>


### C++ 7 lines, DP O(n * n * m)
- Author: votrubac
- Creation Date: Wed Dec 19 2018 12:54:05 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Dec 19 2018 12:54:05 GMT+0800 (Singapore Standard Time)

<p>
A twist of the [300. Longest Increasing Subsequence](https://leetcode.com/problems/longest-increasing-subsequence/description/) problem.

For each position ```i```,we track the maximum increasing subsequence. To do that, we analyze all ```j < i```, and if ```A[j] < A[i]``` for *all strings* , then ```dp[i] = dp[j] + 1```.The runtime complexity is O(n * n * m), where n is the number of characters, and m is the number of strings.

Unlike #300, we cannot use the binary search approach here because there is no stable comparasion (e.g. ```["ba", "ab"]``` are both "less" than each other).
```
int minDeletionSize(vector<string>& A) {
  vector<int> dp(A[0].size(), 1);
  for (auto i = 0; i < A[0].size(); ++i) {
    for (auto j = 0; j < i; ++j)
      for (auto k = 0; k <= A.size(); ++k) {
        if (k == A.size()) dp[i] = max(dp[i], dp[j] + 1);
        else if (A[k][j] > A[k][i]) break;
      }
  }
  return A[0].size() - *max_element(begin(dp), end(dp));
}
```
</p>


### Java Solution With Explanation
- Author: naveen_kothamasu
- Creation Date: Wed Jul 24 2019 12:00:22 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 24 2019 12:00:22 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
**Prereq** https://leetcode.com/problems/longest-increasing-subsequence/discuss/342274/Intuitive-Java-Solution-With-Explanation

Now, when filling the `dp` array instead of looking at one string for comparing `i` and `j` indices, we need to look at all strings at those indices and confirm char at `i` can be prepended to char at `j` in all rows.

```
public int minDeletionSize(String[] a) {
        int n = a[0].length(), res = n-1;
        int[] dp = new int[n];
        for(int k=0; k < a.length; k++){
            int max = 0;
            for(int i=n-1; i >= 0; i--){
                dp[i] = 1;
                for(int j=i+1; j < n; j++)
                    if(compare(a, i, j) == -1)
                        dp[i] = Math.max(dp[i], 1+dp[j]);
                max = Math.max(max, dp[i]);
            }
            res = Math.min(res, n-max);
        }
        return res;
    }
    private int compare(String[] a, int i, int j){
        for(int k=0; k < a.length; k++){
            if(a[k].charAt(i) > a[k].charAt(j))
                return 1;
        }
        return -1;
    }
```
</p>


