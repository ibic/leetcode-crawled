---
title: "Find the Winner of an Array Game"
weight: 1404
#id: "find-the-winner-of-an-array-game"
---
## Description
<div class="description">
<p>Given an integer array <code>arr</code> of <strong>distinct</strong> integers and an integer <code>k</code>.</p>

<p>A game will be played between the first two elements of the array (i.e. <code>arr[0]</code> and <code>arr[1]</code>). In each round of the game, we compare <code>arr[0]</code> with <code>arr[1]</code>, the larger integer&nbsp;wins and remains at position <code>0</code> and the smaller integer moves to the end of the array. The game ends when an integer wins <code>k</code> consecutive rounds.</p>

<p>Return <em>the integer which will win the game</em>.</p>

<p>It is <strong>guaranteed</strong> that there will be a winner of the game.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,1,3,5,4,6,7], k = 2
<strong>Output:</strong> 5
<strong>Explanation:</strong> Let&#39;s see the rounds of the game:
Round |       arr       | winner | win_count
  1   | [2,1,3,5,4,6,7] | 2      | 1
  2   | [2,3,5,4,6,7,1] | 3      | 1
  3   | [3,5,4,6,7,1,2] | 5      | 1
  4   | [5,4,6,7,1,2,3] | 5      | 2
So we can see that 4 rounds will be played and 5 is the winner because it wins 2 consecutive games.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [3,2,1], k = 10
<strong>Output:</strong> 3
<strong>Explanation:</strong> 3 will win the first 10 rounds consecutively.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,9,8,2,3,7,6,4,5], k = 7
<strong>Output:</strong> 9
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,11,22,33,44,55,66,77,88,99], k = 1000000000
<strong>Output:</strong> 99
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= arr.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 10^6</code></li>
	<li><code>arr</code> contains <b>distinct</b>&nbsp;integers.</li>
	<li><code>1 &lt;= k &lt;= 10^9</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Directi - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] One Pass, O(1) Space
- Author: lee215
- Creation Date: Sun Aug 02 2020 12:06:38 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 07 2020 11:33:05 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Don\'t need a queue or push the elements.

If you don\'t find the winner after one pass,
the winner will be `max(A)`,
bacause no one will beats him anymore.
<br>

# **Explanation**
`cur` is the current winner, wich is the current biggest element.
`win` is the number of games it has won.
If `max(A)` conpetes, it will be the winner.
<br>

# **Complexity**
Time `O(N)`
Space `O(1)`
<br>

**Java:**
```java
    public int getWinner(int[] A, int k) {
        int cur = A[0], win = 0;
        for (int i = 1; i < A.length; ++i) {
            if (A[i] > cur) {
                cur = A[i];
                win = 0;
            }
            if (++win == k) break;
        }
        return cur;
    }
```
**C++:**
```cpp
    int getWinner(vector<int>& A, int k) {
        int cur = A[0], win = 0;
        for (int i = 1; i < A.size(); ++i) {
            if (A[i] > cur) {
                cur = A[i];
                win = 0;
            }
            if (++win == k) break;
        }
        return cur;
    }
```

**Python:**
```py
    def getWinner(self, A, k):
        cur = A[0]
        win = 0
        for i in xrange(1, len(A)):
            if A[i] > cur:
                cur = A[i]
                win = 0
            win += 1
            if (win == k): break
        return cur
```

</p>


### [C++, Python, Java] Single Pass, Easy and Readable Code with Comments
- Author: interviewrecipes
- Creation Date: Sun Aug 02 2020 12:03:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 08 2020 13:07:59 GMT+0800 (Singapore Standard Time)

<p>
**Key Idea**
It\'s not difficult to realize that when `k` is greater than the size of the array, then the max number, say `M`, in the array has to be the winner.
This is because, the moment we reach the `M`, the winner is not going to change. It will always be `M`.

So if you find an answer before reaching `M`, immediately return. Otherwise, `M` is going to be the answer.

Reading C++ code should give the detailed idea. 

**Thanks**
Thanks for reading. If you find this helpful, please upvote so that -
1.  It encourages me to write more and share solutions on Leetcode and on my blog/website, if I have an easy, intuitive approach to solve the problem.
2.  Other learners get to see the post near the top


**C++**
```
class Solution {
public:
    int getWinner(vector<int>& arr, int k) {
        unordered_map<int, int> matchesWon;
        int n = arr.size();
        int currentMax = arr[0];  // Assume first element as max, but don\'t increment the "matchesWon" count.
        for (int i=1; i<n; i++) {
            currentMax = max(currentMax, arr[i]); // Match.
            matchesWon[currentMax]++;  // Increment the winner.
            if (matchesWon[currentMax] >= k) { // Check the matches won so far.
                return currentMax;
            }
        }
		// Didn\'t find the overall winner yet? Then the max element in the array will never lose, so ultimately
		// that number will be the winner.
        return currentMax;
    }
};
```

**Python**
```
class Solution(object):
    def getWinner(self, arr, k):
        d = dict()
        n = len(arr)
        mx = arr[0]
        for i in range(1, n):
            mx = max(mx, arr[i])
            d[mx] = d[mx] + 1 if mx in d.keys() else 1
            if d[mx] >= k:
                return mx
        return mx
```

**Java**
```
public int getWinner(int[] arr, int k) {
        Map<Integer, Integer> matchesWon = new HashMap<>();
        int n = arr.length;
        int currentMax = arr[0];  // Assume first element as max, but don\'t increment the "matchesWon" count.
        for (int i = 1; i < n; i++) {
            currentMax = Math.max(currentMax, arr[i]); // Match.
            int count = matchesWon.getOrDefault(currentMax, 0);
            matchesWon.put(currentMax, ++count);  // Increment the winner.
            if (matchesWon.get(currentMax) >= k) { // Check the matches won so far.
                return currentMax;
            }
        }
        // Didn\'t find the overall winner yet? Then the max element in the array will never lose, so ultimately
        // that number will be the winner.
        return currentMax;
    }
```

</p>


### Java | Easy one | One pass |  With clear explanation
- Author: logan138
- Creation Date: Sun Aug 02 2020 12:01:09 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 03 2020 21:07:33 GMT+0800 (Singapore Standard Time)

<p>
```
Explanation:-
    The idea is how many rounds the current winner can win before the new winner comes.
    If the current winner wins k round, then return that winner
	
	I have seen many people having a problem when k > n
	So, if k > n, then there is no element that can win k consecutive rounds except the maximum number.
	
	Ex:- 
	Suppose we have A = [1, 2, 3] and k = 5
	Round 1 : A = [2, 3, 1] and wins = 1 (2 won)
	R2 : A = [3, 1, 2] and wins = 1 (3 won)
	R3 : A = [3, 2, 1] and wins = 2 (3 won)
	R4 : A = [3, 1, 2] and wins = 3 (3 won)
	R5 : A = [3, 2, 1] and wins = 4 (3 won)
	R6 : A = [3, 1, 2] and wins = 5 (3 won)
	
	Now, 3 won 5 consecutive rounds. So, 3 is the winner.
	
	From above example, we can understand that
	if k > n, maximum number in the array will be moved to 0th position, 
	no matter how much bigger your k is, the maximum number is always winner.
	
	That\'s why we are not checking the case k > n.
	Here, what we are cheking is an element that can win k consecutive rounds, 
	if we found, then we are coming out of the loop and returning the winner.
	
	IF YOU HAVE ANY DOUBTS, FEEL FREE TO ASK
```
```
class Solution {
    public int getWinner(int[] arr, int k) {
        int winner = arr[0];
        int wins = 0;
        for(int i = 1; i < arr.length; i++){
            if(winner > arr[i])
			    // increment wins count
                wins++;
            else{
                wins = 1;
				// new winner
                winner = arr[i];
            }
            if(wins == k)
			    // if wins count is k, then return winner 
                break;
        }
        return winner;
    }
}
```
</p>


