---
title: "Sum of Distances in Tree"
weight: 777
#id: "sum-of-distances-in-tree"
---
## Description
<div class="description">
<p>An undirected, connected&nbsp;tree with <code>N</code> nodes labelled <code>0...N-1</code> and <code>N-1</code> <code>edges</code>&nbsp;are&nbsp;given.</p>

<p>The <code>i</code>th edge connects nodes&nbsp;<code>edges[i][0] </code>and<code>&nbsp;edges[i][1]</code>&nbsp;together.</p>

<p>Return a list <code>ans</code>, where <code>ans[i]</code> is the sum of the distances between node <code>i</code> and all other nodes.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>N = 6, edges = [[0,1],[0,2],[2,3],[2,4],[2,5]]
<strong>Output: </strong>[8,12,6,10,10,10]
<strong>Explanation: </strong>
Here is a diagram of the given tree:
  0
 / \
1   2
   /|\
  3 4 5
We can see that dist(0,1) + dist(0,2) + dist(0,3) + dist(0,4) + dist(0,5)
equals 1 + 1 + 2 + 2 + 2 = 8.  Hence, answer[0] = 8, and so on.
</pre>

<p>Note:<font face="monospace">&nbsp;<code>1 &lt;= N &lt;= 10000</code></font></p>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Subtree Sum and Count [Accepted]

**Intuition**

Let `ans` be the returned answer, so that in particular `ans[x]` be the answer for node `x`.

Naively, finding each `ans[x]` would take $$O(N)$$ time  (where $$N$$ is the number of nodes in the graph), which is too slow.  This is the motivation to find out how `ans[x]` and `ans[y]` are related, so that we cut down on repeated work.

Let's investigate the answers of neighboring nodes $$x$$ and $$y$$.  In particular, say $$xy$$ is an edge of the graph, that if cut would form two trees $$X$$ (containing $$x$$) and $$Y$$ (containing $$y$$).

<center>
    <img src="../Figures/834/sketch1.png" alt="Tree diagram illustrating recurrence for ans[child]" style="width: 1000px;"/>
</center>

Then, as illustrated in the diagram, the answer for $$x$$ in the entire tree, is the answer of $$x$$ on $$X$$ `"x@X"`, plus the answer of $$y$$ on $$Y$$ `"y@Y"`, plus the number of nodes in $$Y$$ `"#(Y)"`.  The last part `"#(Y)"` is specifically because for any node `z in Y`, `dist(x, z) = dist(y, z) + 1`.

By similar reasoning, the answer for $$y$$ in the entire tree is `ans[y] = x@X + y@Y + #(X)`.  Hence, for neighboring nodes $$x$$ and $$y$$, `ans[x] - ans[y] = #(Y) - #(X)`.

**Algorithm**

Root the tree.  For each node, consider the subtree $$S_{\text{node}}$$ of that node plus all descendants.  Let `count[node]` be the number of nodes in $$S_{\text{node}}$$, and `stsum[node]` ("subtree sum") be the sum of the distances from `node` to the nodes in $$S_{\text{node}}$$.

We can calculate `count` and `stsum` using a post-order traversal, where on exiting some `node`, the `count` and `stsum` of all descendants of this node is correct, and we now calculate `count[node] += count[child]` and `stsum[node] += stsum[child] + count[child]`.

This will give us the right answer for the `root`: `ans[root] = stsum[root]`.

Now, to use the insight explained previously: if we have a node `parent` and it's child `child`, then these are neighboring nodes, and so `ans[child] = ans[parent] - count[child] + (N - count[child])`.  This is because there are `count[child]` nodes that are `1` easier to get to from `child` than `parent`, and `N-count[child]` nodes that are `1` harder to get to from `child` than `parent`.

<center>
    <img src="../Figures/834/sketch2.png" alt="Tree diagram illustrating recurrence for ans[child]" style="height: 200px;"/>
</center>

Using a second, pre-order traversal, we can update our answer in linear time for all of our nodes.

<iframe src="https://leetcode.com/playground/oEgDt9DY/shared" frameBorder="0" width="100%" height="500" name="oEgDt9DY"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the graph.

* Space Complexity:  $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Pre-order and Post-order DFS, O(N)
- Author: lee215
- Creation Date: Sun May 13 2018 11:06:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Feb 15 2020 15:31:11 GMT+0800 (Singapore Standard Time)

<p>
# **Preword**
Well, another long solution.
what I am always trying is to:
1. let you understand my solution (with my poor explanation)
2. prevent from reading my codes
<br>

# **Intuition**
What if given a tree, with a certain root `0`?
In `O(N)` we can find sum of distances in tree from root and all other nodes.
Now for all `N` nodes?
Of course, we can do it `N` times and solve it in `O(N^2)`.
C++ and Java may get accepted luckily, but it\'s not what we want.

When we move our root from one node to its connected node,
one part of nodes get closer, one the other part get further.

If we know exactly how many nodes in both parts, we can solve this problem.

With one single traversal in tree, we should get enough information for it and
don\'t need to do it again and again.
<br>

# **Explanation**
0. Let\'s solve it with node `0` as root.
1. Initial an array of hashset `tree`, `tree[i]` contains all connected nodes to `i`.
   Initial an array `count`, `count[i]` counts all nodes in the subtree `i`.
   Initial an array of `res`, `res[i]` counts sum of distance in subtree `i`.

2. Post order dfs traversal, update `count` and `res`:
`count[root] = sum(count[i]) + 1`
`res[root] = sum(res[i]) + sum(count[i])`

3. Pre order dfs traversal, update `res`:
When we move our root from parent to its child `i`, `count[i]` points get 1 closer to root, `n - count[i]` nodes get 1 futhur to root.
`res[i] = res[root] - count[i] + N - count[i]`

4. return res, done.
<br>

# **Time Complexity**:
dfs: `O(N)` time
dfs2: `O(N)` time
<br>

**Java:**
```java
    int[] res, count;
    ArrayList<HashSet<Integer>> tree;
    public int[] sumOfDistancesInTree(int N, int[][] edges) {
        tree = new ArrayList<HashSet<Integer>>();
        res = new int[N];
        count = new int[N];
        for (int i = 0; i < N ; ++i)
            tree.add(new HashSet<Integer>());
        for (int[] e : edges) {
            tree.get(e[0]).add(e[1]);
            tree.get(e[1]).add(e[0]);
        }
        dfs(0, -1);
        dfs2(0, -1);
        return res;
    }

    public void dfs(int root, int pre) {
        for (int i : tree.get(root)) {
            if (i == pre) continue;
            dfs(i, root);
            count[root] += count[i];
            res[root] += res[i] + count[i];
        }
        count[root]++;
    }


    public void dfs2(int root, int pre) {
        for (int i : tree.get(root)) {
            if (i == pre) continue;
            res[i] = res[root] - count[i] + count.length - count[i];
            dfs2(i, root);
        }
    }
```

**C++:**
```cpp
    vector<unordered_set<int>> tree;
    vector<int> res, count;

    vector<int> sumOfDistancesInTree(int N, vector<vector<int>>& edges) {
        tree.resize(N);
        res.assign(N, 0);
        count.assign(N, 1);
        for (auto e : edges) {
            tree[e[0]].insert(e[1]);
            tree[e[1]].insert(e[0]);
        }
        dfs(0, -1);
        dfs2(0, -1);
        return res;

    }

    void dfs(int root, int pre) {
        for (auto i : tree[root]) {
            if (i == pre) continue;
            dfs(i, root);
            count[root] += count[i];
            res[root] += res[i] + count[i];
        }
    }

    void dfs2(int root, int pre) {
        for (auto i : tree[root]) {
            if (i == pre) continue;
            res[i] = res[root] - count[i] + count.size() - count[i];
            dfs2(i, root);
        }
    }
```


**Python:**
```py
    def sumOfDistancesInTree(self, N, edges):
        tree = collections.defaultdict(set)
        res = [0] * N
        count = [1] * N
        for i, j in edges:
            tree[i].add(j)
            tree[j].add(i)

        def dfs(root, pre):
            for i in tree[root]:
                if i != pre:
                    dfs(i, root)
                    count[root] += count[i]
                    res[root] += res[i] + count[i]

        def dfs2(root, pre):
            for i in tree[root]:
                if i != pre:
                    res[i] = res[root] - count[i] + N - count[i]
                    dfs2(i, root)
        dfs(0, -1)
        dfs2(0, -1)
        return res
```
</p>


### Two traversals O(N) python solution with Explanation
- Author: LuckyPants
- Creation Date: Sun May 13 2018 11:01:02 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 14:07:29 GMT+0800 (Singapore Standard Time)

<p>
Use two traversals on the tree, we can root at any node.

- The first step is to calculate the size of each subtree and the sum distance for the root of each subtree. Also, the sum distance for our chosen root is the correct sum distance for the entire tree.
- The second step is to calculte the sum distance in the whole tree for each node, using the size of each subtree.

![image](https://s3-lc-upload.s3.amazonaws.com/users/luckypants/image_1526180451.png)


```
class Solution:
    def sumOfDistancesInTree(self, N, edges):
        dic1 = collections.defaultdict(list)
        for e in edges:
            dic1[e[0]].append(e[1])
            dic1[e[1]].append(e[0])
        
        exclude = {0}
        
        # eachItem subtreeDist[n]=[a, b] means subtree rooted at n has totally a nodes, 
        # and sum of distance in the subtree for n is b
        subtreeDist = [[0, 0] for _ in range(N)]
        
        ans = [0]*N
        
        def sumSubtreeDist(n, exclude):
            cnt, ret = 0, 0
            exclude.add(n)
            for x in dic1[n]:
                if x in exclude:
                    continue
                res = sumSubtreeDist(x, exclude)
                cnt += res[0]
                ret += (res[0]+res[1])
            subtreeDist[n][0] = cnt+1
            subtreeDist[n][1] = ret
            return cnt+1, ret
            
        # recursively calculate the sumDist for all subtrees 
        # 0 can be replaced with any other number in [0, N-1]
        # and the chosen root has its correct sum distance in the whole tree
        sumSubtreeDist(0, set())
        
        # visit and calculates the sum distance in the whole tree
        def visit(n, pre, exclude):
            if pre==-1:
                ans[n] = subtreeDist[n][1]
            else:
                ans[n] = ans[pre]-2*subtreeDist[n][0]+N
            exclude.add(n)
            for x in dic1[n]:
                if x not in exclude:
                    visit(x, n, exclude)
                
        visit(0, -1, set())
        return ans
```
</p>


### Classic Dp on Trees | Thoroughly explained | C++ Implementation with O(n) time
- Author: prabhAmbrose
- Creation Date: Fri Jun 19 2020 20:31:57 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 14 2020 19:36:53 GMT+0800 (Singapore Standard Time)

<p>
* This is personally one of my fav question, Such a lovely question it is.

**Basic Intuition**
* First of all lets discuss the naive / brute force approach, if you see the constraints for the same then you would realize that O(n^2) solution is viable option.
* Then talking abt a particular node, then for the ans of a single is dist values of all its child + size of the subtree of its child(since I add 1 to each of the member of its subtree) then like this I can make a dfs for each node and in linear time calculate the answer for the single node and total would be quadratic solution

**Realizing "Dp on Tree"**
* But if you carefully notice, lets say all the node in tree and I am standing at a intermediate node, then the only problem is I can have the answer from my child but not from my parent, So if somehow I can get the value of the my parent, then in single dfs run I can calculate the answers for all my child
* So what I can assume is my parent needs to act as a child for him to return its val, So lets say I would be having a variable in my dfs call that would take care for the same.
* The whole crux of the problem is here that WHAT IF SOMEHOW I CAN MAKE MY PARENT AS MY CHILD AND THEN IT WOULD BE MUCH BETTER FOR ME AS A NODE TO GET ALL THE VALUES 

**Explaining the Implemented Code**
* I have two dfs calls
* Lets talk of dfs1 first, this is just a assumption that how things would have worked if I would have been told to do calculation for a single element, then we come back to naive approach that we discussed above and here I have two vectors - one storing the distance value from my child and another size of my subtree 
* Now lets talk for the dfs2 which is the actual dp part. Here I am taking a variable called dpVal which will be used to pass the val of par to my child ( hence making parent act as a child ) 
* Now standing at a node, ans[node] = already stored val of mine + dpVal of my parent 
* And for the subsequent calls I update dpVal as required 

**I suggest you go through the code throughly, at first things may appear hard, but if you get the thing it would help you in long run**

**Code**
```
class Solution {
	int n;
	vector<list<int>> adjList;
	vector<int> sz, val, ans;

	void dfs1(int node, int par) {

		for (int child : adjList[node]) {
			if (child != par) {
				dfs1(child, node);
				sz[node] += sz[child];
				val[node] += val[child] + sz[child];
			}
		}
		sz[node]++;
	}

	void dfs2(int node, int par, int dpVal) {
		ans[node] = val[node] + dpVal + (n - sz[node]);

		for (int child : adjList[node]) {
			if (child != par)
				dfs2(child, node, ans[node] - val[child] - sz[child]);
		}
	}
    
public:
	vector<int> sumOfDistancesInTree(int N, vector<vector<int>>& edges) {
		n = N;
		adjList.resize(n), sz.resize(n), val.resize(n), ans.resize(n);
		for (vector<int> &v : edges) {
			adjList[v[0]].push_back(v[1]);
			adjList[v[1]].push_back(v[0]);
		}
		dfs1(0, 0);
		dfs2(0, 0, 0);
		return ans;
	}
};
```
* If you found the post helpful in any way, I would really request you to **upvote** the post as it helps the creator in many moral ways 

</p>


