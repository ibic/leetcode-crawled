---
title: "Find the Start and End Number of Continuous Ranges"
weight: 1557
#id: "find-the-start-and-end-number-of-continuous-ranges"
---
## Description
<div class="description">
<p>Table: <code>Logs</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| log_id        | int     |
+---------------+---------+
id is the primary key for this table.
Each row of this table contains the ID in a log Table.

</pre>

<p>Since some IDs&nbsp;have been removed from <code>Logs</code>. Write an SQL query to find the start and end number of continuous ranges in table <code>Logs</code>.</p>

<p>Order the result table by <code>start_id</code>.</p>

<p>The query result format is in the following example:</p>

<pre>
Logs table:
+------------+
| log_id     |
+------------+
| 1          |
| 2          |
| 3          |
| 7          |
| 8          |
| 10         |
+------------+

Result table:
+------------+--------------+
| start_id   | end_id       |
+------------+--------------+
| 1          | 3            |
| 7          | 8            |
| 10         | 10           |
+------------+--------------+
The result table should contain all ranges in table Logs.
From 1 to 3 is contained in the table.
From 4 to 6 is missing in the table
From 7 to 8 is contained in the table.
Number 9 is missing in the table.
Number 10 is contained in the table.
</pre>
</div>

## Tags


## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### my simple MS SQL Solution
- Author: TinyDad
- Creation Date: Fri Dec 20 2019 06:24:52 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Dec 20 2019 06:24:52 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT min(log_id) as start_id, max(log_id) as end_id
FROM
(SELECT log_id, ROW_NUMBER() OVER(ORDER BY log_id) as num
FROM Logs) a
GROUP BY log_id - num
```

inspiration from https://leetcode.com/problems/report-contiguous-dates/discuss/407117/MSSQL-100-Simple-solution
</p>


### MYSQL GROUP BY - EASY TO UNDERSTAND
- Author: ct2333
- Creation Date: Sat Dec 14 2019 07:41:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Dec 14 2019 07:41:57 GMT+0800 (Singapore Standard Time)

<p>
```
select L1.log_id as START_ID, min(L2.log_id) as END_ID
from 
	(select log_id from Logs 
	where log_id-1 not in (select log_id from Logs)) L1,
	(select log_id from Logs 
	where log_id+1 not in (select log_id from Logs)) L2
where L1.log_id <= L2.log_id
group by L1.log_id
```
</p>


### MySQL easy solution, 6 lines
- Author: xwang139
- Creation Date: Tue Dec 24 2019 03:45:12 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 24 2019 03:45:12 GMT+0800 (Singapore Standard Time)

<p>
	select min(log_id) as start_id, max(log_id) as end_id
	from(
		select *, (@id:=@id+1) as id
		from logs, (select @id:= 0) as init
	) tmp
	group by log_id - id
</p>


