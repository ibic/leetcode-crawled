---
title: "Largest 1-Bordered Square"
weight: 1114
#id: "largest-1-bordered-square"
---
## Description
<div class="description">
<p>Given a 2D <code>grid</code> of <code>0</code>s and <code>1</code>s, return the number of elements in&nbsp;the largest <strong>square</strong>&nbsp;subgrid that has all <code>1</code>s on its <strong>border</strong>, or <code>0</code> if such a subgrid&nbsp;doesn&#39;t exist in the <code>grid</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,1,1],[1,0,1],[1,1,1]]
<strong>Output:</strong> 9
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,1,0,0]]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= grid.length &lt;= 100</code></li>
	<li><code>1 &lt;= grid[0].length &lt;= 100</code></li>
	<li><code>grid[i][j]</code> is <code>0</code> or <code>1</code></li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Samsung - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### c++, beats 100% (both time and memory) concise, with algorithm and image
- Author: goelrishabh5
- Creation Date: Sun Jul 28 2019 12:09:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 28 2019 15:11:16 GMT+0800 (Singapore Standard Time)

<p>
Create auxillary horizontal and vertical arrays first
For example : 
![image](https://assets.leetcode.com/users/goelrishabh5/image_1564287873.png)

Then starting from bottom right,for every i,j ; we find small=min (ver[i][j], hor[i][j]) (marked in orange) , then look at all distances in [1,small] vertically in hor array and horizontally in ver array. If values(shown in blue) are greater than small and if small is greater than curr result, then we update result
![image](https://assets.leetcode.com/users/goelrishabh5/image_1564288202.png)


```
 int findLargestSquare(vector<vector<int>>& mat) 
    { 
    int max = 0; int m = mat.size() , n = mat[0].size();
    vector<vector<int>> hor(m,vector<int> (n,0)) , ver(m,vector<int> (n,0));
   
    for (int i=0; i<m; i++) { 
        for (int j=0; j<n; j++) { 
            if (mat[i][j] == 1) 
            { 
                hor[i][j] = (j==0)? 1: hor[i][j-1] + 1;   // auxillary horizontal array
                ver[i][j] = (i==0)? 1: ver[i-1][j] + 1;  // auxillary vertical array
            } 
        } 
    } 
        
    for (int i = m-1; i>=0; i--) { 
        for (int j = n-1; j>=0; j--) { 
            int small = min(hor[i][j], ver[i][j]);  // choose smallest of horizontal and vertical value
            while (small > max) { 
                if (ver[i][j-small+1] >= small &&  hor[i-small+1][j] >= small)  // check if square exists with \'small\' length
                    max = small; 
                small--; 
            } 
        } 
    } 
    return max*max; 
} 
    
    int largest1BorderedSquare(vector<vector<int>>& grid) {
        return findLargestSquare(grid); 
    }
```
</p>


### [Java/C++/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Jul 28 2019 12:03:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 28 2019 12:03:20 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
1. Count the number of consecutive 1s on the top and on the left.
2. From length of edge `l = min(m,n)` to `l = 1`, check if the 1-bordered square exist.
<br>

## **Complexity**
Time `O(N^3)`
Space `O(N^2)`
<br>

**Java:**
```java
class Solution {
    public int largest1BorderedSquare(int[][] A) {
        int m = A.length, n = A[0].length;
        int[][] left = new int[m][n], top = new int[m][n];
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                if (A[i][j] > 0) {
                    left[i][j] = j > 0 ? left[i][j - 1] + 1 : 1;
                    top[i][j] = i > 0  ? top[i - 1][j] + 1 : 1;
                }
            }
        }
        for (int l = Math.min(m, n); l > 0; --l)
            for (int i = 0; i < m - l + 1; ++i)
                for (int j = 0; j < n - l + 1; ++j)
                    if (top[i + l - 1][j] >= l &&
                            top[i + l - 1][j + l - 1] >= l &&
                            left[i][j + l - 1] >= l &&
                            left[i + l - 1][j + l - 1] >= l)
                        return l * l;
        return 0;
    }
}
```

**C++:**
```cpp
    int largest1BorderedSquare(vector<vector<int>>& A) {
        int m = A.size(), n = A[0].size();
        vector<vector<int>> left(m, vector<int>(n)), top(m, vector<int>(n));
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                left[i][j] = A[i][j] + (j && A[i][j] ? left[i][j - 1] : 0);
                top[i][j] = A[i][j] + (i && A[i][j] ? top[i - 1][j] : 0);
            }
        }
        for (int l = min(m, n); l > 0; --l)
            for (int i = 0; i < m - l + 1; ++i)
                for (int j = 0; j < n - l + 1; ++j)
                    if (min({top[i + l - 1][j], top[i + l - 1][j + l - 1], left[i][j + l - 1], left[i + l - 1][j + l - 1]}) >= l)
                        return l * l;
        return 0;
    }
```

**Python:**
```python
    def largest1BorderedSquare(self, A):
        m, n = len(A), len(A[0])
        res = 0
        top, left = [a[:] for a in A], [a[:] for a in A]
        for i in xrange(m):
            for j in xrange(n):
                if A[i][j]:
                    if i: top[i][j] = top[i - 1][j] + 1
                    if j: left[i][j] = left[i][j - 1] + 1
        for r in xrange(min(m, n), 0, -1):
            for i in xrange(m - r + 1):
                for j in xrange(n - r + 1):
                    if min(top[i + r - 1][j], top[i + r - 1][j + r - 1], left[i]
                           [j + r - 1], left[i + r - 1][j + r - 1]) >= r:
                        return r * r
        return 0
```

</p>


### clean JAVA DP over 100% ! just to store the maximum possibility
- Author: Sumonon
- Creation Date: Sun Jul 28 2019 12:44:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 28 2019 13:00:22 GMT+0800 (Singapore Standard Time)

<p>
The main trick here is to use two dp 2d array to respectively store the maximum left-side outreach point and top-side outreach point. 

By using these two dp, we can directly be inferred whether currenly possible length is valid or not. 

So in the third for loop, we just need to test the current possible length step by step, from the maximum point to the closest. (Early stop when found the valid length helps to reduce time).

goelrishabh5\'s image might help you to understand the meaning of outreach point
https://leetcode.com/problems/largest-1-bordered-square/discuss/345265/c%2B%2B-beats-100-(both-time-and-memory)-concise-with-algorithm-and-image

Please be free to leave any question~ GOOD LUCK
```
class Solution {
    public int largest1BorderedSquare(int[][] grid) {
        if (grid.length==0) return 0;
        int[][] dpr = new int[grid.length+1][grid[0].length+1];
        int[][] dpc = new int[grid.length+1][grid[0].length+1];
        int dist, max=0;
        for (int r=1;r<=grid.length;r++){
            for (int c=1;c<=grid[0].length;c++){
                if (grid[r-1][c-1]==0) continue;
                dpr[r][c] = dpr[r-1][c]+1;
                dpc[r][c] = dpc[r][c-1]+1;
                dist = Math.min(dpr[r][c],dpc[r][c]);
                for (int i=dist;i>=1;i--){
                    if (dpr[r][c-i+1]>=i 
                        && dpc[r-i+1][c]>=i){
                        max = Math.max(max, i*i);
                        break;
                    }
                }
            }
        }
        return max;
    }
}
```
</p>


