---
title: "Smallest Common Region"
weight: 1079
#id: "smallest-common-region"
---
## Description
<div class="description">
<p>You are given some lists of <code>regions</code>&nbsp;where the first region of each list includes all other regions in that list.</p>

<p>Naturally, if a region <code>X</code>&nbsp;contains another region <code>Y</code>&nbsp;then <code>X</code>&nbsp;is bigger than <code>Y</code>. Also by definition a region X contains itself.</p>

<p>Given two regions <code>region1</code>, <code>region2</code>, find out the <strong>smallest</strong> region that contains both of them.</p>

<p>If you are given regions <code>r1</code>, <code>r2</code> and <code>r3</code> such that <code>r1</code> includes <code>r3</code>, it is guaranteed there is no <code>r2</code> such that <code>r2</code> includes <code>r3</code>.<br />
<br />
It&#39;s guaranteed the smallest region exists.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:
</strong>regions = [[&quot;Earth&quot;,&quot;North America&quot;,&quot;South America&quot;],
[&quot;North America&quot;,&quot;United States&quot;,&quot;Canada&quot;],
[&quot;United States&quot;,&quot;New York&quot;,&quot;Boston&quot;],
[&quot;Canada&quot;,&quot;Ontario&quot;,&quot;Quebec&quot;],
[&quot;South America&quot;,&quot;Brazil&quot;]],
region1 = &quot;Quebec&quot;,
region2 = &quot;New York&quot;
<strong>Output:</strong> &quot;North America&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= regions.length &lt;= 10^4</code></li>
	<li><code>region1 != region2</code></li>
	<li>All strings consist of English letters and spaces with at most 20 letters.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Airbnb - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Lowest common ancestor w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Nov 17 2019 00:06:25 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 18 2019 00:59:29 GMT+0800 (Singapore Standard Time)

<p>
1. Build family tree from offsprings to their parents;
2. Use a HashSet to construct ancestry history of region1;
3. Retrieve ancestry of region2 by family tree till find the first common ancestry in ancestry history of region1.

```java
    public String findSmallestRegion(List<List<String>> regions, String region1, String region2) {
        Map<String, String> parents = new HashMap<>();
        Set<String> ancestryHistory = new HashSet<>();
        for (List<String> region : regions)
            for (int i = 1; i < region.size(); ++i)
                parents.put(region.get(i), region.get(0));
        while (region1 != null) {
            ancestryHistory.add(region1);
            region1 = parents.get(region1);
        }
        while (!ancestryHistory.contains(region2))
            region2 = parents.get(region2);
        return region2;
    }
```
```python
    def findSmallestRegion(self, regions: List[List[str]], region1: str, region2: str) -> str:
        parents = {region[i] : region[0] for region in regions for i in range(1, len(region))}
        ancestry_history = {region1}
        while region1 in parents:
            region1 = parents[region1]
            ancestry_history.add(region1)
        while region2 not in ancestry_history:
            region2 = parents[region2]
        return region2
```
**Analysis:**
Time & space: O(n), where n is the the number of totoal regions.
</p>


### Python simple solution
- Author: davyjing
- Creation Date: Sun Nov 17 2019 00:02:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 00:18:05 GMT+0800 (Singapore Standard Time)

<p>
Find the direct parent of each region.
```chain``` is all the ancestors of ```region1```.
Go from the lowest parent of ```region2```, move upward, until it joins ```chain```.
```
class Solution:
    def findSmallestRegion(self, regions: List[List[str]], region1: str, region2: str) -> str:
        parent = {}
        for region in regions:
            for i in range(1,len(region)):
                parent[region[i]] = region[0]
        chain = set([region1])
        while region1 in parent:
            region1 = parent[region1]
            chain.add(region1)
        while region2 not in chain:
            region2 = parent[region2]
        return region2
```
</p>


### C++ Beats 100% Time and Space
- Author: makeavish
- Creation Date: Sun Nov 17 2019 00:17:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 00:51:16 GMT+0800 (Singapore Standard Time)

<p>
Simple solution using hashmap.

```
class Solution {
public:
    
    string findSmallestRegion(vector<vector<string>>& regions, string region1, string region2) {
        unordered_map<string,string> m;
        vector<string> vec1;
        vector<string> vec2;
        string t1 = region1;
        string t2 = region2;
        for(int i=0;i<regions.size();i++)
        {
            
            for(int j=1;j<regions[i].size();j++)
            {
                m[regions[i][j]]=regions[i][0]; // Map each region to it\'s Parent region.
            }
        }
        while(region1.size()!=0)
        {
            vec1.push_back(region1); // Insert all the parents to vector of region1
            region1 = m[region1];
        }
        while(region2.size()!=0)
        {
            vec2.push_back(region2); // Insert all the parents to vector of region1
            region2 = m[region2];
        }
        
        for(auto v1 : vec1)
        {
            for(auto v2 : vec2)
            {
                if(v2==v1)
                    return v1;    // Matching all the parents in increasing order
            }
        }
        return "anything";
    }
};
```
</p>


