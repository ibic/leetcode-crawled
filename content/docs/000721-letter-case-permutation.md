---
title: "Letter Case Permutation"
weight: 721
#id: "letter-case-permutation"
---
## Description
<div class="description">
<p>Given a string S, we can transform every letter individually&nbsp;to be lowercase or uppercase to create another string.</p>

<p>Return <em>a list of all possible strings we could create</em>. You can return the output&nbsp;in <strong>any order</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> S = &quot;a1b2&quot;
<strong>Output:</strong> [&quot;a1b2&quot;,&quot;a1B2&quot;,&quot;A1b2&quot;,&quot;A1B2&quot;]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> S = &quot;3z4&quot;
<strong>Output:</strong> [&quot;3z4&quot;,&quot;3Z4&quot;]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> S = &quot;12345&quot;
<strong>Output:</strong> [&quot;12345&quot;]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> S = &quot;0&quot;
<strong>Output:</strong> [&quot;0&quot;]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>S</code> will be a string with length between <code>1</code> and <code>12</code>.</li>
	<li><code>S</code> will consist only of letters or digits.</li>
</ul>

</div>

## Tags
- Backtracking (backtracking)
- Bit Manipulation (bit-manipulation)

## Companies
- Bloomberg - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Spotify - 2 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Yelp - 0 (taggedByAdmin: true)
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Recursion [Accepted]

**Intuition**

Maintain the correct answer as we increase the size of the prefix of `S` we are considering.

For example, when `S = "abc"`, maintain `ans = [""]`, and update it to `ans = ["a", "A"]`, `ans = ["ab", "Ab", "aB", "AB"]`, `ans = ["abc", "Abc", "aBc", "ABc", "abC", "AbC", "aBC", "ABC"]` as we consider the letters `"a", "b", "c"`.

**Algorithm**

If the next character `c` is a letter, then we will duplicate all words in our current answer, and add `lowercase(c)` to every word in the first half, and `uppercase(c)` to every word in the second half.

If instead `c` is a digit, we'll add it to every word.

<iframe src="https://leetcode.com/playground/riB6THWW/shared" frameBorder="0" width="100%" height="480" name="riB6THWW"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(2^{N} * N)$$, where $$N$$ is the length of `S`.  This reflects the cost of writing the answer.

* Space Complexity:  $$O(2^N * N)$$.

---
#### Approach #2: Binary Mask [Accepted]

**Intuition**

Say there are $$B$$ letters in the string `S`.  There will be $$2^B$$ strings in the answer, which we can represent uniquely by the bitmask `bits`.

For example, we could represent `a7b` by `00`, `a7B` by `01`, `A7b` by `10`, and `A7B` by `11`.  Note that numbers are not part of the bitmask.

**Algorithm**

For every possible bitmask, construct the correct result to put in the final answer.  If the next letter in the word is a letter, write a lowercase or uppercase letter depending on the bitmask.  Otherwise, write the digit as given.

<iframe src="https://leetcode.com/playground/kZsm88TQ/shared" frameBorder="0" width="100%" height="500" name="kZsm88TQ"></iframe>

**Complexity Analysis**

* Time and Space Complexity:  $$O(2^{N} * N)$$.  The analysis is the same as in *Approach #1*.

---
#### Approach #3: Built-In Library Function [Accepted]

**Intuition and Algorithm**

A *cartesian product* of sets is every possible combination of one choice from those sets.  For example, `{1, 2} x {a, b, c} = {1a, 1b, 1c, 2a, 2b, 2c}`.

For languages that have a built-in function to calculate a cartesian product, we can use this function to minimize our work.

<iframe src="https://leetcode.com/playground/E3L2h2Fa/shared" frameBorder="0" width="100%" height="123" name="E3L2h2Fa"></iframe>

**Complexity Analysis**

* Time and Space Complexity:  $$O(2^{N} * N)$$.  The analysis is the same as in *Approach #1*.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Easy BFS / DFS solution with explanation
- Author: FLAGbigoffer
- Creation Date: Sun Feb 18 2018 14:41:00 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 14:01:39 GMT+0800 (Singapore Standard Time)

<p>
When I saw a problem, my first step is to draw a figure. See the figure below:
 ` abc`  
`  abc  Abc`     0
 ` abc aBc  Abc ABc`    1
`abc abC  aBc  aBC  Abc  AbC  ABc  ABC`   2

There we go! Is that a typical BFS/DFS problem? Yes, you are right!
Be careful to check whether a character is a digit or a letter(lower case or upper case).
`BFS Solution:`
```
class Solution {
    public List<String> letterCasePermutation(String S) {
        if (S == null) {
            return new LinkedList<>();
        }
        Queue<String> queue = new LinkedList<>();
        queue.offer(S);
        
        for (int i = 0; i < S.length(); i++) {
            if (Character.isDigit(S.charAt(i))) continue;            
            int size = queue.size();
            for (int j = 0; j < size; j++) {
                String cur = queue.poll();
                char[] chs = cur.toCharArray();
                
                chs[i] = Character.toUpperCase(chs[i]);
                queue.offer(String.valueOf(chs));
                
                chs[i] = Character.toLowerCase(chs[i]);
                queue.offer(String.valueOf(chs));
            }
        }
        
        return new LinkedList<>(queue);
    }
}
```

`DFS Solution:`
```
class Solution {
    public List<String> letterCasePermutation(String S) {
        if (S == null) {
            return new LinkedList<>();
        }
        
        List<String> res = new LinkedList<>();
        helper(S.toCharArray(), res, 0);
        return res;
    }
    
    public void helper(char[] chs, List<String> res, int pos) {
        if (pos == chs.length) {
            res.add(new String(chs));
            return;
        }
        if (chs[pos] >= \'0\' && chs[pos] <= \'9\') {
            helper(chs, res, pos + 1);
            return;
        }
        
        chs[pos] = Character.toLowerCase(chs[pos]);
        helper(chs, res, pos + 1);
        
        chs[pos] = Character.toUpperCase(chs[pos]);
        helper(chs, res, pos + 1);
    }
}
```
</p>


### Python simple solution (7 lines)
- Author: blehart
- Creation Date: Sun Feb 18 2018 15:56:10 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 08:19:29 GMT+0800 (Singapore Standard Time)

<p>
```
def letterCasePermutation(self, S):
        res = ['']
        for ch in S:
            if ch.isalpha():
                res = [i+j for i in res for j in [ch.upper(), ch.lower()]]
            else:
                res = [i+ch for i in res]
        return res
```
</p>


### C++ backtrack solution w/ trick
- Author: tarekd
- Creation Date: Sun Feb 18 2018 16:22:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 22 2018 04:26:31 GMT+0800 (Singapore Standard Time)

<p>
Straightforward recursive solution. Simple trick with case toggling.
Thanks @shiowen for noticing redundant lines.
```
class Solution {
    void backtrack(string &s, int i, vector<string> &res) {
        if (i == s.size()) {
            res.push_back(s);
            return;
        }
        backtrack(s, i + 1, res);
        if (isalpha(s[i])) {
            // toggle case
            s[i] ^= (1 << 5);
            backtrack(s, i + 1, res);
        }
    }
public:
    vector<string> letterCasePermutation(string S) {
        vector<string> res;
        backtrack(S, 0, res);
        return res;
    }
};
```
</p>


