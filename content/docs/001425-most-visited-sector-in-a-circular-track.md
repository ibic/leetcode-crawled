---
title: "Most Visited Sector in  a Circular Track"
weight: 1425
#id: "most-visited-sector-in-a-circular-track"
---
## Description
<div class="description">
<p>Given an integer <code>n</code> and an integer array <code>rounds</code>.&nbsp;We&nbsp;have a circular track which consists of <code>n</code> sectors labeled from <code>1</code> to <code>n</code>. A marathon will be held on this track, the marathon consists of <code>m</code> rounds. The <code>i<sup>th</sup></code>&nbsp;round starts at sector <code>rounds[i - 1]</code> and ends at sector <code>rounds[i]</code>. For example, round 1 starts at sector <code>rounds[0]</code> and ends at sector <code>rounds[1]</code></p>

<p>Return <em>an array of the most visited sectors</em> sorted in <strong>ascending</strong> order.</p>

<p>Notice that you&nbsp;circulate the track in ascending order of sector numbers in the counter-clockwise direction (See the first example).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/14/tmp.jpg" style="width: 433px; height: 341px;" />
<pre>
<strong>Input:</strong> n = 4, rounds = [1,3,1,2]
<strong>Output:</strong> [1,2]
<strong>Explanation:</strong> The marathon starts at sector 1. The order of the visited sectors is as follows:
1 --&gt; 2 --&gt; 3 (end of round 1) --&gt; 4 --&gt; 1 (end of round 2) --&gt; 2 (end of round 3 and the marathon)
We can see that both sectors 1 and 2 are visited twice and they are the most visited sectors. Sectors 3 and 4 are visited only once.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2, rounds = [2,1,2,1,2,1,2,1,2]
<strong>Output:</strong> [2]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 7, rounds = [1,3,5,7]
<strong>Output:</strong> [1,2,3,4,5,6,7]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 100</code></li>
	<li><code>1 &lt;= m &lt;= 100</code></li>
	<li><code>rounds.length == m + 1</code></li>
	<li><code>1 &lt;= rounds[i] &lt;= n</code></li>
	<li><code>rounds[i] != rounds[i + 1]</code> for <code>0 &lt;= i &lt; m</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Expedia - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] From Start to End
- Author: lee215
- Creation Date: Sun Aug 23 2020 12:06:42 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 25 2020 16:10:00 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
We only need to care the start point and the end point.
```
                s ----- n
1 --------------------- n
1 --------------------- n
1 ----- e
```
<br>

## **Explanation**
If start <= end, return the range [start, end].
If end < start, return the range [1, end] + range [start, n].
<br>

## **Complexity**
Time `O(N)`
Space `O(N)`
<br>

**Java:**
```java
    public List<Integer> mostVisited(int n, int[] A) {
        List<Integer> res = new ArrayList<>();
        for (int i = A[0]; i <= A[A.length - 1]; ++i)
            res.add(i);
        if (res.size() > 0) return res;
        for (int i = 1; i <= A[A.length - 1]; ++i)
            res.add(i);
        for (int i = A[0]; i <= n; ++i)
            res.add(i);
        return res;
    }
```

**C++:**
```cpp
    vector<int> mostVisited(int n, vector<int>& A) {
        vector<int> res;
        for (int i = A[0]; i <= A[A.size() - 1]; ++i)
            res.push_back(i);
        if (res.size() > 0) return res;
        for (int i = 1; i <= A[A.size() - 1]; ++i)
            res.push_back(i);
        for (int i = A[0]; i <= n; ++i)
            res.push_back(i);
        return res;
    }
```

**Python:**
```py
    def mostVisited(self, n, A):
        return range(A[0], A[-1] + 1) or range(1, A[-1] + 1) + range(A[0], n + 1)
```

</p>


### Java check first and last
- Author: hobiter
- Creation Date: Sun Aug 23 2020 12:00:41 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 25 2020 07:31:17 GMT+0800 (Singapore Standard Time)

<p>
Like a clock, check first and last
pay attention if count cross a circle;

details of thoughts: let fr = arr[0] (first), to = arr[m -1] (last);
1, when fr <= to, cnt for each node [..., n,  n+1(fr), n+1, ..., n + 1(to), n, n, ...]
2, when fr > to, cnt for each node [n+1, n+1, ..., n + 1(to), n, ... , n,  n+1(fr), n+1, ..., n + 1]
3, You may find that you don\'t worry about the value of n, n could be 0, 1, 2, 3, ..., whatever it is won\'t influence the result. You just need to check whether it is model 1 or 2 based on fr and to.
```
    public List<Integer> mostVisited(int n, int[] rounds) {
        int len = rounds.length, fr = rounds[0], to = rounds[len - 1];
        List<Integer> res = new ArrayList<>();
        if (to >= fr) {     // no circle, such as [1,3,1,2]
            for (int i = fr; i <= to; i++) res.add(i);
        } else {            // cross a circle, such as [2,3,2,1]
            for (int i = 1; i <= n; i++) {
                if (i == to + 1) i = fr;
                res.add(i);
            }
        }
        return res;
    }
```
</p>


### [C++] Solution Explained First and Last Element
- Author: priys
- Creation Date: Sun Aug 23 2020 20:30:47 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 02 2020 05:21:12 GMT+0800 (Singapore Standard Time)

<p>
There are 2 cases:

##### 1.   if start <= end
**Example 1: **
 ```
 n = 7  and  array = [1, 3, 5, 1, 2, 3]
				1  2  3  4  5  6  7              --1st circle
				1  2  3                              --2nd circle
```
			Note: in this case ans will be [1, 2, 3] which are being repeated twice. i. e. range array[start] to array[end]
			ans = [1,2,3]
**Example 2:**
```
			n = 2 and array = [2, 1, 2, 1, 2, 1, 2]
				2
				1  2
				1  2
				1  2
```
			Note: array[start] == array[end] so the ans is [2]
			ans = [2]

##### 2. if start > end
**Example 1**
```
			n = 2 and array = [2, 1, 2, 1, 2, 1]
					 2
				 1  2
				 1  2
				 1
```
			Note: ans = [1 to array[end], array[start] to n]	in this case 1 to array[end]: [1]
			and array[start] to n: [2]
			ans = [1,2]
**Example 2**
```			n = 5 and array = [3, 1, 3, 1]
						 3  4  5
				 1  2  3  4  5
				 1
```
			 Note: ans = 1 to array[end]  --->   [1] +
			array[start] to n ---->  [3, 4, 5]					
			ans = [1, 3, 4, 5]


**Solution:**
```
class Solution {
public:
    vector<int> mostVisited(int n, vector<int>& rounds) {
        vector <int> ans;
        int size = rounds.size();
        
        if(rounds[0] <= rounds[size-1]) {
            for(int i=rounds[0]; i<= rounds[size-1]; i++) {
                ans.push_back(i);
            }
            return ans;
        }
        else {
            for(int i=1; i<= rounds[size-1]; i++) {
                ans.push_back(i);
            }   
            
            for(int i=rounds[0]; i<=n; i++) {
                ans.push_back(i);
            }
        }
        
        return ans;
    }
};
```
		
</p>


