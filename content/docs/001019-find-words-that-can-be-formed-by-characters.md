---
title: "Find Words That Can Be Formed by Characters"
weight: 1019
#id: "find-words-that-can-be-formed-by-characters"
---
## Description
<div class="description">
<p>You are given an array of strings&nbsp;<code>words</code>&nbsp;and a string&nbsp;<code>chars</code>.</p>

<p>A string is <em>good</em>&nbsp;if&nbsp;it can be formed by&nbsp;characters from <code>chars</code>&nbsp;(each character&nbsp;can only be used once).</p>

<p>Return the sum of lengths of all good strings in <code>words</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>words = <span id="example-input-1-1">[&quot;cat&quot;,&quot;bt&quot;,&quot;hat&quot;,&quot;tree&quot;]</span>, chars = <span id="example-input-1-2">&quot;atach&quot;</span>
<strong>Output: </strong><span id="example-output-1">6</span>
<strong>Explanation: </strong>
The strings that can be formed are &quot;cat&quot; and &quot;hat&quot; so the answer is 3 + 3 = 6.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>words = <span id="example-input-2-1">[&quot;hello&quot;,&quot;world&quot;,&quot;leetcode&quot;]</span>, chars = <span id="example-input-2-2">&quot;welldonehoneyr&quot;</span>
<strong>Output: </strong><span id="example-output-2">10</span>
<strong>Explanation: </strong>
The strings that can be formed are &quot;hello&quot; and &quot;world&quot; so the answer is 5 + 5 = 10.
</pre>

<p>&nbsp;</p>

<p><span><strong>Note:</strong></span></p>

<ol>
	<li><code>1 &lt;= words.length &lt;= 1000</code></li>
	<li><code>1 &lt;= words[i].length, chars.length&nbsp;&lt;= 100</code></li>
	<li>All strings contain lowercase English letters only.</li>
</ol>
</div>

## Tags
- Array (array)
- Hash Table (hash-table)

## Companies
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (java)
```java
class Solution {
    private Map<Character, Integer> convertStringToSet(String words) {
        char[] charArray = words.toCharArray();
        Map<Character, Integer> charMap = new HashMap<>();
        for (char ch: charArray) {
            if (charMap.containsKey(ch)) {
                charMap.put(ch, charMap.get(ch) + 1);
            } else {
                charMap.put(ch, 1);
            }
        }
        return charMap;
    }
    public int countCharacters(String[] words, String chars) {
        int r = 0;
        Map<Character, Integer> charsMap = convertStringToSet(chars);
        for (String word: words) {
            boolean has = true;
            Map<Character, Integer> wordMap = convertStringToSet(word);
            for (Character wk: wordMap.keySet()) {
                if (!charsMap.containsKey(wk) || charsMap.get(wk) < wordMap.get(wk)) {
                    has = false;
                    break;
                }
            }
            if (has) {
                r += word.length();
            }
        }
        return r;
    }
}
```

## Top Discussions
### C++ Track Count
- Author: votrubac
- Creation Date: Sun Aug 18 2019 12:04:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 18 2019 12:04:21 GMT+0800 (Singapore Standard Time)

<p>
```
int countCharacters(vector<string>& words, string chars, int res = 0) {
  vector<int> cnt(26);
  for (auto ch : chars) ++cnt[ch - \'a\'];
  for (auto w : words) {
    vector<int> cnt1 = cnt;
    bool match = true;
    for (auto ch : w) {
      if (--cnt1[ch - \'a\'] < 0) {
        match = false;
        break;
      }
    }
    if (match) res += w.size();
  }
  return res;
}
```
</p>


### [Easy Explained] Simple Java Check all char count
- Author: JatinYadav96
- Creation Date: Sun Aug 18 2019 12:07:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 18 2019 12:07:16 GMT+0800 (Singapore Standard Time)

<p>
```

    public static int countCharacters(String[] words, String chars) {
        int count = 0;
        int[] seen = new int[26];
		//Count char of Chars String
        for (char c : chars.toCharArray())
            seen[c - \'a\']++;
		// Comparing each word in words
        for (String word : words) {
			// simple making copy of seen arr
            int[] tSeen = Arrays.copyOf(seen, seen.length); 
            int Tcount = 0;
            for (char c : word.toCharArray()) {
                if (tSeen[c - \'a\'] > 0) {
                    tSeen[c - \'a\']--;
                    Tcount++;
                }
            }
            if (Tcount == word.length())
                count += Tcount;
        }
        return count;
    }

```
</p>


### [Java/Python 3] Count chars in each word, .
- Author: rock
- Creation Date: Sun Aug 18 2019 12:02:02 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 28 2020 13:57:17 GMT+0800 (Singapore Standard Time)

<p>
Count the characters in `chars` in array `cnt`, then for each word in `words` check if `cnt` has enough to cover it.
```
    public int countCharacters(String[] words, String chars) {
        int cnt[] = new int[26], ans = 0;
        chars.chars().forEach(c -> ++cnt[c - \'a\']); // count chars.
outer: 
        for (String w : words) {
            int[] count = cnt.clone();
            for (char c : w.toCharArray())
                if (--count[c - \'a\'] < 0) // not enough, continue next word in words.
                    continue outer;
            ans += w.length();
        }
        return ans;
    }
```
```
    def countCharacters(self, words: List[str], chars: str) -> int:
        sum, chars_counter = 0, collections.Counter(chars)
        for word in words:
            word_counter = collections.Counter(word)
            for c in word_counter:
                if word_counter[c] > chars_counter[c]:
                    break
            else:
                sum += len(word)
        return sum
```
Similar version without using `for - else`.
```python
    def countCharacters(self, words: List[str], chars: str) -> int:
        sum, ct = 0, collections.Counter
        chars_counter = ct(chars)
        for word in words:
            word_counter = ct(word)
            if all(word_counter[c] <= chars_counter[c] for c in word_counter):
                sum += len(word)
        return sum
```
**Analysis:**

Time & space: O(n), n is total characters in `words` and `chars`
</p>


