---
title: "Maximum Size Subarray Sum Equals k"
weight: 308
#id: "maximum-size-subarray-sum-equals-k"
---
## Description
<div class="description">
<p>Given an array <i>nums</i> and a target value <i>k</i>, find the maximum length of a subarray that sums to <i>k</i>. If there isn&#39;t one, return 0 instead.</p>

<p><b>Note:</b><br />
The sum of the entire <i>nums</i> array is guaranteed to fit within the 32-bit signed integer range.</p>

<p><b>Example 1:</b></p>

<pre>
<strong>Input: </strong><i>nums</i> = <code>[1, -1, 5, -2, 3]</code>, <i>k</i> = <code>3</code>
<strong>Output: </strong>4 
<strong>Explanation: </strong>The subarray <code>[1, -1, 5, -2]</code> sums to 3 and is the longest.
</pre>

<p><b>Example 2:</b></p>

<pre>
<strong>Input: </strong><i>nums</i> = <code>[-2, -1, 2, 1]</code>, <i>k</i> = <code>1</code>
<strong>Output: </strong>2 <strong>
Explanation: </strong>The subarray <code>[-1, 2]</code> sums to 1 and is the longest.</pre>

<p><b>Follow Up:</b><br />
Can you do it in O(<i>n</i>) time?</p>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Facebook - 3 (taggedByAdmin: true)
- Google - 5 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Palantir Technologies - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### O(n) super clean 9-line Java solution with HashMap
- Author: vivaTM
- Creation Date: Tue Jan 05 2016 12:54:34 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 09:43:03 GMT+0800 (Singapore Standard Time)

<p>
    public int maxSubArrayLen(int[] nums, int k) {
        int sum = 0, max = 0;
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; i++) {
            sum = sum + nums[i];
            if (sum == k) max = i + 1;
            else if (map.containsKey(sum - k)) max = Math.max(max, i - map.get(sum - k));
            if (!map.containsKey(sum)) map.put(sum, i);
        }
        return max;
    }

The HashMap stores the sum of all elements before index i as key, and i as value. For each i, check not only the current sum but also (currentSum - previousSum) to see if there is any that equals k, and update max length.

PS: An "else" is added. Thanks to beckychiu1988 for comment.
</p>


### Java O(n) explain how I come up with this idea
- Author: xuyirui
- Creation Date: Fri Jan 08 2016 12:30:23 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 01:58:16 GMT+0800 (Singapore Standard Time)

<p>
The subarray sum reminds me the range sum problem. Preprocess the input array such that you get
the range sum in constant time.
sum[i] means the sum from 0 to i inclusively
the sum from i to j is sum[j] -  sum[i - 1] except that from 0 to j is sum[j].

j-i is equal to the length of subarray of original array. we want to find the max(j - i)
for any sum[j] we need to find if there is a previous sum[i] such that sum[j] - sum[i] = k
Instead of scanning from 0 to j -1 to find such i, we use hashmap to do the job in constant time.
However, there might be duplicate value of of sum[i] we should avoid overriding its index as we want the max j - i, so we want to keep i as left as possible.

    public class Solution {
        public int maxSubArrayLen(int[] nums, int k) {
            if (nums == null || nums.length == 0)
                return 0;
            int n = nums.length;
            for (int i = 1; i < n; i++)
                nums[i] += nums[i - 1];
            Map<Integer, Integer> map = new HashMap<>();
            map.put(0, -1); // add this fake entry to make sum from 0 to j consistent
            int max = 0;
            for (int i = 0; i < n; i++) {
                if (map.containsKey(nums[i] - k))
                    max = Math.max(max, i - map.get(nums[i] - k));
                if (!map.containsKey(nums[i])) // keep only 1st duplicate as we want first index as left as possible
                    map.put(nums[i], i);
            }
            return max;
        }
    }
</p>


### Clean python solution, one pass
- Author: cbmbbz
- Creation Date: Wed Jan 06 2016 09:03:10 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 12:20:40 GMT+0800 (Singapore Standard Time)

<p>
Revised code, 84ms, thanks Stefan !

    def maxSubArrayLen(self, nums, k):
        ans, acc = 0, 0               # answer and the accumulative value of nums
        mp = {0:-1}                 #key is acc value, and value is the index
        for i in xrange(len(nums)):
            acc += nums[i]
            if acc not in mp:
                mp[acc] = i 
            if acc-k in mp:
                ans = max(ans, i-mp[acc-k])
        return ans
</p>


