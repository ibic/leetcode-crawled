---
title: "Maximum Performance of a Team"
weight: 1285
#id: "maximum-performance-of-a-team"
---
## Description
<div class="description">
<p>There are <code>n</code> engineers numbered from 1 to <code>n</code>&nbsp;and&nbsp;two arrays: <code>speed</code>&nbsp;and <code>efficiency</code>, where <code>speed[i]</code> and <code>efficiency[i]</code> represent the speed and efficiency for the i-th engineer respectively. <em>Return the maximum <strong>performance</strong> of a team composed of&nbsp;at most&nbsp;<code>k</code>&nbsp;engineers, since the answer can be a huge number, return this modulo&nbsp;10^9 + 7.</em></p>

<p>The <strong>performance</strong> of a team is the sum of their engineers&#39; speeds multiplied by the minimum efficiency among&nbsp;their engineers.&nbsp;</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 6, speed = [2,10,3,1,5,8], efficiency = [5,4,3,9,7,2], k = 2
<strong>Output:</strong> 60
<strong>Explanation:</strong> 
We have the maximum performance of the team by selecting engineer 2 (with speed=10 and efficiency=4) and engineer 5 (with speed=5 and efficiency=7). That is, performance = (10 + 5) * min(4, 7) = 60.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 6, speed = [2,10,3,1,5,8], efficiency = [5,4,3,9,7,2], k = 3
<strong>Output:</strong> 68
<strong>Explanation:
</strong>This is the same example as the first but k = 3. We can select engineer 1, engineer 2 and engineer 5 to get the maximum performance of the team. That is, performance = (2 + 10 + 5) * min(5, 4, 7) = 68.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 6, speed = [2,10,3,1,5,8], efficiency = [5,4,3,9,7,2], k = 4
<strong>Output:</strong> 72
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
	<li><code>speed.length == n</code></li>
	<li><code>efficiency.length == n</code></li>
	<li><code>1 &lt;= speed[i] &lt;= 10^5</code></li>
	<li><code>1 &lt;= efficiency[i] &lt;= 10^8</code></li>
	<li><code>1 &lt;= k &lt;= n</code></li>
</ul>
</div>

## Tags
- Greedy (greedy)
- Sort (sort)

## Companies
- Citrix - 4 (taggedByAdmin: true)
- DoorDash - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Python] Priority Queue
- Author: insane_reckless
- Creation Date: Sun Mar 15 2020 12:11:32 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 16 2020 20:55:56 GMT+0800 (Singapore Standard Time)

<p>
(This problem is very similar to the problem **857 Minimum Cost to Hire K Workers** https://leetcode.com/problems/minimum-cost-to-hire-k-workers/. Employ priority queue to get the O(NlogN) solution. You can just check it.)
Each engineer is characterized by his/her speed and efficiency. 
**Step1.** We sort engineers by their efficiency in decreasing order. **O(NlogN)**
**Step2.** With the sorted array, for index i = 0, 1, ..., k-1, we push the speed into the min_heap and calculate the performance with first (i+1) engineers. We only need to record the max performance. 
**Step3.** With the sorted array, for index i = k, k+1, ..., n-1, if the speed of the current engineer is greater than the top of the min_heap, we pop and push with it. In this way, we can calculate the max performance with respect to the i-th largest efficiency since we have the engineers with the k largest speeds in the min_heap. **O(NlogK)**

**C++**
```
class Solution {
public:
    int maxPerformance(int n, vector<int>& speed, vector<int>& efficiency, int k) {
        priority_queue<int, vector<int>, greater<int>> heap;
        vector<vector<int>> worker;
        vector<int> tmp(2,0);
        for (int i = 0; i < n; i++) {
            tmp[0] = speed[i];
            tmp[1] = efficiency[i];
            worker.push_back(tmp);
        }
        sort(worker.begin(), worker.end(), compare);
        long res = 0;
        long total = 0;
        long minE;
        for (int i = 0; i < k; i++) {
            total += worker[i][0];
            minE = worker[i][1];
            res = max(res, total*minE);
            heap.push(worker[i][0]);
        }
        for (int i = k; i < n; i++) {
            if (worker[i][0] > heap.top()) {
                total += (-heap.top()+worker[i][0]);
                minE = worker[i][1];
                res = max(res, total*minE);
                heap.pop();
                heap.push(worker[i][0]);
            }
        }
        return (int)(res%1000000007);
    }
    
    static bool compare(vector<int>& w1, vector<int>& w2) {
        return w1[1] > w2[1];
    }
};
```

**Python**
```
class Solution(object):
    def maxPerformance(self, n, speed, efficiency, k):
        worker = []
        for i in range(n):
            worker.append([speed[i], efficiency[i]])
        worker.sort(cmp = self.cmp)
        import heapq
        total = 0
        heap = []
        res = 0
        for i in range(k):
            total += worker[i][0]
            minE = worker[i][1]
            heapq.heappush(heap, worker[i][0])
            res = max(res, total*minE)
        for i in range(k, n):
            if worker[i][0] > heap[0]:
                total += (-heap[0]+worker[i][0])
                minE = worker[i][1]
                res = max(res, minE*total)
                heapq.heappop(heap)
                heapq.heappush(heap, worker[i][0])
        return res%1000000007
        
    def cmp(self, w1, w2):
        if w1[1] > w2[1]:
            return -1
        elif w1[1] == w2[1]:
            return 0
        else:
            return 1
```
</p>


### [Java/C++/Python] Priority Queue
- Author: lee215
- Creation Date: Sun Mar 15 2020 12:01:06 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 17 2020 01:46:08 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Doesn\'t efficiency and speed are the same thing?
Like efficiency = speed = produce / time?
Speed sounds more like time actually.
You work slow, so you need more time.

@Msylcatam reminded me this problem [857. Minimum Cost to Hire K Workers](https://leetcode.com/problems/minimum-cost-to-hire-k-workers/discuss/141768/Detailed-explanation-O(NlogN))
for your extra exercise as revision.

Hungry and out for meal.
For C++, may you refer to 857, exact the same template.

Special period, take care of yourself.
Wash your hand, touch the keyboard instead of your face.
Consider of putting a mask if necessary.
Very important, mute tv and twitter of **t**er**r**ibl**e** speech.
<br>

# Solution 1: Binary Insert
We hire the team from the most efficent people to less.
The current iterated engineer has the smallest `efficency` in the team.
The performance of a team = `efficency[i] * sumSpeed`

Each time we have a new engineer,
though will reduce the efficency of the team,
it may increment the sum of speed.

If the size of team is bigger than `k`,
we have to layoff the smallest `speed`.
Then we update the result.

Note we should try better team work.
But there is the chance that less engineers
and achieve greater performance than `k` engineers.
If you have `efficient = speed = 100`,
other engineer have nothing,
you should be independent engineer and be your own boss.
 (I didn\'t test if LeetCode include this case.)

We keep our engineers in order of their `speed`,
and we insert the new engineer inside the list as per its `speed`.

<br>

# Complexity
Time `O(N^2)`, since insert take `O(N)`
Space `O(N)`
<br>

**Python:**
```py
    def maxPerformance(self, n, speed, efficiency, k):
        h = []
        res = sSum = 0
        for e,s in sorted(zip(efficiency, speed), reverse=1):
            bisect.insort(h, -s)
            sSum += s
            if len(h) > k:
                sSum += h.pop()
            res = max(res, sSum * e)
        return res % (10**9+7)
```
<br>

# Solution 2: Priority Queue
We keep the `queue` with maximum size of `k`.
Each time when we introduce a new engineer,
we need only `O(logK)` to find the smallest `speed` in the team now.
<br>

# Complexity
Time `O(NlogN)` for sorting
Time `O(NlogK)` for priority queue
Space `O(N)`
<br>

**Java**
```Java
    public int maxPerformance(int n, int[] speed, int[] efficiency, int k) {
        int[][] ess = new int[n][2];
        for (int i = 0; i < n; ++i)
            ess[i] = new int[] {efficiency[i], speed[i]};
        Arrays.sort(ess, (a, b) -> b[0] - a[0]);
        PriorityQueue<Integer> pq = new PriorityQueue<>(k, (a, b) -> a - b);
        long res = 0, sumS = 0;
        for (int[] es : ess) {
            pq.add(es[1]);
            sumS = (sumS + es[1]);
            if (pq.size() > k) sumS -= pq.poll();
            res = Math.max(res, (sumS * es[0]));
        }
        return (int) (res % (long)(1e9 + 7));
    }
```
**C++**
```cpp
    int maxPerformance(int n, vector<int>& speed, vector<int>& efficiency, int k) {
        vector<pair<int, int>> ess;
        for (int i = 0; i < n; ++i)
            ess.emplace_back(efficiency[i], speed[i]);
        sort(rbegin(ess), rend(ess));
        long sumS = 0, res = 0;
        priority_queue <int, vector<int>, greater<int>> pq; //min heap
        for(auto& [e, s]: ess){
            pq.emplace(s);
            sumS += s;
            if (pq.size() > k) {
                sumS -= pq.top();
                pq.pop();
            }
            res = max(res, sumS * e);
        }
        return res % (int)(1e9+7);
    }
```
**Python:**
```py
    def maxPerformance(self, n, speed, efficiency, k):
        h = []
        res = sSum = 0
        for e, s in sorted(zip(efficiency, speed), reverse=1):
            heapq.heappush(h, s)
            sSum += s
            if len(h) > k:
                sSum -= heapq.heappop(h)
            res = max(res, sSum * e)
        return res % (10**9 + 7)
```
<br>


</p>


### [Java] Detailed Explanation - PriorityQueue - O(NlogN)
- Author: frankkkkk
- Creation Date: Sun Mar 15 2020 12:00:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 16 2020 07:31:06 GMT+0800 (Singapore Standard Time)

<p>
Same ideal with [857. Minimum Cost to Hire K Workers](https://leetcode.com/problems/minimum-cost-to-hire-k-workers/).

Performance = sum(speed) * min(efficiency). **Idea is simple: try every efficiency value from highest to lowest and at the same time maintain an as-large-as-possible speed group, keep adding speed to total speed,  if size of engineers group exceeds K, lay off the engineer with lowest speed.**

1. **Sort efficiency with descending order.** Because, afterwards, when we iterate whole engineers, every round, when calculating the current performance, minimum efficiency is the effiency of the new incoming engineer.
2. **Maintain a pq to track of the minimum speed in the group.** If size of group is == K, kick the engineer with minimum speed out (since efficiency is fixed by new coming engineer, the only thing matters now is sum of speed).
3. **Calculate/Update performance.**

```java
int MOD = (int) (1e9 + 7);
int[][] engineers = new int[n][2];
for (int i = 0; i < n; ++i) 
	engineers[i] = new int[] {efficiency[i], speed[i]};

Arrays.sort(engineers, (a, b) -> b[0] - a[0]);

PriorityQueue<Integer> pq = new PriorityQueue<>(k, (a, b) -> a - b);
long res = Long.MIN_VALUE, totalSpeed = 0;

for (int[] engineer : engineers) {
	if (pq.size() == k) totalSpeed -= pq.poll();  // layoff the one with min speed
	pq.add(engineer[1]);
	totalSpeed = (totalSpeed + engineer[1]);
	res = Math.max(res, (totalSpeed * engineer[0]));  // min efficiency is the efficiency of new engineer
}

return (int) (res % MOD);
```

</p>


