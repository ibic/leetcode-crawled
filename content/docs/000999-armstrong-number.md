---
title: "Armstrong Number"
weight: 999
#id: "armstrong-number"
---
## Description
<div class="description">
<p>The k-digit number <code>N</code> is an Armstrong number if and only if the k-th power of each digit sums to N.</p>

<p>Given a positive integer <code>N</code>, return true if and only if it is an Armstrong number.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">153</span>
<strong>Output: </strong><span id="example-output-1">true</span>
<strong>Explanation: </strong>
153 is a 3-digit number, and 153 = 1^3 + 5^3 + 3^3.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">123</span>
<strong>Output: </strong><span id="example-output-2">false</span>
<strong>Explanation: </strong>
123 is a 3-digit number, and 123 != 1^3 + 2^3 + 3^3 = 36.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= N &lt;= 10^8</code></li>
</ol>

</div>

## Tags
- Math (math)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### java 100%/100% w/ comments
- Author: carti
- Creation Date: Fri Oct 18 2019 13:56:23 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 18 2019 13:56:46 GMT+0800 (Singapore Standard Time)

<p>
```java
public boolean isArmstrong(int N) {
        //number of digits in N
        int k = (int) (Math.log10(N) + 1);
        //temporary variable (so we dont modify N)
        int x = N;
        //to hold sum
        int sum = 0;
        //get each digit
        while (x != 0) {
            //add this digit^k to sum
            sum += Math.pow(x % 10, k);
            //get next digit
            x /= 10;
        }
        return sum == N;
    }
```
</p>


### C++ 3 lines
- Author: votrubac
- Creation Date: Sun Jul 28 2019 00:26:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 28 2019 00:26:31 GMT+0800 (Singapore Standard Time)

<p>
```
bool isArmstrong(int N, int n = 0) {
  auto s = to_string(N);
  for (auto c : s) n += pow(c - \'0\', s.size());
  return n == N;
}
```
</p>


### Python - Calculate k with formula
- Author: nybblr
- Creation Date: Sun Aug 11 2019 16:34:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 11 2019 16:34:01 GMT+0800 (Singapore Standard Time)

<p>
```python
import math
class Solution(object):
    def isArmstrong(self, N):
        total = 0 # Keep track of the sum for each digit to the power of k
		
		# Calculate k
		# One method of calculating k is to convert N to a string and then get the length of that.
		# This second method is from this: https://brilliant.org/wiki/finding-digits-of-a-number/
		# It\'s a pretty useful and interesting formula.
        k = math.floor(math.log10(N)) + 1 
        
		# Create a copy of N so that we can compare the total to N without modifying its original value.
        c = N
        while c: # Terminates when c is 0
            digit = c % 10 # Extract the digit
            total += digit ** k 
            c //= 10
        
        return total == N
```
</p>


