---
title: "Reachable Nodes In Subdivided Graph"
weight: 832
#id: "reachable-nodes-in-subdivided-graph"
---
## Description
<div class="description">
<p>Starting with an&nbsp;<strong>undirected</strong> graph (the &quot;original graph&quot;) with nodes from <code>0</code> to <code>N-1</code>, subdivisions are made to some of the edges.</p>

<p>The graph is given as follows: <code>edges[k]</code> is a list of integer pairs <code>(i, j, n)</code> such that <code>(i, j)</code> is an edge of the original graph,</p>

<p>and <code>n</code> is the total number of <strong>new</strong> nodes on that edge.&nbsp;</p>

<p>Then, the edge <code>(i, j)</code> is deleted from the original graph,&nbsp;<code>n</code>&nbsp;new nodes <code>(x_1, x_2, ..., x_n)</code> are added to the original graph,</p>

<p>and <code>n+1</code> new&nbsp;edges <code>(i, x_1), (x_1, x_2), (x_2, x_3), ..., (x_{n-1}, x_n), (x_n, j)</code>&nbsp;are added to the original&nbsp;graph.</p>

<p>Now, you start at node <code>0</code>&nbsp;from the original graph, and in each move, you travel along one&nbsp;edge.&nbsp;</p>

<p>Return how many nodes you can reach in at most <code>M</code> moves.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><code>edges </code>= <span id="example-input-1-1">[[0,1,10],[0,2,1],[1,2,2]]</span>, M = <span id="example-input-1-2">6</span>, N = <span id="example-input-1-3">3</span>
<strong>Output: </strong><span id="example-output-1">13</span>
<strong>Explanation: </strong>
The nodes that are reachable in the final graph after M = 6 moves are indicated below.
<span><img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/08/01/origfinal.png" style="width: 487px; height: 200px;" /></span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><code>edges </code>= <span id="example-input-2-1">[[0,1,4],[1,2,6],[0,2,8],[1,3,1]]</span>, M = <span id="example-input-2-2">10</span>, N = <span id="example-input-2-3">4</span>
<strong>Output: </strong><span id="example-output-2">23</span></pre>

<p>&nbsp;</p>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= edges.length &lt;= 10000</code></li>
	<li><code>0 &lt;= edges[i][0] &lt;&nbsp;edges[i][1] &lt; N</code></li>
	<li>There does not exist any&nbsp;<code>i != j</code> for which <code>edges[i][0] == edges[j][0]</code> and <code>edges[i][1] == edges[j][1]</code>.</li>
	<li>The original graph&nbsp;has no parallel edges.</li>
	<li><code>0 &lt;= edges[i][2] &lt;= 10000</code></li>
	<li><code>0 &lt;= M &lt;= 10^9</code></li>
	<li><code><font face="monospace">1 &lt;= N &lt;= 3000</font></code></li>
	<li>A reachable node is a node that can be travelled to&nbsp;using at most&nbsp;M moves starting from&nbsp;node 0.</li>
</ol>

<div>
<div>&nbsp;</div>
</div>

</div>

## Tags
- Heap (heap)

## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach 1: Dijkstra's

**Intuition**

Treating the original graph as a weighted, undirected graph, we can use Dijkstra's algorithm to find all reachable nodes in the original graph.  However, this won't be enough to solve examples where subdivided edges are only used partially.

When we travel along an edge (in either direction), we can keep track of how much we use it.  At the end, we want to know every node we reached in the original graph, plus the sum of the utilization of each edge.

**Algorithm**

We use *Dijkstra's algorithm* to find the shortest distance from our source to all targets.  This is a textbook algorithm, refer to [this link](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm) for more details.

Additionally, for each (directed) edge `(node, nei)`, we'll keep track of how many "new" nodes (new from subdivision of the original edge) were `used`.  At the end, we'll sum up the utilization of each edge.

Please see the inline comments for more details.

<iframe src="https://leetcode.com/playground/Xb7eFEx3/shared" frameBorder="0" width="100%" height="500" name="Xb7eFEx3"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(E \log N)$$, where $$E$$ is the length of `edges`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Dijkstra Solution
- Author: wangzi6147
- Creation Date: Sun Aug 05 2018 11:32:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 09:32:25 GMT+0800 (Singapore Standard Time)

<p>
This seems like a standard Dijkstra problem. Remember to calculate move even cannot reach the next node.
```
class Solution {
    public int reachableNodes(int[][] edges, int M, int N) {
        int[][] graph = new int[N][N];
        for (int i = 0; i < N; i++) {
            Arrays.fill(graph[i], -1);
        }
        for (int[] edge : edges) {
            graph[edge[0]][edge[1]] = edge[2];
            graph[edge[1]][edge[0]] = edge[2];
        }
        int result = 0;
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> (b[1] - a[1]));
        boolean[] visited = new boolean[N];
        pq.offer(new int[]{0, M});
        while (!pq.isEmpty()) {
            int[] cur = pq.poll();
            int start = cur[0];
            int move = cur[1];
            if (visited[start]) {
                continue;
            }
            visited[start] = true;
            result++;
            for (int i = 0; i < N; i++) {
                if (graph[start][i] > -1) {
                    if (move > graph[start][i] && !visited[i]) {
                        pq.offer(new int[]{i, move - graph[start][i] - 1});
                    }
                    graph[i][start] -= Math.min(move, graph[start][i]);
                    result += Math.min(move, graph[start][i]);
                }
            }
        }
        return result;
    }
}
```
</p>


### [C++/Java/Python] Dijkstra + Priority Queue
- Author: lee215
- Creation Date: Sun Aug 05 2018 11:05:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 09:51:16 GMT+0800 (Singapore Standard Time)

<p>

**Intuition**:
Store `edges` to another 2D hashtable `e`, so that we can easier get length between two node by `e[i][j]`.
`seen[i]` means that we can arrive at node `i` and have `seen[i]` moves left.

We use a dijkstra algorithm in this solution.
Priority queue `pq` store states `(moves left, node index)`.
So every time when we pop from `pq`, we get the state with the most moves left.

In the end, we calculate the number of nodes that we can reach.

`res = seen.length`
For every edge `e[i][j]`:
`res += min(seen.getOrDefault(i, 0) + seen.getOrDefault(j, 0), e[i][j])`


**Time Complexity**:
Reminded by @kAc:
Dijkstra + Heap is O(E log E)
Dijkstra + Fibonacci heap is O(N log N + E)

**C++:**
```
    int reachableNodes(vector<vector<int>> edges, int M, int N) {
        unordered_map<int, unordered_map<int, int>> e;
        for (auto v : edges) e[v[0]][v[1]] = e[v[1]][v[0]] = v[2];
        priority_queue<pair<int, int>> pq;
        pq.push({M, 0});
        unordered_map<int, int> seen;
        while (pq.size()) {
            int moves = pq.top().first, i = pq.top().second;
            pq.pop();
            if (!seen.count(i)) {
                seen[i] = moves;
                for (auto j : e[i]) {
                    int moves2 = moves - j.second - 1;
                    if (!seen.count(j.first) && moves2 >= 0)
                        pq.push({ moves2, j.first});
                }
            }
        }
        int res = seen.size();
        for (auto v : edges) {
            int a = seen.find(v[0]) == seen.end() ? 0 : seen[v[0]];
            int b = seen.find(v[1]) == seen.end() ? 0 : seen[v[1]];
            res += min(a + b, v[2]);
        }
        return res;
    }
```
**Java:**
```
    public int reachableNodes(int[][] edges, int M, int N) {
        HashMap<Integer, HashMap<Integer, Integer>> e = new HashMap<>();
        for (int i = 0; i < N; ++i) e.put(i, new HashMap<>());
        for (int[] v : edges) {
            e.get(v[0]).put(v[1], v[2]);
            e.get(v[1]).put(v[0], v[2]);
        }
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> (b[0] - a[0]));
        pq.offer(new int[] {M, 0});
        HashMap<Integer, Integer> seen = new HashMap<>();
        while (!pq.isEmpty()) {
            int moves = pq.peek()[0], i = pq.peek()[1];
            pq.poll();
            if (!seen.containsKey(i)) {
                seen.put(i,moves);
                for (int j : e.get(i).keySet()) {
                    int moves2 = moves - e.get(i).get(j) - 1;
                    if (!seen.containsKey(j) && moves2 >= 0)
                        pq.offer(new int[] { moves2, j});
                }
            }
        }
        int res = seen.size();
        for (int[] v : edges) {
            int a = seen.getOrDefault(v[0],0);
            int b = seen.getOrDefault(v[1],0);
            res += Math.min(a + b, v[2]);
        }
        return res;
    }
```
**Python:**
```
    def reachableNodes(self, edges, M, N):
        e = collections.defaultdict(dict)
        for i, j, l in edges: e[i][j] = e[j][i] = l
        pq = [(-M, 0)]
        seen = {}
        while pq:
            moves, i = heapq.heappop(pq)
            if i not in seen:
                seen[i] = -moves
                for j in e[i]:
                    moves2 = -moves - e[i][j] - 1
                    if j not in seen and moves2 >= 0:
                        heapq.heappush(pq, (-moves2, j))
        res = len(seen)
        for i, j, k in edges:
            res += min(seen.get(i, 0) + seen.get(j, 0), e[i][j])
        return res
```
</p>


### Logical Thinking with Clear Code
- Author: GraceMeng
- Creation Date: Mon Aug 06 2018 21:39:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 09:40:14 GMT+0800 (Singapore Standard Time)

<p>
Thanks @wangzi6147 for the original post.

**Logical Thinking**
The problem is to get maximum number of nodes I can reach from node 0 within M moves. If I figure out each node\'s shortest distance to src, the one with distance <= M moves should be reachable. In this way, the problem can be regarded as a single-source shortest-path problem, which can be solved by **Dijkstra\'s Algorithm**.

Instead of maintaining a MinHeap which keeps track of shortest distances to the source, we maintain a **MaxHeap** that keeps track of maximum moves remained for each node. Since for a node,
```
moves remained + distance from current node to source = M
```
The bigger moves remained is, the smaller the distance will be. Thus, the MaxHeap can also promise the shortest distance.

We rebuild the graph to a weighted graph such that weight of an edge is total number of new nodes on that edge.


**Clear Code**
```
    public int reachableNodes(int[][] edges, int M, int N) {
        int[][] graph = new int[N][N];

        for (int[] tmp : graph) {
            Arrays.fill(tmp, -1);
        }
        for (int[] edge: edges) { // e.g. graph[0][1] = 4
            graph[edge[0]][edge[1]] = edge[2];
            graph[edge[1]][edge[0]] = edge[2];
        }
        int result = 0;
        
        boolean[] visited = new boolean[N];
        PriorityQueue<int[]> pq_node_movesRemained = new PriorityQueue<>((a, b) -> (b[1] - a[1]));
        pq_node_movesRemained.add(new int[] {0, M});
        
        while (!pq_node_movesRemained.isEmpty()) {
            int[] tmp = pq_node_movesRemained.poll();
            int node = tmp[0];
            int movesRemained = tmp[1];
            if (visited[node]) { 
                continue;
            }
            visited[node] = true;
            result++;
            for (int nei = 0; nei < N; nei++) {
                if (graph[node][nei] != -1) {
                    if (!visited[nei] && movesRemained >= graph[node][nei] + 1) {
                        pq_node_movesRemained.add(new int[]{nei, movesRemained - graph[node][nei] - 1});
                    }
                    int movesCost = Math.min(movesRemained, graph[node][nei]);
                    graph[nei][node] -= movesCost;
                    result += movesCost;
                }
            }
        }
        
        return result;
    }
```
**I would appreciate your VOTE UP ;)**
</p>


