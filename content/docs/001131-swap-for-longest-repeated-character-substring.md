---
title: "Swap For Longest Repeated Character Substring"
weight: 1131
#id: "swap-for-longest-repeated-character-substring"
---
## Description
<div class="description">
<p>Given a string <code>text</code>, we are allowed to swap two of the characters in the string. Find the length of the longest substring with repeated characters.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;ababa&quot;
<strong>Output:</strong> 3
<strong>Explanation:</strong> We can swap the first &#39;b&#39; with the last &#39;a&#39;, or the last &#39;b&#39; with the first &#39;a&#39;. Then, the longest repeated character substring is &quot;aaa&quot;, which its length is 3.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;aaabaaa&quot;
<strong>Output:</strong> 6
<strong>Explanation:</strong> Swap &#39;b&#39; with the last &#39;a&#39; (or the first &#39;a&#39;), and we get longest repeated character substring &quot;aaaaaa&quot;, which its length is 6.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;aaabbaaa&quot;
<strong>Output:</strong> 4
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;aaaaa&quot;
<strong>Output:</strong> 5
<strong>Explanation:</strong> No need to swap, longest repeated character substring is &quot;aaaaa&quot;, length is 5.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;abcdef&quot;
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= text.length &lt;= 20000</code></li>
	<li><code>text</code> consist of lowercase English characters only.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Microsoft - 4 (taggedByAdmin: false)
- Nutanix - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python Groupby
- Author: lee215
- Creation Date: Sun Aug 11 2019 12:04:15 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 13 2019 11:26:19 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
There are only 2 cases that we need to take care of:
- extend the group by 1
- merge 2 adjacent groups together, which are separated by only 1 character
<br>

## **Explanation**
For `S = "AAABBCB"`
`[[c, len(list(g))] for c, g in groupby(S)]` --> `[[A,3],[B,2],[C,1],[B,1]]`
`collections.Counter(S)` --> `{A:3, B:3, C:1}`
With these two data, calculate the best solution for the two cases above.
<br>

## Complexity
Time `O(N)`
Space `O(N)`
<br>

**Python:**
commented by @henry34
```python
    def maxRepOpt1(self, S):
        # We get the group\'s key and length first, e.g. \'aaabaaa\' -> [[a , 3], [b, 1], [a, 3]
        A = [[c, len(list(g))] for c, g in itertools.groupby(S)]
        # We also generate a count dict for easy look up e.g. \'aaabaaa\' -> {a: 6, b: 1}
        count = collections.Counter(S)
        # only extend 1 more, use min here to avoid the case that there\'s no extra char to extend
        res = max(min(k + 1, count[c]) for c, k in A)
        # merge 2 groups together
        for i in xrange(1, len(A) - 1):
            # if both sides have the same char and are separated by only 1 char
            if A[i - 1][0] == A[i + 1][0] and A[i][1] == 1:
                # min here serves the same purpose
                res = max(res, min(A[i - 1][1] + A[i + 1][1] + 1, count[A[i + 1][0]]))
        return res
```

</p>


### C++ 2 approaches
- Author: votrubac
- Creation Date: Sun Aug 11 2019 12:33:43 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 09 2019 13:07:24 GMT+0800 (Singapore Standard Time)

<p>
## Intuition
We need to find the longest substring with repeated character, which can contain no more than one different character. This can be done using a sliding window technique.
## Approach 1: Sliding Window
For each character ```ch``` within ```a..z```, use the sliding window to detect the longest substring (with up to one different character). 

Note that the result will be the minimum of the longest substring and the total number of characters ```ch```, since we may not have an extra character to swap and close the gap.
```
int maxRepOpt1(string str, int res = 0) {
  for (auto ch = \'a\'; ch <= \'z\'; ++ch) {
      int i = 0, j = 0, gap = 0;
      while (i < str.size()) {
          gap += str[i++] != ch;
          if (gap > 1) gap -= str[j++] != ch;
      }
      res = max(res, min(i - j, (int)count_if(begin(str), end(str), [&](char ch1) { return ch1 == ch; })));
  }
  return res;
}
```
### Complexity Analysis
Time: O(n * m), where m is the number of distinct elements (26 in this case).
Memory: O(1).
## Approach 2: Group and Count
Instead of going through the whole string 26 times, we first collect indexes for each character. Then we process each character and use its indexes to determine the longest substring.

In the solution below, we count the number of consecutive indices ```cnt```, and track the number of previous consecutive indices ```cnt1```. 
> Note that we set ```cnt1``` to zero if the gap is larger than one.

In the end, we\'ll get a max count of the repeated characters with no more than one-character gap. If we have more of that character somewhere in the string (```idx[n].size() > mx```), we add ```1``` for the swap operation. 
```
int maxRepOpt1(string text, int res = 1) {
  vector<vector<int>> idx(26);
  for (auto i = 0; i < text.size(); ++i) idx[text[i] - \'a\'].push_back(i);
  for (auto n = 0; n < 26; ++n) {
    auto cnt = 1, cnt1 = 0, mx = 0;
    for (auto i = 1; i < idx[n].size(); ++i) {
      if (idx[n][i] == idx[n][i - 1] + 1) ++cnt;
      else {
          cnt1 = idx[n][i] == idx[n][i - 1] + 2 ? cnt : 0;
          cnt = 1;
      }
      mx = max(mx, cnt1 + cnt);        
    }
    res = max(res, mx + (idx[n].size() > mx ? 1 : 0));
  }
  return res;
}
```
### Complexity Analysis
Time: O(n).
Memory: O(n) to store all indexes..
</p>


### Java solution with very detail explanation, O(n) Time, beat 100%
- Author: zhbmsqx
- Creation Date: Sun Aug 11 2019 12:10:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 11 2019 14:43:25 GMT+0800 (Singapore Standard Time)

<p>
The core algorithm is:
1. Identify the count for repeat characters
Let\'s say the example is aabaaba.
We should get some thing a-2, b-1, a-2, b-1, a-1.

2. Get the total count of one character.
take the same case for example, aabaaba, a total count = 5, b =2.
This will be used later.

3. The answer contains two scenarios.
// scenario 1, aaabba, find another a to replace b to increase maxlen+1; so result would be aaaabb. 
// scenario 2, aabaaba, firstly find the middle char b, length equals to 1, then make sure the left side and right side character are the same, then find if there is another a to replace b, update maxLen accordingly.

```
class Solution {
    public int maxRepOpt1(String text) {
        List<CharState> stateList = new ArrayList<CharState>();
        int startPos=0, endPos=0;
        char c = text.charAt(0);
        int i = 1;
        
        int[] charCount = new int[26];
        
        charCount[c-\'a\']++;
        
        while(i<text.length())
        {
            char cur = text.charAt(i);
            charCount[cur-\'a\']++;
            
            if(cur != c)
            {
                stateList.add(new CharState(c, startPos, endPos));
                
                c = cur;
                startPos = i;
                endPos = i;                
            }
            else
            {
                endPos = i;
            }
            
            i++;
        }
        
        stateList.add(new CharState(c, startPos, endPos));
        
        int maxLen = 1;
        // scenario 1, aaabba, find another a to replace b to increase maxlen+1;
        for(i=0;i<stateList.size();i++)
        {
            CharState cs = stateList.get(i);
            int len = cs.endPos-cs.startPos+1;
            
            if(len < charCount[cs.c-\'a\'])
            {
                len++;
            }
            
            maxLen = Math.max(maxLen, len);            
        }
        
        // scenario 2, aaabaa, find another a to replace b
        
        for(i=1;i<stateList.size()-1;i++)
        {
            CharState before = stateList.get(i-1);
            CharState cs = stateList.get(i);
            CharState after = stateList.get(i+1);
            
            int beforeLen = before.endPos-before.startPos+1;
            int afterLen = after.endPos-after.startPos+1;
            
            if(cs.startPos == cs.endPos && before.c == after.c)
            {
                int totalLen = beforeLen + afterLen;
                if(beforeLen + afterLen < charCount[before.c - \'a\'])
                {
                    totalLen++;
                }
                maxLen = Math.max(maxLen, totalLen);                
            }
        }
        
        return maxLen;
    }
    
    public class CharState
    {
        char c;
        int startPos;
        int endPos;
        
        public CharState(char ch, int s, int e)
        {
            c = ch;
            startPos = s;
            endPos = e;
        }
    }
}
```
</p>


