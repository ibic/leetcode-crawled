---
title: "Reformat The String"
weight: 1312
#id: "reformat-the-string"
---
## Description
<div class="description">
<p>Given alphanumeric string <code>s</code>. (<strong>Alphanumeric string</strong> is a string consisting of lowercase English letters and digits).</p>

<p>You have to find a permutation of&nbsp;the string where no letter is followed by another letter and no digit is followed by another digit. That is, no two adjacent characters have the same type.</p>

<p>Return <em>the reformatted string</em> or return <strong>an empty string</strong> if it is impossible to reformat the string.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;a0b1c2&quot;
<strong>Output:</strong> &quot;0a1b2c&quot;
<strong>Explanation:</strong> No two adjacent characters have the same type in &quot;0a1b2c&quot;. &quot;a0b1c2&quot;, &quot;0a1b2c&quot;, &quot;0c2a1b&quot; are also valid permutations.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;leetcode&quot;
<strong>Output:</strong> &quot;&quot;
<strong>Explanation:</strong> &quot;leetcode&quot; has only characters so we cannot separate them by digits.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1229857369&quot;
<strong>Output:</strong> &quot;&quot;
<strong>Explanation:</strong> &quot;1229857369&quot; has only digits so we cannot separate them by characters.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;covid2019&quot;
<strong>Output:</strong> &quot;c2o0v1i9d&quot;
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;ab123&quot;
<strong>Output:</strong> &quot;1a2b3&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 500</code></li>
	<li><code>s</code> consists of only lowercase English letters and/or digits.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ SIMPLE EASY WITH EXPLANATION
- Author: Lovedeep
- Creation Date: Wed Apr 22 2020 23:02:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 23 2020 11:15:43 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    string reformat(string s) {
        string a="",d="";
        // split string into alpha string and digit strings
        for(auto x:s)
            isalpha(x)?a.push_back(x):d.push_back(x);
  
        // if difference is more than 1, return "" since not possible to reformat
        if(abs(int(a.size()-d.size()))>1)
            return "";
        // merge the strings accordingly, starting with longer string
        bool alpha=a.size()>d.size();
        int i=0,j=0,k=0;
            while(i<s.size()){
                if(alpha){
                    s[i++]=a[j++];
                }
                else{
                    s[i++]=d[k++];
                }
                alpha=!alpha;
            }
        return s;
    }
};
```
</p>


### [Python] Simple solution
- Author: katapan
- Creation Date: Sun Apr 19 2020 12:37:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 12:37:46 GMT+0800 (Singapore Standard Time)

<p>
```
    def reformat(self, s: str) -> str:
        a, b = [], []
        for c in s:
            if \'a\' <= c <= \'z\':
                a.append(c)
            else:
                b.append(c)
        if len(a) < len(b):
            a, b = b, a
        if len(a) - len(b) >= 2:
            return \'\'
        ans = \'\'
        for i in range(len(a)+len(b)):
            if i % 2 == 0:
                ans += a[i//2]
            else:
                ans += b[i//2]
        return ans
```
</p>


### [Java] Use Character.isDigit()
- Author: pwaykar
- Creation Date: Sun Apr 19 2020 12:35:36 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 20 2020 13:18:00 GMT+0800 (Singapore Standard Time)

<p>
**Understand the problem**
1. Reformat the string with alternate digit and character, and vice versa.
2. Create 2 lists one for digits and the second one as characters
3. Traverse the string and segregate the items to reformat
4. Use a boolean to switch between character and digit and append all the items
```
public String reformat(String s) {
        
        if(s == null || s.length() <= 1) {
            return s;
        }
        List<Character> digits = new ArrayList<>(s.length());
        List<Character> characters = new ArrayList<>(s.length());
        // Check if it is a digit or character
        for (char ch : s.toCharArray()) {
            if (Character.isDigit(ch)) {
                digits.add(ch);
            } else  {
                characters.add(ch);
            }
        }
        // it is impossible to reformat the string.
        if(Math.abs(digits.size() - characters.size()) >= 2) return "";
        
        StringBuilder result = new StringBuilder();
        // boolean to decide if we should start with characters or digits
        boolean digitOrChar = (digits.size() >= characters.size()) ? true : false; 

       for (int i = 0; i < s.length(); i++) {
            if (digitOrChar) {
                if (digits.size() > 0) {
                    result.append(digits.remove(0));    
                }
            } else {
                if (characters.size() > 0) {
                    result.append(characters.remove(0));    
                }
            }
            digitOrChar = !digitOrChar;
        }
        return result.toString();
    }
```
</p>


