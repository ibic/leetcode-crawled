---
title: "Reformat Department Table"
weight: 1544
#id: "reformat-department-table"
---
## Description
<div class="description">
<p>Table: <code>Department</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| revenue       | int     |
| month         | varchar |
+---------------+---------+
(id, month) is the primary key of this table.
The table has information about the revenue of each department per month.
The month has values in [&quot;Jan&quot;,&quot;Feb&quot;,&quot;Mar&quot;,&quot;Apr&quot;,&quot;May&quot;,&quot;Jun&quot;,&quot;Jul&quot;,&quot;Aug&quot;,&quot;Sep&quot;,&quot;Oct&quot;,&quot;Nov&quot;,&quot;Dec&quot;].
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to reformat the table such that there is a department id column&nbsp;and a revenue column <strong>for each month</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
Department table:
+------+---------+-------+
| id   | revenue | month |
+------+---------+-------+
| 1    | 8000    | Jan   |
| 2    | 9000    | Jan   |
| 3    | 10000   | Feb   |
| 1    | 7000    | Feb   |
| 1    | 6000    | Mar   |
+------+---------+-------+

Result table:
+------+-------------+-------------+-------------+-----+-------------+
| id   | Jan_Revenue | Feb_Revenue | Mar_Revenue | ... | Dec_Revenue |
+------+-------------+-------------+-------------+-----+-------------+
| 1    | 8000        | 7000        | 6000        | ... | null        |
| 2    | 9000        | null        | null        | ... | null        |
| 3    | null        | 10000       | null        | ... | null        |
+------+-------------+-------------+-------------+-----+-------------+

Note that the result table has 13 columns (1 for the department id + 12 for the months).
</pre>

</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL/PostgreSQL solutions
- Author: quabouquet
- Creation Date: Sat Sep 07 2019 06:18:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 07 2019 06:18:41 GMT+0800 (Singapore Standard Time)

<p>
```
select id, 
	sum(case when month = \'jan\' then revenue else null end) as Jan_Revenue,
	sum(case when month = \'feb\' then revenue else null end) as Feb_Revenue,
	sum(case when month = \'mar\' then revenue else null end) as Mar_Revenue,
	sum(case when month = \'apr\' then revenue else null end) as Apr_Revenue,
	sum(case when month = \'may\' then revenue else null end) as May_Revenue,
	sum(case when month = \'jun\' then revenue else null end) as Jun_Revenue,
	sum(case when month = \'jul\' then revenue else null end) as Jul_Revenue,
	sum(case when month = \'aug\' then revenue else null end) as Aug_Revenue,
	sum(case when month = \'sep\' then revenue else null end) as Sep_Revenue,
	sum(case when month = \'oct\' then revenue else null end) as Oct_Revenue,
	sum(case when month = \'nov\' then revenue else null end) as Nov_Revenue,
	sum(case when month = \'dec\' then revenue else null end) as Dec_Revenue
from department
group by id
order by id
```
</p>


### MSSQL Multiple joins, GroupBy and Pivot table solutions
- Author: pogodin
- Creation Date: Mon Sep 16 2019 03:04:28 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 18 2019 13:33:34 GMT+0800 (Singapore Standard Time)

<p>
**Multiple left joins**
```
;WITH ids AS 
(
    SELECT DISTINCT id FROM Department
)
SELECT 
    ids.id, 
    d_jan.revenue AS Jan_Revenue,
    d_feb.revenue AS Feb_Revenue,
    d_mar.revenue AS Mar_Revenue,
    d_apr.revenue AS Apr_Revenue,
    d_may.revenue AS May_Revenue,
    d_jun.revenue AS Jun_Revenue,
    d_jul.revenue AS Jul_Revenue,
    d_aug.revenue AS Aug_Revenue,
    d_sep.revenue AS Sep_Revenue,
    d_oct.revenue AS Oct_Revenue,
    d_nov.revenue AS Nov_Revenue,
    d_dec.revenue AS Dec_Revenue
FROM ids
LEFT JOIN Department d_jan ON d_jan.id = ids.id AND d_jan.month = \'Jan\'
LEFT JOIN Department d_feb ON d_feb.id = ids.id AND d_feb.month = \'Feb\'
LEFT JOIN Department d_mar ON d_mar.id = ids.id AND d_mar.month = \'Mar\'
LEFT JOIN Department d_apr ON d_apr.id = ids.id AND d_apr.month = \'Apr\'
LEFT JOIN Department d_may ON d_may.id = ids.id AND d_may.month = \'May\'
LEFT JOIN Department d_jun ON d_jun.id = ids.id AND d_jun.month = \'Jun\'
LEFT JOIN Department d_jul ON d_jul.id = ids.id AND d_jul.month = \'Jul\'
LEFT JOIN Department d_aug ON d_aug.id = ids.id AND d_aug.month = \'Aug\'
LEFT JOIN Department d_sep ON d_sep.id = ids.id AND d_sep.month = \'Sep\'
LEFT JOIN Department d_oct ON d_oct.id = ids.id AND d_oct.month = \'Oct\'
LEFT JOIN Department d_nov ON d_nov.id = ids.id AND d_nov.month = \'Nov\'
LEFT JOIN Department d_dec ON d_dec.id = ids.id AND d_dec.month = \'Dec\'
```


**Group by solution**
```
SELECT 
    id, 
    MAX(CASE WHEN month = \'Jan\' THEN revenue ELSE null END) AS Jan_Revenue,
    MAX(CASE WHEN month = \'Feb\' THEN revenue ELSE null END) AS Feb_Revenue,
    MAX(CASE WHEN month = \'Mar\' THEN revenue ELSE null END) AS Mar_Revenue,
    MAX(CASE WHEN month = \'Apr\' THEN revenue ELSE null END) AS Apr_Revenue,
    MAX(CASE WHEN month = \'May\' THEN revenue ELSE null END) AS May_Revenue,
    MAX(CASE WHEN month = \'Jun\' THEN revenue ELSE null END) AS Jun_Revenue,
    MAX(CASE WHEN month = \'Jul\' THEN revenue ELSE null END) AS Jul_Revenue,
    MAX(CASE WHEN month = \'Aug\' THEN revenue ELSE null END) AS Aug_Revenue,
    MAX(CASE WHEN month = \'Sep\' THEN revenue ELSE null END) AS Sep_Revenue,
    MAX(CASE WHEN month = \'Oct\' THEN revenue ELSE null END) AS Oct_Revenue,
    MAX(CASE WHEN month = \'Nov\' THEN revenue ELSE null END) AS Nov_Revenue,
    MAX(CASE WHEN month = \'Dec\' THEN revenue ELSE null END) AS Dec_Revenue
FROM Department
GROUP BY id
```


**Pivot table solution**
```
SELECT 
    id,
    Jan AS Jan_Revenue,
    Feb AS Feb_Revenue, 
    Mar AS Mar_Revenue, 
    Apr AS Apr_Revenue,
    May AS May_Revenue,
    Jun AS Jun_Revenue,
    Jul AS Jul_Revenue,
    Aug AS Aug_Revenue,
    Sep AS Sep_Revenue,
    Oct AS Oct_Revenue,
    Nov AS Nov_Revenue,
    Dec AS Dec_Revenue
FROM Department
PIVOT 
(
    MAX(revenue)
    FOR month IN (Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec)        
) AS MonthsRevenue
```

</p>


### MySQL Solution with 381 ms, faster than 100.00%
- Author: furkankahvecii
- Creation Date: Sat Sep 07 2019 02:08:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 07 2019 02:08:32 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT 
    id, 
    sum( if( month = \'Jan\', revenue, null ) ) AS Jan_Revenue,
    sum( if( month = \'Feb\', revenue, null ) ) AS Feb_Revenue,
    sum( if( month = \'Mar\', revenue, null ) ) AS Mar_Revenue,
    sum( if( month = \'Apr\', revenue, null ) ) AS Apr_Revenue,
    sum( if( month = \'May\', revenue, null ) ) AS May_Revenue,
    sum( if( month = \'Jun\', revenue, null ) ) AS Jun_Revenue,
    sum( if( month = \'Jul\', revenue, null ) ) AS Jul_Revenue,
    sum( if( month = \'Aug\', revenue, null ) ) AS Aug_Revenue,
    sum( if( month = \'Sep\', revenue, null ) ) AS Sep_Revenue,
    sum( if( month = \'Oct\', revenue, null ) ) AS Oct_Revenue,
    sum( if( month = \'Nov\', revenue, null ) ) AS Nov_Revenue,
    sum( if( month = \'Dec\', revenue, null ) ) AS Dec_Revenue
FROM 
    Department
GROUP BY 
    id;
```
</p>


