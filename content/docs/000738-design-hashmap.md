---
title: "Design HashMap"
weight: 738
#id: "design-hashmap"
---
## Description
<div class="description">
<p>Design a HashMap&nbsp;without using any built-in hash table libraries.</p>

<p>To be specific, your design should include these functions:</p>

<ul>
	<li><code>put(key, value)</code> :&nbsp;Insert a (key, value) pair into the HashMap. If the value already exists in the HashMap, update the value.</li>
	<li><code>get(key)</code>: Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key.</li>
	<li><code>remove(key)</code> :&nbsp;Remove the mapping for the value key if this map contains the mapping for the key.</li>
</ul>

<p><br />
<strong>Example:</strong></p>

<pre>
MyHashMap hashMap = new MyHashMap();
hashMap.put(1, 1); &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
hashMap.put(2, 2); &nbsp; &nbsp; &nbsp; &nbsp; 
hashMap.get(1); &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;// returns 1
hashMap.get(3); &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;// returns -1 (not found)
hashMap.put(2, 1); &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;// update the existing value
hashMap.get(2); &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;// returns 1 
hashMap.remove(2); &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;// remove the mapping for 2
hashMap.get(2); &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;// returns -1 (not found) 
</pre>

<p><br />
<strong>Note:</strong></p>

<ul>
	<li>All keys and values will be in the range of <code>[0, 1000000]</code>.</li>
	<li>The number of operations will be in the range of&nbsp;<code>[1, 10000]</code>.</li>
	<li>Please do not use the built-in HashMap library.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Design (design)

## Companies
- Amazon - 8 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Twitter - 4 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Goldman Sachs - 3 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- VMware - 3 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Oracle - 7 (taggedByAdmin: false)
- Adobe - 4 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Tableau - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Dell - 2 (taggedByAdmin: false)
- Reddit - 4 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Intuition

Hashmap is a common data structure that is implemented in various forms in different programming languages, _e.g._ `dict` in Python and `HashMap` in Java. The most distinguish characteristic about hashmap is that it provides a fast access to a **value** that is associated with a given **key**.

>There are two main issues that we should tackle, in order to design an _efficient_ hashmap data structure: _1). hash function design_ and _2). collision handling_. 

- **1). hash function design**: the purpose of hash function is to map a key value to an address in the storage space, similarly to the system that we assign a postcode to each mail address.
As one can image, for a good hash function, it should map different keys **_evenly_** across the storage space, so that we don't end up with the case that the majority of the keys are _concentrated_ in a few spaces.

- **2). collision handling**: essentially the hash function reduces the vast key space into a limited address space. As a result, there could be the case where two different keys are mapped to the same address, which is what we call _'collision'_. 
Since the collision is inevitable, it is important that we have a strategy to handle the collision. 

Depending on how we deal with each of the above two issues, we could have various implementation of hashmap data structure. 
<br/>
<br/>

---
#### Approach 1: Modulo + Array

**Intuition**

As one of the most intuitive implementations, we could adopt the `modulo` operator as the hash function, since the key value is of integer type. In addition, in order to minimize the potential collisions, it is advisable to use a prime number as the base of modulo, _e.g._ `2069`.

We organize the storage space as an **array** where each element is indexed with the output value of the hash function.

In case of _collision_, where two different keys are mapped to the same address, we use a **bucket** to hold all the values. The bucket is a container that hold all the values that are assigned by the hash function. We could use either a `LinkedList` or an `Array` to implement the bucket data structure. 

**Algorithm**

For each of the methods in hashmap data structure, namely `get()`, `put()` and `remove()`, it all boils down to the method to locate the value that is stored in hashmap, given the key.

This localization process can be done in two steps:

- For a given `key` value, first we apply the hash function to generate a hash key, which corresponds to the address in our main storage. With this hash key, we would find the _bucket_ where the value should be stored. 

- Now that we found the bucket, we simply iterate through the bucket to check if the desired `<key, value>` pair does exist. 


![pic](../Figures/706/706_hashmap.png)

<iframe src="https://leetcode.com/playground/Poe3m4md/shared" frameBorder="0" width="100%" height="500" name="Poe3m4md"></iframe>

Note that in the above implementations, we use `Array` to implement the _bucket_ in Python, while we use `LinkedList` in Java.

**Complexity Analysis**

- Time Complexity: for each of the methods, the time complexity is $$\mathcal{O}(\frac{N}{K})$$ where $$N$$ is the number of all possible keys and $$K$$ is the number of predefined buckets in the hashmap, which is `2069` in our case.

    - In the ideal case, the keys are evenly distributed in all buckets. As a result, *on average*, we could consider the size of the bucket is $$\frac{N}{K}$$.

    - Since in the worst case we need to iterate through a bucket to find the desire value, the time complexity of each method is $$\mathcal{O}(\frac{N}{K})$$.

- Space Complexity: $$\mathcal{O}(K+M)$$ where $$K$$ is the number of predefined buckets in the hashmap and $$M$$ is the number of unique keys that have been inserted into the hashmap.
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution
- Author: climberig
- Creation Date: Mon Jul 23 2018 10:33:50 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 07:13:12 GMT+0800 (Singapore Standard Time)

<p>
Collisions are resolved using linked list
```java
 class MyHashMap {
        final ListNode[] nodes = new ListNode[10000];

        public void put(int key, int value) {
            int i = idx(key);
            if (nodes[i] == null)
                nodes[i] = new ListNode(-1, -1);
            ListNode prev = find(nodes[i], key);
            if (prev.next == null)
                prev.next = new ListNode(key, value);
            else prev.next.val = value;
        }

        public int get(int key) {
            int i = idx(key);
            if (nodes[i] == null)
                return -1;
            ListNode node = find(nodes[i], key);
            return node.next == null ? -1 : node.next.val;
        }

        public void remove(int key) {
            int i = idx(key);
            if (nodes[i] == null) return;
            ListNode prev = find(nodes[i], key);
            if (prev.next == null) return;
            prev.next = prev.next.next;
        }

        int idx(int key) { return Integer.hashCode(key) % nodes.length;}

        ListNode find(ListNode bucket, int key) {
            ListNode node = bucket, prev = null;
            while (node != null && node.key != key) {
                prev = node;
                node = node.next;
            }
            return prev;
        }

        class ListNode {
            int key, val;
            ListNode next;

            ListNode(int key, int val) {
                this.key = key;
                this.val = val;
            }
        }
    }
```
**11/03/2018 update:** remove ```Bucket``` class and rename ```final ListNode[] buckets = new ListNode[10000]``` to ```final ListNode[] nodes = new ListNode[10000]```. Thanks to https://leetcode.com/johnson9432
		
</p>


### Hash with Chaining [Python]
- Author: douzigege
- Creation Date: Thu Oct 25 2018 01:05:03 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 01:05:03 GMT+0800 (Singapore Standard Time)

<p>
I do not think array solutions are acceptable interviews. So here is my naive implementation of Hash with chaining. One can play with m (number of slots in HashTable) to optimize the runtime. I got 90% with setting m = 1000. 

```
# using just arrays, direct access table
# using linked list for chaining

class ListNode:
    def __init__(self, key, val):
        self.pair = (key, val)
        self.next = None

class MyHashMap:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.m = 1000;
        self.h = [None]*self.m
        

    def put(self, key, value):
        """
        value will always be non-negative.
        :type key: int
        :type value: int
        :rtype: void
        """
        index = key % self.m
        if self.h[index] == None:
            self.h[index] = ListNode(key, value)
        else:
            cur = self.h[index]
            while True:
                if cur.pair[0] == key:
                    cur.pair = (key, value) #update
                    return
                if cur.next == None: break
                cur = cur.next
            cur.next = ListNode(key, value)
        

    def get(self, key):
        """
        Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key
        :type key: int
        :rtype: int
        """
        index = key % self.m
        cur = self.h[index]
        while cur:
            if cur.pair[0] == key:
                return cur.pair[1]
            else:
                cur = cur.next
        return -1
            
        

    def remove(self, key):
        """
        Removes the mapping of the specified value key if this map contains a mapping for the key
        :type key: int
        :rtype: void
        """
        index = key % self.m
        cur = prev = self.h[index]
        if not cur: return
        if cur.pair[0] == key:
            self.h[index] = cur.next
        else:
            cur = cur.next
            while cur:
                if cur.pair[0] == key:
                    prev.next = cur.next
                    break
                else:
                    cur, prev = cur.next, prev.next
                


# Your MyHashMap object will be instantiated and called as such:
# obj = MyHashMap()
# obj.put(key,value)
# param_2 = obj.get(key)
# obj.remove(key)
```
</p>


### Java Solutions
- Author: pratik_patil
- Creation Date: Sun Jan 27 2019 23:53:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 27 2019 23:53:10 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1: Traditional Hash-Table Implementation - Using Array of LinkedList - Accepted in 75 ms**

- The general implementation of `HashMap` uses `bucket` which is basically a **chain of linked lists** and each node containing `<key, value>` pair. 
- So if we have duplicate nodes, that doesn\'t matter - it will still replicate each key with it\'s value in linked list node.
- When we insert the pair `(10, 20)` and then (`10, 30)`, there is technically no collision involved. We are just replacing the old value with the new value for a given key `10`, since in both cases, `10` is equal to `10` and also the hash code for `10` is always `10`.
- Collision happens when multiple keys hash to the same `bucket`. In that case, we need to make sure that we can distinguish between those keys. **Chaining collision resolution** is one of those techniques which is used for this.
- Just for the information. In JDK 8, `HashMap` has been tweaked so that if keys can be compared for ordering, then any densely-populated `bucket` is implemented as a `tree`, so that even if there are lots of entries with the same hash-code, the complexity is` O(log n)`.

Time complexity: `O(1)` average and `O(n)` worst case  - for all `get()`,` put()` and `remove()` methods.
Space complexity: `O(n)` - where `n` is the number of entries in HashMap

```
class MyHashMap
{
	ListNode[] nodes = new ListNode[10000];

	public int get(int key)
	{
		int index = getIndex(key);
		ListNode prev = findElement(index, key);
		return prev.next == null ? -1 : prev.next.val;
	}
	
	public void put(int key, int value)
	{
		int index = getIndex(key);
		ListNode prev = findElement(index, key);
		
		if (prev.next == null)
			prev.next = new ListNode(key, value);
		else 
			prev.next.val = value;
	}

	public void remove(int key)
	{
		int index = getIndex(key);
        ListNode prev = findElement(index, key);
			
        if(prev.next != null)
		    prev.next = prev.next.next;
	}

	private int getIndex(int key)
	{	
		return Integer.hashCode(key) % nodes.length;
	}

	private ListNode findElement(int index, int key)
	{
		if(nodes[index] == null)
			return nodes[index] = new ListNode(-1, -1);
        
        ListNode prev = nodes[index];
		
		while(prev.next != null && prev.next.key != key)
		{
			prev = prev.next;
		}
		return prev;
	}

	private static class ListNode
	{
		int key, val;
		ListNode next;

		ListNode(int key, int val)
		{
			this.key = key;
			this.val = val;
		}
	}
}
```

**Solution 2: Using Large Sized Array  - Accepted in 100 ms**

Time complexity: `O(1)` - for all `get()`,` put()` and `remove()` methods.
Space complexity: `O(n)` - where `n` is the maximum possible value for the key.

```
class MyHashMap
{
    int[] map;

    public MyHashMap()
	{
        map = new int[1000001];
        Arrays.fill(map,-1);
    }
	
    public int get(int key)
	{
        return map[key];
    }
    
	public void put(int key, int value)
	{
        map[key] = value;
    }
    
	public void remove(int key)
	{
        map[key] = -1;
    }
}
```
</p>


