---
title: "Clone N-ary Tree"
weight: 1384
#id: "clone-n-ary-tree"
---
## Description
<div class="description">
<p>Given a <code>root</code>&nbsp;of an N-ary tree,&nbsp;return a&nbsp;<a href="https://en.wikipedia.org/wiki/Object_copying#Deep_copy" target="_blank"><strong>deep copy</strong></a>&nbsp;(clone) of the tree.</p>

<p>Each node in the n-ary tree&nbsp;contains a val (<code>int</code>) and a list (<code>List[Node]</code>) of its children.</p>

<pre>
class Node {
    public int val;
    public List&lt;Node&gt; children;
}
</pre>

<p><em>Nary-Tree input serialization&nbsp;is represented in their level order traversal, each group of children is separated by the null value (See examples).</em></p>

<p><strong>Follow up:&nbsp;</strong>Can your solution work for the <a href="https://leetcode.com/problems/clone-graph/">graph problem</a>?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/12/narytreeexample.png" style="width: 100%; max-width: 300px;" /></p>

<pre>
<strong>Input:</strong> root = [1,null,3,2,4,null,5,6]
<strong>Output:</strong> [1,null,3,2,4,null,5,6]
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/11/08/sample_4_964.png" style="width: 296px; height: 241px;" /></p>

<pre>
<strong>Input:</strong> root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
<strong>Output:</strong> [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The depth of the n-ary tree is less than or equal to <code>1000</code>.</li>
	<li>The total number of nodes is between <code>[0,&nbsp;10^4]</code>.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Tree (tree)
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] 1 Liner
- Author: aswinsureshk
- Creation Date: Sat Jun 27 2020 16:43:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 27 2020 16:43:43 GMT+0800 (Singapore Standard Time)

<p>
```
def cloneTree(self, root: \'Node\') -> \'Node\':
	return Node(root.val, [self.cloneTree(child) for child in root.children]) if root else None
```

</p>


### C++/Java Recursion
- Author: votrubac
- Creation Date: Fri Jun 26 2020 00:43:13 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 26 2020 00:56:22 GMT+0800 (Singapore Standard Time)

<p>
**C++**
```cpp
Node* cloneTree(Node* r) {
    if (r == nullptr)
        return nullptr;
    auto new_r = new Node(r->val);
    for (auto child: r->children)
        new_r->children.push_back(cloneTree(child));
    return new_r;
}
```
**Java**
```java
public Node cloneTree(Node r) {
    if (r == null)
        return null;
    var new_r = new Node(r.val);
    for (var child: r.children)
        new_r.children.add(cloneTree(child));
    return new_r;    
}
```
**Python**
```python
class Solution:
    def cloneTree(self, r: \'Node\') -> \'Node\':
        if r is None:
            return None
        new_r = Node(r.val)
        for child in r.children:
            new_r.children.append(self.cloneTree(child))
        return new_r
```
</p>


### 3 line Python
- Author: pze168
- Creation Date: Sat Jun 27 2020 08:50:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 27 2020 08:50:59 GMT+0800 (Singapore Standard Time)

<p>
Recursive way.

```
class Solution:
    def cloneTree(self, root: \'Node\') -> \'Node\':
        if root is None:
            return None
        return Node(root.val, [self.cloneTree(child) for child in root.children])
```
</p>


