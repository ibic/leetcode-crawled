---
title: "Find the Longest Substring Containing Vowels in Even Counts"
weight: 1263
#id: "find-the-longest-substring-containing-vowels-in-even-counts"
---
## Description
<div class="description">
<p>Given the string <code>s</code>, return the size of the longest substring containing each vowel an even number of times. That is, &#39;a&#39;, &#39;e&#39;, &#39;i&#39;, &#39;o&#39;, and &#39;u&#39; must appear an even number of times.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;eleetminicoworoep&quot;
<strong>Output:</strong> 13
<strong>Explanation: </strong>The longest substring is &quot;leetminicowor&quot; which contains two each of the vowels: <strong>e</strong>, <strong>i</strong> and <strong>o</strong> and zero of the vowels: <strong>a</strong> and <strong>u</strong>.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;leetcodeisgreat&quot;
<strong>Output:</strong> 5
<strong>Explanation:</strong> The longest substring is &quot;leetc&quot; which contains two e&#39;s.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;bcbcbc&quot;
<strong>Output:</strong> 6
<strong>Explanation:</strong> In this case, the given string &quot;bcbcbc&quot; is the longest because all vowels: <strong>a</strong>, <strong>e</strong>, <strong>i</strong>, <strong>o</strong> and <strong>u</strong> appear zero times.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 5 x 10^5</code></li>
	<li><code>s</code>&nbsp;contains only lowercase English letters.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] One Pass
- Author: lee215
- Creation Date: Sun Mar 08 2020 00:05:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 15:17:10 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
`cur` records the count of "aeiou"
`cur & 1` = the records of `a` % 2
`cur & 2` = the records of `e` % 2
`cur & 4` = the records of `i` % 2
`cur & 8` = the records of `o` % 2
`cur & 16` = the records of `u` % 2
`seen` note the index of first occurrence of `cur`


Note that we don\'t really need the exact count number,
we only need to know if it\'s odd or even.

If it\'s one of `aeiou`,
`\'aeiou\'.find(c)` can find the index of vowel,
`cur ^= 1 << \'aeiou\'.find(c)` will toggle the count of vowel.

But for no vowel characters,
`\'aeiou\'.find(c)` will return -1,
that\'s reason that we do `1 << (\'aeiou\'.find(c) + 1) >> 1`.

Hope this explain enough.

# Complexity
Time `O(N)`, Space `O(1)`
<br>

**Java:**
```java
    public int findTheLongestSubstring(String s) {
        int res = 0 , cur = 0, n = s.length();
        HashMap<Integer, Integer> seen = new HashMap<>();
        seen.put(0, -1);
        for (int i = 0; i < n; ++i) {
            cur ^= 1 << ("aeiou".indexOf(s.charAt(i)) + 1 ) >> 1;
            seen.putIfAbsent(cur, i);
            res = Math.max(res, i - seen.get(cur));
        }
        return res;
    }
```
**C++**
by @chenkkkabc
```cpp
    int findTheLongestSubstring(string s) {
        unordered_map<int, int> m{{0, -1}};
        int res = 0, n = s.length(), cur = 0;
        for (int i = 0; i < n; i++) {
            cur ^= 1 << string("aeiou").find(s[i]) + 1 >> 1;
            if (!m.count(cur)) m[cur] = i;
            res = max(res, i - m[cur]);
        }
        return res;
    }
```
**Python:**
```py
    def findTheLongestSubstring(self, s):
        seen = {0: -1}
        res = cur = 0
        for i, c in enumerate(s):
            cur ^= 1 << (\'aeiou\'.find(c) + 1) >> 1
            seen.setdefault(cur, i)
            res = max(res, i - seen[cur])
        return res
```

</p>


### C++/Java with picture
- Author: votrubac
- Creation Date: Mon Mar 09 2020 18:45:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 14 2020 08:11:50 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
We do not need to know the exact count, we just need a flag indicating whether a vowel is even or odd. So, we can track the status of each vowel using a bit mask. Since we only have 5 vowels in the English alphabet, we will have 32 possible combinations.

Now, if our mask is zero, then our string contains only even vowels. Also, if a mask is the same for indexes `i` and `j`, the mask for substring `[i +1, j]` must be zero. Therefore, substring `[i +1, j]` also contains even vowels only.

#### Algorithm
As we go through our string, we update `mask`, and track the smallest index for each `mask` combination. If we encounter the same `mask` later in the string, that means the string between smallest (exclusive) and current (inclusive) index meets the problem criteria.

![image](https://assets.leetcode.com/users/votrubac/image_1584056896.png)

In other words, we need to find the maximum distance between the first and last index for each `mask` combination.

> Note that for zero `mask` (all vowels\' count is even), the first index is \'-1\' - so that we include the string from very beginning. 

**C++**
I am using arrays here (e.g. instead of hash maps) for the performance. With fast IO settings, the runtime of this C++ code is 16 ms.

```cpp
static constexpr char c_m[26] = {1,0,0,0,2,0,0,0,4,0,0,0,0,0,8,0,0,0,0,0,16,0,0,0,0,0};    
int findTheLongestSubstring(string s) {
    int mask = 0, res = 0;
    vector<int> m(32, -1);
    for (int i = 0; i < s.size(); ++i) {
        mask ^= c_m[s[i] - \'a\'];
        if (mask != 0 && m[mask] == -1)
            m[mask] = i;
        res = max(res, i - m[mask]);
    }
    return res;
}
```
**Java**
```java
char[] c_m = {1,0,0,0,2,0,0,0,4,0,0,0,0,0,8,0,0,0,0,0,16,0,0,0,0,0};
public int findTheLongestSubstring(String s) {
    int mask = 0, res = 0;
    int[] m = new int[32];
    Arrays.fill(m, -1);
    for (int i = 0; i < s.length(); ++i) {
        mask ^= c_m[s.charAt(i) - \'a\'];
        if (mask != 0 && m[mask] == -1)
            m[mask] = i;
        res = Math.max(res, i - m[mask]);
    }
    return res;
}
```

#### Complexity Analysis
- Time: O(n)
- Memory: O(2 ^ m), where m is the number of characters we need to track counts for.
  - 5 vowels needs 32 positions to track in this problem.
</p>


### This problem should be marked as hard
- Author: Switch2on
- Creation Date: Sun Mar 08 2020 00:50:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 01:35:59 GMT+0800 (Singapore Standard Time)

<p>
According to the time top contesters spent on this problem and number of people who solved this question, I think the problem should be marked as hard. What do you think?
</p>


