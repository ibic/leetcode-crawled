---
title: "Water and Jug Problem"
weight: 348
#id: "water-and-jug-problem"
---
## Description
<div class="description">
<p>You are given two jugs with capacities <i>x</i> and <i>y</i> litres. There is an infinite amount of water supply available. You need to determine whether it is possible to measure exactly <i>z</i> litres using these two jugs.</p>

<p>If <i>z</i> liters of water is measurable, you must have <i>z</i> liters of water contained within <b>one or both buckets</b> by the end.</p>

<p>Operations allowed:</p>

<ul>
	<li>Fill any of the jugs completely with water.</li>
	<li>Empty any of the jugs.</li>
	<li>Pour water from one jug into another till the other jug is completely full or the first jug itself is empty.</li>
</ul>

<p><b>Example 1:</b> (From the famous <a href="https://www.youtube.com/watch?v=BVtQNK_ZUJg" target="_blank"><i>&quot;Die Hard&quot;</i> example</a>)</p>

<pre>
Input: x = 3, y = 5, z = 4
Output: True
</pre>

<p><b>Example 2:</b></p>

<pre>
Input: x = 2, y = 6, z = 5
Output: False
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= x &lt;= 10^6</code></li>
	<li><code>0 &lt;= y&nbsp;&lt;= 10^6</code></li>
	<li><code>0 &lt;= z&nbsp;&lt;= 10^6</code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Lyft - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Math solution - Java solution
- Author: myfavcat123
- Creation Date: Sat Jun 25 2016 16:06:09 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 12:04:29 GMT+0800 (Singapore Standard Time)

<p>
This is a pure Math problem. We need the knowledge of number theory to cover the proof and solution. No idea why microsoft uses this problem in real interview. 

The basic idea is to use the property of B\xe9zout's identity  and check if z is a multiple of GCD(x, y)

Quote from wiki:

> B\xe9zout's identity (also called B\xe9zout's lemma) is a theorem in the elementary theory of numbers:
>
> let a and b be nonzero integers and let d be their greatest common divisor. Then there exist integers x
> and y such that ax+by=d
> 
> In addition, the greatest common divisor d is the smallest positive integer that can be written as ax + by
>
> every integer of the form ax + by is a multiple of the greatest common divisor d.

If  a or  b is negative this means we are emptying a jug of  x or  y gallons respectively.

Similarly if  a or  b is positive this means we are filling a jug of  x or  y gallons respectively.

x = 4, y = 6, z = 8.

GCD(4, 6) = 2

8 is multiple of 2

so this input is valid and we have: 

-1 * 4 + 6 * 2 = 8

In this case, there is a solution obtained by filling the 6 gallon jug twice and emptying the 4 gallon jug once. (Solution. Fill the 6 gallon jug and empty 4 gallons to the 4 gallon jug. Empty the 4 gallon jug. Now empty the remaining two gallons from the 6 gallon jug to the 4 gallon jug. Next refill the 6 gallon jug. This gives 8 gallons in the end)

See wiki:

[B\xe9zout's identity][1]

and comments in the code


    public boolean canMeasureWater(int x, int y, int z) {
        //limit brought by the statement that water is finallly in one or both buckets
        if(x + y < z) return false;
        //case x or y is zero
        if( x == z || y == z || x + y == z ) return true;
        
        //get GCD, then we can use the property of B\xe9zout's identity
        return z%GCD(x, y) == 0;
    }
    
    public int GCD(int a, int b){
        while(b != 0 ){
            int temp = b;
            b = a%b;
            a = temp;
        }
        return a;
    }


  [1]: https://en.wikipedia.org/wiki/B%C3%A9zout%27s_identity
</p>


### This problem should be classified as HARD
- Author: jedihy
- Creation Date: Mon Jun 27 2016 04:33:01 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 17:14:59 GMT+0800 (Singapore Standard Time)

<p>
This problem should be classified as HARD.
</p>


### Breadth-First Search with explanation.
- Author: leetcodedavy
- Creation Date: Thu Jul 07 2016 13:44:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 13:28:10 GMT+0800 (Singapore Standard Time)

<p>
It took me a while to understand the GCD method. My first attempt to this problem was using BFS, which is very intuitive and easy to understand.
The complexity is definitely much longer than GCD-based method.

```
class Solution(object):
    def canMeasureWater(self, x, y, z):
        """
        :type x: int
        :type y: int
        :type z: int
        :rtype: bool
        """
        if x > y:
            temp = x;
            x = y;
            y = temp;
            
        if z > x + y:
            return False;
        
        # set the initial state will empty jars;
        queue = [(0, 0)];
        visited = set((0, 0));
        while len(queue) > 0:
            a, b = queue.pop(0);
            if a + b == z:
                return True;
            
            states = set()
            
            states.add((x, b)) # fill jar x;
            states.add((a, y)) # fill jar y;
            states.add((0, b)) # empty jar x;
            states.add((a, 0)) # empty jar y;
            states.add((min(x, b + a), 0 if b < x - a else b - (x - a))) # pour jar y to x;
            states.add((0 if a + b < y else a - (y - b), min(b + a, y))) # pour jar x to y;

            for state in states:
                if state in visited:
                    continue;
                queue.append(state)
                visited.add(state);
                
        return False;
```
</p>


