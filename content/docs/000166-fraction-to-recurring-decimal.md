---
title: "Fraction to Recurring Decimal"
weight: 166
#id: "fraction-to-recurring-decimal"
---
## Description
<div class="description">
<p>Given two integers representing the <code>numerator</code> and <code>denominator</code> of a fraction, return <em>the fraction in string format</em>.</p>

<p>If the fractional part is repeating, enclose the repeating part in parentheses.</p>

<p>If multiple answers are possible, return <strong>any of them</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> numerator = 1, denominator = 2
<strong>Output:</strong> "0.5"
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> numerator = 2, denominator = 1
<strong>Output:</strong> "2"
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> numerator = 2, denominator = 3
<strong>Output:</strong> "0.(6)"
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> numerator = 4, denominator = 333
<strong>Output:</strong> "0.(012)"
</pre><p><strong>Example 5:</strong></p>
<pre><strong>Input:</strong> numerator = 1, denominator = 5
<strong>Output:</strong> "0.2"
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>-2<sup>31</sup> &lt;=&nbsp;numerator, denominator &lt;= 2<sup>31</sup> - 1</code></li>
	<li><code>denominator != 0</code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Math (math)

## Companies
- Goldman Sachs - 11 (taggedByAdmin: false)
- IXL - 7 (taggedByAdmin: true)
- Facebook - 4 (taggedByAdmin: false)
- Airbnb - 13 (taggedByAdmin: false)
- Google - 6 (taggedByAdmin: true)
- Uber - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Rubrik - 4 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Cohesity - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary

This is a straight forward coding problem but with a fair amount of details to get right.

## Hints

1. No scary math, just apply elementary math knowledge. Still remember how to perform a <i>long division</i>?
2. Try a long division on $$\dfrac{4}{9}$$, the repeating part is obvious. Now try $$\dfrac{4}{333}$$. Do you see a pattern?
3. Be wary of edge cases! List out as many test cases as you can think of and test your code thoroughly.

## Solution
---
#### Approach 1: Long Division

**Intuition**

The key insight here is to notice that once the remainder starts repeating, so does the divided result.

$$
\begin{array}{rll}
 0.16 \\
6{\overline{\smash{\big)}\,1.00}} \\[-1pt]
\underline{0\phantom{.00}} \\[-1pt]
1\phantom{.}0 \phantom{0} && \Leftarrow \textrm{$remainder$ = 1, mark 1 as seen at $position$ = 0.} \\[-1pt]
\underline{\phantom{0}6\phantom{0}} \\[-1pt]
\phantom{0}40 && \Leftarrow \textrm{$remainder$ = 4, mark 4 as seen at $position$ = 1.} \\[-1pt]
\underline{\phantom{0}36} \\[-1pt]
\phantom{00}4 && \Leftarrow \textrm{$remainder$ = 4 was seen before at $position$ = 1,} \\ \phantom{00} && \quad \textrm{so the fractional part starts repeating at $position$ = 1} \Rightarrow 1(6). \\[-1pt]
\end{array}
$$

<br>

**Algorithm**

You will need a hash table that maps from the remainder to its position of the fractional part. Once you found a repeating remainder, you may enclose the reoccurring fractional part with parentheses by consulting the position from the table.

The remainder could be zero while doing the division. That means there is no repeating fractional part and you should stop right away.

Just like the question [Divide Two Integers](https://leetcode.com/problems/divide-two-integers/), be wary of edge cases such as negative fractions and nasty extreme case such as $$\dfrac{-2147483648}{-1}$$.


Here are some good test cases:


| Test case | Explanation |
| ------------- | ---------------- |
| $$\frac{0}{1}$$ | Numerator is zero. |
| $$\frac{1}{0}$$ | Divisor is 0, should handle it by throwing an exception but here we ignore for simplicity sake. |
| $$\frac{20}{4}$$ | Answer is a whole integer, should not contain the fractional part. |
| $$\frac{1}{2}$$ | Answer is 0.5, no recurring decimal. |
| $$\frac{-1}{4}$$ or $$\frac{1}{-4}$$ | One of the numerator or denominator is negative, fraction is negative. |
| $$\frac{-1}{-4}$$ | Both numerator and denominator are negative, should result in positive fraction. |
| $$\frac{-2147483648}{-1}$$ | Beware of overflow if you cast to positive. |

<br>
<iframe src="https://leetcode.com/playground/hnynQ3ya/shared" frameBorder="0" width="100%" height="500" name="hnynQ3ya"></iframe>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My clean Java solution
- Author: dchen0215
- Creation Date: Sun Jan 25 2015 08:05:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 01:47:37 GMT+0800 (Singapore Standard Time)

<p>
The important thing is to consider all edge cases while thinking this problem through, including: negative integer, possible overflow, etc.

Use HashMap to store a remainder and its associated index while doing the division so that whenever a same remainder comes up, we know there is a repeating fractional part.

Please comment if you see something wrong or can be improved. Cheers!

    public class Solution {
        public String fractionToDecimal(int numerator, int denominator) {
            if (numerator == 0) {
                return "0";
            }
            StringBuilder res = new StringBuilder();
            // "+" or "-"
            res.append(((numerator > 0) ^ (denominator > 0)) ? "-" : "");
            long num = Math.abs((long)numerator);
            long den = Math.abs((long)denominator);
            
            // integral part
            res.append(num / den);
            num %= den;
            if (num == 0) {
                return res.toString();
            }
            
            // fractional part
            res.append(".");
            HashMap<Long, Integer> map = new HashMap<Long, Integer>();
            map.put(num, res.length());
            while (num != 0) {
                num *= 10;
                res.append(num / den);
                num %= den;
                if (map.containsKey(num)) {
                    int index = map.get(num);
                    res.insert(index, "(");
                    res.append(")");
                    break;
                }
                else {
                    map.put(num, res.length());
                }
            }
            return res.toString();
        }
    }
</p>


### Accepted cpp solution, with explainations
- Author: mzchen
- Creation Date: Tue Dec 16 2014 20:59:39 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 13:02:40 GMT+0800 (Singapore Standard Time)

<p>
    // upgraded parameter types
    string fractionToDecimal(int64_t n, int64_t d) {
        // zero numerator
        if (n == 0) return "0";
    
        string res;
        // determine the sign
        if (n < 0 ^ d < 0) res += '-';
    
        // remove sign of operands
        n = abs(n), d = abs(d);
    
        // append integral part
        res += to_string(n / d);
    
        // in case no fractional part
        if (n % d == 0) return res;
    
        res += '.';
    
        unordered_map<int, int> map;
    
        // simulate the division process
        for (int64_t r = n % d; r; r %= d) {
    
            // meet a known remainder
            // so we reach the end of the repeating part
            if (map.count(r) > 0) {
                res.insert(map[r], 1, '(');
                res += ')';
                break;
            }
    
            // the remainder is first seen
            // remember the current position for it
            map[r] = res.size();
    
            r *= 10;
    
            // append the quotient digit
            res += to_string(r / d);
        }
    
        return res;
    }
</p>


### C++ unordered_map
- Author: jianchao-li
- Creation Date: Thu Jun 25 2015 22:15:26 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 09:37:35 GMT+0800 (Singapore Standard Time)

<p>
For the decimal part to recur, **the remainder should recur**. So we maintain the remainders we have seen. Once we see an existing remainder, we have reached the end of the recurring part (enclose it with a `)`). Moreover, insert the `(` to the starting index of the recurring part by maintaining a mapping from each remainder to the index of the corresponding digit and using it to retrieve the starting index.

For those without fractional parts or with non-recursive fractional points, we may find them out by `%` or zero remainder. Some other problems that need to be considered include the sign and overflow (`-2147483648 / -1`).

```cpp
class Solution {
public:
    string fractionToDecimal(int numerator, int denominator) {
        if (!numerator) {
            return "0";
        }
        string ans;
        if (numerator > 0 ^ denominator > 0) {
            ans += \'-\';
        }
        long n = labs(numerator), d = labs(denominator), r = n % d;
        ans += to_string(n / d);
        if (!r) {
            return ans;
        }
        ans += \'.\';
        unordered_map<long, int> rs;
        while (r) {
            if (rs.find(r) != rs.end()) {
                ans.insert(rs[r], "(");
                ans += \')\';
                break;
            }
            rs[r] = ans.size();
            r *= 10;
            ans += to_string(r / d);
            r %= d;
        }
        return ans;
    }
};
```
</p>


