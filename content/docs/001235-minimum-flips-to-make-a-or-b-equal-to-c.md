---
title: "Minimum Flips to Make a OR b Equal to c"
weight: 1235
#id: "minimum-flips-to-make-a-or-b-equal-to-c"
---
## Description
<div class="description">
<p>Given 3 positives numbers <code>a</code>, <code>b</code> and <code>c</code>. Return the minimum flips required in some bits of <code>a</code> and <code>b</code> to make (&nbsp;<code>a</code> OR <code>b</code> == <code>c</code>&nbsp;). (bitwise OR operation).<br />
Flip operation&nbsp;consists of change&nbsp;<strong>any</strong>&nbsp;single bit 1 to 0 or change the bit 0 to 1&nbsp;in their binary representation.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/01/06/sample_3_1676.png" style="width: 260px; height: 87px;" /></p>

<pre>
<strong>Input:</strong> a = 2, b = 6, c = 5
<strong>Output:</strong> 3
<strong>Explanation: </strong>After flips a = 1 , b = 4 , c = 5 such that (<code>a</code> OR <code>b</code> == <code>c</code>)</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> a = 4, b = 2, c = 7
<strong>Output:</strong> 1
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> a = 1, b = 2, c = 3
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= a &lt;= 10^9</code></li>
	<li><code>1 &lt;= b&nbsp;&lt;= 10^9</code></li>
	<li><code>1 &lt;= c&nbsp;&lt;= 10^9</code></li>
</ul>
</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ bitwise xor solution, 1 line
- Author: mzchen
- Creation Date: Tue Jan 14 2020 13:44:57 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 14 2020 13:50:01 GMT+0800 (Singapore Standard Time)

<p>
**Step 1**: `a | b` is what we have while `c` is what we want. An XOR operation finds all different bits, i.e. `(a | b) ^ c` sets the bits where flip(s) is needed. Then we count the set bits.
**Step 2**: There is only one case when two flips are needed: a bit is `0` in `c` but is `1` in both `a` and `b`. An AND operation finds all common `1` bits, i.e. `a & b & ((a | b) ^ c)` sets the common `1` bits in `a`, `b` and the must-flip bits found in Step 1.
```
int minFlips(int a, int b, int c) {
    return popcount((a | b) ^ c) + popcount(a & b & ((a | b) ^ c));
}
```
or simplify with an assignment:
```
int minFlips(int a, int b, int c) {
    return popcount(c ^= a | b) + popcount(a & b & c);
}
```
Note: The `popcount` function is a C++ 20 standard builtin function that counts set bits. LeetCode uses g++ compiler with the C++17 standard so we can use `__builtin_popcount` instead. For other compilers please use `bitset<32>().count()`.
</p>


### [Java/Python 3] Bit manipulation w/ explanation and analysis.
- Author: rock
- Creation Date: Sun Jan 12 2020 12:03:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 12 2020 18:59:23 GMT+0800 (Singapore Standard Time)

<p>
**Credit to @codedayday:** for contribution to remove redundant part of the code.

----

**Q & A**
Q1: What is the `mask`? is it `00...01` at the beginning, and move bit `1` to the left when `i` is increasing?
A1: Exactly. `mask` changes as the following during iteration:
	
	00...01 -> 00...010 -> 00...0100 -> 00...01000 -> ...

Q2: Is the `31` here because in C++ int is 32-bit long and there\'s a sign bit?
A2: Because the question said that `a,b,c` are positive numbers. -- **Credit to @KHVic**

----

1. if `(a | b) ^ c` is `0`, `a | b` and `c` are equal, otherwise not equal and we need to check them bit by bit;
2. For `ith` bit of `(a | b) ^ c`, use `1 << i` as mask to do `&` operation to check if the bit is `0`; if not, `ith` bits of `a | b` and `c` are not same and we need at least `1` flip; there are 3 cases:
	i) the `ith` bit of `a | b`  less than that of `c`; then `ith` bit of `a | b` must be `0`, we only need to flip the `ith` bit of either `a` or `b`;
	ii) the `ith` bit of `a | b`  bigger than that of `c`; then `ith` bit of `a | b` must be `1`, but if only one of `a` or `b`\'s `ith` bit is `1`, we only need to flip one of them;
	iii) Other case, we need to flip both set bit of `a` and `b`, hence need `2` flips.
	In short, **if `ith` bits of `a | b` and `c` are not same, then only if  `ith` bits of `a` and `b` are both `1` and  that of `c` is `0`, we need `2` flips; otherwise only `1` flip needed.**
```java
    public int minFlips(int a, int b, int c) {
        int ans = 0, ab = a | b, equal = ab ^ c;
        for (int i = 0; i < 31; ++i) {
            int mask = 1 << i;
            if ((equal & mask) > 0)  // ith bits of a | b and c are not same, need at least 1 flip.
             // ans += (ab & mask) < (c & mask) || (a & mask) != (b & mask) ? 1 : 2;
                ans += (a & mask) == (b & mask) && (c & mask) == 0 ? 2 : 1; // ith bits of a and b are both 1 and that of c is 0?
        }
        return ans;
    }
```
```python
    def minFlips(self, a: int, b: int, c: int) -> int:
        ab, equal, ans = a | b, (a | b) ^ c, 0
        for i in range(31):
            mask = 1 << i
            if equal & mask > 0:
              # ans += 1 if (ab & mask) < (c & mask) or (a & mask) != (b & mask) else 2
                ans += 2 if (a & mask) == (b & mask) and (c & mask) == 0 else 1
        return ans
```
**Analysis:**
Time: O(L), space: O(1), where L is the number of bits in an integer.
</p>


### [Java] O(1) time & O(1) space
- Author: manrajsingh007
- Creation Date: Sun Jan 12 2020 12:01:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 12 2020 12:29:05 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int minFlips(int a, int b, int c) {
        int count = 0;
        for(int i = 1; i <= 32; i++) {
            int b1 = 0, b2 = 0, b3 = 0;
            if(((a >> (i - 1)) & 1) >= 1) b1 = 1; // check the ith bit of a
            if(((b >> (i - 1)) & 1) >= 1) b2 = 1; // check the ith bit of b
            if(((c >> (i - 1)) & 1) >= 1) b3 = 1; // check the ith bit of c
            if(b3 == 0 && (b1 == 1 || b2 == 1)) count += b1 + b2; // if the ith bit of c is 0 and any of the ith bits of a or b is 1
            else if(b3 == 1 && b1 == 0 && b2 == 0) count++; // if the ith bit of c is 1, check the ith bits of a and b
        }
        return count;
    }
}
</p>


