---
title: "Duplicate Emails"
weight: 1476
#id: "duplicate-emails"
---
## Description
<div class="description">
<p>Write a SQL query to find all duplicate emails in a table named <code>Person</code>.</p>

<pre>
+----+---------+
| Id | Email   |
+----+---------+
| 1  | a@b.com |
| 2  | c@d.com |
| 3  | a@b.com |
+----+---------+
</pre>

<p>For example, your query should return the following for the above table:</p>

<pre>
+---------+
| Email   |
+---------+
| a@b.com |
+---------+
</pre>

<p><strong>Note</strong>: All emails are in lowercase.</p>

</div>

## Tags


## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach I: Using `GROUP BY` and a temporary table [Accepted]

**Algorithm**

Duplicated emails existed more than one time. To count the times each email exists, we can use the following code.

```sql
select Email, count(Email) as num
from Person
group by Email;
```

```
| Email   | num |
|---------|-----|
| a@b.com | 2   |
| c@d.com | 1   |
```

Taking this as a temporary table, we can get a solution as below.

```sql
select Email from
(
  select Email, count(Email) as num
  from Person
  group by Email
) as statistic
where num > 1
;
```

#### Approach II: Using `GROUP BY` and `HAVING` condition [Accepted]

A more common used way to add a condition to a `GROUP BY` is to use the `HAVING` clause, which is much simpler and more efficient.

So we can rewrite the above solution to this one.

**MySQL**

```sql
select Email
from Person
group by Email
having count(Email) > 1;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### I have this Simple Approach, anybody has some other way
- Author: dashinghimay
- Creation Date: Sat Jan 17 2015 15:07:15 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 09:20:17 GMT+0800 (Singapore Standard Time)

<p>
select Email 
from Person 
group by Email 
having count(*) > 1
</p>


### My simple accepted solution
- Author: keqinwu
- Creation Date: Tue Apr 28 2015 10:42:21 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 06 2018 23:14:21 GMT+0800 (Singapore Standard Time)

<p>
    select Email
    from Person
    group by Email
    having count(*)>1;
</p>


### A solution using a GROUP BY and another one using a self join
- Author: ohini
- Creation Date: Fri Aug 21 2015 11:27:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 21 2015 11:27:45 GMT+0800 (Singapore Standard Time)

<p>
914 ms

    SELECT Email from Person
    Group By Email
    Having Count(*) > 1;

933 ms

    SELECT distinct p1.Email from Person p1
    INNER JOIN Person p2
    ON p1.Email = p2.Email
    WHERE p1.Id <> p2.Id;
</p>


