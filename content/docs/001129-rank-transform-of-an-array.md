---
title: "Rank Transform of an Array"
weight: 1129
#id: "rank-transform-of-an-array"
---
## Description
<div class="description">
<p>Given an array of integers&nbsp;<code>arr</code>, replace each element with its rank.</p>

<p>The rank represents how large the element is. The rank has the following rules:</p>

<ul>
	<li>Rank is an integer starting from 1.</li>
	<li>The larger the element, the larger the rank. If two elements are equal, their rank must be the same.</li>
	<li>Rank should be as small as possible.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [40,10,20,30]
<strong>Output:</strong> [4,1,2,3]
<strong>Explanation</strong>: 40 is the largest element. 10 is the smallest. 20 is the second smallest. 30 is the third smallest.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [100,100,100]
<strong>Output:</strong> [1,1,1]
<strong>Explanation</strong>: Same elements share the same rank.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [37,12,28,9,100,56,80,5,12]
<strong>Output:</strong> [5,3,4,2,8,6,7,1,3]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= arr.length &lt;= 10<sup>5</sup></code></li>
	<li><code>-10<sup>9</sup>&nbsp;&lt;= arr[i] &lt;= 10<sup>9</sup></code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Python] HashMap
- Author: lee215
- Creation Date: Sun Jan 26 2020 00:13:52 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 28 2020 11:29:25 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Use a hashmap `rank` to record the rank for each element.
<br>

## **Complexity**
Time `O(NlogN)`
Space `O(N)`
<br>

**C++**
```cpp
    vector<int> arrayRankTransform(vector<int>& arr) {
      vector<int> A(arr);
      sort(A.begin(), A.end());
      unordered_map<int, int> rank;
      for (int& a : A)
          rank.emplace(a, rank.size() + 1);
      for (int &a: arr)
          a = rank[a];
      return arr;
    }
```
**Python:**
```python
    def arrayRankTransform(self, A):
        rank = {}
        for a in sorted(A):
            rank.setdefault(a, len(rank) + 1)
        return map(rank.get, A)
```

**1-line Python**
```py
    def arrayRankTransform(self, A):
        return map({a: i + 1 for i, a in enumerate(sorted(set(A)))}.get, A)
```
</p>


### Java - TreeMap - sorting
- Author: ijaz20
- Creation Date: Sun Jan 26 2020 00:01:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 14 2020 15:37:14 GMT+0800 (Singapore Standard Time)

<p>
```
  public int[] arrayRankTransform(int[] arr) {
        Map<Integer, List<Integer>> map = new TreeMap<>();
        for (int i = 0; i < arr.length; i++) {
			map.computeIfAbsent(arr[i], k -> new ArrayList<>()).add(i);
        }
        int rank = 1;
        for (Map.Entry<Integer, List<Integer>> entry : map.entrySet()) {
            List<Integer> currentList = entry.getValue();
            for(int i: currentList)  arr[i] = rank;
            rank++;
        }
        return arr;
    }

</p>


### Python set+sorted+dict (and SciPy)
- Author: StefanPochmann
- Creation Date: Sun Jan 26 2020 01:09:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 26 2020 07:59:17 GMT+0800 (Singapore Standard Time)

<p>
Remove duplicates with `set`, then sort, then build a dict mapping values to ranks, then use that dict on the given list.

Time: `O(n log n)`
Space: `O(n)`

```
def arrayRankTransform(self, A):
    return map({a: i+1 for i, a in enumerate(sorted(set(A)))}.get, A)
```
or
```
def arrayRankTransform(self, A):
    return map(dict(zip(sorted(set(A)), itertools.count(1))).get, A)
```
SciPy solution (don\'t know the complexities, the [&rarr;doc](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.rankdata.html) doesn\'t say):
```
from scipy.stats import rankdata

class Solution(object):
    def arrayRankTransform(self, A):
        return rankdata(A, \'dense\')
```
</p>


