---
title: "Word Ladder"
weight: 127
#id: "word-ladder"
---
## Description
<div class="description">
<p>Given two words (<em>beginWord</em> and <em>endWord</em>), and a dictionary&#39;s word list, find the length of shortest transformation sequence from <em>beginWord</em> to <em>endWord</em>, such that:</p>

<ol>
	<li>Only one letter can be changed at a time.</li>
	<li>Each transformed word must exist in the word list.</li>
</ol>

<p><strong>Note:</strong></p>

<ul>
	<li>Return 0 if there is no such transformation sequence.</li>
	<li>All words have the same length.</li>
	<li>All words contain only lowercase alphabetic characters.</li>
	<li>You may assume no duplicates in the word list.</li>
	<li>You may assume <em>beginWord</em> and <em>endWord</em> are non-empty and are not the same.</li>
</ul>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong>
beginWord = &quot;hit&quot;,
endWord = &quot;cog&quot;,
wordList = [&quot;hot&quot;,&quot;dot&quot;,&quot;dog&quot;,&quot;lot&quot;,&quot;log&quot;,&quot;cog&quot;]

<strong>Output: </strong>5

<strong>Explanation:</strong> As one shortest transformation is &quot;hit&quot; -&gt; &quot;hot&quot; -&gt; &quot;dot&quot; -&gt; &quot;dog&quot; -&gt; &quot;cog&quot;,
return its length 5.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong>
beginWord = &quot;hit&quot;
endWord = &quot;cog&quot;
wordList = [&quot;hot&quot;,&quot;dot&quot;,&quot;dog&quot;,&quot;lot&quot;,&quot;log&quot;]

<strong>Output:</strong>&nbsp;0

<strong>Explanation:</strong>&nbsp;The endWord &quot;cog&quot; is not in wordList, therefore no possible<strong>&nbsp;</strong>transformation.
</pre>

<ul>
</ul>

</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 38 (taggedByAdmin: true)
- Facebook - 10 (taggedByAdmin: true)
- Google - 10 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Lyft - 5 (taggedByAdmin: false)
- Qualtrics - 5 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- LinkedIn - 7 (taggedByAdmin: true)
- Oracle - 5 (taggedByAdmin: false)
- Uber - 5 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- Spotify - 2 (taggedByAdmin: false)
- Square - 5 (taggedByAdmin: false)
- Snapchat - 4 (taggedByAdmin: true)
- Zillow - 4 (taggedByAdmin: false)
- Affirm - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Samsung - 2 (taggedByAdmin: false)
- Audible - 2 (taggedByAdmin: false)
- Cohesity - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- Yelp - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

We are given a `beginWord` and an `endWord`. Let these two represent `start node` and `end node` of a graph. We have to reach from the start node to the end node using some intermediate nodes/words. The intermediate nodes are determined by the `wordList` given to us. The only condition for every step we take on this ladder of words is the current word should change by just `one letter`.

<center>
<img src="../Figures/127/Word_Ladder_1.png" width="400"/>
</center>

We will essentially be working with an undirected and unweighted graph with words as nodes and edges between words which differ by just one letter. The problem boils down to finding the shortest path from a start node to a destination node, if there exists one. Hence it can be solved using `Breadth First Search` approach.

One of the most important step here is to figure out how to find adjacent nodes i.e. words which differ by one letter. To efficiently find the neighboring nodes for any given word we do some pre-processing on the words of the given `wordList`. The pre-processing involves replacing the letter of a word by a non-alphabet say, `*`.

<center>
<img src="../Figures/127/Word_Ladder_2.png" width="400"/>
</center>

This pre-processing helps to form generic states to represent a single letter change.

For e.g. `Dog ----> D*g <---- Dig`

Both `Dog` and `Dig` map to the same intermediate or generic state `D*g`.

The preprocessing step helps us find out the generic one letter away nodes for any word of the word list and hence making it easier and quicker to get the adjacent nodes. Otherwise, for every word we will have to iterate over the entire word list and find words that differ by one letter. That would take a lot of time. This preprocessing step essentially builds the adjacency list first before beginning the breadth first search algorithm.

For eg. While doing BFS if we have to find the adjacent nodes for `Dug` we can first find all the generic states for `Dug`.

1. `Dug => *ug`
2. `Dug => D*g`
3. `Dug => Du*`

The second transformation `D*g` could then be mapped to `Dog` or `Dig`, since all of them share the same generic state. Having a common generic transformation means two words are connected and differ by one letter.

#### Approach 1: Breadth First Search

**Intuition**

Start from `beginWord` and search the `endWord` using BFS.

**Algorithm**

1. Do the pre-processing on the given `wordList` and find all the possible generic/intermediate states. Save these intermediate states in a dictionary with key as the intermediate word and value as the list of words which have the same intermediate word.
2. Push a tuple containing the `beginWord` and `1` in a queue. The `1` represents the level number of a node. We have to return the level of the `endNode` as that would represent the shortest sequence/distance from the `beginWord`.
3. To prevent cycles, use a visited dictionary.
4. While the queue has elements, get the front element of the queue. Let's call this word as `current_word`.
5. Find all the generic transformations of the `current_word` and find out if any of these transformations is also a transformation of other words in the word list. This is achieved by checking the `all_combo_dict`.
6. The list of words we get from `all_combo_dict` are all the words which have a common intermediate state with the `current_word`. These new set of words will be the adjacent nodes/words to `current_word` and hence added to the queue.
6. Hence, for each word in this list of intermediate words, append `(word, level + 1)` into the queue where `level` is the level for the `current_word`.
7. Eventually if you reach the desired word, its level would represent the shortest transformation sequence length.

    >Termination condition for standard BFS is finding the end word.

<iframe src="https://leetcode.com/playground/bzqGspma/shared" frameBorder="0" width="100%" height="500" name="bzqGspma"></iframe>

**Complexity Analysis**

* Time Complexity: $$O({M}^2 \times N)$$, where $$M$$ is the length of each word and $$N$$ is the total number of words in the input word list.
  * For each word in the word list, we iterate over its length to find all the intermediate words corresponding to it. Since the length of each word is $$M$$ and we have $$N$$ words, the total number of iterations the algorithm takes to create `all_combo_dict` is $$M \times N$$. Additionally, forming each of the intermediate word takes $$O(M)$$ time because of the substring operation used to create the new string. This adds up to a complexity of $$O({M}^2 \times N)$$.

  * Breadth first search in the worst case might go to each of the $$N$$ words. For each word, we need to examine $$M$$ possible intermediate words/combinations. Notice, we have used the substring operation to find each of the combination. Thus, $$M$$ combinations take $$O({M} ^ 2)$$ time. As a result, the time complexity of BFS traversal would also be $$O({M}^2 \times N)$$.

  Combining the above steps, the overall time complexity of this approach is $$O({M}^2 \times N)$$.

* Space Complexity: $$O({M}^2 \times N)$$.
  * Each word in the word list would have $$M$$ intermediate combinations. To create the `all_combo_dict` dictionary we save an intermediate word as the key and its corresponding original words as the value. Note, for each of $$M$$ intermediate words we save the original word of length $$M$$. This simply means, for every word we would need a space of $${M}^2$$ to save all the transformations corresponding to it. Thus, `all_combo_dict` would need a total space of $$O({M}^2 \times N)$$.
  * `Visited` dictionary would need a space of $$O(M \times N)$$ as each word is of length $$M$$.
  * Queue for BFS in worst case would need a space for all $$O(N)$$ words and this would also result in a space complexity of $$O(M \times N)$$.

  Combining the above steps, the overall space complexity is $$O({M}^2 \times N)$$ + $$O(M * N)$$ + $$O(M * N)$$ = $$O({M}^2 \times N)$$ space.

**Optimization:**
 We can definitely reduce the space complexity of this algorithm by storing the indices corresponding to each word instead of storing the word itself.
<br/>
<br/>

---

#### Approach 2: Bidirectional Breadth First Search

**Intuition**

The graph formed from the nodes in the dictionary might be too big. The search space considered by the breadth first search algorithm depends upon the branching factor of the nodes at each level. If the branching factor remains the same for all the nodes, the search space increases exponentially along with the number of levels. Consider a simple example of a binary tree. With each passing level in a complete binary tree, the number of nodes increase in powers of `2`.

We can considerably cut down the search space of the standard breadth first search algorithm if we launch two simultaneous BFS. One from the `beginWord` and one from the `endWord`. We progress one node at a time from both sides and at any point in time if we find a common node in both the searches, we stop the search. This is known as `bidirectional BFS` and it considerably cuts down on the search space and hence reduces the time and space complexity.

<center>
<img src="../Figures/127/Word_Ladder_3.png" width="600"/>
</center>

**Algorithm**

1. The algorithm is very similar to the standard BFS based approach we saw earlier.
2. The only difference is we now do BFS starting two nodes instead of one. This also changes the termination condition of our search.
3. We now have two visited dictionaries to keep track of nodes visited from the search starting at the respective ends.
4. If we ever find a node/word which is in the visited dictionary of the parallel search we terminate our search, since we have found the meet point of this bidirectional search. It's more like meeting in the middle instead of going all the way through.

    >Termination condition for bidirectional search is finding a word which is already been seen by the parallel search.

5. The shortest transformation sequence is the sum of levels of the meet point node from both the ends. Thus, for every visited node we save its level as value in the visited dictionary.

<center>
<img src="../Figures/127/Word_Ladder_4.png" width="600"/>
</center>


<iframe src="https://leetcode.com/playground/mWFUpZoK/shared" frameBorder="0" width="100%" height="500" name="mWFUpZoK"></iframe>

**Complexity Analysis**

* Time Complexity: $$O({M}^2 \times N)$$, where $$M$$ is the length of words and $$N$$ is the total number of words in the input word list. Similar to one directional, bidirectional also takes $$O({M}^2 \times N)$$ time for finding out all the transformations. But the search time reduces to half, since the two parallel searches meet somewhere in the middle.

* Space Complexity: $$O({M}^2 \times N)$$, to store all $$M$$ transformations for each of the $$N$$ words in the `all_combo_dict` dictionary, same as one directional. But bidirectional reduces the search space. It narrows down because of meeting in the middle.  
<br/>
<br/>

## Accepted Submission (python3)
```python3
class Solution:
    def ladderLength(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: int
        """
        if endWord not in wordList:
            return 0
        dist = 1
        level = [beginWord]
        remaining = set(wordList)
        if beginWord in wordList:
            remaining.remove(beginWord)
        while level:
            newLevel = []
            for word in level:
                if word == endWord:
                    return dist
                for i in range(len(word)):
                    for ch in [c for c in 'abcdefghijklmnopqrstuvwxyz' if c != word[i]]:
                        nextWord = word[:i] + ch + word[i+1:]
                        if nextWord in remaining:
                            newLevel.append(nextWord)
                            remaining.remove(nextWord)
            dist += 1
            level = newLevel
        return 0
```

## Top Discussions
### C++ BFS
- Author: jianchao-li
- Creation Date: Thu Jun 25 2015 00:53:35 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 04:48:04 GMT+0800 (Singapore Standard Time)

<p>
This problem has a nice BFS structure. Let\'s illustrate this using the example in the problem statement.

```
beginWord = "hit",
endWord = "cog",
wordList = ["hot","dot","dog","lot","log","cog"]
```

Since only one letter can be changed at a time, if we start from `"hit"`, we can only change to those words which have exactly one letter different from it (in this case, `"hot"`). Putting in graph-theoretic terms, `"hot"` is a neighbor of `"hit"`. The idea is simpy to start from the `beginWord`, then visit its neighbors, then the non-visited neighbors of its neighbors until we arrive at the `endWord`. This is a typical BFS structure.

```cpp
class Solution {
public:
    int ladderLength(string beginWord, string endWord, vector<string>& wordList) {
        unordered_set<string> dict(wordList.begin(), wordList.end());
        queue<string> todo;
        todo.push(beginWord);
        int ladder = 1;
        while (!todo.empty()) {
            int n = todo.size();
            for (int i = 0; i < n; i++) {
                string word = todo.front();
                todo.pop();
                if (word == endWord) {
                    return ladder;
                }
                dict.erase(word);
                for (int j = 0; j < word.size(); j++) {
                    char c = word[j];
                    for (int k = 0; k < 26; k++) {
                        word[j] = \'a\' + k;
                        if (dict.find(word) != dict.end()) {
                            todo.push(word);
                        }
                     }
                    word[j] = c;
                }
            }
            ladder++;
        }
        return 0;
    }
};
```

The above code starts from a single end `beginWord`. We may also start from the `endWord` simultaneously. Once we meet the same word, we are done. [This link](https://leetcode.com/problems/word-ladder/discuss/40708/Share-my-two-end-BFS-in-C%2B%2B-80ms.) provides such a two-end search solution. I rewrite the code below for better readability. This solution uses two pointers `phead` and `ptail` to switch to the smaller set at each step to save time.

```cpp
class Solution {
public:
    int ladderLength(string beginWord, string endWord, vector<string>& wordList) {
        unordered_set<string> dict(wordList.begin(), wordList.end()), head, tail, *phead, *ptail;
        if (dict.find(endWord) == dict.end()) {
            return 0;
        }
        head.insert(beginWord);
        tail.insert(endWord);
        int ladder = 2;
        while (!head.empty() && !tail.empty()) {
            if (head.size() < tail.size()) {
                phead = &head;
                ptail = &tail;
            } else {
                phead = &tail;
                ptail = &head;
            }
            unordered_set<string> temp;
            for (auto it = phead -> begin(); it != phead -> end(); it++) {    
                string word = *it;
                for (int i = 0; i < word.size(); i++) {
                    char t = word[i];
                    for (int j = 0; j < 26; j++) {
                        word[i] = \'a\' + j;
                        if (ptail -> find(word) != ptail -> end()) {
                            return ladder;
                        }
                        if (dict.find(word) != dict.end()) {
                            temp.insert(word);
                            dict.erase(word);
                        }
                    }
                    word[i] = t;
                }
            }
            ladder++;
            phead -> swap(temp);
        }
        return 0;
    }
};
```
</p>


### Two-end BFS in Java 31ms.
- Author: Moriarty
- Creation Date: Wed Nov 11 2015 14:36:13 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 14:24:14 GMT+0800 (Singapore Standard Time)

<p>
Modified from **[Share my two-end BFS in C++ 80ms.][1]** 

    public class Solution {

    public int ladderLength(String beginWord, String endWord, Set<String> wordList) {
		Set<String> beginSet = new HashSet<String>(), endSet = new HashSet<String>();

		int len = 1;
		int strLen = beginWord.length();
		HashSet<String> visited = new HashSet<String>();
		
		beginSet.add(beginWord);
		endSet.add(endWord);
		while (!beginSet.isEmpty() && !endSet.isEmpty()) {
			if (beginSet.size() > endSet.size()) {
				Set<String> set = beginSet;
				beginSet = endSet;
				endSet = set;
			}

			Set<String> temp = new HashSet<String>();
			for (String word : beginSet) {
				char[] chs = word.toCharArray();

				for (int i = 0; i < chs.length; i++) {
					for (char c = 'a'; c <= 'z'; c++) {
						char old = chs[i];
						chs[i] = c;
						String target = String.valueOf(chs);

						if (endSet.contains(target)) {
							return len + 1;
						}

						if (!visited.contains(target) && wordList.contains(target)) {
							temp.add(target);
							visited.add(target);
						}
						chs[i] = old;
					}
				}
			}

			beginSet = temp;
			len++;
		}
		
		return 0;
	}
    }


  [1]: https://leetcode.com/discuss/28573/share-my-two-end-bfs-in-c-80ms
</p>


### Compact Python solution
- Author: kevinend51
- Creation Date: Fri Apr 15 2016 04:34:28 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 07:46:59 GMT+0800 (Singapore Standard Time)

<p>
    class Solution(object):
        def ladderLength(self, beginWord, endWord, wordList):
            wordList.add(endWord)
            queue = collections.deque([[beginWord, 1]])
            while queue:
                word, length = queue.popleft()
                if word == endWord:
                    return length
                for i in range(len(word)):
                    for c in \'abcdefghijklmnopqrstuvwxyz\':
                        next_word = word[:i] + c + word[i+1:]
                        if next_word in wordList:
                            wordList.remove(next_word)
                            queue.append([next_word, length + 1])
            return 0

(2018/04/29) Since the problem description and argument is changed, I update the solution:
```
class Solution(object):
    def ladderLength(self, beginWord, endWord, wordList):
        wordList = set(wordList)
        queue = collections.deque([[beginWord, 1]])
        while queue:
            word, length = queue.popleft()
            if word == endWord:
                return length
            for i in range(len(word)):
                for c in \'abcdefghijklmnopqrstuvwxyz\':
                    next_word = word[:i] + c + word[i+1:]
                    if next_word in wordList:
                        wordList.remove(next_word)
                        queue.append([next_word, length + 1])
        return 0
```
</p>


