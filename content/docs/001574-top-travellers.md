---
title: "Top Travellers"
weight: 1574
#id: "top-travellers"
---
## Description
<div class="description">
<p>Table: <code>Users</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| name          | varchar |
+---------------+---------+
id is the primary key for this table.
name is the name of the user.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Rides</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| user_id       | int     |
| distance      | int     |
+---------------+---------+
id is the primary key for this table.
user_id is the id of the user who travelled the distance &quot;distance&quot;.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to&nbsp;report the distance travelled by each user.</p>

<p>Return the result table ordered by <code>travelled_distance</code> in <strong>descending order</strong>, if two or more users travelled the same distance, order them by their <code>name</code> in <strong>ascending order</strong>.</p>

<p>The query result format is in the following example.</p>

<p>&nbsp;</p>

<pre>
Users table:
+------+-----------+
| id   | name      |
+------+-----------+
| 1    | Alice     |
| 2    | Bob       |
| 3    | Alex      |
| 4    | Donald    |
| 7    | Lee       |
| 13   | Jonathan  |
| 19   | Elvis     |
+------+-----------+

Rides table:
+------+----------+----------+
| id   | user_id  | distance |
+------+----------+----------+
| 1    | 1        | 120      |
| 2    | 2        | 317      |
| 3    | 3        | 222      |
| 4    | 7        | 100      |
| 5    | 13       | 312      |
| 6    | 19       | 50       |
| 7    | 7        | 120      |
| 8    | 19       | 400      |
| 9    | 7        | 230      |
+------+----------+----------+

Result table:
+----------+--------------------+
| name     | travelled_distance |
+----------+--------------------+
| Elvis    | 450                |
| Lee      | 450                |
| Bob      | 317                |
| Jonathan | 312                |
| Alex     | 222                |
| Alice    | 120                |
| Donald   | 0                  |
+----------+--------------------+
Elvis and Lee travelled 450 miles, Elvis is the top traveller as his name is alphabetically smaller than Lee.
Bob, Jonathan, Alex and Alice have only one ride and we just order them by the total distances of the ride.
Donald didn&#39;t have any rides, the distance travelled by him is 0.
</pre>

</div>

## Tags


## Companies
- Point72 - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 405ms MySQL Solution
- Author: Ginny47
- Creation Date: Wed Apr 15 2020 12:46:50 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Apr 15 2020 12:46:50 GMT+0800 (Singapore Standard Time)

<p>
select u.name, ifnull(sum(r.distance), 0) as travelled_distance
from users u 
left join rides r 
on u.id = r.user_id
group by r.user_id
order by travelled_distance desc, u.name asc
</p>


### MySQL solution
- Author: ruddysimonpour
- Creation Date: Fri Jun 26 2020 17:56:13 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 26 2020 17:56:13 GMT+0800 (Singapore Standard Time)

<p>
```
select u.name, coalesce(sum(r.distance),0) as "travelled_distance"
from users as u
left join rides as r
on u.id = r.user_id
group by u.name
order by travelled_distance desc, u.name
```
</p>


### 3 different solutions
- Author: jatin7
- Creation Date: Thu May 28 2020 13:07:13 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 28 2020 13:07:13 GMT+0800 (Singapore Standard Time)

<p>
```
-- using left outer join
select u.name, isnull(sum(r.distance),0) as travelled_distance
from users u left outer join rides r
on u.id = r.user_id
group by u.name
order by travelled_distance desc, name asc


--using coorelated sub-query
select u.name, (select isnull(sum(r.distance),0)
                from rides r
                where u.id = r.user_id) as travelled_distance
from users u
order by travelled_distance desc, name asc

--using cte
with cte
as
(
    select r.user_id, sum(r.distance) as travelled_distance
    from rides r
    group by r.user_id
)
    select u.name, isnull(c.travelled_distance,0) as travelled_distance
    from users u left outer join cte c
    on u.id = c.user_id
    order by travelled_distance desc, name asc

</p>


