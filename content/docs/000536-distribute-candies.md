---
title: "Distribute Candies"
weight: 536
#id: "distribute-candies"
---
## Description
<div class="description">
<p>You have <code>n</code>&nbsp;<code>candies</code>, the <code>i<sup>th</sup></code> candy is of type <code>candies[i]</code>.</p>

<p>You want to distribute the candies equally between a sister and a brother so that each of them gets <code>n / 2</code>&nbsp;candies (<code>n</code> is even). The sister loves to collect different types of candies, so you want to give her the <strong>maximum number of different types</strong> of candies.</p>

<p>Return <em>the maximum number of different types</em> of candies you can give to the sister.</p>

<ol>
</ol>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> candies = [1,1,2,2,3,3]
<strong>Output:</strong> 3
<strong>Explanation:</strong>
There are three different kinds of candies (1, 2 and 3), and two candies for each kind.
Optimal distribution: The sister has candies [1,2,3] and the brother has candies [1,2,3], too. 
The sister has three different kinds of candies. 
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> candies = [1,1,2,3]
<strong>Output:</strong> 2
<strong>Explanation:</strong> For example, the sister has candies [2,3] and the brother has candies [1,1]. 
The sister has two different kinds of candies, the brother has only one kind of candies.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> candies = [1,1]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> candies = [1,11]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> candies = [2,2]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == candies.length</code></li>
	<li><code>2 &lt;= n &lt;= 10^4</code></li>
	<li><code>n</code>&nbsp;is even.</li>
	<li><code>-10^5 &lt;= candies[i] &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Microsoft - 2 (taggedByAdmin: false)
- LiveRamp - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force

**Algorithm**

The brute force approach is really simple. We can generate all the permutations of the given $$nums$$ array representing the candies and determine the number of unique elements in the first half of the generated array.

In order to determine the number of unique elements in the first half of the array, we put all the required elements in a set and count the number of elements in the set. We count such unique elements in the first half of the generated arrays for all the permutations possible and return the size of the largest set.

<iframe src="https://leetcode.com/playground/UdTbUMNu/shared" frameBorder="0" width="100%" height="497" name="UdTbUMNu"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n!)$$. A total of $$n!$$ permutations are possible for $$nums$$ array of size $$n$$. 

* Space complexity : $$O(n)$$. The depth of the recursion tree can go upto $$n$$.

---
#### Approach 2: Better Brute Force

**Algorithm**

Before looking into the idea behind this approach, firstly we need to observe one point. The maximum no. of unique candies which the girl can obtain could be atmost $$n/2$$, where $$n$$ refers to the number of candies. Further, in case the number of unique candies are below $$n/2$$, to maximize the number of unique candies that the girl will obtain, we'll assign all the unique candies to the girl. Thus, in such a case, the number of unique candies the girl gets is equal to the total number of unique candies in the given $$candies$$ array. 

Now, let's look at the idea behind this approach. We need to find the total number of unique candies in the given $$candies$$ array. One way to find the number of unique candies is to traverse over the given $$candies$$ array. Whenever we encounter an element, say $$candies[j]$$, we can mark all the elements which are the same as $$candies[j]$$ as invalid and increment the count of unique elements by 1.

Thus, we need to do such markings for all the elements of $$candies$$ array. At the end, $$count$$ gives the required number of unique candies that can be given to the girl. Further, the value to be returned is given by: $$\text{min}(\frac{n}{2}, count)$$. Instead of finding the $$\text{min}$$, we can stop the traversal over the given $$candies$$ array as soon as the $$count$$ exceeds $$\frac{n}{2}$$. 

<iframe src="https://leetcode.com/playground/aGu6JeKy/shared" frameBorder="0" width="100%" height="310" name="aGu6JeKy"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. We traverse over all the elements of $$candies$$ for every new element found. In the worst case, we do so for every element of $$candies$$ array. $$n$$ refers to the size of $$candies$$ array.

* Space complexity : $$O(1)$$. Constant space is used.

---

#### Approach 3: Using Sorting

**Algorithm**

We can sort the given $$candies$$ array and find out the elements which are unique by comparing the adjacent elements of the sorted array. For every new element found(which isn't the same as the previous element), we need to update the $$count$$. At the end, we can return the required result as $$\text{min}(n/2, count)$$, as discussed in the previous approach.

<iframe src="https://leetcode.com/playground/Xco64dCA/shared" frameBorder="0" width="100%" height="225" name="Xco64dCA"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n\log n)$$. Sorting takes $$O(n\log n)$$ time.

* Space complexity : $$O(1)$$. Constant space is used.

---

#### Approach 4: Using Set

**Algorithm**

Another way to find the number of unique elements is to traverse over all the elements of the given $$candies$$ array and keep on putting the elements in a set. By the property of a set, it will contain only unique elements. At the end, we can count the number of elements in the set, given by, say $$count$$. The value to be returned will again be given by $$\text{min}(count, n/2)$$, as discussed in previous approaches. Here, $$n$$ refers to the size of the $$candies$$ array.

<iframe src="https://leetcode.com/playground/nepiHUoh/shared" frameBorder="0" width="100%" height="208" name="nepiHUoh"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. The entire $$candies$$ array is traversed only once. Here, $$n$$ refers to the size of $$candies$$ array.

* Space complexity : $$O(n)$$. $$set$$ will be of size $$n$$ in the worst case.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun May 07 2017 11:08:19 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 22:57:33 GMT+0800 (Singapore Standard Time)

<p>
There are ```len(set(candies))``` unique candies, and the sister picks only ```len(candies) / 2``` of them, so she can't have more than this amount.

For example, if there are 5 unique candies, then if she is picking 4 candies, she will take 4 unique ones.  If she is picking 7 candies, then she will only take 5 unique ones.

```
def distributeCandies(self, candies):
    return min(len(candies) / 2, len(set(candies)))
```
</p>


### Java Solution, 3 lines, HashSet
- Author: shawngao
- Creation Date: Sun May 07 2017 11:06:23 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 15 2018 15:32:04 GMT+0800 (Singapore Standard Time)

<p>
Thanks @wmcalyj , modified to use HashSet.
```
public class Solution {
    public int distributeCandies(int[] candies) {
        Set<Integer> kinds = new HashSet<>();
        for (int candy : candies) kinds.add(candy);
        return kinds.size() >= candies.length / 2 ? candies.length / 2 : kinds.size();
    }
}
```
</p>


### C++, bitset, beats 99.60%
- Author: zestypanda
- Creation Date: Fri May 12 2017 07:08:48 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 12 2017 07:08:48 GMT+0800 (Singapore Standard Time)

<p>
The idea is to use biset as hash table instead of unordered_set.
```
int distributeCandies(vector<int>& candies) {
        bitset<200001> hash;
        int count = 0;
        for (int i : candies) {
            if (!hash.test(i+100000)) {
               count++;
               hash.set(i+100000);
            }
        }
        int n = candies.size();
        return min(count, n/2);
    }
```
</p>


