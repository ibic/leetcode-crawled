---
title: "Uncommon Words from Two Sentences"
weight: 834
#id: "uncommon-words-from-two-sentences"
---
## Description
<div class="description">
<p>We are given two sentences <code>A</code> and <code>B</code>.&nbsp; (A <em>sentence</em>&nbsp;is a string of space separated words.&nbsp; Each <em>word</em> consists only of lowercase letters.)</p>

<p>A word is <em>uncommon</em>&nbsp;if it appears exactly once in one of the sentences, and does not appear in the other sentence.</p>

<p>Return a list of all uncommon words.&nbsp;</p>

<p>You may return the list in any order.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">&quot;this apple is sweet&quot;</span>, B = <span id="example-input-1-2">&quot;this apple is sour&quot;</span>
<strong>Output: </strong><span id="example-output-1">[&quot;sweet&quot;,&quot;sour&quot;]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">&quot;apple apple&quot;</span>, B = <span id="example-input-2-2">&quot;banana&quot;</span>
<strong>Output: </strong><span id="example-output-2">[&quot;banana&quot;]</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= A.length &lt;= 200</code></li>
	<li><code>0 &lt;= B.length &lt;= 200</code></li>
	<li><code>A</code> and <code>B</code> both contain only spaces and lowercase letters.</li>
</ol>
</div>
</div>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Counting

**Intuition and Algorithm**

Every uncommon word occurs exactly once in total.  We can count the number of occurrences of every word, then return ones that occur exactly once.

<iframe src="https://leetcode.com/playground/kxSLUGni/shared" frameBorder="0" width="100%" height="327" name="kxSLUGni"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(M + N)$$, where $$M, N$$ are the lengths of `A` and `B` respectively.

* Space Complexity:  $$O(M + N)$$, the space used by `count`.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Easy Solution with Explanation
- Author: lee215
- Creation Date: Sun Aug 12 2018 11:04:35 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 11:21:40 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**:
No matter how many sentences,
**uncommon word = words that appears only once**

I recall another similar problem:
[819. Most Common Word](https://leetcode.com/problems/most-common-word/discuss/123854/)
So I open my solutions there and copy some codes.

**Explanation**:

Two steps:
Count words occurrence to a HashMap<string, int> `count`.
Loop on the hashmap, find words that appears only once.

**C++:**
```
    vector<string> uncommonFromSentences(string A, string B) {
        unordered_map<string, int> count;
        istringstream iss(A + " " + B);
        while (iss >> A) count[A]++;
        vector<string> res;
        for (auto w: count)
            if (w.second == 1)
                res.push_back(w.first);
        return res;
    }
```

**Java:**
```
    public String[] uncommonFromSentences(String A, String B) {
        Map<String, Integer> count = new HashMap<>();
        for (String w : (A + " " + B).split(" "))
            count.put(w, count.getOrDefault(w, 0) + 1);
        ArrayList<String> res = new ArrayList<>();
        for (String w : count.keySet())
            if (count.get(w) == 1)
                res.add(w);
        return res.toArray(new String[0]);
    }
```
**Python:**
```
    def uncommonFromSentences(self, A, B):
        c = collections.Counter((A + " " + B).split())
        return [w for w in c if c[w] == 1]
```

</p>


### [Java/Python 3] HashMap/Counter and HashSets.
- Author: rock
- Creation Date: Sun Aug 12 2018 11:16:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 02 2020 01:07:41 GMT+0800 (Singapore Standard Time)

<p>
**Method 1:**
1. Count the number of each String in A and B;
2. Filter out those with occurrence more than 1.

```
    public String[] uncommonFromSentences(String A, String B) {
        Map<String, Integer> count = new HashMap<>();
        for (String s : (A + " " + B).split("\\s")) { 
             count.put(s, count.getOrDefault(s, 0) + 1); 
        }
        return count.entrySet().stream().filter(e -> e.getValue() == 1).map(e -> e.getKey()).toArray(String[]::new);
    }
```
```python
    def uncommonFromSentences(self, A: str, B: str) -> List[str]:
        return [s for s, v in collections.Counter(A.split() + B.split()).items() if v == 1]
```

----

**Method 2:**

Use 2 HashSets to store distinct and common Strings.
1. If encounter a String that is not in either Set, add it to distinct;
2. Otherwise, remove it from the distinct and add it to the common one.

In other words:
a) first check if the com Set contains the String s, 
          if yes, go to b); 
					if no, check if distinct Set can add s, if yes, no further operation; if no, go to b).

b) remove s from distinct and add it the com Set.

```
    public String[] uncommonFromSentences(String A, String B) {
        Set<String> distinct = new HashSet<>(), com = new HashSet<>();
        for (String s : (A + " " + B).split("\\s")) {
            if (com.contains(s) || !distinct.add(s)) { 
                distinct.remove(s); com.add(s); 
            }
        }
        return distinct.toArray(new String[0]);
    }
```
</p>


### C++ 100% with unordered_map
- Author: groothedde
- Creation Date: Tue Oct 09 2018 00:51:42 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 22:41:52 GMT+0800 (Singapore Standard Time)

<p>
Combine two strings separated with a string, istringstream and getline split up the string so that we can count the occurrences in an unordered_map, then simply list the entries with an occurrence of 1 in a vector.

```cpp
class Solution {
public:
    vector<string> uncommonFromSentences(string A, string B) {
        // combine the two strings for simplicity and construct an istringstream
        istringstream combined(A + " " + B);
        
        // our word will be put in this variable by getline
        string word;
        
        // keep track of word occurrences
        unordered_map<string, int> counts;
        
        // get the next word and increase its count in our map
        while (getline(combined, word, \' \'))
            counts[word] += 1;
        
        // put the words with an occurrence of 1 in our result vector
        vector<string> result;
        for(auto &p : counts) {
            if(p.second == 1)
                result.push_back(p.first);
        }
        
        return result;
    }
};
```
</p>


