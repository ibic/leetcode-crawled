---
title: "Power of Four"
weight: 325
#id: "power-of-four"
---
## Description
<div class="description">
<p>Given an integer (signed 32 bits), write a function to check whether it is a power of 4.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">16</span>
<strong>Output: </strong><span id="example-output-1">true</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">5</span>
<strong>Output: </strong><span id="example-output-2">false</span></pre>
</div>

<p><b>Follow up</b>: Could you solve it without loops/recursion?</p>
</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Two Sigma - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Overview

**Prerequisites**

This bitwise trick will be used as something already known:

- How to check if the number is a power of two : `x > 0 and x & (x - 1) == 0`.

    Please check the article [Power of Two](https://leetcode.com/articles/power-of-two/)
    for the detailed explanation.

**Intuition**

There is an obvious $$\mathcal{O}(\log N)$$ time solution
and we're not going to discuss it here.

<iframe src="https://leetcode.com/playground/LkDCTd6H/shared" frameBorder="0" width="100%" height="174" name="LkDCTd6H"></iframe>

Let's discuss $$\mathcal{O}(1)$$ time and $$\mathcal{O}(1)$$ space solutions only.
<br />
<br />


---
#### Approach 1: Brute Force + Precomputations

Let's precompute all possible answers, as we once did for the problem 
[Nth Tribonacci Number](https://leetcode.com/articles/n-th-tribonacci-number/).

Input number is known to be [signed 32 bits integer](https://en.wikipedia.org/wiki/Integer_(computer_science)#Common_long_integer_sizes),
i.e. $$x \le 2^{31} - 1$$. Hence the max power of four to be considered is
$$[\log_4\left(2^{31} - 1\right)] = 15$$. 
Voila, here is all 16 possible 
answers: $$4^0$$, $$4^1$$, $$4^2$$, ..., $$4^{15}$$. Let's precompute them all,
and then during the runtime just check if input number is in the list of answers. 

<iframe src="https://leetcode.com/playground/aGJqGn53/shared" frameBorder="0" width="100%" height="378" name="aGJqGn53"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(1)$$.

* Space complexity: $$\mathcal{O}(1)$$.
<br />
<br />


---
#### Approach 2: Math

If num is a power of four $$x = 4^a$$, then 
$$a = \log_4 x = \frac{1}{2}\log_2 x$$ is an integer. 
Hence let's simply check if $$\log_2 x$$ is an even number.

<iframe src="https://leetcode.com/playground/j6uRnyrz/shared" frameBorder="0" width="100%" height="140" name="j6uRnyrz"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$.

* Space complexity : $$\mathcal{O}(1)$$.
<br />
<br />


---
#### Approach 3: Bit Manipulation

Let's first check if num is a power of two: 
`x > 0 and x & (x - 1) == 0`. 

Now the problem is to distinguish between even powers of two (when $$x$$ 
is a power of four) and odd powers of two (when $$x$$ is _not_ a power of four).
In binary representation both cases are single 1-bit followed by zeros.

> What is the difference? In the first case (power of four), 
1-bit is at even position: bit 0, bit 2, bit 4, etc. In the
second case, at odd position. 
  
![fig](../Figures/342/odd_even.png)

Hence power of four would make a zero in a bitwise AND with number 
$$(101010...10)_2$$:

$$4^a \land (101010...10)_2 == 0$$

> How long should be $$(101010...10)_2$$ if $$x$$ is a signed integer?
32 bits. To write shorter, in 8 charaters instead of 32, it's common to use 
[hexadecimal](https://en.wikipedia.org/wiki/Hexadecimal) representation:
$$(101010...10)_2 = (aaaaaaaa)_{16}$$.

$$x \land (aaaaaaaa)_{16} == 0$$

<iframe src="https://leetcode.com/playground/AHMkexcn/shared" frameBorder="0" width="100%" height="140" name="AHMkexcn"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$.

* Space complexity : $$\mathcal{O}(1)$$.
<br />
<br />


---
#### Approach 4: Bit Manipulation + Math

Let's first check if $$x$$ is a power of two: 
`x > 0 and x & (x - 1) == 0`. 
Now one could be sure that $$x = 2^a$$.
Though $$x$$ is a power of four only if $$a$$ is even.

Next step is to consider both cases $$a = 2k$$ and $$a = 2k + 1$$,
and to compute $$x$$ modulo after division by three:

$$(2^{2k} \mod 3) = (4^k \mod 3) = ((3 + 1)^k \mod 3) = 1$$

$$((2^{2k + 1}) \mod 3) = ((2 \times 4^k) \mod 3) = ((2 \times(3 + 1)^k) \mod 3) = 2$$

> If $$x$$ is a power of two and `x % 3 == 1`, then $$x$$ _is_ a power of four.

<iframe src="https://leetcode.com/playground/JEjTwxXw/shared" frameBorder="0" width="100%" height="140" name="JEjTwxXw"></iframe>

**How this works: mod arithmetic**

To show the idea, let's compute $$x = 2^{2k} \mod 3$$.

First, $$2^{2k} = {2^2}^k = 4^k$$. Second, since $$4 = 3 + 1$$, $$x$$
could be rewritten as 

$$x = ((3 + 1)^k \mod 3)$$

Let's decompose 

$$(3 + 1)^k = (3 + 1) \times (3 + 1)^{k - 1} = 3 \times (3 + 1)^{k - 1} + (3 + 1)^{k - 1}$$. 

The first term is divisible by 3, i.e. $$(3 \times (3 + 1)^{k - 1}) \mod 3 = 0$$. Hence

$$x = ((3 + 1)^{k - 1} \mod 3)$$

One could continue like this k -> k - 1 -> k - 2 -> ... -> 1 and finally rewrite $$x$$ as

$$x = ((3 + 1)^1 \mod 3) = 1$$. 

The job is done. Now $$y = 2^{2k + 1} \mod 3$$ is simple, because if $$x \mod 3 = 1$$,
then $$y \mod 3 = 2x \mod 3 = 2$$.

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$.

* Space complexity : $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 1-line (cheating for the purpose of not using loops)
- Author: aiscong
- Creation Date: Mon Apr 18 2016 06:36:01 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 23:24:50 GMT+0800 (Singapore Standard Time)

<p>
        public boolean isPowerOfFour(int num) {
            return num > 0 && (num&(num-1)) == 0 && (num & 0x55555555) != 0;
            //0x55555555 is to get rid of those power of 2 but not power of 4
            //so that the single 1 bit always appears at the odd position 
        }
</p>


### 1 line C++ solution without confusing bit manipulations
- Author: Beibeibei
- Creation Date: Mon Apr 18 2016 16:31:36 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 14:57:17 GMT+0800 (Singapore Standard Time)

<p>
    bool isPowerOfFour(int num) {
        return num > 0 && (num & (num - 1)) == 0 && (num - 1) % 3 == 0;
    }
</p>


### O(1) one-line solution without loops
- Author: diegozeng
- Creation Date: Mon Apr 18 2016 06:00:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 03:22:50 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public boolean isPowerOfFour(int num) {
            return (num > 0) && ((num & (num - 1)) == 0) && ((num & 0x55555555) == num);
        }
    }

The basic idea is from power of 2, We can use "n&(n-1) == 0" to determine if n is power of 2. For power of 4, the additional restriction is that in binary form, the only "1" should always located at the odd position.  For example, 4^0 = 1, 4^1 = 100, 4^2 = 10000. So we can use "num & 0x55555555==num" to check if "1" is located at the odd position.
</p>


