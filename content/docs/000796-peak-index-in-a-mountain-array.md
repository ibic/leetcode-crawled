---
title: "Peak Index in a Mountain Array"
weight: 796
#id: "peak-index-in-a-mountain-array"
---
## Description
<div class="description">
<p>Let&#39;s call an array <code>arr</code> a <strong>mountain</strong>&nbsp;if the following properties hold:</p>

<ul>
	<li><code>arr.length &gt;= 3</code></li>
	<li>There exists some <code>i</code> with&nbsp;<code>0 &lt; i&nbsp;&lt; arr.length - 1</code>&nbsp;such that:
	<ul>
		<li><code>arr[0] &lt; arr[1] &lt; ... arr[i-1] &lt; arr[i] </code></li>
		<li><code>arr[i] &gt; arr[i+1] &gt; ... &gt; arr[arr.length - 1]</code></li>
	</ul>
	</li>
</ul>

<p>Given an integer array arr that is <strong>guaranteed</strong> to be&nbsp;a mountain, return any&nbsp;<code>i</code>&nbsp;such that&nbsp;<code>arr[0] &lt; arr[1] &lt; ... arr[i - 1] &lt; arr[i] &gt; arr[i + 1] &gt; ... &gt; arr[arr.length - 1]</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> arr = [0,1,0]
<strong>Output:</strong> 1
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> arr = [0,2,1,0]
<strong>Output:</strong> 1
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> arr = [0,10,5,2]
<strong>Output:</strong> 1
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> arr = [3,4,5,1]
<strong>Output:</strong> 2
</pre><p><strong>Example 5:</strong></p>
<pre><strong>Input:</strong> arr = [24,69,100,99,79,78,67,36,26,19]
<strong>Output:</strong> 2
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>3 &lt;= arr.length &lt;= 10<sup>4</sup></code></li>
	<li><code>0 &lt;= arr[i] &lt;= 10<sup>6</sup></code></li>
	<li><code>arr</code> is <strong>guaranteed</strong> to be a mountain array.</li>
</ul>

</div>

## Tags
- Binary Search (binary-search)

## Companies
- Bloomberg - 2 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: true)
- Amazon - 5 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Quora - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach 1: Linear Scan

**Intuition and Algorithm**

The mountain increases until it doesn't.  The point at which it stops increasing is the peak.

<iframe src="https://leetcode.com/playground/nyrN6Rc6/shared" frameBorder="0" width="100%" height="174" name="nyrN6Rc6"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.


---
#### Approach 2: Binary Search

**Intuition and Algorithm**

The comparison `A[i] < A[i+1]` in a mountain array looks like `[True, True, True, ..., True, False, False, ..., False]`: 1 or more boolean `True`s, followed by 1 or more boolean `False`.  For example, in the mountain array `[1, 2, 3, 4, 1]`, the comparisons `A[i] < A[i+1]` would be `True, True, True, False`.

We can binary search over this array of comparisons, to find the largest index `i` such that `A[i] < A[i+1]`.  For more on *binary search*, see the [LeetCode explore topic here.](https://leetcode.com/explore/learn/card/binary-search/)

<iframe src="https://leetcode.com/playground/udiXhFoT/shared" frameBorder="0" width="100%" height="276" name="udiXhFoT"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\log N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.

## Accepted Submission (java)
```java
class Solution {
    public int peakIndexInMountainArray(int[] A) {
        int len = A.length;
        int start = 0;
        int stop = len;
        while (true) {
            int pos = (start + stop) / 2;
            if (A[pos] > A[pos+1] && A[pos] > A[pos-1]) {
                return pos;
            } else if (A[pos] < A[pos+1]) {
                start = pos;
            } else {
                stop = pos;
            }
        }
    }
}
```

## Top Discussions
### [C++/Java/Python] Better than Binary Search
- Author: lee215
- Creation Date: Sun Jun 17 2018 11:04:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 23 2019 12:06:13 GMT+0800 (Singapore Standard Time)

<p>
## Approach 1, index of max, O(N)

**C++:**
```
    int peakIndexInMountainArray(vector<int> A) {
       return max_element(A.begin(), A.end()) - A.begin();
    }
```

**Python:**
```
    def peakIndexInMountainArray(self, A):
        return A.index(max(A))
```

## Approach 2, For loop to find the first `A[i] > A[i + i]`, O(N)
**Java:**
```
    public int peakIndexInMountainArray(int[] A) {
        for (int i = 1; i + 1 < A.length; ++i) if (A[i] > A[i + 1]) return i;
        // for (int i = A.length - 1; i > 0; --i) if (A[i] > A[i - 1]) return i;
        return 0;
    }
```

**C++:**
```
    int peakIndexInMountainArray2(vector<int> A) {
        for (int i = 1; i + 1 < A.size(); ++i) if (A[i] > A[i + 1]) return i;
    }
```

## Approach 3, Binary search, O(logN)

**C++:**
```
    int peakIndexInMountainArray(vector<int> A) {
        int l = 0, r = A.size() - 1, mid;
        while (l < r) {
            mid = (l + r) / 2;
            if (A[mid] < A[mid + 1])
                l = mid + 1;
            else
                r = mid;
        }
        return l;
    }
```

**Java:**
```
    public int peakIndexInMountainArray(int[] A) {
        int l = 0, r = A.length - 1, m;
        while (l < r) {
            m = (l + r) / 2;
            if (A[m] < A[m + 1])
                l = m + 1;
            else
                r = m;
        }
        return l;
    }
```
**Python:**
```
    def peakIndexInMountainArray(self, A):
        l, r = 0, len(A) - 1
        while l < r:
            m = (l + r) / 2
            if A[m] < A[m + 1]:
                l = m + 1
            else:
                r = m
        return l
```

**Follow-up:**
Can you do faster than binary search?

## Approach 4, Golden-section search

It\'s gurentee only one peak, we can apply golden-section search.

```
    def peakIndexInMountainArray(self, A):
        def gold1(l, r):
            return l + int(round((r - l) * 0.382))

        def gold2(l, r):
            return l + int(round((r - l) * 0.618))
        l, r = 0, len(A) - 1
        x1, x2 = gold1(l, r), gold2(l, r)
        while x1 < x2:
            if A[x1] < A[x2]:
                l = x1
                x1 = x2
                x2 = gold1(x1, r)
            else:
                r = x2
                x2 = x1
                x1 = gold2(l, x2)
        return A.index(max(A[l:r + 1]), l)
```
</p>


### Help!  I don't understand the question.
- Author: natalie11594
- Creation Date: Mon Jul 01 2019 01:09:08 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 01 2019 01:09:08 GMT+0800 (Singapore Standard Time)

<p>
I\'ve been having issues understanding what the question is even asking for several problems that are worded in the way this one is.  What exactly is the question asking?  Could someone rephrase it for me?
</p>


### [Java/Python 3] O(n) and O(log(n)) codes
- Author: rock
- Creation Date: Sun Jun 17 2018 11:02:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 13 2020 19:44:07 GMT+0800 (Singapore Standard Time)

<p>
Method 1: Straightforwad O(n) code:
```java
    public int peakIndexInMountainArray(int[] A) {
        for (int i = 1; i < A.length; ++i) {
            if (A[i - 1] < A[i] && A[i] > A[i + 1]) { return i; }
        }
        return -1; // no peak.
    }
```
Method 2: Based on the judge condition in O(n) code, using binary search we can implement O(log(n)) code:
```java
    public int peakIndexInMountainArray(int[] A) {
        int lo = 0, hi = A.length - 1;
        while (lo < hi) {
            int mid = lo + (hi - lo) / 2;
            if (A[mid] < A[mid + 1]) { // peak index is after mid.
                lo = mid + 1;
            }else if (A[mid -1] > A[mid]) { // peak index is before mid.
                hi = mid;
            }else { // peak index is mid.
                return mid;
            }
        }
        return -1; // no peak.
    }
```
Since the problem is guaranteed to have one and only one peak, we can simplify the above O(n) and O(log(n)) codes:
Method 1:  O(n)
```java
    public int peakIndexInMountainArray(int[] A) {
        for (int i = 1; i < A.length; ++i) {
            if A[i] > A[i + 1]) { return i; }
        }
        return -1; // no peak.
    }
```
```python
    def peakIndexInMountainArray(self, A: List[int]) -> int:
        for i in range(1, len(A)):
            if A[i] > A[i + 1]:
                return i
```
Method 2: O(log(n))
```java
    public int peakIndexInMountainArray(int[] A) {
        int lo = 0, hi = A.length - 1;
        while (lo < hi) {
            int mid = lo + (hi - lo) / 2;
            if (A[mid] < A[mid + 1]) { lo = mid + 1; } // peak index is after mid.
            else{ hi = mid; } // peak index <= mid.
        }
        return lo; // found peak index.
    }
```
```python
    def peakIndexInMountainArray(self, A: List[int]) -> int:
        lo, hi = 0, len(A)
        while lo < hi:
            mid = lo + (hi - lo) // 2
            if A[mid] < A[mid + 1]:
                lo = mid + 1
            else:
                hi = mid 
        return lo
```
</p>


