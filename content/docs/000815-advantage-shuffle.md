---
title: "Advantage Shuffle"
weight: 815
#id: "advantage-shuffle"
---
## Description
<div class="description">
<p>Given two arrays <code>A</code> and <code>B</code> of equal size, the <em>advantage of <code>A</code> with respect to <code>B</code></em> is the number of indices <code>i</code>&nbsp;for which <code>A[i] &gt; B[i]</code>.</p>

<p>Return <strong>any</strong> permutation of <code>A</code> that maximizes its advantage with respect to <code>B</code>.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[2,7,11,15]</span>, B = <span id="example-input-1-2">[1,10,4,11]</span>
<strong>Output: </strong><span id="example-output-1">[2,11,7,15]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">[12,24,8,32]</span>, B = <span id="example-input-2-2">[13,25,32,11]</span>
<strong>Output: </strong><span id="example-output-2">[24,32,8,12]</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length = B.length &lt;= 10000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 10^9</code></li>
	<li><code>0 &lt;= B[i] &lt;= 10^9</code></li>
</ol>
</div>
</div>

</div>

## Tags
- Array (array)
- Greedy (greedy)

## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach 1: Greedy

**Intuition**

If the smallest card `a` in `A` beats the smallest card `b` in `B`, we should pair them.  Otherwise, `a` is useless for our score, as it can't beat any cards.

Why should we pair `a` and `b` if `a > b`?  Because every card in `A` is larger than `b`, any card we place in front of `b` will score a point.  We might as well use the weakest card to pair with `b` as it makes the rest of the cards in `A` strictly larger, and thus have more potential to score points.

**Algorithm**

We can use the above intuition to create a greedy approach.  The current smallest card to beat in `B` will always be `b = sortedB[j]`.  For each card `a` in `sortedA`, we will either have `a` beat that card `b` (put `a` into `assigned[b]`), or throw `a` out (put `a` into `remaining`).

Afterwards, we can use our annotations `assigned` and `remaining` to reconstruct the answer.  Please see the comments for more details.


<iframe src="https://leetcode.com/playground/uqaehqf3/shared" frameBorder="0" width="100%" height="500" name="uqaehqf3"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the length of `A` and `B`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### JAVA Greedy 6 lines with Explanation
- Author: caraxin
- Creation Date: Sun Jul 15 2018 11:01:00 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 19:28:05 GMT+0800 (Singapore Standard Time)

<p>
Contest version below:
```
class Solution {
    public int[] advantageCount(int[] A, int[] B) {
        Arrays.sort(A);
        int n=A.length;
        int[] res= new int[n];
        PriorityQueue<int[]> pq= new PriorityQueue<>((a,b)->b[0]-a[0]);
        for (int i=0; i<n; i++) pq.add(new int[]{B[i], i});
        int lo=0, hi=n-1;
        while(!pq.isEmpty()){
            int[] cur= pq.poll();
            int idx=cur[1], val=cur[0];
            if (A[hi]>val) res[idx]=A[hi--];
            else res[idx]=A[lo++];
        }
        return res;
    }  
}
```
![image](https://s3-lc-upload.s3.amazonaws.com/users/caraxin/image_1531625378.png)
Shorter version just for fun (promise me never use it during an interview):
```
class Solution {
    public int[] advantageCount(int[] A, int[] B) {
        Arrays.sort(A);
        PriorityQueue<int[]> pq= new PriorityQueue<>((a,b)->b[0]-a[0]);
        for (int i=0; i<B.length; i++) pq.add(new int[]{B[i], i});
        int lo=0, hi=A.length-1, res[] = new int[A.length];
        while(!pq.isEmpty()) res[pq.peek()[1]]=pq.poll()[0]<A[hi]?A[hi--]:A[lo++];
        return res;
    }
}
```
If you are chasing speed, it\'s time to forget lambda, beats 90%:
```
class Solution {
    public int[] advantageCount(int[] A, int[] B) {
        Arrays.sort(A);
        int n=A.length;
        int[] res= new int[n];
        PriorityQueue<int[]> pq= new PriorityQueue<>(new Comparator<int[]>(){
            public int compare(int[] a, int[] b){
                return b[0]-a[0];
            }
        });
        for (int i=0; i<n; i++) pq.add(new int[]{B[i], i});
        int lo=0, hi=n-1;
        while(!pq.isEmpty()){
            int[] cur= pq.poll();
            int idx=cur[1], val=cur[0];
            if (A[hi]>val) res[idx]=A[hi--];
            else res[idx]=A[lo++];
        }
        return res;
    }  
}
```
Happy coding.

</p>


### [Python] Greedy Solution Using Sort
- Author: lee215
- Creation Date: Sun Jul 15 2018 11:07:14 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 08:46:06 GMT+0800 (Singapore Standard Time)

<p>
1. Sort `A`
2. For every element `b` in B from big to small,
if `A[-1] > b`, then this `b` will take `A.pop()`
3. Assign all elements in B an element from `take` or the rest of `A`.

**Time Complexity**:
O(NlogN)

```
    def advantageCount(self, A, B):
        A = sorted(A)
        take = collections.defaultdict(list)
        for b in sorted(B)[::-1]:
            if b < A[-1]: take[b].append(A.pop())
        return [(take[b] or A).pop() for b in B]
```

</p>


### C++ 6 lines greedy, O(n log n)
- Author: votrubac
- Creation Date: Sun Jul 15 2018 11:03:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 22:40:32 GMT+0800 (Singapore Standard Time)

<p>
For each B[i], we select the smallest number in A that is greater than B[i]. If there are no such number, we select the smalest number in A.

I am usign multiset to sort and keep track of numbers in A. After a number is selected, we need to remove it from the multiset (erase by iterator takes a constant time).
```
vector<int> advantageCount(vector<int>& A, vector<int>& B) {
  multiset<int> s(begin(A), end(A));
  for (auto i = 0; i < B.size(); ++i) {
    auto p = *s.rbegin() <= B[i] ? s.begin() : s.upper_bound(B[i]);
    A[i] = *p;
    s.erase(p);
  }
  return A;
}
```
</p>


