---
title: "Find Leaves of Binary Tree"
weight: 349
#id: "find-leaves-of-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree, collect a tree&#39;s nodes as if you were doing this: Collect and remove all leaves, repeat until the tree is empty.</p>

<p>&nbsp;</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,2,3,4,5]
&nbsp; 
&nbsp;         </span>1
         / \
        2   3
       / \     
      4   5    

<strong>Output: </strong><span id="example-output-1">[[4,5,3],[2],[1]]</span>
</pre>

<p>&nbsp;</p>

<p><strong>Explanation:</strong></p>

<p>1. Removing the leaves <code>[4,5,3]</code> would result in this tree:</p>

<pre>
          1
         / 
        2          
</pre>

<p>&nbsp;</p>

<p>2. Now removing the leaf <code>[2]</code> would result in this tree:</p>

<pre>
          1          
</pre>

<p>&nbsp;</p>

<p>3. Now removing the leaf <code>[1]</code> would result in the empty tree:</p>

<pre>
          []         
</pre>
[[3,5,4],[2],[1]], [[3,4,5],[2],[1]], etc, are also consider correct answers since per each level it doesn&#39;t matter the order on which elements are returned.
</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Pocket Gems - 3 (taggedByAdmin: false)
- Atlassian - 3 (taggedByAdmin: false)
- LinkedIn - 9 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 10 lines simple Java solution using recursion with explanation
- Author: sky-xu
- Creation Date: Sat Jun 25 2016 08:41:49 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 08:20:52 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public List<List<Integer>> findLeaves(TreeNode root) {
            List<List<Integer>> res = new ArrayList<>();
            height(root, res);
            return res;
        }
        private int height(TreeNode node, List<List<Integer>> res){
            if(null==node)  return -1;
            int level = 1 + Math.max(height(node.left, res), height(node.right, res));
            if(res.size()<level+1)  res.add(new ArrayList<>());
            res.get(level).add(node.val);
            return level;
        }
    }
For this question we need to take bottom-up approach. The key is to find the height of each node. Here the definition of height is:
<cite>The height of a node is the number of edges from the node to the deepest leaf. --[CMU 15-121 Binary Trees](https://www.cs.cmu.edu/~adamchik/15-121/lectures/Trees/trees.html)</cite>

I used a helper function to return the height of current node. According to the definition, the height of leaf is 0. ```h(node) = 1 + max(h(node.left), h(node.right))```.
The height of a node is also the its index in the result list (res). For example, leaves, whose heights are 0, are stored in res[0]. Once we find the height of a node, we can put it directly into the result.

UPDATE:
Thanks @adrianliu0729 for pointing out that my previous code does not actually remove leaves. I added one line ```node.left = node.right = null;``` to remove visited nodes

UPDATE:
There seems to be some debate over whether we need to actually "remove" leaves from the input tree. Anyway, it is just a matter of one line code. In the actual interview, just confirm with the interviewer whether removal is required.
</p>


### Java backtracking O(n) time O(n) space No hashing!
- Author: xuyirui
- Creation Date: Sat Jun 25 2016 11:06:45 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 24 2018 07:09:09 GMT+0800 (Singapore Standard Time)

<p>
The essential of problem is not to find the leaves, but group leaves of same level together and also to cut the tree. This is the exact role backtracking plays. The helper function returns the level which is the distance from its furthest subtree leaf to root, which helps to identify which group the root belongs to 

        public class Solution {
            public List<List<Integer>> findLeaves(TreeNode root) {
                List<List<Integer>> list = new ArrayList<>();
                findLeavesHelper(list, root);
                return list;
            }
            
      // return the level of root
            private int findLeavesHelper(List<List<Integer>> list, TreeNode root) {
                if (root == null) {
                    return -1;
                }
                int leftLevel = findLeavesHelper(list, root.left);
                int rightLevel = findLeavesHelper(list, root.right);
                int level = Math.max(leftLevel, rightLevel) + 1;
                if (list.size() == level) {
                    list.add(new ArrayList<>());
                }
                list.get(level).add(root.val);
                root.left = root.right = null;
                return level;
            }
        }
</p>


### 1 ms Easy understand Java Solution
- Author: gwenwoods
- Creation Date: Mon Jun 27 2016 08:13:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 13:29:03 GMT+0800 (Singapore Standard Time)

<p>

    public class Solution {
        public List<List<Integer>> findLeaves(TreeNode root) {
            
            List<List<Integer>> leavesList = new ArrayList< List<Integer>>();
            List<Integer> leaves = new ArrayList<Integer>();
            
            while(root != null) {
                if(isLeave(root, leaves)) root = null;
                leavesList.add(leaves);
                leaves = new ArrayList<Integer>();
            }
            return leavesList;
        }
        
        public boolean isLeave(TreeNode node, List<Integer> leaves) {
            
            if (node.left == null && node.right == null) {
                leaves.add(node.val);
                return true;
            }
            
            if (node.left != null) {
                 if(isLeave(node.left, leaves))  node.left = null;
            }
            
            if (node.right != null) {
                 if(isLeave(node.right, leaves)) node.right = null;
            }
            
            return false;
        }
    }
</p>


