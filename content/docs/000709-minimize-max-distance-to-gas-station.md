---
title: "Minimize Max Distance to Gas Station"
weight: 709
#id: "minimize-max-distance-to-gas-station"
---
## Description
<div class="description">
<p>On a horizontal number line, we have gas stations at positions <code>stations[0], stations[1], ..., stations[N-1]</code>, where <code>N = stations.length</code>.</p>

<p>Now, we add <code>K</code> more gas stations so that <strong>D</strong>, the maximum distance between adjacent gas stations, is minimized.</p>

<p>Return the smallest possible value of <strong>D</strong>.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> stations = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], K = 9
<strong>Output:</strong> 0.500000
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li><code>stations.length</code> will be an integer in range <code>[10, 2000]</code>.</li>
	<li><code>stations[i]</code> will be an integer in range <code>[0, 10^8]</code>.</li>
	<li><code>K</code> will be an integer in range <code>[1, 10^6]</code>.</li>
	<li>Answers within <code>10^-6</code> of the true value will be accepted as correct.</li>
</ol>

</div>

## Tags
- Binary Search (binary-search)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Dynamic Programming [Memory Limit Exceeded]

**Intuition**

Let `dp[n][k]` be the answer for adding `k` more gas stations to the first `n` intervals of stations.  We can develop a recurrence expressing `dp[n][k]` in terms of `dp[x][y]` with smaller `(x, y)`.

**Algorithm**

Say the `i`th interval is `deltas[i] = stations[i+1] - stations[i]`.  We want to find `dp[n+1][k]` as a recursion.  We can put `x` gas stations in the `n+1`th interval for a best distance of `deltas[n+1] / (x+1)`, then the rest of the intervals can be solved with an answer of `dp[n][k-x]`.  The answer is the minimum of these over all `x`.

From this recursion, we can develop a dynamic programming solution.


<iframe src="https://leetcode.com/playground/Qtn5v8rj/shared" frameBorder="0" width="100%" height="446" name="Qtn5v8rj"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N K^2)$$, where $$N$$ is the length of `stations`.

* Space Complexity: $$O(N K)$$, the size of `dp`.

---
#### Approach #2: Brute Force [Time Limit Exceeded]

**Intuition**

As in *Approach #1*, let's look at `deltas`, the distances between adjacent gas stations.

Let's repeatedly add a gas station to the current largest interval, so that we add `K` of them total.  This greedy approach is correct because if we left it alone, then our answer never goes down from that point on.

**Algorithm**

To find the largest current interval, we keep track of how many parts `count[i]` the `i`th (original) interval has become.  (For example, if we added 2 gas stations to it total, there will be 3 parts.)  The new largest interval on this section of road will be `deltas[i] / count[i]`.


<iframe src="https://leetcode.com/playground/3YkyBvfy/shared" frameBorder="0" width="100%" height="500" name="3YkyBvfy"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N K)$$, where $$N$$ is the length of `stations`.

* Space Complexity: $$O(N)$$, the size of `deltas` and `count`.

---
#### Approach #3: Heap [Time Limit Exceeded]

**Intuition**

Following the intuition of *Approach #2*, if we are taking a repeated maximum, we can replace this with a heap data structure, which performs repeated maximum more efficiently.

**Algorithm**

As in *Approach #2*, let's repeatedly add a gas station to the next larget interval `K` times.  We use a heap to know which interval is largest.  In Python, we use a negative priority to simulate a max heap with a min heap.

<iframe src="https://leetcode.com/playground/BHgkBbYV/shared" frameBorder="0" width="100%" height="361" name="BHgkBbYV"></iframe>

**Complexity Analysis**

Let $$N$$ be the length of stations, and $$K$$ be the number of gas stations to add.

* Time Complexity:  $$O(N + K \log N)$$

    - First of all, we scan the stations to obtain a list of intervals between each adjacent stations.

    - Then it takes another $$O(N)$$ to build a heap out of the list of intervals.

    - Finally, we repeatedly pop out an element and push in a new element into the heap, which takes $$O(\log N)$$ respectively. In total, we repeat this step for $$K$$ times (_i.e._ to add $$K$$ gas stations).

    - To sum up, the overall time complexity of the algorithm is $$O(N) + O(N) + O(K \cdot \log N) = O(N + K\cdot \log N)$$.


* Space Complexity: $$O(N)$$, the size of `deltas` and `count`.


---
#### Approach #4: Binary Search [Accepted]

**Intuition**

Let's ask `possible(D)`: with `K` (or less) gas stations, can we make every adjacent distance between gas stations at most `D`?  This function is monotone, so we can apply a binary search to find $$D^{\text{*}}$$.

**Algorithm**

 More specifically, there exists some `D*` (the answer) for which `possible(d) = False` when `d < D*` and `possible(d) = True` when `d > D*`.  Binary searching a monotone function is a typical technique, so let's focus on the function `possible(D)`.

 When we have some interval like `X = stations[i+1] - stations[i]`, we'll need to use $$\lfloor \frac{X}{D} \rfloor$$ gas stations to ensure every subinterval has size less than `D`.  This is independent of other intervals, so in total we'll need to use $$\sum_i \lfloor \frac{X_i}{D} \rfloor$$ gas stations.  If this is at most `K`, then it is possible to make every adjacent distance between gas stations at most `D`.

<iframe src="https://leetcode.com/playground/ebGLsF3C/shared" frameBorder="0" width="100%" height="395" name="ebGLsF3C"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log W)$$, where $$N$$ is the length of `stations`, and $$W = 10^{14}$$ is the range of possible answers ($$10^8$$), divided by the acceptable level of precision ($$10^{-6}$$).

* Space Complexity: $$O(1)$$ in additional space complexity.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Binary Search 
- Author: lee215
- Creation Date: Sun Jan 28 2018 12:16:02 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 20:34:36 GMT+0800 (Singapore Standard Time)

<p>
**Why did I use s binary search?** 
In fact there are some similar problems on Leetcode so that is part of experience.
Secondly, I got a hint from "Answers within 10^-6 of the true value will be accepted as correct.". The first solution I tried was binary search.
Because binary search may not find exact value but it can approach the true answer.

**Explanation of solution**
Now we are using binary search to find the smallest possible value of D.
I initilze ```left = 0``` and ```right = the distance between the first and the last station```
```count``` is the number of gas station we need to make it possible.
```if count > K```, it means ```mid``` is too small to realize using only K more stations.
```if count <= K```, it means ```mid``` is possible and we can continue to find a bigger one.
When ```left + 1e-6 >= right```, it means the answer within 10^-6 of the true value and it will be accepted. 

**Time complexity**:
O(NlogM), where N is station length and M is st[N - 1] - st[0]



C++
```
double minmaxGasDist(vector<int>& st, int K) {
        int count, N = st.size();
        double left = 0, right = st[N - 1] - st[0], mid;
        while (left + 1e-6 < right) {
            mid = (left + right) / 2;
            count = 0;
            for (int i = 0; i < N - 1; ++i)
                count += ceil((st[i + 1] - st[i]) / mid) - 1;
            if (count > K) left = mid;
            else right = mid;
        }
        return right;
    }
```

Java:
```
public double minmaxGasDist(int[] st, int K) {
        int count, N = st.length;
        double left = 0, right = st[N - 1] - st[0], mid;

        while (left +1e-6 < right) {
            mid = (left + right) / 2;
            count = 0;
            for (int i = 0; i < N - 1; ++i)
                count += Math.ceil((st[i + 1] - st[i]) / mid) - 1;
            if (count > K) left = mid;
            else right = mid;
        }
        return right;
    }
```

Python:
```
def minmaxGasDist(self, st, K):
        left, right = 1e-6, st[-1] - st[0]
        while left + 1e-6 < right:
            mid = (left + right) / 2
            count = 0
            for a, b in zip(st, st[1:]):
                count += math.ceil((b - a) / mid) - 1
            if count > K:
                left = mid
            else:
                right = mid
        return right
</p>


### Approach the problem using the "trial and error" algorithm
- Author: fun4LeetCode
- Creation Date: Sun Jan 28 2018 14:53:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 14:49:16 GMT+0800 (Singapore Standard Time)

<p>
**Note** this is an adaptation of my original post [here](https://discuss.leetcode.com/topic/109015/approach-the-problem-using-the-trial-and-error-algorithm) with updated solution for this problem. Hopefully it can shed some light on these type of problems. First here is a list of LeetCode problems that can be solved using the trial and error algorithm (you're welcome to add more):

1. [774. Minimize Max Distance to Gas Station](https://leetcode.com/problems/minimize-max-distance-to-gas-station/description/)

2. [719. Find K-th Smallest Pair Distance](https://leetcode.com/problems/find-k-th-smallest-pair-distance/description/)

3. [668. Kth Smallest Number in Multiplication Table](https://leetcode.com/problems/kth-smallest-number-in-multiplication-table/description/)

4. [644. Maximum Average Subarray II](https://leetcode.com/articles/maximum-average-subarray-ii/)

---

Well, normally we would refrain from using the naive [trial and error](https://en.wikipedia.org/wiki/Trial_and_error) algorithm for solving problems since it generally leads to bad time performance. However, there are situations where this naive algorithm may outperform other more sophisticated solutions, and LeetCode does have a few such problems (listed at the end of this post -- ironically they are all "hard" problems). So I figure it might be a good idea to bring it up and describe a general procedure for applying this algorithm.

The basic idea for the trial and error algorithm is actually very simple and summarized below:

`Step 1`: Construct a candidate solution.
`Step 2`: Verify if it meets our requirements.
`Step 3`: If it does, accept the solution; else discard it and repeat from `Step 1`.

However, to make this algorithm work efficiently, the following two conditions need to be true:

**Condition 1**: We have an efficient verification algorithm in `Step 2`;
**Condition 2**: The search space formed by all candidate solutions is small or we have efficient ways to search this space if it is large.

The first condition ensures that each verification operation can be done quickly while the second condition limits the total number of such operations that need to be done. The two combined will guarantee that we have an efficient trial and error algorithm (which also means if any of them cannot be satisfied, you should probably not even consider this algorithm).

Now let's look at this problem: `Minimize Max Distance to Gas Station`, and see how we can apply the above trial and error algorithm.

---

`I -- Construct a candidate solution`

To construct a candidate solution, we need to understand first what the desired solution is. The problem description requires we output the **minimum value** of the maximum distance between adjacent gas stations. This value is nothing more than a non-negative double value, since the distance between any adjacent stations cannot be negative and in general it should be of double type. Therefore our candidate solution should also be a non-negative double value.

---

`II -- Search space formed by all the candidate solutions`

Let `max` be the maximum value of the distances between any adjacent stations without adding those additional stations, then our desired solution should lie within the range `[0, max]`. This is because adding extra stations (and placing them at proper positions) can only reduce the maximum distance between any adjacent stations. Therefore our search space will be `[0, max]` (or the upper bound should be at least `max`).

---

`III -- Verify a given candidate solution`

This is the key part of this trial and error algorithm. So given a candidate double value `d`, how do we determine if it can be the minimum value of the maximum distance between any adjacent stations after adding `K` extra stations? The answer is by counting.

If `d` can be the minimum value of the maximum distance between any adjacent stations after adding `K` extra stations, then the following two conditions should be true at the same time:

1. The total number of stations we can add between all adjacent stations cannot go beyond `K`. In other words, assume we added `cnt_i` stations in between the pair of adjacent stations `(i, i+1)`, then we should have `sum(cnt_i) <= K`. 

2. For each pair of adjacent stations `(i, i+1)`, the minimum value of the maximum distance between adjacent stations after adding `cnt_i` additional stations cannot go beyond `d`. If the original distance between station `i` and `i+1` is `d_i = stations[i+1] - stations[i]`, then after adding `cnt_i` additional stations, the minimum value of maximum distance between any adjacent stations we can obtain is `d_i / (cnt_i + 1)`, which is achieved by placing those stations evenly in between stations `i` and `i+1`. Therefore we require: `d_i / (cnt_i + 1) <= d`, which is equivalent to `cnt_i >= (d_i/d - 1)`.

So you can see that the two conditions are actually "contradictory" to each other: to meet condition **1**, we want `cnt_i` to be as small as possible; but to meet condition **2**, we'd like it to be as large as possible. So in practice, we can alway choose the set of smallest `cnt_i`'s that satisfy the second condition and double check if they also meet the first condition. If they do, then `d` will set an upper limit on the final minimum value obtainable; otherwise `d` will set a lower limit on this minimum value.

This verification algorithm runs at `O(n)`, where `n` is the length of the `stations` array. This is acceptable if we can walk the search space very efficiently (which can be done at the order of `O(log(max/step))`, with `step = 10^-6`). In particular, this is much faster than the straightforward `O(Klogn)` solution where we add the stations one by one in a greedy manner (i.e., always reduce the current maximum distance first), given that `K` could be orders of magnitude larger than `n` (note this greedy algorithm can be optimized to run at `O(nlogn)`, see wannacry89's post [here](https://discuss.leetcode.com/topic/118742/java-proportional-assignment-priorityqueue-greedy-for-leftover-o-n-log-n-easy-to-understand)).

---

`IV -- How to walk the search space efficiently`

Up to this point, we know the search space, know how to construct the candidate solution and how to verify it by counting, we still need one last piece for the puzzle: how to walk the search space.

Of course we can do the naive linear walk by trying each double value from `0` up to `max` at a step of `10^-6` and choose the first double value `d` such that `sum(d_i/d - 1) <= K`. The time complexity will be `O(n * max/step)`. However, given that `max/step` can be much larger than `K`, this algorithm could end up being much worse than the aforementioned `O(Klogn)` solution.

The key observation here is that the candidate solutions are ordered naturally in ascending order, so a binary search is possible. Another fact is the non-decreasing property of the `sum` function: give two double values `d1` and `d2` such that `d1 < d2`, then `sum(d_i/d1 - 1) >= sum(d_i/d2 - 1)`. So here is what a binary walk of the search space may look like:

1. Let `[l, r]` be the current search space that is initialized as `l = 0`, `r = max`.

2. As long as `r - l >=  10^-6`, compute the middle point `d = (l + r) / 2` and evaluate `sum(d_i/d - 1)`.

3. If `sum(d_i/d - 1) <= K`, we throw away the right half of current search space by setting `r = d`; else we throw away the left half by setting `l = d`. Then go to step 2.

---

`V -- Putting everything together, aka, solutions`

Despite the above lengthy analyses, the final solution is much simpler to write once you understand it. Here is the Java program for the trial and error algorithm. The time complexity is `O(n*log(max/step))` and space complexity is `O(1)`.

```
public double minmaxGasDist(int[] stations, int K) {
    double l = 0, r = stations[stations.length - 1] - stations[0]; // assuming the positions are sorted, or alternatively we can go through the stations array to find the maximum distance between all adjacent stations
    
    while (r - l >= 1e-6) {
        double d = (r + l) / 2;
        
        int cnt = 0;
        
        for (int i = 0; i < stations.length - 1; i++) {
            cnt += Math.ceil((stations[i + 1] - stations[i]) / d) - 1;
        }
        
        if (cnt <= K) {
            r = d;
        } else {
            l = d;
        }
    }
        
    return l;
}
```

---

**Final remarks**: This post is just a reminder to you that the trial and error algorithm is worth trying if you find all other common solutions suffer severely from bad time or space performance. Also it's always recommended to perform a quick evaluation of the search space size and potential verification algorithm to estimate the complexity before you are fully committed to this algorithm.
</p>


### [Java] Proportional Assignment + PriorityQueue Greedy for leftover  O(n log n) - easy to understand
- Author: wannacry89
- Creation Date: Sun Jan 28 2018 15:33:44 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 08:40:45 GMT+0800 (Singapore Standard Time)

<p>
At first I thought this was a sequential station add problem, not concurrent station add problem. Quickly you see that it is the concurrent station add problem when you try the former and it fails. The idea now is that we view each inter-station interval as having a fraction of the total ground covered. That is, we assign the extra stations to the intervals according to how much of the total ground they cover. Then, we have up to N leftover because we are always taking the floor (integer) when calculating these assignments. So we take care of the remainder greedily using priorityqueue (O(remainingmax*log (intervals)) == O(n log n)). Also I assume the stations are originally given potentially out of order, so the only way I can consider them as adjacency intervals it to sort. This also costs O(n log n).

```
class Solution {
    public double minmaxGasDist(int[] stations, int K) {

        Arrays.sort(stations);
        PriorityQueue<Interval> que = new PriorityQueue<Interval>(new Comparator<Interval>() {
            public int compare(Interval a, Interval b) {

                double diff = a.distance() - b.distance();
                if (diff < 0) { return +1; }
                else if (diff > 0) { return -1; }
                else { return 0; }
            }
        });

        double leftToRight = stations[stations.length-1] - stations[0];
        int remaining = K;

        for (int i = 0; i < stations.length-1; i++) {
            int numInsertions = (int)(K*(((double)(stations[i+1]-stations[i]))/leftToRight));
            que.add(new Interval(stations[i], stations[i+1], numInsertions));
            remaining -= numInsertions;
        }

        while (remaining > 0) {
            Interval interval = que.poll();
            interval.numInsertions++;
            que.add(interval);
            remaining--;
        }

        Interval last = que.poll();
        return last.distance();

    }

    class Interval {
        double left;
        double right;
        int numInsertions;
        double distance() { return (right - left)/  ((double)(numInsertions+1)) ; }
        Interval(double left, double right, int numInsertions) { this.left = left; this.right = right; this.numInsertions = numInsertions; }
    }
}
```

p.s. I had originally tried using only PriorityQueue + greedy approach alone, without the pre-assignment based on proportion. This was TLE.

The binary approach should not be faster than this, and this one is easier to understand IMO.
</p>


