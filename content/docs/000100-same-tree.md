---
title: "Same Tree"
weight: 100
#id: "same-tree"
---
## Description
<div class="description">
<p>Given two binary trees, write a function to check if they are the same or not.</p>

<p>Two binary trees are considered the same if they are structurally identical and the nodes have the same value.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong>     1         1
          / \       / \
         2   3     2   3

        [1,2,3],   [1,2,3]

<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong>     1         1
          /           \
         2             2

        [1,2],     [1,null,2]

<strong>Output:</strong> false
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong>     1         1
          / \       / \
         2   1     1   2

        [1,2,1],   [1,1,2]

<strong>Output:</strong> false
</pre>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Google - 5 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Recursion

**Intuition**

The simplest strategy here is to use recursion. 
Check if `p` and `q` nodes are not `None`, and their values are equal.
If all checks are OK, do the same for the child nodes
recursively.

**Implementation**

!?!../Documents/100_LIS.json:1000,373!?!

<iframe src="https://leetcode.com/playground/nxYY8VVo/shared" frameBorder="0" width="100%" height="395" name="nxYY8VVo"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$, 
where N is a number of nodes in the tree, since one visits
each node exactly once.
 
* Space complexity : $$\mathcal{O}(\log(N))$$ in the best case of completely 
balanced tree and $$\mathcal{O}(N)$$ in the worst case
of completely unbalanced tree, to keep a recursion stack.
<br />
<br />


---
#### Approach 2: Iteration

**Intuition**

Start from the root and then at each iteration 
pop the current node out of the deque. Then do the same checks as in
 the approach 1 :

- `p` and `p` are not `None`, 

- `p.val` is equal to `q.val`,

and if checks are OK, push the child nodes. 

**Implementation**

<iframe src="https://leetcode.com/playground/F42oaJNc/shared" frameBorder="0" width="100%" height="500" name="F42oaJNc"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since each node is visited
exactly once.
 
* Space complexity : $$\mathcal{O}(\log(N))$$ in the best case of completely 
balanced tree and $$\mathcal{O}(N)$$ in the worst case
of completely unbalanced tree, to keep a deque.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Five line Java solution with recursion
- Author: usmeng
- Creation Date: Tue Nov 04 2014 10:24:40 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 00:07:57 GMT+0800 (Singapore Standard Time)

<p>
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if(p == null && q == null) return true;
        if(p == null || q == null) return false;
        if(p.val == q.val)
            return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
        return false;
    }
</p>


### Shortest+simplest Python
- Author: StefanPochmann
- Creation Date: Sat May 23 2015 05:34:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 19:55:01 GMT+0800 (Singapore Standard Time)

<p>
The "proper" way:

    def isSameTree(self, p, q):
        if p and q:
            return p.val == q.val and self.isSameTree(p.left, q.left) and self.isSameTree(p.right, q.right)
        return p is q

The "tupleify" way:

    def isSameTree(self, p, q):
        def t(n):
            return n and (n.val, t(n.left), t(n.right))
        return t(p) == t(q)

The first way as one-liner:

    def isSameTree(self, p, q):
        return p and q and p.val == q.val and all(map(self.isSameTree, (p.left, p.right), (q.left, q.right))) or p is q
</p>


### Here's a C++ recursion solution in minimal lines of code
- Author: satyakam
- Creation Date: Sat Dec 13 2014 10:48:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 22:06:22 GMT+0800 (Singapore Standard Time)

<p>
    //
    // Algorithm for the recursion:
    // 1)
    // If one of the node is NULL then return the equality result of p an q.
    // This boils down to if both are NULL then return true, 
    // but if one of them is NULL but not the other one then return false
    // 2)
    // At this point both root nodes represent valid pointers.
    // Return true if the root nodes have same value and 
    // the left tree of the roots are same (recursion)
    // and the right tree of the roots are same (recursion). 
    // Otherwise return false. 
    //
    
    bool isSameTree(TreeNode *p, TreeNode *q) {
        if (p == NULL || q == NULL) return (p == q);
        return (p->val == q->val && isSameTree(p->left, q->left) && isSameTree(p->right, q->right));
    }
</p>


