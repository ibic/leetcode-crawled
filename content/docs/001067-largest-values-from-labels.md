---
title: "Largest Values From Labels"
weight: 1067
#id: "largest-values-from-labels"
---
## Description
<div class="description">
<p>We have a set of items: the <code>i</code>-th item has value <code>values[i]</code> and label <code>labels[i]</code>.</p>

<p>Then, we choose&nbsp;a subset <code>S</code> of these items, such that:</p>

<ul>
	<li><code>|S| &lt;= num_wanted</code></li>
	<li>For every label <code>L</code>, the number of items in <code>S</code> with&nbsp;label <code>L</code> is <code>&lt;= use_limit</code>.</li>
</ul>

<p>Return the largest possible sum of the subset <code>S</code>.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>values = <span id="example-input-1-1">[5,4,3,2,1]</span>, labels = <span id="example-input-1-2">[1,1,2,2,3]</span>, <code>num_wanted </code>= <span id="example-input-1-3">3</span>, use_limit = <span id="example-input-1-4">1</span>
<strong>Output: </strong><span id="example-output-1">9</span>
<strong>Explanation: </strong>The subset chosen is the first, third, and fifth item.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>values = <span id="example-input-2-1">[5,4,3,2,1]</span>, labels = <span id="example-input-2-2">[1,3,3,3,2]</span>, <code>num_wanted </code>= <span id="example-input-2-3">3</span>, use_limit = <span id="example-input-2-4">2</span>
<strong>Output: </strong><span id="example-output-2">12</span>
<strong>Explanation: </strong>The subset chosen is the first, second, and third item.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>values = <span id="example-input-3-1">[9,8,8,7,6]</span>, labels = <span id="example-input-3-2">[0,0,0,1,1]</span>, <code>num_wanted </code>= <span id="example-input-3-3">3</span>, use_limit = <span id="example-input-3-4">1</span>
<strong>Output:</strong>&nbsp;16
<strong>Explanation: </strong>The subset chosen is the first and fourth item.
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong>values = <span id="example-input-4-1">[9,8,8,7,6]</span>, labels = <span id="example-input-4-2">[0,0,0,1,1]</span>, <code>num_wanted </code>= <span id="example-input-4-3">3</span>, use_limit = <span id="example-input-4-4">2</span>
<strong>Output: </strong><span id="example-output-4">24</span>
<strong>Explanation: </strong>The subset chosen is the first, second, and fourth item.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= values.length == labels.length &lt;= 20000</code></li>
	<li><code>0 &lt;= values[i], labels[i]&nbsp;&lt;= 20000</code></li>
	<li><code>1 &lt;= num_wanted, use_limit&nbsp;&lt;= values.length</code></li>
</ol>
</div>
</div>
</div>
</div>
</div>

## Tags
- Hash Table (hash-table)
- Greedy (greedy)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### I spend an hour to understand what the topic mean!
- Author: Oterman
- Creation Date: Sun Jun 16 2019 12:04:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 16 2019 12:05:35 GMT+0800 (Singapore Standard Time)

<p>
what a fucccccccccccccck!

make me verrrrrry angry!
</p>


### Question Explanation and Simple Solution | Java | 100%
- Author: mayank-kt9
- Creation Date: Sun Jun 16 2019 17:45:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 16 2019 18:08:17 GMT+0800 (Singapore Standard Time)

<p>
Lets understand the question 
Here, we need to find a subset where, the length of the subset will be less than equal to num_wanted.

**Example**: values = [5,4,3,2,1], labels = [1,1,2,2,3], num_wanted = 3, use_limit = 1

In this example, subset size <= 3 (as num_wanted = 3)
Listing members in each label
Label 1 -> [5,4]
Label 2 -> [3,2]
Label 3 -> [1]

Now from each label we can use only 1 element because use_limit for each label is 1.
The question asks us to find the largest possible sum of the subset, so we take from each label group, the largest element available.
So in this example, 
from label 1 : we take 5,
from label 2: we take 3
from label 3: we take 1 (since label 1 has only one element)
5+3+1=9

**Example**
We can see in given example : 
Input: values = [9,8,8,7,6], labels = [0,0,0,1,1], num_wanted = 3, use_limit = 1
Output: 16
Explanation: The subset chosen is the first and fourth item.
Listing members in each label
Label 0 -> [9,8,8]
Label 1 -> [7,6]
Here we are allowed to take 3 elements for the subset(num_wanted=3) but we cannot take more than two because the use limit for each label group is ONE(use_limit = 1) and there are only two label groups. Hence we take maximum element from each label group i.e. - 9+7 = 16

**Example**
Input: values = [5,4,3,2,1], labels = [1,3,3,3,2], num_wanted = 3, use_limit = 2
Output: 12
Listing members in each label
Label 1 -> [5]
Label 3 -> [4,3,2]
Label 2 -> [1]

Here use_limit is 2 hence we are allowed to take two elements from each label group,
Hence we take 5 from label 1 and [4, 3] from label 2 that satisfies our need for the subset of length <= 3

**Approach** : 

Hashmap that will mantain the count of use of each label, where key will be label and value will be count of that label used.
Taking 2D array to store value and its corresponding label, Sorting it based on values in descending order. 
Traversing each element of the array and adding to the subset and updating the label count  for that element in the hashmap.

Below is the **working solution in Java** :

```
class Solution {
    public int largestValsFromLabels(int[] values, int[] labels, int num_wanted, int use_limit) {
        HashMap<Integer, Integer> hash_label = new HashMap<Integer, Integer>();

        // create a hashmap for maintaining the use_limit of each label group
		for (int i : labels) {
			hash_label.put(i, 0);
		}

		int size = values.length;
		int[][] val_lab = new int[size][2];

        // creating a 2D array which has values and labels corresponding to the values
		for (int i = 0; i < size; i++) {
			val_lab[i][0] = values[i];
			val_lab[i][1] = labels[i];
		}

        // sorting the array in descending order based on the values from column 0
		Arrays.sort(val_lab, new Comparator<int[]>() {
			public int compare(final int[] entry1, final int[] entry2) {

				if (entry1[0] < entry2[0])
					return 1;
				else
					return -1;
			}
		});
		int sum = 0;

		for (int i = 0; i < size; i++) {
			int val = val_lab[i][0];
			int lab = val_lab[i][1];
        // if label usage less than use_limit and subset size is less than num_wanted, include array item in the subset
			if (num_wanted > 0 && hash_label.get(lab) < use_limit) {

				sum += val;
				hash_label.put(lab, hash_label.get(lab) + 1);
				num_wanted--;
			}
		}

		return sum;
    }
}
```

Correct me if I am wrong somewhere 


</p>


### C++ Greedy
- Author: votrubac
- Creation Date: Sun Jun 16 2019 12:05:22 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 17 2019 13:45:24 GMT+0800 (Singapore Standard Time)

<p>
Sort all labels by value. Then, start with the largest value, greedily pick labels. Track how many labels we have used in ```m```, and do not pick that label if we reached the limit.
```
int largestValsFromLabels(vector<int>& vs, vector<int>& ls, int wanted, int limit, int res = 0) {
  multimap<int, int> s;
  unordered_map<int, int> m;
  for (auto i = 0; i < vs.size(); ++i) s.insert({vs[i], ls[i]});
  for (auto it = rbegin(s); it != rend(s) && wanted > 0; ++it) {
    if (++m[it->second] <= limit) {
      res += it->first;
      --wanted;
    }
  }
  return res;
}
```
</p>


