---
title: "Unique Paths III"
weight: 931
#id: "unique-paths-iii"
---
## Description
<div class="description">
<p>On a 2-dimensional&nbsp;<code>grid</code>, there are 4 types of squares:</p>

<ul>
	<li><code>1</code> represents the starting square.&nbsp; There is exactly one starting square.</li>
	<li><code>2</code> represents the ending square.&nbsp; There is exactly one ending square.</li>
	<li><code>0</code> represents empty squares we can walk over.</li>
	<li><code>-1</code> represents obstacles that we cannot walk over.</li>
</ul>

<p>Return the number of 4-directional walks&nbsp;from the starting square to the ending square, that <strong>walk over every non-obstacle square&nbsp;exactly once</strong>.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[1,0,0,0],[0,0,0,0],[0,0,2,-1]]</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>We have the following two paths: 
1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2)
2. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2)</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[[1,0,0,0],[0,0,0,0],[0,0,0,2]]</span>
<strong>Output: </strong><span id="example-output-2">4</span>
<strong>Explanation: </strong>We have the following four paths: 
1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2),(2,3)
2. (0,0),(0,1),(1,1),(1,0),(2,0),(2,1),(2,2),(1,2),(0,2),(0,3),(1,3),(2,3)
3. (0,0),(1,0),(2,0),(2,1),(2,2),(1,2),(1,1),(0,1),(0,2),(0,3),(1,3),(2,3)
4. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2),(2,3)</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[[0,1],[2,0]]</span>
<strong>Output: </strong><span id="example-output-3">0</span>
<strong>Explanation: </strong>
There is no path that walks over every empty square exactly once.
Note that the starting and ending square can be anywhere in the grid.
</pre>
</div>
</div>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= grid.length * grid[0].length &lt;= 20</code></li>
</ol>
</div>

## Tags
- Backtracking (backtracking)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- LimeBike - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Overview

Whenever we see the context of grid traversal, the technique of backtracking or DFS (Depth-First Search) should ring a bell.

In terms of this problem, it fits the bill perfectly, with a _canonical_ setting, unlike another similar problem called [robot room cleaner](https://leetcode.com/problems/robot-room-cleaner/) which has certain twists.

>As a reminder, [backtracking](https://en.wikipedia.org/wiki/Backtracking) is a _general_ algorithm for finding all (or some) solutions to some problems with constraints.
It incrementally builds candidates to the solutions, and abandons a candidate as soon as it determines that the candidate cannot possibly lead to a solution.

In this article, we will showcase how to apply the backtracking algorithm to solve this problem.

---
#### Approach 1: Backtracking

**Intuition**

We can consider backtracking as a state machine, where we start off from an initial state, each action we take will move the state from one to another, and there should be some final state where we reach our goal.

As a result, let us first clarify the initial and the final states of the problem.

- **Initial State**

    - There are different types of squares/cells in a grid.
    
    - There are an origin and a destination cell, which are not given explicitly.

    - Initially, all the cells are not **_visited_**.


- **Final State**

    - We reach the destination cell, _i.e._ cell filled with the value `2`.

    - We have visited all the non-obstacle cells, including the empty cells (_i.e._ filled with `0`) and the initial cell (_i.e._ `1`).

>With the above definition, we can then translate the problem as finding all paths that can lead us from the initial state to the final state.

![state machine](../Figures/980/980_state_machine.png)

More specifically, we could summarise the steps to implement the backtracking algorithm for this problem in the following pseudo code.

```python

    def backtrack(cell):
        1. if we arrive at the final state:
             path_count ++
             return

        2. mark the current cell as visited

        3. for next_cell in 4 directions:
             if next_cell is not visited and non-obstacle:
                 backtrack(next_cell)

        4. unmark the current cell
```

![map](../Figures/980/980_map.png)

**Algorithm**

As one can see, backtracking is more of a methodology to solve a specific type of problems.
For a backtracking problem, it would not be exaggerating to say that there are a thousand backtracking implementations in a thousand people's eyes, as one would find out in the implementation later.

Here we would simply show one example of implementation, following the pseudo code shown in the intuition section.

<iframe src="https://leetcode.com/playground/ff4ZoL4Z/shared" frameBorder="0" width="100%" height="500" name="ff4ZoL4Z"></iframe>

Here we would like to highlight some important design decisions we took in the above implementation.
As one can imagine, with different decisions, one would have variations of backtracking implementations.

- **In-place Modification**

    - This is an important technique that allows us to save some space in the algorithm.

    - In order to mark the cell as **_visited_**, often the case we use some matrix or hashtable with boolean values to keep track of the state of each cell, _i.e._ whether it is visited or not.

    - With the in-place technique, we simply assign a _specific_ value to the cell in the grid, rather than creating an additional matrix or hashtable.

- **Boundary Check**

    - There are several boundary conditions that we could check during the backtracking, namely whether the coordinate of a cell is _valid_ or not and whether the current cell is _visited_ or not.

    - We could do the checking right before we make the recursive call, or at the beginning of the `backtrack` function.

    - We decided to go with the former one, which could save us some recursive calls when the boundary check does not pass.


**Complexity Analysis**

Let $$N$$ be the total number of cells in the input grid.

- Time Complexity: $$\mathcal{O}(3^N)$$ 

    - Although technically we have 4 directions to explore at each step, we have *at most* 3 directions to try at any moment except the first step.
    The last direction is the direction where we came from, therefore we don't need to explore it, since we have been there before.

    - In the worst case where none of the cells is an obstacle, we have to explore each cell.
    Hence, the time complexity of the algorithm is $$\mathcal{O}(4 * 3 ^{(N-1)}) = \mathcal{O}(3^N)$$. 

- Space Complexity: $$\mathcal{O}(N)$$

    - Thanks to the in-place technique, we did not use any additional memory to keep track of the state.

    - On the other hand, we apply recursion in the algorithm, which could incur $$\mathcal{O}(N)$$ space in the function call stack.

    - Hence, the overall space complexity of the algorithm is $$\mathcal{O}(N)$$.

---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python] Brute Force Backstracking
- Author: lee215
- Creation Date: Sun Jan 20 2019 12:05:10 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 16 2020 11:44:33 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
First find out where the start and the end is.
Also We need to know the number of empty cells.

We we try to explore a cell,
it will change 0 to -2 and do a dfs in 4 direction.

If we hit the target and pass all empty cells, increment the result.
<br>

# Complexity
Time complexity is as good as dp,
but it take less space and easier to implement.
<br>

**Java:**
```java
    int res = 0, empty = 1, sx, sy, ex, ey;
    public int uniquePathsIII(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                if (grid[i][j] == 0) empty++;
                else if (grid[i][j] == 1) {
                    sx = i;
                    sy = j;
                }
            }
        }
        dfs(grid, sx, sy);
        return res;
    }

    public void dfs(int[][] grid, int x, int y) {
        if (x < 0 || x >= grid.length || y < 0 || y >= grid[0].length || grid[x][y] < 0)
            return;
        if (grid[x][y] == 2) {
            if (empty == 0) res++;
            return;
        }
        grid[x][y] = -2;
        empty--;
        dfs(grid, x + 1, y);
        dfs(grid, x - 1, y);
        dfs(grid, x, y + 1);
        dfs(grid, x, y - 1);
        grid[x][y] = 0;
        empty++;
    }
```

**Python:**
```py
    def uniquePathsIII(self, A):
        self.res = 0
        m, n, empty = len(A), len(A[0]), 1
        for i in range(m):
            for j in range(n):
                if A[i][j] == 1:
                    x, y = (i, j)
                elif A[i][j] == 0:
                    empty += 1

        def dfs(x, y, empty):
            if not (0 <= x < m and 0 <= y < n and A[x][y] >= 0): return
            if A[x][y] == 2:
                self.res += empty == 0
                return
            A[x][y] = -2
            dfs(x + 1, y, empty - 1)
            dfs(x - 1, y, empty - 1)
            dfs(x, y + 1, empty - 1)
            dfs(x, y - 1, empty - 1)
            A[x][y] = 0
        dfs(x, y, empty)
        return self.res
```

</p>


### C++ brute force DFS
- Author: votrubac
- Creation Date: Sun Jan 20 2019 12:04:26 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 20 2020 07:22:46 GMT+0800 (Singapore Standard Time)

<p>
First, we find our startig point (```i1, j1```) and count the number of empty cells. We then do DFS, marking visited elements and tracking number of steps (```s```). If we reach the target, and number of steps matches the number of empty cells (``` s == t_s```) - we found a path.
```
int dfs(vector<vector<int>>& g, int i, int j, int s, int t_s) {
  if (i < 0 || j < 0 || i >= g.size() || j >= g[0].size() || g[i][j] == -1) return 0;
  if (g[i][j] == 2) return s == t_s ? 1 : 0;
  g[i][j] = -1;
  int paths = dfs(g, i + 1, j, s + 1, t_s) + dfs(g, i - 1, j, s + 1, t_s) +
    dfs(g, i, j + 1, s + 1, t_s) + dfs(g, i, j - 1, s + 1, t_s);
  g[i][j] = 0;
  return paths;
}
int uniquePathsIII(vector<vector<int>>& g) {
  auto i1 = 0, j1 = 0, t_steps = 0;
  for (auto i = 0; i < g.size(); ++i)
    for (auto j = 0; j < g[0].size(); ++j) {
      if (g[i][j] == 1) i1 = i, j1 = j;
      if (g[i][j] != -1) ++t_steps;
    }
  return dfs(g, i1, j1, 1, t_steps);
}
```
## Complexity Analysis
Runtime: O(3 ^ n), where n is the total number of cells without obstacles.
Memory: O(n) for the DFS stack.
</p>


