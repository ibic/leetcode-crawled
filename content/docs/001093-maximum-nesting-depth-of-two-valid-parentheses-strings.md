---
title: "Maximum Nesting Depth of Two Valid Parentheses Strings"
weight: 1093
#id: "maximum-nesting-depth-of-two-valid-parentheses-strings"
---
## Description
<div class="description">
<p>A string is a <em>valid parentheses string</em>&nbsp;(denoted VPS) if and only if it consists of <code>&quot;(&quot;</code> and <code>&quot;)&quot;</code> characters only, and:</p>

<ul>
	<li>It is the empty string, or</li>
	<li>It can be written as&nbsp;<code>AB</code>&nbsp;(<code>A</code>&nbsp;concatenated with&nbsp;<code>B</code>), where&nbsp;<code>A</code>&nbsp;and&nbsp;<code>B</code>&nbsp;are VPS&#39;s, or</li>
	<li>It can be written as&nbsp;<code>(A)</code>, where&nbsp;<code>A</code>&nbsp;is a VPS.</li>
</ul>

<p>We can&nbsp;similarly define the <em>nesting depth</em> <code>depth(S)</code> of any VPS <code>S</code> as follows:</p>

<ul>
	<li><code>depth(&quot;&quot;) = 0</code></li>
	<li><code>depth(A + B) = max(depth(A), depth(B))</code>, where <code>A</code> and <code>B</code> are VPS&#39;s</li>
	<li><code>depth(&quot;(&quot; + A + &quot;)&quot;) = 1 + depth(A)</code>, where <code>A</code> is a VPS.</li>
</ul>

<p>For example,&nbsp; <code>&quot;&quot;</code>,&nbsp;<code>&quot;()()&quot;</code>, and&nbsp;<code>&quot;()(()())&quot;</code>&nbsp;are VPS&#39;s (with nesting depths 0, 1, and 2), and <code>&quot;)(&quot;</code> and <code>&quot;(()&quot;</code> are not VPS&#39;s.</p>

<p>&nbsp;</p>

<p>Given a VPS <font face="monospace">seq</font>, split it into two disjoint subsequences <code>A</code> and <code>B</code>, such that&nbsp;<code>A</code> and <code>B</code> are VPS&#39;s (and&nbsp;<code>A.length + B.length = seq.length</code>).</p>

<p>Now choose <strong>any</strong> such <code>A</code> and <code>B</code> such that&nbsp;<code>max(depth(A), depth(B))</code> is the minimum possible value.</p>

<p>Return an <code>answer</code> array (of length <code>seq.length</code>) that encodes such a&nbsp;choice of <code>A</code> and <code>B</code>:&nbsp; <code>answer[i] = 0</code> if <code>seq[i]</code> is part of <code>A</code>, else <code>answer[i] = 1</code>.&nbsp; Note that even though multiple answers may exist, you may return any of them.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> seq = &quot;(()())&quot;
<strong>Output:</strong> [0,1,1,1,1,0]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> seq = &quot;()(())()&quot;
<strong>Output:</strong> [0,0,0,1,1,0,1,1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= seq.size &lt;= 10000</code></li>
</ul>

</div>

## Tags
- Binary Search (binary-search)
- Greedy (greedy)

## Companies
- Bloomreach - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Confused by this problem? I was, too, but here is how it became crystal clear...
- Author: Domiii
- Creation Date: Wed Aug 14 2019 16:35:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 07 2020 18:17:43 GMT+0800 (Singapore Standard Time)

<p>
# Some observations

1. This problem uses very abstract and non-intuitive language; this is not a bad thing, just makes it hard to understand for those of us who are not well versed in that particular language.
2. The problem uses seemingly ambiguous language, when overloading the meaning of `A` and `B`: It first talks about `A` and `B` in the context of the definition of a VPS, but then re-defines `A` and `B` later as two disjoint subsequences in the solution of the problem, absolutely unrelated to the first definition.
3. After looking at some explanations and code samples, I see that the solution depends on whether or not the depth of a parenthesis is odd or not, but I did not get why, so I kept on pondering...

# First things first: What is a VPS?
A VPS (or "valid parentheses string") is just a string containing balanced parentheses, i.e. any opening parenthesis is followed by exactly one that closes it, and any closing parenthesis is preceded by at least one unbalanced opening parenthesis.

Valid examples: `((()))`, `(())()`, `()((((((()))))))()()()()` etc.

Invalid examples: `)(`, `))`, `(())()))` etc.

# Now, what does this problem really want from me?

We are trying to minimize depth of a VPS by splitting all pairs of parentheses into two groups `A` and `B`. `A` and `B` can be any **disjoint** sub-sequence of the input sequence, as long as they are still VPS **without changing the order** of anything (side-note: the `order` constrained is implied by the word `sequence`).

Some examples:

1. `"(())"` can be grouped into `A = "()"` and `B = "()"` or, `A = ""` and `B = "(())"`, but, for example, not `A = "(("` and `B = "))"` as those are not VPS
1. `"(())()"` can be grouped into `A = "(())"` and `B = "()"`, and many other ways

However, the goal is to **minimize** the **max depth** of both groups.

In the last example (`"(())()"`), the grouping (`A = "(())"` and `B = "()"`) is not minimal, because `A` has a max-depth of 2 while there exists a grouping where both only have a depth of 1, namely: `A = ()()` and `B = ()`, or to visualize the designation:

```
parentheses =   [ (, (, ), ), (, )]
depths =        [ 1, 2, 2, 1, 1, 1 ]
groups =        [ A, B, B, A, A, A]
solution =      [ 0, 1, 1, 0, 0, 0]
```

NOTE: One of several other solutions would be: `[1, 0, 0, 1, 0, 0]`, as `A` and `B` maintain a max depth of 1. Solutions are not unique.


# Going deeper: How to minimize the depth?

This optimization problem does not really care too much about anything but the most deeply nested subset. For example, in the previous example that deepest stack would be `(())`. The remaining `()` we can put either in `A` or `B` (because, as it turns out they will have a depth of at most (roughly) `maxDepth/2`, so we don\'t care whether they are in `A` or in `B`; explained later).

So, as long as we fix the most deeply nested parentheses, we are all good.

Given a tall, but simple stack of parentheses, such as `((((((((()))))))))`, how do we split it into two disjoint sub-sequences that happen to be VPS, while also *minimizing* each of their depth?

Since we can only split them into two groups, the minimum depth we can achieve will always be `ceil(maxDepth/2)` and we achieve it by putting one half in each group. Trying to put less than half in one of the two groups, will leave the other group with more than one half.

Any other stack, we can handle using the same strategy, making sure that no other parenthesis will increase the max depth.

Interesting side note: Any stack in the sequence that is of max depth less or equal to `ceil(globalMaxDepth/2)` (of the entire sequence) we can assign any which way we want (as long as we maintain the VPS property), since they cannot increase the max depth of the resulting split.
E.g.: when looking at `"((()))()()()()"`, we just gotta take good care of the first `"((()))"`, then we can assign each following parenthesis pair `"()"` to `A` or `B` any which way we want, since their max depth is less or equal to `ceil(3/2) = 2`.

# Solution

So what we need is a strategy to cut any stack in half while making sure that the resulting half-stacks are balanced VPS. There is many ways of doing it, but one of the easiest (and seemingly a very common) approach is by going for an odd/even strategy:

1. Get the depth at every index of the string
1. Put all odd-depth parentheses in one group, and all even-depth in the other
1. Done.

NOTE: Using this solution, parentheses at the same depth are always in the same group, so you can ensure that the resulting groups are balanced VPS.


# Code

You can find many examples on this discussion board; here is mine:

```python
class Solution:
  def maxDepthAfterSplit(self, s: str) -> List[int]:
    groups = []
    d = 0
    for c in s:
      open = c == \'(\'
      if open:
        d += 1
      groups.append(d % 2)  # group determined through parity (odd/even?) of depth
      if not open:
        d -=1
    
    return groups
```
</p>


### [Java/C++/Python] O(1) Extra Space Except Output
- Author: lee215
- Creation Date: Sun Jul 07 2019 12:00:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Mar 12 2020 23:15:37 GMT+0800 (Singapore Standard Time)

<p>
## Solution 0: Alternatively Distribute Parentheses
Basically, `(` is 1 point, `)` is `-1` point.
We try to keep total points of two groups even,
by distributing parentheses alternatively.

The good part of this solution is that,
we actually need no extra variable to record anything.


**Java:**
```java
    public int[] maxDepthAfterSplit(String seq) {
        int n = seq.length(), res[] = new int[n];
        for (int i = 0; i < n; ++i)
            res[i] = seq.charAt(i) == \'(\' ? i & 1 : (1 - i & 1);
        return res;
    }
```

**C++:**
```cpp
    vector<int> maxDepthAfterSplit(string seq) {
        vector<int> res(seq.length());
        for (int i = 0; i < seq.length(); ++i)
            res[i] = i & 1 ^ (seq[i] == \'(\');
        return res;
    }
```

**1-line Python:**
```python
    def maxDepthAfterSplit(self, seq):
        return [i & 1 ^ (seq[i] == \'(\') for i, c in enumerate(seq)]
```
<br>

# Complexity
Time `O(N)` for one pass
Space `O(1)` extra space,  `O(N)` for output
<br>

# More
Also provide some more easy understood ideas for this problem,
pick the the one you like.
(As I keep receiving complaints about the readability,
like no parentheses in solution for problem of parentheses)
<br>

# Solution 1: Keep Two Group Even
Count the number of open parentheses of group `A` and group `B`.
**Java:**
```java
    public int[] maxDepthAfterSplit(String seq) {
        int A = 0, B = 0, n = seq.length();
        int[] res = new int[n];
        for (int i = 0; i < n; ++i) {
            if (seq.charAt(i) == \'(\') {
                if (A < B) {
                    ++A;
                } else {
                    ++B;
                    res[i] = 1;
                }
            } else {
                if (A > B) {
                    --A;
                } else {
                    --B;
                    res[i] = 1;
                }
            }
        }
        return res;
    }
```

**C++:**
```cpp
    vector<int> maxDepthAfterSplit(string seq) {
        int A = 0, B = 0, n = seq.length();
        vector<int> res(n, 0);
        for (int i = 0; i < n; ++i) {
            if (seq[i] == \'(\') {
                if (A < B) ++A;
                else ++B, res[i] = 1;
            } else {
                if (A > B) --A;
                else --B, res[i] = 1;
            }
        }
        return res;
    }
```

**Python:**
```python
    def maxDepthAfterSplit(self, seq):
        A = B = 0
        res = [0] * len(seq)
        for i, c in enumerate(seq):
            v = 1 if c == \'(\' else -1
            if (v > 0) == (A < B):
                A += v
            else:
                B += v
                res[i] = 1
        return res

```
<br>

# Solution 2: Split by Half
Count the number of level of whole string.
Then split it by half.
Group 0: the part under the half height
Group 1: the part above the half height

**Java:**
```java
    public int[] maxDepthAfterSplit(String seq) {
        int depth = 0, cur = 0, n = seq.length();
        for (int i = 0; i < n; ++i) {
            cur +=  seq.charAt(i) == \'(\' ?  1 : -1;
            depth = Math.max(depth, cur);
        }
        int[] res = new int[n];
        for (int i = 0; i < n; ++i) {
            if (seq.charAt(i) == \'(\') {
                if (++cur > depth / 2)
                    res[i] = 1;
            } else {
                if (cur-- > depth / 2)
                    res[i] = 1;
            }
        }
        return res;
    }
```

**C++:**
```cpp
    vector<int> maxDepthAfterSplit(string seq) {
        int depth = 0, cur = 0, n = seq.length();
        for (char c : seq)
            depth = max(depth, cur += c == \'(\' ? 1 : -1);
        vector<int> res(n, 0);
        for (int i = 0; i < n; ++i) {
            if (seq[i] == \'(\' && ++cur > depth / 2) res[i] = 1;
            if (seq[i] == \')\' && cur-- > depth / 2) res[i] = 1;
        }
        return res;
    }
```

**Python:**
```python
    def maxDepthAfterSplit(self, seq):
        depth = cur = 0
        for c in seq:
            if c == \'(\':
                cur += 1
                depth = max(depth, cur)
            else:
                cur -= 1
        half = depth / 2
        res = [0] * len(seq)
        for i, c in enumerate(seq):
            if c == \'(\':
                cur += 1
                if cur > half: res[i] = 1
            else:
                if cur > half: res[i] = 1
                cur -= 1
        return res
```

</p>


### very easy and clean code one-pass O(N) with explanation
- Author: BarOrz
- Creation Date: Sun Jul 07 2019 12:14:58 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 08 2019 23:26:42 GMT+0800 (Singapore Standard Time)

<p>
# intuition
if we are going deeper, take turns to assign to A and B
```
1.         ( ( ( ( ( ) ) ) ) )
   level   1 2 3 4 5 5 4 3 2 1  
   A takes level 1,3,5 and B takes level 2,4 ==> A : ( ( ( ) ) ) , B : ( ( ) )
2.         ( ( ) ) ( ( ( ) ) )
   level   1 2 2 1 1 2 3 3 2 1
   A takes level 1,3 and B takes level 2 ==> A : ( ) ( ( ) ) , B : ( ) ( )
   
when to increase/decrese level ?
1. meet a new \'(\' level up
2. meet a new \')\' level down

if you can understand the level conception, coding is quite easy.
```
```
class Solution {
public:
    vector<int> maxDepthAfterSplit(string seq) {
        vector<int> res(seq.size(),0);
        int level = 0, index = 0;
        while(index < seq.size()){
            if(seq[index] ==\'(\')
                res[index] = ++level%2;
            else
                res[index] = level--%2;
            index++;
        }
        return res;
    }
};
```
</p>


