---
title: "Constrained Subsequence Sum"
weight: 1150
#id: "constrained-subsequence-sum"
---
## Description
<div class="description">
<p>Given an integer array&nbsp;<code>nums</code>&nbsp;and an integer <code>k</code>, return the maximum sum of a <strong>non-empty</strong> subsequence&nbsp;of that array such that for every&nbsp;two <strong>consecutive</strong> integers in the subsequence,&nbsp;<code>nums[i]</code>&nbsp;and&nbsp;<code>nums[j]</code>, where&nbsp;<code>i &lt; j</code>, the condition&nbsp;<code>j - i &lt;= k</code>&nbsp;is satisfied.</p>

<p>A&nbsp;<em>subsequence</em>&nbsp;of an array is&nbsp;obtained by deleting some number of elements (can be&nbsp;zero) from the array, leaving the remaining elements in their original order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [10,2,-10,5,20], k = 2
<strong>Output:</strong> 37
<b>Explanation:</b> The subsequence is [10, 2, 5, 20].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [-1,-2,-3], k = 1
<strong>Output:</strong> -1
<b>Explanation:</b> The subsequence must be non-empty, so we choose the largest number.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [10,-2,-10,-5,20], k = 2
<strong>Output:</strong> 23
<b>Explanation:</b> The subsequence is [10, -2, -5, 20].
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= k &lt;= nums.length &lt;= 10^5</code></li>
	<li><code>-10^4&nbsp;&lt;= nums[i] &lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Akuna Capital - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] O(N) Decreasing Deque
- Author: lee215
- Creation Date: Sun Apr 26 2020 12:04:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 16:11:31 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
We need to know the maximum in the window of size `k`.
Use `heqp` will be `O(NlogN)`
Use `TreeMap` will be `O(NlogK)`
Use `deque` will be `O(N)`
Done. (If not done, continue read)
<br>

## **Prepare**
How about google "sliding window maximum",
and make sure you understand 239. Sliding Window Maximum
Done. (If not done, continue read)
<br>

## **Explanation**
Update `res[i]`,
where `res[i]` means the maximum result you can get if the last element is `A[i]`.

I directly modify on the input `A`,
if you don\'t like it,
use a copy of `A`

Keep a decreasing deque `q`,
`deque[0]` is the maximum result in the last element of result.

If `deque[0] > 0`. we add it to `A[i]`

In the end, we return the maximum `res`.
<br>

## **Complexity**
Because all element are pushed and popped at most once.
Time `O(N)`

Because at most O(K) elements in the deque.
Space `O(K)`
<br>

## **More**
Deque can be widely used in all bfs problems.
For more complecated usage,
I recall this problem
[862. Shortest Subarray with Sum at Least K](https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/discuss/143726/)
<br>

**Java:**
```java
    public int constrainedSubsetSum(int[] A, int k) {
        int res = A[0];
        Deque<Integer> q = new ArrayDeque<>();
        for (int i = 0; i < A.length; ++i) {
            A[i] += !q.isEmpty() ? q.peek() : 0;
            res = Math.max(res, A[i]);
            while (!q.isEmpty() && A[i] > q.peekLast())
                q.pollLast();
            if (A[i] > 0)
                q.offer(A[i]);
            if (i >= k && !q.isEmpty() && q.peek() == A[i - k])
                q.poll();
        }
        return res;
    }
```

**C++:**
```cpp
    int constrainedSubsetSum(vector<int>& A, int k) {
        deque<int> q;
        int res = A[0];
        for (int i = 0; i < A.size(); ++i) {
            A[i] += q.size() ? q.front() : 0;
            res = max(res, A[i]);
            while (q.size() && A[i] > q.back())
                q.pop_back();
            if (A[i] > 0)
                q.push_back(A[i]);
            if (i >= k && q.size() && q.front() == A[i - k])
                q.pop_front();
        }
        return res;
    }
```

**Python:**
```py
    def constrainedSubsetSum(self, A, k):
        deque = collections.deque()
        for i in xrange(len(A)):
            A[i] += deque[0] if deque else 0
            while len(deque) and A[i] > deque[-1]:
                deque.pop()
            if A[i] > 0:
                deque.append(A[i])
            if i >= k and deque and deque[0] == A[i - k]:
                deque.popleft()
        return max(A)
```

</p>


### [Python/C++] DP with decreasing deque
- Author: yanrucheng
- Creation Date: Sun Apr 26 2020 12:01:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 26 2020 12:13:11 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
This is a typical knapsack problem. we maintain an array `dp`, where `dp[i]` is the maximum sum we can get from nums[:i] and nums[i] is guaranteed to be included.

- Base case: `dp[0] = nums[0]`
- state transition: `dp[i] = max(dp[i - k], dp[i-k+1], ..., dp[i - 1], 0) + x`
    - NOTE that x can be a fresh start when all the previous dp are negative.

This algorithm is only \x10`O(n * k)`, we need to improve it to `O(n)` because both `k` and `n` can be 10^5.

The Idea is straight-forward, we can maintain an non-increasing deque `decrease` that records the maximum value among `dp[i - k], dp[i-k+1], ..., dp[i - 1]`. When encountering a new value `x`, we only record it in `decrease` if `x > decrease[decrease.size - 1]`. Thus the first element in `decrease` will always be the largest value we want.

**Complexity**
Time: `O(n)`
Space: `O(n)`

**Python**
```
class Solution:
    def constrainedSubsetSum(self, nums: List[int], k: int) -> int:
        dp = nums[:1]
        decrease = collections.deque(dp)
        for i, x in enumerate(nums[1:], 1):
            if i > k and decrease[0] == dp[i - k - 1]:
                decrease.popleft()
            tmp = max(x, decrease[0] + x)
            dp += tmp,
            while decrease and decrease[-1] < tmp:
                decrease.pop()
            decrease += tmp,                
        return max(dp)  
```

**C++**
```
class Solution {
public:
    int constrainedSubsetSum(vector<int>& nums, int k) {
        vector<int> dp {nums[0]};
        deque<int> decrease {nums[0]};
        int res = nums[0];
        
        for (int i=1; i<nums.size(); i++) {
            if (i > k && decrease[0] == dp[i - k - 1])
                decrease.pop_front();
            int tmp = max(nums[i], decrease[0] + nums[i]);
            dp.push_back(tmp);
            while (!decrease.empty() && decrease.back() < tmp)
                decrease.pop_back();
            decrease.push_back(tmp);
            
            res = max(res, tmp);
        }
        return res;
        
    }
};
```
</p>


### [Java] Decreasing Monotonic Queue - Clean code - O(N)
- Author: hiepit
- Creation Date: Fri May 01 2020 11:29:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 01 2020 12:50:44 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
dp[i] is the max sum we can have from A[:i] when A[i] has been chosen.

**Solution 1: DP Straight forward - TLE**
```java
class Solution {
    public int constrainedSubsetSum(int[] arr, int k) {
        int n = arr.length;
        int[] dp = new int[n];
        int ans = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            int max = 0;
            for (int j = Math.max(i - k, 0); j < i; j++) { // choose the max element in latest k elements, it\'s in range [i-k, i-1]
                max = Math.max(max, dp[j]);
            }
            dp[i] = arr[i] + max;
            ans = Math.max(ans, dp[i]);
        }
        return ans;
    }
}
```
Complexity:
- Time: `O(N*K)`
- Space: `O(N)`

**Solution 2: DP + Decreasing Monotonic Queue**
We need to get the max element in k latest elements in `O(1)` by using Decreasing Monotonic Queue, the same with this problem: [239. Sliding Window Maximum](https://leetcode.com/problems/sliding-window-maximum/discuss/598751)
```java
class Solution {
    public int constrainedSubsetSum(int[] arr, int k) {
        int n = arr.length;
        int[] dp = new int[n];
        Deque<Integer> deque = new LinkedList<>();
        int ans = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            int max = Math.max(0, deque.isEmpty() ? 0 : dp[deque.peekFirst()]);
            dp[i] = arr[i] + max;
            ans = Math.max(ans, dp[i]);
            while (!deque.isEmpty() && dp[i] >= dp[deque.peekLast()]) { // If dp[i] >= deque.peekLast() -> Can discard the tail since it\'s useless
                deque.pollLast();
            }
            deque.addLast(i);
            if (i - deque.peekFirst() + 1 > k) { // remove the last element of range k
                deque.removeFirst();
            }
        }
        return ans;
    }
}
```
Complexity:
- Time: `O(N)`
- Space: `O(N)`

**Solution 3: DP + Decreasing Monotonic Queue + Optimized Space**
Use `arr` as `dp` instead of create new one.
```java
class Solution {
    public int constrainedSubsetSum(int[] arr, int k) {
        int n = arr.length;
        Deque<Integer> deque = new LinkedList<>();
        int ans = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
			int max = Math.max(0, deque.isEmpty() ? 0 : arr[deque.peekFirst()]);
            arr[i] += max;
            ans = Math.max(ans, arr[i]);
            while (!deque.isEmpty() && arr[i] >= arr[deque.peekLast()]) { // If dp[i] >= deque.peekLast() -> Can discard the tail since it\'s useless
                deque.pollLast();
            }
            deque.addLast(i);
            if (i - deque.peekFirst() + 1 > k) { // remove the last element of range k
                deque.removeFirst();
            }
        }
        return ans;
    }
}
```
Complexity:
- Time: `O(N)`
- Space: `O(K)`
</p>


