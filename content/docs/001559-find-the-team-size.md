---
title: "Find the Team Size"
weight: 1559
#id: "find-the-team-size"
---
## Description
<div class="description">
<p>Table: <code>Employee</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| employee_id   | int     |
| team_id       | int     |
+---------------+---------+
employee_id is the primary key for this table.
Each row of this table contains the ID of each employee and their respective team.
</pre>

<p>Write an SQL query to find the team size of each of the employees.</p>

<p>Return result table in any order.</p>

<p>The query result format is in the following example:</p>

<pre>
Employee Table:
+-------------+------------+
| employee_id | team_id    |
+-------------+------------+
|     1       |     8      |
|     2       |     8      |
|     3       |     8      |
|     4       |     7      |
|     5       |     9      |
|     6       |     9      |
+-------------+------------+
Result table:
+-------------+------------+
| employee_id | team_size  |
+-------------+------------+
|     1       |     3      |
|     2       |     3      |
|     3       |     3      |
|     4       |     1      |
|     5       |     2      |
|     6       |     2      |
+-------------+------------+
Employees with Id 1,2,3 are part of a team with team_id = 8.
Employees with Id 4 is part of a team with team_id = 7.
Employees with Id 5,6 are part of a team with team_id = 9.

</pre>

</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Window function
- Author: MinionJosh
- Creation Date: Sat Jan 04 2020 05:10:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jan 04 2020 05:10:06 GMT+0800 (Singapore Standard Time)

<p>
```
select employee_id, count(*) over(partition by team_id) as team_size
from employee
```
</p>


### Intuitive Left Join with Explanation
- Author: yangmexi
- Creation Date: Sun Dec 29 2019 07:57:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 29 2019 07:57:49 GMT+0800 (Singapore Standard Time)

<p>
*This solution uses a subquery in FROM clauses*
1. First, calculate the team size for every team using `GROUP BY`
```
# Subquery standalone
SELECT team_id, COUNT(DISTINCT employee_id) AS team_size
FROM Employee
GROUP BY team_id
```
2. Then use the original table left join the team size table on `team_id`
```
SELECT employee_id, team_size
FROM Employee AS e
LEFT JOIN (
      SELECT team_id, COUNT(DISTINCT employee_id) AS team_size
      FROM Employee
      GROUP BY team_id
) AS teams
ON e.team_id = teams.team_id
```
This is a very intuitive solution with an intermediate table of team size. Maybe redundant comparing to self-join solutions, but is more straightforward to understand.
</p>


### Two lines
- Author: lawlessz
- Creation Date: Fri Jan 03 2020 16:46:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 05 2020 03:43:55 GMT+0800 (Singapore Standard Time)

<p>
```
select e.employee_id, (select count(team_id) from Employee where e.team_id = team_id) as team_size
from Employee e
```
</p>


