---
title: "Build an Array With Stack Operations"
weight: 1327
#id: "build-an-array-with-stack-operations"
---
## Description
<div class="description">
<p>Given an array <code>target</code> and&nbsp;an integer <code>n</code>. In each iteration, you will read a number from &nbsp;<code>list = {1,2,3..., n}</code>.</p>

<p>Build the <code>target</code>&nbsp;array&nbsp;using the following operations:</p>

<ul>
	<li><strong>Push</strong>: Read a new element from the beginning&nbsp;<code>list</code>, and push it in the array.</li>
	<li><strong>Pop</strong>: delete the last element of&nbsp;the array.</li>
	<li>If the target array is already&nbsp;built, stop reading more elements.</li>
</ul>

<p>You are guaranteed that the target array is strictly&nbsp;increasing, only containing&nbsp;numbers between 1 to <code>n</code>&nbsp;inclusive.</p>

<p>Return the operations to build the target array.</p>

<p>You are guaranteed that the answer is unique.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> target = [1,3], n = 3
<strong>Output:</strong> [&quot;Push&quot;,&quot;Push&quot;,&quot;Pop&quot;,&quot;Push&quot;]
<strong>Explanation: 
</strong>Read number 1 and automatically push in the array -&gt; [1]
Read number 2 and automatically push in the array then Pop it -&gt; [1]
Read number 3 and automatically push in the array -&gt; [1,3]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> target = [1,2,3], n = 3
<strong>Output:</strong> [&quot;Push&quot;,&quot;Push&quot;,&quot;Push&quot;]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> target = [1,2], n = 4
<strong>Output:</strong> [&quot;Push&quot;,&quot;Push&quot;]
<strong>Explanation: </strong>You only need to read the first 2 numbers and stop.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> target = [2,3,4], n = 4
<strong>Output:</strong> [&quot;Push&quot;,&quot;Pop&quot;,&quot;Push&quot;,&quot;Push&quot;,&quot;Push&quot;]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= target.length &lt;= 100</code></li>
	<li><code>1 &lt;= target[i]&nbsp;&lt;= 100</code></li>
	<li><code>1 &lt;= n &lt;= 100</code></li>
	<li><code>target</code> is strictly&nbsp;increasing.</li>
</ul>

</div>

## Tags
- Stack (stack)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Simple Check
- Author: Poorvank
- Creation Date: Sun May 10 2020 16:21:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 10 2020 16:22:51 GMT+0800 (Singapore Standard Time)

<p>
```
public List<String> buildArray(int[] target, int n) {
        List<String> result = new ArrayList<>();
        int j=0;
        for (int i=1;i<=n && j<target.length;i++) {
            result.add("Push");
            if(target[j]==i) {
                j++;
            } else {
                result.add("Pop");
            }
        }
        return result;
    }
```
</p>


### I don't understand the question. What do they ask?
- Author: dhtmlkitchen
- Creation Date: Sun May 10 2020 13:52:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 10 2020 13:52:31 GMT+0800 (Singapore Standard Time)

<p>
I don\'t understand the question. What do they ask? Can you explain it?
</p>


### Python - Simple. Faster than 99%
- Author: p-maria
- Creation Date: Tue May 12 2020 03:14:16 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 16 2020 10:40:31 GMT+0800 (Singapore Standard Time)

<p>
<b>Time Complexity:</b> O(N)
<b>Runtime:</b> Faster than 99% <i>(as of May 11th, 2020)</i>
<b>Memory Usage:</b> Less than 100% 
<b>Explanation:</b>
Step 1: Create an empty list ```res``` to store the result.
Step 2: Add the elements of the input array ```target``` to a set to improve lookup performance. 
Step 3: Iterate through the integers of ```target``` and add "Push" to the ```res``` list for each integer ```in range(1, target[-1] + 1)```. Then, add "Pop" if the integer is <b>not</b> in the set.
Step 4: Return the result.

<i>* Note: The ```n``` parameter is not used in this approach.</i> 
```
def solution(target, n):
    res = []
	s = set(target)
    for i in range(1, target[-1] + 1):
        res.append("Push")
        if i not in s:
            res.append("Pop")
    return res
```
If you like my solution and/or find it useful, please upvote :) Thank you.
</p>


