---
title: "Find Smallest Common Element in All Rows"
weight: 1048
#id: "find-smallest-common-element-in-all-rows"
---
## Description
<div class="description">
<p>Given a matrix <code>mat</code>&nbsp;where every row is sorted in <strong>strictly</strong>&nbsp;<strong>increasing</strong> order, return&nbsp;the <strong>smallest common element</strong> in all rows.</p>

<p>If there is no common element, return&nbsp;<code>-1</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> mat = [[1,2,3,4,5],[2,4,5,8,10],[3,5,7,9,11],[1,3,5,7,9]]
<strong>Output:</strong> 5
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= mat.length, mat[i].length &lt;= 500</code></li>
	<li><code>1 &lt;= mat[i][j] &lt;= 10^4</code></li>
	<li><code>mat[i]</code> is sorted in strictly increasing order.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Binary Search (binary-search)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

The fact that every row is sorted in the *increasing* order tells us that there are no duplicates within a single row. So, if an element appears in all rows, it will appear exactly `n` times (where `n` is the number of rows).

We can count all elements, and then pick the smallest one that appears `n` times. This approach has a linear time complexity - at the cost of additional memory for storing counts.

Also, we can use a binary search to look elements up directly in the matrix. We won't need any additional memory, though this approach will be a bit slower.

Finally, we can track positions for each row. We will then repeatedly advance positions for smaller elements until all positions point to the common element - if there is one. The time complexity will be linear, and it will require less memory than when storing counts.

---

#### Approach 1: Count Elements

Iterate through all elements row-by-row and count each element. Since elements are constrained to `[1...10000]`, we'll use an array of that size to store counts.

Then, iterate through the array left-to-right, and return the first element that appears `n` times. This is, by the way, how the counting sort works.

> For an unconstrained problem, we'll need to use an ordered map to store counts.

![Count Elements Illustration](../Figures/1198/1198_approach1.png)

**Algorithm**

1. Iterate `i` through each row.

    - Iterate `j` through each column.

        - Increment `count` for element `mat[i][j]`.

2. Iterate `k` from `1` to `10000`.

    - If `count[k]` equals `n`, return `k`.

3. Return `-1`.

<iframe src="https://leetcode.com/playground/z3DQUubv/shared" frameBorder="0" width="100%" height="310" name="z3DQUubv"></iframe>

**Improved Solution**

We can improve the average time complexity if we count elements column-by-column. This way, smaller elements will be counted first, and we can exit as soon as we get to an element that repeats `n` times.

> For an unconstrained problem, we can use an unordered map (which should be faster than the ordered map as for the initial solution) if we count elements column-by-column.

<iframe src="https://leetcode.com/playground/Qe7cbjcQ/shared" frameBorder="0" width="100%" height="259" name="Qe7cbjcQ"></iframe>

**Handling Duplicates**

If elements are in non-decreasing order, we'll need to modify these solutions to properly handle duplicates. For example, we return ```4``` (initial solution) and ```7``` (improved solution) instead of ```5``` for this test case:

`[[1,2,3,4,5],[5,7,7,7,7],[5,7,7,7,7],[1,2,4,4,5],[1,2,4,4,5]]`

It's easy to modify these solutions to handle duplicates. Since elements in a row are sorted, we can skip the current element if it's equal to the previous one.

**Complexity Analysis**

- Time complexity: $$\mathcal{O}(nm)$$, where $$n$$ and $$m$$ are the number of rows and columns.

- Space complexity:

    - Constrained problem: $$\mathcal{O}(10000)=\mathcal{O}(1)$$.

    - Unconstrained problem: $$\mathcal{O}(k)$$, where $$k$$ is the number of unique elements.

---

#### Approach 2: Binary Search

We can go through each element in the first row, and then use binary search to check if that element exists in all other rows.

!?!../Documents/1198_approach2.json:1000,475!?!

**Algorithm**

1. Iterate through each element in the first row.

    - Initialize `found` to true.

    - For each row:

        - Use binary search to check if the element exists.

        - If it does not, set `found` to false and exit the loop.

    - If `found` is true, return the element.

2. Return `-1`.

<iframe src="https://leetcode.com/playground/uLNKEEuY/shared" frameBorder="0" width="100%" height="276" name="uLNKEEuY"></iframe>

**Improved Solution** <a name="approach2_improved"></a>

In the solution above, we always search the entire row. We can improve the average time complexity if we start the next search from the position returned by the previous search. We can also return `-1` if all elements in the row are smaller than value we searched for.

> Note that `lower_bound` in C++ returns the position of first element that is equal (if exists) or greater than the searched value. In Java, `binarySearch` returns a positive index if the element exists, or `(-insertion_point - 1)`, where `insertion_point` is also the position of the next greater element. In both cases, it points past the last element if all elements are smaller than the value being searched for.

<iframe src="https://leetcode.com/playground/yNFEpFg9/shared" frameBorder="0" width="100%" height="412" name="yNFEpFg9"></iframe>

**Handling Duplicates**

Since we search for an element in each row, this approach works correctly if there are duplicates.

**Complexity Analysis**

- Time complexity: $$\mathcal{O}(mn\log{m})$$

    - We iterate through $$m$$ elements in the first row.

    - For each element, we perform the binary search $$n$$ times over $$m$$ elements.

- Space complexity:

    - Original solution: $$\mathcal{O}(1)$$.    

    - Improved solution: $$\mathcal{O}(n)$$ to store search positions for all rows.

---

#### Approach 3: Row Positions

We can enumerate elements in all rows in the sorted order, as described in approach 2 for the [23. Merge k Sorted List](https://leetcode.com/problems/merge-k-sorted-lists/solution/) problem.

For each row, we track the position of the current element starting from zero. Then, we find the smallest element among all positions, and advance the position for the corresponding row. We find our answer when all positions point to elements with the same value.

For this problem, however, we do not need to enumerate elements in the perfectly sorted order. We can determine the largest element among all positions and skip smaller elements in all other rows. 

!?!../Documents/1198_approach3.json:1000,475!?!

**Algorithm**

1. Initialize row positions, current max and counter with zeros.

2. For each row:

    - Increment the row position until the value is equal or greater than the current max.

        - If we reach the end of the row, return `-1`.

    - If the value equals the current max, increase the counter.

        - Otherwise, reset the counter to `1` and update the current max.

    - If the counter is equal to `n`, return the current max.

3. Repeat step 2.

<iframe src="https://leetcode.com/playground/gKtT2uuz/shared" frameBorder="0" width="100%" height="429" name="gKtT2uuz"></iframe>

**Handling Duplicates**

Since we take one element from each row, this approach works correctly if there are duplicates.

**Complexity Analysis**

- Time complexity: $$\mathcal{O}(nm)$$; we iterate through all $$nm$$ elements in the matrix in the worst case.

- Space complexity: $$\mathcal{O}(n)$$ to store row indices.

**Improved Solution**

We can use a binary search to advance positions, like in [Improved Solution for Approach 2](#approach2_improved).

While it can certantly improve the runtime, the worst case time complexity will be $$\mathcal{O}(mn\log{m})$$, which is a downgrade from $$\mathcal{O}(nm)$$ for the simple increment. The reason is that, if we need to advance row positions one-by-one, the binary search will still take $$\mathcal{O}(\log{m})$$ to find that very next value.

To optimize for the worst-case scenario, we can use the one-sided binary search (also known as the meta binary search), where we iterativelly double the distance from out position. The number of operations performed by such search will not exceed the distance between the original and resulting position, bringing the time complexity back to $$\mathcal{O}(nm)$$.

<iframe src="https://leetcode.com/playground/TR6MGFj6/shared" frameBorder="0" width="100%" height="500" name="TR6MGFj6"></iframe>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ | O(RClogC) time and O(1) space | Binary search approach
- Author: GimmeDanger
- Creation Date: Sun Sep 22 2019 00:30:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 22 2019 01:02:38 GMT+0800 (Singapore Standard Time)

<p>
Algorithm:
1) Iterate through the first row in O(C) time.
2) Check if current element is common -- search for it in every other row in O(RlogC) time.
3) First common element is an answer, because rows are sorted.

Total complexity: O(RClogC)

```
int smallestCommonElement(vector<vector<int>>& mat) {
        // corner case
        if (mat.size() == 1)
            return mat[0][0];
			
        for (int v : mat[0]) {
            bool is_common = true;
            for (auto &row : mat) {
                // Note: first row wasn\'t skipped for simplicity 
                if (!binary_search(begin(row), end(row), v)) {
                    is_common = false;
                    break;
                }                    
            }
            if (is_common)
                return v;
        }
        return -1;
    }
```
</p>


### [Java/C++/Python] Brute Force Count
- Author: lee215
- Creation Date: Sun Sep 22 2019 00:44:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 22 2019 00:44:14 GMT+0800 (Singapore Standard Time)

<p>
## **Complexity**
Time `O(NM)`
Space `O(10000)`

<br>

**Java:**
```java
    public int smallestCommonElement(int[][] A) {
        int[] count = new int[10001];
        int n = A.length, m = A[0].length;
        for (int j = 0; j < m; ++j)
            for (int i = 0; i < n; ++i)
                if (++count[A[i][j]] == n)
                    return A[i][j];
        return -1;
    }
```

**C++:**
```cpp
    int smallestCommonElement(vector<vector<int>>& A) {
        vector<int> count(10001);
        int n = A.size(), m = A[0].size();
        for (int j = 0; j < m; ++j)
            for (int i = 0; i < n; ++i)
                if (++count[A[i][j]] == n)
                    return A[i][j];
        return -1;
    }
```

**Python:**
```python
    def smallestCommonElement(self, M):
        c = collections.Counter()
        for row in M:
            for a in row:
                c[a] += 1
                if c[a] == len(M):
                    return a
        return -1
```

</p>


### C++ 3 approaches
- Author: votrubac
- Creation Date: Tue Sep 24 2019 15:58:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 28 2019 09:56:15 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Since elements are **increasing**, there are no duplicates in a single row. So, we can just count all elements and pick the smallest that appears *n* times (where *n* is the number of rows). This approach has a linear time complexity, but with a cost of an extra *nm* memory (where *m* is the number of columns).

If a memory becomes a concern (though there is no mention of such constraint in the problem description), we can use a binary search to efficiently look elements up directly in the matrix.
# Approach 1: Count Elements
### Algorithm
Count every element using, for example, a hash map. Since here elements are constrained to ```[1..10000]```, we\'ll use an array of that size instead. 

Then, iterate through the array starting from the smallest element, and return the first one that appears *n* times.

> Note that this is exactly how counting sort works.
```
int smallestCommonElement(vector<vector<int>>& mat) {
  int m[10001] = {};
  for (auto i = 0; i < mat.size(); ++i)
    for (auto j = 0; j < mat[i].size(); ++j)
      ++m[mat[i][j]];
  for (auto n = 1; n <= 10000; ++n)
    if (m[n] >= mat.size()) return n;
  return -1;
}   
```
We can improve the average time complexity if we count elements row-by-row. Since we will process smaller elements first, we can exit as soon as we get to an element that repeats *n* times. 
```
int smallestCommonElement(vector<vector<int>>& mat) {
  int m[10001] = {};
  for (auto j = 0; j < mat[0].size(); ++j)
    for (auto i = 0; i < mat.size(); ++i)
      if (++m[mat[i][j]] == mat.size()) return mat[i][j];
  return -1;
}
```
### Additional Considerations
If elements are in non-decreasing order, we\'ll need to deal with duplicates. For example, the first solution above returns ```4```, and the second - ```7``` instead of ```5``` for this test case: 
```
[[1,2,3,4,5],[5,7,7,7,7],[5,7,7,7,7],[1,2,4,4,5],[1,2,4,4,5]]
```
It\'s easy to modify these solutions to handle duplicates. Since elements in a row are sorted, we can skipp the current element if it\'s equal to the previous one. 
### Complexity Analysis
- Time: *O(nm)*, where n and m are rows and columns in the matrix.
- Memory: *O(nm)*.
# Approach 2: Binary Search
### Algorithm
Go through each element in the first row, then use a binary search to check if the element exists in all other rows.
```
int smallestCommonElement(vector<vector<int>>& mat) {
  for (auto j = 0; j < mat[0].size(); ++j)
    for (auto i = 1; i <= mat.size(); ++i) {
      if (i == mat.size()) return mat[0][j];
      if (!binary_search(begin(mat[i]), end(mat[i]), mat[0][j])) break;
    }
  return -1;
}
```
In the solution above, we always search the entire row. We can improve the average time complexity if we  remember the position of an previously found element and use it as a starting point for the next search. We also can exit early and return ```-1``` if all elements in the row are smaller than value we search for.
> Note that ```lower_bound``` returns the position of the first element that is equal or greater (if no equal element exists) than the value. If there are no greater elements, the potion will point past the last element.
```
int smallestCommonElement(vector<vector<int>>& mat) {
  vector<int> last_pos(mat.size());
  for (auto j = 0; j < mat[0].size(); ++j)
    for (auto i = 1; i <= mat.size(); ++i) {
      if (i == mat.size()) return mat[0][j];
      last_pos[i] = lower_bound(begin(mat[i]) + last_pos[i], end(mat[i]), mat[0][j]) - begin(mat[i]);
      if (last_pos[i] >= mat[i].size()) return -1;
      if (mat[i][last_pos[i]] != mat[0][j]) break;
    }
  return -1;
}
```
### Additional Considerations
Since we search rows individually, this approach works correctly if there are duplicates.
### Complexity Analysis
- Time: *O(mn log m)*.
  - We iterate through *m* elements in the first row.
  - For each element in the first row, we perform binary search for *n* rows over *m* elements.
- Memory: 
  - *O(1)* for the original solution.
  - *O(n)* for the optimized solution to store search positions for every row.
 # Approach 3: Priority Queue
The idea is described in approach 2 and 3 for [23. Merge k Sorted List](https://leetcode.com/problems/merge-k-sorted-lists/solution/).
### Algorithm
Our priority queue will hold a value, and row and column of the corresponding element. We will use a sorted set as a queue so we can easily check if values of first and last element are the same.
- Populate the set with the first element from each row.
- Remove the first (smallest) element and insert the next element from the same row.
- If values of first and last elements is the set are the same, that means that all values are the same and we found the smallest common element.
```
int smallestCommonElement(vector<vector<int>>& mat) {
  set<array<int, 3>> s;
  for (auto i = 0; i < mat.size(); ++i) s.insert({ mat[i][0], i, 0 });
  while (s.size() == mat.size()) {
    auto& first_el = *begin(s), & last_el = *rbegin(s);
    if (first_el[0] == last_el[0]) return first_el[0];
    auto i = first_el[1], j = first_el[2];
    s.erase(begin(s));
    if (j + 1 < mat[i].size()) s.insert({ mat[i][j + 1], i, j + 1 });
  }
  return -1;
}
````
If you consider the improved solution in the approach 2, it might give you an idea on how to improve the average time complexity here. Instead of processing all elements in the row, we can skip ones that are smaller than the last (largest) element in our set. Even better, we can use a binary search to find next candidate element faster.
```
int smallestCommonElement(vector<vector<int>>& mat) {
  set<array<int, 3>> s;
  for (auto i = 0; i < mat.size(); ++i) s.insert({ mat[i][0], i, 0 });
  while (s.size() == mat.size()) {
    auto& first = *begin(s), & last = *rbegin(s);
    if (first[0] == last[0]) return first[0];
    auto i = first[1];
    auto j = lower_bound(begin(mat[i]) + first[2], end(mat[i]), last[0]) - begin(mat[i]);
    if (j < mat[i].size()) s.insert({ mat[i][j], i, j });
    s.erase(begin(s));  
  }
  return -1;
}  
```
### Additional Considerations
Since our queue contains one element from each row, this approach works correctly if there are duplicates.

This approach is faster that approach #2 above if the number of columns dominates the number of rows, and slower otherwise.
### Complexity Analysis
- Time: *O(mn log n)*.
  - We iterate through all elements in the array.
  - We insert each element into a priority queue of size *n*.
- Memory: *O(n)* for the queue.
</p>


