---
title: "Magnetic Force Between Two Balls"
weight: 1418
#id: "magnetic-force-between-two-balls"
---
## Description
<div class="description">
<p>In universe Earth&nbsp;C-137, Rick discovered a special form of magnetic force between&nbsp;two balls if they are put in his new invented basket. Rick has&nbsp;<code>n</code> empty baskets, the <code>i<sup>th</sup></code> basket is at <code>position[i]</code>, Morty has <code>m</code> balls and needs to distribute the balls into the baskets such that the <strong>minimum&nbsp;magnetic force</strong>&nbsp;between any two balls is <strong>maximum</strong>.</p>

<p>Rick stated that&nbsp;magnetic force between two different balls at positions <code>x</code> and <code>y</code> is <code>|x - y|</code>.</p>

<p>Given the integer array <code>position</code>&nbsp;and the integer <code>m</code>. Return <em>the required force</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/11/q3v1.jpg" style="width: 562px; height: 195px;" />
<pre>
<strong>Input:</strong> position = [1,2,3,4,7], m = 3
<strong>Output:</strong> 3
<strong>Explanation:</strong> Distributing the 3 balls into baskets 1, 4 and 7 will make the magnetic force between ball pairs [3, 3, 6]. The minimum magnetic force is 3. We cannot achieve a larger minimum magnetic force than 3.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> position = [5,4,3,2,1,1000000000], m = 2
<strong>Output:</strong> 999999999
<strong>Explanation:</strong> We can use baskets 1 and 1000000000.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == position.length</code></li>
	<li><code>2 &lt;= n &lt;= 10^5</code></li>
	<li><code>1 &lt;= position[i] &lt;= 10^9</code></li>
	<li>All integers in <code>position</code> are <strong>distinct</strong>.</li>
	<li><code>2 &lt;= m &lt;= position.length</code></li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Roblox - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Binary search solution with explanation and similar questions
- Author: alanlzl
- Creation Date: Sun Aug 16 2020 12:00:34 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 18 2020 02:25:53 GMT+0800 (Singapore Standard Time)

<p>
**Explaination**

We can use binary search to find the answer.

Define function `count(d)` that counts the number of balls can be placed in to baskets, under the condition that the minimum distance between any two balls is `d`.

We want to find the maximum `d` such that `count(d) == m`.
- If the `count(d) > m` , we have too many balls placed, so `d` is too small.
- If the `count(d) < m` , we don\'t have enough balls placed, so `d` is too large.


Since `count(d)` is monotonically decreasing with respect to `d`, we can use binary search to find the optimal `d`.

 <br />

**Complexity**

Time complexity: `O(Nlog(10^9))` or `O(NlogM)`, where `M = max(position) - min(position)`
Space complexity: `O(1)`

<br />

**Similar Questions**

Many other questions can be solved using similar techniques. I listed some of them below:
- [410. Split Array Largest Sum](https://leetcode.com/problems/split-array-largest-sum/)
- [774. Minimize Max Distance to Gas Station](https://leetcode.com/problems/minimize-max-distance-to-gas-station/)
- [875. Koko Eating Bananas](https://leetcode.com/problems/koko-eating-bananas/)
- [1011. Capacity To Ship Packages Within D Days](https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/)
- [1231. Divide Chocolate](https://leetcode.com/problems/divide-chocolate/)
- [1482. Minimum Number of Days to Make m Bouquets](https://leetcode.com/problems/minimum-number-of-days-to-make-m-bouquets/)

<br />


**Python**
```Python
class Solution:
    def maxDistance(self, position: List[int], m: int) -> int:
        n = len(position)
        position.sort()
        
        def count(d):
            ans, curr = 1, position[0]
            for i in range(1, n):
                if position[i] - curr >= d:
                    ans += 1
                    curr = position[i]
            return ans
        
        l, r = 0, position[-1] - position[0]
        while l < r:
            mid = r - (r - l) // 2
            if count(mid) >= m:
                l = mid
            else:
                r = mid - 1
        return l
```
</p>


### Simple Explanation
- Author: khaufnak
- Creation Date: Sun Aug 16 2020 12:00:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 22 2020 23:49:34 GMT+0800 (Singapore Standard Time)

<p>
Let ```x``` be the required answer. Then, we know that we can also place **all** ```m``` balls even if minimum distance between any two were 1, 2, ... (x-1). But, we cannot place **all** of them if minimum distance between any two were (x+1), (x+2), ...

Search space becomes:
1, 2, 3, 4, 5, ... x, x+1, x+2, x+3 (Each number represents minimum distance between each ball)
T, T, T, T, T, ..... T, F, F, F ....  (Can we place all ```m``` balls if minimum distance were above number? T = true, F = false)

Now, we need to find last occurrence of `true` in above array.

This is a general binary search structure. And turns the problem into: 
https://leetcode.com/problems/first-bad-version/

```
class Solution {
    public int maxDistance(int[] position, int m) {
        Arrays.sort(position);

        int hi = 1000000000;
        int lo = 1;

        while (lo < hi) {
            int mi = (lo + hi + 1) / 2; // Read comments if you wanna know why +1 is done.
            if (check(position, mi, m)) {
                lo = mi;
            } else {
                hi = mi - 1;
            }
        }

        return lo;
    }

    private boolean check(int[] position, int minimumDistance, int m) {
        // Always place first object at position[0]
        int lastBallPosition = position[0];
        int ballsLeftToBePlaced = m - 1;
        for (int i = 1; i < position.length && ballsLeftToBePlaced != 0; ) {
            if (position[i] - lastBallPosition < minimumDistance) {
                // Try to place the next ball, since this ball isn\'t minimumDistance away from lastBall
                i++;
            } else {
                // Place the ball only if this ball is farther than the previous ball by minimumDistance
                lastBallPosition = position[i];
                ballsLeftToBePlaced--;
            }
        }
        return ballsLeftToBePlaced == 0;
    }
}
```

Complexity: ```O(n logn)```
</p>


### Java binary search
- Author: mlogn
- Creation Date: Sun Aug 16 2020 12:06:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 19 2020 03:21:46 GMT+0800 (Singapore Standard Time)

<p>
```java
class Solution {
    public int maxDistance(int[] position, int m) {
        Arrays.sort(position);
        int lo = 0;
        int hi = position[position.length-1];
        int optimal = 0;
        while (lo <= hi) {
            int mid = lo + (hi-lo)/2;
            if (canPut(position, m, mid)) {
                optimal = mid;
                lo = mid+1;
            } else {
                hi = mid-1;
            }
        }
        return optimal;
    }

    /*
    * returns whether we can put m balls such that minimum distance between two consecutive ball is always greater than or equal to the max.
    */
    private boolean canPut(int[] positions, int m, int max) {
        int count = 1;
        int last = positions[0];
        for (int i = 0; i < positions.length; i++) {
            if (positions[i] - last >= max) {
                last = positions[i];
                count++;
            }
        }
        return count >= m;
    }
}
```

**Similar Problems**
* https://leetcode.com/problems/divide-chocolate/
* https://leetcode.com/problems/minimum-number-of-days-to-make-m-bouquets/
* https://leetcode.com/problems/split-array-largest-sum/
* https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/
* https://leetcode.com/problems/minimize-max-distance-to-gas-station/
* https://leetcode.com/problems/minimum-number-of-days-to-make-m-bouquets/
* https://leetcode.com/problems/magnetic-force-between-two-balls/
</p>


