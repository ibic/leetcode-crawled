---
title: "Bomb Enemy"
weight: 344
#id: "bomb-enemy"
---
## Description
<div class="description">
<p>Given a 2D grid, each cell is either a wall <code>&#39;W&#39;</code>, an enemy <code>&#39;E&#39;</code> or empty <code>&#39;0&#39;</code> (the number zero), return the maximum enemies you can kill using one bomb.<br />
The bomb kills all the enemies in the same row and column from the planted point until it hits the wall since the wall is too strong to be destroyed.<br />
<strong>Note: </strong>You can only put the bomb at an empty cell.</p>

<p><strong>Example:</strong></p>

<div>
<pre>
<strong>Input: </strong><span id="example-input-1-1">[[&quot;0&quot;,&quot;E&quot;,&quot;0&quot;,&quot;0&quot;],[&quot;E&quot;,&quot;0&quot;,&quot;W&quot;,&quot;E&quot;],[&quot;0&quot;,&quot;E&quot;,&quot;0&quot;,&quot;0&quot;]]</span>
<strong>Output: </strong><span id="example-output-1">3 
<strong>Explanation: </strong></span>For the given grid,

0 E 0 0 
E 0 W E 
0 E 0 0

Placing a bomb at (1,1) kills 3 enemies.
</pre>
</div>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short O(mn) time O(n) space solution
- Author: StefanPochmann
- Creation Date: Sat Jun 18 2016 19:29:59 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 14:17:54 GMT+0800 (Singapore Standard Time)

<p>
Walk through the matrix. At the start of each non-wall-streak (row-wise or column-wise), count the number of hits in that streak and remember it. O(mn) time, O(n) space.

    int maxKilledEnemies(vector<vector<char>>& grid) {
        int m = grid.size(), n = m ? grid[0].size() : 0;
        int result = 0, rowhits, colhits[n];
        for (int i=0; i<m; i++) {
            for (int j=0; j<n; j++) {
                if (!j || grid[i][j-1] == 'W') {
                    rowhits = 0;
                    for (int k=j; k<n && grid[i][k] != 'W'; k++)
                        rowhits += grid[i][k] == 'E';
                }
                if (!i || grid[i-1][j] == 'W') {
                    colhits[j] = 0;
                    for (int k=i; k<m && grid[k][j] != 'W'; k++)
                        colhits[j] += grid[k][j] == 'E';
                }
                if (grid[i][j] == '0')
                    result = max(result, rowhits + colhits[j]);
            }
        }
        return result;
    }
</p>


### Simple DP solution in Java
- Author: maimaihu
- Creation Date: Tue Jun 21 2016 06:33:21 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 22 2018 04:49:56 GMT+0800 (Singapore Standard Time)

<p>
 only need to store one killed enemies value for a row and an array of each column; 
if current grid value is W, this means previous stored value becomes invalid, you need to recalculate. 


     public int maxKilledEnemies(char[][] grid) {
        if(grid == null || grid.length == 0 ||  grid[0].length == 0) return 0;
        int max = 0;
        int row = 0;
        int[] col = new int[grid[0].length];
        for(int i = 0; i<grid.length; i++){
            for(int j = 0; j<grid[0].length;j++){
                if(grid[i][j] == 'W') continue;
                if(j == 0 || grid[i][j-1] == 'W'){
                     row = killedEnemiesRow(grid, i, j);
                }
                if(i == 0 || grid[i-1][j] == 'W'){
                     col[j] = killedEnemiesCol(grid,i,j);
                }
                if(grid[i][j] == '0'){
                    max = (row + col[j] > max) ? row + col[j] : max;
                }
            }

        }
        
        return max;
    }

    //calculate killed enemies for row i from column j
    private int killedEnemiesRow(char[][] grid, int i, int j){
        int num = 0;
        while(j <= grid[0].length-1 && grid[i][j] != 'W'){
            if(grid[i][j] == 'E') num++;
            j++;
        }
        return num;
    }
    //calculate killed enemies for  column j from row i
    private int killedEnemiesCol(char[][] grid, int i, int j){
        int num = 0;
        while(i <= grid.length -1 && grid[i][j] != 'W'){
            if(grid[i][j] == 'E') num++;
            i++;
        }
        return num;
    }
</p>


### Java straightforward solution DP O(mn) time and space
- Author: xuyirui
- Creation Date: Sun Jun 19 2016 05:27:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 02:46:28 GMT+0800 (Singapore Standard Time)

<p>
The code might be a little bit long and there should exist more elegant one.
However the idea of this very straightforward. We do simply two traversals. One from upper left to bottom right, for each spot we compute enemies to its left and up including itself. The other traversal is from bottom right to upper left, we compute enemies to its right and down and in the same time we add them up all to find the maxKill. Any suggestion to make it more concise?


    public class Solution {
        public int maxKilledEnemies(char[][] grid) {
            if (grid == null || grid.length == 0) {
                return 0;
            }
            int m = grid.length, n = grid[0].length;
            Spot[][] spots = new Spot[m][n];
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    spots[i][j] = new Spot();
                    if (grid[i][j] == 'W') {
                        continue;
                    }
                    spots[i][j].up = (i == 0 ? 0 : spots[i - 1][j].up) + (grid[i][j] == 'E' ? 1 : 0);
                    spots[i][j].left = (j == 0 ? 0 : spots[i][j - 1].left) + (grid[i][j] == 'E' ? 1 : 0);
                }
            }
            
            int maxKill = 0;
            for (int i = m - 1; i >= 0; i--) {
                for (int j = n - 1; j >= 0; j--) {
                    if (grid[i][j] == 'W') {
                        continue;
                    }
                    spots[i][j].bottom = (i == m - 1 ? 0 : spots[i + 1][j].bottom) + (grid[i][j] == 'E' ? 1 : 0);
                    spots[i][j].right = (j == n - 1 ? 0 : spots[i][j + 1].right) + (grid[i][j] == 'E' ? 1 : 0);
                    
                    if (grid[i][j] == '0') {
                        maxKill = Math.max(maxKill, spots[i][j].up + spots[i][j].left + spots[i][j].bottom + spots[i][j].right);
                    }
                }
            }
            return maxKill;
        }
    }
    
    class Spot {
        int up; // enemies to its up including itself
        int left; // enemies to its left including itself
        int bottom;
        int right;
    }
</p>


