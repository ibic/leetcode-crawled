---
title: "Path with Maximum Probability"
weight: 1170
#id: "path-with-maximum-probability"
---
## Description
<div class="description">
<p>You are given an undirected weighted graph of&nbsp;<code>n</code>&nbsp;nodes (0-indexed), represented by an edge list where&nbsp;<code>edges[i] = [a, b]</code>&nbsp;is an undirected edge connecting the nodes&nbsp;<code>a</code>&nbsp;and&nbsp;<code>b</code>&nbsp;with a probability of success of traversing that edge&nbsp;<code>succProb[i]</code>.</p>

<p>Given two nodes&nbsp;<code>start</code>&nbsp;and&nbsp;<code>end</code>, find the path with the maximum probability of success to go from&nbsp;<code>start</code>&nbsp;to&nbsp;<code>end</code>&nbsp;and return its success probability.</p>

<p>If there is no path from&nbsp;<code>start</code>&nbsp;to&nbsp;<code>end</code>, <strong>return&nbsp;0</strong>. Your answer will be accepted if it differs from the correct answer by at most <strong>1e-5</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/09/20/1558_ex1.png" style="width: 187px; height: 186px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 3, edges = [[0,1],[1,2],[0,2]], succProb = [0.5,0.5,0.2], start = 0, end = 2
<strong>Output:</strong> 0.25000
<strong>Explanation:</strong>&nbsp;There are two paths from start to end, one having a probability of success = 0.2 and the other has 0.5 * 0.5 = 0.25.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/09/20/1558_ex2.png" style="width: 189px; height: 186px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 3, edges = [[0,1],[1,2],[0,2]], succProb = [0.5,0.5,0.3], start = 0, end = 2
<strong>Output:</strong> 0.30000
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/09/20/1558_ex3.png" style="width: 215px; height: 191px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 3, edges = [[0,1]], succProb = [0.5], start = 0, end = 2
<strong>Output:</strong> 0.00000
<strong>Explanation:</strong>&nbsp;There is no path between 0 and 2.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 10^4</code></li>
	<li><code>0 &lt;= start, end &lt; n</code></li>
	<li><code>start != end</code></li>
	<li><code>0 &lt;= a, b &lt; n</code></li>
	<li><code>a != b</code></li>
	<li><code>0 &lt;= succProb.length == edges.length &lt;= 2*10^4</code></li>
	<li><code>0 &lt;= succProb[i] &lt;= 1</code></li>
	<li>There is at most one edge between every two nodes.</li>
</ul>
</div>

## Tags
- Graph (graph)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Detailed Explanation - BFS
- Author: frankkkkk
- Creation Date: Sun Jul 12 2020 12:04:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 13 2020 08:04:41 GMT+0800 (Singapore Standard Time)

<p>
**Key notes:**
- Markov Chain
- Simply BFS, at every stage, you need to remember 2 things: **current node** + **current probability** at this node
- One node can be reached from mulitple paths, we cannot simply use a visited array or a set to avoid repeatness.
	- What we can do is to record "**best probability so far for each node**". Then add to queue for BFS only if: it can make a better prob for this current node.
# **Bellman Ford Algorithm: BFS with Queue**
```java
class State {
	int node;
	double prob;
	State(int _node, double _prob) {
		node = _node;
		prob = _prob;
	}
}

public double maxProbability(int n, int[][] edges, double[] succProb, int start, int end) {

	// build graph -> double[0]: node, double[1]: edge prob
	Map<Integer, List<double[]>> map = new HashMap<>();
	for (int i = 0; i < edges.length; ++i) {
		int[] edge = edges[i];

		map.putIfAbsent(edge[0], new ArrayList<>());
		map.putIfAbsent(edge[1], new ArrayList<>());

		map.get(edge[0]).add(new double[] {edge[1], succProb[i]});
		map.get(edge[1]).add(new double[] {edge[0], succProb[i]});
	}

	double[] probs = new double[n];  // best prob so far for each node
	Queue<State> queue = new LinkedList<>();
	queue.add(new State(start, 1.0));

	while (!queue.isEmpty()) {

		State state = queue.poll();
		int parent = state.node;
		double prob = state.prob;

		for (double[] child : map.getOrDefault(parent, new ArrayList<>())) {
			// add to queue only if: it can make a better prob
			if (probs[(int) child[0]] >= prob * child[1]) continue;

			queue.add(new State((int) child[0], prob * child[1]));
			probs[(int) child[0]] = prob * child[1];
		}

	}

	return probs[end];
}
```
# **Dijkstra Algorithm: PriorityQueue to Save Some Searches**
```java
class State {
	int node;
	double prob;
	State(int _node, double _prob) {
		node = _node;
		prob = _prob;
	}
}

public double maxProbability(int n, int[][] edges, double[] succProb, int start, int end) {

	// build graph -> double[0]: node, double[1]: parent-to-node prob
	Map<Integer, List<double[]>> map = new HashMap<>();
	for (int i = 0; i < edges.length; ++i) {
		int[] edge = edges[i];

		map.putIfAbsent(edge[0], new ArrayList<>());
		map.putIfAbsent(edge[1], new ArrayList<>());

		map.get(edge[0]).add(new double[] {edge[1], succProb[i]});
		map.get(edge[1]).add(new double[] {edge[0], succProb[i]});
	}

	double[] probs = new double[n];  // best prob so far for each node
	PriorityQueue<State> pq = new PriorityQueue<>((a, b) -> (((Double) b.prob).compareTo((Double) a.prob)));
	pq.add(new State(start, 1.0));

	while (!pq.isEmpty()) {

		State state = pq.poll();
		int parent = state.node;
		double prob = state.prob;

		if (parent == end) return prob;

		for (double[] child : map.getOrDefault(parent, new ArrayList<>())) {
			// add to pq only if: it can make a better prob
			if (probs[(int) child[0]] >= prob * child[1]) continue;

			pq.add(new State((int) child[0], prob * child[1]));
			probs[(int) child[0]] = prob * child[1];
		}

	}

	return 0;
}
```

# **Floyd\u2013Warshall Algorithm for Every Pair of Nodes**
If you want to calculate Maximum Probability for every pair of nodes, **Floyd\u2013Warshall Algorithm** can help. But it will TLE in this question, just for your reference.
```java
double[][] probs = new double[n][n];

for (int i = 0; i < succProb.length; ++i) {
	int a = edges[i][0];
	int b = edges[i][1];
	probs[a][b] = probs[b][a] = succProb[i];
}

for (int k = 0; k < n; ++k) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			probs[i][j] = Math.max(probs[i][j], probs[i][k] * probs[k][j]);
		}
	}
}

return probs[start][end];
```
</p>


### [Java/Python 3] 2 codes: Bellman Ford and Dijkstra's algorithm w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Jul 12 2020 12:28:36 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 14 2020 10:45:18 GMT+0800 (Singapore Standard Time)

<p>
**Update:**

**Q & A**
`Q1`: Why negative weights can be used in Dijkstra\'s algorithm here in Python 3 code? I thought it\'s not allowed.
`A1`: Excellent question, negative weight is used ONLY in `heapq` to order its priority by reversed weight order. In fact, when calculating Prob., we still use positive value `-prob`.
`Q2`: Why Belman Ford actually works when vertices and edges can be as large as `10000` and `20000`, respectively? If algo is `O(n * E)` it would be at least `200 000 000` operations, which is too much.
`A2`:  - credit to **@Binga45**: `O(n * E)` is a very loose upper bound, and the code actually prunes enough branches to make itself run fast; Specifically, the bellman ford code won\'t add already visited vertices in most of the scenarios as we are enforcing below condition before adding to the queue.
```
	if (p[cur] * succProb[index] > p[neighbor]) {
		p[neighbor] = p[cur] * succProb[index];
		q.offer(neighbor);
	}
```
So, let\'s imagine an edge v-t and the probability connecting them to be 1. We have visited `v `and based on probability p[v], we updated p[t] also to the same value as p[v] as the edge connecting them has a prob of 1. Now, when we add `t` to the queue, we won\'t revisit the already visited neighbor v as p[t] * 1 is not > p[v].

The only exception to the above scenario is when we have another vertex `u` in the same level(depth) as `v` with a p[u] > p[v] and probability of edge u-t is 1. Then, we will revisit `v` from `t`, because in this scenario, p[t] * 1 > p[v] as p[t] is updated by `u` vertex. -- credit to **@Binga45**

**End of Q & A.**

In case you are NOT familiar with Bellman Ford and Dijkstra\'s algorithm, the following links are excellent materials for you to learn:

**Bellman Ford algorithm:**
https://algs4.cs.princeton.edu/44sp/
https://algs4.cs.princeton.edu/44sp/BellmanFordSP.java.html
https://web.stanford.edu/class/archive/cs/cs161/cs161.1168/lecture14.pdf
https://en.wikipedia.org/wiki/Bellman%E2%80%93Ford_algorithm
https://www.geeks for geeks.org/bellman-ford-algorithm-dp-23/(remove the 2 spaces among the links to make it valid)

**Dijkstra\'s algorithm:**
https://algs4.cs.princeton.edu/44sp/
https://algs4.cs.princeton.edu/44sp/DijkstraSP.java.html
https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
https://www.geeks for geeks.org/dijkstras-shortest-path-algorithm-greedy-algo-7/ (remove the 2 spaces among the links to make it valid)

Similar problems:

[407. Trapping Rain Water II](https://leetcode.com/problems/trapping-rain-water-ii)
[499. The Maze III](https://leetcode.com/problems/the-maze-iii)
[505. The Maze II](https://leetcode.com/problems/the-maze-ii)
[743. Network Delay Time](https://leetcode.com/problems/network-delay-time/)
[787. Cheapest Flights Within K Stops](https://leetcode.com/problems/cheapest-flights-within-k-stops/)

----

**Note:** It is necessary to build a graph in order to quickly locate relevant edges for a specific vertex; otherwise, it would cost too much time, say O(E), to search the input `edges` all way for even one vertex.

----
**Bellman Ford:**
1. Initialize all vertices probabilities as `0`, except `start`, which is `1`;
2. Use BFS to traverse all reachable vertices from `start`, update the corresponding probilities whenever we can have a higher probability; otherwise, ignore that vertex; *Note: when forwarding one step, multiply the corresponding `succProb` value with the probaboility of current vertex;*
3. Repeat 2 till all probabilities reach their maximum values.
4. Return the probability of `end` as solution.

```java
    public double maxProbability(int n, int[][] edges, double[] succProb, int start, int end) {
        Map<Integer, List<int[]>> g = new HashMap<>();
        for (int i = 0; i < edges.length; ++i) {
            int a = edges[i][0], b = edges[i][1];
            g.computeIfAbsent(a, l -> new ArrayList<>()).add(new int[]{b, i});
            g.computeIfAbsent(b, l -> new ArrayList<>()).add(new int[]{a, i});
        }
        double[] p = new double[n];
        p[start] = 1d;
        Queue<Integer> q = new LinkedList<>(Arrays.asList(start));
        while (!q.isEmpty()) {
            int cur = q.poll();
            for (int[] a : g.getOrDefault(cur, Collections.emptyList())) {
                int neighbor = a[0], index = a[1];
                if (p[cur] * succProb[index] > p[neighbor]) {
                    p[neighbor] = p[cur] * succProb[index];
                    q.offer(neighbor);
                }
            }
        }
        return p[end];
    }
```
```python
    def maxProbability(self, n: int, edges: List[List[int]], succProb: List[float], start: int, end: int) -> float:
        g, dq = defaultdict(list), deque([start])
        for i, (a, b) in enumerate(edges):
            g[a].append([b, i])
            g[b].append([a, i])
        p = [0.0] * n    
        p[start] = 1.0
        while dq:
            cur = dq.popleft()
            for neighbor, i in g.get(cur,[]):
                if p[cur] * succProb[i] > p[neighbor]:
                    p[neighbor] = p[cur] * succProb[i]
                    dq.append(neighbor)
        return p[end]
```
**Analysis:**
Time: O(n * E), space: O(n + E), where E =  edges.length.

----

**Dijkstra:**
1. Initialize all vertices probabilities as `0`, except `start`, which is `1`;
2. Put all currently reachable vertices into a Priority Queue/heap, priority ordered by the corresponding current probability, REVERSELY;
3. Whenever popped out a vertex with currently highest probability, check if it is the `end` vertex; If yes, we have already found the solution; otherwise, traverse all its neighbors to update neighbors\' probabilities if necessary; *Note: when forwarding one step, multiply the corresponding `succProb` value with the probaboility of current vertex.*
4. Repeat 2 & 3 to find the max probability for `end`; If can NOT, return `0.0d`.
```java
    public double maxProbability(int n, int[][] edges, double[] succProb, int start, int end) {
        Map<Integer, List<int[]>> g = new HashMap<>();
        for (int i = 0; i < edges.length; ++i) {
            int a = edges[i][0], b = edges[i][1];
            g.computeIfAbsent(a, l -> new ArrayList<>()).add(new int[]{b, i});    
            g.computeIfAbsent(b, l -> new ArrayList<>()).add(new int[]{a, i});    
        }
        double[] p = new double[n];
        p[start] = 1d;
        PriorityQueue<Integer> pq = new PriorityQueue<>(Comparator.comparingDouble(i -> -p[i]));
        pq.offer(start);
        while (!pq.isEmpty()) {
            int cur = pq.poll();
            if (cur == end) {
                return p[end];
            }
            for (int[] a : g.getOrDefault(cur, Collections.emptyList())) {
                int neighbor = a[0], index = a[1];
                if (p[cur] * succProb[index] > p[neighbor]) {
                    p[neighbor] = p[cur] * succProb[index];
                    pq.offer(neighbor);
                }
            }
        }
        return 0d;
    }
```
```python
    def maxProbability(self, n: int, edges: List[List[int]], succProb: List[float], start: int, end: int) -> float:
        p, g = [0.0] * n, defaultdict(list)
        for index, (a, b) in enumerate(edges):
            g[a].append((b, index))
            g[b].append((a, index))
        p[start] = 1.0
        heap = [(-p[start], start)]    
        while heap:
            prob, cur = heapq.heappop(heap)
            if cur == end:
                return -prob
            for neighbor, index in g.get(cur, []):
                if -prob * succProb[index] > p[neighbor]:
                    p[neighbor] = -prob * succProb[index]
                    heapq.heappush(heap, (-p[neighbor], neighbor))
        return 0.0
```
**Analysis:**
Time: O(n + Elogn), space: O(n + E), where E =  edges.length.


</p>


### [Python] O(len(edges) * log(n)) Dijkstra's Algorithm with explanations.
- Author: Chanchal_Maji
- Creation Date: Sun Jul 12 2020 12:00:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 12 2020 12:05:58 GMT+0800 (Singapore Standard Time)

<p>
The idea here is to apply Dijkstra\'s Algorithm in an efficient way. Now, if the probability of an edge is `p`, we make the weight of that edge as `log2(1/p)`. This is because we want to find the path with max probability, i.e, we want to find the path with min inverse probabilities. Now, since Dijkstra\'s algorithm uses sum of the weights we use `log2` so that later on we can get the product of the probabilities. 


```python
from heapq import heappush, heappop
from math import log2

class Solution:
    def maxProbability(self, n: int, edges: List[List[int]], succProb: List[float], start: int, end: int) -> float:
        AdjList = [set() for _ in range(n)]
        for (u, v), p in zip(edges, succProb):
            AdjList[u].add((v, log2(1/p)))
            AdjList[v].add((u, log2(1/p)))
        dist = [float(\'inf\') for _ in range(n)]
        dist[start] = 0
        h = [(0, start)]
        while h:
            d, u = heappop(h)
            if d == dist[u]:
                for (v, p) in AdjList[u]:
                    if dist[u] + p < dist[v]:
                        dist[v] = dist[u] + p
                        heappush(h, (dist[v], v))
        return 1 / (2 ** dist[end])
```

Please `upvote` if u find this useful. ^_^ .
</p>


