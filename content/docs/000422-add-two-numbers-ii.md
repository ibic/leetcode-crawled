---
title: "Add Two Numbers II"
weight: 422
#id: "add-two-numbers-ii"
---
## Description
<div class="description">
<p>You are given two <b>non-empty</b> linked lists representing two non-negative integers. The most significant digit comes first and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.</p>

<p>You may assume the two numbers do not contain any leading zero, except the number 0 itself.</p>

<p><b>Follow up:</b><br />
What if you cannot modify the input lists? In other words, reversing the lists is not allowed.
</p>

<p>
<b>Example:</b>
<pre>
<b>Input:</b> (7 -> 2 -> 4 -> 3) + (5 -> 6 -> 4)
<b>Output:</b> 7 -> 8 -> 0 -> 7
</pre>
</p>
</div>

## Tags
- Linked List (linked-list)

## Companies
- Bloomberg - 13 (taggedByAdmin: true)
- Amazon - 9 (taggedByAdmin: false)
- Microsoft - 6 (taggedByAdmin: true)
- eBay - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Grab - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

**Prerequisites**

The problem is a combination of three basic problems:

- [Reverse Linked List](https://leetcode.com/problems/reverse-linked-list/solution/).

- [Add Strings](https://leetcode.com/problems/add-strings/solution/) - the good problem
to refresh textbook digit-by-digit addition algorithm.

- [Add Two Numbers](https://leetcode.com/problems/add-two-numbers/) - the same 
problem as the current one, but the digits are stored in _reverse order_.

**Time and Space Complexity To Target**

Each list should be parsed at least once, hence the best time complexity 
we could have is $$\mathcal{O}(N_1 + N_2)$$, where $$N_1$$ and $$N_2$$ are the numbers 
of elements in the lists.

Space complexity is more interesting. It's relatively standard for
linked list problems not to allocate any data structure 
but the output list. 
This way, one could target $$\mathcal{O}(1)$$ space complexity
without taking the output list into account.   

**How to Solve: Reverse Input vs. Reverse Output**

The standard textbook addition algorithm begins by summing the least-significant digits. Each digit is in the range from 0 to 9, 
and the "overflows" are managed by moving the carry into the next range.

![img](../Figures/445/textbook.png)
*Figure 1. Textbook Addition.*
{:align="center"}

That is quite convenient for the problem 
[Add Two Numbers](https://leetcode.com/problems/add-two-numbers/), 
where the digits are stored in _reverse order_.
In this problem, the digits are stored in _direct order_. 
There are two ways to proceed:

- Approach 1. The idea is to adapt input by reversing the input lists.
This way, one could parse reverse lists starting from the head and
use the textbook addition algorithm.

![img](../Figures/445/reverse.png)
*Figure 2. Approach 1: Reverse Input + Construct Output by Adding to Front.*
{:align="center"}

- Approach 2. The idea is to adapt the addition algorithm. 

![img](../Figures/445/adaptation2.png)
*Figure 3. Approach 2: Adapt Addition Algorithm + Reverse Output.*
{:align="center"}

<br /> 
<br />


--- 
#### Approach 1: Reverse Input + Construct Output by Adding to Front

![img](../Figures/445/reverse.png)
*Figure 4. Reverse Input + Construct Output by Adding to Front.*
{:align="center"}

**Algorithm**

- [Implement reverseList function](https://leetcode.com/problems/reverse-linked-list/solution/). 

- Reverse both input lists: `l1 = reverseList(l1)`, `l2 = reverseList(l2)`.

- Initialize the result list: `head = None`.

- Initialize the carry: `carry = 0`.

- Loop through lists `l1` and `l2` until you reach both ends.

    - Set `x1 = l1.val` if `l1` is not finished yet, and `x1 = 0` otherwise.
    
    - Set `x2 = l2.val` if `l2` is not finished yet, and `x2 = 0` otherwise.
    
    - Compute the current value: `val = (carry + x1 + x2) % 10`, and the 
    current carry: `carry = (carry + x1 + x2) / 10`.
    
    - Update the result by adding the current value to front.
    
    - Move to the next elements in the lists.

- If the carry is not equal to zero, append it to frond of the result list.

- Return the result list: `return head`.

**Implementation**

<iframe src="https://leetcode.com/playground/gmmPtqAi/shared" frameBorder="0" width="100%" height="500" name="gmmPtqAi"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N_1 + N_2)$$, where $$N_1 + N_2$$ is a number 
of elements in both lists.   

* Space complexity: $$\mathcal{O}(1)$$ space complexity
without taking the output list into account, and $$\mathcal{O}(\max(N_1, N_2))$$
to store the output list. 
<br /> 
<br />


---
#### Approach 2: Follow Up: Do not Reverse Input. 

![img](../Figures/445/adaptation2.png)
*Figure 5. Adapt Addition Algorithm + Reverse Output.*
{:align="center"}

**Algorithm**

- Find the length of both lists: `n1` and `n2`.

- Parse both lists and sum the corresponding positions without taking carry into account,
i.e. convert `3->3->3` + `7->7` into `3->10->10` and then into `10->10->3`.

    - To sum the corresponding positions, do the following:
    
        - If `n1 >= n2`, add to the current value the value of the node from the first list,
        and decrease the number of elements to parse: `n1 -= 1`.
        
        - If `n1 < n2`, add to the current value the value of the node from the 
        second list and decrease the number of elements to parse: `n2 -= 1`.
        
    - Update the result by adding the current value to the front.
    
- Now it's time to take care about the carry, to limit each node value by 9,
i.e. to convert `10->10->3` into `0->1->4` and then into `4->1->0`:

    - Initialize the carry `carry = 0`.

    - Re-initialize the current list: `curr1 = head` 
    and the output list: `head = None`.
    
    - Parse the current list `curr1`:
    
        - Normalize the current value to be less than 10:
        `val = (curr1.val + carry) % 10`, and keep the carry:
        `carry = (curr1.val + carry) // 10`.

        - Update the result by adding the current value to front.
    
        - Move to the next element in the list: `curr1 = curr1.next`.

- If the carry is not equal to zero, append it to frond of the result list.

- Return the result list: `return head`.

**Implementation**

<iframe src="https://leetcode.com/playground/gQsouJoH/shared" frameBorder="0" width="100%" height="500" name="gQsouJoH"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N_1 + N_2)$$, where $$N_1 + N_2$$ is a number 
of elements in both lists.   

* Space complexity: $$\mathcal{O}(1)$$ space complexity
without taking the output list into account, and $$\mathcal{O}(\max(N_1, N_2))$$
to store the output list. 
<br /> 
<br />


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy O(n) Java Solution using Stack
- Author: Hx2
- Creation Date: Sat Oct 29 2016 05:46:39 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 10:01:11 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        Stack<Integer> s1 = new Stack<Integer>();
        Stack<Integer> s2 = new Stack<Integer>();
        
        while(l1 != null) {
            s1.push(l1.val);
            l1 = l1.next;
        };
        while(l2 != null) {
            s2.push(l2.val);
            l2 = l2.next;
        }
        
        int sum = 0;
        ListNode list = new ListNode(0);
        while (!s1.empty() || !s2.empty()) {
            if (!s1.empty()) sum += s1.pop();
            if (!s2.empty()) sum += s2.pop();
            list.val = sum % 10;
            ListNode head = new ListNode(sum / 10);
            head.next = list;
            list = head;
            sum /= 10;
        }
        
        return list.val == 0 ? list.next : list;
    }
}
```
</p>


### C++ O(1) extra space except for output. Reverse output instead. Is this cheating?
- Author: jade86
- Creation Date: Sun Nov 13 2016 12:47:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 02:54:16 GMT+0800 (Singapore Standard Time)

<p>
Idea is to reverse output instead of input. Not sure if this is cheating.
```
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        int n1 = 0, n2 = 0, carry = 0;
        ListNode *curr1 = l1, *curr2 = l2, *res = NULL;
        while( curr1 ){ curr1=curr1->next; n1++; }
        while( curr2 ){ curr2=curr2->next; n2++; } 
        curr1 = l1; curr2 = l2;
        while( n1 > 0 && n2 > 0){
            int sum = 0;
            if( n1 >= n2 ){ sum += curr1->val; curr1=curr1->next; n1--;}
            if( n2 > n1 ){ sum += curr2->val; curr2=curr2->next; n2--;}
            res = addToFront( sum, res );
        }
        curr1 = res; res = NULL;
        while( curr1 ){
            curr1->val += carry; carry = curr1->val/10;
            res = addToFront( curr1->val%10, res );
            curr2 = curr1; 
            curr1 = curr1->next;
            delete curr2;
        }
        if( carry ) res = addToFront( 1, res );
        return res;
    }
    ListNode* addToFront( int val, ListNode* head ){
        ListNode* temp = new ListNode(val);
        temp->next = head;
        return temp;
    }
```
</p>


### Java O(n) recursive solution by counting the difference of length
- Author: sometimescrazy
- Creation Date: Sat Oct 29 2016 08:19:37 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 28 2018 19:23:31 GMT+0800 (Singapore Standard Time)

<p>
````
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int size1 = getLength(l1);
        int size2 = getLength(l2);
        ListNode head = new ListNode(1);
        // Make sure l1.length >= l2.length
        head.next = size1 < size2 ? helper(l2, l1, size2 - size1) : helper(l1, l2, size1 - size2);
        // Handle the first digit
        if (head.next.val > 9) {
            head.next.val = head.next.val % 10;
            return head;
        }
        return head.next;
    }
    // get length of the list
    public int getLength(ListNode l) {
        int count = 0;
        while(l != null) {
            l = l.next;
            count++;
        }
        return count;
    }
    // offset is the difference of length between l1 and l2
    public ListNode helper(ListNode l1, ListNode l2, int offset) {
        if (l1 == null) return null;
        // check whether l1 becomes the same length as l2
        ListNode result = offset == 0 ? new ListNode(l1.val + l2.val) : new ListNode(l1.val);
        ListNode post = offset == 0 ? helper(l1.next, l2.next, 0) : helper(l1.next, l2, offset - 1);
        // handle carry 
        if (post != null && post.val > 9) {
            result.val += 1;
            post.val = post.val % 10;
        }
        // combine nodes
        result.next = post;
        return result;
    }
````
</p>


