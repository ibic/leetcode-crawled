---
title: "Robot Bounded In Circle"
weight: 1026
#id: "robot-bounded-in-circle"
---
## Description
<div class="description">
<p>On an infinite plane, a&nbsp;robot initially stands at <code>(0, 0)</code> and faces north.&nbsp;&nbsp;The robot can receive one of three instructions:</p>

<ul>
	<li><code>&quot;G&quot;</code>: go straight 1 unit;</li>
	<li><code>&quot;L&quot;</code>: turn 90 degrees to the left;</li>
	<li><code>&quot;R&quot;</code>: turn 90 degress to the right.</li>
</ul>

<p>The robot performs the <code>instructions</code> given in order, and repeats them forever.</p>

<p>Return <code>true</code> if and only if there exists a circle in the plane such that the robot never leaves the circle.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>&quot;GGLLGG&quot;
<strong>Output: </strong>true
<strong>Explanation: </strong>
The robot moves from (0,0) to (0,2), turns 180 degrees, and then returns to (0,0).
When repeating these instructions, the robot remains in the circle of radius 2 centered at the origin.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>&quot;GG&quot;
<strong>Output: </strong>false
<strong>Explanation: </strong>
The robot moves north indefinitely.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>&quot;GL&quot;
<strong>Output: </strong>true
<strong>Explanation: </strong>
The robot moves from (0, 0) -&gt; (0, 1) -&gt; (-1, 1) -&gt; (-1, 0) -&gt; (0, 0) -&gt; ...
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= instructions.length &lt;= 100</code></li>
	<li><code>instructions[i]</code> is in <code>{&#39;G&#39;, &#39;L&#39;, &#39;R&#39;}</code></li>
</ol>

</div>

## Tags
- Math (math)

## Companies
- Goldman Sachs - 41 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Paypal - 5 (taggedByAdmin: false)
- Qualtrics - 3 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Overview

The robot's [trajectory attractor](https://en.wikipedia.org/wiki/Attractor) 
is a set of trajectories toward which a system tends to evolve.
The question may sound a bit theoretical - is this attractor is limited or not. In other words, if there exists a circle in the plane such 
that the robot never leaves the circle.

Diverging Trajectory            |  Limit Cycle Trajectory
:------------------------------:|:------------------------------:
![bla](../Figures/1041/pic1.png)|  ![bla](../Figures/1041/pic2.png)

*Figure 1. Diverging Trajectory vs Limit Cycle Trajectory.*
{:align="center"}

Why is it interesting to know? There is a bunch of practical problems
related to topology, networks planning, and password brute-forcing. 
For all these problems, the first thing to understand is do we work within 
a limited space or the behavior of our system 
could drastically diverge at some point? 

Diverging Trajectory            |  Limit Cycle Trajectory
:------------------------------:|:------------------------------:
![bla](../Figures/1041/pic3.png)|  ![bla](../Figures/1041/pic4.png)

*Figure 2. Diverging Trajectory vs Limit Cycle Trajectory.*
{:align="center"}

**Draw some trajectories**

[Here is a Jupiter notebook used to draw all figures in this article](https://github.com/leetcode/solution_assets/blob/master/solution_assets/1041_robot_bounded_in_circle/robot_trajectory.ipynb).
Do not hesitate to play with it in local or on the online platforms.
Drawing different trajectories might help to notice some patterns. 

<br />
<br />


---
#### Approach 1: One Pass

**Intuition**

This solution is based on two facts about the limit cycle trajectory.

- After at most 4 cycles, the limit cycle trajectory returns to the initial point
`x = 0, y  = 0`. That is related to the fact that 4 directions 
(north, east, south, west) define the repeated cycles' plane symmetry [[proof]](#appendix-mathematical-proof). 

Ex. 1: Trajectory 1             |  Ex. 2: Trajectory 2
:------------------------------:|:------------------------------:
![bla](../Figures/1041/pic5.png)|  ![bla](../Figures/1041/pic6.png)

*Figure 3. After 4 cycles the limit cycle trajectory returns to the initial point `x = 0, y  = 0`.*
{:align="center"}

- We do not need to run 4 cycles to identify the limit cycle trajectory.
One cycle is enough. There could be two situations here.

    - First, if the robot returns to the initial point after one cycle, 
    that's the limit cycle trajectory. 

    - Second, if the robot doesn't face north at the end of the first cycle, 
    that's the limit cycle trajectory. 
    Once again, that's the consequence of the plane symmetry for the repeated cycles [[proof]](#appendix-mathematical-proof).
    
Ex. 1: Trajectory 1             |  Ex. 2: Trajectory 2
:------------------------------:|:------------------------------:
![bla](../Figures/1041/pic7.png)|  ![bla](../Figures/1041/pic8.png)

*Figure 4. If at the end of one cycle the robot doesn't face north, that's the limit cycle trajectory.*
{:align="center"}

**Algorithm**

- Let's use numbers from 0 to 3 to mark the directions: 
`north = 0`, `east = 1`, `south = 2`, `west = 3`.
In the array `directions` we could store corresponding coordinates changes,
_i.e._ `directions[0]` is to go north, `directions[1]` is to go east,
`directions[2]` is to go south, and `directions[3]` is to go west.

- The initial robot position is in the center `x = y = 0`, 
facing north `idx = 0`.

- Now everything is ready to iterate over the instructions.

    - If the current instruction is `R`, _i.e._ to turn on the right,
    the next direction is `idx = (idx + 1) % 4`. Modulo here
    is needed to deal with the situation - facing west, `idx = 3`,
    turn to the right to face north, `idx = 0`.  

    - If the current instruction is `L`, _i.e._ to turn on the left,
    the next direction could written in a symmetric way `idx = (idx - 1) % 4`. 
    That means we have to deal with negative indices. A more simple way is to
    notice that 1 turn to the left = 3 turns to the right: `idx = (idx + 3) % 4`.
    
    - If the current instruction is to move, we simply update the coordinates:
    `x += directions[idx][0]`, `y += directions[idx][1]`.
    
- After one cycle we have everything to decide. 
It's a limit cycle trajectory if the robot is back to the center: `x = y = 0`
or if the robot doesn't face north: `idx != 0`.

**Implementation**

<iframe src="https://leetcode.com/playground/D6YFxfcZ/shared" frameBorder="0" width="100%" height="497" name="D6YFxfcZ"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, where $$N$$ is a number of instructions
to parse. 
 
* Space complexity: $$\mathcal{O}(1)$$ because the array `directions` contains 
only 4 elements.

<br />
<br />


---
#### Appendix: Mathematical Proof

Let's provide a strict mathematical proof.  

**If the robot doesn't face north at the end of the first cycle, then that's the limit cycle trajectory.**

> First, let's check which direction the robot faces after 4 cycles.

Let's use numbers from 0 to 3 to mark the directions: 
`north = 0`, `east = 1`, `south = 2`, `west = 3`.
After one cycle the robot is facing direction `k != 0`. 

After 4 cycles, the robot faces direction `(k * 4) % 4 = 0`, i.e. 
_after 4 cycles, the robot is always facing north_.

> Second, let's find the robot coordinates after 4 cycles.

The robot initial coordinates are `x = y = 0`. After one cycle,
the new coordinates are $$x_1 = x + \Delta x$$, $$y_1 = y + \Delta y$$, where
both $$\Delta x$$ and $$\Delta y$$ could be positive or negative.

Let's consider four situations.

- After one cycle, the robot faces north.

Then here is what we have after 4 cycles:

$$x_4 = x + \Delta x + \Delta x - \Delta x + \Delta x = x + 4 \Delta x$$

$$y_4 = y + \Delta y + \Delta y + \Delta y + \Delta y = y + 4 \Delta y$$

- After one cycle, the robot faces east.

Then here is what we have after 4 cycles:

$$x_4 = x + \Delta x + \Delta y - \Delta x - \Delta y = x$$

$$y_4 = y + \Delta y - \Delta x - \Delta y + \Delta x = y$$

- After one cycle, the robot faces south.

Then here is what we have after 4 cycles:

$$x_4 = x + \Delta x - \Delta x + \Delta x - \Delta x = x$$

$$y_4 = y + \Delta y - \Delta y + \Delta y - \Delta y = y$$

- After one cycle, the robot faces west.

Then here is what we have after 4 cycles:

$$x_4 = x + \Delta x - \Delta y - \Delta x + \Delta y = x$$

$$y_4 = y + \Delta y + \Delta x - \Delta y - \Delta x = y$$

> Hence, if the robot doesn't face north at the end of the first cycle,
then after 4 cycles, the robot is back to the initial coordinates and faces north.

The following statement is a straight consequence. 

**After at most 4 cycles, the limit cycle trajectory returns to the initial point.**

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Let Chopper Help Explain
- Author: lee215
- Creation Date: Sun May 12 2019 12:02:52 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 27 2020 21:37:56 GMT+0800 (Singapore Standard Time)

<p>
I expect this problem to be medium problem.
![image](https://assets.leetcode.com/users/lee215/image_1557633739.png)

## **Intuition**
Let chopper help explain.

Starting at the origin and face north `(0,1)`,
after one sequence of `instructions`,
1. if chopper return to the origin, he is obvious in an circle.
2. if chopper finishes with face not towards north,
it will get back to the initial status in another one or three sequences.
<br>

## **Explanation**
`(x,y)` is the location of chopper.
`d[i]` is the direction he is facing.
`i = (i + 1) % 4` will turn right
`i = (i + 3) % 4` will turn left
Check the final status after `instructions`.
<br>

## **Complexity**
Time `O(N)`
Space `O(1)`

<br>

**Java:**
```
    public boolean isRobotBounded(String ins) {
        int x = 0, y = 0, i = 0, d[][] = {{0, 1}, {1, 0}, {0, -1}, { -1, 0}};
        for (int j = 0; j < ins.length(); ++j)
            if (ins.charAt(j) == \'R\')
                i = (i + 1) % 4;
            else if (ins.charAt(j) == \'L\')
                i = (i + 3) % 4;
            else {
                x += d[i][0]; y += d[i][1];
            }
        return x == 0 && y == 0 || i > 0;
    }
```

**C++:**
```
    bool isRobotBounded(string instructions) {
        int x = 0, y = 0, i = 0;
        vector<vector<int>> d = {{0, 1}, {1, 0}, {0, -1}, { -1, 0}};
        for (char & ins : instructions)
            if (ins == \'R\')
                i = (i + 1) % 4;
            else if (ins == \'L\')
                i = (i + 3) % 4;
            else
                x += d[i][0], y += d[i][1];
        return x == 0 && y == 0 || i > 0;
    }
```

**Python:**
```
    def isRobotBounded(self, instructions):
        x, y, dx, dy = 0, 0, 0, 1
        for i in instructions:
            if i == \'R\': dx, dy = dy, -dx
            if i == \'L\': dx, dy = -dy, dx
            if i == \'G\': x, y = x + dx, y + dy
        return (x, y) == (0, 0) or (dx, dy) != (0,1)
```

</p>


### A bad problem although it is easy
- Author: alenny
- Creation Date: Mon May 13 2019 05:31:08 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 13 2019 05:31:08 GMT+0800 (Singapore Standard Time)

<p>
This problem is easy. Just return true if the final position does not change or the facing direction is different from the beginning.
However, this problem is tricky. And I don\'t think a tricky problem can test or improve a developer\'s programming skill. 
I find more and more tricky problems recently. I feel it is not a good trend if LeetCode goes deeper into it.
</p>


### Python O(N) time, O(1) space, beats 100%, detailed explanations
- Author: chenby04
- Creation Date: Mon May 13 2019 00:49:00 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 13 2019 02:59:35 GMT+0800 (Singapore Standard Time)

<p>
There are only two scenarios where the robot can come back to its original location:
1) the robot is already back to its origin by the end of the string traversal, and 
2) the robot is away from the origin, but heading to a direction different from its initial direction. For example, if the robot is facing left by the end of the first string traversal, after three other traversals of left->left->left, it is back to the origin. A second example is that if the robot is facing down by the end of the first string traversal, it only takes another traversal for it to get back to the origin.

Another tip on how to change the directions. You actually do not need to keep a list of all possible directions [(0,1), (-1,0), (0,-1), (1,0)]. Just swap dx and dy and add a negative sign to one of it to rotate the direction by 90 degrees. The proof is that the dot product of (dx,dy) and (dy,-dx) is 0, meaning that these two vectors are perpendicular to each other. 
```
class Solution(object):
    def isRobotBounded(self, instructions):
        """
        :type instructions: str
        :rtype: bool
        """
        di = (0,1)
        x,y = 0,0
        for instruction in instructions:
            if instruction == \'G\':
                x,y = x+di[0],y+di[1]
            elif instruction == \'L\':
                di = (-di[1],di[0])
            else:
                di = (di[1],-di[0])
            
        return (x==0 and y==0) or di!=(0,1)
```
            

</p>


