---
title: "Longest Continuous Subarray With Absolute Diff Less Than or Equal to Limit"
weight: 1325
#id: "longest-continuous-subarray-with-absolute-diff-less-than-or-equal-to-limit"
---
## Description
<div class="description">
<p>Given an&nbsp;array of integers <code>nums</code> and an&nbsp;integer <code>limit</code>, return the size of the longest <strong>non-empty</strong> subarray such that the absolute difference between any two elements of this subarray is less than or equal to&nbsp;<code>limit</code><em>.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [8,2,4,7], limit = 4
<strong>Output:</strong> 2 
<strong>Explanation:</strong> All subarrays are: 
[8] with maximum absolute diff |8-8| = 0 &lt;= 4.
[8,2] with maximum absolute diff |8-2| = 6 &gt; 4. 
[8,2,4] with maximum absolute diff |8-2| = 6 &gt; 4.
[8,2,4,7] with maximum absolute diff |8-2| = 6 &gt; 4.
[2] with maximum absolute diff |2-2| = 0 &lt;= 4.
[2,4] with maximum absolute diff |2-4| = 2 &lt;= 4.
[2,4,7] with maximum absolute diff |2-7| = 5 &gt; 4.
[4] with maximum absolute diff |4-4| = 0 &lt;= 4.
[4,7] with maximum absolute diff |4-7| = 3 &lt;= 4.
[7] with maximum absolute diff |7-7| = 0 &lt;= 4. 
Therefore, the size of the longest subarray is 2.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [10,1,2,4,7,2], limit = 5
<strong>Output:</strong> 4 
<strong>Explanation:</strong> The subarray [2,4,7,2] is the longest since the maximum absolute diff is |2-7| = 5 &lt;= 5.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [4,2,2,2,4,4,2,2], limit = 0
<strong>Output:</strong> 3
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 10^9</code></li>
	<li><code>0 &lt;= limit &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Array (array)
- Sliding Window (sliding-window)

## Companies
- Google - 32 (taggedByAdmin: true)
- eBay - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (java)
```java
class Solution {
    public int longestSubarray(int[] nums, int limit) {
        int i = 0;
        int j;
//        int r = 0;
        Deque<Integer> maxDeq = new ArrayDeque<>();
        Deque<Integer> minDeq = new ArrayDeque<>();
        for (j = 0; j < nums.length; j++) {
            int cv = nums[j];
            while (!maxDeq.isEmpty() && maxDeq.peekLast() < cv) {
                maxDeq.pollLast();
            }
            maxDeq.offerLast(cv);
            while (!minDeq.isEmpty() && minDeq.peekLast() > cv) {
                minDeq.pollLast();
            }
            minDeq.offerLast(cv);
            if (maxDeq.peekFirst() - minDeq.peekFirst() > limit) {
                if (maxDeq.peekFirst() == nums[i]) {
                    maxDeq.pollFirst();
                }
                if (minDeq.peekFirst() == nums[i]) {
                    minDeq.pollFirst();
                }
                i++;
//                if (j - i > r) {
//                    r = j - i;
//                }
            }
        }
        return j - i;
//        return r;
    }
}
```

## Top Discussions
### [Java/C++/Python] Deques, O(N)
- Author: lee215
- Creation Date: Sun May 03 2020 12:05:50 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 30 2020 14:21:53 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Last week we learned,
in [1425. Constrained Subsequence Sum](https://leetcode.com/problems/constrained-subsequence-sum/discuss/597751/JavaC++Python-O(N)-Decreasing-Deque)
how to get minimum in a subarray when sliding.

This week, we need to get both the maximum and the minimum,
at the same time.

So I opened my post last week,
and copy some my own codes.
<br>


# **Solution 0**: Binary insert and remove
Keep an increasing list `L`.
Binary insert the current element.
If the `L[L.size() - 1] - L[0] > limit`,
binary search the position of `A[i]` and remove it from the list.

Time `O(N^2)`
Space `O(N)`
<br>

```python
    def longestSubarray(self, A, limit):
        i, L = 0, []
        for j in xrange(len(A)):
            bisect.insort(L, A[j])
            if L[-1] - L[0] > limit:
                L.pop(bisect.bisect(L, A[i]) - 1)
                i += 1
        return j - i + 1
```
<br>

# **Solution 1: Use two heaps**
Time `O(NogN)`
Space `O(N)`
<br>

**Python**
```py
    def longestSubarray(self, A, limit):
        maxq, minq = [], []
        res = i = 0
        for j, a in enumerate(A):
            heapq.heappush(maxq, [-a, j])
            heapq.heappush(minq, [a, j])
            while -maxq[0][0] - minq[0][0] > limit:
                i = min(maxq[0][1], minq[0][1]) + 1
                while maxq[0][1] < i: heapq.heappop(maxq)
                while minq[0][1] < i: heapq.heappop(minq)
            res = max(res, j - i + 1)
        return res
```
<br>

# **Solution 2: Use TreeMap**
Use one tree map can easily get the maximum and the minimum at the same time.
In java, we can use `TreeMap` to count elements.
In cpp, it suports multi treeset, that\'s even better.

Time `O(NogN)`
Space `O(N)`

**Java**
@prdp89
```java
public int longestSubarray(int[] A, int limit) {
    int i = 0, j;
    TreeMap<Integer, Integer> m = new TreeMap<>();
    for (j = 0; j < A.length; j++) {
        m.put(A[j], 1 + m.getOrDefault(A[j], 0));
        if (m.lastEntry().getKey() - m.firstEntry().getKey() > limit) {
            m.put(A[i], m.get(A[i]) - 1);
            if (m.get(A[i]) == 0)
                m.remove(A[i]);
            i++;
        }
    }
    return j - i;
}
```
**C++**
```cpp
    int longestSubarray(vector<int>& A, int limit) {
        int i = 0, j;
        multiset<int> m;
        for (j = 0; j < A.size(); ++j) {
            m.insert(A[j]);
            if (*m.rbegin() - *m.begin() > limit)
                m.erase(m.find(A[i++]));
        }
        return j - i;
    }
```
<br>


# **Solution 3: Use two deques**
Time `O(N)`
Space `O(N)`
<br>

**Java**
```java
    public int longestSubarray(int[] A, int limit) {
        Deque<Integer> maxd = new ArrayDeque<>();
        Deque<Integer> mind = new ArrayDeque<>();
        int i = 0, j;
        for (j = 0; j < A.length; ++j) {
            while (!maxd.isEmpty() && A[j] > maxd.peekLast()) maxd.pollLast();
            while (!mind.isEmpty() && A[j] < mind.peekLast()) mind.pollLast();
            maxd.add(A[j]);
            mind.add(A[j]);
            if (maxd.peek() - mind.peek() > limit) {
                if (maxd.peek() == A[i]) maxd.poll();
                if (mind.peek() == A[i]) mind.poll();
                ++i;
            }
        }
        return j - i;
    }
```
**C++**
```cpp
    int longestSubarray(vector<int>& A, int limit) {
        deque<int> maxd, mind;
        int i = 0, j;
        for (j = 0; j < A.size(); ++j) {
            while (!maxd.empty() && A[j] > maxd.back()) maxd.pop_back();
            while (!mind.empty() && A[j] < mind.back()) mind.pop_back();
            maxd.push_back(A[j]);
            mind.push_back(A[j]);
            if (maxd.front() - mind.front() > limit) {
                if (maxd.front() == A[i]) maxd.pop_front();
                if (mind.front() == A[i]) mind.pop_front();
                ++i;
            }
        }
        return j - i;
    }
```
**Python**
```py
    def longestSubarray(self, A, limit):
        maxd = collections.deque()
        mind = collections.deque()
        i = 0
        for a in A:
            while len(maxd) and a > maxd[-1]: maxd.pop()
            while len(mind) and a < mind[-1]: mind.pop()
            maxd.append(a)
            mind.append(a)
            if maxd[0] - mind[0] > limit:
                if maxd[0] == A[i]: maxd.popleft()
                if mind[0] == A[i]: mind.popleft()
                i += 1
        return len(A) - i
```
<br>

# More Similar Sliding Window Problems
Here are some similar sliding window problems.
Also find more explanations.
If you have question about the complexity and `if/while` clause,
pick an easier one first.

- 1425. [Constrained Subsequence Sum](https://leetcode.com/problems/constrained-subsequence-sum/discuss/597751/JavaC++Python-O(N)-Decreasing-Deque)
- 1358. [Number of Substrings Containing All Three Characters](https://leetcode.com/problems/number-of-substrings-containing-all-three-characters/discuss/516977/JavaC++Python-Easy-and-Concise)
- 1248. [Count Number of Nice Subarrays](https://leetcode.com/problems/count-number-of-nice-subarrays/discuss/419378/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
- 1234. [Replace the Substring for Balanced String](https://leetcode.com/problems/replace-the-substring-for-balanced-string/discuss/408978/javacpython-sliding-window/367697)
- 1004. [Max Consecutive Ones III](https://leetcode.com/problems/max-consecutive-ones-iii/discuss/247564/javacpython-sliding-window/379427?page=3)
-  930. [Binary Subarrays With Sum](https://leetcode.com/problems/binary-subarrays-with-sum/discuss/186683/)
-  992. [Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/discuss/234482/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
-  904. [Fruit Into Baskets](https://leetcode.com/problems/fruit-into-baskets/discuss/170740/Sliding-Window-for-K-Elements)
-  862. [Shortest Subarray with Sum at Least K](https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/discuss/143726/C%2B%2BJavaPython-O(N)-Using-Deque)
-  209. [Minimum Size Subarray Sum](https://leetcode.com/problems/minimum-size-subarray-sum/discuss/433123/JavaC++Python-Sliding-Window)
<br>

# More Good Stack Problems
Here are some stack problems that impressed me.

- 1425. [Constrained Subsequence Sum](https://leetcode.com/problems/constrained-subsequence-sum/discuss/597751/JavaC++Python-O(N)-Decreasing-Deque)
- 1130. [Minimum Cost Tree From Leaf Values](https://leetcode.com/problems/minimum-cost-tree-from-leaf-values/discuss/339959/One-Pass-O(N)-Time-and-Space)
- 907. [Sum of Subarray Minimums](https://leetcode.com/problems/sum-of-subarray-minimums/discuss/170750/C++JavaPython-Stack-Solution)
- 901. [Online Stock Span](https://leetcode.com/problems/online-stock-span/discuss/168311/C++JavaPython-O(1))
- 856. [Score of Parentheses](https://leetcode.com/problems/score-of-parentheses/discuss/141777/C++JavaPython-O(1)-Space)
- 503. [Next Greater Element II](https://leetcode.com/problems/next-greater-element-ii/discuss/98270/JavaC++Python-Loop-Twice)
- 496. Next Greater Element I
- 84. Largest Rectangle in Histogram
- 42. Trapping Rain Water
<br>

# **FAQ**
**Q:** Why doest the return value work? Why use `if` instead of `while`
**A:** Please refer to the discussion of @hzfmer
(Maybe I should summary up an explanation)

**Q:** Is your first thought compared with what @hzfmer suggests?
**A:**  If you follow my posts, you\'ll know that I use them everywhere.

</p>


### [Java] Detailed Explanation - Sliding Window - Deque - O(N)
- Author: frankkkkk
- Creation Date: Sun May 03 2020 12:03:40 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 04 2020 10:01:57 GMT+0800 (Singapore Standard Time)

<p>
**Key Notes:**
1. "Absolute difference between **any two elements** is less than or equal to limit" is basically => "Absolute difference between **min and max elements** of subarray"
2. Now the question becomes => find the longest subarray in which the absolute difference between min and max is less than or equal to limit. What we can do is to have two pointers: left and right, and then **find the longest subarray for every right pointer (iterate it) by shrinking left pointer.** And return the longest one among them.
3. Let\'s work on sliding window max first. How to efficiently find [239. sliding window maximum](https://leetcode.com/problems/sliding-window-maximum/)? 
	By using max Deque. We maintain list of max element candidates in **monotonically decreasing order**. Everytime the right pointer reaches a new position, we need to dequeue the "tail" element who is smaller than the nums[right]. Since, **those "old small tail" elements will never be the range maximum from now on.** After "clean up" the "old small tail" elements, add nums[right] into the deque, and then, **the head of deque is the current maximum.**
	
	Same for the min Deque. Move right poniter by 1, and clean up "old big tail" elements, add nums[right], the head of deque is the current minimum.
	
	What we should do next is to **shrink left pointer** because of limit. If current.max - current.min > limit. We should move the left pointer. Accordingdly, we need to **update our min max deques**. If head of max deque is equal to the nums[left], that means, **it is the one, we need to dequeue it**, since we are gonna move the left pointer by 1. (Note: nums[left] will be never larger than head of max deque, and if nums[left] is smaller than the head, we good, keep moving left pointer until satisfying the limit).
	
```java
Deque<Integer> maxDeque = new LinkedList<>();
Deque<Integer> minDeque = new LinkedList<>();

int res = 1;

int l = 0;

// find the longest subarray for every right pointer by shrinking left pointer
for (int r = 0; r < nums.length; ++r) {

	// update maxDeque with new right pointer
	while (!maxDeque.isEmpty() && maxDeque.peekLast() < nums[r]) {
		maxDeque.removeLast();
	}
	maxDeque.addLast(nums[r]);

	// update minDeque with new right pointer
	while (!minDeque.isEmpty() && minDeque.peekLast() > nums[r]) {
		minDeque.removeLast();
	}
	minDeque.addLast(nums[r]);

	// shrink left pointer if exceed limit
	while (maxDeque.peekFirst() - minDeque.peekFirst() > limit) {
		if (maxDeque.peekFirst() == nums[l]) maxDeque.pollFirst();
		if (minDeque.peekFirst() == nums[l]) minDeque.pollFirst();
		++l;  // shrink it!
	}

	// update res
	res = Math.max(res, r - l + 1);
}

return res;
```
</p>


### [Python] Clean Monotonic Queue solution with detail explanation - O(N)
- Author: alanlzl
- Creation Date: Sun May 03 2020 12:01:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 19 2020 05:32:36 GMT+0800 (Singapore Standard Time)

<p>
**Explanation**

The idea is to use sliding window and two monotonic queues to keep track of the window max and window min. 

In the beginning,  we set both left pointer `l` and right pointer  `r` at index 0. We keep moving `r`  forward until the max absolute difference within the window exceeds the limit. Then, we move forward `l` until the max absolute difference falls back within the limit. 

We know that:

- max absolute difference = max value within the window - min value within the window

The tricky part  is to get the max and min value within a window. To do so, we can use two monotonic queues: `min_deque` and `max_deque`.

`min_deque` is monotonically increasing and `max_deque` is monotonically decreasing. In this way, `min_deque[0]` will be the min value within a window and `max_deque[0]` will be the max value within the window.

We can store index in `min_deque` and `max_deque` to get rid of out-of-window numbers more easily. So we will use `nums[min_deque[0]]` instead of `min_deque[0]` to get numbers. The concept stays the same.

<br />

**Complexity**

Time complexity: `O(N)`
Space complexity: `O(N)`

<br />

**Python**
```
class Solution:
    def longestSubarray(self, nums: List[int], limit: int) -> int:
        min_deque, max_deque = deque(), deque()
        l = r = 0
        ans = 0
        while r < len(nums):
            while min_deque and nums[r] <= nums[min_deque[-1]]:
                min_deque.pop()
            while max_deque and nums[r] >= nums[max_deque[-1]]:
                max_deque.pop()
            min_deque.append(r)
            max_deque.append(r)
            
            while nums[max_deque[0]] - nums[min_deque[0]] > limit:
                l += 1
                if l > min_deque[0]:
                    min_deque.popleft()
                if l > max_deque[0]:
                    max_deque.popleft()
            
            ans = max(ans, r - l + 1)
            r += 1
                
        return ans
```
</p>


