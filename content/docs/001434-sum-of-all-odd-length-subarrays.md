---
title: "Sum of All Odd Length Subarrays"
weight: 1434
#id: "sum-of-all-odd-length-subarrays"
---
## Description
<div class="description">
<p>Given an array of positive integers&nbsp;<code>arr</code>, calculate the sum of all possible odd-length subarrays.</p>

<p>A subarray is a contiguous&nbsp;subsequence of the array.</p>

<p>Return&nbsp;<em>the sum of all odd-length subarrays of&nbsp;</em><code>arr</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,4,2,5,3]
<strong>Output:</strong> 58
<strong>Explanation: </strong>The odd-length subarrays of arr and their sums are:
[1] = 1
[4] = 4
[2] = 2
[5] = 5
[3] = 3
[1,4,2] = 7
[4,2,5] = 11
[2,5,3] = 10
[1,4,2,5,3] = 15
If we add all these together we get 1 + 4 + 2 + 5 + 3 + 7 + 11 + 10 + 15 = 58</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2]
<strong>Output:</strong> 3
<b>Explanation: </b>There are only 2 subarrays of odd length, [1] and [2]. Their sum is 3.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [10,11,12]
<strong>Output:</strong> 66
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 100</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- LinkedIn - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] O(N) Time, O(1) Space
- Author: lee215
- Creation Date: Sun Sep 20 2020 00:03:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 27 2020 15:51:51 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Hmmm, totally not an easy problem.
That where it\'s misleading:
It lets brute force get accepted,
and mark it as easy.
<br>

# Solution 1: Brute Force
Enumerate all possible odd length of subarray.
Time `O(n^3)`
Space `O(1)`
<br>

**python**
```py
    def sumOddLengthSubarrays(self, A):
        n = len(A)
        res = 0
        for l in xrange(1, n + 1, 2):
            for i in xrange(n - l + 1):
                res += sum(A[i:i + l])
        return res
```
**Python 1 line**
```py
    def sumOddLengthSubarrays(self, A):
        return sum(sum(A[i:i + l]) for l in xrange(1, 100, 2) for i in xrange(len(A) - l + 1))
```
<br><br>

# Solution 2: Consider the contribution of A[i]
Also suggested by @mayank12559 and @simtully.

Consider the subarray that contains `A[i]`,
we can take 0,1,2..,i elements on the left,
from A[0] to A[i],
we have i + 1 choices.

we can take 0,1,2..,n-1-i elements on the right,
from A[i] to A[n-1],
we have n - i choices.

In total, there are `(i + 1) * (n - i)` subarrays, that contains `A[i]`.
And there are `((i + 1) * (n - i) + 1) / 2` subarrays with odd length, that contains `A[i]`.
`A[i]` will be counted `((i + 1) * (n - i) + 1) / 2` times.
<br>

# Example of array [1,2,3,4,5]

1 2 3 4 5 subarray length 1
1 2 X X X subarray length 2
X 2 3 X X subarray length 2
X X 3 4 X subarray length 2
X X X 4 5 subarray length 2
1 2 3 X X subarray length 3
X 2 3 4 X subarray length 3
X X 3 4 5 subarray length 3
1 2 3 4 X subarray length 4
X 2 3 4 5 subarray length 4
1 2 3 4 5 subarray length 5

5 8 9 8 5 total times each index was added.
3 4 5 4 3 total times in odd length array with (x + 1) / 2
2 4 4 4 2 total times in even length array with x / 2
<br>

# Complexity
Time `O(N)`
Space `O(1)`
<br>

**Java**
```java
    public int sumOddLengthSubarrays(int[] A) {
        int res = 0, n = A.length;
        for (int i = 0; i < n; ++i) {
            res += ((i + 1) * (n - i) + 1) / 2 * A[i];
        }
        return res;
    }
```
**C++:**
```cpp
    int sumOddLengthSubarrays(vector<int>& A) {
        int res = 0, n = A.size();
        for (int i = 0; i < n; ++i) {
            res += ((i + 1) * (n - i) + 1) / 2 * A[i];
        }
        return res;
    }
```

**Python:**
```py
    def sumOddLengthSubarrays(self, A):
        res, n = 0, len(A)
        for i, a in enumerate(A):
            res += ((i + 1) * (n - i) + 1) / 2 * a
        return res

    def sumOddLengthSubarrays(self, A):
        return sum(((i + 1) * (len(A) - i) + 1) / 2 * a for i, a in enumerate(A))
```

</p>


### [C++] O(N) -Single Line Logic
- Author: rahulanandyadav2000
- Creation Date: Sun Sep 20 2020 00:45:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 21 2020 02:27:20 GMT+0800 (Singapore Standard Time)

<p>
	Just see number of times every position occurs in all the odd length subarray.
	Multiply the contribution of position with element at that position.
	
	How to calculate contribution?
	Every element will contribute size of array to its right (n-i) * size of array to its left(i+1)
	Since here we only consider odd length divide it by two (ceil divison)
	
	
	Example 
	Given array: arr = [1, 2, 3, 4, 5] (n = 5) and formula (i + 1) * (n - i)
	i = 0, contribution = 1 * 5 = 5
	i = 1, contribution = 2 * 4 = 8
	i = 2, contribution = 3 * 3 = 9
	i = 3, contribution = 4 * 2 = 8
	i = 4, contribution = 5 * 1 = 5

	For detailed explanation on example refer to comment below
	Please upvote if you like the approach :)

```
int sumOddLengthSubarrays(vector<int>& arr) {
        int ans = 0,n=arr.size(),i;
        for(i =0;i<n;i++)
		{
            int contribution = ceil((i+1)*(n-i)/2.0);
            ans+=(contribution*arr[i]);
        }
        return ans;
    }
```

	Time Complexity O(N)
</p>


### [Java/Python 3] Prefix Sum.
- Author: rock
- Creation Date: Sun Sep 20 2020 00:01:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 20 2020 00:01:17 GMT+0800 (Singapore Standard Time)

<p>
[Java/Python 3] Prefix Sum.

```java
    public int sumOddLengthSubarrays(int[] arr) {
        int ans = 0, n = arr.length;
        int[] prefixSum = new int[n + 1];
        for (int i = 0; i < n; ++i) {
            prefixSum[i + 1] = prefixSum[i] + arr[i];
        }
        for (int i = 0; i < n; ++i) {
            for (int j = i + 1; j <= n; j += 2) {
                ans += prefixSum[j] - prefixSum[i];
            }
        }
        return ans;        
    }
```
```python
    def sumOddLengthSubarrays(self, arr: List[int]) -> int:
        n, sum_odd = len(arr), 0
        p_sum = [0] * ( n + 1)
        for i, a in enumerate(arr):
            p_sum[i + 1] = p_sum[i] + a
        for i, p in enumerate(p_sum):
            for j in range(i + 1, n + 1, 2):
                sum_odd += p_sum[j] - p_sum[i] 
        return sum_odd
```
</p>


