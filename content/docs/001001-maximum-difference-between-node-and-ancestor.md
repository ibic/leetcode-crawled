---
title: "Maximum Difference Between Node and Ancestor"
weight: 1001
#id: "maximum-difference-between-node-and-ancestor"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a binary tree, find the maximum value <code>V</code> for which there exists <strong>different</strong> nodes <code>A</code> and <code>B</code> where <code>V = |A.val - B.val|</code>&nbsp;and <code>A</code> is an ancestor of <code>B</code>.</p>

<p>(A node A is an ancestor of B if either: any child of A is equal to B, or any child of A is an ancestor of B.)</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/09/09/2whqcep.jpg" style="height: 230px; width: 300px;" /></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[8,3,10,1,6,null,14,null,null,4,7,13]</span>
<strong>Output: </strong><span id="example-output-1">7</span>
<strong>Explanation: </strong>
We have various ancestor-node differences, some of which are given below :
|8 - 3| = 5
|3 - 7| = 4
|8 - 1| = 7
|10 - 13| = 3
Among all possible differences, the maximum value of 7 is obtained by |8 - 1| = 7.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>The number of nodes in the tree is between <code>2</code> and <code>5000</code>.</li>
	<li>Each node will have value between <code>0</code> and <code>100000</code>.</li>
</ol>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Facebook - 15 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Top Down
- Author: lee215
- Creation Date: Sun Apr 14 2019 12:06:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 14 2019 12:06:58 GMT+0800 (Singapore Standard Time)

<p>
We pass the minimum and maximum values to the children,
At the leaf node, we return `max - min` through the path from the root to the leaf.


**Java:**
```
    public int maxAncestorDiff(TreeNode root) {
        return dfs(root, root.val, root.val);
    }

    public int dfs(TreeNode root, int mn, int mx) {
        if (root == null) return mx - mn;
        mx = Math.max(mx, root.val);
        mn = Math.min(mn, root.val);
        return Math.max(dfs(root.left, mn, mx), dfs(root.right, mn, mx));
    }
```

**C++ 1-line**
```
    int maxAncestorDiff(TreeNode* r, int mn = 100000, int mx = 0) {
        return r ? max(maxAncestorDiff(r->left, min(mn, r->val), max(mx, r->val)),
        maxAncestorDiff(r->right, min(mn, r->val), max(mx, r->val))) : mx - mn;
    }
```
**Python 1-line**
```
    def maxAncestorDiff(self, root, mn=100000, mx=0):
        return max(self.maxAncestorDiff(root.left, min(mn, root.val), max(mx, root.val)), \
            self.maxAncestorDiff(root.right, min(mn, root.val), max(mx, root.val))) \
            if root else mx - mn
```

</p>


### Python/Java Recursion
- Author: hobiter
- Creation Date: Sun Apr 14 2019 12:19:20 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 30 2020 01:10:58 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def maxAncestorDiff(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root: return 0
        return self.helper(root, root.val, root.val)
    
    def helper(self, node, high, low):
        if not node:
            return high - low
        high = max(high, node.val)
        low = min(low, node.val)
        return max(self.helper(node.left, high, low), self.helper(node.right,high,low))
```

Java version:
```
class Solution {
    int res = 0;
    public int maxAncestorDiff(TreeNode root) {
        if (root == null) return 0;
        dfs(root, root.val, root.val);
        return res;
    }
    
    private void dfs(TreeNode node, int min, int max) {
        if (node == null) return;
        min = Math.min(node.val, min);
        max = Math.max(node.val, max);
        res = Math.max(res, max - min);
        dfs(node.left, min, max);
        dfs(node.right, min, max);
    }
}
```

</p>


### C++ track min/max top-down
- Author: votrubac
- Creation Date: Sun Apr 14 2019 12:01:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 14 2019 12:01:34 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
To make sure min/max values belong to an ancestor, we track min/max from the root till the leaf, and pick the biggest difference among all leaves.
# Solution
"Unpacked" version:
```
int maxAncestorDiff(TreeNode* r, int mn = 100000, int mx = 0) {
  if (r == nullptr) return mx - mn;
  mx = max(mx, r->val);
  mn = min(mn, r->val);
  return max(maxAncestorDiff(r->left, mn, mx), maxAncestorDiff(r->right, mn, mx));
}
```
And one-liner:
```
int maxAncestorDiff(TreeNode* r, int mn = 100000, int mx = 0) {
  return r == nullptr ? mx - mn :
    max(maxAncestorDiff(r->left, min(mn, r->val), max(mx, r->val)),
      maxAncestorDiff(r->right, min(mn, r->val), max(mx, r->val)));
}
```
</p>


