---
title: "N-ary Tree Postorder Traversal"
weight: 697
#id: "n-ary-tree-postorder-traversal"
---
## Description
<div class="description">
<p>Given an n-ary tree, return the <i>postorder</i> traversal of its nodes&#39; values.</p>

<p><em>Nary-Tree input serialization&nbsp;is represented in their level order traversal, each group of children is separated by the null value (See examples).</em></p>

<p>&nbsp;</p>

<p><strong>Follow up:</strong></p>

<p>Recursive solution is trivial, could you do it iteratively?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/12/narytreeexample.png" style="width: 100%; max-width: 300px;" /></p>

<pre>
<strong>Input:</strong> root = [1,null,3,2,4,null,5,6]
<strong>Output:</strong> [5,6,3,2,4,1]
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/11/08/sample_4_964.png" style="width: 296px; height: 241px;" /></p>

<pre>
<strong>Input:</strong> root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
<strong>Output:</strong> [2,6,14,11,7,3,12,8,4,13,9,10,5,1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The height of the n-ary tree is less than or equal to <code>1000</code></li>
	<li>The total number of nodes is between <code>[0,&nbsp;10^4]</code></li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### How to traverse the tree

First of all, please refer to [this article](https://leetcode.com/articles/binary-tree-postorder-transversal/) 
for the solution in case of binary tree.
This article offers the same ideas with a bit of generalisation. 

There are two general strategies to traverse a tree:

- *Breadth First Search* (`BFS`)

    We scan through the tree level by level, following the order of height,
    from top to bottom. The nodes on higher level would be visited before
    the ones with lower levels.
     
- *Depth First Search* (`DFS`)

    In this strategy, we adopt the `depth` as the priority, so that one
    would start from a root and reach all the way down to certain leaf,
    and then back to root to reach another branch.

    The DFS strategy can further be distinguished as
    `preorder`, `inorder`, and `postorder` depending on the relative order
    among the root node, left node and right node.
    
On the following figure the nodes are numerated in the order you visit them,
please follow ```1-2-3-4-5``` to compare different strategies.

![postorder](../Figures/145_transverse.png)

Here the problem is to implement postorder traversal using iterations.
<br />
<br />


---
#### Approach 1: Iterations

**Algorithm**

First of all, here is the definition of the ```TreeNode``` which we would use
in the following implementation.

<iframe src="https://leetcode.com/playground/j9wCDU7u/shared" frameBorder="0" width="100%" height="259" name="j9wCDU7u"></iframe>

Let's start from the root and then at each iteration 
pop the current node out of the stack and
push its child nodes. 
In the implemented strategy we push nodes into stack following the order ```Top->Bottom``` and ```Left->Right```.
Since DFS postorder traversal is ```Bottom->Top``` and ```Left->Right``` the output list
should be reverted after the end of loop.

<iframe src="https://leetcode.com/playground/X9jAEE3R/shared" frameBorder="0" width="100%" height="412" name="X9jAEE3R"></iframe>



**Complexity Analysis**

* Time complexity : we visit each node exactly once, thus the 
time complexity is $$\mathcal{O}(N)$$,
where $$N$$ is the number of nodes, *i.e.* the size of tree.

* Space complexity : depending on the tree structure, 
we could keep up to the entire tree, therefore, the space complexity is $$\mathcal{O}(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Iterative and Recursive Solutions
- Author: icydragoon
- Creation Date: Tue Jul 10 2018 16:12:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 23:56:57 GMT+0800 (Singapore Standard Time)

<p>
Iterative
```
class Solution {
    public List<Integer> postorder(Node root) {
        List<Integer> list = new ArrayList<>();
        if (root == null) return list;
        
        Stack<Node> stack = new Stack<>();
        stack.add(root);
        
        while(!stack.isEmpty()) {
            root = stack.pop();
            list.add(root.val);
            for(Node node: root.children)
                stack.add(node);
        }
        Collections.reverse(list);
        return list;
    }
}
```
Recursive
```
class Solution {
    List<Integer> list = new ArrayList<>();
    public List<Integer> postorder(Node root) {
        if (root == null)
            return list;
        
        for(Node node: root.children)
            postorder(node);
        
        list.add(root.val);
        
        return list;
    }
}
```
</p>


### C++ super easy 11-line solution, beats 100%!
- Author: MichaelZ
- Creation Date: Mon Jul 16 2018 13:22:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 12:05:59 GMT+0800 (Singapore Standard Time)

<p>
    vector<int> postorder(Node* root) {
        if(root==NULL) return {};
        vector<int> res;
        stack<Node*> stk;
        stk.push(root);
        while(!stk.empty())
        {
            Node* temp=stk.top();
            stk.pop();
            for(int i=0;i<temp->children.size();i++) stk.push(temp->children[i]);
            res.push_back(temp->val);
        }
        reverse(res.begin(), res.end());
        return res;
    }
</p>


### Python iterative and recursive solution with explanation 
- Author: hzhu007
- Creation Date: Thu Aug 02 2018 06:28:01 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 17:08:05 GMT+0800 (Singapore Standard Time)

<p>
**Recursion** is easy to implement and understand by definition https://en.wikipedia.org/wiki/Tree_traversal#Post-order_(LRN).
```
def postorder(self, root):
        """
        :type root: Node
        :rtype: List[int]
        """
        res = []
        if root == None: return res

        def recursion(root, res):
            for child in root.children:
                recursion(child, res)
            res.append(root.val)

        recursion(root, res)
        return res
```

**Iteration** is basically pre-order traversal but rather than go left, go right first then reverse its result.
```
def postorder(self, root):
        res = []
        if root == None: return res

        stack = [root]
        while stack:
            curr = stack.pop()
            res.append(curr.val)
            stack.extend(curr.children)

        return res[::-1]
```
</p>


