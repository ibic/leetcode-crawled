---
title: "Isomorphic Strings"
weight: 189
#id: "isomorphic-strings"
---
## Description
<div class="description">
<p>Given two strings <b><i>s</i></b> and <b><i>t</i></b>, determine if they are isomorphic.</p>

<p>Two strings are isomorphic if the characters in <b><i>s</i></b> can be replaced to get <b><i>t</i></b>.</p>

<p>All occurrences of a character must be replaced with another character while preserving the order of characters. No two characters may map to the same character but a character may map to itself.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> <b><i>s</i></b> = <code>&quot;egg&quot;, </code><b><i>t = </i></b><code>&quot;add&quot;</code>
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> <b><i>s</i></b> = <code>&quot;foo&quot;, </code><b><i>t = </i></b><code>&quot;bar&quot;</code>
<strong>Output:</strong> false</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> <b><i>s</i></b> = <code>&quot;paper&quot;, </code><b><i>t = </i></b><code>&quot;title&quot;</code>
<strong>Output:</strong> true</pre>

<p><b>Note:</b><br />
You may assume both <b><i>s&nbsp;</i></b>and <b><i>t&nbsp;</i></b>have the same length.</p>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Amazon - 8 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: true)
- Oracle - 2 (taggedByAdmin: false)
- Yahoo - 5 (taggedByAdmin: false)
- Yelp - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My 6 lines solution
- Author: grandyang
- Creation Date: Wed Apr 29 2015 13:09:35 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 18:13:02 GMT+0800 (Singapore Standard Time)

<p>
The idea is that we need to map a char to another one, for example, "egg" and "add", we need to constract the mapping \'e\' -> \'a\' and \'g\' -> \'d\'. Instead of directly mapping \'e\' to \'a\', another way is to mark them with same value, for example, \'e\' -> 1, \'a\'-> 1, and \'g\' -> 2, \'d\' -> 2, this works same.

So we use two arrays here m1 and m2, initialized space is 256 (Since the whole ASCII size is 256, 128 also works here). Traverse the character of both s and t on the same position, if their mapping  values in m1 and m2 are different, means they are not mapping correctly, returen false; else we construct the mapping, since m1 and m2 are both initialized as 0, we want to use a new value when i == 0, so i + 1 works here.

	
	class Solution {
    public:
        bool isIsomorphic(string s, string t) {
            int m1[256] = {0}, m2[256] = {0}, n = s.size();
            for (int i = 0; i < n; ++i) {
                if (m1[s[i]] != m2[t[i]]) return false;
                m1[s[i]] = i + 1;
                m2[t[i]] = i + 1;
            }
            return true;
        }
    };
</p>


### Python different solutions (dictionary, etc).
- Author: OldCodingFarmer
- Creation Date: Tue Jul 28 2015 21:36:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 04:15:39 GMT+0800 (Singapore Standard Time)

<p>
    def isIsomorphic1(self, s, t):
        d1, d2 = {}, {}
        for i, val in enumerate(s):
            d1[val] = d1.get(val, []) + [i]
        for i, val in enumerate(t):
            d2[val] = d2.get(val, []) + [i]
        return sorted(d1.values()) == sorted(d2.values())
            
    def isIsomorphic2(self, s, t):
        d1, d2 = [[] for _ in xrange(256)], [[] for _ in xrange(256)]
        for i, val in enumerate(s):
            d1[ord(val)].append(i)
        for i, val in enumerate(t):
            d2[ord(val)].append(i)
        return sorted(d1) == sorted(d2)
        
    def isIsomorphic3(self, s, t):
        return len(set(zip(s, t))) == len(set(s)) == len(set(t))
        
    def isIsomorphic4(self, s, t): 
        return [s.find(i) for i in s] == [t.find(j) for j in t]
        
    def isIsomorphic5(self, s, t):
        return map(s.find, s) == map(t.find, t)
    
    def isIsomorphic(self, s, t):
        d1, d2 = [0 for _ in xrange(256)], [0 for _ in xrange(256)]
        for i in xrange(len(s)):
            if d1[ord(s[i])] != d2[ord(t[i])]:
                return False
            d1[ord(s[i])] = i+1
            d2[ord(t[i])] = i+1
        return True
</p>


### Short Java solution without maps
- Author: shpolsky
- Creation Date: Wed Apr 29 2015 20:00:22 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 13:16:07 GMT+0800 (Singapore Standard Time)

<p>
Hi guys!

The main idea is to store the last seen positions of current (i-th) characters in both strings. If previously stored positions are different then we know that the fact they're occuring in the current i-th position simultaneously is a mistake. We could use a map for storing but as we deal with chars which are basically ints and can be used as indices we can do the whole thing with an array.

Check the code below. Happy coding! 

----------

    public class Solution {
        public boolean isIsomorphic(String s1, String s2) {
            int[] m = new int[512];
            for (int i = 0; i < s1.length(); i++) {
                if (m[s1.charAt(i)] != m[s2.charAt(i)+256]) return false;
                m[s1.charAt(i)] = m[s2.charAt(i)+256] = i+1;
            }
            return true;
        }
    }
</p>


