---
title: "HTML Entity Parser"
weight: 1306
#id: "html-entity-parser"
---
## Description
<div class="description">
<p><strong>HTML entity parser</strong> is the parser that takes HTML code as input and replace all the entities of the special characters by the characters itself.</p>

<p>The special characters and their entities for HTML are:</p>

<ul>
	<li><strong>Quotation Mark:</strong>&nbsp;the entity is <code>&amp;quot;</code> and&nbsp;symbol character is <code>&quot;</code>.</li>
	<li><strong>Single Quote&nbsp;Mark:</strong>&nbsp;the entity is <code>&amp;apos;</code> and&nbsp;symbol character is <code>&#39;</code>.</li>
	<li><strong>Ampersand:</strong>&nbsp;the entity is <code>&amp;amp;</code> and symbol character is <code>&amp;</code>.</li>
	<li><strong>Greater Than Sign:</strong>&nbsp;the entity is <code>&amp;gt;</code>&nbsp;and symbol character is <code>&gt;</code>.</li>
	<li><strong>Less Than Sign:</strong>&nbsp;the entity is <code>&amp;lt;</code>&nbsp;and symbol character is <code>&lt;</code>.</li>
	<li><strong>Slash:</strong>&nbsp;the entity is <code>&amp;frasl;</code> and&nbsp;symbol character is <code>/</code>.</li>
</ul>

<p>Given the input <code>text</code> string to the HTML parser, you have to implement the entity parser.</p>

<p>Return <em>the text</em> after replacing the entities by the special characters.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;&amp;amp; is an HTML entity but &amp;ambassador; is not.&quot;
<strong>Output:</strong> &quot;&amp; is an HTML entity but &amp;ambassador; is not.&quot;
<strong>Explanation:</strong> The parser will replace the &amp;amp; entity by &amp;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;and I quote: &amp;quot;...&amp;quot;&quot;
<strong>Output:</strong> &quot;and I quote: \&quot;...\&quot;&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;Stay home! Practice on Leetcode :)&quot;
<strong>Output:</strong> &quot;Stay home! Practice on Leetcode :)&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;x &amp;gt; y &amp;amp;&amp;amp; x &amp;lt; y is always false&quot;
<strong>Output:</strong> &quot;x &gt; y &amp;&amp; x &lt; y is always false&quot;
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;leetcode.com&amp;frasl;problemset&amp;frasl;all&quot;
<strong>Output:</strong> &quot;leetcode.com/problemset/all&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= text.length &lt;= 10^5</code></li>
	<li>The string may contain any possible characters out of all the 256&nbsp;ASCII characters.</li>
</ul>

</div>

## Tags
- String (string)
- Stack (stack)

## Companies
- Oracle - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ two-pointers O(n) | O(1)
- Author: votrubac
- Creation Date: Sun Apr 12 2020 15:42:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 09:07:23 GMT+0800 (Singapore Standard Time)

<p>
We can use two-pointers approach and re-use the input string to store the output:
- Copy the current character `text[p] = text[i]`
- Track the starting position of \'&\'
- When we see `\';\'`, check for the special character.
	- For a match, decrease the slow pointer `p`, and copy the corresponding symbol.

For true O(n) solution, we need to use Trie to do the matching. I feel lazy today so I just broke special characters by size to minimize checks.
> **Update:** see below for Trie-based, true O(n) solution.

```cpp
string entityParser(string text) {    
    vector<pair<string,char>> encoded[8] = {{}, {}, {}, {}, {{"&gt;", \'>\'}, {"&lt;", \'<\'}},
        {{"&amp;", \'&\'}}, {{"&quot;", \'"\'}, {"&apos;", \'\\'\'}}, {{"&frasl;", \'/\'}}};
    int st = 0, p = 0;
    for (auto i = 0; i < text.size(); ++i, ++p) {
        text[p] = text[i];
        if (text[p] == \'&\')
            st = p;
        if (text[p] == \';\') { 
            auto sz = p - st + 1;
            if (sz >= 4 && sz <= 7)
                for (auto &[enc, dec] : encoded[sz]) {
                    if (text.compare(st, sz, enc) == 0) {
                        p = st;
                        text[p] = dec;
                        break;
                    }
                }
            st = p + 1;
        }
    }
    text.resize(p);
    return text;
}
```
#### True O(n) Solution
```cpp
struct Trie {
    Trie * chars[26] = {};
    char symbol = 0;
    void insert(string &w, char ch, int p = 0) {
        if (p == w.size())
            symbol = ch;
        else {
            if (chars[w[p] - \'a\'] == nullptr)
                chars[w[p] - \'a\'] = new Trie();
            chars[w[p] - \'a\']->insert(w, ch, p + 1);
        }
    }
    char check(string &w, int p, int sz) {
        if (sz == 0 && symbol != 0)
            return symbol;
        if (w[p] < \'a\' || w[p] > \'z\' || chars[w[p] - \'a\'] == nullptr)
            return 0;
        return chars[w[p] - \'a\']->check(w, p + 1, sz - 1);
    }
};
vector<pair<string, char>> special = {{"gt", \'>\'}, {"lt", \'<\'},
    {"amp", \'&\'}, {"quot", \'"\'}, {"apos", \'\\'\'}, {"frasl", \'/\'}};
string entityParser(string &text) {
    Trie root;
    for (auto &[str, ch] : special)
        root.insert(str, ch);
    int st = 0, p = 0;
    for (auto i = 0; i < text.size(); ++i, ++p) {
        text[p] = text[i];
        if (text[p] == \'&\')
            st = p;
        if (text[p] == \';\') {
            auto ch = root.check(text, st + 1, p - st - 1);
            if (ch != 0) {
                p -= p - st;
                text[p] = ch;
            }
            st = p + 1;
        }
    }
    text.resize(p);
    return text;
}
```
</p>


### Java simple replace all
- Author: hobiter
- Creation Date: Fri Jun 05 2020 15:20:58 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 05 2020 15:21:28 GMT+0800 (Singapore Standard Time)

<p>
Only take care of "&" lastly;
```
public String entityParser(String text) {
        if (text == null || text.length() == 0) {
            return "";
        }
        Map<String, String> map = 
            Map.of("&quot;", "\"", "&apos;", "\'", 
                   "&gt;", ">", 
                   "&lt;", "<", "&frasl;", "/");
        for (String key : map.keySet()) {
            text = text.replaceAll(key, map.get(key));
        }
        return text.replaceAll("&amp;", "&");  // "&" must be put in last;
    }
```
</p>


### [C++] With explanation
- Author: white_shadow_
- Creation Date: Fri May 29 2020 10:14:33 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 29 2020 10:22:59 GMT+0800 (Singapore Standard Time)

<p>
https://youtu.be/wt-a_MTYuO8?t=138
```
Please Upvote :)
string entityParser(string text)
    {
        unordered_map<string,string> m;
        m["&quot;"]="\"";
        m["&apos;"]="\'";
        m["&amp;"]="&";
        m["&gt;"]=">";
        m["&lt;"]="<";
        m["&frasl;"]="/";
        string ans;
        for(int i=0;i<text.size();i++)
        {
            if(text[i]==\'&\')
            {
                string s;
                while(i<text.size())
                {
                    s+=text[i];
                    
                    if((text[i]==\';\')&&m.find(s)!=m.end())
                    {
                        ans+=m[s];
                        s="";
                        break;
                    }
                    else i++;
                    if(text[i]==\'&\')
                    {
                        i--;
                        break;
                    }
                }
                ans+=s;
            }
            else
            {
                ans+=text[i];
            }
        }
        return ans;
    }

</p>


