---
title: "Closest Binary Search Tree Value II"
weight: 255
#id: "closest-binary-search-tree-value-ii"
---
## Description
<div class="description">
<p>Given a non-empty binary search tree and a target value, find <i>k</i> values in the BST that are closest to the target.</p>

<p><b>Note:</b></p>

<ul>
	<li>Given target value is a floating point.</li>
	<li>You may assume <i>k</i> is always valid, that is: <i>k</i> &le; total nodes.</li>
	<li>You are guaranteed to have only one unique set of <i>k</i> values in the BST that are closest to the target.</li>
</ul>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> root = [4,2,5,1,3], target = 3.714286, and <em>k</em> = 2

    4
   / \
  2   5
 / \
1   3

<strong>Output:</strong> [4,3]</pre>

<p><b>Follow up:</b><br />
Assume that the BST is balanced, could you solve it in less than <i>O</i>(<i>n</i>) runtime (where <i>n</i> = total nodes)?</p>

</div>

## Tags
- Stack (stack)
- Tree (tree)

## Companies
- LinkedIn - 11 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- ForUsAll - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

The problem is a BST variation of the "kth-smallest" classical problem. It is 
popular both in Google and Facebook, but these two companies are waiting for you
to show different approaches to this problem. 
We're proposing 3 solutions here, and it's more an overview.

**Prerequisites**

Because of that, you might want first to check out the list of prerequisites:

- [Inorder traversal of BST is an array sorted in the ascending order.](https://leetcode.com/problems/delete-node-in-a-bst/solution/)
To compute inorder traversal follow the direction `Left -> Node -> Right`.

- [Closest BST value: find _one_ closest element](https://leetcode.com/problems/closest-binary-search-tree-value/solution/).

- [kth-smallest problem for the array could be solved by using heap in 
$$\mathcal{O}(N \log k)$$ time, or by using quickselect in $$\mathcal{O}(N)$$ time.](https://leetcode.com/problems/top-k-frequent-elements/solution/)

**Google vs. Facebook**

There are three ways to solve the problem:

- Approach 1. Sort, $$\mathcal{O}(N \log N)$$ time. The idea is to convert BST into an array, 
sort it by the distance to the target, and return the k closest elements.

- Approach 2. Facebook-friendly, heap, $$\mathcal{O}(N \log k)$$ time. 
We could use the heap of capacity k, sorted by the
distance to the target. It's not an optimal but very straightforward solution - 
traverse the tree, push the elements into the heap, and then return this heap. 
Facebook interviewer would insist on implementing this solution because the interviews are a bit shorter than Google ones, and it's important to get 
problem solved end-to-end.

- Approach 3. Google-friendly, quickselect, $$\mathcal{O}(N)$$ time.
[Here you could find a very detailed explanation of quickselect algorithm.](https://leetcode.com/problems/top-k-frequent-elements/solution/)
In this article, we're going to provide a relatively brief implementation. 
Google guys usually prefer the best-time solutions, well-structured clean skeleton,
even if you have no time to implement everything in time end-to-end.
<br /> 
<br />


--- 
#### Approach 1: Recursive Inorder + Sort, O(N log N) time

**Intuition**

![img](../Figures/272/inorder.png)
*Figure 1. Sort.*
{:align="center"}

The most straightforward approach is to build inorder traversal
and then find the k closest elements using build-in sort.  

**Algorithm**

- Build an inorder traversal array.

- Find the k closest to the target elements using build-in sort.

**Implementation**

<iframe src="https://leetcode.com/playground/d6fu73dD/shared" frameBorder="0" width="100%" height="412" name="d6fu73dD"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N \log N)$$. $$\mathcal{O}(N)$$ to build inorder traversal
and then $$\mathcal{O}(N \log N)$$ to sort it.

* Space complexity: $$\mathcal{O}(N)$$ to store list `nums` of $$N$$ elements.
<br /> 
<br />


---
#### Approach 2: Recursive Inorder + Heap, O(N log k) time

![img](../Figures/272/heap.png)
*Figure 2. Heap.*
{:align="center"}

**Algorithm**

- Instantiate the heap with "less close element first" 
strategy so that the heap contains the elements 
that are closest to the target. 

- Use inorder traversal to traverse the tree 
following the direction `Left -> Node -> Right`.

    - Push all elements into heap during the traversal, keeping the heap size less than or equal to $$k$$.

- As a result, the heap contains $$k$$ elements that are closest to target.
Convert it into a list and return.

**Implementation**

<iframe src="https://leetcode.com/playground/avn4tDSj/shared" frameBorder="0" width="100%" height="412" name="avn4tDSj"></iframe>

**Optimisations**

One could optimize the solution by adding the stop condition.
Inorder traversal pops the elements in the sorted order. Hence once
the distance of the current element to the target becomes greater than
the distance of the first element in a heap, 
one could stop the computations. The overall worst-case time
complexity would be still $$\mathcal{O}(N \log k)$$, but the average time
could be improved to $$\mathcal{O}(H \log k)$$, where $$H$$ is a tree height.

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N \log k)$$ to push N elements into 
the heap of the size $$k$$.  

* Space complexity: $$\mathcal{O}(k + H)$$ to keep the heap of k elements 
and the recursion stack of the tree height.
<br /> 
<br />


--- 
#### Approach 3: QuickSelect, O(N) time.

**Hoare's selection algorithm**

Quickselect is a [textbook algorithm](https://en.wikipedia.org/wiki/Quickselect) 
typically used to solve the problems "find `k`*th* something":
`k`*th* smallest, `k`*th* largest, etc. Like quicksort, quickselect was developed 
by [Tony Hoare](https://en.wikipedia.org/wiki/Tony_Hoare), 
and also known as _Hoare's selection algorithm_.

It has $$\mathcal{O}(N)$$ _average_ time complexity and widely used in practice. 
It is worth to note that its worst-case time complexity 
is $$\mathcal{O}(N^2)$$, although the probability of this worst-case 
is negligible.

The approach is the same as for quicksort.

> One chooses a pivot and defines its position in a sorted array in a 
linear time using the so-called _partition algorithm_. 

As an output, we have an array where the pivot is on its perfect position
in the ascending sorted array, sorted by the frequency. 
All elements on the left of the pivot are more close to the target 
than the pivot, and all elements on the right are less close 
or on the same distance from
the target.

The array is now split into two parts.
If by chance, our pivot element took `k`*th* final position, 
then $$k$$ elements on the left are these $$k$$ 
closest elements we're looking for. 
If not, we can choose one more pivot and 
place it in its perfect position.

![img](../Figures/272/quickselect.png)
*Figure 3. Quickselect.*
{:align="center"}

If that were a quicksort algorithm, one would have to process
both parts of the array. That would result in $$\mathcal{O}(N \log N)$$ time complexity.
In this case, there is no need to deal with both parts since one knows 
in which part to search for `k`*th* closest element, and that
reduces the average time complexity to $$\mathcal{O}(N)$$.

**Algorithm**

The algorithm is relatively straightforward:

* Traverse the tree and convert it into array `nums`. 

* Implement the simple function to compute the distance to the target. Note that the distance is not unique. 
That means we need a partition algorithm 
that works fine with _duplicates_. 

* Work with `nums` array. 
Use a partition scheme (please check the next section) to place the pivot 
into its perfect position `pivot_index` in the sorted array,
move more close elements to the left of the pivot, 
and less close or of the same distance - to the right.

* Compare `pivot_index` and `k`.
 
    - If `pivot_index == k`, the pivot is the
    `k`*th* less close element, and all elements on the left
    are the $$k$$ closest elements to the target. 
    Return these elements.
    
    - Otherwise, choose the side of the array to proceed recursively.

**Hoare's Partition vs. Lomuto's Partition**

There is a zoo of partition algorithms. The most simple 
one is [Lomuto's Partition Scheme](https://en.wikipedia.org/wiki/Quicksort#Lomuto_partition_scheme).

> The drawback of Lomuto's partition is that it fails with duplicates. 

Here we work with an array of unique elements, but they are compared by the distances to the target, which are _not unique_. That's why we choose 
_Hoare's Partition_ here. 

> Hoare's partition is more efficient than Lomuto's partition 
because it does three times fewer swaps on average, 
and creates efficient partitions even when all values are equal.

Here is how it works:

- Move pivot at the end of the array using swap. 

- Set the pointer at the beginning of the array `store_index = left`.
    
- Iterate over the array and move all more close elements 
to the left
`swap(store_index, i)`. Move `store_index` one step to the right after each swap.

- Move the pivot to its final place, and return this index.

**Implementation**

<iframe src="https://leetcode.com/playground/V6BJggQ8/shared" frameBorder="0" width="100%" height="500" name="V6BJggQ8"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$,
    $$\mathcal{O}(N^2)$$ in the worst case. 
    [Please refer to this card for the good detailed explanation of Master Theorem](https://leetcode.com/explore/learn/card/recursion-ii/470/divide-and-conquer/2871/).
    Master Theorem helps to get an average complexity by writing the algorithm cost
    as $$T(N) = a T(N / b) + f(N)$$. 
    Here we have an example of Master Theorem case III: 
    $$T(N) = T \left(\frac{N}{2}\right) + N$$, 
    that results in $$\mathcal{O}(N)$$ time complexity.
    That's the case of random pivots.
    
    In the worst-case of constantly bad chosen pivots, the problem is 
    not divided by half at each step, it becomes just one element less,
    that leads to $$\mathcal{O}(N^2)$$ time complexity. 
    It happens, for example, if at each step you choose the pivot not 
    randomly, but take the rightmost element. 
    For the random pivot choice, the probability of having such a 
    worst-case is negligibly small.  

* Space complexity: $$\mathcal{O}(N)$$ to store `nums`.
<br /> 
<br />


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### AC clean Java solution using two stacks
- Author: jeantimex
- Creation Date: Mon Aug 31 2015 05:21:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 08:24:15 GMT+0800 (Singapore Standard Time)

<p>
The idea is to compare the predecessors and successors of the closest node to the target, we can use two stacks to track the predecessors and successors, then like what we do in merge sort, we compare and pick the closest one to the target and put it to the result list.

As we know, inorder traversal gives us sorted predecessors, whereas reverse-inorder traversal gives us sorted successors.

We can use iterative inorder traversal rather than recursion, but to keep the code clean, here is the recursion version.

    public List<Integer> closestKValues(TreeNode root, double target, int k) {
      List<Integer> res = new ArrayList<>();

      Stack<Integer> s1 = new Stack<>(); // predecessors
      Stack<Integer> s2 = new Stack<>(); // successors

      inorder(root, target, false, s1);
      inorder(root, target, true, s2);
      
      while (k-- > 0) {
        if (s1.isEmpty())
          res.add(s2.pop());
        else if (s2.isEmpty())
          res.add(s1.pop());
        else if (Math.abs(s1.peek() - target) < Math.abs(s2.peek() - target))
          res.add(s1.pop());
        else
          res.add(s2.pop());
      }
      
      return res;
    }
    
    // inorder traversal
    void inorder(TreeNode root, double target, boolean reverse, Stack<Integer> stack) {
      if (root == null) return;

      inorder(reverse ? root.right : root.left, target, reverse, stack);
      // early terminate, no need to traverse the whole tree
      if ((reverse && root.val <= target) || (!reverse && root.val > target)) return;
      // track the value of current node
      stack.push(root.val);
      inorder(reverse ? root.left : root.right, target, reverse, stack);
    }
</p>


### O(logN) Java Solution with two stacks following hint
- Author: qianzhige
- Creation Date: Wed Sep 02 2015 09:54:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 05:51:57 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public List<Integer> closestKValues(TreeNode root, double target, int k) {
            List<Integer> ret = new LinkedList<>();
            Stack<TreeNode> succ = new Stack<>();
            Stack<TreeNode> pred = new Stack<>();
            initializePredecessorStack(root, target, pred);
            initializeSuccessorStack(root, target, succ);
            if(!succ.isEmpty() && !pred.isEmpty() && succ.peek().val == pred.peek().val) {
                getNextPredecessor(pred);
            }
            while(k-- > 0) {
                if(succ.isEmpty()) {
                    ret.add(getNextPredecessor(pred));
                } else if(pred.isEmpty()) {
                    ret.add(getNextSuccessor(succ));
                } else {
                    double succ_diff = Math.abs((double)succ.peek().val - target);
                    double pred_diff = Math.abs((double)pred.peek().val - target);
                    if(succ_diff < pred_diff) {
                        ret.add(getNextSuccessor(succ));
                    } else {
                        ret.add(getNextPredecessor(pred));
                    }
                }
            }
            return ret;
        }
    
        private void initializeSuccessorStack(TreeNode root, double target, Stack<TreeNode> succ) {
            while(root != null) {
                if(root.val == target) {
                    succ.push(root);
                    break;
                } else if(root.val > target) {
                    succ.push(root);
                    root = root.left;
                } else {
                    root = root.right;
                }
            }
        }
    
        private void initializePredecessorStack(TreeNode root, double target, Stack<TreeNode> pred) {
            while(root != null){
                if(root.val == target){
                    pred.push(root);
                    break;
                } else if(root.val < target){
                    pred.push(root);
                    root = root.right;
                } else{
                    root = root.left;
                }
            }
        }
        
        private int getNextSuccessor(Stack<TreeNode> succ) {
            TreeNode curr = succ.pop();
            int ret = curr.val;
            curr = curr.right;
            while(curr != null) {
                succ.push(curr);
                curr = curr.left;
            }
            return ret;
        }
    
        private int getNextPredecessor(Stack<TreeNode> pred) {
            TreeNode curr = pred.pop();
            int ret = curr.val;
            curr = curr.left;
            while(curr != null) {
                pred.push(curr);
                curr = curr.right;
            }
            return ret;
        }
    }
</p>


### Java 5ms iterative, following hint, O(klogn) time and space
- Author: yanggao
- Creation Date: Fri Nov 27 2015 12:58:07 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 05:16:13 GMT+0800 (Singapore Standard Time)

<p>
Following the hint, I use a predecessor stack and successor stack. I do a logn traversal to initialize them until I reach the null node. Then I use the getPredecessor and getSuccessor method to pop k closest nodes and update the stacks.

Time complexity is O(klogn), since k BST traversals are needed and each is bounded by O(logn) time. Note that it is not O(logn + k) which is the time complexity for k closest numbers in a linear array.

Space complexity is O(klogn), since each traversal brings  O(logn) new nodes to the stack.



    public class Solution {
        public List<Integer> closestKValues(TreeNode root, double target, int k) {
            List<Integer> result = new LinkedList<Integer>();
            // populate the predecessor and successor stacks 
            Stack<TreeNode> pred = new Stack<TreeNode>();
            Stack<TreeNode> succ = new Stack<TreeNode>();
            TreeNode curr = root;
            while (curr != null) {
                if (target <= curr.val) {
                    succ.push(curr);
                    curr = curr.left;
                } else {
                    pred.push(curr);
                    curr = curr.right;
                }
            }
            while (k > 0) {
                if (pred.empty() && succ.empty()) {
                    break; 
                } else if (pred.empty()) {
                    result.add( getSuccessor(succ) );
                } else if (succ.empty()) {
                    result.add( getPredecessor(pred) );
                } else if (Math.abs(target - pred.peek().val) < Math.abs(target - succ.peek().val)) {
                    result.add( getPredecessor(pred) );                    
                } else {
                    result.add( getSuccessor(succ) );
                }
                k--;
            }
            return result;
         }
    
        private int getPredecessor(Stack<TreeNode> st) {
            TreeNode popped = st.pop();
            TreeNode p = popped.left;
            while (p != null) {
                st.push(p);
                p = p.right;
            }
            return popped.val;
        }
    
        private int getSuccessor(Stack<TreeNode> st) {
            TreeNode popped = st.pop();
            TreeNode p = popped.right;
            while (p != null) {
                st.push(p);
                p = p.left;
            }
            return popped.val;
        }
    }
</p>


