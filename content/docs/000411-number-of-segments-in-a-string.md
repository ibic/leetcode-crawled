---
title: "Number of Segments in a String"
weight: 411
#id: "number-of-segments-in-a-string"
---
## Description
<div class="description">
<p>You are given a string <code>s</code>, return <em>the number of segments in the string</em>.&nbsp;</p>

<p>A <strong>segment</strong> is defined to be a contiguous sequence of <strong>non-space characters</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;Hello, my name is John&quot;
<strong>Output:</strong> 5
<strong>Explanation:</strong> The five segments are [&quot;Hello,&quot;, &quot;my&quot;, &quot;name&quot;, &quot;is&quot;, &quot;John&quot;]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;Hello&quot;
<strong>Output:</strong> 1
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;love live! mu&#39;sic forever&quot;
<strong>Output:</strong> 4
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;&quot;
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= s.length &lt;= 300</code></li>
	<li><code>s</code> consists of lower-case and upper-case English letters, digits or one of the following characters <code>&quot;!@#$%^&amp;*()_+-=&#39;,.:&quot;</code>.</li>
	<li>The only space character in <code>s</code> is <code>&#39; &#39;</code>.</li>
</ul>

</div>

## Tags
- String (string)

## Companies


## Official Solution
[TOC]

#### Approach #1 Using Language Builtins [Accepted]

**Intuition**

In a situation where raw efficiency is less important than code legibility,
it is likely better to use language-idiomatic builtin functions to solve this
problem.

**Algorithm**

There are a few corner cases that you can get snagged on in this problem, at
least in Java. First, one or more leading spaces will cause `split` to deduce
an erroneous `""` token at the beginning of the string, so we use the builtin
`trim` method to remove leading and trailing spaces. Then, if the resulting
string is the empty string, then we can simply output `0`. This is necessary due
to the following behavior of the `split` method:

```java
String[] tokens = "".split("\\s++");
tokens.length; // 1
tokens[0]; // ""
```

If we reach the final return statement, we `split` the trimmed string on
sequences of one or more whitespace characters (`split` can take a regular
expression) and return the length of the resulting array.

The Python solution is trivially short because Python's `split` has a lot of
default behavior that makes it perfect for this sort of problem. Notably, it
returns an empty list when `split`ting an empty string, it splits on
whitespace by default, and it implicitly `trim`s (`strip`s, in Python lingo)
the string beforehand.

<iframe src="https://leetcode.com/playground/Rz4k7nzM/shared" frameBorder="0" width="100%" height="208" name="Rz4k7nzM"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n)$$

    All builtin language functionality used here (in both the Java and Python
    examples) runs in either $$\mathcal{O}(n)$$ or $$\mathcal{O}(1)$$ time, so the entire algorithm
    runs in linear time.

* Space complexity : $$\mathcal{O}(n)$$

    `split` (in both languages) returns an array/list of $$\mathcal{O}(n)$$ length, so
    the algorithm uses linear additional space.

---

#### Approach #2 In-place [Accepted]

**Intuition**

If we cannot afford to allocate linear additional space, a fairly simple
algorithm can deduce the number of segments in linear time and constant
space.

**Algorithm**

To count the number of segments, it is equivalent to count the number of
string indices at which a segment begins. Therefore, by formally defining the
characteristics of such an index, we can simply iterate over the string and
test each index in turn. Such a definition is as follows: a string index
begins a segment if it is preceded by whitespace (or is the first index) and
is not whitespace itself, which can be checked in constant time. Finally, we
simply return the number of indices for which the condition is satisfied.

<iframe src="https://leetcode.com/playground/g7aUUfCy/shared" frameBorder="0" width="100%" height="276" name="g7aUUfCy"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n)$$

    We do a constant time check for each of the string's $$n$$ indices, so the
    runtime is overall linear.

* Space complexity : $$\mathcal{O}(1)$$

    There are only a few integers allocated, so the memory footprint is
    constant.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Clean java solution O(n)
- Author: asurana28
- Creation Date: Sun Dec 04 2016 12:47:21 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 09:30:47 GMT+0800 (Singapore Standard Time)

<p>
    public int countSegments(String s) {
        int res=0;
        for(int i=0; i<s.length(); i++)
            if(s.charAt(i)!=' ' && (i==0 || s.charAt(i-1)==' '))
                res++;        
        return res;
    }
	
	Time complexity:  O(n)
	Space complexity: O(1)
</p>


### O(n) sentinel value concise solution, C++
- Author: vsmnv
- Creation Date: Sun Dec 04 2016 12:44:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 04 2016 12:44:06 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    int countSegments(string s) {
        int res = 0;
        s.push_back(' ');
        for(int i = 1; i < s.size(); ++i)
          if(s[i] == ' ' && s[i-1] != ' ') ++res;
        return res;
    }
};
```
</p>


### one-liners
- Author: StefanPochmann
- Creation Date: Mon Dec 05 2016 06:11:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 13:26:27 GMT+0800 (Singapore Standard Time)

<p>
Ruby:
```
def count_segments(s)
  s.split.size
end
```
Python:

    def countSegments(self, s):
        return len(s.split())

Java:

    public int countSegments(String s) {
        return ("x " + s).split(" +").length - 1;
    }

C++:

    int countSegments(string s) {
        return regex_replace(regex_replace(s, regex("\\S+"), "x"), regex(" "), "").size();
    }
</p>


