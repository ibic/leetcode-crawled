---
title: "Path In Zigzag Labelled Binary Tree"
weight: 1082
#id: "path-in-zigzag-labelled-binary-tree"
---
## Description
<div class="description">
<p>In an infinite binary tree where every node has two children, the nodes are labelled in row order.</p>

<p>In the odd numbered rows (ie., the first, third, fifth,...), the labelling is left to right, while in the even numbered rows (second, fourth, sixth,...), the labelling is right to left.</p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/06/24/tree.png" style="width: 300px; height: 138px;" /></p>

<p>Given the <code>label</code> of a node in this tree, return the labels in the path from the root of the tree to the&nbsp;node with that <code>label</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> label = 14
<strong>Output:</strong> [1,3,4,14]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> label = 26
<strong>Output:</strong> [1,2,6,10,26]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= label &lt;= 10^6</code></li>
</ul>

</div>

## Tags
- Math (math)
- Tree (tree)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Bloomberg - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python O(logn) time, and space with readable code, and step by step explanation
- Author: laser
- Creation Date: Mon Jul 01 2019 04:08:37 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 27 2020 01:45:04 GMT+0800 (Singapore Standard Time)

<p>
Step by Step Explanation
With the thought in mind that in an ordered binary tree that goes from 1 to n:
```
Normally Ordered Binary Tree:
             1
           /   \
         2       3
       /  \     /  \
     4     5   6     7
   / |    /|   |\    | \
 8   9  10 11 12 13  14  15
```
Thought 1) You can easily determine the parent by dividing by 2 with a normally ordered (non-zigzag) binary tree
For example the parent of 9 can be calculated via int(9/2) which is 4

Thought 2) So we now how how to trace from the input `label` to the root node. So lets start with `label` In our example, we will use 14. To determine the parent of 14, notice that in the same spot in a normally ordered binary tree that it is 9. So you just need to calculate how to get from 14 to 9.
```
Zig Zag Binary Tree:
             1
           /   \
         3       2  <- 3+2-3 = 2/2 = 1
       /  \     /  \
     4     5   6     7   <- 7+4-4 = 7/2 = 3
   / |    /|   |\    | \
 15 14  13 12 11 10  9  8   <- 15+8-14 = 9/2 = 4
```
inversion formula: (max number of current level + min number of current level) - current number
For example to find the inversion of 14: 15 + 8 - 14 = 9
From here you just divide 9 by 2 to find the parent 4

Thought 3) You have to run the inversion formula at every level because at every level the row is inverted relative to the previous row

Time Complexity:
O(3 log n). 3 are needed as commented in the code.

Space Complexity:
If including the space required for the return `res` object counts as space then we need
O(log n) because we need to store the path from the root to the `label`.

```
class Solution:
    def pathInZigZagTree(self, label):
        res = [] # O(log n) space
        node_count = 1
        level = 1
        # Determine level of the label
        while label >= node_count*2: # O(log n) time
            node_count *= 2
            level += 1
        # Iterate from the target label to the root
        while label != 0: # O(log n) time
            res.append(label)
            level_max = 2**(level) - 1
            level_min = 2**(level-1)
            label = int((level_max + level_min - label)/2)
            level -= 1
        return res[::-1] # O(n) time
```
</p>


### C++ O(log n)
- Author: votrubac
- Creation Date: Sun Jun 30 2019 12:02:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 30 2019 14:30:24 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
If the tree is numbered left-to-right (not zigzag), the parent\'s label can be always determined as ```label / 2```. For zigzag, we need to "invert" the parent label.
# Solution
Determine the tree ```level``` where our value is located. The maximum label in the level is ```1 << level - 1```, and minimum is ```1 << (level - 1)```. We will use this fact to "invert" the parent label.
```
vector<int> pathInZigZagTree(int label, int level = 0) {
  while (1 << level <= label) ++level;
  vector<int> res(level);
  for(; label >= 1; label /= 2, --level) {
    res[level - 1] = label;
    label = (1 << level) - 1 - label + (1 << (level - 1));
  }
  return res;
}
```
# Complexity Analysis
Runtime: *O(log n)*
Memory: *O(1)* or *O(log n)* if we consider the memory required for the result.
</p>


### Simple solution in java (Using properties of complete binary tree) (O log N)
- Author: alok5
- Creation Date: Sun Jun 30 2019 12:05:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 01 2019 13:56:47 GMT+0800 (Singapore Standard Time)

<p>
1) Calculate current depth of the label
2) Calculate offset (for each depth, values lie from 2^d -> 2^(d+1) -1
3) Find the real parent based on offset
4) Repeat until we reach 1

e.g. parent of 14 is 4
1) depth = 3, values in this depth lie from 8 to 15 (since it is a complete binary tree)
2) offset = 15 - 14 = 1
3) real parent of 14 = parent of ( 8 + offset ) = parent (9) = 9/2 = 4

```
public List<Integer> pathInZigZagTree(int label) {
        LinkedList<Integer> result = new LinkedList<>();
        int parent = label;
        result.addFirst(parent);

        while(parent != 1) {
            int d = (int)(Math.log(parent)/Math.log(2));
            int offset = (int)Math.pow(2, d+1) - 1 - parent;
            parent = ((int)Math.pow(2, d) + offset) / 2;
            result.addFirst(parent);   
        }
        
        return result;
    }
```
</p>


