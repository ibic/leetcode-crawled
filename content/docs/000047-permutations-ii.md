---
title: "Permutations II"
weight: 47
#id: "permutations-ii"
---
## Description
<div class="description">
<p>Given a collection of numbers that might contain duplicates, return all possible unique permutations.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> [1,1,2]
<strong>Output:</strong>
[
  [1,1,2],
  [1,2,1],
  [2,1,1]
]
</pre>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Apple - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- LinkedIn - 10 (taggedByAdmin: true)
- VMware - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Overview

As the name of the problem suggests, this problem is an extension of the [Permutation](https://leetcode.com/problems/permutations/) problem.
The problem is different from the previous permutation problem on the condition that the input array can contain **_duplicates_**.

The key to solve the problem is still the **_backtracking_** algorithm.
However, we need some adaptation to ensure that the _enumerated_ solutions generated from our backtracking exploration do not have any duplicates.

>As a reminder, **[backtracking](https://leetcode.com/explore/learn/card/recursion-ii/472/backtracking/)** is a general algorithm for finding all (or some) solutions to some problems with constraints.
It incrementally builds candidates to the solutions, and abandons a candidate as soon as it determines that the candidate cannot possibly lead to a solution.

In this article, we will present a yet another backtracking solution to solve the problem.

---
#### Approach 1: Backtracking with Groups of Numbers

**Intuition**

First of all, let us review the general idea of permutation with an example.

Given the input array `[1, 1, 2]`, to generate a permutation of the array, we could follow the _Depth-First Search_ (DFS) approach, or more precisely the backtracking technique as one will see later.

>The idea is that we pick the numbers one by one. For a permutation of length $$N$$, we would then need $$N$$ stages to generate a valid permutation.
At each stage, we need to pick one number into the permutation, out of the remaining available numbers.
Later at the same stage, we will try out all available choices.
By trying out, we progressively build up candidates to the solution, and revert each choice with another alternative until there is no more choice.

Let us walk through the example with paper and pencil, as follows:

- Given the input of `[1, 1, 2]`, at the first stage, we have 2 choices to pick a number as the first number in the final permutation, _i.e._ `1` and `2`.
Suppose that we pick the number `1`, now the remaining numbers would become `[1, 2]`.
**Note:** The reason that we have only 2 choices instead of 3, is that there is a duplicate in the given input.
Picking any of the duplicate numbers as the first number of the permutation would lead us to the same permutation at the end.
Should the numbers in the array be all unique, we would then have the same number of choices as the length of the array. 

- At the second stage, we now then have again 2 choices, _i.e._ `[1, 2]`. 
Let us pick again the number `1`, which leaves us the only remaining number `2`.

- Now at the third stage, we have only one candidate number left, _i.e._ `[2]`. We then pick the last remaining number, which leads to a final permutation sequence of `[1, 1, 2]`.

- Moreover, we need to **_revisit_** each of the above stages, and make a different choice in order to try out all possibilities.
The reversion of the choices is what we call __*backtracking*__.

We illustrate all potential exploration in the following graph where each node represents a choice at a specific stage:

![permutation tree](../Figures/47/47_permutations.png)

>A key insight to avoid generating any **_redundant_** permutation is that at each step rather than viewing each number as a candidate, we consider each **_unique_** number as the true candidate.
For instance, at the very beginning, given in the input of `[1, 1, 2]`, we have only two true candidates instead of three.


**Algorithm**

Given the above insight, in order to find out all the unique numbers at each stage, we can build a **_hash table_** (denoted as `counter`), with each unique number as the key and its occurrence as the corresponding value.

To implement the algorithm, first we define a function called `backtrack(comb, counter)` which generates all permutations, starting from the current combination (`comb`) and the remaining numbers (`counter`).

Once the function is implemented, it suffices to invoke the function with the initial empty combination and the hash table we built out of the input array, to solve the problem.

Here are some sample implementations.

<iframe src="https://leetcode.com/playground/KghC3tCD/shared" frameBorder="0" width="100%" height="500" name="KghC3tCD"></iframe>

**Note:** for a backtracking algorithm, usually there are some explorations that would lead to a *dead end*, and we have to abandon those explorations in the middle.

However, due to the specificity of this problem and our exploration strategy, each exploration will result in a valid permutation, _i.e._ none of the efforts is in vain.
This insight would prove to be useful in the following complexity analysis.

**Complexity Analysis**

Let $$N$$ be the length of the input array.
Hence, the number of permutations would be at maximum $$N!$$, _i.e._ $$N \cdot (N-1) \cdot (N-2) ... 1$$, when each number in the array is unique.

- Time Complexity: $$\mathcal{O}\big(\sum_{k = 1}^{N}{P(N, k)}\big)$$ where $$P(N, k) = \frac{N!}{(N - k)!} = N (N - 1) ... (N - k + 1)$$
is so-called [_k-permutations_of_N_ or _partial permutation_](https://en.wikipedia.org/wiki/Permutation#k-permutations_of_n). 

    - As one can see in the exploration graph we have shown earlier, the execution of the backtracking algorithm will unfold itself as a tree, where each node is an invocation of the recursive function `backtrack(comb, counter)`.
    The total number of steps to complete the exploration is _exactly_ the number of nodes in the tree.
    Therefore, the time complexity of the algorithm is linked directly with the size of the tree.

    - It now boils down to estimating the number of nodes in the tree.
    As we know now, each level of the tree corresponds to a specific _stage_ of the exploration.
    At each stage, the number of candidates to explore is **bounded**.
    For instance, at the first stage, _at most_ we would have $$N$$ candidates to explore, _i.e._ the number of nodes at this level would be $$N$$.
    Moving on to the next stage, for each of the nodes in the first stage, we would have $$N-1$$ child nodes. Therefore, the number of nodes at this stage would be $$N \cdot (N-1)$$.
    So on and so forwards.

    ![number of nodes](../Figures/47/47_number_of_nodes.png)

    - By summing up all the nodes across the stages, we would then obtain the total number of nodes as $$\sum_{k = 1}^{N}{P(N, k)}$$ where $$P(N, k) = \frac{N!}{(N - k)!} = N (N - 1) ... (N - k + 1)$$.
    As a result, the exact time complexity of the algorithm is $$\mathcal{O}\big(\sum_{k = 1}^{N}{P(N, k)}\big)$$.

    - The above complexity might appear a bit too abstract to comprehend.
    Here we could provide another __*loose upper bound*__ on the complexity.

    - It takes $$N$$ steps to generate a single permutation. Since there are in total $$N!$$ possible permutations, at most it would take us $$N \cdot N!$$ steps to generate all permutations, simply assuming that there is no overlapping effort (which is not true).


- Space Complexity: $$\mathcal{O}(N)$$

    - First of all, we build a hash table out of the input numbers. In the worst case where each number is unique, we would need $$\mathcal{O}(N)$$ space for the table.

    - Since we applied recursion in the algorithm which consumes some extra space in the function call stack, we would need another $$\mathcal{O}(N)$$ space for the recursion.

    - During the exploration, we keep a candidate of permutation along the way, which takes yet another $$\mathcal{O}(N)$$.

    - To sum up, the total space complexity would be $$\mathcal{O}(N) + \mathcal{O}(N) + \mathcal{O}(N) = \mathcal{O}(N)$$.

    - **Note**, we did not take into account the space needed to hold the results. Otherwise, the space complexity would become $$\mathcal{O}(N \cdot N!)$$.


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Really easy Java solution, much easier than the solutions with very high vote
- Author: UpTheHell
- Creation Date: Thu Dec 10 2015 00:05:27 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 08:31:56 GMT+0800 (Singapore Standard Time)

<p>
Use an extra boolean array " boolean[] used"  to indicate whether the value is added to list. 

Sort the array "int[] nums" to make sure we can skip the same value.

when a number has the same value with its previous, we can use this number only if his previous is used
 

    public class Solution {
        public List<List<Integer>> permuteUnique(int[] nums) {
            List<List<Integer>> res = new ArrayList<List<Integer>>();
            if(nums==null || nums.length==0) return res;
            boolean[] used = new boolean[nums.length];
            List<Integer> list = new ArrayList<Integer>();
            Arrays.sort(nums);
            dfs(nums, used, list, res);
            return res;
        }
    
        public void dfs(int[] nums, boolean[] used, List<Integer> list, List<List<Integer>> res){
            if(list.size()==nums.length){
                res.add(new ArrayList<Integer>(list));
                return;
            }
            for(int i=0;i<nums.length;i++){
                if(used[i]) continue;
                if(i>0 &&nums[i-1]==nums[i] && !used[i-1]) continue;
                used[i]=true;
                list.add(nums[i]);
                dfs(nums,used,list,res);
                used[i]=false;
                list.remove(list.size()-1);
            }
        }
    }
</p>


### A simple C++ solution in only 20 lines
- Author: guoang
- Creation Date: Fri Feb 13 2015 17:31:52 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 02:50:48 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        void recursion(vector<int> num, int i, int j, vector<vector<int> > &res) {
            if (i == j-1) {
                res.push_back(num);
                return;
            }
            for (int k = i; k < j; k++) {
                if (i != k && num[i] == num[k]) continue;
                swap(num[i], num[k]);
                recursion(num, i+1, j, res);
            }
        }
        vector<vector<int> > permuteUnique(vector<int> &num) {
            sort(num.begin(), num.end());
            vector<vector<int> >res;
            recursion(num, 0, num.size(), res);
            return res;
        }
    };
</p>


### 9-line python solution with 1 line to handle duplication, beat 99% of others :-)
- Author: cbmbbz
- Creation Date: Sat Jan 02 2016 14:44:35 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 04:30:29 GMT+0800 (Singapore Standard Time)

<p>
Very similar to Permutation I, see explanations in https://leetcode.com/discuss/19510/my-ac-simple-iterative-java-python-solution. To handle duplication, just avoid inserting a number after any of its duplicates.

    def permuteUnique(self, nums):
        ans = [[]]
        for n in nums:
            new_ans = []
            for l in ans:
                for i in xrange(len(l)+1):
                    new_ans.append(l[:i]+[n]+l[i:])
                    if i<len(l) and l[i]==n: break              #handles duplication
            ans = new_ans
        return ans
		

Below is a backtracking solution:
```
    from collections import Counter
    def permuteUnique(self, nums):
        def btrack(path, counter):
            if len(path)==len(nums):
                ans.append(path[:])
            for x in counter:  # dont pick duplicates
                if counter[x] > 0:
                    path.append(x)
                    counter[x] -= 1
                    btrack(path, counter)
                    path.pop()
                    counter[x] += 1
        ans = []
        btrack([], Counter(nums))
        return ans
```
</p>


