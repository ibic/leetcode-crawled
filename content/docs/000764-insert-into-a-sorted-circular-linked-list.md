---
title: "Insert into a Sorted Circular Linked List"
weight: 764
#id: "insert-into-a-sorted-circular-linked-list"
---
## Description
<div class="description">
<p>Given a node from a <strong>Circular Linked List</strong> which is sorted in ascending order,&nbsp;write a function to insert a value&nbsp;<code>insertVal</code> into the list such that it remains a&nbsp;sorted circular list. The given node can be a reference to <em>any</em> single node in the list, and may not be necessarily the smallest value in the circular&nbsp;list.</p>

<p>If there are multiple suitable places for insertion, you may choose any place to insert the new value. After the insertion, the circular list should remain sorted.</p>

<p>If the list is empty (i.e., given node is <code>null</code>), you should create a new single circular list and return the reference to that single node. Otherwise, you should return the original given node.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/01/19/example_1_before_65p.jpg" style="width: 250px; height: 149px;" /><br />
&nbsp;
<pre>
<strong>Input:</strong> head = [3,4,1], insertVal = 2
<strong>Output:</strong> [3,4,1,2]
<strong>Explanation:</strong> In the figure above, there is a sorted circular list of three elements. You are given a reference to the node with value 3, and we need to insert 2 into the list. The new node should be inserted between node 1 and node 3. After the insertion, the list should look like this, and we should still return node 3.

<img alt="" src="https://assets.leetcode.com/uploads/2019/01/19/example_1_after_65p.jpg" style="width: 250px; height: 149px;" />

</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> head = [], insertVal = 1
<strong>Output:</strong> [1]
<strong>Explanation:</strong> The list is empty (given head is&nbsp;<code>null</code>). We create a new single circular list and return the reference to that single node.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> head = [1], insertVal = 0
<strong>Output:</strong> [1,0]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= Number of Nodes &lt;= 5 * 10^4</code></li>
	<li><code><font face="monospace">-10^6 &lt;= Node.val &lt;= 10^6</font></code></li>
	<li><code>-10^6 &lt;=&nbsp;insertVal &lt;= 10^6</code></li>
</ul>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Facebook - 10 (taggedByAdmin: false)
- Quip (Salesforce) - 9 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Two-Pointers Iteration

**Intuition**

As simple as the problem might seem to be, it is actually not trivial to write a solution that covers all cases. 

>Often the case for the problems with linked list, one could apply the approach of **Two-Pointers Iteration**, where one uses two pointers as surrogate to traverse the linked list.

One of reasons of having two pointers rather than one is that in singly-linked list one does not have a reference to the precedent node, therefore we keep an additional pointer which points to the precedent node.

>For this problem, we iterate through the cyclic list using two pointers, namely `prev` and `curr`. When we find a suitable place to insert the new value, we insert it between the `prev` and `curr` nodes. 

![pic](../Figures/708/708_two_pointers.png)


**Algorithm**

First of all, let us define the skeleton of two-pointers iteration algorithm as follows:

- As we mentioned in the intuition, we *loop over* the linked list with two pointers (_i.e._ `prev` and `curr`) step by step. The termination condition of the loop is that we get back to the starting point of the two pointers (_i.e._ `prev == head`) 

- During the loop, at each step, we check if the current place bounded by the two pointers is the right place to insert the new value.

- If not, we move both pointers one step forwards.

Now, the tricky part of this problem is to sort out different cases that our algorithm should deal with within the loop, and then design a *concise* logic to handle them sound and properly. Here we break it down into _three_ general cases.

>**Case 1).** The value of new node sits between the minimal and maximal values of the current list. As a result, it should be inserted within the list.

![pic](../Figures/708/708_case_1.png)

As we can see from the above example, the new value (`6`) sits between the minimal and maximal values of the list (_i.e._ `1` and `9`). No matter where we start from (in this example we start from the node `{3}`), the new node would end up being inserted between the nodes `{5}` and `{7}`.

_The condition is to find the place that meets the constraint of `{prev.val <= insertVal <= curr.val}`._

>**Case 2).** The value of new node goes beyond the minimal and maximal values of the current list, either less than the minimal value or greater than the maximal value. In either case, the new node should be added right after the _tail_ node (_i.e._ the node with the maximal value of the list).

Here are the examples with the same input list as in the previous example.

![pic](../Figures/708/708_case_2_1.png)

![pic](../Figures/708/708_case_2_2.png)

Firstly, we should locate the position of the **tail** node, by finding a descending order between the adjacent, _i.e._ the condition of `{prev.val > curr.val}`, since the nodes are sorted in ascending order, the tail node would have the greatest value of all nodes.

Furthermore, we check if the new value goes beyond the values of tail and head nodes, which are pointed by the `prev` and `curr` pointers respectively.

The Case 2.1 corresponds to the condition where the value to be inserted is _greater than or equal to_ the one of tail node, _i.e._ `{insertVal >= prev.val}`.

The Case 2.2 corresponds to the condition where the value to be inserted is _less than or equal to_ the head node, _i.e._ `{insertVal <= curr.val}`. 

Once we locate the tail and head nodes, we basically **_extend_** the original list by inserting the value in between the tail and head nodes, _i.e._ in between the `prev` and `curr` pointers, the same operation as in the Case 1.

>Case 3). Finally, there is one case that does not fall into any of the above two cases. This is the case where the list contains uniform values.

Though not explicitly stated in the problem description, our sorted list can contain some duplicate values. And in the extreme case, the entire list has only one single unique value.

![pic](../Figures/708/708_case_3.png)

In this case, we would end up looping through the list and getting back to the starting point.

_The followup action is just to add the new node after any node in the list, regardless the value to be inserted._ Since we are back to the starting point, we might as well add the new node right after the starting point (our entrance node).

Note that, we cannot skip the iteration though, since we have to iterate through the list to determine if our list contains a single unique value.

>The above three cases cover the scenarios within and after our iteration loop. There is however one minor **corner** case we still need to deal with, where we have an **empty** list. This, we could easily handle before the loop.

<iframe src="https://leetcode.com/playground/KsikHD8H/shared" frameBorder="0" width="100%" height="500" name="KsikHD8H"></iframe>


**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the size of list. In the worst case, we would iterate through the entire list.

- Space Complexity: $$\mathcal{O}(1)$$. It is a constant space solution.
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### (Accepted) C++ Solution in O(n) with Explanation
- Author: bevis
- Creation Date: Wed Jun 06 2018 18:51:33 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 16:16:32 GMT+0800 (Singapore Standard Time)

<p>
```
Node* insert(Node* head, int insertVal) {
    if (!head) {
        head = new Node(insertVal, nullptr);
        head->next = head;
        return head;
    }

    Node* prev = head;
    Node* next = head->next;
    bool inserted = false;
    while (true) {
        // insert when:
        // 1. prev <= insertVal <= next
        // 2. insertVal < min (insert at the tail)
        // 3. insertVal > max (insert at the tail)
        if ((prev->val <= insertVal && insertVal <= next->val) ||
            (prev->val > next->val && insertVal < next->val) ||
            (prev->val > next->val && insertVal > prev->val)) {
            prev->next = new Node(insertVal, next);
            inserted = true;
            break;
        }

        prev = prev->next;
        next = next->next;
        if (prev == head) break;
    }
    
    if (!inserted) {
        // The only reason why `value` was not inserted is that
        // all values in the list are the same and are not equal to `value`.
        // So, we could insert `value` anywhere.
        prev->next = new Node(insertVal, next);
    }

    return head;
}
```
</p>


### Java 5ms One Pass and Two Pass Traverse With Detailed Comments and Edge cases!
- Author: meganlee
- Creation Date: Sat Jul 14 2018 07:00:38 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 02:03:03 GMT+0800 (Singapore Standard Time)

<p>
`SOLUTION 1 ONE PASS`
Just need to be super clear about all the **edge cases**, especially when start is  `null`, `single node` or has `duplicates`, and need to be able to divide the problem up **clearly into categories**

**CASE 1**  if there is `tipping point` in the list, which means that there are at least 2 distinct values, we name the node that has the `max` value to be the `tipping point`, the node after `tipping point` has the `min` value (min != max)
   * *CASE 1A*:  if the to be inserted value `x` is in a climbing stage, which means there is a node satisfying `node.val <= x <= node.next.val`, we insert `x` after this node
   * *CASE 1B*:  if the to be inserted value `x` is the new `min`  or `max` value after its insertion,  x needs to be inserted after the `tipping point`

**CASE 2**  if there is `NO tipping point` in the list, which means that `all nodes in the list have the same value`
  * we just insert x before we traverse back to `start` node

```
// Test case 1:  insert(null, 1)
// Test case 2:  insert(1->null, 0)
// Test case 3:  insert(1->null, 1)
// Test case 4:  insert(1->null, 2)
// Test case 5:  insert(1->1->1->null, 0)
// Test case 6:  insert(1->1->1->null, 1)
// Test case 7:  insert(1->1->1->null, 2)
// Test case 8:  insert(1->1->3->3->null, 0)
// Test case 9:  insert(1->1->3->3->null, 1)
// Test case 10: insert(1->1->3->3->null, 2)
// Test case 11: insert(1->1->3->3->null, 3)

class Solution {
    public Node insert(Node start, int x) {
        // if start is null, create a node pointing to itself and return
        if (start == null) {
            Node node = new Node(x, null);
            node.next = node;
            return node;
        }
        // is start is NOT null, try to insert it into correct position
        Node cur = start;
        while (true) {
            // case 1A: has a tipping point, still climbing
            if (cur.val < cur.next.val) { 
                if (cur.val <= x && x <= cur.next.val) { // x in between cur and next
                    insertAfter(cur, x);
                    break;
                }
            // case 1B: has a tipping point, about to return back to min node
            } else if (cur.val > cur.next.val) { 
                if (cur.val <= x || x <= cur.next.val) { // cur is the tipping point, x is max or min val
                    insertAfter(cur, x);
                    break;
                }
            // case 2: NO tipping point, all flat
            } else {
                if (cur.next == start) {  // insert x before we traverse all nodes back to start
                    insertAfter(cur, x);
                    break;
                }
            }
            // None of the above three cases met, go to next node
            cur = cur.next;
        }
        return start;
    }
    
    // insert value x after Node cur
    private void insertAfter(Node cur, int x) {
        cur.next = new Node(x, cur.next);
    }
}
```

`SOLUTION 2 TWO PASS`
We could also use 2 pass traverse where in the 1st pass try to find `max` the `tipping point` node, then break the cycle into ordinary list, then insert x, then reconnect newTail and head to be a cycle.
I think the following solution is correct. but the judging system go against with this one case` insert(1->3->5->{loopback} , 1).`
This solution returns `1->3->5->1->{loopback}` vs what \'s expected by the judging system `1->1->3->5->{loopback}`, since it\'s already mentioned in the problems
`If there are multiple suitable places for insertion, you may choose any place to insert the new value. After the insertion, the cyclic list should remain sorted.` I think  `1->3->5->1->{loopback}`  could also be considered correct. Let me know if you have any insight into this.
```
class Solution {
    public Node insert(Node start, int x) {
        // if start is null, create a node pointing to itself and return
        if (start == null) {
            Node node = new Node(x, null);
            node.next = node;
            return node;
        }
        // if start is not null, try to insert it into correct position
        // 1st pass to find max node
        Node cur = start;
        while (cur.val <= cur.next.val && cur.next != start) 
            cur = cur.next;
        // 2nd pass to insert the node in to correct position
        Node max = cur;
        Node dummy = new Node(0, max.next); // use a dummy head to make insertion process simpler
        max.next = null; // break the cycle
        cur = dummy;
        while (cur.next != null && cur.next.val < x) {
            cur = cur.next;
        }
        cur.next = new Node(x, cur.next); // insert
        Node newMax = max.next == null ? max : max.next; // reconnect to cycle
        newMax.next = dummy.next;
        return start;
    }
}
```
</p>


### My clean easy understand python3 solution
- Author: RayRayRay
- Creation Date: Thu Aug 16 2018 23:40:16 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 17 2018 03:35:59 GMT+0800 (Singapore Standard Time)

<p>
Find insert position, only 2 cases
1st: prev.val <= x<=cur.val
2nd: x is the max value or min value in the new list, here when prev.val > cur.val, then prev is max, cur is min in the origin list.    1-2-3-4-5-1, prev is 5, cur is 1, 
```
class Solution:
    def insert(self, head, val):
        """
        :type head: Node
        :type insertVal: int
        :rtype: Node
        """
        node = Node(val, head)
        if not head:
            return node 
        prev, cur = head, head.next
        while 1:
            if prev.val <= val <= cur.val:
                break
            elif prev.val > cur.val and (val<cur.val or val>prev.val):
                break
            prev, cur = prev.next, cur.next
            if prev == head:
                break 
        prev.next = node
        node.next = cur 
        return head
```
</p>


