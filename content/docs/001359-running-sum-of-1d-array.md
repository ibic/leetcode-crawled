---
title: "Running Sum of 1d Array"
weight: 1359
#id: "running-sum-of-1d-array"
---
## Description
<div class="description">
<p>Given an array <code>nums</code>. We define a running sum of an array as&nbsp;<code>runningSum[i] = sum(nums[0]&hellip;nums[i])</code>.</p>

<p>Return the running sum of <code>nums</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4]
<strong>Output:</strong> [1,3,6,10]
<strong>Explanation:</strong> Running sum is obtained as follows: [1, 1+2, 1+2+3, 1+2+3+4].</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,1,1,1,1]
<strong>Output:</strong> [1,2,3,4,5]
<strong>Explanation:</strong> Running sum is obtained as follows: [1, 1+1, 1+1+1, 1+1+1+1, 1+1+1+1+1].</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,1,2,10,1]
<strong>Output:</strong> [3,4,6,16,17]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 1000</code></li>
	<li><code>-10^6&nbsp;&lt;= nums[i] &lt;=&nbsp;10^6</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Bloomberg - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Python Partial Sum
- Author: votrubac
- Creation Date: Sun Jun 14 2020 12:02:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 19 2020 07:02:03 GMT+0800 (Singapore Standard Time)

<p>
**C++**
```cpp
vector<int> runningSum(vector<int>& nums) {
    partial_sum(begin(nums), end(nums), begin(nums));
    return nums;
}
```

**Python**
```python
class Solution:
    def runningSum(self, nums: List[int]) -> List[int]:
        return accumulate(nums)   
```
</p>


### [Java/C++/Python] Array, Time O(N) Space O(1)
- Author: yedige
- Creation Date: Sun Jun 14 2020 12:01:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 14 2020 12:18:40 GMT+0800 (Singapore Standard Time)

<p>
**Explanation**
Loop once, we can get the sum of subarray starting from the initial point.

**Complexity**

Time ```O(N)```
Space ```O(1)```

**Java:**
```
public int[] runningSum(int[] nums) {
        int i = 1;
        while (i<nums.length){
            nums[i]+=nums[i-1];
            i++;
        }
        return nums;
    }
```
**C++**
```
vector<int> runningSum(vector<int>& nums) {
        int i = 1;
        while (i<nums.size()){
            nums[i]+=nums[i-1];
            i++;
        }
        return nums;
    }
```
**Python**
```
 def runningSum(self, nums):
        i = 1
        while i<len(nums):
            nums[i]+=nums[i-1]
            i+=1
        return nums
```

</p>


### JavaScript => reduce
- Author: trevH
- Creation Date: Fri Jun 19 2020 07:16:03 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 19 2020 08:35:09 GMT+0800 (Singapore Standard Time)

<p>
```
const runningSum = nums => {
    nums.reduce((a,c,i,arr) => arr[i] += a)
    return nums
}
```
**Remember that assignment is an expression and therefore returns a value.
Specifically, the new value of the variable being assigned something.**
</p>


