---
title: "Largest Multiple of Three"
weight: 1142
#id: "largest-multiple-of-three"
---
## Description
<div class="description">
<p>Given an integer array of <code>digits</code>,&nbsp;return the largest multiple of <strong>three</strong> that can be formed by concatenating some of the given digits in any order.</p>

<p>Since the answer may not fit in an integer data type, return the answer as a string.</p>

<p>If there is no answer return an empty string.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> digits = [8,1,9]
<strong>Output:</strong> &quot;981&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> digits = [8,6,7,1,0]
<strong>Output:</strong> &quot;8760&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> digits = [1]
<strong>Output:</strong> &quot;&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> digits = [0,0,0,0,0,0]
<strong>Output:</strong> &quot;0&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= digits.length &lt;= 10^4</code></li>
	<li><code>0 &lt;= digits[i] &lt;= 9</code></li>
	<li>The returning answer must not contain unnecessary leading zeros.</li>
</ul>

</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Basic Math
- Author: lee215
- Creation Date: Sun Feb 23 2020 12:19:05 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 18 2020 14:30:33 GMT+0800 (Singapore Standard Time)

<p>
# **Basic Math**
 999....999 % 3 == 0
1000....000 % 3 == 1
a000....000 % 3 == a % 3
abcdefghijk % 3 == (a+b+c+..+i+j+k) % 3
<br>

# **Explanation**
0. Calculate the sum of digits `total = sum(A)`
1. If `total % 3 == 0`, we got it directly
2. If `total % 3 == 1` and we have one of 1,4,7 in A:
    we try to remove one digit of 1,4,7
3. If `total % 3 == 2` and we have one of 2,5,8 in A:
    we try to remove one digit of 2,5,8
4. If `total % 3 == 2`:
    we try to remove two digits of 1,4,7
5. If `total % 3 == 1`:
    we try to remove two digits of 2,5,8
6. Submit
<br>

# **Complexity**
Time `O(nlogn)`, where I use quick sort.
We can also apply counting sort, so it will be `O(n)`
Space `O(sort)`
<br>

# Solution 1
**Python:**
```py
    def largestMultipleOfThree(self, A):
        total = sum(A)
        count = collections.Counter(A)
        A.sort(reverse=1)

        def f(i):
            if count[i]:
                A.remove(i)
                count[i] -= 1
            if not A: return \'\'
            if not any(A): return \'0\'
            if sum(A) % 3 == 0: return \'\'.join(map(str, A))

        if total % 3 == 0:
            return f(-1)
        if total % 3 == 1 and count[1] + count[4] + count[7]:
            return f(1) or f(4) or f(7)
        if total % 3 == 2 and count[2] + count[5] + count[8]:
            return f(2) or f(5) or f(8)
        if total % 3 == 2:
            return f(1) or f(1) or f(4) or f(4) or f(7) or f(7)
        return f(2) or f(2) or f(5) or f(5) or f(8) or f(8)
```
<br>

# Solution 2: DP
inspired by @Aimar88
```py
    def largestMultipleOfThree(self, digits):
        dp = [-1,-1,-1]
        for a in sorted(digits)[::-1]:
            for x in dp[:] + [0]:
                y = x * 10 + a
                dp[y % 3] = max(dp[y % 3], y)
        return str(dp[0]) if dp[0] >= 0 else ""
```
</p>


### [C++/Java] Concise O(n)
- Author: votrubac
- Creation Date: Mon Feb 24 2020 09:27:28 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Feb 25 2020 00:30:35 GMT+0800 (Singapore Standard Time)

<p>
Obviously, trying combinations of numbers won\'t work as we can have up to 10,000 numbers. Luckily, there is a handy divisibility test:
> A number is divisible by 3 if the sum of all its digits is divisible by 3.

Observation 1: since the order does not matter, the largest number can be formed by adding digits from largest (`9`) to smallest (`0`), e.g. `9999966330000`.

Therefore, we can just count the occurrences of each digit, and then generate the string.

Observation 2:  we need to use all digits to form the maximum number. If we sum all digits, and the modulo of 3 is not zero, we need to remove 1 (preferably) or 2 smallest digits.  If modulo 3 of the sum is `1`, for example, we will try to remove `1`, `4`, or `7`, if exists, or two of `2`, `5`, or `8`.

More examples: 
- `9965341 % 3 == 1`; we remove `1` to get the largest number. 
- `9952000 % 3 == 1`; now we need to remove two digits, `2` and `5`, as there is no `1`, `4`, or `7`.

These observations yield the following algorithm.

**C++**
```cpp
string largestMultipleOfThree(vector<int>& digits, string res = "") {
  int m1[] = {1, 4, 7, 2, 5, 8}, m2[] = {2, 5, 8, 1, 4, 7};
  int sum = 0, ds[10] = {};  
  for (auto d : digits) {
      ++ds[d];
      sum += d;
  }
  while (sum % 3 != 0) {
    for (auto i : sum % 3 == 1 ? m1 : m2) {
      if (ds[i]) {
        --ds[i];
        sum -= i;
        break;
      }
    }
  }
  for (int i = 9; i >= 0; --i)
    res += string(ds[i], \'0\' + i);
  return res.size() && res[0] == \'0\' ? "0" : res;
}
```
**Java**
```java
public String largestMultipleOfThree(int[] digits) {
    int m1[] = new int[] {1, 4, 7, 2, 5, 8}, m2[] = new int[] {2, 5, 8, 1, 4, 7};
    int sum = 0, ds[] = new int[10];
    for (int d : digits) {
        ++ds[d];
        sum += d;
    }
    while (sum % 3 != 0) {
        for (int i : sum % 3 == 1 ? m1 : m2) {
          if (ds[i] > 0) {
            --ds[i];
            sum -= i;
            break;
          }
        }
      }
    StringBuilder sb = new StringBuilder();
    for (int i = 9; i >= 0; --i)
        sb.append(Character.toString(\'0\' + i).repeat(ds[i]));     
    return sb.length() > 0 && sb.charAt(0) == \'0\' ? "0" : sb.toString();
}
```
</p>


### [Java] Basic Multiple of 3 - Clean code - O(N) ~ 2ms
- Author: hiepit
- Creation Date: Sun Feb 23 2020 12:38:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 22:02:06 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
The number is multiple of 3 if and only if the sum of all digits is a multiple of 3
Our goal is to **find the maximum number of digits whose sum is a multiple of 3**.

Three cases arise:
- The sum of digits produces remainder 1 when divided by 3
Delete 1 smallest digit with the remainder = 1 or Delete 2 smallest digits with the remainder = 2 
=> Then return the maximum result
- The sum of digits produces remainder 2 when divided by 3
Delete 1 smallest digit with the remainder = 2 or Delete 2 smallest digits with the remainder = 1 
=> Then return the maximum result
- The sum of digits is divisible by 3: 
=> Return the maximum result

**Solution 1: Straight Forward ~ 9ms**
```java
class Solution {
    public String largestMultipleOfThree(int[] digits) {
        Arrays.sort(digits);
        List<Integer> remain1Indices = new ArrayList<>(2); // Indices of up to 2 elements with remainder = 1
        List<Integer> remain2Indices = new ArrayList<>(2); // Indices of up to 2 elements with remainder = 2
        for (int i = 0; i < digits.length; i++) {
            if (digits[i] % 3 == 1 && remain1Indices.size() < 2) remain1Indices.add(i);
            else if (digits[i] % 3 == 2 && remain2Indices.size() < 2) remain2Indices.add(i);
        }
        int remainSum = Arrays.stream(digits).sum() % 3;
        if (remainSum == 1) { // Delete 1 smallest digit with remainder = 1 or Delete 2 smallest digits the remainder = 2
            if (remain1Indices.size() >= 1) return getResult(digits, remain1Indices.get(0), -1);
            else return getResult(digits, remain2Indices.get(0), remain2Indices.get(1));
        } else if (remainSum == 2) { // Delete 1 smallest digit with remainder = 2 or Delete 2 smallest digits with remainder = 1
            if (remain2Indices.size() >= 1) return getResult(digits, remain2Indices.get(0), -1);
            else return getResult(digits, remain1Indices.get(0), remain1Indices.get(1));
        }
        return getResult(digits, -1, -1);
    }
    private String getResult(int[] digits, int ban1, int ban2) {
        StringBuilder sb = new StringBuilder();
        for (int i = digits.length - 1; i >= 0; i--) {
            if (i == ban1 || i == ban2) continue; // Skip banned digits
            sb.append(digits[i]);
        }
        if (sb.length() > 0 && sb.charAt(0) == \'0\') return "0"; // Remove leading 0 case [0,...,0]
        return sb.toString();
    }
}
```
Complexity:
- Time: `O(NlogN)` for Arrays.sort(digits)
- Space: `O(1)`

**Solution 2: Counting Sort Optimized ~ 2ms**
```java
class Solution {
    public String largestMultipleOfThree(int[] digits) {
        int[] cnt = new int[10];
        for (int d : digits) cnt[d]++;
        int remain1Cnt = cnt[1] + cnt[4] + cnt[7]; // Number of elements with remainder = 1
        int remain2Cnt = cnt[2] + cnt[5] + cnt[8]; // Number of elements with remainder = 2
        int remainSum = (remain1Cnt + 2 * remain2Cnt) % 3;
        if (remainSum == 1) { // Delete 1 smallest digit with remainder = 1 or Delete 2 smallest digits with remainder = 2
            if (remain1Cnt >= 1) remain1Cnt -= 1;
            else remain2Cnt -= 2;
        } else if (remainSum == 2) { // Delete 1 smallest digit with remainder = 2 or Delete 2 smallest digits with remainder = 1
            if (remain2Cnt >= 1) remain2Cnt -= 1;
            else remain1Cnt -= 2;
        }

        StringBuilder sb = new StringBuilder();
        for (int d = 9; d >= 0; d--) {
            if (d % 3 == 0) while (cnt[d]-- > 0) sb.append(d);
            else if (d % 3 == 1) while (cnt[d]-- > 0 && remain1Cnt-- > 0) sb.append(d);
            else while (cnt[d]-- > 0 && remain2Cnt-- > 0) sb.append(d);
        }
        if (sb.length() > 0 && sb.charAt(0) == \'0\') return "0"; // Remove leading 0 case [0,...,0]
        return sb.toString();
    }
}
```
Complexity:
- Time: `O(N)`
- Space: `O(1)`
</p>


