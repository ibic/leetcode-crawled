---
title: "Missing Ranges"
weight: 163
#id: "missing-ranges"
---
## Description
<div class="description">
<p>You are given an inclusive range <code>[lower, upper]</code> and a <strong>sorted unique</strong> integer array <code>nums</code>, where all elements are in the inclusive range.</p>

<p>A number <code>x</code> is considered <strong>missing</strong> if <code>x</code> is in the range <code>[lower, upper]</code> and <code>x</code> is not in <code>nums</code>.</p>

<p>Return <em>the <strong>smallest sorted</strong> list of ranges that <strong>cover every missing number exactly</strong></em>. That is, no element of <code>nums</code> is in any of the ranges, and each missing number is in one of the ranges.</p>

<p>Each range <code>[a,b]</code> in the list should be output as:</p>

<ul>
	<li><code>&quot;a-&gt;b&quot;</code> if <code>a != b</code></li>
	<li><code>&quot;a&quot;</code> if <code>a == b</code></li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,1,3,50,75], lower = 0, upper = 99
<strong>Output:</strong> [&quot;2&quot;,&quot;4-&gt;49&quot;,&quot;51-&gt;74&quot;,&quot;76-&gt;99&quot;]
<strong>Explanation:</strong> The ranges are:
[2,2] --&gt; &quot;2&quot;
[4,49] --&gt; &quot;4-&gt;49&quot;
[51,74] --&gt; &quot;51-&gt;74&quot;
[76,99] --&gt; &quot;76-&gt;99&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [], lower = 1, upper = 1
<strong>Output:</strong> [&quot;1&quot;]
<strong>Explanation:</strong> The only missing range is [1,1], which becomes &quot;1&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [], lower = -3, upper = -1
<strong>Output:</strong> [&quot;-3-&gt;-1&quot;]
<strong>Explanation:</strong> The only missing range is [-3,-1], which becomes &quot;-3-&gt;-1&quot;.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [-1], lower = -1, upper = -1
<strong>Output:</strong> []
<strong>Explanation:</strong> There are no missing ranges since there are no missing numbers.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> nums = [-1], lower = -2, upper = -1
<strong>Output:</strong> [&quot;-2&quot;]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>-10<sup>9</sup> &lt;= lower &lt;= upper &lt;= 10<sup>9</sup></code></li>
	<li><code>0 &lt;= nums.length &lt;= 100</code></li>
	<li><code>lower &lt;= nums[i] &lt;= upper</code></li>
	<li>All the values of <code>nums</code> are <strong>unique</strong>.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Facebook - 8 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Linear Scan
  

**Intuition and Algorithm**

Since the input array, `nums`, is sorted ascendingly and all the elements in it are within the given ```[lower, upper]``` bounds, we can simply check consecutive elements to see if they differ by one or not. If they don't, then we have found a missing range.

- When `nums[i] - nums[i-1] == 1`, we know that there are no missing elements between `nums[i-1]` and `nums[i]`.
- When `nums[i] - nums[i-1] > 1`, we know that `[nums[i-1] + 1, nums[i] - 1]` range of elements are missing.      

![missing ranges](../Figures/163/1.png)

However, there are two edge cases:

- Case 1: If we don't start with `lower` as the first element of the array, we will need to consider `[lower, num[0] - 1]` missing range as well.

![missing ranges](../Figures/163/2.png)

- Case 2: Similarly, if we don't end with `upper` as the last element of the array, we will need to consider `[nums[n-1] + 1, upper]` missing range as well.  Note `n` here is the length of the input array `nums`.

<iframe src="https://leetcode.com/playground/G243qYva/shared" frameBorder="0" width="100%" height="500" name="G243qYva"></iframe>


**Complexity Analysis**

* Time complexity : $$O(N)$$, where $$N$$ is the length of the input array. This is because we are only iterating over the array once.

* Space complexity : $$O(N)$$, where $$N$$ is the length of the input array. This is because we could have a missing range between each of the consecutive element of the input array. Hence, our output list that we need to return will be of size $$N$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted Java solution with explanation
- Author: jeantimex
- Creation Date: Mon Jul 13 2015 13:37:35 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 13:15:09 GMT+0800 (Singapore Standard Time)

<p>
    public List<String> findMissingRanges(int[] a, int lo, int hi) {
      List<String> res = new ArrayList<String>();
      
      // the next number we need to find
      int next = lo;
      
      for (int i = 0; i < a.length; i++) {
        // not within the range yet
        if (a[i] < next) continue;
        
        // continue to find the next one
        if (a[i] == next) {
          next++;
          continue;
        }
        
        // get the missing range string format
        res.add(getRange(next, a[i] - 1));
        
        // now we need to find the next number
        next = a[i] + 1;
      }
      
      // do a final check
      if (next <= hi) res.add(getRange(next, hi));
    
      return res;
    }
    
    String getRange(int n1, int n2) {
      return (n1 == n2) ? String.valueOf(n1) : String.format("%d->%d", n1, n2);
    }
</p>


### Ten line python solution
- Author: jccg1000021953
- Creation Date: Tue Mar 24 2015 12:21:14 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 09:10:42 GMT+0800 (Singapore Standard Time)

<p>
      def findMissingRanges(self, A, lower, upper):
            result = []
            A.append(upper+1)
            pre = lower - 1
            for i in A:
               if (i == pre + 2):
                   result.append(str(i-1))
               elif (i > pre + 2):
                   result.append(str(pre + 1) + "->" + str(i -1))
               pre = i
            return result
</p>


### Accepted Java solution 8 lines & 0ms
- Author: mcopes
- Creation Date: Fri Jan 13 2017 08:41:33 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jan 13 2017 08:41:33 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public List<String> findMissingRanges(int[] nums, int lower, int upper) {
        List<String> res = new ArrayList<>();
        for(int i : nums) {
            if(i > lower) res.add(lower+((i-1 > lower)?"->"+(i-1):""));
            if(i == upper) return res; // Avoid overflow
            lower = i+1;
        }
        if(lower <= upper) res.add(lower + ((upper > lower)?"->"+(upper):""));
        return res;
    }
}
```
Just 8 lines but still very readable.
</p>


