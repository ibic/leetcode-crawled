---
title: "Next Closest Time"
weight: 613
#id: "next-closest-time"
---
## Description
<div class="description">
<p>Given a time represented in the format "HH:MM", form the next closest time by reusing the current digits. There is no limit on how many times a digit can be reused.</p>

<p>You may assume the given input string is always valid. For example, "01:34", "12:09" are all valid. "1:34", "12:9" are all invalid.</p>

<p><b>Example 1:</b>
<pre>
<b>Input:</b> "19:34"
<b>Output:</b> "19:39"
<b>Explanation:</b> The next closest time choosing from digits <b>1</b>, <b>9</b>, <b>3</b>, <b>4</b>, is <b>19:39</b>, which occurs 5 minutes later.  It is not <b>19:33</b>, because this occurs 23 hours and 59 minutes later.
</pre>
</p>

<p><b>Example 2:</b>
<pre>
<b>Input:</b> "23:59"
<b>Output:</b> "22:22"
<b>Explanation:</b> The next closest time choosing from digits <b>2</b>, <b>3</b>, <b>5</b>, <b>9</b>, is <b>22:22</b>. It may be assumed that the returned time is next day's time since it is smaller than the input time numerically.
</pre>
</p>
</div>

## Tags
- String (string)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Google - 6 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
#### Approach #1: Simulation [Accepted]

**Intuition and Algorithm**

Simulate the clock going forward by one minute.  Each time it moves forward, if all the digits are allowed, then return the current time.

The natural way to represent the time is as an integer `t` in the range `0 <= t < 24 * 60`.  Then the hours are `t / 60`, the minutes are `t % 60`, and each digit of the hours and minutes can be found by `hours / 10, hours % 10` etc.

<iframe src="https://leetcode.com/playground/vBGi23jY/shared" frameBorder="0" name="vBGi23jY" width="100%" height="377"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(1)$$.  We try up to $$24 * 60$$ possible times until we find the correct time.

* Space Complexity: $$O(1)$$.

---
#### Approach #2: Build From Allowed Digits [Accepted]

**Intuition and Algorithm**

We have up to 4 different allowed digits, which naively gives us `4 * 4 * 4 * 4` possible times.  For each possible time, let's check that it can be displayed on a clock: ie., `hours < 24 and mins < 60`.  The best possible `time != start` is the one with the smallest `cand_elapsed = (time - start) % (24 * 60)`, as this represents the time that has elapsed since `start`, and where the modulo operation is taken to be always non-negative.

For example, if we have `start = 720` (ie. noon), then times like `12:05 = 725` means that `(725 - 720) % (24 * 60) = 5` seconds have elapsed; while times like `00:10 = 10` means that `(10 - 720) % (24 * 60) = -710 % (24 * 60) = 730` seconds have elapsed.

Also, we should make sure to handle `cand_elapsed` carefully.  When our current candidate time `cur` is equal to the given starting time, then `cand_elapsed` will be `0` and we should handle this case appropriately.

<iframe src="https://leetcode.com/playground/Yr63hFuh/shared" frameBorder="0" name="Yr63hFuh" width="100%" height="479"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(1)$$.  We all $$4^4$$ possible times and take the best one.

* Space Complexity: $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java AC 5ms simple solution with detailed explaination
- Author: cwleoo
- Creation Date: Sun Sep 24 2017 12:06:52 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 12:48:14 GMT+0800 (Singapore Standard Time)

<p>
This approach here is trying to find next digit for each postion in "HH:MM" from right to left. If the next digit is greater than current digit, return directly and keep other digits unchanged.
Here is the steps: (e.g. `"17:38"`)
1. Retrieve all four digits from given string and sort them in asscending order, `"17:38"` -> `digits[] {'1', '3', '7', '8'}`
2. Apply `findNext()` from the right most digit to left most digit, try to find next greater digit from `digits[]` (if exist) which is suitable for current position, otherwise return the minimum digit (`digits[0]`):

    - `"HH:M_"`: there is no upperLimit for this position (0-9). Just pick the next different digit in the sequence. In the example above, `'8'` is already the greatest one, so we change it into the smallest one (`digits[0]` i.e. `'1'`) and move to next step: `"17:38" -> "17:31"`

    - `"HH:_M"`: the upperLimit is `'5'` (00~59). The next different digit for `'3'` is `'7'`, which is greater than `'5'`, so we should omit it and try next. Similarly, `'8'` is beyond the limit, so we should choose next, i.e. `'1'`: `"17:38" -> "17:11"`
    - `"H_:MM"`: the upperLimit depends on `result[0]`. If `result[0] == '2'`, then it should be no more than `'3'`; else no upperLimit (0-9). Here we have `result[0]` = `'1'`, so we could choose any digit we want. The next digit for `'7'` is `'8'`, so we change it and return the result directly. `"17:38" -> "18:11"`
    - `"_H:MM"`: the upperLimit is `'2'` (00~23). e.g. `"03:33" -> "00:00"`
```
class Solution {
    
    public String nextClosestTime(String time) {
        char[] result = time.toCharArray();
        char[] digits = new char[] {result[0], result[1], result[3], result[4]};
        Arrays.sort(digits);
        
        // find next digit for HH:M_
        result[4] = findNext(result[4], (char)('9' + 1), digits);  // no upperLimit for this digit, i.e. 0-9
        if(result[4] > time.charAt(4)) return String.valueOf(result);  // e.g. 23:43 -> 23:44
        
        // find next digit for HH:_M
        result[3] = findNext(result[3], '5', digits);
        if(result[3] > time.charAt(3)) return String.valueOf(result);  // e.g. 14:29 -> 14:41
        
        // find next digit for H_:MM
        result[1] = result[0] == '2' ? findNext(result[1], '3', digits) : findNext(result[1], (char)('9' + 1), digits);
        if(result[1] > time.charAt(1)) return String.valueOf(result);  // e.g. 02:37 -> 03:00 
        
        // find next digit for _H:MM
        result[0] = findNext(result[0], '2', digits);
        return String.valueOf(result);  // e.g. 19:59 -> 11:11
    }
    
    /** 
     * find the next bigger digit which is no more than upperLimit. 
     * If no such digit exists in digits[], return the minimum one i.e. digits[0]
     * @param current the current digit
     * @param upperLimit the maximum possible value for current digit
     * @param digits[] the sorted digits array
     * @return 
     */
    private char findNext(char current, char upperLimit, char[] digits) {
        //System.out.println(current);
        if(current == upperLimit) {
            return digits[0];
        }
        int pos = Arrays.binarySearch(digits, current) + 1;
        while(pos < 4 && (digits[pos] > upperLimit || digits[pos] == current)) { // traverse one by one to find next greater digit
            pos++;
        }
        return pos == 4 ? digits[0] : digits[pos];
    }
    
}
```
</p>


### Python simple 20ms solution
- Author: thegoldenboy
- Creation Date: Thu Nov 08 2018 08:42:42 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Nov 08 2018 08:42:42 GMT+0800 (Singapore Standard Time)

<p>
While there are some very clever solutions in the discussion, this is a simple (and fast) solution that I'd expect someone to come up with in an interview.

The core idea here is time wraps around minute and hour values, both of which can only be two digit values.  We generate all the possible two digit values (there are at most 16) in order to solve for both.

The minute value of the solution will be: 
1. the smallest generated number higher than the input minute, if it is valid minute
OR
2. the smallest generated number, since we have to wrap around the next hour

Likewise, the hour value of the solution will be
1. the smallest generated number higher than the input hour, if it is a valid hour
OR
2. the smallest generated number, since we have to wrap around the next day


```
class Solution(object):
    def nextClosestTime(self, time):
        """
        :type time: str
        :rtype: str
        """
        hour, minute = time.split(":")
        
        # Generate all possible 2 digit values
        # There are at most 16 sorted values here
        nums = sorted(set(hour + minute))
        two_digit_values = [a+b for a in nums for b in nums]

        # Check if the next valid minute is within the hour
        i = two_digit_values.index(minute)
        if i + 1 < len(two_digit_values) and two_digit_values[i+1] < "60":
            return hour + ":" + two_digit_values[i+1]

        # Check if the next valid hour is within the day
        i = two_digit_values.index(hour)
        if i + 1 < len(two_digit_values) and two_digit_values[i+1] < "24":
            return two_digit_values[i+1] + ":" + two_digit_values[0]
        
        # Return the earliest time of the next day
        return two_digit_values[0] + ":" + two_digit_values[0]
```

Follow ups:
1. You could optimize the `.index()` calls by using binary search.  Though since there are at most 16 values, this isn't really necessary.
</p>


### Verbose Java solution, DFS
- Author: shawngao
- Creation Date: Sun Sep 24 2017 11:07:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 05:41:31 GMT+0800 (Singapore Standard Time)

<p>
Since there are at max ```4 * 4 * 4 * 4``` = ```256``` possible times, just try them all...
```
class Solution {
    int diff = Integer.MAX_VALUE;
    String result = "";
    
    public String nextClosestTime(String time) {
        Set<Integer> set = new HashSet<>();
        set.add(Integer.parseInt(time.substring(0, 1)));
        set.add(Integer.parseInt(time.substring(1, 2)));
        set.add(Integer.parseInt(time.substring(3, 4)));
        set.add(Integer.parseInt(time.substring(4, 5)));
        
        if (set.size() == 1) return time;
        
        List<Integer> digits = new ArrayList<>(set);
        int minute = Integer.parseInt(time.substring(0, 2)) * 60 + Integer.parseInt(time.substring(3, 5));

        dfs(digits, "", 0, minute);

        return result;
    }

    private void dfs(List<Integer> digits, String cur, int pos, int target) {
        if (pos == 4) {
            int m = Integer.parseInt(cur.substring(0, 2)) * 60 + Integer.parseInt(cur.substring(2, 4));
            if (m == target) return;
            int d = m - target > 0 ? m - target : 1440 + m - target;
            if (d < diff) {
                diff = d;
                result = cur.substring(0, 2) + ":" + cur.substring(2, 4);
            }
            return;
        }

        for (int i = 0; i < digits.size(); i++) {
            if (pos == 0 && digits.get(i) > 2) continue;
            if (pos == 1 && Integer.parseInt(cur) * 10 + digits.get(i) > 23) continue;
            if (pos == 2 && digits.get(i) > 5) continue;
            if (pos == 3 && Integer.parseInt(cur.substring(2)) * 10 + digits.get(i) > 59) continue;
            dfs(digits, cur + digits.get(i), pos + 1, target);
        }
    }
}
```
</p>


