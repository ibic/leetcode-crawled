---
title: "Longest Repeating Character Replacement"
weight: 407
#id: "longest-repeating-character-replacement"
---
## Description
<div class="description">
<p>Given a string <code>s</code>&nbsp;that consists of only uppercase English letters, you can perform at most <code>k</code> operations on that string.</p>

<p>In one operation, you can choose <strong>any</strong> character of the string and change it to any other uppercase English character.</p>

<p>Find the length of the longest sub-string containing all repeating letters you can get after performing the above operations.</p>

<p><b>Note:</b><br />
Both the string&#39;s length and <i>k</i> will not exceed 10<sup>4</sup>.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b>
s = &quot;ABAB&quot;, k = 2

<b>Output:</b>
4

<b>Explanation:</b>
Replace the two &#39;A&#39;s with two &#39;B&#39;s or vice versa.
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b>
s = &quot;AABABBA&quot;, k = 1

<b>Output:</b>
4

<b>Explanation:</b>
Replace the one &#39;A&#39; in the middle with &#39;B&#39; and form &quot;AABBBBA&quot;.
The substring &quot;BBBB&quot; has the longest repeating letters, which is 4.
</pre>

<p>&nbsp;</p>

</div>

## Tags
- Two Pointers (two-pointers)
- Sliding Window (sliding-window)

## Companies
- Google - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Pocket Gems - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 12 lines O(n) sliding window solution with explanation
- Author: Joshua924
- Creation Date: Mon Oct 17 2016 05:23:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 11:24:46 GMT+0800 (Singapore Standard Time)

<p>
```
    public int characterReplacement(String s, int k) {
        int len = s.length();
        int[] count = new int[26];
        int start = 0, maxCount = 0, maxLength = 0;
        for (int end = 0; end < len; end++) {
            maxCount = Math.max(maxCount, ++count[s.charAt(end) - 'A']);
            while (end - start + 1 - maxCount > k) {
                count[s.charAt(start) - 'A']--;
                start++;
            }
            maxLength = Math.max(maxLength, end - start + 1);
        }
        return maxLength;
    }
```

There's no edge case for this question. The initial step is to extend the window to its limit, that is, the longest we can get to with maximum number of modifications. Until then the variable **start** will remain at 0.

Then as **end** increase, the whole substring from 0 to **end** will violate the rule, so we need to update **start** accordingly (slide the window). We move **start** to the right until the whole string satisfy the constraint again. Then each time we reach such situation, we update our max length.
</p>


### Sliding window, similar to finding longest substring with k distinct characters
- Author: andulas
- Creation Date: Sun Oct 16 2016 17:11:34 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 06:14:41 GMT+0800 (Singapore Standard Time)

<p>
The problem says that we can make at most k changes to the string (any character can be replaced with any other character). So, let's say there were no constraints like the k. Given a string convert it to a string with all same characters with minimal changes. The answer to this is 
>length of the entire string - number of times of the maximum occurring character in the string

Given this, we can apply the at most k changes constraint and maintain a sliding window such that
> (length of substring - number of times of the maximum occurring character in the substring) <= k

```
class Solution {
public:
    int characterReplacement(string s, int k) {
        vector<int> counts(26, 0);
        int start = 0;
        int maxCharCount = 0;
        int n = s.length();
        int result = 0;
        for(int end = 0; end < n; end++){
            counts[s[end]-'A']++;
            if(maxCharCount < counts[s[end]-'A']){
                maxCharCount = counts[s[end]-'A'];
            }
            while(end-start-maxCharCount+1 > k){
                counts[s[start]-'A']--;
                start++;
                for(int i = 0; i < 26; i++){
                    if(maxCharCount < counts[i]){
                        maxCharCount = counts[i];
                    }
                }
            }
            result = max(result, end-start+1);
        }
        return result;
    }
};
```
</p>


### Java Solution - Explained & Easy to Understand for Interviews
- Author: doej4566
- Creation Date: Thu Aug 15 2019 10:02:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 29 2019 04:03:38 GMT+0800 (Singapore Standard Time)

<p>
**Intution:**

The question asks to find the longest substring that contains the same characters. It also says that we can change k characters to make a substring longer and valid.

Ex:
```
"ABAB" k = 1
```

Here we know that we can change 1 character to make a substring that is a valid answer 
AKA: a substring with all the same characters.

So a valid substring answer would be s.substring(0, 3) -> "ABA" because with can replace 1 character.

Another answer could be "BAB".

Using the sliding window technique, we set up pointers ```left = 0``` and ```right = 0```
We know that a our current window / substring is valid when the number of characters that need to be replaced is <= k.

Lets take the example below to understand it better:
Ex:

```
"AABABCC" k = 2
left = 0
right = 4 inclusive
```

This is example above shows a valid substring window because we have enough k changes to change the B\'s to A\'s and match the rest of the string.

"AABAB" with 2 changes is valid

We will need to know how many letters in our substring that we need to replace.
To find out the ```lettersToReplace = (end - start + 1) - mostFreqLetter;```
Pretty much you take the size of the window minus the most freq letter that is in the current window.

Now that we know how many characters that need to be replaced in our window, we can deduce that if ```lettersToReplace > k``` than the window is invalid and we decrease the window size from the left.

Pulling the whole algorithm together we get:

```
class Solution {
    public int characterReplacement(String s, int k) {
        int[] freq = new int[26];
        int mostFreqLetter = 0;
        int left = 0;
        int max = 0;
        
        for(int right = 0; right < s.length(); right++){
            freq[s.charAt(right) - \'A\']++;
            mostFreqLetter = Math.max(mostFreqLetter, freq[s.charAt(right) - \'A\']);
            
            int lettersToChange = (right - left + 1) - mostFreqLetter;
            if(lettersToChange > k){
                freq[s.charAt(left) - \'A\']--;
                left++;
            }
            
            max = Math.max(max, right - left + 1);
        }
        
        return max;
    }
}
```

```Time Complexity: O(N)```
```Space Complexity: O(26) = O(1)```
</p>


