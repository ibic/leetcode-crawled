---
title: "To Lower Case"
weight: 663
#id: "to-lower-case"
---
## Description
<div class="description">
<p>Implement function ToLowerCase() that has a string parameter str, and returns the same string in lowercase.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;Hello&quot;</span>
<strong>Output: </strong><span id="example-output-1">&quot;hello&quot;</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;here&quot;</span>
<strong>Output: </strong><span id="example-output-2">&quot;here&quot;</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">&quot;LOVELY&quot;</span>
<strong>Output: </strong><span id="example-output-3">&quot;lovely&quot;</span>
</pre>
</div>
</div>
</div>
</div>

## Tags
- String (string)

## Companies
- Apple - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

One could solve the problem using built-in functions. 

<iframe src="https://leetcode.com/playground/DEYzs3Qp/shared" frameBorder="0" width="100%" height="140" name="DEYzs3Qp"></iframe>

Since these functions are well-known, most probably the interview will
ask to implement them. In this article we will consider two solutions:

- Usage of hashmap `A --> a, B --> b, ..., Z --> z`.

- Implementation of Python function `lower`, which uses the fact 
that ASCII code of small letter is equal to 
ASCII code of corresponding capital letter + $$2^5$$.

<br /> 
<br />


---
#### Approach 1: HashMap

**Algorithm**

- Build hashmap uppercase letter --> lowercase letter.

- Parse the string. If the current character is an uppercase letter, 
_i.e._ it's in the hashmap, then replace it by the hashmap value. 
Otherwise, keep it unchanged.

![fig](../Figures/709/hashmap.png)

**Implementation**

<iframe src="https://leetcode.com/playground/k7Za56mf/shared" frameBorder="0" width="100%" height="327" name="k7Za56mf"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ to parse the input string. 

* Space complexity: $$\mathcal{O}(N)$$ to keep the output.
<br /> 
<br />


---
#### Approach 2: Implementation of Python Function lower

Let's reproduce the [implementation of Python function `lower`](https://github.com/python/cpython/blob/e42b705188271da108de42b55d9344642170aa2b/Objects/unicodectype.c#L223).

For that, we need two functions.

The first one, `is_upper`, simply checks if the character is between `A`
and `Z` characters.

The second one, `to_lower`, should convert uppercase letter into 
the corresponding lowercase one. 

> It's based on the fact that ASCII code of small letter is equal to 
ASCII code of corresponding capital letter + $$2^5$$: 
`ord('a') = ord('A') + 32`, or `ord('a') = ord('A') | 32`.

![fig](../Figures/709/shift.png)

**Implementation**

!?!../Documents/709_LIS.json:1000,327!?!

<iframe src="https://leetcode.com/playground/dzcJ4Yxp/shared" frameBorder="0" width="100%" height="344" name="dzcJ4Yxp"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ to parse the input string. 

* Space complexity: $$\mathcal{O}(N)$$ to keep the output.
<br /> 
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java, no library methods
- Author: climberig
- Creation Date: Fri Jul 13 2018 07:55:12 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 01:33:13 GMT+0800 (Singapore Standard Time)

<p>
I am assuming the whole point is to avoid ```String#toLowerCase()``` and ```Character#toLowerCase()``` methods
```java
    public String toLowerCase(String str) {
        char[] a = str.toCharArray();
        for (int i = 0; i < a.length; i++)
            if (\'A\' <= a[i] && a[i] <= \'Z\')
                a[i] = (char) (a[i] - \'A\' + \'a\');
        return new String(a);
    }
</p>


### Python short 1 line ASCII & string method solutions
- Author: cenkay
- Creation Date: Thu Jul 12 2018 21:13:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 08:49:21 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def toLowerCase(self, str): 
        return "".join(chr(ord(c) + 32) if 65 <= ord(c) <= 90 else c for c in str)
```
```
class Solution:
    def toLowerCase(self, str): 
        return "".join(chr(ord(c) + 32) if "A" <= c <= "Z" else c for c in str)
```
```
class Solution:
    def toLowerCase(self, str): 
        return str.lower()
```
</p>


### Easy C++ Solution
- Author: dnuang
- Creation Date: Sun Jul 22 2018 08:17:56 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 08:36:09 GMT+0800 (Singapore Standard Time)

<p>
```
string toLowerCase(string str) {        
    for (char& c : str) {
        if (c >= \'A\' && c <= \'Z\') c += 32;
    }
    return str;
}
```
</p>


