---
title: "Split Concatenated Strings"
weight: 522
#id: "split-concatenated-strings"
---
## Description
<div class="description">
<p>Given a list of strings, you could concatenate these strings together into a loop, where for each string you could choose to reverse it or not. Among all the possible loops, you need to find the lexicographically biggest string after cutting the loop, which will make the looped string into a regular one.</p>

<p>Specifically, to find the lexicographically biggest string, you need to experience two phases: 
<ol>
<li>Concatenate all the strings into a loop, where you can reverse some strings or not and connect them in the same order as given.</li>
<li>Cut and make one breakpoint in any place of the loop, which will make the looped string into a regular one starting from the character at the cutpoint. </li>
</ol>
</p>

<p>And your job is to find the lexicographically biggest one among all the possible regular strings.</p>


<p><b>Example:</b><br />
<pre>
<b>Input:</b> "abc", "xyz"
<b>Output:</b> "zyxcba"
<b>Explanation:</b> You can get the looped string "-abcxyz-", "-abczyx-", "-cbaxyz-", "-cbazyx-", <br/>where '-' represents the looped status. <br/>The answer string came from the fourth looped one, <br/>where you could cut from the middle character 'a' and get "zyxcba".
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The input strings will only contain lowercase letters.</li>
<li>The total length of all the strings will not over 1,000.</li>
</ol>
</p>
</div>

## Tags
- String (string)

## Companies
- Alibaba - 0 (taggedByAdmin: true)

## Official Solution
[TOC]
 

## Summary

We are given a list of strings: $$s_1, s_2, s_3,.., s_n$$. We need to concatenate all these strings in a circular fashion in the same given order, but we can reverse every individual string before concatenating. Now, we need to make a cut in the final concatenated string such that the new string formed is the largest one possible in the lexicographic sense

## Solution

---
#### Approach #1 Depth First Search [Time Limit Exceeded]

The simplest solution is to try forming every possible concatenated string by making use of the given strings and then forming every possible cut in each such final concatenated string.

To do so, we can make use of a recursive function `dfs` which appends the current string to the concatenated string formed till now and calls itself with the new concatenated string.  It also appends the reversed current string to the current concatenated string and calls itself. The concatenation of strings goes in the manner of a Depth First Search.
Thus, after reaching the full depth of every branch traversal, we obtain a new concatenated string as illustrated in the animation below. We can apply all the possible cuts to these strings and find the lexicographically largest string out of all of them.

!?!../Documents/555_Split_Assembled_Strings.json:1000,563!?!


<iframe src="https://leetcode.com/playground/HVWgAzK8/shared" frameBorder="0" name="HVWgAzK8" width="100%" height="394"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n \cdot 2^n)$$. Size of Recursion tree can grow upto $$2^n$$ 
where $$n$$ is the number of strings in the list, and at each recursion call one deals with 
the string of length $$n$$.
* Space complexity : $$O(n)$$. Depth of Recursion tree will be $$n$$

---
#### Approach #2 Breadth First Search [Memory Limit Exceeded]

**Algorithm**

Exploring all strings can also be done using BFS method. A Queue $$queue$$ is maintained which stores the strings formed till now after concatenation of the next string and also by concatenation of reverse of next string. Every time we remove a string from the front of the queue, we add two strings to the back of the queue(one by concatenating the next string directly and another by concatenating the next string after reversing).

When all the strings are traversed queue contains $$O(2^n)$$ strings, which correspond to every possible valid string which can be formed by doing the concatenations. We check every string into the queue after circularly rotating by placing the cuts at every possible location. While doing this, we keep a track of the lexicographically largest string.

This animation will depict the method:

!?!../Documents/555_Split_Assembled_Strings1.json:1000,563!?!

<iframe src="https://leetcode.com/playground/5MoksFZL/shared" frameBorder="0" name="5MoksFZL" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n \cdot 2^n)$$. 
$$2^n$$ possible strings will be formed where $$n$$ is the number of strings in the list,
and at each recursion call one deals with the string of length $$n$$.

* Space complexity : $$O(2^n)$$. $$queue$$'s size can grow upto $$2^n$$.

---
#### Approach #3 Optimized Solution [Accepted]

**Algorithm**

In order to understand how this solution works, firstly we'll look at some of the properties of the transformation involved. The first point to note is that the relative ordering of the strings doesn't change after applying the transformations(i.e. reversing and applying cuts).

The second property will be explained taking the help of an example. Consider the given list of strings: $$[s_1, s_2, s_3,..,s_j,..s_n]$$. Now, assume that we choose $$s_j$$ to be the string on which the current cut is placed leading to the formation of two substrings from $$s_j$$, namely, say $$s_{jpre}$$, $$s_{jpost}$$. Thus, the concatenated string formed by such a cut will be: $$[s_{jpost}, s_{j+1},..., s_n, s_{1rev}, s_{2rev},.., s_{(jpre)rev}]$$. Here, $$s_{irev}$$ means the reversed $$s_i$$ string. 

The concatenated string formed follows the same pattern irrespective of where the cut is placed in $$s_j$$. But still, the relative ordering of the strings persists, even if we include the reverse operation as well. 

Now, if we consider only a single cut for the time being, in string $$s_j$$(not reversed) as discussed above, and allow for the reverse operation among the remaining strings, the lexicographically largest concatenated string will be given by: $$[s_{jpost}, \text{max}(s_{j+1},s_{(j+1)rev}) ,..., \text{max}(s_{n},s_{(n)rev}), \text{max}(s_{1},s_{(1)rev}), ..., s_{(jpre)rev}]$$. Here, $$\text{max}$$ refers to the lexicographic maximum operation. 

Thus, if a particular string $$s_j$$ is finalized for the cut, the largest lexicographic concatenated string is dependent only on whether the string $$s_j$$ is reversed or not, and also on the position of the cut. This happens because the reverse/not reverse operation for the rest of the strings is fixed for a chosen $$s_j$$ as shown above and thus, doesn't impact the final result.

Based on the above observations, we follow the given procedure. For every given string, we replace the string with the lexicographically larger string out of the original string and the reversed one. After this, we pick up every new string(chosen as the string on which the cuts will be applied), and apply a cut at all the positions of the currently picked string and form the full concantenated string keeping the rest of the newly formed strings intact. We also reverse the current string and follow the same process. While doing this, we keep a track of the largest lexicographic string found so far.

For a better understanding of the procedure, watch this animation:

!?!../Documents/555_Split_Assembled_Strings2.json:1000,563!?!


<iframe src="https://leetcode.com/playground/A2jShPos/shared" frameBorder="0" name="A2jShPos" width="100%" height="496"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. where $$n$$ is the total number of characters in a list.

* Space complexity : $$O(n)$$. $$t$$ and $$res$$ of size $$n$$ are used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy understanding C++ solution with detailed explnation
- Author: just_not_over_12
- Creation Date: Sun Apr 16 2017 13:22:42 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 20 2018 16:10:16 GMT+0800 (Singapore Standard Time)

<p>
To solve this problem, we must keep in heart the following points:
1. We know the cut point must come from the one string, assumed it is called c-string.
2. Then except the c-string, **all the other string** must become its lexicographically biggest status, assumed it is called b-status. Since only in this situation, we could get the lexicographically biggest string after cutting.
3. To reach the point 2, we need to first let all the string reach its b-status for the convenience of traversing all the strings afterward.
4. Then, for each string's traversal procedure, we need to decide whether it should be reversed or not since we don't know which might generate the final answer, and then we enumerated all the characters in this string.

```
class Solution {
public:
    int n;
    string ans = "";
    // solve function: flag - whether we need to reverse the string; i - the ith string in the strs
    void solve(vector<string>& strs, int i, bool flag) {
        string temp = strs[i]; // intermediate string for not disturbing the original structure of 'strs'
        if (flag) reverse(temp.begin(), temp.end());
        int size = (int)temp.size();
        string str1 = "", str2 = "";
        for (int j=i+1; j<n; ++j) str1 += strs[j]; // Concatenate all the string behind the strs[i]
        for (int j=0; j<i; ++j) str2 += strs[j]; // Concatenate all the string before the strs[i]
        // traverse all the string character
        for (int k=0; k<size; ++k) {
            string newOne = temp.substr(k) + str1 + str2 + temp.substr(0, k); // cut and find the regular string
            ans = ans == "" ? newOne : max(ans, newOne); // update the ans
        }
    }
    // Reach the b-status for all the strings.
    void findMaxStrings(vector<string>& strs) {
        for (int i=0; i<n; ++i) {
            string temp = strs[i];
            reverse(temp.begin(), temp.end());
            strs[i] = strs[i] > temp ? strs[i] : temp;
        }
    }
    // Main function
    string splitLoopedString(vector<string>& strs) {
        n = (int)strs.size();
        if (n == 0) return "";
        findMaxStrings(strs);
        for (int i=0; i<n; ++i) {
            // we dont's know which will generate the final answer, so we traverse both situations.
            solve(strs, i, true); // reverse the string situation
            solve(strs, i, false); // not reverse the string situation
        }
        return ans;
    }
};
```
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Apr 16 2017 11:49:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 16 2017 11:49:32 GMT+0800 (Singapore Standard Time)

<p>
For every starting direction and letter, let's determine the best string we can make.  For subsequent fragments we encounter, we always want them flipped in the orientation that makes them largest.

Thus, for every token, for every starting direction, for every starting letter in the token, we can compute the candidate string directly.  We take the maximum of these.

```
def splitLoopedString(self, A):
    B = [max(x, x[::-1]) for x in A]
    ans = None
    for i, token in enumerate(B):
        for start in (token, token[::-1]):
            for j in xrange(len(start) + 1):
                ans = max(ans, start[j:] + "".join(B[i+1:] + B[:i]) + start[:j])
    return ans
```
</p>


### Neat Java Solution
- Author: vinod23
- Creation Date: Sun Apr 16 2017 11:10:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 16 2017 11:10:12 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public String splitLoopedString(String[] strs) {
        for (int i = 0; i < strs.length; i++) {
            String rev = new StringBuilder(strs[i]).reverse().toString();
            if (strs[i].compareTo(rev) < 0)
                strs[i] = rev;
        }
        String res = "";
        for (int i = 0; i < strs.length; i++) {
            String rev = new StringBuilder(strs[i]).reverse().toString();
            for (String st: new String[] {strs[i], rev}) {
                for (int k = 0; k < st.length(); k++) {
                    StringBuilder t = new StringBuilder(st.substring(k));
                    for (int j = i + 1; j < strs.length; j++)
                        t.append(strs[j]);
                    for (int j = 0; j < i; j++)
                        t.append(strs[j]);
                    t.append(st.substring(0, k));
                    if (t.toString().compareTo(res) > 0)
                        res = t.toString();
                }
            }
        }
        return res;
    }
}
</p>


