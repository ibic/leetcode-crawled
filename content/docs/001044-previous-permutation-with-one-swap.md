---
title: "Previous Permutation With One Swap"
weight: 1044
#id: "previous-permutation-with-one-swap"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of positive integers (not necessarily distinct), return the lexicographically largest permutation that is smaller than <code>A</code>, that can be <strong>made with one swap</strong> (A <em>swap</em> exchanges the positions of two numbers <code>A[i]</code> and <code>A[j]</code>).&nbsp; If it cannot be done, then return the same array.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>[3,2,1]
<strong>Output: </strong>[3,1,2]
<strong>Explanation: </strong>Swapping 2 and 1.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>[1,1,5]
<strong>Output: </strong>[1,1,5]
<strong>Explanation: </strong>This is already the smallest permutation.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>[1,9,4,6,7]
<strong>Output: </strong>[1,7,4,6,9]
<strong>Explanation: </strong>Swapping 9 and 7.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong>[3,1,1,3]
<strong>Output: </strong>[1,3,1,3]
<strong>Explanation: </strong>Swapping 1 and 3.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 10000</code></li>
	<li><code>1 &lt;= A[i] &lt;= 10000</code></li>
</ol>

</div>

## Tags
- Array (array)
- Greedy (greedy)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Quora - 2 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### “1 1 3 3” < “1 3 1 3” < “3 1 1 3”
- Author: beet
- Creation Date: Sun May 26 2019 12:01:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 26 2019 12:01:57 GMT+0800 (Singapore Standard Time)

<p>
\u201C1 1 3 3\u201D < \u201C1 3 1 3\u201D < \u201C3 1 1 3\u201D
</p>


### Why LeetCode Delete My Post
- Author: lee215
- Creation Date: Sat Feb 08 2020 01:39:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Feb 08 2020 01:39:54 GMT+0800 (Singapore Standard Time)

<p>
Leetcode deleted the original post.
May Leetcode please give the reason?

Because I kindly point out the OJ is wrong?
Or Because I put the link of my explanation on the Youtube?

## Wondring why 1000+ accpected?
Sceenshot when I first did this problem.
The description and test cases were both wrong, I didn\'t even understand the the problem.
But somehow I got feeling of what it wants. This record how I did that.
<br>

## Test cases was wrong
Leetcode\'s solution is wrong at first when this problem was published.
**Example 4:**
Input: `[3,1,1,3]`
Leetcode Output: `[1,1,3,3]`
Lee\'s Code Output: `[1,3,1,3]`
Leetcode < Lee Code < Input
LeetCode didn\'t match Lee\'s Code.
<br>

</p>


### Similar to find previous permutation, written in Java
- Author: thyang93
- Creation Date: Sun May 26 2019 12:07:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 26 2019 12:41:49 GMT+0800 (Singapore Standard Time)

<p>
This problem can be solved in a similar way (yet simpler) as finding the previous permutation. But here, we do not have to reverse the numbers between `i` and `j`.
```
class Solution {
    public int[] prevPermOpt1(int[] A) {
        if (A.length <= 1) return A;
        int idx = -1;
		// find the largest i such that A[i] > A[i + 1]
        for (int i = A.length - 1; i >= 1; i--) {
            if (A[i] < A[i - 1]) {
                idx = i - 1;
                break;
            }
        }
		// the array already sorted, not smaller permutation
        if (idx == -1) return A;
		// find the largest j such that A[j] > A[i], then swap them
        for (int i = A.length - 1; i > idx; i--) {
			// the second check to skip duplicate numbers
            if (A[i] < A[idx] && A[i] != A[i - 1]) {
                int tmp = A[i];
                A[i] = A[idx];
                A[idx] = tmp;
                break;
            }
        }
        return A;
    }
}
```
</p>


