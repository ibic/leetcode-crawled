---
title: "Encode Number"
weight: 1078
#id: "encode-number"
---
## Description
<div class="description">
<p>Given a non-negative integer <code>num</code>, Return its <em>encoding</em> string.</p>

<p>The encoding is done by converting the integer to a string using a secret function that you should deduce from the following table:</p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/06/21/encode_number.png" style="width: 164px; height: 360px;" /></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> num = 23
<strong>Output:</strong> &quot;1000&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> num = 107
<strong>Output:</strong> &quot;101100&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= num &lt;= 10^9</code></li>
</ul>
</div>

## Tags
- Math (math)
- Bit Manipulation (bit-manipulation)

## Companies
- Quora - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Binary of n + 1
- Author: lee215
- Creation Date: Sun Nov 17 2019 00:02:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 00:30:39 GMT+0800 (Singapore Standard Time)

<p>
# Solution 1: Recursion idea
The following sequence can be built up form the ealier result.
So I search index of the prefix part
For example:
`f(5) = "10"`
`f(6) = "11"`
The prefix are both `f(2) = "1"`

so we found that `f(n)` has `f((n - 1) / 2)` as prefix.
<br>

**Java:**
```java
    public String encode(int n) {
        return n > 0 ? encode((n - 1) / 2) + "10".charAt(n % 2) : "";
    }
```

**C++:**
```cpp
    string encode(int n) {
        return n > 0 ? encode((n - 1) / 2) + "10"[n % 2] : "";
    }
```

**Python:**
```python
    def encode(self, n):
        return self.encode((n - 1) / 2) + \'10\'[n % 2] if n else ""
```
<br>

# Solution 2: Binary of n + 1
Assume `g(n) = "1" + f(n)`
we can find:
`g(0) =  "1"
g(1) =  "10"
g(2) = "111"
g(3) = "100"
g(4) = "101"
g(5) = "110"
g(6) = "111"`

Now everything is obvious:
`g(n) = binary(n + 1)`
`"1" + f(n) = binary(n + 1)`
`f(n) = binary(n + 1).substring(1)`
<br>

**Java:**
```java
    public String encode(int n) {
        return Integer.toBinaryString(n + 1).substring(1);
    }
```

**Python:**
```python
    def encode(self, n):
        return bin(n + 1)[3:]
```
<br>

# Complexity
Time `O(logN)`
Space `O(logN)`
<br>

# More
Thanks for upvotes and followers.
**weibo** leecode
**youtube** lee215
</p>


### Intuitive Explanation With Logic And Picture
- Author: gagandeepahuja09
- Creation Date: Sun Nov 17 2019 00:07:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 00:26:57 GMT+0800 (Singapore Standard Time)

<p>
INTUTION
![image](https://assets.leetcode.com/users/gagandeepahuja09/image_1573920431.png)
1) Seeing the constraints, it is clear that pattern cannot be entirely simulated. So, we need to do something in log time complexity.
2) From the pattern formed, we can see that a particular number n is affecting the answer of 2 * n + 1 and 2 * n + 2. Hence, we can at each step divide the number by (n - 1) / 2, to reach back to the old state.
3) If it is divisible by 2, we append 1. Else 0.
4) Since, we are doing the reverse process, we need to reverse back the string.

```
class Solution {
public:
    string encode(int num) {
        string ans = "";
        while(num >= 2) {
            if(num % 2)
                ans += "0";
            else
                ans += "1";
            num = (num - 1) / 2;
        }
        if(num == 2)
            ans += "1";
        if(num == 1)
            ans += "0";
        reverse(ans.begin(), ans.end());
        return ans;
    }
};
```
</p>


### My personal opinion about this question
- Author: disturbance_is_created
- Creation Date: Sun Nov 17 2019 22:51:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 22:51:35 GMT+0800 (Singapore Standard Time)

<p>
It was kind of stupid question with not enough explanation
</p>


