---
title: "Confusing Number II"
weight: 986
#id: "confusing-number-ii"
---
## Description
<div class="description">
<p>We can rotate digits by 180 degrees to form new digits. When 0, 1, 6, 8, 9 are rotated 180 degrees, they become 0, 1, 9, 8, 6 respectively. When 2, 3, 4, 5 and 7 are rotated 180 degrees, they become invalid.</p>

<p>A <em>confusing number</em> is a number that when rotated 180 degrees becomes a <strong>different</strong> number with each digit valid.(Note that the rotated number can be greater than the original number.)</p>

<p>Given a positive integer <code>N</code>, return the number of confusing numbers between <code>1</code> and <code>N</code>&nbsp;inclusive.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">20</span>
<strong>Output: </strong><span id="example-output-1">6</span>
<strong>Explanation: </strong>
The confusing numbers are [6,9,10,16,18,19].
6 converts to 9.
9 converts to 6.
10 converts to 01 which is just 1.
16 converts to 91.
18 converts to 81.
19 converts to 61.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">100</span>
<strong>Output: </strong><span id="example-output-2">19</span>
<strong>Explanation: </strong>
The confusing numbers are [6,9,10,16,18,19,60,61,66,68,80,81,86,89,90,91,98,99,100].
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= N &lt;= 10^9</code></li>
</ol>

</div>

## Tags
- Math (math)
- Backtracking (backtracking)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python ugly backtracking solution
- Author: mujroom
- Creation Date: Sun Oct 20 2019 06:43:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 06:43:13 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def confusingNumberII(self, N):
        """
        :type N: int
        :rtype: int
        """
        valid = [0,1,6,8,9]
        mapping = {0: 0,1: 1,6: 9,8: 8, 9: 6}

        self.count = 0

        def backtrack(v, rotation,digit):
            if v: 
                if v != rotation: 
                    self.count += 1  
            for i in valid: 
                if v*10+i > N:
                    break 
                else:
                    backtrack(v*10+i, mapping[i]*digit + rotation, digit*10)
        
        backtrack(1,1, 10)
        backtrack(6,9,10)
        backtrack(8,8,10)
        backtrack(9,6,10)

        return self.count   
```
</p>


### Easy to understand Java backtracking solution, covers edge case
- Author: cherrrrry
- Creation Date: Sun Dec 08 2019 15:28:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 08 2019 15:28:36 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    Map<Integer, Integer> map = new HashMap<>();
    int res = 0;
    public int confusingNumberII(int N) {
        map.put(0, 0);
        map.put(1, 1);
        map.put(6, 9);
        map.put(8, 8);
        map.put(9, 6);
        helper(N, 0);
        return res;
    }
    private void helper(int N, long cur) {
        if (isConfusingNumber(cur)) {
            res++;
        }
        for (Integer i : map.keySet()) {
            if (cur * 10 + i <= N && cur * 10 + i != 0) {
                helper(N, cur * 10 + i);
            }
        }
    }
    private boolean isConfusingNumber(long n) {
        long src = n;
        long res = 0;
        while (n > 0) {
            res = res * 10 + map.get((int) n % 10); 
            n /= 10;
        }
        return res != src;
    }
}
```
</p>


### Confusing number = Total number - Strobogrammatic number (Java 1ms)
- Author: thyang93
- Creation Date: Sun Sep 29 2019 10:53:08 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 18 2019 10:22:01 GMT+0800 (Singapore Standard Time)

<p>
This solution is inspired by [@joshualian1989](https://leetcode.com/problems/confusing-number-ii/discuss/312388/C%2B%2B-12ms-Counting-numbers-with-digits-"01689"-and-minus-count-of-confusing-numbers). 

The intuition is there are more confusing number than strobogrammatic number,
and it is fairly staightforward to calculate the total number. 
For example, in the extreme case, 
```N=1e9, total=1953126, confusing=1950627, strobogrammatic=2499```

The ```dfs()``` function is almost the same as [this one](https://leetcode.com/problems/strobogrammatic-number-iii/discuss/67378/Concise-Java-Solution). There are two things to notice while performing the dfs:
1. Use char[] or StringBuilder, instead of generating lots of strings, which is a waste of memory
2. For numbers to be valid, leading zero is not allowed

Time complexity: ```O(5^M)```
Space complexity: ```O(M)```
(M is the length of str(N))
```
class Solution {
    static char[][] pairs = {{\'0\', \'0\'}, {\'1\', \'1\'}, {\'6\', \'9\'}, {\'8\', \'8\'}, {\'9\', \'6\'}};
    public int confusingNumberII(int N) {
        String num = Integer.toString(N);
        int res = findTotal(num);
        for (int len = 1; len <= num.length(); len++) {
            char[] curr = new char[len];
            res -= dfs(curr, num, 0, len - 1);
        }
        return res;
    }
    // count the # of numbers from "01689" that is less than N
    private int findTotal(String s) {
        if (s.length() == 0) return 1;
        char first = s.charAt(0);
        int res = count(first) * (int) (Math.pow(5, s.length() - 1));
        if (first == \'0\' || first == \'1\' || first == \'6\' || first == \'8\' || first == \'9\') {
            res += findTotal(s.substring(1));
        }
        return res;
    }
    // count the # of Strobogrammatic numbers
    private int dfs(char[] curr, String num, int left, int right) {
        int res = 0;
        if (left > right) {
            String s = new String(curr);
            if (s.length() < num.length() || s.compareTo(num) <= 0) {
                res += 1;
            }
        } else {
            for (char[] p : pairs) {
                curr[left] = p[0];
                curr[right] = p[1];
                if ((curr[0] == \'0\' && curr.length > 1) || (left == right && p[0] != p[1])) continue;
                res += dfs(curr, num, left + 1, right - 1);
            }
        }
        return res;
    }
    // a helper function that counts the # of chars in "01689" less than given \'c\'
    private int count(Character c) {
        int res = 0;
        for (char[] p : pairs) {
            if (p[0] < c) res += 1;
        }
        return res;
    }
}
```
</p>


