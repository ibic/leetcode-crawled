---
title: "Number of Valid Words for Each Puzzle"
weight: 1146
#id: "number-of-valid-words-for-each-puzzle"
---
## Description
<div class="description">
With respect to a given <code>puzzle</code> string, a <code>word</code> is <em>valid</em>&nbsp;if both the following conditions are satisfied:
<ul>
	<li><code>word</code> contains the first letter of <code>puzzle</code>.</li>
	<li>For each letter in <code>word</code>, that letter is in <code>puzzle</code>.<br />
	For example, if the puzzle is &quot;abcdefg&quot;, then valid words are &quot;faced&quot;, &quot;cabbage&quot;, and &quot;baggage&quot;; while invalid words are &quot;beefed&quot; (doesn&#39;t include &quot;a&quot;) and &quot;based&quot; (includes &quot;s&quot; which isn&#39;t in the puzzle).</li>
</ul>
Return an array <code>answer</code>, where <code>answer[i]</code> is the number of words in the given word list&nbsp;<code>words</code> that are valid with respect to the puzzle <code>puzzles[i]</code>.
<p>&nbsp;</p>
<p><strong>Example :</strong></p>

<pre>
<strong>Input:</strong> 
words = [&quot;aaaa&quot;,&quot;asas&quot;,&quot;able&quot;,&quot;ability&quot;,&quot;actt&quot;,&quot;actor&quot;,&quot;access&quot;], 
puzzles = [&quot;aboveyz&quot;,&quot;abrodyz&quot;,&quot;abslute&quot;,&quot;absoryz&quot;,&quot;actresz&quot;,&quot;gaswxyz&quot;]
<strong>Output:</strong> [1,1,3,2,4,0]
<strong>Explanation:</strong>
1 valid word&nbsp;for &quot;aboveyz&quot; : &quot;aaaa&quot; 
1 valid word&nbsp;for &quot;abrodyz&quot; : &quot;aaaa&quot;
3 valid words for &quot;abslute&quot; : &quot;aaaa&quot;, &quot;asas&quot;, &quot;able&quot;
2 valid words for&nbsp;&quot;absoryz&quot; : &quot;aaaa&quot;, &quot;asas&quot;
4 valid words for&nbsp;&quot;actresz&quot; : &quot;aaaa&quot;, &quot;asas&quot;, &quot;actt&quot;, &quot;access&quot;
There&#39;re&nbsp;no valid words for&nbsp;&quot;gaswxyz&quot; cause none of the words in the list contains letter &#39;g&#39;.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= words.length &lt;= 10^5</code></li>
	<li><code>4 &lt;= words[i].length &lt;= 50</code></li>
	<li><code>1 &lt;= puzzles.length &lt;= 10^4</code></li>
	<li><code>puzzles[i].length == 7</code></li>
	<li><code>words[i][j]</code>, <code>puzzles[i][j]</code> are English lowercase letters.</li>
	<li>Each <code>puzzles[i] </code>doesn&#39;t contain repeated characters.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Bit Manipulation (bit-manipulation)

## Companies
- Dropbox - 4 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Bit-manipulation + Map Solution 90ms
- Author: Edwin_Z
- Creation Date: Mon Sep 02 2019 01:14:21 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 02 2019 01:28:12 GMT+0800 (Singapore Standard Time)

<p>
First of all, create a map to cache the frequency of the encoded word.

Next, loop the puzzles. During the inner loop, instead of loop through map\'s keyset (which cause TLE), use ``` sub = (sub - 1) & mask ``` to find all possible char combinations of current puzzel. 

We need update count only if combination contains first element of puzzle as well as map has a record of it (means this puzzle\'s char combination is as the same as one of the encoded word).

Time Compliexity: O( n * 2 ^ 7 +  m * k) = O(n + mk)
n = len(puzzles);
m = len(words);
k = len(single word)


```
class Solution {
    public List<Integer> findNumOfValidWords(String[] words, String[] puzzles) {
        
        Map<Integer, Integer> map = new HashMap<>();
        
        for(String w : words){
            int mask = 0;
            for(int i = 0; i < w.length(); i++){
                mask |= 1 << (w.charAt(i) - \'a\');
            }
            map.put(mask, map.getOrDefault(mask, 0) + 1);
        }
        
        List<Integer> res = new ArrayList<>();
        
        for(String p : puzzles){
            int mask = 0;
            for(int i = 0; i < p.length(); i++){
                mask |= 1 << (p.charAt(i) - \'a\');
            }
            int c = 0;
            int sub = mask;
            int first = 1 << (p.charAt(0) - \'a\');
            while(true){
                if((sub & first) == first && map.containsKey(sub)){
                    c += map.get(sub);
                }
                
                if(sub == 0) break;
                
                sub = (sub - 1) & mask; // get the next substring
            }
            
            res.add(c);
        }
        
        return res;
    }
}
```
</p>


### [Python] Find all Sub Puzzles
- Author: lee215
- Creation Date: Sun Sep 01 2019 12:08:21 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 03 2019 20:51:29 GMT+0800 (Singapore Standard Time)

<p>
## **Part I**
1. For each word `w` in `words`, calculate the set of word\'s letters.
2. count the number of differen set, save to the `counter`.
<br>

## **Part II**
1. For each puzzle `p` in `puzzles`, calculate all subset of puzzle `p`.
The subset has to include puzzle\'s first letter.
2. For each set of sub-puzzle, find its occurrence from the counter of part.
3. Sum up the occurrencesmof all sub-puzzles, push it to the result list `res`
<br>

## **Complexity**
**Part I** Time `O(W)`, space `O(W)`
**Part II** Time `O(2^6 * P)` = `O(64P)`, space `O(1)`
<br>

## **Set of letters**
We use hashmap to count the sets of letters.
So that the set of letters need to presented as hashable(immutable).
I used `str(sorted(set(w)))`, it works but not very efficient.

`frozenset(w)` seems to be a good choice,
It is an inbuilt function is Python,
Same as set, except its elements are immutable.
Simply it freezes the iterable objects and makes them unchangeable.

A faster way of doing that is to use a bit mask.
We use an integer of 26 bits to present the set of letters.

Hashmap of words can still be improved.
A much faster way to build up a trie of words.
<br>

**Python, use build-in combinations:**
```python
    def findNumOfValidWords(self, words, puzzles):
        count = collections.Counter(frozenset(w) for w in words)
        res = []
        for p in puzzles:
            cur = 0
            for k in xrange(7):
                for c in itertools.combinations(p[1:], k):
                    cur += count[frozenset(tuple(p[0]) + c)]
            res.append(cur)
        return res
```
**Python, use bfs to find all combinations**
```python
    def findNumOfValidWords(self, words, puzzles):
        count = collections.Counter(frozenset(w) for w in words)
        res = []
        for p in puzzles:
            subs = [p[0]]
            for c in p[1:]:
                subs += [s + c for s in subs]
            res.append(sum(count[frozenset(s)] for s in subs))
        return res
```

**Python, use bfs + mask**
```python
    def findNumOfValidWords(self, words, puzzles):
        count = collections.Counter()
        for w in words:
            if len(set(w)) > 7: continue
            m = 0
            for c in w:
                m |= 1 << (ord(c) - 97)
            count[m] += 1
        res = []
        for p in puzzles:
            bfs = [1 << (ord(p[0]) - 97)]
            for c in p[1:]:
                bfs += [m | 1 << (ord(c) - 97) for m in bfs]
            res.append(sum(count[m] for m in bfs))
        return res
```

</p>


### Detailed Explanation using Trie O(word_length + 100*puzzle_length)
- Author: Just__a__Visitor
- Creation Date: Sun Sep 01 2019 12:11:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 01 2019 15:13:06 GMT+0800 (Singapore Standard Time)

<p>
The bitwise solution is a genius one. However, `bits` didn\'t even cross my mind during the contest. I need to up my coding skills!
Anyway, I was thinking of the *trie* based approach during the contest (by looking at the constraints given). It cleared all the test cases and hence I would like to share my approach.
# Intuition
* First of all, notice that for each word, the ordering and the count of elements does not matter. So, we can keep all our words in a sorted order and avoid the duplicate letters. This makes the maximum size of the word to be 26 (instead of 50). 

* Further, notice that the ordering of words in the puzzle doesn\'t matter. We just need to keep track of the first letter of the puzzle.  So, we can also keep our puzzle in sorted manner.

* Let us define a **trie** on the set of words. The `bool isEndOfWord` field has been modified to store the number of words ending at that node. 

* Insertion in the trie is trivial, just follow the normal procedure for insertion, and in the end, increase the count of words ending at the last node by 1. 

* For any puzzle, we willl traverse the tree and get the answer. Observe that we only need to go 7 levels deep. 

* We explore all the 7 possibilities. As soon as we see the first element, we can keep incrementing the count from this node onwards. 

* If we don\'t see the first element, we keep going down without changing count.


# Time Complexity Analysis
At the first level, we have 7 choices, but at the next level, we only have 6 choice, and the next 5, and so on, This gives us an upper bound od `7 * 6 * ....1` = `7!`,  for each query. The overall complexity becomes **O(query_length * 7!)**, which is on the border but would hopefully pass.

### Is the bound asymptotically tight?
If we think about it in big O notation, the derivation is correct. But to make the bounds tighter, let\'s use the theta notation and amortized analysis,

## Lowering the upper bound
Notice that we can only go 7 levels deep. So, the answer can lie in either or all of this levels.

1) How many answers lie in the first level ? Clearly, in the worst case, it is `7C1` , as selecting any 1 would give us an answer.

2) How many answers lie in the second level? Clearly, selecting any 2 elements out of the 7 would give us an answer (since any 2 elements from the set can have only 1 sorted order, hence we need to just select elements instead of permuting them). So, the answer at the second level is `7C2`

3) Similarly, how many at the third level? Clearly, we can select 3 elements out of the 7, and it would give us a unique answer. Therefore, at the third level. we have `7C3`.

.
.
.

7) At the 7th level, we have `7C7` = 1 answer (as there is only one way to reach the 7th level as everything has to be sorted).


In conclusion, the maximum nodes we would have to visit is `7C1 + ... 7C7` = `2^7 - 1` = 127.

So, the worst case traversal would be of the order of `10 ^ 2`. So, the overall complexity is O(100 * queries_length). 

[Even if you include the hidden constant factors, it won\'t go beyond `100 * queries_length`



```
const int ALPHABET_SIZE = 26;

/* The structure of a trie node */
struct TrieNode
{
    struct TrieNode* children[ALPHABET_SIZE];
    int count = 0;
};

/* Creates a new trie node and returns the pointer */
struct TrieNode* getNode()
{
    struct TrieNode* newNode = new TrieNode;

    for(int i = 0; i < ALPHABET_SIZE; i++)
        newNode->children[i] = nullptr;
    
    newNode->count = 0;

    return newNode;
}

/* Inserts the given string to the collection */
void insert(struct TrieNode* root, string str)
{
    struct TrieNode* pCrawl = root;

    for(int i = 0; i < str.length(); i++)
    {
        int index = str[i]-\'a\';

        if(!pCrawl->children[index])
            pCrawl->children[index] = getNode();
        
        pCrawl = pCrawl->children[index];
    }

    pCrawl->count++;
}

/* Returns the count of strings which are valid */
int search(struct TrieNode* root, string str, bool firstSeen, char firstLetter)
{
    if(!root)
        return 0;
    
    int count = 0;
    
    if(firstSeen)
        count += root->count;
    
    for(int i = 0; i < str.length(); i++)
    {
        int index = str[i] - \'a\';
        
        if(str[i] == firstLetter)
            count += search(root->children[index], str, true, firstLetter);
        else
            count += search(root->children[index], str, firstSeen, firstLetter);
    }

    return count;
}

class Solution
{
public:
    vector<int> findNumOfValidWords(vector<string>& words, vector<string>& puzzles);
};

vector<int> Solution :: findNumOfValidWords(vector<string>& words, vector<string>& puzzles)
{
    struct TrieNode* root = getNode();
    
    for(auto str : words)
    {
        set<char> temp;
        temp.insert(str.begin(), str.end());
        
        string sorted = "";
        for(auto ele : temp)
            sorted += ele;
        
        insert(root, sorted);
    }
    
    vector<int> count;
    for(auto puzzle : puzzles)
    {
        char firstLetter = puzzle[0];
        sort(puzzle.begin(), puzzle.end());
        count.push_back(search(root, puzzle, false, firstLetter));
    }
    
    return count;
    
}
```
</p>


