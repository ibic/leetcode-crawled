---
title: "Jump Game III"
weight: 1227
#id: "jump-game-iii"
---
## Description
<div class="description">
<p>Given an array of non-negative integers <code>arr</code>, you are initially positioned at <code>start</code>&nbsp;index of the array. When you are at index <code>i</code>, you can jump&nbsp;to <code>i + arr[i]</code> or <code>i - arr[i]</code>, check if you can reach to <strong>any</strong> index with value 0.</p>

<p>Notice that you can not jump outside of the array at any time.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [4,2,3,0,3,1,2], start = 5
<strong>Output:</strong> true
<strong>Explanation:</strong> 
All possible ways to reach at index 3 with value 0 are: 
index 5 -&gt; index 4 -&gt; index 1 -&gt; index 3 
index 5 -&gt; index 6 -&gt; index 4 -&gt; index 1 -&gt; index 3 
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [4,2,3,0,3,1,2], start = 0
<strong>Output:</strong> true 
<strong>Explanation: 
</strong>One possible way to reach at index 3 with value 0 is: 
index 0 -&gt; index 4 -&gt; index 1 -&gt; index 3
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [3,0,2,1,2], start = 2
<strong>Output:</strong> false
<strong>Explanation: </strong>There is no way to reach at index 1 with value 0.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 5 * 10^4</code></li>
	<li><code>0 &lt;= arr[i] &lt;&nbsp;arr.length</code></li>
	<li><code>0 &lt;= start &lt; arr.length</code></li>
</ul>

</div>

## Tags
- Breadth-first Search (breadth-first-search)
- Graph (graph)

## Companies
- Microsoft - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

### Overview

You probably can guess from the problem title, this is the third problem in the series of [Jump Game](https://leetcode.com/problems/jump-game/) problems. Those problems are similar, but have considerable differences, making their solutions quite different.

Here, two approaches are introduced: *Breadth-First Search* approach and *Depth-First Search* approach. 

---

#### Approach 1: Breadth-First Search

Most solutions start from a brute force approach and are optimized by removing unnecessary calculations. Same as this one.

A naive brute force approach is to iterate all the possible routes and check if there is one reaches `zero`. However, if we already checked one index, we do not need to check it again. We can mark the index as visited by make it negative. 

<iframe src="https://leetcode.com/playground/WKyWvaQm/shared" frameBorder="0" width="100%" height="500" name="WKyWvaQm"></iframe>

**Complexity Analysis**

Assume $$N$$ is the length of `arr`.

* Time complexity: $$\mathcal{O}(N)$$ since we will visit every index at most once.
 
* Space complexity : $$\mathcal{O}(N)$$ since it needs `q` to store next index. In fact, `q` would keep at most two levels of nodes. Since we got two children for each node, the traversal of this solution is a binary tree. The maximum number of nodes within a single level for a binary tree would be $$\frac{N}{2}$$, so the maximum length of `q` is 
$$\mathcal{O}(\frac{N}{2} + \frac{N}{2})=\mathcal{O}(N)$$.

---

#### Approach 2: Depth-First Search

DFS is similar to BFS but differs in the order of searching. You should consider DFS when you think of BFS.

Still, we make the value negative to mark as visited.

<iframe src="https://leetcode.com/playground/iv9LQw5m/shared" frameBorder="0" width="100%" height="259" name="iv9LQw5m"></iframe>

**Complexity Analysis**

Assume $$N$$ is the length of `arr`.

* Time complexity: $$\mathcal{O}(N)$$, since we will visit every index only once.
 
* Space complexity: $$\mathcal{O}(N)$$ since it needs at most $$\mathcal{O}(N)$$ stacks for recursions.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java Recursion
- Author: votrubac
- Creation Date: Sun Dec 29 2019 14:16:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 05 2020 09:40:15 GMT+0800 (Singapore Standard Time)

<p>
... split lines and added brackets for readability.
#### C++
```CPP
unordered_set<int> vis;
bool canReach(vector<int>& arr, int st) {
    if (st >= 0 && st < arr.size() && vis.insert(st).second) {
        return arr[st] == 0 ||
            canReach(arr, st + arr[st]) || canReach(arr, st - arr[st]);
    }
    return false;
}
```
#### Java
We can also use the input array to track visited cells. Note that I just add `arr.length` to the value, in case we want to restore the original values later.
```JAVA
public boolean canReach(int[] arr, int st) {
    if (st >= 0 && st < arr.length && arr[st] < arr.length) {
        int jump = arr[st];
        arr[st] += arr.length;
        return jump == 0 || canReach(arr, st + jump) || canReach(arr, st - jump);
    }
    return false;
}
```
**Complexity Analysis**
- Time: O(n); we mark elements as visited and do not process them again.
- Memory: O(n) for the recursion.
</p>


### [Java/C++/Python] 1 Line Recursion
- Author: lee215
- Creation Date: Tue Dec 31 2019 01:48:41 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 02 2020 10:09:30 GMT+0800 (Singapore Standard Time)

<p>
Hiking last week, sorry for late.
Should I have a way to announce in advance?
<br>

## **Explanation**
```python
    def canReach(self, A, i):
        if 0 <= i < len(A) and A[i] >= 0:
            A[i] = -A[i]
            return A[i] == 0 or self.canReach(A, i + A[i]) or self.canReach(A, i - A[i])
        return False
```

1. Check `0 <= i < A.length`
2. flip the checked number to negative `A[i] = -A[i]`
3. If `A[i] == 0`, get it and return `true`
4. Continue to check `canReach(A, i + A[i])` and `canReach(A, i - A[i])`
<br>

## **Complexity**
Time `O(N)`, as each number will be flipper at most once.
Space `O(N)` for recursion.
<br>

**Java:**
```java
    public boolean canReach(int[] A, int i) {
        return 0 <= i && i < A.length && A[i] >= 0 && ((A[i] = -A[i]) == 0 || canReach(A, i + A[i]) || canReach(A, i - A[i]));
    }
```

**C++:**
```cpp
    bool canReach(vector<int>& A, int i) {
        return 0 <= i && i < A.size() && A[i] >= 0 && (!(A[i] = -A[i]) || canReach(A, i + A[i]) || canReach(A, i - A[i]));
    }
```


</p>


### Simple one using queue and visited paths [JAVA]
- Author: JatinYadav96
- Creation Date: Sun Dec 29 2019 12:11:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 29 2019 12:11:08 GMT+0800 (Singapore Standard Time)

<p>
```
public static boolean canReach(int[] arr, int start) {
        int n = arr.length;
        HashSet<Integer> visited = new HashSet<>(); // visited set
        Queue<Integer> q = new LinkedList<>();
        q.add(start);
        while (!q.isEmpty()) {
            int i = q.poll();
            if (arr[i] == 0) return true; // found then return it
            if (visited.contains(i)) continue; // already visited than continue
            visited.add(i);
            if (i + arr[i] < n)
                q.add(i + arr[i]);
            if (i - arr[i] >= 0)
                q.add(i - arr[i]);
        }
        return false;// not found
    }
```
</p>


