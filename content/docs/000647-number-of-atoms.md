---
title: "Number of Atoms"
weight: 647
#id: "number-of-atoms"
---
## Description
<div class="description">
<p>Given a chemical <code>formula</code> (given as a string), return the count of each atom.</p>

<p>The atomic element always starts with an uppercase character, then zero or more lowercase letters, representing the name.</p>

<p>One or more digits representing that element&#39;s count may follow if the count is greater than 1. If the count is 1, no digits will follow. For example, H2O and H2O2 are possible, but H1O2 is impossible.</p>

<p>Two formulas concatenated together to produce another formula. For example, H2O2He3Mg4 is also a formula.</p>

<p>A formula placed in parentheses, and a count (optionally added) is also a formula. For example, (H2O2) and (H2O2)3 are formulas.</p>

<p>Given a <code>formula</code>, return <em>the count of all elements as a string in the following form</em>: the first name (in sorted order), followed by its count (if that count is more than 1), followed by the second name (in sorted order), followed by its count (if that count is more than 1), and so on.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> formula = &quot;H2O&quot;
<strong>Output:</strong> &quot;H2O&quot;
<strong>Explanation:</strong> The count of elements are {&#39;H&#39;: 2, &#39;O&#39;: 1}.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> formula = &quot;Mg(OH)2&quot;
<strong>Output:</strong> &quot;H2MgO2&quot;
<strong>Explanation:</strong> The count of elements are {&#39;H&#39;: 2, &#39;Mg&#39;: 1, &#39;O&#39;: 2}.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> formula = &quot;K4(ON(SO3)2)2&quot;
<strong>Output:</strong> &quot;K4N2O14S4&quot;
<strong>Explanation:</strong> The count of elements are {&#39;K&#39;: 4, &#39;N&#39;: 2, &#39;O&#39;: 14, &#39;S&#39;: 4}.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> formula = &quot;Be32&quot;
<strong>Output:</strong> &quot;Be32&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= formula.length&nbsp;&lt;= 1000</code></li>
	<li><code>formula</code> consists of English letters, digits, <code>&#39;(&#39;</code>, and <code>&#39;)&#39;</code>.</li>
	<li><code>formula</code> is always valid.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Stack (stack)
- Recursion (recursion)

## Companies
- Amazon - 5 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- ByteDance - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- IXL - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Recursion [Accepted]

**Intuition and Algorithm**

Write a function `parse` that parses the formula from index `i`, returning a map `count` from names to multiplicities (the number of times that name is recorded).

We will put `i` in global state: our `parse` function increments `i` throughout any future calls to `parse`.

* When we see a `'('`, we will parse whatever is inside the brackets (up to the closing ending bracket) and add it to our count.

* Otherwise, we should see an uppercase character: we will parse the rest of the letters to get the name, and add that (plus the multiplicity if there is one.)

* At the end, if there is a final multiplicity (representing the multiplicity of a bracketed sequence), we'll multiply our answer by this.

<iframe src="https://leetcode.com/playground/ANiqSCxc/shared" frameBorder="0" width="100%" height="500" name="ANiqSCxc"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$, where $$N$$ is the length of the formula.  It is $$O(N)$$ to parse through the formula, but each of $$O(N)$$ multiplicities after a bracket may increment the count of each name in the formula (inside those brackets), leading to an $$O(N^2)$$ complexity.

* Space Complexity: $$O(N)$$.  We aren't recording more intermediate information than what is contained in the formula.

---
#### Approach #2: Stack [Accepted]

**Intuition and Algorithm**

Instead of recursion, we can simulate the call stack by using a stack of `count`s directly.

<iframe src="https://leetcode.com/playground/iRChyyGM/shared" frameBorder="0" width="100%" height="500" name="iRChyyGM"></iframe>

**Complexity Analysis**

* Time Complexity $$O(N^2)$$, and Space Complexity $$O(N)$$.  The analysis is the same as *Approach #1*.

---
#### Approach #3: Regular Expressions [Accepted]

**Intuition and Algorithm**

Whenever parsing is involved, we can use *regular expressions*, a language for defining patterns in text.

Our regular expression will be `"([A-Z][a-z]*)(\d*)|(\()|(\))(\d*)"`.  Breaking this down by *capture group*, this is:

* `([A-Z][a-z]*)` Match an uppercase character followed by any number of lowercase characters, then (`(\d*)`) match any number of digits.
* OR, `(\()` match a left bracket or `(\))` right bracket, then `(\d*)` match any number of digits.

Now we can proceed as in *Approach #2*.

* If we parsed a name and multiplicity `([A-Z][a-z]*)(\d*)`, we will add it to our current count.

* If we parsed a left bracket, we will append a new `count` to our stack, representing the nested depth of parentheses.

* If we parsed a right bracket (and possibly another multiplicity), we will multiply our deepest level `count`, `top = stack.pop()`, and add those entries to our current count.

<iframe src="https://leetcode.com/playground/iuXagcZD/shared" frameBorder="0" width="100%" height="500" name="iuXagcZD"></iframe>

**Complexity Analysis**

* Time Complexity $$O(N^2)$$, and Space Complexity $$O(N)$$.  The analysis is the same as *Approach #1*, as this regular expression did not look backwards when parsing.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution using Stack and Map
- Author: kay_deep
- Creation Date: Sun Nov 12 2017 12:24:48 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 07:30:43 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public String countOfAtoms(String formula) {
        Stack<Map<String,Integer>> stack= new Stack<>();
        Map<String,Integer> map= new HashMap<>();
        int i=0,n=formula.length();
        while(i<n){
            char c=formula.charAt(i);i++;
            if(c=='('){
                stack.push(map);
                map=new HashMap<>();
            }
            else if(c==')'){
                int val=0;
                while(i<n && Character.isDigit(formula.charAt(i)))
                    val=val*10+formula.charAt(i++)-'0';

                if(val==0) val=1;
                if(!stack.isEmpty()){
                Map<String,Integer> temp= map;
                map=stack.pop();
                    for(String key: temp.keySet())
                        map.put(key,map.getOrDefault(key,0)+temp.get(key)*val);
                }
            }
            else{
                int start=i-1;
                while(i<n && Character.isLowerCase(formula.charAt(i))){
                 i++;
                }
                String s= formula.substring(start,i);
                int val=0;
                while(i<n && Character.isDigit(formula.charAt(i))) val=val*10+ formula.charAt(i++)-'0';
                if(val==0) val=1;
                map.put(s,map.getOrDefault(s,0)+val);
            }
        }
        StringBuilder sb= new StringBuilder();
        List<String> list= new ArrayList<>(map.keySet());
        Collections.sort(list);
        for(String key: list){ 
            sb.append(key);
          if(map.get(key)>1) sb.append(map.get(key));
                                    }
        return sb.toString();
    }
}

```
</p>


### Python // 20 lines very readable, simplest and shortest solution // 36 ms // beats 100 %
- Author: cenkay
- Creation Date: Wed Jun 20 2018 17:46:37 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 08:53:55 GMT+0800 (Singapore Standard Time)

<p>
* Just check every character from right to left
* if character is digit, add to current numeric counter
* if character is ")", multiply big coefficient and add numeric counter to stack
* if character is "(", we won\'t multiply next elements with last numeric counter so remove last counter and divide big coefficient by last counter
* if character is alphabetic and uppercase, we have to add element and its coefficient to dictionary
* if character is alphabetic and lowercase, just add character to current element.
* return string by joining sorted key and value pairs in dictionary
```
class Solution:
    def countOfAtoms(self, formula):
        dic, coeff, stack, elem, cnt, i = collections.defaultdict(int), 1, [], "", 0, 0  
        for c in formula[::-1]:
            if c.isdigit():
                cnt += int(c) * (10 ** i)
                i += 1
            elif c == ")":
                stack.append(cnt)
                coeff *= cnt
                i = cnt = 0
            elif c == "(":
                coeff //= stack.pop()
                i = cnt = 0
            elif c.isupper():
                elem += c
                dic[elem[::-1]] += (cnt or 1) * coeff
                elem = ""
                i = cnt = 0
            elif c.islower():
                elem += c
        return "".join(k + str(v > 1 and v or "") for k, v in sorted(dic.items()))
```
</p>


### [C++] Recursive Parser
- Author: alexander
- Creation Date: Sun Nov 12 2017 16:54:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 12 2017 16:54:52 GMT+0800 (Singapore Standard Time)

<p>
```
/**
 * formula :
 *     unit[]
 * unit :
 *     atom (count)
 *     '(' formula ')' count
 * atom :
 *    upper (lower[])
 * count :
 *    digit[]
 * -----------------------------
 * upper : '[A-Z]'
 * lower : '[a-z]'
 * digit : '[0-9]'
 */
```
```
class Solution {
public:
    string countOfAtoms(string formula) {
        string output;
        const int n = formula.size();
        int i = 0;
        map<string, int> counts = parseFormula(formula, i);
        for (pair<string, int> p : counts) {
            output += p.first;
            if (p.second > 1) output += to_string(p.second);
        }
        return output;
    }

private:
    map<string, int> parseFormula(string& s, int& i) {
        map<string, int> counts;
        const int n = s.size();
        while (i < n && s[i] != ')') {
            map<string, int> cnts = parseUnit(s, i);
            merge(counts, cnts, 1);
        }
        return counts;
    }

    map<string, int> parseUnit(string& s, int& i) {
        map<string, int> counts;
        const int n = s.size();
        if (s[i] == '(') {
            map<string, int> cnts = parseFormula(s, ++i); // ++i for '('
            int digits = parseDigits(s, ++i); // ++i for ')'
            merge(counts, cnts, digits);
        }
        else {
            int i0 = i++; // first letter
            while (i < n && islower(s[i])) { i++; }
            string atom = s.substr(i0, i - i0);
            int digits = parseDigits(s, i);
            counts[atom] += digits;
        }
        return counts;
    }

    int parseDigits(string& s, int& i) {
        int i1 = i;
        while (i < s.size() && isdigit(s[i])) { i++; }
        int digits = i == i1 ? 1 : stoi(s.substr(i1, i - i1));
        return digits;
    }

    void merge(map<string, int>& counts, map<string, int>& cnts, int times) {
        for (pair<string, int> p : cnts) counts[p.first] += p.second * times;
    }
};
```
</p>


