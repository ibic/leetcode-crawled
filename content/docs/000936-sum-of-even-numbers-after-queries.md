---
title: "Sum of Even Numbers After Queries"
weight: 936
#id: "sum-of-even-numbers-after-queries"
---
## Description
<div class="description">
<p>We have an array <code>A</code> of integers, and an array <code>queries</code>&nbsp;of queries.</p>

<p>For the <code>i</code>-th&nbsp;query <code>val =&nbsp;queries[i][0], index&nbsp;= queries[i][1]</code>, we add <font face="monospace">val</font>&nbsp;to <code>A[index]</code>.&nbsp; Then, the answer to the <code>i</code>-th query is the sum of the even values of <code>A</code>.</p>

<p><em>(Here, the given <code>index = queries[i][1]</code> is a 0-based index, and each query permanently modifies the array <code>A</code>.)</em></p>

<p>Return the answer to all queries.&nbsp; Your <code>answer</code> array should have&nbsp;<code>answer[i]</code>&nbsp;as&nbsp;the answer to the <code>i</code>-th query.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[1,2,3,4]</span>, queries = <span id="example-input-1-2">[[1,0],[-3,1],[-4,0],[2,3]]</span>
<strong>Output: </strong><span id="example-output-1">[8,6,2,4]</span>
<strong>Explanation: </strong>
At the beginning, the array is [1,2,3,4].
After adding 1 to A[0], the array is [2,2,3,4], and the sum of even values is 2 + 2 + 4 = 8.
After adding -3 to A[1], the array is [2,-1,3,4], and the sum of even values is 2 + 4 = 6.
After adding -4 to A[0], the array is [-2,-1,3,4], and the sum of even values is -2 + 4 = 2.
After adding 2 to A[3], the array is [-2,-1,3,6], and the sum of even values is -2 + 6 = 4.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 10000</code></li>
	<li><code>-10000 &lt;= A[i] &lt;= 10000</code></li>
	<li><code>1 &lt;= queries.length &lt;= 10000</code></li>
	<li><code>-10000 &lt;= queries[i][0] &lt;= 10000</code></li>
	<li><code>0 &lt;= queries[i][1] &lt; A.length</code></li>
</ol>

</div>

## Tags
- Array (array)

## Companies
- Indeed - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Maintain Array Sum

**Intuition and Algorithm**

Let's try to maintain `S`, the sum of the array throughout one query operation.

When acting on an array element `A[index]`, the rest of the values of `A` remain the same.  Let's remove `A[index]` from `S` if it is even, then add `A[index] + val` back (if it is even.)

Here are some examples:

* If we have `A = [2,2,2,2,2]`, `S = 10`, and we do `A[0] += 4`: we will update `S -= 2`, then `S += 6`.  At the end, we will have `A = [6,2,2,2,2]` and `S = 14`.

* If we have `A = [1,2,2,2,2]`, `S = 8`, and we do `A[0] += 3`: we will skip updating `S` (since `A[0]` is odd), then `S += 4`.  At the end, we will have `A = [4,2,2,2,2]` and `S = 12`.

* If we have `A = [2,2,2,2,2]`, `S = 10` and we do `A[0] += 1`: we will update `S -= 2`, then skip updating `S` (since `A[0] + 1` is odd.)  At the end, we will have `A = [3,2,2,2,2]` and `S = 8`.

* If we have `A = [1,2,2,2,2]`, `S = 8` and we do `A[0] += 2`: we will skip updating `S` (since `A[0]` is odd), then skip updating `S` again (since `A[0] + 2` is odd.)  At the end, we will have `A = [3,2,2,2,2]` and `S = 8`.

These examples help illustrate that our algorithm actually maintains the value of `S` throughout each query operation.

<iframe src="https://leetcode.com/playground/c5zVhvET/shared" frameBorder="0" width="100%" height="395" name="c5zVhvET"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N+Q)$$, where $$N$$ is the length of `A` and $$Q$$ is the number of `queries`.

* Space Complexity:  $$O(Q)$$, though we only allocate $$O(1)$$ additional space.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] odd / even analysis, time O(max(m, n))
- Author: rock
- Creation Date: Sun Feb 03 2019 12:02:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 12 2020 12:00:43 GMT+0800 (Singapore Standard Time)

<p>
**Track sum of all even #s.**
There are 4 cases for odd / even property of A[k] and queries[i][0], where k = queries[i][1]:
1). **even** and odd, lose an even item in A; `sum` need to **deduct A[k]**;
2). **even** and even, get a **bigger even item in A**; `sum` need to add queries[i][0](same as **deduct A[k]** first then **add both**);
3). odd and odd, get a **bigger even item in A**; `sum` need to **add both**;
4). odd and even, no influence on even items in A;

Therefore, we can simplify the above as following procedure:

1. find `sum` of all even #s;
2. for each queries, check the item in A and after-added-up value, if
    a) the item in A is even, deduct it from `sum`; according to 1) & 2).
	b) after-added-up we have an even value, then add the new value to `sum`; according to 2) & 3).
	
```java
    public int[] sumEvenAfterQueries(int[] A, int[][] queries) {
        int sum = 0;
        for (int a : A) { 
            sum += (a % 2 == 0 ? a : 0); 
        }
        int[] ans = new int[queries.length];
        for (int i = 0; i < ans.length; ++i) {
            int idx = queries[i][1];
            if (A[idx] % 2 == 0) { 
                sum -= A[idx]; 
            }
            A[idx] += queries[i][0];
            if (A[idx] % 2 == 0) { 
                sum += A[idx]; 
            }
            ans[i] = sum;
        }
        return ans;    
    }
```
```python
    def sumEvenAfterQueries(self, A: List[int], queries: List[List[int]]) -> List[int]:
        s = sum(a for a in A if a % 2 == 0)
        ans = []
        for val, idx in queries: 
            if A[idx] % 2 == 0:
                s -= A[idx]
            A[idx] += val
            if A[idx] % 2 == 0:
                s += A[idx] 
            ans.append(s)
        return ans
```
**Analysis:**

**Time: O(max(m, n)), space: O(n)**, where m = A.length, n = queries.length.
</p>


### C++ O(n), track even sum
- Author: votrubac
- Creation Date: Sun Feb 03 2019 12:02:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 03 2019 12:02:05 GMT+0800 (Singapore Standard Time)

<p>
First, we calculate ```sum``` of even numbers. Then, for each query, if ```A[index]``` is even, we subtract its value from ```sum```. Then we add ```val``` to ```A[index]```. If ```A[index]``` is now even, we add its value to ```sum```.
```
vector<int> sumEvenAfterQueries(vector<int>& A, vector<vector<int>>& qs, vector<int> res = {}) {
  int sum = accumulate(begin(A), end(A), 0, [](int s, int a) { return s + (a % 2 == 0 ? a : 0); });
  for (auto &q : qs) {
    if (A[q[1]] % 2 == 0) sum -= A[q[1]];
    A[q[1]] += q[0];
    if (A[q[1]] % 2 == 0) sum += A[q[1]];
    res.push_back(sum);
  }
  return res;
}
```
</p>


### Easy to understand python solution
- Author: abhi1027
- Creation Date: Mon Feb 04 2019 00:30:53 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 04 2019 00:30:53 GMT+0800 (Singapore Standard Time)

<p>
1. Compute total as sum of all even values.
2. For each query, record prev value at query position. Modify the value at query position.
3. Adjust the total by subtracting prev if even and adding modified value if even.
```
def sumEvenAfterQueries(self, A: \'List[int]\', queries: \'List[List[int]]\') -> \'List[int]\':
        res=[]
        total=0
        for i in A:
            if i%2==0:
                total+=i
        
        for val,idx in queries:
            prev=A[idx]
            A[idx]+=val
            if prev%2==0:
                total-=prev
            if A[idx]%2==0:
                total+=A[idx]
            res.append(total)
            
        return res
```
</p>


