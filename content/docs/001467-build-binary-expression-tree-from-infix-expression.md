---
title: "Build Binary Expression Tree From Infix Expression"
weight: 1467
#id: "build-binary-expression-tree-from-infix-expression"
---
## Description
<div class="description">
<p>A <strong><a href="https://en.wikipedia.org/wiki/Binary_expression_tree" target="_blank">binary expression tree</a></strong> is a kind of binary tree used to represent arithmetic expressions. Each node of a binary expression tree has either zero or two children. Leaf nodes (nodes with 0 children) correspond to operands (numbers), and internal nodes (nodes with 2 children) correspond to the operators <code>&#39;+&#39;</code> (addition), <code>&#39;-&#39;</code> (subtraction), <code>&#39;*&#39;</code> (multiplication), and <code>&#39;/&#39;</code> (division).</p>

<p>For each internal node with operator <code>o</code>, the <a href="https://en.wikipedia.org/wiki/Infix_notation" target="_blank"><strong>infix expression</strong></a> that it represents is <code>(A o B)</code>, where <code>A</code> is the expression the left subtree represents and <code>B</code> is the expression the right subtree represents.</p>

<p>You are given a string <code>s</code>, an <strong>infix expression</strong> containing operands, the operators described above, and parentheses <code>&#39;(&#39;</code> and <code>&#39;)&#39;</code>.</p>

<p>Return <em>any valid&nbsp;<strong>binary expression tree</strong>,&nbsp;which its <strong><a href="https://en.wikipedia.org/wiki/Tree_traversal#In-order_(LNR)" target="_blank">in-order traversal</a></strong>&nbsp;reproduces&nbsp;</em><code>s</code>&nbsp;after omitting&nbsp;the parenthesis from it (see examples below)<em>.</em></p>

<p><strong>Please note that order of operations applies in </strong><code>s</code><strong>.</strong> That is, expressions in parentheses are evaluated first, and multiplication and division happen before addition and subtraction.</p>

<p>Operands must also appear in the <strong>same order</strong> in both <code>s</code>&nbsp;and the in-order traversal of the tree.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/10/02/ex1-2.png" style="width: 201px; height: 281px;" /></p>

<pre>
<strong>Input:</strong> s = &quot;2-3/(5*2)+1&quot;
<strong>Output:</strong> [+,-,1,2,/,null,null,null,null,3,*,null,null,5,2]
<strong>Explanation: </strong>The inorder traversal of the tree above is 2-3/5*2+1 which is the same as s without the parenthesis. The tree also produces the correct result and its operands are in the same order as they appear in s.
The tree below is also a valid binary expression tree with the same inorder traversal as s:
<img alt="" src="https://assets.leetcode.com/uploads/2020/10/02/ex1-1.png" style="width: 201px; height: 281px;" />
The third tree below however is not valid. Although it produces the same result and is equivalent to the above trees, its inorder traversal doesn&#39;t produce s and its operands are not in the same order as s.
<img alt="" src="https://assets.leetcode.com/uploads/2020/10/02/ex1-3.png" style="width: 281px; height: 281px;" />
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/10/02/ex1-4.png" style="width: 281px; height: 161px;" /></p>

<pre>
<strong>Input:</strong> s = &quot;3*4-2*5&quot;
<strong>Output:</strong> [-,*,*,3,4,2,5]
<strong>Explanation: </strong>The tree above is the only valid tree whose inorder traversal produces s.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1+2+3+4+5&quot;
<strong>Output:</strong> [+,+,5,+,4,null,null,+,3,null,null,1,2]
<strong>Explanation: </strong>The tree [+,+,5,+,+,null,null,1,2,3,4] is also one of many other valid trees.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10<sup>5</sup></code></li>
	<li><code>s</code> consists of digits and the characters <code>&#39;+&#39;</code>, <code>&#39;-&#39;</code>, <code>&#39;*&#39;</code>, <code>&#39;/&#39;</code>, <code>&#39;(&#39;</code>, and <code>&#39;)&#39;</code>.</li>
	<li>Operands in <code>s</code> are <strong>exactly</strong> 1 digit.</li>
	<li>It is guaranteed that <code>s</code> is a valid expression.</li>
</ul>

</div>

## Tags
- String (string)
- Tree (tree)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Standard parser implementation
- Author: forestgump
- Creation Date: Sat Sep 26 2020 10:54:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 26 2020 10:55:15 GMT+0800 (Singapore Standard Time)

<p>
Not a terribly interesting problem. If you remember from the dusty memories of compiler class, this calls for a standard expression parser implementation where you call parse-functions on those with the lowest precendence and recursively invoke parse-functions of things with higher precendence.

```
class Solution:
    # Standard parser implementation based on this BNF
    #   s := expression
    #   expression := term | term { [+,-] term] }
    #   term := factor | factor { [*,/] factor] }
    #   factor :== digit | \'(\' expression \')\'
    #   digit := [0..9]
    
    def expTree(self, s: str) -> \'Node\':
        tokens = collections.deque(list(s))
        return self.parse_expression(tokens)

    def parse_expression(self, tokens):
        lhs = self.parse_term(tokens)
        while len(tokens) > 0 and tokens[0] in [\'+\', \'-\']:
            op = tokens.popleft()
            rhs = self.parse_term(tokens)
            lhs = Node(val=op, left=lhs, right=rhs)
        return lhs
    
    def parse_term(self, tokens):
        lhs = self.parse_factor(tokens)
        while len(tokens) > 0 and tokens[0] in [\'*\', \'/\']:
            op = tokens.popleft()
            rhs = self.parse_factor(tokens)
            lhs = Node(val=op, left=lhs, right=rhs)
        return lhs

    def parse_factor(self, tokens):
        if tokens[0] == \'(\':
            tokens.popleft() # consume \'(\'
            node = self.parse_expression(tokens)
            tokens.popleft() # consume \')\'
            return node
        else:
            # Single operand
            token = tokens.popleft()
            return Node(val=token)
```
</p>


### Recursion + 2 Passes
- Author: votrubac
- Creation Date: Thu Sep 24 2020 10:11:48 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 24 2020 10:16:40 GMT+0800 (Singapore Standard Time)

<p>
The tricky part of this question is the data structure. I decided I would convert all characters to nodes, and store them in a list. 

1. Conversion with recursion:
	- Convert all characters into nodes and store them in list `l`.
	- **Recursion:** if we detect a group `(...)`, call `expTree` recursively and insert the returned tree root into `l`.
2. Pass 1 for elements in `l`:
	- For `*` and `/` leaf nodes, \'adopt\' left and right neighbors to be its children (neighbors are removed from the list).
3. Pass 2 for the remaining elements in `l`:
	- Do the same as in pass 1 for `+` and `-` leaf nodes.

After this, we should have one element remaining in `l` - our root node.

```cpp
list<Node*> op(const list<Node*> &l, char op1, char op2) {
    list<Node*> l1;
    for (auto it = begin(l); it != end(l); ++it) {
        auto o = *it;
        if (o->left == nullptr && (o->val == op1 || o->val == op2)) {
            o->left = l1.back();
            o->right = *next(it);
            l1.back() = o;
            ++it;
        }
        else
            l1.push_back(o);
    }        
    return l1;
}
Node* expTree(string s) {
    list<Node*> l;
    for (auto i = 0; i < s.size(); ++i) {
        if (s[i] == \'(\') {
            int j = i + 1;
            for (int bal = 1; bal > 0; ++j)
                bal += s[j] == \')\' ? -1 : s[j] == \'(\' ? 1 : 0;
            l.push_back(expTree(s.substr(i + 1, j - i - 2)));
            i = j - 1;
        }
        else
            l.push_back(new Node(s[i]));
    }
    return op(op(l, \'*\', \'/\'), \'+\', \'-\').front();
}
```
</p>


