---
title: "Check If String Is Transformable With Substring Sort Operations"
weight: 1447
#id: "check-if-string-is-transformable-with-substring-sort-operations"
---
## Description
<div class="description">
<p>Given two strings&nbsp;<code>s</code> and <code>t</code>, you want to transform string&nbsp;<code>s</code> into string&nbsp;<code>t</code> using the following&nbsp;operation any number of times:</p>

<ul>
	<li>Choose a <strong>non-empty</strong> substring in&nbsp;<code>s</code>&nbsp;and sort it in-place&nbsp;so the characters are in&nbsp;<strong>ascending order</strong>.</li>
</ul>

<p>For example, applying the operation on the underlined substring in&nbsp;<code>&quot;1<u>4234</u>&quot;</code>&nbsp;results in <code>&quot;1<u>2344</u>&quot;</code>.</p>

<p>Return <code>true</code> if <em>it is possible to transform string <code>s</code>&nbsp;into string <code>t</code></em>. Otherwise,&nbsp;return <code>false</code>.</p>

<p>A <strong>substring</strong>&nbsp;is a contiguous sequence of characters within a string.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;84532&quot;, t = &quot;34852&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong> You can transform s into t using the following sort operations:
&quot;84<u>53</u>2&quot; (from index 2 to 3) -&gt; &quot;84<u>35</u>2&quot;
&quot;<u>843</u>52&quot; (from index 0 to 2) -&gt; &quot;<u>348</u>52&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;34521&quot;, t = &quot;23415&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong> You can transform s into t using the following sort operations:
&quot;<u>3452</u>1&quot; -&gt; &quot;<u>2345</u>1&quot;
&quot;234<u>51</u>&quot; -&gt; &quot;234<u>15</u>&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;12345&quot;, t = &quot;12435&quot;
<strong>Output:</strong> false
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1&quot;, t = &quot;2&quot;
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>s.length == t.length</code></li>
	<li><code>1 &lt;= s.length &lt;= 10<sup>5</sup></code></li>
	<li><code>s</code> and <code>t</code>&nbsp;only contain digits from <code>&#39;0&#39;</code> to <code>&#39;9&#39;</code>.</li>
</ul>

</div>

## Tags
- String (string)
- Greedy (greedy)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java/Python O(n)
- Author: votrubac
- Creation Date: Sun Sep 13 2020 12:00:40 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 14 2020 15:01:18 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**
If `ch[i] > ch[j]`, we can swap these characters. In other words, we can move a character freely to the left, until it hits a smaller character, e.g.:

"023**1**" > "02**1**3" > "0**1**23"

So, we do not need to sort anything, we just need to check if we can move required characters to the left to get the target string.

> Note: we can also process the string right-to-left and move larger numbers right. In that case, we can just pop used indexes instead tracking them in a separate array `pos`. I though about it, but it appeared a bit harder to get right during the contest. If interested, check the solution by [0xFFFFFFFF](https://leetcode.com/0xFFFFFFFF/) in the comments below.

**Algorithm**
Collect indexes of all characters `0`-`9` of the source strings in `idx`. For each characters, we track which indexes we have used in `pos`.

For each character `ch` in the target string, check if we have it in `idx`. If so, verify that there are no smaller characters in front of it. To do that, we check the current idexes of all characters less than `ch`.

If the character can be moved, mark its index as used by advancing `pos[ch]`. 

**C++**
```cpp
bool isTransformable(string s, string t) {
    vector<vector<int>> idx(10);
    vector<int> pos(10);
    for (int i = 0; i < s.size(); ++i)
        idx[s[i] - \'0\'].push_back(i);
    for (auto ch : t) {
        int d = ch - \'0\';
        if (pos[d] >= idx[d].size())
            return false;
        for (auto i = 0; i < d; ++i)
            if (pos[i] < idx[i].size() && idx[i][pos[i]] < idx[d][pos[d]])
                return false;
        ++pos[d];
    }
    return true;
}
```

**Java**
```java
public boolean isTransformable(String s, String t) {
    ArrayList<Integer> idx[] = new ArrayList[10];
    int pos[] = new int[10];
    for (int i = 0; i <= 9; ++i)
        idx[i] = new ArrayList<Integer>();
    for (int i = 0; i < s.length(); ++i)
        idx[s.charAt(i) - \'0\'].add(i);
    for (int i = 0; i < t.length(); ++i) {
        int d = t.charAt(i) - \'0\';
        if (pos[d] >= idx[d].size())
            return false;
        for (int j = 0; j < d; ++j)
            if (pos[j] < idx[j].size() && idx[j].get(pos[j]) < idx[d].get(pos[d]))
                return false;        
        ++pos[d];
    }
    return true;
}
```
**Python**
```python
def isTransformable(self, s: str, t: str) -> bool:
	idx, pos = [[] for _ in range(10)], [0 for _ in range(10)]
	for i, ch in enumerate(s):
		idx[int(ch)].append(i)
	for ch in t:
		d = int(ch)
		if pos[d] >= len(idx[d]):
			return False
		for i in range(d):
			if pos[i] < len(idx[i]) and idx[i][pos[i]] < idx[d][pos[d]]:
				return False
		pos[d] += 1
	return True
```
**Complexity Analysis**
- Time: O(n) - we go through each string once.
- Memory: O(n) - to hold indices from the source string.
</p>


### Python [backward + forward pass with stack]
- Author: GSAN
- Creation Date: Sun Sep 13 2020 12:02:48 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 14 2020 08:04:03 GMT+0800 (Singapore Standard Time)

<p>
Very tricky question!

1. Probably the hardest part is to realize that: For every digit in `t` you find the first occurrence in `s`. Call that current digit as `key`. You need to make sure in `s`, no digit smaller than `key` appears to the left.
2. As we check one-by-one, we need to remove the digits we considered from `s`. In order to do this operation constant time, store all indices in reversed form using `stack` in the first loop (backward pass over `s`). Then use the `.pop()` operation in the second loop (forward pass over `t`).

```
from collections import defaultdict
class Solution:
    def isTransformable(self, s: str, t: str) -> bool:
        #places: this stores the indices of every digit from 0 to 9
        places = defaultdict(list)
        for i in reversed(range(len(s))):
            key = int(s[i])
            places[key].append(i)
        
        for e in t: #we loop over t and check every digit
            key = int(e) #current digit
            if not places[key]: #digit is not in s, return False
                return False 
            i = places[key][-1] #location of current digit
            for j in range(key): #only loop over digits smaller than current digit
                if places[j] and places[j][-1] < i: #there is a digit smaller than current digit, return false
                    return False
            places[key].pop()
        return True
```
</p>


### intuitive proof of why:It's essentially bubble sort,
- Author: alanmiller
- Creation Date: Sun Sep 13 2020 13:14:45 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 14 2020 00:28:30 GMT+0800 (Singapore Standard Time)

<p>
I will quickly explain why these methods work, while some don\'t.

First the operation of subarray sorting is the same as the basic operation in bubble sort, i.e. swap the two adjacent elements x, y if x > y.

Proof: if we can do bubble sort operation, then we can do subarray sorting as in bubble sort. Conversely, just sorting the adjacent 2 elements x, y will reduce to the bubble sort operation.

Please stop reading here, the following needs to be modified. See why here:
https://leetcode.com/problems/check-if-string-is-transformable-with-substring-sort-operations/discuss/845059/More-methods-are-wrong-including-the-No1-in-the-contest%3A-231-132
------------------------------
Now for each string we need to count: for each character: how many elements on its left are smaller than itself. We want each corresponding number for s (dic1 in my codes) is smaller than or equal to that for t (dic2 in my codes). This (together with Counter(s) == Counter(t)) is the equivalent condition for this problem.

Because if so, you can start with the largest elements, move them up to the correct positions, without altering other elements values in dic1 and dic2. Once the largest numbers are done, then proceed to the second largest, etc. The converse is obvious, these number in s can only get bigger by the operations.

The problem with the "inversion method" is it counts the inversions with every numbers from 0-9, but ignoring different occurances of the same number, that\'s how I come up with 1331 and 3113. Both of them have two inversions on (1,3), but each 3 in 1331 contribute one inversion, while the second 3 in 3113 contributes two.

As a general advice, for a problem like that, always start with some necessary conditions, find as many conditions as you can that will definitely False,  and just submit it, maybe it is sufficient too. It took me 20 minutes in the contest to prove it though...

```
class Solution:
    def isTransformable(self, s: str, t: str) -> bool:
        c1 = collections.Counter()
        c2 = collections.Counter()

        
        dic1 = collections.defaultdict(list)
        dic2 = collections.defaultdict(list)
        for a, b in zip(s, t):
            a = int(a)
            b = int(b)
            c1[a] += 1
            c2[b] += 1
            dic1[a].append(sum(c1[x] for x in range(a)))
            dic2[b].append(sum(c2[x] for x in range(b)))
        
        if c1 != c2:
            return False
        for x in range(10):
            for a, b in zip(dic1[x], dic2[x]):
                if a > b:
                    return False
        return True
```
</p>


