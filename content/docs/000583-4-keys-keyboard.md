---
title: "4 Keys Keyboard"
weight: 583
#id: "4-keys-keyboard"
---
## Description
<div class="description">
<p>Imagine you have a special keyboard with the following keys: </p>
<p><code>Key 1: (A)</code>:  Print one 'A' on screen.</p>
<p><code>Key 2: (Ctrl-A)</code>: Select the whole screen.</p>
<p><code>Key 3: (Ctrl-C)</code>: Copy selection to buffer.</p>
<p><code>Key 4: (Ctrl-V)</code>: Print buffer on screen appending it after what has already been printed. </p>



<p>Now, you can only press the keyboard for <b>N</b> times (with the above four keys), find out the maximum numbers of 'A' you can print on screen.</p>


<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> N = 3
<b>Output:</b> 3
<b>Explanation:</b> 
We can at most get 3 A's on screen by pressing following key sequence:
A, A, A
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> N = 7
<b>Output:</b> 9
<b>Explanation:</b> 
We can at most get 9 A's on screen by pressing following key sequence:
A, A, A, Ctrl A, Ctrl C, Ctrl V, Ctrl V
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>1 <= N <= 50 </li>
<li>Answers will be in the range of 32-bit signed integer.</li>
</ol>
</p>

</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)
- Greedy (greedy)

## Companies
- Google - 0 (taggedByAdmin: true)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---

#### Approach Framework

**Explanation**

We either press 'A', or press 'CTRL+A', 'CTRL+C', and some number of 'CTRL+V's.  Thus, in the context of making `N` keypresses to write the letter 'A' `M` times, there are only two types of moves:

* Add (`1` keypress):  Add `1` to `M`.
* Multiply (`k+1` keypresses):  Multiply `M` by `k`, where `k >= 2`.

In the following explanations, we will reference these as moves.

---
#### Approach #1: Dynamic Programming [Accepted]

**Intuition and Algorithm**

Say `best[k]` is the largest number of written 'A's possible after `k` keypresses.

If the last move in some optimal solution of `k` keypresses was adding, then `best[k] = best[k-1] + 1`.

Otherwise, if the last move was multiplying, then we multiplied by `x`, and `best[k-(x+1)] = best[k-(x+1)] * x` for some `x < k-1`.

Taking the best of these candidates lets us find `best[k]` in terms of previous `best[j]`, when `j < k`.

<iframe src="https://leetcode.com/playground/MD4Gt5Gu/shared" frameBorder="0" width="100%" height="242" name="MD4Gt5Gu"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$.  We have two nested for-loops, each of which do $$O(N)$$ work.

* Space Complexity: $$O(N)$$, the size of `best`.

---
#### Approach #2: Optimized Dynamic Programming [Accepted]

**Intuition**

If we multiply by `2N`, paying a cost of `2N+1`, we could instead multiply by `N` then `2`, paying `N+4`.  When `N >= 3`, we don't pay more by doing it the second way.

Similarly, if we are to multiply by `2N+1` paying `2N+2`, we could instead multiply by `N+1` then `2`, paying `N+5`.  Again, when `N >= 3`, we don't pay more doing it the second way.

Thus, we never multiply by more than `5`.

**Algorithm**

Our approach is the same as *Approach #1*, except we do not consider multiplying by more than 5 in our inner loop.  For brevity, we have omitted this solution.

**Complexity Analysis**

* Time Complexity: $$O(N)$$.  We have two nested for-loops, but the inner loop does $$O(1)$$ work.

* Space Complexity: $$O(N)$$, the size of `best`.

---
#### Approach #3: Mathematical [Accepted]

**Explanation**

As in *Approach #2*, we never multiply by more than 5.

When `N` is arbitrarily large, the long run behavior of multiplying by `k` repeatedly is to get to the value $$k^{\frac{N}{k+1}}$$.  Analyzing the function $$k^{\frac{1}{k+1}}$$ at values $$k = 2, 3, 4, 5$$, it attains a peak at $$k = 4$$.  Thus, we should expect that *eventually*, `best[K] = best[K-5] * 4`.

Now, we need to make a few more deductions.

* We never add after multiplying: if we add `c` after multiplying by `k`, we should instead multiply by `k+c`.

* We never add after 5: If we add `1` then multiply by `k` to get to `(x+1) * k = xk + k`, we could instead multiply by `k+1` to get to `xk + x`.  Since `k <= 5`, we must have `x <= 5` for our additions to not be dominated.

* The number of multiplications by 2, 3, or 5 is bounded.

  * Every time we've multiplied by 2 two times, we prefer to multiply by 4 once for less cost. (4^1 for a cost of 5, vs 2^2 for a cost of 6.)
  * Every time we've multiplied by 3 five times, we prefer to multiply by 4 four times for the same cost but a larger result. (4^4 > 3^5, and cost is 20.)
  * Every time we've multiplied by 5 five times, we prefer to multiply by 4 six times for the same cost but a larger result. (4^6 > 5^5, and cost is 30.)

Together, this shows there are at most 5 additions and 9 multiplications by a number that isn't 4.

We can find the first 14 operations on 1 by hand: `1, 2, 3, 4, 5, 6, 9, 12, 16, 20, 27, 36, 48, 64, 81`. After that, every subsequent number is achieved by multiplying by 4: ie., `best[K] = best[K-5] * 4`.

<iframe src="https://leetcode.com/playground/SY7cm8tD/shared" frameBorder="0" width="100%" height="191" name="SY7cm8tD"></iframe>

**Complexity Analysis**

* Time and Space Complexity: $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 4 lines recursion, with step-by-step explanation to derive DP
- Author: yuxiangmusic
- Creation Date: Sun Jul 30 2017 12:32:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 15:17:39 GMT+0800 (Singapore Standard Time)

<p>
We use `i` steps to reach `maxA(i)` then use the remaining `n - i` steps to reach `n - i - 1` copies of `maxA(i)`

For example:
A, A, A, Ctrl A, Ctrl C, Ctrl V, Ctrl V
Here we have `n = 7` and we used `i = 3` steps to reach `AAA`
Then we use the remaining `n - i = 4` steps: Ctrl A, Ctrl C, Ctrl V, Ctrl V, to reach `n - i - 1 = 3` copies of `AAA`

We either don't make copies at all, in which case the answer is just `n`, or if we want to make copies, we need to have 3 steps reserved for Ctrl A, Ctrl C, Ctrl V so `i` can be at most `n - 3`

```
    public int maxA(int n) {
        int max = n;
        for (int i = 1; i <= n - 3; i++)
            max = Math.max(max, maxA(i) * (n - i - 1));
        return max;
    }
```

Now making it a DP where `dp[i]` is the solution to sub-problem `maxA(i)`

```
    public int maxA(int n) {
        int[] dp = new int[n + 1];
        for (int i = 0; i <= n; i++) {
            dp[i] = i;
            for (int j = 1; j <= i - 3; j++)
                dp[i] = Math.max(dp[i], dp[j] * (i - j - 1));
        }
        return dp[n];
    }
```
</p>


### Two Java DP solution O(n^2) and O(n)
- Author: xiyunyue2
- Creation Date: Sun Jul 30 2017 12:52:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 30 2017 12:52:32 GMT+0800 (Singapore Standard Time)

<p>
dp[i] = max(dp[i], dp[i-j]*(j-1)) j in [3, i)
```
public int maxA(int N) {
        int[] dp = new int[N+1];
        for(int i=1;i<=N;i++){
            dp[i] = i;
            for(int j=3;j<i;j++){
                dp[i] = Math.max(dp[i], dp[i-j] * (j-1));
            }
        }
        return dp[N];
    }
```
This one is O(n), inspired by paulalexis58. We don't have to run the second loop between [3,i). Instead, we only need to recalculate the last two steps. It's interesting to observe that dp[i - 4] * 3 and dp[i - 5] * 4 always the largest number in the series. Welcome to add your mathematics proof here.
```
public int maxA(int N) {
    if (N <= 6)  return N;
    int[] dp = new int[N + 1];
    for (int i = 1; i <= 6; i++) {
      dp[i] = i;
    }
    for (int i = 7; i <= N; i++) {
      dp[i] = Math.max(dp[i - 4] * 3, dp[i - 5] * 4);
      // dp[i] = Math.max(dp[i - 4] * 3, Math.max(dp[i - 5] * 4, dp[i - 6] * 5));
    }
    return dp[N];
  }
```
</p>


### Mathematical proof of the O(N) solution
- Author: Spin0za
- Creation Date: Mon Jul 31 2017 02:36:00 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 31 2017 02:36:00 GMT+0800 (Singapore Standard Time)

<p>
The reason we could use ```dp[i] = max(dp[i-4]*3, dp[i-5]*4)``` instead of  ```dp[i] = max(dp[i-3]*2, dp[i-4]*3, dp[i-5]*4, dp[i-6]*5, ...)``` is the following property:

When ```i >= 6```, we have ```5/4 * dp[i] <= dp[i+1] <= 3/2 * dp[i]```.

We prove it using strong mathematical induction. Base case is trivial: ```dp[6] = 6, dp[7] = 9, dp[8] = 12```.
Now assume ```5/4 * dp[i] <= dp[i+1] <= 3/2 * dp[i]``` for all ```i >= 6 && i < n```, we prove ```5/4 * dp[n] <= dp[n+1] <= 3/2 * dp[n]```. By the given DP formula, ```dp[n+1] = max(dp[n-2]*2, dp[n-3]*3, dp[n-4]*4, dp[n-5]*5, ...)```. We have ```dp[n-3]*3 >= dp[n-2]*2``` because ```dp[i+1] <= 3/2 * dp[i]``` holds when ```i = n-3```. Similarly, we have ```dp[n-4]*4 >= dp[n-5]*5``` because ```dp[i+1] >= 5/4 * dp[i]``` holds when ```i = n-5```.
Now the key part: for all ```k >= 5 && k < n```, we have ```dp[n-4]*4 >= dp[n-k]*k``` i.e. ```dp[n-4] >= k/4 * dp[n-k]``` because ```dp[n-4] >= 5/4 * dp[n-5] >= (5/4)^2 * dp[n-6] >= ... >= (5/4)^j * dp[n-j-4]```. Now let ```j = k-4```, we get ```dp[n-4] >= (5/4)^(k-4) * dp[n-k] = (1 + 1/4)^(k-4) * dp[n-k] >= (1 + 1/4 * (k - 4)) * dp[n-k] = k/4 * dp[n-k]```, by the Bernoulli inequality. Proof complete.




(To be ultimately rigorous, I actually have to prove ```dp[n-5] >= k/5 * dp[n-k]``` and ```dp[n-4] >= 5/4 * dp[n-5]``` separately, due to the ```i >= 6``` limit in the property. But I'd rather keep the proof simpler.)
</p>


