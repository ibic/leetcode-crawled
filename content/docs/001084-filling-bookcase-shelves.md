---
title: "Filling Bookcase Shelves"
weight: 1084
#id: "filling-bookcase-shelves"
---
## Description
<div class="description">
<p>We have a sequence of <code>books</code>: the <code>i</code>-th book has thickness <code>books[i][0]</code> and height <code>books[i][1]</code>.</p>

<p>We want to place these books <strong>in order</strong>&nbsp;onto bookcase shelves that have total width <code>shelf_width</code>.</p>

<p>We choose&nbsp;some of the books to place on this shelf (such that the sum of their thickness is <code>&lt;= shelf_width</code>), then build another level of shelf of the bookcase so that the total height of the bookcase has increased by the maximum height of the books we just put down.&nbsp; We repeat this process until there are no more books to place.</p>

<p>Note again that at each step of the above&nbsp;process, <u>the order of the books we place is the same order as the given sequence of books</u>.&nbsp; For example, if we have an ordered list of 5&nbsp;books, we might place the first and second book onto the first shelf, the third book on the second shelf, and the fourth and fifth book on the last shelf.</p>

<p>Return the minimum possible height that the total bookshelf can be after placing shelves in this manner.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/06/24/shelves.png" style="width: 250px; height: 370px;" />
<pre>
<strong>Input:</strong> books = [[1,1],[2,3],[2,3],[1,1],[1,1],[1,1],[1,2]], shelf_width = 4
<strong>Output:</strong> 6
<strong>Explanation:</strong>
The sum of the heights of the 3 shelves are 1 + 3 + 2 = 6.
Notice that book number 2 does not have to be on the first shelf.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= books.length &lt;= 1000</code></li>
	<li><code>1 &lt;= books[i][0] &lt;= shelf_width &lt;= 1000</code></li>
	<li><code>1 &lt;= books[i][1] &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Bloomberg - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java DP solution
- Author: tankztc
- Creation Date: Sun Jun 30 2019 12:05:34 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 08 2019 10:40:30 GMT+0800 (Singapore Standard Time)

<p>
`dp[i]`: the min height for placing first books `i - 1` on shelves
For `dp[i+1]`, 
either place book `i` on a new shelve => `dp[i] + height[i]`,
or grab previous books together with book `i` and move to next level together, utlitzing the sub problem `dp[j]` => `min(dp[j] + max(height[j+1] .. height[i]))`, where `sum(width[j+1] + ... + sum(width[i]) <= shelve_width`

```
class Solution {
    public int minHeightShelves(int[][] books, int shelf_width) {
        int[] dp = new int[books.length + 1];
        
        dp[0] = 0;
        
        for (int i = 1; i <= books.length; ++i) {
            int width = books[i-1][0];
            int height = books[i-1][1];
            dp[i] = dp[i-1] + height;
            for (int j = i - 1; j > 0 && width + books[j-1][0] <= shelf_width; --j) {
                height = Math.max(height, books[j-1][1]);
                width += books[j-1][0];
                dp[i] = Math.min(dp[i], dp[j-1] + height);
            }
        }
        return dp[books.length];
    }
}
```
</p>


### C++ 4 lines DFS + Memo
- Author: votrubac
- Creation Date: Sun Jun 30 2019 12:03:49 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 03 2019 04:01:57 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
For each book, we need to explore two cases: add to the shelf and start a new shelf. We can use DFS and use memoisation to prune (you\'ll get TLE without the memoisation). 
# Solution
The basic DFS logic is straightforward, but for me it was tricky to come up with a memoisation strategy: for each book (```i```), and for each position on the shelf (```w```) in the shelf, store the minimum height for the remaining books.

In other words, the position ```m[i][w]``` contains the minimum height for books ```[i...n]```, so we do not need to care what the current height is.
```
int m[1001][1001] = {};
int minHeightShelves(vector<vector<int>>& b, int max_w, int i = 0, int w = 0, int h = 0) {
  if (i >= b.size()) return h;
  if (m[i][w] != 0) return m[i][w];
  return m[i][w] = min(h + minHeightShelves(b, max_w, i + 1, b[i][0], b[i][1]),
    w + b[i][0] > max_w ? INT_MAX : minHeightShelves(b, max_w, i + 1, w + b[i][0], max(h, b[i][1])));
}
```
# Complexity Analysis
Runtime: 
- Without memoisation, *O(2 ^ n)*, where n is the number of books.
- With memoisation, *O(n * m)*, where m is the maximum width.

Memory: *O(n * m)*, where m is the maximum width.
# Bottom-Up Solution
```
int minHeightShelves(vector<vector<int>>& b, int sh_w) {
  vector<int> dp(b.size() + 1);
  for (int p = 1; p <= b.size(); ++p) {
    for (int i = p, w = 0, h = 0; i > 0 && w + b[i - 1][0] <= sh_w; --i) {
      w += b[i - 1][0];
      h = max(h, b[i - 1][1]);
      dp[p] = min(dp[p] == 0 ? INT_MAX : dp[p], dp[i - 1] + h);
    }
  }
  return dp.back();
}
```
</p>


### simple Python DP solution
- Author: otoc
- Creation Date: Sun Jun 30 2019 12:50:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 30 2019 12:50:55 GMT+0800 (Singapore Standard Time)

<p>
```
    def minHeightShelves(self, books: List[List[int]], shelf_width: int) -> int:
        n = len(books)
        dp = [float(\'inf\') for _ in range(n + 1)]
        dp[0] = 0
        for i in range(1, n + 1):
            max_width = shelf_width
            max_height = 0
            j = i - 1
            while j >= 0 and max_width - books[j][0] >= 0:
                max_width -= books[j][0]
                max_height = max(max_height, books[j][1])
                dp[i] = min(dp[i], dp[j] + max_height)
                j -= 1
        return dp[n]
```
</p>


