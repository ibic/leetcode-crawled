---
title: "Find Two Non-overlapping Sub-arrays Each With Target Sum"
weight: 1346
#id: "find-two-non-overlapping-sub-arrays-each-with-target-sum"
---
## Description
<div class="description">
<p>Given an array of integers <code>arr</code> and an integer <code>target</code>.</p>

<p>You have to find <strong>two non-overlapping sub-arrays</strong> of <code>arr</code> each with sum equal <code>target</code>. There can be multiple answers so you have to find an answer where the sum of the lengths of the two sub-arrays is <strong>minimum</strong>.</p>

<p>Return <em>the minimum sum of the lengths</em> of the two required sub-arrays, or return <em><strong>-1</strong></em> if you cannot&nbsp;find such two sub-arrays.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [3,2,2,4,3], target = 3
<strong>Output:</strong> 2
<strong>Explanation:</strong> Only two sub-arrays have sum = 3 ([3] and [3]). The sum of their lengths is 2.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [7,3,4,7], target = 7
<strong>Output:</strong> 2
<strong>Explanation:</strong> Although we have three non-overlapping sub-arrays of sum = 7 ([7], [3,4] and [7]), but we will choose the first and third sub-arrays as the sum of their lengths is 2.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [4,3,2,6,2,3,4], target = 6
<strong>Output:</strong> -1
<strong>Explanation:</strong> We have only one sub-array of sum = 6.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [5,5,4,4,5], target = 3
<strong>Output:</strong> -1
<strong>Explanation:</strong> We cannot find a sub-array of sum = 3.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> arr = [3,1,1,1,5,1,2,1], target = 3
<strong>Output:</strong> 3
<strong>Explanation:</strong> Note that sub-arrays [1,2] and [2,1] cannot be an answer because they overlap.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 1000</code></li>
	<li><code>1 &lt;= target &lt;= 10^8</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 11 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [JAVA] O(N) Time, Two Pass Solution using HashMap.
- Author: pramitb
- Creation Date: Sun Jun 14 2020 00:31:28 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 17 2020 15:11:58 GMT+0800 (Singapore Standard Time)

<p>
Concept: First traverse through the array once and store the (key,value) pair as (sum(arr[0:i+1]),i) for 0<=i<size of arr. Put, (0,-1) in hashmap as default. Now traverse through the array again, and for every i, find the minimum value of length of sub-array on the left or starting with i whose value is equal to target. Find another sub-array starting with i+1, whose sum is target. Update the result with the minimum value of the sum of both the sub-array. This is possible because all values are positive and the value of sum is strictly increasing.
```
class Solution {
    public int minSumOfLengths(int[] arr, int target) {
        HashMap<Integer,Integer> hmap=new HashMap<>();
        int sum=0,lsize=Integer.MAX_VALUE,result=Integer.MAX_VALUE;
        hmap.put(0,-1);
        for(int i=0;i<arr.length;i++){
            sum+=arr[i];
            hmap.put(sum,i); // stores key as sum upto index i, and value as i.
        }
        sum=0;
        for(int i=0;i<arr.length;i++){
            sum+=arr[i];
            if(hmap.get(sum-target)!=null){
                lsize=Math.min(lsize,i-hmap.get(sum-target));      // stores minimum length of sub-array ending with index<= i with sum target. This ensures non- overlapping property.
            }
			//hmap.get(sum+target) searches for any sub-array starting with index i+1 with sum target.
            if(hmap.get(sum+target)!=null&&lsize<Integer.MAX_VALUE){
                result=Math.min(result,hmap.get(sum+target)-i+lsize); // updates the result only if both left and right sub-array exists.
            }
        }
        return result==Integer.MAX_VALUE?-1:result;
    }
}
```
</p>


### [Python] One-pass, prefix-sum, O(n)
- Author: chuan-chih
- Creation Date: Sun Jun 14 2020 00:24:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 14 2020 02:49:12 GMT+0800 (Singapore Standard Time)

<p>
Keep track of the running prefix-sum and the length of the shortest sub-array that sums to the target up to that point (`best_till` in my solution).
Each time we find another such sub-array, look up that length value at the index right before it starts.
```
class Solution:
    def minSumOfLengths(self, arr: List[int], target: int) -> int:
        prefix = {0: -1}
        best_till = [math.inf] * len(arr)
        ans = best = math.inf
        for i, curr in enumerate(itertools.accumulate(arr)):
            if curr - target in prefix:
                end = prefix[curr - target]
                if end > -1:
                    ans = min(ans, i - end + best_till[end])
                best = min(best, i - end)
            best_till[i] = best
            prefix[curr] = i
        return -1 if ans == math.inf else ans
```
In the contest I misread sub-array as subsequence so ran out of time \uD83D\uDE22
</p>


### JAVA | Sliding window with only one array | No HasMap
- Author: ping_pong
- Creation Date: Sun Jun 14 2020 08:54:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 21 2020 08:16:41 GMT+0800 (Singapore Standard Time)

<p>
I see, mot of the people confused this with 560. `Subarray Sum Equals K` off course that would give correct answer, no doubt about that. But I think we can leverage the constraint here, which says `a[i] >= 1` due to this constraint we don\'t need a `HashMap` here. Problem can be solved by vanilla sliding window.

 ```
    public int minSumOfLengths(int[] arr, int target) {
        int n = arr.length;
        int best[] = new int[n];
        Arrays.fill(best, Integer.MAX_VALUE);
        int sum = 0, start = 0, ans = Integer.MAX_VALUE, bestSoFar = Integer.MAX_VALUE;
        for(int i = 0; i < n; i++){
            sum += arr[i];
            while(sum > target){
                sum -= arr[start];
                start++;
            }
            if(sum == target){
                if(start > 0 && best[start - 1] != Integer.MAX_VALUE){
                    ans = min(ans, best[start - 1] + i - start + 1);
                }
                bestSoFar = min(bestSoFar, i - start + 1);
            }
            best[i] = bestSoFar;
        }
        return ans == Integer.MAX_VALUE ? -1 : ans;
    }
```
</p>


