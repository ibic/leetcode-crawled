---
title: "Find Root of N-Ary Tree"
weight: 1398
#id: "find-root-of-n-ary-tree"
---
## Description
<div class="description">
<p>Given all the nodes of an <a href="https://leetcode.com/articles/introduction-to-n-ary-trees/">N-ary tree</a> as an array &nbsp;<code>Node[] tree</code>&nbsp;where each&nbsp;node has a&nbsp;<strong>unique value</strong>.</p>

<p>Find and return the <strong>root</strong> of the N-ary tree.</p>

<p>&nbsp;</p>

<p><strong>Follow up:</strong></p>

<p>Could you solve this problem in constant space complexity with a linear time algorithm?</p>

<p>&nbsp;</p>

<p><em>Nary-Tree input serialization&nbsp;is represented in their level order traversal, each group of children is separated by the null value (See examples).</em></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/11/08/sample_4_964.png" style="width: 296px; height: 241px;" /></p>

<p>For example, the above tree is serialized as [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14].</p>

<p><br />
<strong>Custom testing:</strong><br />
You should provide the serialization of the input <code>tree</code>.<br />
The Driver code then extracts the nodes from the tree and shuffles them. You shouldn&#39;t&nbsp;care how the extracted nodes are shuffled.<br />
The driver code will provide you with an array of the extracted nodes in random order and you need to find the root of the tree out of these nodes.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/12/narytreeexample.png" style="width: 100%; max-width: 300px;" /></p>

<pre>
<strong>Input:</strong> tree = [1,null,3,2,4,null,5,6]
<strong>Output:</strong> [1,null,3,2,4,null,5,6]
<strong>Explanation:</strong> The input tree is shown above. The driver code first extracts the nodes so we now have an array of all tree nodes [Node(1),Node(3),Node(2),Node(4),Node(5),Node(6)], then the array is randomly shuffled, thus the actual input is [Node(5),Node(4),Node(3),Node(6),Node(2),Node(1)].
The root of the tree is Node(1) and the output is the serialization of the node you return.
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/11/08/sample_4_964.png" style="width: 296px; height: 241px;" /></p>

<pre>
<strong>Input:</strong> tree = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
<strong>Output:</strong> [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The total number of nodes is between <code>[1, 5 * 10^4]</code>.</li>
	<li>Each&nbsp;node has a unique value.</li>
</ul>

</div>

## Tags


## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n) time with O(n) space and O(1) space [follow-up]
- Author: Anonymouso
- Creation Date: Thu Jul 09 2020 12:16:50 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 10 2020 00:25:07 GMT+0800 (Singapore Standard Time)

<p>
A straightforward solution would be to go over all the nodes and add **only the node\'s children** to a "seen" hashset.
After we\'re done - go over the list of nodes in the tree one more time, and return the node that doesn\'t exist in the "seen" hashset.
```
public Node findRoot(List<Node> tree) {
	// Edge Case
	if (tree==null || tree.size()==0) return null;

	Set<Node> seen = new HashSet<>();

	// For each node in the tree
	for (Node node : tree){
		// For each child of the current node
		for (Node child : node.children)
			seen.add(child);
	}

	// For each node in the tree, if node doesn\'t exist in the set, return it as it\'s our root
	for (Node node : tree)
		if (!seen.contains(node)) return node;

	return null;
}
```		

Time: O(n) for first loop, O(n) for 2nd loop - **total O(n) time**
Space: **O(n) space** for the "seen" set

.
.
.
***Follow Up: Can you do O(1) space?***
Using the same logic as above - and considering the problem\'s constraint [The total number of nodes is between [1, 5x10^4].] and that all the nodes are unique, we can come up with a better space complexity.

Instead of using a "seen" set - we can simply add all the values of the nodes into a Long sum variable, then deduct the children from same variable.
For example: [1,null,3,2,4,null,5,6]
Our variable as we progress through the nodes would be: 
sum=0;
sum= +1 -3 -2 -4 +3 -5 -6 +2 +4 +5 +6   [=1]

**Explanation**: 
1 (current node) 
-3 -2 -4 (current children)
+3 (current node) 
-5 -6 (current children) 
+2 (current node) 
+4 (current node) 
+5 (current node) 
+6 (current node)

Once we\'re done, we simply run over the tree nodes one more time and return the node which val equals the remaining of our "long sum" variable (remember, all nodes have unique values so this will work).

```
public Node findRoot(List<Node> tree) {
	// Edge Case
	if (tree==null || tree.size()==0) return null;

	long sum=0;
	
	// For each node in the tree
	for (Node node : tree){
		// Add current node value to sum
		sum+=node.val;
		
		// For each child - reduce value of child from sum
		for (Node child : node.children)
			sum-=child.val;
	}

	// Remaining value in sum is the only node that doesn\'t have a parent (meaning isn\'t a child of any other node) - which is the root.
	for (Node node : tree)
		if (node.val==sum) return node;

	return null;
}
```

Time: O(n) for first loop, O(n) for 2nd loop - **total O(n) time**
Space: **O(1) space**

.
.
.

**Update**: (thanks to [@hanzhoutang\'s solution](https://leetcode.com/problems/find-root-of-n-ary-tree/discuss/726536/c%2B%2B-bit-operation-1-pass-with-O(1)-space-solution))
There\'s actually a slightly faster way to calculate the sum using bit operations.
Instead of using "+" and "-" we can simply XOR the values.
If you remember, doing XOR between same number will result in 0 (2 XOR 2 = 0) thus XORing all the nodes with their children will eventually leave us with the only node that wasn\'t a child - the root.

```
public Node findRoot(List<Node> tree) {
	// Edge Case
	if (tree==null || tree.size()==0) return null;

	long sum=0;

	// For each node in the tree
	for (Node node : tree){
		// Add current node value to sum
		sum^=node.val;

		// For each child - reduce value of child from sum
		for (Node child : node.children)
			sum^=child.val;
	}

	// Remaining value in sum is the only node that doesn\'t have a parent (meaning isn\'t a child of any other node) - which is the root.
	for (Node node : tree)
		if (node.val==sum) return node;

	return null;
}
```

If you\'re not familiar with how the XOR operation works - see [this](https://leetcode.com/problems/single-number/) problem.
</p>


### Java Solution - Space O(1)
- Author: myCafeBabe
- Creation Date: Thu Jul 09 2020 13:48:21 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 09 2020 13:48:21 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public Node findRoot(List<Node> tree) {
        int allSum = 0;
        int allChildrenSum = 0;
        for (Node node : tree) {
            allSum += node.val;
            for (Node child : node.children) {
                allChildrenSum += child.val;
            }
        }
        for (Node node : tree) {
            if (node.val == allSum - allChildrenSum) {
                return node;
            }
        }
        return null;
    }
}
```
</p>


### Python O(1) space solution credit @Anonymouso
- Author: ethan101
- Creation Date: Thu Jul 09 2020 14:27:26 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 09 2020 14:52:03 GMT+0800 (Singapore Standard Time)

<p>
```
## Credit goes to @Anonymouso who comes up the solution
## https://leetcode.com/problems/find-root-of-n-ary-tree/discuss/726453/Java-O(n)-time-with-O(n)-space-and-O(1)-space-follow-up
## 
## Basically we can go through each node of the list, and add the value
## of the node while substract the values of its children.
## Because root is not a child of any node, it will be kept to the end of sum but all other nodes will be naturalized
## 
## eg .[7,null,3,2,4,null,5,6]
## sumVal = 7 - (3 + 2 + 4) + 3 - (5 + 6) + 2 + 4 + 5 + 6 = 7
## So node with value 7 is the root
          
class Solution:
  def findRoot(self, tree: List[\'Node\']) -> \'Node\':
    sumVal = 0
    for node in tree:
      sumVal += node.val - sum(child.val for child in node.children)
    for node in tree:
      if node.val == sumVal:
        return node
    return None
```
</p>


