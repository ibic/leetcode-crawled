---
title: "String Transforms Into Another String"
weight: 1031
#id: "string-transforms-into-another-string"
---
## Description
<div class="description">
<p>Given two strings <code>str1</code> and <code>str2</code>&nbsp;of the same length, determine whether you can transform <code>str1</code>&nbsp;into <code>str2</code> by doing <strong>zero or more</strong>&nbsp;<em>conversions</em>.</p>

<p>In one conversion you can convert&nbsp;<strong>all</strong> occurrences of one character in <code>str1</code> to&nbsp;<strong>any</strong> other lowercase English character.</p>

<p>Return <code>true</code>&nbsp;if and only if you can transform <code>str1</code> into <code>str2</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>str1 = <span id="example-input-1-1">&quot;aabcc&quot;</span>, str2 = <span id="example-input-1-2">&quot;ccdee&quot;</span>
<strong>Output: </strong><span id="example-output-1">true</span>
<strong>Explanation: </strong>Convert &#39;c&#39; to &#39;e&#39; then &#39;b&#39; to &#39;d&#39; then &#39;a&#39; to &#39;c&#39;. Note that the order of conversions matter.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>str1 = <span id="example-input-2-1">&quot;leetcode&quot;</span>, str2 = <span id="example-input-2-2">&quot;codeleet&quot;</span>
<strong>Output: </strong><span id="example-output-2">false</span>
<strong>Explanation: </strong>There is no way to transform str1 to str2.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= str1.length == str2.length &lt;= 10^4</code></li>
	<li>Both <code>str1</code> and <code>str2</code> contain only lowercase English letters.</li>
</ol>

</div>

## Tags
- Graph (graph)

## Companies
- Google - 11 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Need One Unused Character
- Author: lee215
- Creation Date: Sun Aug 11 2019 00:02:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 11 2019 02:06:01 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Scan `s1` and `s2` at the same time,
record the transform mapping into a map/array.
The same char should transform to the same char.
Otherwise we can directly return false.

To realise the transformation:
1. transformation of link link ,like `a -> b -> c`:
we do the transformation from end to begin, that is `b->c` then `a->b`

2. transformation of cycle, like `a -> b -> c -> a`:
in this case we need a `tmp`
`c->tmp`, `b->c` `a->b` and `tmp->a`
Same as the process of swap two variable.

In both case, there should at least one character that is unused,
to use it as the `tmp` for transformation.
So we need to return if the size of set of unused characters < 26.
<br>

## **Complexity**
Time `O(N)` for scanning input
Space `O(26)` to record the mapping
running time can be improved if count available character during the scan.
<br>

**Java**
```java
    public boolean canConvert(String s1, String s2) {
        if (s1.equals(s2)) return true;
        Map<Character, Character> dp = new HashMap<>();
        for (int i = 0; i < s1.length(); ++i) {
            if (dp.getOrDefault(s1.charAt(i), s2.charAt(i)) != s2.charAt(i))
                return false;
            dp.put(s1.charAt(i), s2.charAt(i));
        }
        return new HashSet<Character>(dp.values()).size() < 26;
    }
```

**C++:**
```cpp
    bool canConvert(string s1, string s2) {
        if (s1 == s2) return true;
        unordered_map<char, char> dp;
        for (int i = 0; i < s1.length(); ++i) {
            if (dp[s1[i]] != NULL && dp[s1[i]] != s2[i])
                return false;
            dp[s1[i]] = s2[i];
        }
        return set(s2.begin(), s2.end()).size() < 26;
    }
```

**Python:**
can be 1 line but too long.
```python
    def canConvert(self, s1, s2):
        if s1 == s2: return True
        dp = {}
        for i, j in zip(s1, s2):
            if dp.setdefault(i, j) != j:
                return False
        return len(set(s2)) < 26
```
</p>


### Complete Logical Thinking (This is why only check if str2 has unused character)
- Author: unfinishedmission
- Creation Date: Mon Oct 07 2019 14:54:40 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 07 2019 15:25:01 GMT+0800 (Singapore Standard Time)

<p>
**How to model this problem?**
1. Model it as a graph would be the most intuitive way. The edges in the graph represent the mapping relationships. 
2. Realize that the out degree of a node can only be smaller or equal to 1, otherwise we should just return false immediately! So the graph is  just linked lists and using a hashmap will be sufficient to keep track of the edges.
3. Use a HashMap<Character, Character> to keep track of the edges.

**Why only check if str2 has unused character?**
See point 8 and 9 below

**Logical Thinking Step by Step:**
1. Model it as a graph
2. Realize that the out degree of a node can only be smaller or equal to 1, so the graph is  just a linked list and using a hashmap will be sufficient to keep track of the edges.
3. Note that size(key set) >= size(value set) because of point 2 above.
4. Realize that if there is no cycle in the graph, we can process the linked list from the end to the beginning, so just return true if no cycle exists.
5. **It would be tempting for us to conclude that result=hasCycle(graph) but this is incorrect!**
6. Realize that having cycle doesn\'t necessarily mean the result is false. This is because cycles can be break by using a temp node (temp node has zero out degree, in other words, temp node doesn\'t show up in the keyset of the hashmap or temp character doesn\'t show up in str1)
7. **It would be tempting for us to conclude that ressult = size(key set)<26 but this is incorrect!**
8. Realize that size(key set)=26 doesn\'t necessarily mean the result is false. we still need to pay attention to a spacial case when size(key set)=26 but size(value set)<26. In such case, multiple characters are mapped to the same character. Given this, we can create an unused character
9. How to create an unused characte in str1r: let\'s say c1, c2 are both mapped to c3. we transform c1 to c2 first. After this transformation, c1 disappears from the str1, so c1 is an unused character now!
10. We reach the conclusion that "if size(value set)<26, return true". As the final step, Let\'s prove that "if size(value set)=26, return false". When size(value set)=26, it implies that size(key set)=26 too, so we have 26 one-to-one mappings. If str1 is not the same as str2, cycle must exist, but there is no unused character to break the cycle. So return false.

**Actual Algorithm:**
1. Loop through str1 and str2, update hashmap with the mapping str1[i] -> str2[i]
2. If one-to-more mapping are found, return flase immediately
3. return size(value set) < 26 at the end

**Solution:**
```Java
class Solution {
    public boolean canConvert(String str1, String str2) {
        if (str1.equals(str2)) {
            return true;
        }
        Map<Character, Character> map = new HashMap<>();
        for (int i = 0; i < str1.length(); i++) {
            char c1 = str1.charAt(i);
            char c2 = str2.charAt(i);
           if (map.containsKey(c1) && map.get(c1) != c2) {
                return false;
            }
            map.put(c1, c2);
        }
        return new HashSet<Character>(map.values()).size() < 26;
    }
}
```
</p>


### Python simple O(n) with explanation
- Author: cjporteo
- Creation Date: Sun Aug 11 2019 00:23:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 11 2019 00:23:09 GMT+0800 (Singapore Standard Time)

<p>
Map each character in ```str1``` to what it needs to be in ```str2```. If any of these mappings collide (e.g. ```str1``` = "aa", ```str2``` = "bc", "a" needs to become both "b" and "c") we immediately return ```False``` since the transformation is impossible.

Next, we check the number of unique characters in ```str2```. If all 26 characters are represented, there are no characters available to use for temporary conversions, and the transformation is impossible. The only exception to this is if ```str1``` is equal to ```str2```, so we handle this case at the start of the function.

```
class Solution:
    def canConvert(self, str1: str, str2: str) -> bool:
        if str1 == str2:
            return True
        m = {}
        for i in range(len(str1)):
            if str1[i] not in m:
                m[str1[i]] = str2[i]
            elif m[str1[i]] != str2[i]:
                return False
        return len(set(str2)) < 26
```
</p>


