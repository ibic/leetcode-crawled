---
title: "Lowest Common Ancestor of a Binary Search Tree"
weight: 219
#id: "lowest-common-ancestor-of-a-binary-search-tree"
---
## Description
<div class="description">
<p>Given a binary search tree (BST), find the lowest common ancestor (LCA) of two given nodes in the BST.</p>

<p>According to the <a href="https://en.wikipedia.org/wiki/Lowest_common_ancestor" target="_blank">definition of LCA on Wikipedia</a>: &ldquo;The lowest common ancestor is defined between two nodes p and q&nbsp;as the lowest node in T that has both p and q&nbsp;as descendants (where we allow <b>a node to be a descendant of itself</b>).&rdquo;</p>

<p>Given binary search tree:&nbsp; root =&nbsp;[6,2,8,0,4,7,9,null,null,3,5]</p>
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/14/binarysearchtree_improved.png" style="width: 200px; height: 190px;" />
<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 8
<strong>Output:</strong> 6
<strong>Explanation: </strong>The LCA of nodes <code>2</code> and <code>8</code> is <code>6</code>.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 4
<strong>Output:</strong> 2
<strong>Explanation: </strong>The LCA of nodes <code>2</code> and <code>4</code> is <code>2</code>, since a node can be a descendant of itself according to the LCA definition.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>All of the nodes&#39; values will be unique.</li>
	<li>p and q are different and both values will&nbsp;exist in the BST.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 2 (taggedByAdmin: true)
- Microsoft - 5 (taggedByAdmin: true)
- Facebook - 4 (taggedByAdmin: true)
- LinkedIn - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

We can solve this using the approaches to find [LCA in a binary tree](https://leetcode.com/articles/lowest-common-ancestor-of-a-binary-tree/).

But, binary search tree's property could be utilized, to come up with a better algorithm.

Lets review properties of a BST:
>1. Left subtree of a node N contains nodes whose values are lesser than or equal to node N's value.
>2. Right subtree of a node N contains nodes whose values are greater than node N's value.
>3. Both left and right subtrees are also BSTs.

#### Approach 1: Recursive Approach

**Intuition**

Lowest common ancestor for two nodes `p` and `q` would be the last ancestor node common to both of them. Here `last` is defined in terms of the depth of the node. The below diagram would help in understanding what `lowest` means.

<center>
<img src="../Figures/235/235_LCA_Binary_1.png" width="600"/>
</center>

Note: One of `p` or `q` would be in the left subtree and the other in the right subtree of the LCA node.

Following cases are possible:
<center>
<img src="../Figures/235/235_LCA_Binary_2.png" width="600"/>
</center>

**Algorithm**

1. Start traversing the tree from the root node.
2. If both the nodes `p` and `q` are in the right subtree, then continue the search with right subtree starting step 1.
3. If both the nodes `p` and `q` are in the left subtree, then continue the search with left subtree starting step 1.
4. If both step 2 and step 3 are not true, this means we have found the node which is common to node `p`'s and `q`'s subtrees.
and hence we return this common node as the LCA.

<iframe src="https://leetcode.com/playground/ub9aBGUZ/shared" frameBorder="0" width="100%" height="497" name="ub9aBGUZ"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of nodes in the BST. In the worst case we might be visiting all the nodes of the BST.

* Space Complexity: $$O(N)$$. This is because the maximum amount of space utilized by the recursion stack would be $$N$$ since the height of a skewed BST could be $$N$$.
<br/>
<br/>

---

#### Approach 2: Iterative Approach

**Algorithm**

The steps taken are also similar to approach 1. The only difference is instead of recursively calling the function, we traverse down the tree iteratively. This is possible without using a stack or recursion since we don't need to backtrace to find the LCA node. In essence of it the problem is iterative, it just wants us to find the split point. The point from where `p` and `q` won't be part of the same subtree or when one is the parent of the other.

<iframe src="https://leetcode.com/playground/UtgsdU9t/shared" frameBorder="0" width="100%" height="500" name="UtgsdU9t"></iframe>

**Complexity Analysis**

* Time Complexity : $$O(N)$$, where $$N$$ is the number of nodes in the BST. In the worst case we might be visiting all the nodes of the BST.

* Space Complexity : $$O(1)$$.
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 3 lines with O(1) space, 1-Liners, Alternatives
- Author: StefanPochmann
- Creation Date: Sat Jul 11 2015 06:06:11 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 19:44:06 GMT+0800 (Singapore Standard Time)

<p>
Just walk down from the whole tree's root as long as both p and q are in the same subtree (meaning their values are both smaller or both larger than root's). This walks straight from the root to the LCA, not looking at the rest of the tree, so it's pretty much as fast as it gets. A few ways to do it:

**Iterative, O(1) space**

Python

    def lowestCommonAncestor(self, root, p, q):
        while (root.val - p.val) * (root.val - q.val) > 0:
            root = (root.left, root.right)[p.val > root.val]
        return root

Java

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        while ((root.val - p.val) * (root.val - q.val) > 0)
            root = p.val < root.val ? root.left : root.right;
        return root;
    }

(in case of overflow, I'd do `(root.val - (long)p.val) * (root.val - (long)q.val)`)

Different Python

    def lowestCommonAncestor(self, root, p, q):
        a, b = sorted([p.val, q.val])
        while not a <= root.val <= b:
            root = (root.left, root.right)[a > root.val]
        return root

"Long" Python, maybe easiest to understand

    def lowestCommonAncestor(self, root, p, q):
        while root:
            if p.val < root.val > q.val:
                root = root.left
            elif p.val > root.val < q.val:
                root = root.right
            else:
                return root

**Recursive**

Python

    def lowestCommonAncestor(self, root, p, q):
        next = p.val < root.val > q.val and root.left or \
               p.val > root.val < q.val and root.right
        return self.lowestCommonAncestor(next, p, q) if next else root

Python One-Liner

    def lowestCommonAncestor(self, root, p, q):
        return root if (root.val - p.val) * (root.val - q.val) < 1 else \
               self.lowestCommonAncestor((root.left, root.right)[p.val > root.val], p, q)

Java One-Liner

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        return (root.val - p.val) * (root.val - q.val) < 1 ? root :
               lowestCommonAncestor(p.val < root.val ? root.left : root.right, p, q);
    }

"Long" Python, maybe easiest to understand

    def lowestCommonAncestor(self, root, p, q):
        if p.val < root.val > q.val:
            return self.lowestCommonAncestor(root.left, p, q)
        if p.val > root.val < q.val:
            return self.lowestCommonAncestor(root.right, p, q)
        return root
</p>


### My Java Solution
- Author: jingzhetian
- Creation Date: Sat Jul 11 2015 05:01:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 18:17:31 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
            if(root.val > p.val && root.val > q.val){
                return lowestCommonAncestor(root.left, p, q);
            }else if(root.val < p.val && root.val < q.val){
                return lowestCommonAncestor(root.right, p, q);
            }else{
                return root;
            }
        }
    }
</p>


### C++ Recursive and Iterative
- Author: jianchao-li
- Creation Date: Mon Jul 13 2015 13:52:53 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 08:39:36 GMT+0800 (Singapore Standard Time)

<p>
Well, remember to take advantage of the property of binary search trees, which is, `node -> left -> val < node -> val < node -> right -> val`. Moreover, both `p` and `q` will be the descendants of the `root` of the subtree that contains both of them. And the `root` with the largest depth is just the lowest common ancestor. This idea can be turned into the following simple recursive code.

```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        if ((root -> val > p -> val) && (root -> val > q -> val)) {
            return lowestCommonAncestor(root -> left, p, q);
        }
        if ((root -> val < p -> val) && (root -> val < q -> val)) {
            return lowestCommonAncestor(root -> right, p, q);
        }
        return root;
    }
};
```

Of course, we can also solve it iteratively.

```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        TreeNode* cur = root;
        while (true) {
            if (p -> val < cur -> val && q -> val < cur -> val) {
                cur = cur -> left;
            } else if (p -> val > cur -> val && q -> val > cur -> val) {
                cur = cur -> right;
            } else {
                break;
            }
        }
        return cur;
    }
};
```
</p>


