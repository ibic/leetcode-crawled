---
title: "Goat Latin"
weight: 765
#id: "goat-latin"
---
## Description
<div class="description">
<p>A sentence <code>S</code> is given, composed of words separated by spaces. Each word consists of lowercase and uppercase letters only.</p>

<p>We would like to convert the sentence to &quot;<em>Goat Latin&quot;</em>&nbsp;(a made-up language similar to Pig Latin.)</p>

<p>The rules of Goat Latin are as follows:</p>

<ul>
	<li>If a word begins with a vowel (a, e, i, o, or u), append <code>&quot;ma&quot;</code>&nbsp;to the end of the word.<br />
	For example, the word &#39;apple&#39; becomes &#39;applema&#39;.<br />
	&nbsp;</li>
	<li>If a word begins with a consonant (i.e. not a vowel), remove the first letter and append it to the end, then add <code>&quot;ma&quot;</code>.<br />
	For example, the word <code>&quot;goat&quot;</code>&nbsp;becomes <code>&quot;oatgma&quot;</code>.<br />
	&nbsp;</li>
	<li>Add one letter <code>&#39;a&#39;</code>&nbsp;to the end of each word per its word index in the sentence, starting with 1.<br />
	For example,&nbsp;the first word gets <code>&quot;a&quot;</code> added to the end, the second word gets <code>&quot;aa&quot;</code> added to the end and so on.</li>
</ul>

<p>Return the&nbsp;final sentence representing the conversion from <code>S</code>&nbsp;to Goat&nbsp;Latin.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>&quot;I speak Goat Latin&quot;
<strong>Output: </strong>&quot;Imaa peaksmaaa oatGmaaaa atinLmaaaaa&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>&quot;The quick brown fox jumped over the lazy dog&quot;
<strong>Output: </strong>&quot;heTmaa uickqmaaa rownbmaaaa oxfmaaaaa umpedjmaaaaaa overmaaaaaaa hetmaaaaaaaa azylmaaaaaaaaa ogdmaaaaaaaaaa&quot;
</pre>

<p>&nbsp;</p>

<p>Notes:</p>

<ul>
	<li><code>S</code> contains only uppercase, lowercase and spaces.&nbsp;Exactly one space between each word.</li>
	<li><code>1 &lt;= S.length &lt;= 150</code>.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Facebook - 4 (taggedByAdmin: true)
- Square - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach 1: Just Strings

The problem is pretty straightforward and the main intention is to focus on deep knowledge about Strings and other abstract types, how they are stored in memory, and their performance characteristics across languages and platforms.

Depending on the language, splitting strings into words (or _tokens_) can be functionally easy (e.g. python) or a major hassle (e.g. C++).

Similarly, building strings can show a wide gamut of performance:

+ For languages where strings are immutable (e.g. Java, Python), building strings by simply concatenating existing ones is usually a very expensive process, because it generates entirely new strings. [^note-1]   
Such languages usually solve this problem by providing a function to efficiently join a collection of strings (all of whose lengths are known before-hand). [^note-2]

+ Other languages, which treat strings as mutable data types (e.g. C++), have fairly efficient concatenation operators / functions, usually providing an acceptable amortized performance. [^note-3]

Back to the problem; note that the following two suffixes are added to _every_ word:
+ The string `"ma"` is appended to the word.
+ A string comprised entirely of character `'a'` is appended to the resulting word. This string increases in length with every word processed.

**Algorithm**

1. Split the sentence into words. Words are separated by a single space.
2. For each word:
    + If the word doesn't start with a vowel, we rotate the first character; it is removed from the word and appended at the end of the word.
    + Append the final word to our generated sentence, along with the required suffixes.
    + Finally add a single space character.
3. Remove the extraneous last space from the generated sentence.

<iframe src="https://leetcode.com/playground/hwLToHmr/shared" frameBorder="0" width="100%" height="500" name="hwLToHmr"></iframe>

**Complexity Analysis**

Before we get on to computing complexity of the entire algorithm, let's first break it down into its constituent operations:

+ **OP-1**: Testing whether a word starts with a vowel is a constant time operation, and take constant space of storing the vowels.
+ **OP-2**: Appending the character `'a'` to the suffix in each iteration of the loop takes amortized constant time. The suffix grows by one character with each iteration.
+ **OP-3**: Rotating the word is basically two operations: deleting the first character, and then appending the first character to the resultant string. This takes time and space linear to the length of the word.
+ **OP-4**: Appending the suffixes and space take time amortized to length of the concatenated word. The space is also linear to the length of the concatenated word.
+ **OP-5**: Joining the collection of words into the final output string takes length and space linear in length of the output string.
+ **OP-6**: Removing the last extraneous space character takes constant time, and doesn't change the order of our space usage.

Now let's look at the overall complexity of our algorithm:

* Time Complexity: $$O(n^2)$$, where $$n$$ is the length of input string, in the worst case.
    
    Let's assume that the average length of each word is $$w$$. Thus, the number of words available is roughly $$k = \frac{n}{w}$$. Let's also say that the worst has happened: every word starts with a consonent (so, we need to rotate every word).

    + **OP-1** and **OP-2** take $$O(k)$$ time over $$k$$ words.
    + **OP-3** takes $$O(w)$$ time for each of the $$k$$ words. Thus total time taken is about $$k \cdot O(w) \simeq O(n)$$.
    + **OP-4** takes time linear to length of the concatenated word (i.e. the word, the suffixes, and a single space character). Over $$k$$ words, the time taken is:  
    
    $$
    \begin{gathered}
    \begin{aligned}
        &\quad \; O(w) + 1 \cdot O(1) + O(1) + O(1)     \\
        &+ O(w) + 2 \cdot O(1) + O(1) + O(1)     \\
        &+ O(w) + 3 \cdot O(1) + O(1) + O(1)     \\
        &+ O(w) + 4 \cdot O(1) + O(1) + O(1)     \\
        &+\; ...    \\
        &+ O(w) + k \cdot O(1) + O(1) + O(1)     \\ 
        &= k \cdot O(w) + (1 + 2 + 3 + ... + k) \cdot O(1) + 2k \cdot O(1)      \\
        &\simeq O(n) + O(k^2)
    \end{aligned}
    \end{gathered}
    $$

    + **OP-5** takes the same time as, the total time **OP-4** takes over $$k$$ words, i.e. $$\sim O(n) + O(k^2)$$. This is because **OP-5** is just joining all the characters generated by **OP-4**, after all $$k$$ iterations of **OP-4**.
    + **OP-6** takes constant time $$O(1)$$.

    > Overall, the time required is in the order of $$\sim O(n) + O(k^2)$$. [^note-4]

    In the worst case, each word is only a single character long. Thus $$k = n$$; the overall time complexity becomes $$\sim O(n) + O(n^2) \; \simeq \; O(n^2)$$ in the worst case.

 
* Space Complexity: $$O(n^2)$$, where $$n$$ is the length of input string, in the worst case.

    Let's make the same assumptions we made for time complexity analysis:

    + **OP-1** takes constant extra space.
    + **OP-2** takes a maximum space in the length of the suffix string, i.e. $$O(k)$$ space.
    + **OP-3** takes the same amount of space as the word itself (since overall length doesn't change due to rotation), i.e. $$O(w)$$ space.
    + **OP-4** takes space linear to length of the concatenated word (i.e. the word, the suffixes, and a single space character). Over $$k$$ words, the space taken is:
    
    $$
    \begin{gathered}
    \begin{aligned}
        &\quad \; O(w) + 1 \cdot O(1) + O(1) + O(1)     \\
        &+ O(w) + 2 \cdot O(1) + O(1) + O(1)     \\
        &+ O(w) + 3 \cdot O(1) + O(1) + O(1)     \\
        &+ O(w) + 4 \cdot O(1) + O(1) + O(1)     \\
        &+\; ...    \\
        &+ O(w) + k \cdot O(1) + O(1) + O(1)     \\ 
        &= k \cdot O(w) + (1 + 2 + 3 + ... + k) \cdot O(1) + 2k \cdot O(1)      \\
        &\simeq O(n) + O(k^2)
    \end{aligned}
    \end{gathered}
    $$

    + **OP-5** takes the same space as, the total space **OP-4** takes over $$k$$ words, i.e. $$\sim O(n) + O(k^2)$$. This is because **OP-5** is just joining all the characters generated by **OP-4**, after all $$k$$ iterations of **OP-4**.
    + **OP-6** reduces constant space $$O(1)$$.

    > Overall, the extra space required is in the order of $$\sim O(n) + O(k^2)$$. [^note-4]

    In the worst case, each word is only a single character long. Thus $$k = n$$; the overall space complexity becomes $$\sim O(n) + O(n^2) \; \simeq \; O(n^2)$$ in the worst case.


[^note-1]: This can quickly lead to quadratic runtime complexity, just from concatenating strings! [This article](https://leetcode.com/articles/sort-characters-by-frequency/#remember-strings-are-immutable) has a nice introduction on immutable strings.

[^note-2]: In Java, that's usually the [`StringBuilder`](https://docs.oracle.com/javase/7/docs/api/java/lang/StringBuilder.html) class. In Python, [`str.join()`](https://docs.python.org/2/library/stdtypes.html#str.join) will do the trick.

[^note-3]: Most implementations of C++'s [`std::string`](https://en.cppreference.com/w/cpp/string/basic_string) provide concatenation operations (e.g. `+` operator, `string::append` function etc.) which run in amortized linear time complexity.

[^note-4]: Notice that we discarded lower order terms (like $$O(1)$$ and $$O(k)$$). That's because lower order terms do not contribute to algorithmic complexity as much as higher order terms do. The highest order term (and comparable terms) tend to _dominate_ complexity anlysis.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Straight Forward Solution
- Author: lee215
- Creation Date: Sun Apr 29 2018 14:45:49 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 19 2020 23:49:05 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
1. Build a vowel set.(optional)
2. Split \'S\' to words.
3. For each word, check if it starts with a vowel. (O(1) complexity with set).
4. If it does, keep going. If it does not, rotate the first letter to the end.
5. Add it to result string.
<br>

**C++:**
```cpp
    string toGoatLatin(string S) {
        unordered_set<char> vowel({\'a\', \'e\', \'i\', \'o\', \'u\', \'A\', \'E\', \'I\', \'O\', \'U\'});
        istringstream iss(S);
        string res, w;
        int i = 0, j;
        while (iss >> w) {
            res += \' \' + (vowel.count(w[0]) == 1 ? w : w.substr(1) + w[0]) + "ma";
            for (j = 0, ++i; j < i; ++j) res += "a";
        }
        return res.substr(1);
    }
```
**Java:**
```java
    public String toGoatLatin(String S) {
        Set<Character> vowels = new HashSet<>(Arrays.asList(\'a\', \'e\', \'i\', \'o\', \'u\', \'A\', \'E\', \'I\', \'O\', \'U\'));
        String res = "";
        int i = 0, j = 0;
        for (String w : S.split("\\s")) {
            res += \' \' + (vowels.contains(w.charAt(0)) ? w : w.substring(1) + w.charAt(0)) + "ma";
            for (j = 0, ++i; j < i; ++j) {
                res += "a";
            }
        };
        return res.substring(1);
    }
```
**Python:**
```py
    def toGoatLatin(self, S):
        vowel = set(\'aeiouAEIOU\')
        def latin(w, i):
            if w[0] not in vowel:
                w = w[1:] + w[0]
            return w + \'ma\' + \'a\' * (i + 1)
        return \' \'.join(latin(w, i) for i, w in enumerate(S.split()))
```

**1-line Python:**
```py
    def toGoatLatin(self, S):
        return \' \'.join((w if w[0].lower() in \'aeiou\' else w[1:] + w[0]) + \'ma\' + \'a\' * (i + 1) for i, w in enumerate(S.split()))
```

</p>


### Short C++ solution using i/o stringstream
- Author: mzchen
- Creation Date: Sun Apr 29 2018 11:29:57 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 26 2018 23:51:43 GMT+0800 (Singapore Standard Time)

<p>
```
string toGoatLatin(string S) {
    stringstream iss(S), oss;
    string vowels("aeiouAEIOU"), word, a;
    while (iss >> word) {
        a.push_back(\'a\');
        if (vowels.find_first_of(word[0]) != string::npos) // begin with a vowel
            oss << \' \' << word << "ma" << a;
        else // begin with a consonant
            oss << \' \' << word.substr(1) << word[0] << "ma" << a;
    }
    return oss.str().substr(1);
}
```
</p>


