---
title: "Plus One Linked List"
weight: 352
#id: "plus-one-linked-list"
---
## Description
<div class="description">
<p>Given a non-negative integer represented as <b>non-empty</b> a singly linked list of digits, plus one to the integer.</p>

<p>You may assume the integer do not contain any leading zero, except the number 0 itself.</p>

<p>The digits are stored such that the most significant digit is at the head of the list.</p>

<div>
<p><strong>Example :</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,2,3]</span>
<strong>Output: </strong><span id="example-output-1">[1,2,4]</span>
</pre>
</div>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Google - 4 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Overview.

"Plus One" is a subset of a problem set "Add Number",
and the solution patterns are the same. 

All these problems could be solved in linear time, 
and the question here is how to solve without using addition operation or 
how to fit into constant space complexity.

The choice of algorithm should be based on input format:

1. Integers. 
Usually addition operation is not allowed for such a case.
Use Bit Manipulation Approach. 
Here is an example: [Add Binary](https://leetcode.com/articles/add-binary/).

2. Strings. 
Use schoolbook bit by bit computation. 
Note, that to fit into constant space is not possible for 
languages with immutable strings, for ex. for Java and Python.
Here is an example: [Add Binary](https://leetcode.com/articles/add-binary/).

3. Arrays. 
The same textbook addition.
Here is an example: [Add to Array Form of Integer](https://leetcode.com/articles/add-to-array-form-of-integer/).

4. Linked Lists, current problem. 
Sentinel Head + Textbook Addition. 

Note, that straightforward idea to convert everything into 
integers and then use addition could be risky for Java interviews
because of possible overflow issues, [here is in more details](https://leetcode.com/articles/add-binary/).
<br /> 
<br />


---
#### Approach 1: Sentinel Head + Textbook Addition.

**Textbook Addition**

Let's identify the rightmost digit which is not equal to nine and 
increase that digit by one. All the following nines should be set to zero.

Here is the simplest use case which works fine.

![simple](../Figures/369/simple.png)

Here is more difficult case which still passes.

![diff](../Figures/369/diff.png)

And here is the case which breaks everything.

![diff](../Figures/369/handle.png)

**Sentinel Head**

To handle the last use case, one needs so called [Sentinel Node](https://en.wikipedia.org/wiki/Sentinel_node).
Sentinel nodes are widely used for trees and linked lists as pseudo-heads, 
pseudo-tails, etc. 
They are purely functional, and usually don't hold any data. 
Their main purpose is to standardize the situation to avoid edge case
handling.
 
For example, here one could add pseudo-head with zero value,
and hence there will always be not-nine node.

![diff](../Figures/369/sentinel.png)

**Algorithm**

- Initialize sentinel node as `ListNode(0)` 
and set it to be the new head: `sentinel.next = head`.

- Find the rightmost digit not equal to nine.

- Increase that digit by one.

- Set all the following nines to zero. 

- Return sentinel node if it was set to 1, 
and head `sentinel.next` otherwise.

**Implementation**

<iframe src="https://leetcode.com/playground/UssTC83r/shared" frameBorder="0" width="100%" height="497" name="UssTC83r"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since it's not more that 
two passes along the input list.
 
* Space complexity : $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Iterative Two-Pointers with dummy node Java O(n) time, O(1) space
- Author: oceanator
- Creation Date: Tue Jun 28 2016 15:42:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 12:58:38 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public ListNode plusOne(ListNode head) {
            ListNode dummy = new ListNode(0);
            dummy.next = head;
            ListNode i = dummy;
            ListNode j = dummy;
            
            while (j.next != null) {
                j = j.next;
                if (j.val != 9) {
                    i = j;
                }
            }
            
            if (j.val != 9) {
                j.val++;
            } else {
                i.val++;
                i = i.next;
                while (i != null) {
                    i.val = 0;
                    i = i.next;
                }
            }
            
            if (dummy.val == 0) {
                return dummy.next;
            }
            
            return dummy;
        }
    }

- i stands for the most significant digit that is going to be incremented if there exists a carry
- dummy node can handle cases such as "9->9>-9" automatically
</p>


### Java recursive solution
- Author: myfavcat123
- Creation Date: Tue Jun 28 2016 13:51:22 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 22:05:43 GMT+0800 (Singapore Standard Time)

<p>
At the first glance, I want to reverse the inputs, add one, then reverse back. But that is too intuitive and I don't think this is an expected solution. Then what kind of alg would adding one in reverse way for list?

Recursion! With recursion, we can visit list in reverse way! So here is my recursive solution.


    public ListNode plusOne(ListNode head) {
        if( DFS(head) == 0){
            return head;
        }else{
            ListNode newHead = new ListNode(1);
            newHead.next = head;
            return newHead;
        }
    }
    
    public int DFS(ListNode head){
        if(head == null) return 1;
        
        int carry = DFS(head.next);
        
        if(carry == 0) return 0;
        
        int val = head.val + 1;
        head.val = val%10;
        return val/10;
    }
</p>


### Two-Pointers Java Solution: O(n) time, O(1) space
- Author: xsunfeng
- Creation Date: Wed Jun 29 2016 00:47:48 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 19 2018 23:53:06 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public ListNode plusOne(ListNode head) {
            ListNode dummy = new ListNode(0);
            dummy.next = head;
            ListNode i = dummy;
            ListNode j = dummy;
    
            while (j.next != null) {
                j = j.next;
                if (j.val != 9) {
                    i = j;
                }
            }
            // i = index of last non-9 digit
        
            i.val++;
            i = i.next;
            while (i != null) {
                i.val = 0;
                i = i.next;
            }
            
            if (dummy.val == 0) return dummy.next;
            return dummy;
        }
    }
</p>


