---
title: "Guess Number Higher or Lower"
weight: 357
#id: "guess-number-higher-or-lower"
---
## Description
<div class="description">
<p>We are playing the Guess Game. The game is as follows:</p>

<p>I pick a number from <code>1</code> to <code>n</code>. You have to guess which number I picked.</p>

<p>Every time you guess wrong, I will tell you whether the number I picked is higher or lower than your guess.</p>

<p>You call a pre-defined API <code>int guess(int num)</code>, which returns 3 possible results:</p>

<ul>
	<li><code>-1</code>: The number I picked is lower than your guess (i.e. <code>pick &lt; num</code>).</li>
	<li><code>1</code>: The number I picked is higher than your guess (i.e. <code>pick &gt; num</code>).</li>
	<li><code>0</code>: The number I picked is equal to your guess (i.e. <code>pick == num</code>).</li>
</ul>

<p>Return <em>the number that I picked</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> n = 10, pick = 6
<strong>Output:</strong> 6
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> n = 1, pick = 1
<strong>Output:</strong> 1
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> n = 2, pick = 1
<strong>Output:</strong> 1
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> n = 2, pick = 2
<strong>Output:</strong> 2
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 2<sup>31</sup> - 1</code></li>
	<li><code>1 &lt;= pick &lt;= n</code></li>
</ul>

</div>

## Tags
- Binary Search (binary-search)

## Companies
- Google - 2 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force

We check every number from 1 to n-1 and pass it to the $$guess$$ function. The number
for which a 0 is returned is the required answer.

<iframe src="https://leetcode.com/playground/fbFHDKsc/shared" frameBorder="0" width="100%" height="276" name="fbFHDKsc"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We scan all the numbers from 1 to n.
* Space complexity : $$O(1)$$. No extra space is used.
<br>
<br>

---
#### Approach 2: Using Binary Search

**Algorithm**

We can apply Binary Search to find the given number. We start with the mid
number. We pass that number to the $$guess$$ function. If it returns a -1, it implies that the guessed number is larger than the required one. Thus, we use Binary Search for numbers lower than itself. Similarly, if it returns a 1, we use Binary Search
 for numbers higher than itself.

<iframe src="https://leetcode.com/playground/84NfKnsE/shared" frameBorder="0" width="100%" height="429" name="84NfKnsE"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(\log_2 n\big)$$. Binary Search is used.
* Space complexity : $$O(1)$$. No extra space is used.
<br>
<br>

---
#### Approach 3: Ternary Search

**Algorithm**

In Binary Search, we choose the middle element as the pivot in splitting. In Ternary Search, we choose two pivots (say $$m1$$ and $$m2$$) such that the given range is divided into three equal parts. If the required number $$num$$ is less than $$m1$$ then we apply ternary search on the left segment of $$m1$$. If $$num$$ lies between $$m1$$ and $$m2$$, we apply ternary search between $$m1$$ and $$m2$$. Otherwise we will search in the segment right to $$m2$$.

<iframe src="https://leetcode.com/playground/ZVkdvE5j/shared" frameBorder="0" width="100%" height="500" name="ZVkdvE5j"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(\log_3 n \big)$$. Ternary Search is used.
* Space complexity : $$O(1)$$. No extra space is used.
<br>
<br>

---
## Follow up

It seems that ternary search is able to terminate earlier compared to binary search. But why is binary search more widely used?

#### Comparisons between Binary Search and Ternary Search

Ternary Search is worse than Binary Search. The following outlines the recursive formula to count comparisons of Binary Search in the worst case.

$$
\begin{aligned}
T(n) &= T\bigg(\frac{n}{2} \ \bigg) + 2, \quad T(1) = 1 \\
T\bigg(\frac{n}{2} \ \bigg) &= T\bigg(\frac{n}{2^2} \ \bigg) + 2 \\
\\
\therefore{} \quad T(n) &= T\bigg(\frac{n}{2^2} \ \bigg) + 2 \times 2 \\
&= T\bigg(\frac{n}{2^3} \ \bigg) + 3 \times 2 \\
&= \ldots \\
&= T\bigg(\frac{n}{2^{\log_2 n}} \ \bigg) + 2 \log_2 n \\
&= T(1) + 2 \log_2 n \\
&= 1 + 2 \log_2 n
\end{aligned}
$$

The following outlines the recursive formula to count comparisons of Ternary Search in the worst case.

$$
\begin{aligned}
T(n) &= T\bigg(\frac{n}{3} \ \bigg) + 4, \quad T(1) = 1 \\
T\bigg(\frac{n}{3} \ \bigg) &= T\bigg(\frac{n}{3^2} \ \bigg) + 4 \\
\\
\therefore{} \quad T(n) &= T\bigg(\frac{n}{3^2} \ \bigg) + 2 \times 4 \\
&= T\bigg(\frac{n}{3^3} \ \bigg) + 3 \times 4 \\
&= \ldots \\
&= T\bigg(\frac{n}{3^{\log_3 n}} \ \bigg) + 4 \log_3 n \\
&= T(1) + 4 \log_3 n \\
&= 1 + 4 \log_3 n
\end{aligned}
$$

As shown above, the total comparisons in the worst case for ternary and binary search are $$1 + 4 \log_3 n$$ and $$1 + 2 \log_2 n$$ comparisons respectively. To determine which is larger, we can just look at the expression $$2 \log_3 n$$ and $$\log_2 n$$ . The expression $$2 \log_3 n$$ can be written as $$\frac{2}{\log_2 3} \times \log_2 n$$ . Since the value of $$\frac{2}{\log_2 3}$$ is greater than one, Ternary Search does more comparisons than Binary Search in the worst case.

Analysis written by: [@vinod23](https://leetcode.com/vinod23)

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### The key point is to read the problem carefully.
- Author: Nakanu
- Creation Date: Wed Jul 13 2016 19:55:48 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 10:14:09 GMT+0800 (Singapore Standard Time)

<p>
-1 : My number is lower
 1 : My number is higher
 0 : Congrats! You got it!

Here "My" means the number which is given for you to guess not the number you put into      **guess(int num).**
</p>


### 2 lines as usual
- Author: StefanPochmann
- Creation Date: Wed Jul 13 2016 19:53:45 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 21:30:35 GMT+0800 (Singapore Standard Time)

<p>
Using binary search to find the smallest number that's not too small.

```
    def guessNumber(self, n):
        class C: __getitem__ = lambda _, i: -guess(i)
        return bisect.bisect(C(), -1, 1, n)
```

Alternatively, without using the library:

```
    def guessNumber(self, n):
        lo, hi = 1, n
        while lo < hi:
            mid = (lo + hi) / 2
            if guess(mid) == 1:
                lo = mid + 1
            else:
                hi = mid
        return lo
```

Funny variation:

```
    def guessNumber(self, n):
        lo, hi = 1, n
        while lo < hi:
            mid = (lo + hi) / 2
            lo, hi = ((mid, mid), (mid+1, hi), (lo, mid-1))[guess(mid)]
        return lo
```
</p>


### 0ms c++ binary search
- Author: Dusg
- Creation Date: Thu Jul 14 2016 16:20:08 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 13 2018 18:13:48 GMT+0800 (Singapore Standard Time)

<p>
```
int guessNumber(int n) {
        int maxNumber = n, minNumber = 1;
        while (true) {
            int meanNumber = (maxNumber - minNumber) / 2 + minNumber;
            // Do NOT use (maxNumber+minNumber)/2 in case of over flow
            int res = guess(meanNumber);
            if (res == 0) { 
                return meanNumber;
            } else if (res == 1) {
                minNumber = meanNumber + 1;
            } else {
                maxNumber = meanNumber - 1;
            }
        }
    }
```
</p>


