---
title: "Remove Duplicates from Sorted List II"
weight: 82
#id: "remove-duplicates-from-sorted-list-ii"
---
## Description
<div class="description">
<p>Given a sorted linked list, delete all nodes that have duplicate numbers, leaving only <em>distinct</em> numbers from the original list.</p>

<p>Return the linked list sorted as well.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 1-&gt;2-&gt;3-&gt;3-&gt;4-&gt;4-&gt;5
<strong>Output:</strong> 1-&gt;2-&gt;5
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> 1-&gt;1-&gt;1-&gt;2-&gt;3
<strong>Output:</strong> 2-&gt;3
</pre>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Adobe - 5 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Salesforce - 6 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My accepted Java code
- Author: snowfish
- Creation Date: Wed Oct 08 2014 12:47:15 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 17:26:41 GMT+0800 (Singapore Standard Time)

<p>
    public ListNode deleteDuplicates(ListNode head) {
            if(head==null) return null;
            ListNode FakeHead=new ListNode(0);
            FakeHead.next=head;
            ListNode pre=FakeHead;
            ListNode cur=head;
            while(cur!=null){
                while(cur.next!=null&&cur.val==cur.next.val){
                    cur=cur.next;
                }
                if(pre.next==cur){
                    pre=pre.next;
                }
                else{
                    pre.next=cur.next;
                }
                cur=cur.next;
            }
            return FakeHead.next;
        }
</p>


### Python in-place solution with dummy head node.
- Author: OldCodingFarmer
- Creation Date: Sat Aug 15 2015 04:00:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 19:36:32 GMT+0800 (Singapore Standard Time)

<p>
        
    def deleteDuplicates(self, head):
        dummy = pre = ListNode(0)
        dummy.next = head
        while head and head.next:
            if head.val == head.next.val:
                while head and head.next and head.val == head.next.val:
                    head = head.next
                head = head.next
                pre.next = head
            else:
                pre = pre.next
                head = head.next
        return dummy.next
</p>


### My Recursive Java Solution
- Author: totalheap
- Creation Date: Mon Nov 17 2014 08:26:40 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 14 2018 15:58:12 GMT+0800 (Singapore Standard Time)

<p>
    public ListNode deleteDuplicates(ListNode head) {
        if (head == null) return null;
        
        if (head.next != null && head.val == head.next.val) {
            while (head.next != null && head.val == head.next.val) {
                head = head.next;
            }
            return deleteDuplicates(head.next);
        } else {
            head.next = deleteDuplicates(head.next);
        }
        return head;
    }
    


if current node is not unique, return deleteDuplicates with head.next. 
If current node is unique, link it to the result of next list made by recursive call. Any improvement?
</p>


