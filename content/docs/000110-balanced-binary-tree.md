---
title: "Balanced Binary Tree"
weight: 110
#id: "balanced-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree, determine if it is height-balanced.</p>

<p>For this problem, a height-balanced binary tree is defined as:</p>

<blockquote>
<p>a binary tree in which the left and right subtrees of <em>every</em> node differ in height by no more than 1.</p>
</blockquote>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/10/06/balance_1.jpg" style="width: 342px; height: 221px;" />
<pre>
<strong>Input:</strong> root = [3,9,20,null,null,15,7]
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/10/06/balance_2.jpg" style="width: 452px; height: 301px;" />
<pre>
<strong>Input:</strong> root = [1,2,2,3,3,null,null,4,4]
<strong>Output:</strong> false
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = []
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is in the range <code>[0, 5000]</code>.</li>
	<li><code>-10<sup>4</sup> &lt;= Node.val &lt;= 10<sup>4</sup></code></li>
</ul>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Google - 4 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

Given the definition of a balanced tree
we know that a tree $$T$$ is *not* balanced if and only if there is some node
$$p\in T$$ such that $$|\texttt{height}(p.left) - \texttt{height}(p.right)| > 1$$. 
The tree below has each node is labeled by its height, 
as well as the unbalanced subtree highlighted.

<center>

![pic](../Figures/110/110-unbalanced-wheight-highlighted.png)

</center>

> The balanced subtree definition hints at the fact that we should treat each 
> subtree as a subproblem. The question is: in which order should we solve the 
> subproblems?

---

#### Approach 1: Top-down recursion

**Algorithm**

First we define a function $$\texttt{height}$$ such that  for any node
$$p\in T$$

<center>
$$
\texttt{height}(p) = 
\begin{cases}
-1 & p \text{ is an empty subtree i.e. } \texttt{null}\\
1 + \max(\texttt{height}(p.left), \texttt{height}(p.right)) & \text{ otherwise}
\end{cases}
$$
</center>

Now that we have a method for determining the height of a tree, 
all that remains is to compare the height of every node's children. A tree $$T$$ 
rooted at $$r$$ is balanced if and only if the height of its two children are within
1 of each other and the subtrees at each child are also balanced. Therefore, we can 
compare the two child subtrees' heights then recurse on each one. 

```
isBalanced(root):
    if (root == NULL):
        return true
    if (abs(height(root.left) - height(root.right)) > 1):
        return false
    else:
        return isBalanced(root.left) && isBalanced(root.right)

```

<iframe src="https://leetcode.com/playground/7sL2MFc5/shared" frameBorder="0" width="100%" height="463" name="7sL2MFc5"></iframe>

<center>

!?!../Documents/110_Balanced_Binary_Tree_topdown.json:1000,500!?!

</center>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n\log n)$$
    * For a node $$p$$ at depth $$d$$, $$\texttt{height}(p)$$ will be called $$d$$ times.

    * We first need to obtain a bound on the height of a balanced tree. Let
    $$f(h)$$ represent the minimum number of nodes in a balanced tree with height $$h$$. 
    We have the relation

    <center>
    $$
    f(h) = f(h - 1) + f(h - 2) + 1
    $$
    </center>

    which looks nearly identical to the Fibonacci recurrence relation. In 
    fact, the complexity analysis for $$f(h)$$ is similar and we claim that the lower 
    bound is $$f(h) = \Omega\left(\left(\frac{3}{2}\right)^h\right)$$. 
    
    <center>      
    $$                                                                          
     \begin{align}                                                                   
    f(h+1) &= f(h) + f(h-1) + 1 \\                                                  
    &> f(h) + f(h-1) & \qquad\qquad \text{This is the fibonacci sequence}\\               
    &\geq \left(\frac{3}{2}\right)^{h} + \left(\frac{3}{2}\right)^{h-1} & \text{via our claim} \\
    &= \frac{5}{2} \left(\frac{3}{2}\right)^{h-1}\\
    &> \frac{9}{4} \left(\frac{3}{2}\right)^{h-1} & \frac{9}{4} < \frac{5}{2}\\
    &> \left(\frac{3}{2}\right)^{h+1}
    \end{align}                                                                     
    $$                                                                              
    </center>  
    
    Therefore, the height $$h$$ of a balanced tree
    is bounded by $$\mathcal{O}(\log_{1.5}(n))$$. With this bound we can guarantee that 
    $$\texttt{height}$$ will be called
    on each node $$\mathcal{O}(\log n)$$ times.

    * If our algorithm didn't have any early-stopping, we may end up having 
    $$\mathcal{O}(n^2)$$ complexity if our tree is skewed since height is bounded by $$\mathcal{O}(n)$$.
    However, it is important to note that we stop recursion as soon as the 
    height of a node's children are not within 1. In fact, in the skewed-tree
    case our algorithm is bounded by $$\mathcal{O}(n)$$, as it only checks the height of 
    the first two subtrees.
        
* Space complexity : $$\mathcal{O}(n)$$. The recursion stack may contain all nodes if the 
    tree is skewed.

**Fun fact**: $$f(n) = f(n-1) + f(n-2) + 1$$ is known as a [Fibonacci meanders](http://oeis.org/wiki/User:Peter_Luschny/FibonacciMeanders)
sequence.


<br />

---

#### Approach 2: Bottom-up recursion

**Intuition** 

In approach 1, we perform redundant calculations when computing $$\texttt{height}$$.
In each call to $$\texttt{height}$$, we require that the subtree's heights also be
computed. Therefore, when working top down we will compute the height of a subtree
once for every parent. We can remove the redundancy by first recursing  on the
children  of the current node and then using their computed height to determine 
whether the current node is balanced.

**Algorithm**

We will use the same $$\texttt{height}$$ defined in the first approach. The 
bottom-up approach is a reverse of the logic of the top-down approach
since we *first* check if the child subtrees are balanced *before*
comparing their heights. The algorithm is as follows: 

> Check if the child subtrees are balanced. If they are, use their
> heights to determine if the current subtree is balanced as well as to calculate
> the current subtree's height.


<iframe src="https://leetcode.com/playground/eXoedT2T/shared" frameBorder="0" width="100%" height="500" name="eXoedT2T"></iframe>

<center>

!?!../Documents/110_Balanced_Binary_Tree_bottomup.json:1000,500!?!

</center>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n)$$

    For every subtree, we compute its height in constant time as well as 
    compare the height of its children. 
* Space complexity : $$\mathcal{O}(n)$$. The recursion stack may go up to $$\mathcal{O}(n)$$ if the tree is unbalanced.


<br />

## Accepted Submission (golang)
```golang
import (
	"fmt"
	"math"
	"math/big"
	"sort"
	"strings"
	"strconv"
)

func unused() int {
	fmt.Println("no bugger")
	math.Abs(0)
	sort.Ints([]int{0})
	strings.Title("hello world")
	strconv.Atoi("0")
	fmt.Println(big.NewInt(0))
	return 0
}

func prl(a ...interface{}) (n int, err error) {
	//return fmt.Println(a)
	return 0, nil
}

/**
 * Definition for binary tree
 * type TreeNode struct {
 *     left *TreeNode
 *     value int
 *     right *TreeNode
 * }
 * 
 * func TreeNode_new(val int) *TreeNode{
 *     var node *TreeNode = new(TreeNode)
 *     node.value=val
 *     node.Left=nil
 *     node.Right=nil
 *     return node
 * }
 */

func height(node *TreeNode) int {
    if node == nil {
        return 0
    }
    lh := height(node.Left)
    rh := height(node.Right)
    if lh == -1 || rh == -1 {
        return -1
    }
    hd := lh - rh
    if hd > 1 || hd < -1 {
        return -1
    }
    h := lh
    if h < rh {
        h = rh
    }
    //prl(h)
    return h + 1
}

func heightmemo(node *TreeNode, memo map[*TreeNode]int) int {
    if node == nil {
        return 0
    }
    mh, in := memo[node]
    if in {
        return mh
    }
    lh := heightmemo(node.Left, memo)
    rh := heightmemo(node.Right, memo)
    h := lh
    if h < rh {
        h = rh
    }
    h++
    memo[node] = h
    //prl("height:", node, h)
    return h
}

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isBalanced(root *TreeNode) bool {
    /*
    memo := map[*TreeNode]int{}
    prl("dp verdict", isBalancedUsingDp(A, memo))
    return isBalancedUsingDp(A, memo)
    */
    h := height(root)
    return h != -1
}

func isBalancedUsingDp(root *TreeNode, memo map[*TreeNode]int) int {
    if root == nil {
        return 1
    }
    lh := heightmemo(root.Left, memo)
    rh := heightmemo(root.Right, memo)
    hd := lh - rh
    prl("lh", lh, "rh", rh, "hd", hd)
    if hd > 1 || hd < -1 {
        return 0
    }
    if isBalancedUsingDp(root.Left, memo) == 1 && isBalancedUsingDp(root.Right, memo) == 1 {
        return 1
    }
    return 0
}

```

## Top Discussions
### The bottom up O(N) solution would be better
- Author: benlong
- Creation Date: Fri Jan 23 2015 15:59:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 10:42:15 GMT+0800 (Singapore Standard Time)

<p>
This problem is generally believed to have two solutions: the top down approach and the bottom up way.

1.The first method checks whether the tree is balanced strictly according to the definition of balanced binary tree: the difference between the heights of the two sub trees are not bigger than 1, and both the left sub tree and right sub tree are also balanced. With the helper function depth(), we could easily write the code; 

    class solution {
    public:
        int depth (TreeNode *root) {
            if (root == NULL) return 0;
            return max (depth(root -> left), depth (root -> right)) + 1;
        }
    
        bool isBalanced (TreeNode *root) {
            if (root == NULL) return true;
            
            int left=depth(root->left);
            int right=depth(root->right);
            
            return abs(left - right) <= 1 && isBalanced(root->left) && isBalanced(root->right);
        }
    };

For the current node root, calling depth() for its left and right children actually has to access all of its children, thus the complexity is O(N). We do this for each node in the tree, so the overall complexity of isBalanced will be O(N^2). This is a top down approach.

2.The second method is based on DFS. Instead of calling depth() explicitly for each child node, we return the height of the current node in DFS recursion. When the sub tree of the current node (inclusive) is balanced, the function dfsHeight() returns a non-negative value as the height. Otherwise -1 is returned.  According to the leftHeight and rightHeight of the two children, the parent node could check if the sub tree
is balanced, and decides its return value.

    class solution {
    public:
    int dfsHeight (TreeNode *root) {
            if (root == NULL) return 0;
            
            int leftHeight = dfsHeight (root -> left);
            if (leftHeight == -1) return -1;
            int rightHeight = dfsHeight (root -> right);
            if (rightHeight == -1) return -1;
            
            if (abs(leftHeight - rightHeight) > 1)  return -1;
            return max (leftHeight, rightHeight) + 1;
        }
        bool isBalanced(TreeNode *root) {
            return dfsHeight (root) != -1;
        }
    };

In this bottom up approach, each node in the tree only need to be accessed once. Thus the time complexity is O(N), better than the first solution.
</p>


### VERY SIMPLE Python solutions (iterative and recursive), both beat 90%
- Author: agave
- Creation Date: Tue Apr 19 2016 03:44:00 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 05:27:15 GMT+0800 (Singapore Standard Time)

<p>
```  
class Solution(object):
    def isBalanced(self, root):
            
        def check(root):
            if root is None:
                return 0
            left  = check(root.left)
            right = check(root.right)
            if left == -1 or right == -1 or abs(left - right) > 1:
                return -1
            return 1 + max(left, right)
            
        return check(root) != -1

# 226 / 226 test cases passed.
# Status: Accepted
# Runtime: 80 ms
```


Iterative, based on postorder traversal:

```
class Solution(object):
    def isBalanced(self, root):
        stack, node, last, depths = [], root, None, {}
        while stack or node:
            if node:
                stack.append(node)
                node = node.left
            else:
                node = stack[-1]
                if not node.right or last == node.right:
                    node = stack.pop()
                    left, right  = depths.get(node.left, 0), depths.get(node.right, 0)
                    if abs(left - right) > 1: return False
                    depths[node] = 1 + max(left, right)
                    last = node
                    node = None
                else:
                    node = node.right
        return True


# 226 / 226 test cases passed.
# Status: Accepted
# Runtime: 84 ms

```
</p>


### Java solution based on height, check left and right node in every recursion to avoid further useless search
- Author: mingyuan
- Creation Date: Mon Mar 30 2015 17:25:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 14:06:07 GMT+0800 (Singapore Standard Time)

<p>

    public boolean isBalanced(TreeNode root) {
        if(root==null){
            return true;
        }
        return height(root)!=-1;
        
    }
    public int height(TreeNode node){
        if(node==null){
            return 0;
        }
        int lH=height(node.left);
        if(lH==-1){
            return -1;
        }
        int rH=height(node.right);
        if(rH==-1){
            return -1;
        }
        if(lH-rH<-1 || lH-rH>1){
            return -1;
        }
        return Math.max(lH,rH)+1;
    }
</p>


