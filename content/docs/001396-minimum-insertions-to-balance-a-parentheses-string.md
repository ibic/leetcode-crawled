---
title: "Minimum Insertions to Balance a Parentheses String"
weight: 1396
#id: "minimum-insertions-to-balance-a-parentheses-string"
---
## Description
<div class="description">
<p>Given a parentheses string <code>s</code> containing only the characters <code>&#39;(&#39;</code> and <code>&#39;)&#39;</code>. A parentheses string is <strong>balanced</strong> if:</p>

<ul>
	<li>Any left parenthesis&nbsp;<code>&#39;(&#39;</code>&nbsp;must have a corresponding two consecutive right parenthesis&nbsp;<code>&#39;))&#39;</code>.</li>
	<li>Left parenthesis&nbsp;<code>&#39;(&#39;</code>&nbsp;must go before the corresponding two&nbsp;consecutive right parenthesis&nbsp;<code>&#39;))&#39;</code>.</li>
</ul>

<p>In other words, we treat <code>&#39;(&#39;</code> as openning parenthesis and <code>&#39;))&#39;</code> as closing parenthesis.</p>

<p>For example, <code>&quot;())&quot;</code>, <code>&quot;())(())))&quot;</code> and <code>&quot;(())())))&quot;</code> are&nbsp;balanced, <code>&quot;)()&quot;</code>, <code>&quot;()))&quot;</code> and <code>&quot;(()))&quot;</code> are not balanced.</p>

<p>You can insert the characters <code>&#39;(&#39;</code> and <code>&#39;)&#39;</code> at any position of the string to balance it if needed.</p>

<p>Return <em>the minimum number of insertions</em> needed to make <code>s</code> balanced.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;(()))&quot;
<strong>Output:</strong> 1
<strong>Explanation:</strong> The second &#39;(&#39; has two matching &#39;))&#39;, but the first &#39;(&#39; has only &#39;)&#39; matching. We need to to add one more &#39;)&#39; at the end of the string to be &quot;(())))&quot; which is balanced.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;())&quot;
<strong>Output:</strong> 0
<strong>Explanation:</strong> The string is already balanced.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;))())(&quot;
<strong>Output:</strong> 3
<strong>Explanation:</strong> Add &#39;(&#39; to match the first &#39;))&#39;, Add &#39;))&#39; to match the last &#39;(&#39;.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;((((((&quot;
<strong>Output:</strong> 12
<strong>Explanation:</strong> Add 12 &#39;)&#39; to balance the string.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;)))))))&quot;
<strong>Output:</strong> 5
<strong>Explanation:</strong> Add 4 &#39;(&#39; at the beginning of the string and one &#39;)&#39; at the end. The string becomes &quot;(((())))))))&quot;.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10^5</code></li>
	<li><code>s</code> consists of <code>&#39;(&#39;</code> and <code>&#39;)&#39;</code> only.</li>
</ul>

</div>

## Tags
- String (string)
- Stack (stack)

## Companies
- CasaOne - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Straight Forward One Pass
- Author: lee215
- Creation Date: Sun Aug 09 2020 01:41:20 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 10 2020 16:41:15 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Similar to [921. Minimum Add to Make Parentheses Valid](https://leetcode.com/problems/minimum-add-to-make-parentheses-valid/discuss/181132/C++JavaPython-Straight-Forward-One-Pass).
Just need to pay attention:
left parenthesis \'(\' must have a corresponding two **consecutive** right parenthesis \'))\'.
<br>

# **Explanation**
`res` represents the number of left/right parentheses already added.
`right` represents the number of right parentheses needed.

**1) case `)`**
If we meet a right parentheses , `right--`.
If `right < 0`, we need to add a left parentheses before it.
Then we update `right += 2` and `res++`
This part is easy and normal.

**2) case `(`**
If we meet a left parentheses,
we check if we have odd number `\')\'` before.
If we `right`, we have odd `\')\'` before,
but we want right parentheses in paires.
So add one `\')\'` here, then update `right--; res++;`.
Note that this part is not necessary if two **consecutive** right parenthesis not required.

Because we have `)`, we update `right += 2`.
<br>

# **Dry run**
All by @himanshusingh11:

**Example 1: Consider ((()(,n= 5 ,i=0,1...4**
i=0, we have ( it means we need two right parenthesis (they are in pair) so.. right+=2 => res =0, right =2
i=1, again we have ( it means we need two right parenthesis (they are in pair) so.. right+=2 => res =0, right =4
i=2, again we have ( it means we need two right parenthesis (they are in pair) so.. right+=2 => res =0, right =6
i=3, we have ) we subtract one from right. so.. right-- => res =0, right =5
i=4, we have ( but here right is odd so we need to make it even with right-- and increment res++ => res =1, right =4. Also, as we have got a left parenthesis then we need two right parenthesis (they are in pair) so.. right+=2 => res =1, right =6

finally ans is res + right => 1 +6 == 7

**Example 2: ((()**
Similarly, we can see when we have right<0 then we increment res by one & add 2 to right as they should be in pairs..
<br>

# **Complexity**
Time `O(N)`
Space `O(1)`
<br>

**Java:**
```java
    public int minInsertions(String s) {
        int res = 0, right = 0;
        for (int i = 0; i < s.length(); ++i) {
            if (s.charAt(i) == \'(\') {
                if (right % 2 > 0) {
                    right--;
                    res++;
                }
                right += 2;
            } else {
                right--;
                if (right < 0) {
                    right += 2;
                    res++;
                }
            }
        }
        return right + res;
    }
```
**C++:**
```cpp
    int minInsertions(string s) {
        int res = 0, right = 0;
        for (char &c: s) {
            if (c == \'(\') {
                if (right % 2 > 0) {
                    right--;
                    res++;
                }
                right += 2;
            } else {
                right--;
                if (right < 0) {
                    right += 2;
                    res++;
                }
            }
        }
        return right + res;
    }
```
**Python:**
```py
    def minInsertions(self, s):
        res = right = 0
        for c in s:
            if c == \'(\':
                if right % 2:
                    right -= 1
                    res += 1
                right += 2
            if c == \')\':
                right -= 1
                if right < 0:
                    right += 2
                    res += 1
        return right + res
```

</p>


### How output of "(()))(()))()())))" is 4 and not 1
- Author: chrisTris
- Creation Date: Sun Aug 09 2020 00:18:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 09 2020 00:19:34 GMT+0800 (Singapore Standard Time)

<p>
For test case "(()))(()))()())))" expected is 4
but consider steps
(()))(()))()()))) => Removing all ()) => ()()()))
()()())) => Removing ()) => ()())
()()) => Removing ()) => ()
() => insert ) => ()) => ""
</p>


### Simple O(n) stack solution with detailed explanation
- Author: vbsinha
- Creation Date: Sun Aug 09 2020 00:03:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 30 2020 07:47:15 GMT+0800 (Singapore Standard Time)

<p>
**Brief Idea**: Maintain a stack. Every time we see a ```(``` we need to have 2 ```)``` to balance. So we would push 2 to the stack. Then when we see ```)``` we would check the top of the stack. If the top is 2, that means this is the first ```)``` that will match the previous ```(```. So we would change the top to 1. In case the top of stack had 1 it means we are seeing the second ```)``` for some ```(``` and so we just pop this 1 from the stack. 

**Details of transition**:
1. When we see ```(```:
* And the stack is empty or has a 2 at the top: we just push another 2.
 * And the stack had 1 at the top: This means we have just seen ```()``` and now we are seeing ```(```. In this case, we need to first provide a ```)``` to the previous ```(``` and then we can process the current ```(```. So increment the answer, pop the 1 (which was for last ```(```) and push a 2 (for current ```(```).
2. When we see ```)```:
 * And the stack is empty: We have encountered a lone ```)```. We would need to add ```(``` to the sequence to balance, which would match one of its ```)``` with the current ```)```. So we increment the answer.  Moreover, it would also need another ```)``` to match. So we push 1 to the top. This is like saying we have seen ```()``` (of which ```(``` was inserted by us).
* And the stack had 1 at the top: This the second ```)``` for some ```(```. We just pop from the stack.
* And the stack had 2 at the top: This the first ```)``` for some ```(```. We just pop 2 from the stack and push 1.
At the end of going through the string we just add up all the numbers in stack, which represents the number of ```)``` we need to balance.

```
class Solution {
public:
    int minInsertions(string s) {
        int ans = 0;
        stack<int> t;
        for (char c : s) {
            if (c == \'(\') {
                if (t.empty() || t.top() == 2) t.push(2);
                else {
                    t.pop();
                    t.push(2);
                    ans++;
                }
            }
            else {
                if (t.empty()) {
                    t.push(1); ans++;
                } else if (t.top() == 1) {
                    t.pop();
                } else if (t.top() == 2) {
                    t.pop(); t.push(1);
                }
            }
        }
        while (!t.empty()) {
            ans += t.top();
            t.pop();
        }
        return ans;
    }
};
```
</p>


