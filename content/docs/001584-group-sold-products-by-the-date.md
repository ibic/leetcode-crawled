---
title: "Group Sold Products By The Date"
weight: 1584
#id: "group-sold-products-by-the-date"
---
## Description
<div class="description">
<p>Table <code>Activities</code>:</p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| sell_date   | date    |
| product     | varchar |
+-------------+---------+
There is no primary key for this table, it may contains duplicates.
Each row of this table contains the product name and the date it was sold in a market.</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find for each date, the number of distinct products sold and their names.</p>

<p>The sold-products names for each date should be sorted lexicographically.&nbsp;</p>

<p>Return the result table ordered by <code>sell_date</code>.</p>

<p>The query result format is in the following example.</p>

<pre>
<code>Activities</code> table:
+------------+-------------+
| sell_date  | product     |
+------------+-------------+
| 2020-05-30 | Headphone   |
| 2020-06-01 | Pencil      |
| 2020-06-02 | Mask        |
| 2020-05-30 | Basketball  |
| 2020-06-01 | Bible       |
| 2020-06-02 | Mask        |
| 2020-05-30 | T-Shirt     |
+------------+-------------+

Result table:
+------------+----------+------------------------------+
| sell_date  | num_sold | products                     |
+------------+----------+------------------------------+
| 2020-05-30 | 3        | Basketball,Headphone,T-shirt |
| 2020-06-01 | 2        | Bible,Pencil                 |
| 2020-06-02 | 1        | Mask                         |
+------------+----------+------------------------------+
For 2020-05-30, Sold items were (Headphone, Basketball, T-shirt), we sort them lexicographically and separate them by comma.
For 2020-06-01, Sold items were (Pencil, Bible), we sort them lexicographically and separate them by comma.
For 2020-06-02, Sold item is (Mask), we just return it.

</pre>

</div>

## Tags


## Companies
- Startup - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL Order by Product name AND Sell date
- Author: keyvank1
- Creation Date: Thu Jun 18 2020 07:03:29 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 18 2020 07:04:53 GMT+0800 (Singapore Standard Time)

<p>
Almost immediately detected, the only serious challange of this problem is how to aggregate the product names in one cell. In MySql, this can be done using GROUP_CONCAT, in which you can also specify the sorting mechanism for the group concatenation (aggregation). The rest is simple.
```
SELECT sell_date,
		COUNT(DISTINCT(product)) AS num_sold, 
		GROUP_CONCAT(DISTINCT product ORDER BY product ASC SEPARATOR \',\') AS products
FROM Activities
GROUP BY sell_date
ORDER BY sell_date ASC
</p>


### MS SQL SERVER Simple Solution
- Author: nazir_cinu
- Creation Date: Fri Jul 24 2020 12:02:37 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 24 2020 12:02:37 GMT+0800 (Singapore Standard Time)

<p>
```
select sell_date, 
    COUNT(product) as num_sold,
    STRING_AGG(product,\',\') WITHIN GROUP (ORDER BY product) as products from
    (select distinct sell_date,product FROM Activities) Act
    GROUP BY sell_date
```
</p>


### Oracle Solution - LISTAGG
- Author: ppal87
- Creation Date: Thu Jun 18 2020 21:45:38 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 18 2020 21:45:38 GMT+0800 (Singapore Standard Time)

<p>
```
WITH cte as(
select distinct sell_date, product from Activities)

select TO_CHAR(sell_date,\'YYYY-MM-DD\') as sell_date, count(product) as num_sold,
LISTAGG(product, \',\') WITHIN GROUP (ORDER BY product) as products
from cte
group by sell_date
order by sell_date
```
</p>


