---
title: "Game Play Analysis V"
weight: 1527
#id: "game-play-analysis-v"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Activity</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| player_id    | int     |
| device_id    | int     |
| event_date   | date    |
| games_played | int     |
+--------------+---------+
(player_id, event_date) is the primary key of this table.
This table shows the activity of players of some game.
Each row is a record of a player who logged in and played a number of games (possibly 0) before logging out on some day using some device.
</pre>

<p>&nbsp;</p>

<p>We define the <em>install date</em> of a player to be the first login day of that player.</p>

<p>We also define <em>day 1 retention</em>&nbsp;of some date <code>X</code>&nbsp;to be the number&nbsp;of players whose install date is&nbsp;<code>X</code>&nbsp;and they logged back in on the day right after <code>X</code>, divided by the number of players whose install date is&nbsp;<code>X</code>, <strong>rounded to 2 decimal places</strong>.</p>

<p>Write an SQL query that reports for each <strong>install date</strong>, the <strong>number&nbsp;of players</strong> that installed the game on that day and the <strong>day 1 retention</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
Activity table:
+-----------+-----------+------------+--------------+
| player_id | device_id | event_date | games_played |
+-----------+-----------+------------+--------------+
| 1         | 2         | 2016-03-01 | 5            |
| 1         | 2         | 2016-03-02 | 6            |
| 2         | 3         | 2017-06-25 | 1            |
| 3         | 1         | 2016-03-01 | 0            |
| 3         | 4         | 2016-07-03 | 5            |
+-----------+-----------+------------+--------------+

Result table:
+------------+----------+----------------+
| install_dt | installs | Day1_retention |
+------------+----------+----------------+
| 2016-03-01 | 2        | 0.50           |
| 2017-06-25 | 1        | 0.00           |
+------------+----------+----------------+
Player 1 and 3 installed the game on 2016-03-01 but only player 1 logged back in on 2016-03-02 so the day 1 retention of 2016-03-01 is 1 / 2 = 0.50
Player 2 installed the game on 2017-06-25 but didn&#39;t log back in on 2017-06-26 so the day 1 retention of 2017-06-25 is 0 / 1 = 0.00
</pre>

</div>

## Tags


## Companies
- GSN Games - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Intuitive MySQL answer
- Author: tschijnmo
- Creation Date: Thu Jun 27 2019 10:53:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 27 2019 10:53:28 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT install_dt, COUNT(player_id) AS installs,
ROUND(COUNT(next_day) / COUNT(player_id), 2) AS Day1_retention
FROM (
    SELECT a1.player_id, a1.install_dt, a2.event_date AS next_day
    FROM
    (
        SELECT player_id, MIN(event_date) AS install_dt 
        FROM Activity
        GROUP BY player_id
    ) AS a1 
    LEFT JOIN Activity AS a2
    ON a1.player_id = a2.player_id
    AND a2.event_date = a1.install_dt + 1
) AS t
GROUP BY install_dt;
```
</p>


### mysql solution
- Author: maoyic
- Creation Date: Sat Jun 29 2019 04:11:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 29 2019 04:11:19 GMT+0800 (Singapore Standard Time)

<p>
select a1.event_date as install_dt, count(distinct a1.player_id)as installs,round(sum(case when a2.event_date is not null then 1 else 0 end)/count(distinct a1.player_id),2) as Day1_retention
from
(select player_id,min(event_date) as event_date
from Activity
group by player_id) a1 left join Activity a2 on a1.player_id=a2.player_id and a1.event_date=a2.event_date-1
group by install_dt
</p>


### Simple MySQL answer
- Author: vijay_collooru
- Creation Date: Sat Aug 10 2019 03:43:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 10 2019 03:43:07 GMT+0800 (Singapore Standard Time)

<p>
select A.event_date as install_dt, count(A.player_id) as installs, round(count(B.player_id)/count(A.player_id),2) as Day1_retention 
from (select player_id, min(event_date) AS event_date from Activity group by player_id) AS A
left join Activity B
ON A.player_id = B.player_id
and A.event_date + 1 = B.event_Date
group by A.event_date
</p>


