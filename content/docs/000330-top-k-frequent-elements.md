---
title: "Top K Frequent Elements"
weight: 330
#id: "top-k-frequent-elements"
---
## Description
<div class="description">
<p>Given a non-empty array of integers, return the <b><i>k</i></b> most frequent elements.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>nums = <span id="example-input-1-1">[1,1,1,2,2,3]</span>, k = <span id="example-input-1-2">2</span>
<strong>Output: </strong><span id="example-output-1">[1,2]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>nums = <span id="example-input-2-1">[1]</span>, k = <span id="example-input-2-2">1</span>
<strong>Output: </strong><span id="example-output-2">[1]</span></pre>
</div>

<p><b>Note: </b></p>

<ul>
	<li>You may assume <i>k</i> is always valid, 1 &le; <i>k</i> &le; number of unique elements.</li>
	<li>Your algorithm&#39;s time complexity <b>must be</b> better than O(<i>n</i> log <i>n</i>), where <i>n</i> is the array&#39;s size.</li>
	<li>It&#39;s guaranteed that the answer is unique, in other words the set of the top k frequent elements is unique.</li>
	<li>You can return the answer in any order.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Heap (heap)

## Companies
- Facebook - 20 (taggedByAdmin: false)
- Amazon - 18 (taggedByAdmin: false)
- Bloomberg - 9 (taggedByAdmin: false)
- Google - 9 (taggedByAdmin: false)
- ByteDance - 6 (taggedByAdmin: false)
- Capital One - 5 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Oracle - 4 (taggedByAdmin: false)
- Pocket Gems - 2 (taggedByAdmin: true)
- eBay - 2 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Yelp - 4 (taggedByAdmin: true)
- Uber - 3 (taggedByAdmin: false)
- Snapchat - 3 (taggedByAdmin: false)
- Spotify - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Yahoo - 5 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Hulu - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Heap

Let's start from the simple [heap](https://en.wikipedia.org/wiki/Heap_(data_structure)) 
approach with $$\mathcal{O}(N \log k)$$
time complexity. To ensure that $$\mathcal{O}(N \log k)$$ is always 
less than $$\mathcal{O}(N \log N)$$, the particular case $$k = N$$ could be 
considered separately and solved in $$\mathcal{O}(N)$$ time. 

**Algorithm**

- The first step is to build a hash map `element -> its frequency`.
In Java, we use the data structure `HashMap`.
Python provides dictionary subclass `Counter` to initialize the hash map we need
directly from the input array.   
This step takes $$\mathcal{O}(N)$$ time where `N` is a number of elements in the list.

- The second step is to build a heap of _size k using N elements_. 
To add the first `k` elements takes 
a linear time $$\mathcal{O}(k)$$ in the average case, and
$$\mathcal{O}(\log 1 + \log 2 + ... + \log k) = 
\mathcal{O}(log k!) = \mathcal{O}(k \log k)$$ in the worst case.
It's equivalent to
[heapify implementation in Python](https://hg.python.org/cpython/file/2.7/Lib/heapq.py#l16).
After the first `k` elements we start to push and pop at each step,
`N - k` steps in total.
The time complexity of heap push/pop 
is $$\mathcal{O}(\log k)$$ and we do it `N - k` times that means
$$\mathcal{O}((N - k)\log k)$$ time complexity.
Adding both parts up, we get $$\mathcal{O}(N \log k)$$ time 
complexity for the second step.

- The third and the last step is to convert the heap into an output array. 
That could be done in $$\mathcal{O}(k \log k)$$ time.
 
In Python, library `heapq` provides a method `nlargest`,
which [combines the last two steps under the hood](https://hg.python.org/cpython/file/2.7/Lib/heapq.py#l203) 
and has the same $$\mathcal{O}(N \log k)$$ time complexity.

![diff](../Figures/347_rewrite/summary.png)

**Implementation**

<iframe src="https://leetcode.com/playground/FGKV3weh/shared" frameBorder="0" width="100%" height="500" name="FGKV3weh"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N \log k)$$ if $$k < N$$ and $$\mathcal{O}(N)$$
in the particular case of $$N = k$$. That ensures time complexity to be better
than $$\mathcal{O}(N \log N)$$.

* Space complexity : $$\mathcal{O}(N + k)$$ to store the hash map with not more 
$$N$$ elements and a heap with $$k$$ elements.
<br />
<br />


---
#### Approach 2: Quickselect

**Hoare's selection algorithm**

Quickselect is a [textbook algorthm](https://en.wikipedia.org/wiki/Quickselect) 
typically used to solve the problems "find `k`*th* something":
`k`*th* smallest, `k`*th* largest, `k`*th* most frequent, 
`k`*th* less frequent, etc. Like quicksort, quickselect was developed 
by [Tony Hoare](https://en.wikipedia.org/wiki/Tony_Hoare), 
and also known as _Hoare's selection algorithm_.

It has $$\mathcal{O}(N)$$ _average_ time complexity and widely used 
in practice. It worth to note that its worth case time complexity 
is $$\mathcal{O}(N^2)$$, although the probability of this worst-case 
is negligible.

The approach is the same as for quicksort.

> One chooses a pivot and defines its position in a sorted array in a 
linear time using so-called _partition algorithm_. 

As an output, we have an array where the pivot is on its perfect position
in the ascending sorted array, sorted by the frequency. 
All elements on the left of the pivot are less frequent than the pivot,
and all elements on the right are more frequent or have the
same frequency.

Hence the array is now split into two parts.
If by chance our pivot element took `N - k`*th* final position, 
then $$k$$ elements on the right are these top $$k$$ 
frequent we're looking for. If not, we can choose one more pivot and 
place it in its perfect position.

![diff](../Figures/347_rewrite/hoare.png)

If that were a quicksort algorithm, one would have to process
both parts of the array. That would result in $$\mathcal{O}(N \log N)$$ time complexity.
In this case, there is no need to deal with both parts since one knows 
in which part to search for `N - k`*th* less frequent element, and that
reduces the average time complexity to $$\mathcal{O}(N)$$.

**Algorithm**

The algorithm is quite straightforward :

* Build a hash map `element -> its frequency` and convert its keys into
the array `unique` of unique elements. Note that elements are unique, but
their frequencies are _not_. That means we need a partition algorithm 
that works fine with _duplicates_. 

* Work with `unique` array. 
Use a partition scheme (please check the next section) to place the pivot 
into its perfect position `pivot_index` in the sorted array,
move less frequent elements to the left of pivot, 
and more frequent or of the same frequency - to the right.

* Compare `pivot_index` and `N - k`.
 
    - If `pivot_index == N - k`, the pivot is 
    `N - k`*th* most frequent element, and all elements on the right
    are more frequent or of the same frequency. 
    Return these top $$k$$ frequent elements.
    
    - Otherwise, choose the side of the array to proceed recursively.
    
![diff](../Figures/347_rewrite/details.png)

**Hoare's Partition vs Lomuto's Partition**

There is a zoo of partition algorithms. The most simple 
one is [Lomuto's Partition Scheme](https://en.wikipedia.org/wiki/Quicksort#Lomuto_partition_scheme).

> The drawback of Lomuto's partition is it fails with duplicates. 

Here we work with an array of unique elements, but they are compared 
by frequencies, which are _not unique_. That's why we choose 
_Hoare's Partition_ here. 

> Hoare's partition is more efficient than Lomuto's partition 
because it does three times fewer swaps on average, 
and creates efficient partitions even when all values are equal.

Here is how it works:

- Move pivot at the end of the array using swap. 

- Set the pointer at the beginning of the array `store_index = left`.
    
- Iterate over the array and move all less frequent elements 
to the left
`swap(store_index, i)`. Move `store_index` one step to the 
right after each swap.

- Move the pivot to its final place, and return this index.

!?!../Documents/347_RES.json:1000,556!?!

<iframe src="https://leetcode.com/playground/EEPtU7fZ/shared" frameBorder="0" width="100%" height="378" name="EEPtU7fZ"></iframe>
 
**Implementation**

Here is a total algorithm implementation. 

<iframe src="https://leetcode.com/playground/5V8xi8W5/shared" frameBorder="0" width="100%" height="500" name="5V8xi8W5"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ in the average case, 
    $$\mathcal{O}(N^2)$$ in the worst case. 
    [Please refer to this card for the good detailed explanation of Master Theorem](https://leetcode.com/explore/learn/card/recursion-ii/470/divide-and-conquer/2871/).
    Master Theorem helps to get an average complexity by writing the algorithm cost
    as $$T(N) = a T(N / b) + f(N)$$. 
    Here we have an example of Master Theorem case III: 
    $$T(N) = T \left(\frac{N}{2}\right) + N$$, 
    that results in $$\mathcal{O}(N)$$ time complexity.
    That's the case of random pivots.
    
    In the worst-case of constantly bad chosen pivots, the problem is 
    not divided by half at each step, it becomes just one element less,
    that leads to $$\mathcal{O}(N^2)$$ time complexity. 
    It happens, for example, if at each step you choose the pivot not 
    randomly, but take the rightmost element. 
    For the random pivot choice the probability of having such a 
    worst-case is negligibly small. 

* Space complexity: up to $$\mathcal{O}(N)$$ to store hash map
and array of unique elements.
<br />
<br />


---
#### Further Discussion: Could We Do Worst-Case Linear Time? 

In theory, we could, the algorithm is called 
[Median of Medians](https://en.wikipedia.org/wiki/Median_of_medians).

This method is never used in practice because of two drawbacks:

- It's _outperformer_. Yes, it works in a linear time $$\alpha N$$, but
the constant $$\alpha$$ is so large that in practice it often works even 
slower than $$N^2$$.  

- It doesn't work with duplicates.
<br />
<br />


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n) Solution - Bucket Sort
- Author: mo10
- Creation Date: Mon May 02 2016 12:09:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 03:51:59 GMT+0800 (Singapore Standard Time)

<p>
Idea is simple. Build a array of list to be buckets with length 1 to sort.

    public List<Integer> topKFrequent(int[] nums, int k) {

		List<Integer>[] bucket = new List[nums.length + 1];
		Map<Integer, Integer> frequencyMap = new HashMap<Integer, Integer>();

		for (int n : nums) {
			frequencyMap.put(n, frequencyMap.getOrDefault(n, 0) + 1);
		}

		for (int key : frequencyMap.keySet()) {
			int frequency = frequencyMap.get(key);
			if (bucket[frequency] == null) {
				bucket[frequency] = new ArrayList<>();
			}
			bucket[frequency].add(key);
		}

		List<Integer> res = new ArrayList<>();

		for (int pos = bucket.length - 1; pos >= 0 && res.size() < k; pos--) {
			if (bucket[pos] != null) {
				res.addAll(bucket[pos]);
			}
		}
		return res;
	}
</p>


### 3 Java Solution using Array, MaxHeap, TreeMap
- Author: UpTheHell
- Creation Date: Tue Jun 14 2016 22:58:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 14:40:29 GMT+0800 (Singapore Standard Time)

<p>
    // use an array to save numbers into different bucket whose index is the frequency
    public class Solution {
        public List<Integer> topKFrequent(int[] nums, int k) {
            Map<Integer, Integer> map = new HashMap<>();
            for(int n: nums){
                map.put(n, map.getOrDefault(n,0)+1);
            }
            
            // corner case: if there is only one number in nums, we need the bucket has index 1.
            List<Integer>[] bucket = new List[nums.length+1];
            for(int n:map.keySet()){
                int freq = map.get(n);
                if(bucket[freq]==null)
                    bucket[freq] = new LinkedList<>();
                bucket[freq].add(n);
            }
            
            List<Integer> res = new LinkedList<>();
            for(int i=bucket.length-1; i>0 && k>0; --i){
                if(bucket[i]!=null){
                    List<Integer> list = bucket[i]; 
                    res.addAll(list);
                    k-= list.size();
                }
            }
            
            return res;
        }
    }
    
    
    
    // use maxHeap. Put entry into maxHeap so we can always poll a number with largest frequency
    public class Solution {
        public List<Integer> topKFrequent(int[] nums, int k) {
            Map<Integer, Integer> map = new HashMap<>();
            for(int n: nums){
                map.put(n, map.getOrDefault(n,0)+1);
            }
               
            PriorityQueue<Map.Entry<Integer, Integer>> maxHeap = 
                             new PriorityQueue<>((a,b)->(b.getValue()-a.getValue()));
            for(Map.Entry<Integer,Integer> entry: map.entrySet()){
                maxHeap.add(entry);
            }
            
            List<Integer> res = new ArrayList<>();
            while(res.size()<k){
                Map.Entry<Integer, Integer> entry = maxHeap.poll();
                res.add(entry.getKey());
            }
            return res;
        }
    }
    
    
    
    // use treeMap. Use freqncy as the key so we can get all freqencies in order
    public class Solution {
        public List<Integer> topKFrequent(int[] nums, int k) {
            Map<Integer, Integer> map = new HashMap<>();
            for(int n: nums){
                map.put(n, map.getOrDefault(n,0)+1);
            }
            
            TreeMap<Integer, List<Integer>> freqMap = new TreeMap<>();
            for(int num : map.keySet()){
               int freq = map.get(num);
               if(!freqMap.containsKey(freq)){
                   freqMap.put(freq, new LinkedList<>());
               }
               freqMap.get(freq).add(num);
            }
            
            List<Integer> res = new ArrayList<>();
            while(res.size()<k){
                Map.Entry<Integer, List<Integer>> entry = freqMap.pollLastEntry();
                res.addAll(entry.getValue());
            }
            return res;
        }
    }
</p>


### C++ O(n log(n-k)) unordered_map and priority_queue(maxheap) solution
- Author: sxycwzwzq
- Creation Date: Mon May 02 2016 10:37:53 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 16:06:31 GMT+0800 (Singapore Standard Time)

<p>

    class Solution {
    public:
        vector<int> topKFrequent(vector<int>& nums, int k) {
            unordered_map<int,int> map;
            for(int num : nums){
                map[num]++;
            }
            
            vector<int> res;
            // pair<first, second>: first is frequency,  second is number
            priority_queue<pair<int,int>> pq; 
            for(auto it = map.begin(); it != map.end(); it++){
                pq.push(make_pair(it->second, it->first));
                if(pq.size() > (int)map.size() - k){
                    res.push_back(pq.top().second);
                    pq.pop();
                }
            }
            return res;
        }
    };
</p>


