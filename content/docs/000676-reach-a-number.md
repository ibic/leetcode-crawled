---
title: "Reach a Number"
weight: 676
#id: "reach-a-number"
---
## Description
<div class="description">
<p>
You are standing at position <code>0</code> on an infinite number line.  There is a goal at position <code>target</code>.
</p><p>
On each move, you can either go left or right.  During the <i>n</i>-th move (starting from 1), you take <i>n</i> steps.
</p><p>
Return the minimum number of steps required to reach the destination.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> target = 3
<b>Output:</b> 2
<b>Explanation:</b>
On the first move we step from 0 to 1.
On the second step we step from 1 to 3.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> target = 2
<b>Output:</b> 3
<b>Explanation:</b>
On the first move we step from 0 to 1.
On the second move we step  from 1 to -1.
On the third move we step from -1 to 2.
</pre>
</p>

<p><b>Note:</b><br>
<li><code>target</code> will be a non-zero integer in the range <code>[-10^9, 10^9]</code>.</li>
</p>
</div>

## Tags
- Math (math)

## Companies
- InMobi - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Mathematical [Accepted]

**Intuition**

The crux of the problem is to put `+` and `-` signs on the numbers `1, 2, 3, ..., k` so that the sum is `target`.

When `target < 0` and we made a sum of `target`, we could switch the signs of all the numbers so that it equals `Math.abs(target)`.  Thus, the answer for `target` is the same as `Math.abs(target)`, and so without loss of generality, we can consider only `target > 0`.

Now let's say `k` is the smallest number with `S = 1 + 2 + ... + k >= target`.  If `S == target`, the answer is clearly `k`.

If `S > target`, we need to change some number signs.  If `delta = S - target` is even, then we can always find a subset of `{1, 2, ..., k}` equal to `delta / 2` and switch the signs, so the answer is `k`.  (This depends on `T = delta / 2` being at most `S`.)  [The proof is simple: either `T <= k` and we choose it, or we choose `k` in our subset and try to solve the same instance of the problem for `T -= k` and the set `{1, 2, ..., k-1}`.]

Otherwise, if `delta` is odd, we can't do it, as every sign change from positive to negative changes the sum by an even number.  So let's consider a candidate answer of `k+1`, which changes `delta` by `k+1`.  If this is odd, then `delta` will be even and we can have an answer of `k+1`.  Otherwise, `delta` will be odd, and we will have an answer of `k+2`.

For concrete examples of the above four cases, consider the following:

* If `target = 3`, then `k = 2, delta = 0` and the answer is `k = 2`.
* If `target = 4`, then `k = 3, delta = 2`, delta is even and the answer is `k = 3`.
* If `target = 7`, then `k = 4, delta = 3`, delta is odd and adding `k+1` makes delta even.  The answer is `k+1 = 5`.
* If `target = 5`, then `k = 3, delta = 1`, delta is odd and adding `k+1` keeps delta odd.  The answer is `k+2 = 5`.

**Algorithm**

Subtract `++k` from `target` until it goes non-positive.  Then `k` will be as described, and `target` will be `delta` as described.  We can output the four cases above: if `delta` is even then the answer is `k`, if `delta` is odd then the answer is `k+1` or `k+2` depending on the parity of `k`.

<iframe src="https://leetcode.com/playground/VkwjioHV/shared" frameBorder="0" width="100%" height="208" name="VkwjioHV"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(\sqrt{\text{target}})$$.  Our while loop needs this many steps, as $$1 + 2 + \dots + k = \frac{k(k+1)}{2}$$.

* Space Complexity: $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short JAVA Solution with Explanation
- Author: jun1013
- Creation Date: Sun Dec 31 2017 14:26:50 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 09:49:23 GMT+0800 (Singapore Standard Time)

<p>
Step 0: Get positive ```target``` value (```step``` to get negative ```target``` is the same as to get positive value due to symmetry).
Step 1: Find the smallest ```step``` that the summation from ```1``` to ```step``` just exceeds or equals```target```.
Step 2: Find the difference between ```sum``` and ```target```. The goal is to get rid of the difference to reach ```target```. For ```ith``` move, if we switch the right move to the left, the change in summation will be ``` 2*i``` less. Now the difference between ```sum``` and ```target``` has to be an even number in order for the math to check out. 
Step 2.1: If the difference value is even, we can return the current ```step```.
Step 2.2: If the difference value is odd, we need to increase the step until the difference is even (at most 2 more steps needed).
Eg:
``` target = 5```
Step 0: ```target = 5```.
Step 1: ```sum = 1 + 2 + 3 = 6 > 5```, ```step = 3```.
Step 2: Difference = ``` 6 - 5 = 1```. Since the difference is an odd value, we will not reach the target by switching any right move to the left. So we increase our ```step```.
Step 2.2: We need to increase ```step``` by 2 to get an even difference (i.e. ```1 + 2 + 3 + 4 + 5 = 15```, now ```step = 5```, difference = ```15 - 5 = 10```). Now that we have an even difference, we can simply switch any move to the left (i.e. change ```+``` to ```-```) as long as the summation of the changed value equals to half of the difference. We can switch 1 and 4 or 2 and 3 or 5.
```
class Solution {
    public int reachNumber(int target) {
        target = Math.abs(target);
        int step = 0;
        int sum = 0;
        while (sum < target) {
            step++;
            sum += step;
        }
        while ((sum - target) % 2 != 0) {
            step++;
            sum += step;
        }
        return step;
    }
}
```
</p>


### Not an easy
- Author: hey1
- Creation Date: Sun Dec 31 2017 12:01:16 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 12:07:41 GMT+0800 (Singapore Standard Time)

<p>
This didn't feel like an easy to me
</p>


### C++ O(1) Solution, without loop
- Author: fsqhxrcjw
- Creation Date: Sun Dec 31 2017 12:04:54 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 20:54:45 GMT+0800 (Singapore Standard Time)

<p>

* We can always take abs(target), since the axis is symmetric.

* First of all we keep adding sum=1+2+..+n>=target, solve this quadratic equation gives the smallest n such that sum>=target.

* If 1+2+..+n==target, return n.

* Now we must minus res=sum-target. If res is even, we can flip one number x in [1,n] to be -x. 

* Otherwise if res is odd, and n+1 is odd, we can first add n+1, then res is even. Next flip an x to be -x. 

* If res is odd and n+1 is even, we add n+1 then subtract n+2, res becomes even, then flip an x.

```
class Solution {
public:
    int reachNumber(int target) {
        target = abs(target);
        long long n = ceil((-1.0 + sqrt(1+8.0*target)) / 2);
        long long sum = n * (n+1) / 2;
        if (sum == target) return n;
        long long res = sum - target;
        if ((res&1) == 0) 
            return n;
        else 
            return n+((n&1) ? 2 : 1);
        
    }
};
```
</p>


