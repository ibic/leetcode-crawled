---
title: "Number of Boomerangs"
weight: 424
#id: "number-of-boomerangs"
---
## Description
<div class="description">
<p>Given <i>n</i> points in the plane that are all pairwise distinct, a &quot;boomerang&quot; is a tuple of points <code>(i, j, k)</code> such that the distance between <code>i</code> and <code>j</code> equals the distance between <code>i</code> and <code>k</code> (<b>the order of the tuple matters</b>).</p>

<p>Find the number of boomerangs. You may assume that <i>n</i> will be at most <b>500</b> and coordinates of points are all in the range <b>[-10000, 10000]</b> (inclusive).</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b>
[[0,0],[1,0],[2,0]]

<b>Output:</b>
2

<b>Explanation:</b>
The two boomerangs are <b>[[1,0],[0,0],[2,0]]</b> and <b>[[1,0],[2,0],[0,0]]</b>
</pre>

<p>&nbsp;</p>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Clean java solution: O(n^2) 166ms
- Author: asurana28
- Creation Date: Sun Nov 06 2016 21:52:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 03:00:44 GMT+0800 (Singapore Standard Time)

<p>
    public int numberOfBoomerangs(int[][] points) {
        int res = 0;

        Map<Integer, Integer> map = new HashMap<>();
        for(int i=0; i<points.length; i++) {
            for(int j=0; j<points.length; j++) {
                if(i == j)
                    continue;
                
                int d = getDistance(points[i], points[j]);                
                map.put(d, map.getOrDefault(d, 0) + 1);
            }
            
            for(int val : map.values()) {
                res += val * (val-1);
            }            
            map.clear();
        }
        
        return res;
    }
    
    private int getDistance(int[] a, int[] b) {
        int dx = a[0] - b[0];
        int dy = a[1] - b[1];
        
        return dx*dx + dy*dy;
    }
	
	Time complexity:  O(n^2)
	Space complexity: O(n)
</p>


### Short Python O(n^2) hashmap solution
- Author: Devanshi
- Creation Date: Tue Nov 08 2016 06:12:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 02:15:52 GMT+0800 (Singapore Standard Time)

<p>
for each point, create a hashmap and count all points with same distance. If for a point p, there are k points with distance d, number of boomerangs corresponding to that are k*(k-1). Keep adding these to get the final result.
```
        res = 0
        for p in points:
            cmap = {}
            for q in points:
                f = p[0]-q[0]
                s = p[1]-q[1]
                cmap[f*f + s*s] = 1 + cmap.get(f*f + s*s, 0)
            for k in cmap:
                res += cmap[k] * (cmap[k] -1)
        return res

```
</p>


### C++ clean solution O(n^2). Fully commented and explained.
- Author: kdtree
- Creation Date: Sun Nov 06 2016 14:31:47 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 12 2018 00:40:59 GMT+0800 (Singapore Standard Time)

<p>
For each point ```i```,  ```map<```distance ```d,``` count of all points at distance ```d``` from ```i>```.
Given that count, choose ```2``` (with permutation) from it, to form a boomerang with point ```i```.
[use ```long``` appropriately for ```dx```, ```dy``` and ```key```; though not required for the given test cases]

Time Complexity: ```O(n^2)```

Updated: Using initial size for the map to avoid table resizing. Thanks @StefanPochmann 
```
int numberOfBoomerangs(vector<pair<int, int>>& points) {
    
    int res = 0;
    
    // iterate over all the points
    for (int i = 0; i < points.size(); ++i) {
        
        unordered_map<long, int> group(points.size());
        
        // iterate over all points other than points[i]
        for (int j = 0; j < points.size(); ++j) {
            
            if (j == i) continue;
            
            int dy = points[i].second - points[j].second;
            int dx = points[i].first - points[j].first;
            
            // compute squared euclidean distance from points[i]
            int key = dy * dy;
            key += dx * dx;
            
            // accumulate # of such "j"s that are "key" distance from "i"
            ++group[key];
        }
        
        for (auto& p : group) {
            if (p.second > 1) {
                /*
                 * for all the groups of points, 
                 * number of ways to select 2 from n = 
                 * nP2 = n!/(n - 2)! = n * (n - 1)
                 */
                res += p.second * (p.second - 1);
            }
        }
    }
    
    return res;
}
```
</p>


