---
title: "Time Based Key-Value Store"
weight: 932
#id: "time-based-key-value-store"
---
## Description
<div class="description">
<p>Create a timebased key-value store class&nbsp;<code>TimeMap</code>, that supports two operations.</p>

<p>1. <code>set(string key, string value, int timestamp)</code></p>

<ul>
	<li>Stores the <code>key</code> and <code>value</code>, along with the given <code>timestamp</code>.</li>
</ul>

<p>2. <code>get(string key, int timestamp)</code></p>

<ul>
	<li>Returns a value such that <code>set(key, value, timestamp_prev)</code> was called previously, with <code>timestamp_prev &lt;= timestamp</code>.</li>
	<li>If there are multiple such values, it returns the one with the largest <code>timestamp_prev</code>.</li>
	<li>If there are no values, it returns the empty string (<code>&quot;&quot;</code>).</li>
</ul>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>inputs = <span id="example-input-1-1">[&quot;TimeMap&quot;,&quot;set&quot;,&quot;get&quot;,&quot;get&quot;,&quot;set&quot;,&quot;get&quot;,&quot;get&quot;]</span>, inputs = <span id="example-input-1-2">[[],[&quot;foo&quot;,&quot;bar&quot;,1],[&quot;foo&quot;,1],[&quot;foo&quot;,3],[&quot;foo&quot;,&quot;bar2&quot;,4],[&quot;foo&quot;,4],[&quot;foo&quot;,5]]</span>
<strong>Output: </strong><span id="example-output-1">[null,null,&quot;bar&quot;,&quot;bar&quot;,null,&quot;bar2&quot;,&quot;bar2&quot;]</span>
<strong>Explanation: </strong><span id="example-output-1">&nbsp; 
TimeMap kv; &nbsp; 
kv.set(&quot;foo&quot;, &quot;bar&quot;, 1); // store the key &quot;foo&quot; and value &quot;bar&quot; along with timestamp = 1 &nbsp; 
kv.get(&quot;foo&quot;, 1);  // output &quot;bar&quot; &nbsp; 
kv.get(&quot;foo&quot;, 3); // output &quot;bar&quot; since there is no value corresponding to foo at timestamp 3 and timestamp 2, then the only value is at timestamp 1 ie &quot;bar&quot; &nbsp; 
kv.set(&quot;foo&quot;, &quot;bar2&quot;, 4); &nbsp; 
kv.get(&quot;foo&quot;, 4); // output &quot;bar2&quot; &nbsp; 
kv.get(&quot;foo&quot;, 5); //output &quot;bar2&quot; &nbsp; 
</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>inputs = <span id="example-input-2-1">[&quot;TimeMap&quot;,&quot;set&quot;,&quot;set&quot;,&quot;get&quot;,&quot;get&quot;,&quot;get&quot;,&quot;get&quot;,&quot;get&quot;]</span>, inputs = <span id="example-input-2-2">[[],[&quot;love&quot;,&quot;high&quot;,10],[&quot;love&quot;,&quot;low&quot;,20],[&quot;love&quot;,5],[&quot;love&quot;,10],[&quot;love&quot;,15],[&quot;love&quot;,20],[&quot;love&quot;,25]]</span>
<strong>Output: </strong><span id="example-output-2">[null,null,null,&quot;&quot;,&quot;high&quot;,&quot;high&quot;,&quot;low&quot;,&quot;low&quot;]</span>
</pre>
</div>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>All key/value strings are lowercase.</li>
	<li>All key/value strings have&nbsp;length in the range&nbsp;<code>[1, 100]</code></li>
	<li>The <code>timestamps</code> for all <code>TimeMap.set</code> operations are strictly increasing.</li>
	<li><code>1 &lt;= timestamp &lt;= 10^7</code></li>
	<li><code>TimeMap.set</code> and <code>TimeMap.get</code>&nbsp;functions will be called a total of <code>120000</code> times (combined) per test case.</li>
</ol>

</div>

## Tags
- Hash Table (hash-table)
- Binary Search (binary-search)

## Companies
- Lyft - 8 (taggedByAdmin: false)
- Apple - 5 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Netflix - 4 (taggedByAdmin: false)
- Zillow - 3 (taggedByAdmin: false)
- VMware - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Sumologic - 9 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Twitter - 3 (taggedByAdmin: false)
- Square - 2 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Cruise Automation - 2 (taggedByAdmin: false)
- Flexport - 0 (taggedByAdmin: true)
- Databricks - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: HashMap + Binary Search

**Intuition and Algorithm**

For each `key` we get or set, we only care about the timestamps and values for that key.  We can store this information in a `HashMap`.

Now, for each `key`, we can binary search the sorted list of timestamps to find the relevant `value` for that `key`.

<iframe src="https://leetcode.com/playground/sUnkGSSK/shared" frameBorder="0" width="100%" height="500" name="sUnkGSSK"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(1)$$ for each `set` operation, and $$O(\log N)$$ for each `get` operation, where $$N$$ is the number of entries in the `TimeMap`.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: TreeMap

**Intuition and Algorithm**

In `Java`, we can use `TreeMap.floorKey(timestamp)` to find the largest timestamp smaller than the given `timestamp`.

We build our solution in the same way as *Approach 1*, swapping in this functionality.

<iframe src="https://leetcode.com/playground/2DESkepc/shared" frameBorder="0" width="100%" height="429" name="2DESkepc"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(1)$$ for each `set` operation, and $$O(\log N)$$ for each `get` operation, where $$N$$ is the number of entries in the `TimeMap`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ 3 lines, hash map + map
- Author: votrubac
- Creation Date: Sun Jan 27 2019 12:03:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 30 2019 01:18:35 GMT+0800 (Singapore Standard Time)

<p>
We use hash map to lookup ordered ```{timestamp, value}``` pairs by key in O(1). Then, we use binary search to find the value with a timestamp less or equal than the requested one.
```
unordered_map<string, map<int, string>> m;
void set(string key, string value, int timestamp) {
  m[key].insert({ timestamp, value });
}
string get(string key, int timestamp) {
  auto it = m[key].upper_bound(timestamp);
  return it == m[key].begin() ? "" : prev(it)->second;
}
```
Since our timestamps are only increasing, we can use a vector instead of a map, though it\'s not as concise.
```
unordered_map<string, vector<pair<int, string>>> m;
void set(string key, string value, int timestamp) {
  m[key].push_back({ timestamp, value });
}
string get(string key, int timestamp) {
  auto it = upper_bound(begin(m[key]), end(m[key]), pair<int, string>(timestamp, ""), [](
    const pair<int, string>& a, const pair<int, string>& b) { return a.first < b.first; });
  return it == m[key].begin() ? "" : prev(it)->second;
}
```
# Complexity analysis
Assuming ```n``` is the number of set operations, and ```m``` is the number of get operations:
- Time Complexity: 
    - Set: ```O(1)``` single operation, and total ```O(n)```.
Note: assuing timestamps are only increasing. If not, it\'s ```O(n log n)```.
    - Get: ```O(log n)``` for a single operation, and total ```O(m log n)```.
- Space Complexity: ```O(n)``` (assuming every ```{ timestamp, value }``` is unique).
</p>


### TreeMap Solution Java
- Author: Poorvank
- Creation Date: Sun Jan 27 2019 12:02:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 27 2019 12:02:54 GMT+0800 (Singapore Standard Time)

<p>
```
public class TimeMap {

    private Map<String,TreeMap<Integer,String>> map;

    /** Initialize your data structure here. */
    public TimeMap() {
        map = new HashMap<>();
    }

    public void set(String key, String value, int timestamp) {
        if(!map.containsKey(key)) {
            map.put(key,new TreeMap<>());
        }
        map.get(key).put(timestamp,value);
    }

    public String get(String key, int timestamp) {
        TreeMap<Integer,String> treeMap = map.get(key);
        if(treeMap==null) {
            return "";
        }
        Integer floor = treeMap.floorKey(timestamp);
        if(floor==null) {
            return "";
        }
        return treeMap.get(floor);
    }
}
```
</p>


### Java beats 100%
- Author: ramankes
- Creation Date: Tue Feb 26 2019 07:01:35 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Feb 26 2019 07:01:35 GMT+0800 (Singapore Standard Time)

<p>
```
class Data {
    String val;
    int time;
    Data(String val, int time) {
        this.val = val;
        this.time = time;
    }
}
class TimeMap {

    /** Initialize your data structure here. */
    Map<String, List<Data>> map;
    public TimeMap() {
        map = new HashMap<String, List<Data>>();
    }
    
    public void set(String key, String value, int timestamp) {
        if (!map.containsKey(key)) map.put(key, new ArrayList<Data>());
        map.get(key).add(new Data(value, timestamp));
    }
    
    public String get(String key, int timestamp) {
        if (!map.containsKey(key)) return "";
        return binarySearch(map.get(key), timestamp);
    }
    
    protected String binarySearch(List<Data> list, int time) {
        int low = 0, high = list.size() - 1;
        while (low < high) {
            int mid = (low + high) >> 1;
            if (list.get(mid).time == time) return list.get(mid).val;
            if (list.get(mid).time < time) {
                if (list.get(mid+1).time > time) return list.get(mid).val;
                low = mid + 1;
            }
            else high = mid -1;
        }
        return list.get(low).time <= time ? list.get(low).val : "";
    }
}
```
</p>


