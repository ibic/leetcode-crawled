---
title: "Course Schedule"
weight: 191
#id: "course-schedule"
---
## Description
<div class="description">
<p>There are a total of <code>numCourses</code> courses you have to take, labeled from <code>0</code> to <code>numCourses-1</code>.</p>

<p>Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: <code>[0,1]</code></p>

<p>Given the total number of courses and a list of prerequisite <b>pairs</b>, is it possible for you to finish all courses?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> numCourses = 2, prerequisites = [[1,0]]
<strong>Output:</strong> true
<strong>Explanation:</strong>&nbsp;There are a total of 2 courses to take. 
&nbsp;            To take course 1 you should have finished course 0. So it is possible.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> numCourses = 2, prerequisites = [[1,0],[0,1]]
<strong>Output:</strong> false
<strong>Explanation:</strong>&nbsp;There are a total of 2 courses to take. 
&nbsp;            To take course 1 you should have finished course 0, and to take course 0 you should
&nbsp;            also have finished course 1. So it is impossible.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The input prerequisites is a graph represented by <b>a list of edges</b>, not adjacency matrices. Read more about <a href="https://www.khanacademy.org/computing/computer-science/algorithms/graph-representation/a/representing-graphs" target="_blank">how a graph is represented</a>.</li>
	<li>You may assume that there are no duplicate edges in the input prerequisites.</li>
	<li><code>1 &lt;=&nbsp;numCourses &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)
- Graph (graph)
- Topological Sort (topological-sort)

## Companies
- Amazon - 11 (taggedByAdmin: false)
- Facebook - 10 (taggedByAdmin: false)
- ByteDance - 8 (taggedByAdmin: false)
- Roblox - 4 (taggedByAdmin: false)
- Wish - 3 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: true)
- Microsoft - 11 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- LinkedIn - 2 (taggedByAdmin: false)
- Intuit - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Cohesity - 2 (taggedByAdmin: false)
- Salesforce - 4 (taggedByAdmin: false)
- Goldman Sachs - 3 (taggedByAdmin: false)
- Nutanix - 3 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Yelp - 0 (taggedByAdmin: true)
- Zenefits - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Backtracking

**Intuition**

The problem could be modeled as yet another *__graph traversal__* problem, where each course can be represented as a _vertex_ in a graph and the dependency between the courses can be modeled as a directed edge between two vertex.

>And the problem to determine if one could build a valid schedule of courses that satisfies all the dependencies (_i.e._ _**constraints**_) would be equivalent to determine if the corresponding graph is a **DAG** (_Directed Acyclic Graph_), _i.e._ there is no cycle existed in the graph.

![pic](../Figures/207/207_graph.png)

A typical strategy for graph traversal problems would be [backtracking](https://leetcode.com/explore/learn/card/recursion-ii/472/backtracking/) or simply DFS (depth-first search).

Here let us start with the backtracking algorithm, which arguably might be more intuitive.

>As a reminder, [backtracking](https://en.wikipedia.org/wiki/Backtracking) is a general algorithm that is often applied to solve the [constraint satisfaction problems](https://en.wikipedia.org/wiki/Constraint_satisfaction_problem), which incrementally builds candidates to the solutions, and abandons a candidate (_i.e._ backtracks) as soon as it determines that the candidate would not lead to a valid solution.

The general idea here is that we could enumerate each course (vertex), to check if it could form cyclic dependencies (_i.e._ a cyclic path) starting from this course.

The check of cyclic dependencies for each course could be done via *__backtracking__*, where we incrementally follow the dependencies until either there is no more dependency or we come across a previously visited course along the path.


**Algorithm**

The overall structure of the algorithm is simple, which consists of three main steps: 

- Step 1). we build a graph data structure from the given list of course dependencies. Here we adopt the adjacency list data structure as shown below to represent the graph, which can be implemented via hashmap or dictionary. Each entry in the adjacency list represents a node which consists of a node index and a list of neighbors nodes that follow from the node.
![pic](../Figures/207/207_adjacency_list.png)
<br/>
- Step 2). we then enumerate each node (course) in the constructed graph, to check if we could form a dependency cycle starting from the node. 
<br/>
- Step 3). we perform the cyclic check via backtracking, where we **_breadcrumb_** our path (_i.e._ mark the nodes we visited) to detect if we come across a previously visited node (hence a cycle detected). We also remove the breadcrumbs for each iteration.

!?!../Documents/207_LIS.json:1000,383!?!

<iframe src="https://leetcode.com/playground/3GfTcsvg/shared" frameBorder="0" width="100%" height="500" name="3GfTcsvg"></iframe>



**Complexity**

- Time Complexity: $$\mathcal{O}(|E| + |V| ^ 2)$$ where $$|E|$$ is the number of dependencies, $$|V|$$ is the number of courses and $$d$$ is the maximum length of acyclic paths in the graph.
    <br/>
    - First of all, it would take us $$|E|$$ steps to build a graph in the first step.
    <br/>
    - For a single round of backtracking, in the worst case where all the nodes chained up in a line, it would take us maximum $$|V|$$ steps to terminate the backtracking.
    ![pic](../Figures/207/207_chain.png)
    <br/>
    - Again, follow the above worst scenario where all nodes are chained up in a line, it would take us in total $$\sum_{i=1}^{|V|}{i} = \frac{(1+|V|)\cdot|V|}{2}$$ steps to finish the check for all nodes.
    <br/>
    - As a result, the overall time complexity of the algorithm would be $$\mathcal{O}(|E| + |V| ^ 2)$$.
<br/>
<br/>

- Space Complexity: $$\mathcal{O}(|E| + |V|)$$, with the same denotation as in the above time complexity.
    </br>
    - We built a graph data structure in the algorithm, which would consume $$|E| + |V|$$ space.
    <br/>
    - In addition, during the backtracking process, we employed a sort of bitmap (`path`) to keep track of all visited nodes, which consumes $$|V|$$ space.
    <br/>
    - Finally, since we implement the function in recursion, which would incur additional memory consumption on call stack. In the worst case where all nodes are chained up in a line, the recursion would pile up $$|V|$$ times.
    <br/>
    - Hence the overall space complexity of the algorithm would be $$\mathcal{O}(|E| + 3\cdot|V|) = \mathcal{O}(|E| + |V|) $$.
<br/>
<br/>

---
#### Approach 2: Postorder DFS (Depth-First Search)

**Intuition**

As one might notice that, with the above backtracking algorithm, we would visit certain nodes multiple times, which is not the most efficient way.

![pic](../Figures/207/207_chain.png)

For instance, in the above graph where the nodes are chained up in a line, the backtracking algorithm would end up of being a nested two-level iteration over the nodes, which we could rewrite as the following pseudo code:

```python
for i in range(0, len(nodes)):
    # start from the current node to check if a cycle might be formed.
    for j in range(i, len(nodes)):
        isCyclic(nodes[j], courseDict, path)
```

One might wonder that if there is a better algorithm that visits each node once and only once. And the answer is yes.

_In the above example, for the first node in the chain, once we've done the check that there would be no cycle formed starting from this node, we don't have to do the same check for all the nodes in the downstream._

>The rationale is that given a node, if the subgraph formed by all descendant nodes from this node has no cycle, then adding this node to the subgraph would not form a cycle either. 

From the perspective of graph traversal, the above rationale could be implemented with the strategy of **postorder DFS** (depth-first search), in which strategy we visit a node's descendant nodes before the node itself. 

**Algorithm**

We could implement the postorder DFS based on the above backtracking algorithm, by simply adding another bitmap (_i.e._ `checked[node_index]`) which indicates whether we have done the cyclic check starting from a particular node.

Here are the breakdowns of the algorithm, where the first 2 steps are the same as in the previous backtracking algorithm.

- Step 1). We build a graph data structure from the given list of course dependencies.
<br/>
- Step 2). We then enumerate each node (course) in the constructed graph, to check if we could form a dependency cycle starting from the node.
<br/>
- Step 3.1). We check if the current node has been *checked* before, otherwise we enumerate through its child nodes via backtracking, where we **_breadcrumb_** our path (_i.e._ mark the nodes we visited) to detect if we come across a previously visited node (hence a cycle detected). We also remove the breadcrumbs for each iteration.
<br/>
- Step 3.2). Once we visited all the child nodes (_i.e._ postorder), we mark the current node as `checked`.

<iframe src="https://leetcode.com/playground/RQmjEhiw/shared" frameBorder="0" width="100%" height="500" name="RQmjEhiw"></iframe>

Note, one could also use a single bitmap with 3 states such as  `not_visited`, `visited`, `checked`, rather than having two bitmaps as we did in the algorithm, though we argue that it might be clearer to have two separated bitmaps.

**Complexity**

- Time Complexity: $$\mathcal{O}(|E| + |V|)$$ where $$|V|$$ is the number of courses, and $$|E|$$ is the number of dependencies.
<br/>
    - As in the previous algorithm, it would take us $$|E|$$ time complexity to build a graph in the first step.
    <br/>
    - Since we perform a postorder DFS traversal in the graph, we visit each vertex and each edge once and only once in the worst case, _i.e._ $$|E| + |V|$$.
<br/>
<br/>

- Space Complexity: $$\mathcal{O}(|E| + |V|)$$, with the same denotation as in the above time complexity.
    </br>
    - We built a graph data structure in the algorithm, which would consume $$|E| + |V|$$ space.
    <br/>
    - In addition, during the backtracking process, we employed two bitmaps (`path` and `visited`) to keep track of the visited path and the status of check respectively, which consumes $$2 \cdot |V|$$ space.
    <br/>
    - Finally, since we implement the function in recursion, which would incur additional memory consumption on call stack. In the worst case where all nodes chained up in a line, the recursion would pile up $$|V|$$ times.
    <br/>
    - Hence the overall space complexity of the algorithm would be $$\mathcal{O}(|E| + 4\cdot|V|) = \mathcal{O}(|E| + |V|)$$.
<br/>
<br/>

---
#### Approach 3: Topological Sort

**Intuition**

Actually, the problem is also known as [topological sort](https://en.wikipedia.org/wiki/Topological_sorting) problem, which is to find a global order for all nodes in a DAG (_Directed Acyclic Graph_) with regarding to their dependencies.

A linear algorithm was first proposed by [Arthur Kahn](https://en.wikipedia.org/wiki/Topological_sorting) in 1962, in his paper of ["Topological order of large networks"](https://dl.acm.org/doi/10.1145/368996.369025). The algorithm returns a topological order if there is any. Here we quote the pseudo code of the Kahn's algorithm from wikipedia as follows:

```python
L = Empty list that will contain the sorted elements
S = Set of all nodes with no incoming edge

while S is non-empty do
    remove a node n from S
    add n to tail of L
    for each node m with an edge e from n to m do
        remove edge e from the graph
        if m has no other incoming edges then
            insert m into S

if graph has edges then
    return error   (graph has at least one cycle)
else 
    return L   (a topologically sorted order)
```

To better understand the above algorithm, we summarize a few points here:

- In order to find a global order, we can start from those nodes which do not have any prerequisites (_i.e._ indegree of node is zero), we then incrementally add new nodes to the global order, following the dependencies (edges).
<br/>
- Once we follow an edge, we then remove it from the graph.
<br/>
- With the removal of edges, there would more nodes appearing without any prerequisite dependency, in addition to the initial list in the first step.
<br/>
- The algorithm would terminate when we can no longer remove edges from the graph. There are two possible outcomes:
    <br/>
    - 1). If there are still some edges left in the graph, then these edges must have formed certain cycles, which is similar to the deadlock situation. It is due to these cyclic dependencies that we cannot remove them during the above processes.
    <br/>
    - 2). Otherwise, _i.e._ we have removed all the edges from the graph, and we got ourselves a topological order of the graph.
    <br/>

**Algorithm**

Following the above intuition and pseudo code, here we list some sample implementations.

!?!../Documents/207_RES.json:1000,353!?!

<iframe src="https://leetcode.com/playground/hPhgAUJT/shared" frameBorder="0" width="100%" height="500" name="hPhgAUJT"></iframe>

Note that we could use different types of containers, such as Queue, Stack or Set, to keep track of the nodes that have no incoming dependency, _i.e._ `indegree = 0`. Depending on the type of container, the resulting topological order would be different, though they are all valid.


**Complexity**

- Time Complexity: $$\mathcal{O}(|E| + |V|)$$ where $$|V|$$ is the number of courses, and $$|E|$$ is the number of dependencies.
<br/>
    - As in the previous algorithm, it would take us $$|E|$$ time complexity to build a graph in the first step.
    <br/>
    - Similar with the above postorder DFS traversal, we would visit each vertex and each edge once and only once in the worst case, _i.e._ $$|E| + |V|$$.
    <br/>
    - As a result, the overall time complexity of the algorithm would be $$\mathcal{O}(2\cdot|E| + |V|) = \mathcal{O}(|E| + |V|)$$.
<br/>
<br/>

- Space Complexity: $$\mathcal{O}(|E| + |V|)$$, with the same denotation as in the above time complexity.
    </br>
    - We built a graph data structure in the algorithm, which would consume $$|E| + |V|$$ space.
    <br/>
    - In addition, we use a container to keep track of the courses that have no prerequisite, and the size of the container would be bounded by $$|V|$$.
    <br/>
    - As a result, the overall space complexity of the algorithm would be $$\mathcal{O}(|E| + 2\cdot|V|) = \mathcal{O}(|E| + |V|)$$.

## Accepted Submission (python3)
```python3
class Solution:
    def canFinishBFS(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        prereqMap = defaultdict(set)
        depsMap = defaultdict(set)
        for [course, prereq] in prerequisites:
            prereqMap[course].add(prereq)
            depsMap[prereq].add(course)

        q = deque()
        for course in range(numCourses):
            if not prereqMap[course]:
                q.append(course)
                del prereqMap[course]

        while len(prereqMap) > 0 and len(q) > 0:
            course = q.pop()
            for dep in depsMap[course]:
                depPrereq = prereqMap[dep]
                depPrereq.remove(course)
                if not depPrereq:
                    q.append(dep)
                    del depPrereq

        return all(map(lambda key: not prereqMap[key], prereqMap))

    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        def visit(course: int) -> bool:
            if visited[course]:
                return True

            if visiting[course]:
                return False

            visiting[course] = True
            for dep in depsMap[course]:
                if not visit(dep):
                    return False

            visiting[course] = True
            visited[course] = True
            return True

        visited = [False] * (numCourses + 1)
        visiting = [False] * (numCourses + 1)

        depsMap = defaultdict(list)
        for k, v in prerequisites:
            depsMap[k].append(v)
            depsMap[numCourses].append(k)

        return visit(numCourses)
```

## Top Discussions
### C++ BFS/DFS
- Author: jianchao-li
- Creation Date: Sun Jun 28 2015 01:08:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 04:28:53 GMT+0800 (Singapore Standard Time)

<p>
This problem is equivalent to detecting a cycle in the directed graph represented by `prerequisites`. Both BFS and DFS can be used to solve it using the idea of **topological sort**. Since `pair<int, int>` is inconvenient for implementing graph algorithms, we first transform it to the adjacency-list representation. If course `u` is a prerequisite of course `v`, then the adjacency list of `u` will contain `v`.

**BFS**

BFS uses the indegrees of each node. We will first try to find a node with `0` indegree. If we fail to do so, there must be a cycle in the graph and we return `false`. Otherwise we set its indegree to be `-1` to prevent from visiting it again and reduce the indegrees of its neighbors by `1`. This process will be repeated for `n` (number of nodes) times.

```cpp
class Solution {
public:
    bool canFinish(int numCourses, vector<pair<int, int>>& prerequisites) {
        graph g = buildGraph(numCourses, prerequisites);
        vector<int> degrees = computeIndegrees(g);
        for (int i = 0; i < numCourses; i++) {
            int j = 0;
            for (; j < numCourses; j++) {
                if (!degrees[j]) {
                    break;
                }
            }
            if (j == numCourses) {
                return false;
            }
            degrees[j]--;
            for (int v : g[j]) {
                degrees[v]--;
            }
        }
        return true;
    }
private:
    typedef vector<vector<int>> graph;
    
    graph buildGraph(int numCourses, vector<pair<int, int>>& prerequisites) {
        graph g(numCourses);
        for (auto p : prerequisites) {
            g[p.second].push_back(p.first);
        }
        return g;
    }
    
    vector<int> computeIndegrees(graph& g) {
        vector<int> degrees(g.size(), 0);
        for (auto adj : g) {
            for (int v : adj) {
                degrees[v]++;
            }
        }
        return degrees;
    }
};
```

**DFS**

For DFS, in each visit, we start from a node and keep visiting its neighbors, if at a time we return to a visited node, there is a cycle. Otherwise, start again from another unvisited node and repeat this process. We use `todo` and `done` for nodes to visit and visited nodes.

```cpp
class Solution {
public:
    bool canFinish(int numCourses, vector<pair<int, int>>& prerequisites) {
        graph g = buildGraph(numCourses, prerequisites);
        vector<bool> todo(numCourses, false), done(numCourses, false);
        for (int i = 0; i < numCourses; i++) {
            if (!done[i] && !acyclic(g, todo, done, i)) {
                return false;
            }
        }
        return true;
    }
private:
    typedef vector<vector<int>> graph;
    
    graph buildGraph(int numCourses, vector<pair<int, int>>& prerequisites) {
        graph g(numCourses);
        for (auto p : prerequisites) {
            g[p.second].push_back(p.first);
        }
        return g;
    }
    
    bool acyclic(graph& g, vector<bool>& todo, vector<bool>& done, int node) {
        if (todo[node]) {
            return false;
        }
        if (done[node]) {
            return true;
        }
        todo[node] = done[node] = true;
        for (int v : g[node]) {
            if (!acyclic(g, todo, done, v)) {
                return false;
            }
        }
        todo[node] = false;
        return true;
    }
};
```
</p>


### Easy BFS Topological sort, Java
- Author: justjiayu
- Creation Date: Thu May 14 2015 02:07:07 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 17:19:25 GMT+0800 (Singapore Standard Time)

<p>

    public boolean canFinish(int numCourses, int[][] prerequisites) {
        int[][] matrix = new int[numCourses][numCourses]; // i -> j
        int[] indegree = new int[numCourses];
        
        for (int i=0; i<prerequisites.length; i++) {
            int ready = prerequisites[i][0];
            int pre = prerequisites[i][1];
            if (matrix[pre][ready] == 0)
                indegree[ready]++; //duplicate case
            matrix[pre][ready] = 1;
        }
        
        int count = 0;
        Queue<Integer> queue = new LinkedList();
        for (int i=0; i<indegree.length; i++) {
            if (indegree[i] == 0) queue.offer(i);
        }
        while (!queue.isEmpty()) {
            int course = queue.poll();
            count++;
            for (int i=0; i<numCourses; i++) {
                if (matrix[course][i] != 0) {
                    if (--indegree[i] == 0)
                        queue.offer(i);
                }
            }
        }
        return count == numCourses;
    }
</p>


### Python 20 lines DFS solution sharing with explanation
- Author: rodolphe
- Creation Date: Thu May 07 2015 10:54:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 07:29:55 GMT+0800 (Singapore Standard Time)

<p>
    def canFinish(self, numCourses, prerequisites):
        graph = [[] for _ in xrange(numCourses)]
        visit = [0 for _ in xrange(numCourses)]
        for x, y in prerequisites:
            graph[x].append(y)
        def dfs(i):
            if visit[i] == -1:
                return False
            if visit[i] == 1:
                return True
            visit[i] = -1
            for j in graph[i]:
                if not dfs(j):
                    return False
            visit[i] = 1
            return True
        for i in xrange(numCourses):
            if not dfs(i):
                return False
        return True

1. if node `v` has not been visited, then mark it as `0`.
2. if node `v` is being visited, then mark it as `-1`. If we find a vertex marked as `-1` in DFS, then their is a ring.
3. if node `v` has been visited, then mark it as `1`. If a vertex was marked as `1`, then no ring contains `v` or its successors.

*References: [daoluan.net][1]* 


  [1]: http://daoluan.net/blog/map-ring/
</p>


