---
title: "Print FooBar Alternately"
weight: 1605
#id: "print-foobar-alternately"
---
## Description
<div class="description">
<p>Suppose you are given the following code:</p>

<pre>
class FooBar {
  public void foo() {
&nbsp; &nbsp; for (int i = 0; i &lt; n; i++) {
&nbsp; &nbsp; &nbsp; print(&quot;foo&quot;);
&nbsp;   }
  }

  public void bar() {
&nbsp; &nbsp; for (int i = 0; i &lt; n; i++) {
&nbsp; &nbsp; &nbsp; print(&quot;bar&quot;);
&nbsp; &nbsp; }
  }
}
</pre>

<p>The same instance of <code>FooBar</code> will be passed to two different threads. Thread A will call&nbsp;<code>foo()</code> while thread B will call&nbsp;<code>bar()</code>.&nbsp;Modify the given program to output &quot;foobar&quot; <em>n</em> times.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<b>Input:</b> n = 1
<b>Output:</b> &quot;foobar&quot;
<strong>Explanation:</strong> There are two threads being fired asynchronously. One of them calls foo(), while the other calls bar(). &quot;foobar&quot; is being output 1 time.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<b>Input:</b> n = 2
<b>Output:</b> &quot;foobarfoobar&quot;
<strong>Explanation:</strong> &quot;foobar&quot; is being output 2 times.
</pre>

</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 5 Python threading solutions (Barrier, Event, Condition, Lock, Semaphore) with explanation
- Author: mereck
- Creation Date: Wed Jul 17 2019 00:25:36 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 17 2019 00:28:41 GMT+0800 (Singapore Standard Time)

<p>
Raise a barrier which makes both threads wait for each other before they are allowed to continue. `foo` prints before reaching the barrier. `bar` prints after reaching the barrier. 

```
from threading import Barrier

class FooBar:
    def __init__(self, n):
        self.n = n
        self.barrier = Barrier(2)

    def foo(self, printFoo):
        for i in range(self.n):
            printFoo()
            self.barrier.wait()

    def bar(self, printBar):
        for i in range(self.n):
            self.barrier.wait()
            printBar()
```

Count the number of times foo and bar was printed and only print foo if the number of times is equal. `bar` prints if foo was printed fewer times. Use Condition and `wait_for` to syncrhonize the threads.

```
from threading import Condition
class FooBar:
    def __init__(self, n):
        self.n = n
        self.foo_counter = 0
        self.bar_counter = 0
        self.condition = Condition()

    def foo(self, printFoo):
        for i in range(self.n):
            with self.condition:
                self.condition.wait_for(lambda: self.foo_counter == self.bar_counter)
                printFoo()
                self.foo_counter += 1
                self.condition.notify(1)

    def bar(self, printBar):
        for i in range(self.n):
            with self.condition:
                self.condition.wait_for(lambda: self.foo_counter > self.bar_counter)
                printBar()
                self.bar_counter += 1
                self.condition.notify(1)
```

Each thread can wait on each other to set their corresponding `foo_printed` and `bar_printed` events. Each thread also resets the corresponding printed events with `.clear()` for the next loop iteration.

```
from threading import Event

class FooBar:
    def __init__(self, n):
        self.n = n
        self.foo_printed = Event()
        self.bar_printed = Event()
        self.bar_printed.set()

    def foo(self, printFoo):
        for i in range(self.n):
            self.bar_printed.wait()
            self.bar_printed.clear()
            printFoo()
            self.foo_printed.set()            

    def bar(self, printBar):
        for i in range(self.n):
            self.foo_printed.wait()
            self.foo_printed.clear()
            printBar()
            self.bar_printed.set()            
```

Use two locks for the threads to signal to each other when the other should run. `bar_lock` starts in a locked state because we always want `foo` to print first.

```
from threading import Lock

class FooBar:
    def __init__(self, n):
        self.n = n
        self.foo_lock = Lock()
        self.bar_lock = Lock()
        self.bar_lock.acquire()

    def foo(self, printFoo):
        for i in range(self.n):
            self.foo_lock.acquire()
            printFoo()
            self.bar_lock.release()

	def bar(self, printBar):
        for i in range(self.n):
            self.bar_lock.acquire()
            printBar()
            self.foo_lock.release()
```       

Use two Semaphores just as we used two locks. The `foo_gate` semaphore starts with a value of 1 because we want `foo` to print first.

```
from threading import Semaphore

class FooBar:
    def __init__(self, n):
        self.n = n
        self.foo_gate = Semaphore(1)
        self.bar_gate = Semaphore(0)

    def foo(self, printFoo):
        for i in range(self.n):
            self.foo_gate.acquire()
            printFoo()
            self.bar_gate.release()

    def bar(self, printBar):
        for i in range(self.n):
            self.bar_gate.acquire()
            printBar()
            self.foo_gate.release()
```


</p>


### [Java] Semaphore solution
- Author: yujd13
- Creation Date: Sat Jul 13 2019 04:11:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 13 2019 04:11:34 GMT+0800 (Singapore Standard Time)

<p>
```
import java.util.concurrent.Semaphore;
class FooBar {
    private int n;
    Semaphore s = new Semaphore(0);
    Semaphore s2 = new Semaphore(1);

    public FooBar(int n) {
        this.n = n;
    }

    public void foo(Runnable printFoo) throws InterruptedException {

        for (int i = 0; i < n; i++) {

            // printFoo.run() outputs "foo". Do not change or remove this line.
            s2.acquire();

            printFoo.run();
            s.release();

        }
    }

    public void bar(Runnable printBar) throws InterruptedException {

        for (int i = 0; i < n; i++) {

            // printBar.run() outputs "bar". Do not change or remove this line.
            s.acquire();

            printBar.run();
            s2.release();

        }
    }
}
```
</p>


### [Java] 4 Java threading solutions (Synchronized,Lock,Volatile,CAS)
- Author: supertizzy
- Creation Date: Thu Aug 01 2019 14:57:11 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 26 2019 11:37:22 GMT+0800 (Singapore Standard Time)

<p>
Overall,solution 2(lock+condition) performs better:
* comparing to solution 1,solution 2 using more waiting queues to avoid unnecessary notify, reduce lock competition.
* comparing to solution 3 and solution 4,solution 2 using synchronization queue to avoid unnecessary cpu execution.

## 1.Synchronized(monitor exit happens-before monitor enter)

```
/**
 * @autor yeqiaozhu.
 * @date 2019-08-01
 */
public class FooBarSynchronized {


    private int n;
    //flag 0->foo to be print  1->foo has been printed
    private int flag = 0;


    public FooBarSynchronized(int n) {
        this.n = n;
    }

    public void foo(Runnable printFoo) throws InterruptedException {

        for (int i = 0; i < n; i++) {
            synchronized (this) {
                while (flag == 1) {
                    this.wait();
                }
                // printFoo.run() outputs "foo". Do not change or remove this line.
                printFoo.run();
                flag = 1;
                this.notifyAll();
            }
        }
    }

    public void bar(Runnable printBar) throws InterruptedException {

        for (int i = 0; i < n; i++) {
            synchronized (this) {
                while (flag == 0) {
                    this.wait();
                }
                // printBar.run() outputs "bar". Do not change or remove this line.
                printBar.run();
                flag = 0;
                this.notifyAll();
            }
        }
    }
}
```

## 2.Lock(volatile write happens-before volatile read)
```

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

class FooBar {

    private int n;
    //flag 0->foo to be print  1->foo has been printed
    private int flag=0;
    ReentrantLock reentrantLock= new ReentrantLock();
    Condition fooPrintedCondition=reentrantLock.newCondition();
    Condition barPrintedCondition=reentrantLock.newCondition();

    public FooBar(int n) {
        this.n = n;
    }

    public void foo(Runnable printFoo) throws InterruptedException {
        
        for (int i = 0; i < n; i++) {
            try {
                reentrantLock.lock();
                while (flag ==1){
                    barPrintedCondition.await();
                }
                // printFoo.run() outputs "foo". Do not change or remove this line.
                printFoo.run();
                flag=1;
                fooPrintedCondition.signalAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                reentrantLock.unlock();
            }
        }
    }

    public void bar(Runnable printBar) throws InterruptedException {
        
        for (int i = 0; i < n; i++) {
            reentrantLock.lock();
            while (flag==0){
                fooPrintedCondition.await();
            }
            // printBar.run() outputs "bar". Do not change or remove this line.
        	printBar.run();
            flag=0;
        	barPrintedCondition.signalAll();
        	reentrantLock.unlock();
        }
    }
}
```
## 3.Voatile(volatile write happens-before volatile read)

```

/**
 * @autor yeqiaozhu.
 * @date 2019-08-01
 */
public class FooBarVolatile {

    private int n;
   //flag 0->foo to be print  1->foo has been printed  using volatile
    private volatile int flag=0;


    public FooBarVolatile(int n) {
        this.n = n;
    }

    public void foo(Runnable printFoo) throws InterruptedException {

        for (int i = 0; i < n; i++) {
            while (true){
                if(flag==0){
                    printFoo.run();
                    flag=1;
                    break;
                }
                Thread.sleep(1);
            }
        }
    }

    public void bar(Runnable printBar) throws InterruptedException {

        for (int i = 0; i < n; i++) {
            while (true){
                if(flag==1){
                    printBar.run();
                    flag=0;
                    break;
                }
                Thread.sleep(1);
            }
        }
    }
}

```

## 4.CAS(volatile write happens-before volatile read)

```
package P1115;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @autor yeqiaozhu.
 * @date 2019-08-01
 */
public class FooBarCAS {


        private int n;
        ////flag 0->foo to be print  1->foo has been printed
        private AtomicInteger flag=new AtomicInteger(0);



        public FooBarCAS(int n) {
            this.n = n;
        }

        public void foo(Runnable printFoo) throws InterruptedException {

            for (int i = 0; i < n; i++) {
                while (!flag.compareAndSet(0,1)){
                    Thread.sleep(1);
                }
                printFoo.run();
            }
        }

        public void bar(Runnable printBar) throws InterruptedException {

            for (int i = 0; i < n; i++) {
                while (!flag.compareAndSet(1,0)){
                    Thread.sleep(1);
                }
                printBar.run();
            }
        }

}

```
</p>


