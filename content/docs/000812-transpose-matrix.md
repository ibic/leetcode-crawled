---
title: "Transpose Matrix"
weight: 812
#id: "transpose-matrix"
---
## Description
<div class="description">
<p>Given a&nbsp;matrix <code>A</code>, return the transpose of <code>A</code>.</p>

<p>The transpose of a matrix is the matrix flipped over it&#39;s main diagonal, switching the row and column indices of the matrix.</p>

<br>
<img src="https://assets.leetcode.com/uploads/2019/10/20/hint_transpose.png" width="700"/>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[1,2,3],[4,5,6],[7,8,9]]</span>
<strong>Output: </strong><span id="example-output-1">[[1,4,7],[2,5,8],[3,6,9]]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[[1,2,3],[4,5,6]]</span>
<strong>Output: </strong><span id="example-output-2">[[1,4],[2,5],[3,6]]</span>
</pre>

<p>&nbsp;</p>

<p><span><strong>Note:</strong></span></p>

<ol>
	<li><code><span>1 &lt;= A.length&nbsp;&lt;= 1000</span></code></li>
	<li><code><span>1 &lt;= A[0].length&nbsp;&lt;= 1000</span></code></li>
</ol>
</div>
</div>
</div>

## Tags
- Array (array)

## Companies
- Apple - 2 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Copy Directly

**Intuition and Algorithm**

The transpose of a matrix `A` with dimensions `R x C` is a matrix `ans` with dimensions `C x R` for which `ans[c][r] = A[r][c]`.

Let's initialize a new matrix `ans` representing the answer.  Then, we'll copy each entry of the matrix as appropriate.

<iframe src="https://leetcode.com/playground/irg2mSz8/shared" frameBorder="0" width="100%" height="242" name="irg2mSz8"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(R * C)$$, where $$R$$ and $$C$$ are the number of rows and columns in the given matrix `A`.

* Space Complexity:  $$O(R * C)$$, the space used by the answer.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python - 1 Liner
- Author: shreyansh94
- Creation Date: Sun Jul 08 2018 11:01:00 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 08:49:54 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def transpose(self, A):
        return zip(*A)
```
</p>


### [C++/Java/Python] Easy Understood
- Author: lee215
- Creation Date: Sun Jul 08 2018 11:11:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 01:01:41 GMT+0800 (Singapore Standard Time)

<p>
**C++:**
```cpp
    vector<vector<int>> transpose(vector<vector<int>> A) {
        int M = A.size(), N = A[0].size();
        vector<vector<int>> B(N, vector<int>(M, 0));
        for (int j = 0; j < N; j++)
            for (int i = 0; i < M; i++)
                B[j][i] = A[i][j];
        return B;
    }
```

**Java:**
```java
    public int[][] transpose(int[][] A) {
        int M = A.length, N = A[0].length;
        int[][] B = new int[N][M];
        for (int j = 0; j < N; j++)
            for (int i = 0; i < M; i++)
                B[j][i] = A[i][j];
        return B;
    }
```
**Python:**
```py
    def tranpose(self, M):
        return zip(*M)
```
</p>


### Python self-explanatory 1 line solution
- Author: cenkay
- Creation Date: Sun Jul 08 2018 11:08:09 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 19 2019 19:31:05 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def transpose(self, A):
        return [[A[i][j] for i in range(len(A))] for j in range(len(A[0]))]
```
**Bonus:**
```
class Solution:
    def transpose(self, A):
        R = len(A)
        C = len(A[0])
        transpose = []
        for c in range(C):
            newRow = []
            for r in range(R):
                newRow.append(A[r][c])
            transpose.append(newRow)
        return transpose
```
</p>


