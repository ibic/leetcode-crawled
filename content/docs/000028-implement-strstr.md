---
title: "Implement strStr()"
weight: 28
#id: "implement-strstr"
---
## Description
<div class="description">
<p>Implement <a href="http://www.cplusplus.com/reference/cstring/strstr/" target="_blank">strStr()</a>.</p>

<p>Return the index of the first occurrence of needle in haystack, or <code>-1</code> if <code>needle</code> is not part of <code>haystack</code>.</p>

<p><strong>Clarification:</strong></p>

<p>What should we return when <code>needle</code> is an empty string? This is a great question to ask during an interview.</p>

<p>For the purpose of this problem, we will return 0 when <code>needle</code> is an empty string. This is consistent to C&#39;s&nbsp;<a href="http://www.cplusplus.com/reference/cstring/strstr/" target="_blank">strstr()</a> and Java&#39;s&nbsp;<a href="https://docs.oracle.com/javase/7/docs/api/java/lang/String.html#indexOf(java.lang.String)" target="_blank">indexOf()</a>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> haystack = "hello", needle = "ll"
<strong>Output:</strong> 2
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> haystack = "aaaaa", needle = "bba"
<strong>Output:</strong> -1
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> haystack = "", needle = ""
<strong>Output:</strong> 0
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= haystack.length, needle.length &lt;= 5 * 10<sup>4</sup></code></li>
	<li><code>haystack</code> and&nbsp;<code>needle</code> consist of only lower-case English characters.</li>
</ul>

</div>

## Tags
- Two Pointers (two-pointers)
- String (string)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: true)
- Yahoo - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Bloomberg - 4 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Dropbox - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Pocket Gems - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

The problem is to find needle of length L in the haystack of length N.

Let's discuss three different ideas how to proceed. 
They are all based on sliding window idea. 
The key point is how to implement a window slice.

Linear time window slice $$\mathcal{O}(L)$$ is quite easy, 
move the window of length L along the haystack 
and compare substring in the window with the needle. 
Overall that would result in 
$$\mathcal{O}((N - L)L)$$ time complexity.

Could that be improved? Yes. 
Two pointers approach is still the case of linear time slice, though 
the comparison happens not for all substrings, and that improves the best
time complexity up to $$\mathcal{O}(N)$$. 
The worst time complexity is still $$\mathcal{O}((N - L)L)$$ though.

Could that be improved to $$\mathcal{O}(N)$$? Yes, but one has to implement 
constant time slice $$\mathcal{O}(1)$$. There are two ways to do it:

- Rabin-Karp = constant-time slice using rolling hash algorithm.

- Bit manipulation = constant-time slice using bitmasks.

Bit Manipulation approach in Java is more suitable for the short 
strings or strings with very limited number of characters, ex. 
[Repeated DNA Sequences](https://leetcode.com/articles/repeated-dna-sequences/).
That's a consequence of overflow issues in Java
(in Python there is no such a problem).
Here we deal with quite long strings and it's more 
simple to implement the basic version of Rabin Karp algorithm.

#### Approach 1: Substring: Linear Time Slice

Quite straightforward approach - move sliding window along the string 
and compare substring in the window with the needle.

![fig](../Figures/28/substrings.png)

**Implementation**

<iframe src="https://leetcode.com/playground/MXqu2aJU/shared" frameBorder="0" width="100%" height="259" name="MXqu2aJU"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}((N - L)L)$$, where N is a length of 
haystack and L is a length of needle. We compute a substring
of length L in a loop, which is executed (N - L) times.

* Space complexity: $$\mathcal{O}(1)$$.
<br /> 
<br />


---
#### Approach 2: Two Pointers: Linear Time Slice 

Drawback of the previous algorithm is that one compares absolutely 
all substrings of length L with the needle.
There is no need to that. 

First, let's compare only substrings which starts from the first 
character in the needle substring. 

![fig](../Figures/28/two_pointers_start2.png)

Second, let's compare the characters one by one and stop 
immediately in the case of mismatch.

![fig](../Figures/28/mismatch2.png)

Here it was impossible to manage the full match up to the length
of needle string, which is L = 5. Let's backtrack then.
Note, that we move pn pointer back to the position 
pn = pn - curr_len + 1, and _not_ to the position pn = pn - curr_len,
since this last one was already investigated. 

![fig](../Figures/28/backtrack2.png)

Let's try again. Here we've managed to get the full match during the 
second attempt, so let's return the start position of that match,
pn - L.

![fig](../Figures/28/match.png)

**Algorithm**

- Move pn till you'll find the first character of the needle string
in the haystack.

- Compute the max string match by increasing pn, pL and curr_len in 
the case of equal characters. 

- If you managed to get the full match, curr_len == L,
return the start position of that match, pn - L.

- If you didn't, backtrack: pn = pn - curr_len + 1, pL = 0, curr_len = 0.

**Implementation**

<iframe src="https://leetcode.com/playground/ENhfQVVX/shared" frameBorder="0" width="100%" height="500" name="ENhfQVVX"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}((N - L)L)$$ in the worst case of 
numerous almost complete false matches, 
and $$\mathcal{O}(N)$$ in the best case of one single match.

* Space complexity: $$\mathcal{O}(1)$$.
<br /> 
<br />


---
#### Approach 3: Rabin Karp: Constant Time Slice

Let's now design the algorithm with $$\mathcal{O}(N)$$ time complexity
even in the worst case. The idea is simple: move along the string,
generate hash of substring in the sliding window and compare it 
with the reference hash of the needle string. 

There are two technical problems:

1. How to implement a string slice in a constant time? 

2. How to generate substring hash in a constant time?

**String slice in a constant time**

Strings are immutable in Java and Python, 
and to move sliding window in a constant time
one has to convert string to another data structure, 
for example, to integer array of ascii-values.

**Rolling hash: hash generation in a constant time**

To generate hash of array of length L, one needs $$\mathcal{O}(L)$$ time.

> How to have constant time of hash generation? Use the advantage of 
slice: only one integer in, and only one - out. 

That's the idea of [rolling hash](https://en.wikipedia.org/wiki/Rolling_hash).
Here we'll implement the simplest one, polynomial rolling hash.
Beware that's polynomial rolling hash is NOT the [Rabin fingerprint](https://en.wikipedia.org/wiki/Rolling_hash#Rabin_fingerprint).

Since one deals here with lowercase English letters, all values 
in the integer array are between 0 and 25 :
`arr[i] = (int)S.charAt(i) - (int)'a'`.  
So one could consider string `abcd` -> `[0, 1, 2, 3]` as a number 
in a [numeral system](https://en.wikipedia.org/wiki/Numeral_system) with the base 26. 
Hence `abcd` -> `[0, 1, 2, 3]` could be hashed as 

$$
h_0 = 0 \times 26^3 + 1 \times 26^2 + 2 \times 26^1 + 3 \times 26^0
$$

Let's write the same formula in a generalised way, where $$c_i$$
is an integer array element and $$a = 26$$ is a system base.

$$
h_0 = c_0 a^{L - 1} + c_1 a^{L - 2} + ... + c_i a^{L - 1 - i} + ... + c_{L - 1} a^1 + c_L a^0
$$

$$
h_0 = \sum_{i = 0}^{L - 1}{c_i a^{L - 1 - i}}
$$

Now let's consider the slice `abcd` -> `bcde`. For int arrays that means
`[0, 1, 2, 3]` -> `[1, 2, 3, 4]`, to remove number 0 and to add number 4.

$$
h_1 = (h_0 - 0 \times 26^3) \times 26 + 4 \times 26^0
$$

In a generalised way

$$
h_1 = (h_0 a - c_0 a^L) + c_{L + 1}
$$

Now hash regeneration is perfect and fits in a constant time. 
There is one more issue to address: possible overflow problem. 

**How to avoid overflow**

$$a^L$$ could be a large number and hence
the idea is to set limits to avoid the overflow. 
To set limits means to limit a hash by a given number called modulus
and use everywhere not hash itself but `h % modulus`.

It's quite obvious that modulus should be large enough, but how 
large? [Here one could read more about the topic](https://en.wikipedia.org/wiki/Linear_congruential_generator#Parameters_in_common_use),
for the problem here $$2^{31}$$ is enough.

**Algorithm**

- Compute the hash of substring `haystack.substring(0, L)` and
reference hash of `needle.substring(0, L)`.

- Iterate over the start position of possible match: from 1 to N - L.

    - Compute rolling hash based on the previous hash value.

    - Return start position if the hash is equal to the reference one.

- Return -1, that means that needle is not found.

**Implementation**

<iframe src="https://leetcode.com/playground/CfgNqtKj/shared" frameBorder="0" width="100%" height="500" name="CfgNqtKj"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, one computes the reference hash 
of the needle string in $$\mathcal{O}(L)$$ time, and then runs a loop of
$$(N - L)$$ steps with constant time operations in it.

* Space complexity: $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Elegant Java solution
- Author: jeantimex
- Creation Date: Thu Jul 16 2015 00:40:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 08:11:08 GMT+0800 (Singapore Standard Time)

<p>
    public int strStr(String haystack, String needle) {
      for (int i = 0; ; i++) {
        for (int j = 0; ; j++) {
          if (j == needle.length()) return i;
          if (i + j == haystack.length()) return -1;
          if (needle.charAt(j) != haystack.charAt(i + j)) break;
        }
      }
    }
</p>


### C++ Brute-Force and KMP
- Author: jianchao-li
- Creation Date: Sat Jun 06 2015 22:25:18 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 18 2018 21:35:38 GMT+0800 (Singapore Standard Time)

<p>
Traverse all the possible starting points of `haystack` (from `0` to `haystack.length() - needle.length()`) and see if the following characters in `haystack` match those of `needle`.

```cpp
class Solution {
public:
    int strStr(string haystack, string needle) {
        int m = haystack.size(), n = needle.size();
        for (int i = 0; i <= m - n; i++) {
            int j = 0;
            for (; j < n; j++) {
                if (haystack[i + j] != needle[j]) {
                    break;
                }
            }
            if (j == n) {
                return i;
            }
        }
        return -1;
    }
};
```

The following is another implementation, shorter but harder to understand.

```cpp
class Solution {
public:
    int strStr(string haystack, string needle) {
        int m = haystack.size(), n = needle.size(), p = 0;
        while (p + n - 1 < m) {
            if (haystack.substr(p, n) == needle) {
                return p;
            }
            while (p++ + n - 1 < m && haystack[p] != needle[0]);
        }
        return -1;
    }
};
```

Finally comes the KMP algorithm. You may refer to [KMP on jBoxer\'s blog](http://jakeboxer.com/blog/2009/12/13/the-knuth-morris-pratt-algorithm-in-my-own-words/) and [KMP on geeksforgeeks](http://www.geeksforgeeks.org/searching-for-patterns-set-2-kmp-algorithm/) for some explanations. I rewrote the code from the second link.

```cpp
class Solution {
public:
    int strStr(string haystack, string needle) {
        int m = haystack.size(), n = needle.size();
        if (!n) {
            return 0;
        }
        vector<int> lps = kmpProcess(needle);
        for (int i = 0, j = 0; i < m;) {
            if (haystack[i] == needle[j]) { 
                i++, j++;
            }
            if (j == n) {
                return i - j;
            }
            if (i < m && haystack[i] != needle[j]) {
                j ? j = lps[j - 1] : i++;
            }
        }
        return -1;
    }
private:
    vector<int> kmpProcess(string needle) {
        int n = needle.size();
        vector<int> lps(n, 0);
        for (int i = 1, len = 0; i < n;) {
            if (needle[i] == needle[len]) {
                lps[i++] = ++len;
            } else if (len) {
                len = lps[len - 1];
            } else {
                lps[i++] = 0;
            }
        }
        return lps;
    }
};
```
</p>


### My answer by Python
- Author: ChangYuXiaoXiao
- Creation Date: Wed Nov 18 2015 08:52:24 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 23:55:53 GMT+0800 (Singapore Standard Time)

<p>
    class Solution(object):
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        for i in range(len(haystack) - len(needle)+1):
            if haystack[i:i+len(needle)] == needle:
                return i
        return -1
</p>


