---
title: "Smallest Rotation with Highest Score"
weight: 735
#id: "smallest-rotation-with-highest-score"
---
## Description
<div class="description">
<p>&nbsp;Given an array <code>A</code>, we may rotate it by a non-negative integer <code>K</code> so that the array becomes <code>A[K], A[K+1], A{K+2], ... A[A.length - 1], A[0], A[1], ..., A[K-1]</code>.&nbsp; Afterward, any entries that are less than or equal to their index are worth 1 point.&nbsp;</p>

<p>For example, if we have <code>[2, 4, 1, 3, 0]</code>, and we rotate by <code>K = 2</code>, it becomes <code>[1, 3, 0, 2, 4]</code>.&nbsp; This is worth 3 points because 1 &gt; 0 [no points], 3 &gt; 1 [no points], 0 &lt;= 2 [one point], 2 &lt;= 3 [one point], 4 &lt;= 4 [one point].</p>

<p>Over all possible rotations, return the rotation index K that corresponds to the highest score we could receive.&nbsp; If there are multiple answers, return the smallest such index K.</p>

<pre>
<strong>Example 1:</strong>
<strong>Input:</strong> [2, 3, 1, 4, 0]
<strong>Output:</strong> 3
<strong>Explanation: </strong> 
Scores for each K are listed below: 
K = 0,  A = [2,3,1,4,0],    score 2
K = 1,  A = [3,1,4,0,2],    score 3
K = 2,  A = [1,4,0,2,3],    score 3
K = 3,  A = [4,0,2,3,1],    score 4
K = 4,  A = [0,2,3,1,4],    score 3
</pre>

<p>So we should choose K = 3, which has the highest score.</p>

<p>&nbsp;</p>

<pre>
<strong>Example 2:</strong>
<strong>Input:</strong> [1, 3, 0, 2, 4]
<strong>Output:</strong> 0
<strong>Explanation: </strong> A will always have 3 points no matter how it shifts.
So we will choose the smallest K, which is 0.
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li><code>A</code>&nbsp;will have&nbsp;length at most <code>20000</code>.</li>
	<li><code>A[i]</code> will be in the range <code>[0, A.length]</code>.</li>
</ul>

</div>

## Tags


## Companies


## Official Solution
[TOC]

---
#### Approach #1: Interval Stabbing [Accepted]

**Intuition**

Say `N = 10` and `A[2] = 5`.  Then there are 5 rotations that are bad for this number: rotation indexes `0, 1, 2, 8, 9` - these rotations will cause this number to not get 1 point later.

In general, for each number in the array, we can map out what rotation indexes will be bad for this number.  It will always be a region of one interval, possibly two if the interval wraps around (eg. `8, 9, 0, 1, 2` wraps around, to become `[8, 9]` and `[0, 1, 2]`.)

At the end of plotting these intervals, we need to know which rotation index has the least intervals overlapping it - this one is the answer.

**Algorithm**

First, an element like `A[2] = 5` will not get score in (up to) 5 posiitons: when the 5 is at final index 0, 1, 2, 3, or 4.  When we shift by 2, we'll get final index 0.  If we shift `5-1 = 4` before this, this element will end up at final index 4.  In general (modulo N), a shift of `i - A[i] + 1` to `i` will be the rotation indexes that will make `A[i]` not score a point.

If we are trying to plot an interval like `[2, 3, 4]`, then instead of doing `bad[2]--; bad[3]--; bad[4]--;`, what we will do instead is keep track of the cumulative total: `bad[2]--; bad[5]++`.  For "wrap-around" intervals like `[8, 9, 0, 1, 2]`, we will keep track of this as two separate intervals: `bad[8]--, bad[10]++, bad[0]--, bad[3]++`.  (Actually, because of our implementation, we don't need to remember the `bad[10]++` part.)

At the end, we want to find a rotation index with the least intervals overlapping.  We'll maintain a cumulative total `cur` representing how many intervals are currently overlapping our current rotation index, then update it as we step through each rotation index.

<iframe src="https://leetcode.com/playground/mRb99d8k/shared" frameBorder="0" width="100%" height="480" name="mRb99d8k"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity: $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
###  [C++/Java/Python] Solution with Explanation
- Author: lee215
- Creation Date: Sun Mar 11 2018 16:53:42 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 03:53:44 GMT+0800 (Singapore Standard Time)

<p>
**Key point**
Don\'t calculate the score for ```K=0```, we don\'t need it at all.
(I see almost all other solutions did)
The key point is to find out how score changes when ```K++```

**Time complexity:**
\u201CA will have length at most 20000.\u201D
I think it means you should find a O(N) solution.

**How dosen score change when K++ ?**
* **Get point**
Each time when we rotate, we make index ```0``` to index ```N-1```, then we get one more point.
We know that for sure, so I don\'t need to record it.

* **Lose point**
```(i - A[i] + N) % N``` is the value of K making ```A[i]\'s``` index just equal to ```A[i]```.
For example, If ```A[6] = 1```, then ```K = (6 - A[6]) % 6 = 5``` making ```A[6]``` to ```index 1``` of new array.
So when ```K=5```, we get this point for ```A[6]```
Then if ```K``` is bigger when ```K = (i - A[i] + 1) % N```, we start to lose this point, making our score -= 1
All I have done is record the value of ```K```  for all ```A[i]``` where we will lose points.

* **```A[i]=0```**
Rotation makes no change for it, becasue we alwars have ```0 <= index```.
However, it is covered in "get point" and "lose point".

**Explanation of codes**
1. Search the index where score decrease and record this changement to a list ```change```.
2. A simple for loop to calculate the score for every ```K``` value. 
```score[K] = score[K-1] + change[K]```
In my codes I accumulated  ```changes``` so I get the changed score for every ```K``` value compared to ```K=0``` 
3. Find the index of best score.


C++:
```
    int bestRotation(vector<int>& A) {
        int N = A.size();
        int change[N] = {0};
        for (int i = 0; i < N; ++i) change[(i - A[i] + 1 + N) % N] -= 1;
        for (int i = 1; i < N; ++i) change[i] += change[i - 1] + 1;
        return distance(change, max_element(change, change + N));
    }
```
Java
```
    public int bestRotation(int[] A) {
        int N = A.length;
        int change[] = new int[N];
        for (int i = 0; i < N; ++i) change[(i - A[i] + 1 + N) % N] -= 1;
        int max_i = 0;
        for (int i = 1; i < N; ++i) {
            change[i] += change[i - 1] + 1;
            max_i = change[i] > change[max_i] ? i : max_i;
        }
        return max_i;
    }
```
Python
```
    def bestRotation(self, A):
        N = len(A)
        change = [1] * N
        for i in range(N): change[(i - A[i] + 1) % N] -= 1
        for i in range(1, N): change[i] += change[i - 1]
        return change.index(max(change))

</p>


### Simple Java O(n) Solution
- Author: ibmtp380
- Creation Date: Mon Mar 12 2018 09:13:48 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 06:13:06 GMT+0800 (Singapore Standard Time)

<p>
The trick of this problem is to convert it into finding most overlap interval problem (like meeting room II, finding most airplanes at a time)

```
class Solution {
    public int bestRotation(int[] A) {
        int n = A.length;
        int[] a = new int[n];  // to record interval start/end
        for (int i = 0; i < A.length; i++) {
            a[(i + 1) % n]++;             // interval start
            a[(i + 1 - A[i] + n) % n]--;  // interval end
        }
        int count = 0;
        int maxCount = -1;
        int res = 0;
        for (int i = 0; i < n; i++) {  // find most overlap interval
            count += a[i];
            if (count > maxCount) {
                res = i;
                maxCount = count;
            }
        }
        return res;
    }
}
```
</p>


### A (hopefully) understandable explanation O(n)
- Author: harsh700ca
- Creation Date: Sun May 13 2018 06:27:37 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 23 2018 22:55:17 GMT+0800 (Singapore Standard Time)

<p>
This problem can be thought of as a transformation to a more well known problem. After we have the transformation, we can solve it effiently.

**Definitions**
Let A be the input array, and N be the number of elements in the array. Let A[i] denote the value at index i in input array A. And let K be the number of rotations.

**The Transformation**
For every number in the  array, write the ranges which the value will yield a score of + 1.

 Example:
 A: [2, 3, 1, 4, 0]
 Range for 2: [1, 3] (Rotating the array for any value of 1 <= K <= 3 will give us a point for 2.
 Range for 3: [2, 3]
 Range for 1: [0, 1] and [3, 4]
 ... and so on
 
 After doing that, the value of K that gives the highest score is the value that is common to the most # of ranges. The question is how do we solve for that value? A simple approach would be to keep an array, called count, with indices from 0 to 4, and add 1 to each indice for every range that contains it. At the end the best value of k is  the indice for which count has the largest value.
 
 Example:
 count would look like this if we added + 1 for range [1, 3]: count [0, 1, 1, 1, 0]. We would continue to add for every range. So after adding range [2, 3], count would look like [0, 1,  2, 2, 0] ... and so on.
 
 But this would be O(n^2). We can improve this by making a simple adaptation. To represent some range [a, b] we can instead add +1 to only the indice a, and subtract 1 to the last b + 1 (if it exists). This would mean that we add +1 to every index >= a, and subtract -1 to every index b + 1. After doing this for every range, we can accumate from the front, we should get the same array count, as we did with out n^2 version.
 
```
 class Solution(object):
    def bestRotation(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        count = [0]*len(A)
        for i in range(len(A)):
            if A[i] <= i:
                mini = 0
                maxi = i - A[i]
                count[mini] += 1
                if maxi + 1 < len(A): count[maxi + 1] += -1
                if i + 1 < len(A): count[i+1] += 1
            else:
                if A[i] == len(A):
                    continue #no valid range
                mini = i + 1
                maxi = len(A) - (A[i] - i)
                print(mini, maxi)
                count[mini] += 1
                if maxi + 1 < len(A): count[maxi + 1] += -1
        for i in range(1, len(A)):
            count[i] += count[i-1]
        return count.index(max(count))
                
                
                    
```
 
</p>


