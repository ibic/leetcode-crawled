---
title: "Closest Leaf in a Binary Tree"
weight: 664
#id: "closest-leaf-in-a-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree <b>where every node has a unique value</b>, and a target key <code>k</code>, find the value of the nearest leaf node to target <code>k</code> in the tree.
</p><p>
Here, <i>nearest</i> to a leaf means the least number of edges travelled on the binary tree to reach any leaf of the tree.  Also, a node is called a <i>leaf</i> if it has no children.
</p><p>
In the following examples, the input tree is represented in flattened form row by row.
The actual <code>root</code> tree given will be a TreeNode object.
</p><p>
<b>Example 1:</b>
<pre>
<b>Input:</b>
root = [1, 3, 2], k = 1
Diagram of binary tree:
          1
         / \
        3   2

<b>Output:</b> 2 (or 3)

<b>Explanation:</b> Either 2 or 3 is the nearest leaf node to the target of 1.
</pre>
</p><p>
<b>Example 2:</b>
<pre>
<b>Input:</b>
root = [1], k = 1
<b>Output:</b> 1

<b>Explanation:</b> The nearest leaf node is the root node itself.
</pre>
</p>

<p>
<b>Example 3:</b>
<pre>
<b>Input:</b>
root = [1,2,3,4,null,null,null,5,null,6], k = 2
Diagram of binary tree:
             1
            / \
           2   3
          /
         4
        /
       5
      /
     6

<b>Output:</b> 3
<b>Explanation:</b> The leaf node with value 3 (and not the leaf node with value 6) is nearest to the node with value 2.
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li><code>root</code> represents a binary tree with at least <code>1</code> node and at most <code>1000</code> nodes.</li>
<li>Every node has a unique <code>node.val</code> in range <code>[1, 1000]</code>.</li>
<li>There exists some node in the given binary tree for which <code>node.val == k</code>.</li>
</ol>
</p>
</div>

## Tags
- Tree (tree)

## Companies
- Databricks - 4 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- JPMorgan - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Convert to Graph [Accepted]

**Intuition**

Instead of a binary tree, if we converted the tree to a general graph, we could find the shortest path to a leaf using breadth-first search.

**Algorithm**

We use a depth-first search to record in our graph each edge travelled from parent to node.

After, we use a breadth-first search on nodes that started with a value of `k`, so that we are visiting nodes in order of their distance to `k`.  When the node is a leaf (it has one outgoing edge, where the `root` has a "ghost" edge to `null`), it must be the answer.

<iframe src="https://leetcode.com/playground/DcqcYwow/shared" frameBorder="0" width="100%" height="500" name="DcqcYwow"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ where $$N$$ is the number of nodes in the given input tree.  We visit every node a constant number of times.

* Space Complexity: $$O(N)$$, the size of the graph.

---
#### Approach #2: Annotate Closest Leaf [Accepted]

**Intuition and Algorithm**

Say from each node, we already knew where the closest leaf in it's subtree is.  Using any kind of traversal plus memoization, we can remember this information.

Then the closest leaf to the target (in general, not just subtree) has to have a lowest common ancestor with the `target` that is on the path from the `root` to the `target`.  We can find the path from `root` to `target` via any kind of traversal, and look at our annotation for each node on this path to determine all leaf candidates, choosing the best one.

<iframe src="https://leetcode.com/playground/yeQeLYBq/shared" frameBorder="0" width="100%" height="500" name="yeQeLYBq"></iframe>

**Complexity Analysis**

* Time and Space Complexity: $$O(N)$$.  The analysis is the same as in *Approach #1*.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java DFS + BFS 27ms
- Author: cwleoo
- Creation Date: Sun Dec 10 2017 12:32:45 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 02:31:29 GMT+0800 (Singapore Standard Time)

<p>
My solution is simple: 
1. First, preform DFS on root in order to find the node whose val = k, at the meantime use `HashMap` to keep record of all back edges from child to parent;
2. Then perform BFS on this node to find the closest leaf node.
```
class Solution {
    
    public int findClosestLeaf(TreeNode root, int k) {
        Map<TreeNode, TreeNode> backMap = new HashMap<>();   // store all edges that trace node back to its parent
        Queue<TreeNode> queue = new LinkedList<>();          // the queue used in BFS
        Set<TreeNode> visited = new HashSet<>();             // store all visited nodes
        
        // DFS: search for node whoes val == k
        TreeNode kNode = DFS(root, k, backMap);
        queue.add(kNode);
        visited.add(kNode);
        
        // BFS: find the shortest path
        while(!queue.isEmpty()) {
            TreeNode curr = queue.poll();
            if(curr.left == null && curr.right == null) {
                return curr.val;
            }
            if(curr.left != null && visited.add(curr.left)) {
                queue.add(curr.left);
            }
            if(curr.right != null && visited.add(curr.right)) {
                queue.add(curr.right);
            }
            if(backMap.containsKey(curr) && visited.add(backMap.get(curr))) {  // go alone the back edge
                queue.add(backMap.get(curr));
            }
        }
        return -1; // never hit
    }
    
    private TreeNode DFS(TreeNode root, int k, Map<TreeNode, TreeNode> backMap) {
        if(root.val == k) {
            return root;
        }
        if(root.left != null) {
            backMap.put(root.left, root);        // add back edge
            TreeNode left = DFS(root.left, k, backMap);
            if(left != null) return left;
        }
        if(root.right != null) {
            backMap.put(root.right, root);       // add back edge
            TreeNode right = DFS(root.right, k, backMap);
            if(right != null) return right;
        }
        return null;
    }
    
}
```
</p>


### Who ever wrote the description for this problem...
- Author: galster
- Creation Date: Mon Dec 11 2017 03:06:01 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 06:08:13 GMT+0800 (Singapore Standard Time)

<p>
Should be immediately banned from submitting questions. What a horrible description!!! The word "closest" could be interpreted in so many ways. The fact that this person did not even consider that others will get confused by his/her choice of words says a lot about their communication style.
</p>


### Intuitive Python O(n) - BFS on Undirected Graph
- Author: yangshun
- Creation Date: Sun Dec 10 2017 15:06:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 22:14:31 GMT+0800 (Singapore Standard Time)

<p>
Treat the tree as an undirected graph and perform BFS from the target node to find the first leaf node. 

We first run `traverse` function to convert the tree into an undirected graph and keep track of the leaf nodes in a set. 

For a tree that looks like this:

```py
             1
            / \
           2   3
          /
         4
        /
       5
      /
     6
```

the `graph` dict and `leaves` set look like:

```py
# graph
{
  1: [2, 3],
  2: [1, 4],
  3: [1],
  4: [2, 5],
  5: [4, 6],
  6: [5],
}

# leaves
{3, 6}
```

Note that a node can never have more than three neighbors as per the Binary Tree property (two children, one parent).

Then we perform a BFS on the graph from node K, terminating as soon as we find a leaf node.

*- Yangshun*

```
class Solution(object):
    def findClosestLeaf(self, root, k):
        # Time: O(n)
        # Space: O(n)
        from collections import defaultdict
        graph, leaves = defaultdict(list), set()
        # Graph construction
        def traverse(node):
            if not node:
                return
            if not node.left and not node.right:
                leaves.add(node.val)
                return
            if node.left:
                graph[node.val].append(node.left.val)
                graph[node.left.val].append(node.val)
                traverse(node.left)
            if node.right:
                graph[node.val].append(node.right.val)
                graph[node.right.val].append(node.val)
                traverse(node.right)
        traverse(root)
        # Graph traversal - BFS
        queue = [k]
        while len(queue):
            next_queue = []
            for node in queue:
                if node in leaves:
                    return node
                next_queue += graph.pop(node, [])
            queue = next_queue
```

Thanks to @ManuelP for shortening my solution yet again \U0001f389
</p>


