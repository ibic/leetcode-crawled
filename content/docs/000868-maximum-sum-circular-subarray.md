---
title: "Maximum Sum Circular Subarray"
weight: 868
#id: "maximum-sum-circular-subarray"
---
## Description
<div class="description">
<p>Given a <strong>circular&nbsp;array</strong>&nbsp;<strong>C</strong> of integers represented by&nbsp;<code>A</code>, find the maximum possible sum of a non-empty subarray of <strong>C</strong>.</p>

<p>Here, a&nbsp;<em>circular&nbsp;array</em> means the end of the array connects to the beginning of the array.&nbsp; (Formally, <code>C[i] = A[i]</code> when <code>0 &lt;= i &lt; A.length</code>, and <code>C[i+A.length] = C[i]</code>&nbsp;when&nbsp;<code>i &gt;= 0</code>.)</p>

<p>Also, a subarray may only include each element of the fixed buffer <code>A</code> at most once.&nbsp; (Formally, for a subarray <code>C[i], C[i+1], ..., C[j]</code>, there does not exist <code>i &lt;= k1, k2 &lt;= j</code> with <code>k1 % A.length&nbsp;= k2 % A.length</code>.)</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,-2,3,-2]</span>
<strong>Output: </strong><span id="example-output-1">3
<strong>Explanation: </strong>Subarray [3] has maximum sum 3</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[5,-3,5]</span>
<strong>Output: </strong><span id="example-output-2">10
</span><span id="example-output-3"><strong>Explanation:</strong>&nbsp;</span><span id="example-output-1">Subarray [5,5] has maximum sum </span><span>5 + 5 = 10</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[3,-1,2,-1]</span>
<strong>Output: </strong><span id="example-output-3">4
<strong>Explanation:</strong>&nbsp;</span><span id="example-output-1">Subarray [2,-1,3] has maximum sum </span><span>2 + (-1) + 3 = 4</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">[3,-2,2,-3]</span>
<strong>Output: </strong><span id="example-output-4">3
</span><span id="example-output-3"><strong>Explanation:</strong>&nbsp;</span><span id="example-output-1">Subarray [3] and [3,-2,2] both have maximum sum </span><span>3</span>
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-5-1">[-2,-3,-1]</span>
<strong>Output: </strong><span id="example-output-5">-1
</span><span id="example-output-3"><strong>Explanation:</strong>&nbsp;</span><span id="example-output-1">Subarray [-1] has maximum sum -1</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note: </strong></p>

<ol>
	<li><code>-30000 &lt;= A[i] &lt;= 30000</code></li>
	<li><code>1 &lt;= A.length &lt;= 30000</code></li>
</ol>
</div>
</div>
</div>
</div>

</div>

## Tags
- Array (array)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Two Sigma - 2 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Notes and A Primer on Kadane's Algorithm

**About the Approaches**

In both Approach 1 and Approach 2, "grindy" solutions are presented that require less insight, but may be more intuitive to those with a solid grasp of the techniques in those approaches.  Without prior experience, these approaches would be very challenging to emulate.

Approaches 3 and 4 are much easier to implement, but require some insight.

**Explanation of Kadane's Algorithm**

To understand the solutions in this article, we need some familiarity with Kadane's algorithm.  In this section, we will explain the core idea behind it.

For a given array `A`, Kadane's algorithm can be used to find the maximum sum of the subarrays of `A`.  Here, we only consider non-empty subarrays.

Kadane's algorithm is based on dynamic programming.  Let `dp[j]` be the maximum sum of a subarray that ends in `A[j]`.  That is,

$$
\text{dp}[j] = \max\limits_i (A[i] + A[i+1] + \cdots + A[j])
$$

Then, a subarray ending in `j+1` (such as `A[i], A[i+1] + ... + A[j+1]`) maximizes the `A[i] + ... + A[j]` part of the sum by being equal to `dp[j]` if it is non-empty, and `0` if it is.  Thus, we have the recurrence:

$$
\text{dp}[j+1] = A[j+1] + \max(\text{dp}[j], 0)
$$

Since a subarray must end somewhere, $$\max\limits_j dp[j]$$ must be the desired answer.

To compute `dp` efficiently, Kadane's algorithm is usually written in the form that reduces space complexity.  We maintain two variables: `ans` as $$\max\limits_j dp[j]$$, and `cur` as $$dp[j]$$; and update them as $$j$$ iterates from $$0$$ to $$A\text{.length} - 1$$.

Then, Kadane's algorithm is given by the following psuedocode:

```python
#Kadane's algorithm
ans = cur = None
for x in A:
    cur = x + max(cur, 0)
    ans = max(ans, cur)
return ans
```


---
#### Approach 1: Next Array

**Intuition and Algorithm**

Subarrays of circular arrays can be classified as either as *one-interval* subarrays, or *two-interval* subarrays, depending on how many intervals of the fixed-size buffer `A` are required to represent them.

For example, if `A = [0, 1, 2, 3, 4, 5, 6]` is the underlying buffer of our circular array, we could represent the subarray `[2, 3, 4]` as one interval $$[2, 4]$$, but we would represent the subarray `[5, 6, 0, 1]` as two intervals $$[5, 6], [0, 1]$$.

Using Kadane's algorithm, we know how to get the maximum of *one-interval* subarrays, so it only remains to consider *two-interval* subarrays.

Let's say the intervals are $$[0, i], [j, A\text{.length} - 1]$$.  Let's try to compute the *i-th candidate*: the largest possible sum of a two-interval subarray for a given $$i$$.  Computing the $$[0, i]$$ part of the sum is easy.  Let's write

$$
T_j = A[j] + A[j+1] + \cdots + A[A\text{.length} - 1]
$$

and

$$
R_j = \max\limits_{k \geq j} T_k
$$

so that the desired i-th candidate is:

$$
(A[0] + A[1] + \cdots + A[i]) + R_{i+2}
$$

Since we can compute $$T_j$$ and $$R_j$$ in linear time, the answer is straightforward after this setup.


<iframe src="https://leetcode.com/playground/inU2gPTt/shared" frameBorder="0" width="100%" height="500" name="inU2gPTt"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Prefix Sums + Monoqueue

**Intuition**

First, we can frame the problem as a problem on a fixed array.

We can consider any subarray of the circular array with buffer `A`, to be a subarray of the fixed array `A+A`.

For example, if `A = [0,1,2,3,4,5]` represents a circular array, then the subarray `[4,5,0,1]` is also a subarray of fixed array `[0,1,2,3,4,5,0,1,2,3,4,5]`.  Let `B = A+A` be this fixed array.

Now say $$N = A\text{.length}$$, and consider the prefix sums

$$
P_k = B[0] + B[1] + \cdots + B[k-1]
$$

Then, we want the largest $$P_j - P_i$$ where $$j - i \leq N$$.

Now, consider the j-th candidate answer: the best possible $$P_j - P_i$$ for a fixed $$j$$.  We want the $$i$$ so that $$P_i$$ is smallest, with $$j - N \leq i < j$$.  Let's call this the *optimal i for the j-th candidate answer*.  We can use a monoqueue to manage this.

**Algorithm**

Iterate forwards through $$j$$, computing the $$j$$-th candidate answer at each step.  We'll maintain a `queue` of potentially optimal $$i$$'s.

The main idea is that if $$i_1 < i_2$$ and $$P_{i_1} \geq P_{i_2}$$, then we don't need to remember $$i_1$$ anymore.

Please see the inline comments for more algorithmic details about managing the queue.

<iframe src="https://leetcode.com/playground/B353GP6q/shared" frameBorder="0" width="100%" height="500" name="B353GP6q"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 3: Kadane's (Sign Variant)

**Intuition and Algorithm**

As in Approach 1, subarrays of circular arrays can be classified as either as *one-interval* subarrays, or *two-interval* subarrays.

Using Kadane's algorithm `kadane` for finding the maximum sum of non-empty subarrays, the answer for one-interval subarrays is `kadane(A)`.

Now, let $$N = A\text{.length}$$.  For a two-interval subarray like:

$$
(A_0 + A_1 + \cdots + A_i) + (A_j + A_{j+1} + \cdots + A_{N - 1})
$$

we can write this as

$$
(\sum_{k=0}^{N-1} A_k) - (A_{i+1} + A_{i+2} + \cdots + A_{j-1})
$$

For two-interval subarrays, let $$B$$ be the array $$A$$ with each element multiplied by $$-1$$.  Then the answer for two-interval subarrays is $$\text{sum}(A) + \text{kadane}(B)$$.

Except, this isn't quite true, as if the subarray of $$B$$ we choose is the entire array, the resulting two interval subarray $$[0, i] + [j, N-1]$$ would be empty.

We can remedy this problem by doing Kadane twice: once on $$B$$ with the first element removed, and once on $$B$$ with the last element removed.

<iframe src="https://leetcode.com/playground/YsCM6TR5/shared" frameBorder="0" width="100%" height="463" name="YsCM6TR5"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$ in additional space complexity.
<br />
<br />


---
#### Approach 4: Kadane's (Min Variant)

**Intuition and Algorithm**

As in Approach 3, subarrays of circular arrays can be classified as either as *one-interval* subarrays (which we can use Kadane's algorithm), or *two-interval* subarrays.

We can modify Kadane's algorithm to use `min` instead of `max`.  All the math in our explanation of Kadane's algorithm remains the same, but the algorithm lets us find the minimum sum of a subarray instead.

For a two interval subarray written as $$(\sum_{k=0}^{N-1} A_k) - (\sum_{k=i+1}^{j-1} A_k)$$, we can use our `kadane-min` algorithm to minimize the "interior" $$(\sum_{k=i+1}^{j-1} A_k)$$ part of the sum.

Again, because the interior $$[i+1, j-1]$$ must be non-empty, we can break up our search into a search on `A[1:]` and on `A[:-1]`.

<iframe src="https://leetcode.com/playground/aJPu3ACt/shared" frameBorder="0" width="100%" height="500" name="aJPu3ACt"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$ in additional space complexity.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### One Pass
- Author: lee215
- Creation Date: Sun Oct 07 2018 11:06:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 16 2020 18:26:18 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
I guess you know how to solve max subarray sum (without circular).
If not, you can have a reference here: 53. Maximum Subarray
<br>

# Explanation
So there are two case.
Case 1. The first is that the subarray take only a middle part, and we know how to find the max subarray sum.
Case2. The second is that the subarray take a part of head array and a part of tail array.
We can transfer this case to the first one.
The maximum result equals to the total sum minus the minimum subarray sum.
<br>

Here is a diagram by @motorix:
![image](https://assets.leetcode.com/users/motorix/image_1538888300.png)

So the max subarray circular sum equals to
`max(the max subarray sum, the total sum - the min subarray sum)`
<br>

# Prove of the second case
  max(prefix+suffix)
= max(total sum - subarray)
= total sum + max(-subarray)
= total sum - min(subarray)
<br>

# Corner case
Just one to pay attention:
If all numbers are negative, `maxSum = max(A)` and `minSum = sum(A)`.
In this case, `max(maxSum, total - minSum) = 0`, which means the sum of an empty subarray.
According to the deacription, We need to return the `max(A)`, instead of sum of am empty subarray.
So we return the `maxSum` to handle this corner case.
<br>

# Complexity
One pass, time `O(N)`
No extra space, space `O(1)`
<br>

**C++:**
```cpp
    int maxSubarraySumCircular(vector<int>& A) {
        int total = 0, maxSum = A[0], curMax = 0, minSum = A[0], curMin = 0;
        for (int& a : A) {
            curMax = max(curMax + a, a);
            maxSum = max(maxSum, curMax);
            curMin = min(curMin + a, a);
            minSum = min(minSum, curMin);
            total += a;
        }
        return maxSum > 0 ? max(maxSum, total - minSum) : maxSum;
    }
```

**Java:**
```java
    public int maxSubarraySumCircular(int[] A) {
        int total = 0, maxSum = A[0], curMax = 0, minSum = A[0], curMin = 0;
        for (int a : A) {
            curMax = Math.max(curMax + a, a);
            maxSum = Math.max(maxSum, curMax);
            curMin = Math.min(curMin + a, a);
            minSum = Math.min(minSum, curMin);
            total += a;
        }
        return maxSum > 0 ? Math.max(maxSum, total - minSum) : maxSum;
    }
```

**Python:**
```py
    def maxSubarraySumCircular(self, A):
        total, maxSum, curMax, minSum, curMin = 0, A[0], 0, A[0], 0
        for a in A:
            curMax = max(curMax + a, a)
            maxSum = max(maxSum, curMax)
            curMin = min(curMin + a, a)
            minSum = min(minSum, curMin)
            total += a
        return max(maxSum, total - minSum) if maxSum > 0 else maxSum
```

</p>


### Java | C++ | Python3 | With detailed explanation | O(N) time | O(1)
- Author: logan138
- Creation Date: Fri May 15 2020 15:42:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 16 2020 14:52:42 GMT+0800 (Singapore Standard Time)

<p>
```
EXPLANATION:-
EX 1:-
   A = [1, -2, 3, -2]
   max = 3   (This means max subarray sum for normal array)
   min = -2  (This means min subarray sum for normal array)
   sum = 0   (total array sum)
   Maximum Sum = 3 (max)

EX2:-
  A = [5, -3, 5]
  max = 7
  min  = -3
  sum = 7
  maximum subarray sum = 7 - (-3) = 10
  
EX3:-
   A = [3, -1, 2, -1]
   max = 4
   min = -1
   sum = 3
   maximum subarray sum  = sum - min = 3 - (-1) = 4

EX4:-
   A = [-2, -3, -1]
   max = -1
   min = -6
   sum = -6
   In this example, if we do (sum - min) then result is 0. but there is no subarray with sum 0.
   So, in this case we return max value. that is -1.
   
From above examples, 
we can understand that 
maximum sum is either max (we get using kadane\'s algo) or (sum - min).
There is a special case, if sum == min,
then maximum sum is max.

Let\'s develop an algorithm to solve this problem.
1. Find maximum subarray sum using kadane\'s algorithm (max) 
2. Find minimum subarray sum using kadane\'s algorithm (min)
3. Find total sum of the array (sum)
4. Now, if sum == min return max
5. Otherwise, return maximum ( max, sum - min )

IF YOU HAVE ANY DOUBTS, FEEL FREE TO ASK
IF YOU UNDERSTAND, DON\'T FORGET TO UPVOTE.

IF YOU WANT MORE ANSWERS, PLEASE FOLLOW ME.

Time :- O(N)
Space :- O(1)
```
```
JAVA:-
class Solution {
    public int maxSubarraySumCircular(int[] A) {
        if(A.length == 0) return 0;
        int sum = A[0];
        int maxSoFar = A[0];
        int maxTotal = A[0];
        int minTotal = A[0];
        int minSoFar = A[0];
        for(int i = 1; i < A.length; i++){
            int num = A[i];
            maxSoFar = Math.max(num, maxSoFar + num);
            maxTotal = Math.max(maxSoFar, maxTotal);
            
            minSoFar = Math.min(num, minSoFar + num);
            minTotal = Math.min(minTotal, minSoFar);
            
            sum += num;
        }
        if(sum == minTotal) return maxTotal;
        return Math.max(sum - minTotal, maxTotal);
    }
}

C++:-
class Solution {
public:
    int maxSubarraySumCircular(vector<int>& A) {
        if(A.size() == 0) return 0;
        int sum = A[0];
        int maxSoFar = A[0];
        int maxTotal = A[0];
        int minSoFar = A[0];
        int minTotal = A[0];
        for(int i = 1; i< A.size();i++){
            maxSoFar = max(A[i], maxSoFar + A[i]);
            maxTotal = max(maxTotal, maxSoFar);
            
            minSoFar = min(A[i], minSoFar + A[i]);
            minTotal = min(minTotal, minSoFar);
            sum += A[i];

        }
        if(sum == minTotal) return maxTotal;
        return max(sum - minTotal, maxTotal);
    }
};

Python3:-
class Solution:
    def maxSubarraySumCircular(self, A: List[int]) -> int:
        if len(A) == 0:
            return 0
        maxTotal,maxSoFar,minSoFar,minTotal,s = A[0], A[0], A[0], A[0],A[0]
        for i in range(1, len(A)):
            maxSoFar = max(A[i], maxSoFar + A[i])
            maxTotal = max(maxTotal, maxSoFar)            
            
            minSoFar = min(A[i], minSoFar + A[i])            
            minTotal = min(minTotal, minSoFar)            
            s += A[i]
        if(s == minTotal):
            return maxTotal
        
        return max(s - minTotal, maxTotal);
```
</p>


### Kadane Algorithm Trick, beats 100% [Java] Explained
- Author: Shradha1994
- Creation Date: Fri May 15 2020 21:38:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 16 2020 11:41:24 GMT+0800 (Singapore Standard Time)

<p>
The maximum subarray sum in **circular** array is maximum of following 
- Maximum subarray sum in non circular array
- Maximum subarray sum in circular array.

`Example - [1,2,5,-2,-3,5]`
Steps -
- Find the maximum subarray sum using Kadane Algorithm. This is maximum sum for non circular array.
![image](https://assets.leetcode.com/users/shradha1994/image_1589549466.png)
1+2+5 = **8**
- For non circular sum,
Step 1)  find the minimum subarray sum of array.
![image](https://assets.leetcode.com/users/shradha1994/image_1589549546.png)
-2-3 =**-5**
Step 2) Now find the total sum of array = 1 + 2 + 5 -2 - 3 + 5 = 8
Step 3) The max subarray sum for circular array = Total Sum - Minimum subarray Sum
= 8 - (-5) = 8 + 5 =**13**
![image](https://assets.leetcode.com/users/shradha1994/image_1589549744.png)
As illustrated above, substracting minimum subarray sum from total sum gives maximum circular subarray sum.
**Answer = Max ( Non circular max sum + circular max sum )**= max(8,13) = 13

Code -
The trick here is that to find the minimum subarray sum, we invert the sign of all the numbers in original subarray, and find the maximum subarray sum using Kadane algorithm. Then add it with the total sum. (which is similar to 
[total - minimum subarray sum ])
```
class Solution {
    public int maxSubarraySumCircular(int[] A) {
        int nonCircularSum = kadaneMaxSum(A);
        int totalSum = 0;
        for(int i=0;i<A.length;i++){
            totalSum += A[i];
            A[i] = -A[i];
        }
              
        int circularSum = totalSum + kadaneMaxSum(A);
        if(circularSum == 0)
            return nonCircularSum;
        return Math.max(circularSum,nonCircularSum);
    }
    
    int kadaneMaxSum(int[] A){
        int currentSum = A[0];
        int maxSum = A[0];
        for(int i=1;i<A.length;i++){
            if(currentSum < 0)
                currentSum = 0;
            currentSum = A[i] + currentSum;
            maxSum = Math.max(maxSum,currentSum);
        }
        return maxSum;
    }
}
```

*Please feel free to ask any questions in comments. Do upvote if you understood the solution*
</p>


