---
title: "As Far from Land as Possible"
weight: 1024
#id: "as-far-from-land-as-possible"
---
## Description
<div class="description">
<p>Given an N x N <code>grid</code>&nbsp;containing only values <code>0</code> and <code>1</code>, where&nbsp;<code>0</code> represents water&nbsp;and <code>1</code> represents land, find a water cell such that its distance to the nearest land cell is maximized and return the distance.</p>

<p>The distance used in this problem is the <em>Manhattan distance</em>:&nbsp;the distance between two cells <code>(x0, y0)</code> and <code>(x1, y1)</code> is <code>|x0 - x1| + |y0 - y1|</code>.</p>

<p>If no land or water exists in the grid, return <code>-1</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/05/03/1336_ex1.JPG" style="width: 185px; height: 87px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[1,0,1],[0,0,0],[1,0,1]]</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>
The cell (1, 1) is as far as possible from all the land with distance 2.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/05/03/1336_ex2.JPG" style="width: 184px; height: 87px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[[1,0,0],[0,0,0],[0,0,0]]</span>
<strong>Output: </strong><span id="example-output-2">4</span>
<strong>Explanation: </strong>
The cell (2, 2) is as far as possible from all the land with distance 4.
</pre>

<p>&nbsp;</p>

<p><span><strong>Note:</strong></span></p>

<ol>
	<li><span><code>1 &lt;= grid.length == grid[0].length&nbsp;&lt;= 100</code></span></li>
	<li><span><code>grid[i][j]</code>&nbsp;is <code>0</code> or <code>1</code></span></li>
</ol>

</div>

## Tags
- Breadth-first Search (breadth-first-search)
- Graph (graph)

## Companies
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Uipath - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ with picture, DFS and BFS
- Author: votrubac
- Creation Date: Sun Aug 18 2019 12:02:13 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 20 2020 04:35:00 GMT+0800 (Singapore Standard Time)

<p>
# Intution
Normally, we would run breadth-first search from each cell simultaneonly, tracking water cells we visited. Sort of like Dijkastra\u2019s algorithm. However, I wanted to try a depth-first search solution, as it seemed easier to implement at the time.

The DFS solution is accepted but has higher runtime complexity, so I then added the BFS solution to compare.
# DFS Solution
For each \'land\' cell, start DFS and record the distance in \'water\' cells.

If the distance in the \'water\' cell is smaller than the current distance, we stop there. Otherwise, we update the distance to the smaller value and keep going.

So our grid will have the following values:
- ```1``` for land
- ```0``` for water
- ```>=2``` water with the recorded distance

In the end, we scan the grid again and returning the largest value.

In this example, the cells contains distances after DFS is complete for each land cell. In the end, the maximum distance from the land is 3 (4 - 1).
![image](https://assets.leetcode.com/users/votrubac/image_1566199200.png)
```cpp
void dfs(vector<vector<int>>& g, int i, int j, int dist = 1) {
  if (i < 0 || j < 0 || i >= g.size() || j >= g[i].size() || (g[i][j] != 0 && g[i][j] <= dist)) return;
  g[i][j] = dist;
  dfs(g, i - 1, j, dist + 1), dfs(g, i + 1, j, dist + 1), dfs(g, i, j - 1, dist + 1), dfs(g, i, j + 1, dist + 1);
}
int maxDistance(vector<vector<int>>& g, int mx = -1) {
  for (auto i = 0; i < g.size(); ++i)
    for (auto j = 0; j < g[i].size(); ++j)
      if (g[i][j] == 1) {
          g[i][j] = 0;
          dfs(g, i, j);
      }
  for (auto i = 0; i < g.size(); ++i)
    for (auto j = 0; j < g[i].size(); ++j)
      if (g[i][j] > 1) mx = max(mx, g[i][j] - 1);
  return mx;
}
```
## Complexity Analysis
Runtime: O(m * n * n), where m is the number of land cells.
Memory: O(n * n) for the recursion.
# BFS Solution
The solution above is slow, and BFS can help to make sure we process a single cell only once (well, twice in our case to scan for the land first).

Here, we find our land cells and put surrounding water cells in the queue. We mark water cells as visited and continue the expansion from land cells until there are no more water cells left. In the end, the number of steps in DFS is how far can we go from the land.

Using the same example as above, the picture below shows the state of the grid after each step of BFS
![image](https://assets.leetcode.com/users/votrubac/image_1566199213.png)
```cpp
int maxDistance(vector<vector<int>>& g, int steps = 0) {
  queue<pair<int, int>> q, q1;
  for (auto i = 0; i < g.size(); ++i)
    for (auto j = 0; j < g[i].size(); ++j)
      if (g[i][j] == 1)
        q.push({ i - 1, j }), q.push({ i + 1, j }), q.push({ i, j - 1 }), q.push({ i, j + 1 });
  while (!q.empty()) {
    ++steps;
    while (!q.empty()) {
      int i = q.front().first, j = q.front().second;
      q.pop();
      if (i >= 0 && j >= 0 && i < g.size() && j < g[i].size() && g[i][j] == 0) {
        g[i][j] = steps;
        q1.push({ i - 1, j }), q1.push({ i + 1, j }), q1.push({ i, j - 1 }), q1.push({ i, j + 1 });
      }
    }
    swap(q1, q);
  }
  return steps == 1 ? -1 : steps - 1;
}
```
## Complexity Analysis
Runtime: O(n * n). We process an individual cell only once (or twice).
Memory: O(n) for the queue.
</p>


### Python BFS
- Author: li-_-il
- Creation Date: Sun Aug 18 2019 12:01:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 18 2019 12:02:28 GMT+0800 (Singapore Standard Time)

<p>
```
def maxDistance(self, grid: List[List[int]]) -> int:
        m,n = len(grid), len(grid[0])
        q = deque([(i,j) for i in range(m) for j in range(n) if grid[i][j] == 1])    
        if len(q) == m * n or len(q) == 0: return -1
        level = 0
        while q:
            size = len(q)
            for _ in range(size):
                i,j = q.popleft()
                for x,y in [(1,0), (-1, 0), (0, 1), (0, -1)]:
                    xi, yj = x+i, y+j
                    if 0 <= xi < m and 0 <= yj < n and grid[xi][yj] == 0:
                        q.append((xi, yj))
                        grid[xi][yj] = 1
            level += 1
        return level-1
```
</p>


### 7ms DP solution with example, beats 100%
- Author: Mandelbrot
- Creation Date: Thu Nov 07 2019 06:49:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Nov 09 2019 00:00:03 GMT+0800 (Singapore Standard Time)

<p>
The idea is completly the same as 542. 01 Matrix. 
We maintain a dp table, the entries in the dp table represent the **distance to the nearest \'1\' + 1**, why +1? Will explain this in a second. 
We traverse the grid 2 times, first from left up -> bottom right, second bottom right -> left up. 
In the first loop, we update the minimum distance to reach a \'1\' from the current position either keep going left or going upward. Here\'s a small trick, i pick 201 as the max value, cuz per the problem description, the # of rows won\'t exceed 100, so the length of longest path in the matrix will not exceed 200. 
Say, a matrix A, after the first loop, it will become
```
[1, 0, 0]      [1, 2, 3]
[0, 0, 0]  ->  [2, 3, 4]
[0, 0, 1]      [3, 4, 1]
```
please note that this is not the **real distance**

In the second loop, we go from bottom right to up left to update the min distance from another side. At the end, please not that  ```res``` is  not the value we want, if there\'s no \'1\'s in the matrix, all the entry will be set to 201 in such a case, we should return -1 instead of 201; if there are \'1\'s in the matrix, as mentioned at the begining, res is the maximum distance + 1, so we need res-1.

```
[1, 2, 3]    [1, 2, 3]  real distance [0, 1, 2]
[2, 3, 4] -> [2, 3, 2]        ->      [1, 2, 1]
[3, 4, 1]    [3, 2, 1]        -1      [2, 1, 0]
```
the maximum value in the table is 3, this means the answer is 3 - 1 = 2.
time/space: O(nm)/O(1)
code:
```
class Solution {
    public int maxDistance(int[][] grid) {
        int n = grid.length, m = grid[0].length, res = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (grid[i][j] == 1) continue;
                grid[i][j] = 201; //201 here cuz as the despription, the size won\'t exceed 100.
                if (i > 0) grid[i][j] = Math.min(grid[i][j], grid[i-1][j] + 1);
                if (j > 0) grid[i][j] = Math.min(grid[i][j], grid[i][j-1] + 1);
            }
        }
        
        for (int i = n-1; i > -1; i--) {
            for (int j = m-1; j > -1; j--) {
                if (grid[i][j] == 1) continue;
                if (i < n-1) grid[i][j] = Math.min(grid[i][j], grid[i+1][j] + 1);
                if (j < m-1) grid[i][j] = Math.min(grid[i][j], grid[i][j+1] + 1);
                res = Math.max(res, grid[i][j]); //update the maximum
            }
        }
        
        return res == 201 ? -1 : res - 1;
    }
}
```
</p>


