---
title: "Simplify Path"
weight: 71
#id: "simplify-path"
---
## Description
<div class="description">
<p>Given an <strong>absolute path</strong> for a file (Unix-style), simplify it. Or in other words, convert it to the <strong>canonical path</strong>.</p>

<p>In a UNIX-style file system, a period <code>.</code>&nbsp;refers to the current directory. Furthermore, a double period <code>..</code>&nbsp;moves the directory up a level.</p>

<p>Note that the returned canonical path must always begin&nbsp;with a slash <code>/</code>, and there must be only a single slash <code>/</code>&nbsp;between two directory names.&nbsp;The last directory name (if it exists) <b>must not</b>&nbsp;end with a trailing <code>/</code>. Also, the canonical path must be the <strong>shortest</strong> string&nbsp;representing the absolute path.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: &quot;</strong><span id="example-input-1-1">/home/&quot;</span>
<strong>Output: &quot;</strong><span id="example-output-1">/home&quot;
<strong>Explanation:</strong> Note that there is no trailing slash after the last directory name.</span>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: &quot;</strong><span id="example-input-1-1">/../&quot;</span>
<strong>Output: &quot;</strong><span id="example-output-1">/&quot;</span>
<strong>Explanation:</strong> Going one level up from the root directory is a no-op, as the root level is the highest level you can go.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: &quot;</strong><span id="example-input-1-1">/home//foo/&quot;</span>
<strong>Output: &quot;</strong><span id="example-output-1">/home/foo&quot;</span>
<strong>Explanation: </strong>In the canonical path, multiple consecutive slashes are replaced by a single one.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: &quot;</strong><span id="example-input-1-1">/a/./b/../../c/&quot;</span>
<strong>Output: &quot;</strong><span id="example-output-1">/c&quot;</span>
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input: &quot;</strong><span id="example-input-1-1">/a/../../b/../c//.//&quot;</span>
<strong>Output: &quot;</strong><span id="example-output-1">/c&quot;</span>
</pre>

<p><strong>Example 6:</strong></p>

<pre>
<strong>Input: &quot;</strong><span id="example-input-1-1">/a//b////c/d//././/..&quot;</span>
<strong>Output: &quot;</strong><span id="example-output-1">/a/b/c&quot;</span>
</pre>

</div>

## Tags
- String (string)
- Stack (stack)

## Companies
- Facebook - 10 (taggedByAdmin: true)
- Amazon - 5 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)
- Oracle - 2 (taggedByAdmin: false)
- Docusign - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Qualtrics - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

#### Approach: Using Stacks

**Intuition**

This is a direct implementation of one of the most common commands used (in one form of the other) on all the famous operating systems. For e.g. this directly translates to a part of the implementation of the `cd` command functionality in the Unix OS. It basically helps us to change directory. Obviously, there's much more to it than simply figuring out the smallest path of the final directory where the user wants to go. There is kernel level implementation involved that actually uses the underlying file system to `change` the directory you are in to the one where you want to go. You might argue that nobody would go crazy on the command line and want to run something like:

<pre>
cd /a/b/c/.././././//d</pre>

However, our code needs to be able to handle the different scenarios and all the special characters like `.`, `..`, and `/`. All these weird scenarios will be taken care of by a couple of checks here and there. The main idea for solving this problem would remain the same. The heading of this approach mentions the data structure that we are going to use. However, we are going to work our way through the problem so as to understand as to why a `stack` fits in here. Let's look at a tree-ish representation of a simple directory path.

<center>
<img src="../Figures/71/img1.png"/>
</center>

Now suppose that to a path like `/a/b/c`, we add another component like `/a/b/c/..`. Now, this is interesting because the `..` is no longer a sub-directory name. It has a special meaning and an indication to the operating system to move up one level in the directory structure thus transforming the overall path to just `/a/b`. It's as if we `popped out` the subdirectory `c` from the overall path. That's the core idea of this problem. If you think about it, the only actionable special character is `..`. The single dot is kind of a no-op because it simply means the current directory. So nothing changes in the overall path as such. Now that we have a general idea about how this problem can be potentially solved using the stack, let's get right into the algorithm and discuss the solution.

**Algorithm**

1. Initialize a stack, `S` that we will be using for our implementation.
2. Split the input string using `/` as the delimiter. This step is really important because no matter what, the given input is a `valid` path and we simply have to shorten it. So, that means that whatever we have between two `/` characters is either a directory name or a special character and we have to process them accordingly.
3. Once we are done splitting the input path, we will process one component at a time.
4. If the current component is a `.` or an empty string, we will do nothing and simply continue. Well if you think about it, the split string array for the string `/a//b` would be `[a,,b]`. yes, that's an empty string in between `a` and `b`. Again, from the perspective of the overall path, it doesn't mean anything.
5. If we encounter a double-dot `..`, we have to do some processing. This simply means go one level up in the current directory path. So, we will pop an entry from our stack if it's not empty.
6. Finally, if the component we are processing right now is not one of the special characters, then we will simply add it to our stack because it's a legitimate directory name.
7. Once we are done processing all the components, we simply have to connect all the directory names in our stack together using `/` as the delimiter and we will have our shortest path that leads us to the same directory as the one provided as an input.

<iframe src="https://leetcode.com/playground/aCtEJqds/shared" frameBorder="0" width="100%" height="500" name="aCtEJqds"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ if there are $$N$$ characters in the original path. First, we spend $$O(N)$$ trying to split the input path into components and then we process each component one by one which is again an $$O(N)$$ operation. We can get rid of the splitting part and just string together the characters and form directory names etc. However, that would be too complicated and not worth depicting in the implementation. The main idea of this algorithm is to use a stack. How you decide to process the input string is a personal choice.
* Space Complexity: $$O(N)$$. Actually, it's $$2N$$ because we have the array that contains the split components and then we have the stack.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 10-lines solution with stack
- Author: shpolsky
- Creation Date: Wed Jan 21 2015 08:13:29 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 11:00:58 GMT+0800 (Singapore Standard Time)

<p>
Hi guys!

The main idea is to push to the stack every valid file name (not in {"",".",".."}), popping only if there's smth to pop and we met "..". I don't feel like the code below needs any additional comments.

    public String simplifyPath(String path) {
        Deque<String> stack = new LinkedList<>();
        Set<String> skip = new HashSet<>(Arrays.asList("..",".",""));
        for (String dir : path.split("/")) {
            if (dir.equals("..") && !stack.isEmpty()) stack.pop();
            else if (!skip.contains(dir)) stack.push(dir);
        }
        String res = "";
        for (String dir : stack) res = "/" + dir + res;
        return res.isEmpty() ? "/" : res;
    }

Hope it helps!
</p>


### C++ 10-lines solution
- Author: monaziyi
- Creation Date: Tue Feb 10 2015 18:36:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 02:32:03 GMT+0800 (Singapore Standard Time)

<p>
C++ also have  *getline* which acts like Java's *split*. I guess the code can comment itself.

    string simplifyPath(string path) {
        string res, tmp;
        vector<string> stk;
        stringstream ss(path);
        while(getline(ss,tmp,'/')) {
            if (tmp == "" or tmp == ".") continue;
            if (tmp == ".." and !stk.empty()) stk.pop_back();
            else if (tmp != "..") stk.push_back(tmp);
        }
        for(auto str : stk) res += "/"+str;
        return res.empty() ? "/" : res;
    }
</p>


### Can someone please explain what does simplify means in this context?
- Author: rabeeh
- Creation Date: Mon Aug 03 2015 20:50:54 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 15 2018 10:33:55 GMT+0800 (Singapore Standard Time)

<p>
Hi, I need more explanation about the question, how does the question define simplify here?
</p>


