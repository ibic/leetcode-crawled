---
title: "Count Square Submatrices with All Ones"
weight: 1211
#id: "count-square-submatrices-with-all-ones"
---
## Description
<div class="description">
<p>Given a <code>m * n</code> matrix of ones and zeros, return how many <strong>square</strong> submatrices have all ones.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> matrix =
[
&nbsp; [0,1,1,1],
&nbsp; [1,1,1,1],
&nbsp; [0,1,1,1]
]
<strong>Output:</strong> 15
<strong>Explanation:</strong> 
There are <strong>10</strong> squares of side 1.
There are <strong>4</strong> squares of side 2.
There is  <strong>1</strong> square of side 3.
Total number of squares = 10 + 4 + 1 = <strong>15</strong>.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> matrix = 
[
  [1,0,1],
  [1,1,0],
  [1,1,0]
]
<strong>Output:</strong> 7
<strong>Explanation:</strong> 
There are <b>6</b> squares of side 1.  
There is <strong>1</strong> square of side 2. 
Total number of squares = 6 + 1 = <b>7</b>.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length&nbsp;&lt;= 300</code></li>
	<li><code>1 &lt;= arr[0].length&nbsp;&lt;= 300</code></li>
	<li><code>0 &lt;= arr[i][j] &lt;= 1</code></li>
</ul>

</div>

## Tags
- Array (array)
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] DP Solution + Thinking Process Diagrams (O(mn) runtime; O(1) space)
- Author: arkaung
- Creation Date: Thu May 21 2020 16:53:02 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 21 2020 17:03:03 GMT+0800 (Singapore Standard Time)

<p>
The idea is to scan each cell in the `matrix` to update the placeholder `result` variable with the number of squares that can be formed from the currently looking cell (when it is the bottom right corner cell of the **any possible** square).

The main workhorse of this Dynamic Programming approach is `Line 17`. Let\'s try to understand that:

![image](https://assets.leetcode.com/users/arkaung/image_1590051166.png)
![image](https://assets.leetcode.com/users/arkaung/image_1590051442.png)

![image](https://assets.leetcode.com/users/arkaung/image_1590051171.png)

Since we don\'t create any additional grid to hold our results, and if we don\'t account for the original input matrix, the space complexity is `O(1)` and runtime complexity is `O(mn)` where m and n are rows and cols for the matrix.

**Python**

``` Python
1. class Solution:
2.     def countSquares(self, matrix: List[List[int]]) -> int:
3.         if matrix is None or len(matrix) == 0:
4.             return 0
5.         
6.         rows = len(matrix)
7.         cols = len(matrix[0])
8.         
9.         result = 0
10.         
11.         for r in range(rows):
12.             for c in range(cols):
13.                 if matrix[r][c] == 1:   
14.                     if r == 0 or c == 0: # Cases with first row or first col
15.                         result += 1      # The 1 cells are square on its own               
16.                     else:                # Other cells
17.                         cell_val = min(matrix[r-1][c-1], matrix[r][c-1], matrix[r-1][c]) + matrix[r][c]
18.                         result += cell_val
19.                         matrix[r][c] = cell_val #**memoize the updated result**
20.         return result  
```
</p>


### [Java/C++/Python] DP solution
- Author: lee215
- Creation Date: Sun Dec 01 2019 12:02:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 23:16:51 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
`dp[i][j]` means the size of biggest square with `A[i][j]` as bottom-right corner.
`dp[i][j]` also means the number of squares with `A[i][j]` as bottom-right corner.

If `A[i][j] == 0`, no possible square.
If `A[i][j] == 1`,
we compare the size of square `dp[i-1][j-1]`, `dp[i-1][j]` and `dp[i][j-1]`.
`min(dp[i-1][j-1], dp[i-1][j], dp[i][j-1]) + 1` is the maximum size of square that we can find.
<br>

## **Complexity**
Time `O(MN)`
Space `O(1)`
<br>

**Java:**
```java
    public int countSquares(int[][] A) {
        int res = 0;
        for (int i = 0; i < A.length; ++i) {
            for (int j = 0; j < A[0].length; ++j) {
                if (A[i][j] > 0 && i > 0 && j > 0) {
                    A[i][j] = Math.min(A[i - 1][j - 1], Math.min(A[i - 1][j], A[i][j - 1])) + 1;
                }
                res += A[i][j];
            }
        }
        return res;
    }
```

**C++:**
```cpp
    int countSquares(vector<vector<int>>& A) {
        int res = 0;
        for (int i = 0; i < A.size(); ++i)
            for (int j = 0; j < A[0].size(); res += A[i][j++])
                if (A[i][j] && i && j)
                    A[i][j] += min({A[i - 1][j - 1], A[i - 1][j], A[i][j - 1]});
        return res;
    }
```

**Python:**
```python
    def countSquares(self, A):
        for i in xrange(1, len(A)):
            for j in xrange(1, len(A[0])):
                A[i][j] *= min(A[i - 1][j], A[i][j - 1], A[i - 1][j - 1]) + 1
        return sum(map(sum, A))
```

</p>


### DP with figure explanation
- Author: migeater
- Creation Date: Sun Dec 01 2019 16:26:11 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 25 2020 03:00:21 GMT+0800 (Singapore Standard Time)

<p>
![image](https://assets.leetcode.com/users/migeater/image_1575196646.png)

## ## Explanation

 `dp[i][j]` represent edge length of the biggest square whose lower right corner element is `matrix[i][j]`. Also there are `dp[i][j]` squares whose lower right corner element are `matrix[i][j]`. The answer of count-square-submatrices-with-all-ones is sum of all `dp[i][j]`.

As Figure, **the square edge length** ( whose lower right corner element is `matrix[i][j]` ) is not greater than  `the minimum of three arrows + 1`. 

Fortunately it can be equal to when `matrix[i][j]==1`. On this condition `dp[i][j]=the minimum of three arrows + 1`;

And when `matrix[i][j]==0` , `dp[i][j]=0`.

So

```py
if matrix[i][j]==1 : 
    if i!=0 and j!=0: 
        dp[i][j] = min(dp[i-1][j], dp[i][j-1], dp[i-1][j-1])+1
    else: 
        dp[i][j] = 0 + 1
else:
    dp[i][j] = 0
```

In the 3rd line, each `dp[i][j]` is reused for 3 times.
If you think there are too many coincidences, please read next paragraph.
Now we can write a top-down recursive algorithm:

### ### A top-down DP

time O(m*n), space O(m*n)

```py
import functools
class Solution(object):
    def countSquares(self, matrix):
        m, n = len(matrix), len(matrix[0])

        @functools.lru_cache(None) # use this for memory search
        def dp(i,j):
            if matrix[i][j]==1 : 
                if i!=0 and j!=0:
                    return min(dp(i-1,j), dp(i,j-1), dp(i-1,j-1))+1
                else:
                    return 0+1
            else:
                return 0
        
        return sum(dp(i,j) for i in range(m) for j in range(n))
```

After wrtie this we know the call order of the dp function, then we can change it to bottom-up, make sure each dp(i,j) is calculated before dp(i,j+1) and dp(i+1,j) and dp(i+1,j+1). That\'s the classic iteration order: from left to right and from up to down.

### ### A bottom-up DP

time O(m*n), space O(m*n)

Here in order to easily process border condition, use `dp[i+1][j+1]` save edge length of the biggest square whose lower right corner element is `matrix[i][j]`.

```py
class Solution(object):
    def countSquares(self, matrix):
        m, n = len(matrix), len(matrix[0])
        dp = [[0] * (n+1) for _ in range(m+1)] # new int[m+1][n+1];
        ans = 0
        for i in range(m):
            for j in range(n):
                if matrix[i][j]:
                    dp[i+1][j+1] = min(dp[i][j+1], dp[i+1][j], dp[i][j]) + 1
                    ans += dp[i+1][j+1]
        return ans
```
###  ### bottom-up DP with space compression

time O(mn), space O(n)

```py
class Solution(object):
    def countSquares(self, matrix):
        m, n = len(matrix), len(matrix[0])
        dp = [0] * (n+1)
        ans = 0
        
        dp_i_j = 0 # dp[0][0]
        for i in range(m):
            for j in range(n):
                dp[j+1] # by this line it\'s dp[i][j+1]
                dp[j] # by this line it\'s dp[i+1][j] 
                dp_i_j_nxt = dp[j+1]
                
                if matrix[i][j]:
                    # dp[i+1][j+1] = min(dp[i][j+1], dp[i+1][j], dp[i][j]) + 1
                    dp[j+1]        = min(dp[j+1]   , dp[j]     ,   dp_i_j) + 1
                    ans += dp[j+1]
                else:
                    dp[j+1]=0
                    
                dp_i_j = dp_i_j_nxt
        return ans
```

You can save 1 dimension use this trick. In fact, you can use space O(min(m,n)) by transpose the matrix ( in code just change iteration direction ), or even you can save dp value in `matrix` array. Example: the most upvoted post by @lee215.

## ## possible process of thinking

Since DP means optimal substructure which is closed to mathematical induction, in many dp problems, there can be some n represent array size, string length, tree height, etc.
Then you consider the situation when n=1, n=2, n=3, n=4, n=5, ..., this procedure may cost lots of time until you found many processes are repeated and can be cached. You can cache whatever you want. Remember don\'t directly define what your dp array cache and solve the problem by your definition. You solve the problem then you define what dp cache.
In this way, you found the so-called [Bellman equation](https://en.wikipedia.org/wiki/Bellman_equation).
Problems which doesn\'t have an obvious n, or doesn\'t have such n (such as each dp used to save a statement), or has two or more n-s, are often hard to solve.

In this problem, we have two variables n and m, if you know multidimensional mathematical induction ( I have learned it in my calculus course in university ), such as [this Theorem 5.2. (Two dimensional Induction, Version 2)](https://cseweb.ucsd.edu/classes/sp14/cse20-a/InductionNotes.pdf), we can try to solve it use a regular method( I often use this technique stimulate situation in one dimension, just try).

We use P(a,b) denote the subproblem that how many squares are there in `matrix[:a][:b]`, P(a,b) also represent the answer of problem P(a,b).
use D(a,b) denote the problem that how many squares whose lower right corner elements are `matrix[a-1][b-1]`, D(a,b) also represent the answer of problem D(a,b). (Denote is not part of my regular method)

Figure out whether P(a+1,b+1) can be solved by P(a,b+1) and P(a+1,b), then you will find `P(a+1,b+1) = P(a,b+1)+P(a+1,b)-P(a,b)+D(a+1,b+1) `(Inclusion\u2013exclusion principle)

Then we turn to solve D(a,b) you will find P(a,b) can be easily solved if you solved D(a,b). Also Figure out whether D(a+1,b+1) can be solved by D(a,b+1) and D(a+1,b), you will find D(a,b) also represent the edge length of biggest square whose lower right corner element is `matrix[a-1][b-1]`, and 
```py
if matrix[a][b]==0:
    D(a+1,b+1) = 0
elif matrix[a][b]==1:
    # as figure it\'s minimum arrow length
	myarrow = min(D(a,b+1) + D(a+1,b))
    if matrix[a-myarrow][b-myarrow] == 1:
		D(a+1,b+1) = myarrow+1
    else:
	    D(a+1,b+1) = myarrow
```
It\'s okay that you use `D(a,b)` instead of `matrix[a-myarrow][b-myarrow]` just like code in previous paragraph.
In this problem this method happen to success, if this method fail(such as knapsack problem), we can try P(1,1) and P(1,2), P(1,3), ..., P(2,1) ... ,

In conclusion it\'s possible that you solve this problem use this method in 20 mins. 

## ## P.S. 

The same problem: https://leetcode.com/problems/maximal-square/
Similar: https://leetcode.com/problems/minimum-path-sum/

More about top-down and bottom-up 
https://leetcode.com/explore/learn/card/recursion-i/
https://leetcode.com/explore/learn/card/recursion-ii/

</p>


