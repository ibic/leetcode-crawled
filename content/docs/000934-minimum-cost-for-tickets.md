---
title: "Minimum Cost For Tickets"
weight: 934
#id: "minimum-cost-for-tickets"
---
## Description
<div class="description">
<p>In a country popular for train travel, you&nbsp;have planned some train travelling one year in advance.&nbsp; The days of the year that you will travel is given as an array <code>days</code>.&nbsp; Each day is an integer from <code>1</code> to <code>365</code>.</p>

<p>Train tickets are sold in 3 different ways:</p>

<ul>
	<li>a 1-day pass is sold for <code>costs[0]</code> dollars;</li>
	<li>a 7-day pass is sold for <code>costs[1]</code> dollars;</li>
	<li>a 30-day pass is sold for <code>costs[2]</code> dollars.</li>
</ul>

<p>The passes allow that many days of consecutive travel.&nbsp; For example, if we get a 7-day pass on day 2, then we can travel for 7 days: day 2, 3, 4, 5, 6, 7, and 8.</p>

<p>Return the minimum number of dollars you need to travel every day in the given list of <code>days</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>days = <span id="example-input-1-1">[1,4,6,7,8,20]</span>, costs = <span id="example-input-1-2">[2,7,15]</span>
<strong>Output: </strong><span id="example-output-1">11</span>
<strong>Explanation: </strong>
For example, here is one way to buy passes that lets you travel your travel plan:
On day 1, you bought a 1-day pass for costs[0] = $2, which covered day 1.
On day 3, you bought a 7-day pass for costs[1] = $7, which covered days 3, 4, ..., 9.
On day 20, you bought a 1-day pass for costs[0] = $2, which covered day 20.
In total you spent $11 and covered all the days of your travel.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>days = <span id="example-input-2-1">[1,2,3,4,5,6,7,8,9,10,30,31]</span>, costs = <span id="example-input-2-2">[2,7,15]</span>
<strong>Output: </strong><span id="example-output-2">17</span>
<strong>Explanation: </strong>
For example, here is one way to buy passes that lets you travel your travel plan:
On day 1, you bought a 30-day pass for costs[2] = $15 which covered days 1, 2, ..., 30.
On day 31, you bought a 1-day pass for costs[0] = $2 which covered day 31.
In total you spent $17 and covered all the days of your travel.
</pre>

<p>&nbsp;</p>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= days.length &lt;= 365</code></li>
	<li><code>1 &lt;= days[i] &lt;= 365</code></li>
	<li><code>days</code> is in strictly increasing order.</li>
	<li><code>costs.length == 3</code></li>
	<li><code>1 &lt;= costs[i] &lt;= 1000</code></li>
</ol>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Facebook - 5 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Grab - 2 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Dynamic Programming (Day Variant)

**Intuition and Algorithm**

For each day, if you don't have to travel today, then it's strictly better to wait to buy a pass.  If you have to travel today, you have up to 3 choices: you must buy either a 1-day, 7-day, or 30-day pass.

We can express those choices as a recursion and use dynamic programming.  Let's say `dp(i)` is the cost to fulfill your travel plan from day `i` to the end of the plan.  Then, if you have to travel today, your cost is:

$$
\text{dp}(i) = \min(\text{dp}(i+1) + \text{costs}[0], \text{dp}(i+7) + \text{costs}[1], \text{dp}(i+30) + \text{costs}[2])
$$

<iframe src="https://leetcode.com/playground/iPJuw4yh/shared" frameBorder="0" width="100%" height="500" name="iPJuw4yh"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(W)$$, where $$W = 365$$ is the maximum numbered day in your travel plan.

* Space Complexity:  $$O(W)$$.
<br />
<br />


---
#### Approach 2: Dynamic Programming (Window Variant)

**Intuition and Algorithm**

As in *Approach 1*, we only need to buy a travel pass on a day we intend to travel.

Now, let `dp(i)` be the cost to travel from day `days[i]` to the end of the plan.  If say, `j1` is the largest index such that `days[j1] < days[i] + 1`, `j7` is the largest index such that `days[j7] < days[i] + 7`, and `j30` is the largest index such that `days[j30] < days[i] + 30`, then we have:

$$
\text{dp}(i) = \min(\text{dp}(j1) + \text{costs}[0], \text{dp}(j7) + \text{costs}[1], \text{dp}(j30) + \text{costs}[2])
$$

<iframe src="https://leetcode.com/playground/MorsiZna/shared" frameBorder="0" width="100%" height="500" name="MorsiZna"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of unique days in your travel plan.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Two DP solutions with pictures
- Author: votrubac
- Creation Date: Sun Jan 27 2019 12:02:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 27 2019 12:02:08 GMT+0800 (Singapore Standard Time)

<p>
The higher is the bar, the more you\'re expected to use dynamic programming (DP) during an interview. This technique requires a lot of practice to grasp; if you\'ve mastered the recursion, DP is the next level.

This problem appeared on LeetCode [weekly contest #121](https://leetcode.com/contest/weekly-contest-121), and it\'s a good problem to practice the DP thinking.
<!--
# Problem Description
In a country popular for train travel, you have planned some train travelling one year in advance. The days of the year that you will travel is given as an array days. Each day is an integer from 1 to 365.

Train tickets are sold in 3 different ways:
- 1-day pass is sold for costs[0] dollars;
- 7-day pass is sold for costs[1] dollars;
- 30-day pass is sold for costs[2] dollars.

The passes allow that many days of consecutive travel. For example, if we get a 7-day pass on day 2, then we can travel for 7 days: day 2, 3, 4, 5, 6, 7, and 8.

Return the minimum number of dollars you need to travel every day in the given list of days.
# Coding Practice
Try solving this problem before moving on to the solutions. It is available on LeetCode Online Judge ([983. Minimum Cost For Tickets](https://leetcode.com/problems/minimum-cost-for-tickets/)). Also, as you read through a solution, try implementing it yourself.

LeetCode is my favorite destinations for algorithmic problems. It has 978 problems and counting, with test cases to validate the correctness, as well as computational and memory complexity. There is also a large community discussing different approaches, tips and tricks.
-->
# Intuition
For each travel day, we can buy a one-day ticket, or use 7-day or 30-day pass as if we would have purchased it 7 or 30 days ago. We need to track rolling costs for at least 30 days back, and use them to pick the cheapest option for the next travel day.

Here, we can use two approaches: track cost for all calendar days, or process only travel days. The first approach is simpler to implement, but it\'s slower. Since the problem is limited to one calendar year, it does not make much of a difference; for a generalized problem I would recommend the second approach.
# 1. Track calendar days
We track the minimum cost for all calendar days in ```dp```. For non-travel days, the cost stays the same as for the previous day. For travel days, it\'s a minimum of yesterday\'s cost plus single-day ticket, or cost for 8 days ago plus 7-day pass, or cost 31 days ago plus 30-day pass.

![image](https://assets.leetcode.com/users/votrubac/image_1548621855.png)
```
int mincostTickets(vector<int>& days, vector<int>& costs) {
  unordered_set<int> travel(begin(days), end(days));
  int dp[366] = {};
  for (int i = 1; i < 366; ++i) {
    if (travel.find(i) == travel.end()) dp[i] = dp[i - 1];
    else dp[i] = min({ dp[i - 1] + costs[0], dp[max(0, i - 7)] + costs[1], dp[max(0, i - 30)] + costs[2]});
  }
  return dp[365];
}
```
# Optimizations
In the previous solution, we store cost for all calendar days. However, since we only look 30 days back, we can just store the cost for last 30 days in a rolling array.

In addition, we can only look at calendar days within our first and last travel dates, as [@zengxinhai](https://leetcode.com/zengxinhai) suggested.
```
int mincostTickets(vector<int>& days, vector<int>& costs) {
  unordered_set<int> travel(begin(days), end(days));
  int dp[30] = {};
  for (int i = days.front(); i <= days.back(); ++i) {
    if (travel.find(i) == travel.end()) dp[i % 30] = dp[(i - 1) % 30];
    else dp[i % 30] = min({ dp[(i - 1) % 30] + costs[0],
        dp[max(0, i - 7) % 30] + costs[1], dp[max(0, i - 30) % 30] + costs[2] });
  }
  return dp[days.back() % 30];
}
```
# Complexity analysis
- Time Complexity: O(N), where N is the number of calendar days.
- Space Complexity: O(N) or O(31) for the optimized solution. Stricter, it\'s a maximum duration among all pass types.
# 2. Track travel days
We track the minimum ```cost``` for each travel day. We process only travel days and store {day, cost} for 7-and 30-day passes in the ```last7``` and ```last30``` queues. After a pass \'expires\', we remove it from the queue. This way, our queues only contains travel days for the last 7 and 30 days, and the cheapest pass prices are in the front of the queues.

![image](https://assets.leetcode.com/users/votrubac/image_1548617861.png)
```
int mincostTickets(vector<int>& days, vector<int>& costs, int cost = 0) {
  queue<pair<int, int>> last7, last30;
  for (auto d : days) {
    while (!last7.empty() && last7.front().first + 7 <= d) last7.pop();
    while (!last30.empty() && last30.front().first + 30 <= d) last30.pop();
    last7.push({ d, cost + costs[1] });
    last30.push({ d, cost + costs[2] });
    cost = min({ cost + costs[0], last7.front().second, last30.front().second });
  }
  return cost;
}
```
# Complexity analysis
- Time Complexity: O(n), where n is the number of travel days.
- Space Complexity: O(38). Stricter, it\'s a sum of duration for all pass types (1 + 7 + 30 in our case).
</p>


### explanation from someone who took 2 hours to solve
- Author: RohanPrakash
- Creation Date: Thu May 14 2020 08:58:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 29 2020 20:14:31 GMT+0800 (Singapore Standard Time)

<p>
**before starting with DP one must master the art of recursion**
it took me 2 hours to solve this and 1 hours wo write this post , hopefully you will understand the question. and how we get the solution

**step1 : understand the problem**
here we are given days and cost of passes for 1 day pass, week pass, month pass, "pass means , once you buy it is valid for that some period of time which means free travel during that period"

**step 2: logic building and deriving recurence relation**

**1st sub step : asking questions**
when we buy pass what should we think ? 
we should think like, what pass should i buy today ? 
should i buy pass of one day , week or month pass ? 

**2nd sub step : Ariving at recurence relation**
if i buy one month pass , then i would be able to travel for next 30 days for free, then my cost will be 30 day pass cost + cost for remaining days after 30 day
if i buy one week pass , then i would be able to travel for next 7 days for free, then my cost will be 7 day pass cost + cost for remaining days after 30 day
if i buy 1 day pass ....... ("you get it");

so we can skip to next day OR , next week OR next month ,
so cost for i would total pay today is going to be ? ... ? 
```
# a = cost[one day pass] + cost of next day
# b = cost[ week pass ] + cost of next day after week
# c = cost[ month pass ] + cost of next day after month
```

ie we can skip 1, 7, or 30 days ;
cost = min( a , b , c )

here is 1st solution using recursion ( TLE )
**step 3: code recursive solution**
```
 int sub(vector<int> &day, vector<int> &cost, int si)		// si denotes starting index
    {
        int n = day.size();
        if(si>=n)   return 0;
        
        int cost_day = cost[0] + sub(day , cost , si+1);
        
        int i;
        for(i = si ; i<n and day[i]<day[si]+7 ; i++);   //skip till ith day is curr_day+7 as we are buying week pass
        int cost_week = cost[1] + sub(day, cost, i);
        
        for(i = si ; i<n and day[i]<day[si]+30 ; i++);   //skip till ith day is curr_day+30 as we are buying month pass
        int cost_month = cost[2] + sub(day, cost, i);      
        
        return min({cost_day, cost_week , cost_month  });
    }
    
    int mincostTickets(vector<int>& days, vector<int>& costs) {        
        return sub(days,costs,0);
    }
```
Now you know the sub problems so we can convert OUR recursion solution to top-down-memoization , ie we store our answers at each step before returning, then if we already knows the answer , we can return right back withour re calculating;

**step4 : memoization-top-down**
```
vector<int> dp;
    int sub(vector<int> &day, vector<int> &cost, int si)    // subproblem
    {
        int n = day.size();
        if(si>=n)   return 0;
        
        if(dp[si])  return dp[si];      // ie we aready knows the answer so no need to re calculate , return answer.
        
        int cost_day = cost[0] + sub(day , cost , si+1);
        
        int i;
        for(i = si ; i<n and day[i]<day[si]+7 ; i++);
        int cost_week = cost[1] + sub(day, cost, i);
        
        for(i = si ; i<n and day[i]<day[si]+30 ; i++);
        int cost_month = cost[2] + sub(day, cost, i);     
        
        dp[si] = min({cost_day, cost_week , cost_month  });   // we store answer for future references 
        
        return dp[si];
    } 
    int mincostTickets(vector<int>& days, vector<int>& costs) {        
        dp.resize(367);
        return sub(days,costs,0);    
    }
```

so you see , once we get to the recursive solution we converted it into DP top-dow using 3-4 lines.

**step 5 : now you know the base casses , you know the sub problems so try coming up with bottom up solution yourself** 

```
int mincostTickets(vector<int>& DAYS, vector<int>& cost) {
        
        unordered_set<int> days(DAYS.begin(),DAYS.end());
        vector<int> dp(367);
        
        for(int i=1 ; i<366 ; i++)
        {
            dp[i] = dp[i-1];
            if(days.find(i) != days.end())
            {
			// cost of ith day , decision of ith day will be
                dp[i] = min({ dp[i - 1] + cost[0],   // 1st case
                             dp[max(0, i - 7)] + cost[1],  // 2nd case
                             dp[max(0, i - 30)] + cost[2]});  // 3rd case
            }
        }
        return dp[365];
    }
```

**thanks for reading**
**previous tutorial on DP** 
https://leetcode.com/problems/minimum-ascii-delete-sum-for-two-strings/discuss/642422/Lets-solve-it-together-%3A-step-by-step

https://leetcode.com/problems/dungeon-game/discuss/745340/post-Dedicated-to-beginners-of-DP
https://leetcode.com/problems/ones-and-zeroes/discuss/814077/Dedicated-to-Beginners
</p>


### Java DP Solution with explanation O(n)
- Author: Poorvank
- Creation Date: Sun Jan 27 2019 12:03:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 27 2019 12:03:55 GMT+0800 (Singapore Standard Time)

<p>
Let minCost(i) denote the minimum cost that will be payed for all trips on days 1 to day 365.
The desired answer is then minCost(365).

Calculation minCost(i):

If no trip on day i, then minCost(i) = minCost(i-1).
 minCost(i)=0 for all i \u2264 0.
Otherwise:
If a 1-day pass on day i. In this case,  minCost(i) =  minCost(i) + costs[0].
If a 7-day pass ending on day i. then : In this case, minCost(i) = min(minCost(i \u2212 7), minCost(i \u2212 6), \u2026, minCost(i \u2212 1)) + costs[1].
But since since minCost is increasing (adding a day never reduces the minCost) hence:
 minCost(i) = minCost(i \u2212 7) + costs[2]

Same case for 30-day pass also.

```
public int mincostTickets(int[] days, int[] costs) {
        boolean[] dayIncluded = new boolean[366];
        for (int day : days) {
            dayIncluded[day] = true;
        }
        int[] minCost = new int[366];
        minCost[0] = 0;
        for (int day = 1; day <= 365; ++day) {
            if (!dayIncluded[day]) {
                minCost[day] = minCost[day-1];
                continue;
            }
            int min;
            min = minCost[day-1] + costs[0];
            min =Math.min(min, minCost[Math.max(0, day-7)] + costs[1]);
            min = Math.min(min, minCost[Math.max(0, day-30)] + costs[2]);
            minCost[day] = min;
        }

        return minCost[365];

    }
```
</p>


