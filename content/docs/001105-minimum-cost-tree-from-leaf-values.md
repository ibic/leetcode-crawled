---
title: "Minimum Cost Tree From Leaf Values"
weight: 1105
#id: "minimum-cost-tree-from-leaf-values"
---
## Description
<div class="description">
<p>Given an array <code>arr</code> of positive integers, consider all binary trees such that:</p>

<ul>
	<li>Each node has either 0 or 2 children;</li>
	<li>The values of <code>arr</code> correspond to the values of each&nbsp;<strong>leaf</strong> in an in-order traversal of the tree.&nbsp; <em>(Recall that a node is a leaf if and only if it has 0 children.)</em></li>
	<li>The value&nbsp;of each non-leaf node is equal to the product of the largest leaf value in its left and right subtree respectively.</li>
</ul>

<p>Among all possible binary trees considered,&nbsp;return the smallest possible sum of the values of each non-leaf node.&nbsp; It is guaranteed this sum fits into a 32-bit integer.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [6,2,4]
<strong>Output:</strong> 32
<strong>Explanation:</strong>
There are two possible trees.  The first has non-leaf node sum 36, and the second has non-leaf node sum 32.

    24            24
   /  \          /  \
  12   4        6    8
 /  \               / \
6    2             2   4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= arr.length &lt;= 40</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 15</code></li>
	<li>It is guaranteed that the answer fits into a 32-bit signed integer (ie.&nbsp;it is less than <code>2^31</code>).</li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Stack (stack)
- Tree (tree)

## Companies
- Amazon - 5 (taggedByAdmin: false)
- Mathworks - 39 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### One Pass, O(N) Time and Space
- Author: lee215
- Creation Date: Sun Jul 21 2019 12:01:16 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 03 2020 15:18:50 GMT+0800 (Singapore Standard Time)

<p>
# DP Solution
Find the cost for the interval `[i,j]`.
To build up the interval `[i,j]`,
we need to split it into left subtree and sub tree,
`dp[i, j] = dp[i, k] + dp[k + 1, j] + max(A[i, k]) * max(A[k + 1, j])`

If you don\'t understand dp solution,
I won\'t explain it more and you won\'t find the answer here.
Take your time,
read any other <Easy DP> solutions,
and come back at your own will.

If you got it, continue to read.

# DP Complexity
Second question after this dp solution,
**what\'s the complexity?**
`N^2` states and `O(N)` to find each.
So this solution is `O(N^3)` time and `O(N^2)` space.

You thought it\'s fine.
After several nested for loop, you got a happy green accepted.
You smiled and released a sigh as a winner.

What a great practice for DP skill!
Then you noticed it\'s medium.
That\'s it, just a standard medium problem of dp.
Nothing can stop you. Even dp problem.
<br>

# True story
So you didn\'t **Read** and **Upvote** this post.
(upvote is a good mark of having read)
One day, you meet exactly the same solution during an interview.
Your heart welled over with joy,
and you bring up your solution with confidence.

One week later, you receive an email.
The second paragraph starts with a key word "Unfortunately".

**What the heck!?**
You solved the interview problem perfectly,
but the company didn\'t appreciate your talent.
What\'s more on earth did they want?
**WHY?**
<br>

# **Why**
Here is the reason.
This is not a dp problem at all.

Because dp solution test all ways to build up the tree,
including many unnecessay tries.
Honestly speaking, it\'s kinda of brute force.
Yes, brute force testing, with memorization.
<br>

# **Intuition**
Let\'s review the problem again.

When we build a node in the tree, we compared the two numbers `a` and `b`.
In this process,
the smaller one is removed and we won\'t use it anymore,
and the bigger one actually stays.

The problem can translated as following:
Given an array `A`, choose two neighbors in the array `a` and `b`,
we can remove the smaller one `min(a,b)` and the cost is `a * b`.
What is the minimum cost to remove the whole array until only one left?

To remove a number `a`, it needs a cost `a * b`, where `b >= a`.
So `a` has to be removed by a bigger number.
We want minimize this cost, so we need to minimize `b`.

`b` has two candidates, the first bigger number on the left,
the first bigger number on the right.

The cost to remove `a` is `a * min(left, right)`.
<br>

# **Solution 1**
With the intuition above in mind,
the explanation is short to go.

We remove the element form the smallest to bigger.
We check the `min(left, right)`,
For each element `a`, `cost = min(left, right) * a`

Time `O(N^2)`
Space `O(N)`

**Python**
```py
    def mctFromLeafValues(self, A):
        res = 0
        while len(A) > 1:
            i = A.index(min(A))
            res += min(A[i - 1:i] + A[i + 1:i + 2]) * A.pop(i)
        return res
```
<br>

# **Solution 2: Stack Soluton**
we decompose a hard problem into reasonable easy one:
Just find the next greater element in the array, on the left and one right.
Refer to the problem [503. Next Greater Element II](https://leetcode.com/problems/next-greater-element-ii/discuss/98270/JavaC++Python-Loop-Twice)
<br>

Time `O(N)` for one pass
Space `O(N)` for stack in the worst cases
<br>

**Java:**
```java
    public int mctFromLeafValues(int[] A) {
        int res = 0;
        Stack<Integer> stack = new Stack<>();
        stack.push(Integer.MAX_VALUE);
        for (int a : A) {
            while (stack.peek() <= a) {
                int mid = stack.pop();
                res += mid * Math.min(stack.peek(), a);
            }
            stack.push(a);
        }
        while (stack.size() > 2) {
            res += stack.pop() * stack.peek();
        }
        return res;
    }
```

**C++:**
```cpp
    int mctFromLeafValues(vector<int>& A) {
        int res = 0;
        vector<int> stack = {INT_MAX};
        for (int a : A) {
            while (stack.back() <= a) {
                int mid = stack.back();
                stack.pop_back();
                res += mid * min(stack.back(), a);
            }
            stack.push_back(a);
        }
        for (int i = 2; i < stack.size(); ++i) {
            res += stack[i] * stack[i - 1];
        }
        return res;
    }
```

**Python:**
```python
    def mctFromLeafValues(self, A):
        res = 0
        stack = [float(\'inf\')]
        for a in A:
            while stack[-1] <= a:
                mid = stack.pop()
                res += mid * min(stack[-1], a)
            stack.append(a)
        while len(stack) > 2:
            res += stack.pop() * stack[-1]
        return res
```
<br>

# More Good Stack Problems
Here are some problems that impressed me.
Good luck and have fun.

- 1130. [Minimum Cost Tree From Leaf Values](https://leetcode.com/problems/minimum-cost-tree-from-leaf-values/discuss/339959/One-Pass-O(N)-Time-and-Space)
- 907. [Sum of Subarray Minimums](https://leetcode.com/problems/sum-of-subarray-minimums/discuss/170750/C++JavaPython-Stack-Solution)
- 901. [Online Stock Span](https://leetcode.com/problems/online-stock-span/discuss/168311/C++JavaPython-O(1))
- 856. [Score of Parentheses](https://leetcode.com/problems/score-of-parentheses/discuss/141777/C++JavaPython-O(1)-Space)
- 503. [Next Greater Element II](https://leetcode.com/problems/next-greater-element-ii/discuss/98270/JavaC++Python-Loop-Twice)
- 496.  Next Greater Element I
- 84. Largest Rectangle in Histogram
- 42. Trapping Rain Water

</p>


### [RZ] Summary of all the solutions I have learned from Discuss in Python
- Author: theflyingemini
- Creation Date: Mon Jan 13 2020 08:09:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 13 2020 08:11:13 GMT+0800 (Singapore Standard Time)

<p>
**1. Dynamic programming approach**
We are given a list of all the leaf nodes values for certain binary trees, but we do not know which leaf nodes belong to left subtree and which leaf nodes belong to right subtree. Since the given leaf nodes are result of inorder traversal, we know there will be pivots that divide arr into left and right, nodes in the left build left subtree and nodes in the right build right subtree. For each subtree, if we know the minimum sum, we can use it to build the result of the parent tree, so the problem can be divided into subproblems, and we have the following general transition equation (res(i, j) means the minimum non-leaf nodes sum with leaf nodes from arr[i] to arr[j]):

```
for k from i to j
    res(i, j) = min(res(i, k) + res(k + 1, j) + max(arr[i] ... arr[k]) * max(arr[k + 1] ... arr[j]))
```

**Top down code with memorization ---> O(n ^ 3)**
```
class Solution:
    def mctFromLeafValues(self, arr: List[int]) -> int:
        return self.helper(arr, 0, len(arr) - 1, {})
        
    def helper(self, arr, l, r, cache):
        if (l, r) in cache:
            return cache[(l, r)]
        if l >= r:
            return 0
        
        res = float(\'inf\')
        for i in range(l, r):
            rootVal = max(arr[l:i+1]) * max(arr[i+1:r+1])
            res = min(res, rootVal + self.helper(arr, l, i, cache) + self.helper(arr, i + 1, r, cache))
        
        cache[(l, r)] = res
        return res
```

**Bottom up code ---> O(n ^ 3)**
```
class Solution:
    def mctFromLeafValues(self, arr: List[int]) -> int:
        n = len(arr)
        dp = [[float(\'inf\') for _ in range(n)] for _ in range(n)]
        for i in range(n):
            dp[i][i] = 0
        
        for l in range(2, n + 1):
            for i in range(n - l + 1):
                j = i + l - 1
                for k in range(i, j):
                    rootVal = max(arr[i:k+1]) * max(arr[k+1:j+1])
                    dp[i][j] = min(dp[i][j], rootVal + dp[i][k] + dp[k + 1][j])
        return dp[0][n - 1]
```

**2. Greedy approach ---> O(n ^ 2)**
Above approach is kind of like brute force since we calculate and compare the results all possible pivots. To achieve a better time complexity, one important observation is that when we build each level of the binary tree, it is the max left leaf node and max right lead node that are being used, so we would like to put big leaf nodes close to the root. Otherwise, taking the leaf node with max value in the array as an example, if its level is deep, for each level above it, its value will be used to calculate the non-leaf node value, which will result in a big total sum.

With above observation, the greedy approach is to find the smallest value in the array, use it and its smaller neighbor to build a non-leaf node, then we can safely delete it from the array since it has a smaller value than its neightbor so it will never be used again. Repeat this process until there is only one node left in the array (which means we cannot build a new level any more)

```
class Solution:
    def mctFromLeafValues(self, arr: List[int]) -> int:
        res = 0
        while len(arr) > 1:
            index = arr.index(min(arr))
            if 0 < index < len(arr) - 1:
                res += arr[index] * min(arr[index - 1], arr[index + 1])
            else:
                res += arr[index] * (arr[index + 1] if index == 0 else arr[index - 1])
            arr.pop(index)
        return res
```

**3. Monotonic stack approach ---> O(n)**
In the greedy approach of 2), every time we delete the current minimum value, we need to start over and find the next smallest value again, so repeated operations are more or less involved. To further accelerate it, one observation is that for each leaf node in the array, when it becomes the minimum value in the remaining array, its left and right neighbors will be the first bigger value in the original array to its left and right. This observation is a clue of a possible monotonic stack solution as follows.

```
class Solution:
    def mctFromLeafValues(self, arr: List[int]) -> int:
        stack = [float(\'inf\')]
        res = 0
        for num in arr:
            while stack and stack[-1] <= num:
                cur = stack.pop()
                if stack:
                    res += cur * min(stack[-1], num)
            stack.append(num)
        while len(stack) > 2:
            res += stack.pop() * stack[-1]
        return res
```
</p>


### Greedy python solution
- Author: jadore801120
- Creation Date: Sun Jul 21 2019 12:17:27 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 12 2019 09:39:16 GMT+0800 (Singapore Standard Time)

<p>
The thought is quite straight forward.
1. Pick up the leaf node with minimum value.
2. Combine it with its inorder neighbor which has smaller value between neighbors.
3. Once we get the new generated non-leaf node, the node with minimum value is useless (For the new generated subtree will be represented with the largest leaf node value.)
4. Repeat it until there is only one node.
```
class Solution:
    def mctFromLeafValues(self, arr: List[int]) -> int:
        res = 0
        while len(arr) > 1:
            mini_idx = arr.index(min(arr))
            if 0 < mini_idx < len(arr) - 1:
                res += min(arr[mini_idx - 1], arr[mini_idx + 1]) * arr[mini_idx]
            else:
                res += arr[1 if mini_idx == 0 else mini_idx - 1] * arr[mini_idx]
            arr.pop(mini_idx)
        return res
```
</p>


