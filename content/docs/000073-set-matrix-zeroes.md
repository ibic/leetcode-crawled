---
title: "Set Matrix Zeroes"
weight: 73
#id: "set-matrix-zeroes"
---
## Description
<div class="description">
<p>Given an&nbsp;<code><em>m</em> x <em>n</em></code> matrix. If an element is <strong>0</strong>, set its entire row and column to <strong>0</strong>. Do it <a href="https://en.wikipedia.org/wiki/In-place_algorithm" target="_blank"><strong>in-place</strong></a>.</p>

<p><strong>Follow up:</strong></p>

<ul>
	<li>A straight forward solution using O(<em>m</em><em>n</em>) space is probably a bad idea.</li>
	<li>A simple improvement uses O(<em>m</em> + <em>n</em>) space, but still not the best solution.</li>
	<li>Could you devise a constant space solution?</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/17/mat1.jpg" style="width: 450px; height: 169px;" />
<pre>
<strong>Input:</strong> matrix = [[1,1,1],[1,0,1],[1,1,1]]
<strong>Output:</strong> [[1,0,1],[0,0,0],[1,0,1]]
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/17/mat2.jpg" style="width: 450px; height: 137px;" />
<pre>
<strong>Input:</strong> matrix = [[0,1,2,0],[3,4,5,2],[1,3,1,5]]
<strong>Output:</strong> [[0,0,0,0],[0,4,5,0],[0,3,1,0]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m == matrix.length</code></li>
	<li><code>n == matrix[0].length</code></li>
	<li><code>1 &lt;= m, n &lt;= 200</code></li>
	<li><code>-2<sup>31</sup> &lt;= matrix[i][j] &lt;= 2<sup>31</sup> - 1</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Facebook - 9 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: true)
- Apple - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)
- Audible - 2 (taggedByAdmin: false)
- Paypal - 3 (taggedByAdmin: false)
- Docusign - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Expedia - 3 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- TripAdvisor - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---

The question seems to be pretty simple but the trick here is that we need to modify the given matrix in place i.e. our space complexity needs to $$O(1)$$.

We will go through two different approaches to the question. The first approach makes use of additional memory while the other does not.
<br/>
<br/>

---

#### Approach 1: Additional Memory Approach

**Intuition**

If any cell of the matrix has a zero we can record its row and column number. All the cells of this recorded row and column can be marked zero in the next iteration.

**Algorithm**

1. We make a pass over our original array and look for zero entries.
2. If we find that an entry at `[i, j]` is 0, then we need to record somewhere the row `i` and column `j`.
3. So, we use two `sets`, one for the rows and one for the columns.
    <pre>
    if cell[i][j] == 0 {
        row_set.add(i)
        column_set.add(j)
    }</pre>

4. Finally, we iterate over the original matrix. For every cell we check if the row `r` or column `c` had been marked earlier. If any of them was marked, we set the value in the cell to 0.
    <pre>
    if r in row_set or c in column_set {
        cell[r][c] = 0
    }</pre>

<iframe src="https://leetcode.com/playground/ahbkEVZb/shared" frameBorder="0" width="100%" height="500" name="ahbkEVZb"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(M \times N)$$ where M and N are the number of rows and columns respectively.

* Space Complexity: $$O(M + N)$$.
<br/>
<br/>

---

#### Approach 2: O(1) Space, Efficient Solution

**Intuition**

Rather than using additional variables to keep track of rows and columns to be reset, we use the matrix itself as the *indicators*.

> The idea is that we can use the **first cell** of every row and column as a **flag**. This flag would determine whether a row or column has been set to zero. This means for every cell instead of going to $$M+N$$ cells and setting it to zero we just set the flag in two cells.

<pre>
if cell[i][j] == 0 {
    cell[i][0] = 0
    cell[0][j] = 0
}
</pre>

 These flags are used later to update the matrix. If the first cell of a row is set to zero this means the row should be marked zero. If the first cell of a column is set to zero this means the column should be marked zero.

**Algorithm**

1. We iterate over the matrix and we mark the first cell of a row `i` and first cell of a column `j`, if the condition in the pseudo code above is satisfied. i.e. if `cell[i][j] == 0`.

2. The first cell of row and column for the first row and first column is the same i.e. `cell[0][0]`. Hence, we use an additional variable to tell us if the first column had been marked or not and the `cell[0][0]` would be used to tell the same for the first row.

3. Now, we iterate over the original matrix starting from second row and second column i.e. `matrix[1][1]` onwards. For every cell we check if the row `r` or column `c` had been marked earlier by checking the respective first row cell or first column cell. If any of them was marked, we set the value in the cell to 0. Note the first row and first column serve as the `row_set` and `column_set` that we used in the first approach.

5. We then check if `cell[0][0] == 0`, if this is the case, we mark the first row as zero.

6. And finally, we check if the first column was marked, we make all entries in it as zeros.

!?!../Documents/73_Matrix_Zeroes.json:1000,400!?!

In the above animation we iterate all the cells and mark the corresponding first row/column cell incase of a cell with zero value.

<center>
<img src="../Figures/73/MatrixZeros_18_1.png" width="400"/>
</center>

We iterate the matrix we got from the above steps and mark respective cells zeroes.

<center>
<img src="../Figures/73/MatrixZeros_18_2.png" width="400"/>
</center>

<br>

<iframe src="https://leetcode.com/playground/WnHaFEGY/shared" frameBorder="0" width="100%" height="500" name="WnHaFEGY"></iframe>

**Complexity Analysis**

* Time Complexity : $$O(M \times N)$$
* Space Complexity : $$O(1)$$

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Any shorter O(1) space solution?
- Author: mzchen
- Creation Date: Thu Nov 13 2014 14:03:08 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 07:51:05 GMT+0800 (Singapore Standard Time)

<p>
My idea is simple: store states of each row in the first of that row, and store states of each column in the first of that column. Because the state of row0 and the state of column0 would occupy the same cell, I let it be the state of row0, and use another variable "col0" for column0. In the first phase, use matrix elements to set states in a top-down way. In the second phase, use states to set matrix elements in a bottom-up way.

    void setZeroes(vector<vector<int> > &matrix) {
        int col0 = 1, rows = matrix.size(), cols = matrix[0].size();
    
        for (int i = 0; i < rows; i++) {
            if (matrix[i][0] == 0) col0 = 0;
            for (int j = 1; j < cols; j++)
                if (matrix[i][j] == 0)
                    matrix[i][0] = matrix[0][j] = 0;
        }
    
        for (int i = rows - 1; i >= 0; i--) {
            for (int j = cols - 1; j >= 1; j--)
                if (matrix[i][0] == 0 || matrix[0][j] == 0)
                    matrix[i][j] = 0;
            if (col0 == 0) matrix[i][0] = 0;
        }
    }
</p>


### My AC java O(1) solution (easy to read)
- Author: lz2343
- Creation Date: Mon Jun 01 2015 17:05:32 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 03:58:56 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
    public void setZeroes(int[][] matrix) {
        boolean fr = false,fc = false;
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++) {
                if(matrix[i][j] == 0) {
                    if(i == 0) fr = true;
                    if(j == 0) fc = true;
                    matrix[0][j] = 0;
                    matrix[i][0] = 0;
                }
            }
        }
        for(int i = 1; i < matrix.length; i++) {
            for(int j = 1; j < matrix[0].length; j++) {
                if(matrix[i][0] == 0 || matrix[0][j] == 0) {
                    matrix[i][j] = 0;
                }
            }
        }
        if(fr) {
            for(int j = 0; j < matrix[0].length; j++) {
                matrix[0][j] = 0;
            }
        }
        if(fc) {
            for(int i = 0; i < matrix.length; i++) {
                matrix[i][0] = 0;
            }
        }
        
    }
}
</p>


### My C++ O(1) yoooooo
- Author: lugiavn
- Creation Date: Sun Jan 11 2015 02:47:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 10:27:50 GMT+0800 (Singapore Standard Time)

<p>
I find the last row which has 0, and use it to store the 0-collumns.
Then go row by row set them to 0.
Then go column by column set them to 0.
Finally set the last row which has 0. It's long but hey it's O(1) 


    class Solution {
    public:
        void setZeroes(vector<vector<int> > &matrix) {
            
            int H = matrix.size();
            int W = matrix[0].size();
            
            // find the last 0 row
            int last_0_row = -1;
            for (int y = H - 1; y >= 0 && last_0_row == -1; y--)
                for (int x = 0; x < W; x++)
                    if (matrix[y][x] == 0)
                    {
                        last_0_row = y;
                        break;
                    }
            if (last_0_row == -1)
                return;
            
            // go row by row
            for (int y = 0; y < last_0_row; y++)
            {
                bool this_is_a_0_row = false;
                
                for (int x = 0; x < W; x++)
                {
                    if (matrix[y][x] == 0)
                    {
                        this_is_a_0_row = true;
                        matrix[last_0_row][x] = 0;
                    }
                }
                
                if (this_is_a_0_row)
                for (int x = 0; x < W; x++)
                {
                    matrix[y][x] = 0;
                }
            }
            
            // set collums to 0
            for (int y = 0; y < H; y++)
            for (int x = 0; x < W; x++)
            {
                if (matrix[last_0_row][x] == 0)
                    matrix[y][x] = 0;
            }
            
            // set the last 0 row 
            for (int x = 0; x < W; x++)
            {
                matrix[last_0_row][x] = 0;
            }
        }
    };
</p>


