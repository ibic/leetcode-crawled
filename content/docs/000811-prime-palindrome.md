---
title: "Prime Palindrome"
weight: 811
#id: "prime-palindrome"
---
## Description
<div class="description">
<p>Find the smallest prime palindrome greater than or equal to <code>N</code>.</p>

<p>Recall that a&nbsp;number is <em>prime</em> if it&#39;s only divisors are 1 and itself, and it is greater than 1.&nbsp;</p>

<p>For example, 2,3,5,7,11 and 13 are&nbsp;primes.</p>

<p>Recall that a number is a <em>palindrome</em> if it reads the same from left to right as it does from right to left.&nbsp;</p>

<p>For example, 12321 is a palindrome.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">6</span>
<strong>Output: </strong><span id="example-output-1">7</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">8</span>
<strong>Output: </strong><span id="example-output-2">11</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">13</span>
<strong>Output: </strong><span id="example-output-3">101</span></pre>
</div>
</div>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>1 &lt;= N &lt;= 10^8</code></li>
	<li>The answer is guaranteed to exist and be less than <code>2 * 10^8</code>.</li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Amazon - 9 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach Framework

**Investigation of Brute Force**

Let's investigate and improve on a brute force method.

With basic methods, we can check whether an integer $$N$$ is a palindrome in $$O(\log N)$$ time, and check whether it is prime in $$O(\sqrt{N})$$ time.  So we would probably like to do the palindrome check first.

Now, say we naively check every number $$N, N+1, \cdots, N+K$$.  How big is $$K$$?

Well, the palindromes could be approximately $$10^4$$ apart, since for example `99988999`'s next palindrome is `99999999`.  

If we assume being a palindrome and being a prime is independent, then based on the density of primes, $$K \approx 10^4 \log N$$, and we would do a palindrome check on approximately $$10^4 \log^2 N$$ values, and a primality test on $$\log N$$ values of complexity $$\sqrt{N} \log N$$.  This seems to work.

However, we can't make this assumption of independence: whether a number is a palindrome or prime are *negatively correlated* events!  For example, $$22, 33, 44, \cdots, 99$$ are clearly not prime.  Actually, all palindromes with an even number of digits are divisible by 11, and are therefore not prime!  (Except for 11.)  For example, an 8 digit palindrome can be written as:

$$\sum_{i=0}^{3} a_i(10^{7-i} + 10^i) \equiv \sum a_i((-1)^{7-i} + (-1)^i) \equiv \sum a_i(0) \equiv 0 \pmod{11}$$

where the second-last equivalence follows as $$i$$ and $$7-i$$ have different parity.

**Density of Palindromes**

For a palindrome of $$d$$ digits, choosing the first $$k = \lfloor \frac{d+1}{2} \rfloor$$ digits uniquely determines the remaining digits.  Hence, there are $$9 * 10^{k-1}$$ of them (the first digit can't be 0.)  Thus, there are

$$9(10^0 + 10^0 + 10^1 + 10^1 + 10^2 + 10^2 + 10^3 + 10^3) < 20000$$

palindromes of 8 digits or less.  

Actually, we don't need to check the palindromes with an even number of digits, so there are under 10000 palindromes we need to check.  However, we also need to check palindromes until we encounter the first 9 digit prime palindrome, as all 8 digit numbers $$N$$ will have an answer equal to that.  Luckily, it occurs quickly: `100030001` is the 4th 9-digit value checked.  (We can verify this with brute force.)

For each palindrome, we can test whether it is prime in $$O(\sqrt{N})$$ operations.  So in total, an approach centered around enumerating palindromes seems like it will succeed.
<br />
<br />


---
#### Approach 1: Iterate Palindromes

**Intuition**

Say we have a palindrome $$X$$.  What is the next palindrome?

Each palindrome of $$d$$ digits has a *palindromic root*, it's first $$k = \frac{d+1}{2}$$ digits.  The next palindrome is formed by the next root.

For example, if $$123$$ is a root for the 5 digit palindrome $$12321$$, then the next palindrome is $$12421$$ with root $$124$$.

Notice that roots and palindromes are not a bijection, as palindromes $$123321$$ and $$12321$$ have the same root.

**Algorithm**

For each *palindromic root*, let's find the two associated palindromes (one with an odd number of digits, and one with an even number.)  For roots with $$k$$ digits, they will generate palindromes of $$2*k - 1$$ and $$2*k$$ digits.

If we didn't know that palindromes with an even number of digits (and greater than 11) are never prime, we're still fine - we can just check both possibilities.  When checking both possibilities, we check the palindromes with $$2k - 1$$ digits first, as they are all smaller than the palindromes with $$2k$$ digits.

We'll use an idea from [[LeetCode Problem: Reverse an Integer]](https://leetcode.com/problems/reverse-integer), in order to check whether an integer is a palindrome.  We could have also converted the integer to a string, and checked the indices directly.

As for testing primes with `isPrime(N)`, we'll use the standard $$O(\sqrt{N})$$ check: testing whether every number $$\leq \sqrt{N}$$ is a divisor of $$N$$.

<iframe src="https://leetcode.com/playground/NADFFoBY/shared" frameBorder="0" width="100%" height="500" name="NADFFoBY"></iframe>

**Complexity Analysis**

* Time Complexity:  Based on our analysis above, we performed on the order of $$O(N)$$ operations (not counting log factors from dealing with integers), given we knew the existence of prime palindrome `100030001`.  

  Interestingly, the time complexity is an open problem in mathematics, as it is not even known whether there are infinitely many prime palindromes, or whether palindromes behave as random integers for our purposes here - see [["Almost All Palindromes are Composite"]](https://arxiv.org/pdf/math/0405056.pdf) for more.

* Space Complexity:  $$O(\log N)$$, the space used by `s` (or `sb` in Java.)
<br />
<br />


---
#### Approach 2: Brute Force with Mathematical Shortcut

**Intuition**

Our brute force works except when $$N$$ is 8 digits (as explained in *Approach Framework* when we established that all 8 digit palindromes are not prime), so we can skip all 8 digit numbers.

**Algorithm**

For each number, check whether it is a palindrome.  If it is, check whether it is a prime.  If the number is 8 digits, skip to the 9 digit numbers.

<iframe src="https://leetcode.com/playground/KCCbseD9/shared" frameBorder="0" width="100%" height="500" name="KCCbseD9"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, with the caveats explained in *Approach #1*, and ignoring the $$\log N$$ factor when checking an integer for palindromes.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] All Even Length Palindrome are Divisible by 11
- Author: lee215
- Creation Date: Sun Jul 08 2018 11:14:14 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 09 2019 23:49:56 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Write some example, you find all even length palindromes are divisible by 11.
So we need to search only palindrome with odd length.

We can prove as follow:
11 % 11 = 0
1111 % 11 = 0
111111 % 11 = 0
11111111 % 11 = 0

**So:**
1001 % 11 = (1111 - 11 * 10) % 11 = 0
100001 % 11 = (111111 - 1111 * 10) % 11 = 0
10000001 % 11 = (11111111 - 111111 * 10) % 11 = 0

For any palindrome with even length:
abcddcba % 11
= (a * 10000001 + b * 100001 * 10 + c * 1001 * 100 + d * 11 * 1000) % 11
= 0

All palindrome with even length is multiple of `11`.
So among them, 11 is the only one prime
`if (8 <= N <= 11) return 11`

For other cases, **we consider only palindrome with odd dights.**
<br>

# More Generally
Explanation from @chuan-chih:
A number is divisible by 11 if `sum(even digits) - sum(odd digits)` is divisible by 11.
**Base case: 0**
**Inductive step:**
If there is no carry when we add 11 to a number, both sums +1.
Whenever carry happens, one sum -10 and the other +1.
The invariant holds in both cases.
<br>

# **Time Complexity**
`O(10000)` to check all numbers 1 - 10000.
`isPrime` function is `O(sqrt(x))` in worst case.
But only `sqrt(N)` worst cases for `1 <= x <= N`
In general it\'s `O(logx)`
<br>

**C++:**
```cpp
    int primePalindrome(int N) {
        if (8 <= N && N <= 11) return 11;
        for (int x = 1; x < 100000; ++x) {
            string s = to_string(x), r(s.rbegin(), s.rend());
            int y = stoi(s + r.substr(1));
            if (y >= N && isPrime(y)) return y;
        }
        return -1;
    }
    bool isPrime(int num) {
        if (num < 2 || num % 2 == 0) return num == 2;
        for (int i = 3; i * i <= num; i += 2)
            if (num % i == 0) return false;
        return true;
    }
```

**Java:**
```java
    public int primePalindrome(int N) {
        if (8 <= N && N <= 11) return 11;
        for (int x = 1; x < 100000; x++) {
            String s = Integer.toString(x), r = new StringBuilder(s).reverse().toString();
            int y = Integer.parseInt(s + r.substring(1));
            if (y >= N && isPrime(y)) return y;
        }
        return -1;
    }

    public Boolean isPrime(int x) {
        if (x < 2 || x % 2 == 0) return x == 2;
        for (int i = 3; i * i <= x; i += 2)
            if (x % i == 0) return false;
        return true;
    }
```


**Python:**
```py
    def primePalindrome(self, N):
        def isPrime(x):
            if x < 2 or x % 2 == 0: return x == 2
            for i in xrange(3, int(x**0.5) + 1, 2):
                if x % i == 0: return False
            return True
        if 8 <= N <= 11: return 11
        for x in xrange(10 ** (len(str(N)) / 2), 10**5):
            y = int(str(x) + str(x)[-2::-1])
            if y >= N and isPrime(y): return y
```
</p>


### C++ O(1) 0ms
- Author: 0xFFFFFFFF
- Creation Date: Sun Jul 08 2018 11:00:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 11:34:39 GMT+0800 (Singapore Standard Time)

<p>
base on exp, it should not be a big table; 

    class Solution {
    public:
        int primePalindrome(int N) {
            static vector<int> tab{
                2, 3, 5, 7, 11, 101, 131, 151, 181, 191, 313, 
                353, 373, 383, 727, 757, 787, 797, 919, 929, 10301, 
                10501, 10601, 11311, 11411, 12421, 12721, 12821, 13331, 13831, 13931, 
                14341, 14741, 15451, 15551, 16061, 16361, 16561, 16661, 17471, 17971, 
                18181, 18481, 19391, 19891, 19991, 30103, 30203, 30403, 30703, 30803, 
                31013, 31513, 32323, 32423, 33533, 34543, 34843, 35053, 35153, 35353, 
                35753, 36263, 36563, 37273, 37573, 38083, 38183, 38783, 39293, 70207, 
                70507, 70607, 71317, 71917, 72227, 72727, 73037, 73237, 73637, 74047, 
                74747, 75557, 76367, 76667, 77377, 77477, 77977, 78487, 78787, 78887, 
                79397, 79697, 79997, 90709, 91019, 93139, 93239, 93739, 94049, 94349, 
                94649, 94849, 94949, 95959, 96269, 96469, 96769, 97379, 97579, 97879, 
                98389, 98689, 1003001, 1008001, 1022201, 1028201, 1035301, 1043401, 1055501, 1062601, 
                1065601, 1074701, 1082801, 1085801, 1092901, 1093901, 1114111, 1117111, 1120211, 1123211, 
                1126211, 1129211, 1134311, 1145411, 1150511, 1153511, 1160611, 1163611, 1175711, 1177711, 
                1178711, 1180811, 1183811, 1186811, 1190911, 1193911, 1196911, 1201021, 1208021, 1212121, 
                1215121, 1218121, 1221221, 1235321, 1242421, 1243421, 1245421, 1250521, 1253521, 1257521, 
                1262621, 1268621, 1273721, 1276721, 1278721, 1280821, 1281821, 1286821, 1287821, 1300031, 
                1303031, 1311131, 1317131, 1327231, 1328231, 1333331, 1335331, 1338331, 1343431, 1360631, 
                1362631, 1363631, 1371731, 1374731, 1390931, 1407041, 1409041, 1411141, 1412141, 1422241, 
                1437341, 1444441, 1447441, 1452541, 1456541, 1461641, 1463641, 1464641, 1469641, 1486841, 
                1489841, 1490941, 1496941, 1508051, 1513151, 1520251, 1532351, 1535351, 1542451, 1548451, 
                1550551, 1551551, 1556551, 1557551, 1565651, 1572751, 1579751, 1580851, 1583851, 1589851, 
                1594951, 1597951, 1598951, 1600061, 1609061, 1611161, 1616161, 1628261, 1630361, 1633361, 
                1640461, 1643461, 1646461, 1654561, 1657561, 1658561, 1660661, 1670761, 1684861, 1685861, 
                1688861, 1695961, 1703071, 1707071, 1712171, 1714171, 1730371, 1734371, 1737371, 1748471, 
                1755571, 1761671, 1764671, 1777771, 1793971, 1802081, 1805081, 1820281, 1823281, 1824281, 
                1826281, 1829281, 1831381, 1832381, 1842481, 1851581, 1853581, 1856581, 1865681, 1876781, 
                1878781, 1879781, 1880881, 1881881, 1883881, 1884881, 1895981, 1903091, 1908091, 1909091, 
                1917191, 1924291, 1930391, 1936391, 1941491, 1951591, 1952591, 1957591, 1958591, 1963691, 
                1968691, 1969691, 1970791, 1976791, 1981891, 1982891, 1984891, 1987891, 1988891, 1993991, 
                1995991, 1998991, 3001003, 3002003, 3007003, 3016103, 3026203, 3064603, 3065603, 3072703, 
                3073703, 3075703, 3083803, 3089803, 3091903, 3095903, 3103013, 3106013, 3127213, 3135313, 
                3140413, 3155513, 3158513, 3160613, 3166613, 3181813, 3187813, 3193913, 3196913, 3198913, 
                3211123, 3212123, 3218123, 3222223, 3223223, 3228223, 3233323, 3236323, 3241423, 3245423, 
                3252523, 3256523, 3258523, 3260623, 3267623, 3272723, 3283823, 3285823, 3286823, 3288823, 
                3291923, 3293923, 3304033, 3305033, 3307033, 3310133, 3315133, 3319133, 3321233, 3329233, 
                3331333, 3337333, 3343433, 3353533, 3362633, 3364633, 3365633, 3368633, 3380833, 3391933, 
                3392933, 3400043, 3411143, 3417143, 3424243, 3425243, 3427243, 3439343, 3441443, 3443443, 
                3444443, 3447443, 3449443, 3452543, 3460643, 3466643, 3470743, 3479743, 3485843, 3487843, 
                3503053, 3515153, 3517153, 3528253, 3541453, 3553553, 3558553, 3563653, 3569653, 3586853, 
                3589853, 3590953, 3591953, 3594953, 3601063, 3607063, 3618163, 3621263, 3627263, 3635363, 
                3643463, 3646463, 3670763, 3673763, 3680863, 3689863, 3698963, 3708073, 3709073, 3716173, 
                3717173, 3721273, 3722273, 3728273, 3732373, 3743473, 3746473, 3762673, 3763673, 3765673, 
                3768673, 3769673, 3773773, 3774773, 3781873, 3784873, 3792973, 3793973, 3799973, 3804083, 
                3806083, 3812183, 3814183, 3826283, 3829283, 3836383, 3842483, 3853583, 3858583, 3863683, 
                3864683, 3867683, 3869683, 3871783, 3878783, 3893983, 3899983, 3913193, 3916193, 3918193, 
                3924293, 3927293, 3931393, 3938393, 3942493, 3946493, 3948493, 3964693, 3970793, 3983893, 
                3991993, 3994993, 3997993, 3998993, 7014107, 7035307, 7036307, 7041407, 7046407, 7057507, 
                7065607, 7069607, 7073707, 7079707, 7082807, 7084807, 7087807, 7093907, 7096907, 7100017, 
                7114117, 7115117, 7118117, 7129217, 7134317, 7136317, 7141417, 7145417, 7155517, 7156517, 
                7158517, 7159517, 7177717, 7190917, 7194917, 7215127, 7226227, 7246427, 7249427, 7250527, 
                7256527, 7257527, 7261627, 7267627, 7276727, 7278727, 7291927, 7300037, 7302037, 7310137, 
                7314137, 7324237, 7327237, 7347437, 7352537, 7354537, 7362637, 7365637, 7381837, 7388837, 
                7392937, 7401047, 7403047, 7409047, 7415147, 7434347, 7436347, 7439347, 7452547, 7461647, 
                7466647, 7472747, 7475747, 7485847, 7486847, 7489847, 7493947, 7507057, 7508057, 7518157, 
                7519157, 7521257, 7527257, 7540457, 7562657, 7564657, 7576757, 7586857, 7592957, 7594957, 
                7600067, 7611167, 7619167, 7622267, 7630367, 7632367, 7644467, 7654567, 7662667, 7665667, 
                7666667, 7668667, 7669667, 7674767, 7681867, 7690967, 7693967, 7696967, 7715177, 7718177, 
                7722277, 7729277, 7733377, 7742477, 7747477, 7750577, 7758577, 7764677, 7772777, 7774777, 
                7778777, 7782877, 7783877, 7791977, 7794977, 7807087, 7819187, 7820287, 7821287, 7831387, 
                7832387, 7838387, 7843487, 7850587, 7856587, 7865687, 7867687, 7868687, 7873787, 7884887, 
                7891987, 7897987, 7913197, 7916197, 7930397, 7933397, 7935397, 7938397, 7941497, 7943497, 
                7949497, 7957597, 7958597, 7960697, 7977797, 7984897, 7985897, 7987897, 7996997, 9002009, 
                9015109, 9024209, 9037309, 9042409, 9043409, 9045409, 9046409, 9049409, 9067609, 9073709, 
                9076709, 9078709, 9091909, 9095909, 9103019, 9109019, 9110119, 9127219, 9128219, 9136319, 
                9149419, 9169619, 9173719, 9174719, 9179719, 9185819, 9196919, 9199919, 9200029, 9209029, 
                9212129, 9217129, 9222229, 9223229, 9230329, 9231329, 9255529, 9269629, 9271729, 9277729, 
                9280829, 9286829, 9289829, 9318139, 9320239, 9324239, 9329239, 9332339, 9338339, 9351539, 
                9357539, 9375739, 9384839, 9397939, 9400049, 9414149, 9419149, 9433349, 9439349, 9440449, 
                9446449, 9451549, 9470749, 9477749, 9492949, 9493949, 9495949, 9504059, 9514159, 9526259, 
                9529259, 9547459, 9556559, 9558559, 9561659, 9577759, 9583859, 9585859, 9586859, 9601069, 
                9602069, 9604069, 9610169, 9620269, 9624269, 9626269, 9632369, 9634369, 9645469, 9650569, 
                9657569, 9670769, 9686869, 9700079, 9709079, 9711179, 9714179, 9724279, 9727279, 9732379, 
                9733379, 9743479, 9749479, 9752579, 9754579, 9758579, 9762679, 9770779, 9776779, 9779779, 
                9781879, 9782879, 9787879, 9788879, 9795979, 9801089, 9807089, 9809089, 9817189, 9818189, 
                9820289, 9822289, 9836389, 9837389, 9845489, 9852589, 9871789, 9888889, 9889889, 9896989, 
                9902099, 9907099, 9908099, 9916199, 9918199, 9919199, 9921299, 9923299, 9926299, 9927299, 
                9931399, 9932399, 9935399, 9938399, 9957599, 9965699, 9978799, 9980899, 9981899, 9989899, 
                100030001
            };
            return *lower_bound(tab.begin(), tab.end(), N);
        }
    };
</p>


### Getting one over the system (O(1) solution in Java)
- Author: JavaProgrammer21
- Creation Date: Sun Jul 08 2018 11:06:56 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 17 2018 08:23:45 GMT+0800 (Singapore Standard Time)

<p>
So I spend 30 mins (and 5 bugs) trying to get my O(N^2) solution to work. It doesn\'t. So I get mad and rage-generate an array of all the palindromic primes using 1553 ms. Next, I put them into my revenge array.

```
class Solution {
    private final int[] hehehe = new int[]{2, 3, 5, 7, 11, 101, 131, 151, 181, 191, 
		313, 353, 373, 383, 727, 757, 787, 797, 919, 929, 10301, 10501, 10601, 
		11311, 11411, 12421, 12721, 12821, 13331, 13831, 13931, 14341, 14741, 
		15451, 15551, 16061, 16361, 16561, 16661, 17471, 17971, 18181, 18481, 
		19391, 19891, 19991, 30103, 30203, 30403, 30703, 30803, 31013, 31513, 
		32323, 32423, 33533, 34543, 34843, 35053, 35153, 35353, 35753, 36263, 
		36563, 37273, 37573, 38083, 38183, 38783, 39293, 70207, 70507, 70607, 
		71317, 71917, 72227, 72727, 73037, 73237, 73637, 74047, 74747, 75557, 
		76367, 76667, 77377, 77477, 77977, 78487, 78787, 78887, 79397, 79697, 
		79997, 90709, 91019, 93139, 93239, 93739, 94049, 94349, 94649, 94849, 
		94949, 95959, 96269, 96469, 96769, 97379, 97579, 97879, 98389, 98689, 
		1003001, 1008001, 1022201, 1028201, 1035301, 1043401, 1055501, 
		1062601, 1065601, 1074701, 1082801, 1085801, 1092901, 1093901, 
		1114111, 1117111, 1120211, 1123211, 1126211, 1129211, 1134311...};
```
You get the idea.
O(1) solution I got after brute-forcing all the primes I needed.
```
    public final int[] hehehe; //(see above)
    public int primePalindrome(int N) {
        //2lazE to implement binary search
        for(int i = 0; i < hehehe.length; i++){
            if(N <= hehehe[i]){
                return hehehe[i];
            }
        }
        return -1;
    }
}
```

It didn\'t nab me for memory usage LOL. Check out the C++ one also, its faster (coder used binary search) and was posted first.
</p>


