---
title: "Check If Two Expression Trees are Equivalent"
weight: 1469
#id: "check-if-two-expression-trees-are-equivalent"
---
## Description
<div class="description">
<p>A <strong><a href="https://en.wikipedia.org/wiki/Binary_expression_tree" target="_blank">binary expression tree</a></strong> is a kind of binary tree used to represent arithmetic expressions. Each node of a binary expression tree has either zero or two children. Leaf nodes (nodes with 0 children) correspond to operands (variables), and internal nodes (nodes with two children) correspond to the operators. In this problem, we only consider the <code>&#39;+&#39;</code> operator (i.e. addition).</p>

<p>You are given the roots of two binary expression trees, <code>root1</code> and <code>root2</code>. Return <code>true</code><em> if the two binary expression trees are equivalent</em>. Otherwise, return <code>false</code>.</p>

<p>Two binary expression trees are equivalent if they <strong>evaluate to the same value</strong> regardless of what the variables are set to.</p>

<p><strong>Follow up:</strong> What will you change in your solution if the tree also supports the <code>&#39;-&#39;</code> operator (i.e. subtraction)?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> root1 = [x], root2 = [x]
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/10/04/tree1.png" style="width: 211px; height: 131px;" /></strong></p>

<pre>
<strong>Input:</strong> root1 = [+,a,+,null,null,b,c], root2 = [+,+,b,c,a]
<strong>Output:</strong> true
<strong>Explaination:</strong> <code>a + (b + c) == (b + c) + a</code></pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/10/04/tree2.png" style="width: 211px; height: 131px;" /></strong></p>

<pre>
<strong>Input:</strong> root1 = [+,a,+,null,null,b,c], root2 = [+,+,b,d,a]
<strong>Output:</strong> false
<strong>Explaination:</strong> <code>a + (b + c) != (b + d) + a</code>
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in both trees are equal, odd and, in the range <code>[1, 4999]</code>.</li>
	<li><code>Node.val</code> is <code>&#39;+&#39;</code> or a lower-case English letter.</li>
	<li>It&#39;s <strong>guaranteed</strong> that the tree given is a valid binary expression tree.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Tree (tree)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] simple DFS solution and follow-up
- Author: alanlzl
- Creation Date: Thu Oct 08 2020 11:46:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 02:12:43 GMT+0800 (Singapore Standard Time)

<p>
**Idea**

We can use a dictionary to keep track of each value.

When traverse the first tree, we do `dic[node.val] += 1`. And when traverse the second tree, we do `dic[node.val] -= 1`. If two trees are equivalent, all dic values should be 0 at the end.

This method extends nicely to the follow-up question. If there\'s a minus sign, we simply revert the increment value (`sign`) of the right child. 

</br>

**Complexity**

Time complexity: `O(N)`
Space complexity: `O(logN)`

</br>

**Python**

```Python
class Solution:
    def checkEquivalence(self, root1: \'Node\', root2: \'Node\') -> bool:
        
        dic = collections.Counter()
        
        def dfs(node, sign):
            if not node:
                return
            if node.val == \'+\':
                dfs(node.left, sign)
                dfs(node.right, sign)
            elif node.val == \'-\': # for follow-up
                dfs(node.left, sign)
                dfs(node.right, -sign)
            else:
                dic[node.val] += sign
        
        dfs(root1, 1)
        dfs(root2, -1)
        return all(x == 0 for x in dic.values())
```
</p>


### Java Preorder | O(n) time O(26) space|  Followup solution
- Author: logN-
- Creation Date: Fri Oct 09 2020 21:03:37 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 09 2020 21:08:25 GMT+0800 (Singapore Standard Time)

<p>
Since the problem says given tree are valid expressions we only need to see that both trees have same elments. We can easily do that by traversing the first tree in any order we like and recording the values in a map followed by traversing the second tree and taking values out from the map.

If there is any non zero residue in map we return false. 



    public boolean checkEquivalence(Node root1, Node root2) {
        int[]map=new int[26];
        addToMap(root1,map,1);
        addToMap(root2,map,-1);
        for(int val:map)
            if(val!=0)
                return false;
        return true;
    }
    
    private void addToMap(Node root,int[]map,int add){
        if(root==null)
            return;
        if(root.val<=\'z\' && root.val>=\'a\')
			map[root.val-\'a\']+=add;
          
        addToMap(root.left,map,add);
        addToMap(root.right,map,add);
    }
.
.

For followup we again need to make sure both tree have same elements. But this time we need to also take into account the sign of elements. We can do this by adding another param sign to the addToMap function and reversing sign for right children of negative nodes as follows:

	private void addToMap(Node root,int[]map,int add,int sign=1){
    if(root==null)
        return;
    if(root.val<=\'z\' && root.val>=\'a\')
		 map[root.val-\'a\']+=(sign*add);
   
	//Left child wont have any effect of sign
    addToMap(root.left,map,add,sign);
	
	//Right child have their signed parents in front of them hence reverse sign for right subtree if current node is negative
	if(root.val==\'-\') sign*=-1;
    addToMap(root.right,map,add,sign);
}




</p>


### C++ #minimalizm
- Author: votrubac
- Creation Date: Sat Oct 10 2020 02:14:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 10 2020 02:39:37 GMT+0800 (Singapore Standard Time)

<p>
Just for fun: build a hash that represents counts of each variable, and compare in the end. 

Ideally, we would use prime numbers, but adding a square of each character is accepted here (idea by [theryanyao](https://leetcode.com/theryanyao/)).

> Note: ``\'`\'`` character appears before  `\'a\'`. So  `\'a\'` will be converted to `1`, `\'b\'` to `2`, and so on.

```cpp
int dfs(Node* n) {
    return n->val == \'+\' ? dfs(n->left) + dfs(n->right) : (n->val - \'`\') * (n->val - \'`\');
}   
bool checkEquivalence(Node* root1, Node* root2) {
    return dfs(root1) == dfs(root2);
}
```
</p>


