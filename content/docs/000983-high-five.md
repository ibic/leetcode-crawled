---
title: "High Five"
weight: 983
#id: "high-five"
---
## Description
<div class="description">
<p>Given a list of scores of different students, return the average score of each student&#39;s <strong>top five scores</strong> in<strong> the order of each student&#39;s id</strong>.</p>

<p>Each entry <code>items[i]</code>&nbsp;has <code>items[i][0]</code> the student&#39;s id, and <code>items[i][1]</code> the student&#39;s score.&nbsp; The average score is calculated using integer division.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[1,91],[1,92],[2,93],[2,97],[1,60],[2,77],[1,65],[1,87],[1,100],[2,100],[2,76]]</span>
<strong>Output: </strong><span id="example-output-1">[[1,87],[2,88]]</span>
<strong>Explanation: </strong>
The average of the student with id = 1 is 87.
The average of the student with id = 2 is 88.6. But with integer division their average converts to 88.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= items.length &lt;= 1000</code></li>
	<li><code>items[i].length == 2</code></li>
	<li>The IDs of the students is between <code>1</code> to <code>1000</code></li>
	<li>The score of the students is between <code>1</code> to <code>100</code></li>
	<li>For each student,&nbsp;there are at least 5 scores</li>
</ol>
</div>

## Tags
- Array (array)
- Hash Table (hash-table)
- Sort (sort)

## Companies
- Goldman Sachs - 16 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Map + PriorityQueue
- Author: ultaabhi
- Creation Date: Thu Jul 04 2019 18:15:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 27 2020 11:40:40 GMT+0800 (Singapore Standard Time)

<p>
```

class Solution {
    public int[][] highFive(int[][] items) {
    
       TreeMap<Integer, PriorityQueue<Integer>> map = new TreeMap<>();
        
       for(int[] item : items){
           int id = item[0];
           int score = item[1];
           
           if(!map.containsKey(id)){
               PriorityQueue<Integer> pq = new PriorityQueue<Integer>(5);
               pq.offer(score);
               map.put(id, pq);
           }else{
               PriorityQueue<Integer> pq = map.get(id);
               pq.offer(score);
               if(pq.size() > 5){
                   pq.poll();
               }
               map.put(id, pq);
           }
       }
        
       int index = 0;
        
       int[][] res = new int[map.size()][2];
        
       for(int id : map.keySet()){
           
           res[index][0] = id;
        
           PriorityQueue<Integer> pq = map.get(id);
           int sum = 0;
           int size = pq.size();
           
           while(!pq.isEmpty()){
               sum+= pq.poll();
           }
         
           res[index][1] = sum/size;
           
           index++;
           
       }
        
        return res;  
    }
}

```
</p>


### Python - Sort or Priority Queue solution
- Author: cravo123
- Creation Date: Sun Jun 16 2019 02:27:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 16 2019 02:27:42 GMT+0800 (Singapore Standard Time)

<p>
Solution 1, first sort reversely by id and score, and then calculate average for each id, similar to Running Length Encoding.
```
class Solution:
    def highFive(self, items: List[List[int]]) -> List[List[int]]:
        items.sort(reverse=True)
        
        res = []
        curr = []
        idx = items[0][0]
        
        for i, val in items:
            if i == idx:
                if len(curr) < 5:
                    curr.append(val)
            else:
                res.append([idx, sum(curr) // len(curr)])
                curr = [val]
                idx = i
        
        res.append([idx, sum(curr) // len(curr)])
        
        res = res[::-1]
        
        return res
```

Solution 2, use priority queue for each id,
```
class Solution:
    def highFive(self, items: List[List[int]]) -> List[List[int]]:
        d = collections.defaultdict(list)
        
        for idx, val in items:
            heapq.heappush(d[idx], val)
            
            if len(d[idx]) > 5:
                heapq.heappop(d[idx])
        
        res = [[i, sum(d[i]) // len(d[i])] for i in sorted(d)]
        
        return res
```
</p>


### Python faster than 98.5%
- Author: buddyeorl
- Creation Date: Fri Jul 26 2019 13:55:06 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 26 2019 13:55:06 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def highFive(self, items: List[List[int]]) -> List[List[int]]:
        d={}
        for i in items:
            try:
                d[i[0]].append(i[1])
            except:
                d[i[0]]=[i[1]]
        for i in d:
            d[i].sort()
            d[i]=d[i][::-1][:5]
        return [[i,sum(d[i])//len(d[i])] for i in d]
```
</p>


