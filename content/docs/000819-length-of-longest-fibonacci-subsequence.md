---
title: "Length of Longest Fibonacci Subsequence"
weight: 819
#id: "length-of-longest-fibonacci-subsequence"
---
## Description
<div class="description">
<p>A sequence <code>X_1, X_2, ..., X_n</code>&nbsp;is <em>fibonacci-like</em> if:</p>

<ul>
	<li><code>n &gt;= 3</code></li>
	<li><code>X_i + X_{i+1} = X_{i+2}</code>&nbsp;for all&nbsp;<code>i + 2 &lt;= n</code></li>
</ul>

<p>Given a <b>strictly increasing</b>&nbsp;array&nbsp;<code>A</code> of positive integers forming a sequence, find the <strong>length</strong> of the longest fibonacci-like subsequence of <code>A</code>.&nbsp; If one does not exist, return 0.</p>

<p>(<em>Recall that a subsequence is derived from another sequence <code>A</code> by&nbsp;deleting any number of&nbsp;elements (including none)&nbsp;from <code>A</code>, without changing the order of the remaining elements.&nbsp; For example, <code>[3, 5, 8]</code> is a subsequence of <code>[3, 4, 5, 6, 7, 8]</code>.</em>)</p>

<p>&nbsp;</p>

<ul>
</ul>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>[1,2,3,4,5,6,7,8]
<strong>Output: </strong>5
<strong>Explanation:
</strong>The longest subsequence that is fibonacci-like: [1,2,3,5,8].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>[1,3,7,11,12,14,18]
<strong>Output: </strong>3
<strong>Explanation</strong>:
The longest subsequence that is fibonacci-like:
[1,11,12], [3,11,14] or [7,11,18].
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>3 &lt;= A.length &lt;= 1000</code></li>
	<li><code>1 &lt;= A[0] &lt; A[1] &lt; ... &lt; A[A.length - 1] &lt;= 10^9</code></li>
	<li><em>(The time limit has been reduced by 50% for submissions in Java, C, and C++.)</em></li>
</ul>

</div>

## Tags
- Array (array)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Baidu - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Brute Force with Set

**Intuition**

Every Fibonacci-like subsequence has each two adjacent terms determine the next expected term.  For example, with `2, 5`, we expect that the sequence must continue `7, 12, 19, 31`, etc.

We can use a `Set` structure to determine quickly whether the next term is in the array `A` or not.  Because of the exponential growth of these terms, there are at most 43 terms in any Fibonacci-like subsequence that has maximum value $$\leq 10^9$$.

**Algorithm**

For each starting pair `A[i], A[j]`, we maintain the next expected value `y = A[i] + A[j]` and the previously seen largest value `x = A[j]`.  If `y` is in the array, then we can then update these values `(x, y) -> (y, x+y)`.

Also, because subsequences are only fibonacci-like if they have length 3 or more, we must perform the check `ans >= 3 ? ans : 0` at the end.

<iframe src="https://leetcode.com/playground/ds2tXy7s/shared" frameBorder="0" width="100%" height="500" name="ds2tXy7s"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2 \log M)$$, where $$N$$ is the length of `A`, and $$M$$ is the maximum value of `A`.

* Space Complexity:  $$O(N)$$, the space used by the set `S`.
<br />
<br />


---
#### Approach 2: Dynamic Programming

**Intuition**

Think of two consecutive terms `A[i], A[j]` in a fibonacci-like subsequence as a single node `(i, j)`, and the entire subsequence is a path between these consecutive nodes.  For example, with the fibonacci-like subsequence `(A[1] = 2, A[2] = 3, A[4] = 5, A[7] = 8, A[10] = 13)`, we have the path between nodes `(1, 2) <-> (2, 4) <-> (4, 7) <-> (7, 10)`.

The motivation for this is that two nodes `(i, j)` and `(j, k)` are connected if and only if `A[i] + A[j] == A[k]`, and we needed this amount of information to know about this connection.  Now we have a problem similar to *Longest Increasing Subsequence*.

**Algorithm**

Let `longest[i, j]` be the longest path ending in `[i, j]`.  Then `longest[j, k] = longest[i, j] + 1` if `(i, j)` and `(j, k)` are connected.  Since `i` is uniquely determined as `A.index(A[k] - A[j])`, this is efficient: we check for each `j < k` what `i` is potentially, and update `longest[j, k]` accordingly.

<iframe src="https://leetcode.com/playground/vVG7P67m/shared" frameBorder="0" width="100%" height="463" name="vVG7P67m"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N \log M)$$, where $$M$$ is the largest element of `A`.  We can show that the number of elements in a subsequence is bounded by $$O(\log \frac{M}{a})$$ where $$a$$ is the minimum element in the subsequence.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Check Pair
- Author: lee215
- Creation Date: Sun Jul 22 2018 11:12:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 04 2019 00:57:37 GMT+0800 (Singapore Standard Time)

<p>
# Solution 1
Save array `A` to a hash set `s`.
Start from base (A[i], A[j]) as the first two element in the sequence,
we try to find the Fibonacci like subsequence as long as possible,

Initial `(a, b) = (A[i], A[j])`
While the set `s` contains `a + b`, we update `(a, b) = (b, a + b)`.
In the end we update the longest length we find.

**Time Complexity**:
`O(N^2logM)`, where `M` is the `max(A)`.

Quote from @renato4:
Just clarifying a little bit more.
Since the values grow exponentially,
the amount of numbers needed to accommodate a sequence
that ends in a number `M` is at most `log(M)`.

**C++:**
```
    int lenLongestFibSubseq(vector<int>& A) {
        unordered_set<int> s(A.begin(), A.end());
        int res = 0;
        for (int i = 0; i < A.size(); ++i) {
            for (int j = i + 1; j < A.size(); ++j) {
                int  a = A[i], b = A[j], l = 2;
                while (s.count(a + b))
                    b = a + b, a = b - a, l++;
                res = max(res, l);
            }
        }
        return res > 2 ? res : 0;
    }
```

**Java:**
```
    public int lenLongestFibSubseq(int[] A) {
        Set<Integer> s = new HashSet<Integer>();
        for (int x : A) s.add(x);
        int res = 2;
        for (int i = 0; i < A.length; ++i)
            for (int j = i + 1; j < A.length; ++j) {
                int a = A[i], b = A[j], l = 2;
                while (s.contains(a + b)) {
                    b = a + b;
                    a = b - a;
                    l++;
                }
                res = Math.max(res, l);
            }
        return res > 2 ? res : 0;
    }
```
**Python:**
```
    def lenLongestFibSubseq(self, A):
        s = set(A)
        res = 2
        for i in range(len(A)):
            for j in range(i + 1, len(A)):
                a, b, l = A[i], A[j], 2
                while a + b in s:
                    a, b, l = b, a + b, l + 1
                res = max(res, l)
        return res if res > 2 else 0
```

<br>

## Solution 2
Another solution is kind of dp.
`dp[a, b]` represents the length of fibo sequence ends up with `(a, b)`
Then we have `dp[a, b] = (dp[b - a, a] + 1 ) or 2`
The complexity reduce to `O(N^2)`.
In C++/Java, I use 2D dp and index as key.
In Python, I use value as key.

**Time Complexity**:
`O(N^2)`

**C++**
```
    int lenLongestFibSubseq(vector<int>& A) {
        unordered_map<int, int> m;
        int N = A.size(), res = 0;
        int dp[N][N];
        for (int j = 0; j < N; ++j) {
            m[A[j]] = j;
            for (int i = 0; i < j; ++i) {
                int k = m.find(A[j] - A[i]) == m.end() ? -1 : m[A[j] - A[i]];
                dp[i][j] = (A[j] - A[i] < A[i] && k >= 0) ? dp[k][i] + 1 : 2;
                res = max(res, dp[i][j]);
            }
        }
        return res > 2 ? res : 0;
    }
```

**Java**
```
    public int lenLongestFibSubseq(int[] A) {
        int res = 0;
        int[][] dp = new int[A.length][A.length];
        Map<Integer, Integer> index = new HashMap<>();
        for (int j = 0; j < A.length; j++) {
            index.put(A[j], j);
            for (int i = 0; i < j; i++) {
                int k = index.getOrDefault(A[j] - A[i], -1);
                dp[i][j] = (A[j] - A[i] < A[i] && k >= 0) ? dp[k][i] + 1 : 2;
                res = Math.max(res, dp[i][j]);
            }
        }
        return res > 2 ? res : 0;
    }
```

**Python**
```
    def lenLongestFibSubseq(self, A):
        dp = collections.defaultdict(int)
        s = set(A)
        for j in xrange(len(A)):
            for i in xrange(j):
                if A[j] - A[i] < A[i] and A[j] - A[i] in s:
                    dp[A[i], A[j]] = dp.get((A[j] - A[i], A[i]), 2) + 1
        return max(dp.values() or [0])
```
</p>


### Java beat 98% DP + 2Sum
- Author: peritan
- Creation Date: Fri Aug 31 2018 22:53:55 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 09 2020 22:29:19 GMT+0800 (Singapore Standard Time)

<p>
For each A[i] find previous 2 elements sum up to A[i], 
then it\'s a classical follow up problem for Two Sum 
`167. Two Sum II - Input array is sorted`
if 2 elements A[l] and A[r] sum up to A[i]
`dp[r][i]`: length of longest fibonacchi sequence end with A[r], A[i]
 `dp[r][i] = dp[l][r] + 1`
return the max(all posible dp[r][i])
```
    public int lenLongestFibSubseq(int[] A) {
        int n = A.length;
        int max = 0;
        int[][] dp = new int[n][n];
        for (int i = 2; i < n; i++) {
            int l = 0, r = i - 1;
	        while (l < r) {
                int sum = A[l] + A[r];
                if (sum > A[i]) {
                    r--;  
                } else if (sum < A[i]) {
                    l++;
                } else {
                    dp[r][i] = dp[l][r] + 1;
                    max = Math.max(max, dp[r][i]);
                    r--;
                    l++;
                }
            }
        }
        return max == 0 ? 0 : max + 2;
    }
```
Time Complexity: `O(N^2)`
Space Complexity: `O(N^2)`
</p>


### Java clean DP O(n^2) time O(n^2) space
- Author: wangzi6147
- Creation Date: Sun Jul 22 2018 11:09:19 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 07:23:49 GMT+0800 (Singapore Standard Time)

<p>
`dp[i][j]` represents the length of longest sequence which ends with `A[i]` and `A[j]`.

```
class Solution {
    public int lenLongestFibSubseq(int[] A) {
        int n = A.length;
        int[][] dp = new int[n][n];
        Map<Integer, Integer> pos = new HashMap<>();
        for (int i = 0; i < n; i++) {
            pos.put(A[i], i);
            for (int j = i; j < n; j++) {
                dp[i][j] = 2;
            }
        }
        for (int j = 2; j < n; j++) {
            for (int i = j - 1; i > 0; i--) {
                int prev = A[j] - A[i];
                if (prev >= A[i]) {
                    break;
                }
                if (!pos.containsKey(prev)) {
                    continue;
                }
                dp[i][j] = dp[pos.get(prev)][i] + 1;
            }
        }
        int result = 0;
        for (int j = 2; j < n; j++) {
            for (int i = 1; i < n - 1; i++) {
                if (dp[i][j] > 2) {
                    result = Math.max(result, dp[i][j]);
                }
            }
        }
        return result;
    }
}
```
</p>


