---
title: "Count Unhappy Friends"
weight: 1445
#id: "count-unhappy-friends"
---
## Description
<div class="description">
<p>You are given a list of&nbsp;<code>preferences</code>&nbsp;for&nbsp;<code>n</code>&nbsp;friends, where <code>n</code> is always <strong>even</strong>.</p>

<p>For each person <code>i</code>,&nbsp;<code>preferences[i]</code>&nbsp;contains&nbsp;a list of friends&nbsp;<strong>sorted</strong> in the <strong>order of preference</strong>. In other words, a friend earlier in the list is more preferred than a friend later in the list.&nbsp;Friends in&nbsp;each list are&nbsp;denoted by integers from <code>0</code> to <code>n-1</code>.</p>

<p>All the friends are divided into pairs.&nbsp;The pairings are&nbsp;given in a list&nbsp;<code>pairs</code>,&nbsp;where <code>pairs[i] = [x<sub>i</sub>, y<sub>i</sub>]</code> denotes <code>x<sub>i</sub></code>&nbsp;is paired with <code>y<sub>i</sub></code> and <code>y<sub>i</sub></code> is paired with <code>x<sub>i</sub></code>.</p>

<p>However, this pairing may cause some of the friends to be unhappy.&nbsp;A friend <code>x</code>&nbsp;is unhappy if <code>x</code>&nbsp;is paired with <code>y</code>&nbsp;and there exists a friend <code>u</code>&nbsp;who&nbsp;is paired with <code>v</code>&nbsp;but:</p>

<ul>
	<li><code>x</code>&nbsp;prefers <code>u</code>&nbsp;over <code>y</code>,&nbsp;and</li>
	<li><code>u</code>&nbsp;prefers <code>x</code>&nbsp;over <code>v</code>.</li>
</ul>

<p>Return <em>the number of unhappy friends</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 4, preferences = [[1, 2, 3], [3, 2, 0], [3, 1, 0], [1, 2, 0]], pairs = [[0, 1], [2, 3]]
<strong>Output:</strong> 2
<strong>Explanation:</strong>
Friend 1 is unhappy because:
- 1 is paired with 0 but prefers 3 over 0, and
- 3 prefers 1 over 2.
Friend 3 is unhappy because:
- 3 is paired with 2 but prefers 1 over 2, and
- 1 prefers 3 over 0.
Friends 0 and 2 are happy.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2, preferences = [[1], [0]], pairs = [[1, 0]]
<strong>Output:</strong> 0
<strong>Explanation:</strong> Both friends 0 and 1 are happy.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 4, preferences = [[1, 3, 2], [2, 3, 0], [1, 3, 0], [0, 2, 1]], pairs = [[1, 3], [0, 2]]
<strong>Output:</strong> 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 500</code></li>
	<li><code>n</code>&nbsp;is even.</li>
	<li><code>preferences.length&nbsp;== n</code></li>
	<li><code>preferences[i].length&nbsp;== n - 1</code></li>
	<li><code>0 &lt;= preferences[i][j] &lt;= n - 1</code></li>
	<li><code>preferences[i]</code>&nbsp;does not contain <code>i</code>.</li>
	<li>All values in&nbsp;<code>preferences[i]</code>&nbsp;are unique.</li>
	<li><code>pairs.length&nbsp;== n/2</code></li>
	<li><code>pairs[i].length&nbsp;== 2</code></li>
	<li><code>x<sub>i</sub> != y<sub>i</sub></code></li>
	<li><code>0 &lt;= x<sub>i</sub>, y<sub>i</sub>&nbsp;&lt;= n - 1</code></li>
	<li>Each person is contained in <strong>exactly one</strong> pair.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Bloomberg - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### What a Bad Question
- Author: LeetCodeFunker
- Creation Date: Sun Sep 13 2020 12:01:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 13 2020 13:29:32 GMT+0800 (Singapore Standard Time)

<p>
First of all, it took me a long long time to understand the question, I think Leetcode should hire a better problem writer! 

Secondly, even you figured out what to do, it pretty much feels like "**if you try to solve this by a fantasy algorithm, then you will fail this. Because the most time efficient way is brutal force. (BEEP)**"

Last but not least, **I am definitely an "unhappy friend" with this question.** Does anyone feel the same?

```
public int unhappyFriends(int n, int[][] preferences, int[][] pairs) {
        int[] map = new int[n];
        for(int[] pair: pairs){ // Keep record of current matches.
            map[pair[0]] = pair[1];
            map[pair[1]] = pair[0];
        }
        int res = 0;
		
        Map<Integer, Integer>[] prefer = new Map[n]; // O(1) to fetch the index from the preference array. 
         
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n-1; j++){
                if(prefer[i] == null) prefer[i] = new HashMap<>();
                prefer[i].put(preferences[i][j], j);
            }
        }

        for(int i = 0; i < n; i++){
            for(int j : preferences[i]){
                if(prefer[j].get(i) < prefer[j].get(map[j]) 
					&& prefer[i].get(map[i]) > prefer[i].get(j)){ // Based on the definition of "unhappy"...
                    res++;
                    break;
                }
            }
        }
	    return res;
```
</p>


### O(n^2), Store preferences at the beginning
- Author: interviewrecipes
- Creation Date: Sun Sep 13 2020 12:01:21 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 14 2020 02:27:56 GMT+0800 (Singapore Standard Time)

<p>
Well, I understand the frustration. The problem could have been certainly be worded better.

Important thing to note:
**When**
`x` prefers `u` over `y`, and
`u` prefers `x` over `v`,
**Then**
Both `x` and `u` are unhappy.
NOT just `x`.

The problem statement mentions that `x` is unhappy. We have to figure out that `u` is also unhappy.

--

You have to try each pair with every other pair to identify the unhappiness. That leads to O(n^2) complexity already. So how do we optimize the inner operation to constant time so that the overall complexity remains O(n^2)?

To know whether `x` prefers `y` over `z`, we need to know their positions in `x`s list.
If you want to find the position of `y` in `x`s list, you might have to search the entire list and get the index. Similarly for `z`.
This would be O(n) and solution will become O(n^3) which will lead to TLE.

Hence, we first store the position of each friend in every other friends\' list, in a map at the beginning.


```
class Solution {

    // To efficiently know whether a person x prefers a person y or a person z, wouldn\'t 
    // it be better to have that information ready?
    // We store the index of each person in every other person\'s list in a map at the beginning.
    
    // `positions[i][j]` should be read as position of i in the list of j is positions[i][j].
    unordered_map<int, unordered_map<int, int>> positions;
    
    unordered_set<int> unhappy; // Stores unhappy people. In the end, we will return it\'s size.
public:
    void checkHappiness(int x, int y, int u, int v) {
        if (positions[u][x] < positions[y][x] &&
            positions[x][u] < positions[v][u]) {
            unhappy.insert(x);
            unhappy.insert(u);
        }
    }
    
    int unhappyFriends(int n, vector<vector<int>>& preferences, vector<vector<int>>& pairs) {
        for (int i=0; i<n; i++) {
            for (int j=0; j<n-1; j++) {
                positions[preferences[i][j]][i] = j;
            }
        }
        int n = pairs.size();
        for (int i=0; i<n-1; i++) {
            for (int j=i+1; j<n; j++) {
                int x = pairs[i][0], y = pairs[i][1], u = pairs[j][0], v = pairs[j][1];
                checkHappiness(x, y, u, v); // If x prefers u over y,  and u prefers x over v
                checkHappiness(x, y, v, u); // If x prefers v over y,  and v prefers x over u
                checkHappiness(y, x, u, v); // If y prefers u over x,  and u prefers y over v
                checkHappiness(y, x, v, u); // If y prefers v over x,  and v prefers y over u
            }
        }
        
        return unhappy.size();
    }
};
```

**Some of my other posts**
1. Leetcode 1584: https://leetcode.com/problems/min-cost-to-connect-all-points/discuss/844270
2. Leetcode 1583: https://leetcode.com/problems/count-unhappy-friends/discuss/843928
3. Leetcode 1579: https://leetcode.com/problems/remove-max-number-of-edges-to-keep-graph-fully-traversable/discuss/831506
4. Leetcode 1578: https://leetcode.com/problems/minimum-deletion-cost-to-avoid-repeating-letters/discuss/831533
5. Leetcode 1577: https://leetcode.com/problems/number-of-ways-where-square-of-number-is-equal-to-product-of-two-numbers/discuss/831548
6. Leetcode 1553: https://leetcode.com/problems/minimum-number-of-days-to-eat-n-oranges/discuss/794075
7. Leetcode 1545: https://leetcode.com/problems/find-kth-bit-in-nth-binary-string/discuss/781105
8. Leetcode 1536: https://leetcode.com/problems/minimum-swaps-to-arrange-a-binary-grid/discuss/768076
9. Leetcode 1535: https://leetcode.com/problems/find-the-winner-of-an-array-game/discuss/767958
10. Leetcode 1534: https://leetcode.com/problems/count-good-triplets/discuss/768170
11. Leetcode 1529: https://leetcode.com/problems/bulb-switcher-iv/discuss/755939
12. Leetcode 1503: https://leetcode.com/problems/last-moment-before-all-ants-fall-out-of-a-plank/discuss/720313
13. Leetcode 1488: https://leetcode.com/problems/avoid-flood-in-the-city/discuss/697687
14. Leetcode 1487: https://leetcode.com/problems/making-file-names-unique/discuss/719962
15. Leetcode 1472: https://leetcode.com/problems/design-browser-history/discuss/674486
16. Leetcode 1471: https://leetcode.com/problems/the-k-strongest-values-in-an-array/discuss/674346
17. Leetcode 1466: https://leetcode.com/problems/reorder-routes-to-make-all-paths-lead-to-the-city-zero/discuss/661710
18. Leetcode 1465: https://leetcode.com/problems/maximum-area-of-a-piece-of-cake-after-horizontal-and-vertical-cuts/discuss/661995
19. Leetcode 1458: https://leetcode.com/problems/max-dot-product-of-two-subsequences/discuss/648528
20. Leetcode 1443: https://leetcode.com/problems/minimum-time-to-collect-all-apples-in-a-tree/discuss/623673
21. Leetcode 1436: https://leetcode.com/problems/destination-city/discuss/609874
22. Leetcode 1424: https://leetcode.com/problems/diagonal-traverse-ii/discuss/597741
23. Leetcode 1423: https://leetcode.com/problems/maximum-points-you-can-obtain-from-cards/discuss/597825
24. Leetcode 1419: https://leetcode.com/problems/minimum-number-of-frogs-croaking/discuss/586653
</p>


### [Python] Clean Solution
- Author: thejoshie
- Creation Date: Sun Sep 13 2020 12:38:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 13 2020 12:48:05 GMT+0800 (Singapore Standard Time)

<p>
Create dictionary using each friend as keys and a list of people they are closer to than the person they are paired with as values. This can be done using index.

Then use nested for loop to find when people are on each other\'s list.

```
class Solution:
    def unhappyFriends(self, n: int, preferences: List[List[int]], pairs: List[List[int]]) -> int:
        
        dd = {}
        
        for i,x in pairs:
            dd[i] = preferences[i][:preferences[i].index(x)]
            dd[x] = preferences[x][:preferences[x].index(i)]
        
        ans = 0
            
        for i in dd:
            for x in dd[i]:
                if i in dd[x]:
                    ans += 1
                    break
        
        return ans
                            
                            
```
</p>


