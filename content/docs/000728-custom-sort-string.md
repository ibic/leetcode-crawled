---
title: "Custom Sort String"
weight: 728
#id: "custom-sort-string"
---
## Description
<div class="description">
<p><code>S</code> and <code>T</code> are strings composed of lowercase letters. In <code>S</code>, no letter occurs more than once.</p>

<p><code>S</code> was sorted in some custom order previously. We want to permute the characters of <code>T</code> so that they match the order that <code>S</code> was sorted. More specifically, if <code>x</code> occurs before <code>y</code> in <code>S</code>, then <code>x</code> should occur before <code>y</code> in the returned string.</p>

<p>Return any permutation of <code>T</code> (as a string) that satisfies this property.</p>

<pre>
<strong>Example :</strong>
<strong>Input:</strong> 
S = &quot;cba&quot;
T = &quot;abcd&quot;
<strong>Output:</strong> &quot;cbad&quot;
<strong>Explanation:</strong> 
&quot;a&quot;, &quot;b&quot;, &quot;c&quot; appear in S, so the order of &quot;a&quot;, &quot;b&quot;, &quot;c&quot; should be &quot;c&quot;, &quot;b&quot;, and &quot;a&quot;. 
Since &quot;d&quot; does not appear in S, it can be at any position in T. &quot;dcba&quot;, &quot;cdba&quot;, &quot;cbda&quot; are also valid outputs.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>S</code> has length at most <code>26</code>, and no character is repeated in <code>S</code>.</li>
	<li><code>T</code> has length at most <code>200</code>.</li>
	<li><code>S</code> and <code>T</code> consist of lowercase letters only.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Facebook - 6 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: true)
- Uber - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Count and Write [Accepted]

**Intuition**

Let's first write to our answer the elements of `T` that occur in `S`, in order of `S`.  After, we'll write any elements of `T` we didn't write.  This obviously keeps all the ordering relationships we wanted.

In the second write, the order doesn't matter because those elements aren't in `S`, so there are no ordering relationships these elements have to satisfy.

**Algorithm**

The trick is to count the elements of `T`.  After we have some `count[char] = (the number of occurrences of char in T)`, we can write these elements in the order we want.  The order is `S + (characters not in S in any order)`.

For more details on the algorithm, please see the inlined comments in each implementation.

<iframe src="https://leetcode.com/playground/MiQSARtb/shared" frameBorder="0" width="100%" height="500" name="MiQSARtb"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(S.\text{length} + T.\text{length})$$, the time it takes to iterate through `S` and `T`

* Space Complexity:  $$O(T.\text{length})$$.  We count at most 26 different lowercase letters, but the final answer has the same length as `T`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] one Java 10 liner, Python 6 liner and 2 liner solutions w/ comment
- Author: rock
- Creation Date: Sun Feb 25 2018 17:53:48 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 08 2020 22:31:34 GMT+0800 (Singapore Standard Time)

<p>
You can also refer to my solution: [[Java/Python 3] O(1001) code, similar to 791 Custom Sort String.](https://leetcode.com/problems/relative-sort-array/discuss/334570/JavaPython-3-O(1001)-code-similar-to-791-Custom-Sort-String.) of this problem: [1122. Relative Sort Array](https://leetcode.com/problems/relative-sort-array/description/)

**update:** Time and space: O(T.length())

**Java**

```
    public String customSortString(String S, String T) {
        int[] count = new int[26];
        for (char c : T.toCharArray()) { ++count[c - \'a\']; }  // count each char in T.
        StringBuilder sb = new StringBuilder();
        for (char c : S.toCharArray()) {                            
            while (count[c - \'a\']-- > 0) { sb.append(c); }    // sort chars both in T and S by the order of S.
        }
        for (char c = \'a\'; c <= \'z\'; ++c) {
            while (count[c - \'a\']-- > 0) { sb.append(c); }    // group chars in T but not in S.
        }
        return sb.toString();
   }
```

----

**Python 3:** learn from and therefore credit to @a1m1

```
    def customSortString(self, S: str, T: str) -> str:
        ans, cnt = [], collections.Counter(T)           # count each char in T. 
        for c in S:
            if cnt[c]: ans.extend(c * cnt.pop(c))       # sort chars both in T and S by the order of S.
        for c, v in cnt.items():
            ans.extend(c * v)                           # group chars in T but not in S.
        return \'\'.join(ans);
```
---

**Python 3 Lambda:** learn from and therefore credit to @lee215

I think the time of the following code is `O(nlogn), where n = len(T)`.

**Feel free to correct me if I am wrong.**

```
    def customSortString(self, S: str, T: str) -> str:
        d = {k : i for i, k in enumerate(S)}
        return \'\'.join(sorted(T, key=lambda k: d.get(k, len(T) + ord(k))))
```
</p>


### Two Lines C++
- Author: lailianqi
- Creation Date: Sun Feb 25 2018 12:46:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 10:45:48 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
  public:
    string customSortString(string S, string T) {
        sort(T.begin(), T.end(),
             [&](char a, char b) { return S.find(a) < S.find(b); });
        return T;
    }
};
```
The second Edition. It is easier to understand.
```
class Solution {
  public:
    string customSortString(string S, string T) {
        unordered_map<char, int> dir;
        for (int i = 0; i < S.size(); i++) {
            dir[S[i]] = i + 1;
        }
        sort(T.begin(), T.end(),
             [&](char a, char b) { return dir[a] < dir[b]; });
        return T;
    }
};
```
we also can use stl
```
class Solution {
  public:
    string customSortString(string S, string T) {
        unordered_map<char, int> dir;
        int i = 0;
        transform(S.begin(), S.end(), inserter(dir, dir.end()),
                  [&](char &a) { return make_pair(a, ++i); });
        sort(T.begin(), T.end(),
             [&](char a, char b) { return dir[a] < dir[b]; });
        return T;
    }
};
```
</p>


### Java Bucket sort solution O(N+M) with follow up questions
- Author: FLAGbigoffer
- Creation Date: Sun Feb 25 2018 14:04:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 03:16:20 GMT+0800 (Singapore Standard Time)

<p>
Use bucket sort to achieve linear time.
1. Put string T into bucket;
2. Scan each character of S and construct the result string using bucket;
3. Scan bucket again to append the rest of characters which are not existing in string S.
`Actually, this problem has two follow up questions:`
`Follow up 1: If the custom order S is too large, how to solve it efficiently?`
`Follow up 2: If the string T is too large, how to solve it efficiently?`
```
class Solution {
    public String customSortString(String S, String T) {
        int[] bucket = new int[26];
        for (char c : T.toCharArray()) {
            bucket[c - 'a']++;
        }
        
        StringBuilder sb = new StringBuilder();
        for (char c : S.toCharArray()) {
            for (int i = 0; i < bucket[c - 'a']; i++) {
                sb.append(c);
            }
            bucket[c - 'a'] = 0;
        }
        
        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < bucket[i]; j++) {
                sb.append((char) (i + 'a'));
            }
        }
        
        return sb.toString();
    }
}
```
</p>


