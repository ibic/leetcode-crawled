---
title: "Cat and Mouse"
weight: 863
#id: "cat-and-mouse"
---
## Description
<div class="description">
<p>A game on an <strong>undirected</strong> graph is played by two players, Mouse and Cat, who alternate turns.</p>

<p>The graph is given as follows: <code>graph[a]</code> is a list of all nodes <code>b</code> such that <code>ab</code> is an edge of the graph.</p>

<p>Mouse starts at node 1 and goes first, Cat starts at node 2 and goes second, and there is a Hole at node 0.</p>

<p>During each player&#39;s turn, they <strong>must</strong> travel along one&nbsp;edge of the graph that meets where they are.&nbsp; For example, if the Mouse is at node <code>1</code>, it <strong>must</strong> travel to any node in <code>graph[1]</code>.</p>

<p>Additionally, it is not allowed for the Cat to travel to the Hole (node 0.)</p>

<p>Then, the game can end in 3 ways:</p>

<ul>
	<li>If ever the Cat occupies the same node as the Mouse, the Cat wins.</li>
	<li>If ever the Mouse reaches the Hole, the Mouse wins.</li>
	<li>If ever a position is repeated (ie.&nbsp;the players are in the same position as a previous turn, and&nbsp;it is the same player&#39;s turn to move), the game is a draw.</li>
</ul>

<p>Given a <code>graph</code>, and assuming both players play optimally, return <code>1</code>&nbsp;if the game is won by Mouse, <code>2</code>&nbsp;if the game is won by Cat, and <code>0</code>&nbsp;if the game is a draw.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[2,5],[3],[0,4,5],[1,4,5],[2,3],[0,2,3]]</span>
<strong>Output: </strong><span id="example-output-1">0
<strong>Explanation:</strong>
</span>4---3---1
|&nbsp; &nbsp;|
2---5
&nbsp;\&nbsp;/
&nbsp; 0
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>3 &lt;= graph.length &lt;= 50</code></li>
	<li>It is guaranteed that <code>graph[1]</code> is non-empty.</li>
	<li>It is guaranteed that <code>graph[2]</code> contains a non-zero element.&nbsp;</li>
</ol>
</div>

</div>

## Tags
- Breadth-first Search (breadth-first-search)
- Minimax (minimax)

## Companies
- Google - 8 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Minimax / Percolate from Resolved States

**Intuition**

The state of the game can be represented as `(m, c, t)` where `m` is the location of the mouse, `c` is the location of the cat, and `t` is `1` if it is the mouse's move, else `2`.  Let's call these states *nodes*.  These states form a directed graph: the player whose turn it is has various moves which can be considered as outgoing edges from this node to other nodes.

Some of these nodes are already resolved: if the mouse is at the hole `(m = 0)`, then the mouse wins; if the cat is where the mouse is `(c = m)`, then the cat wins.  Let's say that nodes will either be colored $$\small\text{MOUSE}$$, $$\small\text{CAT}$$, or $$\small\text{DRAW}$$ depending on which player is assured victory.

As in a standard minimax algorithm, the Mouse player will prefer $$\small\text{MOUSE}$$ nodes first, $$\small\text{DRAW}$$ nodes second, and $$\small\text{CAT}$$ nodes last, and the Cat player prefers these nodes in the opposite order.

**Algorithm**

We will color each `node` marked $$\small\text{DRAW}$$ according to the following rule.  (We'll suppose the `node` has `node.turn = Mouse`: the other case is similar.)

* ("Immediate coloring"):  If there is a child that is colored $$\small\text{MOUSE}$$, then this node will also be colored $$\small\text{MOUSE}$$.

* ("Eventual coloring"):  If all children are colored $$\small\text{CAT}$$, then this node will also be colored $$\small\text{CAT}$$.

We will repeatedly do this kind of coloring until no `node` satisfies the above conditions.  To perform this coloring efficiently, we will use a queue and perform a *bottom-up percolation*:

* Enqueue any node initially colored (because the Mouse is at the Hole, or the Cat is at the Mouse.)

* For every `node` in the queue, for each `parent` of that `node`:

  * Do an immediate coloring of `parent` if you can.

  * If you can't, then decrement the side-count of the number of children marked $$\small\text{DRAW}$$.  If it becomes zero, then do an "eventual coloring" of this parent.

  * All `parents` that were colored in this manner get enqueued to the queue.

**Proof of Correctness**

Our proof is similar to a proof that minimax works.

Say we cannot color any nodes any more, and say from any node colored $$\small\text{CAT}$$ or $$\small\text{MOUSE}$$ we need at most $$K$$ moves to win.  If say, some node marked $$\small\text{DRAW}$$ is actually a win for Mouse, it must have been with $$> K$$ moves.  Then, a path along optimal play (that tries to prolong the loss as long as possible) must arrive at a node colored $$\small\text{MOUSE}$$ (as eventually the Mouse reaches the Hole.)  Thus, there must have been some transition $$\small\text{DRAW} \rightarrow \small\text{MOUSE}$$ along this path.

If this transition occurred at a `node` with `node.turn = Mouse`, then it breaks our immediate coloring rule.  If it occured with `node.turn = Cat`, and all children of `node` have color $$\small\text{MOUSE}$$, then it breaks our eventual coloring rule.  If some child has color $$\small\text{CAT}$$, then it breaks our immediate coloring rule.  Thus, in this case `node` will have some child with $$\small\text{DRAW}$$, which breaks our optimal play assumption, as moving to this child ends the game in $$> K$$ moves, whereas moving to the colored neighbor ends the game in $$\leq K$$ moves.

<iframe src="https://leetcode.com/playground/bC3g72rg/shared" frameBorder="0" width="100%" height="500" name="bC3g72rg"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^3)$$, where $$N$$ is the number of nodes in the graph.  There are $$O(N^2)$$ states, and each state has an outdegree of $$N$$, as there are at most $$N$$ different moves.

* Space Complexity:  $$O(N^2)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### When you see this problem during an interview...
- Author: yuxiong
- Creation Date: Sat Jan 19 2019 15:30:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jan 19 2019 15:30:39 GMT+0800 (Singapore Standard Time)

<p>
If I see this problem during an interview, it should imply that the interviewer doesn\'t want to hire me for some reason. So I\'d better say goodbye and gently close the door...
</p>


### Most of the DFS solutions are WRONG, check this case
- Author: wangzi6147
- Creation Date: Mon Oct 01 2018 03:33:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 03:18:47 GMT+0800 (Singapore Standard Time)

<p>
Basically the DFS with memo solutions generate status nodes like `(cat, mouse, turn)`. Before DFS to next level, we mark current node to `0` which means `Draw`. However, there might be **cycles** in the `status graph`.

i.e, We mark current node to `Draw`. Then we go into the cycle and mark all nodes in the cycle to `Draw`. However, the right answer to current node could be `Cat` or `Mouse`, this can only be figured out in the later DFS. But the nodes in the cycle have already been marked to `Draw` mistakenly.

Check this case:
[[6],[4],[9],[5],[1,5],[3,4,6],[0,5,10],[8,9,10],[7],[2,7],[6,7]]
![image](https://assets.leetcode.com/users/wangzi6147/image_1538335324.png)

We will first get a `Draw` in node `(7, 5, mouse)` then later we use this information to mark `(9, 5, cat)` to `Draw`. However `(9, 5, cat)` should be `Mouse`.

I believe the right solution should be using Topological traversal and coloring like the post in [solution](https://leetcode.com/problems/cat-and-mouse/solution/) to solve the cycle problem. Also I noticed that many people used DP to solve this problem correctly but I don\'t understand those solutions. Can anyone help?

This test case was generated manually. Correct me if I\'m wrong.

The idea of Topological traversal is start from each ending status, topologically traversal to color the previous status.

Similar to the solution, my code for reference:

```
class Solution {
    public int catMouseGame(int[][] graph) {
        int n = graph.length;
        // (cat, mouse, mouseMove = 0)
        int[][][] color = new int[n][n][2];
        int[][][] outdegree = new int[n][n][2];
        for (int i = 0; i < n; i++) { // cat
            for (int j = 0; j < n; j++) { // mouse
                outdegree[i][j][0] = graph[j].length;
                outdegree[i][j][1] = graph[i].length;
                for (int k : graph[i]) {
                    if (k == 0) {
                        outdegree[i][j][1]--;
                        break;
                    }
                }
            }
        }
        Queue<int[]> q = new LinkedList<>();
        for (int k = 1; k < n; k++) {
            for (int m = 0; m < 2; m++) {
                color[k][0][m] = 1;
                q.offer(new int[]{k, 0, m, 1});
                color[k][k][m] = 2;
                q.offer(new int[]{k, k, m, 2});
            }
        }
        while (!q.isEmpty()) {
            int[] cur = q.poll();
            int cat = cur[0], mouse = cur[1], mouseMove = cur[2], c = cur[3];
            if (cat == 2 && mouse == 1 && mouseMove == 0) {
                return c;
            }
            int prevMouseMove = 1 - mouseMove;
            for (int prev : graph[prevMouseMove == 1 ? cat : mouse]) {
                int prevCat = prevMouseMove == 1 ? prev : cat;
                int prevMouse = prevMouseMove == 1 ? mouse : prev;
                if (prevCat == 0) {
                    continue;
                }
                if (color[prevCat][prevMouse][prevMouseMove] > 0) {
                    continue;
                }
                if (prevMouseMove == 1 && c == 2 || prevMouseMove == 0 && c == 1
                    || --outdegree[prevCat][prevMouse][prevMouseMove] == 0) {
                    color[prevCat][prevMouse][prevMouseMove] = c;
                    q.offer(new int[]{prevCat, prevMouse, prevMouseMove, c});
                }
            }
        }
        return color[2][1][0];
    }
}
```
</p>


### Java solution with bug, updated on 02/02/2019
- Author: Goku_Wukong
- Creation Date: Mon Oct 15 2018 14:55:53 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 14:25:58 GMT+0800 (Singapore Standard Time)

<p>
Updated on 02/02/2019.
Thanks for @sguaaa pointting out bug.   If there are two cycles on both end, the code failed on: [[6],[4,11],[9,12],[5],[1,5,11],[3,4,6],[0,5,10],[8,9,10],[7],[2,7,12],[6,7],[1,4],[2,9]]. 
I guess the problem is I preallocate ```dp[mouse][cat] = 0``` at the beginning of each iteration, which may not right. 

I feel exhausted after spending hours on investigating this problem but still no conclution.  Anyone has idea on how to fix the code?   
Thank you for discussion!
```
class Solution {
    public int catMouseGame(int[][] graph) {
        int size = graph.length;
        int dp[][] = new int[size][size];
        for (int i = 0; i < size; i++)
            Arrays.fill(dp[i], -1);  // unvisited

        for (int i = 1; i < size; ++i) {
            dp[0][i] = 1;   // mouse reached home, m win
            dp[i][i] = 2;   // cat met mouse, cat win
        }

        return helper(graph, 1, 2, dp);
    }

    public int helper(int[][] graph, int mouse, int cat, int dp[][]) {

        if (dp[mouse][cat] != -1)
            return dp[mouse][cat];  // use cached value

        dp[mouse][cat] = 0;  // if there is a cycle, draw
        int mouseDefault = 2;  //  default cat win, try to update this number to 1 or 0
        int[] mouseGoList = graph[mouse], catGoList = graph[cat];

        for (int mouseGo : mouseGoList) {
            if (mouseGo == cat)
                continue;   // I\'m a mouse, why go for a cat?

            int catDefault = 1;  //  default mouse win, try to update this number to 2 or 0
            for (int catGo : catGoList) {
                if (catGo == 0)
                    continue;  // cannot go to hole
                int next = helper(graph, mouseGo, catGo, dp);
                if (next == 2) {   // if cat win in this path, no need to continue
                    catDefault = 2;
                    break;
                }
                if (next == 0) {   // at least it\'s a draw
                    catDefault = 0;
                }
            }

            if (catDefault == 1) {  // if mouse can win in this path, no need to continue
                mouseDefault = 1;
                break;
            }
            if (catDefault == 0) {  // at least it\'s a draw
                mouseDefault = 0;
            }
        }
        dp[mouse][cat] = mouseDefault;
        return dp[mouse][cat];
    }
}
```
</p>


