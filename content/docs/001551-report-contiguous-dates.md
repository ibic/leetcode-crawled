---
title: "Report Contiguous Dates"
weight: 1551
#id: "report-contiguous-dates"
---
## Description
<div class="description">
<p>Table: <code>Failed</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| fail_date    | date    |
+--------------+---------+
Primary key for this table is fail_date.
Failed table contains the days of failed tasks.
</pre>

<p>Table: <code>Succeeded</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| success_date | date    |
+--------------+---------+
Primary key for this table is success_date.
Succeeded table contains the days of succeeded tasks.
</pre>

<p>&nbsp;</p>

<p>A system is running one task <strong>every day</strong>. Every task is independent of the previous tasks. The tasks can fail or succeed.</p>

<p>Write an SQL query to generate a report of&nbsp;<code>period_state</code> for each continuous interval of days in the period from&nbsp;<strong>2019-01-01</strong> to <strong>2019-12-31</strong>.</p>

<p><code>period_state</code> is <em>&#39;failed&#39;&nbsp;</em>if tasks in this interval failed or <em>&#39;succeeded&#39;</em>&nbsp;if tasks in this interval succeeded. Interval of days are retrieved as <code>start_date</code> and <code>end_date.</code></p>

<p>Order result by <code>start_date</code>.</p>

<p>The query result format is in the following example:</p>

<pre>
Failed table:
+-------------------+
| fail_date         |
+-------------------+
| 2018-12-28        |
| 2018-12-29        |
| 2019-01-04        |
| 2019-01-05        |
+-------------------+

Succeeded table:
+-------------------+
| success_date      |
+-------------------+
| 2018-12-30        |
| 2018-12-31        |
| 2019-01-01        |
| 2019-01-02        |
| 2019-01-03        |
| 2019-01-06        |
+-------------------+


Result table:
+--------------+--------------+--------------+
| period_state | start_date   | end_date     |
+--------------+--------------+--------------+
| succeeded    | 2019-01-01   | 2019-01-03   |
| failed       | 2019-01-04   | 2019-01-05   |
| succeeded    | 2019-01-06   | 2019-01-06   |
+--------------+--------------+--------------+

The report ignored the system state in 2018 as we care about the system in the period 2019-01-01 to 2019-12-31.
From 2019-01-01 to 2019-01-03 all tasks succeeded and the system state was &quot;succeeded&quot;.
From 2019-01-04 to 2019-01-05 all tasks failed and system state was &quot;failed&quot;.
From 2019-01-06 to 2019-01-06 all tasks succeeded and system state was &quot;succeeded&quot;.
</pre>

</div>

## Tags


## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MSSQL 100% Simple solution
- Author: vinodismyname
- Creation Date: Thu Oct 17 2019 23:49:11 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 17 2019 23:49:11 GMT+0800 (Singapore Standard Time)

<p>
The idea here is to use row_number to get a unique grouping label for each continous sequence. 
We can then easily find the min/max dates in each group
```
with a  as (
(select fail_date as date,
       \'failed\' as period_state
       from failed)
union all
 
 (select success_date as date,
         \'succeeded\' as period_state
         from succeeded)
    ),
    
  b as (    
select date,
       period_state,
       row_number() over (order by period_state, date asc) as seq
   from a where date between \'2019-01-01\' and \'2019-12-31\'
         ),

 c as (
select date, period_state,seq, dateadd(d, -seq, date) as seqStart from b
)

select period_state, min(date) as start_date, max(date) as end_date from c
group by seqStart,period_state
order by start_date asc


```
</p>


### MYSQL with Single subquery
- Author: njirafe
- Creation Date: Mon Jun 08 2020 03:13:07 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 08 2020 03:13:07 GMT+0800 (Singapore Standard Time)

<p>
```
WITH combined as 
(
     SELECT 
        fail_date as dt, 
        \'failed\' as period_state,
        DAYOFYEAR(fail_date) - row_number() over(ORDER BY fail_date) as period_group 
     FROM 
        Failed
     WHERE fail_date BETWEEN \'2019-01-01\' AND \'2019-12-31\'
     UNION ALL
     SELECT 
        success_date as dt, 
        \'succeeded\' as period_state,
        DAYOFYEAR(success_date) - row_number() over(ORDER BY success_date) as period_group 
     FROM Succeeded
     WHERE success_date BETWEEN \'2019-01-01\' AND \'2019-12-31\'  
)

SELECT 
    period_state,
    min(dt) as start_date,
    max(dt) as end_date
FROM
        combined
GROUP BY period_state,period_group
ORDER BY start_date
```
</p>


### Concise MySQL solution using variables
- Author: hegupta
- Creation Date: Fri Oct 18 2019 14:23:00 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 18 2019 14:23:00 GMT+0800 (Singapore Standard Time)

<p>
```
select s as period_state, min(ed) as start_date, max(ed) as end_date from
(select s, ed,
    @rank := case when @prev = s then @rank else @rank + 1 end as rank,
    @prev := s as prev
from
(select * from
(select fail_date as ed, "failed" as s from Failed
union all
select success_date as ed, "succeeded" as s from Succeeded) a
where ed between \'2019-01-01\' and \'2019-12-31\' order by ed asc) b,
(select @rank := 0, @prev := "unknown") c) d
group by d.rank
order by start_date asc
```
</p>


