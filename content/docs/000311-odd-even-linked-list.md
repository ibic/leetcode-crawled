---
title: "Odd Even Linked List"
weight: 311
#id: "odd-even-linked-list"
---
## Description
<div class="description">
<p>Given a singly linked list, group all odd nodes together followed by the even nodes. Please note here we are talking about the node number and not the value in the nodes.</p>

<p>You should try to do it in place. The program should run in O(1) space complexity and O(nodes) time complexity.</p>

<p><b>Example 1:</b></p>

<pre>
<strong>Input: </strong><code>1-&gt;2-&gt;3-&gt;4-&gt;5-&gt;NULL</code>
<strong>Output: </strong><code>1-&gt;3-&gt;5-&gt;2-&gt;4-&gt;NULL</code>
</pre>

<p><b>Example 2:</b></p>

<pre>
<strong>Input: </strong>2<code>-&gt;1-&gt;3-&gt;5-&gt;6-&gt;4-&gt;7-&gt;NULL</code>
<strong>Output: </strong><code>2-&gt;3-&gt;6-&gt;7-&gt;1-&gt;5-&gt;4-&gt;NULL</code>
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The relative order inside both the even and odd groups should remain as it was in the input.</li>
	<li>The first node is considered odd, the second node even and so on ...</li>
	<li>The length of the linked list is between <code>[0, 10^4]</code>.</li>
</ul>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Facebook - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Capital One - 14 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)

## Official Solution
## Solution

**Intuition**

Put the odd nodes in a linked list and the even nodes in another. Then link the evenList to the tail of the oddList.

**Algorithm**

The solution is very intuitive. But it is not trivial to write a concise and bug-free code.

A well-formed `LinkedList` need two pointers head and tail to support operations at both ends. The variables `head` and `odd` are the head pointer and tail pointer of one `LinkedList` we call oddList; the variables `evenHead` and `even` are the head pointer and tail pointer of another `LinkedList` we call evenList. The algorithm traverses the original LinkedList and put the odd nodes into the oddList and the even nodes into the evenList. To traverse a LinkedList we need at least one pointer as an iterator for the current node. But here the pointers `odd` and `even` not only serve as the tail pointers but also act as the iterators of the original list.

The best way of solving any linked list problem is to visualize it either in your mind or on a piece of paper. An illustration of our algorithm is following:

![Illustration of odd even linked list](../Figures/328_Odd_Even.svg "Odd Even Linked List"){:width="539px"}
{:align="center"}

*Figure 1. Step by step example of the odd and even linked list.*
{:align="center"}


<iframe src="https://leetcode.com/playground/hwsGSV9j/shared" frameBorder="0" width="100%" height="293" name="hwsGSV9j"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. There are total $$n$$ nodes and we visit each node once.

* Space complexity : $$O(1)$$. All we need is the four pointers.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple O(N) time, O(1), space Java solution.
- Author: harrison6
- Creation Date: Sat Jan 16 2016 12:25:20 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 05:10:48 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
    public ListNode oddEvenList(ListNode head) {
        if (head != null) {
        
            ListNode odd = head, even = head.next, evenHead = even; 
        
            while (even != null && even.next != null) {
                odd.next = odd.next.next; 
                even.next = even.next.next; 
                odd = odd.next;
                even = even.next;
            }
            odd.next = evenHead; 
        }
        return head;
    }}
</p>


### Straigntforward Java solution, O(1) space, O(n) time
- Author: yangneu2015
- Creation Date: Sat Jan 16 2016 10:53:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jan 04 2020 03:47:39 GMT+0800 (Singapore Standard Time)

<p>
(Updated with better format.)

```
public ListNode oddEvenList(final ListNode head) {
	if (head == null || head.next == null) {
		return head;
	}

	ListNode odd = head;
	final ListNode eHead = head.next;
	ListNode even = eHead;
	while (even != null && even.next != null) {
		odd.next = even.next;
		odd = odd.next;
		even.next = odd.next;
		even = even.next;
	}

	odd.next = eHead;
	return head;
}
```
</p>


### Simple C++ solution, O(n) time, O(1) space
- Author: csk
- Creation Date: Thu Jan 21 2016 02:44:37 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 22 2018 21:40:18 GMT+0800 (Singapore Standard Time)

<p>
        ListNode* oddEvenList(ListNode* head) 
        {
            if(!head) return head;
            ListNode *odd=head, *evenhead=head->next, *even = evenhead;
            while(even && even->next)
            {
                odd->next = odd->next->next;
                even->next = even->next->next;
                odd = odd->next;
                even = even->next;
            }
            odd->next = evenhead;
            return head;
        }
</p>


