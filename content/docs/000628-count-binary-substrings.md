---
title: "Count Binary Substrings"
weight: 628
#id: "count-binary-substrings"
---
## Description
<div class="description">
<p>Give a string <code>s</code>, count the number of non-empty (contiguous) substrings that have the same number of 0's and 1's, and all the 0's and all the 1's in these substrings are grouped consecutively. 
</p>
<p>Substrings that occur multiple times are counted the number of times they occur.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> "00110011"
<b>Output:</b> 6
<b>Explanation:</b> There are 6 substrings that have equal number of consecutive 1's and 0's: "0011", "01", "1100", "10", "0011", and "01".
<br>Notice that some of these substrings repeat and are counted the number of times they occur.
<br>Also, "00110011" is not a valid substring because <b>all</b> the 0's (and 1's) are not grouped together.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> "10101"
<b>Output:</b> 4
<b>Explanation:</b> There are 4 substrings: "10", "01", "10", "01" that have equal number of consecutive 1's and 0's.
</pre>
</p>

<p><b>Note:</b>
<li><code>s.length</code> will be between 1 and 50,000.</li>
<li><code>s</code> will only consist of "0" or "1" characters.</li>
</p>
</div>

## Tags
- String (string)

## Companies
- Helix - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Group By Character [Accepted]

**Intuition**

We can convert the string `s` into an array `groups` that represents the length of same-character contiguous blocks within the string.  For example, if `s = "110001111000000"`, then `groups = [2, 3, 4, 6]`.

For every binary string of the form `'0' * k + '1' * k` or `'1' * k + '0' * k`, the middle of this string must occur between two groups.  

Let's try to count the number of valid binary strings between `groups[i]` and `groups[i+1]`.  If we have `groups[i] = 2, groups[i+1] = 3`, then it represents either `"00111"` or `"11000"`.  We clearly can make `min(groups[i], groups[i+1])` valid binary strings within this string.  Because the binary digits to the left or right of this string must change at the boundary, our answer can never be larger.

**Algorithm**

Let's create `groups` as defined above.  The first element of `s` belongs in it's own group.  From then on, each element either doesn't match the previous element, so that it starts a new group of size 1; or it does match, so that the size of the most recent group increases by 1.

Afterwards, we will take the sum of `min(groups[i-1], groups[i])`.

**Python**
```python
class Solution(object):
    def countBinarySubstrings(self, s):
        groups = [1]
        for i in xrange(1, len(s)):
            if s[i-1] != s[i]:
                groups.append(1)
            else:
                groups[-1] += 1

        ans = 0
        for i in xrange(1, len(groups)):
            ans += min(groups[i-1], groups[i])
        return ans
```

*Alternate Implentation*
```python
class Solution(object):
    def countBinarySubstrings(self, s):
        groups = [len(list(v)) for _, v in itertools.groupby(s)]
        return sum(min(a, b) for a, b in zip(groups, groups[1:]))
```

**Java**
```java
class Solution {
    public int countBinarySubstrings(String s) {
        int[] groups = new int[s.length()];
        int t = 0;
        groups[0] = 1;
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i-1) != s.charAt(i)) {
                groups[++t] = 1;
            } else {
                groups[t]++;
            }
        }

        int ans = 0;
        for (int i = 1; i <= t; i++) {
            ans += Math.min(groups[i-1], groups[i]);
        }
        return ans;
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `s`.  Every loop is through $$O(N)$$ items with $$O(1)$$ work inside the for-block.

* Space Complexity: $$O(N)$$, the space used by `groups`.

---
#### Approach #2: Linear Scan [Accepted]

**Intuition and Algorithm**

We can amend our *Approach #1* to calculate the answer on the fly.  Instead of storing `groups`, we will remember only `prev = groups[-2]` and `cur = groups[-1]`.  Then, the answer is the sum of `min(prev, cur)` over each different final `(prev, cur)` we see.

**Python**
```python
class Solution(object):
    def countBinarySubstrings(self, s):
        ans, prev, cur = 0, 0, 1
        for i in xrange(1, len(s)):
            if s[i-1] != s[i]:
                ans += min(prev, cur)
                prev, cur = cur, 1
            else:
                cur += 1

        return ans + min(prev, cur)
```

**Java**
```java
class Solution {
    public int countBinarySubstrings(String s) {
        int ans = 0, prev = 0, cur = 1;
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i-1) != s.charAt(i)) {
                ans += Math.min(prev, cur);
                prev = cur;
                cur = 1;
            } else {
                cur++;
            }
        }
        return ans + Math.min(prev, cur);
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `s`.  Every loop is through $$O(N)$$ items with $$O(1)$$ work inside the for-block.

* Space Complexity: $$O(1)$$, the space used by `prev`, `cur`, and `ans`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy and Concise with Explanation
- Author: lee215
- Creation Date: Sun Oct 15 2017 11:05:45 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 10 2020 15:21:04 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
First, I count the number of 1 or 0 grouped consecutively.
For example "0110001111" will be ```[1, 2, 3, 4]```.

Second, for any possible substrings with 1 and  0 grouped consecutively, the number of valid substring will be the minimum number of 0 and 1.
For example "0001111", will be ```min(3, 4) = 3```,  (```"01", "0011", "000111"```)
<br>

# Complexity
Time `O(N)`
Space `O(1)`
<br>

**Java:**:
```java
    public int countBinarySubstrings(String s) {
        int cur = 1, pre = 0, res = 0;
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == s.charAt(i - 1)) cur++;
            else {
                res += Math.min(cur, pre);
                pre = cur;
                cur = 1;
            }
        }
        return res + Math.min(cur, pre);
    }
```
**C++:**
```cpp
    int countBinarySubstrings(string s) {
        int cur = 1, pre = 0, res = 0;
        for (int i = 1; i < s.size(); i++) {
            if (s[i] == s[i - 1]) cur++;
            else {
                res += min(cur, pre);
                pre = cur;
                cur = 1;
            }
        }
        return res + min(cur, pre);
    }
```
**Python:**
O(n) space
```py
    def countBinarySubstrings(self, s):
        s = map(len, s.replace(\'01\', \'0 1\').replace(\'10\', \'1 0\').split())
        return sum(min(a, b) for a, b in zip(s, s[1:]))
```
</p>


### Java O(n) Time O(1) Space
- Author: compton_scatter
- Creation Date: Sun Oct 15 2017 11:00:25 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 25 2018 12:55:52 GMT+0800 (Singapore Standard Time)

<p>
```
public int countBinarySubstrings(String s) {
    int prevRunLength = 0, curRunLength = 1, res = 0;
    for (int i=1;i<s.length();i++) {
        if (s.charAt(i) == s.charAt(i-1)) curRunLength++;
        else {
            prevRunLength = curRunLength;
            curRunLength = 1;
        }
        if (prevRunLength >= curRunLength) res++;
    }
    return res;
}
```
</p>


### Acceptable JAVA solution with explaination
- Author: jinleec
- Creation Date: Tue Oct 17 2017 04:44:26 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 05:48:08 GMT+0800 (Singapore Standard Time)

<p>
It takes me some time to understand this problem, after look at the top solution. I figured out how to solve it, Thanks to @compton_scatter, here is just some explaination of his solution: 
1. preRun count the same item happend before (let say you have 0011, preRun = 2 when you hit the first 1, means there are two zeros before first '1')
2. curRun count the current number of items (let say you have 0011, curRun = 2 when you hit the second 1, means there are two 1s so far)
3. Whenever item change (from 0 to 1 or from 1 to 0), preRun change to curRun, reset curRun to 1 (store the curRun number into PreRun, reset curRun)
4. Every time preRun >= curRun means there are more 0s before 1s, so could do count++ . (This was the tricky one, ex. 0011 when you hit the first '1', curRun = 1, preRun = 2, means 0s number is larger than 1s number, so we could form "01" at this time, count++ .  When you hit the second '1', curRun = 2, preRun = 2, means 0s' number equals to 1s' number, so we could form "0011" at this time, that is why count++)
```
class Solution {
public int countBinarySubstrings(String s) {
if (s == null || s.length() == 0) return 0;
        int preRun = 0;
        int curRun =1;
        int count = 0;
        for (int i = 1; i < s.length(); i++){
            if (s.charAt(i) == s.charAt(i-1)) curRun++;
            else {
                preRun = curRun;
                curRun = 1;
            }
            if (preRun >= curRun) count++;
        }
        return count;
    }
}
```
</p>


