---
title: "Binary Tree Tilt"
weight: 528
#id: "binary-tree-tilt"
---
## Description
<div class="description">
<p>Given a binary tree, return the tilt of the <b>whole tree</b>.</p>

<p>The tilt of a <b>tree node</b> is defined as the <b>absolute difference</b> between the sum of all left subtree node values and the sum of all right subtree node values. Null node has tilt 0.</p>

<p>The tilt of the <b>whole tree</b> is defined as the sum of all nodes' tilt.</p>

<p><b>Example:</b><br />
<pre>
<b>Input:</b> 
         1
       /   \
      2     3
<b>Output:</b> 1
<b>Explanation:</b> 
Tilt of node 2 : 0
Tilt of node 3 : 0
Tilt of node 1 : |2-3| = 1
Tilt of binary tree : 0 + 0 + 1 = 1
</pre>
</p>

<p><b>Note:</b>
<ol>
<li>The sum of node values in any subtree won't exceed the range of 32-bit integer. </li>
<li>All the tilt values won't exceed the range of 32-bit integer.</li>
</ol>
</p>
</div>

## Tags
- Tree (tree)

## Companies
- Indeed - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


## Solution

---
#### Approach 1: Using Recursion

**Algorithm**

From the problem statement, it is clear that we need to find the tilt value at every node of the given tree and add up all the tilt values to obtain the final result. To find the tilt value at any node, we need to subtract the sum of all the nodes in its left subtree and the sum of all the nodes in its right subtree. 

Thus, to find the solution, we make use of a recursive function `traverse` which when called from any node, returns the sum of the nodes below the current node including itself. With the help of such sum values for the right and left subchild of any node, we can directly obtain the tilt value corresponding to that node.

The below animation depicts how the value passing and tilt calculation:

!?!../Documents/563_Binary.json:1000,563!?!

<iframe src="https://leetcode.com/playground/tHFCZYff/shared" frameBorder="0" width="100%" height="480" name="tHFCZYff"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. where $$n$$ is the number of nodes. Each node is visited once.
* Space complexity : $$O(n)$$. In worst case when the tree is skewed depth of tree will be $$n$$. In average case depth will be $$\log n$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Insufficient example. Please read left + right + node.value
- Author: igornovik
- Creation Date: Mon Apr 29 2019 07:22:01 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 29 2019 07:22:01 GMT+0800 (Singapore Standard Time)

<p>
I think that explanation and example provided are not sufficient. You need to have a biger tree to explain this problem:

Input :
     4
     / \\
   2   9
   / \     \\
 3   5   7
Output : 15
Explanation: 
Tilt of node 3 : 0
Tilt of node 5 : 0
Tilt of node 7 : 0
Tilt of node 2 : |3-5| = 2
Tilt of node 9 : |0-7| = 7
Tilt of node 4 : |(3+5+2)-(9+7)| = 6
Tilt of binary tree : 0 + 0 + 0 + 2 + 7 + 6 = 15

</p>


### Java Solution, post-order traversal
- Author: shawngao
- Creation Date: Sun Apr 23 2017 11:09:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 15:08:22 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    int result = 0;
    
    public int findTilt(TreeNode root) {
        postOrder(root);
        return result;
    }
    
    private int postOrder(TreeNode root) {
        if (root == null) return 0;
        
        int left = postOrder(root.left);
        int right = postOrder(root.right);
        
        result += Math.abs(left - right);
        
        return left + right + root.val;
    }
}
```
</p>


### Python, Simple with Explanation
- Author: awice
- Creation Date: Sun Apr 23 2017 11:09:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 11:18:48 GMT+0800 (Singapore Standard Time)

<p>
If we had each node's subtree sum, our answer would look like this psuedocode:  ```for each node: ans += abs(node.left.subtreesum - node.right.subtreesum)```.  Let ```_sum(node)``` be the node's subtree sum.  We can find it by adding the subtree sum of the left child, plus the subtree sum of the right child, plus the node's value.  While we are visiting the node (each node is visited exactly once), we might as well do the ```ans += abs(left_sum - right_sum)``` part.
```
def findTilt(self, root):
    self.ans = 0
    def _sum(node):
        if not node: return 0
        left, right = _sum(node.left), _sum(node.right)
        self.ans += abs(left - right)
        return node.val + left + right
    _sum(root)
    return self.ans
```
</p>


