---
title: "Two Sum BSTs"
weight: 1055
#id: "two-sum-bsts"
---
## Description
<div class="description">
<p>Given two binary search trees, return <code>True</code>&nbsp;if and only if there is a node in the first tree and a node in the second tree whose values&nbsp;sum up to a given integer&nbsp;<code>target</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/05/31/1368_1_a2.png" style="width: 150px; height: 140px;" /><img alt="" src="https://assets.leetcode.com/uploads/2019/05/31/1368_1_b.png" style="width: 150px; height: 136px;" /></strong></p>

<pre>
<strong>Input:</strong> root1 = [2,1,4], root2 = [1,0,3], target = 5
<strong>Output:</strong> true
<strong>Explanation: </strong>2 and 3 sum up to 5.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/05/31/1368_2_a.png" style="width: 150px; height: 137px;" /><img alt="" src="https://assets.leetcode.com/uploads/2019/05/31/1368_2_b.png" style="width: 150px; height: 168px;" /></strong></p>

<pre>
<strong>Input:</strong> root1 = [0,-10,10], root2 = [5,1,7,0,2], target = 18
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>Each tree has at most <code>5000</code> nodes.</li>
	<li><code>-10^9 &lt;= target, node.val &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Binary Search Tree (binary-search-tree)

## Companies
- Amazon - 5 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Overview

This problem is a combination of two other problems:

- [Inorder Traversal of BST](https://leetcode.com/articles/recover-binary-search-tree/).

- [Two Sum](https://leetcode.com/articles/two-sum/). 

You may want to check these articles first, or to continue to read having in mind 
that everything will be explained a bit shorter.

**Solution Pattern**

> Two sum problems can be solved by using hashset of complements. Complement = 
target - element.

The idea is simple: 

- Traverse the first tree, and store the complements (target - val) 
of all node values in a hashset. 

- Traverse the second tree and check if any of its elements exists in the hashset.
If yes - return True. If no - return False.  

![traversal](../Figures/1214/recursive.png)

**Prerequisites: How to traverse BST**

The only remaining question is how to traverse BST.
The best choice for BST is usually the DFS inorder traversal:
 
- Recursive inorder traversal is the simplest one to write, 
it's one liner in Python and 5-liner in Java.

- Iterative inorder traversal has the best time performance.  

Here are some reminders about the DFS inorder traversal:

1. There are three DFS ways to traverse the tree: _preorder_, _postorder_ and _inorder_.
Please check two minutes picture explanation, if you don't remember them quite well:
[here is Python version](https://leetcode.com/problems/binary-tree-inorder-traversal/discuss/283746/all-dfs-traversals-preorder-inorder-postorder-in-python-in-1-line) 
and 
[here is Java version](https://leetcode.com/problems/binary-tree-inorder-traversal/discuss/328601/all-dfs-traversals-preorder-postorder-inorder-in-java-in-5-lines).

2. > The result of the DFS inorder traversal of BST is an array sorted in the ascending order.

3. To compute inorder traversal follow the direction `Left -> Node -> Right`.

<iframe src="https://leetcode.com/playground/GYcYvBpv/shared" frameBorder="0" width="100%" height="174" name="GYcYvBpv"></iframe>

![traversal](../Figures/1214/inorder.png)
<br />
<br />


---
#### Approach 1: Recursive Inorder Traversal

Let's start with the simplest solution: 

- Traverse the first tree using recursive inorder traversal. 
Store the complements (target - val) of all node values in a hashset. 

- Traverse the second tree using recursive inorder traversal. 
Check if any of its elements exists in the hashset.
If yes - return True. If no - return False. 

Both steps are done with recursive functions:

- `in_hashset` to build hashset of complements (target - val) 
while traversing the first tree.

- `in_check` to check if at least one element of the second tree exists in hashset.

These functions are slightly modified versions of 
recursive inorder traversal presented in the overview.   

**Implementation**

![traversal](../Figures/1214/recursive.png)

<iframe src="https://leetcode.com/playground/hXMNcnjz/shared" frameBorder="0" width="100%" height="378" name="hXMNcnjz"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N_1 + N_2)$$, 
where $$N_1$$ and $$N_2$$ are the numbers of nodes in 
the first and the second tree respectively.
    
* Space complexity: $$\mathcal{O}(2 \times N_1 + N_2)$$,
$$N_1$$ to keep the hashset and up to $$N_1 + N_2$$ for the recursive stacks. 
<br />
<br />


---
#### Approach 2: Iterative Inorder Traversal

**Intuition**

The drawback of the recursive approach is that one has to 
traverse the entire second tree, even if it's not really needed.

For example, if `root2.val` value is already present in the hashset, 
there is no need to traverse further, one could stop immediately and return True. 

![traversal](../Figures/1214/it2.png)  

That could be implemented with the help of iterative traversal. 

> Iterative inorder traversal is simple: go to the left as far as you can,
pushing nodes in the stack. Then pop one node out of stack and do one step to the right. 
Repeat till the end of nodes in the tree and in the stack.

**Algorithm**

- Do iterative inorder traversal of the first tree to build hashset of complements 
(target - val). 
     
- Do iterative inorder traversal of the second tree to check 
if at least one element of the second tree is in hashset. 
Stop the traversal and return True once you find such an element.

- We are here because tree2 doesn't contain any complement. 
Return False.

**Implementation**

!?!../Documents/1214_LIS.json:1000,460!?!

<iframe src="https://leetcode.com/playground/7dJJ4VPJ/shared" frameBorder="0" width="100%" height="500" name="7dJJ4VPJ"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N_1 + N_2)$$, 
where $$N_1$$ and $$N_2$$ are the numbers of nodes in 
the first and the second tree respectively.

* Space complexity: $$\mathcal{O}(N_1 + \max(N_1, N_2))$$,
$$N_1$$ to keep the hashset and up to $$\max(N_1, N_2)$$ for the stack.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Stack Solution
- Author: Poorvank
- Creation Date: Sun Oct 06 2019 00:05:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 02:14:39 GMT+0800 (Singapore Standard Time)

<p>
Traverse root 1 from smallest value to node to largest.
Traverse root 2 from largest value node to smallest.
Sum up the corresponding node\u2019s value : If sum == target return true
 If target > sum,
 then move to the **inorder successor** of the current node of root1,
 else
 move to the **inorder predecessor** of the current node of root2.

```
public boolean twoSumBSTs(TreeNode root1, TreeNode root2, int target) {
        // if either of the tree is empty
        if (root1 == null || root2 == null)
            return false;

        // stack \'stack1\' used for the inorde traversal of root 1
        // stack \'stack2\' used for the reverse inorder traversal of root 2
        Stack<TreeNode> stack1 = new Stack<>();
        Stack<TreeNode> stack2 = new Stack<>();
        TreeNode t1, t2;

        while (true) {
            // LeftMost Node.
            while (root1 != null) {
                stack1.push(root1);
                root1 = root1.left;
            }
            // RighMost Node.
            while (root2 != null) {
                stack2.push(root2);
                root2 = root2.right;
            }
            // If either is empty then break.
            if (stack1.empty() || stack2.empty())
                break;

            t1 = stack1.peek();
            t2 = stack2.peek();

            // if the sum of the node\'s is equal to \'target\'
            if ((t1.val + t2.val) == target) {
                return true;
            }

            // move to next possible node in the inorder traversal of root 1
            else if ((t1.val + t2.val) < target) {
                stack1.pop();
                root1 = t1.right;
            }

            // move to next possible node in the reverse inorder traversal of root 2
            else {
                stack2.pop();
                root2 = t2.left;
            }
        }

        return false;
    }
```
</p>


### Java recursive solution in 5 lines
- Author: Sun_WuKong
- Creation Date: Sun Oct 06 2019 00:01:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 00:01:36 GMT+0800 (Singapore Standard Time)

<p>
If one of the node is null, return false.
If the sum == target, return true;
If the sum > target, we need to move left. Check root1.left + root2 and root1 + root2.left.
Otherwise, sum < target, we move right. Check root1.right + root2 and root1 + root2.right.


```
class Solution {
    public boolean twoSumBSTs(TreeNode root1, TreeNode root2, int target) {
        if (root1 == null || root2 == null) return false;
        int sum = root1.val + root2.val;
        if (sum == target) return true;
        else if (sum > target) return twoSumBSTs(root1.left, root2, target) || twoSumBSTs(root1, root2.left, target);
        else return twoSumBSTs(root1.right, root2, target) || twoSumBSTs(root1, root2.right, target);
    }
}
```
</p>


### Searching in a BST is not O( log(n) )
- Author: Just__a__Visitor
- Creation Date: Sun Oct 06 2019 02:11:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 05:30:09 GMT+0800 (Singapore Standard Time)

<p>
# Time Complexity
I see a lot of solutions applying **Binary Search** on the second tree. The worst case time complexity of these solution is *O(n1 * n2)*. This is because *Searching* in a Balanced BST is *O(log(n))* but searching in a normal BST is only bounded by *O(h)* where, `h` is the height of the tree. Hence, in the worst case, the second tree might be tilted to one side and hence searching would take *O(n)* time for each call resulting in *O(n1 * n2)* time complexity.

**My Suggestion** : Since we are already using Linear Space (due to recursive calls), why not just store the second BST in  a Red Black Tree (such as Maps in C++). This would ensure worst case **O(n log(n))**.

# Intuition
Perhaps this is better visualized as an array problem. Given 2 arrays, does there exists 2 numbers (one in each) that sums upto `k`. Calculating this is very easy. Just store all the values of both the trees in 2 hashmap, and then for each element in one hashmap, check if its complement is present in the other or not.
```cpp
void populateMap(TreeNode* root, map<int, int> &count)
{
    if(!root)
        return;
    
    count[root->val]++;
    populateMap(root->left, count);
    populateMap(root->right, count);
}

class Solution
{
public:
    bool twoSumBSTs(TreeNode* root1, TreeNode* root2, int target);
};



bool Solution :: twoSumBSTs(TreeNode* root1, TreeNode* root2, int target)
{
    map<int, int> count1, count2;
    populateMap(root1, count1);
    populateMap(root2, count2);
    
    for(auto ele : count1)
        if(ele.first == target or count2[target - ele.first] != 0)
            return true;
    
    return false;
    
}
```
</p>


