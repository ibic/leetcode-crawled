---
title: "Coloring A Border"
weight: 1013
#id: "coloring-a-border"
---
## Description
<div class="description">
<p>Given a 2-dimensional&nbsp;<code>grid</code> of integers, each value in the grid represents the color of the grid square at that location.</p>

<p>Two squares belong to the same <em>connected component</em> if and only if they have the same color and are next to each other in any of the 4 directions.</p>

<p>The&nbsp;<em>border</em> of a connected component is&nbsp;all the squares in the connected component that are&nbsp;either 4-directionally adjacent to&nbsp;a square not in the component, or on the boundary of the grid (the first or last row or column).</p>

<p>Given a square at location&nbsp;<code>(r0, c0)</code>&nbsp;in the grid and a <code>color</code>, color the&nbsp;border of the connected component of that square with the given <code>color</code>, and return the final <code>grid</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>grid = <span id="example-input-1-1">[[1,1],[1,2]]</span>, r0 = <span id="example-input-1-2">0</span>, c0 = <span id="example-input-1-3">0</span>, color = <span id="example-input-1-4">3</span>
<strong>Output: </strong><span id="example-output-1">[[3, 3], [3, 2]]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>grid = <span id="example-input-2-1">[[1,2,2],[2,3,2]]</span>, r0 = <span id="example-input-2-2">0</span>, c0 = <span id="example-input-2-3">1</span>, color = <span id="example-input-2-4">3</span>
<strong>Output: </strong><span id="example-output-2">[[1, 3, 3], [2, 3, 3]]</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>grid = <span id="example-input-3-1">[[1,1,1],[1,1,1],[1,1,1]]</span>, r0 = <span id="example-input-3-2">1</span>, c0 = <span id="example-input-3-3">1</span>, color = <span id="example-input-3-4">2</span>
<strong>Output: </strong><span id="example-output-3">[[2, 2, 2], [2, 1, 2], [2, 2, 2]]</span></pre>
</div>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= grid.length &lt;= 50</code></li>
	<li><code>1 &lt;= grid[0].length &lt;= 50</code></li>
	<li><code>1 &lt;= grid[i][j] &lt;= 1000</code></li>
	<li><code>0 &lt;= r0 &lt; grid.length</code></li>
	<li><code>0 &lt;= c0 &lt; grid[0].length</code></li>
	<li><code>1 &lt;= color &lt;= 1000</code></li>
</ol>
</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Booking - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ with picture, DFS
- Author: votrubac
- Creation Date: Sun Apr 28 2019 12:02:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 28 2019 12:02:23 GMT+0800 (Singapore Standard Time)

<p>
From an initial point, perform DFS and flip the cell color to negative to track visited cells.

After DFS is complete for the cell, check if this cell is inside. If so, flip its color back to the positive.

In the end, cells with the negative color are on the border. Change their color to the target color.
![image](https://assets.leetcode.com/users/votrubac/image_1556425139.png)
```
void dfs(vector<vector<int>>& g, int r, int c, int cl) {
  if (r < 0 || c < 0 || r >= g.size() || c >= g[r].size() || g[r][c] != cl) return;
  g[r][c] = -cl;
  dfs(g, r - 1, c, cl), dfs(g, r + 1, c, cl), dfs(g, r, c - 1, cl), dfs(g, r, c + 1, cl);
  if (r > 0 && r < g.size() - 1 && c > 0 && c < g[r].size() - 1 && cl == abs(g[r - 1][c]) &&
    cl == abs(g[r + 1][c]) && cl == abs(g[r][c - 1]) && cl == abs(g[r][c + 1]))
    g[r][c] = cl;
}
vector<vector<int>> colorBorder(vector<vector<int>>& grid, int r0, int c0, int color) {
  dfs(grid, r0, c0, grid[r0][c0]);
  for (auto i = 0; i < grid.size(); ++i)
    for (auto j = 0; j < grid[i].size(); ++j) grid[i][j] = grid[i][j] < 0 ? color : grid[i][j];
  return grid;
}
```
</p>


### [Python] BFS and DFS
- Author: lee215
- Creation Date: Sun Apr 28 2019 12:01:51 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 03 2019 23:57:10 GMT+0800 (Singapore Standard Time)

<p>
**Python, DFS:**

Suggested by @IvanaGyro, we can color the border inside the DFS.
```
    def colorBorder(self, grid, r0, c0, color):
        seen, m, n = set(), len(grid), len(grid[0])

        def dfs(x, y):
            if (x, y) in seen: return True
            if not (0 <= x < m and 0 <= y < n and grid[x][y] == grid[r0][c0]):
                return False
            seen.add((x, y))
            if dfs(x + 1, y) + dfs(x - 1, y) + dfs(x, y + 1) + dfs(x, y - 1) < 4:
                grid[x][y] = color
            return True
        dfs(r0, c0)
        return grid
```

**Python, BFS:**
```
    def colorBorder(self, grid, r0, c0, color):
        m, n = len(grid), len(grid[0])
        bfs, component, border = [[r0, c0]], set([(r0, c0)]), set()
        for r0, c0 in bfs:
            for i, j in [[0, 1], [1, 0], [-1, 0], [0, -1]]:
                r, c = r0 + i, c0 + j
                if 0 <= r < m and 0 <= c < n and grid[r][c] == grid[r0][c0]:
                    if (r, c) not in component:
                        bfs.append([r, c])
                        component.add((r, c))
                else:
                    border.add((r0, c0))
        for x, y in border: grid[x][y] = color
        return grid
```

</p>


### [Java/Python 3] BFS and DFS codes
- Author: rock
- Creation Date: Mon Apr 29 2019 01:54:26 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 07 2020 00:31:03 GMT+0800 (Singapore Standard Time)

<p>
**Method 1**:

**BFS**

1. Let m = grid.length, n = grid[0].length, use the number 
from 0 to m * n - 1 to identify the cells to avoid duplicates;
e.g., grid[x][y]\'s cell number is x * n + y; 
2. put the initial cell [r0, c0] into the Queue then poll it out,
then check if it is on the grid bounday; If yes, color the cell;
3. Traverse the cell\'s 4 neighbors: 
a) if its neighbor is of different color, the cell is on the 
component border; 
b) if same color, put the neighbor into Queue;
4. repeat the above 2 and 3 till Queue is empty.

```java
    private static final int[] d = { 0, 1, 0, -1, 0 }; // neighbors\' relative displacements.
    public int[][] colorBorder(int[][] grid, int r0, int c0, int color) {
        int clr = grid[r0][c0], m = grid.length, n = grid[0].length;
        Set<Integer> component = new HashSet<>(); // put the cell number into Set to avoid visiting again.
        Queue<int[]> q = new LinkedList<>();
        q.offer(new int[]{ r0, c0 }); // add initial cell.
        component.add(r0 * n + c0); // add initial cell number.
        while (!q.isEmpty()) { // BFS starts.
            int r = q.peek()[0], c = q.poll()[1];
            if (r == 0 || r == m - 1 || c == 0 || c == n - 1) { grid[r][c] = color; } // on grid boundary.
            for (int k = 0; k < 4; ++k) { // travers its 4 neighbors.
                int i = r + d[k], j = c + d[k + 1]; // neighbor coordinates.
                if (i >= 0 && i < m && j >= 0 && j < n && !component.contains(i * n + j)) { // not visited before.
                    if (grid[i][j] == clr) { // its neighbor is of same color, put it into Queue. 
                        component.add(i * n + j); // avoid visiting again.
                        q.offer(new int[]{ i, j }); // put it into Queue. 
                    }else { // its neighbor is of different color, hence it is on component boundary.
                        grid[r][c] = color; 
                    }
                }
            }
        }
        return grid;
    }
```
```python
    def colorBorder(self, grid: List[List[int]], r0: int, c0: int, color: int) -> List[List[int]]:
         m, n, clr = len(grid), len(grid[0]), grid[r0][c0]
        bfs, component = collections.deque([(r0, c0)]), {(r0, c0)}
        while bfs:
            r, c = bfs.popleft()
            if r * c * (r - m + 1) * (c - n + 1) == 0:
                grid[r][c] = color
            for i, j in (r, c + 1), (r, c - 1), (r + 1, c), (r - 1, c):
                if m > i >= 0 <= j < n and (i, j) not in component:
                    if grid[i][j] == clr:
                        component.add((i, j))
                        bfs.append((i, j))
                    else:    
                        grid[r][c] = color
        return grid
```

----

**Method 2**: 

**DFS**

Use DFS to explore the cell (r0, c0)\'s component, and negate the visited cell, traverse its 4 neighbors. After the traversal, change back from the negative if the component cell is at inner part.

```java
    private static final int[] d = { 0, 1, 0, -1, 0 }; // neighbors\' relative displacements.
    public int[][] colorBorder(int[][] grid, int r0, int c0, int color) {
        negateBorder(grid, r0, c0, grid[r0][c0]);
        for (int[] g : grid) {
            for (int i = 0; i < g.length; ++i) {
                if (g[i] < 0) { g[i] = color; }
            }
        }
        return grid;
    }
    private void negateBorder(int[][] grid, int r, int c, int clr) {
        grid[r][c] = -clr; // mark as visited.
        int cnt = 0; // use to count grid[r][c]\'s component neighbors (same color as it).
        for (int k = 0; k < 4; ++k) { // traverse 4 neighbors.
            int i = r + d[k], j = c + d[k + 1]; // neighbor\'s coordinates.
            if (i < 0 || i >= grid.length || j < 0 || j >= grid[0].length || Math.abs(grid[i][j]) != clr) { continue; } // out of grid or not same component.
            ++cnt; // only if the 4 neighbors of grid[r][c] are all have same color as it, it is on inner part.
            if (grid[i][j] == clr) { negateBorder(grid, i, j, clr); } // recurse with respect to unvisited component neighbor.
        }
        if (cnt == 4) { grid[r][c] = clr; } // inner part, change back.
    }
```
```python
    def colorBorder(self, grid: List[List[int]], r0: int, c0: int, color: int) -> List[List[int]]:
        m, n, clr = len(grid), len(grid[0]), grid[r0][c0]

        def negateBorder(r, c):
            grid[r][c], cnt = -clr, 0
            for i, j in (r, c + 1), (r, c - 1), (r + 1, c), (r - 1, c):
                if m > i >= 0 <= j < n:
                    cnt +=  abs(grid[i][j]) == clr
                    if grid[i][j] == clr:
                        negateBorder(i, j)
            if cnt == 4:
                grid[r][c] = clr
                
        negateBorder(r0, c0)
        for r, row in enumerate(grid):
            for c, cell in enumerate(row):
                if cell < 0:
                    grid[r][c] = color
        return grid
```
</p>


