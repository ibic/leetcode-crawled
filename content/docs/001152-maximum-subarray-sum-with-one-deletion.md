---
title: "Maximum Subarray Sum with One Deletion"
weight: 1152
#id: "maximum-subarray-sum-with-one-deletion"
---
## Description
<div class="description">
<p>Given an array of integers, return the maximum sum for a <strong>non-empty</strong>&nbsp;subarray (contiguous elements) with at most one element deletion.&nbsp;In other words, you want to choose a subarray and optionally delete one element from it so that there is still at least one element left and the&nbsp;sum of the remaining elements is maximum possible.</p>

<p>Note that the subarray needs to be <strong>non-empty</strong> after deleting one element.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,-2,0,3]
<strong>Output:</strong> 4
<strong>Explanation: </strong>Because we can choose [1, -2, 0, 3] and drop -2, thus the subarray [1, 0, 3] becomes the maximum value.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,-2,-2,3]
<strong>Output:</strong> 3
<strong>Explanation: </strong>We just choose [3] and it&#39;s the maximum sum.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [-1,-1,-1,-1]
<strong>Output:</strong> -1
<strong>Explanation:</strong>&nbsp;The final subarray needs to be non-empty. You can&#39;t choose [-1] and delete -1 from it, then get an empty subarray to make the sum equals to 0.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 10^5</code></li>
	<li><code>-10^4 &lt;= arr[i] &lt;= 10^4</code></li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Two Sigma - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Intuitive Java Solution With Explanation
- Author: naveen_kothamasu
- Creation Date: Sun Sep 08 2019 12:07:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 09 2019 10:16:53 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
Compute `maxEndHere` and `maxStartHere` arrays and also find overall `max` along with them.
Now, evaluate the case where 1-element can be eliminated, that is at each index, we can make use of `maxEndHere[i-1]+maxStartHere[i+1]`

**Thought process**
This approach is a slight improvisation on the idea of https://leetcode.com/problems/maximum-subarray/. Basically, the difference here is we can eliminate 1 number and still can continue with expanding our subarray. So imagine a subarray where you removed 1 element, then it forms two subarrays ending at prev index and starting at next index. We know how to get `maxEndHere` from the max sum subarray problem for each index. If we reverse our thinking to follow the same logic to solve for subarray at next index, we should be able to see computing `maxStartHere` is just backwards of `maxEndHere`. So now at each index, it is just about looking at prev and next numbers from the respective arrays to get overall `max`.

```
public int maximumSum(int[] a) {
        int n = a.length;
        int[] maxEndHere = new int[n], maxStartHere = new int[n];
        int max = a[0];
        maxEndHere[0] = a[0];
        for(int i=1; i < n; i++){
            maxEndHere[i] = Math.max(a[i], maxEndHere[i-1]+a[i]);
            max = Math.max(max, maxEndHere[i]);
        }
        maxStartHere[n-1] = a[n-1];
        for(int i=n-2; i >= 0; i--)
            maxStartHere[i] = Math.max(a[i], maxStartHere[i+1]+a[i]);
        for(int i=1; i < n-1; i++)
            max = Math.max(max, maxEndHere[i-1]+maxStartHere[i+1]);
        return max;
    }
```
</p>


### C++ forward and backward solution with explanation and picture
- Author: Bug_Exceeded
- Creation Date: Sun Sep 08 2019 13:32:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 13:32:49 GMT+0800 (Singapore Standard Time)

<p>
* This problem is kind a modifiled version from Kenede\'s algorithm. see [here](https://en.wikipedia.org/wiki/Maximum_subarray_problem)
* We calculate the maximum subarray sum foward and backward
* Picture below shows how it goes
![image](https://assets.leetcode.com/users/bug_exceeded/image_1567920728.png)

```cpp
class Solution 
{
public:
    int maximumSum(vector<int>& v) 
    {
        int res = 0, n = v.size();
        int cur_max = v[0], overall_max = v[0];
        vector<int> f(n);
        vector<int> b(n);
        f[0] = v[0];
        
        for(int i = 1; i < n; i++)
        {
            cur_max = max(v[i], cur_max + v[i]); 
            overall_max = max(overall_max, cur_max); 
  
            f[i] = cur_max; 
        }
        
        cur_max = overall_max = b[n - 1] = v[n - 1];
        for(int i = n - 2; i >= 0; i--)
        {
            cur_max = max(v[i], cur_max + v[i]); 
            overall_max = max(overall_max, cur_max); 

            b[i] = cur_max; 
        }
        
        res = overall_max;
        for(int i = 1; i < n - 1; i++)
        {
            res = max(res, f[i - 1] + b[i + 1]);
        }
        return res;
    }
};
```
</p>


### Simple Python DP solution
- Author: otoc
- Creation Date: Sun Sep 08 2019 12:14:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 22:44:30 GMT+0800 (Singapore Standard Time)

<p>
Inspired by the solution for Maximum Subarray Sum
```
    def maximumSum(self, arr: List[int]) -> int:
        n = len(arr)
        max_ending_here = n * [arr[0]]
        for i in range(1, n):
            max_ending_here[i] = max(max_ending_here[i-1] + arr[i], arr[i])
        return max(max_ending_here)
```

It is not difficult to get this solution:
```
    def maximumSum(self, arr: List[int]) -> int:
        n = len(arr)
        max_ending_here0 = n * [arr[0]]  # no deletion
        max_ending_here1 = n * [arr[0]]  # at most 1 deletion
        for i in range(1, n):
            max_ending_here0[i] = max(max_ending_here0[i-1] + arr[i], arr[i])
            max_ending_here1[i] = max(max_ending_here1[i-1] + arr[i], arr[i])
            if i >= 2:
                max_ending_here1[i] = max(max_ending_here1[i], max_ending_here0[i-2] + arr[i])
        return max(max_ending_here1)
```
Space complexity can be easily improved to O(1).
</p>


