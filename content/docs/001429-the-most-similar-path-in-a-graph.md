---
title: "The Most Similar Path in a Graph"
weight: 1429
#id: "the-most-similar-path-in-a-graph"
---
## Description
<div class="description">
<p>We have <code>n</code> cities and <code>m</code> bi-directional <code>roads</code> where <code>roads[i] = [a<sub>i</sub>, b<sub>i</sub>]</code> connects city <code>a<sub>i</sub></code> with city <code>b<sub>i</sub></code>. Each city has a&nbsp;name consisting of exactly 3 upper-case English letters given in the string array <code>names</code>. Starting at any city <code>x</code>, you can reach any city <code>y</code> where <code>y != x</code> (i.e. the cities and the roads are forming an undirected connected graph).</p>

<p>You will be given a string array <code>targetPath</code>. You should find a path in the graph of the <strong>same length</strong> and with the <strong>minimum edit distance</strong> to <code>targetPath</code>.</p>

<p>You need to return <em>the order of the nodes in the path with the minimum edit distance</em>, The path should be&nbsp;of the same length of <code>targetPath</code>&nbsp;and should be valid (i.e. there should be a direct road between <code>ans[i]</code> and <code>ans[i + 1]</code>). If there are multiple answers return any one of them.</p>

<p>The <strong>edit distance</strong> is defined as follows:</p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/08/08/edit.jpg" style="width: 403px; height: 273px;" /></p>

<p><strong>Follow-up:</strong> If each node can be visited only once in the path, What should you change in your solution?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/08/e1.jpg" style="width: 378px; height: 532px;" />
<pre>
<strong>Input:</strong> n = 5, roads = [[0,2],[0,3],[1,2],[1,3],[1,4],[2,4]], names = [&quot;ATL&quot;,&quot;PEK&quot;,&quot;LAX&quot;,&quot;DXB&quot;,&quot;HND&quot;], targetPath = [&quot;ATL&quot;,&quot;DXB&quot;,&quot;HND&quot;,&quot;LAX&quot;]
<strong>Output:</strong> [0,2,4,2]
<strong>Explanation:</strong> [0,2,4,2], [0,3,0,2] and [0,3,1,2] are accepted answers.
[0,2,4,2] is equivalent to [&quot;ATL&quot;,&quot;LAX&quot;,&quot;HND&quot;,&quot;LAX&quot;] which has edit distance = 1 with targetPath.
[0,3,0,2] is equivalent to [&quot;ATL&quot;,&quot;DXB&quot;,&quot;ATL&quot;,&quot;LAX&quot;] which has edit distance = 1 with targetPath.
[0,3,1,2] is equivalent to [&quot;ATL&quot;,&quot;DXB&quot;,&quot;PEK&quot;,&quot;LAX&quot;] which has edit distance = 1 with targetPath.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/08/e2.jpg" style="width: 361px; height: 361px;" />
<pre>
<strong>Input:</strong> n = 4, roads = [[1,0],[2,0],[3,0],[2,1],[3,1],[3,2]], names = [&quot;ATL&quot;,&quot;PEK&quot;,&quot;LAX&quot;,&quot;DXB&quot;], targetPath = [&quot;ABC&quot;,&quot;DEF&quot;,&quot;GHI&quot;,&quot;JKL&quot;,&quot;MNO&quot;,&quot;PQR&quot;,&quot;STU&quot;,&quot;VWX&quot;]
<strong>Output:</strong> [0,1,0,1,0,1,0,1]
<strong>Explanation:</strong> Any path in this graph has edit distance = 8 with targetPath.
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/08/09/e3.jpg" style="width: 800px; height: 141px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 6, roads = [[0,1],[1,2],[2,3],[3,4],[4,5]], names = [&quot;ATL&quot;,&quot;PEK&quot;,&quot;LAX&quot;,&quot;ATL&quot;,&quot;DXB&quot;,&quot;HND&quot;], targetPath = [&quot;ATL&quot;,&quot;DXB&quot;,&quot;HND&quot;,&quot;DXB&quot;,&quot;ATL&quot;,&quot;LAX&quot;,&quot;PEK&quot;]
<strong>Output:</strong> [3,4,5,4,3,2,1]
<strong>Explanation:</strong> [3,4,5,4,3,2,1] is the only path with edit distance = 0 with targetPath.
It&#39;s equivalent to [&quot;ATL&quot;,&quot;DXB&quot;,&quot;HND&quot;,&quot;DXB&quot;,&quot;ATL&quot;,&quot;LAX&quot;,&quot;PEK&quot;]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 100</code></li>
	<li><code>m == roads.length</code></li>
	<li><code>n - 1 &lt;= m &lt;= (n * (n - 1) / 2)</code></li>
	<li><code>0 &lt;= a<sub>i</sub>, b<sub>i</sub> &lt;= n - 1</code></li>
	<li><code>a<sub>i</sub> != b<sub>i</sub>&nbsp;</code></li>
	<li>The graph is guaranteed to be <strong>connected</strong> and each pair of nodes may have <strong>at most one</strong> direct road.</li>
	<li><code>names.length == n</code></li>
	<li><code>names[i].length == 3</code></li>
	<li><code>names[i]</code> consists of upper-case English letters.</li>
	<li>There can be two cities with <strong>the same</strong> name.</li>
	<li><code>1 &lt;= targetPath.length &lt;= 100</code></li>
	<li><code>targetPath[i].length == 3</code></li>
	<li><code>targetPath[i]</code> consists of upper-case English letters.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Graph (graph)

## Companies
- Google - 5 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Clean Code w/ Comments and Video Explanation.
- Author: asher23
- Creation Date: Fri Aug 14 2020 17:43:06 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 14 2020 17:52:08 GMT+0800 (Singapore Standard Time)

<p>
Hello, as others mentioned, it\'s basically a dfs from each node with memoization. 
Just keep track of the mins. The video explains it in depth.
```
class Solution {
    List<List<Integer>> adjMatrix;
    String[] names;
    String[] targetPath;
    int[][] visited;
    int[][] nextChoiceForMin;
    public List<Integer> mostSimilar(int n, int[][] roads, String[] names, String[] targetPath) {
        // INITIALIZE VARIABLES
        this.visited = new int[names.length][targetPath.length];
        this.nextChoiceForMin = new int[names.length][targetPath.length];
        this.targetPath = targetPath;
        this.names = names;
        this.adjMatrix = new ArrayList<List<Integer>>();
       
        for (int[] x : visited) Arrays.fill(x, -1);
        
        // BUILD ADJACENCY MATRIX
        for (int i = 0;i < n; i++) adjMatrix.add(new ArrayList<Integer>());
        for (int[] road : roads) {
            adjMatrix.get(road[0]).add(road[1]);
            adjMatrix.get(road[1]).add(road[0]);
        }
        
        // FROM EACH NODE, CALCULATE MIN COST AND THE CITY THAT GAVE THE MIN COST
        int min = Integer.MAX_VALUE;
        int start = 0;
        for (int i = 0;i < names.length; i++) {
            int resp = dfs(i, 0);
            if (resp < min) {
                min = resp;
                start = i;
            }
        }
        
        // BUILD THE ANSWER BASED ON WHATS THE BEST NEXT CHOICE 
        List<Integer> ans = new ArrayList<Integer>();
        while (ans.size() < targetPath.length) {
            ans.add(start);
            start = nextChoiceForMin[start][ans.size()-1];
        }
        return ans;
    }
    
    public int dfs(int namesIdx, int currPathIdx) {
        // IF WE VISITED IT ALREADY, RETURN THE PREVIOUS COUNT
        if (visited[namesIdx][currPathIdx] != -1) return visited[namesIdx][currPathIdx];
        
        // IF ITS DIFFERENT, ADD 1 ELSE ADD 0.
        int editDist = (names[namesIdx].equals(targetPath[currPathIdx])) ? 0 : 1;
        
        // IF WE FILLED UP THE PATH, WE\'RE DONE
        if (currPathIdx == targetPath.length-1) return editDist;
        
        // FROM EACH NEIGHBOR, CALCULATE MIN COST AND SAVE THE CITY THAT GAVE THE MIN COST  
        int min = Integer.MAX_VALUE;
        for (int neighbor : adjMatrix.get(namesIdx)) {
            int neighborCost = dfs(neighbor, currPathIdx+1);
            if (neighborCost < min) {
                min = neighborCost;
                nextChoiceForMin[namesIdx][currPathIdx] = neighbor; // HERE IS WHERE WE SAVE
            }
        }
        
        
        editDist += min; // UPDATE OUR EDIT DISTANCE
        visited[namesIdx][currPathIdx] = editDist; // SAVE OUR EDIT DISTANCE
        return editDist; // RETURN OUR EDIT DISTANCE
    }
}    

/*
                0.     1.   2.   3.   4
  names         [ATL, PEK, LAX, DXB, HND]
    
                           0.      1.         2.         3
   targetPath   ["ATL","DXB","HND","LAX"]
    
    DO THIS STARTING AT EVERY NODE
    ATL at idx = 0 is it different? no, so add to cost + 0 = 0
        3
        DXB at idx = 1 is it different? no, so add to cost + 0 = 0 
            ATL at idx = 2 is it different? yes, so add to cost + 1 = 1
                DXB at idx = 3 is it different? yes, so add to cost + 1 AND RETURN = 2
                LAX at idx = 3 is it different? no, so add to cost + 0 AND RETURN = 1
            PEK at idx = 2 is it different? yes, so add to cost + 1 = 1
                DXB at idx = 3 SOLVED ALREADY RETURN PREV   RETURN 
                LAX at idx = 3 SOLVED ALREADY RETURN PREV
                HND at idx = 3 is it different? yes, so add to cost + 1 AND RETURN
        LAX at idx = 1 is it different? yes, so add to cost + 1
            ATL at idx = 2, SOLVED ALREADY RETURN PREV
            PEK at idx = 2, SOLVED ALREADY RETURN PREV
            HND at idx = 2 is it different? no, so addto cost + 0
                PEK at idx = 3 is it diffferent? yes, so add to cost + 
                LAX at idx = 3 SOLVED ALREADY RETURN PREV
    */
```

<iframe width="966" height="604" src="https://www.youtube.com/embed/DTn8A9D-NCM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>


### [Python] Clean bottom up DP solution with explanation - O(N^2 * len(tp))
- Author: alanlzl
- Creation Date: Fri Aug 14 2020 04:57:09 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 14 2020 07:43:44 GMT+0800 (Singapore Standard Time)

<p>
**Idea**

Let\'s first calculate the minimum edit distance without worrying about the path. We can use 2D DP to do that:

- `dp[i][v]` means the minimum edit distance for `tp[:i+1]` ending with city `v`.

The trainsition function is:
- `dp[i][v] = min(dp[i-1][u] + edit_cost(v))` for all edges `(u, v)`

, where `edit_cost(v)` at index `i` is `names[v] != tp[i]`.

And the minimum edit distance will be `min(dp[-1][v] for v in range(n))`. 

To construct the optimal path, we can maintain a 2D array (or dict) `prev` when populate `dp`. Suppose `prev[i][v]` is `u`. Then `(u, v)` is the ending edge of the optimal path at `dp[i][v]`.

 <br />

**Complexity**

Time complexity: `O(N^2 * len(tp))`
Space complexity: `O(N * len(tp))`

<br />

**Python**
```Python
class Solution:
    def mostSimilar(self, n: int, roads: List[List[int]], names: List[str], tp: List[str]) -> List[int]:
        # construct graph
        graph = [[] for _ in range(n)]
        for u, v in roads:
            graph[u].append(v)
            graph[v].append(u)
        
        # init variables
        m = len(tp)
        dp = [[m] * n for _ in range(m)]
        prev = [[0] * n for _ in range(m)]
        
        # populate dp
        for v in range(n):
            dp[0][v] = (names[v] != tp[0])
        for i in range(1, m):
            for v in range(n):
                for u in graph[v]:
                    if dp[i-1][u] < dp[i][v]:
                        dp[i][v] = dp[i-1][u]
                        prev[i][v] = u
                dp[i][v] += (names[v] != tp[i])
                
        # re-construct path
        path, min_dist = [0], m
        for v in range(n):
            if dp[-1][v] < min_dist:
                min_dist = dp[-1][v]
                path[0] = v
        for i in range(m - 1, 0, -1):
            u = prev[i][path[-1]]
            path.append(u)
            
        return path[::-1]
```
</p>


### Java. Dijkstra (70ms)
- Author: SleepyFarmer
- Creation Date: Thu Aug 13 2020 13:15:52 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 14 2020 13:33:40 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public List<Integer> mostSimilar(int n, int[][] roads, String[] names, String[] targetPath) {
        // build the graph
        List<Integer>[] g = new ArrayList[n];
        for(int[] r: roads) {
            int a = r[0]; int b = r[1];
            if (g[a] == null) g[a] = new ArrayList<>();
            if (g[b] == null) g[b] = new ArrayList<>();
            g[a].add(b);
            g[b].add(a);
        }
        
        int m = targetPath.length;
        int[][] path = new int[n][m]; // record the path. path[i][j] stores the previous city 
                                      // for city i at position j
        int[][] dist = new int[n][m]; // dist[i][j] is the min edit distance for city i at position j
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b)->{
            int da = dist[a[0]][a[1]];
            int db = dist[b[0]][b[1]];
            if (da == db) return b[1] - a[1];
            return da - db;
        });
        
        for(int i = 0; i < n; i++) {
            dist[i][0] = targetPath[0].equals(names[i]) ? 0 : 1;
            pq.offer(new int[]{i, 0});
            for(int j = 1; j < m; j++) dist[i][j] = Integer.MAX_VALUE;
        }
        
        int min = Integer.MAX_VALUE;
        while(!pq.isEmpty()) {
            int[] a = pq.poll();
            int c = a[0]; int p = a[1];
            int d = dist[c][p];
            if (p == m-1) break;
            for(int b: g[a[0]]) {
                int dd = d + (targetPath[p+1].equals(names[b]) ? 0 : 1);
                if (dd < dist[b][p+1]) {
                    dist[b][p+1] = dd;
                    pq.offer(new int[]{b, p+1});
                    path[b][p+1] = c;
                }
            }
        }
        
        int last = 0;
        for(int i = 1; i < n; i++) {
            if (dist[i][m-1] < dist[last][m-1]) last = i;
        }
        
        LinkedList<Integer> ans = new LinkedList<>();
        for(int i = m-1; i >= 0; i--) {
            ans.push(last);
            last = path[last][i];
        }
        return ans;
    }
}
```
</p>


