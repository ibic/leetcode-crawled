---
title: "Wildcard Matching"
weight: 44
#id: "wildcard-matching"
---
## Description
<div class="description">
<p>Given an input string (<code>s</code>) and a pattern (<code>p</code>), implement wildcard pattern matching with support for <code>&#39;?&#39;</code> and <code>&#39;*&#39;</code>.</p>

<pre>
&#39;?&#39; Matches any single character.
&#39;*&#39; Matches any sequence of characters (including the empty sequence).
</pre>

<p>The matching should cover the <strong>entire</strong> input string (not partial).</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>s</code>&nbsp;could be empty and contains only lowercase letters <code>a-z</code>.</li>
	<li><code>p</code> could be empty and contains only lowercase letters <code>a-z</code>, and characters like <code><font face="monospace">?</font></code>&nbsp;or&nbsp;<code>*</code>.</li>
</ul>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong>
s = &quot;aa&quot;
p = &quot;a&quot;
<strong>Output:</strong> false
<strong>Explanation:</strong> &quot;a&quot; does not match the entire string &quot;aa&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong>
s = &quot;aa&quot;
p = &quot;*&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong>&nbsp;&#39;*&#39; matches any sequence.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong>
s = &quot;cb&quot;
p = &quot;?a&quot;
<strong>Output:</strong> false
<strong>Explanation:</strong>&nbsp;&#39;?&#39; matches &#39;c&#39;, but the second letter is &#39;a&#39;, which does not match &#39;b&#39;.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong>
s = &quot;adceb&quot;
p = &quot;*a*b&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong>&nbsp;The first &#39;*&#39; matches the empty sequence, while the second &#39;*&#39; matches the substring &quot;dce&quot;.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong>
s = &quot;acdcb&quot;
p = &quot;a*c?b&quot;
<strong>Output:</strong> false
</pre>

</div>

## Tags
- String (string)
- Dynamic Programming (dynamic-programming)
- Backtracking (backtracking)
- Greedy (greedy)

## Companies
- Goldman Sachs - 9 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: true)
- Microsoft - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Coursera - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Two Sigma - 4 (taggedByAdmin: true)
- Oracle - 2 (taggedByAdmin: false)
- Cruise Automation - 2 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)
- Snapchat - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Recursion with Memoization

**Intuition**

The first idea here is a recursion. That's a straightforward
approach but quite time consuming because of huge recursion depth for long input strings.

- If the strings are equal `p == s`, return `True`.

- If the pattern matches whatever string `p == '*'`, return `True`.

- If `p` is empty, or `s` is empty, return False.

- If the current characters match `p[0] == s[0]` or `p[0] == '?'`, 
then compare the next ones and return `isMatch(s[1:], p[1:])`.

- If the current pattern character is a star `p[0] == '*'`, then
there are two possible situations:

    - The star matches no characters, and hence the answer is
    `isMatch(s, p[1:])`.
    
    - The star matches one or more characters, and so the answer is
    `isMatch(s[1:], p)`.
    
- If `p[0] != s[0]`, return `False`.

![bla](../Figures/44/stupid.png)

The problem of this algorithm is that it doesn't pass 
all test cases because of time limit issue, 
and hence has to be optimised.
Here is what could be done:

1. _Memoization_. That is a standard way to optimise the recursion. 
Let's have a memoization hashmap using pair `(s, p)` as a key and
match/doesn't match as a boolean value. 
One could keep all already checked pairs `(s, p)` in this hashmap, so that
if there are any duplicate checks, the answer is right here, 
and there is no need to proceed to the computations again.

2. _Clean up of the input data_. Whether the patterns with multiple stars 
in a row `a****bc**cc` are valid wildcards or not, they could be 
simplified without any data loss `a*bc*cc`. Such a cleanup helps to decrease
the recursion depth. 

**Algorithm**

Here is the algorithm.

- Clean up the input by replacing more than one star in a row by a single star: 
`p = remove_duplicate_stars(p)`.

- Initiate the memoization hashmap `dp`.

- Return the helper function with a cleaned input: `helper(s, p)`.

- `helper(s, p)`:

    - If (s, p) is already known and stored in dp, return the value.

    - If the strings are equal `p == s`, or the pattern matches whatever string `p == '*'`,
    add `dp[(s, p)] = True`.
    
    - Else if `p` is empty, or `s` is empty, add `dp[(s, p)] = False`.
    
    - Else if the current characters match `p[0] == s[0]` or `p[0] == '?'`, 
    then compare the next ones and add `dp[(s, p)] = helper(s[1:], p[1:])`.
    
    - Else if the current pattern character is a star `p[0] == '*'`, then
    there are two possible situations: the star matches no characters,
    and the star matches one or more characters. 
    `dp[(s, p)] = helper(s, p[1:]) or helper(s, p[1:])`.
        
    - Else `p[0] != s[0]`, add `dp[(s, p)] = False`.
    
    - Now that the value is computed, return it `dp[(s, p)]`.

**Implementation**   

<iframe src="https://leetcode.com/playground/PLRNc9vy/shared" frameBorder="0" width="100%" height="500" name="PLRNc9vy"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(\min(S, P))$$ for the best case, 
and $$\mathcal{O}(2^{\min(S, P/2)})$$ for the worst case, where S and P are lengths of 
the input string and the pattern correspondingly. The best case is quite obvious, 
let's estimate the worst case. The most time consuming is the situation where 
recursion forms the tree on the star character in the pattern. 
In this situation 2 branches are executed : `helper(s, p[1:])` and `helper(s[1:], p)`. 
The maximum number of stars in the pattern after data cleanup is $$P/2$$ and hence
the time complexity is $$\mathcal{O}(2^{\min(S, P/2)})$$.
* Space complexity: $$\mathcal{O}(2^{\min(S, P/2)})$$ to keep the memoization hashmap and
the recursion stack.
<br />
<br />


---
#### Approach 2: Dynamic Programming 

**Intuition**

Recursion approach above shows how painful the large recursion depth could be,
so let's try something more iterative. 

Memoization from the first approach gives an idea to try a dynamic programming.
The problem is very similar with [Edit Distance problem](https://leetcode.com/problems/edit-distance/solution/),
so let's use exactly the same approach here.

The idea would be to reduce the problem to simple ones. 
For example, there is a string `adcebdk` and pattern `*a*b?k`,
and we want to compute if there is a match for them: `D = True/False`. 
One could notice that it seems to be more simple for short strings and patterns 
and so it would be logical to relate a match `D[p_len][s_len]` with the lengths `p_len` 
and `s_len` of input pattern and string correspondingly.

Let's go further and introduce a match `D[p_idx][s_idx]` 
which is a match between the first `p_idx` characters of the pattern 
and the first `s_idx` characters of the string.

![bla](../Figures/44/dp_match2.png)

It turns out that one could compute `D[p_idx][s_idx]`, knowing 
a match without the last characters `D[p_idx - 1][s_idx - 1]`.

> If the last characters are the same or pattern character is '?', then 

$$
D[p_{idx}][s_{idx}] = D[p_{idx} - 1][s_{idx} - 1] \qquad (1)
$$

![bla](../Figures/44/word_match3.png)

> If the pattern character is '*' and there was a match on the previous step
`D[p_idx - 1][s_idx - 1] = True`, then 

- The star at the end of pattern still results in a match. 

- The star could much as many characters as you wish.

$$
D[p_{idx} - 1][i] = \textrm{True}, i \ge s_{idx} - 1 \qquad(2)
$$

So each step of the computation would be done based on the previous ones,
as follows: 

![bla](../Figures/44/if_match.png)

![bla](../Figures/44/dpstar.png)

**Algorithm**

- Start from the table initiated as `False` everywhere but `D[0][0] = True`.

- Apply rules (1) and (2) in a loop and return `D[p_len][s_len]` as an answer.

![bla](../Figures/44/fixed.png)

**Implementation**

<iframe src="https://leetcode.com/playground/rXSdgzgX/shared" frameBorder="0" width="100%" height="500" name="rXSdgzgX"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(S P)$$ where S and P are lengths of 
the input string and the pattern correspondingly. 
* Space complexity : $$\mathcal{O}(S P)$$ to keep the matrix.
<br />
<br />


---
#### Approach 3: Backtracking

**Intuition**

Complexity $$\mathcal{O}(S P)$$ is much better than $$\mathcal{O}(2^{\min(S, P/2)})$$,
but still could be improved. There is no need to compute the entire matrix,
and i.e. to check all the possibilities for each star :

- Star matches zero characters.
- Star matches one character.
- Star matches two characters. 

...

- Star matches all remaining characters.  

Let's just pick up the first opportunity "matches zero characters" and proceed further.
If this assumption would lead in "no match" situation, then _backtrack_ : come back
to the previous star, assume now that it matches one more character (one) and 
proceed again. Again "no match" situation? 
_Backtrack again_ : come back to the previous star, 
and assume now that it matches one more character (two), etc. 

![bla](../Figures/44/backtrack.png)

**Algorithm**

Here is the algorithm.

- Let's use two pointers here: `s_idx` to iterate over the string, and `p_idx` to 
iterate over the pattern. While `s_idx < s_len`:

    - If there are still characters in the pattern `p_idx < p_len` and
    the characters under the pointers match 
    `p[p_idx] == s[s_idx]` or `p[p_idx] == '?'`,
    then move forward by increasing both pointers.
    
    - Else if there are still characters in the pattern `p_idx < p_len`, and
    `p[p_idx] == '*'`, then first check "match zero characters" situation, i.e.
    increase only pattern pointer `p_idx++`.
    Write down for a possible backtrack the star position in `star_idx` variable,
    and the current string pointer in `s_tmp_idx` variable.
    
    - Else if there is "no match" situation: 
    the pattern is used up `p_idx < p_len`
    or the characters under the pointers doesn't match. 
    
        - If there was no stars in the pattern, i.e. no `star_idx`, return `False`.
        
        - If there was a star, then backtrack: set pattern pointer
        just after the last star `p_idx = star_idx + 1`, and string 
        pointer `s_idx = s_tmp_idx + 1`, i.e. assume that this time the star
        matches _one more character_. Save the current string pointer 
        for the possible backtrack `s_tmp_idx = s_idx`.
        
- Return `True` if all remaining characters in the pattern are stars. 

**Implementation**

<iframe src="https://leetcode.com/playground/yD3WHysy/shared" frameBorder="0" width="100%" height="500" name="yD3WHysy"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(\min(S, P))$$ for the best case and
better than $$\mathcal{O}(S \log P)$$ for the average case, where S and P are lengths of 
the input string and the pattern correspondingly. 
Please refer to [this article](https://arxiv.org/pdf/1407.0950.pdf) for the detailed proof. 
* Space complexity : $$\mathcal{O}(1)$$ since it's a constant space solution.
<br />
<br />


---
#### Further reading

There are a lot of search-related questions around this problem 
which could pop up during the interview.
To prepare, you could read about [string searching algorithm](https://en.wikipedia.org/wiki/String-searching_algorithm)
and [KMP algorithm](https://en.wikipedia.org/wiki/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm).

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Linear runtime and constant space solution
- Author: pandora111
- Creation Date: Sun Aug 31 2014 12:16:30 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 10:57:31 GMT+0800 (Singapore Standard Time)

<p>
I found this solution from http://yucoding.blogspot.com/2013/02/leetcode-question-123-wildcard-matching.html


----------


The basic idea is to have one pointer for the string and one pointer for the pattern.  This algorithm iterates at most length(string) + length(pattern) times, for each iteration, at least one pointer advance one step. 


----------

Here is Yu's elegant solution in C++

     bool isMatch(const char *s, const char *p) {
            const char* star=NULL;
            const char* ss=s;
            while (*s){
                //advancing both pointers when (both characters match) or ('?' found in pattern)
                //note that *p will not advance beyond its length 
                if ((*p=='?')||(*p==*s)){s++;p++;continue;} 

                // * found in pattern, track index of *, only advancing pattern pointer 
                if (*p=='*'){star=p++; ss=s;continue;} 

                //current characters didn't match, last pattern pointer was *, current pattern pointer is not *
                //only advancing pattern pointer
                if (star){ p = star+1; s=++ss;continue;} 

               //current pattern pointer is not star, last patter pointer was not *
               //characters do not match
                return false;
            }
 
           //check for remaining characters in pattern
            while (*p=='*'){p++;}

            return !*p;  
        }


----------


Here is my re-write in Java

    \ufeff\ufeff\ufeffboolean comparison(String str, String pattern) {
            int s = 0, p = 0, match = 0, starIdx = -1;            
            while (s < str.length()){
                // advancing both pointers
                if (p < pattern.length()  && (pattern.charAt(p) == '?' || str.charAt(s) == pattern.charAt(p))){
                    s++;
                    p++;
                }
                // * found, only advancing pattern pointer
                else if (p < pattern.length() && pattern.charAt(p) == '*'){
                    starIdx = p;
                    match = s;
                    p++;
                }
               // last pattern pointer was *, advancing string pointer
                else if (starIdx != -1){
                    p = starIdx + 1;
                    match++;
                    s = match;
                }
               //current pattern pointer is not star, last patter pointer was not *
              //characters do not match
                else return false;
            }
            
            //check for remaining characters in pattern
            while (p < pattern.length() && pattern.charAt(p) == '*')
                p++;
            
            return p == pattern.length();
    }
</p>


### Detailed Intuition - From Brute force to Bottom-up DP
- Author: Sarmon
- Creation Date: Fri Aug 30 2019 22:30:49 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 10 2020 17:09:38 GMT+0800 (Singapore Standard Time)

<p>
Because I couldn\'t find a detailed intuition about how we can think about the DP bottom-up solution in the discuss section. I\'m writing this post to share the detailed thinking process that leads to it.

*Quote : Every DP solution comes from a Brute force solution that repeats some computations many times. The difference between the two is that DP doesn\'t repeat a work it had done before.*

We have 2 strings *S* and *P*. We need to know whether *P* is a pattern of *S*.

The idea is pretty straightforward : scan *S* and *P* while there is a match between the current character of *S* and the current character of *P*. If we reach the end of both strings while there is still a match, return True, otherwise return False. The scan is done by having a pointer in S and a pointer in P.

Example: S="code"
The character \'c\' of S matches the first character of P if the first character of P is:
* \'c\'

* \'?\'
* \'*\'

When the first character of P is a lowercase letter different from \'c\', return False.
If the first character of P is \'c\' or \'?\', we move both pointers one step to the right.
If the first character of P is \'*\', we have 2 possibilities:
* \'*\' matches 0 character : in this case we move the pointer in P one step
* \'*\' matches 1 or more characters : in this case we move the pointer in S one step

And we continue like this for each two positions taken by the two pointers.

If we reach the end of P but there is still characters from S, simply return .. False !
If we reach the end of S and there is still characters from P, the only case when there is a match is that all the remaining characters in P are \'*\', in this case these stars will be matched with the empty string.

Here is the code:

```python
def isMatch(self, s, p):
	return self.helper(s, p, 0, 0)

def helper(self, s, p, start_s, start_p):
	if start_s==len(s) and start_p==len(p): # reached the end of both S and P
		return True
	if start_p==len(p): # there are still characters in S => there is no match
		return False
	if start_s==len(s): # Hopefully the remaining characters in P are all stars
		return p[start_p]==\'*\' and self.helper(s, p, start_s, start_p+1) 
	if p[start_p]==\'*\':
		# star either matches 0 or >=1 character
		return self.helper(s, p, start_s+1, start_p) or self.helper(s, p, start_s, start_p+1)
	elif p[start_p]==\'?\' or p[start_p]==s[start_s]:
		# move both pointers
		return  self.helper(s, p, start_s+1, start_p+1)
	else: # the current char from P is a lowercase char different from s[start_s]
		return False
```

The problem with the solution above is that there are many repeated computations. Add :
```python
print(start_s, start_p)
```
at the beginning of the helper function and you will see that there are many calls to `helper` with the *same* start_s and start_p.

So the solution to this is simply memoize (=cache) the work we have done already, here is the code with caching.
```python
def wildcardMatch(self, s, p):
	self.memo = {} # Define the cache
	return self.helper(s, p, 0, 0)

def helper(self, s, p, start_s, start_p):
	if (start_s, start_p) in self.memo: # If the computation is already done, directly return the cached result
		return self.memo[(start_s, start_p)]
	res = False
	if start_s==len(s) and start_p==len(p):
		res = True
	elif start_s==len(s):
		res = p[start_p]==\'*\' and self.helper(s, p, start_s, start_p+1)
	elif start_p==len(p):
		res = False
	elif p[start_p]==\'*\':
		res = self.helper(s, p, start_s+1, start_p) or self.helper(s, p, start_s, start_p+1)
	elif p[start_p]==\'?\' or p[start_p]==s[start_s]:
		res = self.helper(s, p, start_s+1, start_p+1)
	else:
		res = False
	self.memo[(start_s, start_p)] = res # Before returning the result, cache it !
	return res
```
I removed the returns and replaced them with assignment to res so that I don\'t repeat the `self.memo[(start_s, start_p)] = res` in each if statement.

This kind of solution is called Top-Down DP, because we started from the biggest problem (the whole S and the whole P) to the smallest problems.

A Top-down DP solution (recursive) may be converted to a Bottom-Up solution (iterative).

A Top-Down solution doesn\'t compute a sub result before needing it. Bottom-Up knows that it will need this subresult so it computes it before moving to the next level.

![image](https://assets.leetcode.com/users/ayoubom/image_1567173912.png)

And because the cache keys were couples (i, j) with i between 0 and N and j between 0 and M. We usually work with a matrix instead of a hashtable (to avoid key collisions, memory waste, ...)

Here is the bottom up version:

```python
def isMatch(self, s, p):
	dp = [[False for _ in range(len(p)+1)] for _ in range(len(s)+1)]

	for i in range(len(s)+1):
		for j in range(len(p)+1):
			start_s, start_p = i-1, j-1
			if i==0 and j==0:
				dp[i][j] = True

			elif i == 0:
				dp[i][j] = p[start_p]==\'*\' and dp[i][j-1]

			elif j == 0:
				dp[i][j] = False

			elif p[start_p] == \'*\':
				dp[i][j] = dp[i][j-1] or dp[i-1][j]

			elif s[start_s] == p[start_p] or p[start_p] == \'?\':
				dp[i][j] = dp[i-1][j-1]


	return dp[len(s)][len(p)]
```

I kept the code a little bit long so that you can do the mappings between the Top-Down and this bottom-up solution. They do the same thing !

One small note is that the bottom up solution starts from (0, 0) and not (len(s), len(p)) like mentioned in the drawing above. This is because the problem is *symmetric* : if there is a match between `"code"` and `"*o?e"`, then there is a match between `"edoc"` and `"e?o*"`. For Top-down the smallest problem is (len(s), len(p)), and for Bottom-up the smallest is (0, 0).

Hope it helps !

</p>


### My java DP solution using 2D table
- Author: leetcode_deleted_user
- Creation Date: Wed Aug 26 2015 11:07:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 05:54:53 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public boolean isMatch(String s, String p) {
            boolean[][] match=new boolean[s.length()+1][p.length()+1];
            match[s.length()][p.length()]=true;
            for(int i=p.length()-1;i>=0;i--){
                if(p.charAt(i)!='*')
                    break;
                else
                    match[s.length()][i]=true;
            }
            for(int i=s.length()-1;i>=0;i--){
                for(int j=p.length()-1;j>=0;j--){
                    if(s.charAt(i)==p.charAt(j)||p.charAt(j)=='?')
                            match[i][j]=match[i+1][j+1];
                    else if(p.charAt(j)=='*')
                            match[i][j]=match[i+1][j]||match[i][j+1];
                    else
                        match[i][j]=false;
                }
            }
            return match[0][0];
        }
    }
</p>


