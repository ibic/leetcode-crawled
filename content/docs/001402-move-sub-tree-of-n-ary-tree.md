---
title: "Move Sub-Tree of N-Ary Tree"
weight: 1402
#id: "move-sub-tree-of-n-ary-tree"
---
## Description
<div class="description">
<p>Given the <code>root</code> of an&nbsp;<a href="https://leetcode.com/articles/introduction-to-n-ary-trees/">N-ary tree</a>&nbsp;of unique values, and two nodes of the tree&nbsp;<code>p</code> and <code>q</code>.</p>

<p>You should move&nbsp;the subtree of the node&nbsp;<code>p</code>&nbsp;to become a direct child of node <code>q</code>. If <code>p</code>&nbsp;is already a direct child of <code>q</code>, don&#39;t change anything. Node <code>p</code> <strong>must be</strong> the last child in the children list of node <code>q</code>.</p>

<p>Return <em>the root of the tree</em> after adjusting it.</p>

<p>&nbsp;</p>

<p>There are 3 cases for nodes <code>p</code> and <code>q</code>:</p>

<ol>
	<li>Node <code>q</code> is in the sub-tree of node <code>p</code>.</li>
	<li>Node <code>p</code> is in the sub-tree of node <code>q</code>.</li>
	<li>Neither node <code>p</code> is&nbsp;in the sub-tree of node <code>q</code> nor node <code>q</code> is in the sub-tree of node <code>p</code>.</li>
</ol>

<p>In cases 2 and 3, you just need to move <code><span>p</span></code>&nbsp;(with its sub-tree) to be a child of <code>q</code>, but in case 1 the tree may be disconnected, thus you need to reconnect the tree again. <strong>Please read the examples carefully before solving this problem.</strong></p>

<p>&nbsp;</p>

<p><em>Nary-Tree input serialization&nbsp;is represented in their level order traversal, each group of children is separated by the null value (See examples).</em></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/11/08/sample_4_964.png" style="width: 296px; height: 241px;" /></p>

<p>For example, the above tree is serialized as [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14].</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/13/move_e1.jpg" style="width: 450px; height: 188px;" />
<pre>
<strong>Input:</strong> root = [1,null,2,3,null,4,5,null,6,null,7,8], p = 4, q = 1
<strong>Output:</strong> [1,null,2,3,4,null,5,null,6,null,7,8]
<strong>Explanation:</strong> This example follows the second case as node p is in the sub-tree of node q. We move node p with its sub-tree to be a direct child of node q.
Notice that node 4 is the last child of node 1.</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/13/move_e2.jpg" style="width: 281px; height: 281px;" />
<pre>
<strong>Input:</strong> root = [1,null,2,3,null,4,5,null,6,null,7,8], p = 7, q = 4
<strong>Output:</strong> [1,null,2,3,null,4,5,null,6,null,7,8]
<strong>Explanation:</strong> Node 7 is already a direct child of node 4. We don&#39;t change anything.
</pre>

<p><strong>Example 3:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/13/move_e3.jpg" style="width: 450px; height: 331px;" />
<pre>
<strong>Input:</strong> root = [1,null,2,3,null,4,5,null,6,null,7,8], p = 3, q = 8
<strong>Output:</strong> [1,null,2,null,4,5,null,7,8,null,null,null,3,null,6]
<strong>Explanation:</strong> This example follows case 3 because node p is not in the sub-tree of node q and vice-versa. We can move node 3 with its sub-tree and make it as node 8&#39;s child.
</pre>

<p><strong>Example 4:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/13/move_e4.jpg" style="width: 700px; height: 254px;" />
<pre>
<strong>Input:</strong> root = [1,null,2,3,null,4,5,null,6,null,7,8], p = 2, q = 7
<strong>Output:</strong> [1,null,7,3,null,2,null,6,null,4,5,null,null,8]
<strong>Explanation:</strong> Node q is in the sub-tree of node p, so this is case 1.
The first step, we move node p (with all of its sub-tree except for node q) and add it as a child to node q.
Then we will see that the tree is disconnected, you need to reconnect node q to replace node p as shown.
</pre>

<p><strong>Example 5:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/13/move_e5.jpg" style="width: 700px; height: 182px;" />
<pre>
<strong>Input:</strong> root = [1,null,2,3,null,4,5,null,6,null,7,8], p = 1, q = 2
<strong>Output:</strong> [2,null,4,5,1,null,7,8,null,null,3,null,null,null,6]
<strong>Explanation:</strong> Node q is in the sub-tree of node p, so this is case 1.
The first step, we move node p (with all of its sub-tree except for node q) and add it as a child to node q.
As node p was the root of the tree, node q replaces it and becomes the root of the tree.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The total number of nodes is between <code>[2,&nbsp;1000]</code>.</li>
	<li>Each&nbsp;node has a <strong>unique</strong> value.</li>
	<li><code>p != null</code></li>
	<li><code>q != null</code></li>
	<li><code>p</code> and <code>q</code> are two different nodes (i.e. <code>p != q</code>).</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ #minimalizm
- Author: votrubac
- Creation Date: Thu Jul 23 2020 13:54:06 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 23 2020 14:36:56 GMT+0800 (Singapore Standard Time)

<p>
I am shooting for the shortest/simplest solution, please let me know if it can be simplified further!

So far I have two traversals - one from root, one from `p`, one remove/replace, plus another remove if `p` is a parent of `q`.

```cpp
Node* getParent(Node* r, Node* t) {
    Node* parent = nullptr;
    for (auto i = 0; i < r->children.size() && parent == nullptr; ++i)
        parent = r->children[i] == t ? r : getParent(r->children[i], t);
    return parent;
}
void removeOrReplace(Node* r, Node* target, Node* replace = nullptr) {
    if (r != nullptr)
        for (auto i = 0; i < r->children.size(); ++i)
            if (r->children[i] == target) {
                if (replace != nullptr)
                    r->children[i] = replace;
                else
                    r->children.erase(begin(r->children) + i);                
            }
}
Node* moveSubTree(Node* root, Node* p, Node* q) {
    auto parentP = getParent(root, p);
    if (parentP != q) {
        auto parentQInP = getParent(p, q);
        if (parentQInP != nullptr)
            removeOrReplace(parentQInP, q);
        removeOrReplace(parentP, p, parentQInP != nullptr ? q : nullptr);
        q->children.push_back(p);
    }
    return root == p ? q : root;
}
```
</p>


### [python] Boiled down to 3 major cases and treat each case by case
- Author: forestgump
- Creation Date: Mon Jul 20 2020 05:46:57 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 27 2020 00:35:58 GMT+0800 (Singapore Standard Time)

<p>
Tough problem to do without the help of test cases. You just have to try and fail at various tree arrangments to realize there are only three major cases as documented in the code.

```
class Solution:
    def moveSubTree(self, root: \'Node\', p: \'Node\', q: \'Node\') -> \'Node\':
        # return True if y is under the subtree of x
        def in_subtree(y, x):
            nodes = [x]
            while len(nodes) > 0:
                new_nodes = []
                for u in nodes:
                    if y == u:
                        return True
                    new_nodes += u.children
                nodes = new_nodes
            return False
        
        # returns the parent node of x and x\'s position in the children array
        def get_parent(node, x):
            for i in range(len(node.children)):
                if x == node.children[i]:
                    return [node, i]
                result = get_parent(node.children[i], x)
                if result != None:
                    return result
            return None
        
        # case A: p is a direct child of q, so do nothing
        for c in q.children:
            if p == c:
                return root
        
        # case B: q is in the subtree of p (p could potentially be the root)
        if in_subtree(q, p):
            # First cut out q from p\'s subtree
            q_parent, child_node_idx = get_parent(root, q)
            q_parent.children = q_parent.children[:child_node_idx] + q_parent.children[child_node_idx+1:]
            if p != root:
                # Graft q to be under the parent of p at the same index as p, replacing p\'s place, 
                p_parent, child_node_idx = get_parent(root, p)
                p_parent.children[child_node_idx] = q
                # Finally add p to the end of q\'s children
                q.children.append(p)
                return root
            else:
                # q will be the new root with p as its child
                q.children.append(p)
                return q
                
        # Case C, the normal case: q is not in the subtree of p (so p also cannot be the root because
        # if it were, q would be in the subtree of p)
        p_parent, p_idx = get_parent(root, p) # p is not the root so its parent is guaranteed to exist
        # Cut p out of its parent\'s children list
        p_parent.children = p_parent.children[:p_idx] + p_parent.children[p_idx+1:]
        # And add it to q\'s children
        q.children.append(p)
        return root
</p>


### [Java] - Easy to understand code with comments
- Author: philip2207
- Creation Date: Sat Sep 05 2020 21:05:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 05 2020 21:07:50 GMT+0800 (Singapore Standard Time)

<p>
```java
public Node moveSubTree(Node root, Node p, Node q) {
	Map<Node, Node> parents = new HashMap<>();
	getParents(root, parents);

	// Q is already parent of P.
	if (parents.get(p) == q) {
		return root;
	}

	// If P is not ancestor of Q, then remove P from its parent and add to Q.
	if (!isAncestor(p, q, parents)) {
		// Remove parentLink of P.
		removeParentLink(p, parents);

		// Add P to Q\'s children.
		q.children.add(p);
		return root;
	}

	// Remove parentLink of Q, since P is ancestor. (To prevent cycles and following the examples).
	removeParentLink(q, parents);

	// If P == root, attach P to Q and Q is root.
	if (parents.get(p) == null) {
		q.children.add(p);
		return q;
	}

	Node parentP = parents.get(p);
	int N = parentP.children.size();
	for (int i = 0; i < N; i++) {
		if (parentP.children.get(i) != p) {
			continue;
		}

		// Replace parentP child index of P with Q.
		parentP.children.set(i, q);
		// Add P to Q\'s children. Remember parentLink of Q is removed, so P is no longer ancestor of Q.
		q.children.add(p);
		break;
	}
	return root;
}

private void removeParentLink(Node node, Map<Node, Node> parents) {
	if (parents.get(node) != null) {
		parents.get(node).children.remove(node);
		parents.remove(node);
	}
}

private boolean isAncestor(Node p, Node q, Map<Node, Node> parents) {
	while (q != null) {
		q = parents.get(q);

		if (q == p) {
			return true;
		}
	}
	return false;
}

private void getParents(Node node, Map<Node, Node> parents) {
	if (node == null) {
		return;
	}

	for (Node child: node.children) {
		parents.put(child, node);
		getParents(child, parents);
	}
}
```

Note we can clean up `q.children.add(p);` but for explanation sake, it is set below removing the links (so at no point are there cycles).
</p>


