---
title: "Arithmetic Slices II - Subsequence"
weight: 423
#id: "arithmetic-slices-ii-subsequence"
---
## Description
<div class="description">
<p>A sequence of numbers is called arithmetic if it consists of at least three elements and if the difference between any two consecutive elements is the same.</p>

<p>For example, these are arithmetic sequences:</p>

<pre>
1, 3, 5, 7, 9
7, 7, 7, 7
3, -1, -5, -9</pre>

<p>The following sequence is not arithmetic.</p>

<pre>
1, 1, 2, 5, 7</pre>
&nbsp;

<p>A zero-indexed array A consisting of N numbers is given. A <b>subsequence</b> slice of that array is any sequence of integers (P<sub>0</sub>, P<sub>1</sub>, ..., P<sub>k</sub>) such that 0 &le; P<sub>0</sub> &lt; P<sub>1</sub> &lt; ... &lt; P<sub>k</sub> &lt; N.</p>

<p>A <b>subsequence</b> slice (P<sub>0</sub>, P<sub>1</sub>, ..., P<sub>k</sub>) of array A is called arithmetic if the sequence A[P<sub>0</sub>], A[P<sub>1</sub>], ..., A[P<sub>k-1</sub>], A[P<sub>k</sub>] is arithmetic. In particular, this means that k &ge; 2.</p>

<p>The function should return the number of arithmetic subsequence slices in the array A.</p>

<p>The input contains N integers. Every integer is in the range of -2<sup>31</sup> and 2<sup>31</sup>-1 and 0 &le; N &le; 1000. The output is guaranteed to be less than 2<sup>31</sup>-1.</p>
&nbsp;

<p><b>Example:</b></p>

<pre>
<b>Input:</b> [2, 4, 6, 8, 10]

<b>Output:</b> 7

<b>Explanation:</b>
All arithmetic subsequence slices are:
[2,4,6]
[4,6,8]
[6,8,10]
[2,4,6,8]
[4,6,8,10]
[2,4,6,8,10]
[2,6,10]
</pre>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Adobe - 2 (taggedByAdmin: false)
- Baidu - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1 Brute Force [Time Limit Exceeded]

**Intuition**

Enumerate all possible subsequences to see if they are arithmetic sequences.

**Algorithm**

We can use depth-first search to generate all subsequences. We can check a Subsequence is arithmetic or not by its definition.


<iframe src="https://leetcode.com/playground/yNoZjyFt/shared" frameBorder="0" width="100%" height="500" name="yNoZjyFt"></iframe>

**Complexity Analysis**

* Time complexity : $$O(2^n)$$. For each element in the array, it can be in or outside the subsequence. So the time complexity is $$O(2^n)$$.

* Space complexity : $$O(n)$$. We only need the space to store the array.

---
#### Approach #2 Dynamic Programming [Accepted]

**Intuition**

To determine an arithmetic sequence, we need at least two parameters: the first (or last) element of the sequence, and the common difference.


**Algorithm**

Starting from this point, we can easily figure out that one state representation that may work:

> `f[i][d]` denotes the number of arithmetic subsequences that ends with `A[i]` and its common difference is `d`.

Let's try to find the state transitions based on the representation above. Assume we want to append a new element `A[i]` to existing arithmetic subsequences to form new subsequences. We can append `A[i]` to an existing arithmetic subsequence, only if the difference between the sequence's last element and `A[i]` is equal to the sequence's common difference.

Thus, we can define the state transitions for the element `A[i]` intuitively :

> for all `j < i`, f[i][A[i] - A[j]] += f[j][A[i] - A[j]].

This demonstrates the appending process above to form new arithmetic subsequences.

But here comes the problem. Initially all `f[i][d]` are set to be `0`, but how can we form a new arithmetic subsequence if there are no existing subsequences before?

In the original definition of arithmetic subsequences, the length of the subsequence must be at least `3`. This makes it hard to form new subsequences if only two indices `i` and `j` are given. How about taking the subsequences of length `2` into account?

We can define `weak arithmetic subsequences` as follows:

> **Weak arithmetic subsequences** are subsequences that  consist of at least two elements and if the difference between any two consecutive elements is the same.

There are two properties of weak arithmetic subsequences that are very useful:

- For any pair `i, j (i != j)`, `A[i]` and `A[j]` can always form a weak arithmetic subsequence.

- If we can append a new element to a weak arithmetic subsequence and keep it arithmetic, then the new subsequence must be an arithmetic subsequence.

The second property is quite trival, because the only difference between arithmetic subsequences and weak arithmetic subsequences is their length.

Thus we can change the state representations accordingly:

> `f[i][d]` denotes the number of weak arithmetic subsequences that ends with `A[i]` and its common difference is `d`.

Now the state transitions are quite straightforward:

> for all `j < i`, f[i][A[i] - A[j]] += (f[j][A[i] - A[j]] + 1).

The `1` appears here because of the property one, we can form a new weak arithmetic subsequence for the pair `(i, j)`.

Now the number of all weak arithmetic subsequences is the sum of all `f[i][d]`. But how can we get the number of arithmetic subsequences that are not `weak`?

There are two ways:

- First, we can count the number of `pure weak` arithmetic subsequences directly. The `pure weak` arithmetic subsequences are the arithmetic subsequences of length `2`, so the number of `pure weak` arithmetic subsequences should be equal to the number of pairs `(i, j)`, which is $$\binom{n}{2} = \frac{n * (n - 1)}{2}.$$

- Second, for the summation `f[i][A[i] - A[j]] += (f[j][A[i] - A[j]] + 1)`, `f[j][A[i] - A[j]]` is the number of existing weak arithmetic subsequences, while `1` is the new subsequence built with `A[i]` and `A[j]`. Based on property two, when we are appending new elements to existing weak arithmetic subsequences, we are forming arithmetic subsequences. So the first part, `f[j][A[i] - A[j]]` is the number of new formed arithmetic subsequences, and can be added to the answer.

We can use the following example to illustrate the process:

> [1, 1, 2, 3, 4, 5]

We need to count the answer for the above sequence.

- For the first element `1`, there is no element in front of it, the answer remains `0`.

- For the second element `1`, the element itself with the previous `1` can form a pure weak arithmetic subsequence with common difference `0` : `[1, 1]`.

- For the third element `2`, it cannot be appended to the only weak arithmetic subsequence `[1, 1]`, so the answer remains `0`. Similar to the second element, it can form new weak arithmetic subsequences `[1, 2]` and `[1, 2]`.

- For the forth element `3`, if we append it to some arithmetic subsequences ending with `2`, these subsequences must have a common difference of `3 - 2 = 1`. Indeed there are two: `[1, 2]` and `[1, 2]`. So we can append `3` to the end of these subsequences, and the answer is added by `2`. Similar to above, it can form new weak arithmetic subsequences `[1, 3], [1, 3]` and `[2, 3]`.

- The other elements are the same, we can view the process in the figure below. The red bracket indicates the weak arithmetic subsequence of length `2`, and the black bracket indicates the arithmetic subsequence. The answer should be the total number of black brackets.

<img src="../Figures/446_Arithmetic_Slices_II_Subsequence.png" width=80% height=80% />

<iframe src="https://leetcode.com/playground/MVagoidb/shared" frameBorder="0" width="100%" height="446" name="MVagoidb"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n ^ 2)$$. We can use double loop to enumerate all possible states.

* Space complexity : $$O(n ^ 2)$$. For each `i`, we need to store at most `n` distinct common differences, so the total space complexity is $$O(n ^ 2)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Detailed explanation for Java O(n^2) solution
- Author: fun4LeetCode
- Creation Date: Sat Nov 12 2016 07:15:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 04:09:22 GMT+0800 (Singapore Standard Time)

<p>
At first glance of the problem description, I had a strong feeling that the solution to the original problem can be built through its subproblems, i.e., the total number of arithmetic subsequence slices of the whole input array can be constructed from those of the subarrays of the input array. While I was on the right track to the final solution, it's not so easy to figure out the relations between the original problem and its subproblems. 

To begin with, let's be ambitious and reformulate our problem as follows: let `T(i)` denote the total number of arithmetic subsequence slices that can be formed within subarray `A[0, i]`, where `A` is the input array and `0 <= i < n` with `n = A.length`. Then our original problem will be `T(n - 1)`, and the base case is `T(0) = 0`.

To make the above idea work, we need to relate `T(i)` to all `T(j)` with `0 <= j < i`. Let's take some specific `j` as an example. If we want to incorporate element `A[i]` into the subarray `A[0, j]`, what information do we need? As far as I can see, we need to know at least the total number of arithmetic subsequence slices ending at each index `k` with difference `d` where `0 <= k <= j` and `d = A[i] - A[k]`, (i.e., for each such slice, its last element is `A[k]` and the difference between every two consecutive elements is `d`), so that adding `A[i]` to the end of each such slice will make a new arithmetic subsequence slice.

However, our original formulation of `T(i)` says nothing about the the total number of arithmetic subsequence slices ending at some particular index and with some particular difference. This renders it impossible to relate `T(i)` to all `T(j)`. **As a rule of thumb, when there is difficulty relating original problem to its subproblems, it usually indicates something goes wrong with your formulation for the original problem.** 

From our analyses above, each intermediate solution should at least contain information about the total number of arithmetic subsequence slices ending at some particular index with some particular difference. So let's go along this line and reformulate our problem as `T(i, d)`, which denotes the total number of arithmetic subsequence slices ending at index `i` with difference `d`. The base case and recurrence relation are as follows:
1. Base case: `T(0, d) = 0` (This is true for any `d`).
2. Recurrence relation: `T(i, d) = summation of (1 + T(j, d))` as long as `0 <= j < i && d == A[i] - A[j]`.

For the recurrence relation, it's straightforward to understand the `T(j, d)` part: for each slice ending at index `j` with difference `d == A[i] - A[j]`, adding `A[i]` to the end of the slice will make a new arithmetic subsequence slice, therefore the total number of such new slices will be the same as `T(j, d)`. What you are probably wondering is: where does the `1` come from?

The point here is that to make our recurrence relation work properly, the meaning of arithmetic subsequence slice has to be extended to include slices with only `two elements` (of course we will make sure these "phony" slices won't contribute to our final count). This is because for each slice, we are adding `A[i]` to its end to form a new one. If the original slice is of length two, after adding we will have a valid arithmetic subsequence slice with three elements. Our `T(i, d)` will include all these "generalized" slices. And for each pair of elements `(A[j], A[i])`, they will form one such "generalized" slice (with only two elements) and thus contribute to one count of `T(i, d)`.

Before jumping to the solution below, I'd like to point out that there are actually overlapping among our subproblems (for example, both `T(i, d)` and `T(i + 1, d)` require knowledge of `T(j, d)` with `0 <= j < i`). This necessitates memorization of the intermediate results. Each intermediate result is characterized by two integers: `i` and `d`. The former is bounded (i.e., `0 <= i < n`) since they are the indices of the element in the input array while the latter is not as `d` is the difference of two elements in the input array and can be any value. For bounded integers, we can use them to index arrays (or lists) while for unbounded ones, use of HashMap would be more appropriate. So we end up with an array of the same length as the input and whose element type is HashMap.

Here is the Java program (with a quick explanation given at the end). Both time and space complexities are `O(n^2)`. Some minor points for improving the time and space performance are:
1. Define the type of the difference as Integer type instead of Long. This is because there is no valid arithmetic subsequence slice that can have difference out of the Integer value range. But we do need a long integer to filter out those invalid cases.
2. Preallocate the HashMap to avoid reallocation to deal with extreme cases.
3. Refrain from using lambda expressions inside loops.

```
public int numberOfArithmeticSlices(int[] A) {
    int res = 0;
    Map<Integer, Integer>[] map = new Map[A.length];
		
    for (int i = 0; i < A.length; i++) {
        map[i] = new HashMap<>(i);
        	
        for (int j = 0; j < i; j++) {
            long diff = (long)A[i] - A[j];
            if (diff <= Integer.MIN_VALUE || diff > Integer.MAX_VALUE) continue;
        		
            int d = (int)diff;
            int c1 = map[i].getOrDefault(d, 0);
            int c2 = map[j].getOrDefault(d, 0);
            res += c2;
            map[i].put(d, c1 + c2 + 1);
        }
    }
		
    return res;
}
```
Quick explanation:
1. `res` is the final count of all valid arithmetic subsequence slices; `map` will store the intermediate results `T(i, d)`, with `i` indexed into the array and `d` as the key of the corresponding HashMap.
2. For each index `i`, we find the total number of "generalized" arithmetic subsequence slices ending at it with all possible differences. This is done by attaching `A[i]` to all slices of `T(j, d)` with `j` less than `i`.
3. Within the inner loop, we first use a long variable `diff` to filter out invalid cases, then get the counts of all valid slices (with element >= 3) as `c2` and add it to the final count. At last we update the count of all "generalized" slices for `T(i, d)` by adding the three parts together: the original value of `T(i, d)`, which is `c1` here, the counts from `T(j, d)`, which is `c2` and lastly the `1` count of the "two-element" slice `(A[j], A[i])`.
</p>


### 11 line Python O(n^2) solution
- Author: joey13
- Creation Date: Sun Nov 13 2016 07:44:51 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 06:22:30 GMT+0800 (Singapore Standard Time)

<p>
```
def numberOfArithmeticSlices(self, A):
    from collections import defaultdict
    total = 0
    dp = [defaultdict(int) for item in A]
    for i in xrange(len(A)):
        for j in xrange(i):
            dp[i][A[i] - A[j]] += 1
            if A[i]-A[j] in dp[j]:
                dp[i][A[i] - A[j]] += dp[j][A[i]-A[j]]
                total += dp[j][A[i]-A[j]]
    return total
```
We memoize with an array of dicts, `dp`, such that `dp[i][j]` stores the number of arithmetic slices (including those with only length 2) whose constant difference is `j` ending at `i`. The key is basically to store all 2+-length arithmetic slices (which is helps to build up the solution from its sub-problems)  while only adding valid 3+-length slices to the total.

The we iterate over all pairs in the array. Each `(A[j], A[i])` is a 2-length slice with constant difference `A[i] - A[j]` that we've never encountered before, so increment `dp[i][A[i] - A[j]]` by 1 (but leave the total as is, because its not length 3 or more). 

If there are any slices with `A[i] - A[j]` length that finish at index `j` (`if A[i]-A[j] in dp[j]:`), we 'extend' them to index `i` and add to the total, since any slice that terminated at index `j` would now have at least length 3 terminating at `i`.
</p>


### JAVA 15 lines solution
- Author: aaaeeeo
- Creation Date: Wed Nov 09 2016 08:24:24 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 09 2016 08:24:24 GMT+0800 (Singapore Standard Time)

<p>
```Java
    public int numberOfArithmeticSlices(int[] A) {
        int re = 0;
        HashMap<Integer, Integer>[] maps = new HashMap[A.length];
        for(int i=0; i<A.length; i++) {
            maps[i] = new HashMap<>();
            int num = A[i];
            for(int j=0; j<i; j++) {
                if((long)num-A[j]>Integer.MAX_VALUE) continue;
                if((long)num-A[j]<Integer.MIN_VALUE) continue;
                int diff = num - A[j];
                int count = maps[j].getOrDefault(diff, 0);
                maps[i].put(diff, maps[i].getOrDefault(diff,0)+count+1);
                re += count;
            }
        }
        return re;
    }
```
</p>


