---
title: "Diagonal Traverse II"
weight: 1318
#id: "diagonal-traverse-ii"
---
## Description
<div class="description">
Given a list of lists of integers,&nbsp;<code>nums</code>,&nbsp;return all elements of <code>nums</code> in diagonal order as shown in the below images.
<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/04/08/sample_1_1784.png" style="width: 158px; height: 143px;" /></strong></p>

<pre>
<strong>Input:</strong> nums = [[1,2,3],[4,5,6],[7,8,9]]
<strong>Output:</strong> [1,4,2,7,5,3,8,6,9]
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/04/08/sample_2_1784.png" style="width: 230px; height: 177px;" /></strong></p>

<pre>
<strong>Input:</strong> nums = [[1,2,3,4,5],[6,7],[8],[9,10,11],[12,13,14,15,16]]
<strong>Output:</strong> [1,6,2,8,7,3,9,4,12,10,5,13,11,14,15,16]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [[1,2,3],[4],[5,6,7],[8],[9,10,11]]
<strong>Output:</strong> [1,4,2,5,3,8,6,9,7,10,11]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [[1,2,3,4,5,6]]
<strong>Output:</strong> [1,2,3,4,5,6]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= nums[i].length &lt;=&nbsp;10^5</code></li>
	<li><code>1 &lt;= nums[i][j] &lt;= 10^9</code></li>
	<li>There at most <code>10^5</code> elements in <code>nums</code>.</li>
</ul>

</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Facebook - 7 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Clean, Simple] Easiest Explanation with a picture and code with comments
- Author: interviewrecipes
- Creation Date: Sun Apr 26 2020 12:03:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 08 2020 13:22:49 GMT+0800 (Singapore Standard Time)

<p>
**Key Idea**
In a 2D matrix, elements in the same diagonal have same sum of their indices.

![image](https://assets.leetcode.com/users/interviewrecipes/image_1591684927.png)



So if we have all elements with same sum of their indices together, then it\u2019s just a matter of printing those elements in order.

**Algorithm**
1. Insert all elements into an appropriate bucket i.e. nums[i][j] in (i+j)th bucket.
2. For each bucket starting from 0 to max, print all elements in the bucket. 
**Note**: Here, diagonals are from bottom to top, but we traversed the input matrix from first row to last row. Hence we need to print the elements in reverse order.

Please upvote if you liked the idea, it would be encouraging. If you like this, checkout my blog/website for posts on coding interviews, programming, data structures and algorithms, problem solving.

**C++**
```
vector<int> findDiagonalOrder(vector<vector<int>>& nums) {
        vector<int> answer;
        unordered_map<int, vector<int>> m;
        int maxKey = 0; // maximum key inserted into the map i.e. max value of i+j indices.
        
        for (int i=0; i<nums.size(); i++) {
            for (int j=0; j<nums[i].size(); j++) {
                m[i+j].push_back(nums[i][j]); // insert nums[i][j] in bucket (i+j).
                maxKey = max(maxKey, i+j); // 
            }
        }
        for (int i=0; i<= maxKey; i++) { // Each diagonal starting with sum 0 to sum maxKey.
            for (auto x = m[i].rbegin(); x != m[i].rend(); x++) { // print in reverse order.
                answer.push_back(*x); 
            }
        }
        
        return answer;
    }

```
</p>


### [Java/C++] HashMap with Picture - Clean code - O(N)
- Author: hiepit
- Creation Date: Sun Apr 26 2020 12:01:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 26 2020 15:41:49 GMT+0800 (Singapore Standard Time)

<p>
Example:
![image](https://assets.leetcode.com/users/hiepit/image_1587879572.png)

**Java**
```java
class Solution {
    public int[] findDiagonalOrder(List<List<Integer>> nums) {
        int n = 0, i = 0, maxKey = 0;
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int r = nums.size() - 1; r >= 0; --r) { // The values from the bottom rows are the starting values of the diagonals.
            for (int c = 0; c < nums.get(r).size(); ++c) {
                map.putIfAbsent(r + c, new ArrayList<>());
                map.get(r + c).add(nums.get(r).get(c));
                maxKey = Math.max(maxKey, r + c);
                n++;
            }
        }
        int[] ans = new int[n];
        for (int key = 0; key <= maxKey; ++key) {
            List<Integer> value = map.get(key);
            if (value == null) continue;
            for (int v : value) ans[i++] = v;
        }
        return ans;
    }
}
```
**C++**
```c++
class Solution {
public:
    vector<int> findDiagonalOrder(vector<vector<int>>& nums) {
        int maxKey = 0;
        unordered_map<int, vector<int>> map;
        for (int r = nums.size() - 1; r >= 0; --r) { // The values from the bottom rows are the starting values of the diagonals.
            for (int c = 0; c < nums[r].size(); ++c) {
                map[r + c].push_back(nums[r][c]);
                maxKey = max(maxKey, r + c);
            }
        }
        vector<int> ans;
        for (int key = 0; key <= maxKey; ++key)
            for (int v : map[key]) ans.push_back(v);
        return ans;
    }
};
```
Time: `O(N)`, where `N` is the number of elements of `nums`
Space: `O(N)`
</p>


### [Python] One pass
- Author: lee215
- Creation Date: Sun Apr 26 2020 12:08:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 26 2020 12:08:14 GMT+0800 (Singapore Standard Time)

<p>
Time `O(A)`, Space `O(A)`

**Python:**
```py
    def findDiagonalOrder(self, A):
        res = []
        for i, r in enumerate(A):
            for j, a in enumerate(r):
                if len(res) <= i + j:
                    res.append([])
                res[i + j].append(a)
        return [a for r in res for a in reversed(r)]
```

</p>


