---
title: "Best Time to Buy and Sell Stock"
weight: 121
#id: "best-time-to-buy-and-sell-stock"
---
## Description
<div class="description">
<p>Say you have an array for which the <em>i</em><sup>th</sup> element is the price of a given stock on day <em>i</em>.</p>

<p>If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock), design an algorithm to find the maximum profit.</p>

<p>Note that you cannot sell a stock before you buy one.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> [7,1,5,3,6,4]
<strong>Output:</strong> 5
<strong>Explanation:</strong> Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
&nbsp;            Not 7-1 = 6, as selling price needs to be larger than buying price.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> [7,6,4,3,1]
<strong>Output:</strong> 0
<strong>Explanation:</strong> In this case, no transaction is done, i.e. max profit = 0.
</pre>

</div>

## Tags
- Array (array)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 36 (taggedByAdmin: true)
- Facebook - 13 (taggedByAdmin: true)
- Google - 7 (taggedByAdmin: false)
- Bloomberg - 6 (taggedByAdmin: true)
- Microsoft - 5 (taggedByAdmin: true)
- Goldman Sachs - 5 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- JPMorgan - 4 (taggedByAdmin: false)
- ByteDance - 4 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Morgan Stanley - 3 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Oracle - 7 (taggedByAdmin: false)
- Uber - 5 (taggedByAdmin: true)
- Walmart Labs - 4 (taggedByAdmin: false)
- Expedia - 3 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Zillow - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Audible - 2 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- DoorDash - 17 (taggedByAdmin: false)
- Tencent - 3 (taggedByAdmin: false)
- Akuna Capital - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- Citrix - 2 (taggedByAdmin: false)
- Grab - 2 (taggedByAdmin: false)
- BlackRock - 2 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- Deutsche Bank - 2 (taggedByAdmin: false)
- Groupon - 2 (taggedByAdmin: false)
- Redfin - 2 (taggedByAdmin: false)
- Intel - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

We need to find out the maximum difference (which will be the maximum profit) between two numbers in the given array. Also, the second number (selling price) must be larger than the first one (buying price).

In formal terms, we need to find $$\max(\text{prices[j]} - \text{prices[i]})$$, for every $$i$$ and $$j$$ such that $$j > i$$.

---
#### Approach 1: Brute Force

<iframe src="https://leetcode.com/playground/tVDNcEFQ/shared" frameBorder="0" width="100%" height="276" name="tVDNcEFQ"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Loop runs $$\dfrac{n (n-1)}{2}$$ times.

* Space complexity : $$O(1)$$. Only two variables - $$\text{maxprofit}$$ and $$\text{profit}$$ are used.
<br />
<br />
---
#### Approach 2: One Pass

**Algorithm**

Say the given array is:

[7, 1, 5, 3, 6, 4]

If we plot the numbers of the given array on a graph, we get:

![Profit Graph](https://leetcode.com/media/original_images/121_profit_graph.png)

The points of interest are the peaks and valleys in the given graph. We need to find the largest peak following the smallest valley.
We can maintain two variables - minprice and maxprofit corresponding to the smallest valley and maximum profit (maximum difference between selling price and minprice) obtained so far respectively.

<iframe src="https://leetcode.com/playground/WQBerWww/shared" frameBorder="0" width="100%" height="276" name="WQBerWww"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Only a single pass is needed.

* Space complexity : $$O(1)$$. Only two variables are used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Kadane's Algorithm - Since no one has mentioned about this so far :) (In case if interviewer twists the input)
- Author: earlme
- Creation Date: Mon Jul 27 2015 10:22:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 08:43:29 GMT+0800 (Singapore Standard Time)

<p>
The logic to solve this problem is same as "max subarray problem" using `Kadane's Algorithm`. Since no body has mentioned this so far, I thought it's a good thing for everybody to know. 

All the straight forward solution should work, but if the interviewer twists the question slightly by giving the ***difference array of prices***, Ex: for `{1, 7, 4, 11}`, if he gives `{0, 6, -3, 7}`, you might end up being confused. 

Here, the logic is to calculate the difference (`maxCur += prices[i] - prices[i-1]`) of the original array, and find a contiguous subarray giving maximum profit. If the difference falls below 0, reset it to zero.


        public int maxProfit(int[] prices) {
            int maxCur = 0, maxSoFar = 0;
            for(int i = 1; i < prices.length; i++) {
                maxCur = Math.max(0, maxCur += prices[i] - prices[i-1]);
                maxSoFar = Math.max(maxCur, maxSoFar);
            }
            return maxSoFar;
        }

*`maxCur = current maximum value`

*`maxSoFar = maximum value found so far`
</p>


### Sharing my simple and clear C++ solution
- Author: zxyperfect
- Creation Date: Wed Dec 10 2014 08:07:23 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 19:07:02 GMT+0800 (Singapore Standard Time)

<p>
    int maxProfit(vector<int> &prices) {
        int maxPro = 0;
        int minPrice = INT_MAX;
        for(int i = 0; i < prices.size(); i++){
            minPrice = min(minPrice, prices[i]);
            maxPro = max(maxPro, prices[i] - minPrice);
        }
        return maxPro;
    }

minPrice is the minimum price from day 0 to day i. And maxPro is the maximum profit we can get from day 0 to day i. 

How to get maxPro? Just get the larger one between current maxPro and prices[i] - minPrice.
</p>


### Easy O(n) Python solution
- Author: girikuncoro
- Creation Date: Tue Jan 05 2016 09:29:54 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 14:13:39 GMT+0800 (Singapore Standard Time)

<p>
    def maxProfit(prices):
        max_profit, min_price = 0, float('inf')
        for price in prices:
            min_price = min(min_price, price)
            profit = price - min_price
            max_profit = max(max_profit, profit)
        return max_profit
</p>


