---
title: "Decompress Run-Length Encoded List"
weight: 1116
#id: "decompress-run-length-encoded-list"
---
## Description
<div class="description">
<p>We are given a list <code>nums</code> of integers representing a list compressed with run-length encoding.</p>

<p>Consider each adjacent pair&nbsp;of elements <code>[freq, val] = [nums[2*i], nums[2*i+1]]</code>&nbsp;(with <code>i &gt;= 0</code>).&nbsp; For each such pair, there are <code>freq</code> elements with value <code>val</code> concatenated in a sublist. Concatenate all the sublists from left to right to generate the decompressed list.</p>

<p>Return the decompressed list.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4]
<strong>Output:</strong> [2,4,4,4]
<strong>Explanation:</strong> The first pair [1,2] means we have freq = 1 and val = 2 so we generate the array [2].
The second pair [3,4] means we have freq = 3 and val = 4 so we generate [4,4,4].
At the end the concatenation [2] + [4,4,4] is [2,4,4,4].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,1,2,3]
<strong>Output:</strong> [1,3,3]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= nums.length &lt;= 100</code></li>
	<li><code>nums.length % 2 == 0</code></li>
	<li><code><font face="monospace">1 &lt;= nums[i] &lt;= 100</font></code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Google - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Bad description
- Author: ebou
- Creation Date: Sun Jan 12 2020 00:04:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 12 2020 00:04:15 GMT+0800 (Singapore Standard Time)

<p>
I found the description of the question very vague and confusing
</p>


### C++ vector.insert(end, a, b)
- Author: StefanPochmann
- Creation Date: Sun Jan 12 2020 08:13:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 12 2020 08:30:07 GMT+0800 (Singapore Standard Time)

<p>
```
vector<int> decompressRLElist(vector<int>& nums) {
    vector<int> res;
    for (int i=0; i<nums.size(); i+=2)
        res.insert(res.end(), nums[i], nums[i+1]);
    return res;
}
```
I think I checked all C++ solutions posted so far, and nobody used that `insert`... I guess it\'s not well-known? (I didn\'t know it, either, but I\'m no C++ expert :-)
</p>


### Java 100% time
- Author: nikopol86
- Creation Date: Thu Feb 27 2020 08:25:16 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Feb 27 2020 08:26:52 GMT+0800 (Singapore Standard Time)

<p>
```java
class Solution {
    public int[] decompressRLElist(int[] nums) {
        int arrSize = 0;
        for (int i = 0; i < nums.length; i += 2) {
            arrSize += nums[i];
        }
        
        int[] result = new int[arrSize];

        int startIdx = 0;
        for (int i = 0; i < nums.length; i += 2) {
            Arrays.fill(result, startIdx, startIdx + nums[i], nums[i + 1]);
            startIdx += nums[i];
        }
        return result;
    }
}
```
</p>


