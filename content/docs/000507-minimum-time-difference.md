---
title: "Minimum Time Difference"
weight: 507
#id: "minimum-time-difference"
---
## Description
<div class="description">
Given a list of 24-hour clock time points in "Hour:Minutes" format, find the minimum <b>minutes</b> difference between any two time points in the list. 

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> ["23:59","00:00"]
<b>Output:</b> 1
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The number of time points in the given list is at least 2 and won't exceed 20000.</li>
<li>The input time is legal and ranges from 00:00 to 23:59.</li>
</ol>
</p>
</div>

## Tags
- String (string)

## Companies
- Palantir Technologies - 2 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Verbose Java Solution, Bucket
- Author: shawngao
- Creation Date: Sun Mar 12 2017 12:19:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 03:03:06 GMT+0800 (Singapore Standard Time)

<p>
There is only 24 * 60 = 1440 possible time points. Just create a boolean array, each element stands for if we see that time point or not. Then things become simple...

```
public class Solution {
    public int findMinDifference(List<String> timePoints) {
        boolean[] mark = new boolean[24 * 60];
        for (String time : timePoints) {
            String[] t = time.split(":");
            int h = Integer.parseInt(t[0]);
            int m = Integer.parseInt(t[1]);
            if (mark[h * 60 + m]) return 0;
            mark[h * 60 + m] = true;
        }
        
        int prev = 0, min = Integer.MAX_VALUE;
        int first = Integer.MAX_VALUE, last = Integer.MIN_VALUE;
        for (int i = 0; i < 24 * 60; i++) {
            if (mark[i]) {
                if (first != Integer.MAX_VALUE) {
                    min = Math.min(min, i - prev);
                }
                first = Math.min(first, i);
                last = Math.max(last, i);
                prev = i;
            }
        }
        
        min = Math.min(min, (24 * 60 - last + first));
        
        return min;
    }
}
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Mar 12 2017 12:51:03 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 19 2018 11:57:56 GMT+0800 (Singapore Standard Time)

<p>
Convert each timestamp to it's integer number of minutes past midnight, and sort the array of minutes.
The required minimum difference must be a difference between two adjacent elements in the circular array (so the last element is "adjacent" to the first.)  We take the minimum value of all of them.

```
def findMinDifference(self, A):
    def convert(time):
        return int(time[:2]) * 60 + int(time[3:])
    minutes = map(convert, A)
    minutes.sort()
    
    return min( (y - x) % (24 * 60) 
                for x, y in zip(minutes, minutes[1:] + minutes[:1]) )
```
</p>


### Java 10 liner solution. Simplest so far
- Author: hu20
- Creation Date: Wed Mar 15 2017 12:05:10 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 10:36:07 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public int findMinDifference(List<String> timePoints) {
        int mm = Integer.MAX_VALUE;
        List<Integer> time = new ArrayList<>();
        
        for(int i = 0; i < timePoints.size(); i++){
            Integer h = Integer.valueOf(timePoints.get(i).substring(0, 2));
            time.add(60 * h + Integer.valueOf(timePoints.get(i).substring(3, 5)));
        }
        
        Collections.sort(time, (Integer a, Integer b) -> a - b);
        
        for(int i = 1; i < time.size(); i++){
            System.out.println(time.get(i));
            mm = Math.min(mm, time.get(i) - time.get(i-1));
        }
        
        int corner = time.get(0) + (1440 - time.get(time.size()-1));
        return Math.min(mm, corner);
    }
}
</p>


