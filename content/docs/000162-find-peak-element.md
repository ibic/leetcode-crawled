---
title: "Find Peak Element"
weight: 162
#id: "find-peak-element"
---
## Description
<div class="description">
<p>A peak element is an element that is greater than its neighbors.</p>

<p>Given an input array <code>nums</code>, where <code>nums[i] &ne; nums[i+1]</code>, find a peak element and return its index.</p>

<p>The array may contain multiple peaks, in that case return the index to any one of the peaks is fine.</p>

<p>You may imagine that <code>nums[-1] = nums[n] = -&infin;</code>.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> <strong>nums</strong> = <code>[1,2,3,1]</code>
<strong>Output:</strong> 2
<strong>Explanation:</strong> 3 is a peak element and your function should return the index number 2.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> <strong>nums</strong> = <code>[</code>1,2,1,3,5,6,4]
<strong>Output:</strong> 1 or 5 
<strong>Explanation:</strong> Your function can return either index number 1 where the peak element is 2, 
&nbsp;            or index number 5 where the peak element is 6.
</pre>

<p><strong>Follow up:</strong>&nbsp;Your solution should be in logarithmic complexity.</p>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Facebook - 11 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Yahoo - 6 (taggedByAdmin: false)
- Quora - 6 (taggedByAdmin: false)
- IXL - 3 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)

## Official Solution
[TOC]


## Solution

---
#### Approach 1: Linear Scan

In this approach, we make use of the fact that two consecutive numbers $$nums[j]$$ and $$nums[j + 1]$$ are never equal. Thus, we can traverse over the $$nums$$ array starting from the beginning. Whenever, we find a number $$nums[i]$$, we only need to check if it is larger than the next number $$nums[i+1]$$ for determining if $$nums[i]$$ is the peak element. The reasoning behind this can be understood by taking the following three cases which cover every case into which any problem can be divided.


Case 1. All the numbers appear in a descending order. In this case, the first element corresponds to the peak element. We start off by checking if the current element is larger than the next one. The first element satisfies this criteria, and is hence identified as the peak correctly. In this case, we didn't reach a point where we needed to compare $$nums[i]$$ with $$nums[i-1]$$ also, to determine if it is the peak element or not.

![Graph](../Figures/162/Find_Peak_Case1.PNG)
{:align="center"}

Case 2. All the elements appear in ascending order. In this case, we keep on comparing $$nums[i]$$ with $$nums[i+1]$$ to determine if $$nums[i]$$ is the peak element or not. None of the elements satisfy this criteria, indicating that we are currently on a rising slope and not on a peak. Thus, at the end, we need to return the last element as the peak element, which turns out to be correct. In this case also, we need not compare $$nums[i]$$ with $$nums[i-1]$$, since being on the rising slope is a sufficient condition to ensure that $$nums[i]$$ isn't the peak element.

![Graph](../Figures/162/Find_Peak_Case2.PNG)
{:align="center"}

Case 3. The peak appears somewhere in the middle. In this case, when we are traversing on the rising edge, as in Case 2, none of the elements will satisfy $$nums[i] > nums[i + 1]$$. We need not compare $$nums[i]$$ with $$nums[i-1]$$ on the rising slope as discussed above. When we finally reach the peak element, the condition $$nums[i] > nums[i + 1]$$ is satisfied. We again, need not compare $$nums[i]$$ with $$nums[i-1]$$. This is because, we could reach $$nums[i]$$ as the current element only when the check $$nums[i] > nums[i + 1]$$ failed for the previous($$(i-1)^{th}$$ element, indicating that $$nums[i-1] < nums[i]$$. Thus, we are able to identify the peak element correctly in this case as well.

![Graph](../Figures/162/Find_Peak_Case3.PNG)
{:align="center"}

<iframe src="https://leetcode.com/playground/MLfS4Quj/shared" frameBorder="0" width="100%" height="208" name="MLfS4Quj"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We traverse the $$nums$$ array of size $$n$$ once only.

* Space complexity : $$O(1)$$. Constant extra space is used.
<br />
<br />
---
#### Approach 2: Recursive Binary Search

**Algorithm**

We can view any given sequence in $$nums$$ array as alternating ascending and descending sequences. By making use of this, and the fact that we can return any peak as the result, we can make use of Binary Search to find the required peak element.

In case of simple Binary Search, we work on a sorted sequence of numbers and try to find out the required number by reducing the search space at every step. In this case, we use a modification of this simple Binary Search to our advantage. We start off by finding the middle element, $$mid$$ from the given $$nums$$ array. If this element happens to be lying in a descending sequence of numbers. or a local falling slope(found by comparing $$nums[i]$$ to its right neighbour), it means that the peak will always lie towards the left of this element. Thus, we reduce the search space to the left of $$mid$$(including itself) and perform the same process on left subarray.

If the middle element, $$mid$$ lies in an ascending sequence of numbers, or a rising slope(found by comparing $$nums[i]$$ to its right neighbour), it obviously implies that the peak lies towards the right of this element. Thus, we reduce the search space to the right of $$mid$$ and perform the same process on the right subarray.

In this way, we keep on reducing the search space till we eventually reach a state where only one element is remaining in the search space. This single element is the peak element.

To see how it works, let's consider the three cases discussed above again.

Case 1. In this case, we firstly find $$3$$ as the middle element. Since it lies on a falling slope, we reduce the search space to `[1, 2, 3]`. For this subarray, $$2$$ happens to be the middle element, which again lies on a falling slope, reducing the search space to `[1, 2]`. Now, $$1$$ acts as the middle element and it lies on a falling slope, reducing the search space to `[1]` only. Thus, $$1$$ is returned as the peak correctly.

!?!../Documents/Find_Peak_Case1.json:1000,563!?!

Case 2. In this case, we firstly find $$3$$ as the middle element. Since it lies on a rising slope, we reduce the search space to `[4, 5]`. Now, $$4$$ acts as the middle element for this subarray and it lies on a rising slope, reducing the search space to `[5]` only. Thus, $$5$$ is returned as the peak correctly.

!?!../Documents/Find_Peak_Case2.json:1000,563!?!

Case 3. In this case, the peak lies somewhere in the middle. The first middle element is $$4$$. It lies on a rising slope, indicating that the peak lies towards its right. Thus, the search space is reduced to `[5, 1]`. Now, $$5$$ happens to be the on a falling slope(relative to its right neighbour), reducing the search space to `[5]` only. Thus, $$5$$ is identified as the peak element correctly.

!?!../Documents/Find_Peak_Case3.json:1000,563!?!

<iframe src="https://leetcode.com/playground/3MGjFqJ4/shared" frameBorder="0" width="100%" height="276" name="3MGjFqJ4"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(log_2(n)\big)$$. We reduce the search space in half at every step. Thus, the total search space will be consumed in $$log_2(n)$$ steps. Here, $$n$$ refers to the size of $$nums$$ array.

* Space complexity : $$O\big(log_2(n)\big)$$. We reduce the search space in half at every step. Thus, the total search space will be consumed in $$log_2(n)$$ steps. Thus, the depth of recursion tree will go upto $$log_2(n)$$.
<br />
<br />
---
#### Approach 3: Iterative Binary Search

**Algorithm**

The binary search discussed in the previous approach used a recursive method. We can do the same process in an iterative fashion also. This is done in the current approach.

<iframe src="https://leetcode.com/playground/EnevWycv/shared" frameBorder="0" width="100%" height="276" name="EnevWycv"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(log_2(n)\big)$$. We reduce the search space in half at every step. Thus, the total search space will be consumed in $$log_2(n)$$ steps. Here, $$n$$ refers to the size of $$nums$$ array.

* Space complexity : $$O(1)$$. Constant extra space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Find the maximum by binary search (recursion and iteration)
- Author: gangan
- Creation Date: Fri Dec 05 2014 22:07:35 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 05:07:59 GMT+0800 (Singapore Standard Time)

<p>
Consider that each local maximum is one valid peak.
My solution is to find one local maximum with binary search.
Binary search satisfies the O(logn) computational complexity.

Binary Search: recursion

    class Solution {
    public:

    int findPeakElement(const vector<int> &num) {
        return Helper(num, 0, num.size()-1);
    }
    int Helper(const vector<int> &num, int low, int high)
    {
        if(low == high)
            return low;
        else
        {
            int mid1 = (low+high)/2;
            int mid2 = mid1+1;
            if(num[mid1] > num[mid2])
                return Helper(num, low, mid1);
            else
                return Helper(num, mid2, high);
        }
    }
    };

Binary Search: iteration

    class Solution {
    public:
        int findPeakElement(const vector<int> &num) 
        {
            int low = 0;
            int high = num.size()-1;
            
            while(low < high)
            {
                int mid1 = (low+high)/2;
                int mid2 = mid1+1;
                if(num[mid1] < num[mid2])
                    low = mid2;
                else
                    high = mid1;
            }
            return low;
        }
    };

Sequential Search:

    class Solution {
    public:
        int findPeakElement(const vector<int> &num) {
            for(int i = 1; i < num.size(); i ++)
            {
                if(num[i] < num[i-1])
                {// <
                    return i-1;
                }
            }
            return num.size()-1;
        }
    };
</p>


### Java solution and explanation using invariants
- Author: cosmin79
- Creation Date: Thu Nov 12 2015 04:04:48 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 14 2018 10:34:05 GMT+0800 (Singapore Standard Time)

<p>
I find it useful to reason about binary search problems using invariants. While there are many solutions posted here, neither of them provide (in my opinion) a good explanation about why they work. I just spent some time thinking about this and I thought it might be a good idea to share my thoughts.

Assume we initialize left = 0, right = nums.length - 1. The invariant I'm using is the following:

**nums[left - 1] < nums[left] && nums[right] > nums[right + 1]**

That basically means that in the current interval we're looking, [left, right] the function started increasing to left and will eventually decrease at right. The behavior between [left, right] falls into the following 3 categories:

1) nums[left] > nums[left + 1]. From the invariant, nums[left - 1] < nums[left] => left is a peak

2) The function is increasing from left to right i.e. nums[left] < nums[left + 1] < .. < nums[right - 1] < nums[right]. From the invariant, nums[right] > nums[right + 1] => right is a peak

3) the function increases for a while and then decreases (in which case the point just before it starts decreasing is a peak) e.g. 2 5 6 3 (6 is the point in question)


As shown, if the invariant above holds, there is at least a peak between [left, right]. Now we need to show 2 things:

I) the invariant is initially true. Since left = 0 and right = nums.length - 1 initially and we know that nums[-1] = nums[nums.length] = -oo, this is obviously true

II) At every step of the loop the invariant gets reestablished. If we consider the code in the loop, we have mid = (left + right) / 2 and the following 2 cases:

a) nums[mid] < nums[mid + 1]. It turns out that the interval [mid + 1, right] respects the invariant (nums[mid] < nums[mid + 1] -> part of the cond + nums[right] > nums[right + 1] -> part of the invariant in the previous loop iteration)

b) nums[mid] > nums[mid + 1]. Similarly, [left, mid] respects the invariant (nums[left - 1] < nums[left] -> part of the invariant in the previous loop iteration and nums[mid] > nums[mid + 1] -> part of the cond)

As a result, the invariant gets reestablished and it will also hold when we exit the loop. In that case we have an interval of length 2 i.e. right = left + 1. If nums[left] > nums[right], using the invariant (nums[left - 1] < nums[left]), we get that left is a peak. Otherwise right is the peak (nums[left] < nums[right] and nums[right] < nums[right + 1] from the invariant).

    public int findPeakElement(int[] nums) {
        int N = nums.length;
        if (N == 1) {
            return 0;
        }
       
        int left = 0, right = N - 1;
        while (right - left > 1) {
            int mid = left + (right - left) / 2;
            if (nums[mid] < nums[mid + 1]) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        
        return (left == N - 1 || nums[left] > nums[left + 1]) ? left : right;
    }


I hope this makes things clear despite the long explanation.
</p>


### O(logN) Solution JavaCode
- Author: xctom
- Creation Date: Tue Dec 09 2014 14:29:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 16:20:38 GMT+0800 (Singapore Standard Time)

<p>

This problem is similar to Local Minimum. And according to the given condition, num[i] != num[i+1], there must exist a O(logN) solution. So we use binary search for this problem.

 - If  num[i-1] < num[i] > num[i+1], then num[i] is peak
 - If num[i-1] < num[i] < num[i+1], then num[i+1...n-1] must contains a peak
 - If num[i-1] > num[i] > num[i+1], then num[0...i-1] must contains a peak
 - If num[i-1] > num[i] < num[i+1], then both sides have peak
(n is num.length)

Here is the code

    public int findPeakElement(int[] num) {    
        return helper(num,0,num.length-1);
    }
    
    public int helper(int[] num,int start,int end){
        if(start == end){
            return start;
        }else if(start+1 == end){
            if(num[start] > num[end]) return start;
            return end;
        }else{
            
            int m = (start+end)/2;
            
            if(num[m] > num[m-1] && num[m] > num[m+1]){
    
                return m;
    
            }else if(num[m-1] > num[m] && num[m] > num[m+1]){
    
                return helper(num,start,m-1);
    
            }else{
    
                return helper(num,m+1,end);
    
            }
            
        }
    }
</p>


