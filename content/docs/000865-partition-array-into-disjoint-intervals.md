---
title: "Partition Array into Disjoint Intervals"
weight: 865
#id: "partition-array-into-disjoint-intervals"
---
## Description
<div class="description">
<p>Given an array <code>A</code>, partition it&nbsp;into two (contiguous) subarrays&nbsp;<code>left</code>&nbsp;and <code>right</code>&nbsp;so that:</p>

<ul>
	<li>Every element in <code>left</code>&nbsp;is less than or equal to every element in <code>right</code>.</li>
	<li><code>left</code> and <code>right</code> are non-empty.</li>
	<li><code>left</code>&nbsp;has the smallest possible size.</li>
</ul>

<p>Return the <strong>length</strong> of <code>left</code> after such a partitioning.&nbsp; It is guaranteed that such a partitioning exists.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[5,0,3,8,6]</span>
<strong>Output: </strong><span id="example-output-1">3</span>
<strong>Explanation: </strong>left = [5,0,3], right = [8,6]
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[1,1,1,0,6,12]</span>
<strong>Output: </strong><span id="example-output-2">4</span>
<strong>Explanation: </strong>left = [1,1,1,0], right = [6,12]
</pre>

<p>&nbsp;</p>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>2 &lt;= A.length&nbsp;&lt;= 30000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 10^6</code></li>
	<li>It is guaranteed there is at least one way to partition <code>A</code> as described.</li>
</ol>

<div>
<div>&nbsp;</div>
</div>

</div>

## Tags
- Array (array)

## Companies
- Grab - 2 (taggedByAdmin: false)
- TandemG - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Next Array

**Intuition**

Instead of checking whether `all(L <= R for L in left for R in right)`, let's check whether `max(left) <= min(right)`.

**Algorithm**

Let's try to find `max(left)` for subarrays `left = A[:1], left = A[:2], left =  A[:3], ...` etc.  Specifically, `maxleft[i]` will be the maximum of subarray `A[:i]`.  They are related to each other: `max(A[:4]) = max(max(A[:3]), A[3])`, so `maxleft[4] = max(maxleft[3], A[3])`.

Similarly, `min(right)` for every possible `right` can be found in linear time.

After we have a way to query `max(left)` and `min(right)` quickly, the solution is straightforward.

<iframe src="https://leetcode.com/playground/v7bcQBsw/shared" frameBorder="0" width="100%" height="480" name="v7bcQBsw"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java, one pass, 7 lines
- Author: climberig
- Creation Date: Sun Sep 30 2018 13:28:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 22:07:48 GMT+0800 (Singapore Standard Time)

<p>
```java
public int partitionDisjoint(int[] a) {
        int localMax = a[0], partitionIdx = 0, max = localMax;
        for (int i = 1; i < a.length; i++)
            if (localMax > a[i]) {
                localMax = max;
                partitionIdx = i;
            } else max = Math.max(max, a[i]);
        return partitionIdx + 1;
    }
</p>


### Explained - Python simple O(N) time O(1) space
- Author: yljiang
- Creation Date: Sun Sep 30 2018 11:59:35 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 23:29:54 GMT+0800 (Singapore Standard Time)

<p>
Looping through each element  `A[i]` we will keep track of the `max_so_far` and `disjoint` index. If we see a value `A[i] < max_so_far` we know that value must be in the left partition, so we update the `disjoint` location and check if we have a new  `max_so_far` in the left partition. Once we go through the list, every element on the right side of `disjoint` is guarenteed to be larger than elements left of `disjoint`

```  
def partitionDisjoint(self, A):
        disjoint = 0
        v = A[disjoint]
        max_so_far = v
        for i in range(len(A)):
            max_so_far = max(max_so_far, A[i])
            if A[i] < v: 
                disjoint = i
                v = max_so_far
        return disjoint + 1
			
</p>


### Python solutions, 3 passes -> 2 passes -> 1 pass, straight forward
- Author: lyl0812
- Creation Date: Mon Nov 26 2018 03:44:44 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 26 2018 03:44:44 GMT+0800 (Singapore Standard Time)

<p>
**3 passes solution:**
 first pass: use an array to keep the maximun of all left element,
 second pass:  use an array to keep the minimun of all right element,
 third pass: find where the maxleft <= minright:
 ```
     def partitionDisjoint(self, A):
        maxleft = [A[0]]*len(A)
        for i in range(1,len(A)):
            maxleft[i] = max(maxleft[i-1], A[i])
            
        minright = [A[-1]]*len(A)
        for i in range(len(A)-2,-1,-1):
            minright[i] = min(minright[i+1], A[i]) 
        
        for i in range(len(A)-1):
            if maxleft[i] <= minright[i+1]:
                return i+1
 ```
 
 **2 passes solution:**
 skip the first maxleft pass, keep only the minright array,  and use maxleft as a variable:
 ```
     def partitionDisjoint(self, A):
        minright = [A[-1]]*len(A)
        for i in range(len(A)-2,-1,-1):
            minright[i] = min(minright[i+1], A[i]) 

        maxleft = -float(\'inf\')
        for i in range(len(A)-1):
            maxleft = max(maxleft, A[i])  
            if maxleft <= minright[i+1]:
                return i+1
```				
 
 **1 pass solution:**
 use one more variable leftmax to moniter:
 ```
     def partitionDisjoint(self, A):
        disjoint = 0
        curmax = leftmax = A[0]
        for i,num in enumerate(A):
            curmax = max(curmax, num)
            if num < leftmax:
                leftmax = curmax
                disjoint = i
        return disjoint + 1 
 ```
</p>


