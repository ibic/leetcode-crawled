---
title: "Android Unlock Patterns"
weight: 334
#id: "android-unlock-patterns"
---
## Description
<div class="description">
<p>Given an Android <b>3x3</b> key lock screen and two integers <b>m</b> and <b>n</b>, where 1 &le; m &le; n &le; 9, count the total number of unlock patterns of the Android lock screen, which consist of minimum of <b>m</b> keys and maximum <b>n</b> keys.</p>

<p>&nbsp;</p>

<p><b>Rules for a valid pattern:</b></p>

<ol>
	<li>Each pattern must connect at least <b>m</b> keys and at most <b>n</b> keys.</li>
	<li>All the keys must be distinct.</li>
	<li>If the line connecting two consecutive keys in the pattern passes through any other keys, the other keys must have previously selected in the pattern. No jumps through non selected key is allowed.</li>
	<li>The order of keys used matters.</li>
</ol>

<p>&nbsp;</p>

<pre>
<img src="https://assets.leetcode.com/uploads/2018/10/12/android-unlock.png" style="width: 418px; height: 128px;" /></pre>

<p>&nbsp;</p>

<p><b>Explanation:</b></p>

<pre>
| 1 | 2 | 3 |
| 4 | 5 | 6 |
| 7 | 8 | 9 |</pre>

<p><b>Invalid move:</b> <code>4 - 1 - 3 - 6 </code><br />
Line 1 - 3 passes through key 2 which had not been selected in the pattern.</p>

<p><b>Invalid move:</b> <code>4 - 1 - 9 - 2</code><br />
Line 1 - 9 passes through key 5 which had not been selected in the pattern.</p>

<p><b>Valid move:</b> <code>2 - 4 - 1 - 3 - 6</code><br />
Line 1 - 3 is valid because it passes through key 2, which had been selected in the pattern</p>

<p><b>Valid move:</b> <code>6 - 5 - 4 - 1 - 9 - 2</code><br />
Line 1 - 9 is valid because it passes through key 5, which had been selected in the pattern.</p>

<p>&nbsp;</p>

<p><strong>Example:</strong></p>

<div>
<pre>
<strong>Input: </strong>m = <span id="example-input-1-1">1</span>, n = <span id="example-input-1-2">1</span>
<strong>Output: </strong><span id="example-output-1">9</span>
</pre>
</div>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Backtracking (backtracking)

## Companies
- Google - 2 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary

After Android launched its "unlock pattern" system to protect our smart phones from unauthorized access, the most common question that comes to one's mind is: How secure exactly are these patterns? The current article gives an answer to this question, as presenting an algorithm, which computes the number of all valid pattern combinations. It is intended for intermediate users and introduces the following ideas:
Backtracking, Arrays.

## Solution

---
#### Approach #1: (Backtracking) [Accepted]

**Algorithm**

The algorithm uses backtracking technique to enumerate all possible $$k$$ combinations of numbers $$[1\dots\ 9]$$ where $$m \leq k \leq n$$. During the generation of the recursive solution tree, the algorithm cuts all the branches  which lead to patterns which doesn't satisfy the rules and counts only the valid patterns.
In order to compute a valid pattern, the algorithm performs the following steps:

- Select a digit $$i$$ which is not used in the pattern till this moment. This is done with the help of a $$used$$ array which stores all available digits.

- We need to keep last inserted digit $$last$$. The algorithm makes a check whether one of the following conditions is valid.

    - There is a knight move (as in chess)  from $$last$$ towards $$i$$ or $$last$$ and $$i$$ are adjacent digits in a row, in a column. In this case the sum of both digits should be an odd number.

    - The middle element $$mid$$ in the line which connects $$i$$ and $$last$$ was previously selected. In case $$i$$ and $$last$$ are positioned at both ends of the diagonal, digit $$mid$$ = 5 should be previously selected.

    - $$last$$ and $$i$$ are adjacent digits in a diagonal

In case one of the conditions above is satisfied, digit $$i$$ becomes part of partially generated valid pattern and the algorithm continues with the next candidate digit till the pattern is fully generated. Then it counts it.
In case none of the conditions are satisfied, the algorithm rejects the current digit $$i$$, backtracks and continues to search for other valid digits among the unused ones.

![Android unlock pattern recursive solution tree](https://leetcode.com/cms/documents/5/351_Android.png){:width="80%"}
{:align="center"}


<iframe src="https://leetcode.com/playground/mjE2nWzc/shared" frameBorder="0" name="mjE2nWzc" width="100%" height="515"></iframe>
**Complexity Analysis**

* Time complexity : $$O( n!)$$, where $$n$$ is maximum pattern length

    The algorithm computes each pattern once and no element can appear in the pattern twice. The time complexity is proportional to the number of the computed patterns. One upper bound of the number of all possible combinations is :

$$
\ \sum_{i=m}^{n} {_9}P_i = \ \sum_{i=m}^{n} \frac{9!}{(9 - i)!}
$$


* Space complexity : $$O(n)$$, where $$n$$ is maximum pattern length
In the worst case the maximum depth of recursion is $$n$$. Therefore we need $$O( n)$$ space used by the system recursive stack

## Further Thoughts

The algorithm above could be optimized if we consider the symmetry property of the problem. We notice that the number of valid patterns with first digit 1, 3, 7, 9 are the same. A similar observation is true for patterns which starts with digit 2, 4, 6, 8.  Hence we only  need to calculate one among each group and multiply by 4.

You can find the optimized solution [here](https://leetcode.com/problems/android-unlock-patterns/discuss/82463/Java-DFS-solution-with-clear-explanations-and-optimization-beats-97.61-(12ms)).

Analysis written by: @elmirap.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java DFS solution with clear explanations and optimization, beats 97.61% (12ms)
- Author: zzz1322
- Creation Date: Tue May 24 2016 22:46:10 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 08:26:01 GMT+0800 (Singapore Standard Time)

<p>
The optimization idea is that 1,3,7,9 are symmetric, 2,4,6,8 are also symmetric. Hence we only calculate one among each group and multiply by 4.

    public class Solution {
        // cur: the current position
        // remain: the steps remaining
        int DFS(boolean vis[], int[][] skip, int cur, int remain) {
            if(remain < 0) return 0;
            if(remain == 0) return 1;
            vis[cur] = true;
            int rst = 0;
            for(int i = 1; i <= 9; ++i) {
                // If vis[i] is not visited and (two numbers are adjacent or skip number is already visited)
                if(!vis[i] && (skip[cur][i] == 0 || (vis[skip[cur][i]]))) {
                    rst += DFS(vis, skip, i, remain - 1);
                }
            }
            vis[cur] = false;
            return rst;
        }
        
        public int numberOfPatterns(int m, int n) {
            // Skip array represents number to skip between two pairs
            int skip[][] = new int[10][10];
            skip[1][3] = skip[3][1] = 2;
            skip[1][7] = skip[7][1] = 4;
            skip[3][9] = skip[9][3] = 6;
            skip[7][9] = skip[9][7] = 8;
            skip[1][9] = skip[9][1] = skip[2][8] = skip[8][2] = skip[3][7] = skip[7][3] = skip[4][6] = skip[6][4] = 5;
            boolean vis[] = new boolean[10];
            int rst = 0;
            // DFS search each length from m to n
            for(int i = m; i <= n; ++i) {
                rst += DFS(vis, skip, 1, i - 1) * 4;    // 1, 3, 7, 9 are symmetric
                rst += DFS(vis, skip, 2, i - 1) * 4;    // 2, 4, 6, 8 are symmetric
                rst += DFS(vis, skip, 5, i - 1);        // 5
            }
            return rst;
        }
    }
</p>


### Description should be clear about a move like 1-8
- Author: fentoyal
- Creation Date: Fri Jun 10 2016 05:32:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 03:55:37 GMT+0800 (Singapore Standard Time)

<p>
I thought 1-8 touches 4 and 5, and they should be tested. However my code with such a check fails. Then I removed this test, and my code passed.
</p>


### Simple and concise Java solution in 69ms
- Author: davydliu
- Creation Date: Wed May 25 2016 23:45:07 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 04 2018 02:02:32 GMT+0800 (Singapore Standard Time)

<p>
The general idea is DFS all the possible combinations from 1 to 9 and skip invalid moves along the way. 

We can check invalid moves by using a jumping table. e.g. If a move requires a jump and the key that it is crossing is not visited, then the move is invalid. Furthermore, we can utilize symmetry to reduce runtime, in this case it reduced from ~120ms to ~70ms.

I felt clueless when first encountered this problem, and considered there must be lots of edge cases. Turns out, it's pretty straight forward. Hope this solution helps :D

    private int[][] jumps;
    private boolean[] visited;
    
    public int numberOfPatterns(int m, int n) {
        jumps = new int[10][10];
        jumps[1][3] = jumps[3][1] = 2;
        jumps[4][6] = jumps[6][4] = 5;
        jumps[7][9] = jumps[9][7] = 8;
        jumps[1][7] = jumps[7][1] = 4;
        jumps[2][8] = jumps[8][2] = 5;
        jumps[3][9] = jumps[9][3] = 6;
		jumps[1][9] = jumps[9][1] = jumps[3][7] = jumps[7][3] = 5;
        visited = new boolean[10];
        int count = 0;
		count += DFS(1, 1, 0, m, n) * 4; // 1, 3, 7, 9 are symmetrical
		count += DFS(2, 1, 0, m, n) * 4; // 2, 4, 6, 8 are symmetrical
		count += DFS(5, 1, 0, m, n);
		return count;
    }
    
    private int DFS(int num, int len, int count, int m, int n) {
		if (len >= m) count++; // only count if moves are larger than m
		len++;
		if (len > n) return count;
        visited[num] = true;
        for (int next = 1; next <= 9; next++) {
            int jump = jumps[num][next];
            if (!visited[next] && (jump == 0 || visited[jump])) {
                count = DFS(next, len, count, m, n);
            }
        }
		visited[num] = false; // backtracking
        return count;
    }
</p>


