---
title: "Longest Palindrome"
weight: 392
#id: "longest-palindrome"
---
## Description
<div class="description">
<p>Given a string <code>s</code> which consists of lowercase or uppercase letters, return <em>the length of the <strong>longest palindrome</strong></em>&nbsp;that can be built with those letters.</p>

<p>Letters are <strong>case sensitive</strong>, for example,&nbsp;<code>&quot;Aa&quot;</code> is not considered a palindrome here.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abccccdd&quot;
<strong>Output:</strong> 7
<strong>Explanation:</strong>
One longest palindrome that can be built is &quot;dccaccd&quot;, whose length is 7.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;a&quot;
<strong>Output:</strong> 1
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;bb&quot;
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 2000</code></li>
	<li><code>s</code> consits of lower-case <strong>and/or</strong> upper-case English&nbsp;letters only.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Lyft - 2 (taggedByAdmin: false)
- Amazon - 7 (taggedByAdmin: false)
- Intuit - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Greedy [Accepted]

**Intuition**

A palindrome consists of letters with equal partners, plus possibly a unique center (without a partner).  The letter `i` from the left has its partner `i` from the right.  For example in `'abcba'`, `'aa'` and `'bb'` are partners, and `'c'` is a unique center.

Imagine we built our palindrome.  It consists of as many partnered letters as possible, plus a unique center if possible.  This motivates a greedy approach.

**Algorithm**

For each letter, say it occurs `v` times.  We know we have `v // 2 * 2` letters that can be partnered for sure.  For example, if we have `'aaaaa'`, then we could have `'aaaa'` partnered, which is `5 // 2 * 2 = 4` letters partnered.

At the end, if there was any `v % 2 == 1`, then that letter could have been a unique center.  Otherwise, every letter was partnered.  To perform this check, we will check for `v % 2 == 1` and `ans % 2 == 0`, the latter meaning we haven't yet added a unique center to the answer.

<iframe src="https://leetcode.com/playground/ZGjLAGRH/shared" frameBorder="0" width="100%" height="310" name="ZGjLAGRH"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `s`.  We need to count each letter.

* Space Complexity: $$O(1)$$, the space for our count, as the alphabet size of `s` is fixed.  We should also consider that in a bit complexity model, technically we need $$O(\log N)$$ bits to store the count values.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple HashSet solution Java
- Author: charliebob64
- Creation Date: Sun Oct 02 2016 14:18:20 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 11:34:24 GMT+0800 (Singapore Standard Time)

<p>
```
public int longestPalindrome(String s) {
        if(s==null || s.length()==0) return 0;
        HashSet<Character> hs = new HashSet<Character>();
        int count = 0;
        for(int i=0; i<s.length(); i++){
            if(hs.contains(s.charAt(i))){
                hs.remove(s.charAt(i));
                count++;
            }else{
                hs.add(s.charAt(i));
            }
        }
        if(!hs.isEmpty()) return count*2+1;
        return count*2;
}
```
</p>


### What are the odds? (Python & C++)
- Author: StefanPochmann
- Creation Date: Sun Oct 02 2016 21:44:40 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 16:52:45 GMT+0800 (Singapore Standard Time)

<p>
I count how many letters appear an odd number of times. Because we can use **all** letters, except for each odd-count letter we must leave one, except one of them we can use.

Python:

    def longestPalindrome(self, s):
        odds = sum(v & 1 for v in collections.Counter(s).values())
        return len(s) - odds + bool(odds)

C++:

    int longestPalindrome(string s) {
        int odds = 0;
        for (char c='A'; c<='z'; c++)
            odds += count(s.begin(), s.end(), c) & 1;
        return s.size() - odds + (odds > 0);
    }

Similar solutions (I actually like the `use` solutions better than the above, but I'm just so fond of my topic title :-)

    def longestPalindrome(self, s):
        use = sum(v & ~1 for v in collections.Counter(s).values())
        return use + (use < len(s))

    def longestPalindrome(self, s):
        counts = collections.Counter(s).values()
        return sum(v & ~1 for v in counts) + any(v & 1 for v in counts)

    int longestPalindrome(string s) {
        int use = 0;
        for (char c='A'; c<='z'; c++)
            use += count(s.begin(), s.end(), c) & ~1;
        return use + (use < s.size());
    }

    int longestPalindrome(string s) {
        vector<int> count(256);
        for (char c : s)
            ++count[c];
        int odds = 0;
        for (int c : count)
            odds += c & 1;
        return s.size() - odds + (odds > 0);
    }

    int longestPalindrome(string s) {
        vector<int> count(256);
        int odds = 0;
        for (char c : s)
            odds += ++count[c] & 1 ? 1 : -1;
        return s.size() - odds + (odds > 0);
    }
</p>


### python simple set solution
- Author: HoweZZ
- Creation Date: Mon Jun 26 2017 03:41:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 20 2018 03:11:06 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: int
        """
        hash = set()
        for c in s:
            if c not in hash:
                hash.add(c)
            else:
                hash.remove(c)
        # len(hash) is the number of the odd letters
        return len(s) - len(hash) + 1 if len(hash) > 0 else len(s)
```
</p>


