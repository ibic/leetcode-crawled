---
title: "Count Good Nodes in Binary Tree"
weight: 1321
#id: "count-good-nodes-in-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree <code>root</code>, a node <em>X</em> in the tree is named&nbsp;<strong>good</strong> if in the path from root to <em>X</em> there are no nodes with a value <em>greater than</em> X.</p>

<p>Return the number of <strong>good</strong> nodes in the binary tree.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/04/02/test_sample_1.png" style="width: 263px; height: 156px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [3,1,4,3,null,1,5]
<strong>Output:</strong> 4
<strong>Explanation:</strong> Nodes in blue are <strong>good</strong>.
Root Node (3) is always a good node.
Node 4 -&gt; (3,4) is the maximum value in the path starting from the root.
Node 5 -&gt; (3,4,5) is the maximum value in the path
Node 3 -&gt; (3,1,3) is the maximum value in the path.</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/04/02/test_sample_2.png" style="width: 157px; height: 161px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [3,3,null,4,2]
<strong>Output:</strong> 3
<strong>Explanation:</strong> Node 2 -&gt; (3, 3, 2) is not good, because &quot;3&quot; is higher than it.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [1]
<strong>Output:</strong> 1
<strong>Explanation:</strong> Root is considered as <strong>good</strong>.</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the binary tree is in the range&nbsp;<code>[1, 10^5]</code>.</li>
	<li>Each node&#39;s value is between <code>[-10^4, 10^4]</code>.</li>
</ul>
</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] One line
- Author: lee215
- Creation Date: Sun May 17 2020 00:03:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 00:32:05 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**:
Record the maximum value along the path from the root to the node.

Time `O(N)`
Space `O(height)`
<br>

**Java:**
```java
class Solution {
    public int goodNodes(TreeNode root) {
        return goodNodes(root, -10000);
    }

    public int goodNodes(TreeNode root, int ma) {
        if (root == null) return 0;
        int res = root.val >= ma ? 1 : 0;
        res += goodNodes(root.left, Math.max(ma, root.val));
        res += goodNodes(root.right, Math.max(ma, root.val));
        return res;
    }
}
```

**C++:**
```cpp
    int goodNodes(TreeNode* r, int ma = -10000) {
        return r ? goodNodes(r->left, max(ma, r->val)) + goodNodes(r->right, max(ma, r->val)) + (r->val >= ma) : 0;
    }
```

**Python:**
```py
    def goodNodes(self, r, ma=-10000):
        return self.goodNodes(r.left, max(ma, r.val)) + self.goodNodes(r.right, max(ma, r.val)) + (r.val >= ma) if r else 0
```

</p>


### C++ one-liner
- Author: votrubac
- Creation Date: Sun May 17 2020 00:01:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 00:01:34 GMT+0800 (Singapore Standard Time)

<p>
```cpp
int goodNodes(TreeNode* root, int val = INT_MIN) {
    return root == nullptr ? 0 : (val <= root->val) 
        + goodNodes(root->left, max(root->val, val))
            + goodNodes(root->right, max(root->val, val));
}
```
</p>


### [Java/Python 3] Simple recursion w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun May 17 2020 00:03:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 01:29:13 GMT+0800 (Singapore Standard Time)

<p>
1. Update the maximum value found while recurse down to the paths from root to leaves;
2. If node value >= current maximum, count it in.
3. return the total number after the completion of all recursions.

```java
    public int goodNodes(TreeNode root) {
        return preorder(root, root.val);
    }
    private int preorder(TreeNode n, int v) {
        if (n == null) // base cases.
            return 0;
        int max = Math.max(n.val, v); // maximum so far on the path.
        return (n.val >= v ? 1 : 0) + preorder(n.left, max) + preorder(n.right, max); // recurse to children.
    }
```
```python
    def goodNodes(self, root: TreeNode) -> int:
       
        def count(node: TreeNode, v: int) -> int:
            if node is None:
                return 0
            mx = max(node.val, v)
            return (node.val >= v) + count(node.left, mx) + count(node.right, mx)
        
        return count(root, root.val)
```

**Analysis:**

Time: O(n), space: O(h), where n and h are the number and height of the binary tree, respectively.
</p>


