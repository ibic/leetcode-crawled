---
title: "Minimum Swaps To Make Sequences Increasing"
weight: 740
#id: "minimum-swaps-to-make-sequences-increasing"
---
## Description
<div class="description">
<p>We have two integer sequences <code>A</code> and <code>B</code> of the same non-zero length.</p>

<p>We are allowed to swap elements <code>A[i]</code> and <code>B[i]</code>.&nbsp; Note that both elements are in the same index position in their respective sequences.</p>

<p>At the end of some number of swaps, <code>A</code> and <code>B</code> are both strictly increasing.&nbsp; (A sequence is <em>strictly increasing</em> if and only if <code>A[0] &lt; A[1] &lt; A[2] &lt; ... &lt; A[A.length - 1]</code>.)</p>

<p>Given A and B, return the minimum number of swaps to make both sequences strictly increasing.&nbsp; It is guaranteed that the given input always makes it possible.</p>

<pre>
<strong>Example:</strong>
<strong>Input:</strong> A = [1,3,5,4], B = [1,2,3,7]
<strong>Output:</strong> 1
<strong>Explanation: </strong>
Swap A[3] and B[3].  Then the sequences are:
A = [1, 3, 5, 7] and B = [1, 2, 3, 4]
which are both strictly increasing.
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li><code>A, B</code> are arrays with the same length, and that length will be in the range <code>[1, 1000]</code>.</li>
	<li><code>A[i], B[i]</code> are integer values in the range <code>[0, 2000]</code>.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 10 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: true)
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Dynamic Programming [Accepted]

**Intuition**

The cost of making both sequences increasing up to the first `i` columns can be expressed in terms of the cost of making both sequences increasing up to the first `i-1` columns.  This is because the only thing that matters to the `i`th column is whether the previous column was swapped or not.  This makes dynamic programming an ideal choice.

Let's remember `n1` (`natural1`), the cost of making the first `i-1` columns increasing and not swapping the `i-1`th column; and `s1` (`swapped1`), the cost of making the first `i-1` columns increasing and swapping the `i-1`th column.

Now we want candidates `n2` (and `s2`), the costs of making the first `i` columns increasing if we do not swap (or swap, respectively) the `i`th column.

**Algorithm**

For convenience, say `a1 = A[i-1], b1 = B[i-1]` and `a2 = A[i], b2 = B[i]`.

Now, if `a1 < a2` and `b1 < b2`, then it is allowed to have both of these columns natural (unswapped), or both of these columns swapped.  This possibility leads to `n2 = min(n2, n1)` and `s2 = min(s2, s1 + 1)`.

Another, (not exclusive) possibility is that `a1 < b2` and `b1 < a2`.  This means that it is allowed to have exactly one of these columns swapped.  This possibility leads to `n2 = min(n2, s1)` or `s2 = min(s2, n1 + 1)`.

Note that it is important to use two if statements separately, because both of the above possibilities might be possible.

At the end, the optimal solution must leave the last column either natural or swapped, so we take the minimum number of swaps between the two possibilities.


<iframe src="https://leetcode.com/playground/fmF34Yhh/shared" frameBorder="0" width="100%" height="395" name="fmF34Yhh"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$.

* Space Complexity:  $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n) DP Solution
- Author: wangzi6147
- Creation Date: Mon Mar 19 2018 12:20:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 13:49:21 GMT+0800 (Singapore Standard Time)

<p>
```swapRecord``` means for the ith element in A and B, the minimum swaps if we swap ```A[i]``` and ```B[i]```
```fixRecord``` means for the ith element in A and B, the minimum swaps if we DONOT swap ```A[i]``` and ```B[i]```
```
class Solution {
    public int minSwap(int[] A, int[] B) {
        int swapRecord = 1, fixRecord = 0;
        for (int i = 1; i < A.length; i++) {
            if (A[i - 1] >= B[i] || B[i - 1] >= A[i]) {
		// In this case, the ith manipulation should be same as the i-1th manipulation
                // fixRecord = fixRecord;
                swapRecord++;
            } else if (A[i - 1] >= A[i] || B[i - 1] >= B[i]) {
		// In this case, the ith manipulation should be the opposite of the i-1th manipulation
                int temp = swapRecord;
                swapRecord = fixRecord + 1;
                fixRecord = temp;
            } else {
                // Either swap or fix is OK. Let\'s keep the minimum one
                int min = Math.min(swapRecord, fixRecord);
                swapRecord = min + 1;
                fixRecord = min;
            }
        }
        return Math.min(swapRecord, fixRecord);
    }
}
```

Let me firstly explain the O(n) space DP solution which uses swapRecord[n] and fixRecord[n]. It would be more explicit.

One thing should be kept in mind is, the array A and B would always be valid after you do the swap manipulation or not for each element. Take an example:

```
index               0    1    2    3    4
A                   1    3    5    4    9
B                   1    2    3    7    10   
swapRecord          1    1    2    1    2
fixRecord           0    0    0    2    1
```

```swapRecord[i]``` means for the ith element in A and B, the minimum swaps if we swap A[i] and B[i]
```fixRecord[i]``` means for the ith element in A and B, the minimum swaps if we DONOT swap A[i] and B[i]

Obviously, ```swapRecord[0] = 1``` and ```fixRecord[0] = 0```.

For ```i = 1```, either swap or fix is OK. So we take the minimum previous result, ```min = min(swapRecord[0], fixRecord[0]) = 0```. ```swapRecord[1] = min + 1 = 1```, ```fixRecord[1] = min = 0```
For ```i = 2```, notice that A[1] >= B[2], which means the manipulation of ```A[2] and B[2]``` should be same as ```A[1] and B[1]```, if A[1] and B[1] swap, A[2] and B[2] should swap, vice versa. Make sense, right? So ```swapRecord[2] = swapRecord[1] + 1``` and ```fixRecord[2] = fixRecord[1]```
For ```i = 3```, notice that A[2] >= A[3], which mean the manipulation of ```A[3] and B[3]``` and ```A[2] and B[2]``` should be opposite. In this case, ```swapRecord[3] = fixRecord[2] + 1``` and ```fixRecord[3] = swapRecord[2]```
For the last elements, it\'s similiar as the elements when i = 1. Either swap or fix is OK. You can try to figure this out. :D

Finally, we get the minimum of last swapRecord and fixRecord. It should be the result.

Notice that every ith swapRecord and fixRecord is only relevant with the previous one. So the algorithm should be optimized to an O(1) space version. Just like the code I write above.
</p>


### [C++/Java/Python] DP O(N) Solution
- Author: lee215
- Creation Date: Mon Mar 19 2018 16:27:53 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 17 2020 14:48:39 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
```swap[n]``` means the minimum swaps to make the ```A[i]``` and ```B[i]``` sequences increasing for ```0 <= i <= n```,
in condition that we swap ```A[n]``` and ```B[n]```
```not_swap[n]``` is the same with ```A[n]``` and ```B[n]``` not swapped.

@Acker help explain:
1. `A[i - 1] < A[i] && B[i - 1] < B[i]`.
In this case, if we want to keep A and B increasing before the index i, can only have two choices.
-> 1.1 don\'t swap at (i-1) and don\'t swap at i, we can get `not_swap[i] = not_swap[i-1]`
-> 1.2 swap at (i-1) and swap at i, we can get `swap[i] = swap[i-1]+1`
if swap at (i-1) and do not swap at i, we can not guarantee A and B increasing.

2. `A[i-1] < B[i] && B[i-1] < A[i]`
In this case, if we want to keep A and B increasing before the index i, can only have two choices.
-> 2.1 swap at (i-1) and do not swap at i, we can get `notswap[i] = Math.min(swap[i-1], notswap[i] )`
-> 2.2 do not swap at (i-1) and swap at i, we can get `swap[i]=Math.min(notswap[i-1]+1, swap[i])`
<br>

**C++**
```cpp
    int minSwap(vector<int>& A, vector<int>& B) {
        int N = A.size();
        int not_swap[1000] = {0};
        int swap[1000] = {1};
        for (int i = 1; i < N; ++i) {
            not_swap[i] = swap[i] = N;
            if (A[i - 1] < A[i] && B[i - 1] < B[i]) {
                swap[i] = swap[i - 1] + 1;
                not_swap[i] = not_swap[i - 1];
            }
            if (A[i - 1] < B[i] && B[i - 1] < A[i]) {
                swap[i] = min(swap[i], not_swap[i - 1] + 1);
                not_swap[i] = min(not_swap[i], swap[i - 1]);
            }
        }
        return min(swap[N - 1], not_swap[N - 1]);
    }
```
**Java**
```java
    public int minSwap(int[] A, int[] B) {
        int N = A.length;
        int[] swap = new int[1000];
        int[] not_swap = new int[1000];
        swap[0] = 1;
        for (int i = 1; i < N; ++i) {
            not_swap[i] = swap[i] = N;
            if (A[i - 1] < A[i] && B[i - 1] < B[i]) {
                swap[i] = swap[i - 1] + 1;
                not_swap[i] = not_swap[i - 1];
            }
            if (A[i - 1] < B[i] && B[i - 1] < A[i]) {
                swap[i] = Math.min(swap[i], not_swap[i - 1] + 1);
                not_swap[i] = Math.min(not_swap[i], swap[i - 1]);
            }
        }
        return Math.min(swap[N - 1], not_swap[N - 1]);
    }
```
**Python**
```py
    def minSwap(self, A, B):
        N = len(A)
        not_swap, swap = [N] * N, [N] * N
        not_swap[0], swap[0] = 0, 1
        for i in range(1, N):
            if A[i - 1] < A[i] and B[i - 1] < B[i]:
                swap[i] = swap[i - 1] + 1
                not_swap[i] = not_swap[i - 1]
            if A[i - 1] < B[i] and B[i - 1] < A[i]:
                swap[i] = min(swap[i], not_swap[i - 1] + 1)
                not_swap[i] = min(not_swap[i], swap[i - 1])
        return min(swap[-1], not_swap[-1])
```
<br>

# Solution 2
Use only `O(1)` space
**Java**
```java
    public int minSwap(int[] A, int[] B) {
        int N = A.length;
        int swap = 1, not_swap = 0;
        for (int i = 1; i < N; ++i) {
            int not_swap2 = N, swap2 = N;
            if (A[i - 1] < A[i] && B[i - 1] < B[i]) {
                swap2 = swap + 1;
                not_swap2 = not_swap;
            }
            if (A[i - 1] < B[i] && B[i - 1] < A[i]) {
                swap2 = Math.min(swap2, not_swap + 1);
                not_swap2 = Math.min(not_swap2, swap);
            }
            swap = swap2;
            not_swap = not_swap2;
        }
        return Math.min(swap, not_swap);
    }
```
**C++**
```cpp
    int minSwap(vector<int>& A, vector<int>& B) {
        int N = A.size(), swap = 1, not_swap = 0;
        for (int i = 1; i < N; ++i) {
            int not_swap2 = N, swap2 = N;
            if (A[i - 1] < A[i] && B[i - 1] < B[i]) {
                swap2 = swap + 1;
                not_swap2 = not_swap;
            }
            if (A[i - 1] < B[i] && B[i - 1] < A[i]) {
                swap2 = min(swap2, not_swap + 1);
                not_swap2 = min(not_swap2, swap);
            }
            swap = swap2;
            not_swap = not_swap2;
        }
        return min(swap, not_swap);
    }
```
**Python**
```py
    def minSwap(self, A, B):
        N = len(A)
        not_swap, swap = 0, 1
        for i in range(1, N):
            not_swap2 = swap2 = N 
            if A[i - 1] < A[i] and B[i - 1] < B[i]:
                swap2 = swap + 1
                not_swap2 = not_swap
            if A[i - 1] < B[i] and B[i - 1] < A[i]:
                swap2 = min(swap2, not_swap + 1)
                not_swap2 = min(not_swap2, swap)
            swap, not_swap = swap2, not_swap2
        return min(swap, not_swap)
```
</p>


### Bottom-up DP with Optimization (Java / Python)
- Author: GraceMeng
- Creation Date: Tue Aug 21 2018 09:59:58 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 03:14:32 GMT+0800 (Singapore Standard Time)

<p>
**state**	
whether we swap the element at index i to make A[0..i]  and B[0..i] both increasing can uniquely identify a\xA0state, i.e. a node in the state graph.
**state function**	
`state(i, 0)`\xA0is the minimum swaps to make A[0..i] and B[0..i] both increasing if we donot swap A[i] with B[i]
`state(i, 1)`\xA0is the minimum swaps to make A[0..i] and B[0..i] both increasing if we do swap A[i] with B[i]
**goal state**	
`min{state(n - 1, 0), state(n - 1, 1)}` where n = A.length
**state transition**	
We define `areBothSelfIncreasing: A[i - 1] < A[i] && B[i - 1] < B[i], areInterchangeIncreasing: A[i - 1] < B[i] && B[i - 1] < A[i]`.
Since *\'the given input always makes it possible\'*, at least one of the two conditions above should be satisfied.
```
if i == 0, 
	        state(0, 0) = 0; 
	        state(0, 1) = 1;
			
Generally speaking,
	if areBothSelfIncreasing && areInterchangeIncreasing
	        // Doesn\'t matter whether the previous is swapped or not.
	        state(i, 0) = Math.min(state(i - 1, 0), state(i - 1, 1));
	        state(i, 1) = Math.min(state(i - 1, 0), state(i - 1, 1)) + 1;
	else if areBothSelfIncreasing
	        // Following the previous action.
	        state(i, 0) =  state(i - 1, 0);
	        state(i, 1) =  state(i - 1, 1) + 1;
	else if areInterchangeIncreasing
	        // Opposite to the previous action.
	        state(i, 0) = state(i - 1, 1);
	        state(i, 1) = state(i - 1, 0) + 1;
```
**Java 0.0**
```
    public int minSwap(int[] A, int[] B) {
        int n = A.length;
        
        /* 
		 state[i][0] is min swaps too make A[0..i] and B[0..i] increasing if we do not swap A[i] and B[i]; 
		 state[i][1] is min swaps too make A[0..i] and B[0..i] increasing if we swap A[i] and B[i].
         */
        int[][] state = new int[n][2];
        state[0][1] = 1;
        
        for (int i = 1; i < n; i++) {
            boolean areBothSelfIncreasing = A[i - 1] < A[i] && B[i - 1] < B[i];
            boolean areInterchangeIncreasing = A[i - 1] < B[i] && B[i - 1] < A[i];
            
            if (areBothSelfIncreasing && areInterchangeIncreasing) {
                state[i][0] = Math.min(state[i - 1][0], state[i - 1][1]);
                state[i][1] = Math.min(state[i - 1][0], state[i - 1][1]) + 1;
            } else if (areBothSelfIncreasing) {
                state[i][0] = state[i - 1][0];
                state[i][1] = state[i - 1][1] + 1;
            } else { // if (areInterchangeIncreasing)
                state[i][0] = state[i - 1][1];
                state[i][1] = state[i - 1][0] + 1;
            }
        }
        
        return Math.min(state[n - 1][0], state[n - 1][1]);
    }
```
**Optimization**
Since current state depends on its previous state only, we may use variables to save states rather than the state array.
**Java 0.1**
```
    public int minSwap(int[] A, int[] B) {
        int n = A.length, prevNotSwap = 0, prevSwap = 1;
        
        for (int i = 1; i < n; i++) {
            boolean areBothSelfIncreasing = A[i - 1] < A[i] && B[i - 1] < B[i];
            boolean areInterchangeIncreasing = A[i - 1] < B[i] && B[i - 1] < A[i];
            
            if (areBothSelfIncreasing && areInterchangeIncreasing) {
                int newPrevNotSwap = Math.min(prevNotSwap, prevSwap);
                prevSwap = Math.min(prevNotSwap, prevSwap) + 1;
                prevNotSwap = newPrevNotSwap;
            } else if (areBothSelfIncreasing) {
                prevSwap++;
            } else { // if (areInterchangeIncreasing)
                int newPrevNotSwap = prevSwap;
                prevSwap = prevNotSwap + 1;
                prevNotSwap = newPrevNotSwap;
            }
        }
        
        return Math.min(prevSwap, prevNotSwap);
    }
```
**Python**
```
    def minSwap(self, A, B):
        n = len(A)
        prevNotSwap = 0
        prevSwap = 1
        for i in range(1, n):
            areBothSelfIncreasing = A[i - 1] < A[i] and B[i - 1] < B[i] 
            areInterchangeIncreasing = A[i - 1] < B[i] and B[i - 1] < A[i]
            if areBothSelfIncreasing and areInterchangeIncreasing:
                newPrevNotSwap = min(prevNotSwap, prevSwap)
                prevSwap = min(prevNotSwap, prevSwap) + 1
                prevNotSwap = newPrevNotSwap
            elif areBothSelfIncreasing:
                prevSwap += 1 
            else: # if areInterchangeIncreasing:
                newPrevNotSwap = prevSwap
                prevSwap = prevNotSwap + 1
                prevNotSwap = newPrevNotSwap
        return min(prevNotSwap, prevSwap)
```
**(\u4EBA \u2022\u0348\u1D17\u2022\u0348)** Thanks for voting!
</p>


