---
title: "Frog Position After T Seconds"
weight: 1281
#id: "frog-position-after-t-seconds"
---
## Description
<div class="description">
<p>Given an undirected tree&nbsp;consisting of <code>n</code> vertices numbered from 1 to <code>n</code>. A frog starts jumping&nbsp;from the <strong>vertex 1</strong>. In one second, the frog&nbsp;jumps from its&nbsp;current&nbsp;vertex to another <strong>unvisited</strong> vertex if they are directly connected. The frog can not jump back to a visited vertex.&nbsp;In case the frog can jump to several vertices it jumps randomly to one of them with the same probability, otherwise, when the frog can not jump to any unvisited vertex it jumps forever on the same vertex.&nbsp;</p>

<p>The edges of the undirected tree&nbsp;are given in the array <code>edges</code>, where <code>edges[i] = [from<sub>i</sub>, to<sub>i</sub>]</code> means that exists an edge connecting directly the vertices <code>from<sub>i</sub></code> and <code>to<sub>i</sub></code>.</p>

<p><em>Return the probability that after <code>t</code> seconds the frog is on the vertex <code><font face="monospace">target</font></code>.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/02/20/frog_2.png" style="width: 350px; height: 236px;" /></p>

<pre>
<strong>Input:</strong> n = 7, edges = [[1,2],[1,3],[1,7],[2,4],[2,6],[3,5]], t = 2, target = 4
<strong>Output:</strong> 0.16666666666666666 
<strong>Explanation: </strong>The figure above shows the given graph. The frog starts at vertex 1, jumping with 1/3 probability to the vertex 2 after <strong>second 1</strong> and then jumping with 1/2 probability to vertex 4 after <strong>second 2</strong>. Thus the probability for the frog is on the vertex 4 after 2 seconds is 1/3 * 1/2 = 1/6 = 0.16666666666666666. 
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/02/20/frog_3.png" style="width: 350px; height: 236px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 7, edges = [[1,2],[1,3],[1,7],[2,4],[2,6],[3,5]], t = 1, target = 7
<strong>Output:</strong> 0.3333333333333333
<strong>Explanation: </strong>The figure above shows the given graph. The frog starts at vertex 1, jumping with 1/3 = 0.3333333333333333 probability to the vertex 7 after <strong>second 1</strong>. 
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 7, edges = [[1,2],[1,3],[1,7],[2,4],[2,6],[3,5]], t = 20, target = 6
<strong>Output:</strong> 0.16666666666666666
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 100</code></li>
	<li><code>edges.length == n-1</code></li>
	<li><code>edges[i].length == 2</code></li>
	<li><code>1 &lt;= edges[i][0], edges[i][1] &lt;= n</code></li>
	<li><code>1 &lt;= t&nbsp;&lt;= 50</code></li>
	<li><code>1 &lt;= target&nbsp;&lt;= n</code></li>
	<li>Answers within <code>10^-5</code> of the actual value will be accepted as correct.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Straightforward BFS - Clean code - O(N)
- Author: hiepit
- Creation Date: Sun Mar 08 2020 12:02:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 18:07:06 GMT+0800 (Singapore Standard Time)

<p>
```java
class Solution {
    public double frogPosition(int n, int[][] edges, int t, int target) {
        List<Integer>[] graph = new List[n];
        for (int i = 0; i < n; i++) graph[i] = new ArrayList<>();
        for (int[] e : edges) {
            graph[e[0] - 1].add(e[1] - 1);
            graph[e[1] - 1].add(e[0] - 1);
        }
        boolean[] visited = new boolean[n]; visited[0] = true;
        double[] prob = new double[n]; prob[0] = 1f;
        Queue<Integer> q = new LinkedList<>(); q.offer(0);
        while (!q.isEmpty() && t-- > 0) {
            for (int size = q.size(); size > 0; size--) {
                int u = q.poll(), nextVerticesCount = 0;
                for (int v : graph[u]) if (!visited[v]) nextVerticesCount++;
                for (int v : graph[u]) {
                    if (!visited[v]) {
                        visited[v] = true;
                        q.offer(v);
                        prob[v] = prob[u] / nextVerticesCount;
                    }
                }
                if (nextVerticesCount > 0) prob[u] = 0; // frog don\'t stay vertex u, he keeps going to the next vertex
            }
        }
        return prob[target - 1];
    }
}
```
**Complexity:**
- Time: `O(n)`
- Space: `O(n)`
</p>


### [C++] Simple DFS
- Author: PhoenixDD
- Creation Date: Sun Mar 08 2020 12:02:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 12:30:50 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
The answer lies in the examples.
We can use a simple DFS to reach the target node while storing the probablity of reaching that node at `t` by multiplying by the number of children that node has.
Once we reach that target and depth required in the problem we can return that probablity.
Edge cases:
1. When when either the node is somewhere in the middle and we reach that node but not in target time, we simply return 0.
1. When it\'s a leaf node but the time is less than the required one, it\'ll simply jump until the goal time, thus we can return the same probablity.

**Solution**
```c++
class Solution {
public:
    long double prob=0;
    int target;
    vector<vector<int>> adjList;
    vector<bool> visited;
    bool dfs(int node,int depth,long double prob)
    {
        if(depth<0)              //We don\'t need to check for depth greater than time.
            return false;
        visited[node]=true;
        if(node==target)
        {
            if(depth==0||adjList[node].size()==(node!=1))		//depth=time or it\'s a leaf node.
                this->prob=prob;
            return true;		//Early exit.
        }
        for(int &n:adjList[node])
            if(!visited[n]&&dfs(n,depth-1,prob*(long double)1/(adjList[node].size()-(node!=1))))		//DFS to each children with carrying the probablity to reach them. (Early exit if found)
                return true;
        return false;                
    }
    double frogPosition(int n, vector<vector<int>>& edges, int t, int target) 
    {
        adjList.resize(n+1);
        visited.resize(n+1,false);
        this->target=target;
        for(vector<int> &v:edges)		//Create adjacency list.
            adjList[v[0]].push_back(v[1]),adjList[v[1]].push_back(v[0]);
        dfs(1,t,1);
        return prob;
    }
};
```
**Complexity**
Space: `O(n)`.
Time: `O(n)`.
</p>


### [Java/C++/Python] DFS
- Author: lee215
- Creation Date: Sun Mar 08 2020 12:06:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 15:07:17 GMT+0800 (Singapore Standard Time)

<p>
**Java**
by @feelspecial21
```java
class Solution {
    LinkedList<Integer> adjListArray[];
    public double frogPosition(int n, int[][] edges, int t, int target) {
        if (n == 1) return 1.0;
        adjListArray = new LinkedList[n + 1];
        for(int i = 0; i <= n ; i++) adjListArray[i] = new LinkedList<>();
        for (int[] edge : edges) {
            adjListArray[edge[0]].add(edge[1]);
            adjListArray[edge[1]].add(edge[0]);
        }

        return dfs(1, t, target, new boolean[n + 1]);
    }

    private double dfs(int node, int t, int target, boolean[] visited) {
        if (node != 1 && adjListArray[node].size() == 1 || t == 0) {
            if (node == target)
                return 1;
            else return 0;
        }
        visited[node] = true;
        double res = 0.0;
        for (int child : adjListArray[node]) {
            if (visited[child]) continue; // skip visited children
            res += dfs(child, t - 1, target, visited);
        }
        if (node != 1)
            return res * 1.0 / (adjListArray[node].size() - 1);
        else
            return res * 1.0 / (adjListArray[node].size());
    }
}
```
**C++**
by @SuperWhw
```cpp
    vector<vector<int> > G;
    vector<bool> seen;
    int target;

    double frogPosition(int n, vector<vector<int>>& edges, int t, int target) {
        if (n == 1) return 1.0;
        this->target = target;
        G = vector<vector<int> >(n + 1);
        for (auto e : edges) {
            G[e[0]].push_back(e[1]);
            G[e[1]].push_back(e[0]);
        }
        seen = vector<bool>(n + 1, false);

        return dfs(1, t);
    }

    double dfs(int i, int t) {
        if (i != 1 && G[i].size() == 1 || t == 0) {
            return i == target;
        }
        seen[i] = true;
        double res = 0;
        for (auto j : G[i]) {
            if (! seen[j]) {
                res += dfs(j, t - 1);
            }
        }
        return res / (G[i].size() - (i != 1));
    }
```
**Python:**
```py
    def frogPosition(self, n, edges, t, target):
        if n == 1: return 1.0
        G = [[] for i in xrange(n + 1)]
        for i, j in edges:
            G[i].append(j)
            G[j].append(i)
        seen = [0] * (n + 1)

        def dfs(i, t):
            if i != 1 and len(G[i]) == 1 or t == 0:
                return i == target
            seen[i] = 1
            res = sum(dfs(j, t - 1) for j in G[i] if not seen[j])
            return res * 1.0 / (len(G[i]) - (i != 1))
        return dfs(1, t)
```

</p>


