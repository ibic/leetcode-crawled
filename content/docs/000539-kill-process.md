---
title: "Kill Process"
weight: 539
#id: "kill-process"
---
## Description
<div class="description">
<p>Given <b>n</b> processes, each process has a unique <b>PID (process id)</b> and its <b>PPID (parent process id)</b>. 

<p>Each process only has one parent process, but may have one or more children processes. This is just like a tree structure.  Only one process has PPID that is 0, which means this process has no parent process. All the PIDs will be distinct positive integers.</p>

<p>We use two list of integers to represent a list of processes, where the first list contains PID for each process and the second list contains the corresponding PPID. </p>
 
<p>Now given the two lists, and a PID representing a process you want to kill, return a list of PIDs of processes that will be killed in the end. You should assume that when a process is killed, all its children processes will be killed. No order is required for the final answer.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> 
pid =  [1, 3, 10, 5]
ppid = [3, 0, 5, 3]
kill = 5
<b>Output:</b> [5,10]
<b>Explanation:</b> 
           3
         /   \
        1     5
             /
            10
Kill 5 will also kill 10.
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The given kill id is guaranteed to be one of the given PIDs.</li>
<li>n >= 1.</li>
</ol>
</p>
</div>

## Tags
- Tree (tree)
- Queue (queue)

## Companies
- Bloomberg - 2 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC] 

## Solution

---
#### Approach #1 Depth First Search [Time Limit Exceeded]

**Algorithm**

Since killing a process leads to killing all its children processes, the simplest solution is to traverse over the $$ppid$$ array and find out all the children of the process to be killed. Further, for every child chosen to be killed we recursively make call to the `killProcess` function now treating this child as the new parent to be killed. In every such call, we again traverse over the $$ppid$$ array now considering the id of the child process, and continue in the same fashion. Further, at every step, for every process chosen to be killed, it is added to the list $$l$$ that needs to be returned at the end.

<iframe src="https://leetcode.com/playground/aMxgv45H/shared" frameBorder="0" name="aMxgv45H" width="100%" height="309"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^n)$$. $$O(n^n)$$ function calls will be made in the worst case

* Space complexity : $$O(n)$$. The depth of the recursion tree can go upto $$n$$.

---
#### Approach #2 Tree Simulation [Accepted]

**Algorithm**

We can view the given process relationships in the form of a tree. We can construct the tree in such a way that every node stores information about its own value as well as the list of all its direct children nodes. Thus, now, once the tree has been generated, we can simply start off by killing the required node, and recursively killing the children of each node encountered rather than traversing over the whole $$ppid$$ array for every node as done in the previous approach.

In order to implement this, we've made use of a $$Node$$ class which represents a node of a tree. Each node represents a process. Thus, every node stores its own value($$Node.val$$) and the list of all its direct children($$Node.children$$). We traverse over the whole $$pid$$ array and create nodes for all of them. Then, we traverse over the $$ppid$$ array, and make the parent nodes out of them, and at the same time add all their direct children nodes in their $$Node.children$$ list. In this way, we convert the given process structure into a tree structure. 

Now, that we've obtained the tree structure, we can add the node to be killed to the return list $$l$$. Now, we can directly obtain all the direct children of this node from the tree, and add its direct children to the return list. For every node added to the return list, we repeat the same process of obtaining the children recursively. 

<iframe src="https://leetcode.com/playground/3vVYTuvw/shared" frameBorder="0" name="3vVYTuvw" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We need to traverse over the $$ppid$$ and $$pid$$ array of size $$n$$ once. The `getAllChildren` function also takes atmost $$n$$ time, since no node can be a child of two nodes.

* Space complexity : $$O(n)$$. $$map$$ of size $$n$$ is used. 

---

#### Approach #3 HashMap + Depth First Search  [Accepted]

**Algorithm**

Instead of making the tree structure, we can directly make use of a data structure which stores a particular process value and the list of its direct children. For this, in the current implementation, we make use of a hashmap $$map$$, which stores the data in the form $${parent: [list of all its direct children]}$$.

Thus, now, by traversing just once over the $$ppid$$ array, and adding the corresponding $$pid$$ values to the children list at the same time, we can obtain a better structure storing the parent-children relationship.

Again, similar to the previous approach, now we can add the process to be killed to the return list, and keep on adding its children to the return list in a recursive manner by obtaining the child information from the structure created previously.

!?!../Documents/582_Kill_Process.json:1000,563!?!

<iframe src="https://leetcode.com/playground/RqpXvrnt/shared" frameBorder="0" name="RqpXvrnt" width="100%" height="462"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n)$$. We need to traverse over the $$ppid$$ array of size $$n$$ once. The `getAllChildren` function also takes atmost $$n$$ time, since no node can be a child of two nodes.

* Space complexity : $$O(n)$$. $$map$$ of size $$n$$ is used.

---
#### Approach #4 HashMap + Breadth First Search [Accepted]:

**Algorithm**

We can also make use of Breadth First Search to obtain all the children(direct+indirect) of a particular node, once the data structure of the form $$(process: [list of all its direct children]$$ has been obtained. The process of obtaining the data structure is the same as in the previous approach. 

In order to obtain all the child processes to be killed for a particular parent chosen to be killed, we can make use of Breadth First Search. For this, we add the node to be killed to a $$queue$$. Then, we remove an element from the front of the $$queue$$ and add it to the return list. Further, for every element removed from the front of the queue, we add all its direct children(obtained from the data structure created) to the end of the queue. We keep on doing so till the queue becomes empty.

!?!../Documents/582_Kill_Process_BFS.json:1000,563!?!

<iframe src="https://leetcode.com/playground/XkRcahEn/shared" frameBorder="0" name="XkRcahEn" width="100%" height="462"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We need to traverse over the $$ppid$$ array of size $$n$$ once. Also, atmost $$n$$ additions/removals are done from the $$queue$$.

* Space complexity : $$O(n)$$. $$map$$ of size $$n$$ is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution, HashMap
- Author: shawngao
- Creation Date: Sun May 14 2017 11:04:06 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 01:52:15 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public List<Integer> killProcess(List<Integer> pid, List<Integer> ppid, int kill) {
        if (kill == 0) return pid;
        
        int n = pid.size();
        Map<Integer, Set<Integer>> tree = new HashMap<>();
        for (int i = 0; i < n; i++) {
            tree.put(pid.get(i), new HashSet<Integer>());
        }
        for (int i = 0; i < n; i++) {
            if (tree.containsKey(ppid.get(i))) {
                Set<Integer> children = tree.get(ppid.get(i));
                children.add(pid.get(i));
                tree.put(ppid.get(i), children);
            }
        }
        
        List<Integer> result = new ArrayList<>();
        traverse(tree, result, kill);
        
        return result;
    }
    
    private void traverse(Map<Integer, Set<Integer>> tree, List<Integer> result, int pid) {
        result.add(pid);
        
        Set<Integer> children = tree.get(pid);
        for (Integer child : children) {
            traverse(tree, result, child);
        }
    }
}
```
</p>


### Java DFS O(n) Time O(n) Space
- Author: compton_scatter
- Creation Date: Sun May 14 2017 11:04:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 14 2017 11:04:00 GMT+0800 (Singapore Standard Time)

<p>
```
public List<Integer> killProcess(List<Integer> pid, List<Integer> ppid, int kill) {
    
    // Store process tree as an adjacency list
    Map<Integer, List<Integer>> adjacencyLists = new HashMap<>();
    for (int i=0;i<ppid.size();i++) {
        adjacencyLists.putIfAbsent(ppid.get(i), new LinkedList<>());
        adjacencyLists.get(ppid.get(i)).add(pid.get(i));
    }
    
    // Kill all processes in the subtree rooted at process "kill"
    List<Integer> res = new LinkedList<>();
    Stack<Integer> stack = new Stack<>();
    stack.add(kill);
    while (!stack.isEmpty()) {
        int cur = stack.pop();
        res.add(cur);
        List<Integer> adjacencyList = adjacencyLists.get(cur);
        if (adjacencyList == null) continue;
        stack.addAll(adjacencyList);
    }
    return res;   

}
```
</p>


### Simple Python BFS Solution
- Author: lee215
- Creation Date: Wed May 17 2017 20:24:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 09 2018 01:49:04 GMT+0800 (Singapore Standard Time)

<p>
Build a  ```(int parent: list[int] children) ```hashMap and do a simple bfs.
``````
def killProcess(self, pid, ppid, kill):
        d = collections.defaultdict(list)
        for c, p in zip(pid, ppid): d[p].append(c)
        bfs = [kill]
        for i in bfs: bfs.extend(d.get(i, []))
        return bfs
</p>


