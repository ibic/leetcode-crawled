---
title: "Simplified Fractions"
weight: 1320
#id: "simplified-fractions"
---
## Description
<div class="description">
<p>Given an integer <code>n</code>, return a list of all <strong>simplified</strong> fractions between 0 and 1 (exclusive) such that the denominator is less-than-or-equal-to <code>n</code>. The fractions can be in <strong>any</strong> order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 2
<strong>Output:</strong> [&quot;1/2&quot;]
<strong>Explanation: </strong>&quot;1/2&quot; is the only unique fraction with a denominator less-than-or-equal-to 2.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 3
<strong>Output:</strong> [&quot;1/2&quot;,&quot;1/3&quot;,&quot;2/3&quot;]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 4
<strong>Output:</strong> [&quot;1/2&quot;,&quot;1/3&quot;,&quot;1/4&quot;,&quot;2/3&quot;,&quot;3/4&quot;]
<strong>Explanation: </strong>&quot;2/4&quot; is not a simplified fraction because it can be simplified to &quot;1/2&quot;.</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> []
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 100</code></li>
</ul>
</div>

## Tags
- Math (math)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Use GCD to judge valid fraction w/ analysis.
- Author: rock
- Creation Date: Sun May 17 2020 00:02:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 01:27:17 GMT+0800 (Singapore Standard Time)

<p>
Compute greatest common divisor (GCD) for each pair (i, j), where i < j, if GCD(i, j) == 1, then `i / j` is a valid fraction.

```java
    public List<String> simplifiedFractions(int n) {
        List<String> ans = new ArrayList<>();
        for (int denominator = 2; denominator <= n; ++denominator) {
            for (int numerator = 1; numerator < denominator; ++numerator) {
                if (gcd(numerator, denominator) == 1) {
                    ans.add(numerator + "/" + denominator);
                }
            }
        }
        return ans;
    }
    private int gcd(int x, int y) {
        return x == 0 ? y : gcd(y % x, x);
    }
```
```python
    def simplifiedFractions(self, n: int) -> List[str]:
        
        def gcd(x: int, y: int) -> int:
            return y if x == 0 else gcd(y % x, x)
        
        return [str(i) + \'/\' + str(j) for j in range(2, n + 1) for i in range(1, j) if gcd(i, j) == 1]
```
**Analysis:**
Each GCD computation cost O(log(max(x,y))), and there are O(n ^ 2) iterations for the nested loop, therefore,

Time::O(n ^ 2 * logn)`, space: `O(n ^ 2)` if including return list, otherwise `O(logn) due to the recursion.

In case you are after O(1) space excluding return list, use the following GCD code:
```java
    private int gcd(int x, int y) {
        while (x > 0) {
            int t = x;
            x = y % x;
            y = t;
        }
        return y;
    }
```

```python
        def gcd(x: int, y: int) -> int:
            while x > 0:
                x, y = y % x, x
            return y
```
</p>


### [Python3] Easy GCD
- Author: localhostghost
- Creation Date: Sun May 17 2020 00:03:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 00:03:51 GMT+0800 (Singapore Standard Time)

<p>
Just plain old GCD.
##### Writing GCD Ourselves
```
class Solution:
    def simplifiedFractions(self, n: int) -> List[str]:
        def gcd(x, y):
            while y:
                x, y = y, x % y
            return x    
        
        res = []
        for i in range(2, n + 1):
            for j in range(1, i):
                if gcd(i, j) == 1:
                    res.append(str(j) + \'/\' + str(i))
        return res
```
##### Using Python math library
```
class Solution:
    def simplifiedFractions(self, n: int) -> List[str]:
        res = []
        for i in range(2, n + 1):
            for j in range(1, i):
                if math.gcd(i, j) == 1:
                    res.append(str(j) + \'/\' + str(i))
        return res
```
</p>


### Easy to Understand | Detailed Explanation
- Author: rkkrdssd1
- Creation Date: Mon Sep 28 2020 23:14:14 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 28 2020 23:14:14 GMT+0800 (Singapore Standard Time)

<p>
https://www.youtube.com/watch?v=5PQySV1LIsU

```
class Solution {
    public List<String> simplifiedFractions(int n) {
        List<String> list = new ArrayList();
        for(int i = 1; i < n; i++) {
            for(int j = i + 1; j <= n; j++) {
                if(gcd(i, j) == 1)
                    list.add(i + "/" + j);
            }
        }
        return list;
    }
    
    private int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }
}
	```
</p>


