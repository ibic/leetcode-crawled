---
title: "Customer Placing the Largest Number of Orders"
weight: 1493
#id: "customer-placing-the-largest-number-of-orders"
---
## Description
<div class="description">
<p>Query the <b>customer_number</b> from the <b><i>orders</i></b> table for the customer who has placed the largest number of orders.</p>

<p>It is guaranteed that exactly one customer will have placed more orders than any other customer.</p>

<p>The <b><i>orders</i></b> table is defined as follows:</p>

<pre>
| Column            | Type      |
|-------------------|-----------|
| order_number (PK) | int       |
| customer_number   | int       |
| order_date        | date      |
| required_date     | date      |
| shipped_date      | date      |
| status            | char(15)  |
| comment           | char(200) |
</pre>

<p><b>Sample Input</b></p>

<pre>
| order_number | customer_number | order_date | required_date | shipped_date | status | comment |
|--------------|-----------------|------------|---------------|--------------|--------|---------|
| 1            | 1               | 2017-04-09 | 2017-04-13    | 2017-04-12   | Closed |         |
| 2            | 2               | 2017-04-15 | 2017-04-20    | 2017-04-18   | Closed |         |
| 3            | 3               | 2017-04-16 | 2017-04-25    | 2017-04-20   | Closed |         |
| 4            | 3               | 2017-04-18 | 2017-04-28    | 2017-04-25   | Closed |         |
</pre>

<p><b>Sample Output</b></p>

<pre>
| customer_number |
|-----------------|
| 3               |
</pre>

<p><b>Explanation</b></p>

<pre>
The customer with number &#39;3&#39; has two orders, which is greater than either customer &#39;1&#39; or &#39;2&#39; because each of them  only has one order. 
So the result is customer_number &#39;3&#39;.
</pre>

<p><i><b>Follow up:</b> What if more than one customer have the largest number of orders, can you find all the customer_number in this case?</i></p>

</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `LIMIT` [Accepted]

**Algorithm**

First, we can select the <b>customer_number</b> and the according count of orders using `GROUP BY`.
```sql
SELECT
    customer_number, COUNT(*)
FROM
    orders
GROUP BY customer_number
```

| customer_number | COUNT(*) |
|-----------------|----------|
| 1               | 1        |
| 2               | 1        |
| 3               | 2        |

Then, the <b>customer_number</b> of first record is the result after sorting them by order count descending.

| customer_number | COUNT(*) |
|-----------------|----------|
| 3               | 2        |

In MySQL, the [LIMIT](https://dev.mysql.com/doc/refman/5.7/en/select.html) clause can be used to constrain the number of rows returned by the SELECT statement. It takes one or two nonnegative numeric arguments, the first of which specifies the offset of the first row to return, and the second specifies the maximum number of rows to return. The offset of the initial row is 0 (not 1).

It can be used with only one argument, which specifies the number of rows to return from the beginning of the result set. So `LIMIT 1` will return the first record.

**MySQL**

```sql
SELECT
    customer_number
FROM
    orders
GROUP BY customer_number
ORDER BY COUNT(*) DESC
LIMIT 1
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Solution using group by
- Author: sonali3
- Creation Date: Thu Jun 01 2017 04:55:26 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 06:47:42 GMT+0800 (Singapore Standard Time)

<p>
```
select customer_number from orders
group by customer_number
order by count(*) desc limit 1;
```
</p>


### follow-up: GROUP BY + HAVING COUNT = + SUBQUERY without using MAX
- Author: BobbyCurry
- Creation Date: Wed Jul 18 2018 09:48:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 05:16:43 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT customer_number
FROM orders
GROUP BY customer_number
HAVING COUNT(order_number) = (
	SELECT COUNT(order_number) cnt
	FROM orders
	GROUP BY customer_number
	ORDER BY cnt DESC
	LIMIT 1
)
```
</p>


### Solution with GROUP BY and ORDER BY
- Author: HapiCoding
- Creation Date: Sat Sep 02 2017 03:14:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 02 2017 03:14:38 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT  customer_number
FROM    orders
GROUP BY customer_number
ORDER BY COUNT(order_number) DESC 
LIMIT 1
```
</p>


