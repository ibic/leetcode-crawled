---
title: "Last Substring in Lexicographical Order"
weight: 1040
#id: "last-substring-in-lexicographical-order"
---
## Description
<div class="description">
<p>Given a string <code>s</code>, return the last substring of <code>s</code> in lexicographical order.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;abab&quot;</span>
<strong>Output: </strong><span id="example-output-1">&quot;bab&quot;</span>
<strong>Explanation: </strong>The substrings are [&quot;a&quot;, &quot;ab&quot;, &quot;aba&quot;, &quot;abab&quot;, &quot;b&quot;, &quot;ba&quot;, &quot;bab&quot;]. The lexicographically maximum substring is &quot;bab&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;leetcode&quot;</span>
<strong>Output: </strong><span id="example-output-2">&quot;tcode&quot;</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= s.length &lt;= 4&nbsp;* 10^5</code></li>
	<li><font face="monospace">s</font> contains only lowercase English letters.</li>
</ol>

</div>

## Tags
- String (string)
- Suffix Array (suffix-array)

## Companies
- Goldman Sachs - 15 (taggedByAdmin: false)
- Roblox - 4 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Mathworks - 8 (taggedByAdmin: true)
- Two Sigma - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short python code, O(n) time and O(1) space, with proof and visualization
- Author: nate17
- Creation Date: Wed Aug 21 2019 19:06:30 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 27 2019 22:53:15 GMT+0800 (Singapore Standard Time)

<p>
The solution must be in one of those n suffix candidates, and **the main idea here is to eliminate those candidates we are certain not the solution, at last, the only one left is our solution**.

Given `i < j`, assuming that for any `p < j` and `p != i`, `s[p:]` is smaller than the maximum substring (not the solution). 

For `j = i+d`, where `d >= 1`, and  `k >= 0`, considering for any `0 <= x < k, s[i+x] == s[j+x]`, we have several cases to discuss.
1. `s[i+k] == s[j+k]`, we simply `k++`;
2. `s[i+k] > s[j+k]`, for any `0 <= p0 <= k`, we have substring `s[j+p0:] < s[i+p0:] = s[j-d+p0:] = s[j+p1:] < s[i+p1:] = ...` and so on, where we have `p0 = p1+d > p1 > p2... `until `j+pn < j`, in other words `s[j+p0:] < s[p:]` for some `p` where `i <= p < j`, therefore `s[j+p0:]` will not be the solution, we can safely update `j` to` j+k+1` **while still holding our assumption for any `p < j
and
p != i`, `s[p:]` is not the solution;**
<img src="https://assets.leetcode.com/users/nate17/image_1566444266.png" alt="" style="width: 400px;"/>


3. `s[i+k] < s[j+k]`, for any `0 <= p0 <= k`, we have substring `s[i+p0:] < s[j+p0:] = s[i+d+p0:] = s[i+p1:] < s[j+p1:] = ...` and so on, where we have `p0 = p1-d < p1 < p2...` until `i+pn > i+k`, in other words `s[i+p0:] < s[p:]` for some p where `p > i+k`, therefore `s[i+p0:]` will not be our solution, we can safely update `i` to `max(i+k+1, j)` and then update `j` to `i+1`  **while holding our assumption;**
<img src="https://assets.leetcode.com/users/nate17/image_1566445075.png" alt="" style="width: 450px;"/>

In addition, we take into account the base case where `i+1 == j`, and `i == 0`, our assumption holds by itself. 

In the end, we can regard `s[j+k] == ""` as a empty character when `j+k == n`, which falls into case two `s[i+k] > s[j+k]` in our discussion above, therefore eliminating all candidates from `j` to `j+k`, leaving the only one `s[i:]`, hence our solution.

```
class Solution:
    def lastSubstring(self, s: str) -> str:
        i, j, k = 0, 1, 0
        n = len(s)
        while j + k < n:
            if s[i+k] == s[j+k]:
                k += 1
                continue
            elif s[i+k] > s[j+k]:
                j = j + k + 1
            else:
                i = max(i + k + 1, j)
                j = i + 1
            k = 0
        return s[i:]
```
</p>


### Python O(n) with explanation
- Author: mapleisle
- Creation Date: Sun Aug 18 2019 12:52:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 18 2019 12:54:25 GMT+0800 (Singapore Standard Time)

<p>
First, the answer must be starting with the largest letter, ending at the end of string S. So we save all the "candidates" by their starting point, and assign a pointer to each of them (pointing at the start).
Then we increment the pointer for each candidate, using following rules:
1. We filter the candidates by the letter their pointers are at. Only the ones with the largest letter will go into next round.
2. If a pointer meets a starting point, it "swallows" the next candidate like a snake.
In the following image, pointer of A meets the starting point of B. Suppose we want to keep candidate B. The only possibility is the future letters are lexicographically larger than candidate B, so appending it to B makes B larger than A. Apprently it can not be, otherwise B (and A) will not be selected initially as candidates.

![image](https://assets.leetcode.com/users/mapleisle/image_1566102896.png)

Finally we will have only one candidate.
This gives O(n) time complexity. Not very strictly, assume we start with k candidates, then eliminating all except one candidate takes n/k steps. In each step, we only increment the pointer of each candidate by one.
Correct me if I made a mistake. :)

```
class Solution:
    def lastSubstring(self, s: str) -> str:
        count=collections.defaultdict(list)
        for i in range(len(s)):
            count[s[i]].append(i)
        largeC=max(count.keys())
        starts={}
        for pos in count[largeC]:
            starts[pos]=pos+1
		# Initialize all candidates and their pointers
		
        while len(starts)>1:
		# Eliminate till we have only one
            toDel=set()
            nextC=collections.defaultdict(list)
            for start,end in starts.items():
                if end==len(s):
				# Remove if reaching the end
                    toDel.add(start)
                    continue
					
                nextC[s[end]].append(start)
				# Filter by current letter
				
                if end in starts:
                    toDel.add(end)
				# "Swallow" the latter candidate
			
            nextStarts={}
            largeC=max(nextC.keys())
            for start in nextC[largeC]:
                if start not in toDel:
                    nextStarts[start]=starts[start]+1
					# Select what we keep for the next step
            starts=nextStarts.copy()
        for start,end in starts.items():
            return s[start:]
```
</p>


### C++ SIMPLE EASY SOLUTION WITH EXPLANATION, CHEERS!!!
- Author: Lovedeep
- Creation Date: Fri Apr 17 2020 02:17:24 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Apr 17 2020 02:17:24 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
// We use "j" to find a better starting index. If any is found, we use it to update "i"

// 1."i" is the starting index of the first substring
// 2."j" is the staring index of the second substring
// 3."k" is related to substring.length() (eg. "k" is substring.length()-1)

// Case 1 (s[i+k]==s[j+k]):
// -> If s.substr(i,k+1)==s.substr(j,k+1), we keep increasing k.
// Case 2 (s[i+k]<s[j+k]):
// -> If the second substring is larger, we update i with j. (The final answer is s.substr(i))
// Case 3 (s[i+k]>s[j+k]):
// -> If the second substring is smaller, we just change the starting index of the second string to j+k+1. Because s[j]~s[j+k] must be less than s[i], otherwise "i" will be updated by "j". So the next possible candidate is "j+k+1".

    string lastSubstring(string s) {
        int n=s.length(),i=0,j=1,k=0;
        while(j+k<n)
        {
            if(s[i+k]==s[j+k]) k++; 
            else if(s[i+k]<s[j+k]) i=j++,k=0;
            else j+=k+1,k=0;
        }
        return s.substr(i);
    }
};
```
</p>


