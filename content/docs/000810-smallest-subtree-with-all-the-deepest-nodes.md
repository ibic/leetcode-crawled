---
title: "Smallest Subtree with all the Deepest Nodes"
weight: 810
#id: "smallest-subtree-with-all-the-deepest-nodes"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a binary tree, the depth of each node is <strong>the shortest distance to the root</strong>.</p>

<p>Return <em>the smallest subtree</em> such that it contains <strong>all the deepest nodes</strong> in the original tree.</p>

<p>A node is called <strong>the&nbsp;deepest</strong> if it has the largest depth possible among&nbsp;any node in the entire tree.</p>

<p>The <strong>subtree</strong> of a node is tree consisting of that node, plus the set of all descendants of that node.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/07/01/sketch1.png" style="width: 600px; height: 510px;" />
<pre>
<strong>Input:</strong> root = [3,5,1,6,2,0,8,null,null,7,4]
<strong>Output:</strong> [2,7,4]
<strong>Explanation:</strong> We return the node with value 2, colored in yellow in the diagram.
The nodes coloured in blue are the deepest nodes of the tree.
Notice that nodes 5, 3 and 2 contain the deepest nodes in the tree but node 2 is the smallest subtree among them, so we return it.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = [1]
<strong>Output:</strong> [1]
<strong>Explanation:</strong> The root is the deepest node in the tree.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [0,1,3,null,2]
<strong>Output:</strong> [2]
<strong>Explanation:</strong> The deepest node in the tree is 2, the valid subtrees are the subtrees of nodes 2, 1 and 0 but the subtree of node 2 is the smallest.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree will be in the range <code>[1, 500]</code>.</li>
	<li>The values of the nodes in the tree&nbsp;are <strong>unique</strong>.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Facebook - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Paint Deepest Nodes

**Intuition**

We try a straightforward approach that has two phases.

The first phase is to identify the nodes of the tree that are deepest.  To do this, we have to annotate the depth of each node.  We can do this with a depth first search.

Afterwards, we will use that annotation to help us find the answer:

* If the `node` in question has maximum depth, it is the answer.

* If both the left and right child of a `node` have a deepest descendant, then the answer is this parent `node`.  

* Otherwise, if some child has a deepest descendant, then the answer is that child.

* Otherwise, the answer for this subtree doesn't exist.

**Algorithm**

In the first phase, we use a depth first search `dfs` to annotate our nodes.

In the second phase, we also use a depth first search `answer(node)`, returning the answer for the subtree at that `node`, and using the rules above to build our answer from the answers of the children of `node`.

Note that in this approach, the `answer` function returns answers that have the deepest nodes of the *entire* tree, not just the subtree being considered.

<iframe src="https://leetcode.com/playground/yJHD2oW7/shared" frameBorder="0" width="100%" height="500" name="yJHD2oW7"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the tree.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Recursion

**Intuition**

We can combine both depth first searches in *Approach #1* into an approach that does both steps in one pass.  We will have some function `dfs(node)` that returns both the answer for this subtree, and the distance from `node` to the deepest nodes in this subtree.

**Algorithm**

The `Result` (on some subtree) returned by our (depth-first search) recursion will have two parts:
* `Result.node`: the largest depth node that is equal to or an ancestor of all the deepest nodes of this subtree.
* `Result.dist`: the number of nodes in the path from the root of this subtree, to the deepest node in this subtree.

We can calculate these answers disjointly for `dfs(node)`:

* To calculate the `Result.node` of our answer:

    * If one `childResult` has deeper nodes, then `childResult.node` will be the answer.

    * If they both have the same depth nodes, then `node` will be the answer.

* The `Result.dist` of our answer is always 1 more than the largest `childResult.dist` we have.

<iframe src="https://leetcode.com/playground/ajdfAWTD/shared" frameBorder="0" width="100%" height="500" name="ajdfAWTD"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the tree.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] One Pass
- Author: lee215
- Creation Date: Sun Jul 08 2018 11:22:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 27 2020 16:01:51 GMT+0800 (Singapore Standard Time)

<p>
# Explanatoin
Write a sub function `deep(TreeNode root)`.
Return a `pair(int depth, TreeNode subtreeWithAllDeepest)`

In sub function `deep(TreeNode root)`:

**if root == null**,
return pair(0, null)

**if left depth == right depth**,
deepest nodes both in the left and right subtree,
return pair (left.depth + 1, root)

**if left depth > right depth**,
deepest nodes only in the left subtree,
return pair (left.depth + 1, left subtree)

**if left depth < right depth**,
deepest nodes only in the right subtree,
return pair (right.depth + 1, right subtree)
<br>

# Complexity
Time `O(N)`
Space `O(height)`

**C++**
```cpp
    TreeNode* subtreeWithAllDeepest(TreeNode* root) {
        return deep(root).second;
    }

    pair<int, TreeNode*> deep(TreeNode* root) {
        if (!root) return {0, NULL};
        pair<int, TreeNode*> l = deep(root->left), r = deep(root->right);

        int d1 = l.first, d2 = r.first;
        return {max(d1, d2) + 1, d1 == d2 ? root : d1 > d2 ? l.second : r.second};
    }
```

**Java**
```java
    public TreeNode subtreeWithAllDeepest(TreeNode root) {
        return deep(root).getValue();
    }

    public Pair<Integer, TreeNode> deep(TreeNode root) {
        if (root == null) return new Pair(0, null);
        Pair<Integer, TreeNode> l = deep(root.left), r = deep(root.right);

        int d1 = l.getKey(), d2 = r.getKey();
        return new Pair(Math.max(d1, d2) + 1, d1 == d2 ? root : d1 > d2 ? l.getValue() : r.getValue());
    }
```
**Python**
```py
    def subtreeWithAllDeepest(self, root):
        def deep(root):
            if not root: return 0, None
            l, r = deep(root.left), deep(root.right)
            if l[0] > r[0]: return l[0] + 1, l[1]
            elif l[0] < r[0]: return r[0] + 1, r[1]
            else: return l[0] + 1, root
        return deep(root)[1]
```

</p>


### Description of what the problem is asking is unclear
- Author: hp685
- Creation Date: Mon Jul 09 2018 12:43:40 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 11:24:04 GMT+0800 (Singapore Standard Time)

<p>
Problem definition can be rephrased or additional sample test cases added to make the problem statement clear. 
</p>


### Simple recursive Java Solution 
- Author: the-traveller
- Creation Date: Sun Jul 08 2018 11:06:11 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 28 2018 11:32:31 GMT+0800 (Singapore Standard Time)

<p>
```
	public int depth(TreeNode root){
		if(root == null ) return 0;
		return Math.max(depth(root.left),depth(root.right))+1;
	}
	public TreeNode subtreeWithAllDeepest(TreeNode root) {
			if( root == null ) return null;
			int left =  depth(root.left);
			int right = depth(root.right);
			if( left  == right ) return root;
			if( left > right ) return subtreeWithAllDeepest(root.left);
			return subtreeWithAllDeepest(root.right);
	}
```
		

The time complexity of above code is O(N^2) since a binary tree can degenerate to a linked list, the worst complexity to calculate depth is O(N) and so the overall time complexity is O(N^2). Here is the memoized version:

Time complexity: O(N)

 ```
 public int depth(TreeNode root,HashMap<TreeNode,Integer> map){
         if(root == null ) return 0;
         if( map.containsKey(root) ) return map.get(root); 
         int max = Math.max(depth(root.left,map),depth(root.right,map))+1;
         map.put(root,max);   
         return max;
    }
    public TreeNode dfs(TreeNode root, HashMap<TreeNode,Integer> map){
        int left =  depth(root.left,map);
        int right = depth(root.right,map);
        if( left  == right ) return root;
        if( left > right ) return dfs(root.left,map);
        return dfs(root.right,map);
    }
    public TreeNode subtreeWithAllDeepest(TreeNode root) {
        if( root == null ) return null;
        HashMap<TreeNode,Integer> map = new HashMap<>();
        depth(root,map);
        return dfs(root,map);
    }
```
		
</p>


