---
title: "Construct K Palindrome Strings"
weight: 1288
#id: "construct-k-palindrome-strings"
---
## Description
<div class="description">
<p>Given a string <code>s</code> and an integer <code>k</code>. You should construct <code>k</code> non-empty <strong>palindrome</strong> strings using <strong>all the characters</strong> in <code>s</code>.</p>

<p>Return <em><strong>True</strong></em> if you can use all the characters in <code>s</code> to construct <code>k</code> palindrome strings or <em><strong>False</strong></em> otherwise.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;annabelle&quot;, k = 2
<strong>Output:</strong> true
<strong>Explanation:</strong> You can construct two palindromes using all characters in s.
Some possible constructions &quot;anna&quot; + &quot;elble&quot;, &quot;anbna&quot; + &quot;elle&quot;, &quot;anellena&quot; + &quot;b&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;leetcode&quot;, k = 3
<strong>Output:</strong> false
<strong>Explanation:</strong> It is impossible to construct 3 palindromes using all the characters of s.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;true&quot;, k = 4
<strong>Output:</strong> true
<strong>Explanation:</strong> The only possible solution is to put each character in a separate string.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;yzyzyzyzyzyzyzy&quot;, k = 2
<strong>Output:</strong> true
<strong>Explanation:</strong> Simply you can put all z&#39;s in one string and all y&#39;s in the other string. Both strings will be palindrome.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;cr&quot;, k = 7
<strong>Output:</strong> false
<strong>Explanation:</strong> We don&#39;t have enough characters in s to construct 7 palindromes.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10^5</code></li>
	<li>All characters in <code>s</code> are lower-case English letters.</li>
	<li><code>1 &lt;= k &lt;= 10^5</code></li>
</ul>
</div>

## Tags
- Greedy (greedy)

## Companies
- Uber - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Apr 05 2020 00:09:29 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 15 2020 23:11:45 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Condition 1. `odd characters <= k`
Count the occurrences of all characters.
If one character has odd times occurrences,
there must be at least one palindrome,
with odd length and this character in the middle.
So we count the characters that appear odd times,
the number of odd character should not be bigger than `k`.

Condition 2. `k <= s.length()`
Also, if we have one character in each palindrome,
we will have at most `s.length()`  palindromes,
so we need `k <= s.length()`.

The above two condition are necessary and sufficient conditions for this problem.
So we return `odd <= k <= n`
<br>

# **Construction**
@spjparmar immediately got a question like why this works always for all strings.
He gave the some following dry runs. :)
For any string with 0 odd character count , we can form k no. of palindrome strings for sure with k<=n
(This is why k<=n)

**eg**
aabb, k=1| abba
aabb, k=2 | aa, bb
aabb, k=3 | a, a, bb
aabb, k=4 | a, a, b, b

For any string with odd character count <=k , we can always form k palindrome string for sure with k<=n
**eg2**
aabbc, k=1 | aacbb
aabbc, k=2 | aca, bb
aabbc, k=3 | a,a, bcb
aabbc, k=4 | a, a, c ,bb
aabbc, k=5 | a, a, c, b, b

**eg3**
aabc, k=1 | N/A
aabc, k=2 | aba, c
aabc, k=3 | aa, b, c
aabc, k=4 | a, a, b, c

Hope this helps somebody.
<br>

# **Complexity**
Time `O(N)`
Space `O(1)`
<br>

**Java:**
```java
    public boolean canConstruct(String s, int k) {
        int odd = 0, n = s.length(), count[] = new int[26];
        for (int i = 0; i < n; ++i) {
            count[s.charAt(i) - \'a\'] ^= 1;
            odd += count[s.charAt(i) - \'a\'] > 0 ? 1 : -1;
        }
        return odd <= k && k <= n;
    }
```

**C++:**
```cpp
    bool canConstruct(string s, int k) {
        bitset<26> odd;
        for (char& c : s)
            odd.flip(c - \'a\');
        return odd.count() <= k && k <= s.length();
    }
```

**Python:**
```py
    def canConstruct(self, s, k):
        return sum(i & 1 for i in collections.Counter(s).values()) <= k <= len(s)
```
</p>


### 2 Conditions Only
- Author: white_shadow_
- Creation Date: Sun Apr 05 2020 00:11:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 05 2020 01:06:29 GMT+0800 (Singapore Standard Time)

<p>
Upvote please 
1. k>=(no of chars with count odd)
2. s.size()>=k
click :-https://www.youtube.com/watch?v=rmyj8GtVFmo
</p>


### [java/Python 3] Count odd occurrences - 2 codes w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Apr 05 2020 00:02:00 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 18 2020 11:14:41 GMT+0800 (Singapore Standard Time)

<p>
**Explanation of the method** - credit to **@lionkingeatapple**
If we need to construct `k` palindromes, and each palindrome at most has`1` character with odd times of occurrence. The oddest times of occurrence we can have is `1 * k = k`. Therefore, `oddNum <= k`. And it is impossible to construct `k` palindromes when the input string s\'s length is less than `k`. So we can conclude enable to construct `k` palindromes it must satisfy the condition `oddNum <= k` && `k <= s.length()`.

----

1. Each palindrome at most has one character with odd times of occurrence;
2. `Non-empty` means each  palindrome at least has one character, hence `k <= s.length()`.

**Method 1:**
```java
    public boolean canConstruct(String s, int k) {
        int[] count = new int[26];
        for (int i = 0; i < s.length(); ++i) {
            ++count[s.charAt(i) - \'a\'];
        }
        int oddNum = 0;
        for (int i = 0; i < 26; ++i) {
            oddNum += count[i] % 2;
        }
        return oddNum <= k && k <= s.length();
    }
```
Or make the above 2 for loops as 1:
```java
    public boolean canConstruct(String s, int k) {
        int[] count = new int[26];
        int oddNum = 0;
        for (int i = 0; i < s.length(); ++i) {
            oddNum += ++count[s.charAt(i) - \'a\'] % 2 == 0 ? -1 : 1;
        }
        return oddNum <= k && k <= s.length();
    }
```
```python
    def canConstruct(self, s: str, k: int) -> bool:
        return sum(v % 2 for v in collections.Counter(s).values()) <= k <= len(s)
```

----

**Method 2:**
Or use bit manipulation:
```java
    public boolean canConstruct(String s, int k) {
        int odd = 0;
        for (int i = 0; i < s.length(); ++i) {
            odd ^= 1 << (s.charAt(i) - \'a\');
        }
        return Integer.bitCount(odd) <= k && k <= s.length();
    }
```
```python
    def canConstruct(self, s: str, k: int) -> bool:
        odd = 0
        for c in s:
            odd ^= 1 << (ord(c) - ord(\'a\'))
        return bin(odd).count(\'1\') <= k <= len(s)
```

**Analysis:**
Time: O(n), space: O(1), where n = s.length().
</p>


