---
title: "Lexicographical Numbers"
weight: 369
#id: "lexicographical-numbers"
---
## Description
<div class="description">
<p>Given an integer <i>n</i>, return 1 - <i>n</i> in lexicographical order.</p>

<p>For example, given 13, return: [1,10,11,12,13,2,3,4,5,6,7,8,9].</p>

<p>Please optimize your algorithm to use less time and space. The input size may be as large as 5,000,000.</p>

</div>

## Tags


## Companies
- ByteDance - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Bloomberg - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java DFS Solution
- Author: xialanxuan1015
- Creation Date: Wed Aug 24 2016 10:39:52 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 22:19:02 GMT+0800 (Singapore Standard Time)

<p>

```
The idea is pretty simple. If we look at the order we can find out we just keep adding digit from 0 to 9 to every digit and make it a tree.
Then we visit every node in pre-order. 
       1        2        3    ...
      /\        /\       /\
   10 ...19  20...29  30...39   ....

```


```
public class Solution {
    public List<Integer> lexicalOrder(int n) {
        List<Integer> res = new ArrayList<>();
        for(int i=1;i<10;++i){
          dfs(i, n, res); 
        }
        return res;
    }
    
    public void dfs(int cur, int n, List<Integer> res){
        if(cur>n)
            return;
        else{
            res.add(cur);
            for(int i=0;i<10;++i){
                if(10*cur+i>n)
                    return;
                dfs(10*cur+i, n, res);
            }
        }
    }
}

```
</p>


### Java O(n) time, O(1) space iterative solution 130ms
- Author: songzec
- Creation Date: Tue Aug 23 2016 06:17:01 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 20:25:03 GMT+0800 (Singapore Standard Time)

<p>
```
public List<Integer> lexicalOrder(int n) {
        List<Integer> list = new ArrayList<>(n);
        int curr = 1;
        for (int i = 1; i <= n; i++) {
            list.add(curr);
            if (curr * 10 <= n) {
                curr *= 10;
            } else if (curr % 10 != 9 && curr + 1 <= n) {
                curr++;
            } else {
                while ((curr / 10) % 10 == 9) {
                    curr /= 10;
                }
                curr = curr / 10 + 1;
            }
        }
        return list;
    }
```

The basic idea is to find the next number to add.
Take 45 for example: if the current number is 45, the next one will be 450 (450 == 45 * 10)(if 450 <= n), or 46 (46 == 45 + 1) (if 46 <= n) or 5 (5 == 45 / 10 + 1)(5 is less than 45 so it is for sure less than n).
We should also consider n = 600, and the current number = 499, the next number is 5 because there are all "9"s after "4" in "499" so we should divide 499 by 10 until the last digit is not "9".
It is like a tree, and we are easy to get a sibling, a left most child and the parent of any node.
</p>


### AC 200ms c++ solution, beats 98%
- Author: withacup
- Creation Date: Mon Aug 22 2016 22:49:19 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 03:02:37 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    vector<int> lexicalOrder(int n) {
        vector<int> res(n);
        int cur = 1;
        for (int i = 0; i < n; i++) {
            res[i] = cur;
            if (cur * 10 <= n) {
                cur *= 10;
            } else {
                if (cur >= n) 
                    cur /= 10;
                cur += 1;
                while (cur % 10 == 0)
                    cur /= 10;
            }
        }
        return res;
    }
};
```
</p>


