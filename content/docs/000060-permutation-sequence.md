---
title: "Permutation Sequence"
weight: 60
#id: "permutation-sequence"
---
## Description
<div class="description">
<p>The set <code>[1,2,3,...,<em>n</em>]</code> contains a total of <em>n</em>! unique permutations.</p>

<p>By listing and labeling all of the permutations in order, we get the following sequence for <em>n</em> = 3:</p>

<ol>
	<li><code>&quot;123&quot;</code></li>
	<li><code>&quot;132&quot;</code></li>
	<li><code>&quot;213&quot;</code></li>
	<li><code>&quot;231&quot;</code></li>
	<li><code>&quot;312&quot;</code></li>
	<li><code>&quot;321&quot;</code></li>
</ol>

<p>Given <em>n</em> and <em>k</em>, return the <em>k</em><sup>th</sup> permutation sequence.</p>

<p><strong>Note:</strong></p>

<ul>
	<li>Given <em>n</em> will be between 1 and 9 inclusive.</li>
	<li>Given&nbsp;<em>k</em>&nbsp;will be between 1 and <em>n</em>! inclusive.</li>
</ul>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 3, k = 3
<strong>Output:</strong> &quot;213&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 4, k = 9
<strong>Output:</strong> &quot;2314&quot;
</pre>

</div>

## Tags
- Math (math)
- Backtracking (backtracking)

## Companies
- Google - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Solution Pattern

There are three main types of interview questions about permutations:

- 1.[Generate all permutations](https://leetcode.com/articles/permutations/).

- 2.[Generate next permutation](https://leetcode.com/articles/next-permutation/).

- 3.Generate the permutation number k (current problem).

If the order of generated permutations is not important, one could use 
["swap" backtracking](https://leetcode.com/articles/permutations/) 
to solve the first problem and to generate all 
$$N!$$ permutations in $$\mathcal{O}(N \times N!)$$ time. 

Although, it is better to generate permutations in lexicographically sorted order using 
[D.E. Knuth algorithm](https://leetcode.com/articles/next-permutation/). This algorithm 
generates new permutation from the previous one in $$\mathcal{O}(N)$$ time.
The same algorithm could be used to solve the second problem above.

The problem number three is where the fun starts because the above two algorithms do not apply: 

- You will be asked to fit into polynomial time complexity, _i.e._ no backtracking.

- The previous permutation is unknown, _i.e._ you cannot use D.E. Knuth algorithm. 

To solve the problem, one could use a pretty elegant idea that is based on the mapping.
It's much easier to generate numbers than combinations or permutations.

>So let us generate numbers, and then map them to combinations/subsets/permutations.

This sort of encoding is widely used in password-cracking algorithms. 

For example, [in a previous article](https://leetcode.com/articles/subsets/) we discussed 
how one could map a subset with a binary bitmask of length N, 
where i*th* `0` means "the element number i is absent" and i*th* `1` means 
"the element number i is present".

One could do the same for permutations, mapping permutation with the integer in 
[Factorial Number System Representation](https://en.wikipedia.org/wiki/Factorial_number_system). 
<br />
<br />


---
#### Approach 1: Factorial Number System

**Why Do We Need Factorial Number System**

Usually standard decimal or binary 
[positional system](https://en.wikipedia.org/wiki/Numeral_system#Positional_systems_in_detail)
could meet our needs. For example, each subset could be described by a number in binary representation

$$
k = \sum\limits_{m = 0}^{N - 1}{k_m 2^m}, \qquad 0 \le k_m \le 1 
$$

Here is how it works:

![diff](../Figures/60/subsets.png)

The problem with permutations is that there is a much more permutations than subsets, 
$$N!$$ grows up much faster than $$2^N$$.
Therefore, the solution space provided by the positional system with constant base 
cannot match with the number of permutations.

Here is where the factorial number system enters the scene. 
It's a positional system with _non-constant base_ $$m!$$

$$
k = \sum\limits_{m = 0}^{N - 1}{k_m m!}, \qquad 0 \le k_m \le m 
$$
 
Note, that magnitude of weights is not constant as well and depends on base: 
$$0 \le k_m \le m$$ for the base $$m!$$, _i.e._ $$k_0 = 0$$, $$0 \le k_1 \le 1$$,
$$0 \le k_2 \le 2$$, etc. 

Here is how this mapping works:

![diff](../Figures/60/permutations2.png)

We could now map all permutations, from permutation number zero: 
$$k = 0 = \sum\limits_{m = 0}^{N - 1}{0 \times m!}$$ to permutation number $$N! - 1$$:
$$k = N! - 1 = \sum\limits_{m = 0}^{N - 1}{m \times m!}$$.

> Hence we have a way to encode permutation number into
factorial representation. Now let us use this factorial representation 
to construct the permutation itself.

**How to Construct the Permutation from its Factorial Representation**

Let us pick up $$N = 3$$, which corresponds to the input array `nums = [1, 2, 3]`, and 
construct its permutation number $$k = 3$$. 
Since we number the permutations from 0 to $$N! - 1$$ 
(and _not_ from 1 to $$N!$$ as in the problem description), for us that 
will be the permutation number $$k = 2$$.

Let us first construct the factorial representation of $$k = 2$$:

$$
k = 2 = 1 \times 2! + 0 \times 1! + 0 \times 0! = (1, 0, 0)
$$

> The coefficients in factorial representation 
are indexes of elements in the input array. 
These are not direct indexes, but the indexes after the removal of already used elements. 
That's a consequence of the fact that each element 
should be used in permutation only once.

![diff](../Figures/60/index.png)

Here the first number is `1`, _i.e._ the first element in the permutation 
is `nums[1] = 2`. Let us use `nums[1] = 2` in the permutation and then delete it
from `nums`, since each element should be used only once.

![diff](../Figures/60/step1.png)

Next coefficient in factorial representation is `0`. 
Let's use `nums[0] = 1` in the permutation and then delete it
from `nums`.

![diff](../Figures/60/step2.png)

Next coefficient in factorial representation is `0`. 
Let's use `nums[0] = 3` in the permutation and then delete it
from `nums`.
The job is done.

![diff](../Figures/60/step3.png)

**Algorithm**

- Generate input array `nums` of numbers from $$1$$ to $$N$$.

- Compute all factorial bases from $$0$$ to $$(N  - 1)!$$.  

- Decrease $$k$$ by 1 to make it fit into $$(0, N! - 1)$$ interval.

- Compute factorial representation of $$k$$. Use factorial 
coefficients to construct the permutation. 

- Return the permutation string.

**Implementation**

<iframe src="https://leetcode.com/playground/VcFhgJ7r/shared" frameBorder="0" width="100%" height="500" name="VcFhgJ7r"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N^2)$$, because to delete elements from 
the list in a loop one has to perform $$N + (N - 1) + ... + 1 = 
N(N - 1)/2$$ operations. 
    
* Space complexity: $$\mathcal{O}(N)$$. 
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### "Explain-like-I'm-five" Java Solution in O(n)
- Author: tso
- Creation Date: Mon Jun 29 2015 02:07:10 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 15:42:35 GMT+0800 (Singapore Standard Time)

<p>
I'm sure somewhere can be simplified so it'd be nice if anyone can let me know. The pattern was that:

say n = 4, you have {1, 2, 3, 4}

If you were to list out all the permutations you have 

1 + (permutations of 2, 3, 4)
<br>2 + (permutations of 1, 3, 4)
<br>3 + (permutations of 1, 2, 4)
<br>4 + (permutations of 1, 2, 3)

<br>We know how to calculate the number of permutations of n numbers... n! So each of those with permutations of 3 numbers means there are 6 possible permutations. Meaning there would be a total of 24 permutations in this particular one. So if you were to look for the (k = 14) 14th permutation, it would be in the 

3 + (permutations of 1, 2, 4) subset. 

To programmatically get that, you take k = 13 (subtract 1 because of things always starting at 0) and divide that by the 6 we got from the factorial, which would give you the index of the number you want. In the array {1, 2, 3, 4}, k/(n-1)! = 13/(4-1)! = 13/3! = 13/6 = 2. The array {1, 2, 3, 4} has a value of 3 at index 2. So the first number is a 3.

Then the problem repeats with less numbers.

The permutations of {1, 2, 4} would be:

1 + (permutations of 2, 4)
<br>2 + (permutations of 1, 4)
<br>4 + (permutations of 1, 2)

But our k is no longer the 14th, because in the previous step, we've already eliminated the 12 4-number permutations starting with 1 and 2. So you subtract 12 from k.. which gives you 1. Programmatically that would be...

k = k - (index from previous) * (n-1)! = k - 2*(n-1)! = 13 - 2*(3)! = 1

In this second step, permutations of 2 numbers has only 2 possibilities, meaning each of the three permutations listed above a has two possibilities, giving a total of 6. We're looking for the first one, so that would be in the 1 + (permutations of 2, 4) subset. 

Meaning: index to get number from is k / (n - 2)!  = 1 / (4-2)! = 1 / 2! = 0.. from {1, 2, 4}, index 0 is 1

<br>so the numbers we have so far is 3, 1... and then repeating without explanations.

<br>{2, 4}
<br>k = k - (index from pervious) * (n-2)! = k - 0 * (n - 2)! = 1 - 0 = 1;
<br>third number's index = k / (n - 3)! = 1 / (4-3)! = 1/ 1! = 1... from {2, 4}, index 1 has 4
<br>Third number is 4

<br>{2}
<br>k = k - (index from pervious) * (n - 3)! = k - 1 * (4 - 3)! = 1 - 1 = 0;
<br>third number's index = k / (n - 4)! = 0 / (4-4)! = 0/ 1 = 0... from {2}, index 0 has 2
<br>Fourth number is 2

<br>Giving us 3142. If you manually list out the permutations using DFS method, it would be 3142. Done! It really was all about pattern finding.



    public class Solution {
    public String getPermutation(int n, int k) {
        int pos = 0;
        List<Integer> numbers = new ArrayList<>();
        int[] factorial = new int[n+1];
        StringBuilder sb = new StringBuilder();
        
        // create an array of factorial lookup
        int sum = 1;
        factorial[0] = 1;
        for(int i=1; i<=n; i++){
            sum *= i;
            factorial[i] = sum;
        }
        // factorial[] = {1, 1, 2, 6, 24, ... n!}
        
        // create a list of numbers to get indices
        for(int i=1; i<=n; i++){
            numbers.add(i);
        }
        // numbers = {1, 2, 3, 4}
        
        k--;
        
        for(int i = 1; i <= n; i++){
            int index = k/factorial[n-i];
            sb.append(String.valueOf(numbers.get(index)));
            numbers.remove(index);
            k-=index*factorial[n-i];
        }
        
        return String.valueOf(sb);
    }
}
</p>


### Share my Python solution with detailed explanation
- Author: dasheng2
- Creation Date: Mon Jul 20 2015 23:59:53 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 13:51:06 GMT+0800 (Singapore Standard Time)

<p>
The idea is as follow:

For permutations of n, the first (n-1)! permutations start with 1, next (n-1)! ones start with 2, ... and so on. And in each group of (n-1)! permutations, the first (n-2)! permutations start with the smallest remaining number, ...

take n = 3 as an example, the first 2 (that is, (3-1)! ) permutations start with 1, next 2 start with 2 and last 2 start with 3. For the first 2 permutations (123 and 132), the 1st one (1!) starts with 2, which is the smallest remaining number (2 and 3). So we can use a loop to check the region that the sequence number falls in and get the starting digit. Then we adjust the sequence number and continue.

    import math
    class Solution:
        # @param {integer} n
        # @param {integer} k
        # @return {string}
        def getPermutation(self, n, k):
            numbers = range(1, n+1)
            permutation = ''
            k -= 1
            while n > 0:
                n -= 1
                # get the index of current digit
                index, k = divmod(k, math.factorial(n))
                permutation += str(numbers[index])
                # remove handled number
                numbers.remove(numbers[index])
    
            return permutation
</p>


### An iterative solution for reference
- Author: Adeath
- Creation Date: Fri Nov 14 2014 06:02:06 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 05:45:40 GMT+0800 (Singapore Standard Time)

<p>
Recursion will use more memory, while this problem can be solved by iteration. I solved this problem before, but I didn't realize that using k = k-1 would avoid dealing with case k%(n-1)!==0. Rewrote this code, should be pretty concise now. 

Only thing is that I have to use a list to store the remaining numbers, neither linkedlist nor arraylist are very efficient, anyone has a better idea?

The logic is as follows: for n numbers the permutations can be divided to (n-1)! groups, for n-1 numbers can be divided to (n-2)! groups, and so on. Thus k/(n-1)! indicates the index of current number, and k%(n-1)! denotes remaining index for the remaining n-1 numbers.
We keep doing this until n reaches 0, then we get n numbers permutations that is kth. 

    public String getPermutation(int n, int k) {
            List<Integer> num = new LinkedList<Integer>();
            for (int i = 1; i <= n; i++) num.add(i);
            int[] fact = new int[n];  // factorial
            fact[0] = 1;
            for (int i = 1; i < n; i++) fact[i] = i*fact[i-1];
            k = k-1;
            StringBuilder sb = new StringBuilder();
            for (int i = n; i > 0; i--){
                int ind = k/fact[i-1];
                k = k%fact[i-1];
                sb.append(num.get(ind));
                num.remove(ind);
            }
            return sb.toString();
        }
</p>


