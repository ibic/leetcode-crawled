---
title: "Minimum Moves to Reach Target with Rotations"
weight: 1169
#id: "minimum-moves-to-reach-target-with-rotations"
---
## Description
<div class="description">
<p>In an&nbsp;<code>n*n</code>&nbsp;grid, there is a snake that spans 2 cells and starts moving from the top left corner at <code>(0, 0)</code> and <code>(0, 1)</code>. The grid has empty cells represented by zeros and blocked cells represented by ones. The snake wants to reach the lower right corner at&nbsp;<code>(n-1, n-2)</code>&nbsp;and&nbsp;<code>(n-1, n-1)</code>.</p>

<p>In one move the snake can:</p>

<ul>
	<li>Move one cell to the right&nbsp;if there are no blocked cells there. This move keeps the horizontal/vertical position of the snake as it is.</li>
	<li>Move down one cell&nbsp;if there are no blocked cells there. This move keeps the horizontal/vertical position of the snake as it is.</li>
	<li>Rotate clockwise if it&#39;s in a horizontal position and the two cells under it are both empty. In that case the snake moves from&nbsp;<code>(r, c)</code>&nbsp;and&nbsp;<code>(r, c+1)</code>&nbsp;to&nbsp;<code>(r, c)</code>&nbsp;and&nbsp;<code>(r+1, c)</code>.<br />
	<img alt="" src="https://assets.leetcode.com/uploads/2019/09/24/image-2.png" style="width: 300px; height: 134px;" /></li>
	<li>Rotate counterclockwise&nbsp;if it&#39;s in a vertical position and the two cells to its right are both empty. In that case the snake moves from&nbsp;<code>(r, c)</code>&nbsp;and&nbsp;<code>(r+1, c)</code>&nbsp;to&nbsp;<code>(r, c)</code>&nbsp;and&nbsp;<code>(r, c+1)</code>.<br />
	<img alt="" src="https://assets.leetcode.com/uploads/2019/09/24/image-1.png" style="width: 300px; height: 121px;" /></li>
</ul>

<p>Return the minimum number of moves to reach the target.</p>

<p>If there is no way to reach the target, return&nbsp;<code>-1</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/09/24/image.png" style="width: 400px; height: 439px;" /></strong></p>

<pre>
<strong>Input:</strong> grid = [[0,0,0,0,0,1],
               [1,1,0,0,1,0],
&nbsp;              [0,0,0,0,1,1],
&nbsp;              [0,0,1,0,1,0],
&nbsp;              [0,1,1,0,0,0],
&nbsp;              [0,1,1,0,0,0]]
<strong>Output:</strong> 11
<strong>Explanation:
</strong>One possible solution is [right, right, rotate clockwise, right, down, down, down, down, rotate counterclockwise, right, down].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> grid = [[0,0,1,1,1,1],
&nbsp;              [0,0,0,0,1,1],
&nbsp;              [1,1,0,0,0,1],
&nbsp;              [1,1,1,0,0,1],
&nbsp;              [1,1,1,0,0,1],
&nbsp;              [1,1,1,0,0,0]]
<strong>Output:</strong> 9
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 100</code></li>
	<li><code>0 &lt;= grid[i][j] &lt;= 1</code></li>
	<li>It is guaranteed that the snake starts at empty cells.</li>
</ul>

</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies
- Kakao - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Standard Python BFS solution
- Author: otoc
- Creation Date: Sun Sep 29 2019 12:23:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 29 2019 12:23:34 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def minimumMoves(self, grid: List[List[int]]) -> int:
        n = len(grid)
        start = (0, 0, 0, 1)
        end = (n - 1, n - 2, n - 1, n - 1)
        curr_level = {start}
        moves = 0
        visited = set()
        while curr_level:
            next_level = set()
            for pos in curr_level:
                visited.add(pos)
                r1, c1, r2, c2 = pos
                if c1 + 1 < n and grid[r1][c1+1] == 0 and c2 + 1 < n and grid[r2][c2+1] == 0:
                    if (r1, c1 + 1, r2, c2 + 1) not in visited:
                        next_level.add((r1, c1 + 1, r2, c2 + 1))
                if r1 + 1 < n and grid[r1+1][c1] == 0 and r2 + 1 < n and grid[r2+1][c2] == 0:
                    if (r1 + 1, c1, r2 + 1, c1) not in visited:
                        next_level.add((r1 + 1, c1, r2 + 1, c2))
                if r1 == r2 and c2 == c1 + 1 and r1 + 1 < n and grid[r1+1][c1] + grid[r1+1][c1+1] == 0 :
                    if (r1, c1, r1 + 1, c1) not in visited:
                        next_level.add((r1, c1, r1 + 1, c1))
                if c1 == c2 and r2 == r1 + 1 and c1 + 1 < n and grid[r1][c1+1] + grid[r1+1][c1+1] == 0:
                    if (r1, c1, r1, c1 + 1) not in visited:
                        next_level.add((r1, c1, r1, c1 + 1))
            if end in next_level:
                return moves + 1
            curr_level = next_level
            moves += 1
        return -1
```
</p>


### C++ BFS
- Author: votrubac
- Creation Date: Sun Sep 29 2019 12:05:47 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 02 2019 07:08:43 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
This looks like BFS problem, where horizontal and vertical paths are independent (but can be transitioned to and from). So, we need to track \'visited\' state independently for horizontal and vertical orientations.
# Modified BFS
This type of problems is not hard, but it could be trickly to get the code right. I created three helper functions to make it a bit easier.

1. Use a queue to store the tail coordinates and orientation.
2. For every element in the queue, check the visited flag and move up, down or rotate, if possible.
> Here, I am using second and third bit (2 and 4) to track the state directly in the grid.
3. Return the number of steps in BFS if we reach the target; otherwise return ```-1```
```
bool canRotate(vector<vector<int>>& g, int i, int j) {
  return i < g.size() - 1 && j < g[i].size() - 1
    && (g[i + 1][j] & 1) == 0 && (g[i][j + 1] & 1) == 0 && (g[i + 1][j + 1] & 1) == 0;
}
bool canGoDown(vector<vector<int>>& g, int i, int j, bool vertical) {
  if (vertical) return i < g.size() - 2 && (g[i + 2][j] & 1) == 0;
  return i < g.size() - 1 && (g[i + 1][j] & 1) == 0 && (g[i + 1][j + 1] & 1) == 0;
}
bool canGoRight(vector<vector<int>>& g, int i, int j, bool vertical) {
  if (!vertical) return j < g[i].size() - 2 && (g[i][j + 2] & 1) == 0;
  return j < g[i].size() - 1 && (g[i][j + 1] & 1) == 0 && (g[i + 1][j + 1] & 1) == 0;
}
int minimumMoves(vector<vector<int>>& grid, int steps = 0) {
  queue<array<int, 3>> q1, q2;
  q1.push({ 0, 0, false }); // not vertical.
  while (!q1.empty()) {
    while (!q1.empty()) {
      auto& a = q1.front();
      if (a[0] == grid.size() - 1 && a[1] == grid[a[0]].size() - 2) return steps;
      if ((grid[a[0]][a[1]] & (a[2] ? 2 : 4)) == 0) {
        grid[a[0]][a[1]] = grid[a[0]][a[1]] | (a[2] ? 2 : 4);
        if (canGoDown(grid, a[0], a[1], a[2])) q2.push({ a[0] + 1, a[1], a[2] });
        if (canGoRight(grid, a[0], a[1], a[2])) q2.push({ a[0], a[1] + 1, a[2] });
        if (canRotate(grid, a[0], a[1])) q2.push({ a[0], a[1], a[2] ? false : true });
      }
      q1.pop();
    }
    ++steps;
    swap(q1, q2);
  }
  return -1;
}
```
</p>


### [C++] Short BFS (No brainer version)
- Author: PhoenixDD
- Creation Date: Mon Sep 30 2019 10:11:07 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 07 2019 04:13:04 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
The two parts of the snake (head and tail) together define it\'s position and thus we need to BFS keeping that in mind.

**Solution**
Since this is a no brainer version, we keep track of visited nodes just as regular BFS keeping in mind that 1 node is defined by the 2 positions (Snake\'s head and snake\'s tail).
Since C++ doesn\'t provide a hash function for a vector by default we can use `set<vector<int>>` (This increases the time complexity) as our way to keep track of visited nodes.
We can now run a simle BFS using `queue<vector<int>>` and iterate on all possible neighbours for that node by adding them to the queue and keep repeating until we find the target or the queue gets empty.
```c++
class Solution {
public:
    set<vector<int>> visited;
    queue<vector<int>> q;
    void commonMoves(vector<vector<int>>& grid,vector<int> &pos)
    {
        if(pos[3]+1<grid.size()&&!grid[pos[2]][pos[3]+1]&&!grid[pos[0]][pos[1]+1]&&!visited.count({pos[0],pos[1]+1,pos[2],pos[3]+1}))     //Move right
            visited.insert({pos[0],pos[1]+1,pos[2],pos[3]+1}),q.push({pos[0],pos[1]+1,pos[2],pos[3]+1});
        if(pos[2]+1<grid.size()&&!grid[pos[2]+1][pos[3]]&&!grid[pos[0]+1][pos[1]]&&!visited.count({pos[0]+1,pos[1],pos[2]+1,pos[3]}))       //Move down
            visited.insert({pos[0]+1,pos[1],pos[2]+1,pos[3]}),q.push({pos[0]+1,pos[1],pos[2]+1,pos[3]});
    }
    void horizontal(vector<vector<int>>& grid,vector<int> &pos)
    {
        if(pos[0]+1<grid.size()&&!grid[pos[0]+1][pos[1]]&&!grid[pos[2]+1][pos[3]]&&!visited.count({pos[0],pos[1],pos[0]+1,pos[1]}))         //Rotate clockwise
            visited.insert({pos[0],pos[1],pos[0]+1,pos[1]}),q.push({pos[0],pos[1],pos[0]+1,pos[1]});
    }
    void vertical(vector<vector<int>>& grid,vector<int> &pos)
    {
        if(pos[1]+1<grid.size()&&!grid[pos[0]][pos[1]+1]&&!grid[pos[2]][pos[3]+1]&&!visited.count({pos[0],pos[1],pos[0],pos[1]+1}))      //Rotate counter-clockwise
            visited.insert({pos[0],pos[1],pos[0],pos[1]+1}),q.push({pos[0],pos[1],pos[0],pos[1]+1});
    }
    int minimumMoves(vector<vector<int>>& grid) 
    {
        vector<int> target={grid.size()-1,grid.size()-2,grid.size()-1,grid.size()-1}; 
        q.push({0,0,0,1});
        visited.insert({0,0,0,1});
        int size,moves=0;
        while(!q.empty())
        {
            size=q.size();
            while(size--)
            {
                if(q.front()==target)                             //Reached target
                    return moves;
                if(q.front()[0]==q.front()[2])                 //When snake is horizontal
                    horizontal(grid,q.front());
                else                                                   //When snake is vertical
                    vertical(grid,q.front());
		commonMoves(grid,q.front());                 //Common moves (Right and down)
                q.pop();
            }
            moves++;
        }
        return -1;
    }
};
```
**Note**
To further reduce the time complexity, we can use an `unordered_set` with a custom implementation of hash function for a vector, this would reduce complexity of adding and searching on `visited` nodes from O(nlogn) to O(n).
</p>


