---
title: "Letter Combinations of a Phone Number"
weight: 17
#id: "letter-combinations-of-a-phone-number"
---
## Description
<div class="description">
<p>Given a string containing digits from <code>2-9</code> inclusive, return all possible letter combinations that the number could represent. Return the answer in <strong>any order</strong>.</p>

<p>A mapping of digit to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any letters.</p>

<p><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Telephone-keypad2.svg/200px-Telephone-keypad2.svg.png" style="width: 200px; height: 162px;" /></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> digits = &quot;23&quot;
<strong>Output:</strong> [&quot;ad&quot;,&quot;ae&quot;,&quot;af&quot;,&quot;bd&quot;,&quot;be&quot;,&quot;bf&quot;,&quot;cd&quot;,&quot;ce&quot;,&quot;cf&quot;]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> digits = &quot;&quot;
<strong>Output:</strong> []
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> digits = &quot;2&quot;
<strong>Output:</strong> [&quot;a&quot;,&quot;b&quot;,&quot;c&quot;]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= digits.length &lt;= 4</code></li>
	<li><code>digits[i]</code> is a digit in the range <code>[&#39;2&#39;, &#39;9&#39;]</code>.</li>
</ul>

</div>

## Tags
- String (string)
- Backtracking (backtracking)

## Companies
- Amazon - 25 (taggedByAdmin: true)
- Google - 6 (taggedByAdmin: true)
- Atlassian - 6 (taggedByAdmin: false)
- Capital One - 6 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: true)
- eBay - 3 (taggedByAdmin: false)
- Twilio - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Databricks - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- Walmart Labs - 5 (taggedByAdmin: false)
- Salesforce - 4 (taggedByAdmin: false)
- Nutanix - 4 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: true)
- Oracle - 3 (taggedByAdmin: false)
- Roblox - 3 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- Square - 2 (taggedByAdmin: false)
- Dropbox - 4 (taggedByAdmin: true)
- Yahoo - 4 (taggedByAdmin: false)
- VMware - 4 (taggedByAdmin: false)
- JPMorgan - 2 (taggedByAdmin: false)
- Morgan Stanley - 2 (taggedByAdmin: false)
- Quip (Salesforce) - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Backtracking

[Backtracking](https://en.wikipedia.org/wiki/Backtracking) 
is an algorithm for finding all
solutions by exploring all potential candidates.
If the solution candidate turns to be _not_ a solution 
(or at least not the _last_ one), 
backtracking algorithm discards it by making some changes 
on the previous step, *i.e.* _backtracks_ and then try again.

Here is a backtrack function `backtrack(combination, next_digits)`
which takes as arguments an ongoing letter combination 
and the next digits to check.

* If there is no more digits to check
that means that the current combination is done.
* If there are still digits to check :
    * Iterate over the letters mapping the next available digit.
        * Append the current letter to the current combination 
        `combination = combination + letter`.
        * Proceed to check next digits : 
        `backtrack(combination + letter, next_digits[1:])`.
        
!?!../Documents/17_LIS.json:1000,592!?!

<iframe src="https://leetcode.com/playground/3R4cvK34/shared" frameBorder="0" width="100%" height="500" name="3R4cvK34"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(3^N \times 4^M)$$
where `N` is the number of digits in the input that maps to  3 letters 
(*e.g.* `2, 3, 4, 5, 6, 8`) and `M` is the number of digits in the input
that maps to 4 letters (*e.g.* `7, 9`),
and `N+M` is the total number digits in the input.
 
* Space complexity : $$\mathcal{O}(3^N \times 4^M)$$ since one has to keep
$$3^N \times 4^M$$ solutions.

## Accepted Submission (python3)
```python3
class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        if len(digits) == 0:
            return []
        mapping = {
            "2": "abc",
            "3": "def",
            "4": "ghi",
            "5": "jkl",
            "6": "mno",
            "7": "pqrs",
            "8": "tuv",
            "9": "wxyz",
        }
        combs = list(mapping[digits[0]])
        for digit in digits[1:]:
            chars = mapping[digit]
            combs = [comb + c for c in chars for comb in combs]
#            newcombs = []
#            for letter in mapping[digit]:
#                for c in combs:
#                    newcombs.append(c + letter)
#            combs = newcombs
        return combs
```

## Top Discussions
### My java solution with FIFO queue
- Author: lirensun
- Creation Date: Fri Feb 06 2015 08:39:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 04:21:33 GMT+0800 (Singapore Standard Time)

<p>
```
public List<String> letterCombinations(String digits) {
		LinkedList<String> ans = new LinkedList<String>();
		if(digits.isEmpty()) return ans;
		String[] mapping = new String[] {"0", "1", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
		ans.add("");
		for(int i =0; i<digits.length();i++){
			int x = Character.getNumericValue(digits.charAt(i));
			while(ans.peek().length()==i){
				String t = ans.remove();
				for(char s : mapping[x].toCharArray())
					ans.add(t+s);
			}
		}
		return ans;
	}
```

A version without first loop, but same time complexity. Both are single queue BFS solutions.:
```
public List<String> letterCombinations(String digits) {
		LinkedList<String> ans = new LinkedList<String>();
		if(digits.isEmpty()) return ans;
		String[] mapping = new String[] {"0", "1", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
		ans.add("");
		while(ans.peek().length()!=digits.length()){
			String remove = ans.remove();
			String map = mapping[digits.charAt(remove.length())-\'0\'];
			for(char c: map.toCharArray()){
				ans.addLast(remove+c);
			}
		}
		return ans;
	}
```
</p>


### My recursive solution using Java
- Author: lei31
- Creation Date: Fri Dec 26 2014 12:37:36 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 07:50:54 GMT+0800 (Singapore Standard Time)

<p>

 

       public class Solution {
        	private static final String[] KEYS = { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };
        
        	public List<String> letterCombinations(String digits) {
        		List<String> ret = new LinkedList<String>();
        		combination("", digits, 0, ret);
        		return ret;
        	}
        
        	private void combination(String prefix, String digits, int offset, List<String> ret) {
        		if (offset >= digits.length()) {
        			ret.add(prefix);
        			return;
        		}
        		String letters = KEYS[(digits.charAt(offset) - '0')];
        		for (int i = 0; i < letters.length(); i++) {
        			combination(prefix + letters.charAt(i), digits, offset + 1, ret);
        		}
        	}
        }
</p>


### Iterative c++ solution in 0ms
- Author: asbear
- Creation Date: Sat Jun 27 2015 22:35:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 09:12:28 GMT+0800 (Singapore Standard Time)

<p>
    vector<string> letterCombinations(string digits) {
        vector<string> result;
        if(digits.empty()) return vector<string>();
        static const vector<string> v = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
        result.push_back("");   // add a seed for the initial case
        for(int i = 0 ; i < digits.size(); ++i) {
            int num = digits[i]-'0';
            if(num < 0 || num > 9) break;
            const string& candidate = v[num];
            if(candidate.empty()) continue;
            vector<string> tmp;
            for(int j = 0 ; j < candidate.size() ; ++j) {
                for(int k = 0 ; k < result.size() ; ++k) {
                    tmp.push_back(result[k] + candidate[j]);
                }
            }
            result.swap(tmp);
        }
        return result;
    }


Simple and efficient iterative solution.

Explanation with sample input "123"

Initial state:

- result = {""}

Stage 1 for number "1":

- result has {""}
- candiate is "abc"
- generate three strings "" + "a", ""+"b", ""+"c" and put into tmp,
  tmp = {"a", "b","c"}
- swap result and tmp (swap does not take memory copy)
- Now result has {"a", "b", "c"}
 
Stage 2 for number "2":

- result has {"a", "b", "c"}
- candidate is "def"
- generate nine strings and put into tmp,
   "a" + "d", "a"+"e", "a"+"f", 
   "b" + "d", "b"+"e", "b"+"f",
   "c" + "d", "c"+"e", "c"+"f" 
- so tmp has {"ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf" }
- swap result and tmp
- Now result has {"ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf" }

Stage 3 for number "3":

- result has {"ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf" }
- candidate is "ghi"
- generate 27 strings and put into tmp,
-  add "g" for each of "ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf" 
-  add "h" for each of "ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf" 
-  add "h" for each of "ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf" 
- so, tmp has
            {"adg", "aeg", "afg", "bdg", "beg", "bfg", "cdg", "ceg", "cfg"
             "adh", "aeh", "afh", "bdh", "beh", "bfh", "cdh", "ceh", "cfh" 
             "adi", "aei", "afi", "bdi", "bei", "bfi", "cdi", "cei", "cfi" }
- swap result and tmp
- Now result has
            {"adg", "aeg", "afg", "bdg", "beg", "bfg", "cdg", "ceg", "cfg"
             "adh", "aeh", "afh", "bdh", "beh", "bfh", "cdh", "ceh", "cfh" 
             "adi", "aei", "afi", "bdi", "bei", "bfi", "cdi", "cei", "cfi" }


Finally, return result.
</p>


