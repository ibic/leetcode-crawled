---
title: "Number of Closed Islands"
weight: 1198
#id: "number-of-closed-islands"
---
## Description
<div class="description">
<p>Given a 2D&nbsp;<code>grid</code> consists of <code>0s</code> (land)&nbsp;and <code>1s</code> (water).&nbsp; An <em>island</em> is a maximal 4-directionally connected group of <code><font face="monospace">0</font>s</code> and a <em>closed island</em>&nbsp;is an island <strong>totally</strong>&nbsp;(all left, top, right, bottom) surrounded by <code>1s.</code></p>

<p>Return the number of <em>closed islands</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/10/31/sample_3_1610.png" style="width: 240px; height: 120px;" /></p>

<pre>
<strong>Input:</strong> grid = [[1,1,1,1,1,1,1,0],[1,0,0,0,0,1,1,0],[1,0,1,0,1,1,1,0],[1,0,0,0,0,1,0,1],[1,1,1,1,1,1,1,0]]
<strong>Output:</strong> 2
<strong>Explanation:</strong> 
Islands in gray are closed because they are completely surrounded by water (group of 1s).</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/10/31/sample_4_1610.png" style="width: 160px; height: 80px;" /></p>

<pre>
<strong>Input:</strong> grid = [[0,0,1,0,0],[0,1,0,1,0],[0,1,1,1,0]]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,1,1,1,1,1,1],
&nbsp;              [1,0,0,0,0,0,1],
&nbsp;              [1,0,1,1,1,0,1],
&nbsp;              [1,0,1,0,1,0,1],
&nbsp;              [1,0,1,1,1,0,1],
&nbsp;              [1,0,0,0,0,0,1],
               [1,1,1,1,1,1,1]]
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= grid.length, grid[0].length &lt;= 100</code></li>
	<li><code>0 &lt;= grid[i][j] &lt;=1</code></li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Google - 2 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java/C++ with picture, Number of Enclaves
- Author: votrubac
- Creation Date: Sun Nov 10 2019 12:08:41 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 11 2019 02:51:13 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
This is similar to [1020. Number of Enclaves](https://leetcode.com/problems/number-of-enclaves/discuss/265555/C%2B%2B-with-picture-DFS-and-BFS). 

#### Approach 1: Flood Fill

First, we need to remove all land connected to the edges using flood fill.

Then, we can count and flood-fill the remaining islands.

![image](https://assets.leetcode.com/users/votrubac/image_1573360058.png)

**Java**
```
int fill(int[][] g, int i, int j) {
  if (i < 0 || j < 0 || i >= g.length || j >= g[i].length || g[i][j] != 0)
    return 0;
  return (g[i][j] = 1) + fill(g, i + 1, j) + fill(g, i, j + 1)
    + fill(g, i - 1, j) + fill(g, i, j - 1);
}
public int closedIsland(int[][] g) {
  for (int i = 0; i < g.length; ++i)
    for (int j = 0; j < g[i].length; ++j)
      if (i * j * (i - g.length + 1) * (j - g[i].length + 1) == 0)
        fill(g, i, j);
  int res = 0;
  for (int i = 0; i < g.length; ++i)
    for (int j = 0; j < g[i].length; ++j)
      res += fill(g, i, j) > 0 ? 1 : 0;
  return res;
}
```
**C++**
```
int fill(vector<vector<int>>& g, int i, int j) {
    if (i < 0 || j < 0 || i >= g.size() || j >= g[i].size() || g[i][j])
        return 0;
    return (g[i][j] = 1) + fill(g, i + 1, j) + fill(g, i, j + 1) 
        + fill(g, i - 1, j) + fill(g, i, j - 1);
}
int closedIsland(vector<vector<int>>& g, int res = 0) {
    for (int i = 0; i < g.size(); ++i)
        for (int j = 0; j < g[i].size(); ++j)
            if (i * j == 0 || i == g.size() - 1 || j == g[i].size() - 1)
                fill(g, i, j);
    for (int i = 0; i < g.size(); ++i)
        for (int j = 0; j < g[i].size(); ++j)
            res += fill(g, i, j) > 0;
    return res;
}
```
**Complexity Analysis**
- Time: `O(n)`, where `n` is the total number of cells. We flood-fill all land cells once.

- Memory: `O(n)` for the stack. Flood fill is DFS, and the maximum depth is `n`.
</p>


### Python easy understand DFS solution
- Author: too-young-too-naive
- Creation Date: Sun Nov 10 2019 12:03:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 10 2019 12:04:25 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def closedIsland(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if not grid or not grid[0]:
            return 0
        
        m, n = len(grid), len(grid[0])
        
        def dfs(i, j, val):
            if 0<=i<m and 0<=j<n and grid[i][j]==0:
                grid[i][j] = val
                dfs(i, j+1, val)
                dfs(i+1, j, val)
                dfs(i-1, j, val)
                dfs(i, j-1, val)
        
        for i in xrange(m):
            for j in xrange(n):
                if (i == 0 or j == 0 or i == m-1 or j == n-1) and grid[i][j] == 0:
                    dfs(i, j, 1)
                
        res = 0
        for i in xrange(m):
            for j in xrange(n):
                if grid[i][j] == 0:
                    dfs(i, j, 1)
                    res += 1
                    
        return res
        
        
        
        
        
```
</p>


### [Java] Very Simple DFS Solution
- Author: Edwin_Z
- Creation Date: Sun Nov 10 2019 12:06:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 10 2019 12:10:45 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {

    int[][] dir = new int[][]{{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    
    public int closedIsland(int[][] grid) {
        int res = 0;
        for(int i = 0; i < grid.length; i++){
            for(int j = 0; j < grid[0].length; j++){
                if(grid[i][j] == 0){
                    if(dfs(grid, i, j)) res++;
                }
            }
        }
        
        return res;
    }
    
    public boolean dfs(int[][] grid, int x, int y){
        
        if(x < 0 || x >= grid.length || y < 0 || y >= grid[0].length) return false;
        
        if(grid[x][y] == 1) return true;
        
        grid[x][y] = 1;
        
        boolean res = true;
        
        for(int[] d : dir){
            res = res & dfs(grid, x + d[0], y + d[1]);
        }
        
        return res;
    }
}
```
</p>


