---
title: "Strobogrammatic Number"
weight: 230
#id: "strobogrammatic-number"
---
## Description
<div class="description">
<p>A strobogrammatic number is a number that looks the same when rotated 180 degrees (looked at upside down).</p>

<p>Write a function to determine if a number is strobogrammatic. The number is represented as a string.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> num = "69"
<strong>Output:</strong> true
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> num = "88"
<strong>Output:</strong> true
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> num = "962"
<strong>Output:</strong> false
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> num = "1"
<strong>Output:</strong> true
</pre>
</div>

## Tags
- Hash Table (hash-table)
- Math (math)

## Companies
- Facebook - 4 (taggedByAdmin: false)
- Cisco - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 4 lines in Java
- Author: StefanPochmann
- Creation Date: Fri Aug 07 2015 01:47:29 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 23:46:30 GMT+0800 (Singapore Standard Time)

<p>
Just checking the pairs, going inwards from the ends.

    public boolean isStrobogrammatic(String num) {
        for (int i=0, j=num.length()-1; i <= j; i++, j--)
            if (!"00 11 88 696".contains(num.charAt(i) + "" + num.charAt(j)))
                return false;
        return true;
    }
</p>


### Accepted Java solution
- Author: szn1992
- Creation Date: Mon Aug 17 2015 06:28:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 06:55:24 GMT+0800 (Singapore Standard Time)

<p>
    public boolean isStrobogrammatic(String num) {
        Map<Character, Character> map = new HashMap<Character, Character>();
        map.put('6', '9');
        map.put('9', '6');
        map.put('0', '0');
        map.put('1', '1');
        map.put('8', '8');
       
        int l = 0, r = num.length() - 1;
        while (l <= r) {
            if (!map.containsKey(num.charAt(l))) return false;
            if (map.get(num.charAt(l)) != num.charAt(r))
                return false;
            l++;
            r--;
        }
        
        return true;
    }
</p>


### Simple Python Solution
- Author: ZachLiu
- Creation Date: Wed Feb 24 2016 11:25:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 15:20:51 GMT+0800 (Singapore Standard Time)

<p>
    def isStrobogrammatic(self, num):
        """
        :type num: str
        :rtype: bool
        """
        maps = {("0", "0"), ("1", "1"), ("6", "9"), ("8", "8"), ("9", "6")}
        i,j = 0, len(num) - 1
        while i <= j:
            if (num[i], num[j]) not in maps:
                return False
            i += 1
            j -= 1
        return True
</p>


