---
title: "Largest Triangle Area"
weight: 751
#id: "largest-triangle-area"
---
## Description
<div class="description">
<p>You have a list of points in the plane. Return the area of the largest triangle that can be formed by any 3 of the points.</p>

<pre>
<strong>Example:</strong>
<strong>Input:</strong> points = [[0,0],[0,1],[1,0],[0,2],[2,0]]
<strong>Output:</strong> 2
<strong>Explanation:</strong> 
The five points are show in the figure below. The red triangle is the largest.
</pre>

<p><img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/04/04/1027.png" style="height:328px; width:400px" /></p>

<p><strong>Notes: </strong></p>

<ul>
	<li><code>3 &lt;= points.length &lt;= 50</code>.</li>
	<li>No points will be duplicated.</li>
	<li>&nbsp;<code>-50 &lt;= points[i][j] &lt;= 50</code>.</li>
	<li>Answers within&nbsp;<code>10^-6</code>&nbsp;of the true value will be accepted as correct.</li>
</ul>

<p>&nbsp;</p>

</div>

## Tags
- Math (math)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Brute Force [Accepted]

**Intuition**

For each possible triangle, check it's area and keep the area of the largest.

**Algorithm**

We will have 3 for loops to cycle through each choice of 3 points in the array.

After, we'll need a function to calculate the area given 3 points.  Here we have some options:

* We can use the Shoelace formula directly, which tells us the area given the 3 points;

* We can use Heron's formula, which requires the 3 side lengths which we can get by taking the distance of two points;

* We can use the formula `area = 0.5 * a * b * sin(C)` and calculate the angle `C` with trigonometry.

Our implementation illustrates the use of the shoelace formula.

If we did not know the shoelace formula, we could derive it for triangles with the following approach: starting with points `(px, py), (qx, qy), (rx, ry)`, the area of this triangle is the same under a translation by `(-rx, -ry)`, so that the points become `(px-rx, py-ry), (qx-rx, qy-ry), (0, 0)`.

From there, we could draw a square around the triangle with sides touching the coordinate axes, and calculate the area of the square minus the area of the right triangles surrounding the inner triangle.

For more on this approach, see the [Wikipedia entry for the Shoelace formula](https://en.wikipedia.org/wiki/Shoelace_formula).

<iframe src="https://leetcode.com/playground/8zQ89Q5X/shared" frameBorder="0" width="100%" height="327" name="8zQ89Q5X"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^3)$$, where $$N$$ is the length of `points`.  We use three for-loops of length $$O(N)$$, and our work calculating the area of a single triangle is $$O(1)$$.

* Space Complexity: $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Solution with Explanation and Prove
- Author: lee215
- Creation Date: Sun Apr 08 2018 11:05:37 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 01:41:07 GMT+0800 (Singapore Standard Time)

<p>
**Explanaiton**
Burete force loop on all combinations of three points and calculate the area of these three points.
If you google "three pointes triangle area formula", you can find the answer with the first result in second.

**Time complexity**
```O(N^3)``` solution, but ```N <= 50```, so it\'s fast enough.
You may find convex hull first as @weidairpi replies. It help improve to O(M^3 + NlogN) in the best case where M is the number of points on the hull. 
But it make this easy problem complex and it stays same complexity in the worst case.

**Prove 1**
Well, someone complains the situation without any formula.
In fact the formula is not that difficult to find out.

For this case:
![image](https://s3-lc-upload.s3.amazonaws.com/users/lee215/image_1523209147.png)

We can calculate the area as follow:
![image](https://s3-lc-upload.s3.amazonaws.com/users/lee215/image_1523379988.png)


In the result A,B,C are symmetrical, so it won\'t matter what order we name it.
In this case, we calculate the total area by addition three triangle.
In the other cases, you may need to use substraction and it\'s quite the same process.

**Prove 2**
If you are familar with vector product. The result is quite obvious.
![image](https://s3-lc-upload.s3.amazonaws.com/users/lee215/image_1523380000.png)


**C++:**
```
    double largestTriangleArea(vector<vector<int>>& p) {
        double res = 0;
        for (auto &i : p)
            for (auto &j : p)
                for (auto &k : p)
            res = max(res, 0.5 * abs(i[0] * j[1] + j[0] * k[1] + k[0] * i[1]- j[0] * i[1] - k[0] * j[1] - i[0] * k[1]));
        return res;
    }
```
**Java:**
```
    public double largestTriangleArea(int[][] p) {
        double res = 0;
        for (int[] i: p)
            for (int[] j: p)
                for (int[] k: p)
            res = Math.max(res, 0.5 * Math.abs(i[0] * j[1] + j[0] * k[1] + k[0] * i[1]- j[0] * i[1] - k[0] * j[1] - i[0] * k[1]));
        return res;
    }
```

**1-line Python**
```
def largestTriangleArea(self, p):
        return max(0.5 * abs(i[0] * j[1] + j[0] * k[1] + k[0] * i[1]- j[0] * i[1] - k[0] * j[1] - i[0] * k[1])
            for i, j, k in itertools.combinations(p, 3))
```
</p>


### Bad Problem, Solution is Brute Force
- Author: xitizzz
- Creation Date: Wed Apr 18 2018 00:52:01 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 12:49:30 GMT+0800 (Singapore Standard Time)

<p>
This is a bad problem. I don\'t think any major company will ask this. It\'s labeled easy, because the solution if brute force. When you are going to brute force then any problem is easy and unrepresentative of one\'s algorithmic thinking and programming skills. 

I know there are more optimal solutions, but as I understand they are quite complex for \'easy\' problem and are more like \'tricks\' instead of a pure algorithms. Don\'t waste your time on finding a better solution like I did.
</p>


### Simple Java - Easy Understand 
- Author: zzhai
- Creation Date: Mon Apr 16 2018 06:27:08 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 16 2018 06:27:08 GMT+0800 (Singapore Standard Time)

<p>
How to pick all combinaitons from an array that consist of 3 points - 
https://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/

How to calculate the triangle area knowing the coordinates of the 3 points? 
https://www.mathopenref.com/coordtrianglearea.html


```
class Solution {
    public double largestTriangleArea(int[][] points) {
        double max = 0.0; 
        for (int i = 0; i < points.length - 2; i++) 
            for (int j = i + 1; j < points.length - 1; j++) 
                for (int k = j + 1; k < points.length; k++) 
                    max = Math.max(max, areaCal(points[i], points[j], points[k])); 
        return max; 
    }
    
    public double areaCal(int[] pt1, int[] pt2, int[] pt3) {
        return Math.abs(pt1[0] * (pt2[1] - pt3[1]) + pt2[0] * (pt3[1] - pt1[1]) + pt3[0] * (pt1[1] - pt2[1])) / 2.0; 
    }
}
</p>


