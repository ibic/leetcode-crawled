---
title: "Flip Binary Tree To Match Preorder Traversal"
weight: 921
#id: "flip-binary-tree-to-match-preorder-traversal"
---
## Description
<div class="description">
<p>Given a binary tree with <code>N</code> nodes, each node has a different value from&nbsp;<code>{1, ..., N}</code>.</p>

<p>A node in this binary tree can be <em>flipped</em>&nbsp;by swapping the left child and the right child of that node.</p>

<p>Consider the sequence of&nbsp;<code>N</code> values reported by a preorder traversal starting from the root.&nbsp; Call such a sequence of <code>N</code> values the&nbsp;<em>voyage</em>&nbsp;of the tree.</p>

<p>(Recall that a <em>preorder traversal</em>&nbsp;of a node means we report the current node&#39;s value, then preorder-traverse the left child, then preorder-traverse the right child.)</p>

<p>Our goal is to flip the <strong>least number</strong> of nodes in the tree so that the voyage of the tree matches the <code>voyage</code> we are given.</p>

<p>If we can do so, then return a&nbsp;list&nbsp;of the values of all nodes flipped.&nbsp; You may return the answer in any order.</p>

<p>If we cannot do so, then return the list <code>[-1]</code>.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/01/02/1219-01.png" style="width: 88px; height: 120px;" /></strong></p>

<pre>
<strong>Input: </strong>root = <span id="example-input-1-1">[1,2]</span>, voyage = <span id="example-input-1-2">[2,1]</span>
<strong>Output: </strong><span id="example-output-1">[-1]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/01/02/1219-02.png" style="width: 127px; height: 120px;" /></strong></p>

<pre>
<strong>Input: </strong>root = <span id="example-input-2-1">[1,2,3]</span>, voyage = <span id="example-input-2-2">[1,3,2]</span>
<strong>Output: </strong><span id="example-output-2">[1]</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/01/02/1219-02.png" style="width: 127px; height: 120px;" /></strong></p>

<pre>
<strong>Input: </strong>root = <span id="example-input-3-1">[1,2,3]</span>, voyage = <span id="example-input-3-2">[1,2,3]</span>
<strong>Output: </strong><span id="example-output-3">[]</span>
</pre>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ol>
	<li><code>1 &lt;= N &lt;= 100</code></li>
</ol>
</div>
</div>
</div>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Depth-First Search

**Intuition**

As we do a pre-order traversal, we will flip nodes on the fly to try to match our voyage with the given one.

If we are expecting the next integer in our voyage to be `voyage[i]`, then there is only at most one choice for path to take, as all nodes have different values.

**Algorithm**

Do a depth first search.  If at any node, the node's value doesn't match the voyage, the answer is `[-1]`.

Otherwise, we know when to flip: the next number we are expecting in the voyage `voyage[i]` is different from the next child.

<iframe src="https://leetcode.com/playground/2bKKutHj/shared" frameBorder="0" width="100%" height="500" name="2bKKutHj"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the given tree.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] DFS Solution
- Author: lee215
- Creation Date: Sun Jan 06 2019 12:10:44 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 03 2020 12:05:02 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
Global integer `i` indicates next index in voyage `v`.
If current `node == null`, it\'s fine, we return `true`
If current `node.val != v[i]`, doesn\'t match wanted value, return `false`
If left child exist but don\'t have wanted value, flip it with right child.
<br>

# Complexity
Time `O(N)`
Space `O(height)`
<br>

**Java:**
```java
    List<Integer> res = new ArrayList<>();
    int i = 0;
    public List<Integer> flipMatchVoyage(TreeNode root, int[] v) {
        return dfs(root, v) ? res : Arrays.asList(-1);
    }

    public Boolean dfs(TreeNode node, int[] v) {
        if (node == null) return true;
        if (node.val != v[i++]) return false;
        if (node.left != null && node.left.val != v[i]) {
            res.add(node.val);
            return dfs(node.right, v) && dfs(node.left, v);
        }
        return dfs(node.left, v) && dfs(node.right, v);
    }
```

**C++:**
```cpp
    vector<int> res;
    int i = 0;
    vector<int> flipMatchVoyage(TreeNode* root, vector<int>& v) {
        return dfs(root, v) ? res : vector<int>{-1};
    }

    bool dfs(TreeNode* node, vector<int>& v) {
        if (!node) return true;
        if (node->val != v[i++]) return false;
        if (node->left && node->left->val != v[i]) {
            res.push_back(node->val);
            return dfs(node->right, v) && dfs(node->left, v);
        }
        return dfs(node->left, v) && dfs(node->right, v);
    }
```

**Python:**
```py
    def flipMatchVoyage(self, root, voyage):
        res = []
        stack = [root]
        i = 0
        while stack:
            node = stack.pop()
            if not node: continue
            if node and node.val != voyage[i]: return [-1]
            i += 1
            if node.right and node.right.val == voyage[i]:
                if node.left: res.append(node.val)
                stack.extend([node.left, node.right])
            else:
                stack.extend([node.right, node.left])
        return res
```

</p>


### This Problem is Horribly Ambiguous
- Author: woojoo666
- Creation Date: Mon Jan 07 2019 13:53:38 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 07 2019 13:53:38 GMT+0800 (Singapore Standard Time)

<p>
This is a very ambiguously worded problem and I\'m surprised nobody has brought this up yet. There are two ambiguities.

First, the problem gives no information on how the array is being converted to the binary tree, even though it is crucial to determining whether or not the output voyage is achievable or not. For example, if we are to assume that the input array is a pre-order traversal, then there are still multiple ways it can be interpreted (even if we assume the output tree is balanced)

For example, the input `1 2 3 4 5` can be interpreted in two ways:

![image](https://assets.leetcode.com/users/woojoo666/image_1546847268.png)


In fact, almost every input array can be mapped to multiple trees (assuming the input is a pre-order traversal)

Second, the problem does not indicate whether or not children are carried in the swap. The problem states "A node in this binary tree can be flipped by swapping the left child and the right child of that node.", but are we swapping the child nodes, or the child values? If we swap the nodes, then it would move the descendants as well. If we only swap values, then it could lead to problems, eg if the left child has descendants, and the right child is null, then if we swap the values the left child becomes null while still keeping its descendants.

So can anybody clear up these ambiguities? I\'m honestly surprised people are able to solve this problem despite all these issues, but I have a feeling that people are making huge assumptions in order to solve it.
</p>


### C++ 8 lines, simple traversal
- Author: votrubac
- Creation Date: Sun Jan 06 2019 12:39:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 06 2019 12:39:36 GMT+0800 (Singapore Standard Time)

<p>
Note: the clause "flip the least number of nodes" in the problem statement is missleading. There could be zero, or only one possible tree re-arrangement because we have to flip if the next value in the arrangement is not equal to the left child value.

With that, we can just do pre-order traversal and verify that the current node value matches the current voyage value. Also, we check if the next voyage value matches the left child value; if it does not - we flip left and right child and continue. In order to preserve the original tree structure, we are flipping local variables ```l``` and ```r```.
```
vector<int> flips;
bool traverse(TreeNode* root, vector<int>& voyage, int &pos) {
  if (root == nullptr) return true;
  if (root->val != voyage[pos++]) return false;
  auto l = root->left, r = root->right;
  if (l != nullptr && l->val != voyage[pos]) {
    flips.push_back(root->val);
    swap(l, r);
  }
  return traverse(l, voyage, pos) && traverse(r, voyage, pos);
}
vector<int> flipMatchVoyage(TreeNode* root, vector<int>& voyage, int pos = 0) {
  return traverse(root, voyage, pos) ? flips : vector<int>() = { -1 };
}
```
</p>


