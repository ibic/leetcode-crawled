---
title: "Permutation in String"
weight: 532
#id: "permutation-in-string"
---
## Description
<div class="description">
<p>Given two strings <b>s1</b> and <b>s2</b>, write a function to return true if <b>s2</b> contains the permutation of <b>s1</b>. In other words, one of the first string&#39;s permutations is the <b>substring</b> of the second string.</p>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input: </b>s1 = &quot;ab&quot; s2 = &quot;eidbaooo&quot;
<b>Output: </b>True
<b>Explanation:</b> s2 contains one permutation of s1 (&quot;ba&quot;).
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b>s1= &quot;ab&quot; s2 = &quot;eidboaoo&quot;
<b>Output:</b> False
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The input strings only contain lower case letters.</li>
	<li>The length of both given strings is in range [1, 10,000].</li>
</ul>

</div>

## Tags
- Two Pointers (two-pointers)
- Sliding Window (sliding-window)

## Companies
- Facebook - 6 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- Uber - 7 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

**Algorithm**

The simplest method is to generate all the permutations of the short string  and to check if the generated permutation is a substring of the longer string.

In order to generate all the possible pairings, we make use of a function `permute(string_1, string_2, current_index)`. This function creates all the possible permutations of the short string $$s1$$.

To do so, permute takes the index of the current element $$current_index$$ as one of the arguments. Then, it swaps the current element with every other element in the array, lying towards its right, so as to generate a new ordering of the array elements. After the swapping has been done, it makes another call to permute but this time with the index of the next element in the array. While returning back, we reverse the swapping done in the current function call.

Thus, when we reach the end of the array, a new ordering of the array's elements is generated. The following animation depicts the process of generating the permutations.

!?!../Documents/561_Array.json:1000,563!?!

<iframe src="https://leetcode.com/playground/p3HAqkFZ/shared" frameBorder="0" name="p3HAqkFZ" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n!)$$. We match all the permutations of the short string $$s1$$, of length $$s1$$, with $$s2$$. Here, $$n$$ refers to the length of $$s1$$.

* Space complexity : $$O(n^2)$$. The depth of the recursion tree is $$n$$($$n$$ refers to the length of the short string $$s1$$). Every node of the recursion tree contains a string of max. length $$n$$.

---

#### Approach #2 Using sorting [Time Limit Exceeded]:

**Algorithm**

The idea behind this approach is that one string will be a permutation of another string only if both of them contain the same characters the same number of times. One string $$x$$ is a permutation of other string $$y$$ only if $$sorted(x)=sorted(y)$$. 

In order to check this, we can sort the two strings and compare them.  We sort the short string $$s1$$ and all the substrings of $$s2$$, sort them and compare them with the sorted $$s1$$ string. If the two match completely, $$s1$$'s permutation is a substring of $$s2$$, otherwise not.

<iframe src="https://leetcode.com/playground/zWvWj8oK/shared" frameBorder="0" name="zWvWj8oK" width="100%" height="326"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(l_1log(l_1)+(l_2-l_1)l_1log(l_1)\big)$$. where $$l_1$$ is the length of string $$l_1$$ and $$l_2$$ is the length of string $$l_2$$.

* Space complexity : $$O(l_1)$$. $$t$$ array is used .

---

#### Approach #3 Using Hashmap [Time Limit Exceeded]

**Algorithm**

As discussed above, one string will be a permutation of another string only if both of them contain the same charaters with the same frequency. We can consider every possible substring in the long string $$s2$$ of the same length as that of $$s1$$ and check the frequency of occurence of the characters appearing in the two. If the frequencies of every letter match exactly, then only $$s1$$'s permutation can be a substring of $$s2$$. 

In order to implement this approach, instead of sorting and then comparing the elements for equality, we make use of a hashmap $$s1map$$ which stores the frequency of occurence of all the characters in the short string $$s1$$. We consider every possible substring of $$s2$$ of the same length as that of $$s1$$, find its corresponding hashmap as well, namely $$s2map$$. Thus, the substrings considered can be viewed as a window of length as that of $$s1$$ iterating over $$s2$$. If the two hashmaps obtained are identical for any such window, we can conclude that $$s1$$'s permutation is a substring of $$s2$$, otherwise not.

<iframe src="https://leetcode.com/playground/SvwMjwJX/shared" frameBorder="0" name="SvwMjwJX" width="100%" height="513"></iframe>

**Complexity Analysis**

* Time complexity : $$O(l_1+26*l_1*(l_2-l_1))$$. hashmap contains atmost 26 keys. where $$l_1$$ is the length of string $$l_1$$ and $$l_2$$ is the length of string $$l_2$$.

* Space complexity : $$O(1)$$. hashmap contains atmost 26 key-value pairs.

---
#### Approach #4 Using Array [Accepted]

**Algorithm**

Instead of making use of a special HashMap datastructure just to store the frequency of occurence of characters, we can use a simpler array data structure to store the frequencies. Given strings contains only lowercase alphabets ('a' to 'z'). So we need to take an array of size 26.The rest of the process remains the same as the last approach.

<iframe src="https://leetcode.com/playground/48H3F2LJ/shared" frameBorder="0" name="48H3F2LJ" width="100%" height="496"></iframe>
**Complexity Analysis**

* Time complexity : $$O(l_1+26*l_1*(l_2-l_1))$$,  where $$l_1$$ is the length of string $$l_1$$ and $$l_2$$ is the length of string $$l_2$$.

* Space complexity : $$O(1)$$. $$s1map$$ and $$s2map$$ of size 26 is used.

---
#### Approach #5 Sliding Window  [Accepted]:

**Algorithm**

Instead of generating the hashmap afresh for every window considered in $$s2$$, we can create the hashmap just once for the first window in $$s2$$. Then, later on when we slide the window, we know that we remove one preceding character and add a new succeeding character to the new window considered. Thus, we can update the hashmap by just updating the indices associated with those two characters only. Again, for every updated hashmap, we compare all the elements of the hashmap for equality to get the required result.

<iframe src="https://leetcode.com/playground/o4pqWwhn/shared" frameBorder="0" name="o4pqWwhn" width="100%" height="496"></iframe>

**Complexity Analysis**

* Time complexity : $$O(l_1+26*(l_2-l_1))$$,  where $$l_1$$ is the length of string $$l_1$$ and $$l_2$$ is the length of string $$l_2$$.

* Space complexity : $$O(1)$$. Constant space is used.

---
#### Approach #6 Optimized Sliding Window [Accepted]:

**Algorithm**

The last approach can be optimized, if instead of comparing all the elements of the hashmaps for every updated $$s2map$$ corresponding to every window of $$s2$$ considered, we keep a track of the number of elements which were already matching in the earlier hashmap and update just the count of matching elements when we shift the window towards the right.

To do so, we maintain a $$count$$ variable, which stores the number of characters(out of the 26 alphabets), which have the same frequency of occurence in $$s1$$ and the current window in $$s2$$. When we slide the window, if the deduction of the last element and the addition of the new element leads to a new frequency match of any of the characters, we increment the $$count$$ by 1. If not, we keep the $$count$$ intact. But, if a character whose frequency was the same earlier(prior to addition and removal) is added, it now leads to a frequency mismatch which is taken into account by decrementing the same $$count$$ variable. If, after the shifting of the window, the $$count$$ evaluates to 26, it means all the characters match in frequency totally. So, we return a True in that case immediately.

<iframe src="https://leetcode.com/playground/T5J3SxqR/shared" frameBorder="0" name="T5J3SxqR" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(l_1+(l_2-l_1))$$. where $$l_1$$ is the length of string $$l_1$$ and $$l_2$$ is the length of string $$l_2$$.

* Space complexity : $$O(1)$$. Constant space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution, Sliding Window
- Author: shawngao
- Creation Date: Sun Apr 30 2017 11:03:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 13:11:48 GMT+0800 (Singapore Standard Time)

<p>
1. How do we know string ```p``` is a permutation of string ```s```? Easy, each character in ```p``` is in ```s``` too. So we can abstract all permutation strings of ```s``` to a map (Character -> Count). i.e. ```abba``` -> ```{a:2, b:2}```. Since there are only 26 lower case letters in this problem, we can just use an array to represent the map. 
2. How do we know string ```s2``` contains a permutation of ```s1```?  We just need to create a sliding window with length of ```s1```, move from beginning to the end of ```s2```. When a character moves in from right of the window, we subtract ```1``` to that character count from the map. When a character moves out from left of the window, we add ```1``` to that character count. So once we see all zeros in the map, meaning equal numbers of every characters between ```s1``` and the substring in the sliding window, we know the answer is true.
```
public class Solution {
    public boolean checkInclusion(String s1, String s2) {
        int len1 = s1.length(), len2 = s2.length();
        if (len1 > len2) return false;
        
        int[] count = new int[26];
        for (int i = 0; i < len1; i++) {
            count[s1.charAt(i) - 'a']++;
            count[s2.charAt(i) - 'a']--;
        }
        if (allZero(count)) return true;
        
        for (int i = len1; i < len2; i++) {
            count[s2.charAt(i) - 'a']--;
            count[s2.charAt(i - len1) - 'a']++;
            if (allZero(count)) return true;
        }
        
        return false;
    }
    
    private boolean allZero(int[] count) {
        for (int i = 0; i < 26; i++) {
            if (count[i] != 0) return false;
        }
        return true;
    }
}
```
</p>


### What?
- Author: ibra7
- Creation Date: Mon May 18 2020 15:26:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 18 2020 15:26:03 GMT+0800 (Singapore Standard Time)

<p>
Is this not basically the same problem as yesterday, just checking if there is at least one anagram in the string? leetcode cmon man
</p>


### 8 lines slide window solution in Java
- Author: tyuan73
- Creation Date: Sun Apr 30 2017 12:26:17 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 26 2018 14:13:54 GMT+0800 (Singapore Standard Time)

<p>

```
    public boolean checkInclusion(String s1, String s2) {
        int[] count = new int[128];
        for(int i = 0; i < s1.length(); i++) count[s1.charAt(i)]--;
        for(int l = 0, r = 0; r < s2.length(); r++) {
            if (++count[s2.charAt(r)] > 0)
                while(--count[s2.charAt(l++)] != 0) { /* do nothing */}
            else if ((r - l + 1) == s1.length()) return true;
        }
        return s1.length() == 0;
    }
```

**Update**:

I gonna use pictures to describe what the above code does. The first "*for*" loop counts all chars we need to find in a way like digging holes on the ground:

![0_1493821046494_Screen Shot 2017-05-03 at 8.56.24 AM.png](/uploads/files/1493821047834-screen-shot-2017-05-03-at-8.56.24-am.png) 
Blank bars are the holes that we need to fill.

We scan each one char of the string *s2* (by moving index **r** in above code) and put it in the right hole:
![0_1493821246327_Screen Shot 2017-05-03 at 8.56.46 AM.png](/uploads/files/1493821247502-screen-shot-2017-05-03-at-8.56.46-am.png) 
The blue blocks are chars from s2.

But if the char in s2 is not in s1, or, the count of the char is more than the count of the same char in s1, we got some thing like this:
![0_1493822441927_Screen Shot 2017-05-03 at 8.57.32 AM.png](/uploads/files/1493822440796-screen-shot-2017-05-03-at-8.57.32-am.png) 
Note the last blue block sticks out of ground. Any time we encounter a sticking out block - meaning a block with value 1 - we stop scanning (that is moving "***r***"). At this point, there is only one sticking out block.

Now, we have an invalid substring with either invalid char or invalid number of chars. How to remove the invalid char and continue our scan? We use a left index ("***l***" in above code) to remove chars in the holes in the same order we filled them into the holes. We stop removing chars until the only sticking out block is fixed - it has a value of 0 after fixing. Then, we continue our scanning by moving right index "***r***".

Our target is to get:
![0_1493823560108_Screen Shot 2017-05-03 at 9.02.56 AM.png](/uploads/files/1493823558513-screen-shot-2017-05-03-at-9.02.56-am.png) 
To check if all holes are filled perfectly - no more, no less, all have value of 0 - we just need to make sure (r - l + 1) == s1.length().

**Update 2:**
Thanks to  mylemoncake comment. I have updated the last line to :  return s1.length() == 0;  This takes care of the case s1 is an empty string.
</p>


