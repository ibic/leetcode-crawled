---
title: "Minimum Number of Refueling Stops"
weight: 816
#id: "minimum-number-of-refueling-stops"
---
## Description
<div class="description">
<p>A car travels from a starting position to a destination which is <code>target</code> miles east of the starting position.</p>

<p>Along the way, there are gas stations.&nbsp; Each <code>station[i]</code>&nbsp;represents a gas station that is <code>station[i][0]</code> miles east of the starting position, and has <code>station[i][1]</code> liters of gas.</p>

<p>The car starts with an infinite tank of gas, which initially has&nbsp;<code>startFuel</code>&nbsp;liters of fuel in it.&nbsp; It uses 1 liter of gas per 1 mile that it drives.</p>

<p>When the car&nbsp;reaches a gas station, it may stop and refuel, transferring all the gas from the station into the car.</p>

<p>What is the least number of refueling stops the car must make in order to reach its destination?&nbsp; If it cannot reach the destination, return <code>-1</code>.</p>

<p>Note that if the car reaches a gas station with 0 fuel left, the car can still refuel there.&nbsp; If the car reaches the destination with 0 fuel left, it is still considered to have arrived.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>target = <span id="example-input-1-1">1</span>, startFuel = <span id="example-input-1-2">1</span>, stations = <span id="example-input-1-3">[]</span>
<strong>Output: </strong><span id="example-output-1">0</span>
<strong>Explanation: </strong>We can reach the target without refueling.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>target = <span id="example-input-2-1">100</span>, startFuel = <span id="example-input-2-2">1</span>, stations = <span id="example-input-2-3">[[10,100]]</span>
<strong>Output: </strong><span id="example-output-2">-1</span>
<strong>Explanation: </strong>We can&#39;t reach the target (or even the first gas station).
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>target = <span id="example-input-3-1">100</span>, startFuel = <span id="example-input-3-2">10</span>, stations = <span id="example-input-3-3">[[10,60],[20,30],[30,30],[60,40]]</span>
<strong>Output: </strong><span id="example-output-3">2</span>
<strong>Explanation: </strong>
We start with 10 liters of fuel.
We drive to position 10, expending 10 liters of fuel.  We refuel from 0 liters to 60 liters of gas.
Then, we drive from position 10 to position 60 (expending 50 liters of fuel),
and refuel from 10 liters to 50 liters of gas.  We then drive to and reach the target.
We made 2 refueling stops along the way, so we return 2.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= target, startFuel, stations[i][1] &lt;= 10^9</code></li>
	<li><code>0 &lt;= stations.length &lt;= 500</code></li>
	<li><code>0 &lt; stations[0][0] &lt; stations[1][0] &lt; ... &lt; stations[stations.length-1][0] &lt; target</code></li>
</ol>
</div>
</div>
</div>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Heap (heap)

## Companies
- Flipkart - 7 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Morgan Stanley - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Baidu - 2 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Dynamic Programming

**Intuition**

Let's determine `dp[i]`, the farthest location we can get to using `i` refueling stops.  This is motivated by the fact that we want the smallest `i` for which `dp[i] >= target`.

**Algorithm**

Let's update `dp` as we consider each station in order.  With no stations, clearly we can get a maximum distance of `startFuel` with `0` refueling stops.

Now let's look at the update step.  When adding a station `station[i] = (location, capacity)`, any time we could reach this station with `t` refueling stops, we can now reach `capacity` further with `t+1` refueling stops.

For example, if we could reach a distance of 15 with 1 refueling stop, and now we added a station at location 10 with 30 liters of fuel, then we could potentially reach a distance of 45 with 2 refueling stops.

<iframe src="https://leetcode.com/playground/ef5ZxTEk/shared" frameBorder="0" width="100%" height="310" name="ef5ZxTEk"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `stations`.

* Space Complexity:  $$O(N)$$, the space used by `dp`.
<br />
<br />


---
#### Approach 2: Heap

**Intuition**

When driving past a gas station, let's remember the amount of fuel it contained.  We don't need to decide yet whether to fuel up here or not - for example, there could be a bigger gas station up ahead that we would rather refuel at.

When we run out of fuel before reaching the next station, we'll retroactively fuel up: greedily choosing the largest gas stations first.

This is guaranteed to succeed because we drive the largest distance possible before each refueling stop, and therefore have the largest choice of gas stations to (retroactively) stop at.

**Algorithm**

`pq` ("priority queue") will be a max-heap of the capacity of each gas station we've driven by.  We'll also keep track of `tank`, our current fuel.

When we reach a station but have negative fuel (ie. we needed to have refueled at some point in the past), we will add the capacities of the largest gas stations we've driven by until the fuel is non-negative.

If at any point this process fails (that is, no more gas stations), then the task is impossible.

<iframe src="https://leetcode.com/playground/iD5V7Uuv/shared" frameBorder="0" width="100%" height="500" name="iD5V7Uuv"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the length of `stations`.

* Space Complexity:  $$O(N)$$, the space used by `pq`.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### DP O(N^2) and Priority Queue O(NlogN)
- Author: lee215
- Creation Date: Sun Jul 15 2018 11:06:15 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 18 2020 14:04:50 GMT+0800 (Singapore Standard Time)

<p>
## Approach 1: 1D DP, O(N^2)

`dp[t]` means the furthest distance that we can get with `t` times of refueling.

So for every station `s[i]`,
if the current distance `dp[t] >= s[i][0]`, we can refuel:
`dp[t + 1] = max(dp[t + 1], dp[t] + s[i][1])`

In the end, we\'ll return the first `t` with `dp[t] >= target`,
otherwise we\'ll return -1.


**C++:**
```
    int minRefuelStops(int target, int startFuel, vector<vector<int>> s) {
        long dp[501] = {startFuel};
        for (int i = 0; i < s.size(); ++i)
            for (int t = i; t >= 0 && dp[t] >= s[i][0]; --t)
                dp[t + 1] = max(dp[t + 1], dp[t] + s[i][1]);
        for (int t = 0; t <= s.size(); ++t)
            if (dp[t] >= target) return t;
        return -1;
    }
```

**Java:**
```
    public int minRefuelStops(int target, int startFuel, int[][] s) {
        long[] dp = new long[s.length + 1];
        dp[0] = startFuel;
        for (int i = 0; i < s.length; ++i)
            for (int t = i; t >= 0 && dp[t] >= s[i][0]; --t)
                dp[t + 1] = Math.max(dp[t + 1], dp[t] + s[i][1]);
        for (int t = 0; t <= s.length; ++t)
            if (dp[t] >= target) return t;
        return -1;
    }
```
**Python:**
```
    def minRefuelStops(self, target, startFuel, s):
        dp = [startFuel] + [0] * len(s)
        for i in range(len(s)):
            for t in range(i + 1)[::-1]:
                if dp[t] >= s[i][0]:
                    dp[t + 1] = max(dp[t + 1], dp[t] + s[i][1])
        for t, d in enumerate(dp):
            if d >= target: return t
        return -1
```


## Approach 2: Priority Queue, O(NlogN)

`i` is the index of next stops to refuel.
`res` is the times that we have refeuled.
`pq` is a priority queue that we store all available gas.


We initial `res = 0` and in every loop:
1. We add all reachable stop to priority queue.
2. We pop out the largest gas from `pq` and refeul once.
3. If we can\'t refuel, means that we can not go forward and return `-1`


**C++:**
```
    int minRefuelStops(int target, int cur, vector<vector<int>> s) {
        int i = 0, res;
        priority_queue<int>pq;
        for (res = 0; cur < target; res++) {
            while (i < s.size() && s[i][0] <= cur)
                pq.push(s[i++][1]);
            if (pq.empty()) return -1;
            cur += pq.top(), pq.pop();
        }
        return res;
    }
```

**Java:**
```
    public int minRefuelStops(int target, int cur, int[][] s) {
        Queue<Integer> pq = new PriorityQueue<>();
        int i = 0, res;
        for (res = 0; cur < target; res++) {
            while (i < s.length && s[i][0] <= cur)
                pq.offer(-s[i++][1]);
            if (pq.isEmpty()) return -1;
            cur += -pq.poll();
        }
        return res;
    }
```
**Python:**
```
    def minRefuelStops(self, target, cur, s):
        pq = []
        res = i = 0
        while cur < target:
            while i < len(s) and s[i][0] <= cur:
                heapq.heappush(pq, -s[i][1])
                i += 1
            if not pq: return -1
            cur += -heapq.heappop(pq)
            res += 1
        return res

```
Approach 2 inspired by @laiden.
</p>


### Simple Java Solution Using PriorityQueue O(nlogn)
- Author: jasonzzzzz
- Creation Date: Sun Jul 15 2018 11:14:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 06:10:11 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int minRefuelStops(int target, int startFuel, int[][] stations) {
        Queue<Integer> queue = new PriorityQueue<>();
        long dist = startFuel;
        int res = 0;
        int idx = 0;
        while (true) {
            while (idx < stations.length && stations[idx][0] <= dist) {
                queue.offer(-stations[idx][1]);
                idx++;
            }
            
            if (dist >= target) return res;
            if (queue.isEmpty()) return -1;
            dist += -queue.poll();
            res++;
        }
        
    }
}
```
</p>


### [Java] Simple Code - Greedy
- Author: chipbk10
- Creation Date: Fri May 17 2019 21:58:05 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 22 2020 02:38:37 GMT+0800 (Singapore Standard Time)

<p>
Let\'s start with an example:

- Start at `0`, with start fuel = `35`
- Stations = `[(10, 25), (20, 12), (30,21), (40, 5), (50,3)]`

`35.......25.......12.......21.......5........3...................` `(fuel)`
`|--------|--------|--------|--------|--------|------------------>` (`stations`)
`0.......10.......20.......30........40.......50..................` `(distance)`

Obviously, with **`0`** steps, the `max distance` we can reach is `35`.
The question now is with **`1`** steps, what is the `max distance` we can reach?

`35.......25.......12.......21.......5........3...................` `(fuel)`
`|--------|--------|--------|--------|--------|------------------>` (`stations`)
`0.......10.......20.......30...|....40.......50..................` `(distance)`
`...............................|.................................`
`...............................35................................` `(max distance can reach after 0 step)`

When we reach `35`, we pass by 3 stations `[10, 20, 30]`. It means we can possibly refuel at these stations.
- Refuel at `10`: `max distance = 10 + (35 - 10 + 25) = 35 + 25 = 60`
- Refuel at `20`: `max distance = 20 + (35 - 20 + 12) = 35 + 12 = 47`
- Refuel at `30`: `max distance = 30 + (35 - 30 + 21) = 35 + 21 = 56`

We notice that apparently the max distance **does not** depends on the station\'s **position**, but the station\'s **fuel**. 
Apparently, the maximum distance of `k+1` steps = maximum distance of `k` steps + maximum fuel of stations that the car has passed by (counting from the last station that makes the previous maximum distance)

`35......[25]......12.......21.......5........3...................` `(fuel)`
`|--------|--------|--------|--------|--------|------------------>` (`stations`)
`0.......[10]......20.......30...|...40.......50..................` `(distance)`
`...............................|....................|............`
`...............................35...................|............` `(max distance can reach after 0 step)`
`....................................................60...........` `(max distance can reach after 1 step)`

When we reach `60`, we reach more 2 stations `[40, 50]`, so if :

- Refuel at `20`: `max distance = 10 + (60-10) - (20-10) + (20-10) + 12 =  60 + 12 = 72`
- Refuel at `30`: `max distance = 10 + (60-10) - (30-10) + (30-10) + 21 = 60 + 21 = 81`
- Refuel at `40`: `max distance = 10 + (60-10) - (40-10) + (40-10) + 5 = 60 + 5 = 65`
- Refuel at `50`: `max distance = 10 + (60-10) - (50-10) + (50-10) + 3 = 60 + 3 = 63`

Our guess is correct, the max distance only depends on the amount of fuel at each station. And each time, we should choose the largest amount of fuel. We come up with the following code:

````
public int minRefuelStops(int target, int startFuel, int[][] stations) {
        if (startFuel >= target) return 0;
        Queue<Integer> queue = new PriorityQueue<>((a,b) -> b-a);
        int i = 0, n = stations.length, stops = 0, maxDistance = startFuel;
        while (maxDistance < target) {
            while (i < n && stations[i][0] <= maxDistance) {
                queue.offer(stations[i++][1]);
            }
            if (queue.isEmpty()) return -1;
            maxDistance += queue.poll();
            stops++;
        }
        return stops;
}
````

Not sure in the real interview if we should prove why we must choose the largest amount of fuel every time?
</p>


