---
title: "Building H2O"
weight: 1604
#id: "building-h2o"
---
## Description
<div class="description">
<p>There are two kinds of threads, <code>oxygen</code> and <code>hydrogen</code>. Your goal is to group these threads to form water molecules.&nbsp;There is a barrier where each thread has to&nbsp;wait until a complete molecule can be formed. Hydrogen and oxygen threads will be given <code>releaseHydrogen</code>&nbsp;and <code>releaseOxygen</code>&nbsp;methods respectively, which will allow them to pass the barrier. These threads should pass the barrier in groups of three, and they must be able to immediately bond with each other to form a water molecule.&nbsp;You must guarantee that all the threads from one molecule bond <em>before</em> any other threads from the next molecule do.</p>

<p>In other words:</p>

<ul>
	<li>If an oxygen thread arrives at the barrier when no hydrogen threads are present, it has to wait for two hydrogen threads.</li>
	<li>If a hydrogen thread arrives at the barrier when no other threads are present, it has to wait for an oxygen thread and another hydrogen thread.</li>
</ul>

<p>We don&rsquo;t have to worry about matching the threads up explicitly; that is, the threads do not necessarily know which other threads they are paired up with. The key is just that threads pass the barrier in complete sets; thus, if we examine the sequence of threads that bond and divide them into groups of three, each group should contain one oxygen and two hydrogen threads.</p>

<p>Write synchronization code for oxygen and hydrogen molecules that enforces these constraints.</p>

<div>
<p>&nbsp;</p>
</div>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;HOH&quot;</span>
<strong>Output: </strong><span id="example-output-1">&quot;HHO&quot;
<strong>Explanation:</strong> &quot;HOH&quot; and &quot;OHH&quot; are also valid answers.</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;OOHHHH&quot;</span>
<strong>Output: </strong><span id="example-output-1">&quot;HHOHHO&quot;
<strong>Explanation:</strong> &quot;HOHHHO&quot;, &quot;OHHHHO&quot;, &quot;HHOHOH&quot;, &quot;HOHHOH&quot;, &quot;OHHHOH&quot;, &quot;HHOOHH&quot;, &quot;HOHOHH&quot; and &quot;OHHOHH&quot; are also valid answers.</span>
</pre>
</div>
</div>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>Total length of input string will be 3<em>n</em>, where 1 &le;&nbsp;<em>n</em>&nbsp;&le; 20.</li>
	<li>Total number of <code>H</code> will be 2<em>n</em>&nbsp;in the input string.</li>
	<li>Total number of <code>O</code> will&nbsp;be <em>n</em>&nbsp;in the input&nbsp;string.</li>
</ul>

</div>

## Tags


## Companies
- LinkedIn - 3 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Very simple Java solution using 2 semaphores
- Author: jainrishabh
- Creation Date: Sun Jul 14 2019 00:04:38 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 03 2019 14:39:10 GMT+0800 (Singapore Standard Time)

<p>
```
import java.util.concurrent.*;
class H2O {
    
    Semaphore h, o;
    public H2O() {
        h = new Semaphore(2, true);
        o = new Semaphore(0, true);
    }

    public void hydrogen(Runnable releaseHydrogen) throws InterruptedException {
		h.acquire();
        releaseHydrogen.run();
        o.release();
        
    }

    public void oxygen(Runnable releaseOxygen) throws InterruptedException {
        o.acquire(2);
		releaseOxygen.run();
        h.release(2);
    }
}
```
</p>


### Python 3 Solution with semaphore and barrier
- Author: pzhzqt
- Creation Date: Tue Nov 05 2019 06:58:15 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 14 2020 08:28:49 GMT+0800 (Singapore Standard Time)

<p>
(If by any chance you saw the version where I corrected myself, ignore it, my head wasn\'t working. Below is the original version and it should be correct.)
```
from threading import Barrier, Semaphore
class H2O:
    def __init__(self):
        self.b = Barrier(3)
        self.h = Semaphore(2)
        self.o = Semaphore(1)

    def hydrogen(self, releaseHydrogen: \'Callable[[], None]\') -> None:
        self.h.acquire()
		self.b.wait()
        # releaseHydrogen() outputs "H". Do not change or remove this line.
        releaseHydrogen()
        self.h.release()

    def oxygen(self, releaseOxygen: \'Callable[[], None]\') -> None:
        self.o.acquire()
		self.b.wait()
        # releaseOxygen() outputs "O". Do not change or remove this line.
        releaseOxygen()
        self.o.release()
```
Since it takes only 1 oxygen each time, self.o can be a lock as well.
</p>


### [JAVA] 97% 100% solution by synchronized lock simple to understand
- Author: zhj1990530
- Creation Date: Tue Jul 23 2019 06:18:08 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 23 2019 06:18:08 GMT+0800 (Singapore Standard Time)

<p>
```
class H2O {
   
    private Object lock = new Object();
    private int counter =0;
    public H2O() {
        
    }

    public void hydrogen(Runnable releaseHydrogen) throws InterruptedException {
		
     synchronized (lock) {
            while(counter==2){
                lock.wait();
            }
            releaseHydrogen.run();
            counter++;
            lock.notifyAll();
      }
    }

    public void oxygen(Runnable releaseOxygen) throws InterruptedException {
        synchronized (lock) {
            while(counter!=2){
                lock.wait();
            }
            releaseOxygen.run();
            counter=0;
            lock.notifyAll();
      }
        
    }
}
```
</p>


