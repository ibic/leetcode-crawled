---
title: "Grid Illumination"
weight: 952
#id: "grid-illumination"
---
## Description
<div class="description">
<p>You are given a <code>grid</code> of size <code>N x N</code>, and each cell of this grid has a lamp that is initially <strong>turned off</strong>.</p>

<p>You are also given an array of lamp positions <code>lamps</code>, where <code>lamps[i] = [row<sub>i</sub>, col<sub>i</sub>]</code> indicates that the lamp at <code>grid[row<sub>i</sub>][col<sub>i</sub>]</code> is <strong>turned on</strong>. When a lamp is turned on, it <strong>illuminates its cell</strong> and <strong>all&nbsp;other cells</strong> in the same <strong>row, column, or diagonal</strong>.</p>

<p>Finally, you are given a query array <code>queries</code>, where&nbsp;<code>queries[i] = [row<sub>i</sub>, col<sub>i</sub>]</code>. For the <code>i<sup>th</sup></code>&nbsp;query, determine whether <code>grid[row<sub>i</sub>][col<sub>i</sub>]</code> is illuminated or not. After answering the <code>i<sup>th</sup></code> query,&nbsp;<strong>turn off</strong> the lamp at&nbsp;<code>grid[row<sub>i</sub>][col<sub>i</sub>]</code> and&nbsp;its <strong>8 adjacent lamps</strong> if they exist. A lamp is adjacent if its cell shares either&nbsp;a side or corner with&nbsp;<code>grid[row<sub>i</sub>][col<sub>i</sub>]</code>.</p>

<p>Return <em>an array of integers </em><code>ans</code><em>,</em><em>&nbsp;where </em><code>ans[i]</code><em> should be </em><code>1</code><em>&nbsp;if the lamp in the&nbsp;</em><code>i<sup>th</sup></code><em> query was illuminated, or&nbsp;</em><code>0</code><em>&nbsp;if the lamp was not.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/19/illu_1.jpg" style="width: 750px; height: 209px;" />
<pre>
<strong>Input:</strong> N = 5, lamps = [[0,0],[4,4]], queries = [[1,1],[1,0]]
<strong>Output:</strong> [1,0]
<strong>Explanation:</strong> We have the initial grid with all lamps turned off. In the above picture we see the grid after turning on the lamp at grid[0][0] then turning on the lamp at grid[4][4].
The 0<sup>th</sup>&nbsp;query asks if the lamp at grid[1][1] is illuminated or not (the blue square). It is illuminated, so set ans[0] = 1. Then, we turn off all lamps in the red square.
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/19/illu_step1.jpg" style="width: 500px; height: 218px;" />
The 1<sup>st</sup>&nbsp;query asks if the lamp at grid[1][0] is illuminated or not (the blue square). It is not illuminated, so set ans[1] = 1. Then, we turn off all lamps in the red rectangle.
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/19/illu_step2.jpg" style="width: 500px; height: 219px;" />
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> N = 5, lamps = [[0,0],[4,4]], queries = [[1,1],[1,1]]
<strong>Output:</strong> [1,1]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> N = 5, lamps = [[0,0],[0,4]], queries = [[0,4],[0,1],[1,4]]
<strong>Output:</strong> [1,1,0]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= N &lt;= 10<sup>9</sup></code></li>
	<li><code>0 &lt;= lamps.length &lt;= 20000</code></li>
	<li><code>lamps[i].length == 2</code></li>
	<li><code>0 &lt;= lamps[i][j] &lt; N</code></li>
	<li><code>0 &lt;= queries.length &lt;= 20000</code></li>
	<li><code>queries[i].length == 2</code></li>
	<li><code>0 &lt;= queries[i][j] &lt; N</code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Dropbox - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ with picture, similar to N-Queens
- Author: votrubac
- Creation Date: Sun Feb 24 2019 12:01:08 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 31 2019 08:29:39 GMT+0800 (Singapore Standard Time)

<p>
> *I did not look at the problem constraints and implemented a simple solution where everything is tracked in a grid. Obviously, I got MLE. Stepping on the same rake again and again; could have saved 20 minutes in the contest if I read the description carefully...*

We can use a similar technique as in [N-Queens](https://leetcode.com/problems/n-queens/). We track illuminated ```x``` and ```y``` coordinates, as well as two diagonals. Ascending (left-to-right) diagonal ```a_d``` is identified by the sum of the coordinates, and descending ```d_d``` - by their difference.

For example, for the light in position ```[3, 2]```, we would record ```++x[3]```, ```++y[2]```, ```++a_d[5]```, and ```++d_d[1]```. Since lamps can be removed later, we use hashmap (not a hashset) to track how many lamps are illuminating coordinates and diagonals. Also, we use another hashmap ```ls``` to track lamp positions.

![image](https://assets.leetcode.com/users/votrubac/image_1550986316.png)

Now, when we process the queries, we just need to check if any coordinate or diagonal is illuminated. If it is, we need to check if there are lamps needing to be turned off. When turning off a lamp, we remove its coordinates from ```ls``` and  decrement the count of the corresponding coordinates and diagonals.
> Note that if the queried position is not illuminated, we do not need to check for lamps.
> Note about duplicated lamps in the input. The OJ\'s solution ignores duplicates, so I updated this solution to match OJ. See the comment by [vksonagara1996](https://leetcode.com/vksonagara1996/) for the additional details.
```
vector<int> gridIllumination(int N, vector<vector<int>>& lamps, vector<vector<int>>& queries) {
  vector<int> res;
  unordered_map<int, int> x, y, a_d, d_d;
  unordered_map<int, unordered_set<int>> ls;
  for (auto l : lamps) {
    auto i = l[0], j = l[1];
    if (ls[i].insert(j).second) ++x[i], ++y[j], ++a_d[i + j], ++d_d[i - j];
  }
  for (auto q : queries) {
    auto i = q[0], j = q[1];
    if (x[i] || y[j] || a_d[i + j] || d_d[i - j]) {
      res.push_back(1);
      for (auto li = i - 1; li <= i + 1; ++li)
        for (auto lj = j - 1; lj <= j + 1; ++lj){
          if (ls[li].erase(lj)) {
            --x[li], --y[lj], --a_d[li + lj], --d_d[li - lj];
          }
      }
    }
    else res.push_back(0);
  }
  return res;
}
```
You can also use ```set<pair<int, int>>``` to track lamps. I found the runtime be similar to ```unordered_map<int, unordered_set<int>>```. Alternatively, you can define a hash function for the pair (STL does not provide one) and use ```unordered_set<pair<int, int>>```. This implementation is ~10% faster comparing to hashmap and set.
```
struct pairHash {
  size_t operator()(const pair<int, int> &x) const { return x.first ^ x.second; }
};
vector<int> gridIllumination(int N, vector<vector<int>>& lamps, vector<vector<int>>& queries) {
  vector<int> res;
  unordered_map<int, int> x, y, a_d, d_d;
  unordered_set<pair<int, int>, pairHash> ls;
  for (auto l : lamps) {
    auto i = l[0], j = l[1];
    if (ls.insert({ i, j }).second) ++x[i], ++y[j], ++a_d[i + j], ++d_d[i - j];
  }
  for (auto q : queries) {
    auto i = q[0], j = q[1];
    if (x[i] || y[j] || a_d[i + j] || d_d[i - j]) {
      res.push_back(1);
      for (int li = i - 1; li <= i + 1; ++li)
        for (int lj = j - 1; lj <= j + 1; ++lj) {
          if (ls.erase({li, lj})) {
            --x[li], --y[lj], --a_d[li + lj], --d_d[li - lj];
          }
        }
    }
    else res.push_back(0);
  }
  return res;
}
```
</p>


### Java Clean Code O(N) Time and O(N) Space Beats 100%
- Author: grkulkarni
- Creation Date: Sun Feb 24 2019 14:31:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 24 2019 14:31:36 GMT+0800 (Singapore Standard Time)

<p>
The basic idea is:
- The row, column or diagonal will remain illuminated  if there are > 0 lamps on that particular row, column or diagonal
- all the diagonals with slope= 1, can be represented by `x= y+c` i.e. they have x-y = constant
- all the diagonals with slope= -1, can be represented by `x= -y+c` i.e they have x+y = constant
- store the counts in separate maps
- When a lamp is turned off, the count of lamps in respective row, column or diagonal decreases by 1

```
class Solution {
    int[][] dirs = {{0,1}, {0,-1}, {1,0}, {-1,0}, {1,1}, {1,-1}, {-1,1}, {-1,-1}, {0,0}};
    
	public int[] gridIllumination(int N, int[][] lamps, int[][] queries) {
        Map<Integer, Integer> m1 = new HashMap();       // row number to count of lamps
        Map<Integer, Integer> m2 = new HashMap();       // col number to count of lamps
        Map<Integer, Integer> m3 = new HashMap();       // diagonal x-y to count of lamps
        Map<Integer, Integer> m4 = new HashMap();       // diagonal x+y to count of lamps
        Map<Integer, Boolean> m5 = new HashMap();       // whether lamp is  ON|OFF
        
        // increment counters while adding lamps
        for(int i=0; i<lamps.length; i++){
            int x = lamps[i][0];
            int y = lamps[i][1];
            m1.put(x, m1.getOrDefault(x, 0) + 1);
            m2.put(y, m2.getOrDefault(y, 0) + 1);
            m3.put(x-y, m3.getOrDefault(x-y, 0) + 1);
            m4.put(x+y, m4.getOrDefault(x+y, 0) + 1);
            m5.put(N*x + y, true);
        }

        int[] ans = new int[queries.length];
        // address queries
        for(int i=0; i<queries.length; i++){
            int x = queries[i][0];
            int y = queries[i][1];
            
            ans[i] = (m1.getOrDefault(x, 0) > 0 || m2.getOrDefault(y, 0) > 0 || m3.getOrDefault(x-y, 0) > 0 || m4.getOrDefault(x+y, 0) > 0) ? 1 : 0;            
            // switch off the lamps, if any
            for(int d=0; d<dirs.length; d++){
                int x1 = x + dirs[d][0];
                int y1 = y + dirs[d][1];
				if(m5.containsKey(N*x1 + y1) && m5.get(N*x1 + y1)){
                    // the lamp is on, turn it off, so decrement the count of the lamps
                    m1.put(x1, m1.getOrDefault(x1, 1) - 1);
                    m2.put(y1, m2.getOrDefault(y1, 1) - 1);
                    m3.put(x1 - y1, m3.getOrDefault(x1 - y1, 1) - 1);
                    m4.put(x1 + y1, m4.getOrDefault(x1 + y1, 1) - 1);
                    m5.put(N*x1+y1, false);
                }
            }
        }
        
        return ans;
    }
}
```


##### Update:
Following redundant code removed, Thanks to [j46](https://leetcode.com/j46/) for pointing it out!
```

	// following condition updated
	if(isValid(x1, y1) && m5.containsKey(N*x1 + y1) && m5.get(N*x1 + y1)){
	// removed the function for checking if the co-ordinates are valid, it is taken care by the .containsKey
    private boolean isValid(int x, int y){
        return (x >= 0 && x < MAX && y>=0 && y< MAX);    // MAX was class variable equal to N
    }
```

The exact complexity in terms of number of lamps, `L` and number of queries, `Q` is as follows:
**Time Complexity:** `O(L+Q)`
**Space Complexity:** `O(L)`
</p>


### Screencast of LeetCode Weekly Contest 125
- Author: cuiaoxiang
- Creation Date: Sun Feb 24 2019 13:16:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 24 2019 13:16:04 GMT+0800 (Singapore Standard Time)

<p>
https://www.youtube.com/watch?v=EJjB2WI7EdM&feature=youtu.be
</p>


