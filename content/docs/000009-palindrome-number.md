---
title: "Palindrome Number"
weight: 9
#id: "palindrome-number"
---
## Description
<div class="description">
<p>Determine whether an integer is a palindrome. An integer&nbsp;is&nbsp;a&nbsp;palindrome when it&nbsp;reads the same backward as forward.</p>

<p><strong>Follow up:</strong> Could you solve&nbsp;it without converting the integer to a string?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> x = 121
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> x = -121
<strong>Output:</strong> false
<strong>Explanation:</strong> From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> x = 10
<strong>Output:</strong> false
<strong>Explanation:</strong> Reads 01 from right to left. Therefore it is not a palindrome.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> x = -101
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>-2<sup>31</sup>&nbsp;&lt;= x &lt;= 2<sup>31</sup>&nbsp;- 1</code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Google - 8 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Amazon - 5 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Facebook - 7 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: false)
- JPMorgan - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Revert half of the number

**Intuition**

The first idea that comes to mind is to convert the number into string, and check if the string is a palindrome, but
this would require extra non-constant space for creating the string which is not allowed by the problem description.

Second idea would be reverting the number itself, and then compare the number with original number,
if they are the same, then the number is a palindrome. However, if the reversed number is larger than $$\text{int.MAX}$$,
we will hit integer overflow problem.

Following the thoughts based on the second idea, to avoid the overflow issue of the reverted number, what if we only
revert half of the $$\text{int}$$ number? After all, the reverse of the last half of the palindrome should be the same as the
first half of the number, if the number is a palindrome.

For example, if the input is `1221`, if we can revert the last part of the number "12**21**" from "**21**" to "**12**",
and compare it with the first half of the number "12", since 12 is the same as 12, we know that the number is a palindrome.

Let's see how we could translate this idea into an algorithm.

**Algorithm**

First of all we should take care of some edge cases. All negative numbers are not palindrome, for example: -123 is
not a palindrome since the '-' does not equal to '3'. So we can return false for all negative numbers.

Now let's think about how to revert the last half of the number. For number `1221`, if we do `1221 % 10`, we get the
last digit `1`, to get the second to the last digit, we need to remove the last digit from `1221`, we could do so by
dividing it by 10, `1221 / 10 = 122`. Then we can get the last digit again by doing a modulus by 10, `122 % 10 = 2`, and if we multiply the last digit by 10 and add the second last digit, `1 * 10 + 2 = 12`, it gives us the reverted number we want. Continuing this process would give us the reverted number with more digits.

Now the question is, how do we know that we've reached the half of the number?

Since we divided the number by 10, and multiplied the reversed number by 10, when the original number is less than the
reversed number, it means we've processed half of the number digits.

<iframe src="https://leetcode.com/playground/A2cW8TnM/shared" frameBorder="0" width="100%" height="446" name="A2cW8TnM"></iframe>

**Complexity Analysis**

* Time complexity : $$O(\log_{10}(n))$$.
We divided the input by 10 for every iteration, so the time complexity is $$O(\log_{10}(n))$$

* Space complexity : $$O(1)$$.

## Accepted Submission (python3)
```python3
class Solution:
    def isPalindromeString(self, x: int) -> bool:
        s = str(x)
        le = len(s)
        for i in range(le // 2):
            if (s[i] != s[le - 1 - i]):
                return False
        return True

    def isPalindrome(self, x: int) -> bool:
        if (x < 0):
            return False
        stack = []
        while (x > 0):
            digit = x % 10
            stack.append(digit)
            x = x // 10
        le = len(stack)
        for i in range(le // 2):
            if (stack[i] != stack[le - 1 - i]):
                return False
        return True

```

## Top Discussions
### 9-line accepted Java code, without the need of handling overflow
- Author: cbmbbz
- Creation Date: Thu Jan 29 2015 10:45:50 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 19:08:43 GMT+0800 (Singapore Standard Time)

<p>
compare half of the digits in x, so don't need to deal with overflow.

    public boolean isPalindrome(int x) {
        if (x<0 || (x!=0 && x%10==0)) return false;
        int rev = 0;
        while (x>rev){
        	rev = rev*10 + x%10;
        	x = x/10;
        }
        return (x==rev || x==rev/10);
    }
</p>


### An easy c++ 8 lines code (only reversing till half and then compare)
- Author: gaurav5
- Creation Date: Mon Apr 27 2015 08:43:06 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 08:56:31 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        bool isPalindrome(int x) {
            if(x<0|| (x!=0 &&x%10==0)) return false;
            int sum=0;
            while(x>sum)
            {
                sum = sum*10+x%10;
                x = x/10;
            }
            return (x==sum)||(x==sum/10);
        }
    };
</p>


### This problem is meanless
- Author: CandyRobbery
- Creation Date: Sun Dec 13 2015 09:43:05 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 11:47:11 GMT+0800 (Singapore Standard Time)

<p>
- Impossible to solve without extra space. Always need space for constants, variables or whatever. Recursion calls will take space for call stack.

- If you are talking about constant space, then even declaring a string / stack will take constant space. (In fact at most (log(10, INT_MAX) * sizeof char), which is no worse than declaring an integer or more). Actually, even recursion will take constant space.
</p>


