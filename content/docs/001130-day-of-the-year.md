---
title: "Day of the Year"
weight: 1130
#id: "day-of-the-year"
---
## Description
<div class="description">
<p>Given a string <code>date</code> representing a <a href="https://en.wikipedia.org/wiki/Gregorian_calendar" target="_blank">Gregorian&nbsp;calendar</a> date formatted as <code>YYYY-MM-DD</code>, return the day number of the year.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> date = &quot;2019-01-09&quot;
<strong>Output:</strong> 9
<strong>Explanation:</strong> Given date is the 9th day of the year in 2019.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> date = &quot;2019-02-10&quot;
<strong>Output:</strong> 41
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> date = &quot;2003-03-01&quot;
<strong>Output:</strong> 60
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> date = &quot;2004-03-01&quot;
<strong>Output:</strong> 61
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>date.length == 10</code></li>
	<li><code>date[4] == date[7] == &#39;-&#39;</code>, and all other <code>date[i]</code>&#39;s are digits</li>
	<li><code>date</code> represents a calendar date between Jan 1st, 1900 and Dec 31, 2019.</li>
</ul>
</div>

## Tags
- Math (math)

## Companies
- zscaler - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++, Number of Days in a Month
- Author: votrubac
- Creation Date: Sun Aug 11 2019 12:27:00 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 12 2019 07:53:58 GMT+0800 (Singapore Standard Time)

<p>
We need to sum the number of days in previous months, so we build on top of [1118. Number of Days in a Month](https://leetcode.com/problems/number-of-days-in-a-month/).

Note that we need to add one day for February for a leap year.
```
int days[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
int dayOfYear(string dt) {
  int y = stoi(dt.substr(0, 4)), m = stoi(dt.substr(5, 2)), d = stoi(dt.substr(8));
  if (m > 2 && y % 4 == 0 && (y % 100 != 0 || y % 400 == 0)) ++d; 
  while (--m > 0) d += days[m - 1];
  return d;
}
```
</p>


### Simple Java Solution [easy to understand] #GregorianCalendar
- Author: JatinYadav96
- Creation Date: Sun Aug 11 2019 12:12:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 11 2019 14:25:31 GMT+0800 (Singapore Standard Time)

<p>
If it is leap year then simple add 1 to feb
Rest is easy to understand
Java has inBuilt split function no need to use substring for that :P
```
 public int dayOfYear(String S) {
        String[] s = S.split("-");
        int year = Integer.parseInt(s[0]);
        int month = Integer.parseInt(s[1]);
        int date = Integer.parseInt(s[2]);
        boolean isLeap = checkYear(year);
        int noOfDays = 0;
        int[] days = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        for (int i = 0; i < month - 1; i++) {
            if (isLeap && i == 1) {
                noOfDays += days[i] + 1;
                continue;
            }
            noOfDays += days[i];
        }
        return noOfDays + date;
    }
``````
And to check leap Year

```

    boolean checkYear(int year) {
        if (year % 400 == 0)
            return true;
        if (year % 100 == 0)
            return false;
        if (year % 4 == 0)
            return true;
        return false;
    }
```

we can also check leap year with help of GregorianCalendar inBuilt class in java 
```
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
        boolean isLeap = gregorianCalendar.isLeapYear(year);
```

</p>


### [Python] Cheat
- Author: lee215
- Creation Date: Sun Aug 11 2019 12:05:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 11 2019 12:05:14 GMT+0800 (Singapore Standard Time)

<p>
**Python:**
```python
    def ordinalOfDate(self, date):
        Y, M, D = map(int, date.split(\'-\'))
        return int((datetime.datetime(Y, M, D) - datetime.datetime(Y, 1, 1)).days + 1)
```

</p>


