---
title: "Stepping Numbers"
weight: 1056
#id: "stepping-numbers"
---
## Description
<div class="description">
<p>A <em>Stepping Number</em> is&nbsp;an integer&nbsp;such that&nbsp;all of its adjacent digits have an absolute difference of exactly <code>1</code>. For example, <code>321</code> is a Stepping Number while <code>421</code> is not.</p>

<p>Given two integers <code>low</code> and <code>high</code>, find and return a <strong>sorted</strong> list of all the Stepping Numbers in the range <code>[low, high]</code>&nbsp;inclusive.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> low = 0, high = 21
<strong>Output:</strong> [0,1,2,3,4,5,6,7,8,9,10,12,21]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= low &lt;= high &lt;= 2 * 10^9</code></li>
</ul>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Epic Systems - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple BFS
- Author: Poorvank
- Creation Date: Sun Oct 06 2019 00:01:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 00:46:44 GMT+0800 (Singapore Standard Time)

<p>
BFS:
start node = 0
From 0, we can move to 1 2 3 4 5 6 7 8 9 [ Add these to queue. ]
now from 1, we can move to 12 and 10
from 2, we can move to 23 and 21
from 3,, we can move to 34 and 32
.
.
and so on

```

   public ArrayList<Integer> countSteppingNumbers(int low, int high) {
        ArrayList<Integer> res = new ArrayList<>();
        if (low > high) return res;

        Queue<Long> queue = new LinkedList<>();
        for (long i = 1; i <= 9; i++) queue.add(i);

        if (low == 0) res.add(0);
        while (!queue.isEmpty()) {
            long p = queue.poll();
            if (p < high) {
                long last = p % 10;
                if (last > 0) queue.add(p * 10 + last - 1);
                if (last < 9) queue.add(p * 10 + last + 1);
            }
            if (p >= low && p <= high) {
                res.add((int) p);
            }
        }
        return res;
    }
```
</p>


### [Python3] Queue and DFS solutions
- Author: cenkay
- Creation Date: Sun Oct 06 2019 00:04:24 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 08 2019 18:25:05 GMT+0800 (Singapore Standard Time)

<p>
* DFS
```
class Solution:
    def countSteppingNumbers(self, low: int, high: int) -> List[int]:
        def dfs(n):
            if n > high: 
                return 
            if n >= low:
                q.add(n)
            d = n % 10
            if d == 0:
                dfs(n * 10 + 1)
            elif d == 9:
                dfs(n * 10 + 8)
            else:
                dfs(n * 10 + d + 1) 
                dfs(n * 10 + d - 1)
        q = set()
        for i in range(10):
            dfs(i)
        return sorted(q)
```
* Queue
```
class Solution:
    def countSteppingNumbers(self, low: int, high: int) -> List[int]:
        q1, q2 = set(), collections.deque(range(10))
        while q2:
            n = q2.popleft()
            if low <= n <= high:
                q1.add(n)
            if n < high:
                d = n % 10
                if d == 0:
                    q2.append(n * 10 + 1)
                elif d == 9:
                    q2.append(n * 10 + 8)
                else:
                    q2.append(n * 10 + d + 1)
                    q2.append(n * 10 + d - 1)
        return sorted(q1)
```
</p>


### Java DFS solution (Read this if you got TLE)
- Author: Sun_WuKong
- Creation Date: Sun Oct 06 2019 00:01:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 00:12:37 GMT+0800 (Singapore Standard Time)

<p>
Start with an 1-digit number from 0 to 9. If they are in range, add them to list.
For number from 1 to 9, append a new digit to the end that is 1 away from current. For example, `1` will be `12` and `10`, same for others. If new numbers are in range, add them to list.
...
Recursively, find the last digit of a number, append a new digit that is 1 away from current last digit. For example, `8767` will become `87678` and `87676`. Add those numbers if they are in range. If they are already `> high`, stop the recursion.

**Note:** We need long here because we want to stop when `cur > high`. If we don\'t use long, **overflow** will cause `cur` to be **negative** which mean less than `high`. Then instead of stop, it will continue with garbage data, which cause **TLE**

```
class Solution {
    public List<Integer> countSteppingNumbers(int low, int high) {
        List<Integer> list = new ArrayList();
        
        for (long i = 0; i <= 9; i++) {
            dfs(low, high, i, list);
        }
        Collections.sort(list);
        
        return list;
    }
    
    private void dfs(int low, int high, long cur, List<Integer> list) {
        if (cur >= low && cur <= high) list.add((int)cur);
        if (cur == 0 || cur > high) return;
        
        long last = cur%10, inc = cur*10 + last + 1, dec = cur*10 + last - 1;
        
        if (last == 0) dfs(low, high, inc, list);
        else if (last == 9) dfs(low, high, dec, list);
        else {
            dfs(low, high, inc, list);
            dfs(low, high, dec, list);
        }
    }
}
```
</p>


