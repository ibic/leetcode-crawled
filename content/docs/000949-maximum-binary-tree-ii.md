---
title: "Maximum Binary Tree II"
weight: 949
#id: "maximum-binary-tree-ii"
---
## Description
<div class="description">
<p>We are given the <code>root</code>&nbsp;node of a <em>maximum tree:</em> a tree where every node has a value greater than any other value in its subtree.</p>

<p>Just as in the <a href="https://leetcode.com/problems/maximum-binary-tree/">previous problem</a>, the given tree&nbsp;was constructed from an list&nbsp;<code>A</code>&nbsp;(<code>root = Construct(A)</code>) recursively with the following&nbsp;<code>Construct(A)</code> routine:</p>

<ul>
	<li>If <code>A</code> is empty, return <code>null</code>.</li>
	<li>Otherwise, let <code>A[i]</code> be the largest element of <code>A</code>.&nbsp; Create a <code>root</code> node with value <code>A[i]</code>.</li>
	<li>The left child of <code>root</code> will be <code>Construct([A[0], A[1], ..., A[i-1]])</code></li>
	<li>The right child of <code>root</code>&nbsp;will be <code>Construct([A[i+1], A[i+2], ..., A[A.length - 1]])</code></li>
	<li>Return <code>root</code>.</li>
</ul>

<p>Note that we were not given A directly, only a root node <code>root = Construct(A)</code>.</p>

<p>Suppose <code>B</code> is a copy of <code>A</code> with the value <code>val</code> appended to it.&nbsp; It is guaranteed that <code>B</code> has unique values.</p>

<p>Return <code>Construct(B)</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/02/21/maximum-binary-tree-1-1.png" style="width: 159px; height: 160px;" /><img alt="" src="https://assets.leetcode.com/uploads/2019/02/21/maximum-binary-tree-1-2.png" style="width: 169px; height: 160px;" /></strong></p>

<pre>
<strong>Input: </strong>root = <span id="example-input-1-1">[4,1,3,null,null,2]</span>, val = <span id="example-input-1-2">5</span>
<strong>Output: </strong><span id="example-output-1">[5,4,null,1,3,null,null,2]
<strong>Explanation:</strong> A = </span><span>[1,4,2,3], B = </span><span>[1,4,2,3,5]</span>
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/02/21/maximum-binary-tree-2-1.png" style="width: 180px; height: 160px;" /><img alt="" src="https://assets.leetcode.com/uploads/2019/02/21/maximum-binary-tree-2-2.png" style="width: 214px; height: 160px;" /></strong></p>

<pre>
<strong>Input: </strong>root = <span id="example-input-2-1">[5,2,4,null,1]</span>, val = <span id="example-input-2-2">3</span>
<strong>Output: </strong><span id="example-output-2">[5,2,4,null,1,null,3]
</span><span id="example-output-1"><strong>Explanation:</strong> A = </span><span>[2,1,5,4], B = </span><span>[2,1,5,4,3]</span>
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/02/21/maximum-binary-tree-3-1.png" style="width: 180px; height: 160px;" /><img alt="" src="https://assets.leetcode.com/uploads/2019/02/21/maximum-binary-tree-3-2.png" style="width: 201px; height: 160px;" /></strong></p>

<pre>
<strong>Input: </strong>root = <span id="example-input-3-1">[5,2,3,null,1]</span>, val = <span id="example-input-3-2">4</span>
<strong>Output: </strong><span id="example-output-3">[5,2,4,null,1,3]
</span><span id="example-output-1"><strong>Explanation:</strong> A = </span><span>[2,1,5,3], B = </span><span>[2,1,5,3,4]</span>
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= B.length &lt;= 100</code></li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Recursion and Iteration
- Author: lee215
- Creation Date: Sun Feb 24 2019 12:06:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 24 2019 12:06:16 GMT+0800 (Singapore Standard Time)

<p>
## **Solution 1: Recursion**
If root.val > val, recusion on the right.
Else, put right subtree on the left of new node `TreeNode(val)`

**Time Complexity**:
O(N) time,
O(N) recursion space.

<br>

**Java:**
```
    public TreeNode insertIntoMaxTree(TreeNode root, int val) {
        if (root != null && root.val > val) {
            root.right = insertIntoMaxTree(root.right, val);
            return root;
        }
        TreeNode node = new TreeNode(val);
        node.left = root;
        return node;
    }
```

**C++:**
```
    TreeNode* insertIntoMaxTree(TreeNode* root, int val) {
        if (root && root->val > val) {
            root->right = insertIntoMaxTree(root->right, val);
            return root;
        }
        TreeNode* node = new TreeNode(val);
        node->left = root;
        return node;        
    }
```

**Python:**
```
    def insertIntoMaxTree(self, root, val):
        if root and root.val > val:
            root.right = self.insertIntoMaxTree(root.right, val)
            return root
        node = TreeNode(val)
        node.left = root
        return node
```

<br>

## **Solution 2: Iteration**

Search on the right, find the node that `cur.val > val > cur.right.val`
Then create new node `TreeNode(val)`,
put old `cur.right` as `node.left`,
put `node` as new `cur.right`.


**Time Complexity**:
`O(N)` time,
`O(1)` space


**Java:**
```
    public TreeNode insertIntoMaxTree(TreeNode root, int val) {
        TreeNode node = new TreeNode(val), cur = root;
        if (root.val < val) {
            node.left = root;
            return node;
        }
        while (cur.right != null && cur.right.val > val) {
            cur = cur.right;
        }
        node.left = cur.right;
        cur.right = node;
        return root;
    }
```

**C++:**
```
    TreeNode* insertIntoMaxTree(TreeNode* root, int val) {
        TreeNode* node = new TreeNode(val), *cur = root;
        if (root->val < val) {
            node->left = root;
            return node;
        }
        while (cur->right && cur->right->val > val) {
            cur = cur->right;
        }
        node->left = cur->right;
        cur->right = node;
        return root;
    }
```

**Python:**
```
    def insertIntoMaxTree(self, root, val):
        pre,cur = None, root
        while cur and cur.val > val:
            pre, cur = cur, cur.right
        node = TreeNode(val)
        node.left = cur
        if pre: pre.right = node
        return root if root.val > val else node
```

</p>


### How many people can't understand what the question means?
- Author: SmileChen
- Creation Date: Sun Feb 24 2019 17:34:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 09 2019 09:27:51 GMT+0800 (Singapore Standard Time)

<p>
```
We are given the root node of a maximum tree: a tree where every node has a value greater than any other value in its subtree.

Just as in the previous problem, the given tree was constructed from an list A (root = Construct(A)) recursively with the following Construct(A) routine:

If A is empty, return null.
Otherwise, let A[i] be the largest element of A.  Create a root node with value A[i].
The left child of root will be Construct([A[0], A[1], ..., A[i-1]])
The right child of root will be Construct([A[i+1], A[i+2], ..., A[A.length - 1]])
Return root.
Note that we were not given A directly, only a root node root = Construct(A).

Suppose B is a copy of A with the value val appended to it.  It is guaranteed that B has unique values.

Return Construct(B).
```
Example 1:

![image](https://assets.leetcode.com/users/smilechen/image_1551000961.png)


Input: root = [4,1,3,null,null,2], val = 5
Output: [5,4,null,1,3,null,null,2]
Explanation: A = [1,4,2,3], B = [1,4,2,3,5]
Example 2:
![image](https://assets.leetcode.com/users/smilechen/image_1551000971.png)


Input: root = [5,2,4,null,1], val = 3
Output: [5,2,4,null,1,null,3]
Explanation: A = [2,1,5,4], B = [2,1,5,4,3]
Example 3:
![image](https://assets.leetcode.com/users/smilechen/image_1551000980.png)


Input: root = [5,2,3,null,1], val = 4
Output: [5,2,4,null,1,3]
Explanation: A = [2,1,5,3], B = [2,1,5,3,4]
 

Note:

1 <= B.length <= 100

```
The question is so confused but the solution is so easy.

class Solution {
    public TreeNode insertIntoMaxTree(TreeNode root, int val) {
        if(root==null)return new TreeNode(val);
        if(root.val<val){
            TreeNode node = new TreeNode(val);
            node.left=root;
            return node;
        }
        root.right=insertIntoMaxTree(root.right,val);
        return root;
    }
}
```
```
\u611F\u8C22\u6240\u6709\u63D0\u4F9B\u4FE1\u606F\u7684\u540C\u5B66\uFF0C\u6211\u89E3\u91CA\u4E0B\u4E3A\u4EC0\u4E48\u5F53val<root.val\u65F6\uFF0C\u662F\u5F80\u53F3\u8FB9\u52A0\u7684\u3002
1. the given tree was constructed from an list A (root = Construct(A)). So, List<Integer> A = new ArrayList();
2. Suppose B is a copy of A with the value val appended to it. So, B = new ArrayList(A) and B.add(val);
3. The left child of root will be Construct([A[0], A[1], ..., A[i-1]]), 
The right child of root will be Construct([A[i+1], A[i+2], ..., A[A.length - 1]]).
in this case A represent B, B[B.length-1] = val, So.
4. If val is the largest, i = B.length-1, the root node\'s value is val, i=0 to i-1 are in the left child of root. 
This explains why when val > root.val, root should be the left child of new node with value val.
5. Else val is not the largest, the new node with value val is always the right child of root. 
```

</p>


### Java clean recursive solution
- Author: dancingPizza
- Creation Date: Sun Feb 24 2019 12:00:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 24 2019 12:00:58 GMT+0800 (Singapore Standard Time)

<p>
The idea is to insert node to the right parent or right sub-tree of current node. Using recursion can achieve this:
If inserted value is greater than current node, the inserted goes to right parent
If inserted value is smaller than current node, we recursively re-cauculate right subtree
```
class Solution {
    public TreeNode insertIntoMaxTree(TreeNode root, int v) {
        if(root==null)return new TreeNode(v);
        if(root.val<v){
            TreeNode node = new TreeNode(v);
            node.left=root;
            return node;
        }
        root.right=insertIntoMaxTree(root.right,v);
        return root;
    }
}
```
</p>


