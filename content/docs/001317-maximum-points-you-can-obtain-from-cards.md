---
title: "Maximum Points You Can Obtain from Cards"
weight: 1317
#id: "maximum-points-you-can-obtain-from-cards"
---
## Description
<div class="description">
<p>There are several cards&nbsp;<strong>arranged in a row</strong>, and each card has an associated number of points&nbsp;The points are given in the integer array&nbsp;<code>cardPoints</code>.</p>

<p>In one step, you can take one card from the beginning or from the end of the row. You have to take exactly <code>k</code> cards.</p>

<p>Your score is the sum of the points of the cards you have taken.</p>

<p>Given the integer array <code>cardPoints</code> and the integer <code>k</code>, return the <em>maximum score</em> you can obtain.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> cardPoints = [1,2,3,4,5,6,1], k = 3
<strong>Output:</strong> 12
<strong>Explanation:</strong> After the first step, your score will always be 1. However, choosing the rightmost card first will maximize your total score. The optimal strategy is to take the three cards on the right, giving a final score of 1 + 6 + 5 = 12.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> cardPoints = [2,2,2], k = 2
<strong>Output:</strong> 4
<strong>Explanation:</strong> Regardless of which two cards you take, your score will always be 4.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> cardPoints = [9,7,7,9,7,7,9], k = 7
<strong>Output:</strong> 55
<strong>Explanation:</strong> You have to take all the cards. Your score is the sum of points of all cards.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> cardPoints = [1,1000,1], k = 1
<strong>Output:</strong> 1
<strong>Explanation:</strong> You cannot take the card in the middle. Your best score is 1. 
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> cardPoints = [1,79,80,1,1,1,200,1], k = 3
<strong>Output:</strong> 202
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= cardPoints.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= cardPoints[i] &lt;= 10^4</code></li>
	<li><code>1 &lt;= k &lt;= cardPoints.length</code></li>
</ul>

</div>

## Tags
- Array (array)
- Dynamic Programming (dynamic-programming)
- Sliding Window (sliding-window)

## Companies
- Google - 18 (taggedByAdmin: false)
- Flipkart - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python3] Easy Sliding Window O(n): Find minimum subarray
- Author: localhostghost
- Creation Date: Sun Apr 26 2020 12:05:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 26 2020 23:50:00 GMT+0800 (Singapore Standard Time)

<p>
Problem Translation: Find the smallest subarray sum of length `len(cardPoints) - k`
```
class Solution:
    def maxScore(self, cardPoints: List[int], k: int) -> int:
        size = len(cardPoints) - k
        minSubArraySum = float(\'inf\')
        j = curr = 0
        
        for i, v in enumerate(cardPoints):
            curr += v
            if i - j + 1 > size:
                curr -= cardPoints[j]
                j += 1
            if i - j + 1 == size:    
                minSubArraySum = min(minSubArraySum, curr)
				
        return sum(cardPoints) - minSubArraySum
```
DP top down solution timed out for me.
```
class Solution:
    def maxScore(self, cardPoints: List[int], k: int) -> int:
        if k == len(cardPoints):
            return sum(cardPoints)
        @lru_cache(None)
        def dfs(i, j, k, res = 0):
            if k == 0:
                return 0
            res = max(cardPoints[i] + dfs(i + 1, j, k - 1), cardPoints[j] + dfs(i, j - 1, k - 1))
            return res   
			
        return dfs(0, len(cardPoints) - 1, k)
```
Please upvote if you think this was useful.
</p>


### ⭐️  [ Javascript / Python3 / C++ ] 👉  brute-force sliding window
- Author: claytonjwong
- Creation Date: Sun Apr 26 2020 12:23:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 15 2020 07:28:37 GMT+0800 (Singapore Standard Time)

<p>
**Synopsis:**

Brute-force check all possiblities using a sliding window to find the maximum sum of length `K`.  Initially the sliding window\'s `sum` is set to the accumulated sum of the last `K` digits of `A`.  Then as the window "slides", we increment the `sum` by `A[i]` and decrement the `sum` by `A[j]`.

*Example 1:*

**Input:** cardPoints = `[1,2,3,4,5,6,1]`, k = `3`
**Output:** 12

As the window "slides":

* Each `i`-th element of `A` highlighted in green is included in the sliding window\'s`sum`
* Each `j`-th element of `A` in red text  is removed from the sliding window\'s `sum`

![image](https://assets.leetcode.com/users/claytonjwong/image_1587913136.png)

**Note:** An astute observer will notice there\'s a handful of valid exit conditions for iteratively sliding the window by `K`.  For example, any of the below conditions are sufficient to determine when the window has slid by `K`, however, this choice is arbitrary and irrelevant, as long as the window slides by `K`, we\'re good to go! \uD83D\uDC4D
* `i < K`
* `j < N`
* `K--`

---

*Javascript*
```
let maxScore = (A, K) => {
    let N = A.length,
        i = 0,
        j = N - K;
    let sum = A.slice(j, N).reduce((a, b) => a + b),
        max = sum;
    while (K--) max = Math.max(max, sum += A[i++] - A[j++]);  // slide window by K \uD83D\uDC49
    return max;
}
```

*Python3*
```
class Solution:
    def maxScore(self, A: List[int], K: int) -> int:
        N = len(A)
        i = 0
        j = N - K
        total = sum(A[j:])
        best = total
        for _ in range(K):  # slide window by K \uD83D\uDC49
            total += A[i] - A[j]
            best = max(best, total)
            i += 1
            j += 1
        return best
```

*C++*
```
class Solution {
public:
    using VI = vector<int>;
    int maxScore(VI& A, int K) {
        int N = A.size(),
            i = 0,
            j = N - K;
        auto sum = accumulate(A.begin() + j, A.end(), 0),
             max = sum;
        while (K--) max = std::max(max, sum += A[i++] - A[j++]);  // slide window by K \uD83D\uDC49
        return max;
    }
};
```
</p>


### [Simple, Clean] Intuitive Explanation with Visualization
- Author: interviewrecipes
- Creation Date: Sun Apr 26 2020 12:11:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 08 2020 13:24:17 GMT+0800 (Singapore Standard Time)

<p>
**Key idea**: You can\u2019t choose 2nd element from the beginning unless you have chosen the first one.  Similarly, you can\u2019t choose 2nd element from last unless you have chosen the last one. 

So now just try all possible combinations. Choose 0 from the beginning and K from the last, 1 from front and K-1 from last and so on until K from beginning and 0 from behind. Maximum out of all those combinations is the answer.

![image](https://assets.leetcode.com/users/images/aac454f9-15ca-4617-8877-f429fdf3ad2b_1592777668.0916321.png)

To make it faster to find sum of first `i` cards, store the cumulative sum from the beginning to current index `i` in an array. In the similar way, store cumulative sums from the back in separate array.

**How to solve exactly?**
1. Find cumulative sum from beginning to the current index.
2. Find cumulative sum from behind till the current index.
3. If you choose `i` elements from front, you will need to choose `k-i` elements from behind.
Sum of first i elements = `cumulativeSumFromFront[i]`, 
Sum of last (k-i) elements = `cumulativeSumFromBehind[K-i]`.
So points obtained when choosing `i` elements from the front = `cumulativeSumFromFront[i] + cumulativeSumFromBehind[K-i]`
4. Repeat Step 3 for all `i` ranging from `0` to `K`.
5. Return the maximum value of points reached.

Hope it is easy to understand. 
Let me know if there is something unclear and I can fix it.

Otherwise, please upvote if you like the solution, it would be encouraging.

Python (Python can be amazing at reducing lines of code)
```
class Solution(object):
    def maxScore(self, cardPoints, k):
        print \'cardPoints:\', cardPoints
        print \'k:\', k
        frontSum, backSum = [0], [0]
        for n in cardPoints:
            frontSum.append(frontSum[-1]+n)
            print \'frontSum:\', frontSum
        for n in cardPoints[::-1]:
            backSum.append(backSum[-1]+n)
            print \'backSum:\', backSum
        allCombinations = [frontSum[i]+backSum[k-i] for i in range(k+1)]
        print \'allCombinations:\', allCombinations
        return max(allCombinations)

\'\'\'
I intentionally kept the print statements for easier understanding. Here is the output:
cardPoints: [1, 2, 3, 4, 5, 6, 1]
k: 3
frontSum: [0, 1]
frontSum: [0, 1, 3]
frontSum: [0, 1, 3, 6]
frontSum: [0, 1, 3, 6, 10]
frontSum: [0, 1, 3, 6, 10, 15]
frontSum: [0, 1, 3, 6, 10, 15, 21]
frontSum: [0, 1, 3, 6, 10, 15, 21, 22]
backSum : [0, 1]
backSum : [0, 1, 7]
backSum : [0, 1, 7, 12]
backSum : [0, 1, 7, 12, 16]
backSum : [0, 1, 7, 12, 16, 19]
backSum : [0, 1, 7, 12, 16, 19, 21]
backSum : [0, 1, 7, 12, 16, 19, 21, 22]
allCombinations: [12, 8, 4, 6]
\'\'\'
```

C++
```
    int maxScore(vector<int>& cardPoints, int k) {
        int sum = 0;
        int n = cardPoints.size();
        
        vector<int> cummulativeSumFromFront(n+1, 0);
        vector<int> cummulativeSumFromBehind(n+1, 0);
        
        sum = 0;
        for (int i=0; i<n; i++) {
            sum += cardPoints[i];
            cummulativeSumFromFront[i+1] = sum;
        }
        sum = 0;
        for (int i=n-1; i>=0; i--) {
            sum += cardPoints[i];
            cummulativeSumFromBehind[i] = sum;
        }
        
        // Reversing is optional. I reversed it so that it would be easy
        // to access sum of last (k-i) elements by just indexing at [k-i]
        // Otherwise, I would have had to index it at [n-k+i] which would
        // have made it difficult to read.
        reverse(cummulativeSumFromBehind.begin(), cummulativeSumFromBehind.end());
        
        int answer = 0;
        for(int i=0; i<=k; i++) {      
            answer = max(answer, 
                           cummulativeSumFromFront[i] // Sum of first \'i\' cards.
                         + cummulativeSumFromBehind[k-i]); // Sum of last \'k-i\' cards.
        }
        return answer;
    }
```

![image](https://assets.leetcode.com/users/images/1bcd57ac-4f9e-4acd-83c5-2817a391563f_1592777668.189349.png)

</p>


