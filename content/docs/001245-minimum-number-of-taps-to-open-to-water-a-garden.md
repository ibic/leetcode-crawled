---
title: "Minimum Number of Taps to Open to Water a Garden"
weight: 1245
#id: "minimum-number-of-taps-to-open-to-water-a-garden"
---
## Description
<div class="description">
<p>There is a one-dimensional garden on the x-axis. The garden starts at the point <code>0</code> and ends at the point <code>n</code>. (i.e The length of the garden is <code>n</code>).</p>

<p>There are&nbsp;<code>n + 1</code> taps located&nbsp;at points <code>[0, 1, ..., n]</code> in the garden.</p>

<p>Given an integer <code>n</code> and an integer array <code>ranges</code> of length <code>n + 1</code> where <code>ranges[i]</code> (0-indexed) means the <code>i-th</code> tap can water the area <code>[i - ranges[i], i + ranges[i]]</code> if it was open.</p>

<p>Return <em>the minimum number of taps</em> that should be open to water the whole garden, If the garden cannot be watered return <strong>-1</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/01/16/1685_example_1.png" style="width: 525px; height: 255px;" />
<pre>
<strong>Input:</strong> n = 5, ranges = [3,4,1,1,0,0]
<strong>Output:</strong> 1
<strong>Explanation:</strong> The tap at point 0 can cover the interval [-3,3]
The tap at point 1 can cover the interval [-3,5]
The tap at point 2 can cover the interval [1,3]
The tap at point 3 can cover the interval [2,4]
The tap at point 4 can cover the interval [4,4]
The tap at point 5 can cover the interval [5,5]
Opening Only the second tap will water the whole garden [0,5]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 3, ranges = [0,0,0,0]
<strong>Output:</strong> -1
<strong>Explanation:</strong> Even if you activate all the four taps you cannot water the whole garden.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 7, ranges = [1,2,1,0,2,1,0,1]
<strong>Output:</strong> 3
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 8, ranges = [4,0,0,0,0,0,0,0,4]
<strong>Output:</strong> 2
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 8, ranges = [4,0,0,0,4,0,0,0,4]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^4</code></li>
	<li><code>ranges.length == n + 1</code></li>
	<li><code>0 &lt;= ranges[i] &lt;= 100</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Greedy (greedy)

## Companies
- Google - 3 (taggedByAdmin: false)
- Atlassian - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Twitter - 4 (taggedByAdmin: false)
- Akuna Capital - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Similar to LC1024
- Author: lee215
- Creation Date: Sun Jan 19 2020 12:01:06 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 23 2020 20:30:11 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Example 2 did confuse me.
We actually need to water the whole segment, instead of `n + 1` point.
The taps with value `0` can water nothing.
<br>

# Solution 1: Brute Force DP
`dp[i]` is the minimum number of taps to water `[0, i]`.
Initialize `dp[i]` with max = `n + 2`
`dp[0] = [0]` as we need no tap to water nothing.

Find the leftmost point of garden to water with tap `i`.
Find the rightmost point of garden to water with tap `i`.
We can water `[left, right]` with onr tap,
and water `[0, left - 1]` with `dp[left - 1]` taps.
<br>

# Complexity
Time `O(NR)`, where `R = ranges[i] <= 100`
Space `O(N)`
<br>

**Java:**
```java
    public int minTaps(int n, int[] A) {
        int[] dp = new int[n + 1];
        Arrays.fill(dp, n + 2);
        dp[0] = 0;
        for (int i = 0; i <= n; ++i)
            for (int j = Math.max(i - A[i] + 1, 0); j <= Math.min(i + A[i], n); ++j)
                dp[j] = Math.min(dp[j], dp[Math.max(0, i - A[i])] + 1);
        return dp[n]  < n + 2 ? dp[n] : -1;
    }
```

**C++:**
```cpp
    int minTaps(int n, vector<int>& A) {
        vector<int> dp(n + 1, n + 2);
        dp[0] = 0;
        for (int i = 0; i <= n; ++i)
            for (int j = max(i - A[i] + 1, 0); j <= min(i + A[i], n); ++j)
                dp[j] = min(dp[j], dp[max(0, i - A[i])] + 1);
        return dp[n]  < n + 2 ? dp[n] : -1;
    }
```

**Python:**
```python
    def minTaps(self, n, A):
        dp = [0] + [n + 2] * n
        for i, x in enumerate(A):
            for j in xrange(max(i - x + 1, 0), min(i + x, n) + 1):
                dp[j] = min(dp[j], dp[max(0, i - x)] + 1)
        return dp[n] if dp[n] < n + 2 else -1
```
<br>

# Solution 2+: 1024. Video Stitching
Actually it\'s just a duplicate problem with a confusing description.
See this [1024. Video Stitching](https://leetcode.com/problems/video-stitching/discuss/270036/JavaC%2B%2BPython-Greedy-Solution-O(1)-Space)
and its 3 different solutions.
</p>


### [Python] Jump Game II
- Author: K_kkkyle
- Creation Date: Sun Jan 19 2020 12:10:25 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 23 2020 11:21:51 GMT+0800 (Singapore Standard Time)

<p>

We build a list `max_range` to store the max range it can be watered from each index.  

Then it becomes [Jump Game II](https://leetcode.com/problems/jump-game-ii/), where we want to find the minimum steps to jump from `0` to `n`.

The only difference is Jump Game II guarantees we can jump to the last index but this one not. We need to additionally identify the unreachable cases.

```python
class Solution:
    def minTaps(self, n: int, ranges: List[int]) -> int:
        max_range = [0] * (n + 1)
        
        for i, r in enumerate(ranges):
            left, right = max(0, i - r), min(n, i + r)
            max_range[left] = max(max_range[left], right - left)
        
		# it\'s a jump game now
        start = end = step = 0
        
        while end < n:
            step += 1
            start, end = end, max(i + max_range[i] for i in range(start, end + 1))
            if start == end:
                return -1
            
        return step
```
</p>


### [Java] A general greedy solution to process similar problems
- Author: BlueCamel
- Creation Date: Wed Feb 12 2020 16:57:13 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Feb 21 2020 15:13:02 GMT+0800 (Singapore Standard Time)

<p>
This problem is similiar to **Jump game II** and **Video Stiching**. Just summary a general idea to process this kind of problems for the conveninece of review in the future. (Welcome supplement other similar ones, thanks in advance).

First we need ensure that the array has been sorted by start time. Then say in each step, we have a range [left, right], which means we can only visit the elements within the range in this step.  We keep checking the elements until we are at the rightmost postion in [left, right] to update "farCanReach" ,  which means the farthest we can reach in next step. 


[1024]. **Video Stitching** (https://leetcode.com/problems/video-stitching/)
```
class Solution {
    public int videoStitching(int[][] clips, int T) {
        int end = 0, farCanReach = 0, cnt = 0;
        Arrays.sort(clips, (a, b) -> (a[0] - b[0])); // sort by start time
        if(clips[clips.length - 1][1] < T) return -1;
        for(int i = 0; end < T; end = farCanReach) {   // at the beginning of each step, we need update the "end"           
            cnt++;                       
            while(i < clips.length && clips[i][0] <= end) { // check all elements within the range
                farCanReach = Math.max(farCanReach, clips[i++][1]); // update the "farCanReach" for next step
            }
            if(end == farCanReach) return -1;   
			// if "farCanReach" isn\'t updated after we checked all elements within the range, we will fail in next step. 
			// say the first element in next step is [curS, curE], "curS" must be larger than "end" = "farCanReach".
        }		
        return cnt;
    }
}
```

[45]. **Jump Game II** (https://leetcode.com/problems/jump-game-ii/)
In this problem, the "left" in range [left, right] becomes the index of element. And the "right" becomes index + nums[index].
```
class Solution {
    public int jump(int[] nums) {
        int cnt = 0, farCanReach = 0, end = 0;
        for(int i = 0; end < nums.length - 1; end = farCanReach) {
            cnt++;
            while(i < nums.length && i <= end) {
                farCanReach = Math.max(farCanReach, i + nums[i++]);
            }           
            if(end == farCanReach) return -1;
        }
        return cnt;
    }
}
```

[1326]. **Minimum Number of Taps to Open to Water a Garden** (https://leetcode.com/problems/minimum-number-of-taps-to-open-to-water-a-garden/)
For this problem, we just need construct a new array to move the value into the leftmost point we can water, so the problem becomes **Jump Game II**. For example, at index i we could water (i - arr[i]) ~ (i + arr[i]). So we store the farthest point we can water at "i - arr[i]". Then "left" in range [left, right] is index and "right" is the value in arr[index].
```
class Solution {
    public int minTaps(int n, int[] ranges) {
        // construct the arr
        int[] arr = new int[n + 1];
        for(int i = 0; i < ranges.length; i++) {
            if(ranges[i] == 0) continue;
            int left = Math.max(0, i - ranges[i]);
            arr[left] = Math.max(arr[left], i + ranges[i]);
        }
		
		// same part like previous problem
        int end = 0, farCanReach = 0, cnt = 0;        
        for(int i = 0; i < arr.length && end < n; end = farCanReach) {
            cnt++;
            while(i < arr.length && i <= end) {
                farCanReach = Math.max(farCanReach, arr[i++]);                            
            }
            if(end == farCanReach) return -1; 
        }
        return cnt;
    }
}
```



</p>


