---
title: "Invalid Transactions"
weight: 1137
#id: "invalid-transactions"
---
## Description
<div class="description">
<p>A transaction is <em>possibly invalid</em> if:</p>

<ul>
	<li>the amount exceeds $1000, or;</li>
	<li>if it occurs within (and including) 60 minutes of another transaction with the same name in a different city.</li>
</ul>

<p>Each transaction string <code>transactions[i]</code>&nbsp;consists of&nbsp;comma separated values representing&nbsp;the name, time (in minutes), amount, and city of the transaction.</p>

<p>Given a list of <code>transactions</code>,&nbsp;return a list of transactions that are possibly invalid.&nbsp; You may return the answer in any order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> transactions = [&quot;alice,20,800,mtv&quot;,&quot;alice,50,100,beijing&quot;]
<strong>Output:</strong> [&quot;alice,20,800,mtv&quot;,&quot;alice,50,100,beijing&quot;]
<strong>Explanation:</strong> The first transaction is invalid because the second transaction occurs within a difference of 60 minutes, have the same name and is in a different city. Similarly the second one is invalid too.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> transactions = [&quot;alice,20,800,mtv&quot;,&quot;alice,50,1200,mtv&quot;]
<strong>Output:</strong> [&quot;alice,50,1200,mtv&quot;]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> transactions = [&quot;alice,20,800,mtv&quot;,&quot;bob,50,1200,mtv&quot;]
<strong>Output:</strong> [&quot;bob,50,1200,mtv&quot;]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>transactions.length &lt;= 1000</code></li>
	<li>Each <code>transactions[i]</code> takes the form <code>&quot;{name},{time},{amount},{city}&quot;</code></li>
	<li>Each <code>{name}</code> and <code>{city}</code>&nbsp;consist of&nbsp;lowercase English letters, and have lengths between <code>1</code> and <code>10</code>.</li>
	<li>Each <code>{time}</code> consist of&nbsp;digits, and represent an integer between <code>0</code> and <code>1000</code>.</li>
	<li>Each <code>{amount}</code>&nbsp;consist of&nbsp;digits, and represent an integer between <code>0</code> and <code>2000</code>.</li>
</ul>

</div>

## Tags
- Array (array)
- String (string)

## Companies
- Bloomberg - 10 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Garbage question
- Author: BigGreta
- Creation Date: Sun Aug 25 2019 12:11:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 25 2019 12:11:21 GMT+0800 (Singapore Standard Time)

<p>
1) Lots of ambuguity
2) Brute force solution works


</p>


### Super naive solution
- Author: hpj0408
- Creation Date: Sun Aug 25 2019 12:05:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 25 2019 12:05:21 GMT+0800 (Singapore Standard Time)

<p>
This is what I got in the shortest time and get accepted...
```
public List<String> invalidTransactions(String[] transactions) {
        List<String> ans = new ArrayList();
        int n = transactions.length;
        if (n == 0) return ans;
        String[] name = new String[n];
        int[] time = new int[n];
        int[] money = new int[n];
        String[] city = new String[n];
        boolean[] add = new boolean[n];
        for (int i = 0; i < n; i++) {
            String[] items = transactions[i].split(",");
            name[i] = items[0];
            time[i] = Integer.parseInt(items[1]);
            money[i] = Integer.parseInt(items[2]);
            city[i] = items[3];
        }
        
        for (int i = 0; i < n; i++) {
            if (money[i] > 1000)
                add[i] = true;
            for (int j = i + 1; j < n; j++) {
                if (name[i].equals(name[j]) && Math.abs(time[i] - time[j]) <= 60 && !city[i].equals(city[j])) {
                    add[i] = true;
                    add[j] = true;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            if (add[i])
                ans.add(transactions[i]);
        }
        return ans;
    }
```
</p>


### Brute force solution
- Author: keyvank1
- Creation Date: Sun Aug 25 2019 12:06:48 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Nov 29 2019 03:03:54 GMT+0800 (Singapore Standard Time)

<p>
The brute force worked during the exam time. We had to synthesize the given input vector of strings and break them into pieces that can be analyzed for the further conditions that exist in the problem.
The code goes as below. 
Time complexity: ```O(n^2)```
Space complexity: ```O(n)```
```
struct info{
    string name;
    int price;
    int time;
    string city;
};
class Solution {
public:
    vector<string> invalidTransactions(vector<string>& t) {
        
        vector<info> tmod;
        string st = "";
        info temp;
        int count;
        for (int i = 0 ;i<t.size();i++){
            count = 0;
            for (int j = 0;j<t[i].size();j++){
                if (t[i][j]==\',\'){
                    if (count==0)
                        temp.name = st;
                    if (count==1)
                        temp.time = stoi(st);
                    if (count==2)
                        temp.price = stoi(st);
                    count++;
                    st = "";
                    continue;
                }
                st+=t[i][j];
            }
            temp.city = st;
            tmod.push_back(temp);
            st = "";
        }
		
        vector<string> ret;
        bool found = false;
		//The brute force search of O(n^2) is as follows
        for (int i = 0;i<tmod.size();i++){
            found = false;
            if (tmod[i].price>=1000){
                ret.push_back(t[i]);
                continue;
            }
            for (int j = 0;j<tmod.size();j++){   
                if (i == j) continue;
                if (tmod[i].name==tmod[j].name && (abs)(tmod[i].time-tmod[j].time)<=60 && tmod[i].city!=tmod[j].city){
                    found = true;
                    break;
                }
            }
            if (found){
                ret.push_back(t[i]);
                found = false;
            }
        }
        return ret;
    }
};
```

</p>


