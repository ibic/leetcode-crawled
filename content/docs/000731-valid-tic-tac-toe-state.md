---
title: "Valid Tic-Tac-Toe State"
weight: 731
#id: "valid-tic-tac-toe-state"
---
## Description
<div class="description">
<p>A Tic-Tac-Toe board is given as a string array <code>board</code>. Return True if and only if it is possible to reach this board position during the course of a valid tic-tac-toe game.</p>

<p>The <code>board</code> is a 3 x 3 array, and consists of characters <code>&quot; &quot;</code>, <code>&quot;X&quot;</code>, and <code>&quot;O&quot;</code>.&nbsp; The &quot; &quot; character represents an empty square.</p>

<p>Here are the rules of Tic-Tac-Toe:</p>

<ul>
	<li>Players take turns placing characters into empty squares (&quot; &quot;).</li>
	<li>The first player always places &quot;X&quot; characters, while the second player always places &quot;O&quot; characters.</li>
	<li>&quot;X&quot; and &quot;O&quot; characters are always placed into empty squares, never filled ones.</li>
	<li>The game ends when there are 3 of the same (non-empty) character filling any row, column, or diagonal.</li>
	<li>The game also ends if all squares are non-empty.</li>
	<li>No more moves can be played if the game is over.</li>
</ul>

<pre>
<strong>Example 1:</strong>
<strong>Input:</strong> board = [&quot;O&nbsp; &quot;, &quot;&nbsp; &nbsp;&quot;, &quot;&nbsp; &nbsp;&quot;]
<strong>Output:</strong> false
<strong>Explanation:</strong> The first player always plays &quot;X&quot;.

<strong>Example 2:</strong>
<strong>Input:</strong> board = [&quot;XOX&quot;, &quot; X &quot;, &quot;   &quot;]
<strong>Output:</strong> false
<strong>Explanation:</strong> Players take turns making moves.

<strong>Example 3:</strong>
<strong>Input:</strong> board = [&quot;XXX&quot;, &quot;   &quot;, &quot;OOO&quot;]
<strong>Output:</strong> false

<strong>Example 4:</strong>
<strong>Input:</strong> board = [&quot;XOX&quot;, &quot;O O&quot;, &quot;XOX&quot;]
<strong>Output:</strong> true
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li><code>board</code> is a length-3 array of strings, where each string <code>board[i]</code> has length 3.</li>
	<li>Each <code>board[i][j]</code> is a character in the set <code>{&quot; &quot;, &quot;X&quot;, &quot;O&quot;}</code>.</li>
</ul>

</div>

## Tags
- Math (math)
- Recursion (recursion)

## Companies
- Microsoft - 5 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Ad-Hoc [Accepted]

**Intuition**

Let's try to think about the necessary conditions for a tic-tac-toe board to be valid.

* Since players take turns, the number of `'X'`s must be equal to or one greater than the number of `'O'`s.

* A player that wins has to win on the move that they make:
    * If the first player wins, the number of `'X'`s is one more than the number of `'O'`s
    * If the second player wins, the number of `'X'`s is equal to the number of `'O'`s.

* The board can't simultaneously have three `'X'`s and three `'O'`s in a row: once one player has won (on their move), there are no subsequent moves.

It turns out these conditions are also sufficient to check the validity of all boards.  This is because we can check these conditions in reverse: in any board, either no player, one player, or both players have won.  In the first two cases, we should check the previously stated counting conditions (a relationship between `xCount` and `oCount`), and this is sufficient.  In the last case, it is not allowed to have both players win simultaneously, so our check was also sufficient.

**Algorithm**

We'll count the number of `'X'`s and `'O'`s as `xCount` and `oCount`.  

We'll also use a helper function `win(player)` which checks if that player has won.  This checks the 8 horizontal, vertical, or diagonal lines of the board for if there are three in a row equal to that player.

After, we just have to check our conditions as stated above.

<iframe src="https://leetcode.com/playground/Z43YvKo4/shared" frameBorder="0" width="100%" height="500" name="Z43YvKo4"></iframe>

**Complexity Analysis**

* Time and Space Complexity:  $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Straightforward Java solution with explaination
- Author: myCafeBabe
- Creation Date: Sun Mar 04 2018 12:02:26 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 22:46:36 GMT+0800 (Singapore Standard Time)

<p>
When `turns` is 1, X moved. When `turns` is 0, O moved. `rows` stores the number of X or O in each row. `cols` stores the number of X or O in each column. `diag` stores the number of X or O in diagonal. `antidiag` stores the number of X or O in antidiagonal. When any of the value gets to 3, it means X wins. When any of the value gets to -3, it means O wins.

When X wins, O cannot move anymore, so `turns` must be 1. When O wins, X cannot move anymore, so `turns` must be 0. Finally, when we return, `turns` must be either 0 or 1, and X and O cannot win at same time.
```
class Solution {
    public boolean validTicTacToe(String[] board) {
        int turns = 0;
        boolean xwin = false; 
        boolean owin = false;
        int[] rows = new int[3];
        int[] cols = new int[3];
        int diag = 0;
        int antidiag = 0;
				
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i].charAt(j) == \'X\') {
                    turns++; rows[i]++; cols[j]++;
                    if (i == j) diag++;
                    if (i + j == 2) antidiag++;
                } else if (board[i].charAt(j) == \'O\') {
                    turns--; rows[i]--; cols[j]--;
                    if (i == j) diag--;
                    if (i + j == 2) antidiag--;
                }
            }
        }
		
        xwin = rows[0] == 3 || rows[1] == 3 || rows[2] == 3 || 
               cols[0] == 3 || cols[1] == 3 || cols[2] == 3 || 
               diag == 3 || antidiag == 3;
        owin = rows[0] == -3 || rows[1] == -3 || rows[2] == -3 || 
               cols[0] == -3 || cols[1] == -3 || cols[2] == -3 || 
               diag == -3 || antidiag == -3;
        
        if (xwin && turns == 0 || owin && turns == 1) {
            return false;
        }
        return (turns == 0 || turns == 1) && (!xwin || !owin);
    }
}
```
</p>


### Simple Python Solution with explanation
- Author: shriram2112
- Creation Date: Sun Mar 04 2018 12:09:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 11:52:26 GMT+0800 (Singapore Standard Time)

<p>
To find the validity of a given board, we could first think about the cases where the board is invalid
1) Since X starts first, x_count >= o_count. So if o_count > x_count, we can return False
2) Since the players take turns, we could also return False if x_count-o_count>1

After the corner cases, this is the algorithm used:

1)  If player O has a winning condition, also check the following:
     a)     If player X also has a winning condition, return False
     b)     If x_count != o_count , return False (Since player O always plays second, it has to meet this condition always)
2)  If player X has a winning condition,  check the following:
	  a)  If x_count != o_count + 1, return False (Since player X plays the first move, if player X wins, the player X's count would be 1 more than player O)
	


```
class Solution(object):
    def check_win_positions(self, board, player):
	"""
	Check if the given player has a win position.
	Return True if there is a win position. Else return False.
	"""
        #Check the rows
        for i in range(len(board)):
            if board[i][0] == board[i][1] == board[i][2] == player:
                return True                        

        #Check the columns
        for i in range(len(board)):
            if board[0][i] == board[1][i] == board[2][i] == player:
                return True 
										
        #Check the diagonals
        if board[0][0] == board[1][1] == board[2][2]  == player or \
               board[0][2] == board[1][1] == board[2][0] == player:
            return True
						
        return False
        
    def validTicTacToe(self, board):
        """
        :type board: List[str]
        :rtype: bool
        """
        
        x_count, o_count = 0, 0
        for i in range(len(board)):
            for j in range(len(board[0])):
                if board[i][j] == "X":
                    x_count += 1
                elif  board[i][j] == "O":
                    o_count += 1
										
        if o_count > x_count or x_count-o_count>1:
            return False
        
        if self.check_win_positions(board, 'O'):
            if self.check_win_positions(board, 'X'):
                return False
            return o_count == x_count
        
        if self.check_win_positions(board, 'X') and x_count!=o_count+1:
            return False

        return True
```
</p>


### Java faster than 100% Memory 99,66%
- Author: Kkennelly
- Creation Date: Mon Jun 24 2019 23:13:32 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 24 2019 23:13:32 GMT+0800 (Singapore Standard Time)

<p>
I was attempting to use some of the principles of "Clean Code" on this particular solution.  The purpose of this solution was to ensure proper naming of functions/variables and to keep functions/methods short and limited to one principle. I also attempted to maintain readablility of the code from top to bottom.

The solution doesn\'t require the 2D gameBoard array, I just find that it makes processing a little more straightforward with minimal memory cost.

\'\'\'
class Solution {
    
    private char[][] gameBoard = new char[3][3]; 
    
    public boolean validTicTacToe(String[] board) {
        
        unpackBoard(board);

        return gameBoardValidation();
    }
    
    private void unpackBoard(String[] board){
        for(int i=0; i<3;i++){
            for(int j=0; j<3; j++){
                this.gameBoard[i][j] = board[i].charAt(j);
            }
        }
    }
    
    private boolean gameBoardValidation(){
        
        int numX, numO;
        
        numX = count(\'X\');
        numO = count(\'O\');
        
        //X goes first
        if(numO>numX) return false;
        
        //players take turns
        if(numX>numO+1) return false;
        
        //both players can\'t win
        if(winner(\'X\') && winner(\'O\')) return false;
        
        //game ends when one player wins
        if(winner(\'X\') && numX==numO) return false;
        
        //game ends when one player wins
        if(winner(\'O\') && numX>numO) return false;
        
        return true;
    }
    
    private int count(char player){
        
        int num = 0;
        
        for(int i=0; i<3;i++){
            for(int j=0; j<3; j++){
                if(gameBoard[i][j] == player) num++;
            }
        }
        
        return num;
    }
    
    private boolean winner(char player){
        
        if(validateRows(player)) return true;
        if(validateColumns(player)) return true;
        if(validateDiagonal(player)) return true;
        
        return false;
    }
    
    private boolean validateRows(char player){
        for(int i=0; i<3; i++){
            if(gameBoard[i][0] == player && gameBoard[i][1] == player && gameBoard[i][2] == player) return true;
        }
        
        return false;
    }
    
    private boolean validateColumns(char player){
        for(int i=0; i<3; i++){
            if(gameBoard[0][i] == player && gameBoard[1][i] == player && gameBoard[2][i] == player) return true;
        }
        
        return false;
    }
    
    private boolean validateDiagonal(char player){
        if(gameBoard[0][0] == player && gameBoard[1][1] == player && gameBoard[2][2] == player) return true;
        if(gameBoard[0][2] == player && gameBoard[1][1] == player && gameBoard[2][0] == player) return true;
        
        return false;
    }
}
\'\'\'
</p>


