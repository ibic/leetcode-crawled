---
title: "Find and Replace Pattern"
weight: 840
#id: "find-and-replace-pattern"
---
## Description
<div class="description">
<p>You have a list of&nbsp;<code>words</code> and a <code>pattern</code>, and you want to know which words in <code>words</code> matches the pattern.</p>

<p>A word matches the pattern if there exists a permutation of letters <code>p</code> so that after replacing every letter <code>x</code> in the pattern with <code>p(x)</code>, we get the desired word.</p>

<p>(<em>Recall that a permutation of letters is a bijection from letters to letters: every letter maps to another letter, and no two letters map to the same letter.</em>)</p>

<p>Return a list of the words in <code>words</code>&nbsp;that match the given pattern.&nbsp;</p>

<p>You may return the answer in any order.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>words = <span id="example-input-1-1">[&quot;abc&quot;,&quot;deq&quot;,&quot;mee&quot;,&quot;aqq&quot;,&quot;dkd&quot;,&quot;ccc&quot;]</span>, pattern = <span id="example-input-1-2">&quot;abb&quot;</span>
<strong>Output: </strong><span id="example-output-1">[&quot;mee&quot;,&quot;aqq&quot;]</span>
<strong><span>Explanation: </span></strong>&quot;mee&quot; matches the pattern because there is a permutation {a -&gt; m, b -&gt; e, ...}. 
&quot;ccc&quot; does not match the pattern because {a -&gt; c, b -&gt; c, ...} is not a permutation,
since a and b map to the same letter.</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>1 &lt;= words.length &lt;= 50</code></li>
	<li><code>1 &lt;= pattern.length = words[i].length&nbsp;&lt;= 20</code></li>
</ul>
</div>

</div>

## Tags
- String (string)

## Companies
- Google - 4 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Two Maps

**Intuition and Algorithm**

If say, the first letter of the pattern is `"a"`, and the first letter of the word is `"x"`, then in the permutation, `"a"` must map to `"x"`.

We can write this bijection using two maps: a forward map $$\text{m1}$$ and a backwards map $$\text{m2}$$.

$$
\text{m1} : \text{"a"} \rightarrow \text{"x"}
$$
$$
\text{m2} : \text{"x"} \rightarrow \text{"a"}
$$

Then, if there is a contradiction later, we can catch it via one of the two maps.  For example, if the `(word, pattern)` is `("aa", "xy")`, we will catch the mistake in $$\text{m1}$$ (namely, $$\text{m1}(\text{"a"}) = \text{"x"} = \text{"y"}$$).  Similarly, with `(word, pattern) = ("ab", "xx")`, we will catch the mistake in $$\text{m2}$$.

<iframe src="https://leetcode.com/playground/7BNZbe2j/shared" frameBorder="0" width="100%" height="480" name="7BNZbe2j"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N * K)$$, where $$N$$ is the number of words, and $$K$$ is the length of each word.

* Space Complexity:  $$O(N * K)$$, the space used by the answer.
<br />
<br />


---
#### Approach 2: One Map

**Intuition and Algorithm**

As in *Approach 1*, we can have some forward map $$\text{m1} : \mathbb{L} \rightarrow \mathbb{L}$$, where $$\mathbb{L}$$ is the set of letters.  

However, instead of keeping track of the reverse map $$\text{m2}$$, we could simply make sure that every value $$\text{m1}(x)$$ in the codomain is reached at most once.  This would guarantee the desired permutation exists.

So our algorithm is this: after defining $$\text{m1}(x)$$ in the same way as *Approach 1* (the forward map of the permutation), afterwards we make sure it reaches distinct values.

<iframe src="https://leetcode.com/playground/nTprp8qf/shared" frameBorder="0" width="100%" height="497" name="nTprp8qf"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N * K)$$, where $$N$$ is the number of words, and $$K$$ is the length of each word.

* Space Complexity:  $$O(N * K)$$, the space used by the answer.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Normalise Word
- Author: lee215
- Creation Date: Sun Aug 19 2018 11:14:15 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 14:45:36 GMT+0800 (Singapore Standard Time)

<p>
Normalise a string to a standard pattern.
Yes. You can pass the F(pattern) to the sub function and return earlier in case of dismatch.

**C++**
```
    vector<string> findAndReplacePattern(vector<string> words, string p) {
        vector<string> res;
        for (string w : words) if (F(w) == F(p)) res.push_back(w);
        return res;
    }

    string F(string w) {
        unordered_map<char, int> m;
        for (char c : w) if (!m.count(c)) m[c] = m.size();
        for (int i = 0; i < w.length(); ++i) w[i] = \'a\' + m[w[i]];
        return w;
    }
```

**Java:**
```
    public List<String> findAndReplacePattern(String[] words, String pattern) {
        int[] p = F(pattern);
        List<String> res = new ArrayList<String>();
        for (String w : words)
            if (Arrays.equals(F(w), p)) res.add(w);
        return res;
    }

    public int[] F(String w) {
        HashMap<Character, Integer> m = new HashMap<>();
        int n = w.length();
        int[] res = new int[n];
        for (int i = 0; i < n; i++) {
            m.putIfAbsent(w.charAt(i), m.size());
            res[i] = m.get(w.charAt(i));
        }
        return res;
    }
```
**Python:**
```
    def findAndReplacePattern(self, words, p):
        def F(w):
            m = {}
            return [m.setdefault(c, len(m)) for c in w]
        return [w for w in words if F(w) == F(p)]
```

</p>


### Short python isomorphism solution
- Author: wpanther
- Creation Date: Mon Aug 20 2018 23:21:07 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 23:53:20 GMT+0800 (Singapore Standard Time)

<p>
Similar to an earlier isomorphic string problem, check the length of the sets and the length of the set when the characters are zipped together.

```
        b = pattern
        def is_iso(a):
            return len(a) == len(b) and len(set(a)) == len(set(b)) == len(set(zip(a, b)))
        return filter(is_iso, words)
```
</p>


### JAVA 3ms Clear Code
- Author: caraxin
- Creation Date: Sun Aug 19 2018 11:02:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 06:29:10 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public List<String> findAndReplacePattern(String[] words, String pattern) {
        List<String> res= new LinkedList<>();
        for (String w: words){
            int[] p= new int[26], s= new int[26];
            boolean same=true;
            for (int i=0; i<w.length(); i++){
                if (s[w.charAt(i)-\'a\']!=p[pattern.charAt(i)-\'a\']){
                    same=false;
                    break;
                }else{
                    s[w.charAt(i)-\'a\']=p[pattern.charAt(i)-\'a\']=i+1;
                }
            }
            if (same) res.add(w);
        }
        return res;
    }
}
```
</p>


