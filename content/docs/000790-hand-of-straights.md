---
title: "Hand of Straights"
weight: 790
#id: "hand-of-straights"
---
## Description
<div class="description">
<p>Alice has a <code>hand</code> of cards, given as an array of integers.</p>

<p>Now she wants to rearrange the cards into groups so that each group is size <code>W</code>, and consists of <code>W</code> consecutive cards.</p>

<p>Return <code>true</code> if and only if she can.</p>

<p>&nbsp;</p>

<ol>
</ol>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>hand = [1,2,3,6,2,3,4,7,8], W = 3
<strong>Output: </strong>true
<strong>Explanation:</strong> Alice&#39;s <code>hand</code> can be rearranged as <code>[1,2,3],[2,3,4],[6,7,8]</code>.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>hand = [1,2,3,4,5], W = 4
<strong>Output: </strong>false
<strong>Explanation:</strong> Alice&#39;s <code>hand</code> can&#39;t be rearranged into groups of <code>4</code>.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= hand.length &lt;= 10000</code></li>
	<li><code>0 &lt;= hand[i]&nbsp;&lt;= 10^9</code></li>
	<li><code>1 &lt;= W &lt;= hand.length</code></li>
</ul>
<strong>Note:</strong> This question is the same as&nbsp;1296:&nbsp;<a href="https://leetcode.com/problems/divide-array-in-sets-of-k-consecutive-numbers/">https://leetcode.com/problems/divide-array-in-sets-of-k-consecutive-numbers/</a>
</div>

## Tags
- Ordered Map (ordered-map)

## Companies
- Google - 11 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Brute Force [Accepted]

**Intuition**

We will repeatedly try to form a group (of size W) starting with the lowest card.  This works because the lowest card still in our hand must be the bottom end of a size `W` straight.

**Algorithm**

Let's keep a count `{card: number of copies of card}` as a `TreeMap` (or `dict`).

Then, repeatedly we will do the following steps: find the lowest value card that has 1 or more copies (say with value `x`), and try to remove `x, x+1, x+2, ..., x+W-1` from our count.  If we can't, then the task is impossible.

<iframe src="https://leetcode.com/playground/LqHyAdPA/shared" frameBorder="0" width="100%" height="446" name="LqHyAdPA"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N * (N/W))$$, where $$N$$ is the length of `hand`.  The $$(N / W)$$ factor comes from `min(count)`.  In Java, the $$(N / W)$$ factor becomes $$\log N$$ due to the complexity of `TreeMap`.

* Space Complexity:  $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] O(MlogM) Complexity
- Author: lee215
- Creation Date: Sun Jun 03 2018 11:03:54 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 06 2020 15:26:31 GMT+0800 (Singapore Standard Time)

<p>

# **Solution 1**
1. Count number of different cards to a map `c`
2. Loop from the smallest card number.
3. Everytime we meet a new card `i`, we cut off `i` - `i + W - 1` from the counter.
<br>

# **Complexity**:
Time `O(MlogM + MW)`, where `M` is the number of different cards.
<br>

**Java:**
```java
    public boolean isNStraightHand(int[] hand, int W) {
        Map<Integer, Integer> c = new TreeMap<>();
        for (int i : hand) c.put(i, c.getOrDefault(i, 0)+1);
        for (int it : c.keySet())
            if (c.get(it) > 0)
                for (int i = W - 1; i >= 0; --i) {
                    if (c.getOrDefault(it + i, 0) < c.get(it)) return false;
                    c.put(it + i, c.get(it + i) - c.get(it));
                }
        return true;
    }
```

**C++:**
```cpp
    bool isNStraightHand(vector<int> hand, int W) {
        map<int, int> c;
        for (int i : hand) c[i]++;
        for (auto it : c)
            if (c[it.first] > 0)
                for (int i = W - 1; i >= 0; --i)
                    if ((c[it.first + i] -= c[it.first]) < 0)
                        return false;
        return true;
    }
```
**Python:**
```py
    def isNStraightHand(self, hand, W):
        c = collections.Counter(hand)
        for i in sorted(c):
            if c[i] > 0:
                for j in range(W)[::-1]:
                    c[i + j] -= c[i]
                    if c[i + j] < 0:
                        return False
        return True
```

# **Follow Up**
We just got lucky AC solution. Because `W <= 10000`.
What if W is huge, should we cut off card on by one?
<br>

# **Solution 2**:
1. Count number of different cards to a map `c`
2. `Cur` represent current open straight groups.
3. In a deque `start`, we record the number of opened a straight group.
4. Loop from the smallest card number.

For example, hand = [1,2,3,2,3,4], W = 3
We meet one 1:
    opened = 0, we open a new straight groups starting at 1, push (1,1) to `start`.
We meet two 2:
    opened = 1, we need open another straight groups starting at 1, push (2,1) to `start`.
We meet two 3:
    opened = 2, it match current opened groups.
    We open one group at 1, now we close it. opened = opened - 1 = 1
We meet one 4:
    opened = 1, it match current opened groups.
    We open one group at 2, now we close it. opened = opened - 1 = 0

5. return if no more open groups.
<br>

# **Complexity**
`O(MlogM + N)`, where `M` is the number of different cards.
Because I count and sort cards.
In Cpp and Java it\'s O(NlogM), which can also be improved.
<br>

**Java**
```java
    public boolean isNStraightHand(int[] hand, int W) {
        Map<Integer, Integer> c = new TreeMap<>();
        for (int i : hand) c.put(i, c.getOrDefault(i, 0)+1);
        Queue<Integer> start = new LinkedList<>();
        int last_checked = -1, opened = 0;
        for (int i : c.keySet()) {
            if (opened > 0 && i > last_checked + 1 || opened > c.get(i)) return false;
            start.add(c.get(i) - opened);
            last_checked = i; opened = c.get(i);
            if (start.size() == W) opened -= start.remove();
        }
        return opened == 0;
    }
```

**C++:**
```cpp
    bool isNStraightHand(vector<int> hand, int W) {
        map<int, int> c;
        for (int i : hand) c[i]++;
        queue<int> start;
        int last_checked = -1, opened = 0;
        for (auto it : c) {
            int i = it.first;
            if (opened > 0 && i > last_checked + 1 || opened > c[i]) return false;
            start.push(c[i] - opened);
            last_checked = i, opened = c[i];
            if (start.size() == W) {
                opened -= start.front();
                start.pop();
            }
        }
        return opened == 0;
    }
```

**Python:**
```py
    def isNStraightHand(self, hand, W):
        c = collections.Counter(hand)
        start = collections.deque()
        last_checked, opened = -1, 0
        for i in sorted(c):
            if opened > c[i] or opened > 0 and i > last_checked + 1: return False
            start.append(c[i] - opened)
            last_checked, opened = i, c[i]
            if len(start) == W: opened -= start.popleft()
        return opened == 0
```
</p>


### Simple Java solution using priority queue
- Author: dbarbs
- Creation Date: Tue Jun 05 2018 08:13:45 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 05:36:52 GMT+0800 (Singapore Standard Time)

<p>
Would love any criticism / recommendations!

Empirical runtime: 
40ms

Motivation: 
Since the numbers need to consecutive we immediately can figure out the next W - 1 numbers that must occur after a given number.  By using a priority queue we can poll the smallest number and remove the next W - 1 consecutive numbers. If any of the consecutive numbers are not in the priority queue, it implies the hand is invalid and thus returns false.


```
class Solution {
    public boolean isNStraightHand(int[] hand, int W) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for(int i : hand){
            minHeap.add(i);
        }
        while(minHeap.size() != 0) {
            int start = minHeap.poll();
            for(int j = 1; j < W; j++){
                if(minHeap.remove(start + j)) {
                    continue;
                }
                else {
                    return false;
                }
            }
        }
        return true;
    }
}
```
</p>


### Python O(nlgn) simple solution with intuition
- Author: ZhaoJing_0x
- Creation Date: Sun Jun 03 2018 11:39:38 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 04 2018 00:14:05 GMT+0800 (Singapore Standard Time)

<p>
Given test case like ```hand = [1,1,1,2,2,2,3,3,3], W = 3```, what do you think?

My thought is: **If I know the first number in one of the group, I know the whole group.**

e.g. If i know the first number in a group is 1 and W == 3, then the group must be [1, 2, 3]. 

That is to say, I just need to find all starting numbers of each group, then go check if all the other numbers in the group can be found in the given ```hand```.

1. First suppose we know the starting number of each group. The question is: **How do you know if the other numbers of the group exist in ```hand```**?

	Like if we know 2 must be the starting number in a group of length 3, then we immediately know that the group consists of [2,3,4]. 
	
	To check if 3 and 4 exist in ```hand```, we can just use a hash table to find this out.
2. Still, there might be groups with the same starting number, like **if group_1 and group_2 both have starting number 2, how do you know if there are two 3s and 4s**?

	This can be easily solved by storing each number\'s time of appearence in the hash table, like a Python dictionary or a C++ std::unordered_map. 
	
	Each time we used a number, we decrease its counts, once its counts is 0, we know the number is no longer available.

3. Final question is: **How do you find the starting number of each group?**
	We can get some intuition by observing an example:
	```latex
	hand = [1, 2, 3, 3, 4, 5] and W = 3
	```
	We know that there are 2 groups and their corresponding starting numbers are 1 and 3. How do we find this out?
	
	We can easily find the starting number of the first group, namely 1. It\'s just the minimum of the given ```hand```. Then we automatically constructed the group ```[1,2,3]```. After that, the minimum of remaining numbers in ```hand``` is 3, of course it\'s the starting number of the other group! 
	
	Yes, we can iteratively find the minimum after each group\'s construction. To achieve this, we can use a minimum heap. Each time we want to find a starting number, we pop the heap. If the number is no longer available, we pop again until we find the minimum of the remaining numbers.
	
Below is my Python code using heap and dictionary:
```Python
from collections import defaultdict
from heapq import heappop, heapify
class Solution:
    def isNStraightHand(self, hand, W):
        """
        :type hand: List[int]
        :type W: int
        :rtype: bool
        """
        l = len(hand)
        if l % W:
            return False
        if W == 1:
            return True
        
	# first we count the numbers
        cnt = defaultdict(int)
        for i in hand:
            cnt[i] += 1
	# then we build the minimum heap
        heapify(hand)
				
        for i in range(l // W):    
	    # first we find the starting number of current group
            start = heappop(hand)  
            while cnt[start] == 0:  # if the number is no loner available
                start = heappop(hand)  # we pop again
            
	    # Now we find the all other numbers in the group
            for i in range(W):  
                cnt[start] -= 1  # decrease its counts
                if cnt[start] < 0:  # the number is not available
                    return False
                start += 1
        return True
```
	

</p>


