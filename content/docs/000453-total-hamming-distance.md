---
title: "Total Hamming Distance"
weight: 453
#id: "total-hamming-distance"
---
## Description
<div class="description">
<p>The <a href="https://en.wikipedia.org/wiki/Hamming_distance" target="_blank">Hamming distance</a> between two integers is the number of positions at which the corresponding bits are different.</p>

<p>Now your job is to find the total Hamming distance between all pairs of the given numbers.</p>


<p><b>Example:</b><br />
<pre>
<b>Input:</b> 4, 14, 2

<b>Output:</b> 6

<b>Explanation:</b> In binary representation, the 4 is 0100, 14 is 1110, and 2 is 0010 (just
showing the four bits relevant in this case). So the answer will be:
HammingDistance(4, 14) + HammingDistance(4, 2) + HammingDistance(14, 2) = 2 + 2 + 2 = 6.
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>Elements of the given array are in the range of <code>0 </code> to <code>10^9</code>
<li>Length of the array will not exceed <code>10^4</code>. </li>
</ol>
</p>
</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Facebook - 7 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach #1 Brute Force [Time Limit Exceeded]

**Intuition**

1. Check all the unique pairs of elements for differing bits.
2. `xor` of two bits is `1` if they are not the same, and `0` otherwise.

**Algorithm**

Bitwise `xor` of two numbers will give a bitwise representation of where the bits of two numbers differ. Such bit positions are represented by a `1` bit in the resultant. For example:

        0010 1101   ==  45
      ^ 0100 1010   ==  74
    ----------------
        0110 0111
         ^^   ^^^   => 5 differing bits

Hence the numbers $$45$$ and $$74$$ have five differing bits. Thus the Hamming Distance between them is $$5$$.

For each of the $$\approx n^2/2$$ pairs of elements, simply apply bitwise `xor` to them to find out a resultant representation which tells us the differing bits. Count the `1` bits to find the Hamming Distance for that pair of elements. Sum over all pairs to get the total Hamming Distance.

<iframe src="https://leetcode.com/playground/HpH92ApX/shared" frameBorder="0" name="HpH92ApX" width="100%" height="275"></iframe>

**NOTE:** The `__builtin_popcount()` method is an internal built-in function available in **C** (and hence by extension **C++**) which gives the count of `1` bits for an `int` type argument. Being a low level built-in method, it is understandably faster than running a hand rolled loop.
As an alternative, you can iterate over all the bits of the number and count the `1` bits. Take a look at the code for [Approach #2](#approach-2-loop-over-the-bits-accepted) for hints on how to achieve that.

**Complexity Analysis**

* Time complexity: $$O(n^2 \cdot log_2V) \simeq O(n^2)$$.

    + There are exactly $$\binom{n}{2} = {}^nC_2 = \frac{n \cdot (n-1)}{2}$$ unique pairs of elements for an $$n$$ element array.
    + Each of these pairs, when `xor`ed, result in a resultant number which is $$\lceil log_2V \rceil$$ bits long. Here $$V$$ is the largest value any of the elements can ever take.
    + We iterate over these many bits to count the number of `1` bits.  In our case, since all elements are $$\leq 10^9$$, the value $$\lceil log_2V \rceil = 30$$ is a small constant. Hence counting the `1` bits takes place in nearly constant (i.e. $$O(1)$$) time.

* Space complexity: $$O(1)$$ constant space.

---
#### Approach #2 Loop over the bits! [Accepted]

**Intuition**

Looping over all possible combinations of two element pairs, increases quadratically over the size of the input. If we could instead loop over the bits of the elements (which is constant), we could shave off an input dimension from our runtime complexity.

**Algorithm**

Say for any particular bit position, count the number of elements with this bit **ON** (i.e. this particular bit is `1`). Let this count be $$k$$. Hence the number of elements with this bit **OFF** (i.e. `0`) is $$(n - k)$$ (in an $$n$$ element array).

Certainly unique pairs of elements exists where one element has this particular bit **ON** while the other element has this **OFF** (i.e. this particular bit differs for the two elements of this pair).
> We can argue that every such pair contributes one unit to the Hamming Distance for this particular bit.

We know that the count of such unique pairs is $${}^kC_1 * {}^{n-k}C_1 = k \cdot (n-k)$$ for this particular bit. Hence Hamming Distance for this particular bit is $$k \cdot (n-k)$$.

For each of the $$\lceil log_2V \rceil$$ bits that we can check, we can calculate a Hamming Distance pertaining to that bit. Taking sum over the Hamming Distances of all these bits, we get the total Hamming Distance.

<iframe src="https://leetcode.com/playground/EoYRygcG/shared" frameBorder="0" name="EoYRygcG" width="100%" height="445"></iframe>

**NOTE:** You *can switch the order of the loops* while counting `1` bits without affecting complexity. However it might give some performance changes due to locality of reference and the resultant cache hits/misses.

**Complexity Analysis**

* Time complexity: $$O(n \cdot log_2V) \simeq O(n)$$. Runtime performance is limited by the double loop where we are counting elements for particular bits. In our case, since all elements are $$\leq 10^9$$, the value $$\lceil log_2V \rceil = 30$$ is a small constant. Thus the inner loop runs in nearly constant time.

* Space complexity: $$O(log_2V) \to O(1)$$ extra space.

    + For each of the $$\lceil log_2V \rceil$$ bits, we need to maintain a count which is later used to calculate the Hamming Distance for that bit. Since $$\lceil log_2V \rceil \approx 32$$ is a small constant in our case, that takes nearly constant extra space.
    + Another thing to notice, is that if we **switch the order of the double loop,** we can do away with storing the count and calculate the Hamming Distance for that bit then and there. That results in only constant extra space being used.

---
#### Bonus!

This question is a perfect example of how vectorized operations can result in small, elegant and good performance code. Take a look at this slick **Python** solution to this problem (by [@StefanPochmann](https://leetcode.com/StefanPochmann/)):

<iframe src="https://leetcode.com/playground/uskMAwMG/shared" frameBorder="0" name="uskMAwMG" width="100%" height="88"></iframe>

The `*` operator turns the list of `32`-bit wide binary strings returned by `map` into individual arguments to the `zip` method. The `zip` method vectorizes the string arguments to create a list of vectors (each of which is a vector `b` of particular bits from every element in the input array; There are `32` such vectors of size `len(nums)` each, in this list ). Finally we use the same technique as [Approach #2](#approach-2-loop-over-the-bits-accepted) to calculate the total Hamming Distance.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n) time O(1) Space
- Author: compton_scatter
- Creation Date: Sun Dec 18 2016 12:46:14 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 13:14:58 GMT+0800 (Singapore Standard Time)

<p>
For each bit position 1-32 in a 32-bit integer, we count the number of integers in the array which have that bit set. Then, if there are n integers in the array and k of them have a particular bit set and (n-k) do not, then that bit contributes k*(n-k) hamming distance to the total.

```
public int totalHammingDistance(int[] nums) {
    int total = 0, n = nums.length;
    for (int j=0;j<32;j++) {
        int bitCount = 0;
        for (int i=0;i<n;i++) 
            bitCount += (nums[i] >> j) & 1;
        total += bitCount*(n - bitCount);
    }
    return total;
}
```
</p>


### Share my O(n) C++ bitwise solution with thinking process and explanation
- Author: KJer
- Creation Date: Sun Dec 18 2016 12:54:53 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 00:10:47 GMT+0800 (Singapore Standard Time)

<p>
---
## 1. Problem

---
The problem is to find the total Hamming distance between all pairs of the given numbers.

---
## 2. Thinking process

---
#### 2.1 For one pair

---
When you calculate Hamming distance between x and y, you just

---
1. calculate p = x ^ y;
2. count the number of 1's in p

---
**The distance from x to y is as same as y to x.**

---
#### 2.2 Trivial approach

---
For a series of number: a1, a2, a3,..., an

Use the approach in 2.1
(suppose distance(x, y) is the Hamming distance between x and y):

For a1, calculate S(1) = distance(a1, a2)+distance(a1, a3)+ ... +distance(a1, an)
For a2, calculate S(2) = distance(a2, a3)+distance(a2, a4)+ ... +distance(a2, an)
......
For a(n - 1), calculate S(n - 1) = distance(a(n - 1), a(n))

Finally , **S = S(1) + S(2) + ... + S(n - 1)**.

The function distance is called **1 + 2 + ... + (n - 1) = n(n - 1)/2** times! That's too much!

---
#### 2.3 New idea

---
The total Hamming distance is constructed **bit by bit** in this approach.

Let's take a series of number: **a1, a2, a3,..., an**

Just think about all the **Least Significant Bit (LSB)** of a(k) (1 \u2264 k \u2264 n).

**How many Hamming distance will they bring to the total?**

1. If a pair of number has same LSB, the total distance will get 0.

2. If a pair of number has different LSB, the total distance will get 1.

---
For all number **a1, a2, a3,..., a(n)**, if there are **p** numbers have **0 as LSB (put in set M)**, and **q** numbers have **1 for LSB (put in set N)**. 

There are **2 situations**:

**Situation 1**. If the **2 number in a pair both comes from M (or N)**, the total will get **0**.

**Situation 2**. If the **1 number in a pair comes from M, the other comes from N**, the total will get **1**.

Since **Situation 1** will add **NOTHING** to the total, we only need to think about **Situation 2**

**How many pairs are there in Situation 2?** 

We choose **1 number from M (p possibilities)**, and **1 number from N (q possibilities)**.

The total possibilities is **p \xd7 q = pq**, which means

>#### **The total Hamming distance will get pq from LSB.**

---
If we **remove the LSB of all numbers (right logical shift)**, the same idea can be used **again and again until all numbers becomes zero**

---
#### 2.4 Time complexity

---
In each loop, we need to **visit all numbers in nums** to **calculate how many numbers has 0 (or 1) as LSB**.

If the biggest number in nums[] is K, **the total number of loop is [logK] + 1**.

So, **the total time complexity of this approach is O(n)**.

---
## 3. Code
---
```
class Solution {
public:
    int totalHammingDistance(vector<int>& nums) {
        int size = nums.size();
        if(size < 2) return 0;
        int ans = 0;
        int *zeroOne = new int[2];
        while(true)
        {
            int zeroCount = 0;
            zeroOne[0] = 0;
            zeroOne[1] = 0;
            for(int i = 0; i < nums.size(); i++)
            {
                if(nums[i] == 0) zeroCount++;
                zeroOne[nums[i] % 2]++;
                nums[i] = nums[i] >> 1;
            }
            ans += zeroOne[0] * zeroOne[1];
            if(zeroCount == nums.size()) return ans;
        }
    }
};
```
---
</p>


### Python via Strings
- Author: StefanPochmann
- Creation Date: Mon Dec 19 2016 00:51:35 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 19:43:24 GMT+0800 (Singapore Standard Time)

<p>

    def totalHammingDistance(self, nums):
        return sum(b.count('0') * b.count('1') for b in zip(*map('{:032b}'.format, nums)))
</p>


