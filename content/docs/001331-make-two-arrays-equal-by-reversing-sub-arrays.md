---
title: "Make Two Arrays Equal by Reversing Sub-arrays"
weight: 1331
#id: "make-two-arrays-equal-by-reversing-sub-arrays"
---
## Description
<div class="description">
<p>Given two integer arrays of equal length <code>target</code> and <code>arr</code>.</p>

<p>In one step, you can select any <strong>non-empty sub-array</strong> of <code>arr</code> and reverse it. You are allowed to make any number of steps.</p>

<p>Return <em>True</em> if you can make <code>arr</code> equal to <code>target</code>, or <em>False</em> otherwise.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> target = [1,2,3,4], arr = [2,4,1,3]
<strong>Output:</strong> true
<strong>Explanation:</strong> You can follow the next steps to convert arr to target:
1- Reverse sub-array [2,4,1], arr becomes [1,4,2,3]
2- Reverse sub-array [4,2], arr becomes [1,2,4,3]
3- Reverse sub-array [4,3], arr becomes [1,2,3,4]
There are multiple ways to convert arr to target, this is not the only way to do so.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> target = [7], arr = [7]
<strong>Output:</strong> true
<strong>Explanation:</strong> arr is equal to target without any reverses.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> target = [1,12], arr = [12,1]
<strong>Output:</strong> true
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> target = [3,7,9], arr = [3,7,11]
<strong>Output:</strong> false
<strong>Explanation:</strong> arr doesn&#39;t have value 9 and it can never be converted to target.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> target = [1,1,1,1,1], arr = [1,1,1,1,1]
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>target.length == arr.length</code></li>
	<li><code>1 &lt;= target.length &lt;= 1000</code></li>
	<li><code>1 &lt;= target[i] &lt;= 1000</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Facebook - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Python] 1-lines
- Author: lee215
- Creation Date: Sun May 31 2020 00:06:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 07 2020 00:04:02 GMT+0800 (Singapore Standard Time)

<p>
# Solution1
Time `O(nlogn)`
**python**
```py
    def canBeEqual(self, target, A):
        return sorted(target) == sorted(A)
```
<br>

# Solution2
Time `O(n)`
**C++**
```cpp
    bool canBeEqual(vector<int>& target, vector<int>& A) {
        return unordered_multiset<int>(A.begin(), A.end()) == unordered_multiset<int>(target.begin(),target.end());
    }
```
**python**
```py
    def canBeEqual(self, target, A):
        return collections.Counter(target) == collections.Counter(A)
```



</p>


### 100% faster | Java Arrays sort and equals | Video Explanation
- Author: nikcode
- Creation Date: Sun May 31 2020 00:13:26 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 03 2020 00:19:37 GMT+0800 (Singapore Standard Time)

<p>
Approach1 - Just sort both the arrays and compare if equals
Approach2 - Use Bucket to count frequency of each number from target ++ and arr --

**Detailed Video Explanation and Live Coding** https://www.youtube.com/watch?v=Uj8zeMO6iXE


**Problem Constraints:**

```
target.length == arr.length
1 <= target.length <= 1000
1 <= target[i] <= 1000
1 <= arr[i] <= 1000
```

**Approach1**
`TC - O(nlogn)`

```
class Solution {
    public boolean canBeEqual(int[] target, int[] arr) {
        Arrays.sort(target);
        Arrays.sort(arr);
        return Arrays.equals(target, arr);
    }
}
```

**Approach2**
`TC = O(n)`

```
class Solution {
    public boolean canBeEqual(int[] target, int[] arr) {
        int[] f = new int[1001];
        for(int i = 0; i < arr.length; i++) {
            f[target[i]]++;
            f[arr[i]]--;
        }
        
        for(int i = 0 ; i < 1001; i++)
            if(f[i] != 0)
                return false;
        return  true;
    }
}
```

</p>


### [Java/Python 3] Count the arrays and compare.
- Author: rock
- Creation Date: Sun May 31 2020 00:01:52 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 22 2020 22:55:46 GMT+0800 (Singapore Standard Time)

<p>


```java
    public boolean canBeEqual(int[] target, int[] arr) {
        int[] cnt = new int[1001];
        for (int t : target)
            ++cnt[t];
        for (int a : arr) {
            if (--cnt[a] < 0) {
                return false;
            }
        }
        return true;
    }
```


```python
    def canBeEqual(self, target: List[int], arr: List[int]) -> bool:
        c = collections.Counter(target)
        for a in arr:
            c[a] -= 1
            if c[a] < 0:
                return False
        return True    
```
Or simplified as 
```python
    def canBeEqual(self, target: List[int], arr: List[int]) -> bool:
        return collections.Counter(target) == collections.Counter(arr)
```
</p>


