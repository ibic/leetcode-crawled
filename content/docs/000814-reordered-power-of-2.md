---
title: "Reordered Power of 2"
weight: 814
#id: "reordered-power-of-2"
---
## Description
<div class="description">
<p>Starting with a positive integer <code>N</code>, we reorder the digits in any order (including the original order) such that the leading digit is not zero.</p>

<p>Return <code>true</code>&nbsp;if and only if we can do this in a way such that the resulting number is a power of 2.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">1</span>
<strong>Output: </strong><span id="example-output-1">true</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">10</span>
<strong>Output: </strong><span id="example-output-2">false</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">16</span>
<strong>Output: </strong><span id="example-output-3">true</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">24</span>
<strong>Output: </strong><span id="example-output-4">false</span>
</pre>

<div>
<p><strong>Example 5:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-5-1">46</span>
<strong>Output: </strong><span id="example-output-5">true</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= N &lt;= 10^9</code></li>
</ol>
</div>
</div>
</div>
</div>
</div>

</div>

## Tags
- Math (math)

## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach 1: Permutations

**Intuition**

For each permutation of the digits of `N`, let's check if that permutation is a power of 2.

**Algorithm**

This approach has two steps: how will we generate the permutations of the digits, and how will we check that the permutation represents a power of 2?

To generate permutations of the digits, we place any digit into the first position (`start = 0`), then any of the remaining digits into the second position (`start = 1`), and so on.  In Python, we can use the builtin function `itertools.permutations`.

To check whether a permutation represents a power of 2, we check that there is no leading zero, and divide out all factors of 2.  If the result is `1` (that is, it contained no other factors besides `2`), then it was a power of 2.  In Python, we can use the check `bin(N).count('1') == 1`.


<iframe src="https://leetcode.com/playground/njvoSmkx/shared" frameBorder="0" width="100%" height="500" name="njvoSmkx"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O((\log N)! * \log N)$$.  Note that $$\log N$$ is the number of digits in the binary representation of $$N$$.  For each of $$(\log N)!$$ permutations of the digits of $$N$$, we need to check that it is a power of 2 in $$O(\log N)$$ time.

* Space Complexity:  $$O(\log N)$$, the space used by `A` (or `cand` in Python).
<br />
<br />


---
#### Approach 2: Counting

**Intuition and Algorithm**

We can check whether two numbers have the same digits by comparing the *count* of their digits.  For example, 338 and 833 have the same digits because they both have exactly two 3's and one 8.

Since $$N$$ could only be a power of 2 with 9 digits or less (namely, $$2^0, 2^1, \cdots, 2^29$$), we can just check whether $$N$$ has the same digits as any of these possibilities.

<iframe src="https://leetcode.com/playground/5mfD3xtv/shared" frameBorder="0" width="100%" height="395" name="5mfD3xtv"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\log^2 N)$$.  There are $$\log N$$ different candidate powers of 2, and each comparison has $$O(\log N)$$ time complexity.

* Space Complexity:  $$O(\log N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Jul 15 2018 11:07:40 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 10:53:21 GMT+0800 (Singapore Standard Time)

<p>
`counter` will counter the number of digits 9876543210 in the given number.
Then I just compare `counter(N)` with all `counter(power of 2)`.
`1 <= N <= 10^9`, so up to 8 same digits.
If `N > 10^9`, we can use a hash map.

**C++:**
```
    bool reorderedPowerOf2(int N) {
        long c = counter(N);
        for (int i = 0; i < 32; i++)
            if (counter(1 << i) == c) return true;
        return false;
    }

    long counter(int N) {
        long res = 0;
        for (; N; N /= 10) res += pow(10, N % 10);
        return res;
    }
```

**Java:**
```
    public boolean reorderedPowerOf2(int N) {
        long c = counter(N);
        for (int i = 0; i < 32; i++)
            if (counter(1 << i) == c) return true;
        return false;
    }
    public long counter(int N) {
        long res = 0;
        for (; N > 0; N /= 10) res += (int)Math.pow(10, N % 10);
        return res;
    }
```
**Python:**
```
    def reorderedPowerOf2(self, N):
        c = collections.Counter(str(N))
        return any(c == collections.Counter(str(1 << i)) for i in xrange(30))
```


**Python 1-line**
suggested by @urashima9616, bests 80%
```
    def reorderedPowerOf2(self, N):
        return sorted(str(N)) in [sorted(str(1 << i)) for i in range(30)]
```
</p>


### Simple Java Solution Based on String Sorting
- Author: trotro
- Creation Date: Sat Jul 21 2018 07:27:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 21 2018 07:27:49 GMT+0800 (Singapore Standard Time)

<p>
The idea here is similar to that of group Anagrams problem (Leetcode #49). 

First, we convert the input number (N) into a string and sort the string. Next, we get the digits that form the power of 2 (by using 1 << i and vary i), convert them into a string, and then sort them. As we convert the powers of 2 (and there are only 31 that are <= 10^9), for each power of 2, we compare if the string is equal to that of string based on N. If the two strings are equal, then we return true.

```
class Solution {
    public boolean reorderedPowerOf2(int N) {
        char[] a1 = String.valueOf(N).toCharArray();
        Arrays.sort(a1);
        String s1 = new String(a1);
        
        for (int i = 0; i < 31; i++) {
            char[] a2 = String.valueOf((int)(1 << i)).toCharArray();
            Arrays.sort(a2);
            String s2 = new String(a2);
            if (s1.equals(s2)) return true;
        }
        
        return false;
    }
}
```
</p>


### JAVA Naive Backtracking 15 lines
- Author: caraxin
- Creation Date: Sun Jul 15 2018 11:01:41 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 11 2018 17:38:32 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public boolean reorderedPowerOf2(int N) {
        char[] ca=(N+"").toCharArray();
        return helper(ca, 0, new boolean[ca.length]);
    }
    public boolean helper(char[] ca, int cur, boolean[] used){
        if (cur!=0 && (cur+"").length()==ca.length){
            if (Integer.bitCount(cur)==1) return true;
            return false;
        }
        for (int i=0; i<ca.length; i++){
            if (used[i]) continue;
            used[i]=true;
            if (helper(ca, cur*10+ca[i]-\'0\', used)) return true;
            used[i]=false;
        }
        return false;
    }
}
```
It would be faster if you use memo to prune.
```
class Solution {
    public boolean reorderedPowerOf2(int N) {
        char[] ca=(N+"").toCharArray();
        return helper(ca, 0, new boolean[ca.length], new HashSet<Integer>());
    }
    public boolean helper(char[] ca, int cur, boolean[] used, HashSet<Integer> vis){
        if (!vis.add(cur)) return false;
        if (cur!=0 && (cur+"").length()==ca.length){
            if (Integer.bitCount(cur)==1) return true;
            return false;
        }
        for (int i=0; i<ca.length; i++){
            if (used[i]) continue;
            used[i]=true;
            if (helper(ca, cur*10+ca[i]-\'0\', used, vis)) return true;
            used[i]=false;
        }
        return false;
    }
}
```
</p>


