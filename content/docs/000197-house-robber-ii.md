---
title: "House Robber II"
weight: 197
#id: "house-robber-ii"
---
## Description
<div class="description">
<p>You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed. All houses at this place are <strong>arranged in a circle.</strong> That means the first house is the neighbor of the last one. Meanwhile, adjacent houses have a security system connected, and&nbsp;<b>it will automatically contact the police if two adjacent houses were broken into on the same night</b>.</p>

<p>Given a list of non-negative integers <code>nums</code> representing the amount of money of each house, return <em>the maximum amount of money you can rob tonight <strong>without alerting the police</strong></em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [2,3,2]
<strong>Output:</strong> 3
<strong>Explanation:</strong> You cannot rob house 1 (money = 2) and then rob house 3 (money = 2), because they are adjacent houses.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,1]
<strong>Output:</strong> 4
<strong>Explanation:</strong> Rob house 1 (money = 1) and then rob house 3 (money = 3).
Total amount you can rob = 1 + 3 = 4.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [0]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 100</code></li>
	<li><code>0 &lt;= nums[i] &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Google - 6 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

This problem is a minor extension to the original [House Robber Problem](https://leetcode.com/problems/house-robber/). The only difference is that the first and the last houses are adjacent to each other and therefore, if the thief has robbed the first house, they cannot steal the last house and vice versa. As Hint 1 in the question suggests, "the problem becomes to rob either `House[1]-House[n-1]` or `House[2]-House[n]`, depending on which choice offers more money. Now the problem has degenerated to the House Robber".

---

#### Approach 1: Dynamic Programming

**Intuition**

Assume we have `nums` of `[7,4,1,9,3,8,6,5]` as shown in the figure.
Since the start house and last house are adjacent to each other, if the thief decides to rob the start house `7`, they cannot rob the last house `5`. Similarly, if they select last house `5`, they have to start from a house with value `4`. Therefore, the final solution that we are looking for is to take the maximum value the thief can rob between houses of list `[7,4,1,9,3,8,6]` and `[4,1,9,3,8,6,5]`. For each of the lists, all we need to do is to figure the maximum value the thief can get using the approach in the original [House Robber Problem](https://leetcode.com/problems/house-robber/).

![alt text](../Figures/213/213_house_robberII_approach1_slide01.png "Figure")

**Solving Original House Robber Problem with Dynamic Programming**

Trivial cases:

- If there is one house, the answer is the value of that house.
- If there are two houses, the answer is `max(house1, house2)`.
- If there are three houses, you can either pick the middle house or the sum of the first and the last house. Therefore, it boils down to `max(house3 + house1, house2)`.

To make the example more illustrative, imagine two thieves (`t1` and `t2`) coordinating a grand robbery. They are equipped with walkie-talkies to communicate the values of houses to each other.

- Before entering any of the houses, both `t1` and `t2` have values of zero.

- `t1`, enters the first house and record the value of the house. If that is the only house to rob, they can rob this house and be done with it.
- If there is more than one house, `t1` will leave a note of maximum value reaped until this point (which is just the value of the first house) and move to the next house while `t2` moves into the house `t1` was in. Now, `t1` and `t2` are going to communicate over the walkie-talkie to ask who has the most value. At this point, `t2` will read the note left by `t1` when the values are compared. If they have only two houses to rob, they would rob the house with the most value and be done with it.
- If there are three houses, `t1` will leave a note of the maximum value reaped until this point and move to the next house. Then `t1` will compare the value of the sum of the current house and the house which `t2` is in with the value of the house `t1` was in. The maximum value between those two will be chosen and `t2` will move into the house next to it.
- If there are four houses, `t1` will leave a note of the maximum value reaped until this point and move to the next house. Then `t1` will compare the value of the sum of the current house and the house which `t2` is in with the value of the house `t1` was in. The maximum value between those two will be chosen and `t2` will move into the house next to it.
- This procedure is done over and over again as long as there are houses in `nums`. If `t1` has reached to the end of `nums`, `t1` should have reaped the maximum amount obtainable from houses in `nums`.


!?!../Documents/213_house_robber_ii.json:1200,600!?!

**Implementation**

<iframe src="https://leetcode.com/playground/ckv4f4Qy/shared" frameBorder="0" width="100%" height="500" name="ckv4f4Qy"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N)$$ where $$N$$ is the size of `nums`. We are accumulating results as we are scanning `nums`.

* Space complexity : $$O(1)$$ since we are not consuming additional space other than variables for two previous results and a temporary variable to hold one of the previous results.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple AC solution in Java in O(n) with explanation
- Author: lx223
- Creation Date: Wed May 20 2015 23:51:15 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 13:45:01 GMT+0800 (Singapore Standard Time)

<p>
Since this question is a follow-up to House Robber, we can assume we already have a way to solve the simpler question, i.e. given a 1 row of house, we know how to rob them. So we already have such a helper function. We modify it a bit to rob a given range of houses.
    
    private int rob(int[] num, int lo, int hi) {
        int include = 0, exclude = 0;
        for (int j = lo; j <= hi; j++) {
            int i = include, e = exclude;
            include = e + num[j];
            exclude = Math.max(e, i);
        }
        return Math.max(include, exclude);
    }

Now the question is how to rob a circular row of houses. It is a bit complicated to solve like the simpler question. It is because in the simpler question whether to rob *num[lo]* is entirely our choice. But, it is now constrained by whether *num[hi]* is robbed. 

However, since we already have a nice solution to the simpler problem. We do not want to throw it away. Then, it becomes how can we reduce this problem to the simpler one. Actually, extending from the logic that if house i is not robbed, then you are free to choose whether to rob house i + 1, you can break the circle by assuming a house is not robbed.

For example, 1 -> 2 -> 3 -> 1 becomes 2 -> 3 if 1 is not robbed.

Since every house is either robbed or not robbed and at least half of the houses are not robbed, the solution is simply the larger of two cases with consecutive houses, i.e. house i not robbed, break the circle, solve it, or house i + 1 not robbed. Hence, the following solution. I chose i = n and i + 1 = 0 for simpler coding. But, you can choose whichever two consecutive ones.

    public int rob(int[] nums) {
        if (nums.length == 1) return nums[0];
        return Math.max(rob(nums, 0, nums.length - 2), rob(nums, 1, nums.length - 1));
    }
</p>


### 9-lines 0ms O(1)-Space C++ solution
- Author: jianchao-li
- Creation Date: Fri May 22 2015 12:59:03 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 12:40:17 GMT+0800 (Singapore Standard Time)

<p>
This problem is a little tricky at first glance. However, if you have finished the **House Robber** problem, this problem can simply  be **decomposed into two House Robber problems**. 
    Suppose there are `n` houses, since house `0` and `n - 1` are now neighbors, we cannot rob them together and thus the solution is now the maximum of

 1. Rob houses `0` to `n - 2`;
 2. Rob houses `1` to `n - 1`.

The code is as follows. Some edge cases (`n < 2`) are handled explicitly.

    class Solution {
    public:
        int rob(vector<int>& nums) {
            int n = nums.size(); 
            if (n < 2) return n ? nums[0] : 0;
            return max(robber(nums, 0, n - 2), robber(nums, 1, n - 1));
        }
    private:
        int robber(vector<int>& nums, int l, int r) {
            int pre = 0, cur = 0;
            for (int i = l; i <= r; i++) {
                int temp = max(pre + nums[i], cur);
                pre = cur;
                cur = temp;
            }
            return cur;
        }
    };
</p>


### [C++] Super Simple 0ms solution with explanation
- Author: macoberry
- Creation Date: Thu Aug 06 2015 10:23:48 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 06 2015 10:23:48 GMT+0800 (Singapore Standard Time)

<p>
Since you cannot rob both the first and last house, just create two separate vectors, one excluding the first house, and another excluding the last house. The best solution generated from these two vectors using the original House Robber DP algorithm is the optimal one. 

    class Solution {
    public:
    
        int robOriginal(vector<int>& nums) {
            int a = 0, b = 0, res = 0;
            
            for(int i = 0; i < nums.size(); ++i){
                res = max(b + nums[i], a);
                b = a;
                a = res;
            }
            
            return res;
        }
    
        int rob(vector<int>& nums) {
            if(nums.empty()) return 0;
            if(nums.size() == 1) return nums[0];
            
            vector<int> numsA(nums.begin() + 1, nums.end());
            vector<int> numsB(nums.begin(), nums.end()-1);
            
            return max(robOriginal(numsA), robOriginal(numsB));
        }
    };
</p>


