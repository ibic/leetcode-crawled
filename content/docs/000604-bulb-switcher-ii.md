---
title: "Bulb Switcher II"
weight: 604
#id: "bulb-switcher-ii"
---
## Description
<div class="description">
<p>There is a room with <code>n</code> lights which are turned on initially and 4 buttons on the wall. After performing exactly <code>m</code> unknown operations towards buttons, you need to return how many different kinds of status of the <code>n</code> lights could be.</p>

<p>Suppose <code>n</code> lights are labeled as number [1, 2, 3 ..., n], function of these 4 buttons are given below:</p>

<ol>
	<li>Flip all the lights.</li>
	<li>Flip lights with even numbers.</li>
	<li>Flip lights with odd numbers.</li>
	<li>Flip lights with (3k + 1) numbers, k = 0, 1, 2, ...</li>
</ol>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> n = 1, m = 1.
<b>Output:</b> 2
<b>Explanation:</b> Status can be: [on], [off]
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> n = 2, m = 1.
<b>Output:</b> 3
<b>Explanation:</b> Status can be: [on, off], [off, on], [off, off]
</pre>

<p>&nbsp;</p>

<p><b>Example 3:</b></p>

<pre>
<b>Input:</b> n = 3, m = 1.
<b>Output:</b> 4
<b>Explanation:</b> Status can be: [off, on, off], [on, off, on], [off, off, off], [off, on, on].
</pre>

<p>&nbsp;</p>

<p><b>Note:</b> <code>n</code> and <code>m</code> both fit in range [0, 1000].</p>

</div>

## Tags
- Math (math)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Reduce Search Space [Accepted]

**Intuition**

As the search space is very large ($$2^N$$ states of lights, naively $$4^M$$ operation sequences), let us try to reduce it.

The first 6 lights uniquely determine the rest of the lights.  This is because every operation that modifies the $$x$$-th light also modifies the $$(x+6)$$-th light.

Also, operations commute: doing operation A followed by B is the same as doing operation B followed by A.  So we can assume we do all the operations in order.

Finally, doing the same operation twice in a row is the same as doing nothing.  So we only need to consider whether each operation was done 0 or 1 times.

**Algorithm**

Say we do the $$i$$-th operation $$f_i$$ times.  Let's first figure out what sets of residues are possible: that is, what sets $$c_i = f_i$$ ($$\mod 2$$ ) are possible.

Because $$c_i \equiv f_i$$ and $$c_i \leq f_i$$, if $$\sum f_i \not\equiv \sum c_i$$, or if $$\sum f_i < \sum c_i$$, it isn't possible.  Otherwise, it is possible by a simple construction: do the operations specified by $$c_i$$, then do operation number 1 with the even number of operations you have left.

For each possible set of residues, let's simulate and remember what the first 6 lights will look like, storing it in a *Set* structure `seen`.  At the end, we'll return the size of this set.

In Java, we make use of bit manipulations to manage the state of lights, where in Python we simulate it directly.

<iframe src="https://leetcode.com/playground/A4ToaE4r/shared" frameBorder="0" width="100%" height="378" name="A4ToaE4r"></iframe>


**Complexity Analysis**

* Time Complexity: $$O(1)$$.  Our checks are bounded by a constant.

* Space Complexity: $$O(1)$$, the size of the data structures used.

---
#### Approach #2: Mathematical [Accepted]

**Intuition and Algorithm**

As before, the first 6 lights uniquely determine the rest of the lights.  This is because every operation that modifies the $$x$$-th light also modifies the $$(x+6)$$-th light, so the $$x$$-th light is always equal to the $$(x+6)$$-th light.

Actually, the first 3 lights uniquely determine the rest of the sequence, as shown by the table below for performing the operations a, b, c, d:

* Light 1 = 1 + a + c + d
* Light 2 = 1 + a + b
* Light 3 = 1 + a + c
* Light 4 = 1 + a + b + d
* Light 5 = 1 + a + c
* Light 6 = 1 + a + b

So that (modulo 2):

* Light 4 = (Light 1) + (Light 2) + (Light 3)
* Light 5 = Light 3
* Light 6 = Light 2

The above justifies taking $$n = min(n, 3)$$ without loss of generality.  The rest is now casework.

Let's denote the state of lights by the tuple $$(a, b, c)$$.  The transitions are to XOR by $$(1, 1, 1), (0, 1, 0), (1, 0, 1),$$ or $$(1, 0, 0)$$.

When $$m = 0$$, all the lights are on, and there is only one state $$(1, 1, 1)$$.  The answer in this case is always 1.

When $$m = 1$$, we could get states $$(0, 0, 0)$$, $$(1, 0, 1)$$, $$(0, 1, 0)$$, or $$(0, 1, 1)$$.  The answer in this case is either $$2, 3, 4$$ for $$n = 1, 2, 3$$ respectively.

When $$m = 2$$, we can manually check that we can get 7 states: all of them except for $$(0, 1, 1)$$.  The answer in this case is either $$2, 4, 7$$ for $$n = 1, 2, 3$$ respectively.

When $$m = 3$$, we can get all 8 states.  The answer in this case is either $$2, 4, 8$$ for $$n = 1, 2, 3$$ respectively.

<iframe src="https://leetcode.com/playground/eMEAtHKj/shared" frameBorder="0" width="100%" height="208" name="eMEAtHKj"></iframe>

**Complexity Analysis**

* Time and Space Complexity: $$O(1)$$.  The entire program uses constants.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(1)
- Author: woshifumingyuan
- Creation Date: Sun Sep 03 2017 11:06:28 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 21:30:54 GMT+0800 (Singapore Standard Time)

<p>
We only need to consider special cases which n<=2 and m < 3. When n >2 and m >=3, the result is 8. 
The four buttons:

1. Flip all the lights.
2. Flip lights with even numbers.
3. Flip lights with odd numbers.
4. Flip lights with (3k + 1) numbers, k = 0, 1, 2, ...


If we use button 1 and 2, it equals to use button 3.
Similarly...

`1 + 2 --> 3,    1 + 3 --> 2,      2 + 3 --> 1`
So, there are only 8 cases. 

`All_on`, `1`, `2`, `3`, `4`, `1+4`, `2+4`, `3+4`

And we can get all the cases, when n>2 and m>=3.
```java
class Solution {
    public int flipLights(int n, int m) {
        if(m==0) return 1;
        if(n==1) return 2;
        if(n==2&&m==1) return 3;
        if(n==2) return 4;
        if(m==1) return 4;
        if(m==2) return 7;
        if(m>=3) return 8;
        return 8;
    }
}
```
</p>


### C++, concise code, O(1)
- Author: zestypanda
- Creation Date: Sun Sep 03 2017 11:00:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 21:01:43 GMT+0800 (Singapore Standard Time)

<p>
When n <= 1, the solution is trial.

From the 4 types of operations, 
``` 
1. Flip all the lights.
2. Flip lights with even numbers.
3. Flip lights with odd numbers.
4. Flip lights with (3k + 1) numbers, k = 0, 1, 2, ...
```
There are three important observations:
1) For any operation, only odd or even matters, i.e. 0 or 1. Two same operations equal no operation.
2) The first 3 operations can be reduced to 1 or 0 operation. For example, flip all + flip even = flip odd. So the result of the first 3 operations is the same as either 1 operation or original.   
3) The solution for n > 3 is the same as n = 3. 
For example, 1 0 0 ....., I use 0 and 1 to represent off and on.
The state of 2nd digit indicates even flip; The state of 3rd digit indicates odd flip; And the state difference of 1st and 3rd digits indicates 3k+1 flip.

In summary, the question can be simplified as m <= 3, n <= 3. I am sure you can figure out the rest easily. 
```
class Solution {
public:
    int flipLights(int n, int m) {
        if (m == 0 || n == 0) return 1;
        if (n == 1) return 2;
        if (n == 2) return m == 1? 3:4;
        if (m == 1) return 4;
        return m == 2? 7:8;
    }
};
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Sep 03 2017 11:53:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 03 2017 11:53:02 GMT+0800 (Singapore Standard Time)

<p>
Suppose we did `f[0]` of the first operation, `f[1]` of the second, `f[2]` of the third, and `f[3]` of the fourth, where `sum(f) == m`.

First, all these operations commute: doing operation A followed by operation B yields the same result as doing operation B followed by operation A.  Also, doing operation A followed by operation A again is the same as doing nothing.  So really, we only needed to know the residues `cand[i] = f[i] % 2`.  There are only 16 different possibilities for the residues in total, so we can try them all.

We'll loop `cand` through all 16 possibilities `(0, 0, 0, 0), (0, 0, 0, 1), ..., (1, 1, 1, 1)`.  A necessary and sufficient condition for `cand` to be valid is that `sum(cand) % 2 == m % 2 and sum(cand) <= m`, as only when these conditions are satisfied can we find some `f` with `sum(f) == m` and `cand[i] = f[i] % 2`.

Also, as the sequence of lights definitely repeats every 6 lights, we could replace `n` with `min(n, 6)`. Actually, we could replace it with `min(n, 3)`, as those lights are representative: that is, knowing the first 3 lights is enough to reconstruct what the next 3 lights will be.  If the first 3 lights are X, Y, Z, then with a little effort we can prove the next 3 lights will be (X^Y^Z), Z, Y.

```python
def flipLights(self, n, m):
    seen = set()
    for cand in itertools.product((0, 1), repeat = 4):
        if sum(cand) % 2 == m % 2 and sum(cand) <= m:
            A = []
            for i in xrange(min(n, 3)):
                light = 1
                light ^= cand[0]
                light ^= cand[1] and i % 2
                light ^= cand[2] and i % 2 == 0
                light ^= cand[3] and i % 3 == 0
                A.append(light)
            seen.add(tuple(A))

    return len(seen)
```
</p>


