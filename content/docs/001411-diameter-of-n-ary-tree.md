---
title: "Diameter of N-Ary Tree"
weight: 1411
#id: "diameter-of-n-ary-tree"
---
## Description
<div class="description">
<p>Given a&nbsp;<code>root</code>&nbsp;of an <a href="https://leetcode.com/articles/introduction-to-n-ary-trees/">N-ary tree</a>,&nbsp;you need to compute the length of the diameter of the tree.</p>

<p>The diameter of an N-ary tree is the length of the&nbsp;<strong>longest</strong>&nbsp;path between any two nodes in the&nbsp;tree. This path may or may not pass through the root.</p>

<p>(<em>Nary-Tree input serialization&nbsp;is represented in their level order traversal, each group of children is separated by the null value.)</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/07/19/sample_2_1897.png" style="width: 324px; height: 173px;" /></p>

<pre>
<strong>Input:</strong> root = [1,null,3,2,4,null,5,6]
<strong>Output:</strong> 3
<strong>Explanation: </strong>Diameter is shown in red color.</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/07/19/sample_1_1897.png" style="width: 253px; height: 246px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [1,null,2,null,3,4,null,5,null,6]
<strong>Output:</strong> 4
</pre>

<p><strong>Example 3:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/07/19/sample_3_1897.png" style="width: 369px; height: 326px;" /></p>

<pre>
<strong>Input:</strong> root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
<strong>Output:</strong> 7
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The depth of the n-ary tree is less than or equal to <code>1000</code>.</li>
	<li>The total number of nodes is between <code>[0,&nbsp;10^4]</code>.</li>
</ul>
</div>

## Tags


## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ #miminalizm
- Author: votrubac
- Creation Date: Sun Jul 26 2020 01:58:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 01:58:36 GMT+0800 (Singapore Standard Time)

<p>
```cpp
int max_dia = 0;
int diameter(Node* r, bool isRoot = true) {
    int max_depth = 0;
    for (auto ch : r->children) {
        auto depth = 1 + diameter(ch, false);
        max_dia = max(max_dia, max_depth + depth);
        max_depth = max(max_depth, depth);
    }
    return isRoot ? max_dia : max_depth;
}
```
</p>


### [Java] Simpler and Cleaner DFS recursion
- Author: ziyao
- Creation Date: Thu Jul 23 2020 13:14:50 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 23 2020 13:14:50 GMT+0800 (Singapore Standard Time)

<p>
DFS through each node and calculate height of each node. 

```
class Solution {
    int res = 0;
    public int diameter(Node root) {
        getHeight(root);
        return res;
    }
    
    public int getHeight(Node root) {
        if(root == null)
            return 0;
        
        int max1 = 0;
        int max2 = 0;
        for(Node child : root.children) {
            int height = getHeight(child);
            if(height > max1) {
                max2 = max1;
                max1 = height;
            }
            else if(height > max2) {
                max2 = height;
            }
        }
        
        res = Math.max(res, max1+max2);
        return max1+1;
    }
}
```
</p>


### Python3 recursion O(n) with explanation beat 98%
- Author: GodfreyLin
- Creation Date: Sun Jul 26 2020 02:02:40 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 29 2020 10:47:37 GMT+0800 (Singapore Standard Time)

<p>
Same concept as 543. Diameter of Binary Tree. 
Traverse the tree and record the depths current node to children leaf. 
Keep the first and second maximum depth and compare to the diameter.
base case [0,0] is for leaf
```
class Solution:
    def diameter(self, root: \'Node\') -> int:
        self.diameter = 0
        self.dfs(root)
        return self.diameter
    
    def dfs(self, root):
        first = second = 0  # base case for leaf, first store the maximum depth, second is second maximum depth
        for neighbor in root.children:
            depth = self.dfs(neighbor)
            if depth > first:
                first, second = depth, first
            elif depth > second:
                second = depth
        self.diameter = max(self.diameter, first + second)
        return first + 1
```
</p>


