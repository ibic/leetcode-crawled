---
title: "Longest Subarray of 1's After Deleting One Element"
weight: 1357
#id: "longest-subarray-of-1s-after-deleting-one-element"
---
## Description
<div class="description">
<p>Given a binary array <code>nums</code>, you should delete one element from it.</p>

<p>Return the size of the longest non-empty subarray containing only 1&#39;s&nbsp;in the resulting array.</p>

<p>Return 0 if there is no such subarray.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,1,0,1]
<strong>Output:</strong> 3
<strong>Explanation: </strong>After deleting the number in position 2, [1,1,1] contains 3 numbers with value of 1&#39;s.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,1,1,1,0,1,1,0,1]
<strong>Output:</strong> 5
<strong>Explanation: </strong>After deleting the number in position 4, [0,1,1,1,1,1,0,1] longest subarray with value of 1&#39;s is [1,1,1,1,1].</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,1,1]
<strong>Output:</strong> 2
<strong>Explanation: </strong>You must delete one element.</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,1,0,0,1,1,1,0,1]
<strong>Output:</strong> 4
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,0,0]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10^5</code></li>
	<li><code>nums[i]</code>&nbsp;is either&nbsp;<code>0</code>&nbsp;or&nbsp;<code>1</code>.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Yandex - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Sliding Window, at most one 0
- Author: lee215
- Creation Date: Sun Jun 28 2020 00:03:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 00:03:01 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Almost exactly same as 1004. [Max Consecutive Ones III](https://leetcode.com/problems/max-consecutive-ones-iii/discuss/247564/JavaC%2B%2BPython-Sliding-Window)
Given an array A of 0s and 1s,
we may change up to k = 1 values from 0 to 1.
Return the **length - 1** of the longest (contiguous) subarray that contains only 1s.

I felt we discuss the same sliding window solution every two weeks..
And I continue to receive complaints that I didn\'t give explantion again and again.
<br>

# **Complexity**
Time `O(N)`
Space `O(1)`
<br>

# Solution 1:
Sliding window with at most one 0 inside.
**Java**
```java
    public int longestSubarray(int[] A) {
        int i = 0, j, k = 1, res = 0;
        for (j = 0; j < A.length; ++j) {
            if (A[j] == 0) {
                k--;
            }
            while (k < 0) {
                if (A[i] == 0) {
                    k++;
                }
                i++;
            }
            res = Math.max(res, j - i);
        }
        return res;
    }
```
<br>

# Solution 2
Sliding window that size doesn\'t shrink.
**Java:**
```java
    public int longestSubarray(int[] A) {
        int i = 0, j, k = 1;
        for (j = 0; j < A.length; ++j) {
            if (A[j] == 0) k--;
            if (k < 0 && A[i++] == 0) k++;
        }
        return j - i - 1;
    }
```

**C++:**
```cpp
    int longestSubarray(vector<int>& A) {
        int i = 0, j, k = 1;
        for (j = 0; j < A.size(); ++j) {
            if (A[j] == 0) k--;
            if (k < 0 && A[i++] == 0) k++;
        }
        return j - i - 1;
    }
```

**Python:**
```py
    def longestSubarray(self, A):
        k = 1
        i = 0
        for j in xrange(len(A)):
            k -= A[j] == 0
            if k < 0:
                k += A[i] == 0
                i += 1
        return j - i
```
<br>

# More Similar Sliding Window Problems
Here are some similar sliding window problems.
Also find more explanations and discussion.
Good luck and have fun.

- 5434. Longest Subarray of 1\'s After Deleting One Element
- 1425. [Constrained Subsequence Sum](https://leetcode.com/problems/constrained-subsequence-sum/discuss/597751/JavaC++Python-O(N)-Decreasing-Deque)
- 1358. [Number of Substrings Containing All Three Characters](https://leetcode.com/problems/number-of-substrings-containing-all-three-characters/discuss/516977/JavaC++Python-Easy-and-Concise)
- 1248. [Count Number of Nice Subarrays](https://leetcode.com/problems/count-number-of-nice-subarrays/discuss/419378/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
- 1234. [Replace the Substring for Balanced String](https://leetcode.com/problems/replace-the-substring-for-balanced-string/discuss/408978/javacpython-sliding-window/367697)
- 1004. [Max Consecutive Ones III](https://leetcode.com/problems/max-consecutive-ones-iii/discuss/247564/JavaC%2B%2BPython-Sliding-Window)
-  930. [Binary Subarrays With Sum](https://leetcode.com/problems/binary-subarrays-with-sum/discuss/186683/)
-  992. [Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/discuss/523136/JavaC%2B%2BPython-Sliding-Window)
-  904. [Fruit Into Baskets](https://leetcode.com/problems/fruit-into-baskets/discuss/170740/Sliding-Window-for-K-Elements)
-  862. [Shortest Subarray with Sum at Least K](https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/discuss/143726/C%2B%2BJavaPython-O(N)-Using-Deque)
-  209. [Minimum Size Subarray Sum](https://leetcode.com/problems/minimum-size-subarray-sum/discuss/433123/JavaC++Python-Sliding-Window)
<br>
</p>


### [Python] O(n) dynamic programming, detailed explanation
- Author: DBabichev
- Creation Date: Sun Jun 28 2020 00:02:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 00:02:50 GMT+0800 (Singapore Standard Time)

<p>
One way to deal this problem is **dynamic programming**. Let `dp[i][0]` be the length of longest subarray of ones, such that it ends at point `i`. Let `dp[i][1]` be the length longest subarray, which has one **zero** and ends at point `i`. Let us traverse through our `nums` and update our `dp` table, we have two options:

0. `dp[0][0] = 1` if `nums[0] = 1` and `0` in opposite case.
1. If `nums[i] = 1`, than to update `dp[i][0]` we just need to look into `dp[i-1][0]` and add one, the same true is for `dp[i][1]`.
2. If `nums[i] = 0`, than `dp[i][0] = 0`, we need to interrupt our sequence without zeroes. `dp[i][1] = dp[i-1][0]`, because we we added `0` and number of `1` is still `i-1`. 

Let us go through example `[1,0,1,1,0,0,1,1,0,1,1,1]`:
then we have the following `dp` table:

| 1     | 0     | 1     | 2     | 0     | 0     | 1     | 2     | 0     | 1     | 2     | 3     |
|-------|-------|-------|-------|-------|-------|-------|-------|-------|-------|-------|-------|
| **0** | **1** | **2** | **3** | **2** | **0** | **1** | **2** | **2** | **3** | **4** | **5** |


**Complexity**: time and space is `O(n)`, because we iterate over our table `dp` once. Space can be reduced to `O(1)` because we only look into the previous column of our table.

**Extension and follow-up**. Note, that this solution can be easily extended for the case, when we need to delete `k` elements instead of one, with time complexity `O(nk)` and space complexity `O(k)`.

**Other solutions**. This problem can be solved with sliding window technique as well, or we can evaluate sizes of groups of `0`\'s and `1`\'s and then choose two group of `1`\'s, such that it has group of `0`\'s with only one element between them. 


```
class Solution:
    def longestSubarray(self, nums):
        n = len(nums)
        if sum(nums) == n: return n - 1
        
        dp = [[0] * 2 for _ in range(n)]
        dp[0][0] = nums[0]
        
        for i in range(1, n):
            if nums[i] == 1:
                dp[i][0], dp[i][1] = dp[i-1][0] + 1, dp[i-1][1] + 1
            else:
                dp[i][0], dp[i][1] = 0, dp[i-1][0]
        
        return max([i for j in dp for i in j])  
```

If you have any questions, feel free to ask. If you like solution and explanations, please **Upvote!**
</p>


### [java/Python 3] Sliding Window with at most one zero w/ detailed explanation and brief analysis.
- Author: rock
- Creation Date: Sun Jun 28 2020 00:10:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 23:27:42 GMT+0800 (Singapore Standard Time)

<p>
Two methods.

----
**Method 1:**
1. Use `i` and `j` as the lower and upper bounds, both inclusively;
2. Loop through the `nums` by upper bound, accumulate current element on the `sum`, then check if `sum` is less than `j - i` (which **implies the window includes at least 2 `0`s**) : if yes, keep increase the lower bound `i` till either the sliding window has at most `1` zero or lower bound is same as upper bound;

**Core concept here:** - credit to **@lionkingeatapple and @abhinav_7**.
`sum` count the number of `1`s in the window.
`j - i` means `the length of the window - 1`. Since we need to delete one element which is zero from this window, we use `j - i` not `j - i + 1`.
Specifically, if 
```
sum == (j - i) + 1
```
then all `1`\'s exist in the window`(i - j)`; if 
```
sum == (j - i)
```
one zero exist; if 
```
sum < (j - i)
```
more than one zeros exist!

With those two conditions above, we can know if the number of ones in the window is less than the length of the window minus `1`, then we have least two zeros in this window. In other words, we want to find a window that only contains a single zero and the rest of the elements are all ones. Let me give an example below.
```
     window = [1, 1, 1, 0, 0] 
     index =   0, 1, 2, 3, 4
```
The `sum = 3` when `i = 0, j = 3`. We have a qualified window, and `j - i = 3 - 0 = 3` means we have only one zero. When `i = 0, j = 4`, and `j - i = 4 - 0 = 4` means we have two zeros, so we need to move our left bound `i` of the window.

3. Update the max value of the sliding window.

```java
    public int longestSubarray(int[] nums) {
        int ans = 0;
        for (int i = 0, j = 0, sum = 0; j < nums.length; ++j) {
            sum += nums[j];
            while (i < j && sum < j - i)  {
                sum -= nums[i++];
            }
            ans = Math.max(ans, j - i);
        }
        return ans;
    }
```
```python
    def longestSubarray(self, nums: List[int]) -> int:
        ans = sum = lo = 0
        for hi, num in enumerate(nums):
            sum += num
            while lo < hi and sum < hi - lo:
                sum -= nums[lo]
                lo += 1
            ans = max(ans, hi - lo)
        return ans    
```

----

**Method 2: minor optimization**, similar to method 1 but the window will not shrink.

```java
    public int longestSubarray(int[] nums) {
        int ans = 0;
        for (int i = 0, j = 0, sum = 0; j < nums.length; ++j) {
            sum += nums[j];
            if (sum < j - i)  {
                sum -= nums[i++];
            }
            ans = Math.max(ans, j - i);
        }
        return ans;
    }
```
```python
    def longestSubarray(self, nums: List[int]) -> int:
        ans = sum = lo = 0
        for hi, num in enumerate(nums):
            sum += num
            if sum < hi - lo:
                sum -= nums[lo]
                lo += 1
            ans = max(ans, hi - lo)
        return ans
```

**Analysis:**

Time: O(n), space: O(1).

----
Please refer to the following for similar problems:

[209. Minimum Size Subarray Sum](https://leetcode.com/problems/minimum-size-subarray-sum/description/)
[862. Shortest Subarray with Sum at Least K](https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/description/)
[904. Fruit Into Baskets](https://leetcode.com/problems/fruit-into-baskets/description/)
[930. Binary Subarrays With Sum](https://leetcode.com/problems/binary-subarrays-with-sum/description/)
[992. Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/description/)
[1004. Max Consecutive Ones III](https://leetcode.com/problems/max-consecutive-ones-iii/description/)
[1234. Replace the Substring for Balanced String](https://leetcode.com/problems/replace-the-substring-for-balanced-string/description/)
[1248. Count Number of Nice Subarrays](https://leetcode.com/problems/count-number-of-nice-subarrays/description/)
[1358. Number of Substrings Containing All Three Characters](https://leetcode.com/problems/number-of-substrings-containing-all-three-characters/description/)
[1425. Constrained Subsequence Sum](https://leetcode.com/problems/constrained-subsequence-sum/description/)

----

Do let me know if you can make the codes better, and **Upvote** if they are helpful for you.
</p>


