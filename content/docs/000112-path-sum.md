---
title: "Path Sum"
weight: 112
#id: "path-sum"
---
## Description
<div class="description">
<p>Given a binary tree and a sum, determine if the tree has a root-to-leaf path such that adding up all the values along the path equals the given sum.</p>

<p><strong>Note:</strong>&nbsp;A leaf is a node with no children.</p>

<p><strong>Example:</strong></p>

<p>Given the below binary tree and <code>sum = 22</code>,</p>

<pre>
      <strong>5</strong>
     <strong>/</strong> \
    <strong>4</strong>   8
   <strong>/</strong>   / \
  <strong>11</strong>  13  4
 /  <strong>\</strong>      \
7    <strong>2</strong>      1
</pre>

<p>return true, as there exist a root-to-leaf path <code>5-&gt;4-&gt;11-&gt;2</code> which sum is 22.</p>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Bloomberg - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)
- Oracle - 2 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Zillow - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Binary tree definition

First of all, here is the definition of the ```TreeNode``` which we would use
in the following implementation.

<iframe src="https://leetcode.com/playground/zH5qGFyW/shared" frameBorder="0" width="100%" height="225" name="zH5qGFyW"></iframe>
<br />
<br />


---
#### Approach 1: Recursion

The most intuitive way is to use a recursion here.
One is going through the tree 
by considering at each step the node itself and its children.
If node *is not* a leaf, one calls recursively `hasPathSum` method 
for its children with a sum decreased by the current node value.
If node *is* a leaf, one checks if the the current sum is zero, *i.e* 
if the initial sum was discovered.

<iframe src="https://leetcode.com/playground/hQenht6s/shared" frameBorder="0" width="100%" height="293" name="hQenht6s"></iframe>

**Complexity Analysis**

* Time complexity : we visit each node exactly once, 
thus the time complexity is $$\mathcal{O}(N)$$,
where $$N$$ is the number of nodes.
* Space complexity : in the worst case, the tree is completely unbalanced,
*e.g.* each node has only one child node, the recursion call would occur
 $$N$$ times (the height of the tree), therefore the storage to keep the call stack would be $$\mathcal{O}(N)$$.
 But in the best case (the tree is completely balanced), the height of the tree would be $$\log(N)$$.
 Therefore, the space complexity in this case would be $$\mathcal{O}(\log(N))$$.
<br />
<br />


---
#### Approach 2: Iterations

**Algorithm**

We could also convert the above recursion into iteration, 
with the help of stack. DFS would be better than BFS here since 
it works faster except the worst case. In the worst case the path `root->leaf` 
with the given sum is the last considered one and in this case DFS results in
the same productivity as BFS. 

>The idea is to visit each node with the DFS strategy,
while updating the remaining sum to cumulate at each visit.

So we start from a stack which contains the root node and the corresponding 
remaining sum which is ```sum - root.val```.
Then we proceed to the iterations: pop the current node out of the stack 
and return ```True``` if the remaining sum is `0` and we're on the leaf node.
If the remaining sum is not zero or we're not on the leaf yet 
then we push the child nodes 
and corresponding remaining sums into stack.  

<!--![LIS](../Figures/112/112_tr.gif)-->
!?!../Documents/112_LIS.json:1000,543!?!

<iframe src="https://leetcode.com/playground/r2vVLbWq/shared" frameBorder="0" width="100%" height="500" name="r2vVLbWq"></iframe>

**Complexity Analysis**

* Time complexity : the same as the recursion approach $$\mathcal{O}(N)$$.
* Space complexity : $$\mathcal{O}(N)$$ since in the worst case, when the tree is completely unbalanced,
*e.g.* each node has only one child node, we would keep all $$N$$ nodes in the stack.
 But in the best case (the tree is balanced), the height of the tree would be $$\log(N)$$.
 Therefore, the space complexity in this case would be $$\mathcal{O}(\log(N))$$.

## Accepted Submission (java)
```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        int reminder = sum - root.val;
        if (root.left == null && root.right == null) {
            return reminder == 0;
        }
        boolean leftHas = false;
        boolean rightHas = false;
        if (root.left != null) {
            leftHas = hasPathSum(root.left, reminder);
        }
        if (root.right != null) {
            rightHas = hasPathSum(root.right, reminder);
        }
        return leftHas || rightHas;
    }
}

```

## Top Discussions
### [Accepted]My recursive solution in Java
- Author: boy27910230
- Creation Date: Fri Sep 05 2014 22:40:54 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 02:37:28 GMT+0800 (Singapore Standard Time)

<p>
The basic idea is to subtract the value of current node from sum until it reaches a leaf node and the subtraction equals 0, then we know that we got a hit. Otherwise the subtraction at the end could not be 0.

    public class Solution {
        public boolean hasPathSum(TreeNode root, int sum) {
            if(root == null) return false;
        
            if(root.left == null && root.right == null && sum - root.val == 0) return true;
        
            return hasPathSum(root.left, sum - root.val) || hasPathSum(root.right, sum - root.val);
        }
    }
</p>


### Short Python recursive solution - O(n)
- Author: Google
- Creation Date: Sat Mar 21 2015 14:33:10 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 00:18:36 GMT+0800 (Singapore Standard Time)

<p>
    # Definition for a  binary tree node
    # class TreeNode:
    #     def __init__(self, x):
    #         self.val = x
    #         self.left = None
    #         self.right = None
    
    class Solution:
        # @param root, a tree node
        # @param sum, an integer
        # @return a boolean
        # 1:27
        def hasPathSum(self, root, sum):
            if not root:
                return False
    
            if not root.left and not root.right and root.val == sum:
                return True
            
            sum -= root.val
    
            return self.hasPathSum(root.left, sum) or self.hasPathSum(root.right, sum)
</p>


### 3 lines of  c++ solution
- Author: pankit
- Creation Date: Fri Mar 06 2015 21:40:43 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 01:44:43 GMT+0800 (Singapore Standard Time)

<p>
    bool hasPathSum(TreeNode *root, int sum) {
            if (root == NULL) return false;
            if (root->val == sum && root->left ==  NULL && root->right == NULL) return true;
            return hasPathSum(root->left, sum-root->val) || hasPathSum(root->right, sum-root->val);
        }
</p>


