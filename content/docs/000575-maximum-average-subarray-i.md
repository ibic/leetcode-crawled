---
title: "Maximum Average Subarray I"
weight: 575
#id: "maximum-average-subarray-i"
---
## Description
<div class="description">
<p>Given an array consisting of <code>n</code> integers, find the contiguous subarray of given length <code>k</code> that has the maximum average value. And you need to output the maximum average value.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> [1,12,-5,-6,50,3], k = 4
<b>Output:</b> 12.75
<b>Explanation:</b> Maximum average is (12-5-6+50)/4 = 51/4 = 12.75
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>1 &lt;= <code>k</code> &lt;= <code>n</code> &lt;= 30,000.</li>
	<li>Elements of the given array will be in the range [-10,000, 10,000].</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags
- Array (array)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

 
## Solution

---
#### Approach #1 Cumulative Sum [Accepted]

**Algorithm**

We know that in order to obtain the averages of subarrays with length $$k$$, we need to obtain the sum of these $$k$$ length subarrays. One of the methods of obtaining this sum is to make use of a cumulative sum array, $$sum$$, which is populated only once. Here, $$sum[i]$$ is used to store the sum of the elements of the given $$nums$$ array from the first element upto the element at the $$i^{th}$$ index.

Once the $$sum$$ array has been filled up, in order to find the sum of elements from the index $$i$$ to $$i+k$$, all we need to do is to use: $$sum[i] - sum[i-k]$$. Thus, now, by doing one more iteration over the $$sum$$ array, we can determine the maximum average possible from the subarrays of length $$k$$.

The following animation illustrates the process for a simple example.

!?!../Documents/643_Maximum_Average.json:1000,563!?!

<iframe src="https://leetcode.com/playground/dJyoFWQo/shared" frameBorder="0" name="dJyoFWQo" width="100%" height="275"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We iterate over the $$nums$$ array of length $$n$$ once to fill the $$sum$$ array. Then, we iterate over $$n-k$$ elements of $$sum$$ to determine the required result.

* Space complexity : $$O(n)$$. We make use of a $$sum$$ array of length $$n$$ to store the cumulative sum.

---


#### Approach #2 Sliding Window [Accepted]

**Algorithm**

Instead of creating a cumulative sum array first, and then traversing over it to determine the required sum, we can simply traverse over $$nums$$ just once, and on the go keep on determining the sums possible for the subarrays of length $$k$$. To understand the idea, assume that we already know the sum of elements from index $$i$$ to index $$i+k$$, say it is $$x$$.

Now, to determine the sum of elements from the index $$i+1$$ to the index $$i+k+1$$, all we need to do is to subtract the element $$nums[i]$$ from $$x$$ and to add the element $$nums[i+k+1]$$ to $$x$$. We can carry out our process based on this idea and determine the maximum possible average.

<iframe src="https://leetcode.com/playground/uABxt2Z8/shared" frameBorder="0" name="uABxt2Z8" width="100%" height="292"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We iterate over the given $$nums$$ array of length $$n$$ once only.

* Space complexity : $$O(1)$$. Constant extra space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution, Sum of Sliding window
- Author: shawngao
- Creation Date: Sun Jul 16 2017 23:11:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 16 2017 23:11:49 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public double findMaxAverage(int[] nums, int k) {
        long sum = 0;
        for (int i = 0; i < k; i++) sum += nums[i];
        long max = sum;
        
        for (int i = k; i < nums.length; i++) {
            sum += nums[i] - nums[i - k];
            max = Math.max(max, sum);
        }
        
        return max / 1.0 / k;
    }
}
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Jul 16 2017 11:16:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 16 2017 11:16:37 GMT+0800 (Singapore Standard Time)

<p>
We want to find the maximum K-length sum.  After, we can divide by K to get the average.
We have two techniques for getting these sums efficiently: prefix sums, or sliding window.

In the first approach, we calculate ```P[i] = A[0] + A[1] + ... + A[i-1]``` in linear time.  Then, ```A[i] + A[i+1] + ... + A[i+K-1] = P[i+K] - P[i]```, and we should find the max of these.

```
def findMaxAverage(self, A, K):
    P = [0]
    for x in A:
        P.append(P[-1] + x)

    ma = max(P[i+K] - P[i] 
             for i in xrange(len(A) - K + 1))
    return ma / float(K)
```

<hr></hr>

In the second approach, we maintain ```su = the sum of A[i-K+1] + A[i-K+2] + ... + A[i]```.  Then, when we have K elements in this sum (```if i >= K-1```), it is a candidate to be the maximum sum ```ma```.

```
def findMaxAverage(self, A, K):
    su = 0
    ma = float('-inf')
    for i, x in enumerate(A):
        su += x
        if i >= K:
            su -= A[i-K]
        if i >= K - 1:
            ma = max(ma, su)
    return ma / float(K)
```
</p>


### C++ simple sliding-window solution
- Author: MichaelZ
- Creation Date: Sun Jul 16 2017 20:33:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 16 2017 20:33:21 GMT+0800 (Singapore Standard Time)

<p>
```
    double findMaxAverage(vector<int>& nums, int k) {
        double sum=0, res=INT_MIN;
        for(int i=0;i<nums.size();i++) {
            if(i<k) sum+=nums[i];
            else {
                res=max(sum, res);
                sum+=nums[i]-nums[i-k];
            }
        }
        res=max(sum, res);
        return res/k;
    }
</p>


