---
title: "Count Submatrices With All Ones"
weight: 1378
#id: "count-submatrices-with-all-ones"
---
## Description
<div class="description">
<p>Given a&nbsp;<code>rows * columns</code>&nbsp;matrix <code>mat</code> of ones and zeros, return how many&nbsp;<strong>submatrices</strong> have all ones.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> mat = [[1,0,1],
&nbsp;             [1,1,0],
&nbsp;             [1,1,0]]
<strong>Output:</strong> 13
<strong>Explanation:
</strong>There are <b>6</b> rectangles of side 1x1.
There are <b>2</b> rectangles of side 1x2.
There are <b>3</b> rectangles of side 2x1.
There is <b>1</b> rectangle of side 2x2. 
There is <b>1</b> rectangle of side 3x1.
Total number of rectangles = 6 + 2 + 3 + 1 + 1 = <strong>13.</strong>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> mat = [[0,1,1,0],
&nbsp;             [0,1,1,1],
&nbsp;             [1,1,1,0]]
<strong>Output:</strong> 24
<strong>Explanation:</strong>
There are <b>8</b> rectangles of side 1x1.
There are <b>5</b> rectangles of side 1x2.
There are <b>2</b> rectangles of side 1x3. 
There are <b>4</b> rectangles of side 2x1.
There are <b>2</b> rectangles of side 2x2. 
There are <b>2</b> rectangles of side 3x1. 
There is <b>1</b> rectangle of side 3x2. 
Total number of rectangles = 8 + 5 + 2 + 4 + 2 + 2 + 1 = 24<strong>.</strong>
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> mat = [[1,1,1,1,1,1]]
<strong>Output:</strong> 21
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> mat = [[1,0,1],[0,1,0],[1,0,1]]
<strong>Output:</strong> 5
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= rows&nbsp;&lt;= 150</code></li>
	<li><code>1 &lt;= columns&nbsp;&lt;= 150</code></li>
	<li><code>0 &lt;= mat[i][j] &lt;= 1</code></li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 5 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Detailed Explanation - From O(MNM) to O(MN) by using Stack
- Author: frankkkkk
- Creation Date: Sun Jul 05 2020 12:35:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 05 2020 14:47:52 GMT+0800 (Singapore Standard Time)

<p>
**O(M * N * M):**
Imagine you have an one-dimension array, how to count number of all 1 submetrics (size is 1 * X). It\'s pretty simple right?
```java
int res = 0, length = 0;
for (int i = 0; i < A.length; ++i) {
	length = (A[i] == 0 ? 0 : length + 1);
	res += length;
}
return res;
```
Now, Let\'s solve 2D metrics by finding all 1 submetrics from row "up" to row "down". And apply above 1D helper function. Note: the array h[k] == 1 means **all values in column k from row "up" to "down" are 1 (that\'s why we use &)**. So overall, the idea is to "compress" the 2D array to the 1D array, and apply 1D array method on it, while trying all heights up to down.
```java
public int numSubmat(int[][] mat) {
        
	int M = mat.length, N = mat[0].length;

	int res = 0;
	for (int up = 0; up < M; ++up) {
		int[] h = new int[N];
		Arrays.fill(h, 1);
		for (int down = up; down < M; ++down) {
			for (int k = 0; k < N; ++k) h[k] &= mat[down][k];
			res += countOneRow(h);
		}
	}

	return res;
}

private int countOneRow(int[] A) {

	int res = 0, length = 0;
	for (int i = 0; i < A.length; ++i) {
		length = (A[i] == 0 ? 0 : length + 1);
		res += length;
	}
	return res;
}
```

**O(M * N) by Using Stack**
Now in the code, the h[j] means: number of continius 1 in column j from row i up to row 0. By using mono-stack, what we want to achieve is to find the first previous index "preIndex", whose number of continuous 1 is less than current column index i. And the value of index between  preIndex and i are all equal or larger than index i. So it can form a big sub-metrics.

Note: sum[i] means the number of submatrices with the column "i" as the right border.
- If stack is empty, meaning: all previous columns has more/equal ones than current column. So, the number of metrics can form is simply A[i] * (i + 1); (0-index)
- If stack is not empty, meaning: there is a shorter column which breaks our road. Now, the number of metrics can form is  sum[i] += A[i] * (i - preIndex). And plus, we can form a longer submetrics with that previou shorter column sum[preIndex]. 

The best way to understand is to draw a graph.
![image](https://assets.leetcode.com/users/images/9f722cb2-f223-4acf-9883-74fa020fef17_1593925126.526302.png)


```java
public int numSubmat(int[][] mat) {
        
	int M = mat.length, N = mat[0].length;

	int res = 0;

	int[] h = new int[N];
	for (int i = 0; i < M; ++i) {
		for (int j = 0; j < N; ++j) {
			h[j] = (mat[i][j] == 0 ? 0 : h[j] + 1);
		}
		res += helper(h);
	}

	return res;
}

private int helper(int[] A) {

	int[] sum = new int[A.length];
	Stack<Integer> stack = new Stack<>();

	for (int i = 0; i < A.length; ++i) {

		while (!stack.isEmpty() && A[stack.peek()] >= A[i]) stack.pop();

		if (!stack.isEmpty()) {
			int preIndex = stack.peek();
			sum[i] = sum[preIndex];
			sum[i] += A[i] * (i - preIndex);
		} else {
			sum[i] = A[i] * (i + 1);
		}

		stack.push(i);
	}

	int res = 0;
	for (int s : sum) res += s;

	return res;
}
```
</p>


### [C++] Understand the brute force solution first!
- Author: rsdavis6
- Creation Date: Mon Jul 06 2020 00:16:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 12 2020 00:21:28 GMT+0800 (Singapore Standard Time)

<p>
This problem is tricky because there are very similar problems that can be solved easily with dp, but here we need to take a different approach.

Even the brute force solution with `O(m^2 x n^2)` takes some work. This solution passes all the tests and its a good place to start.

At every position within the matrix, we count all of the submatrices that have a top-left corner at that position.
I use the `bound` to "shrink" the search space because once we hit a zero, there is no point in iterating past that point.

```cpp
    // count all submatrices with top-left corner at mat[a][b]
    int helper(vector<vector<int>> & mat, int a, int b) {
        
        int m = mat.size();
        int n = mat[0].size();
        int count = 0;
        int bound = n;
        
        for (int i=a; i<m; i++) {
            for (int j=b; j<bound; j++) {
                if (mat[i][j]) count += 1;
                else bound = j;
            }
        }
        
        return count;

    }

    int numSubmat(vector<vector<int>>& mat) {
        
        int m = mat.size();
        int n = mat[0].size();
        int count = 0;
        
        for (int i=0; i<m; i++) {
            for (int j=0; j<n; j++) {
                count += helper(mat, i, j);
            }
        }
        
        return count;

    }
```
</p>


### Java histogram count
- Author: hobiter
- Creation Date: Sun Jul 05 2020 12:41:52 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 25 2020 07:26:55 GMT+0800 (Singapore Standard Time)

<p>
loop each node, where count all the rectangular that use this node as bottome-right corner: 
*treat all 1s in the left or above as [histogram](https://leetcode.com/problems/largest-rectangle-in-histogram/), 
*counting towards left and just keep decreasing the histogram height;
*keep adding the height to result;
*height[i][j] means the current height of histogram with mat[i][j] as bottom;

version 1, time O(m * n * n), space O(n); 
```
    public int numSubmat(int[][] mat) {
        int m = mat.length, n = mat[0].length, height[] = new int[n], res = 0; 
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                height[j] = mat[i][j] == 0 ? 0 : height[j] + 1;   //  height of histogram;
                for (int k = j, min = height[j]; k >= 0 && min > 0; k--) {
                    min = Math.min(min, height[k]);
                    res += min;
                }
            }
        }
        return res;
    }
```

Follow up: for each row, maintain a monotonic increasing Stack.
optimiazed orig O(M*N * N) time complexity to O(M * N), space(O(N)
Version 2:
```
    public int numSubmat(int[][] mat) {
        int m = mat.length, n = mat[0].length, height[] = new int[n], res = 0; 
        for (int i = 0; i < m; i++) {
            Stack<int[]> st = new Stack<>();
            for (int j = 0; j < n; j++) {
                height[j] = mat[i][j] == 0 ? 0 : (height[j] + 1);   //  height of histogram;
                int sum = 0;
                while(!st.isEmpty() && height[st.peek()[0]] >= height[j]) st.pop();
                if (!st.isEmpty()) sum += height[j] * (j - st.peek()[0]) + st.peek()[1];
                else sum += height[j] * (j + 1);
                st.push(new int[]{j, sum});
                res += sum;
            }
        }
        return res;
    }
```
</p>


