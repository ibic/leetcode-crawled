---
title: "Brace Expansion"
weight: 985
#id: "brace-expansion"
---
## Description
<div class="description">
<p>A string <code>S</code>&nbsp;represents a list of words.</p>

<p>Each letter in the word has 1 or more options.&nbsp; If there is one option, the letter is represented as is.&nbsp; If there is more than one option, then curly braces delimit the options.&nbsp; For example, <code>&quot;{a,b,c}&quot;</code> represents options <code>[&quot;a&quot;, &quot;b&quot;, &quot;c&quot;]</code>.</p>

<p>For example, <code>&quot;{a,b,c}d{e,f}&quot;</code> represents the list <code>[&quot;ade&quot;, &quot;adf&quot;, &quot;bde&quot;, &quot;bdf&quot;, &quot;cde&quot;, &quot;cdf&quot;]</code>.</p>

<p>Return all words that can be formed in this manner, in lexicographical order.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;{a,b}c{d,e}f&quot;</span>
<strong>Output: </strong><span id="example-output-1">[&quot;acdf&quot;,&quot;acef&quot;,&quot;bcdf&quot;,&quot;bcef&quot;]</span>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;abcd&quot;</span>
<strong>Output: </strong><span id="example-output-2">[&quot;abcd&quot;]</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= S.length &lt;= 50</code></li>
	<li>There are no&nbsp;nested curly brackets.</li>
	<li>All characters inside a pair of&nbsp;consecutive opening and ending curly brackets are different.</li>
</ol>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Google - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python backtracking
- Author: kaien
- Creation Date: Sun Jun 16 2019 14:04:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 16 2019 14:04:11 GMT+0800 (Singapore Standard Time)

<p>
```python
class Solution(object):
    def expand(self, S):
        """
        :type S: str
        :rtype: List[str]
        """
        self.res = []
        def helper(s, word):
            if not s:
                self.res.append(word)
            else:
                if s[0] == "{":
                    i = s.find("}")
                    for letter in s[1:i].split(\',\'):
                        helper(s[i+1:], word+letter)
                else:
                    helper(s[1:], word + s[0])
        helper(S, "")
        self.res.sort()
        return self.res
```
</p>


### [Python] 3-line Using Product
- Author: lee215
- Creation Date: Tue Jun 18 2019 15:09:42 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 09 2019 15:06:20 GMT+0800 (Singapore Standard Time)

<p>
```
    def permute(self, S):
        A = S.replace(\'{\', \' \').replace(\'}\', \' \').strip().split(\' \')
        B = [sorted(a.split(\',\')) for a in A]
        return [\'\'.join(c) for c in itertools.product(*B)]
```
</p>


### Java DFS
- Author: wushangzhen
- Creation Date: Sun Jun 16 2019 00:13:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Nov 09 2019 14:13:49 GMT+0800 (Singapore Standard Time)

<p>
Simliar to Word Break
```
class Solution {
    public String[] expand(String S) {
		// TreeSet to sort
        TreeSet<String> set = new TreeSet<>();
        if (S.length() == 0) {
            return new String[]{""}; 
        } else if (S.length() == 1) {
            return new String[]{S};
        }
        if (S.charAt(0) == \'{\') {
            int i = 0; // keep track of content in the "{content}"
            while (S.charAt(i) != \'}\') {
                i++;
            }
            String sub = S.substring(1, i);
            String[] subs = sub.split(",");
            String[] strs = expand(S.substring(i + 1)); // dfs
            for (int j = 0; j < subs.length; j++) {
                for (String str : strs) {
                    set.add(subs[j] + str);
                }
            }
        } else {
            String[] strs = expand(S.substring(1));
            for (String str : strs) {
                set.add(S.charAt(0) + str);
            }
        }
        return set.toArray(new String[0]);
    }
}
```
Without TreeSet

```
class Solution {
    public String[] expand(String S) {
        int n = S.length();
        if (n == 0) {
            return new String[]{""};
        }
        if (n == 1) {
            return new String[]{S};
        }
        List<String> res = new ArrayList<>();
        if (S.charAt(0) == \'{\') {
            int count = 0;
            int i = 0;
            while (i < S.length()) {
                if (S.charAt(i) == \'}\') {
                    break;
                }
                i++;
            }
            String[] l = S.substring(1, i).split(",");
            String[] r = expand(S.substring(i + 1));
            for (String ll : l) {
                for (String rr : r) {
                    res.add(ll + rr);
                }
            }
        } else {
            String[] r = expand(S.substring(1));
            for (String rr : r) {
                res.add(S.charAt(0) + rr);
            }
        }
        Collections.sort(res);
        return res.toArray(new String[0]);
    }
}
```
</p>


