---
title: "Final Prices With a Special Discount in a Shop"
weight: 1343
#id: "final-prices-with-a-special-discount-in-a-shop"
---
## Description
<div class="description">
<p>Given the array <code>prices</code> where <code>prices[i]</code> is the price of the <code>ith</code> item in a shop. There is a special discount for items in the shop, if you buy the <code>ith</code> item, then you will receive a discount equivalent to <code>prices[j]</code> where <code>j</code> is the <strong>minimum</strong>&nbsp;index such that <code>j &gt; i</code> and <code>prices[j] &lt;= prices[i]</code>, otherwise, you will not receive any discount at all.</p>

<p><em>Return an array where the <code>ith</code> element is the final price you will pay for the <code>ith</code> item of the shop considering the special discount.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> prices = [8,4,6,2,3]
<strong>Output:</strong> [4,2,4,2,3]
<strong>Explanation:</strong>&nbsp;
For item 0 with price[0]=8 you will receive a discount equivalent to prices[1]=4, therefore, the final price you will pay is 8 - 4 = 4.&nbsp;
For item 1 with price[1]=4 you will receive a discount equivalent to prices[3]=2, therefore, the final price you will pay is 4 - 2 = 2.&nbsp;
For item 2 with price[2]=6 you will receive a discount equivalent to prices[3]=2, therefore, the final price you will pay is 6 - 2 = 4.&nbsp;
For items 3 and 4 you will not receive any discount at all.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> prices = [1,2,3,4,5]
<strong>Output:</strong> [1,2,3,4,5]
<strong>Explanation:</strong> In this case, for all items, you will not receive any discount at all.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> prices = [10,1,1,6]
<strong>Output:</strong> [9,0,1,6]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= prices.length &lt;= 500</code></li>
	<li><code>1 &lt;= prices[i] &lt;= 10^3</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Dream11 - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Stack, One Pass
- Author: lee215
- Creation Date: Sun Jun 14 2020 00:06:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 14 2020 00:07:56 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Similar to the problem [503. Next Greater Element II](https://leetcode.com/problems/next-greater-element-ii/discuss/98270/JavaC++Python-Loop-Twice)
<br>

## **Complexity**
Time `O(N)`
Space `O(N)`
<br>

**Java:**
```java
    public int[] finalPrices(int[] A) {
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < A.length; i++) {
            while (!stack.isEmpty() && A[stack.peek()] >= A[i])
                A[stack.pop()] -= A[i];
            stack.push(i);
        }
        return A;
    }
```

**C++:**
```cpp
    vector<int> finalPrices(vector<int>& A) {
        vector<int> stack;
        for (int i = 0; i < A.size(); ++i) {
            while (stack.size() && A[stack.back()] >= A[i]) {
                A[stack.back()] -= A[i];
                stack.pop_back();
            }
            stack.push_back(i);
        }
        return A;
    }
```

**Python:**
```py
    def finalPrices(self, A):
        stack = []
        for i, a in enumerate(A):
            while stack and A[stack[-1]] >= a:
                A[stack.pop()] -= a
            stack.append(i)
        return A
```

</p>


### [Python 3]1 pass O(n) code using stack w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Jun 14 2020 01:17:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 14 2020 01:46:37 GMT+0800 (Singapore Standard Time)

<p>
**I noticed that I got downvotes without any critics on the first post, so I put the Python 3 code part into this second post and hope at least one can survive these downvotes storm.**

In short, use a  stack to maintain the indices of strickly increasing prices.

1. Clone the `prices` array as original price before discount;
2. Use a stack to hold the indices of the previous prices that are less than current price;
3. Keep poping out the prices that are NO less than current price, deduct current price as discount from previous prices.

```python
    def finalPrices(self, prices: List[int]) -> List[int]:
        res, stack = prices[:], []
        for i, price in enumerate(prices):
            while stack and prices[stack[-1]] >= price:
                res[stack.pop()] -= price
            stack.append(i)
        return res

```

**Analysis:**

Time & space: O(n).

----

For similar problems, please refer to:

[503. Next Greater Element II](https://leetcode.com/problems/next-greater-element-ii/description/)
[739. Daily Temperatures](https://leetcode.com/problems/daily-temperatures/description/)
[1019. Next Greater Node In Linked List](https://leetcode.com/problems/next-greater-node-in-linked-list/discuss/267163/Java-9-liner-O(n)-using-Stack-and-ArrayList)


</p>


### [Java] O(n) 1 pass code using stack w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Jun 14 2020 00:01:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 14 2020 01:59:15 GMT+0800 (Singapore Standard Time)

<p>
For similar problems, please refer to:

[503. Next Greater Element II](https://leetcode.com/problems/next-greater-element-ii/description/)
[739. Daily Temperatures](https://leetcode.com/problems/daily-temperatures/description/)
[1019. Next Greater Node In Linked List](https://leetcode.com/problems/next-greater-node-in-linked-list/discuss/267163/Java-9-liner-O(n)-using-Stack-and-ArrayList)

----
In short, use a  stack to maintain the indices of strickly increasing prices.

1. Clone the `prices` array as original price before discount;
2. Use a stack to hold the indices of the previous prices that are less than current price;
3. Keep poping out the prices that are NO less than current price, deduct current price as discount from previous prices.

```java
    public int[] finalPrices(int[] prices) {
        int[] res = prices.clone();
        Deque<Integer> stack = new ArrayDeque<>();
        for (int i = 0; i < prices.length; ++i) {
            while (!stack.isEmpty() && prices[stack.peek()] >= prices[i]) {
                res[stack.pop()] -= prices[i];
            }
            stack.push(i);
        }
        return res;        
    }
```
**Analysis:**

Time & space: O(n).
</p>


