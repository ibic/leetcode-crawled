---
title: "Sales Person"
weight: 1500
#id: "sales-person"
---
## Description
<div class="description">
<p><b>Description</b></p>

<p>Given three tables: <code>salesperson</code>, <code>company</code>, <code>orders</code>.<br />
Output all the <b>names</b> in the table <code>salesperson</code>, who didn&rsquo;t have sales to company &#39;RED&#39;.</p>

<p><b>Example</b><br />
<b>Input</b></p>

<p>Table: <code>salesperson</code></p>

<pre>
+----------+------+--------+-----------------+-----------+
| sales_id | name | salary | commission_rate | hire_date |
+----------+------+--------+-----------------+-----------+
|   1      | John | 100000 |     6           | 4/1/2006  |
|   2      | Amy  | 120000 |     5           | 5/1/2010  |
|   3      | Mark | 65000  |     12          | 12/25/2008|
|   4      | Pam  | 25000  |     25          | 1/1/2005  |
|   5      | Alex | 50000  |     10          | 2/3/2007  |
+----------+------+--------+-----------------+-----------+
</pre>
The table <code>salesperson</code> holds the salesperson information. Every salesperson has a <b>sales_id</b> and a <b>name</b>.

<p>Table: <code>company</code></p>

<pre>
+---------+--------+------------+
| com_id  |  name  |    city    |
+---------+--------+------------+
|   1     |  RED   |   Boston   |
|   2     | ORANGE |   New York |
|   3     | YELLOW |   Boston   |
|   4     | GREEN  |   Austin   |
+---------+--------+------------+
</pre>
The table <code>company</code> holds the company information. Every company has a <b>com_id</b> and a <b>name</b>.

<p>Table: <code>orders</code></p>

<pre>
+----------+------------+---------+----------+--------+
| order_id | order_date | com_id  | sales_id | amount |
+----------+------------+---------+----------+--------+
| 1        |   1/1/2014 |    3    |    4     | 100000 |
| 2        |   2/1/2014 |    4    |    5     | 5000   |
| 3        |   3/1/2014 |    1    |    1     | 50000  |
| 4        |   4/1/2014 |    1    |    4     | 25000  |
+----------+----------+---------+----------+--------+
</pre>
The table <code>orders</code> holds the sales record information, salesperson and customer company are represented by <b>sales_id</b> and <b>com_id</b>.

<p><b>output</b></p>

<pre>
+------+
| name | 
+------+
| Amy  | 
| Mark | 
| Alex |
+------+
</pre>

<p><b>Explanation</b></p>

<p>According to order &#39;3&#39; and &#39;4&#39; in table <code>orders</code>, it is easy to tell only salesperson &#39;John&#39; and &#39;Pam&#39; have sales to company &#39;RED&#39;,<br />
so we need to output all the other <b>names</b> in the table <code>salesperson</code>.</p>

</div>

## Tags


## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach: Using `OUTER JOIN` and `NOT IN` [Accepted]

**Intuition**

If we know all the persons who have sales in this company 'RED', it will be fairly easy to know who do not have.

**Algorithm**

To start, we can query the information of sales in company 'RED' as a temporary table. And then try to build a connection between this table and the **salesperson** table since it has the name information.

```sql
SELECT
    *
FROM
    orders o
        LEFT JOIN
    company c ON o.com_id = c.com_id
WHERE
    c.name = 'RED'
;
```
>Note: "LEFT OUTER JOIN" could be written as "LEFT JOIN".

```
| order_id | date     | com_id | sales_id | amount | com_id | name | city   |
|----------|----------|--------|----------|--------|--------|------|--------|
| 3        | 3/1/2014 | 1      | 1        | 50000  | 1      | RED  | Boston |
| 4        | 4/1/2014 | 1      | 4        | 25000  | 1      | RED  | Boston |
```

Obviously, the column *sales_id* exists in table **salesperson** so we may use it as a subquery, and then utilize the [`NOT IN`](https://dev.mysql.com/doc/refman/5.7/en/any-in-some-subqueries.html) to get the target data.


**MySQL**

```sql
SELECT
    s.name
FROM
    salesperson s
WHERE
    s.sales_id NOT IN (SELECT
            o.sales_id
        FROM
            orders o
                LEFT JOIN
            company c ON o.com_id = c.com_id
        WHERE
            c.name = 'RED')
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### No subquery, simple select from where with right join:)
- Author: Joy4fun
- Creation Date: Thu Nov 02 2017 15:59:33 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 01:22:44 GMT+0800 (Singapore Standard Time)

<p>
    select salesperson.name
    from orders o join company c on (o.com_id = c.com_id and c.name = 'RED')
    right join salesperson on salesperson.sales_id = o.sales_id
    where o.sales_id is null
</p>


### The explanation is wrong
- Author: toulio84
- Creation Date: Sat Aug 03 2019 10:09:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 03 2019 10:09:26 GMT+0800 (Singapore Standard Time)

<p>
For someone who\'s confused as I was, there\'s a mistake in explanation below the description.
It says only John and Alex have sales to  company RED.
However, it is Pam instead of Alex has sales to company RED.


</p>


### Beats 98.8% of the solution using left join and not in 
- Author: ajmerasaloni
- Creation Date: Sun Sep 16 2018 08:49:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 16 2018 08:49:24 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT s.NAME 
FROM   salesperson s 
WHERE  sales_id NOT IN(SELECT sales_id 
                       FROM   orders o 
                              LEFT JOIN company c 
                                     ON o.com_id = c.com_id 
                       WHERE  c.NAME = \'RED\') 
```
</p>


