---
title: "K-diff Pairs in an Array"
weight: 501
#id: "k-diff-pairs-in-an-array"
---
## Description
<div class="description">
<p>Given an array of integers <code>nums</code> and an integer <code>k</code>, return <em>the number of <b>unique</b> k-diff pairs in the array</em>.</p>

<p>A <b>k-diff</b> pair is&nbsp;an integer pair <code>(nums[i], nums[j])</code>, where the following are true:</p>

<ul>
	<li><code>0 &lt;= i, j &lt; nums.length</code></li>
	<li><code>i != j</code></li>
	<li><code>|nums[i] - nums[j]| == k</code></li>
</ul>

<p><strong>Notice</strong>&nbsp;that <code>|val|</code> denotes the absolute value of <code>val</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,1,4,1,5], k = 2
<strong>Output:</strong> 2
<strong>Explanation:</strong> There are two 2-diff pairs in the array, (1, 3) and (3, 5).
Although we have two 1s in the input, we should only return the number of <strong>unique</strong> pairs.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4,5], k = 1
<strong>Output:</strong> 4
<strong>Explanation:</strong> There are four 1-diff pairs in the array, (1, 2), (2, 3), (3, 4) and (4, 5).
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,3,1,5,4], k = 0
<strong>Output:</strong> 1
<strong>Explanation:</strong> There is one 0-diff pair in the array, (1, 1).
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,4,4,3,3,0,9,2,3], k = 3
<strong>Output:</strong> 2
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> nums = [-1,-2,-3], k = 1
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10<sup>4</sup></code></li>
	<li><code>-10<sup>7</sup> &lt;= nums[i] &lt;= 10<sup>7</sup></code></li>
	<li><code>0 &lt;= k &lt;= 10<sup>7</sup></code></li>
</ul>

</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)

## Companies
- Goldman Sachs - 14 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Twitter - 4 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Salesforce - 6 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: true)
- Oracle - 2 (taggedByAdmin: false)
- Twilio - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

*Overview:* Approach 1 exhibits a naive way to tackle this problem by checking all possible pairs. Approach 2 improves the time complexity of approach 1 by using left and right pointers. Approach 3 uses Hashmap and is the fastest of all three approaches.

---

#### Approach 1: Brute Force

**Intuition**

The most naive way to tackle this problem is to sort the array and check every possible pair. We can have two loops, one loop fixing at one number while the other looping going over every number after that fixed number, to check every possible pair. One thing that we have to be aware of is to make sure that we don't repeatedly count the duplicate pairs. To do so, we will have to check whether the current number that we are looking at is the same as the previous number. If the current number is the same as the previous number, whether we are in the outer loop or the inner loop, we can just continue to the next number. 

If the difference between the pair that we are looking is the same as `k`, we increment our placeholder variable, `result`.

For `nums = [2,5,1,2,8,1,3,5,7,1]` and `k = 2`:

!?!../Documents/532_k-diff_pairs_in_an_array1.json:1200,600!?!

**Implementation**

<iframe src="https://leetcode.com/playground/eJjfkv7k/shared" frameBorder="0" width="100%" height="463" name="eJjfkv7k"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N^2)$$ where $$N$$ is the size of `nums`. The time complexity for sorting is $$O(N \log N)$$ while the time complexity for going through ever pair in the `nums` is $$O(N^2)$$. Therefore, the final time complexity is $$O(N \log N) + O(N^2) \approx O(N^2)$$.

* Space complexity : $$O(N)$$ where $$N$$ is the size of `nums`. This space complexity is incurred by the sorting algorithm. Space complexity is bound to change depending on the sorting algorithm you use. There is no additional space required for the part with two `for` loops, apart from a single variable `result`. Therefore, the final space complexity is $$O(N) + O(1) \approx O(N)$$.

*Addendum:* We can also approach this problem using brute force without sorting `nums`. First, we have to create a hash set which will record pairs of numbers whose difference is `k`. Then, we look for every possible pair. As soon as we find a pair (say `i` and `j`) whose difference is `k`, we add `(i, j)` and `(j, i)` to the hash set and increment our placeholder `result` variable. Whenever we encounter another pair which is already in the hash set, we simply ignore that pair. By doing so we have a better practical runtime (since we are eliminating the sorting step) even though the time complexity is still $$O(N^2)$$ where $$N$$ is the size of `nums`. 

---

#### Approach 2: Two Pointers

**Intuition**

We can do better than quadratic runtime in Approach 1. Rather than checking for every possible pair, we can have two pointers to point the left number and right number that should be checked in a sorted array.

First, we have to initialize the left pointer to point the first element and the right pointer to point the second element of `nums` array. The way we are going to move the pointers is as follows:

Take the difference between the numbers which left and right pointers point.
    
1. If it is less than `k`, we increment the right pointer.
    * If left and right pointers are pointing to the same number, we increment the right pointer too.
2. If it is greater than `k`, we increment the left pointer.
3. If it is exactly `k`, we have found our pair, we increment our placeholder `result` and increment left pointer.

The idea behind the behavior of incrementing left and right pointers in the manner above is similar to:
    
* Extending the range between left and right pointers when the difference between left and right pointers is less than `k` (i.e. the range is too small).
    * Therefore, we extend the range (by incrementing the right pointer) when left and right pointer are pointing to the same number.
* Contracting the range between left and right pointers when the difference between left and right pointers is more than `k` (i.e. the range is too large). 

This is the core of the idea but there is another issue which we have to take care of to make everything work correctly. We have to make sure duplicate pairs are not counted repeatedly. In order to do so, whenever we have a pair whose difference matches with `k`, we keep incrementing the left pointer as long as the incremented left pointer points to the number which is equal to the previous number.

For `nums = [2,5,1,2,8,1,3,5,7,1]` and `k = 2`:

!?!../Documents/532_k-diff_pairs_in_an_array2.json:1200,600!?!

**Implementation**


<iframe src="https://leetcode.com/playground/SttTap5B/shared" frameBorder="0" width="100%" height="500" name="SttTap5B"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N \log N)$$ where $$N$$ is the size of `nums`. The time complexity for sorting is $$O(N \log N)$$ while the time complexity for going through `nums` is $$O(N)$$. One might mistakenly think that it should be $$O(N^2)$$ since there is another `while` loop inside the first `while` loop. The `while` loop inside is just incrementing the pointer to skip numbers which are the same as the previous number. The animation should explain this behavior clearer. Therefore, the final time complexity is $$O(N \log N) + O(N) \approx O(N \log N)$$. 

* Space complexity : $$O(N)$$ where $$N$$ is the size of `nums`. Similar to approach 1, this space complexity is incurred by the sorting algorithm. Space complexity is bound to change depending on the sorting algorithm you use. There is no additional space required for the part where two pointers are being incremented, apart from a single variable `result`. Therefore, the final space complexity is $$O(N) + O(1) \approx O(N)$$.

---

#### Approach 3: Hashmap

**Intuition**

This method removes the need to sort the `nums` array. Rather than that, we will be building a frequency hash map. This hash map will have every unique number in `nums` as keys and the number of times each number shows up in `nums` as values.

For example:

    nums = [2,4,1,3,5,3,1], k = 3
    hash_map = {1: 2,
                2: 1,
                3: 2,
                4: 1,
                5: 1}

Next, we look at a key (let's call `x`) in the hash map and ask whether:

* There is a key in the hash map which is equal to `x+k` **IF** `k > 0`.
    * For example, if a number in `nums` is 1 `(x=1)` and `k` is 3, you would need to have 4 to satisfy this condition (thus, we need to look for `1+3 = 4` in the hash map). Using addition to look for a complement pair has the advantage of not double-counting the same pair, but in reverse order (i.e. if we have found a pair (1,4), we won't be counting (4,1)). 
* There is more than one occurrence of `x` **IF** `k = 0`.
    * For example, if we have `nums = [1,1,1,1]` and `k = 0`, we have one unique (1,1) pair. In this case, our hash map will be `{1: 4}`, and this condition is satisfied since we have more than one occurrence of number `1`. 

If we can satisfy either of the above conditions, we can increment our placeholder `result` variable.

Then we look at the next key in the hash map.

**Implementation**


<iframe src="https://leetcode.com/playground/VpGAXGST/shared" frameBorder="0" width="100%" height="446" name="VpGAXGST"></iframe>

**Complexity Analysis**

Let $$N$$ be the number of elements in the input list.

* Time complexity : $$O(N)$$.

    - It takes $$O(N)$$ to create an initial frequency hash map and another $$O(N)$$ to traverse the keys of that hash map. One thing to note about is the hash key lookup. The time complexity for hash key lookup is $$O(1)$$ but if there are hash key collisions, the time complexity will become $$O(N)$$. However those cases are rare and thus, the amortized time complexity is $$O(2N) \approx O(N)$$. 

* Space complexity : $$O(N)$$

    - We keep a table to count the frequency of each unique number in the input. In the worst case, all numbers are unique in the array.
    As a result, the maximum size of our table would be $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n) solution - one Hashmap, easy to understand
- Author: tankztc
- Creation Date: Sun Mar 05 2017 13:31:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 08:22:27 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public int findPairs(int[] nums, int k) {
        if (nums == null || nums.length == 0 || k < 0)   return 0;
        
        Map<Integer, Integer> map = new HashMap<>();
        int count = 0;
        for (int i : nums) {
            map.put(i, map.getOrDefault(i, 0) + 1);
        }
        
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (k == 0) {
                //count how many elements in the array that appear more than twice.
                if (entry.getValue() >= 2) {
                    count++;
                } 
            } else {
                if (map.containsKey(entry.getKey() + k)) {
                    count++;
                }
            }
        }
        
        return count;
    }
}
```
</p>


### [Java/Python] Easy Understood Solution
- Author: lee215
- Creation Date: Fri Mar 10 2017 21:34:45 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 18 2020 11:14:20 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
Count the elements with `Counter`
If `k > 0`, for each element `i`, check if `i + k` exist.
If `k == 0`, for each element `i`, check if `count[i] > 1`
<br>

# **Explanation**
Time `O(N)`
Space `O(N)`
<br>

**Python**
```py
def findPairs(self, nums, k):
        res = 0
        c = collections.Counter(nums)
        for i in c:
            if k > 0 and i + k in c or k == 0 and c[i] > 1:
                res += 1
        return res
```
which equals to:
```py
def findPairs(self, nums, k):
        c = collections.Counter(nums)
        return  sum(k > 0 and i + k in c or k == 0 and c[i] > 1 for i in c)
```

**Java**
By @blackspinner
```java
    public int findPairs(int[] nums, int k) {
        Map<Integer, Integer> cnt = new HashMap<>();
        for (int x : nums) {
            cnt.put(x, cnt.getOrDefault(x, 0) + 1);
        }
        int res = 0;
        for (int x : cnt.keySet()) {
            if ((k > 0 && cnt.containsKey(x + k)) || (k == 0 && cnt.get(x) > 1)) {
                res++;
            }
        }
        return res;
    }
```
</p>


### Two-pointer Approach
- Author: lixx2100
- Creation Date: Sun Mar 05 2017 15:47:25 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 12:42:07 GMT+0800 (Singapore Standard Time)

<p>
The problem is just a variant of 2-sum.
**Update:** Fixed a bug that can cause integer subtraction overflow.
**Update:** The code runs in `O(n log n)` time, using `O(1)` space.

```java
public int findPairs(int[] nums, int k) {
    int ans = 0;
    Arrays.sort(nums);
    for (int i = 0, j = 0; i < nums.length; i++) {
        for (j = Math.max(j, i + 1); j < nums.length && (long) nums[j] - nums[i] < k; j++) ;
        if (j < nums.length && (long) nums[j] - nums[i] == k) ans++;
        while (i + 1 < nums.length && nums[i] == nums[i + 1]) i++;
    }
    return ans;
}
```
</p>


