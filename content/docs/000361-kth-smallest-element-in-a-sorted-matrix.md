---
title: "Kth Smallest Element in a Sorted Matrix"
weight: 361
#id: "kth-smallest-element-in-a-sorted-matrix"
---
## Description
<div class="description">
<p>Given a <i>n</i> x <i>n</i> matrix where each of the rows and columns are sorted in ascending order, find the kth smallest element in the matrix.</p>

<p>
Note that it is the kth smallest element in the sorted order, not the kth distinct element.
</p>

<p><b>Example:</b>
<pre>
matrix = [
   [ 1,  5,  9],
   [10, 11, 13],
   [12, 13, 15]
],
k = 8,

return 13.
</pre>
</p>

<p><b>Note: </b><br>
You may assume k is always valid, 1 &le; k &le; n<sup>2</sup>.</p>
</div>

## Tags
- Binary Search (binary-search)
- Heap (heap)

## Companies
- Facebook - 14 (taggedByAdmin: false)
- Amazon - 10 (taggedByAdmin: false)
- Google - 6 (taggedByAdmin: true)
- Microsoft - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

#### Approach 1: Min-Heap approach

**Intuition**

The intuition for this approach is really simple. If you think about it, we can reframe the problem as finding the $$K^{\text{th}}$$ smallest elements from amongst `N sorted lists` right? We know that the rows are sorted and so are the columns. So, we can treat each row (or column) as a sorted list in itself. Then, the problem just boils down to finding the $$K^{\text{th}}$$ smallest element from amongst these `N` sorted lists. However, before we get to this problem, lets first talk about a simpler version of the problem which is to find the $$K^{\text{th}}$$ smallest element from amongst 2 sorted lists. This is easy enough to solve since all we need are a pair of pointers which act as indices in the two lists. 

At each step we check which element is smaller amongst the two being pointed at by the indices and progress the corresponding index accordingly. If you think about it, we just need to run the algorithm for merging two sorted lists without actually merging them. We need to keep on running this algorithm until we find our $$K^{\text{th}}$$ element. Let's quickly look at how this would look like diagrammatically.

<center>
<img src="../Figures/378/img1.png"/>
</center>

We can use this two-pointer approach for finding the $$K^{\text{th}}$$ element. As explained above, at each step we check the two values and move the pointer corresponding to the smaller value forward.

<center>
<img src="../Figures/378/img2.png"/>
</center>

Now that was simple enough to do. In this particular problem, we have $$N$$ sorted lists instead of just 2. That's what adds to the complexity. We can't really keep $$N$$ different pointers now, can we? The heap data structure is perfect for this problem since at all times, we want to maintain `N` different `variables` with each of them pointing to an element in their corresponding lists. We want to be able to find the `minimum` amongst these $$N$$ pointers quickly and then replace that element with the next one in its corresponding list. 

The heap data structure gives us $$O(1)$$ access to the minimum element and $$log(N)$$ removal of the minimum element and addition of a new one. We just need to perform this operation `K` times to get our $$K^{\text{th}}$$ smallest number. It's possible that our matrix has say 100 rows whereas we just need to find out the 5th smallest element. In this case, we can safely discard the rows in the lower part of the matrix i.e. in this case starting from the 6th row to the end of the matrix because the columns are sorted as well. So, we need the $$min(N, K)$$ rows essentially to find out the answer.

**Algorithm**

1. Initialize a min-heap called `H`.
2. For our implementation, we will be considering each row an individual list. Since the columns are sorted as well, we can easily treat each column as an individual list as well.
3. As mentioned before, we will take the first element of $$min(N, K)$$ rows where $$N$$ represents the number of rows, and add each of these elements to the heap. It's important to know what row and column an element belongs to. Without knowing that, we won't be able to move forward in that particular list. So, apart from adding an element to the heap, we also need to add its row and column number. Hence, our min-heap will contain a triplet of information `(value, row, column)`. The heap will be arranged on the basis of the values and we will use the row and column number to add a replacement for the next element in case it gets popped off the heap. 

    <center>
    <img src="../Figures/378/img3.png"/>
    </center>

4. At this point, our heap contains $$min(N, K)$$ elements. Now we start a loop that goes until we iterate over `K` elements.
5. At each step, we remove the minimum element from the heap. The element will tell us which row should be further consumed. Using the row and column information we will add the next element to the heap. Specifically, if the current minimum element was from row `r` and had a column position `c`, then the next element to be added to the heap will be `(r, c+1)`.

    `Extract-Min`
    
    <center>
    <img src="../Figures/378/img4.png"/>
    </center>

    `Add a new element`

    <center>
    <img src="../Figures/378/img5.png"/>
    </center>


6. Keep on iterating till we exhaust `K` elements. The last element to be popped at the end of the loop will be the $$K^{\text{th}}$$ smallest element.

<iframe src="https://leetcode.com/playground/zsr3NGuD/shared" frameBorder="0" width="100%" height="500" name="zsr3NGuD"></iframe>

**Complexity Analysis**

* Time Complexity: $$\text{let X=} \text{min}(K, N); X + K \log(X)$$

    * Well the heap construction takes $$O(X)$$ time.
    * After that, we perform $$K$$ iterations and each iteration has two operations. We extract the minimum element from a heap containing $$X$$ elements. Then we add a new element to this heap. Both the operations will take $$O(\log(X))$$ time. 
    * Thus, the total time complexity for this algorithm comes down to be $$O(X + K\log(X))$$ where $$X$$ is $$\text{min}(K, N)$$.

* Space Complexity: $$O(X)$$ which is occupied by the heap.
<br/>
<br/>

---
#### Approach 2: Binary Search

**Inuition**

Since each row and column of the matrix is sorted, is it possible to use Binary Search to find the $$K^{\text{th}}$$ smallest number? The biggest problem to use Binary Search in this case is that we don’t have a straightforward sorted array, instead we have a matrix. As we remember, in Binary Search, we calculate the middle index of the search space (`1` to `N`) and see if our required number is pointed out by the middle index; if not we either search in the lower half or the upper half. In a sorted matrix, we can't really find a middle. Even if we do consider some index as middle, it is not straightforward to find the search space containing numbers bigger or smaller than the number pointed out by the middle index.

An alternate could be to apply the Binary Search on the `number range` instead of the `index range`. As we know that the smallest number of our matrix is at the top left corner and the biggest number is at the bottom lower corner. These two number can represent the `range` i.e., the start and the end for the Binary Search. This does sound a bit counter-intuitive now, however, it will start to make sense soon. We are all accustomed to the linear array binary search algorithm. So, to delve into this idea a bit deeper, let's represent the `number range` on a single line as a one-dimensional array and see why and how binary search makes sense.

<center>
<img src="../Figures/378/img6.png"/>
</center>

Now, let's proceed with the first step that we take in any binary search implementation. We find the middle element, right? In a normal, one-dimensional binary search, we use the indices to find the middle element. In this case, the left and the right ends of our sorted matrix are the two values. So, we use them to find the `hypothetical` middle of the matrix. The reason we call this hypothetical is because it is not necessary that the middle value will exist in the matrix. However, that is not a requirement for our algorithm. 

<center>
<img src="../Figures/378/img7.png"/>
</center>

Even though this is a stupid question, it will help us to understand the overall logic here: How will you find out the $$K^{\text{th}}$$ smallest number in a sorted one-dimensional array? It's simple, right? You'll just return the $$K^{\text{th}}$$ element in the array. This is because you know the index $$K$$ in the array contains your answer. In our example, we know the two extremes and the middle element value. However, what we don't know are the sizes of the two halves. For all we know, we could have 8 elements in the left half and just 2 on the right in the example above. We don't know! 

>So, after finding the middle element, we need to determine the size of the left half. Why, you might ask? Well because we want the Kth smallest element and not the largest. If the question asked us for the largest, we would be determining the size of the right half.

We already have a [problem on LeetCode](https://leetcode.com/problems/search-a-2d-matrix/solution/) that is about searching for an element in a sorted 2D matrix. However, we don't want to search for our middle element. We want to `count the number of elements` in the left half of the number range of which we know the middle element and the two extremes as well. As it turns out, this can be done in $$O(N + N) = O(N)$$ time where $$N$$ represents the number of rows and columns. We will make use of the sorted nature of the matrix and count all the elements we need without actually iterating over them.

> For example, if an element in the cell [i, j] is smaller than our middle element, then it means that all the elements in the column "j" before this element i.r. (i-1) other elements in this column are also going to be less than the middle element. Why? Because the columns are sorted as well!

<center>
<img src="../Figures/378/img8.png"/>
</center>

<center>
<img src="../Figures/378/img9.png"/>
</center>

<center>
<img src="../Figures/378/img10.png"/>
</center>

What do we do with this newfound information of ours? Well, that depends on the value of K. So, let's consider our 3 different cases here. 

`Case 1: The size of the left half is EQUAL to K`

<center>
<img src="../Figures/378/img11.png"/>
</center>

`Case 2: The size of the left half is LESS than K`

<center>
<img src="../Figures/378/img12.png"/>
</center>

`Case 3: The size of the left half is MORE than K`

<center>
<img src="../Figures/378/img13.png"/>
</center>

So, as we can see from the above figures, when we are trying to determine the size of our left-half, we also need to keep track of two values. The largest element less-than-or-equal-to the middle element and the smallest element greater than the middle. For an index-based binary search, this wouldn't be a problem since `middle+1` and `middle-1` would represent these entries :).

**Algorithm**

* Start the binary search with `start = matrix[0][0]` and `end = matrix[N-1][N-1]`
* Find the `middle` of the start and the end. This middle number is `NOT` necessarily an element in the matrix.
* Count all the numbers smaller than or equal to middle in the matrix. As the matrix is sorted, we can do this in $$O(N)$$. Note that this is determining the size of the left-half of the array.
* While counting, we need to keep track of the `smallest number greater than the middle` (let’s call it `R`) and at the same time the `biggest number less than or equal to the middle` (let’s call it `L`). These two numbers will be used to adjust the `number range` for the binary search in the next iteration.
* If the count is equal to `K`, `L` will be our required number as it is the `biggest number less than or equal to the middle`, and is definitely present in the matrix.
* If the count is less than `K`, we can update `start = R` to search in the higher part of the matrix
* If the count is greater than `K`, we can update `end = L` to search in the lower part of the matrix in the next iteration.

<iframe src="https://leetcode.com/playground/eJKkrhYK/shared" frameBorder="0" width="100%" height="500" name="eJKkrhYK"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \times log(\text{Max}-\text{Min}))$$

    * Let's think about the time complexity in terms of the normal binary search algorithm. For a one-dimensional binary search over an array with $$N$$ elements, the complexity comes out to be $$O(log(N))$$. 
    * For our scenario, we are kind of defining our binary search space in terms of the minimum and the maximum numbers in the array. Going by this idea, the complexity for our binary search should be $$O(log(\text{Max}-\text{Min}))$$ where $$\text{Max}$$ is the maximum element in the array and likewise, $$\text{Min}$$ is the minimum element. 
    * However, we update our search space after each iteration. So, even if the maximum element is super large as compared to the remaining elements in the matrix, we will bring down the search space considerably in the next iterations. But, going purely by the extremes for our search space, the complexity of our binary search in search of $$K^{\text{th}}$$ smallest element will be $$O(log(\text{Max}-\text{Min}))$$.
    * In each iteration of our binary search approach, we iterate over the matrix trying to determine the size of the left-half as explained before. That takes $$O(N)$$. 
    * Thus, the overall time complexity is $$O(N \times log(\text{Max}-\text{Min}))$$

* Space Complexity: $$O(1)$$ since we don't use any additional space for performing our binary search.

Although there are a series of posts about the Binary Search approach in our discussions section, the one I consulted personally was [this](https://leetcode.com/problems/kth-smallest-element-in-a-sorted-matrix/discuss/301357/Java-0ms-(added-Python-and-C++):-Easy-to-understand-solutions-using-Heap-and-Binary-Search). Most of the code and some portions of the explanation are taken from the post. I just tried to add a bit more visual flair to further explain things.

Also, [Stefan Pochmann](https://leetcode.com/stefanpochmann/) has another great approach based on a research paper for this very problem. [Do check it out](https://leetcode.com/problems/kth-smallest-element-in-a-sorted-matrix/discuss/85170/O(n)-from-paper.-Yes-O(rows).)!
<br/><br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Share my thoughts and Clean Java Code
- Author: YuanGao
- Creation Date: Tue Aug 02 2016 01:59:18 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 02:21:18 GMT+0800 (Singapore Standard Time)

<p>
Solution 1 : Heap
Here is the step of my solution:
1. Build a minHeap of elements from the first row.
2. Do the following operations k-1 times : 
  Every time when you poll out the root(Top Element in Heap),  you need to know the row number and column number of that element(so we can create a tuple class here), replace that root with the next element from the same column.

After you finish this problem, thinks more :
1. For this question, you can also build a min Heap from the first column, and do the similar operations as above.(Replace the root with the next element from the same row)
2. What is more, this problem is exact the same with Leetcode373 Find K Pairs with Smallest Sums, I use the same code which beats 96.42%, after you solve this problem, you can check with this link:
https://discuss.leetcode.com/topic/52953/share-my-solution-which-beat-96-42
```
public class Solution {
    public int kthSmallest(int[][] matrix, int k) {
        int n = matrix.length;
        PriorityQueue<Tuple> pq = new PriorityQueue<Tuple>();
        for(int j = 0; j <= n-1; j++) pq.offer(new Tuple(0, j, matrix[0][j]));
        for(int i = 0; i < k-1; i++) {
            Tuple t = pq.poll();
            if(t.x == n-1) continue;
            pq.offer(new Tuple(t.x+1, t.y, matrix[t.x+1][t.y]));
        }
        return pq.poll().val;
    }
}

class Tuple implements Comparable<Tuple> {
    int x, y, val;
    public Tuple (int x, int y, int val) {
        this.x = x;
        this.y = y;
        this.val = val;
    }
    
    @Override
    public int compareTo (Tuple that) {
        return this.val - that.val;
    }
}
```

Solution 2 : Binary Search
We are done here, but let's think about this problem in another way:
The key point for any binary search is to figure out the "Search Space". For me, I think there are two kind of "Search Space" -- index and range(the range from the smallest number to the biggest number). Most usually, when the array is sorted in one direction, we can use index as "search space", when the array is unsorted and we are going to find a specific number, we can use "range".

Let me give you two examples of these two "search space"
1. index -- A bunch of examples -- https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/ ( the array is sorted)
2. range -- https://leetcode.com/problems/find-the-duplicate-number/ (Unsorted Array)

The reason why we did not use index as "search space" for this problem is the matrix is sorted in two directions, we can not find a linear way to map the number and its index.
```
public class Solution {
    public int kthSmallest(int[][] matrix, int k) {
        int lo = matrix[0][0], hi = matrix[matrix.length - 1][matrix[0].length - 1] + 1;//[lo, hi)
        while(lo < hi) {
            int mid = lo + (hi - lo) / 2;
            int count = 0,  j = matrix[0].length - 1;
            for(int i = 0; i < matrix.length; i++) {
                while(j >= 0 && matrix[i][j] > mid) j--;
                count += (j + 1);
            }
            if(count < k) lo = mid + 1;
            else hi = mid;
        }
        return lo;
    }
}
```
</p>


### Java 0ms (added Python & C++): Easy to understand solutions using Heap and Binary Search
- Author: arslanah
- Creation Date: Wed May 29 2019 13:51:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 07 2020 09:58:41 GMT+0800 (Singapore Standard Time)

<p>
## **Solution 1 using Heap: O(min(K,N)+K\u2217logN)**
This problem can be easily converted to \'Kth Smallest Number in M Sorted Lists\'. As each row (or column) of the given matrix can be seen as a sorted list, we essentially need to find the Kth smallest number in \u2018N\u2019 sorted lists. This solution is taken from  [Grokking the Coding Interview](https://www.educative.io/courses/grokking-the-coding-interview/x1NJVYKNvqz?aff=VOY6).

**Java:**
```
import java.util.*;

class Node {
  int row;
  int col;

  Node(int row, int col) {
    this.row = row;
    this.col = col;
  }
}

class Solution {

  public static int findKthSmallest(int[][] matrix, int k) {
    PriorityQueue<Node> minHeap = new PriorityQueue<Node>((n1, n2) -> matrix[n1.row][n1.col] - matrix[n2.row][n2.col]);

    // put the 1st element of each row in the min heap
    // we don\'t need to push more than \'k\' elements in the heap
    for (int i = 0; i < matrix.length && i < k; i++)
		minHeap.add(new Node(i, 0));

    // take the smallest (top) element form the min heap, if the running count is equal to k return the number
    // if the row of the top element has more elements, add the next element to the heap
    int numberCount = 0, result = 0;
    while (!minHeap.isEmpty()) {
      Node node = minHeap.poll();
      result = matrix[node.row][node.col];
      if (++numberCount == k)
        break;
      node.col++;
      if (matrix[0].length > node.col)
        minHeap.add(node);
    }
    return result;
  }
}
```

**Python:**
```
from heapq import *

def find_Kth_smallest(matrix, k):
    minHeap = []

    # put the 1st element of each row in the min heap
    # we don\'t need to push more than \'k\' elements in the heap
    for i in range(min(k, len(matrix))):
        heappush(minHeap, (matrix[i][0], 0, matrix[i]))

    # take the smallest(top) element form the min heap, if the running count is equal to k\' return the number
    # if the row of the top element has more elements, add the next element to the heap
    numberCount, number = 0, 0
    while minHeap:
        number, i, row = heappop(minHeap)
        numberCount += 1
        if numberCount == k:
            break
        if len(row) > i+1:
            heappush(minHeap, (row[i+1], i+1, row))
    return number
```

**C++:**
```
using namespace std;

#include <iostream>
#include <queue>
#include <vector>

class KthSmallestInSortedMatrix {
 public:
  struct numCompare {
    bool operator()(const pair<int, pair<int, int>> &x, const pair<int, pair<int, int>> &y) {
      return x.first > y.first;
    }
  };

  static int findKthSmallest(vector<vector<int>> &matrix, int k) {
    int n = matrix.size();
    priority_queue<pair<int, pair<int, int>>, vector<pair<int, pair<int, int>>>, numCompare> minHeap;

    // put the 1st element of each row in the min heap
    // we don\'t need to push more than \'k\' elements in the heap
    for (int i = 0; i < n && i < k; i++) {
      minHeap.push(make_pair(matrix[i][0], make_pair(i, 0)));
    }

    // take the smallest element form the min heap, if the running count is equal to k return the number
    // if the row of the top element has more elements, add the next element to the heap
    int numberCount = 0, result = 0;
    while (!minHeap.empty()) {
      auto heapTop = minHeap.top();
      minHeap.pop();
      result = heapTop.first;
      if (++numberCount == k) {
        break;
      }

      heapTop.second.second++;
      if (n > heapTop.second.second) {
        heapTop.first = matrix[heapTop.second.first][heapTop.second.second];
        minHeap.push(heapTop);
      }
    }
    return result;
  }
};
```
The space complexity of the above solutions will be O(N) and the time complexity will be O(min(K,N)+K\u2217logN).

## **Solution 2 using Binary Search: O(N\u2217log(max\u2212min))**
Since each row and column of the matrix is sorted, is it possible to use **Binary Search** to find the Kth smallest number?

The biggest problem to use **Binary Search** in this case is that we don\u2019t have a straightforward sorted array, instead we have a matrix. As we remember, in **Binary Search**, we calculate the middle index of the search space (\u20181\u2019 to \u2018N\u2019) and see if our required number is pointed out by the middle index; if not we either search in the lower half or the upper half. In a sorted matrix, we can\u2019t really find a middle. Even if we do consider some index as middle, it is not straightforward to find the search space containing numbers bigger or smaller than the number pointed out by the middle index.

An alternate could be to apply the **Binary Search** on the \u201Cnumber range\u201D instead of the \u201Cindex range\u201D. As we know that the smallest number of our matrix is at the top left corner and the biggest number is at the bottom lower corner. These two number can represent the \u201Crange\u201D i.e., the `start` and the `end` for the **Binary Search**. Here is how our algorithm will work:

1. Start the **Binary Search** with `start = matrix[0][0]` and `end = matrix[n-1][n-1]`.
2. Find `middle` of the `start` and the `end`. This `middle` number is NOT necessarily an element in the matrix.
3. Count all the numbers smaller than or equal to `middle` in the matrix. As the matrix is sorted, we can do this in O(N). 
4. While counting, we can keep track of the \u201Csmallest number greater than the `middle`\u201D (let\u2019s call it `n1`) and at the same time the \u201Cbiggest number less than or equal to the `middle`\u201D (let\u2019s call it `n2`). These two numbers will be used to adjust the "number range" for the **Binary Search** in the next iteration.
5. If the count is equal to \u2018K\u2019, `n1` will be our required number as it is the \u201Cbiggest number less than or equal to the `middle`\u201D, and is definitely present in the matrix.
6. If the count is less than \u2018K\u2019, we can update `start = n2` to search in the higher part of the matrix and if the count is greater than \u2018K\u2019, we can update `end = n1` to search in the lower part of the matrix in the next iteration.

Here is the visual representation of this algorithm:

![image](https://assets.leetcode.com/users/arslanah/image_1559108433.png)


**Java:**
```
class Solution {
  public static int findKthSmallest(int[][] matrix, int k) {
    int n = matrix.length;
    int start = matrix[0][0], end = matrix[n - 1][n - 1];
    while (start < end) {
      int mid = start + (end - start) / 2;
      // first number is the smallest and the second number is the largest
      int[] smallLargePair = { matrix[0][0], matrix[n - 1][n - 1] };

      int count = countLessEqual(matrix, mid, smallLargePair);

      if (count == k)
        return smallLargePair[0];

      if (count < k)
        start = smallLargePair[1]; // search higher
      else
        end = smallLargePair[0]; // search lower
    }

    return start;
  }

  private static int countLessEqual(int[][] matrix, int mid, int[] smallLargePair) {
    int count = 0;
    int n = matrix.length, row = n - 1, col = 0;
    while (row >= 0 && col < n) {
      if (matrix[row][col] > mid) {
        // as matrix[row][col] is bigger than the mid, let\'s keep track of the
        // smallest number greater than the mid
        smallLargePair[1] = Math.min(smallLargePair[1], matrix[row][col]);
        row--;
      } else {
        // as matrix[row][col] is less than or equal to the mid, let\'s keep track of the
        // biggest number less than or equal to the mid
        smallLargePair[0] = Math.max(smallLargePair[0], matrix[row][col]);
        count += row + 1;
        col++;
      }
    }
    return count;
  }
}
```

**Python:**
```
def find_Kth_smallest(matrix, k):
  n = len(matrix)
  start, end = matrix[0][0], matrix[n - 1][n - 1]
  while start < end:
    mid = start + (end - start) / 2
    smaller, larger = (matrix[0][0], matrix[n - 1][n - 1])

    count, smaller, larger = count_less_equal(matrix, mid, smaller, larger)

    if count == k:
      return smaller
    if count < k:
      start = larger  # search higher
    else:
      end = smaller  # search lower

  return start


def count_less_equal(matrix, mid, smaller, larger):
  count, n = 0, len(matrix)
  row, col = n - 1, 0
  while row >= 0 and col < n:
    if matrix[row][col] > mid:
      # as matrix[row][col] is bigger than the mid, let\'s keep track of the
      # smallest number greater than the mid
      larger = min(larger, matrix[row][col])
      row -= 1
    else:
      # as matrix[row][col] is less than or equal to the mid, let\'s keep track of the
      # biggest number less than or equal to the mid
      smaller = max(smaller, matrix[row][col])
      count += row + 1
      col += 1

  return count, smaller, larger
```

**C++:**
```
class KthSmallestInSortedMatrix {
 public:
  static int findKthSmallest(vector<vector<int>> &matrix, int k) {
    int n = matrix.size();
    int start = matrix[0][0], end = matrix[n - 1][n - 1];
    while (start < end) {
      int mid = start + (end - start) / 2;
      // first number is the smallest and the second number is the largest
      pair<int, int> smallLargePair = {matrix[0][0], matrix[n - 1][n - 1]};
      int count = countLessEqual(matrix, mid, smallLargePair);
      if (count == k) {
        return smallLargePair.first;
      }

      if (count < k) {
        start = smallLargePair.second;  // search higher
      } else {
        end = smallLargePair.first;  // search lower
      }
    }

    return start;
  }

 private:
  static int countLessEqual(vector<vector<int>> &matrix, int mid, pair<int, int> &smallLargePair) {
    int count = 0;
    int n = matrix.size(), row = n - 1, col = 0;
    while (row >= 0 && col < n) {
      if (matrix[row][col] > mid) {
        // as matrix[row][col] is bigger than the mid, let\'s keep track of the
        // smallest number greater than the mid
        smallLargePair.second = min(smallLargePair.second, matrix[row][col]);
        row--;
      } else {
        // as matrix[row][col] is less than or equal to the mid, let\'s keep track of the
        // biggest number less than or equal to the mid
        smallLargePair.first = max(smallLargePair.first, matrix[row][col]);
        count += row + 1;
        col++;
      }
    }
    return count;
  }
};
```

The algorithm runs in constant space O(1).

The above solution was taken from [Grokking the Coding Interview](https://www.educative.io/courses/grokking-the-coding-interview/x1NJVYKNvqz?aff=VOY6).
</p>


### O(n) from paper. Yes, O(#rows).
- Author: StefanPochmann
- Creation Date: Wed Aug 03 2016 19:44:29 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 15:44:31 GMT+0800 (Singapore Standard Time)

<p>
It's O(n) where n is the number of rows (and columns), not the number of elements. So it's very efficient. The algorithm is from the paper [Selection in X + Y and matrices with sorted rows and columns](http://www.cse.yorku.ca/~andy/pubs/X+Y.pdf), which I first saw mentioned by @elmirap (thanks).

**The basic idea:** Consider the submatrix you get by removing every second row and every second column. This has about a quarter of the elements of the original matrix. And the k-th element (k-th *smallest* I mean) of the original matrix is roughly the (k/4)-th element of the submatrix. So roughly get the (k/4)-th element of the submatrix and then use that to find the k-th element of the original matrix in O(n) time. It's recursive, going down to smaller and smaller submatrices until a trivial 2\xd72 matrix. For more details I suggest checking out the paper, the first half is easy to read and explains things well. Or @zhiqing_xiao's [solution+explanation](https://discuss.leetcode.com/topic/54262/o-row-time-o-row-space-solution-with-detail-intuitive-explanation-c-accepted).

**Cool:** It uses variants of [saddleback search](http://cs.geneseo.edu/~baldwin/math-thinking/saddleback.html) that you might know for example from the [Search a 2D Matrix II](https://leetcode.com/problems/search-a-2d-matrix-ii/) problem. And it uses the [median of medians](https://en.wikipedia.org/wiki/Median_of_medians) algorithm for linear-time selection.

**Optimization:** If k is less than n, we only need to consider the top-left k\xd7k matrix. Similar if k is almost n<sup>2</sup>. So it's even O(min(n, k, n^2-k)), I just didn't mention that in the title because I wanted to keep it simple and because those few very small or very large k are unlikely, most of the time k will be "medium" (and average n<sup>2</sup>/2).

**Implementation:** I implemented the submatrix by using an index list through which the actual matrix data gets accessed. If [0, 1, 2, ..., n-1] is the index list of the original matrix, then [0, 2, 4, ...] is the index list of the submatrix and [0, 4, 8, ...] is the index list of the subsubmatrix and so on. This also covers the above optimization by starting with [0, 1, 2, ..., **k**-1] when applicable.

**Application:** I believe it can be used to easily solve the [Find K Pairs with Smallest Sums](https://leetcode.com/problems/find-k-pairs-with-smallest-sums/) problem in time O(k) instead of O(k log n), which I think is the best posted so far. I might try that later if nobody beats me to it (if you do, let me know :-). ***Update:*** I [did that now](https://discuss.leetcode.com/topic/53380/o-k-solution).

```
class Solution(object):
    def kthSmallest(self, matrix, k):

        # The median-of-medians selection function.
        def pick(a, k):
            if k == 1:
                return min(a)
            groups = (a[i:i+5] for i in range(0, len(a), 5))
            medians = [sorted(group)[len(group) / 2] for group in groups]
            pivot = pick(medians, len(medians) / 2 + 1)
            smaller = [x for x in a if x < pivot]
            if k <= len(smaller):
                return pick(smaller, k)
            k -= len(smaller) + a.count(pivot)
            return pivot if k < 1 else pick([x for x in a if x > pivot], k)

        # Find the k1-th and k2th smallest entries in the submatrix.
        def biselect(index, k1, k2):

            # Provide the submatrix.
            n = len(index)
            def A(i, j):
                return matrix[index[i]][index[j]]
            
            # Base case.
            if n <= 2:
                nums = sorted(A(i, j) for i in range(n) for j in range(n))
                return nums[k1-1], nums[k2-1]

            # Solve the subproblem.
            index_ = index[::2] + index[n-1+n%2:]
            k1_ = (k1 + 2*n) / 4 + 1 if n % 2 else n + 1 + (k1 + 3) / 4
            k2_ = (k2 + 3) / 4
            a, b = biselect(index_, k1_, k2_)

            # Prepare ra_less, rb_more and L with saddleback search variants.
            ra_less = rb_more = 0
            L = []
            jb = n   # jb is the first where A(i, jb) is larger than b.
            ja = n   # ja is the first where A(i, ja) is larger than or equal to a.
            for i in range(n):
                while jb and A(i, jb - 1) > b:
                    jb -= 1
                while ja and A(i, ja - 1) >= a:
                    ja -= 1
                ra_less += ja
                rb_more += n - jb
                L.extend(A(i, j) for j in range(jb, ja))
                
            # Compute and return x and y.
            x = a if ra_less <= k1 - 1 else \
                b if k1 + rb_more - n*n <= 0 else \
                pick(L, k1 + rb_more - n*n)
            y = a if ra_less <= k2 - 1 else \
                b if k2 + rb_more - n*n <= 0 else \
                pick(L, k2 + rb_more - n*n)
            return x, y

        # Set up and run the search.
        n = len(matrix)
        start = max(k - n*n + n-1, 0)
        k -= n*n - (n - start)**2
        return biselect(range(start, min(n, start+k)), k, k)[0]
```
</p>


