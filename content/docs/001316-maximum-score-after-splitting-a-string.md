---
title: "Maximum Score After Splitting a String"
weight: 1316
#id: "maximum-score-after-splitting-a-string"
---
## Description
<div class="description">
<p>Given a&nbsp;string <code>s</code>&nbsp;of zeros and ones, <em>return the maximum score after splitting the string into two <strong>non-empty</strong> substrings</em> (i.e. <strong>left</strong> substring and <strong>right</strong> substring).</p>

<p>The score after splitting a string is the number of <strong>zeros</strong> in the <strong>left</strong> substring plus the number of <strong>ones</strong> in the <strong>right</strong> substring.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;011101&quot;
<strong>Output:</strong> 5 
<strong>Explanation:</strong> 
All possible ways of splitting s into two non-empty substrings are:
left = &quot;0&quot; and right = &quot;11101&quot;, score = 1 + 4 = 5 
left = &quot;01&quot; and right = &quot;1101&quot;, score = 1 + 3 = 4 
left = &quot;011&quot; and right = &quot;101&quot;, score = 1 + 2 = 3 
left = &quot;0111&quot; and right = &quot;01&quot;, score = 1 + 1 = 2 
left = &quot;01110&quot; and right = &quot;1&quot;, score = 2 + 1 = 3
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;00111&quot;
<strong>Output:</strong> 5
<strong>Explanation:</strong> When left = &quot;00&quot; and right = &quot;111&quot;, we get the maximum score = 2 + 3 = 5
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1111&quot;
<strong>Output:</strong> 3
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= s.length &lt;= 500</code></li>
	<li>The string <code>s</code> consists of characters &#39;0&#39; and &#39;1&#39; only.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 5 Liner (One Pass)
- Author: vikrant_pc
- Creation Date: Sun Apr 26 2020 12:02:02 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 27 2020 11:49:22 GMT+0800 (Singapore Standard Time)

<p>
This can be done in single pass as below. With this approach we do not know the number of ones on right while iterating, so I have added TotalOne\'s to the result before returning.

Logic behind this -
Result = Max of (ZerosOnLeft + OnesOnRight) 
= Max of (ZerosOnLeft + (TotalOnes - OnesOnLeft)) 
= Max of (ZerosOnLeft - OnesOnLeft) + TotalOnes (as TotalOnes is constant)

```
public int maxScore(String s) {
	int zeros = 0, ones = 0, max = Integer.MIN_VALUE;
	for(int i=0;i<s.length();i++) {
		if(s.charAt(i) == \'0\') zeros++; else ones++;
		if(i != s.length()-1) max = Math.max(zeros - ones, max);
	}
	return max + ones;
}
```
</p>


### [Java] StraightForward
- Author: manrajsingh007
- Creation Date: Sun Apr 26 2020 12:03:04 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Apr 29 2020 01:49:02 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int maxScore(String s) {
        int n = s.length();
        int c1 = 0;
        for(int i = 0; i < n; i++) {
            if(s.charAt(i) == \'1\') c1++;
        }
        int max = 0;
        int c2 = 0;
        for(int i = 0; i < n - 1; i++) {
            if(s.charAt(i) == \'0\') c2++;
            else c1--;
            max = Math.max(max, c1 + c2);
        }
        return max;
    }
}
```
**Alternate Single Pass**
```
class Solution {
    public int maxScore(String s) {
        int n = s.length(), max = -n - 1, zeroCount = 0, oneCount = 0;
        for(int i = 0; i < n; i++) {
            if(s.charAt(i) == \'0\') zeroCount++;
            else oneCount++;
            if(i < n - 1) max = Math.max(zeroCount - oneCount, max);
        }
        return max + oneCount;
    }
}
</p>


### Python - Simple. Faster than 94%
- Author: p-maria
- Creation Date: Fri May 08 2020 02:11:35 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 18 2020 05:40:37 GMT+0800 (Singapore Standard Time)

<p>
<b>Time Complexity:</b> O(N)
<b>Runtime:</b> Faster than 94% <i>(as of May 11th, 2020)</i>
<b>Example:</b> ```input``` = "00010", ```output``` = 4
<b>Explanation:</b> 
Step 1: In the first 2 lines of code, calculate the number of 0s in the left substring ("0") and the number of 1s in the right substring ("0010"). Thus, we start with <b>zeros = 1</b> and <b>ones = 1</b>. 
Step 2: Traverse only the highlighted part of the string "0<b>001</b>0" and update the <b>zeros</b>, <b>ones</b>, and <b>score</b> variables as follows:
- At index 1: left = "00" and right = "010", zeros = 2, ones = 1, score 2 + 1 = 3 
- At index 2: left = "000" and right = "10", zeros = 3, ones = 1, score 3 + 1 = <b>4</b> 
- At index 3: left = "0001" and right = "0", zeros = 3, ones = 0, score 3 + 0 = 3 


```
def solution(s):
	zeros = 1 if s[0] == \'0\' else 0
	ones = s.count(\'1\', 1)  # count 1s in s[1:]
	score = zeros + ones
	for i in xrange(1, len(s) - 1):
		if s[i] == \'0\':
			zeros += 1
		else:
			ones -= 1
		score = max(zeros + ones,  score)
	return score
```
If you like my solution and/or find it useful, please upvote :) Thank you.



</p>


