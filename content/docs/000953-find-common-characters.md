---
title: "Find Common Characters"
weight: 953
#id: "find-common-characters"
---
## Description
<div class="description">
<p>Given an array&nbsp;<code>A</code> of strings made only from lowercase letters, return a list of all characters that show up in all strings within the list <strong>(including duplicates)</strong>.&nbsp;&nbsp;For example, if a character occurs 3 times&nbsp;in all strings but not 4 times, you need to include that character three times&nbsp;in the final answer.</p>

<p>You may return the answer in any order.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;bella&quot;,&quot;label&quot;,&quot;roller&quot;]</span>
<strong>Output: </strong><span id="example-output-1">[&quot;e&quot;,&quot;l&quot;,&quot;l&quot;]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[&quot;cool&quot;,&quot;lock&quot;,&quot;cook&quot;]</span>
<strong>Output: </strong><span id="example-output-2">[&quot;c&quot;,&quot;o&quot;]</span>
</pre>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 100</code></li>
	<li><code>1 &lt;= A[i].length &lt;= 100</code></li>
	<li><code>A[i][j]</code> is a lowercase letter</li>
</ol>
</div>
</div>
</div>

## Tags
- Array (array)
- Hash Table (hash-table)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- TripAdvisor - 3 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python 1 Line
- Author: lee215
- Creation Date: Sun Mar 03 2019 12:05:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 03 2019 12:05:13 GMT+0800 (Singapore Standard Time)

<p>
```
    def commonChars(self, A):
        res = collections.Counter(A[0])
        for a in A:
            res &= collections.Counter(a)
        return list(res.elements())
```

1-line version
```
    def commonChars(self, A):
        return list(reduce(collections.Counter.__and__, map(collections.Counter, A)).elements())
```

</p>


### C++ O(n) | O(1), two vectors
- Author: votrubac
- Creation Date: Sun Mar 03 2019 12:08:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 03 2019 12:08:52 GMT+0800 (Singapore Standard Time)

<p>
For each string, we count characters in ```cnt1```. Then, we track the minimum count for each character in ```cnt```.
```
vector<string> commonChars(vector<string>& A) {
  vector<int> cnt(26, INT_MAX);
  vector<string> res;
  for (auto s : A) {
    vector<int> cnt1(26, 0);
    for (auto c : s) ++cnt1[c - \'a\'];
    for (auto i = 0; i < 26; ++i) cnt[i] = min(cnt[i], cnt1[i]);
  }
  for (auto i = 0; i < 26; ++i)
    for (auto j = 0; j < cnt[i]; ++j) res.push_back(string(1, i + \'a\'));
  return res;
}
```
## Complexity Analysis
Runtime: *O(n)*, where *n* is the total number of characters.
Memory: *O(1)* (we use two fixed-size vectors).
</p>


### [Java/Python 3] 12 liner and 7 liner - count and look for minimum.
- Author: rock
- Creation Date: Sun Mar 03 2019 12:05:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 29 2019 20:53:37 GMT+0800 (Singapore Standard Time)

<p>
Initialize `count` array with `Integer.MAX_VALUE`, loop through the input to count the chars in each string; then find out the minimum for each char.

**Method 1:**

```
    public List<String> commonChars(String[] A) {
        List<String> ans = new ArrayList<>();
        int[] count = new int[26]; 
        Arrays.fill(count, Integer.MAX_VALUE);
        for (String str : A) {
            int[] cnt = new int[26];
            str.chars().forEach(c -> ++cnt[c - \'a\']); // count each char\'s frequency in string str.
            for (int i = 0; i < 26; ++i) { count[i] = Math.min(cnt[i], count[i]); } // update minimum frequency.
        }
        for (char c = \'a\'; c <= \'z\'; ++c) {
            while (count[c - \'a\']-- > 0) { ans.add("" + c); }
        }
        return ans;
    }
```
```
    def commonChars(self, A: List[str]) -> List[str]:
        cnt = [float(\'inf\')] * 26
        for s in A:
            cnt2 = [0] * 26
            for c in s:
                cnt2[ord(c) - ord(\'a\')] += 1
            cnt = [min(cnt[i], cnt2[i]) for i in range(26)]    
        return [c for k, c in zip(cnt, string.ascii_lowercase) for _ in range(k)]
```
or use `Counter`:
```
    def commonChars(self, A: List[str]) -> List[str]:
        cnt = collections.Counter(A[0])
        for s in A:
            cnt2 = collections.Counter(s)
            for k in cnt.keys():
                cnt[k] = min(cnt[k], cnt2[k])
        return cnt.elements()
```

----
**Method 2: Java 8 Stream**

```
    public List<String> commonChars(String[] A) {
        int[] count = new int[26]; 
        Arrays.fill(count, Integer.MAX_VALUE);
        for (String str : A) {
            int[] cnt = new int[26];
            str.chars().forEach(c -> ++cnt[c - \'a\']); // count each char\'s frequency in string str.
            IntStream.range(0, 26).forEach(i ->  count[i] = Math.min(cnt[i], count[i])); // update minimum frequency.
        }
        List<String> ans = new ArrayList<>();
        IntStream.range(\'a\', \'z\' + 1).forEach(c ->  ans.addAll(Collections.nCopies(count[c - \'a\'], "" + (char)c)));
        return ans;
    }
```
**Analysis:**
**Time: O(n)**, where `n` is the total number of characters in A; 
**extra space: O(1)**, excludes output/return space.


**Q & A**:

Q: What does ` ++cnt[c - \'a\']; ` mean?

A: 
count the frequency of chars in String str.

c : cnt[c - \'a\'] 
-----------------------------
\'a\': cnt[\'a\' - \'a\'] = cnt[0]
\'b\': cnt[\'b\' - \'a\'] = cnt[1]
\'c\': cnt[\'c\' - \'a\'] = cnt[2]
...
\'z\': cnt[\'z\' - \'a\'] = cnt[25]
--------------------------------
if char `c` represents `\'x\'`, then  `cnt[c - \'a\'] = cnt[23]`. That is, when encountering char \'x\', we increase cnt[23] by 1. 

Therefore, after traversal of all chars in String `str`, we have the frequency (number of occurrence) of each char in `cnt`.
</p>


