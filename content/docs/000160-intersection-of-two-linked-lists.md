---
title: "Intersection of Two Linked Lists"
weight: 160
#id: "intersection-of-two-linked-lists"
---
## Description
<div class="description">
<p>Write a program to find the node at which the intersection of two singly linked lists begins.</p>

<p>For example, the following two linked lists:</p>
<a href="https://assets.leetcode.com/uploads/2018/12/13/160_statement.png" target="_blank"><img alt="" src="https://assets.leetcode.com/uploads/2018/12/13/160_statement.png" style="margin-top: 10px; margin-bottom: 10px; width: 400px; height: 130px;" /></a>

<p>begin to intersect at node c1.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>
<a href="https://assets.leetcode.com/uploads/2020/06/29/160_example_1_1.png" target="_blank"><img alt="" src="https://assets.leetcode.com/uploads/2020/06/29/160_example_1_1.png" style="margin-top: 10px; margin-bottom: 10px; width: 400px; height: 130px;" /></a>

<pre>
<strong>Input: </strong>intersectVal = 8, listA = [4,1,8,4,5], listB = [5,6,1,8,4,5], skipA = 2, skipB = 3
<strong>Output:</strong> Reference of the node with value = 8
<strong>Input Explanation:</strong> The intersected node&#39;s value is 8 (note that this must not be 0 if the two lists intersect). From the head of A, it reads as [4,1,8,4,5]. From the head of B, it reads as [5,6,1,8,4,5]. There are 2 nodes before the intersected node in A; There are 3 nodes before the intersected node in B.</pre>

<p>&nbsp;</p>

<p><strong>Example 2:</strong></p>
<a href="https://assets.leetcode.com/uploads/2020/06/29/160_example_2.png" target="_blank"><img alt="" src="https://assets.leetcode.com/uploads/2020/06/29/160_example_2.png" style="margin-top: 10px; margin-bottom: 10px; width: 350px; height: 136px;" /></a>

<pre>
<strong>Input: </strong>intersectVal&nbsp;= 2, listA = [1,9,1,2,4], listB = [3,2,4], skipA = 3, skipB = 1
<strong>Output:</strong> Reference of the node with value = 2
<strong>Input Explanation:</strong>&nbsp;The intersected node&#39;s value is 2 (note that this must not be 0 if the two lists intersect). From the head of A, it reads as [1,9,1,2,4]. From the head of B, it reads as [3,2,4]. There are 3 nodes before the intersected node in A; There are 1 node before the intersected node in B.
</pre>

<p>&nbsp;</p>

<p><strong>Example 3:</strong></p>
<a href="https://assets.leetcode.com/uploads/2018/12/13/160_example_3.png" target="_blank"><img alt="" src="https://assets.leetcode.com/uploads/2018/12/13/160_example_3.png" style="margin-top: 10px; margin-bottom: 10px; width: 200px; height: 126px;" /></a>

<pre>
<strong>Input: </strong>intersectVal = 0, listA = [2,6,4], listB = [1,5], skipA = 3, skipB = 2
<strong>Output:</strong> null
<strong>Input Explanation:</strong> From the head of A, it reads as [2,6,4]. From the head of B, it reads as [1,5]. Since the two lists do not intersect, intersectVal must be 0, while skipA and skipB can be arbitrary values.
<strong>Explanation:</strong> The two lists do not intersect, so return null.
</pre>

<p>&nbsp;</p>

<p><b>Notes:</b></p>

<ul>
	<li>If the two linked lists have no intersection at all, return <code>null</code>.</li>
	<li>The linked lists must retain their original structure after the function returns.</li>
	<li>You may assume there are no cycles anywhere in the entire linked structure.</li>
	<li>Each value&nbsp;on each linked list is in the range <code>[1, 10^9]</code>.</li>
	<li>Your code should preferably run in O(n) time and use only O(1) memory.</li>
</ul>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Microsoft - 4 (taggedByAdmin: true)
- Facebook - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: true)
- eBay - 3 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Qualcomm - 2 (taggedByAdmin: false)
- Morgan Stanley - 2 (taggedByAdmin: false)
- Amazon - 6 (taggedByAdmin: true)
- Airbnb - 2 (taggedByAdmin: true)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Redfin - 2 (taggedByAdmin: false)
- Oracle - 6 (taggedByAdmin: false)
- Yahoo - 5 (taggedByAdmin: false)
- ByteDance - 5 (taggedByAdmin: false)
- Expedia - 3 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force

For each node a<sub>i</sub> in list A, traverse the entire list B and check if any node in list B coincides with a<sub>i</sub>.

**Complexity Analysis**

* Time complexity : $$O(mn)$$.

* Space complexity : $$O(1)$$.
<br />
<br />
---
#### Approach 2: Hash Table

Traverse list A and store the address / reference to each node in a hash set. Then check every node b<sub>i</sub> in list B: if b<sub>i</sub> appears in the hash set, then b<sub>i</sub> is the intersection node.

**Complexity Analysis**

* Time complexity : $$O(m+n)$$.

* Space complexity : $$O(m)$$ or $$O(n)$$.
<br />
<br />
---
#### Approach 3: Two Pointers

- Maintain two pointers $$pA$$ and $$pB$$ initialized at the head of A and B, respectively. Then let them both traverse through the lists, one node at a time.
- When $$pA$$ reaches the end of a list, then redirect it to the head of B (yes, B, that's right.); similarly when $$pB$$ reaches the end of a list, redirect it the head of A.
- If at any point $$pA$$ meets $$pB$$, then $$pA$$/$$pB$$ is the intersection node.
- To see why the above trick would work, consider the following two lists: A = {1,3,5,7,9,11} and B = {2,4,9,11}, which are intersected at node '9'. Since B.length (=4) < A.length (=6), $$pB$$ would reach the end of the merged list first, because $$pB$$ traverses exactly 2 nodes less than $$pA$$ does. By redirecting $$pB$$ to head A, and $$pA$$ to head B, we now ask $$pB$$ to travel exactly 2 more nodes than $$pA$$ would. So in the second iteration, they are guaranteed to reach the intersection node at the same time.
- If two lists have intersection, then their last nodes must be the same one. So when $$pA$$/$$pB$$ reaches the end of a list, record the last element of A/B respectively. If the two last elements are not the same one, then the two lists have no intersections.

**Complexity Analysis**

* Time complexity : $$O(m+n)$$.

* Space complexity : $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution without knowing the difference in len!
- Author: myfavcat123
- Creation Date: Tue Oct 27 2015 02:25:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 09:39:59 GMT+0800 (Singapore Standard Time)

<p>
I found most solutions here preprocess linkedlists to get the difference in len. 
Actually we don't care about the "value" of difference, we just want to make sure two pointers reach the intersection node at the same time.

We can use two iterations to do that. In the first iteration, we will reset the pointer of one linkedlist to the head of another linkedlist after it reaches the tail node. In the second iteration, we will move two pointers until they points to the same node. Our operations in first iteration will help us counteract the difference. So if two linkedlist intersects, the meeting point in second iteration must be the intersection point. If the two linked lists have no intersection at all, then the meeting pointer in second iteration must be the tail node of both lists, which is null

Below is my commented Java code:

    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        //boundary check
        if(headA == null || headB == null) return null;
        
        ListNode a = headA;
        ListNode b = headB;
        
        //if a & b have different len, then we will stop the loop after second iteration
        while( a != b){
        	//for the end of first iteration, we just reset the pointer to the head of another linkedlist
            a = a == null? headB : a.next;
            b = b == null? headA : b.next;    
        }
        
        return a;
    }
</p>


### My accepted simple and shortest C++ code with comments explaining the algorithm.  Any comments or improvements?
- Author: satyakam
- Creation Date: Sat Nov 29 2014 14:47:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 11:22:51 GMT+0800 (Singapore Standard Time)

<p>
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) 
    {
        ListNode *p1 = headA;
        ListNode *p2 = headB;
            
        if (p1 == NULL || p2 == NULL) return NULL;
    
        while (p1 != NULL && p2 != NULL && p1 != p2) {
            p1 = p1->next;
            p2 = p2->next;

            //
            // Any time they collide or reach end together without colliding 
            // then return any one of the pointers.
            //
            if (p1 == p2) return p1;

            //
            // If one of them reaches the end earlier then reuse it 
            // by moving it to the beginning of other list.
            // Once both of them go through reassigning, 
            // they will be equidistant from the collision point.
            //
            if (p1 == NULL) p1 = headB;
            if (p2 == NULL) p2 = headA;
        }
            
        return p1;
    }
</p>


### Concise python code with comments
- Author: icrtiou
- Creation Date: Thu May 07 2015 12:17:15 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 06:26:50 GMT+0800 (Singapore Standard Time)

<p>
    class Solution:
        # @param two ListNodes
        # @return the intersected ListNode
        def getIntersectionNode(self, headA, headB):
            if headA is None or headB is None:
                return None
    
            pa = headA # 2 pointers
            pb = headB
    
            while pa is not pb:
                # if either pointer hits the end, switch head and continue the second traversal, 
                # if not hit the end, just move on to next
                pa = headB if pa is None else pa.next
                pb = headA if pb is None else pb.next
    
            return pa # only 2 ways to get out of the loop, they meet or the both hit the end=None
    
    # the idea is if you switch head, the possible difference between length would be countered. 
    # On the second traversal, they either hit or miss. 
    # if they meet, pa or pb would be the node we are looking for, 
    # if they didn't meet, they will hit the end at the same iteration, pa == pb == None, return either one of them is the same,None
</p>


