---
title: "Two Sum III - Data structure design"
weight: 170
#id: "two-sum-iii-data-structure-design"
---
## Description
<div class="description">
<p>Design a data structure that accepts a stream of integers and checks if it has a pair of integers that sum up to a particular value.</p>

<p>Implement the <code>TwoSum</code> class:</p>

<ul>
	<li><code>TwoSum()</code> Initializes the <code>TwoSum</code> object, with an empty array initially.</li>
	<li><code>void add(int number)</code> Adds <code>number</code> to the data structure.</li>
	<li><code>boolean find(int value)</code> Returns <code>true</code> if there exists any pair of numbers whose sum is equal to <code>value</code>, otherwise, it returns <code>false</code>.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;TwoSum&quot;, &quot;add&quot;, &quot;add&quot;, &quot;add&quot;, &quot;find&quot;, &quot;find&quot;]
[[], [1], [3], [5], [4], [7]]
<strong>Output</strong>
[null, null, null, null, true, false]

<strong>Explanation</strong>
TwoSum twoSum = new TwoSum();
twoSum.add(1);   // [] --&gt; [1]
twoSum.add(3);   // [1] --&gt; [1,3]
twoSum.add(5);   // [1,3] --&gt; [1,3,5]
twoSum.find(4);  // 1 + 3 = 4, return true
twoSum.find(7);  // No two integers sum up to 7, return false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>-10<sup>5</sup> &lt;= number &lt;= 10<sup>5</sup></code></li>
	<li><code>-2<sup>31</sup> &lt;= value &lt;= 2<sup>31</sup> - 1</code></li>
	<li>At most <code>5 * 10<sup>4</sup></code> calls will be made to <code>add</code> and <code>find</code>.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Design (design)

## Companies
- LinkedIn - 21 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Sorted List

**Intuition**

First of all, the problem description is not terribly clear on the requirements of _time_ and _space_ complexity. But let us consider this as part of the challenge or a freedom of design. We could figure out the desired complexity for each function, by trial and error.

This is one of the followup problems to the first programming problem on LeetCode called [Two Sum](https://leetcode.com/problems/two-sum/), where one is asked to return the indice of two numbers from a **_list_** that could sum up to a given value.

>Let us take the inspiration from the origin problem, by keeping all the incoming numbers in a _list_.

Given a list, one of the solutions to the Two Sum problem is called **_Two-Pointers Iteration_** where we iterate through the list from two directions with _two pointers_ approaching each other.

![pic](../Figures/170/170_two_pointers.png)

>However, one of the preconditions for the Two-Pointers Iteration solution is that the input list should be _**sorted**_. 

So now, here are the questions:

- Should we keep the list in order while inserting new numbers in the function `add(number)` ? 

- Or should we do the sorting on demand, _i.e._ at the invocation of `find(value)` ?

We will address the above two questions later in the Algorithm section.

**Algorithm**

Let us first give the algorithm of Two-Pointers Iteration to find the two-sum solution from a _sorted_ list:

- We initialize **two pointers** `low` and `high` which point to the head and the tail elements of the list respectively.

- With the two pointers, we start a **loop** to iterate the list. The loop would terminate either we find the two-sum solution or the two pointers meet each other.

- Within the loop, at each step, we would move either of the pointers, according to different conditions:

    - If the sum of the elements pointed by the current pointers is _**less than**_ the desired value, then we should try to increase the sum to meet the desired value, _i.e._ we should move the `low` pointer forwards to have a larger value.

    - Similarly if the sum of the elements pointed by the current pointers is _**greater than**_ the desired value, we then should try to reduce the sum by moving the `high` pointer towards the `low` pointer. 

    - If the sum happen to the desired value, then we could simply do an **early return** of the function.

- If the loop is terminated at the case where the two pointers meet each other, then we can be sure that there is no solution to the desired value.


<iframe src="https://leetcode.com/playground/BtJsFEDc/shared" frameBorder="0" width="100%" height="500" name="BtJsFEDc"></iframe>

>The usage pattern of the desired data structure in the online judge, as we would discover, is that the `add(number)` function would be called **frequently** which might be followed a less frequent call of `find(value)` function.

The usage pattern implies that we should try to minimize the cost of `add(number)` function. As a result, we sort the list within the `find(value)` function instead of the `add(number)` function.

_So to the above questions about where to place the sort operation, actually both options are valid and correct._ Due to the usage pattern of the two functions though, it is **less optimal** to sort the list at each _add_ operation.

On the other hand, we do not do sorting at each occasion of `find(value)` neither. But rather, we sort on demand, _i.e._ only when the list is updated. As a result, we **_amortize_** the cost of the sorting over the time. And this is the optimization trick for the solution to pass the online judge.

**Complexity Analysis**

- Time Complexity:

    - For the `add(number)` function: $$\mathcal{O}(1)$$, since we simply append the element into the list.

    - For the `find(value)` function: $$\mathcal{O}(N \cdot \log(N))$$. In the worst case, we would need to sort the list first, which is of $$\mathcal{O}(N \cdot \log(N))$$  time complexity normally. And later, again in the worst case we need to iterate through the entire list, which is of $$\mathcal{O}(N)$$ time complexity. As a result, the overall time complexity of the function lies on  $$\mathcal{O}(N \cdot \log(N))$$ of the sorting operation, which dominates over the later iteration part.

- Space Complexity: the overall space complexity of the data structure is $$\mathcal{O}(N)$$ where $$N$$ is the total number of _numbers_ that have been added.
<br/>
<br/>

---
#### Approach 2: HashTable

**Intuition**

As an alternative solution to the original [Two Sum](https://leetcode.com/problems/two-sum/) problem, one could employ the _HashTable_ to index each number.

>Given a desired sum value `S`, for each number `a`, we just need to verify if there exists a complement number (`S-a`) in the table.

As we know, the data structure of hashtable could offer us a quick _lookup_ as well as _insertion_ operations, which fits well with the above requirements.


**Algorithm**

- First, we initialize a _hashtable_ container in our data structure.

- For the `add(number)` function, we build a frequency hashtable with the _number_ as key and the frequency of the _number_ as the value in the table.

- For the `find(value)` function, we then iterate through the hashtable over the keys. For each key (`number`), we check if there exists a complement (`value - number`) in the table. If so, we could terminate the loop and return the result.

- In a particular case, where the number and its complement are equal, we then need to check if there exists _at least_ **two copies** of the _number_ in the table.

We illustrate the algorithm in the following figure:

![pic](../Figures/170/170_hashtable.png)

<iframe src="https://leetcode.com/playground/RY8zFnba/shared" frameBorder="0" width="100%" height="500" name="RY8zFnba"></iframe>


**Complexity Analysis**

- Time Complexity:

    - For the `add(number)` function: $$\mathcal{O}(1)$$, since it takes a constant time to update an entry in hashtable.

    - For the `find(value)` function: $$\mathcal{O}(N)$$, where $$N$$ is the total number of **unique** _numbers_. In the worst case, we would iterate through the entire table.

- Space Complexity: $$\mathcal{O}(N)$$, where $$N$$ is the total number of **unique** _numbers_ that we will see during the usage of the data structure.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Trade off in this problem should be considered
- Author: syftalent
- Creation Date: Sun Dec 27 2015 05:14:42 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 03:29:40 GMT+0800 (Singapore Standard Time)

<p>
The big data test only have the condition that lots of add and few find. In fact, there has to be one operation's time complexity is O(n) and the other is O(1), no matter add or find. So clearly there's trade off when solve this problem, prefer quick find or quick add.   

If consider more find and less add or we only care time complexity in finding.For example, add operation can be pre-done.
 

    public class TwoSum {
            Set<Integer> sum;
            Set<Integer> num;
            
            TwoSum(){
                sum = new HashSet<Integer>();
                num = new HashSet<Integer>();
            }
            // Add the number to an internal data structure.
        	public void add(int number) {
        	    if(num.contains(number)){
        	        sum.add(number * 2);
        	    }else{
        	        Iterator<Integer> iter = num.iterator();
        	        while(iter.hasNext()){
        	            sum.add(iter.next() + number);
        	        }
        	        num.add(number);
        	    }
        	}
        
            // Find if there exists any pair of numbers which sum is equal to the value.
        	public boolean find(int value) {
        	    return sum.contains(value);
        	}
        }

On the other side

    public class TwoSum {
        Map<Integer,Integer> hm;
        
        TwoSum(){
            hm = new HashMap<Integer,Integer>();
        }
        // Add the number to an internal data structure.
    	public void add(int number) {
    	    if(hm.containsKey(number)){
    	        hm.put(number,2);
    	    }else{
    	        hm.put(number,1);
    	    }
    	}
    
        // Find if there exists any pair of numbers which sum is equal to the value.
    	public boolean find(int value) {
    	    Iterator<Integer> iter = hm.keySet().iterator();
    	    while(iter.hasNext()){
    	        int num1 = iter.next();
    	        int num2 = value - num1;
    	        if(hm.containsKey(num2)){
    	            if(num1 != num2 || hm.get(num2) == 2){
    	                return true;
    	            }
    	        }
    	    }
    	    return false;
    	}
    }
</p>


### Beats 100% Java Code
- Author: FreeLikeWind
- Creation Date: Thu Dec 31 2015 03:08:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 15:34:42 GMT+0800 (Singapore Standard Time)

<p>
Achieved by only maintaining a list with distinct elements.

    public class TwoSum {
        private List<Integer> list = new ArrayList<Integer>();
        private Map<Integer, Integer> map = new HashMap<Integer, Integer>();
    
        // Add the number to an internal data structure.
    	public void add(int number) {
    	    if (map.containsKey(number)) map.put(number, map.get(number) + 1);
    	    else {
    	        map.put(number, 1);
    	        list.add(number);
    	    }
    	}
    
        // Find if there exists any pair of numbers which sum is equal to the value.
    	public boolean find(int value) {
    	    for (int i = 0; i < list.size(); i++){
    	        int num1 = list.get(i), num2 = value - num1;
    	        if ((num1 == num2 && map.get(num1) > 1) || (num1 != num2 && map.containsKey(num2))) return true;
    	    }
    	    return false;
    	}
    }
</p>


### My solutions in Java, C++, and Python. O(1) time for add, O(n) time for find, O(n) space
- Author: xcv58
- Creation Date: Fri Dec 26 2014 12:34:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 15:34:32 GMT+0800 (Singapore Standard Time)

<p>
I use HashMap to store times of number be added.

When find be called, we iterate the keys of HashMap, then find another number minus by value.
Then combine the detections together.

Java:

    public class TwoSum {
        private HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
    
        public void add(int number) {
            map.put(number, map.containsKey(number) ? map.get(number) + 1 : 1);
        }
    
        public boolean find(int value) {
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                int i = entry.getKey();
                int j = value - i;
                if ((i == j && entry.getValue() > 1) || (i != j && map.containsKey(j))) {
                    return true;
                }
            }
            return false;
        }
    }

C++:

    class TwoSum {
        unordered_map<int,int> map;
    public:
        void add(int number) {
            map[number]++;
        }
    
        bool find(int value) {
            for (unordered_map<int,int>::iterator it = map.begin(); it != map.end(); it++) {
                int i = it->first;
                int j = value - i;
                if ((i == j && it->second > 1) || (i != j && map.find(j) != map.end())) {
                    return true;
                }
            }
            return false;
        }
    };

Python:

    class TwoSum:
    
        # initialize your data structure here
        def __init__(self):
            self.table = dict()
    
        # @return nothing
        def add(self, number):
            self.table[number] = self.table.get(number, 0) + 1;
    
        # @param value, an integer
        # @return a Boolean
        def find(self, value):
            for i in self.table.keys():
                j = value - i
                if i == j and self.table.get(i) > 1 or i != j and self.table.get(j, 0) > 0:
                    return True
            return False
</p>


