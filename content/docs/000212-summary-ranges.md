---
title: "Summary Ranges"
weight: 212
#id: "summary-ranges"
---
## Description
<div class="description">
<p>You are given a <strong>sorted unique</strong> integer array <code>nums</code>.</p>

<p>Return <em>the <strong>smallest sorted</strong> list of ranges that <strong>cover all the numbers in the array exactly</strong></em>. That is, each element of <code>nums</code> is covered by exactly one of the ranges, and there is no integer <code>x</code> such that <code>x</code> is in one of the ranges but not in <code>nums</code>.</p>

<p>Each range <code>[a,b]</code> in the list should be output as:</p>

<ul>
	<li><code>&quot;a-&gt;b&quot;</code> if <code>a != b</code></li>
	<li><code>&quot;a&quot;</code> if <code>a == b</code></li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,1,2,4,5,7]
<strong>Output:</strong> [&quot;0-&gt;2&quot;,&quot;4-&gt;5&quot;,&quot;7&quot;]
<strong>Explanation:</strong> The ranges are:
[0,2] --&gt; &quot;0-&gt;2&quot;
[4,5] --&gt; &quot;4-&gt;5&quot;
[7,7] --&gt; &quot;7&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,2,3,4,6,8,9]
<strong>Output:</strong> [&quot;0&quot;,&quot;2-&gt;4&quot;,&quot;6&quot;,&quot;8-&gt;9&quot;]
<strong>Explanation:</strong> The ranges are:
[0,0] --&gt; &quot;0&quot;
[2,4] --&gt; &quot;2-&gt;4&quot;
[6,6] --&gt; &quot;6&quot;
[8,9] --&gt; &quot;8-&gt;9&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = []
<strong>Output:</strong> []
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [-1]
<strong>Output:</strong> [&quot;-1&quot;]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> nums = [0]
<strong>Output:</strong> [&quot;0&quot;]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= nums.length &lt;= 20</code></li>
	<li><code>-2<sup>31</sup> &lt;= nums[i] &lt;= 2<sup>31</sup> - 1</code></li>
	<li>All the values of <code>nums</code> are <strong>unique</strong>.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Capital One - 3 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Indeed - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)

## Official Solution
## Solution

**Intuition**

A range covers consecutive elements. If two adjacent elements have difference larger than $$1$$, the two elements does not belong to the same range.

**Algorithm**

To summarize the ranges, we need to know how to separate them. The array is sorted and without duplicates. In such array, two adjacent elements have difference either 1 or larger than 1. If the difference is 1, they should be put in the same range; otherwise, separate ranges.

We also need to know the start index of a range so that we can put it in the result list. Thus, we keep two indices, representing the two boundaries of current range. For each new element, we check if it extends the current range. If not, we put the current range into the list.

Don't forget to put the last range into the list. One can do this by either a special condition in the loop or putting the last range in to the list after the loop.

**Java**

```java
public class Solution {
    public List<String> summaryRanges(int[] nums) {
        List<String> summary = new ArrayList<>();
        for (int i = 0, j = 0; j < nums.length; ++j) {
            // check if j + 1 extends the range [nums[i], nums[j]]
            if (j + 1 < nums.length && nums[j + 1] == nums[j] + 1)
                continue;
            // put the range [nums[i], nums[j]] into the list
            if (i == j)
                summary.add(nums[i] + "");
            else
                summary.add(nums[i] + "->" + nums[j]);
            i = j + 1;
        }
        return summary;
    }
}
```

**Java (Alternative)**

```java
public class Solution {
    public List<String> summaryRanges(int[] nums) {
        List<String> summary = new ArrayList<>();
        for (int i, j = 0; j < nums.length; ++j){
            i = j;
            // try to extend the range [nums[i], nums[j]]
            while (j + 1 < nums.length && nums[j + 1] == nums[j] + 1)
                ++j;
            // put the range into the list
            if (i == j)
                summary.add(nums[i] + "");
            else
                summary.add(nums[i] + "->" + nums[j]);
        }
        return summary;
    }
}
```

**Complexity Analysis**

* Time complexity : $$O(n)$$. Each element is visited constant times: either in comparison with neighbor or put in the result list.

* Space complexity : $$O(1)$$. All the auxiliary space we need is the two indices, if we don't count the return list.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted JAVA solution--easy to understand
- Author: tanchiyuxx
- Creation Date: Fri Jun 26 2015 13:44:48 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 04:47:03 GMT+0800 (Singapore Standard Time)

<p>
    List<String> list=new ArrayList();
    	if(nums.length==1){
    		list.add(nums[0]+"");
    		return list;
    	}
        for(int i=0;i<nums.length;i++){
        	int a=nums[i];
        	while(i+1<nums.length&&(nums[i+1]-nums[i])==1){
        		i++;
        	}
        	if(a!=nums[i]){
        		list.add(a+"->"+nums[i]);
        	}else{
        		list.add(a+"");
        	}
        }
        return list;
</p>


### 6 lines in Python
- Author: StefanPochmann
- Creation Date: Fri Jun 26 2015 02:47:01 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 22:25:40 GMT+0800 (Singapore Standard Time)

<p>
Three versions of the same algorithm, all take O(n) time.

---

**Solution 1**

Just collect the ranges, then format and return them.

    def summaryRanges(self, nums):
        ranges = []
        for n in nums:
            if not ranges or n > ranges[-1][-1] + 1:
                ranges += [],
            ranges[-1][1:] = n,
        return ['->'.join(map(str, r)) for r in ranges]

---

**Solution 2**

A variation of solution 1, holding the current range in an extra variable `r` to make things easier. Note that `r` contains at most two elements, so the `in`-check takes constant time.

    def summaryRanges(self, nums):
        ranges, r = [], []
        for n in nums:
            if n-1 not in r:
                r = []
                ranges += r,
            r[1:] = n,
        return ['->'.join(map(str, r)) for r in ranges]

---

**Solution 3**

A tricky short version.

    def summaryRanges(self, nums):
        ranges = r = []
        for n in nums:
            if `n-1` not in r:
                r = []
                ranges += r,
            r[1:] = `n`,
        return map('->'.join, ranges)

---

**About the commas :-)**

Three people asked about them in the comments, so I'll also explain it here as well. I have these two basic cases:

    ranges += [],
    r[1:] = n,

Why the trailing commas? Because it turns the right hand side into a tuple and I get the same effects as these more common alternatives:

    ranges += [[]]
    or
    ranges.append([])

    r[1:] = [n]

Without the comma, ...

 - `ranges += []` wouldn't add `[]` itself but only its elements, i.e., nothing.
 - `r[1:] = n` wouldn't work, because my `n` is not an iterable.

Why do it this way instead of the more common alternatives I showed above? Because it's shorter and faster (according to tests I did a while back).
</p>


### 10 line c++ easy understand
- Author: lchen77
- Creation Date: Fri Jun 26 2015 06:12:42 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 26 2015 06:12:42 GMT+0800 (Singapore Standard Time)

<p>
       vector<string> summaryRanges(vector<int>& nums) {
        const int size_n = nums.size();
        vector<string> res;
        if ( 0 == size_n) return res;
        for (int i = 0; i < size_n;) {
            int start = i, end = i;
            while (end + 1 < size_n && nums[end+1] == nums[end] + 1) end++;
            if (end > start) res.push_back(to_string(nums[start]) + "->" + to_string(nums[end]));
            else res.push_back(to_string(nums[start]));
            i = end+1;
        }
        return res;
    }
</p>


