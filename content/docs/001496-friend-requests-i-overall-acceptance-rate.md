---
title: "Friend Requests I: Overall Acceptance Rate"
weight: 1496
#id: "friend-requests-i-overall-acceptance-rate"
---
## Description
<div class="description">
In social network like Facebook or Twitter, people send friend requests and accept others&rsquo; requests as well. Now given two tables as below:
<p>&nbsp;</p>
Table: <code>friend_request</code>

<pre>
| sender_id | send_to_id |request_date|
|-----------|------------|------------|
| 1         | 2          | 2016_06-01 |
| 1         | 3          | 2016_06-01 |
| 1         | 4          | 2016_06-01 |
| 2         | 3          | 2016_06-02 |
| 3         | 4          | 2016-06-09 |
</pre>

<p>&nbsp;</p>
Table: <code>request_accepted</code>

<pre>
| requester_id | accepter_id |accept_date |
|--------------|-------------|------------|
| 1            | 2           | 2016_06-03 |
| 1            | 3           | 2016-06-08 |
| 2            | 3           | 2016-06-08 |
| 3            | 4           | 2016-06-09 |
| 3            | 4           | 2016-06-10 |
</pre>

<p>&nbsp;</p>
Write a query to find the overall acceptance rate of requests rounded to 2 decimals, which is the number of acceptance divide the number of requests.

<p>&nbsp;</p>
For the sample data above, your query should return the following result.

<p>&nbsp;</p>

<pre>
|accept_rate|
|-----------|
|       0.80|
</pre>

<p>&nbsp;</p>
<b>Note:</b>

<ul>
	<li>The accepted requests are not necessarily from the table <code>friend_request</code>. In this case, you just need to simply count the total accepted requests (no matter whether they are in the original requests), and divide it by the number of requests to get the acceptance rate.</li>
	<li>It is possible that a sender sends multiple requests to the same receiver, and a request could be accepted more than once. In this case, the &lsquo;duplicated&rsquo; requests or acceptances are only counted once.</li>
	<li>If there is no requests at all, you should return 0.00 as the accept_rate.</li>
</ul>

<p>&nbsp;</p>
<b>Explanation:</b> There are 4 unique accepted requests, and there are 5 requests in total. So the rate is 0.80.

<p>&nbsp;</p>
<b>Follow-up:</b>

<ul>
	<li>Can you write a query to return the accept rate but for every month?</li>
	<li>How about the cumulative accept rate for every day?</li>
</ul>

</div>

## Tags


## Companies
- Facebook - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `round` and `ifnull` [Accepted]

**Intuition**

Count the accepted requests and then divides it by the number of all requests.

**Algorithm**

To get the distinct number of accepted requests, we can query from the **request_accepted** table.
```sql
select count(*) from (select distinct requester_id, accepter_id from request_accepted;
```

With the same technique, we can have the total number of requests from the **friend_request** table:
```sql
select count(*) from (select distinct sender_id, send_to_id from friend_request;
```

At last, divide these two numbers and [`round`](https://dev.mysql.com/doc/refman/5.7/en/mathematical-functions.html#function_round) it to a scale of 2 decimal places to get the required acceptance rate.

Wait! The divisor (total number of requests) could be '0' if the table **friend_request** is empty. So, we have to utilize  [`ifnull`](https://dev.mysql.com/doc/refman/5.7/en/control-flow-functions.html#function_ifnull) to deal with this special case.

**MySQL**

```sql
select
round(
    ifnull(
    (select count(*) from (select distinct requester_id, accepter_id from request_accepted) as A)
    /
    (select count(*) from (select distinct sender_id, send_to_id from friend_request) as B),
    0)
, 2) as accept_rate;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Detailed Explaination for Question and 2 follow-up
- Author: letscoding_john
- Creation Date: Wed Aug 14 2019 23:26:59 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 14 2019 23:26:59 GMT+0800 (Singapore Standard Time)

<p>
This post is a summary of the solutions to all these 3 questions.
##### First, the original question:
This is simple since we just need to count unique request number and divide with each other.
```
select ifnull(round((count(distinct requester_id,accepter_id)/count(distinct sender_id,send_to_id)),2),0.00) as accept_rate
from friend_request, request_accepted
```
##### Second, follow-up1:Can you write a query to return the accept rate but for every month?
This solution comes from this post:
https://leetcode.com/problems/friend-requests-i-overall-acceptance-rate/discuss/103579/Following-up-questions.-Solved-Q1-how-to-solve-Q2
```
##  create a subquery which contains count, month, join each other with month and group by month so we can get the finally result.
select if(d.req =0, 0.00, round(c.acp/d.req,2)) as accept_rate, c.month from 
(select count(distinct requester_id, accepter_id) as acp, Month(accept_date) as month from request_accepted) c, 
(select count(distinct sender_id, send_to_id) as req, Month(request_date) as month from friend_request) d 
where c.month = d.month 
group by c.month
```
##### Third, follow-up2:How about the cumulative accept rate for every day?
This solution comes from this post:
https://leetcode.com/problems/friend-requests-i-overall-acceptance-rate/discuss/103583/one-possible-answer-for-question-2-cumulative-sums-by-day.
```
## sum up the case when ind is \'a\', which means it belongs to accept table, divided by sum of ind is \'r\', which means it belong to request table
select s.date1, ifnull(round(sum(case when t.ind = \'a\' then t.cnt else 0 end)/sum(case when t.ind = \'r\' then t.cnt else 0 end),2),0) 
from
## get a table of all unique dates
(select distinct x.request_date as date1 from friend_request x
## The reason here use union sicne we don\'t want duplicate date
union 
 select distinct y.accept_date as date1 from request_accepted y 
) s
## left join to make sure all dates are in the final output
left join 
## get a table of all dates, count of each days, ind to indicate which table it comes from
(select v.request_date as date1, count(*) as cnt,\'r\' as ind from friend_request v group by v.request_date
## The reason here use union all sicne union all will be faster
union all
select w.accept_date as date1, count(*) as cnt,\'a\' as ind from request_accepted w group by w.accept_date) t
## s.date1 >= t.date1, which for each reacord in s, it will join with all records earlier than it in t
on s.date1 >= t.date1
# group by s.date1 then we can get a cumulative result to that day
group by s.date1
order by s.date1
;
```
before final group by the reslut is like this:
![image](https://assets.leetcode.com/users/letscoding_john/image_1565796221.png)

after group by is like this:
![image](https://assets.leetcode.com/users/letscoding_john/image_1565796228.png)

</p>


### Easy no subquery AC solution
- Author: Joy4fun
- Creation Date: Mon Oct 30 2017 07:04:32 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 21:43:39 GMT+0800 (Singapore Standard Time)

<p>

	select 
	ifnull((select round(count(distinct requester_id,accepter_id)/count(distinct sender_id, send_to_id),2)
	from request_accepted, friend_request),0)
	as accept_rate
</p>


### Strange test case...
- Author: Hanafubuki
- Creation Date: Sun May 21 2017 02:33:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 21 2017 02:33:31 GMT+0800 (Singapore Standard Time)

<p>
Accepted solution:
```
SELECT ROUND(IFNULL(COUNT(DISTINCT requester_id , accepter_id) / COUNT(DISTINCT sender_id, send_to_id), 0), 2) AS accept_rate
FROM friend_request, request_accepted
```
But there is a test case:
```
Input: {
	"headers": {
		"friend_request":["sender_id","send_to_id","request_date"],
		"request_accepted":["requester_id","accepter_id","accept_date"]
	},
 	"rows":{
 		"friend_request":[[1,2,"2016/06/01"],[1,3,"2016/06/01"],[1,4,"2016/06/01"],[2,3,"2016/06/02"]],
		"request_accepted":[[1,2,"2016/06/03"],[1,3,"2016/06/08"],[2,3,"2016/06/08"],[3,4,"2016/06/09"]]
	}
}
```
I think it should output 0.75 while the expected answer is 1.00
</p>


