---
title: "Find Cumulative Salary of an Employee"
weight: 1489
#id: "find-cumulative-salary-of-an-employee"
---
## Description
<div class="description">
<p>The <b>Employee</b> table holds the salary information in a year.</p>

<p>Write a SQL to get the cumulative sum of an employee&#39;s salary over a period of 3 months but exclude the most recent month.</p>

<p>The result should be displayed by &#39;Id&#39; ascending, and then by &#39;Month&#39; descending.</p>

<p><b>Example</b><br />
<b>Input</b></p>

<pre>
| Id | Month | Salary |
|----|-------|--------|
| 1  | 1     | 20     |
| 2  | 1     | 20     |
| 1  | 2     | 30     |
| 2  | 2     | 30     |
| 3  | 2     | 40     |
| 1  | 3     | 40     |
| 3  | 3     | 60     |
| 1  | 4     | 60     |
| 3  | 4     | 70     |
</pre>
<b>Output</b>

<pre>

| Id | Month | Salary |
|----|-------|--------|
| 1  | 3     | 90     |
| 1  | 2     | 50     |
| 1  | 1     | 20     |
| 2  | 1     | 20     |
| 3  | 3     | 100    |
| 3  | 2     | 40     |
</pre>

<p>&nbsp;</p>
<b>Explanation</b>

<p>Employee &#39;1&#39; has 3 salary records for the following 3 months except the most recent month &#39;4&#39;: salary 40 for month &#39;3&#39;, 30 for month &#39;2&#39; and 20 for month &#39;1&#39;<br />
So the cumulative sum of salary of this employee over 3 months is 90(40+30+20), 50(30+20) and 20 respectively.</p>

<pre>
| Id | Month | Salary |
|----|-------|--------|
| 1  | 3     | 90     |
| 1  | 2     | 50     |
| 1  | 1     | 20     |
</pre>
Employee &#39;2&#39; only has one salary record (month &#39;1&#39;) except its most recent month &#39;2&#39;.

<pre>
| Id | Month | Salary |
|----|-------|--------|
| 2  | 1     | 20     |
</pre>

<p>&nbsp;</p>
Employ &#39;3&#39; has two salary records except its most recent pay month &#39;4&#39;: month &#39;3&#39; with 60 and month &#39;2&#39; with 40. So the cumulative salary is as following.

<pre>
| Id | Month | Salary |
|----|-------|--------|
| 3  | 3     | 100    |
| 3  | 2     | 40     |
</pre>

<p>&nbsp;</p>

</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach: Using `OUTER JOIN` and temporary tables

**Intuition**

Solve this issue by two steps. The first one is to get the cumulative sum of an employee's salary over a period of 3 months, and then exclude the most recent month from the result.

**Algorithm**

If you feel hard to work out how to get the cumulative sum of an employee's salary over a period of 3 months, think about 2 months as a start. By joining this **Employee** table with itself, you can get salary information for one more month.

```sql
SELECT *
FROM
    Employee E1
        LEFT JOIN
    Employee E2 ON (E2.id = E1.id
        AND E2.month = E1.month - 1)
ORDER BY E1.id ASC , E1. month DESC
```

| Id | Month | Salary | Id | Month | Salary |
|----|-------|--------|----|-------|--------|
| 1  | 4     | 60     | 1  | 3     | 40     |
| 1  | 3     | 40     | 1  | 2     | 30     |
| 1  | 2     | 30     | 1  | 1     | 20     |
| 1  | 1     | 20     |    |       |        |
| 2  | 2     | 30     | 2  | 1     | 20     |
| 2  | 1     | 20     |    |       |        |
| 3  | 4     | 70     | 3  | 3     | 60     |
| 3  | 3     | 60     | 3  | 2     | 40     |
| 3  | 2     | 40     |    |       |        |
>Note:
> - The blank value in the output is actually `NULL` in the database.
> - The first three columns are from E1, and the rest ones are from E2.

Then we can add the salary to get the cumulative sum for 2 months.

```sql
SELECT
    E1.id,
    E1.month,
    (IFNULL(E1.salary, 0) + IFNULL(E2.salary, 0)) AS Salary
FROM
    Employee E1
        LEFT JOIN
    Employee E2 ON (E2.id = E1.id
        AND E2.month = E1.month - 1)
ORDER BY E1.id ASC , E1.month DESC
```
```
| id | month | Salary |
|----|-------|--------|
| 1  | 4     | 100    |
| 1  | 3     | 70     |
| 1  | 2     | 50     |
| 1  | 1     | 20     |
| 2  | 2     | 50     |
| 2  | 1     | 20     |
| 3  | 4     | 130    |
| 3  | 3     | 100    |
| 3  | 2     | 40     |
```

Similarly, you can join this table one more time to get the cumulative sum for 3 months.

```sql
SELECT
    E1.id,
    E1.month,
    (IFNULL(E1.salary, 0) + IFNULL(E2.salary, 0) + IFNULL(E3.salary, 0)) AS Salary
FROM
    Employee E1
        LEFT JOIN
    Employee E2 ON (E2.id = E1.id
        AND E2.month = E1.month - 1)
        LEFT JOIN
    Employee E3 ON (E3.id = E1.id
        AND E3.month = E1.month - 2)
ORDER BY E1.id ASC , E1.month DESC
;
```
```
| id | month | Salary |
|----|-------|--------|
| 1  | 4     | 130    |
| 1  | 3     | 90     |
| 1  | 2     | 50     |
| 1  | 1     | 20     |
| 2  | 2     | 50     |
| 2  | 1     | 20     |
| 3  | 4     | 170    |
| 3  | 3     | 100    |
| 3  | 2     | 40     |
```

In addition, we have to exclude the most recent month as required. If we have a temp table including every id and most recent month like below, then we can easily opt out these months by join it with the above table.
```
| id | month |
|----|-------|
| 1  | 4     |
| 2  | 2     |
| 3  | 4     |
```

Here is the code to generate this table.
```sql
SELECT
    id, MAX(month) AS month
FROM
    Employee
GROUP BY id
HAVING COUNT(*) > 1
;
```

At last, we can join them together and get the desired cumulative sum of an employee's salary over a period of 3 months excluding the most recent one.

**MySQL**

```sql
SELECT
    E1.id,
    E1.month,
    (IFNULL(E1.salary, 0) + IFNULL(E2.salary, 0) + IFNULL(E3.salary, 0)) AS Salary
FROM
    (SELECT
        id, MAX(month) AS month
    FROM
        Employee
    GROUP BY id
    HAVING COUNT(*) > 1) AS maxmonth
        LEFT JOIN
    Employee E1 ON (maxmonth.id = E1.id
        AND maxmonth.month > E1.month)
        LEFT JOIN
    Employee E2 ON (E2.id = E1.id
        AND E2.month = E1.month - 1)
        LEFT JOIN
    Employee E3 ON (E3.id = E1.id
        AND E3.month = E1.month - 2)
ORDER BY id ASC , month DESC
;
```

| id | month | Salary |
|----|-------|--------|
| 1  | 3     | 90     |
| 1  | 2     | 50     |
| 1  | 1     | 20     |
| 2  | 1     | 20     |
| 3  | 3     | 100    |
| 3  | 2     | 40     |

Note: Thank [@xiaxin](https://discuss.leetcode.com/user/xiaxin) for providing this elegant solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Old school solution 2 (without correlated subqueries)
- Author: sergeyvd
- Creation Date: Sat Jan 20 2018 12:36:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 09:00:07 GMT+0800 (Singapore Standard Time)

<p>
People avoid correlated subqueries, so here is a solution without subqueries:
```
SELECT   A.Id, MAX(B.Month) as Month, SUM(B.Salary) as Salary
FROM     Employee A, Employee B
WHERE    A.Id = B.Id AND B.Month BETWEEN (A.Month-3) AND (A.Month-1)
GROUP BY A.Id, A.Month
ORDER BY Id, Month DESC
```
I don't find stacks of nested ifnull() and joins appealing.
</p>


### Incorrect Test case .......Eat lot of my time
- Author: pathak007
- Creation Date: Sun Jun 11 2017 03:09:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 11 2017 03:09:11 GMT+0800 (Singapore Standard Time)

<p>
Hi moderators Test case are not correct Please correct the same
INPUT
{"headers": {"Employee": ["Id", "Month", "Salary"]}, "rows": {"Employee": [[1, 1, 20], [2, 1, 20], [1, 2, 30], [2, 2, 30], [3,2,40],[1,3,40], [3,3,60],[1,4,60],[3,4,70],[1,5,70]]}}
OUTPUT:
{"headers": ["Id", "Month", "Salary"], "values": [[1, 4, 150], [1, 3, 90], [1, 2, 50], [1, 1, 20], [2, 1, 20], [3, 3, 100], [3, 2, 40]]}
EXPECTED :
{"headers": ["Id", "Month", "Salary"], "values": [[1, 4, 130], [1, 3, 90], [1, 2, 50], [1, 1, 20], [2, 1, 20], [3, 3, 100], [3, 2, 40]]}
</p>


### MS SQL Server and MySQL 8.0 using DENSE_RANK and ROWS .. PRECEDING
- Author: saulcruz87
- Creation Date: Sat Jun 08 2019 13:11:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 08 2019 13:14:16 GMT+0800 (Singapore Standard Time)

<p>
I usually like windows functions,  helps me to read and understand the query. This solutions also works on MySQL 8.0

```sql
SELECT id, month, Salary
FROM
(
SELECT  id, 
        month, 
		-- Every 3 months. ROWS 2 PRECEDING indicates the number of rows or values to precede the current row (1 + 2)
        SUM(salary) OVER(PARTITION BY id  ORDER BY month ROWS 2 PRECEDING) as Salary, 
        DENSE_RANK() OVER(PARTITION BY id ORDER by month DESC) month_no
FROM Employee
)  src
--  exclude the most recent month
where month_no > 1
ORDER BY id , month desc
```
</p>


