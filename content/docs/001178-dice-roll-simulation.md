---
title: "Dice Roll Simulation"
weight: 1178
#id: "dice-roll-simulation"
---
## Description
<div class="description">
<p>A die simulator generates a random number from 1 to 6 for each roll.&nbsp;You introduced a constraint to the generator such that it cannot roll the number <code>i</code> more than <code>rollMax[i]</code> (1-indexed) <strong>consecutive</strong> times.&nbsp;</p>

<p>Given an array of integers&nbsp;<code>rollMax</code>&nbsp;and an integer&nbsp;<code>n</code>, return the number of distinct sequences that can be obtained with exact <code>n</code> rolls.</p>

<p>Two sequences are considered different if at least one element differs from each other. Since the answer&nbsp;may be too large,&nbsp;return it modulo <code>10^9 + 7</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 2, rollMax = [1,1,2,2,2,3]
<strong>Output:</strong> 34
<strong>Explanation:</strong> There will be 2 rolls of die, if there are no constraints on the die, there are 6 * 6 = 36 possible combinations. In this case, looking at rollMax array, the numbers 1 and 2 appear at most once consecutively, therefore sequences (1,1) and (2,2) cannot occur, so the final answer is 36-2 = 34.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2, rollMax = [1,1,1,1,1,1]
<strong>Output:</strong> 30
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 3, rollMax = [1,1,1,2,2,3]
<strong>Output:</strong> 181
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 5000</code></li>
	<li><code>rollMax.length == 6</code></li>
	<li><code>1 &lt;= rollMax[i] &lt;= 15</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 3 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Citrix - 2 (taggedByAdmin: false)
- Akuna Capital - 5 (taggedByAdmin: true)
- Bloomberg - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short Python DP with detailed image explanation
- Author: zzzliu
- Creation Date: Mon Oct 14 2019 16:12:03 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 15 2019 10:39:28 GMT+0800 (Singapore Standard Time)

<p>
Here I illustrate the idea with the example: Input: n = 3, rollMax = [1,1,1,2,2,3]. Output: 181.

Also the definition of DP array:
- for `j = 0, 1, ..., faces - 1`
	- `dp[i][j]` means how many combinations it could be that at `i-th` rolling and the last face is `j`
- for `j = faces`
	- `dp[i][j]` means how many combinations it could be that at `i-th` rolling in total

Basically on every `[i][j]`, we are trying to climb up in the column `[j]`, and how many steps we could climb up is based on `rollMax[j]`.


![image](https://assets.leetcode.com/users/zzzliu/image_1571040537.png)


------
Another example: let\'s say we roll dice `i = 5` times, and the last face is `j = 5`, then we have this sequence `xxxx5` where `x` could ranges from `0` to `5` arbitrarily if we are not restricted. Then let\'s calculate how many combinations we could form `xxxx5` with constraint `rollMax[j] = 3`. They could be categorized as follow:
1. step1: `xxxy5`, where x in `[0, 5]` and y in `[0, 4]`, this means from the end `5`, we have consecutive one `5`
2. step2: `xxy55`, this means from the end `5`, we have consecutive two `5`
3. step3: `xy555`, this means from the end `5`, we have consecutive three `5`
4. We could not climb the column upper because we are restricted to only be able to climb up `rollMax[j] = 3` steps.

Then the question is how to calculate the combinations of `...xy`. Actually if you take a deeper look, what `...xy` means is essentially: give me all the combinations as long as the last face is not 5. We don\'t care what `x` should be here because it is not restricted and could choose any value (it could be even same as `y`, or same as `5`). As long as `y` is not equal to `5`, we are good to go. And that is the definition `dp[i][j]`!

```
class Solution:
    def dieSimulator(self, n: int, rollMax: List[int]) -> int:
        faces = len(rollMax)
        # [n + 1][faces + 1] dimensional dp array
        dp = [[0 for i in range(faces + 1)] for j in range(n + 1)]
        
        # initialization
        # roll 0 times, the total combination is 1
        dp[0][faces] = 1
        # roll 1 times, the combinations that end at face j is 1
        for j in range(faces):
            dp[1][j] = 1
        # roll 1 times, the total combination is faces = 6
        dp[1][faces] = faces
        
        # then roll dices from 2 times, until n times
        for i in range(2, n + 1):
            # iterate through each column (face)
            for j in range(faces):
                # at each [i, j], trying to go up (decrease i) and collect all the sum of previous state
                for k in range(1, rollMax[j] + 1):
                    if i - k < 0:
                        break
                    dp[i][j] += dp[i - k][faces] - dp[i - k][j]
            # update total sum of this row
            dp[i][faces] = sum(dp[i])
        
        return dp[n][faces] % 1000000007
        
```
</p>


### [Java] Share my DP solution
- Author: zzzyq
- Creation Date: Sun Oct 13 2019 12:34:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 13 2019 13:00:10 GMT+0800 (Singapore Standard Time)

<p>
It is a 2D-DP problem. 
DP[i][j] means how many choic for total i dices and the last number is j. 
For each DP[i][j] we need to remove some invalid choice since there is a restriction for consecutive times.

Let\'s take an example:
Assuming restriction for 1 is 2 times and we meet the case **axxb** (a, b are indexes of uncertain values). We all know we can not take 1 at index-b when xx = \'11\', these are all invaild so we need to remove them.

First of all, the total possible cases of dp[b][1] are sum of dp[b-1][1~6]
for index-b if we want to choose 1 so we need to consider the case that the two consecutive number before b is \'11\'. Also we need to be careful about value at the index-a. It shouldn\'t be \'1\'.  In short we need to remove all possible cases of **a11** **and a is not 1**.
The transition equation = **dp[b][1] = sum(dp[b-1][1~6]) - sum(dp[a][2~6(except 1)])**

Following is my code:
I save the sum of dp[i][1~6] at dp[i][7] for easier calculation. 
```
class Solution {
    public int dieSimulator(int n, int[] rollMax) {
        long divisor = (long)Math.pow(10, 9) + 7;
        long[][] dp = new long[n][7];
        for(int i = 0; i < 6; i++) {
            dp[0][i] = 1;
        }
        dp[0][6] = 6;
        for(int i = 1; i < n; i++) {
            long sum = 0;
            for(int j = 0; j < 6; j++) {
                dp[i][j] = dp[i - 1][6];
                if(i - rollMax[j] < 0) {
                    sum = (sum + dp[i][j]) % divisor;
                }
                else {
                    if(i - rollMax[j] - 1 >= 0) dp[i][j] = (dp[i][j] - (dp[i - rollMax[j] - 1][6] - dp[i - rollMax[j] - 1][j])) % divisor + divisor;
                    else dp[i][j] = (dp[i][j] - 1) % divisor;
                    sum = (sum + dp[i][j]) % divisor;
                }

            }
            dp[i][6] = sum;
        }
        return (int)(dp[n - 1][6]);
    }
    
}
```
</p>


### DFS + Memoization, Short and Simple
- Author: yzq789
- Creation Date: Sun Oct 13 2019 13:20:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 02 2020 16:20:30 GMT+0800 (Singapore Standard Time)

<p>
First use DFS:

```
    int ans = 0;
    
    public int dieSimulator(int n, int[] rollMax) {
        dfs(n, rollMax, -1, 0);
        return ans;
    }
    
	// dieLeft : the number of dies
	// last : last number we rolled
	// curlen : current len of same number
	// This function trys to traval all the valid permutation
    private void dfs(int dieLeft, int[] rollMax, int last, int curlen){
        if(dieLeft == 0){
            ans++;
            return;
        }
        for(int i=0; i<6; i++){
            if(i == last && curlen == rollMax[i]) continue;
            dfs(dieLeft - 1, rollMax, i, i == last ? curlen + 1 : 1);
        }
    }
```

But this will be TLE. So we add memoization:

```
    int[][][] dp = new int[5000][6][16];
    final int M = 1000000007;
    
    public int dieSimulator(int n, int[] rollMax) {
        return dfs(n, rollMax, -1, 0);
    }
    
    private int dfs(int dieLeft, int[] rollMax, int last, int curlen){
        if(dieLeft == 0) return 1;
        if(last >= 0 && dp[dieLeft][last][curlen] > 0) return dp[dieLeft][last][curlen];
        int ans = 0;
        for(int i=0; i<6; i++){
            if(i == last && curlen == rollMax[i]) continue;
            ans = (ans + dfs(dieLeft - 1, rollMax, i, i == last ? curlen + 1 : 1)) % M;
        }
        if(last >= 0) dp[dieLeft][last][curlen] = ans;
        return ans;
    }
```
</p>


