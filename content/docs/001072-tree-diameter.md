---
title: "Tree Diameter"
weight: 1072
#id: "tree-diameter"
---
## Description
<div class="description">
<p>Given an undirected tree, return&nbsp;its diameter: the number of <strong>edges</strong> in a longest path in that tree.</p>

<p>The tree is given as an array&nbsp;of&nbsp;<code>edges</code>&nbsp;where <code>edges[i] = [u, v]</code> is a bidirectional edge between nodes&nbsp;<code>u</code> and <code>v</code>.&nbsp; Each node has&nbsp;labels in the set <code>{0, 1, ..., edges.length}</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/06/14/1397_example_1.PNG" style="width: 226px; height: 233px;" /></p>

<pre>
<strong>Input:</strong> edges = [[0,1],[0,2]]
<strong>Output:</strong> 2
<strong>Explanation: </strong>
A longest path of the tree is the path 1 - 0 - 2.
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/06/14/1397_example_2.PNG" style="width: 350px; height: 316px;" /></p>

<pre>
<strong>Input:</strong> edges = [[0,1],[1,2],[2,3],[1,4],[4,5]]
<strong>Output:</strong> 4
<strong>Explanation: </strong>
A longest path of the tree is the path 3 - 2 - 1 - 4 - 5.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= edges.length &lt;&nbsp;10^4</code></li>
	<li><code>edges[i][0] != edges[i][1]</code></li>
	<li><code>0 &lt;= edges[i][j] &lt;= edges.length</code></li>
	<li>The given edges form an undirected tree.</li>
</ul>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Facebook - 4 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Depth of the Tree solution - Time O(N) - Easy to understand
- Author: hiepit
- Creation Date: Sun Nov 03 2019 02:17:58 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 02 2020 22:30:00 GMT+0800 (Singapore Standard Time)

<p>
**Explain:**
- Similar problem: https://leetcode.com/problems/diameter-of-binary-tree/
- Travese all the nodes of the tree. The diameter of the tree is maximum of the longest path through each node.
- Longest path through a node is sum of top 2 depths of children\'s tree.

**Java ~ 5ms**
```java
class Solution {
    int diameter = 0;
    public int treeDiameter(int[][] edges) {
        int n = edges.length + 1;
        List<Integer>[] graph = new List[n];
        for (int i = 0; i < n; ++i) graph[i] = new LinkedList<>();
        for (int[] e : edges) {
            graph[e[0]].add(e[1]);
            graph[e[1]].add(e[0]);
        }
        diameter = 0;
        depth(0, -1, graph);
        return diameter;
    }
    // Depth of the tree is the number of nodes along the longest path from the root node down to the farthest leaf node.
    int depth(int root, int parent, List<Integer>[] graph) {
        int maxDepth1st = 0, maxDepth2nd = 0;
        for (int child : graph[root]) {
            if (child == parent) continue; // Only one way from root node to child node, don\'t allow child node go to root node again!
            int childDepth = depth(child, root, graph);
            if (childDepth > maxDepth1st) {
                maxDepth2nd = maxDepth1st;
                maxDepth1st = childDepth;
            } else if (childDepth > maxDepth2nd) {
                maxDepth2nd = childDepth;
            }
        }
        int longestPathThroughRoot = maxDepth1st + maxDepth2nd + 1; // the number of nodes in the longest path
        diameter = Math.max(diameter, longestPathThroughRoot - 1); // diameter = number of edges = number of nodes - 1
        return maxDepth1st + 1;
    }
}
```
**Complexity**
- Time: `O(n)`, where `n` is the number of edges in the tree
- Space: `O(n)`
</p>


### (Java) Using 2 BFS
- Author: Poorvank
- Creation Date: Sun Nov 03 2019 00:01:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 03 2019 00:04:52 GMT+0800 (Singapore Standard Time)

<p>
A famous problem.
We can find longest path using two BFSs.
If we start BFS from any node \'N\' and find a node with the longest distance from \'N\',
it must be an end point of the longest path.
It can be proved using contradiction.
So the algorithm reduces to simple two BFSs.
First BFS to find an end point of the longest path and second BFS from this end point to find the actual longest path.

Proof: https://stackoverflow.com/questions/20010472/proof-of-correctness-algorithm-for-diameter-of-a-tree-in-graph-theory
```
private class Node {
        int value;
        int distance;

        public Node(int node, int distance) {
            this.value = node;
            this.distance = distance;
        }
    }

    public int treeDiameter(int[][] edges) {
        int n = edges.length+1;

        LinkedList<Integer>[] adjacencyList = new LinkedList[n];
        for(int i = 0; i < n; ++i) {
            adjacencyList[i] = new LinkedList<>();
        }

        for (int[] edge : edges) {
            adjacencyList[edge[0]].add(edge[1]);
            adjacencyList[edge[1]].add(edge[0]);
        }

        Node start = bfs(0,n,adjacencyList);
        return bfs(start.value,n,adjacencyList).distance;

    }

    private Node bfs(int u, int n, LinkedList<Integer>[] adj) {
        int[] distance = new int[n];
        Arrays.fill(distance, -1);
        Queue<Integer> queue = new LinkedList<>();
        queue.add(u);
        distance[u] = 0;
        while (!queue.isEmpty()) {
            int poll = queue.poll();
            for(int i = 0; i < adj[poll].size(); ++i) {
                int v = adj[poll].get(i);
                if(distance[v] == -1) {
                    queue.add(v);
                    distance[v] = distance[poll] + 1;
                }
            }
        }

        int maxDistance = 0;
        int val = 0;
        for(int i = 0; i < n; ++i) {
            if(distance[i] > maxDistance) {
                maxDistance = distance[i];
                val = i;
            }
        }
        return new Node(val,maxDistance);
    }
```
</p>


### C++ with picture, O(n)
- Author: votrubac
- Creation Date: Mon Nov 04 2019 13:22:41 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 04 2019 13:23:03 GMT+0800 (Singapore Standard Time)

<p>
I\'ve never solved this problem before, so it got me thinking. Having a "global" `diameter` variable seems the way to go:
- Start from `0` node.
- For every unvisited sibling, run DFS to get the depth of that route.
		- Track the maximum of any two depth in the global `diameter` variable.
		- Track the maximum depth among all directions, and return it.
- The picture below shows depth returned from running DFS on each node, with two maximums in green.

![image](https://assets.leetcode.com/users/votrubac/image_1572844905.png)

```
int dfs(vector<vector<int>>& al, vector<bool>& visited, int i, int& diameter) {
  visited[i] = true;
  auto max_depth = 0;
  for (auto j : al[i]) {
    if (!visited[j]) {
      auto depth = dfs(al, visited, j, diameter);
      diameter = max(diameter, depth + max_depth);
      max_depth = max(depth, max_depth);
    }
  }
  return i == 0 ? diameter : max_depth + 1;
}
int treeDiameter(vector<vector<int>>& edges) {
  int diameter = 0;
  vector<bool> visited(edges.size() + 1);
  vector<vector<int>> al(edges.size() + 1);
  for (auto& e : edges) {
    al[e[0]].push_back(e[1]);
    al[e[1]].push_back(e[0]);
  }
  return dfs(al, visited, 0, diameter);
}
```
</p>


