---
title: "Maximum Number of Ones"
weight: 1057
#id: "maximum-number-of-ones"
---
## Description
<div class="description">
<p>Consider a matrix <code>M</code> with dimensions <code>width * height</code>, such that every cell has value <code>0</code>&nbsp;or <code>1</code>, and any <strong>square</strong>&nbsp;sub-matrix of <code>M</code> of size <code>sideLength * sideLength</code>&nbsp;has at most <code>maxOnes</code>&nbsp;ones.</p>

<p>Return the maximum possible number of ones that the matrix <code>M</code> can have.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> width = 3, height = 3, sideLength = 2, maxOnes = 1
<strong>Output:</strong> 4
<strong>Explanation:</strong>
In a 3*3 matrix, no 2*2 sub-matrix can have more than 1 one.
The best solution that has 4 ones is:
[1,0,1]
[0,0,0]
[1,0,1]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> width = 3, height = 3, sideLength = 2, maxOnes = 2
<strong>Output:</strong> 6
<strong>Explanation:</strong>
[1,0,1]
[1,0,1]
[1,0,1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= width, height &lt;= 100</code></li>
	<li><code>1 &lt;= sideLength &lt;= width, height</code></li>
	<li><code>0 &lt;= maxOnes &lt;= sideLength * sideLength</code></li>
</ul>

</div>

## Tags
- Math (math)
- Sort (sort)

## Companies
- Qualcomm - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python/C++ Solution with explanation
- Author: davyjing
- Creation Date: Sun Sep 08 2019 00:25:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 03:26:21 GMT+0800 (Singapore Standard Time)

<p>
If we create the first square matrix,  the big matrix will just be the copies of this one. (translation copies)
The value of each location in the square matrix will appear at multiple locations in the big matrix, count them.
Then assign the ones in the square matrix with more occurances with 1.
```
class Solution:
    def maximumNumberOfOnes(self, width: int, height: int, sideLength: int, maxOnes: int) -> int:
        res = []
        for i in range(sideLength):
            for j in range(sideLength):
                res += [((width - i - 1) // sideLength + 1) * ((height - j - 1) // sideLength + 1)]
        res = sorted(res,reverse = True)
        return sum(res[:maxOnes])
```
Or 1-liner just for fun..
```
class Solution:
    def maximumNumberOfOnes(self, width: int, height: int, sideLength: int, maxOnes: int) -> int:
        return sum(sorted([((width-i-1)//sideLength+1)*((height-j-1)//sideLength+1) for i in range(sideLength) for j in range(sideLength)],reverse = True)[:maxOnes])
```
Or in C++
```
class Solution {
public:
    int maximumNumberOfOnes(int width, int height, int sideLength, int maxOnes) {
    std::vector<int> res;
    for(int i = 0;i < sideLength; i++){
        for(int j =0; j < sideLength; j++) res.push_back(((width-i-1)/sideLength+1)*((height-j-1)/sideLength+1));
    }
    sort(res.begin(), res.end(), greater<int>());
    int ans = 0;
    for(int i = 0;i < maxOnes; i++) ans += res[i];
    return ans;
    }
};
```
For large sideLength, we can use heap to store list res, but here sorting the whole list is faster than heapq implementation.

</p>


### Java Easy Solution with Video
- Author: kelvinchandra1024
- Creation Date: Sun Sep 08 2019 10:08:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 10:08:05 GMT+0800 (Singapore Standard Time)

<p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/FjIao84fqLg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>


### C++ O(1) solution with explanation, Totally intuitive
- Author: hiteshgupta
- Creation Date: Sun Sep 08 2019 03:02:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 05:23:59 GMT+0800 (Singapore Standard Time)

<p>
I have uploaded my worksheet solution image, and my working Code also. I know it will take your 2 minutes to understand this, but as soon as it hits you rest is self-explanatory.
![image](https://assets.leetcode.com/users/user0690f/image_1567882703.png)


My code-
```
class Solution {
public:
    int maximumNumberOfOnes(int w, int h, int s, int mo) {
        int ans=0;
        int lx,ly,nx,ny;
        lx=w%s;
        ly=h%s;
        nx=w/s;
        ny=h/s;
        
        ans=nx*ny*mo;
        if(mo<=lx*ly) 
        {
            ans+=(nx+ny+1)*mo;
            return ans;
        }
        else
        {
            ans+=(nx+ny+1)*lx*ly;
            mo-=lx*ly;
            if(nx>ny)
            {
                if(mo<=(s-lx)*ly)
                {
                    ans+=(nx)*mo;
                    return ans;
                }
                else
                {
                    ans+=(s-lx)*ly*(nx);
                    mo-=(s-lx)*ly;
                    ans+=min(mo, (s-ly)*lx )*ny;
                    return ans;
                }
            }
            else
            {
                if(mo<=(s-ly)*lx)
                {
                    ans+=(ny)*mo;
                    return ans;
                }
                else
                {
                    ans+=(s-ly)*lx*(ny);
                    mo-=(s-ly)*lx;
                    ans+=min(mo, (s-lx)*ly )*nx;
                    return ans;
                }
            }
        }
        
    }
};
```
</p>


