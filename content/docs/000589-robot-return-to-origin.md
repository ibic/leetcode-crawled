---
title: "Robot Return to Origin"
weight: 589
#id: "robot-return-to-origin"
---
## Description
<div class="description">
<p>There is a robot starting at position (0, 0), the origin, on a 2D plane. Given a sequence of its moves, judge if this robot <strong>ends up at (0, 0)</strong> after it completes its moves.</p>

<p>The move sequence is represented by a string, and the character moves[i] represents its ith move. Valid moves are R (right), L (left), U (up), and D (down). If the robot returns to the origin after it finishes all of its moves, return true. Otherwise, return false.</p>

<p><strong>Note</strong>: The way that the robot is &quot;facing&quot; is irrelevant. &quot;R&quot; will always make the robot move to the right once, &quot;L&quot; will always make it move left, etc. Also, assume that the magnitude of the robot&#39;s movement is the same for each move.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> moves = &quot;UD&quot;
<strong>Output:</strong> true
<strong>Explanation</strong>: The robot moves up once, and then down once. All moves have the same magnitude, so it ended up at the origin where it started. Therefore, we return true.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> moves = &quot;LL&quot;
<strong>Output:</strong> false
<strong>Explanation</strong>: The robot moves left twice. It ends up two &quot;moves&quot; to the left of the origin. We return false because it is not at the origin at the end of its moves.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> moves = &quot;RRDD&quot;
<strong>Output:</strong> false
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> moves = &quot;LDRRLRUULR&quot;
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= moves.length &lt;= 2 * 10<sup>4</sup></code></li>
	<li><code>moves</code> only contains the characters <code>&#39;U&#39;</code>, <code>&#39;D&#39;</code>, <code>&#39;L&#39;</code> and <code>&#39;R&#39;</code>.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Goldman Sachs - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Simulation [Accepted]

**Intuition**

We can simulate the position of the robot after each command.

**Algorithm**

Initially, the robot is at `(x, y) = (0, 0)`.  If the move is `'U'`, the robot goes to `(x, y-1)`; if the move is `'R'`, the robot goes to `(x, y) = (x+1, y)`, and so on.

<iframe src="https://leetcode.com/playground/tQUYkKDL/shared" frameBorder="0" width="100%" height="259" name="tQUYkKDL"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `moves`.  We iterate through the string.

* Space Complexity: $$O(1)$$.  In Java, our character array is $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python one liner
- Author: aditya74
- Creation Date: Sun Aug 13 2017 12:00:13 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 06:54:22 GMT+0800 (Singapore Standard Time)

<p>
```
def judgeCircle(self, moves):
    return moves.count('L') == moves.count('R') and moves.count('U') == moves.count('D')
```
</p>


### [C++] [Java] Clean Code
- Author: alexander
- Creation Date: Sun Aug 13 2017 12:00:15 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 08:45:48 GMT+0800 (Singapore Standard Time)

<p>
**C++**
```
class Solution {
public:
    bool judgeCircle(string moves) {
        int v = 0;
        int h = 0;
        for (char ch : moves) {
            switch (ch) {
                case 'U' : v++; break;
                case 'D' : v--; break;
                case 'R' : h++; break;
                case 'L' : h--; break;
            }
        }
        return v == 0 && h == 0;
    }
};
```
**Java**
```
public class Solution {
    public boolean judgeCircle(String moves) {
        int x = 0;
        int y = 0;
        for (char ch : moves.toCharArray()) {
            if (ch == 'U') y++;
            else if (ch == 'D') y--;
            else if (ch == 'R') x++;
            else if (ch == 'L') x--;
        }
        return x == 0 && y == 0;
    }
}
```
</p>


### If you can't describe the problem in a right way, please don't make it a problem.
- Author: Wizmann
- Creation Date: Tue Aug 15 2017 22:33:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 23 2018 07:31:16 GMT+0800 (Singapore Standard Time)

<p>
There are a few problems has vague descriptions which is quite confusing that make the contestants to submit multiple of time to "guess" or "try out" what is the real meaning of the problem.

It's remotely what we want in Leetcode. So please STOP making this kind of problem again. It's no more than a waste of our time.

THANK YOU!
</p>


