---
title: "DI String Match"
weight: 892
#id: "di-string-match"
---
## Description
<div class="description">
<p>Given a string <code>S</code> that <strong>only</strong> contains &quot;I&quot; (increase) or &quot;D&quot; (decrease), let <code>N = S.length</code>.</p>

<p>Return <strong>any</strong> permutation <code>A</code> of <code>[0, 1, ..., N]</code> such that for all <code>i = 0,&nbsp;..., N-1</code>:</p>

<ul>
	<li>If <code>S[i] == &quot;I&quot;</code>, then <code>A[i] &lt; A[i+1]</code></li>
	<li>If <code>S[i] == &quot;D&quot;</code>, then <code>A[i] &gt; A[i+1]</code></li>
</ul>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;IDID&quot;</span>
<strong>Output: </strong><span id="example-output-1">[0,4,1,3,2]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;III&quot;</span>
<strong>Output: </strong><span id="example-output-2">[0,1,2,3]</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">&quot;DDI&quot;</span>
<strong>Output: </strong><span id="example-output-3">[3,2,0,1]</span></pre>
</div>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= S.length &lt;= 10000</code></li>
	<li><code>S</code> only contains characters <code>&quot;I&quot;</code> or <code>&quot;D&quot;</code>.</li>
</ol>
</div>

## Tags
- Math (math)

## Companies
- Goldman Sachs - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Ad-Hoc

**Intuition**

If we see say `S[0] == 'I'`, we can always put `0` as the first element; similarly, if we see `S[0] == 'D'`, we can always put `N` as the first element.

Say we have a match for the rest of the string `S[1], S[2], ...` using `N` distinct elements.  Notice it doesn't matter what the elements are, only that they are distinct and totally ordered.  Then, putting `0` or `N` at the first character will match, and the rest of the elements (`1, 2, ..., N` or `0, 1, ..., N-1`) can use the matching we have.

**Algorithm**

Keep track of the smallest and largest element we haven't placed.  If we see an `'I'`, place the small element; otherwise place the large element.

<iframe src="https://leetcode.com/playground/zN3KM2xZ/shared" frameBorder="0" width="100%" height="327" name="zN3KM2xZ"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Nov 18 2018 12:03:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 18 2018 12:03:48 GMT+0800 (Singapore Standard Time)

<p>
**Java, Outside-in:**
```
    public int[] diStringMatch(String S) {
        int n = S.length(), left = 0, right = n;
        int[] res = new int[n + 1];
        for (int i = 0; i < n; ++i)
            res[i] = S.charAt(i) == \'I\' ? left++ : right--;
        res[n] = left;
        return res;
    }
```

**C++, Inside-out:**
```
    vector<int> diStringMatch(string S) {
        int left = count(S.begin(), S.end(), \'D\'), right = left;
        vector<int> res = {left};
        for (char &c : S)
            res.push_back(c == \'I\' ? ++right : --left);
        return res;
    }
```

**Python, Inside-out and subtract left:**
```
    def diStringMatch(self, S):
        left = right = 0
        res = [0]
        for i in S:
            if i == "I":
                right += 1
                res.append(right)
            else:
                left -= 1
                res.append(left)
        return [i - left for i in res]
```
</p>


### C++ 4 lines high-low pointers
- Author: votrubac
- Creation Date: Sun Nov 18 2018 12:04:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 18 2018 12:04:27 GMT+0800 (Singapore Standard Time)

<p>
We track high (```h = N - 1```) and low (```l = 0```) numbers within ```[0 ... N - 1]```. When we encounter \'I\', we insert the current low number and increase it. With \'D\', we insert the current high number and decrease it. In the end, ```h == l```, so we insert that last number to complete the premutation.
```
vector<int> diStringMatch(string S) {
    vector<int> res;
    for (int l = 0, h = S.size(), i = 0; i <= S.size(); ++i)
        res.push_back(i == S.size() || S[i] == \'I\' ? l++ : h--);
    return res;
}
```
</p>


### Python 5-liner easy to understand
- Author: cenkay
- Creation Date: Sun Nov 18 2018 21:04:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 18 2018 21:04:44 GMT+0800 (Singapore Standard Time)

<p>
* Think of available numbers to put as an array of [0, 1, 2, ..., len(S)]
* When you see "I", your safest option is to put lowest(leftmost) number for the next move, so it will always increase
* When you see "D", your safest option is to put highest(rightmost) number for the next move, so it will always decrease
* Don\'t forget to put latest number when l == r
```
class Solution:
    def diStringMatch(self, S):
        l, r, arr = 0, len(S), []
        for s in S:
            arr.append(l if s == "I" else r)
            l, r = l + (s == "I"), r - (s == "D")
        return arr + [l]
```
</p>


