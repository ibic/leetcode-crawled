---
title: "Integer Break"
weight: 326
#id: "integer-break"
---
## Description
<div class="description">
<p>Given a positive integer <i>n</i>, break it into the sum of <b>at least</b> two positive integers and maximize the product of those integers. Return the maximum product you can get.</p>

<p><strong>Example 1:</strong></p>

<div>
<pre>
<strong>Input: </strong><span id="example-input-1-1">2</span>
<strong>Output: </strong><span id="example-output-1">1</span>
<strong>Explanation: </strong>2 = 1 + 1, 1 &times; 1 = 1.</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">10</span>
<strong>Output: </strong><span id="example-output-2">36</span>
<strong>Explanation: </strong>10 = 3 + 3 + 4, 3 &times;&nbsp;3 &times;&nbsp;4 = 36.</pre>

<p><b>Note</b>: You may assume that <i>n</i> is not less than 2 and not larger than 58.</p>
</div>
</div>
</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Why factor 2 or 3? The math behind this problem.
- Author: lixx2100
- Creation Date: Wed Apr 20 2016 00:04:08 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 05:57:36 GMT+0800 (Singapore Standard Time)

<p>
I saw many solutions were referring to factors of 2 and 3. But why these two magic numbers? Why other factors do not work?
Let's study the math behind it.

For convenience, say **n** is sufficiently large and can be broken into any smaller real positive numbers. We now try to calculate which real number generates the largest product.
Assume we break **n** into **(n / x)**  **x**'s, then the product will be **x<sup>n/x</sup>**, and we want to maximize it.

Taking its derivative gives us **n * x<sup>n/x-2</sup> * (1 - ln(x))**.
The derivative is positive when **0 < x < e**, and equal to **0** when **x = e**, then becomes negative when **x > e**,
which indicates that the product increases as **x** increases, then reaches its maximum when **x = e**, then starts dropping.

This reveals the fact that if **n** is sufficiently large and we are allowed to break **n** into real numbers,
the best idea is to break it into nearly all **e**'s.
On the other hand, if **n** is sufficiently large and we can only break **n** into integers, we should choose integers that are closer to **e**.
The only potential candidates are **2** and **3** since **2 < e < 3**, but we will generally prefer **3** to **2**. Why?

Of course, one can prove it based on the formula above, but there is a more natural way shown as follows.

**6 = 2 + 2 + 2 = 3 + 3**. But **2 * 2 * 2 < 3 * 3**.
Therefore, if there are three **2**'s in the decomposition, we can replace them by two **3**'s to gain a larger product.

All the analysis above assumes **n** is significantly large. When **n** is small (say **n <= 10**), it may contain flaws.
For instance, when **n = 4**, we have **2 * 2 > 3 * 1**.
To fix it, we keep breaking **n** into **3**'s until **n** gets smaller than **10**, then solve the problem by brute-force.
</p>


### A simple explanation of the math part and a O(n) solution
- Author: Chuqi
- Creation Date: Sun May 15 2016 11:16:50 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 22:18:35 GMT+0800 (Singapore Standard Time)

<p>
The first thing we should consider is : What is the max product if we break a number N into two factors?

I use a function to express this product: f=x(N-x)

When x=N/2, we get the maximum of this function.

However, factors should be integers. Thus the maximum is (N/2)*(N/2) when N is even or (N-1)/2 *(N+1)/2 when N is odd.

When the maximum of f is larger than N, we should do the break.

(N/2)*(N/2)>=N, then N>=4

(N-1)/2 *(N+1)/2>=N, then N>=5

These two expressions mean that factors should be less than 4, otherwise we can do the break and get a better product. The factors in last result should be 1, 2 or 3. Obviously, 1 should be abandoned. Thus, the factors of the perfect product should be 2 or 3.

The reason why we should use 3 as many as possible is 

For 6, 3 * 3>2 * 2 * 2. Thus, the optimal product should contain no more than three 2. 

Below is my accepted, O(N) solution.

    public class Solution {
        public int integerBreak(int n) {
            if(n==2) return 1;
            if(n==3) return 2;
            int product = 1;
            while(n>4){
                product*=3;
                n-=3;
            }
            product*=n;
            
            return product;
        }
    }
</p>


### Java DP solution
- Author: pei_yang
- Creation Date: Tue Apr 19 2016 12:08:23 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 22:13:44 GMT+0800 (Singapore Standard Time)

<p>
    public int integerBreak(int n) {
           int[] dp = new int[n + 1];
           dp[1] = 1;
           for(int i = 2; i <= n; i ++) {
               for(int j = 1; j < i; j ++) {
                   dp[i] = Math.max(dp[i], (Math.max(j,dp[j])) * (Math.max(i - j, dp[i - j])));
               }
           }
           return dp[n];
        }
</p>


