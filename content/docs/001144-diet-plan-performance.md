---
title: "Diet Plan Performance"
weight: 1144
#id: "diet-plan-performance"
---
## Description
<div class="description">
<p>A dieter consumes&nbsp;<code>calories[i]</code>&nbsp;calories on the <code>i</code>-th day.&nbsp;</p>

<p>Given an integer <code>k</code>, for <strong>every</strong> consecutive sequence of <code>k</code> days (<code>calories[i], calories[i+1], ..., calories[i+k-1]</code>&nbsp;for all <code>0 &lt;= i &lt;= n-k</code>), they look at <em>T</em>, the total calories consumed during that sequence of <code>k</code> days (<code>calories[i] + calories[i+1] + ... + calories[i+k-1]</code>):</p>

<ul>
	<li>If <code>T &lt; lower</code>, they performed poorly on their diet and lose 1 point;&nbsp;</li>
	<li>If <code>T &gt; upper</code>, they performed well on their diet and gain 1 point;</li>
	<li>Otherwise, they performed normally and there is no change in points.</li>
</ul>

<p>Initially, the dieter has zero points. Return the total number of points the dieter has after dieting for <code>calories.length</code>&nbsp;days.</p>

<p>Note that the total points can be negative.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> calories = [1,2,3,4,5], k = 1, lower = 3, upper = 3
<strong>Output:</strong> 0
<strong>Explanation</strong>: Since k = 1, we consider each element of the array separately and compare it to lower and upper.
calories[0] and calories[1] are less than lower so 2 points are lost.
calories[3] and calories[4] are greater than upper so 2 points are gained.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> calories = [3,2], k = 2, lower = 0, upper = 1
<strong>Output:</strong> 1
<strong>Explanation</strong>: Since k = 2, we consider subarrays of length 2.
calories[0] + calories[1] &gt; upper so 1 point is gained.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> calories = [6,5,0,0], k = 2, lower = 1, upper = 5
<strong>Output:</strong> 0
<strong>Explanation</strong>:
calories[0] + calories[1] &gt; upper so 1 point is gained.
lower &lt;= calories[1] + calories[2] &lt;= upper so no change in points.
calories[2] + calories[3] &lt; lower so 1 point is lost.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= k &lt;= calories.length &lt;= 10^5</code></li>
	<li><code>0 &lt;= calories[i] &lt;= 20000</code></li>
	<li><code>0 &lt;= lower &lt;= upper</code></li>
</ul>

</div>

## Tags
- Array (array)
- Sliding Window (sliding-window)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### This is confusing
- Author: oohwooh
- Creation Date: Sun Sep 01 2019 12:39:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 01 2019 12:44:56 GMT+0800 (Singapore Standard Time)

<p>
The question coiuld be understood as looking at different segmentations. For example, given 

```
[6,13,8,7,10,1,12,11]
k = 6
lower = 5
upper = 37
```

The first 6 days would be [6, 13, 8, 7, 10, 1], the second would be [12, 11]. 

This is also how example 3 explains its solution.
</p>


### [Java/Python 3] Sliding window w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Sep 01 2019 12:04:29 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 05 2019 19:11:38 GMT+0800 (Singapore Standard Time)

<p>
Maintain a window of size `k`, update its sum and compare it with `lower` and `upper`.

**Java**
```
    public int dietPlanPerformance(int[] calories, int k, int lower, int upper) {
        int point = 0;
        for (int i = 0, win = 0; i < calories.length; ++i) {
            win += calories[i];
            if (i >= k - 1) {                                                       // reach a k sequence already.
                if (i > k - 1) { win -= calories[i - k]; }                          // more than k sequence already
                if (win < lower) { --point ; }
                else if (win > upper) { ++point; }
            }
        }
        return point;
    }
```

----

**Python 3**

```
    def dietPlanPerformance(self, calories: List[int], k: int, lower: int, upper: int) -> int:
        point, win = 0, 0
        for i, calory in enumerate(calories):
            win += calory
            if i >= k - 1:
                if i > k - 1:
                    win -= calories[i - k]
                if win < lower:
                    point -= 1
                elif win > upper:
                    point += 1
        return point
```

----

**Analysis:**

Time: O(n), space: O(1), where n = calories.length.

----
Note: Python 3 can be simplified as the follows at a space cost of O(n), credit to @rostam:

```
    def dietPlanPerformance(self, calories: List[int], k: int, lower: int, upper: int) -> int:
        point, win = 0, sum(calories[:k - 1])                          # space cost O(k)
        for i, calory in enumerate(calories[k - 1:], k - 1):           # space cost O(n - k)
            win += calory - (i >= k) * calories[i - k]
            point += (win > upper) - (win < lower)
        return point
```
Use `itertools.islice` to avoid space cost:
```
from itertools import islice

    def dietPlanPerformance(self, calories: List[int], k: int, lower: int, upper: int) -> int:
        point, win = 0, sum(islice(calories, k - 1))
        for i, calory in enumerate(islice(calories, k - 1, None), k - 1):
            win += calory - (i >= k) * calories[i - k]
            point += (win > upper) - (win < lower)
        return point
```

</p>


### Easy 5-line Python solution: O(N) time O(1) space
- Author: rostam
- Creation Date: Tue Sep 03 2019 00:44:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 03 2019 11:21:07 GMT+0800 (Singapore Standard Time)

<p>
Initialize score with `0` and calories in the past k days as `sum(calories[0: k])`. Start iterating through the array from `k`th day. Add the new calory consumption of the day and eliminate from the other side of the window. Check if you need to add/subtract a point and proceed.

```
def dietPlanPerformance(self, calories: List[int], k: int, lower: int, upper: int) -> int:
	score, cals = 0, sum(calories[0: k])
	if cals > upper: score += 1
	if cals < lower: score -= 1

	for i in range(k,len(calories)):
		cals = cals+calories[i]-calories[i-k]
		if cals > upper: score += 1
		if cals < lower: score -= 1

	return score
```

If you care about saving some lines by eliminating repeating code, you can simplify it as:

```
def dietPlanPerformance(self, calories: List[int], k: int, lower: int, upper: int) -> int:
	score, cals = 0, sum(calories[0: k-1])
	for i in range(k-1,len(calories)):
		cals = cals+calories[i]-(calories[i-k] if i-k >= 0 else 0)
		score += cals > upper
		score -= cals < lower

	return score
```

And even more compact:
```
def dietPlanPerformance(self, calories: List[int], k: int, lower: int, upper: int) -> int:
	score, cals = 0, sum(calories[0: k-1])
	for i in range(k-1,len(calories)):
		cals += calories[i]- calories[i-k]* (i-k >= 0)
		score += (cals > upper) - (cals < lower)
	return score
```

</p>


