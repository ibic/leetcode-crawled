---
title: "Reorder List"
weight: 143
#id: "reorder-list"
---
## Description
<div class="description">
<p>Given a singly linked list <em>L</em>: <em>L</em><sub>0</sub>&rarr;<em>L</em><sub>1</sub>&rarr;&hellip;&rarr;<em>L</em><sub><em>n</em>-1</sub>&rarr;<em>L</em><sub>n</sub>,<br />
reorder it to: <em>L</em><sub>0</sub>&rarr;<em>L</em><sub><em>n</em></sub>&rarr;<em>L</em><sub>1</sub>&rarr;<em>L</em><sub><em>n</em>-1</sub>&rarr;<em>L</em><sub>2</sub>&rarr;<em>L</em><sub><em>n</em>-2</sub>&rarr;&hellip;</p>

<p>You may <strong>not</strong> modify the values in the list&#39;s nodes, only nodes itself may be changed.</p>

<p><strong>Example 1:</strong></p>

<pre>
Given 1-&gt;2-&gt;3-&gt;4, reorder it to 1-&gt;4-&gt;2-&gt;3.</pre>

<p><strong>Example 2:</strong></p>

<pre>
Given 1-&gt;2-&gt;3-&gt;4-&gt;5, reorder it to 1-&gt;5-&gt;2-&gt;4-&gt;3.
</pre>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Amazon - 6 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Microsoft - 6 (taggedByAdmin: false)
- eBay - 4 (taggedByAdmin: false)
- Tencent - 2 (taggedByAdmin: false)
- Cisco - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Splunk - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Solution Bricks

This problem is a combination of these three easy problems:

- [Middle of the Linked List](https://leetcode.com/articles/middle-of-the-linked-list/).

- [Reverse Linked List](https://leetcode.com/articles/reverse-linked-list/).

- [Merge Two Sorted Lists](https://leetcode.com/articles/merged-two-sorted-lists/).

<br /> 
<br />


---
#### Approach 1: Reverse the Second Part of the List and Merge Two Sorted Lists

**Overview**

- Find a middle node of the linked list. 
If there are two middle nodes, return the second middle node.
Example: for the list `1->2->3->4->5->6`, the middle element is `4`.

- Once a middle node has been found, reverse the second part of the list.
Example: convert `1->2->3->4->5->6` into `1->2->3->4` and `6->5->4`.

- Now merge the two sorted lists.
Example: merge `1->2->3->4` and `6->5->4` into `1->6->2->5->3->4`.

![append](../Figures/143/overview.png)

Now let's check each algorithm part in more detail.

**Find a Middle Node**

Let's use two pointers, `slow` and `fast`. 
While the slow pointer moves one step forward `slow = slow.next`, 
the fast pointer moves two steps forward `fast = fast.next.next`, 
_i.e._ `fast` traverses twice as fast as `slow`. 
When the fast pointer reaches the end of the list, 
the slow pointer should be in the middle.

![append](../Figures/143/slow_fast.png)

<iframe src="https://leetcode.com/playground/GeNJhikz/shared" frameBorder="0" width="100%" height="174" name="GeNJhikz"></iframe>

**Reverse the Second Part of the List**

Let's traverse the list starting from the middle node `slow` and its virtual
predecessor `None`.
For each current node, save its neighbours:
the previous node `prev` and the next node `tmp = curr.next`.

While you're moving along the list, 
change the node's next pointer to point to the previous node: `curr.next = prev`,
and shift the current node to the right for the next iteration: 
`prev = curr`, `curr = tmp`. 
 
![append](../Figures/143/reverse2.png)

<iframe src="https://leetcode.com/playground/Rngn8AQV/shared" frameBorder="0" width="100%" height="242" name="Rngn8AQV"></iframe>

There is a more elegant way to do it in Python:

<iframe src="https://leetcode.com/playground/u4cutXZn/shared" frameBorder="0" width="100%" height="157" name="u4cutXZn"></iframe>

**Merge Two Sorted Lists**

This algorithm is similar to the one for list reversal.

Let's pick the first node of each list - first and second, and save their successors.
While you're traversing the list, 
set the first node's next pointer to point to the second node, 
and the second node's next pointer to point to the successor of
the first node.
For this iteration the job is done, and for the next iteration 
move to the previously saved nodes' successors.   

![append](../Figures/143/first_second.png)

<iframe src="https://leetcode.com/playground/9t4zjDtN/shared" frameBorder="0" width="100%" height="259" name="9t4zjDtN"></iframe>

Once again, there is a way to make things simple in Python

<iframe src="https://leetcode.com/playground/CKZJh2mw/shared" frameBorder="0" width="100%" height="157" name="CKZJh2mw"></iframe>

**Implementation**

Now it's time to put all the pieces together.

<iframe src="https://leetcode.com/playground/LRX8BZhb/shared" frameBorder="0" width="100%" height="500" name="LRX8BZhb"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$. There are three steps here.
To identify the middle node takes $$\mathcal{O}(N)$$ time. 
To reverse the second part of the list, one needs 
$$N/2$$ operations. The final step, to merge two lists, requires $$N/2$$
operations as well. In total, that results in $$\mathcal{O}(N)$$ time 
complexity. 

* Space complexity: $$\mathcal{O}(1)$$, since we do not allocate any 
additional data structures.
  
<br /> 
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution with 3 steps
- Author: wanqing
- Creation Date: Thu May 14 2015 05:31:52 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 04:24:01 GMT+0800 (Singapore Standard Time)

<p>
This question is a combination of **Reverse a linked list I & II**. It should be pretty straight forward to do it in 3 steps :)

    public void reorderList(ListNode head) {
                if(head==null||head.next==null) return;
                
                //Find the middle of the list
                ListNode p1=head;
                ListNode p2=head;
                while(p2.next!=null&&p2.next.next!=null){ 
                    p1=p1.next;
                    p2=p2.next.next;
                }
                
                //Reverse the half after middle  1->2->3->4->5->6 to 1->2->3->6->5->4
                ListNode preMiddle=p1;
                ListNode preCurrent=p1.next;
                while(preCurrent.next!=null){
                    ListNode current=preCurrent.next;
                    preCurrent.next=current.next;
                    current.next=preMiddle.next;
                    preMiddle.next=current;
                }
                
                //Start reorder one by one  1->2->3->6->5->4 to 1->6->2->5->3->4
                p1=head;
                p2=preMiddle.next;
                while(p1!=preMiddle){
                    preMiddle.next=p2.next;
                    p2.next=p1.next;
                    p1.next=p2;
                    p1=p2.next;
                    p2=preMiddle.next;
                }
            }
</p>


### Java solution with 3 steps
- Author: jeantimex
- Creation Date: Wed Jul 08 2015 08:21:48 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 01:04:29 GMT+0800 (Singapore Standard Time)

<p>
      public class Solution {
        
        public void reorderList(ListNode head) {
          if (head == null || head.next == null)
              return;
          
          // step 1. cut the list to two halves
          // prev will be the tail of 1st half
          // slow will be the head of 2nd half
          ListNode prev = null, slow = head, fast = head, l1 = head;
          
          while (fast != null && fast.next != null) {
            prev = slow;
            slow = slow.next;
            fast = fast.next.next;
          }
          
          prev.next = null;
          
          // step 2. reverse the 2nd half
          ListNode l2 = reverse(slow);
          
          // step 3. merge the two halves
          merge(l1, l2);
        }
        
        ListNode reverse(ListNode head) {
          ListNode prev = null, curr = head, next = null;
          
          while (curr != null) {
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
          }
          
          return prev;
        }
        
        void merge(ListNode l1, ListNode l2) {
          while (l1 != null) {
            ListNode n1 = l1.next, n2 = l2.next;
            l1.next = l2;
            
            if (n1 == null)
              break;
                
            l2.next = n1;
            l1 = n1;
            l2 = n2;
          }
        }
    
      }
</p>


### A concise O(n) time, O(1) in place solution
- Author: shichaotan
- Creation Date: Fri Jan 16 2015 14:23:37 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 09:55:02 GMT+0800 (Singapore Standard Time)

<p>
    // O(N) time, O(1) space in total
    void reorderList(ListNode *head) {
        if (!head || !head->next) return;
        
        // find the middle node: O(n)
        ListNode *p1 = head, *p2 = head->next;
        while (p2 && p2->next) {
            p1 = p1->next;
            p2 = p2->next->next;
        }
        
        // cut from the middle and reverse the second half: O(n)
        ListNode *head2 = p1->next;
        p1->next = NULL;
        
        p2 = head2->next;
        head2->next = NULL;
        while (p2) {
            p1 = p2->next;
            p2->next = head2;
            head2 = p2;
            p2 = p1;
        }
        
        // merge two lists: O(n)
        for (p1 = head, p2 = head2; p1; ) {
            auto t = p1->next;
            p1 = p1->next = p2;
            p2 = t;
        }
        
        //for (p1 = head, p2 = head2; p2; ) {
        //    auto t = p1->next;
        //    p1->next = p2;
        //    p2 = p2->next;
        //    p1 = p1->next->next = t;
        //}
    }
</p>


