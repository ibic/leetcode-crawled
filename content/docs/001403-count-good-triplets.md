---
title: "Count Good Triplets"
weight: 1403
#id: "count-good-triplets"
---
## Description
<div class="description">
<p>Given an array of integers <code>arr</code>, and three integers&nbsp;<code>a</code>,&nbsp;<code>b</code>&nbsp;and&nbsp;<code>c</code>. You need to find the number of good triplets.</p>

<p>A triplet <code>(arr[i], arr[j], arr[k])</code>&nbsp;is <strong>good</strong> if the following conditions are true:</p>

<ul>
	<li><code>0 &lt;= i &lt; j &lt; k &lt;&nbsp;arr.length</code></li>
	<li><code>|arr[i] - arr[j]| &lt;= a</code></li>
	<li><code>|arr[j] - arr[k]| &lt;= b</code></li>
	<li><code>|arr[i] - arr[k]| &lt;= c</code></li>
</ul>

<p>Where <code>|x|</code> denotes the absolute value of <code>x</code>.</p>

<p>Return<em> the number of good triplets</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [3,0,1,1,9,7], a = 7, b = 2, c = 3
<strong>Output:</strong> 4
<strong>Explanation:</strong>&nbsp;There are 4 good triplets: [(3,0,1), (3,0,1), (3,1,1), (0,1,1)].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,1,2,2,3], a = 0, b = 0, c = 1
<strong>Output:</strong> 0
<strong>Explanation: </strong>No triplet satisfies all conditions.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>3 &lt;= arr.length &lt;= 100</code></li>
	<li><code>0 &lt;= arr[i] &lt;= 1000</code></li>
	<li><code>0 &lt;= a, b, c &lt;= 1000</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Turvo - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Self Explanatory Code BF | Little Optimization BF
- Author: ngytlcdsalcp1
- Creation Date: Sun Aug 02 2020 12:13:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 02 2020 13:02:58 GMT+0800 (Singapore Standard Time)

<p>
Just do what question says and count good triplets 

```
class Solution {
    public int countGoodTriplets(int[] arr, int a, int b, int c) {
        int count = 0;
        int n = arr.length;
        for(int i = 0; i < n -2; i++) {
            for(int j = i + 1; j < n - 1; j++) {
                for(int k = j + 1; k < n; k++) {
                    if(Math.abs(arr[i] - arr[j]) <= a && Math.abs(arr[j] - arr[k]) <= b && Math.abs(arr[k] - arr[i]) <= c)
                        count++;
                }
            }
        }
        
        return count;
    }
}
```


Little optimization as per @gupta-pramod suggestion
```
class Solution {
    public int countGoodTriplets(int[] arr, int a, int b, int c) {
        int count = 0;
        int n = arr.length;
        for(int i = 0; i < n -2; i++) {
            for(int j = i + 1; j < n - 1; j++) {
                if(Math.abs(arr[i] - arr[j]) <= a) { // check if satisfy then loop for k
                    for(int k = j + 1; k < n; k++) {
                        if(Math.abs(arr[j] - arr[k]) <= b && Math.abs(arr[k] - arr[i]) <= c)
                            count++;
                    }
                }
            }
        }
        
        return count;
    }
}
```
</p>


### Easy Brute Force & Modular, Clean code
- Author: interviewrecipes
- Creation Date: Sun Aug 02 2020 12:47:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 08 2020 13:05:18 GMT+0800 (Singapore Standard Time)

<p>
Just the implementation of english statement into programming language is needed here.

**Thanks**
Please upvote if you like the code so that it remains near the top and the learners can see it :)
It also encourages me to write on Leetcode and on my blog/website.

```
class Solution {
public:
    bool isGood(int first, int second, int third, int a, int b, int c) {
        return abs(first-second) <= a && abs(second-third) <= b && abs(first-third) <= c;
    }
    
    int countGoodTriplets(vector<int>& arr, int a, int b, int c) {
        int n = arr.size();
        int ans = 0;
        for (int i=0; i<n; i++) {
            for (int j=i+1; j<n; j++) {
                for (int k=j+1; k<n; k++) {
                    if (isGood(arr[i], arr[j], arr[k], a, b, c)) {
                        ans++;
                    }
                }
            }
        }
        return ans;
    }
};
```
</p>


### Java. O(n). Well, it is really O(1000 * n)
- Author: SleepyFarmer
- Creation Date: Mon Aug 03 2020 06:20:58 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 03 2020 06:32:39 GMT+0800 (Singapore Standard Time)

<p>
Loop through the middle number, find out the lower and upper bounds for numbers before and after it. Then loop through the possible values before it, adjust the after bounds and add them up. (I am not good at words. This is the best I can do.)
 
I think O(nlogn) is possible if Fenwick Tree is used to store the suffix sum.
I am too lazy to do it. (Actually, I am not sure about Fenwick Tree)
```
class Solution {
    public int countGoodTriplets(int[] arr, int a, int b, int c) {
        int n = arr.length;
        int ans = 0;
        int[] pre = new int[1001];
        int[] post = new int[1001];  // stores suffix sum
        pre[arr[0]] = 1;
        for(int i = n-1; i > 1; i--) {
            post[arr[i]]++;
        }
        for(int i = 1; i <= 1000; i++) {
            post[i] = post[i-1] + post[i];
        }
        
        for(int j = 1; j < n-1; j++) {
            int v = arr[j];
            int p1 = Math.max(0, v-a);
            int p2 = Math.min(1000, v+a);
            int t1 = Math.max(0, v-b);
            int t2 = Math.min(1000, v+b);
            
            for(int s = p1; s <= p2; s++) {
                if (pre[s] == 0) continue;
                int m1 = Math.max(t1, s-c);
                int m2 = Math.min(t2, s+c);
                if (m2 >= m1) {
                    if (m1 == 0) {
                        ans += pre[s] * post[m2];
                    } else {
                        ans += pre[s] * (post[m2] - post[m1-1]);
                    }
                }
            }
            pre[v]++;
            for(int i = arr[j+1]; i <= 1000; i++) {
                post[i]--;
            }
        }
        return ans;
    }
}
```
</p>


