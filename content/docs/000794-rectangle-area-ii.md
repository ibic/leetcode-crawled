---
title: "Rectangle Area II"
weight: 794
#id: "rectangle-area-ii"
---
## Description
<div class="description">
<p>We are given a list of (axis-aligned)&nbsp;<code>rectangles</code>.&nbsp; Each&nbsp;<code>rectangle[i] = [x1, y1, x2, y2]&nbsp;</code>, where (x1, y1) are the coordinates of the bottom-left corner, and (x2, y2) are the coordinates of the top-right corner of the <code>i</code>th rectangle.</p>

<p>Find the total area covered by all <code>rectangles</code> in the plane.&nbsp; Since the answer&nbsp;may be too large, <strong>return it modulo 10^9 + 7</strong>.</p>

<p><img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/06/06/rectangle_area_ii_pic.png" style="width: 480px; height: 360px;" /></p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>[[0,0,2,2],[1,0,2,3],[1,0,3,1]]
<strong>Output: </strong>6
<strong>Explanation: </strong>As illustrated in the picture.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>[[0,0,1000000000,1000000000]]
<strong>Output: </strong>49
<strong>Explanation: </strong>The answer is 10^18 modulo (10^9 + 7), which is (10^9)^2 = (-7)^2 = 49.
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li><code>1 &lt;= rectangles.length &lt;= 200</code></li>
	<li><code><font face="monospace">rectanges[i].length = 4</font></code></li>
	<li><code>0 &lt;= rectangles[i][j] &lt;= 10^9</code></li>
	<li>The total area covered by all rectangles will never exceed&nbsp;<code>2^63 - 1</code>&nbsp;and thus will fit in a 64-bit signed integer.</li>
</ul>
</div>

## Tags
- Segment Tree (segment-tree)
- Line Sweep (line-sweep)

## Companies
- Google - 3 (taggedByAdmin: false)
- Sumologic - 2 (taggedByAdmin: false)

## Official Solution
[TOC]


## Solution
---
#### Approach #1: Principle of Inclusion-Exclusion

**Intuition**

Say we have two rectangles, $$A$$ and $$B$$.  The area of their union is:

$$
|A \cup B| = |A| + |B| - |A \cap B|
$$

Say we have three rectangles, $$A, B, C$$.  The area of their union is:

$$
|A \cup B \cup C| = |A| + |B| + |C| - |A \cap B| - |A \cap C| - |B \cap C| + |A \cap B \cap C|
$$

This can be seen by drawing a Venn diagram.

Say we have four rectangles, $$A, B, C, D$$.  The area of their union is:

$$
\begin{align}
|A \cup B \cup C \cup D| =\,&\left( |A| + |B| + |C| + |D| \right) - \\
\,&\left(|A \cap B| + |A \cap C| + |A \cap D| + |B \cap C| + |B \cap D| + |C \cap D|\right) +\\
\,&\left(|A \cap B \cap C| + |A \cap B \cap D| + |A \cap C \cap D| + |B \cap C \cap D|\right) -\\
\,&\left(|A \cap B \cap C \cap D|\right)
\end{align}
$$

In general, the area of the union of $$n$$ rectangles $$A_1, A_2, \cdots , A_n$$ will be:

$$
\bigg|\bigcup_{i=1}^n A_i\bigg| = \sum_{\emptyset \neq S \subseteq [n]} (-1)^{|S| + 1} \bigg| \bigcap_{i \in S} A_i \bigg|
$$

**Algorithm**

If we do not know the above fact, we can prove it by considering any point in $$\bigg|\bigcup_{i=1}^n A_i\bigg|$$.  Say this point occurs in all $$A_i (i \in S)$$, and let $$|S| = n$$.  Then on the right side, it is counted $$\binom{n}{1} - \binom{n}{2} + \binom{n}{3} - \cdots \pm \binom{n}{n}$$ times.  By considering the binomial expansion of $$(1 - 1)^n$$, this is in fact equal to $$1$$.

From *Rectangle Area I*, we know that the intersection of two axis-aligned rectangles is another axis-aligned rectangle (or nothing).  So in particular, the intersection $$\bigcap_{i \in S} A_i$$ is always a rectangle (or nothing).

Now our algorithm proceeds as follows:  for every subset $$S$$ of $$\{1, 2, 3, \cdots, N\}$$ (where $$N$$ is the number of rectangles), we'll calculate the intersection of the rectangles in that subset $$\bigcap_{i \in S} A_i$$, and then the area of that rectangle.  This allows us to calculate algorithmically the right-hand side of the general equation we wrote earlier.

<iframe src="https://leetcode.com/playground/AfmnPCZM/shared" frameBorder="0" width="100%" height="500" name="AfmnPCZM"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N * 2^N)$$, where $$N$$ is the number of rectangles.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach #2: Coordinate Compression

**Intuition**

<center>
    <img src="../Figures/850/example.png" alt="Image from problem description" style="height: 200px;"/>
</center>

Suppose instead of `rectangles = [[0,0,2,2],[1,0,2,3],[1,0,3,1]]`, we had `[[0,0,200,200],[100,0,200,300],[100,0,300,100]]`.  The answer would just be 100 times bigger.

What about if `rectangles = [[0,0,2,2],[1,0,2,3],[1,0,30002,1]]` ?  Only the blue region would have area `30000` instead of `1`.

Our idea is this: we'll take all the `x` and `y` coordinates, and re-map them to `0, 1, 2, ...` etc.  For example, if `rectangles  = [[0,0,200,200],[100,0,200,300],[100,0,300,100]]`, we could re-map it to `[[0,0,2,2],[1,0,2,3],[1,0,3,1]]`.  Then, we can solve the problem with brute force.  However, each region may actually represent some larger area, so we'll need to adjust for that at the end.

**Algorithm**

Re-map each `x` coordinate to `0, 1, 2, ...`.  Independently, re-map all `y` coordinates too.

We then have a problem that can be solved by brute force: for each rectangle with re-mapped coordinates `(rx1, ry1, rx2, ry2)`, we can fill the grid `grid[x][y] = True` for `rx1 <= x < rx2` and `ry1 <= y < ry2`.

Afterwards, each `grid[rx][ry]` represents the area `(imapx(rx+1) - imapx(rx)) * (imapy(ry+1) - imapy(ry))`, where if `x` got remapped to `rx`, then `imapx(rx) = x` ("inverse-map-x of remapped-x equals x"), and similarly for `imapy`.

<iframe src="https://leetcode.com/playground/X3jv6QUg/shared" frameBorder="0" width="100%" height="500" name="X3jv6QUg"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^3)$$, where $$N$$ is the number of rectangles.

* Space Complexity:  $$O(N^2)$$.
<br />
<br />


---
#### Approach #3: Line Sweep

**Intuition**

Imagine we pass a horizontal line from bottom to top over the shape.  We have some active intervals on this horizontal line, which gets updated twice for each rectangle.  In total, there are $$2 * N$$ events, and we can update our (up to $$N$$) active horizontal intervals for each update.

**Algorithm**

For a rectangle like `rec = [1,0,3,1]`, the first update is to add `[1, 3]` to the active set at `y = 0`, and the second update is to remove `[1, 3]` at `y = 1`.  Note that adding and removing respects multiplicity - if we also added `[0, 2]` at `y = 0`, then removing `[1, 3]` at `y = 1` will still leave us with `[0, 2]` active.

This gives us a plan: create these two events for each rectangle, then process all the events in sorted order of `y`.  The issue now is deciding how to process the events `add(x1, x2)` and `remove(x1, x2)` such that we are able to `query()` the total horizontal length of our active intervals.

We can use the fact that our `remove(...)` operation will always be on an interval that was previously added.  Let's store all the `(x1, x2)` intervals in sorted order.  Then, we can `query()` in linear time using a technique similar to a classic LeetCode problem, [Merge Intervals](https://leetcode.com/problems/merge-intervals/).

<iframe src="https://leetcode.com/playground/Sm5cHKwn/shared" frameBorder="0" width="100%" height="500" name="Sm5cHKwn"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2 \log N)$$, where $$N$$ is the number of rectangles.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach #4: Segment Tree

**Intuition and Algorithm**

As in *Approach #3*, we want to support `add(x1, x2)`, `remove(x1, x2)`, and `query()`.  While outside the scope of a typical interview, this is the perfect setting for using a *segment tree*.  For completeness, we include the following implementation.

You can learn more about Segment Trees by visiting the articles of these problems: [Falling Squares](https://leetcode.com/problems/falling-squares/), [Number of Longest Increasing Subsequence](https://leetcode.com/problems/number-of-longest-increasing-subsequence/).


<iframe src="https://leetcode.com/playground/seWDQFSH/shared" frameBorder="0" width="100%" height="500" name="seWDQFSH"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the number of rectangles.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Python] Discretization and O(NlogN)
- Author: lee215
- Creation Date: Sun Jun 10 2018 11:05:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 15 2020 15:16:13 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
Scan from `y = 0`, count the coverage of rectangles on `x`.
For example, at `y = 0`, the intervale [0, 3] is covered by rectangles.
The current sum of coverage is 3.

Then we move our scan line upper to next `y`.
At `y = 1`, we add (1 - 0) * 3  = 3 to area. Now area = 3.
And we update the sum of coverage to 2 because it\'s coverd on [0, 2].

Then we move our scan line upper to next `y`.
At `y = 2`, we add (2 - 1) * 2  = 2 to area. Now area = 5.
And we update the sum of coverage to 1 because it\'s coverd on [1, 2].

Then we move our scan line upper to next `y`.
At `y = 3`, we add (3 - 2) * 1  = 1 to area. Now area = 6.
And we update the sum of coverage to 0.

The final result is 6.

<img src="https://s3-lc-upload.s3.amazonaws.com/users/lee215/image_1528600475.png" width=450 height=300 />

# **Time Complexity**:
We checked every `y` value, and for every `y`, we count the coverage of `x`.
So this a O(N^2) solution.
In fact I expected the `N` to be big like 10^4. So only solution at least O(N^2) can get accepted.
<br>

**C++:**
```cpp
    int rectangleArea(vector<vector<int>>& rectangles) {
        int mod = 1000000000 + 7;
        vector<int> x;
        for (auto r : rectangles) {
            x.push_back(r[0]);
            x.push_back(r[2]);
        }
        sort(x.begin(), x.end());
        vector<int>::iterator end_unique = unique(x.begin(), x.end());
        x.erase(end_unique, x.end());
        unordered_map<int, int> x_i;
        for (int i = 0; i < x.size(); ++i) {
            x_i[x[i]] = i;
        }
        vector<int> count(x.size(), 0);
        vector<vector<int>> L;
        for (auto r : rectangles) {
            int x1 = r[0], y1 = r[1], x2 = r[2], y2 = r[3];
            L.push_back({y1, x1, x2, 1});
            L.push_back({y2, x1, x2, -1});
        }
        sort(L.begin(), L.end());
        long long cur_y = 0, cur_x_sum = 0, area = 0;
        for (auto l : L) {
            long long  y = l[0], x1 = l[1], x2 = l[2], sig = l[3];
            area = (area + (y - cur_y) * cur_x_sum) % mod;
            cur_y = y;
            for (int i = x_i[x1]; i < x_i[x2]; i++)
                count[i] += sig;
            cur_x_sum = 0;
            for (int i = 0; i < x.size(); ++i) {
                if (count[i] > 0)
                    cur_x_sum += x[i + 1] - x[i];
            }
        }
        return area;
    }
```

**Python:**
```py
    def rectangleArea(self, rectangles):
        xs = sorted(set([x for x1, y1, x2, y2 in rectangles for x in [x1, x2]]))
        x_i = {v: i for i, v in enumerate(xs)}
        count = [0] * len(x_i)
        L = []
        for x1, y1, x2, y2 in rectangles:
            L.append([y1, x1, x2, 1])
            L.append([y2, x1, x2, -1])
        L.sort()
        cur_y = cur_x_sum = area = 0
        for y, x1, x2, sig in L:
            area += (y - cur_y) * cur_x_sum
            cur_y = y
            for i in range(x_i[x1], x_i[x2]):
                count[i] += sig
            cur_x_sum = sum(x2 - x1 if c else 0 for x1, x2, c in zip(xs, xs[1:], count))
        return area % (10 ** 9 + 7)
```
<br>

**Follow Up Time**
Can you do it better in `O(NlogN)`?

When I update the coverage on x, I updated one by one.
But in fact I update for a range.
If we use a segment tree to update the coverage, it will be only O(logN) for add and delete action.

**Python:**
```py
    def rectangleArea(self, rectangles):
        xs = sorted(set([x for x1, y1, x2, y2 in rectangles for x in [x1, x2]]))
        st = NumArray(xs)
        L = []
        for x1, y1, x2, y2 in rectangles:
            L.append([y1, 1, x1, x2])
            L.append([y2, -1, x1, x2])
        L.sort()
        cur_y = cur_x_sum = area = 0
        for y, sig, x1, x2 in L:
            area += (y - cur_y) * cur_x_sum
            cur_y = y
            cur_x_sum = st.update(x1, x2, sig)
        return area % (10 ** 9 + 7)

```
</p>


### Clean Recursive Solution [Java]
- Author: ssg7
- Creation Date: Sun Jun 10 2018 15:22:03 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 11 2018 22:18:15 GMT+0800 (Singapore Standard Time)

<p>
The idea here is to maintain a list of non-overlapping rectangles to calculate final area.
If a new rectangle does not overlap with any of the existing rectanlges, add it to the list.
If there is an overlap, split the non-overlapping regions of the rectangle into smaller rectangles and compare with the rest of the list. 

For example, when a new rectangle (green) is compared with the current rectangle (blue), the non-overlapping regions can be split into two smaller rectangles. Rectangle 1 will be covered by the first overlapping case in addRectangle() and rectangle 2 will be covered by the third case. Rectangle 3 overlaps with the current rectangle and need not be considered.


![image](https://s3-lc-upload.s3.amazonaws.com/users/ssg7/image_1528614965.png)

 
 

```
    public int rectangleArea(int[][] rectangles) {
        
        int mod = (int)Math.pow(10,9)+7;
        long res = 0;
        List<int[]> recList = new ArrayList<>();
        for(int[] rec : rectangles)
            addRectangle(recList, rec, 0);
        
        for(int[] rec: recList)
            res = (res+((long)(rec[2]-rec[0])*(long)(rec[3]-rec[1])))%mod;

        return (int) res%mod;
    }
    
    // Add new rectangle to the list. In case of overlap break up new rectangle into 
    // non-overlapping rectangles. Compare the new rectanlges with the rest of the list.
    public void addRectangle(List<int[]> recList, int[] curRec, int start){
        if(start>=recList.size()){
            recList.add(curRec);
            return;
        }
        
        int[] r = recList.get(start);
        
        // No overlap
        if(curRec[2]<=r[0] || curRec[3]<=r[1] || curRec[0]>=r[2] || curRec[1]>=r[3]){
            addRectangle(recList, curRec, start+1);
            return;
        }

        if( curRec[0]<r[0])
            addRectangle(recList, new int[]{curRec[0],curRec[1],r[0],curRec[3]},start+1);

        if(curRec[2]>r[2])
            addRectangle(recList, new int[]{r[2],curRec[1],curRec[2],curRec[3]},start+1);
        
        if(curRec[1]<r[1])
            addRectangle(recList, new int[]{Math.max(r[0],curRec[0]),curRec[1],Math.min(r[2],curRec[2]),r[1]},start+1);
        
        if(curRec[3]>r[3])
            addRectangle(recList, new int[]{Math.max(r[0],curRec[0]),r[3],Math.min(r[2],curRec[2]),curRec[3]},start+1);
    }
</p>


### Java TreeMap solution inspired by Skyline and Meeting Room
- Author: wangzi6147
- Creation Date: Sun Jun 10 2018 11:21:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 00:11:17 GMT+0800 (Singapore Standard Time)

<p>
1. Sort the points in `x` order
2. For the points in the same `x`, calculate the current `y` (like the meeting room problem).
3. In the next `x`, calculate the area by `preY * (curX - preX)`

The complexity in the worst case is `O(N ^ 2)` (all the rectangles have the same `x`)

```
class Solution {
    class Point {
        int x, y, val;
        Point(int x, int y, int val) {
            this.x = x;
            this.y = y;
            this.val = val;
        }
    }
    public int rectangleArea(int[][] rectangles) {
        int M = 1000000007;
        List<Point> data = new ArrayList<>();
        for (int[] r : rectangles) {
            data.add(new Point(r[0], r[1], 1));
            data.add(new Point(r[0], r[3], -1));
            data.add(new Point(r[2], r[1], -1));
            data.add(new Point(r[2], r[3], 1));
        }
        Collections.sort(data, (a, b) -> {
            if (a.x == b.x) {
                return b.y - a.y;
            }
            return a.x - b.x;
        });
        TreeMap<Integer, Integer> map = new TreeMap<>();
        int preX = -1;
        int preY = -1;
        int result = 0;
        for (int i = 0; i < data.size(); i++) {
            Point p = data.get(i);
            map.put(p.y, map.getOrDefault(p.y, 0) + p.val);
            if (i == data.size() - 1 || data.get(i + 1).x > p.x) {
                if (preX > -1) {
                    result += ((long)preY * (p.x - preX)) % M;
                    result %= M;
                }
                preY = calcY(map);
                preX = p.x;
            }
        }
        return result;
    }
    private int calcY(TreeMap<Integer, Integer> map) {
        int result = 0, pre = -1, count = 0;
        for (Map.Entry<Integer, Integer> e : map.entrySet()) {
            if (pre >= 0 && count > 0) {
                result += e.getKey() - pre;
            }
            count += e.getValue();
            pre = e.getKey();
        }
        return result;
    }
}
```
</p>


