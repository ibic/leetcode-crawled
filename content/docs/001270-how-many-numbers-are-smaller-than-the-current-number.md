---
title: "How Many Numbers Are Smaller Than the Current Number"
weight: 1270
#id: "how-many-numbers-are-smaller-than-the-current-number"
---
## Description
<div class="description">
<p>Given the array <code>nums</code>, for each <code>nums[i]</code> find out how many numbers in the array are smaller than it. That is, for each <code>nums[i]</code> you have to count the number of valid <code>j&#39;s</code>&nbsp;such that&nbsp;<code>j != i</code> <strong>and</strong> <code>nums[j] &lt; nums[i]</code>.</p>

<p>Return the answer in an array.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [8,1,2,2,3]
<strong>Output:</strong> [4,0,1,1,3]
<strong>Explanation:</strong> 
For nums[0]=8 there exist four smaller numbers than it (1, 2, 2 and 3). 
For nums[1]=1 does not exist any smaller number than it.
For nums[2]=2 there exist one smaller number than it (1). 
For nums[3]=2 there exist one smaller number than it (1). 
For nums[4]=3 there exist three smaller numbers than it (1, 2 and 2).
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [6,5,4,8]
<strong>Output:</strong> [2,1,0,3]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [7,7,7,7]
<strong>Output:</strong> [0,0,0,0]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= nums.length &lt;= 500</code></li>
	<li><code>0 &lt;= nums[i] &lt;= 100</code></li>
</ul>

</div>

## Tags
- Array (array)
- Hash Table (hash-table)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### JAVA beats 100% O(n)
- Author: equ1n0x
- Creation Date: Sun Mar 01 2020 12:37:26 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 02 2020 03:07:52 GMT+0800 (Singapore Standard Time)

<p>
Store the count in a bucket and take the running sum.

```
class Solution {
    public int[] smallerNumbersThanCurrent(int[] nums) {
        int[] count = new int[101];
        int[] res = new int[nums.length];
        
        for (int i =0; i < nums.length; i++) {
            count[nums[i]]++;
        }
        
        for (int i = 1 ; i <= 100; i++) {
            count[i] += count[i-1];    
        }
        
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0)
                res[i] = 0;
            else 
                res[i] = count[nums[i] - 1];
        }
        
        return res;        
    }
}
```
</p>


### [Java] ✅ Clean HashMap solution with explanation
- Author: rxdbeard
- Creation Date: Wed Mar 11 2020 02:05:00 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Mar 11 2020 02:07:06 GMT+0800 (Singapore Standard Time)

<p>
So the idea is:
Let\'s use this input for illustration: [8,1,2,2,3]
1. Create a copy of the input array. copy = [8,1,2,2,3]
2. Sort the copy array. copy = [1,2,2,3,8]
3. Fill the map: number => count (where count is an index in sorted array, so first number with index 0 has 0 numbers less than it, index 1 has 1 number less, etc). We update only first time we enocunter the number so that way we skip duplicates.
map[1]=>0
map[2]=>1
map[3]=>3
map[8]=>4
4. We re-use our copy array to get our result, we iterate over original array, and get counts from the map.
[4,0,1,1,3]

```
class Solution {
    public int[] smallerNumbersThanCurrent(int[] nums) {
        
        Map<Integer, Integer> map = new HashMap<>();
        int[] copy = nums.clone();
        
        Arrays.sort(copy);
        
        for (int i = 0; i < nums.length; i++) {
            map.putIfAbsent(copy[i], i);
        }
        
        for (int i = 0; i < nums.length; i++) {
            copy[i] = map.get(nums[i]);
        }
        
        return copy;
    }
}
```
</p>


### Clean Python 3, sorting and counting
- Author: lenchen1112
- Creation Date: Sun Mar 01 2020 12:03:47 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 14 2020 11:20:33 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1**
Record index in sorted nums if it didn\'t appear before.
Then just dump it\'s corresponding index in original nums.

Time: O(NlogN)
Space: O(N) for output list
```
class Solution:
    def smallerNumbersThanCurrent(self, nums: List[int]) -> List[int]:
        indices = {}
        for idx, num in enumerate(sorted(nums)):
            indices.setdefault(num, idx)
        return [indices[num] for num in nums]
```


**Soluiton 2**
Refer to [this solution](https://leetcode.com/problems/how-many-numbers-are-smaller-than-the-current-number/discuss/524996/JAVA-beats-100-O(n)) of @equ1n0x, we are already told the range of the given numbers is between 1 and 100.
So we can easily count each number and sum their prefix and dump.

Time: O(N)
Space: O(N) for output list
```
class Solution:
    def smallerNumbersThanCurrent(self, nums: List[int]) -> List[int]:
        count = [0] * 102
        for num in nums:
            count[num+1] += 1
        for i in range(1, 102):
            count[i] += count[i-1]
        return [count[num] for num in nums]
```
</p>


