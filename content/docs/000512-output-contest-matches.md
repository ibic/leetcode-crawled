---
title: "Output Contest Matches"
weight: 512
#id: "output-contest-matches"
---
## Description
<div class="description">
<p>
During the NBA playoffs, we always arrange the rather strong team to play with the rather weak team, like make the rank 1 team play with the rank n<sub>th</sub> team, which is a good strategy to make the contest more interesting. Now, you're given <b>n</b> teams, you need to output their <b>final</b> contest matches in the form of a string.
</p>

<p>The <b>n</b> teams are given in the form of positive integers from 1 to n, which represents their initial rank. (Rank 1 is the strongest team and Rank n is the weakest team.) We'll use parentheses('(', ')') and commas(',') to represent the contest team pairing - parentheses('(' , ')') for pairing and commas(',') for partition. During the pairing process in each round, you always need to follow the strategy of making the rather strong one pair with the rather weak one.</p> 

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> 2
<b>Output:</b> (1,2)
<b>Explanation:</b> 
Initially, we have the team 1 and the team 2, placed like: 1,2.
Then we pair the team (1,2) together with '(', ')' and ',', which is the final answer.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> 4
<b>Output:</b> ((1,4),(2,3))
<b>Explanation:</b> 
In the first round, we pair the team 1 and 4, the team 2 and 3 together, as we need to make the strong team and weak team together.
And we got (1,4),(2,3).
In the second round, the winners of (1,4) and (2,3) need to play again to generate the final winner, so you need to add the paratheses outside them.
And we got the final answer ((1,4),(2,3)).
</pre>
</p>

<p><b>Example 3:</b><br />
<pre>
<b>Input:</b> 8
<b>Output:</b> (((1,8),(4,5)),((2,7),(3,6)))
<b>Explanation:</b> 
First round: (1,8),(2,7),(3,6),(4,5)
Second round: ((1,8),(4,5)),((2,7),(3,6))
Third round: (((1,8),(4,5)),((2,7),(3,6)))
Since the third round will generate the final winner, you need to output the answer (((1,8),(4,5)),((2,7),(3,6))).
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The <b>n</b> is in range [2, 2<sup>12</sup>].</li>
<li>We ensure that the input <b>n</b> can be converted into the form 2<sup>k</sup>, where k is a positive integer.</li>
</ol>
</p>
</div>

## Tags
- String (string)
- Recursion (recursion)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Simulation [Accepted]

**Intuition**

Let `team[i]` be the correct team string of the `i`-th strongest team for that round.  We will maintain these correctly as the rounds progress.

**Algorithm**

In each round, the `i`-th team becomes `"(" + team[i] + "," + team[n-1-i] + ")"`, and then there are half as many teams.

<iframe src="https://leetcode.com/playground/wqtgEbVm/shared" frameBorder="0" width="100%" height="276" name="wqtgEbVm"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N \log N)$$.  Each of $$O(\log N)$$ rounds performs $$O(N)$$ work.

* Space Complexity: $$O(N \log N)$$.

---
#### Approach #2: Linear Write [Accepted]

**Intuition**

Let's try to solve the problem in linear time.  We can treat this problem as two separate problems: outputting the correct sequence of parentheses and commas, and outputting the correct team number.  With a little effort, one can be convinced that a linear time solution probably exists.

**Algorithm**

Let's focus on the parentheses first.  We can use recursion to find the answer.  For example, when `N = 8`, let `R = log_2(N) = 3` be the number of rounds.  The parentheses and commas look like this:

```python
(((x,x),(x,x)),((x,x),(x,x)))
```

But this is just recursively
```python
"(" + (sequence for R = 2) + "," + (sequence for R = 2) + ")"
= "(" + "((x,x),(x,x))" + "," + "((x,x),(x,x))" + ")"
```

Now let's look at the team numbers.  For `N = 16`, the team numbers are:

`team = [1, 16, 8, 9, 4, 13, 5, 12, 2, 15, 7, 10, 3, 14, 6, 11]`

One thing we might notice is that adjacent numbers sum to `17`.  More specifically, indices that are 0 and 1 (mod 2) sum to `17`.  Also, indices 0 and 2 (mod 4) sum to `9`, indices 0 and 4 (mod 8) sum to `5`, and so on.

The pattern in general is: indices `0` and `2**r` (mod `2**(r+1)`) sum to `N * 2**-r + 1`.

If we want to find the next `team[i]`, then the lowest bit of `i` will help determine it's lower neighbor.  For example, `team[12] = team[0b1100]` has lower bit `w = 4 = 0b100`, so `12` has lower neighbor `12 - w = 8`, and also those team numbers sum to `N / w + 1`.

<iframe src="https://leetcode.com/playground/Ac8ifBs8/shared" frameBorder="0" width="100%" height="497" name="Ac8ifBs8"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$.  We print each of $$O(N)$$ characters in order.

* Space Complexity: $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] [Java] Clean Code
- Author: alexander
- Creation Date: Sun Mar 19 2017 11:02:48 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 15:56:12 GMT+0800 (Singapore Standard Time)

<p>
**C++**
```
class Solution {
public:
    string findContestMatch(int n) {
        vector<string> m(n);
        for (int i = 0; i < n; i++) {
            m[i] = to_string(i + 1);
        }

        while (n > 1) {
            for (int i = 0; i < n / 2; i++) {
                m[i] = "(" + m[i] + "," + m[n - 1 - i] + ")";
            }
            n /= 2;
        }
        
        return m[0];
    }
};
```

**Java**
```
public class Solution {
    public String findContestMatch(int n) {
        String[] m = new String[n];
        for (int i = 0; i < n; i++) {
            m[i] = String.valueOf(i + 1);
        }

        while (n > 1) {
            for (int i = 0; i < n / 2; i++) {
                m[i] = "(" + m[i] + "," + m[n - 1 - i] + ")";
            }
            n /= 2;
        }
        
        return m[0];
    }
}
```
</p>


### Java 10 lines
- Author: fallcreek
- Creation Date: Sun Mar 19 2017 11:01:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 19 2017 11:01:28 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public String findContestMatch(int n) {
        List<String> matches = new ArrayList<>();
        for(int i = 1; i <= n; i++) matches.add(String.valueOf(i));
        
        while(matches.size() != 1){
            List<String> newRound = new ArrayList<>();
            for(int i = 0; i < matches.size()/2; i++)   
                newRound.add("(" + matches.get(i) + "," + matches.get(matches.size() - i - 1) + ")");
            matches = newRound;
        }
        return matches.get(0);
    }
}
```
</p>


### Confusing description
- Author: suhanovsergey
- Creation Date: Tue Jan 01 2019 03:45:35 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 01 2019 03:45:35 GMT+0800 (Singapore Standard Time)

<p>
How solution for 8 (((1,8),(4,5)),((2,7),(3,6))) follows from the description? Why not (((1,8),(2,7)),((3,6),(4,5)))?
</p>


