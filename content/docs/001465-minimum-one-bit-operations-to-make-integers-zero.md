---
title: "Minimum One Bit Operations to Make Integers Zero"
weight: 1465
#id: "minimum-one-bit-operations-to-make-integers-zero"
---
## Description
<div class="description">
<p>Given an integer <code>n</code>, you must transform it into <code>0</code> using the following operations any number of times:</p>

<ul>
	<li>Change the rightmost (<code>0<sup>th</sup></code>) bit in the binary representation of <code>n</code>.</li>
	<li>Change the <code>i<sup>th</sup></code> bit in the binary representation of <code>n</code> if the <code>(i-1)<sup>th</sup></code> bit is set to <code>1</code> and the <code>(i-2)<sup>th</sup></code> through <code>0<sup>th</sup></code> bits are set to <code>0</code>.</li>
</ul>

<p>Return <em>the minimum number of operations to transform </em><code>n</code><em> into </em><code>0</code><em>.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 0
<strong>Output:</strong> 0
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 3
<strong>Output:</strong> 2
<strong>Explanation:</strong> The binary representation of 3 is &quot;11&quot;.
&quot;<u>1</u>1&quot; -&gt; &quot;<u>0</u>1&quot; with the 2nd operation since the 0th bit is 1.
&quot;0<u>1</u>&quot; -&gt; &quot;0<u>0</u>&quot; with the 1st operation.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 6
<strong>Output:</strong> 4
<strong>Explanation:</strong> The binary representation of 6 is &quot;110&quot;.
&quot;<u>1</u>10&quot; -&gt; &quot;<u>0</u>10&quot; with the 2nd operation since the 1st bit is 1 and 0th through 0th bits are 0.
&quot;01<u>0</u>&quot; -&gt; &quot;01<u>1</u>&quot; with the 1st operation.
&quot;0<u>1</u>1&quot; -&gt; &quot;0<u>0</u>1&quot; with the 2nd operation since the 0th bit is 1.
&quot;00<u>1</u>&quot; -&gt; &quot;00<u>0</u>&quot; with the 1st operation.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 9
<strong>Output:</strong> 14
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 333
<strong>Output:</strong> 393
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= n &lt;= 10<sup>9</sup></code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Bit Manipulation (bit-manipulation)

## Companies
- Expedia - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] 3 Solutions with Prove,  O(1) Space
- Author: lee215
- Creation Date: Sun Oct 04 2020 12:09:09 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 08 2020 15:43:59 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
For 1XXXXXXX,
we need to transfer it
1XXXXXXX -> ... -> 11000000 -> 1000000 -> ... -> 0
<br>

# Observation 1
The two operations are undo-able.
If a -> b needs `k` operation,
b -> a also needs `k` operation.
<br>

# Observation 2
1 -> 0 needs 1 operation,
2 -> 0 needs 3 operations,
4 -> 0 needs 7 operations,
`2^k` needs `2^(k+1)-1` operations.

This can be easily proved.
<br>



# Solution 1: Recursion
1XXXXXX -> 1100000 -> 100000 -> 0

1XXXXXX -> 1100000 needs minimumOneBitOperations(1XXXXXX ^ 1100000),
because it needs same operations 1XXXXXX ^ 1100000 -> 1100000 ^ 1100000 = 0.

1100000 -> 100000 needs 1 operation.
 100000 -> 0, where 100000 is 2^k, needs 2^(k+1) - 1 operations.

In total,
`f(n) = f((b >> 1) ^ b ^ n) + 1 + b - 1`,
where b is the maximum power of 2 that small or equals to n.

Time `O(logn)`
Space `O(logn)`

**Python:**
```py
    dp = {0: 0}
    def minimumOneBitOperations(self, n):
        if n not in self.dp:
            b = 1
            while (b << 1) <= n:
                b = b << 1
            self.dp[n] = self.minimumOneBitOperations((b >> 1) ^ b ^ n) + 1 + b - 1
        return self.dp[n]
```
<br>

# Solution 2: Tail Recursion
Time `O(logn)`
Space `O(1)`
**Java:**
```java
    public int minimumOneBitOperations(int n) {
        return minimumOneBitOperations(n, 0);
    }

    public int minimumOneBitOperations(int n, int res) {
        if (n == 0) return res;
        int b = 1;
        while ((b << 1) <= n)
            b = b << 1;
        return minimumOneBitOperations((b >> 1) ^ b ^ n, res + b);
    }
```

**C++:**
```cpp
    int minimumOneBitOperations(int n, int res = 0) {
        if (n == 0) return res;
        int b = 1;
        while ((b << 1) <= n)
            b = b << 1;
        return minimumOneBitOperations((b >> 1) ^ b ^ n, res + b);
    }
```
<br><br>

# Solution 3: Iterative Solution
Inspired by @endlesscheng, can be proved based on solution 2.

We iterate the binary format of `n`,
whenever we meet bit `1` at `i`th position,
we increment the result by (1 << (i + 1)) - 1.

Time `O(logn)`
Space `O(1)`

**Java**
```java
    public int minimumOneBitOperations(int n) {
        int sign = 1, res = 0;
        while (n > 0) {
            res += n ^ (n - 1) * sign;
            n &= n - 1;
            sign = -sign;
        }
        return Math.abs(res);
    }
```
**C++**
```cpp
    int minimumOneBitOperations(int n) {
        int res;
        for (res = 0; n > 0; n &= n - 1)
            res = -(res + (n ^ (n - 1)));
        return abs(res);
    }
```
**Python**
```py
    def minimumOneBitOperations(self, n):
        res = 0
        while n:
            res = -res - (n ^ (n - 1))
            n &= n - 1
        return abs(res)
```
</p>


### C++ solution with explanation
- Author: milu
- Creation Date: Sun Oct 04 2020 12:03:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 05 2020 00:57:10 GMT+0800 (Singapore Standard Time)

<p>
Note that the number of operations for `n` to become 0 is the same as the number of operations for 0 to become `n`...

Let\'s see how it can be done for numbers that are powers of 2.
`1 -> 0` => 1
`10 -> 11 -> 01 -> ...` => 2 + 1
`100 -> 101 -> 111 -> 110 -> 010 -> ...` => 4 + 2 + 1
`1000 -> 1001 -> 1011 -> 1010 -> 1110 -> 1111 -> 1101 -> 1100 -> 0100 -> ...` => 8 + 4 + 2 + 1
We can find that for `2^n`, it needs `2^(n+1) - 1` operations to become 0.

Now suppose we want to know the number of operations for `1110` to become `0`. We know it takes 15 operations for 0 to become `1000`, and it takes 4 operations for `1000` to become `1110`. We get the solution by `15 - 4`.  
Note that `4` here is the number of operations from `1000` to become `1110`, which is the same as the number of operations from `000` to `110` (ignoring the most significant bit), and it can be computed recursively. The observation gives us: `minimumOneBitOperations(1110) + minimumOneBitOperations(0110) = minimumOneBitOperations(1000)`.

From the above intuition, we can reduce `n` bit by bit, starting from the most significant bit.

```
int minimumOneBitOperations(int n) {
    if (n <= 1)
        return n;
    int bit = 0;
    while ((1 << bit) <= n)
        bit++;
    return ((1 << bit) - 1) - minimumOneBitOperations(n - (1 << (bit-1)));
}
```
</p>


### GreyCode to decimal  C++ Math solution
- Author: bharatwaj
- Creation Date: Sun Oct 04 2020 12:11:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 12:11:28 GMT+0800 (Singapore Standard Time)

<p>
GreyCode always has the least number of bit changes, so treating the given number as grey code and converting it to decimal gives the correct answer.

https://en.wikipedia.org/wiki/Gray_code

Decimal	Binary	Gray
0	0000	0000
1	0001	0001
**2	0010	0011**
3	0011	0010
4	0100	0110
5	0101	0111
6	0110	0101
7	0111	0100
8	1000	1100
9	1001	1101
10	1010	1111
11	1011	1110
12	1100	1010
13	1101	1011
**14	1110	1001**
15	1111	1000

The bold test are the example cases 
```
class Solution {
public:
    int minimumOneBitOperations(int n) {
        int output = 0;
        
        while( n> 0)
        {
            output ^= n;
            n = n >> 1;
        }
        
        return output;
    }
};
```
</p>


