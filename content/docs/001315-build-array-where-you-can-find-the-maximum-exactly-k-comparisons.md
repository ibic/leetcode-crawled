---
title: "Build Array Where You Can Find The Maximum Exactly K Comparisons"
weight: 1315
#id: "build-array-where-you-can-find-the-maximum-exactly-k-comparisons"
---
## Description
<div class="description">
<p>Given three integers <code>n</code>, <code>m</code> and <code>k</code>. Consider the following algorithm to find the maximum element of an array of positive integers:</p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/04/02/e.png" style="width: 424px; height: 372px;" />
<p>You should build the array arr which has the following properties:</p>

<ul>
	<li><code>arr</code> has exactly <code>n</code> integers.</li>
	<li><code>1 &lt;= arr[i] &lt;= m</code> where <code>(0 &lt;= i &lt; n)</code>.</li>
	<li>After applying the mentioned algorithm to <code>arr</code>, the value <code>search_cost</code> is equal to <code>k</code>.</li>
</ul>

<p>Return <em>the number of ways</em> to build the array <code>arr</code> under the mentioned conditions.&nbsp;As the answer may grow large, the answer&nbsp;<strong>must be</strong>&nbsp;computed modulo&nbsp;<code>10^9 + 7</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 2, m = 3, k = 1
<strong>Output:</strong> 6
<strong>Explanation:</strong> The possible arrays are [1, 1], [2, 1], [2, 2], [3, 1], [3, 2] [3, 3]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 5, m = 2, k = 3
<strong>Output:</strong> 0
<strong>Explanation:</strong> There are no possible arrays that satisify the mentioned conditions.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 9, m = 1, k = 1
<strong>Output:</strong> 1
<strong>Explanation:</strong> The only possible array is [1, 1, 1, 1, 1, 1, 1, 1, 1]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 50, m = 100, k = 25
<strong>Output:</strong> 34549172
<strong>Explanation:</strong> Don&#39;t forget to compute the answer modulo 1000000007
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 37, m = 17, k = 7
<strong>Output:</strong> 418930126
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 50</code></li>
	<li><code>1 &lt;= m &lt;= 100</code></li>
	<li><code>0 &lt;= k &lt;= n</code></li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- dunzo - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Bottom-Up Dynamic Programming with Explanation
- Author: kekesh
- Creation Date: Sun Apr 19 2020 12:09:37 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 30 2020 20:41:44 GMT+0800 (Singapore Standard Time)

<p>
Let `ways[i][j][k]` be the number of ways to construct an array of length `i` with maximum element equal to `j` at a search cost of `k`. 

There are two subproblems that contribute to `ways[i][j][k]`:

1) Clearly, we require `ways[i][j][k] += j * ways[i - 1][j][k]`. Why? Because if we have an array of length `i - 1` with maximum element equal to `j` at a search cost of `k`, then we can just append any element from `[1, 2, ... j]` to this array, and we end up with an array of length i with maximum element equal to `j` at a search cost of `k`. Note that neither the search cost nor the maximum element change since we\'re only appending elements from `[1,2, ..., j]`. Only the length changes by one.

2) We also require `ways[i][j][k] += SUM from x=1 to j-1 of ways[i - 1][x][k - 1]` (the sum is inclusive on both ends). This is true because if we have an array of length `i - 1` with maximum element (strictly) less than `j` at a search cost of `k - 1`, then we can simply append the element `j` to the end of this array (which adds a comparison cost of one), and we obtain a valid solution.

Now the answer is just the sum over all `ways[i][x][k]` from `x = 1` to `k` because there is no constraint on what the maximum element should be.

Here is an implementation of this solution in C++.

```
class Solution {
public:
    /* let ways[i][j][k] = # ways to construct an array of length i with max element equal to j and a search cost of k. */
    long long ways[51][101][51];
    const int MOD = 1e9 + 7;
    
    int numOfArrays(int n, int m, int k) {
		/* There are our base cases. For each index 1 <= j <= m, we require ways[1][j][1] = 1 because the array consisting of only the element j
			has length 1, maximum element j, and it has a search cost of 1. */
        for (int j = 1; j <= m; j++) {
                ways[1][j][1] = 1;
        }
        
        for (int a = 1; a <= n; a++) {
            for (int b = 1; b <= m; b++) {
                for (int c = 1; c <= k; c++) {
                    long long s = 0;

		            /* In this first case, we can append any element from [1, b] to the end of the array. */
                    s = (s + b * ways[a - 1][b][c]) % MOD;
                    
		           /* In this second case, we can append the element "b" to the end of the array. */
                    for (int x = 1; x < b; x++) {
						s = (s + ways[a - 1][x][c - 1]) % MOD;
                    }
					
                    ways[a][b][c] = (ways[a][b][c] + s) % MOD;
                }
            }
        }

        long long ans = 0;
        for (int j = 1; j <= m; j++) {
            ans = (ans + ways[n][j][k]) % MOD;
        }
        
        return int(ans);
    }
};
```

The runtime of this solution is `O(NKM^2)`, but you can achieve `O(NKM * log(M))` with a Fenwick tree, or  `O(NKM)` by using prefix sums and eliminating the innermost for-loop.
</p>


### [Java] DFS Memoization - Clean code
- Author: hiepit
- Creation Date: Sun Apr 19 2020 12:02:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 15:14:50 GMT+0800 (Singapore Standard Time)

<p>
**Java**
```java
class Solution {
    public int numOfArrays(int n, int m, int k) {
        Integer[][][] dp = new Integer[n + 1][m + 1][k + 1];
        return dfs(n, m, k, 0, 0, 0, dp);
    }
    // dfs(... i, currMax, currCost) the number of ways to build the valid array `arr[i:]`
    //     keeping in mind that current maximum element is `currMax` and current search cost is `currCost`
    int dfs(int n, int m, int k, int i, int currMax, int currCost, Integer[][][] dp) {
        if (i == n) {
            if (k == currCost) return 1; // valid if the value search cost is equal to k
            return 0;
        }
        if (dp[i][currMax][currCost] != null) return dp[i][currMax][currCost];
        int ans = 0;
        for (int num = 1; num <= m; num++) {
            int newCost = currCost;
            int newMax = currMax;
            if (num > currMax) {
                newCost++;
                newMax = num;
            }
            if (newCost > k) break;
            ans += dfs(n, m, k, i + 1, newMax, newCost, dp);
            ans %= 1_000_000_007;
        }
        return dp[i][currMax][currCost] = ans;
    }
}
```

**Complexity**
- Time: `O(n * m * k * m)`
Explain: There are total `n*m*k` states stored in `dp`, each state needs maximum `m` iteration loops to calculate the result.
- Space: `O(n * m * k)`

**Optimized version**
Thank to @joker452 to realize this treat.
```java
class Solution {
    public int numOfArrays(int n, int m, int k) {
        Integer[][][] dp = new Integer[n + 1][m + 1][k + 1];
        return dfs(n, m, k, 0, 0, 0, dp);
    }
    // dfs(... i, currMax, currCost) the number of ways to build the valid array `arr[i:]`
    //     keeping in mind that current maximum element is `currMax` and current search cost is `currCost`
    int dfs(int n, int m, int k, int i, int currMax, int currCost, Integer[][][] dp) {
        if (i == n) {
            if (k == currCost) return 1; // valid if the value search cost is equal to k
            return 0;
        }
        if (dp[i][currMax][currCost] != null) return dp[i][currMax][currCost];
        int ans = 0;
        // Case 1: num in range [1, currMax], newMax = currMax, newCost = currCost
        ans += (long) currMax * dfs(n, m, k, i + 1, currMax, currCost, dp) % 1_000_000_007;

        // Case 2: num in range [currMax+1, m], newMax = num, newCost = currCost + 1
        if (currCost + 1 <= k) {
            for (int num = currMax + 1; num <= m; num++) {
                ans += dfs(n, m, k, i + 1, num, currCost + 1, dp);
                ans %= 1_000_000_007;
            }
        }
        return dp[i][currMax][currCost] = ans;
    }
}
```

**Check out more DFS Memoization Methods**
- [1416. Restore The Array](https://leetcode.com/problems/restore-the-array/discuss/585552)
- [1411. Number of Ways to Paint N \xD7 3 Grid](https://leetcode.com/problems/number-of-ways-to-paint-n-3-grid/discuss/574912)

Please help to **UPVOTE** if this post is useful for you. 
If you have any questions, feel free to comment below.
Happy coding!
</p>


### [Python] 7 liner DP without using DFS
- Author: zzzliu
- Creation Date: Sun Apr 19 2020 13:02:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 13:13:53 GMT+0800 (Singapore Standard Time)

<p>
In this solution, `dp[i][j][k]` means how many possibilities with the restriction:
1. Length of the array is `i`
2. Number of cost is `j` (we jumped `j` times)
3. The **MAX** value in the array is `k`

Therefore, as you can see, at the end, we need to return the sum of all possible `k`\'s, rather than just return the `dp[N][K][M]`. Also please note that many of the other posts put `M` as the second dimension and `K` as the third dimension, which is different with this solution.

Okay, then let\'s see how to construct the DP array. There are two ways to get `dp[i][j][k]`
1. We don\'t want to jump and cost more.
In this case, we need to get `dp[i][j][k]` from `dp[i - 1][j][k]`. `dp[i - 1][j][k]` means the previous state has length `i - 1`, `j` jumps and the max value is `k`. Also since we don\'t want to jump in this step, our last element could be arbitrary value as long as it\'s smaller than or equal to k. In summary:
	```
	dp[i][j][k] += dp[i - 1][j][k] * k
	```
2. We want jump and pay the cost. 
This means we need to arrive `dp[i][j][k]` from `dp[i - 1][j - 1][kk] (kk = 1, 2, ... k - 1)`, remember `kk` needs to be smaller than `k` in order to make the max value in this array is `k`. In summary:
	```
	dp[i][j][k] += dp[i - 1][j - 1][kk], kk = 1, 2, ..., k - 1
	```


```
class Solution:
    def numOfArrays(self, N: int, M: int, K: int) -> int:
        dp = [[[0 for _ in range(M + 1)] for _ in range(K + 1)] for _ in range(N + 1)]
        # the array has length of 1, and 1 jump, only 1 way to do that, for any k
        for k in range(1, M + 1):
            dp[1][1][k] = 1
        
        for i, j, k in itertools.product(range(1, N + 1), range(1, K + 1), range(M + 1)):
            dp[i][j][k] += dp[i - 1][j][k] * k
            dp[i][j][k] += sum(dp[i - 1][j - 1][1:k])
        
        return sum(dp[N][K][1:]) % (10 ** 9 + 7)
```
</p>


