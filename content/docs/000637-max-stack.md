---
title: "Max Stack"
weight: 637
#id: "max-stack"
---
## Description
<div class="description">
<p>Design a max stack that supports push, pop, top, peekMax and popMax.</p>

<p>
<ol>
<li>push(x) -- Push element x onto stack.</li>
<li>pop() -- Remove the element on top of the stack and return it.</li>
<li>top() -- Get the element on the top.</li>
<li>peekMax() -- Retrieve the maximum element in the stack.</li>
<li>popMax() -- Retrieve the maximum element in the stack, and remove it. If you find more than one maximum elements, only remove the top-most one.</li>
</ol>
</p>

<p><b>Example 1:</b><br />
<pre>
MaxStack stack = new MaxStack();
stack.push(5); 
stack.push(1);
stack.push(5);
stack.top(); -> 5
stack.popMax(); -> 5
stack.top(); -> 1
stack.peekMax(); -> 5
stack.pop(); -> 1
stack.top(); -> 5
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>-1e7 <= x <= 1e7</li>
<li>Number of operations won't exceed 10000.</li>
<li>The last four operations won't be called when stack is empty.</li>
</ol>
</p>
</div>

## Tags
- Design (design)

## Companies
- Microsoft - 3 (taggedByAdmin: false)
- Lyft - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- LinkedIn - 14 (taggedByAdmin: true)
- Amazon - 4 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Pure Storage - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Two Stacks [Accepted]

**Intuition and Algorithm**

A regular stack already supports the first 3 operations, so we focus on the last two.

For `peekMax`, we remember the largest value we've seen on the side.  For example if we add `[2, 1, 5, 3, 9]`, we'll remember `[2, 2, 5, 5, 9]`.  This works seamlessly with `pop` operations, and also it's easy to compute: it's just the maximum of the element we are adding and the previous maximum.

For `popMax`, we know what the current maximum (`peekMax`) is.  We can pop until we find that maximum, then push the popped elements back on the stack.

Our implementation in Python will showcase extending the `list` class.

<iframe src="https://leetcode.com/playground/AZuvbhq6/shared" frameBorder="0" width="100%" height="500" name="AZuvbhq6"></iframe>


**Complexity Analysis**

* Time Complexity: $$O(N)$$ for the `popMax` operation, and $$O(1)$$ for the other operations, where $$N$$ is the number of operations performed.

* Space Complexity: $$O(N)$$, the maximum size of the stack.

---
#### Approach #2: Double Linked List + TreeMap [Accepted]

**Intuition**

Using structures like Array or Stack will never let us `popMax` quickly.  We turn our attention to tree and linked-list structures that have a lower time complexity for removal, with the aim of making `popMax` faster than $$O(N)$$ time complexity.

Say we have a double linked list as our "stack".  This reduces the problem to finding which node to remove, since we can remove nodes in $$O(1)$$ time.

We can use a TreeMap mapping values to a list of nodes to answer this question.  TreeMap can find the largest value, insert values, and delete values, all in $$O(\log N)$$ time.

**Algorithm**

Let's store the stack as a double linked list `dll`, and store a `map` from `value` to a `List` of `Node`.

* When we `MaxStack.push(x)`, we add a node to our `dll`, and add or update our entry `map.get(x).add(node)`.

* When we `MaxStack.pop()`, we find the value `val = dll.pop()`, and remove the node from our `map`, deleting the entry if it was the last one.

* When we `MaxStack.popMax()`, we use the `map` to find the relevant node to `unlink`, and return it's value.

The above operations are more clear given that we have a working `DoubleLinkedList` class.  The implementation provided uses `head` and `tail` *sentinels* to simplify the relevant `DoubleLinkedList` operations.

A Python implementation was not included for this approach because there is no analog to *TreeMap* available.

<iframe src="https://leetcode.com/playground/b9tKyzdr/shared" frameBorder="0" width="100%" height="500" name="b9tKyzdr"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(\log N)$$ for all operations except `peek` which is $$O(1)$$, where $$N$$ is the number of operations performed.  Most operations involving `TreeMap` are $$O(\log N)$$.

* Space Complexity: $$O(N)$$, the size of the data structures used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### O(1) isn't possible
- Author: brendon4565
- Creation Date: Tue Nov 07 2017 14:44:53 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 23:30:51 GMT+0800 (Singapore Standard Time)

<p>
Because if it were, you could use this data structure to sort an array of numbers in O(n) time.

So, at the very least, either push(x) or popMax() must be O(logn)
</p>


### If LRU Cache is "Hard", so is this
- Author: FlorenceMachine
- Creation Date: Wed Nov 08 2017 15:00:14 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 08 2017 15:00:14 GMT+0800 (Singapore Standard Time)

<p>
This question is roughly on the same level as LRU Cache in terms of difficulty, so why is this one "Easy" and LRU Cache "Hard"?

It's easier to get a solution accepted by writing sub-optimal O(n) solutions I guess, but just because the wrong answer is easy doesn't mean the question is easy. This question is very complicated with a lot of different possibilities and time/space trade-offs.

Harder than LRU Cache imo.
</p>


### Java AC solution
- Author: inofance
- Creation Date: Fri Nov 10 2017 08:44:36 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 13:52:04 GMT+0800 (Singapore Standard Time)

<p>
```
class MaxStack {
    Stack<Integer> stack;
    Stack<Integer> maxStack;
    /** initialize your data structure here. */
    public MaxStack() {
        stack = new Stack<>();
        maxStack = new Stack<>();
    }
    
    public void push(int x) {
        pushHelper(x);
    }
    
    public void pushHelper(int x) {
        int tempMax = maxStack.isEmpty() ? Integer.MIN_VALUE : maxStack.peek();
        if (x > tempMax) {
            tempMax = x;
        }
        stack.push(x);
        maxStack.push(tempMax);
    }
    
    public int pop() {
        maxStack.pop();
        return stack.pop();
    }
    
    public int top() {
        return stack.peek();
    }
    
    public int peekMax() {
        return maxStack.peek();
    }
    
    public int popMax() {
        int max = maxStack.peek();
        Stack<Integer> temp = new Stack<>();
        
        while (stack.peek() != max) {
            temp.push(stack.pop());
            maxStack.pop();
        }
        stack.pop();
        maxStack.pop();
        while (!temp.isEmpty()) {
            int x = temp.pop();
            pushHelper(x);
        }
        return max;
    }
}
```
</p>


