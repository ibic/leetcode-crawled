---
title: "Sequence Reconstruction"
weight: 421
#id: "sequence-reconstruction"
---
## Description
<div class="description">
<p>Check whether the original sequence <code>org</code> can be uniquely reconstructed from the sequences in <code>seqs</code>. The <code>org</code> sequence is a permutation of the integers from 1 to n, with 1 &le; n &le; 10<sup>4</sup>. Reconstruction means building a shortest common supersequence of the sequences in <code>seqs</code> (i.e., a shortest sequence so that all sequences in <code>seqs</code> are subsequences of it). Determine whether there is only one sequence that can be reconstructed from <code>seqs</code> and it is the <code>org</code> sequence.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> org = [1,2,3], seqs = [[1,2],[1,3]]
<strong>Output:</strong> false
<strong>Explanation:</strong> [1,2,3] is not the only one sequence that can be reconstructed, because [1,3,2] is also a valid sequence that can be reconstructed.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> org = [1,2,3], seqs = [[1,2]]
<strong>Output:</strong> false
<strong>Explanation:</strong> The reconstructed sequence can only be [1,2].
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> org = [1,2,3], seqs = [[1,2],[1,3],[2,3]]
<strong>Output:</strong> true
<strong>Explanation:</strong> The sequences [1,2], [1,3], and [2,3] can uniquely reconstruct the original sequence [1,2,3].
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> org = [4,1,5,2,6,3], seqs = [[5,2,6,3],[4,1,5,2]]
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^4</code></li>
	<li><code>org</code> is a permutation of {1,2,...,n}.</li>
	<li><code>1 &lt;= segs[i].length &lt;= 10^5</code></li>
	<li><code>seqs[i][j]</code>&nbsp;fits in a 32-bit signed integer.</li>
</ul>

<p>&nbsp;</p>

<p><b><font color="red">UPDATE (2017/1/8):</font></b><br />
The <i>seqs</i> parameter had been changed to a list of list of strings (instead of a 2d array of strings). Please reload the code definition to get the latest changes.</p>

</div>

## Tags
- Graph (graph)
- Topological Sort (topological-sort)

## Companies
- Google - 7 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution using BFS Topological Sort
- Author: xietao0221
- Creation Date: Wed Nov 02 2016 12:21:24 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 09:27:20 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public boolean sequenceReconstruction(int[] org, int[][] seqs) {
        Map<Integer, Set<Integer>> map = new HashMap<>();
        Map<Integer, Integer> indegree = new HashMap<>();
        
        for(int[] seq: seqs) {
            if(seq.length == 1) {
                if(!map.containsKey(seq[0])) {
                    map.put(seq[0], new HashSet<>());
                    indegree.put(seq[0], 0);
                }
            } else {
                for(int i = 0; i < seq.length - 1; i++) {
                    if(!map.containsKey(seq[i])) {
                        map.put(seq[i], new HashSet<>());
                        indegree.put(seq[i], 0);
                    }

                    if(!map.containsKey(seq[i + 1])) {
                        map.put(seq[i + 1], new HashSet<>());
                        indegree.put(seq[i + 1], 0);
                    }

                    if(map.get(seq[i]).add(seq[i + 1])) {
                        indegree.put(seq[i + 1], indegree.get(seq[i + 1]) + 1);
                    }
                }
            }
        }

        Queue<Integer> queue = new LinkedList<>();
        for(Map.Entry<Integer, Integer> entry: indegree.entrySet()) {
            if(entry.getValue() == 0) queue.offer(entry.getKey());
        }

        int index = 0;
        while(!queue.isEmpty()) {
            int size = queue.size();
            if(size > 1) return false;
            int curr = queue.poll();
            if(index == org.length || curr != org[index++]) return false;
            for(int next: map.get(curr)) {
                indegree.put(next, indegree.get(next) - 1);
                if(indegree.get(next) == 0) queue.offer(next);
            }
        }
        return index == org.length && index == map.size();
    }
}
```
</p>


### Very short solution with explanation
- Author: dettier
- Creation Date: Mon Oct 31 2016 15:08:38 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 06:38:42 GMT+0800 (Singapore Standard Time)

<p>

For `org` to be uniquely reconstructible from `seqs` we need to satisfy 2 conditions:
1. Every sequence in `seqs` should be a subsequence in `org`. This part is obvious.
2. Every 2 consecutive elements in `org` should be consecutive elements in some sequence from `seqs`. Why is that? Well, suppose condition 1 is satisfied. Then for 2 any consecutive elements `x` and `y` in `org` we have 2 options.
   * We have both `x`and `y` in some sequence from `seqs`. Then (as condition 1 is satisfied) they must be consequtive elements in this sequence.
   * There is no sequence in `seqs` that contains both `x` and `y`. In this case we cannot uniquely reconstruct `org` from `seqs` as sequence with `x` and `y` switched would also be a valid original sequence for `seqs`.

So this are 2 necessary criterions. It is pretty easy to see that this are also sufficient criterions for `org` to be uniquely reconstructible (there is only 1 way to reconstruct sequence when we know that condition 2 is satisfied).

To implement this idea I have `idxs` hash that maps item to its index in `org` sequence to check condition 1. And I have `pairs` set that holds all consequitive element pairs for sequences from `seqs` to check condition 2 (I also consider first elements to be paired with previous `undefined` elements, it is necessary to check this).

```
var sequenceReconstruction = function(org, seqs) {
    const pairs = {};
    const idxs = {};
    
    for (let i = 0; i < org.length; i++)
        idxs[org[i]] = i;

    for (let j = 0; j < seqs.length; j++) {
        const s = seqs[j];
        for (let i = 0; i < s.length; i++) {
            if (idxs[s[i]] == null)
                return false;
            if (i > 0 && idxs[s[i - 1]] >= idxs[s[i]])
                return false;
            pairs[`${s[i - 1]}_${s[i]}`] = 1;
        }
    }

    for (let i = 0; i < org.length; i++)
        if (pairs[`${org[i - 1]}_${org[i]}`] == null)
            return false;

    return true;
};
```
</p>


### Boring Tricky Test Cases
- Author: yya119
- Creation Date: Mon Jun 25 2018 14:31:34 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 05:03:06 GMT+0800 (Singapore Standard Time)

<p>
For the test case [1] [], why the answer is false? Since there is only one element, there is only one permutation and an empty seqs is consistent with this permutation. Some tricky test cases are really boring. 
</p>


