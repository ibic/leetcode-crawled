---
title: "All Paths from Source Lead to Destination"
weight: 483
#id: "all-paths-from-source-lead-to-destination"
---
## Description
<div class="description">
<p>Given the <code>edges</code> of a directed graph where <code>edges[i] = [a<sub>i</sub>, b<sub>i</sub>]</code> indicates there is an edge between nodes <code>a<sub>i</sub></code> and <code>b<sub>i</sub></code>, and two nodes <code>source</code> and <code>destination</code> of this graph, determine whether or not all paths starting from <code>source</code> eventually, end at <code>destination</code>, that is:</p>

<ul>
	<li>At least one path exists from the <code>source</code> node to the <code>destination</code> node</li>
	<li>If a path exists from the <code>source</code> node to a node with no outgoing edges, then that node is equal to <code>destination</code>.</li>
	<li>The number of possible paths from <code>source</code> to <code>destination</code> is a finite number.</li>
</ul>

<p>Return <code>true</code> if and only if all roads from <code>source</code> lead to <code>destination</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/03/16/485_example_1.png" style="width: 200px; height: 208px;" />
<pre>
<strong>Input:</strong> n = 3, edges = [[0,1],[0,2]], source = 0, destination = 2
<strong>Output:</strong> false
<strong>Explanation:</strong> It is possible to reach and get stuck on both node 1 and node 2.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/03/16/485_example_2.png" style="width: 200px; height: 230px;" />
<pre>
<strong>Input:</strong> n = 4, edges = [[0,1],[0,3],[1,2],[2,1]], source = 0, destination = 3
<strong>Output:</strong> false
<strong>Explanation:</strong> We have two possibilities: to end at node 3, or to loop over node 1 and node 2 indefinitely.
</pre>

<p><strong>Example 3:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/03/16/485_example_3.png" style="width: 200px; height: 183px;" />
<pre>
<strong>Input:</strong> n = 4, edges = [[0,1],[0,2],[1,3],[2,3]], source = 0, destination = 3
<strong>Output:</strong> true
</pre>

<p><strong>Example 4:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/03/16/485_example_4.png" style="width: 200px; height: 183px;" />
<pre>
<strong>Input:</strong> n = 3, edges = [[0,1],[1,1],[1,2]], source = 0, destination = 2
<strong>Output:</strong> false
<strong>Explanation:</strong> All paths from the source node end at the destination node, but there are an infinite number of paths, such as 0-1-2, 0-1-1-2, 0-1-1-1-2, 0-1-1-1-1-2, and so on.
</pre>

<p><strong>Example 5:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/03/16/485_example_5.png" style="width: 150px; height: 131px;" />
<pre>
<strong>Input:</strong> n = 2, edges = [[0,1],[1,1]], source = 0, destination = 1
<strong>Output:</strong> false
<strong>Explanation:</strong> There is infinite self-loop at destination node.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10<sup>4</sup></code></li>
	<li><code>0 &lt;= edges.length&nbsp;&lt;= 10<sup>4</sup></code></li>
	<li><code>edges.length == 2</code></li>
	<li><code>0 &lt;= a<sub>i</sub>, b<sub>i</sub> &lt;= n - 1</code></li>
	<li><code>0 &lt;= source &lt;= n - 1</code></li>
	<li><code>0 &lt;= destination &lt;= n - 1</code></li>
	<li>The given graph may have self-loops and parallel edges.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Graph (graph)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] DFS with cycle detection (5ms)
- Author: Sithis
- Creation Date: Sun Jun 02 2019 05:47:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 04 2019 19:09:16 GMT+0800 (Singapore Standard Time)

<p>
Time complexity: `O(E + V)`.
Space complexity: `O(E + V)`.
```
class Solution {
    enum State { PROCESSING, PROCESSED }
    
    public boolean leadsToDestination(int n, int[][] edges, int src, int dest) {
        List<Integer>[] graph = buildDigraph(n, edges);
        return leadsToDest(graph, src, dest, new State[n]);
    }
    
    private boolean leadsToDest(List<Integer>[] graph, int node, int dest, State[] states) {
        if (states[node] != null) return states[node] == State.PROCESSED;
        if (graph[node].isEmpty()) return node == dest;
        states[node] = State.PROCESSING;
        for (int next : graph[node]) {
            if (!leadsToDest(graph, next, dest, states)) return false;
        }
        states[node] = State.PROCESSED;
        return true;
    }
    
    private List<Integer>[] buildDigraph(int n, int[][] edges) {
        List<Integer>[] graph = new List[n];
        for (int i = 0; i < n; i++) {
            graph[i] = new ArrayList<>();
        }
        for (int[] edge : edges) {
            graph[edge[0]].add(edge[1]);
        }
        return graph;
    }
}
```
</p>


### C++ Cycle Detection
- Author: votrubac
- Creation Date: Fri Jun 14 2019 04:10:38 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 14 2019 04:10:38 GMT+0800 (Singapore Standard Time)

<p>
For the ```visited``` array, ```-1``` means never processed, ```false``` is being analyzed, and ```true``` is that all routes from that nodes lead to the destination. This is to avoid re-processing portions of the graph.
```
bool dfs(vector<vector<int>> &al, vector<int> &visited, int i, int dest) {
  if (al[i].empty()) return i == dest;
  if (visited[i] != -1) return visited[i];
  visited[i] = 0;
  for (auto j : al[i])
    if (!dfs(al, visited, j, dest)) return false;
  return visited[i] = true;
}
bool leadsToDestination(int n, vector<vector<int>>& edges, int source, int destination) {
  vector<vector<int>> al(n);
  vector<int> visited(n, -1);
  for (auto e : edges) al[e[0]].push_back(e[1]);
  return dfs(al, visited, source, destination);
}
```
</p>


### Python | DFS
- Author: reddddddd
- Creation Date: Thu Jun 13 2019 00:04:50 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 13 2019 00:12:05 GMT+0800 (Singapore Standard Time)

<p>
In DFS approach, we procced with the regular cycle detection approach. If there is a cycle, then there are infinite number of paths, hence we return false. Also when the DFS path ends, we check if we are at destination to check if return value is `true` are `false` .  We also make use of optimizations to ensure paths once checked are not checked again. 

```python
from collections import defaultdict
class Solution:
    def leadsToDestination(self, n: int, edges: List[List[int]], source: int, destination: int) -> bool:
        g = defaultdict(set)
        visited = defaultdict(int)
        for [x,y] in edges:
            g[x].add(y)
                
        def dfs(node):
            if visited[node] == 1:
                return True
            elif visited[node] == -1:
                return False
            elif len(g[node]) == 0:
                return node==destination
            else:
                visited[node] = -1
                for child in g[node]:
                    if not dfs(child):
                        return False
                visited[node] = 1
                return True
        
        return dfs(source)
```
</p>


