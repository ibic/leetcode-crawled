---
title: "Number of 1 Bits"
weight: 181
#id: "number-of-1-bits"
---
## Description
<div class="description">
<p>Write a function that takes an unsigned integer and returns&nbsp;the number of &#39;1&#39;&nbsp;bits it has (also known as the <a href="http://en.wikipedia.org/wiki/Hamming_weight" target="_blank">Hamming weight</a>).</p>

<p><strong>Note:</strong></p>

<ul>
	<li>Note that in some languages such as Java, there is no unsigned integer type. In this case, the input will be given as a signed integer type. It should not affect your implementation, as the integer&#39;s internal binary representation is the same, whether it is signed or unsigned.</li>
	<li>In Java,&nbsp;the compiler represents the signed integers using <a href="https://en.wikipedia.org/wiki/Two%27s_complement" target="_blank">2&#39;s complement notation</a>. Therefore, in <strong>Example 3</strong>&nbsp;above, the input represents the signed integer.&nbsp;<code>-3</code>.</li>
</ul>

<p><b>Follow up</b>: If this function is called many times, how would you optimize it?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 00000000000000000000000000001011
<strong>Output:</strong> 3
<strong>Explanation:</strong> The input binary string <strong>00000000000000000000000000001011</strong> has a total of three &#39;1&#39; bits.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 00000000000000000000000010000000
<strong>Output:</strong> 1
<strong>Explanation:</strong> The input binary string <strong>00000000000000000000000010000000</strong> has a total of one &#39;1&#39; bit.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 11111111111111111111111111111101
<strong>Output:</strong> 31
<strong>Explanation:</strong> The input binary string <strong>11111111111111111111111111111101</strong> has a total of thirty one &#39;1&#39; bits.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The input must be a <strong>binary string</strong> of length <code>32</code></li>
</ul>

</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Box - 8 (taggedByAdmin: false)
- Cisco - 7 (taggedByAdmin: false)
- Facebook - 5 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: true)
- Qualcomm - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: true)
- Google - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
## Solution
---

#### Approach #1 (Loop and Flip) [Accepted]

** Algorithm**

The solution is straight-forward. We check each of the $$32$$ bits of the number. If the bit is $$1$$, we add one to the number of $$1$$-bits.

We can check the $$i^{th}$$ bit of a number using a *bit mask*. We start with a mask $$m=1$$, because the binary representation of $$1$$ is,

$$
0000\ 0000\ 0000\ 0000\ 0000\ 0000\ 0000\ 0001
$$
Clearly, a logical AND between any number and the mask $$1$$ gives us the least significant bit of this number. To check the next bit, we shift the mask to the left by one.

$$
0000\ 0000\ 0000\ 0000\ 0000\ 0000\ 0000\ 0010
$$

And so on.

**Java**

```java
public int hammingWeight(int n) {
    int bits = 0;
    int mask = 1;
    for (int i = 0; i < 32; i++) {
        if ((n & mask) != 0) {
            bits++;
        }
        mask <<= 1;
    }
    return bits;
}
```

**Complexity Analysis**

The run time depends on the number of bits in $$n$$. Because $$n$$ in this piece of code is a 32-bit integer, the time complexity is $$O(1)$$.

The space complexity is $$O(1)$$, since no additional space is allocated.

---
#### Approach #2 (Bit Manipulation Trick) [Accepted]

**Algorithm**

We can make the previous algorithm simpler and a little faster. Instead of checking every bit of the number, we repeatedly flip the least-significant $$1$$-bit of the number to $$0$$, and add $$1$$ to the sum. As soon as the number becomes $$0$$, we know that it does not have any more $$1$$-bits, and we return the sum.

The key idea here is to realize that for any number $$n$$, doing a bit-wise AND of $$n$$ and $$n - 1$$ flips the least-significant $$1$$-bit in $$n$$ to $$0$$. Why? Consider the binary representations of $$n$$ and $$n - 1$$.

![Number of 1 Bits](https://leetcode.com/media/original_images/191_Number_Of_Bits.png){:width="400px"}
{:align="center"}

*Figure 1. AND-ing $$n$$ and $$n-1$$ flips the least-significant $$1$$-bit to 0.*
{:align="center"}

In the binary representation, the least significant $$1$$-bit in $$n$$ always corresponds to a $$0$$-bit in $$n - 1$$. Therefore, anding the two numbers $$n$$ and $$n - 1$$ always flips the least significant $$1$$-bit in $$n$$ to $$0$$, and keeps all other bits the same.

Using this trick, the code becomes very simple.

**Java**

```java
public int hammingWeight(int n) {
    int sum = 0;
    while (n != 0) {
        sum++;
        n &= (n - 1);
    }
    return sum;
}
```

**Complexity Analysis**

The run time depends on the number of $$1$$-bits in $$n$$. In the worst case, all bits in $$n$$ are $$1$$-bits. In case of a 32-bit integer, the run time is $$O(1)$$.

The space complexity is $$O(1)$$, since no additional space is allocated.

Analysis written by: @noran.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java Solution, Bit Shifting
- Author: fabrizio3
- Creation Date: Sun Apr 05 2015 16:01:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 00:36:29 GMT+0800 (Singapore Standard Time)

<p>
```
public static int hammingWeight(int n) {
	int ones = 0;
    	while(n!=0) {
    		ones = ones + (n & 1);
    		n = n>>>1;
    	}
    	return ones;
}
```
 - An Integer in Java has 32 bits, e.g. 00101000011110010100001000011010.
 - To count the 1s in the Integer representation we put the input int
   n in bit AND with 1 (that is represented as
   00000000000000000000000000000001, and if this operation result is 1,
   that means that the last bit of the input integer is 1. Thus we add it to the 1s count.

> ones = ones + (n & 1);

 - Then we shift the input Integer by one on the right, to check for the
   next bit.

> n = n>>>1;

We need to use bit shifting unsigned operation **>>>** (while **>>** depends on sign extension)

 - We keep doing this until the input Integer is 0.

In Java we need to put attention on the fact that the maximum integer is 2147483647. Integer type in Java is signed and there is no unsigned int. So the input 2147483648 is represented in Java as -2147483648 (in java int type has a cyclic representation, that means **Integer.MAX_VALUE+1==Integer.MIN_VALUE**).
This force us to use 

> n!=0

 in the while condition and we cannot use 

> n>0

because the input 2147483648 would correspond to -2147483648 in java and the code would not enter the while if the condition is n>0 for n=2147483648.
</p>


### C++ Solution: n & (n - 1)
- Author: housed
- Creation Date: Thu Jul 30 2015 09:29:33 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 09:32:36 GMT+0800 (Singapore Standard Time)

<p>
    int hammingWeight(uint32_t n) {
        int count = 0;
        
        while (n) {
            n &= (n - 1);
            count++;
        }
        
        return count;
    }

n & (n - 1) drops the lowest set bit. It's a neat little bit trick.

Let's use n = 00101100 as an example. This binary representation has three 1s.

If n = 00101100, then n - 1 = 00101011, so n & (n - 1) = 00101100 & 00101011 = 00101000. Count = 1.

If n = 00101000, then n - 1 = 00100111, so n & (n - 1) = 00101000 & 00100111 = 00100000. Count = 2.

If n = 00100000, then n - 1 = 00011111, so n & (n - 1) = 00100000 & 00011111 = 00000000. Count = 3.

n is now zero, so the while loop ends, and the final count (the numbers of set bits) is returned.
</p>


### Short code of C++, O(m) by time, m is the count of 1's,  and another several method of O(1) time
- Author: makuiyu
- Creation Date: Tue Mar 10 2015 10:30:28 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 20:23:57 GMT+0800 (Singapore Standard Time)

<p>
Each time of "n &= n - 1", we delete one '1' from n.

    int hammingWeight(uint32_t n)
    {
        int res = 0;
        while(n)
        {
            n &= n - 1;
            ++ res;
        }
        return res;
    }

Another several method of O(1) time.

Add 1 by Tree:

    // This is a naive implementation, shown for comparison, and to help in understanding the better functions. 
    // It uses 24 arithmetic operations (shift, add, and).
    int hammingWeight(uint32_t n)
    {
        n = (n & 0x55555555) + (n >>  1 & 0x55555555); // put count of each  2 bits into those  2 bits 
        n = (n & 0x33333333) + (n >>  2 & 0x33333333); // put count of each  4 bits into those  4 bits 
        n = (n & 0x0F0F0F0F) + (n >>  4 & 0x0F0F0F0F); // put count of each  8 bits into those  8 bits 
        n = (n & 0x00FF00FF) + (n >>  8 & 0x00FF00FF); // put count of each 16 bits into those 16 bits 
        n = (n & 0x0000FFFF) + (n >> 16 & 0x0000FFFF); // put count of each 32 bits into those 32 bits 
        return n;
    }

    // This uses fewer arithmetic operations than any other known implementation on machines with slow multiplication.
    // It uses 17 arithmetic operations.
    int hammingWeight(uint32_t n)
    {
        n -= (n >> 1) & 0x55555555; //put count of each 2 bits into those 2 bits
        n = (n & 0x33333333) + (n >> 2 & 0x33333333); //put count of each 4 bits into those 4 bits
        n = (n + (n >> 4)) & 0x0F0F0F0F; //put count of each 8 bits into those 8 bits
        n += n >> 8; // put count of each 16 bits into those 8 bits
        n += n >> 16; // put count of each 32 bits into those 8 bits
        return n & 0xFF;
    }

    // This uses fewer arithmetic operations than any other known implementation on machines with fast multiplication.
    // It uses 12 arithmetic operations, one of which is a multiply.
    int hammingWeight(uint32_t n)
    {
        n -= (n >> 1) & 0x55555555; // put count of each 2 bits into those 2 bits
        n = (n & 0x33333333) + (n >> 2 & 0x33333333); // put count of each 4 bits into those 4 bits
        n = (n + (n >> 4)) & 0x0F0F0F0F; // put count of each 8 bits into those 8 bits 
        return n * 0x01010101 >> 24; // returns left 8 bits of x + (x<<8) + (x<<16) + (x<<24)
    }

\u2014\u2014From Wikipedia.
</p>


