---
title: "Complement of Base 10 Integer"
weight: 963
#id: "complement-of-base-10-integer"
---
## Description
<div class="description">
<p>Every non-negative integer <code>N</code>&nbsp;has a binary representation.&nbsp; For example,&nbsp;<code>5</code> can be represented as <code>&quot;101&quot;</code>&nbsp;in binary, <code>11</code> as <code>&quot;1011&quot;</code>&nbsp;in binary, and so on.&nbsp; Note that except for <code>N = 0</code>, there are no leading zeroes in any&nbsp;binary representation.</p>

<p>The <em>complement</em>&nbsp;of a binary representation&nbsp;is the number in binary you get when changing every <code>1</code> to a <code>0</code> and <code>0</code> to a <code>1</code>.&nbsp; For example, the complement of <code>&quot;101&quot;</code> in binary is <code>&quot;010&quot;</code> in binary.</p>

<p>For a given number <code>N</code> in base-10, return the complement of it&#39;s binary representation as a&nbsp;base-10 integer.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">5</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>5 is &quot;101&quot; in binary, with complement &quot;010&quot; in binary, which is 2 in base-10.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">7</span>
<strong>Output: </strong><span id="example-output-2">0</span>
<span id="example-output-1"><strong>Explanation: </strong>7 is &quot;111&quot; in binary, with complement &quot;000&quot; in binary, which is 0 in base-10.
</span></pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">10</span>
<strong>Output: </strong><span id="example-output-3">5</span>
<strong>Explanation: </strong>10 is &quot;1010&quot; in binary, with complement &quot;0101&quot; in binary, which is 5 in base-10.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= N &lt; 10^9</code></li>
	<li>This question is the same as 476:&nbsp;<a href="https://leetcode.com/problems/number-complement/">https://leetcode.com/problems/number-complement/</a></li>
</ol>
</div>
</div>
</div>

</div>

## Tags
- Math (math)

## Companies
- Cloudera - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Prerequisites

**XOR**

[XOR](https://en.wikipedia.org/wiki/Exclusive_or) of zero and a bit 
results in that bit

$$
0 \oplus x = x  
$$

> XOR of one and a bit flips that bit

$$
1 \oplus x = 1 - x  
$$

**Right Shift and Left Shift**

![fig](../Figures/476/shift2.png)

<br/>
<br/>

---
#### Overview

> The article is long, and the best approach is the one number 4. 
In the case of limited time, you could jump to it directly. 

There are two standard ways to solve the problem:

- To move along the number and flip bit by bit.

- To construct 1-bits bitmask which has the same length as the input number,
and to get the answer as `bitmask - num` or `bitmask ^ num`.

    For example, for $$\textrm{num} = 5 = (101)_2$$ the bitmask is $$\textrm{bitmask} = (111)_2$$,
    and the complement number is $$\textrm{bitmask} \oplus \textrm{num} = (010)_2 = 2$$.

![fig](../Figures/476/overview2.png)

#### Approach 1: Flip Bit by Bit

**Algorithm**

- Initiate 1-bit variable which will be used to flip bits one by one. 
Set it to the smallest register `bit = 1`.

- Initiate the marker variable which will be used to stop the loop 
over the bits `todo = num`.

- Loop over the bits. While `todo != 0`:

    - Flip the current bit: `num = num ^ bit`.

    - Prepare for the next run. Shift flip variable to the left and 
    `todo` variable to the right.

- Return `num`.

![fig](../Figures/476/flip2.png)

**Implementation**

<iframe src="https://leetcode.com/playground/wpUrrBou/shared" frameBorder="0" width="100%" height="293" name="wpUrrBou"></iframe>

**Complexity**

- Time Complexity: $$\mathcal{O}(1)$$, since we're doing not more
than 32 iterations here.

- Space Complexity: $$\mathcal{O}(1)$$.
<br/>
<br/>

---
#### Approach 2: Compute Bit Length and Construct 1-bits Bitmask

Instead of flipping bits one by one, let's construct 1-bits bitmask 
and flip all the bits at once. 

There are many ways to do it, let's start from the simplest one:

- Compute bit length of the input number $$l = [\log_2 \textrm{num}] + 1$$.

- Compute 1-bits bitmask of length $$l$$: $$\textrm{bitmask} = (1 << l) - 1$$.

- Return `num ^ bitmask`.

![fig](../Figures/476/bitmask3.png)

**Implementation**

<iframe src="https://leetcode.com/playground/aLEmppTD/shared" frameBorder="0" width="100%" height="242" name="aLEmppTD"></iframe>

**Complexity**

- Time Complexity: $$\mathcal{O}(1)$$.

- Space Complexity: $$\mathcal{O}(1)$$.
<br/>
<br/>


---
#### Approach 3: Built-in Functions to Construct 1-bits Bitmask

Approach 2 could be rewritten with the help of built-in functions:
`bit_length` in Python and `highestOneBit` in Java.
The first one is trivial, and `Integer.highestOneBit(int x)` 
method in Java returns int with leftmost bit set in x, 
i.e. `Integer.highestOneBit(3) = 2`. 

**Implementation**

<iframe src="https://leetcode.com/playground/UQXAMesv/shared" frameBorder="0" width="100%" height="140" name="UQXAMesv"></iframe>

**Complexity**

- Time Complexity: $$\mathcal{O}(1)$$ because one deals here with integers 
of not more than 32 bits.

- Space Complexity: $$\mathcal{O}(1)$$.
<br/>
<br/>

---
#### Approach 4: highestOneBit OpenJDK algorithm from Hacker's Delight

The best algorithm for this task is an implementation of `highestOneBit`
in OpenJDK. [This implementation is taken from "Hacker's Delight" book](http://hg.openjdk.java.net/jdk8/jdk8/jdk/file/687fd7c7986d/src/share/classes/java/lang/Integer.java#l40).

The idea is to create the same 1-bits bitmask by propagating 
the highest 1-bit into the lower ones. 

!?!../Documents/476_LIS.json:1000,316!?!

**Implementation**

<iframe src="https://leetcode.com/playground/PWxDAZyw/shared" frameBorder="0" width="100%" height="293" name="PWxDAZyw"></iframe>

**Complexity**

- Time Complexity: $$\mathcal{O}(1)$$.

- Space Complexity: $$\mathcal{O}(1)$$.

<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Find 111.....1111 >= N
- Author: lee215
- Creation Date: Sun Mar 17 2019 12:02:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 07 2019 20:38:35 GMT+0800 (Singapore Standard Time)

<p>
## **Hints**
1. what is the relationship between input and output
2. input + output = 111....11 in binary format
3. Is there any corner case?
4. 0 is a corner case expecting 1, output > input


## **Intuition**
Let\'s find the first number `X` that `X = 1111....1 > N`
And also, it has to be noticed that,
`N = 0` is a corner case expecting`1` as result.

<br>

## **Solution 1**:
`N + bitwiseComplement(N) = 11....11 = X`
Then `bitwiseComplement(N) = X - N`

**Java:**
```
    public int bitwiseComplement(int N) {
        int X = 1;
        while (N > X) X = X * 2 + 1;
        return X - N;
    }
```

**C++:**
```
    int bitwiseComplement(int N) {
        int X = 1;
        while (N > X) X = X * 2 + 1;
        return X - N;
    }
```

**Python:**
```
    def bitwiseComplement(self, N):
        X = 1
        while N > X: X = X * 2 + 1
        return X - N
```
<br>

## **Solution 2**:
`N ^ bitwiseComplement(N) = 11....11 = X`
`bitwiseComplement(N) = N ^ X`

**Java:**
```
    public int bitwiseComplement(int N) {
        int X = 1;
        while (N > X) X = X * 2 + 1;
        return N ^ X;
    }
```

**C++:**
```
    int bitwiseComplement(int N) {
        int X = 1;
        while (N > X) X = X * 2 + 1;
        return N ^ X;
    }
```

**Python:**
```
    def bitwiseComplement(self, N):
        X = 1
        while N > X: X = X * 2 + 1;
        return N ^ X
```
<br>

## **Complexity**
`O(logN)` Time
`O(1)` Space
<br>

## Python 1-lines
Use `bin`
```
    def bitwiseComplement(self, N):
        return (1 << len(bin(N)) >> 2) - N - 1
```
Use `translate`
```
    def bitwiseComplement(self, N):
        return int(bin(N)[2:].translate(string.maketrans(\'01\', \'10\')), 2)
```
</p>


### C++ 2 lines, XOR mask
- Author: votrubac
- Creation Date: Sun Mar 17 2019 12:00:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 17 2019 12:00:28 GMT+0800 (Singapore Standard Time)

<p>
We construct a XOR mask from all ones until it\'s larger or equal than the number. For example, for ```100101```, the mask will be ```111111```. And for ```1111```, the mask will be ```1111```.

The result is the XOR operation of the number and the mask.
```
int bitwiseComplement(int N, int c = 1) {
  while (c < N) c = (c << 1) + 1;
  return N ^ c;
}
```
</p>


### 4 approaches: bitwise operation, math formular, one naive simulation, and bonus
- Author: codedayday
- Creation Date: Mon May 04 2020 23:19:22 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 05 2020 22:23:48 GMT+0800 (Singapore Standard Time)

<p>
Find the smallest binary number c that is all 1s, (e.g. \u201C111\u201D, \u201C11111\u201D) that is greater or equal to N.
ans = C ^ N or C \u2013 N

More details:
^: bitwise XOR operator
**Table 1. The truth table of bitwise XOR operator:**
**1^0 = 1
1^1 = 0**
0^0 = 0
0^1 = 1
**The upper two rows  of Table 1 is used in the problem.**



Time complexity: O(log(n))
Space complexity: O(1)

Solution 1:  bit shift based solution
```
class Solution {// bit shift based solution
public:
  int bitwiseComplement(int N) {
    int c = 1; // c = pow(2, x) \u2013 1; c is the smallest number >= N
    while (c < N) 
      c = (c << 1) | 1;
      //c = c*2 + 1;  same as above
    return N ^ c;  
    //return c - N; // also ok  
  }
};
```

Solution 2: math based solution: log2, pow

```
class Solution { //math based solution: log2, pow
public:
    int bitwiseComplement(int N) {        
        if(N == 0) return 1;
        int exponenet = (int)log2(N)+1;
        int flipper = (int)pow(2, exponenet)-1;            
        return N ^ flipper;
    }
};
```

What, the above two solution is still too anti-human?!
Of course, it is.
If you are still not comfortable with the above two methods due to too much specific knowledge about bit operation,
you might find the last solution might be more natural :

Solution 3:  naive simulation: find each single digit, then flip it
```
class Solution { // BEST1: find each single digit, then flip it
public:
    int findComplement(int num) {
        int res = 0, ind = 0;
        while(num > 0){
            //find single digit from low to high significance
            int digit = num % 2; 
            int flippedDigit = (digit == 1? 0 : 1); // flip digit
            res += (flippedDigit << ind);
            num /= 2;            
            ind++;
        }
        return res;
    }
};

```
This is a bonus solution: 

Solution 4:  Get all-1 mask by spreading 1 from most significant to the rest
```
class Solution { // Get all-1 mask by spreading 1 from most significant to the rest:
public://Time/Space: O(1); O(1)
    int findComplement(int num) {
	    if(num ==0) return 1;
        int mask = num;
        mask |= mask >> 1;
        mask |= mask >> 2;
        mask |= mask >> 4;
        mask |= mask >> 8;
        mask |= mask >> 16;
        return num ^ mask;
    }
};

```
Explanation: 

To understand the solution, we need to go backwards. The aim is to xor the given number with a mask. The mask should contain all 1s in its rightmost bits. However, the number of rightmost bits is important. In the case of 5(101), for example, the number of rightmost bits must be 3, since 5 uses 3 rightmost bits. The mask must be 111 for 5(101). When we xor 111 with 101, we will get 010. As another example, the mask will be 111111 for 38(100110)

So the problem boils down to generating the mask. Let\'s think about 8-bit numbers. We can later extend the same logic to 32-bit integers. I will count the bits from right to left, starting with 1.

The largest positive numbers represented by 8 bits will set 7th bit to be 1.
In binary format, they should be like:
01ABCDEF
where A,B,C,D,E,and F might be 0 or 1 and doesn\'t matter.

The first operation, mask |= mask>>1; will set the 6th bit. So, mask will become (011bcdef). Once again, we do not care the actual value of c,d,e,f.
Now, we know that the 7th and 6th bits are set, we can safely shift the mask to right by not 1 but 2 bits. mask |= mask>>2; will now set the 5th and 4th bits. 
By the same reason, we can now shift the mask to right by not 1, 2 or 3 but 4 bits. 
That is the threshold for 8-bit numbers. We do not need to shift more.

The following is a detailed illustration with an example:

```
Note1: 8-bit: 2^7-1, the large number is: 127
Note2: the first few positive numbers in 8-bit, must be in the format like: 01ABCDEF, where A,B,C,D,E,F belongs to [0,1]
For example: 
127 is: 01111111
126 is: 01111110
125 is: 01111101
..


Let\'s assume: num = 64 to make our point:
mask =  num;  // this ensure the highest significance digit is same as that of num.

01ABCDEF  (mask, we do not the value of A,B,C,D,E,F)         

Illustration of operation: mask |= mask >> 1;
01ABCDEF
 01ABCDE
---------- OR
011?????

Illustration of operation: mask |= mask >> 2;
011?????
  011???
---------- OR
01111???

Illustration of operation: mask |= mask >> 4;
01111???
    0111
---------- OR
01111111

We found our goal: 
01111111

```




For 32-bit integers, the shift operations should continue until reaching to 16. For 64-bit longs, that will be 32.


Reference/Credit:
https://leetcode.com/problems/number-complement/discuss/96103/maybe-fewest-operations

</p>


