---
title: "Number of Dice Rolls With Target Sum"
weight: 1133
#id: "number-of-dice-rolls-with-target-sum"
---
## Description
<div class="description">
<p>You have <code>d</code> dice, and each die has <code>f</code> faces numbered <code>1, 2, ..., f</code>.</p>

<p>Return the number of possible ways (out of <code>f<sup>d</sup></code>&nbsp;total ways) <strong>modulo <code>10^9 + 7</code></strong> to roll the dice so the sum of the face up numbers equals <code>target</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> d = 1, f = 6, target = 3
<strong>Output:</strong> 1
<strong>Explanation: </strong>
You throw one die with 6 faces.  There is only one way to get a sum of 3.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> d = 2, f = 6, target = 7
<strong>Output:</strong> 6
<strong>Explanation: </strong>
You throw two dice, each with 6 faces.  There are 6 ways to get a sum of 7:
1+6, 2+5, 3+4, 4+3, 5+2, 6+1.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> d = 2, f = 5, target = 10
<strong>Output:</strong> 1
<strong>Explanation: </strong>
You throw two dice, each with 5 faces.  There is only one way to get a sum of 10: 5+5.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> d = 1, f = 2, target = 3
<strong>Output:</strong> 0
<strong>Explanation: </strong>
You throw one die with 2 faces.  There is no way to get a sum of 3.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> d = 30, f = 30, target = 500
<strong>Output:</strong> 222616187
<strong>Explanation: </strong>
The answer must be returned modulo 10^9 + 7.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= d, f &lt;= 30</code></li>
	<li><code>1 &lt;= target &lt;= 1000</code></li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 8 (taggedByAdmin: true)
- Microsoft - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python DP with memoization - explained
- Author: cjporteo
- Creation Date: Sun Aug 11 2019 12:16:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 23 2020 09:06:06 GMT+0800 (Singapore Standard Time)

<p>
Let **dp**(d, f, target) be the number of possible dice rolls for the given parameters.
<br>
<hr>
<br>
As an initial example, pretend we have 5 dice with 6 faces each and we want to determine how many ways to make 18.

In other words, what is **dp**(5, 6, 18)?

At first glance, this is seems difficult and overwhelming. But if we make one simple observation, we can reduce this big problem into several smaller sub-problems. We have 5 dice, but let\'s first just look at ONE of these dice (say the last one). This die can take on ```f=6``` different values (1 to 6), so we consider what happens when we fix its value to each possibility (6 cases):

**Case 1**: The last die is a 1. The remaining 4 dice must sum to 18-1=**17**. This can happen **dp**(4, 6, 17) ways.
**Case 2**: The last die is a 2. The remaining 4 dice must sum to 18-2=**16**. This can happen **dp**(4, 6, 16) ways.
**Case 3**: The last die is a 3. The remaining 4 dice must sum to 18-3=**15**. This can happen **dp**(4, 6, 15) ways.
**Case 4**: The last die is a 4. The remaining 4 dice must sum to 18-4=**14**. This can happen **dp**(4, 6, 14) ways.
**Case 5**: The last die is a 5. The remaining 4 dice must sum to 18-5=**13**. This can happen **dp**(4, 6, 13) ways.
**Case 6**: The last die is a 6. The remaining 4 dice must sum to 18-6=**12**. This can happen **dp**(4, 6, 12) ways.

dp(5, 6, 18) = dp(4, 6, 17) + dp(4, 6, 16) + dp(4, 6, 15) + dp(4, 6, 14) + dp(4, 6, 13) + dp(4, 6, 12)

We sum up the solutions these 6 cases to arrive at our result. Of course, each of these cases branches off into 6 cases of its own, and the recursion only resolves when ```d```=0. The handling of this base case is explained below.
<br>
<hr>
<br>
The general relation is:

**dp**(d, f, target) = **dp**(d-1, f, target-1) + **dp**(d-1, f, target-2) + ... + **dp**(d-1, f, target-f)

The base case occurs when ```d``` = 0. We can make ```target=0``` with 0 dice, but nothing else.
So **dp**(0, f, t) = 0 iff ```t``` != 0, and **dp**(0, f, 0) = 1. 

Use memoization to avoid repeated calculations and don\'t conisider negative targets.

```
class Solution:
    def numRollsToTarget(self, d: int, f: int, target: int) -> int:
        memo = {}
        def dp(d, target):
            if d == 0:
                return 0 if target > 0 else 1
            if (d, target) in memo:
                return memo[(d, target)]
            to_return = 0
            for k in range(max(0, target-f), target):
                to_return += dp(d-1, k)
            memo[(d, target)] = to_return
            return to_return
        return dp(d, target) % (10**9 + 7)
```


</p>


### C++ Coin Change 2
- Author: votrubac
- Creation Date: Sun Aug 11 2019 12:52:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 18 2019 05:18:00 GMT+0800 (Singapore Standard Time)

<p>
This problem is like [518. Coin Change 2](https://leetcode.com/problems/coin-change-2/), with the difference that the total number of coins (dices) should be equal to ```d```.
# Basic Solution (TLE)
A basic brute-force solution could be to try all combinations of all faces for all dices, and count the ones that give a total sum of ```target```.
```
int numRollsToTarget(int d, int f, int target, int res = 0) {
  if (d == 0 || target <= 0) return d == target;
  for (auto i = 1; i <= f; ++i)
    res = (res + numRollsToTarget(d - 1, f, target - i)) % 1000000007;
  return res;
}
```
> ```d == target``` is the shorthand for ``` d == 0 && target == 0```.
## Complexity Analysis
Runtime: O(f ^ d).
Memory: O(d) for the stack.
# Top-down DP
We memoise the previously computed results for dice ```i``` and and ```target```.

Since ```dp``` is initialized with zeros, we store there ```res + 1``` to check if the result has been pre-computed. This is an optimization for brevity and speed.
```
int dp[31][1001] = {};
int numRollsToTarget(int d, int f, int target, int res = 0) {
  if (d == 0 || target <= 0) return d == target;
  if (dp[d][target]) return dp[d][target] - 1;
  for (auto i = 1; i <= f; ++i)
    res = (res + numRollsToTarget(d - 1, f, target - i)) % 1000000007;
  dp[d][target] = res + 1;
  return res;
}
```
## Complexity Analysis
Runtime: O(d * f * target).
Memory: O(d * target) for the memoisation.
# Bottom-Up DP
We can define our ```dp[i][k]``` as number of ways we can get ```k``` using ```i``` dices. As an initial point, there is one way to get to ```0``` using zero dices.

Then, for each dice ```i``` and face ```j```, accumulate the number of ways we can get to ```k```. 

Note that for the bottom-up solution, we can reduce our memory complexity as we only need to store counts for the previous dice.
```
int numRollsToTarget(int d, int f, int target) {
  vector<int> dp(target + 1);
  dp[0] = 1;
  for (int i = 1; i <= d; ++i) {
    vector<int> dp1(target + 1);
    for (int j = 1; j <= f; ++j)
      for (auto k = j; k <= target; ++k)
        dp1[k] = (dp1[k] + dp[k - j]) % 1000000007;
    swap(dp, dp1);
  }
  return dp[target];
}
```
## Complexity Analysis
Runtime: O(d * f * target).
Memory: O(target) as we only store counts for the last dice.
## Further Optimizations
We can re-use the same array if we process our targets backwards. Just be sure to clear the previous target count (```dp[k]```) - this value is for the previous dice and we do not need it anymore.

> Note that we need to flip the order of processing - we iterate though targets first (```k```), and then through the faces (```j```). I left the variable names the same as for the previous solution. 

Thanks [dylan20](https://leetcode.com/dylan20/) for this memory optimization tip!

You can also see that, this way, we can stop at ```dp[target]``` when processing the last dice.
```
int numRollsToTarget(int d, int f, int target) {
  int dp[target + 1] = { 1 }, i, j, k;
  for (i = 1; i <= d; ++i)
    for (k = target; k >= (i == d ? target : 0); --k)
      for (j = k - 1, dp[k] = 0; j >= max(0, k - f); --j)
        dp[k] = (dp[k] + dp[j]) % 1000000007;
  return dp[target];
}
```
</p>


### Java Memo DFS
- Author: wushangzhen
- Creation Date: Sun Aug 11 2019 12:02:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Nov 01 2019 14:49:30 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    int MOD = 1000000000 + 7;
    Map<String, Integer> memo = new HashMap<>();
    public int numRollsToTarget(int d, int f, int target) {
        if (d == 0 && target == 0) {
            return 1;
        }
        if (d == 0 || target == 0) {
            return 0;
        }
        String str = d + " " + target;
        if (memo.containsKey(str)) {
            return memo.get(str);
        }
        int res = 0;
        for (int i = 1; i <= f; i++) {
            if (target >= i) {
                res = (res + numRollsToTarget(d - 1, f, target - i)) % MOD;
            } else {
                break;
            }
        }
        memo.put(str, res);
        return res;
    }
}
```
Bottom Up Version

```
class Solution {
    public int numRollsToTarget(int d, int f, int target) {
        int MOD = (int)Math.pow(10, 9) + 7;
        long[][] dp = new long[d + 1][target + 1];
        dp[0][0] = 1;
        for (int i = 1; i <= d; i++) {
            for (int j = 0; j <= target; j++) {
                for (int k = 1; k <= f; k++) {
                    if (j >= k) {
                        dp[i][j] = (dp[i][j] + dp[i - 1][j - k]) % MOD;
                    } else {
                        break;
                    }
                }
            }
        }
        return (int)dp[d][target];
    }
}
```
</p>


