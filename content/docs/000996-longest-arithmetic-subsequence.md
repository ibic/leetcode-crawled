---
title: "Longest Arithmetic Subsequence"
weight: 996
#id: "longest-arithmetic-subsequence"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of integers, return the <strong>length</strong> of the longest arithmetic subsequence in <code>A</code>.</p>

<p>Recall that a <em>subsequence</em> of <code>A</code> is a list <code>A[i_1], A[i_2], ..., A[i_k]</code> with <code>0 &lt;= i_1 &lt; i_2 &lt; ... &lt; i_k &lt;= A.length - 1</code>, and that a sequence <code>B</code>&nbsp;is <em>arithmetic</em> if <code>B[i+1] - B[i]</code> are all the same value (for <code>0 &lt;= i &lt; B.length - 1</code>).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> A = [3,6,9,12]
<strong>Output:</strong> 4
<strong>Explanation: </strong>
The whole array is an arithmetic sequence with steps of length = 3.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> A = [9,4,7,2,10]
<strong>Output:</strong> 3
<strong>Explanation: </strong>
The longest arithmetic subsequence is [4,7,10].
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> A = [20,1,15,3,10,5,8]
<strong>Output:</strong> 4
<strong>Explanation: </strong>
The longest arithmetic subsequence is [20,15,10,5].
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= A.length &lt;= 1000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 500</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 3 (taggedByAdmin: true)
- Facebook - 6 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Quora - 4 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)
- snapdeal - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] DP
- Author: lee215
- Creation Date: Sun Apr 14 2019 12:07:16 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 23 2020 17:16:07 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
`dp[index][diff]` equals to the length of arithmetic sequence at `index` with difference `diff`.
<br>

# Complexity
Time `O(N^2)`
Space `O(N^2)`
<br>

**Java**
```java
    public int longestArithSeqLength(int[] A) {
        int res = 2, n = A.length;
        HashMap<Integer, Integer>[] dp = new HashMap[n];
        for (int j = 0; j < A.length; j++) {
            dp[j] = new HashMap<>();
            for (int i = 0; i < j; i++) {
                int d = A[j] - A[i];
                dp[j].put(d, dp[i].getOrDefault(d, 1) + 1);
                res = Math.max(res, dp[j].get(d));
            }
        }
        return res;
    }
```
**C++, using unordered_map**
TLE now.
```cpp
    int longestArithSeqLength(vector<int>& A) {
        unordered_map<int, unordered_map<int, int>> dp;
        int res = 2, n = A.size();
        for (int i = 0; i < n; ++i)
            for (int j = i + 1; j < n; ++j)  {
                int d = A[j] - A[i];
                dp[d][j] = dp[d].count(i) ? dp[d][i] + 1 : 2;
                res = max(res, dp[d][j]);
            }
        return res;
    }
```
**C++, using vector**
```cpp
    int longestArithSeqLength(vector<int>& A) {
        int res = 2, n = A.size();
        vector<vector<int>> dp(n, vector<int>(2000, 0));
        for (int i = 0; i < n; ++i)
            for (int j = i + 1; j < n; ++j)  {
                int d = A[j] - A[i] + 1000;
                dp[j][d] = max(2, dp[i][d] + 1);
                res = max(res, dp[j][d]);
            }
        return res;
    }
```
**Python:**
```py
    def longestArithSeqLength(self, A):
        dp = {}
        for i in xrange(len(A)):
            for j in xrange(i + 1, len(A)):
                dp[j, A[j] - A[i]] = dp.get((i, A[j] - A[i]), 1) + 1
        return max(dp.values())
```

</p>


### Java DP O(n^2) solution with explanation
- Author: saket4
- Creation Date: Sun Apr 14 2019 12:47:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 14 2019 12:47:56 GMT+0800 (Singapore Standard Time)

<p>
The main idea is to maintain a map of differences seen at each index. 

We iteratively build the map for a new index `i`, by considering all elements to the left one-by-one.
For each pair of indices `(i,j)`  and difference `d = A[i]-A[j]` considered, we check if there was an existing chain at the index `j` with difference `d` already. 
 - If yes, we can then extend the existing chain length by 1.
  - Else, if not, then we can start a new chain of length `2` with this new difference `d` and `(A[j], A[i])` as its elements.

At the end, we can then return the maximum chain length that we have seen so far.
```
Time Complexity: O(n^2)
Space Complexity: O(n^2)
```

```java
class Solution {
    public int longestArithSeqLength(int[] A) {
        if (A.length <= 1) return A.length;
       
        int longest = 0;
        
        // Declare a dp array that is an array of hashmaps.
        // The map for each index maintains an element of the form-
        //   (difference, length of max chain ending at that index with that difference).        
        HashMap<Integer, Integer>[] dp = new HashMap[A.length];
        
        for (int i = 0; i < A.length; ++i) {
            // Initialize the map.
            dp[i] = new HashMap<Integer, Integer>();
        }
        
        for (int i = 1; i < A.length; ++i) {
            int x = A[i];
            // Iterate over values to the left of i.
            for (int j = 0; j < i; ++j) {
                int y = A[j];
                int d = x - y;
                
                // We at least have a minimum chain length of 2 now,
                // given that (A[j], A[i]) with the difference d, 
                // by default forms a chain of length 2.
                int len = 2;  
                
                if (dp[j].containsKey(d)) {
                    // At index j, if we had already seen a difference d,
                    // then potentially, we can add A[i] to the same chain
                    // and extend it by length 1.
                    len = dp[j].get(d) + 1;
                }
                
                // Obtain the maximum chain length already seen so far at index i 
                // for the given differene d;
                int curr = dp[i].getOrDefault(d, 0);
                
                // Update the max chain length for difference d at index i.
                dp[i].put(d, Math.max(curr, len));
                
                // Update the global max.
                longest = Math.max(longest, dp[i].get(d));
            }
        }
        
        return longest;
    }
}
```
</p>


### C++ DP with explanation
- Author: HaelC
- Creation Date: Thu Jan 09 2020 12:16:57 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 09 2020 12:16:57 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    int longestArithSeqLength(vector<int>& A) {
        int n = A.size();
        int result = 0;
        vector<unordered_map<int, int>> dp(n);
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < i; j++) {
                int diff = A[i]-A[j];
                dp[i][diff] = dp[j].count(diff) > 0 ? dp[j][diff] + 1 : 2;
                result = max(result, dp[i][diff]);
            }
        }
        return result;
    }
};
```

This problem is similar to [Longest Increasing Subsequence](https://leetcode.com/problems/longest-increasing-subsequence/) problem. The difference is that we need to consider the *arithmetic difference* in this problem. How to keep track of the length as well as the difference? We can use a hashmap, whose key is the difference and value is the length. Then we can solve the problem with dynamic programming:  
As noted in the problem description, `2 <= A.length`, so we don\'t need to consider the edge case when there is no element or only one element in `A`. The base case is `A.length == 2`, then `A` itself is the longest arithmetic subsequence because any two numbers meet the condition of *arithmetic*.  
The optimization step is that for two elements `A[i]` and `A[j]` where `j < i`, the difference between `A[i]` and `A[j]` (name it `diff`) is a critical condition. If the hashmap at position `j` has the key `diff`, it means that there is an arithmetic subsequence ending at index `j`, with arithmetic difference `diff` and length `dp[j][diff]`. And we just add the length by `1`. If hashmap does not have the key `diff`, then those two elements can form a 2-length arithmetic subsequence. And update the `result` if necessary during the iteration.
</p>


