---
title: "Longest Absolute File Path"
weight: 371
#id: "longest-absolute-file-path"
---
## Description
<div class="description">
<p>Suppose we have the file system represented in the following picture:</p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/08/28/mdir.jpg" style="width: 681px; height: 322px;" /></p>

<p>We will represent the file system as a string where <code>&quot;\n\t&quot;</code> mean a subdirectory of the main directory, <code>&quot;\n\t\t&quot;</code> means a subdirectory of the subdirectory of the main directory and so on. Each folder will be represented as a string of letters <strong>and/or</strong> digits. Each file will be in the form <code>&quot;s1.s2&quot;</code> where <code>s1</code> and <code>s2</code> are strings of letters <strong>and/or</strong> digits.</p>

<p>For example, the file system above is represented as&nbsp;<code>&quot;dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext&quot;</code>.</p>

<p>Given a string <code>input</code> representing the file system in the explained&nbsp;format, return the length of the longest absolute path to <strong>a file</strong> in the abstracted file system. If there is no file in the system, return <code>0</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/28/dir1.jpg" style="width: 401px; height: 202px;" />
<pre>
<strong>Input:</strong> input = &quot;dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext&quot;
<strong>Output:</strong> 20
<strong>Explanation:</strong> We have only one file and its path is &quot;dir/subdir2/file.ext&quot; of length 20.
The path &quot;dir/subdir1&quot; doesn&#39;t contain any files.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/28/dir2.jpg" style="width: 641px; height: 322px;" />
<pre>
<strong>Input:</strong> input = &quot;dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext&quot;
<strong>Output:</strong> 32
<strong>Explanation:</strong> We have two files:
&quot;dir/subdir1/file1.ext&quot; of length 21
&quot;dir/subdir2/subsubdir2/file2.ext&quot; of length 32.
We return 32 since it is the longest path.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> input = &quot;a&quot;
<strong>Output:</strong> 0
<strong>Explanation:</strong> We don&#39;t have any files.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= input.length &lt;= 10<sup>4</sup></code></li>
	<li><code>input</code>&nbsp;may contain lower-case or upper-case English letters, a new line character <code>&#39;\n&#39;</code>, a tab character <code>&#39;\t&#39;</code>,&nbsp;a dot <code>&#39;.&#39;</code>, a space <code>&#39; &#39;</code>&nbsp;or digits.</li>
</ul>

</div>

## Tags


## Companies
- Square - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)
- Wish - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (java)
```java
import java.util.ArrayList;
import java.util.List;

public class Solution {
    private int getLevel(String path) {
        int level = 0;
        int pl = path.length();
        for (int i = 0; i < pl && path.charAt(i) == '\t'; i++) {
            level += 1;
        }
        return level;
    }

    // absPath will _not_ be empty
    private int getAbsPathLen(List<Integer> absPath) {
        int len = 0;
        for (Integer path : absPath) {
            len += path + 1;
        }
        return len - 1;
    }

    public int lengthLongestPath(String input) {
        int longest = 0;
        String[] paths = input.split("\\n");
        List<Integer> absPath = new ArrayList<Integer>();
        for (String path : paths) {
            int level = getLevel(path);
            String thisPath = path.substring(level);
            int thisPathLen = thisPath.length();
            int currentLevel = absPath.size() - 1;
            if (currentLevel < level) {
                absPath.add(thisPathLen);
            } else {
                absPath.set(level, thisPathLen);
                int removeCount = currentLevel - level;
                for (int i = 0; i < removeCount; i++) {
                    absPath.remove(absPath.size() - 1);
                }
            }
            if (path.contains(".")) {
                int absPathLen = getAbsPathLen(absPath);
                if (absPathLen > longest) {
                    longest = absPathLen;
                }
            }
        }
        return longest;
    }
}

```

## Top Discussions
### 9 lines 4ms Java solution
- Author: sky-xu
- Creation Date: Tue Aug 23 2016 13:59:23 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 12:01:28 GMT+0800 (Singapore Standard Time)

<p>
```
public int lengthLongestPath(String input) {
        Deque<Integer> stack = new ArrayDeque<>();
        stack.push(0); // "dummy" length
        int maxLen = 0;
        for(String s:input.split("\
")){
            int lev = s.lastIndexOf("\	")+1; // number of "\	"
            while(lev+1<stack.size()) stack.pop(); // find parent
            int len = stack.peek()+s.length()-lev+1; // remove "/t", add"/"
            stack.push(len);
            // check if it is file
            if(s.contains(".")) maxLen = Math.max(maxLen, len-1); 
        }
        return maxLen;
    }
```

An even shorter and faster solution using array instead of stack:
```
public int lengthLongestPath(String input) {
    String[] paths = input.split("\
");
    int[] stack = new int[paths.length+1];
    int maxLen = 0;
    for(String s:paths){
        int lev = s.lastIndexOf("\	")+1, curLen = stack[lev+1] = stack[lev]+s.length()-lev+1;
        if(s.contains(".")) maxLen = Math.max(maxLen, curLen-1);
    }
    return maxLen;
}
```
</p>


### Simple Python solution
- Author: StefanPochmann
- Creation Date: Mon Aug 22 2016 15:48:07 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 04 2018 08:21:26 GMT+0800 (Singapore Standard Time)

<p>
The number of tabs is my `depth` and for each depth I store the current path length.

    def lengthLongestPath(self, input):
        maxlen = 0
        pathlen = {0: 0}
        for line in input.splitlines():
            name = line.lstrip('\	')
            depth = len(line) - len(name)
            if '.' in name:
                maxlen = max(maxlen, pathlen[depth] + len(name))
            else:
                pathlen[depth + 1] = pathlen[depth] + len(name) + 1
        return maxlen
</p>


### This problem is not well-defined. It should state that 4-space is considered as a TAB under certain situation.
- Author: yubad2000
- Creation Date: Wed Aug 24 2016 05:16:16 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 12:43:27 GMT+0800 (Singapore Standard Time)

<p>
After many try-and-error, I finally figured out the solution is required to handle the blank spaces in a special way. I think it should be stated in the problem statement, not just let people guess this from the expected answers. This will waste many people a lot of time....
</p>


