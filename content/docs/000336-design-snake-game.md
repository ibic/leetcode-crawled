---
title: "Design Snake Game"
weight: 336
#id: "design-snake-game"
---
## Description
<div class="description">
<p>Design a <a href="https://en.wikipedia.org/wiki/Snake_(video_game)" target="_blank">Snake game</a> that is played on a device with screen size = <i>width</i> x <i>height</i>. <a href="http://patorjk.com/games/snake/" target="_blank">Play the game online</a> if you are not familiar with the game.</p>

<p>The snake is initially positioned at the top left corner (0,0) with length = 1 unit.</p>

<p>You are given a list of food&#39;s positions in row-column order. When a snake eats the food, its length and the game&#39;s score both increase by 1.</p>

<p>Each food appears one by one on the screen. For example, the second food will not appear until the first food was eaten by the snake.</p>

<p>When a food does appear on the screen, it is guaranteed that it will not appear on a block occupied by the snake.</p>

<p><b>Example:</b></p>

<pre>
Given width = 3, height = 2, and food = [[1,2],[0,1]].

Snake snake = new Snake(width, height, food);

Initially the snake appears at position (0,0) and the food at (1,2).

|S| | |
| | |F|

snake.move(&quot;R&quot;); -&gt; Returns 0

| |S| |
| | |F|

snake.move(&quot;D&quot;); -&gt; Returns 0

| | | |
| |S|F|

snake.move(&quot;R&quot;); -&gt; Returns 1 (Snake eats the first food and right after that, the second food appears at (0,1) )

| |F| |
| |S|S|

snake.move(&quot;U&quot;); -&gt; Returns 1

| |F|S|
| | |S|

snake.move(&quot;L&quot;); -&gt; Returns 2 (Snake eats the second food)

| |S|S|
| | |S|

snake.move(&quot;U&quot;); -&gt; Returns -1 (Game over because snake collides with border)
</pre>

</div>

## Tags
- Design (design)
- Queue (queue)

## Companies
- Amazon - 6 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Zillow - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Deque and HashSet design with detailed comments
- Author: xuyirui
- Creation Date: Sun Jun 19 2016 12:17:20 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 13:20:35 GMT+0800 (Singapore Standard Time)

<p>
    public class SnakeGame {
    
        //2D position info is encoded to 1D and stored as two copies 
        Set<Integer> set; // this copy is good for fast loop-up for eating body case
        Deque<Integer> body; // this copy is good for updating tail
        int score;
        int[][] food;
        int foodIndex;
        int width;
        int height;
        
        public SnakeGame(int width, int height, int[][] food) {
            this.width = width;
            this.height = height;
            this.food = food;
            set = new HashSet<>();
            set.add(0); //intially at [0][0]
            body = new LinkedList<>();
            body.offerLast(0);
        }
        
      
        public int move(String direction) {
            //case 0: game already over: do nothing
            if (score == -1) {
                return -1;
            }
            
            // compute new head
            int rowHead = body.peekFirst() / width;
            int colHead = body.peekFirst() % width;
            switch (direction) {
                case "U" : rowHead--;
                           break;
                case "D" : rowHead++;
                           break;
                case "L" : colHead--;
                           break;
                default :  colHead++;
            }
            int head = rowHead * width + colHead;
            
            //case 1: out of boundary or eating body
            set.remove(body.peekLast()); // new head is legal to be in old tail's position, remove from set temporarily 
            if (rowHead < 0 || rowHead == height || colHead < 0 || colHead == width || set.contains(head)) {
                return score = -1;
            }
            
            // add head for case2 and case3
            set.add(head); 
            body.offerFirst(head);
            
            //case2: eating food, keep tail, add head
            if (foodIndex < food.length && rowHead == food[foodIndex][0] && colHead == food[foodIndex][1]) {
                set.add(body.peekLast()); // old tail does not change, so add it back to set
                foodIndex++;
                return ++score;
            }
            
            //case3: normal move, remove tail, add head
            body.pollLast();
            return score;
            
        }
    }
</p>


### Straightforward Python solution using deque
- Author: ninjaVic
- Creation Date: Tue Jul 05 2016 09:25:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 08:04:07 GMT+0800 (Singapore Standard Time)

<p>
class SnakeGame(object):

    def __init__(self, width,height,food):
        """
        Initialize your data structure here.
        @param width - screen width
        @param height - screen height 
        @param food - A list of food positions
        E.g food = [[1,1], [1,0]] means the first food is positioned at [1,1], the second is at [1,0].
        :type width: int
        :type height: int
        :type food: List[List[int]]
        """
        self.snake = collections.deque([[0,0]])    # snake head is at the front
        self.width = width
        self.height = height
        self.food = collections.deque(food)
        self.direct = {'U': [-1, 0], 'L': [0, -1], 'R': [0, 1], 'D': [1, 0]}
        

    def move(self, direction):
        """
        Moves the snake.
        @param direction - 'U' = Up, 'L' = Left, 'R' = Right, 'D' = Down 
        @return The game's score after the move. Return -1 if game over. 
        Game over when snake crosses the screen boundary or bites its body.
        :type direction: str
        :rtype: int
        """
        newHead = [self.snake[0][0]+self.direct[direction][0], self.snake[0][1]+self.direct[direction][1]]
        
        # notice that the newHead can be equal to self.snake[-1]
        if (newHead[0] < 0 or newHead[0] >= self.height) or (newHead[1] < 0 or newHead[1] >= self.width)\
        or (newHead in self.snake and newHead != self.snake[-1]): return -1

        if self.food and self.food[0] == newHead:  # eat food
            self.snake.appendleft(newHead)   # just make the food be part of snake
            self.food.popleft()   # delete the food that's already eaten
        else:    # not eating food: append head and delete tail                 
            self.snake.appendleft(newHead)   
            self.snake.pop()   
            
        return len(self.snake)-1
</p>


### Share my easy java solution
- Author: juanren
- Creation Date: Sat Jun 04 2016 10:46:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 10:52:06 GMT+0800 (Singapore Standard Time)

<p>
public class SnakeGame {
	
	    class Position{
	        int x;
	        int y;
	        public Position(int x,int y){
	            this.x = x;
	            this.y = y;
	        }
	        public boolean isEqual(Position p){
	            return this.x==p.x && this.y == p.y ;
	        }
	    }
	    int len;
	    int rows ,cols;
	    
	    int[][] food;
	    LinkedList<Position> snake;
	   
	    /** Initialize your data structure here.
	        @param width - screen width
	        @param height - screen height 
	        @param food - A list of food positions
	        E.g food = [[1,1], [1,0]] means the first food is positioned at [1,1], the second is at [1,0]. */
	    public SnakeGame(int width, int height, int[][] food) {
	        this.rows = height;
	        this.cols = width;
	        this.food = food;
	   
	        snake = new LinkedList<Position>();
	        snake.add(new Position(0,0));
	        len = 0;
	    }
	    
	    /** Moves the snake.
	        @param direction - 'U' = Up, 'L' = Left, 'R' = Right, 'D' = Down 
	        @return The game's score after the move. Return -1 if game over. 
	        Game over when snake crosses the screen boundary or bites its body. */
	    public int move(String direction) {
	    	//if(len>=food.length) return len;
	    
	        Position cur = new Position(snake.get(0).x,snake.get(0).y);
	        
	        switch(direction){
	        case "U": 
	            cur.x--;  break;
	        case "L": 
	            cur.y--; break;
	        case "R": 
	            cur.y++;   break;
	        case "D": 
	            cur.x++;   break;
	        }
	        
	        if(cur.x<0 || cur.x>= rows || cur.y<0 || cur.y>=cols) return -1;
	        
	
	        for(int i=1;i<snake.size()-1;i++){
	            Position next = snake.get(i);
	            if(next.isEqual(cur)) return -1;	       
	        }
	        snake.addFirst(cur);     
	        if(len<food.length){
	            Position p = new Position(food[len][0],food[len][1]);	        
	            if(cur.isEqual(p)){	            
	                len++;
	            }
	        }
	        while(snake.size()>len+1) snake.removeLast();
	       
	        return len;
	    }


	/**
	 * Your SnakeGame object will be instantiated and called as such:
	 * SnakeGame obj = new SnakeGame(width, height, food);
	 * int param_1 = obj.move(direction);
	 */


}
</p>


