---
title: "Subrectangle Queries"
weight: 1345
#id: "subrectangle-queries"
---
## Description
<div class="description">
<p>Implement the class <code>SubrectangleQueries</code>&nbsp;which receives a <code>rows x cols</code> rectangle as a matrix of integers in the constructor and supports two methods:</p>

<p>1.<code>&nbsp;updateSubrectangle(int row1, int col1, int row2, int col2, int newValue)</code></p>

<ul>
	<li>Updates all values with <code>newValue</code> in the subrectangle whose upper left coordinate is <code>(row1,col1)</code> and bottom right coordinate is <code>(row2,col2)</code>.</li>
</ul>

<p>2.<code>&nbsp;getValue(int row, int col)</code></p>

<ul>
	<li>Returns the current value of the coordinate <code>(row,col)</code> from&nbsp;the rectangle.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;SubrectangleQueries&quot;,&quot;getValue&quot;,&quot;updateSubrectangle&quot;,&quot;getValue&quot;,&quot;getValue&quot;,&quot;updateSubrectangle&quot;,&quot;getValue&quot;,&quot;getValue&quot;]
[[[[1,2,1],[4,3,4],[3,2,1],[1,1,1]]],[0,2],[0,0,3,2,5],[0,2],[3,1],[3,0,3,2,10],[3,1],[0,2]]
<strong>Output</strong>
[null,1,null,5,5,null,10,5]
<strong>Explanation</strong>
SubrectangleQueries subrectangleQueries = new SubrectangleQueries([[1,2,1],[4,3,4],[3,2,1],[1,1,1]]);  
// The initial rectangle (4x3) looks like:
// 1 2 1
// 4 3 4
// 3 2 1
// 1 1 1
subrectangleQueries.getValue(0, 2); // return 1
subrectangleQueries.updateSubrectangle(0, 0, 3, 2, 5);
// After this update the rectangle looks like:
// 5 5 5
// 5 5 5
// 5 5 5
// 5 5 5 
subrectangleQueries.getValue(0, 2); // return 5
subrectangleQueries.getValue(3, 1); // return 5
subrectangleQueries.updateSubrectangle(3, 0, 3, 2, 10);
// After this update the rectangle looks like:
// 5   5   5
// 5   5   5
// 5   5   5
// 10  10  10 
subrectangleQueries.getValue(3, 1); // return 10
subrectangleQueries.getValue(0, 2); // return 5
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input</strong>
[&quot;SubrectangleQueries&quot;,&quot;getValue&quot;,&quot;updateSubrectangle&quot;,&quot;getValue&quot;,&quot;getValue&quot;,&quot;updateSubrectangle&quot;,&quot;getValue&quot;]
[[[[1,1,1],[2,2,2],[3,3,3]]],[0,0],[0,0,2,2,100],[0,0],[2,2],[1,1,2,2,20],[2,2]]
<strong>Output</strong>
[null,1,null,100,100,null,20]
<strong>Explanation</strong>
SubrectangleQueries subrectangleQueries = new SubrectangleQueries([[1,1,1],[2,2,2],[3,3,3]]);
subrectangleQueries.getValue(0, 0); // return 1
subrectangleQueries.updateSubrectangle(0, 0, 2, 2, 100);
subrectangleQueries.getValue(0, 0); // return 100
subrectangleQueries.getValue(2, 2); // return 100
subrectangleQueries.updateSubrectangle(1, 1, 2, 2, 20);
subrectangleQueries.getValue(2, 2); // return 20
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>There will be at most <code><font face="monospace">500</font></code>&nbsp;operations considering both methods:&nbsp;<code>updateSubrectangle</code> and <code>getValue</code>.</li>
	<li><code>1 &lt;= rows, cols &lt;= 100</code></li>
	<li><code>rows ==&nbsp;rectangle.length</code></li>
	<li><code>cols == rectangle[i].length</code></li>
	<li><code>0 &lt;= row1 &lt;= row2 &lt; rows</code></li>
	<li><code>0 &lt;= col1 &lt;= col2 &lt; cols</code></li>
	<li><code>1 &lt;= newValue, rectangle[i][j] &lt;= 10^9</code></li>
	<li><code>0 &lt;= row &lt; rows</code></li>
	<li><code>0 &lt;= col &lt; cols</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Nuro - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ Original and Updates
- Author: votrubac
- Creation Date: Sun Jun 14 2020 00:34:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 14 2020 00:45:07 GMT+0800 (Singapore Standard Time)

<p>
We remember the original rectangle. Then, we record all updates as coordinates and values.

When querying, we first going through updates newest to oldest. If our position is within, we return the value for that update. If there is no matching update, we use the value from the original rectangle.

```cpp
vector<vector<int>> rect, subs;
SubrectangleQueries(vector<vector<int>>& rectangle) {
    swap(rect, rectangle);
}
void updateSubrectangle(int row1, int col1, int row2, int col2, int newValue) {
    subs.push_back({row1, col1, row2, col2, newValue});
}
int getValue(int row, int col) {
    for (int i = subs.size() - 1; i >= 0; --i)
        if (subs[i][0] <= row && subs[i][1] <= col && row <= subs[i][2] && col <= subs[i][3])
            return subs[i][4];
    return rect[row][col];
}
```
</p>


### [Java/Python 3] Two methods: change input and not w/ brief analysis.
- Author: rock
- Creation Date: Sun Jun 14 2020 00:02:51 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 16 2020 12:20:56 GMT+0800 (Singapore Standard Time)

<p>

**Change input:**

```java
    private final int[][] r;
    
    public SubrectangleQueries(int[][] rectangle) {
        r = rectangle;
    }
    
    public void updateSubrectangle(int row1, int col1, int row2, int col2, int newValue) {
        for (int i = row1; i <= row2; ++i) {
            for (int j = col1; j <= col2; ++j) {
                r[i][j] = newValue;
            }
        }
    }
    
    public int getValue(int row, int col) {
        return r[row][col];
    }
```
```python

    def __init__(self, rectangle: List[List[int]]):
        self.r = rectangle        

    def updateSubrectangle(self, row1: int, col1: int, row2: int, col2: int, newValue: int) -> None:
        for i in range(row1, row2 + 1):
            for j in range(col1, col2 + 1):
                self.r[i][j] = newValue

    def getValue(self, row: int, col: int) -> int:
        return self.r[row][col]

```
**Analysis:**

For each call,

*Time:*
updateSubrectangle: O(m * n)
getValue: O(1)

*Space:*
updateSubrectangle: O(1)
getValue: O(1)

----

**Not change input:**

No change of input but keep the update history; whenever getting a value, check the most recent history; If not in history, return the original value in the matrix. 
```java
    private final int[][] r;
    private final LinkedList<int[]> histories = new LinkedList<>();
    
    public SubrectangleQueries(int[][] rectangle) { // deep copy the input array - credit to @_xavier_
        r = new int[rectangle.length][0];
        int i = 0;
        for (int[] row : rectangle) {
            r[i++] = row.clone();
        }
    }
    
    public void updateSubrectangle(int row1, int col1, int row2, int col2, int newValue) {
        histories.push(new int[]{row1, col1, row2, col2, newValue});
    }
    
    public int getValue(int row, int col) {
        for (int[] his: histories) {
            if (his[0] <= row && row <= his[2] && his[1] <= col && col <= his[3]) {
                return his[4];
            }
        }
        return r[row][col];
    }
```
```python
    def __init__(self, rectangle: List[List[int]]):
        self.r = copy.deepcopy(rectangle) # deep copy the input array - credit to @_xavier_
        self.histories = []

    def updateSubrectangle(self, row1: int, col1: int, row2: int, col2: int, newValue: int) -> None:
        self.histories.append([row1, col1, row2, col2, newValue])

    def getValue(self, row: int, col: int) -> int:
        for his in reversed(self.histories):
            if his[0] <= row <= his[2] and his[1] <= col <= his[3]:
                return his[4]
        return self.r[row][col]
```
**Analysis:**

For each call,

*Time:*
updateSubrectangle: O(1)
getValue: O(histories.size())

*Space:*
updateSubrectangle: O(1)
getValue: O(1)

----

**Comparison between the two methods:**
1. If time  O(1) is required for `getValue()`, method 1 is a better option;
2. If time  O(1) is required for ` updateSubrectangle()`, method 2 is a better option;
3. Generally speaking, if the the size of update history is greater than the updated data volume, method 1 is better; otherwise, method 2 is better.
</p>


### [C++] [easy understand] This probably should not be a medium question
- Author: wabi
- Creation Date: Sun Jun 14 2020 00:10:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 14 2020 00:26:44 GMT+0800 (Singapore Standard Time)

<p>
Data-structure desgin {accepted]

Since we update the whoel range with the value, its enough if we store the history of the updates with the boundary points along with the value. Now we can iterate backwards from the snapshots, and return the first match (within bounary of updated region). This will contain the latest value for {r,c}. If nothing matches, then we can return from the base rectange. 
```
    vector<vector<int>> snapshots;
    vector<vector<int>> base;
    
    SubrectangleQueries(vector<vector<int>>& rectangle) {
        base = rectangle;
    }
    
    void updateSubrectangle(int row1, int col1, int row2, int col2, int newValue) {
        vector<int> snap = {row1, col1, row2, col2, newValue};
        snapshots.push_back(snap);
    }
    
    int getValue(int row, int col) {
        int n = snapshots.size();
        for (int i = n - 1; i >= 0; i --) {
            if (row >= snapshots[i][0] && row <= snapshots[i][2] 
                && col >= snapshots[i][1] && col <= snapshots[i][3]) return snapshots[i][4];
        }
        return base[row][col];
    }
```

Brute-force [accepted]

```
    vector<vector<int>> rec;
    
    SubrectangleQueries(vector<vector<int>>& rectangle) {
        rec = rectangle;
    }
    
    void updateSubrectangle(int row1, int col1, int row2, int col2, int newValue) {
        for (int r = row1; r <= row2; r ++) {
            for (int c = col1; c <= col2; c ++) {
                rec[r][c] = newValue;
            }
        }
    }
    
    int getValue(int row, int col) {
        return rec[row][col];
    }
	```
</p>


