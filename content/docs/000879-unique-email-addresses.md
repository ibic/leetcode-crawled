---
title: "Unique Email Addresses"
weight: 879
#id: "unique-email-addresses"
---
## Description
<div class="description">
<p>Every email consists of a local name and a domain name, separated by the @ sign.</p>

<p>For example, in <code>alice@leetcode.com</code>,&nbsp;<code>alice</code> is the local name, and <code>leetcode.com</code> is the domain name.</p>

<p>Besides lowercase letters, these emails may contain <code>&#39;.&#39;</code>s or <code>&#39;+&#39;</code>s.</p>

<p>If you add periods (<code>&#39;.&#39;</code>) between some characters in the <strong>local name</strong> part of an email address, mail sent there will be forwarded to the same address without dots in the local name.&nbsp; For example, <code>&quot;alice.z@leetcode.com&quot;</code> and <code>&quot;alicez@leetcode.com&quot;</code> forward to the same email address.&nbsp; (Note that this rule does not apply for domain names.)</p>

<p>If you add a plus (<code>&#39;+&#39;</code>) in the <strong>local name</strong>, everything after the first plus sign will be&nbsp;<strong>ignored</strong>. This allows certain emails to be filtered, for example&nbsp;<code>m.y+name@email.com</code>&nbsp;will be forwarded to&nbsp;<code>my@email.com</code>.&nbsp; (Again, this rule does not apply for domain names.)</p>

<p>It is possible to use both of these rules at the same time.</p>

<p>Given a list of <code>emails</code>, we send one email to each address in the list.&nbsp;&nbsp;How many different addresses actually receive mails?&nbsp;</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;test.email+alex@leetcode.com&quot;,&quot;test.e.mail+bob.cathy@leetcode.com&quot;,&quot;testemail+david@lee.tcode.com&quot;]</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong><span>Explanation:</span></strong><span>&nbsp;&quot;</span><span id="example-input-1-1">testemail@leetcode.com&quot; and &quot;testemail@lee.tcode.com&quot; </span>actually receive mails
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>1 &lt;= emails[i].length&nbsp;&lt;= 100</code></li>
	<li><code>1 &lt;= emails.length &lt;= 100</code></li>
	<li>Each <code>emails[i]</code> contains exactly one <code>&#39;@&#39;</code> character.</li>
	<li>All local and domain names are non-empty.</li>
	<li>Local names do not start with a <code>&#39;+&#39;</code> character.</li>
</ul>
</div>

</div>

## Tags
- String (string)

## Companies
- Google - 276 (taggedByAdmin: true)
- Amazon - 8 (taggedByAdmin: false)
- Adobe - 5 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Canonical Form

**Intuition and Algorithm**

For each email address, convert it to the *canonical* address that actually receives the mail.  This involves a few steps:

* Separate the email address into a `local` part and the `rest` of the address.

* If the `local` part has a `'+'` character, remove it and everything beyond it from the `local` part.

* Remove all the zeros from the `local` part.

* The canonical address is `local + rest`.

After, we can count the number of unique canonical addresses with a `Set` structure.

<iframe src="https://leetcode.com/playground/jxonoHQ9/shared" frameBorder="0" width="100%" height="395" name="jxonoHQ9"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\mathcal{C})$$, where $$\mathcal{C}$$ is the total content of `emails`.
<br/>
* Space Complexity:  $$O(\mathcal{C})$$.
<br />
<br />

## Accepted Submission (python3)
```python3
class Solution:
    def numUniqueEmails(self, emails: List[str]) -> int:
        def normalizeEmails(email: str) -> str:
            name, domain = email.split('@')
            ename = name.split('+')[0]
            ename = ename.replace('.', '')
            return ename + '@' + domain

        nemails = set(map(lambda s: normalizeEmails(s), emails))
        print(nemails)
        return len(nemails)
```

## Top Discussions
### [Java/Python 3] 7 and 6 liners with comment and analysis.
- Author: rock
- Creation Date: Sun Oct 28 2018 14:35:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 20 2020 20:16:52 GMT+0800 (Singapore Standard Time)

<p>
```java
    public int numUniqueEmails(String[] emails) {
        Set<String> normalized = new HashSet<>(); // used to save simplified email address, cost O(n) sapce.
        for (String email : emails) {
            String[] parts = email.split("@"); // split into local and domain parts.
            String[] local = parts[0].split("\\+"); // split local by \'+\'.
            normalized.add(local[0].replace(".", "") + "@" + parts[1]); // remove all \'.\', and concatenate \'@\' and domain.        
        }
        return normalized.size();
    }
```
```python
    def numUniqueEmails(self, emails: List[str]) -> int:
        seen = set()
        for email in emails:
            name, domain = email.split(\'@\') 
            local = name.split(\'+\')[0].replace(\'.\', \'\')
            seen.add(local + \'@\' + domain)
        return len(seen)
```
**Analysis**
Let `n` be the total characters in the input array `emails`. The HashSet `normalized` and the for loop both cost `O(n)`, in terms of space and time, respectively. 

**Time & space: O(n).**
# --------------------

**Q & A**

**Q1**: What is Java metacharacter?
**A1**: A metacharacter \u2014 a character with special meaning interpreted by the Java regular expression engine / matcher.
https://en.wikipedia.org/wiki/Metacharacter
https://docs.oracle.com/javase/tutorial/essential/regex/literals.html. 

**Q2**: Why does Java regular expression use `\\`, instead of `\`, to escape metacharacter such as `+, ., *`, etc?

**A2**: I guess the reason is that the backslash character is an escape character in Java String literals already. 

**Update:**

Credit to @helengu1996 for her [link](https://stackoverflow.com/a/25145826/4150692):

*"There are two "escapings" going on here. The first backslash is to escape the second backslash for the Java language, to create an actual backslash character. The backslash character is what escapes the + or the s for interpretation by the regular expression engine. That\'s why you need two backslashes -- one for Java, one for the regular expression engine. With only one backslash, Java reports \s and \+ as illegal escape characters -- not for regular expressions, but for an actual character in the Java language."*

----

Similar solution:

[1592. Rearrange Spaces Between Words](https://leetcode.com/problems/rearrange-spaces-between-words/discuss/855335/JavaPython-3-6-liners.)
</p>


### [C++] Concise Solution
- Author: KasraGhodsi
- Creation Date: Sat Jun 22 2019 23:13:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 23 2019 03:34:43 GMT+0800 (Singapore Standard Time)

<p>
For each email address in `emails`, clean up the local name, append the domain name, and add the cleaned up email to a set.

Return the size of the set.
 
	int numUniqueEmails(vector<string>& emails) {
        unordered_set<string> st;
        for(string &email : emails) {
            string cleanEmail;
            for(char c : email) {
                if(c == \'+\' || c == \'@\') break;
                if(c == \'.\') continue;
                cleanEmail += c;
            }
            cleanEmail += email.substr(email.find(\'@\'));
            st.insert(cleanEmail);
        }
        return st.size();
    }
</p>


### python solution
- Author: dchen53
- Creation Date: Tue Oct 30 2018 10:09:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 30 2018 10:09:00 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def numUniqueEmails(self, emails):
        """
        :type emails: List[str]
        :rtype: int
        """
        email_set = set()
        for email in emails:
            local_name,domain_name = email.split("@")
            local_name ="".join(local_name.split('+')[0].split('.'))
            email = local_name +'@' + domain_name
            email_set.add(email)
        return len(email_set)
 ```       
</p>


