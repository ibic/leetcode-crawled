---
title: "Expressive Words"
weight: 748
#id: "expressive-words"
---
## Description
<div class="description">
<p>Sometimes people repeat letters to represent extra feeling, such as &quot;hello&quot; -&gt; &quot;heeellooo&quot;, &quot;hi&quot; -&gt; &quot;hiiii&quot;.&nbsp; In these strings like &quot;heeellooo&quot;, we have <em>groups</em> of adjacent letters that are all the same:&nbsp; &quot;h&quot;, &quot;eee&quot;, &quot;ll&quot;, &quot;ooo&quot;.</p>

<p>For some given string <code>S</code>, a query word is <em>stretchy</em> if it can be made to be equal to <code>S</code> by any&nbsp;number of&nbsp;applications of the following <em>extension</em> operation: choose a group consisting of&nbsp;characters <code>c</code>, and add some number of characters <code>c</code> to the group so that the size of the group is 3 or more.</p>

<p>For example, starting with &quot;hello&quot;, we could do an extension on the group &quot;o&quot; to get &quot;hellooo&quot;, but we cannot get &quot;helloo&quot; since the group &quot;oo&quot; has size less than 3.&nbsp; Also, we could do another extension like &quot;ll&quot; -&gt; &quot;lllll&quot; to get &quot;helllllooo&quot;.&nbsp; If <code>S = &quot;helllllooo&quot;</code>, then the query word &quot;hello&quot; would be stretchy because of these two extension operations:&nbsp;<code>query = &quot;hello&quot; -&gt; &quot;hellooo&quot; -&gt;&nbsp;&quot;helllllooo&quot; = S</code>.</p>

<p>Given a list of query words, return the number of words that are stretchy.&nbsp;</p>

<p>&nbsp;</p>

<pre>
<strong>Example:</strong>
<strong>Input:</strong> 
S = &quot;heeellooo&quot;
words = [&quot;hello&quot;, &quot;hi&quot;, &quot;helo&quot;]
<strong>Output:</strong> 1
<strong>Explanation:</strong> 
We can extend &quot;e&quot; and &quot;o&quot; in the word &quot;hello&quot; to get &quot;heeellooo&quot;.
We can&#39;t extend &quot;helo&quot; to get &quot;heeellooo&quot; because the group &quot;ll&quot; is not size 3 or more.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= len(S) &lt;= 100</code>.</li>
	<li><code>0 &lt;= len(words) &lt;= 100</code>.</li>
	<li><code>0 &lt;= len(words[i]) &lt;= 100</code>.</li>
	<li><code>S</code> and all words in <code>words</code>&nbsp;consist only of&nbsp;lowercase letters</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Google - 18 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Run Length Encoding 

**Intuition**

For some word, write the head character of every group, and the count of that group.  For example, for `"abbcccddddaaaaa"`, we'll write the "key" of `"abcda"`, and the "count" `[1,2,3,4,5]`.

Let's see if a `word` is stretchy.  Evidently, it needs to have the same key as `S`.

Now, let's say we have individual counts `c1 = S.count[i]` and `c2 = word.count[i]`.

* If `c1 < c2`, then we can't make the `i`th group of `word` equal to the `i`th word of `S` by adding characters.

* If `c1 >= 3`, then we can add letters to the `i`th group of `word` to match the `i`th group of `S`, as the latter is *extended*.

* Else, if `c1 < 3`, then we must have `c2 == c1` for the `i`th groups of `word` and `S` to match.

<iframe src="https://leetcode.com/playground/FAsRV5hm/shared" frameBorder="0" width="100%" height="500" name="FAsRV5hm"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(QK)$$, where $$Q$$ is the length of `words` (at least 1), and $$K$$ is the maximum length of a word.

* Space Complexity: $$O(K)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution using Two Pointers with Detailed Explanation
- Author: myCafeBabe
- Creation Date: Sun Apr 01 2018 11:20:34 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 23 2020 15:47:58 GMT+0800 (Singapore Standard Time)

<p>
We have two pointers, use `i` to scan `S`, and use `j` to scan each `word` in `words`.
Firstly, for `S` and `word`, we can calculate the length of the susbtrings which contains the repeated letters of the letter currently pointed by the two pointers, and get `len1` and `len2`.

The two letters currently pointed by the two pointers must be equal, otherwise the `word` is not stretchy, we return false. Then, if we find that `len1` is smaller than 3, it means the letter cannot be extended, so `len1` must equals to `len2`, otherwise this `word` is not stretchy. In the other case, if `len1` equals to or larger than 3, we must have `len1` equals to or larger than `len2`, otherwise there are not enough letters in `S` to match the letters in `word`.

Finally, if the `word` is stretchy, we need to guarantee that both of the two pointers has scanned the whole string.
```
class Solution {
    public int expressiveWords(String S, String[] words) {
        if (S == null || words == null) {
            return 0;
        }
        int count = 0;
        for (String word : words) {
            if (stretchy(S, word)) {
                count++;
            }
        }
        return count;
    }
    
    public boolean stretchy(String S, String word) {
        if (word == null) {
            return false;
        }
        int i = 0;
        int j = 0;
        while (i < S.length() && j < word.length()) {
            if (S.charAt(i) == word.charAt(j)) {
                int len1 = getRepeatedLength(S, i);
                int len2 = getRepeatedLength(word, j);
                if (len1 < 3 && len1 != len2 || len1 >= 3 && len1 < len2) {
                    return false;
                }
                i += len1;
                j += len2;
            } else {
                return false;
            }
        }
        return i == S.length() && j == word.length();
    }
    
    public int getRepeatedLength(String str, int i) {
        int j = i;
        while (j < str.length() && str.charAt(j) == str.charAt(i)) {
            j++;
        }
        return j - i;
    }
}
```
</p>


### [C++/Java/Python] 2 Pointers and 4 pointers
- Author: lee215
- Creation Date: Sun Apr 08 2018 02:45:07 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 14:06:35 GMT+0800 (Singapore Standard Time)

<p>
Loop through all words. ```check(string S, string W)``` checks if ```W``` is stretchy to ```S```.

In `check` function, use two pointer:
1. If `S[i] == W[j`, `i++, j++`
2. If `S[i - 2] == S[i - 1] == S[i]` or `S[i - 1] == S[i] == S[i + 1]`, `i++`
3. return false


**C++**:
```
    int expressiveWords(string S, vector<string>& words) {
        int res = 0;
        for (auto &W : words) if (check(S, W)) res++;
        return res;
    }

    bool check(string S, string W) {
        int n = S.size(), m = W.size(), j = 0;
        for (int i = 0; i < n; i++)
            if (j < m && S[i] == W[j]) j++;
            else if (i > 1 && S[i - 2] == S[i - 1] && S[i - 1] == S[i]);
            else if (0 < i && i < n - 1 && S[i - 1] == S[i] && S[i] == S[i + 1]);
            else return false;
        return j == m;
    }
```
**Java**:
```
    public int expressiveWords(String S, String[] words) {
        int res = 0;
        for (String W : words) if (check(S, W)) res++;
        return res;
    }
    public boolean check(String S, String W) {
        int n = S.length(), m = W.length(), j = 0;
        for (int i = 0; i < n; i++)
            if (j < m && S.charAt(i) == W.charAt(j)) j++;
            else if (i > 1 && S.charAt(i) == S.charAt(i - 1) && S.charAt(i - 1) == S.charAt(i - 2));
            else if (0 < i && i < n - 1 && S.charAt(i - 1) == S.charAt(i) && S.charAt(i) == S.charAt(i + 1));
            else return false;
        return j == m;
    }
```
**Python**:
```
    def expressiveWords(self, S, words):
        return sum(self.check(S, W) for W in words)

    def check(self, S, W):
        i, j, n, m = 0, 0, len(S), len(W)
        for i in range(n):
            if j < m and S[i] == W[j]: j += 1
            elif S[i - 1:i + 2] != S[i] * 3 != S[i - 2:i + 1]: return False
        return j == m
```

Another approach use 4 pointers, but will be much easier to undersand and debug.

**C++:**
```
    bool check(string S, string W) {
        int n = S.size(), m = W.size(), i = 0, j = 0;
        for (int i2 = 0, j2 = 0; i < n && j < m; i = i2, j = j2) {
            if (S[i] != W[j]) return false;
            while (i2 < n && S[i2] == S[i]) i2++;
            while (j2 < m && W[j2] == W[j]) j2++;
            if (i2 - i != j2 - j && i2 - i < max(3, j2 - j)) return false;
        }
        return i == n && j == m;
    }
```

**Java:**
```
    public boolean check(String S, String W) {
        int n = S.length(), m = W.length(), i = 0, j = 0;
        for (int i2 = 0, j2 = 0; i < n && j < m; i = i2, j = j2) {
            if (S.charAt(i) != W.charAt(j)) return false;
            while (i2 < n && S.charAt(i2) == S.charAt(i)) i2++;
            while (j2 < m && W.charAt(j2) == W.charAt(j)) j2++;
            if (i2 - i != j2 - j && i2 - i < Math.max(3, j2 - j)) return false;
        }
        return i == n && j == m;
    }
```
**Python:**
```
    def check(self, S, W):
        i, j, i2, j2, n, m = 0, 0, 0, 0, len(S), len(W)
        while i < n and j < m:
            if S[i] != W[j]: return False
            while i2 < n and S[i2] == S[i]: i2 += 1
            while j2 < m and W[j2] == W[j]: j2 += 1
            if i2 - i != j2 - j and i2 - i < max(3, j2 - j): return False
            i, j = i2, j2
        return i == n and j == m
```

**Update 2018-06-05**:
I updated all solution and add another approach, hope it helps.
The first one is similar to @mzchen\'s solution. 
The second one will be more straight forward.

</p>


### Problem setter should work harder on his English.
- Author: Wizmann
- Creation Date: Tue Apr 03 2018 16:52:08 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 03 2018 14:54:34 GMT+0800 (Singapore Standard Time)

<p>
I read it for about 10 times, try my best, but fail to understand the real meaning of the problem.

We are working on algorithm problems, not the puzzles.

Thanks a lot.
</p>


