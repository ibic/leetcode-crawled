---
title: "Flipping an Image"
weight: 775
#id: "flipping-an-image"
---
## Description
<div class="description">
<p>Given a binary matrix <code>A</code>, we want to flip the image horizontally, then invert it, and return the resulting image.</p>

<p>To flip an image horizontally means that each row of the image is reversed.&nbsp; For example, flipping&nbsp;<code>[1, 1, 0]</code>&nbsp;horizontally results in&nbsp;<code>[0, 1, 1]</code>.</p>

<p>To invert an image means&nbsp;that each <code>0</code> is replaced by <code>1</code>, and each <code>1</code> is replaced by <code>0</code>.&nbsp;For example, inverting&nbsp;<code>[0, 1, 1]</code>&nbsp;results in&nbsp;<code>[1, 0, 0]</code>.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>[[1,1,0],[1,0,1],[0,0,0]]
<strong>Output: </strong>[[1,0,0],[0,1,0],[1,1,1]]
<strong>Explanation:</strong> First reverse each row: [[0,1,1],[1,0,1],[0,0,0]].
Then, invert the image: [[1,0,0],[0,1,0],[1,1,1]]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>[[1,1,0,0],[1,0,0,1],[0,1,1,1],[1,0,1,0]]
<strong>Output: </strong>[[1,1,0,0],[0,1,1,0],[0,0,0,1],[1,0,1,0]]
<strong>Explanation:</strong> First reverse each row: [[0,0,1,1],[1,0,0,1],[1,1,1,0],[0,1,0,1]].
Then invert the image: [[1,1,0,0],[0,1,1,0],[0,0,0,1],[1,0,1,0]]
</pre>

<p><strong>Notes:</strong></p>

<ul>
	<li><code>1 &lt;= A.length = A[0].length &lt;= 20</code></li>
	<li><code>0 &lt;= A[i][j]<font face="sans-serif, Arial, Verdana, Trebuchet MS">&nbsp;&lt;=&nbsp;</font>1</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Microsoft - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Direct [Accepted]

**Intuition and Algorithm**

We can do this in place.  In each row, the `i`th value from the left is equal to the inverse of the `i`th value from the right.

We use `(C+1) / 2` (with floor division) to iterate over all indexes `i` in the first half of the row, including the center.

<iframe src="https://leetcode.com/playground/z4eHNwcY/shared" frameBorder="0" width="100%" height="276" name="z4eHNwcY"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where `N` is the total number of elements in `A`.

* Space Complexity: $$O(1)$$ in *additional* space complexity.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Reverse and Toggle
- Author: lee215
- Creation Date: Sun May 13 2018 11:09:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Feb 08 2020 01:34:25 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**:
1. reverse every row.
2. toggle every value.

In Java, I did both steps together:
Compare the `i` th and `n - i - 1` th in a row.
The "trick" is that if the values are not the same,
but you swap and flip, nothing will change.
So if they are same, we toggle both, otherwise we do nothing.
<br>

## **Complexity**:
Time `O(N^2)`
Space `O(N^2)` for output
<br>

**C++:**
```cpp
    vector<vector<int>> flipAndInvertImage(vector<vector<int>>& A) {
        for (auto& row : A) reverse(row.begin(), row.end());
        for (auto& row : A) for (int& i: row) i ^= 1;
        return A;
    }
```

**Java:**
```java
    public int[][] flipAndInvertImage(int[][] A) {
        int n = A.length;
        for (int[] row : A)
            for (int i = 0; i * 2 < n; i++)
                if (row[i] == row[n - i - 1])
                    row[i] = row[n - i - 1] ^= 1;
        return A;
    }
```
**1-line Python:**
```python
    def flipAndInvertImage(self, A):
        return [[1 ^ i for i in reversed(row)] for row in A]
```

</p>


### Python 1 line
- Author: Nishil
- Creation Date: Sat May 19 2018 00:18:54 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 22:08:47 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def flipAndInvertImage(self, A):
        """
        :type A: List[List[int]]
        :rtype: List[List[int]]
        """
        
        return [[1-i for i in row[::-1]] for row in A]
```
</p>


### Easy Understand One pass Java Solution absolutely beat 100%
- Author: FLAGbigoffer
- Creation Date: Wed Jul 11 2018 13:01:59 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 11 2018 13:01:59 GMT+0800 (Singapore Standard Time)

<p>
The idea is simple. For each row, use two pointers. One is going forward and the other is going backward. 
(1). If the two elements are the same, then make a slight change like this 0 -> 1 or 1 -> 0. 
(2). If the two elements are different, DON\'T do anything. Just let it go.

Only scan the whole matrix once.

```
class Solution {
    public int[][] flipAndInvertImage(int[][] A) {
        for (int i = 0; i < A.length; i++) {
            int lo = 0, hi = A[0].length - 1;
            while (lo <= hi) {
                if (A[i][lo] == A[i][hi]) {
                    A[i][lo] = 1 - A[i][lo];
                    A[i][hi] = A[i][lo];
                }
                lo++;
                hi--;
            }
        }
        
        return A;
    }
}
````
</p>


