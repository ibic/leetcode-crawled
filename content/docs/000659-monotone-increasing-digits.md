---
title: "Monotone Increasing Digits"
weight: 659
#id: "monotone-increasing-digits"
---
## Description
<div class="description">
<p>
Given a non-negative integer <code>N</code>, find the largest number that is less than or equal to <code>N</code> with monotone increasing digits.
</p><p>
(Recall that an integer has <i>monotone increasing digits</i> if and only if each pair of adjacent digits <code>x</code> and <code>y</code> satisfy <code>x <= y</code>.)
</p><p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> N = 10
<b>Output:</b> 9
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> N = 1234
<b>Output:</b> 1234
</pre>
</p>

<p><b>Example 3:</b><br />
<pre>
<b>Input:</b> N = 332
<b>Output:</b> 299
</pre>
</p>

<p><b>Note:</b>
<code>N</code> is an integer in the range <code>[0, 10^9]</code>.
</p>
</div>

## Tags
- Greedy (greedy)

## Companies
- SAP - 8 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Greedy [Accepted]

**Intuition**

Let's construct the answer digit by digit.

If the current answer is say, `123`, and the next digit is `5`, then the answer must be at least `123555...5`, since the digits in the answer must be monotonically increasing.  If this is larger than `N`, then it's impossible.

**Algorithm**

For each digit of `N`, let's build the next digit of our answer `ans`.  We'll find the smallest possible digit `d` such that `ans + (d repeating) > N` when comparing by string; that means `d-1` must have satisfied `ans + (d-1 repeating) <= N`, and so we'll add `d-1` to our answer.  If we don't find such a digit, we can add a `9` instead.

<iframe src="https://leetcode.com/playground/XMdFqNyG/shared" frameBorder="0" width="100%" height="429" name="XMdFqNyG"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(D^2)$$, where $$D \approx \log N$$ is the number of digits in $$N$$.  We do $$O(D)$$ work building and comparing each candidate, and we do this $$O(D)$$ times.

* Space Complexity: $$O(D)$$, the size of the answer and the temporary string we are comparing.

---
#### Approach #2: Truncate After Cliff [Accepted]

**Intuition**

One initial thought that comes to mind is we can always have a candidate answer of `d999...9` (a digit `0 <= d <= 9` followed by some number of nines.)  For example if `N = 432543654`, we could always have an answer of at least `399999999`.

We can do better.  For example, when the number is `123454321`, we could have a candidate of `123449999`.  It seems like a decent strategy is to take a monotone increasing prefix of `N`, then decrease the number before the "cliff" (the index where adjacent digits decrease for the first time) if it exists, and replace the rest of the characters with `9`s.

When does that strategy fail?  If `N = 333222`, then our strategy would give us the candidate answer of `332999` - but this isn't monotone increasing.  However, since we are looking at all indexes before the original first occurrence of a cliff, the only place where a cliff could exist, is next to where we just decremented a digit.

Thus, we can repair our strategy, by successfully morphing our answer `332999 -> 329999 -> 299999` with a linear scan.

**Algorithm**

We'll find the first cliff `S[i-1] > S[i]`.  Then, while the cliff exists, we'll decrement the appropriate digit and move `i` back.  Finally, we'll make the rest of the digits `9`s and return our work.

We can prove our algorithm is correct because every time we encounter a cliff, the digit we decrement has to decrease by at least 1.  Then, the largest possible selection for the rest of the digits is all nines, which is always going to be monotone increasing with respect to the other digits occurring earlier in the number.

<iframe src="https://leetcode.com/playground/t4uVHAHr/shared" frameBorder="0" width="100%" height="242" name="t4uVHAHr"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(D)$$, where $$D \approx \log N$$ is the number of digits in $$N$$.  Each step in the algorithm is a linear scan of the digits.

* Space Complexity: $$O(D)$$, the size of the answer.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple and very short C++ solution
- Author: Zee
- Creation Date: Sun Dec 03 2017 12:22:26 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 17 2018 03:01:56 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    int monotoneIncreasingDigits(int N) {
        string n_str = to_string(N);
        
        int marker = n_str.size();
        for(int i = n_str.size()-1; i > 0; i --) {
            if(n_str[i] < n_str[i-1]) {
                marker = i;
                n_str[i-1] = n_str[i-1]-1;
            }
        }
        
        for(int i = marker; i < n_str.size(); i ++) n_str[i] = '9';
        
        return stoi(n_str);
    }
};
```
</p>


### Simple Python solution w/ Explanation
- Author: li-_-il
- Creation Date: Sun Dec 03 2017 12:04:22 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 12 2018 15:23:11 GMT+0800 (Singapore Standard Time)

<p>
The idea is to go from the LSB to MSB and find the last digit, where an inversion happens.
There are 2 cases to consider:

case 1:
In 14267 , we see that inversion happens at 4. In this case, then answer is obtained by reducing 4 to 3, and changing all the following digits to 9. 
=> 13999

case 2:
1444267, here eventhough the last inversion happens at the last 4 in 1444, if we reduce it to 3, then that itself breaks the rule. So once we find the last digit where inversion happens,  if that digit is repeated, then we have to find the last position of that digit. After that it is same as case1, where we reduce it by 1 and set the remaining digits to 9.
=> 1399999

The steps are:
1) Convert n into num array in reverse order
2) Find the leftmost position that is inverted and if the inverted character repeats itself, find the leftmost repeated digit. 
3) Fill the digits after inversion as 9
4) Reduce the digit that caused the inversion by -1
5) Reverse back the num array and convert to int
```
def monotoneIncreasingDigits(self, N):
        if N < 10: return N
        n, inv_index = N, -1
        num = [int(d) for d in str(n)[::-1]] 

        for i in range(1, len(num)): 
            if num[i] > num[i - 1] or (inv_index != -1 and num[inv_index] == num[i]):
                inv_index = i

        if inv_index == -1: return N

        for i in range(inv_index): num[i] = 9
        num[inv_index] -= 1
        
        return int(''.join([ str(i) for i in num[::-1]])) 
```
</p>


### Clean Code
- Author: GraceMeng
- Creation Date: Tue Jul 17 2018 15:00:20 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 21 2020 23:33:16 GMT+0800 (Singapore Standard Time)

<p>
The result should be:
```
preceding monotone increasing digits (ending digit should decrease by 1) 
```
followed by 
```
all remaining digits set to \'9\'
```
.

`e.g. N = 12321, the result is 12299`.
```
monotoneIncreasingEnd is finalized as : 2
current arrN : 12211
arrN is finalized as : 12299
```
****
```
class Solution {
    public int monotoneIncreasingDigits(int N) {
        char[] arrN = String.valueOf(N).toCharArray();
        
        int monotoneIncreasingEnd = arrN.length - 1;
        for (int i = arrN.length - 1; i > 0; i--) {
            if (arrN[i] < arrN[i - 1]) {
                monotoneIncreasingEnd = i - 1;
                arrN[i - 1]--;
            }
        }
        // System.out.println("monotoneIncreasingEnd is finalized as : " + monotoneIncreasingEnd);
        // System.out.println("current arrN : " + new String(arrN));
        for (int i = monotoneIncreasingEnd + 1; i < arrN.length; i++) {
            arrN[i] = \'9\';
        }
        // System.out.println("arrN is finalized as : " + new String(arrN));
        return Integer.parseInt(new String(arrN));
    }
}
```

</p>


