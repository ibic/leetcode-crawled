---
title: "Airplane Seat Assignment Probability"
weight: 1189
#id: "airplane-seat-assignment-probability"
---
## Description
<div class="description">
<p><code data-stringify-type="code">n</code>&nbsp;passengers board an airplane with exactly&nbsp;<code data-stringify-type="code">n</code>&nbsp;seats. The first passenger has lost the ticket and picks a seat randomly. But after that, the rest of passengers will:</p>

<ul>
	<li>Take their own seat if it is still available,&nbsp;</li>
	<li>Pick other seats randomly when they find their seat occupied&nbsp;</li>
</ul>

<p>What is the probability that the n-th person can get his own seat?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> 1.00000
<strong>Explanation: </strong>The first person can only get the first seat.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2
<strong>Output:</strong> 0.50000
<strong>Explanation: </strong>The second person has a probability of 0.5 to get the second seat (when first person gets the first seat).
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
</ul>
</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)
- Brainteaser (brainteaser)

## Companies
- Uber - 2 (taggedByAdmin: false)
- Microstrategy - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### It's not obvious to me at all. Foolproof explanation here!!! And proof for why it's 1/2
- Author: winterock
- Creation Date: Thu Oct 24 2019 03:29:30 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 29 2019 17:04:36 GMT+0800 (Singapore Standard Time)

<p>
Everyone is saying, it\'s obvious
f(n) = 1 / n + (n - 2) / n * f(n - 1)

What? Not to me at all.

Here is my explanation,
```
f(n) = 1/n                                    -> 1st person picks his own seat
    + 1/n * 0                                 -> 1st person picks last one\'s seat
	+ (n-2)/n * (                            ->1st person picks one of seat from 2nd to (n-1)th
        1/(n-2) * f(n-1)                   -> 1st person pick 2nd\'s seat
        1/(n-2) * f(n-2)                  -> 1st person pick 3rd\'s seat
        ......
        1/(n-2) * f(2)                     -> 1st person pick (n-1)th\'s seat
	)
	
=> f(n) = 1/n * ( f(n-1) + f(n-2) + f(n-3) + ... + f(1) )

Now, you can easily get
f(1) = 1
f(2) = 1/2
f(3) = 1/2
...
```

A bit more explanation,
If the 1st one picks i\'s seat, we know that
* Anyone between 1st and ith, gets their own seats
 
Then, when we come to person i, it becomes a sub-problem
* i takes a random seat
* the rest follow the same rule
* from i to last (inclusive), there are n - (i-1) seats left
* so it\'s a f(n-i+1)


**Proof for why it\'s 1/2 when n > 2**
From the proof above we know that
```
when n > 2,
f(n) = 1/n * ( f(n-1) + f(n-2) + ... + f(1) )
f(n-1) = 1/(n-1) * (f(n-2) + f(n-3) + ... + f(1))

because, the 2nd equation requires n-1 > 1
```
So that
```
n * f(n) = f(n-1) + f(n-2) + f(n-3) + ... + f(1)
(n-1) * f(n-1) = f(n-2) + f(n-3) + ... + f(1)
```
Substract both sides, we get
```
n * f(n) - (n-1)*f(n-1) = f(n-1)

=> n * f(n) = n * f(n-1)
=> f(n) = f(n-1) , when n > 2
```

Then we know, 
f(1) =1
f(2) = 1/2
From here
f(n) = f(n-1) = ... = f(2) = 1/2

</p>


### Proof by mathematical induction that answer is 1/2 when n >= 2.
- Author: rock
- Creation Date: Fri Oct 18 2019 22:13:22 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 07 2020 13:13:33 GMT+0800 (Singapore Standard Time)

<p>
**Q & A**
Q1:  Say if there are `n` passengers and the first passenger took the `3rd` seat. Now, like you explained, there are `n - 1` passengers and `n - 1` seats left. But when the `2nd` passenger comes in, he *doesn\'t* have `2` options to make it possible for the `nth` passenger to take the `nth` seat. Instead, he only has one option, which is to take the `2nd` seat because it is not occupied by the first passenger. I don\'t see how that is the case of a subproblem of `(n - 1)`. Could you shed some light please, thanks!
A1: For any case, we can get rid of those sitting on own seats (except the first passenger) and get a problem of `n\'` (= `n - k`, where `k` is the number of passengers sitting on own seats), then re-number (without changing the relative order) them as passenger `1, 2, ..., n\'`, hence the result is in same form, the only difference is to change `n` to `n\'`.
Except `n\' = 1`, results for `n\'` of other values are independent on `n\'`. In short, changing from `n` to `n\'` will not influence the result.

Q2: What would have happened if we didn\'t knew which one of them lost the ticket?
A2: If last one lost ticket, the Prob. is `1`; otherwise `0.5`.
Proof:
Assume there are `n\'` passengers totally, and the `(k + 1)th` passenger lost his ticket. First `k` ones definitely seat on their own seats Let `n = n\' - k`, we get a problem of `n`, which is exactly same as the original problem.

----


#  Part 1: [Java] 2 liners w/ explanation and analysis.
For any case, we can get rid of those sitting on own seats (except the first passenger) and get a problem of n\' (= n - k, where k is the number of passengers sitting on own seats), then re-number (without changing the relative order) them as passenger 1, 2, ..., n\', hence the result is in same form, the only difference is to change n to n\'.

Except n\' = 1, results for n\' of other values are independent on n\'. In short, changing from n to n\' will not influence the result.

For the `1st` passenger, there are 2 cases that the `nth` passenger could take the right seat:
`1st` passenger
1. Take his own seat, the probability is `1 / n`;
2. Take a seat neither his own nor the one of the `nth` passenger, and the `corresponding` probability is `(n - 2) / n`; In addition, other passengers (except the `nth` one) should not occupy the `nth` seat;
Now there are `n - 1` passengers and `n - 1` seats remaining, and the `2nd` passenger, like the `1st` one, have `2` options to make it possible the `nth` passenger take the right seat: 
	a) take the `1st` passenger\'s seat, the probability is `1 / (n - 1)`; 
	b) Take a seat that is neither the `1st` passenger\'s nor the `nth` passenger\'s, and the `corresponding` probability is `((n -  1) - 2) /( n - 1)`; 
Obviouly, we recurse to subproblem of `(n - 1)` .

Combined the above `2` cases, we have the following code:
```
    public double nthPersonGetsNthSeat(int n) {
        if (n == 1) return 1.0d;
        return 1d / n + (n - 2d) / n * nthPersonGetsNthSeat(n - 1);
    }
```
**Analysis**
Time: O(n), space: O(1).

----

Based on the code in part 1, we have the following formula:
```
f(n) = 1 / n + (n - 2) / n * f(n - 1)
```

# Part2: Proof when n > 1, the f(n) is 1/2

1. `n = 2`, we have `f(2) = 1/2`; the assumption holds;
2. Suppose `n = k` we have `f(k) = 1/2`, when `n = k + 1`, 
```
f(k + 1) = 1 / (k + 1) + (k + 1 - 2) / (k + 1) * f(k)
         = 2 / (2 * (k + 1)) + (k - 1) / (k + 1) * 1/2
         = 1 / 2
```
That is, `f(k + 1) = 1 / 2` also holds.

From above 1 and 2, we complete the proof.

----

With the conclusion, it is easy to have 1 liners for Java and Python 3:

```
    public double nthPersonGetsNthSeat(int n) {
        return n == 1 ? 1.0d : .5d;
    }
```
```
    def nthPersonGetsNthSeat(self, n: int) -> float:
        return 1.0 if n == 1 else 0.5
```
</p>


### 【Proof without math】Three tiny stories
- Author: kaiwensun
- Creation Date: Mon Nov 04 2019 13:23:33 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 04 2019 13:25:51 GMT+0800 (Singapore Standard Time)

<p>
Let\'s say the first passenger\'s name is Trump.

In the last passenger\'s view, if his seat is taken, he doesn\'t care who takes his seat. He only cares whether his seat is taken.

Therefore, until the last person is on board, in terms of whether the last person\'s seat is taken, the following stories have the same probability of the seat being taken:

* The original story. if a passenger\'s seat is taken, randomly pick an untaken one.
* If a passenger finds his/her seat taken, he/she rudely extrudes the person on the seat. And the extruded person has to randomly pick an empty seat again. Obviously, in this scenario, Trump is the only person being extruded (maybe again and again). This won\'t change the final probability, because whoever being extruded must randomly pick a seat. So it doesn\'t matter who is extruded. Trump, or the ticket holder, it doesn\'t matter. So let\'s make Trump always be extruded.
* As long as Trump picks the seat that doesn\'t belong to him, he will be extruded. All ticket holders (except the last passenger) will always sit on their own seats. So it doesn\'t change the final probability if we change the boarding order into: the `n - 2` ticket holders -> Trump -> the last passenger.

In the thrid scenario, When Trump is boarding, he can randomly choose a seat from the remaining two seats. So when the last passenger is boarding, the probability that his seat is taken is 1/2.
</p>


