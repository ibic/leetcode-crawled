---
title: "Accounts Merge"
weight: 642
#id: "accounts-merge"
---
## Description
<div class="description">
<p>Given a list <code>accounts</code>, each element <code>accounts[i]</code> is a list of strings, where the first element <code>accounts[i][0]</code> is a <i>name</i>, and the rest of the elements are <i>emails</i> representing emails of the account.</p>

<p>Now, we would like to merge these accounts.  Two accounts definitely belong to the same person if there is some email that is common to both accounts.  Note that even if two accounts have the same name, they may belong to different people as people could have the same name.  A person can have any number of accounts initially, but all of their accounts definitely have the same name.</p>

<p>After merging the accounts, return the accounts in the following format: the first element of each account is the name, and the rest of the elements are emails <b>in sorted order</b>.  The accounts themselves can be returned in any order.</p>

<p><b>Example 1:</b><br />
<pre style="white-space: pre-wrap">
<b>Input:</b> 
accounts = [["John", "johnsmith@mail.com", "john00@mail.com"], ["John", "johnnybravo@mail.com"], ["John", "johnsmith@mail.com", "john_newyork@mail.com"], ["Mary", "mary@mail.com"]]
<b>Output:</b> [["John", 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com'],  ["John", "johnnybravo@mail.com"], ["Mary", "mary@mail.com"]]
<b>Explanation:</b> 
The first and third John's are the same person as they have the common email "johnsmith@mail.com".
The second John and Mary are different people as none of their email addresses are used by other accounts.
We could return these lists in any order, for example the answer [['Mary', 'mary@mail.com'], ['John', 'johnnybravo@mail.com'], 
['John', 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com']] would still be accepted.
</pre>
</p>

<p><b>Note:</b>
<li>The length of <code>accounts</code> will be in the range <code>[1, 1000]</code>.</li>
<li>The length of <code>accounts[i]</code> will be in the range <code>[1, 10]</code>.</li>
<li>The length of <code>accounts[i][j]</code> will be in the range <code>[1, 30]</code>.</li>
</p>
</div>

## Tags
- Depth-first Search (depth-first-search)
- Union Find (union-find)

## Companies
- Facebook - 20 (taggedByAdmin: true)
- Amazon - 12 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- LinkedIn - 6 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Houzz - 4 (taggedByAdmin: false)

## Official Solution
[TOC]


#### Approach #1: Depth-First Search [Accepted]

**Intuition**

Draw an edge between two emails if they occur in the same account.  The problem comes down to finding connected components of this graph.

**Algorithm**

For each account, draw the edge from the first email to all other emails.  Additionally, we'll remember a map from emails to names on the side.  After finding each connected component using a depth-first search, we'll add that to our answer.

**Python**
```python
class Solution(object):
    def accountsMerge(self, accounts):
        em_to_name = {}
        graph = collections.defaultdict(set)
        for acc in accounts:
            name = acc[0]
            for email in acc[1:]:
                graph[acc[1]].add(email)
                graph[email].add(acc[1])
                em_to_name[email] = name

        seen = set()
        ans = []
        for email in graph:
            if email not in seen:
                seen.add(email)
                stack = [email]
                component = []
                while stack:
                    node = stack.pop()
                    component.append(node)
                    for nei in graph[node]:
                        if nei not in seen:
                            seen.add(nei)
                            stack.append(nei)
                ans.append([em_to_name[email]] + sorted(component))
        return ans
```

**Java**
```java
class Solution {
    public List<List<String>> accountsMerge(List<List<String>> accounts) {
        Map<String, String> emailToName = new HashMap();
        Map<String, ArrayList<String>> graph = new HashMap();
        for (List<String> account: accounts) {
            String name = "";
            for (String email: account) {
                if (name == "") {
                    name = email;
                    continue;
                }
                graph.computeIfAbsent(email, x-> new ArrayList<String>()).add(account.get(1));
                graph.computeIfAbsent(account.get(1), x-> new ArrayList<String>()).add(email);
                emailToName.put(email, name);
            }
        }

        Set<String> seen = new HashSet();
        List<List<String>> ans = new ArrayList();
        for (String email: graph.keySet()) {
            if (!seen.contains(email)) {
                seen.add(email);
                Stack<String> stack = new Stack();
                stack.push(email);
                List<String> component = new ArrayList();
                while (!stack.empty()) {
                    String node = stack.pop();
                    component.add(node);
                    for (String nei: graph.get(node)) {
                        if (!seen.contains(nei)) {
                            seen.add(nei);
                            stack.push(nei);
                        }
                    }
                }
                Collections.sort(component);
                component.add(0, emailToName.get(email));
                ans.add(component);
            }
        }
        return ans;
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(\sum a_i \log a_i)$$, where $$a_i$$ is the length of `accounts[i]`.  Without the log factor, this is the complexity to build the graph and search for each component.  The log factor is for sorting each component at the end.

* Space Complexity: $$O(\sum a_i)$$, the space used by our graph and our search.

---
#### Approach #2: Union-Find [Accepted]

**Intuition**

As in *Approach #1*, our problem comes down to finding the connected components of a graph.  This is a natural fit for a *Disjoint Set Union* (DSU) structure.

**Algorithm**

As in *Approach #1*, draw edges between emails if they occur in the same account.  For easier interoperability between our DSU template, we will map each email to some integer index by using `emailToID`.  Then, `dsu.find(email)` will tell us a unique id representing what component that email is in.

For more information on DSU, please look at *Approach #2* in the [article here](https://leetcode.com/articles/redundant-connection/).  For brevity, the solutions showcased below do not use *union-by-rank*.

**Python**
```python
class DSU:
    def __init__(self):
        self.p = range(10001)
    def find(self, x):
        if self.p[x] != x:
            self.p[x] = self.find(self.p[x])
        return self.p[x]
    def union(self, x, y):
        self.p[self.find(x)] = self.find(y)

class Solution(object):
    def accountsMerge(self, accounts):
        dsu = DSU()
        em_to_name = {}
        em_to_id = {}
        i = 0
        for acc in accounts:
            name = acc[0]
            for email in acc[1:]:
                em_to_name[email] = name
                if email not in em_to_id:
                    em_to_id[email] = i
                    i += 1
                dsu.union(em_to_id[acc[1]], em_to_id[email])

        ans = collections.defaultdict(list)
        for email in em_to_name:
            ans[dsu.find(em_to_id[email])].append(email)

        return [[em_to_name[v[0]]] + sorted(v) for v in ans.values()]
```

**Java**
```java
class Solution {
    public List<List<String>> accountsMerge(List<List<String>> accounts) {
        DSU dsu = new DSU();
        Map<String, String> emailToName = new HashMap();
        Map<String, Integer> emailToID = new HashMap();
        int id = 0;
        for (List<String> account: accounts) {
            String name = "";
            for (String email: account) {
                if (name == "") {
                    name = email;
                    continue;
                }
                emailToName.put(email, name);
                if (!emailToID.containsKey(email)) {
                    emailToID.put(email, id++);
                }
                dsu.union(emailToID.get(account.get(1)), emailToID.get(email));
            }
        }

        Map<Integer, List<String>> ans = new HashMap();
        for (String email: emailToName.keySet()) {
            int index = dsu.find(emailToID.get(email));
            ans.computeIfAbsent(index, x-> new ArrayList()).add(email);
        }
        for (List<String> component: ans.values()) {
            Collections.sort(component);
            component.add(0, emailToName.get(component.get(0)));
        }
        return new ArrayList(ans.values());
    }
}
class DSU {
    int[] parent;
    public DSU() {
        parent = new int[10001];
        for (int i = 0; i <= 10000; ++i)
            parent[i] = i;
    }
    public int find(int x) {
        if (parent[x] != x) parent[x] = find(parent[x]);
        return parent[x];
    }
    public void union(int x, int y) {
        parent[find(x)] = find(y);
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(A \log A)$$, where $$A = \sum a_i$$, and $$a_i$$ is the length of `accounts[i]`.  If we used union-by-rank, this complexity improves to $$O(A \alpha(A)) \approx O(A)$$, where $$\alpha$$ is the *Inverse-Ackermann* function.

* Space Complexity: $$O(A)$$, the space used by our DSU structure.

## Accepted Submission (python3)
```python3
#
# @lc app=leetcode id=721 lang=python3
#
# [721] Accounts Merge
#
from typing import List

class DisjointSet:
    class Element:
        elements = {}
        def __init__(self, v):
            self.v = v
            self.parent = self
            self.rank = 0
            self.size = 0
    @staticmethod
    def createElement(v):
        if v in DisjointSet.Element.elements:
            return DisjointSet.Element.elements[v]
        else:
            e = DisjointSet.Element(v)
            DisjointSet.Element.elements[v] = e
            return e
    @staticmethod
    def clear():
        DisjointSet.Element.elements.clear()
    def __hash__(self):
        return hash(self.v)
    def __eq__(self, rhs):
        return self.v == rhs.v

    @staticmethod
    def find(x):
        if x.parent != x:
            x.parent = DisjointSet.find(x.parent)
        return x.parent
    @staticmethod
    def unionByRank(x, y):
        xRoot = DisjointSet.find(x)
        yRoot = DisjointSet.find(y)
        if xRoot == yRoot:
            return xRoot
        if xRoot.rank < yRoot.rank:
            xRoot, yRoot = yRoot, xRoot
        yRoot.parent = xRoot
        if xRoot.rank == yRoot.rank:
            xRoot.rank += 1
        return xRoot
    @staticmethod
    def unionBySize(x, y):
        xRoot = DisjointSet.find(x)
        yRoot = DisjointSet.find(y)
        if xRoot == yRoot:
            return xRoot
        if xRoot.size < yRoot.size:
            xRoot, yRoot = yRoot, xRoot
        yRoot.parent = xRoot
        xRoot.size += yRoot.size
        return xRoot

class Solution:
    def accountsMerge(self, accounts: List[List[str]]) -> List[List[str]]:
        r = []
        allEmails = set(DisjointSet.createElement(email) for account in accounts for email in account[1:])
        emailNameMap = {}
        for account in accounts:
            lena = len(account)
            if lena <= 1:
                r.append(account)
                continue
            name = account[0]
            firstEmail = account[1]
            firstEmailElement = DisjointSet.createElement(firstEmail)
            for email in account[1:]:
                emailNameMap[email] = name
                emailElement = DisjointSet.createElement(email)
                emailRoot = DisjointSet.unionByRank(firstEmailElement, emailElement)
        rootMap = {}
        for email in allEmails:
            emailRoot = DisjointSet.find(email)
            if emailRoot in rootMap:
                rootMap[emailRoot].append(email.v)
            else:
                #print(emailNameMap, email.v)
                newList = [emailNameMap[email.v], email.v]
                r.append(newList)
                rootMap[emailRoot] = newList
        for i in range(len(r)):
            r[i] = [r[i][0]] + sorted(r[i][1:])
        DisjointSet.clear()
        return r

def main():
    sol = Solution()
    testInput = [["John","johnsmith@mail.com","john_newyork@mail.com"],["John","johnsmith@mail.com","john00@mail.com"],["Mary","mary@mail.com"],["John","johnnybravo@mail.com"]]
    testInput38 = [["Kevin","Kevin1@m.co","Kevin5@m.co","Kevin2@m.co"],["Bob","Bob3@m.co","Bob1@m.co","Bob2@m.co"],["Lily","Lily3@m.co","Lily2@m.co","Lily0@m.co"],["Gabe","Gabe2@m.co","Gabe0@m.co","Gabe2@m.co"],["Kevin","Kevin4@m.co","Kevin3@m.co","Kevin3@m.co"]]
    print(sol.accountsMerge(
        testInput38
    ))

if __name__ == "__main__":
    main()


```

## Top Discussions
### [Java/C++] Union Find
- Author: alexander
- Creation Date: Sun Nov 05 2017 11:16:21 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 22:07:25 GMT+0800 (Singapore Standard Time)

<p>
1. The key task here is to `connect` those `emails`, and this is a perfect use case for union find.
2. to group these emails, each group need to have a `representative`, or `parent`.
3. At the beginning, set each email as its own representative.
4. Emails in each account naturally belong to a same group, and should be joined by assigning to the same parent (let's use the parent of first email in that list);

Simple Example:
```
a b c // now b, c have parent a
d e f // now e, f have parent d
g a d // now abc, def all merged to group g

parents populated after parsing 1st account: a b c
a->a
b->a
c->a

parents populated after parsing 2nd account: d e f
d->d
e->d
f->d

parents populated after parsing 3rd account: g a d
g->g
a->g
d->g

```

**Java**
```
class Solution {
    public List<List<String>> accountsMerge(List<List<String>> acts) {
        Map<String, String> owner = new HashMap<>();
        Map<String, String> parents = new HashMap<>();
        Map<String, TreeSet<String>> unions = new HashMap<>();
        for (List<String> a : acts) {
            for (int i = 1; i < a.size(); i++) {
                parents.put(a.get(i), a.get(i));
                owner.put(a.get(i), a.get(0));
            }
        }
        for (List<String> a : acts) {
            String p = find(a.get(1), parents);
            for (int i = 2; i < a.size(); i++)
                parents.put(find(a.get(i), parents), p);
        }
        for(List<String> a : acts) {
            String p = find(a.get(1), parents);
            if (!unions.containsKey(p)) unions.put(p, new TreeSet<>());
            for (int i = 1; i < a.size(); i++)
                unions.get(p).add(a.get(i));
        }
        List<List<String>> res = new ArrayList<>();
        for (String p : unions.keySet()) {
            List<String> emails = new ArrayList(unions.get(p));
            emails.add(0, owner.get(p));
            res.add(emails);
        }
        return res;
    }
    private String find(String s, Map<String, String> p) {
        return p.get(s) == s ? s : find(p.get(s), p);
    }
}
```
**C++**
```
class Solution {
public:
    vector<vector<string>> accountsMerge(vector<vector<string>>& acts) {
        map<string, string> owner;
        map<string, string> parents;
        map<string, set<string>> unions;
        for (int i = 0; i < acts.size(); i++) {
            for (int j = 1; j < acts[i].size(); j++) {
                parents[acts[i][j]] = acts[i][j];
                owner[acts[i][j]] = acts[i][0];
            }
        }
        for (int i = 0; i < acts.size(); i++) {
            string p = find(acts[i][1], parents);
            for (int j = 2; j < acts[i].size(); j++)
                parents[find(acts[i][j], parents)] = p;
        }
        for (int i = 0; i < acts.size(); i++)
            for (int j = 1; j < acts[i].size(); j++)
                unions[find(acts[i][j], parents)].insert(acts[i][j]);

        vector<vector<string>> res;
        for (pair<string, set<string>> p : unions) {
            vector<string> emails(p.second.begin(), p.second.end());
            emails.insert(emails.begin(), owner[p.first]);
            res.push_back(emails);
        }
        return res;
    }
private:
    string find(string s, map<string, string>& p) {
        return p[s] == s ? s : find(p[s], p);
    }
};
```
**C++ Lambda**
```
class Solution {
public:
    vector<vector<string>> accountsMerge(vector<vector<string>>& acts) {
        map<string, string> owner;
        map<string, string> parents;
        function<string(string)> find = [&](string s) {return parents[s] == s ? s : find(parents[s]); };
        for (int i = 0; i < acts.size(); i++) {
            for (int j = 1; j < acts[i].size(); j++) {
                parents[acts[i][j]] = acts[i][j];
                owner[acts[i][j]] = acts[i][0];
            }
        }
        for (int i = 0; i < acts.size(); i++) {
            string p = find(acts[i][1]);
            for (int j = 2; j < acts[i].size(); j++) {
                parents[find(acts[i][j])] = p;
            }
        }
        map<string, set<string>> unions;
        for (int i = 0; i < acts.size(); i++) {
            for (int j = 1; j < acts[i].size(); j++) {
                unions[find(acts[i][j])].insert(acts[i][j]);
            }
        }
        vector<vector<string>> merged;
        for (pair<string, set<string>> p : unions) {
            vector<string> emails(p.second.begin(), p.second.end());
            emails.insert(emails.begin(), owner[p.first]);
            merged.push_back(emails);
        }
        return merged;
    }
};
```
</p>


### Java Solution (Build graph + DFS search)
- Author: FLAGbigoffer
- Creation Date: Sun Nov 05 2017 11:15:56 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 23:54:36 GMT+0800 (Singapore Standard Time)

<p>
I have tried my best to make my code clean. Hope the basic idea below may help you. Happy coding!

Basicly, this is a graph problem. Notice that each account[ i ] tells us some edges. What we have to do is as follows:
1. Use these edges to build some components. Common email addresses are like the intersections that connect each single component for each account.
2. Because each component represents a merged account, do DFS search for each components and add into a list. Before add the name into this list, sort the emails. Then add name string into it.

Examples: Assume we have three accounts, we connect them like this in order to use DFS. 
{Name, 1, 2, 3} => Name -- 1 -- 2 -- 3
{Name, 2, 4, 5} => Name -- 2 -- 4 -- 5 (The same graph node 2 appears)
{Name, 6, 7, 8} => Name -- 6 -- 7 -- 8
(Where numbers represent email addresses).

```
class Solution {
    public List<List<String>> accountsMerge(List<List<String>> accounts) {
        Map<String, Set<String>> graph = new HashMap<>();  //<email node, neighbor nodes>
        Map<String, String> name = new HashMap<>();        //<email, username>
        // Build the graph;
        for (List<String> account : accounts) {
            String userName = account.get(0);
            for (int i = 1; i < account.size(); i++) {
                if (!graph.containsKey(account.get(i))) {
                    graph.put(account.get(i), new HashSet<>());
                }
                name.put(account.get(i), userName);
                
                if (i == 1) continue;
                graph.get(account.get(i)).add(account.get(i - 1));
                graph.get(account.get(i - 1)).add(account.get(i));
            }
        }
        
        Set<String> visited = new HashSet<>();
        List<List<String>> res = new LinkedList<>();
        // DFS search the graph;
        for (String email : name.keySet()) {
            List<String> list = new LinkedList<>();
            if (visited.add(email)) {
                dfs(graph, email, visited, list);
                Collections.sort(list);
                list.add(0, name.get(email));
                res.add(list);
            }
        }
        
        return res;
    }
    
    public void dfs(Map<String, Set<String>> graph, String email, Set<String> visited, List<String> list) {
        list.add(email);
        for (String next : graph.get(email)) {
            if (visited.add(next)) {
                dfs(graph, next, visited, list);
            }
        }
    }
}
````
Please check the clean version posted by @zhangchunli below. Thanks for updating.
</p>


### Python Simple DFS with explanation!!!
- Author: yangshun
- Creation Date: Sun Nov 05 2017 12:32:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 04:19:58 GMT+0800 (Singapore Standard Time)

<p>
We give each account an ID, based on the index of it within the list of accounts.

```
[
["John", "johnsmith@mail.com", "john00@mail.com"], # Account 0
["John", "johnnybravo@mail.com"], # Account 1
["John", "johnsmith@mail.com", "john_newyork@mail.com"],  # Account 2
["Mary", "mary@mail.com"] # Account 3
]
```

Next, build an `emails_accounts_map` that maps an email to a list of accounts, which can be used to track which email is linked to which account. This is essentially our graph.

```
# emails_accounts_map of email to account ID
{
  "johnsmith@mail.com": [0, 2],
  "john00@mail.com": [0],
  "johnnybravo@mail.com": [1],
  "john_newyork@mail.com": [2],
  "mary@mail.com": [3]
}
```

Next we do a DFS on each account in accounts list and look up `emails_accounts_map` to tell us which accounts are linked to that particular account via common emails. This will make sure we visit each account only once. This is a recursive process and we should collect all the emails that we encounter along the way.

Lastly, sort the collected emails and add it to final results, `res` along with the name.

*- Yangshun*

```
class Solution(object):
    def accountsMerge(self, accounts):
        from collections import defaultdict
        visited_accounts = [False] * len(accounts)
        emails_accounts_map = defaultdict(list)
        res = []
        # Build up the graph.
        for i, account in enumerate(accounts):
            for j in range(1, len(account)):
                email = account[j]
                emails_accounts_map[email].append(i)
        # DFS code for traversing accounts.
        def dfs(i, emails):
            if visited_accounts[i]:
                return
            visited_accounts[i] = True
            for j in range(1, len(accounts[i])):
                email = accounts[i][j]
                emails.add(email)
                for neighbor in emails_accounts_map[email]:
                    dfs(neighbor, emails)
        # Perform DFS for accounts and add to results.
        for i, account in enumerate(accounts):
            if visited_accounts[i]:
                continue
            name, emails = account[0], set()
            dfs(i, emails)
            res.append([name] + sorted(emails))
        return res
```
</p>


