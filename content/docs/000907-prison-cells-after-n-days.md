---
title: "Prison Cells After N Days"
weight: 907
#id: "prison-cells-after-n-days"
---
## Description
<div class="description">
<p>There are 8 prison cells in a row, and each cell is either occupied or vacant.</p>

<p>Each day, whether the cell is occupied or vacant changes according to the following rules:</p>

<ul>
	<li>If a cell has two adjacent neighbors that are both occupied or both vacant,&nbsp;then the cell becomes occupied.</li>
	<li>Otherwise, it becomes vacant.</li>
</ul>

<p>(Note that because the prison is a row, the first and the last cells in the row can&#39;t have two adjacent neighbors.)</p>

<p>We describe the current state of the prison&nbsp;in the following way:&nbsp;<code>cells[i] == 1</code> if the <code>i</code>-th cell is occupied, else <code>cells[i] == 0</code>.</p>

<p>Given the initial state of the prison, return the state of the prison after <code>N</code> days (and <code>N</code> such changes described above.)</p>

<p>&nbsp;</p>

<div>
<ol>
</ol>
</div>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>cells = <span id="example-input-1-1">[0,1,0,1,1,0,0,1]</span>, N = <span id="example-input-1-2">7</span>
<strong>Output: </strong><span id="example-output-1">[0,0,1,1,0,0,0,0]</span>
<strong>Explanation: 
</strong><span id="example-output-1">The following table summarizes the state of the prison on each day:
Day 0: [0, 1, 0, 1, 1, 0, 0, 1]
Day 1: [0, 1, 1, 0, 0, 0, 0, 0]
Day 2: [0, 0, 0, 0, 1, 1, 1, 0]
Day 3: [0, 1, 1, 0, 0, 1, 0, 0]
Day 4: [0, 0, 0, 0, 0, 1, 0, 0]
Day 5: [0, 1, 1, 1, 0, 1, 0, 0]
Day 6: [0, 0, 1, 0, 1, 1, 0, 0]
Day 7: [0, 0, 1, 1, 0, 0, 0, 0]</span>

</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>cells = <span id="example-input-2-1">[1,0,0,1,0,0,1,0]</span>, N = <span id="example-input-2-2">1000000000</span>
<strong>Output: </strong><span id="example-output-2">[0,0,1,1,1,1,1,0]</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>cells.length == 8</code></li>
	<li><code>cells[i]</code> is in <code>{0, 1}</code></li>
	<li><code>1 &lt;= N &lt;= 10^9</code></li>
</ol>
</div>
</div>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Amazon - 31 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Overview

First of all, one can consider this problem as a simplified version of the [Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) invented by the British mathematician John Horton Conway in 1970.

By simplification, this problem is played on one dimensional array (compared to 2D in Game of Life), and it has less rules.

>Due to the nature of game, one of the most intuitive solutions to solve this problem is _playing the game_, _i.e._ we can simply run the **simulation**.

Starting from the initial state of the prison cells, we could evolve the states following the rules defined in the problem _step by step_.

In the following sections, we will give some approaches on how to run the simulation efficiently.

---
#### Approach 1: Simulation with Fast Forwarding

**Intuition**

One important observation from the Game of Life is that we would encounter some already-seen state over the time, simply due to the fact that there are limited number of states.

The above observation applies to our problem here as well. Given $$K$$ number of cells, there could be at most $$2^K$$ possible states. If the number of steps is larger than all possible states (_i.e._ $$N \gt 2^K$$), we are destined to repeat ourselves sooner or later.

In fact, we would encounter the repetitive states **sooner** than the theoretical boundary we estimated above.
For instance, with the initial state of `[1,0,0,0,1,0,0,1]`, just after `15` steps, we would encounter a previously seen state.
Once we encounter a state seen before, the history would then repeat itself again and again, assuming that time is infinite.

>All states between two repetitive states form a cycle, which would repeat itself over the time.
Therefore, based on this observation, we could **fast-forward** the simulation rather than going step by step, once we encounter any repetitive state.

**Algorithm**

Here is the overall idea to implement our fast-forward strategy.

- First of all, we record the state at each step, with the index of the current step, _i.e._ `state -> step_index`.

- Once we discover a repetitive state, we can then determine the **_length_** (denoted as $$C$$) of the cycle, with the help of hashmap that we recorded.

- Starting from this repetitive state, the prison cells would play out the states within the cycle over and over, until we run out of steps.

- In other words, if the remaining steps is $$N$$, at least we could **_fast-forward_** to the step of $$N \mod C$$.

- And then from the step of $$N \mod C$$, we continue the simulation step by step.

![fastforward](../Figures/957/957_fastforward.png)

Note: we only need to do the fast-forward once, if there is any.

Here are some sample implementations based on the above idea.

<iframe src="https://leetcode.com/playground/RaVwhut9/shared" frameBorder="0" width="100%" height="500" name="RaVwhut9"></iframe>



**Complexity Analysis**

Let $$K$$ be the number of cells, and $$N$$ be the number of steps.

- Time Complexity: $$\mathcal{O}\big(K \cdot \min(N, 2^K)\big)$$ 

    - As we discussed before, at most we could have $$2^K$$ possible states. While we run the simulation with $$N$$ steps, we might need to run $$\min(N, 2^K)$$ steps without fast-forwarding in the worst case.

    - For each simulation step, it takes $$\mathcal{O}(K)$$ time to process and evolve the state of cells.

    - Hence, the overall time complexity of the algorithm is $$\mathcal{O}\big(K \cdot \min(N, 2^K)\big)$$.

- Space Complexity:

    - The main memory consumption of the algorithm is the hashmap that we used to keep track of the states of the cells. The maximal number of entries in the hashmap would be $$2^K$$ as we discussed before.

    - In the Java implementation, we encode the state as a single integer value. Therefore, its space complexity would be $$\mathcal{O}(2^K)$$, assuming that $$K$$ does not exceed 32 so that a state can fit into a single integer number.

    - In the Python implementation, we keep the states of cells as they are in the hashmap. As a result, for each entry, it takes $$\mathcal{O}(K)$$ space. In total, its space complexity becomes $$\mathcal{O}(K \cdot 2^K)$$.

---
#### Approach 2: Simulation with Bitmap

**Intuition**

In the above approach, we implemented the function `nextDay(state)`, which runs an **iteration** to calculate the next state of cells, given the current state.

Given that we have already encoded the state as a bitmap in the Java implementation of the previous approach, a more efficient way to implement the `nextDay` function would be to apply the **_bit operations_** (_e.g_ _AND, OR, XOR_ _etc._).

The next state of a cell depends on its left and right neighbors.
To align the states of its neighbors, we could make a _left_ and a _right_ shift respectively on the bitmap.
Upon the shifted bitmaps, we then apply the _XOR_ and _NOT_ operations sequentially, which would lead to the next state of the cell.

Here we show how it works with a concrete example.

![bit operations](../Figures/957/957_bit_operations.png)

Note that, the head and tail cells are particular, which would remain vacant once we start the simulation. Therefore, we should reset the head and tail bits by applying the bit _AND_ operation with the **bitmask** of `01111110` (_i.e._ `0x7e`).

**Algorithm**

We could reuse the bulk of the previous implementations, and simply rewrite the `nextDay` function with the bit operations as we discussed.

Additionally, at the end of the simulation, we should *decode* the states of the cells from the final bitmap.

<iframe src="https://leetcode.com/playground/EdosuxcW/shared" frameBorder="0" width="100%" height="500" name="EdosuxcW"></iframe>


**Complexity Analysis**

Let $$K$$ be the number of cells, and $$N$$ be the number of steps.

- Time Complexity: $$\mathcal{O}\big(\min(N, 2^K)\big)$$ assuming that $$K$$ does not exceed 32.

    - As we discussed before, at most we could have $$2^K$$ possible states. While we run the simulation, we need to run $$\min(N, 2^K)$$ steps without fast-forwarding in the worst case.

    - For each simulation step, it takes a constant $$\mathcal{O}(1)$$ time to process and evolve the states of cells, since we applied the bit operations rather than iteration. 

    - Hence, the overall time complexity of the algorithm is $$\mathcal{O}\big(\min(N, 2^K)\big)$$.

- Space Complexity: $$\mathcal{O}(2^K)$$

    - The main memory consumption of the algorithm is the hashmap that we used to keep track of the states of the cells. The maximal number of entries in the hashmap would be $$2^K$$ as we discussed before.

    - This time we adopted the bitmap for both Java and Python implementation, so that each state consumes a constant $$\mathcal{O}(1)$$ space.

    - To sum up, the overall space complexity of the algorithm is $$\mathcal{O}(2^K)$$.

---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java: easy to understand
- Author: xiaogugu
- Creation Date: Tue Apr 02 2019 07:49:12 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 03 2019 07:38:30 GMT+0800 (Singapore Standard Time)

<p>
While the most voted solutions are smart and brilliant, I can\'t wrap my head around to write their solutions in an actual interview. So here goes my solution that is  easy to understand:

1.Have a sub function nextDay() that finds the next day\'s cell states
2.Iterate and store the cell states that occurred previously
3.If there\'s no cycle, return. If there\'s a cycle, break the loop and rerun N%cycle times to find the target cell states


```
class Solution {
    public int[] prisonAfterNDays(int[] cells, int N) {
		if(cells==null || cells.length==0 || N<=0) return cells;
        boolean hasCycle = false;
        int cycle = 0;
        HashSet<String> set = new HashSet<>(); 
        for(int i=0;i<N;i++){
            int[] next = nextDay(cells);
            String key = Arrays.toString(next);
            if(!set.contains(key)){ //store cell state
                set.add(key);
                cycle++;
            }
            else{ //hit a cycle
                hasCycle = true;
                break;
            }
            cells = next;
        }
        if(hasCycle){
            N%=cycle;
            for(int i=0;i<N;i++){
                cells = nextDay(cells);
            }   
        }
        return cells;
    }
    
    private int[] nextDay(int[] cells){
        int[] tmp = new int[cells.length];
        for(int i=1;i<cells.length-1;i++){
            tmp[i]=cells[i-1]==cells[i+1]?1:0;
        }
        return tmp;
    }
}
```

Complexity Analysis:
As the cells have a fixed size of 8 but the first and last cell will not be updated because they do not have two adjacent neighbors, we have at most 2^6 = 64 states. So regardless of input N, we have both space and time complexities as O(1). 

</p>


### [Java/Python] Find the Loop or Mod 14
- Author: lee215
- Creation Date: Sun Dec 16 2018 12:06:42 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 14 2020 14:45:11 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
```java
    public int[] prisonAfterNDays(int[] cells, int N) {
        while (N > 0) {
            N--;
            int[] cells2 = new int[8];
            for (int i = 1; i < 7; ++i)
                cells2[i] = cells[i - 1] == cells[i + 1] ? 1 : 0;
            cells = cells2;
        }
        return cells;
    }
```

This is right solution, but it will get TLE when `N` is big.
Note that `cells.length = 8`, and `cells[0]` and `cells[7]` will become `0`.
In fact, `cells` have only 2 ^ 6 = 64 different states.
And there will be a loop.
<br>
<br>


# Solution 1: HashMap
We record all seen states.
Be careful,
we need transform array to string as the key,
otherwise it use the reference.

Time `O(2^6)`
Space `O(2^6)`

(Actually they are both `O(14)` after the observation of solution 2)
<br>

**Java**
```java
    public int[] prisonAfterNDays(int[] cells, int N) {
        Map<String, Integer> seen = new HashMap<>();
        while (N > 0) {
            int[] cells2 = new int[8];
            seen.put(Arrays.toString(cells), N--);
            for (int i = 1; i < 7; ++i)
                cells2[i] = cells[i - 1] == cells[i + 1] ? 1 : 0;
            cells = cells2;
            if (seen.containsKey(Arrays.toString(cells))) {
                N %= seen.get(Arrays.toString(cells)) - N;
            }
        }
        return cells;
    }
```
**Python**
```python
    def prisonAfterNDays(self, cells, N):
        seen = {str(cells): N}
        while N:
            seen.setdefault(str(cells), N)
            N -= 1
            cells = [0] + [cells[i - 1] ^ cells[i + 1] ^ 1 for i in range(1, 7)] + [0]
            if str(cells) in seen:
                N %= seen[str(cells)] - N
        return cells
```
<br>
<br>


# Solution 2: Find the loop pattern
This a solution I conducted from the answer.  Don\'t take it as a solution for interview.
The intuition is that, the length of the loop should follow a pattern, 
for example, the length of cells.

So I tried to find the pattern of the loop and brute force all combinations.
Well, then I found that the length of loop will be 1, 7, or 14.

So once we enter the loop, every 14 steps must be the same state.


Also a little more of prove, the length of cells is even,
so for any state, we can find a previous state.
So all states are in a loop.

Combine the above two condition, 
after a single step from the initial state, 
we will enter the loop.

So we can do `N = (N - 1) % 14 + 1` to reduce the run time.

Time `O(14)`
Space `O(14)`
<br>

**Java**
```java
    public int[] prisonAfterNDays(int[] cells, int N) {
        for (N = (N - 1) % 14 + 1; N > 0; --N) {
            int[] cells2 = new int[8];
            for (int i = 1; i < 7; ++i)
                cells2[i] = cells[i - 1] == cells[i + 1] ? 1 : 0;
            cells = cells2;
        }
        return cells;
    }
```
**C++**
```cpp
    vector<int> prisonAfterNDays(vector<int>& cells, int N) {
        for (N = (N - 1) % 14 + 1; N > 0; --N) {
            vector<int> cells2(8, 0);
            for (int i = 1; i < 7; ++i)
                cells2[i] = cells[i - 1] == cells[i + 1] ? 1 : 0;
            cells = cells2;
        }
        return cells;
    }
```
**Python:**
```py
    def prisonAfterNDays(self, cells, N):
        for i in xrange((N - 1) % 14 + 1):
            cells = [0] + [cells[i - 1] ^ cells[i + 1] ^ 1 for i in range(1, 7)] + [0]
        return cells
```
</p>


### Can you solve this efficiently in an interview, if you are seeing it for the first time?
- Author: user6339
- Creation Date: Mon Apr 15 2019 04:47:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 15 2019 04:47:54 GMT+0800 (Singapore Standard Time)

<p>
There are two key observations to make here
1> The transformations form a cycle.
2>The original array that is passed in, is not a part of that cycle.

Is it realistic to expect someone to make these observations if they are seeing this problem for the first time?

I personally think if someone is seeing this for the first time and within 30 mins they can make this insight and solve this, then they must be an olympiad mathematics champion. Most people cannot solve it that fast even if they are really good with datastructures and algorithms.
</p>


