---
title: "N-Queens"
weight: 51
#id: "n-queens"
---
## Description
<div class="description">
<p>The <em>n</em>-queens puzzle is the problem of placing <em>n</em> queens on an <em>n</em>&times;<em>n</em> chessboard such that no two queens attack each other.</p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2018/10/12/8-queens.png" style="width: 258px; height: 276px;" /></p>

<p>Given an integer <em>n</em>, return all distinct solutions to the <em>n</em>-queens puzzle.</p>

<p>Each solution contains a distinct board configuration of the <em>n</em>-queens&#39; placement, where <code>&#39;Q&#39;</code> and <code>&#39;.&#39;</code> both indicate a queen and an empty space respectively.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> 4
<strong>Output:</strong> [
 [&quot;.Q..&quot;,  // Solution 1
  &quot;...Q&quot;,
  &quot;Q...&quot;,
  &quot;..Q.&quot;],

 [&quot;..Q.&quot;,  // Solution 2
  &quot;Q...&quot;,
  &quot;...Q&quot;,
  &quot;.Q..&quot;]
]
<strong>Explanation:</strong> There exist two distinct solutions to the 4-queens puzzle as shown above.
</pre>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Amazon - 7 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Rubrik - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Tableau - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Intuition

The first idea is to use brute-force 
that means to generate all possible ways to put `N` queens on the board,
and then check them to keep only the combinations 
with no queen under attack. 
That means $$\mathcal{O}(N^N)$$ time complexity
and hence we're forced to think further how to optimize.

There are two programming conceptions here which could
help.

> The first one is called _constrained programming_. 

That basically means
to put restrictions after each queen placement. One puts a queen on the 
board and that immediately excludes one column, one row and 
two diagonals for the further queens placement. That propagates 
_constraints_ and helps to reduce the number of combinations to consider.

<img src="../Figures/51/51_pic.png" width="500">

> The second one called _backtracking_. 

Let's imagine that one 
puts several queens on the board so that they don't attack each other. 
But the combination chosen is not the optimal one and there is no place
for the next queen. What to do? _To backtrack_. That means to come back,
to change the position of the previously placed queen and try 
to proceed again. If that would not work either, _backtrack_ again.

<img src="../Figures/51/51_backtracking_.png" width="500">
<br />
<br />


---
#### Approach 1: Backtracking

Before to construct the algorithm, 
let's figure out two tips that could
help.

> There could be the only one queen in a row and the only one queen
in a column.

That means that there is no need to consider all squares on the 
board. One could just iterate over the columns.

> For all "hill" diagonals `row + column = const`, 
and for all "dale" diagonals `row - column = const`.  

That would allow us to mark the diagonals which are already under 
attack and to check if a given square `(row, column)` is under attack.

<img src="../Figures/51/51_diagonals.png" width="500">

Now everything is ready to write down the backtrack function 
`backtrack(row = 0)`.

* Start from the first `row = 0`. 
* Iterate over the columns and try to put a queen in each `column`.

    * If square `(row, column)` is not under attack
        
        * Place the queen in `(row, column)` square.
        * Exclude one row, one column and two diagonals from further 
        consideration.
        * If all rows are filled up `row == N`
            * That means that we find out one more solution.
        * Else
            * Proceed to place further queens `backtrack(row + 1)`.
        * Now backtrack : remove the queen from `(row, column)` square.
        
Here is a straightforward implementation of the above algorithm.  

!?!../Documents/51_LIS.json:1000,705!?!

<iframe src="https://leetcode.com/playground/4egsENbb/shared" frameBorder="0" width="100%" height="500" name="4egsENbb"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N!)$$. There is `N` possibilities to put 
the first queen, not more than `N (N - 2)` to put the second one,
not more than `N(N - 2)(N - 4)` for the third one etc. In total that
results in $$\mathcal{O}(N!)$$ time complexity.
* Space complexity : $$\mathcal{O}(N)$$ to keep an information about 
diagonals and rows.

## Accepted Submission (python3)
```python3
class Solution:
    def getBoardStr(self, board, n):
        arr = [['.' for _ in range(n)] for _ in range(n)]
        for i in range(n):
            arr[i][board[i]] = 'Q'
        return [''.join(row) for row in arr]

    def possible(self, arr, y, x):
        for cy, cx in enumerate(arr):
            if x - cx == y - cy or x - cx == cy - y:
                return False
        return True

    def solveBT(self, arr, remaining):
        if len(remaining) == 0:
            yield arr
        else:
            lena = len(arr)
            for cy, cx in enumerate(remaining):
                if self.possible(arr, lena, cx):
                    yield from self.solveBT(arr + [cx], [e for e in remaining if e != cx])

    def solveNQueens(self, n):
        """
        :type n: int
        :rtype: List[List[str]]
        """
        r = self.solveBT([], [e for e in range(n)])
        #queens = [self.getBoardStr(sol, n) for sol in r]
        queens = [["."*i + "Q" + "."*(n-i-1) for i in sol] for sol in r]
        return queens

```

## Top Discussions
### Accepted 4ms c++ solution use backtracking and bitmask, easy understand.
- Author: prime_tang
- Creation Date: Sun May 10 2015 18:10:32 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 00:15:20 GMT+0800 (Singapore Standard Time)

<p>
In this problem, we can go row by row, and in each position, we need to check if the `column`, the `45\xb0 diagonal` and the `135\xb0 diagonal` had a queen before.

**Solution A:** Directly check the validity of each position, *12ms*:

    class Solution {
    public:
        std::vector<std::vector<std::string> > solveNQueens(int n) {
            std::vector<std::vector<std::string> > res;
            std::vector<std::string> nQueens(n, std::string(n, '.'));
            solveNQueens(res, nQueens, 0, n);
            return res;
        }
    private:
        void solveNQueens(std::vector<std::vector<std::string> > &res, std::vector<std::string> &nQueens, int row, int &n) {
            if (row == n) {
                res.push_back(nQueens);
                return;
            }
            for (int col = 0; col != n; ++col)
                if (isValid(nQueens, row, col, n)) {
                    nQueens[row][col] = 'Q';
                    solveNQueens(res, nQueens, row + 1, n);
                    nQueens[row][col] = '.';
                }
        }
        bool isValid(std::vector<std::string> &nQueens, int row, int col, int &n) {
            //check if the column had a queen before.
            for (int i = 0; i != row; ++i)
                if (nQueens[i][col] == 'Q')
                    return false;
            //check if the 45\xb0 diagonal had a queen before.
            for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; --i, --j)
                if (nQueens[i][j] == 'Q')
                    return false;
            //check if the 135\xb0 diagonal had a queen before.
            for (int i = row - 1, j = col + 1; i >= 0 && j < n; --i, ++j)
                if (nQueens[i][j] == 'Q')
                    return false;
            return true;
        }
    };

**Solution B:** Use flag vectors as bitmask, *4ms*:

The number of columns is `n`,  the number of 45\xb0 diagonals is `2 * n - 1`,  the number of 135\xb0 diagonals is also `2 * n - 1`. When reach `[row, col]`, the column No. is `col`, the 45\xb0 diagonal No. is `row + col` and the 135\xb0 diagonal No. is `n - 1 + col - row`. We can use three arrays to indicate if the column or the diagonal had a queen before, if not, we can put a queen in this position and continue. 

**NOTE:** Please don't use `vector<bool> flag` to replace `vector<int> flag` in the following C++ code. In fact, `vector<bool>` is not a STL container. You should avoid to use it. You can also get the knowledge from [here](http://stackoverflow.com/questions/17794569/why-is-vectorbool-not-a-stl-container) and [here](http://stackoverflow.com/questions/670308/alternative-to-vectorbool).

    /**    | | |                / / /             \ \ \
      *    O O O               O O O               O O O
      *    | | |              / / / /             \ \ \ \
      *    O O O               O O O               O O O
      *    | | |              / / / /             \ \ \ \ 
      *    O O O               O O O               O O O
      *    | | |              / / /                 \ \ \
      *   3 columns        5 45\xb0 diagonals     5 135\xb0 diagonals    (when n is 3)
      */
    class Solution {
    public:
        std::vector<std::vector<std::string> > solveNQueens(int n) {
            std::vector<std::vector<std::string> > res;
            std::vector<std::string> nQueens(n, std::string(n, '.'));
            std::vector<int> flag_col(n, 1), flag_45(2 * n - 1, 1), flag_135(2 * n - 1, 1);
            solveNQueens(res, nQueens, flag_col, flag_45, flag_135, 0, n);
            return res;
        }
    private:
        void solveNQueens(std::vector<std::vector<std::string> > &res, std::vector<std::string> &nQueens, std::vector<int> &flag_col, std::vector<int> &flag_45, std::vector<int> &flag_135, int row, int &n) {
            if (row == n) {
                res.push_back(nQueens);
                return;
            }
            for (int col = 0; col != n; ++col)
                if (flag_col[col] && flag_45[row + col] && flag_135[n - 1 + col - row]) {
                    flag_col[col] = flag_45[row + col] = flag_135[n - 1 + col - row] = 0;
                    nQueens[row][col] = 'Q';
                    solveNQueens(res, nQueens, flag_col, flag_45, flag_135, row + 1, n);
                    nQueens[row][col] = '.';
                    flag_col[col] = flag_45[row + col] = flag_135[n - 1 + col - row] = 1;
                }
        }
    };

But we actually do not need to use three arrays, we just need one. Now, when reach `[row, col]`, the subscript of column is `col`, the subscript of 45\xb0 diagonal is `n + row + col` and the subscript of 135\xb0 diagonal  is `n + 2 * n - 1 + n - 1 + col - row`.

    class Solution {
    public:
        std::vector<std::vector<std::string> > solveNQueens(int n) {
            std::vector<std::vector<std::string> > res;
            std::vector<std::string> nQueens(n, std::string(n, '.'));
            /*
            flag[0] to flag[n - 1] to indicate if the column had a queen before.
            flag[n] to flag[3 * n - 2] to indicate if the 45\xb0 diagonal had a queen before.
            flag[3 * n - 1] to flag[5 * n - 3] to indicate if the 135\xb0 diagonal had a queen before.
            */
            std::vector<int> flag(5 * n - 2, 1);
            solveNQueens(res, nQueens, flag, 0, n);
            return res;
        }
    private:
        void solveNQueens(std::vector<std::vector<std::string> > &res, std::vector<std::string> &nQueens, std::vector<int> &flag, int row, int &n) {
            if (row == n) {
                res.push_back(nQueens);
                return;
            }
            for (int col = 0; col != n; ++col)
                if (flag[col] && flag[n + row + col] && flag[4 * n - 2 + col - row]) {
                    flag[col] = flag[n + row + col] = flag[4 * n - 2 + col - row] = 0;
                    nQueens[row][col] = 'Q';
                    solveNQueens(res, nQueens, flag, row + 1, n);
                    nQueens[row][col] = '.';
                    flag[col] = flag[n + row + col] = flag[4 * n - 2 + col - row] = 1;
                }
        }
    };
</p>


### Fast, short, and easy-to-understand python solution, 11 lines, 76ms
- Author: cmc
- Creation Date: Fri Jul 31 2015 09:57:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 03:03:08 GMT+0800 (Singapore Standard Time)

<p>
ideas: <br>
Use the `DFS` helper function to find solutions recursively. A solution will be found when the length of    `queens` is equal to `n` ( `queens` is a list of the indices of the queens).<br><br>
In this problem, whenever a location `(x, y`) is occupied, any other locations `(p, q )` where `p + q == x + y` or `p - q == x - y` would be  invalid. We can use this information to keep track of the indicators (`xy_dif` and  `xy_sum` ) of the invalid positions and then call DFS recursively with valid positions only. <br><br>

At the end, we convert the result (a list of lists; each sublist is the indices of the queens) into the desire format.


    def solveNQueens(self, n):
        def DFS(queens, xy_dif, xy_sum):
            p = len(queens)
            if p==n:
                result.append(queens)
                return None
            for q in range(n):
                if q not in queens and p-q not in xy_dif and p+q not in xy_sum: 
                    DFS(queens+[q], xy_dif+[p-q], xy_sum+[p+q])  
        result = []
        DFS([],[],[])
        return [ ["."*i + "Q" + "."*(n-i-1) for i in sol] for sol in result]
</p>


### My easy understanding Java Solution
- Author: pirateutopia
- Creation Date: Thu Jul 23 2015 00:46:14 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 15:53:58 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public List<List<String>> solveNQueens(int n) {
            char[][] board = new char[n][n];
            for(int i = 0; i < n; i++)
                for(int j = 0; j < n; j++)
                    board[i][j] = '.';
            List<List<String>> res = new ArrayList<List<String>>();
            dfs(board, 0, res);
            return res;
        }
        
        private void dfs(char[][] board, int colIndex, List<List<String>> res) {
            if(colIndex == board.length) {
                res.add(construct(board));
                return;
            }
            
            for(int i = 0; i < board.length; i++) {
                if(validate(board, i, colIndex)) {
                    board[i][colIndex] = 'Q';
                    dfs(board, colIndex + 1, res);
                    board[i][colIndex] = '.';
                }
            }
        }
        
        private boolean validate(char[][] board, int x, int y) {
            for(int i = 0; i < board.length; i++) {
                for(int j = 0; j < y; j++) {
                    if(board[i][j] == 'Q' && (x + j == y + i || x + y == i + j || x == i))
                        return false;
                }
            }
            
            return true;
        }
        
        private List<String> construct(char[][] board) {
            List<String> res = new LinkedList<String>();
            for(int i = 0; i < board.length; i++) {
                String s = new String(board[i]);
                res.add(s);
            }
            return res;
        }
    }
</p>


