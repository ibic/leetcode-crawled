---
title: "Reverse Vowels of a String"
weight: 328
#id: "reverse-vowels-of-a-string"
---
## Description
<div class="description">
<p>Write a function that takes a string as input and reverse only the vowels of a string.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;hello&quot;</span>
<strong>Output: </strong><span id="example-output-1">&quot;holle&quot;</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;leetcode&quot;</span>
<strong>Output: </strong><span id="example-output-2">&quot;leotcede&quot;</span></pre>
</div>

<p><b>Note:</b><br />
The vowels does not include the letter &quot;y&quot;.</p>

<p>&nbsp;</p>

</div>

## Tags
- Two Pointers (two-pointers)
- String (string)

## Companies
- Facebook - 5 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Standard Two Pointer Solution
- Author: ninacc
- Creation Date: Sat Apr 23 2016 13:37:25 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 24 2018 09:39:00 GMT+0800 (Singapore Standard Time)

<p>
In the inner while loop, don't forget the condition "start less than end" while incrementing start  and decrementing end. This is my friend's google phone interview question. Cheers!
// update! May use a HashSet<Character> to reduce the look up time to O(1)

    public class Solution {
    public String reverseVowels(String s) {
        if(s == null || s.length()==0) return s;
        String vowels = "aeiouAEIOU";
        char[] chars = s.toCharArray();
        int start = 0;
        int end = s.length()-1;
        while(start<end){
            
            while(start<end && !vowels.contains(chars[start]+"")){
                start++;
            }
            
            while(start<end && !vowels.contains(chars[end]+"")){
                end--;
            }
            
            char temp = chars[start];
            chars[start] = chars[end];
            chars[end] = temp;
            
            start++;
            end--;
        }
        return new String(chars);
    }
   }
</p>


### 1-2 lines Python/Ruby
- Author: StefanPochmann
- Creation Date: Sun Apr 24 2016 01:08:09 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 23:24:00 GMT+0800 (Singapore Standard Time)

<p>
**Ruby**

    def reverse_vowels(s)
      vowels = s.scan(/[aeiou]/i)
      s.gsub(/[aeiou]/i) { vowels.pop }
    end

---

**Python**

    def reverseVowels(self, s):
        vowels = re.findall('(?i)[aeiou]', s)
        return re.sub('(?i)[aeiou]', lambda m: vowels.pop(), s)

---

It's possible in one line, but I don't really like it:

    def reverseVowels(self, s):
        return re.sub('(?i)[aeiou]', lambda m, v=re.findall('(?i)[aeiou]', s): v.pop(), s)

---

Another version, finding replacement vowels on the fly instead of collecting all in advance:

    def reverseVowels(self, s):
        vowels = (c for c in reversed(s) if c in 'aeiouAEIOU')
        return re.sub('(?i)[aeiou]', lambda m: next(vowels), s)
</p>


### Super clean C++ solution using find_first_of and find_last_of
- Author: ChowTaiFok
- Creation Date: Sat Apr 23 2016 22:02:33 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 05 2018 08:54:57 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        string reverseVowels(string s) {
            int i = 0, j = s.size() - 1;
            while (i < j) {
                i = s.find_first_of("aeiouAEIOU", i);
                j = s.find_last_of("aeiouAEIOU", j);
                if (i < j) {
                    swap(s[i++], s[j--]);
                }
            }
            return s;
        }
    };
</p>


