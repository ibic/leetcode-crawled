---
title: "Design Circular Queue"
weight: 774
#id: "design-circular-queue"
---
## Description
<div class="description">
<p>Design your implementation of the circular queue. The circular queue is a linear data structure in which the operations are performed based on FIFO (First In First Out) principle and the last position is connected back to the first position to make a circle. It is also called &quot;Ring Buffer&quot;.</p>

<p>One of the benefits of the circular queue is that we can make use of the spaces in front of the queue. In a normal queue, once the queue becomes full, we cannot insert the next element even if there is a space in front of the queue. But using the circular queue, we can use the space to store new values.</p>

<p>Your implementation should support following operations:</p>

<ul>
	<li><code>MyCircularQueue(k)</code>: Constructor, set the size of the queue to be k.</li>
	<li><code>Front</code>: Get the front item from the queue. If the queue is empty, return -1.</li>
	<li><code>Rear</code>: Get the last item from the queue. If the queue is empty, return -1.</li>
	<li><code>enQueue(value)</code>: Insert an element into the circular queue. Return true if the operation is successful.</li>
	<li><code>deQueue()</code>: Delete an element from the circular queue. Return true if the operation is successful.</li>
	<li><code>isEmpty()</code>: Checks whether the circular queue is empty or not.</li>
	<li><code>isFull()</code>: Checks whether the circular queue is full or not.</li>
</ul>

<p>&nbsp;</p>

<p><strong>Example:</strong></p>

<pre>
MyCircularQueue circularQueue = new MyCircularQueue(3); // set the size to be 3
circularQueue.enQueue(1); &nbsp;// return true
circularQueue.enQueue(2); &nbsp;// return true
circularQueue.enQueue(3); &nbsp;// return true
circularQueue.enQueue(4); &nbsp;// return false, the queue is full
circularQueue.Rear(); &nbsp;// return 3
circularQueue.isFull(); &nbsp;// return true
circularQueue.deQueue(); &nbsp;// return true
circularQueue.enQueue(4); &nbsp;// return true
circularQueue.Rear(); &nbsp;// return 4
</pre>
&nbsp;

<p><strong>Note:</strong></p>

<ul>
	<li>All values will be in the range of [0, 1000].</li>
	<li>The number of operations will be in the range of&nbsp;[1, 1000].</li>
	<li>Please do not use the built-in Queue library.</li>
</ul>

</div>

## Tags
- Design (design)
- Queue (queue)

## Companies
- Amazon - 7 (taggedByAdmin: false)
- Facebook - 6 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Cloudera - 4 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Airbnb - 2 (taggedByAdmin: false)
- Splunk - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Array

**Intuition**

Based on the description of the problem, an intuitive data structure that meets all the requirements would be a **_ring_** where the head and the tail are adjacent to each other.

However, there does not exist a ring data structure in any programming language. A similar data structure at our disposal is the one called **_Array_** which is a collection of elements that reside continuously in one dimensional space.

The essence of many design problems, is how one can build more advanced data structures with the basic building blocks such as Array.

>In this case, to build a circular queue, we could form a _virtual_ ring structure with the Array, via the manipulation of index.

Given a fixed size array, any of the elements could be considered as a head in a queue. As long as we know the length of the queue, we then can instantly locate its tail, based on the following formula:

$$
\text{tailIndex} = (\text{headIndex} + \text{count} - 1) \mod \text{capacity}
$$

where the variable `capacity` is the size of the array, the `count` is the length of the queue and the `headIndex` and `tailIndex` are the indice of head and tail elements respectively in the array. Here we showcase a few examples how a circular queue could reside in a fixed size array.

![pic](../Figures/622/622_queue_with_array.png)


**Algorithm**

The procedure to design a data structure lies essentially on how we design the _attributes_ within the data structure.

One of the traits of a good design is to have as less attributes as possible, which arguably could bring several benefits. 

- Less attributes usually implies little or no redundancy among the attributes.
<br/>
- The less redundant the attributes are, the simpler the manipulation logic, which eventually could be less error-prone.
</br>
- Less attributes also requires less space and therefore it could also bring efficiency to the runtime performance.

_However, it is not advisable to seek for the minimum set of attributes._ Sometimes, a bit of redundancy could help with the time complexity. After all, like many other problems, we are trying to strike a balance between the space and the time.

Following the above principles, here we give a list of attributes and the thoughts behind each attribute.

- `queue`: a fixed size array to hold the elements for the circular queue.
</br>
- `headIndex`: an integer which indicates the current head element in the circular queue.
</br>
- `count`: the current length of the circular queue, _i.e._ the number of elements in the circular queue. Together with the `headIndex`, we could locate the current tail element in the circular queue, based on the formula we gave previously. Therefore, we choose not to add another attribute for the index of tail.
<br/>
- `capacity`: the capacity of the circular queue, _i.e._ the maximum number of elements that can be hold in the queue. One might argument that it is not absolutely necessary to add this attribute, since we could obtain the capacity from the `queue` attribute. It is true. Yet, since we would frequently use this `capacity` in our algorithm, we choose to keep it as an attribute, instead of invoking function `len(queue)` in Python at every occasion. Though in other programming languages such as Java, it might be more efficient to omit this attribute, since it is part of the attributes (`queue.length`) in Java array. _Note: for the sake of consistency, we keep this attribute for all implementations._


<iframe src="https://leetcode.com/playground/3Lj3CViQ/shared" frameBorder="0" width="100%" height="500" name="3Lj3CViQ"></iframe>

**Complexity**

- Time complexity: $$\mathcal{O}(1)$$. All of the methods in our circular data structure is of constant time complexity.
<br/>
- Space Complexity: $$\mathcal{O}(N)$$. The overall space complexity of the data structure is linear, where $$N$$ is the pre-assigned capacity of the queue. _However, it is worth mentioning that the memory consumption of the data structure remains as its pre-assigned capacity during its entire life cycle._


**Improvement: Thread-Safe**

One might be happy with the above implementation, after all it meets all the requirements of the problem.

>As a followup question though, an interviewer might ask one to point out a _potential defect_ about the implementation and the solution.

This time, it is not about the space or time complexity, but **_concurrency_**. Our circular queue is NOT _thread-safe_. One could end up with _corrupting_ our data structure in a multi-threaded environment.

For example, here is an execution sequence where we exceed the designed capacity of the queue and overwrite the tail element undesirably.

![pic](../Figures/622/622_concurrency.png)

The above scenario is called race conditions as described in the problem of [Print in Order](https://leetcode.com/problems/print-in-order/solution/). One can find more [concurrency problems](https://leetcode.com/problemset/concurrency/) to practice on LeetCode.

There are several ways to mitigate the above concurrency problem. 
Take the function `enQueue(int value)` as an example, we show how we can make the function thread-safe in the following implementation:

<iframe src="https://leetcode.com/playground/ezgQwy7x/shared" frameBorder="0" width="100%" height="500" name="ezgQwy7x"></iframe>

With the protection of locks, we now feel more confident to apply our circular queue in critical scenarios.

The above improvement does not alter the time and space complexity of the original data structure, though overall the thread-safe measures do incur some extra costs.
<br/>
<br/>

---

#### Approach 2: Singly-Linked List

**Intuition**

Similar with Array, the _Linked List_ is another common building block for more advanced data structures.

>Different than a fixed size Array, a linked list could be more memory efficient, since it does not pre-allocate memory for unused capacity. 

With a singly-linked list, we could design a circular queue with the same time and space complexity as the approach with Array. But we could gain some memory efficiency, since we don't need to pre-allocate the memory upfront.

In the following graph, we show how the operations of `enQueue()` and `deQueue()` can be done via singly-linked list.

![pic](../Figures/622/622_queue_linked_list.png)


**Algorithm**

Here we give a list of attributes in our circular queue data structure and the thoughts behind each attribute.

- `capacity`: the maximum number of elements that the circular queue will hold.
<br/>
- `head`: the reference to the head element in the queue.
</br>
- `count`: the current length of the queue. This is a critical attribute that helps us to do the boundary check in each method.
<br/>
- `tail`: the reference to the tail element in the queue. Unlike the Array approach, we need to explicitly keep the reference to the tail element. Without this attribute, it would take us $$\mathcal{O}(N)$$ time complexity to locate the tail element from the head element.
</br>

<iframe src="https://leetcode.com/playground/sMsMoq33/shared" frameBorder="0" width="100%" height="500" name="sMsMoq33"></iframe>


**Complexity**

- Time complexity: $$\mathcal{O}(1)$$ for each method in our circular queue.
<br/>
- Space Complexity: The upper bound of the memory consumption for our circular queue would be $$\mathcal{O}(N)$$, same as the Array approach. However, it should be more memory efficient as we discussed in the intuition section.
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise Java using array
- Author: climberig
- Creation Date: Sat Jul 14 2018 10:15:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 14:27:13 GMT+0800 (Singapore Standard Time)

<p>
```java
class MyCircularQueue {
        final int[] a;
        int front, rear = -1, len = 0;

        public MyCircularQueue(int k) { a = new int[k];}

        public boolean enQueue(int val) {
            if (!isFull()) {
                rear = (rear + 1) % a.length;
                a[rear] = val;
                len++;
                return true;
            } else return false;
        }

        public boolean deQueue() {
            if (!isEmpty()) {
                front = (front + 1) % a.length;
                len--;
                return true;
            } else return false;
        }

        public int Front() { return isEmpty() ? -1 : a[front];}

        public int Rear() {return isEmpty() ? -1 : a[rear];}

        public boolean isEmpty() { return len == 0;}

        public boolean isFull() { return len == a.length;}
    }
</p>


### Python3 with a single list; beats 100%
- Author: reddddddd
- Creation Date: Thu Sep 20 2018 06:47:52 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 27 2018 04:49:38 GMT+0800 (Singapore Standard Time)

<p>
The idea is to use a single list of fixed size `k` and keep the references to `front` and `rear` indices in the array. Since we don\'t deal with any of the memory allocation/deletion/pointer manipulation, this solution is very fast IMO

```python
class MyCircularQueue:

    def __init__(self, k):
        self.size = 0
        self.max_size = k
        self.t = [0]*k
        self.front = self.rear = -1
        
    def enQueue(self, value):
        if self.size == self.max_size: return False
        else:
            if self.rear == -1:
                self.rear = self.front = 0
            else:
                self.rear = (self.rear + 1)%self.max_size
            self.t[self.rear] = value
            self.size += 1
            return True
        
    def deQueue(self):
        if self.size == 0: return False
        if self.front == self.rear:
            self.front = self.rear = -1
        else:
            self.front = (self.front + 1)%self.max_size
        self.size -= 1
        return True
        

    def Front(self):
        return self.t[self.front] if self.size != 0 else -1

    def Rear(self):
        return self.t[self.rear] if self.size != 0 else -1

    def isEmpty(self):
        return self.size == 0

    def isFull(self):
        return self.size == self.max_size
```
</p>


### Straightforward Implementation in C++ [20ms]
- Author: NiYiWeiNe
- Creation Date: Wed Jul 25 2018 19:36:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 24 2018 10:58:16 GMT+0800 (Singapore Standard Time)

<p>
```
class MyCircularQueue {
public:
    /** Initialize your data structure here. Set the size of the queue to be k. */
    MyCircularQueue(int k) {
        data.resize(k);
        head = 0;
        tail = 0;
        reset = true;
    }
    
    /** Insert an element into the circular queue. Return true if the operation is successful. */
    bool enQueue(int value) {
        if (isFull()) return false;
        // update the reset value when first enqueue happens
        if (head == tail && reset) reset = false;
        data[tail] = value;
        tail = (tail + 1) % data.size();
        return true;
    }
    
    /** Delete an element from the circular queue. Return true if the operation is successful. */
    bool deQueue() {
        if (isEmpty()) return false;
        head = (head + 1) % data.size();
        // update the reset value when last dequeue happens
        if (head == tail && !reset) reset = true; 
        return true;
    }
    
    /** Get the front item from the queue. */
    int Front() {
        if (isEmpty()) return -1;
        return data[head];
    }
    
    /** Get the last item from the queue. */
    int Rear() {
        if (isEmpty()) return -1;
        return data[(tail + data.size() - 1) % data.size()];
    }
    
    /** Checks whether the circular queue is empty or not. */
    bool isEmpty() {
        if (tail == head && reset) return true;
        return false;
    }
    
    /** Checks whether the circular queue is full or not. */
    bool isFull() {
        if (tail == head && !reset) return true;
        return false;
    }
private:
    vector<int> data;
    int head;
    int tail;
    // reset is the mark when the queue is empty
    // to differentiate from queue is full
    // because in both conditions (tail == head) stands
    bool reset;
};

/**
 * Your MyCircularQueue object will be instantiated and called as such:
 * MyCircularQueue obj = new MyCircularQueue(k);
 * bool param_1 = obj.enQueue(value);
 * bool param_2 = obj.deQueue();
 * int param_3 = obj.Front();
 * int param_4 = obj.Rear();
 * bool param_5 = obj.isEmpty();
 * bool param_6 = obj.isFull();
 */
</p>


