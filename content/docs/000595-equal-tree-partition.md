---
title: "Equal Tree Partition"
weight: 595
#id: "equal-tree-partition"
---
## Description
<div class="description">
<p>
Given a binary tree with <code>n</code> nodes, your task is to check if it's possible to partition the tree to two trees which have the equal sum of values after removing <b>exactly</b> one edge on the original tree.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b>     
    5
   / \
  10 10
    /  \
   2   3

<b>Output:</b> True
<b>Explanation:</b> 
    5
   / 
  10
      
Sum: 15

   10
  /  \
 2    3

Sum: 15
</pre>
</p>


<p><b>Example 2:</b><br />
<pre>
<b>Input:</b>     
    1
   / \
  2  10
    /  \
   2   20

<b>Output:</b> False
<b>Explanation:</b> You can't split the tree into two trees with equal sum after removing exactly one edge on the tree.
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The range of tree node value is in the range of [-100000, 100000].</li>
<li>1 <= n <= 10000</li>
</ol>
</p>
</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Depth-First Search [Accepted]

**Intuition and Algorithm**

After removing some edge from `parent` to `child`, (where the `child` cannot be the original `root`) the subtree rooted at `child` must be half the sum of the entire tree.

Let's record the sum of every subtree.  We can do this recursively using depth-first search.  After, we should check that half the sum of the entire tree occurs somewhere in our recording (and not from the total of the entire tree.)

Our careful treatment and analysis above prevented errors in the case of these trees:
```python
  0
 / \
-1  1

 0
  \
   0
```

<iframe src="https://leetcode.com/playground/bY5XPeuF/shared" frameBorder="0" width="100%" height="378" name="bY5XPeuF"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ where $$N$$ is the number of nodes in the input tree.  We traverse every node.

* Space Complexity: $$O(N)$$, the size of `seen` and the implicit call stack in our DFS.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] Simple solution with only one HashMap<>.
- Author: caihao0727mail
- Creation Date: Sun Aug 20 2017 12:44:39 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 18 2018 09:00:03 GMT+0800 (Singapore Standard Time)

<p>
The idea is to use a hash table to record all the different sums of each subtree in the tree. If the total sum of the tree is ```sum```, we just need to check if the hash table constains ```sum/2```. 

The following code has the correct result at a special case when the tree is ```[0,-1,1]```, which many solutions dismiss. I think this test case should be added.  

Java version:
```
    public boolean checkEqualTree(TreeNode root) {
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        int sum = getsum(root, map);
        if(sum == 0)return map.getOrDefault(sum, 0) > 1;
        return sum%2 == 0 && map.containsKey(sum/2);
    }
    
    public int getsum(TreeNode root, Map<Integer, Integer> map ){
        if(root == null)return 0;
        int cur = root.val + getsum(root.left, map) + getsum(root.right, map);
        map.put(cur, map.getOrDefault(cur,0) + 1);
        return cur;
    }
        
```

C++ version:
```
    bool checkEqualTree(TreeNode* root) {
        unordered_map<int, int> map;
        int sum = getsum(root, map);
        if(sum == 0)return map[sum] > 1;
        return sum%2 == 0 && map.count(sum/2);
    }
    
    int getsum(TreeNode* root,  unordered_map<int, int>& map){
        if(root == NULL)return 0;
        int cur = root->val + getsum(root->left, map) + getsum(root->right, map);
        map[cur]++;
        return cur;
    }
    
```
</p>


### Logical Thinking with Java Code Beats 97.25%
- Author: GraceMeng
- Creation Date: Sat Jul 14 2018 11:14:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 14 2018 11:14:32 GMT+0800 (Singapore Standard Time)

<p>
**Logical Thinking**
It is inductive that we get the `totalSum` of the tree first, then `totalSum / 2` should be the target sum for both tree partitions.
The tree partition mentioned above is actually a subtree. So we keep track of all subtree sums while `getTotalSum()`.
Please note that, `totalSum / 2` cannot be the sum of the whole tree, for example,
```
   0
  / \
-1   1
```
the tree above will return false. Thus, we remove the last element of the list `subtreeSum`.

**Clear Java Code**
```
    List<Integer> subtreeSum;

    public boolean checkEqualTree(TreeNode root) {
        subtreeSum = new LinkedList<>();
        int totalSum = getTotalSum(root);
        if (totalSum % 2 != 0) {
            return false;
        }
        subtreeSum.remove(subtreeSum.size() - 1);
        return subtreeSum.contains(totalSum / 2);
    }

    private int getTotalSum(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int totalSum = root.val + getTotalSum(root.left) + getTotalSum(root.right);
        subtreeSum.add(totalSum);
        return totalSum;
    }
```
**I would appreciate your VOTE UP ;)**
</p>


### Two Pythons...
- Author: StefanPochmann
- Creation Date: Sun Aug 20 2017 14:58:05 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 12 2018 13:32:56 GMT+0800 (Singapore Standard Time)

<p>
Cutting an edge means cutting off a proper subtree (i.e., a subtree but not the whole tree). I collect the sums of these proper subtrees in a set and check whether half the total tree sum is a possible cut.

    def checkEqualTree(self, root):
        def sum(node):
            if not node:
                return 0
            s = node.val + sum(node.left) + sum(node.right)
            if node is not root:
                cuts.add(s)
            return s
        cuts = set()
        return sum(root) / 2 in cuts

Alternatively, I collect all subtree sums in a *list* so that the whole tree's sum is at the end. No need for a hash set's searching speed, as I'm searching the collection only once.

    def checkEqualTree(self, root):
        def sum(root):
            if not root:
                return 0
            sums.append(root.val + sum(root.left) + sum(root.right))
            return sums[-1]
        sums = []
        sum(root)
        return sums.pop() / 2 in sums

Oh, an alternative ending (not sure what I like better):

        sums = []
        return sum(root) / 2 in sums[:-1]

Note: I used Python 3. In Python 2, change the `/ 2` to `/ 2.`.
</p>


