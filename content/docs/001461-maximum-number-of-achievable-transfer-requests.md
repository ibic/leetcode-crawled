---
title: "Maximum Number of Achievable Transfer Requests"
weight: 1461
#id: "maximum-number-of-achievable-transfer-requests"
---
## Description
<div class="description">
<p>We have <code>n</code> buildings numbered from <code>0</code> to <code>n - 1</code>. Each building has a number of employees. It&#39;s transfer season, and some employees want to change the building they reside in.</p>

<p>You are given an array <code>requests</code> where <code>requests[i] = [from<sub>i</sub>, to<sub>i</sub>]</code> represents an employee&#39;s request to transfer from building <code>from<sub>i</sub></code> to building <code>to<sub>i</sub></code>.</p>

<p><strong>All buildings are full</strong>, so a list of requests is achievable only if for each building, the <strong>net change in employee transfers is zero</strong>. This means the number of employees <strong>leaving</strong> is <strong>equal</strong> to the number of employees <strong>moving in</strong>. For example if <code>n = 3</code> and two employees are leaving building <code>0</code>, one is leaving building <code>1</code>, and one is leaving building <code>2</code>, there should be two employees moving to building <code>0</code>, one employee moving to building <code>1</code>, and one employee moving to building <code>2</code>.</p>

<p>Return <em>the maximum number of achievable requests</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/10/move1.jpg" style="width: 600px; height: 406px;" />
<pre>
<strong>Input:</strong> n = 5, requests = [[0,1],[1,0],[0,1],[1,2],[2,0],[3,4]]
<strong>Output:</strong> 5
<strong>Explantion:</strong> Let&#39;s see the requests:
From building 0 we have employees x and y and both want to move to building 1.
From building 1 we have employees a and b and they want to move to buildings 2 and 0 respectively.
From building 2 we have employee z and they want to move to building 0.
From building 3 we have employee c and they want to move to building 4.
From building 4 we don&#39;t have any requests.
We can achieve the requests of users x and b by swapping their places.
We can achieve the requests of users y, a and z by swapping the places in the 3 buildings.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/10/move2.jpg" style="width: 450px; height: 327px;" />
<pre>
<strong>Input:</strong> n = 3, requests = [[0,0],[1,2],[2,1]]
<strong>Output:</strong> 3
<strong>Explantion:</strong> Let&#39;s see the requests:
From building 0 we have employee x and they want to stay in the same building 0.
From building 1 we have employee y and they want to move to building 2.
From building 2 we have employee z and they want to move to building 1.
We can achieve all the requests. </pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 4, requests = [[0,3],[3,1],[1,2],[2,0]]
<strong>Output:</strong> 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 20</code></li>
	<li><code>1 &lt;= requests.length &lt;= 16</code></li>
	<li><code>requests[i].length == 2</code></li>
	<li><code>0 &lt;= from<sub>i</sub>, to<sub>i</sub> &lt; n</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Backtracking, Straightforward, No Bit-Masking
- Author: CHEN0426
- Creation Date: Sun Sep 27 2020 12:02:35 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 28 2020 14:58:06 GMT+0800 (Singapore Standard Time)

<p>
Iterating all possibilisties by backtracking. (Choose or Not Choose)
Count the incoming and outgoing paths for each building
Check if all the buildings have the count of 0 (means incoming and outgoing paths are balanced according to the question description)

Time Complexity: ```O(N   * 2 ^ R)```
```
class Solution {
    int max = 0;
    public int maximumRequests(int n, int[][] requests) {
        helper(requests, 0, new int[n], 0);
        return max;
    }
    
    private void helper(int[][] requests, int index, int[] count, int num) {
        // Traverse all n buildings to see if they are all 0. (means balanced)
        if (index == requests.length) {
            for (int i : count) {
                if (0 != i) {
                    return;
                }
            }
            max = Math.max(max, num);
            return;
        }
		// Choose this request
        count[requests[index][0]]++;
        count[requests[index][1]]--;
        helper(requests, index + 1, count, num + 1);
        count[requests[index][0]]--;
        count[requests[index][1]]++;
        
		// Not Choose the request
        helper(requests, index + 1, count, num);
    }
}
</p>


### [Python] Check All Combinations
- Author: lee215
- Creation Date: Sun Sep 27 2020 12:06:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 27 2020 15:55:37 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
We can brute forces all conbinations of requests,
and then check if it\'s achievable.
<br>

# **Explanation**
For each combination, use a `mask` to present the picks.
The `k`th bits means we need to satisfy the `k`th request.

If for all buildings, in degrees == out degrees,
it\'s achievable.
<br>

# **Complexity**
Time `O((N + R) * 2^R)`
Space `O(N)`
<br>

# Solution 1
**Python:**
```py
    def maximumRequests(self, n, requests):
        nr = len(requests)
        res = 0

        def test(mask):
            outd = [0] * n
            ind = [0] * n
            for k in xrange(nr):
                if (1 << k) & mask:
                    outd[requests[k][0]] += 1
                    ind[requests[k][1]] += 1
            return sum(outd) if outd == ind else 0

        for i in xrange(1 << nr):
            res = max(res, test(i))
        return res
```
<br>

# Solution 2: Using Combination
Based on solution from @uds5501
```py
    def maximumRequests(self, n, req):
        for k in range(len(req), 0, -1):
            for c in itertools.combinations(range(len(req)), k):
                degree = [0] * n
                for i in c:
                    degree[req[i][0]] -= 1
                    degree[req[i][1]] += 1
                if not any(degree):
                    return k
        return 0
```
</p>


### C++ | Simple solution with Comments  | 100% 100%
- Author: voidp
- Creation Date: Sun Sep 27 2020 14:51:22 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 28 2020 14:53:38 GMT+0800 (Singapore Standard Time)

<p>
```
    void recur(vector<vector<int>>& requests, vector<int>& capacity, int reqId, int reqProcessed, int& maxRequests)
    {
    	//Base case: Processed all the requests, wind up.
        if(reqId == requests.size())
        {
        	//Check if all the buildings are balanced ( 0 incoming-0 outgoing)
            for(auto c: capacity)
            {
                if(c != 0)
                    return;
            }            
            //All the building are balanced, check and update max Req.
			maxRequests = max(maxRequests, reqProcessed);                
            return;
        }
        
        //Check without processing this request.
        recur(requests, capacity, reqId+1, reqProcessed, maxRequests);
        
        //Check with processing this request.
        //Update incoming and outgoing building requests.
        capacity[requests[reqId][0]]--; capacity[requests[reqId][1]]++;
		
       	recur(requests, capacity, reqId+1, reqProcessed+1, maxRequests);
        
        //Rollback the changes.
        capacity[requests[reqId][0]]++;   capacity[requests[reqId][1]]--;
    }
    
    int maximumRequests(int n, vector<vector<int>>& requests) {        
        vector<int> capacity(n, 0);
        int maxRequests;
        //Recur and process the requests.
        recur(requests, capacity, 0, 0, maxRequests);
        return maxRequests;        
    }
```
</p>


