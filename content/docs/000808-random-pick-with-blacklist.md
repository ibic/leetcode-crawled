---
title: "Random Pick with Blacklist"
weight: 808
#id: "random-pick-with-blacklist"
---
## Description
<div class="description">
<p>Given a blacklist&nbsp;<code>B</code> containing unique integers&nbsp;from <code>[0, N)</code>, write a function to return a uniform random integer from <code>[0, N)</code> which is <strong>NOT</strong>&nbsp;in <code>B</code>.</p>

<p>Optimize it such that it minimizes the call to system&rsquo;s <code>Math.random()</code>.</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= N &lt;= 1000000000</code></li>
	<li><code>0 &lt;= B.length &lt; min(100000, N)</code></li>
	<li><code>[0, N)</code>&nbsp;does NOT include N. See <a href="https://en.wikipedia.org/wiki/Interval_(mathematics)" target="_blank">interval notation</a>.</li>
</ol>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: 
</strong><span id="example-input-1-1">[&quot;Solution&quot;,&quot;pick&quot;,&quot;pick&quot;,&quot;pick&quot;]
</span><span id="example-input-1-2">[[1,[]],[],[],[]]</span>
<strong>Output: </strong><span id="example-output-1">[null,0,0,0]</span>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: 
</strong><span id="example-input-2-1">[&quot;Solution&quot;,&quot;pick&quot;,&quot;pick&quot;,&quot;pick&quot;]
</span><span id="example-input-2-2">[[2,[]],[],[],[]]</span>
<strong>Output: </strong><span id="example-output-2">[null,1,1,1]</span>
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: 
</strong><span id="example-input-3-1">[&quot;Solution&quot;,&quot;pick&quot;,&quot;pick&quot;,&quot;pick&quot;]
</span><span id="example-input-3-2">[[3,[1]],[],[],[]]</span>
<strong>Output: </strong><span id="example-output-3">[null,0,0,2]</span>
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: 
</strong><span id="example-input-4-1">[&quot;Solution&quot;,&quot;pick&quot;,&quot;pick&quot;,&quot;pick&quot;]
</span><span id="example-input-4-2">[[4,[2]],[],[],[]]</span>
<strong>Output: </strong><span id="example-output-4">[null,1,3,1]</span>
</pre>

<p><strong>Explanation of Input Syntax:</strong></p>

<p>The input is two lists:&nbsp;the subroutines called&nbsp;and their&nbsp;arguments.&nbsp;<code>Solution</code>&#39;s&nbsp;constructor has two arguments,&nbsp;<code>N</code> and the blacklist <code>B</code>. <code>pick</code> has no arguments.&nbsp;Arguments&nbsp;are&nbsp;always wrapped with a list, even if there aren&#39;t any.</p>

</div>

## Tags
- Hash Table (hash-table)
- Binary Search (binary-search)
- Sort (sort)
- Random (random)

## Companies
- Two Sigma - 2 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Uber - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---

#### Approach 1: Whitelist

**Intuition**

The problem is trivial if we have a whitelist.

**Algorithm**

Create a whitelist by initializing a HashSet with all numbers $$[0, N)$$, removing all blacklisted numbers, and then storing the remaining numbers into a list.

<iframe src="https://leetcode.com/playground/ks2H3G2k/shared" frameBorder="0" width="100%" height="361" name="ks2H3G2k"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ preprocessing. $$O(1)$$ pick. Preprocessing is too slow to pass the time limit.
* Space Complexity: $$O(N)$$. <code>MLE</code> (Memory Limit Exceeded) will occur.

<br/>

---

#### Approach 2: Binary Search over Blacklist

**Intuition**

Given a sorted blacklist, we can quickly find the gap between the blacklist numbers where the k-th largest whitelist number would be located. This makes it easy to compute the k-th largest whitelist number.

**Algorithm**

Lets say that we are given a non-empty blacklist $$B$$ and need to figure out what the k-th [zero-based](https://en.wikipedia.org/wiki/Zero-based_numbering) largest whitelist number, hereafter called $$W[k]$$, is.

First, sort the blacklist.

Then, use binary search to find the largest blacklist number which is smaller than $$W[k]$$.

Initially, the search space is the entire blacklist, so $$\text{lo} = 0$$ and $$\text{hi} = \text{len}(B)-1$$.

Loop while $$\text{lo} \neq \text{hi}$$:

* 　$$\text{mid} = \frac{\text{lo} + \text{hi} + 1}{2}$$
* 　$$c = B[\text{mid}]-\text{mid}$$, the number of whitelist numbers less than $$B[\text{mid}]$$.
* If $$c > k$$, then $$B[\text{mid}]$$ is larger than $$W[k]$$. $$B[\text{mid}]$$ and larger blacklist numbers are no longer candidates, so $$\text{hi} = \text{mid}-1$$.
* If $$c \leq k$$, then $$B[\text{mid}]$$ is smaller than $$W[k]$$. Blacklist numbers smaller than $$B[\text{mid}]$$ are no longer candidates, so $$\text{lo} = \text{mid}$$.

At termination, the search space will narrow down to one blacklist number. If it is smaller than $$W[k]$$, it is the largest blacklist number smaller than $$W[k]$$. In this case, the equation for $$W[k]$$ is $$k + \text{lo} + 1$$. If it is larger than $$W[k]$$, no blacklist number is smaller than $$W[k]$$, so $$W[k]$$ is simply $$k$$.

Lastly, to get random whitelist number, randomly pick $$k$$ in $$[0, N-\text{len}(B))$$.

<iframe src="https://leetcode.com/playground/8EWTFwKE/shared" frameBorder="0" width="100%" height="497" name="8EWTFwKE"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(B\text{log}B)$$ preprocessing. $$O(\text{log}B)$$ pick.
* Space Complexity: $$O(B)$$. Or $$O(1)$$ if in-place sort is used and input array is not considered extra space.

<br/>

---

#### Approach 3: Virtual Whitelist

**Intuition**

Re-map all blacklist numbers in $$[0, N-\text{len}(B))$$ to whitelist numbers such that when we randomly pick a number from $$[0, N-\text{len}(B))$$, we actually randomly pick amongst all whitelist numbers.

For example, for $$N = 6$$ and $$B = [0, 2, 3]$$ a remapping could look like this:

<center>
    <img src="../Figures/864/864_Virtual_Whitelist.png" alt="864_Virtual_Whitelist" style="height: 300px;"/>
</center>

**Algorithm**

Split $$B$$ into two blacklists, $$X$$ and $$Y$$, such that $$X$$ contains all blacklist numbers less than $$N-\text{len}(B)$$ and $$Y$$ contains the rest.

Use $$Y$$ to create $$W$$, a list of all whitelist numbers in $$[N-\text{len(B)}, N)$$. Approach 1 describes an efficient way to create this whitelist.

Define a HashMap $$M$$, where $$M[i] = i$$ by default (when there is nothing assigned to $$M[i]$$ yet), but $$M[i]$$ can also be assigned some other value.

Now, iterate through all numbers in $$X$$, assigning $$M[X[i]] = W[i]$$. Note that $$\text{len}(X) == \text{len}(W)$$.

　$$M[0] ... M[N-\text{len}(B)-1]$$ now maps to all whitelist numbers, so we can randomly pick in $$[0, N-\text{len}(B))$$ to get a random whitelist number.



The implementation below optimizes this algorithm in various ways, but the overall idea remains the same.

<iframe src="https://leetcode.com/playground/ezdrcp8a/shared" frameBorder="0" width="100%" height="463" name="ezdrcp8a"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(B)$$ preprocessing. $$O(1)$$ pick.
* Space Complexity: $$O(B)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(B) / O(1), HashMap
- Author: CAFEBABY
- Creation Date: Wed Jul 04 2018 06:49:51 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 11:15:54 GMT+0800 (Singapore Standard Time)

<p>
Suppose N=10, blacklist=[3, 5, 8, 9], re-map 3 and 5 to 7 and 6.
![image](https://s3-lc-upload.s3.amazonaws.com/users/cafebaby/image_1530657902.png)

```
class Solution {
    
    // N: [0, N)
    // B: blacklist
    // B1: < N
    // B2: >= N
    // M: N - B1
    int M;
    Random r;
    Map<Integer, Integer> map;

    public Solution(int N, int[] blacklist) {
        map = new HashMap();
        for (int b : blacklist) // O(B)
            map.put(b, -1);
        M = N - map.size();
        
        for (int b : blacklist) { // O(B)
            if (b < M) { // re-mapping
                while (map.containsKey(N - 1))
                    N--;
                map.put(b, N - 1);
                N--;
            }
        }
        
        r = new Random();
    }
    
    public int pick() {
        int p = r.nextInt(M);
        if (map.containsKey(p))
            return map.get(p);
        return p;
    }
}
```

</p>


### Super Simple Python AC w/ Remapping
- Author: austinschwartz
- Creation Date: Sun Jul 08 2018 00:58:13 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 27 2018 21:49:01 GMT+0800 (Singapore Standard Time)

<p>
Treat the first N - |B| numbers as those we can pick from. Iterate through the blacklisted numbers and map each of them to to one of the remaining non-blacklisted |B| numbers

For picking, just pick a random uniform int in 0, N - |B|. If its not blacklisted, return the number. If it is, return the number that its mapped to
```
import random

class Solution:
    def __init__(self, N, blacklist):
        blacklist = sorted(blacklist)
        self.b = set(blacklist)
        self.m = {}
        self.length = N - len(blacklist)
        j = 0
        for i in range(self.length, N):
            if i not in self.b:
                self.m[blacklist[j]] = i
                j += 1

    def pick(self):
        i = random.randint(0, self.length - 1)
        return self.m[i] if i in self.m else i
```
</p>


### [C++] DO NOT use rand() after C++11 !
- Author: zhoubowei
- Creation Date: Sat Jul 07 2018 13:57:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 21:46:56 GMT+0800 (Singapore Standard Time)

<p>
It is not recommand to use `rand()` when N is large.
https://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution

Other solutions which use `rand()/M` are wrong. 
`rand()` generates random numbers in [0, RAND_MAX], where `RAND_MAX` depends on the complier.
Usually, `RAND_MAX` is 32767 (in leetcode it is 2147483647), which could not generate numbers larger than it. In this problem, `N` could be 1000000000, so we cannot directly use `rand()/M` here.

Another problem is, even if N is less than 10000, it is also not accurate to use `rand()/M`.
For example, use `rand()/10000` to generate numbers in [0,9999]. It is more likely to generate a number in [0, 2767] than in [2868, 9999]:
```
[0,2767] % 10000->[0,2767]
[2768,9999] % 10000->[2768,9999]
[10000,12767] % 10000->[0,2767]
[12768,19999] % 10000->[2768,9999]
[20000,22767] % 10000->[0,2767]
[22768,29999] % 10000->[2768,9999]
[30000,32767] % 10000->[0,2767]
```

Here is my code, O(b log b) construction and O(log b) pick. (not the fastest, see other solutions)

```
class Solution {
public:
    vector<int> v;
    std::mt19937 gen;
    std::uniform_int_distribution<> dis;
    Solution(int N, vector<int> blacklist) {
        v = blacklist;
        sort(v.begin(), v.end());
        v.push_back(N);
        for (int i = 0; i < v.size(); i++) v[i] -= i;
        
        std::random_device rd;  //Will be used to obtain a seed for the random number engine
        gen = std::mt19937(rd()); //Standard mersenne_twister_engine seeded with rd()
        dis = std::uniform_int_distribution<>(0, N - v.size());
    }
    
    int pick() {
        int rnd = dis(gen);
        auto it = upper_bound(v.begin(), v.end(), rnd) - 1;
        int idx = it - v.begin();
        return idx + rnd + 1;
    }
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(N, blacklist);
 * int param_1 = obj.pick();
 */
```
</p>


