---
title: "Reconstruct a 2-Row Binary Matrix"
weight: 1197
#id: "reconstruct-a-2-row-binary-matrix"
---
## Description
<div class="description">
<p>Given the following details of a matrix with <code>n</code> columns and <code>2</code> rows :</p>

<ul>
	<li>The matrix is a binary matrix, which means each element in the matrix can be <code>0</code> or <code>1</code>.</li>
	<li>The sum of elements of the 0-th(upper) row is given as <code>upper</code>.</li>
	<li>The sum of elements of the 1-st(lower) row is given as <code>lower</code>.</li>
	<li>The sum of elements in the i-th column(0-indexed) is <code>colsum[i]</code>, where <code>colsum</code> is given as an integer array with length <code>n</code>.</li>
</ul>

<p>Your task is to reconstruct the matrix with <code>upper</code>, <code>lower</code> and <code>colsum</code>.</p>

<p>Return it as a 2-D integer array.</p>

<p>If there are more than one valid solution, any of them will be accepted.</p>

<p>If no valid solution exists, return an empty 2-D array.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> upper = 2, lower = 1, colsum = [1,1,1]
<strong>Output:</strong> [[1,1,0],[0,0,1]]
<strong>Explanation: </strong>[[1,0,1],[0,1,0]], and [[0,1,1],[1,0,0]] are also correct answers.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> upper = 2, lower = 3, colsum = [2,2,1,1]
<strong>Output:</strong> []
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> upper = 5, lower = 5, colsum = [2,1,2,0,1,0,1,2,0,1]
<strong>Output:</strong> [[1,1,1,0,1,0,0,1,0,0],[1,0,1,0,0,0,1,1,0,1]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= colsum.length &lt;= 10^5</code></li>
	<li><code>0 &lt;= upper, lower &lt;= colsum.length</code></li>
	<li><code>0 &lt;= colsum[i] &lt;= 2</code></li>
</ul>

</div>

## Tags
- Math (math)
- Greedy (greedy)

## Companies
- Grab - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java 5 lines
- Author: votrubac
- Creation Date: Mon Nov 11 2019 04:33:59 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 11 2019 10:10:48 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
If the column sum is `2` or `0`, the choice is obvius.

If it\'s `1`, we set the upper bit if `upper` is larger than `lower`, and lower bit otherwise.

**C++**
> See Java version below for less compacted version :)
```
vector<vector<int>> reconstructMatrix(int u, int l, vector<int>& cs) {
    vector<vector<int>> res(2, vector<int>(cs.size()));
    for (auto i = 0; i < cs.size(); u -= res[0][i], l -= res[1][i++]) {
        res[0][i] = cs[i] == 2 || (cs[i] == 1 && l < u);
        res[1][i] = cs[i] == 2 || (cs[i] == 1 && !res[0][i]);
    }
    return u == 0 && l == 0 ? res : vector<vector<int>>();
}
```
**Java**
```
public List<List<Integer>> reconstructMatrix(int u, int l, int[] cs) {
    boolean[][] res = new boolean[2][cs.length];
    for (int i = 0; i < cs.length; ++i) {
        res[0][i] = cs[i] == 2 || (cs[i] == 1 && l < u);
        res[1][i] = cs[i] == 2 || (cs[i] == 1 && !res[0][i]);
        u -= res[0][i] ? 1 : 0;
        l -= res[1][i] ? 1 : 0;
    }
    return l == 0 && u == 0 ? new ArrayList(Arrays.asList(res[0], res[1])) : new ArrayList();    
}
```
</p>


### Detailed Explanation using Greedy Approach
- Author: Just__a__Visitor
- Creation Date: Sun Nov 10 2019 12:12:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 10 2019 13:21:24 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
* First, intialize both the rows as `0`. Now, fill the indices where the vertical column sum is `2` (as they don\'t have a choice). So, now we only need to make choices for the columns with sum `1`. Find out the current sum of the first row. Compare it with the required amount. If the difference becomes negative, it means that there is no solution. Else, greedily fill the required `1s` in the top row and the remaining `1s` in the bottom row. Finally, check if the sum of the bottom row equals `lower`

```cpp
class Solution
{
public:
    vector<vector<int>> reconstructMatrix(int upper, int lower, vector<int>& colsum);
};

vector<vector<int>> Solution :: reconstructMatrix(int upper, int lower, vector<int>& colsum)
{
    int n = colsum.size();
    vector<vector<int>> mat(2, vector<int> (n, 0));
    
    for(int i = 0; i < n; i++)
        if(colsum[i] == 2)
            mat[0][i] = 1, mat[1][i] = 1;
    
    auto& first_row = mat[0];
    int current_upper_sum = accumulate(first_row.begin(), first_row.end(), 0);
    
    int diff = upper - current_upper_sum;
    
    if(diff < 0)
        return vector<vector<int>>();
    
    for(int i = 0; i < n; i++)
    {
        if(colsum[i] == 1)
        {
            if(diff > 0)
                mat[0][i] = 1, diff--;
            else 
                mat[1][i] = 1;
        }
    }
    
    auto& second_row = mat[1];
    int current_lower_sum = accumulate(second_row.begin(), second_row.end(), 0);
    
    if(current_lower_sum != lower)
        return vector<vector<int>> ();
    
    return mat;
        
}
```
</p>


### O(n) time Java Solution Easy to understand with Comments and Explaination
- Author: manrajsingh007
- Creation Date: Sun Nov 10 2019 12:02:53 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 11 2019 16:10:34 GMT+0800 (Singapore Standard Time)

<p>
We count the number of columns for which we need 1 in both rows(colsum[i] == 2), similarly we count the number of columns for which we need column-sum as 1 and column-sum as 0.


For the columns where we need the column-sum as 2, we definitely know that we need 1 in both the rows, similarly if the column-sum is 0 we know we need to place 0s in both the rows.


For the case where we need column-sum as 1, we need to see if we can have a 1 in row1 or do we have to have a 1 in row2. 
For those cases I have done a precomputation and followed a somewhat greedy approach.
The number of columns where we need a 1 in row1 and 0 in row2 is say count1 (as mentioned in code). 
We start assigning values to the two rows now by iterating over each value in the colsum array. If we encounter a colsum[i] == 2 or colsum[i] == 0, we assign 1s to both the rows and 0s to both the rows respectively.
**For the cases where colsum[i] == 1, we check value of count1 variable which will tell us if we can assign a 1 to row1 or not. If value of count1 > 0, we can assign 1 to row1 and 0 to row2 and we simultaneously decrement count1. Else if count1 == 0, we assign a 0 to row1 and 1 to row2.**

```
class Solution {
    public List<List<Integer>> reconstructMatrix(int upper, int lower, int[] colsum) {
        
        int n = colsum.length;
        int sum0 = 0; // no. of columns with colsum 0
        int sum1 = 0; // no. of columns with colsum 1
        int sum2 = 0; // no. of columns with colsum 2
        
        for(int i = 0; i < n; i++){
            if(colsum[i] == 0) sum0 ++;
            else if(colsum[i] == 1) sum1 ++;
            else sum2 ++;
        }
        
        int count1 = upper - sum2; // no. of columns with 1 in 1st row and 0 in 2nd row
        int count2 = lower - sum2; // no. of columns with 0 in 1st row and 1 in 2nd row
        
        // check if arrangement is possible or not
        if(count1 < 0 || count2 < 0 || count1 + count2 != sum1) return new ArrayList<>();
        
        List<List<Integer>> ans = new ArrayList<>();
        for(int i = 0; i < 2; i++) ans.add(new ArrayList<>());
        
        for(int i = 0; i < n; i++){
            if(colsum[i] == 2){
                ans.get(0).add(1);
                ans.get(1).add(1);
            }
            else if(colsum[i] == 0){
                ans.get(0).add(0);
                ans.get(1).add(0);
            }
            else{
                if(count1 > 0){
                    count1 --;
                    ans.get(0).add(1);
                    ans.get(1).add(0);
                }
                else{
                    ans.get(0).add(0);
                    ans.get(1).add(1);
                }
            }
        }
        
        return ans;
    }
}
</p>


