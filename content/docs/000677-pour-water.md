---
title: "Pour Water"
weight: 677
#id: "pour-water"
---
## Description
<div class="description">
<p>
We are given an elevation map, <code>heights[i]</code> representing the height of the terrain at that index.  The width at each index is 1.  After <code>V</code> units of water fall at index <code>K</code>, how much water is at each index?
</p><p>
Water first drops at index <code>K</code> and rests on top of the highest terrain or water at that index.  Then, it flows according to the following rules:
<li>If the droplet would eventually fall by moving left, then move left.</li>
<li>Otherwise, if the droplet would eventually fall by moving right, then move right.</li>
<li>Otherwise, rise at it's current position.</li>
Here, "eventually fall" means that the droplet will eventually be at a lower level if it moves in that direction.
Also, "level" means the height of the terrain plus any water in that column.
</p><p>
We can assume there's infinitely high terrain on the two sides out of bounds of the array.  Also, there could not be partial water being spread out evenly on more than 1 grid block - each unit of water has to be in exactly one block.
<p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> heights = [2,1,1,2,1,2,2], V = 4, K = 3
<b>Output:</b> [2,2,2,3,2,2,2]
<b>Explanation:</b>
#       #
#       #
##  # ###
#########
 0123456    <- index

The first drop of water lands at index K = 3:

#       #
#   w   #
##  # ###
#########
 0123456    

When moving left or right, the water can only move to the same level or a lower level.
(By level, we mean the total height of the terrain plus any water in that column.)
Since moving left will eventually make it fall, it moves left.
(A droplet "made to fall" means go to a lower height than it was at previously.)

#       #
#       #
## w# ###
#########
 0123456    

Since moving left will not make it fall, it stays in place.  The next droplet falls:

#       #
#   w   #
## w# ###
#########
 0123456  

Since the new droplet moving left will eventually make it fall, it moves left.
Notice that the droplet still preferred to move left,
even though it could move right (and moving right makes it fall quicker.)

#       #
#  w    #
## w# ###
#########
 0123456  

#       #
#       #
##ww# ###
#########
 0123456  

After those steps, the third droplet falls.
Since moving left would not eventually make it fall, it tries to move right.
Since moving right would eventually make it fall, it moves right.

#       #
#   w   #
##ww# ###
#########
 0123456  

#       #
#       #
##ww#w###
#########
 0123456  

Finally, the fourth droplet falls.
Since moving left would not eventually make it fall, it tries to move right.
Since moving right would not eventually make it fall, it stays in place:

#       #
#   w   #
##ww#w###
#########
 0123456  

The final answer is [2,2,2,3,2,2,2]:

    #    
 ####### 
 ####### 
 0123456 
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> heights = [1,2,3,4], V = 2, K = 2
<b>Output:</b> [2,3,3,4]
<b>Explanation:</b>
The last droplet settles at index 1, since moving further left would not cause it to eventually fall to a lower height.
</pre>
</p>

<p><b>Example 3:</b><br />
<pre>
<b>Input:</b> heights = [3,1,3], V = 5, K = 1
<b>Output:</b> [4,4,4]
</pre>
</p>

<p><b>Note:</b><br><ol>
<li><code>heights</code> will have length in <code>[1, 100]</code> and contain integers in <code>[0, 99]</code>.</li>
<li><code>V</code> will be in range <code>[0, 2000]</code>.</li>
<li><code>K</code> will be in range <code>[0, heights.length - 1]</code>.</li>
</ol></p>
</div>

## Tags
- Array (array)

## Companies
- Airbnb - 7 (taggedByAdmin: true)
- Robinhood - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Simulation [Accepted]

**Intuition and Algorithm**

We attempt to perform the steps directly as described.

First, notice that an index with terrain or with water is indistinguishable with respect to the flow of water.  Thus, we can model `heights` as the total level of terrain and water directly as we perform our simulation.

When a droplet falls, we should check if it is possible for it to fall left.  For our left pointer `i = K`, if `i - 1` is in bounds and `heights[i - 1] <= heights[i]`, the water will fall to a candidate block in that direction.  We keep track of every time we actually fall at index `best`.  If we "eventually fall" (`best != K`) as described in the problem statement, then we will drop the water there.

Otherwise, (if moving left will not cause the droplet to eventually fall), we can perform a similar check for `i = K` going right, and otherwise the droplet stays in place.

For convenience, we will name the initial array `H = heights`.

<iframe src="https://leetcode.com/playground/j7amhN74/shared" frameBorder="0" width="100%" height="378" name="j7amhN74"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(V * N)$$, where $$N$$ is the length of `heights`.  For each of $$V$$ droplets, our while loop might iterate $$N$$ times.

* Space Complexity: $$O(1)$$ in additional space complexity.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Share my Java straightforward and concise solution
- Author: zxyperfect
- Creation Date: Mon Sep 17 2018 07:03:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 17 2018 07:03:31 GMT+0800 (Singapore Standard Time)

<p>
Idea is simple. Imagine water drop moves left, and then moves right, and then moves left to position K. The position it stops is where it will stay.
```
class Solution {
    public int[] pourWater(int[] heights, int V, int K) {
        for(int i = 0; i < V; i++) {
            int cur = K;
            // Move left
            while(cur > 0 && heights[cur] >= heights[cur - 1]) {
                cur--;
            }
            // Move right
            while(cur < heights.length - 1 && heights[cur] >= heights[cur + 1]) {
                cur++;
            }
            // Move left to K
            while(cur > K && heights[cur] >= heights[cur - 1]) {
                cur--;
            }
            heights[cur]++;
        }

        return heights;
    }
}
```
</p>


### [C++/Java/Python] O(V+N) time solution using 2 pointers and 2 stacks
- Author: gengyouchen
- Creation Date: Mon Jan 15 2018 04:58:06 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 15 2018 04:58:06 GMT+0800 (Singapore Standard Time)

<p>
(not yet finished writing all the text. But the picture is still useful for understanding the code)

## Motivation
The [official solution](https://leetcode.com/problems/pour-water/solution/#approach-1-simulation-accepted) propose an algorithm running at **O(VN)** time, where N is the size of `heights`.
It's acceptable because N is between `[1, 100]` for all the test cases.
However, **if N is very large**, this algorithm becomes extremely slow.

The reason why the [official solution](https://leetcode.com/problems/pour-water/solution/#approach-1-simulation-accepted) is slow is because when each droplet falls at `i=K`, for each direction (i.e. `d=-1` or `d=+1`), the scanning of candidate blocks always needs to re-start from `i=K`.
In other words, it doesn't re-use the previous droplets' scanning information wisely to reduce the next droplet's scanning cost.

To address the above problems, we propose a faster algorithm, which only takes **O(V+N)** time.

## Observation

Lets look at an example:
Input: `heights=[2,1,4,2,2,2,3,5,5,8,8,7,7,7,6,6,7,10], V=8, K=10`
Output: `[2,2,4,4,4,4,4,5,5,8,8,7,7,7,6,6,7,10]`

## Explanation
(not yet finished writing all the text. But the picture is still useful for understanding the code):


* During extending the boundary of each direction, if its height is falling, push its location into the stack.
* Pause extending the boundary when its height is rising.


While extending leftBoundary from 10 (initial value) to 3, we push the falling locations 8, 6, and 5 into leftFall stack.
While extending rightBoundary from 10 (initial value) to 15, we push the falling locations 11 and 14 into rightFall stack.
Because the top of leftFall stack is leftFall[2]=5, the next droplet will eventually fall\xa0at location 5.
![0_1516061241046_example_step0.png](/assets/uploads/files/1516061242236-example_step0-resized.png) 

When V=1, because height[5] == height[5+1], we must pop 5 from leftFall stack.
Also, because leftBoundary < 5, we must push 4 into leftFall stack.
![0_1516061259731_example_step1.png](/assets/uploads/files/1516061260254-example_step1-resized.png) 
![0_1516061277906_example_step2.png](/assets/uploads/files/1516061278299-example_step2-resized.png) 
![0_1516061291778_example_step3.png](/assets/uploads/files/1516061292279-example_step3-resized.png) 
![0_1516061303119_example_step4.png](/assets/uploads/files/1516061303502-example_step4-resized.png) 
![0_1516061313736_example_step5.png](/assets/uploads/files/1516061314078-example_step5-resized.png) 
![0_1516061323536_example_step6.png](/assets/uploads/files/1516061323864-example_step6-resized.png) 
![0_1516061333545_example_step7.png](/assets/uploads/files/1516061333991-example_step7-resized.png) 
![0_1516061344366_example_step8.png](/assets/uploads/files/1516061344734-example_step8-resized.png) 

**C++ Solution**
```
class Solution {
public:
    vector<int> pourWater(vector<int>& heights, int V, int K) {
        stack<int> leftFall, rightFall;
        int leftBoundary = K, rightBoundary = K;
        for (int step = 0; step < V; ++step) {
            while (leftBoundary > 0 &&
                   heights[leftBoundary - 1] <= heights[leftBoundary]) {
                --leftBoundary;
                if (heights[leftBoundary] < heights[leftBoundary + 1])
                    leftFall.push(leftBoundary);
            }
            while (rightBoundary < heights.size() - 1 &&
                   heights[rightBoundary + 1] <= heights[rightBoundary]) {
                ++rightBoundary;
                if (heights[rightBoundary] < heights[rightBoundary - 1])
                    rightFall.push(rightBoundary);
            }
            if (!leftFall.empty()) {
                int leftmostFall = leftFall.top();
                ++heights[leftmostFall];
                if (heights[leftmostFall] == heights[leftmostFall + 1])
                    leftFall.pop();
                if (leftmostFall > leftBoundary)
                    leftFall.push(leftmostFall - 1);
            } else if (!rightFall.empty()) {
                int rightmostFall = rightFall.top();
                ++heights[rightmostFall];
                if (heights[rightmostFall] == heights[rightmostFall - 1])
                    rightFall.pop();
                if (rightmostFall < rightBoundary)
                    rightFall.push(rightmostFall + 1);
            } else {
                ++heights[K];
                if (K > leftBoundary)
                    leftFall.push(K - 1);
                if (K < rightBoundary)
                    rightFall.push(K + 1);
            }
        }
        return heights;
    }
};
```
**Java Solution**
```
class Solution {
    public int[] pourWater(int[] heights, int V, int K) {
        Stack<Integer> leftFall = new Stack<Integer>();
        Stack<Integer> rightFall = new Stack<Integer>();
        int leftBoundary = K, rightBoundary = K;
        for (int step = 0; step < V; ++step) {
            while (leftBoundary > 0 &&
                   heights[leftBoundary - 1] <= heights[leftBoundary]) {
                --leftBoundary;
                if (heights[leftBoundary] < heights[leftBoundary + 1])
                    leftFall.push(leftBoundary);
            }
            while (rightBoundary < heights.length - 1 &&
                   heights[rightBoundary + 1] <= heights[rightBoundary]) {
                ++rightBoundary;
                if (heights[rightBoundary] < heights[rightBoundary - 1])
                    rightFall.push(rightBoundary);
            }
            if (!leftFall.empty()) {
                int leftmostFall = leftFall.peek();
                ++heights[leftmostFall];
                if (heights[leftmostFall] == heights[leftmostFall + 1])
                    leftFall.pop();
                if (leftmostFall > leftBoundary)
                    leftFall.push(leftmostFall - 1);
            } else if (!rightFall.empty()) {
                int rightmostFall = rightFall.peek();
                ++heights[rightmostFall];
                if (heights[rightmostFall] == heights[rightmostFall - 1])
                    rightFall.pop();
                if (rightmostFall < rightBoundary)
                    rightFall.push(rightmostFall + 1);
            } else {
                ++heights[K];
                if (K > leftBoundary)
                    leftFall.push(K - 1);
                if (K < rightBoundary)
                    rightFall.push(K + 1);
            }
        }
        return heights;
    }
}
```
**Python Solution**
```
class Solution(object):
    def pourWater(self, heights, V, K):
        leftFall, rightFall, leftBoundary, rightBoundary = [], [], K, K
        for step in xrange(0, V):
            while (leftBoundary > 0 and
                   heights[leftBoundary - 1] <= heights[leftBoundary]):
                leftBoundary -= 1
                if heights[leftBoundary] < heights[leftBoundary + 1]:
                    leftFall.append(leftBoundary)
            while (rightBoundary < len(heights) - 1 and
                   heights[rightBoundary + 1] <= heights[rightBoundary]):
                rightBoundary += 1
                if heights[rightBoundary] < heights[rightBoundary - 1]:
                    rightFall.append(rightBoundary)
            if leftFall:
                leftmostFall = leftFall[-1]
                heights[leftmostFall] += 1
                if heights[leftmostFall] == heights[leftmostFall + 1]:
                    leftFall.pop()
                if leftmostFall > leftBoundary:
                    leftFall.append(leftmostFall - 1)
            elif rightFall:
                rightmostFall = rightFall[-1]
                heights[rightmostFall] += 1
                if heights[rightmostFall] == heights[rightmostFall - 1]:
                    rightFall.pop()
                if rightmostFall < rightBoundary:
                    rightFall.append(rightmostFall + 1)
            else:
                heights[K] += 1
                if K > leftBoundary:
                    leftFall.append(K - 1)
                if K < rightBoundary:
                    rightFall.append(K + 1)
        return heights
```
</p>


### Share my Java Solution
- Author: alucard945
- Creation Date: Sun Dec 31 2017 12:03:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 16:56:58 GMT+0800 (Singapore Standard Time)

<p>
```
public int[] pourWater(int[] heights, int V, int K) {
        if (heights == null || heights.length == 0 || V == 0) {
            return heights;
        }
        int index;
        while (V > 0) {
            index = K;
            for (int i = K - 1; i >= 0; i--) {
                if (heights[i] > heights[index]) {
                    break;
                } else if (heights[i] < heights[index]) {
                    index = i;
                }
            }
            if (index != K) {
                heights[index]++;
                V--;
                continue;
            }
            for (int i = K + 1; i < heights.length; i++) {
                if (heights[i] > heights[index]) {
                    break;
                } else if (heights[i] < heights[index]) {
                    index = i;
                }
            }
            heights[index]++;
            V--;
        }
        return heights;
    }
```
</p>


