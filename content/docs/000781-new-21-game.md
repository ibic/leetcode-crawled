---
title: "New 21 Game"
weight: 781
#id: "new-21-game"
---
## Description
<div class="description">
<p>Alice plays the following game, loosely based on the card game &quot;21&quot;.</p>

<p>Alice starts with <code>0</code> points, and draws numbers while she has less than <code>K</code> points.&nbsp; During each draw, she gains an integer number of points randomly from the range <code>[1, W]</code>, where <code>W</code> is an integer.&nbsp; Each draw is independent and the outcomes have equal probabilities.</p>

<p>Alice stops drawing numbers when she gets <code>K</code> or more points.&nbsp; What is the probability&nbsp;that she has <code>N</code> or less points?</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>N = 10, K = 1, W = 10
<strong>Output: </strong>1.00000
<strong>Explanation: </strong> Alice gets a single card, then stops.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>N = 6, K = 1, W = 10
<strong>Output: </strong>0.60000
<strong>Explanation: </strong> Alice gets a single card, then stops.
In 6 out of W = 10 possibilities, she is at or below N = 6 points.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>N = 21, K = 17, W = 10
<strong>Output: </strong>0.73278</pre>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= K &lt;= N &lt;= 10000</code></li>
	<li><code>1 &lt;= W &lt;= 10000</code></li>
	<li>Answers will be accepted as correct if they are within <code>10^-5</code> of the correct answer.</li>
	<li>The judging time limit has been reduced for this question.</li>
</ol>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 3 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Dynamic Programming [Accepted]

**Intuition**

It is clear that the probability that Alice wins the game is only related to how many points `x` she starts the next draw with, so we can try to formulate an answer in terms of `x`.

**Algorithm**

Let `f(x)` be the answer when we already have `x` points.  When she has between `K` and `N` points, then she stops drawing and wins.  If she has more than `N` points, then she loses.

The key recursion is $$f(x) = (\frac{1}{W}) * (f(x+1) + f(x+2) + ... + f(x+W))$$  This is because there is a probability of $$\frac{1}{W}$$ to draw each card from $$1$$ to $$W$$.

With this recursion, we could do a naive dynamic programming solution as follows:

```python
#psuedocode
dp[k] = 1.0 when K <= k <= N, else 0.0
# dp[x] = the answer when Alice has x points
for k from K-1 to 0:
    dp[k] = (dp[k+1] + ... + dp[k+W]) / W
return dp[0]
```

This leads to a solution with $$O(K*W + (N-K))$$ time complexity, which isn't efficient enough.  Every calculation of `dp[k]` does $$O(W)$$ work, but the work is quite similar.

Actually, the difference is $$f(x) - f(x-1) = \frac{1}{W} \big( f(x+W) - f(x) \big)$$.  This allows us to calculate subsequent $$f(k)$$'s in $$O(1)$$ time, by maintaining the numerator $$S = f(x+1) + f(x+2) + \cdots + f(x+W)$$.

As we calculate each `dp[k] = S / W`, we maintain the correct value of this numerator $$S \Rightarrow S + f(k) - f(k+W)$$.

<iframe src="https://leetcode.com/playground/q6vdxri7/shared" frameBorder="0" width="100%" height="327" name="q6vdxri7"></iframe>

**Complexity Analysis**

* Time and Space Complexity:  $$O(N + W)$$.  
Note that we can reduce the time complexity to $$O(\max(K, W))$$ and the space complexity to $$O(W)$$ by only keeping track of the last $$W$$ values of `dp`, but it isn't required.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### One Pass DP O(N)
- Author: lee215
- Creation Date: Sun May 20 2018 11:25:37 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 23 2019 01:06:57 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**:
The same problems as "Climbing Stairs" or "Fibo Sequence".

## **Explanation**:
`dp[i]` is the probability that we get points `i` at some moment.
In another word:
`1 - dp[i]`is the probability that we skip the points `i`.

The do equation is that:
`dp[i] = sum(last W dp values) / W`

To get `Wsum = sum(last W dp values)`,
we can maintain a sliding window with size at most `W`.
<br>

## **Time Complexity**:
Time `O(N)`
Space `O(N)`, can be improve to `O(W)`
<br>

**C++:**
```cpp
    double new21Game(int N, int K, int W) {
        if (K == 0 || N >= K + W) return 1.0;
        vector<double> dp(N + 1);
        dp[0] = 1.0;
        double Wsum = 1.0, res = 0.0;
        for (int i = 1; i <= N; ++i) {
            dp[i] = Wsum / W;
            if (i < K) Wsum += dp[i]; else res += dp[i];
            if (i - W >= 0) Wsum -= dp[i - W];
        }
        return res;
    }
```

**Java:**
```java
    public double new21Game(int N, int K, int W) {
        if (K == 0 || N >= K + W) return 1;
        double dp[] = new double[N + 1],  Wsum = 1, res = 0;
        dp[0] = 1;
        for (int i = 1; i <= N; ++i) {
            dp[i] = Wsum / W;
            if (i < K) Wsum += dp[i]; else res += dp[i];
            if (i - W >= 0) Wsum -= dp[i - W];
        }
        return res;
    }
```
**Python:**
```py
    def new21Game(self, N, K, W):
        if K == 0 or N >= K + W: return 1
        dp = [1.0] + [0.0] * N
        Wsum = 1.0
        for i in range(1, N + 1):
            dp[i] = Wsum / W
            if i < K: Wsum += dp[i]
            if i - W >= 0: Wsum -= dp[i - W]
        return sum(dp[K:])
```
</p>


### Java O(K + W) DP solution with explanation
- Author: wangzi6147
- Creation Date: Sun May 20 2018 11:49:36 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 13:12:27 GMT+0800 (Singapore Standard Time)

<p>
Firstly I came up with a basic DP solution which cost `O((K + W) * W)` time and it runs TLE:

```
class Solution {
    public double new21Game(int N, int K, int W) {
        if (K == 0) {
            return 1;
        }
        int max = K + W - 1;
        double[] dp = new double[max + 1];
        dp[0] = 1;
        for (int i = 1; i <= max; i++) {
            for (int j = 1; j <= W; j++) {
                if (i - j >= 0 && i - j < K) {
                    dp[i] += dp[i - j] / W;
                }
            }
        }
        double result = 0;
        for (int i = K; i <= N; i++) {
            result += dp[i];
        }
        return result;
    }
}
```

Then I realize that the transition equation `dp[i] = (dp[i - W] + dp[i - W + 1] + ... + dp[i - 1]) / W` could be simplified to `dp[i] = (sum[i - 1] - sum[i - W - 1]) / W`.

Furthermore, if we use `dp[i]` to directly represent the `sum[i]`, we can get `dp[i] = dp[i - 1] + (dp[i - 1] - dp[i - W - 1]) / W`. This equation takes us to the final `O(K + W)` solution. Just take care with the beginning and the end of the array.

```
class Solution {
    public double new21Game(int N, int K, int W) {
        if (K == 0) {
            return 1;
        }
        int max = K + W - 1;
        double[] dp = new double[max + 1];
        dp[0] = 1;
        for (int i = 1; i <= max; i++) {
            dp[i] = dp[i - 1];
            if (i <= W) {
                dp[i] += dp[i - 1] / W;
            } else {
                dp[i] += (dp[i - 1] - dp[i - W - 1]) / W;
            }
            if (i > K) {
                dp[i] -= (dp[i - 1] - dp[K - 1]) / W;
            }
        }
        return dp[N] - dp[K - 1];
    }
}
```

</p>


### My take on how to reach at Solution
- Author: cooldown
- Creation Date: Mon May 21 2018 01:18:23 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 06:43:01 GMT+0800 (Singapore Standard Time)

<p>
Firstly, observe that when Alice is done playing, her points can be in range [K, K+W-1]. She can\'t get past K+W because she stops playing when she is at K or more.

Now, we will see how to find the probabilities to stay in the positions in the above range and store them in p[] array.

We need to know the probabilities of all elems in range [0,K] first. From there we can calculate for elems in range (K, K+W-1]

For positions  x <= K,
p[x] = (p[x-1] + p[x-2] + ..... + p[x-W]) * (1/W); (Probabilities of previous possible positions to get to x, multiplied by probability of choosing an elem to get to x)

For implementation, we can keep track past values using a single variable. For each new positions, we will subtract p[x-W-1] and add p[x-1]. Look at implementation below

For positions x > K,
p[x] = (p[K-1] + p[K-2] + .... + p[x - W]) * 1/W; (Since, we can\'t/shouldn\'t reach x from >= K positions)

\'\'\'
public double new21Game(int N, int K, int W) {
        
        if(N >= K+W-1) return 1.0; // all possibilities of positions after alice stops playing are from [K, K+W-1]
        
        double p[] = new double[K+W];
        double prob = 1/(W+0.0); // single elem probability
        
        double prev = 0;
        
        p[0] = 1; // Since we start from 0, initialize it to 1
        
	     //Until K
        for(int i = 1; i <= K; i++) {
            prev = prev - (i-W-1 >= 0 ? p[i - W -1] : 0) + p[i-1];
            p[i] = prev*prob;
        }
        
        double req = p[K];
        
	    // From K+1, we don\'t add the p[i-1] term here as it is >= K
        for(int i = K+1; i <= N; i++) {
            prev = prev - (i-W-1 >= 0 ? p[i - W -1] : 0);
            p[i] = prev * prob;
            req += p[i];
            //System.out.println(p[i]);
        }
        
        return req;
    }
\'\'\'

Please comment below, if you didn\'t understand something. I will try to clarify.

</p>


