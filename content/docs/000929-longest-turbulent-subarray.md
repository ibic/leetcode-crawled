---
title: "Longest Turbulent Subarray"
weight: 929
#id: "longest-turbulent-subarray"
---
## Description
<div class="description">
<p>A subarray <code>A[i], A[i+1], ..., A[j]</code>&nbsp;of <code>A</code> is said to be <em>turbulent</em> if and only if:</p>

<ul>
	<li>For <code>i &lt;= k &lt; j</code>, <code>A[k] &gt; A[k+1]</code> when <code>k</code> is odd, and <code>A[k] &lt; A[k+1]</code> when <code>k</code> is even;</li>
	<li><strong>OR</strong>, for <code>i &lt;= k &lt; j</code>, <code>A[k] &gt; A[k+1]</code> when <code>k</code> is even, and <code>A[k] &lt; A[k+1]</code> when <code>k</code> is odd.</li>
</ul>

<p>That is, the subarray is turbulent if the comparison sign flips between each adjacent pair of elements in the subarray.</p>

<p>Return the <strong>length</strong> of a&nbsp;maximum size turbulent subarray of A.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[9,4,2,10,7,8,8,1,9]</span>
<strong>Output: </strong><span id="example-output-1">5</span>
<strong>Explanation: </strong>(A[1] &gt; A[2] &lt; A[3] &gt; A[4] &lt; A[5])
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[4,8,12,16]</span>
<strong>Output: </strong><span id="example-output-2">2</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[100]</span>
<strong>Output: </strong><span id="example-output-3">1</span>
</pre>
</div>
</div>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 40000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 10^9</code></li>
</ol>
</div>

## Tags
- Array (array)
- Dynamic Programming (dynamic-programming)
- Sliding Window (sliding-window)

## Companies
- Amazon - 6 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Sliding Window

**Intuition**

Evidently, we only care about the comparisons between adjacent elements.  If the comparisons are represented by `-1, 0, 1` (for `<, =, >`), then we want the longest sequence of alternating `1, -1, 1, -1, ...` (starting with either `1` or `-1`).

These alternating comparisons form contiguous blocks.  We know when the next block ends: when it is the last two elements being compared, or when the sequence isn't alternating.

For example, take an array like `A = [9,4,2,10,7,8,8,1,9]`.  The comparisons are `[1,1,-1,1,-1,0,-1,1]`.  The blocks are `[1], [1,-1,1,-1], [0], [-1,1]`.

**Algorithm**

Scan the array from left to right.  If we are at the end of a block (last elements OR it stopped alternating), then we should record the length of that block as our candidate answer, and set the start of the new block as the next element.

<iframe src="https://leetcode.com/playground/3kKneXoj/shared" frameBorder="0" width="100%" height="378" name="3kKneXoj"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(N) time O(1) space
- Author: wangzi6147
- Creation Date: Sun Jan 20 2019 12:03:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 20 2019 12:03:33 GMT+0800 (Singapore Standard Time)

<p>
For each `A[i]`
`inc`: The length of current valid sequence which ends with two **increasing** numbers
`dec`: The length of current valid sequence which ends with two **decreasing** numbers

```
class Solution {
    public int maxTurbulenceSize(int[] A) {
        int inc = 1, dec = 1, result = 1;
        for (int i = 1; i < A.length; i++) {
            if (A[i] < A[i - 1]) {
                dec = inc + 1;
                inc = 1;
            } else if (A[i] > A[i - 1]) {
                inc = dec + 1;
                dec = 1;
            } else {
                inc = 1;
                dec = 1;
            }
            result = Math.max(result, Math.max(dec, inc));
        }
        return result;
    }
}
```
</p>


### C++/Java 6 lines, flip the sign
- Author: votrubac
- Creation Date: Sun Jan 20 2019 12:02:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 20 2019 12:02:43 GMT+0800 (Singapore Standard Time)

<p>
We use the counter (```cnt```) to track subarray size. Our counter is positive if we expect \'>\', and \'<\' otherwise. Obviously, for each turn we flip the sign.

If the comparison matches the counter sign (e.g. ```A[i] > A[i + 1]``` and ``` cnt > 0``` ), we increment (or decrement the negative) counter. Otherwise, we reset the counter to 1 (or -1). One edge case here is when two numbers are equal. We set the counter to zero in this case.
```
int maxTurbulenceSize(vector<int>& A, int res = 0) {
  for (auto i = 0, cnt = 0; i + 1 < A.size(); ++i, cnt *= -1) {
    if (A[i] > A[i + 1]) cnt = cnt > 0 ? cnt + 1 : 1;
    else if (A[i] < A[i + 1])  cnt = cnt < 0 ? cnt - 1 : -1;
    else cnt = 0;
    res = max(res, abs(cnt));
  }
  return res + 1;
}
```
Java version:
```
public int maxTurbulenceSize(int[] A) {
  int res = 0;
  for (int i = 0, cnt = 0; i + 1 < A.length; ++i, cnt *= -1) {
    if (A[i] > A[i + 1]) cnt = cnt > 0 ? cnt + 1 : 1;
    else if (A[i] < A[i + 1])  cnt = cnt < 0 ? cnt - 1 : -1;
    else cnt = 0;
    res = Math.max(res, Math.abs(cnt));
  }
  return res + 1;
}
```
</p>


### [Python/Java/C++] O(n) time O(1) space Simple and Clean solution
- Author: Twohu
- Creation Date: Sun Jan 20 2019 15:01:55 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 07 2019 10:11:14 GMT+0800 (Singapore Standard Time)

<p>
A subarray is turbulent if the comparison sign alternates between consecutive elements (ex. ```nums[0] < nums[1] > nums[2] < nums[3] > ...``` ). Looking at the structure of the array, this means every element of a turbulent subarray must belong to either a peak ```(A[i-2] < A[i-1] > A[i])``` or a valley ```(A[i-2] > A[i-1] < A[i])``` structure.

The algorithm works as follows. Keep track of the length of the longest run ending at index ```i```. This is tracked in a variable named ```clen```. If the last three elements form a peak or a valley, we can extend the previous run length by 1 (meaning ```clen += 1```). Otherwise, we can no longer extend this run and need to reset ```clen``` to the length of the longest run ending at index ```i```. This run length will be ```1``` if the previous and current elements are the same (Ex: ```[2,2,2]```), or ```2``` if the previous and current elements differ (Ex: ```[2,4,6]```). The answer is the length of the best run found.

## Python
```
def maxTurbulenceSize(self, A):
    best = clen = 0

    for i in range(len(A)):
        if i >= 2 and (A[i-2] > A[i-1] < A[i] or A[i-2] < A[i-1] > A[i]):
            clen += 1
        elif i >= 1 and A[i-1] != A[i]:
            clen = 2
        else:
            clen = 1
        best = max(best, clen)
    return best
```
## Java
```
public int maxTurbulenceSize(int[] A) {
    int best = 0, clen = 0;

    for(int i = 0; i < A.length; i++) {
        if(i >= 2 && ((A[i-2] > A[i-1] && A[i-1] < A[i]) ||
                      (A[i-2] < A[i-1] && A[i-1] > A[i])) ) {
            // If the last three elements are turbulent, increment run length by 1.
            clen++;
        } else if(i >= 1 && A[i-1] != A[i]) {
            // The last three elements are not turbulent, so we\'ll reset the run length.
            // If the previous and current elements are not equal, the new run length is 2.
            clen = 2;
        } else {
            // Otherwise, the new run length is 1.
            clen = 1;
        }
        best = Math.max(best, clen);
    }
    return best;    
}
```

## C++
```
int maxTurbulenceSize(vector<int>& A) {
    int best = 0, clen = 0;

    for(int i = 0; i < A.size(); ++i) {
        if (i >= 2 && ((A[i-2] > A[i-1] && A[i-1] < A[i]) ||
                       (A[i-2] < A[i-1] && A[i-1] > A[i])) ) {
            clen++;
        } else if (i >= 1 && A[i-1] != A[i]) {
            clen = 2;
        } else {
            clen = 1;
        }
        best = max(best, clen);
    }
    return best;
}
```

</p>


