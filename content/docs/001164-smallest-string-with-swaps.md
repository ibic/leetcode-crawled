---
title: "Smallest String With Swaps"
weight: 1164
#id: "smallest-string-with-swaps"
---
## Description
<div class="description">
<p>You are given a string <code>s</code>, and an array of pairs of indices in the string&nbsp;<code>pairs</code>&nbsp;where&nbsp;<code>pairs[i] =&nbsp;[a, b]</code>&nbsp;indicates 2 indices(0-indexed) of the string.</p>

<p>You can&nbsp;swap the characters at any pair of indices in the given&nbsp;<code>pairs</code>&nbsp;<strong>any number of times</strong>.</p>

<p>Return the&nbsp;lexicographically smallest string that <code>s</code>&nbsp;can be changed to after using the swaps.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;dcab&quot;, pairs = [[0,3],[1,2]]
<strong>Output:</strong> &quot;bacd&quot;
<strong>Explaination:</strong> 
Swap s[0] and s[3], s = &quot;bcad&quot;
Swap s[1] and s[2], s = &quot;bacd&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;dcab&quot;, pairs = [[0,3],[1,2],[0,2]]
<strong>Output:</strong> &quot;abcd&quot;
<strong>Explaination: </strong>
Swap s[0] and s[3], s = &quot;bcad&quot;
Swap s[0] and s[2], s = &quot;acbd&quot;
Swap s[1] and s[2], s = &quot;abcd&quot;</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;cba&quot;, pairs = [[0,1],[1,2]]
<strong>Output:</strong> &quot;abc&quot;
<strong>Explaination: </strong>
Swap s[0] and s[1], s = &quot;bca&quot;
Swap s[1] and s[2], s = &quot;bac&quot;
Swap s[0] and s[1], s = &quot;abc&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10^5</code></li>
	<li><code>0 &lt;= pairs.length &lt;= 10^5</code></li>
	<li><code>0 &lt;= pairs[i][0], pairs[i][1] &lt;&nbsp;s.length</code></li>
	<li><code>s</code>&nbsp;only contains lower case English letters.</li>
</ul>

</div>

## Tags
- Array (array)
- Union Find (union-find)

## Companies
- ByteDance - 4 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- PHONEPE - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (python3)
```python3
from typing import List
from collections import defaultdict


class DisjointSet:
    def __init__(self, n):
        self.makeSet(n)

    def makeSet(self, n):
        self.parent = [[i, 0] for i in range(n)]

    def union(self, i, j):
        ir = self.find(i)
        jr = self.find(j)
        if ir == jr:
            return

        irrank = self.parent[ir][1]
        jrrank = self.parent[jr][1]

        if irrank < jrrank:
            self.parent[ir][0] = jr
        elif irrank > jrrank:
            self.parent[jr][0] = ir
        else:
            self.parent[jr][0] = ir
            self.parent[ir][1] += 1

    def find(self, i):
        if self.parent[i][0] != i:
            self.parent[i][0] = self.find(self.parent[i][0])
        return self.parent[i][0]


class Solution:
    def smallestStringWithSwaps(self, s: str, pairs: List[List[int]]) -> str:
        sa = []
        lens = len(s)
        ds = DisjointSet(lens)
        for i, j in pairs:
            ds.union(i, j)
        cm = defaultdict(list)
        for i in range(lens):
            ip = ds.find(i)
            cm[ip].append(s[i])
        for _, li in cm.items():
            li.sort(reverse=True)
        for i in range(lens):
            sa.append(cm[ds.find(i)].pop())

        return ''.join(sa)

```

## Top Discussions
### Short Python Union find solution w/ Explanation
- Author: li-_-il
- Creation Date: Sun Sep 22 2019 12:01:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 22 2019 12:05:59 GMT+0800 (Singapore Standard Time)

<p>
The core of the idea is that if (0, 1) is an exchange pair and (0, 2) is an exchange pair, then any 2 in (0, 1, 2) can be exchanged.

This implies, we can build connected components where each component is a list of indices that can be exchanged with any of them. In Union find terms, we simply iterate through each pair, and do a union on the indices in the pair.
At the end of the union of all the pairs, we have built connected component of indices that can be exchanged with each other.
 
Then we build a sorted list of characters for every connected component.
 
The final step is, we iterate through all the indices, and for each index we locate its component id and find the sorted list correspondng to that component and grab the next lowest character from that list.

This way for every index, we find the lowest possible character that can be exchanged and fitted there.

```
 def smallestStringWithSwaps(self, s: str, pairs: List[List[int]]) -> str:
        class UF:
            def __init__(self, n): self.p = list(range(n))
            def union(self, x, y): self.p[self.find(x)] = self.find(y)
            def find(self, x):
                if x != self.p[x]: self.p[x] = self.find(self.p[x])
                return self.p[x]
        uf, res, m = UF(len(s)), [], defaultdict(list)
        for x,y in pairs: 
            uf.union(x,y)
        for i in range(len(s)): 
            m[uf.find(i)].append(s[i])
        for comp_id in m.keys(): 
            m[comp_id].sort(reverse=True)
        for i in range(len(s)): 
            res.append(m[uf.find(i)].pop())
        return \'\'.join(res)
```
</p>


### C++ with picture, union-find
- Author: votrubac
- Creation Date: Mon Sep 23 2019 07:29:42 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 09 2020 00:55:10 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
If a group of characters is interconnected by swap pairs, you can freely rearrange characters within that group.

In this example, we have two group of interconnected characters, so we can make rearrangements to achieve the smallest string as shown in the picture below.
```
"zdcyxbwa"
[[0,3],[4,6],[3,4],[1,7],[2,5],[5,7]]
```
![image](https://assets.leetcode.com/users/votrubac/image_1569195129.png)
# Solution
Identify groups using union-find. For each group, collect all its characters in a string.
> Note that in the disjoined set ```ds```, the find operation returns the parent index (```p```). We use that index to identify the group, and put all group characters into ```m[p]```.

Sort the string, then put the rearranged characters back to their respective positions in the group.
```
int find(vector<int>& ds, int i) {
  return ds[i] < 0 ? i : ds[i] = find(ds, ds[i]);
}
string smallestStringWithSwaps(string s, vector<vector<int>>& pairs) {
  vector<int> ds(s.size(), -1);
  vector<vector<int>> m(s.size());
  for (auto& p : pairs) {
    auto i = find(ds, p[0]), j = find(ds, p[1]);
    if (i != j) 
        ds[j] = i;
  }
  for (auto i = 0; i < s.size(); ++i) 
      m[find(ds, i)].push_back(i);
  for (auto &ids : m) {
    string ss = "";
    for (auto id : ids) 
        ss += s[id];
    sort(begin(ss), end(ss));
    for (auto i = 0; i < ids.size(); ++i) 
        s[ids[i]] = ss[i];
  }
  return s;
}
```
## Disjoint set optimizations
Note that, in the ```find``` function, we are using path compression. In other words, we "flatten" our hierarchy every time we search, so that elements references the parent index directly: ```ds[i] = find(ds, ds[i]);```. Without path compression, you will get TLE on the last test case. 

In addition, we can store the count of elements (negated) in the parent of the group. Then, when merging two groups, we add elements from the smaller group into the larger one. That way, we reduce the number of recursive calls for the ```find``` function to "flatten" newly added elements.

Below is the optimized solution (the changes are for ```if (i != j)```` condition). Note that, for this problem, this optimization is not that important (though the runtime went down from 140 to 128 ms for me) since we only have 26 elements. Though, it may help you quite a bit when you deal with larger groups.
```
int find(vector<int>& ds, int i) {
  return ds[i] < 0 ? i : ds[i] = find(ds, ds[i]);
}
string smallestStringWithSwaps(string s, vector<vector<int>>& pairs) {
  vector<int> ds(s.size(), -1);
  vector<vector<int>> m(s.size());
  for (auto& p : pairs) {
    auto i = find(ds, p[0]), j = find(ds, p[1]);
    if (i != j) {
        if (-ds[i] < -ds[j]) 
            swap(i, j);
        ds[i] += ds[j];
        ds[j] = i;
    }
  }
  for (auto i = 0; i < s.size(); ++i) 
      m[find(ds, i)].push_back(i);
  for (auto &ids : m) {
    string ss = "";
    for (auto id : ids) 
        ss += s[id];
    sort(begin(ss), end(ss));
    for (auto i = 0; i < ids.size(); ++i) 
        s[ids[i]] = ss[i];
  }
  return s;
}
```
## Complexity Analysis
- Runtime: O(n log n)
- Memory: O(n)
</p>


### [C++] O(nlogn) Simple DFS Solution
- Author: PhoenixDD
- Creation Date: Sun Sep 22 2019 12:03:34 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 07 2019 04:12:00 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
When there are multiple indices that overlap each other eg: [1,2] and [2,3] we can always get the characters at those indices (1,2,3) at any of the indice we like by swapping in some permutation. 
These overlapped indices form our "connected graph or connected components" and these belong to one group.
eg: [2,3],[4,5],[3,6],[2,7] for a string of length 8 we have the following groups:
* 0
* 1
* 2,3,6,7
* 4,5

We use this observation to build our solution.

**Solution**
All we need to do is get all those groups of indices that can be swapped with each other and sort the string formed by those indices since they can always be replaced/swapped by any other charater in those indices as noted above.
Repeat this for all the groups and you get your sorted string.
```c++
class Solution {
public:
    vector<int> indices;                                                 //Stores indices of same group.
    vector<bool> visited;
    vector<vector<int>> adjList;
    string indiceString;                                                 //Stores  string formed by indices in the same group.
    void dfs(string &s,int n)                                             //DFS to get all indices in same group.
    {
        visited[n]=true;
        indices.push_back(n);
        indiceString+=s[n];
        for(int &i:adjList[n])
            if(!visited[i])
               dfs(s,i);
    }
    string smallestStringWithSwaps(string s, vector<vector<int>>& pairs) 
    {
        adjList.resize(s.length());
        visited.resize(s.length(),false);
        for(vector<int> &v:pairs)                               //Create adjacency list using the indice pairs
            adjList[v[0]].push_back(v[1]),adjList[v[1]].push_back(v[0]);
        for(int i=0;i<s.length();i++)
            if(!visited[i])
            {
                indiceString="";                              //Clear string formed by one group of indices before finding next group.
                indices.clear();                             //Clear indices vector before finding another group.
                dfs(s,i);
                sort(indiceString.begin(),indiceString.end());                    //Sort the characters in the same group.
                sort(indices.begin(),indices.end());                                  //Sort the indices in the same group.            
                for(int i=0;i<indices.size();i++)          //Replace all the indices in the same group with the sorted characters.
                    s[indices[i]]=indiceString[i];
            }
        return s;
    }
};
```
</p>


