---
title: "Leftmost Column with at Least a One"
weight: 1195
#id: "leftmost-column-with-at-least-a-one"
---
## Description
<div class="description">
<p><em>(This problem is an&nbsp;<strong>interactive problem</strong>.)</em></p>

<p>A binary matrix means that all elements are&nbsp;<code>0</code>&nbsp;or&nbsp;<code>1</code>. For each&nbsp;<strong>individual</strong> row of the matrix, this row&nbsp;is sorted in non-decreasing order.</p>

<p>Given a row-sorted binary matrix binaryMatrix, return leftmost column index(0-indexed) with at least a&nbsp;<code>1</code>&nbsp;in it. If such&nbsp;index&nbsp;doesn&#39;t exist, return <code>-1</code>.</p>

<p><strong>You can&#39;t access the Binary Matrix directly.</strong>&nbsp; You may only access the matrix using a&nbsp;<code>BinaryMatrix</code>&nbsp;interface:</p>

<ul>
	<li><code>BinaryMatrix.get(row, col)</code> returns the element of the matrix&nbsp;at index <code>(row, col)</code>&nbsp;(0-indexed).</li>
	<li><code>BinaryMatrix.dimensions()</code>&nbsp;returns a list of 2 elements&nbsp;<code>[rows, cols]</code>, which means the matrix is <code>rows * cols</code>.</li>
</ul>

<p>Submissions making more than <code>1000</code>&nbsp;calls to&nbsp;<code>BinaryMatrix.get</code>&nbsp;will be judged <em>Wrong Answer</em>.&nbsp; Also, any solutions that attempt to circumvent the judge&nbsp;will result in disqualification.</p>

<p>For custom testing purposes you&#39;re given the binary matrix <code>mat</code>&nbsp;as input&nbsp;in the following four examples. You will not have&nbsp;access the binary matrix directly.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/10/25/untitled-diagram-5.jpg" style="width: 81px; height: 81px;" /></strong></p>

<pre>
<strong>Input:</strong> mat = [[0,0],[1,1]]
<strong>Output:</strong> 0
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/10/25/untitled-diagram-4.jpg" style="width: 81px; height: 81px;" /></strong></p>

<pre>
<strong>Input:</strong> mat = [[0,0],[0,1]]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/10/25/untitled-diagram-3.jpg" style="width: 81px; height: 81px;" /></strong></p>

<pre>
<strong>Input:</strong> mat = [[0,0],[0,0]]
<strong>Output:</strong> -1</pre>

<p><strong>Example 4:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/10/25/untitled-diagram-6.jpg" style="width: 161px; height: 121px;" /></strong></p>

<pre>
<strong>Input:</strong> mat = [[0,0,0,1],[0,0,1,1],[0,1,1,1]]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>rows == mat.length</code></li>
	<li><code>cols == mat[i].length</code></li>
	<li><code>1 &lt;= rows, cols&nbsp;&lt;= 100</code></li>
	<li><code>mat[i][j]</code> is either <code>0</code>&nbsp;or&nbsp;<code>1</code>.</li>
	<li><code>mat[i]</code>&nbsp;is sorted in a&nbsp;non-decreasing way.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Facebook - 45 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Linear Search Each Row

**Intuition**

*This approach won't pass, but we'll use it as a starting point. Also, it might be helpful to you if you just needed an example of how to use the API, but don't want to see a complete solution yet!*

The leftmost `1` is the `1` with the lowest *column* index.

The problem can be broken down into finding the index of the first `1` in each row and then taking the minimum of those indexes.

![Diagram showing a linear search that has uncovered all the zeroes, along with the first 1 in each row.](../Figures/10012/linear_search.png)

The simplest way of doing this would be a linear search on each row.

**Algorithm**

<iframe src="https://leetcode.com/playground/XTt2kjpu/shared" frameBorder="0" width="100%" height="327" name="XTt2kjpu"></iframe>

**Complexity Analysis**

If you ran this code, you would have gotten the following error.

```text
You made too many calls to BinaryMatrix.get().
```

The maximum grid size is `100 by 100`, so it would contain `10000` cells. In the worst case, the linear search algorithm we implemented has to check every cell. With the problem description telling us that we can only make up to `1000` API calls, this clearly isn't going to work.

Let $$N$$ be the number of rows, and $$M$$ be the number of columns.

- Time complexity : $$O(N \cdot M)$$

    We don't know the time complexity of `binaryMatrix.get()` as its implementation isn't our concern. Therefore, we can assume it's $$O(1)$$.
    
    In the worst case, we are retrieving a value for each of the $$N \cdot M$$ cells. At $$O(1)$$ per operation, this gives a total of $$O(N \cdot M)$$.

- Space complexity : $$O(1)$$.

    We are only using constant extra space.

<br/>

---

#### Approach 2: Binary Search Each Row

**Intuition**

*This isn't the best approach, but it passes, and coding it up is a good opportunity to practice binary search.*

When linear search is too slow, we should try to find a way to use binary search. If you're not familiar with binary search, [check out this Explore Card!](https://leetcode.com/explore/learn/card/binary-search/). We recommend doing the first couple of binary search questions to get familiar with the algorithm before coming back to this problem.

Also, have a go at [First Bad Version](https://leetcode.com/problems/first-bad-version/). The only difference between that problem and this one is that instead of `0` and `1`, it uses `false` and `true`.

Like we did with the linear search, we're going to apply binary search independently on each row. The *target element* we're searching for is *the first 1 in the row*.

The core part of a binary search algorithm is how it decides whether the target element is to the left or the right of the middle element. Let's figure this out by thinking through a couple of examples.

In the row below, we've determined that the middle element is a `0`. On what side must the target element (first `1`) be? The left, or the right? Don't forget, *all the 0's are before all the 1's*.

![Diagram showing a single uncovered 0 at the "middle" index](../Figures/10012/binary_search_example_1.png)

In this next row, the middle element is a `1`? What side must the target element be on? Could it also possibly be the `1` we just found?

![Diagram showing a single uncovered 1 at the "middle" index](../Figures/10012/binary_search_example_2.png)

For the first example, we can conclude that the target element (*if* it exists) must be to the **right** of the middle element. This is because we know that everything to the left of a `0` must also be a `0`,

For the second example, we can conclude that the target element is either the middle element itself or it is some other `1` to the **left** of the middle element. We know that everything to the right of a `1` is also a `1`, but these can't possibly be further left than the one we just found.

In summary, if the middle element is a:

- **0**, then the target must be to the **right**. 
- **1**, then the target is either this element or to the **left**.

We can then put this together into an algorithm that finds the index of the target element (first `1`) in each row, and then returns the minimum of those indexes. Here is an animation of how that algorithm would look. The light grey numbers are the ones that we could *infer* without needing to make an API call. They are only there to help you understand.

!?!../Documents/10012_binary_search_animation.json:600,400!?!

**Algorithm**

*If you're already quite familiar with binary search, feel free to skip down to the implementation below*. I've decided to include lots of details here, as binary search is one of those algorithms that a lot of people get frustrated with easily and find it difficult to master.

In a binary search, we always keep track of the range that the target might be in by using two variables: `lo` to represent the lowest possible index it could be, and `hi` to represent the highest possible index it could be. Ignoring the binaryMatrix API details for the moment, here is a rough outline of our binary search in pseudocode.

```text
define function binary_search(input_list):
    lo = the lowest possible index
    hi = the highest possible index
    while the search space contains 2 or more items:
        mid = the middle index in the remaining search space
        if the element at input_list[mid] is 0:
            lo = mid + 1 (the first 1 is *further right*, and can't be mid itself)
        else:
            hi = mid (the first 1 is either mid itself, *or is further left*)
    return the only index remaining in the search space
```

As always in binary search, there are a couple more key implementation details we still need to deal with:

1. An even-length search space has two middles. Which do we choose?
2. The row might be all 0's.

Let's tackle these issues one at a time.

The first issue, the choice of middle, is important, as otherwise, the search space might stop shrinking when it gets down to two elements. When the search space doesn't shrink, the algorithm does the exact same thing the next loop cycle, resulting in an infinite loop. Remember that when the search space only contains two elements, they are the ones pointed to by `lo` and `hi`. This means that the lower middle equals `lo`, and the upper-middle equals `hi`. We, therefore, need to think about which cases would shrink the search space, and which would not.

If we use the *lower-middle*

- If it is a `0`, then we set `lo = mid + 1`. Because `hi == mid + 1`, this means that `lo == hi` (search space is of length-one).
- If it is a `1`, then we set `hi = mid`. Because `mid == lo`, this means that `hi == lo` (search space is of length-one).

If we use the *upper-middle*

- If it is a `0`, then we set `lo = mid + 1`. Because `hi = mid`, we now have `hi > lo` (search space is of length-zero).
- If it is a `1`, then we set `hi = mid`. Because `hi == mid` was already true, the search space stays as is (of length-two).

If we use the *lower-middle*, we know the search space will always shrink. If we use the *upper-middle*, it might not. Therefore, we should go with the *lower-middle*. The formula for this is `mid = (low + high) / 2`.

The second issue, a row of all zeroes, is solved by recognizing that the algorithm always shrinks down the search space to a single element. This is supposed to be the first `1`, but if that doesn't exist, then it *has* to be a `0`. Therefore, we can detect this case by checking whether or not the last element in the search space is a `1`.

It is good practice to think these details through carefully so that you can write your binary search algorithm decisively and confidently. Resist the urge to [Program by Permutation](https://en.wikipedia.org/wiki/Programming_by_permutation)!

Anyway, putting this all together, we get the following code.

<iframe src="https://leetcode.com/playground/7LsEUExe/shared" frameBorder="0" width="100%" height="500" name="7LsEUExe"></iframe>


**Complexity Analysis**

Let $$N$$ be the number of rows, and $$M$$ be the number of columns.

- Time complexity : $$O(N \, \log \, M)$$.

    There are $$M$$ items in each row. Therefore, each binary search will have a cost of $$O(\log \, M)$$. We are performing $$N$$ of these binary searches, giving a time complexity of $$N \cdot O(\log \, M) = O(N \, \log \, M)$$.

- Space complexity : $$O(1)$$.

    We are using constant extra space.

<br/>

---

#### Approach 3: Start at Top Right, Move Only Left and Down

**Intuition**

Did you notice in Approach 2 that we didn't need to finish searching all the rows? One example of this was row 3 on the example in the animation. At the point shown in the image below, it was clear that row 3 *could not possibly be better than the minimum we'd found so far*.

![Diagram showing redundant search in Row 3](../Figures/10012/redundant_search.png)

Therefore, an optimization we could have made was to keep track of the minimum index so far, and then abort the search on any rows where we have discovered a `0` at, or to the right of, that minimum index.

We can do even better than that; on each search, we can set `hi = smallest_index - 1`, where `smallest_index` is the smallest index of a `1` we've seen so far. In most cases, this is a substantial improvement. It works because we're only interested in finding `1`s at lower indexes than we previously found. Here is an animation of the above example with this optimized algorithm. The algorithm eliminates as many cells as it can with each API call. It also starts by checking the last cell of the row before proceeding with the binary search, to eliminate needless binary searches where the row only had `0`s left in it.

!?!../Documents/10012_optimized_binary_search.json:600,400!?!

Here is what the worst-case looks like. Like before, its time complexity is still $$O(M \, \log \, N)$$.

![The worst case for the optimized algorithm](../Figures/10012/optimized_binary_search_worst_case.png)

While this is no worse than Approach 2, there is a better algorithm.

> Start in the top right corner, and if the current value is a `0`, move down. If it is a `1`, then move left.

The easiest way to see why this works is an example. Here is an animation of it.

!?!../Documents/10012_2D_linear_animation.json:600,400!?!

You probably gained some intuitive sense as to why this works, just from watching the animation.

- When we encounter a `0`, we know that the leftmost `1` can't be to the left of it. 
- When we encounter a `1`, we should continue the search on that row (move pointer to the left), in order to find an even smaller index.


**Algorithm**

<iframe src="https://leetcode.com/playground/yv4JVHcn/shared" frameBorder="0" width="100%" height="446" name="yv4JVHcn"></iframe>

**Complexity Analysis**

Let $$N$$ be the number of rows, and $$M$$ be the number of columns.

- Time complexity : $$O(N + M)$$.
    
    At each step, we're moving 1 step left or 1 step down. Therefore, we'll always finish looking at either one of the $$M$$ rows or $$N$$ columns. Therefore, we'll stay in the grid for at most $$N + M$$ steps, and therefore get a time complexity of $$O(N + M)$$.

- Space complexity : $$O(1)$$.

    We are using constant extra space.

<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Binary Search & Linear Solutions with Picture Explain - Clean Code
- Author: hiepit
- Creation Date: Wed Apr 22 2020 00:57:50 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Apr 22 2020 18:24:46 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1 - Binary Search**
Since rows are sorted in non-decreasing order.
We do binary search to find the target column.
Can check the following easy to undestand code.
```java
class Solution {
    public int leftMostColumnWithOne(BinaryMatrix binaryMatrix) {
        List<Integer> dimen = binaryMatrix.dimensions();
        int m = dimen.get(0), n = dimen.get(1);
        int left = 0, right = n - 1, ans = -1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (existOneInColumn(binaryMatrix, m, mid)) {
                ans = mid;          // record as current ans
                right = mid - 1;    // try to find in the left side
            } else {
                left = mid + 1;     // try to find in the right side
            }
        }
        return ans;
    }
    boolean existOneInColumn(BinaryMatrix binaryMatrix, int m, int c) {
        for (int r = 0; r < m; r++) if (binaryMatrix.get(r, c) == 1) return true;
        return false;
    }
}
```
Complexity:
- Time: `O(MlogN`, where `M` is the number of rows, `N` is the number of columns in `Binary Matrix`
   Explain: `existOneInColumn` costs `O(M)`, `binary search columns` costs `O(logN)`
- Space: `O(1)`

**Solution 2 - Linear Time**
Picture explain:
![image](https://assets.leetcode.com/users/hiepit/image_1587490344.png)
```java
class Solution {
    public int leftMostColumnWithOne(BinaryMatrix binaryMatrix) {
        List<Integer> dimen = binaryMatrix.dimensions();
        int m = dimen.get(0), n = dimen.get(1);
        int ans = -1, r = 0, c = n - 1;
        while (r < m && c >= 0) {
            if (binaryMatrix.get(r, c) == 1) {
                ans = c; // record as current ans
                c--;
            } else {
                r++;
            }
        }
        return ans;
    }
}
```
Complexity:
- Time: `O(M + N)`, where `M` is the number of rows, `N` is the number of columns in `Binary Matrix`
- Space: `O(1)`

Please help to **UPVOTE** if this post is useful for you. 
If you have any questions, feel free to comment below.
Happy coding!
</p>


### Mistake in Task description detected !!
- Author: arexol
- Creation Date: Tue Apr 21 2020 18:03:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 21 2020 18:03:00 GMT+0800 (Singapore Standard Time)

<p>
int get(int x, int y); - is provided but in fact x and y is mixed (binaryMatrix.get(y,x);  should be called)
That\'s confusing - please correct.
</p>


### Easy Python solution - O(M + N)
- Author: auto676
- Creation Date: Tue Apr 21 2020 15:51:52 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 21 2020 15:51:52 GMT+0800 (Singapore Standard Time)

<p>
Using the information that the rows are sorted, if we start searching from the right top corner(1st row, last column) and every time when we get a 1, as the row is sorted in non-decreasing order, there is a chance of getting 1 in the left column, so go to previous column in the same row. And if we get `0`, there is no chance that in that row we can find a `1`, so go to next row. Here is the implementation:

```
class Solution:
    def leftMostColumnWithOne(self, binaryMatrix: \'BinaryMatrix\') -> int:
        M, N = binaryMatrix.dimensions()
        
        r, c = 0, N - 1 
        leftmost_col = -1
        while r < M and c >= 0:
            if binaryMatrix.get(r,c) == 1:
                leftmost_col = c
                c -= 1
            else:
                r += 1
        return leftmost_col
```

* **Time complexity:** O(M+N), M is number of rows and N is number of columns
* **Space complexity:** O(1)
</p>


