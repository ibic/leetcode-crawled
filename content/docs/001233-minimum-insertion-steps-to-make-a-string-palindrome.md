---
title: "Minimum Insertion Steps to Make a String Palindrome"
weight: 1233
#id: "minimum-insertion-steps-to-make-a-string-palindrome"
---
## Description
<div class="description">
<p>Given a string <code>s</code>. In one step you can insert any character at any index of the string.</p>

<p>Return <em>the minimum number of steps</em> to make <code>s</code>&nbsp;palindrome.</p>

<p>A&nbsp;<b>Palindrome String</b>&nbsp;is one that reads the same backward as well as forward.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;zzazz&quot;
<strong>Output:</strong> 0
<strong>Explanation:</strong> The string &quot;zzazz&quot; is already palindrome we don&#39;t need any insertions.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;mbadm&quot;
<strong>Output:</strong> 2
<strong>Explanation:</strong> String can be &quot;mbdadbm&quot; or &quot;mdbabdm&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;leetcode&quot;
<strong>Output:</strong> 5
<strong>Explanation:</strong> Inserting 5 characters the string becomes &quot;leetcodocteel&quot;.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;g&quot;
<strong>Output:</strong> 0
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;no&quot;
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 500</code></li>
	<li>All characters of <code>s</code>&nbsp;are lower case English letters.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 4 (taggedByAdmin: false)
- SAP - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Simple DP (Memoization) and Bottom-up with O(n) Space
- Author: PhoenixDD
- Creation Date: Sun Jan 05 2020 12:00:58 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 06 2020 14:20:49 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
Let\'s imagine matching the characters of the string like a palindrome, from the begining and the end with 2 pointers `i` and `j`.
We may encounter 2 scenarios:
1) The character at `i` matches character at `j`.
2) The characters don\'t match each other

In case of 1 we just increase the pointer `i` and decrease the pointer `j`, `i++` and `j--` respectively.

In the second case we have 2 options:
1) Insert one character at `j` to match the character at `i`.

Or

2) Insert one character at `i` to match the character at `j`.

Since we are not actually adding the characters in the string but just calculating the cost,
In case 1 we increase the pointer `i` by `1` and `j` stays as it is, as we still need a character to match at `j`
and in case 2 we decrease the pointer `j` by `1` and `i` stays as it is, as we still need a character to match at `i`.
both the cases adds cost `1` since we are inserting a letter.

We can then use these two different pairs of new `i` and `j` values (`i+1, j` and `i, j-1`) to again repeat the process and use the **minimum** result of these as our result for current pair of `i, j`.
We can see that this is recursive and thus we can use recursion with caching to store the repeated values.

**Solution (Memoization)**
```c++
class Solution {
public:
    vector<vector<int>> memo;
    int dp(string &s,int i,int j)
    {
        if(i>=j)							//Base case.
            return 0;
        if(memo[i][j]!=-1)					//Check if we have already calculated the value for the pair `i` and `j`.
            return memo[i][j];
        return memo[i][j]=s[i]==s[j]?dp(s,i+1,j-1):1+min(dp(s,i+1,j),dp(s,i,j-1));		//Recursion as mentioned above.
    }
    int minInsertions(string s) 
    {
        memo.resize(s.length(),vector<int>(s.length(),-1));
        return dp(s,0,s.length()-1);
    }
};
```
**Complexity**
Space: `O(n^2)` as we can store at max all possible pairs of `i` and `j`.
Time: `O(n^2)` as we calculate all pairs of possible `i` and `j`.

**Note:** Space complexity can be reduced to `O(n)` by using bottom-up DP and storing only the previous state. (As we can see from the recursion, we only need `i-1` or `j-1` at any given point.)

**Solution (Bottom-up)** 
As I mentioned in the **Note** earlier, that we can reduce the space complexity as we only need the previous `i+1` or `j-1` states, I thought why not just add the solution as well. So here goes.

We can be cognizant of how we fill the `memo` table to optimize the space complexity when we have such scenarios. The code is pretty self explanatory with the added comments.
```c++
class Solution {
public:
    int minInsertions(string s) 
    {
        vector<int> memo(s.length(),0);
        int prev,temp;
        for(int i=s.length()-2;i>=0;i--)
        {
            prev=0;                                          //This stores the value at memo[i+1][j-1];
            for(int j=i;j<s.length();j++)
            {
                temp=memo[j];                               //Get the value of memo[i+1][j].
                memo[j]=s[i]==s[j]?prev:1+min(memo[j],memo[j-1]);     //memo[j]=memo[i+1][j], memo[j-1]=memo[i][j-1], prev=memo[i+1][j-1].
                prev=temp;                        //Store the value of memo[i+1][j] to use it as memo[i+1][j-1] in the next iteration.
            }
        }
        return memo[s.length()-1];
    }
};
```
**Complexity**
Space: `O(n)` as we reuse the previously stored value from previous state from the `memo` or `prev` variable.
Time: `O(n^2)` as we calculate all pairs of possible `i` and `j`.
</p>


### [Java/C++/Python] Longest Common Sequence
- Author: lee215
- Creation Date: Sun Jan 05 2020 12:01:48 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jan 10 2020 11:06:47 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Split the string `s` into to two parts,
and we try to make them symmetrical by adding letters.

The more common symmetrical subsequence they have,
the less letters we need to add.

Now we change the problem to find the length of longest common sequence.
This is a typical dynamic problem.
<br>

## **Explanation**
**Step1.**
Initialize `dp[n+1][n+1]`,
where`dp[i][j]` means the length of longest common sequence between
`i` first letters in `s1` and `j` first letters in `s2`.

**Step2.**
Find the the longest common sequence between `s1` and `s2`,
where `s1 = s` and `s2 = reversed(s)`

**Step3.**
`return n - dp[n][n]`
<br>

## **Complexity**
Time `O(N^2)`
Space `O(N^2)`
<br>

**Java:**
```java
    public int minInsertions(String s) {
        int n = s.length();
        int[][] dp = new int[n+1][n+1];
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                dp[i + 1][j + 1] = s.charAt(i) == s.charAt(n - 1 - j) ? dp[i][j] + 1 : Math.max(dp[i][j + 1], dp[i + 1][j]);
        return n - dp[n][n];
    }
```

**C++:**
```cpp
    int minInsertions(string s) {
        int n = s.length();
        vector<vector<int>> dp(n + 1, vector<int>(n + 1));
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                dp[i + 1][j + 1] = s[i] == s[n - 1 - j] ? dp[i][j] + 1 : max(dp[i][j + 1], dp[i + 1][j]);
        return n - dp[n][n];
    }
```

**Python:**
Note that `~j = -j - 1`
```python
    def minInsertions(self, s):
        n = len(s)
        dp = [[0] * (n + 1) for i in xrange(n + 1)]
        for i in xrange(n):
            for j in xrange(n):
                dp[i + 1][j + 1] = dp[i][j] + 1 if s[i] == s[~j] else max(dp[i][j + 1], dp[i + 1][j])
        return n - dp[n][n]
```

</p>


### 516. Longest Palindromic Subsequence
- Author: votrubac
- Creation Date: Sun Jan 05 2020 12:03:33 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 06 2020 07:12:23 GMT+0800 (Singapore Standard Time)

<p>
If we figure out the longest palindromic subsequence, then we can tell the miminum number of characters to add or remove to make the string a palindrome. 

So, we can simply reuse [516. Longest Palindromic Subsequence](https://leetcode.com/problems/longest-palindromic-subsequence/).
```CPP
int minInsertions(string s) {
  return s.size() - longestPalindromeSubseq(s);
}
int longestPalindromeSubseq(string s) {
  int dp[s.size() + 1][s.size() + 1] = {};
  for (int len = 1; len <= s.size(); ++len)
    for (auto i = 0; i + len <= s.size(); ++i) 
      dp[i][i + len] = s[i] == s[i + len - 1] ? 
        dp[i + 1][i + len - 1] + (len == 1 ? 1 : 2) : max(dp[i][i + len - 1], dp[i + 1][i + len]);
  return dp[0][s.size()];
}
```
</p>


