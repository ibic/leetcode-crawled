---
title: "Cherry Pickup II"
weight: 1334
#id: "cherry-pickup-ii"
---
## Description
<div class="description">
<p>Given a <code>rows x cols</code> matrix <code>grid</code> representing a field of cherries.&nbsp;Each cell in&nbsp;<code>grid</code>&nbsp;represents the number of cherries that you can collect.</p>

<p>You have two&nbsp;robots that can collect cherries for you, Robot #1 is located at the top-left corner (0,0) , and Robot #2 is located at the top-right corner (0, cols-1) of the grid.</p>

<p>Return the maximum number of cherries collection using both robots&nbsp; by following the rules below:</p>

<ul>
	<li>From a cell (i,j), robots can move to cell (i+1, j-1) , (i+1, j) or (i+1, j+1).</li>
	<li>When any robot is passing through a cell, It picks it up all cherries, and the cell becomes an empty cell (0).</li>
	<li>When both robots stay on the same cell, only one of them takes the cherries.</li>
	<li>Both robots cannot move outside of the grid at&nbsp;any moment.</li>
	<li>Both robots should reach the bottom row in the <code>grid</code>.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/04/29/sample_1_1802.png" style="width: 139px; height: 182px;" /></strong></p>

<pre>
<strong>Input:</strong> grid = [[3,1,1],[2,5,1],[1,5,5],[2,1,1]]
<strong>Output:</strong> 24
<strong>Explanation:</strong>&nbsp;Path of robot #1 and #2 are described in color green and blue respectively.
Cherries taken by Robot #1, (3 + 2 + 5 + 2) = 12.
Cherries taken by Robot #2, (1 + 5 + 5 + 1) = 12.
Total of cherries: 12 + 12 = 24.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/04/23/sample_2_1802.png" style="width: 284px; height: 257px;" /></strong></p>

<pre>
<strong>Input:</strong> grid = [[1,0,0,0,0,0,1],[2,0,0,0,0,3,0],[2,0,9,0,0,0,0],[0,3,0,5,4,0,0],[1,0,2,3,0,0,6]]
<strong>Output:</strong> 28
<strong>Explanation:</strong>&nbsp;Path of robot #1 and #2 are described in color green and blue respectively.
Cherries taken by Robot #1, (1 + 9 + 5 + 2) = 17.
Cherries taken by Robot #2, (1 + 3 + 4 + 3) = 11.
Total of cherries: 17 + 11 = 28.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,0,0,3],[0,0,0,3],[0,0,3,3],[9,0,3,3]]
<strong>Output:</strong> 22
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,1],[1,1]]
<strong>Output:</strong> 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>rows == grid.length</code></li>
	<li><code>cols == grid[i].length</code></li>
	<li><code>2 &lt;= rows, cols &lt;= 70</code></li>
	<li><code>0 &lt;= grid[i][j] &lt;= 100&nbsp;</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 5 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Overview
You probably can guess from the problem title that this problem is an extension of the original [Cherry Pickup](https://leetcode.com/problems/cherry-pickup/). 

In this problem, we do not need to return to the starting point but have two robots instead of one. However, the essence of this problem does not change, and the same method is still available.

Below, we will discuss two similar approaches: *Dynamic Programming (Top Down)* and *Dynamic Programming (Bottom Up)*.

The first one is also known as dfs with memoization or memoization dp, and the second one is also known as tabulation dp. They have the same main idea but different coding approaches.

---
#### Approach #1: Dynamic Programming (Top Down)

**Intuition**

>In this part, we will explain how to think of this approach step by step.
>
>If you are only interested in the pure algorithm, you can jump to the algorithm part.

We need to move two robots! Note that the order of moving robot1 or robot2 does not matter since it would not impact the cherries we can pick. The number of cherries we can pick only depends on the tracks of our robots.

Therefore, we can move the robot1 and robot2 in any order we want. We aim to apply DP, so we are looking for an order that suitable for DP.

Let's try a few possible moving orders.

Can we move robot1 firstly to the bottom row, and then move robot2?

Maybe not. In this case, the movement of robot1 will impact the movement of robot2. In other words, the optimal track of robot2 depends on the track of robot1. If we want to apply DP, we need to record the whole track of robot1 as the state. The number of sub-problems is too much.

In fact, in any case, when we move one robot several steps earlier than the other, the movement of the first robot will impact the movement of the second robot.

Unless we move them **synchronously** (i.e., move one step of robot1 and robot2 at the same time).

Let's define the DP state as `(row1, col1, row2, col2)`, where `(row1, col1)` represents the location of robot1, and `(row2, col2)` represents the location of robot2.

If we move them synchronously, robot1 and robot2 will always on the same row. Therefore, `row1 == row2`.

Let `row = row1`. The DP state is simplified to `(row, col1, col2)`, where `(row, col1)` represents the location of robot1, and `(row, col2)` represents the location of robot2.

OK, time to define the DP function.

>Since it's a top-down DP approach, we try to solve the problem with the DP function. Check approach 2 for DP array (bottom-up).

Let `dp(row, col1, col2)` return the maximum cherries we can pick if robot1 starts at `(row, col1)` and robot2 starts at `(row, col2)`.

>You can try changing different `dp` meaning to yield some other similar approaches. For example, let `dp(row, col1, col2)` mean the maximum cherries we can pick if robot1 **ends** at `(row, col1)` and robot2 **ends** at `(row, col2)`.

The base cases are that robot1 and robot2 both start at the bottom line. In this case, they do not need to move. All we need to do is to collect the cherries at current cells. Remember not to double count if robot1 and robot2 are at exactly the same cell.

In other cases, we need to add the maximum cherries robots can pick in the future. Here comes the transition function.

Since we move robots synchronously, and each robot has three different movements for one step, we totally have $$3*3 = 9$$ possible movements for two robots:

```text
    ROBOT1 | ROBOT2
------------------------
 LEFT DOWN |  LEFT DOWN
 LEFT DOWN |       DOWN
 LEFT DOWN | RIGHT DOWN
      DOWN |  LEFT DOWN
      DOWN |       DOWN
      DOWN | RIGHT DOWN
RIGHT DOWN |  LEFT DOWN
RIGHT DOWN |       DOWN  
RIGHT DOWN | RIGHT DOWN  
```

The maximum cherries robots can pick in the future would be the max of those 9 movements, which is the maximum of `dp(row+1, new_col1, new_col2)`, where `new_col1` can be `col1`, `col1+1`, or `col1-1`, and `new_col2` can be `col2`, `col2+1`, or `col2-1`.

Remember to use a map or an array to store the results of our `dp` function to prevent redundant calculating.

**Algorithm**

Define a `dp` function that takes three integers `row`, `col1`, and `col2` as input.

`(row, col1)` represents the location of robot1, and `(row, col2)` represents the location of robot2.

The `dp` function returns the maximum cherries we can pick if robot1 starts at `(row, col1)` and robot2 starts at `(row, col2)`.

In the `dp` function:

- Collect the cherry at `(row, col1)` and `(row, col2)`. Do not double count if `col1 == col2`.

- If we do not reach the last row, we need to add the maximum cherries we can pick in the future.

- The maximum cherries we can pick in the future is the maximum of `dp(row+1, new_col1, new_col2)`, where `new_col1` can be `col1`, `col1+1`, or `col1-1`, and `new_col2` can be `col2`, `col2+1`, or `col2-1`.

- Return the total cherries we can pick.

Finally, return `dp(row=0, col1=0, col2=last_column)` in the main function.

**Implementation**

<iframe src="https://leetcode.com/playground/8zMvf8km/shared" frameBorder="0" width="100%" height="500" name="8zMvf8km"></iframe>

**Complexity Analysis**

Let $$M$$ be the number of rows in `grid` and $$N$$ be the number of columns in `grid`.

* Time Complexity: $$\mathcal{O}(MN^2)$$, since our `helper` function have three variables as input, which have $$M$$, $$N$$, and $$N$$ possible values respectively. In the worst case, we have to calculate them all once, so that would cost $$\mathcal{O}(M \cdot N \cdot N) = \mathcal{O}(MN^2)$$. Also, since we save the results after calculating, we would not have repeated calculation.

* Space Complexity: $$\mathcal{O}(MN^2)$$, since our `helper` function have three variables as input, and they have $$M$$, $$N$$, and $$N$$ possible values respectively. We need a map with size of $$\mathcal{O}(M \cdot N \cdot N) = \mathcal{O}(MN^2)$$ to store the results.

---

#### Approach #2: Dynamic Programming (Bottom Up)

**Intuition**

Similarly, we need a three-dimension array `dp[row][col1][col2]` to store calculated results:

`dp[row][col1][col2]` represents the maximum cherries we can pick if robot1 starts at `(row, col1)` and robot2 starts at `(row, col2)`.

Remember, we move robot1 and robot2 synchronously, so they are always on the same row.

The base cases are that robot1 and robot2 both start at the bottom row. In this case, we only need to calculate the cherry at current cells.

Otherwise, apply the transition equation to get the maximum cherries we can pick in the future.

Since the base case is at the bottom row, we need to iterate from the bottom row to the top row when filling the `dp` array.

>You can use state compression to save the first dimension since we only need `dp[row+1]` when calculating `dp[row]`.

>You can change the meaning of `dp[row][col1][col2]` and some corresponding codes to get some other similar approaches. For example, let `dp[row][col1][col2]` mean the maximum cherries we can pick if robot1 **ends** at `(row, col1)` and robot2 **ends** at `(row, col2)`.

**Algorithm**

Define a three-dimension `dp` array where each dimension has a size of `m`, `n`, and `n` respectively.

`dp[row][col1][col2]` represents the maximum cherries we can pick if robot1 starts at `(row, col1)` and robot2 starts at `(row, col2)`.

To compute `dp[row][col1][col2]` (transition equation):

- Collect the cherry at `(row, col1)` and `(row, col2)`. Do not double count if `col1 == col2`.

- If we are not in the last row, we need to add the maximum cherries we can pick in the future.

- The maximum cherries we can pick in the future is the maximum of `dp[row+1][new_col1][new_col2]`, where `new_col1` can be `col1`, `col1+1`, or `col1-1`, and `new_col2` can be `col2`, `col2+1`, or `col2-1`.

Finally, return `dp[0][0][last_column]`.

>State compression can be used to save the first dimension: `dp[col1][col2]`. Just reuse the dp array after iterating one row.

**Implementation**

<iframe src="https://leetcode.com/playground/8uhq5WcG/shared" frameBorder="0" width="100%" height="500" name="8uhq5WcG"></iframe>

**Complexity Analysis**

Let $$M$$ be the number of rows in `grid` and $$N$$ be the number of columns in `grid`.

* Time Complexity: $$\mathcal{O}(MN^2)$$, since our dynamic programming has three nested for-loops, which have $$M$$, $$N$$, and $$N$$ iterations respectively. In total, it costs $$\mathcal{O}(M \cdot N \cdot N) = \mathcal{O}(MN^2)$$.

* Space Complexity: $$\mathcal{O}(MN^2)$$ if not use state compression, since our `dp` array has $$\mathcal{O}(M \cdot N \cdot N) = \mathcal{O}(MN^2)$$ elements. $$\mathcal{O}(N^2)$$ if use state compression, since we can reuse the first dimension, and our `dp` array only has $$\mathcal{O}(N \cdot N) = \mathcal{O}(N^2)$$ elements.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Top Down DP - Clean code
- Author: hiepit
- Creation Date: Sun May 31 2020 00:03:35 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 02 2020 14:14:11 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
This is dp solution. At each step, we move both robot1 and robot2 to next row, and with all possibles columns `(j-1, j, j+1)`. Please keep in mind that if 2 robots go the same cell, we only collect cherries once.

**Complexity**
- Time: `O(9 * m * n^2)`, where `m` is number of rows, `n` is number of cols of `grid`.
- Space: `O(m * n^2)`

**Java ~ 16ms**
```java
    public int cherryPickup(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        Integer[][][] dp = new Integer[m][n][n];
        return dfs(grid, m, n, 0, 0, n - 1, dp);
    }
    int dfs(int[][] grid, int m, int n, int r, int c1, int c2, Integer[][][] dp) {
        if (r == m) return 0; // Reach to bottom row
        if (dp[r][c1][c2] != null) return dp[r][c1][c2];
        int ans = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                int nc1 = c1 + i, nc2 = c2 + j;
                if (nc1 >= 0 && nc1 < n && nc2 >= 0 && nc2 < n) {
                    ans = Math.max(ans, dfs(grid, m, n, r + 1, nc1, nc2, dp));
                }
            }
        }
        int cherries = c1 == c2 ? grid[r][c1] : grid[r][c1] + grid[r][c2];
        return dp[r][c1][c2] = ans + cherries;
    }
```

**C++ ~ 44ms**
```c++
    int dp[70][70][70] = {};
    int cherryPickup(vector<vector<int>>& grid) {
        memset(dp, -1, sizeof(dp));
        int m = grid.size(), n = grid[0].size();
        return dfs(grid, m, n, 0, 0, n - 1);
    }
    int dfs(vector<vector<int>>& grid, int m, int n, int r, int c1, int c2) {
        if (r == m) return 0; // Reach to bottom row
        if (dp[r][c1][c2] != -1) return dp[r][c1][c2];
        int ans = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                int nc1 = c1 + i, nc2 = c2 + j;
                if (nc1 >= 0 && nc1 < n && nc2 >= 0 && nc2 < n) {
                    ans = max(ans, dfs(grid, m, n, r + 1, nc1, nc2));
                }
            }
        }
		int cherries = c1 == c2 ? grid[r][c1] : grid[r][c1] + grid[r][c2];
        return dp[r][c1][c2] = ans + cherries;
    }
```

**Python 3 ~ 1224 ms**
```python
    def cherryPickup(self, grid: List[List[int]]) -> int:
        m, n = len(grid), len(grid[0])
        
        @lru_cache(None)
        def dfs(r, c1, c2):
            if r == m: return 0
            cherries = grid[r][c1] if c1 == c2 else grid[r][c1] + grid[r][c2]
            ans = 0
            for nc1 in range(c1 - 1, c1 + 2):
                for nc2 in range(c2 - 1, c2 + 2):
                    if nc1 >= 0 and nc1 < n and nc2 >= 0 and nc2 < n:
                        ans = max(ans, dfs(r + 1, nc1, nc2))
            return ans + cherries
        
        return dfs(0, 0, n - 1)
```
</p>


### Python3 DFS with Memo, easy to understand
- Author: Sakata_Gintoki
- Creation Date: Sun May 31 2020 01:29:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 31 2020 01:29:56 GMT+0800 (Singapore Standard Time)

<p>
```python
class Solution:
    def cherryPickup(self, grid: List[List[int]]) -> int:
        m, n = len(grid), len(grid[0])
        self.memo = {}
        return max(self.dfs(grid,0,0,0,n - 1), 0)
    
    def dfs(self,grid,i1,j1,i2,j2):
        if (i1,j1,i2,j2) in self.memo: return self.memo[(i1,j1,i2,j2)]
        m, n = len(grid), len(grid[0])
        #end cases
        if j1 == n or j2 == n or j1 == -1 or j2 == -1: return -float(\'inf\')
        if i1 == m and i2 == m: return 0
         
        # 9 different next steps
        d1 = self.dfs(grid,i1 + 1,j1 - 1,i2 + 1,j2 - 1)
        d2 = self.dfs(grid,i1 + 1,j1 - 1,i2 + 1,j2)
        d3 = self.dfs(grid,i1 + 1,j1 - 1,i2 + 1,j2 + 1)
        d4 = self.dfs(grid,i1 + 1,j1 ,i2 + 1,j2 - 1)
        d5 = self.dfs(grid,i1 + 1,j1 ,i2 + 1,j2)
        d6 = self.dfs(grid,i1 + 1,j1 ,i2 + 1,j2 + 1)
        d7 = self.dfs(grid,i1 + 1,j1 + 1 ,i2 + 1,j2 - 1)
        d8 = self.dfs(grid,i1 + 1,j1 + 1,i2 + 1,j2)
        d9 = self.dfs(grid,i1 + 1,j1 + 1,i2 + 1,j2 + 1)
        max_res = max([d1,d2,d3,d4,d5,d6,d7,d8,d9])
        
        #if two robots step on same place
        if i1 == i2 and j1 == j2:
            res = max_res + grid[i1][j1]
        else:
            res = max_res + grid[i1][j1] + grid[i2][j2]
        self.memo[(i1,j1,i2,j2)] = res
        return res
# I really appreciate it if u vote up! \uFF08\uFFE3\uFE36\uFFE3\uFF09\u2197
```
</p>


### My screencast
- Author: cuiaoxiang
- Creation Date: Sun May 31 2020 00:01:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 31 2020 00:01:22 GMT+0800 (Singapore Standard Time)

<p>
https://www.youtube.com/watch?v=JK68oDRScXs
</p>


