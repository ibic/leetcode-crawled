---
title: "Valid Perfect Square"
weight: 350
#id: "valid-perfect-square"
---
## Description
<div class="description">
<p>Given a <strong>positive</strong> integer <i>num</i>, write a function which returns True if <i>num</i> is a perfect square else False.</p>

<p><b>Follow up:</b> <b>Do not</b> use any built-in library function such as <code>sqrt</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> num = 16
<strong>Output:</strong> true
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> num = 14
<strong>Output:</strong> false
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= num &lt;= 2^31 - 1</code></li>
</ul>

</div>

## Tags
- Math (math)
- Binary Search (binary-search)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- LinkedIn - 5 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

Square root related problems usually could be solved in logarithmic time.
There are three standard logarithmic time approaches, 
listed here from the worst to the best:

- Recursion. The slowest one. 

- Binary Search. The simplest one.

- Newton's Method. The fastest one, and therefore widely used in dynamical
simulations. 

The last two algorithms are interesting ones, let's discuss them in details. 

These solutions have the same starting point.
If one knows an [integer square](https://en.wikipedia.org/wiki/Integer_square_root)
$$x$$ of num, the answer is straightforward: num is a perfect square 
if $$x * x == \textrm{num}$$. Hence the problem is to compute this integer
square.

<br /> 
<br />


---
#### Approach 1: Binary Search 

For $$\textrm{num} > 2$$ the square root $$a$$ is always less than 
$$\textrm{num} / 2$$ and greater than 1: 
$$1 < x < \textrm{num} / 2$$.
Since $$x$$ is an integer, the problem goes down to the search in the 
sorted set of integer numbers.
Binary search is a standard way to proceed in such a situation. 

**Algorithm**

- If num < 2, return True.

- Set the left boundary to 2, and the right boundary to num / 2.

- While left <= right:

    - Take x = (left + right) / 2 as a guess. 
    Compute guess_squared = x * x and compare it with num:
    
        - If guess_squared == num, then the perfect square is right here, return True.
        
        - If guess_squared > num, move the right boundary right = x - 1.
        
        - Otherwise, move the left boundary left = x + 1.
        
- If we're here, the number is not a prefect square. Return False.

**Implementation**

![fig](../Figures/367/binary.png) 

<iframe src="https://leetcode.com/playground/QxVjwegu/shared" frameBorder="0" width="100%" height="429" name="QxVjwegu"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(\log N)$$. 

    Let's compute time complexity with the help of 
    [master theorem](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)) 
    $$T(N) = aT\left(\frac{N}{b}\right) + \Theta(N^d)$$.
    The equation represents dividing the problem 
    up into $$a$$ subproblems of size $$\frac{N}{b}$$ in $$\Theta(N^d)$$ time. 
    Here at step there is only one subproblem `a = 1`, its size 
    is a half of the initial problem `b = 2`, 
    and all this happens in a constant time `d = 0`.
    That means that $$\log_b{a} = d$$ and hence we're dealing with 
    [case 2](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)#Case_2_example)
    that results in $$\mathcal{O}(n^{\log_b{a}} \log^{d + 1} N)$$
    = $$\mathcal{O}(\log N)$$ time complexity.

* Space complexity : $$\mathcal{O}(1)$$.
<br /> 
<br />


---
#### Approach 2: Newton's Method

**Newton's Algorithm: How to Figure out the Formula**

Let's do a very rough derivation of Newton's sequence which could be
done in two minutes during the interview. Please note that it's more
a way to memorize than a strict mathematical proof. 

The problem is to find a root of 

$$f(x) = x^2 - \textrm{num} = 0$$

The idea of Newton's algorithm is to start from a seed (= initial guess) 
and then to compute a root as a sequence of improved guesses.  

![fig](../Figures/367/parabola4.png) 

For example, there is a guess $$x_k$$.
To compute next guess $$x_{k + 1}$$, let's
approximate $$f(x_k)$$ by its tangent line, that would result in

$$x_{k + 1} = x_k - \frac{f(x_k)}{f'(x_k)}$$

Now use $$f(x_k) = x_k^2 - \textrm{num}$$ and $$f'(x_k) = 2x_k$$, and voila
the result

$$x_{k + 1} = \frac{1}{2}\left(x_k + \frac{\textrm{num}}{x_k}\right)$$

**Choose a seed**

How to choose a seed? Since the function $$f(x) = x^2 - \textrm{num}$$
is monotonous, the smaller seed the better, so let's take $$\textrm{num}/2$$.

**Algorithm**

- Take num / 2 as a seed.

- While x * x > num, compute the next guess using Newton's method: 
$$x = \frac{1}{2}\left(x + \frac{\textrm{num}}{x}\right)$$.

- Return x * x == num

**Implementation**

![fig](../Figures/367/newton2.png) 

<iframe src="https://leetcode.com/playground/Rc8xCfCk/shared" frameBorder="0" width="100%" height="242" name="Rc8xCfCk"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(\log N)$$ because [guess sequence converges
quadratically](https://en.wikipedia.org/wiki/Newton%27s_method#Proof_of_quadratic_convergence_for_Newton's_iterative_method). 

* Space complexity : $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A square number is 1+3+5+7+..., JAVA code
- Author: fhqplzj
- Creation Date: Sun Jun 26 2016 14:19:08 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 21:25:25 GMT+0800 (Singapore Standard Time)

<p>
   ```
 public boolean isPerfectSquare(int num) {
        int i = 1;
        while (num > 0) {
            num -= i;
            i += 2;
        }
        return num == 0;
    }
```

The time complexity is **O(sqrt(n))**, a more efficient one using binary search whose time complexity is **O(log(n))**:
```
public boolean isPerfectSquare(int num) {
        int low = 1, high = num;
        while (low <= high) {
            long mid = (low + high) >>> 1;
            if (mid * mid == num) {
                return true;
            } else if (mid * mid < num) {
                low = (int) mid + 1;
            } else {
                high = (int) mid - 1;
            }
        }
        return false;
    }
```
One thing to note is that we have to use **long** for mid to avoid **mid\*mid** from overflow. Also, you can use **long** type for **low** and **high** to avoid type casting for mid from long to int.
And a third way is to use **Newton Method** to calculate the square root or num, refer to [Newton Method](https://en.wikipedia.org/wiki/Integer_square_root#Using_only_integer_division) for details. 
```
public boolean isPerfectSquare(int num) {
        long x = num;
        while (x * x > num) {
            x = (x + num / x) >> 1;
        }
        return x * x == num;
    }
```
</p>


### Python 4 Methods with time testing
- Author: geom1try
- Creation Date: Thu May 10 2018 15:09:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 05:19:41 GMT+0800 (Singapore Standard Time)

<p>
1. Solving with Bitwise trick.
 ```
	 def BitwiseTrick(self, num):
        root = 0
        bit = 1 << 15
        
        while bit > 0 :
            root |= bit
            if root > num // root:    
                root ^= bit                
            bit >>= 1        
        return root * root == num
```

2. Using Newton\'s Method
```
    def NewtonMethod(self, num):
        r = num
        while r*r > num:
            r = (r + num/r) // 2
        return r*r == num
```

3. Math Trick for Square number is 1+3+5+ ... +(2n-1)
```
    def Math(self, num):
        i = 1
        while (num>0):
            num -= i
            i += 2       
        return num == 0
```

4. Binary Search Method
```
    def BinarySearch(self, num):
        left = 0
        right = num
        
        while left <= right:
            mid = left + (right-left)//2
            if  mid ** 2 == num:
                return True
            elif mid ** 2 > num:
                right = mid -1
            else:
                left = mid +1
        return False
```

5. Linear Method (Naive) - For comparison
```
    def Linear(self, num):
        i = 1
        while i ** 2 <= num:
            if i ** 2 == num:
                return True
            else:
                i += 1
        return False
```
I test these five methods with ``` for i in range(100000): function(i) ```, and get results as below.

Time for Bitwise\'s Method :             0.45249104499816895
Time for Newton\'s Method :            0.3492701053619385
Time for Math\'s Method :                2.641957998275757
Time for Binary Search\'s Method :  1.5031492710113525
Time for Linear\'s Method :              17.613927125930786


</p>


### 3-4 short lines, Integer Newton, Most Languages
- Author: StefanPochmann
- Creation Date: Sun Jun 26 2016 17:41:04 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 29 2018 14:39:14 GMT+0800 (Singapore Standard Time)

<p>
Just slightly modified my [sqrt solutions](https://leetcode.com/discuss/58631/3-4-short-lines-integer-newton-every-language). You can find some explanation there.

(Note I renamed the parameter to x because that's the name in the sqrt problem and I like it better.)

**Java, C++, C, C#**

        long r = x;
        while (r*r > x)
            r = (r + x/r) / 2;
        return r*r == x;

**Python**

        r = x
        while r*r > x:
            r = (r + x/r) / 2
        return r*r == x

**Ruby**

        r  = x
        r = (r + x/r) / 2 while r*r > x
        r*r == x

**JavaScript**

        r = x;
        while (r*r > x)
            r = ((r + x/r) / 2) | 0;
        return r*r == x;
</p>


