---
title: "Nth Highest Salary"
weight: 1472
#id: "nth-highest-salary"
---
## Description
<div class="description">
<p>Write a SQL query to get the <em>n</em><sup>th</sup> highest salary from the <code>Employee</code> table.</p>

<pre>
+----+--------+
| Id | Salary |
+----+--------+
| 1  | 100    |
| 2  | 200    |
| 3  | 300    |
+----+--------+
</pre>

<p>For example, given the above Employee table, the <em>n</em><sup>th</sup> highest salary where <em>n</em> = 2 is <code>200</code>. If there is no <em>n</em><sup>th</sup> highest salary, then the query should return <code>null</code>.</p>

<pre>
+------------------------+
| getNthHighestSalary(2) |
+------------------------+
| 200                    |
+------------------------+
</pre>

</div>

## Tags


## Companies
- Amazon - 4 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (mysql)
```mysql
CREATE FUNCTION getNthHighestSalary(N INT) RETURNS INT
BEGIN
  declare m int;
  set m = N - 1;
  RETURN (
      # Write your MySQL query statement below.
      select distinct Salary from Employee order by Salary desc limit m, 1
  );
END
```

## Top Discussions
### Accpted Solution for the Nth Highest Salary
- Author: rekinyz
- Creation Date: Sun Jan 11 2015 23:48:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 01:54:56 GMT+0800 (Singapore Standard Time)

<p>
    CREATE FUNCTION getNthHighestSalary(N INT) RETURNS INT
    BEGIN
    DECLARE M INT;
    SET M=N-1;
      RETURN (
          # Write your MySQL query statement below.
          SELECT DISTINCT Salary FROM Employee ORDER BY Salary DESC LIMIT M, 1
      );
    END
</p>


### No Variable, No Limit X,1, Just one query, 808ms
- Author: januarui
- Creation Date: Mon Sep 28 2015 13:47:35 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 02 2018 08:33:23 GMT+0800 (Singapore Standard Time)

<p>
        CREATE FUNCTION getNthHighestSalary(N INT) RETURNS INT
    BEGIN
        
      RETURN (
          # Write your MySQL query statement below.
          
        
          SELECT e1.Salary
          FROM (SELECT DISTINCT Salary FROM Employee) e1
          WHERE (SELECT COUNT(*) FROM (SELECT DISTINCT Salary FROM Employee) e2 WHERE e2.Salary > e1.Salary) = N - 1      
          
          LIMIT 1
          
          
          
          
      );
    END
</p>


### My accepted simply solution.Any advising?
- Author: ky512
- Creation Date: Mon Mar 02 2015 20:15:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 08 2018 20:47:25 GMT+0800 (Singapore Standard Time)

<p>
    CREATE FUNCTION getNthHighestSalary(N INT) RETURNS INT
    BEGIN
        set N=N-1;
      RETURN (
          select distinct Salary from Employee order by Salary desc limit N,1
      );
    END
</p>


