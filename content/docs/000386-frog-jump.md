---
title: "Frog Jump"
weight: 386
#id: "frog-jump"
---
## Description
<div class="description">
<p>A frog is crossing a river. The river is divided into x units and at each unit there may or may not exist a stone. The frog can jump on a stone, but it must not jump into the water.</p>

<p>Given a list of stones' positions (in units) in sorted ascending order, determine if the frog is able to cross the river by landing on the last stone. Initially, the frog is on the first stone and assume the first jump must be 1 unit.
</p>

<p>If the frog's last jump was <i>k</i> units, then its next jump must be either <i>k</i> - 1, <i>k</i>, or <i>k</i> + 1 units. Note that the frog can only jump in the forward direction.</p>

<p><b>Note:</b>
<ul>
<li>The number of stones is &ge; 2 and is < 1,100.</li>
<li>Each stone's position will be a non-negative integer < 2<sup>31</sup>.</li>
<li>The first stone's position is always 0.</li>
</ul>
</p>

<p><b>Example 1:</b>
<pre>
<b>[0,1,3,5,6,8,12,17]</b>

There are a total of 8 stones.
The first stone at the 0th unit, second stone at the 1st unit,
third stone at the 3rd unit, and so on...
The last stone at the 17th unit.

<b>Return true</b>. The frog can jump to the last stone by jumping 
1 unit to the 2nd stone, then 2 units to the 3rd stone, then 
2 units to the 4th stone, then 3 units to the 6th stone, 
4 units to the 7th stone, and 5 units to the 8th stone.
</pre>
</p>

<p><b>Example 2:</b>
<pre>
<b>[0,1,2,3,4,8,9,11]</b>

<b>Return false</b>. There is no way to jump to the last stone as 
the gap between the 5th and 6th stone is too large.
</pre>
</p>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 11 (taggedByAdmin: false)
- ByteDance - 9 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Oracle - 6 (taggedByAdmin: false)
- Nutanix - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Snapchat - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Summary

Given a sorted stone array containing the positions at which there are stones in a river. We need to determine whether it is possible or not for a frog to cross the river by stepping over these stones,
provided that the frog starts at position 0, and at every step the frog can make a jump of size $$k-1$$, $$k$$ or $$k+1$$ if the previous jump is of size $$k$$.

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

In the brute force approach, we make use of a recursive function $$canCross$$ which takes the given stone array, the current position and the current $$jumpsize$$ as input
arguments. We start with $$currentPosition=0$$ and $$jumpsize=0$$. Then for every function call, we start from the $$currentPosition$$ and check if there lies a stone at $$(currentPostion + newjumpsize)$$, where, the
$$newjumpsize$$ could be $$jumpsize$$, $$jumpsize+1$$ or $$jumpsize-1$$. In order to check whether a stone exists at the specified positions, we check the elements of the array in a linear manner.
If a stone exists at any of these positions, we call the recursive function again with the same stone array, the $$currentPosition$$ and
the $$newjumpsize$$ as the parameters. If we are able to reach the end of the stone array through any of these calls, we return $$true$$ to indicate the possibility of reaching the end.


<iframe src="https://leetcode.com/playground/SLsn858o/shared" frameBorder="0" name="SLsn858o" width="100%" height="326"></iframe>

**Complexity Analysis**

* Time complexity : $$O(3^n)$$. Recursion tree can grow upto $$3^n$$.
* Space complexity : $$O(n)$$. Recursion of depth $$n$$ is used.

---
#### Approach #2 Better Brute Force[Time Limit Exceeded]

**Algorithm**

In the previous brute force approach, we need to find if a stone exists at $$(currentPosition + new
jumpsize)$$, where $$newjumpsize$$ could be either of $$jumpsize-1$$, $$jumpsize$$ or
$$jumpsize+1$$. But in order to check if a stone exists at the specified location, we searched the given array in linearly. To optimize this, we can use binary search to look for the element
in the given array since it is sorted. Rest of the method remains the same.

<iframe src="https://leetcode.com/playground/xnG53xSp/shared" frameBorder="0" name="xnG53xSp" width="100%" height="462"></iframe>

**Complexity Analysis**

* Time complexity : $$O(3^n)$$. Recursion tree can grow upto $$3^n$$.
* Space complexity : $$O(n)$$. Recursion of depth $$n$$ is used.

---
#### Approach #3 Using Memorization [Accepted]

**Algorithm**

Another problem with above approaches is that we can make the same function calls coming through different paths e.g. For a given $$currentIndex$$, we can call the recursive function
$$canCross$$ with the $$jumpsize$$, say $$n$$. This $$n$$ could be resulting from previous $$jumpsize$$ being $$n-1$$,$$n$$ or $$n+1$$. Thus, many redundant function calls could be made
prolonging the running time. This redundancy can be removed by making use of memorization. We make use of a 2-d $$memo$$ array, initialized by $$-1$$s, to store the result returned from a function call for
a particular $$currentIndex$$ and $$jumpsize$$. If the same $$currentIndex$$ and $$jumpsize$$ happens is encountered again, we can return the result directly using the $$memo$$ array. This helps to prune the
search tree to a great extent.

<iframe src="https://leetcode.com/playground/ZauYpsEv/shared" frameBorder="0" name="ZauYpsEv" width="100%" height="479"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n^3)$$. Memorization will reduce time complexity to $$O(n^3)$$.

* Space complexity : $$O(n^2)$$. $$memo$$ matrix of size $$n^2$$ is used.

---

#### Approach #4 Using Memorization with Binary Search [Accepted]

**Algorithm**

 We can optimize the above memorization approach, if we make use of Binary Search to find if a stone
exists at $$currentPostion + newjumpsize$$ instead of searching linearly.

<iframe src="https://leetcode.com/playground/ApwgcSaM/shared" frameBorder="0" name="ApwgcSaM" width="100%" height="515"></iframe>
**Complexity Analysis**

* Time complexity : $$O\big(n^2 log(n)\big)$$. We traverse the complete $$dp$$ matrix once $$(O(n^2))$$. For every entry we take atmost $$n$$ numbers as pivot.

* Space complexity : $$O(n^2)$$. $$dp$$ matrix of size $$n^2$$ is used.

---

#### Approach #5 Using Dynamic Programming[Accepted]

**Algorithm**

In the DP Approach, we make use of a hashmap $$map$$ which contains $$key:value$$ pairs such that $$key$$ refers to the position at which a stone is present and $$value$$ is a
set containing the $$jumpsize$$ which can lead to the current stone position. We start by making a hashmap whose $$key$$s are all the positions at which a stone is present and the $$value$$s are
all empty except position 0 whose value contains 0. Then, we start traversing the elements(positions) of the given stone array in sequential order. For the $$currentPosition$$, for every possible $$jumpsize$$ in the
$$value$$ set, we check if $$currentPosition + newjumpsize$$ exists in the $$map$$, where $$newjumpsize$$ can be either $$jumpsize-1$$, $$jumpsize$$,
$$jumpsize+1$$. If so, we append the corresponding $$value$$ set with $$newjumpsize$$. We continue in the same manner. If at the end, the $$value$$ set corresponding to the
last position is non-empty, we conclude that reaching the end is possible, otherwise, it isn't.

For more understanding see this animation-

<!--![Frog Jump](../Figures/135_FrogJump.gif)-->
!?!../Documents/403_Frog.json:1000,563!?!

<iframe src="https://leetcode.com/playground/WkNK2fiw/shared" frameBorder="0" name="WkNK2fiw" width="100%" height="377"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Two nested loops are there.

* Space complexity : $$O(n^2)$$. $$hashmap$$ size can grow upto $$n^2$$ .

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Very easy to understand JAVA solution with explanations
- Author: quincyhehe
- Creation Date: Wed Sep 21 2016 14:44:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 04:49:14 GMT+0800 (Singapore Standard Time)

<p>
Use map to represent a mapping from the stone (not index) to the steps that can be taken from this stone.

so this will be 

[0,1,3,5,6,8,12,17]

{17=[], 0=[1], 1=[1, 2], 3=[1, 2, 3], 5=[1, 2, 3], 6=[1, 2, 3, 4], 8=[1, 2, 3, 4], 12=[3, 4, 5]}

Notice that no need to calculate the last stone.

On each step, we look if any other stone can be reached from it, if so, we update that stone's steps by adding step, step + 1, step - 1. If we can reach the final stone, we return true. No need to calculate to the last stone.

Here is the code:

```
    public boolean canCross(int[] stones) {
        if (stones.length == 0) {
        	return true;
        }
        
        HashMap<Integer, HashSet<Integer>> map = new HashMap<Integer, HashSet<Integer>>(stones.length);
        map.put(0, new HashSet<Integer>());
        map.get(0).add(1);
        for (int i = 1; i < stones.length; i++) {
        	map.put(stones[i], new HashSet<Integer>() );
        }
        
        for (int i = 0; i < stones.length - 1; i++) {
        	int stone = stones[i];
        	for (int step : map.get(stone)) {
        		int reach = step + stone;
        		if (reach == stones[stones.length - 1]) {
        			return true;
        		}
        		HashSet<Integer> set = map.get(reach);
        		if (set != null) {
        		    set.add(step);
        		    if (step - 1 > 0) set.add(step - 1);
        		    set.add(step + 1);
        		}
        	}
        }
        
        return false;
    } 
```
</p>


### Concise and fast DP solution using 2D array instead of HashMap with text and video explanation.
- Author: wasabistudio
- Creation Date: Thu Nov 15 2018 13:30:12 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Nov 15 2018 13:30:12 GMT+0800 (Singapore Standard Time)

<p>
## LeetCode 403. Frog Jump
### Note
If you prefer video over text, please check out this link: https://www.youtube.com/watch?v=oTCPG1ezlKc
which has the same content as follows.
### Explanation
```java
                +----+    +----+        +----+     +----+       
stone:          | S1 |    | S2 |        | S3 |     | S4 | 
            ____|____|____|____|________|____|_____|____|____________
           ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
position:"         0         1             3          5             "        

jump size:         1     [0, 1, 2]     [1, 2, 3]

// Suppose we want to know if the frog can reach stone 2 (S2),
// and we know the frog must come from S1, 
// dist(S1->S2) = 1 - 0 = 1, and we already know the frog is able to make a jump of size 1 at S1.
// Hence, the frog is able to reach S2, and the next jump would be 0, 1 or 2 units.


// Then, we want to know if the frog can reach stone 3 (S3),
// we know the frog must be at either S1 or S2 before reaching S3,

// If the frog comes from S1, then 
// we know dist(S1->S3) = 3 - 0 = 3, and we know frog couldn\'t make a jump of size 3 at S1.
// So it is not possible the frog can jump from S1 to S3.

// If the frog comes from S2, then
// we know dist(S2->S3) = 3 - 1 = 2, and we know frog could make a jump of size 2 at S2.
// Hence, the frog is able to reach S3, and the next jump would be 1, 2 or 3 units.

// If we repeat doing this for the rest stones, we\'ll end with something like below:
Exapme 1:
            
index:        0   1   2   3   4   5   6   7 
            +---+---+---+---+---+---+---+---+
stone pos:  | 0 | 1 | 3 | 5 | 6 | 8 | 12| 17|
            +---+---+---+---+---+---+---+---+
k:          | 1 | 0 | 1 | 1 | 0 | 1 | 3 | 5 |
            |   | 1 | 2 | 2 | 1 | 2 | 4 | 6 |
            |   | 2 | 3 | 3 | 2 | 3 | 5 | 7 |
            |   |   |   |   | 3 | 4 |   |   |
            |   |   |   |   | 4 |   |   |   |
            |   |   |   |   |   |   |   |   |

// Sub-problem and state:
let dp(i) denote a set containing all next jump size at stone i

// Recurrence relation:
for any j < i,
dist = stones[i] - stones[j];
if dist is in dp(j):
    put dist - 1, dist, dist + 1 into dp(i). 

// Now lets make this approach more efficient.
// BECAUSE 
// 1. The number of stones is \u2265 2 and is < 1,100. 
// 2. The frog is on the first stone and assume the first jump must be 1 unit.
// 3. If the frog\'s last jump was k units, then its next jump must be either k - 1, k, or k + 1 units,

// The maximum jump size the frog can make at each stone if possible is shown as followings: 
// stone:      0, 1, 2, 3, 4, 5
// jump size:  1, 2, 3, 4, 5, 6 (suppose frog made jump with size k + 1 at each stone)

// So instead of creating a HashSet for lookup for each stone, 
// we can create a boolean array with size of N + 1 (N is the number of stones),
// Like in the given example, at stone 2 the next jump could be 1, 2, 3, 
// we can use a bool array to represent this like
// index:    0  1  2  3  4  5  6  7  ...
//          [0, 1, 1, 1, 0, 0, 0, 0, ...]
// index is jump size, boolean value represents if the frog can make this jump.

// Then, the 2D array will be something like below.

index:        0   1   2   3   4   5   6   7 
            +---+---+---+---+---+---+---+---+
stone pos:  | 0 | 1 | 3 | 5 | 6 | 8 | 12| 17|
            +---+---+---+---+---+---+---+---+
k:        0 | 0 | 1 | 0 | 0 | 1 | 0 | 0 | 0 |
          1 | 1 | 1 | 1 | 1 | 1 | 1 | 0 | 0 |
          2 | 0 | 1 | 1 | 1 | 1 | 1 | 0 | 0 |
          3 | 0 | 0 | 1 | 1 | 1 | 1 | 1 | 0 |
          4 | 0 | 0 | 0 | 0 | 1 | 1 | 1 | 0 |
          5 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 1 |
          6 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 |
          7 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 |

// Sub-problem and state:
let dp[i][j] denote at stone i, the frog can or cannot make jump of size j

// Recurrence relation:
for any j < i,
dist = stones[i] - stones[j];
if dp[j][dist]:
    dp[i][dist - 1] = ture
    dp[i][dist] = ture
    dp[i][dist + 1] = ture
```
### Code
```
class Solution {
    public boolean canCross(int[] stones) {
        int N = stones.length;
        boolean[][] dp = new boolean[N][N + 1];
        dp[0][1] = true;
        
        for(int i = 1; i < N; ++i){
            for(int j = 0; j < i; ++j){
                int diff = stones[i] - stones[j];
                if(diff < 0 || diff > N || !dp[j][diff]) continue;
                dp[i][diff] = true;
                if(diff - 1 >= 0) dp[i][diff - 1] = true;
                if(diff + 1 <= N) dp[i][diff + 1] = true;
                if(i == N - 1) return true;
            }
        }

        return false;
    }
}
```
</p>


### Straight-forward 9ms 7-line c++ solution with explanation
- Author: mzchen
- Creation Date: Mon Sep 19 2016 02:09:21 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 05:39:03 GMT+0800 (Singapore Standard Time)

<p>
Search for the last stone in a depth-first way, prune those exceeding the [k-1,k+1] range. Well, I think the code is simple enough and need no more explanation.
```
bool canCross(vector<int>& stones, int pos = 0, int k = 0) {
    for (int i = pos + 1; i < stones.size(); i++) {
        int gap = stones[i] - stones[pos];
        if (gap < k - 1) continue;
        if (gap > k + 1) return false;
        if (canCross(stones, i, gap)) return true;
    }
    return pos == stones.size() - 1;
}
```
This can pass OJ at 9ms but is inefficient for extreme cases. (update: new test cases are added and the solution above no longer passes OJ, please see the solution below which takes 62ms) We can memorize the returns with minimum effort:
```
unordered_map<int, bool> dp;

bool canCross(vector<int>& stones, int pos = 0, int k = 0) {
    int key = pos | k << 11;

    if (dp.count(key) > 0)
        return dp[key];

    for (int i = pos + 1; i < stones.size(); i++) {
        int gap = stones[i] - stones[pos];
        if (gap < k - 1)
            continue;
        if (gap > k + 1)
            return dp[key] = false;
        if (canCross(stones, i, gap))
            return dp[key] = true;
    }

    return dp[key] = (pos == stones.size() - 1);
}
```
The number of stones is less than 1100 so **pos** will always be less than 2^11 (2048).
Stone positions could be theoretically up to 2^31 but **k** is practically not possible to be that big for the parameter as the steps must start from 0 and 1 and at the 1100th step the greatest valid k would be 1100. So combining **pos** and **k** is safe here.
</p>


