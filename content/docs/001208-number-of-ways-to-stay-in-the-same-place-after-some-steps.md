---
title: "Number of Ways to Stay in the Same Place After Some Steps"
weight: 1208
#id: "number-of-ways-to-stay-in-the-same-place-after-some-steps"
---
## Description
<div class="description">
<p>You have a pointer at index <code>0</code> in an array of size <code><font face="monospace">arrLen</font></code>. At each step, you can move 1 position to the left, 1 position to the right&nbsp;in the array or stay in the same place&nbsp; (The pointer should not be placed outside the array at any time).</p>

<p>Given two integers&nbsp;<code>steps</code> and <code>arrLen</code>, return the number of&nbsp;ways such that your pointer still at index <code>0</code> after <strong>exactly </strong><code><font face="monospace">steps</font></code>&nbsp;steps.</p>

<p>Since the answer&nbsp;may be too large,&nbsp;return it <strong>modulo</strong>&nbsp;<code>10^9 + 7</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> steps = 3, arrLen = 2
<strong>Output:</strong> 4
<strong>Explanation: </strong>There are 4 differents ways to stay at index 0 after 3 steps.
Right, Left, Stay
Stay, Right, Left
Right, Stay, Left
Stay, Stay, Stay
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> steps = 2, arrLen = 4
<strong>Output:</strong> 2
<strong>Explanation:</strong> There are 2 differents ways to stay at index 0 after 2 steps
Right, Left
Stay, Stay
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> steps = 4, arrLen = 2
<strong>Output:</strong> 8
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= steps &lt;= 500</code></li>
	<li><code>1 &lt;= arrLen&nbsp;&lt;= 10^6</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Recursive DP (Memoization)
- Author: PhoenixDD
- Creation Date: Sun Nov 24 2019 12:01:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 25 2019 15:52:23 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
We can use a simple DFS/recursion to form the solution.
The key observation is that we can prune all the positions where `i>steps` as we can never reach `0` in that case.
We can now use memoization to cache all our answers, the only thing we need to worry about is memory which can be solved by using the observation `i>steps` will return `0` which means `i` will never exceed `steps/2` due to pruning.

**Solution**
```c++
static int MOD=1e9+7;
class Solution {
public:
    vector<vector<int>> memo;
    int arrLen;
    int dp(int i, int steps)
    {
        if(steps==0&&i==0)                                             //Base condition
            return 1;
        if(i<0||i>=arrLen||steps==0||i>steps)					//Pruning.
            return 0;
        if(memo[i][steps]!=-1)         //If we have already cached the result for current `steps` and `index` get it.
            return memo[i][steps];
        return memo[i][steps]=((dp(i+1,steps-1)%MOD+dp(i-1,steps-1))%MOD+dp(i,steps-1))%MOD;        //Either move right, left or stay.
            
    }
    int numWays(int steps, int arrLen) 
    {
        memo.resize(steps/2+1,vector<int>(steps+1,-1));
        this->arrLen=arrLen;
        return dp(0,steps);
    }
};
```
**Complexity**
Space: `O(steps^2).` This can be reduecd to `O(steps)` by using top-down DP.
Time: `O(steps^2).`
</p>


### Very simple and easy to understand java solution
- Author: renato4
- Creation Date: Tue Nov 26 2019 10:05:22 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 26 2019 10:08:44 GMT+0800 (Singapore Standard Time)

<p>
If we choose our dp state as `dp[steps][position]`, from this state we can either:
* Stay. Then we consume one step and stay at the same position => `dp[steps-1][position]`
* Go right. Then we consume one step and go right => `dp[steps-1][position+1]`
* Go left (if not at position zero). Then we consume one step and go left => `if position > 0 then dp[steps-1][position-1]`

Then our state can be calculated as: `dp[steps][position] = dp[steps-1][position] + dp[steps-1][position+1] + dp[steps-1][position-1] `. 

We can use the case when we have only one step as base case. Those cases are:
* We are at position zero and with only one step, then there is only one way (stay) => `dp[1][0] = 1`
* We are at position one and with only one step, then there is only one way (go left) => `dp[1][1] = 1`

Notice that we can only go right as far as many steps we have. For example, for 500 steps, we can only go as far as the position 500th, doesn\'t matter if the arrLen is 99999999. So we use this to avoid memory/time limit. `min(steps,arrLen)`

```
class Solution {
    public int numWays(int steps, int arrLen) {
        int maxPos = Math.min(steps,arrLen);
        long[][] dp = new long[steps+1][maxPos+1];
        
        dp[1][0]=1;
        dp[1][1]=1;
        for(int i = 2; i <= steps; i++) {
            for(int j = 0; j < maxPos; j++) {
                dp[i][j] = (dp[i-1][j] + dp[i-1][j+1] + (j>0?dp[i-1][j-1]:0))%1000000007;
            }
        }
        
        return (int)dp[steps][0];
    }
}
```
</p>


### C++ Bottom-Up DP
- Author: votrubac
- Creation Date: Sun Nov 24 2019 15:02:11 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 25 2019 09:44:26 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
This is similar to [935. Knight Dialer](https://leetcode.com/problems/knight-dialer/discuss/189251/C%2B%2B-5-lines-DP).
#### Catch
The array size can be larger than the number of steps. We can ingore array elements greater than `steps / 2`, as we won\'t able to go back to the first element from there.
```
int numWays(int steps, int arrLen) {
    int sz = min(steps / 2 + 1, arrLen);
    vector<int> v1(sz + 2), v2(sz + 2);
    v1[1] = 1;
    while (steps-- > 0) {
        for (auto i = 1; i <= sz; ++i)
            v2[i] = ((long)v1[i] + v1[i - 1] + v1[i + 1]) % 1000000007;
        swap(v1, v2);
    }
    return v1[1];
}
```
**Complexity Analysis**
- Time: O(n * min(n, m)), where n is the number of steps, and m - array size.
- Memory: O(min(n, m)).
</p>


