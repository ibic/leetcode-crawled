---
title: "Warehouse Manager"
weight: 1595
#id: "warehouse-manager"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Warehouse</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| name         | varchar |
| product_id   | int     |
| units        | int     |
+--------------+---------+
(name, product_id) is the primary key for this table.
Each row of this table contains the information of the products in each warehouse.
</pre>

<p>&nbsp;</p>

<p>Table: <code><font face="monospace">Products</font></code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| product_id    | int     |
| product_name  | varchar |
| Width         | int     |
| Length        | int     |
| Height        | int     |
+---------------+---------+
product_id is the primary key for this table.
Each row of this table contains the information about the product dimensions (Width, Lenght and Height) in feets of each product.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to report,&nbsp;How much cubic feet of <strong>volume </strong>does the inventory occupy in each warehouse.</p>

<ul>
	<li>warehouse_name</li>
	<li>volume</li>
</ul>

<p>Return the result table in <strong>any</strong> order.</p>

<p>The query result format is in the following example.</p>

<p>&nbsp;</p>

<pre>
<code>Warehouse </code>table:
+------------+--------------+-------------+
| name       | product_id   | units       |
+------------+--------------+-------------+
| LCHouse1   | 1            | 1           |
| LCHouse1   | 2            | 10          |
| LCHouse1   | 3            | 5           |
| LCHouse2   | 1            | 2           |
| LCHouse2   | 2            | 2           |
| LCHouse3   | 4            | 1           |
+------------+--------------+-------------+

<font face="monospace">Products </font>table:
+------------+--------------+------------+----------+-----------+
| product_id | product_name | Width      | Length   | Height    |
+------------+--------------+------------+----------+-----------+
| 1          | LC-TV        | 5          | 50       | 40        |
| 2          | LC-KeyChain  | 5          | 5        | 5         |
| 3          | LC-Phone     | 2          | 10       | 10        |
| 4          | LC-T-Shirt   | 4          | 10       | 20        |
+------------+--------------+------------+----------+-----------+

Result table:
+----------------+------------+
| <code>warehouse_name </code>| <code>volume   </code>  | 
+----------------+------------+
| LCHouse1       | 12250      | 
| LCHouse2       | 20250      |
| LCHouse3       | 800        |
+----------------+------------+
Volume of product_id = 1 (LC-TV), 5x50x40 = 10000
Volume of product_id = 2 (LC-KeyChain), 5x5x5 = 125 
Volume of product_id = 3 (LC-Phone), 2x10x10 = 200
Volume of product_id = 4 (LC-T-Shirt), 4x10x20 = 800
LCHouse1: 1 unit of LC-TV + 10 units of LC-KeyChain + 5 units of LC-Phone.
&nbsp;         Total volume: 1*10000 + 10*125  + 5*200 = 12250 cubic feet
LCHouse2: 2 units of LC-TV + 2 units of LC-KeyChain.
&nbsp;         Total volume: 2*10000 + 2*125 = 20250 cubic feet
LCHouse3: 1 unit of LC-T-Shirt.
          Total volume: 1*800 = 800 cubic feet.
</pre>

</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Two simple solutions w/o and w/ subquery
- Author: vwang0
- Creation Date: Sat Sep 05 2020 03:01:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 16:46:17 GMT+0800 (Singapore Standard Time)

<p>
```
-- Solution 1
SELECT name warehouse_name,
       SUM(units * Width * Length * Height) volume
FROM Warehouse W
LEFT JOIN Products P 
ON W.product_id = P.product_id
GROUP BY name


-- Solution 2
SELECT name warehouse_name, SUM(units * size) volume
FROM Warehouse W
LEFT JOIN
(
    SELECT product_id, Width * Length * Height size
    FROM Products
) ps
ON W.product_id = ps.product_id
GROUP BY name
```
</p>


### Simple Solution for ANY SQL
- Author: mirandanathan
- Creation Date: Fri Sep 04 2020 13:48:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 04 2020 13:48:45 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT 
  name AS warehouse_name,
  SUM(units*Width*Length*Height) AS volume
FROM Warehouse 
LEFT JOIN Products ON Warehouse.product_id = Products.product_id
GROUP BY name
```
</p>


### Clean MySQL solution
- Author: yadddd
- Creation Date: Tue Sep 08 2020 11:49:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 03 2020 00:53:42 GMT+0800 (Singapore Standard Time)

<p>
select name as warehouse_name, SUM(units* Width* Length* Height) as volume

from Warehouse w join Products p on w.product_id = p.product_id 

group by name


</p>


