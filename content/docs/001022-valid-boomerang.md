---
title: "Valid Boomerang"
weight: 1022
#id: "valid-boomerang"
---
## Description
<div class="description">
<p>A <em>boomerang</em> is a set of 3 points that are all distinct and <strong>not</strong> in a straight line.</p>

<p>Given a list&nbsp;of three points in the plane, return whether these points are a boomerang.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[1,1],[2,3],[3,2]]</span>
<strong>Output: </strong><span id="example-output-1">true</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[[1,1],[2,2],[3,3]]</span>
<strong>Output: </strong><span id="example-output-2">false</span></pre>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>points.length == 3</code></li>
	<li><code>points[i].length == 2</code></li>
	<li><code>0 &lt;= points[i][j] &lt;= 100</code></li>
</ol>

<div>
<div>&nbsp;</div>
</div>
</div>

## Tags
- Math (math)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Straight Forward
- Author: lee215
- Creation Date: Sun May 05 2019 12:09:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 05 2019 12:28:05 GMT+0800 (Singapore Standard Time)

<p>
Assuming three points are A, B, C.

The first idea is that, calculate the area of ABC.
We can reuse the conclusion and prove in [812. Largest Triangle Area](https://leetcode.com/problems/largest-triangle-area/discuss/122711/C++JavaPython-Solution-with-Explanation-and-Prove)

The other idea is to calculate the slope of AB and AC.
`K_AB = (p[0][0] - p[1][0]) / (p[0][1] - p[1][1])`
`K_AC = (p[0][0] - p[2][0]) / (p[0][1] - p[2][1])`

We check if `K_AB != K_AC`, instead of calculate a fraction.

Time `O(1)` Space `O(1)`

<br>

**Java:**
```
    public boolean isBoomerang(int[][] p) {
        return (p[0][0] - p[1][0]) * (p[0][1] - p[2][1]) != (p[0][0] - p[2][0]) * (p[0][1] - p[1][1]);
    }
```

**C++:**
```
    bool isBoomerang(vector<vector<int>>& p) {
        return (p[0][0] - p[1][0]) * (p[0][1] - p[2][1]) != (p[0][0] - p[2][0]) * (p[0][1] - p[1][1]);
    }
```

**Python:**
```
    def isBoomerang(self, p):
        return (p[0][0] - p[1][0]) * (p[0][1] - p[2][1]) != (p[0][0] - p[2][0]) * (p[0][1] - p[1][1])
```

</p>


### C++ 1-liner: Triangle Area
- Author: votrubac
- Creation Date: Sun May 05 2019 12:01:16 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 06 2019 15:01:49 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
In other words, we need to return true if the triangle area is not zero.

For the detailed explanation, see the comment by [EOAndersson](https://leetcode.com/eoandersson/) below.
# Solution
Calculate the area of the triangle: ```x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)``` and compare it to zero.
```
bool isBoomerang(vector<vector<int>>& p) {
  return p[0][0] * (p[1][1] - p[2][1]) + p[1][0] * (p[2][1] - p[0][1]) + p[2][0] * (p[0][1] - p[1][1]) != 0;
}
```
</p>


### C++ Calculate Slopes!! (1 liner with explanation)
- Author: DDev
- Creation Date: Sun May 05 2019 12:01:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 05 2019 12:17:32 GMT+0800 (Singapore Standard Time)

<p>
	class Solution {
	public:
		bool isBoomerang(vector<vector<int>>& points) {
			if(points.size() != 3)
				return false;

			// formula for slope = (y2 - y1)/(x2 - x1)
			// assume the slope of the line using points 0 and 1 (a/b) and that of using points 0  and 2 (c / d)
			// avoid dividing, just compare a/b and c/d if(a * d > c * b)==> first fraction is greater otherwise second
			// if slopes of the both lines (p0--p1 and p0--p2) are equal then three points form a straight line (cannot form a triangle)
			return ((points[1][1] - points[0][1]) * (points[2][0] - points[0][0])) != ((points[2][1] - points[0][1]) * (points[1][0] - points[0][0]));
		}
	};
</p>


