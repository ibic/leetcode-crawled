---
title: "Binary Prefix Divisible By 5"
weight: 980
#id: "binary-prefix-divisible-by-5"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of <code>0</code>s and <code>1</code>s, consider <code>N_i</code>: the i-th subarray from <code>A[0]</code> to <code>A[i]</code>&nbsp;interpreted&nbsp;as a binary number (from most-significant-bit to least-significant-bit.)</p>

<p>Return a list of booleans&nbsp;<code>answer</code>, where <code>answer[i]</code> is <code>true</code>&nbsp;if and only if <code>N_i</code>&nbsp;is divisible by 5.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[0,1,1]</span>
<strong>Output: </strong><span id="example-output-1">[true,false,false]</span>
<strong>Explanation: </strong>
The input numbers in binary are 0, 01, 011; which are 0, 1, and 3 in base-10.  Only the first number is divisible by 5, so answer[0] is true.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[1,1,1]</span>
<strong>Output: </strong><span id="example-output-2">[false,false,false]</span>
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[0,1,1,1,1,1]</span>
<strong>Output: </strong><span id="example-output-3">[true,false,false,false,true,false]</span>
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">[1,1,1,0,1]</span>
<strong>Output: </strong><span id="example-output-4">[false,false,false,false,false]</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 30000</code></li>
	<li><code>A[i]</code> is <code>0</code> or <code>1</code></li>
</ol>

</div>

## Tags
- Array (array)

## Companies


## Official Solution
N.A.

## Accepted Submission (python3)
```python3
class Solution:
    def convertToInt(self, li):
        leny = len(li)
        r = 0
        for i in range(leny):
            r += li[i] << (leny -  1 - i)
        return r

    def convertToSum(self, li):
        ra = [li[0]]
        leny = len(li)
        for i in range(1, leny):
            ra.append((ra[i-1] << 1) + li[i])
        return ra
    def prefixesDivBy5(self, A: List[int]) -> List[bool]:
        sums = self.convertToSum(A)
        #print(sums)
        return list(map(lambda x: x % 5 == 0, sums))

```

## Top Discussions
### Detailed Explanation using Modular Arithmetic O(n)
- Author: Just__a__Visitor
- Creation Date: Sun Mar 31 2019 13:02:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 31 2019 13:02:07 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**
* First, let us see how to append an extra bit at the end of a binary number
* Any binary number **a<sub>k</sub>a<sub>k-1</sub>...a<sub>0</sub>**  has the form a<sub>0</sub>*2<sup>0</sup> + a<sub>1</sub>*2<sup>1</sup> + ........ a<sub>0</sub>*2<sup>k</sup>


* To free up an extra slot at the end, we can just multiply the original number by 2.
* Initially, the contribution of each **a<sub>i</sub>** is 2<sup>i</sup>. If the number is multiplied by 2, the contribution of each a<sup>i</sup> become 2<sup>i+1</sup>.
* The resulting number is 0*2<sup>0</sup> + a<sub>0</sub>*2<sup>1</sup> + a<sub>1</sub>*2<sup>2</sup> + ........ a<sub>0</sub>*2<sup>k+1</sup>
* Consider the number **a<sub>k</sub>a<sub>k-1</sub>...a<sub>0</sub>0**. As per the above definition, it is clear that the resulting number is just the decimal representation of this number
----

* We\'ve seen how to append an extra bit at the end of a binary number. 
* The extra bit appended by the above method is unset (0).
* How do we append a set bit? Well, we can just free up a slot at the end (by appending 0) and then add 1 to the resulting number.
* Since the current rightmost bit is zero, therefore there won\'t be any carry-effects. Hence the new number would be **a<sub>k</sub>a<sub>k-1</sub>...a<sub>0</sub>1**
---
**Conclusion**
* For appending a digit **d** at the end of a binary number **old_number** = **a<sub>k</sub>a<sub>k-1</sub>...a<sub>0</sub>**, we can just do **new_number** = old_number*2 + **d**. 
* This gives us the number with binary representation  **a<sub>k</sub>a<sub>k-1</sub>...a<sub>0</sub>d**
---
**Modular Arithemtic**
* A number is divisible by 5 **iff** the number is equivalent to **0** in the modular arithemtic of 5.
---
**Naive_Algorithm**
* Since we know how to append any digit at the end, we start with 0 and keep appending digits. We can get all the numbers in this manner.
* At each step we can take the modulo with respect to 5. If the modulo is zero, we append **true** to our answer.
---
**Problems**
* The problem with the above approach is that the number can overflow easily. (As the largest integer that can be accomodated is of 31 bits in C++).
---
**Optimizations**
* Observe that we only care about the remainder, not the actual number.
* Use the fact that (a*b + c)%d is same as ((a%d)*(b%d) + c%d)%d.
* We now have the relation **new_number**%5 = ((old_number%5)*2 + d)%5;
*  This tells us the if we provide the modulo of the old_number instead of the original number, we\'ll get the modulo of the new number.
* This would prevent overflows, since **new_number** is the equivalent representation of the original number in the modular arithemtic of 5.
---
**Optimized Algorithm**
* Start with **num**=0.
* For each valid **i** update **num** as **num** = (num*2 + a[i])%5
* At each stage, if **num** is zero, the substring ending at **i** is divisible by 5.
---
**Time Complexity**
* We pass each element exactly once. Hence *O(n)*.
```
class Solution
{
public:
    vector<bool> prefixesDivBy5(vector<int>& a);
};

vector<bool> Solution :: prefixesDivBy5(vector<int>& a)
{
    vector<bool> answer;
    int num=0;
    for(int i=0; i<a.size(); i++)
    {
        num = (num*2 + a[i])%5;
        answer.push_back(num==0);
    }
    return answer;
}
```

</p>


### [Java/Python 3] 7/1 liners - left shift, bitwise or, and mod.
- Author: rock
- Creation Date: Sun Mar 31 2019 12:30:38 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jan 24 2020 20:14:57 GMT+0800 (Singapore Standard Time)

<p>
Since A includes only `0s` and `1s`, we can imitate binary operations.

```java
    public List<Boolean> prefixesDivBy5(int[] A) {
        int k = 0;
        List<Boolean> ans = new ArrayList<>();
        for (int a : A) {
            k = (k << 1 | a) % 5; // left shift k by 1 bit and plus current element a is the binary number.
            ans.add(k == 0); 
        }
        return ans;
    }
```
```python
    def prefixesDivBy5(self, A: List[int]) -> List[bool]:
        ans, b = [], 0
        for a in A:
            b = b << 1 | a
            ans.append(b % 5 == 0)
        return ans
```
Use `itertools.accumulate` to make the above 1 liner:
```python
    def prefixesDivBy5(self, A: List[int]) -> List[bool]:
        return [x % 5 == 0 for x in itertools.accumulate(A, lambda a, b: a << 1 | b)]
```
**Analysis:**

Time & space: O(n), where n = A.length;

**Q & A:**

Q:
why do we need to mod 5 there?
A: 
The problem ask if the binary numbers are divisible by 5.
Also, mod 5 in each iteration will prevent potential int overflow.

Q: 
can you give me the explanation for why after mod 5 the value of k plus a can still represent the original value \uFF1F
A:

Rewrite original code as follows, ignore the overflow test cases.
```
    public List<Boolean> prefixesDivBy5(int[] A) {
        int[] k = new int[A.length + 1];
        List<Boolean> ans = new ArrayList<>();
        for (int i = 0; i < A.length; ++i) {
            k[i + 1] = (k[i] << 1 | a); // left shift k by 1 bit and plus current element a is the binary number.
            ans.add(k[i + 1] % 5 == 0); 
        }
        return ans;
    }
```
```

i = 0,
```
Let F0 = k[0 + 1] / 5,
    r0  = k[0 + 1] % 5,
Denote k[0 + 1] (= A[0]) = 5 * F0 + r0, 
       k[0 + 1] % 5 = r0

similarly,
```
i = 1,
```
k[1 + 1] = 2 * (5 * F0 + r0) + A[1] 
             = 2 * 5 * F0 + (**2 * r0 + A[1]**)
		     = 2 * 5 * F0 + (5 * F1 + r1)
k[1 + 1] % 5 = **(2 * r0 + A[1]) % 5**
                    = r1
```
i = 2,
```
k[2 + 1] = 2 * (2 * 5 * F0 + (5 * F1 + r1)) + A[2] 
             = (4 * 5 * F0 + 2 * 5 * F1) + (**2 * r1 + A[2]**)
             = 5  * (4 * F0 + 2 * F1) + (5 * F2 + r2)
k[2 + 1] % 5 =  (**2 * r1 + A[2]**) % 5
                    = r2

repeat the above till the end of the array...

Look at the bold part of the derivation, which is consistent with the original code. Let me know if it answers your question.

----

For more bit shift operations practice and solution, refer to [1290. Convert Binary Number in a Linked List to Integer](https://leetcode.com/problems/convert-binary-number-in-a-linked-list-to-integer/discuss/451815/javapython-3-simulate-binary-operations/434516)
</p>


### [Python] Calculate Prefix Mod
- Author: lee215
- Creation Date: Sun Mar 31 2019 12:03:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 31 2019 12:03:41 GMT+0800 (Singapore Standard Time)

<p>
I changed the input. Maybe you don\'t like it.
**Python:**
```
    def prefixesDivBy5(self, A):
        for i in xrange(1, len(A)):
            A[i] += A[i - 1] * 2 % 5
        return [a % 5 == 0 for a in A]
```

</p>


