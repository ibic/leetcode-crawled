---
title: "Magical String"
weight: 456
#id: "magical-string"
---
## Description
<div class="description">
<p>
A magical string <b>S</b> consists of only '1' and '2' and obeys the following rules:
</p>
<p>
The string <b>S</b> is magical because concatenating the number of contiguous occurrences of characters '1' and '2' generates the string <b>S</b> itself.
</p>

<p>
The first few elements of string <b>S</b> is the following:
<b>S</b> = "1221121221221121122……"
</p>

<p>
If we group the consecutive '1's and '2's in <b>S</b>, it will be:
</p>
<p>
1   22  11  2  1  22  1  22  11  2  11  22 ......
</p>
<p>
and the occurrences of '1's or '2's in each group are:
</p>
<p>
1   2	   2    1   1    2     1    2     2    1    2    2 ......
</p>

<p>
You can see that the occurrence sequence above is the <b>S</b> itself. 
</p>

<p>
Given an integer N as input, return the number of '1's in the first N number in the magical string <b>S</b>.
</p>

<p><b>Note:</b>
N will not exceed 100,000.
</p>


<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> 6
<b>Output:</b> 3
<b>Explanation:</b> The first 6 elements of magical string S is "12211" and it contains three 1's, so return 3.
</pre>
</p>
</div>

## Tags


## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java solution using one array and two pointers
- Author: shawngao
- Creation Date: Wed Jan 11 2017 02:57:42 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 11:50:27 GMT+0800 (Singapore Standard Time)

<p>
Algorithm:
1. Create an ```int``` array ```a``` and initialize the first 3 elements with ```1, 2, 2```.
2. Create two pointers ```head``` and ```tail```. ```head``` points to the number which will be used to generate new numbers. ```tail``` points to the next empty position to put the new number. Then keep generating new numbers until ```tail``` >= ```n```. 
3. Need to create the array 1 element more than ```n``` to avoid overflow because the last round ```head``` might points to a number ```2```. 
4. A trick to flip number back and forth between ```1``` and ```2```: ```num = num ^ 3```
```
public class Solution {
    public int magicalString(int n) {
        if (n <= 0) return 0;
        if (n <= 3) return 1;
        
        int[] a = new int[n + 1];
        a[0] = 1; a[1] = 2; a[2] = 2;
        int head = 2, tail = 3, num = 1, result = 1;
        
        while (tail < n) {
            for (int i = 0; i < a[head]; i++) {
                a[tail] = num;
                if (num == 1 && tail < n) result++;
                tail++;
            }
            num = num ^ 3;
            head++;
        }
        
        return result;
    }
}
```
</p>


### Short C++
- Author: StefanPochmann
- Creation Date: Sun Jan 08 2017 20:03:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 09 2018 10:15:34 GMT+0800 (Singapore Standard Time)

<p>
Just build enough of the string and then count.

    int magicalString(int n) {
        string S = "122";
        int i = 2;
        while (S.size() < n)
            S += string(S[i++] - '0', S.back() ^ 3);
        return count(S.begin(), S.begin() + n, '1');
    }
</p>


### Is the magical string unique?
- Author: xiaoboluo
- Creation Date: Fri Jan 13 2017 08:36:06 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 10 2018 00:52:43 GMT+0800 (Singapore Standard Time)

<p>
Based on the rule, if start with 2, I can get string:
2211212212211....

Compare with the one in the problem 1**2211212212211**...., it removes the prefix 1.

If that is correct, for some N  like 5, then the answer will be different depending on which one we are referring to.
</p>


