---
title: "Number of Equivalent Domino Pairs"
weight: 1104
#id: "number-of-equivalent-domino-pairs"
---
## Description
<div class="description">
<p>Given a list of <code>dominoes</code>,&nbsp;<code>dominoes[i] = [a, b]</code>&nbsp;is <em>equivalent</em> to <code>dominoes[j] = [c, d]</code>&nbsp;if and only if either (<code>a==c</code> and <code>b==d</code>), or (<code>a==d</code> and <code>b==c</code>) - that is, one domino can be rotated to be equal to another domino.</p>

<p>Return the number of pairs <code>(i, j)</code> for which <code>0 &lt;= i &lt; j &lt; dominoes.length</code>, and&nbsp;<code>dominoes[i]</code> is equivalent to <code>dominoes[j]</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> dominoes = [[1,2],[2,1],[3,4],[5,6]]
<strong>Output:</strong> 1
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= dominoes.length &lt;= 40000</code></li>
	<li><code>1 &lt;= dominoes[i][j] &lt;= 9</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy and Concise
- Author: lee215
- Creation Date: Sun Jul 21 2019 12:21:05 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 21 2019 22:29:11 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Just count the number of different dominoes.
<br>

# **Explanation**
You need to distinguish the different dominoes and count the same.

I did it in this way:
`f(domino) = min(d[0], d[1]) * 10 + max(d[0], d[1])`
For each domino `d`, calculate `min(d[0], d[1]) * 10 + max(d[0], d[1])`
This will put the smaller number on the left and bigger one on the right (in decimal).
So same number same domino, different number different domino.

Take the example from the problem:
`dominoes = [[1,2],[2,1],[3,4],[5,6]]`
now we transform it into `[12,12,34,56]`.

@sendAsync also suggest other intersting ways to do that:
1. Use the product of primes
`primes = [2,3,5,7,11,13,17,19,23,29]`
`f(domino) = primes[d[0]] * primes[d[1]]`
(though primes[0] is not used)

2. Use the bit manipulation.
`primes = [2,3,5,7,11,13,17,19,23,29]`
`f(domino) = 1 << d[0]| 1 << d[1];`

Personaly I like the second more.
<br>

# **Complexity**
Time `O(N)`
Space `O(N)`
<br>

# Solution 1

We sum up the pair in the end after the loop,
using the guass formula `sum = v * (v + 1) / 2`,
where `v` is the number of count.

**Java:**
```java
    public int numEquivDominoPairs(int[][] dominoes) {
        Map<Integer, Integer> count = new HashMap<>();
        int res = 0;
        for (int[] d : dominoes) {
            int k = Math.min(d[0], d[1]) * 10 + Math.max(d[0], d[1]);
            count.put(k, count.getOrDefault(k, 0) + 1);
        }
        for (int v : count.values()) {
            res += v * (v - 1) / 2;
        }
        return res;
    }
```

**Python:**
```python
    def numEquivDominoPairs(self, A):
        return sum(v * (v - 1) / 2 for v in collections.Counter(tuple(sorted(x)) for x in A).values())
```
<br>

# Solution 2

We sum up the pairs right away during the loop.
`count[domino]` is the number of same dominoes that we have seen.

**C++:**
```cpp
    int numEquivDominoPairs(vector<vector<int>>& dominoes) {
        unordered_map<int, int> count;
        int res = 0;
        for (auto& d : dominoes) {
            res += count[min(d[0], d[1]) * 10 + max(d[0], d[1])]++;
        }
        return res;
    }
```

</p>


### [Java/Python 3] O(n) code with brief explanation and analysis.
- Author: rock
- Creation Date: Sun Jul 21 2019 12:02:41 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 09 2020 14:57:39 GMT+0800 (Singapore Standard Time)

<p>
**Explanation of `cnt += paris`** credit to **@wunanpty**
I want to explain why here we accumulate count like: cnt += pairs;
for example our map is

map: <23, 1> <24, 1>
current key = 23, so pair = 1, count += 1 -> count = 1 (which means we have 2 dominoes has same key, 2 dominoes is 1 pair)
map become: <23, 2><24, 1>

current key = 23, so pair = 2, count += 2 -> count = 3 (which means we have 3 dominoes has same key, 3 dominoes is 3 pair -> (a, b)(a,c)(b,c))
map become :<23, 3><24, 1>

current key = 23, so pair = 3, count += 3 -> count = 6 (4 dominoes has same key, 4 dominoes is 6 pair ->(a,b)(a,c)(a,d)(b,c)(b,d)(c,d))
map become :<23, 4><24, 1>

**End**

**Q & A:**
**Q:** 
1. Why do we use this for min * 10 + max, encoding?  how to decide on encoding?  
2. what if the maximum is infinite?

**A:**
1. We do NOT have to use `10`, as long as the number `m` > `max(dominoes)`, there will be no collision; In addition, the corresponding decoding is `encoded / m`, `encoded % m`;
2. .If the size of the number is limited, we can convert it to a String. 

**End of Q & A**

----
1. Encode each domino as min * 10 + max, where min and max are the smaller and bigger values of each domino, respectively;
2. Use a HashMap to store the number of dominoes with same encoding, and for each encoding (key), count the newly found pairs;

**Java:**
```
    public int numEquivDominoPairs(int[][] dominoes) {
        int cnt = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int[] a : dominoes) {
            int max = Math.max(a[0], a[1]), min = Math.min(a[0], a[1]);
            int key = min * 10 + max;
            int pairs = map.getOrDefault(key, 0); // the number of dominoes already in the map is the number of the newly found pairs.
            cnt += pairs;
            map.put(key, 1 + pairs);
                               
        }
        return cnt;
    }
```

----
**Python 3:**

```
    def numEquivDominoPairs(self, dominoes: List[List[int]]) -> int:
        d = {}
        cnt = 0
        for a, b in dominoes:
            key = min(a, b) * 10 + max(a, b) 
            if key in d:
                cnt += d[key] # the number of dominoes already in the map is the number of the newly found pairs.
                d[key] += 1
            else:
                d[key] = 1   
        return cnt
```

**Analysis:**

Since `1 <= dominoes[i][j] <= 9`, there are at most `9 * (9 + 1) / 2 = 45` encodings.
Time: O(n), space: O(45), where n = dominoes.length.
</p>


### Easy Without HashMap Java Solution  Runtime 0ms
- Author: Frankenstein32
- Creation Date: Tue Mar 24 2020 13:57:12 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 24 2020 14:04:56 GMT+0800 (Singapore Standard Time)

<p>
__Idea__ 
```
The main idea is to store each domino pair as a number for example..
[1,2] it will be stored as 12 and for domino [2,1] it is same as the above domino 
according to the question so this will also be stored as number 12. 

Maximum range for each number is 9 so the array which will store the frequency of each number will be of size100.
At the end, In the freq array we will have freq of each type of domino and now we need to do nCr of those values.
See the example to support this above line..,
[[1,2], [2,1], [1,2], [5,6], [6,5], [1,2]] 
Here [1,2] this type of domino are 4 and [5,6] this type of domino are 2. 
The ans will be 4C2 + 2C2. so 4! \xF7 (2! * 2!) + 2! \xF7 (2! * 1!). 
```


To shorten the Formula see the below picture...,!
![image](https://assets.leetcode.com/users/frankenstein32/image_1585029327.png)

```
class Solution {
    public int numEquivDominoPairs(int[][] dominoes) {
        int[] freq = new int[100];
        for(int[] d : dominoes){
            int x = Math.min(d[0],d[1]);
            int y = Math.max(d[1],d[0]);
            freq[x*10 + y]++;
        }
        int res = 0;
        for(int i = 0;i < 100;i++){
            if(freq[i] > 0){
                res += ((freq[i] * (freq[i] - 1)) / 2);
            }       
        }
        return res;
    }
}
```
</p>


