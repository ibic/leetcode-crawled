---
title: "Students With Invalid Departments"
weight: 1566
#id: "students-with-invalid-departments"
---
## Description
<div class="description">
<p>Table: <code>Departments</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| name          | varchar |
+---------------+---------+
id is the primary key of this table.
The table has information about the id of each department of a university.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Students</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| name          | varchar |
| department_id | int     |
+---------------+---------+
id is the primary key of this table.
The table has information about the id of each student at a university and the id of the department he/she studies at.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the id and the name of all students who are enrolled in departments that no longer exists.</p>

<p>Return the result table in any order.</p>

<p>The query result format is in the following example:</p>

<pre>
Departments table:
+------+--------------------------+
| id   | name                     |
+------+--------------------------+
| 1    | Electrical Engineering   |
| 7    | Computer Engineering     |
| 13   | Bussiness Administration |
+------+--------------------------+

Students table:
+------+----------+---------------+
| id   | name     | department_id |
+------+----------+---------------+
| 23   | Alice    | 1             |
| 1    | Bob      | 7             |
| 5    | Jennifer | 13            |
| 2    | John     | 14            |
| 4    | Jasmine  | 77            |
| 3    | Steve    | 74            |
| 6    | Luis     | 1             |
| 8    | Jonathan | 7             |
| 7    | Daiana   | 33            |
| 11   | Madelynn | 1             |
+------+----------+---------------+

Result table:
+------+----------+
| id   | name     |
+------+----------+
| 2    | John     |
| 7    | Daiana   |
| 4    | Jasmine  |
| 3    | Steve    |
+------+----------+

John, Daiana, Steve and Jasmine are enrolled in departments 14, 33, 74 and 77 respectively. department 14, 33, 74 and 77 doesn&#39;t exist in the Departments table.
</pre>
</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple SQL beats 100%
- Author: MrDelhi
- Creation Date: Fri Feb 14 2020 06:54:06 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Feb 14 2020 06:54:06 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT id, name FROM Students
WHERE department_id not in (SELECT id from Departments)
```
</p>


### MySQL two easy solutions: OUTER JOIN and SUBQUERY
- Author: BREEZEYJ
- Creation Date: Mon Feb 17 2020 01:07:05 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 17 2020 01:12:56 GMT+0800 (Singapore Standard Time)

<p>
```
Solution 1:

SELECT s.id, s.name
FROM Departments d
RIGHT JOIN Students s
ON d.id = s.department_id
WHERE d.id IS NULL

Solution 2:

SELECT id, name
FROM Students 
WHERE department_id NOT IN (SELECT id FROM Departments)
```

</p>


### 3 MySQL solutions: 1.in; 2. left join; 3: exists
- Author: ddoudle
- Creation Date: Mon Mar 09 2020 12:28:36 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 30 2020 06:59:26 GMT+0800 (Singapore Standard Time)

<p>
1. sub-query + in
```
# Write your MySQL query statement below
SELECT id, name
FROM Students
WHERE department_id NOT IN (
    SELECT id 
    FROM Departments
)
```

2. sub-query + left join
```
# Write your MySQL query statement below
SELECT id, sname AS name
FROM (
    SELECT s.id, s.name AS sname, d.name AS dname
FROM Students s
LEFT JOIN Departments d
ON s.department_id = d.id
) AS ljt
WHERE dname IS NULL
```
3. EXISTS
```
# Write your MySQL query statement below
SELECT id, name
FROM Students s
WHERE NOT EXISTS (
    SELECT id
    FROM Departments 
    WHERE id = s.department_id
)
```
</p>


