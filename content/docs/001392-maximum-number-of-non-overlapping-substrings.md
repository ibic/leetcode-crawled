---
title: "Maximum Number of Non-Overlapping Substrings"
weight: 1392
#id: "maximum-number-of-non-overlapping-substrings"
---
## Description
<div class="description">
<p>Given a string <code>s</code>&nbsp;of lowercase letters, you need to find the maximum number of <strong>non-empty</strong> substrings of&nbsp;<code>s</code>&nbsp;that meet the following conditions:</p>

<ol>
	<li>The substrings do not overlap, that is for any two substrings <code>s[i..j]</code> and <code>s[k..l]</code>, either <code>j &lt; k</code> or <code>i &gt; l</code>&nbsp;is true.</li>
	<li>A substring that contains a certain character&nbsp;<code>c</code>&nbsp;must also contain all occurrences of <code>c</code>.</li>
</ol>

<p>Find <em>the maximum number of substrings that meet the above conditions</em>. If there are multiple solutions with the same number of substrings, <em>return the one with minimum total length.&nbsp;</em>It can be shown that there exists a unique solution of minimum total length.</p>

<p>Notice that you can return the substrings in <strong>any</strong> order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;adefaddaccc&quot;
<strong>Output:</strong> [&quot;e&quot;,&quot;f&quot;,&quot;ccc&quot;]
<b>Explanation:</b>&nbsp;The following are all the possible substrings that meet the conditions:
[
&nbsp; &quot;adefaddaccc&quot;
&nbsp; &quot;adefadda&quot;,
&nbsp; &quot;ef&quot;,
&nbsp; &quot;e&quot;,
  &quot;f&quot;,
&nbsp; &quot;ccc&quot;,
]
If we choose the first string, we cannot choose anything else and we&#39;d get only 1. If we choose &quot;adefadda&quot;, we are left with &quot;ccc&quot; which is the only one that doesn&#39;t overlap, thus obtaining 2 substrings. Notice also, that it&#39;s not optimal to choose &quot;ef&quot; since it can be split into two. Therefore, the optimal way is to choose [&quot;e&quot;,&quot;f&quot;,&quot;ccc&quot;] which gives us 3 substrings. No other solution of the same number of substrings exist.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abbaccd&quot;
<strong>Output:</strong> [&quot;d&quot;,&quot;bb&quot;,&quot;cc&quot;]
<b>Explanation: </b>Notice that while the set of substrings [&quot;d&quot;,&quot;abba&quot;,&quot;cc&quot;] also has length 3, it&#39;s considered incorrect since it has larger total length.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10^5</code></li>
	<li><code>s</code>&nbsp;contains only lowercase English letters.</li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Amazon - 4 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java Greedy O(n)
- Author: votrubac
- Creation Date: Sun Jul 19 2020 12:09:57 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 23 2020 04:51:49 GMT+0800 (Singapore Standard Time)

<p>
Oh, human, so many edge cases... **Update**: see below for FAQ.

The greedy logic is quite simple though:

- There could be no more that 26 valid substrings; each potential valid substring starts from the first occurrence of a given character.
- If we find a valid substring, and then another valid substring within the first substring - we can ignore the larger substring.
	- E.g. if we find "abccba", and then "bccb", and then "cc", we only care about "cc". This can be easily proven by a contradiction.

**Algorithm**
1. First, collect left (`l`) and right(`r`) indices for every character. 
2. Then, go through the string. If the current index `i` is the left index for the character `s[i]`, it may be a valid substring. 
	- We then use a sliding window pattern to find the length of the substring. 
		- If we find a character that appears before `i` - we do not have a valid string starting at `i`.
	- If substring is valid, we remember it, as well as it\'s right edge.
	- This logic, repeated, will ensure that we find the valid substring with the smallest right edge.
3. If the valid substring starts after the previous right edge, there is no overlap. The previous substring should be included into the result.

**FAQ**
1. Why the answer for "abab" is "abab", and not "bab"?
	- According to condition 2 (see the problem description): a substring containing \'a\' must also contain all occurrences of \'a\'. So, "bab" is simply not a valid substring. This is checked by this line in the code below: `if (l[s[j] - \'a\'] < i)`.
2. The time complexity seems to be O(n ^ 2). There are two nested loops, each goes through `n` elements in the worst case.
	- Correct, the inner loop could go through `n` elements, but we will do it no more than 26 times. We only search for a substring starting form indexes in `l`, and we have 26 such indexes. This is checked by this line: ` if (i == l[s[i] - \'a\'])`.
	- Alternatively, instead of going through `n` elements in the outer loop, we can just go iterate through indexes in `l` directly. However, we need to sort `l` in that case, which complicates the implementation a bit. Some folks suggested to use a stack, but the implementation was just longer :)
3. Why do we need to do `res.push_back("")`?
	-  I am using the back of the result to track the **last valid** substring. We can use a string variable instead, and push that variable to the result. However, that will require another check in the end to make sure the latest substring is included.
	-  We add a new element to the result when a new valid substring that does not overlap with the **last valid** substring (`i > right`). Otherwise, we just keep on updating the last valid substring.
		- We also add a new element for the very first substring (`res.empty()`).

**C++**
```cpp
int checkSubstr(string &s, int i, vector<int> &l, vector<int> &r) {
    int right = r[s[i] - \'a\'];
    for (auto j = i; j <= right; ++j) {
        if (l[s[j] - \'a\'] < i)
            return -1;
        right = max(right, r[s[j] - \'a\']);
    }
    return right;
}
vector<string> maxNumOfSubstrings(string s) {
    vector<int> l(26, INT_MAX), r(26, INT_MIN);
    vector<string> res;
    for (int i = 0; i < s.size(); ++i) {
        l[s[i] - \'a\'] = min(l[s[i] - \'a\'], i);
        r[s[i] - \'a\'] = i;
    }
    int right = -1;
    for (int i = 0; i < s.size(); ++i) {
        if (i == l[s[i] - \'a\']) {
            int new_right = checkSubstr(s, i, l, r);
            if (new_right != -1) {
                if (i > right)
                    res.push_back("");                     
                right = new_right;
                res.back() = s.substr(i, right - i + 1);
            }
        }
    }
    return res;
}
```

**Java**
```java
int checkSubstr(String s, int i, int l[], int r[]) {
    int right = r[s.charAt(i) - \'a\'];
    for (int j = i; j <= right; ++j) {
        if (l[s.charAt(j) - \'a\'] < i)
            return -1;
        right = Math.max(right, r[s.charAt(j) - \'a\']);
    }
    return right;
}    
public List<String> maxNumOfSubstrings(String s) {
    int l[] = new int[26], r[] = new int[26];
    Arrays.fill(l, s.length());
    var res = new ArrayList<String>();
    for (int i = 0; i < s.length(); ++i) {
        var ch = s.charAt(i) - \'a\';
        l[ch] = Math.min(l[ch], i);
        r[ch] = i;
    }
    int right = -1;
    for (int i = 0; i < s.length(); ++i)
        if (i == l[s.charAt(i) - \'a\']) {
            int new_right = checkSubstr(s, i, l, r);
            if (new_right != -1) {
                if (i > right)
                    res.add("");                     
                right = new_right;
                res.set(res.size() - 1, s.substring(i, right + 1));
            }
        }
    return res;
}
```
**Complexity Analysis**
- Time: O(n). In the worst case, we search for substring 26 times, and each search is O(n)
- Memory: O(1). We store left and right positions for 26 characters.
	- For the complexity analysis purposes, we ignore memory required by inputs and outputs.

</p>


### [C++/Java/Python] Interval Scheduling Maximization (ISMP)
- Author: karutz
- Creation Date: Mon Jul 20 2020 01:02:04 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 22 2020 17:56:15 GMT+0800 (Singapore Standard Time)

<p>
First find all the possible substrings. There will be at most one for each letter in `s`. 
If we start at the first occurence of each letter and keep expanding the range to cover all occurences, we\'ll find all the substrings in `O(26 * n)` time.

Once we\'ve found all the possible substrings, this is a standard problem:
*interval scheduling maximization problem (ISMP)* (https://en.wikipedia.org/wiki/Interval_scheduling)

We can solve this in `O(n)` time by greedily taking the next non-overlapping substring with the left-most endpoint.

**Python**
```python
class Solution:
    def maxNumOfSubstrings(self, s):
        fst = { c : s.index(c) for c in set(s) }
        lst = { c : s.rindex(c) for c in set(s) }
        
        intervals = []
        for c in set(s):
            b, e = fst[c], lst[c]
            i = b
            while i <= e:
                b = min(b, fst[s[i]])
                e = max(e, lst[s[i]])
                i += 1
            if b == fst[c]:
                intervals.append((e, b))
        
        intervals.sort()
        ans, prev = [], -1
        for e, b in intervals:
            if b > prev:
                ans.append(s[b:e + 1])
                prev = e
        
        return ans
```

**C++**
```c++
class Solution {
public:
    vector<string> maxNumOfSubstrings(string s) {
        int n = s.length();
        
        vector<int> fst(26, INT_MAX);
        vector<int> lst(26, INT_MIN);
        for (int i = 0; i < n; i++) {
            fst[s[i] - \'a\'] = min(fst[s[i] - \'a\'], i);
            lst[s[i] - \'a\'] = max(lst[s[i] - \'a\'], i);
        }
        
        vector<pair<int, int>> t;
        for (int i = 0; i < 26; i++) {
            if (fst[i] < n) {
                int b = fst[i];
                int e = lst[i];
                for (int j = b; j <= e; j++) {
                    b = min(b, fst[s[j] - \'a\']);
                    e = max(e, lst[s[j] - \'a\']);
                }
                if (b == fst[i]) {
                    t.emplace_back(e, b);
                }
            }
        }
        
        sort(t.begin(), t.end());
        vector<string> ans;
        int prev = -1;
        for (auto &[e, b] : t) {
            if (b > prev) {
                ans.push_back(s.substr(b, e - b + 1));
                prev = e;
            }
        }
        
        return ans;
    }
};
```
**Java**
```java
class Solution {
    private static class Interval {
        public int b;
        public int e;

        public Interval(int b, int e) {
            this.b = b;
            this.e = e;
        }
    }
    
    public List<String> maxNumOfSubstrings(String s) {
        int n = s.length();
        int[] vals = new int[n];
        for (int i = 0; i < n; i++) {
            vals[i] = s.charAt(i) - \'a\';
        }
        
        int[] fst = new int[26];
        int[] lst = new int[26];
        for (int i = 0; i < n; i++) {
            lst[vals[i]] = i;
        }
        for (int i = n - 1; i >= 0; i--) {
            fst[vals[i]] = i;
        }
        
        List<Interval> t = new ArrayList<Interval>();
        for (int i = 0; i < 26; i++) {
            if (fst[i] < n) {
                int b = fst[i];
                int e = lst[i];
                for (int j = b; j <= e; j++) {
                    b = Math.min(b, fst[vals[j]]);
                    e = Math.max(e, lst[vals[j]]);
                }
                if (b == fst[i]) {
                    t.add(new Interval(b, e));
                }
            }
        }
        
        Collections.sort(t, Comparator.comparing(i -> i.e));
        List<String> ans = new ArrayList<String>();
        int prev = -1;
        for (Interval i : t) {
            if (i.b > prev) {
                ans.add(s.substring(i.b, i.e + 1));
                prev = i.e;
            }
        }
        
        return ans;
    }
}
```

**Shorter solution**

We can avoid sorting the intervals if we make sure to find them in earliest-finish-time order.

- Python
```python
class Solution:
    def maxNumOfSubstrings(self, s):
        fst = { c : s.index(c) for c in set(s) }
        lst = { c : s.rindex(c) for c in set(s) }
        
        ans, prev = [], -1
        for i in sorted(lst.values()):
            b, e = fst[s[i]], lst[s[i]]
            j = e
            while j >= b and b > prev and e == i:
                b = min(b, fst[s[j]])
                e = max(e, lst[s[j]])
                j -= 1
            if b > prev and e == i:
                ans.append(s[b:e + 1])
                prev = e
        
        return ans
```

- C++
```c++
class Solution {
public:
    vector<string> maxNumOfSubstrings(string s) {
        int n = s.length(), fst[26], lst[26];
        vector<string> ans;
        for (int i = 0; i < n; ++i) lst[s[i] - \'a\'] = i;
        for (int i = n - 1; i >= 0; --i) fst[s[i] - \'a\'] = i;
        for (int i = 0, prev = -1; i < n; ++i) {
            if (i == lst[s[i] - \'a\']) {
                int b = fst[s[i] - \'a\'];
                int e = lst[s[i] - \'a\'];
                for (int j = e - 1; j > b && b > prev && e == i; --j) {
                    b = min(b, fst[s[j] - \'a\']);
                    e = max(e, lst[s[j] - \'a\']);
                }
                if (b > prev && e == i) {
                    ans.push_back(s.substr(b, e - b + 1));
                    prev = e;
                }
            }
        }
        return ans;
    }
};
```
</p>


### Java O(n) solution with comment
- Author: DCXiaoBing
- Creation Date: Sun Jul 19 2020 12:13:01 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 23 2020 04:32:11 GMT+0800 (Singapore Standard Time)

<p>
1. Intuitively, we need to get each character\'s range.
2. If we get each character\'s range, we can change this to a very famalier quesiton: How many meeting we can hold if we have only one meeting room
3. Pitfall is that, in each character\'s range, it could contain other characters(eg, origin string is "abab", substring "bab"), so we need to extend the range to cover them.
4. Time complexity:
    - Gather ranges will cost O(n). n is length of string
    - There will be at most 26 ranges, extend each range at most take O(n) time(Visit each index only once). So this part       - takes O(26 * n) time
    - Sort the ranges will cost O(26 * log26)
    - Generate answers wil cost O(26 * n) time
    - So total time complexity is O(n). Thanks @s_tsat for pointing out.

```
class Solution {
    
    public List<String> maxNumOfSubstrings(String ss) {
        char[] s = ss.toCharArray();
        HashMap<Character, int[]> range = new HashMap<>();
        
		// get each character\'s range
        for(int i = 0; i < s.length; i++) {
            char cur = s[i];
            if(range.containsKey(cur)) range.get(cur)[1] = i;
            else range.put(cur, new int[]{i, i});
        }
        
		// extend the range
        List<int[]> values = new ArrayList<>(range.values());
        for(int[] r : values) {
            
			// gather all characters in the range r[0]...r[1]
            HashSet<Character> seen = new HashSet<>();
            int size = 0, min = r[0], max = r[1];
            for(int i = r[0]; i <= r[1]; i++) {
                if(seen.add(s[i])) {
                    int temp[] = range.get(s[i]);
                    min = Math.min(min, temp[0]);
                    max = Math.max(max, temp[1]);
                }
            }
            
			// extend the range untile the range will not cover any other new character
            while(seen.size() != size) {
                size = seen.size();
                int nMin = min;
                int nMax = max;
                
                for(int i = min; i < r[0]; i++) {
                    if(seen.add(s[i])){
                        int temp[] = range.get(s[i]);
                        nMin = Math.min(nMin, temp[0]);
                        nMax = Math.max(nMax, temp[1]);
                    }
                }
                
                for(int i = r[1] + 1; i <= max; i++) {
                    if(seen.add(s[i])){
                        int temp[] = range.get(s[i]);
                        nMin = Math.min(nMin, temp[0]);
                        nMax = Math.max(nMax, temp[1]);
                    }
                }
                
                r[0] = min; // new end point
                r[1] = max;
                min = nMin; // new start point
                max = nMax;
                // System.out.println("====");
            }
        }

		// meeting room solution
        Collections.sort(values, (a, b) -> a[1] - b[1]);
		
        int p = -1;
        List<String> res = new ArrayList<>();
        
        for(int[] r : values) {
            if(r[0] > p) { // can be accepted
                p = r[1]; // p means lastest used index
                res.add(ss.substring(r[0], r[1] + 1));
            }
        }
        return res;
    }
}
```

Thanks @Anonymouso for improving my code.
Thanks @igor84 for pointing out a bug in my code.
</p>


