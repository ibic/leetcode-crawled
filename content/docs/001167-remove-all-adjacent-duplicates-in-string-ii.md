---
title: "Remove All Adjacent Duplicates in String II"
weight: 1167
#id: "remove-all-adjacent-duplicates-in-string-ii"
---
## Description
<div class="description">
<p>Given a string&nbsp;<code>s</code>, a <em>k</em>&nbsp;<em>duplicate removal</em>&nbsp;consists of choosing <code>k</code>&nbsp;adjacent and equal letters from&nbsp;<code>s</code> and removing&nbsp;them causing the left and the right side of the deleted substring to concatenate together.</p>

<p>We repeatedly make <code>k</code> duplicate removals on <code>s</code> until we no longer can.</p>

<p>Return the final string after all such duplicate removals have been made.</p>

<p>It is guaranteed that the answer is unique.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abcd&quot;, k = 2
<strong>Output:</strong> &quot;abcd&quot;
<strong>Explanation: </strong>There&#39;s nothing to delete.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;deeedbbcccbdaa&quot;, k = 3
<strong>Output:</strong> &quot;aa&quot;
<strong>Explanation: 
</strong>First delete &quot;eee&quot; and &quot;ccc&quot;, get &quot;ddbbbdaa&quot;
Then delete &quot;bbb&quot;, get &quot;dddaa&quot;
Finally delete &quot;ddd&quot;, get &quot;aa&quot;</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;pbbcggttciiippooaais&quot;, k = 2
<strong>Output:</strong> &quot;ps&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10^5</code></li>
	<li><code>2 &lt;= k &lt;= 10^4</code></li>
	<li><code>s</code> only contains lower case English letters.</li>
</ul>

</div>

## Tags
- Stack (stack)

## Companies
- Bloomberg - 17 (taggedByAdmin: false)
- Facebook - 6 (taggedByAdmin: false)
- Roblox - 6 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- FactSet - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Brute Force

We can do exactly what the problem asks: count repeating adjacent letters and remove them when the count reaches `k`. Then, we do it again until there is nothing to remove.

![Brute Force Illustration](../Figures/1209/1209_approach1.png)

**Algorithm**

1. Remember the length of the string.

2. Iterate through the string:

    - If the current character is the same as the one before, increase the count.

        - Otherwise, reset the count to `1`.

    - If the count equals `k`,  erase last `k` characters.

3. If the length of the string was changed, repeat starting from the first step.

<iframe src="https://leetcode.com/playground/xGxVx4eh/shared" frameBorder="0" width="100%" height="327" name="xGxVx4eh"></iframe>

**Complexity Analysis**

- Time complexity: $$\mathcal{O}(n^2 / k)$$, where $$n$$ is a string length. We scan the string no more than $$n / k$$ times.

- Space complexity: $$\mathcal{O}(1)$$. A copy of a string may be created in some languages, however, the algorithm itself only uses the current string.

---

#### Approach 2: Memoise Count

If you observe how the count changes in the approach above, you may have an idea to store it for each character. That way, we do not have to start from the beginning each time we remove a substring. This approach will give us linear time complexity, and the trade-off is the extra memory to store counts.

**Algorithm**

1. Initialize `counts` array of size `n`.

2. Iterate through the string:

    - If the current character is the same as the one before, set `counts[i]` to `counts[i - 1] + 1`.

        - Otherwise, set `counts[i]` to `1`.

    - If `counts[i]` equals `k`,  erase last `k` characters and decrease `i` by `k`.

<iframe src="https://leetcode.com/playground/QCzgSYMN/shared" frameBorder="0" width="100%" height="327" name="QCzgSYMN"></iframe>

**Complexity Analysis**

- Time complexity: $$\mathcal{O}(n)$$, where $$n$$ is a string length. We process each character in the string once.

- Space complexity: $$\mathcal{O}(n)$$ to store the count for each character.

---

#### Approach 3: Stack

Instead of storing counts for each character, we can use a stack. When a character does not match the previous one, we push `1` to the stack. Otherwise, we increment the count on the top of the stack.

![Stack Illustration](../Figures/1209/1209_approach3.png)

**Algorithm**

1. Iterate through the string:

    - If the current character is the same as the one before, increment the count on the top of the stack.

        - Otherwise, push `1` to the stack.

    - If the count on the top of the stack equals `k`, erase last `k` characters and pop from the stack.

> Note that, since `Integer` is immutable in Java, we need to pop the value first, increment it, and then push it back (if it's less than `k`).

<iframe src="https://leetcode.com/playground/hU9u6BRm/shared" frameBorder="0" width="100%" height="361" name="hU9u6BRm"></iframe>

**Complexity Analysis** <a name="approach3complexity"></a>

- Time complexity: $$\mathcal{O}(n)$$, where $$n$$ is a string length. We process each character in the string once.

- Space complexity: $$\mathcal{O}(n)$$ for the stack.

---
#### Approach 4: Stack with Reconstruction

If we store both the count and the character in the stack, we do not have to modify the string. Instead, we can reconstruct the result from characters and counts in the stack.

**Algorithm**

1. Iterate through the string:

    - If the current character is the same as on the top of the stack, increment the count.

        - Otherwise, push `1` and the current character to the stack.

    - If the count on the top of the stack equals `k`, pop from the stack.

2. Build the result string using characters and counts in the stack.

<iframe src="https://leetcode.com/playground/mKwtSpdG/shared" frameBorder="0" width="100%" height="500" name="mKwtSpdG"></iframe>

**Complexity Analysis**

- Same as for [approach 3](#approach3complexity) above.

---

#### Approach 5: Two Pointers

This method was proposed by @[lee215](https://leetcode.com/lee215/), and we can use it to optimize string operations in approaches 2 and 3. Here, we copy characters within the same string using the fast and slow pointers. Each time we need to erase `k` characters, we just move the slow pointer `k` positions back.

![Two Pointers Illustration](../Figures/1209/1209_approach5.png)

**Algorithm**

1. Initialize the slow pointer (`j`) with zero.

2. Move the fast pointer (`i`) through the string:

    - Copy s[i] into s[j].

    - If `s[j]` is the same as `s[j - 1]`, increment the count on the top of the stack.

        - Otherwise, push `1` to the stack.

    - If the count equals `k`,  decrease `j` by `k` and pop from the stack.

3. Return `j` first characters of the string.

<iframe src="https://leetcode.com/playground/pMPzmxaS/shared" frameBorder="0" width="100%" height="378" name="pMPzmxaS"></iframe>

**Complexity Analysis**

- Same as for [approach 3](#approach3complexity) above.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Two Pointers and Stack Solution
- Author: lee215
- Creation Date: Sun Sep 29 2019 12:21:18 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 03 2020 10:14:44 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
If you read my post last month about [1047. Remove All Adjacent Duplicates In String](https://leetcode.com/problems/remove-all-adjacent-duplicates-in-string/discuss/294893/JavaC++Python-Two-Pointers-and-Stack)
you cannot solve this problem easier.
<br>

# **Solution 1: Two Pointers**
**java**
```java
    public String removeDuplicates(String s, int k) {
        int i = 0, n = s.length(), count[] = new int[n];
        char[] stack = s.toCharArray();
        for (int j = 0; j < n; ++j, ++i) {
            stack[i] = stack[j];
            count[i] = i > 0 && stack[i - 1] == stack[j] ? count[i - 1] + 1 : 1;
            if (count[i] == k) i -= k;
        }
        return new String(stack, 0, i);
    }
```

**C++**
```cpp
    string removeDuplicates(string s, int k) {
        int i = 0, n = s.length();
        vector<int> count(n);
        for (int j = 0; j < n; ++j, ++i) {
            s[i] = s[j];
            count[i] = i > 0 && s[i - 1] == s[j] ? count[i - 1] + 1 : 1;
            if (count[i] == k) i -= k;
        }
        return s.substr(0, i);
    }
```
<br>

# **Solution 2: Stack**
Save the character `c` and its count to the `stack`.
If the next character `c` is same as the last one, increment the count.
Otherwise push a pair `(c, 1)` into the stack.
I used a dummy element `(\'#\', 0)` to avoid empty stack.
<br>

**Java**
By @motorix
We can use StringBuilder as a stack.
```java
    public String removeDuplicates(String s, int k) {
        int[] count = new int[s.length()];
        StringBuilder sb = new StringBuilder();
        for(char c : s.toCharArray()) {
            sb.append(c);
            int last = sb.length()-1;
            count[last] = 1 + (last > 0 && sb.charAt(last) == sb.charAt(last-1) ? count[last-1] : 0);
            if(count[last] >= k) sb.delete(sb.length()-k, sb.length());
        }
        return sb.toString();
    }
```

**C++**
```cpp
    string removeDuplicates(string s, int k) {
        vector<pair<int, char>> stack = {{0, \'#\'}};
        for (char c: s) {
            if (stack.back().second != c) {
                stack.push_back({1, c});
            } else if (++stack.back().first == k)
                stack.pop_back();
        }
        string res;
        for (auto & p : stack) {
            res.append(p.first, p.second);
        }
        return res;
    }
```

**Python:**
```python
    def removeDuplicates(self, s, k):
        stack = [[\'#\', 0]]
        for c in s:
            if stack[-1][0] == c:
                stack[-1][1] += 1
                if stack[-1][1] == k:
                    stack.pop()
            else:
                stack.append([c, 1])
        return \'\'.join(c * k for c, k in stack)
```
<br>

# **Complexity**
Time `O(N)` for one pass
Space `O(N)`
<br>
</p>


### [Java] Simple Stack, O(N) ~ 10ms, Clean code easy to understand
- Author: hiepit
- Creation Date: Sun Sep 29 2019 12:22:51 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Apr 08 2020 09:25:28 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1: Simple Version ~ 20ms**
```java
class Solution {
    public String removeDuplicates(String s, int k) {
        Stack<Character> stackChar = new Stack<>();
        Stack<Integer> stackAdjCnt = new Stack<>();

        for (char c : s.toCharArray()) {
            if (!stackChar.isEmpty() && stackChar.peek() == c) {
                stackAdjCnt.push(stackAdjCnt.peek() + 1);
            } else {
                stackAdjCnt.push(1);
            }
            stackChar.push(c);
            if (stackAdjCnt.peek() == k) {
                for (int i = 0; i < k; i++) { // pop k adjacent equal chars
                    stackChar.pop();
                    stackAdjCnt.pop();
                }
            }
        }

        // Convert stack to string
        StringBuilder sb = new StringBuilder();
        while (!stackChar.empty()) {
            sb.append(stackChar.pop());
        }
        return sb.reverse().toString();
    }
}
```
Time & Space: `O(n)`, where `n` is the length of the string

**Solution 2: Optimized Version ~ 6ms**
```java
class Solution {
    public String removeDuplicates(String s, int k) {
        // LinkedList will be more efficient than Stack because Stack has to reallocate when size over capacity
        LinkedList<Adjacent> stack = new LinkedList<>();

        for (char c : s.toCharArray()) {
            if (!stack.isEmpty() && stack.peek().ch == c) {
                stack.peek().freq++;
            } else {
                stack.push(new Adjacent(c, 1));
            }
            if (stack.peek().freq == k) {
                stack.pop();
            }
        }

        // Convert linked list stack to string
        StringBuilder sb = new StringBuilder();
        while (stack.size() > 0) {
            Adjacent peek = stack.removeLast();
            for (int i = 0; i < peek.freq; i++) { // pop once
                sb.append(peek.ch);
            }
        }
        return sb.toString();
    }

    class Adjacent {
        char ch;
        int freq;

        public Adjacent(char ch, int freq) {
            this.ch = ch;
            this.freq = freq;
        }
    }
}
```
Time & Space: `O(n)`, where `n` is the length of the string
</p>


### [Java/Python 3] O(n) codes using Stack w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Sep 29 2019 15:33:50 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 02 2019 23:19:33 GMT+0800 (Singapore Standard Time)

<p>
1. Use Stack(s) to save adjacent duplicate chars and the corresponding occurrences, pop them out when the occurrences reach k;
2. Build String by the items remaining in Stack(s).

**Java**
**Method 1**
Use `javafx.util.Pair` to wrap a char and its frequency:
```
import javafx.util.Pair;

    public String removeDuplicates(String s, int k) {
        Deque<Pair<Character, Integer>> stk = new ArrayDeque<>();
        for (char c : s.toCharArray()) {
            if (stk.isEmpty() || c != stk.peek().getKey()) {
                stk.push(new Pair(c, 1));
            }else {
                int cnt = stk.pop().getValue() + 1;
                if (cnt != k) stk.push(new Pair(c, cnt));
            }
        }
        StringBuilder sb = new StringBuilder();
        while (!stk.isEmpty()) {
            int cnt = stk.peek().getValue();
            char c = stk.pop().getKey();
            while (cnt-- > 0) { sb.append(c); }
        }
        return sb.reverse().toString();
    }
```
**Method 2**
Since `2 <= k <= 10^4`, we can use `char` type, range `0~65535`, to count the frequency of the chars.
```
    public String removeDuplicates(String s, int k) {
        Deque<char[]> stk = new ArrayDeque<>();
        for (char c : s.toCharArray())
            if (stk.isEmpty() || c != stk.peek()[0])
                stk.push(new char[]{c, 1}); // here 1 is an int literal initializing char type variable.
            else if (++stk.peek()[1] == k)
                stk.pop();
        StringBuilder sb = new StringBuilder();
        while (!stk.isEmpty()) {
            while (stk.peekLast()[1]-- > 0) 
                sb.append(stk.peekLast()[0]);
            stk.pollLast();
        }
        return sb.toString();
    }
```
**Method 3**
If you are not comfortable with using `char` type to count, use 2 Stacks instead:
```
    public String removeDuplicates(String s, int k) {
        Deque<Integer> intStk = new ArrayDeque<>();
        Deque<Character> charStk = new ArrayDeque<>();
        for (char c : s.toCharArray()) {
            if (charStk.isEmpty() || c != charStk.peek()) {
                charStk.push(c);
                intStk.push(1);
            }else {
                int cnt = (intStk.pop() + 1) % k;
                if (cnt == 0) { charStk.pop(); }
                else { intStk.push(cnt); }
            }
        }
        StringBuilder sb = new StringBuilder();
        while (!intStk.isEmpty()) {
            int cnt = intStk.pop();
            char c = charStk.pop();
            while (cnt-- > 0) { sb.append(c); }
        }
        return sb.reverse().toString();
    }
````
----

**Python 3**
```
    def removeDuplicates(self, s: str, k: int) -> str:
        stk = []
        for c in s:
            if not stk or stk[-1][0] != c:
                stk.append((c, 1))
            else:
                cnt = (stk.pop()[1] + 1) % k
                if cnt:
                    stk.append((c, cnt))
        return \'\'.join(c * cnt for c, cnt in stk)
```
**Analysis:**

Time & space: O(n), where n = s.length().
</p>


