---
title: "Find Permutation"
weight: 459
#id: "find-permutation"
---
## Description
<div class="description">
<p>
By now, you are given a <b>secret signature</b> consisting of character 'D' and 'I'. 'D' represents a decreasing relationship between two numbers, 'I' represents an increasing relationship between two numbers. And our <b>secret signature</b> was constructed by a special integer array, which contains uniquely all the different number from 1 to n (n is the length of the secret signature plus 1). For example, the secret signature "DI" can be constructed by array [2,1,3] or [3,1,2], but won't be constructed by array [3,2,4] or [2,1,3,4], which are both illegal constructing special string that can't represent the "DI" <b>secret signature</b>.
</p>

<p>
On the other hand, now your job is to find the lexicographically smallest permutation of [1, 2, ... n] could refer to the given <b>secret signature</b> in the input.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> "I"
<b>Output:</b> [1,2]
<b>Explanation:</b> [1,2] is the only legal initial spectial string can construct secret signature "I", where the number 1 and 2 construct an increasing relationship.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> "DI"
<b>Output:</b> [2,1,3]
<b>Explanation:</b> Both [2,1,3] and [3,1,2] can construct the secret signature "DI", </br>but since we want to find the one with the smallest lexicographical permutation, you need to output [2,1,3]
</pre>
</p>

<p><b>Note:</b>
<li>The input string will only contain the character 'D' and 'I'.</li>
<li>The length of input string is a positive integer and will not exceed 10,000</li>
</p>
</div>

## Tags
- Greedy (greedy)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Using Stack [Accepted]

Let's revisit the important points of the given problem statement. For a given $$n$$, we need to use all the integers in the range $$(1,n)$$ to generate a lexicographically smallest permutation of these $$n$$ numbers which satsfies the pattern given in the string $$s$$.

Firstly, we note that the lexicographically smallest permutation that can be generated(irrelevant of the given pattern $$s$$) using the $$n$$ integers from $$(1,n)$$ is $$[1, 2, 3,.., n]$$(say $$min$$). Thus, while generating the required permutation, we can surely say that the permutation generated should be as close as possible to $$min$$.

Now, we can also note that the number generated will be the smallest possible only if the digits lying towards the most significant positions are as small as possible while satisfying the given pattern. Now, to understand how these observations help in providing the solution of the given problem, we'll look at a simple example.

Say, the given pattern $$s$$ is `"DDIIIID"`. This corresponds to $$n=8$$. Thus, the $$min$$ permutation possible will be `[1, 2, 3, 4, 5, 6, 7, 8]`. Now, to satisfy the first two `"DD"` pattern, we can observe that the best course of action to generate the smallest arrangement will be to rearrange only `1, 2, 3`, because these are the smallest numbers that can be placed at the three most significant positions to generate the smallest arrangement satisfying the given pattern till now, leading to the formation of `[3, 2, 1, 4, 5, 6, 7, 8]` till now. We can note that placing any number larger than 3 at any of these positions will lead to the generation of a lexicographically larger arrangement as discussed above.

We can also note that irrespective of how we rearrange the first three `1, 2, 3`, the relationship of the last number among them with the  next number always satisfies the criteria required for satisfying the first `I` in $$s$$. Further, note that, the pattern generated till now already satisfies the subpattern `"IIII"` in $$s$$. This will always be satisfied since the $$min$$ number considered originally always satisfies the increasing criteria.

Now, when we find the last `"D"` in the pattern $$s$$, we again need to make rearrangements in the last two positions only and we need to use only the numbers `7, 8` in such rearrangements at those positions. This is because, again, we would like to keep the larger number towards the least significant possible as much as possible to generate the lexicographically smallest arrangement. Thus, to satisfy the last `"D"` the best arrangement of the last two numbers is `8, 7` leading to the generation of `[3, 2, 1, 4, 5, 6, 8, 7]` as the required output.

Based on the above example, we can summarize that, to generate the required arrangement, we can start off with the $$min$$ number that can be formed for the given $$n$$. Then, to satisfy the given pattern $$s$$, we need to reverse only those subsections of the $$min$$ array which have a `D` in the pattern at their corresponding positions.

To perform these operations, we need not necessarily create the $$min$$ array first, because it simply consists of numbers from $$1$$ to $$n$$ in ascending order. 

To perform the operations discussed above, we can make use of a $$stack$$. We can start considering the numbers $$i$$ from $$1$$ to $$n$$. For every current number, whenver we find a `D` in the pattern $$s$$, we just push that number onto the $$stack$$. This is done so that, later on, when we find the next `I`, we can pop off these numbers from the stack leading to the formation of a reversed (descending) subpattern of those numbers corrresponding to the `D`'s in $$s$$(as discussed above). 

When we encounter an `I` in the pattern, as discussed above, we push the current number as well onto the $$stack$$ and then pop-off all the numbers on the $$stack$$ and append these numbers to the resultant pattern formed till now.

Now, we could reach the end of the pattern $$s$$ with a trail of `D`'s at the end. In this case, we won't find an ending `I` to pop-off the numbers on the $$stack$$. Thus, at the end, we push the value $$n$$ on the stack and then pop all the values on the $$stack$$ and append them to the resultant pattern formed till now. Now, if the second last character in $$s$$ was an `I`, $$n$$ will be appended at the end of the resultant arrangement correctly. If the second last character was a `D`, the reversed pattern appended at the end of the resultant arrangement will be reversed including the last number $$n$$. In both the cases, the resultant arrangement turns out to be correct. 

The following animation better illustrates the process.

!?!../Documents/484_Find_Permutation_1.json:1000,563!?!

Below code is inpired by [@horseno](http://leetcode.com/horseno)

<iframe src="https://leetcode.com/playground/oQngSEVg/shared" frameBorder="0" name="oQngSEVg" width="100%" height="394"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. $$n$$ numbers will be pushed and popped from the $$stack$$. Here, $$n$$ refers to the number of elements in the resultant arrangement.

* Space complexity : $$O(n)$$. The $$stack$$ can grow upto a depth of $$n$$ in the worst case.

---
#### Approach #2 Reversing the subarray [Accepted]

**Algorithm**

In order to reverse the subsections of the $$min$$ array, as discussed in the last approach, we can also start by initializing the resultant arrangement $$res$$ with the $$min$$ array i.e. by filling with elements $$(1,n)$$ in ascending order. Then, while traversing the pattern $$s$$, we can keep a track of the starting and ending indices in $$res$$ corresponding to the `D`'s in the pattern $$s$$, and reverse the portions of the sub-arrays in $$res$$ corresponding to these indices. The reasoning behind this remains the same as discussed in the last approach.

The following animation illustrates the process.

!?!../Documents/484_Find_Permutation_Reverse.json:1000,563!?!

<iframe src="https://leetcode.com/playground/JWJKE7wc/shared" frameBorder="0" name="JWJKE7wc" width="100%" height="462"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. The resultant array of size $$n$$ is traversed atmost three times, in the worst case e.g. "DDDDDD"

* Space complexity : $$O(1)$$. Constant extra space is used.

---
#### Approach #3 Two pointers [Accepted]

**Algorithm**

Instead of initializing the $$res$$ array once with ascending numbers, we can save one iteration over $$res$$ if we fill it on the fly. To do this, we can keep on filling the numbers in ascending order in $$res$$ for `I`'s found in the pattern $$s$$. Whenever we find a `D` in the pattern $$s$$, we can store the current position(counting from 1) being filled in the $$res$$ array in a pointer $$j$$. Now, whenever we find the first `I` following this last consecutive set of `D`'s, say at the $$i^{th}$$ position(counting from 1) in $$res$$, we know, that the elements from $$j^{th}$$ position to the $$i^{th}$$ position in $$res$$ need to be filled with the numbers from $$j$$ to $$i$$ in reverse order. Thus, we can fill the numbers in $$res$$ array starting from the $$j^{th}$$ position, with $$i$$ being the number filled at that position and continue the filling in descending order till reaching the $$i^{th}$$ position. In this way, we can generate the required arrangement without initializing $$res$$.

<iframe src="https://leetcode.com/playground/XWhYV6uZ/shared" frameBorder="0" name="XWhYV6uZ" width="100%" height="428"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n)$$. The resultant array of size $$n$$ is traversed atmost two times, in the worst case e.g. "DDDDDD"

* Space complexity : $$O(1)$$. Constant extra space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n) clean solution easy to understand
- Author: yuxiangmusic
- Creation Date: Sun Jan 22 2017 01:56:49 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 03:50:08 GMT+0800 (Singapore Standard Time)

<p>
For example, given ```IDIIDD``` we start with sorted sequence ```1234567```
Then for each ```k``` continuous ```D``` starting at index ```i``` we need to reverse ```[i, i+k]``` portion of the sorted sequence.

```
IDIIDD
1234567 // sorted
1324765 // answer
```
```
    public int[] findPermutation(String s) {
        int n = s.length(), arr[] = new int[n + 1]; 
        for (int i = 0; i <= n; i++) arr[i] = i + 1; // sorted
        for (int h = 0; h < n; h++) {
            if (s.charAt(h) == 'D') {
                int l = h;
                while (h < n && s.charAt(h) == 'D') h++;
                reverse(arr, l, h); 
            }   
        }   
        return arr;
    }   

    void reverse(int[] arr, int l, int h) {
        while (l < h) {
            arr[l] ^= arr[h];
            arr[h] ^= arr[l];
            arr[l] ^= arr[h];
            l++; h--;
        }   
    }
```
</p>


### 1-liner and 5-liner, visual explanation
- Author: StefanPochmann
- Creation Date: Sun Jan 22 2017 07:06:48 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 01:14:33 GMT+0800 (Singapore Standard Time)

<p>
If it's all just `I`, then the answer is the numbers in ascending order. And if there are streaks of `D`, then just reverse the number streak under each:

![0_1485039424299_Find Permutation.png](/uploads/files/1485039426666-find-permutation.png) 

My 5-liner does exactly that. Start from sorted, then find the `D`-streaks and reverse the numbers under them:

    def findPermutation(self, s):
        a = range(1, len(s) + 2)
        for m in re.finditer('D+', s):
            i, j = m.start(), m.end() + 1
            a[i:j] = a[i:j][::-1]
        return a

My 1-liner tells `sorted` that the (larger) number `i` comes before the (smaller) number `j` iff they're both under the same `D`-streak, i.e., iff there's no `I` between them. (I'm not totally sure that `i` will always be the larger number, but it appears to be the case).

    def findPermutation(self, s):
        return sorted(range(1, len(s) + 2), cmp=lambda i, j: -('I' not in s[j-1:i-1]))
</p>


### Greedy O(n) JAVA solution with explanation
- Author: dettier
- Creation Date: Sun Jan 22 2017 01:35:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 22 2017 01:35:29 GMT+0800 (Singapore Standard Time)

<p>
Idea is pretty simple. There are 2 possibilities:
1. String starts with `"I"`. Then we should use 1 as the first item.
2. String starts with `"D..DI"` (`k` letters) or the string is just `"D...D"`. In this case we should use `k, k - 1, ..., 1` to get lexicographically smallest permutation.

Then we proceed computing the rest of the array. Also we should increase `min` variable that is used to store minimum value in remaining part of the array.

```
public int[] findPermutation(String s) {
    
    s = s + ".";
    int[] res = new int[s.length()];
    int min = 1, i = 0;

    while (i < res.length) {
        if (s.charAt(i) != 'D') {
            res[i++] = min++;
        } else {
            int j = i;
            while (s.charAt(j) == 'D') j++;
            for (int k = j; k >= i; k--)
                res[k] = min++;
            i = j + 1;
        }
    }

    return res;
}
```
</p>


