---
title: "3Sum Smaller"
weight: 243
#id: "3sum-smaller"
---
## Description
<div class="description">
<p>Given an array of <code>n</code> integers <code>nums</code> and an integer&nbsp;<code>target</code>, find the number of index triplets <code>i</code>, <code>j</code>, <code>k</code> with <code>0 &lt;= i &lt; j &lt; k &lt; n</code> that satisfy the condition <code>nums[i] + nums[j] + nums[k] &lt; target</code>.</p>

<p><b style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;">Follow up:</b> Could you solve it in <code>O(n<sup>2</sup>)</code> runtime?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [-2,0,1,3], target = 2
<strong>Output:</strong> 2
<strong>Explanation:</strong> Because there are two triplets which sums are less than 2:
[-2,0,1]
[-2,0,3]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [], target = 0
<strong>Output:</strong> 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [0], target = 0
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == nums.length</code></li>
	<li><code>0 &lt;= n &lt;= 300</code></li>
	<li><code>-100 &lt;= nums[i] &lt;= 100</code></li>
	<li><code>-100 &lt;= target &lt;= 100</code></li>
</ul>

</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)

## Companies
- Google - 3 (taggedByAdmin: true)
- IBM - 3 (taggedByAdmin: false)
- Mathworks - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach #1 (Brute Force) [Time Limit Exceeded]

The brute force approach is to find every possible triplets $$(i, j, k)$$ subjected to $$i < j < k$$ and test for the condition.

**Complexity analysis**

* Time complexity : $$O(n^3)$$.
The total number of such triplets is $$\binom{n}{3}$$, which is $$\frac{n!}{(n - 3)! \times 3!} = \frac{n \times (n - 1) \times (n - 2)}{6}$$. Therefore, the time complexity of the brute force approach is $$O(n^3)$$.

* Space complexity : $$O(1)$$.

---
#### Approach #2 (Binary Search) [Accepted]

Before we solve this problem, it is helpful to first solve this simpler *twoSum* version.

>Given a $$nums$$ array, find the number of index pairs $$i$$, $$j$$ with $$0 \leq i < j < n$$ that satisfy the condition $$nums[i] + nums[j] < target$$

If we sort the array first, then we could apply binary search to find the largest index $$j$$ such that $$nums[i] + nums[j] < target$$ for each $$i$$. Once we found that largest index $$j$$, we know there must be $$j-i$$ pairs that satisfy the above condition with $$i$$'s value fixed.

Finally, we can now apply the *twoSum* solution to *threeSum* directly by wrapping an outer for-loop around it.

```java
public int threeSumSmaller(int[] nums, int target) {
    Arrays.sort(nums);
    int sum = 0;
    for (int i = 0; i < nums.length - 2; i++) {
        sum += twoSumSmaller(nums, i + 1, target - nums[i]);
    }
    return sum;
}

private int twoSumSmaller(int[] nums, int startIndex, int target) {
    int sum = 0;
    for (int i = startIndex; i < nums.length - 1; i++) {
        int j = binarySearch(nums, i, target - nums[i]);
        sum += j - i;
    }
    return sum;
}

private int binarySearch(int[] nums, int startIndex, int target) {
    int left = startIndex;
    int right = nums.length - 1;
    while (left < right) {
        int mid = (left + right + 1) / 2;
        if (nums[mid] < target) {
            left = mid;
        } else {
            right = mid - 1;
        }
    }
    return left;
}
```

Note that in the above binary search we choose the upper middle element $$(\frac{left+right+1}{2})$$ instead of the lower middle element $$(\frac{left+right}{2})$$. The reason is due to the terminating condition when there are two elements left. If we chose the lower middle element and the condition $$nums[mid] < target$$ evaluates to true, then the loop will never terminate. Choosing the upper middle element will guarantee termination.

**Complexity analysis**

* Time complexity : $$O(n^2 \log n)$$.
The *binarySearch* function takes $$O(\log n)$$ time, therefore the *twoSumSmaller* takes $$O(n \log n)$$ time. The *threeSumSmaller* wraps with another for-loop, and therefore is $$O(n^2 \log n)$$ time.

* Space complexity : $$O(1)$$.

---
#### Approach #3 (Two Pointers) [Accepted]

Let us try sorting the array first. For example, $$nums = [3,5,2,8,1]$$ becomes $$[1,2,3,5,8]$$.

Let us look at an example $$nums = [1,2,3,5,8]$$, and $$target = 7$$.

    [1, 2, 3, 5, 8]
     ↑           ↑
    left       right

Let us initialize two indices, $$left$$ and $$right$$ pointing to the first and last element respectively.

When we look at the sum of first and last element, it is $$1 + 8 = 9$$, which is $$\geq target$$. That tells us no index pair will ever contain the index $$right$$. So the next logical step is to move the right pointer one step to its left.

    [1, 2, 3, 5, 8]
     ↑        ↑
    left    right

Now the pair sum is $$1 + 5 = 6$$, which is $$< target$$. How many pairs with one of the $$index = left$$ that satisfy the condition? You can tell by the difference between $$right$$ and $$left$$ which is $$3$$, namely $$(1,2), (1,3),$$ and $$(1,5)$$. Therefore, we move $$left$$ one step to its right.

```java
public int threeSumSmaller(int[] nums, int target) {
    Arrays.sort(nums);
    int sum = 0;
    for (int i = 0; i < nums.length - 2; i++) {
        sum += twoSumSmaller(nums, i + 1, target - nums[i]);
    }
    return sum;
}

private int twoSumSmaller(int[] nums, int startIndex, int target) {
    int sum = 0;
    int left = startIndex;
    int right = nums.length - 1;
    while (left < right) {
        if (nums[left] + nums[right] < target) {
            sum += right - left;
            left++;
        } else {
            right--;
        }
    }
    return sum;
}
```

**Complexity analysis**

* Time complexity : $$O(n^2)$$.
The *twoSumSmaller* function takes $$O(n)$$ time because both *left* and *right* traverse at most *n* steps. Therefore, the overall time complexity is $$O(n^2)$$.

* Space complexity : $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple and easy-understanding O(n^2) JAVA solution
- Author: SOY
- Creation Date: Sat Sep 05 2015 03:39:22 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 07:52:47 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        int count;
        
        public int threeSumSmaller(int[] nums, int target) {
            count = 0;
            Arrays.sort(nums);
            int len = nums.length;
        
            for(int i=0; i<len-2; i++) {
                int left = i+1, right = len-1;
                while(left < right) {
                    if(nums[i] + nums[left] + nums[right] < target) {
                        count += right-left;
                        left++;
                    } else {
                        right--;
                    }
                }
            }
            
            return count;
        }
    }
</p>


### Python solution with comments.
- Author: OldCodingFarmer
- Creation Date: Tue Aug 18 2015 04:13:07 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 05:04:33 GMT+0800 (Singapore Standard Time)

<p>
        
    # O(n*n) time
    def threeSumSmaller(self, nums, target):
        count = 0
        nums.sort()
        for i in xrange(len(nums)):
            j, k = i+1, len(nums)-1
            while j < k:
                s = nums[i] + nums[j] + nums[k]
                if s < target:
                    # if (i,j,k) works, then (i,j,k), (i,j,k-1),..., 
                    # (i,j,j+1) all work, totally (k-j) triplets
                    count += k-j
                    j += 1
                else:
                    k -= 1
        return count
</p>


### Accepted and Simple Java O(n^2) solution with detailed explanation
- Author: kevinhsu
- Creation Date: Fri Oct 09 2015 02:26:57 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 13:59:27 GMT+0800 (Singapore Standard Time)

<p>
We sort the array first. Then, for each element, we use the two pointer approach to find the number of triplets that meet the requirements. 

Let me illustrate how the two pointer technique works with an example:

    target = 2

      i  lo    hi
    [-2, 0, 1, 3]

We use a for loop (index i) to iterate through each element of the array. For each i, we create two pointers, lo and hi, where lo is initialized as the next element of i, and hi is initialized at the end of the array.  If we know that nums[i] + nums[lo] + nums[hi] < target, then we know that since the array is sorted, we can replace hi with any element from lo+1 to nums.length-1, and the requirements will still be met. Just like in the example above, we know that since -2 + 0 + 3 < 2, we can replace hi (3) with 1, and it would still work. Therefore, we can just add hi - lo to the triplet count.


    public class Solution {
        public int threeSumSmaller(int[] nums, int target) {
            int result = 0;
            Arrays.sort(nums);
            for(int i = 0; i <= nums.length-3; i++) {
                int lo = i+1;
                int hi = nums.length-1;
                while(lo < hi) {
                    if(nums[i] + nums[lo] + nums[hi] < target) {
                        result += hi - lo;
                        lo++;
                    } else {
                        hi--;
                    }
                }
            }
            return result;
        }
    }
</p>


