---
title: "Minimum Distance Between BST Nodes"
weight: 720
#id: "minimum-distance-between-bst-nodes"
---
## Description
<div class="description">
<p>Given a Binary Search Tree (BST) with the root node <code>root</code>, return&nbsp;the minimum difference between the values of any two different nodes in the tree.</p>

<p><strong>Example :</strong></p>

<pre>
<strong>Input:</strong> root = [4,2,6,1,3,null,null]
<strong>Output:</strong> 1
<strong>Explanation:</strong>
Note that root is a TreeNode object, not an array.

The given tree [4,2,6,1,3,null,null] is represented by the following diagram:

          4
        /   \
      2      6
     / \    
    1   3  

while the minimum difference in this tree is 1, it occurs between node 1 and node 2, also between node 3 and node 2.
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li>The size of the BST will be between 2 and&nbsp;<code>100</code>.</li>
	<li>The BST is always valid, each node&#39;s value is an integer, and each node&#39;s value is different.</li>
	<li>This question is the same as 530:&nbsp;<a href="https://leetcode.com/problems/minimum-absolute-difference-in-bst/">https://leetcode.com/problems/minimum-absolute-difference-in-bst/</a></li>
</ol>

</div>

## Tags
- Tree (tree)
- Recursion (recursion)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Write to Array [Accepted]

**Intuition and Algorithm**

Write all the values to an array, then sort it.  The minimum distance must occur between two adjacent values in the sorted list.

<iframe src="https://leetcode.com/playground/TpEMnQbx/shared" frameBorder="0" width="100%" height="429" name="TpEMnQbx"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the number of nodes in the tree.  The complexity comes from the sorting operation.

* Space Complexity:  $$O(N)$$, the size of `vals`.

---
#### Approach #2: In-Order Traversal [Accepted]

**Intuition and Algorithm**

In a binary search tree, an in-order traversal outputs the values of the tree in order.  By remembering the previous value in this order, we could iterate over each possible difference, keeping the smallest one.

<iframe src="https://leetcode.com/playground/NizQHa7c/shared" frameBorder="0" width="100%" height="361" name="NizQHa7c"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the tree.  We iterate over every node once.

* Space Complexity:  $$O(H)$$, where $$H$$ is the height of the tree.  This is the space used by the implicit call stack when calling `dfs`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Come on guys, it is obviously the same as Problems 530. Minimum Absolute Difference in BST
- Author: petenlf1025
- Creation Date: Sat Mar 10 2018 16:14:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 06:11:26 GMT+0800 (Singapore Standard Time)

<p>
I submitted the same codes and received two Accepted!
</p>


### [C++/Java/Python] Inorder Traversal O(N) time Recursion
- Author: lee215
- Creation Date: Sun Feb 11 2018 16:32:26 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 27 2019 11:22:27 GMT+0800 (Singapore Standard Time)

<p>
Classical inorder traverse. Time complexity O(N). Space complexity O(h)

This question is the same as problem 530.Minimum Absolute Difference in BST. Except that in 530th, we are given a binary search tree with **non-negative values**.
However, it seems that it doesn\'t have any negative case and my solution in cpp get accepted.


C++
```
class Solution {
  public:
    int res = INT_MAX, pre = -1;
    int minDiffInBST(TreeNode* root) {
        if (root->left != NULL) minDiffInBST(root->left);
        if (pre >= 0) res = min(res, root->val - pre);
        pre = root->val;
        if (root->right != NULL) minDiffInBST(root->right);
        return res;
    }
};
```
Java
```
class Solution {
    Integer res = Integer.MAX_VALUE, pre = null;
    public int minDiffInBST(TreeNode root) {
        if (root.left != null) minDiffInBST(root.left);
        if (pre != null) res = Math.min(res, root.val - pre);
        pre = root.val;
        if (root.right != null) minDiffInBST(root.right);
        return res;
    }
}
```
Python
```
class Solution(object):
    pre = -float(\'inf\')
    res = float(\'inf\')

    def minDiffInBST(self, root):
        if root.left:
            self.minDiffInBST(root.left)
        self.res = min(self.res, root.val - self.pre)
        self.pre = root.val
        if root.right:
            self.minDiffInBST(root.right)
        return self.res
</p>


### Problem title is confusing
- Author: eequalsmc2
- Creation Date: Wed Feb 14 2018 11:00:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 02:48:19 GMT+0800 (Singapore Standard Time)

<p>
I think the problem title '**Minimum Distance between BST nodes** ' is quite confusing here. It should be '**Minimun difference between BST nodes**'.
</p>


