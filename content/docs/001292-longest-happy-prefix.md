---
title: "Longest Happy Prefix"
weight: 1292
#id: "longest-happy-prefix"
---
## Description
<div class="description">
<p>A string is called a&nbsp;<em>happy prefix</em>&nbsp;if is a <strong>non-empty</strong> prefix which is also a suffix (excluding itself).</p>

<p>Given a string <code>s</code>. Return the <strong>longest happy prefix</strong>&nbsp;of <code>s</code>&nbsp;.</p>

<p>Return an empty string if no such prefix exists.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;level&quot;
<strong>Output:</strong> &quot;l&quot;
<strong>Explanation:</strong> s contains 4 prefix excluding itself (&quot;l&quot;, &quot;le&quot;, &quot;lev&quot;, &quot;leve&quot;), and suffix (&quot;l&quot;, &quot;el&quot;, &quot;vel&quot;, &quot;evel&quot;). The largest prefix which is also suffix is given by &quot;l&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;ababab&quot;
<strong>Output:</strong> &quot;abab&quot;
<strong>Explanation:</strong> &quot;abab&quot; is the largest prefix which is also suffix. They can overlap in the original string.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;leetcodeleet&quot;
<strong>Output:</strong> &quot;leet&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;a&quot;
<strong>Output:</strong> &quot;&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10^5</code></li>
	<li><code>s</code> contains only lowercase English letters.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java with picture, incremental hash and KMP
- Author: votrubac
- Creation Date: Sun Mar 22 2020 13:54:42 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 31 2020 19:18:28 GMT+0800 (Singapore Standard Time)

<p>
#### Approach 1: Incremental Hash
Compute hashes for increasing prefix and suffix, reusing the previously computed hashes. This is similar to the Karp\u2013Rabin algorithm.

For the string of `i` size, the hash is: s[0] * 26 ^ (i - 1) + s[1] * 26 ^ (i -2) + ... + s[i - 2] * 26 + s[i - 1].

- For the prefix hash, we will multiply the previous value by 26 and add a new letter.
- For the suffix hash, we add a new letter multiplied by 26 ^ (i - 1).

Note that we need to use some large prime as a modulo to avoid overflow errors, e.g. 10 ^ 9 + 7. 

> Beware of collisions. After you find a matching hash, you need to check the string. Since OJ test cases do not cause collisions, I am not doing it here.
> 
> **Update:** see sample solutions that handle collisions in the appendices bellow. 
> 
> Example strings with collisions:

```cpp
"vwantmbocxcwrqtvgzuvgrmdltfiglltaxkjfajxthcppcatddcunpkqsgpnjjgqanrwabgrtwuqbrfl"
"gqijndnbohkujykncuooajkhsxcbjbwhrrfgkliqatdndcrpmaqa"
"tzqzalgnxstxzlul"
```

**C++**
```cpp
string longestPrefix(string &s) {
    long h1 = 0, h2 = 0, mul = 1, len = 0, mod = 1000000007;
    for (int i = 0, j = s.length() - 1; j > 0; ++i, --j) {
        int first = s[i] - \'a\', last = s[j] - \'a\';
        h1 = (h1 * 26 + first) % mod;
        h2 = (h2 + mul * last) % mod;
        mul = mul * 26 % mod;
        if (h1 == h2)
            len = i + 1;
    }
    return s.substr(0, len);
}
```
**Java**
```java
public String longestPrefix(String s) {
    long h1 = 0, h2 = 0, mul = 1, len = 0, mod = 1000000007;
    for (int i = 0, j = s.length() - 1; j > 0; ++i, --j) {
        int first = s.charAt(i) - \'a\', last = s.charAt(j) - \'a\';
        h1 = (h1 * 26 + first) % mod;
        h2 = (h2 + mul * last) % mod;
        mul = mul * 26 % mod;
        if (h1 == h2)
            len = i + 1;
    }
    return s.substring(0, (int)len);        
}
```
**Complexity Analysis**
- Time: O(n)
- Memory: O(1)
#### Approach 2: KMP
What we are basically doing here is searching for the prefix `[0, *]` for each position `i > 0`, and check if the match is a suffix. A naive approach has O(n * n) time complexity, as we start matching the prefix from the beginning for every position `i`.

We can adapt KMP algorithm, which has O(n + m) complexity. Two running pointers `i` and `j` are the current matching positions in suffix and prefix.  The count of matched so far letters is tracked in `dp`. If there is a mismatch, we use `dp` to determine the new matching position for prefix (`j`). This allows the matching position for suffix (`i`) to never go back. 

The credit goes to [dirkbe11](https://leetcode.com/dirkbe11/) for [this solution](https://leetcode.com/problems/longest-happy-prefix/discuss/547204/C%2B%2B-KMP-preprocessing). The valuable insight there is that, in the end, the match position `j` (or the last value in `dp`) indicates the length of the matching prefix and suffix.

There are lots of good description and videos of KMP online. Here, I just want to provide a picture example to demonstrate it.


![image](https://assets.leetcode.com/users/votrubac/image_1584911476.png)

**C++**
```cpp
string longestPrefix(string &s) {
    vector<int> dp(s.size());
    int j = 0;
    for (int i = 1; i < s.size(); ++i) {
        if (s[i] == s[j])
            dp[i] = ++j;
        else if (j > 0) {
            j = dp[j - 1];
            --i;
        }
    }
    return s.substr(0, j);
}
```
**Java**
```java
public String longestPrefix(String s) {
    int dp[] = new int[s.length()], j = 0;
    for (int i = 1; i < s.length(); ++i) {
        if (s.charAt(i) == s.charAt(j))
            dp[i] = ++j;
        else if (j > 0) {
            j = dp[j - 1];
            --i;
        }
    }
    return s.substring(0, j);       
}
```
**Complexity Analysis**
- Time: O(n)
- Memory: O(n)

#### Appendix A: Collision-Aware Incremental Hash
This is little tricky to implement efficiently. If you check the string for every matching hash, it will be inefficient for repeated patterns. If you only store the largest size, like in the incremental hash solution above, what do you do if suffix and preffx of that size do not match?

We can store all sizes where we have a potential match, but that would require extra memory. We can also try to compute the hash backwards, but you will need to compute the inverse modulo. 

> **Update:** see the decremental hash solution in the following appendix.

A simpler idea, which did not occur to me initially, is to compare the string every time you find a matching hash, but only do it for characters we haven\'t checked before. That way, we can handle repeating patterns efficiently.

```cpp
string longestPrefix(string &s) {
    long h1 = 0, h2 = 0, mul = 1, len = 0, mod = 1000000007;
    for (int i = 0, j = s.length() - 1; j > 0; ++i, --j) {
        int first = s[i] - \'a\', last = s[j] - \'a\';
        h1 = (h1 * 26 + first) % mod;
        h2 = (h2 + mul * last) % mod;
        mul = mul * 26 % mod;
        if (h1 == h2) {
            if (s.compare(0, i + 1 - len, s, j, s.size() - j - len) == 0 &&
               s.compare(len, i + 1 - len, s, j + len, s.size() - j - len) == 0)
                len = i + 1;
        }
    }
    return s.substr(0, len);
}
```
#### Appendix B: Decremental Hash
The problem with the incremental hash that we do not know if a larger happy prefix exists, and re-checking the string for collisions is expensive (O(n * n) for string with repeating pattern).

What if we compute the hash for the entire string, and then decrement it for suffix and prefix? This way, the first match we find will correspond to the largest happy prefix.

**Beware: Inverse Modulo**
The formula to compute decremental hash is the reverse of the incremental hash. However, there is no division in the modular arithmetic; instead, we need to multiply by inverse modulo for that number with respect to the modulus (10 ^ 9 + 7 in our case).

Inverse modulo for 26 with respect to 10 ^ 9 + 7 is 576923081. Search online for more information on inverse modulo and extended Euclidian algorithm to compute it.

```cpp
string longestPrefix(string &s) {
    long h = 0, mod = 1000000007, mul = 1, inverseMod26 = 576923081;
    for (int i = 0, j = s.length() - 1; j >= 0; ++i, --j) {
        h = (h * 26 + s[i] - \'a\') % mod;
        mul = mul * 26 % mod;
    }
    long h1 = h, h2 = h;
    for (int i = 0, j = s.length() - 1; j > 0; ++i, --j) {
        mul = mul * inverseMod26 % mod;
        h1 = (h1 - (s[j] - \'a\')) * inverseMod26 % mod;
        h2 = (mod + h2 - mul * (s[i] - \'a\') % mod) % mod;
        if (h1 == h2) {
            if (s.compare(0, j, s, i + 1, s.size() - i - 1) == 0)
                return s.substr(0, j);
        }
    }
    return "";
}
```
</p>


### [Java/Python] Rolling Hash
- Author: lee215
- Creation Date: Sun Mar 22 2020 12:06:42 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Mar 26 2020 15:16:13 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Imgaine that we are given integer array including only 0-9, 
`A = [1,2,3,4,5,1,2,3]`,
find the longest matched prefix and suffix.

For one digits, we have 1 and 3.
For two digits. we have 12 and 23 
For three digits, we have 123 and 123, matched!
<br>

# **Explanation**
Calculate the value of prefix and suffix at the same time.
Though here we have 0-25 instead of just 0-9,
so we should apply >= 26-nary instead of 10-nary.

If you\'re worried about the collisions,
we can easily check if prefix is equal to the suffix.
We\'d better do that, like in an interview.
I didn\'t do that, because I knew LeetCode test cases are far from strong.
<br>

# **Complexity**
Time `O(N)`
Space `O(1)`
<br>

**Java**
```java
    public String longestPrefix(String s) {
        long l = 0, r = 0, p = 1, mod = (long)1e9 + 7;
        int k = 0, n = s.length();
        for (int i = 0; i < n - 1; i++) {
            l = (l * 128 + s.charAt(i)) % mod;
            r = (r + p * s.charAt(n - i - 1)) % mod;
            if (l == r) k = i + 1;
            p = p * 128 % mod;
        }
        return s.substring(0, k);
    }
```

**Python:**
Comments added by @lichuan199010
`pow` is not `O(1)` in Python, 
we can update the pow value each round making it O(1), like in Java .
```py
    def longestPrefix(self, s):
        # res stores the index of the end of the prefix, used for output the result
        # l stores the hash key for prefix
        # r stores the hash key for suffix
        # mod is used to make sure that the hash value doesn\'t get too big, you can choose another mod value if you want.
        res, l, r, mod = 0, 0, 0, 10**9 + 7

        # now we start from the beginning and the end of the string
        # note you shouldn\'t search the whole string! because the longest prefix and suffix is the string itself
        for i in range(len(s) - 1):

            # based on an idea that is similar to prefix sum, we calculate the prefix hash in O(1) time.
            # specifically, we multiply the current prefix by 128 (which is the length of ASCII, but you can use another value as well)
            # then add in the ASCII value of the upcoming letter
            l = (l * 128 + ord(s[i])) % mod

            # similarly, we can calculate the suffix hash in O(1) time.
            # Specifically, we get the ith letter from the end using s[~i], note ~i is -i-1
            # we find the pow(128, i, mod) and multiply by the letter\'s ASCII value
            # Actually, if we don\'t care about the beautifulness of the code, you can have a variable to keep track of pow(128, i, mod) as you increase i
            r = (r + pow(128, i, mod) * ord(s[~i])) % mod

           # we check if the prefix and suffix agrees, if yes, we find yet another longer prefix, so we record the index
            if l == r: res = i + 1

       # after we finish searching the string, output the prefix
        return s[:res]
```

</p>


### [C++], KMP preprocessing
- Author: dirkbe11
- Creation Date: Sun Mar 22 2020 12:02:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2020 14:26:48 GMT+0800 (Singapore Standard Time)

<p>
The preprocessing for KMP actually computes the longest prefix that is a suffix, so we can just use the same algorithm.

Obligatory Roy video: https://www.youtube.com/watch?v=GTJr8OvyEVQ

```
class Solution {
public:
    vector<int> lps(string& word)
    {
        vector<int> lp(word.length(), 0);
        
        int index = 0;
        for(int i = 1; i < word.length();)
        {
            if(word[i] == word[index])
            {
                index++;
                lp[i] = index;
                i++;
            }
            else
            {
                if(index != 0)
                    index = lp[index-1];
                else
                    i++;
            }
        }
        
        return lp;
    }
    
    string longestPrefix(string s) {
        vector<int> lp = lps(s);
        
        return s.substr(0, lp.back());
    }
};
```
</p>


