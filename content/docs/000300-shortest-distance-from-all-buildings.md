---
title: "Shortest Distance from All Buildings"
weight: 300
#id: "shortest-distance-from-all-buildings"
---
## Description
<div class="description">
<p>You want to build a house on an <i>empty</i> land which reaches all buildings in the shortest amount of distance. You can only move up, down, left and right. You are given a 2D grid of values <b>0</b>, <b>1</b> or <b>2</b>, where:</p>

<ul>
	<li>Each <b>0</b> marks an empty land which you can pass by freely.</li>
	<li>Each <b>1</b> marks a building which you cannot pass through.</li>
	<li>Each <b>2</b> marks an obstacle which you cannot pass through.</li>
</ul>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> [[1,0,2,0,1],[0,0,0,0,0],[0,0,1,0,0]]

1 - 0 - 2 - 0 - 1
|   |   |   |   |
0 - 0 - 0 - 0 - 0
|   |   |   |   |
0 - 0 - 1 - 0 - 0

<strong>Output:</strong> 7 

<strong>Explanation:</strong> Given three buildings at <code>(0,0)</code>, <code>(0,4)</code>, <code>(2,2)</code>, and an obstacle at <code>(0,2),
             t</code>he point <code>(1,2)</code> is an ideal empty land to build a house, as the total 
&nbsp;            travel distance of 3+3+1=7 is minimal. So return 7.</pre>

<p><b>Note:</b><br />
There will be at least one building. If it is not possible to build such house according to the above rules, return -1.</p>

</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies
- Facebook - 10 (taggedByAdmin: false)
- Amazon - 6 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Google - 6 (taggedByAdmin: true)
- Mathworks - 6 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Splunk - 2 (taggedByAdmin: false)
- Zenefits - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution with explanation and time complexity analysis
- Author: shuoshankou
- Creation Date: Sat Dec 19 2015 07:40:29 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 01:56:15 GMT+0800 (Singapore Standard Time)

<p>
Inspired by previous solution.
The main idea is the following:

Traverse the matrix. For each building, use BFS to compute the shortest distance from each '0' to
 this building. After we do this for all the buildings, we can get the sum of shortest distance
 from every '0' to all reachable buildings. This value is stored
 in 'distance[][]'. For example, if grid[2][2] == 0, distance[2][2] is the sum of shortest distance from this block to all reachable buildings.
Time complexity: O(number of 1)*O(number of 0) ~ O(m^2*n^2)
    
We also count how many building each '0' can be reached. It is stored in reach[][]. This can be done during the BFS. We also need to count how many total buildings are there in the matrix, which is stored in 'buildingNum'.

Finally, we can traverse the distance[][] matrix to get the point having shortest distance to all buildings. O(m*n)
    
The total time complexity will be O(m^2*n^2), which is quite high!. Please let me know if I did the analysis wrong or you have better solution.


    public class Solution {
        public int shortestDistance(int[][] grid) {
            if (grid == null || grid[0].length == 0) return 0;
            final int[] shift = new int[] {0, 1, 0, -1, 0};
            
            int row  = grid.length, col = grid[0].length;
            int[][] distance = new int[row][col];
            int[][] reach = new int[row][col];
            int buildingNum = 0;
            
            for (int i = 0; i < row; i++) {
                for (int j =0; j < col; j++) {
                    if (grid[i][j] == 1) {
                        buildingNum++;
                        Queue<int[]> myQueue = new LinkedList<int[]>();
                        myQueue.offer(new int[] {i,j});
    
                        boolean[][] isVisited = new boolean[row][col];
                        int level = 1;
                        
                        while (!myQueue.isEmpty()) {
                            int qSize = myQueue.size();
                            for (int q = 0; q < qSize; q++) {
                                int[] curr = myQueue.poll();
                                
                                for (int k = 0; k < 4; k++) {
                                    int nextRow = curr[0] + shift[k];
                                    int nextCol = curr[1] + shift[k + 1];
                                    
                                    if (nextRow >= 0 && nextRow < row && nextCol >= 0 && nextCol < col
                                        && grid[nextRow][nextCol] == 0 && !isVisited[nextRow][nextCol]) {
                                            //The shortest distance from [nextRow][nextCol] to thic building
                                            // is 'level'.
                                            distance[nextRow][nextCol] += level;
                                            reach[nextRow][nextCol]++;
                                            
                                            isVisited[nextRow][nextCol] = true;
                                            myQueue.offer(new int[] {nextRow, nextCol});
                                        }
                                }
                            }
                            level++;
                        }
                    }
                }
            }
            
            int shortest = Integer.MAX_VALUE;
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    if (grid[i][j] == 0 && reach[i][j] == buildingNum) {
                        shortest = Math.min(shortest, distance[i][j]);
                    }
                }
            }
            
            return shortest == Integer.MAX_VALUE ? -1 : shortest;
            
            
        }
    }
</p>


### 36 ms C++ solution
- Author: StefanPochmann
- Creation Date: Tue Dec 15 2015 06:51:39 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 07:52:39 GMT+0800 (Singapore Standard Time)

<p>
I also tested the other three C++ solutions posted so far, they took 340-1812 ms. I think mine is faster because I don't use a fresh "`visited`" for each BFS. Instead, I walk only onto the cells that were reachable from all previous buildings. From the first building I only walk onto cells where `grid` is 0, and make them -1. From the second building I only walk onto cells where `grid` is -1, and I make them -2. And so on.

    int shortestDistance(vector<vector<int>> grid) {
        int m = grid.size(), n = grid[0].size();
        auto total = grid;
        int walk = 0, mindist, delta[] = {0, 1, 0, -1, 0};
        for (int i=0; i<m; ++i) {
            for (int j=0; j<n; ++j) {
                if (grid[i][j] == 1) {
                    mindist = -1;
                    auto dist = grid;
                    queue<pair<int, int>> q;
                    q.emplace(i, j);
                    while (q.size()) {
                        auto ij = q.front();
                        q.pop();
                        for (int d=0; d<4; ++d) {
                            int i = ij.first + delta[d];
                            int j = ij.second + delta[d+1];
                            if (i >= 0 && i < m && j >= 0 && j < n && grid[i][j] == walk) {
                                grid[i][j]--;
                                dist[i][j] = dist[ij.first][ij.second] + 1;
                                total[i][j] += dist[i][j] - 1;
                                q.emplace(i, j);
                                if (mindist < 0 || mindist > total[i][j])
                                    mindist = total[i][j];
                            }
                        }
                    }
                    walk--;
                }
            }
        }
        return mindist;
    }
</p>


### Python solution 72ms beats 100%, BFS with pruning
- Author: kitt
- Creation Date: Wed Jan 20 2016 00:23:44 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 11:11:33 GMT+0800 (Singapore Standard Time)

<p>
Use `hit` to record how many times a `0` grid has been reached and use `distSum` to record the sum of distance from all `1` grids to this `0` grid. A powerful pruning is that during the BFS we use `count1` to count how many `1` grids we reached. If `count1 < buildings` then we know not all `1` grids are connected are we can return `-1` immediately, which greatly improved speed (beat 100% submissions).

    def shortestDistance(self, grid):
        if not grid or not grid[0]: return -1
        M, N, buildings = len(grid), len(grid[0]), sum(val for line in grid for val in line if val == 1)
        hit, distSum = [[0] * N for i in range(M)], [[0] * N for i in range(M)]
        
        def BFS(start_x, start_y):
            visited = [[False] * N for k in range(M)]
            visited[start_x][start_y], count1, queue = True, 1, collections.deque([(start_x, start_y, 0)])
            while queue:
                x, y, dist = queue.popleft()
                for i, j in ((x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)):
                    if 0 <= i < M and 0 <= j < N and not visited[i][j]:
                        visited[i][j] = True
                        if not grid[i][j]:
                            queue.append((i, j, dist + 1))
                            hit[i][j] += 1
                            distSum[i][j] += dist + 1
                        elif grid[i][j] == 1:
                            count1 += 1
            return count1 == buildings  
        
        for x in range(M):
            for y in range(N):
                if grid[x][y] == 1:
                    if not BFS(x, y): return -1
        return min([distSum[i][j] for i in range(M) for j in range(N) if not grid[i][j] and hit[i][j] == buildings] or [-1])
</p>


