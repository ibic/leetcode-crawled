---
title: "Zuma Game"
weight: 463
#id: "zuma-game"
---
## Description
<div class="description">
<p>Think about Zuma Game. You have a row of balls on the table, colored red(R), yellow(Y), blue(B), green(G), and white(W). You also have several balls in your hand.</p>

<p>Each time, you may choose a ball in your hand, and insert it into the row (including the leftmost place and rightmost place). Then, if there is a group of 3 or more balls in the same color touching, remove these balls. Keep doing this until no more balls can be removed.</p>

<p>Find the minimal balls you have to insert to remove all the balls on the table. If you cannot remove all the balls, output -1.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> board = &quot;WRRBBW&quot;, hand = &quot;RB&quot;
<strong>Output:</strong> -1
<strong>Explanation:</strong> WRRBBW -&gt; WRR[R]BBW -&gt; WBBW -&gt; WBB[B]W -&gt; WW
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> board = &quot;WWRRBBWW&quot;, hand = &quot;WRBRW&quot;
<strong>Output:</strong> 2
<strong>Explanation:</strong> WWRRBBWW -&gt; WWRR[R]BBWW -&gt; WWBBWW -&gt; WWBB[B]WW -&gt; WWWW -&gt; empty
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> board = &quot;G&quot;, hand = &quot;GGGGG&quot;
<strong>Output:</strong> 2
<strong>Explanation:</strong> G -&gt; G[G] -&gt; GG[G] -&gt; empty 
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> board = &quot;RBYYBBRRB&quot;, hand = &quot;YRBGB&quot;
<strong>Output:</strong> 3
<strong>Explanation:</strong> RBYYBBRRB -&gt; RBYY[Y]BBRRB -&gt; RBBBRRB -&gt; RRRB -&gt; B -&gt; B[B] -&gt; BB[B] -&gt; empty 
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>You may assume that the initial row of balls on the table won&rsquo;t have any 3 or more consecutive balls with the same color.</li>
	<li><code>1 &lt;= board.length &lt;= 16</code></li>
	<li><code>1 &lt;= hand.length &lt;= 5</code></li>
	<li>Both input strings will be non-empty and only contain characters &#39;R&#39;,&#39;Y&#39;,&#39;B&#39;,&#39;G&#39;,&#39;W&#39;.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Bloomberg - 3 (taggedByAdmin: false)
- Baidu - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Standard test program is wrong?
- Author: herbix
- Creation Date: Thu Jan 26 2017 14:02:37 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 05:02:36 GMT+0800 (Singapore Standard Time)

<p>
Hi all,

I tried case `"RRWWRRBBRR", "WB"`. The test program gave the expected answer `-1`. However, I thought the answer might be `2`. Because:

`RRWWRRBBRR -> RRWWRRBBR[W]R -> RRWWRRBB[B]RWR -> RRWWRRRWR -> RRWWWR -> RRR -> empty`

The possible reason might be the first `[W]` was inserted but not adjacent to a `W` in the sequence. I read the description twice but didn't find any condition about it.

Could someone give me some ideas about it?
</p>


### "short" java solution, beats 98%
- Author: magtron_3
- Creation Date: Sat Feb 18 2017 17:23:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 07:30:24 GMT+0800 (Singapore Standard Time)

<p>
Since most top solutions are very long and it's uncomfortable to read, so I share my shorter solution, and it beats 98% java solution.

My idea is simple and clear. it's just like a DFS or a Backtracking solution. word is poor, just look the code.

'''
public class Solution {

    int MAXCOUNT = 6;   // the max balls you need will not exceed 6 since "The number of balls in your hand won't exceed 5"

    public int findMinStep(String board, String hand) {
        int[] handCount = new int[26];
        for (int i = 0; i < hand.length(); ++i) ++handCount[hand.charAt(i) - 'A'];
        int rs = helper(board + "#", handCount);  // append a "#" to avoid special process while j==board.length, make the code shorter.
        return rs == MAXCOUNT ? -1 : rs;
    }
    private int helper(String s, int[] h) {
        s = removeConsecutive(s);     
        if (s.equals("#")) return 0;
        int  rs = MAXCOUNT, need = 0;
        for (int i = 0, j = 0 ; j < s.length(); ++j) {
            if (s.charAt(j) == s.charAt(i)) continue;
            need = 3 - (j - i);     //balls need to remove current consecutive balls.
            if (h[s.charAt(i) - 'A'] >= need) {
                h[s.charAt(i) - 'A'] -= need;
                rs = Math.min(rs, need + helper(s.substring(0, i) + s.substring(j), h));
                h[s.charAt(i) - 'A'] += need;
            }
            i = j;
        }
        return rs;
    }
    //remove consecutive balls longer than 3
    private String removeConsecutive(String board) {
        for (int i = 0, j = 0; j < board.length(); ++j) {
            if (board.charAt(j) == board.charAt(i)) continue;
            if (j - i >= 3) return removeConsecutive(board.substring(0, i) + board.substring(j));
            else i = j;
        }
        return board;
    }
}
'''
</p>


### StraightForward Recursive Java Solution beat 97%
- Author: fatalme
- Creation Date: Wed Feb 15 2017 15:21:08 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 17:32:54 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    
    private int aux(String s, int[] c){
        if("".equals(s)) return 0;
//worst case, every character needs 2 characters; plus one to make it impossible, ;-)
        int res = 2 * s.length() + 1; 
        for(int i = 0; i < s.length();){
            int j = i++;
            while(i < s.length() && s.charAt(i) == s.charAt(j)) i++;
            int inc = 3 - i + j;
            if(c[s.charAt(j)] >= inc){
                int used = inc <= 0 ? 0 : inc;
                c[s.charAt(j)] -= used;
                int temp = aux(s.substring(0, j) + s.substring(i), c);
                if(temp >= 0) res = Math.min(res, used + temp);
                c[s.charAt(j)] += used;
            }
        }
        return res == 2 * s.length() + 1 ? -1 : res;
    }
    
    public int findMinStep(String board, String hand) {
        int[] c = new int[128];
        for(char x : hand.toCharArray()){
            c[x]++;
        }
        return  aux(board, c);
    }
}
```
</p>


