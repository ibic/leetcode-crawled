---
title: "Search in a Binary Search Tree"
weight: 704
#id: "search-in-a-binary-search-tree"
---
## Description
<div class="description">
<p>Given the root node of a binary search tree (BST) and a value. You need to find the node in the BST that the node&#39;s value equals the given value. Return the subtree rooted with that node. If such node doesn&#39;t exist, you should return NULL.</p>

<p>For example,&nbsp;</p>

<pre>
Given the tree:
        4
       / \
      2   7
     / \
    1   3

And the value to search: 2
</pre>

<p>You should return this subtree:</p>

<pre>
      2     
     / \   
    1   3
</pre>

<p>In the example above, if we want to search the value <code>5</code>, since there is no node with value <code>5</code>, we should return <code>NULL</code>.</p>

<p>Note that an empty tree is represented by <code>NULL</code>, therefore you would see the expected output (serialized tree format) as&nbsp;<code>[]</code>, not <code>null</code>.</p>

</div>

## Tags
- Tree (tree)

## Companies
- Apple - 2 (taggedByAdmin: false)
- IBM - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Binary Search Tree.

Binary Search Tree is a binary tree where the key in each node 

- is greater than any key stored in the left sub-tree, 

- and less than any key stored in the right sub-tree.

Here is an example:

![bla](../Figures/700/bst.png)

Such data structure provides the following operations in a 
logarithmic time: 

- Search. 

- [Insert](https://leetcode.com/articles/insert-into-a-bst/). 

- [Delete](https://leetcode.com/articles/delete-node-in-a-bst/).
<br />
<br />


---
#### Approach 1: Recursion

**Algorithm**

The recursion implementation is very straightforward:

- If the tree is empty `root == null` 
or the value to find is here `val == root.val` - return root.

- If `val < root.val` - go to search into the left subtree `searchBST(root.left, val)`.

- If `val > root.val` - go to search into the right subtree `searchBST(root.right, val)`.

- Return `root`.

![bla](../Figures/700/recursion.png)

**Implementation**

<iframe src="https://leetcode.com/playground/XQYNaftt/shared" frameBorder="0" width="100%" height="174" name="XQYNaftt"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(H)$$, where $$H$$ is a tree height. That results in
$$\mathcal{O}(\log N)$$ in the average case, and $$\mathcal{O}(N)$$ in the worst case. 

    Let's compute time complexity with the help of 
    [master theorem](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)) 
    $$T(N) = aT\left(\frac{N}{b}\right) + \Theta(N^d)$$.
    The equation represents dividing the problem 
    up into $$a$$ subproblems of size $$\frac{N}{b}$$ in $$\Theta(N^d)$$ time. 
    Here at step there is only one subproblem `a = 1`, its size 
    is a half of the initial problem `b = 2`, 
    and all this happens in a constant time `d = 0`, as for
    the binary search.
    That means that $$\log_b{a} = d$$ and hence we're dealing with 
    [case 2](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)#Case_2_example)
    that results in $$\mathcal{O}(n^{\log_b{a}} \log^{d + 1} N)$$
    = $$\mathcal{O}(\log N)$$ time complexity.
    
* Space complexity : $$\mathcal{O}(H)$$ to keep the recursion stack,
i.e. $$\mathcal{O}(\log N)$$ in the average case, 
and $$\mathcal{O}(N)$$ in the worst case.
<br />
<br />


---
#### Approach 2: Iteration

To reduce the space complexity, one could convert recursive
approach into the iterative one:

- While the tree is not empty `root != null` 
and the value to find is _not_ here `val != root.val`:

    - If `val < root.val` - go to search into the left subtree `root = root.left`.
    
    - If `val > root.val` - go to search into the right subtree `root = root.right`.

- Return `root`. 

![bla](../Figures/700/iteration.png)

**Implementation**

<iframe src="https://leetcode.com/playground/ng7BDtDQ/shared" frameBorder="0" width="100%" height="174" name="ng7BDtDQ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(H)$$, where $$H$$ is a tree height. That results in
$$\mathcal{O}(\log N)$$ in the average case, and $$\mathcal{O}(N)$$ in the worst case. 

    Let's compute time complexity with the help of 
    [master theorem](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)) 
    $$T(N) = aT\left(\frac{N}{b}\right) + \Theta(N^d)$$.
    The equation represents dividing the problem 
    up into $$a$$ subproblems of size $$\frac{N}{b}$$ in $$\Theta(N^d)$$ time. 
    Here at step there is only one subproblem `a = 1`, its size 
    is a half of the initial problem `b = 2`, 
    and all this happens in a constant time `d = 0`, as for
    the binary search.
    That means that $$\log_b{a} = d$$ and hence we're dealing with 
    [case 2](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)#Case_2_example)
    that results in $$\mathcal{O}(n^{\log_b{a}} \log^{d + 1} N)$$
    = $$\mathcal{O}(\log N)$$ time complexity.
    
* Space complexity : $$\mathcal{O}(1)$$ since it's a constant space
solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java beats 100% concise method using recursion and iteration
- Author: qiaojinghao
- Creation Date: Fri Jul 13 2018 23:45:14 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 06:32:45 GMT+0800 (Singapore Standard Time)

<p>
recursion:
```
public TreeNode searchBST(TreeNode root, int val) {
        if(root == null) return root;
        if(root.val == val){
            return root;
        }
        else{
            return val<root.val? searchBST(root.left,val):searchBST(root.right,val);
        }
    }
```
iteration:
```
public TreeNode searchBST(TreeNode root, int val) {
        while(root != null && root.val != val){
            root = val<root.val? root.left:root.right;
        }
        return root;
    }
```
</p>


### Concise iterative solution (C++)
- Author: sidvishnoi
- Creation Date: Sat Jun 16 2018 21:02:23 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 21:20:22 GMT+0800 (Singapore Standard Time)

<p>
``` cpp
TreeNode* searchBST(TreeNode* root, int val) {
    while (root != nullptr && root->val != val) {
      root = (root->val > val) ? root->left : root->right;
    }
    return root;
}
```
</p>


### 1 line code | 0 ms java code | 4 steps [c++ |  java | python] [iterative | recursive]
- Author: amit_gupta10
- Creation Date: Mon Jun 15 2020 15:06:59 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 15 2020 18:41:12 GMT+0800 (Singapore Standard Time)

<p>
***upvote if it helps***
**ALGO EXPLAINED**

1. if root is null return null
2. root->val is equals to val return that node
3. root->val > val search in the left subtree
4. root->val < val search in the right subtree


**RECURSIVE APPROACH 1 LINER**
```
class Solution {
   public:
     TreeNode* searchBST(TreeNode* root, int val) {
     return root==NULL?NULL:(val==root->val?root:(root->val>val?searchBST(root->left,val):searchBST(root->right,val)));
  }
};
```

**ITERATIVE APPROACH 3 LINER**
```

class Solution {
public:
   TreeNode* searchBST(TreeNode* root, int val) {
    while (root != nullptr && root->val != val) {
      root = (root->val > val) ? root->left : root->right;
    }
    return root;
}
};
```

**java code**

```
class Solution {
    public TreeNode searchBST(TreeNode root, int val) {
        while(root != null) {
            if (root.val == val)
                return root;
            else if (val < root.val) 
                root = root.left;
            else 
                root = root.right;
        }
        
        return null;
    }
}
```

**python code**

```
class Solution(object):
     def searchBST(self, root, val):
        
        while root:
            if root.val == val:     # bingo
                return root
            elif val < root.val:    # go left
                root = root.left
            else:                   # go right
                root = root.right 
        
        return root
```
</p>


