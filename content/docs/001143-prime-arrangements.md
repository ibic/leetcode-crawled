---
title: "Prime Arrangements"
weight: 1143
#id: "prime-arrangements"
---
## Description
<div class="description">
<p>Return the number of permutations of 1 to <code>n</code> so that prime numbers are at prime indices (1-indexed.)</p>

<p><em>(Recall that an integer&nbsp;is prime if and only if it is greater than 1, and cannot be written as a product of two positive integers&nbsp;both smaller than it.)</em></p>

<p>Since the answer may be large, return the answer <strong>modulo <code>10^9 + 7</code></strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 5
<strong>Output:</strong> 12
<strong>Explanation:</strong> For example [1,2,5,4,3] is a valid permutation, but [5,2,3,4,1] is not because the prime number 5 is at index 1.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 100
<strong>Output:</strong> 682289015
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 100</code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java With comment [sieve_of_eratosthenes]
- Author: JatinYadav96
- Creation Date: Sun Sep 01 2019 12:14:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 01 2019 12:14:23 GMT+0800 (Singapore Standard Time)

<p>
Used sieve of eratosthenes to generate count prime no 
```
    public static int countPrimes(int n) {
        boolean[] prime = new boolean[n + 1];
        Arrays.fill(prime, 2, n + 1, true);
        for (int i = 2; i * i <= n; i++)
            if (prime[i])
                for (int j = i * i; j <= n; j += i)
                    prime[j] = false;
        int cnt = 0;
        for (int i = 0; i < prime.length; i++)
            if (prime[i])
               cnt++;

        return cnt;
    }
```
if you dont know what is sieve of eratosthenes read this
https://www.geeksforgeeks.org/sieve-of-eratosthenes/

After getting count of prime \'pn\'
calculate no fo arragement of prime numbers i.e pn!
calculate no fo arragement of non-prime numbers i.e (n-pn)!

afterwards simple multiply them I used BigInteger of java

Hope you will like it :P
```

    static int MOD = 1000000007;

    public static int numPrimeArrangements(int n) {
        int noOfPrime = generatePrimes(n);
        BigInteger x = factorial(noOfPrime);
        BigInteger y = factorial(n - noOfPrime);
        return x.multiply(y).mod(BigInteger.valueOf(MOD)).intValue();
    }

    public static BigInteger factorial(int n) {
        BigInteger fac = BigInteger.ONE;
        for (int i = 2; i <= n; i++) {
            fac = fac.multiply(BigInteger.valueOf(i));
        }
        return fac.mod(BigInteger.valueOf(MOD));
    }
```


</p>


### [Java/Python 3] two codes each, count only primes then compute factorials for both w/ analysis.
- Author: rock
- Creation Date: Sun Sep 01 2019 12:07:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 28 2020 13:54:26 GMT+0800 (Singapore Standard Time)

<p>
compute (# of primes)! * (# of composites)!

**Method 1: judge each number by dividing all possible factors**

**Java**
```
    public int numPrimeArrangements(int n) {
        int cnt = 1; // # of primes, first prime is 2.
        outer:
        for (int i = 3; i <= n; i += 2) { // only odd number could be a prime, if i > 2.
            for (int factor = 3; factor * factor <= i; factor += 2)
                if (i % factor == 0)
                    continue outer;
            ++cnt;
        }
        long ans = 1;
        for (int i = 1; i <= cnt; ++i) // (# of primes)!
            ans = ans * i % 1_000_000_007;
        for (int i = 1; i <= n - cnt; ++i) // (# of non-primes)!
            ans = ans * i % 1_000_000_007;
        return (int)ans;
    }
```

----

**Python 3**

```
    def numPrimeArrangements(self, n: int) -> int:
        cnt = 1                                                     # number of primes, first prime is 2.
        for i in range(3, n + 1, 2):                                # only odd number could be a prime, if i > 2.
            factor = 3
            while factor * factor <= i:
                if i % factor == 0:
                    break 
                factor += 2    
            else:
                cnt += 1        
        ans = 1
        for i in range(1, cnt + 1):                                # (number of primes)!
            ans *= i        
        for i in range(1, n - cnt + 1):                            # (number of non-primes)!
            ans *= i
        return ans % (10**9 + 7)
```
With Python lib, we can replace the last 6 lines of the above code by the follows:
```
        return math.factorial(cnt) * math.factorial(n - cnt) % (10**9 + 7)
```

----

**Analysis:**

For each outer iteration, inner loop cost O(n ^ 0.5).

Time: O(n ^ (3/2)), space: O(1).

----

**Method 2: Sieve - mark all composites based on known primes**

1. Initially, the only know prime is 2; 
2. Mark the prime, prime + 1, prime + 2, ..., times of current known primes as composites.

Obviouly, **each time the algorithm runs the inner for loop, it always uses a new prime factor `prime` and also `times` starts with `prime`**, which guarantees there is no repeated composites marking and hence very time efficient.

**Java:**

```
    public int numPrimeArrangements(int n) {
        boolean[] composites = new boolean[n + 1];
        for (int prime = 2; prime * prime <= n; ++prime)
            for (int cmpst = prime * prime; !composites[prime] && cmpst <= n; cmpst += prime)
                composites[cmpst] = true;
        long cnt = IntStream.rangeClosed(2, n).filter(i -> !composites[i]).count();
        return (int)LongStream.concat(LongStream.rangeClosed(1, cnt), LongStream.rangeClosed(1, n - cnt))
                              .reduce(1, (a, b) -> a * b % 1_000_000_007);
    }
```

----

**Python 3:**

```
    def numPrimeArrangements(self, n: int) -> int:
        primes = [True] * (n + 1)
        for prime in range(2, int(math.sqrt(n)) + 1):
            if primes[prime]:
                for composite in range(prime * prime, n + 1, prime):
                    primes[composite] = False
        cnt = sum(primes[2:])
        return math.factorial(cnt) * math.factorial(n - cnt) % (10**9 + 7)
```

**Analysis:**

Time: O(n * log(logn)), space: O(n).
For time complexity analysis in details, please refer to [here](https://en.m.wikipedia.org/wiki/Sieve_of_Eratosthenes).

----

**Brief Summary:**
Method 1 and 2 are space and time efficient respectively.
</p>


### Detailed Explanation using Sieve
- Author: Just__a__Visitor
- Creation Date: Sun Sep 01 2019 13:11:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 01 2019 13:16:24 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
* First, count all the primes from 1 to **n** using **Sieve**. Remember to terminate the outer loop at **sqrt(n)**.
* Next , iterate over each positon and get the count of prime positions, call it `k`.
* So, for the `k` prime numbers, we have limited choice, we need to arrange them in `k` prime spots.
* For the `n-k` non prime numbers, we also have limited choice. We need to arrange them in `n-k` non prime spots.
* Both the events are indepent, so the total ways would be product of them.
* Number of ways to arrange `k` objects in `k` boxes is `k!`.
* Use the property that `(a*b) %m = ( (a%m) * (b%m) ) % m`.

```
class Solution
{
public:
    int numPrimeArrangements(int n);
};

int Solution :: numPrimeArrangements(int n)
{
    vector<bool> prime(n + 1, true);
    prime[0] = false;
    prime[1] = false;
    
    for(int i = 2; i <= sqrt(n); i++)
    {
        if(prime[i])
            for(int factor = 2; factor*i <= n; factor++)
                prime[factor*i] = false;
    }
    
    int primeIndices = 0;
    for(int i = 1; i <= n; i++)
        if(prime[i])
            primeIndices++;
    
    int mod = 1e9 + 7, res = 1;
	
    for(int i = 1; i <= primeIndices; i++)
        res = (1LL*res*i) % mod;
    for(int i = 1; i<= (n-primeIndices); i++)
        res = (1LL*res*i) % mod;
    
    return res;
    
}
```
</p>


