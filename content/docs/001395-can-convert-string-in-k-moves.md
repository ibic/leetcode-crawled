---
title: "Can Convert String in K Moves"
weight: 1395
#id: "can-convert-string-in-k-moves"
---
## Description
<div class="description">
<p>Given two strings&nbsp;<code>s</code>&nbsp;and&nbsp;<code>t</code>, your goal is to convert&nbsp;<code>s</code>&nbsp;into&nbsp;<code>t</code>&nbsp;in&nbsp;<code>k</code><strong>&nbsp;</strong>moves or less.</p>

<p>During the&nbsp;<code>i<sup>th</sup></code>&nbsp;(<font face="monospace"><code>1 &lt;= i &lt;= k</code>)&nbsp;</font>move you can:</p>

<ul>
	<li>Choose any index&nbsp;<code>j</code>&nbsp;(1-indexed) from&nbsp;<code>s</code>, such that&nbsp;<code>1 &lt;= j &lt;= s.length</code>&nbsp;and <code>j</code>&nbsp;has not been chosen in any previous move,&nbsp;and shift the character at that index&nbsp;<code>i</code>&nbsp;times.</li>
	<li>Do nothing.</li>
</ul>

<p>Shifting a character means replacing it by the next letter in the alphabet&nbsp;(wrapping around so that&nbsp;<code>&#39;z&#39;</code>&nbsp;becomes&nbsp;<code>&#39;a&#39;</code>). Shifting a character by&nbsp;<code>i</code>&nbsp;means applying the shift operations&nbsp;<code>i</code>&nbsp;times.</p>

<p>Remember that any index&nbsp;<code>j</code>&nbsp;can be picked at most once.</p>

<p>Return&nbsp;<code>true</code>&nbsp;if it&#39;s possible to convert&nbsp;<code>s</code>&nbsp;into&nbsp;<code>t</code>&nbsp;in no more than&nbsp;<code>k</code>&nbsp;moves, otherwise return&nbsp;<code>false</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;input&quot;, t = &quot;ouput&quot;, k = 9
<strong>Output:</strong> true
<b>Explanation: </b>In the 6th move, we shift &#39;i&#39; 6 times to get &#39;o&#39;. And in the 7th move we shift &#39;n&#39; to get &#39;u&#39;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abc&quot;, t = &quot;bcd&quot;, k = 10
<strong>Output:</strong> false
<strong>Explanation: </strong>We need to shift each character in s one time to convert it into t. We can shift &#39;a&#39; to &#39;b&#39; during the 1st move. However, there is no way to shift the other characters in the remaining moves to obtain t from s.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aab&quot;, t = &quot;bbb&quot;, k = 27
<strong>Output:</strong> true
<b>Explanation: </b>In the 1st move, we shift the first &#39;a&#39; 1 time to get &#39;b&#39;. In the 27th move, we shift the second &#39;a&#39; 27 times to get &#39;b&#39;.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length, t.length &lt;= 10^5</code></li>
	<li><code>0 &lt;= k &lt;= 10^9</code></li>
	<li><code>s</code>, <code>t</code> contain&nbsp;only lowercase English letters.</li>
</ul>

</div>

## Tags
- String (string)
- Greedy (greedy)

## Companies
- infosys - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] O(n) Count the shift displacement, w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Aug 09 2020 00:01:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 10 2020 11:49:59 GMT+0800 (Singapore Standard Time)

<p>
1. Check if the 2 strings `s` and `t` have same length, if not, return `false`; 
2. Loop through the input strings and count the shift displacement, in case negative, plus 26 to make it positive;
3. If same displacement appears multiple times, the 1st time use the displacement itself, the 2nd time add `26 `to it, the 3rd time add `26 * (3 - 1) = 52 `, the 4th time add `26 * (4 - 3) = 78`, etc.; if after adding the result is greater than `k`, return `false`;
4. If never encounter `false` in the above 3, return `false`.

```java
    public boolean canConvertString(String s, String t, int k) {
        if (s.length() != t.length()) {
            return false;
        }
        int[] count = new int[26];
        for (int i = 0; i < s.length(); ++i) {
            int diff = (t.charAt(i) - s.charAt(i) + 26) % 26;
            if (diff > 0 && diff + count[diff] * 26 > k) {
                return false;
            }
            ++count[diff];
        }
        return true;
    }
```
```python
    def canConvertString(self, s: str, t: str, k: int) -> bool:
        if len(s) != len(t):
            return False
        cnt = [0] * 26
        for cs, ct in zip(s, t):
            diff = (ord(ct) - ord(cs)) % 26
            if diff > 0 and cnt[diff] * 26 + diff > k:
                return False
            cnt[diff] += 1
        return True
```
**Analysis:**

Time: O(n), space: O(1), where n = Math.min(s.length(), t.length()).
</p>


### O(n) c++ simple solution with explanation
- Author: CodingMission
- Creation Date: Sun Aug 09 2020 00:03:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 09 2020 06:14:12 GMT+0800 (Singapore Standard Time)

<p>
Basic idea is to convert str1 to str2 using less or equal to k iterations where in ith iteration you can choose **atmost** one character to shift **exactly** i places.

Intuition :
1. if string size is not equal, return false;
2. use a hashmap to check how many times the diff is same and keep using the iteration with the formula (26 *(num of occurreces of same diff) + diff;

For eg. "abc" -> "bcd", diff for a -> b is 1 and we can check hashmap for occurences of diff as following:


```hashMap = {....... {1 = 0}....}```
We can use iteration 1 in this case and we update the hashmap to the following to denote that 1 iteration has already been used.
```hashMap = {....... {1 = 1}....}```

Now to shift b -> c also we need 1 Iteration,which is already being used.
```hashMap = {....... {1 = 1}....}```

Since 1 iteration is already being used, we can use 26 + 1, and update hash map :
```hashMap = {....... {1 = 2}....}```
and we also keep track of the maximum iteration found so far.
...
...
and so on.

Finally if max iteration <= k return true.

Note: Please upvote if you find this useful, this motivates me to write more descriptive solutions

```
class Solution {
public:
    bool canConvertString(string s, string t, int k) {
        int m = s.length(), n = t.length(), count = 0;
        if (m != n) return false;
        unordered_map<int, int> mp;
        for (int i = 0; i < m; i++) {
            if (t[i] == s[i]) continue;
            int diff = t[i] - s[i] < 0 ? 26 + t[i] - s[i] : t[i] - s[i];
            if (mp.find(diff) == mp.end()) {
                count = max(count, diff);
            } else {
                count = max(count, (mp[diff] * 26) + diff);
            }
            mp[diff]++;
            if (count > k) return false;
        }
        return count <= k;
    }
    
}; 
```

More Concise solution

```
class Solution {
public:
    bool canConvertString(string s, string t, int k) {
        int m = s.length(), n = t.length(), count = 0;
        if (m != n) return false;
        unordered_map<int, int> mp;
        for (int i = 0; i < m; i++) {
            if (t[i] == s[i]) continue;
            int diff = t[i] - s[i] < 0 ? 26 + t[i] - s[i] : t[i] - s[i];
            count = max(count, (mp[diff] * 26) + diff);
            mp[diff]++;
            if (count > k) return false;
        }
        return count <= k;
    }
    
};
</p>


### C++ O(n) Track Multiplier
- Author: votrubac
- Creation Date: Sun Aug 09 2020 00:02:14 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 10 2020 10:03:19 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
You can only shift a letter once, and you cannot change more than once letter by the same number of shifts (`i`). In other words, if you shift one letter by `1`, no other letters can be shifted by `1`. If you need to shift by `1` again, you need to use "wrapping" and shift by `27` (which is `1 + 26`).

Therefore, if our strings are `"aaa"` and `"bbb"`, we need to shift the first letter by `1`, the second by `27` (`1 + 26`), and the third - by `53` (`1 + 2 * 26`). So, you  can accomplish the task if `k` is equal or greater than `53`.

#### Algorithm
Go through the strings and determine `shift` for each letter. If letter in `t` is smaller, we need to "wrap" it by adding 26 (e.g. changing `b` into `a` needs 25 shifts).

After we use a certain number of shifts, we need to add 26, 52, and so on if we need to use it again. So we track the multiplier for each number of shifts in `mul`. If, at any time, the needed number of shifts exceeds `k`, we return `false`.

```cpp
bool canConvertString(string s, string t, int k) {
    if (s.size() != t.size())
        return false;
    int mul[26] = {};
    for (int i = 0; i < s.size(); ++i) {
        int shift = t[i] - s[i] + (t[i] < s[i] ? 26 : 0);
        if (shift != 0 && shift + mul[shift] * 26 > k)
            return false;
        ++mul[shift];
    }
    return true;
}
```
</p>


