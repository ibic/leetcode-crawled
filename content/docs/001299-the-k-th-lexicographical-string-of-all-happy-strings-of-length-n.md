---
title: "The k-th Lexicographical String of All Happy Strings of Length n"
weight: 1299
#id: "the-k-th-lexicographical-string-of-all-happy-strings-of-length-n"
---
## Description
<div class="description">
<p>A <strong>happy string</strong> is a string that:</p>

<ul>
	<li>consists only of letters of the set <code>[&#39;a&#39;, &#39;b&#39;, &#39;c&#39;]</code>.</li>
	<li><code>s[i] != s[i + 1]</code>&nbsp;for all values of <code>i</code> from <code>1</code> to <code>s.length - 1</code> (string is 1-indexed).</li>
</ul>

<p>For example, strings <strong>&quot;abc&quot;, &quot;ac&quot;, &quot;b&quot;</strong> and <strong>&quot;abcbabcbcb&quot;</strong> are all happy strings and strings <strong>&quot;aa&quot;, &quot;baa&quot;</strong> and&nbsp;<strong>&quot;ababbc&quot;</strong> are not happy strings.</p>

<p>Given two integers <code>n</code> and <code>k</code>, consider a list of all happy strings of length <code>n</code> sorted in lexicographical order.</p>

<p>Return <em>the kth string</em> of this list or return an <strong>empty string</strong>&nbsp;if there are less than <code>k</code> happy strings of length <code>n</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 1, k = 3
<strong>Output:</strong> &quot;c&quot;
<strong>Explanation:</strong> The list [&quot;a&quot;, &quot;b&quot;, &quot;c&quot;] contains all happy strings of length 1. The third string is &quot;c&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 1, k = 4
<strong>Output:</strong> &quot;&quot;
<strong>Explanation:</strong> There are only 3 happy strings of length 1.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 3, k = 9
<strong>Output:</strong> &quot;cab&quot;
<strong>Explanation:</strong> There are 12 different happy string of length 3 [&quot;aba&quot;, &quot;abc&quot;, &quot;aca&quot;, &quot;acb&quot;, &quot;bab&quot;, &quot;bac&quot;, &quot;bca&quot;, &quot;bcb&quot;, &quot;cab&quot;, &quot;cac&quot;, &quot;cba&quot;, &quot;cbc&quot;]. You will find the 9th string = &quot;cab&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 2, k = 7
<strong>Output:</strong> &quot;&quot;
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 10, k = 100
<strong>Output:</strong> &quot;abacbabacb&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10</code></li>
	<li><code>1 &lt;= k &lt;= 100</code></li>
</ul>

<div id="vidyowebrtcscreenshare_is_installed">&nbsp;</div>
</div>

## Tags
- Backtracking (backtracking)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java] DFS and Math
- Author: votrubac
- Creation Date: Sun Apr 19 2020 00:03:12 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 14 2020 14:26:07 GMT+0800 (Singapore Standard Time)

<p>
#### Aproach 1: DFS
Here, DFS builds all possible strings in a lexicographical order. Each time a string is built, we decrease the global counter `k`. When `k` reaches zero, we short-circut DFS and build the resulting string right-to-left. 

> Exploring references and optional parameters for a concise code.

```cpp
string getHappyString(int n, int &k, int p = 0, char last_ch = 0) {
    if (p == n)
        --k;
    else
        for (char ch = \'a\'; ch <= \'c\'; ++ch) {
            if (ch != last_ch) {
                auto res = getHappyString(n, k, p + 1, ch);
                if (k == 0)
                    return string(1, ch) + res;
            }
        }
    return "";
}
```
**Complexity Analysis**
- Time: O(n * k); we evaluate `k` strings of the size `n`.
- Memory: O(n) for the stack (not including the output).

#### Aproach 2: Math
For the string of size `n`, we can build `3 * pow(2, n - 1)` strings. So, if `k <= pow(2, n - 1)`, then the first letter is `a`, `k <= 2 * pow(2, n - 1)` - then `b`, otherwise `c`. We can also return empty string right away if `k > 3 * pow(2, n - 1)`.

We continue building the string using the same approach but now with two choices.

**C++**
```cpp
string getHappyString(int n, int k) {
    auto prem = 1 << (n - 1);
    if (k > 3 * prem)
        return "";
    string s = string(1, \'a\' + (k - 1) / prem);
    while (prem > 1) {
        k = (k - 1) % prem + 1;
        prem >>= 1;
        s += (k - 1) / prem == 0 ? \'a\' + (s.back() == \'a\') : \'b\' + (s.back() != \'c\');
    }
    return s;
}
```

**Java**
```java
public String getHappyString(int n, int k) {
    int prem = 1 << (n - 1);
    if (k > 3 * prem)
        return "";
    int ch = \'a\' + (k - 1) / prem;
    StringBuilder sb = new StringBuilder(Character.toString(ch));
    while (prem > 1) {
        k = (k - 1) % prem + 1;
        prem >>= 1;
        ch = (k - 1) / prem == 0 ? \'a\' + (ch == \'a\' ? 1 : 0) : \'b\' + (ch != \'c\' ? 1 : 0);
        sb.append((char)ch);
    }
    return sb.toString();
}  
```

**Complexity Analysis**
- Time: O(n); we build one string of the size `n`.
- Memory: O(1) - not considering output.
</p>


### [Python3] Easy Python3 BFS + Graph Like
- Author: localhostghost
- Creation Date: Sun Apr 19 2020 05:55:14 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 20 2020 19:53:01 GMT+0800 (Singapore Standard Time)

<p>
` nextLetter ` is like a graph. `a -> bc`, `b -> ac`, `c -> ab`. 
Since we are doing BFS with the next node in the graph already sorted, 
we are guaranteed to have a sorted queue with all the element of the 
same length.

Break when the first string in the queue is length `n` and find the `k`th value in the queue.

```
class Solution:
    def getHappyString(self, n: int, k: int) -> str:
        nextLetter = {\'a\': \'bc\', \'b\': \'ac\', \'c\': \'ab\'} 
        q = collections.deque([\'a\', \'b\', \'c\'])
        while len(q[0]) != n:
            u = q.popleft()    
            for v in nextLetter[u[-1]]:
                q.append(u + v)
        return q[k - 1] if len(q) >= k else \'\'     
```
</p>


### Easiest solution using bfs---C++
- Author: mr_233
- Creation Date: Sun Apr 19 2020 00:01:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 15:20:29 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    string getHappyString(int n, int k) {
        vector<string> ans;
        queue<string> q;
        q.push("a");
        q.push("b");
        q.push("c");
        while(!q.empty())
        {
            string x=q.front();
            q.pop();
            if(x.length()==n)
            {
                ans.push_back(x);
            }
            string s1="",s2="";
            if(x[x.length()-1]==\'a\')
            {
                s1=x+"b";
                s2=x+"c";
                
            }
            if(x[x.length()-1]==\'b\')
            {
                s1=x+"a";
                s2=x+"c";
                
            }
            if(x[x.length()-1]==\'c\')
            {
                s1=x+"a";
                s2=x+"b";
                
            }
            
            //push only when less than n
            
            if(s1.length()<=n)
            {
                q.push(s1);
            }
            if(s2.length()<=n)
            {
                q.push(s2);
            }
        }
   
        string s="";
        if(k-1>=ans.size())
        {
            s="";
        }
        else
        {
            s=ans[k-1];
        }
        return s;
        
    
    }
};
```
</p>


