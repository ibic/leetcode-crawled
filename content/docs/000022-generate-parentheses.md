---
title: "Generate Parentheses"
weight: 22
#id: "generate-parentheses"
---
## Description
<div class="description">
<p>Given <code>n</code> pairs of parentheses, write a function to <em>generate all combinations of well-formed parentheses</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> n = 3
<strong>Output:</strong> ["((()))","(()())","(())()","()(())","()()()"]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> n = 1
<strong>Output:</strong> ["()"]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 8</code></li>
</ul>

</div>

## Tags
- String (string)
- Backtracking (backtracking)

## Companies
- Facebook - 13 (taggedByAdmin: false)
- Amazon - 12 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: true)
- Apple - 4 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Walmart Labs - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Spotify - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Bloomberg - 6 (taggedByAdmin: false)
- Yandex - 5 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)
- Nutanix - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Lyft - 6 (taggedByAdmin: false)
- Yelp - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Samsung - 2 (taggedByAdmin: false)
- Zenefits - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach 1: Brute Force

**Intuition**

We can generate all $$2^{2n}$$ sequences of `'('` and `')'` characters.  Then, we will check if each one is valid.

**Algorithm**

To generate all sequences, we use a recursion.  All sequences of length `n` is just `'('` plus all sequences of length `n-1`, and then `')'` plus all sequences of length `n-1`.

To check whether a sequence is valid, we keep track of `balance`, the net number of opening brackets minus closing brackets.  If it falls below zero at any time, or doesn't end in zero, the sequence is invalid - otherwise it is valid.

<iframe src="https://leetcode.com/playground/eDRvbWjL/shared" frameBorder="0" width="100%" height="500" name="eDRvbWjL"></iframe>

**Complexity Analysis**

* Time Complexity : $$O(2^{2n}n)$$.  For each of $$2^{2n}$$ sequences, we need to create and validate the sequence, which takes $$O(n)$$ work.

* Space Complexity : $$O(2^{2n}n)$$.  Naively, every sequence could be valid.  See [Approach 3](#approach-3-closure-number) for development of a tighter asymptotic bound.
<br />
<br />
---
#### Approach 2: Backtracking

**Intuition and Algorithm**

Instead of adding `'('` or `')'` every time as in [Approach 1](#approach-1-brute-force), let's only add them when we know it will remain a valid sequence.  We can do this by keeping track of the number of opening and closing brackets we have placed so far.

We can start an opening bracket if we still have one (of `n`) left to place.  And we can start a closing bracket if it would not exceed the number of opening brackets.

<iframe src="https://leetcode.com/playground/npPa38Mh/shared" frameBorder="0" width="100%" height="378" name="npPa38Mh"></iframe>

**Complexity Analysis**

Our complexity analysis rests on understanding how many elements there are in `generateParenthesis(n)`.  This analysis is outside the scope of this article, but it turns out this is the `n`-th Catalan number $$\dfrac{1}{n+1}\binom{2n}{n}$$, which is bounded asymptotically by $$\dfrac{4^n}{n\sqrt{n}}$$.

* Time Complexity : $$O(\dfrac{4^n}{\sqrt{n}})$$.  Each valid sequence has at most `n` steps during the backtracking procedure.

* Space Complexity : $$O(\dfrac{4^n}{\sqrt{n}})$$, as described above, and using $$O(n)$$ space to store the sequence.
<br />
<br />
---

#### Approach 3: Closure Number

**Intuition**

To enumerate something, generally we would like to express it as a sum of disjoint subsets that are easier to count.

Consider the *closure number* of a valid parentheses sequence `S`: the least `index >= 0` so that `S[0], S[1], ..., S[2*index+1]` is valid.  Clearly, every parentheses sequence has a unique *closure number*.  We can try to enumerate them individually.

**Algorithm**

For each closure number `c`, we know the starting and ending brackets must be at index `0` and `2*c + 1`. Then, the `2*c` elements between must be a valid sequence, plus the rest of the elements must be a valid sequence.

<iframe src="https://leetcode.com/playground/Z3ZYfRAo/shared" frameBorder="0" width="100%" height="293" name="Z3ZYfRAo"></iframe>

**Complexity Analysis**

* Time and Space Complexity : $$O(\dfrac{4^n}{\sqrt{n}})$$.  The analysis is similar to [Approach 2](#approach-2-backtracking).

## Accepted Submission (python3)
```python3
class Solution:
    def genDFS(self, s, left: int, right: int, n: int):
        lens = len(s)
        if left == right and left == n:
            self.r.append(s)
            return
        if right > left:
            return
        if left < n:
            self.genDFS(s + '(', left + 1, right, n)
        if right < n and right < left:
            self.genDFS(s + ')', left, right + 1, n)
    def genBT(self, s, left: int, right: int, n: int):
        lens = len(s)
        if left == right and left == n:
            self.r.append(''.join(s[:]))
            return
        if right > left:
            return
        if left < n:
            s += ['(']
            self.genBT(s, left + 1, right, n)
            del s[-1]
        if right < n and right < left:
            s += [')']
            self.genBT(s, left, right + 1, n)
            del s[-1]
    def generateParenthesis(self, n: int) -> List[str]:
        self.r = []
        #s = ""
        #self.genDFS(s, 0, 0, n)
        s = []
        self.genBT(s, 0, 0, n)
        return self.r
```

## Top Discussions
### Easy to understand Java backtracking solution
- Author: brobins9
- Creation Date: Thu Feb 12 2015 01:19:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 15:36:54 GMT+0800 (Singapore Standard Time)

<p>
     public List<String> generateParenthesis(int n) {
            List<String> list = new ArrayList<String>();
            backtrack(list, "", 0, 0, n);
            return list;
        }
        
        public void backtrack(List<String> list, String str, int open, int close, int max){
            
            if(str.length() == max*2){
                list.add(str);
                return;
            }
            
            if(open < max)
                backtrack(list, str+"(", open+1, close, max);
            if(close < open)
                backtrack(list, str+")", open, close+1, max);
        }

The idea here is to only add '(' and ')' that we know will guarantee us a solution (instead of adding 1 too many close). Once we add a '(' we will then discard it and try a ')' which can only close a valid '('. Each of these steps are recursively called.
</p>


### Concise recursive C++ solution
- Author: klyc0k
- Creation Date: Tue Oct 28 2014 03:43:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 02:26:49 GMT+0800 (Singapore Standard Time)

<p>
The idea is intuitive. Use two integers to count the remaining left parenthesis (n) and the right parenthesis (m) to be added. At each function call add a left parenthesis if n >0 and add a right parenthesis if m>0. Append the result and terminate recursive calls when both m and n are zero.

    class Solution {
    public:
        vector<string> generateParenthesis(int n) {
            vector<string> res;
            addingpar(res, "", n, 0);
            return res;
        }
        void addingpar(vector<string> &v, string str, int n, int m){
            if(n==0 && m==0) {
                v.push_back(str);
                return;
            }
            if(m > 0){ addingpar(v, str+")", n, m-1); }
            if(n > 0){ addingpar(v, str+"(", n-1, m+1); }
        }
    };
</p>


### An iterative method.
- Author: left.peter
- Creation Date: Sat Sep 20 2014 02:41:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 16:11:46 GMT+0800 (Singapore Standard Time)

<p>
My method is DP. First consider how to get the result f(n) from previous result f(0)...f(n-1).
Actually, the result f(n) will be put an extra () pair to f(n-1). Let the "(" always at the first position, to produce a valid result, we can only put ")" in a way that there will be i pairs () inside the extra () and n - 1 - i pairs () outside the extra pair.

Let us consider an example to get clear view:

f(0):  ""

f(1):  "("f(0)")"

f(2): "("f(0)")"f(1), "("f(1)")"

f(3): "("f(0)")"f(2), "("f(1)")"f(1), "("f(2)")"

So f(n) = "("f(0)")"f(n-1) , "("f(1)")"f(n-2) "("f(2)")"f(n-3) ... "("f(i)")"f(n-1-i) ... "(f(n-1)")"

Below is my code:

    public class Solution
    {
        public List<String> generateParenthesis(int n)
        {
            List<List<String>> lists = new ArrayList<>();
            lists.add(Collections.singletonList(""));
            
            for (int i = 1; i <= n; ++i)
            {
                final List<String> list = new ArrayList<>();
                
                for (int j = 0; j < i; ++j)
                {
                    for (final String first : lists.get(j))
                    {
                        for (final String second : lists.get(i - 1 - j))
                        {
                            list.add("(" + first + ")" + second);
                        }
                    }
                }
                
                lists.add(list);
            }
            
            return lists.get(lists.size() - 1);
        }
    }
</p>


