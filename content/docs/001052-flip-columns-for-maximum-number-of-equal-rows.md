---
title: "Flip Columns For Maximum Number of Equal Rows"
weight: 1052
#id: "flip-columns-for-maximum-number-of-equal-rows"
---
## Description
<div class="description">
<p>Given a <code>matrix</code> consisting of 0s and 1s, we may choose any number of columns in the matrix and flip <strong>every</strong>&nbsp;cell in that column.&nbsp; Flipping a cell changes the value of that cell from 0 to 1 or from 1 to 0.</p>

<p>Return the maximum number of rows that have all values equal after some number of flips.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[0,1],[1,1]]</span>
<strong>Output: </strong><span id="example-output-1">1</span>
<strong>Explanation: </strong>After flipping no values, 1 row has all values equal.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[[0,1],[1,0]]</span>
<strong>Output: </strong><span id="example-output-2">2</span>
<strong>Explanation: </strong>After flipping values in the first column, both rows have equal values.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[[0,0,0],[0,0,1],[1,1,0]]</span>
<strong>Output: </strong><span id="example-output-3">2</span>
<strong>Explanation: </strong>After flipping values in the first two columns, the last two rows have equal values.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= matrix.length &lt;= 300</code></li>
	<li><code>1 &lt;= matrix[i].length &lt;= 300</code></li>
	<li>All <code>matrix[i].length</code>&#39;s are equal</li>
	<li><code>matrix[i][j]</code> is&nbsp;<code>0</code> or <code>1</code></li>
</ol>
</div>
</div>
</div>

</div>

## Tags
- Hash Table (hash-table)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java easy solution + explanation
- Author: motorix
- Creation Date: Sun Jun 02 2019 14:08:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2020 03:19:36 GMT+0800 (Singapore Standard Time)

<p>
Assume the ```i-th``` row is an all-0s row after flipping ```x``` columns.
1. If there are any other all-0s row, say ```j-th``` row, then the ```j-th``` row before flipping should be the same as the ```i-th``` row.
2. If there are any other all-1s row, say ```k-th``` row, then the ```k-th``` row before flipping should be totally different from the ```i-th``` row.

For example:	
```
 [1,0,0,1,0]                                                       [0,0,0,0,0]  // all-0s
 [1,0,0,1,0]  after flipping every cell in 0-th and 3-th columns   [0,0,0,0,0]  // all-0s
 [1,0,1,1,1] ----------------------------------------------------> [0,0,1,0,1]
 [0,1,1,0,1]                                                       [1,1,1,1,1]  // all-1s
 [1,0,0,1,1]                                                       [0,0,0,0,1]
 
 1st, 2nd, and 4th rows have all values equal.
```
After flipping, the 1st and 2nd rows are all-0s, and the 4th row are all-1s. We can find that before flipping:
    the 2nd row is the same as the 1st row.
    the 4th row is totally different from the 1st row.

So, the problem is transformed to: **Find the i-th row, which has the most same or totaly different rows in the matrix.**

--

Java:
```
class Solution {
    public int maxEqualRowsAfterFlips(int[][] matrix) {
        int ans = 0;
        int m = matrix.length, n = matrix[0].length;
        for(int i = 0; i < m; i++) {
            int cnt = 0;
            int[] flip = new int[n];
            for(int j = 0; j < n; j++) flip[j] = 1 - matrix[i][j];
            for(int k = i; k < m; k++) {
                if(Arrays.equals(matrix[k], matrix[i]) || Arrays.equals(matrix[k], flip)) cnt++;
            }
            ans = Math.max(ans, cnt);
        }
        return ans;
    }
}
```
</p>


### [Python] 1 Line
- Author: lee215
- Creation Date: Sun Jun 02 2019 12:04:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 12:04:01 GMT+0800 (Singapore Standard Time)

<p>
**Python:**
```
    def maxEqualRowsAfterFlips(self, A):
        return max(collections.Counter(tuple(x ^ r[0] for x in r) for r in A).values())
```
</p>


### Simple C++ Solution with comments
- Author: sourov_roy
- Creation Date: Sun Jun 02 2019 12:58:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 12:58:44 GMT+0800 (Singapore Standard Time)

<p>
Finding maximum number of rows that have identical relative ordering with respect to the top elements. 

```
class Solution {
public:
    int maxEqualRowsAfterFlips(vector<vector<int>>& matrix) {
        unordered_map<string,int> hmap;
        for(auto x: matrix){
            string s=""; 
            //s keeps track of relative ordering of elements in a row w.r.t the first element
            int top=x[0];
            for(int i=0;i<x.size();i++){
                if(x[i]==top)
                    s+=\'1\';
                else
                    s+=\'0\';
            }
            hmap[s]++; 
        }
        
        int result=0;
        for(auto &[key,val]:hmap)
            result=max(result,val); 
        //rows that have identical relative ordering, can be simultaneously toggled columnwise in order to make all
        //elements of any of those rows unique. So the answer is maximum number of rows with identical relative ordering.
        return result;
    }
};
```
</p>


