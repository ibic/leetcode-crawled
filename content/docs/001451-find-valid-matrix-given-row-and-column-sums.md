---
title: "Find Valid Matrix Given Row and Column Sums"
weight: 1451
#id: "find-valid-matrix-given-row-and-column-sums"
---
## Description
<div class="description">
<p>You are given two arrays <code>rowSum</code> and <code>colSum</code> of non-negative integers where <code>rowSum[i]</code> is the sum of the elements in the <code>i<sup>th</sup></code> row and <code>colSum[j]</code> is the sum of the elements of the <code>j<sup>th</sup></code> column of a 2D matrix. In other words, you do not know the elements of the matrix, but you do know the sums of each row and column.</p>

<p>Find any matrix of <strong>non-negative</strong> integers of size <code>rowSum.length x colSum.length</code> that satisfies the <code>rowSum</code> and <code>colSum</code> requirements.</p>

<p>Return <em>a 2D array representing <strong>any</strong> matrix that fulfills the requirements</em>. It&#39;s guaranteed that <strong>at least one </strong>matrix that fulfills the requirements exists.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> rowSum = [3,8], colSum = [4,7]
<strong>Output:</strong> [[3,0],
         [1,7]]
<strong>Explanation:</strong>
0th row: 3 + 0 = 0 == rowSum[0]
1st row: 1 + 7 = 8 == rowSum[1]
0th column: 3 + 1 = 4 == colSum[0]
1st column: 0 + 7 = 7 == colSum[1]
The row and column sums match, and all matrix elements are non-negative.
Another possible matrix is: [[1,2],
                             [3,5]]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> rowSum = [5,7,10], colSum = [8,6,8]
<strong>Output:</strong> [[0,5,0],
         [6,1,0],
         [2,0,8]]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> rowSum = [14,9], colSum = [6,9,8]
<strong>Output:</strong> [[0,9,5],
         [6,0,3]]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> rowSum = [1,0], colSum = [1]
<strong>Output:</strong> [[1],
         [0]]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> rowSum = [0], colSum = [0]
<strong>Output:</strong> [[0]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= rowSum.length, colSum.length &lt;= 500</code></li>
	<li><code>0 &lt;= rowSum[i], colSum[i] &lt;= 10<sup>8</sup></code></li>
	<li><code>sum(rows) == sum(columns)</code></li>
</ul>
</div>

## Tags
- Greedy (greedy)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy and Concise with Prove
- Author: lee215
- Creation Date: Sun Oct 04 2020 00:04:07 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 05 2020 15:52:21 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
The greedy pick won\'t break anything, so just take as much as possible.
<br>

# Explanation
For each result value at `A[i][j]`,
we greedily take the min(row[i], col[j]).

Then we update the row sum and col sum:
`row[i] -= A[i][j]`
`col[j] -= A[i][j]`
<br>

# Easy Prove
1) `A[i][j]` will clear either `row[i]` or `col[j]`,
that means either `row[i] == 0` and `col[j] == 0` in the end.

2) "It\'s guaranteed that at least one matrix that fulfills the requirements exists."
Also `sum(row) == sum(col)` always valid.

In the end, if `row[i] > 0` and `col[j] > 0`, 
then A[i][j] clear neither of them, based on 1) that\'s impossible.

So either sum(row) == 0 or sum(col) == 0.
Based on 2), we can have that `sum(row) == sum(col) == 0`.
That mean we find a right answer.

Done.
<br>

# Solution 1
Time `O(mn)`
Space `O(mn)`
<br>

**Java:**
```java
    public int[][] restoreMatrix(int[] row, int[] col) {
        int m = row.length, n = col.length;
        int[][] A = new int[m][n];
        for (int i = 0; i < m; ++i) {
            for (int j = 0 ; j < n; ++j) {
                A[i][j] = Math.min(row[i], col[j]);
                row[i] -= A[i][j];
                col[j] -= A[i][j];
            }
        }
        return A;
    }
```

**C++:**
```cpp
    vector<vector<int>> restoreMatrix(vector<int>& row, vector<int>& col) {
        int m = row.size(), n = col.size();
        vector<vector<int>> A(m, vector<int>(n, 0));
        for (int i = 0; i < m; ++i) {
            for (int j = 0 ; j < n; ++j) {
                A[i][j] = min(row[i], col[j]);
                row[i] -= A[i][j];
                col[j] -= A[i][j];
            }
        }
        return A;
    }
```

**Python:**
```py
    def restoreMatrix(self, row, col):
        m, n = len(row), len(col)
        A = [[0] * n for i in xrange(m)]
        for i in xrange(m):
            for j in xrange(n):
                A[i][j] = min(row[i], col[j])
                row[i] -= A[i][j]
                col[j] -= A[i][j]
        return A
```

<br><br>

# Solution 2
Time `O(mn)` for initializing output
Time `O(m + n)` for process
Space `O(mn)`
<br>

**Java**
```java
    public int[][] restoreMatrix(int[] row, int[] col) {
        int m = row.length, n = col.length, i = 0, j = 0, a;
        int[][] A = new int[m][n];
        while (i < m && j < n) {
            a = A[i][j] = Math.min(row[i], col[j]);
            if ((row[i] -= a) == 0) ++i;
            if ((col[j] -= a) == 0) ++j;
        }
        return A;
    }
```
**C++**
```cpp
    vector<vector<int>> restoreMatrix(vector<int>& row, vector<int>& col) {
        int m = row.size(), n = col.size(), i = 0, j = 0, a;
        vector<vector<int>> A(m, vector<int>(n, 0));
        while (i < m && j < n) {
            a = A[i][j] = min(row[i], col[j]);
            if ((row[i] -= a) == 0) ++i;
            if ((col[j] -= a) == 0) ++j;
        }
        return A;
    }
```

</p>


### O(mn) Simple Solution (with video)
- Author: akku_1
- Creation Date: Sun Oct 04 2020 00:03:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 12:17:18 GMT+0800 (Singapore Standard Time)

<p>


click link :- https://youtu.be/Qmg_uzlmw1g?t=1


Space (m*n)
Time (m+n)  (  igonring the matrix creation )

Start with first row anf first column 
fill the cell with minimum of rowSum[i] and colSum[j] 
reduce this minimum from rowSum[i] and colSum[j]
if minimum was rowSum[i] then move to next row 
if minimum was colSum[j] then move to next col


code 

		vector<vector<int>> restoreMatrix(vector<int>& rowSum, vector<int>& colSum) 
			{
				int n=rowSum.size();
				int m=colSum.size();
				int i=0,j=0;
				vector<vector<int>> ans(n,vector<int>(m,0));
				while(i<n&&j<m)
				{
					int x=min(rowSum[i],colSum[j]);
					ans[i][j]=x;
					rowSum[i]-=x;
					colSum[j]-=x;
					if(rowSum[i]==0)i++;
					if(colSum[j]==0)j++;
				}
				return ans;
			}
</p>


### Easy to Understand Java
- Author: ngytlcdsalcp1
- Creation Date: Sun Oct 04 2020 00:13:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 00:13:57 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int[][] restoreMatrix(int[] rowSum, int[] colSum) {
        int m = rowSum.length;
        int n = colSum.length;
        int[][] mat = new int[m][n];
        
        Set<Integer> rSet = new HashSet();
        Set<Integer> cSet = new HashSet();
        
        while(rSet.size() != m && cSet.size() != n) {
            int ri = minInd(rowSum, rSet);
            int ci = minInd(colSum, cSet);
            if(rowSum[ri] < colSum[ci]) {
                mat[ri][ci] = rowSum[ri];
                colSum[ci] -= rowSum[ri];
                rSet.add(ri);
            } else {
                mat[ri][ci] = colSum[ci];
                rowSum[ri] -= colSum[ci];
                cSet.add(ci);
            }
        }
        
        return mat;
    }
    
    private int minInd(int[] a, Set<Integer> set) {
        int min = Integer.MAX_VALUE;
        int ind = 0;
        for(int i = 0; i < a.length; i++) {
            if(a[i] < min && !set.contains(i)) {
                min = a[i];
                ind = i;
            }
        }
        return ind;
    }
}
```
</p>


