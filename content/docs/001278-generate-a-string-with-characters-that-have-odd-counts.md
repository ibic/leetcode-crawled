---
title: "Generate a String With Characters That Have Odd Counts"
weight: 1278
#id: "generate-a-string-with-characters-that-have-odd-counts"
---
## Description
<div class="description">
<p>Given an&nbsp;integer <code>n</code>, <em>return a string with <code>n</code>&nbsp;characters such that each character in such string occurs <strong>an odd number of times</strong></em>.</p>

<p>The returned string must contain only lowercase English letters. If there are multiples valid strings, return <strong>any</strong> of them. &nbsp;</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 4
<strong>Output:</strong> &quot;pppz&quot;
<strong>Explanation:</strong> &quot;pppz&quot; is a valid string since the character &#39;p&#39; occurs three times and the character &#39;z&#39; occurs once. Note that there are many other valid strings such as &quot;ohhh&quot; and &quot;love&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2
<strong>Output:</strong> &quot;xy&quot;
<strong>Explanation:</strong> &quot;xy&quot; is a valid string since the characters &#39;x&#39; and &#39;y&#39; occur once. Note that there are many other valid strings such as &quot;ag&quot; and &quot;ur&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 7
<strong>Output:</strong> &quot;holasss&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 500</code></li>
</ul>

</div>

## Tags
- String (string)

## Companies
- DiDi - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] One Lines
- Author: lee215
- Creation Date: Sun Mar 08 2020 12:03:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 12:03:11 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
If `n` is odd, we return `"bbbb....b"`.
If `n` is even, we return `"baaa...a"`.
<br>

## **Complexity**
Time `O(N)`
Space `O(N)`
<br>

**Java:**
```java
    public String generateTheString(int n) {
        return "b" + "ab".substring(n % 2, 1 + n % 2).repeat(n - 1);
    }
```

**C++:**
```cpp
    string generateTheString(int n) {
        return "b" + string(n - 1, \'a\' + n % 2);
    }
```

**Python:**
```py
    def generateTheString(self, n):
        return \'b\' + \'ab\'[n & 1] * (n - 1)
```

</p>


### Java 100% speed and 100% memory
- Author: Riyafa
- Creation Date: Sat Mar 14 2020 20:41:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 20:56:59 GMT+0800 (Singapore Standard Time)

<p>
Use a character array

```
public String generateTheString(int n) {
    char[] str = new char[n];
    Arrays.fill(str, \'a\');
    if(n%2==0) {
        str[0] = \'b\';
    }
    return new String(str);
}

```

I have created a video explaining the solution 
https://www.youtube.com/watch?v=x61_G5le9jM
</p>


### [C++] Simple Construction / One-Line
- Author: orangezeit
- Creation Date: Sun Mar 08 2020 12:01:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 12:06:19 GMT+0800 (Singapore Standard Time)

<p>
```cpp
class Solution {
public:
    string generateTheString(int n) {
        return n % 2 ? string(n, \'a\') : string(n - 1, \'a\') + \'b\';
    }
};
```
</p>


