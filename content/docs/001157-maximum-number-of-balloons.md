---
title: "Maximum Number of Balloons"
weight: 1157
#id: "maximum-number-of-balloons"
---
## Description
<div class="description">
<p>Given a string&nbsp;<code>text</code>, you want to use the characters of&nbsp;<code>text</code>&nbsp;to form as many instances of the word <strong>&quot;balloon&quot;</strong> as possible.</p>

<p>You can use each character in <code>text</code> <strong>at most once</strong>. Return the maximum number of instances that can be formed.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/09/05/1536_ex1_upd.JPG" style="width: 132px; height: 35px;" /></strong></p>

<pre>
<strong>Input:</strong> text = &quot;nlaebolko&quot;
<strong>Output:</strong> 1
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/09/05/1536_ex2_upd.JPG" style="width: 267px; height: 35px;" /></strong></p>

<pre>
<strong>Input:</strong> text = &quot;loonbalxballpoon&quot;
<strong>Output:</strong> 2
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;leetcode&quot;
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= text.length &lt;= 10^4</code></li>
	<li><code>text</code>&nbsp;consists of lower case English letters only.</li>
</ul>
</div>

## Tags
- Hash Table (hash-table)
- String (string)

## Companies
- Tesla - 8 (taggedByAdmin: false)
- Wayfair - 4 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### #WithComments #StraightForward Java Simple count of chars
- Author: JatinYadav96
- Creation Date: Sun Sep 15 2019 12:12:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 15 2019 12:12:57 GMT+0800 (Singapore Standard Time)

<p>
```
public int maxNumberOfBalloons(String text) {
        int[] chars = new int[26]; //count all letters
        for (char c : text.toCharArray()) {
            chars[c - \'a\']++;
        }
        int min = chars[1];//for b
        min = Math.min(min, chars[0]);//for a
        min = Math.min(min, chars[11] / 2);// for l /2 
        min = Math.min(min, chars[14] / 2);//similarly for o/2
        min = Math.min(min, chars[13]);//for n
        return min;        
    }
```
Coundn\'t think of maps or any other DS :(
But My space complexity is O(1) :P
Hope you will understand :)

</p>


### [Java/Python 3] Count solution w/ analysis.
- Author: rock
- Creation Date: Sun Sep 15 2019 12:11:03 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 18 2019 00:05:53 GMT+0800 (Singapore Standard Time)

<p>
Count chars of both `text` and `balloon` and find the minimum of the times of the count of chars of `text / balloon`.

```
    public int maxNumberOfBalloons(String text) {
        int[] cnt = new int[26], cntBaloon = new int[26];
        for (int i = 0; i < text.length(); ++i)
            ++cnt[text.charAt(i) - \'a\'];
        int min = text.length();
        for (char c : "balloon".toCharArray())
            ++cntBaloon[c - \'a\'];
        for (char c : "balloon".toCharArray())
            min = Math.min(min, cnt[c - \'a\'] / cntBaloon[c - \'a\']);
        return min;
    }
```
```
    def maxNumberOfBalloons(self, text: str) -> int:
        cnt = collections.Counter(text)
        cntBalloon = collections.Counter(\'balloon\')
        return min([cnt[c] // cntBalloon[c] for c in cntBalloon])
```
**Analysis:**

Time: O(text.length()), space: O(1).
</p>


### C++ 3 line solution, using unordered_map. 4ms 9mb
- Author: scaa
- Creation Date: Wed Oct 02 2019 08:10:09 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 30 2020 07:18:08 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    int maxNumberOfBalloons(string text) {
        unordered_map<char, int> mm;
        for (char i : text) mm[i] += 1;
        return min(mm[\'b\'], min(mm[\'a\'], min(mm[\'l\']/2, min(mm[\'o\']/2, mm[\'n\']))));
    }
};
```

---
# **Suggestions listed below**
---

Suggestion made by [muhammetzahitaydin](https://leetcode.com/muhammetzahitaydin/): you can use one min for return:
```
return min ( { mm[\'b\'] , mm[\'a\'] , mm[\'l\']/2 , mm[\'o\']/2 ,mm[\'n\'] } ) ;
```

Suggestion made by [Body_sweep69](https://leetcode.com/body_sweep69/) to use array of size 256 instead of unordered map to reduce memory usage. [BSalwiczek](https://leetcode.com/bsalwiczek/) suggested using array of size 5 since we only care about 5 chars (b, a, l, o, n).

---

*suggestions added on June 29th 2020
</p>


