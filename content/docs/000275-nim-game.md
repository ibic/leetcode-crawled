---
title: "Nim Game"
weight: 275
#id: "nim-game"
---
## Description
<div class="description">
<p>You are playing the following Nim Game with your friend:</p>

<ul>
	<li>Initially, there is a heap of stones on the table.</li>
	<li>You and your friend will alternate taking turns, and <strong>you go first</strong>.</li>
	<li>On each turn, the person whose turn it is will remove 1 to 3 stones from the heap.</li>
	<li>The one who removes the last stone is the winner.</li>
</ul>

<p>Given <code>n</code>, the number of stones in the heap, return <code>true</code><em> if you can win the game assuming both you and your friend play optimally, otherwise return </em><code>false</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 4
<strong>Output:</strong> false
<strong>Explanation:</strong> These are the possible outcomes:
1. You remove 1 stone. Your friend removes 3 stones, including the last stone. Your friend wins.
2. You remove 2 stones. Your friend removes 2 stones, including the last stone. Your friend wins.
3. You remove 3 stones. Your friend removes the last stone. Your friend wins.
In all outcomes, your friend wins.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> true
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 2
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 2<sup>31</sup> - 1</code></li>
</ul>

</div>

## Tags
- Brainteaser (brainteaser)
- Minimax (minimax)

## Companies
- Bloomberg - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Adobe - 0 (taggedByAdmin: true)

## Official Solution
## Solution

You can *always* win a Nim game if the number of stones $$n$$ in the pile is not divisible by $$4$$.

**Reasoning**

Let us think of the small cases. It is clear that if there are only one, two, or three stones in the pile, and it is your turn, you can win the game by taking all of them. Like the problem description says, if there are exactly four stones in the pile, you will lose. Because no matter how many you take, you will leave some stones behind for your opponent to take and win the game. So in order to win, you have to ensure that you never reach the situation where there are exactly four stones on the pile on your turn.

Similarly, if there are five, six, or seven stones you can win by taking just enough to leave four stones for your opponent so that they lose. But if there are eight stones on the pile, you will inevitably lose, because regardless whether you pick one, two or three stones from the pile, your opponent can pick three, two or one stone to ensure that, again, four stones will be left to you on your turn.

It is obvious that the same pattern repeats itself for $$n=4,8,12,16,\dots$$, basically all multiples of $$4$$.


**Java**


```java
public boolean canWinNim(int n) {
    return (n % 4 != 0);
}
```

**Complexity Analysis**

Time complexity is $$O(1)$$, since only one check is performed. No additional space is used, so space complexity is also $$O(1)$$.

**References**

[Lecture on Nim Games](https://www.cs.umd.edu/~gordon/ysp/nim.pdf) from University of Maryland: MATH 199: Math, Game Theory and the Theory of Games, Summer 2006.

Analysis written by: @noran

## Accepted Submission (python3)
```python3
class Solution:
    def canWinNim(self, n: int) -> bool:
        return n % 4 != 0
```

## Top Discussions
### Theorem: all 4s shall be false
- Author: liaison
- Creation Date: Tue Oct 13 2015 05:43:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 21:40:43 GMT+0800 (Singapore Standard Time)

<p>
> Theorem: The first one who got the number that is multiple of 4 (i.e. n % 4 == 0) will lost, otherwise he/she will win.

Proof: 

 1. the base case: when `n = 4`, as suggested by the hint from the problem, no matter which number that that first player, the second player would always be able to pick the remaining number.
 
 2. For `1* 4 < n < 2 * 4, (n = 5, 6, 7)`, the first player can reduce the initial number into 4 accordingly, which will leave the death number 4 to the second player. i.e. The numbers 5, 6, 7 are winning numbers for any player who got it first. 

 3. Now to the beginning of the next cycle, `n = 8`, no matter which number that the first player picks, it would always leave the winning numbers (5, 6, 7) to the second player. Therefore, 8 % 4 == 0, again is a death number.

 4. Following the second case, for numbers between (2\*4 = 8) and (3\*4=12), which are `9, 10, 11`, are winning numbers for the first player again, because the first player can always reduce the number into the death number 8.


Following the above theorem and proof, the solution could not be simpler: 

    public boolean canWinNim(int n) {    
        return n % 4 != 0 ;
    }
</p>


### One line O(1) solution and explanation
- Author: kennethliaoke
- Creation Date: Wed Oct 14 2015 16:05:03 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 04 2018 13:54:12 GMT+0800 (Singapore Standard Time)

<p>
suppose there are x stones left for first player (A), he can take 1,2,3 stones away, so second player B will have three cases to deal with (x -1), (x-2), (x-3). after he pick the stones, there will be 9 cases left for A.

    B (x-1) -> A: (x-2), (x-3), (x-4)
    B (x-2) -> A: (x-3), (x-4), (x-5)
    B (x-3) -> A: (x-4), (x-5), (x-6)

Now, if A can guarantee he win at either of three groups, then he can force B to into that one of the three states and A can end up in that particular group after B's move. 

    f(x) = (f(x-2)&&f(x-3)&&f(x-4)) || (f(x-3)&&f(x-4)&&f(x-5)) || (f(x-4)&&f(x-5)&&f(x-6))

if we examine the equation a little closer, we can find f(x - 4) is a critical point, if f(x-4) is false, then f(x) will be always false.

we can also find out the initial conditions, f(1), f(2), f(3) will be true (A always win), and f(4) will be false. so
based on previous equation and initial conditions f(5) = f(6) = f(7) = true, f(8) = false;
obviously, f(1), f(2), f(3) can make all f(4n + 1), f(4n + 2), f(4n + 3) to be true, only f(4n) will be false then.
so here we go our one line solution:

return (n % 4 != 0);
</p>


### O(1) Efficient Single-line Java using Bit Checking
- Author: oluwasayo
- Creation Date: Thu Jan 14 2016 05:53:41 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 14 2016 05:53:41 GMT+0800 (Singapore Standard Time)

<p>
If the two least significant bits of `n` are zeros, you will never win.
Why? Because **whoever is dealt a hand of 4 will never win**. Since your opponent is also very smart, she can reduce any multiple of `4` to `4` for you and you'll never win. Instead of using modulo or division, you can verify if a number is a multiple of `4` by checking its two least significant bits.

    public boolean canWinNim(int n) {
      return (n & 0b11) != 0;
    }
</p>


