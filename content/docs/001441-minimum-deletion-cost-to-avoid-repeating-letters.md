---
title: "Minimum Deletion Cost to Avoid Repeating Letters"
weight: 1441
#id: "minimum-deletion-cost-to-avoid-repeating-letters"
---
## Description
<div class="description">
<p>Given a&nbsp;string <code>s</code> and an array of integers <code>cost</code> where <code>cost[i]</code>&nbsp;is the cost of deleting the <code>i<sup>th</sup></code> character in <code>s</code>.</p>

<p>Return the minimum cost of deletions such that there are no two identical letters next to each other.</p>

<p>Notice that you will delete the chosen characters at the same time, in other words, after deleting a character, the costs of deleting other characters will not change.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abaac&quot;, cost = [1,2,3,4,5]
<strong>Output:</strong> 3
<strong>Explanation:</strong> Delete the letter &quot;a&quot; with cost 3 to get &quot;abac&quot; (String without two identical letters next to each other).
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abc&quot;, cost = [1,2,3]
<strong>Output:</strong> 0
<strong>Explanation:</strong> You don&#39;t need to delete any character because there are no identical letters next to each other.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aabaa&quot;, cost = [1,2,3,4,1]
<strong>Output:</strong> 2
<strong>Explanation:</strong> Delete the first and the last character, getting the string (&quot;aba&quot;).
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>s.length == cost.length</code></li>
	<li><code>1 &lt;= s.length, cost.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= cost[i] &lt;=&nbsp;10^4</code></li>
	<li><code>s</code> contains only lowercase English letters.</li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Sep 06 2020 12:07:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 20:42:01 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
For a group of continuous same characters,
we need to delete all the group but leave only one character.
<br>

# **Explanation**
For each group of continuous same characters,
we need cost = sum_cost(group)  - max_cost(group)
<br>

# **Complexity**
Time `O(N)`
Space `O(1)`

<br>

# Solution 1:
**Java:**
```java
    public int minCost(String s, int[] cost) {
        int res = 0, max_cost = 0, sum_cost = 0, n = s.length();
        for (int i = 0; i < n; ++i) {
            if (i > 0 && s.charAt(i) != s.charAt(i - 1)) {
                res += sum_cost - max_cost;
                sum_cost = max_cost = 0;
            }
            sum_cost += cost[i];
            max_cost = Math.max(max_cost, cost[i]);
        }
        res += sum_cost - max_cost;
        return res;
    }
```

**C++:**
```cpp
    int minCost(string s, vector<int>& cost) {
        int res = 0, max_cost = 0, sum_cost = 0, n = s.size();
        for (int i = 0; i < n; ++i) {
            if (i > 0 && s[i] != s[i - 1]) {
                res += sum_cost - max_cost;
                sum_cost = max_cost = 0;
            }
            sum_cost += cost[i];
            max_cost = max(max_cost, cost[i]);
        }
        res += sum_cost - max_cost;
        return res;
    }
```
<br>

# Solution 2: Same idea, just shorter
Inspired by @avarughe,
Hold the character with the biggest cost in hand.

**C++:**
```cpp
    int minCost(string s, vector<int>& cost) {
        int res = 0, max_cost = 0, n = s.size();
        for (int i = 0; i < n; ++i) {
            if (i > 0 && s[i] != s[i - 1])
                max_cost = 0;
            res += min(max_cost, cost[i]);
            max_cost = max(max_cost, cost[i]);
        }
        return res;
    }
```
**Python:**
```py
    def minCost(self, s, cost):
        res = max_cost = 0
        for i in xrange(len(s)):
            if i > 0 and s[i] != s[i - 1]:
                max_cost = 0
            res += min(max_cost, cost[i])
            max_cost = max(max_cost, cost[i])
        return res
```

</p>


### C++/Java O(n)
- Author: votrubac
- Creation Date: Sun Sep 06 2020 12:01:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 18:52:45 GMT+0800 (Singapore Standard Time)

<p>
This one threw me off... I did not expect third problem to be easier than the first one. So I tryied DP, got TLE, and then got my "you should read the problem description carefully" moment.

So, the realization was - if we have repeated characters, we need to remove all of them except one - the "most expensive" character to remove.

**Algorithm**
Go through the string, accumulating cost in `res` and tracking `max_cost`. If the current character is different than previous one, we would subtract `max_cost` and set it to zero.

For a non-repeating characters, that means that the cost is zero (as we keep the most expensive single character).

For repeating characters, we would add cost to remove all of them except the most expensive one.

**C++**
```cpp
int minCost(string s, vector<int>& cost) {
    int res = cost[0], max_cost = cost[0];
    for (int i = 1; i < s.size(); ++i) {
        if (s[i] != s[i - 1]) {
            res -= max_cost;
            max_cost = 0;
        }
        res += cost[i];
        max_cost = max(max_cost, cost[i]);
    }
    return res - max_cost;
}
```

**Java**
```java
public int minCost(String s, int[] cost) {
    int res = cost[0], max_cost = cost[0];
    for (int i = 1; i < s.length(); ++i) {
        if (s.charAt(i) != s.charAt(i - 1)) {
            res -= max_cost;
            max_cost = 0;
        }
        res += cost[i];
        max_cost = Math.max(max_cost, cost[i]);
    }
    return res - max_cost;
}
```
</p>


### [Python3] greedy
- Author: ye15
- Creation Date: Sun Sep 06 2020 12:01:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 12:01:51 GMT+0800 (Singapore Standard Time)

<p>

```
class Solution:
    def minCost(self, s: str, cost: List[int]) -> int:
        ans = prev = 0 # index of previously retained letter 
        for i in range(1, len(s)): 
            if s[prev] != s[i]: prev = i
            else: 
                ans += min(cost[prev], cost[i])
                if cost[prev] < cost[i]: prev = i
        return ans 
```
</p>


