---
title: "Valid Sudoku"
weight: 36
#id: "valid-sudoku"
---
## Description
<div class="description">
<p>Determine if a&nbsp;<code>9 x 9</code> Sudoku board&nbsp;is valid.&nbsp;Only the filled cells need to be validated&nbsp;<strong>according to the following rules</strong>:</p>

<ol>
	<li>Each row&nbsp;must contain the&nbsp;digits&nbsp;<code>1-9</code> without repetition.</li>
	<li>Each column must contain the digits&nbsp;<code>1-9</code>&nbsp;without repetition.</li>
	<li>Each of the nine&nbsp;<code>3 x 3</code> sub-boxes of the grid must contain the digits&nbsp;<code>1-9</code>&nbsp;without repetition.</li>
</ol>

<p><strong>Note:</strong></p>

<ul>
	<li>A Sudoku board (partially filled) could be valid but is not necessarily solvable.</li>
	<li>Only the filled cells need to be validated according to the mentioned&nbsp;rules.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Sudoku-by-L2G-20050714.svg/250px-Sudoku-by-L2G-20050714.svg.png" style="height:250px; width:250px" />
<pre>
<strong>Input:</strong> board = 
[[&quot;5&quot;,&quot;3&quot;,&quot;.&quot;,&quot;.&quot;,&quot;7&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;]
,[&quot;6&quot;,&quot;.&quot;,&quot;.&quot;,&quot;1&quot;,&quot;9&quot;,&quot;5&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;]
,[&quot;.&quot;,&quot;9&quot;,&quot;8&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;6&quot;,&quot;.&quot;]
,[&quot;8&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;6&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;3&quot;]
,[&quot;4&quot;,&quot;.&quot;,&quot;.&quot;,&quot;8&quot;,&quot;.&quot;,&quot;3&quot;,&quot;.&quot;,&quot;.&quot;,&quot;1&quot;]
,[&quot;7&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;2&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;6&quot;]
,[&quot;.&quot;,&quot;6&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;2&quot;,&quot;8&quot;,&quot;.&quot;]
,[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;4&quot;,&quot;1&quot;,&quot;9&quot;,&quot;.&quot;,&quot;.&quot;,&quot;5&quot;]
,[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;8&quot;,&quot;.&quot;,&quot;.&quot;,&quot;7&quot;,&quot;9&quot;]]
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> board = 
[[&quot;8&quot;,&quot;3&quot;,&quot;.&quot;,&quot;.&quot;,&quot;7&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;]
,[&quot;6&quot;,&quot;.&quot;,&quot;.&quot;,&quot;1&quot;,&quot;9&quot;,&quot;5&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;]
,[&quot;.&quot;,&quot;9&quot;,&quot;8&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;6&quot;,&quot;.&quot;]
,[&quot;8&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;6&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;3&quot;]
,[&quot;4&quot;,&quot;.&quot;,&quot;.&quot;,&quot;8&quot;,&quot;.&quot;,&quot;3&quot;,&quot;.&quot;,&quot;.&quot;,&quot;1&quot;]
,[&quot;7&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;2&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;6&quot;]
,[&quot;.&quot;,&quot;6&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;2&quot;,&quot;8&quot;,&quot;.&quot;]
,[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;4&quot;,&quot;1&quot;,&quot;9&quot;,&quot;.&quot;,&quot;.&quot;,&quot;5&quot;]
,[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;8&quot;,&quot;.&quot;,&quot;.&quot;,&quot;7&quot;,&quot;9&quot;]]
<strong>Output:</strong> false
<strong>Explanation:</strong> Same as Example 1, except with the <strong>5</strong> in the top left corner being modified to <strong>8</strong>. Since there are two 8&#39;s in the top left 3x3 sub-box, it is invalid.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>board.length == 9</code></li>
	<li><code>board[i].length == 9</code></li>
	<li><code>board[i][j]</code> is a digit or <code>&#39;.&#39;</code>.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Apple - 8 (taggedByAdmin: true)
- Amazon - 8 (taggedByAdmin: false)
- Roblox - 5 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- DoorDash - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Cruise Automation - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Snapchat - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Intuition

The naive solution would be to iterate _three_ times over the board to ensure that :

* There is no rows with duplicates.
* There is no columns with duplicates.
* There is no sub-boxes with duplicates.

Actually, all this could be done in just one iteration. 

---
   
#### Approach 1: One iteration

Let's first discuss two questions.

* How to enumerate sub-boxes?

> One could use `box_index = (row / 3) * 3 + col / 3` where
`/` is an integer division, `row` is a row number, and `col`
is a column number.

<img src="../Figures/36/36_boxes_2.png" width="500">

* How to ensure that there is no duplicates in a row / column / box?

> One could just track all values which were already encountered
in a hash map `value -> count`.

Now everything is ready for the overall algorithm :

* Move along the board. 
   * Check for each cell value if it was seen already in the current
   row / column / box :
      * Return `false` if yes.
      * Keep this value for a further tracking if no. 
* Return `true`.

!?!../Documents/36_LIS.json:1000,616!?!

<iframe src="https://leetcode.com/playground/SzBDpwc9/shared" frameBorder="0" width="100%" height="500" name="SzBDpwc9"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$ since all we do here is just one 
iteration over the board with `81` cells. 
* Space complexity : $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short+Simple Java using Strings
- Author: StefanPochmann
- Creation Date: Mon Oct 19 2015 01:53:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 12:01:45 GMT+0800 (Singapore Standard Time)

<p>
Collect the set of things we see, encoded as strings. For example:

- `'4' in row 7` is encoded as `"(4)7"`.
- `'4' in column 7` is encoded as `"7(4)"`.
- `'4' in the top-right block` is encoded as `"0(4)2"`.

Scream `false` if we ever fail to add something because it was already added (i.e., seen before).

    public boolean isValidSudoku(char[][] board) {
        Set seen = new HashSet();
        for (int i=0; i<9; ++i) {
            for (int j=0; j<9; ++j) {
                if (board[i][j] != '.') {
                    String b = "(" + board[i][j] + ")";
                    if (!seen.add(b + i) || !seen.add(j + b) || !seen.add(i/3 + b + j/3))
                        return false;
                }
            }
        }
        return true;
    }

---

**Edit:** Just occurred to me that we can also make it really clear and self-explaining. I'm loving it.

    public boolean isValidSudoku(char[][] board) {
        Set seen = new HashSet();
        for (int i=0; i<9; ++i) {
            for (int j=0; j<9; ++j) {
                char number = board[i][j];
                if (number != '.')
                    if (!seen.add(number + " in row " + i) ||
                        !seen.add(number + " in column " + j) ||
                        !seen.add(number + " in block " + i/3 + "-" + j/3))
                        return false;
            }
        }
        return true;
    }
</p>


### My short solution by C++. O(n2)
- Author: makuiyu
- Creation Date: Sun Feb 01 2015 13:29:05 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 11:53:47 GMT+0800 (Singapore Standard Time)

<p>
Three flags are used to check whether a number appear.

used1: check each row

used2: check each column

used3: check each sub-boxes
 
    class Solution
    {
    public:
        bool isValidSudoku(vector<vector<char> > &board)
        {
            int used1[9][9] = {0}, used2[9][9] = {0}, used3[9][9] = {0};
            
            for(int i = 0; i < board.size(); ++ i)
                for(int j = 0; j < board[i].size(); ++ j)
                    if(board[i][j] != '.')
                    {
                        int num = board[i][j] - '0' - 1, k = i / 3 * 3 + j / 3;
                        if(used1[i][num] || used2[j][num] || used3[k][num])
                            return false;
                        used1[i][num] = used2[j][num] = used3[k][num] = 1;
                    }
            
            return true;
        }
    };
</p>


### Shared my concise Java code
- Author: Lorraine921
- Creation Date: Sat Mar 07 2015 05:29:55 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 19:35:09 GMT+0800 (Singapore Standard Time)

<p>
    public boolean isValidSudoku(char[][] board) {
        for(int i = 0; i<9; i++){
            HashSet<Character> rows = new HashSet<Character>();
            HashSet<Character> columns = new HashSet<Character>();
            HashSet<Character> cube = new HashSet<Character>();
            for (int j = 0; j < 9;j++){
                if(board[i][j]!='.' && !rows.add(board[i][j]))
                    return false;
                if(board[j][i]!='.' && !columns.add(board[j][i]))
                    return false;
                int RowIndex = 3*(i/3);
                int ColIndex = 3*(i%3);
                if(board[RowIndex + j/3][ColIndex + j%3]!='.' && !cube.add(board[RowIndex + j/3][ColIndex + j%3]))
                    return false;
            }
        }
        return true;
    }
</p>


