---
title: "Basic Calculator IV"
weight: 702
#id: "basic-calculator-iv"
---
## Description
<div class="description">
<p>Given an <code>expression</code>&nbsp;such as <code>expression = &quot;e + 8 - a + 5&quot;</code> and an evaluation map such as <code>{&quot;e&quot;: 1}</code> (given in terms of <code>evalvars = [&quot;e&quot;]</code> and <code>evalints = [1]</code>), return a list of tokens representing the simplified expression, such as <code>[&quot;-1*a&quot;,&quot;14&quot;]</code></p>

<ul>
	<li>An expression alternates chunks and symbols, with a space separating each chunk and symbol.</li>
	<li>A chunk is either an expression in parentheses, a variable, or a non-negative integer.</li>
	<li>A variable is a string of lowercase letters (not including digits.) Note that variables can be multiple letters, and note that variables never have a leading coefficient or unary operator like <code>&quot;2x&quot;</code> or <code>&quot;-x&quot;</code>.</li>
</ul>

<p>Expressions are evaluated in the usual order: brackets first, then multiplication, then addition and subtraction. For example, <code>expression = &quot;1 + 2 * 3&quot;</code> has an answer of <code>[&quot;7&quot;]</code>.</p>

<p>The format of the output is as follows:</p>

<ul>
	<li>For each term of free variables with non-zero coefficient, we write the free variables within a term in sorted order lexicographically. For example, we would never write a term like <code>&quot;b*a*c&quot;</code>, only <code>&quot;a*b*c&quot;</code>.</li>
	<li>Terms have degree equal to the number of free variables being multiplied, counting multiplicity. (For example, <code>&quot;a*a*b*c&quot;</code> has degree 4.) We write the largest degree terms of our answer first, breaking ties by lexicographic order ignoring the leading coefficient of the term.</li>
	<li>The leading coefficient of the term is placed directly to the left with an asterisk separating it from the variables (if they exist.)&nbsp; A leading coefficient of 1 is still printed.</li>
	<li>An example of a well formatted answer is <code>[&quot;-2*a*a*a&quot;, &quot;3*a*a*b&quot;, &quot;3*b*b&quot;, &quot;4*a&quot;, &quot;5*c&quot;, &quot;-6&quot;]</code>&nbsp;</li>
	<li>Terms (including constant terms) with coefficient 0 are not included.&nbsp; For example, an expression of &quot;0&quot; has an output of [].</li>
</ul>

<p><strong>Examples:</strong></p>

<pre>
<strong>Input:</strong> expression = &quot;e + 8 - a + 5&quot;, evalvars = [&quot;e&quot;], evalints = [1]
<strong>Output:</strong> [&quot;-1*a&quot;,&quot;14&quot;]

<strong>Input:</strong> expression = &quot;e - 8 + temperature - pressure&quot;,
evalvars = [&quot;e&quot;, &quot;temperature&quot;], evalints = [1, 12]
<strong>Output:</strong> [&quot;-1*pressure&quot;,&quot;5&quot;]

<strong>Input:</strong> expression = &quot;(e + 8) * (e - 8)&quot;, evalvars = [], evalints = []
<strong>Output:</strong> [&quot;1*e*e&quot;,&quot;-64&quot;]

<strong>Input:</strong> expression = &quot;7 - 7&quot;, evalvars = [], evalints = []
<strong>Output:</strong> []

<strong>Input:</strong> expression = &quot;a * b * c + b * a * c * 4&quot;, evalvars = [], evalints = []
<strong>Output:</strong> [&quot;5*a*b*c&quot;]

<strong>Input:</strong> expression = &quot;((a - b) * (b - c) + (c - a)) * ((a - b) + (b - c) * (c - a))&quot;,
evalvars = [], evalints = []
<strong>Output:</strong> [&quot;-1*a*a*b*b&quot;,&quot;2*a*a*b*c&quot;,&quot;-1*a*a*c*c&quot;,&quot;1*a*b*b*b&quot;,&quot;-1*a*b*b*c&quot;,&quot;-1*a*b*c*c&quot;,&quot;1*a*c*c*c&quot;,&quot;-1*b*b*b*c&quot;,&quot;2*b*b*c*c&quot;,&quot;-1*b*c*c*c&quot;,&quot;2*a*a*b&quot;,&quot;-2*a*a*c&quot;,&quot;-2*a*b*b&quot;,&quot;2*a*c*c&quot;,&quot;1*b*b*b&quot;,&quot;-1*b*b*c&quot;,&quot;1*b*c*c&quot;,&quot;-1*c*c*c&quot;,&quot;-1*a*a&quot;,&quot;1*a*b&quot;,&quot;1*a*c&quot;,&quot;-1*b*c&quot;]
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li><code>expression</code> will have length in range <code>[1, 250]</code>.</li>
	<li><code>evalvars, evalints</code> will have equal lengths in range <code>[0, 100]</code>.</li>
</ol>

</div>

## Tags
- Hash Table (hash-table)
- String (string)
- Stack (stack)

## Companies
- Roblox - 4 (taggedByAdmin: false)
- Indeed - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Intuit - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Polynomial Class [Accepted]

**Intuition**

Keep a class `Poly` that knows a map from a list of free variables to a coefficient, and support operations on this class.

**Algorithm**

Each function is straightforward individually.  Let's list the functions we use:

* `Poly:add(this, that)` returns the result of `this + that`.
* `Poly:sub(this, that)` returns the result of `this - that`.
* `Poly:mul(this, that)` returns the result of `this * that`.
* `Poly:evaluate(this, evalmap)` returns the polynomial after replacing all free variables with constants as specified by `evalmap`.
* `Poly:toList(this)` returns the polynomial in the correct output format.

* `Solution::combine(left, right, symbol)` returns the result of applying the binary operator represented by `symbol` to `left` and `right`.
* `Solution::make(expr)` makes a new `Poly` represented by either the constant or free variable specified by `expr`.
* `Solution::parse(expr)` parses an expression into a new `Poly`.

<iframe src="https://leetcode.com/playground/LUTpxxXx/shared" frameBorder="0" width="100%" height="500" name="LUTpxxXx"></iframe>

**Complexity Analysis**

* Time Complexity:  Let $$N$$ is the length of `expression` and $$M$$ is the length of `evalvars` and `evalints`.  With an expression like `(a + b) * (c + d) * (e + f) * ...` the complexity is $$O(2^N + M)$$.

* Space Complexity: $$O(N + M)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### If any interviewers dare ask me this problem....
- Author: yuxiong
- Creation Date: Wed Apr 17 2019 07:59:01 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Apr 17 2019 07:59:01 GMT+0800 (Singapore Standard Time)

<p>
If any interviewer dare ask me this problem, I will ask him how long I\'m expected solve this problem.
Then I will politely make a bet: If he can solve this problem bug free within the time he gave me, I will never apply that company ever. If he can\'t, I will ask him why the huck did you ask a problem you can\'t solve correctly?
Unless the interviewer is StefanPochmann or lee215...
</p>


### Easy :-P
- Author: StefanPochmann
- Creation Date: Mon Jan 22 2018 05:13:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 22:28:22 GMT+0800 (Singapore Standard Time)

<p>
    def basicCalculatorIV(self, expression, evalvars, evalints):
        class C(collections.Counter):
            def __add__(self, other):
                self.update(other)
                return self
            def __sub__(self, other):
                self.subtract(other)
                return self
            def __mul__(self, other):
                product = C()
                for x in self:
                    for y in other:
                        xy = tuple(sorted(x + y))
                        product[xy] += self[x] * other[y]
                return product
        vals = dict(zip(evalvars, evalints))
        def f(s):
            s = str(vals.get(s, s))
            return C({(s,): 1}) if s.isalpha() else C({(): int(s)})
        c = eval(re.sub('(\w+)', r'f("\1")', expression))
        return ['*'.join((str(c[x]),) + x)
                for x in sorted(c, key=lambda x: (-len(x), x))
                if c[x]]

I let `eval` and `collections.Counter` do most of the work. First I wrap every variable and number in the given expression in a call to `f`. For example the expression `e + 8 - a + 5` becomes `f("e") + f("8") - f("a") + f("5")`. Then when I `eval` that, my function `f` converts its argument to a `C` object, which is a subclass of `Counter`.

A term like `42*a*a*b` is represented by `C({('a', 'a', 'b'): 42})`. That is, the key is the variables as sorted tuple, and the value is the coefficient. So `f` converts free variables to `C({('x',): 1})` (where `x` is the variable name) and converts known variables or numbers to `C({(): x})` (where `x` is the number).

Counters already know how to add and subtract each other, but I had to teach them multiplication. And in the end I need to turn the resulting `C` object into the desired output format.
</p>


### Java solution, using stack
- Author: KakaHiguain
- Creation Date: Mon Jan 22 2018 02:40:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 15 2018 16:37:11 GMT+0800 (Singapore Standard Time)

<p>
use stack to implement the arithmetic operation
We can give each operator a priority number, + and - is 0, while * is 1.
When an operator is inside "()", its priority increases by 2 for every "()".

For example:
```
4*(a-(b+(x*y))+d*e)
```
the operator priorities are:
```
* - + * + *
1 2 4 7 2 3
```
always do the operation with highest priority first.

class Term is for each single term in our final answer.
class Expression is a list of class Terms.
We can do +-* for two Expressions.

```
class Solution {
    Map<String,Integer> map=new HashMap<>(); //evaluation map
    class Term
    {
        int para=1; //the parameter of this term
        List<String> var=new ArrayList<>(); //each factor (e.a. a*b*b*c->{a,b,b,c})
        @Override
        public String toString()
        {
            if (para==0) return "";
            String ans="";
            for (String s:var) ans+="*"+s;
            return para+ans;
        }
        boolean equals(Term that)
        {
            if (this.var.size()!=that.var.size()) return false;
            for (int i=0;i<this.var.size();i++)
                if (!this.var.get(i).equals(that.var.get(i))) return false;
            return true;
        }
        int compareTo(Term that)
        {
            if (this.var.size()>that.var.size()) return -1;
            if (this.var.size()<that.var.size()) return 1;
            for (int i=0;i<this.var.size();i++)
            {
                int x=this.var.get(i).compareTo(that.var.get(i));
                if (x!=0) return x;
            }
            return 0;
        }
        Term times(Term that)
        {
            Term pro=new Term(this.para*that.para);
            for (String s:this.var) pro.var.add(new String(s));
            for (String s:that.var) pro.var.add(new String(s));
            Collections.sort(pro.var);
            return pro;
        }
        Term(int x) { para=x; }
        Term(String s) { if (map.containsKey(s)) para=map.get(s); else var.add(s); }
        Term(Term that) { this.para=that.para; this.var=new ArrayList<>(that.var); }
    }
    class Expression 
    {
        List<Term> list=new ArrayList<>(); //Term List
        char oper='+'; // Arithmetic symbol
        Expression(int x) { list.add(new Term(x)); }
        Expression(String s) { list.add(new Term(s)); }
        Expression(List<Term> l) { list=l; }
        Expression times(Expression that)
        {
            List<Term> c=new ArrayList<>();
            for (Term t1:this.list)
                for (Term t2:that.list)
                    c.add(t1.times(t2));
            c=combine(c);
            return new Expression(c);
        }
        Expression plus(Expression that,int sgn)
        {
            List<Term> c=new ArrayList<>();
            for (Term t:this.list) c.add(new Term(t));
            for (Term t:that.list) 
            {
                Term t2=new Term(t);
                t2.para=t2.para*sgn;
                c.add(t2);
            }
            c=combine(c);
            return new Expression(c);
        }
        Expression cal(Expression that)
        {
            if (oper=='+') return plus(that,1);
            if (oper=='-') return plus(that,-1);
            return times(that);
        }
        List<String> toList()
        {
            List<String> ans=new ArrayList<>();
            for (Term t:list) 
            {
                String s=t.toString();
                if (s.length()>0) ans.add(s);
            }
            return ans;
        }
    }  
    List<Term> combine(List<Term> a) //combine the similar terms
    {
        Collections.sort(a,(t1,t2)->(t1.compareTo(t2))); //sort all terms to make similar terms together
        List<Term> c=new ArrayList<>();
        for (Term t:a)
        {
            if (c.size()!=0 && t.equals(c.get(c.size()-1))) c.get(c.size()-1).para+=t.para;
            else c.add(new Term(t));
        }
        return c;
    }
    public List<String> basicCalculatorIV(String expression, String[] evalvars, int[] evalints) {
        for (int i=0;i<evalvars.length;i++) map.put(evalvars[i],evalints[i]);
        int i=0,l=expression.length();
        Stack<Expression> stack=new Stack<>();
        Stack<Integer> priStack=new Stack<>();
        Expression zero=new Expression(0);
        stack.push(zero);
        priStack.push(0);
        int pri=0;
        while (i<l)
        {
            char ch=expression.charAt(i);
            if (Character.isDigit(ch))
            {
                int num=0;
                while (i<l && Character.isDigit(expression.charAt(i)))
                {
                    num=num*10+(expression.charAt(i)-48);
                    i++;
                }
                stack.add(new Expression(num));
                continue;
            }
            if (Character.isLetter(ch))
            {
                String s="";
                while (i<l && Character.isLetter(expression.charAt(i)))
                {
                    s+=expression.charAt(i);
                    i++;
                }
                stack.add(new Expression(s));
                continue;
            }
            if (ch=='(') pri+=2;
            if (ch==')') pri-=2;
            if (ch=='+' || ch=='-' || ch=='*')
            {
                int nowPri=pri;
                if (ch=='*') nowPri++;
                while (!priStack.isEmpty() && nowPri<=priStack.peek())
                {
                    Expression now=stack.pop(),last=stack.pop();
                    priStack.pop();
                    stack.push(last.cal(now));
                }
                stack.peek().oper=ch;
                priStack.push(nowPri);
            }
            i++;
        }
        while (stack.size()>1)
        {
            Expression now=stack.pop(),last=stack.pop();
            stack.push(last.cal(now));
        }
        return stack.peek().toList();
    }
}
```
</p>


