---
title: "Investments in 2016"
weight: 1492
#id: "investments-in-2016"
---
## Description
<div class="description">
<p>Write a query to print the sum of all total investment values in 2016 (<b>TIV_2016</b>), to a scale of 2 decimal places, for all policy holders who meet the following criteria:</p>

<ol>
	<li>Have the same <b>TIV_2015</b> value as one or more other policyholders.</li>
	<li>Are not located in the same city as any other policyholder (i.e.: the (latitude, longitude) attribute pairs must be unique).</li>
</ol>

<p><b>Input Format:</b><br />
The <b><i>insurance</i></b> table is described as follows:</p>

<pre>
| Column Name | Type          |
|-------------|---------------|
| PID         | INTEGER(11)   |
| TIV_2015    | NUMERIC(15,2) |
| TIV_2016    | NUMERIC(15,2) |
| LAT         | NUMERIC(5,2)  |
| LON         | NUMERIC(5,2)  |
</pre>

<p>where <b>PID</b> is the policyholder&#39;s policy ID, <b>TIV_2015</b> is the total investment value in 2015, <b>TIV_2016</b> is the total investment value in 2016, <b>LAT</b> is the latitude of the policy holder&#39;s city, and <b>LON</b> is the longitude of the policy holder&#39;s city.</p>

<p><b>Sample Input</b></p>

<pre>
| PID | TIV_2015 | TIV_2016 | LAT | LON |
|-----|----------|----------|-----|-----|
| 1   | 10       | 5        | 10  | 10  |
| 2   | 20       | 20       | 20  | 20  |
| 3   | 10       | 30       | 20  | 20  |
| 4   | 10       | 40       | 40  | 40  |
</pre>

<p><b>Sample Output</b></p>

<pre>
| TIV_2016 |
|----------|
| 45.00    |
</pre>

<p><b>Explanation</b></p>

<pre>
The first record in the table, like the last record, meets both of the two criteria.
The <b>TIV_2015</b> value &#39;10&#39; is as the same as the third and forth record, and its location unique.

The second record does not meet any of the two criteria. Its <b>TIV_2015</b> is not like any other policyholders.

And its location is the same with the third record, which makes the third record fail, too.

So, the result is the sum of <b>TIV_2016</b> of the first and last record, which is 45.</pre>

</div>

## Tags


## Companies
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `GROUP BY` and `COUNT` [Accepted]

**Intuition**

To decide whether a value in a column is unique or not, we can use `GROUP BY` and `COUNT`.

**Algorithm**

Check whether the value of a record's **TIV_2015** is unique, if it is not unique, and at the same time, its location (LAT, LON) pair is unique, then this record meeting the criteria. So it should be counted in the sum.

**MySQL**

```sql
SELECT
    SUM(insurance.TIV_2016) AS TIV_2016
FROM
    insurance
WHERE
    insurance.TIV_2015 IN
    (
      SELECT
        TIV_2015
      FROM
        insurance
      GROUP BY TIV_2015
      HAVING COUNT(*) > 1
    )
    AND CONCAT(LAT, LON) IN
    (
      SELECT
        CONCAT(LAT, LON)
      FROM
        insurance
      GROUP BY LAT , LON
      HAVING COUNT(*) = 1
    )
;
```
>Tips: Concat the **LAT** and **LON** as a whole to represent the location information.

Note: These two criteria should be met without an order, so if you attempt to filter data using criteria #1 first and then criteria #2, you will get a wrong result.

Taking the sample input as an example, the data set will be as following after taking the first criteria.

| PID | TIV_2015 | TIV_2016 | LAT | LON |
|-----|----------|----------|-----|-----|
| 1   | 10       | 5        | 10  | 10  |
| 3   | 10       | 30       | 20  | 20  |
| 4   | 10       | 40       | 40  | 40  |

Then, the second criteria cannot filter any records on this data set. So the result is 75(5+30+40), which is obviously wrong since the location of record with PID '3' is actually the same with the record having been filtered by the first criteria.

| PID | TIV_2015 | TIV_2016 | LAT | LON |
|-----|----------|----------|-----|-----|
| 2   | 20       | 20       | 20  | 20  |
| 3   | 10       | 30       | 20  | 20  |

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Here is my simple code
- Author: 400Coffee
- Creation Date: Fri May 19 2017 02:54:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 07:18:27 GMT+0800 (Singapore Standard Time)

<p>

```
select sum(TIV_2016) TIV_2016
from insurance a
where 1 = (select count(*) from insurance b where a.LAT=b.LAT and a.LON=b.LON) 
and 1 < (select count(*) from insurance c where a.TIV_2015=c.TIV_2015)  ;
```
</p>


### Using GROUP BY and HAVING COUNT(*) to choose the (not) unique data.
- Author: Mr-Bin
- Creation Date: Fri May 12 2017 09:54:04 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 12 2017 09:54:04 GMT+0800 (Singapore Standard Time)

<p>
Another trick is to represent the location information by concat the LAT and LON.
```
SELECT SUM(insurance.TIV_2016) AS TIV_2016
FROM insurance
WHERE insurance.TIV_2015 IN -- meet the creteria #1
    (
       SELECT TIV_2015
        FROM insurance
        GROUP BY TIV_2015
        HAVING COUNT(*) > 1
        )
AND CONCAT(LAT, LON) IN -- meet the creteria #2
    (
      SELECT CONCAT(LAT, LON) -- trick to take the LAT and LON as a pair
      FROM insurance
      GROUP BY LAT , LON
      HAVING COUNT(*) = 1
)
;
```
</p>


### My easy to understand query
- Author: quinoa
- Creation Date: Wed Feb 07 2018 15:42:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 03:48:49 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT ROUND(SUM(TIV_2016), 2) AS TIV_2016 FROM insurance 
WHERE PID IN 
(SELECT PID FROM insurance GROUP BY LAT, LON HAVING COUNT(*) = 1) 
AND PID NOT IN
(SELECT PID FROM insurance GROUP BY TIV_2015 HAVING COUNT(*) = 1)
```
</p>


