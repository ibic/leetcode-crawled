---
title: "Destination City"
weight: 1323
#id: "destination-city"
---
## Description
<div class="description">
<p>You are given the array <code>paths</code>, where <code>paths[i] = [cityA<sub>i</sub>, cityB<sub>i</sub>]</code> means there&nbsp;exists a direct path going from <code>cityA<sub>i</sub></code> to <code>cityB<sub>i</sub></code>. <em>Return the destination city, that is, the city without any path outgoing to another city.</em></p>

<p>It is guaranteed that the graph of paths forms a line without any loop, therefore, there will be exactly one destination city.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> paths = [[&quot;London&quot;,&quot;New York&quot;],[&quot;New York&quot;,&quot;Lima&quot;],[&quot;Lima&quot;,&quot;Sao Paulo&quot;]]
<strong>Output:</strong> &quot;Sao Paulo&quot; 
<strong>Explanation:</strong> Starting at &quot;London&quot; city you will reach &quot;Sao Paulo&quot; city which is the destination city. Your trip consist of: &quot;London&quot; -&gt; &quot;New York&quot; -&gt; &quot;Lima&quot; -&gt; &quot;Sao Paulo&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> paths = [[&quot;B&quot;,&quot;C&quot;],[&quot;D&quot;,&quot;B&quot;],[&quot;C&quot;,&quot;A&quot;]]
<strong>Output:</strong> &quot;A&quot;
<strong>Explanation:</strong> All possible trips are:&nbsp;
&quot;D&quot; -&gt; &quot;B&quot; -&gt; &quot;C&quot; -&gt; &quot;A&quot;.&nbsp;
&quot;B&quot; -&gt; &quot;C&quot; -&gt; &quot;A&quot;.&nbsp;
&quot;C&quot; -&gt; &quot;A&quot;.&nbsp;
&quot;A&quot;.&nbsp;
Clearly the destination city is &quot;A&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> paths = [[&quot;A&quot;,&quot;Z&quot;]]
<strong>Output:</strong> &quot;Z&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= paths.length &lt;= 100</code></li>
	<li><code>paths[i].length == 2</code></li>
	<li><code>1 &lt;=&nbsp;cityA<sub>i</sub>.length,&nbsp;cityB<sub>i</sub>.length &lt;= 10</code></li>
	<li><code>cityA<sub>i&nbsp;</sub><font face="monospace">!=&nbsp;</font>cityB<sub>i</sub></code></li>
	<li>All strings&nbsp;consist of lowercase and uppercase English letters and the space character.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Yelp - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java tricky 4 lines using one set
- Author: FunBam
- Creation Date: Sun May 03 2020 12:20:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 12:42:25 GMT+0800 (Singapore Standard Time)

<p>
[
["London","New York"]
["New York","Lima"]
["Lima","Sao Paulo"]
]

**Obeservation :** destination city will always be the right one

we transverse the input for two times
first time, we add all the cities on the right side to a set
second time, we check if the city occurs on the left side, if so, remove it from the set
result will be the city left in the set

```
class Solution {
    public String destCity(List<List<String>> paths) {
        Set<String> set= new HashSet<>();
        for (List<String> l: paths) set.add(l.get(1));
        for (List<String> l: paths) set.remove(l.get(0));
        return set.iterator().next();
    }
}
```
</p>


### Clean Python 3, Set in two lines
- Author: lenchen1112
- Creation Date: Sun May 03 2020 12:05:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 12:05:49 GMT+0800 (Singapore Standard Time)

<p>
Time: `O(N)`
Space: `O(cities)`
```
class Solution:
    def destCity(self, paths: List[List[str]]) -> str:
        A, B = map(set, zip(*paths))
        return (B - A).pop()
```
</p>


### [Python] 2 Set. 1 Line. 2 Loops. O(n) time, O(n) space, Code, Explanation
- Author: interviewrecipes
- Creation Date: Sun May 03 2020 12:23:07 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 29 2020 03:00:56 GMT+0800 (Singapore Standard Time)

<p>

**Key Idea**
Smart way: Add destination cities into a set and remove source cities from set.

**Why smart way works**
You want to find a city which will not go anywhere, so that city would never come into source cities i.e. `paths[i][0]` for all `i`.
Every other city will certainly appear once in source `paths[i][0]` for all `i`.

Hence, if you add all `paths[i][1]` into a set and remove `paths[i][0]` for all `i`\'s, it will eventually have just one value.

**Complexity Analysis**
If there are `n` paths, there would be at most `n` insertions into the set. and simmilarly `n` removals.
Hence space required would `O(n)`.
Each insertion/removal would take constant time, so time complexity `O(n)`.

Hope this helps!
Please upvote if you like it.

```
class Solution(object):
    def destCity(self, paths):
        return (set([path[1] for path in paths]) - set([path[0] for path in paths])).pop()
```
</p>


