---
title: "Maximize Sum Of Array After K Negations"
weight: 956
#id: "maximize-sum-of-array-after-k-negations"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of integers, we <strong>must</strong>&nbsp;modify the array in the following way: we choose an <code>i</code>&nbsp;and replace&nbsp;<code>A[i]</code> with <code>-A[i]</code>, and we repeat this process <code>K</code> times in total.&nbsp; (We may choose the same index <code>i</code> multiple times.)</p>

<p>Return the largest possible sum of the array after modifying it in this way.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[4,2,3]</span>, K = <span id="example-input-1-2">1</span>
<strong>Output: </strong><span id="example-output-1">5
<strong>Explanation: </strong>Choose indices (1,) and A becomes [4,-2,3].</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">[3,-1,0,2]</span>, K = <span id="example-input-2-2">3</span>
<strong>Output: </strong>6
<span id="example-output-1"><strong>Explanation: </strong>Choose indices (1, 2, 2) and A becomes [3,1,0,2].</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-3-1">[2,-3,-1,5,-4]</span>, K = <span id="example-input-3-2">2</span>
<strong>Output: </strong><span id="example-output-3">13
</span><span id="example-output-1"><strong>Explanation: </strong>Choose indices (1, 4) and A becomes [2,3,-1,5,4].</span>
</pre>
</div>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 10000</code></li>
	<li><code>1 &lt;= K &lt;= 10000</code></li>
	<li><code>-100 &lt;= A[i] &lt;= 100</code></li>
</ol>

</div>

## Tags
- Greedy (greedy)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- druva - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A very simple java solution
- Author: ebou
- Creation Date: Sun Mar 10 2019 12:03:15 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 16 2019 02:23:02 GMT+0800 (Singapore Standard Time)

<p>
```
    public int largestSumAfterKNegations(int[] A, int K) {
        PriorityQueue<Integer> pq = new PriorityQueue<Integer>();
        
        for(int x: A) pq.add(x);
        while( K--  > 0) pq.add(-pq.poll());
  
        int sum  = 0;
        for(int i = 0; i < A.length; i++){
            sum += pq.poll();
        }
        return sum;
    }
```
</p>


### [Java/C++/Python] Sort
- Author: lee215
- Creation Date: Sun Mar 10 2019 12:05:35 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Dec 20 2019 22:24:11 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
@codePower:
In case someone needs an explanation:
1- sort the numbers in ascending order
2- flip all the negative numbers, as long as k > 0
3- find the sum of the new array (with flipped numbers if any) and keep track of the minimum number
4- Now for the return statement

`res` is the total sum of the new array
`K % 2` check if the remaining `K` is odd.

Because if it\'s even, it will have no effect
(we will flip a number and then get it back to the original)

If it\'s odd,
flip the minimum number and remove twice its value from the result
(twice because we already added it as positive in our sum operation)
<br>

## **Complexity**
Time `O(NlogN)` for sorting.
Space `O(1)` extra space, in-place sort

Time can be easily improved to `O(N)`,
by quick selecting the `k`th negative.
<br>

**Java**
```java
    public int largestSumAfterKNegations(int[] A, int K) {
        Arrays.sort(A);
        for (int i = 0; K > 0 && i < A.length && A[i] < 0; ++i, --K)
            A[i] = -A[i];
        int res = 0, min = Integer.MAX_VALUE;
        for (int a : A) {
            res += a;
            min = Math.min(min, a);
        }
        return res - (K % 2) * min * 2;
    }
```

**C++**
```cpp
    int largestSumAfterKNegations(vector<int>& A, int K) {
        sort(A.begin(), A.end());
        for (int i = 0; K > 0 && i < A.size() && A[i] < 0; ++i, --K)
            A[i] = -A[i];
        return accumulate(A.begin(), A.end(), 0) - (K%2) * *min_element(A.begin(), A.end()) * 2;
    }
```

**Python:**
```py
    def largestSumAfterKNegations(self, A, K):
        A.sort()
        i = 0
        while i < len(A) and i < K and A[i] < 0:
            A[i] = -A[i]
            i += 1
        return sum(A) - (K - i) % 2 * min(A) * 2
```

</p>


### C++/Java O(n) | O(1)
- Author: votrubac
- Creation Date: Mon Mar 11 2019 03:46:05 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 11 2019 03:46:05 GMT+0800 (Singapore Standard Time)

<p>
Since the ```A[i]``` is limited to [-100, 100], we can use an fixed-size array ```cnt``` to count occurrences. Bucket sort (or, more precise, countint sort), in other words :)

Then, as we process numbers ```[-100, 100]```, we flip the negative numbers by moving count from ```cnt[i + 100]``` to ```cnt[-i + 100]```. This guaranties that, if ```K > 0``` after processing all negative numbers, the first positive number will have the smallest absolute value.

Therefore, when we encounter the first positive number, and our ```K % 2 == 1```, we negate one occurrence of that number.
```
int largestSumAfterKNegations(vector<int>& A, int K) {
  int cnt[201] = {}, j = -100;
  for (auto i : A) ++cnt[i + 100];
  for (auto i = -100; i <= 100 && K; ++i) {
    if (cnt[i + 100]) {
      auto k = i < 0 ? min(K, cnt[i + 100]) : K % 2;
      cnt[-i + 100] += k;
      cnt[i + 100] -= k;
      K = i < 0 ? K - k : 0;
    }
  }
  return accumulate(begin(cnt), end(cnt), 0, [&](int s, int cnt) { return s + cnt * j++; });
}
```
Java version:
```
public int largestSumAfterKNegations(int[] A, int K) {
  int[] cnt = new int[201];
  int res = 0;
  for (int i : A) ++cnt[i + 100];
  for (int i = -100; i <= 100 && K > 0; ++i) {
    if (cnt[i + 100] > 0) {
      int k = i < 0 ? Math.min(K, cnt[i + 100]) : K % 2;
      cnt[-i + 100] += k;
      cnt[i + 100] -= k;
      K = i < 0 ? K - k : 0;
    }
  }
  for (int i = -100; i <= 100; ++i) res += i * cnt[i + 100];
  return res;
}
```
</p>


