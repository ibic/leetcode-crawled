---
title: "Remove Zero Sum Consecutive Nodes from Linked List"
weight: 1134
#id: "remove-zero-sum-consecutive-nodes-from-linked-list"
---
## Description
<div class="description">
<p>Given the <code>head</code> of a linked list, we repeatedly delete consecutive sequences of nodes that sum to <code>0</code> until there are no such sequences.</p>

<p>After doing so, return the head of the final linked list.&nbsp; You may return any such answer.</p>

<p>&nbsp;</p>
<p>(Note that in the examples below, all sequences are serializations of <code>ListNode</code> objects.)</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> head = [1,2,-3,3,1]
<strong>Output:</strong> [3,1]
<strong>Note:</strong> The answer [1,2,1] would also be accepted.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> head = [1,2,3,-3,4]
<strong>Output:</strong> [1,2,4]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> head = [1,2,3,-3,-2]
<strong>Output:</strong> [1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The given linked list will contain between <code>1</code> and <code>1000</code> nodes.</li>
	<li>Each node in the linked list has <code>-1000 &lt;= node.val &lt;= 1000</code>.</li>
</ul>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Uber - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Greedily Skip with HashMap
- Author: lee215
- Creation Date: Sun Aug 25 2019 12:01:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 21:50:13 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Assume the input is an array.
Do you know how to solve it?
Scan from the left, and calculate the prefix sum.
Whenever meet the seen prefix,
remove all elements of the subarray between them.
<br>

# **Solution 1**
Because the head ListNode can be removed in the end,
I create a `dummy` ListNode and set it as a previous node of `head`.
`prefix` calculates the prefix sum from the first node to the current `cur` node.

Next step, we need an important hashmap `m` (no good name for it),
It takes a prefix sum as key, and the related node as the value.

Then we scan the linked list, accumulate the node\'s value as `prefix` sum.
1. If it\'s a prefix that we\'ve never seen, we set `m[prefix] = cur`.
2. If we have seen this prefix, `m[prefix]` is the node we achieve this prefix sum.
We want to skip all nodes between `m[prefix]` and `cur.next` (exclusive).
So we simplely do `m[prefix].next = cur.next`.

We keep doing these and it\'s done.
<br>

**Complexity**
Time `O(N)`, one pass
Space`O(N)`, for hashmap
<br>

**Java:**
```java
    public ListNode removeZeroSumSublists(ListNode head) {
        ListNode dummy = new ListNode(0), cur = dummy;
        dummy.next = head;
        int prefix = 0;
        Map<Integer, ListNode> m = new HashMap<>();
        while (cur != null) {
            prefix += cur.val;
            if (m.containsKey(prefix)) {
                cur =  m.get(prefix).next;
                int p = prefix + cur.val;
                while (p != prefix) {
                    m.remove(p);
                    cur = cur.next;
                    p += cur.val;
                }
                m.get(prefix).next = cur.next;
            } else {
                m.put(prefix, cur);
            }
            cur = cur.next;
        }
        return dummy.next;
    }
```

**C++:**
```cpp
    ListNode* removeZeroSumSublists(ListNode* head) {
        ListNode* dummy = new ListNode(0), *cur = dummy;
        dummy->next = head;
        int prefix = 0;
        map<int, ListNode*> m;
        while (cur) {
            prefix += cur->val;
            if (m.count(prefix)) {
                cur =  m[prefix]->next;
                int p = prefix + cur->val;
                while (p != prefix) {
                    m.erase(p);
                    cur = cur->next;
                    p += cur->val;
                }
                m[prefix]->next = cur->next;
            } else {
                m[prefix] = cur;
            }
            cur = cur->next;
        }
        return dummy->next;
    }
```

**Python:**
```python
    def removeZeroSumSublists(self, head):
        cur = dummy = ListNode(0)
        dummy.next = head
        prefix = 0
        seen = collections.OrderedDict()
        while cur:
            prefix += cur.val
            node = seen.get(prefix, cur)
            while prefix in seen:
                seen.popitem()
            seen[prefix] = node
            node.next = cur = cur.next
        return dummy.next
```
<br>

# Improvement
I think that\'s the best part of my post.
It\'s a great discuss in the leetcode\'s discuss.

People are willing to read my article and help me improve it.
To be honest, I think I take good responsiblilty to maintain my solution.
(Though the case I don\'t have prime membership and canot even read my own post in locked problem)

Thanks to @alexjst inspired me the follwing solution.
<br>

# Soluiton 2: Two Passes

The story is that,
I wrote the really concise solution,
it got accepted but actully it\'s wrong.
I fixed it by adding another while loop.
That is the Solution 1.

If we don\'t insist on one pass,
we can find the two passes is actually really neat.

That turned back to the intuition that I mentioned:
Assume the input is an array.
How will you solve the problem?

Iterate for the first time,
calculate the `prefix` sum,
and save the it to `seen[prefix]`

Iterate for the second time,
calculate the `prefix` sum,
and directly skip to last occurrence of this `prefix`

**Java**
```java
    public ListNode removeZeroSumSublists(ListNode head) {
        int prefix = 0;
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        Map<Integer, ListNode> seen = new HashMap<>();
        seen.put(0, dummy);
        for (ListNode i = dummy; i != null; i = i.next) {
            prefix += i.val;
            seen.put(prefix, i);
        }
        prefix = 0;
        for (ListNode i = dummy; i != null; i = i.next) {
            prefix += i.val;
            i.next = seen.get(prefix).next;
        }
        return dummy.next;
    }
```

**C++**
```
    ListNode* removeZeroSumSublists(ListNode* head) {
        ListNode* dummy = new ListNode(0);
        dummy->next = head;
        int prefix = 0;
        unordered_map<int, ListNode*> seen;
        for (ListNode* i = dummy; i; i = i->next) {
            seen[prefix += i->val] = i;
        }
        prefix = 0;
        for (ListNode* i = dummy; i; i = i->next) {
            i->next = seen[prefix += i->val]->next;
        }
        return dummy->next;
    }
```

**Python**
```py
    def removeZeroSumSublists(self, head):
        prefix = 0
        seen = {}
        seen[0] = dummy = ListNode(0)
        dummy.next = head
        while head:
            prefix += head.val
            seen[prefix] = head
            head = head.next
        head = dummy
        prefix = 0
        while head:
            prefix += head.val
            head.next = seen[prefix].next
            head = head.next
        return dummy.next
```
<br>

# **Update 2019-08-25**
The OJ solution was wrong.
It didn\'t block the right submit,
but wrong submit can also get accepted.

Following the test case given by @kay_deep:
`[1, 3, 2, -3, -2, 5, 100, -100, 1]`
The expected result should be `[1,5,1]` or `[1,3,2,1]`.

Some solution in the discuss part are still wrong.
</p>


### C++  O(n) (explained with pictures)
- Author: hamlet_fiis
- Creation Date: Sun Aug 25 2019 12:05:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 25 2019 12:07:38 GMT+0800 (Singapore Standard Time)

<p>
**Intuition and Algorithm**
Imagine that you have this Linked list:
head:      [3,4,2,-6, 1,1,5, -6] 

**What elements should I remove?**
We will accumulate the values.
head\u2019 :   [3, 7,9,3,4,5,10,4]
(Like an array), head\'[i] = head[i] + head\' [i-1]
If we see repeated elements then we have to deleted as follows.

![image](https://assets.leetcode.com/users/hamlet_fiis/image_1566705933.png)

After delete:

![image](https://assets.leetcode.com/users/hamlet_fiis/image_1566705946.png)

**Answer:**
head: [3,1]

**Algorithm**
* Iterate LinkedList from left to right
* Process each accumulative value in this datastructure. (unordered_map<int,ListNode*>um) um[ac] = currentNode;
* Given a current accumulative value check if exists in our map (get previous element).
* If they exist  um[ac]->next = currentNode->next, then delete intermediate nodes in our map between  um[ac] and currentNode


```
class Solution {
public:
    ListNode* removeZeroSumSublists(ListNode* head) {
        
        ListNode* root =new ListNode(0);
        root->next=head;
        unordered_map<int,ListNode*>um;
        um[0]=root;
        int ac=0;
        
        while(head!=NULL){
            ac+=head->val;
            
            //found value
            if(um.find(ac)!=um.end()){
                ListNode* prev= um[ac];
                ListNode* start= prev;
                
                //delete bad references
                int aux = ac;
                while(prev!=head){
                    prev=prev->next;
                    aux+=prev->val;
                    if(prev!=head)um.erase(aux);
                }
                
                start->next = head->next;
            }else{
                um[ac]= head;
            }
            
            head=head->next;
        }
        
        return root->next;
    }
};
```

<b>Complexity Analysis </b>
* Time Complexity: O(n) where n is the length of head
* Space Complexity: O(n) where n is the length of head
</p>


### Java O(N) with detail explanation
- Author: leetcoder2236
- Creation Date: Fri Oct 25 2019 22:37:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 25 2019 22:37:45 GMT+0800 (Singapore Standard Time)

<p>
```
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode removeZeroSumSublists(ListNode head) {
        // The observation here is that the sum from index 0 to index M will be 
        // equal to sum from index 0 to index N if sum from index (M+1) to index N is 0.
        // Thus, here we track the sum from index 0 to each index, using a Map to indicate
        // the farthest index N that we can remove from index M, then we shall be able to
        // remove M+1 -> N and continue from N+1. This works since we don\'t have to optimize
        // for the number of sequences to be removed
        
        // Map from sum from index 0 to the farthest value that the sum stays unchanged.
        Map<Integer, ListNode> sumToFarthestNodeMap = new HashMap<>();
        
        // Need the dummy node to track the new head if changed.
        ListNode preHead = new ListNode(0);
        preHead.next = head;
        
        // First iteration to compute the map.
        int sum = 0;
        for (ListNode p = preHead; p != null; p = p.next) {
            sum += p.val;
            sumToFarthestNodeMap.put(sum, p);
        }
        
        // Second iteration to re-connect the nodes to the farthest node where the sum stays unchanged
        sum = 0;
        for (ListNode p = preHead; p != null; p = p.next) {
            sum += p.val;
            p.next = sumToFarthestNodeMap.get(sum).next;
        }
        
        // Done, return the head from preHead
        return preHead.next;
    }
}
```
</p>


