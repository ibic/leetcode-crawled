---
title: "Consecutive Available Seats"
weight: 1499
#id: "consecutive-available-seats"
---
## Description
<div class="description">
Several friends at a cinema ticket office would like to reserve consecutive available seats.<br />
Can you help to query all the consecutive available seats order by the seat_id using the following <code>cinema</code> table?
<pre>
| seat_id | free |
|---------|------|
| 1       | 1    |
| 2       | 0    |
| 3       | 1    |
| 4       | 1    |
| 5       | 1    |
</pre>

<p>&nbsp;</p>
Your query should return the following result for the sample case above.

<p>&nbsp;</p>

<pre>
| seat_id |
|---------|
| 3       |
| 4       |
| 5       |
</pre>
<b>Note</b>:

<ul>
	<li>The seat_id is an auto increment int, and free is bool (&#39;1&#39; means free, and &#39;0&#39; means occupied.).</li>
	<li>Consecutive available seats are more than 2(inclusive) seats consecutively available.</li>
</ul>

</div>

## Tags


## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach: Using self `join` and `abs()`[Accepted]

**Intuition**

There is only one table in this problem, so we probably need to use **self join** for this relative complex problem.

**Algorithm**

First, let's see what we have after joining this table with itself.
>Note: The result of join two tables is the [Cartesian product](https://en.wikipedia.org/wiki/Cartesian_product) of these two tables.

```sql
select a.seat_id, a.free, b.seat_id, b.free
from cinema a join cinema b;
```

| seat_id | free | seat_id | free |
|---------|------|---------|------|
| 1       | 1    | 1       | 1    |
| 2       | 0    | 1       | 1    |
| 3       | 1    | 1       | 1    |
| 4       | 1    | 1       | 1    |
| 5       | 1    | 1       | 1    |
| 1       | 1    | 2       | 0    |
| 2       | 0    | 2       | 0    |
| 3       | 1    | 2       | 0    |
| 4       | 1    | 2       | 0    |
| 5       | 1    | 2       | 0    |
| 1       | 1    | 3       | 1    |
| 2       | 0    | 3       | 1    |
| 3       | 1    | 3       | 1    |
| 4       | 1    | 3       | 1    |
| 5       | 1    | 3       | 1    |
| 1       | 1    | 4       | 1    |
| 2       | 0    | 4       | 1    |
| 3       | 1    | 4       | 1    |
| 4       | 1    | 4       | 1    |
| 5       | 1    | 4       | 1    |
| 1       | 1    | 5       | 1    |
| 2       | 0    | 5       | 1    |
| 3       | 1    | 5       | 1    |
| 4       | 1    | 5       | 1    |
| 5       | 1    | 5       | 1    |


To find the consecutive available seats, the value in the a.seat_id should be more(or less) than the value b.seat_id, and both of them should be free.

```sql
select a.seat_id, a.free, b.seat_id, b.free
from cinema a join cinema b
  on abs(a.seat_id - b.seat_id) = 1
  and a.free = true and b.free = true;
```

| seat_id | free | seat_id | free |
|---------|------|---------|------|
| 4       | 1    | 3       | 1    |
| 3       | 1    | 4       | 1    |
| 5       | 1    | 4       | 1    |
| 4       | 1    | 5       | 1    |

At last, choose the concerned column seat_id, and display the result ordered by seat_id.
>Note: You may notice that the seat with *seat_id* '4' appears twice in this table. This is because seat '4' next to '3' and also next to '5'. So we need to use `distinct` to filter the duplicated records.

**MySQL**

```sql
select distinct a.seat_id
from cinema a join cinema b
  on abs(a.seat_id - b.seat_id) = 1
  and a.free = true and b.free = true
order by a.seat_id
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### AC using self join
- Author: Douglas1612
- Creation Date: Sun Jun 11 2017 22:47:02 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 08:42:14 GMT+0800 (Singapore Standard Time)

<p>
```
select distinct a.seat_id
from cinema a
join cinema b
on abs(a.seat_id - b.seat_id) = 1
and a.free=true and b.free=true
order by a.seat_id;
```
</p>


### Use LAG and LEAD function
- Author: samu99
- Creation Date: Mon May 13 2019 22:52:42 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 13 2019 22:52:42 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT
    seat_id
FROM
    (SELECT
        seat_id,
        free,
        LAG(free,1) OVER (ORDER BY seat_id) as free_lag,
        LEAD(free,1) OVER (ORDER BY seat_id) as free_lead
    FROM cinema ) as t
WHERE (free=1 AND free_lag=1)
OR (free=1 AND free_lead=1)
;
```

Writen in MS SQL Server because it has `LAG` and `LEAD` function. I think it is more intuitive and  easier to understand than Cartesian product answer

| seat_id | free | free_lag | free_lead |
|---------|------|----------|-----------|
| 1       | 1    | NULL     | 0         |
| 2       | 0    | 1        | 1         |
| 3       | 1    | 0        | 1         |
| 4       | 1    | 1        | 1         |
| 5       | 1    | 1        | NULL      |

`(free=1 AND free_lag=1)` can choose consecutive seats without first one
`(free=1 AND free_lead=1)`  can choose consecutive seats without last one 
Use `OR` to combine these two to get correct result.
This code does not involved with `JOIN` so I think it is faster when TABLE is big
</p>


### Accepted Answer
- Author: richarddia
- Creation Date: Sun Jun 04 2017 14:24:09 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 21:35:31 GMT+0800 (Singapore Standard Time)

<p>
```
select C1.seat_id from cinema C1  where 
C1.free=1 
and 
(
    C1.seat_id+1 in (select seat_id from cinema where free=1) 
    or 
    C1.seat_id-1 in (select seat_id from cinema where free=1) 
) 
order by C1.seat_id
```
</p>


