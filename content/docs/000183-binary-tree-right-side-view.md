---
title: "Binary Tree Right Side View"
weight: 183
#id: "binary-tree-right-side-view"
---
## Description
<div class="description">
<p>Given a binary tree, imagine yourself standing on the <em>right</em> side of it, return the values of the nodes you can see ordered from top to bottom.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong>&nbsp;[1,2,3,null,5,null,4]
<strong>Output:</strong>&nbsp;[1, 3, 4]
<strong>Explanation:
</strong>
   1            &lt;---
 /   \
2     3         &lt;---
 \     \
  5     4       &lt;---
</pre>
</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Facebook - 55 (taggedByAdmin: false)
- Amazon - 12 (taggedByAdmin: true)
- eBay - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)
- Mathworks - 7 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Overview

**DFS vs. BFS**

There are two ways to traverse the tree: DFS _depth first search_ and 
BFS _breadth first search_. Here is a small summary 

![diff](../Figures/199_rewrite/traversals.png)

BFS traverses level by level, and DFS first goes to the leaves.

![diff](../Figures/199_rewrite/dfs_bfs2.png) 

> Which approach to choose, BFS or DFS? 

- The problem is to return a list of last elements from all levels, 
so it's the way more natural to implement BFS here. 

- Time complexity is the same $$\mathcal{O}(N)$$ 
both for DFS and BFS since one has to visit all nodes.

- Space complexity is $$\mathcal{O}(H)$$ for DFS and  
$$\mathcal{O}(D)$$ for BFS, where $$H$$ is a tree height,
and $$D$$ is a tree diameter. They both result in 
$$\mathcal{O}(N)$$ space in the worst-case scenarios:
skewed tree for DFS and complete tree for BFS. 

BFS wins for this problem, so let's use the opportunity 
to check out three different BFS implementations with the queue.

**BFS implementation**  

All three implementations use the queue in a standard BFS way:

- Push the root into the queue. 

- Pop-out a node from the _left_.
 
- Push the _left_ child into the queue, and then push the _right_ child. 

![diff](../Figures/199_rewrite/implem2.png)

**Three BFS approaches**

The difference is how to find the end of the level, i.e. the rightmost
element:

- Two queues, one for the previous level and one for the current.

- One queue with sentinel to mark the end of the level.

- One queue + level size measurement. 

<br />
<br />


---
#### Approach 1: BFS: Two Queues

Let's use two queues: one for the current level,
and one for the next. The idea is to pop the nodes one by one from
the current level and push their children into the next level queue.
Each time the current queue is empty, we have the right side element in hands. 

![diff](../Figures/199_rewrite/levels.png)

**Algorithm**

- Initiate the list of the right side view `rightside`.

- Initiate two queues: one for the current level, 
and one for the next. Add root into `nextLevel` queue.

- While `nextLevel` queue is not empty:

    - Initiate the current level: `currLevel = nextLevel`,
    and empty the next level `nextLevel`. 
    
    - While current level queue is not empty:

        - Pop out a node from the current level queue.
        
        - Add first _left_ and then _right_ child node into `nextLevel`
        queue.
    
    - Now `currLevel` is empty, and the node we have in hands 
    is the last one, and makes a part of the right side view.
    Add it into `rightside`.
    
- Return `rightside`.  

**Implementation**

<iframe src="https://leetcode.com/playground/LLFwqumS/shared" frameBorder="0" width="100%" height="500" name="LLFwqumS"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since one has to visit each node. 
    
* Space complexity: $$\mathcal{O}(D)$$ to keep the queues,
where $$D$$ is a tree diameter. 
Let's use the last level to estimate the queue size. 
This level could contain up to $$N/2$$ tree nodes in the case of 
[complete binary tree](https://leetcode.com/problems/count-complete-tree-nodes/).    
<br />
<br />


---
#### Approach 2: BFS: One Queue + Sentinel

Another approach is to push all the nodes in one queue and
to use a [sentinel node](https://en.wikipedia.org/wiki/Sentinel_node)
to separate the levels. Typically, one could use `null` as
a sentinel.

![diff](../Figures/199_rewrite/sentinel.png)

The first step is to initiate the first level: `root` + `null` as
a sentinel. Once it's done, continue to pop the nodes one by one from the left and push their children to the right.
Stop each time the current node is `null` because it means we hit the end of the current level. Each stop is a time to
update a right side view list and to push `null` in the queue to mark the end of the next level.

**Algorithm**

- Initiate the list of the right side view `rightside`.

- Initiate the queue by adding a root. Add `null` sentinel
to mark the end of the first level. 

- Initiate the current node as `root`. 

- While queue is not empty:

    - Save the previous node `prev = curr` and pop the 
    current node from the queue `curr = queue.poll()`. 
    
    - While the current node is not `null`:

        - Add first _left_ and then _right_ child node into the
        queue.
        
        - Update both previous and current nodes:
        `prev = curr`, `curr = queue.poll()`.
    
    - Now the current node is null, _i.e._ we reached the end of the current level. Hence the previous node is the rightmost one and makes a part of the right side view.
    Add it into `rightside`.
    
    - If the queue is not empty, push the null node as a sentinel,
    to mark the end of the next level.
    
- Return `rightside`.  

**Implementation**

Note, that `ArrayDeque` in Java doesn't support null
elements, and hence the data structure to use here 
is `LinkedList`.  

<iframe src="https://leetcode.com/playground/doc7wZUu/shared" frameBorder="0" width="100%" height="500" name="doc7wZUu"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since one has to visit each node. 
    
* Space complexity: $$\mathcal{O}(D)$$ to keep the queues,
where $$D$$ is a tree diameter. 
Let's use the last level to estimate the queue size. 
This level could contain up to $$N/2$$ tree nodes in the case of 
[complete binary tree](https://leetcode.com/problems/count-complete-tree-nodes/).   
<br />
<br />


---
#### Approach 3: BFS: One Queue + Level Size Measurements

Instead of using the sentinel, we could write down the length 
of the current level.

![diff](../Figures/199_rewrite/length.png)

**Algorithm**

- Initiate the list of the right side view `rightside`.

- Initiate the queue by adding a root. 

- While the queue is not empty:

    - Write down the length of the current level:
    `levelLength = queue.size()`.

    - Iterate over `i` from `0` to `level_length - 1`:
    
        - Pop the current node from the queue: 
        `node = queue.poll()`.
        
        - If `i == levelLength - 1`, then it's the last node
        in the current level, push it to `rightsize` list.

        - Add first _left_ and then _right_ child node 
        into the queue.
    
- Return `rightside`. 

**Implementation**

<iframe src="https://leetcode.com/playground/TQ8s6xwe/shared" frameBorder="0" width="100%" height="500" name="TQ8s6xwe"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since one has to visit each node. 
    
* Space complexity: $$\mathcal{O}(D)$$ to keep the queues,
where $$D$$ is a tree diameter. 
Let's use the last level to estimate the queue size. 
This level could contain up to $$N/2$$ tree nodes in the case of 
[complete binary tree](https://leetcode.com/problems/count-complete-tree-nodes/).   
<br />
<br />


___
#### Approach 4: Recursive DFS

Everyone likes recursive DFS, so let's add it here as well.
The idea is simple: to traverse the tree level by level, 
starting each time from the rightmost child.  

**Implementation**

<iframe src="https://leetcode.com/playground/V39TSeby/shared" frameBorder="0" width="100%" height="395" name="V39TSeby"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since one has to visit each node. 
    
* Space complexity: $$\mathcal{O}(H)$$ to keep the recursion stack,
where $$H$$ is a tree height. 
The worst-case situation is a skewed tree, when $$H = N$$.

## Accepted Submission (python3)
```python3
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def rightSideView(self, root: TreeNode) -> List[int]:
        def traverse(node, level):
            nonlocal r
            if not node:
                return
            if level >= len(r):
                r.append(node.val)
            traverse(node.right, level + 1)
            traverse(node.left, level + 1)
        r = []
        traverse(root, 0)
        return r
```

## Top Discussions
### My simple accepted solution(JAVA)
- Author: zwangbo
- Creation Date: Sat Apr 11 2015 12:30:34 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Mar 26 2020 11:18:40 GMT+0800 (Singapore Standard Time)

<p>
The core idea of this algorithm: 

1.Each depth of the tree only select one node.  
2. View depth is current size of result list.

Here is the code: 

    public class Solution {
        public List<Integer> rightSideView(TreeNode root) {
            List<Integer> result = new ArrayList<Integer>();
            rightView(root, result, 0);
            return result;
        }
        
        public void rightView(TreeNode curr, List<Integer> result, int currDepth){
            if(curr == null){
                return;
            }
            if(currDepth == result.size()){
                result.add(curr.val);
            }
            
            rightView(curr.right, result, currDepth + 1);
            rightView(curr.left, result, currDepth + 1);
            
        }
    }
	

</p>


### My C++ solution, modified preorder traversal
- Author: gavin5
- Creation Date: Sat Apr 04 2015 03:39:46 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 27 2018 10:42:13 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        void recursion(TreeNode *root, int level, vector<int> &res)
        {
            if(root==NULL) return ;
            if(res.size()<level) res.push_back(root->val);
            recursion(root->right, level+1, res);
            recursion(root->left, level+1, res);
        }
        
        vector<int> rightSideView(TreeNode *root) {
            vector<int> res;
            recursion(root, 1, res);
            return res;
        }
    };
</p>


### Reverse Level Order Traversal, java
- Author: justjiayu
- Creation Date: Sat Apr 04 2015 05:21:27 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 06 2018 12:39:29 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public List<Integer> rightSideView(TreeNode root) {
            // reverse level traversal
            List<Integer> result = new ArrayList();
            Queue<TreeNode> queue = new LinkedList();
            if (root == null) return result;
            
            queue.offer(root);
            while (queue.size() != 0) {
                int size = queue.size();
                for (int i=0; i<size; i++) {
                    TreeNode cur = queue.poll();
                    if (i == 0) result.add(cur.val);
                    if (cur.right != null) queue.offer(cur.right);
                    if (cur.left != null) queue.offer(cur.left);
                }
                
            }
            return result;
        }
    }
</p>


