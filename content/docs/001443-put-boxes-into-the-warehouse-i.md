---
title: "Put Boxes Into the Warehouse I"
weight: 1443
#id: "put-boxes-into-the-warehouse-i"
---
## Description
<div class="description">
<p>You are given two arrays of positive integers, <code>boxes</code> and <code>warehouse</code>, representing the heights of some boxes of unit width and the heights of <code>n</code> rooms in a warehouse respectively. The warehouse&#39;s rooms are labelled from <code>0</code> to <code>n - 1</code> from left to right where <code>warehouse[i]</code> (0-indexed) is the height of the <code>i<sup>th</sup></code> room.</p>

<p>Boxes are put into the warehouse by the following rules:</p>

<ul>
	<li>Boxes cannot be stacked.</li>
	<li>You can rearrange the insertion order of the boxes.</li>
	<li>Boxes&nbsp;can only be pushed into the warehouse from left to right only.</li>
	<li>If the height of some room in the warehouse is less than the height of a box, then that box and all other boxes behind it will be stopped before that room.</li>
</ul>

<p>Return <em>the maximum number of boxes you can put into the warehouse.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/26/11.png" style="width: 400px; height: 242px;" />
<pre>
<strong>Input:</strong> boxes = [4,3,4,1], warehouse = [5,3,3,4,1]
<strong>Output:</strong> 3
<strong>Explanation:&nbsp;
</strong><img alt="" src="https://assets.leetcode.com/uploads/2020/08/26/12.png" style="width: 280px; height: 242px;" />
We can first put the box of height 1 in room 4. Then we can put the box of height 3 in either of the 3 rooms 1, 2, or 3. Lastly, we can put one box of height 4 in room 0.
There is no way we can fit all 4 boxes in the warehouse.</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/26/21.png" style="width: 400px; height: 202px;" />
<pre>
<strong>Input:</strong> boxes = [1,2,2,3,4], warehouse = [3,4,1,2]
<strong>Output:</strong> 3
<strong>Explanation: 
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/26/22.png" style="width: 280px; height: 202px;" />
</strong>Notice that it&#39;s not possible to put the box of height 4 into the warehouse since it cannot pass the first room of height 3.
Also, for the last two rooms, 2 and 3, only boxes of height 1 can fit.
We can fit 3 boxes maximum as shown above. The yellow box can also be put in room 2 instead.
Swapping the orange and green boxes is also valid, or swapping one of them with the red box.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> boxes = [1,2,3], warehouse = [1,2,3,4]
<strong>Output:</strong> 1
<strong>Explanation: </strong>Since the first room in the warehouse is of height 1, we can only put boxes of height 1.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> boxes = [4,5,6], warehouse = [3,3,3,3,3]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == warehouse.length</code></li>
	<li><code>1 &lt;= boxes.length, warehouse.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= boxes[i], warehouse[i] &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### JAVA Short O(NlgN+M) Time/ O(1) Space
- Author: REED_W
- Creation Date: Thu Aug 27 2020 15:51:07 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 27 2020 15:54:45 GMT+0800 (Singapore Standard Time)

<p>
```
	private int sln2(int[] boxes, int[] wh){
        Arrays.sort(boxes);
        int m = boxes.length;
        int n = wh.length;
        int res = 0; 
        for(int i = 0;i < m & res<n;i++){
            if(boxes[m-i-1] <= wh[res]) {
                res++;
            }
        }
        return res;
    }
</p>


### [Python] Greedy approach with example - O(NlogN + M) / O(1)
- Author: alanlzl
- Creation Date: Thu Aug 27 2020 14:07:04 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 04 2020 11:10:17 GMT+0800 (Singapore Standard Time)

<p>
**Idea**

We update the height of warehouse rooms, `warehouse[i]`, to be `min(warehouse[:i+1])`. Then, we sort all boxes and greedily push boxes to the right.

<br />

**Example**

boxes = `[4, 3, 4, 1]` => `[1, 3, 4, 4]` (sorting)
warehouse = `[5, 3, 3, 4, 1]` => `[5, 3, 3, 3, 1]` (updating heights)

Match 1 (`j = 4, i = 0`)
 - [5, 3, 3, 3, **1**]
 - [**1**, 3, 4, 4]

Match 2 (`j = 3, i = 1`)
 - [5, 3, 3, **3**, 1]
 - [1, **3**, 4, 4]

Match 3 (`j = 0, i = 2`)
 - [**5**, 3, 3, 3, 1]
 - [1, 3, **4**, 4]

We return `ans = 3`.

 <br />
 
**Complexity**

Time complexity: `O(NlogN + M)`
Space complexity: `O(1)`

<br />

**Python**
```Python
class Solution:
    def maxBoxesInWarehouse(self, boxes: List[int], warehouse: List[int]) -> int:
        n, m = len(boxes), len(warehouse)
        min_h = warehouse[0]
        for i in range(m):
            warehouse[i] = min_h = min(warehouse[i], min_h)
        boxes.sort()
        ans = 0
        for j in range(m - 1, -1, -1):
            if boxes[ans] <= warehouse[j]:
                ans += 1
        return ans
```

<br />

**Edit**

Credit ot [@REED_W](https://leetcode.com/problems/put-boxes-into-the-warehouse-i/discuss/814301/JAVA-Short-O(NlgN%2BM)-Time-O(1)-Space).

We can do this in one-pass without changing the content of `warehouse`. The basic idea is to greedily add large box to the left of warehouse rooms.

Time and space complexities are the same.

```Python
class Solution:
    def maxBoxesInWarehouse(self, boxes, warehouse):
        boxes.sort()
        m, n, ans = len(warehouse), len(boxes), 0
        for i in range(n - 1, -1, -1):
            if boxes[i] <= warehouse[ans]:
                ans += 1
                if ans == m: return m
        return ans
```
</p>


### Greedy: Push the Shortest Box All the Way to the Rightmost Position
- Author: FighterNan
- Creation Date: Thu Aug 27 2020 12:48:15 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 27 2020 13:08:15 GMT+0800 (Singapore Standard Time)

<p>
From right to left, consider any index `i` in the warehouse, boxes can be pushed from the very left to `i` should satisify `height <= min(warehouse[0:i+1])`, otherwise it will get stuck in the middle. If a box get stuck in the middle position `j`, we lose `warehouse[j+1:i+1]`, which is bad.

In order to maximum the utilization of the warehouse, we **push the shortest box first**, and we push it **all the way to the rightmost position** that this shortest box can reach. The algorithm can be implemented efficiently using two pointers after sorting the boxes, since `min(warehouse[0:i+1])` is non-increasing.

`C++`
```
class Solution {
public:
    int maxBoxesInWarehouse(vector<int>& boxes, vector<int>& warehouse) {
        sort(boxes.begin(), boxes.end());
        int m = boxes.size(), n = warehouse.size();
		// smallest[i]: min(warehouse[0:i+1])
        vector<int> smallest(n);
        int min_height = INT_MAX;
        for (int i = 0; i < n; ++i) {
            min_height = min(min_height, warehouse[i]);
            smallest[i] = min_height;
        }
		
		// j: the current shortest box
		// i: the rightmost position
        int j = 0;
        int res = 0;
        for (int i = n - 1; i >= 0 && j < m; --i) {
			// if the current shortest box can be pushed, do so.
            if (boxes[j] <= smallest[i]) {
                res++;
                j++;
            }
        }
        return res;
    }
};
```
Time: `O(max(mlogm, n))`; Space: `O(n)`
</p>


