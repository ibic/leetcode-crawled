---
title: "Sort List"
weight: 148
#id: "sort-list"
---
## Description
<div class="description">
<p>Given the <code>head</code> of a linked list, return <em>the list after sorting it in <strong>ascending order</strong></em>.</p>

<p><strong>Follow up:</strong> Can you sort the linked list in <code>O(n logn)</code> time and <code>O(1)</code>&nbsp;memory (i.e. constant space)?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/14/sort_list_1.jpg" style="width: 450px; height: 194px;" />
<pre>
<strong>Input:</strong> head = [4,2,1,3]
<strong>Output:</strong> [1,2,3,4]
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/14/sort_list_2.jpg" style="width: 550px; height: 184px;" />
<pre>
<strong>Input:</strong> head = [-1,5,3,4,0]
<strong>Output:</strong> [-1,0,3,4,5]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> head = []
<strong>Output:</strong> []
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the list is in the range <code>[0, 5 * 10<sup>4</sup>]</code>.</li>
	<li><code>-10<sup>5</sup> &lt;= Node.val &lt;= 10<sup>5</sup></code></li>
</ul>

</div>

## Tags
- Linked List (linked-list)
- Sort (sort)

## Companies
- Amazon - 8 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Microsoft - 7 (taggedByAdmin: false)
- Facebook - 5 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---

#### Overview ####

The problem is to sort the linked list in $$\mathcal{O}(n \log n)$$ time and using only constant extra space. If we look at various sorting algorithms, [Merge Sort](https://en.wikipedia.org/wiki/Merge_sort) is one of the efficient sorting algorithms that is popularly used for sorting the linked list. The merge sort algorithm runs in $$\mathcal{O}(n \log n)$$ time in all the cases. Let's discuss approaches to sort linked list using merge sort.

> [Quicksort](https://en.wikipedia.org/wiki/Quicksort) is also one of the efficient algorithms with the average time complexity of $$\mathcal{O}(n \log n)$$. But the worst-case time complexity is $$\mathcal{O}(n ^{2})$$. Also, variations of the quick sort like randomized quicksort are not efficient for the linked list because unlike arrays, random access in the linked list is not possible in $$\mathcal{O}(1)$$ time.
If we sort the linked list using quicksort, we would end up using the head as a pivot element which may not be efficient in all scenarios.
---

#### Approach 1: Top Down Merge Sort

**Intuition**

Merge sort is a popularly known algorithm that follows the[ Divide and Conquer Strategy](https://en.wikipedia.org/wiki/Divide-and-conquer_algorithm). The divide and conquer strategy can be split into 2 phases:

 _Divide phase_: Divide the problem into subproblems.

_Conquer phase_: Repeatedly solve each subproblem independently and combine the result to form the original problem.

The Top Down approach for merge sort recursively splits the original list into sublists of equal sizes, sorts each sublist independently, and eventually merge the sorted lists.  Let's look at the algorithm to implement merge sort in Top Down Fashion.

**Algorithm**

- Recursively split the original list into two halves. The split continues until there is only one node in the linked list (Divide phase). To split the list into two halves, we find the middle of the linked list using the Fast and Slow pointer approach as mentioned in [Find Middle Of Linked List](https://leetcode.com/problems/middle-of-the-linked-list/).

- Recursively sort each sublist and combine it into a single sorted list. (Merge Phase). This is similar to the problem [Merge two sorted linked lists](https://leetcode.com/problems/merge-two-sorted-lists/)


The process continues until we get the original list in sorted order.

For the linked list = `[10,1,60,30,5]`, the following figure illustrates the merge sort process using a top down approach.

![img](../Figures/148/topDown_merge_sort.png)

If we have sorted lists, list1 = `[1,10]` and list2 = `[5,30,60]`. The following animation illustrates the merge process of both lists into a single sorted list.

!?!../Documents/148_sort_list.json:1584,570!?!

<iframe src="https://leetcode.com/playground/SAXk3Mon/shared" frameBorder="0" width="100%" height="500" name="SAXk3Mon"></iframe>

**Complexity Analysis**

* Time Complexity: $$\mathcal{O}(n \log n)$$, where $$n$$ is the number of nodes in linked list.
The algorithm can be split into 2 phases, Split and Merge.

Let's assume that $$n$$ is power of $$2$$. For `n = 16`, the split and merge operation in  Top Down fashion can be visualized as follows

![img](../Figures/148/top_down_time_complexity.png)

**_Split_**

The recursion tree expands in form of a complete binary tree, splitting the list into two halves recursively. The number of levels in a complete binary tree is given by $$\log_{2} n$$. For $$n=16$$, number of splits = $$\log_{2} 16 = 4$$

**_Merge_**  

At each level, we merge n nodes which takes $$\mathcal{O}(n)$$ time.
For $$n = 16$$, we perform merge operation on $$16$$ nodes in each of the $$4$$ levels.

So the time complexity for split and merge operation is $$\mathcal{O}(n \log n)$$

* Space Complexity: $$\mathcal{O}(\log n)$$ , where $$n$$ is the number of nodes in linked list. Since the problem is recursive, we need additional space to store the recursive call stack. The maximum depth of the recursion tree is $$\log n$$

---

#### Approach 2: Bottom Up Merge Sort

**Intuition**

The Top Down Approach for merge sort uses $$\mathcal{O}(\log n)$$ extra space due to recursive call stack. Let's understand how we can implement merge sort using constant extra space using Bottom Up Approach.

The Bottom Up approach for merge sort starts by splitting the problem into the smallest subproblem and iteratively merge the result to solve the original problem.
- First, the list is split into sublists of size 1 and merged iteratively in sorted order. The merged list is solved similarly.

- The process continues until we sort the entire list.

This approach is solved iteratively and can be implemented using constant extra space. Let's look at the algorithm to implement merge sort in Bottom Up Fashion.


**Algorithm**

Assume, $$n$$ is the number of nodes in the linked list.
- Start with splitting the list into sublists of size $$1$$. Each adjacent pair of sublists of size $$1$$ is merged in sorted order. After the first iteration, we get the sorted lists of size $$2$$. A similar process is repeated for a sublist of size $$2$$. In this way, we iteratively split the list into sublists of size $$1,2,4,8 ..$$ and so on until we reach $$n$$.

- To split the list into two sublists of given $$\text{size}$$ beginning from $$\text{start}$$, we use two pointers, $$\text{mid}$$ and $$\text{end}$$ that references to the start and end of second linked list respectively. The split process finds the middle of linked lists for the given $$\text{size}$$.

- Merge the lists in sorted order as discussed in  _Approach 1_

- As we iteratively split the list and merge, we have to keep track of the previous merged list using pointer $$\text{tail}$$ and the next sublist to be sorted using pointer $$\text{nextSubList}$$.

For the linked list = `[10,1,30,2,5]`, the following figure illustrates the merge sort process using a Bottom Up approach.

![img](../Figures/148/bottom_up_merge_sort.png)


<iframe src="https://leetcode.com/playground/KyrjGgDu/shared" frameBorder="0" width="100%" height="500" name="KyrjGgDu"></iframe>

**Complexity Analysis**

* Time Complexity: $$\mathcal{O}(n \log n)$$, where $$n$$ is the number of nodes in linked list.
 Let's analyze the time complexity of each step:

1) Count Nodes - Get the count of number nodes in the linked list requires $$\mathcal{O}(n)$$ time.

2) Split and Merge - This operation is similar to _Approach 1_ and takes  $$ \mathcal{O}(n \log n)$$ time.
For `n = 16`, the split and merge operation in Bottom Up fashion can be visualized as follows

![img](../Figures/148/bottom_up_time_complexity.png)

This gives us total time complexity as
$$\mathcal{O}(n) + \mathcal{O}(n \log n) = \mathcal{O}(n \log n)$$

* Space Complexity: $$\mathcal{O}(1)$$ We use only constant space for storing the reference pointers  $$\text{tail}$$ , $$\text{nextSubList}$$ etc.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java merge sort solution
- Author: jeantimex
- Creation Date: Wed Jul 08 2015 09:09:42 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 10:53:47 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
      
      public ListNode sortList(ListNode head) {
        if (head == null || head.next == null)
          return head;
            
        // step 1. cut the list to two halves
        ListNode prev = null, slow = head, fast = head;
        
        while (fast != null && fast.next != null) {
          prev = slow;
          slow = slow.next;
          fast = fast.next.next;
        }
        
        prev.next = null;
        
        // step 2. sort each half
        ListNode l1 = sortList(head);
        ListNode l2 = sortList(slow);
        
        // step 3. merge l1 and l2
        return merge(l1, l2);
      }
      
      ListNode merge(ListNode l1, ListNode l2) {
        ListNode l = new ListNode(0), p = l;
        
        while (l1 != null && l2 != null) {
          if (l1.val < l2.val) {
            p.next = l1;
            l1 = l1.next;
          } else {
            p.next = l2;
            l2 = l2.next;
          }
          p = p.next;
        }
        
        if (l1 != null)
          p.next = l1;
        
        if (l2 != null)
          p.next = l2;
        
        return l.next;
      }
    
    }
</p>


### Bottom-to-up(not recurring)  with o(1) space complextity and o(nlgn) time complextity
- Author: zdwu
- Creation Date: Wed Mar 18 2015 18:08:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 10:30:58 GMT+0800 (Singapore Standard Time)

<p>
this problem can be easily solved using recurrence and divide-and-conquer. But it consumes program stack to store the recurring function stack frame, actually it consumes o(lgn) space complexity. Recursion use up-to-bottom strategy , why not try the opposite way--bottom-to-up, luckily it works, it only consumes 0(1) space complexity and o(nlgn) time complextity.

    /**
     * Merge sort use bottom-up policy, 
     * so Space Complexity is O(1)
     * Time Complexity is O(NlgN)
     * stable sort
    */
    class Solution {
    public:
    	ListNode *sortList(ListNode *head) {
    		if(!head || !(head->next)) return head;
    		
    		//get the linked list's length
    		ListNode* cur = head;
    		int length = 0;
    		while(cur){
    			length++;
    			cur = cur->next;
    		}
    		
    		ListNode dummy(0);
    		dummy.next = head;
    		ListNode *left, *right, *tail;
    		for(int step = 1; step < length; step <<= 1){
    			cur = dummy.next;
    			tail = &dummy;
    			while(cur){
    				left = cur;
    				right = split(left, step);
    				cur = split(right,step);
    				tail = merge(left, right, tail);
    			}
    		}
    		return dummy.next;
    	}
    private:
    	/**
    	 * Divide the linked list into two lists,
         * while the first list contains first n ndoes
    	 * return the second list's head
    	 */
    	ListNode* split(ListNode *head, int n){
    		//if(!head) return NULL;
    		for(int i = 1; head && i < n; i++) head = head->next;
    		
    		if(!head) return NULL;
    		ListNode *second = head->next;
    		head->next = NULL;
    		return second;
    	}
    	/**
    	  * merge the two sorted linked list l1 and l2,
    	  * then append the merged sorted linked list to the node head
    	  * return the tail of the merged sorted linked list
    	 */
    	ListNode* merge(ListNode* l1, ListNode* l2, ListNode* head){
    		ListNode *cur = head;
    		while(l1 && l2){
    			if(l1->val > l2->val){
    				cur->next = l2;
    				cur = l2;
    				l2 = l2->next;
    			}
    			else{
    				cur->next = l1;
    				cur = l1;
    				l1 = l1->next;
    			}
    		}
    		cur->next = (l1 ? l1 : l2);
    		while(cur->next) cur = cur->next;
    		return cur;
    	}
    };
</p>


### c++ and java, legit solution. O(nlogn) time and O(1) space! No recursion! With detailed explaination
- Author: ChrisTrompf
- Creation Date: Mon Sep 03 2018 14:43:12 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 10:15:12 GMT+0800 (Singapore Standard Time)

<p>
# TL;DR
### Short and sweet. O(nlogn) time, O(1) space.
#### **cpp**
```cpp
  ListNode* sortList(ListNode* head)
    bool done = (!head);
    // Keep partitioning our list into bigger sublists length. Starting with a size of 1 and doubling each time
    for (int step = 1; !done; step *= 2) {
      done = true;
      ListNode** next_ptr = &head;
      ListNode* remaining = head;
      ListNode* list[2];
      do {
        // Split off two sublists of size step
        for (auto& sub_head : list) {
          sub_head = remaining;
          ListNode* tail = nullptr;
          for (int i = 0; i < step && remaining; ++i, remaining = remaining->next) {
            tail = remaining;
          }
          // Terminate our sublist
          if (tail) {
            tail->next = nullptr;
          }
        }

        // We\'re done if these are the first two sublists in this pass and they
        // encompass the entire primary list
        done &= !remaining;

        // If we have two sublists, merge them into one
        if (list[1]) {
          while (list[0] || list[1]) {
            int idx = (!list[1] || list[0] && list[0]->val <= list[1]->val) ? 0 : 1;
            *next_ptr = list[idx];
            list[idx] = list[idx]->next;
            next_ptr = &(**next_ptr).next;
          }

          // Terminate our new sublist
          *next_ptr = nullptr;
        } else {
          // Only a single sublist, no need to merge, just attach to the end
          *next_ptr = list[0];
        }
      } while (remaining);
    }
    return head;
  }
```	
#### **Java**
```java
  public ListNode sortList(ListNode head) {
    ListNode dummy = new ListNode(0);
    dummy.next = head;
    ListNode [] list = new ListNode[2];
    boolean done = (null == head);
    // Keep partitioning our list into bigger sublists length. Starting with a size of 1 and doubling each time
    for (int step = 1; !done; step *= 2) {
      done = true;
      ListNode prev = dummy;
      ListNode remaining = prev.next;
      do {
        // Split off two sublists of size step
        for (int i = 0; i < 2; ++i) {
          list[i] = remaining;
          ListNode tail = null;
          for (int j = 0; j < step && null != remaining; ++j, remaining = remaining.next) {
            tail = remaining;
          }
          // Terminate our sublist
          if (null != tail) {
            tail.next = null;
          }
        }

        // We\'re done if these are the first two sublists in this pass and they
        // encompass the entire primary list
        done &= (null == remaining);

        // If we have two sublists, merge them into one
        if (null != list[1]) {
          while (null != list[0] || null != list[1]) {
            int idx = (null == list[1] || null != list[0] && list[0].val <= list[1].val) ? 0 : 1;
            prev.next = list[idx];
            list[idx] = list[idx].next;
            prev = prev.next;
          }

          // Terminate our new sublist
          prev.next = null;
        } else {
          // Only a single sublist, no need to merge, just attach to the end
          prev.next = list[0];
        }
      } while (null != remaining);
    }
    return dummy.next;
  }
```		

### Using a holding buffer for extra speed. O(nlogn) time, O(1) space, with lower constant.
```cpp
  /// Merge two sorted lists into one, the head and tail of the new list is returned
  std::pair<ListNode*, ListNode*> merge_lists(ListNode* l1, ListNode* l2)
  {
    ListNode* head;
    ListNode* tail = nullptr;
    ListNode** next_ptr = &head;
    while (l1 || l2) {
      ListNode** list = (!l2 || l1 && l1->val <= l2->val) ? &l1 : &l2;
      *next_ptr = tail = *list;
      *list = (*list)->next;
      next_ptr = &(*next_ptr)->next;
    }
    *next_ptr = nullptr;
    return std::make_pair(head, tail);
  }

  /// Take a list and split it into sublists of the requested size, returns number sublists and remaining list
  std::pair<int, ListNode*> split_list(ListNode* head, int sz, ListNode* lists[], int num_lists)
  {
    int count = 0;
    while (head && count < num_lists) {
      lists[count++] = head;
      ListNode* list_tail = nullptr;
      for (int i = 0; i < sz && head; ++i, head = head->next) {
        list_tail = head;
      }
      if (list_tail) {
        list_tail->next = nullptr;
      }
    }
    return std::make_pair(count, head);
  }
  
  ListNode* sortList(ListNode* head)
  {
    // Working buffer size, must be an even number
    constexpr int buf_sz = 8;
    ListNode* buf[buf_sz];

    bool done = (!head);
    // Initially split list into sublists of size 1, then increase the size until
    // the entire list is sorted
    for (int step = 1; !done; step *= buf_sz) {
      done = true;
      int num;
      ListNode* remaining = head;
      ListNode* tail = nullptr;
      ListNode** next_ptr = &head;
      do {
        // Split the list into increasingly larger sublists
        std::tie(num, remaining) = split_list(remaining, step, buf, buf_sz);
      
        // We\'ll be done if this is the first split this pass and there is no more remaining
        done &= (!remaining);
        
        // Keep merging our sublists together until a single sorted list is made
        for (; 1 < num; num = (num + 1) / 2) {
          // Merge each pair of sublists
          for (int i = 0; i < num / 2; ++i) {
            std::tie(buf[i], tail) = merge_lists(buf[i * 2], buf[i * 2 + 1]);
          }
          
          // If there is an odd number of lists, just move the last one. It will get
          // merged next time around
          if (num % 2) {
            buf[num / 2] = buf[num - 1];
          }
        }
        *next_ptr = buf[0];
        next_ptr = &(tail->next);
      } while (remaining);
    }

    return head;
  }
```
### Short quicksort for fun. O(nlogn) time, O(logn) stack space.
```cpp
  ListNode* sortList(ListNode* head, ListNode* tail = nullptr)
  {
    if (head != tail) {
      // Use head node as the pivot node
      // Everything in the _smaller_ list will be less than _head_
      // and everything appearing after _head_ in the list is greater or equal
      ListNode* smaller;
      ListNode** smaller_next = &smaller;
      for (ListNode** prev = &head->next; *prev != tail; ) {
        if (head->val > (**prev).val) {
          *smaller_next = *prev;
          smaller_next = &((**smaller_next).next);

          // Remove smaller node from original list
          *prev = (**prev).next;
        } else {
          // Correct position, skip over
          prev = &((**prev).next);
        }
      }

      // Connect the end of smaller list to the head (which is the partition node)
      // We now have. [smaller list...] -> head -> [larger list]
      *smaller_next = head;

      // Continue to sort everything after head
      head->next = sortList(head->next, tail);

      // Sort everything upto head
      head = sortList(smaller, head);
    }
    return head;
  }
```	
# Details
### **Basic description**
The problem calls for O(1) space. Therefore a solution that does not use recursion is required. Quicksort is out, merge sort is in.
The idea is to merge progressively larger sublists together until the resulting list is sorted. A walkthough will help explain.

Imagine the input list [3, 45, 2, 15, 37, 19, 39, 20], first process the list as sublists of length _1_, [3], [45], [2], [15], [37], [19], [39], [20]. As they are size _1_, they are obviously sorted sublists. Merge each pair of sublists together to produce 4 sorted sublists of size _2_ [3, 45], [2, 15], [19, 37], [20, 39], these are reassemble into the original list to produce [3, 45, 2, 15, 19, 37, 20, 39]. Increasing the step size to _2_ and repeat. This produces the following table;
|Progress|Step size|Sublists|Merged|
|---|---|---|---|
|[3, 45, 2, 15, 37, 19, 39, 20]|1|[3], [45], [2], [15], [37], [19], [39], [20]|[3, 45], [2, 15], [19, 37], [20, 39]|
|[3, 45, 2, 15, 19, 37, 20, 39]|2|[3, 45], [2, 15], [19, 37], [20, 39]|[2, 3, 15, 45], [19, 20, 37, 39]|
|[2, 3, 15, 45, 19, 20, 37, 39]|4|[2, 3, 15, 45], [19, 20, 37, 39]|[2, 3, 15, 19, 20, 37, 39, 45]|

Since recursion is out, the process must be done inplace as we go. Basically for each pass;
1. Grab two sorted lists of size _step_
2. Merge the two lists into a single sorted list of size _step * 2_ and reattach to input list
3. Repeat from step 1. until entire list has been exhausted

This produces the first relatively compact solution shown above.
### **Complexity**
#### **Space complexity**
There are only a few fixed, stack allocated, variables whose creation is not based on the length of the input list and there is no recursion. Therefore it is O(1) space complexity.
#### **Time complexity**
The list will be completely sorted once _step_ * 2 becomes greater than _n_, with _step_ doubling each pass (1, 2, 4, 8, ...). Therefore it will take logn passes to sort the list. Each pass though requires _n_ moves to produce the sublists and the merge of two sorted lists takes _step_ moves for _n/step_ sublists. Therefore it takes _n_ time for each of the logn passes. Therefore the time complexity is O(nlogn).
### **Improvements**
In practice, in the real world no one would implement the initial solution as is, instead it would be improved substansually (without impacting space complexity), by using a small holding buffer. Allowing for larger sorted sublists to be created with each full pass of the list. This is the second solution presented above.

Consider the first solution, the output of each pass produces the input of the next pass. For example, the first pass, when _step_ is 1, produces sublists of _step * 2_, which is exactly what is needed for the next pass. However, there is nowhere to hold this in O(1) space so we must reassemble the list fully ready for the next pass. However, with a small working buffer we can leaverage this and focus on sorting blocks of nodes. By that I mean, we grab _buf_sz_ sublists, and keep merging them together until we have a single, sorted, sublist in our holding buffer. The sublist is then linked back into the input list before progressing onto the next set of _buf_sz_ sublists.

With a working buffer of 4, using the input [6, 38, 25, 46, 45, 90, 97, 52, 75, 18] an initial pass would progress as follows;
|Progress|State 1, grab 4 sublists of _step_ size (1)|State 2, merge into 2 sublists|State 3, merge into 1 sublist|
|---|---|---|---|---|
|[6, 38, 25, 46, 45, 90, 97, 52, 75, 18]|[6], [38], [25], [46], [45, 90, 97, 52, 75, 18]|[6, 38], [25, 46], [45, 90, 97, 52, 75, 18]|[6, 25, 38, 46], [45, 90, 97, 52, 75, 18]|
|[6, 25, 38, 46, 45, 90, 97, 52, 75, 18]|[6, 25, 38, 46], [45], [90], [97], [52], [75, 18]|[6, 25, 38, 46], [45, 90], [52, 97], [75, 18]|[6, 25, 38, 46], [45, 52, 90, 97], [75, 18]|
|[6, 25, 38, 46, 45, 52, 90, 97, 75, 18]|[6, 25, 38, 46, 45, 52, 90, 97], [75], [18]|[6, 25, 38, 46, 45, 52, 90, 97], [18, 75]|[6, 25, 38, 46, 45, 52, 90, 97], [18, 75]
The end of the list has been reached, so _step_ is increased, but this time, instead of doubling, it is multiplied by 4, as we now know every 4 nodes forms a sorted sublist. The next pass then becomes;
|Progress|State 1, grab 4 sublists of _step_ size (4)|State 2, merge into 2 sublists|State 3, merge into 1 sublist|
|---|---|---|---|---|
|[6, 25, 38, 46, 45, 52, 90, 97, 18, 75]|[6, 25, 38, 46], [45, 52, 90, 97], [18, 75]|[6, 25, 38, 45, 46, 52, 90, 97], [18, 75]|[6, 18, 25, 38, 45, 46, 52, 75, 90, 97]|
Finished!
Increasing the buffer to 8 or 16 makes a huge difference as we will be sorting blocks of nodes as follows;
|Buffer size|Pass 1|Pass 2|Pass 3|
|---|---|---|---|
|2|2|4|8|
|4|4|16|64|
|8|8|64|512|
|16|16|256|4096|
So after 3 passes of the list, with a buffer size of 8, we will have divided the input list into sorted sublists of size 512. Where as with buffer size of 2, we will only have sublists of size 8. Making processing a huge lists considerably faster.
## **Quicksort**
I added the quicksort solution for fun as it is quite compact, even if it doesn\'t fullfil the requirements of O(1) space. It does of course suffer from the normal problems of quicksort, that being a worst case of O(n^n) if the input list is already sorted.

Basically, grab the _head_ of the list and use it as a pivot. Then using code adopted from [partition list](https://leetcode.com/problems/partition-list/discuss/155293/Short-single-pass-iterative-c++-solution.-No-allocation-or-dummy-required.-O(n)-time-O(1)-space), divide into two sublists, those nodes less than _head->val_ and those greater or equal. Recurse around, partitioning those lists around a new pivot. Rince and repeat until each sublist is down to a size of 1 and is hence sorted.

I did it more for fun as the code is really small. Without comments it is;
```cpp
  ListNode* sortList(ListNode* head, ListNode* tail = nullptr)
  {
    if (head != tail) {
      ListNode* smaller;
      ListNode** smaller_next = &smaller;
      for (ListNode** prev = &head->next; *prev != tail; ) {
        if (head->val > (**prev).val) {
          *smaller_next = *prev;
          smaller_next = &((**smaller_next).next);
          *prev = (**prev).next;
        } else {
          prev = &((**prev).next);
        }
      }

      *smaller_next = head;
      head->next = sortList(head->next, tail);
      head = sortList(smaller, head);
    }
    return head;
  }
```	

**Please give me a thumbs up if this helped explain this problem for you**
</p>


