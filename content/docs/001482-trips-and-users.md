---
title: "Trips and Users"
weight: 1482
#id: "trips-and-users"
---
## Description
<div class="description">
<p>The <code>Trips</code> table holds all taxi trips. Each trip has a unique Id, while Client_Id and Driver_Id are both foreign keys to the Users_Id at the <code>Users</code> table. Status is an ENUM type of (&lsquo;completed&rsquo;, &lsquo;cancelled_by_driver&rsquo;, &lsquo;cancelled_by_client&rsquo;).</p>

<pre>
+----+-----------+-----------+---------+--------------------+----------+
| Id | Client_Id | Driver_Id | City_Id |        Status      |Request_at|
+----+-----------+-----------+---------+--------------------+----------+
| 1  |     1     |    10     |    1    |     completed      |2013-10-01|
| 2  |     2     |    11     |    1    | cancelled_by_driver|2013-10-01|
| 3  |     3     |    12     |    6    |     completed      |2013-10-01|
| 4  |     4     |    13     |    6    | cancelled_by_client|2013-10-01|
| 5  |     1     |    10     |    1    |     completed      |2013-10-02|
| 6  |     2     |    11     |    6    |     completed      |2013-10-02|
| 7  |     3     |    12     |    6    |     completed      |2013-10-02|
| 8  |     2     |    12     |    12   |     completed      |2013-10-03|
| 9  |     3     |    10     |    12   |     completed      |2013-10-03| 
| 10 |     4     |    13     |    12   | cancelled_by_driver|2013-10-03|
+----+-----------+-----------+---------+--------------------+----------+
</pre>

<p>The <code>Users</code> table holds all users. Each user has an unique Users_Id, and Role is an ENUM type of (&lsquo;client&rsquo;, &lsquo;driver&rsquo;, &lsquo;partner&rsquo;).</p>

<pre>
+----------+--------+--------+
| Users_Id | Banned |  Role  |
+----------+--------+--------+
|    1     |   No   | client |
|    2     |   Yes  | client |
|    3     |   No   | client |
|    4     |   No   | client |
|    10    |   No   | driver |
|    11    |   No   | driver |
|    12    |   No   | driver |
|    13    |   No   | driver |
+----------+--------+--------+
</pre>

<p>Write a SQL query to find the cancellation rate of requests made by unbanned users (both client and driver must be unbanned)&nbsp;between <strong>Oct 1, 2013</strong> and <strong>Oct 3, 2013</strong>. The cancellation rate is computed by dividing the number of canceled (by client or driver) requests&nbsp;made by unbanned users by the total number of requests&nbsp;made by unbanned users.</p>

<p>For the above tables, your SQL query should return the following rows with the cancellation rate being rounded to <em>two</em> decimal places.</p>

<pre>
+------------+-------------------+
|     Day    | Cancellation Rate |
+------------+-------------------+
| 2013-10-01 |       0.33        |
| 2013-10-02 |       0.00        |
| 2013-10-03 |       0.50        |
+------------+-------------------+
</pre>

<p><strong>Credits:</strong><br />
Special thanks to <a href="https://leetcode.com/discuss/user/cak1erlizhou">@cak1erlizhou</a> for contributing this question, writing the problem description and adding part of the test cases.</p>

</div>

## Tags


## Companies
- Amazon - 6 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Solution without join
- Author: ndt93
- Creation Date: Sun Apr 10 2016 01:08:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 08:30:41 GMT+0800 (Singapore Standard Time)

<p>
    SELECT Request_at as Day,
           ROUND(COUNT(IF(Status != 'completed', TRUE, NULL)) / COUNT(*), 2) AS 'Cancellation Rate'
    FROM Trips
    WHERE (Request_at BETWEEN '2013-10-01' AND '2013-10-03')
          AND Client_id NOT IN (SELECT Users_Id FROM Users WHERE Banned = 'Yes')
    GROUP BY Request_at;
</p>


### Sharing my solution,
- Author: simplyida
- Creation Date: Tue Aug 18 2015 15:48:49 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 20 2018 22:56:29 GMT+0800 (Singapore Standard Time)

<p>
    select 
    t.Request_at Day, 
    round(sum(case when t.Status like 'cancelled_%' then 1 else 0 end)/count(*),2) Rate
    from Trips t 
    inner join Users u 
    on t.Client_Id = u.Users_Id and u.Banned='No'
    where t.Request_at between '2013-10-01' and '2013-10-03'
    group by t.Request_at
</p>


### [MySQL] *UPDATED* solutions. Both explained. Easy and simple
- Author: SkandaB
- Creation Date: Tue Jul 03 2018 22:18:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 01:00:54 GMT+0800 (Singapore Standard Time)

<p>
### Solution for the updated question

```
-- Updated query when both \'Client\' and \'Driver\' should be unbanned

SELECT  t.request_at AS "Day",
        ROUND(
            COUNT(CASE
                    WHEN t.status != \'completed\' THEN 1     -- numerator is total cancelled trips
                    ELSE NULL
                 END) / COUNT(id)                           -- denominator is all trips for that day
        , 2) AS "Cancellation Rate"
FROM    trips AS t
JOIN    users AS client                                     -- users table role-playing as client
ON      t.client_id = client.users_id
AND     client.banned = \'No\'                                -- unbanned client
JOIN    users AS driver                                     -- users table role-playing as driver
ON      driver.users_id = t.driver_id
AND     driver.banned = \'No\'                                -- unbanned driver
AND     t.request_at BETWEEN \'2013-10-01\' 
                         AND \'2013-10-03\'
GROUP BY t.request_at;                                      -- per date calculation
```

----

### solution for the old question
----

The where condition checks for two things.
1. The current client is not \'Banned\'
2. Date is between the range

The second cluase (column) of SELECT statement is tricky. Let\'s understand what each function is used for
* LOWER(column) to change the values to lowercase. This is because MySQL doesn\'t have ILIKE function to have case-insensitive comparision
* LIKE for partial text matches
* CASE for counting only cancelled rides instead of all non-null values
* 1.000 to account for decimals after divisions
* COUNT(id) to get total trips for that day
* ROUND(value, 2) to round off the result to 2 decimal places **Do not confuse with TRUNCATE function**
* Rename the processed column to the expected one


```
SELECT      request_at AS "Day", 
            ROUND(((SUM(CASE WHEN LOWER(Status) LIKE "cancelled%" THEN 1.000 ELSE 0 END)) / COUNT(id)), 2) AS "Cancellation Rate" 
FROM        trips
WHERE       client_id NOT IN (SELECT users_id FROM users WHERE banned = \'Yes\')
AND         request_at BETWEEN \'2013-10-01\' AND \'2013-10-03\'
GROUP BY    request_at;
```

***This solution is faster than the above one***
We are joining and hence each record doesn\'t need to run an inner query to check for the banned status. If it did, it takes a lot of time repeating the same task over and over.

```
SELECT      request_at AS "Day", 
            ROUND(((SUM(CASE WHEN LOWER(Status) LIKE "cancelled%" THEN 1.000 ELSE 0 END)) / COUNT(id)), 2) AS "Cancellation Rate" 
FROM        trips AS t
JOIN        users AS u
ON          t.client_id = u.users_id
AND         u.banned =\'No\'
WHERE       request_at BETWEEN \'2013-10-01\' AND \'2013-10-03\'
GROUP BY    request_at;
```
</p>


