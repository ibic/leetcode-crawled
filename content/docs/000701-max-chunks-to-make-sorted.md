---
title: "Max Chunks To Make Sorted"
weight: 701
#id: "max-chunks-to-make-sorted"
---
## Description
<div class="description">
<p>Given an array <code>arr</code> that is a permutation of <code>[0, 1, ..., arr.length - 1]</code>, we split the array into some number of &quot;chunks&quot; (partitions), and individually sort each chunk.&nbsp; After concatenating them,&nbsp;the result equals the sorted array.</p>

<p>What is the most number of chunks we could have made?</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [4,3,2,1,0]
<strong>Output:</strong> 1
<strong>Explanation:</strong>
Splitting into two or more chunks will not return the required result.
For example, splitting into [4, 3], [2, 1, 0] will result in [3, 4, 0, 1, 2], which isn&#39;t sorted.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,0,2,3,4]
<strong>Output:</strong> 4
<strong>Explanation:</strong>
We can split into two chunks, such as [1, 0], [2, 3, 4].
However, splitting into [1, 0], [2], [3], [4] is the highest number of chunks possible.
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li><code>arr</code> will have length in range <code>[1, 10]</code>.</li>
	<li><code>arr[i]</code> will be a permutation of <code>[0, 1, ..., arr.length - 1]</code>.</li>
</ul>

<p>&nbsp;</p>

</div>

## Tags
- Array (array)

## Companies
- Google - 4 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Brute Force [Accepted]

**Intuition and Algorithm**

Let's try to find the smallest left-most chunk.  If the first `k` elements are `[0, 1, ..., k-1]`, then it can be broken into a chunk, and we have a smaller instance of the same problem.

We can check whether `k+1` elements chosen from `[0, 1, ..., n-1]` are `[0, 1, ..., k]` by checking whether the maximum of that choice is `k`.

<iframe src="https://leetcode.com/playground/T88N5oTT/shared" frameBorder="0" width="100%" height="225" name="T88N5oTT"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `arr`

* Space Complexity: $$O(1)$$.

---

For more approaches, please visit the article for the companion problem [Max Chunks To Make Sorted II](https://leetcode.com/articles/max-chunks-to-make-sorted-ii/).

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java O(n) Solution with detailed explanation
- Author: lily4ever
- Creation Date: Sun Jan 21 2018 12:02:17 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 05:55:32 GMT+0800 (Singapore Standard Time)

<p>
The basic idea is to use max[] array to keep track of the max value until the current position, and compare it to the sorted array (indexes from 0 to arr.length - 1). If the max[i] equals the element at index i in the sorted array, then the final count++.

Update: As @AF8EJFE pointed out, the numbers range from 0 to arr.length - 1. So there is no need to sort the arr, we can simply use the index for comparison. Now this solution is even more straightforward with O(n) time complelxity.

For example,
```
original: 0, 2, 1, 4, 3, 5, 7, 6
max:      0, 2, 2, 4, 4, 5, 7, 7
sorted:   0, 1, 2, 3, 4, 5, 6, 7
index:    0, 1, 2, 3, 4, 5, 6, 7
```
The chunks are: 0 | 2, 1 | 4, 3 | 5 | 7, 6

```
    public int maxChunksToSorted(int[] arr) {
        if (arr == null || arr.length == 0) return 0;
        
        int[] max = new int[arr.length];
        max[0] = arr[0];
        for (int i = 1; i < arr.length; i++) {
            max[i] = Math.max(max[i - 1], arr[i]);
        }
        
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (max[i] == i) {
                count++;
            }
        }
        
        return count;
    }
```

Update2:
The code can be further simplified as follows. 
```
    public int maxChunksToSorted(int[] arr) {
        if (arr == null || arr.length == 0) return 0;
        
        int count = 0, max = 0;
        for (int i = 0; i < arr.length; i++) {
            max = Math.max(max, arr[i]);
            if (max == i) {
                count++;
            }
        }
        
        return count;
    }
```

The solution to Ver2 is very similar, with only slight modification: 
https://discuss.leetcode.com/topic/117855/simple-java-solution-with-explanation
</p>


### Java solution, left max and right min.
- Author: shawngao
- Creation Date: Sun Jan 21 2018 12:14:20 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 05:27:14 GMT+0800 (Singapore Standard Time)

<p>
Algorithm: Iterate through the array, each time all elements to the left are smaller (or equal) to all elements to the right, there is a new chunck.
Use two arrays to store the left max and right min to achieve O(n) time complexity. Space complexity is O(n) too.
This algorithm can be used to solve ver2 too.
```
class Solution {
    public int maxChunksToSorted(int[] arr) {
        int n = arr.length;
        int[] maxOfLeft = new int[n];
        int[] minOfRight = new int[n];

        maxOfLeft[0] = arr[0];
        for (int i = 1; i < n; i++) {
            maxOfLeft[i] = Math.max(maxOfLeft[i-1], arr[i]);
        }

        minOfRight[n - 1] = arr[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            minOfRight[i] = Math.min(minOfRight[i + 1], arr[i]);
        }

        int res = 0;
        for (int i = 0; i < n - 1; i++) {
            if (maxOfLeft[i] <= minOfRight[i + 1]) res++;
        }

        return res + 1;
    }
}
```
</p>


### [Python] Easy Understood Solution
- Author: lee215
- Creation Date: Sun Jan 21 2018 12:10:08 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Dec 18 2019 23:38:45 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**:
Iterate the array, if the `max(A[0] ~ A[i]) = i`,
then we can split the array into two chunks at this index.
<br>

## **Time Complexity**:
Timen `O(N)`
Space `O(1)`
<br>

```py
    def maxChunksToSorted(self, arr):
        curMax, res = -1, 0
        for i, v in enumerate(arr):
            curMax = max(curMax, v)
            res += curMax == i
        return res
```
<br>

Make it in 1 line by reduce:
```py
    def maxChunksToSorted(self, A):
        return reduce(lambda (m, res), (i, v): (max(m, v), res + (max(m, v) == i)), enumerate(A), (-1, 0))[1]
```
<br>

Another 1 linear solution, `O(N^2)` where `N <= 10`
```py
    def maxChunksToSorted(self, A):
        return sum(max(A[:i + 1]) == i for i in range(len(A)))
```
</p>


