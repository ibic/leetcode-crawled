---
title: "ZigZag Conversion"
weight: 6
#id: "zigzag-conversion"
---
## Description
<div class="description">
<p>The string <code>&quot;PAYPALISHIRING&quot;</code> is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)</p>

<pre>
P   A   H   N
A P L S I I G
Y   I   R
</pre>

<p>And then read line by line: <code>&quot;PAHNAPLSIIGYIR&quot;</code></p>

<p>Write the code that will take a string and make this conversion given a number of rows:</p>

<pre>
string convert(string s, int numRows);
</pre>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;PAYPALISHIRING&quot;, numRows = 3
<strong>Output:</strong> &quot;PAHNAPLSIIGYIR&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;PAYPALISHIRING&quot;, numRows = 4
<strong>Output:</strong> &quot;PINALSIGYAHRPI&quot;
<strong>Explanation:</strong>
P     I    N
A   L S  I G
Y A   H R
P     I
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;A&quot;, numRows = 1
<strong>Output:</strong> &quot;A&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 1000</code></li>
	<li><code>s</code> consists of English letters (lower-case and upper-case), <code>&#39;,&#39;</code> and <code>&#39;.&#39;</code>.</li>
	<li><code>1 &lt;= numRows &lt;= 1000</code></li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Amazon - 5 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---

#### Approach 1: Sort by Row

**Intuition**

By iterating through the string from left to right, we can easily determine which row in the Zig-Zag pattern that a character belongs to.

**Algorithm**

We can use $$\text{min}( \text{numRows}, \text{len}(s))$$ lists to represent the non-empty rows of the Zig-Zag Pattern.

Iterate through $$s$$ from left to right, appending each character to the appropriate row. The appropriate row can be tracked using two variables: the current row and the current direction.

The current direction changes only when we moved up to the topmost row or moved down to the bottommost row.

<iframe src="https://leetcode.com/playground/F7ATKV4h/shared" frameBorder="0" width="100%" height="446" name="F7ATKV4h"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(n)$$, where $$n == \text{len}(s)$$
* Space Complexity: $$O(n)$$

<br/>

---

#### Approach 2: Visit by Row

**Intuition**

Visit the characters in the same order as reading the Zig-Zag pattern line by line.

**Algorithm**

Visit all characters in row 0 first, then row 1, then row 2, and so on...

For all whole numbers $$k$$,

- Characters in row $$0$$ are located at indexes $$k \; (2 \cdot \text{numRows} - 2)$$
- Characters in row $$\text{numRows}-1$$ are located at indexes $$k \; (2 \cdot \text{numRows} - 2) + \text{numRows} - 1$$
- Characters in inner row $$i$$ are located at indexes $$k \; (2 \cdot \text{numRows}-2)+i$$ and $$(k+1)(2 \cdot \text{numRows}-2)- i$$.

<iframe src="https://leetcode.com/playground/Deg3hGi4/shared" frameBorder="0" width="100%" height="395" name="Deg3hGi4"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(n)$$, where $$n == \text{len}(s)$$. Each index is visited once.
* Space Complexity: $$O(n)$$. For the cpp implementation, $$O(1)$$ if return string is not considered extra space.

## Accepted Submission (java)
```java
class Solution {
    public String convert(String s, int numRows) {
        if (numRows <= 1) {
            return s;
        }
        int n = s.length();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numRows; i++) {
            if (i % numRows == 0 || i % (numRows - 1) == 0) {
                for (int j = 0; ; j++) {
                    int a = i + j * (2 * numRows - 2);
                    if (a >= n) {
                       break;
                    }
                    sb.append(s.charAt(a));
                }
            } else {
                for (int j = 0; ; j++) {
                    int a = i + j * (2 * numRows - 2);
                    if (a >= n) {
                        break;
                    }
                    sb.append(s.charAt(a));
                    int b = (2 * numRows - 2) * (j + 1) - i;
                    if (b >= n) {
                        break;
                    }
                    sb.append(s.charAt(b));
                }
            }
        }
        return sb.toString();
    }
}
```

## Top Discussions
### Easy to understand Java solution
- Author: dylan_yu
- Creation Date: Sat Sep 06 2014 16:05:15 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 11:49:44 GMT+0800 (Singapore Standard Time)

<p>
Create nRows StringBuffers, and keep collecting characters from original string to corresponding StringBuffer. Just take care of your index to keep them in bound.

    public String convert(String s, int nRows) {
        char[] c = s.toCharArray();
        int len = c.length;
        StringBuffer[] sb = new StringBuffer[nRows];
        for (int i = 0; i < sb.length; i++) sb[i] = new StringBuffer();
        
        int i = 0;
        while (i < len) {
            for (int idx = 0; idx < nRows && i < len; idx++) // vertically down
                sb[idx].append(c[i++]);
            for (int idx = nRows-2; idx >= 1 && i < len; idx--) // obliquely up
                sb[idx].append(c[i++]);
        }
        for (int idx = 1; idx < sb.length; idx++)
            sb[0].append(sb[idx]);
        return sb[0].toString();
    }
</p>


### If you are confused with zigzag pattern,come and see!
- Author: HelloKenLee
- Creation Date: Mon Aug 31 2015 00:05:41 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 08:27:50 GMT+0800 (Singapore Standard Time)

<p>
    /*n=numRows
    \u0394=2n-2    1                           2n-1                         4n-3
    \u0394=        2                     2n-2  2n                    4n-4   4n-2
    \u0394=        3               2n-3        2n+1              4n-5       .
    \u0394=        .           .               .               .            .
    \u0394=        .       n+2                 .           3n               .
    \u0394=        n-1 n+1                     3n-3    3n-1                 5n-5
    \u0394=2n-2    n                           3n-2                         5n-4
    */
that's the zigzag pattern the question asked!
Be careful with nR=1 && nR=2

----------


----------


----------


----------


----------


----------


----------


----------


my 16ms code in c++:

    class Solution {
    public:
        string convert(string s, int numRows) {
            string result="";
            if(numRows==1)
    			return s;
            int step1,step2;
            int len=s.size();
            for(int i=0;i<numRows;++i){
                step1=(numRows-i-1)*2;
                step2=(i)*2;
                int pos=i;
                if(pos<len)
                    result+=s.at(pos);
                while(1){
                    pos+=step1;
                    if(pos>=len)
                        break;
    				if(step1)
    					result+=s.at(pos);
                    pos+=step2;
                    if(pos>=len)
                        break;
    				if(step2)
    					result+=s.at(pos);
                }
            }
            return result;
        }
    };
</p>


### Python O(n) Solution in 96ms (99.43%)
- Author: pharrellyhy
- Creation Date: Mon Jan 18 2016 17:17:24 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 00:57:32 GMT+0800 (Singapore Standard Time)

<p>
    class Solution(object):
        def convert(self, s, numRows):
            """
            :type s: str
            :type numRows: int
            :rtype: str
            """
            if numRows == 1 or numRows >= len(s):
                return s
    
            L = [''] * numRows
            index, step = 0, 1
    
            for x in s:
                L[index] += x
                if index == 0:
                    step = 1
                elif index == numRows -1:
                    step = -1
                index += step
    
            return ''.join(L)
</p>


