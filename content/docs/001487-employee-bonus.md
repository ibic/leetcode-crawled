---
title: "Employee Bonus"
weight: 1487
#id: "employee-bonus"
---
## Description
<div class="description">
<p>Select all employee&#39;s name and bonus whose bonus is &lt; 1000.</p>

<p>Table:<code>Employee </code></p>

<pre>
+-------+--------+-----------+--------+
| empId |  name  | supervisor| salary |
+-------+--------+-----------+--------+
|   1   | John   |  3        | 1000   |
|   2   | Dan    |  3        | 2000   |
|   3   | Brad   |  null     | 4000   |
|   4   | Thomas |  3        | 4000   |
+-------+--------+-----------+--------+
empId is the primary key column for this table.
</pre>

<p>Table: <code>Bonus</code></p>

<pre>
+-------+-------+
| empId | bonus |
+-------+-------+
| 2     | 500   |
| 4     | 2000  |
+-------+-------+
empId is the primary key column for this table.
</pre>

<p>Example ouput:</p>

<pre>
+-------+-------+
| name  | bonus |
+-------+-------+
| John  | null  |
| Dan   | 500   |
| Brad  | null  |
+-------+-------+
</pre>

</div>

## Tags


## Companies
- Netsuite - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `OUTER JOIN` and `WHERE` clause [Accepted]

**Intuition**

Join table **Employee** with **Bonus** and then use `WHERE` clause to get the required records.

**Algorithm**

Since foreign key **Bonus.empId** refers to **Employee.empId** and some employees do not have bonus records, we can use `OUTER JOIN` to link these two tables as the first step.

```sql
SELECT
    Employee.name, Bonus.bonus
FROM
    Employee
        LEFT OUTER JOIN
    Bonus ON Employee.empid = Bonus.empid
;
```
>Note: "LEFT OUTER JOIN" could be written as "LEFT JOIN".

The output to run this code with the sample data is as below.

```
| name   | bonus |
|--------|-------|
| Dan    | 500   |
| Thomas | 2000  |
| Brad   |       |
| John   |       |
```
The bonus value for 'Brad' and 'John' is empty, which is actually `NULL` in the database. "Conceptually, NULL means “a missing unknown value” and it is treated somewhat differently from other values." Check the [Working with NULL Values](https://dev.mysql.com/doc/refman/5.7/en/working-with-null.html) in MySQL manual for more details. In addition, we have to use `IS NULL` or `IS NOT NULL` to compare a value with `NULL`.

At last, we can add a `WHERE` clause with the proper conditions to filter these records.

**MySQL**

```sql
SELECT
    Employee.name, Bonus.bonus
FROM
    Employee
        LEFT JOIN
    Bonus ON Employee.empid = Bonus.empid
WHERE
    bonus < 1000 OR bonus IS NULL
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted Solution
- Author: BKB_Sven
- Creation Date: Fri May 05 2017 22:12:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 03:25:33 GMT+0800 (Singapore Standard Time)

<p>
**SELECT** name, bonus
**FROM** Employee **LEFT** **JOIN** Bonus
**ON** Employee.empID=Bonus.empID
**WHERE** bonus<1000 **OR** bonus **IS** **NULL**
</p>


### Definition
- Author: JackHo327
- Creation Date: Fri Aug 18 2017 04:08:06 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 18 2017 04:08:06 GMT+0800 (Singapore Standard Time)

<p>
Maybe the content could be changed from 

"Select all employee's name and bonus whose bonus is < 1000."

to

"Select all employee's name and bonus whose bonus is < 1000. (People have no bonus records are also needed to be considered)"
</p>


### MySQL solution (LEFT JOIN)
- Author: ye15
- Creation Date: Mon Nov 11 2019 02:09:25 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 11 2019 02:09:25 GMT+0800 (Singapore Standard Time)

<p>

```
SELECT name, bonus
FROM Employee LEFT JOIN Bonus USING(empId)
WHERE COALESCE(bonus, 0) < 1000; 
```
</p>


