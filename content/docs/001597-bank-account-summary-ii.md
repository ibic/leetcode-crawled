---
title: "Bank Account Summary II"
weight: 1597
#id: "bank-account-summary-ii"
---
## Description
<div class="description">
<p>Table: <code>Users</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| account      | int     |
| name         | varchar |
+--------------+---------+
account is the primary key for this table.
Each row of this table contains the account number of each user in the bank.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Transactions</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| trans_id      | int     |
| account       | int     |
| amount        | int     |
| transacted_on | date    |
+---------------+---------+
trans_id is the primary key for this table.
Each row of this table contains all changes made to all accounts.
amount is positive if the user received money and negative if they transferred money.
All accounts start with a balance 0.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to report the name and balance of users with a balance higher than 10000. The balance of an account is equal to the sum of the&nbsp;amounts of all transactions involving that account.</p>

<p>Return the result table in <strong>any</strong> order.</p>

<p>The query result format is in the following example.</p>

<p>&nbsp;</p>

<pre>
<code>Users</code> table:
+------------+--------------+
| account    | name         |
+------------+--------------+
| 900001     | Alice        |
| 900002     | Bob          |
| 900003     | Charlie      |
+------------+--------------+

<code>Transactions</code> table:
+------------+------------+------------+---------------+
| trans_id   | account    | amount     | transacted_on |
+------------+------------+------------+---------------+
| 1          | 900001     | 7000       |  2020-08-01   |
| 2          | 900001     | 7000       |  2020-09-01   |
| 3          | 900001     | -3000      |  2020-09-02   |
| 4          | 900002     | 1000       |  2020-09-12   |
| 5          | 900003     | 6000       |  2020-08-07   |
| 6          | 900003     | 6000       |  2020-09-07   |
| 7          | 900003     | -4000      |  2020-09-11   |
+------------+------------+------------+---------------+

Result table:
+------------+------------+
| <code>name    </code>   | <code>balance  </code>  |
+------------+------------+
| Alice      | 11000      |
+------------+------------+
Alice&#39;s balance is (7000 + 7000 - 3000) = 11000.
Bob&#39;s balance is 1000.
Charlie&#39;s balance is (6000 + 6000 - 4000) = 8000.
</pre>
</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### straight forward mysql solution
- Author: chrisg
- Creation Date: Fri Sep 18 2020 07:58:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 18 2020 07:58:50 GMT+0800 (Singapore Standard Time)

<p>
```
select
    a.name,
    sum(b.amount) balance
from
    Users a
join
    Transactions b
on
    a.account = b.account
group by
    a.account
having
    balance > 10000;
```
</p>


### MySQL with CTE faster 91%
- Author: GloriaChen8
- Creation Date: Thu Sep 24 2020 10:10:42 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 24 2020 10:10:42 GMT+0800 (Singapore Standard Time)

<p>
```
with tmp as(
select t.account, u.name, sum(amount) as balance
from Transactions t
left join Users u on t.account = u.account
group by account )

select name, balance
from tmp
where balance > 10000
```
</p>


### MySQL faster than 100%
- Author: yasheng
- Creation Date: Fri Sep 18 2020 15:14:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 20 2020 12:01:16 GMT+0800 (Singapore Standard Time)

<p>
```
select name, sum(amount) balance
from users
left join transactions
using(account)
group by account
having balance > 10000
```
</p>


