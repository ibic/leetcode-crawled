---
title: "N-Repeated Element in Size 2N Array"
weight: 911
#id: "n-repeated-element-in-size-2n-array"
---
## Description
<div class="description">
<p>In a array <code>A</code> of size <code>2N</code>, there are <code>N+1</code> unique elements, and exactly one of these elements is repeated <code>N</code> times.</p>

<p>Return the element repeated <code>N</code> times.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,2,3,3]</span>
<strong>Output: </strong><span id="example-output-1">3</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[2,1,2,5,3,2]</span>
<strong>Output: </strong><span id="example-output-2">2</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[5,1,5,2,5,3,5,4]</span>
<strong>Output: </strong><span id="example-output-3">5</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>4 &lt;= A.length &lt;= 10000</code></li>
	<li><code>0 &lt;= A[i] &lt; 10000</code></li>
	<li><code>A.length</code> is even</li>
</ul>
</div>
</div>
</div>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Apple - 2 (taggedByAdmin: false)
- akamai - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Count

**Intuition and Algorithm**

Let's count the number of elements.  We can use a `HashMap` or an array - here, we use a `HashMap`.

After, the element with a count larger than 1 must be the answer.

<iframe src="https://leetcode.com/playground/t2rozZws/shared" frameBorder="0" width="100%" height="293" name="t2rozZws"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Compare

**Intuition and Algorithm**

If we ever find a repeated element, it must be the answer.  Let's call this answer the *major element*.

Consider all subarrays of length 4.  There must be a major element in at least one such subarray.

This is because either:

* There is a major element in a length 2 subarray, or;
* Every length 2 subarray has exactly 1 major element, which means that a length 4 subarray that begins at a major element will have 2 major elements.

Thus, we only have to compare elements with their neighbors that are distance 1, 2, or 3 away.

<iframe src="https://leetcode.com/playground/VUR9s3tb/shared" frameBorder="0" width="100%" height="225" name="VUR9s3tb"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] O(1) Solution
- Author: lee215
- Creation Date: Mon Dec 24 2018 02:50:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Feb 01 2020 11:34:46 GMT+0800 (Singapore Standard Time)

<p>
### Solution 1
Use array or set and return seen number at once.
`O(N)` time, `O(N)` space

**Java, use array**
```
    public int repeatedNTimes(int[] A) {
        int[] count = new int[10000];
        for (int a : A)
            if (count[a]++ == 1)
                return a;
        return -1;
    }
```
**C++, use set**
```
    int repeatedNTimes2(vector<int>& A) {
        unordered_set<int> seen;
        for (int a: A) {
            if (seen.count(a))
                return a;
            seen.insert(a);
        }
    }
```
<br>


## Solution 2
Check if `A[i] == A[i - 1]` or `A[i] == A[i - 2]`
If so, we return `A[i]`
If not, it must be `[x, x, y, z]` or `[x, y, z, x]`.
We return `A[0]` for the cases that we miss.
`O(N)` time `O(1)` space

**C++**
```
    int repeatedNTimes(vector<int>& A) {
        for (int i = 2; i < A.size(); ++i)
            if (A[i] == A[i - 1] || A[i] == A[i - 2])
                return A[i];
        return A[0];
    }
```

**Java**
```
    public int repeatedNTimes(int[] A) {
        for (int i = 2; i < A.length; ++i)
            if (A[i] == A[i - 1] || A[i] == A[i - 2])
                return A[i];
        return A[0];
    }
```
<br>

## Solution 3
This is a solution just for fun, not for interview.
Instead of compare from left to right,
we can compare in random order.

Random pick two numbers.
Return if same.

50% to get the right number.
Each turn, 25% to get two right numbers.
Return the result in average 4 turns.
Time complexity amortized `O(4)`, space `O(1)`


**C++:**
```
    int repeatedNTimes(vector<int>& A) {
        int i = 0, j = 0, n = A.size();
        while (i == j || A[i] != A[j])
            i = rand() % n, j = rand() % n;
        return A[i];
    }
```

**Java:**
```
    public int repeatedNTimes(int[] A) {
        int i = 0, j = 0, n = A.length;
        while (i == j || A[i] != A[j]) {
            i = (int)(Math.random() * n);
            j = (int)(Math.random() * n);
        }
        return A[i];
    }
```
**Python:**
```
    def repeatedNTimes(self, A):
        while 1:
            s = random.sample(A, 2)
            if s[0] == s[1]:
                return s[0]
```

</p>


### C++ 2 lines O(4) | O (1)
- Author: votrubac
- Creation Date: Sun Dec 23 2018 12:01:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 23 2018 12:01:53 GMT+0800 (Singapore Standard Time)

<p>
The intuition here is that the repeated numbers have to appear either next to each other (```A[i] == A[i + 1]```), or alternated (```A[i] == A[i + 2]```).

The only exception is sequences like ```[2, 1, 3, 2]```. In this case, the result is the last number, so we just return it in the end. This solution has O(n) runtime.
```
int repeatedNTimes(vector<int>& A) {
  for (auto i = 0; i < A.size() - 2; ++i)
    if (A[i] == A[i + 1] || A[i] == A[i + 2]) return A[i];
  return A[A.size() - 1]; 
}
```
Another interesting approach is to use randomization (courtesy of [@lee215 ](https://leetcode.com/lee215)). If you pick two numbers randomly, there is a 25% chance you bump into the repeated number. So, in average, we will find the answer in 4 attempts, thus O(4) runtime.
```
int repeatedNTimes(vector<int>& A, int i = 0, int j = 0) {
  while (A[i = rand() % A.size()] != A[j = rand() % A.size()] || i == j);
  return A[i];
}
```
</p>


### Circular array O(n) time and O(1) Space
- Author: destinynitsed
- Creation Date: Sun Dec 23 2018 12:16:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 23 2018 12:16:29 GMT+0800 (Singapore Standard Time)

<p>
If a number is repeated N times in a list of size 2N, it is always possible for the repeated number to stay within a distance of 2.
Consider this exaple where N = 4, Number **x** is repeated twice. All possible comibnations for x to fit in a list of size 4 are:
[a,b,x,x]
[x,a,b,x] (distance between both the x is still 1, consider it as a circular list)
[x,a,x,b]
[x,x,a,b]
[a,x,b,x]
[a,x,x,b]
```python
class Solution:
    def repeatedNTimes(self, A):
        for i in range(len(A)):
            if A[i - 1] == A[i] or A[i - 2] == A[i]:
                return A[i]
```
</p>


