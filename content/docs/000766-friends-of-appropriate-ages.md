---
title: "Friends Of Appropriate Ages"
weight: 766
#id: "friends-of-appropriate-ages"
---
## Description
<div class="description">
<p>Some people will make friend requests. The&nbsp;list of their ages is given and&nbsp;<code>ages[i]</code>&nbsp;is the age of the&nbsp;ith person.&nbsp;</p>

<p>Person A will NOT friend request person B (B != A) if any of the following conditions are true:</p>

<ul>
	<li><code>age[B]&nbsp;&lt;= 0.5 * age[A]&nbsp;+ 7</code></li>
	<li><code>age[B]&nbsp;&gt; age[A]</code></li>
	<li><code>age[B]&nbsp;&gt; 100 &amp;&amp;&nbsp;age[A]&nbsp;&lt; 100</code></li>
</ul>

<p>Otherwise, A will friend request B.</p>

<p>Note that if&nbsp;A requests B, B does not necessarily request A.&nbsp; Also, people will not friend request themselves.</p>

<p>How many total friend requests are made?</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>[16,16]
<strong>Output: </strong>2
<strong>Explanation: </strong>2 people friend request each other.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>[16,17,18]
<strong>Output: </strong>2
<strong>Explanation: </strong>Friend requests are made 17 -&gt; 16, 18 -&gt; 17.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>[20,30,100,110,120]
<strong>Output: </strong>3
<strong>Explanation: </strong>Friend requests are made 110 -&gt; 100, 120 -&gt; 110, 120 -&gt; 100.
</pre>

<p>&nbsp;</p>

<p>Notes:</p>

<ul>
	<li><code>1 &lt;= ages.length&nbsp;&lt;= 20000</code>.</li>
	<li><code>1 &lt;= ages[i] &lt;= 120</code>.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Facebook - 8 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Counting [Accepted]

**Intuition**

Instead of processing all `20000` people, we can process pairs of `(age, count)` representing how many people are that age.  Since there are only 120 possible ages, this is a much faster loop.

**Algorithm**

For each pair `(ageA, countA)`, `(ageB, countB)`, if the conditions are satisfied with respect to age, then `countA * countB` pairs of people made friend requests.

If `ageA == ageB`, then we overcounted: we should have `countA * (countA - 1)` pairs of people making friend requests instead, as you cannot friend request yourself.

<iframe src="https://leetcode.com/playground/aT3ST2sP/shared" frameBorder="0" width="100%" height="412" name="aT3ST2sP"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\mathcal{A}^2 + N)$$, where $$N$$ is the number of people, and $$\mathcal{A}$$ is the number of ages.

* Space Complexity: $$O(\mathcal{A})$$, the space used to store `count`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### age[B] > age[A] and age[B] > 100 && age[A] < 100, isn't the latter redundant?
- Author: elvinyang
- Creation Date: Sun Apr 29 2018 11:03:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 03:41:48 GMT+0800 (Singapore Standard Time)

<p>
age[B] > age[A] and age[B] > 100 && age[A] < 100, isn\'t the latter redundant?
</p>


### 10ms concise Java  solution, O(n) time and O(1) space
- Author: saobiaozi
- Creation Date: Mon Apr 30 2018 23:34:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 02:44:44 GMT+0800 (Singapore Standard Time)

<p>
Three conditions could be merged to one: 
The Person with age A can request person with age B if
* B is in range **(** 0.5 * A + 7, A **]** 
```
    public int numFriendRequests(int[] ages) {
        int res = 0;
        int[] numInAge = new int[121], sumInAge = new int[121];
        
        for(int i : ages) 
            numInAge[i] ++;
        
        for(int i = 1; i <= 120; ++i) 
            sumInAge[i] = numInAge[i] + sumInAge[i - 1];
        
        for(int i = 15; i <= 120; ++i) {
            if(numInAge[i] == 0) continue;
            int count = sumInAge[i] - sumInAge[i / 2 + 7];
            res += count * numInAge[i] - numInAge[i]; //people will not friend request themselves, so  - numInAge[i]
        }
        return res;
    }
```


</p>


### [C++/Java/Python] Easy and Straight Forward
- Author: lee215
- Creation Date: Sun Apr 29 2018 14:52:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 14 2019 20:46:28 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
1. Write a sub function `request(a, b)` to check if age `a` will friend requests age `b`.
I just copy it from description:
`return !(condition1 || condition2 || condition3)`

2. Count nunmber of all ages to a `map`.
Because we have at most 20000 ages but only in range [1, 120].
3. For each age `a` and each age `b != a`, if request(a, b), we will make count[a] * count[b] requests.
4. For each age `a`, if request(a, a), we will make `count[a] * (count[a] - 1)` requests.

**C++:**
```cpp
    int numFriendRequests(vector<int>& ages) {
        unordered_map<int, int> count;
        for (int &age : ages)
            count[age]++;
        int res = 0;
        for (auto &a : count)
            for (auto &b : count)
                if (request(a.first, b.first))
                    res += a.second * (b.second - (a.first == b.first ? 1 : 0));
        return res;
    }

    bool request(int a, int b) {
        return !(b <= 0.5 * a + 7 || b > a || (b > 100 && a < 100));
    }
```

**Java:**
```java
    public int numFriendRequests(int[] ages) {
        Map<Integer, Integer> count = new HashMap<>();
        for (int age : ages)
            count.put(age, count.getOrDefault(age, 0) + 1);
        int res = 0;
        for (Integer a : count.keySet())
            for (Integer b : count.keySet())
                if (request(a, b)) res += count.get(a) * (count.get(b) - (a == b ? 1 : 0));
        return res;
    }

    private boolean request(int a, int b) {
        return !(b <= 0.5 * a + 7 || b > a || (b > 100 && a < 100));
    }
```

**Python:**
```py
    def numFriendRequests(self, ages):
        def request(a, b):
            return not (b <= 0.5 * a + 7 or b > a or b > 100 and a < 100)
        c = collections.Counter(ages)
        return sum(request(a, b) * c[a] * (c[b] - (a == b)) for a in c for b in c)
```

</p>


