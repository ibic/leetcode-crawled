---
title: "Find the Duplicate Number"
weight: 270
#id: "find-the-duplicate-number"
---
## Description
<div class="description">
<p>Given an array of integers <code>nums</code> containing&nbsp;<code>n + 1</code> integers where each integer is in the range <code>[1, n]</code> inclusive.</p>

<p>There is only <strong>one duplicate number</strong> in <code>nums</code>, return <em>this&nbsp;duplicate number</em>.</p>

<p><b>Follow-ups:</b></p>

<ol>
	<li>How can we prove that at least one duplicate number must exist in <code>nums</code>?&nbsp;</li>
	<li>Can you solve the problem <strong>without</strong>&nbsp;modifying&nbsp;the array <code>nums</code>?</li>
	<li>Can you solve the problem using&nbsp;only constant, <code>O(1)</code> extra space?</li>
	<li>Can you solve the problem with runtime complexity less than <code>O(n<sup>2</sup>)</code>?</li>
</ol>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> nums = [1,3,4,2,2]
<strong>Output:</strong> 2
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> nums = [3,1,3,4,2]
<strong>Output:</strong> 3
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> nums = [1,1]
<strong>Output:</strong> 1
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> nums = [1,1,2]
<strong>Output:</strong> 1
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 3 * 10<sup>4</sup></code></li>
	<li><code>nums.length == n + 1</code></li>
	<li><code>1 &lt;= nums[i] &lt;= n</code></li>
	<li>All the integers in <code>nums</code> appear only <strong>once</strong> except for <strong>precisely one integer</strong> which appears <strong>two or more</strong> times.</li>
</ul>

</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)
- Binary Search (binary-search)

## Companies
- Amazon - 9 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Intuit - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Salesforce - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Tencent - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Note #####

The first two approaches mentioned do not satisfy the constraints given in
the prompt, but they are solutions that you might be likely to come up with
during a technical interview. As an interviewer, I personally would _not_
expect someone to come up with the cycle detection solution unless they have
heard it before.

#### Proof ####

Proving that at least one duplicate must exist in `nums` is simple
application of the
[pigeonhole principle](https://en.wikipedia.org/wiki/Pigeonhole_principle).
Here, each number in `nums` is a "pigeon" and each distinct number that can
appear in `nums` is a "pigeonhole". Because there are $$n+1$$ numbers are
$$n$$ distinct possible numbers, the pigeonhole principle implies that at
least one of the numbers is duplicated.

#### Approach 1: Sorting 

**Intuition**

If the numbers are sorted, then any duplicate numbers will be adjacent in the
sorted array.

**Algorithm**

Given the intuition, the algorithm follows fairly simply. First, we sort the
array, and then we compare each element to the previous element. Because
there is exactly one duplicated element in the array, we know that the array
is of at least length 2, and we can return the duplicate element as soon as
we find it.

<iframe src="https://leetcode.com/playground/B4ycKeiE/shared" frameBorder="0" width="100%" height="259" name="B4ycKeiE"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(nlgn)$$

    The `sort` invocation costs $$\mathcal{O}(nlgn)$$ time in Python and Java, so it
    dominates the subsequent linear scan.

* Space complexity : $$\mathcal{O}(1)$$ (or $$\mathcal{O}(n)$$)

    Here, we sort `nums` in place, so the memory footprint is constant. If we
    cannot modify the input array, then we must allocate linear space for a
    copy of `nums` and sort that instead.

---

#### Approach 2: Set 

**Intuition**

If we store each element as we iterate over the array, we can simply check
each element as we iterate over the array.

**Algorithm**

In order to achieve linear time complexity, we need to be able to insert
elements into a data structure (and look them up) in constant time. A `Set`
satisfies these constraints nicely, so we iterate over the array and insert
each element into `seen`. Before inserting it, we check whether it is already
there. If it is, then we found our duplicate, so we return it.

<iframe src="https://leetcode.com/playground/kyTyF74u/shared" frameBorder="0" width="100%" height="276" name="kyTyF74u"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n)$$

    `Set` in both Python and Java rely on underlying hash tables, so
    insertion and lookup have amortized constant time complexities. The
    algorithm is therefore linear, as it consists of a `for` loop that
    performs constant work $$n$$ times.

* Space complexity : $$\mathcal{O}(n)$$

    In the worst case, the duplicate element appears twice, with one of its
    appearances at array index $$n-1$$. In this case, `seen` will contain
    $$n-1$$ distinct values, and will therefore occupy $$\mathcal{O}(n)$$ space.

---

#### Approach 3: Floyd's Tortoise and Hare (Cycle Detection)

**Intuition**

The idea is to reduce the problem to
[Linked List Cycle II](https://leetcode.com/problems/linked-list-cycle-ii/solution/):

> Given a linked list, return the node where the cycle begins.

First of all, where does the cycle come from?
Let's use the function `f(x) = nums[x]` to construct the sequence:
`x, nums[x], nums[nums[x]], nums[nums[nums[x]]], ...`.

Each new element in the sequence is an element in nums at the index 
of the _previous_ element. 

If one starts from `x = nums[0]`, such a sequence will produce a linked list 
with a cycle. 

> The cycle appears because `nums` contains duplicates. The duplicate node 
is a cycle entrance.

Here is how it works:

![pic](../Figures/287/simple_cycle.png)  

The example above is simple because the loop is small. Here is a
more interesting example 
(special thanks to @[sushant_chaudhari](https://leetcode.com/sushant_chaudhari)) 

![pic](../Figures/287/complex_cycle.png)

Now the problem is to find the entrance of the cycle.

**Algorithm**

[Floyd's algorithm](https://en.wikipedia.org/wiki/The_Tortoise_and_the_Hare) 
consists of two phases and uses two pointers, usually called `tortoise` and `hare`. 

**In phase 1**, `hare = nums[nums[hare]]` is twice as fast as 
`tortoise = nums[tortoise]`. Since the hare goes fast, 
it would be the first one who enters the cycle and starts to run around the cycle. 
At some point, the tortoise enters the cycle as well, and since 
it's moving slower the hare catches the tortoise up at some _intersection_ point.
Now phase 1 is over, and the tortoise has lost.

> Note that the intersection point is not the cycle entrance in the general case.  

![pic](../Figures/287/first_intersection.png)

To compute the intersection point, let's note that the hare has 
traversed twice as many nodes as the tortoise, 
_i.e._ $$2d(\text{tortoise}) = d(\text{hare})$$, that means

$$2(F + a) = F + nC + a$$, where $$n$$ is some integer. 

> Hence the coordinate of the intersection point is $$F + a = nC$$.

**In phase 2**, we give the tortoise a second chance by slowing down the hare,
so that it now moves with the speed of tortoise: `tortoise = nums[tortoise]`, 
`hare = nums[hare]`. The tortoise is back at the starting 
position, and the hare starts from the intersection point. 

![pic](../Figures/287/phase2.png)

Let's show that this time they meet at the cycle entrance after $$F$$ steps.

- The tortoise started from zero, so its position after $$F$$ steps is $$F$$.

- The hare started at the intersection point $$F + a = nC$$, 
so its position after F steps is 
$$nC + F$$, that is the same point as $$F$$.
 
- So the tortoise and the (slowed down) hare will meet at the entrance of the cycle.

**Implementation**

!?!../Documents/287_LIS.json:1000,362!?!

<iframe src="https://leetcode.com/playground/NsJPbZt9/shared" frameBorder="0" width="100%" height="395" name="NsJPbZt9"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n)$$

    For detailed analysis, refer to 
    [Linked List Cycle II](https://leetcode.com/problems/linked-list-cycle-ii/solution/#approach-2-floyds-tortoise-and-hare-accepted).
    

* Space complexity : $$\mathcal{O}(1)$$

    For detailed analysis, refer to 
    [Linked List Cycle II](https://leetcode.com/problems/linked-list-cycle-ii/solution/#approach-2-floyds-tortoise-and-hare-accepted).
    

---

Analysis and solutions written by: [@emptyset](https://leetcode.com/emptyset)

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My easy understood solution with O(n) time and O(1) space without modifying the array. With clear explanation.
- Author: echoxiaolee
- Creation Date: Thu Oct 01 2015 19:54:31 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 11:53:56 GMT+0800 (Singapore Standard Time)

<p>
The main idea is the same with problem ***Linked List Cycle II***,*https://leetcode.com/problems/linked-list-cycle-ii/*. Use two pointers the fast and the slow. The fast one goes forward two steps each time, while the slow one goes only step each time. They must meet the same item when slow==fast. In fact, they meet in a circle, the duplicate number must be the entry point of the circle when visiting the array from nums[0]. Next we just need to find the entry point. We use a point(we can use the fast one before) to visit form begining with one step each time, do the same job to slow. When fast==slow, they meet at the entry point of the circle. The easy understood code is as follows.

    int findDuplicate3(vector<int>& nums)
    {
    	if (nums.size() > 1)
    	{
    		int slow = nums[0];
    		int fast = nums[nums[0]];
    		while (slow != fast)
    		{
    			slow = nums[slow];
    			fast = nums[nums[fast]];
    		}
    
    		fast = 0;
    		while (fast != slow)
    		{
    			fast = nums[fast];
    			slow = nums[slow];
    		}
    		return slow;
    	}
    	return -1;
    }
</p>


### Two Solutions (with explanation): O(nlog(n)) and O(n) time , O(1) space, without changing the input array
- Author: mehran
- Creation Date: Mon Sep 28 2015 11:05:45 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 08:18:04 GMT+0800 (Singapore Standard Time)

<p>
This solution is based on binary search.

At first the search space is numbers between 1 to n. Each time I select a number `mid` (which is the one in the middle) and count all the numbers equal to or less than `mid`. Then if the `count` is more than `mid`, the search space will be `[1 mid]` otherwise `[mid+1 n]`. I do this until search space is only one number.
 
Let's say `n=10` and I select `mid=5`. Then I count all the numbers in the array which are less than equal `mid`. If the there are more than `5` numbers that are less than `5`, then by Pigeonhole Principle (https://en.wikipedia.org/wiki/Pigeonhole_principle) one of them has occurred more than once. So I shrink the search space from `[1 10]` to `[1 5]`. Otherwise the duplicate number is in the second half so for the next step the search space would be `[6 10]`. 

    class Solution(object):
        def findDuplicate(self, nums):
            """
            :type nums: List[int]
            :rtype: int
            """
            low = 1
            high = len(nums)-1
            
            while low < high:
                mid = low+(high-low)/2
                count = 0
                for i in nums:
                    if i <= mid:
                        count+=1
                if count <= mid:
                    low = mid+1
                else:
                    high = mid
            return low


There's also a better algorithm with `O(n)` time. Please read this very interesting solution here:
[http://keithschwarz.com/interesting/code/?dir=find-duplicate](http://keithschwarz.com/interesting/code/?dir=find-duplicate)
</p>


### Simple C++ code with O(1) space and O(nlogn) time complexity
- Author: ningwangpanda
- Creation Date: Mon Nov 09 2015 02:08:46 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 11:17:39 GMT+0800 (Singapore Standard Time)

<p>
class Solution {

public:

    int findDuplicate(vector<int>& nums) {
        int n=nums.size()-1;
        int low=1;
        int high=n;
        int mid;
        while(low<high){
            mid=(low+high)/2;
            int count=0;
            for(int num:nums){
                if(num<=mid) count++;
            }
            if(count>mid) high=mid;
            else low=mid+1; 
        }
        return low;
    }
};
</p>


