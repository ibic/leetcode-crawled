---
title: "Count Odd Numbers in an Interval Range"
weight: 1380
#id: "count-odd-numbers-in-an-interval-range"
---
## Description
<div class="description">
<p>Given two non-negative integers <code>low</code> and <code><font face="monospace">high</font></code>. Return the <em>count of odd numbers between </em><code>low</code><em> and </em><code><font face="monospace">high</font></code><em>&nbsp;(inclusive)</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> low = 3, high = 7
<strong>Output:</strong> 3
<b>Explanation: </b>The odd numbers between 3 and 7 are [3,5,7].</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> low = 8, high = 10
<strong>Output:</strong> 1
<b>Explanation: </b>The odd numbers between 8 and 10 are [9].</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= low &lt;= high&nbsp;&lt;= 10^9</code></li>
</ul>
</div>

## Tags
- Math (math)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] 1-Lines
- Author: lee215
- Creation Date: Sun Jul 26 2020 00:04:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 00:13:23 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
the count of odd numbers between 1 and `low - 1` is `low / 2`
the count of odd numbers between 1 and `high` is `(high + 1 ) / 2`
<br>

## **Complexity**
Time `O(1)`
Space `O(1)`

<br>

**Java:**
```java
    public int countOdds(int low, int high) {
        return (high + 1) / 2 - low / 2;
    }
```

**C++:**
```cpp
    int countOdds(int low, int high) {
        return (high + 1) / 2 - low / 2;
    }
```

**Python:**
```py
    def countOdds(self, low, high):
        return (high + 1) / 2 - low / 2
```

</p>


### C++ | Easy to understand | O(1)
- Author: di_b
- Creation Date: Sun Jul 26 2020 00:07:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 12:26:19 GMT+0800 (Singapore Standard Time)

<p>
```
int countOdds(int low, int high) {
    int res = (high-low)/2;
    if(low %2 != 0 || high%2 != 0)
        res++;
    return res;
}
```
(high-low)/2 - half the numbers in any given range will be odd
if(low %2 != 0 || high%2 != 0) - If either low or high is odd or both are odd, we will have 1 more odd number. Some examples:
1) low = 4, high = 10 (odd numbers - 5,7,9) -> (10-4)/2 = 3
2) low = 4, high = 11 (odd numbers - 5,7,9,11) -> (11-4)/2 = 3, as high is odd, 3+1 = 4
3) low = 5, high = 10 (odd numbers - 5,7,9) -> (10-5)/2 = 2, as low is odd, 2+1 = 3
4) low = 5, high = 11 (odd numbers - 5,7,9,11) -> (11-5)/2 = 3, as both high and low are odd, 3+1 = 4
</p>


### Java - 100% faster - O(1) space, O(1) Time using MATH
- Author: jenish1097
- Creation Date: Tue Aug 18 2020 03:22:30 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 18 2020 03:22:30 GMT+0800 (Singapore Standard Time)

<p>
My idea here is to make both the numbers odd as it would make the problem much easier. 

We increase the low by 1 and decrease the high by 1 if they are not odd. If the nums are odd already, we do not change them.
Simply return the answer as **(high - low)/2 + 1;**. As, now we count the low and high inclusive. 


For instance, low = 4, high = 8 is same as low = 5 and high = 7 as the odds are only [5,7].

```
public static int countOdds(int low, int high) {
		if(low%2 == 0)
			low += 1;
		if(high%2 == 0)
			high -= 1;
		return (high - low)/2 + 1;
	}
```

**Time - O(1)** - Because just a constant time math operation is involved.
**Space  O(1)** - Because we don\'t even use a single variable.
</p>


