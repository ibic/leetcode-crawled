---
title: "Multiply Strings"
weight: 43
#id: "multiply-strings"
---
## Description
<div class="description">
<p>Given two non-negative integers <code>num1</code> and <code>num2</code> represented as strings, return the product of <code>num1</code> and <code>num2</code>, also represented as a string.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> num1 = &quot;2&quot;, num2 = &quot;3&quot;
<strong>Output:</strong> &quot;6&quot;</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> num1 = &quot;123&quot;, num2 = &quot;456&quot;
<strong>Output:</strong> &quot;56088&quot;
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li>The length of both <code>num1</code> and <code>num2</code> is &lt; 110.</li>
	<li>Both <code>num1</code> and <code>num2</code> contain&nbsp;only digits <code>0-9</code>.</li>
	<li>Both <code>num1</code> and <code>num2</code>&nbsp;do not contain any leading zero, except the number 0 itself.</li>
	<li>You <strong>must not use any built-in BigInteger library</strong> or <strong>convert the inputs to integer</strong> directly.</li>
</ol>

</div>

## Tags
- Math (math)
- String (string)

## Companies
- Facebook - 14 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Houzz - 4 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Zillow - 2 (taggedByAdmin: false)
- Redfin - 2 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easiest JAVA Solution with Graph Explanation
- Author: yavinci
- Creation Date: Thu Nov 26 2015 04:36:55 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 03:18:43 GMT+0800 (Singapore Standard Time)

<p>
Remember how we do multiplication? 

Start from right to left, perform multiplication on every pair of digits, and add them together.  Let's draw the process! From the following draft, we can immediately conclude:

     `num1[i] * num2[j]` will be placed at indices `[i + j`, `i + j + 1]` 
 
<hr>

<a href='https://drscdn.500px.org/photo/130178585/m%3D2048/300d71f784f679d5e70fadda8ad7d68f' target='_blank'><img src='https://drscdn.500px.org/photo/130178585/m%3D2048/300d71f784f679d5e70fadda8ad7d68f' border='0' alt="Multiplication" width="100%"/></a>

<hr>

Here is my solution. Hope it helps!

    public String multiply(String num1, String num2) {
        int m = num1.length(), n = num2.length();
        int[] pos = new int[m + n];
       
        for(int i = m - 1; i >= 0; i--) {
            for(int j = n - 1; j >= 0; j--) {
                int mul = (num1.charAt(i) - '0') * (num2.charAt(j) - '0'); 
                int p1 = i + j, p2 = i + j + 1;
                int sum = mul + pos[p2];

                pos[p1] += sum / 10;
                pos[p2] = (sum) % 10;
            }
        }  
        
        StringBuilder sb = new StringBuilder();
        for(int p : pos) if(!(sb.length() == 0 && p == 0)) sb.append(p);
        return sb.length() == 0 ? "0" : sb.toString();
    }


  [1]: http://postimg.org/image/tltx29dcx/
</p>


### AC solution in Java with explanation
- Author: lx223
- Creation Date: Thu Apr 30 2015 06:49:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 01:49:36 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public String multiply(String num1, String num2) {
            int n1 = num1.length(), n2 = num2.length();
            int[] products = new int[n1 + n2];
            for (int i = n1 - 1; i >= 0; i--) {
                for (int j = n2 - 1; j >= 0; j--) {
                    int d1 = num1.charAt(i) - '0';
                    int d2 = num2.charAt(j) - '0';
                    products[i + j + 1] += d1 * d2;
                }
            }
            int carry = 0;
            for (int i = products.length - 1; i >= 0; i--) {
                int tmp = (products[i] + carry) % 10;
                carry = (products[i] + carry) / 10;
                products[i] = tmp;
            }
            StringBuilder sb = new StringBuilder();
            for (int num : products) sb.append(num);
            while (sb.length() != 0 && sb.charAt(0) == '0') sb.deleteCharAt(0);
            return sb.length() == 0 ? "0" : sb.toString();
        }
    }

If we break it into steps, it will have the following steps. 1. compute products from each pair of digits from num1 and num2. 2. carry each element over. 3. output the solution.

Things to note:

1. The product of two numbers cannot exceed the sum of the two lengths. (e.g. 99 * 99 cannot be five digit)

2.

    int d1 = num1.charAt(i) - '0';
    int d2 = num2.charAt(j) - '0';
    products[i + j + 1] += d1 * d2;
</p>


### Brief C++ solution using only strings and without reversal
- Author: ChiangKaiShrek
- Creation Date: Sat Feb 28 2015 22:29:34 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 21:51:59 GMT+0800 (Singapore Standard Time)

<p>
This is the standard manual multiplication algorithm. We use two nested for loops, working backward from the end of each input number. We pre-allocate our result and accumulate our partial result in there. One special case to note is when our carry requires us to write to our sum string outside of our for loop.

At the end, we trim any leading zeros, or return 0 if we computed nothing but zeros.

    string multiply(string num1, string num2) {
        string sum(num1.size() + num2.size(), '0');
        
        for (int i = num1.size() - 1; 0 <= i; --i) {
            int carry = 0;
            for (int j = num2.size() - 1; 0 <= j; --j) {
                int tmp = (sum[i + j + 1] - '0') + (num1[i] - '0') * (num2[j] - '0') + carry;
                sum[i + j + 1] = tmp % 10 + '0';
                carry = tmp / 10;
            }
            sum[i] += carry;
        }
        
        size_t startpos = sum.find_first_not_of("0");
        if (string::npos != startpos) {
            return sum.substr(startpos);
        }
        return "0";
    }
</p>


