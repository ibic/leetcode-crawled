---
title: "Market Analysis I"
weight: 1539
#id: "market-analysis-i"
---
## Description
<div class="description">
<p>Table: <code>Users</code></p>

<pre>
+----------------+---------+
| Column Name    | Type    |
+----------------+---------+
| user_id        | int     |
| join_date      | date    |
| favorite_brand | varchar |
+----------------+---------+
user_id is the primary key of this table.
This table has the info of the users of an online shopping website where users can sell and buy items.</pre>

<p>Table: <code>Orders</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| order_id      | int     |
| order_date    | date    |
| item_id       | int     |
| buyer_id      | int     |
| seller_id     | int     |
+---------------+---------+
order_id is the primary key of this table.
item_id is a foreign key to the Items table.
buyer_id and seller_id are foreign keys to the Users table.
</pre>

<p>Table: <code>Items</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| item_id       | int     |
| item_brand    | varchar |
+---------------+---------+
item_id is the primary key of this table.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find for each user, the join date and the number of orders they made as a buyer in <strong>2019</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
Users table:
+---------+------------+----------------+
| user_id | join_date  | favorite_brand |
+---------+------------+----------------+
| 1       | 2018-01-01 | Lenovo         |
| 2       | 2018-02-09 | Samsung        |
| 3       | 2018-01-19 | LG             |
| 4       | 2018-05-21 | HP             |
+---------+------------+----------------+

Orders table:
+----------+------------+---------+----------+-----------+
| order_id | order_date | item_id | buyer_id | seller_id |
+----------+------------+---------+----------+-----------+
| 1        | 2019-08-01 | 4       | 1        | 2         |
| 2        | 2018-08-02 | 2       | 1        | 3         |
| 3        | 2019-08-03 | 3       | 2        | 3         |
| 4        | 2018-08-04 | 1       | 4        | 2         |
| 5        | 2018-08-04 | 1       | 3        | 4         |
| 6        | 2019-08-05 | 2       | 2        | 4         |
+----------+------------+---------+----------+-----------+

Items table:
+---------+------------+
| item_id | item_brand |
+---------+------------+
| 1       | Samsung    |
| 2       | Lenovo     |
| 3       | LG         |
| 4       | HP         |
+---------+------------+

Result table:
+-----------+------------+----------------+
| buyer_id  | join_date  | orders_in_2019 |
+-----------+------------+----------------+
| 1         | 2018-01-01 | 1              |
| 2         | 2018-02-09 | 2              |
| 3         | 2018-01-19 | 0              |
| 4         | 2018-05-21 | 0              |
+-----------+------------+----------------+
</pre>

</div>

## Tags


## Companies
- Poshmark - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple MySQL Solution: Easy to Understand and Fast
- Author: Morganmm
- Creation Date: Sat Oct 05 2019 06:10:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 05 2019 06:10:26 GMT+0800 (Singapore Standard Time)

<p>
Be careful if you use where in join here you would lose the Null occurence of orders thus zero number of orders won\'t show up in the final table!
```
SELECT u.user_id AS buyer_id, join_date, 
IFNULL(COUNT(order_date), 0) AS orders_in_2019 
FROM Users as u
LEFT JOIN
Orders as o
ON u.user_id = o.buyer_id
AND YEAR(order_date) = \'2019\'
GROUP BY u.user_id
```
If you like the solution please upvote so more people can see and benefit from it!
</p>


### A simple MYSQL solution beats 100% so far
- Author: leoatcle
- Creation Date: Tue Aug 13 2019 21:06:14 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 13 2019 21:06:14 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT user_id AS buyer_id, join_date, COALESCE(COUNT(o.order_id),0) AS orders_in_2019
FROM Users u
LEFT JOIN Orders o ON u.user_id = o.buyer_id AND YEAR(order_date)=\'2019\'
GROUP BY user_id
ORDER BY user_id
```
</p>


### MySQL | Simple Approach Solution | 99.6% faster
- Author: DixitVivek
- Creation Date: Fri Sep 11 2020 20:36:14 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 11 2020 20:36:14 GMT+0800 (Singapore Standard Time)

<p>
Below code is pretty straightforward. However, as we all are here trying to learn new things and ways to solve the questions in an optimized way (after getting the correct solution ofcourse) I observed one key differentiator impacting the performance.

Since we have to consider data only for year 2019 so instead of using **between** keyword use the **<** and **>** operators for filtering the data in the desired range. **"Between"** is CPU intensive operation hence the performance increased from 56% to 99.6% just by making the small change. Below is the optimized code. Happy Learning!!

```select user_id as buyer_id, join_date, ifnull(orders_in_2019,0) as orders_in_2019 from Users u left join (select count(order_id) as orders_in_2019, buyer_id from Orders where order_date >=\'2019-01-01\' and order_date <=\'2019-12-31\' group by buyer_id ) ord on u.user_id = ord.buyer_id;```
</p>


