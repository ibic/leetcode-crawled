---
title: "Inorder Successor in BST II"
weight: 482
#id: "inorder-successor-in-bst-ii"
---
## Description
<div class="description">
<p>Given a <code>node</code> in a binary search tree, find&nbsp;the in-order successor of that node in the BST.</p>

<p>If that node has no in-order successor, return&nbsp;<code>null</code>.</p>

<p>The successor of a <code>node</code>&nbsp;is the node with the smallest key greater than <code>node.val</code>.</p>

<p>You will have direct access to the node but not to the root of the tree. Each node will have a reference to its parent node.&nbsp;Below is the definition for <code>Node</code>:</p>

<pre>
class Node {
    public int val;
    public Node left;
    public Node right;
    public Node parent;
}
</pre>

<p>&nbsp;</p>

<p><strong>Follow up:</strong></p>

<p>Could you solve&nbsp;it without&nbsp;looking up any of the&nbsp;node&#39;s values?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/01/23/285_example_1.PNG" style="width: 122px; height: 117px;" />
<pre>
<strong>Input:</strong> tree = [2,1,3], node = 1
<strong>Output:</strong> 2
<strong>Explanation: </strong>1&#39;s in-order successor node is 2. Note that both the node and the return value is of Node type.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/01/23/285_example_2.PNG" style="width: 246px; height: 229px;" />
<pre>
<strong>Input:</strong> tree = [5,3,6,2,4,null,null,1], node = 6
<strong>Output:</strong> null
<strong>Explanation: </strong>There is no in-order successor of the current node, so the answer is null.
</pre>

<p><strong>Example 3:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/02/02/285_example_34.PNG" style="width: 438px; height: 335px;" />
<pre>
<strong>Input:</strong> tree = [15,6,18,3,7,17,20,2,4,null,13,null,null,null,null,null,null,null,null,9], node = 15
<strong>Output:</strong> 17
</pre>

<p><strong>Example 4:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/02/02/285_example_34.PNG" style="width: 438px; height: 335px;" />
<pre>
<strong>Input:</strong> tree = [15,6,18,3,7,17,20,2,4,null,13,null,null,null,null,null,null,null,null,9], node = 13
<strong>Output:</strong> 15
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> tree = [0], node = 0
<strong>Output:</strong> null
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>-10^5 &lt;= Node.val &lt;= 10^5</code></li>
	<li><code>1 &lt;= Number of Nodes &lt;=&nbsp;10^4</code></li>
	<li>All Nodes will have unique values.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Microsoft - 4 (taggedByAdmin: false)
- Quip (Salesforce) - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Successor and Predecessor

> Successor = "after node", i.e. the next node in the inorder traversal, 
or the smallest node _after_ the current one.  

> Predecessor = "before node", i.e. the previous node in the inorder traversal, 
or the largest node _before_ the current one.  

![img](../Figures/510/succ.png)
<br />
<br />


---
#### Approach 1: Iteration

**Intuition**

There are two possible situations here :

- Node has a right child, and hence its successor is somewhere
 lower in the tree. To find the successor, go to the right once
and then as many times to the left as you could.

![pic](../Figures/510/right_child2.png)

- Node has no right child, then its successor is somewhere
 upper in the tree. To find the successor, go up till the node
 that is _left_ child of its parent. The answer is the parent.
 Beware that there could be no successor (= null successor) in such a situation.

![pac](../Figures/510/case.png)

---

![fic](../Figures/510/casenull.png)


**Algorithm**

1. If the node has a right child, and hence its successor is somewhere lower
in the tree. Go to the right once 
and then as many times to the left as you could.
Return the node you end up with.

2. Node has no right child, and hence its successor is somewhere
upper in the tree. Go up till the node
that is _left_ child of its parent. The answer is the parent.

**Implementation**

<iframe src="https://leetcode.com/playground/7oMkroXR/shared" frameBorder="0" width="100%" height="310" name="7oMkroXR"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(H)$$, where $$H$$ is the height of the tree. 
That means $$\mathcal{O}(\log N)$$ in the average case, 
and $$\mathcal{O}(N)$$ in the worst case, where $$N$$ is the number of nodes in the tree.
* Space complexity : $$\mathcal{O}(1)$$, since no additional space is allocated during the calculation.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java find in parents or find in descendents
- Author: wangzi6147
- Creation Date: Mon Feb 04 2019 04:33:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 04 2019 04:33:54 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public Node inorderSuccessor(Node x) {
        if (x.right == null) {
            Node result = x.parent;
            while (result != null && result.val < x.val) {
                result = result.parent;
            }
            return result;
        } else {
            Node result = x.right;
            while (result.left != null) {
                result = result.left;
            }
            return result;
        }
    }
}
```
</p>


### JAVA iterative Beats 100%, 4ms - not looking up node values. Detailed explanation.
- Author: indish
- Creation Date: Mon Feb 18 2019 14:05:00 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 18 2019 14:05:00 GMT+0800 (Singapore Standard Time)

<p>
If the given node has a right, then the left most node on the right side must be the successor. 

If the given node is on the left side and its parent\'s right is pointing to the given node, then we must find a parent node whose left is current node for it to become the successor.

Take a look the example 3 provided in the question. Observe the successor for 13.
Think about successor of 18 in the same example if it didn\'t have any child nodes.
```
public Node inorderSuccessor(Node x) {
        if(x == null) return null;
        
        if(x.right != null) {
            x = x.right;
            
            while(x != null && x.left != null) {
                x = x.left;
            }
            
            return x;
        }
        
        while(x != null)
        {
            if(x.parent == null)
                return null;
                
            if(x.parent.left == x)
                return x.parent;
            
            else {
                x = x.parent;
            }
                
        }
        
        return x;
    }
```
</p>


### Python Iterative O(logN)
- Author: ArizonaTea
- Creation Date: Tue Feb 05 2019 09:50:22 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Feb 05 2019 09:50:22 GMT+0800 (Singapore Standard Time)

<p>
*  if node.right, find the most left node in the right subtree of the node;
*  otherwise, find a parent upward that contains the node in its left subtree (and thus the node is the most right one in the subtree).
```
class Solution:
    def inorderSuccessor(self, node: \'Node\') -> \'Node\':
        if node.right:
            node = node.right
            while node.left:
                node = node.left
            return node
        
        while node.parent and node.parent.val < node.val:
            node = node.parent
        return node.parent
            
```
</p>


