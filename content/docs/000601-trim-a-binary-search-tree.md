---
title: "Trim a Binary Search Tree"
weight: 601
#id: "trim-a-binary-search-tree"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a binary search tree and the lowest and highest boundaries as <code>low</code> and <code>high</code>, trim the tree so that all its elements lies in <code>[low, high]</code>. Trimming the tree should <strong>not</strong> change the relative structure of the elements that will remain in the tree (i.e., any node&#39;s descendant should remain a descendant). It can be proven that there is a <strong>unique answer</strong>.</p>

<p>Return <em>the root of the trimmed binary search tree</em>. Note that the root may change depending on the given bounds.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/09/trim1.jpg" style="width: 450px; height: 126px;" />
<pre>
<strong>Input:</strong> root = [1,0,2], low = 1, high = 2
<strong>Output:</strong> [1,null,2]
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/09/trim2.jpg" style="width: 450px; height: 277px;" />
<pre>
<strong>Input:</strong> root = [3,0,4,null,2,null,null,1], low = 1, high = 3
<strong>Output:</strong> [3,2,null,1]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [1], low = 1, high = 2
<strong>Output:</strong> [1]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root = [1,null,2], low = 1, high = 3
<strong>Output:</strong> [1,null,2]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> root = [1,null,2], low = 2, high = 4
<strong>Output:</strong> [2]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree in the range <code>[1, 10<sup>4</sup>]</code>.</li>
	<li><code>0 &lt;= Node.val &lt;= 10<sup>4</sup></code></li>
	<li>The value of each node in the tree is <strong>unique</strong>.</li>
	<li><code>root</code> is guaranteed to be a valid binary search tree.</li>
	<li><code>0 &lt;= low &lt;= high &lt;= 10<sup>4</sup></code></li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Google - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Samsung - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Bloomberg - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1: Recursion

**Intuition**

Let `trim(node)` be the desired answer for the subtree at that node.  We can construct the answer recursively.

**Algorithm**

When $$\text{node.val > high}$$, we know that the trimmed binary tree must occur to the left of the node. Similarly, when $$\text{node.val < low}$$, the trimmed binary tree occurs to the right of the node. Otherwise, we will trim both sides of the tree.

<iframe src="https://leetcode.com/playground/JrGecm7C/shared" frameBorder="0" width="100%" height="500" name="JrGecm7C"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the total number of nodes in the given tree.  We visit each node at most once.

* Space Complexity: $$O(N)$$.  Even though we don't explicitly use any additional memory, the call stack of our recursion could be as large as the number of nodes in the worst case.

---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution, 6 liner
- Author: shawngao
- Creation Date: Sun Sep 03 2017 11:06:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 11:48:03 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public TreeNode trimBST(TreeNode root, int L, int R) {
        if (root == null) return null;
        
        if (root.val < L) return trimBST(root.right, L, R);
        if (root.val > R) return trimBST(root.left, L, R);
        
        root.left = trimBST(root.left, L, R);
        root.right = trimBST(root.right, L, R);
        
        return root;
    }
}
```
</p>


### Java solution, iteration version
- Author: Stefan_Azure
- Creation Date: Tue Sep 19 2017 02:06:12 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 08:37:38 GMT+0800 (Singapore Standard Time)

<p>
I just found everyone using recursion to solve this problem. And it's true that recursive solution is much clean and readable. But I just want to write a iterative version which is much longer. Using three loops, one for finding a new root. Second for removing the invalid nodes from left subtree of new root. The last one for removing the invalids nodes from right subtree of new root. Have fun with this solution. lol
```
class Solution {
    public TreeNode trimBST(TreeNode root, int L, int R) {
        if (root == null) {
            return root;
        }
        //Find a valid root which is used to return.
        while (root.val < L || root.val > R) {
            if (root.val < L) {
                root = root.right;
            }
            if (root.val > R) {
                root = root.left;
            }
        }
        TreeNode dummy = root;
        // Remove the invalid nodes from left subtree.
        while (dummy != null) {
            while (dummy.left != null && dummy.left.val < L) {
                dummy.left = dummy.left.right; 
                // If the left child is smaller than L, then we just keep the right subtree of it. 
            }
            dummy = dummy.left;
        }
        dummy = root;
        // Remove the invalid nodes from right subtree
        while (dummy != null) {
            while (dummy.right != null && dummy.right.val > R) {
                dummy.right = dummy.right.left;
                // If the right child is biggrt than R, then we just keep the left subtree of it. 
            }
            dummy = dummy.right;
        }
        return root;
    }
}
```
</p>


### C++, recursion
- Author: zestypanda
- Creation Date: Sun Sep 03 2017 11:01:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 05:15:00 GMT+0800 (Singapore Standard Time)

<p>
The code works as recursion. 
```
If the root value in the range [L, R]
      we need return the root, but trim its left and right subtree;
else if the root value < L
      because of binary search tree property, the root and the left subtree are not in range;
      we need return trimmed right subtree.
else
      similarly we need return trimmed left subtree.
```
Without freeing memory
```
class Solution {
public:
    TreeNode* trimBST(TreeNode* root, int L, int R) {
        if (root == NULL) return NULL;
        if (root->val < L) return trimBST(root->right, L, R);
        if (root->val > R) return trimBST(root->left, L, R);
        root->left = trimBST(root->left, L, R);
        root->right = trimBST(root->right, L, R);
        return root;
    }
};

```
Free the memory
As @StefanPochmann pointed out, it works well to delete only non-root nodes of the whole tree. His solution is as below. Thanks.
```
TreeNode* trimBST(TreeNode* root, int L, int R, bool top=true) {
    if (!root)
        return root;
    root->left = trimBST(root->left, L, R, false);
    root->right = trimBST(root->right, L, R, false);
    if (root->val >= L && root->val <= R)
        return root;
    auto result = root->val < L ? root->right : root->left;
    if (!top)
        delete root;
    return result;
}
```
</p>


