---
title: "The k Strongest Values in an Array"
weight: 1352
#id: "the-k-strongest-values-in-an-array"
---
## Description
<div class="description">
<p>Given an array of integers <code>arr</code>&nbsp;and an integer <code>k</code>.</p>

<p>A value <code>arr[i]</code> is said to be stronger than a value <code>arr[j]</code> if <code>|arr[i] - m| &gt; |arr[j]&nbsp;- m|</code> where <code>m</code> is the <strong>median</strong> of the array.<br />
If <code>|arr[i] - m| == |arr[j] - m|</code>, then <code>arr[i]</code> is said to be stronger than <code>arr[j]</code> if <code>arr[i] &gt; arr[j]</code>.</p>

<p>Return <em>a list of the strongest <code>k</code></em> values in the array. return the answer <strong>in any arbitrary order</strong>.</p>

<p><strong>Median</strong> is the middle value in an ordered integer list. More formally, if the length of the list is n, the median is the element in position <code>((n - 1) / 2)</code> in the sorted list&nbsp;<strong>(0-indexed)</strong>.</p>

<ul>
	<li>For <code>arr =&nbsp;[6, -3, 7, 2, 11]</code>,&nbsp;<code>n = 5</code> and the median is obtained by sorting the array&nbsp;<code>arr = [-3, 2, 6, 7, 11]</code> and the median is <code>arr[m]</code> where <code>m = ((5 - 1) / 2) = 2</code>. The median is <code>6</code>.</li>
	<li>For <code>arr =&nbsp;[-7, 22, 17,&thinsp;3]</code>,&nbsp;<code>n = 4</code> and the median is obtained by sorting the array&nbsp;<code>arr = [-7, 3, 17, 22]</code> and the median is <code>arr[m]</code> where <code>m = ((4 - 1) / 2) = 1</code>. The median is <code>3</code>.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,3,4,5], k = 2
<strong>Output:</strong> [5,1]
<strong>Explanation:</strong> Median is 3, the elements of the array sorted by the strongest are [5,1,4,2,3]. The strongest 2 elements are [5, 1]. [1, 5] is also <strong>accepted</strong> answer.
Please note that although |5 - 3| == |1 - 3| but 5 is stronger than 1 because 5 &gt; 1.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,1,3,5,5], k = 2
<strong>Output:</strong> [5,5]
<strong>Explanation:</strong> Median is 3, the elements of the array sorted by the strongest are [5,5,1,1,3]. The strongest 2 elements are [5, 5].
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [6,7,11,7,6,8], k = 5
<strong>Output:</strong> [11,8,6,6,7]
<strong>Explanation:</strong> Median is 7, the elements of the array sorted by the strongest are [11,8,6,6,7,7].
Any permutation of [11,8,6,6,7] is <strong>accepted</strong>.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [6,-3,7,2,11], k = 3
<strong>Output:</strong> [-3,11,2]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> arr = [-7,22,17,3], k = 2
<strong>Output:</strong> [22,17]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 10^5</code></li>
	<li><code>-10^5 &lt;= arr[i] &lt;= 10^5</code></li>
	<li><code>1 &lt;= k &lt;= arr.length</code></li>
</ul>
</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java/Python Two Pointers + 3 Bonuses
- Author: votrubac
- Creation Date: Sun Jun 07 2020 12:02:50 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 08 2020 05:51:19 GMT+0800 (Singapore Standard Time)

<p>
My initial solution during the contest was to sort the array to figure out the `median`, and then sort the array again based on the `abs(arr[i] - median)` criteria.

Then I noticed that we have smallest and largest elements in the array in the result. That makes sense based on the problem definition. So, instead of sorting the array again, we can use the two pointers pattern to enumerate smallest and largest numbers, picking the largest `abs(a[i] - median)` each time.

The code above has one trick: we check that `a[i] > a[j]` first. This way, if `a[i] == a[j]`, we pick the element from the right side, which is largest because the array is sorted!

> Update: check bonus sections below for O(n log k), O(n + k log n), and O(n) solutions.

**C++**
```cpp
vector<int> getStrongest(vector<int>& arr, int k) {
    sort(begin(arr), end(arr));
    int i = 0, j = arr.size() - 1;
    int med = arr[(arr.size() - 1) / 2];
    while (--k >= 0)
        if (med - arr[i] > arr[j] - med)
            ++i;  
        else
            --j;
    arr.erase(begin(arr) + i, begin(arr) + j + 1);
    return arr;
}
```
**Java**
```java
public int[] getStrongest(int[] arr, int k) {
    Arrays.sort(arr);
    int i = 0, j = arr.length - 1, p = 0;
    int median = arr[(arr.length - 1) / 2];
    int[] res = new int[k];
    while (p < k)
        if (median - arr[i] > arr[j] - median)
            res[p++] = arr[i++];  
        else
            res[p++] = arr[j--];      
    return res;
}
```
**Python**
```python
class Solution:
    def getStrongest(self, arr: List[int], k: int) -> List[int]:
        arr.sort()
        i, j = 0, len(arr) - 1
        median = arr[(len(arr) - 1) // 2]
        while len(arr) + i - j <= k:
            if median - arr[i] > arr[j] - median:
                i = i + 1
            else:
                j = j - 1
        return arr[:i] + arr[j + 1:]
```
**Complexity Analysis**
- Time: O(n log n) for the sorting.
- Memory: only what\'s needed for the sorting (implementation-specific), and output.

#### Bonus 1: O(n log k)
We can find a median in O(n) average complexity by using quick select. Then, we insert all elements in a sorted set, limiting the set size to `k`. Even better, we can use a partial sort algorithm that will move `k` strongest values to the front of the array.

```cpp
vector<int> getStrongest(vector<int>& arr, int k) {
    nth_element(begin(arr), begin(arr) + (arr.size() - 1) / 2, end(arr));
    int m = arr[(arr.size() -1) / 2];
    partial_sort(begin(arr), begin(arr) + k, end(arr), [&](int a, int b) { 
        return abs(a - m) == abs(b - m) ? a > b : abs(a - m) > abs(b - m); 
    });
    arr.resize(k);
    return arr;
}
```
#### Bonus 2: O(n + k log n)
Similar to the first bonus, but using a max heap. Why this is O(n + k log n):
- `nth_element` is quick select, which is O(n);
- `priority_queue` initialized with array is heapify, which is also O(n);
- Pulling from priority queue is O(log n), and we do it `k` times.

```cpp
vector<int> getStrongest(vector<int>& arr, int k) {
    nth_element(begin(arr), begin(arr) + (arr.size() - 1) / 2, end(arr));
    int m = arr[(arr.size() -1) / 2];
    auto cmp = [&](int a, int b) { 
        return abs(a - m) == abs(b - m) ? a < b : abs(a - m) < abs(b - m); 
    };
    priority_queue<int, vector<int>, decltype(cmp)> pq(begin(arr), end(arr), cmp);
    vector<int> res;
    while (res.size() < k) {
        res.push_back(pq.top());
        pq.pop();
    }
    return res;
}
```
#### Bonus 3: O(n)
The order in output does not need to be sorted. So, after we find our median, we can do another quick select to move k strongest elements to the beginning of the array.
> Note: quickselect has O(n) average time complexity, and O(n ^ 2) worst time complexity. It can be improved to O(n) worst time complexity using the median of medians approach to find a pivot. In practice though, a simpler randomization version is faster than median of medians, as the latter requires an extra O(n) scan to find a pivot.

```cpp
vector<int> getStrongest(vector<int>& arr, int k) {
    nth_element(begin(arr), begin(arr) + (arr.size() - 1) / 2, end(arr));
    int m = arr[(arr.size() -1) / 2];
    nth_element(begin(arr), begin(arr) + k, end(arr), [&](int a, int b) { 
        return abs(a - m) == abs(b - m) ? a > b : abs(a - m) > abs(b - m); 
    });
    arr.resize(k);
    return arr;
}
```
</p>


### Why not right definition of median
- Author: lee215
- Creation Date: Sun Jun 07 2020 12:04:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 07 2020 12:05:35 GMT+0800 (Singapore Standard Time)

<p>
`median = A[(n - 1) / 2]` ??

No, why did leetcode define median itself....

`median = (A[n/2] + A[(n-1)/2]) / 2`

Please noted that that\'s universal definition of "median"

Time `O(nlogn)`
Space `O(n)`

**C++**
```cpp
    vector<int> getStrongest(vector<int> A, int k) {
        sort(A.begin(), A.end());
        int n = A.size(), i = 0, j = n - 1, m = A[(n - 1) / 2];
        while (k--) {
            if (A[j] - m >= m - A[i])
                j--;
            else
                i++;
        }
        A.erase(A.begin() + i, A.begin() + j + 1);
        return A;
    }
```
</p>


### Learn randomized algorithm today which solves this problem in O(n).
- Author: interviewrecipes
- Creation Date: Sun Jun 07 2020 12:02:01 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 11 2020 02:17:50 GMT+0800 (Singapore Standard Time)

<p>
**Two Approaches: O(nlogn) and O(n) - Sort and Randomized Algorithm**

The problem is not complex to find the solution of but it can get complex in implementation. Basically you want to find the median of the array and then find k values that result in the maximum value for the requested operation.

**A Straightforward way - Sorting**
Sort the array.
Get the median i.e. ((n-1)/2)th element.
Create a new array where each element, new_arr[i] = abs(original_arr[i] -median)
Sort the new array.
Return the highest k values.

The tricky part is to handle the scenarios when values in new_array are the same. You then want to sort based the original array value. For this, you might have to write a custom comparison method depending on the programming language.

If you sort the entire array, then time complexity is O(nlogn).
Hence the time complexity of this solution is O(nlogn).

```
int median;
struct compare {
    bool operator()(const int &a, const int &b) {
        return (abs(a-median) > abs(b-median)) || (abs(a-median) == abs(b-median) && a>b);
    }
} compare;

class Solution {
public:    
    vector<int> getStrongest(vector<int>& arr, int k) {
        sort(arr.begin(), arr.end());                   // Sort first to find the median.
        median = arr[(arr.size()-1)/2];                 // Find median.
        sort(arr.begin(), arr.end(), compare);          // Sort again based on a specific way.
        return vector<int>(arr.begin(), arr.begin()+k); // Return the answer.
    }
};
```


**B. Advanced way - Randomized Algorithms**
You don\u2019t have to sort the entire array. There are only 2 things of interest.
The median. So you want `((n-1)/2)`th number in the array. There is a randomized algorithm that can give the result in O(n) time. (Basically how quicksort sorts the array by choosing a pivot element).
The strongest k values. An important thing to note here is that the order of those values is not important. The question states that any order is fine. So the same randomized algorithm helps here too.

Time complexity: O(n).


In C++, the method is nth_element for arranging first k-1 elements to be smaller than kth element & remaining elements at the end. Other languages must also have something similar.
What is the way in your language? I would be curious to know, put that in comments.

**Thanks**
Please upvote if you found this helpful.

```
int median;
struct compare {
    bool operator()(const int &a, const int &b) {
        return (abs(a-median) > abs(b-median)) || (abs(a-median) == abs(b-median) && a>b);
    }
} compare;

class Solution {
public:    
    vector<int> getStrongest(vector<int>& arr, int k) {
        nth_element(arr.begin(), arr.begin()+(arr.size()-1)/2, arr.end());  // Just find median element correctly.
        median = arr[(arr.size()-1)/2];                                                             // Get median.
        nth_element(arr.begin(), arr.begin()+k, arr.end(), compare);             // Sort just enough that k strongest are at the beginning.
        return vector<int>(arr.begin(), arr.begin()+k);                                   // Return the answer.
    }
};
```


**How Randomized Median Find Work**

1. It choses an element as pivot.
2. Shuffles the array so that all elements to the left of it are smaller than itself while the ones on the right are greater.
3. If that element turns out to be at the index \'k\' that we pass as a parameter,  the algorithm stops. At this time - we have the array with `k`-th element at correct place (the place had the array been sorted.) AND we have smaller elements on left side and larger on right. This is exactly what helps us here for solving this problem.
4. If the element is not at `k` we decide which part (left or right) to go into to get to the kth element.

Additional Info on ref: https://en.wikipedia.org/wiki/Partial_sorting
</p>


