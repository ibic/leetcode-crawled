---
title: "Maximum Equal Frequency"
weight: 1179
#id: "maximum-equal-frequency"
---
## Description
<div class="description">
<p>Given an array <code>nums</code>&nbsp;of positive integers, return the longest possible length of an array prefix of <code>nums</code>, such that it is possible to remove <strong>exactly one</strong> element from this prefix so that every number that has appeared in it will have the same number of occurrences.</p>

<p>If after removing one element there are no remaining elements, it&#39;s still considered that every appeared number has the same number of ocurrences (0).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [2,2,1,1,5,3,3,5]
<strong>Output:</strong> 7
<strong>Explanation:</strong> For the subarray [2,2,1,1,5,3,3] of length 7, if we remove nums[4]=5, we will get [2,2,1,1,3,3], so that each number will appear exactly twice.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,1,1,2,2,2,3,3,3,4,4,4,5]
<strong>Output:</strong> 13
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,1,1,2,2,2]
<strong>Output:</strong> 5
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [10,2,8,9,3,8,1,5,2,3,7,6]
<strong>Output:</strong> 8
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= nums.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- American Express - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy Python Solution, Concise 10 lines, Explained, O(N)
- Author: davyjing
- Creation Date: Sun Oct 13 2019 12:01:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Nov 16 2019 11:05:53 GMT+0800 (Singapore Standard Time)

<p>
```cnt``` records the occurence of each num, ```freq``` records the frequence of number of occurences. ```max_F``` is the largest frequence.
There are three cases which satify the condition:
1. all elements appear exact once.
2. all elements appear ```max_F``` times, except one appears once.
3. all elements appear ```max_F-1``` times, except one appears ```max_F```.
```
class Solution:
    def maxEqualFreq(self, nums: List[int]) -> int:
        cnt,freq,maxF,res = collections.defaultdict(int), collections.defaultdict(int),0,0
        for i,num in enumerate(nums):
            cnt[num] += 1
            freq[cnt[num]-1] -= 1
            freq[cnt[num]] += 1
            maxF = max(maxF,cnt[num])
            if maxF*freq[maxF] == i or (maxF-1)*(freq[maxF-1]+1) == i or maxF == 1:
                res = i + 1
        return res
```
</p>


### [Java/C++/Python] Only 2 Cases: Delete it or not
- Author: lee215
- Creation Date: Sun Oct 13 2019 12:30:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 13 2019 13:04:26 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
We need to count the frequency of numbers in `A`
Also we need to know, for each frequency, we have how many different numbers.
`count[a]` means the frequency of number `a`
`freq[c]` means how many numbers that occur `c` times.
<br>

## **Explanation**
Iterate the input array `A` and we count the `n` first numbers.

There actually only 2 situation to discuss:
1. we delete the current number `a`.
In this case, the `n - 1` first numbers have the same frequency,
and we can easier detect this case when we iterate the previous number `A[n - 1]`

2. we don\'t delete the current number `a`
the current `a` occurs `c` times.
So except all numbers that also occurs `c` times,
it should leave one single number, or `c + 1` same numeber.

That\'s it, done.
<br>

## **Complexity**
Time `O(N)`
Space `O(K)`, where `K = set(A).length`
<br>

**Java**
```java
    public int maxEqualFreq(int[] A) {
        int[] count = new int[100001], freq = new int[100001];
        int res = 0, N = A.length, a,c,d;
        for (int n = 1; n <= N; ++n) {
            a = A[n - 1];
            --freq[count[a]];
            c = ++count[a];
            ++freq[count[a]];

            if (freq[c] * c == n && n < N)
                res = n + 1;
            d = n - freq[c] * c;
            if ((d == c + 1 || d == 1) && freq[d] == 1)
                res = n;
        }
        return res;
    }
```

**C++**
```cpp
    int maxEqualFreq(vector<int>& A) {
        vector<int> count(100001), freq(100001);
        int res = 0, N = A.size(), a,c,d;
        for (int n = 1; n <= N; ++n) {
            a = A[n - 1];
            --freq[count[a]];
            c = ++count[a];
            ++freq[count[a]];

            if (freq[c] * c == n && n < N)
                res = n + 1;
            d = n - freq[c] * c;
            if ((d == c + 1 || d == 1) && freq[d] == 1)
                res = n;
        }
        return res;
    }
```

**Python:**
```python
    def maxEqualFreq(self, A):
        count = collections.Counter()
        freq = [0 for _ in xrange(len(A) + 1)]
        res = 0
        for n, a in enumerate(A, 1):
            freq[count[a]] -= 1
            freq[count[a] + 1] += 1
            c = count[a] = count[a] + 1
            if freq[c] * c == n and n < len(A):
                res = n + 1
            d = n - freq[c] * c
            if d in [c + 1, 1] and freq[d] == 1:
                res = n
        return res
```
<br>

## **Missing Test Case**
One more test case: `[1, 1, 2, 2, 3, 4, 3]`
</p>


### [Java/C++/Python] Easy to understand (compare counts)
- Author: tylertay
- Creation Date: Sun Oct 13 2019 15:36:42 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 16 2019 14:26:18 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Count frequency of the elements
We also need to count the number of elements with that frequency

# Explanation
There are 2 cases where we need to update the result:

Case 1:
frequency * (number of elements with that frequency) == length AND `i` != nums.length - 1
E.g. `[1,1,2,2,3]`
When the iteration is at index 3, the count will be equal to the length. It should update the result with (length + 1) as it should take an extra element in order to fulfil the condition.

Case 2:
frequency * (number of elements with that frequency) == length - 1
E.g. `[1,1,1,2,2,3]`
When the iteration is at index 4, the count will be equal to (length - 1). It should update the result with length as it fulfil the condition.

# Complexity
Time: `O(N)` where N is the number of elements
Space: `O(N)`

# Java
``` Java
public int maxEqualFreq(int[] nums) {
	Map<Integer, Integer> countMap = new HashMap<>();
	Map<Integer, Integer> freqMap = new HashMap<>();
	int res = 0;
	for (int i = 0; i < nums.length; i++) {
		// update counts
		countMap.put(nums[i], countMap.getOrDefault(nums[i], 0) + 1);
		int freq = countMap.get(nums[i]);
		// update counts with that frequency
		freqMap.put(freq, freqMap.getOrDefault(freq, 0) + 1);

		int count = freqMap.get(freq) * freq;
		if (count == i + 1 && i != nums.length - 1) { // case 1
			res = Math.max(res, i + 2);
		} else if (count == i) { // case 2
			res = Math.max(res, i + 1);
		}
	}
	return res;
}
```

# C++
``` C++
int maxEqualFreq(vector<int>& nums) {
	unordered_map<int, int> countMap;
	unordered_map<int, int> freqMap;
	int res = 0;
	for (int i = 0; i < nums.size(); i++) {
		// update counts
		countMap[nums[i]]++;
		int freq = countMap[nums[i]];
		// update counts with that frequency
		freqMap[freq]++;

		int count = freqMap[freq] * freq;
		if (count == i + 1 && i != nums.size() - 1) { // case 1
			res = max(res, i + 2);
		} else if (count == i) { // case 2
			res = max(res, i + 1);
		}
	}
	return res;
}
```

# Python
```Python
def maxEqualFreq(self, nums):
	counts, freq = collections.Counter(), collections.Counter()
	res = 0
	for i, num in enumerate(nums):
		# update counts
		counts[num] += 1
		# update counts with that frequency
		freq[counts[num]] += 1

		count = freq[counts[num]] * counts[num]
		if count == i + 1 and i != len(nums) - 1: # case 1
			res = max(res, i + 2)
		elif count == i: # case 2
			res = max(res, i + 1)
	return res
```

# Missing Test Cases
`[1,1,1,2,2,2,3,3,3]`

Thanks @fan42 for pointing out a corner case.
</p>


