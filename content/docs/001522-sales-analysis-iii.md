---
title: "Sales Analysis III"
weight: 1522
#id: "sales-analysis-iii"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Product</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| product_id   | int     |
| product_name | varchar |
| unit_price   | int     |
+--------------+---------+
product_id is the primary key of this table.
</pre>

<p>Table:&nbsp;<code>Sales</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| seller_id   | int     |
| product_id  | int     |
| buyer_id    | int     |
| sale_date   | date    |
| quantity    | int     |
| price       | int     |
+------ ------+---------+
This table has no primary key, it can have repeated rows.
product_id is a foreign key to Product table.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query that reports the <strong>products</strong>&nbsp;that were <strong>only</strong>&nbsp;sold in spring 2019. That is, between&nbsp;<strong>2019-01-01</strong> and <strong>2019-03-31</strong> inclusive.</p>

<p>The query result format is in the following example:</p>

<pre>
Product table:
+------------+--------------+------------+
| product_id | product_name | unit_price |
+------------+--------------+------------+
| 1          | S8           | 1000       |
| 2          | G4           | 800        |
| 3          | iPhone       | 1400       |
+------------+--------------+------------+

<code>Sales </code>table:
+-----------+------------+----------+------------+----------+-------+
| seller_id | product_id | buyer_id | sale_date  | quantity | price |
+-----------+------------+----------+------------+----------+-------+
| 1         | 1          | 1        | 2019-01-21 | 2        | 2000  |
| 1         | 2          | 2        | 2019-02-17 | 1        | 800   |
| 2         | 2          | 3        | 2019-06-02 | 1        | 800   |
| 3         | 3          | 4        | 2019-05-13 | 2        | 2800  |
+-----------+------------+----------+------------+----------+-------+

Result table:
+-------------+--------------+
| product_id  | product_name |
+-------------+--------------+
| 1           | S8           |
+-------------+--------------+
The product with id 1 was only sold in spring 2019 while the other two were sold after.</pre>

</div>

## Tags


## Companies
- Amazon - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### simple MySQL solution
- Author: henryzch88
- Creation Date: Thu Jun 20 2019 23:28:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 23 2019 08:05:35 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT s.product_id, product_name
FROM Sales s
LEFT JOIN Product p
ON s.product_id = p.product_id
GROUP BY s.product_id
HAVING MIN(sale_date) >= CAST(\'2019-01-01\' AS DATE) AND
       MAX(sale_date) <= CAST(\'2019-03-31\' AS DATE)
```
</p>


### Beat 95% Simple subquery
- Author: zzzzhu
- Creation Date: Sat Oct 05 2019 08:58:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 05 2019 08:58:11 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT product_id, product_name 
FROM Product 
WHERE product_id IN
(SELECT product_id
FROM Sales
GROUP BY product_id
HAVING MIN(sale_date) >= \'2019-01-01\' AND MAX(sale_date) <= \'2019-03-31\')
```
</p>


### simple mysql solution
- Author: Merciless
- Creation Date: Fri Jun 14 2019 11:55:12 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 14 2019 11:55:12 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT product_id, product_name
FROM product
JOIN SALES
USING(product_id)
GROUP BY product_id
HAVING sum(CASE WHEN sale_date between \'2019-01-01\' and \'2019-03-31\' THEN 1 ELSE 0 end) > 0
AND sum(CASE WHEN sale_date between \'2019-01-01\' and \'2019-03-31\' THEN 0 else 1 end) = 0
```
</p>


