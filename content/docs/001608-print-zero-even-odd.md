---
title: "Print Zero Even Odd"
weight: 1608
#id: "print-zero-even-odd"
---
## Description
<div class="description">
<p>Suppose you are given the following code:</p>

<pre>
class ZeroEvenOdd {
&nbsp; public ZeroEvenOdd(int n) { ... }&nbsp;     // constructor
  public void zero(printNumber) { ... }  // only output 0&#39;s
  public void even(printNumber) { ... }  // only output even numbers
  public void odd(printNumber) { ... }   // only output odd numbers
}
</pre>

<p>The same instance of <code>ZeroEvenOdd</code> will be passed to three different threads:</p>

<ol>
	<li>Thread A will call&nbsp;<code>zero()</code>&nbsp;which should only output 0&#39;s.</li>
	<li>Thread B will call&nbsp;<code>even()</code>&nbsp;which should only ouput even numbers.</li>
	<li>Thread C will call <code>odd()</code>&nbsp;which should only output odd numbers.</li>
</ol>

<p>Each of the threads is given a&nbsp;<code>printNumber</code> method to output&nbsp;an integer. Modify the given program to output the series&nbsp;<code>010203040506</code>... where the length of the series must be 2<em>n</em>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<b>Input:</b> n = 2
<b>Output:</b> &quot;0102&quot;
<strong>Explanation:</strong> There are three threads being fired asynchronously. One of them calls zero(), the other calls even(), and the last one calls odd(). &quot;0102&quot; is the correct output.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<b>Input:</b> n = 5
<b>Output:</b> &quot;0102030405&quot;
</pre>

</div>

## Tags


## Companies
- Microsoft - 2 (taggedByAdmin: false)
- Goldman Sachs - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Basic semaphore solution - 4ms, 35.8MB [Updated]
- Author: lk00100100
- Creation Date: Fri Jul 12 2019 10:22:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 13 2019 11:05:06 GMT+0800 (Singapore Standard Time)

<p>
Leetcode fixed the bug where you could:
*You\'ll notice that you dont need the "printZero" thread. You could just print a zero before you print an odd or an even number.*
So you can\'t do that anymore...

Non-shared variables (local to one thread) doesn\'t need synchronization.

Upvote and check out my other concurrency solutions.
```
import java.util.concurrent.*;
class ZeroEvenOdd {
    int n;
    
    Semaphore semaphoreEven, semaphoreOdd, semaphoreZero;
    
    public ZeroEvenOdd(int n) {
        this.n = n;
        
        semaphoreZero = new Semaphore(1);
        semaphoreEven = new Semaphore(0);
        semaphoreOdd = new Semaphore(0);
    }

    // printNumber.accept(x) outputs "x", where x is an integer.
    public void zero(IntConsumer printNumber) throws InterruptedException {
        int numTimes = this.n;
        boolean printOdd = true;
        
        for(int i = 0; i < numTimes; i++){
            semaphoreZero.acquire();
			
            printNumber.accept(0);

            //print the next number
            if(printOdd)
                semaphoreOdd.release();
            else
                semaphoreEven.release();
            
            printOdd = !printOdd;   //flip it!
        }
    }

    public void even(IntConsumer printNumber) throws InterruptedException {
        int numTimes = this.n / 2;
        
        int nextEvenNum = 2;
        for(int i = 0; i < numTimes; i++){
            semaphoreEven.acquire();
            
            printNumber.accept(nextEvenNum);
            nextEvenNum += 2;
            
            semaphoreZero.release();
        }
    }

    public void odd(IntConsumer printNumber) throws InterruptedException {
        int numTimes = (int)Math.ceil((double)this.n / 2);
        
        int nextOdd = 1;
        for(int i = 0; i < numTimes; i++){
            semaphoreOdd.acquire();
            
            printNumber.accept(nextOdd);
            nextOdd += 2;
            
            semaphoreZero.release();
        }
    }
}
```
</p>


### [Java/Python 3] Two clean codes either language.
- Author: rock
- Creation Date: Thu Jul 18 2019 22:38:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 21 2019 20:09:24 GMT+0800 (Singapore Standard Time)

<p>
**Method 1: Semaphore**

**Java:**
```
import java.util.concurrent.*;

class ZeroEvenOdd {

    private int n;
    private Semaphore zeroSem, oddSem, evenSem;
    
    public ZeroEvenOdd(int n) {
        this.n = n;
        zeroSem = new Semaphore(1);
        oddSem = new Semaphore(0);
        evenSem = new Semaphore(0);
    }

    // printNumber.accept(x) outputs "x", where x is an integer.
    public void zero(IntConsumer printNumber) throws InterruptedException {
        for (int i = 0; i < n; ++i) {
            zeroSem.acquire();
            printNumber.accept(0);
            (i % 2 == 0 ? oddSem : evenSem).release(); // Alternately release odd() and even().
        }
    }

    public void even(IntConsumer printNumber) throws InterruptedException {
        for (int i = 2; i <= n; i += 2) {
            evenSem.acquire();
            printNumber.accept(i);
            zeroSem.release();
        }
    }

    public void odd(IntConsumer printNumber) throws InterruptedException {
        for (int i = 1; i <= n; i += 2) {
            oddSem.acquire();
            printNumber.accept(i);
            zeroSem.release();
        }
    }
	
}
```
**Python 3:**
```
from threading import Semaphore

class ZeroEvenOdd:
    def __init__(self, n):
        self.n = n
        self.zeroSem = Semaphore() # default is 1.
        self.oddSem = Semaphore(0)       
        self.evenSem = Semaphore(0)       
        
	# printNumber(x) outputs "x", where x is an integer.
    def zero(self, printNumber: \'Callable[[int], None]\') -> None:
        for i in range(self.n):
            self.zeroSem.acquire()
            printNumber(0)
            (self.evenSem if i % 2 else self.oddSem).release()  # Alternately release oddSem and evenSem.   
        
    def even(self, printNumber: \'Callable[[int], None]\') -> None:
        for i in range(2, self.n + 1, 2):
            self.evenSem.acquire()
            printNumber(i)
            self.zeroSem.release()
        
    def odd(self, printNumber: \'Callable[[int], None]\') -> None:
        for i in range(1, self.n + 1, 2):
            self.oddSem.acquire()
            printNumber(i)
            self.zeroSem.release()
        
``` 
----

**Method 2:**
**Java Synchronization**
```
    private int n;
    private int sem = 0;
    
    public ZeroEvenOdd (int n) {
        this.n = n;
    }

    // printNumber.accept(x) outputs "x", where x is an integer.
    public synchronized void zero(IntConsumer printNumber) throws InterruptedException {
        for (int i = 0; i < n; ++i) {
            while (sem != 0) // need sem == 0 to unlock.
                wait();
            printNumber.accept(0);
            sem = i % 2 == 0 ? 1 : 2;
            notifyAll();
        }
    }

    public synchronized void even(IntConsumer printNumber) throws InterruptedException {
        for (int i = 2; i <= n; i += 2) {
            while (sem != 2) // need sem == 2 to unlock.
                wait();
            printNumber.accept(i);
            sem = 0;
            notifyAll();
        }
    }

    public synchronized void odd(IntConsumer printNumber) throws InterruptedException {
        for (int i = 1; i <= n; i += 2) {
            while (sem != 1) // need sem == 1 to unlock.
                wait();
            printNumber.accept(i);
            sem = 0;
            notifyAll();
        }
    }
```

**Python 3 Condition:**
```
from threading import Condition

class ZeroEvenOdd:
    def __init__(self, n):
        self.n = n
        self.sem = 0
        self.c = Condition()       
        
	# printNumber(x) outputs "x", where x is an integer.
    def zero(self, printNumber: \'Callable[[int], None]\') -> None:
        for i in range(self.n):
            with self.c:
                while self.sem: self.c.wait()
                printNumber(0)
                self.sem = 2 if i % 2 else 1  # Alternately turn to odd() and even().   
                self.c.notify_all()
        
    def even(self, printNumber: \'Callable[[int], None]\') -> None:
        for i in range(2, self.n + 1, 2):
            with self.c:
                while self.sem - 2: self.c.wait()
                printNumber(i)
                self.sem = 0
                self.c.notify_all()
        
    def odd(self, printNumber: \'Callable[[int], None]\') -> None:
        for i in range(1, self.n + 1, 2):
            with self.c:
                while self.sem - 1: self.c.wait()
                printNumber(i)
                self.sem = 0
                self.c.notify_all()
```
</p>


### 5 Python solutions using threading (Barrier, Condition, Event, Lock, Semaphore) with explanation
- Author: mereck
- Creation Date: Thu Jul 18 2019 02:17:02 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 18 2019 02:18:02 GMT+0800 (Singapore Standard Time)

<p>
Use a for loop to print `n` times from `zero` thread, `n//2` times from `even`, and `(n+1)//2` times from `odd` to form `2*n`-digit output. 

Use two two-thread barriers for the even / odd threads and a lock for the zero thread. Each time zero thread prints, it increments `self.ct` and determines whether to run `even` or `odd` thread next. Each time `even` or `odd` prints, it unlocks the zero thread. 

```
from threading import Barrier, Lock

class ZeroEvenOdd:
    def __init__(self, n):
        self.n = n
        self.ct = 0
        self.barriers = [Barrier(2),Barrier(2)]
        self.zero_lock = Lock()

    def zero(self, printNumber):
        for _ in range(self.n):
            self.zero_lock.acquire()
            printNumber(0)
            self.ct += 1
            self.barriers[self.ct % 2].wait()
        
    def even(self, printNumber):
        for _ in range(self.n//2):
            self.barriers[0].wait()
            printNumber(self.ct)
            self.zero_lock.release()
        
    def odd(self, printNumber):
        for _ in range((self.n+1)//2):
            self.barriers[1].wait()
            printNumber(self.ct)
            self.zero_lock.release()
```

Use three 0-value Semaphores to lock and unlock each thread.

```
from threading import Semaphore

class ZeroEvenOdd:
    def __init__(self, n):
        self.n = n
        self.ct = 0
        self.gates = [Semaphore(),Semaphore(),Semaphore()]
        self.gates[0].acquire()
        self.gates[1].acquire()
        
     def zero(self, printNumber):
        for _ in range(self.n):
            self.gates[2].acquire()
            printNumber(0)
            self.ct += 1
            self.gates[self.ct % 2].release()
        
    def even(self, printNumber):
        for _ in range(self.n//2):
            self.gates[0].acquire()
            printNumber(self.ct)
            self.gates[2].release()
        
    def odd(self, printNumber):
        for _ in range((self.n+1)//2):
            self.gates[1].acquire()
            printNumber(self.ct)
            self.gates[2].release()
```

The same can be done using Lock

```
from threading import Lock

class ZeroEvenOdd:
    def __init__(self, n):
        self.n = n
        self.ct = 0
        self.gates = [Lock(),Lock(),Lock()]
        self.gates[0].acquire()
        self.gates[1].acquire()
        
    def zero(self, printNumber):
        for _ in range(self.n):
            self.gates[2].acquire()
            printNumber(0)
            self.ct += 1
            self.gates[self.ct % 2].release()
        
    def even(self, printNumber):
        for _ in range(self.n//2):
            self.gates[0].acquire()
            printNumber(self.ct)
            self.gates[2].release()
        
    def odd(self, printNumber):
        for _ in range((self.n+1)//2):
            self.gates[1].acquire()
            printNumber(self.ct)
            self.gates[2].release()
```

Event works in a similar way but with `set`, `wait` and `clear` instead of `release`, `acquire`

```
from threading import Event

class ZeroEvenOdd:
    def __init__(self, n):
        self.n = n
        self.ct = 0
        self.print = [Event(),Event(),Event()]
        self.print[2].set()
        
    def zero(self, printNumber):
        for _ in range(self.n):
            self.print[2].wait()
            self.print[2].clear()
            printNumber(0)
            self.ct += 1
            self.print[self.ct % 2].set()
        
    def even(self, printNumber):
        for _ in range(self.n//2):
            self.print[0].wait()
            self.print[0].clear()
            printNumber(self.ct)
            self.print[2].set()
        
    def odd(self, printNumber):
        for _ in range((self.n+1)//2):
            self.print[1].wait()
            self.print[1].clear()
            printNumber(self.ct)
            self.print[2].set()
```

Use Condition to track the next thread to run in a shared variable

```
from threading import Condition

class ZeroEvenOdd:
    def __init__(self, n):
        self.n = n
        self.ct = 0
        self.condition = Condition()
        self.order = 2
        
    def zero(self, printNumber):
        for _ in range(self.n):
            with self.condition:
                self.condition.wait_for(lambda: self.order == 2)
                printNumber(0)
                self.ct += 1
                self.order = self.ct % 2
                self.condition.notify(2)
        
    def even(self, printNumber):
        for _ in range(self.n//2):
            with self.condition:
                self.condition.wait_for(lambda: self.order == 0)
                printNumber(self.ct)
                self.order = 2
                self.condition.notify(2)
        
    def odd(self, printNumber):
        for _ in range((self.n+1)//2):
            with self.condition:
                self.condition.wait_for(lambda: self.order == 1)
                printNumber(self.ct)
                self.order = 2
                self.condition.notify(2)
```
</p>


