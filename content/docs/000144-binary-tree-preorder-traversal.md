---
title: "Binary Tree Preorder Traversal"
weight: 144
#id: "binary-tree-preorder-traversal"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a binary tree, return <em>the preorder traversal of its nodes&#39; values</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/15/inorder_1.jpg" style="width: 202px; height: 324px;" />
<pre>
<strong>Input:</strong> root = [1,null,2,3]
<strong>Output:</strong> [1,2,3]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = []
<strong>Output:</strong> []
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [1]
<strong>Output:</strong> [1]
</pre>

<p><strong>Example 4:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/15/inorder_5.jpg" style="width: 202px; height: 202px;" />
<pre>
<strong>Input:</strong> root = [1,2]
<strong>Output:</strong> [1,2]
</pre>

<p><strong>Example 5:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/15/inorder_4.jpg" style="width: 202px; height: 202px;" />
<pre>
<strong>Input:</strong> root = [1,null,2]
<strong>Output:</strong> [1,2]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is in the range <code>[0, 100]</code>.</li>
	<li><code>-100 &lt;= Node.val &lt;= 100</code></li>
</ul>

<p>&nbsp;</p>

<p><strong>Follow up:</strong></p>

<p>Recursive solution is trivial, could you do it iteratively?</p>

<p>&nbsp;</p>

</div>

## Tags
- Stack (stack)
- Tree (tree)

## Companies
- Microsoft - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### How to traverse the tree


There are two general strategies to traverse a tree:

- *Breadth First Search* (`BFS`)

    We scan through the tree level by level, following the order of height,
    from top to bottom. The nodes on higher level would be visited before
    the ones with lower levels.
     
- *Depth First Search* (`DFS`)

    In this strategy, we adopt the `depth` as the priority, so that one
    would start from a root and reach all the way down to certain leaf,
    and then back to root to reach another branch.

    The DFS strategy can further be distinguished as
    `preorder`, `inorder`, and `postorder` depending on the relative order
    among the root node, left node and right node.
    
On the following figure the nodes are numerated in the order you visit them,
please follow ```1-2-3-4-5``` to compare different strategies.

![postorder](../Figures/145_transverse.png)

Here the problem is to implement preorder traversal using iterations.
<br />
<br />


---
#### Approach 1: Iterations

**Algorithm**

First of all, here is the definition of the ```TreeNode``` which we would use
in the following implementation.

<iframe src="https://leetcode.com/playground/aZ49GYLQ/shared" frameBorder="0" width="100%" height="225" name="aZ49GYLQ"></iframe>

Let's start from the root and then at each iteration 
pop the current node out of the stack and
push its child nodes. 
In the implemented strategy we push nodes into output list 
following the order ```Top->Bottom``` and ```Left->Right```, that
naturally reproduces preorder traversal.

<iframe src="https://leetcode.com/playground/emA62tse/shared" frameBorder="0" width="100%" height="429" name="emA62tse"></iframe>



**Complexity Analysis**

* Time complexity : we visit each node exactly once, thus the 
time complexity is $$\mathcal{O}(N)$$,
where $$N$$ is the number of nodes, *i.e.* the size of tree.

* Space complexity : depending on the tree structure, 
we could keep up to the entire tree, therefore, the space complexity is $$\mathcal{O}(N)$$.
<br />
<br />


---
#### Approach 2: Morris traversal

This approach is based on [Morris's article](https://www.sciencedirect.com/science/article/pii/0020019079900681) 
which is intended to optimize the space complexity. The algorithm does not use additional space 
for the computation, and the memory 
is only used to keep the output. 
If one prints the output directly along the computation, the space complexity 
would be $$\mathcal{O}(1)$$.

**Algorithm**

Here the idea is to go down from the node to its predecessor, 
and each predecessor will be visited twice.
For this go one step left if possible and then always right till the end.
When we visit a leaf (node's predecessor) first time, it has a zero right child, 
so we update output and establish 
the pseudo link ```predecessor.right = root``` to mark the fact the predecessor is visited.
When we visit the same predecessor the second time, it 
already points to the current node, thus we remove pseudo link and 
move right to the next node.

If the first one step left is impossible, 
update output and move right to next node.

<!--![LIS](../Figures/144/144_gif.gif)-->
!?!../Documents/144_LIS.json:1000,582!?!
 
<iframe src="https://leetcode.com/playground/Pq3E2abu/shared" frameBorder="0" width="100%" height="500" name="Pq3E2abu"></iframe>

**Complexity Analysis**

* Time complexity : we visit each predecessor exactly twice descending down 
from the node, thus the time complexity is $$\mathcal{O}(N)$$,
where $$N$$ is the number of nodes, *i.e.* the size of tree.

* Space complexity : we use no additional memory for the computation itself, 
but output list contains $$N$$ elements, and thus space complexity is $$\mathcal{O}(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted iterative solution in Java using stack.
- Author: pavel-shlyk
- Creation Date: Mon Dec 29 2014 23:03:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 10:03:29 GMT+0800 (Singapore Standard Time)

<p>
Note that in this solution only right children are stored to stack.

    public List<Integer> preorderTraversal(TreeNode node) {
		List<Integer> list = new LinkedList<Integer>();
		Stack<TreeNode> rights = new Stack<TreeNode>();
		while(node != null) {
			list.add(node.val);
			if (node.right != null) {
				rights.push(node.right);
			}
			node = node.left;
			if (node == null && !rights.isEmpty()) {
				node = rights.pop();
			}
		}
        return list;
    }
</p>


### 3 Different Solutions
- Author: fabrizio3
- Creation Date: Wed Apr 22 2015 15:50:47 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 10:07:21 GMT+0800 (Singapore Standard Time)

<p>
Recursive method with List as returning value:

    	public List<Integer> preorderTraversal(TreeNode root) {
    		List<Integer> pre = new LinkedList<Integer>();
    		if(root==null) return pre;
    		pre.add(root.val);
    		pre.addAll(preorderTraversal(root.left));
    		pre.addAll(preorderTraversal(root.right));
    		return pre;
    	}

Recursive method with Helper method to have a List as paramater, so we can modify the parameter and don't have to instantiate a new List at each recursive call:

    	public List<Integer> preorderTraversal(TreeNode root) {
    		List<Integer> pre = new LinkedList<Integer>();
    		preHelper(root,pre);
    		return pre;
    	}
    	public void preHelper(TreeNode root, List<Integer> pre) {
    		if(root==null) return;
    		pre.add(root.val);
    		preHelper(root.left,pre);
    		preHelper(root.right,pre);
    	}

Iterative method with Stack:

    	public List<Integer> preorderIt(TreeNode root) {
    		List<Integer> pre = new LinkedList<Integer>();
    		if(root==null) return pre;
    		Stack<TreeNode> tovisit = new Stack<TreeNode>();
    		tovisit.push(root);
    		while(!tovisit.empty()) {
    			TreeNode visiting = tovisit.pop();
    			pre.add(visiting.val);
    			if(visiting.right!=null) tovisit.push(visiting.right);
    			if(visiting.left!=null) tovisit.push(visiting.left);
    		}
    		return pre;
    	}
</p>


### Very simple iterative Python solution
- Author: clue
- Creation Date: Tue Jan 27 2015 12:17:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 22:36:23 GMT+0800 (Singapore Standard Time)

<p>
Classical usage of stack's LIFO feature, very easy to grasp:

    
    def preorderTraversal(self, root):
        ret = []
        stack = [root]
        while stack:
            node = stack.pop()
            if node:
                ret.append(node.val)
                stack.append(node.right)
                stack.append(node.left)
        return ret
</p>


