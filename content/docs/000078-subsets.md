---
title: "Subsets"
weight: 78
#id: "subsets"
---
## Description
<div class="description">
<p>Given a set of <strong>distinct</strong> integers, <em>nums</em>, return all possible subsets (the power set).</p>

<p><strong>Note:</strong> The solution set must not contain duplicate subsets.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3]
<strong>Output:</strong>
[
  [3],
&nbsp; [1],
&nbsp; [2],
&nbsp; [1,2,3],
&nbsp; [1,3],
&nbsp; [2,3],
&nbsp; [1,2],
&nbsp; []
]</pre>

</div>

## Tags
- Array (array)
- Backtracking (backtracking)
- Bit Manipulation (bit-manipulation)

## Companies
- Facebook - 17 (taggedByAdmin: true)
- Bloomberg - 8 (taggedByAdmin: true)
- Google - 5 (taggedByAdmin: false)
- ByteDance - 4 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Uber - 6 (taggedByAdmin: true)
- Adobe - 5 (taggedByAdmin: false)
- Yahoo - 4 (taggedByAdmin: false)
- Walmart Labs - 4 (taggedByAdmin: false)
- Lyft - 3 (taggedByAdmin: false)
- Coupang - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Solution Pattern

Let us first review the problems of Permutations / Combinations / Subsets,
since they are quite similar to each other and
there are some common strategies to solve them.

First, their solution space is often quite large:

- [Permutations](https://en.wikipedia.org/wiki/Permutation#k-permutations_of_n): 
$$N!$$. 

- [Combinations](https://en.wikipedia.org/wiki/Combination#Number_of_k-combinations): $$C_N^k = \frac{N!}{(N - k)! k!}$$

- Subsets: $$2^N$$, since each element could be absent or present. 

Given their exponential solution space, it is tricky to ensure that the generated solutions
are _**complete**_ and _**non-redundant**_.
It is essential to have a clear and easy-to-reason strategy.

There are generally three strategies to do it:

- Recursion

- Backtracking

- Lexicographic generation based on the mapping between binary bitmasks and the corresponding  
permutations / combinations / subsets.

As one would see later, the third method could be a good candidate for the interview
because it simplifies the problem to the generation of binary numbers,
therefore it is easy to implement and verify that no solution is missing.

Besides, this method has the best time complexity,
and as a bonus, it generates lexicographically sorted output for the sorted inputs.
<br />
<br />


---
#### Approach 1: Cascading

**Intuition**

Let's start from empty subset in output list.
At each step one takes new integer into consideration and generates
new subsets from the existing ones. 

![diff](../Figures/78/recursion.png)

**Implementation**

<iframe src="https://leetcode.com/playground/NvoqKtdz/shared" frameBorder="0" width="100%" height="344" name="NvoqKtdz"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N \times 2^N)$$ to generate all subsets 
and then copy them into output list. 
    
* Space complexity: $$\mathcal{O}(N \times 2^N)$$. This is exactly the number of solutions for subsets
multiplied by the number $$N$$ of elements to keep for each subset.  
    - For a given number, it could be present or absent (_i.e._ binary choice) in a subset solution.
    As as result, for $$N$$ numbers, we would have in total $$2^N$$ choices (solutions). 
<br />
<br />


---
#### Approach 2: Backtracking

**Algorithm**

>Power set is all possible combinations of all possible _lengths_, from 0 to n.

Given the definition, the problem can also be interpreted as finding the _power set_ from a sequence.

So, this time let us loop over _the length of combination_,
rather than the candidate numbers, and generate 
all combinations for a given length with the help of _backtracking_ technique.

![diff](../Figures/78/combinations.png)

>[Backtracking](https://en.wikipedia.org/wiki/Backtracking) 
is an algorithm for finding all
solutions by exploring all potential candidates.
If the solution candidate turns to be _not_ a solution 
(or at least not the _last_ one), 
backtracking algorithm discards it by making some changes 
on the previous step, *i.e.* _backtracks_ and then try again.

![diff](../Figures/78/backtracking.png)

**Algorithm**

We define a backtrack function named `backtrack(first, curr)`
which takes the index of first element to add and 
a current combination as arguments.

- If the current combination is done, we add the combination to the final output.

- Otherwise, we iterate over the indexes `i` from `first` to the length of the entire sequence `n`.

    - Add integer `nums[i]` into the current combination `curr`.

    - Proceed to add more integers into the combination : 
    `backtrack(i + 1, curr)`.

    - Backtrack by removing `nums[i]` from `curr`.

**Implementation**

<iframe src="https://leetcode.com/playground/GpGSxz9C/shared" frameBorder="0" width="100%" height="500" name="GpGSxz9C"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N \times 2^N)$$ to generate all subsets 
and then copy them into output list.
 
* Space complexity: $$\mathcal{O}(N \times 2^N)$$ to keep all the subsets
of length $$N$$, 
since each of $$N$$ elements could be present or absent. 
<br />
<br />


---
#### Approach 3: Lexicographic (Binary Sorted) Subsets

**Intuition**

The idea of this solution is originated from [Donald E. Knuth](https://www-cs-faculty.stanford.edu/~knuth/taocp.html).

>The idea is that we map each subset to a bitmask of length n,
where `1` on the i*th* position in bitmask means the presence of `nums[i]`
in the subset, and `0` means its absence. 

![diff](../Figures/78/bitmask4.png)

For instance, the bitmask `0..00` (all zeros) corresponds to an empty subset, 
and the bitmask `1..11` (all ones) corresponds to the entire input array `nums`. 

Hence to solve the initial problem, we just need to generate n bitmasks
from `0..00` to `1..11`. 

It might seem simple at first glance to generate binary numbers, but 
the real problem here is how to deal with 
[zero left padding](https://en.wikipedia.org/wiki/Padding_(cryptography)#Zero_padding),
because one has to generate bitmasks of fixed length, _i.e._ `001` and not just `1`.
For that one could use standard bit manipulation trick:

<iframe src="https://leetcode.com/playground/oXyiovQx/shared" frameBorder="0" width="100%" height="123" name="oXyiovQx"></iframe>

or keep it simple stupid and shift iteration limits:

<iframe src="https://leetcode.com/playground/juFAhCen/shared" frameBorder="0" width="100%" height="106" name="juFAhCen"></iframe>

**Algorithm**

- Generate all possible binary bitmasks of length n.

- Map a subset to each bitmask: 
`1` on the i*th* position in bitmask means the presence of `nums[i]`
in the subset, and `0` means its absence. 

- Return output list.

**Implementation**

<iframe src="https://leetcode.com/playground/udkGup4y/shared" frameBorder="0" width="100%" height="378" name="udkGup4y"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N \times 2^N)$$ to generate all subsets 
and then copy them into output list.
    
* Space complexity: $$\mathcal{O}(N \times 2^N)$$ to keep all the subsets
of length $$N$$, 
since each of $$N$$ elements could be present or absent. 
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A general approach to backtracking questions in Java (Subsets, Permutations, Combination Sum, Palindrome Partitioning)
- Author: issac3
- Creation Date: Mon May 23 2016 23:51:29 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 14:13:41 GMT+0800 (Singapore Standard Time)

<p>
This structure might apply to many other backtracking questions, but here I am just going to demonstrate Subsets, Permutations, and Combination Sum. 

Subsets : [https://leetcode.com/problems/subsets/][1]

    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, 0);
        return list;
    }
    
    private void backtrack(List<List<Integer>> list , List<Integer> tempList, int [] nums, int start){
        list.add(new ArrayList<>(tempList));
        for(int i = start; i < nums.length; i++){
            tempList.add(nums[i]);
            backtrack(list, tempList, nums, i + 1);
            tempList.remove(tempList.size() - 1);
        }
    }


Subsets II (contains duplicates) : [https://leetcode.com/problems/subsets-ii/][2]

    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, 0);
        return list;
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums, int start){
        list.add(new ArrayList<>(tempList));
        for(int i = start; i < nums.length; i++){
            if(i > start && nums[i] == nums[i-1]) continue; // skip duplicates
            tempList.add(nums[i]);
            backtrack(list, tempList, nums, i + 1);
            tempList.remove(tempList.size() - 1);
        }
    } 


----------

Permutations : [https://leetcode.com/problems/permutations/][3]

    public List<List<Integer>> permute(int[] nums) {
       List<List<Integer>> list = new ArrayList<>();
       // Arrays.sort(nums); // not necessary
       backtrack(list, new ArrayList<>(), nums);
       return list;
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums){
       if(tempList.size() == nums.length){
          list.add(new ArrayList<>(tempList));
       } else{
          for(int i = 0; i < nums.length; i++){ 
             if(tempList.contains(nums[i])) continue; // element already exists, skip
             tempList.add(nums[i]);
             backtrack(list, tempList, nums);
             tempList.remove(tempList.size() - 1);
          }
       }
    } 

Permutations II (contains duplicates) : [https://leetcode.com/problems/permutations-ii/][4]

    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, new boolean[nums.length]);
        return list;
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums, boolean [] used){
        if(tempList.size() == nums.length){
            list.add(new ArrayList<>(tempList));
        } else{
            for(int i = 0; i < nums.length; i++){
                if(used[i] || i > 0 && nums[i] == nums[i-1] && !used[i - 1]) continue;
                used[i] = true; 
                tempList.add(nums[i]);
                backtrack(list, tempList, nums, used);
                used[i] = false; 
                tempList.remove(tempList.size() - 1);
            }
        }
    }


----------

Combination Sum : [https://leetcode.com/problems/combination-sum/][5]

    public List<List<Integer>> combinationSum(int[] nums, int target) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, target, 0);
        return list;
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums, int remain, int start){
        if(remain < 0) return;
        else if(remain == 0) list.add(new ArrayList<>(tempList));
        else{ 
            for(int i = start; i < nums.length; i++){
                tempList.add(nums[i]);
                backtrack(list, tempList, nums, remain - nums[i], i); // not i + 1 because we can reuse same elements
                tempList.remove(tempList.size() - 1);
            }
        }
    }

Combination Sum II (can't reuse same element) : [https://leetcode.com/problems/combination-sum-ii/][6]

    public List<List<Integer>> combinationSum2(int[] nums, int target) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, target, 0);
        return list;
        
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums, int remain, int start){
        if(remain < 0) return;
        else if(remain == 0) list.add(new ArrayList<>(tempList));
        else{
            for(int i = start; i < nums.length; i++){
                if(i > start && nums[i] == nums[i-1]) continue; // skip duplicates
                tempList.add(nums[i]);
                backtrack(list, tempList, nums, remain - nums[i], i + 1);
                tempList.remove(tempList.size() - 1); 
            }
        }
    } 


Palindrome Partitioning : [https://leetcode.com/problems/palindrome-partitioning/][7]

    public List<List<String>> partition(String s) {
       List<List<String>> list = new ArrayList<>();
       backtrack(list, new ArrayList<>(), s, 0);
       return list;
    }
    
    public void backtrack(List<List<String>> list, List<String> tempList, String s, int start){
       if(start == s.length())
          list.add(new ArrayList<>(tempList));
       else{
          for(int i = start; i < s.length(); i++){
             if(isPalindrome(s, start, i)){
                tempList.add(s.substring(start, i + 1));
                backtrack(list, tempList, s, i + 1);
                tempList.remove(tempList.size() - 1);
             }
          }
       }
    }
    
    public boolean isPalindrome(String s, int low, int high){
       while(low < high)
          if(s.charAt(low++) != s.charAt(high--)) return false;
       return true;
    } 



  [1]: https://leetcode.com/problems/subsets/
  [2]: https://leetcode.com/problems/subsets-ii/
  [3]: https://leetcode.com/problems/permutations/
  [4]: https://leetcode.com/problems/permutations-ii/
  [5]: https://leetcode.com/problems/combination-sum/
  [6]: https://leetcode.com/problems/combination-sum-ii/
  [7]: https://leetcode.com/problems/palindrome-partitioning/
</p>


### C++ Recursive/Iterative/Bit-Manipulation
- Author: jianchao-li
- Creation Date: Sat Jul 18 2015 23:01:38 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 23:21:44 GMT+0800 (Singapore Standard Time)

<p>
**Recursive (Backtracking)**

```cpp
class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) {
        vector<vector<int>> subs;
        vector<int> sub;
        subsets(nums, 0, sub, subs);
        return subs;
    }
private:
    void subsets(vector<int>& nums, int i, vector<int>& sub, vector<vector<int>>& subs) {
        subs.push_back(sub);
        for (int j = i; j < nums.size(); j++) {
            sub.push_back(nums[j]);
            subsets(nums, j + 1, sub, subs);
            sub.pop_back();
        }
    }
};
```

**Iterative**

Using `[1, 2, 3]` as an example, the iterative process is like:

 1. Initially, one empty subset `[[]]`
 2. Adding `1` to `[]`: `[[], [1]]`;
 3. Adding `2` to `[]` and `[1]`: `[[], [1], [2], [1, 2]]`;
 4. Adding `3` to `[]`, `[1]`, `[2]` and `[1, 2]`: `[[], [1], [2], [1, 2], [3], [1, 3], [2, 3], [1, 2, 3]]`.

```cpp
class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) {
        vector<vector<int>> subs = {{}};
        for (int num : nums) {
            int n = subs.size();
            for (int i = 0; i < n; i++) {
                subs.push_back(subs[i]); 
                subs.back().push_back(num);
            }
        }
        return subs;
    }
}; 
```

**Bit Manipulation**

To give all the possible subsets, we just need to exhaust all the possible combinations of the numbers. And each number has only two possibilities: either in or not in a subset. And this can be represented using a bit.

Using `[1, 2, 3]` as an example, `1` appears once in every two consecutive subsets, `2` appears twice in every four consecutive subsets, and `3` appears four times in every eight subsets (initially all subsets are empty):

```
[], [ ], [ ], [    ], [ ], [    ], [    ], [       ]
[], [1], [ ], [1   ], [ ], [1   ], [    ], [1      ]
[], [1], [2], [1, 2], [ ], [1   ], [2   ], [1, 2   ]
[], [1], [2], [1, 2], [3], [1, 3], [2, 3], [1, 2, 3]
```

```cpp
class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) {
        int n = nums.size(), p = 1 << n;
        vector<vector<int>> subs(p);
        for (int i = 0; i < p; i++) {
            for (int j = 0; j < n; j++) {
                if ((i >> j) & 1) {
                    subs[i].push_back(nums[j]);
                }
            }
        }
        return subs;
    }
};
```
</p>


### 3ms, easiest solution, no backtracking, no bit manipulation, no dfs, no bullshit
- Author: sikp
- Creation Date: Sun Apr 08 2018 00:42:18 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 07:02:05 GMT+0800 (Singapore Standard Time)

<p>
While iterating through all numbers, for each new number, we can either pick it or not pick it
1, if pick, just add current number to every existing subset.
2, if not pick, just leave all existing subsets as they are.
We just combine both into our result.

For example, {1,2,3} intially we have an emtpy set as result [ [ ] ]
Considering 1, if not use it, still [ ], if use 1, add it to [ ], so we have [1] now
Combine them, now we have [ [ ], [1] ] as all possible subset

Next considering 2, if not use it, we still have [ [ ], [1] ], if use 2, just add 2 to each previous subset, we have [2], [1,2]
Combine them, now we have [ [ ], [1], [2], [1,2] ]

Next considering 3, if not use it, we still have  [ [ ], [1], [2], [1,2] ], if use 3, just add 3 to each previous subset, we have [ [3], [1,3], [2,3], [1,2,3] ]
Combine them, now we have  [ [ ], [1], [2], [1,2], [3], [1,3], [2,3], [1,2,3] ]

```
public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        result.add(new ArrayList<>());
        for(int n : nums){
            int size = result.size();
            for(int i=0; i<size; i++){
                List<Integer> subset = new ArrayList<>(result.get(i));
                subset.add(n);
                result.add(subset);
            }
        }
        return result;
    }
```



</p>


