---
title: "Minimum Numbers of Function Calls to Make Target Array"
weight: 1409
#id: "minimum-numbers-of-function-calls-to-make-target-array"
---
## Description
<div class="description">
<p><img alt="" src="https://assets.leetcode.com/uploads/2020/07/10/sample_2_1887.png" style="width: 573px; height: 294px;" /></p>

<p>Your task is to form&nbsp;an integer array <code>nums</code> from an initial array of zeros&nbsp;<code>arr</code> that is the&nbsp;same size&nbsp;as <code>nums</code>.</p>

<p>Return the minimum number of&nbsp;function calls to make <code>nums</code> from <code>arr</code>.</p>

<p>The answer is guaranteed to fit in a 32-bit signed integer.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,5]
<strong>Output:</strong> 5
<strong>Explanation:</strong> Increment by 1 (second element): [0, 0] to get [0, 1] (1 operation).
Double all the elements: [0, 1] -&gt; [0, 2] -&gt; [0, 4] (2 operations).
Increment by 1 (both elements)  [0, 4] -&gt; [1, 4] -&gt; <strong>[1, 5]</strong> (2 operations).
Total of operations: 1 + 2 + 2 = 5.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [2,2]
<strong>Output:</strong> 3
<strong>Explanation:</strong> Increment by 1 (both elements) [0, 0] -&gt; [0, 1] -&gt; [1, 1] (2 operations).
Double all the elements: [1, 1] -&gt; <strong>[2, 2]</strong> (1 operation).
Total of operations: 2 + 1 = 3.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [4,2,5]
<strong>Output:</strong> 6
<strong>Explanation:</strong> (initial)[0,0,0] -&gt; [1,0,0] -&gt; [1,0,1] -&gt; [2,0,2] -&gt; [2,1,2] -&gt; [4,2,4] -&gt; <strong>[4,2,5]</strong>(nums).
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,2,2,4]
<strong>Output:</strong> 7
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> nums = [2,4,8,16]
<strong>Output:</strong> 8
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10^5</code></li>
	<li><code>0 &lt;= nums[i] &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Bit Counts
- Author: lee215
- Creation Date: Sun Aug 23 2020 00:05:01 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 25 2020 14:14:01 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Think in backward way,
for each number `a` in A,
if `a % 2 == 1`, we do operation 0 backward, turning 1 to 0.
If all `a % 2 == 0`, we do operation 1 backward.

Some observation here:
1. For each bit "1" in the bianry format of `a`,
we need at least one operation 0.
2. All operation 1 can be shared.

# **Explanation**
For each number `a`,
we count the number of bits "1",
as well as the length of `a` in binary format.

The number of operation 0 equals to the total number of bits "1".
The number of operation 1 equals to maximum bit length - 1.
<br>

# **Complexity**
Time `O(Nlog(10^9)))`
Space `O(1)`
<br>

**Java:**
```java
    public int minOperations(int[] A) {
        int res = 0, maxLen = 1;
        for (int a : A) {
            int bits = 0;
            while (a > 0) {
                res += a & 1;
                bits++;
                a >>= 1;
            }
            maxLen = Math.max(maxLen, bits);
        }
        return res + maxLen - 1;
    }
```
**Java, using build-in**
By @FrenkieDeJong
```java
    public int minOperations(int[] A) {
        int res = 0, max = 0;
        for (int a : A) {
            if (a == 0) continue;
            res += Integer.bitCount(a);
            max = Math.max(max, Integer.numberOfTrailingZeros(Integer.highestOneBit(a)));
        }
        return res + max;
    }
```
**C++:**
```cpp
    int minOperations(vector<int>& A) {
        int res = 0, maxLen = 1;
        for (int a : A) {
            int bits = 0;
            while (a > 0) {
                res += a & 1;
                bits++;
                a >>= 1;
            }
            maxLen = max(maxLen, bits);
        }
        return res + maxLen - 1;
    }
```

**Python:**
```py
    def minOperations(self, A):
        return sum(bin(a).count(\'1\') for a in A) + len(bin(max(A))) - 3
```

</p>


### Simple Explanation
- Author: khaufnak
- Creation Date: Sun Aug 23 2020 00:00:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 23 2020 14:51:17 GMT+0800 (Singapore Standard Time)

<p>
```(Minimum number of operations) = minimum((number of +1 operations) +  (number of x2 operations)) ```

```number of x2 operations``` = ```(highest set bit in binary representation)``` because each x2 operation shifts the binary digit to left, hence highest set bit will be number of x2 operations we will have to perform. By only performing (highest set bit) number of operations, we reuse x2 operation across all nums.

```number of +1 operations``` = ```(number of 1s in the binary representation of each number)``` because each 1 in binary representation is introduced by +1 operation.
```
class Solution {
    public int minOperations(int[] nums) {
        int addOneOperations = 0; // +1 operations
        int highestSetBit = 0; // x2 operations
        for (int bit = 0; bit <= 30; ++bit) {
            for (int num : nums) {
                if ((num & (1 << bit)) != 0) {
                    addOneOperations++;
                    highestSetBit = bit;
                }
            }
        }
        return addOneOperations + highestSetBit; // (+1 operations) + (x2 operations)
    }
}
```
Complexity: ```O(nlog(max(nums))```

</p>


### [Java] Explanation with VIDEO & comments, O(n*log(m))
- Author: asher23
- Creation Date: Sun Aug 23 2020 00:33:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 23 2020 01:44:42 GMT+0800 (Singapore Standard Time)

<p>
Work backwards, if any number is odd, subtract 1. If even, divide by 2. 
EXAMPLE:
[4,2,5]
1. [4,2,4] + 1 operation, previous had odd, so subtract 1 
2. [2,1,2] + 1 op, previous had all even, divide by 2
3. [2,0,2] + 1 op, previous had odd, so subtract 1
4. [1,0,1] + 1 op , previous had all even, divide by 2
5. [0,0,1] + 1 op , previous had odd, so subtract 1 
6. [0,0,0] + 1 op , previous had odd, so subtract 1 

That is 6 operations. 

But we can\'t really iterate like that in code, so instead, we iterate through the numbers individually and for each one, we divide by 2 and subtract by 1 each time.

-> If we divide, we ONLY increase the count if it\'s greater than how many divides we previously had. That\'s because the divide operation happens across every number unlike the subtract operation. So if we divided previously, that means this number had been divided already, so we shouldn\'t increment count.

-> If we subtract, we increase count each time.

Once the number hits 0, we can stop.
```
class Solution {
    public int minOperations(int[] nums) {
        // WORK BACKWARDS
        int count = 0;
        int currDivides = 0; // how many times have we currently divided by 2
        
        // The most a number has been divided by 2      
        int maxDivides = Integer.MIN_VALUE;//(basically dependant on the largest number)

        for (int i = 0; i < nums.length; i++) {
            currDivides = 0; // reset to 0
            int n = nums[i];
            while (n > 0) { // while the number hasn\'t hit zero
                
                if (n % 2 == 0) { // if its even, divide by 2
                    n = n / 2;
                    currDivides++;
                    
                    // if our current number of divides is greater than the max, we can increase our count
                    if (currDivides > maxDivides) {
                        count++;
                        maxDivides = currDivides;
                    }
                } else { // if its odd, subtract 11
                    n--;
                    count++;
                }
            }
        }
        return count;
    }
}
```

<iframe width="966" height="604" src="https://www.youtube.com/embed/_2OI6es836M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>


