---
title: "Encode and Decode TinyURL"
weight: 503
#id: "encode-and-decode-tinyurl"
---
## Description
<div class="description">
<blockquote>Note: This is a companion problem to the <a href="https://leetcode.com/discuss/interview-question/system-design/" target="_blank">System Design</a> problem: <a href="https://leetcode.com/discuss/interview-question/124658/Design-a-URL-Shortener-(-TinyURL-)-System/" target="_blank">Design TinyURL</a>.</blockquote>

<p>TinyURL is a URL shortening service where you enter a URL such as <code>https://leetcode.com/problems/design-tinyurl</code> and it returns a short URL such as <code>http://tinyurl.com/4e9iAk</code>.</p>

<p>Design the <code>encode</code> and <code>decode</code> methods for the TinyURL service. There is no restriction on how your encode/decode algorithm should work. You just need to ensure that a URL can be encoded to a tiny URL and the tiny URL can be decoded to the original URL.</p>

</div>

## Tags
- Hash Table (hash-table)
- Math (math)

## Companies
- Amazon - 3 (taggedByAdmin: true)
- Bloomberg - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Microsoft - 5 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Using Simple Counter[Accepted]

In order to encode the URL, we make use of a counter($$i$$), which is incremented for every new URL encountered. We put the URL along with its encoded count($$i$$) in a HashMap. This way we can retrieve it later at the time of decoding easily.

<iframe src="https://leetcode.com/playground/BEsJT7hF/shared" frameBorder="0" name="BEsJT7hF" width="100%" height="275"></iframe>

**Performance Analysis**

* The range of URLs that can be decoded is limited by the range of $$\text{int}$$.

* If excessively large number of URLs have to be encoded, after the range of $$\text{int}$$ is exceeded, integer overflow could lead to overwriting the previous URLs' encodings, leading to the performance degradation.

* The length of the URL isn't necessarily shorter than the incoming $$\text{longURL}$$. It is only dependent on the relative order in which the URLs are encoded.

* One problem with this method is that it is very easy to predict the next code generated, since the pattern can be detected by generating a few encoded URLs.


---
#### Approach #2 Variable-length Encoding[Accepted]

**Algorithm**

In this case, we make use of variable length encoding to encode the given URLs. For every $$\text{longURL}$$, we choose a variable codelength for the input URL, which can be any length between 0 and 61. Further, instead of using only numbers as the Base System for encoding the URLSs, we make use of a set of integers and alphabets to be used for encoding.



<iframe src="https://leetcode.com/playground/wbFtiFeG/shared" frameBorder="0" name="wbFtiFeG" width="100%" height="515"></iframe>

**Performance Analysis**

* The number of URLs that can be encoded is, again, dependent on the range of $$\text{int}$$, since, the same $$count$$ will be generated after overflow of integers.

* The length of the encoded URLs isn't necessarily short, but is to some extent dependent on the order in which the incoming $$\text{longURL}$$'s are encountered. For example, the codes generated will have the lengths in the following order: 1(62 times), 2(62 times) and so on.

* The performance is quite good, since the same code will be repeated only after the integer overflow limit, which is quite large.

* In this case also, the next code generated could be predicted by the use of some calculations.

---
#### Approach #3 Using hashcode[Accepted]

**Algorithm**

In this method, we make use of an inbuilt function $$\text{hashCode()}$$ to determine a code for mapping every URL. Again, the mapping is stored in a HashMap for decoding.

The hash code for a String object is computed(using int arithmetic) as −

$$s[0]*31^{(n - 1)} + s[1]*31^{(n - 2)} + ... + s[n - 1]$$ , where s[i] is the ith character of the string, n is the length of the string.

<iframe src="https://leetcode.com/playground/gzRtcrHu/shared" frameBorder="0" name="gzRtcrHu" width="100%" height="292"></iframe>

**Performance Analysis**

* The number of URLs that can be encoded is limited by the range of $$\text{int}$$, since $$\text{hashCode}$$ uses integer calculations.

* The average length of the encoded URL isn't directly related to the incoming $$\text{longURL}$$ length.

* The $$\text{hashCode()}$$ doesn't generate unique codes for different string. This property of getting the same code for two different inputs is called collision. Thus, as the number of encoded URLs increases, the probability of collisions increases, which leads to failure.

* The following figure demonstrates the mapping of different objects to the same hashcode and the increasing probability of collisions with increasing number of objects.

![Encode_and_Decode_URLs](../Figures/535_Encode_and_Decode.png)

* Thus, it isn't necessary that the collisions start occuring only after a certain number of strings have been encoded, but they could occur way before the limit is even near to the $$\text{int}$$. This is similar to birthday paradox i.e. the probability of two people having the same birthday is nearly 50% if we consider only 23 people and 99.9% with just 70 people.

* Predicting the encoded URL isn't easy in this scheme.

---
#### Approach #4 Using random number[Accepted]

**Algorithm**

In this case, we generate a random integer to be used as the code. In case the generated code happens to be already mapped to some previous $$\text{longURL}$$, we generate a new random integer to be used as the code. The data is again stored in a HashMap to help in the decoding process.

<iframe src="https://leetcode.com/playground/CdZ9PRZt/shared" frameBorder="0" name="CdZ9PRZt" width="100%" height="377"></iframe>
**Performance Analysis**

* The number of URLs that can be encoded is limited by the range of $$\text{int}$$.

* The average length of the codes generated is independent of the $$\text{longURL}$$'s length, since a random integer is used.

* The length of the URL isn't necessarily shorter than the incoming $$\text{longURL}$$. It is only dependent on the relative order in which the URLs are encoded.

* Since a random number is used for coding, again, as in the previous case, the number of collisions could increase with the increasing number of input strings, leading to performance degradation.

* Determining the encoded URL isn't possible in this scheme, since we make use of random numbers.

---
#### Approach #5 Random fixed-length encoding[Accepted]

**Algorithm**

In this case, again, we make use of the set of numbers and alphabets to generate the coding for the given URLs, similar to Approach 2. But in this case, the length of the code is fixed to 6 only. Further, random characters from the string to form the characters of the code. In case, the code generated collides with some previously generated code, we form a new random code.

<iframe src="https://leetcode.com/playground/u5oj9dfd/shared" frameBorder="0" name="u5oj9dfd" width="100%" height="515"></iframe>

**Performance Analysis**

* The number of URLs that can be encoded is quite large in this case, nearly of the order $$(10+26*2)^6$$.

* The length of the encoded URLs is fixed to 6 units, which is a significant reduction for very large URLs.

* The performance of this scheme is quite good, due to a very less probability of repeated same codes generated.

* We can increase the number of encodings possible as well, by increasing the length of the encoded strings. Thus, there exists a  tradeoff between the length of the code and the number of encodings possible.

* Predicting the encoding isn't possible in this scheme since random numbers are used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Two solutions and thoughts
- Author: StefanPochmann
- Creation Date: Sun Mar 05 2017 04:53:09 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 09:50:20 GMT+0800 (Singapore Standard Time)

<p>
My first solution produces short URLs like `http://tinyurl.com/0`, `http://tinyurl.com/1`, etc, in that order.
```
class Codec:

    def __init__(self):
        self.urls = []

    def encode(self, longUrl):
        self.urls.append(longUrl)
        return 'http://tinyurl.com/' + str(len(self.urls) - 1)

    def decode(self, shortUrl):
        return self.urls[int(shortUrl.split('/')[-1])]
```
Using increasing numbers as codes like that is simple but has some disadvantages, which the below solution fixes:
- If I'm asked to encode the same long URL several times, it will get several entries. That wastes codes and memory.
- People can find out how many URLs have already been encoded. Not sure I want them to know.
- People might try to get special numbers by spamming me with repeated requests shortly before their desired number comes up.
- Only using digits means the codes can grow unnecessarily large. Only offers a million codes with length 6 (or smaller). Using six digits or lower or upper case letters would offer (10+26*2)<sup>6</sup> = 56,800,235,584 codes with length 6.

The following solution doesn't have these problems. It produces short URLs like `http://tinyurl.com/KtLa2U`, using a random code of six digits or letters. If a long URL is already known, the existing short URL is used and no new entry is generated.
```
class Codec:

    alphabet = string.ascii_letters + '0123456789'

    def __init__(self):
        self.url2code = {}
        self.code2url = {}

    def encode(self, longUrl):
        while longUrl not in self.url2code:
            code = ''.join(random.choice(Codec.alphabet) for _ in range(6))
            if code not in self.code2url:
                self.code2url[code] = longUrl
                self.url2code[longUrl] = code
        return 'http://tinyurl.com/' + self.url2code[longUrl]

    def decode(self, shortUrl):
        return self.code2url[shortUrl[-6:]]
```
It's possible that a randomly generated code has already been generated before. In that case, another random code is generated instead. Repeat until we have a code that's not already in use. How long can this take? Well, even if we get up to using half of the code space, which is a whopping 62<sup>6</sup>/2 = 28,400,117,792 entries, then each code has a 50% chance of not having appeared yet. So the expected/average number of attempts is 2, and for example only one in a billion URLs takes more than 30 attempts. And if we ever get to an even larger number of entries and this does become a problem, then we can just use length 7. We'd need to anyway, as we'd be running out of available codes.
</p>


### A true stateless one in C++ (joke)
- Author: xplorld
- Creation Date: Sat Mar 04 2017 17:18:20 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 17:33:37 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:

    // Encodes a URL to a shortened URL.
    string encode(string longUrl) {
        return longUrl;
    }

    // Decodes a shortened URL to its original URL.
    string decode(string shortUrl) {
        return shortUrl;
    }
};
```
</p>


### Easy solution in java, 5 line code.
- Author: qiu5
- Creation Date: Sun Mar 05 2017 03:31:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 03:13:16 GMT+0800 (Singapore Standard Time)

<p>
below is the tiny url solution in java, also this is the similar method in industry. In industry, most of shorten url service is by database, one auto increasing long number as primary key. whenever a long url need to be shorten, append to the database, and return the primary key number. (the database is very easy to distribute to multiple machine like HBase,  or even you can use the raw file system to store data and improve performance by shard and replica).
Note, it's meaningless to promise the same long url to be shorten as the same short url. if you do the promise and use something like hash to check existing, the benefit is must less than the cost.
Note: if you want the shorted url contains '0-9a-zA-Z' instead of '0-9', then you need to use 62 number system, not 10 number system(decimal) to convert the primary key number. like 123->'123' in decimal, 123->'1Z' in 62 number system (or '0001Z' for align).

```
public class Codec {
    List<String> urls = new ArrayList<String>();
    // Encodes a URL to a shortened URL.
    public String encode(String longUrl) {
        urls.add(longUrl);
        return String.valueOf(urls.size()-1);
    }

    // Decodes a shortened URL to its original URL.
    public String decode(String shortUrl) {
        int index = Integer.valueOf(shortUrl);
        return (index<urls.size())?urls.get(index):"";
    }
}
```
</p>


