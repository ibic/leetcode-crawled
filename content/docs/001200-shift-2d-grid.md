---
title: "Shift 2D Grid"
weight: 1200
#id: "shift-2d-grid"
---
## Description
<div class="description">
<p>Given a 2D <code>grid</code> of size <code>m x n</code>&nbsp;and an integer <code>k</code>. You need to shift the <code>grid</code>&nbsp;<code>k</code> times.</p>

<p>In one shift operation:</p>

<ul>
	<li>Element at <code>grid[i][j]</code> moves to <code>grid[i][j + 1]</code>.</li>
	<li>Element at <code>grid[i][n - 1]</code> moves to <code>grid[i + 1][0]</code>.</li>
	<li>Element at <code>grid[m&nbsp;- 1][n - 1]</code> moves to <code>grid[0][0]</code>.</li>
</ul>

<p>Return the <em>2D grid</em> after applying shift operation <code>k</code> times.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/11/05/e1.png" style="width: 400px; height: 178px;" />
<pre>
<strong>Input:</strong> <code>grid</code> = [[1,2,3],[4,5,6],[7,8,9]], k = 1
<strong>Output:</strong> [[9,1,2],[3,4,5],[6,7,8]]
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/11/05/e2.png" style="width: 400px; height: 166px;" />
<pre>
<strong>Input:</strong> <code>grid</code> = [[3,8,1,9],[19,7,2,5],[4,6,11,10],[12,0,21,13]], k = 4
<strong>Output:</strong> [[12,0,21,13],[3,8,1,9],[19,7,2,5],[4,6,11,10]]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> <code>grid</code> = [[1,2,3],[4,5,6],[7,8,9]], k = 9
<strong>Output:</strong> [[1,2,3],[4,5,6],[7,8,9]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m ==&nbsp;grid.length</code></li>
	<li><code>n ==&nbsp;grid[i].length</code></li>
	<li><code>1 &lt;= m &lt;= 50</code></li>
	<li><code>1 &lt;= n &lt;= 50</code></li>
	<li><code>-1000 &lt;= grid[i][j] &lt;= 1000</code></li>
	<li><code>0 &lt;= k &lt;= 100</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Simulation

**Intuition**

We are given instructions on how to apply the transformation. Therefore, we could just repeat applying the transformation $$k$$ times.

**Algorithm**

Reading explanations with lots of notation and array indexes can be confusing, and there is a lot of room for misunderstanding. A good first step might be to draw a picture (a rough sketch on a whiteboard would be fine) of each of the 3 cases to be certain that you understand them.

*Element at grid[i][j] moves to grid[i][j + 1].*

![Diagram showing case #1.](../Figures/1260/case1.png)

*Element at grid[i][n - 1] moves to grid[i + 1][0].*

![Diagram showing case #2.](../Figures/1260/case2.png)

*Element at grid[m - 1][n - 1] moves to grid[0][0].*

![Diagram showing case #3.](../Figures/1260/case3.png)

Then, $$k$$ times, we need to create a new 2D array and follow the given rules to move the values. If we're using Java, we'll also need to then convert the output from a 2D Array to a 2D list.

<iframe src="https://leetcode.com/playground/RxxVT4de/shared" frameBorder="0" width="100%" height="500" name="RxxVT4de"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(n \cdot m \cdot k)$$, where the grid size is $$n \cdot m$$ and we need to apply the transform $$k$$ times.

* Space Complexity: $$O(n \cdot m)$$. We are creating a new array in each iteration of the loop. We only keep track of at most 2 arrays at a time, though. The rest are garbage collected/ free'd.

<br />

---

#### Approach 2: Simulation, Recycling Same Array

**Intuition**

The previous approach created `k` new arrays. We can simplify it to do the movements in-place. To do this, let's look at how an individual value moves around the grid. Looking at the movement of a single value is a good strategy for getting started on 2D grid translation problems. The value we're looking at is the yellow square. The numbers show the order in which it moved into each cell.

!?!../Documents/1260.json:960,540!?!

The movement is a straightforward pattern. The value moves in "reading" order, and then when it gets to the bottom right, it wraps around to the top left.

![Arrow diagram showing the movement of the cell.](../Figures/1260/movement_pattern.png)

We can simulate this wrapping in-place by repeatedly moving each value "forward" by one place.

**Algorithm**

For each step, we'll need to keep track of the current value we're moving forward. For Java, we'll need to finish by copying the input into a 2D list. If you were writing this algorithm in your own Java code and wanted it to be in-place, you would choose the same input and output type.

<iframe src="https://leetcode.com/playground/vdpHcmgq/shared" frameBorder="0" width="100%" height="500" name="vdpHcmgq"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(n \cdot m \cdot k)$$, where the grid size is $$n \cdot m$$ and we need to apply the transform $$k$$ times.

* Space Complexity: Depends on whether or not the input/ output types are the same. Here on Leetcode, this depends on the language you're using.

    * *If the input and output are the same type* (Python and C++): $$O(1)$$. We aren't creating any new data structures.

    * *If the input and output are different types* (Java): $$O(n \cdot m)$$. We're creating a single `n x m` 2D list.

If you were using this algorithm as part of your own code, you'd choose input and output types based on your requirements.

<br />

---

#### Approach 3: Using Modulo Arithmetic

**Intuition**

*Note that a solution that uses modulo arithmetic would probably be considered as "medium" level.*

If you're not familiar with the modulo operator, then think back to when you learned division in school. Before you learned about decimals and fractions, you probably had to determine the *quotient* and the *remainder*. For example, $$127 / 19$$ would be $$6$$, remainder $$13$$ because $$6 * 19 + 13 = 127$$. The modulo operator gives you the *remainder* if you were to do division in this way.

For the problem constraints given here, $$k$$ is never more than $$100$$. For such a low $$k$$, the simulation approach is acceptable. However, an interviewer will probably expect you to further optimize your solution so that it runs in $$O(n * m)$$ time instead of $$O(k * n * m)$$ time.

In addition to the pattern we observed in the previous approach, another good strategy for 2D grid problems that we'll use is calculating the new locations as 2 separate steps.

1. What is the new column?
2. What is the new row?

Let's figure out how to do each of these steps with an example. We'll use a grid with 3 rows and 5 columns, and we'll look at the value located at $$i = 1$$ and $$j = 3$$. We'll also use $$k = 88$$.

![Picture of the example we'll be working through.](../Figures/1260/example.png)

*Step 1: Calculating the new column for the value*

The value's column changes exactly $$k$$ times. Every step, it moves forward by 1 column.

Which *column* will the value be in when it is moved forward $$88$$ times? Well, it starts in column $$3$$, and if the grid had an infinite number of columns, it would be in column $$3 + 88 = 91$$ at the end. However, the grid isn't infinite. We need to take into account the "wrap around".

**The key observation is that** every 5 steps will move the value back to the same column it started in. Therefore, we want to repeatedly subtract $$5$$ from $$91$$ until we have a value less than $$5$$ left. This is exactly what the *modulo operator* does for us. It gives us what is left after these repeated subtractions (it goes about it in a very efficient way though!). $$91 % 5 = 1$$. Therefore, we know that this value's new position is *in column 0*.

*Step 2: Calculating the new row for the value*

Now we need to determine the new *row* that the value is in. The *row* the value is in doesn't change as often as the *column* does. For example, here the column changes but the row stays the same.

![Diagram showing a case where the row does not change.](../Figures/1260/case1.png)

And here, both the row and the column change.

![Diagram showing a case where the row changes.](../Figures/1260/case2.png)

The value only moves down a row when it is moving from the last column to the first column. Therefore, determining the new row will require us to calculate how many times the value moves from the last to first column.

When we calculated the new *column* for our example, we only looked at the ***remainder*** of $$91 / 5$$, which was $$1$$. This time however, we want to look at the ***quotient***, because the ***quotient*** tells us *how many times* we removed $$5$$ from $$91$$.

Doing this calculation, we get $$91 / 5 = 18$$ (This is just division with truncation, like how it's done in programming languages).

If the number of rows was infinite, the final row would be $$1 + 18 = 19$$, as we would simply add the number of row increments to the starting row.

But of course the number of rows is not infinite, so we need to do the same thing we did to the column. Because there are 3 rows, we do $$19 % 3 = 1$$. The value will therefore end in row 1.

We have calculated that for the example above, after 88 increments, the value that started at $$(1, 3)$$ will now be at $$(1, 0)$$. We can do the same calculations for the other $$14 values in the grid$$.

*Coming up with general formulae*

We now need to pull all of this together into some formulae that we can use regardless of the starting position, $$k$$ value, and size of the grid.

For the column, we calculated what column it'd end up in if the grid was infinite, and then we took it modulo the total number of columns. As a general formula, this is:

```
new_col = (j + k) % num_cols
```

Where $$j$$ is the column the value starts in and $$num_cols$$ is the total number of columns in the grid.

The row was a little more complicated; we needed to tackle it in several parts. Firstly, we calculated how many times it would move down by 1 row. Secondly, we calculated what the infinite row would be and then took that modulo the number of rows. This gives us the following formula:

```
number_of_increments = (j + k) / num_cols
new_row = (i + number_of_increments) % num_rows
```


**Algorithm**

<iframe src="https://leetcode.com/playground/dWjnLT7R/shared" frameBorder="0" width="100%" height="500" name="dWjnLT7R"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(n \cdot m)$$, where the grid size is $$n \cdot m$$. This time, we're calculating where each value moves to in $$O(1)$$ time instead of $$O(k)$$ time. We can't do better than this in the average case, because each of the $$n \cdot m$$ values needs to be moved to its new location.

* Space Complexity: $$O(n \cdot m)$$. We are creating a new 2D list to write the output into.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Functional Programming 3 lines
- Author: dj_khaled
- Creation Date: Sun Nov 17 2019 13:29:47 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 18 2019 15:47:19 GMT+0800 (Singapore Standard Time)

<p>
shift all the numbers `k` steps, is the same idea as, moving last `k` numbers to head of the list: `nums = nums[-k:] + nums[:-k]`
```python
class Solution:
    def shiftGrid(self, grid: List[List[int]], k: int) -> List[List[int]]:
        col, nums = len(grid[0]), sum(grid, [])
        k = k % len(nums)
        nums = nums[-k:] + nums[:-k]
        return [nums[i:i+col] for i in range(0, len(nums), col)]
```
# Functional Programming, 3 lines:
```python
class Solution:
    def shiftGrid(self, grid: List[List[int]], k: int) -> List[List[int]]:
        f1 = lambda col, nums: [nums[i:i+col] for i in range(0, len(nums), col)]
        f2 = lambda nums, k: nums[-k%len(nums):] + nums[:-k%len(nums)]
        return f1(len(grid[0]), f2(sum(grid, []), k))
```
</p>


### [Java/Python 3] 2 simple codes using mod, space O(1).
- Author: rock
- Creation Date: Sun Nov 17 2019 12:10:03 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 12 2020 23:35:34 GMT+0800 (Singapore Standard Time)

<p>
1. Number the cells from 0 to m * n - 1;
2. In case k >= m * n, use k % (m * n) to avoid those whole cycles of m * n operations;

**Method 1: space O(1) - excluding return list**
Since shifting right will put the last k cells in grid on the first k cells, we start from the kth cells from last, the index of which is m * n - k % (m * n).
```java
    public List<List<Integer>> shiftGrid(int[][] grid, int k) {
        int m = grid.length, n = grid[0].length; 
        int start = m * n - k % (m * n);
        LinkedList<List<Integer>> ans = new LinkedList<>();
        for (int i = start; i < m * n + start; ++i) {
            int j = i % (m * n), r = j / n, c = j % n;
            if ((i - start) % n == 0)
                ans.add(new ArrayList<>());
            ans.peekLast().add(grid[r][c]);
        }
        return ans;
    }
```

```python
    def shiftGrid(self, grid: List[List[int]], k: int) -> List[List[int]]:
        m, n = len(grid), len(grid[0])
        start = m * n - k % (m * n)
        ans = []
        for i in range(start, m * n + start):
            j = i % (m * n)
            r, c =  j // n, j % n
            if not (j - start) % n:
                ans.append([])
            ans[-1].append(grid[r][c]) 
        return ans
```

----

**Method 2: space O(1) - excluding return list**
1. Imagine the grid to be a `1-D` array of size `m * n`;
2. reverse the whole array;
3. reverse the first `k` elements
4. reverse the remaing `m * n - k` element.
```java
    public List<List<Integer>> shiftGrid(int[][] grid, int k) {
        int m = grid.length, n = grid[0].length;
        k %= m * n;
        reverse(grid, 0, m * n - 1);
        reverse(grid, 0, k - 1);
        reverse(grid, k, m * n - 1);
        List<List<Integer>> ans = new ArrayList<>();
        for (int[] row : grid)
            ans.add(Arrays.stream(row).boxed().collect(Collectors.toList()));
        return ans;
    }
    private void reverse(int[][] g, int lo, int hi) {
        int m = g.length, n = g[0].length;
        while (lo < hi) {
            int r = lo / n, c = lo++ % n, i = hi / n, j = hi-- % n, 
            tmp = g[r][c];
            g[r][c] = g[i][j];
            g[i][j] = tmp;
        }
    }
```
```python
    def shiftGrid(self, grid: List[List[int]], k: int) -> List[List[int]]:

        def reverse(lo: int, hi: int) -> None:
            while lo < hi:
                r, c, i, j = lo // cols, lo % cols, hi // cols, hi % cols
                grid[r][c], grid[i][j] = grid[i][j], grid[r][c]
                lo += 1
                hi -= 1

        rows, cols = len(grid), len(grid[0])
        k %= rows * cols
        reverse(0, rows * cols - 1)
        reverse(0, k - 1)
        reverse(k, rows * cols - 1)
        return grid
```
**Analysis:**
Both methods cost time  O(m * n).

</p>


### [C++] Straight forward solution
- Author: luyzh
- Creation Date: Mon Dec 23 2019 21:14:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 09 2020 09:10:57 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    vector<vector<int>> shiftGrid(vector<vector<int>>& grid, int k) {
        int m = grid.size();
        int n = grid[0].size();
        vector<vector<int>> ans(m, vector<int>(n, 0));
        for(int i = 0; i < m * n; i++) {
            int ori_y = i / n;
            int ori_x = i % n;
            int new_y = (ori_y + (ori_x + k) / n) % m; // important
            int new_x = (ori_x + k) % n;
            
            ans[new_y][new_x] = grid[ori_y][ori_x];
        }
        
        return ans;
    }
};
```

Here are some similar matrix(2D Grid) problems.
* 498 [Diagonal Traverse](https://leetcode.com/problems/diagonal-traverse/)
* 566 [Reshape the Matrix](https://leetcode.com/problems/reshape-the-matrix/)
* 867 [Transpose Matrix](https://leetcode.com/problems/transpose-matrix/)
</p>


