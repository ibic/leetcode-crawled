---
title: "Group the People Given the Group Size They Belong To"
weight: 1215
#id: "group-the-people-given-the-group-size-they-belong-to"
---
## Description
<div class="description">
<p>There are <code>n</code> people&nbsp;that are split into some unknown number of groups. Each person is labeled with a&nbsp;<strong>unique ID</strong>&nbsp;from&nbsp;<code>0</code>&nbsp;to&nbsp;<code>n - 1</code>.</p>

<p>You are given an integer array&nbsp;<code>groupSizes</code>, where <code>groupSizes[i]</code>&nbsp;is the size of the group that person&nbsp;<code>i</code>&nbsp;is in. For example, if&nbsp;<code>groupSizes[1] = 3</code>, then&nbsp;person&nbsp;<code>1</code>&nbsp;must be in a&nbsp;group of size&nbsp;<code>3</code>.</p>

<p>Return&nbsp;<em>a list of groups&nbsp;such that&nbsp;each person&nbsp;<code>i</code>&nbsp;is in a group of size&nbsp;<code>groupSizes[i]</code></em>.</p>

<p>Each person should&nbsp;appear in&nbsp;<strong>exactly one group</strong>,&nbsp;and every person must be in a group. If there are&nbsp;multiple answers, <strong>return any of them</strong>. It is <strong>guaranteed</strong> that there will be <strong>at least one</strong> valid solution for the given input.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> groupSizes = [3,3,3,3,3,1,3]
<strong>Output:</strong> [[5],[0,1,2],[3,4,6]]
<b>Explanation:</b> 
The first group is [5]. The size is 1, and groupSizes[5] = 1.
The second group is [0,1,2]. The size is 3, and groupSizes[0] = groupSizes[1] = groupSizes[2] = 3.
The third group is [3,4,6]. The size is 3, and groupSizes[3] = groupSizes[4] = groupSizes[6] = 3.
Other possible solutions are [[2,1,6],[5],[0,4,3]] and [[5],[0,6,2],[4,3,1]].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> groupSizes = [2,1,3,3,3,2]
<strong>Output:</strong> [[1],[0,5],[2,3,4]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>groupSizes.length == n</code></li>
	<li><code>1 &lt;= n&nbsp;&lt;= 500</code></li>
	<li><code>1 &lt;=&nbsp;groupSizes[i] &lt;= n</code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Roblox - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java Greedy
- Author: votrubac
- Creation Date: Sun Dec 08 2019 14:05:10 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 10 2019 16:19:55 GMT+0800 (Singapore Standard Time)

<p>
Keep track of people in each group based on the group size. Greedily assign people to the corresponding group. 

As soon as the group is filled, add it to the result and reset the list of its members.

> We can use a hash map or array (the maximum group size is 501) to track each group.

**C++**
```
vector<vector<int>> groupThePeople(vector<int>& gz) {
  vector<vector<int>> res, groups(gz.size() + 1);
  for (auto i = 0; i < gz.size(); ++i) {
    groups[gz[i]].push_back(i);
    if (groups[gz[i]].size() == gz[i]) {
      res.push_back({});
      swap(res.back(), groups[gz[i]]);
    }
  }
  return res;
}
```
**Java**
```
public List<List<Integer>> groupThePeople(int[] gz) {
  List<List<Integer>> res = new ArrayList();
  Map<Integer, List<Integer>> groups = new HashMap<>();
  for (int i = 0; i < gz.length; ++i) {
    List<Integer> list = groups.computeIfAbsent(gz[i], k -> new ArrayList());
    list.add(i);
    if (list.size() == gz[i]) {
      res.add(list);
      groups.put(gz[i], new ArrayList());
    }
  }
  return res;
}
```
</p>


### [Python] HashMap
- Author: lee215
- Creation Date: Sun Dec 08 2019 12:04:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 08 2019 12:04:06 GMT+0800 (Singapore Standard Time)

<p>
**Python:**
```python
    def groupThePeople(self, groupSizes):
        count = collections.defaultdict(list)
        for i, size in enumerate(groupSizes):
            count[size].append(i)
        return [l[i:i + s]for s, l in count.items() for i in xrange(0, len(l), s)]
```

</p>


### i was very confused by the question itself.
- Author: arpit_jit
- Creation Date: Fri Apr 03 2020 12:05:05 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Apr 03 2020 12:05:05 GMT+0800 (Singapore Standard Time)

<p>
I am very confused by the question, i have looked at the discussion and understood the solution, but still its very confusing what is the ask.
</p>


