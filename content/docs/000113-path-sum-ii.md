---
title: "Path Sum II"
weight: 113
#id: "path-sum-ii"
---
## Description
<div class="description">
<p>Given a binary tree and a sum, find all root-to-leaf paths where each path&#39;s sum equals the given sum.</p>

<p><strong>Note:</strong>&nbsp;A leaf is a node with no children.</p>

<p><strong>Example:</strong></p>

<p>Given the below binary tree and <code>sum = 22</code>,</p>

<pre>
      <strong>5</strong>
     <strong>/ \</strong>
    <strong>4   8</strong>
   <strong>/</strong>   / <strong>\</strong>
  <strong>11</strong>  13  <strong>4</strong>
 /  <strong>\</strong>    <strong>/</strong> \
7    <strong>2</strong>  <strong>5</strong>   1
</pre>

<p>Return:</p>

<pre>
[
   [5,4,11,2],
   [5,8,4,5]
]
</pre>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 10 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Quora - 4 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Zillow - 3 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Baidu - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Bloomberg - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

#### Approach: Depth First Traversal | Recursion

**Intuition**

The intuition for this approach is pretty straightforward. The problem statement asks us to find *all* root to leaf paths in a given binary tree. If you simply consider the depth first traversal on a tree, all it does is traverse once branch after another. All we need to do here is to simply execute the depth first traversal and maintain two things along the way:

1. A running sum of all the nodes traversed till that point in recursion and
2. A list of all those nodes

If ever the sum becomes equal to the required sum, and the node where this happens is a leaf node, we can simply add the list of nodes to our final solution. We keep on doing this for every branch of the tree and we will get all the root to leaf paths in this manner that add up to a certain value. Basically, these paths are branches and hence the depth first traversal makes the most sense here. We can also use the breadth first approach for solving this problem. However, that would be super heavy on memory and is not a recommended approach for this very problem. We will look into more details towards the end.

**Algorithm**

1. We'll define a function called `recurseTree` which will take the following parameters
    - `node` which represents the current node we are on during recursion
    - `remainingSum` which represents the remaining sum that we need to find going down the tree. We can also pass the current sum in our recursive calls. However, then we'd also need to pass the required sum as an additional variable since we'd have to compare the current sum against that value. By passing in remaining sum, we can avoid having to pass additional variable and just see if the remaining sum is 0 (or equal to the value of the current node).
    - Finally, we'll have to pass a list of nodes here which will simply contain the list of all the nodes we have seen till now on the current branch. Let's call this `pathNodes`.
    - The following examples assume the sum to be found is 22.

    <center>
    <img src="../Figures/113/img4.png" width="500"/>
    </center>

2. At every step along the way, we will simply check if the remaining sum is equal to the value of the current node. If that is the case and the current node is a leaf node, we will add the current `pathNodes` to the final list of paths that we have to return as a result.

    <center>
    <img src="../Figures/113/img2.png" width="600"/>
    </center>
    
3. The problem statement here specifically mentions `root to leaf` paths. A slight *modification* is to find all `root to node paths`. The solutions are almost similar except that we'd get rid of the *leaf check*. 
    - An important thing to consider for this modification is that the problem statement doesn't mention anything about the values of the nodes. That means, we can't assume them to be positive. Had the values been positive, we could stop at the node where the sum became equal to the node's value. 
    - However, if the values of the nodes can be negative, then we have to traverse all the branches, all the way up to the roots. Let's look at a modified tree for reference.
  
    <center>
    <img src="../Figures/113/img3.png" />
    </center>
    
4. We process one node at a time and every time the value of the remaining sum becomes equal to the value of the current node, we add the `pathNodes` to our final list. Let's go ahead and look at the implementation for this algorithm.

<iframe src="https://leetcode.com/playground/Euat9yon/shared" frameBorder="0" width="100%" height="500" name="Euat9yon"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$ where $$N$$ are the number of nodes in a tree. In the worst case, we could have a complete binary tree and if that is the case, then there would be $$N/2$$ leafs. For every leaf, we perform a potential $$O(N)$$ operation of copying over the `pathNodes` nodes to a new list to be added to the final `pathsList`. Hence, the complexity in the worst case could be $$O(N^2)$$.
* Space Complexity: $$O(N)$$. The space complexity, like many other problems is debatable here. I personally choose *not* to consider the space occupied by the `output` in the space complexity. So, all the `new` lists that we create for the paths are actually a part of the output and hence, don't count towards the final space complexity. The only *additional* space that we use is the `pathNodes` list to keep track of nodes along a branch. 

  We could include the space occupied by the new lists (and hence the output) in the space complexity and in that case the space would be $$O(N^2)$$. There's a great answer on [Stack Overflow](https://cs.stackexchange.com/questions/83574/does-space-complexity-analysis-usually-include-output-space) about whether to consider input and output space in the space complexity or not. I prefer *not* to include them.
  
**Why Breadth First Search is bad for this problem?**

We did touch briefly on this in the intuition section. BFS would solve this problem perfectly. However, note that the problem statement actually asks us to return a list of all the paths that add up to a particular sum. Breadth first search moves one level at a time. That means, we would have to maintain the `pathNodes` lists for `all` the paths till a particular level/depth at the same time.

Say we are at the level 10 in the tree and that level has e.g. 20 nodes. BFS uses a queue for processing the nodes. Along with 20 nodes in the queue, we would also need to maintain 20 different `pathNodes` lists since there is no backtracking here. That is too much of a space overhead. 

The good thing about depth first search is that it uses recursion for processing one branch at a time and once we are done processing the nodes of a particular branch, we `pop` them from the `pathNodes` list thus saving on space. At a time, this list would only contain all the nodes in a single branch of the tree and nothing more. Had the problem statement asked us the total number of paths that add up to a particular sum (root to leaf), then breadth first search would be an equally viable approach.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python solutions (Recursively, BFS+queue, DFS+stack)
- Author: OldCodingFarmer
- Creation Date: Sat Jul 11 2015 19:56:15 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 26 2018 12:19:32 GMT+0800 (Singapore Standard Time)

<p>
    def pathSum(self, root, sum):
        if not root:
            return []
        res = []
        self.dfs(root, sum, [], res)
        return res
        
    def dfs(self, root, sum, ls, res):
        if not root.left and not root.right and sum == root.val:
            ls.append(root.val)
            res.append(ls)
        if root.left:
            self.dfs(root.left, sum-root.val, ls+[root.val], res)
        if root.right:
            self.dfs(root.right, sum-root.val, ls+[root.val], res)
            
    def pathSum2(self, root, sum):
        if not root:
            return []
        if not root.left and not root.right and sum == root.val:
            return [[root.val]]
        tmp = self.pathSum(root.left, sum-root.val) + self.pathSum(root.right, sum-root.val)
        return [[root.val]+i for i in tmp]
    
    # BFS + queue    
    def pathSum3(self, root, sum): 
        if not root:
            return []
        res = []
        queue = [(root, root.val, [root.val])]
        while queue:
            curr, val, ls = queue.pop(0)
            if not curr.left and not curr.right and val == sum:
                res.append(ls)
            if curr.left:
                queue.append((curr.left, val+curr.left.val, ls+[curr.left.val]))
            if curr.right:
                queue.append((curr.right, val+curr.right.val, ls+[curr.right.val]))
        return res
        
    # DFS + stack I  
    def pathSum4(self, root, sum): 
        if not root:
            return []
        res = []
        stack = [(root, sum-root.val, [root.val])]
        while stack:
            curr, val, ls = stack.pop()
            if not curr.left and not curr.right and val == 0:
                res.append(ls)
            if curr.right:
                stack.append((curr.right, val-curr.right.val, ls+[curr.right.val]))
            if curr.left:
                stack.append((curr.left, val-curr.left.val, ls+[curr.left.val]))
        return res 
    
    # DFS + stack II   
    def pathSum5(self, root, s): 
        if not root:
            return []
        res = []
        stack = [(root, [root.val])]
        while stack:
            curr, ls = stack.pop()
            if not curr.left and not curr.right and sum(ls) == s:
                res.append(ls)
            if curr.right:
                stack.append((curr.right, ls+[curr.right.val]))
            if curr.left:
                stack.append((curr.left, ls+[curr.left.val]))
        return res
</p>


### 12ms 11-lines C++ Solution
- Author: jianchao-li
- Creation Date: Sat Jul 11 2015 23:10:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 14:51:34 GMT+0800 (Singapore Standard Time)

<p>
Well, a typical backtracking problem. The code is as follows. You may walk through it using the example in the problem statement to see how it works.

    class Solution {
    public:
        vector<vector<int>> pathSum(TreeNode* root, int sum) {
            vector<vector<int> > paths;
            vector<int> path;
            findPaths(root, sum, path, paths);
            return paths;  
        }
    private:
        void findPaths(TreeNode* node, int sum, vector<int>& path, vector<vector<int> >& paths) {
            if (!node) return;
            path.push_back(node -> val);
            if (!(node -> left) && !(node -> right) && sum == node -> val)
                paths.push_back(path);
            findPaths(node -> left, sum - node -> val, path, paths);
            findPaths(node -> right, sum - node -> val, path, paths);
            path.pop_back();
        }
    };
</p>


### DFS with one LinkedList , accepted java solution
- Author: wdj0xda
- Creation Date: Tue Nov 25 2014 03:13:10 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 18:56:15 GMT+0800 (Singapore Standard Time)

<p>
	public List<List<Integer>> pathSum(TreeNode root, int sum){
		List<List<Integer>> result  = new LinkedList<List<Integer>>();
		List<Integer> currentResult  = new LinkedList<Integer>();
		pathSum(root,sum,currentResult,result);
		return result;
	}

	public void pathSum(TreeNode root, int sum, List<Integer> currentResult,
			List<List<Integer>> result) {

		if (root == null)
			return;
		currentResult.add(new Integer(root.val));
		if (root.left == null && root.right == null && sum == root.val) {
			result.add(new LinkedList(currentResult));
			currentResult.remove(currentResult.size() - 1);//don't forget to remove the last integer
			return;
		} else {
			pathSum(root.left, sum - root.val, currentResult, result);
			pathSum(root.right, sum - root.val, currentResult, result);
		}
		currentResult.remove(currentResult.size() - 1);
	}
</p>


