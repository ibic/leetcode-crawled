---
title: "Maximum Width Ramp"
weight: 912
#id: "maximum-width-ramp"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of integers, a <em>ramp</em>&nbsp;is a tuple <code>(i, j)</code> for which <code>i &lt; j</code>&nbsp;and&nbsp;<code>A[i] &lt;= A[j]</code>.&nbsp; The width of such a&nbsp;ramp is <code>j - i</code>.</p>

<p>Find the maximum width of a ramp in <code>A</code>.&nbsp; If one doesn&#39;t exist, return 0.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[6,0,8,2,1,5]</span>
<strong>Output: </strong><span id="example-output-1">4</span>
<strong>Explanation: </strong>
The maximum width ramp is achieved at (i, j) = (1, 5): A[1] = 0 and A[5] = 5.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[9,8,1,0,1,9,4,0,4,1]</span>
<strong>Output: </strong><span id="example-output-2">7</span>
<strong>Explanation: </strong>
The maximum width ramp is achieved at (i, j) = (2, 9): A[2] = 1 and A[9] = 1.
</pre>
</div>

<div>
<div>
<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>2 &lt;= A.length &lt;= 50000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 50000</code></li>
</ol>
</div>
</div>

<div>
<div>&nbsp;</div>
</div>

</div>

## Tags
- Array (array)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Sort

**Intuition and Algorithm**

For all elements like `A[i] = v`, let's write the indices `i` in sorted order of their values `v`.  For example with `A[0] = 7, A[1] = 2, A[2] = 5, A[3] = 4`, we can write the order of indices `i=1, i=3, i=2, i=0`.

Then, whenever we write an index `i`, we know there was a ramp of width `i - min(indexes_previously_written)` (if this quantity is positive).  We can keep track of the minimum of all indexes previously written as `m`.

<iframe src="https://leetcode.com/playground/FCZ4JdpQ/shared" frameBorder="0" width="100%" height="378" name="FCZ4JdpQ"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$, depending on the implementation of the sorting function.
<br />
<br />


---
#### Approach 2: Binary Search Candidates

**Intuition**

Consider `i` in decreasing order.  We want to find the largest `j` with `A[j] >= A[i]` if it exists.

Thus, the candidates for `j` are decreasing: if there is `j1 < j2` and `A[j1] <= A[j2]` then we strictly prefer `j2`.

**Algorithm**

Let's keep a list of these candidates `j`.  For example, with `A = [0,8,2,7,5]`, the candidates for `i = 0` would be `candidates = [(v=5, i=4), (v=7, i=3), (v=8, i=1)]`.  We keep the list of `candidates` in decreasing order of `i` and increasing order of `v`.

Now we can binary search to find the largest `j` with `A[j] >= A[i]`: it's the first one in this list of candidates with `v >= A[i]`.

<iframe src="https://leetcode.com/playground/o5UVGLQm/shared" frameBorder="0" width="100%" height="500" name="o5UVGLQm"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] O(N) Using Stack
- Author: lee215
- Creation Date: Sun Dec 23 2018 12:35:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 23 2018 12:35:45 GMT+0800 (Singapore Standard Time)

<p>
## Solution 1

**Intuition**:
Keep a decraesing stack.
For each number,
binary search the first smaller number in the stack.

When the number is smaller the the last,
push it into the stack.

**Time Complexity**:
`O(NlogN)`

**Java**
```
    public int maxWidthRamp(int[] A) {
        List<Integer> s = new ArrayList<>();
        int res = 0, n = A.length;
        for (int i = 0; i < n; ++i) {
            if (s.size() == 0 || A[i] < A[s.get(s.size() - 1)]) {
                s.add(i);
            } else {
                int left = 0, right = s.size() - 1, mid = 0;
                while (left < right) {
                    mid = (left + right) / 2;
                    if (A[s.get(mid)] > A[i]) {
                        left = mid + 1;
                    } else {
                        right = mid;
                    }
                }
                res = Math.max(res, i - s.get(left));
            }
        }
        return res;
    }
```
**Python**
```
    def maxWidthRamp(self, A):
        stack = []
        res = 0
        for i in range(len(A))[::-1]:
            if not stack or A[i] > stack[-1][0]:
                stack.append([A[i], i])
            else:
                j = stack[bisect.bisect(stack, [A[i], i])][1]
                res = max(res, j - i)
        return res
```
<br>

## Solution 2

Improve the idea above.
Still one pass and keep a decraesing stack.

**Time Complexity**:
`O(N)`


**Java**
```
    public int maxWidthRamp(int[] A) {
        Stack<Integer> s = new Stack<>();
        int res = 0, n = A.length;
        for (int i = 0; i < n; ++i)
            if (s.empty() || A[s.peek()] > A[i])
                s.add(i);
        for (int i = n - 1; i > res; --i)
            while (!s.empty() && A[s.peek()] <= A[i])
                res = Math.max(res, i - s.pop());
        return res;
    }
```

**C++**
```
    int maxWidthRamp(vector<int>& A) {
        stack<int> s;
        int res = 0, n = A.size();
        for (int i = 0; i < n; ++i)
            if (s.empty() || A[s.top()] > A[i])
                s.push(i);
        for (int i = n - 1; i > res; --i)
            while (!s.empty() && A[s.top()] <= A[i])
                res = max(res, i - s.top()), s.pop();
        return res;
    }
```

**Python:**
```
    def maxWidthRamp(self, A):
        s = []
        res = 0
        for i, a in enumerate(A):
            if not s or A[s[-1]] > a:
                s.append(i)
        for j in range(len(A))[::-1]:
            while s and A[s[-1]] <= A[j]:
                res = max(res, j - s.pop())
        return res
```

</p>


### Detailed Explaination of all the three approaches
- Author: its_dark
- Creation Date: Sun Mar 31 2019 17:42:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 31 2019 17:42:23 GMT+0800 (Singapore Standard Time)

<p>
Essentially, we have this problem to solve : Given an array, for every index, we need to find farthest larger element on its right.

**Approach 1**

Not exactly \u201Cfind\u201D for every index, rather consider. The problem is to find maximum of all such distances, so trick is that for many indices, we can eliminate that calculation.

For 6 0 8 2 1 5, when I know that for 0, right end of the ramp (let\u2019s call it idx2) is 5, I needn\u2019t calculate it for numbers occurring between 0 and 5 for the case of idx2=5, since their distance to 5 would anyways be less than the one between 0 to 5.

Classical two pointer problem. Right pointer expands the range and left pointer contracts it. The trick is that left pointer iterates over original array and right pointer iterates over an array which stores maximum no. on the right for each index.
O(n)


```
public int maxWidthRampI(int[] A) {
  int n = A.length;
  int[] rMax = new int[n];
  rMax[n - 1] = A[n - 1];
  for (int i = n - 2; i >= 0; i--) {
    rMax[i] = Math.max(rMax[i + 1], A[i]);
  }
  int left = 0, right = 0;
  int ans = 0;
  while (right < n) {
    while (left < right && A[left] > rMax[right]) {
      left++;
    }
    ans = Math.max(ans, right - left);
    right++;
  }
  return ans;
}
```
This approach performs fastest on leetcode.

**Approach 2**

Sort indices in a separate array based on elements.

For 7 2 5 4, indices array would be [1, 3, 2, 0]. Now these indices are in non-decreasing order of elements. So for any i < j, if index[i] < index[j] (i.e. the smaller element appears before the larger element in original array), it qualifies to be a candidate for the solution. So, iterate and maintain the min index encountered till now.

O(nlogn)
Code:

```
public int maxWidthRampII(int[] A) {
  int n = A.length;
  Integer[] b = new Integer[n];
  for (int i = 0; i < n; i++) {
    b[i] = i;
  }
  Arrays.sort(b, Comparator.comparingInt(i -> A[i]));
  int mn = n;
  int ans = 0;
  for (int i = 0; i < n; i++) {
    ans = Math.max(ans, b[i] - mn);
    mn = Math.min(mn, b[i]);
  }
  return ans;
}
```

**Approach 3**

Stack based.

Keep a stack of decreasing order elements.

Let\u2019s take an example of : [6,0,8,2,1,5]

Decreasing order stack : 6,0.

There\u2019s no point of adding 8 to stack.
Since for any element on the right for which 8 would be a part of solution (i.e. 8 is on the left end of ramp) , 0 can also be the left end for that ramp and provides a larger length of ramp since index of 0 is less than that of 8. This assures as that 8 will never be the left end of our largest ramp.

Similar explanation applies for 2,1 and 5.

Now after we have stack, start iterating the array from end considering :

Current element to be the right end of ramp and top element of the stack to be left end of the ramp. If stack\u2019s top element satisfies the condition, we have a candidate ramp.

The trick here is: Now we can pop that element out of the stack. Why?
Let\u2019s say we were right now at index j of the array and stack\u2019s top is at index i.

So ramp is i..j.

As we are iterating backwards in the array, the next possible right end of the ramp will be j-1. Even if formes a ramp with i, it\u2019s length would be shorter than our current ramp (i.e. j-i).

So, no point in keeping in 0 in stack now.

Keep popping elements off the stack whenever we have a candidate ramp. Since the current candidate ramp is the best ramp considering the stack\u2019s top element to the left end of that ramp

Code:

```
public int maxWidthRampIII(int[] A) {
  int n = A.length;
  Stack<Integer> st = new Stack<>();
  for (int i = 0; i < n; i++) {
    if (st.empty() || A[i] < A[st.peek()]) {
      st.push(i);
    }
  }
  int ans = 0;
  for (int i = n - 1; i >= 0; i--) {
    while (!st.empty() && A[i] > A[st.peek()]) {
      ans = Math.max(i - st.pop(), ans);
    }
  }
  return ans;
}
```

Written an article for it too : https://medium.com/@yashgirdhar/maximum-width-ramp-problem-b1687a40d0bf
</p>


### O(N) JAVA
- Author: Big_News
- Creation Date: Sun Dec 23 2018 12:27:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 23 2018 12:27:15 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int maxWidthRamp(int[] A) {
        int n = A.length;
        int i, j , max = 0;
        int[] maxR = new int[n], minL = new int[n];
        minL[0] = A[0];
        for (i = 1; i < n; i++){
            minL[i] = Math.min(A[i], minL[i - 1]);
        }
        maxR[n - 1] = A[n - 1];
        for (j = n - 2; j >= 0; j--){
            maxR[j] = Math.max(A[j], maxR[j + 1]);
        }
        i = 0; j = 0;
        while (i < n && j < n){
            if (minL[i] <= maxR[j]){
                max = Math.max(max, j - i);
                j++;
            }else{
                i++;
            }
        }
        return max;
    }
}
```
</p>


