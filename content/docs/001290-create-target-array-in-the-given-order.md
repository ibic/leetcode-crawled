---
title: "Create Target Array in the Given Order"
weight: 1290
#id: "create-target-array-in-the-given-order"
---
## Description
<div class="description">
<p>Given two arrays of integers&nbsp;<code>nums</code> and <code>index</code>. Your task is to create <em>target</em> array under the following rules:</p>

<ul>
	<li>Initially <em>target</em> array is empty.</li>
	<li>From left to right read nums[i] and index[i], insert at index <code>index[i]</code>&nbsp;the value <code>nums[i]</code>&nbsp;in&nbsp;<em>target</em> array.</li>
	<li>Repeat the previous step until there are no elements to read in <code>nums</code> and <code>index.</code></li>
</ul>

<p>Return the <em>target</em> array.</p>

<p>It is guaranteed that the insertion operations will be valid.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [0,1,2,3,4], index = [0,1,2,2,1]
<strong>Output:</strong> [0,4,1,3,2]
<strong>Explanation:</strong>
nums       index     target
0            0        [0]
1            1        [0,1]
2            2        [0,1,2]
3            2        [0,1,3,2]
4            1        [0,4,1,3,2]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4,0], index = [0,1,2,3,0]
<strong>Output:</strong> [0,1,2,3,4]
<strong>Explanation:</strong>
nums       index     target
1            0        [1]
2            1        [1,2]
3            2        [1,2,3]
4            3        [1,2,3,4]
0            0        [0,1,2,3,4]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1], index = [0]
<strong>Output:</strong> [1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length, index.length &lt;= 100</code></li>
	<li><code>nums.length == index.length</code></li>
	<li><code>0 &lt;= nums[i] &lt;= 100</code></li>
	<li><code>0 &lt;= index[i] &lt;= i</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Visa - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### O(nlogn) based on "smaller elements after self".
- Author: heshan1234
- Creation Date: Tue Mar 24 2020 11:50:44 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 24 2020 11:51:41 GMT+0800 (Singapore Standard Time)

<p>
For each index i, if there are j indices on the left, which is >= i, all those j needs to bump 1.
In other words, for each index i, if there are x indices on the right which are <= i, i needs to bump x.
e.g., nums = [0,1,2,3,4], index = [0,1,2,2,1].
Number of elements on the right which is <= the current element: [0, 1, 2, 1, 0],
Then add to the original index to get [0, 2, 4, 3, 1], which is the final index to set nums.

Finding number of elements on the right <= self, can be found in O(nlogn) using merge sort.
```
    public int[] createTargetArray(int[] nums, int[] index) {
        int n = nums.length;
        int[] a = new int[n];
        for(int i = 0; i < n; ++i) {
            a[i] = i;
        }
        helper(a, 0, n, index, new int[n]);
        int[] result = new int[n];
        for(int i = 0; i < n; ++i) {
            result[index[i]] = nums[i];
        }
        return result;
    }
    
    static void helper(int[] a, int i, int j, int[] index, int[] tmp) {
        if (j - i <= 1) {
            return;
        }
        int k = (i + j) >>> 1;
        helper(a, i, k, index, tmp);
        helper(a, k, j, index, tmp);
        int x = i;
        int y = k;
        int z = 0;
        int count = 0;
        while(x < k && y < j) {
            while(y < j && index[a[y]] <= index[a[x]] + count) {
                ++count;
                tmp[z++] = a[y++];
            }
            index[a[x]] += count;
            tmp[z++] = a[x++];
        }
        while(x < k) {
            index[a[x]] += count;
            tmp[z++] = a[x++];
        }
        while(y < j) {
            tmp[z++] = a[y++];
        }
        for(int p = i, q = 0; p < j; ++p, ++q) {
            a[p] = tmp[q];
        }
    }
```
</p>


### Python - Using insert() and without insert()
- Author: noobie12
- Creation Date: Sat Mar 28 2020 05:58:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 28 2020 05:58:37 GMT+0800 (Singapore Standard Time)

<p>
**Using the insert function:**

```
class Solution:
    def createTargetArray(self, nums: List[int], index: List[int]) -> List[int]:
        target =[]
        for i in range(len(nums)):
            target.insert(index[i], nums[i])
        return target
```

**Without using the insert function:**
```
class Solution:
    def createTargetArray(self, nums: List[int], index: List[int]) -> List[int]:
        target =[]
        for i in range(len(nums)):
            if index[i] == len(target) :
                target.append(nums[i])
            else:
                target = target[:index[i]] + [nums[i]] + target[index[i]:]
        return target
```

</p>


### JAVA | Simple Solution | Runtime: 0 ms | Faster than 100.00%
- Author: nike_nick934
- Creation Date: Sat Jun 06 2020 20:02:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 06 2020 20:04:11 GMT+0800 (Singapore Standard Time)

<p>
If you have any doubt feel free to ask!
```
class Solution {
    public int[] createTargetArray(int[] nums, int[] index) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        for(int i=0;i<nums.length;i++)
        {
            a.add(index[i],nums[i]);
        }
        int target[] = new int[nums.length];
        for(int i=0;i<nums.length;i++)
        {
            target[i] = a.get(i);
        }
        return target;
    }
}
Do Upvote if you got this!
</p>


