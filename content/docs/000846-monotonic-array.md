---
title: "Monotonic Array"
weight: 846
#id: "monotonic-array"
---
## Description
<div class="description">
<p>An array is <em>monotonic</em> if it is either monotone increasing or monotone decreasing.</p>

<p>An array <code>A</code> is monotone increasing if for all <code>i &lt;= j</code>, <code>A[i] &lt;= A[j]</code>.&nbsp; An array <code>A</code> is monotone decreasing if for all <code>i &lt;= j</code>, <code>A[i] &gt;= A[j]</code>.</p>

<p>Return <code>true</code> if and only if the given array <code>A</code> is monotonic.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,2,2,3]</span>
<strong>Output: </strong><span id="example-output-1">true</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[6,5,4,4]</span>
<strong>Output: </strong><span id="example-output-2">true</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[1,3,2]</span>
<strong>Output: </strong><span id="example-output-3">false</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">[1,2,4,5]</span>
<strong>Output: </strong><span id="example-output-4">true</span>
</pre>

<div>
<p><strong>Example 5:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-5-1">[1,1,1]</span>
<strong>Output: </strong><span id="example-output-5">true</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 50000</code></li>
	<li><code>-100000 &lt;= A[i] &lt;= 100000</code></li>
</ol>
</div>
</div>
</div>
</div>
</div>

</div>

## Tags
- Array (array)

## Companies
- Facebook - 17 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Two Pass

**Intuition**

An array is *monotonic* if it is monotone increasing, or monotone decreasing.  Since `a <= b` and `b <= c` implies `a <= c`, we only need to check adjacent elements to determine if the array is monotone increasing (or decreasing, respectively).  We can check each of these properties in one pass.

**Algorithm**

To check whether an array `A` is monotone increasing, we'll check `A[i] <= A[i+1]` for all `i`.  The check for monotone decreasing is similar.

<iframe src="https://leetcode.com/playground/7a5nAben/shared" frameBorder="0" width="100%" height="344" name="7a5nAben"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.
<br />
<br />


---
#### Approach 2: One Pass

**Intuition**

To perform this check in one pass, we want to handle a stream of comparisons from $$\{-1, 0, 1\}$$, corresponding to `<`, `==`, or `>`.  For example, with the array `[1, 2, 2, 3, 0]`, we will see the stream `(-1, 0, -1, 1)`.

**Algorithm**

Keep track of `store`, equal to the first non-zero comparison seen (if it exists.)  If we see the opposite comparison, the answer is `False`.

Otherwise, every comparison was (necessarily) in the set $$\{-1, 0\}$$, or every comparison was in the set $$\{0, 1\}$$, and therefore the array is monotonic.

<iframe src="https://leetcode.com/playground/Mp3RXdJy/shared" frameBorder="0" width="100%" height="310" name="Mp3RXdJy"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.
<br />
<br />


---
#### Approach 3: One Pass (Simple Variant)

**Intuition and Algorithm**

To perform this check in one pass, we want to remember if it is monotone increasing or monotone decreasing.

It's monotone increasing if there aren't some adjacent values `A[i], A[i+1]` with `A[i] > A[i+1]`, and similarly for monotone decreasing.

If it is either monotone increasing or monotone decreasing, then `A` is monotonic.

<iframe src="https://leetcode.com/playground/Gqh6PaBh/shared" frameBorder="0" width="100%" height="293" name="Gqh6PaBh"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] One Pass O(N)
- Author: lee215
- Creation Date: Sun Sep 02 2018 11:11:21 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 23:33:11 GMT+0800 (Singapore Standard Time)

<p>
We check if `A` is increasing and decreasing.

**C++:**
```
    bool isMonotonic(vector<int> A) {
        bool inc = true, dec = true;
        for (int i = 1; i < A.size(); ++i)
            inc &= A[i - 1] <= A[i], dec &= A[i - 1] >= A[i];
        return inc || dec;
    }
```

**Java:**
```
    public boolean isMonotonic(int[] A) {
        boolean inc = true, dec = true;
        for (int i = 1; i < A.length; ++i) {
            inc &= A[i - 1] <= A[i];
            dec &= A[i - 1] >= A[i];
        }
        return inc || dec;
    }
```

**Python:**
```
    def isMonotonic(self, A):
        return not {cmp(i, j) for i, j in zip(A, A[1:])} >= {1, -1}
```

</p>


### Python Solution, Easy to Understand
- Author: cslzy
- Creation Date: Sun Sep 02 2018 13:29:56 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 06:02:53 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def isMonotonic(self, A):
        """
        :type A: List[int]
        :rtype: bool
        """

        n = len(A)
        if n <= 2: return True
				
        isGreat = False
        isLess = False
        for i in range(1, n):
            if A[i - 1] > A[i]:
                isGreat = True
            if A[i - 1] < A[i]:
                isLess = True

            if isGreat and isLess:
                return False

        return True
```
</p>


### JAVA Tricky Solution
- Author: caraxin
- Creation Date: Sun Sep 02 2018 11:49:13 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 13:57:27 GMT+0800 (Singapore Standard Time)

<p>
1. Find the trend based on A[0] and A[n-1]
2. Check if the consecutive neighbors are consistent with the trend.
```
class Solution {
    public boolean isMonotonic(int[] A) {
        if (A.length==1) return true;
        int n=A.length;
        boolean up= (A[n-1]-A[0])>0;
        for (int i=0; i<n-1; i++)
            if (A[i+1]!=A[i] && (A[i+1]-A[i]>0)!=up) 
                return false;
        return true;
    }
}
```
</p>


