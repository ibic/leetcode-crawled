---
title: "Unique Word Abbreviation"
weight: 271
#id: "unique-word-abbreviation"
---
## Description
<div class="description">
<p>An abbreviation of a word follows the form &lt;first letter&gt;&lt;number&gt;&lt;last letter&gt;. Below are some examples of word abbreviations:</p>

<pre>
a) it                      --&gt; it    (no abbreviation)

     1
     &darr;
b) d|o|g                   --&gt; d1g

              1    1  1
     1---5----0----5--8
     &darr;   &darr;    &darr;    &darr;  &darr;    
c) i|nternationalizatio|n  --&gt; i18n

              1
     1---5----0
&nbsp;    &darr;   &darr;    &darr;
d) l|ocalizatio|n          --&gt; l10n

Additionally for any string s of size less than or equal to 2 their abbreviation is the same string s.
</pre>

<p>Find whether its abbreviation is unique in the dictionary. A word&#39;s abbreviation is called <em>unique</em> if any of the following conditions is met:</p>

<ul>
	<li>There is no word in <code>dictionary</code>&nbsp;such that their abbreviation is equal to the abbreviation of <code>word</code>.</li>
	<li>Else, for all words in <code>dictionary</code> such that their abbreviation is equal to the abbreviation of <code>word</code>&nbsp;those words are equal to <code>word</code>.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;ValidWordAbbr&quot;, &quot;isUnique&quot;, &quot;isUnique&quot;, &quot;isUnique&quot;, &quot;isUnique&quot;]
[[[&quot;deer&quot;, &quot;door&quot;, &quot;cake&quot;, &quot;card&quot;]], [&quot;dear&quot;], [&quot;cart&quot;], [&quot;cane&quot;], [&quot;make&quot;]]
<strong>Output</strong>
[null, false, true, false, true]

<strong>Explanation</strong>
ValidWordAbbr validWordAbbr = new ValidWordAbbr([&quot;deer&quot;, &quot;door&quot;, &quot;cake&quot;, &quot;card&quot;]);
validWordAbbr.isUnique(&quot;dear&quot;); // return False
validWordAbbr.isUnique(&quot;cart&quot;); // return True
validWordAbbr.isUnique(&quot;cane&quot;); // return False
validWordAbbr.isUnique(&quot;make&quot;); // return True
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>Each word will only consist of lowercase English characters.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Design (design)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Google - 7 (taggedByAdmin: true)

## Official Solution
[TOC]

## Summary

This problem has a low acceptance rate for a reason. The logic in `isUnique` can be a little tricky to get right due to the number of cases you need to consider. We highly recommend that you practice this similar but easier problem first - [Two Sum III - Data structure design](https://leetcode.com/problems/two-sum-iii-data-structure-design/).

## Solution
---
#### Approach #1 (Brute Force)

Let us begin by storing the dictionary first in the constructor. To determine if a word's abbreviation is unique with respect to a word in the dictionary, we check if all the following conditions are met:

1. <a name="condition-1"></a>They are not the same word.
2. They both have equal lengths.
3. They both share the same first and last letter.

Note that [Condition #1](#condition-1) is implicit because from the problem statement:

>A word's abbreviation is unique if no ***other*** word from the dictionary has the same abbreviation.

```java
public class ValidWordAbbr {
    private final String[] dict;

    public ValidWordAbbr(String[] dictionary) {
        dict = dictionary;
    }

    public boolean isUnique(String word) {
        int n = word.length();
        for (String s : dict) {
            if (word.equals(s)) {
                continue;
            }
            int m = s.length();
            if (m == n
                && s.charAt(0) == word.charAt(0)
                && s.charAt(m - 1) == word.charAt(n - 1)) {
                return false;
            }
        }
        return true;
    }
}
```

**Complexity analysis**

* Time complexity : $$O(n)$$ for each `isUnique` call.
Assume that $$n$$ is the number of words in the dictionary, each `isUnique` call takes $$O(n)$$ time.

---
#### Approach #2 (Hash Table) [Accepted]

Note that `isUnique` is called repeatedly for the same set of words in the dictionary each time. We should pre-process the dictionary to speed it up.

Ideally, a hash table supports constant time look up. What should the key-value pair be?

Well, the idea is to *group* the words that fall under the same abbreviation together. For the value, we use a Set instead of a List to guarantee uniqueness.

The logic in `isUnique(word)` is tricky. You need to consider the following cases:

1. Does the word's abbreviation exists in the dictionary? If not, then it must be unique.
2. If above is yes, then it can only be unique if the grouping of the abbreviation contains no other words except *word*.

```java
public class ValidWordAbbr {
    private final Map<String, Set<String>> abbrDict = new HashMap<>();

    public ValidWordAbbr(String[] dictionary) {
        for (String s : dictionary) {
            String abbr = toAbbr(s);
            Set<String> words = abbrDict.containsKey(abbr)
                ? abbrDict.get(abbr) : new HashSet<>();
            words.add(s);
            abbrDict.put(abbr, words);
        }
    }

    public boolean isUnique(String word) {
        String abbr = toAbbr(word);
        Set<String> words = abbrDict.get(abbr);
        return words == null || (words.size() == 1 && words.contains(word));
    }

    private String toAbbr(String s) {
        int n = s.length();
        if (n <= 2) {
            return s;
        }
        return s.charAt(0) + Integer.toString(n - 2) + s.charAt(n - 1);
    }
}
```

---
#### Approach #3 (Hash Table) [Accepted]

Let us consider another approach using a counter as the table's value. For example, assume the dictionary = `["door", "deer"]`, we have the mapping of `{"d2r" -> 2}`. However, this mapping alone is not enough, because we need to consider whether the word exists in the dictionary. This can be easily overcome by inserting the entire dictionary into a set.

When an abbreviation's counter exceeds one, we know this abbreviation must not be unique because at least two different words share the same abbreviation. Therefore, we can further simplify the counter to just a boolean.

```java
public class ValidWordAbbr {
    private final Map<String, Boolean> abbrDict = new HashMap<>();
    private final Set<String> dict;

    public ValidWordAbbr(String[] dictionary) {
        dict = new HashSet<>(Arrays.asList(dictionary));
        for (String s : dict) {
            String abbr = toAbbr(s);
            abbrDict.put(abbr, !abbrDict.containsKey(abbr));
        }
    }

    public boolean isUnique(String word) {
        String abbr = toAbbr(word);
        Boolean hasAbbr = abbrDict.get(abbr);
        return hasAbbr == null || (hasAbbr && dict.contains(word));
    }

    private String toAbbr(String s) {
        int n = s.length();
        if (n <= 2) {
            return s;
        }
        return s.charAt(0) + Integer.toString(n - 2) + s.charAt(n - 1);
    }
}
```

**Complexity analysis**

* Time complexity : $$O(n)$$ pre-processing, $$O(1)$$ for each `isUnique` call.
Both [Approach #2](#approach-2) and [Approach #3](#approach-3) above take $$O(n)$$ pre-processing time in the constructor. This is totally worth it if `isUnique` is called repeatedly.

* Space complexity : $$O(n)$$.
We traded the extra $$O(n)$$ space storing the table to reduce the time complexity in `isUnique`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Let me explain the question with better examples.
- Author: 2016trojan
- Creation Date: Tue Feb 16 2016 06:40:28 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 08:28:14 GMT+0800 (Singapore Standard Time)

<p>
The description (**A word's abbreviation is unique if no other word from the dictionary has the same abbreviation**) is clear however a bit twisting. It took me a few "Wrong Answer"s to finally understand what it's asking for.  
We are trying to search for a word in a dictionary. If this word (also this word\u2019s abbreviation) is not in the dictionary OR this word and only it\u2019s abbreviation in the dictionary. We call a word\u2019s abbreviation unique.  
EX:  

    1) [\u201cdog\u201d]; isUnique(\u201cdig\u201d);   

//False, because \u201cdig\u201d has the same abbreviation with \u201cdog" and \u201cdog\u201d is already in the dictionary. It\u2019s not unique.  

    2) [\u201cdog\u201d, \u201cdog"]; isUnique(\u201cdog\u201d);  

 //True, because \u201cdog\u201d is the only word that has \u201cd1g\u201d abbreviation.  

    3) [\u201cdog\u201d, \u201cdig\u201d]; isUnique(\u201cdog\u201d);   

//False, because if we have more than one word match to the same abbreviation, this abbreviation will never be unique.
</p>


### Java Solution with One HashMap<String, String> beats 90% of Submissions
- Author: skylinebaby
- Creation Date: Thu Nov 26 2015 13:10:47 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 13:18:51 GMT+0800 (Singapore Standard Time)

<p>
    public class ValidWordAbbr {
        HashMap<String, String> map;
        public ValidWordAbbr(String[] dictionary) {
            map = new HashMap<String, String>();
            for(String str:dictionary){
                String key = getKey(str);
                // If there is more than one string belong to the same key
                // then the key will be invalid, we set the value to ""
                if(map.containsKey(key)){
                    if(!map.get(key).equals(str)){
                        map.put(key, "");
                    }
                }
                else{
                    map.put(key, str);
                }
            }
        }
    
        public boolean isUnique(String word) {
            return !map.containsKey(getKey(word))||map.get(getKey(word)).equals(word);
        }
        
        String getKey(String str){
            if(str.length()<=2) return str;
            return str.charAt(0)+Integer.toString(str.length()-2)+str.charAt(str.length()-1);
        }
    }
</p>


### I really don't understand some weird test cases and very very simple spec.
- Author: oldxing
- Creation Date: Wed Apr 27 2016 08:20:51 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Apr 27 2016 08:20:51 GMT+0800 (Singapore Standard Time)

<p>
I think working on unclear specs and weird test cases is 

REALLY REALLY 

a waste of my life.
</p>


