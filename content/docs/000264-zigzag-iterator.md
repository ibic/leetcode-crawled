---
title: "Zigzag Iterator"
weight: 264
#id: "zigzag-iterator"
---
## Description
<div class="description">
<p>Given two 1d vectors, implement an iterator to return their elements alternately.</p>

<p>&nbsp;</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong>
v1 = [1,2]
v2 = [3,4,5,6] 
<strong>Output:</strong> <code>[1,3,2,4,5,6]
<strong>Explanation:</strong></code>&nbsp;By calling <i>next</i> repeatedly until <i>hasNext</i> returns <code>false</code>, the order of elements returned by <i>next</i> should be: <code>[1,3,2,4,5,6]</code>.</pre>

<p>&nbsp;</p>

<p><b>Follow up</b>:</p>

<p>What if you are given <code>k</code> 1d vectors? How well can your code be extended to such cases?</p>

<p><strong>Clarification </strong><b>for the follow up question</b><strong>:</strong><br />
The &quot;Zigzag&quot; order is not clearly defined and is ambiguous for <code>k &gt; 2</code> cases. If &quot;Zigzag&quot; does not look right to you, replace &quot;Zigzag&quot; with &quot;Cyclic&quot;. For example:</p>

<pre>
<strong>Input:</strong>
[1,2,3]
[4,5,6,7]
[8,9]

<strong>Output: </strong><code>[1,4,8,2,5,9,3,6,7]</code>.
</pre>

</div>

## Tags
- Design (design)

## Companies
- Yandex - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java solution for K-vector
- Author: kevinhsu
- Creation Date: Fri Oct 09 2015 05:27:05 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 06:03:17 GMT+0800 (Singapore Standard Time)

<p>
Uses a linkedlist to store the iterators in different vectors. Every time we call next(), we pop an element from the list, and re-add it to the end to cycle through the lists.

    public class ZigzagIterator {
        LinkedList<Iterator> list;
        public ZigzagIterator(List<Integer> v1, List<Integer> v2) {
            list = new LinkedList<Iterator>();
            if(!v1.isEmpty()) list.add(v1.iterator());
            if(!v2.isEmpty()) list.add(v2.iterator());
        }
    
        public int next() {
            Iterator poll = list.remove();
            int result = (Integer)poll.next();
            if(poll.hasNext()) list.add(poll);
            return result;
        }
    
        public boolean hasNext() {
            return !list.isEmpty();
        }
    }
</p>


### C++ with queue (compatible with k vectors)
- Author: lightmark
- Creation Date: Thu Sep 17 2015 11:10:32 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 04 2018 05:38:52 GMT+0800 (Singapore Standard Time)

<p>
    class ZigzagIterator {
    public:
        ZigzagIterator(vector<int>& v1, vector<int>& v2) {
            if (v1.size() != 0)
                Q.push(make_pair(v1.begin(), v1.end()));
            if (v2.size() != 0)
                Q.push(make_pair(v2.begin(), v2.end()));
        }
    
        int next() {
            vector<int>::iterator it = Q.front().first;
            vector<int>::iterator endIt = Q.front().second;
            Q.pop();
            if (it + 1 != endIt)
                Q.push(make_pair(it+1, endIt));
            return *it;
        }
    
        bool hasNext() {
            return !Q.empty();
        }
    private:
        queue<pair<vector<int>::iterator, vector<int>::iterator>> Q;
    };

somehow similar to BFS.
</p>


### Short Java O(1) space
- Author: StefanPochmann
- Creation Date: Mon Sep 14 2015 09:05:02 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 13:58:03 GMT+0800 (Singapore Standard Time)

<p>
Two iterators, one for each list. Switching them *before* reading the next number instead of afterwards saves a bit of code, I think.

     public class ZigzagIterator {
    
        private Iterator<Integer> i, j, tmp;
    
        public ZigzagIterator(List<Integer> v1, List<Integer> v2) {
            i = v2.iterator();
            j = v1.iterator();
        }
    
        public int next() {
            if (j.hasNext()) { tmp = j; j = i; i = tmp; }
            return i.next();
        }
    
        public boolean hasNext() {
            return i.hasNext() || j.hasNext();
        }
    }
</p>


