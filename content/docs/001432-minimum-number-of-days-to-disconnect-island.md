---
title: "Minimum Number of Days to Disconnect Island"
weight: 1432
#id: "minimum-number-of-days-to-disconnect-island"
---
## Description
<div class="description">
<p>Given a 2D&nbsp;<code>grid</code> consisting&nbsp;of <code>1</code>s (land)&nbsp;and <code>0</code>s (water).&nbsp; An <em>island</em> is a maximal 4-directionally (horizontal or vertical) connected group of <code>1</code>s.</p>

<p>The grid is said to be <strong>connected</strong> if we have <strong>exactly one&nbsp;island</strong>, otherwise is said <strong>disconnected</strong>.</p>

<p>In one day, we are allowed to change <strong>any </strong>single land cell <code>(1)</code> into a water cell <code>(0)</code>.</p>

<p>Return <em>the minimum number of days</em> to disconnect the grid.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/08/13/1926_island.png" style="width: 498px; height: 139px;" /></strong></p>

<pre>
<strong>Input:</strong> grid = [[0,1,1,0],[0,1,1,0],[0,0,0,0]]
<strong>Output:</strong> 2
<strong>Explanation:</strong> We need at least 2 days to get a disconnected grid.
Change land grid[1][1] and grid[0][2] to water and get 2 disconnected island.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,1]]
<strong>Output:</strong> 2
<strong>Explanation: </strong>Grid of full water is also disconnected ([[1,1]] -&gt; [[0,0]]), 0 islands.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,0,1,0]]
<strong>Output:</strong> 0
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,1,0,1,1],
&nbsp;              [1,1,1,1,1],
&nbsp;              [1,1,0,1,1],
&nbsp;              [1,1,0,1,1]]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,1,0,1,1],
&nbsp;              [1,1,1,1,1],
&nbsp;              [1,1,0,1,1],
&nbsp;              [1,1,1,1,1]]
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= grid.length, grid[i].length &lt;= 30</code></li>
	<li><code>grid[i][j]</code>&nbsp;is <code>0</code> or <code>1</code>.</li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Unacademy - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python [you need at most 2 days]
- Author: GSAN
- Creation Date: Sun Aug 30 2020 12:02:58 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 16 2020 12:38:18 GMT+0800 (Singapore Standard Time)

<p>
The tricky part of this question is to notice that, you can disconnect any given island formation in at most 2 days (and you need to convince yourself that this is true).
Example: Imagine we have 30-by-30 array of all 1\'s. For the top left corner, just remove the one to the right and below. And you are done.

**Day 0:** Check islands at day 0, return 0 if you have less than or greater than one island.

**Day 1**: If not, try to add water at any given location, and check if that gives you a valid island formation.

**Day 2:** Else, just return 2!

```
import copy
class Solution:
	#this is just a helper function for the no_islands function below
    def no_islands_recur(self, grid, i, j, m, n):
        if grid[i][j]==0:
            return
        grid[i][j]=0
        if i-1>=0:
            self.no_islands_recur(grid, i-1, j, m, n)
        if i+1<m:
            self.no_islands_recur(grid, i+1, j, m, n)
        if j-1>=0:
            self.no_islands_recur(grid, i, j-1, m, n)
        if j+1<n:
            self.no_islands_recur(grid, i, j+1, m, n)
    
	
    #find how many islands the given grid has        
    def no_islands(self, grid):
        ret = 0
        m, n = len(grid), len(grid[0])
        for i in range(m):
            for j in range(n):
                if grid[i][j] == 1:
                    ret += 1
                    self.no_islands_recur(grid, i, j, m, n)
        return ret
    
    
    def minDays(self, grid: List[List[int]]) -> int:
        #if we have 0 or more than 1 islands at day 0, return day 0
        time = 0
        grid_copy = copy.deepcopy(grid)
        n = self.no_islands(grid_copy)
        if n!=1:
            return time
        
		#try to remove any land any see if it works
        time = 1
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                grid_copy = copy.deepcopy(grid)
                grid_copy[i][j] = 0
                n = self.no_islands(grid_copy)
                if n!=1:
                    return time
        
		#well then just return 2
        time = 2
        return time
```

**Edit:** I sincerely appreciate all the kindness in the comments. One thing I noticed time over time, over the solutions of many amazing people here, when a problem looks too difficult to be solved, often there is a catch that lands itself to easy coding. For this one it was the limited number of iterations one would need. So overall it might be a good strategy to think about this, which can save time and coding effort.
</p>


### DFS c++ clean code [with explanation]
- Author: mudit458
- Creation Date: Sun Aug 30 2020 12:02:32 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 02 2020 02:03:45 GMT+0800 (Singapore Standard Time)

<p>
# Observation
## **ans <= 2**
ans is always less-equal to 2
## **why?**
for any island we can remove the two blocks around the bottom left corner to make it disconnected
```
x x x
x . x
x x x
```
can be changed to
```
x x x
x . .
x . x
```
if you still don\'t get it read this: [comment](https://leetcode.com/problems/minimum-number-of-days-to-disconnect-island/discuss/819296/DFS-c++-clean-code-with-explanation/679802)
we need to check for only when ans is 1 or 0
## **ans = 1**
we remove a block and check if it disconnects the islands
## **ans = 0**
we check if there are > 1 islands already

```cpp
class Solution {
public:
    vector<int> dx = {1, -1, 0, 0};
    vector<int> dy = {0, 0, 1, -1};
    void dfs(int x, int y, vector<vector<int>> &grid, vector<vector<int>> & vis)
    {
        int n = grid.size();
        int m = grid[0].size();
        vis[x][y] = 1;
        for (int a = 0; a < 4; a++)
        {
            int nx = x + dx[a];
            int ny = y + dy[a];
            if (nx >= 0 and ny >= 0 and nx < n and ny < m and !vis[nx][ny] and grid[nx][ny])
            {
                dfs(nx, ny, grid, vis);
            }
        }
    }
    int count_islands(vector<vector<int>> & grid)
    {
        int islands = 0;
        int n = grid.size();
        int m = grid[0].size();
        vector<vector<int>> vis(n, vector<int> (m, 0));
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (!vis[i][j] and grid[i][j])
                {
                    dfs(i, j, grid, vis);
                    islands ++;
                }
            }
        }
        return islands;
    }
    int minDays(vector<vector<int>>& grid) {
        int islands = count_islands(grid);
        int n = grid.size();
        int m = grid[0].size();
		// check for 0 ans
        if (islands > 1 or islands == 0)
        {
            return 0;
        }
        else
        {
			// check for 1 ans
            for (int i = 0 ; i < n; i ++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (grid[i][j])
                    {
                        grid[i][j] = 0;
						// remove this block
                        islands = count_islands(grid);
						// add back the block
                        grid[i][j] = 1;
                        if (islands > 1 or islands == 0)
                            return 1;
                    }

                }
            }
        }
		// else
        return 2;
    }
};
```
### Time Complexity: O((m*n)^2)
</p>


### [Python / Golang] There are only 3 possible answers!
- Author: yanrucheng
- Creation Date: Sun Aug 30 2020 12:00:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 30 2020 12:09:59 GMT+0800 (Singapore Standard Time)

<p>

## Idea
**How to make the grid disconnected?**
We can tell from the first official example that, the worst situation we may get into is to take 2 steps and separate a single island out.
More specifically, there are 3 situation.
1. The number of island on the grid is not 1.
	- return 0
1. The number of island on the grid is 1, and we can break them into 2 islands within 1 step.
	- return 1
1. The number of island on the grid is 1, and we cannot break them into 2 islands within 1 step.
	- return 2, because no matter what, we can always separate 1 island out within 2 steps


**How to count the number of islands on the grid**
There are many different ways like DFS, Union Find. I use union find here.

## Complexity
- Time: O(n^4)
- Space: O(n^2)

## Python
```
class Solution:
    def minDays(self, grid: List[List[int]]) -> int:
        m, n = len(grid), len(grid[0])
        
        def countIsland():
            roots = {(i,j):(i,j) for i in range(m) for j in range(n)}
            def find(i):
                if roots[i] != i: roots[i] = find(roots[i])
                return roots[i]                    
            
            def unite(i, j):
                roots[find(i)] = find(j)
                
            for i in range(m):
                for j in range(n):
                    if grid[i][j]:
                        if i < m - 1 and grid[i + 1][j]:
                            unite((i, j), (i + 1, j))
                        if j < n - 1 and grid[i][j + 1]:
                            unite((i, j), (i, j + 1))
            return len(set(find((i, j)) for i in range(m) for j in range(n) if grid[i][j]))                            
        
        if countIsland() != 1:
            return 0
        
        for i in range(m):
            for j in range(n):
                if grid[i][j]:
                    grid[i][j] = 0
                    if countIsland() != 1:
                        return 1
                    grid[i][j] = 1
        return 2
```

## Golang
```
import "fmt"

func minDays(grid [][]int) int {
    if countIslands(grid) != 1 {
        return 0
    }
    for i := range grid {
        for j := range grid[0] {
            if grid[i][j] == 1 {
                grid[i][j] = 0
                if countIslands(grid) != 1 {
                    return 1
                }
                grid[i][j] = 1
            }
        }
    }
    return 2
}

var roots [][][2]int

func countIslands(grid [][]int) int {
    m, n := len(grid), len(grid[0])
    roots = make([][][2]int, m)
    for i := range grid {
        roots[i] = make([][2]int, n)
        for j := range roots[i] {
            roots[i][j] = [2]int {i, j}
        }
    }
    
    for i := range grid {
        for j := range grid[0] {
            if grid[i][j] == 1 {
                if i < m - 1 && grid[i + 1][j] == 1 {
                    unite(i, j, i + 1, j)
                }
                if j < n - 1 && grid[i][j + 1] == 1 {
                    unite(i, j, i, j + 1)
                }
            }
        }
    }
    set := make(map[int]bool)
    for i := range grid {
        for j := range grid[0] {
            if grid[i][j] == 1 {
                tmp := find(i, j)
                set[tmp[0] * n + tmp[1]] = true
            }
        }
    }
    return len(set)
}

func find(i, j int) [2]int {
    if roots[i][j] != [2]int {i, j} {
        tmp := roots[i][j]
        roots[i][j] = find(tmp[0], tmp[1])
    }
    return roots[i][j]
}

func unite(xi, xj, yi, yj int) {
    x := find(xi, xj)
    roots[x[0]][x[1]] = find(yi, yj)
}
```
</p>


