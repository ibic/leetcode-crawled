---
title: "Shortest Subarray to be Removed to Make Array Sorted"
weight: 1422
#id: "shortest-subarray-to-be-removed-to-make-array-sorted"
---
## Description
<div class="description">
<p>Given an integer array&nbsp;<code>arr</code>, remove a&nbsp;subarray (can be empty) from&nbsp;<code>arr</code>&nbsp;such that the remaining elements in <code>arr</code>&nbsp;are <strong>non-decreasing</strong>.</p>

<p>A subarray is a contiguous&nbsp;subsequence of the array.</p>

<p>Return&nbsp;<em>the length of the shortest subarray to remove</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,3,10,4,2,3,5]
<strong>Output:</strong> 3
<strong>Explanation: </strong>The shortest subarray we can remove is [10,4,2] of length 3. The remaining elements after that will be [1,2,3,3,5] which are sorted.
Another correct solution is to remove the subarray [3,10,4].</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [5,4,3,2,1]
<strong>Output:</strong> 4
<strong>Explanation: </strong>Since the array is strictly decreasing, we can only keep a single element. Therefore we need to remove a subarray of length 4, either [5,4,3,2] or [4,3,2,1].
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,3]
<strong>Output:</strong> 0
<strong>Explanation: </strong>The array is already non-decreasing. We do not need to remove any elements.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [1]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 10^5</code></li>
	<li><code>0 &lt;= arr[i] &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ O(N) Sliding window; Explanation with Illustrations
- Author: lzl124631x
- Creation Date: Sun Sep 06 2020 00:07:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 17:37:01 GMT+0800 (Singapore Standard Time)

<p>
See my latest update in repo [LeetCode](https://github.com/lzl124631x/LeetCode)


----

# Solution 1.

Scan from left to right, find the first index `left` that `A[left] > A[left + 1]`.

If `left == N - 1`, this array is already non-descending, return `0`.

Scan from right to left, find the first index `right` that `A[right] < A[right - 1]`.

![image](https://assets.leetcode.com/users/images/83d5a46b-1785-4682-8f65-706fd25017cc_1599384728.703493.png)


Now we loosely have two options, either deleting the left-side `right` nodes, or deleting the right-side `N - left - 1` nodes.
![image](https://assets.leetcode.com/users/images/ffa7aae1-9e33-4749-85db-9fd91f313dca_1599384738.062273.png)

![image](https://assets.leetcode.com/users/images/6063c0e4-9136-48e6-b6a5-7407fb538845_1599384745.1044443.png)


So the answer is at most `min(N - left - 1, right)`.

Now we can use a sliding window / two pointers to get tighter result.

Let `i = 0, j = right`. And we examine if we can delete elements between `i` and `j` (exclusive) by comparing `A[i]` and `A[j]`.

![image](https://assets.leetcode.com/users/images/859141be-738d-4a0c-979f-899b5ceb9766_1599384968.365021.png)



**Case 1:** `A[j] >= A[i]`, we can delete elements inbetween, so we can try to update the answer using `j - i - 1` and increment `i` to tighten the window.

![image](https://assets.leetcode.com/users/images/c90e526e-ea2a-4c78-a791-8ddda8048c60_1599384760.5704896.png)


**Case 2:** `A[j] < A[i]`, we can\'t delete elements inbetween, so we increment `j` to loosen the window.

![image](https://assets.leetcode.com/users/images/1f717bbb-1009-4f05-b463-f7a0d60c7ef9_1599384769.8815608.png)


We loop until `i > left` or `j == N`. And the answer we get should be the minimal possible solution.

```cpp
// OJ: https://leetcode.com/problems/shortest-subarray-to-be-removed-to-make-array-sorted/
// Author: github.com/lzl124631x
// Time: O(N)
// Space: O(1)
// Ref: https://leetcode.com/problems/shortest-subarray-to-be-removed-to-make-array-sorted/discuss/830416/Java-Increasing-From-Left-Right-and-Merge-O(n)
class Solution {
public:
    int findLengthOfShortestSubarray(vector<int>& A) {
        int N = A.size(), left = 0, right = N - 1;
        while (left + 1 < N && A[left] <= A[left + 1]) ++left;
        if (left == A.size() - 1) return 0;
        while (right > left && A[right - 1] <= A[right]) --right;
        int ans = min(N - left - 1, right), i = 0, j = right;
        while (i <= left && j < N) {
            if (A[j] >= A[i]) {
                ans = min(ans, j - i - 1);
                ++i;
            } else ++j;
        }
        return ans;
    }
};
```

---
# Original Approach (Incorrect)
**Oops, sorry my original approach below is incorrect because it can\'t pass `[1,2,3,10,0,7,8,9]`. :(**
## Case 1

![image](https://assets.leetcode.com/users/images/add08aaa-158c-4581-92b7-30d0080419d6_1599322591.4854705.png)


We scan from left to right and stop at the first "bad" node where `A[i] < A[i -1]`. Now we have an option to delete all the nodes from `A[i]` to `A[j]` where `A[j]` is the first node from the end that is either smaller than `A[i - 1]` or `A[j] > A[j + 1]`.

## Case 2

![image](https://assets.leetcode.com/users/images/8292c620-9803-4903-a01f-f8c982a24ba7_1599322630.6638007.png)

We scan from right to left and stop at the first "bad" node where `A[i] > A[i + 1]`. Now we have an option to delete all the nodes from `A[i]` to `A[j]` where `A[j]` is the first node from the beginning that is either greater than `A[i + 1]` or `A[j] < A[j - 1]`.


```cpp
// OJ: https://leetcode.com/contest/biweekly-contest-34/problems/shortest-subarray-to-be-removed-to-make-array-sorted/
// Author: github.com/lzl124631x
// Time: O(N)
// Space: O(1)
class Solution {
public:
    int findLengthOfShortestSubarray(vector<int>& A) {
        int N = A.size(), ans = N - 1, i;
        for (i = 1; i < N; ++i) {
            if (A[i] >= A[i - 1]) continue;
            int j = N - 1;
            while (j > i && A[j] >= A[i - 1] && (j == N - 1 || A[j] <= A[j + 1])) --j;
            ans = min(ans, j - i + 1);
            break;
        }
        if (i == N) return 0;
        for (i = N - 2; i >= 0; --i) {
            if (A[i] <= A[i + 1]) continue;
            int j = 0;
            while (j < i && A[j] <= A[i + 1] && (j == 0 || A[j] >= A[j - 1])) ++j;
            ans = min(ans, i - j + 1);
            break;
        }
        return ans;
    }
};
```
</p>


### [Java] Increasing From Left, Right and Merge O(n)
- Author: nihalanim9
- Creation Date: Sun Sep 06 2020 00:01:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 05:56:04 GMT+0800 (Singapore Standard Time)

<p>
Idea:
1. You need to find some sequence from 0th index which is in increasing order, let this sequence `a_1 <= a_2 <= ... <= a_i`
2. Then you need to find some sequence from the back which is in decreasing order such that `b_j <= b_(j+1) <= ... <= b_n` (j <= i)

Then it is guranteed you need to remove subarray from (i + 1, j - 1). But it is possible if we could merge some part from  `b_j <= b_(j+1) <= ... <= b_n`, with `a_1 <= a_2 <= ... <= a_i`, to create a bigger increasing subarray.
```
class Solution {
    public int findLengthOfShortestSubarray(int[] arr) {
        int left = 0;
        while(left + 1 < arr.length && arr[left] <= arr[left+1]) left++;
        if(left == arr.length - 1) return 0;
        
        int right = arr.length - 1;
        while(right > left && arr[right-1] <= arr[right]) right--;
        int result = Math.min(arr.length - left - 1, right);
        
        int i = 0;
        int j = right;
        while(i <= left && j < arr.length) {
            if(arr[j] >= arr[i]) {
                result = Math.min(result, j - i - 1);
                i++;
            }else {
                j++;
            }   
        }
        return result;
    }
}
```
</p>


### [Python] Two Pointers Approach with Explanation
- Author: binarypanda
- Creation Date: Sun Sep 06 2020 01:44:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 01:44:44 GMT+0800 (Singapore Standard Time)

<p>
Since we can only remove a subarray, the final remaining elements must be either: (1) solely a prefix, (2) solely a suffix or (3) a merge of the prefix and suffix.
1. Find the monotone non-decreasing prefix [a_0 <= ... a_i | ...]
	- `l` is the index such that `arr[l+1] < arr[l]`
2. Find the monotone non-decreasing suffix [... | a_j <= ... a_n]
	- `r` is the index such that `arr[r-1] > arr[r]`
3. Try to "merge 2 sorted arrays", if we can merge, update our minimum to remove.

```
class Solution:
    def findLengthOfShortestSubarray(self, arr: List[int]) -> int:
        l, r = 0, len(arr) - 1
        while l < r and arr[l+1] >= arr[l]:
            l += 1
        if l == len(arr) - 1:
            return 0 # whole array is sorted
        while r > 0 and arr[r-1] <= arr[r]:
            r -= 1
        toRemove = min(len(arr) - l - 1, r) # case (1) and (2)
		
		# case (3): try to merge
        for iL in range(l+1):
            if arr[iL] <= arr[r]:
                toRemove = min(toRemove, r - iL - 1)
            elif r < len(arr) - 1:
                r += 1
            else:
                break
        return toRemove
```
</p>


