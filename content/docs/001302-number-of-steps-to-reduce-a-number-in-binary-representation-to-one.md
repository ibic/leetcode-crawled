---
title: "Number of Steps to Reduce a Number in Binary Representation to One"
weight: 1302
#id: "number-of-steps-to-reduce-a-number-in-binary-representation-to-one"
---
## Description
<div class="description">
<p>Given a number&nbsp;<code>s</code> in their binary representation. Return the number of steps to reduce it to 1 under the following rules:</p>

<ul>
	<li>
	<p>If the current number is even, you have to divide it by 2.</p>
	</li>
	<li>
	<p>If the current number is odd, you have to add 1 to it.</p>
	</li>
</ul>

<p>It&#39;s guaranteed that you can always reach to one for all testcases.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1101&quot;
<strong>Output:</strong> 6
<strong>Explanation:</strong> &quot;1101&quot; corressponds to number 13 in their decimal representation.
Step 1) 13 is odd, add 1 and obtain 14.&nbsp;
Step 2) 14 is even, divide by 2 and obtain 7.
Step 3) 7 is odd, add 1 and obtain 8.
Step 4) 8 is even, divide by 2 and obtain 4.&nbsp; 
Step 5) 4 is even, divide by 2 and obtain 2.&nbsp;
Step 6) 2 is even, divide by 2 and obtain 1.&nbsp; 
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;10&quot;
<strong>Output:</strong> 1
<strong>Explanation:</strong> &quot;10&quot; corressponds to number 2 in their decimal representation.
Step 1) 2 is even, divide by 2 and obtain 1.&nbsp; 
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1&quot;
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length&nbsp;&lt;= 500</code></li>
	<li><code>s</code> consists of characters &#39;0&#39; or &#39;1&#39;</li>
	<li><code>s[0] == &#39;1&#39;</code></li>
</ul>

</div>

## Tags
- String (string)
- Bit Manipulation (bit-manipulation)

## Companies
- TripAdvisor - 2 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java O(n)
- Author: votrubac
- Creation Date: Sun Apr 05 2020 12:05:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 06 2020 10:56:05 GMT+0800 (Singapore Standard Time)

<p>
> Update: check out two alternative solutions below, it may give you different viewpoints on this problem. Let the gang know which one you preffer in the comments!

**Intuition:** division by two is the same as the right shift by one bit (character). If the bit is `0`, we just do the shift - one operation. If the bit is `1` - we do plus one, and our bit changes to zero. So, we set `carry` to `1` and shift. Two operations.

**Algorithm**
We have three phases here:
1. We haven\'t encountered any `1`. Every char adds one operation.
2. We encounter our first `1`. We set `carry` to `1` and add two operations.
3. The rest:
	 3A. Every `1` needs one operation (`carry` makes it `0`). `carry` is still `1` due to addition.
	 3B. Every `0` needs two operations (`carry` makes it `1`). `carry` is still `1` as we need to add `1` in this case.

Observation: as you can see from 3A and 3B, `carry` is always `1` after the second phase. 

**C++**
```cpp
int numSteps(string &s) {
    int res = 0, carry = 0;
    for (auto i = s.size() - 1; i > 0; --i) {
        ++res;
        if (s[i] - \'0\' + carry == 1) {
            carry = 1;
            ++res;
        }
    }
    return res + carry;
}
```

**Java**
```java
public int numSteps(String s) {
    int res = 0, carry = 0;
    for (int i = s.length() - 1; i > 0; --i) {
        ++res;
        if (s.charAt(i) - \'0\' + carry == 1) {
            carry = 1;
            ++res;
        }
    }
    return res + carry;
}
```
#### Alternative Solution 1
Here, we are couting `1` (exept the very first one), and tracking the position of the last `1`. So we can compute operations for every phase. Note that, if `1` was found, we need to add `2` to the result: one for the first `1`, and one for the last `1`.

**C++**
```cpp
int numSteps(string &s) {
    int ones = 0, last_one = 0;
    for (auto i = 1; i < s.size(); ++i)
        if (s[i] == \'1\') {
            last_one = i;
            ++ones;
        }  
    return s.size() - 1 + (last_one - ones) + (last_one ? 2 : 0);
}
```
#### Alternative Solution 2
This is #minimalizm version of the inital solution.
**C++**
```cpp
int numSteps(string &s) {
    int divs = s.size() - 1, additions = 0, carry = 0;
    for (auto i = s.size() - 1; i > 0; --i) {
        additions += s[i] - \'0\' + carry == 1;
        carry |= s[i] == \'1\';
    }
    return divs + additions + carry;
}
```
**Complexity Analsysis**
- Time: O(n)
- Memory: O(1)
</p>


### Java.. Forget the String operations. O(N^2)
- Author: dsmyda
- Creation Date: Sun Apr 05 2020 12:19:32 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 06 2020 11:15:51 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**: If len(S) could fit into an integer, this problem would be easy to simulate using additions and right shifts. However, len(S) can be up to 500. Luckily, BigInteger was designed for arbitrary precision integers, so we can still extend the same logic for this problem.

```
import java.math.BigInteger;

class Solution {
    public int numSteps(String s) {
        BigInteger b = new BigInteger(s, 2);
        int steps = 0;
        while(!b.equals(BigInteger.ONE)) {
            if(b.testBit(0)) {
                b = b.add(BigInteger.ONE);
            } else {
                b = b.shiftRight(1);
            }
            steps++;
        }
        return steps;
    }
}
```
</p>


### Clean Python 3, two pointers O(n)/O(1)
- Author: lenchen1112
- Creation Date: Sun Apr 05 2020 12:10:13 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 07 2020 11:06:33 GMT+0800 (Singapore Standard Time)

<p>
We have to push all `1`s to be only one `1` which takes `middle zeros + 1` moves.
And reduce it to one will cost `len(s) - 1` moves

Example:
`110010` -> `110100` -> `111000` -> `1000000` takes `2(which is count of mid zeros) + 1` moves.
`1000000` -> `1` takes 6 moves because length of `s` increases 1.

Time: `O(N)`
Space: `O(1)`
```
class Solution:
    def numSteps(self, s: str) -> int:
        i, mid_zero = 0, 0
        for j in range(1, len(s)):
            if s[j] == \'1\':
                mid_zero += j - i - 1
                i = j
        if i == 0: return len(s) - 1
        return mid_zero + 1 + len(s) # s size will plus 1
```
</p>


