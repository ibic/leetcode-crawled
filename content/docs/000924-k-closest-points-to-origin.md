---
title: "K Closest Points to Origin"
weight: 924
#id: "k-closest-points-to-origin"
---
## Description
<div class="description">
<p>We have a list of <code>points</code>&nbsp;on the plane.&nbsp; Find the <code>K</code> closest points to the origin <code>(0, 0)</code>.</p>

<p>(Here, the distance between two points on a plane is the Euclidean distance.)</p>

<p>You may return the answer in any order.&nbsp; The&nbsp;answer is guaranteed to be unique (except for the order that it is in.)</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>points = <span id="example-input-1-1">[[1,3],[-2,2]]</span>, K = <span id="example-input-1-2">1</span>
<strong>Output: </strong><span id="example-output-1">[[-2,2]]</span>
<strong>Explanation: </strong>
The distance between (1, 3) and the origin is sqrt(10).
The distance between (-2, 2) and the origin is sqrt(8).
Since sqrt(8) &lt; sqrt(10), (-2, 2) is closer to the origin.
We only want the closest K = 1 points from the origin, so the answer is just [[-2,2]].
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>points = <span id="example-input-2-1">[[3,3],[5,-1],[-2,4]]</span>, K = <span id="example-input-2-2">2</span>
<strong>Output: </strong><span id="example-output-2">[[3,3],[-2,4]]</span>
(The answer [[-2,4],[3,3]] would also be accepted.)
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= K &lt;= points.length &lt;= 10000</code></li>
	<li><code>-10000 &lt; points[i][0] &lt; 10000</code></li>
	<li><code>-10000 &lt; points[i][1] &lt; 10000</code></li>
</ol>
</div>
</div>
</div>

## Tags
- Divide and Conquer (divide-and-conquer)
- Heap (heap)
- Sort (sort)

## Companies
- Facebook - 88 (taggedByAdmin: true)
- Amazon - 58 (taggedByAdmin: false)
- Asana - 5 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Oracle - 4 (taggedByAdmin: false)
- Expedia - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Spotify - 2 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Sort

**Intuition**

Sort the points by distance, then take the closest K points.

**Algorithm**

There are two variants.

In Java, we find the K-th distance by creating an array of distances and then sorting them.  After, we select all the points with distance less than or equal to this K-th distance.

In Python, we sort by a custom key function - namely, the distance to the origin.  Afterwards, we return the first K elements of the list.

<iframe src="https://leetcode.com/playground/9oyiFCRm/shared" frameBorder="0" width="100%" height="429" name="9oyiFCRm"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the length of `points`.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Divide and Conquer

**Intuition**

We want an algorithm faster than $$N \log N$$.  Clearly, the only way to do this is to use the fact that the K elements returned can be in any order -- otherwise we would be sorting which is at least $$N \log N$$.

Say we choose some random element `x = A[i]` and split the array into two buckets: one bucket of all the elements less than `x`, and another bucket of all the elements greater than or equal to `x`.  This is known as "quickselecting by a pivot `x`".

The idea is that if we quickselect by some pivot, on average in linear time we'll reduce the problem to a problem of half the size.

**Algorithm**

Let's do the `work(i, j, K)` of partially sorting the subarray `(points[i], points[i+1], ..., points[j])` so that the smallest `K` elements of this subarray occur in the first `K` positions `(i, i+1, ..., i+K-1)`.

First, we quickselect by a random pivot element from the subarray.  To do this in place, we have two pointers `i` and `j`, and move these pointers to the elements that are in the wrong bucket -- then, we swap these elements.

After, we have two buckets `[oi, i]` and `[i+1, oj]`, where `(oi, oj)` are the original `(i, j)` values when calling `work(i, j, K)`.  Say the first bucket has `10` items and the second bucket has `15` items.  If we were trying to partially sort say, `K = 5` items, then we only need to partially sort the first bucket: `work(oi, i, 5)`.  Otherwise, if we were trying to partially sort say, `K = 17` items, then the first `10` items are already partially sorted, and we only need to partially sort the next 7 items: `work(i+1, oj, 7)`.

<iframe src="https://leetcode.com/playground/upAahADq/shared" frameBorder="0" width="100%" height="500" name="upAahADq"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ in *average case* and $$O(N^2)$$ in the worst case, 
where $$N$$ is the length of `points`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Three solutions to this classical K-th problem.
- Author: Frimish
- Creation Date: Thu Jan 17 2019 07:11:50 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 17 2019 07:11:50 GMT+0800 (Singapore Standard Time)

<p>
This is a very classical problem, so-called K-th problem.
Here I will share some summaries and some classical solutions to this kind of problem.

**I.** The very naive and simple solution is sorting the all points by their distance to the origin point directly, then get the top k closest points. We can use the sort function and the code is very short. 

**Theoretically**, the time complexity is **O(NlogN)**, **pratically**, the real time it takes on leetcode is **104ms**.

The **advantages** of this solution are **short**, intuitive and easy to implement.
The **disadvatages** of this solution are not very efficient and have to know all of the points previously, and it is unable to deal with real-time(online) case, it is an **off-line** solution.

The short code shows as follows: 


    public int[][] kClosest(int[][] points, int K) {
        Arrays.sort(points, (p1, p2) -> p1[0] * p1[0] + p1[1] * p1[1] - p2[0] * p2[0] - p2[1] * p2[1]);
        return Arrays.copyOfRange(points, 0, K);
    }


**II.** The second solution is based on the first one. We don\'t have to sort all points.
	Instead, we can maintain a **max-heap** with size K. Then for each point, we add it to the heap. Once the size of the heap is greater than K, we are supposed to extract one from the max heap to ensure the size of the heap is always K. Thus, the max heap is always maintain top K smallest elements from the first one to crruent one. Once the size of the heap is over its maximum capacity, it will exclude the maximum element in it, since it can not be the proper candidate anymore.
	
**Theoretically**, the time complexity is **O(NlogK)**, but **pratically**, the real time it takes on leetcode is **134ms**. 

The **advantage** of this solution is it can deal with **real-time(online) stream data**. It does not have to know the size of the data previously.
The **disadvatage** of this solution is it is not the most efficient solution.

The short code shows as follows:

    public int[][] kClosest(int[][] points, int K) {
        PriorityQueue<int[]> pq = new PriorityQueue<int[]>((p1, p2) -> p2[0] * p2[0] + p2[1] * p2[1] - p1[0] * p1[0] - p1[1] * p1[1]);
        for (int[] p : points) {
            pq.offer(p);
            if (pq.size() > K) {
                pq.poll();
            }
        }
        int[][] res = new int[K][2];
        while (K > 0) {
            res[--K] = pq.poll();
        }
        return res;
    }

**III.** The last solution is based on quick sort, we can also call it **quick select**. In the quick sort, we will always choose a pivot to compare with other elements. After one iteration, we will get an array that  all elements smaller than the pivot are on the left side of the pivot and all elements greater than the pivot are on the right side of the pviot (assuming we sort the array in ascending order). So, inspired from this, each iteration, we choose a pivot and then find the position **p** the pivot should be. Then we compare  **p** with the **K**, if the **p** is smaller than the **K**, meaning the all element on the left of the pivot are all proper candidates but it is not adequate, we have to do the same thing on right side, and vice versa. If the **p**  is exactly equal to the **K**, meaning that we\'ve found the K-th position. Therefore, we just return the first K elements, since they are not greater than the pivot.

**Theoretically**, the average time complexity is **O(N)** , but just like quick sort, in the worst case, this solution would be degenerated to **O(N^2)**, and **pratically**, the real time it takes on leetcode is **15ms**.

The **advantage** of this solution is it is very efficient.
The **disadvatage** of this solution are it is neither an online solution nor a stable one. And the K elements closest are **not sorted** in ascending order.

The short code shows as follows:

    public int[][] kClosest(int[][] points, int K) {
        int len =  points.length, l = 0, r = len - 1;
        while (l <= r) {
            int mid = helper(points, l, r);
            if (mid == K) break;
            if (mid < K) {
                l = mid + 1;
            } else {
                r = mid - 1;
            }
        }
        return Arrays.copyOfRange(points, 0, K);
    }
    
    private int helper(int[][] A, int l, int r) {
        int[] pivot = A[l];
        while (l < r) {
            while (l < r && compare(A[r], pivot) >= 0) r--;
            A[l] = A[r];
            while (l < r && compare(A[l], pivot) <= 0) l++;
            A[r] = A[l];
        }
        A[l] = pivot;
        return l;
    }
    
    private int compare(int[] p1, int[] p2) {
        return p1[0] * p1[0] + p1[1] * p1[1] - p2[0] * p2[0] - p2[1] * p2[1];
    }

</p>


### C++ STL, quickselect, priority_queue and multiset
- Author: jianchao-li
- Creation Date: Sat Jan 19 2019 17:26:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jan 19 2019 17:26:25 GMT+0800 (Singapore Standard Time)

<p>
The simplest solution is to use `partial_sort` or `nth_element` to order the `K` closest points at the beginning of `points`. Here we need a custom comparator to compare the *closeness* of points. This solution is of `O(nlogK)` time. If we ignore the space of the output since that is inevitable, this solution is of `O(1)` space.

```cpp
class Solution {
public:
    vector<vector<int>> kClosest(vector<vector<int>>& points, int K) {
        partial_sort(points.begin(), points.begin() + K, points.end(), [](vector<int>& p, vector<int>& q) {
            return p[0] * p[0] + p[1] * p[1] < q[0] * q[0] + q[1] * q[1];
        });
        return vector<vector<int>>(points.begin(), points.begin() + K);
    }
};
```

```cpp
class Solution {
public:
    vector<vector<int>> kClosest(vector<vector<int>>& points, int K) {
        nth_element(points.begin(), points.begin() + K - 1, points.end(), [](vector<int>& p, vector<int>& q) {
            return p[0] * p[0] + p[1] * p[1] < q[0] * q[0] + q[1] * q[1];
        });
        return vector<vector<int>>(points.begin(), points.begin() + K);
    }
};
```

You may also implement the underlying quickselect algorithm yourself.

```cpp
class Solution {
public:
    vector<vector<int>> kClosest(vector<vector<int>>& points, int K) {
        int l = 0, r = points.size() - 1;
        while (true) {
            int p = partition(points, l, r);
            if (p == K - 1) {
                break;
            }
            if (p < K - 1) {
                l = p + 1;
            } else {
                r = p - 1;
            }
        }
        return vector<vector<int>>(points.begin(), points.begin() + K);
    }
private:
    bool farther(vector<int>& p, vector<int>& q) {
        return p[0] * p[0] + p[1] * p[1] > q[0] * q[0] + q[1] * q[1];
    }
    
    bool closer(vector<int>& p, vector<int>& q) {
        return p[0] * p[0] + p[1] * p[1] < q[0] * q[0] + q[1] * q[1];
    }
    
    int partition(vector<vector<int>>& points, int left, int right) {
        int pivot = left, l = left + 1, r = right;
        while (l <= r) {
            if (farther(points[l], points[pivot]) && closer(points[r], points[pivot])) {
                swap(points[l++], points[r--]);
            }
            if (!farther(points[l], points[pivot])) {
                l++;
            }
            if (!closer(points[r], points[pivot])) {
                r--;
            }
        }
        swap(points[pivot], points[r]);
        return r;
    }
};
```

If you would not like to modify `points`, you may maintain the `K` closest points so far in a separate data structure. We can use a **max** heap to maintain the `K` closest points. A max heap has its largest element in the root. Each time we add a point to the heap, if its size exceeds `K`, we pop the root, which means we get rid of the farthest point and keep the closest ones. This solution is also of `O(nlogK)` time.

```cpp
class Solution {
public:
    vector<vector<int>> kClosest(vector<vector<int>>& points, int K) {
        priority_queue<vector<int>, vector<vector<int>>, compare> pq;
        for (vector<int>& point : points) {
            pq.push(point);
            if (pq.size() > K) {
                pq.pop();
            }
        }
        vector<vector<int>> ans;
        while (!pq.empty()) {
            ans.push_back(pq.top());
            pq.pop();
        }
        return ans;
    }
private:
    struct compare {
        bool operator()(vector<int>& p, vector<int>& q) {
            return p[0] * p[0] + p[1] * p[1] < q[0] * q[0] + q[1] * q[1];
        }
    };
};
```

We can also use a min heap. A min heap has the smallest element in the root. We add all the points to the heap, and then pop the first `K` ones, we are just the closest ones. This makes the code shorter. Now this one is of `O(n + Klogn)` time. The `n` part is on adding all points to the heap (building a min heap for all the points) and the `Klogn` part is on fetching the top `K` points from the heap.

```cpp
class Solution {
public:
    vector<vector<int>> kClosest(vector<vector<int>>& points, int K) {
        priority_queue<vector<int>, vector<vector<int>>, compare> pq(points.begin(), points.end());
        vector<vector<int>> ans;
        for (int i = 0; i < K; i++) {
            ans.push_back(pq.top());
            pq.pop();
        }
        return ans;
    }
private:
    struct compare {
        bool operator()(vector<int>& p, vector<int>& q) {
            return p[0] * p[0] + p[1] * p[1] > q[0] * q[0] + q[1] * q[1];
        }
    };
};
```

Note that for `priority_queue`, if you would like to use it as a max heap, the comparator should be `<` and if as a min heap, the comparator is `>`.

Max/min heaps can also be implemented using `multiset`. For `multiset`, max heap has `>` and min heap has `<` in the comparator. The following two solutions are respectively max/min heap using `multiset`.

```cpp
class Solution {
public:
    vector<vector<int>> kClosest(vector<vector<int>>& points, int K) {
        multiset<vector<int>, compare> mset;
        for (vector<int>& point : points) {
            mset.insert(point);
            if (mset.size() > K) {
                mset.erase(mset.begin());
            }
        }
        vector<vector<int>> ans;
        copy_n(mset.begin(), K, back_inserter(ans));
        return ans;
    }
private:
    struct compare {
        bool operator()(const vector<int>& p, const vector<int>& q) const {
            return p[0] * p[0] + p[1] * p[1] > q[0] * q[0] + q[1] * q[1];
        }
    };
};
```

```cpp
class Solution {
public:
    vector<vector<int>> kClosest(vector<vector<int>>& points, int K) {
        multiset<vector<int>, compare> mset(points.begin(), points.end());
        vector<vector<int>> ans;
        copy_n(mset.begin(), K, back_inserter(ans));
        return ans;
    }
private:
    struct compare {
        bool operator()(const vector<int>& p, const vector<int>& q) const {
            return p[0] * p[0] + p[1] * p[1] < q[0] * q[0] + q[1] * q[1];
        }
    };
};
```
</p>


### Easy to read Python min heap solution ( beat 99% python solutions )
- Author: TurtleShip
- Creation Date: Sat May 18 2019 15:07:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 18 2019 15:07:11 GMT+0800 (Singapore Standard Time)

<p>
We keep a min heap of size K.
For each item, we insert an item to our heap.
If inserting an item makes heap size larger than k, then we immediately pop an item after inserting ( `heappushpop` ).

Runtime: 
Inserting an item to a heap of size k take `O(logK)` time.
And we do this for each item points.
So runtime is `O(N * logK)` where N is the length of `points`.

Space: O(K) for our heap.
```
import heapq

class Solution:
    def kClosest(self, points: List[List[int]], K: int) -> List[List[int]]:
        
        heap = []
        
        for (x, y) in points:
            dist = -(x*x + y*y)
            if len(heap) == K:
                heapq.heappushpop(heap, (dist, x, y))
            else:
                heapq.heappush(heap, (dist, x, y))
        
        return [(x,y) for (dist,x, y) in heap]
```

I found it interesting that my solution ran much faster than "Divide And Conquer" solution under "Solution" tab which is supposed to run in O(N). 
Mine ran at 316ms while D&C solution ran at 536 ms.

I am guessing that the D&C solution ran much slower than mine because it used recursions which would involved creating callstacks.
</p>


