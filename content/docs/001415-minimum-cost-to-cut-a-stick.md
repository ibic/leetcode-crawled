---
title: "Minimum Cost to Cut a Stick"
weight: 1415
#id: "minimum-cost-to-cut-a-stick"
---
## Description
<div class="description">
<p>Given a wooden stick of length <code>n</code> units. The stick is labelled from <code>0</code> to <code>n</code>. For example, a stick of length <strong>6</strong> is labelled as follows:</p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/21/statement.jpg" style="width: 521px; height: 111px;" />
<p>Given an integer array <code>cuts</code>&nbsp;where <code>cuts[i]</code>&nbsp;denotes a position you should perform a cut at.</p>

<p>You should perform the cuts in order, you can change the order of the cuts as you wish.</p>

<p>The cost of one cut is the length of the stick to be cut, the total cost is the sum of costs of all cuts. When you cut a stick, it will be split into two smaller sticks (i.e. the sum of their lengths is the length of the stick before the cut). Please refer to the first example for a better explanation.</p>

<p>Return <em>the minimum total cost</em> of the&nbsp;cuts.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/23/e1.jpg" style="width: 350px; height: 284px;" />
<pre>
<strong>Input:</strong> n = 7, cuts = [1,3,4,5]
<strong>Output:</strong> 16
<strong>Explanation:</strong> Using cuts order = [1, 3, 4, 5] as in the input leads to the following scenario:
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/21/e11.jpg" style="width: 350px; height: 284px;" />
The first cut is done to a rod of length 7 so the cost is 7. The second cut is done to a rod of length 6 (i.e. the second part of the first cut), the third is done to a rod of length 4 and the last cut is to a rod of length 3. The total cost is 7 + 6 + 4 + 3 = 20.
Rearranging the cuts to be [3, 5, 1, 4] for example will lead to a scenario with total cost = 16 (as shown in the example photo 7 + 4 + 3 + 2 = 16).</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 9, cuts = [5,6,1,4,2]
<strong>Output:</strong> 22
<strong>Explanation:</strong> If you try the given cuts ordering the cost will be 25.
There are much ordering with total cost &lt;= 25, for example, the order [4, 6, 5, 2, 1] has total cost = 22 which is the minimum possible.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 10^6</code></li>
	<li><code>1 &lt;= cuts.length &lt;= min(n - 1, 100)</code></li>
	<li><code>1 &lt;= cuts[i] &lt;= n - 1</code></li>
	<li>All the integers in <code>cuts</code>&nbsp;array are <strong>distinct</strong>.</li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### DP with picture (Burst Balloons)
- Author: votrubac
- Creation Date: Sun Aug 09 2020 12:01:35 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 12 2020 22:09:19 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
Similar problems:
- [312. Burst Balloons](https://leetcode.com/problems/burst-balloons/)
- [1000. Minimum Cost to Merge Stones](https://leetcode.com/problems/minimum-cost-to-merge-stones/)
- [1039. Minimum Score Triangulation of Polygon](https://leetcode.com/problems/minimum-score-triangulation-of-polygon/)
	- Check out ["with picture" solution](https://leetcode.com/problems/minimum-score-triangulation-of-polygon/discuss/286753/C%2B%2B-with-picture), and also - how the heck that problem is "Medium"??

To make it simpler, we add two sentinel values to `cuts` - left and right edges of the stick. Then, we sort the cuts so we can easily identify all cuts between two points. DFS can help us find the most efficient sequence of cuts. To avoid recomputation, we memoise the best answer for stick between `cuts[i]` and `cuts[j]` in `dp[i][j]`.

In the following example, you can see the first cut at points `1` (or `4`) results in total cost of `13` (`5 + 0 + 8`). If we first cut at point `2` (or `3`), the cost will be `12` (`5 + 2 + 5`).
![image](https://assets.leetcode.com/users/images/d8e82982-420d-4928-8db8-23033718f8f6_1597035318.945624.png)

#### Top-Down DP
**C++**
```cpp
int dp[102][102] = {};
int dfs(vector<int>& cuts, int i, int j) {
    if (j - i <= 1)
        return 0;
    if (!dp[i][j]) {
        dp[i][j] = INT_MAX;
        for (auto k = i + 1; k < j; ++k)
            dp[i][j] = min(dp[i][j], 
                cuts[j] - cuts[i] + dfs(cuts, i, k) + dfs(cuts, k, j));
    }
    return dp[i][j];
}
int minCost(int n, vector<int>& cuts) {
    cuts.push_back(0);
    cuts.push_back(n);
    sort(begin(cuts), end(cuts));
    return dfs(cuts, 0, cuts.size() - 1);
}
```

#### Bottom-Up DP
After you figure out top-down DP, it\'s easy to convert it to bottom-up solution.  The only trick here is to move `i` backward. This ensures that smaller cuts are processed before calculating min cost for the larger ones. It happens this way naturally in the recursive solution.

**Java**
```java
public int minCost(int n, int[] cuts) {
    var c = new ArrayList<Integer>();
    for (int cut : cuts)
        c.add(cut);
    c.addAll(Arrays.asList(0, n));
    Collections.sort(c);
    int[][] dp = new int[c.size()][c.size()];
    for (int i = c.size() - 1; i >= 0; --i)
        for (int j = i + 1; j < c.size(); ++j) {
            for (int k = i + 1; k < j; ++k)
                dp[i][j] = Math.min(dp[i][j] == 0 ? Integer.MAX_VALUE : dp[i][j],
                    c.get(j) - c.get(i) + dp[i][k] + dp[k][j]);
        }
    return dp[0][c.size() - 1];    
}
```

**Complexity Analysis**
- Time: O(n ^ 3), where n is the number of cuts. It takes O(n) to calculate each case, and we have O(n ^ 2) distinct cases. 
- Memory: O(n ^ 2) for memoisation/tabulation.
</p>


### [Java/C++/Python] Merge Stones
- Author: lee215
- Creation Date: Sun Aug 09 2020 12:16:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 09 2020 12:57:52 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Similar to the problem [1000. Minimum Cost to Merge Stones](https://leetcode.com/problems/minimum-cost-to-merge-stones/discuss/247567/JavaC++Python-DP).

Instead of considering the cost to cut,
we can transform the problem to the cost to stick all sticks.

Then we have the problem "merge stones".
Though in the format of dp, they are exatly the same.
<br>

## **Explanation**
Add the "cut" index 0 and n, then sort all stick position.
`dp[i][j]` means the minimum cost to stick all sticks between `A[i]` and `A[j]`
<br>

## **Complexity**
Time `O(N^3)`
Space `O(N^2)`, can be imporved to `O(N)`
<br>

**Java**
```java
    public int minCost(int n, int[] cuts) {
        List<Integer> A = new ArrayList<>();
        for (int a : cuts) {
            A.add(a);
        }
        A.add(0);
        A.add(n);
        Collections.sort(A);
        int k = A.size();
        int[][] dp = new int[k][k];
        for (int d = 2; d < k; ++d) {
            for (int i = 0; i < k - d; ++i) {
                dp[i][i + d] = 1000000000;
                for (int m = i + 1; m < i + d; ++m) {
                    dp[i][i + d] = Math.min(dp[i][i + d], dp[i][m] + dp[m][i + d] + A.get(i + d) - A.get(i));
                }
            }
        }
        return dp[0][k - 1];
    }
```
**C++**
```cpp
    int minCost(int n, vector<int>& A) {
        A.push_back(0);
        A.push_back(n);
        sort(A.begin(), A.end());
        int k = A.size();
        vector<vector<int>> dp(k, vector<int>(k));
        for (int d = 2; d < k; ++d) {
            for (int i = 0; i < k - d; ++i) {
                dp[i][i + d] = 1e9;
                for (int m = i + 1; m < i + d; ++m) {
                    dp[i][i + d] = min(dp[i][i + d], dp[i][m] + dp[m][i + d] + A[i + d] - A[i]);
                }
            }
        }
        return dp[0][k - 1];
    }
```
**Python:**
```py
    def minCost(self, n, A):
        A = sorted(A + [0, n])
        k = len(A)
        dp = [[0] * k for _ in xrange(k)]
        for d in xrange(2, k):
            for i in xrange(k - d):
                dp[i][i + d] = min(dp[i][m] + dp[m][i + d] for m in xrange(i + 1, i + d)) + A[i + d] - A[i]
        return dp[0][k - 1]
```

</p>


### [Java] Detailed Explanation - DFS+Memo - Top Down DP
- Author: frankkkkk
- Creation Date: Sun Aug 09 2020 12:03:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 09 2020 13:55:29 GMT+0800 (Singapore Standard Time)

<p>
**Key Notes:**
- DFS+Memo is very good at handling this kind of question
- Try **every cuts in every order** and return the min of them
- Naive Implementation:
	- Use a boolean array to indicate used or not
	- Use **TreeSet to find the left and right of a cut**. (when cut at somewhere, you need to know the cost (length of stick))
	- Use the boolean array **state** as memo.

**It is simple but will TLE, let\'s improve.**
- Use boolean array as state is painful (O(N))
- Basically, we want to cache the min cost for **a specific stick [left to right]**
- We could use **[left, right] to indicate a stick and cache it** as memo
- Every cut, you will \'create\' two more sticks: **[left, cuts[i]]** and **[cuts[i], right]**

```java
public int minCost(int n, int[] cuts) {

    Arrays.sort(cuts);
	return helper(cuts, new HashMap<>(), 0, n);
}

private int helper(int[] cuts, Map<String, Integer> memo, int l, int r) {

	int res = Integer.MAX_VALUE;

	String key = l + "-" + r;
	if (memo.containsKey(key)) return memo.get(key);

	for (int i = 0; i < cuts.length; ++i) {
		if (cuts[i] <= l || cuts[i] >= r) continue;

		int cost = r - l;
		res = Math.min(
		           // min cost of left stick + cost + min cost of right stick
				   helper(cuts, memo, l, cuts[i]) + cost + helper(cuts, memo, cuts[i], r), 
				   res);
	}

	res = res == Integer.MAX_VALUE ? 0 : res;
	memo.put(key, res);

	return res;
}
```
</p>


