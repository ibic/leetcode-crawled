---
title: "Binary Tree Inorder Traversal"
weight: 94
#id: "binary-tree-inorder-traversal"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a binary tree, return <em>the inorder traversal of its nodes&#39; values</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/15/inorder_1.jpg" style="width: 202px; height: 324px;" />
<pre>
<strong>Input:</strong> root = [1,null,2,3]
<strong>Output:</strong> [1,3,2]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = []
<strong>Output:</strong> []
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [1]
<strong>Output:</strong> [1]
</pre>

<p><strong>Example 4:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/15/inorder_5.jpg" style="width: 202px; height: 202px;" />
<pre>
<strong>Input:</strong> root = [1,2]
<strong>Output:</strong> [2,1]
</pre>

<p><strong>Example 5:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/15/inorder_4.jpg" style="width: 202px; height: 202px;" />
<pre>
<strong>Input:</strong> root = [1,null,2]
<strong>Output:</strong> [1,2]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is in the range <code>[0, 100]</code>.</li>
	<li><code>-100 &lt;= Node.val &lt;= 100</code></li>
</ul>

<p>&nbsp;</p>

<p><strong>Follow up:</strong></p>

<p>Recursive solution is trivial, could you do it iteratively?</p>

<p>&nbsp;</p>

</div>

## Tags
- Hash Table (hash-table)
- Stack (stack)
- Tree (tree)

## Companies
- Oracle - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 7 (taggedByAdmin: true)
- Google - 7 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- SAP - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Recursive Approach

The first method to solve this problem is using recursion.
This is the classical method and is straightforward. We can define a helper function to implement recursion.

<iframe src="https://leetcode.com/playground/stzQZusR/shared" frameBorder="0" width="100%" height="378" name="stzQZusR"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. The time complexity is $$O(n)$$ because the recursive function is $$T(n) = 2 \cdot T(n/2)+1$$.

* Space complexity : The worst case space required is $$O(n)$$, and in the average case it's $$O(\log n)$$ where $$ n$$ is number of nodes.
<br />
<br />
---
#### Approach 2: Iterating method using Stack

The strategy is very similiar to the first method, the different is using stack.

Here is an illustration:

!?!../Documents/94_Binary.json:1000,563!?!

<iframe src="https://leetcode.com/playground/C9344qJ6/shared" frameBorder="0" width="100%" height="344" name="C9344qJ6"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$.

* Space complexity : $$O(n)$$.
<br />
<br />
---
#### Approach 3: Morris Traversal


In this method, we have to use a new data structure-Threaded Binary Tree, and the strategy is as follows:


>Step 1: Initialize current as root
>
>Step 2: While current is not NULL,
>
>     If current does not have left child
>
>         a. Add current’s value
>
>         b. Go to the right, i.e., current = current.right
>
>     Else
>
>         a. In current's left subtree, make current the right child of the rightmost node
>
>         b. Go to this left child, i.e., current = current.left


For example:
```

          1
        /   \
       2     3
      / \   /
     4   5 6

```
First, 1 is the root, so initialize 1 as current, 1 has left child which is 2, the current's left subtree is

```
         2
        / \
       4   5
```
 So in this subtree, the rightmost node is 5, then make the current(1) as the right child of 5. Set current = cuurent.left (current = 2).
The tree now looks like:
```
         2
        / \
       4   5
            \
             1
              \
               3
              /
             6
```
For current 2, which has left child 4, we can continue with thesame process as we did above
```
        4
         \
          2
           \
            5
             \
              1
               \
                3
               /
              6
```
 then add 4 because it has no left child, then add 2, 5, 1, 3 one by one, for node 3 which has left child 6, do the same as above.
Finally, the inorder taversal is [4,2,5,1,6,3].

For more details, please check
[Threaded binary tree](https://en.wikipedia.org/wiki/Threaded_binary_tree) and
[Explaination of Morris Method](https://stackoverflow.com/questions/5502916/explain-morris-inorder-tree-traversal-without-using-stacks-or-recursion)


<iframe src="https://leetcode.com/playground/osLqwuNN/shared" frameBorder="0" width="100%" height="446" name="osLqwuNN"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. To prove that the time complexity is $$O(n)$$,
the biggest problem lies in finding the time complexity of finding the predecessor nodes of all the nodes in the binary tree.
Intuitively, the complexity is $$O(n\log n)$$, because to find the predecessor node for a single node related to the height of the tree.
But in fact, finding the predecessor nodes for all nodes only needs $$O(n)$$ time. Because a binary Tree with $$n$$ nodes has $$n-1$$ edges, the whole processing for each edges up to 2 times, one is to locate a node, and the other is to find the predecessor node.
So the complexity is $$O(n)$$.

* Space complexity : $$O(n)$$. Arraylist of size $$n$$ is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Iterative solution in Java - simple and readable
- Author: lvlolitte
- Creation Date: Mon Dec 29 2014 14:03:48 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 04:01:54 GMT+0800 (Singapore Standard Time)

<p>
    
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<Integer>();

        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode cur = root;

        while(cur!=null || !stack.empty()){
            while(cur!=null){
                stack.add(cur);
                cur = cur.left;
            }
            cur = stack.pop();
            list.add(cur.val);
            cur = cur.right;
        }

        return list;
    }
</p>


### Python recursive and iterative solutions.
- Author: OldCodingFarmer
- Creation Date: Fri Aug 14 2015 22:02:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 03:27:32 GMT+0800 (Singapore Standard Time)

<p>
        
    # recursively
    def inorderTraversal1(self, root):
        res = []
        self.helper(root, res)
        return res
        
    def helper(self, root, res):
        if root:
            self.helper(root.left, res)
            res.append(root.val)
            self.helper(root.right, res)
     
    # iteratively       
    def inorderTraversal(self, root):
        res, stack = [], []
        while True:
            while root:
                stack.append(root)
                root = root.left
            if not stack:
                return res
            node = stack.pop()
            res.append(node.val)
            root = node.right
</p>


### C++ Iterative, Recursive and Morris
- Author: jianchao-li
- Creation Date: Fri May 22 2015 00:52:29 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 17:26:01 GMT+0800 (Singapore Standard Time)

<p>
There are three solutions to this problem.

 1. Iterative solution using stack: `O(n)` time and `O(n)` space;
 2. Recursive solution: `O(n)` time and `O(n)` space (function call stack);
 3. Morris traversal: `O(n)` time and `O(1)` space.

**Iterative solution using stack**

```cpp
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> nodes;
        stack<TreeNode*> todo;
        while (root || !todo.empty()) {
            while (root) {
                todo.push(root);
                root = root -> left;
            }
            root = todo.top();
            todo.pop();
            nodes.push_back(root -> val);
            root = root -> right;
        }
        return nodes;
    }
};
```

**Recursive solution**

```cpp
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> nodes;
        inorder(root, nodes);
        return nodes;
    }
private:
    void inorder(TreeNode* root, vector<int>& nodes) {
        if (!root) {
            return;
        }
        inorder(root -> left, nodes);
        nodes.push_back(root -> val);
        inorder(root -> right, nodes);
    }
};
```

**Morris traversal**

```cpp
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> nodes;
        while (root) {
            if (root -> left) {
                TreeNode* pre = root -> left;
                while (pre -> right && pre -> right != root) {
                    pre = pre -> right;
                }
                if (!pre -> right) {
                    pre -> right = root;
                    root = root -> left;
                } else {
                    pre -> right = NULL;
                    nodes.push_back(root -> val);
                    root = root -> right;
                }
            } else {
                nodes.push_back(root -> val);
                root = root -> right;
            }
        }
        return nodes;
    }
};
```		
</p>


