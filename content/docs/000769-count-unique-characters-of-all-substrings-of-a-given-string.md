---
title: "Count Unique Characters of All Substrings of a Given String"
weight: 769
#id: "count-unique-characters-of-all-substrings-of-a-given-string"
---
## Description
<div class="description">
<p>Let&#39;s define a function <code>countUniqueChars(s)</code>&nbsp;that returns the number of unique characters on <code>s</code>, for example if <code>s = &quot;LEETCODE&quot;</code>&nbsp;then <code>&quot;L&quot;</code>, <code>&quot;T&quot;</code>,<code>&quot;C&quot;</code>,<code>&quot;O&quot;</code>,<code>&quot;D&quot;</code> are the unique characters since they appear only once in <code>s</code>, therefore&nbsp;<code>countUniqueChars(s) = 5</code>.<br />
<br />
On this problem given a string <code>s</code> we need to return the sum of&nbsp;<code>countUniqueChars(t)</code>&nbsp;where <code>t</code> is a substring of <code>s</code>. Notice that some substrings can be repeated so on this case you have to count the repeated ones too.</p>

<p>Since the answer can be very large, return&nbsp;the answer&nbsp;modulo&nbsp;<code>10 ^ 9 + 7</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;ABC&quot;
<strong>Output:</strong> 10
<strong>Explanation: </strong>All possible substrings are: &quot;A&quot;,&quot;B&quot;,&quot;C&quot;,&quot;AB&quot;,&quot;BC&quot; and &quot;ABC&quot;.
Evey substring is composed with only unique letters.
Sum of lengths of all substring is 1 + 1 + 1 + 2 + 2 + 3 = 10
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;ABA&quot;
<strong>Output:</strong> 8
<strong>Explanation: </strong>The same as example 1, except <code>countUniqueChars</code>(&quot;ABA&quot;) = 1.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;LEETCODE&quot;
<strong>Output:</strong> 92
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= s.length &lt;= 10^4</code></li>
	<li><code>s</code>&nbsp;contain upper-case English letters only.</li>
</ul>

</div>

## Tags
- Two Pointers (two-pointers)

## Companies
- Microsoft - 2 (taggedByAdmin: false)
- Twitch - 5 (taggedByAdmin: false)
- ForUsAll - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Maintain Answer of Suffix [Accepted]

**Intuition**

We can think of substrings as two for-loops, for the left and right boundary of the substring.  To get a handle on this problem, let's try to answer the question: what is the answer over all substrings that start at index `i`?  Let's call this $$F(i)$$.  If we can compute this faster than linear (brute force), we have an approach.

Now let $$U$$ be the unique letters function, eg. $$U(\text{"LETTER"}) = 2$$.

The key idea is we can write $$U$$ as a sum of disjoint functions over each character.  Let $$U_{\text{"A"}}(x)$$ be $$1$$ if $$\text{"A"}$$ occurs exactly once in $$x$$, otherwise $$0$$, and so on with every letter.  Then $$U(x) = \sum_{c \in \mathcal{A}} U_c(x)$$, where $$\mathcal{A} = \{ \text{"A"}, \text{"B"}, \dots \}$$ is the alphabet.

**Algorithm**

This means we only need to answer the following question (26 times, one for each character): how many substrings have exactly one $$\text{"A"}$$?  If we knew that `S[10] = S[14] = S[20] = "A"` (and only those indexes have an `"A"`), then when `i = 8`, the answer is `4` (`j = 10, 11, 12, 13`); when `i = 12` the answer is `6` (`j = 14, 15, 16, 17, 18, 19`), and so on.

In total, $$F(0) = \sum_{c \in \mathcal{A}} \text{index}[c][1] - \text{index}[c][0]$$, where `index[c]` are the indices `i` (in order) where `S[i] == c` (and padded with `S.length` if out of bounds).  In the above example, `index["A"] = [10, 14, 20]`.

Now, we want the final answer of $$\sum_{i \geq 0} F(i)$$.  There is a two pointer approach: how does $$F(1)$$ differ from $$F(0)$$?  If for example `S[0] == "B"`, then most of the sum remains unchanged (specifically, $$\sum_{c \in \mathcal{A}, c \neq \text{"B"}} \text{index}[c][1] - \text{index}[c][0]$$), and only the $$c = \text{"B"}$$ part changes, from $$\text{index}[\text{"B"}][1] - \text{index}[\text{"B"}][0]$$ to $$\text{index}[\text{"B"}][2] - \text{index}[\text{"B"}][1]$$.

We can manage this in general by keeping track of `peek[c]`, which tells us the correct index `i = peek[c]` such that our current contribution by character `c` of $$F(i)$$ is `index[c][peek[c] + 1] - index[c][peek[c]]`.


<iframe src="https://leetcode.com/playground/BvJwQ4qF/shared" frameBorder="0" width="100%" height="500" name="BvJwQ4qF"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.

* Space Complexity: $$O(N)$$.

---
#### Approach #2: Split by Character [Accepted]

**Intuition**

As in *Approach #1*, we have $$U(x) = \sum_{c \in \mathcal{A}} U_c(x)$$, where $$\mathcal{A} = \{ \text{"A"}, \text{"B"}, \dots \}$$ is the alphabet, and we only need to answer the following question (26 times, one for each character): how many substrings have exactly one $$\text{"A"}$$?

**Algorithm**

Consider how many substrings have a specific $$\text{"A"}$$.  For example, let's say `S` only has three `"A"`'s, at '`S[10] = S[14] = S[20] = "A"`; and we want to know the number of substrings that contain `S[14]`.  The answer is that there are 4 choices for the left boundary of the substring `(11, 12, 13, 14)`, and 6 choices for the right boundary `(14, 15, 16, 17, 18, 19)`.  So in total, there are 24 substrings that have `S[14]` as their unique `"A"`.

Continuing our example, if we wanted to count the number of substrings that have `S[10]`, this would be `10 * 4` - note that when there is no more `"A"` characters to the left of `S[10]`, we have to count up to the left edge of the string.

We can add up all these possibilities to get our final answer.

<iframe src="https://leetcode.com/playground/WBAYrDuV/shared" frameBorder="0" width="100%" height="395" name="WBAYrDuV"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.

* Space Complexity: $$O(N)$$.  We could reduce this to $$O(\mathcal{A})$$ if we do not store all the indices, but compute the answer on the fly.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] One pass O(N)
- Author: lee215
- Creation Date: Sun May 06 2018 11:04:05 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Feb 06 2020 20:54:39 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Let\'s think about how a character can be found as a unique character.

Think about string `"XAXAXXAX"` and focus on making the second `"A"` a unique character.
We can take `"XA(XAXX)AX"` and between `"()"` is our substring.
We can see here, to make the second "A" counted as a uniq character, we need to:
1. insert `"("` somewhere between the first and second `A`
2. insert `")"` somewhere between the second and third `A`

For step 1 we have `"A(XA"` and `"AX(A"`, 2 possibility.
For step 2 we have `"A)XXA"`, `"AX)XA"` and `"AXX)A"`, 3 possibilities.

So there are in total `2 * 3 = 6` ways to make the second `A` a unique character in a substring.
In other words, there are only 6 substring, in which this `A` contribute 1 point as unique string.


**Instead of** counting all unique characters and struggling with all possible substrings,
we can count for every char in S, how many ways to be found as a unique char.
We count and sum, and it will be out answer.
<br>

# Explanation
1. `index[26][2]` record last two occurrence index for every upper characters.
2. Initialise all values in `index` to `-1`.
3. Loop on string S, for every character `c`, update its last two occurrence index to `index[c]`.
4. Count when loop. For example, if "A" appears twice at index 3, 6, 9 seperately, we need to count:
    * For the first "A": (6-3) * (3-(-1))"
    * For the second "A": (9-6) * (6-3)"
    * For the third "A": (N-9) * (9-6)"
<br>

# Complexity
One pass, time complexity `O(N)`.
Space complexity `O(1)`.
<br>

**C++:**
```cpp
    int uniqueLetterString(string S) {
        int index[26][2], res = 0, N = S.length(), mod = pow(10, 9) + 7;
        memset(index, -1, sizeof(int) * 52);
        for (int i = 0; i < N; ++i) {
            int c = S[i] - \'A\';
            res = (res + (i - index[c][1]) * (index[c][1] - index[c][0]) % mod) % mod;
            index[c][0] = index[c][1];
            index[c][1] = i;
        }
        for (int c = 0; c < 26; ++c)
            res = (res + (N - index[c][1]) * (index[c][1] - index[c][0]) % mod) % mod;
        return res;
    }

```

**Java:**
```java
    public int uniqueLetterString(String S) {
        int[][] index = new int[26][2];
        for (int i = 0; i < 26; ++i) Arrays.fill(index[i], -1);
        int res = 0, N = S.length(), mod = (int)Math.pow(10, 9) + 7;
        for (int i = 0; i < N; ++i) {
            int c = S.charAt(i) - \'A\';
            res = (res + (i - index[c][1]) * (index[c][1] - index[c][0]) % mod) % mod;
            index[c] = new int[] {index[c][1], i};
        }
        for (int c = 0; c < 26; ++c)
            res = (res + (N - index[c][1]) * (index[c][1] - index[c][0]) % mod) % mod;
        return res;
    }

```

**Python:**
```py
    def uniqueLetterString(self, S):
        index = {c: [-1, -1] for c in string.ascii_uppercase}
        res = 0
        for i, c in enumerate(S):
            k, j = index[c]
            res += (i - j) * (j - k)
            index[c] = [j, i]
        for c in index:
            k, j = index[c]
            res += (len(S) - j) * (j - k)
        return res % (10**9 + 7)
```


</p>


### O(N) Java Solution, DP, Clear and easy to Understand
- Author: akliang
- Creation Date: Sun May 06 2018 13:20:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 09 2018 00:34:46 GMT+0800 (Singapore Standard Time)

<p>
In each loop, We caculate `cur[i]`, which represent the sum of Uniq() for all substrings whose last char is S.charAt(i).

For example, 
`S = \'ABCBD\'`
When i = 2,  `cur[2] = Uniq(\'ABC\') +  Uniq(\'BC\') +  Uniq(\'C\')`
When i = 3,  `cur[3] = Uniq(\'ABCB\') +  Uniq(\'BCB\') +  Uniq(\'CB\') +  Uniq(\'B\')`

Notice, we append char \'B\' into each previous substrings. Only in substrings \'CB\' and \'B\', the char \'B\' can be identified as uniq. The contribution of \'B\' from cur[2] to cur[3] is `i - showLastPosition[\'B\']`. At the same time, in substrings \'ABCB\', \'BCB\', the char \'B\' can\u2018t\u2019 be identified as uniq any more, the previous  contribution of \'B\' should be removed.

So we have`\'cur[i] = cur[i - 1] - contribution[S.charAt(i)] +  (i - showLastPosition[S.charAt(i)]) `
Then the new `contribution[S.charAt(i)] = i - showLastPosition[S.charAt(i)]`

The final result is the sum of all `cur[i]`. 

Thanks [@wangzi6147](https://discuss.leetcode.com/user/wangzi6147) for this picture:
![image](https://s3-lc-upload.s3.amazonaws.com/users/ankailiang/image_1525586587.png)




This is my code. I didn\'t do mod operation, but it still pass : )
```
    public int uniqueLetterString(String S) {
        
        int res = 0;
        if (S == null || S.length() == 0)
            return res;    
        int[] showLastPosition = new int[128];
        int[] contribution = new int[128];
        int cur = 0;
        for (int i = 0; i < S.length(); i++) {
            char x = S.charAt(i);
            cur -= contribution[x]; 
            contribution[x] = (i - (showLastPosition[x] - 1));
            cur += contribution[x]; 
            showLastPosition[x] = i + 1;
            res += cur;
        }   
        return res;
    }
```

The showLastPosition[x] represent the last showing positon of char x befor i. (The initial showLastPosition[x] for all chars should be \'-1\'. For convenience, I shift it to \'0\'. It may cause some confusion, notice I update "showLastPosition[x] = i + 1" for each loop.)
</p>


### Concise DP O(n) solution
- Author: meng789987
- Creation Date: Fri Aug 10 2018 11:03:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 22 2018 20:26:08 GMT+0800 (Singapore Standard Time)

<p>
Let `dp[i]` is sum of unique char in all substring ending at `i`, then the answer is `sum(dp[i]), i=[0..n-1]`. It\'s not difficult to get the recursive formula:
   `dp[i] = dp[i-1] + (i - first_from_i(s[i])) - (first_from_i(s[i]) - second_from_i(s[i]))`
	 
Take the below example:
```
BBBBBBBBBBBBBBBBBOABCDOABCOABC
                 ^    ^   ^
                 s    f   i

dp[i] = dp[i-1] + (i-f) - (f-s)
```
When extending `s[i]` to all substrings ending with `s[i-1]`, there are `(i-f)` more unique char `s[i]`, and `(f-s)` less unique char because of duplicate of `s[i]`.

The implementation is quite simple. Here is Java version:
```
    public int uniqueLetterString(String s) {
            final int MOD = 1000000007;
            int res = 0, dp = 0;
            int[] first = new int[26], second = new int[26];

            for (int i = 0; i < s.length(); i++) {
                int ci = s.charAt(i) - \'A\';
                dp = dp + 1 + i - first[ci] * 2 + second[ci];
                res = (res + dp) % MOD;

                second[ci] = first[ci];
                first[ci] = i + 1;
            }

            return res;
    }
```

Please vote if you like :)
</p>


