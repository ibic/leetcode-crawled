---
title: "Minimum Window Substring"
weight: 76
#id: "minimum-window-substring"
---
## Description
<div class="description">
<p>Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input: S</strong> = &quot;ADOBECODEBANC&quot;, <strong>T</strong> = &quot;ABC&quot;
<strong>Output:</strong> &quot;BANC&quot;
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li>If there is no such window in S that covers all characters in T, return the empty string <code>&quot;&quot;</code>.</li>
	<li>If there is such window, you are guaranteed that there will always be only one unique minimum window in S.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Two Pointers (two-pointers)
- String (string)
- Sliding Window (sliding-window)

## Companies
- Facebook - 22 (taggedByAdmin: true)
- Amazon - 15 (taggedByAdmin: false)
- Google - 8 (taggedByAdmin: false)
- Adobe - 4 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- LinkedIn - 8 (taggedByAdmin: true)
- Cohesity - 4 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Visa - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- eBay - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- GoDaddy - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Snapchat - 3 (taggedByAdmin: true)
- Twitter - 3 (taggedByAdmin: false)
- Deutsche Bank - 3 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Nutanix - 2 (taggedByAdmin: false)
- Tencent - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---

#### Approach 1: Sliding Window

**Intuition**

The question asks us to return the minimum window from the string $$S$$ which has all the characters of the string $$T$$. Let us call a window `desirable` if it has all the characters from $$T$$.

We can use a simple sliding window approach to solve this problem.

In any sliding window based problem we have two pointers. One $$right$$ pointer whose job is to expand the current window and then we have the $$left$$ pointer whose job is to contract a given window. At any point in time only one of these pointers move and the other one remains fixed.

The solution is pretty intuitive. We keep expanding the window by moving the right pointer. When the window has all the desired characters, we contract (if possible) and save the smallest window till now.

The answer is the smallest desirable window.

For eg. ` S = "ABAACBAB" T = "ABC"`. Then our answer window is `"ACB"` and shown below is one of the possible desirable windows.
<center>
<img src="../Figures/76/76_Minimum_Window_Substring_1.png" width="500"/>
</center>
<br>

**Algorithm**

1. We start with two pointers, $$left$$ and $$right$$ initially pointing to the first element of the string $$S$$.

2. We use the $$right$$ pointer to expand the window until we get a desirable window i.e. a window that contains all of the characters of $$T$$.

3. Once we have a window with all the characters, we can move the left pointer ahead one by one. If the window is still a desirable one we keep on updating the minimum window size.

4. If the window is not desirable any more, we repeat $$step \; 2$$ onwards.

<center>
<img src="../Figures/76/76_Minimum_Window_Substring_2.png" width="500"/>
</center>

The above steps are repeated until we have looked at all the windows. The smallest window is returned.

<center>
<img src="../Figures/76/76_Minimum_Window_Substring_3.png" width="500"/>
</center>
<br>

<iframe src="https://leetcode.com/playground/XEVk3wZb/shared" frameBorder="0" width="100%" height="500" name="XEVk3wZb"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(|S| + |T|)$$ where |S| and |T| represent the lengths of strings $$S$$ and $$T$$.
In the worst case we might end up visiting every element of string $$S$$ twice, once by left pointer and once by right pointer. $$|T|$$ represents the length of string $$T$$.

* Space Complexity: $$O(|S| + |T|)$$. $$|S|$$ when the window size is equal to the entire string $$S$$. $$|T|$$ when $$T$$ has all unique characters.
<br/>
<br/>

---

#### Approach 2: Optimized Sliding Window

**Intuition**

A small improvement to the above approach can reduce the time complexity of the algorithm to $$O(2*|filtered\_S| + |S| + |T|)$$, where $$filtered\_S$$ is the string formed from S by removing all the elements not present in $$T$$.

This complexity reduction is evident when $$|filtered\_S| <<< |S|$$.

This kind of scenario might happen when length of string $$T$$ is way too small than the length of string $$S$$ and string $$S$$ consists of numerous characters which are not present in $$T$$.

**Algorithm**

We create a list called $$filtered\_S$$ which has all the characters from string $$S$$ along with their indices in $$S$$, but these characters should be present in $$T$$.

<pre>
  S = "ABCDDDDDDEEAFFBC" T = "ABC"
  filtered_S = [(0, 'A'), (1, 'B'), (2, 'C'), (11, 'A'), (14, 'B'), (15, 'C')]
  Here (0, 'A') means in string S character A is at index 0.
</pre>


We can now follow our sliding window approach on the smaller string $$filtered\_S$$.

<iframe src="https://leetcode.com/playground/EyRdt4FZ/shared" frameBorder="0" width="100%" height="500" name="EyRdt4FZ"></iframe>

**Complexity Analysis**

* Time Complexity : $$O(|S| + |T|)$$ where |S| and |T| represent the lengths of strings $$S$$ and $$T$$. The complexity is same as the previous approach. But in certain cases where $$|filtered\_S|$$ <<< $$|S|$$, the complexity would reduce because the number of iterations would be $$2*|filtered\_S| + |S| + |T|$$.
* Space Complexity : $$O(|S| + |T|)$$.
<br /><br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Here is a 10-line template that can solve most 'substring' problems
- Author: zjh08177
- Creation Date: Thu Dec 03 2015 05:18:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 09:23:06 GMT+0800 (Singapore Standard Time)

<p>
I will first give the solution then show you the magic template.

**The code of solving this problem is below. It might be the shortest among all solutions provided in Discuss**.

    string minWindow(string s, string t) {
            vector<int> map(128,0);
            for(auto c: t) map[c]++;
            int counter=t.size(), begin=0, end=0, d=INT_MAX, head=0;
            while(end<s.size()){
                if(map[s[end++]]-->0) counter--; //in t
                while(counter==0){ //valid
                    if(end-begin<d)  d=end-(head=begin);
                    if(map[s[begin++]]++==0) counter++;  //make it invalid
                }  
            }
            return d==INT_MAX? "":s.substr(head, d);
        }

**Here comes the template.**

For most substring problem, we are given a string and need to find a substring of it which satisfy some restrictions. A general way is to use a hashmap assisted with two pointers. The template is given below.
 

    int findSubstring(string s){
            vector<int> map(128,0);
            int counter; // check whether the substring is valid
            int begin=0, end=0; //two pointers, one point to tail and one  head
            int d; //the length of substring

            for() { /* initialize the hash map here */ }
    
            while(end<s.size()){

                if(map[s[end++]]-- ?){  /* modify counter here */ }
    
                while(/* counter condition */){ 
                     
                     /* update d here if finding minimum*/

                    //increase begin to make it invalid/valid again
                    
                    if(map[s[begin++]]++ ?){ /*modify counter here*/ }
                }  
  
                /* update d here if finding maximum*/
            }
            return d;
      }

*One thing needs to be mentioned is that when asked to find maximum substring, we should update maximum after the inner while loop to guarantee that the substring is valid. On the other hand, when asked to find minimum substring, we should update minimum inside the inner while loop.*


The code of solving **Longest Substring with At Most Two Distinct Characters** is below:

    int lengthOfLongestSubstringTwoDistinct(string s) {
            vector<int> map(128, 0);
            int counter=0, begin=0, end=0, d=0; 
            while(end<s.size()){
                if(map[s[end++]]++==0) counter++;
                while(counter>2) if(map[s[begin++]]--==1) counter--;
                d=max(d, end-begin);
            }
            return d;
        }

The code of solving **Longest Substring Without Repeating Characters** is below:

**Update 01.04.2016, thanks @weiyi3 for advise.**

    int lengthOfLongestSubstring(string s) {
            vector<int> map(128,0);
            int counter=0, begin=0, end=0, d=0; 
            while(end<s.size()){
                if(map[s[end++]]++>0) counter++; 
                while(counter>0) if(map[s[begin++]]-->1) counter--;
                d=max(d, end-begin); //while valid, update d
            }
            return d;
        }
    
I think this post deserves some upvotes! : )
</p>


### 12 lines Python
- Author: StefanPochmann
- Creation Date: Wed Aug 05 2015 17:20:09 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 10:38:03 GMT+0800 (Singapore Standard Time)

<p>
The current window is `s[i:j]` and the result window is `s[I:J]`. In `need[c]` I store how many times I need character `c` (can be negative) and `missing` tells how many characters are still missing. In the loop, first add the new character to the window. Then, if nothing is missing, remove as much as possible from the window start and then update the result.

    def minWindow(self, s, t):
        need, missing = collections.Counter(t), len(t)
        i = I = J = 0
        for j, c in enumerate(s, 1):
            missing -= need[c] > 0
            need[c] -= 1
            if not missing:
                while i < j and need[s[i]] < 0:
                    need[s[i]] += 1
                    i += 1
                if not J or j - i <= J - I:
                    I, J = i, j
        return s[I:J]
</p>


### Java solution. using two pointers + HashMap
- Author: Three_Thousand_world
- Creation Date: Wed Aug 12 2015 03:49:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 12:37:26 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
    public String minWindow(String s, String t) {
        if(s == null || s.length() < t.length() || s.length() == 0){
            return "";
        }
        HashMap<Character,Integer> map = new HashMap<Character,Integer>();
        for(char c : t.toCharArray()){
            if(map.containsKey(c)){
                map.put(c,map.get(c)+1);
            }else{
                map.put(c,1);
            }
        }
        int left = 0;
        int minLeft = 0;
        int minLen = s.length()+1;
        int count = 0;
        for(int right = 0; right < s.length(); right++){
            if(map.containsKey(s.charAt(right))){
                map.put(s.charAt(right),map.get(s.charAt(right))-1);
                if(map.get(s.charAt(right)) >= 0){
                    count ++;
                }
                while(count == t.length()){
                    if(right-left+1 < minLen){
                        minLeft = left;
                        minLen = right-left+1;
                    }
                    if(map.containsKey(s.charAt(left))){
                        map.put(s.charAt(left),map.get(s.charAt(left))+1);
                        if(map.get(s.charAt(left)) > 0){
                            count --;
                        }
                    }
                    left ++ ;
                }
            }
        }
        if(minLen>s.length())  
        {  
            return "";  
        }  
        
        return s.substring(minLeft,minLeft+minLen);
    }
}
</p>


