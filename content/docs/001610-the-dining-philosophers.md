---
title: "The Dining Philosophers"
weight: 1610
#id: "the-dining-philosophers"
---
## Description
<div class="description">
<p>Five silent philosophers&nbsp;sit at a round table with bowls of spaghetti. Forks are placed between each pair of adjacent philosophers.</p>

<p>Each philosopher must alternately think and eat. However, a philosopher can only eat spaghetti when they have both left and right forks. Each fork can be held by only one philosopher and so a philosopher can use the fork only if it is not being used by another philosopher. After an individual philosopher finishes eating, they need to put down both forks so that the forks become available to others. A philosopher can take the fork on their right or the one on their left as they become available, but cannot start eating before getting both forks.</p>

<p>Eating is not limited by the remaining amounts of spaghetti or stomach space; an infinite supply and an infinite demand are assumed.</p>

<p>Design a discipline of behaviour (a concurrent algorithm) such that no philosopher will starve;&nbsp;<i>i.e.</i>, each can forever continue to alternate between eating and thinking, assuming that no philosopher can know when others may want to eat or think.</p>

<p style="text-align: center"><img alt="" src="https://assets.leetcode.com/uploads/2019/09/24/an_illustration_of_the_dining_philosophers_problem.png" style="width: 400px; height: 415px;" /></p>

<p style="text-align: center"><em>The problem statement and the image above are taken from <a href="https://en.wikipedia.org/wiki/Dining_philosophers_problem" target="_blank">wikipedia.org</a></em></p>

<p>&nbsp;</p>

<p>The philosophers&#39; ids are numbered from <strong>0</strong> to <strong>4</strong> in a <strong>clockwise</strong> order. Implement the function&nbsp;<code>void wantsToEat(philosopher, pickLeftFork, pickRightFork, eat, putLeftFork, putRightFork)</code> where:</p>

<ul>
	<li><code>philosopher</code>&nbsp;is the id of the philosopher who wants to eat.</li>
	<li><code>pickLeftFork</code>&nbsp;and&nbsp;<code>pickRightFork</code>&nbsp;are functions you can call to pick the corresponding forks of that philosopher.</li>
	<li><code>eat</code>&nbsp;is a function you can call to let the philosopher eat once he has picked&nbsp;both forks.</li>
	<li><code>putLeftFork</code>&nbsp;and&nbsp;<code>putRightFork</code>&nbsp;are functions you can call to put down the corresponding forks of that philosopher.</li>
	<li>The philosophers are assumed to be thinking as long as they are not asking to eat (the function is not being called with their number).</li>
</ul>

<p>Five threads, each representing a philosopher, will&nbsp;simultaneously use one object of your class to simulate the process. The function may be called for the same philosopher more than once, even before the last call ends.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> [[4,2,1],[4,1,1],[0,1,1],[2,2,1],[2,1,1],[2,0,3],[2,1,2],[2,2,2],[4,0,3],[4,1,2],[0,2,1],[4,2,2],[3,2,1],[3,1,1],[0,0,3],[0,1,2],[0,2,2],[1,2,1],[1,1,1],[3,0,3],[3,1,2],[3,2,2],[1,0,3],[1,1,2],[1,2,2]]
<strong>Explanation:</strong>
n is the number of times each philosopher will call the function.
The output array describes the calls you made to the functions controlling the forks and the eat function, its format is:
output[i] = [a, b, c] (three integers)
- a is the id of a philosopher.
- b specifies the fork: {1 : left, 2 : right}.
- c specifies the operation: {1 : pick, 2 : put, 3 : eat}.</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 60</code></li>
</ul>

</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python Two Solutions
- Author: awice
- Creation Date: Sat Oct 19 2019 07:41:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 19 2019 07:41:04 GMT+0800 (Singapore Standard Time)

<p>
In both solutions, we have to analyze when a deadlock happens.  If a philosopher picks up 2 forks, then they will set them down after.  So a philosopher could only really get stuck on 0 or 1 forks.  Its easy to see that this means a deadlock only occurs if 5 forks are picked up (1 for each philosopher).

**Solution 1:**
Enforce that at most 4 philosophers can approach the table with `sizelock`.  Then at most 4 forks are picked up, so there can\'t be a deadlock.

**Solution 2:**
Enforce that some philosophers pick up forks left then right, and others pick them up right then left.  Then a fork is preferred by two neighboring philosophers, guaranteeing that if one of them has 1 fork, the other has 0, and thus at most 4 forks are picked up.

```
from threading import Semaphore

class DiningPhilosophers:
    def __init__(self):
        self.sizelock = Semaphore(4)
        self.locks = [Semaphore(1) for _ in range(5)]

    def wantsToEat(self, index, *actions):
        left, right = index, (index - 1) % 5
        with self.sizelock:
            with self.locks[left], self.locks[right]:
                for action in actions:
                    action()
```

```
from threading import Semaphore

class DiningPhilosophers:
    def __init__(self):
        self.locks = [Semaphore(1) for _ in range(5)]

    def wantsToEat(self, index, *actions):
        left, right = index, (index - 1) % 5
        
        if index:
            with self.locks[left], self.locks[right]:
                for action in actions:
                    action()
        else:
            with self.locks[right], self.locks[left]:
                for action in actions:
                    action()
```
</p>


### [JAVA] Using semaphores
- Author: atjha
- Creation Date: Wed Oct 23 2019 10:49:42 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Apr 24 2020 23:21:17 GMT+0800 (Singapore Standard Time)

<p>
```
class DiningPhilosophers {

    Semaphore[] sems;
    public DiningPhilosophers() {
        this.sems = new Semaphore[5];
        for (int i = 0; i < 5; i++) {
            sems[i] = new Semaphore(1);
        }
    }

    public void wantsToEat(int philosopher,
                           Runnable pickLeftFork,
                           Runnable pickRightFork,
                           Runnable eat,
                           Runnable putLeftFork,
                           Runnable putRightFork) throws InterruptedException {
        Semaphore left = sems[philosopher];
        Semaphore right = (philosopher == 0) ? sems[4] : sems[philosopher - 1];
        
        if (philosopher % 2 == 0) {
            left.acquire();
            pickLeftFork.run();
            right.acquire();
            pickRightFork.run();
        } else {
            right.acquire();
            pickRightFork.run();
            left.acquire();
            pickLeftFork.run();
        }
        
        eat.run();
        
        if (philosopher % 2 == 0) {
            putRightFork.run();
            right.release();
            putLeftFork.run();
            left.release();
        } else {
            putLeftFork.run();
            left.release();
            putRightFork.run();
            right.release();
        }
    }
}
```
</p>


### Semaphore + mutex
- Author: coder206
- Creation Date: Fri Oct 18 2019 06:52:32 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 18 2019 06:52:32 GMT+0800 (Singapore Standard Time)

<p>
Every philosopher starts by picking the left fork and after that he picks the right one. Without any limitations a deadlock is possible when all philosophers pick the left fork simultaneously. If we limit the number of philosophers who can pick the left fork to 4 there will always be a philosopher who can pick the right fork (the one to the left of a philosopher without any forks).
```
class DiningPhilosophers
{
    private Lock forks[] = new Lock[5];
    private Semaphore semaphore = new Semaphore(4);
    
    public DiningPhilosophers()
    {
        for (int i = 0; i < 5; i++)
            forks[i] = new ReentrantLock();
    }
    
    void pickFork(int id, Runnable pick)
    {
        forks[id].lock();
        pick.run();
    }
    
    void putFork(int id, Runnable put)
    {
        put.run();
        forks[id].unlock();
    }
    
    // call the run() method of any runnable to execute its code
    public void wantsToEat(int philosopher,
                           Runnable pickLeftFork,
                           Runnable pickRightFork,
                           Runnable eat,
                           Runnable putLeftFork,
                           Runnable putRightFork) throws InterruptedException
    {
        int leftFork = philosopher;
        int rightFork = (philosopher + 4) % 5;
        
        semaphore.acquire();
        
        pickFork(leftFork, pickLeftFork);
        pickFork(rightFork, pickRightFork);
        eat.run();
        putFork(rightFork, putRightFork);
        putFork(leftFork, putLeftFork);
        
        semaphore.release();
    }
}
```
</p>


