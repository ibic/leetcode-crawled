---
title: "Jump Game V"
weight: 1257
#id: "jump-game-v"
---
## Description
<div class="description">
<p>Given an array of&nbsp;integers <code>arr</code> and an integer <code>d</code>. In one step you can jump from index <code>i</code> to index:</p>

<ul>
	<li><code>i + x</code> where:&nbsp;<code>i + x &lt; arr.length</code> and <code> 0 &lt;&nbsp;x &lt;= d</code>.</li>
	<li><code>i - x</code> where:&nbsp;<code>i - x &gt;= 0</code> and <code> 0 &lt;&nbsp;x &lt;= d</code>.</li>
</ul>

<p>In addition, you can only jump from index <code>i</code> to index <code>j</code>&nbsp;if <code>arr[i] &gt; arr[j]</code> and <code>arr[i] &gt; arr[k]</code> for all indices <code>k</code> between <code>i</code> and <code>j</code> (More formally <code>min(i,&nbsp;j) &lt; k &lt; max(i, j)</code>).</p>

<p>You can choose any index of the array and start jumping. Return <em>the maximum number of indices</em>&nbsp;you can visit.</p>

<p>Notice that you can not jump outside of the array at any time.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/01/23/meta-chart.jpeg" style="width: 633px; height: 419px;" />
<pre>
<strong>Input:</strong> arr = [6,4,14,6,8,13,9,7,10,6,12], d = 2
<strong>Output:</strong> 4
<strong>Explanation:</strong> You can start at index 10. You can jump 10 --&gt; 8 --&gt; 6 --&gt; 7 as shown.
Note that if you start at index 6 you can only jump to index 7. You cannot jump to index 5 because 13 &gt; 9. You cannot jump to index 4 because index 5 is between index 4 and 6 and 13 &gt; 9.
Similarly You cannot jump from index 3 to index 2 or index 1.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [3,3,3,3,3], d = 3
<strong>Output:</strong> 1
<strong>Explanation:</strong> You can start at any index. You always cannot jump to any index.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [7,6,5,4,3,2,1], d = 1
<strong>Output:</strong> 7
<strong>Explanation:</strong> Start at index 0. You can visit all the indicies. 
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [7,1,7,1,7,1], d = 2
<strong>Output:</strong> 2
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> arr = [66], d = 1
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 1000</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 10^5</code></li>
	<li><code>1 &lt;= d &lt;= arr.length</code></li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### O(N) Solution is Gone
- Author: lee215
- Creation Date: Fri Feb 07 2020 11:44:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 09 2020 22:17:41 GMT+0800 (Singapore Standard Time)

<p>
I wrote an article of `O(N)` solution, and was voted top 3.
But it\'s deleted by admin. Why?
"Because of violating community rules, Your topic [Python] Different DP Solutions, O(N) Time has been deleted by admin."
I think at least I should know the reason, right?
Is that because I added my youtube link, where I speak my mother language?

I\'ll quit the contest for this weekend.

The original article: https://mp.weixin.qq.com/s/kEQ00_WLqDTG6tbsjQ2Xjw
</p>


### Top-Down DP O(nd)
- Author: votrubac
- Creation Date: Sun Feb 02 2020 12:02:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 02 2020 12:11:51 GMT+0800 (Singapore Standard Time)

<p>
For each index `i`, run search starting from `i` left and right up to `d` steps. This way, we can easily detect if `arr[j]` is blocking further jumps.

> So, we stop the search when we encounter `j` where `arr[i] <= arr[j]`.

To prevent re-computations, we need to memoise max jumps for every index in `dp`.

```CPP
int dp[1001] = {};
int dfs(vector<int>& arr, int i, int d, int res = 1) {
    if (dp[i]) return dp[i];
    for (auto j = i + 1; j <= min(i + d, (int)arr.size() - 1) && arr[j] < arr[i]; ++j)
        res = max(res, 1 + dfs(arr, j, d));
    for (auto j = i - 1; j >= max(0, i - d) && arr[j] < arr[i]; --j)
        res = max(res, 1 + dfs(arr, j, d));
    return dp[i] = res;
}
int maxJumps(vector<int>& arr, int d, int res = 1) {
    for (auto i = 0; i < arr.size(); ++i)
        res = max(res, dfs(arr, i, d));
    return res;
}
```
**Complexity Analysis**
- Time: O(nd)
- Memory: O(n) to memoize jumps for every index.
</p>


### [C++] Top-Down DP (Memoization)
- Author: PhoenixDD
- Creation Date: Sun Feb 02 2020 12:51:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 02 2020 17:51:36 GMT+0800 (Singapore Standard Time)

<p>
**Observation**

Once we get the maximum jumps possible from a certain index it will be same no matter how we end up at that index since we can only go to a smaller valued index.
Thus we can use caching to store the results of each index as we calulate them to solve other indices.

**Solution**

Use a simple recursion to check to the right and left of the current index and cache those results in memo table.
Do this for all indices as starting indices.
Also notice that we can stop once we encounter an index who\'s value is greater than or equal to the current index in the recursion function, since we cannot jump to any index after that.

```c++
int memo[100000];
class Solution {
public:
    int d;
    int dp(vector<int>& arr,int index)
    {
        if(memo[index]!=-1)                                       //Return the cached value if exists.
            return memo[index];
        memo[index]=0;
        for(int i=index+1;i<arr.size()&&arr[i]<arr[index]&&i<=index+d;i++)     //Check the indices on the right while storing Max.
            memo[index]=max(memo[index],1+dp(arr,i));
        for(int i=index-1;i>=0&&arr[i]<arr[index]&&i>=index-d;i--)                 //Check the indices on the left while storing Max.
            memo[index]=max(memo[index],1+dp(arr,i));
        return memo[index];                                                         //Return the maximum of all checked indices.
    }
    int maxJumps(vector<int>& arr, int d) 
    {
        memset(memo,-1,sizeof memo);
        int result=0;
        this->d=d;
        for(int i=0;i<arr.size();i++)                     //Check for all indices as starting point.
            result=max(result,1+dp(arr,i));
        return result;
    }
};
```
**Complexity**
Space: `O(n)`.
Time: `O(nd)`.
</p>


