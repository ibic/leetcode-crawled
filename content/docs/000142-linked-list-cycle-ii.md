---
title: "Linked List Cycle II"
weight: 142
#id: "linked-list-cycle-ii"
---
## Description
<div class="description">
<p>Given a linked list, return the node where the cycle begins. If there is no cycle, return <code>null</code>.</p>

<p>There is a cycle in a linked list if there is some node in the list that can be reached again by continuously following the&nbsp;<code>next</code>&nbsp;pointer. Internally, <code>pos</code>&nbsp;is used to denote the index of the node that&nbsp;tail&#39;s&nbsp;<code>next</code>&nbsp;pointer is connected to.&nbsp;<strong>Note that&nbsp;<code>pos</code>&nbsp;is not passed as a parameter</strong>.</p>

<p><strong>Notice</strong> that you <strong>should not modify</strong> the linked list.</p>

<p><strong>Follow up:</strong></p>

<p>Can you solve it using <code>O(1)</code> (i.e. constant) memory?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/07/circularlinkedlist.png" style="height: 145px; width: 450px;" />
<pre>
<strong>Input:</strong> head = [3,2,0,-4], pos = 1
<strong>Output:</strong> tail connects to node index 1
<strong>Explanation:</strong> There is a cycle in the linked list, where tail connects to the second node.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/07/circularlinkedlist_test2.png" style="height: 105px; width: 201px;" />
<pre>
<strong>Input:</strong> head = [1,2], pos = 0
<strong>Output:</strong> tail connects to node index 0
<strong>Explanation:</strong> There is a cycle in the linked list, where tail connects to the first node.
</pre>

<p><strong>Example 3:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/07/circularlinkedlist_test3.png" style="height: 65px; width: 65px;" />
<pre>
<strong>Input:</strong> head = [1], pos = -1
<strong>Output:</strong> no cycle
<strong>Explanation:</strong> There is no cycle in the linked list.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of the nodes in the list is in the range <code>[0, 10<sup>4</sup>]</code>.</li>
	<li><code>-10<sup>5</sup> &lt;= Node.val &lt;= 10<sup>5</sup></code></li>
	<li><code>pos</code> is <code>-1</code> or a <strong>valid index</strong> in the linked-list.</li>
</ul>

</div>

## Tags
- Linked List (linked-list)
- Two Pointers (two-pointers)

## Companies
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Amazon - 5 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach 1: Hash Table

**Intuition**

If we keep track of the nodes that we've seen already in a `Set`, we can
traverse the list and return the first duplicate node.

**Algorithm**

First, we allocate a `Set` to store `ListNode` references. Then, we traverse
the list, checking `visited` for containment of the current node. If the node
has already been seen, then it is necessarily the entrance to the cycle. If
any other node were the entrance to the cycle, then we would have already
returned that node instead. Otherwise, the `if` condition will never be
satisfied, and our function will return `null`.

The algorithm necessarily terminates for any list with a finite number of
nodes, as the domain of input lists can be divided into two categories:
cyclic and acyclic lists. An acyclic list resembles a `null`-terminated chain
of nodes, while a cyclic list can be thought of as an acyclic list with the
final `null` replaced by a reference to some previous node. If the `while`
loop terminates, we return `null`, as we have traversed the entire list
without encountering a duplicate reference. In this case, the list is
acyclic. For a cyclic list, the `while` loop will never terminate, but at
some point the `if` condition will be satisfied and cause the function to
return.

<iframe src="https://leetcode.com/playground/cMo8bmHh/shared" frameBorder="0" width="100%" height="327" name="cMo8bmHh"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$

    For both cyclic and acyclic inputs, the algorithm must visit each node exactly once. This is transparently obvious for acyclic lists because the $$n$$th node points to `null`, causing the loop to terminate. For cyclic lists, the `if` condition will cause the function to return after visiting the $$n$$th node, as it points to some node that is already in `visited`. In both cases, the number of nodes visited is exactly $$n$$, so the runtime is linear in the number of nodes.

* Space complexity : $$O(n)$$

    For both cyclic and acyclic inputs, we will need to insert each node into the `Set` once. The only difference between the two cases is whether we discover that the "last" node points to `null` or a previously-visited node. Therefore, because the `Set` will contain $$n$$ distinct nodes, the memory footprint is linear in the number of nodes.

<br />

---
#### Approach 2: Floyd's Tortoise and Hare

**Intuition**

What happens when a fast runner (a hare) races a slow runner (a tortoise) on
a circular track? At some point, the fast runner will catch up to the slow
runner from behind.

**Algorithm**

Floyd's algorithm is separated into two distinct _phases_. In the first
phase, it determines whether a cycle is present in the list. If no cycle is
present, it returns `null` immediately, as it is impossible to find the
entrance to a nonexistant cycle. Otherwise, it uses the located "intersection
node" to find the entrance to the cycle.

*Phase 1*

Here, we initialize two pointers - the fast `hare` and the slow `tortoise`.
Then, until `hare` can no longer advance, we increment `tortoise` once and
`hare` twice.[^1] If, after advancing them, `hare` and `tortoise` point to
the same node, we return it. Otherwise, we continue. If the `while` loop
terminates without returning a node, then the list is acyclic, and we return
`null` to indicate as much.

To see why this works, consider the image below:

<center>
    <img src="../Figures/142/Slide1.PNG" alt="Diagram of cyclic list" style="width: 650px;"/>
</center>

Here, the nodes in the cycle have been labelled from 0 to $$C-1$$, where
$$C$$ is the length of the cycle. The noncyclic nodes have been labelled from
$$-F$$ to -1, where $$F$$ is the number of nodes outside of the cycle. After
$$F$$ iterations, `tortoise` points to node 0 and `hare` points to some node
$$h$$, where $$F \equiv h \pmod C$$. This is because `hare` traverses $$2F$$
nodes over the course of $$F$$ iterations, exactly $$F$$ of which are in the
cycle. After $$C-h$$ more iterations, `tortoise` obviously points to node
$$C-h$$, but (less obviously) `hare` also points to the same node. To see why,
remember that `hare` traverses $$2(C-h)$$ from its starting position of $$h$$:

$$
\begin{aligned}
    h + 2(C-h) &= 2C - h \\
                &\equiv C-h \pmod C
\end{aligned}
$$

Therefore, given that the list is cyclic, `hare` and `tortoise` will
eventually both point to the same node, known henceforce as the
_intersection_.

*Phase 2*

Given that phase 1 finds an intersection, phase 2 proceeds to find the node
that is the entrance to the cycle. To do so, we initialize two more pointers:
`ptr1`, which points to the head of the list, and `ptr2`, which points to
the intersection. Then, we advance each of them by 1 until they meet; the
node where they meet is the entrance to the cycle, so we return it.

Use the diagram below to help understand the proof of this approach's
correctness.

<center>
    <img src="../Figures/142/diagram.png" alt="Phase 2 diagram" style="width: 650px;"/>
</center>

We can harness the fact that `hare` moves twice as quickly as `tortoise` to
assert that when `hare` and `tortoise` meet at node $$h$$, `hare` has
traversed twice as many nodes. Using this fact, we deduce the following:

$$
\begin{aligned}
    2 \cdot distance(tortoise) &= distance(hare) \\
                        2(F+a) &= F+a+b+a \\
                         2F+2a &= F+2a+b \\
                             F &= b \\
\end{aligned}
$$

Because $$F=b$$, pointers starting at nodes $$h$$ and $$0$$ will traverse the
same number of nodes before meeting.

To see the entire algorithm in action, check out the animation below:

!?!../Documents/142_Linked_List_Cycle_II.json:1280,720!?!

<iframe src="https://leetcode.com/playground/ZZzspTLp/shared" frameBorder="0" width="100%" height="500" name="ZZzspTLp"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$

    For cyclic lists, `hare` and `tortoise` will point to the same node after
    $$F+C-h$$ iterations, as demonstrated in the proof of correctness.
    $$F+C-h \leq F+C = n$$, so phase 1 runs in $$O(n)$$ time. Phase 2
    runs for $$F < n$$ iterations, so it also runs in $$O(n)$$ time.

    For acyclic lists, `hare` will reach the end of the list in roughly
    $$\dfrac{n}{2}$$ iterations, causing the function to return before phase
    2. Therefore, regardless of which category of list the algorithm
    receives, it runs in time linearly proportional to the number of nodes.

* Space complexity : $$O(1)$$

    Floyd's Tortoise and Hare algorithm allocates only pointers, so it runs
    with constant overall memory usage.


**Footnotes**

[^1]: It is sufficient to check only `hare` because it will always be ahead
of `tortoise` in an acyclic list.

## Accepted Submission (java)
```java
import java.util.*;

/**
 * Definition for singly-linked list.
 * class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
	public ListNode detectCycle(ListNode head) {
		if (head == null) {
			return null;
		}
		ListNode p1 = head;
		ListNode p2 = head;
		while (p2.next != null && p2.next.next != null) {
			p2 = p2.next.next;
			p1 = p1.next;
			if (p1 == p2) {
				p1 = head;
				while (p2 != p1) {
					p2 = p2.next;
					p1 = p1.next;
				}
				return p1;
			}
		}
		return null;
	}
}

```

## Top Discussions
### O(n) solution by using two pointers without change anything
- Author: wall0p
- Creation Date: Wed Aug 27 2014 01:23:09 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 20:28:52 GMT+0800 (Singapore Standard Time)

<p>
my solution is like this: using two pointers, one of them one step at a time. another pointer each take two steps. Suppose the first meet at step ***k***,the length of the Cycle is ***r***.   so..**2k-k=nr,k=nr**
Now, the distance between the start node of list and the start node of cycle is  ***s***. the distance between the start of list and the first meeting node is ***k***(the pointer which wake one step at a time waked k steps).the distance between the start node of cycle and the first meeting node is ***m***, so...**s=k-m,
s=nr-m=(n-1)r+(r-m),here we takes n = 1**..so, using one pointer start from the start node of list, another pointer start from the first meeting node, all of them wake one step at a time, the first time they meeting each other is the start of the cycle.

        ListNode *detectCycle(ListNode *head) {
        if (head == NULL || head->next == NULL) return NULL;
        
        ListNode* firstp = head;
        ListNode* secondp = head;
        bool isCycle = false;
        
        while(firstp != NULL && secondp != NULL) {
            firstp = firstp->next;
            if (secondp->next == NULL) return NULL;
            secondp = secondp->next->next;
            if (firstp == secondp) { isCycle = true; break; }
        }
        
        if(!isCycle) return NULL;
        firstp = head;
        while( firstp != secondp) {
            firstp = firstp->next;
            secondp = secondp->next;
        }

        return firstp;
    }
</p>


### Concise O(n) solution by using C++ with Detailed Alogrithm Description
- Author: ngcl
- Creation Date: Wed Nov 19 2014 23:38:27 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 03:08:15 GMT+0800 (Singapore Standard Time)

<p>
**Alogrithm Description:**
================================================================
**Step 1: Determine whether there is a cycle**

1.1) Using a slow pointer that move forward 1 step  each time

1.2) Using a fast  pointer that move forward 2 steps each time

1.3) If the slow pointer and fast pointer both point to the same location after several moving steps, there is a cycle;

1.4) Otherwise, if (fast->next == NULL || fast->next->next == NULL), there has no cycle.

**Step 2: If there is a cycle, return the entry location of the cycle**

2.1) L1 is defined as the distance between the head point and entry point

2.2) L2 is defined as the distance between the entry point and the meeting point

2.3) C   is defined as the length of the cycle

2.4) n   is defined as the travel times of the fast pointer around the cycle When the first encounter of the slow pointer and the fast pointer

**According to the definition of L1, L2 and C, we can obtain:**

 - the total distance of the slow pointer traveled when encounter is L1 + L2

 - the total distance of the fast  pointer traveled when encounter is L1 + L2 + n * C

 - Because the total distance the fast pointer traveled is twice as the slow pointer, Thus:

 - 2 * (L1+L2) = L1 + L2 + n * C => L1 + L2 = n * C => **L1 = (n - 1)* C + (C - L2)**

**It can be concluded that the distance between the head location and entry location is equal to the distance between the meeting location and the entry location along the direction of forward movement.**

So, when the slow pointer and the fast pointer encounter in the cycle, we can define a pointer "entry" that point to the head, this "entry" pointer moves one step each time so as the slow pointer. When this "entry" pointer and the slow pointer both point to the same location, this location is the node where the cycle begins.

================================================================

Here is the code:

    ListNode *detectCycle(ListNode *head) {
        if (head == NULL || head->next == NULL)
            return NULL;
        
        ListNode *slow  = head;
        ListNode *fast  = head;
        ListNode *entry = head;
        
        while (fast->next && fast->next->next) {
            slow = slow->next;
            fast = fast->next->next;
            if (slow == fast) {                      // there is a cycle
                while(slow != entry) {               // found the entry location
                    slow  = slow->next;
                    entry = entry->next;
                }
                return entry;
            }
        }
        return NULL;                                 // there has no cycle
    }
</p>


### Java O(1) space solution with detailed explanation.
- Author: qgambit2
- Creation Date: Tue Jul 21 2015 22:14:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 12:04:48 GMT+0800 (Singapore Standard Time)

<p>
Define two pointers slow and fast. Both start at head node, fast is twice as fast as slow. If it reaches the end it means there is no cycle, otherwise eventually it will eventually catch up to slow pointer somewhere in the cycle.

Let the distance from the first node to the the node where cycle begins be A, and let say the slow pointer travels travels A+B.  The fast pointer must travel 2A+2B to catch up.  The cycle size is N.  Full cycle is also how much more fast pointer has traveled than slow pointer at meeting point.

    A+B+N = 2A+2B
    N=A+B

From our calculation slow pointer traveled exactly full cycle when it meets fast pointer, and since originally it travled A before starting on a cycle, it must travel A to reach the point where cycle begins! We can start another slow pointer at head node, and move both pointers until they meet at the beginning of a cycle.

    public class Solution {
                public ListNode detectCycle(ListNode head) {
                    ListNode slow = head;
                    ListNode fast = head;
            
                    while (fast!=null && fast.next!=null){
                        fast = fast.next.next;
                        slow = slow.next;
                        
                        if (fast == slow){
                            ListNode slow2 = head; 
                            while (slow2 != slow){
                                slow = slow.next;
                                slow2 = slow2.next;
                            }
                            return slow;
                        }
                    }
                    return null;
                }
            }
</p>


