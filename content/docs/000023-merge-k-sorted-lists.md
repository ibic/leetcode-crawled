---
title: "Merge k Sorted Lists"
weight: 23
#id: "merge-k-sorted-lists"
---
## Description
<div class="description">
<p>You are given an array of <code>k</code> linked-lists <code>lists</code>, each linked-list is sorted in ascending order.</p>

<p><em>Merge all the linked-lists into one sorted linked-list and return it.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> lists = [[1,4,5],[1,3,4],[2,6]]
<strong>Output:</strong> [1,1,2,3,4,4,5,6]
<strong>Explanation:</strong> The linked-lists are:
[
  1-&gt;4-&gt;5,
  1-&gt;3-&gt;4,
  2-&gt;6
]
merging them into one sorted list:
1-&gt;1-&gt;2-&gt;3-&gt;4-&gt;4-&gt;5-&gt;6
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> lists = []
<strong>Output:</strong> []
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> lists = [[]]
<strong>Output:</strong> []
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>k == lists.length</code></li>
	<li><code>0 &lt;= k &lt;= 10^4</code></li>
	<li><code>0 &lt;= lists[i].length &lt;= 500</code></li>
	<li><code>-10^4 &lt;= lists[i][j] &lt;= 10^4</code></li>
	<li><code>lists[i]</code> is sorted in <strong>ascending order</strong>.</li>
	<li>The sum of <code>lists[i].length</code> won&#39;t exceed <code>10^4</code>.</li>
</ul>

</div>

## Tags
- Linked List (linked-list)
- Divide and Conquer (divide-and-conquer)
- Heap (heap)

## Companies
- Amazon - 54 (taggedByAdmin: true)
- Facebook - 35 (taggedByAdmin: true)
- Apple - 8 (taggedByAdmin: false)
- Microsoft - 7 (taggedByAdmin: true)
- Bloomberg - 6 (taggedByAdmin: false)
- Databricks - 5 (taggedByAdmin: false)
- ByteDance - 4 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Lyft - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Uber - 7 (taggedByAdmin: true)
- IXL - 4 (taggedByAdmin: true)
- Walmart Labs - 4 (taggedByAdmin: false)
- Atlassian - 3 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: true)
- Cohesity - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: true)
- Wish - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)
- Indeed - 4 (taggedByAdmin: false)
- Salesforce - 4 (taggedByAdmin: false)
- SAP - 3 (taggedByAdmin: false)
- Cruise Automation - 3 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Zillow - 2 (taggedByAdmin: false)
- Box - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Tableau - 2 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)
- Audible - 2 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
[TOC]
## Solution

---
#### Approach 1: Brute Force

**Intuition & Algorithm**

- Traverse all the linked lists and collect the values of the nodes into an array.
- Sort and iterate over this array to get the proper value of nodes.
- Create a new sorted linked list and extend it with the new nodes.

As for sorting, you can refer [here](https://www.cs.cmu.edu/~adamchik/15-121/lectures/Sorting%20Algorithms/sorting.html) for more about sorting algorithms.

<iframe src="https://leetcode.com/playground/SxUGUJZR/shared" frameBorder="0" width="100%" height="327" name="SxUGUJZR"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N\log N)$$ where $$N$$ is the total number of nodes.
    - Collecting all the values costs $$O(N)$$ time.
    - A stable sorting algorithm costs $$O(N\log N)$$ time.
    - Iterating for creating the linked list costs $$O(N)$$ time.


* Space complexity : $$O(N)$$.
    - Sorting cost $$O(N)$$ space (depends on the algorithm you choose).
    - Creating a new linked list costs $$O(N)$$ space.
<br />
<br />
---

#### Approach 2: Compare one by one

**Algorithm**

- Compare every $$\text{k}$$ nodes (head of every linked list) and get the node with the smallest value.
- Extend the final sorted linked list with the selected nodes.

!?!../Documents/23_Merge_lists.json:1000,563!?!

**Complexity Analysis**

* Time complexity : $$O(kN)$$ where $$\text{k}$$ is the number of linked lists.
    - Almost every selection of node in final linked costs $$O(k)$$ ($$\text{k-1}$$ times comparison).
    - There are $$N$$ nodes in the final linked list.


* Space complexity :
    - $$O(n)$$ Creating a new linked list costs $$O(n)$$ space.
    - $$O(1)$$ It's not hard to apply in-place method - connect selected nodes instead of creating new nodes to fill the new linked list.
<br />
<br />
---
#### Approach 3: Optimize Approach 2 by Priority Queue

**Algorithm**

Almost the same as the one above but optimize the **comparison process** by **priority queue**. You can refer [here](https://en.wikipedia.org/wiki/Priority_queue) for more information about it.

<iframe src="https://leetcode.com/playground/h2rJbTJz/shared" frameBorder="0" width="100%" height="412" name="h2rJbTJz"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N\log k)$$ where $$\text{k}$$ is the number of linked lists.
    - The comparison cost will be reduced to $$O(\log k)$$ for every pop and insertion to priority queue. But finding the node with the smallest value just costs $$O(1)$$ time.
    - There are $$N$$ nodes in the final linked list.


* Space complexity :
    - $$O(n)$$ Creating a new linked list costs $$O(n)$$ space.
    - $$O(k)$$ The code above present applies in-place method which cost $$O(1)$$ space. And the priority queue (often implemented with heaps) costs $$O(k)$$ space (it's far less than $$N$$ in most situations).
<br />
<br />
---

#### Approach 4: Merge lists one by one

**Algorithm**

Convert merge $$\text{k}$$ lists problem to merge 2 lists ($$\text{k-1}$$) times. Here is the [merge 2 lists](https://leetcode.com/problems/merge-two-sorted-lists/description/) problem page.


**Complexity Analysis**

* Time complexity : $$O(kN)$$ where $$\text{k}$$ is the number of linked lists.
    - We can merge two sorted linked list in $$O(n)$$ time where $$n$$ is the total number of nodes in two lists.
    - Sum up the merge process and we can get:  $$O(\sum_{i=1}^{k-1} (i*(\frac{N}{k}) + \frac{N}{k})) = O(kN)$$.


* Space complexity : $$O(1)$$
    - We can merge two sorted linked list in $$O(1)$$ space.
<br />
<br />
---

#### Approach 5: Merge with Divide And Conquer

**Intuition & Algorithm**

This approach walks alongside the one above but is improved a lot. We don't need to traverse most nodes many times repeatedly

  - Pair up $$\text{k}$$ lists and merge each pair.

  - After the first pairing, $$\text{k}$$ lists are merged into $$k/2$$ lists with average $$2N/k$$ length, then $$k/4$$, $$k/8$$ and so on.

  -  Repeat this procedure until we get the final sorted linked list.

Thus, we'll traverse almost $$N$$ nodes per pairing and merging, and repeat this procedure about $$\log_{2}{k}$$  times.

![Divide_and_Conquer](../Figures/23/23_divide_and_conquer_new.png)


<iframe src="https://leetcode.com/playground/BsrhGdCo/shared" frameBorder="0" width="100%" height="500" name="BsrhGdCo"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N\log k)$$ where $$\text{k}$$ is the number of linked lists.
    - We can merge two sorted linked list in $$O(n)$$ time where $$n$$ is the total number of nodes in two lists.
    - Sum up the merge process and we can get: $$O\big(\sum_{i=1}^{log_{2}{k}}N \big)= O(N\log k)$$


* Space complexity : $$O(1)$$
    - We can merge two sorted linked lists in $$O(1)$$ space.

## Accepted Submission (java)
```java
import java.util.*;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */

class ListNodeComparator implements Comparator<ListNode> {
    public int compare(ListNode n1, ListNode n2) {
        return n1.val - n2.val;
    }
}

public class Solution {
    public ListNode mergeKLists(ListNode[] lists) {
        PriorityQueue<ListNode> heap = new PriorityQueue<ListNode>(lists.length > 0 ? lists.length : 11, new ListNodeComparator());
        for (ListNode list : lists) {
            if (list != null) {
                heap.add(list);
            }
        }
        ListNode dummy = new ListNode(0);
        ListNode p = dummy;
        while (heap.size() > 0) {
            ListNode n = heap.poll();
            p.next = n;
            p = n;
            if (n.next != null) {
                heap.add(n.next);
            }
        }
        return dummy.next;
    }
}

```

## Top Discussions
### A java solution based on Priority Queue
- Author: reeclapple
- Creation Date: Sun Aug 17 2014 07:21:07 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 11:24:46 GMT+0800 (Singapore Standard Time)

<p>
If someone understand how priority queue works, then it would be trivial to walk through the codes. 

My question: is that possible to solve this question under the same time complexity without implementing the priority queue?


    public class Solution {
        public ListNode mergeKLists(List<ListNode> lists) {
            if (lists==null||lists.size()==0) return null;
            
            PriorityQueue<ListNode> queue= new PriorityQueue<ListNode>(lists.size(),new Comparator<ListNode>(){
                @Override
                public int compare(ListNode o1,ListNode o2){
                    if (o1.val<o2.val)
                        return -1;
                    else if (o1.val==o2.val)
                        return 0;
                    else 
                        return 1;
                }
            });
            
            ListNode dummy = new ListNode(0);
            ListNode tail=dummy;
            
            for (ListNode node:lists)
                if (node!=null)
                    queue.add(node);
                
            while (!queue.isEmpty()){
                tail.next=queue.poll();
                tail=tail.next;
                
                if (tail.next!=null)
                    queue.add(tail.next);
            }
            return dummy.next;
        }
    }
</p>


### My simple java Solution use recursion
- Author: mouqi123
- Creation Date: Sat Oct 03 2015 18:46:46 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 08:33:55 GMT+0800 (Singapore Standard Time)

<p>
    public static ListNode mergeKLists(ListNode[] lists){
        return partion(lists,0,lists.length-1);
    }

    public static ListNode partion(ListNode[] lists,int s,int e){
        if(s==e)  return lists[s];
        if(s<e){
            int q=(s+e)/2;
            ListNode l1=partion(lists,s,q);
            ListNode l2=partion(lists,q+1,e);
            return merge(l1,l2);
        }else
            return null;
    }

    //This function is from Merge Two Sorted Lists.
    public static ListNode merge(ListNode l1,ListNode l2){
        if(l1==null) return l2;
        if(l2==null) return l1;
        if(l1.val<l2.val){
            l1.next=merge(l1.next,l2);
            return l1;
        }else{
            l2.next=merge(l1,l2.next);
            return l2;
        }
    }
</p>


### Sharing my straightforward C++ solution without data structure other than vector
- Author: zxyperfect
- Creation Date: Wed Jan 07 2015 06:04:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 23:49:22 GMT+0800 (Singapore Standard Time)

<p>
    ListNode *mergeKLists(vector<ListNode *> &lists) {
        if(lists.empty()){
            return nullptr;
        }
        while(lists.size() > 1){
            lists.push_back(mergeTwoLists(lists[0], lists[1]));
            lists.erase(lists.begin());
            lists.erase(lists.begin());
        }
        return lists.front();
    }
    ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
        if(l1 == nullptr){
            return l2;
        }
        if(l2 == nullptr){
            return l1;
        }
        if(l1->val <= l2->val){
            l1->next = mergeTwoLists(l1->next, l2);
            return l1;
        }
        else{
            l2->next = mergeTwoLists(l1, l2->next);
            return l2;
        }
    }

The second function is from Merge Two Sorted Lists. 

The basic idea is really simple. We can merge first two lists and then push it back. Keep doing this until there is only one list left in vector. Actually, we can regard this as an iterative divide-and-conquer solution.
</p>


