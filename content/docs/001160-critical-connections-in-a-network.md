---
title: "Critical Connections in a Network"
weight: 1160
#id: "critical-connections-in-a-network"
---
## Description
<div class="description">
<p>There are&nbsp;<code>n</code> servers numbered from&nbsp;<code>0</code>&nbsp;to&nbsp;<code>n-1</code> connected by&nbsp;undirected server-to-server <code>connections</code> forming a network where <code>connections[i] = [a, b]</code>&nbsp;represents a connection between servers <code>a</code>&nbsp;and <code>b</code>. Any server can reach any other server directly or indirectly through the network.</p>

<p>A <em>critical connection</em>&nbsp;is a connection that, if removed, will make some server unable to reach some other server.</p>

<p>Return all critical connections in the network in any order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/09/03/1537_ex1_2.png" style="width: 198px; height: 248px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 4, connections = [[0,1],[1,2],[2,0],[1,3]]
<strong>Output:</strong> [[1,3]]
<strong>Explanation:</strong> [[3,1]] is also accepted.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
	<li><code>n-1 &lt;= connections.length &lt;= 10^5</code></li>
	<li><code>connections[i][0] != connections[i][1]</code></li>
	<li>There are no repeated connections.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 85 (taggedByAdmin: true)
- Google - 4 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### DFS detailed explanation, O(|E|) solution
- Author: kaiwensun
- Creation Date: Sun Sep 15 2019 16:36:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 08:27:57 GMT+0800 (Singapore Standard Time)

<p>
## **First thought**

Thiking for a little while, you will easily find out this theorem on a connected graph:

* **An edge is a critical connection, if and only if it is not in a cycle.**

So, if we know how to find cycles, and discard all edges in the cycles, then the remaining connections are a complete collection of critical connections.

----

## **How to find eges in cycles, and remove them**

We will use DFS algorithm to find cycles and decide whether or not an edge is in a cycle.

Define **rank** of a node: The depth of a node during a DFS. The starting node has a *rank* 0.

Only the nodes on the current DFS path have non-special *ranks*. In other words, only the nodes that we\'ve started visiting, but haven\'t finished visiting, have *ranks*. So `0 <= rank < n`.

(For coding purpose, if a node is not visited yet, it has a special rank `-2`; if we\'ve fully completed the visit of a node, it has a special rank `n`.)

**How can "rank" help us with removing cycles?** Imagine you have a current path of length `k` during a DFS. The nodes on the path has increasing ranks from `0` to `k`and incrementing by `1`. Surprisingly, your next visit finds a node that has a rank of `p` where `0 <= p < k`. Why does it happen? Aha! You found a node that is on the current search path! That means, congratulations, you found a cycle!

But only the current level of search knows it finds a cycle. How does the upper level of search knows, if you backtrack? Let\'s make use of the return value of DFS: **`dfs` function returns the minimum rank it finds.** During a step of search from node `u` to its neighbor `v`, **if `dfs(v)` returns something smaller than or equal to `rank(u)`**, then `u` knows its neighbor `v` helped it to find a cycle back to `u` or `u`\'s ancestor. So `u` knows it should discard the edge `(u, v)` which is in a cycle.

After doing dfs on all nodes, all edges in cycles are discarded. So the remaining edges are critical connections.

----

## **Python code**

```
import collections
class Solution(object):
    def criticalConnections(self, n, connections):
        def makeGraph(connections):
            graph = collections.defaultdict(list)
            for conn in connections:
                graph[conn[0]].append(conn[1])
                graph[conn[1]].append(conn[0])
            return graph

        graph = makeGraph(connections)
        connections = set(map(tuple, (map(sorted, connections))))
        rank = [-2] * n

        def dfs(node, depth):
            if rank[node] >= 0:
                # visiting (0<=rank<n), or visited (rank=n)
                return rank[node]
            rank[node] = depth
            min_back_depth = n
            for neighbor in graph[node]:
                if rank[neighbor] == depth - 1:
                    continue  # don\'t immmediately go back to parent. that\'s why i didn\'t choose -1 as the special value, in case depth==0.
                back_depth = dfs(neighbor, depth + 1)
                if back_depth <= depth:
                    connections.discard(tuple(sorted((node, neighbor))))
                min_back_depth = min(min_back_depth, back_depth)
            rank[node] = n  # this line is not necessary. see the "brain teaser" section below
            return min_back_depth
            
        dfs(0, 0)  # since this is a connected graph, we don\'t have to loop over all nodes.
        return list(connections)
```

----

## **Complexity analysis**
DFS time complexity is O(|E| + |V|), attempting to visit each edge at most twice. (the second attempt will immediately return.)
As the graph is always a connected graph, |E| >= |V|.

So, time complexity = O(|E|).

Space complexity = O(graph) + O(rank) + O(connections) = 3 * O(|E| + |V|) = O(|E|).

----

## **FAQ: Are you reinventing Tarjan?**
Honestly, I didn\'t know Tarjan beforehand. The idea of using `rank` is inspired by [preordering](https://en.wikipedia.org/wiki/Depth-first_search#Vertex_orderings) which is a basic concept of DFS. Now I realize they are similar, but there are still major differences between them.

* This solution uses only one array `rank`. While Tarjan uses two arrays: `dfn` and `low`.
* This solution\'s `min_back_depth` is similar to Tarjan\'s `low`, but `rank` is very different than `dfn`. `max(dfn)` is always n-1, while `max(rank)` could be smaller than n-1.
* This solution construsts the result by removing non-critical edges **during** the dfs, while Tarjan constructs the result by collecting non-critical edges **after** the dfs.
* In this solution, only nodes actively in the current search path have `0<=rank[node]<n`; while in Tarjan, nodes not actively in the current search path may still have `0<=dfn[node]<=low[node]<n`.

----

## **Brain teaser**

Thanks [@migfulcrum](https://leetcode.com/migfulcrum) for [pointing out](https://leetcode.com/discuss/comment/359567) that `rank[node] = n` is not necessary. He is totally right. I\'ll leave this as a brain teaser for you: why is it not necessary?
(Hint: after we\'ve finished visiting a node, is it possible to have another search path attempting to visit this node again?)

----

(If you like this post, don\'t forget to vote up so more people will read it!)
</p>


### Python (98% Time, 100% Memory) clean solution with explanaions for confused people like me
- Author: soccer_player
- Creation Date: Tue Oct 22 2019 01:44:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 08 2019 15:46:30 GMT+0800 (Singapore Standard Time)

<p>
This is 1st time I ever post any codes here because I felt the explanations in other posts are really confusing to me (probably because I am not smart enough...) So I posted my codes with explanations (after long time thinking) and hopefully to help those who also feel confused when they first got into this problem. Hope they could get the idea quickly (no need to spend as much as time as I did)....

The basic idea is, we do DFS through the graph and marked the order (or call it rank, or timestamp) it is in this DFS process. Bascially this records how far this vertex(or we say node, or server) away from the first vertex since you started your DFS.

While going along this DFS, you also need to mark the lowester order (or rank) of the node that can reach this node (except from its previous node). If there is any other connection to this node other than its previous node (the one that help you enter DFS of this level), then you know you have more than 1 connection to this vertex, then it is NOT a critical connection. A critical connection that connects two nodes, but these two nodes NOT necessarily have only one neighbor. They may have more other neighbors. The very special things about these two nodes are, their following nodes (except its parent/previous node) in DFS always come after itself. ("following nodes" means nodes that have higher rank).

Another confusion is, the num on the vertex (like a name for the vertex/server) is not necessarily same as the order/rank/timestamp, although they are all integers from 0~N-1. So in the following, if it is a rank, I always use parameter names like xxxRank, if it is a num (like a name) on the vertex, I use names like xxxVertex.

Probably still little confusing.... maybe look at codes can make you feel better.


```
class Solution:
    def criticalConnections(self, n: int, connections: List[List[int]]) -> List[List[int]]:

		graph = [[] for _ in range(n)] ## vertex i ==> [its neighbors]
		
        currentRank = 0 ## please note this rank is NOT the num (name) of the vertex itself, it is the order of your DFS level
		
        lowestRank = [i for i in range(n)] ## here lowestRank[i] represents the lowest order of vertex that can reach this vertex i
		
        visited = [False for _ in range(n)] ## common DFS/BFS method to mark whether this node is seen before
        
        ## build graph:
        for connection in connections:
            ## this step is straightforward, build graph as you would normally do
            graph[connection[0]].append(connection[1])
            graph[connection[1]].append(connection[0])
        
        res = []
        prevVertex = -1 ## This -1 a dummy. Does not really matter in the beginning. 
		## It will be used in the following DFS because we need to know where the current DFS level comes from. 
		## You do not need to setup this parameter, I setup here ONLY because it is more clear to see what are passed on in the DFS method.
		
        currentVertex = 0 ## we start the DFS from vertex num 0 (its rank is also 0 of course)
        self._dfs(res, graph, lowestRank, visited, currentRank, prevVertex, currentVertex)
        return res
    
    def _dfs(self, res, graph, lowestRank, visited, currentRank, prevVertex, currentVertex):

        visited[currentVertex] = True 
        lowestRank[currentVertex] = currentRank

        for nextVertex in graph[currentVertex]:
            if nextVertex == prevVertex:
                continue ## do not include the the incoming path to this vertex since this is the possible ONLY bridge (critical connection) that every vertex needs.

            if not visited[nextVertex]:
                self._dfs(res, graph, lowestRank, visited, currentRank + 1, currentVertex, nextVertex)
				# We avoid visiting visited nodes here instead of doing it at the beginning of DFS - 
				# the reason is, even that nextVertex may be visited before, we still need to update my lowestRank using the visited vertex\'s information.

            lowestRank[currentVertex] = min(lowestRank[currentVertex], lowestRank[nextVertex]) 
			# take the min of the current vertex\'s and next vertex\'s ranking
            if lowestRank[nextVertex] >= currentRank + 1: ####### if all the neighbors lowest rank is higher than mine + 1, then it means I am one connecting critical connection ###
                res.append([currentVertex, nextVertex])
```
</p>


### Clean Java Solution With Explanation!!! Great Question!
- Author: om_namah_shivay
- Creation Date: Thu Oct 10 2019 06:21:54 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 26 2019 04:40:43 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    // We record the timestamp that we visit each node. For each node, we check every neighbor except its parent and return a smallest timestamp in all its neighbors. If this timestamp is strictly less than the node\'s timestamp, we know that this node is somehow in a cycle. Otherwise, this edge from the parent to this node is a critical connection
    public List<List<Integer>> criticalConnections(int n, List<List<Integer>> connections) {
        List<Integer>[] graph = new ArrayList[n];
        for (int i = 0; i < n; i++) graph[i] = new ArrayList<>();
        
        for(List<Integer> oneConnection :connections) {
            graph[oneConnection.get(0)].add(oneConnection.get(1));
            graph[oneConnection.get(1)].add(oneConnection.get(0));
        }
        int timer[] = new int[1];
        List<List<Integer>> results = new ArrayList<>();
        boolean[] visited = new boolean[n];
        int []timeStampAtThatNode = new int[n]; 
        criticalConnectionsUtil(graph, -1, 0, timer, visited, results, timeStampAtThatNode);
        return results;
    }
    
    
    public void criticalConnectionsUtil(List<Integer>[] graph, int parent, int node, int timer[], boolean[] visited, List<List<Integer>> results, int []timeStampAtThatNode) {
        visited[node] = true;
        timeStampAtThatNode[node] = timer[0]++;
        int currentTimeStamp = timeStampAtThatNode[node];
        
        for(int oneNeighbour : graph[node]) {
            if(oneNeighbour == parent) continue;
            if(!visited[oneNeighbour]) criticalConnectionsUtil(graph, node, oneNeighbour, timer, visited, results, timeStampAtThatNode);
            timeStampAtThatNode[node] = Math.min(timeStampAtThatNode[node], timeStampAtThatNode[oneNeighbour]);
            if(currentTimeStamp < timeStampAtThatNode[oneNeighbour]) results.add(Arrays.asList(node, oneNeighbour));
        }
        
        
    }
    
}
```
Listed below are all Questions asked In Concise Manner:
1. Why do we write a special case for parent?before iterating every child ,visited[parent] becomes true.
Ans: he reason for writing a special condition for checking parent is that, I don\'t want the timing of my currently node\'s parent to update my current node\'s timing which will always be less than my current node which would ultimately lead to result being populated with all the connections in the graph. Let me know if you understand the explanation otherwise I can explain you the same with an example. 

2.  We don\'t need "visited[]". If timestampAtThatNode[I] == 0, then it is not visited, given timer[] start with 1.
Ans: Yes, that\'s a great idea we can go ahead with that as well!
Will save memory of visited array.

3. why the timer is an array with one value in it? what if we use a integer to store it ?
Ans: Yes, you can also use one int value for the timer but generally in these types of graph problems there are a couple of timers some time and it doesnt make sense have different fields every time but in this problem it doesn\'t matter. You can use anything!

4. How would you recommend that we maintain the order of the input nodes? (For the case: Input: n = 4, connections = [[0,1],[1,2],[2,0],[1,3]] Output: [[1,3]]
[[3,1]] is also accepted. How could we preserve the input order, i.e. only return [1,3] and not [3,1])
Ans: First of all, the entire graph is undirected which means that the order does not matter!
But even then if you need to preserve the order the easiest way is to traverse the input connections for every result connection to check if it is in the same order which is there in the input if not we reverse the order. Its complexity will be order of (input connections, result connection)
Easier way will be to add all the input connections to hashset. While adding a connection to result we just check whether that connection is in the set. If it is we add it as it is, if not we will reverse it and add it. This will be order of number of connections in the result.

5. why if(currentTimeStamp < timeStampAtThatNode[oneNeighbour]) other than if(currentTimeStamp > timeStampAtThatNode[oneNeighbour]) ?
currentTimeStamp means the timestamp of current node, if the timestamp of its neighbor is less than its own timestamp, then there\'s a critical connection. Am I understanding correct ?
Ans : A critical connection (A-B) where A is the current node and B is the neighbour of A in the DFS traversal is given by a connection when A is visited before its neighbour(B). Which means that only way to go to B is through A. Now, if B is visited before A through some other node\'s DFS traversal and updated the visited time of B to a value lower than visited time of A it means that there is a way to reach B which will not go through A. So edge A-B is not a critical connection because even after removing edge A-B , B will not be isolated.


</p>


