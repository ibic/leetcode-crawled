---
title: "Construct Binary Tree from Inorder and Postorder Traversal"
weight: 106
#id: "construct-binary-tree-from-inorder-and-postorder-traversal"
---
## Description
<div class="description">
<p>Given inorder and postorder traversal of a tree, construct the binary tree.</p>

<p><strong>Note:</strong><br />
You may assume that duplicates do not exist in the tree.</p>

<p>For example, given</p>

<pre>
inorder =&nbsp;[9,3,15,20,7]
postorder = [9,15,7,20,3]</pre>

<p>Return the following binary tree:</p>

<pre>
    3
   / \
  9  20
    /  \
   15   7
</pre>

</div>

## Tags
- Array (array)
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 6 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)
- Facebook - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### How to traverse the tree

There are two general strategies to traverse a tree:
     
- *Depth First Search* (`DFS`)

    In this strategy, we adopt the `depth` as the priority, so that one
    would start from a root and reach all the way down to certain leaf,
    and then back to root to reach another branch.

    The DFS strategy can further be distinguished as
    `preorder`, `inorder`, and `postorder` depending on the relative order
    among the root node, left node and right node.
    
- *Breadth First Search* (`BFS`)

    We scan through the tree level by level, following the order of height,
    from top to bottom. The nodes on higher level would be visited before
    the ones with lower levels.
    
On the following figure the nodes are enumerated in the order you visit them,
please follow `1-2-3-4-5` to compare different strategies.

![postorder](../Figures/106/bfs_dfs.png)

> In this problem one deals with inorder and postorder traversals.

<br /> 
<br />


---
#### Approach 1: Recursion

**How to construct the tree from two traversals: inorder and preorder/postorder/etc**

Problems like this one are often at Facebook interviews, and could be solved in
$$\mathcal{O}(N)$$ time:

- Start from not inorder traversal, usually it's preorder or postorder one, and 
use the traversal picture above to define the strategy to pick the nodes. 
For example, for preorder traversal the _first_ value is a root, then
its left child, then its right child, etc. 
For postorder traversal the _last_ value is a root, then its right child,
then its left child, etc. 

- The value picked from preorder/postorder traversal splits the inorder 
traversal into left and right subtrees. The only information one needs from
inorder - if the current subtree is empty (= return `None`) or not 
(= continue to construct the subtree). 

![bla](../Figures/106/recursion.png)

**Algorithm**

- Build hashmap `value -> its index` for inorder traversal.

- Return `helper` function which takes as the arguments 
the left and right boundaries for the current subtree in the inorder traversal.
These boundaries are used only to check if the subtree is empty or not.
Here is how it works `helper(in_left = 0, in_right = n - 1)`:

    - If `in_left > in_right`, the subtree is empty, return `None`.
    
    - Pick the last element in postorder traversal as a root.
    
    - Root value has index `index` in the inorder traversal,
    elements from `in_left` to `index - 1` belong to the left subtree,
    and elements from `index + 1` to `in_right` belong to the right subtree.
    
    - Following the postorder logic, proceed recursively first to construct the
    right subtree `helper(index + 1, in_right)` and then to construct 
    the left subtree `helper(in_left, index - 1)`.
    
    - Return `root`.

**Implementation**

!?!../Documents/106_LIS.json:1000,560!?!

<iframe src="https://leetcode.com/playground/PikW3feZ/shared" frameBorder="0" width="100%" height="500" name="PikW3feZ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$. Let's compute the solution with the help of 
[master theorem](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)) 
$$T(N) = aT\left(\frac{b}{N}\right) + \Theta(N^d)$$.
The equation represents dividing the problem 
up into $$a$$ subproblems of size $$\frac{N}{b}$$ in $$\Theta(N^d)$$ time. 
Here one divides the problem in two subproblemes `a = 2`, the size of each subproblem 
(to compute left and right subtree) is a half of initial problem `b = 2`, 
and all this happens in a constant time `d = 0`.
That means that $$\log_b(a) > d$$ and hence we're dealing with 
[case 1](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)#Case_1_example)
that means $$\mathcal{O}(N^{\log_b(a)}) = \mathcal{O}(N)$$ time complexity.

* Space complexity : $$\mathcal{O}(N)$$, since we store the entire tree.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My recursive Java code with O(n) time and O(n) space
- Author: lurklurk
- Creation Date: Sat Sep 13 2014 08:04:21 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 22:16:18 GMT+0800 (Singapore Standard Time)

<p>
The the basic idea is to take the last element in postorder array as the root, find the position of the root in the inorder array; then locate the range for left sub-tree and right sub-tree and do recursion. Use a HashMap to record the index of root in the inorder array.

    public TreeNode buildTreePostIn(int[] inorder, int[] postorder) {
    	if (inorder == null || postorder == null || inorder.length != postorder.length)
    		return null;
    	HashMap<Integer, Integer> hm = new HashMap<Integer,Integer>();
    	for (int i=0;i<inorder.length;++i)
    		hm.put(inorder[i], i);
    	return buildTreePostIn(inorder, 0, inorder.length-1, postorder, 0, 
                              postorder.length-1,hm);
    }
    
    private TreeNode buildTreePostIn(int[] inorder, int is, int ie, int[] postorder, int ps, int pe, 
                                     HashMap<Integer,Integer> hm){
    	if (ps>pe || is>ie) return null;
    	TreeNode root = new TreeNode(postorder[pe]);
    	int ri = hm.get(postorder[pe]);
    	TreeNode leftchild = buildTreePostIn(inorder, is, ri-1, postorder, ps, ps+ri-is-1, hm);
    	TreeNode rightchild = buildTreePostIn(inorder,ri+1, ie, postorder, ps+ri-is, pe-1, hm);
    	root.left = leftchild;
    	root.right = rightchild;
    	return root;
    }
</p>


### Don't use top voted Python solution for interview, here is why.
- Author: yuxiong
- Creation Date: Sun Jan 20 2019 02:22:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 20 2019 02:22:41 GMT+0800 (Singapore Standard Time)

<p>
Here I copied the top voted Python solution:
```
class Solution:
    def buildTree(self, inorder, postorder):
        if not inorder or not postorder:
            return None
        
        root = TreeNode(postorder.pop())
        inorderIndex = inorder.index(root.val) # Line A

        root.right = self.buildTree(inorder[inorderIndex+1:], postorder) # Line B
        root.left = self.buildTree(inorder[:inorderIndex], postorder) # Line C

        return root
```

The code is clean and short. However, if you give this implementation during an interview, there is a good chance you will be asked, "can you improve/optimize your solution?"

Why? Take a look at Line A, Line B and Line C.
Line A takes O(N) time.
Line B and C takes O(N) time and extra space.
Thus, the overall running time and extra space is O(N^2).
So this implementation has a very bad performance, and you can avoid it.

Here is my solution which has O(N) time and extra space.
```
class Solution:
    def buildTree(self, inorder, postorder):
        map_inorder = {}
        for i, val in enumerate(inorder): map_inorder[val] = i
        def recur(low, high):
            if low > high: return None
            x = TreeNode(postorder.pop())
            mid = map_inorder[x.val]
            x.right = recur(mid+1, high)
            x.left = recur(low, mid-1)
            return x
        return recur(0, len(inorder)-1)
```
</p>


### Sharing my straightforward recursive solution
- Author: zxyperfect
- Creation Date: Tue Dec 09 2014 13:58:57 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 14:51:12 GMT+0800 (Singapore Standard Time)

<p>
    TreeNode *buildTree(vector<int> &inorder, vector<int> &postorder) {
        return create(inorder, postorder, 0, inorder.size() - 1, 0, postorder.size() - 1);
    }
    
    TreeNode* create(vector<int> &inorder, vector<int> &postorder, int is, int ie, int ps, int pe){
        if(ps > pe){
            return nullptr;
        }
        TreeNode* node = new TreeNode(postorder[pe]);
        int pos;
        for(int i = is; i <= ie; i++){
            if(inorder[i] == node->val){
                pos = i;
                break;
            }
        }
        node->left = create(inorder, postorder, is, pos - 1, ps, ps + pos - is - 1);
        node->right = create(inorder, postorder, pos + 1, ie, pe - ie + pos, pe - 1);
        return node;
    }

Actually, this problem is pretty similar as the previous one. 

[Here is a like to that solution. ][1]


  [1]: https://oj.leetcode.com/discuss/18101/sharing-my-straightforward-recursive-solution
</p>


