---
title: "Plus One"
weight: 66
#id: "plus-one"
---
## Description
<div class="description">
<p>Given a <strong>non-empty</strong> array of digits&nbsp;representing a non-negative integer, increment&nbsp;one to the integer.</p>

<p>The digits are stored such that the most significant digit is at the head of the list, and each element in the array contains a single digit.</p>

<p>You may assume the integer does not contain any leading zero, except the number 0 itself.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> digits = [1,2,3]
<strong>Output:</strong> [1,2,4]
<strong>Explanation:</strong> The array represents the integer 123.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> digits = [4,3,2,1]
<strong>Output:</strong> [4,3,2,2]
<strong>Explanation:</strong> The array represents the integer 4321.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> digits = [0]
<strong>Output:</strong> [1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= digits.length &lt;= 100</code></li>
	<li><code>0 &lt;= digits[i] &lt;= 9</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Google - 9 (taggedByAdmin: true)
- Amazon - 5 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Capital One - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Spotify - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Overview

"Plus One" is a subset of the problem set "Add Number",
which shares the same solution pattern.

All these problems could be solved in linear time, 
and the question here is how to solve it without using the addition operation or 
how to solve it in constant space complexity.

The choice of algorithm should be based on the format of input.
Here we list a few examples:

1. Integers

    Usually the addition operation is not allowed for such a case.
Use Bit Manipulation Approach. 
Here is an example: [Add Binary](https://leetcode.com/articles/add-binary/).

2. Strings 

    Use bit by bit computation. 
Note, sometimes it might not be feasible to come up a solution with the constant space
for languages with immutable strings, _e.g._ for Java and Python.
Here is an example: [Add Binary](https://leetcode.com/articles/add-binary/).

3. Linked Lists 

    Sentinel Head + Schoolbook Addition with Carry. 
Here is an example: [Plus One Linked List](https://leetcode.com/articles/plus-one-linked-list/).

4. Arrays (also the current problem)
    
    Schoolbook addition with carry.

> Note that, a straightforward idea to convert everything into 
integers and then apply the addition could be risky,
especially for the implementation in Java,
due to the potential integer overflow issue.

As one can imagine, once the array gets long, the result of conversion 
cannot fit into the data type of Integer, or Long, or even 
[BigInteger](https://docs.oracle.com/javase/8/docs/api/java/math/BigInteger.html).

<br /> 
<br />


---
#### Approach 1: Schoolbook Addition with Carry

**Intuition**

Let us identify the rightmost digit which is not equal to nine and 
increase that digit by one.
All the following consecutive digits of nine should be set to zero.

Here is the simplest use case which works fine.

![simple](../Figures/66/simple2.png)

Here is a slightly complicated case which still passes.

![more](../Figures/66/more.png)

And here is the case which breaks everything, because _all_ the digits are nines.

![handle](../Figures/66/handle.png)

In this case, we need to set all nines to zero and append 1 to the left side
of the array.

![append](../Figures/66/append.png)

**Algorithm**

- Move along the input array starting from the end of array.

- Set all the nines at the end of array to zero.

- If we meet a not-nine digit, we would increase it by one. The job is done - 
return `digits`.

- We're here because **_all_** the digits were equal to nine.
Now they have all been set to zero.
We then append the digit `1` in front of the other digits and return the result. 

**Implementation**

!?!../Documents/66_LIS.json:1000,212!?!

<iframe src="https://leetcode.com/playground/drn3a2N7/shared" frameBorder="0" width="100%" height="463" name="drn3a2N7"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since it's not more than 
one pass along the input list.
 
* Space complexity : $$\mathcal{O}(1)$$ when `digits` contains at least one
not-nine digit, and $$\mathcal{O}(N)$$ otherwise.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My Simple Java Solution
- Author: diaa
- Creation Date: Mon Sep 14 2015 23:55:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 11:34:21 GMT+0800 (Singapore Standard Time)

<p>
    public int[] plusOne(int[] digits) {
            
        int n = digits.length;
        for(int i=n-1; i>=0; i--) {
            if(digits[i] < 9) {
                digits[i]++;
                return digits;
            }
            
            digits[i] = 0;
        }
        
        int[] newNumber = new int [n+1];
        newNumber[0] = 1;
        
        return newNumber;
    }
</p>


### Is it a simple code(C++)?
- Author: zezedi
- Creation Date: Wed Oct 29 2014 19:19:26 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 03:32:59 GMT+0800 (Singapore Standard Time)

<p>
    void plusone(vector<int> &digits)
    {
    	int n = digits.size();
    	for (int i = n - 1; i >= 0; --i)
    	{
    		if (digits[i] == 9)
    		{
    			digits[i] = 0;
    		}
    		else
    		{
    			digits[i]++;
    			return;
    		}
    	}
    		digits[0] =1;
    		digits.push_back(0);
    		
    }
</p>


### I cannot fully understand the meaning of question 'Plus One'
- Author: mooc
- Creation Date: Sat Jan 03 2015 23:54:43 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 25 2018 04:51:44 GMT+0800 (Singapore Standard Time)

<p>
Could someone explain this to me? And please don't show up the code. Thanks
</p>


