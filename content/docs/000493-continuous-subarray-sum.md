---
title: "Continuous Subarray Sum"
weight: 493
#id: "continuous-subarray-sum"
---
## Description
<div class="description">
<p>Given a list of <b>non-negative</b> numbers and a target <b>integer</b> k, write a function to check if the array has a continuous subarray of size at least 2 that sums up to a multiple of <b>k</b>, that is, sums up to n*k where n is also an <b>integer</b>.</p>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> [23, 2, 4, 6, 7],  k=6
<b>Output:</b> True
<b>Explanation:</b> Because [2, 4] is a continuous subarray of size 2 and sums up to 6.
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> [23, 2, 6, 4, 7],  k=6
<b>Output:</b> True
<b>Explanation:</b> Because [23, 2, 6, 4, 7] is an continuous subarray of size 5 and sums up to 42.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The length of the array won&#39;t exceed 10,000.</li>
	<li>You may assume the sum of all the numbers is in the range of a signed 32-bit integer.</li>
</ul>

</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- Facebook - 32 (taggedByAdmin: true)
- Amazon - 5 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Samsung - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force 

The brute force approach is trivial. We consider every possible subarray of size greater than or equal to 2, find out its sum by iterating over the elements of the subarray, and then we check if the sum obtained is an integer multiple of the given $$k$$.

<iframe src="https://leetcode.com/playground/Fr8V6rNp/shared" frameBorder="0" width="100%" height="310" name="Fr8V6rNp"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^3)$$. Three for loops iterating over the array are used.
* Space complexity : $$O(1)$$. Constant extra space is used.

---
#### Approach 2: Better Brute Force

**Algorithm**

We can optimize the brute force approach to some extent, if we make use of an array $$sum$$ that stores the cumulative sum of the elements of the array, such that $$sum[i]$$ stores the sum of the elements upto the $$i^{th}$$ element of the array.

Thus, now as before, we consider every possible subarray for checking its sum. But, instead of iterating over a new subarray everytime to determine its sum, we make use of the cumulative sum array. Thus, to determine the sum of elements from the $$i^{th}$$ index to the $$j^{th}$$ index, including both the corners, we can use: $$sum[j] - sum[i] + nums[i]$$. 

<iframe src="https://leetcode.com/playground/BZs8HzVS/shared" frameBorder="0" width="100%" height="327" name="BZs8HzVS"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Two for loops are used for considering every subarray possible.

* Space complexity : $$O(n)$$. $$sum$$ array of size $$n$$ is used.

---

#### Approach 3: Using HashMap 

**Algorithm**

In this solution, we make use of a HashMap that is used to store the cumulative sums upto the $$i^{th}$$ index after some processing along with the index $$i$$. The processing done is taking the modulus of the the sum upto the $$i^{th}$$ index with the given $$k$$. The reasoning behind this will become clear soon. 

We traverse over the given array, and keep on calculating the $$sum%k$$ values upto the current index. Whenever we find a new $$sum%k$$ value, which isn't present in the HashMap already, we make an entry in the HashMap of the form, $$(sum%k, i)$$. 

Now, assume that the given $$sum%k$$ value at the $$i^{th}$$ index be equal to $$rem$$. Now, if any subarray follows the $$i^{th}$$ element, which has a sum equal to the integer multiple of $$k$$, say extending upto the $$j^{th}$$ index, the sum value to be stored in the HashMap for the $$j^{th}$$ index will be: $$(rem + n*k)%k$$, where $$n$$ is some integer > 0. We can observe that $$(rem + n*k)%k = rem$$, which is the same value as stored corresponding to the $$i^{th}$$ index.

From this observation, we come to the conclusion that whenever the same $$sum%k$$ value is obtained corresponding to two indices $$i$$ and $$j$$, it implies that sum of elements betweeen those indices is an integer multiple of $$k$$. Thus, if the same $$sum%k$$ value is encountered again during the traversal, we return a $$\text{True}$$ directly.

The slideshow below depicts the process for the array `nums: [2, 5, 33, 6, 7, 25, 15]` and `k=13`.

!?!../Documents/523_Continous_Subarray_Sum.json:640,360!?!


<iframe src="https://leetcode.com/playground/YchJk7Zc/shared" frameBorder="0" width="100%" height="361" name="YchJk7Zc"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Only one traversal of the array $$nums$$ is done.

* Space complexity : $$O(min(n,k))$$. The HashMap can contain upto $$min(n,k)$$ different pairings.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n) time O(k) space
- Author: compton_scatter
- Creation Date: Sun Feb 26 2017 12:00:37 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 14:39:20 GMT+0800 (Singapore Standard Time)

<p>
We iterate through the input array exactly once, keeping track of the running sum mod k of the elements in the process. If we find that a running sum value at index j has been previously seen before in some earlier index i in the array, then we know that the sub-array (i,j] contains a desired sum.

```
public boolean checkSubarraySum(int[] nums, int k) {
    Map<Integer, Integer> map = new HashMap<Integer, Integer>(){{put(0,-1);}};;
    int runningSum = 0;
    for (int i=0;i<nums.length;i++) {
        runningSum += nums[i];
        if (k != 0) runningSum %= k; 
        Integer prev = map.get(runningSum);
        if (prev != null) {
            if (i - prev > 1) return true;
        }
        else map.put(runningSum, i);
    }
    return false;
}
```
</p>


### Math behind the solutions
- Author: yuxiong
- Creation Date: Mon Jul 16 2018 12:44:46 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 04 2018 21:08:24 GMT+0800 (Singapore Standard Time)

<p>
Haven\'t seen anyone post the math or theory behind the solutions yet, so I\'m sharing mine. Let me know if there is any better one.
    In short, start with mod =0, then we always do mod = (mod+nums[i])%k, if mod repeats, that means between these two mod = x occurences the sum is multiple of k.
    Math: c = a % k, c = b % k, so we have a % k = b % k. 
		Where a is the mod at i and b is the mod at j and a <= b, i < j, because all nums are non-negative. And c is the mod that repeats.
		Suppose b-a=d, then we have b % k = ((a+d) % k)%k = (a%k + d%k)%k
    In order to make the equation valid: a % k = (a%k + d%k)%k
    d%k has to be 0, so d, the different between b and a, is a multiple of k
    Example: 
         [23, 2, 1, 6, 7] k=9
    mod = 5,  7, 8, 5 <-- at here we found it
</p>


### Need to pay attention to a lot of corner cases...
- Author: shawngao
- Creation Date: Sun Feb 26 2017 12:14:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 04:51:33 GMT+0800 (Singapore Standard Time)

<p>
This problem contributed a lot of bugs to my contest score... Let's read the description again, pay attention to ```red``` sections:

Given a list of ```non-negative``` numbers and a target integer k, write a function to check if the array has a ```continuous subarray``` of size ```at least 2``` that sums up to the ```multiple``` of k, that is, sums up to n*k where n is also an ```integer```.

Some ```damn it!``` test cases:
1. [0], 0 -> false;
2. [5, 2, 4], 5 -> false;
3. [0, 0], 100 -> true;
4. [1,5], -6 -> true;
etc...
```
public class Solution {
    public boolean checkSubarraySum(int[] nums, int k) {
        // Since the size of subarray is at least 2.
        if (nums.length <= 1) return false;
        // Two continuous "0" will form a subarray which has sum = 0. 0 * k == 0 will always be true.
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == 0 && nums[i + 1] == 0) return true;
        }

        // At this point, k can't be "0" any longer.
        if (k == 0) return false;
        // Let's only check positive k. Because if there is a n makes n * k = sum, it is always true -n * -k = sum.
        if (k < 0) k = -k;

        Map<Integer, Integer> sumToIndex = new HashMap<>();
        int sum = 0;
        sumToIndex.put(0, -1);

        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            // Validate from the biggest possible n * k to k
            for (int j = (sum / k) * k; j >= k; j -= k) {
                if (sumToIndex.containsKey(sum - j) && (i - sumToIndex.get(sum - j) > 1)) return true;
            }
            if (!sumToIndex.containsKey(sum)) sumToIndex.put(sum, i);
        }

        return false;
    }
}
```
</p>


