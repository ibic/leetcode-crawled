---
title: "Palindrome Partitioning"
weight: 131
#id: "palindrome-partitioning"
---
## Description
<div class="description">
<p>Given a string <em>s</em>, partition <em>s</em> such that every substring of the partition is a palindrome.</p>

<p>Return all possible palindrome partitioning of <em>s</em>.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong>&nbsp;&quot;aab&quot;
<strong>Output:</strong>
[
  [&quot;aa&quot;,&quot;b&quot;],
  [&quot;a&quot;,&quot;a&quot;,&quot;b&quot;]
]
</pre>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Qualtrics - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---

#### Overview ####

The aim to partition the string into all possible palindrome combinations. To achieve this, we must generate all possible substrings of a string by partitioning at every index until we reach the end of the string. Example, `abba` can be partitioned as `["a","ab","abb","abba"]`. Each generated substring is considered as a potential candidate if it a [Palindrome](https://en.wikipedia.org/wiki/Palindrome).

Let's look at a few approaches to implement this idea.

---

#### Approach 1: Backtracking

**Intuition**

The idea is to generate all possible substrings of a given string and expand each possibility if is a potential candidate. The first thing that comes to our mind is  [Depth First Search](https://en.wikipedia.org/wiki/Depth-first_search). In Depth First Search, we recursively expand potential candidate until the defined goal is achieved. After that, we backtrack to explore the next potential candidate.

[Backtracking](https://en.wikipedia.org/wiki/Backtracking)  incrementally build the candidates for the solution and discard the candidates (backtrack) if it doesn't satisfy the condition.

The backtracking algorithms consists of the following steps:

- _Choose_: Choose the potential candidate. Here, our potential candidates are all substrings that could be generated from the given string.

- _Constraint_: Define a constraint that must be satisfied by the chosen candidate. In this case, the constraint is that the string must be a _palindrome_.

- _Goal_: We must define the goal that determines if have found the required solution and we must backtrack. Here, our goal is achieved if we have reached the end of the string.

**Algorithm**

In the backtracking algorithm, we recursively traverse over the string in depth-first search fashion. For each recursive call, the beginning index of the string is given as $$\text{start}$$.

1) Iteratively generate all possible substrings beginning at $$\text{start}$$ index. The $$\text{end}$$ index increments from $$\text{start}$$ till the end of the string.

3) For each of the substring generated, check if it is a palindrome.

4) If the substring is a palindrome, the substring is a potential candidate. Add substring to the $$\text{currentList}$$ and perform a depth-first search on the remaining substring. If current substring ends at index $$\text{end}$$, $$\text{end}+1$$ becomes the $$\text{start}$$ index for the next recursive call.

5) Backtrack if $$\text{start}$$ index is greater than or equal to the string length and add the $$\text{currentList}$$ to the result.


!?!../Documents/131_LIS.json:1772,888!?!

**Implementation**

<iframe src="https://leetcode.com/playground/SVz6Z7cu/shared" frameBorder="0" width="100%" height="500" name="SVz6Z7cu"></iframe>

**Complexity Analysis**

- Time Complexity : $$\mathcal{O}(N \cdot 2^{N})$$, where $$N$$ is the length of string $$s$$. This is the worst-case time complexity when all the possible substrings are palindrome.

Example, if = s = `aaa`, the recursive tree can be illustrated as follows:

![img](../Figures/131/time_complexity.png)


Hence, there could be $$2^{N}$$ possible substrings in the worst case. For each substring, it takes $$\mathcal{O}(N)$$ time to generate substring and determine if it a palindrome or not. This gives us time complexity as $$\mathcal{O}(N \cdot 2^{N})$$

- Space Complexity: $$\mathcal{O}(N)$$, where $$N$$ is the length of the string $$s$$. This space will be used to store the recursion stack. For s = `aaa`, the maximum depth of the recursive call stack is 3 which is equivalent to $$N$$.

---

#### Approach 2: Backtracking with Dynamic Programming

**Intuition**

This approach uses a similar Backtracking algorithm as discussed in _Approach 1_. But, the previous approach performs one extra iteration to determine if a given substring is a palindrome or not. Here, we are repeatedly iterating over the same substring multiple times and the result is always the same. There are [Overlapping Subproblems](https://en.wikipedia.org/wiki/Overlapping_subproblems) and we could further optimize the approach by using dynamic programming to determine if a string is a palindrome in constant time. Let's understand the algorithm in detail.

**Algorithm**

A given string $$s$$ starting at index $$\text{start}$$ and ending at index $$\text{end}$$ is a palindrome if  following conditions are satisfied :
1) The characters at $$\text{start}$$ and $$\text{end}$$ indexes are equal.
2) The substring starting at index $$\text{start}+1$$ and ending at index $$\text{end}-1$$ is a palindrome.

![img](../Figures/131/palindrome_dp.png)

Let $$N$$ be the length of the string.
To determine if a substring starting at index $$\text{start}$$ and ending at index $$\text{end}$$ is a palindrome or not, we use a 2 Dimensional array $$\text{dp}$$ of size $$N \cdot N$$  where,

$$\text{dp[start][end]} = \text{true}$$ , if the substring beginning at index $$\text{start}$$ and ending at index $$\text{end}$$ is a palindrome.

Otherwise, $$\text{dp[start][end] }== \text{false}$$.

Also, we must update the $$\text{dp}$$ array, if we find that the current string is a palindrome.  

**Implementation**

<iframe src="https://leetcode.com/playground/foWP6HLD/shared" frameBorder="0" width="100%" height="463" name="foWP6HLD"></iframe>

**Complexity Analysis**

- Time Complexity : $$\mathcal{O}(N \cdot 2^{N})$$, where $$N$$ is the length of string $$s$$. In the worst case, there could be $$2^{N}$$ possible substrings and it will take $$\mathcal{O}(N)$$ to generate each substring using `substr` as in _Approach 1_. However, we are eliminating one additional iteration to check if substring is a palindrome or not.

- Space Complexity: $$\mathcal{O}(N \cdot N)$$, where $$N$$ is the length of the string $$s$$. The recursive call stack would require $$N$$ space as in _Approach 1_. Additionally we also use 2 dimensional array $$\text{dp}$$ of size $$N \cdot N$$ .

This gives us total space complexity as $$\mathcal{O}(N \cdot N)$$ + $$\mathcal{O}(N)$$ = $$\mathcal{O}(N \cdot N)$$

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java: Backtracking solution.
- Author: xiaodoubao
- Creation Date: Sat Dec 20 2014 02:01:30 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 14:34:09 GMT+0800 (Singapore Standard Time)

<p>
if the input is "aab", check if [0,0] "a" is palindrome. then check [0,1] "aa", then [0,2] "aab".
While checking [0,0], the rest of string is "ab",  use ab as input to make a recursive call.
![enter image description here][1]

in this example, in the loop of i=l+1, a recursive call will be made with input = "ab".
Every time a recursive call is made, the position of l move right. 

How to define a correct answer?
Think about DFS, if the current string to be checked (Palindrome) contains the last position, in this case "c", this path is a correct answer, otherwise, it's a false answer.

![enter image description here][2]

line 13:  is the boundary to check if the current string contains the last element. 
 l>=s.length()  

    public class Solution {
            List<List<String>> resultLst;
    	    ArrayList<String> currLst;
    	    public List<List<String>> partition(String s) {
    	        resultLst = new ArrayList<List<String>>();
    	        currLst = new ArrayList<String>();
    	        backTrack(s,0);
    	        return resultLst;
    	    }
    	    public void backTrack(String s, int l){
    	        if(currLst.size()>0 //the initial str could be palindrome
    	            && l>=s.length()){
    	                List<String> r = (ArrayList<String>) currLst.clone();
    	                resultLst.add(r);
    	        }
    	        for(int i=l;i<s.length();i++){
    	            if(isPalindrome(s,l,i)){
    	                if(l==i)
    	                    currLst.add(Character.toString(s.charAt(i)));
    	                else
    	                    currLst.add(s.substring(l,i+1));
    	                backTrack(s,i+1);
    	                currLst.remove(currLst.size()-1);
    	            }
    	        }
    	    }
    	    public boolean isPalindrome(String str, int l, int r){
    	        if(l==r) return true;
    	        while(l<r){
    	            if(str.charAt(l)!=str.charAt(r)) return false;
    	            l++;r--;
    	        }
    	        return true;
    	    }
    }


  [1]: http://1.bp.blogspot.com/-3g_qWEIsyUI/VJR0Co__PcI/AAAAAAAAAfg/okeb7u1mZnI/s1600/test.png
  [2]: http://i58.tinypic.com/2la69p2.png
</p>


### Java DP + DFS solution
- Author: yfcheng
- Creation Date: Sun Feb 21 2016 00:47:14 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 10:50:12 GMT+0800 (Singapore Standard Time)

<p>
The normal dfs backtracking will need to check each substring for palindrome, but a dp array can be used to record the possible break for palindrome before we start recursion.

Edit:
Sharing my thought process:
first, I ask myself that how to check if a string is palindrome or not, usually a two point solution scanning from front and back.  Here if you want to get all the possible palindrome partition, first a nested for loop to get every possible partitions for a string, then a scanning for all the partitions.  That's a O(n^2) for partition and O(n^2) for the scanning of string, totaling at O(n^4) just for the partition.  However, if we use a 2d array to keep track of any string we have scanned so far, with an addition pair, we can determine whether it's palindrome or not by justing looking at that pair, which is this line `if(s.charAt(i) == s.charAt(j) && (i - j <= 2 || dp[j+1][i-1]))`.  This way, the 2d array `dp` contains the possible palindrome partition among all.  

second, based on the prescanned palindrome partitions saved in dp array, a simple backtrack does the job.

    public class Solution {
        public List<List<String>> partition(String s) {
            List<List<String>> res = new ArrayList<>();
            boolean[][] dp = new boolean[s.length()][s.length()];
            for(int i = 0; i < s.length(); i++) {
                for(int j = 0; j <= i; j++) {
                    if(s.charAt(i) == s.charAt(j) && (i - j <= 2 || dp[j+1][i-1])) {
                        dp[j][i] = true;
                    }
                }
            }
            helper(res, new ArrayList<>(), dp, s, 0);
            return res;
        }
        
        private void helper(List<List<String>> res, List<String> path, boolean[][] dp, String s, int pos) {
            if(pos == s.length()) {
                res.add(new ArrayList<>(path));
                return;
            }
            
            for(int i = pos; i < s.length(); i++) {
                if(dp[pos][i]) {
                    path.add(s.substring(pos,i+1));
                    helper(res, path, dp, s, i+1);
                    path.remove(path.size()-1);
                }
            }
        }
    }
</p>


### Java: Backtracking Template -- General Approach
- Author: y495711146
- Creation Date: Wed Oct 17 2018 05:44:20 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 11:10:30 GMT+0800 (Singapore Standard Time)

<p>
All backtracking problems are composed by these three steps: ```choose```, ```explore```, ```unchoose```.
So for each problem, you need to know:
1. 	```choose what?``` For this problem, we choose each substring.
2. 	```how to explore?``` For this problem, we do the same thing to the remained string.
3. 	```unchoose``` Do the opposite operation of choose.

Let\'s take this problem as an example:
```1.Define helper()```: Usually we need a helper funcition in backtracking problem, to accept more parameters.
```2.Parameters```:  Usually we need the following parameters
```
    1. The object you are working on:  For this problem is String s.
    2. A start index or an end index which indicate which part you are working on: For this problem, we use substring to indicate the start index.
    3. A step result, to remember current choose and then do unchoose : For this problem, we use List<String> step.
    4. A final result, to remember the final result. Usually when we add, we use \'result.add(new ArrayList<>(step))\' instead of \'result.add(step)\', since step is reference passed. We will modify step later, so we need to copy it and add the copy to the result;
```
```3.Base case```:  The base case defines when to add step into result, and when to return.
```4.Use for-loop ```:  Usually we need a for loop to iterate though the input String s, so that we can choose all the options.
```5.Choose ```: In this problem, if the substring of s is palindrome, we add it into the step, which means we choose this substring.
```6.Explore ```: In this problem, we want to do the same thing to the remaining substring. So we recursively call our function.
```7.Un-Choose ```: We draw back, remove the chosen substring, in order to try other options. 

<hr/>

```The above is mainly the template, the code is shown below:```

```java
public List<List<String>> partition(String s) {
        // Backtracking
        // Edge case
        if(s == null || s.length() == 0) return new ArrayList<>();
        
        List<List<String>> result = new ArrayList<>();
        helper(s, new ArrayList<>(), result);
        return result;
    }
    public void helper(String s, List<String> step, List<List<String>> result) {
        // Base case
        if(s == null || s.length() == 0) {
            result.add(new ArrayList<>(step));
            return;
        }
        for(int i = 1; i <= s.length(); i++) {
            String temp = s.substring(0, i);
            if(!isPalindrome(temp)) continue; // only do backtracking when current string is palindrome
            
            step.add(temp);  // choose
            helper(s.substring(i, s.length()), step, result); // explore
            step.remove(step.size() - 1); // unchoose
        }
        return;
    }
    public boolean isPalindrome(String s) {
        int left = 0, right = s.length() - 1;
        while(left <= right) {
            if(s.charAt(left) != s.charAt(right))
                return false;
            left ++;
            right --;
        }
        return true;
    }
```
Other related backtracking problems discuss:
[A general approach to backtracking questions in Java (Subsets, Permutations, Combination Sum, Palindrome Partitioning)
](https://leetcode.com/problems/subsets/discuss/27281/A-general-approach-to-backtracking-questions-in-Java-(Subsets-Permutations-Combination-Sum-Palindrome-Partitioning))
</p>


