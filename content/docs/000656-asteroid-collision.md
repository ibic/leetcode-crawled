---
title: "Asteroid Collision"
weight: 656
#id: "asteroid-collision"
---
## Description
<div class="description">
<p>
We are given an array <code>asteroids</code> of integers representing asteroids in a row.
</p><p>
For each asteroid, the absolute value represents its size, and the sign represents its direction (positive meaning right, negative meaning left).  Each asteroid moves at the same speed.
</p><p>
Find out the state of the asteroids after all collisions.  If two asteroids meet, the smaller one will explode.  If both are the same size, both will explode.  Two asteroids moving in the same direction will never meet.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> 
asteroids = [5, 10, -5]
<b>Output:</b> [5, 10]
<b>Explanation:</b> 
The 10 and -5 collide resulting in 10.  The 5 and 10 never collide.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> 
asteroids = [8, -8]
<b>Output:</b> []
<b>Explanation:</b> 
The 8 and -8 collide exploding each other.
</pre>
</p>

<p><b>Example 3:</b><br />
<pre>
<b>Input:</b> 
asteroids = [10, 2, -5]
<b>Output:</b> [10]
<b>Explanation:</b> 
The 2 and -5 collide resulting in -5.  The 10 and -5 collide resulting in 10.
</pre>
</p>

<p><b>Example 4:</b><br />
<pre>
<b>Input:</b> 
asteroids = [-2, -1, 1, 2]
<b>Output:</b> [-2, -1, 1, 2]
<b>Explanation:</b> 
The -2 and -1 are moving left, while the 1 and 2 are moving right.
Asteroids moving the same direction never meet, so no asteroids will meet each other.
</pre>
</p>

<p><b>Note:</b>
<li>The length of <code>asteroids</code> will be at most <code>10000</code>.</li>
<li>Each asteroid will be a non-zero integer in the range <code>[-1000, 1000].</code>.</li>
</p>
</div>

## Tags
- Stack (stack)

## Companies
- Amazon - 7 (taggedByAdmin: false)
- Lyft - 5 (taggedByAdmin: false)
- ByteDance - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Visa - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Uber - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Stack [Accepted]

**Intuition**

A row of asteroids is stable if no further collisions will occur.  After adding a new asteroid to the right, some more collisions may happen before it becomes stable again, and all of those collisions (if they happen) must occur right to left.  This is the perfect situation for using a *stack*.

**Algorithm**

Say we have our answer as a stack with rightmost asteroid `top`, and a `new` asteroid comes in.  If `new` is moving right (`new > 0`), or if `top` is moving left (`top < 0`), no collision occurs.

Otherwise, if `abs(new) < abs(top)`, then the `new` asteroid will blow up; if `abs(new) == abs(top)` then both asteroids will blow up; and if `abs(new) > abs(top)`, then the `top` asteroid will blow up (and possibly more asteroids will, so we should continue checking.)

<iframe src="https://leetcode.com/playground/hPCRzMdB/shared" frameBorder="0" width="100%" height="480" name="hPCRzMdB"></iframe>


**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of asteroids.  Our stack pushes and pops each asteroid at most once.

* Space Complexity: $$O(N)$$, the size of `ans`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] Clean Code
- Author: alexander
- Creation Date: Sun Nov 26 2017 12:02:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 13:54:30 GMT+0800 (Singapore Standard Time)

<p>
[735. Asteroid Collision](https://leetcode.com/problems/asteroid-collision/)
- at the end, all the negative star has to be on the left, and all the positive star has to be on the right.
- from the left, a negative star will pass through if no positive star on the left;
- keep track of all the positive stars moving to the right, the right most one will be the 1st confront the challenge of any future negative star.
- if it survives, keep going, otherwise, any past positive star will be exposed to the challenge, by being popped out of the stack.

**C++**
```
class Solution {
public:
    vector<int> asteroidCollision(vector<int>& a) {
        vector<int> s; // use vector to simulate stack.
        for (int i = 0; i < a.size(); i++) {
            if (a[i] > 0 || s.empty() || s.back() < 0) // a[i] is positive star or a[i] is negative star and there is no positive on stack
                s.push_back(a[i]);
            else if (s.back() <= -a[i]) { // a[i] is negative star and stack top is positive star
                if(s.back() < -a[i]) i--; // only positive star on stack top get destroyed, stay on i to check more on stack.
                s.pop_back(); // destroy positive star on the frontier;
            } // else : positive on stack bigger, negative star destroyed.
        }
        return s;
    }
};
```
**java**
```
class Solution {
    public int[] asteroidCollision(int[] a) {
        LinkedList<Integer> s = new LinkedList<>(); // use LinkedList to simulate stack so that we don't need to reverse at end.
        for (int i = 0; i < a.length; i++) {
            if (a[i] > 0 || s.isEmpty() || s.getLast() < 0)
                s.add(a[i]);
            else if (s.getLast() <= -a[i])
                if (s.pollLast() < -a[i]) i--;
        }
        return s.stream().mapToInt(i->i).toArray();
    }
}
```

**More Intuitive Solution**
The above approach is short but not intuitive, because it handles positive stars on the stack top with less mess than the incoming negative star once a time each loop.
More intuitively we can pop all positive star with less mass right away using a while loop in side the for loop.
**Java**
```
class Solution {
    public int[] asteroidCollision(int[] a) {
        LinkedList<Integer> s = new LinkedList<>();
        for (int i : a) {
            if (i > 0)
                s.add(i);
            else {
                while (!s.isEmpty() && s.getLast() > 0 && s.getLast() < -i)
                    s.pollLast();
                if (!s.isEmpty() && s.getLast() == -i)
                    s.pollLast();
                else if (s.isEmpty() || s.getLast() < 0)
                    s.add(i);
            }
        }
        return s.stream().mapToInt(i->i).toArray();
    }
}
```
```
class Solution {
    public int[] asteroidCollision(int[] a) {
        LinkedList<Integer> s = new LinkedList<>(); // use LinkedList to simulate stack so that we don't need to reverse at end.
        for (int i : a) {
            while (!s.isEmpty() && s.getLast() > 0 && s.getLast() < -i)
                s.pollLast();
            if (s.isEmpty() || i > 0 || s.getLast() < 0)
                s.add(i);
            else if (i < 0 && s.getLast() == -i)
                s.pollLast();
        }
        return s.stream().mapToInt(i->i).toArray();
    }
}
```
**C++**
```
class Solution {
public:
    vector<int> asteroidCollision(vector<int>& a) {
        vector<int> s; // use vector to simulate stack.
        for (int i : a) {
            while (!s.empty() && s.back() > 0 && s.back() < -i)
                s.pop_back();
            if (s.empty() || i > 0 || s.back() < 0)
                s.push_back(i);
            else if (i < 0 && s.back() == -i)
                s.pop_back();
        }
        return s;
    }
};
```
</p>


### Python O(n) Stack-based with explanation
- Author: yangshun
- Creation Date: Sun Nov 26 2017 15:18:54 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Feb 19 2020 20:57:28 GMT+0800 (Singapore Standard Time)

<p>
Not as clean as the given solution, but perhaps easier to understand.

Some observations:

1. Negative asteroids without any positive asteroids on the left can be ignored as they will never interact with the upcoming asteroids regardless of their direction.
2. Positive asteroids (right-moving) may interact with negative asteroids (left-moving) that come later.

We can use a stack called `res` to efficiently simulate the collisions. We can iterate through the list of `asteroids` and handle the following scenarios as such:

1. If `res` is empty, we push that `asteroid` into it regardless of directions. Because negative asteroids will be part of the final result while positive asteroids may interact with future negative asteroids.
2. If the `asteroid` is positive, push it into `res`. It will never interact with existing asteroids in `res` but may interact with future negative asteroids.
3. If the `asteroid` is negative, we need to simulate the collision process by repeatedly popping the positive asteroids from the top of the stack and compare to see which asteroid survives the collision. We may or may not need to push the negative `asteroid` to `res` depending on the value of the positive asteroids it encounters. Push the negative `asteroid` if it survives all the collisions.

Scenarios 1 and 2 can be handled in the `else` case and scenario 3 is the `while` case.

*- Yangshun*

```
class Solution(object):
    def asteroidCollision(self, asteroids):
        res = []
        for asteroid in asteroids:
            # We only need to resolve collisions under the following conditions:
            # - stack is non-empty
            # - current asteroid is -ve
            # - top of the stack is +ve
            while len(res) and asteroid < 0 and res[-1] > 0:
                # Both asteroids are equal, destroy both.
                if res[-1] == -asteroid: 
                    res.pop()
                    break
                # Stack top is smaller, remove the +ve asteroid 
                # from the stack and continue the comparison.
                elif res[-1] < -asteroid:
                    res.pop()
                    continue
                # Stack top is larger, -ve asteroid is destroyed.
                elif res[-1] > -asteroid:
                    break
            else:
                # -ve asteroid made it all the way to the 
                # bottom of the stack and destroyed all asteroids.
                res.append(asteroid)
        return res
```
</p>


### Java easy to understand solution
- Author: daijidj
- Creation Date: Wed Nov 14 2018 13:58:01 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 14 2018 13:58:01 GMT+0800 (Singapore Standard Time)

<p>
```
public int[] asteroidCollision(int[] asteroids) {
        Stack<Integer> s = new Stack<>();
        for(int i: asteroids){
            if(i > 0){
                s.push(i);
            }else{// i is negative
                while(!s.isEmpty() && s.peek() > 0 && s.peek() < Math.abs(i)){
                    s.pop();
                }
                if(s.isEmpty() || s.peek() < 0){
                    s.push(i);
                }else if(i + s.peek() == 0){
                    s.pop(); //equal
                }
            }
        }
        int[] res = new int[s.size()];   
        for(int i = res.length - 1; i >= 0; i--){
            res[i] = s.pop();
        }
        return res;
    }
```
</p>


