---
title: "Corporate Flight Bookings"
weight: 1091
#id: "corporate-flight-bookings"
---
## Description
<div class="description">
<p>There are <code>n</code> flights, and they are labeled&nbsp;from <code>1</code> to <code>n</code>.</p>

<p>We have a list of flight bookings.&nbsp; The <code>i</code>-th booking&nbsp;<code>bookings[i] = [i, j, k]</code>&nbsp;means that we booked <code>k</code> seats from flights labeled <code>i</code> to <code>j</code> inclusive.</p>

<p>Return an array <code>answer</code> of length <code>n</code>, representing the number of seats booked on each flight in order of their label.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> bookings = [[1,2,10],[2,3,20],[2,5,25]], n = 5
<strong>Output:</strong> [10,55,45,25,25]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= bookings.length &lt;= 20000</code></li>
	<li><code>1 &lt;= bookings[i][0] &lt;= bookings[i][1] &lt;= n &lt;= 20000</code></li>
	<li><code>1 &lt;= bookings[i][2] &lt;= 10000</code></li>
</ul>
</div>

## Tags
- Array (array)
- Math (math)

## Companies
- Goldman Sachs - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java with picture, O(n)
- Author: votrubac
- Creation Date: Sun Jul 07 2019 12:03:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 07 2019 12:56:18 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Since ranges are continuous, what if we add reservations to the first flight in the range, and remove them after the last flight in range? We can then use the running sum to update reservations for all flights.

This picture shows the logic for this test case: ```[[1,2,10],[2,3,20],[3,5,25]]```.
![image](https://assets.leetcode.com/users/votrubac/image_1562473681.png)
## C++ Solution
```
vector<int> corpFlightBookings(vector<vector<int>>& bookings, int n) {
  vector<int> res(n);
  for (auto &v : bookings) {
    res[v[0] - 1] += v[2];
    if (v[1] < n) res[v[1]] -= v[2];
  }
  for (auto i = 1; i < n; ++i) res[i] += res[i - 1];
  return res;
}
```
## Java Solution
```
public int[] corpFlightBookings(int[][] bookings, int n) {
  int[] res = new int[n];
  for (int[] v : bookings) {
    res[v[0] - 1] += v[2];
    if (v[1] < n) res[v[1]] -= v[2];
  }
  for (int i = 1; i < n; ++i) res[i] += res[i - 1];
  return res;
}
```
## Complexity Analysis
Runtime: *O(n)*, where n is the number of flights (or bookings).
Memory: *O(n)*
</p>


### [Java/C++/Python] Sweep Line
- Author: lee215
- Creation Date: Sun Jul 07 2019 12:02:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 20 2020 00:56:26 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Set the change of seats for each day.
If `booking = [i, j, k]`,
it needs `k` more seat on `i`th day,
and we don\'t need these seats on `j+1`th day.
We accumulate these changes then we have the result that we want.
<br>

## **Complexity**
Time `O(booking + N)` for one pass on `bookings`
Space `O(N)` for the result
<br>

**Java:**
```java
    public int[] corpFlightBookings(int[][] bookings, int n) {
        int[] res = new int[n];
        for (int[] b : bookings) {
            res[b[0] - 1] += b[2];
            if (b[1] < n) res[b[1]] -= b[2];
        }
        for (int i = 1; i < n; ++i)
            res[i] += res[i - 1];
        return res;
    }
```

**C++:**
```cpp
    vector<int> corpFlightBookings(vector<vector<int>>& bookings, int n) {
        vector<int> res(n + 1, 0);
        for (auto & b : bookings) {
            res[b[0] - 1] += b[2];
            res[b[1]] -= b[2];
        }
        for (int i = 1; i < n; ++i)
            res[i] += res[i - 1];
        return {res.begin(), res.end() - 1};
    }
```

**Python:**
```python
    def corpFlightBookings(self, bookings, n):
        res = [0] * (n + 1)
        for i, j, k in bookings:
            res[i - 1] += k
            res[j] -= k
        for i in xrange(1, n):
            res[i] += res[i - 1]
        return res[:-1]
```

</p>


### Easy Python O(n) -- Let's step through the algorithm
- Author: bw1226
- Creation Date: Wed Jul 10 2019 05:36:03 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 09 2020 03:14:29 GMT+0800 (Singapore Standard Time)

<p>
The linear solution relies on a "mathematical trick" (derivative?), meaning we can find the final result by computing the cumulative sum so we don\'t need to compute every element within the range of seats. 

Let\'s take this example:
[[1,2,10],[2,3,20],[2,5,25]], n = 5
The first booking is [1,2,10] where 1-2 is the range, and 10 the number of seats

If you look at the code below, it becomes obvious the result array is changed to:
[10,0,-10,0,0] 

**Note we only changed 2 cells. This would be a huge runtime-saver if the range was 1-2000.**

(There\'s actually an extra 0 in the result array to stay within bounds, since we mark the end of the range with **-seats**)

We all know after the first loop, the result array must be [10,10,0,0,0]. **This is exactly what we get if we take the cumulative sum for [10,0,-10,0,0].**

This is just a simple demonstration of the cumulative sum. We actually only do this at the very end of the algorithm, when we have stepped through all the bookings and marked/updated the ranges appropriately. 

If we do this for all the bookings, and then apply the cumulative sum, we get this:

[10, 45, -10, -20, 0, 25, -25]   ->    [10, 55, 45, 25, 25, 0] 

The only tricky part is paying attention to the 0-indexed result array because we added 1 for padding to mark the end of the range. 

So we return only the first n elements. 

See the code below.

```
class Solution:
    def corpFlightBookings(self, bookings: List[List[int]], n: int) -> List[int]:
        result = [0] * (n+1)
        #mark the range with the deltas (head and tail)
        for booking in bookings:
            #start
            result[booking[0]-1] += booking[2]
            #end
            result[booking[1]] -= booking[2]
        #cumulative sum processing
        tmp = 0
        for i in range(n):
            tmp += result[i]
            result[i] = tmp
        return result[:n]
```

If you like this explanation, please consider giving it a star on my [github](https://github.com/bwiens/leetcode-python). Means a lot to me.

*credit also to TheWisp
</p>


