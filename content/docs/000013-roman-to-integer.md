---
title: "Roman to Integer"
weight: 13
#id: "roman-to-integer"
---
## Description
<div class="description">
<p>Roman numerals are represented by seven different symbols:&nbsp;<code>I</code>, <code>V</code>, <code>X</code>, <code>L</code>, <code>C</code>, <code>D</code> and <code>M</code>.</p>

<pre>
<strong>Symbol</strong>       <strong>Value</strong>
I             1
V             5
X             10
L             50
C             100
D             500
M             1000</pre>

<p>For example,&nbsp;<code>2</code> is written as <code>II</code>&nbsp;in Roman numeral, just two one&#39;s added together. <code>12</code> is written as&nbsp;<code>XII</code>, which is simply <code>X + II</code>. The number <code>27</code> is written as <code>XXVII</code>, which is <code>XX + V + II</code>.</p>

<p>Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not <code>IIII</code>. Instead, the number four is written as <code>IV</code>. Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as <code>IX</code>. There are six instances where subtraction is used:</p>

<ul>
	<li><code>I</code> can be placed before <code>V</code> (5) and <code>X</code> (10) to make 4 and 9.&nbsp;</li>
	<li><code>X</code> can be placed before <code>L</code> (50) and <code>C</code> (100) to make 40 and 90.&nbsp;</li>
	<li><code>C</code> can be placed before <code>D</code> (500) and <code>M</code> (1000) to make 400 and 900.</li>
</ul>

<p>Given a roman numeral, convert it to an integer.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;III&quot;
<strong>Output:</strong> 3
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;IV&quot;
<strong>Output:</strong> 4
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;IX&quot;
<strong>Output:</strong> 9
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;LVIII&quot;
<strong>Output:</strong> 58
<strong>Explanation:</strong> L = 50, V= 5, III = 3.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;MCMXCIV&quot;
<strong>Output:</strong> 1994
<strong>Explanation:</strong> M = 1000, CM = 900, XC = 90 and IV = 4.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 15</code></li>
	<li><code>s</code> contains only&nbsp;the characters <code>(&#39;I&#39;, &#39;V&#39;, &#39;X&#39;, &#39;L&#39;, &#39;C&#39;, &#39;D&#39;, &#39;M&#39;)</code>.</li>
	<li>It is <strong>guaranteed</strong>&nbsp;that <code>s</code> is a valid roman numeral in the range <code>[1, 3999]</code>.</li>
</ul>

</div>

## Tags
- Math (math)
- String (string)

## Companies
- Amazon - 17 (taggedByAdmin: false)
- Apple - 7 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: true)
- Facebook - 3 (taggedByAdmin: true)
- Goldman Sachs - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Roblox - 2 (taggedByAdmin: false)
- LinkedIn - 7 (taggedByAdmin: false)
- Bloomberg - 5 (taggedByAdmin: true)
- Yahoo - 4 (taggedByAdmin: true)
- Oracle - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- Qualtrics - 7 (taggedByAdmin: false)
- Zillow - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Overview

In a lot of countries, Roman Numerals are taught in elementary school-level math. This has made them a somewhat popular "easy" interview question. Unfortunately though, this ignores the fact that *not everybody learned them in school*, and therefore a big advantage has been given to those who did. I suspect it's also difficult for a lot of us who *have* learned them previously to fully appreciate how much easier prior experience makes this question. While this is very unfair, and possibly very frustrating, keep in mind that the best thing you can do is work through this question and the related question [Integer to Roman](https://leetcode.com/problems/integer-to-roman/) so that you don't get caught out by it in a real interview.

**Can we assume the input is valid?**

Yes. Here on Leetcode, you can make that assumption because you *haven't* been told what to do if it *isn't*.

In a real interview, this is a question you should ask the interviewer. Don't ever assume without asking in a real interview that the input has to be valid.

**Is there only one valid representation for each number?**

This is more relevant to the other question, [Integer to Roman](https://leetcode.com/problems/integer-to-roman/), however we'll still briefly look at it now.

Given that the representation for `3` is `III`, it could seem natural that the representation for `15` is `VVV`, because that would be `5 + 5 + 5`. However, it's actually `XV`, which is `10 + 5`. How are you even supposed to know which is correct?

The trick is to use the "biggest" symbols you can. Because `X` is bigger than `V`, we should use an `X` first and then make up the remainder with a single `V`, giving `XV`.

We'll talk more about this in the [Integer to Roman](https://leetcode.com/problems/integer-to-roman/) article. This question is a lot simpler because there's only one logical way of converting from a *Roman Numeral to an Integer*. This is also why this question is labelled as "easy", whereas the other is labelled as "medium".

**A few more examples**

If you're not very familiar with Roman Numerals, work through these examples and then have another go at writing your own algorithm before reading the rest of this solution article.

*What is `CXVII` as an integer?*

Recall that `C = 100`, `X = 10`, `V = 5`, and `I = 1`. Because the symbols are ordered from most significant to least, we can simply add the symbols, i.e. `C + X + V + I + I = 100 + 10 + 5 + 1 + 1 = 117`.

*What is `DXCI` as an integer?*

Recall that `D = 500`. 

Now, notice that this time the symbols are *not* ordered from most significant to least—the `X` and `C` are out of numeric order. Because of this, we subtract the value of `X` (`10`) from the value of `C` (`100`) to get `90`.

So, going from left to right, we have `D + (C - X) + I = 500 + 90 + 1 = 591`. 

*What is `CMXCIV` as an integer?*

Recall that `M = 1000`.

The symbols barely look sorted at all here—from left-to-right we have `100, 1000, 10, 100, 1, 5`. Do not panic though, we just need to look for each occurrence of a smaller symbols *preceding* a bigger symbol. The first, third, and fifth symbols are all smaller than their next symbol. Therefore they are all going to be subtracted from their next.

- The first two symbols are `CM`. This is `M - C = 1000 - 100 = 900`
- The second two symbols are `XC`. This is `C - X = 100 - 10 = 90`.
- The final two symbols are `IV`. This is `V - I = 5 - 1 = 4`.

Like we did above, we add these together. `(M - C) + (C - X) + (V - I) = 900 + 90 + 4 = 994`.

---

#### Approach 1: Left-to-Right Pass

**Intuition**

Let's hard-code a mapping with the value of each symbol so that we can easily look them up.

![Symbol mapping](../Figures/13/hardcoding_1.png)

Now, recall that each symbol adds its own value, *except for* when a *smaller valued symbol* is *before* a *larger valued symbol*. In those cases, instead of adding both symbols to the total, we need to *subtract the large from the small*, adding that instead.

Therefore, the simplest algorithm is to use a pointer to scan through the string, at each step deciding whether to add the current symbol and go forward 1 place, or add the difference of the next 2 symbols and go forward 2 places. Here is this algorithm in pseudocode.

```text 
total = 0
i = 0
while i < s.length:
    if at least 2 symbols remaining AND value of s[i] < value of s[i + 1]:
        total = total + (value of s[i + 1]) - (value of s[i])  
        i = i + 2
    else:
        total = total + (value of s[i])
        i = i + 1
return total
```

Here is an animation of the above algorithm.

!?!../Documents/13_animation_1.json:816,307!?!

Recall that the *input is always valid*. This means that we don't need to worry about being given inputs such as `ICD`.

**Algorithm**

<iframe src="https://leetcode.com/playground/kcAKumjx/shared" frameBorder="0" width="100%" height="500" name="kcAKumjx"></iframe>

**Complexity Analysis**

Let $$n$$ be the length of the input string (the total number of symbols in it).

- Time complexity : $$O(1)$$.

    As there is a finite set of roman numerals, the maximum number possible number can be `3999`, which in roman numerals is  `MMMCMXCIX`. As such the time complexity is $$O(1)$$.

    If roman numerals had an arbitrary number of symbols, then the time complexity would be proportional to the length of the input, i.e. $$O(n)$$. This is assuming that looking up the value of each symbol is $$O(1)$$.

- Space complexity : $$O(1)$$.

    Because only a constant number of single-value variables are used, the space complexity is $$O(1)$$.

</br>

---

#### Approach 2: Left-to-Right Pass Improved

**Intuition**

Instead of viewing a Roman Numeral as having `7` unique symbols, we could instead view it as having `13` unique symbols—some of length-1 and some of length-2.

![Symbol mapping](../Figures/13/hardcoding_2_fixed.png)

For example, here is the Roman Numeral `MMCMLXXXIX` broken into its symbols using this definition:

![Splitting the numeral into parts](../Figures/13/length_2_symbols_example.png)

We can then look up the value of each symbol and add them together.

![Adding up the sum of the numeral](../Figures/13/length_2_symbols_calculation.png)

After making a `Map` of `String -> Integer` with the `13` "symbols", we need to work our way down the string in the same way as before (we'll do left-to-right, however right-to-left will work okay too), firstly checking if we're at a length-2 symbol, and if not, then treating it as a length-1 symbol.

```text 
total = 0
i = 0
while i < s.length:
    if at least 2 characters remaining and s.substing(i, i + 1) is in values:
        total = total + (value of s.substring(i, i + 1))  
        i = i + 2
    else:
        total = total + (value of s[i])
        i = i + 1
return total
```

Here is an animation showing the algorithm.

!?!../Documents/13_animation_2.json:816,307!?!

**Algorithm**

<iframe src="https://leetcode.com/playground/2RKp9fC5/shared" frameBorder="0" width="100%" height="500" name="2RKp9fC5"></iframe>

**Complexity Analysis**

- Time complexity : $$O(1)$$.

    Same as Approach 1.

- Space complexity : $$O(1)$$.

    Same as Approach 1.

</br>

---

#### Approach 3: Right-to-Left Pass

**Intuition**

This approach is a more elegant variant of Approach 1. Just to be clear though, *Approach 1 and Approach 2 are probably sufficient for an interview. This approach is still well worth understanding though.*

In the "subtraction" cases, such as `XC`, we've been updating our running `sum` as follows:

```text
sum += value(C) - value(X)
```

However, notice that this is mathematically equivalent to the following:

```text
sum += value(C)
sum -= value(X)
```

Utilizing this means that we can process *one* symbol each time we go around the main loop. We still need to determine whether or not our current symbol should be added or subtracted by looking at the neighbour though.

In Approach 1, we had to be careful when inspecting the next symbol to not go over the end of the string. This check wasn't difficult to do, but it increased the code complexity a bit, and it turns out we can avoid it with this approach!

Observe the following:

1. Without looking at the next symbol, we *don't* know whether or not the left-most symbol should be added or subtracted.
2. The right-most symbol is *always* added. It is either by itself, or the additive part of a pair.

So, what we can do is initialise sum to be the value of the right-most (last) symbol. Then, we work *backwards* through the string, starting from the second-to-last-symbol. We check the symbol *after* (`i + 1`) to determine whether the current symbol should be "added" or "subtracted".

```text
last = s.length - 1
total = value(last)
`
for i from last - 1 down to 0:
    if value(s[i]) < value(s[i+1]):
        total -= value(s[i])
    else:
        total += value(s[i])
return sum
```

Because we're starting at the second-to-last-index, we know that index `i + 1` always exists. We no longer need to handle its potential non-existence as a special case, and additionally we're able to (cleanly) use a `for` loop, as we're always moving along by 1 index at at time, unlike before where it could have been 1 or 2.

Here is an animation of the above approach.

!?!../Documents/13_animation_3.json:816,307!?!

**Algorithm**

<iframe src="https://leetcode.com/playground/rotmeTT4/shared" frameBorder="0" width="100%" height="500" name="rotmeTT4"></iframe>

**Complexity Analysis**

- Time complexity : $$O(1)$$.

    Same as Approach 1.

- Space complexity : $$O(1)$$.

    Same as Approach 1.
</br>

## Accepted Submission (java)
```java
class Solution {
	private Set<String> composites = new HashSet<>();
	private Map<Character, Integer> valueMap = new HashMap<>();

	public Solution() {
		valueMap.put('I', 1);
		valueMap.put('V', 5);
		valueMap.put('X', 10);
		valueMap.put('L', 50);
		valueMap.put('C', 100);
		valueMap.put('D', 500);
		valueMap.put('M', 1000);

		composites.add("IV");
		composites.add("IX");
		composites.add("XL");
		composites.add("XC");
		composites.add("CD");
		composites.add("CM");
	}

    public int romanToInt(String s) {
		int n = 0;
		char[] ca = s.toCharArray();
		int i = 0;
		while (i < ca.length) {
			if (i < ca.length - 1) {
				String twoChars = new String(Arrays.copyOfRange(ca, i, i + 2));
				if (composites.contains(twoChars)) {
					int sub = valueMap.get(twoChars.charAt(1)) - valueMap.get(twoChars.charAt(0));
					n += sub;
					i += 2;
				} else {
					n += valueMap.get(ca[i]);
					i += 1;
				}
			} else {
				n += valueMap.get(ca[i]);
				i += 1;
			}
		}
		return n;
	}
}
```

## Top Discussions
### My solution for this question but I don't know is there any easier way?
- Author: hongbin2
- Creation Date: Sun Jan 26 2014 13:53:45 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 02:26:52 GMT+0800 (Singapore Standard Time)

<p>
count every Symbol and add its value to the sum, and minus the extra part of special cases. 

    public int romanToInt(String s) {
         int sum=0;
        if(s.indexOf("IV")!=-1){sum-=2;}
        if(s.indexOf("IX")!=-1){sum-=2;}
        if(s.indexOf("XL")!=-1){sum-=20;}
        if(s.indexOf("XC")!=-1){sum-=20;}
        if(s.indexOf("CD")!=-1){sum-=200;}
        if(s.indexOf("CM")!=-1){sum-=200;}
        
        char c[]=s.toCharArray();
        int count=0;
        
       for(;count<=s.length()-1;count++){
           if(c[count]=='M') sum+=1000;
           if(c[count]=='D') sum+=500;
           if(c[count]=='C') sum+=100;
           if(c[count]=='L') sum+=50;
           if(c[count]=='X') sum+=10;
           if(c[count]=='V') sum+=5;
           if(c[count]=='I') sum+=1;
           
       }
       
       return sum;
        
    }
</p>


### Clean O(n) c++ solution
- Author: wsugrad77
- Creation Date: Fri Jan 23 2015 11:32:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 21:36:56 GMT+0800 (Singapore Standard Time)

<p>
Problem is simpler to solve by working the string from back to front and using a map.  Runtime speed is 88 ms.



    int romanToInt(string s) 
    {
        unordered_map<char, int> T = { { 'I' , 1 },
                                       { 'V' , 5 },
                                       { 'X' , 10 },
                                       { 'L' , 50 },
                                       { 'C' , 100 },
                                       { 'D' , 500 },
                                       { 'M' , 1000 } };
                                       
       int sum = T[s.back()];
       for (int i = s.length() - 2; i >= 0; --i) 
       {
           if (T[s[i]] < T[s[i + 1]])
           {
               sum -= T[s[i]];
           }
           else
           {
               sum += T[s[i]];
           }
       }
       
       return sum;
    }
</p>


### My Straightforward Python Solution
- Author: wenfengqiu
- Creation Date: Fri Jun 26 2015 04:20:14 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 08:54:16 GMT+0800 (Singapore Standard Time)

<p>

    class Solution:
    # @param {string} s
    # @return {integer}
    def romanToInt(self, s):
        roman = {'M': 1000,'D': 500 ,'C': 100,'L': 50,'X': 10,'V': 5,'I': 1}
        z = 0
        for i in range(0, len(s) - 1):
            if roman[s[i]] < roman[s[i+1]]:
                z -= roman[s[i]]
            else:
                z += roman[s[i]]
        return z + roman[s[-1]]


*Note: The trick is that the last letter is always added. Except the last one, if one letter is less than its latter one, this letter is subtracted.
</p>


