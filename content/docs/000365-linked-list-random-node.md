---
title: "Linked List Random Node"
weight: 365
#id: "linked-list-random-node"
---
## Description
<div class="description">
<p>Given a singly linked list, return a random node's value from the linked list. Each node must have the <b>same probability</b> of being chosen.</p>

<p><b>Follow up:</b><br />
What if the linked list is extremely large and its length is unknown to you? Could you solve this efficiently without using extra space?
</p>

<p><b>Example:</b>
<pre>
// Init a singly linked list [1,2,3].
ListNode head = new ListNode(1);
head.next = new ListNode(2);
head.next.next = new ListNode(3);
Solution solution = new Solution(head);

// getRandom() should return either 1, 2, or 3 randomly. Each element should have equal probability of returning.
solution.getRandom();
</pre>
</p>
</div>

## Tags
- Reservoir Sampling (reservoir-sampling)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Facebook - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Brief explanation for Reservoir Sampling
- Author: WTIFS
- Creation Date: Wed Aug 10 2016 11:06:40 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 01:25:27 GMT+0800 (Singapore Standard Time)

<p>
### Problem:
  - Choose <code>k</code> entries from <code>n</code> numbers. Make sure each number is selected with the probability of <code>k/n</code>

### Basic idea:
  - Choose <code>1, 2, 3, ..., k</code> first and put them into the reservoir.
  - For <code>k+1</code>, pick it with a probability of <code>k/(k+1)</code>, and randomly replace a number in the reservoir.
  - For <code>k+i</code>, pick it with a probability of <code>k/(k+i)</code>, and randomly replace a number in the reservoir.
  - Repeat until <code>k+i</code> reaches <code>n</code>

### Proof:
  - For <code>k+i</code>, the probability that it is selected and will replace a number in the reservoir is <code>k/(k+i)</code>
  - For a number in the reservoir before (let's say <code>X</code>), the probability that it keeps staying in the reservoir is 
    - <code>P(X was in the reservoir last time)</code> \xd7 <code>P(X is not replaced by k+i)</code>
    - = <code>P(X was in the reservoir last time)</code> \xd7 (<code>1</code> - <code>P(k+i is selected and replaces X)</code>)
    - = <code>k/(k+i-1)</code> \xd7 \uff08<code>1</code> - <code>k/(k+i)</code> \xd7 <code>1/k</code>\uff09
    - = <code>k/(k+i)</code>
  - When <code>k+i</code> reaches <code>n</code>, the probability of each number staying in the reservoir is <code>k/n</code>

### Example
  - Choose <code>3</code> numbers from <code>[111, 222, 333, 444]</code>. Make sure each number is selected with a probability of <code>3/4</code>
  - First, choose <code>[111, 222, 333]</code> as the initial reservior
  - Then choose <code>444</code> with a probability of <code>3/4</code>
  - For <code>111</code>, it stays with a probability of 
    - <code>P(444 is not selected)</code> + <code>P(444 is selected but it replaces 222 or 333)</code>
    - = <code>1/4</code> + <code>3/4</code>*<code>2/3</code> 
    - = <code>3/4</code>
  - The same case with <code>222</code> and <code>333</code>
  - Now all the numbers have the probability of <code>3/4</code> to be picked

### This Problem <Linked List Random Node>
  - This problem is the sp case where <code>k=1</code>

___
P.S. Thanks for @WKVictor for pointing out my mistake!
</p>


### Java Solution with cases explain
- Author: DyXrLxSTAOadoD
- Creation Date: Mon Aug 22 2016 07:15:39 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 08:08:19 GMT+0800 (Singapore Standard Time)

<p>
When I first got this question, I went through some articles, but it is painful for me to understand abstract notations like i, k, m, n, n-1, k+1...

After I read this one: [http://blog.jobbole.com/42550/](http://blog.jobbole.com/42550), it comes with a simple example and I understood suddenly, and write the code by myself. I translate it to English, so more people can benefit from it.

Start...
When we read the first node ```head```, if the stream ```ListNode``` stops here, we can just return the ```head.val```. The possibility is ```1/1```.

When we read the second node, we can decide if we replace the result ```r``` or not. The possibility is ```1/2```. So we just generate a random number between ```0``` and ```1```, and check if it is equal to ```1```. If it is ```1```, replace ```r``` as the value of the current node, otherwise we don't touch ```r```, so its value is still the value of head.

When we read the third node, now the result ```r``` is one of value in the head or second node. We just decide if we replace ```the value of r``` as ```the value of current node(third node)```. The possibility of replacing it is ```1/3```, namely the possibility of we don't touch ```r``` is ```2/3```. So we just generate a random number between ```0 ~ 2```, and if the result is ```2``` we replace ```r```.

We can continue to do like this until the end of stream ```ListNode```.

Here is the Java code:
```
public class Solution {
    
    ListNode head;
    Random random;
    
    public Solution(ListNode h) {
        head = h;       
        random = new Random();        
    }
    
    public int getRandom() {
        
        ListNode c = head;
        int r = c.val;
        for(int i=1;c.next != null;i++){
            
            c = c.next;
            if(random.nextInt(i + 1) == i) r = c.val;                        
        }
        
        return r;
    }
}
```
</p>


### using "Reservoir sampling" O(1) space, O(n) time complexity\uff0cc++
- Author: primbo
- Creation Date: Wed Aug 10 2016 23:57:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 06:04:09 GMT+0800 (Singapore Standard Time)

<p>
according  to the wiki https://en.wikipedia.org/wiki/Reservoir_sampling
here is sudo code for k size reservoir:
```
/*
  S has items to sample, R will contain the result
*/
ReservoirSample(S[1..n], R[1..k])
  // fill the reservoir array
  for i = 1 to k
      R[i] := S[i]

  // replace elements with gradually decreasing probability
  for i = k+1 to n
    j := random(1, i)   // important: inclusive range
    if j <= k
        R[j] := S[i]
```
you need to remember  the range [ 0, i ] should be inclusive.
```
class Solution {
private:
    ListNode* head;
public:
    /** @param head The linked list's head. Note that the head is guanranteed to be not null, so it contains at least one node. */
    Solution(ListNode* head) {
        this->head = head;
    }
    
    /** Returns a random node's value. */
    int getRandom() {
        int res = head->val;
        ListNode* node = head->next;
        int i = 2;
        while(node){
            int j = rand()%i;
            if(j==0)
                res = node->val;
            i++;
            node = node->next;
        }
        return res;
    }
};
```
</p>


