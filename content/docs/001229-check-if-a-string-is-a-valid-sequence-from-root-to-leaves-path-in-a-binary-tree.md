---
title: "Check If a String Is a Valid Sequence from Root to Leaves Path in a Binary Tree"
weight: 1229
#id: "check-if-a-string-is-a-valid-sequence-from-root-to-leaves-path-in-a-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree where each path going from the root to any leaf form a <strong>valid sequence</strong>, check if a given string&nbsp;is a <strong>valid sequence</strong> in such binary tree.&nbsp;</p>

<p>We get the given string from the concatenation of an array of integers <code>arr</code> and the concatenation of all&nbsp;values of the nodes along a path results in a <strong>sequence</strong> in the given binary tree.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/12/18/leetcode_testcase_1.png" style="width: 333px; height: 250px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [0,1,0,0,1,0,null,null,1,0,0], arr = [0,1,0,1]
<strong>Output:</strong> true
<strong>Explanation: 
</strong>The path 0 -&gt; 1 -&gt; 0 -&gt; 1 is a valid sequence (green color in the figure). 
Other valid sequences are: 
0 -&gt; 1 -&gt; 1 -&gt; 0 
0 -&gt; 0 -&gt; 0
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/12/18/leetcode_testcase_2.png" style="width: 333px; height: 250px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [0,1,0,0,1,0,null,null,1,0,0], arr = [0,0,1]
<strong>Output:</strong> false 
<strong>Explanation:</strong> The path 0 -&gt; 0 -&gt; 1 does not exist, therefore it is not even a sequence.
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/12/18/leetcode_testcase_3.png" style="width: 333px; height: 250px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [0,1,0,0,1,0,null,null,1,0,0], arr = [0,1,1]
<strong>Output:</strong> false
<strong>Explanation: </strong>The path 0 -&gt; 1 -&gt; 1 is a sequence, but it is not a valid sequence.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 5000</code></li>
	<li><code>0 &lt;= arr[i] &lt;= 9</code></li>
	<li>Each node&#39;s value is between [0 - 9].</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- 23&me - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] DFS & BFS clean codes w/ brief comments, explanation and analysis.
- Author: rock
- Creation Date: Thu Apr 30 2020 15:59:36 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 01 2020 00:30:50 GMT+0800 (Singapore Standard Time)

<p>
**DFS**
For each depth level, check:
1. If reached the end of the current subtree or the input `arr`, or current node value does NOT match the value of `arr`, then current sequence is NOT valid, return `false`;
2. if current node is a leave, check if the end of `arr` reached, if yes it is a valid sequence, return `true`, otherwise `false`;
3. Other cases, increase the `depth` by `1` and recurse to the children; if both end up with `false`, the result is `false`, otherwise `true`.

```java
    public boolean isValidSequence(TreeNode root, int[] arr) {
        return dfs(root, arr, 0);
    }
    
    private boolean dfs(TreeNode n, int[] a, int depth) {
        if (n == null || depth >= a.length || a[depth] != n.val) { // base cases.
            return false;
        }// key base case: a leave found.
        if (n.left == null && n.right == null) { // credit to @The_Legend_ for making the code clean
            return depth + 1 == a.length; // valid sequence?
        }
        return dfs(n.left, a, depth + 1) || dfs(n.right, a, depth + 1); // recurse to the children.
    }
```

```python
    def isValidSequence(self, root: TreeNode, arr: List[int]) -> bool:
        
        def dfs(node: TreeNode, depth: int) -> bool:
            if node is None or depth >= len(arr) or arr[depth] != node.val:
                return False
            if node.left == node.right == None: # credit to @The_Legend_ for making the code clean
                return depth + 1 == len(arr)
            return dfs(node.left, depth + 1) or dfs(node.right, depth + 1)

        return dfs(root, 0)
```

----
**BFS**
For each `depth` level, if the value of `arr` matches that of node, then
1. if reached the end of the subtree and end of the `arr`, a valid sequence found, return `true`;
2. otherwise, put the children of current node into `Queue`, and continue to the next `depth` level of breadth search.
3. repeat 1 & 2 until a valid sequence found; or no valid one till the end, return `false`.
```java
    public boolean isValidSequence(TreeNode root, int[] arr) {
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        for (int depth = 0; !q.isEmpty() && depth < arr.length; ++depth) { // search depth level control.
            for (int sz = q.size(); sz > 0; --sz) { // search breath control.
                TreeNode n = q.poll(); 
                if (n != null && n.val == arr[depth]) { // a matching node found.
                    if (depth + 1 == arr.length && n.left == null && n.right == null) { // match from root to a leave hence it is a valid sequence.
                        return true;
                    }
                    q.addAll(Arrays.asList(n.left, n.right)); // add into Queue its children for next depth level.
                }
            }
        }
        return false; // No valid sequence.
    }
```
```python
    def isValidSequence(self, root: TreeNode, arr: List[int]) -> bool:
        dq = collections.deque([root])
        for depth, a in enumerate(arr):
            for _ in range(len(dq)):
                node = dq.popleft()
                if node and node.val == a:
                    if depth + 1 == len(arr) and node.left == node.right == None:
                        return True
                    dq.extend(child for child in (node.left, node.right) if child)
        return False
```

**Analysis:**

In worst case, each TreeNode is at most visited once or each `depth` a number in `arr` corresponds to `2` options of TreeNodes, hence

Time: `O(min(2 ^ m, n)) `& space: `O(n)`, where `m = arr.length` and `n` is total number of the nodes in binary tree.
</p>


### [summary] 3 solutions, DFS/BFS, recursion or not
- Author: newRuanXY
- Creation Date: Thu Apr 30 2020 16:16:58 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 30 2020 16:51:48 GMT+0800 (Singapore Standard Time)

<p>
## BFS
```python
def isValidSequence(self, root: TreeNode, arr: List[int]) -> bool:   
        if not root or root.val != arr[0]: return False
        queue = [root]
        for val in arr[1:]:
            new_queue = []
            for node in queue:
                if node.left  and node.left.val  == val: new_queue.append( node.left )
                if node.right and node.right.val == val: new_queue.append( node.right )
            if not new_queue: return False
            queue = new_queue
        return any( not node.left and not node.right for node in queue )
```


## DFS, recursion
```python
def isValidSequence(self, root: TreeNode, arr: List[int]) -> bool:   
        N = len(arr)
        
        def dfs(node, index):
            if node and node.val == arr[index]:
                index += 1
                if index == N:
                    return not node.left and not node.right
                else:
                    return dfs(node.left, index) or dfs(node.right, index)
            return False    
            
        return dfs(root, 0)
```

## DFS, no recursion
```python
    def isValidSequence(self, root: TreeNode, arr: List[int]) -> bool:
        
        if root.val != arr[0]:
            return False
        stack, index = [ None, root], 1   # need None so that stack[-1] doesn\'t IndexError
        while index < len(arr):
            last = stack[-1]
            if last.left and arr[index] == last.left.val:
                stack.append( last.left )
                index += 1
            elif last.right and arr[index] == last.right.val:
                stack.append( last.right )
                index += 1
            else:
                index, prev, last = index-1, stack.pop(), stack[-1]
                while index and ( prev == last.right or not last.right or arr[index] != last.right.val ):
                    index, prev, last = index-1, stack.pop(), stack[-1]
                if not index:
                    return False
                stack.append(last.right)
                index += 1
                
                
        last = stack[-1]
        return not last.left and not last.right
```


</p>


### Python DFS
- Author: WangQiuc
- Creation Date: Thu Apr 30 2020 16:12:54 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 30 2020 16:12:54 GMT+0800 (Singapore Standard Time)

<p>
If we find any one valid path, we return True. So dfs recursion should be:
```
# dfs(i, node)
if node.val == arr[i]: 
    return dfs(i+1, node.left) or dfs(i+1, node.right)
```

Then we just need to figure out two base cases:

`return True`
We reach the end of arr and we reach a left node. And that `arr[-1] == node.val`, which gives us:
```
if i == n - 1 and not (node.left or node.right):
    return True
```

`return False`
When `arr[i] != node.val`, that path is invalid and there is no need to dfs on that path any more. 
Or we finish iterating either the entire `arr` , or one of the tree path. But not **simultaneously**.
And we don\'t have to determine that **simultaneousness** because if it is, it will be caught in the previous dfs and return `True` or `False`.
```
if not node or i == n or arr[i] != node.val:
    return False
```

And here is the code:
```
def isValidSequence(root, arr):
    n = len(arr)
    def dfs(i, node):
        if not node or i == n or arr[i] != node.val:
            return False
        if i == n - 1 and not (node.left or node.right):
            return True
        return dfs(i+1, node.left) or dfs(i+1, node.right)
    return dfs(0, root)
```

</p>


