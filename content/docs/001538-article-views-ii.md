---
title: "Article Views II"
weight: 1538
#id: "article-views-ii"
---
## Description
<div class="description">
<p>Table: <code>Views</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| article_id    | int     |
| author_id     | int     |
| viewer_id     | int     |
| view_date     | date    |
+---------------+---------+
There is no primary key for this table, it may have duplicate rows.
Each row of this table indicates that some viewer viewed an article (written by some author) on some date. 
Note that equal author_id and viewer_id indicate the same person.</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find all the people who viewed more than one article on the same date, sorted in ascending order by their id.</p>

<p>The query result format is in the following example:</p>

<pre>
Views table:
+------------+-----------+-----------+------------+
| article_id | author_id | viewer_id | view_date  |
+------------+-----------+-----------+------------+
| 1          | 3         | 5         | 2019-08-01 |
| 3          | 4         | 5         | 2019-08-01 |
| 1          | 3         | 6         | 2019-08-02 |
| 2          | 7         | 7         | 2019-08-01 |
| 2          | 7         | 6         | 2019-08-02 |
| 4          | 7         | 1         | 2019-07-22 |
| 3          | 4         | 4         | 2019-07-21 |
| 3          | 4         | 4         | 2019-07-21 |
+------------+-----------+-----------+------------+

Result table:
+------+
| id   |
+------+
| 5    |
| 6    |
+------+</pre>

</div>

## Tags


## Companies
- LinkedIn - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Very simple MYSQL, no JOIN
- Author: katie_hou
- Creation Date: Mon Sep 02 2019 23:34:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 14 2020 01:40:10 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT DISTINCT viewer_id AS id
FROM Views
GROUP BY viewer_id, view_date
HAVING COUNT(DISTINCT article_id) > 1
ORDER BY 1
```
</p>


### Easy MySQL Solutions with Explanation
- Author: Morganmm
- Creation Date: Sun Oct 06 2019 03:54:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 03:55:00 GMT+0800 (Singapore Standard Time)

<p>
1. Using subquery
```
SELECT t.viewer_id AS id
FROM (SELECT DISTINCT * FROM Views)t
GROUP BY t.viewer_id 
HAVING COUNT(DISTINCT t.view_date)!= COUNT(t.view_date)
ORDER BY t.viewer_id
```
2. Group by on two columns of viewer_id and view_date to exclude duplicate rows in dataset

```
SELECT DISTINCT viewer_id AS id
FROM Views
GROUP BY viewer_id, view_date
HAVING COUNT(DISTINCT article_id) > 1
```
</p>


### MSSQL Self Join
- Author: EmmaZelda
- Creation Date: Tue Aug 20 2019 22:53:48 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 20 2019 22:53:48 GMT+0800 (Singapore Standard Time)

<p>
SELECT DISTINCT v1.viewer_id AS id
FROM views v1
JOIN views v2
ON v1.viewer_id = v2.viewer_id
AND v1.view_date = v2.view_date
AND v1.article_id != v2.article_id
ORDER BY 1;
</p>


