---
title: "Find First and Last Position of Element in Sorted Array"
weight: 34
#id: "find-first-and-last-position-of-element-in-sorted-array"
---
## Description
<div class="description">
<p>Given an array of integers <code>nums</code> sorted in ascending order, find the starting and ending position of a given <code>target</code> value.</p>

<p>If <code>target</code> is not found in the array, return <code>[-1, -1]</code>.</p>

<p><strong>Follow up:</strong>&nbsp;Could you write an algorithm with&nbsp;<code>O(log n)</code> runtime complexity?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> nums = [5,7,7,8,8,10], target = 8
<strong>Output:</strong> [3,4]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> nums = [5,7,7,8,8,10], target = 6
<strong>Output:</strong> [-1,-1]
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> nums = [], target = 0
<strong>Output:</strong> [-1,-1]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= nums.length &lt;= 10<sup>5</sup></code></li>
	<li><code>-10<sup>9</sup>&nbsp;&lt;= nums[i]&nbsp;&lt;= 10<sup>9</sup></code></li>
	<li><code>nums</code> is a non-decreasing array.</li>
	<li><code>-10<sup>9</sup>&nbsp;&lt;= target&nbsp;&lt;= 10<sup>9</sup></code></li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Facebook - 38 (taggedByAdmin: false)
- Amazon - 10 (taggedByAdmin: false)
- Microsoft - 7 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- LinkedIn - 6 (taggedByAdmin: true)
- Oracle - 5 (taggedByAdmin: false)
- Netflix - 2 (taggedByAdmin: false)
- Quip (Salesforce) - 2 (taggedByAdmin: false)
- Yahoo - 3 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Airbnb - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach 1: Linear Scan

**Intuition**

Checking every index for `target` exhausts the search space, so it must work.

**Algorithm**

First, we do a linear scan of `nums` from the left, `break`ing when we find
an instance of `target`. If we never `break`, then `target` is not present,
so we can return the "error code" of `[-1, -1]` early. Given that we did find
a valid left index, we can do a second linear scan, but this time from the
right. In this case, the first instance of `target` encountered will be the
rightmost one (and because a leftmost one exists, there is guaranteed to also
be a rightmost one). We then simply return a list containing the two located
indices.

<iframe src="https://leetcode.com/playground/5UNt5qo6/shared" frameBorder="0" width="100%" height="500" name="5UNt5qo6"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$

    This brute-force approach examines each of the `n` elements of `nums` exactly twice, so the overall runtime is linear.

* Space complexity : $$O(1)$$

    The linear scan method allocates a fixed-size array and a few integers, so it has a constant-size memory footprint.

<br />

---

#### Approach 2: Binary Search

**Intuition**

Because the array is sorted, we can use binary search to locate the left
and rightmost indices.

**Algorithm**

The overall algorithm works fairly similarly to the linear scan approach,
except for the subroutine used to find the left and rightmost indices
themselves. Here, we use a modified binary search to search a sorted array,
with a few minor adjustments. First, because we are locating the leftmost (or
rightmost) index containing `target` (rather than returning `true` iff we
find `target`), the algorithm does not terminate as soon as we find a match.
Instead, we continue to search until `lo == hi` and they contain some index
at which `target` can be found.

The other change is the introduction of the `left` parameter, which is a
boolean indicating what to do in the event that `target == nums[mid]`; if
`left` is `true`, then we "recurse" on the left subarray on ties. Otherwise,
we go right. To see why this is correct, consider the situation where we find
`target` at index `i`. The leftmost `target` cannot occur at any index
greater than `i`, so we never need to consider the right subarray. The same
argument applies to the rightmost index.

The first animation below shows the process for finding the leftmost index,
and the second shows the process for finding the index right of the rightmost
index.

!?!../Documents/34_Search_for_a_Range_left.json:1280,720!?!

!?!../Documents/34_Search_for_a_Range_right.json:1280,720!?!

<iframe src="https://leetcode.com/playground/5m6TUvmH/shared" frameBorder="0" width="100%" height="500" name="5m6TUvmH"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(\log N)$$

    Here we invoke binary search twice. 
    Let's compute time complexity with the help of 
    [master theorem](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)) 
    $$T(N) = aT\left(\frac{N}{b}\right) + \Theta(N^d)$$.
    The equation represents dividing the problem 
    up into $$a$$ subproblems of size $$\frac{N}{b}$$ in $$\Theta(N^d)$$ time. 
    Here at step there is only one subproblem `a = 1`, its size 
    is a half of the initial problem `b = 2`, 
    and all this happens in a constant time `d = 0`.
    That means that $$\log_b{a} = d$$ and hence we're dealing with 
    [case 2](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)#Case_2_example)
    that results in $$\mathcal{O}(n^{\log_b{a}} \log^{d + 1} N)$$
    = $$\mathcal{O}(\log N)$$ time complexity.

* Space complexity : $$\mathcal{O}(1)$$

    All work is done in place, so the overall memory usage is constant.

## Accepted Submission (python3)
```python3
class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        lenn = len(nums)
        if lenn == 0:
            return [-1, -1]
        front = back = 0
        left = 0
        right = lenn - 1
        while left < right:
            m = (left + right) // 2
            mv = nums[m]
            if mv < target:
                left = m + 1
            else:
                right = m
        if nums[left] != target:
            return [-1, -1]
        front = left
        right = lenn - 1 # reset right only
        while left < right:
            m = (left + right + 1) // 2
            mv = nums[m]
            if mv <= target:
                left = m
            else:
                right = m - 1
        back = right
        return [front, back]
```

## Top Discussions
### Clean iterative solution with two binary searches (with explanation)
- Author: stellari
- Creation Date: Thu Dec 11 2014 11:24:44 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 14:18:20 GMT+0800 (Singapore Standard Time)

<p>
The problem can be simply broken down as two binary searches for the begining and end of the range, respectively:

First let's find the left boundary of the range. We initialize the range to [i=0, j=n-1]. In each step, calculate the middle element [mid = (i+j)/2]. Now according to the relative value of A[mid] to target, there are three possibilities:

   1. If A[mid] < target, then the range must begins on the ***right*** of mid (hence i = mid+1 for the next iteration)
   2. If A[mid] > target, it means the range must begins on the ***left*** of mid (j = mid-1)
   3. If A[mid] = target, then the range must begins ***on the left of or at*** mid  (j= mid)

Since we would move the search range to the same side for case 2 and 3, we might as well merge them as one single case so that less code is needed:

   2*. If A[mid] >= target, j = mid;

Surprisingly, 1 and 2* are the only logic you need to put in loop while (i < j). When the while loop terminates, the value of i/j is where the start of the range is. Why? 

No matter what the sequence originally is, as we narrow down the search range, eventually we will be at a situation where there are only two elements in the search range. Suppose our target is 5, then we have only 7 possible cases:

    case 1: [5 7] (A[i] = target < A[j])
	case 2: [5 3] (A[i] = target > A[j])
    case 3: [5 5] (A[i] = target = A[j])
    case 4: [3 5] (A[j] = target > A[i])
    case 5: [3 7] (A[i] < target < A[j])
	case 6: [3 4] (A[i] < A[j] < target)
	case 7: [6 7] (target < A[i] < A[j])

For case 1, 2 and 3, if we follow the above rule, since mid = i => A[mid] = target in these cases, then we would set j = mid. Now the loop terminates and i and j both point to the first 5.  

For case 4, since A[mid] < target, then set i = mid+1. The loop terminates and both i and j point to 5.

For all other cases, by the time the loop terminates, A[i] is not equal to 5. So we can easily know 5 is not in the sequence if the comparison fails.

In conclusion, when the loop terminates, if A[i]==target, then i is the left boundary of the range; otherwise, just return -1;

For the right of the range, we can use a similar idea. Again we can come up with several rules:

   1. If A[mid] > target, then the range must begins on the ***left*** of mid (j = mid-1)
   2. If A[mid] < target, then the range must begins on the ***right*** of mid (hence i = mid+1 for the next iteration)
   3. If A[mid] = target, then the range must begins ***on the right of or at*** mid  (i= mid) 

Again, we can merge condition 2 and 3 into:

    2* If A[mid] <= target, then i = mid;

However, the terminate condition on longer works this time. Consider the following case:

    [5 7], target = 5
	
Now A[mid] = 5, then according to rule 2, we set i = mid. This practically does nothing because i is already equal to mid. As a result, the search range is not moved at all!

The solution is by using a small trick: instead of calculating mid as mid = (i+j)/2, we now do:

    mid = (i+j)/2+1
	
Why does this trick work? When we use mid = (i+j)/2, the mid is rounded to the lowest integer. In other words, mid is always *biased* towards the left. This means we could have i == mid when j - i == mid, but we NEVER have j == mid. So in order to keep the search range moving, you must make sure the new i is set to something different than mid, otherwise we are at the risk that i gets stuck. But for the new j, it is okay if we set it to mid, since it was not equal to mid anyways. Our two rules in search of the left boundary happen to satisfy these requirements, so it works perfectly in that situation. Similarly, when we search for the right boundary, we must make sure i won't get stuck when we set the new i to i = mid. The easiest way to achieve this is by making mid *biased* to the right, i.e. mid = (i+j)/2+1.

All this reasoning boils down to the following simple code:

    vector<int> searchRange(int A[], int n, int target) {
        int i = 0, j = n - 1;
        vector<int> ret(2, -1);
        // Search for the left one
        while (i < j)
        {
            int mid = (i + j) /2;
            if (A[mid] < target) i = mid + 1;
            else j = mid;
        }
        if (A[i]!=target) return ret;
        else ret[0] = i;
        
        // Search for the right one
        j = n-1;  // We don't have to set i to 0 the second time.
        while (i < j)
        {
            int mid = (i + j) /2 + 1;	// Make mid biased to the right
            if (A[mid] > target) j = mid - 1;  
            else i = mid;				// So that this won't make the search range stuck.
        }
        ret[1] = j;
        return ret; 
    }
</p>


### A very simple Java solution, with only one binary search algorithm
- Author: vision57
- Creation Date: Wed Dec 24 2014 20:33:25 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 17:23:42 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
    	public int[] searchRange(int[] A, int target) {
    		int start = Solution.firstGreaterEqual(A, target);
    		if (start == A.length || A[start] != target) {
    			return new int[]{-1, -1};
    		}
    		return new int[]{start, Solution.firstGreaterEqual(A, target + 1) - 1};
    	}
    
    	//find the first number that is greater than or equal to target.
    	//could return A.length if target is greater than A[A.length-1].
    	//actually this is the same as lower_bound in C++ STL.
    	private static int firstGreaterEqual(int[] A, int target) {
    		int low = 0, high = A.length;
    		while (low < high) {
    			int mid = low + ((high - low) >> 1);
    			//low <= mid < high
    			if (A[mid] < target) {
    				low = mid + 1;
    			} else {
    				//should not be mid-1 when A[mid]==target.
    				//could be mid even if A[mid]>target because mid<high.
    				high = mid;
    			}
    		}
    		return low;
    	}
    }
</p>


### Easy java O(logn) solution
- Author: atwenbobu
- Creation Date: Wed Aug 19 2015 03:33:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 04:52:21 GMT+0800 (Singapore Standard Time)

<p>
There must be two indices in the array. Which means, we can just simply apply to binary search twice to find each index of the target element.


    public class Solution {
    public int[] searchRange(int[] nums, int target) {
        int[] result = new int[2];
        result[0] = findFirst(nums, target);
        result[1] = findLast(nums, target);
        return result;
    }
    
    private int findFirst(int[] nums, int target){
        int idx = -1;
        int start = 0;
        int end = nums.length - 1;
        while(start <= end){
            int mid = (start + end) / 2;
            if(nums[mid] >= target){
                end = mid - 1;
            }else{
                start = mid + 1;
            }
            if(nums[mid] == target) idx = mid;
        }
        return idx;
    }
    
    private int findLast(int[] nums, int target){
        int idx = -1;
        int start = 0;
        int end = nums.length - 1;
        while(start <= end){
            int mid = (start + end) / 2;
            if(nums[mid] <= target){
                start = mid + 1;
            }else{
                end = mid - 1;
            }
            if(nums[mid] == target) idx = mid;
        }
        return idx;
    }
}
</p>


