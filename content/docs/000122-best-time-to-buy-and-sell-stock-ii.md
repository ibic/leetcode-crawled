---
title: "Best Time to Buy and Sell Stock II"
weight: 122
#id: "best-time-to-buy-and-sell-stock-ii"
---
## Description
<div class="description">
<p>Say you have an array <code>prices</code> for which the <em>i</em><sup>th</sup> element is the price of a given stock on day <em>i</em>.</p>

<p>Design an algorithm to find the maximum profit. You may complete as many transactions as you like (i.e., buy one and sell one share of the stock multiple times).</p>

<p><strong>Note:</strong> You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> [7,1,5,3,6,4]
<strong>Output:</strong> 7
<strong>Explanation:</strong> Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
&nbsp;            Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> [1,2,3,4,5]
<strong>Output:</strong> 4
<strong>Explanation:</strong> Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
&nbsp;            Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are
&nbsp;            engaging multiple transactions at the same time. You must sell before buying again.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> [7,6,4,3,1]
<strong>Output:</strong> 0
<strong>Explanation:</strong> In this case, no transaction is done, i.e. max profit = 0.</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= prices.length &lt;= 3 * 10 ^ 4</code></li>
	<li><code>0 &lt;= prices[i]&nbsp;&lt;= 10 ^ 4</code></li>
</ul>

</div>

## Tags
- Array (array)
- Greedy (greedy)

## Companies
- Amazon - 12 (taggedByAdmin: false)
- Facebook - 7 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: true)
- Morgan Stanley - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Goldman Sachs - 4 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Intuit - 2 (taggedByAdmin: false)
- Uber - 8 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary

We have to determine the maximum profit that can be obtained by making the transactions (no limit on the number of transactions done). For this we need to find out those sets of buying and selling prices which together lead to the maximization of profit.

## Solution
---

#### Approach 1: Brute Force

In this case, we simply calculate the profit corresponding to all the possible sets of transactions and find out the maximum profit out of them.

<iframe src="https://leetcode.com/playground/DQfAGPiL/shared" frameBorder="0" width="100%" height="463" name="DQfAGPiL"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^n)$$. Recursive function is called $$n^n$$ times.

* Space complexity : $$O(n)$$. Depth of recursion is $$n$$.
<br />
<br />
---
#### Approach 2: Peak Valley Approach

**Algorithm**

Say the given array is:

[7, 1, 5, 3, 6, 4].

If we plot the numbers of the given array on a graph, we get:

![Profit Graph](https://leetcode.com/media/original_images/122_maxprofit_1.PNG){:width="539px"}
{:align="center"}


If we analyze the graph, we notice that the points of interest are the consecutive valleys and peaks.

Mathematically speaking:
$$
Total Profit= \sum_{i}(height(peak_i)-height(valley_i))
$$

The key point is we need to consider every peak immediately following a valley to maximize the profit. In case we skip one of the peaks (trying to obtain more profit), we will end up losing the profit over one of the transactions leading to an overall lesser profit.

For example, in the above case, if we skip $$peak_i$$ and $$valley_j$$ trying to obtain more profit by considering points with more difference in heights, the net profit obtained will always be lesser than the one obtained by including them, since $$C$$ will always be lesser than $$A+B$$.


<iframe src="https://leetcode.com/playground/Yrs8F9na/shared" frameBorder="0" width="100%" height="361" name="Yrs8F9na"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Single pass.

* Space complexity : $$O(1)$$. Constant space required.
<br />
<br />
---
#### Approach 3: Simple One Pass

**Algorithm**

This solution follows the logic used in [Approach 2](#approach-2-peak-valley-approach) itself, but with only a slight variation. In this case, instead of looking for every peak following a valley, we can simply go on crawling over the slope and keep on adding the profit obtained from every consecutive transaction. In the end,we will be using the peaks and valleys effectively, but we need not track the costs corresponding to the peaks and valleys along with the maximum profit, but we can directly keep on adding the difference between the consecutive numbers of the array if the second number is larger than the first one, and at the total sum we obtain will be the maximum profit. This approach will simplify the solution.
This can be made clearer by taking this example:

[1, 7, 2, 3, 6, 7, 6, 7]


The graph corresponding to this array is:

![Profit Graph](https://leetcode.com/media/original_images/122_maxprofit_2.PNG){:width="539px"}
{:align="center"}


From the above graph, we can observe that the sum $$A+B+C$$ is equal to the difference $$D$$ corresponding to the difference between the heights of the consecutive peak and valley.

<iframe src="https://leetcode.com/playground/ppWUGTaj/shared" frameBorder="0" width="100%" height="225" name="ppWUGTaj"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Single pass.

* Space complexity: $$O(1)$$. Constant space needed.

## Accepted Submission (python3)
```python3
class Solution:
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        r = 0
        lenp = len(prices)
        for i in range(1, lenp):
            price = prices[i]
            prePrice = prices[i - 1]
            if price > prePrice:
                r += price - prePrice
        return r

```

## Top Discussions
### Explanation for the dummy like me.
- Author: chipbk10
- Creation Date: Sun Dec 23 2018 07:03:53 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 07 2020 14:18:29 GMT+0800 (Singapore Standard Time)

<p>
The profit is the sum of sub-profits. Each sub-profit is the difference between selling at day ```j```, and buying at day ```i``` (with ```j > i```). The range ```[i, j]``` should be chosen so that the sub-profit is **maximum**:

```sub-profit = prices[j] - prices[i]```

We should choose ```j``` that ```prices[j]``` is as **big** as possible, and choose ```i``` that ```prices[i]``` is as **small** as possible (of course in their local range).

Let\'s say, we have a range ```[3, 2, 5]```, we will choose ```(2,5)``` instead of ```(3,5)```, because ```2<3```.
Now, if we add ```8``` into this range: ```[3, 2, 5, 8]```, we will choose ```(2, 8)``` instead of ```(2,5)``` because ```8>5```.

From this observation, from day ```X```, the buying day will be the last **continuous** day that the price is smallest. Then, the selling day will be the last **continuous** day that the price is biggest.

Take another range ```[3, 2, 5, 8, 1, 9]```, though ```1``` is the smallest, but ```2``` is chosen, because ```2``` is the smallest in a consecutive decreasing prices starting from ```3```.
Similarly, ```9``` is the biggest, but ```8``` is chosen, because ```8``` is the biggest in a consecutive increasing prices starting from ```2``` (the buying price). 
Actually, the range ```[3, 2, 5, 8, 1, 9]``` will be splitted into 2 sub-ranges ```[3, 2, 5, 8]``` and ```[1, 9]```.

We come up with the following code:
```
public int maxProfit(int[] prices) {
        int i = 0, buy, sell, profit = 0, N = prices.length - 1;
        while (i < N) {
            while (i < N && prices[i + 1] <= prices[i]) i++;
            buy = prices[i];

            while (i < N && prices[i + 1] > prices[i]) i++;
            sell = prices[i];

            profit += sell - buy;
        }
        return profit;
}
```

The time complexity is ```O(N)```, space complexity is ```O(5)```

### **BONUS**: 
With this approach, we still can calculate on which buying days and selling days, we reach the max profit:

```
public Pair<List<Pair<Integer, Integer>>, Integer> buysAndSells(int[] prices) {
        int i = 0, iBuy, iSell, profit = 0, N = prices.length - 1;
        List<Pair<Integer, Integer>> buysAndSells = new ArrayList<Pair<Integer, Integer>>();
        while (i < N) {
            while (i < N && prices[i + 1] <= prices[i]) i++;
            iBuy = i;

            while (i < N && prices[i + 1] > prices[i]) i++;
            iSell = i;

            profit += prices[iSell] - prices[iBuy];
            Pair<Integer, Integer> bs = new Pair<Integer, Integer>(iBuy, iSell);
            buysAndSells.add(bs);
        }
        return new Pair<List<Pair<Integer, Integer>>, Integer>(buysAndSells, profit);
}
	
public class Pair<T1, T2> {
    public T1 fst;
    public T2 snd;
    public Pair(T1 f, T2 s) {
        fst = f;
        snd = s;
    }
}
```

P.S: After a long time, this problem came back to me again, and I forgot what I solved :) . I came up with [different approach](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/discuss/564729/Java-Simple-Code-DP)
</p>


### Three lines in C++, with explanation
- Author: summerrainet2008
- Creation Date: Fri Jun 26 2015 00:11:39 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 20 2018 01:01:03 GMT+0800 (Singapore Standard Time)

<p>
  First we post the code here.

    int maxProfit(vector<int> &prices) {
        int ret = 0;
        for (size_t p = 1; p < prices.size(); ++p) 
          ret += max(prices[p] - prices[p - 1], 0);    
        return ret;
    }

Second, suppose the first sequence is "a <= b <= c <= d", the profit is "d - a = (b - a) + (c - b) + (d - c)" without a doubt.  And suppose another one is "a <= b >= b' <= c <= d", the profit is not difficult to be figured out as "(b - a) + (d - b')". So you just target at monotone sequences.
</p>


### Java O(n) solution if we're not greedy
- Author: shpolsky
- Creation Date: Fri Jan 16 2015 22:47:32 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 10:08:18 GMT+0800 (Singapore Standard Time)

<p>
Hi guys!

The greedy pair-wise approach mentioned in other posts is great for this problem indeed, but if we're not allowed to buy and sell stocks within the same day it can't be applied (logically, of course; the answer will be the same). Actually, the straight-forward way of finding next local minimum and next local maximum is not much more complicated, so, just for the sake of having an alternative I share the code in Java for such case.

    public int maxProfit(int[] prices) {
        int profit = 0, i = 0;
        while (i < prices.length) {
            // find next local minimum
            while (i < prices.length-1 && prices[i+1] <= prices[i]) i++;
            int min = prices[i++]; // need increment to avoid infinite loop for "[1]"
            // find next local maximum
            while (i < prices.length-1 && prices[i+1] >= prices[i]) i++;
            profit += i < prices.length ? prices[i++] - min : 0;
        }
        return profit;
    }

Happy coding!
</p>


