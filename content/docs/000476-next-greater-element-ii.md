---
title: "Next Greater Element II"
weight: 476
#id: "next-greater-element-ii"
---
## Description
<div class="description">
<p>
Given a circular array (the next element of the last element is the first element of the array), print the Next Greater Number for every element. The Next Greater Number of a number x is the first greater number to its traversing-order next in the array, which means you could search circularly to find its next greater number. If it doesn't exist, output -1 for this number.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [1,2,1]
<b>Output:</b> [2,-1,2]
<b>Explanation:</b> The first 1's next greater number is 2; </br>The number 2 can't find next greater number; </br>The second 1's next greater number needs to search circularly, which is also 2.
</pre>
</p>

<p><b>Note:</b>
The length of given array won't exceed 10000.
</p>
</div>

## Tags
- Stack (stack)

## Companies
- Amazon - 9 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force (using Double Length Array) [Time Limit Exceeded]

 In this method, we make use of an array $$doublenums$$ which is formed by concatenating two copies of the given $$nums$$ array one after the other. Now, when we need to find out the next greater element for $$nums[i]$$, we can simply scan all the elements $$doublenums[j]$$, such that $$i < j < length(doublenums)$$. The first element found satisfying the given condition is the required result for $$nums[i]$$. If no such element is found, we put a $$\text{-1}$$ at the appropriate position in the $$res$$ array.

 
<iframe src="https://leetcode.com/playground/tRcR8Lx3/shared" frameBorder="0" name="tRcR8Lx3" width="100%" height="377"></iframe>

 **Complexity Analysis**

 * Time complexity : $$O(n^2)$$. The complete $$doublenums$$ array(of size $$\text{2n}$$) is scanned for all the elements of $$nums$$ in the worst case.

 * Space complexity : $$O(n)$$. $$doublenums$$ array of size $$\text{2n}$$ is used. $$res$$ array of size $$\text{n}$$ is used.


---

#### Approach #2 Better Brute Force [Accepted]

Instead of making a double length copy of $$nums$$ array , we can traverse circularly in the $$nums$$ array by making use of the $$\text{%(modulus)}$$ operator. For every element $$nums[i]$$, we start searching in the $$nums$$ array(of length $$n$$) from the index $$(i+1)%n$$ and look at the next(cicularly) $$n-1$$ elements. For $$nums[i]$$ we do so by scanning over $$nums[j]$$, such that
$$(i+1)%n &leq; j &leq; (i+(n-1))%n$$, and we look for the first greater element found. If no such element is found, we put a $$\text{-1}$$ at the appropriate position in the $$res$$ array.

 
<iframe src="https://leetcode.com/playground/LCG759JD/shared" frameBorder="0" name="LCG759JD" width="100%" height="309"></iframe>

 **Complexity Analysis**

 * Time complexity : $$O(n^2)$$. The complete $$nums$$ array of size $$n$$ is scanned for all the elements of $$nums$$ in the worst case.

 * Space complexity : $$O(n)$$. $$res$$ array of size $$n$$ is used.

---

#### Approach #3 Using Stack [Accepted]

This approach makes use of a stack. This stack stores the indices of the appropriate elements from $$nums$$ array.  The top of the stack refers to the index of the Next Greater Element found so far. We store the indices instead of the elements since there could be duplicates in the $$nums$$ array. The description of the method will make the above statement clearer.

We start traversing the $$nums$$ array from right towards the left. For an element $$nums[i]$$ encountered, we pop all the elements
$$stack[top]$$ from the stack such that $$nums\big[stack[top]\big] &le; nums[i]$$. We continue the popping till we encounter a $$stack[top]$$ satisfying $$nums\big[stack[top]\big] > nums[i]$$. Now, it is obvious that the current $$stack[top]$$ only can act as the
Next Greater Element for $$nums[i]$$(right now, considering only the elements lying to the right of $$nums[i]$$).

If no element remains on the top of the stack, it means no larger element than $$nums[i]$$ exists to its right. Along with this, we also push the index of the element just encountered($$nums[i]$$), i.e. $$i$$ over the top of the stack, so that $$nums[i]$$(or $$stack[top$$) now acts as the Next Greater Element for the elements lying to its left.

We go through two such passes over the complete $$nums$$ array. This is done so as to complete a circular traversal over the $$nums$$ array. The first pass could make some wrong entries in the $$res$$ array since it considers only the elements lying to the right of $$nums[i]$$, without a circular traversal. But, these entries are corrected in the second pass.  

Further, to ensure the correctness of the method, let's look at the following cases.

Assume that $$nums[j]$$ is the correct Next Greater Element for $$nums[i]$$, such that $$i < j &le; stack[top]$$. Now, whenever we encounter $$nums[j]$$, if $$nums[j] > nums\big[stack[top]\big]$$, it would have already popped the previous $$stack[top]$$ and $$j$$ would have become the topmost element. On the other hand, if  $$nums[j] < nums\big[stack[top]\big]$$, it would have become the topmost element by being pushed above the previous $$stack[top]$$. In both the cases, if $$nums[j] > nums[i]$$, it will be correctly determined to be the Next Greater Element.

The following example makes the procedure clear:

<!--![Next_Greater_Element_II](../Figures/503_Next_Greater_Element_II.gif)-->
!?!../Documents/503_Next_Greater2.json:1000,563!?!

As the animation above depicts, after the first pass, there are a number of wrong entries(marked as $$\text{-1}$$) in the $$res$$ array, because only the elements lying to the corresponding right(non-circular) have been considered till now. But, after the second pass, the correct values are substituted.



 
<iframe src="https://leetcode.com/playground/in37fqRd/shared" frameBorder="0" name="in37fqRd" width="100%" height="309"></iframe>

 **Complexity Analysis**

 * Time complexity : $$O(n)$$. Only two traversals of the $$nums$$ array are done. Further, atmost $$\text{2n}$$ elements are pushed and popped from the stack.

 * Space complexity : $$O(n)$$. A stack of size $$n$$ is used. $$res$$ array of size $$n$$ is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Loop Twice
- Author: lee215
- Creation Date: Wed Mar 01 2017 06:32:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 26 2020 22:58:33 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Loop once, we can get the Next Greater Number of a normal array.
Loop twice, we can get the Next Greater Number of a circular array
<br>

## **Complexity**
Time `O(N)` for one pass
Spce `O(N)` in worst case
<br>

**Java**
```java
    public int[] nextGreaterElements(int[] A) {
        int n = A.length, res[] = new int[n];
        Arrays.fill(res, -1);
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < n * 2; i++) {
            while (!stack.isEmpty() && A[stack.peek()] < A[i % n])
                res[stack.pop()] = A[i % n];
            stack.push(i % n);
        }
        return res;
    }
```
**C++:**
```cpp
    vector<int> nextGreaterElements(vector<int>& A) {
        int n = A.size();
        vector<int> stack, res(n, -1);
        for (int i = 0; i < n * 2; ++i) {
            while (stack.size() && A[stack.back()] < A[i % n]) {
                res[stack.back()] = A[i % n];
                stack.pop_back();
            }
            stack.push_back(i % n);
        }
        return res;
    }
```

**Python:**
```py
    def nextGreaterElements(self, A):
        stack, res = [], [-1] * len(A)
        for i in range(len(A)) * 2:
            while stack and (A[stack[-1]] < A[i]):
                res[stack.pop()] = A[i]
            stack.append(i)
        return res
```
<br>

# More Good Stack Problems
Here are some problems that impressed me.
Good luck and have fun.

[1130. Minimum Cost Tree From Leaf Values](https://leetcode.com/problems/minimum-cost-tree-from-leaf-values/discuss/339959/One-Pass-O(N)-Time-and-Space)
[907. Sum of Subarray Minimums](https://leetcode.com/problems/sum-of-subarray-minimums/discuss/170750/C++JavaPython-Stack-Solution)
[901. Online Stock Span](https://leetcode.com/problems/online-stock-span/discuss/168311/C++JavaPython-O(1))
[856. Score of Parentheses](https://leetcode.com/problems/score-of-parentheses/discuss/141777/C++JavaPython-O(1)-Space)
[503. Next Greater Element II](https://leetcode.com/problems/next-greater-element-ii/discuss/98270/JavaC++Python-Loop-Twice)
496. Next Greater Element I
84. Largest Rectangle in Histogram
42. Trapping Rain Water

</p>


### Java 10 lines and C++ 12 lines linear time complexity O(n) with explanation
- Author: yuxiangmusic
- Creation Date: Sun Feb 05 2017 13:59:54 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 13:04:25 GMT+0800 (Singapore Standard Time)

<p>
The approach is same as *Next Greater Element I*
See explanation in [my solution to the previous problem](https://discuss.leetcode.com/topic/77916/java-10-lines-linear-time-complexity-o-n-with-explanation)
The only difference here is that we use ```stack``` to keep the **indexes** of the **decreasing** subsequence

**Java**
```
    public int[] nextGreaterElements(int[] nums) {
        int n = nums.length, next[] = new int[n];
        Arrays.fill(next, -1);
        Stack<Integer> stack = new Stack<>(); // index stack
        for (int i = 0; i < n * 2; i++) {
            int num = nums[i % n]; 
            while (!stack.isEmpty() && nums[stack.peek()] < num)
                next[stack.pop()] = num;
            if (i < n) stack.push(i);
        }   
        return next;
    }
```
**C++**
```
    vector<int> nextGreaterElements(vector<int>& nums) {
        int n = nums.size();
        vector<int> next(n, -1);
        stack<int> s; // index stack
        for (int i = 0; i < n * 2; i++) {
            int num = nums[i % n]; 
            while (!s.empty() && nums[s.top()] < num) {
                next[s.top()] = num;
                s.pop();
            }
            if (i < n) s.push(i);
        }   
        return next;
    }
```
</p>


### Typical ways to solve circular array problems. Java solution.
- Author: shawngao
- Creation Date: Sun Feb 05 2017 12:15:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 13:19:14 GMT+0800 (Singapore Standard Time)

<p>
The first typical way to solve circular array problems is to extend the original array to twice length, 2nd half has the same element as first half. Then everything become simple.
Naive by simple solution, just look for the next greater element directly. Time complexity: O(n^2).
```
public class Solution {
    public int[] nextGreaterElements(int[] nums) {
        int max = Integer.MIN_VALUE;
        for (int num : nums) {
            max = Math.max(max, num);
        }
        
        int n = nums.length;
        int[] result = new int[n];
        int[] temp = new int[n * 2];
        
        for (int i = 0; i < n * 2; i++) {
            temp[i] = nums[i % n];
        }
        
        for (int i = 0; i < n; i++) {
            result[i] = -1;
            if (nums[i] == max) continue;
            
            for (int j = i + 1; j < n * 2; j++) {
                if (temp[j] > nums[i]) {
                    result[i] = temp[j];
                    break;
                }
            }
        }
        
        return result;
    }
}
```

The second way is to use a ```stack``` to facilitate the look up. First we put all indexes into the stack, ```smaller``` index on the ```top```. Then we start from ```end``` of the array look for the first element (index) in the stack which is greater than the current one. That one is guaranteed to be the ```Next Greater Element```. Then put the current element (index) into the stack.
Time complexity: O(n).
```
public class Solution {
    public int[] nextGreaterElements(int[] nums) {
        int n = nums.length;
        int[] result = new int[n];
        
        Stack<Integer> stack = new Stack<>();
        for (int i = n - 1; i >= 0; i--) {
            stack.push(i);
        }
        
        for (int i = n - 1; i >= 0; i--) {
            result[i] = -1;
            while (!stack.isEmpty() && nums[stack.peek()] <= nums[i]) {
                stack.pop();
            }
            if (!stack.isEmpty()){
                result[i] = nums[stack.peek()];
            }
            stack.add(i);
        }
        
        return result;
    }
}
```
</p>


