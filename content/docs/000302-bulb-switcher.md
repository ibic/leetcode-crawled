---
title: "Bulb Switcher"
weight: 302
#id: "bulb-switcher"
---
## Description
<div class="description">
<p>There are <i>n</i> bulbs that are initially off. You first turn on all the bulbs. Then, you turn off every second bulb. On the third round, you toggle every third bulb (turning on if it&#39;s off or turning off if it&#39;s on). For the <i>i</i>-th round, you toggle every <i>i</i> bulb. For the <i>n</i>-th round, you only toggle the last bulb. Find how many bulbs are on after <i>n</i> rounds.</p>

<p><b>Example:</b></p>

<pre>
<strong>Input: </strong>3
<strong>Output:</strong> 1 
<strong>Explanation:</strong> 
At first, the three bulbs are <b>[off, off, off]</b>.
After first round, the three bulbs are <b>[on, on, on]</b>.
After second round, the three bulbs are <b>[on, off, on]</b>.
After third round, the three bulbs are <b>[on, off, off]</b>. 

So you should return 1, because there is only one bulb is on.
</pre>

</div>

## Tags
- Math (math)
- Brainteaser (brainteaser)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Mathworks - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (python3)
```python3
import operator
from functools import reduce

class Solution:
    def bulbSwitch(self, n: int) -> int:
        return int(n ** 0.5)
#        return reduce(
#            operator.add,
#            map(
#                lambda i: len(list(
#                    filter(
#                        lambda x: i % x == 0,
#                        range(1, int(i ** 2) + 1)))) % 2,
#                range(1, n+1)))
        
```

## Top Discussions
### Math solution..
- Author: StefanPochmann
- Creation Date: Sat Dec 19 2015 10:01:07 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 13:59:28 GMT+0800 (Singapore Standard Time)

<p>
    int bulbSwitch(int n) {
        return sqrt(n);
    }

A bulb ends up on iff it is switched an odd number of times.

Call them bulb 1 to bulb n. Bulb i is switched in round d if and only if d divides i. So bulb i ends up on if and only if it has an odd number of divisors.

Divisors come in pairs, like i=12 has divisors 1 and 12, 2 and 6, and 3 and 4. Except when i is a square, like 36 has divisors 1 and 36, 2 and 18, 3 and 12, 4 and 9, and double divisor 6. So bulb i ends up on if and only if i is a square.

**So just count the square numbers.**

Let R = int(sqrt(n)). That's the root of the largest square in the range [1,n]. And 1 is the smallest root. So you have the roots from 1 to R, that's R roots. Which correspond to the R squares. So int(sqrt(n)) is the answer. (C++ does the conversion to int automatically, because of the specified return type).
</p>


### Share my o(1) solution with explanation
- Author: KJer
- Creation Date: Thu Mar 10 2016 16:10:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 19:45:32 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        int bulbSwitch(int n) {
            return sqrt(n);
        }
    };
 As we know that there are n bulbs, let's name them as 1, 2, 3, 4, ..., n.  

 1. You first turn on all the bulbs.
 2. Then, you turn off every second bulb.(2, 4, 6, ...)
 3. On the third round, you toggle every third bulb.(3, 6, 9, ...) 
 4. For the ith round, you toggle every i bulb.(i, 2i, 3i, ...)
 5. For the nth round, you only toggle the last bulb.(n)


----------


If n > 6, you can find that bulb 6 is toggled in round 2 and 3. 

Later, it will also be toggled in round 6, and round 6 will be the last round when bulb 6 is toggled.

Here, **2,3 and 6 are all *factors* of 6 (except 1).**

----------
**Prove:**
----------

We can come to the conclusion that **the bulb *i* is toggled *k* times.**

Here, ***k*** is **the number of *i*'s factors (except 1)**.

***k* + 1** will be **the total number of *i*'s factors**


----------


For example:

 - **Factors of 6: 1, 2, 3, 6 (3 factors except 1, so it will be toggled 3 times)**
 - **Factors of 7: 1, 7 (1 factors except 1, so it will be toggled once)**
....

Now, the key problem here is to judge **whether *k* is *even* or *odd*.**

----------

Since **all bulbs are on at the beginning**, we can get:

 - **If *k* is *odd*, the bulb will be *off* in the end.(after odd times of toggling).**
 - **If *k* is *even*, the bulb i will be *on* in the end (after even times of toggling).**

As we all know, **a natural number can divided by 1 and itself**, and **all factors appear *in pairs***.

**When we know that *p* is *i*'s factor, we are sure *q* = *i/p* is also *i*'s factor.**

**If *i* has no factor *p* that makes *p* = *i/p*, *k*+ 1 is even.**

**If *i* has a factor *p* that makes *p* = *i/p* (*i* = *p*^2, *i* is a perfect square of *p*), *k*+ 1 is odd.**


----------


So we get that **in the end**:

 - If ***i*** is a **perfect square** , *k*+ 1 is odd, ***k* is even (bulb *i* is on)**.
 - If ***i*** is **NOT** a **perfect square** , *k*+ 1 is even, ***k* is odd (bulb *i* is off)**.

----------

We want to find **how many *bulbs* are on** after *n* rounds (**In the end**).

That means we need to find out **how many *perfect square numbers* are NO MORE than *n***.

The **number of *perfect square numbers* which are no more than *n***, is the ***square root* of the *maximum perfect square number* which is NO MORE than *n***

----------
**Result:**
----------

The ***square root* of the *maximum perfect square number* which is NO MORE than *n*** is the 
***integer part* of *sqrt(n)*.**

(**If *i* = 1, it will NEVER be toggled, *k* is 0 (even) here which meets the requirement.**)
</p>


### The simplest and most efficient solution well-explained
- Author: LHearen
- Creation Date: Mon Feb 29 2016 19:02:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 15:03:50 GMT+0800 (Singapore Standard Time)

<p>
### Solution
Before we take a jump to the solution, let's first try to clear out what exactly the problem is talking about: 
 - every i-th distance you switch the bulb to the opposite state (from on to off, or from off to on); actually suppose the bulbs are labelled from 1 to n then the every second bulb will mean that 2, 4, 6, 8, ... all even numbers less than n; while every third bulb will be 3, 6, 9, 12, ... all multiples of 3 that is less than n and so on; 
 - since the bulb will only have two different states - on or off, the result will be quite clear now; odd switching operations will result in bulb-on state (since original state is bulb-off) while even switching operations will give us bulb-off state;

Now the purpose here is clear searching for the **odd-operation numbers**: 
 - as for primes, they only have 1 and itself as their factors, even-operation numbers;
 - as for non-primes, normally they will have different pairs of factors like 12 whose factors are (1, 12), (3, 4), (2, 6) - 6 different factors, also even-operation numbers;
 - but among non-primes, there are some special numbers, perfect square numbers like 9 whose factors are (1, 9) and (3, 3) - odd-operation numbers, which means there will be only three different numbers that will affect the current bulb and result in bulb-on state!

So that's all we need to know to hack this problem now. But how to get the amount of squares that are less than n, quite simple. Sqrt(n) is the answer, since all square numbers that is less than n will be 1, 4, 9 ... n and their corresponding square roots will be 1, 2, 3,... sqrt(n).

 - Space cost O(1)
 - Time cost O(1)

```
//AC - 0ms;
int bulbSwitch(int n) {
	return sqrt(n);
}
```
Always welcome new ideas and `practical` tricks, just leave them in the comments!
</p>


