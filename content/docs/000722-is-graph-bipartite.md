---
title: "Is Graph Bipartite?"
weight: 722
#id: "is-graph-bipartite"
---
## Description
<div class="description">
<p>Given an undirected&nbsp;<code>graph</code>, return <code>true</code> if and only if it is bipartite.</p>

<p>Recall that a graph is <em>bipartite</em> if we can split it&#39;s set of nodes into two independent&nbsp;subsets A and B such that every edge in the graph has one node in A and another node in B.</p>

<p>The graph is given in the following form: <code>graph[i]</code> is a list of indexes <code>j</code> for which the edge between nodes <code>i</code> and <code>j</code> exists.&nbsp; Each node is an integer between <code>0</code> and <code>graph.length - 1</code>.&nbsp; There are no self edges or parallel edges: <code>graph[i]</code> does not contain <code>i</code>, and it doesn&#39;t contain any element twice.</p>

<pre>
<strong>Example 1:</strong>
<strong>Input:</strong> [[1,3], [0,2], [1,3], [0,2]]
<strong>Output:</strong> true
<strong>Explanation:</strong> 
The graph looks like this:
0----1
|    |
|    |
3----2
We can divide the vertices into two groups: {0, 2} and {1, 3}.
</pre>

<pre>
<strong>Example 2:</strong>
<strong>Input:</strong> [[1,2,3], [0,2], [0,1,3], [0,2]]
<strong>Output:</strong> false
<strong>Explanation:</strong> 
The graph looks like this:
0----1
| \  |
|  \ |
3----2
We cannot find a way to divide the set of nodes into two independent subsets.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>graph</code> will have length in range <code>[1, 100]</code>.</li>
	<li><code>graph[i]</code> will contain integers in range <code>[0, graph.length - 1]</code>.</li>
	<li><code>graph[i]</code> will not contain <code>i</code> or duplicate values.</li>
	<li>The graph is undirected: if any element <code>j</code> is in <code>graph[i]</code>, then <code>i</code> will be in <code>graph[j]</code>.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)
- Graph (graph)

## Companies
- Facebook - 19 (taggedByAdmin: true)
- Amazon - 4 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: false)
- LiveRamp - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Coloring by Depth-First Search [Accepted]

**Intuition**

Color a node blue if it is part of the first set, else red.  We should be able to greedily color the graph if and only if it is bipartite: one node being blue implies all it's neighbors are red, all those neighbors are blue, and so on.

<br />
<center>
    <img src="../Figures/785/color.png" alt="Diagram of coloring neighbors of nodes" width="350"/>
</center>
<br />

**Algorithm**

We'll keep an array (or hashmap) to lookup the color of each node: `color[node]`.  The colors could be `0`, `1`, or uncolored (`-1` or `null`).

We should be careful to consider disconnected components of the graph, by searching each node.  For each uncolored node, we'll start the coloring process by doing a depth-first-search on that node.  Every neighbor gets colored the opposite color from the current node.  If we find a neighbor colored the same color as the current node, then our coloring was impossible.

To perform the depth-first search, we use a `stack`.  For each uncolored neighbor in `graph[node]`, we'll color it and add it to our `stack`, which acts as a sort of "todo list" of nodes to visit next.  Our larger loop `for start...` ensures that we color every node.

<iframe src="https://leetcode.com/playground/ueLr9bZm/shared" frameBorder="0" width="100%" height="500" name="ueLr9bZm"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N + E)$$, where $$N$$ is the number of nodes in the graph, and $$E$$ is the number of edges.  We explore each node once when we transform it from uncolored to colored, traversing all its edges in the process.

* Space Complexity:  $$O(N)$$, the space used to store the `color`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Clean DFS solution with Explanation
- Author: FLAGbigoffer
- Creation Date: Sun Feb 18 2018 14:48:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 02:54:05 GMT+0800 (Singapore Standard Time)

<p>
`Our goal` is trying to use two colors to color the graph and see if there are any adjacent nodes having the same color.
Initialize a color[] array for each node. Here are three states for `colors[]` array:
`0: Haven\'t been colored yet.`
`1: Blue.`
`-1: Red.`
For each node, 
1. If it hasn\'t been colored, use a color to color it. Then use the other color to color all its adjacent nodes (DFS).
2. If it has been colored, check if the current color is the same as the color that is going to be used to color it. (Please forgive my english... Hope you can understand it.)

`DFS Solution:`
```
class Solution {
    public boolean isBipartite(int[][] graph) {
        int n = graph.length;
        int[] colors = new int[n];			
				
        for (int i = 0; i < n; i++) {              //This graph might be a disconnected graph. So check each unvisited node.
            if (colors[i] == 0 && !validColor(graph, colors, 1, i)) {
                return false;
            }
        }
        return true;
    }
    
    public boolean validColor(int[][] graph, int[] colors, int color, int node) {
        if (colors[node] != 0) {
            return colors[node] == color;
        }       
        colors[node] = color;       
        for (int next : graph[node]) {
            if (!validColor(graph, colors, -color, next)) {
                return false;
            }
        }
        return true;
    }
}
```
`BFS Solution:`
```
class Solution {
    public boolean isBipartite(int[][] graph) {
        int len = graph.length;
        int[] colors = new int[len];
        
        for (int i = 0; i < len; i++) {
            if (colors[i] != 0) continue;
            Queue<Integer> queue = new LinkedList<>();
            queue.offer(i);
            colors[i] = 1;   // Blue: 1; Red: -1.
            
            while (!queue.isEmpty()) {
                int cur = queue.poll();
                for (int next : graph[cur]) {
                    if (colors[next] == 0) {          // If this node hasn\'t been colored;
                        colors[next] = -colors[cur];  // Color it with a different color;
                        queue.offer(next);
                    } else if (colors[next] != -colors[cur]) {   // If it is colored and its color is different, return false;
                        return false;
                    }
                }
            }
        }
        
        return true;
    }
}
```
</p>


### Easy Python Solution
- Author: lee215
- Creation Date: Sun Feb 18 2018 22:01:09 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 06 2019 14:06:47 GMT+0800 (Singapore Standard Time)

<p>

```
    def isBipartite(self, graph):
        color = {}
        def dfs(pos):
            for i in graph[pos]:
                if i in color:
                    if color[i] == color[pos]:
                        return False
                else:
                    color[i] = 1 - color[pos]
                    if not dfs(i):
                        return False
            return True
        for i in range(len(graph)):
            if i not in color:
                color[i] = 0
                if not dfs(i):
                    return False
        return True
```
</p>


### java BFS 
- Author: shawntsai
- Creation Date: Sun Feb 18 2018 15:28:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 06:52:45 GMT+0800 (Singapore Standard Time)

<p>
just want to provide another type of solution

we need to check each if each cluster(edges linked together) is Bipartite.


```

class Solution {
    public boolean isBipartite(int[][] graph) {
        //BFS
        // 0(not meet), 1(black), 2(white)
        int[] visited = new int[graph.length];
        
        for (int i = 0; i < graph.length; i++) {
            if (graph[i].length != 0 && visited[i] == 0) {
                visited[i] = 1;
                Queue<Integer> q = new LinkedList<>();
                q.offer(i);
                while(! q.isEmpty()) {
                    int current = q.poll();
                    for (int c: graph[current]) {

                            if (visited[c] == 0) {
                                visited[c] = (visited[current] == 1) ? 2 : 1;
                                q.offer(c);
                            } else {
                                if (visited[c] == visited[current]) return false;
                            }
                    }
                }                        
                
            }
        }
        
        return true;
    }
}
```
</p>


