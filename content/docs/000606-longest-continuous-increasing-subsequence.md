---
title: "Longest Continuous Increasing Subsequence"
weight: 606
#id: "longest-continuous-increasing-subsequence"
---
## Description
<div class="description">
<p>
Given an unsorted array of integers, find the length of longest <code>continuous</code> increasing subsequence (subarray).
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [1,3,5,4,7]
<b>Output:</b> 3
<b>Explanation:</b> The longest continuous increasing subsequence is [1,3,5], its length is 3. 
Even though [1,3,5,7] is also an increasing subsequence, it's not a continuous one where 5 and 7 are separated by 4. 
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> [2,2,2,2,2]
<b>Output:</b> 1
<b>Explanation:</b> The longest continuous increasing subsequence is [2], its length is 1. 
</pre>
</p>

<p><b>Note:</b>
Length of the array will not exceed 10,000.
</p>
</div>

## Tags
- Array (array)

## Companies
- Facebook - 2 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]


#### Approach #1: Sliding Window [Accepted]

**Intuition and Algorithm**

Every (continuous) increasing subsequence is disjoint, and the boundary of each such subsequence occurs whenever `nums[i-1] >= nums[i]`.  When it does, it marks the start of a new increasing subsequence at `nums[i]`, and we store such `i` in the variable `anchor`.

For example, if `nums = [7, 8, 9, 1, 2, 3]`, then `anchor` starts at `0` (`nums[anchor] = 7`) and gets set again to `anchor = 3` (`nums[anchor] = 1`).  Regardless of the value of `anchor`, we record a candidate answer of `i - anchor + 1`, the length of the subarray `nums[anchor], nums[anchor+1], ..., nums[i]`; and our answer gets updated appropriately.

<iframe src="https://leetcode.com/playground/n3XVE4by/shared" frameBorder="0" width="100%" height="225" name="n3XVE4by"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `nums`.  We perform one loop through `nums`.

* Space Complexity: $$O(1)$$, the space used by `anchor` and `ans`.

## Accepted Submission (python3)
```python3
class Solution:
    def findLengthOfLCIS(self, nums: List[int]) -> int:
        r = 0
        prev = -math.inf
        curLen = 0
        for n in nums + [-math.inf]:
            if n > prev:
                curLen += 1
            else:
                if curLen > r:
                    r = curLen
                curLen = 1
            prev = n
        return r
```

## Top Discussions
### [Java/C++]Clean solution
- Author: caihao0727mail
- Creation Date: Sun Sep 10 2017 11:16:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 27 2018 07:44:47 GMT+0800 (Singapore Standard Time)

<p>
The idea is to use ```cnt``` to record the length of the current continuous increasing subsequence which ends with ```nums[i]```, and use ```res``` to record the maximum ```cnt```.

Java version:
```
    public int findLengthOfLCIS(int[] nums) {
        int res = 0, cnt = 0;
        for(int i = 0; i < nums.length; i++){
            if(i == 0 || nums[i-1] < nums[i]) res = Math.max(res, ++cnt);
            else cnt = 1;
        }
        return res;
    }
```

C++ version:
```
    int findLengthOfLCIS(vector<int>& nums) {
        int res = 0, cnt = 0;
        for(int i = 0; i < nums.size(); i++){
            if(i == 0 || nums[i-1] < nums[i]) res = max(res, ++cnt);
            else cnt = 1;
        }
        return res;
    }
```
</p>


### Python Simple Solution
- Author: yangshun
- Creation Date: Sun Sep 10 2017 11:07:07 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 13:52:16 GMT+0800 (Singapore Standard Time)

<p>
A continuous subsequence is essentially a subarray. Hence this question is asking for the longest increasing subarray and I have no idea why the question calls it continuous subsequence to confuse the readers. 

Anyway, we can make one pass of the array and keep track of the current streak of increasing elements, reset it when it does not increase.

*- Yangshun*

```
class Solution(object):
    def findLengthOfLCIS(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        # Time: O(n)
        # Space: O(1)
        max_len = i = 0
        while i < len(nums):
            curr = 1
            while i + 1 < len(nums) and nums[i] < nums[i + 1]:
                curr, i = curr + 1, i + 1
            max_len = max(max_len, curr)
            i += 1
        return max_len
```
</p>


### Python DP solution
- Author: shijungg
- Creation Date: Wed Nov 28 2018 20:22:14 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 28 2018 20:22:14 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def findLengthOfLCIS(self, nums):
        if not nums:
            return 0
        dp = [1] * len(nums)
        for i in range(1, len(nums)):
            if nums[i] > nums[i-1]:
                dp[i] = dp[i - 1] + 1
        return max(dp)
```
</p>


