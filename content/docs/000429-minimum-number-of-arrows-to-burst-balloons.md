---
title: "Minimum Number of Arrows to Burst Balloons"
weight: 429
#id: "minimum-number-of-arrows-to-burst-balloons"
---
## Description
<div class="description">
<p>There are some spherical balloons spread in two-dimensional space. For each balloon, provided input is the start and end coordinates of the horizontal diameter. Since it&#39;s horizontal, y-coordinates don&#39;t matter, and hence the x-coordinates of start and end of the diameter suffice. The start is always smaller than the end.</p>

<p>An arrow can be shot up exactly vertically from different points along the x-axis. A balloon with <code>x<sub>start</sub></code> and <code>x<sub>end</sub></code> bursts by an arrow shot at <code>x</code> if <code>x<sub>start</sub> &le; x &le; x<sub>end</sub></code>. There is no limit to the number of arrows that can be shot. An arrow once shot keeps traveling up infinitely.</p>

<p>Given an array <code>points</code> where <code>points[i] = [x<sub>start</sub>, x<sub>end</sub>]</code>, return&nbsp;<em>the minimum number of arrows that must be shot to burst all balloons</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> points = [[10,16],[2,8],[1,6],[7,12]]
<strong>Output:</strong> 2
<strong>Explanation:</strong> One way is to shoot one arrow for example at x = 6 (bursting the balloons [2,8] and [1,6]) and another arrow at x = 11 (bursting the other two balloons).
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> points = [[1,2],[3,4],[5,6],[7,8]]
<strong>Output:</strong> 4
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> points = [[1,2],[2,3],[3,4],[4,5]]
<strong>Output:</strong> 2
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> points = [[1,2]]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> points = [[2,3],[2,3]]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= points.length &lt;= 10<sup>4</sup></code></li>
	<li><code>points.length == 2</code></li>
	<li><code>-2<sup>31</sup> &lt;= x<sub>start</sub> &lt;&nbsp;x<sub>end</sub> &lt;= 2<sup>31</sup> - 1</code></li>
</ul>
</div>

## Tags
- Greedy (greedy)
- Sort (sort)

## Companies
- Facebook - 5 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Quora - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Greedy

**Greedy algorithms**

Greedy problems usually look like 
"Find minimum number of _something_ to do _something_" or 
"Find maximum number of _something_ to fit in _some conditions_", 
and typically propose an unsorted input.

> The idea of greedy algorithm is to pick the _locally_
optimal move at each step, that will lead to the _globally_ optimal solution.

The standard solution has $$\mathcal{O}(N \log N)$$ time complexity and consists of two parts:

- Figure out how to sort the input data ($$\mathcal{O}(N \log N)$$ time).
That could be done directly by a sorting or indirectly by a heap usage. 
Typically sort is better than the heap usage because of gain in space.

- Parse the sorted input to have a solution ($$\mathcal{O}(N)$$ time). 

Please notice that in case of well-sorted input one doesn't need the first 
part and the greedy solution could have $$\mathcal{O}(N)$$ time complexity,
[here is an example](https://leetcode.com/articles/gas-station/).

> How to prove that your greedy algorithm provides globally optimal solution?

Usually you could use the [proof by contradiction](https://en.wikipedia.org/wiki/Proof_by_contradiction). 

**Intuition**

Let's consider the following combinations of the balloons.

![bla](../Figures/452/balloons.png)

That's quite obvious that two arrows is enough to burst them all, let's figure out
how to compute this result with the help of greedy algorithm.

![bla](../Figures/452/arrows.png)

Let's sort the balloons by the end coordinate, and then check them 
one by one. The first balloon is a green one number `0`, 
it ends at coordinate `6`, and there is no balloons 
ending before it because of sorting.

The other balloons have two possibilities :

- To have a start coordinate smaller than `6`, like a red balloon.
These ones could be burst together with the balloon `0` by one arrow.

- To have a start coordinate larger than `6`, like a yellow balloon.
These ones couldn't be burst together with the balloon `0` by one arrow,
and hence one needs to increase the number of arrows here.

![bla](../Figures/452/sorted.png)

> That means that one could always track the end of the current balloon,
and ignore all the balloons which end before it. Once the current balloon
is ended (= the next balloon starts after the current balloon),
one has to increase the number of arrows by one and start to track 
the end of the next balloon.

**Algorithm**

Now the algorithm is straightforward :

- Sort the balloons by end coordinate `x_end`.

- Initiate the end coordinate of a balloon which ends first : 
`first_end = points[0][1]`.
 
- Initiate number of arrows: `arrows = 1`.

- Iterate over all balloons:

    - If the balloon starts after `first_end`:
        
        - Increase  the number of arrows by one.
        
        - Set `first_end` to be equal to the end of the current
        balloon.
        
- Return arrows.

**Implementation**

<iframe src="https://leetcode.com/playground/VDaTEDqk/shared" frameBorder="0" width="100%" height="500" name="VDaTEDqk"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N \log N)$$ because of sorting of
input data.
 
* Space complexity : $$\mathcal{O}(N)$$ or $$\mathcal{O}(\log{N})$$

  - The space complexity of the sorting algorithm depends on the implementation of each program language.

  - For instance, the `list.sort()` function in Python is implemented with the [Timsort](https://en.wikipedia.org/wiki/Timsort) algorithm whose space complexity is $$\mathcal{O}(N)$$.

  - In Java, the [Arrays.sort()](https://docs.oracle.com/javase/8/docs/api/java/util/Arrays.html#sort-byte:A-) is implemented as a variant of quicksort algorithm whose space complexity is $$\mathcal{O}(\log{N})$$.


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Share my explained Greedy solution
- Author: Joshua924
- Creation Date: Tue Jan 03 2017 08:44:31 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 02:21:44 GMT+0800 (Singapore Standard Time)

<p>
No offense but the currently highest voted java solution is not ideal, the sorting can be adjusted so that there\'s no need to check again in the for loop.

**Idea:**
We know that eventually we have to shoot down every balloon, so for each ballon there must be an arrow whose position is between **balloon[0]** and **balloon[1]** inclusively. Given that, we can sort the array of balloons by their **ending position**. Then we make sure that **while we take care of each balloon in order, we can shoot as many following balloons as possible.**

So what position should we pick each time? We should shoot as to the right as possible, because since balloons are sorted, this gives you the best chance to take down more balloons. Therefore the position should always be **balloon[i][1]** for the *i*th balloon.

This is exactly what I do in the for loop: check how many balloons I can shoot down with one shot aiming at the **ending position of the current balloon**. Then I skip all these balloons and start again from the next one (or the leftmost remaining one) that needs another arrow.


**Example:**
```
balloons = [[7,10], [1,5], [3,6], [2,4], [1,4]]
```
After sorting, it becomes:
```
balloons = [[2,4], [1,4], [1,5], [3,6], [7,10]]
```
So first of all, we shoot at position **4**, we go through the array and see that all first 4 balloons can be taken care of by this single shot. Then we need another shot for one last balloon. So the result should be 2.
<br>


**Code:**
```
public int findMinArrowShots(int[][] points) {
        if (points.length == 0) {
            return 0;
        }
        Arrays.sort(points, (a, b) -> a[1] - b[1]);
        int arrowPos = points[0][1];
        int arrowCnt = 1;
        for (int i = 1; i < points.length; i++) {
            if (arrowPos >= points[i][0]) {
                continue;
            }
            arrowCnt++;
            arrowPos = points[i][1];
        }
        return arrowCnt;
    }
```
</p>


### A Concise Template for "Overlapping Interval Problem"
- Author: wangxinbo
- Creation Date: Sun Dec 25 2016 02:04:44 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 18 2018 11:57:26 GMT+0800 (Singapore Standard Time)

<p>
Here I provide a concise template that I summarize for the so-called "Overlapping Interval Problem", e.g. Minimum Number of Arrows to Burst Balloons, and Non-overlapping Intervals etc. I found these problems share some similarities on their solutions.
* Sort intervals/pairs in increasing order of the start position.
* Scan the sorted intervals, and maintain an "active set" for overlapping intervals. At most times, we do not need to use an explicit set to store them. Instead, we just need to maintain several key parameters, e.g. the number of overlapping intervals (count), the minimum ending point among all overlapping intervals (minEnd). 
* If the interval that we are currently checking overlaps with the active set, which can be characterized by cur.start > minEnd, we need to renew those key parameters or change some states.
* If the current interval does not overlap with the active set, we just drop current active set, record some parameters, and create a new active set that contains the current interval.
```
int count = 0; // Global parameters that are useful for results.
int minEnd = INT_MAX; // Key parameters characterizing the "active set" for overlapping intervals, e.g. the minimum ending point among all overlapping intervals.
sort(points.begin(), points.end()); // Sorting the intervals/pairs in ascending order of its starting point
for each interval {
      if(interval.start > minEnd) { // If the 
	 // changing some states, record some information, and start a new active set. 
	count++;
	minEnd = p.second;
      }
     else {
	// renew key parameters of the active set
	minEnd = min(minEnd, p.second);
      } 
 }
return the result recorded in or calculated from the global information;
```

For example, for the problem Minimum "**Number of Arrows to Burst Balloons**", we have
* Sort balloons in increasing order of the start position.
* Scan the sorted pairs, and maintain a pointer for the minimum end position for current "active balloons", whose diameters are overlapping. 
* When the next balloon starts after all active balloons, shoot an arrow to burst all active balloons, and start to record next active balloons.

```
int findMinArrowShots(vector<pair<int, int>>& points) {
        int count = 0, minEnd = INT_MAX;
        sort(points.begin(), points.end());
        for(auto& p: points) {
            if(p.first > minEnd) {count++; minEnd = p.second;}
            else minEnd = min(minEnd, p.second);
        }
        return count + !points.empty();
    }
```

For the problem "**Non-overlapping Intervals**", we have
```
int eraseOverlapIntervals(vector<Interval>& intervals) {
        int total = 0, minEnd = INT_MIN, overNb = 1;
        sort(intervals.begin(), intervals.end(), [&](Interval& inter1, Interval& inter2) {return inter1.start < inter2.start;});
        for(auto& p: intervals) {
            if(p.start >= minEnd) {
                total += overNb-1;
                overNb = 1;
                minEnd = p.end;
            }
            else {
                overNb++;
                minEnd = min(minEnd, p.end);
            }
        }
        return total + overNb-1;
    }
```
</p>


### Greedy, Python (132 ms)
- Author: Cornelius194968
- Creation Date: Tue Nov 08 2016 02:25:43 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 00:20:23 GMT+0800 (Singapore Standard Time)

<p>
1. Sort intervals by ending value;
2. Only count valid intervals we need, and skip overlapping intervals
return the count

```
class Solution(object):
    def findMinArrowShots(self, points):
        """
        :type points: List[List[int]]
        :rtype: int
        """
        points = sorted(points, key = lambda x: x[1])
        res, end = 0, -float('inf')
        for interval in points:
            if interval[0] > end:
                res += 1
                end = interval[1]
        return res
```
</p>


