---
title: "Global and Local Inversions"
weight: 711
#id: "global-and-local-inversions"
---
## Description
<div class="description">
<p>We have some permutation <code>A</code> of <code>[0, 1, ..., N - 1]</code>, where <code>N</code> is the length of <code>A</code>.</p>

<p>The number of (global) inversions is the number of <code>i &lt; j</code> with <code>0 &lt;= i &lt; j &lt; N</code> and <code>A[i] &gt; A[j]</code>.</p>

<p>The number of local inversions is the number of <code>i</code> with <code>0 &lt;= i &lt; N</code> and <code>A[i] &gt; A[i+1]</code>.</p>

<p>Return <code>true</code>&nbsp;if and only if the number of global inversions is equal to the number of local inversions.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> A = [1,0,2]
<strong>Output:</strong> true
<strong>Explanation:</strong> There is 1 global inversion, and 1 local inversion.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> A = [1,2,0]
<strong>Output:</strong> false
<strong>Explanation:</strong> There are 2 global inversions, and 1 local inversion.
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li><code>A</code> will be a permutation of <code>[0, 1, ..., A.length - 1]</code>.</li>
	<li><code>A</code> will have length in range <code>[1, 5000]</code>.</li>
	<li>The time limit for this problem has been reduced.</li>
</ul>

</div>

## Tags
- Array (array)
- Math (math)

## Companies
- Amazon - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Brute Force [Time Limit Exceeded]

**Intuition and Algorithm**

A local inversion is also a global inversion.  Thus, we only need to check if our permutation has any non-local inversion (`A[i] > A[j], i < j`) with `j - i > 1`.

To do this, we can check every `i, j` directly.


<iframe src="https://leetcode.com/playground/yuFdd3Zh/shared" frameBorder="0" width="100%" height="208" name="yuFdd3Zh"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `A`.

* Space Complexity: $$O(1)$$.

---
#### Approach #2: Remember Minimum [Accepted]

**Intuition**

In performing a brute force, we repeatedly want to check whether there is some `j >= i+2` for which `A[i] > A[j]`.  This is the same as checking for `A[i] > min(A[i+2:])`.  If we knew these minimums `min(A[0:]), min(A[1:]), min(A[2:]), ...` we could make this check quickly.

**Algorithm**

Let's iterate through `A` from right to left, remembering the minimum value we've seen.  If we remembered the minimum `floor = min(A[i:])` and `A[i-2] > floor`, then we should return `False`.  This search is exhaustive, so if we don't find anything after iterating through `A`, we should return `True`.

<iframe src="https://leetcode.com/playground/K4k5Lyof/shared" frameBorder="0" width="100%" height="242" name="K4k5Lyof"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity: $$O(1)$$.

---
#### Approach #3: Linear Scan [Accepted]

**Intuition and Algorithm**

Let's try to characterize all *ideal* permutations: ones that do not have non-local inversions.  Where does the `0` go?

If the `0` occurs at index `2` or greater, then `A[0] > A[2] = 0` is a non-local inversion.  So `0` can only occur at index `0` or `1`.  If `A[1] = 0`, then we must have `A[0] = 1` otherwise `A[0] > A[j] = 1` is a non-local inversion.  Otherwise, `A[0] = 0`.  To recap, the possibilities are either `A = [0] + (ideal permutation of 1...N-1)` or `A = [1, 0] + (ideal permutation of 2...N-1)`.

A necessary and sufficient condition for these possibilities is that `Math.abs(A[i] - i) <= 1`.  So we check this for every `i`.

<iframe src="https://leetcode.com/playground/EMcRkUJh/shared" frameBorder="0" width="100%" height="191" name="EMcRkUJh"></iframe>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My 3 lines C++ Solution
- Author: grandyang
- Creation Date: Sun Jan 28 2018 12:00:49 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 02:47:50 GMT+0800 (Singapore Standard Time)

<p>
The original order should be [0, 1, 2, 3, 4...], the number i should be on the position i. We just check the offset of each number, if the absolute value is larger than 1, means the number of global inversion must be bigger than local inversion, because a local inversion is a global inversion, but a global one may not be local.

proof:

If A[i] > i + 1, means at least one number that is smaller than A[i] is kicked out from first A[i] numbers, and the distance between this smaller number and A[i] is at least 2, then it is a non-local global inversion. 
For example, A[i] = 3, i = 1, at least one number that is smaller than 3 is kicked out from first 3 numbers, and the distance between the smaller number and 3 is at least 2.

If A[i] < i - 1, means at least one number that is bigger than A[i] is kicked out from last n - i numbers, and the distance between this bigger number and A[i] is at least 2, then it is a non-local global inversion. 

```
class Solution {
public:
    bool isIdealPermutation(vector<int>& A) {
	for (int i = 0; i < A.size(); ++i) {
            if (abs(A[i] - i) > 1) return false;
        }
	return true;
    }
};
</p>


### [C++/Java/Python] Easy and Concise Solution
- Author: lee215
- Creation Date: Sun Jan 28 2018 12:39:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 21:11:19 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
All local inversions are global inversions.
If the number of global inversions is equal to the number of local inversions,
it means that all global inversions in permutations are local inversions.
It also means that we can not find ```A[i] > A[j]``` with ```i+2<=j```.
In other words,  ```max(A[i]) < A[i+2]```

In this first solution, I traverse ```A``` and keep the current biggest number ```cmax```.
Then I check the condition  ```cmax < A[i+2]```

Here come this solutions

# Solution 1
**C++**
```cpp
bool isIdealPermutation(vector<int>& A) {
        int cmax = 0, n = A.size();
        for (int i = 0; i < n - 2; ++i) {
            cmax = max(cmax, A[i]);
            if (cmax > A[i + 2]) return false;
        }
        return true;
    }
```
**Java**
```java
public boolean isIdealPermutation(int[] A) {
        int cmax = 0;
        for (int i = 0; i < A.length - 2; ++i) {
            cmax = Math.max(cmax, A[i]);
            if (cmax > A[i + 2]) return false;
        }
        return true;
    }
```
**Python**
```py
def isIdealPermutation(self, A):
        cmax = 0
        for i in range(len(A) - 2):
            cmax = max(cmax, A[i])
            if cmax > A[i + 2]:
                return False
        return True
```
<br>

# Solution 2
Basic on this idea, I tried to arrange an ideal permutation. 
Then I found that to place number `i`
I could only place `i` at `A[i-1]`, `A[i]` or `A[i+1]`. 
So it came up to me, 
It will be easier just to check if all `A[i] - i` equals to -1, 0 or 1.

**C++**
```cpp
    bool isIdealPermutation(vector<int>& A) {
        for (int i = 0; i < A.size(); ++i)
            if (abs(A[i] - i) > 1) return false;
        return true;
    }
```
**Java**
```java
    public boolean isIdealPermutation(int[] A) {
        for (int i = 0; i < A.length; ++i)
            if (Math.abs(A[i] - i) > 1) return false;
        return true;
    }
```
**Python**
```py
    def isIdealPermutation(self, A):
        return all(abs(i - v) <= 1 for i, v in enumerate(A))
```
</p>


### Logical Thinking with Clear Code
- Author: GraceMeng
- Creation Date: Wed Jul 18 2018 08:43:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 21:59:34 GMT+0800 (Singapore Standard Time)

<p>
**Logical Thinking**
Local Inversions are part of Global Inversions when `j == i + 1`. 
Thus, whenever we find inversions that j != i + 1, we return false.
**Initial Code**
```
    public boolean isIdealPermutation_BF(int[] A) {

        for (int i = 0; i < A.length; i++) {
            for (int j = i + 1; j < A.length; j++) {
                if (A[j] < A[i]) {
                    if (j != i + 1) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
```
However, that will cause **TLE** for the time complexity is O(n^2).
Let\'s think about the problem in an abstract way, local inversions are actually swaps between two adjacent elements only. So whenever we detect  **|i - A[i]| > 1**, we return false.

**Clear Code**
```
    public boolean isIdealPermutation(int[] A) {

        for (int i = 0; i < A.length; i++) {
            if (Math.abs(i - A[i]) > 1)
                return false;
        }

        return true;
    }
```

**I would appreciate your VOTE UP :)**
</p>


