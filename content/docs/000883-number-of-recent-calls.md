---
title: "Number of Recent Calls"
weight: 883
#id: "number-of-recent-calls"
---
## Description
<div class="description">
<p>You have a <code>RecentCounter</code> class which counts the number of recent requests within a certain time frame.</p>

<p>Implement the <code>RecentCounter</code> class:</p>

<ul>
	<li><code>RecentCounter()</code> Initializes the counter with zero recent requests.</li>
	<li><code>int ping(int t)</code> Adds a new request at time <code>t</code>, where <code>t</code> represents some time in milliseconds, and returns the number of requests that has happened in the past <code>3000</code> milliseconds (including the new request). Specifically, return the number of requests that have happened in the inclusive range <code>[t - 3000, t]</code>.</li>
</ul>

<p>It is <strong>guaranteed</strong> that every call to <code>ping</code> uses a strictly larger value of <code>t</code> than the previous call.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;RecentCounter&quot;, &quot;ping&quot;, &quot;ping&quot;, &quot;ping&quot;, &quot;ping&quot;]
[[], [1], [100], [3001], [3002]]
<strong>Output</strong>
[null, 1, 2, 3, 3]

<strong>Explanation</strong>
RecentCounter recentCounter = new RecentCounter();
recentCounter.ping(1);     // requests = [<u>1</u>], range is [-2999,1], return 1
recentCounter.ping(100);   // requests = [<u>1</u>, <u>100</u>], range is [-2900,100], return 2
recentCounter.ping(3001);  // requests = [<u>1</u>, <u>100</u>, <u>3001</u>], range is [1,3001], return 3
recentCounter.ping(3002);  // requests = [1, <u>100</u>, <u>3001</u>, <u>3002</u>], range is [2,3002], return 3
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= t &lt;= 10<sup>9</sup></code></li>
	<li>Each test case will call <code>ping</code> with <strong>strictly increasing</strong> values of <code>t</code>.</li>
	<li>At most <code>10<sup>4</sup></code> calls will be made to <code>ping</code>.</li>
</ul>

</div>

## Tags
- Queue (queue)

## Companies
- Yandex - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Overview

This problem is practical, which can test one's basic knowledge about the data structure and algorithm.

First of all, let us clarify the problem a bit.
We are given a sequence of ping calls, _i.e._ $$[t_1, t_2, t_3, ... t_n]$$, ordered by the chronological order of their arrival time.

>Given the current ping call $$t_i$$, we are asked to count the number of previous calls that fall in the range of $$[t_i - 3000, \space t_i]$$.

Voila. This is how we can reformulate the problem with the basic data structure such as Array.
Note, the sequence of calls is ever-increasing, and we are given the call **one at a time**.

By the way, we also have a dedicated Explore card called [Array 101](https://leetcode.com/explore/learn/card/fun-with-arrays/) where one can review the characteristics and operations about Array.
In addition, one will also discover many interesting problems that can be solved with Array.


---
#### Approach 1: Iteration over Sliding Window

**Intuition**

Now that we've clarified the nature of the problem, it shall not be difficult to come up with a solution.
In fact, the solution is as simple as iterating through an array or a list.

>The idea is that we can use a container such as **array** or **list** to keep track of all the incoming ping calls. At each occasion of `ping(t)` call, first we append the call to the container, and then starting from the current call, we **iterate backwards** to count the calls that fall into the time range of `[t-3000, t]`.

Before rushing to the implementation, let us dwell on the problem a bit more, since there are still plenty of things we could optimize.

One observation is that the sequence is ever-growing, so as our container.

On the other hand, once the ping calls become **_outdated_**, _i.e._ out of the scope of `[t-3000, t]`, we do not need to keep them any longer in the container, since they will not contribute to the solution later.

>As a result, one optimization that we could do is that rather than keeping all the **_historical_** ping calls in the container, we could **_remove_** the _outdated_ calls _on the go_, which can avoid the overflow of the container and reduce the memory consumption to the least.

In summary, our container will function like a **_sliding window_** over the ever-growing sequence of ping calls. 

![sliding window](../Figures/933/933_sliding_window.png)

Based on the above description, the **list** data structure seems to be more fit as the container for our tasks, than the **array**.
Because the list is more adapted for the following two operations:

- **Appending**: we will append each incoming call to the tail of the sliding window. 

- **Popping**:  we need to pop out all the _outdated_ calls from the head of the sliding window. 

**Algorithm**

To implement the sliding window, we could use the `LinkedList` in Java or `deque` in Python.

Then the `ping(t)` function can be implemented in two steps:

- Step 1): we append the current ping call to the tail of the sliding window.

- Step 2): starting from the head of the sliding window, we remove the _outdated_ calls, until we come across a still valid ping call.

As a result, the remaining calls in the sliding window are the ones that fall into the range of `[t - 3000, t]`.


<iframe src="https://leetcode.com/playground/PWPuyNdk/shared" frameBorder="0" width="100%" height="361" name="PWPuyNdk"></iframe>



**Complexity Analysis**

First of all, let us estimate the upper-bound on the size of our sliding window.
Here we quote an important condition from the problem description: _"It is guaranteed that every call to ping uses a strictly larger value of t than before."_
Based on the above condition, the maximal number of elements in our sliding window would be $$3000$$, which is also the maximal time difference between the head and the tail elements. 


- Time Complexity: $$\mathcal{O}(1)$$

    - The main time complexity of our `ping()` function lies in the loop, which in the worst case would run 3000 iterations to pop out all outdated elements, and in the best case a single iteration.

    - Therefore, for a single invocation of `ping()` function, its time complexity is $$\mathcal{O}(3000) = \mathcal{O}(1)$$.

    - If we assume that there is a ping call at each timestamp, then the cost of `ping()` is further amortized, where at each invocation, we would only need to pop out a single element, once the sliding window reaches its upper bound.


- Space Complexity: $$\mathcal{O}(1)$$

    - As we estimated before, the maximal size of our sliding window is 3000, which is a constant.


---
#### Discussion 

Since the elements in our sliding window are _strictly_ ordered, due to the condition of the problem, one might argue that it might be more efficient to use **_[binary search](https://leetcode.com/explore/learn/card/binary-search/)_** to locate the most recent outdated calls and then starting from that point *truncate* all the previous calls.

In terms of search, _binary search_ is seemingly more efficient than our _linear search_.
When the elements are held in the array data structure, it is true that binary search is more efficient.

However, it is not the case for the linked list, since there is no way to locate an element in the middle of a linked list instantly, which is a critical condition for binary search algorithm. 

As a result, in order to apply _binary search_, we might have to opt for the **Array** data structure.
On the other hand, once we use the array as the container, we might have to keep all the historical elements, which in the long run is not space-efficient neither time-efficient later.
Or we have to find a way to efficiently remove the elements from array without frequently reallocating memory.

To conclude, it is doable to have a _**binary search**_ solution.
Yet, it would complicate the design, and at the end the final solution is not necessarily more efficient than the above simple LinkedList-based sliding window.

Finally, if one is interested in such a problem, there is another rather similar problem called [logger rate limiter](https://leetcode.com/problems/logger-rate-limiter/).


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Confused
- Author: rangitaaaa
- Creation Date: Thu Dec 13 2018 03:46:36 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Dec 13 2018 03:46:36 GMT+0800 (Singapore Standard Time)

<p>
I am slightly confused by the problem statement, could anyone describe the sample output? I get how 1 and 2 are counted, but not why 3001 and 3002 are not.
</p>


### [Java/Python 3] Five solutions: TreeMap , TreeSet, ArrayList, Queue, Circular List.
- Author: rock
- Creation Date: Sun Nov 04 2018 11:01:57 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 03 2019 00:17:33 GMT+0800 (Singapore Standard Time)

<p>
**Update 2**: Added 2 implementations of Python 3.
**Update 1**: Method 4 added for those who are interested in space optimization.

**Mehod 1:** `TreeMap`.

Use current time and total number of calls as the key and value of `TreeMap`, respectively.

**Analysis**
Time: O(logN), space: O(N), where N is the number of ping calls from first one till now.

```TreeMap.tailMap()``` and `put()` both cost time O(logN).
```
    TreeMap<Integer, Integer> tm;

    public RecentCounter() {
        tm = new TreeMap<>();
    }
    
    public int ping(int t) {
        tm.put(t, 1 + tm.size());
        return tm.tailMap(t - 3000).size();
    }
```
**Mehod 2:** `TreeSet`.

Or similarly use ```TreeSet``` instead.

```TreeSet.tailSet()``` and `add()` both cost time O(logN).

```
    TreeSet<Integer> ts;

    public RecentCounter() {
        ts = new TreeSet<>();
    }
    
    public int ping(int t) {
        ts.add(t);
        return ts.tailSet(t - 3000).size();
    }
```
Since some complain that the above two methods cause `TLE`, I guess that besides `tailMap` and `tailSet, TreeMap.put()` and `TreeSet.add()` also cost `O(logN)`, therefore the total time cost is high.

The following method will use less time.

**Mehod 3:** 
**Java: Binary Search `ArrayList`**.

Use binary search to find the index of the ceiling of `t - 3000`, then `list.size() - index` is the answer.

`binarySearch()` cost `O(logN)`.
```

    List<Integer> list;

    public RecentCounter() {
        list = new ArrayList<>();
    }
    
    public int ping(int t) {
        list.add(t);
        int index = Collections.binarySearch(list, t - 3000); // search the index of t - 3000.
        if (index < 0) { index = ~index; } // if t - 3000 is not in list, use the index of the ceiling of t - 3000.
        return list.size() - index;
    }
```
**Python 3: bisect**
```
    def __init__(self):
        self.ls = []

    def ping(self, t: int) -> int:
        self.ls.append(t)
        return len(self.ls) - bisect.bisect_left(self.ls, t - 3000)
```
**Mehod 4:** `Queue`.

Time & space: `O(Math.min(N, 3000))`.
**Java**
```
    Queue<Integer> q;

    public RecentCounter() {
        q = new LinkedList<>();
    }
    
    public int ping(int t) {
        q.offer(t);
        while (q.peek() < t - 3000) { q.poll(); }
        return q.size();
    }
```
**Python 3: deque**
```
    def __init__(self):
        self.dq = collections.deque()

    def ping(self, t: int) -> int:
        self.dq.append(t)
        while self.dq[0] < t - 3000:
            self.dq.popleft()
        return len(self.dq)
```
**Method 5: Fixed size array to implement Circular List**
By **@wushangzhen**

```
    int[] time;
    public RecentCounter() {
        time = new int[3001];
    }
    
    public int ping(int t) {
        int res = 0;
        time[t % 3001] = t;
        for (int i = 0; i < 3001; i++) {
            if (time[i] != 0 && time[i] >= t - 3000) {
                res += 1;
            }
        }
        return res;
    }
```
**Note:** the following Python 3 implementation of the method get **TLE**:
Let me know if you have any idea to make it faster.
```
    def __init__(self):
        self.time = [0] * 3001

    def ping(self, t: int) -> int:
        self.time[t % 3001] = t
        return sum([self.time[i] and self.time[i] >= t - 3000 for i in range(3001)])
```
</p>


### Can someone please explain this question to me?
- Author: fadi17
- Creation Date: Sun May 26 2019 05:35:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 26 2019 05:35:55 GMT+0800 (Singapore Standard Time)

<p>
Can someone please explain this question to me?
</p>


