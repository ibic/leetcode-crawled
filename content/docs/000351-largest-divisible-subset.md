---
title: "Largest Divisible Subset"
weight: 351
#id: "largest-divisible-subset"
---
## Description
<div class="description">
<p>Given a set of <b>distinct</b> positive integers, find the largest subset such that every pair (S<sub>i</sub>, S<sub>j</sub>) of elements in this subset satisfies:</p>

<p>S<sub>i</sub> % S<sub>j</sub> = 0 or S<sub>j</sub> % S<sub>i</sub> = 0.</p>

<p>If there are multiple solutions, return any subset is fine.</p>

<p><strong>Example 1:</strong></p>

<div>
<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,2,3]</span>
<strong>Output: </strong><span id="example-output-1">[1,2] </span>(of course, [1,3] will also be ok)
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[1,2,4,8]</span>
<strong>Output: </strong><span id="example-output-2">[1,2,4,8]</span>
</pre>
</div>
</div>
</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 
#### Mathematics

Before elaborating the solutions, we give some corollaries that one can derive from the property of modulo operation, which would prove useful later to solve the problem.

Given a list of values `[E, F, G]` sorted in ascending order (_i.e._ `E < F < G`), and the list itself forms a divisible subset as described in the problem, then we could extend the subset without _**enumerating**_ all numbers in the subset, in the following two cases:

- **Corollary I:** For any value that can be divided by the *largest* element in the divisible subset, by adding the new value into the subset, one can form another divisible subset, _i.e._ for all `h`, if `h % G == 0`, then `[E, F, G, h]` forms a new divisible subset.

- **Corollary II:** For all value that can divide the *smallest* element in the subset, by adding the new value into the subset, one can form another divisible subset, _i.e._ for all `d`, if `E % d == 0`, then `[d, E, F, G]` forms a new divisible subset.

The above two corollaries could help us to structure an efficient solution, since it suffices to have just one comparison in order to extend the subset.
<br /> 
<br />

---
#### Approach 1: Dynamic Programming

**Intuition**

At the first glance, the problem might seem to be similar with those combination problems such as [two sum](https://leetcode.com/problems/two-sum/) and [3sum](https://leetcode.com/problems/3sum/). Indeed, like those combinations problems, it turned out to be rather helpful to _**sort**_ the original list first, which would help us to reduce the number of enumerations at the end.

As another benefit of sorting the original list, we would be able to apply the mathematical corollaries explained at the beginning of the article.  

So first of all, we sort the original list. And as it turns out, this is another dynamic programming problem. The key of solving a dynamic programming problem is to formulate the problem in a _recursive_ and _sound_ way.

Here is our attempt, which you would see some theoretical supports later.
>For an ordered list $$[X_1, X_2, ... X_n]$$, we claim that the *largest* divisible subset from this list is the largest subset among all possible divisible subsets that **end with** each of the number in the list.

Let us define a function named $$\text{EDS}(X_i)$$, which gives the largest divisible subset that ends with the number $$X_i$$. By "_ends with_", we mean that the number $$X_i$$ should be the largest number in the subset. For example, given the list $$[2, 4, 7, 8]$$, let us calculate $$\text{EDS}(4)$$ by enumeration. First, we list all divisible subsets that ends with $$4$$, which should be $$\{4\}$$ and $$\{2, 4\}$$. Then by definition, we have $$\text{EDS}(4) = \{2, 4\}$$. Similarly, one can obtain that $$\text{EDS}(2) = \{2\}$$ and $$\text{EDS}(7) = \{7\}$$.

**Note:** a single number itself forms a divisible subset as well, though it might not be clearly stated in the problem statement. 

Finally let us define our target function that gives the largest divisible subset from an order list $$[X_1, X_2, ... X_n]$$, as $$\text{LDS}([X_1, X_2, ... X_n])$$. Now, without further due, we claim that the following equation should hold:
$$
    \text{LDS}([X_1, X_2,...X_n]) = \max{ ( \forall \space \text{EDS}(X_i) )} \space, \space 1 \le i \le n   \quad \quad (1)
$$

We could prove the above formula literally _by definition_. First of all, $$\forall \space \text{EDS}(X_i)$$ cover the divisible subsets in all cases (_i.e._ subsets ends with $$X_i$$). And then we pick the largest one among the largest subsets.

>Furthermore, we could calculate the function $$\text{EDS}(X_i)$$ recursively, with the corollaries we defined at the beginning of the article.


**Algorithm**

Let us explain the algorithm on how to calculate $$\text{EDS}(X_i)$$, following the above example with the list of $$[2, 4, 7, 8]$$. As a reminder, previously we have obtained the $$\text{EDS}(X_i)$$ value for all elements less than 8, _i.e._:
$$
\text{EDS}(2) = \{2\} \quad \text{EDS}(4) = \{2, 4\} \quad \text{EDS}(7) = \{7\}
$$
To obtain $$\text{EDS}(8)$$, we simply enumerate all elements before $$8$$ and their $$\text{EDS}(X_i)$$ values respectively, with the following procedure:

![pic](../Figures/368/368_dp.png)

- If the number $$8$$ can be divided by the element $$X_i$$, then by appending the number $$8$$ to $$\text{EDS}(X_i)$$ we obtain another divisible subset that ends with $$8$$, according to our **Corollary I**. And this new subset stands as a potential value for $$\text{EDS}(8)$$. For example, since $$8 \bmod 2 == 0$$, therefore $$\{2, 8\}$$ could be the final value for $$\text{EDS}(8)$$, and similarly with the subset $$\{2, 4, 8\}$$ obtained from $$\text{EDS}(4)$$

- If the number $$8$$ can NOT be divided by the element $$X_i$$, then we could be sure that the value of $$\text{EDS}(X_i)$$ would not contribute to $$\text{EDS}(8)$$, according to the definition of divisible subset. For example, the subset $$\text{EDS}(7) = \{7\}$$ has no impact for $$\text{EDS}(8)$$. 

- We then pick the largest new subsets that we form with the help of $$\text{EDS}(X_i)$$. Particularly, the subset $$\{8\}$$ stands for a valid candidate for $$\text{EDS}(8)$$. And in a hypothetical case where $$8$$ cannot be divided by any of its previous elements, we would have $$\text{EDS}(8) = \{8\}$$.

Here we give some sample implementation based on the above idea. _Note:_ for the Python implementation, we showcase the one from [StefanPochmann](https://leetcode.com/problems/largest-divisible-subset/discuss/84002/4-lines-in-Python) for its conciseness and efficiency.

<iframe src="https://leetcode.com/playground/a6EYi9qc/shared" frameBorder="0" width="100%" height="500" name="a6EYi9qc"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N^2)$$. In the major loop of the algorithm, we need to calculate the $$\text{EDS}(X_i)$$ for each element in the input list. And for each $$\text{EDS}(X_i)$$ calculation, we need to enumerate all elements before $$X_i$$. As a result, we end up with the $$\mathcal{O}(N^2)$$ time complexity. 

* Space complexity : $$\mathcal{O}(N^2)$$. We maintain a container to keep track of $$\text{EDS}(X_i)$$ value for each element in the list. And in the worst case where the entire list is a divisible set, the value of $$\text{EDS}(X_i)$$ would be the sublist of $$[X_1, X_2...X_i]$$. As a result, we end up with the $$\mathcal{O}(N^2)$$ space complexity.
<br /> 
<br />


---
#### Approach 2: Dynamic Programming with Less Space

**Intuition**

Following the same intuition of dynamic programming, however we could do a bit better on the *space complexity*.

Instead of keeping the largest divisible subset for each of the input elements, _i.e._ $$\text{EDS}(X_i)$$, we could simply record its size, namely $$\text{size}(\text{EDS}(X_i))$$. As a result, we reduce the space complexity from $$\mathcal{O}(N^2)$$ to $$\mathcal{O}(N)$$.

In exchange, we need to reconstruct the largest divisible subset at the end, which is a tradeoff between time and space.

**Algorithm**

The main algorithm remains almost the same as the Approach #1, which includes calculating **the size** of the largest divisible subset that ends with each element $$X_i$$. We denote this resulting vector as `dp[i]`. 

The difference is that we need some additional logic to extract the resulting subset from `dp[i]`. Here we elaborate the procedure with a concrete example shown in the graph below.

![pic](../Figures/368/368_dp_less_space.png)

In the upper part of the graph, we have a list of elements ($$X_i$$) sorted in ascending order. And in the lower part of the graph, we have the size value of the largest divisible subset that ends with each element $$X_i$$.

To facilitate the reading, we draw a link between each element $$X_i$$ with its neighbor element in its largest divisible subset. For example, for the element $$X_i = 8$$, the resulting largest divisible subset that ends with $$8$$ would be $$\{2, 4, 8\}$$, and we see a link between $$8$$ and its neighbor $$4$$.

- The reconstruction of the resulting subset begins with finding the largest size (_i.e._ $$4$$) and its index in `dp[i]`. The resulting largest divisible subset would end with the element who has the max value of `dp[i]`.

- Then starting from the index of the largest size, we run a loop to go backwards (from $$X_i$$ to $$X_1$$) to find the next element that should be included in the resulting subset.

- We have two criteria to determine this next element: (1). the element should be able to divide the previous tail element in the resulting subset, _e.g._ $$ 16 \bmod 8 == 0$$. (2). The value of `dp[i]` should correspond to the current size of the divisible subset, _e.g._ the element 7 would NOT be the next neighbor element after the element 8, since their `dp[i]` values do not match up.


<iframe src="https://leetcode.com/playground/ee8BFxbr/shared" frameBorder="0" width="100%" height="500" name="ee8BFxbr"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N^2)$$. The additional logic to reconstruct the resulting subset would take us an extra $$\mathcal{O}(N)$$ time complexity which is inferior to the main loop that takes $$\mathcal{O}(N^2)$$. 

* Space complexity : $$\mathcal{O}(N)$$. The vector `dp[i]` that keeps tracks of the size of the largest divisible subset which ends with each of the elements, would take $$\mathcal{O}(N)$$ space in all cases.
<br /> 
<br />

---
#### Approach 3: Recursion with Memoization

**Intuition**

A typical code pattern for dynamic programming problems is to maintain a matrix or vector of intermediate solutions, and having one or two loops that traverse the matrix or vector. During the loop, we reuse the intermediate solutions instead of recalculating them at each occasion.

As one might notice, the above code pattern reminds us the techniques of recursion with memoization. Indeed, there is more than one way to implement the solution with the methodology of dynamic programming. 

**Algorithm**

Once we figure out the essence of the problem, as summarized in the **Formula (1)**, it might be more intuitive to implement it via recursion with memoization.

Here we highlight the importance of applying **memoization** together with the recursion, in this case. 

Following the same example in Approach #1, we draw the call graph below for the calculation of $$\text{EDS}(8)$$, where each node represents the invocation of the function $$\text{EDS}(X_i)$$ and on the edge we indicate the sequence of the invocations. 

![pic](../Figures/368/368_recursion_memoization.png)

As one can see, if we do not keep the intermediate results, the number of calculation would grow exponentially with the length of the list.

<iframe src="https://leetcode.com/playground/tgSRwMct/shared" frameBorder="0" width="100%" height="500" name="tgSRwMct"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N^2)$$.

    - In the above implementation, we adopt the bottom-up strategy where we first calculate the $$\text{EDS}(X_i)$$ for elements with lower index. Through the memoization technique, the latter $$\text{EDS}(X_i)$$ calculation could reuse the intermediate ones. As a result, we reach the same time complexity as in the Approach #1.


* Space complexity : $$\mathcal{O}(N^2)$$.

    - In this implementation, we decide to keep the subset instead of its size, as the intermediate solutions. As we discussed in previous approaches, this would lead to the $$\mathcal{O}(N^2)$$ space complexity, with the worst scenario where the entire input list being a divisible set.

    - In addition, due to the use of recursion, the program would incur a maximum $$\mathcal{O}(N)$$ space for the call stacks, during the invocation of $$\text{EDS}(X_n)$$ for the last element in the ordered list. Though, overall the space complexity remains as $$\mathcal{O}(N^2)$$.
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 3 STEPS [c++ | python | java] [dp] PEN PAPER DIAGRAM explanation simple and clear
- Author: amit_gupta10
- Creation Date: Sat Jun 13 2020 15:08:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 13 2020 20:03:35 GMT+0800 (Singapore Standard Time)

<p>
***NOTE:- if you found anyone\'s post helpful please upvote that post because some persons are downvoting unneccesarily, and you are the one guys that save our post from getting unvisible, upvote for remains visible for others so and other people can also get benefitted***

**DESCRIPTION:-**

**Given an array the task is to largest divisible subset in array. A subset is called divisible if for every pair (x, y) in subset, either x divides y or y divides x.**

**Examples:**

Input  : arr[] = {1, 16, 7, 8, 4}
Output : 16 8 4 1 or 1 4 8 16

**An efficient solution involves following steps.**

**1.  Sort all array elements in increasing order. The purpose of sorting is to make sure that all divisors of an element appear before it.
2.  Create an array dp[] of same size as input array. dp[i] stores size of divisible subset ending with arr[i] (In sorted array). The minimum value of dp[i] would be 1.
3.  Traverse all array elements. For every element, find a divisor arr[j] with largest value of dp[j] and store the value of dp[i] as dp[j] + 1.**

![image](https://assets.leetcode.com/users/amit_gupta10/image_1592042710.png)












**c++ code**

```
class Solution {
public:
    vector<int> largestDivisibleSubset(vector<int>& nums) {
        if(nums.size()==0){
            return nums;
        }
        sort(nums.begin(),nums.end());
        int flag=1;
        if(nums[0]==1){
            flag=0;
        }
        else{
            nums.push_back(1);
            flag=1;
        }
        sort(nums.begin(),nums.end());
        int i,j;
        int dp[nums.size()][2];
        for(i=nums.size()-1;i>=0;i--){
            dp[i][0]=0;
            dp[i][1]=i;
            for(j=i+1;j<nums.size();j++){
                if((nums[j]%nums[i])==0){
                    if(dp[j][0]>dp[i][0]){
                        dp[i][0]=dp[j][0];
                        dp[i][1]=j;
                    }  
                }
            }
            dp[i][0]++;
        }
        i=0;
        vector<int> t;
        t.push_back(nums[i]);
        while(dp[i][1]!=i){
            i=dp[i][1];
            t.push_back(nums[i]);
            
        }
        if(flag==1){
            t.erase(t.begin());
        }
        return t;
        
    }
};
```

*Runtime: 52 ms, faster than 43.10% of C++ online submissions for Largest Divisible Subset.
Memory Usage: 8.6 MB, less than 52.64% of C++ online submissions for Largest Divisible Subset.*

**python code**

```
class Solution(object):
     def largestDivisibleSubset(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        n = len(nums)
        if n <= 1:
            return nums
        nums.sort()
        dp = [(0, 0)] * n
        dp[0] = (1, 0)
        maxIndex, maxVal = 0, 1
        for i in range(1, n):
            dp[i] = max((dp[j][0] + 1, j) for j in range(i + 1) if nums[i] % nums[j] is 0)
            if dp[i] > maxVal:
                maxIndex, maxVal = i, dp[i]
        i, lds = maxIndex, [nums[maxIndex]]
        while i != dp[i][1]:
            i = dp[i][1]
            lds.append(nums[i])
        return lds
        
```

*Runtime: 436 ms, faster than 26.39% of Python online submissions for Largest Divisible Subset.
Memory Usage: 13 MB, less than 24.36% of Python online submissions for Largest Divisible Subset.*


**java**

```
class Solution {
  // if we sort the array, every element in a divisibleSubset can be divisible by the element just before it.
// for any element k, its largestDivisibleSubset that ends with k can be formed in the following way: 
// use element k after any one of the previous elements that is divisble 
public List<Integer> largestDivisibleSubset(int[] nums) {
    int[] l = new int[nums.length]; // the length of largestDivisibleSubset that ends with element i
    int[] prev = new int[nums.length]; // the previous index of element i in the largestDivisibleSubset ends with element i
    
    Arrays.sort(nums);
    
    int max = 0;
    int index = -1;
    for (int i = 0; i < nums.length; i++){
        l[i] = 1;
        prev[i] = -1;
        for (int j = i - 1; j >= 0; j--){
            if (nums[i] % nums[j] == 0 && l[j] + 1 > l[i]){
                l[i] = l[j] + 1;
                prev[i] = j;
            }
        }
        if (l[i] > max){
            max = l[i];
            index = i;
        }
    }
    List<Integer> res = new ArrayList<Integer>();
    while (index != -1){
        res.add(nums[index]);
        index = prev[index];
    }
    return res;
}

}
```

*Runtime: 16 ms, faster than 78.50% of Java online submissions for Largest Divisible Subset.
Memory Usage: 39.4 MB, less than 87.96% of Java online submissions for Largest Divisible Subset.*

references:-  geeks

**if it helps please upvote if you have any doubt comment it**
</p>


### Classic DP solution similar to LIS, O(n^2)
- Author: jiangbowei2010
- Creation Date: Wed Jun 29 2016 10:31:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 12:18:36 GMT+0800 (Singapore Standard Time)

<p>
Use DP to track max Set and pre index.

    public class Solution {
        public List<Integer> largestDivisibleSubset(int[] nums) {
            int n = nums.length;
            int[] count = new int[n];
            int[] pre = new int[n];
            Arrays.sort(nums);
            int max = 0, index = -1;
            for (int i = 0; i < n; i++) {
                count[i] = 1;
                pre[i] = -1;
                for (int j = i - 1; j >= 0; j--) {
                    if (nums[i] % nums[j] == 0) {
                        if (1 + count[j] > count[i]) {
                            count[i] = count[j] + 1;
                            pre[i] = j;
                        }
                    }
                }
                if (count[i] > max) {
                    max = count[i];
                    index = i;
                }
            }
            List<Integer> res = new ArrayList<>();
            while (index != -1) {
                res.add(nums[index]);
                index = pre[index];
            }
            return res;
        }
    }
</p>


### 4 lines in Python
- Author: StefanPochmann
- Creation Date: Mon Jun 27 2016 17:52:24 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 01:28:30 GMT+0800 (Singapore Standard Time)

<p>
    def largestDivisibleSubset(self, nums):
        S = {-1: set()}
        for x in sorted(nums):
            S[x] = max((S[d] for d in S if x % d == 0), key=len) | {x}
        return list(max(S.values(), key=len))

My `S[x]` is the largest subset with `x` as the largest element, i.e., the subset of all divisors of `x` in the input. With `S[-1] = emptyset` as useful base case. Since divisibility is transitive, a multiple `x` of some divisor `d` is also a multiple of all elements in `S[d]`, so it's not necessary to explicitly test divisibility of `x` by all elements in `S[d]`. Testing `x % d` suffices.

While storing entire subsets isn't super efficient, it's also not that bad. To extend a subset, the new element must be divisible by all elements in it, meaning it must be at least twice as large as the largest element in it. So with the 31-bit integers we have here, the largest possible set has size 31 (containing all powers of 2).
</p>


