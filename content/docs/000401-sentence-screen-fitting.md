---
title: "Sentence Screen Fitting"
weight: 401
#id: "sentence-screen-fitting"
---
## Description
<div class="description">
<p>Given a <code>rows x cols</code> screen and a sentence represented by a list of <b>non-empty</b> words, find <b>how many times</b> the given sentence can be fitted on the screen.
</p>

<p><b>Note:</b>
<ol>
<li>A word cannot be split into two lines.</li>
<li>The order of words in the sentence must remain unchanged.</li>
<li>Two consecutive words <b>in a line</b> must be separated by a single space.</li>
<li>Total words in the sentence won't exceed 100.</li>
<li>Length of each word is greater than 0 and won't exceed 10.</li>
<li>1 &le; rows, cols &le; 20,000.</li>
</ol>
</p>

<p>
<b>Example 1:</b> 
<pre>
<b>Input:</b>
rows = 2, cols = 8, sentence = ["hello", "world"]

<b>Output:</b> 
1

<b>Explanation:</b>
hello---
world---

The character '-' signifies an empty space on the screen.
</pre>
</p>

<p>
<b>Example 2:</b> 
<pre>
<b>Input:</b>
rows = 3, cols = 6, sentence = ["a", "bcd", "e"]

<b>Output:</b> 
2

<b>Explanation:</b>
a-bcd- 
e-a---
bcd-e-

The character '-' signifies an empty space on the screen.
</pre>
</p>

<p>
<b>Example 3:</b> 
<pre>
<b>Input:</b>
rows = 4, cols = 5, sentence = ["I", "had", "apple", "pie"]

<b>Output:</b> 
1

<b>Explanation:</b>
I-had
apple
pie-I
had--

The character '-' signifies an empty space on the screen.
</pre>
</p>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 2 (taggedByAdmin: true)
- Robinhood - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 21ms 18 lines Java solution
- Author: kylejao
- Creation Date: Mon Oct 10 2016 06:42:06 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 09:18:23 GMT+0800 (Singapore Standard Time)

<p>
**Update**: See @iambright's post below for optimized code.
**Update**: If you want to shorten the code by getting rid of either the while loop or the if-else check, see update below.
```
public class Solution {
    public int wordsTyping(String[] sentence, int rows, int cols) {
        String s = String.join(" ", sentence) + " ";
        int start = 0, l = s.length();
        for (int i = 0; i < rows; i++) {
            start += cols;
            if (s.charAt(start % l) == ' ') {
                start++;
            } else {
                while (start > 0 && s.charAt((start-1) % l) != ' ') {
                    start--;
                }
            }
        }
        
        return start / s.length();
    }
}
```
**Update (4/7/2017)**: There is a way to get rid of the if-else check (see discussion below). If you would like to shorten the code, see the shorter code below first.
```
    public int wordsTyping(String[] sentence, int rows, int cols) {
        String s = String.join(" ", sentence) + " ";
        int[] offset = new int[s.length()];
        IntStream.range(1, s.length()).forEach(i -> offset[i] = s.charAt(i) == ' ' ? 1 : offset[i-1]-1);
        return IntStream.range(0, rows).reduce(0, (a, b) -> a + cols + offset[(a+cols) % s.length()]) / s.length();
    }
```

**Explanation**:

Say `sentence=["abc", "de", "f]`, `rows=4`, and `cols=6`.
The screen should look like
```
"abc de"
"f abc "
"de f  "
"abc de"
```
Consider the following repeating **sentence string**, with positions of the start character of each row on the screen.
```
"abc de f abc de f abc de f ..."
 ^      ^     ^    ^      ^
 0      7     13   18     25
```
Our goal is to find the start position of the row next to the last row on the screen, which is 25 here. Since actually it's the length of everything earlier, we can get the answer by dividing this number by the length of (non-repeated) **sentence string**. Note that the non-repeated **sentence string** has a space at the end; it is `"abc de f "` in this example.

Here is how we find that position. In each iteration, we need to adjust `start` based on spaces either added or removed.
```
"abc de f abc de f abc de f ..." // start=0
 012345                          // start=start+cols+adjustment=0+6+1=7 (1 space removed in screen string)
        012345                   // start=7+6+0=13
              012345             // start=13+6-1=18 (1 space added)
                   012345        // start=18+6+1=25 (1 space added)
                          012345
```
Hope this helps.
</p>


### JAVA optimized solution 17ms
- Author: tjcd
- Creation Date: Sun Oct 09 2016 22:35:53 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 22:47:52 GMT+0800 (Singapore Standard Time)

<p>
First off, we can easily come up with a brute-force solution. The basic idea of optimized solution is that 
1. sub-problem: if there's a new line which is starting with certain index in sentence, what is the starting index of next line (nextIndex[]). BTW, we compute how many times the pointer in current line passes over the last index (times[]).
2. relation :  ans += times[i], i = nextIndex[i], for _ in 0..<row. where i indicates what is the first word in the current line.

Time complexity : O(n*(cols/lenAverage)) + O(rows), where n is the length of sentence array, lenAverage is the average length of the words in the input array.

Well, It's not a typical "DP" problem and I am not even sure it is a "DP" problem. ( \u0361\xb0 \u035c\u0296 \u0361\xb0)

```
public int wordsTyping(String[] sentence, int rows, int cols) {
        int[] nextIndex = new int[sentence.length];
        int[] times = new int[sentence.length];
        for(int i=0;i<sentence.length;i++) {
            int curLen = 0;
            int cur = i;
            int time = 0;
            while(curLen + sentence[cur].length() <= cols) {
                curLen += sentence[cur++].length()+1;
                if(cur==sentence.length) {
                    cur = 0;
                    time ++;
                }
            }
            nextIndex[i] = cur;
            times[i] = time;
        }
        int ans = 0;
        int cur = 0;
        for(int i=0; i<rows; i++) {
            ans += times[cur];
            cur = nextIndex[cur];
        }
        return ans;
    }
```
</p>


### 12ms Java solution using DP
- Author: destinywz
- Creation Date: Fri Oct 28 2016 10:17:31 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 13:17:07 GMT+0800 (Singapore Standard Time)

<p>
It's kind of like a jump game. I use a array to record for each word, how far it can jump.
eg. dp[index] means if the row start at index then the start of next row is dp[index].
dp[index] can be larger than the length of the sentence, in this case, one row can span multiple sentences.
I comment the check whether a word is longer than the row since there is no such test case. But it's better to check it. And it make little difference to the speed.
```
public class Solution {
    public int wordsTyping(String[] sentence, int rows, int cols) {
        int[] dp = new int[sentence.length];
        int n = sentence.length;
        for(int i = 0, prev = 0, len = 0; i < sentence.length; ++i) {
            // remove the length of previous word and space
            if(i != 0 && len > 0) len -= sentence[i - 1].length() + 1;
            // calculate the start of next line.
            // it's OK the index is beyond the length of array so that 
            // we can use it to count how many words one row has.
            while(len + sentence[prev % n].length() <= cols) len += sentence[prev++ % n].length() + 1;
            dp[i] = prev;
        }
        int count = 0;
        for(int i = 0, k = 0; i < rows; ++i) {
            // count how many words one row has and move to start of next row.
            // It's better to check if d[k] == k but I find there is no test case on it. 
            // if(dp[k] == k) return 0;
            count += dp[k] - k;
            k = dp[k] % n;
        }
        // divide by the number of words in sentence
        return count / n;
    }
}
```
</p>


