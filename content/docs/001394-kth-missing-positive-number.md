---
title: "Kth Missing Positive Number"
weight: 1394
#id: "kth-missing-positive-number"
---
## Description
<div class="description">
<p>Given an array <code>arr</code>&nbsp;of positive integers&nbsp;sorted in a <strong>strictly increasing order</strong>, and an integer <code><font face="monospace">k</font></code>.</p>

<p><em>Find the </em><font face="monospace"><code>k<sup>th</sup></code></font><em>&nbsp;positive integer that is missing from this array.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,3,4,7,11], k = 5
<strong>Output:</strong> 9
<strong>Explanation: </strong>The missing positive integers are [1,5,6,8,9,10,12,13,...]. The 5<sup>th</sup>&nbsp;missing positive integer is 9.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,3,4], k = 2
<strong>Output:</strong> 6
<strong>Explanation: </strong>The missing positive integers are [5,6,7,...]. The 2<sup>nd</sup> missing positive integer is 6.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 1000</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 1000</code></li>
	<li><code>1 &lt;= k &lt;= 1000</code></li>
	<li><code>arr[i] &lt; arr[j]</code> for <code>1 &lt;= i &lt; j &lt;= arr.length</code></li>
</ul>
</div>

## Tags
- Array (array)
- Hash Table (hash-table)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] O(logN)
- Author: lee215
- Creation Date: Sun Aug 09 2020 00:10:35 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 26 2020 10:56:06 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
Assume the final result is `x`,
And there are `m` number not missing in the range of `[1, x]`.
Binary search the `m` in range `[0, A.size()]`.

If there are `m` number not missing,
that is `A[0], A[1] .. A[m-1]`,
the number of missing under `A[m] - 1` is `A[m] - 1 - m`.

If `A[m] - 1 - m < k`, `m` is too small, we update `left = m`.
If `A[m] - 1 - m >= k`, `m` is big enough, we update `right = m`.

Note that, we exit the while loop, `l = r`,
which equals to the number of missing number used.
So the Kth positive number will be `l + k`.
<br>

# **Complexity**
Time `O(logN)`
Space `O(1)`
<br>

**Java:**
```java
    public int findKthPositive(int[] A, int k) {
        int l = 0, r = A.length, m;
        while (l < r) {
            m = (l + r) / 2;
            if (A[m] - 1 - m < k)
                l = m + 1;
            else
                r = m;
        }
        return l + k;
    }
```
**C++:**
```cpp
    int findKthPositive(vector<int>& A, int k) {
        int l = 0, r = A.size(), m;
        while (l < r) {
            m = (l + r) / 2;
            if (A[m] - 1 - m < k)
                l = m + 1;
            else
                r = m;
        }
        return l + k;
    }
```
**Python:**
```py
    def findKthPositive(self, A, k):
        l, r = 0, len(A)
        while l < r:
            m = (l + r) / 2
            if A[m] - 1 - m < k:
                l = m + 1
            else:
                r = m
        return l + k
```

**Python, using bisect**
Suggested by @r0bertz
```py
    def findKthPositive(self, A, k):
        class Count(object):
            def __getitem__(self, i):
                return A[i] - i - 1
        return k + bisect.bisect_left(Count(), k, 0, len(A))
```
<br><br>

# More Good Binary Search Problems
Here are some similar binary search problems.
Also find more explanations.
Good luck and have fun.

- 1539. [Kth Missing Positive Number](https://leetcode.com/problems/kth-missing-positive-number/discuss/779999/JavaC++Python-O(logN))
- 1482. [Minimum Number of Days to Make m Bouquets](https://leetcode.com/problems/minimum-number-of-days-to-make-m-bouquets/discuss/686316/javacpython-binary-search/578488)
- 1283. [Find the Smallest Divisor Given a Threshold](https://leetcode.com/problems/find-the-smallest-divisor-given-a-threshold/discuss/446376/javacpython-bianry-search/401806)
- 1231. [Divide Chocolate](https://leetcode.com/problems/divide-chocolate/discuss/408503/Python-Binary-Search)
- 1011. [Capacity To Ship Packages In N Days](https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/discuss/256729/javacpython-binary-search/351188?page=3)
- 875. [Koko Eating Bananas](https://leetcode.com/problems/koko-eating-bananas/discuss/152324/C++JavaPython-Binary-Search)
- 774. [Minimize Max Distance to Gas Station](https://leetcode.com/problems/minimize-max-distance-to-gas-station/discuss/113633/Easy-and-Concise-Solution-using-Binary-Search-C++JavaPython)
- 410. [Split Array Largest Sum](https://leetcode.com/problems/split-array-largest-sum/)
<br>
</p>


### Java O(lgN) binary Search
- Author: hobiter
- Creation Date: Sun Aug 09 2020 01:43:44 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 10 2020 06:29:36 GMT+0800 (Singapore Standard Time)

<p>
we know it starts from 1, so 
```
arr[mid] - (mid + 1)
```
will be the count of number that missed.

Thanks to @caohuicn for explaining this for us:
" Here is my understanding of why l + k is the answer. ( a bit long, please comment if there\'s a more concise explanation):
* We are maintaining such invariant throughout the loop: l + k <= ans <= r + k. Obviously when the array is missing k or more elements in the beginning, ans == k; when there is no missing elements, ans is arr.length + k;
* When we update l = mid + 1, there are already mid + 1 non-missed elements on the left, and we still need k missed elements, so l + k <= ans still holds true;
* When we update r = mid, we know ans is less than arr[mid], and on the left of mid, there are mid non-missed elements, plus k or more missed elements, so ans is at most mid + k;
* Finally when l == r, we get l + k == ans == r + k "
```
    public int findKthPositive(int[] arr, int k) {
        int l = 0, r = arr.length;
        while (l < r) {
            int mid = l + (r - l) / 2;
            if (arr[mid] - (mid + 1) >= k) r = mid;  //missed more or equal than k numbers, left side;
            else l = mid + 1;   // missed less than k numbers, must be in the right side;
        }
        return l + k;
    }
```
</p>


### C++ O(n)
- Author: votrubac
- Creation Date: Sun Aug 09 2020 00:01:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 09 2020 00:01:46 GMT+0800 (Singapore Standard Time)

<p>
```cpp
int findKthPositive(vector<int>& arr, int k) {
    for (int n = 1, i = 0; n <= 1000; ++n) {
        if (i < arr.size() && arr[i] == n)
            ++i;
        else if (--k == 0)
            return n;
    }
    return 1000 + k;
}
```
</p>


