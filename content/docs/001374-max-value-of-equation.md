---
title: "Max Value of Equation"
weight: 1374
#id: "max-value-of-equation"
---
## Description
<div class="description">
<p>Given an&nbsp;array <code>points</code> containing the coordinates of points on a 2D plane,&nbsp;sorted by the x-values, where <code>points[i] = [x<sub>i</sub>, y<sub>i</sub>]</code>&nbsp;such that&nbsp;<code>x<sub>i</sub> &lt; x<sub>j</sub></code> for all <code>1 &lt;= i &lt; j &lt;= points.length</code>. You are also given an integer&nbsp;<code>k</code>.</p>

<p>Find the <em>maximum value of the equation </em><code>y<sub>i</sub>&nbsp;+ y<sub>j</sub>&nbsp;+ |x<sub>i</sub>&nbsp;- x<sub>j</sub>|</code>&nbsp;where <code>|x<sub>i</sub>&nbsp;- x<sub>j</sub>|&nbsp;&lt;= k</code>&nbsp;and <code>1 &lt;= i &lt; j &lt;= points.length</code>. It is guaranteed that there exists at least one pair of points that satisfy the constraint <code>|x<sub>i</sub>&nbsp;- x<sub>j</sub>|&nbsp;&lt;= k</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> points = [[1,3],[2,0],[5,10],[6,-10]], k = 1
<strong>Output:</strong> 4
<strong>Explanation:</strong> The first two points satisfy the condition |x<sub>i</sub>&nbsp;- x<sub>j</sub>| &lt;= 1 and if we calculate the equation we get 3 + 0 + |1 - 2| = 4. Third and fourth points also satisfy the condition and give a value of 10 + -10 + |5 - 6| = 1.
No other pairs satisfy the condition, so we return the max of 4 and 1.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> points = [[0,0],[3,0],[9,2]], k = 3
<strong>Output:</strong> 3
<strong>Explanation: </strong>Only the first two points have an absolute difference of 3 or less in the x-values, and give the value of 0 + 0 + |0 - 3| = 3.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= points.length &lt;= 10^5</code></li>
	<li><code>points[i].length == 2</code></li>
	<li><code>-10^8&nbsp;&lt;= points[i][0], points[i][1] &lt;= 10^8</code></li>
	<li><code>0 &lt;= k &lt;= 2 * 10^8</code></li>
	<li><code>points[i][0] &lt; points[j][0]</code>&nbsp;for all&nbsp;<code>1 &lt;= i &lt; j &lt;= points.length</code></li>
	<li><code>x<sub>i</sub></code>&nbsp;form a strictly increasing sequence.</li>
</ul>

</div>

## Tags
- Array (array)
- Sliding Window (sliding-window)

## Companies
- Google - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python] Priority Queue and Deque Solution, O(N)
- Author: lee215
- Creation Date: Sun Jun 28 2020 13:12:01 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 08 2020 22:50:17 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
Because `xi < xj`,
`yi + yj + |xi - xj| = (yi - xi) + (yj + xj)`

So we only need to find out the maximum `yi - xi`.
To find out the maximum element in a sliding window,
we can use priority queue or stack.
<br>

# Solution 1: Priority Queue
Time `O(NlogN)`
Space `O(N)`
<br>

**Python:**
```py
    def findMaxValueOfEquation(self, A, k):
        q = []
        res = -float(\'inf\')
        for x, y in A:
            while q and q[0][1] < x - k:
                heapq.heappop(q)
            if q: res = max(res, -q[0][0] + y + x)
            heapq.heappush(q, (x - y, x))
        return res
```
**Java**
By @blackspinner
```java
    public int findMaxValueOfEquation(int[][] points, int k) {
        PriorityQueue<Pair<Integer, Integer>> pq = new PriorityQueue<>((a, b) -> (a.getKey() == b.getKey() ? a.getValue() - b.getValue() : b.getKey() - a.getKey()));
        int res = Integer.MIN_VALUE;
        for (int[] point : points) {
            while (!pq.isEmpty() && point[0] - pq.peek().getValue() > k) {
                pq.poll();
            }
            if (!pq.isEmpty()) {
                res = Math.max(res, pq.peek().getKey() + point[0] + point[1]);
            }
            pq.offer(new Pair<>(point[1] - point[0], point[0]));
        }
        return res;
    }
```
<br>

# Solution 2: Deque
Time `O(N)`
Space `O(N)`
<br>

**Python**
```py
    def findMaxValueOfEquation(self, A, k):
        q = collections.deque()
        res = -float(\'inf\')
        for x, y in A:
            while q and q[0][1] < x - k:
                q.popleft()
            if q: res = max(res, q[0][0] + y + x)
            while q and q[-1][0] <= y - x:
                q.pop()
            q.append([y - x, x])
        return res
```
**Java**
By @blackspinner
```java
    public int findMaxValueOfEquation(int[][] points, int k) {
        Deque<Pair<Integer, Integer>> ms = new ArrayDeque<>();
        int res = Integer.MIN_VALUE;
        for (int[] point : points) {
            while (!ms.isEmpty() && point[0] - ms.peekFirst().getValue() > k) {
                ms.pollFirst();
            }
            if (!ms.isEmpty()) {
                res = Math.max(res, ms.peekFirst().getKey() + point[0] + point[1]);
            }
            while (!ms.isEmpty() && point[1] - point[0] > ms.peekLast().getKey()) {
                ms.pollLast();
            }
            ms.offerLast(new Pair<>(point[1] - point[0], point[0]));
        }
        return res;
    }
```
<br>

# More Good Stack Problems
Here are some problems that impressed me.
Good luck and have fun.

- 1425. [Constrained Subsequence Sum](https://leetcode.com/problems/constrained-subsequence-sum/discuss/597751/JavaC++Python-O(N)-Decreasing-Deque)
- 1130. [Minimum Cost Tree From Leaf Values](https://leetcode.com/problems/minimum-cost-tree-from-leaf-values/discuss/339959/One-Pass-O(N)-Time-and-Space)
- 907. [Sum of Subarray Minimums](https://leetcode.com/problems/sum-of-subarray-minimums/discuss/170750/C++JavaPython-Stack-Solution)
- 901. [Online Stock Span](https://leetcode.com/problems/online-stock-span/discuss/168311/C++JavaPython-O(1))
- 856. [Score of Parentheses](https://leetcode.com/problems/score-of-parentheses/discuss/141777/C++JavaPython-O(1)-Space)
- 503. [Next Greater Element II](https://leetcode.com/problems/next-greater-element-ii/discuss/98270/JavaC++Python-Loop-Twice)
- 496. Next Greater Element I
- 84. Largest Rectangle in Histogram
- 42. Trapping Rain Water
<br>
</p>


### [C++] Simple Solution using Max-Heap
- Author: supreethbaliga
- Creation Date: Sun Jun 28 2020 13:39:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 13:39:17 GMT+0800 (Singapore Standard Time)

<p>
Suppose i < j, then we can translate yi + yj + |xi - xj| to (yi - xi) + (yj + xj).

For a given point_j, since (yj + xj) is fixed, we only need to maximize the (yi - xi) among the previously seen point_i.

What data structure to use to efficiently find the biggest previously yi - xi for each point_j = (xj, yj)? MaxHeap!

Below is the C++ code:
```
class Solution {
public:
    int findMaxValueOfEquation(vector<vector<int>>& pts, int k) {
        priority_queue<pair<int, int>> pq; // max-heap
        pq.push({pts[0][1]-pts[0][0],pts[0][0]});
        int ans= INT_MIN;
        for(int i=1;i<pts.size();i++) {
            int sum = pts[i][0]+pts[i][1];
            while(!pq.empty() && pts[i][0]-pq.top().second>k) pq.pop();
            if(!pq.empty())ans = max(ans,sum+pq.top().first);
            pq.push({pts[i][1]-pts[i][0],pts[i][0]});
            cout<<i<<\' \'<<ans<<\'\
\';
        }
        cout<<\'\
\';
        return ans;
    }
};
```
</p>


### [Java] Max Heap O(nlogn) and Monotone Queue O(n)
- Author: haroldli
- Creation Date: Sun Jun 28 2020 13:21:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 23:44:35 GMT+0800 (Singapore Standard Time)

<p>
**Solution1: Max Heap O(nlogn)** 
```
class Solution {
    public int findMaxValueOfEquation(int[][] points, int k) {
        int result = Integer.MIN_VALUE;
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> (b[1] - b[0] - (a[1] - a[0])));
        for (int[] point : points) {
            while (!pq.isEmpty() && point[0] - pq.peek()[0] > k) {
                pq.poll();
            }
            if (!pq.isEmpty()) {
                int[] head = pq.peek();
                result = Math.max(result, point[1] + head[1] + point[0] - head[0]);
            }
            pq.offer(point);
        }
        
        return result;
    }
}
```

**Solution2: Monotonely Decreasing Queue O(n)**
```
class Solution {
    public int findMaxValueOfEquation(int[][] points, int k) {
        int result = Integer.MIN_VALUE;
        LinkedList<int[]> list = new LinkedList<>();
        for (int[] point : points) {
            while (list.size() > 0 && point[0] - list.getFirst()[0] > k) {
                list.pollFirst();
            }
            if (list.size() > 0) {
                int curVal = point[1] - point[0]; 
                result = Math.max(result, point[0] + point[1] + list.getFirst()[1] - list.getFirst()[0]);
                while (list.size() > 0 && (list.getLast()[1] - list.getLast()[0]) <= curVal) {
                    list.pollLast();
                }
            }
            list.offer(point);
        }
        
        return result; 
    }
}
```
</p>


