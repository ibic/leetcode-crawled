---
title: "Partition Equal Subset Sum"
weight: 399
#id: "partition-equal-subset-sum"
---
## Description
<div class="description">
<p>Given a <b>non-empty</b> array <code>nums</code> containing <b>only positive integers</b>, find if the array can be partitioned into two subsets such that the sum of elements in both subsets is equal.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,5,11,5]
<strong>Output:</strong> true
<strong>Explanation:</strong> The array can be partitioned as [1, 5, 5] and [11].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,5]
<strong>Output:</strong> false
<strong>Explanation:</strong> The array cannot be partitioned into equal sum subsets.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 200</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 100</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- eBay - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---

#### Overview ####

The problem is similar to the classic _Knapsack_ problem. The basic idea is to understand that to partition an array into two subsets of equal sum say $$\text{subSetSum}$$,  the $$\text{totalSum}$$ of given array must be twice the $$\text{subSetSum}$$

$$\text{totalSum} = \text{subsSetSum} * 2$$

This could also be written as,
$$\text{subSetSum} = \text{totalSum}/2$$

_Example_
Assume $$\text{totalSum}$$ of an array is $$20$$ and if we want to partition it into 2 subsets of equal sum, then the $$\text{subSetSum}$$ must be $$(20/2) = 10$$.

Now, the problem to find the subset with a sum equals a given target. Here target is $$\text{subSetSum}$$.

It must be noted that the total sum of an array must be _even_, only then we can divide it into 2 equal subsets. For instance, we cannot have an equal $$\text{subSetSum}$$ for an array with total sum as $$21$$.

**Note:**

>Finding a subset with a sum equal to a given target is different than [Subarray sum equals k](https://leetcode.com/problems/subarray-sum-equals-k/). Subarray is a contiguous sequence of array elements, whereas the subset could consist of any array elements regardless of the sequence. But each array element must belong to exactly one subset.

Let's discuss different algorithms to find the subset with a given sum.

---

#### Approach 1: Brute Force

**Intuition**

We have to find a subset in an array where the sum must be equal to $$\text{subSetSum}$$ that we calculated above. The brute force approach would be to generate all the possible subsets of an array and return true if we find a subset with the required sum.

**Algorithm**

Assume, there is an array $$\text{nums}$$ of size $$n$$ and we have to find if there exists a subset with total $$\text{sum} = \text{subSetSum}$$.
For a given array element $$x$$, there could be either of 2 possibilities:

- Case 1) $$x$$ is included in subset sum. $$\text{subSetSum} = \text{subSetSum} - x$$

- Case 2) $$x$$ is not included in subset sum, so we must take previous sum without $$x$$. $$\text{subSetSum} = \text{subSetSum}$$

We can use _depth first search_ and recursively calculate the $$\text{subSetSum}$$ for each case and check if either of them is true. This can be formulated as

```java
isSum (subSetSum, n) = isSum(subSetSum- nums[n], n-1) ||  isSum(subSetSum, n-1)
```

_Base Cases_

- If $$\text{subSetSum}$$ is $$0$$, return _true_ ( Since we found a subset with sum subSetSum )
- If $$\text{subSetSum}$$ is less than $$0$$, return _false_

<iframe src="https://leetcode.com/playground/8NiVUrbu/shared" frameBorder="0" width="100%" height="497" name="8NiVUrbu"></iframe>

**Complexity Analysis**

- Time Complexity : $$\mathcal{O}(2^{n})$$, where $$n$$ is equal to number of array elements.
The recursive solution takes the form of a binary tree where there are 2 possibilities for every array element and the maximum depth of the tree could be $$n$$.  The time complexity is exponential, hence this approach is exhaustive and results in _Time Limit Exceeded (TLE)_.

- Space Complexity: $$\mathcal{O}(N)$$ This space will be used to store the recursion stack. We can’t have more than $$n$$ recursive calls on the call stack at any time.

---

#### Approach 2: Top Down Dynamic Programming - Memoization

**Intuition**

In the above approach, we observe that the same subproblem is computed and solved multiple times.

Example :

![img](../Figures/416/subset_sum_rec_tree.png)

In the above recursion tree, we could see that   $$\text{isSum}( 3,[6] )$$  is computed twice and the result is always the same. Since the same subproblem is computed again and again, the problem has _Overlapping Subproblem_ property and can be solved using _Dynamic Programming_.

**Algorithm**

 We could have stored the results of our computation for the first time and used it later.
 This technique of computing once and returning the stored value is called memoization.
We use a two dimensional array $$\text{memo}$$ and follow the following steps for each recursive call :
- Check if subSetSum for a given $$n$$ exists in $$\text{memo}$$ to see if we can avoid computing the answer and return the result stored in memo.
- Save the results of any calculations to $$\text{memo}$$.

<iframe src="https://leetcode.com/playground/ZC4gCja9/shared" frameBorder="0" width="100%" height="500" name="ZC4gCja9"></iframe>

**Complexity Analysis**

- Time Complexity : $$\mathcal{O}(2^n)$$, where $$n$$ is the number of array elements. The calculated $$\text{subSetSum}$$ maybe always unique. If the sub-problems are not repeated, the worst-case time complexity would be the same as the non-memoized version in _Approach 1_.

- Space Complexity: $$\mathcal{O}(m \cdot n)$$, where $$n$$ is the number of array elements and $$m$$ is the $$\text{subSetSum}$$. We are using a 2 dimensional array $$\text{memo}$$ of size $$(m \cdot n)$$ and $$\mathcal{O}(n)$$ space to store the recursive call stack. This gives us the space complexity as $$\mathcal{O}(n)$$ + $$\mathcal{O}(m \cdot n)$$ = $$\mathcal{O}(m \cdot n)$$

---

#### Approach 3: Bottom Up Dynamic Programming

**Intuition**

This is another approach to solving the Dynamic Programming problems. We use the iterative approach and store the result of subproblems in bottom-up fashion also known as Tabulation.

**Algorithm**

We maintain a 2D array ,
$$\text{dp}[n][\text{subSetSum}]$$
For an array element $$i$$ and sum $$j$$ in array $$\text{nums}$$,

$$\text{dp}[i][j] =
\text{true}$$ if the sum $$j$$ can be formed by array elements in subset $$\text{nums[0]} .. \text{nums[i]}$$,otherwise $$\text{dp}[i][j] = \text{false}$$

$$\text{dp}[i][j]$$ is $$\text{true}$$ it satisfies one of the following conditions :

- Case 1) sum $$j$$ can be formed without including $$i^{th}$$ element,

$$\text{if } \text{dp}[i-1][j] ==   \text{true}$$

- Case 2) sum $$j$$ can be formed including $$i^{th}$$ element,

$$\text{if } \text{dp}[i-1][j - \text{nums}[i]] == \text{true}$$

!?!../Documents/416_LIS.json:1094,378!?!

<iframe src="https://leetcode.com/playground/FNNnL2Va/shared" frameBorder="0" width="100%" height="500" name="FNNnL2Va"></iframe>

**Complexity Analysis**

- Time Complexity : $$\mathcal{O}(m \cdot n)$$, where $$m$$ is the $$\text{subSetSum}$$, and $$n$$ is the number of array elements. We iteratively fill the array of size $$m \cdot n$$.

- Space Complexity : $$\mathcal{O}(m \cdot n)$$ , where $$n$$ is the number of array elements and $$m$$ is the $$\text{subSetSum}$$. We are using a 2 dimensional array $$\text{dp}$$ of size $$m \cdot n$$

---

#### Approach 4: Optimised Dynamic Programming - Using 1D Array

**Intuition**

We could further optimize _Approach 3_. We must understand that for any array element $$i$$, we need results of the previous iteration (i-1) only. Hence, we could achieve the same using a one-dimensional array as well.

<iframe src="https://leetcode.com/playground/3JHFhKxq/shared" frameBorder="0" width="100%" height="446" name="3JHFhKxq"></iframe>

**Complexity Analysis**

- Time Complexity : $$\mathcal{O}(m \cdot n)$$, where $$m$$ is the $$\text{subSetSum}$$, and $$n$$ is the number of array elements. The time complexity is the same as _Approach 3_.

- Space Complexity: $$\mathcal{O}(m)$$, As we use an array of size $$m$$ to store the result of subproblems.

**Note:**

The overall performance of _Approach 2_ is better than all the approaches discussed above. This is because we terminate our search as soon as we find a subset with the required sum. Hence, it performs better in most cases except for the worst case.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 0/1 knapsack detailed explanation
- Author: ZhuEason
- Creation Date: Sun Nov 13 2016 06:51:56 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jan 04 2019 17:24:01 GMT+0800 (Singapore Standard Time)

<p>
This problem is essentially let us to find whether there are several numbers in a set which are able to sum to a specific value (in this problem, the value is sum/2). 

Actually, this is a 0/1 knapsack problem, for each number, we can pick it or not. Let us assume dp[i][j] means whether the specific sum j can be gotten from the first i numbers. If we can pick such a series  of numbers from 0-i whose sum is j, dp[i][j] is true, otherwise it is false. 

Base case: dp[0][0] is true; (zero number consists of sum 0 is true)

Transition function: For each number, if we don\'t pick it, dp[i][j] = dp[i-1][j], which means if the first i-1 elements has made it to j, dp[i][j] would also make it to j (we can just ignore nums[i]). If we pick nums[i]. dp[i][j] = dp[i-1][j-nums[i]], which represents that j is composed of the current value nums[i] and the remaining composed of other previous numbers. Thus, the transition function is dp[i][j] = dp[i-1][j] || dp[i-1][j-nums[i]]

talking is cheap:

    public boolean canPartition(int[] nums) {
        int sum = 0;
        
        for (int num : nums) {
            sum += num;
        }
        
        if ((sum & 1) == 1) {
            return false;
        }
        sum /= 2;

        int n = nums.length;
        boolean[][] dp = new boolean[n+1][sum+1];
        for (int i = 0; i < dp.length; i++) {
            Arrays.fill(dp[i], false);
        }
        
        dp[0][0] = true;
        
        for (int i = 1; i < n+1; i++) {
            dp[i][0] = true;
        }
        for (int j = 1; j < sum+1; j++) {
            dp[0][j] = false;
        }
        
        for (int i = 1; i < n+1; i++) {
            for (int j = 1; j < sum+1; j++) {
                dp[i][j] = dp[i-1][j];
                if (j >= nums[i-1]) {
                    dp[i][j] = (dp[i][j] || dp[i-1][j-nums[i-1]]);
                }
            }
        }
       
        return dp[n][sum];
    }


But can we optimize it? It seems that we cannot optimize it in time. But we can optimize in space. We currently use two dimensional array to solve it, but we can only use one dimensional array. 

So the code becomes:

    public boolean canPartition(int[] nums) {
        int sum = 0;
        
        for (int num : nums) {
            sum += num;
        }
        
        if ((sum & 1) == 1) {
            return false;
        }
        sum /= 2;
        
        int n = nums.length;
        boolean[] dp = new boolean[sum+1];
        Arrays.fill(dp, false);
        dp[0] = true;
        
        for (int num : nums) {
            for (int i = sum; i > 0; i--) {
                if (i >= num) {
                    dp[i] = dp[i] || dp[i-num];
                }
            }
        }
        
        return dp[sum];
    }

</p>


### Whiteboard Editorial. All Approaches explained.
- Author: GeorgeChryso
- Creation Date: Sat Dec 28 2019 07:50:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 02 2020 01:07:36 GMT+0800 (Singapore Standard Time)

<p>
***-Buckle up Jimbo, this one\'s gonna be a doozy***

So, as I found this problem and the variations of its solution rather interesting, I will be explaining every possible approach. 

Let\'s start by the definition of our problem.
![image](https://assets.leetcode.com/users/georgechryso/image_1577486539.png)

However, we can manipulate the data in order to get to easier requirements.
![image](https://assets.leetcode.com/users/georgechryso/image_1577486605.png)

It is now clear, that all I need is to find a combination(subset) of my elements that sums up to the total sum of the array, divided by two.

First comes the easiest approach:
![image](https://assets.leetcode.com/users/georgechryso/image_1577486723.png)
Here\'s how to implement a **simple hashmap** where I store every possible combination\'s sum and a short example:
![image](https://assets.leetcode.com/users/georgechryso/image_1577486780.png)
```
var canPartition = A => {
    let totalSum = nums.reduce((acc, curr) => acc + curr);
    if (totalSum % 2) return false;

    const target = totalSum / 2;
    const memo = new Set([0]);

    for (let number of A) {
        let possibleSums = Array.from(memo);
        for (let possibleSum of possibleSums) {
            memo.add(possibleSum + number);
        }
    }
    return memo.has(target);
};
```

**BACKTRACKING/DFS**
![image](https://assets.leetcode.com/users/georgechryso/image_1577487038.png)

So I deduce that every possible combination is a series of choices for every item-candidate. That is whether to take the item, or ignore it.
![image](https://assets.leetcode.com/users/georgechryso/image_1577487140.png)

Let\'s demonstrate our solution with an example:
![image](https://assets.leetcode.com/users/georgechryso/image_1577487204.png)

```
//clear backtracking, TLE
var canPartition = (candidates) => {
    
    //sumA
    let target=candidates.reduce((acc, curr) => acc + curr)
    if (target%2) return false; //As we said our sum has to be dividible by two
    target/=2

    
    const backtracking = (currSum, index) => {
        //if our Sum is bigger than my target there\'s no reason to continue expanding
        if (currSum > target || index>=candidates.length)return false
        // when I reach my target, return true
        if (currSum === target)return true

        return backtracking(currSum + candidates[index],index+1)||backtracking(currSum,index+1)

    }
  
    return backtracking(0,0)
  } 

  //get\'s TLE\'d on 
  //[1,1,...,1,1,100]

```
As you can see, this solution doesnt pass a certain case, we can easily remedy that by modifying our method a bit

![image](https://assets.leetcode.com/users/georgechryso/image_1577487820.png)

So If I start from my target and follow the first approach, I see that I pass all my cases:

```
  var canPartition=candidates=>{
  
    candidates.sort((a, b) => b- a); //key for TLE :Essentially means: If you fail,do it fast
    let target=candidates.reduce((acc, curr) => acc + curr)
    if (target%2) return false;
    target/=2

    
    const backtracking = (remaining, index) => {
        if (remaining <candidates[index]  || index>=candidates.length)return false
        if (remaining === candidates[index])return true

        return backtracking(remaining-candidates[index],index+1)||backtracking(remaining,index+1)

    }
  
    return backtracking(target,0)
}

```

**KNAPSACK SOLUTION**
Here follows some theoretical background for the knapsack problem, It is key to understand this in order to fully grasp the solutions that follow.
![image](https://assets.leetcode.com/users/georgechryso/image_1577488032.png)

So, essentially, my task is to find the maximum value combination, given a capacity constraint.
![image](https://assets.leetcode.com/users/georgechryso/image_1577488159.png)
Let\'s dive into the formula 
![image](https://assets.leetcode.com/users/georgechryso/image_1577488207.png)
And here\'s a quick example for demonstration purposes
![image](https://assets.leetcode.com/users/georgechryso/image_1577488290.png)
And here\'s the matrix completed
![image](https://assets.leetcode.com/users/georgechryso/image_1577488336.png)


Back to **my problem**
![image](https://assets.leetcode.com/users/georgechryso/image_1577488379.png)
![image](https://assets.leetcode.com/users/georgechryso/image_1577488395.png)
As you can see, my outputs are not identical. Here\'s how that affects my formula.
![image](https://assets.leetcode.com/users/georgechryso/image_1577488485.png)
```
var canPartition = function(A) {
    //calculate the sum of my Array
    var sumA = A.reduce((acc, curr) => acc + curr);

    if (sumA % 2) return false;

    //create Rows
    // i want a row for each of my candidate elements+ one for my
    // 0th element( no element ) which I know for a fact can add up to 0 if selected
    var B = new Array(A.length + 1).fill(null);
    // create Columns
    // My final total sum ranges from 0 to sumA, which are totally sumA+1 candidate weights(sums)
    B = B.map(d => Array((sumA/2)+1).fill(false));

    // now that the matrix is created I have to use my base case which is:
    // If there is a way for me to get sum=0, with 0 elements
    B[0][0] = true;    // of course there is



    //here i=0 cos everything other column (sum) of this row cannot be created with 0 elements
    for (let i = 1; i <= A.length; i++) {
        for (let j = 0; j <= sumA / 2 ; j++) {
            //I know that i-1>=0 so i dont need an extra check for that
            if (j - A[i - 1] >= 0){
                B[i][j] = B[i - 1][j - A[i - 1]]||B[i - 1][j];
            }
            else{
                B[i][j] = B[i - 1][j];

            }
            
        }
    }
    
    
    return B[A.length][sumA/2];
};
```
**KNAPSACK OPTIMIZATIONS**

There\'s room for improvement on my last solution
![image](https://assets.leetcode.com/users/georgechryso/image_1577489410.png)

**2ROW APPROACH**
```
var canPartition = function(A) {
    var sumA = A.reduce((acc, curr) => acc + curr);

    if (sumA % 2) return false;
  
    var previousRow = new Array((sumA/2)+1).fill(false);
    var currentRow= new Array((sumA/2)+1).fill(false);
    
    previousRow[0] = true; // base case  


    for (let i = 1; i <= A.length; i++) {
        for (let j = 0; j <= sumA / 2 ; j++) {
           
            if (j - A[i - 1] >= 0){
                currentRow[j] = previousRow[j - A[i - 1]]||previousRow[j];
            }
            else{
                currentRow[j] = previousRow[j];

            }
            
        }
        previousRow=currentRow.slice(0) // make previous=current
    }

    return currentRow[sumA/2];
};
```

**1 ROW APPROACH**
![image](https://assets.leetcode.com/users/georgechryso/image_1577489839.png)

```
var canPartition = function(A) {
    var sumA = A.reduce((acc, curr) => acc + curr);

    if (sumA % 2) return false;
  
    var row = new Array((sumA/2)+1).fill(false);
    
    row[0] = true; // base case  


    for (let i = 1; i <= A.length; i++) {
        for (let j = sumA / 2; j >= 0; j--) { //start from right to left
            if (j - A[i - 1] >= 0){
                row[j] = row[j - A[i - 1]]||row[j];
            }
        }
    }

    return row[sumA/2];
};
```

**BIT SOLUTION**

Here follows the final and more elegant approach using bitwise operations
![image](https://assets.leetcode.com/users/georgechryso/image_1577490185.png)

![image](https://assets.leetcode.com/users/georgechryso/image_1577490211.png)

![image](https://assets.leetcode.com/users/georgechryso/image_1577490242.png)

```
var canPartition = function(A) {
    var sumA = A.reduce((acc, curr) => acc + curr);

    if (sumA % 2) return false; /* to start with, i want the number with 1 as its first element so i can mimic the previous[0]=1 state, and length of bits= the length of bits of my desired sum (sumA/2)
	*/
    let row = 1n << BigInt(sumA / 2 );  /*using bigint cos my sum may consist of more than 32bits*/

    for (const weight of A) {
        row = row | (row >> BigInt(weight));
      
    }
     /* check the the column corresponding to my target by bitwise ANDing it with just 1,so if the first bit is 1, it will return true, otherwise false*/
    return row&1n;
};


```

If you got this far, thank you for your attention. I hope I cleared up this problem. Sorry for my sloppy handwriting. Cheers.

</p>


### Simple C++ 4-line solution using a bitset
- Author: alvin-777
- Creation Date: Sun Oct 09 2016 17:21:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 05:23:31 GMT+0800 (Singapore Standard Time)

<p>
Note: as @zhoudayang2 pointed out, the question description has changed (max array size 100 ==> 200). I have modified my code below according to the new description, and also made it a bit easier to understand.

Time complexity O(n), size of the bitset is 1256 bytes
```
class Solution {
public:
    bool canPartition(vector<int>& nums) {
        const int MAX_NUM = 100;
        const int MAX_ARRAY_SIZE = 200;
        bitset<MAX_NUM * MAX_ARRAY_SIZE / 2 + 1> bits(1);
        int sum = 0;
        for (auto n : nums) {
            sum += n;
            bits |= bits << n;
        }
        return !(sum % 2) && bits[sum / 2];
    }
};
```
It's possible to shorten the solution to 4 lines, by using std::accumulate(), but that doesn't really make you type less or make it run faster though...
```
class Solution {
public:
    bool canPartition(vector<int>& nums) {
        bitset<10001> bits(1);
        int sum = accumulate(nums.begin(), nums.end(), 0);
        for (auto n : nums) bits |= bits << n;
        return !(sum & 1) && bits[sum >> 1];
    }
};
```
</p>


