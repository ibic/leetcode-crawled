---
title: "Number Complement"
weight: 452
#id: "number-complement"
---
## Description
<div class="description">
<p>Given a <strong>positive</strong> integer <code>num</code>, output its complement number. The complement strategy is to flip the bits of its binary representation.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> num = 5
<strong>Output:</strong> 2
<strong>Explanation:</strong> The binary representation of 5 is 101 (no leading zero bits), and its complement is 010. So you need to output 2.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> num = 1
<strong>Output:</strong> 0
<strong>Explanation:</strong> The binary representation of 1 is 1 (no leading zero bits), and its complement is 0. So you need to output 0.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The given integer <code>num</code> is guaranteed to fit within the range of a 32-bit signed integer.</li>
	<li><code>num &gt;= 1</code></li>
	<li>You could assume no leading zero bit in the integer&rsquo;s binary representation.</li>
	<li>This question is the same as 1009:&nbsp;<a href="https://leetcode.com/problems/complement-of-base-10-integer/">https://leetcode.com/problems/complement-of-base-10-integer/</a></li>
</ul>

</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Cloudera - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Prerequisites

**XOR**

[XOR](https://en.wikipedia.org/wiki/Exclusive_or) of zero and a bit 
results in that bit

$$
0 \oplus x = x  
$$

> XOR of one and a bit flips that bit

$$
1 \oplus x = 1 - x  
$$

**Right Shift and Left Shift**

![fig](../Figures/476/shift2.png)

<br/>
<br/>

---
#### Overview

> The article is long, and the best approach is the one number 4. 
In the case of limited time, you could jump to it directly. 

There are two standard ways to solve the problem:

- To move along the number and flip bit by bit.

- To construct 1-bits bitmask which has the same length as the input number,
and to get the answer as `bitmask - num` or `bitmask ^ num`.

    For example, for $$\textrm{num} = 5 = (101)_2$$ the bitmask is $$\textrm{bitmask} = (111)_2$$,
    and the complement number is $$\textrm{bitmask} \oplus \textrm{num} = (010)_2 = 2$$.

![fig](../Figures/476/overview2.png)

#### Approach 1: Flip Bit by Bit

**Algorithm**

- Initiate 1-bit variable which will be used to flip bits one by one. 
Set it to the smallest register `bit = 1`.

- Initiate the marker variable which will be used to stop the loop 
over the bits `todo = num`.

- Loop over the bits. While `todo != 0`:

    - Flip the current bit: `num = num ^ bit`.

    - Prepare for the next run. Shift flip variable to the left and 
    `todo` variable to the right.

- Return `num`.

![fig](../Figures/476/flip2.png)

**Implementation**

<iframe src="https://leetcode.com/playground/oN4ofKJR/shared" frameBorder="0" width="100%" height="276" name="oN4ofKJR"></iframe>

**Complexity**

- Time Complexity: $$\mathcal{O}(1)$$, since we're doing not more
than 32 iterations here.

- Space Complexity: $$\mathcal{O}(1)$$.
<br/>
<br/>

---
#### Approach 2: Compute Bit Length and Construct 1-bits Bitmask

Instead of flipping bits one by one, let's construct 1-bits bitmask 
and flip all the bits at once. 

There are many ways to do it, let's start from the simplest one:

- Compute bit length of the input number $$l = [\log_2 \textrm{num}] + 1$$.

- Compute 1-bits bitmask of length $$l$$: $$\textrm{bitmask} = (1 << l) - 1$$.

- Return `num ^ bitmask`.

![fig](../Figures/476/bitmask3.png)

**Implementation**

<iframe src="https://leetcode.com/playground/owENSjkd/shared" frameBorder="0" width="100%" height="225" name="owENSjkd"></iframe>

**Complexity**

- Time Complexity: $$\mathcal{O}(1)$$.

- Space Complexity: $$\mathcal{O}(1)$$.
<br/>
<br/>


---
#### Approach 3: Built-in Functions to Construct 1-bits Bitmask

Approach 2 could be rewritten with the help of built-in functions:
`bit_length` in Python and `highestOneBit` in Java.
The first one is trivial, and `Integer.highestOneBit(int x)` 
method in Java returns int with leftmost bit set in x, 
i.e. `Integer.highestOneBit(3) = 2`. 

**Implementation**

<iframe src="https://leetcode.com/playground/zSwdW4Gt/shared" frameBorder="0" width="100%" height="140" name="zSwdW4Gt"></iframe>

**Complexity**

- Time Complexity: $$\mathcal{O}(1)$$ because one deals here with integers 
of not more than 32 bits.

- Space Complexity: $$\mathcal{O}(1)$$.
<br/>
<br/>

---
#### Approach 4: highestOneBit OpenJDK algorithm from Hacker's Delight

The best algorithm for this task is an implementation of `highestOneBit`
in OpenJDK. [This implementation is taken from "Hacker's Delight" book](http://hg.openjdk.java.net/jdk8/jdk8/jdk/file/687fd7c7986d/src/share/classes/java/lang/Integer.java#l40).

The idea is to create the same 1-bits bitmask by propagating 
the highest 1-bit into the lower ones. 

!?!../Documents/476_LIS.json:1000,316!?!

**Implementation**

<iframe src="https://leetcode.com/playground/DHQefzGX/shared" frameBorder="0" width="100%" height="276" name="DHQefzGX"></iframe>

**Complexity**

- Time Complexity: $$\mathcal{O}(1)$$.

- Space Complexity: $$\mathcal{O}(1)$$.

<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 3 line C++
- Author: lzl124631x
- Creation Date: Sun Jan 08 2017 17:13:16 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 05 2020 03:39:45 GMT+0800 (Singapore Standard Time)

<p>
See my latest update in repo [LeetCode](https://github.com/lzl124631x/LeetCode)

```
// OJ: https://leetcode.com/problems/number-complement/
// Author: github.com/lzl124631x
// Time: O(1) as there are at most 32 bits to move
// Space: O(1)
class Solution {
public:
    int findComplement(int num) {
        unsigned mask = ~0;
        while (num & mask) mask <<= 1;
        return ~mask & ~num;
    }
};
```

For example,
```
num          = 00000101
mask         = 11111000
~mask & ~num = 00000010
```

</p>


### Java, very simple code and self-evident, explanation
- Author: dongdl
- Creation Date: Mon Jan 16 2017 14:07:42 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 02 2018 05:27:07 GMT+0800 (Singapore Standard Time)

<p>
for example:
100110, its complement is 011001, the sum is 111111. So we only need get the min number large or equal to num, then do substraction
```
    public int findComplement(int num) 
    {
        int i = 0;
        int j = 0;
        
        while (i < num)
        {
            i += Math.pow(2, j);
            j++;
        }
        
        return i - num;
    }
```
</p>


### Java 1 line bit manipulation solution
- Author: shawngao
- Creation Date: Sun Jan 08 2017 21:52:23 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 21:32:39 GMT+0800 (Singapore Standard Time)

<p>
I post solution first and then give out explanation. Please think why does it work before read my explanation.

```
public class Solution {
    public int findComplement(int num) {
        return ~num & ((Integer.highestOneBit(num) << 1) - 1);
    }
}
```

According to the problem, the result is
1. The ```flipped``` version of the original ```input``` but
2. Only flip ```N``` bits within the range from ```LEFTMOST``` bit of ```1``` to ```RIGHTMOST```. 
For example input = ```5``` (the binary representation is ```101```), the ```LEFTMOST``` bit of ```1``` is the third one from ```RIGHTMOST``` (```100```, ```N``` =  3). Then we need to flip 3 bits from ```RIGHTMOST``` and the answer is ```010```

To achieve above algorithm, we need to do 3 steps:
1. Create a bit mask which has ```N``` bits of ```1``` from ```RIGHTMOST```. In above example, the mask is ```111```. And we can use the decent Java built-in function ```Integer.highestOneBit``` to get the ```LEFTMOST``` bit of ```1```, left shift one, and then minus one. Please remember this wonderful trick to create bit masks with ```N``` ones at ```RIGHTMOST```, you will be able to use it later.
2. Negate the whole input number.
3. ```Bit AND``` numbers in step ```1``` and ```2```.

Three line solution if you think one line solution is too confusing:
```
public class Solution {
    public int findComplement(int num) {
        int mask = (Integer.highestOneBit(num) << 1) - 1;
        num = ~num;
        return num & mask;
    }
}
```

```UPDATE```
As several people pointed out, we don't need to left shift 1. That's true because the highest ```1``` bit will always become ```0``` in the ```Complement``` result. So we don't need to take care of that bit.

```
public class Solution {
    public int findComplement(int num) {
        return ~num & (Integer.highestOneBit(num) - 1);
    }
}
```
</p>


