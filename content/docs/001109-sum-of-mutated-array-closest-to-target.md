---
title: "Sum of Mutated Array Closest to Target"
weight: 1109
#id: "sum-of-mutated-array-closest-to-target"
---
## Description
<div class="description">
<p>Given an integer array&nbsp;<code>arr</code> and a target value <code>target</code>, return&nbsp;the integer&nbsp;<code>value</code>&nbsp;such that when we change all the integers&nbsp;larger than <code>value</code>&nbsp;in the given array to be equal to&nbsp;<code>value</code>,&nbsp;the sum of the array gets&nbsp;as close as possible (in absolute difference) to&nbsp;<code>target</code>.</p>

<p>In case of a tie, return the minimum such integer.</p>

<p>Notice that the answer is not neccesarilly a number from <code>arr</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [4,9,3], target = 10
<strong>Output:</strong> 3
<strong>Explanation:</strong> When using 3 arr converts to [3, 3, 3] which sums 9 and that&#39;s the optimal answer.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,3,5], target = 10
<strong>Output:</strong> 5
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [60864,25176,27249,21296,20204], target = 56803
<strong>Output:</strong> 11361
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 10^4</code></li>
	<li><code>1 &lt;= arr[i], target &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Just Sort, O(nlogn)
- Author: lee215
- Creation Date: Sun Dec 29 2019 00:49:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 21:07:22 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Binary search is `O(NlogMax(A))`.
In order to ruduce the difficulty, it constrains `A[i] < 10 ^ 5`.

In this solution,
we sort the input and compared `A[i]` with target one by one.

1. Sort the array `A` in decreasing order.
2. We try to make all values in `A` to be the `min(A)` (the last element)
3. If `target >= min(A) * n`, we doesn\'t hit our target yet.
We should continue to try a value bigger.
4. So we pop the `min(A)` value.
Consider that it won\'t be affected anymore,
we can remove it from target by `target -= A.pop()`
5. We continue doing step 2-4, until the next number is too big for target.
6. We split the the target evenly, depending on the number of element left in `A`


At this point, @bobalice help explain the round part:
if A is empty means its impossible to reach target so we just return maximum element.
If A is not empty, intuitively the answer should be the nearest integer to `target / len(A)`.

Since we need to return the minimum such integer if there is a tie,
if `target / len(A)` has `0.5` we should round down,
<br>

## **Complexity**
Time `O(NlogN)`
Space `O(1)`
<br>

**Java**
```java
    public int findBestValue(int[] A, int target) {
        Arrays.sort(A);
        int n = A.length, i = 0;
        while (i < n && target > A[i] * (n - i)) {
            target -= A[i++];
        }
        if (i == n) return A[n - 1];
        int res = target / (n - i);
        if (target - res * (n - i) > (res + 1) * (n - i) - target)
            res++;
        return res;
    }
```
**C++**
```cpp
    int findBestValue(vector<int>& A, int target) {
        sort(A.begin(), A.end());
        int n = A.size(), i = 0;
        while (i < n && target > A[i] * (n - i))
            target -= A[i++];
        return i == n ? A[n - 1] : int(round((target - 0.0001) / (n - i)));
    }
```
**Python:**
```py
    def findBestValue(self, A, target):
        A.sort(reverse=1)
        maxA = A[0]
        while A and target >= A[-1] * len(A):
            target -= A.pop()
        return int(round((target - 0.0001) / len(A))) if A else maxA
```

</p>


### [Java/C++] 4ms, binary search, short readable code + sorting solution
- Author: savvadia
- Creation Date: Sun Dec 29 2019 00:21:59 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 02 2020 16:23:11 GMT+0800 (Singapore Standard Time)

<p>
# Binary search solution
See also a sorting solution below.

The value we are looking for is somewhere between 1 and maxValue (```m```).
Now use Binary search to speed up the process.
* go up if the sum is too small
* go down if the sum is too big

When we are done with binary search, ```l``` and ```r``` are equal, but it might happen that we have not exactly reached the target.
Check if ```l-1``` (should get us below the ```target```) leads to the ```sum``` closer to the ```target```.

Java, 4ms:
```java
class Solution {
    public int findBestValue(int[] arr, int target) {
        int l, r, mi, s=0, m=-1;
        for(int v:arr) { s += v; m=Math.max(m,v); }

        if(s<=target) return m; // return the max value since we will keep all nums as is

        for(l=1,r=m;l<r;) {
            mi=(l+r)/2;
            s=0;
            for(int v:arr) s += (v>mi)?mi:v;
            if(s>=target) r=mi;
            else          l=mi+1;
        }
        // check if we are 1 step off the target 
        int s1=0,s2=0;
        for(int v:arr) {
            s1 += (v>l)?(l):v;
            s2 += (v>l-1)?(l-1):v;
        }
        
        return (Math.abs(s2-target) <= Math.abs(s1-target)) ? l-1 : l;
    }
}
```

C++, 16ms
```cpp
class Solution {
public:
    int findBestValue(vector<int>& arr, int target) {
        int l, r, mi, s=0, m=-1;
        for(int v:arr) s += v, m=max(m,v);

        if(s<=target) return m; // return the max value since we will keep all nums as is

        for(l=1,r=m;l<r;) {
            mi=(l+r)/2;
            s=0;
            for(int v:arr) s += (v>mi)?mi:v;
            if(s>=target) r=mi;
            else          l=mi+1;
        }
        // check if we are 1 step off the target 
        int s1=0,s2=0;
        for(int v:arr) s1 += (v>l)?(l):v, s2 += (v>l-1)?(l-1):v;
        
        return (abs(s2-target) <= abs(s1-target)) ? l-1 : l;
    }
};
```

**UPDATE:**
# Sorting solution
 
I got comments that sorting solution can be more intuitive. It\'s maybe shorter but binary search is easier to apply as a pattern (at least for me). 
Well, sorting leads to O(NLogN) speed while binary search gives O(NLog(Max(A))) so it\'s similar.
However, in practice It\'s slightly slower on given tests. 

Intuition:
  - We will need to replace the highest values with some value ```v```
  - ```v``` must be >= the highest value of the array. In other words: ```v >= max(arr)```
  - We should pop the highest value from arr while the current result is above the target: 
   ```while(target < sum + max(arr)*numOfRemoved)```
  - Sorting is done to speed up the search for the higest values
  - Then we simply calculate ```v``` to replace each removed element and get as close to the ```target``` as possible

In the end of "pop high values" process we will split the sorted array in 2 parts:
```cpp
          Splitted the sorted values to left and right parts:
          a,b,c,d   |   X,Y,Z
          -------       =====
          keep          replace each elem
          as is         with v
  sum:    =sum-X-Y-Z    =v*numOfElems 
          left part + right part = target
```
Values in the left part will stay as is. Their sum is the ```total sum - sum of the removed```.
Values in the right part will be replaced with ```v``` and we keep their number in ```removed```.
```cpp
left part + right part = target
left part + v*removed  = target
v = (target - left part)/removed
``` 
When we do the division, ```v``` is rounded down. I check also ```v+1``` to cover the undesired rounding though I\'m pretty sure that a sophisticated formula can do the same shorter (I prefer simpler things).

Java, 5ms
```java
    public int findBestValue(int[] arr, int target) {
        int s=0, m=-1;
        for(int v:arr) { s += v; m=Math.max(m,v); }
        if(s<=target) return m; // return the max value since we will keep all nums as is

        Arrays.sort(arr);
        
        int removed=0, lastIx=arr.length-1;
        while(lastIx>=0 && target < s + removed*arr[lastIx]) {
            removed++;
            s -= arr[lastIx];
            lastIx--;
        }
        int v=(target-s)/removed; // rounded down
        if(Math.abs(target-s-removed*v) <=
           Math.abs(target-s-removed*(v+1))) // check v+1 to compensate round down
            return v;
        return v+1;
    }
```

C++, 24ms
```cpp
    int findBestValue(vector<int>& arr, int target) {
        int s=0, m=-1;
        for(int v:arr) s += v, m=max(m,v);
        if(s<=target) return m; // return the max value since we will keep all nums as is

        sort(arr.begin(),arr.end());
        
        int removed=0, lastIx=arr.size()-1;
        while(lastIx>=0 && target < s + removed*arr[lastIx]) {
            removed++;
            s -= arr[lastIx];
            lastIx--;
        }
        int v=(target-s)/removed; // rounded down
        if(abs(target-s-removed*v) <=
           abs(target-s-removed*(v+1))) // check v+1 to compensate round down
            return v;
        return v+1;
    }
```
</p>


### [Java] Binary search, O(nlogk), k is the max value in arr
- Author: ToffeeLu
- Creation Date: Sun Dec 29 2019 00:05:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 29 2019 00:18:47 GMT+0800 (Singapore Standard Time)

<p>
``` java
class Solution {
    public int findBestValue(int[] arr, int target) {
        int max = -1;
        for(int a : arr){
            max = Math.max(max, a);
        }
        
        int left = 0, right = max;
        int diff = target;
        int prevValue = max;
        while(left <= right){
            int mid = (left + right) / 2;
            int sum = getSum(arr, mid);
            int currentDiff = Math.abs(target - sum);
            if(currentDiff < diff){
                diff = currentDiff;
                prevValue = mid;
            }
            else if(currentDiff == diff){
                prevValue = Math.min(prevValue, mid);
            }
            if(sum > target){
                right = mid - 1;
            }
            else{
                left = mid + 1;
            }
        }
        
        return prevValue;
    }
    
    
    public int getSum(int[] arr, int value){
        int sum = 0;
        for(int a : arr){
            sum += a > value ? value : a;
        }
        return sum;
    }
}
```
</p>


