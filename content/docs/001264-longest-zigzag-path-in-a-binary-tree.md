---
title: "Longest ZigZag Path in a Binary Tree"
weight: 1264
#id: "longest-zigzag-path-in-a-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree <code>root</code>, a&nbsp;ZigZag path for a binary tree is defined as follow:</p>

<ul>
	<li>Choose <strong>any </strong>node in the binary tree and a direction (right or left).</li>
	<li>If the current direction is right then move to the right child of the current node otherwise move to the left child.</li>
	<li>Change the direction from right to left or right to left.</li>
	<li>Repeat the second and third step until you can&#39;t move in the tree.</li>
</ul>

<p>Zigzag length is defined as the number of nodes visited - 1. (A single node has a length of 0).</p>

<p>Return&nbsp;the longest <strong>ZigZag</strong> path contained in that tree.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/01/22/sample_1_1702.png" style="width: 151px; height: 283px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [1,null,1,1,1,null,null,1,1,null,1,null,null,null,1,null,1]
<strong>Output:</strong> 3
<strong>Explanation:</strong> Longest ZigZag path in blue nodes (right -&gt; left -&gt; right).
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/01/22/sample_2_1702.png" style="width: 120px; height: 253px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [1,1,1,null,1,null,null,1,1,null,1]
<strong>Output:</strong> 4
<strong>Explanation:</strong> Longest ZigZag path in blue nodes (left -&gt; right -&gt; left -&gt; right).
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [1]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>Each tree has at most <code>50000</code> nodes..</li>
	<li>Each node&#39;s value is between <code>[1, 100]</code>.</li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Tree (tree)

## Companies
- Sumerge - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python] DFS Solution
- Author: lee215
- Creation Date: Sun Mar 08 2020 00:13:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 10:23:03 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Recursive return `[left, right, result]`, where:
`left` is the maximum length in direction of `root.left`
`right` is the maximum length in direction of `root.right`
`result` is the maximum length in the whole sub tree.
<br>

## **Complexity**
Time `O(N)`
Space `O(height)`
<br>

**Java**
```java
    public int longestZigZag(TreeNode root) {
        return dfs(root)[2];
    }

    private int[] dfs(TreeNode root) {
        if (root == null) return new int[]{-1, -1, -1};
        int[] left = dfs(root.left), right = dfs(root.right);
        int res = Math.max(Math.max(left[1], right[0]) + 1, Math.max(left[2], right[2]));
        return new int[]{left[1] + 1, right[0] + 1, res};
    }
```
**C++**
by @SuperWhw
```cpp
    int longestZigZag(TreeNode* root) {
        return dfs(root)[2];
    }

    vector<int> dfs(TreeNode* root) {
        if (!root) return { -1, -1, -1};
        vector<int> left = dfs(root->left), right = dfs(root->right);
        int res = max(max(left[1], right[0]) + 1, max(left[2], right[2]));
        return {left[1] + 1, right[0] + 1, res};
    }
```
**Python:**
```py
    def longestZigZag(self, root):
        def dfs(root):
            if not root: return [-1, -1, -1]
            left, right = dfs(root.left), dfs(root.right)
            return [left[1] + 1, right[0] + 1, max(left[1] + 1, right[0] + 1, left[2], right[2])]
        return dfs(root)[-1]
```

</p>


### Simple Java Code With Comments
- Author: JatinYadav96
- Creation Date: Sun Mar 08 2020 00:19:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 13:54:33 GMT+0800 (Singapore Standard Time)

<p>
```
static int max = 0;
public static int longestZigZag(TreeNode root) {
        if (root == null) return -1;// if null return -1
        max = 0;
        helper(root.right, 1, true);// go right
        helper(root.left, 1, false);// go left
        return max;
    }

    private static void helper(TreeNode root, int step, boolean isRight) {
        if (root == null) return;
        max = Math.max(max, step);
        if (isRight) {// if coming from right go left
            helper(root.left, step + 1, false);
            helper(root.right, 1, true);//try again from start
        } else {// else coming from left then go right
            helper(root.right, step + 1, true);
            helper(root.left, 1, false);
        }
    }
```
</p>


### [Java/C++] DFS Solution with comment - O(N) - Clean code
- Author: hiepit
- Creation Date: Tue Mar 10 2020 01:31:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 14 2020 22:26:33 GMT+0800 (Singapore Standard Time)

<p>
**Java**
```java
class Solution {
    int maxStep = 0;
    public int longestZigZag(TreeNode root) {
        dfs(root, true, 0);
        dfs(root, false, 0);
        return maxStep;
    }
    private void dfs(TreeNode root, boolean isLeft, int step) {
        if (root == null) return;
        maxStep = Math.max(maxStep, step); // update max step sofar
        if (isLeft) {
            dfs(root.left, false, step + 1); // keep going from root to left
            dfs(root.right, true, 1); // restart going from root to right
        } else {
            dfs(root.right, true, step + 1); // keep going from root to right
            dfs(root.left, false, 1); // restart going from root to left
        }
    }
}
```
**C++**
```c++
class Solution {
public:
    int maxStep = 0;
    int longestZigZag(TreeNode* root) {
        dfs(root, true, 0);
        dfs(root, false, 0);
        return maxStep;
    }
    void dfs(TreeNode* root, bool isLeft, int step) {
        if (!root) return;
        maxStep = max(maxStep, step); // update max step sofar
        if (isLeft) {
            dfs(root->left, false, step + 1); // keep going from root to left
            dfs(root->right, true, 1); // restart going from root to right
        } else {
            dfs(root->right, true, step + 1); // keep going from root to right
            dfs(root->left, false, 1); // restart going from root to left
        }
    }
};
```
Complexity:
- Time: `O(n)`, it will visit all the nodes and each node once.
- Space: `O(h)`
</p>


