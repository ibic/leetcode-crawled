---
title: "Number of Ways to Reorder Array to Get Same BST"
weight: 1433
#id: "number-of-ways-to-reorder-array-to-get-same-bst"
---
## Description
<div class="description">
<p>Given an array <code>nums</code>&nbsp;that represents a permutation of integers from&nbsp;<code>1</code>&nbsp;to&nbsp;<code>n</code>. We are going to construct a binary search tree (BST) by inserting the elements of&nbsp;<code>nums</code>&nbsp;in&nbsp;order into an initially empty BST. Find the number of different ways to reorder <code>nums</code> so that the constructed BST is identical to that formed from the original array&nbsp;<code>nums</code>.</p>

<p>For example, given&nbsp;<code>nums = [2,1,3]</code>, we will have 2 as the root, 1 as a left child, and 3 as a right child. The array&nbsp;<code>[2,3,1]</code>&nbsp;also yields the same BST but&nbsp;<code>[3,2,1]</code>&nbsp;yields a different BST.</p>

<p>Return <em>the number of ways to reorder</em>&nbsp;<code>nums</code>&nbsp;<em>such that the BST formed is identical to the original BST formed from</em>&nbsp;<code>nums</code>.</p>

<p>Since the answer may be very large,&nbsp;<strong>return it modulo&nbsp;</strong><code>10^9 + 7</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/08/12/bb.png" style="width: 121px; height: 101px;" /></p>

<pre>
<strong>Input:</strong> nums = [2,1,3]
<strong>Output:</strong> 1
<strong>Explanation: </strong>We can reorder nums to be [2,3,1] which will yield the same BST. There are no other ways to reorder nums which will yield the same BST.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/08/12/ex1.png" style="width: 241px; height: 161px;" /></strong></p>

<pre>
<strong>Input:</strong> nums = [3,4,5,1,2]
<strong>Output:</strong> 5
<b>Explanation: </b>The following 5 arrays will yield the same BST: 
[3,1,2,4,5]
[3,1,4,2,5]
[3,1,4,5,2]
[3,4,1,2,5]
[3,4,1,5,2]
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/08/12/ex4.png" style="width: 121px; height: 161px;" /></strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3]
<strong>Output:</strong> 0
<strong>Explanation: </strong>There are no other orderings of nums that will yield the same BST.
</pre>

<p><strong>Example 4:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/08/12/abc.png" style="width: 241px; height: 161px;" /></strong></p>

<pre>
<strong>Input:</strong> nums = [3,1,2,5,4,6]
<strong>Output:</strong> 19
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> nums = [9,4,2,1,3,6,5,7,8,14,11,10,12,13,16,15,17,18]
<strong>Output:</strong> 216212978
<strong>Explanation: </strong>The number of ways to reorder nums to get the same BST is 3216212999. Taking this number modulo 10^9 + 7 gives 216212978.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 1000</code></li>
	<li><code>1 &lt;= nums[i] &lt;= nums.length</code></li>
	<li>All integers in&nbsp;<code>nums</code>&nbsp;are&nbsp;<strong>distinct</strong>.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++]--Just using recursion, very Clean and  Easy to understand--O(n^2)
- Author: Aincrad-Lyu
- Creation Date: Sun Aug 30 2020 12:13:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 30 2020 16:51:26 GMT+0800 (Singapore Standard Time)

<p>
So, we can know that for a fixed root, the left subtree elements and the right subtree elements are also fixed.

We can find the ``left subtree elements`` which are all the elements that is **smaller** than root value, and ``right subtree elements`` which are **greater** than root value.

And in order to make it identical with original BST, we should **keep the relative order** in left subtree elements and in right subtree elements.

Assume the lenght of left subtree elements is ``left_len`` and right is ``right_len``, they **can change their absolute position** but **need to keep their relative position** in either left subtree or right right subtree. 

So as the subtree, so we use recursion.

**Example**
```
[3, 4, 5, 1, 2] // original array with root value is 3

[1, 2] // left sub-sequence, left_len = 2
[4, 5] // right sub-sequence, right_len = 2

// the left sub-sequence and right sub-sequence take 4 position, because left_len + right_len = 4

// keep relative order  in left sub-sequence and in right-sequence, but can change absolute position.
[1, 2, 4, 5]
[1, 4, 2, 5]
[1, 4, 5, 2]
[4, 1, 2, 5]
[4, 1, 5, 2]
[4, 5, 1, 2]
// number of permutation: 6

// in code, we use Pascal triangle to keep a table of permutations, so we can look up the table and get permutation result in O(1)
```

**Code**
```
class Solution {
public:
    int numOfWays(vector<int>& nums) {
        long long mod = 1e9 + 7;
		int n = nums.size();
        
		// Pascal triangle
        table.resize(n + 1);
        for(int i = 0; i < n + 1; ++i){
            table[i] = vector<long long>(i + 1, 1);
            for(int j = 1; j < i; ++j){
                table[i][j] = (table[i-1][j-1] + table[i-1][j]) % mod;
            }
        }
        
        long long ans = dfs(nums, mod);
        return ans % mod - 1;
    }
    
private:
    vector<vector<long long>> table;
    long long dfs(vector<int> &nums, long long mod){
        int n = nums.size();
        if(n <= 2) return 1;
        
		// find left sub-sequence elements and right sub-sequence elements
        vector<int> left, right;
        for(int i = 1; i < nums.size(); ++i){
            if(nums[i] < nums[0]) left.push_back(nums[i]);
            else right.push_back(nums[i]);
        }
		
		// recursion with left subtree and right subtree
        long long left_res = dfs(left, mod) % mod;
        long long right_res = dfs(right, mod) % mod;
		
		// look up table and multiple them together
		int left_len = left.size(), right_len = right.size();
        return (((table[n - 1][left_len] * left_res) % mod) * right_res) % mod;
    }
};
```
</p>


### Python in 6 short lines with easy explanation
- Author: Daciuk
- Creation Date: Sun Aug 30 2020 12:06:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 30 2020 12:11:28 GMT+0800 (Singapore Standard Time)

<p>
We separate all the elements into two lists, depending on whether they are less than or more than the root.  Then we recurse on those left and right sublists.  The combination is for the macro ordering between left and right, and the recursive factors are for the internal ordering of left and right themselves.  I minus 1 from the result because we don\'t count the original ordering.

```
def numOfWays(self, nums: List[int]) -> int:
    def f(nums):
        if len(nums) <= 2: return 1
        left = [v for v in nums if v < nums[0]]
        right = [v for v in nums if v > nums[0]]
        return comb(len(left)+len(right), len(right)) * f(left) * f(right)
    return (f(nums)-1) % (10**9+7)
```
</p>


### [Java] Clean code uses Yang Hui's/Pascal's Triangle With Explanation
- Author: chyyyy
- Creation Date: Sun Aug 30 2020 15:34:26 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 31 2020 07:34:56 GMT+0800 (Singapore Standard Time)

<p>
This is actually a mathematical problem that can be solved by combination calculation, what\'d you do is basically arranging left and right sub-trees in correct order but in all possible combinations.
For example for array ```[3,6,4,1]```
```
[3,6,4,1] left sub tree is [1], right tree is [6,4], 
we just need to keep 6 appear in front of 4 to make permutation a valid one,
so combinations can be [6,4,1], [6,1,4], [1,6,4]
Obviously, if left sub tree length is 1 and total length is 3, combination is 3C1 which is 3
```
Expand to a more complicated case ```[3,6,4,1,7]```
```
left sub tree is [1], right tree is [6,4,7], 
permutations for right tree itself is [6,4,7], [6,7,4] which means it\'s 2C1 (combination of [4] and [7])
for every permuration of right tree you can also combine it with left tree [1] so total # is 4C1*2C1=8
```
Tseudo code:
```
def dfs(nums):
	if len(nums) <= 2:
		return 1
	left = [x in nums which < nums[0]]
	right = [x in nums which > nums[0]]
	return combination(len(lefft+right), len(left)) * dfs(left) * dfs(right)
```
Here comes the tricky part for Java, doing mathematical stuff in Java is really a PAIN in the ass since it doesn\'t have ```comb()``` function in python and it doesn\'t support ```long long```, I didn\'t know why the hint is dynamic programming, now I get it, I can use Yang Hui/Pascal\'s triangle to speed up calculation and get rid of overflow, are you serious leetcode? 
```
class Solution {
    private static final long MOD = 1000000007;
    public int numOfWays(int[] nums) {
        int len = nums.length;
        List<Integer> arr = new ArrayList<>();
        for (int n : nums) {
            arr.add(n);
        }
        return (int)getCombs(arr, getTriangle(len + 1)) - 1;
    }
    
    private long getCombs(List<Integer> nums, long[][] combs) {
        if (nums.size() <= 2) {
            return 1;
        }
        int root = nums.get(0);
        List<Integer> left = new ArrayList<>();
        List<Integer> right = new ArrayList<>();
        for (int n : nums) {
            if (n < root) {
                left.add(n);
            } else if (n > root) {
                right.add(n);
            }
        }
        // mod every number to avoid overflow
        return (combs[left.size() + right.size()][left.size()] * (getCombs(left, combs) % MOD) % MOD) * getCombs(right, combs) % MOD;
    }
    
    private long[][] getTriangle(int n) {
        // Yang Hui (Pascle) triangle
        // 4C2 = triangle[4][2] = 6
        long[][] triangle = new long[n][n];
        for (int i = 0; i < n; i++) {
            triangle[i][0] = triangle[i][i] = 1;
        }
        for (int i = 2; i < n; i++) {
            for (int j = 1; j < i; j++) {
                triangle[i][j] = (triangle[i - 1][j] + triangle[i - 1][j - 1]) % MOD;
            }
        }
        return triangle;
    }
}
```
</p>


