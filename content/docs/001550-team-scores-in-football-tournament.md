---
title: "Team Scores in Football Tournament"
weight: 1550
#id: "team-scores-in-football-tournament"
---
## Description
<div class="description">
<p>Table: <code>Teams</code></p>

<pre>
+---------------+----------+
| Column Name   | Type     |
+---------------+----------+
| team_id       | int      |
| team_name     | varchar  |
+---------------+----------+
team_id is the primary key of this table.
Each row of this table represents a single football team.
</pre>

<p>Table:&nbsp;<code>Matches</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| match_id      | int     |
| host_team     | int     |
| guest_team    | int     | 
| host_goals    | int     |
| guest_goals   | int     |
+---------------+---------+
match_id is the primary key of this table.
Each row is a record of a finished match between two different teams. 
Teams host_team and guest_team are represented by their IDs in the teams table (team_id) and they scored host_goals and guest_goals goals respectively.
</pre>

<p>&nbsp;</p>
You would like to compute the scores of all teams after all matches. Points are awarded as follows:

<ul>
	<li>A team&nbsp;receives three points if they win&nbsp;a match (Score strictly more goals than the opponent team).</li>
	<li>A team&nbsp;receives one point if they draw a match (Same number of goals as the opponent team).</li>
	<li>A team&nbsp;receives no points if they lose a match (Score less goals than the opponent team).</li>
</ul>

<p>Write an SQL query that selects the <strong>team_id</strong>, <strong>team_name</strong> and <strong>num_points</strong> of each team in the tournament after all described matches. Result table should be ordered by <strong>num_points</strong> (decreasing order). In case of a tie, order the records by <strong>team_id</strong> (increasing order).</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Teams </code>table:
+-----------+--------------+
| team_id   | team_name    |
+-----------+--------------+
| 10        | Leetcode FC  |
| 20        | NewYork FC   |
| 30        | Atlanta FC   |
| 40        | Chicago FC   |
| 50        | Toronto FC   |
+-----------+--------------+

<code>Matches </code>table:
+------------+--------------+---------------+-------------+--------------+
| match_id   | host_team    | guest_team    | host_goals  | guest_goals  |
+------------+--------------+---------------+-------------+--------------+
| 1          | 10           | 20            | 3           | 0            |
| 2          | 30           | 10            | 2           | 2            |
| 3          | 10           | 50            | 5           | 1            |
| 4          | 20           | 30            | 1           | 0            |
| 5          | 50           | 30            | 1           | 0            |
+------------+--------------+---------------+-------------+--------------+

Result table:
+------------+--------------+---------------+
| team_id    | team_name    | num_points    |
+------------+--------------+---------------+
| 10         | Leetcode FC  | 7             |
| 20         | NewYork FC   | 3             |
| 50         | Toronto FC   | 3             |
| 30         | Atlanta FC   | 1             |
| 40         | Chicago FC   | 0             |
+------------+--------------+---------------+
</pre>

</div>

## Tags


## Companies
- Wayfair - 2 (taggedByAdmin: true)
- Oracle - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### simple MYSQL solution beats 100% WITH LEFT JOIN
- Author: eminem18753
- Creation Date: Thu Oct 03 2019 12:17:07 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 03 2019 12:18:12 GMT+0800 (Singapore Standard Time)

<p>

```
# Write your MySQL query statement below
SELECT team_id,team_name,
SUM(CASE WHEN team_id=host_team AND host_goals>guest_goals THEN 3 ELSE 0 END)+
SUM(CASE WHEN team_id=guest_team AND guest_goals>host_goals THEN 3 ELSE 0 END)+
SUM(CASE WHEN team_id=host_team AND host_goals=guest_goals THEN 1 ELSE 0 END)+
SUM(CASE WHEN team_id=guest_team AND guest_goals=host_goals THEN 1 ELSE 0 END)
as num_points
FROM Teams
LEFT JOIN Matches
ON team_id=host_team OR team_id=guest_team
GROUP BY team_id
ORDER BY num_points DESC, team_id ASC;
```
</p>


### MySQL/SQL SERVER beats 100%, with no union
- Author: LijianChen
- Creation Date: Sat Oct 05 2019 02:57:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 05 2019 03:06:31 GMT+0800 (Singapore Standard Time)

<p>
The key is: ```JOIN``` on ```t.team_id = m.host_team OR t.team_id = m.guest_team```
```
SELECT team_id, team_name,
SUM(
    CASE WHEN team_id = host_team AND host_goals > guest_goals THEN 3
         WHEN team_id = guest_team AND guest_goals > host_goals THEN 3
         WHEN host_goals = guest_goals THEN 1
         ELSE 0
    END          
) AS "num_points"
FROM Teams t
LEFT JOIN Matches m ON t.team_id = m.host_team OR t.team_id = m.guest_team
GROUP BY team_id, team_name
ORDER BY num_points DESC, team_id
```
</p>


### MySQL beats 100% short and simple
- Author: jennylusuting
- Creation Date: Thu Oct 03 2019 15:43:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 03 2019 15:43:28 GMT+0800 (Singapore Standard Time)

<p>
```
select t.team_id, t.team_name, 
        sum(case 
            when T.host_goals > T.guest_goals then 3 
            when T.host_goals = T.guest_goals then 1
            else 0
            end) as num_points
from teams t left join
(select * from matches
union all
select match_id, guest_team, host_team, guest_goals, host_goals from matches) as T on t.team_id = T.host_team
group by t.team_id
order by num_points desc, t.team_id asc
```
</p>


