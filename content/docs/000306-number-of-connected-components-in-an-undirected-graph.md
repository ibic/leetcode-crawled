---
title: "Number of Connected Components in an Undirected Graph"
weight: 306
#id: "number-of-connected-components-in-an-undirected-graph"
---
## Description
<div class="description">
<p>Given <code>n</code> nodes labeled from <code>0</code> to <code>n - 1</code> and a list of undirected edges (each edge is a pair of nodes), write a function to find the number of connected components in an undirected graph.</p>

<p><b>Example 1:</b></p>

<pre>
<strong>Input: </strong><code>n = 5</code> and <code>edges = [[0, 1], [1, 2], [3, 4]]</code>

     0          3
     |          |
     1 --- 2    4 

<strong>Output: </strong>2
</pre>

<p><b>Example 2:</b></p>

<pre>
<strong>Input: </strong><code>n = 5</code> and <code>edges = [[0, 1], [1, 2], [2, 3], [3, 4]]</code>

     0           4
     |           |
     1 --- 2 --- 3

<strong>Output:&nbsp;&nbsp;</strong>1
</pre>

<p><b>Note:</b><br />
You can assume that no duplicate edges will appear in <code>edges</code>. Since all edges are undirected, <code>[0, 1]</code> is the same as <code>[1, 0]</code> and thus will not appear together in <code>edges</code>.</p>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)
- Union Find (union-find)
- Graph (graph)

## Companies
- Amazon - 5 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Facebook - 5 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easiest 2ms Java Solution
- Author: yavinci
- Creation Date: Wed Dec 30 2015 15:30:50 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 22:53:34 GMT+0800 (Singapore Standard Time)

<p>
This is 1D version of [Number of Islands II][1]. For more explanations, check out this [2D Solution][1].

 1. `n` points = `n` islands = `n` trees = `n` roots.
 2. With each edge added, check which island is `e[0]` or `e[1]` belonging to.
 4. If `e[0]` and `e[1]` are in same islands, do nothing.
 5. Otherwise, **union** two islands, and  reduce islands count by `1`.
 6. Bonus: path compression can reduce time by `50%`.

Hope it helps!

    public int countComponents(int n, int[][] edges) {
        int[] roots = new int[n];
        for(int i = 0; i < n; i++) roots[i] = i; 

        for(int[] e : edges) {
            int root1 = find(roots, e[0]);
            int root2 = find(roots, e[1]);
            if(root1 != root2) {      
                roots[root1] = root2;  // union
                n--;
            }
        }
        return n;
    }

    public int find(int[] roots, int id) {
        while(roots[id] != id) {
            roots[id] = roots[roots[id]];  // optional: path compression
            id = roots[id];
        }
        return id;
    }

  [1]: https://leetcode.com/discuss/69572/easiest-java-solution-with-explanations
</p>


### Java concise DFS
- Author: xuyirui
- Creation Date: Mon Jan 18 2016 06:09:03 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 06:09:58 GMT+0800 (Singapore Standard Time)

<p>
start dfsVisit with sources 0-n-1, count number of unvisited sources.

    public class Solution {
        public int countComponents(int n, int[][] edges) {
            if (n <= 1)
                return n;
            Map<Integer, List<Integer>> map = new HashMap<>();
            for (int i = 0; i < n; i++) {
                map.put(i, new ArrayList<>());
            }
            for (int[] edge : edges) {
                map.get(edge[0]).add(edge[1]);
                map.get(edge[1]).add(edge[0]);
            }
            Set<Integer> visited = new HashSet<>();
            int count = 0;
            for (int i = 0; i < n; i++) {
                if (visited.add(i)) {
                    dfsVisit(i, map, visited);
                    count++;
                }
            }
            return count;
        }
        
        private void dfsVisit(int i, Map<Integer, List<Integer>> map, Set<Integer> visited) {
            for (int j : map.get(i)) {
                if (visited.add(j))
                    dfsVisit(j, map, visited);
            }
        }
    }
</p>


### Python DFS, BFS, Union Find solutions
- Author: ChrisZhang12240
- Creation Date: Thu Dec 31 2015 11:36:03 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 04 2018 06:18:05 GMT+0800 (Singapore Standard Time)

<p>
DFS:
 

    def countComponents(n, edges):
            def dfs(n, g, visited):
                if visited[n]:
                    return
                visited[n] = 1
                for x in g[n]:
                    dfs(x, g, visited)
                    
            visited = [0] * n
            g = {x:[] for x in xrange(n)}
            for x, y in edges:
                g[x].append(y)
                g[y].append(x)
                
            ret = 0
            for i in xrange(n):
                if not visited[i]:
                    dfs(i, g, visited)
                    ret += 1
                    
            return ret

BFS:

    def countComponents(n, edges):
            g = {x:[] for x in xrange(n)}
            for x, y in edges:
                g[x].append(y)
                g[y].append(x)
                
            ret = 0
            for i in xrange(n):
                queue = [i]
                ret += 1 if i in g else 0
                for j in queue:
                    if j in g:
                        queue += g[j]
                        del g[j]
    
            return ret

Union Find:

    def countComponents(n, edges):
            def find(x):
                if parent[x] != x:
                    parent[x] = find(parent[x])
                return parent[x]
                
            def union(xy):
                x, y = map(find, xy)
                if rank[x] < rank[y]:
                    parent[x] = y
                else:
                    parent[y] = x
                    if rank[x] == rank[y]:
                        rank[x] += 1
            
            parent, rank = range(n), [0] * n
            map(union, edges)
            return len({find(x) for x in parent})
</p>


