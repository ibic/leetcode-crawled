---
title: "Bulls and Cows"
weight: 282
#id: "bulls-and-cows"
---
## Description
<div class="description">
<p>You are playing the <strong><a href="https://en.wikipedia.org/wiki/Bulls_and_Cows" target="_blank">Bulls and Cows</a></strong> game with your friend.</p>

<p>You write down a secret number and ask your friend to guess what the number is. When your friend makes a guess, you provide a hint with the following info:</p>

<ul>
	<li>The number of &quot;bulls&quot;, which are digits in the guess that are in the correct position.</li>
	<li>The number of &quot;cows&quot;, which are digits in the guess that are in your secret number but are located in the wrong position. Specifically, the non-bull digits in the guess that could be rearranged such that they become bulls.</li>
</ul>

<p>Given the secret number <code>secret</code> and your friend&#39;s guess <code>guess</code>, return <em>the hint for your friend&#39;s guess</em>.</p>

<p>The hint should be formatted as <code>&quot;xAyB&quot;</code>, where <code>x</code> is the number of bulls and <code>y</code> is the number of cows. Note that both <code>secret</code> and <code>guess</code> may contain duplicate digits.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> secret = &quot;1807&quot;, guess = &quot;7810&quot;
<strong>Output:</strong> &quot;1A3B&quot;
<strong>Explanation:</strong> Bulls are connected with a &#39;|&#39; and cows are underlined:
&quot;1807&quot;
  |
&quot;<u>7</u>8<u>10</u>&quot;</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> secret = &quot;1123&quot;, guess = &quot;0111&quot;
<strong>Output:</strong> &quot;1A1B&quot;
<strong>Explanation:</strong> Bulls are connected with a &#39;|&#39; and cows are underlined:
&quot;1123&quot;        &quot;1123&quot;
  |      or     |
&quot;01<u>1</u>1&quot;        &quot;011<u>1</u>&quot;
Note that only one of the two unmatched 1s is counted as a cow since the non-bull digits can only be rearranged to allow one 1 to be a bull.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> secret = &quot;1&quot;, guess = &quot;0&quot;
<strong>Output:</strong> &quot;0A0B&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> secret = &quot;1&quot;, guess = &quot;1&quot;
<strong>Output:</strong> &quot;1A0B&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= secret.length, guess.length &lt;= 1000</code></li>
	<li><code>secret.length == guess.length</code></li>
	<li><code>secret</code> and <code>guess</code> consist of digits only.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Google - 9 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Zillow - 3 (taggedByAdmin: false)
- Airbnb - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Overview

At first glance, this popular Google question seems to be easy. 
The only difficulty here is to define how to count.

- To count bulls, one could parse both strings and count the matches,
and that's a correct way to do things.

![img](../Figures/299/bulls2.png)
*Figure 1. Count bulls.*
{:align="center"}

- To count cows, one could parse `guess` string and count the characters 
which are present in the string `secret` but located in the wrong positions.
Though the cows are more complicated creatures than bulls. 
For some test cases, it might work.

![img](../Figures/299/bulls2.png)
*Figure 2. Count cows.*
{:align="center"}

- Although, in general, one has to count the characters as well. For example,
if `secret` contains just one digit "4" then the maximum number of 4-cows is 1.
Even if `guess` contains many "4"s. In the following example, only one of two "4"s should be counted as
a cow. 

![img](../Figures/299/cows.png)
*Figure 3. Only one of two "4"s should be counted as "4-cow".*
{:align="center"}
 
To figure out these three points is enough to solve the problem. 
   
In this article, we start from a straightforward two-pass solution which 
already has the best time ($$\mathcal{O}(N)$$) and space complexity ($$\mathcal{O}(1)$$).

As a follow-up, we implement a one-pass solution and discuss some
Java-related optimizations in the approach 2. 

#### Approach 1: HashMap: Two Passes

**Algorithm**

- Initialize the number of bulls and cows to zero.

- Initialize the hashmap `character -> its frequency` for the string `secret`.
This hashmap could be later used during the iteration over the string `guess`
to keep the available characters. 

- It's time to iterate over the string `guess`.

    - If the current character `ch` of the string `guess` is in the string `secret`: `if ch in h`, then there could be two situations.
        
        - The corresponding characters of two strings match: `ch == secret[idx]`.
        
            - Then it's time to update the bulls: `bulls += 1`. 
            
            - The update of the cows is needed if the count for the current character in the hashmap is negative or equal to zero. That means that before it was already used for cows,
            and the cows counter should be decreased: `cows -= int(h[ch] <= 0)`.
        
        - The corresponding characters of two strings don't match: `ch != secret[idx]`.
        Then increase the cows counter: `cows += int(h[ch] > 0)`.
        
        - In both cases, one has to update hashmap, marking the 
        current character as used: `h[ch] -= 1`.

- Return the number of bulls and cows.

!?!../Documents/299_LIS.json:1000,371!?!

**Implementation**

<iframe src="https://leetcode.com/playground/NQfNv546/shared" frameBorder="0" width="100%" height="500" name="NQfNv546"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, we pass over the strings two times.

* Space complexity: $$\mathcal{O}(1)$$ to keep hashmap `h` which 
contains at most 10 elements.

<br />
<br />


---
#### Approach 2: One Pass

**Intuition**

Let's optimize approach 1 
by building the hashmap during the strings' parsing. That would allow
us to reduce the number of passes to one.

**Algorithm**

- Initialize the number of bulls and cows to zero.

- Initialize the hashmap to count characters. 
During the iteration, `secret` string gives a 
positive contribution, and `guess` - negative contribution. 

- Iterate over the strings: `s` is the current character in the string 
`secret` and `g` - the current character in the string `guess`.

    - If `s == g`, update bulls counter: `bulls += 1`.
    
    - Otherwise, if `s != g`:
    
        - Update `cows` by adding 1 if so far `guess` contains
         more `s` characters than `secret`: `h[s] < 0`.
        
        - Update `cows` by adding 1 if so far `secret` contains
         more `g` characters than `guess`: `h[g] > 0`.
        
        - Update the hashmap by marking the presence of `s` character in 
        the string `secret`: `h[s] += 1`.
        
        - Update the hashmap by marking the presence of `g` character in 
        the string `guess`: `h[g] -= 1`.

- Return the number of bulls and cows.

**Implementation**

<iframe src="https://leetcode.com/playground/XoMyvsa8/shared" frameBorder="0" width="100%" height="463" name="XoMyvsa8"></iframe>

To further optimize the Java solution, one could use an array instead
of hashmap because there are known problems with 
[Java HashMap performance](https://github.com/vavr-io/vavr/issues/571).
Another small improvement is to replace string concatenation by a 
StringBuilder.

<iframe src="https://leetcode.com/playground/9cxtVCJx/shared" frameBorder="0" width="100%" height="500" name="9cxtVCJx"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, we do one pass over the strings.

* Space complexity: $$\mathcal{O}(1)$$ to keep hashmap (or array) `h` which 
contains at most 10 elements.

<br />
<br />


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### One pass Java solution
- Author: ruben3
- Creation Date: Sat Oct 31 2015 06:17:26 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 14:47:47 GMT+0800 (Singapore Standard Time)

<p>
The idea is to iterate over the numbers in `secret` and in `guess` and count all bulls right away. For cows maintain an array that stores count of the number appearances in `secret` and in `guess`. Increment cows when either number from `secret` was already seen in `guest` or vice versa.


    public String getHint(String secret, String guess) {
        int bulls = 0;
        int cows = 0;
        int[] numbers = new int[10];
        for (int i = 0; i<secret.length(); i++) {
            int s = Character.getNumericValue(secret.charAt(i));
            int g = Character.getNumericValue(guess.charAt(i));
            if (s == g) bulls++;
            else {
                if (numbers[s] < 0) cows++;
                if (numbers[g] > 0) cows++;
                numbers[s] ++;
                numbers[g] --;
            }
        }
        return bulls + "A" + cows + "B";
    }

A slightly more concise version:

    public String getHint(String secret, String guess) {
        int bulls = 0;
        int cows = 0;
        int[] numbers = new int[10];
        for (int i = 0; i<secret.length(); i++) {
            if (secret.charAt(i) == guess.charAt(i)) bulls++;
            else {
                if (numbers[secret.charAt(i)-'0']++ < 0) cows++;
                if (numbers[guess.charAt(i)-'0']-- > 0) cows++;
            }
        }
        return bulls + "A" + cows + "B";
    }
</p>


### [C++] 4ms Straight forward solution two pass O(N) time
- Author: moumoutsay
- Creation Date: Sat Oct 31 2015 04:25:23 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 25 2018 18:40:12 GMT+0800 (Singapore Standard Time)

<p>
The idea is simple, if two char is match, add aCnt, otherwise, record it and process bCnt in second pass.

    class Solution {
    public:
        // only contains digits 
        string getHint(string secret, string guess) {
            int aCnt = 0;
            int bCnt = 0;
            vector<int> sVec(10, 0); // 0 ~ 9 for secret
            vector<int> gVec(10, 0); // 0 ~ 9 for guess 
            if (secret.size() != guess.size() || secret.empty()) { return "0A0B"; }
            for (int i = 0; i < secret.size(); ++i) {
                char c1 = secret[i]; char c2 = guess[i];
                if (c1 == c2) {
                    ++aCnt; 
                } else {
                    ++sVec[c1-'0'];
                    ++gVec[c2-'0'];
                }
            }
            // count b 
            for (int i = 0; i < sVec.size(); ++i) {
                bCnt += min(sVec[i], gVec[i]);
            }
            return to_string(aCnt) + 'A' + to_string(bCnt) + 'B';
        }
    };
</p>


### My 3ms Java solution may help u
- Author: Blankj
- Creation Date: Sun Feb 28 2016 21:17:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 09 2018 09:29:05 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public String getHint(String secret, String guess) {
            int len = secret.length();
    		int[] secretarr = new int[10];
    		int[] guessarr = new int[10];
    		int bull = 0, cow = 0;
    		for (int i = 0; i < len; ++i) {
    			if (secret.charAt(i) == guess.charAt(i)) {
    				++bull;
    			} else {
    				++secretarr[secret.charAt(i) - '0'];
    				++guessarr[guess.charAt(i) - '0'];
    			}
    		}
    		for (int i = 0; i < 10; ++i) {
    			cow += Math.min(secretarr[i], guessarr[i]);
    		}
    		return "" + bull + "A" + cow + "B";
        }
    }
</p>


