---
title: "Handshakes That Don't Cross"
weight: 1096
#id: "handshakes-that-dont-cross"
---
## Description
<div class="description">
<p>You are given an&nbsp;<strong>even</strong> number of people <code>num_people</code>&nbsp;that stand around a circle and each person shakes hands&nbsp;with someone else, so that there are <code>num_people / 2</code> handshakes total.</p>

<p>Return the number of ways these handshakes could occur such that none of the handshakes cross.</p>

<p>Since this number could be very big, return the answer <strong>mod&nbsp;<code>10^9 + 7</code></strong></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> num_people = 2
<strong>Output:</strong> 1
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/07/11/5125_example_2.png" style="width: 651px; height: 311px;" /></p>

<pre>
<strong>Input:</strong> num_people = 4
<strong>Output:</strong> 2
<strong>Explanation:</strong> There are two ways to do it, the first way is [(1,2),(3,4)] and the second one is [(2,3),(4,1)].
</pre>

<p><strong>Example 3:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/07/11/5125_example_3.png" style="width: 664px; height: 992px;" /></p>

<pre>
<strong>Input:</strong> num_people = 6
<strong>Output:</strong> 5
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> num_people = 8
<strong>Output:</strong> 14
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= num_people &lt;= 1000</code></li>
	<li><code>num_people % 2 == 0</code></li>
</ul>

</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] DP and O(N)
- Author: lee215
- Creation Date: Sun Nov 17 2019 00:26:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 02:14:30 GMT+0800 (Singapore Standard Time)

<p>
# Solution 1: DP
`dp[n]` is the number of shaking ways of `n` pairs people
In the the view of first people in these `n` pairs,
he/she can choose anyone, split `i` pairs on his left and `n - 1 - i` pairs on his right.

So here comes the equation of dynamic programme:
`dp[n + 1] = dp[0] * dp[n] + dp[1] * dp[n - 1] + ..... + dp[n] * dp[0]`
<br>

## **Complexity**
Time `O(N^2)`
Space `O(N)`
<br>

**Java:**
```java
    public int numberOfWays(int n) {
        long mod = (long)1e9 + 7;
        long[] dp = new long[n / 2 + 1];
        dp[0] = 1;
        for (int k = 1; k <= n / 2; ++k) {
            for (int i = 0; i < k; ++i) {
                dp[k] = (dp[k] + dp[i] * dp[k - 1 - i]) % mod;
            }
        }
        return (int)dp[n / 2];
    }
```

**C++:**
```cpp
    int numberOfWays(int n) {
        long mod = 1e9 + 7;
        vector<long> dp(n / 2 + 1);
        dp[0] = 1L;
        for (int k = 1; k <= n / 2; ++k) {
            for (int i = 0; i < k; ++i) {
                dp[k] = (dp[k] + dp[i] * dp[k - 1 - i]) % mod;
            }
        }
        return dp[n / 2];
    }
```

**Python3**
```python
    @functools.lru_cache(None)
    def numberOfWays(self, n):
        return sum(self.numberOfWays(i) * self.numberOfWays(n - 2 - i) for i in range(0, n, 2)) if n else 1
```
<br>

# Solution 2: Catalan Numbers
Inspired form @awice

**Java**
```java
    public int numberOfWays(int n) {
        long[] inv = new long[n / 2 + 2];
        inv[1] = 1;
        long mod = (long)1e9 + 7, res = 1;
        for (int i = 2; i < n / 2 + 2; ++i) {
            inv[i] = mod - mod / i * inv[(int)mod % i] % mod;
        }
        for (int i = 1; i <= n / 2; ++i) {
            res = res * (i + n / 2) % mod;
            res = res * inv[i] % mod;
        }
        return (int)(res * inv[n / 2 + 1] % mod);
    }
```
**C++**
```cpp
    int numberOfWays(int n) {
        vector<long> inv(n / 2 + 3);
        inv[1] = 1;
        long mod = 1e9 + 7, res = 1;
        for (int i = 2; i < n / 2 + 2; ++i) {
            inv[i] = mod - mod / i * inv[mod % i] % mod;
        }
        for (int i = 1; i <= n / 2; ++i) {
            res = res * (i + n / 2) % mod;
            res = res * inv[i] % mod;
        }
        return res * inv[n / 2 + 1] % mod;
    }
```
**Python**
```py
    def numberOfWays(self, n):
        res = 1
        for i in xrange(1, n / 2 + 1):
            res *= n - i + 1
            res /= i
        return res / (n / 2 + 1) % (10**9 + 7)
```
</p>


### [Java] 9 lines DP Solution with details explain
- Author: Sun_WuKong
- Creation Date: Sun Nov 17 2019 00:01:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 00:15:38 GMT+0800 (Singapore Standard Time)

<p>
Consider there are n people (n is even)
For those people to not cross hand, person 1 can shake 2, 4, 6, 8, ..., n:
- Shake 2: divide into 2 sets (an emtpy set and a set of people from 3 to n)
- Shake 4: divide into 2 sets (a set of people 2 & 3 and a set of people from 5 to n)
- Shake 6: divide into 2 sets (a set of people from 2 to 5 and a set of people from 7 to n)
...
- Shake n: divide into 2 sets (a set of people from 2 to n-1 and an empty set)

For for n people, there are n/2 way for perosn 1 to shake hand. If person 1 shake hand with person k, there are `count(2 to k-1)*count(k+1 to n)` scenarios.

If we store an array `cache` where `cache[i]` denotes numbers of way when there are i people. Then:
`count(2 to k-1)*count(k+1 to n) = cache[k-2]*cache[n-k]`

So we can apply DP as follow:

```
class Solution {
    public int numberOfWays(int num_people) {
        long M = 1_000_000_007;
        long[] cache = new long[num_people+1];
        cache[0] = 1;
        for (int i = 2; i <= num_people; i += 2) {
            for (int j = 2; j <= i; j += 2) {
                cache[i] = (cache[i] + (cache[j-2]*cache[i-j])) % M;
            }
        }
        return (int)cache[num_people];
    }
}
```
</p>


### [Java] DP Solution with Video
- Author: kelvinchandra1024
- Creation Date: Sun Nov 17 2019 00:49:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 00:49:29 GMT+0800 (Singapore Standard Time)

<p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/YGsmvcQzpxs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>


