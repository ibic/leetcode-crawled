---
title: "Delete Duplicate Emails"
weight: 1480
#id: "delete-duplicate-emails"
---
## Description
<div class="description">
<p>Write a SQL query to <strong>delete</strong> all duplicate email entries in a table named <code>Person</code>, keeping only unique emails based on its <i>smallest</i> <b>Id</b>.</p>

<pre>
+----+------------------+
| Id | Email            |
+----+------------------+
| 1  | john@example.com |
| 2  | bob@example.com  |
| 3  | john@example.com |
+----+------------------+
Id is the primary key column for this table.
</pre>

<p>For example, after running your query, the above <code>Person</code> table should have the following rows:</p>

<pre>
+----+------------------+
| Id | Email            |
+----+------------------+
| 1  | john@example.com |
| 2  | bob@example.com  |
+----+------------------+
</pre>

<p><strong>Note:</strong></p>

<p>Your output is the whole <code>Person</code>&nbsp;table after executing your sql. Use <code>delete</code> statement.</p>

</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `DELETE` and `WHERE` clause [Accepted]

**Algorithm**

By joining this table with itself on the *Email* column, we can get the following code.
```sql
SELECT p1.*
FROM Person p1,
    Person p2
WHERE
    p1.Email = p2.Email
;
```

Then we need to find the bigger id having same email address with other records. So we can add a new condition to the `WHERE` clause like this.

```sql
SELECT p1.*
FROM Person p1,
    Person p2
WHERE
    p1.Email = p2.Email AND p1.Id > p2.Id
;
```

As we already get the records to be deleted, we can alter this statement to `DELETE` in the end.

**MySQL**

```sql
DELETE p1 FROM Person p1,
    Person p2
WHERE
    p1.Email = p2.Email AND p1.Id > p2.Id
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Solution
- Author: fabrizio3
- Creation Date: Sun Mar 29 2015 17:11:46 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 11:40:34 GMT+0800 (Singapore Standard Time)

<p>
> DELETE p1  
FROM Person p1, Person p2  
WHERE p1.Email = p2.Email AND
>               p1.Id > p2.Id

EXPLANATION:

 - Take the table in the example
 
**Id | Email**

**1 | john@example.com** 

**2 | bob@example.com** 

**3 | john@example.com**

 - Join the table on itself by the Email and you'll get:

> FROM Person p1, Person p2  WHERE p1.Email = p2.Email

**p1.Id  | p1.Email | p2.Id | p2.Email**

**1	| john@example.com	| 1	| john@example.com**

**3	| john@example.com      | 1	| john@example.com**

**2	| bob@example.com	| 2	| bob@example.com**

**1	| john@example.com	| 3	| john@example.com**

**3	| john@example.com	| 3	| john@example.com**

 - From this results filter the records that have p1.Id>p2.ID, in this case you'll get just one record:

> AND           p1.Id > p2.Id

**p1.Id  | p1.Email | p2.Id | p2.Email**

**3	| john@example.com	| 1	| john@example.com**

 - This is the record we need to delete, and by saying

> DELETE p1

in this multiple-table syntax, only matching rows from the tables listed before the FROM clause are deleted, in this case just

**p1.Id  | p1.Email**

**3	| john@example.com**

will be deleted
</p>


### A skillful mysql solution  avoid " select and update conflict"
- Author: number_2
- Creation Date: Sat Apr 11 2015 12:14:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 16:07:05 GMT+0800 (Singapore Standard Time)

<p>
**where we try this clause :**

    delete from Person where id not in(select min(id) as id from Person group by email)

you will be noted " **You can't specify target table 'Person' for update in FROM clause** ",
The solution is using a middle table with select clause:

    delete from Person where id not in( 
        select t.id from (
            select min(id) as id from Person group by email
        ) t
    )
</p>


### I can't believe I get it wrong! What's wrong with my code?
- Author: xiangyucao
- Creation Date: Tue Jun 30 2015 12:41:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 04 2018 02:40:17 GMT+0800 (Singapore Standard Time)

<p>
select email,min(id) from Person group by email

This is very simple sql right? How's possible it return duplicate emails?
</p>


