---
title: "Find Kth Bit in Nth Binary String"
weight: 1413
#id: "find-kth-bit-in-nth-binary-string"
---
## Description
<div class="description">
<p>Given two positive integers&nbsp;<code>n</code>&nbsp;and <code>k</code>,&nbsp;the binary string&nbsp;&nbsp;<code>S<sub>n</sub></code>&nbsp;is formed as follows:</p>

<ul>
	<li><code>S<sub>1</sub>&nbsp;= &quot;0&quot;</code></li>
	<li><code>S<sub><span style="font-size: 10.8333px;">i</span></sub>&nbsp;=&nbsp;S<sub><span style="font-size: 10.8333px;">i-1</span></sub>&nbsp;+ &quot;1&quot; + reverse(invert(S<sub><span style="font-size: 10.8333px;">i-1</span></sub>))</code>&nbsp;for&nbsp;<code>i &gt; 1</code></li>
</ul>

<p>Where&nbsp;<code>+</code>&nbsp;denotes the concatenation operation,&nbsp;<code>reverse(x)</code>&nbsp;returns the reversed string <font face="monospace">x,</font>&nbsp;and&nbsp;<code>invert(x)</code>&nbsp;inverts all the bits in <font face="monospace">x</font> (0 changes to 1 and 1 changes to 0).</p>

<p>For example, the first 4 strings in the above sequence are:</p>

<ul>
	<li><code>S<sub>1&nbsp;</sub>= &quot;0&quot;</code></li>
	<li><code>S<sub>2&nbsp;</sub>= &quot;0<strong>1</strong>1&quot;</code></li>
	<li><code>S<sub>3&nbsp;</sub>= &quot;011<strong>1</strong>001&quot;</code></li>
	<li><code>S<sub>4</sub> = &quot;0111001<strong>1</strong>0110001&quot;</code></li>
</ul>

<p>Return <em>the</em> <code>k<sup>th</sup></code> <em>bit</em> <em>in</em>&nbsp;<code>S<sub>n</sub></code>. It is guaranteed that&nbsp;<code>k</code>&nbsp;is valid for the given&nbsp;<code>n</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 3, k = 1
<strong>Output:</strong> &quot;0&quot;
<strong>Explanation: </strong>S<sub>3</sub>&nbsp;is &quot;<strong><u>0</u></strong>111001&quot;. The first bit is &quot;0&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 4, k = 11
<strong>Output:</strong> &quot;1&quot;
<strong>Explanation: </strong>S<sub>4</sub>&nbsp;is &quot;0111001101<strong><u>1</u></strong>0001&quot;. The 11th bit is &quot;1&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 1, k = 1
<strong>Output:</strong> &quot;0&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 2, k = 3
<strong>Output:</strong> &quot;1&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 20</code></li>
	<li><code>1 &lt;= k &lt;= 2<sup>n</sup> - 1</code></li>
</ul>
</div>

## Tags
- String (string)

## Companies
- Rupeek - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Iterative Solutions
- Author: lee215
- Creation Date: Sun Aug 09 2020 12:47:24 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 11 2020 16:58:25 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
1. If `k` is on the left part of the string, we do nothing.

2. If `K` is right in the middle, and the middle is 1.
We flip `count` times, we return `count % 2 == 0 ? \'1\' : \'0\'`

3. If `k` is on the right part of the string,
find it\'s symeric postion `k = l + 1 - k`.
Also we increment `count++`.
<br>

# **Complexity**
Time `O(n)`
Space `O(1)`
<br>

# Solution 0
Same code as explained above.
**Java**
```java
    public char findKthBit(int n, int k) {
        int count = 0, l = (1 << n) - 1;
        while (k > 1) {
            if (k == l / 2 + 1)
                return count % 2 == 0 ? \'1\' : \'0\';
            if (k > l / 2) {
                k = l + 1 - k;
                count++;
            }
            l /= 2;
        }
        return count % 2 == 0 ? \'0\' : \'1\';
    }
```
<br>

# Solution 2
Remove the `count`, just use a `flip` boolean, (short for isFlipped).
**Java:**
```java
    public char findKthBit(int n, int k) {
        int flip = 0, l = (1 << n) - 1;
        while (k > 1) {
            if (k == l / 2 + 1)
                return flip == 0 ? \'1\' : \'0\';
            if (k > l / 2) {
                k = l + 1 - k;
                flip ^= 1;
            }
            l /= 2;
        }
        return flip == 0 ? \'0\' : \'1\';
    }
```

**C++:**
```cpp
    char findKthBit(int n, int k) {
        int flip = 0, l = (1 << n) - 1;
        while (k > 1) {
            if (k == l / 2 + 1)
                return \'0\' + (flip ^ 1);
            if (k > l / 2) {
                k = l + 1 - k;
                flip ^= 1;
            }
            l /= 2;
        }
        return \'0\' + flip;
    }
```

**Python:**
```py
    def findKthBit(self, n, k):
        flip = 0
        l = 2 ** n - 1
        while k > 1:
            if k == l / 2 + 1:
                return str(1 ^ flip)
            if k > l / 2:
                k = l + 1 - k
                flip = 1 - flip
            l /= 2
        return str(flip)
```
<br>

# Solution 3: O(1) One Line
inspired by @hCaulfield and @luffy_is_joyboy, I updated this `O(1)` solution,
[O(1) One Line Solutions](https://leetcode.com/problems/find-kth-bit-in-nth-binary-string/discuss/785548/JavaC++Python-O(1)-Solutions)
</p>


### Java Recursive Solution
- Author: lazysheep
- Creation Date: Sun Aug 09 2020 12:06:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 09 2020 12:09:34 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public char findKthBit(int n, int k) {
        if (n == 1) {
            return \'0\';
        }
        int len = (1 << n) - 1;
        if (k == len / 2 + 1) {
            return \'1\';
        } else if (k > len / 2 + 1) {
            return findKthBit(n - 1, len - k + 1) == \'0\' ? \'1\' : \'0\';
        } else {
            return findKthBit(n - 1, k);
        }
    }
}
```
</p>


### [Java/C++/Python] O(1) Solutions
- Author: lee215
- Creation Date: Tue Aug 11 2020 16:51:36 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 19 2020 00:54:05 GMT+0800 (Singapore Standard Time)

<p>
# Preword
This solution involves all kinds of bit manipulation.
So if you are beginner and not familar with bit manipulation,
it\'s quite normal tht you find that I can\'t explain well enough.

Like: lowbit = x & -x
<br>

# **Explanation 1**
Let\'s define some notation:
Odd sequence: the sequence on odd positions
Even sequence: the sequence on even positions
Zero-One sequence: The sequence of 1,0,1,0,1,0
One-Zero sequence: The sequence of 1,0,1,0,1,0
Sequence `S`, defined as the description.
Magic sequence: we\'ll talk about that later
<br>

1. Take an observation on the odd sequence of `S`:
They are 0,1,0,1,0,1,0.... a Zero-One sequence.   **(a)**
if `k >> 1` is even, return 0,
if `k >> 1` is odd, return 1,
where `k` is 1-based.

So, return `k >> 1 & 1`

2. Take an observation of the sequence on even position:
They are 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0

We call this sequence as magic sequence `M`.
The properties of `M`:
1). It\'s odd sequence is a One-Zero sequence.      **(b)**
2). It\'s even sequence is still a Magic sequence   **(c)**

If for a even number `k`, it\'s in case c.
We do `k = k / 2`, if it\'s even, still in case c.
We keep doing this, until k is odd, now it\'s in case b.

so for a even number k = k2 * pow(2), where `k2` is odd,
if `k2 >> 1` is odd, return 1
if `k2 >> 1` is even, return 0
we return `k2 >> 1 & 1 ^ 1`
<br>


# **Explanation 2**
Every time we want a symmetical of `k`, we do `k = pow(2) - k`.
For example:
k = 215, k = 256 - 215 = 41
k = 41 , k = 64 - 41 = 23
k = 23 , k = 32 - 23 = 9
k = 9  , k = 16 - 9 = 7
k = 7  , k = 8 - 7 = 1,
k = 1, flipped 5 times, return 0.

In general cases,
if `k = 1` in the end, `S(1) = 0`
fliped odd times, return 1
fliped even times, return 0

if `k = pow(2)` (or `k = 0`) in the end, `S(pow(2)) = 1`
fliped odd times, return 0
fliped even times, return 1

So it depends how many times we will flip,
and what\'s the last value.

Assume k = x * pow(2), where x is odd.
The number of flip time, equals to the number of 0,1 groups in `x >> 1`.
If x >> 1 is odd, we\'ll flip odd times.\xA0
If x >> 1 is even, we\'ll flip even times.
<br>

# **Complexity**
Time `O(1)`
Space `O(1)`
<br>

**Java:**
```java
    public char findKthBit(int n, int k) {
        return (char)((k / (k & -k) >> 1 & 1) ^ (k & 1 ^ 1) + \'0\');
    }
```

**C++:**
```cpp
    char findKthBit(int n, int k) {
        return \'0\' + (k / (k & -k) >> 1 & 1) ^ (k & 1 ^ 1);
    }
```

**Python:**
```py
    def findKthBit1(self, n, k):
        return str(k / (k & -k) >> 1 & 1 ^ k & 1 ^ 1)
```

</p>


