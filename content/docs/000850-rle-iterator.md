---
title: "RLE Iterator"
weight: 850
#id: "rle-iterator"
---
## Description
<div class="description">
<p>Write an iterator that iterates through a run-length encoded sequence.</p>

<p>The iterator is initialized by <code>RLEIterator(int[] A)</code>, where <code>A</code> is a run-length encoding of some&nbsp;sequence.&nbsp; More specifically,&nbsp;for all even <code>i</code>,&nbsp;<code>A[i]</code> tells us the number of times that the non-negative integer value <code>A[i+1]</code> is repeated in the sequence.</p>

<p>The iterator supports one function:&nbsp;<code>next(int n)</code>, which exhausts the next <code>n</code> elements&nbsp;(<code>n &gt;= 1</code>) and returns the last element exhausted in this way.&nbsp; If there is no element left to exhaust, <code>next</code>&nbsp;returns <code>-1</code> instead.</p>

<p>For example, we start with <code>A = [3,8,0,9,2,5]</code>, which is a run-length encoding of the sequence <code>[8,8,8,5,5]</code>.&nbsp; This is because the sequence can be read as&nbsp;&quot;three eights, zero nines, two fives&quot;.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;RLEIterator&quot;,&quot;next&quot;,&quot;next&quot;,&quot;next&quot;,&quot;next&quot;]</span>, <span id="example-input-1-2">[[[3,8,0,9,2,5]],[2],[1],[1],[2]]</span>
<strong>Output: </strong><span id="example-output-1">[null,8,8,5,-1]</span>
<strong>Explanation: </strong>
RLEIterator is initialized with RLEIterator([3,8,0,9,2,5]).
This maps to the sequence [8,8,8,5,5].
RLEIterator.next is then called 4 times:

.next(2) exhausts 2 terms of the sequence, returning 8.  The remaining sequence is now [8, 5, 5].

.next(1) exhausts 1 term of the sequence, returning 8.  The remaining sequence is now [5, 5].

.next(1) exhausts 1 term of the sequence, returning 5.  The remaining sequence is now [5].

.next(2) exhausts 2 terms, returning -1.  This is because the first term exhausted was 5,
but the second term did not exist.  Since the last term exhausted does not exist, we return -1.

</pre>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= A.length &lt;= 1000</code></li>
	<li><code>A.length</code>&nbsp;is an even integer.</li>
	<li><code>0 &lt;= A[i] &lt;= 10^9</code></li>
	<li>There are at most <code>1000</code> calls to <code>RLEIterator.next(int n)</code> per test case.</li>
	<li>Each call to&nbsp;<code>RLEIterator.next(int n)</code>&nbsp;will have <code>1 &lt;= n &lt;= 10^9</code>.</li>
</ol>

</div>

## Tags
- Array (array)

## Companies
- Google - 5 (taggedByAdmin: true)
- Wish - 2 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Store Exhausted Position and Quantity

**Intuition**

We can store an index `i` and quantity `q` which represents that `q` elements of `A[i]` (repeated `A[i+1]` times) are exhausted.

For example, if we have `A = [1,2,3,4]` (mapping to the sequence `[2,4,4,4]`) then `i = 0, q = 0` represents that nothing is exhausted; `i = 0, q = 1` represents that `[2]` is exhausted, `i = 2, q = 1` will represent that we have currently exhausted `[2, 4]`, and so on.

**Algorithm**

Say we want to exhaust `n` more elements.  There are currently `D = A[i] - q` elements left to exhaust (of value `A[i+1]`).

If `n > D`, then we should exhaust all of them and continue: `n -= D; i += 2; q = 0`.

Otherwise, we should exhaust some of them and return the current element's value: `q += D; return A[i+1]`.

<iframe src="https://leetcode.com/playground/J4PYA9Z7/shared" frameBorder="0" width="100%" height="480" name="J4PYA9Z7"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N + Q)$$, where $$N$$ is the length of `A`, and $$Q$$ is the number of calls to `RLEIterator.next`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Straightforward Solution, O(n) time, O(1) space
- Author: mrhhsmr
- Creation Date: Sun Sep 09 2018 11:24:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 13:24:36 GMT+0800 (Singapore Standard Time)

<p>
```
class RLEIterator {
    int index;
    int [] A;
    public RLEIterator(int[] A) {
        this.A = A;
        index = 0;
    }
    
    public int next(int n) {
        while(index < A.length && n > A[index]){
            n = n - A[index];
            index += 2;
        }
        
        if(index >= A.length){
            return -1;
        }
        
        A[index] = A[index] - n;
        return A[index + 1];
    }
}
```
Note: input array doesn\'t count as extra space. I would say this problem is a O(n) space problem, but the solution I use is only O(1) space, since I didn\'t allocate any extra space.
</p>


### [Java/Python 3] straightforward code with comment -- O(n) time and O(1) extra space
- Author: rock
- Creation Date: Sun Sep 09 2018 11:19:33 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 02 2020 23:10:59 GMT+0800 (Singapore Standard Time)

<p>
```java
    private int idx = 0;
    private int[] A;
		
    public RLEIterator(int[] A) { this.A = A; } // Injected A[]
    
    public int next(int n) {
        while (idx < A.length && n > A[idx]) { // exhaust as many terms as possible.
            n -= A[idx]; // exhaust A[idx + 1] for A[idx] times. 
            idx += 2; // move to next term.
        }
        if (idx < A.length) { // not exhaust all terms.
            A[idx] -= n;
            return A[idx + 1];
        }
        return -1; // exhaust all terms but still not enough.
    }
```
Similar but clear logic version - credit to **@chenzuojing**
```java
    private int[] A;
    private int index;
    
    public RLEIterator0(int[] A) {
        this.A = A; // Injected A[]
    }
    
    public int next(int n) {
        while (index < A.length) { // all elements exhausted?
            if (n <= A[index]) { // find the corresponding value.
                A[index] -= n; // deduct n from A[index].
                return A[index + 1]; // A[index + 1] is the nth value.
            }
            n -= A[index]; // not find the value yet, deduct A[index] from n.
            index += 2; // next group of same values.
        }
        return -1; // not enough values remaining.
    }
```
```python

    def __init__(self, A: List[int]):
        self.A = A
        self.index = 0
        
    def next(self, n: int) -> int:
        while self.index < len(self.A):
            if n <= self.A[self.index]:
                self.A[self.index] -= n
                return self.A[self.index + 1]
            n -= self.A[self.index]
            self.index += 2
        return -1
```
</p>


### Simple Python Pointer Solution
- Author: zcc230
- Creation Date: Wed Jun 26 2019 02:40:04 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 26 2019 02:40:04 GMT+0800 (Singapore Standard Time)

<p>
```
class RLEIterator:

    def __init__(self, A: List[int]):
        
        self.values = A
        self.i = 0

    def next(self, n: int) -> int:

        while n>0 and self.i<len(self.values):
            if n>self.values[self.i]:
                n -= self.values[self.i]
                self.i+=2
            else:
                self.values[self.i] -= n
                return self.values[self.i+1]
        return -1

```

</p>


