---
title: "Split BST"
weight: 712
#id: "split-bst"
---
## Description
<div class="description">
<p>Given a Binary Search Tree (BST) with root node <code>root</code>, and a target value <code>V</code>, split the tree into two subtrees&nbsp;where one subtree has nodes that are all smaller or equal to the target value, while the other subtree has all nodes that are greater than the target value.&nbsp; It&#39;s not necessarily the case that the tree contains a node with value <code>V</code>.</p>

<p>Additionally, most of the structure of the original tree should remain.&nbsp; Formally, for any child C with parent P in the original tree, if they are both in the same subtree after the split, then node C should still have the parent P.</p>

<p>You should output the root TreeNode of&nbsp;both subtrees after splitting, in any order.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> root = [4,2,6,1,3,5,7], V = 2
<strong>Output:</strong> [[2,1],[4,3,6,null,null,5,7]]
<strong>Explanation:</strong>
Note that root, output[0], and output[1] are TreeNode objects, not arrays.

The given tree [4,2,6,1,3,5,7] is represented by the following diagram:

          4
        /   \
      2      6
     / \    / \
    1   3  5   7

while the diagrams for the outputs are:

          4
        /   \
      3      6      and    2
            / \           /
           5   7         1
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li>The size of the BST will not exceed <code>50</code>.</li>
	<li>The BST is always valid and each node&#39;s value is different.</li>
</ol>
</div>

## Tags
- Tree (tree)
- Recursion (recursion)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)
- Coupang - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach 1: Recursion

**Intuition and Algorithm**

The `root` node either belongs to the first half or the second half.  Let's say it belongs to the first half.

Then, because the given tree is a *binary search tree* (BST), the entire subtree at `root.left` must be in the first half.  However, the subtree at `root.right` may have nodes in either halves, so it needs to be split.

<br />
<center>
    <img src="../Figures/776/split_line.png" alt="Diagram of tree being split" width="350"/>
</center>
<br />

In the diagram above, the thick lines represent the main child relationships between the nodes, while the thinner colored lines represent the subtrees after the split.

Lets say our secondary answer `bns = split(root.right)` is the result of such a split.  Recall that `bns[0]` and `bns[1]` will both be BSTs on either side of the split.  The left half of `bns` must be in the first half, and it must be to the right of `root` for the first half to remain a BST.  The right half of `bns` is the right half in the final answer.

<br />
<center>
    <img src="../Figures/776/sub_tree.png" alt="Diagram of how root tree connects to split of subtree at root.right" width="350"/>
</center>
<br />

The diagram above explains how we merge the two halves of `split(root.right)` with the main tree, and illustrates the line of code `root.right = bns[0]` in the implementations.

<iframe src="https://leetcode.com/playground/MgCV2itW/shared" frameBorder="0" width="100%" height="344" name="MgCV2itW"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of nodes in the input tree, as each node is checked once.

* Space Complexity: $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 👏🏻 Python DFS配图详解 - 公瑾™
- Author: yuzhoujr
- Creation Date: Wed Aug 15 2018 06:28:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 10:53:36 GMT+0800 (Singapore Standard Time)

<p>
### 776. Split BST
```
> \u7C7B\u578B\uFF1ADFS
> Time Complexity O(N)
> Space Complexity O(H)
```

\u8FD9\u9053\u9898\u7ED3\u5408\u4E86BST\u7684\u7279\u6027\uFF0C\u9996\u5148\u8FD4\u56DE\u53C2\u6570\u662F\u4E00\u4E2A\u5305\u542B\u4E86\u4E24\u4E2A`root`\u7684\u6570\u7EC4\u3002\u8FD9\u91CC\u9762\u6709\u4E00\u4E2A`root`\u4E00\u5B9A\u662F\u6211\u4EEC\u8981\u627E\u7684`target`\u3002

#### **\u641C\u7D22\u90E8\u5206**
\u67093\u79CD\u60C5\u51B5\uFF1A
1. \u5982\u679C\u6211\u4EEC\u8981\u627E\u7684`target`\u8981\u6BD4`root.val`\u5C0F\uFF0C\u4F8B\u5982\uFF1A`target = 2, root.val = 4`\uFF0C\u8BC1\u660E\u6211\u4EEC\u8981\u627E\u7684`target`\u4E00\u5B9A\u5728`root.left`\uFF0C\u53F3\u8FB9subtree\u539F\u5C01\u4E0D\u52A8\u3002\u6211\u4EEC\u5219\u9012\u5F52\u53BB`root.left`\u5373\u53EF\u3002
2. \u548C\u7B2C\u4E00\u79CD\u76F8\u53CD\uFF0C\u5DE6\u8FB9subtree\u539F\u5C01\u4E0D\u52A8\u3002\u53BB`root.right`\u5373\u53EF\u3002
3. \u5982\u679C`target == root.val`\uFF0C\u6211\u4EEC\u4ECD\u7136\u8981\u53BB`root.right`\u5BFB\u627E\u53EF\u80FD\u6BD4`root`\u5927\u7684`node`\uFF0C\u7136\u540E\u628A\u8FD9\u4E2A`node`\u8FDB\u884C\u5904\u7406\u3002

\u6240\u4EE5\u4EE5\u4E0A\u76842\u548C3\u5176\u5B9E\u5B8C\u5168\u4E00\u6837\uFF0C\u5C31\u5408\u5E76\u8D77\u6765\u4E3A: `if root.val <= target` \u5373\u53EF

#### **\u526A\u5207\u90E8\u5206**
\u4E24\u79CD\u60C5\u51B5\uFF08\u53C2\u8003\u4E0B\u56FE\uFF09\uFF1A
1. \u7B2C\u4E00\u79CD\u6BD4\u5982\u5728`node`\u4E3A`4`\u7684\u8FD9\u4E00\u5C42\uFF0C\u6211\u4EEC\u8981\u505A\u7684\u662F\u5C06`4`\u7684`root.left = 3`
2. \u7B2C\u4E8C\u79CD\u6BD4\u5982\u5728`node`\u4E3A`2`\u7684\u8FD9\u4E00\u5C42\uFF0C\u6211\u4EEC\u8981\u505A\u7684\u662F\u5C06`2`\u7684`root.right = None`

\u4EE5\u4E0A\u4E24\u79CD\u60C5\u51B5\u53EF\u4EE5\u5728\u6BCF\u6B21\u904D\u5386\u5230\u53F6\u5B50\u8282\u70B9\u4E4B\u540E\uFF0C\u5BF9root\u7684\u5DE6\u53F3\u8282\u70B9\u8FDB\u884C\u50A8\u5B58\u5E76\u4E14\u5C06\u5176\u4F20\u9001\u5230\u4E0A\u4E00\u5C42\uFF0C\u662Fpost-order\u7684\u601D\u60F3\u3002
\u5177\u4F53\u53C2\u8003\u4E0B\u56FE\u7684\u6D41\u7A0B\u3002

![](https://raw.githubusercontent.com/yuzhoujr/spazzatura/master/img_box/776.jpg)

<br>

#### **Code**

```python
class Solution(object):
    def splitBST(self, root, target):
        if not root: return [None, None]
        
        if root.val > target:
            left, right = self.splitBST(root.left, target)
            root.left = right
            return [left, root]
        else:
            left, right = self.splitBST(root.right, target)
            root.right = left
            return [root, right]
                 
```
</p>


### [C++]Easy recursion in O(n)
- Author: facelessvoid
- Creation Date: Sun Feb 04 2018 12:01:49 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 21:59:14 GMT+0800 (Singapore Standard Time)

<p>
Let ```res[0]``` be tree less than or equal to V, ```res[1]``` be tree greater than V. 

Detailed explanation: First of all, we can see that the given root is always there in the answer (either in the bigger subtree or in the smaller subtree). After that, if ```root->val > V```, there is a chance that there is some subtree within the subtree ```root->left``` which maybe greater than V and that subtree needs to be attached to ```root->left```. Now, we see that this problem of finding "subtree in ```root->left``` which is greater than V" is the same as the current problem of splitting ```root```. So we can recurse on left and get the required results. One thing to worry about is, what if there is no subtree in ```root->left``` that is greater than V? This case is handled automatically by the base case.
Similar argument applies for the case ```root->val <= V```.
```
vector<TreeNode*> splitBST(TreeNode* root, int V) {
        vector<TreeNode *> res(2, NULL);
        if(root == NULL) return res;
        
        if(root->val > V){
            res[1] = root;
            auto res1 = splitBST(root->left, V);
            root->left = res1[1];
            res[0]=res1[0];
        }else{
            res[0] = root;
            auto res1 = splitBST(root->right, V);
            root->right = res1[0];
            res[1] = res1[1];
        }
        
        return res;
    }
```
</p>


### Java Recursion in O(logn)
- Author: ywng
- Creation Date: Mon Feb 12 2018 13:00:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 13:03:02 GMT+0800 (Singapore Standard Time)

<p>
The idea is simple by recursion. 

Just think about the current root, after processing, it will need to return [smaller/eq, larger] subtrees

if root.val <=V, all nodes under root.left(including root) will be in the smaller/eq tree, 
we then split the root.right subtree into [smaller/eq, larger](recursion), the root will need to concat the smaller/eq from the subproblem result (recursion). 

Similarly for the case root.val>V

The runtime will be O(logn) if the input is balanced BST. Worst case is O(n) if it is not balanced.

```
class Solution {
    public TreeNode[] splitBST(TreeNode root, int V) {
        if(root==null) return new TreeNode[]{null, null};
        
        TreeNode[] splitted;
        if(root.val<= V) {
            splitted = splitBST(root.right, V);
            root.right = splitted[0];
            splitted[0] = root;
        } else {
            splitted = splitBST(root.left, V);
            root.left = splitted[1];
            splitted[1] = root;
        }
        
        return splitted;
    }
    
}
```
</p>


