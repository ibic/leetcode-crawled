---
title: "Univalued Binary Tree"
weight: 915
#id: "univalued-binary-tree"
---
## Description
<div class="description">
<p>A binary tree is <em>univalued</em> if every node in the tree has the same value.</p>

<p>Return <code>true</code>&nbsp;if and only if the given tree is univalued.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/28/unival_bst_1.png" style="width: 265px; height: 172px;" />
<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,1,1,1,1,null,1]</span>
<strong>Output: </strong><span id="example-output-1">true</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/28/unival_bst_2.png" style="width: 198px; height: 169px;" />
<pre>
<strong>Input: </strong><span id="example-input-2-1">[2,2,2,5,2]</span>
<strong>Output: </strong><span id="example-output-2">false</span>
</pre>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>The number of nodes in the given tree will be in the range <code>[1, 100]</code>.</li>
	<li>Each node&#39;s value will be an integer in the range <code>[0, 99]</code>.</li>
</ol>

</div>

## Tags
- Tree (tree)

## Companies
- Box - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Twilio - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Depth-First Search

**Intuition and Algorithm**

Let's output all the values of the array.  After, we can check that they are all equal.

To output all the values of the array, we perform a depth-first search.

<iframe src="https://leetcode.com/playground/3XKaxj4P/shared" frameBorder="0" width="100%" height="378" name="3XKaxj4P"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the given tree.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Recursion

**Intuition and Algorithm**

A tree is univalued if both its children are univalued, plus the root node has the same value as the child nodes.

We can write our function recursively.  `left_correct` will represent that the left child is correct: ie., that it is univalued, and the root value is equal to the left child's value.  `right_correct` will represent the same thing for the right child.  We need both of these properties to be true.

<iframe src="https://leetcode.com/playground/KZkAAWBG/shared" frameBorder="0" width="100%" height="208" name="KZkAAWBG"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the given tree.

* Space Complexity:  $$O(H)$$, where $$H$$ is the height of the given tree.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Dec 30 2018 12:03:01 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Feb 05 2020 01:06:53 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
If a node has children,
every child tree should be unival tree and has same value.
<br>

## Solution 1:
Check all nodes have the same value as root.

**Java:**
```java
    int val = -1;
    public boolean isUnivalTree(TreeNode root) {
        if (root == null) return true;
        if (val < 0) val = root.val;
        return root.val == val && isUnivalTree(root.left)  && isUnivalTree(root.right);
    }
```

**C++:**
```cpp
    bool isUnivalTree(TreeNode* root, int val = -1) {
        if (!root) return true;
        if (val < 0) val = root->val;
        return root->val == val && isUnivalTree(root->left, val) && isUnivalTree(root->right, val);
    }
```

**Python**
```py
    def isUnivalTree(self, root):
        def dfs(node):
            return not node or node.val == root.val and dfs(node.left) and dfs(node.right)
        return dfs(root)
```
<br>

## Solution 2: 1-Line Idea
Check left and right children have the same value as parent.

**Java:**
```java
    public boolean isUnivalTree(TreeNode root) {
        return (root.left == null || root.left.val == root.val && isUnivalTree(root.left)) &&
               (root.right == null || root.right.val == root.val && isUnivalTree(root.right));
    }
```

**C++:**
```cpp
    bool isUnivalTree(TreeNode* root) {
        return (!root->left || root->left->val == root->val && isUnivalTree(root->left)) &&
               (!root->right || root->right->val == root->val && isUnivalTree(root->right));
    }
```


</p>


### Simple Python Solution Beats 100%
- Author: mokar2001
- Creation Date: Tue Jan 01 2019 05:13:37 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 01 2019 05:13:37 GMT+0800 (Singapore Standard Time)

<p>
# Python3
```
class Solution:
    def isUnivalTree(self, root):
        if not root:
            return True
        
        if root.right:
            if root.val != root.right.val: #Equavalent
                return False
            
        if root.left:
            if root.val != root.left.val: #Equavalent
                return False
        
        return self.isUnivalTree(root.right) and self.isUnivalTree(root.left)
        
        
```
</p>


### [Java/Python 3] BFS and DFS clean codes w/ brief analysis.
- Author: rock
- Creation Date: Sun Dec 30 2018 17:49:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 06 2020 00:21:45 GMT+0800 (Singapore Standard Time)

<p>
**Q & A**
Q: 
What if the root is null?

A:
We can add the following statement as the first line in  isUnivalTree(TreeNode) for both method 1 & 2:

`if (root == null) return true;`

In fact, the problem description states **"The number of nodes in the given tree will be in the range [1, 100]."**, which implies `root != null`.

**End of Q & A**

----

Breadth/Depth first search to check if any node\'s value is different from the root\'s.

**Method 1:**

Iterative version - BFS.

**Analysis: Time & space: O(n).**
```java
    public boolean isUnivalTree(TreeNode root) {
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        while (!q.isEmpty()) {
            TreeNode n = q.poll();
            if (n.val != root.val) { return false; }
            if (n.left != null) { q.offer(n.left); }        
            if (n.right != null) { q.offer(n.right); }        
        }
        return true;
    }
```
```python
    def isUnivalTree(self, root: TreeNode) -> bool:
        dq = collections.deque([root])
        while dq:
            node = dq.popleft();
            if node.val != root.val:
                return False
            dq.extend([n for n in (node.left, node.right) if n])
        return True
```
**Method 2:**

Recursive version - DFS.

**Analysis: Time & space: O(n).**
```java
    public boolean isUnivalTree(TreeNode root) {
        return dfs(root, root.val);
    }
    private boolean dfs(TreeNode n, int v) {
        if (n == null) { return true; }
        if (n.val != v) { return false; }
        return dfs(n.left, v) && dfs(n.right, v);
    }    
```
```python
    def isUnivalTree(self, root: TreeNode) -> bool:
        def dfs(node: TreeNode) -> bool: 
            return node is None or node.val == root.val and dfs(node.left) and dfs(node.right)
        return dfs(root)
```
**Analysis:**

Time: `O(n)`, space: `O(h)`, where `n` and `h` are the total number of nodes and the height of the tree.

</p>


