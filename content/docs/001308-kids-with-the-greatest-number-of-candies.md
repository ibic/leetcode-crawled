---
title: "Kids With the Greatest Number of Candies"
weight: 1308
#id: "kids-with-the-greatest-number-of-candies"
---
## Description
<div class="description">
<p>Given the array <code>candies</code> and the integer <code>extraCandies</code>, where <code>candies[i]</code> represents the number of candies that the <strong><em>ith</em></strong> kid has.</p>

<p>For each kid check if there is a way to distribute <code>extraCandies</code> among the kids such that he or she can have the <strong>greatest</strong> number of candies among them.&nbsp;Notice that multiple kids can have the <strong>greatest</strong> number of candies.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> candies = [2,3,5,1,3], extraCandies = 3
<strong>Output:</strong> [true,true,true,false,true] 
<strong>Explanation:</strong> 
Kid 1 has 2 candies and if he or she receives all extra candies (3) will have 5 candies --- the greatest number of candies among the kids. 
Kid 2 has 3 candies and if he or she receives at least 2 extra candies will have the greatest number of candies among the kids. 
Kid 3 has 5 candies and this is already the greatest number of candies among the kids. 
Kid 4 has 1 candy and even if he or she receives all extra candies will only have 4 candies. 
Kid 5 has 3 candies and if he or she receives at least 2 extra candies will have the greatest number of candies among the kids. 
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> candies = [4,2,1,1,2], extraCandies = 1
<strong>Output:</strong> [true,false,false,false,false] 
<strong>Explanation:</strong> There is only 1 extra candy, therefore only kid 1 will have the greatest number of candies among the kids regardless of who takes the extra candy.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> candies = [12,1,12], extraCandies = 10
<strong>Output:</strong> [true,false,true]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= candies.length &lt;= 100</code></li>
	<li><code>1 &lt;= candies[i] &lt;= 100</code></li>
	<li><code>1 &lt;= extraCandies &lt;= 50</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] 2 liners O(n).
- Author: rock
- Creation Date: Sun May 03 2020 00:02:29 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 02 2020 18:49:51 GMT+0800 (Singapore Standard Time)

<p>
Find the `max` of `candies`, then traverse to check if each `candy[i]` + `extraCandies` >= `max`.
```java
    public List<Boolean> kidsWithCandies(int[] candies, int extraCandies) {
        List<Boolean> ans = new ArrayList<>();
        int max = 0;
        for (int candy : candies) {
            max = Math.max(candy, max);
        }
        for (int candy : candies) {
            ans.add(candy + extraCandies >= max);
        }
        return ans;
    }
```
Java 8 stream version:
```java
    public List<Boolean> kidsWithCandies(int[] candies, int extraCandies) {
        int max = Arrays.stream(candies).max().getAsInt();
        return Arrays.stream(candies).mapToObj(candy -> candy + extraCandies >= max).collect(Collectors.toList());
    }
```
```python
    def kidsWithCandies(self, candies: List[int], extraCandies: int) -> List[bool]:
        M = max(candies)
        return [candy + extraCandies >= M for candy in candies]
```
</p>


### [JavaScript] Easy to understand - from common to 1 line
- Author: poppinlp
- Creation Date: Thu May 14 2020 14:57:14 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 14 2020 14:57:14 GMT+0800 (Singapore Standard Time)

<p>
The strategy for this problem is pretty straight forward. So, we could easily get this code:

```js
const kidsWithCandies = (candies, extraCandies) => {
  const ret = [];
  let max = 0;
  for (const val of candies) {
    val > max && (max = val);
  }
  for (let i = 0; i < candies.length; ++i) {
    ret.push(candies[i] + extraCandies >= max);
  }
  return ret;
};
```

Then, we could use some native methods to make it more concise:

```js
const kidsWithCandies = (candies, extraCandies) => {
  const max = Math.max(...candies);
  return candies.map(candy => candy + extraCandies >= max);
};
```

Finally, it could be written in 1 line even:

```js
const kidsWithCandies = (candies, extraCandies, max = Math.max(...candies)) => candies.map(candy => candy + extraCandies >= max);
```
</p>


### Python 3 -> Same solution but different variations
- Author: navinmittal29
- Creation Date: Sun Sep 06 2020 10:51:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 10:51:11 GMT+0800 (Singapore Standard Time)

<p>
**Suggestions to make it better are always welcomed.**

We have to traverse the input list from start to end. Time: O(n).
Here\'s the same solution which is followed by code that reduces few lines compared to previous solution.

**Solution 1:**
```
def kidsWithCandies(self, candies: List[int], extraCandies: int) -> List[bool]:
	maxCandies = max(candies)
	result = []
	for i in range(len(candies)):
		if candies[i]+extraCandies >= maxCandies:
			result.append(True)
		else:
			result.append(False)
	return result
```

**Solution 2:**
```
def kidsWithCandies(self, candies: List[int], extraCandies: int) -> List[bool]:
	maxCandies = max(candies)
	result = []
	for i in range(len(candies)):            
		result.append(candies[i]+extraCandies >= maxCandies)            
	return result
```

**Solution 3:** I found this method in discuss forum
```
def kidsWithCandies(self, candies: List[int], extraCandies: int) -> List[bool]:
	maxCandies = max(candies)
	return [candy+extraCandies >= maxCandies for candy in candies]
```

**I hope that you\'ve found this useful.
In that case, please upvote. It only motivates me to write more such posts\uD83D\uDE03**

</p>


