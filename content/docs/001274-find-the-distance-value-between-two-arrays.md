---
title: "Find the Distance Value Between Two Arrays"
weight: 1274
#id: "find-the-distance-value-between-two-arrays"
---
## Description
<div class="description">
<p>Given two integer arrays <code>arr1</code> and <code>arr2</code>, and the integer <code>d</code>, <em>return the distance value between the two&nbsp;arrays</em>.</p>

<p>The distance value is defined as the number of elements <code>arr1[i]</code> such that there is not any element <code>arr2[j]</code> where <code>|arr1[i]-arr2[j]| &lt;= d</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr1 = [4,5,8], arr2 = [10,9,1,8], d = 2
<strong>Output:</strong> 2
<strong>Explanation:</strong> 
For arr1[0]=4 we have: 
|4-10|=6 &gt; d=2 
|4-9|=5 &gt; d=2 
|4-1|=3 &gt; d=2 
|4-8|=4 &gt; d=2 
For arr1[1]=5 we have: 
|5-10|=5 &gt; d=2 
|5-9|=4 &gt; d=2 
|5-1|=4 &gt; d=2 
|5-8|=3 &gt; d=2
For arr1[2]=8 we have:
<strong>|8-10|=2 &lt;= d=2</strong>
<strong>|8-9|=1 &lt;= d=2</strong>
|8-1|=7 &gt; d=2
<strong>|8-8|=0 &lt;= d=2</strong>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr1 = [1,4,2,3], arr2 = [-4,-3,6,10,20,30], d = 3
<strong>Output:</strong> 2
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr1 = [2,1,100,3], arr2 = [-5,-2,10,-3,7], d = 6
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr1.length, arr2.length &lt;= 500</code></li>
	<li><code>-10^3 &lt;= arr1[i], arr2[j] &lt;= 10^3</code></li>
	<li><code>0 &lt;= d &lt;= 100</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Uber - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### This code challenge contains a very bad description.
- Author: donutloop
- Creation Date: Wed Mar 25 2020 19:47:13 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Mar 25 2020 19:47:31 GMT+0800 (Singapore Standard Time)

<p>
This description is pretty bad and example, language are very misleading.
</p>


### [Python] 1-Line
- Author: lee215
- Creation Date: Sun Mar 22 2020 00:06:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2020 00:06:31 GMT+0800 (Singapore Standard Time)

<p>
**Python:**
```py
    def findTheDistanceValue(self, A, B, d):
        return sum(all(abs(a - b) > d for b in B) for a in A)
```

</p>


### [Python], two pointers O(n log n)
- Author: bison_a_besoncon
- Creation Date: Sun Mar 22 2020 00:10:42 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Mar 25 2020 01:37:26 GMT+0800 (Singapore Standard Time)

<p>
```

# Some remarks on how to interpret this algorithm.
#
# Each branch of the nested if-else statement will lead you to a single conclusion about your
# current configuration of pointers regarding two questions:
# 1. does the i-th element of arr1 sastisfies distance condition or not -- if not we drop i-th
# element, i.e. ignore augmenting distance counter and advance the pointer
# 2. is the j-th element of arr2 neccessary for comparisons with current or next elements of
# arr1 -- if not we advance the j pointer
#
# The concluding correction accounts for the tail of arr1 in the case when its values are greater
# than all of the arr2. I need it because my algorithm for the sake of simplicity and its
# correctness assumes that there will be always a concluding element of arr2 that is greater
# that any elmeent of arr1. You can see on the test sets it is not always the case, therefore is
# the correction.


class Solution:
    def findTheDistanceValue(self, arr1, arr2, d):
        arr1.sort()
        arr2.sort()
        i = 0
        j = 0
        dist = 0
        while i < len(arr1) and j < len(arr2):
            if arr1[i] >= arr2[j]:
                if arr1[i] - arr2[j] > d:
                    j += 1
                else:
                    i += 1
            else:
                if arr2[j] - arr1[i] > d:
                    i += 1
                    dist += 1
                else:
                    i += 1
        dist += len(arr1) - i
        return dist
```
</p>


