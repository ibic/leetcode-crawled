---
title: "Spiral Matrix III"
weight: 835
#id: "spiral-matrix-iii"
---
## Description
<div class="description">
<p>On a 2 dimensional grid with <code>R</code> rows and <code>C</code> columns, we start at <code>(r0, c0)</code> facing east.</p>

<p>Here, the north-west corner of the grid is at the&nbsp;first row and column, and the south-east corner of the grid is at the last row and column.</p>

<p>Now, we walk in a clockwise spiral shape to visit every position in this grid.&nbsp;</p>

<p>Whenever we would move outside the boundary of the grid, we continue our walk outside the grid (but may return to the grid boundary later.)&nbsp;</p>

<p>Eventually, we reach all <code>R * C</code> spaces of the grid.</p>

<p>Return a list of coordinates representing the positions of the grid in the order they were visited.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>R = <span id="example-input-1-1">1</span>, C = <span id="example-input-1-2">4</span>, r0 = <span id="example-input-1-3">0</span>, c0 = <span id="example-input-1-4">0</span>
<strong>Output: </strong><span id="example-output-1">[[0,0],[0,1],[0,2],[0,3]]</span>

<img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/08/24/example_1.png" style="width: 174px; height: 99px;" />
</pre>

<p>&nbsp;</p>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>R = <span id="example-input-2-1">5</span>, C = <span id="example-input-2-2">6</span>, r0 = <span id="example-input-2-3">1</span>, c0 = <span id="example-input-2-4">4</span>
<strong>Output: </strong><span id="example-output-2">[[1,4],[1,5],[2,5],[2,4],[2,3],[1,3],[0,3],[0,4],[0,5],[3,5],[3,4],[3,3],[3,2],[2,2],[1,2],[0,2],[4,5],[4,4],[4,3],[4,2],[4,1],[3,1],[2,1],[1,1],[0,1],[4,0],[3,0],[2,0],[1,0],[0,0]]</span>

<img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/08/24/example_2.png" style="width: 202px; height: 142px;" />
</pre>

<div>
<div>
<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= R &lt;= 100</code></li>
	<li><code>1 &lt;= C &lt;= 100</code></li>
	<li><code>0 &lt;= r0 &lt; R</code></li>
	<li><code>0 &lt;= c0 &lt; C</code></li>
</ol>
</div>
</div>
</div>

## Tags
- Math (math)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Dataminr - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Walk in a Spiral

**Intuition**

We can walk in a spiral shape from the starting square, ignoring whether we stay in the grid or not.  Eventually, we must have reached every square in the grid.

**Algorithm**

Examining the lengths of our walk in each direction, we find the following pattern: `1, 1, 2, 2, 3, 3, 4, 4, ...`  That is, we walk 1 unit east, then 1 unit south, then 2 units west, then 2 units north, then 3 units east, etc.  Because our walk is self-similar, this pattern repeats in the way we expect.

After, the algorithm is straightforward: perform the walk and record positions of the grid in the order we visit them.  Please read the inline comments for more details.

<iframe src="https://leetcode.com/playground/AXwzLMCb/shared" frameBorder="0" width="100%" height="500" name="AXwzLMCb"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O((\max(R, C))^2)$$.  Potentially, our walk needs to spiral until we move $$R$$ in one direction, and $$C$$ in another direction, so as to reach every cell of the grid.

* Space Complexity:  $$O(R * C)$$, the space used by the answer.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] 1,1,2,2,3,3 Steps
- Author: lee215
- Creation Date: Sun Aug 12 2018 11:05:25 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 15 2019 13:55:56 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**:
Take steps one by one.
If the location is inside of grid, add it to `res`.
But how to simulate the path?

It seems to be annoying, but if we observer the path:

move right `1` step, turn right
move down `1` step, turn right
move left `2` steps, turn right
move top `2` steps, turn right,
move right `3` steps, turn right
move down `3` steps, turn right
move left `4` steps, turn right
move top `4` steps, turn right,

we can find the sequence of steps: 1,1,2,2,3,3,4,4,5,5....

So there are two thing to figure out:
1. how to generate sequence 1,1,2,2,3,3,4,4,5,5
2. how to turn right?
<br>

## **Generate sequence 1,1,2,2,3,3,4,4,5,5**
Let `n` be index of this sequence.
Then `A0 = 1`, `A1 = 1`, `A2 = 2` ......
We can find that `An = n / 2 + 1`
<br>

## **How to turn right?**
By cross product:
Assume current direction is (x, y) in plane, which is (x, y, 0) in space.
Then the direction after turn right (x, y, 0) \xD7 (0, 0, 1) = (y, -x, 0)
Translate to code: `tmp = x; x = y; y = -tmp;`

By arrays of arrays:
The directions order is (0,1),(1,0),(0,-1),(-1,0), then repeat.
Just define a variable.
<br>

## **Time Complexity**:
Time `O(max(R,C)^2)`
Space `O(R*C)` for output
<br>

**Java:**
```java
    public int[][] spiralMatrixIII(int R, int C, int x, int y) {
        int[][] res = new int[R * C][2];
        int dx = 0, dy = 1, n = 0, tmp;
        for (int j = 0; j < R * C; ++n) {
            for (int i = 0; i < n / 2 + 1; ++i) {
                if (0 <= x && x < R && 0 <= y && y < C)
                    res[j++] = new int[] {x, y};
                x += dx;
                y += dy;
            }
            tmp = dx;
            dx = dy;
            dy = -tmp;
        }
        return res;
    }
```
**C++:**
```cpp
    vector<vector<int>> spiralMatrixIII(int R, int C, int r, int c) {
        vector<vector<int>> res = {{r, c}};
        int dx = 0, dy = 1, tmp;
        for (int n = 0; res.size() < R * C; n++) {
            for (int i = 0; i < n / 2 + 1; i++) {
                r += dx, c += dy;
                if (0 <= r && r < R && 0 <= c && c < C)
                    res.push_back({r, c});
            }
            tmp = dx, dx = dy, dy = -tmp;
        }
        return res;
    }
```
**Python:**
```python
    def spiralMatrixIII(self, R, C, x, y):
        res = []
        dx, dy, n = 0, 1, 0
        while len(res) < R * C:
            for i in xrange(n / 2 + 1):
                if 0 <= x < R and 0 <= y < C:
                    res.append([x, y])
                x, y = x + dx, y + dy
            dx, dy, n = dy, -dx, n + 1
        return res
```

</p>


### Java 15 lines concise solution with comments
- Author: BingleLove
- Creation Date: Sun Aug 12 2018 11:09:19 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 09:07:31 GMT+0800 (Singapore Standard Time)

<p>
```Java
    public int[][] spiralMatrixIII(int R, int C, int r0, int c0) {
        int[][] dirt = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}; // east, south, west, north
        List<int[]> res = new ArrayList<>();
        int len = 0, d = 0; // move <len> steps in the <d> direction
        res.add(new int[]{r0, c0});
        while (res.size() < R * C) {
            if (d == 0 || d == 2) len++; // when move east or west, the length of path need plus 1 
            for (int i = 0; i < len; i++) {
                r0 += dirt[d][0];
                c0 += dirt[d][1];
                if (r0 >= 0 && r0 < R && c0 >= 0 && c0 < C) // check valid
                    res.add(new int[]{r0, c0});
            }
            d = (d + 1) % 4; // turn to next direction
        }
        return res.toArray(new int[R * C][2]);
    }
```
</p>


### [Python] Sort All Coordinates
- Author: lee215
- Creation Date: Sun Aug 12 2018 11:06:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 10:58:10 GMT+0800 (Singapore Standard Time)

<p>
Put all valid coordinates to a list `res`

Sort all coordinates by the following rule:

0. Change coordinates to a relative coordinates to center.

1. Sort ascending by the distance to the center `max(abs(x), abs(y))`
If `max(abs(x), abs(y)) == 0`, it\'s the center.
If `max(abs(x), abs(y)) == 1`, it\'s in the first layer around the center

2. Sort descending by the angle to the center `max(abs(x), abs(y))`


**Python:**
```
    def spiralMatrixIII(self, R, C, r, c):
        def key((x, y)):
            x, y = x - r, y - c
            return (max(abs(x), abs(y)), -((math.atan2(-1, 1) - math.atan2(x, y)) % (math.pi * 2)))
        return sorted([(i, j) for i in xrange(R) for j in xrange(C)], key=key)
```

</p>


