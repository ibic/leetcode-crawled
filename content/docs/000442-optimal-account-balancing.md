---
title: "Optimal Account Balancing"
weight: 442
#id: "optimal-account-balancing"
---
## Description
<div class="description">
<p>A group of friends went on holiday and sometimes lent each other money. For example, Alice paid for Bill's lunch for $10. Then later Chris gave Alice $5 for a taxi ride. We can model each transaction as a tuple (x, y, z) which means person x gave person y $z. Assuming Alice, Bill, and Chris are person 0, 1, and 2 respectively (0, 1, 2 are the person's ID), the transactions can be represented as <code>[[0, 1, 10], [2, 0, 5]]</code>.</p>

<p>Given a list of transactions between a group of people, return the minimum number of transactions required to settle the debt.</p>

<p><b>Note:</b>
<ol>
<li>A transaction will be given as a tuple (x, y, z). Note that <code>x &ne; y</code> and <code>z > 0</code>.</li>
<li>Person's IDs may not be linear, e.g. we could have the persons 0, 1, 2 or we could also have the persons 0, 2, 6.</li>
</ol>
</p>

<p><b>Example 1:</b>
<pre>
<b>Input:</b>
[[0,1,10], [2,0,5]]

<b>Output:</b>
2

<b>Explanation:</b>
Person #0 gave person #1 $10.
Person #2 gave person #0 $5.

Two transactions are needed. One way to settle the debt is person #1 pays person #0 and #2 $5 each.
</pre>
</p>

<p><b>Example 2:</b>
<pre>
<b>Input:</b>
[[0,1,10], [1,0,1], [1,2,5], [2,0,5]]

<b>Output:</b>
1

<b>Explanation:</b>
Person #0 gave person #1 $10.
Person #1 gave person #0 $1.
Person #1 gave person #2 $5.
Person #2 gave person #0 $5.

Therefore, person #1 only need to give person #0 $4, and all debt is settled.
</pre>
</p>
</div>

## Tags


## Companies
- Google - 4 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise 9ms DFS solution (detailed explanation)
- Author: zzg_zzm
- Creation Date: Sat Jan 21 2017 14:00:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 01 2020 11:59:27 GMT+0800 (Singapore Standard Time)

<p>
With all the given transactions, in the end, each person with ID = `id` will have an overall balance `bal[id]`. Note that the `id` value or any person coincidentally with `0` balance is irrelevant to debt settling count, so we can simply use an array `debt[]` to store all non-zero balances, where
* `debt[i] > 0` means a person needs to pay `$ debt[i]` to other person(s);
* `debt[i] < 0` means a person needs to collect `$ debt[i]` back from other person(s).

Starting from first debt `debt[0]`, we look for all other debts `debt[i]` (`i>0`) which have opposite sign to `debt[0]`. Then each such `debt[i]` can make one transaction `debt[i] += debt[0]` to clear the person with debt `debt[0]`. From now on, the person with debt `debt[0]` is dropped out of the problem and we recursively drop persons one by one until everyone\'s debt is cleared meanwhile  updating the minimum number of transactions during DFS.

Note: Thanks to @KircheisHe who found the following great paper about the debt settling problem:
* [Settling Multiple Debts Efficiently: An Invitation to Computing Science](http://www.mathmeth.com/tom/files/settling-debts.pdf) by T. Verhoeff, June 2003.

The question can be transferred to a [3-partition problem](https://en.wikipedia.org/wiki/3-partition_problem), which is NP-Complete.

```
public:
    int minTransfers(vector<vector<int>>& trans) 
	{
        unordered_map<int, long> bal; // each person\'s overall balance
        for(auto& t: trans) {
		  bal[t[0]] -= t[2];
		  bal[t[1]] += t[2];
		}
		
        for(auto& p: bal) // only deal with non-zero debts
		  if(p.second) debt.push_back(p.second);
		  
        return dfs(0);
    }
    
private:
    int dfs(int s) // min number of transactions to settle starting from debt[s]
	{ 
    	while (s < debt.size() && !debt[s]) ++s; // get next non-zero debt
		
    	int res = INT_MAX;
    	for (long i = s+1, prev = 0; i < debt.size(); ++i)
    	  if (debt[i] != prev && debt[i]*debt[s] < 0) // skip already tested or same sign debt
		  {
		    debt[i] += debt[s]; 
			res = min(res, 1+dfs(s+1)); 
			prev = (debt[i]-=debt[s]);
		  }
    	    
    	return res < INT_MAX? res : 0;
    }
    
    vector<long> debt; // all non-zero debts
```
</p>


### Recursion Logical Thinking
- Author: GraceMeng
- Creation Date: Mon May 14 2018 13:34:30 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 10:29:58 GMT+0800 (Singapore Standard Time)

<p>
>? what does it mean to settle the debt
> nobody owes others
> 
>? how do we represent how much money a person owes others
> We build current debt situation `debt[],` e.g. debt[i] = 10 means a person owes others 10
>
> ? how do we settle one\'s debt
> assuming [0, curId - 1] has been settled,
> for `debt[curId]`, 
> any `curId + 1 <= i <debt.length` such that `debt[i] * debt[curId] < 0` can settle it 

>**state**
The next account to balance, `curId`, can uniquely identify a state
**state function**	
`state(debt[], curId)` is the minimum transactions to balance debts[curId...debtLength - 1] such that debts[0...curId-1] are balanced.
**goal state**	
`state(initial debt[], 0)`
**state transition**	
now:  state(debt[], curId) 
next: state (debt[] after balance curId, curId + 1)
```
state(debt[], curId) = 1 + min(state (debt[] after balance curId, curId + 1))
```

**Note**
- How do we decide who can balance the account of\xA0curId?
There are many people who can balance\xA0curId\'s account -- `person i behind\xA0curId\xA0with\xA0debt[i] * debt[curId] < 0`.
****
```
    public int minTransfers(int[][] transactions) {
        int[] debt = buildDebtArray(transactions); // Debt amount to balance for each person.
        
        return getMinTransfersAfter(0, debt);
    }
    
    private int getMinTransfersAfter(int curId, int[] debt) {
        while (curId < debt.length && debt[curId] == 0)
            curId++;
        // Base case.
        if (curId == debt.length)
            return 0;
        // Recursive case.
        int minTransactions = Integer.MAX_VALUE;
        for (int i = curId + 1; i < debt.length; i++) {
            if (debt[i] * debt[curId] < 0) {
                debt[i] += debt[curId];
                minTransactions = Math.min(minTransactions, getMinTransfersAfter(curId + 1, debt) + 1);
                debt[i] -= debt[curId];
            }
        }
        
        return minTransactions;
    }
    
    private int[] buildDebtArray(int[][] transactions) {
        Map<Integer, Integer> debtMap = new HashMap<>(); // Map person ID to debt amount.
        
        for (int[] transaction : transactions) {
            int giver = transaction[0];
            int taker = transaction[1];
            int amount = transaction[2];
            debtMap.put(giver, debtMap.getOrDefault(giver, 0) + amount);
            debtMap.put(taker, debtMap.getOrDefault(taker, 0) - amount);
        }
        
        int[] debt = new int[debtMap.size()];
        int i = 0;
        for (int amount : debtMap.values()) {
            debt[i++] = amount;
        }
        
        return debt;
    }
```
**(\u4EBA \u2022\u0348\u1D17\u2022\u0348)** Thanks for voting!
</p>


### Short O(N * 2^N) DP solution with detailed explanation and complexity analysis
- Author: yuxiong
- Creation Date: Tue Jan 15 2019 08:49:18 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 15 2019 08:49:18 GMT+0800 (Singapore Standard Time)

<p>
The main idea is similar to this post: https://leetcode.com/problems/optimal-account-balancing/discuss/173299/Python-BFS-beats-100

First, we need to exclude those people who ends up not owing or owed money.

Then we will have an array of non-zero numbers, which sums to be zero. For example:
[4, -2, -2, 6, -6]

Then the problem can be converted to the following:
*     Given N accounts/persons who have non zero amounts,
*     find the M = max number of sets where each set s has sum(s)==0.
*     Each of these sets must be min_set. In other words, min_set CANNOT be divided to setA and setB where sum(setA) == sum(setB)==0.
And the answer would be `N - M`. Why?
Let\'s see an example: [-2, 2, -7, 3, 4]
Say we have N = 5, M=2.
set1 = {-2, 2}, set2 = {-7,3,4}
Let n1 = len(set1) = 2, and n2 = len(set2) = 3.
Because both set1 and set2 are min_set, we have:
* For set1, we need n1 - 1 = 1 transaction to settle the debt.
* For set2, we need n2 - 1 = 2 transactions to settle the debt.

Then we are done, we don\'t need any other transactions.
So in other words, for each min_set we need `len(min_set) - 1` transaction to settle the debt.
Then we have:
```
ans = (transactions for min_set1) + (transactions for min_set2) + ... + (transactions for min_setm)
ans = len(min_set1) - 1 + len(min_set2) - 1 + ... + len(min_setm) - 1
Note: len(min_set1) + len(min_set2) + ... + len(min_setm) = N,
and there are M (-1)\'s.
So we have ans = N - M.
```

Once this problem is converted to finding the max number of min_sets, you can solve it in multiple ways: DFS, backtracking, BFS, and so on.

Here I present a DP solution which has the benefit of simple implementation and easier complexity analysis.
The optimal substructure is as following:
Let set[mask] be the set of elements whose position is 1 in the mask.
For example: [-2, 2, -7, 3, 4], set[binary(10000)] = {-2}, set[binary(01010)] = {2, 3}.
Let dp[mask] be the maximum number of min_sets can be formed by the elements in set[mask].
Let sum[mask] be the sum of the elements in set[mask].
Let\'s define sub_mask as a binary number which has 1 less set_bit than mask. For example: binary(01010) is a sub_mask of binary(01110) , binary(00110) is also a sub_mask of binary(01110).
Then:
```
	if sum[mask] == 0:
		dp[mask] = max(dp[sub_mask] + 1) for all sub_masks.
	else:
		dp[mask] = max(dp[sub_mask]) for all sub_masks.
```

Implementation:
```
class Solution:
    def minTransfers(self, transactions):
        persons = collections.defaultdict(int)
        for sender, receiver, amount in transactions:
            persons[sender] -= amount
            persons[receiver] += amount
        
        amounts = [amount for amount in persons.values() if amount != 0]
        
        N = len(amounts)
        dp = [0] * (2**N) # dp[mask] = number of sets whose sum = 0
        sums = [0] * (2**N) # sums[mask] = sum of numbers in mask
        
        for mask in range(2**N):
            set_bit = 1
            for b in range(N):
                if mask & set_bit == 0:
                    nxt = mask | set_bit
                    sums[nxt] = sums[mask] + amounts[b]
                    if sums[nxt] == 0: dp[nxt] = max(dp[nxt], dp[mask] + 1)
                    else: dp[nxt] = max(dp[nxt], dp[mask])
                set_bit <<= 1
        
        return N - dp[-1]
```

Complexity Analysis:
Running Time: O(N * 2^N), there are 2^N subproblems, each subproblem contributes to O(N) larger problems.
Space: O(2^N)

*Please upvote if you like it.*
</p>


