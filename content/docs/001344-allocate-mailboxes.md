---
title: "Allocate Mailboxes"
weight: 1344
#id: "allocate-mailboxes"
---
## Description
<div class="description">
<p>Given the array <code>houses</code> and an integer <code>k</code>. where <code>houses[i]</code> is the location of the ith house along a street, your task is to allocate <code>k</code> mailboxes in&nbsp;the street.</p>

<p>Return the <strong>minimum</strong> total distance between each house and its nearest mailbox.</p>

<p>The answer is guaranteed to fit in a 32-bit signed integer.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/05/07/sample_11_1816.png" style="width: 454px; height: 154px;" /></p>

<pre>
<strong>Input:</strong> houses = [1,4,8,10,20], k = 3
<strong>Output:</strong> 5
<strong>Explanation: </strong>Allocate mailboxes in position 3, 9 and 20.
Minimum total distance from each houses to nearest mailboxes is |3-1| + |4-3| + |9-8| + |10-9| + |20-20| = 5 
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/05/07/sample_2_1816.png" style="width: 433px; height: 154px;" /></strong></p>

<pre>
<strong>Input:</strong> houses = [2,3,5,12,18], k = 2
<strong>Output:</strong> 9
<strong>Explanation: </strong>Allocate mailboxes in position 3 and 14.
Minimum total distance from each houses to nearest mailboxes is |2-3| + |3-3| + |5-3| + |12-14| + |18-14| = 9.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> houses = [7,4,6,1], k = 1
<strong>Output:</strong> 8
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> houses = [3,6,14,10], k = 4
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == houses.length</code></li>
	<li><code>1 &lt;= n&nbsp;&lt;= 100</code></li>
	<li><code>1 &lt;= houses[i] &lt;= 10^4</code></li>
	<li><code>1 &lt;= k &lt;= n</code></li>
	<li>Array <code>houses</code> contain unique integers.</li>
</ul>
</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Bloomberg - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Top down DP - Prove median mailbox - O(n^3)
- Author: hiepit
- Creation Date: Sun Jun 14 2020 01:38:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 21 2020 17:57:16 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
The idea is we try to allocate each mailbox to `k` group of the consecutive houses `houses[i:j]`. We found a solution if we can distribute total `k` mailboxes to `n` houses devided into `k` groups `[0..i], [i+1..j], ..., [l..n-1]`. 
After all, we choose the **minimum cost** among our solutions.
![image](https://assets.leetcode.com/users/hiepit/image_1592073922.png)
*(Attached image from Leetcode for easy to understand the idea)*


`costs[i][j]` is the cost to put a mailbox among `houses[i:j]`, the best way is put the mail box at **median position** among houses[i:j]
![image](https://assets.leetcode.com/users/hiepit/image_1592098756.png)



**Complexity**
- Time: `O(n^3)`
	+ `costs` takes `O(n^3)`
	+ `dp` takes `O(k*n*n)`, because `dp(k, i)` has total `k*n` states, each state need a for loop up to `n` elements.
- Space: `O(n^2)`

**Python 3 ~ 540ms**
```python
class Solution:
    def minDistance(self, houses: List[int], k: int) -> int:
        n = len(houses)
        houses = sorted(houses)
        costs = [[0] * n for _ in range(n)]
        for i in range(n):
            for j in range(n):
                median = houses[(i + j) // 2]
                for t in range(i, j + 1):
                    costs[i][j] += abs(median - houses[t])

        @lru_cache(None)
        def dp(k, i):
            if k == 0 and i == n: return 0
            if k == 0 or i == n: return math.inf
            ans = math.inf
            for j in range(i, n):
                cost = costs[i][j]  # Try to put a mailbox among house[i:j]
                ans = min(ans, cost + dp(k - 1, j + 1))
            return ans

        return dp(k, 0)
```

**Java ~ 12ms**
```java
class Solution {
    public final int MAX = 100, INF = 100 * 10000;
    int[][] costs = new int[MAX][MAX];
    Integer[][] memo = new Integer[MAX][MAX];
    public int minDistance(int[] houses, int k) {
        int n = houses.length;
        Arrays.sort(houses);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                for (int t = i; t <= j; t++)
                    costs[i][j] += Math.abs(houses[(i + j) / 2] - houses[t]);
        return dp(houses, n, k, 0);
    }
    int dp(int[] houses, int n, int k, int i) {
        if (k == 0 && i == n) return 0;
        if (k == 0 || i == n) return INF;
        if (memo[k][i] != null) return memo[k][i];
        int ans = INF;
        for (int j = i; j < n; j++)
            ans = Math.min(ans, costs[i][j] + dp(houses, n, k-1, j + 1));
        return memo[k][i] = ans;
    }
}
```

**C++ ~ 24ms**
```c++
#define MAX 100
#define INF 1000000
class Solution {
public:
    int costs[MAX][MAX] = {};
    int memo[MAX][MAX] = {};
    int minDistance(vector<int>& houses, int k) {
        int n = houses.size();
        sort(houses.begin(), houses.end());
        memset(memo, -1, sizeof(memo));
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                for (int t = i; t <= j; t++)
                    costs[i][j] += abs(houses[(i + j) / 2] - houses[t]);
        return dp(houses, n, k, 0);
    }
    int dp(vector<int>& houses, int n, int k, int i) {
        if (k == 0 && i == n) return 0;
        if (k == 0 || i == n) return INF;
        if (memo[k][i] != -1) return memo[k][i];
		int ans = INF;
        for (int j = i; j < n; j++)
            ans = min(ans, costs[i][j] + dp(houses, n, k-1, j + 1));
        return memo[k][i] = ans;
    }
};
```

</p>


### Java | Heavily Commented | Proof on why median work
- Author: khaufnak
- Creation Date: Sun Jun 14 2020 00:00:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 18 2020 20:58:38 GMT+0800 (Singapore Standard Time)

<p>
Core idea: Best way to place 1 mailbox for a set of houses is to place at the house that sits in the middle.

Why?

>Let\'s assume there are r houses to right and l houses to the left of the place where we place mailbox. Now, If you don\'t place the mailbox in the middle house but to the left of the middle(i.e. r > l). Moving one place to right will reduce distances by r and increase distance by l which will be an improvement since r > l. Hence, it\'s best we keep moving towards middle house.

For example: [6, 8, 9, 11, 15] and you have 1 mailbox. 

Now if you place mailbox at 6: Cost = (8-6) + (9-6) + (11-6) + (15-6)                                 ====> 19
Now if you place mailbox at 7: Cost = (7-6) + (8-7) + (9-7) + (11-7) + (15-7)                    ====> 16
Now if you place mailbox at 8: Cost = (8-6)  + (9-8) + (11-8) + (15-8)                                ====> 13
Now if you place mailbox at 9: Cost = (9-6)  + (9-8) + (11-9) + (15-9)                                ====> 12   <---------- Middle house has lowest
Now if you place mailbox at 10: Cost = (10-6)  + (10-8) + (10-9) + (11-10) + (15-10)    ====> 13
Now if you place mailbox at 11: Cost = (11-6)  + (11-8) + (11-9) + (15-11)                      ====> 14
Now if you place mailbox at 15: Cost = (15-6)  + (15-8) + (15-9) + (15-11)                      ====> 26

9 has the lowest cost.

Now, we can recursively solve this:

At step 1: Try to place 1st mailbox in (1, 2, 3, 4 .. n) houses. After placing at the ith point, move to step 2
At step 2: Try to place 2nd mailbox in (i+1, i+2, ... n) houses.

At step k: Try to place kth mailbox remaining houses.

Possible common mistake: We will need to sort the houses since they are not given sorted in input.


```
class Solution {
    int MAX = 10000000;

    public int minDistance(int[] houses, int k) {
        int n = houses.length;
        Arrays.sort(houses);
        int[][] dp = new int[n][k];
        for (int i = 0; i < n; ++i) Arrays.fill(dp[i], -1);
        return solve(houses, k, 0, 0, dp);
    }

    public int solve(int[] houses, int k, int pos, int curK, int[][] dp) {
        if (pos == houses.length) {
            if (curK == k) {
                return 0;
            }
            return MAX;
        }
        if (curK == k) return MAX;
        if (dp[pos][curK] != -1) return dp[pos][curK];

        int answer = MAX;
        for (int i = pos; i < houses.length; ++i) {
            // Best way to place a mailbox between [i, pos] houses is to place at the median house
            int median = houses[(i + pos) / 2];

            // Step 1: Calculate cost when we place at median house
            int cost = 0;
            for (int j = pos; j <= i; ++j) {
                cost += Math.abs(median - houses[j]);
            }

            // Step 2: Recursively, calculate cost of placing rest of the mailboxes at i+1 pos
            answer = Math.min(answer, solve(houses, k, i + 1, curK + 1, dp) + cost);
        }

        dp[pos][curK] = answer;
        return answer;
    }
}
```


Complexity: There are n * k DP states with each performing n * n operations. ```O(k * n^3)``` 

Note, we can pre-compute cost for all range of houses beforehand and therefore, reducing the complexity:

```
class Solution {
    int MAX = 10000000;

    public int minDistance(int[] houses, int k) {
        int n = houses.length;
        Arrays.sort(houses);
        int[][] dp = new int[n][k];
        int[][] cost = new int[n][n];
		
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                int median = houses[(i + j) / 2];
                int sum = 0;
                for (int l = i; l <= j; ++l) {
                    sum += Math.abs(median - houses[l]);
                }
                cost[i][j] = sum;
            }
        }
        for (int i = 0; i < n; ++i) Arrays.fill(dp[i], -1);
        return solve(houses, k, 0, 0, dp, cost);
    }

    public int solve(int[] houses, int k, int pos, int curK, int[][] dp, int[][] cost) {
        if (pos == houses.length) {
            if (curK == k) {
                return 0;
            }
            return MAX;
        }
        if (curK == k) return MAX;
        if (dp[pos][curK] != -1) return dp[pos][curK];

        int answer = MAX;
        for (int i = pos; i < houses.length; ++i) {
            answer = Math.min(answer, solve(houses, k, i + 1, curK + 1, dp, cost) + cost[pos][i]);
        }

        dp[pos][curK] = answer;
        return answer;
    }
}
```

Complexity: There are n * k DP states with each performing n operations. ```O(n^3 + k * n^2)```
</p>


### [Java/C++/Python] DP Solution
- Author: lee215
- Creation Date: Sun Jun 14 2020 00:09:03 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 17 2020 14:23:31 GMT+0800 (Singapore Standard Time)

<p>
# Explantion
`dp[i]` will means that,
the minimum distance of i + 1 first house.

`B[i] = A[0] + A[1] + A[2] + .. + A[i-1]`
`cal(i, j)` will return the minimum distance,
between `A[i]~A[j]` with only one mailbox.

Initialy, when `k = 1`, `dp[i] = cal(0, i)`
when we increment `k`, we can update `dp` with one more mailbox added.

Here we brute force the number of houses that new mailbox in charge.
The brute force here are good enough to get accepted.
<br>

# Explantion for the cost
What and why is `last = (B[j + 1] - B[m2]) - (B[m1 + 1] - B[i + 1]);`

All from @caohuicn:

First of all,
last is to calculate distances for houses [i + 1, j] with 1 mailbox,
since the dp recurrence relation is:
`dp[j][k] = min(dp[i][k-1] + cal[i + 1, j])`
(second dimension is implicit in the code);

For houses `[i + 1, j]`,
if the number of houses is odd,
the only mailbox should always be placed in the middle house.
If number of house is even, it can be placed anywhere
between the middle 2 houses.

Let\'s say we always put it at m1;
Now let\'s see the meaning of m1 and m2. For even houses,
m1 + 1 == m2, for odd houses, m1 == m2.
The point of introducing 2 variables is to 
make sure number of houses between [i+1, m1] and [m2,j] are always equal.
`(B[j + 1] - B[m2])` means `A[m2] + A[m2+1] + ... + A[j]`,
`(B[m1 + 1] - B[i + 1]`) means `A[i+1] + A[i+2] + ... + A[m1]`,
so last becomes `A[j] - A[i+1] + A[j-1] - A[i+2] +... + A[m2] - A[m1]`.

We can interpret it as:
if the mailbox is placed between any 2 houses x and y,
the sum of distances for both houses will be A[y] - A[x].
Say we have 2n houses, then there will be n pairs;
if we have 2n + 1 houses, then there will n + 1 pairs.
Another way to interpret it is: 
if the mailbox is placed at m1, 
for all the right side houses, 
the sum of distances will be 
`A[m2]-A[m1] + A[m2+1]-A[m1] + ... + A[j]-A[m1]`, 
and for the left side (including m1), 
it\'ll be `A[m1]-A[i+1]+A[m1]-A[i+2]+...+A[m1]-A[m1-1] + A[m1]-A[m1]`. 

Adding these 2 things together, 
A[m1]s will be cancelled out, 
since number of houses between [i+1, m1] and [m2,j] are always equal.

Hope it helps.



# Complexity
Time `O(KNN)`
Space `O(N)`

Note that solution `O(KN)` is also possible to come up with.
<br>

**Java**
```java
    public int minDistance(int[] A, int K) {
        Arrays.sort(A);
        int n = A.length, B[] = new int[n+1], dp[] = new int[n];
        for (int i = 0; i < n; ++i) {
            B[i + 1] = B[i] + A[i];
            dp[i] = (int)1e6;
        }
        for (int k = 1; k <= K; ++k) {
            for (int j = n - 1; j > k - 2; --j) {
                for (int i = k - 2; i < j; ++i) {
                    int m1 =  (i + j + 1) / 2, m2 = (i + j + 2) / 2;
                    int last = (B[j + 1] - B[m2]) - (B[m1 + 1] - B[i + 1]);
                    dp[j] = Math.min(dp[j], (i >= 0 ? dp[i] : 0) + last);
                }
            }
        }
        return dp[n - 1];
    }
```
**C++**
```cpp
    int minDistance(vector<int>& A, int K) {
        sort(A.begin(), A.end());
        int n = A.size();
        vector<int> B(n + 1, 0), dp(n, 1e6);
        for (int i = 0; i < n; ++i)
            B[i + 1] = B[i] + A[i];
        for (int k = 1; k <= K; ++k)
            for (int j = n - 1; j > k - 2; --j)
                for (int i = k - 2; i < j; ++i) {
                    int m1 =  (i + j + 1) / 2, m2 = (i + j + 2) / 2;
                    int last = (B[j + 1] - B[m2]) - (B[m1 + 1] - B[i + 1]);
                    dp[j] = min(dp[j], (i >= 0 ? dp[i] : 0) + last);
                }
        return dp[n - 1];
    }
```
**Python:**
```py
    def minDistance(self, A, k):
        A.sort()
        n = len(A)
        B = [0]
        for i, a in enumerate(A):
            B.append(B[i] + a)

        def cal(i, j):
            m1, m2 = (i + j) / 2, (i + j + 1) / 2
            return (B[j + 1] - B[m2]) - (B[m1 + 1] - B[i])

        dp = [cal(0, j) for j in xrange(n)]
        for k in xrange(2, k + 1):
            for j in xrange(n - 1, k - 2, -1):
                for i in xrange(k - 2, j):
                    dp[j] = min(dp[j], dp[i] + cal(i + 1, j))
        return int(dp[-1])
```
<br>

</p>


