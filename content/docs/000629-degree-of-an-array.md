---
title: "Degree of an Array"
weight: 629
#id: "degree-of-an-array"
---
## Description
<div class="description">
<p>Given a non-empty array of non-negative integers <code>nums</code>, the <b>degree</b> of this array is defined as the maximum frequency of any one of its elements.</p>

<p>Your task is to find the smallest possible length of a (contiguous) subarray of <code>nums</code>, that has the same degree as <code>nums</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,2,3,1]
<strong>Output:</strong> 2
<strong>Explanation:</strong> 
The input array has a degree of 2 because both elements 1 and 2 appear twice.
Of the subarrays that have the same degree:
[1, 2, 2, 3, 1], [1, 2, 2, 3], [2, 2, 3, 1], [1, 2, 2], [2, 2, 3], [2, 2]
The shortest length is 2. So return 2.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,2,3,1,4,2]
<strong>Output:</strong> 6
<strong>Explanation:</strong> 
The degree is 3 because the element 2 is repeated 3 times.
So [2,2,3,1,4,2] is the shortest subarray, therefore returning 6.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>nums.length</code> will be between 1 and 50,000.</li>
	<li><code>nums[i]</code> will be an integer between 0 and 49,999.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Walmart Labs - 3 (taggedByAdmin: false)
- VMware - 3 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)
- Citrix - 4 (taggedByAdmin: false)
- IXL - 3 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Robinhood - 2 (taggedByAdmin: false)
- GE Digital - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Left and Right Index [Accepted]

**Intuition and Algorithm**

An array that has degree `d`, must have some element `x` occur `d` times.  If some subarray has the same degree, then some element `x` (that occured `d` times), still occurs `d` times.  The shortest such subarray would be from the first occurrence of `x` until the last occurrence.

For each element in the given array, let's know `left`, the index of its first occurrence; and `right`, the index of its last occurrence.  For example, with `nums = [1,2,3,2,5]` we have `left[2] = 1` and `right[2] = 3`.

Then, for each element `x` that occurs the maximum number of times, `right[x] - left[x] + 1` will be our candidate answer, and we'll take the minimum of those candidates.

**Python**
```python
class Solution(object):
    def findShortestSubArray(self, nums):
        left, right, count = {}, {}, {}
        for i, x in enumerate(nums):
            if x not in left: left[x] = i
            right[x] = i
            count[x] = count.get(x, 0) + 1

        ans = len(nums)
        degree = max(count.values())
        for x in count:
            if count[x] == degree:
                ans = min(ans, right[x] - left[x] + 1)

        return ans
```

**Java**
```java
class Solution {
    public int findShortestSubArray(int[] nums) {
        Map<Integer, Integer> left = new HashMap(),
            right = new HashMap(), count = new HashMap();

        for (int i = 0; i < nums.length; i++) {
            int x = nums[i];
            if (left.get(x) == null) left.put(x, i);
            right.put(x, i);
            count.put(x, count.getOrDefault(x, 0) + 1);
        }

        int ans = nums.length;
        int degree = Collections.max(count.values());
        for (int x: count.keySet()) {
            if (count.get(x) == degree) {
                ans = Math.min(ans, right.get(x) - left.get(x) + 1);
            }
        }
        return ans;
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `nums`.  Every loop is through $$O(N)$$ items with $$O(1)$$ work inside the for-block.

* Space Complexity: $$O(N)$$, the space used by `left`, `right`, and `count`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] One Pass Solution
- Author: lee215
- Creation Date: Wed Apr 18 2018 04:25:24 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 18 2019 23:10:40 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**:
One pass on `A`,
For each different number `a` in `A`,
we need to count its frequency and it first occurrence index.

If `a` has the maximum frequency,
update the `degree = count[a]` and `res = i - first[A[i]] + 1`.

If `a` is one of the numbers that has the maximum frequency,
update the `res = min(res, i - first[A[i]] + 1)`
<br>

## **Time Complexity**:
Time `O(N)`, hardly find the any reason to scan twice.
Space `O(M)`, where M is the size of different numbers.
<br>



**Java:**
```java
    public int findShortestSubArray(int[] A) {
        Map<Integer, Integer> count = new HashMap<>(), first = new HashMap<>();
        int res = 0, degree = 0;
        for (int i = 0; i < A.length; ++i) {
            first.putIfAbsent(A[i], i);
            count.put(A[i], count.getOrDefault(A[i], 0) + 1);
            if (count.get(A[i]) > degree) {
                degree = count.get(A[i]);
                res = i - first.get(A[i]) + 1;
            } else if (count.get(A[i]) == degree)
                res = Math.min(res, i - first.get(A[i]) + 1);
        }
        return res;
    }
```

**C++:**
```cpp
    int findShortestSubArray(vector<int>& A) {
        unordered_map<int, int> count, first;
        int res = 0, degree = 0;
        for (int i = 0; i < A.size(); ++i) {
            if (first.count(A[i]) == 0) first[A[i]] = i;
            if (++count[A[i]] > degree) {
                degree = count[A[i]];
                res = i - first[A[i]] + 1;
            } else if (count[A[i]] == degree)
                res = min(res, i - first[A[i]] + 1);
        }
        return res;
    }
```

**Python:**
```python
    def findShortestSubArray(self, A):
        first, count, res, degree = {}, {}, 0, 0
        for i, a in enumerate(A):
            first.setdefault(a, i)
            count[a] = count.get(a, 0) + 1
            if count[a] > degree:
                degree = count[a]
                res = i - first[a] + 1
            elif count[a] == degree:
                res = min(res, i - first[a] + 1)
        return res
```

</p>


### The example for this question is poor
- Author: bomv
- Creation Date: Fri Oct 20 2017 11:17:16 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 01:49:05 GMT+0800 (Singapore Standard Time)

<p>
The wording of the example\'s explanation does not make it clear if 2 is being returned because it is highest degree, what number has the highest degree, or "length of shortest contiguous subarray that has the same highest degree of the original array"

Now that I understand the problem it makes sense, but I definitely think a better example could have been chosen.

Also, the title has almost nothing to do the with the question, as evidenced by the fact that the default function name is far from what the title is.

EDIT: Clearer example:
```
Input: [1, 7, 7, 5, 7, 1]
Output: 4
```
Explanation: The degree of this array is 3 because 7 appears the most of any number. The shortest subarray that you can make that has a degree of 3 is [7,7,5,7]. The length of that subarray is 4, so we return 4.
</p>


### Easy understand Java Solution (Beats 100% solutions)
- Author: eatcode
- Creation Date: Thu Oct 19 2017 07:25:33 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 00:30:59 GMT+0800 (Singapore Standard Time)

<p>
```
 public int findShortestSubArray(int[] nums) {
        if (nums.length == 0 || nums == null) return 0;
        Map<Integer, int[]> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++){
           if (!map.containsKey(nums[i])){
               map.put(nums[i], new int[]{1, i, i});  // the first element in array is degree, second is first index of this key, third is last index of this key
           } else {
               int[] temp = map.get(nums[i]);
               temp[0]++;
               temp[2] = i;
           }
        }
        int degree = Integer.MIN_VALUE, res = Integer.MAX_VALUE;
        for (int[] value : map.values()){
            if (value[0] > degree){
                degree = value[0];
                res = value[2] - value[1] + 1;
            } else if (value[0] == degree){
                res = Math.min( value[2] - value[1] + 1, res);
            } 
        }
        return res;
    }
````
</p>


