---
title: "Occurrences After Bigram"
weight: 1058
#id: "occurrences-after-bigram"
---
## Description
<div class="description">
<p>Given words <code>first</code> and <code>second</code>, consider occurrences in some&nbsp;<code>text</code> of the form &quot;<code>first second third</code>&quot;, where <code>second</code> comes immediately after <code>first</code>, and <code>third</code> comes immediately after <code>second</code>.</p>

<p>For each such occurrence, add &quot;<code>third</code>&quot; to the answer, and return the answer.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>text = <span id="example-input-1-1">&quot;alice is a good girl she is a good student&quot;</span>, first = <span id="example-input-1-2">&quot;a&quot;</span>, second = <span id="example-input-1-3">&quot;good&quot;</span>
<strong>Output: </strong><span id="example-output-1">[&quot;girl&quot;,&quot;student&quot;]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>text = <span id="example-input-2-1">&quot;we will we will rock you&quot;</span>, first = <span id="example-input-2-2">&quot;we&quot;</span>, second = <span id="example-input-2-3">&quot;will&quot;</span>
<strong>Output: </strong><span id="example-output-2">[&quot;we&quot;,&quot;rock&quot;]</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= text.length &lt;= 1000</code></li>
	<li><code>text</code> consists of space separated words, where each word consists of lowercase English letters.</li>
	<li><code>1 &lt;= first.length, second.length &lt;= 10</code></li>
	<li><code>first</code> and <code>second</code> consist of lowercase English letters.</li>
</ol>
</div>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ stringstream
- Author: lzl124631x
- Creation Date: Sun Jun 09 2019 13:57:58 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 10 2019 02:46:04 GMT+0800 (Singapore Standard Time)

<p>
See more code in my repo [LeetCode](https://github.com/lzl124631x/LeetCode)

```cpp
// OJ: https://leetcode.com/problems/occurrences-after-bigram/
// Author: github.com/lzl124631x
// Time: O(N)
// Space: O(N)
class Solution {
public:
    vector<string> findOcurrences(string text, string first, string second) {
        vector<string> ans;
        istringstream ss(text);
        string prev2, prev, word;
        while (ss >> word) {
            if (prev2 == first && prev == second) ans.push_back(word);
            prev2 = prev;
            prev = word;
        }
        return ans;
    }
};
```
</p>


### [Java/Python 3] Split String.
- Author: rock
- Creation Date: Sun Jun 09 2019 12:06:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 15 2020 02:16:37 GMT+0800 (Singapore Standard Time)

<p>
**Method 1: Split String**

Split the `text` into `words` array, then loop through it to check if previous two words are `first` and `second`; If yes, add current word into list.

```java
    public String[] findOcurrences(String text, String first, String second) {
        String[] words = text.split(" ");
        List<String> ans = new ArrayList<>();
        for (int i = 2; i < words.length; ++i) {
            if (first.equals(words[i - 2]) && second.equals(words[i - 1]))
                ans.add(words[i]);
        }
        return ans.toArray(new String[0]);
    }
```
```python
    def findOcurrences(self, text: str, first: str, second: str) -> List[str]:
        ans = []
        words = text.split(\' \')
        for i in range(2, len(words)):
            if words[i - 2] == first and words[i - 1] == second:
                ans.append(words[i])
        return ans
```

----
**Method 2:** bruteforce.

1. Locate 1st and 2nd words, then look for the 3rd;
2. start from the 2nd word, repeat the above 1. till no result exists.

```
    public String[] findOcurrences(String text, String first, String second) {
        List<String> ans = new ArrayList<>();
        int i = 0;
        while(i >= 0) {
            String two = first + \' \' + second + \' \';
            int idx = text.indexOf(two, i);
            int end = idx + two.length(), start = end;
            while (end < text.length() && text.charAt(end) != \' \') 
                ++end;
            if (idx >= 0 && start < text.length()) {
                if (idx == 0 || text.charAt(idx - 1) == \' \') // credit to @wang2019 for finding a bug.
                    ans.add(text.substring(start, end));
            }else 
                break;
            i = idx + first.length() + 1;
        }
        return ans.toArray(new String[0]);
    }
```
</p>


### C++ Search for Bigram
- Author: votrubac
- Creation Date: Sun Jun 09 2019 12:39:14 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 20 2019 08:52:34 GMT+0800 (Singapore Standard Time)

<p>
First, compose our bigram as ```first + " " + second + " "```. Thanks [alreadydone](https://leetcode.com/alreadydone/) for the advice to add an extra space in the end.

Update: we search for  ```" " + first + " " + second + " "```, and add an extra space in the front of the original text. So we won\'t match ```"Alice jumped"``` to ```"lice jumped"```. Thanks [smartroy](https://leetcode.com/smartroy) for the find.

Then search for all bigram occurrences, extracting the word that follows.
```
vector<string> findOcurrences(string raw_text, string first, string second) {
  vector<string> res;
  auto bigram = " " + first + " " + second + " ", text = " " + raw_text;
  auto p = text.find(bigram);
  while (p != string::npos) {
    auto p1 = p + bigram.size(), p2 = p1;
    while (p2 < text.size() && text[p2] != \' \') ++p2;
    res.push_back(text.substr(p1, p2 - p1));
    p = text.find(bigram, p + 1);
  }
  return res;
}
```
# Complexity Analysis
Runtime: *O(n + m)*, where *n* is the size of the string, and *m* - size of the bigram. 
> A single find operation is *O(n + m)*; we can search several times but we move forward and don\'t consider the part we searched already.
> 
> Even if our string is ```"a a a a ..."```, and bigram - ```"a a "```, the total number of operation will not exceed *2n*.  

Memory: *O(m)* to store the bigram, or *O(n)* if we consider the memory for the result.
</p>


