---
title: "Powerful Integers"
weight: 920
#id: "powerful-integers"
---
## Description
<div class="description">
<p>Given two positive integers <code>x</code> and <code>y</code>, an integer is <em>powerful</em>&nbsp;if it is equal to <code>x^i + y^j</code>&nbsp;for&nbsp;some integers <code>i &gt;= 0</code> and <code>j &gt;= 0</code>.</p>

<p>Return a list of all <em>powerful</em> integers that have value less than or equal to <code>bound</code>.</p>

<p>You may return the answer in any order.&nbsp; In your answer, each value should occur at most once.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>x = <span id="example-input-1-1">2</span>, y = <span id="example-input-1-2">3</span>, bound = <span id="example-input-1-3">10</span>
<strong>Output: </strong><span id="example-output-1">[2,3,4,5,7,9,10]</span>
<strong>Explanation: </strong>
2 = 2^0 + 3^0
3 = 2^1 + 3^0
4 = 2^0 + 3^1
5 = 2^1 + 3^1
7 = 2^2 + 3^1
9 = 2^3 + 3^0
10 = 2^0 + 3^2
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>x = <span id="example-input-2-1">3</span>, y = <span id="example-input-2-2">5</span>, bound = <span id="example-input-2-3">15</span>
<strong>Output: </strong><span id="example-output-2">[2,4,6,8,10,14]</span>
</pre>
</div>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>1 &lt;= x &lt;= 100</code></li>
	<li><code>1 &lt;= y&nbsp;&lt;= 100</code></li>
	<li><code>0 &lt;= bound&nbsp;&lt;= 10^6</code></li>
</ul>
</div>

## Tags
- Hash Table (hash-table)
- Math (math)

## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach 1: Brute Force

**Intuition**

If $$x^i > \text{bound}$$, the sum $$x^i + y^j$$ can't be less than or equal to the bound.  Similarly for $$y^j$$.

Thus, we only have to check for $$0 \leq i, j \leq \log_x(\text{bound}) < 18$$.

We can use a `HashSet` to store all the different values.

<iframe src="https://leetcode.com/playground/hdhEnDAy/shared" frameBorder="0" width="100%" height="276" name="hdhEnDAy"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\log^2{\text{bound}})$$.

* Space Complexity:  $$O(\log^2{\text{bound}})$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java straightforward try all combinations
- Author: wangzi6147
- Creation Date: Sun Jan 06 2019 12:03:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 06 2019 12:03:01 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public List<Integer> powerfulIntegers(int x, int y, int bound) {
        Set<Integer> result = new HashSet<>();
        for (int a = 1; a < bound; a *= x) {
            for (int b = 1; a + b <= bound; b *= y) {
                result.add(a + b);
                if (y == 1) {
                    break;
                }
            }
            if (x == 1) {
                break;
            }
        }
        return new ArrayList<>(result);
    }
}
```
</p>


### [Java/C++/Python] Easy Brute Force
- Author: lee215
- Creation Date: Sun Jan 06 2019 12:08:57 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 10 2019 23:16:41 GMT+0800 (Singapore Standard Time)

<p>
**Java:**
```java
    public List<Integer> powerfulIntegers(int x, int y, int bound) {
        Set<Integer> s = new HashSet<>();
        for (int i = 1; i < bound; i *= x) {
            for (int j = 1; i + j <= bound; j *= y) {
                s.add(i + j);
                if (y == 1) break;
            }
            if (x == 1) break;
        }
        return new ArrayList<>(s);
    }
```

**C++:**
```cpp
    vector<int> powerfulIntegers(int x, int y, int bound) {
        unordered_set<int> s;
        for (int i = 1; i <= bound; i *= x) {
            for (int j = 1; i + j <= bound; j *= y) {
                s.insert(i + j);
                if (y == 1) break;
            }
            if (x == 1) break;

        }
        return vector<int>(s.begin(), s.end());
    }
```

**Python:**
```python
    def powerfulIntegers(self, x, y, bound):
        xs = {x**i for i in range(20) if x**i < bound}
        ys = {y**i for i in range(20) if y**i < bound}
        return list({i + j for i in xs for j in ys if i + j <= bound})
```

</p>


### Python DFS solution
- Author: motal
- Creation Date: Thu Jan 10 2019 07:29:45 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 10 2019 07:29:45 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def powerfulIntegers(self, x, y, bound):
        """
        :type x: int
        :type y: int
        :type bound: int
        :rtype: List[int]
        """
        s = set()
        stack = [(0, 0)]
        while stack:
            i, j = stack.pop()
            t = x ** i + y ** j
            if t <= bound:
                s.add(t)
                if x > 1:
                    stack.append((i+1, j))
                if y > 1:
                    stack.append((i, j+1))
        
        return list(s)

</p>


