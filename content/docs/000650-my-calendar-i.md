---
title: "My Calendar I"
weight: 650
#id: "my-calendar-i"
---
## Description
<div class="description">
<p>Implement a <code>MyCalendar</code> class to store your events. A new event can be added if adding the event will not cause a double booking.</p>

<p>Your class will have the method, <code>book(int start, int end)</code>. Formally, this represents a booking on the half open interval <code>[start, end)</code>, the range of real numbers <code>x</code> such that <code>start &lt;= x &lt; end</code>.</p>

<p>A <i>double booking</i> happens when two events have some non-empty intersection (ie., there is some time that is common to both events.)</p>

<p>For each call to the method <code>MyCalendar.book</code>, return <code>true</code> if the event can be added to the calendar successfully without causing a double booking. Otherwise, return <code>false</code> and do not add the event to the calendar.</p>
Your class will be called like this: <code>MyCalendar cal = new MyCalendar();</code> <code>MyCalendar.book(start, end)</code>

<p><b>Example 1:</b></p>

<pre>
MyCalendar();
MyCalendar.book(10, 20); // returns true
MyCalendar.book(15, 25); // returns false
MyCalendar.book(20, 30); // returns true
<b>Explanation:</b> 
The first event can be booked.  The second can&#39;t because time 15 is already booked by another event.
The third event can be booked, as the first event takes every time less than 20, but not including 20.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ul>
	<li>The number of calls to <code>MyCalendar.book</code> per test case will be at most <code>1000</code>.</li>
	<li>In calls to <code>MyCalendar.book(start, end)</code>, <code>start</code> and <code>end</code> are integers in the range <code>[0, 10^9]</code>.</li>
</ul>

<p>&nbsp;</p>

</div>

## Tags
- Array (array)

## Companies
- Google - 6 (taggedByAdmin: true)
- Qualtrics - 5 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Intuit - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Brute Force [Accepted]

**Intuition**

When booking a new event `[start, end)`, check if every current event conflicts with the new event.  If none of them do, we can book the event.

**Algorithm**

We will maintain a list of interval *events* (not necessarily sorted).  Evidently, two events `[s1, e1)` and `[s2, e2)` do *not* conflict if and only if one of them starts after the other one ends: either `e1 <= s2` OR `e2 <= s1`.  By De Morgan's laws, this means the events conflict when `s1 < e2` AND `s2 < e1`.

<iframe src="https://leetcode.com/playground/VrfQDFXC/shared" frameBorder="0" width="100%" height="310" name="VrfQDFXC"></iframe>


**Complexity Analysis**

* Time Complexity: $$O(N^2)$$, where $$N$$ is the number of events booked.  For each new event, we process every previous event to decide whether the new event can be booked.  This leads to $$\sum_k^N O(k) = O(N^2)$$ complexity.

* Space Complexity: $$O(N)$$, the size of the `calendar`.

---
#### Approach #2: Balanced Tree [Accepted]

**Intuition**

If we maintained our events in *sorted* order, we could check whether an event could be booked in $$O(\log N)$$ time (where $$N$$ is the number of events already booked) by binary searching for where the event should be placed.  We would also have to insert the event in our sorted structure.

**Algorithm**

We need a data structure that keeps elements sorted and supports fast insertion.  In Java, a `TreeMap` is the perfect candidate.  In Python, we can build our own binary tree structure.

For Java, we will have a `TreeMap` where the keys are the start of each interval, and the values are the ends of those intervals.  When inserting the interval `[start, end)`, we check if there is a conflict on each side with neighboring intervals: we would like `calendar.get(prev)) <= start <= end <= next` for the booking to be valid (or for `prev` or `next` to be null respectively.)

For Python, we will create a binary tree.  Each node represents some interval `[self.start, self.end)` while `self.left, self.right` represents nodes that are smaller or larger than the current node.

<iframe src="https://leetcode.com/playground/fkctVGim/shared" frameBorder="0" width="100%" height="500" name="fkctVGim"></iframe>

**Complexity Analysis**

* Time Complexity (Java): $$O(N \log N)$$, where $$N$$ is the number of events booked.  For each new event, we search that the event is legal in $$O(\log N)$$ time, then insert it in $$O(1)$$ time.

* Time Complexity (Python): $$O(N^2)$$ worst case, with $$O(N \log N)$$ on random data.  For each new event, we insert the event into our binary tree.  As this tree may not be balanced, it may take a linear number of steps to add each event.

* Space Complexity: $$O(N)$$, the size of the data structures used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] Clean Code with Explanation
- Author: alexander
- Creation Date: Sun Nov 19 2017 12:00:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 06:59:13 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1: Check every existed book for overlap**
`overlap` of 2 interval `a` `b` is `(max(a0, b0), min(a1, b1))` 
detail is in: https://discuss.leetcode.com/topic/111198
**Java**
```
class MyCalendar {
    private List<int[]> books = new ArrayList<>();
    public boolean book(int start, int end) {
        for (int[] b : books)
            if (Math.max(b[0], start) < Math.min(b[1], end)) return false;
        books.add(new int[]{ start, end });
        return true;
    }
}
```
**C++**
```
class MyCalendar {
    vector<pair<int, int>> books;
public:    
    bool book(int start, int end) {
        for (pair<int, int> p : books)
            if (max(p.first, start) < min(end, p.second)) return false;
        books.push_back({start, end});
        return true;
    }
};
```

**Solution 2: Keep existing books sorted and only check 2 books start right before & after the new book starts**
Another way to check overlap of 2 intervals is `a started with b`, or, `b started within a`.

Keep the intervals sorted,
if the interval started right before the new interval contains the start, or
if the interval started right after the new interval started within the `new interval`.
```
   floor      ceiling
... |----| ... |----| ...
       |---------|
      s         e
if s < floor.end or e > ceiling.start, there is an overlap.

Another way to think of it:
If there is an interval start within the new book (must be the ceilingEntry) at all, or
books: |----|   |--|
            s |------| e

books: |----|   |----|
            s |----| e
If the new book start within an interval (must be the floorEntry) at all
books: |-------|   |--|
       s |---| e

books: |----|   |----|
        s |----| e
There is a overlap 

```
**Java**
TreeSet
```
class MyCalendar {
    TreeSet<int[]> books = new TreeSet<int[]>((int[] a, int[] b) -> a[0] - b[0]);

    public boolean book(int s, int e) {
        int[] book = new int[] { s, e }, floor = books.floor(book), ceiling = books.ceiling(book);
        if (floor != null && s < floor[1]) return false; // (s, e) start within floor
        if (ceiling != null && ceiling[0] < e) return false; // ceiling start within (s, e)
        books.add(book);
        return true;
    }
}
```
TreeMap
```
class MyCalendar {
    TreeMap<Integer, Integer> books = new TreeMap<>();

    public boolean book(int s, int e) {
        java.util.Map.Entry<Integer, Integer> floor = books.floorEntry(s), ceiling = books.ceilingEntry(s);
        if (floor != null && s < floor.getValue()) return false; // (s, e) start within floor
        if (ceiling != null && ceiling.getKey() < e) return false; // ceiling start within (s, e)
        books.put(s, e);
        return true;
    }
}
```
**C++**
ordered set
```
class MyCalendar {
    set<pair<int, int>> books;
public:
    bool book(int s, int e) {
        auto next = books.lower_bound({s, e}); // first element with key not go before k (i.e., either it is equivalent or goes after).
        if (next != books.end() && next->first < e) return false; // a existing book started within the new book (next)
        if (next != books.begin() && s < (--next)->second) return false; // new book started within a existing book (prev)
        books.insert({ s, e });
        return true;
    }
};
```
ordered map
```
class MyCalendar {
    map<int, int> books;
public:
    bool book(int s, int e) {
        auto next = books.lower_bound(s); // first element with key not go before k (i.e., either it is equivalent or goes after).
        if (next != books.end() && next->first < e) return false; // a existing book started within the new book (next)
        if (next != books.begin() && s < (--next)->second) return false; // new book started within a existing book (prev)
        books[s] = e;
        return true;
    }
};
```
</p>


### Java 8 liner, TreeMap
- Author: shawngao
- Creation Date: Sun Nov 19 2017 12:03:39 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 01:35:32 GMT+0800 (Singapore Standard Time)

<p>
```
class MyCalendar {

    TreeMap<Integer, Integer> calendar;

    public MyCalendar() {
        calendar = new TreeMap<>();
    }

    public boolean book(int start, int end) {
        Integer floorKey = calendar.floorKey(start);
        if (floorKey != null && calendar.get(floorKey) > start) return false;
        Integer ceilingKey = calendar.ceilingKey(start);
        if (ceilingKey != null && ceilingKey < end) return false;

        calendar.put(start, end);
        return true;
    }
}
```
</p>


### Binary Search Tree python
- Author: ahonarmand
- Creation Date: Sun Nov 19 2017 12:03:15 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 12:44:06 GMT+0800 (Singapore Standard Time)

<p>
```
class Node:
    def __init__(self,s,e):
        self.e = e
        self.s = s
        self.left = None
        self.right = None


class MyCalendar(object):

    def __init__(self):
        self.root = None

    def book_helper(self,s,e,node):
        if s>=node.e:
            if node.right:
                return self.book_helper(s,e,node.right)
            else:
                node.right = Node(s,e)
                return True
        elif e<=node.s:
            if node.left:
                return self.book_helper(s,e,node.left)
            else:
                node.left = Node(s,e)
                return True
        else:
            return False
        
    def book(self, start, end):
        """
        :type start: int
        :type end: int
        :rtype: bool
        """
        if not self.root:
            self.root = Node(start,end)
            return True
        return self.book_helper(start,end,self.root)
        
        
        ```
</p>


