---
title: "Shortest Common Supersequence "
weight: 1068
#id: "shortest-common-supersequence"
---
## Description
<div class="description">
<p>Given two strings <code>str1</code> and <code>str2</code>,&nbsp;return the shortest string that has both <code>str1</code>&nbsp;and <code>str2</code>&nbsp;as subsequences.&nbsp;&nbsp;If multiple answers exist, you may return any of them.</p>

<p><em>(A string S is a subsequence of string T if deleting some number of characters from T (possibly 0, and the characters are chosen <u>anywhere</u> from T) results in the string S.)</em></p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>str1 = <span id="example-input-1-1">&quot;abac&quot;</span>, str2 = <span id="example-input-1-2">&quot;cab&quot;</span>
<strong>Output: </strong><span id="example-output-1">&quot;cabac&quot;</span>
<strong>Explanation: </strong>
str1 = &quot;abac&quot; is a subsequence of &quot;cabac&quot; because we can delete the first &quot;c&quot;.
str2 = &quot;cab&quot; is a subsequence of &quot;cabac&quot; because we can delete the last &quot;ac&quot;.
The answer provided is the shortest such string that satisfies these properties.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= str1.length, str2.length &lt;= 1000</code></li>
	<li><code>str1</code> and <code>str2</code> consist of lowercase English letters.</li>
</ol>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Python] Find the LCS
- Author: lee215
- Creation Date: Sun Jun 16 2019 12:04:16 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 13 2020 00:24:55 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Change the problem to find the LCS
<br>

## **Complexity**
TIme O(MN) dp, 
Space O(MN) * O(string), but actually we can also save the storage of string.
<br>

**C++:**
```
    string shortestCommonSupersequence(string& A, string& B) {
        int i = 0, j = 0;
        string res = "";
        for (char c : lcs(A, B)) {
            while (A[i] != c)
                res += A[i++];
            while (B[j] != c)
                res += B[j++];
            res += c, i++, j++;
        }
        return res + A.substr(i) + B.substr(j);
    }

    string lcs(string& A, string& B) {
        int n = A.size(), m = B.size();
        vector<vector<string>> dp(n + 1, vector<string>(m + 1, ""));
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j)
                if (A[i] == B[j])
                    dp[i + 1][j + 1] = dp[i][j] + A[i];
                else
                    dp[i + 1][j + 1] = dp[i + 1][j].size() > dp[i][j + 1].size() ?  dp[i + 1][j] : dp[i][j + 1];
        return dp[n][m];
    }
```

**Python:**
```
    def shortestCommonSupersequence(self, A, B):
        def lcs(A, B):
            n, m = len(A), len(B)
            dp = [["" for _ in xrange(m + 1)] for _ in range(n + 1)]
            for i in range(n):
                for j in range(m):
                    if A[i] == B[j]:
                        dp[i + 1][j + 1] = dp[i][j] + A[i]
                    else:
                        dp[i + 1][j + 1] = max(dp[i + 1][j], dp[i][j + 1], key=len)
            return dp[-1][-1]

        res, i, j = "", 0, 0
        for c in lcs(A, B):
            while A[i] != c:
                res += A[i]
                i += 1
            while B[j] != c:
                res += B[j]
                j += 1
            res += c
            i, j = i + 1, j + 1
        return res + A[i:] + B[j:]
```

</p>


### Java DP Solution(Similiar to LCS)
- Author: Poorvank
- Creation Date: Sun Jun 16 2019 12:03:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 16 2019 12:03:08 GMT+0800 (Singapore Standard Time)

<p>

DP Recurrence:

Let str1[0..m - 1] and str2[0..n - 1] be two strings with lengths m and n .

  if (m == 0) return n;
  if (n == 0) return m;

  // If last characters are same, then add 1 to result and recur for str1[]
  if (str1.charAt(m - 1) == str2.charAt(n - 1))
     return 1 + shortestCommonSupersequence(str1, str2, m - 1, n - 1);

  // Else find shortest of following two
  //  a) Remove last character from str1 and recur
  //  b) Remove last character from str2 and recur
  else return 1 + min( shortestCommonSupersequence(str1, str2, m - 1, n), shortestCommonSupersequence(str1, str2, m, n - 1) );
```
public String shortestCommonSupersequence(String str1, String str2) {
        int m = str1.length();
        int n = str2.length();

        int[][] dp = new int[m + 1][n + 1];

        for (int i = 0; i <= m; i++)
        {
            for (int j = 0; j <= n; j++)
            {
                if (i == 0)
                    dp[i][j] = j;
                else if (j == 0)
                    dp[i][j] = i;
                else if (str1.charAt(i - 1) == str2.charAt(j - 1))
                    dp[i][j] = 1 + dp[i - 1][j - 1];
                else
                    dp[i][j] = 1 + Math.min(dp[i - 1][j],
                            dp[i][j - 1]);
            }
        }

        int l = dp[m][n]; // Length of the ShortestSuperSequence
        char[] arr = new char[l];
        int i=m, j=n;
        while(i>0 && j>0)
        {
            /* If current character in str1 and str2 are same, then
             current character is part of shortest supersequence */
            if(str1.charAt(i-1) == str2.charAt(j-1)) {
                arr[--l] = str1.charAt(i-1);
                i--;j--;
            }else if(dp[i-1][j]<dp[i][j-1]) {
                arr[--l] = str1.charAt(i-1);
                i--;
            }
            else {
                arr[--l] = str2.charAt(j-1);
                j--;
            }
        }
        while (i > 0) {
            arr[--l] = str1.charAt(i-1);
            i--;
        }
        while (j > 0) {
            arr[--l] = str2.charAt(j-1);
            j--;
        }
        return new String(arr);
    }
```
</p>


### [Java/Python 3] O(mn) clean DP code w/ picture, comments and analysis.
- Author: rock
- Creation Date: Sun Jun 16 2019 12:15:12 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 19 2020 02:32:23 GMT+0800 (Singapore Standard Time)

<p>

1. Find LCS;
Let `X` be `\u201CXMJYAUZ\u201D` and `Y` be `\u201CMZJAWXU\u201D`. The longest common subsequence between `X` and `Y` is `\u201CMJAU\u201D`. The following table shows the lengths of the longest common subsequences between prefixes of `X` and `Y`. The `ith` row and `jth` column shows the length of the LCS between `X_{1..i}` and `Y_{1..j}`.
![image](https://assets.leetcode.com/users/rock/image_1568826071.png)
you can refer to [here](https://en.m.wikipedia.org/wiki/Longest_common_subsequence_problem) for more details.

2. Reversely append the chars to StringBuilder, if the char is among the LCS, choose either one between the two strings.
a) start from `i = m - 1` and `j = n - 1`, check if the corresponding chars are equal, that is, s1.charAt(i) == s2.charAt(j); if yes, append either of them; if no, append the char with larger `dp` value.
b) If we reach left end of s1 or s2 first, continue to append remaining chars in the other string.


```
    public String shortestCommonSupersequence(String s1, String s2) {

        // find LCS.
        int m = s1.length(), n = s2.length();
        int[][] dp = new int[m + 1][n + 1];
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                if (s1.charAt(i) == s2.charAt(j)) {
                    dp[i + 1][j + 1] = 1 + dp[i][j];    
                }else {
                    dp[i + 1][j + 1] = Math.max(dp[i + 1][j], dp[i][j + 1]);
                }
            }
        }
		
        // Build result.
        StringBuilder sb = new StringBuilder();
        int i = m - 1, j = n - 1;
        while (i >= 0 || j >= 0) { 
            if (i < 0 ^ j < 0) { // only one string reaches left end.
                char c = i < 0 ? s2.charAt(j--) : s1.charAt(i--); // remaining chars in the other string.
                sb.append(c);
            }else if (s1.charAt(i) == s2.charAt(j)) { // common char in LCS.
                sb.append(s1.charAt(i)); // append the char of either s1 or s2.
                --i; --j;  
            }else { // the char is not in LCS.
                char c = dp[i][j + 1] > dp[i + 1][j] ? s1.charAt(i--) : s2.charAt(j--); // the char corresponding to larger dp value.
                sb.append(c);
            }
        } 
        return sb.reverse().toString();
    }
```
```
    def shortestCommonSupersequence(self, s1: str, s2: str) -> str:
        m, n = len(s1), len(s2)
        dp = [[0] * (n + 1) for _ in range(m + 1)]
        for i, c in enumerate(s1):
            for j, d in enumerate(s2):
                dp[i + 1][j + 1] = 1 + dp[i][j] if c == d else max(dp[i + 1][j], dp[i][j + 1])
        i, j, stk = m - 1, n - 1, []
        while i >= 0 and j >= 0:
            if s1[i] == s2[j]:
                stk.append(s1[i])
                i -= 1
                j -= 1
            elif dp[i + 1][j] < dp[i][j + 1]:
                stk.append(s1[i])
                i -= 1
            else:
                stk.append(s2[j])
                j -= 1    
        return s1[: i + 1] + s2[: j + 1] + \'\'.join(reversed(stk))
```
**Analysis:**

Time & space:: O(m * n), where m = s1.length(), n = s2.length().


----

# Update:
**Q & A**

Q1: Is it possible to move backwards in the find LCS so you would not need to reverse in the second half?
A1:
I believe it is possible, but that will NOT make the code simpler, and there could be more than one solution also. 
If you hope to compute `dp` array **backward** and construct the common  supersequence **forward**, here is the code:
```
    public String shortestCommonSupersequence(String s1, String s2) {
        int m = s1.length(), n = s2.length();
        int[][] dp = new int[m + 1][n + 1];
        for (int i = m - 1; i >= 0; --i) {
            for (int j = n - 1; j >= 0; --j) {
                if (s1.charAt(i) == s2.charAt(j)) {
                    dp[i][j] = 1 + dp[i + 1][j + 1];
                }else {
                    dp[i][j] = Math.max(dp[i + 1][j], dp[i][j + 1]);
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        int i = 0, j = 0;
        while (i < m || j < n) {
            if (i < m ^ j < n) {
                sb.append(i < m ? s1.charAt(i++) : s2.charAt(j++));
            }else if (s1.charAt(i) == s2.charAt(j)) {
                sb.append(s1.charAt(i++));
                ++j;
            }else {
                sb.append(dp[i + 1][j] > dp[i][j + 1] ? s1.charAt(i++) : s2.charAt(j++));
            }
        }
        return sb.toString();
    }
```
```
    def shortestCommonSupersequence(self, s1: str, s2: str) -> str:
        m, n = len(s1), len(s2)
        dp = [[0] * (n + 1) for _ in range(m + 1)]
        for i, c in reversed(list(enumerate(s1))):
            for j, d in reversed(list(enumerate(s2))):
                dp[i][j] = 1 + dp[i + 1][j + 1] if c == d else max(dp[i + 1][j], dp[i][j + 1])
        i, j, seq = 0, 0, []
        while i < m and j < n:
            if s1[i] == s2[j]:
                seq.append(s1[i])
                i += 1
                j += 1
            elif dp[i + 1][j] > dp[i][j + 1]:
                seq.append(s1[i])
                i += 1
            else:
                seq.append(s2[j])
                j += 1    
        return \'\'.join(seq) + s1[i :] + s2[j :]
```
Q2: Could you explain the condition being described here? I understand about finishing one string and tacking on the rest of the other. I looked and saw ^ was a bitwise XOR operator, but I didn\'t understand it in this context.
```
 if (i < 0 ^ j < 0) { // only one string reaches left end.
```
A2:
Similar to bitwise `xor`, which results `1` if and only if one operand is `1` and the other is `0`: `(conditional 1) ^ (conditional 2)` is true iff one is `true` and the other is `false`. In case you are not comfortable with it, you can rewrite it as: 
```
 if (i < 0 && j >= 0 || j < 0 && i >= 0) { // only one string reaches left end.
```
Q3: In your following code, why backtracking gives us the shortest super string?
```
i, j, stk = m - 1, n - 1, []
while i >= 0 and j >= 0:
```
Why can\'t we start with `i, j = 0, 0`?

A3: If we start from `(0, 0)`, we need to search again for the LCS, which we have already found in the first half part of the code.
dp[m][n] is guaranteed to be the largest value, and by starting from its corresponding coordinate `(m - 1, n - 1)` we can follow the LCS path to make sure the chars in LCS appear only **once** not twice. If twice, that would not be the **shortest** super common sequence.

**end** of Q & A

----
</p>


