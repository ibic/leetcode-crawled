---
title: "Subarray Product Less Than K"
weight: 634
#id: "subarray-product-less-than-k"
---
## Description
<div class="description">
<p>Your are given an array of positive integers <code>nums</code>.</p>
<p>Count and print the number of (contiguous) subarrays where the product of all the elements in the subarray is less than <code>k</code>.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> nums = [10, 5, 2, 6], k = 100
<b>Output:</b> 8
<b>Explanation:</b> The 8 subarrays that have product less than 100 are: [10], [5], [2], [6], [10, 5], [5, 2], [2, 6], [5, 2, 6].
Note that [10, 5, 2] is not included as the product of 100 is not strictly less than k.
</pre>
</p>

<p><b>Note:</b>
<li><code>0 < nums.length <= 50000</code>.</li>
<li><code>0 < nums[i] < 1000</code>.</li>
<li><code>0 <= k < 10^6</code>.</li>
</p>
</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)

## Companies
- Akuna Capital - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Coursera - 8 (taggedByAdmin: false)
- Expedia - 3 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Yatra - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


#### Approach #1: Binary Search on Logarithms [Accepted]

**Intuition**

Because $$\log(\prod_i x_i) = \sum_i \log x_i$$, we can reduce the problem to subarray *sums* instead of subarray products.  The motivation for this is that the product of some arbitrary subarray may be way too large (potentially `1000^50000`), and also dealing with sums gives us some more familiarity as it becomes similar to other problems we may have solved before.

**Algorithm**

After this transformation where every value `x` becomes `log(x)`, let us take prefix sums `prefix[i+1] = nums[0] + nums[1] + ... + nums[i]`.  Now we are left with the problem of finding, for each `i`, the largest `j` so that `nums[i] + ... + nums[j] = prefix[j] - prefix[i] < k`.

Because `prefix` is a monotone increasing array, this can be solved with binary search.  We add the width of the interval `[i, j]` to our answer, which counts all subarrays `[i, k]` with `k <= j`.

**Python**
```python
class Solution(object):
    def numSubarrayProductLessThanK(self, nums, k):
        if k == 0: return 0
        k = math.log(k)

        prefix = [0]
        for x in nums:
            prefix.append(prefix[-1] + math.log(x))

        ans = 0
        for i, x in enumerate(prefix):
            j = bisect.bisect(prefix, x + k - 1e-9, i+1)
            ans += j - i - 1
        return ans
```

**Java**
```java
class Solution {
    public int numSubarrayProductLessThanK(int[] nums, int k) {
        if (k == 0) return 0;
        double logk = Math.log(k);
        double[] prefix = new double[nums.length + 1];
        for (int i = 0; i < nums.length; i++) {
            prefix[i+1] = prefix[i] + Math.log(nums[i]);
        }

        int ans = 0;
        for (int i = 0; i < prefix.length; i++) {
            int lo = i + 1, hi = prefix.length;
            while (lo < hi) {
                int mi = lo + (hi - lo) / 2;
                if (prefix[mi] < prefix[i] + logk - 1e-9) lo = mi + 1;
                else hi = mi;
            }
            ans += lo - i - 1;
        }
        return ans;
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(N\log N)$$, where $$N$$ is the length of `nums`.  Inside our for loop, each binary search operation takes $$O(\log N)$$ time.

* Space Complexity: $$O(N)$$, the space used by `prefix`.

---
#### Approach #2: Sliding Window [Accepted]

**Intuition**

For each `right`, call `opt(right)` the smallest `left` so that the product of the subarray `nums[left] * nums[left + 1] * ... * nums[right]` is less than `k`.  `opt` is a monotone increasing function, so we can use a sliding window.

**Algorithm**

Our loop invariant is that `left` is the smallest value so that the product in the window `prod = nums[left] * nums[left + 1] * ... * nums[right]` is less than `k`.

For every right, we update `left` and `prod` to maintain this invariant.  Then, the number of intervals with subarray product less than `k` and with right-most coordinate `right`, is `right - left + 1`.  We'll count all of these for each value of `right`.

**Python**
```python
class Solution(object):
    def numSubarrayProductLessThanK(self, nums, k):
        if k <= 1: return 0
        prod = 1
        ans = left = 0
        for right, val in enumerate(nums):
            prod *= val
            while prod >= k:
                prod /= nums[left]
                left += 1
            ans += right - left + 1
        return ans
```

**Java**
```java
class Solution {
    public int numSubarrayProductLessThanK(int[] nums, int k) {
        if (k <= 1) return 0;
        int prod = 1, ans = 0, left = 0;
        for (int right = 0; right < nums.length; right++) {
            prod *= nums[right];
            while (prod >= k) prod /= nums[left++];
            ans += right - left + 1;
        }
        return ans;
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `nums`.  `left` can only be incremented at most `N` times.

* Space Complexity: $$O(1)$$, the space used by `prod`, `left`, and `ans`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] Clean Code with Explanation
- Author: alexander
- Creation Date: Sun Oct 22 2017 13:31:51 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 14:23:04 GMT+0800 (Singapore Standard Time)

<p>
1. The idea is always keep an `max-product-window` less than `K`;
2. Every time shift window by adding a new number on the right(`j`), if the product is greater than k, then try to reduce numbers on the left(`i`), until the subarray product fit less than `k` again, (subarray could be empty);
3. Each step introduces `x` new subarrays, where x is the size of the current window `(j + 1 - i)`;
example:
 for window (5, 2), when 6 is introduced, it add 3 new subarray:   (5, (2, (6)))

```
        (6)
     (2, 6)
  (5, 2, 6)
```

**Java**
```
class Solution {
    public int numSubarrayProductLessThanK(int[] nums, int k) {
        if (k == 0) return 0;
        int cnt = 0;
        int pro = 1;
        for (int i = 0, j = 0; j < nums.length; j++) {
            pro *= nums[j];
            while (i <= j && pro >= k) {
                pro /= nums[i++];
            }
            cnt += j - i + 1;
        }
        return cnt;        
    }
}
```
**C++**
```
/**
 * The idea is always keep an max-product-window less than K;
 * Every time add a new number on the right(j), reduce numbers on the left(i), until the subarray product fit less than k again, (subarray could be empty);
 * Each step introduces x new subarrays, where x is the size of the current window (j + 1 - i);
 * example:
 * for window (5, 2, 6), when 6 is introduced, it add 3 new subarray:
 *       (6)
 *    (2, 6)
 * (5, 2, 6)
 */
class Solution {
public:
    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
        if (k == 0) return 0;
        int cnt = 0;
        int pro = 1;
        for (int i = 0, j = 0; j < nums.size(); j++) {
            pro *= nums[j];
            while (i <= j && pro >= k) {
                pro /= nums[i++];
            }
            cnt += j - i + 1;
        }
        return cnt;
    }
};
```
</p>


### Java Two Pointers O(n) time O(1) space
- Author: yujun
- Creation Date: Mon Oct 23 2017 00:28:41 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 00:46:30 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int numSubarrayProductLessThanK(int[] nums, int k) {
        int n = nums.length;
        long p = 1l;
        int i = 0;
        int j = 0;
        int total = 0;
        while(j < n){
            p *= nums[j];
            while(i <= j&&p >= k){
                p /= nums[i];
                i++;
            }
            total += (j - i + 1);
            j++;
        }
        return total;
    }
}
</p>


### Python solution with detailed explanation
- Author: gabbu
- Creation Date: Thu Oct 26 2017 06:16:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 05:27:40 GMT+0800 (Singapore Standard Time)

<p>
**Subarray Product Less Than K** https://leetcode.com/problems/subarray-product-less-than-k/description/

**Two Pointer Solution**
* Initialize start and end to index 0. Initialize prod to 1. Iterate end from 0 to len(nums)-1. Now if  prod \*  nums[end] is less than k, then all subarray between start and end contribute to the solution. Since we are moving from left to right, we would have already counted all valid subarrays from start to end-1. How many new subarrays with nums[end]? Answer: end-start+1. What will be the updated prod? Answer: prod * nums[end].
*  What if prod * nums[end] >= k? We need to contract the subarray by advancing start until we get a valid solution again. Now what do we do when start > end? Answer: prod=1.
*  Special case: k=0.
*  Time is O(N) and space is O(1).
*  Issue: Overflow with multiplication.
```
class Solution(object):
    def numSubarrayProductLessThanK(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        if k == 0:
            return 0
        start, prod, cnt = 0, 1, 0
        for end in range(len(nums)):
            while start <= end and prod*nums[end] >= k:
                prod = prod/nums[start]
                start += 1
            prod = 1 if start > end else prod*nums[end]
            cnt = cnt if start > end else cnt+(end-start+1)
        return cnt
```

**Transform prod to sum problem using log transform**
* Say we have a subarray [x1,x2,x3] such that its product is less than k.
* x1\*x2\*x3 < k. Take log of both sides. log(x1) + log(x2) +log(x3) < log(k).
* Lets transform original nums as nums = [log(x) for x in nums]. Next transform it into a cdf array called cums. Also, k = log(k).
* Now if a subarray from index i to index j is a solution, then we can say: cums[j]-cums[i]+nums[i] < k.
* cums[j]-cums[i]+nums[i]-k < 0 or cums[j]-cums[i]+nums[i]-k <= -1e-12. Say target = k+cums[i]-nums[i]-1e-12, then we need to find the largest j such that cums[j] <= target.
* We can use bisect_right to find the rightmost index j such that cums[j] <= target. There are strong reasons why we use bisect_right and also what happens when target is in cums or not.
* Example: nums=[1,2,2,2,2,3]. If target is 2.5, bisect_right will return j=5. We would need to adjust and make j=5. If target is 2, bisect_right will return 4 and no adjustment is required.
* Time is Nlog(N) and Space is O(N).
```
from math import log
from bisect import bisect_right
class Solution(object):
    def transform(self, nums, k):
        k, nums = log(k), [log(x) for x in nums]
        cums = []
        for x in nums:
            if cums == []:
                cums.append(x)
            else:
                cums.append(x+cums[-1])
        return cums, nums, k
    
    
    def numSubarrayProductLessThanK(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        if k <= 1:
            return 0
        cums, nums, k = self.transform(nums, k)
        cnt = 0
        for i in range(len(nums)):
            target = k+cums[i]-nums[i]-1e-12
            j = bisect_right(cums, target, i)
            # Adjust index j  when cums[j] != target (i.e. larger). 
            if j == len(cums) or cums[j] > target:
                j = j-1
            cnt += (j-i+1)
        return cnt
```
</p>


