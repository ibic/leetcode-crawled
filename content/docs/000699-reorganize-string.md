---
title: "Reorganize String"
weight: 699
#id: "reorganize-string"
---
## Description
<div class="description">
<p>Given a string <code>S</code>, check if the letters can be rearranged so that two characters that are adjacent to each other are not the same.</p>

<p>If possible, output any possible result.&nbsp; If not possible, return the empty string.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> S = &quot;aab&quot;
<strong>Output:</strong> &quot;aba&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> S = &quot;aaab&quot;
<strong>Output:</strong> &quot;&quot;
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li><code>S</code> will consist of lowercase letters and have length in range <code>[1, 500]</code>.</li>
</ul>

<p>&nbsp;</p>

</div>

## Tags
- String (string)
- Heap (heap)
- Greedy (greedy)
- Sort (sort)

## Companies
- Facebook - 14 (taggedByAdmin: false)
- Google - 9 (taggedByAdmin: false)
- Amazon - 7 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Qualtrics - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)
- Tesla - 3 (taggedByAdmin: false)
- Twitch - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Sort by Count [Accepted]

**Intuition**

If we should make no two `'a'`s adjacent, it is natural to write `"aXaXaXa..."` where `"X"` is some letter.  For now, let's assume that the task is possible (ie. the answer is not `""`.)

Let's sort the string `S`, so all of the same kind of letter occur in continuous *blocks*.  Then when writing in the following interleaving pattern, like `S[3], S[0], S[4], S[1], S[5], S[2]`, adjacent letters never touch.  (The specific interleaving pattern is that we start writing at index 1 and step by 2; then start from index 0 and step by 2.)

The exception to this rule is if `N` is odd, and then when interleaving like `S[2], S[0], S[3], S[1], S[4]`, we might fail incorrectly if there is a block of the same 3 letters starting at `S[0]` or `S[1]`.  To prevent failing erroneously in this case, we need to make sure that the most common letters all occur at the end.

Finally, it is easy to see that if `N` is the length of the string, and the count of some letter is greater than `(N+1) / 2`, the task is impossible.

**Algorithm**

Find the count of each character, and use it to sort the string by count.

If at some point the number of occurrences of some character is greater than `(N + 1) / 2`, the task is impossible.

Otherwise, interleave the characters in the order described above.

<iframe src="https://leetcode.com/playground/uKQgoU4D/shared" frameBorder="0" width="100%" height="480" name="uKQgoU4D"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(\mathcal{A}(N + \log{\mathcal{A}}))$$, where $$N$$ is the length of $$S$$, and $$\mathcal{A}$$ is the size of the alphabet.  In Java, our implementation is $$O(N + \mathcal{A} \log {\mathcal{A}})$$.  If $$\mathcal{A}$$ is fixed, this complexity is $$O(N)$$.

* Space Complexity: $$O(N)$$.  In Java, our implementation is $$O(N + \mathcal{A})$$.

---
#### Approach #2: Greedy with Heap [Accepted]

**Intuition**

One consequence of the reasoning in *Approach #1*, is that a greedy approach that tries to write the most common letter (that isn't the same as the previous letter written) will work.

The reason is that the task is only impossible if the frequency of a letter exceeds `(N+1) / 2`.  Writing the most common letter followed by the second most common letter keeps this invariant.

A heap is a natural structure to repeatedly return the current top 2 letters with the largest remaining counts.

**Approach**

We store a heap of (count, letter).  [In Python, our implementation stores negative counts.]

We pop the top two elements from the heap (representing different letters with positive remaining count), and then write the most frequent one that isn't the same as the most recent one written.  After, we push the correct counts back onto the heap.

Actually, we don't even need to keep track of the most recent one written.  If it is possible to organize the string, the letter written second can never be written first in the very next writing.

At the end, we might have one element still on the heap, which must have a count of one.  If we do, we'll add that to the answer too.

**Proof of Invariant**

The invariant mentioned in the *[Intuition]* section seems true when playing with it, but here is a proof.  Let $$C_i$$ be the count of each letter yet to be written, and $$N$$ be the number of letters left to write.  We want to show this procedure maintains the invariant $$2 * \max\limits_i(C_i) \leq N+1$$.

Say $$C'_i$$ are the counts after one writing step.

* If $$\max(C_i) > \text{3rdmax}(C_i)$$, then $$\max(C'_i) \leq \max(C_i) - 1$$, and so $$2\max(C'_i) \leq 2\max(C_i) - 2 \leq N-1$$ as desired.

* If $$M = \max(C_i) = \text{3rdmax}(C_i)$$, then $$3M \leq N$$.  Also, because $$M \geq 1$$, $$N \geq 3$$.  Then, $$2M \leq \frac{2N}{3} \leq N-1$$ as desired.

This completes the proof of this invariant.

<iframe src="https://leetcode.com/playground/yzYJD8hf/shared" frameBorder="0" width="100%" height="500" name="yzYJD8hf"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N \log{\mathcal{A}}))$$, where $$N$$ is the length of $$S$$, and $$\mathcal{A}$$ is the size of the alphabet.  If $$\mathcal{A}$$ is fixed, this complexity is $$O(N)$$.

* Space Complexity: $$O(\mathcal{A})$$.  If $$\mathcal{A}$$ is fixed, this complexity is $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java, No Sort, O(N), 0ms, beat 100%
- Author: fangbiyi
- Creation Date: Wed Feb 06 2019 00:22:27 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 31 2019 06:37:02 GMT+0800 (Singapore Standard Time)

<p>
No Sort O(N):
1. count letter appearance and store in hash[i]
2. find the letter with largest occurence. 
3. put the letter into even index numbe (0, 2, 4 ...) char array
4. put the rest into the array

```
    public String reorganizeString(String S) {
        int[] hash = new int[26];
        for (int i = 0; i < S.length(); i++) {
            hash[S.charAt(i) - \'a\']++;
        } 
        int max = 0, letter = 0;
        for (int i = 0; i < hash.length; i++) {
            if (hash[i] > max) {
                max = hash[i];
                letter = i;
            }
        }
        if (max > (S.length() + 1) / 2) {
            return ""; 
        }
        char[] res = new char[S.length()];
        int idx = 0;
        while (hash[letter] > 0) {
            res[idx] = (char) (letter + \'a\');
            idx += 2;
            hash[letter]--;
        }
        for (int i = 0; i < hash.length; i++) {
            while (hash[i] > 0) {
                if (idx >= res.length) {
                    idx = 1;
                }
                res[idx] = (char) (i + \'a\');
                idx += 2;
                hash[i]--;
            }
        }
        return String.valueOf(res);
    }
```
Time O(N): fill hash[] + find the letter + write results into char array
Space O(N + 26): result + hash[]
</p>


### Java solution, PriorityQueue
- Author: shawngao
- Creation Date: Sun Jan 21 2018 12:02:36 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 03:59:01 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public String reorganizeString(String S) {
        // Create map of each char to its count
        Map<Character, Integer> map = new HashMap<>();
        for (char c : S.toCharArray()) {
            int count = map.getOrDefault(c, 0) + 1;
            // Impossible to form a solution
            if (count > (S.length() + 1) / 2) return "";
            map.put(c, count);
        }
        // Greedy: fetch char of max count as next char in the result.
        // Use PriorityQueue to store pairs of (char, count) and sort by count DESC.
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> b[1] - a[1]);
        for (char c : map.keySet()) {
            pq.add(new int[] {c, map.get(c)});
        }
        // Build the result.
        StringBuilder sb = new StringBuilder();
        while (!pq.isEmpty()) {
            int[] first = pq.poll();
            if (sb.length() == 0 || first[0] != sb.charAt(sb.length() - 1)) {
                sb.append((char) first[0]);
                if (--first[1] > 0) {
                    pq.add(first);
                }
            } else {
                int[] second = pq.poll();
                sb.append((char) second[0]);
                if (--second[1] > 0) {
                    pq.add(second);
                }
                pq.add(first);
            }
        }
        return sb.toString();
    }
}
```
</p>


### 4 lines Python
- Author: StefanPochmann
- Creation Date: Sun Jan 21 2018 21:12:32 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 12:54:02 GMT+0800 (Singapore Standard Time)

<p>
Put the *least* common letters at the *odd* indexes and put the *most* common letters at the *even* indexes (both from left to right in order of frequency). The task is only impossible if some letter appears too often, in which case it'll occupy all of the even indexes and at least the last odd index, so I check the last two indexes.

    def reorganizeString(self, S):
        a = sorted(sorted(S), key=S.count)
        h = len(a) / 2
        a[1::2], a[::2] = a[:h], a[h:]
        return ''.join(a) * (a[-1:] != a[-2:-1])
</p>


