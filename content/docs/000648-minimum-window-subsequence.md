---
title: "Minimum Window Subsequence"
weight: 648
#id: "minimum-window-subsequence"
---
## Description
<div class="description">
<p>Given strings <code>S</code> and <code>T</code>, find the minimum (contiguous) <b>substring</b> <code>W</code> of <code>S</code>, so that <code>T</code> is a <b>subsequence</b> of <code>W</code>.</p>

<p>If there is no such window in <code>S</code> that covers all characters in <code>T</code>, return the empty string <code>&quot;&quot;</code>. If there are multiple such minimum-length windows, return the one with the left-most starting index.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> 
S = &quot;abcdebdde&quot;, T = &quot;bde&quot;
<b>Output:</b> &quot;bcde&quot;
<b>Explanation:</b> 
&quot;bcde&quot; is the answer because it occurs before &quot;bdde&quot; which has the same length.
&quot;deb&quot; is not a smaller window because the elements of T in the window must occur in order.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ul>
	<li>All the strings in the input will only contain lowercase letters.</li>
	<li>The length of <code>S</code> will be in the range <code>[1, 20000]</code>.</li>
	<li>The length of <code>T</code> will be in the range <code>[1, 100]</code>.</li>
</ul>

<p>&nbsp;</p>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Sliding Window (sliding-window)

## Companies
- Google - 13 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Houzz - 2 (taggedByAdmin: false)
- eBay - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


#### Approach #1: Dynamic Programming (Postfix Variation) [Accepted]

**Intuition**

Let's work on a simpler problem: `T = 'ab'`.  Whenever we find some `'b'` in `S`, we look for the most recent `'a'` that occurred before it, and that forms a candidate window `'a' = S[i], ..., S[j] = 'b'`.

A weak solution to that problem would be to just search for `'a'` every time we find a `'b'`.  With a string like `'abbb...bb'` this would be inefficient.  A better approach is to remember the last `'a'` seen.  Then when we see a `'b'`, we know the start of the window is where we last saw `'a'`, and the end of the window is where we are now.

How can we extend this approach to say, `T = 'abc'`?  Whenever we find some `'c'` in `S`, such as `S[k] = 'c'`, we can remember the most recent window that ended at `'b'`, let's say `[i, j]`.  Then our candidate window (that is, the smallest possible window ending at `k`) would be `[i, k]`.

Our approach in general works this way.  We add characters to `T` one at a time, and for every `S[k] = T[-1]` we always remember the length of the candidate window ending at `k`.  We can calculate this using knowledge of the length of the previous window (so we'll need to remember the last window seen).  This leads to a dynamic programming solution.

**Algorithm**

As we iterate through `T[j]`, let's maintain `cur[e] = s` as the largest index such that `T[:j]` is a subsequence of `S[s: e+1]`, (or `-1` if impossible.)  Now we want to find `new`, the largest indexes for `T[:j+1]`.

To update our knowledge as `j += 1`, if `S[i] == T[j]`, then `last` (the largest `s` we have seen so far) represents a new valid window `[s, i]`.

In Python, we use `cur` and `new`, while in Java we use `dp[j]` and `dp[~j]` to keep track of the last two rows of our dynamic programming.

At the end, we look at all the windows we have and choose the best.

<iframe src="https://leetcode.com/playground/2fHxphed/shared" frameBorder="0" width="100%" height="500" name="2fHxphed"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(ST)$$, where $$S, T$$ are the lengths of `S, T`.  We have two for-loops.

* Space Complexity: $$O(S)$$, the length of `dp`.

---
#### Approach #2: Dynamic Programming (Next Array Variation) [Accepted]

**Intuition**

Let's guess that the minimum window will start at `S[i]`.  We can assume that `S[i] = T[0]`.  Then, we should find the next occurrence of `T[1]` in `S[i+1:]`, say at `S[j]`.  Then, we should find the next occurrence of `T[2]` in `S[j+1:]`, and so on.

Finding the next occurrence can be precomputed in linear time so that each guess becomes $$O(T)$$ work.

**Algorithm**

We can precompute (for each `i` and `letter`), `next[i][letter]`: the index of the first occurrence of `letter` in `S[i:]`, or `-1` if it is not found.

Then, we'll maintain a set of minimum windows for `T[:j]` as `j` increases.  At the end, we'll take the best minimum window.

<iframe src="https://leetcode.com/playground/gYZephC4/shared" frameBorder="0" width="100%" height="500" name="gYZephC4"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(ST)$$, where $$S, T$$ are the lengths of `S, T`, and assuming a fixed-sized alphabet.  The precomputation of `nxt` is $$O(S)$$, and the other work happens in two for-loops.

* Space Complexity: $$O(S)$$, the size of `windows`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### JAVA two pointer solution (12ms, beat 100%) with explaination
- Author: danzhutest
- Creation Date: Wed Nov 22 2017 07:31:53 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 16:09:36 GMT+0800 (Singapore Standard Time)

<p>
The basic idea is simple and based on two pointer:
(1) check feasibility,
(2) check optimization.
```
class Solution {
    public String minWindow(String S, String T) {
        char[] s = S.toCharArray(), t = T.toCharArray();
        int sindex = 0, tindex = 0, slen = s.length, tlen = t.length, start = -1, len = slen;
        while(sindex < slen) {
            if(s[sindex] == t[tindex]) {
                if(++tindex == tlen) {
                    //check feasibility from left to right of T
                    int end = sindex+1;
                    //check optimization from right to left of T
                    while(--tindex >= 0) {
                        while(s[sindex--] != t[tindex]);
                    }
                    ++sindex;
                    ++tindex;
                    //record the current smallest candidate
                    if(end - sindex < len) {
                        len = end - sindex;
                        start = sindex;
                    }
                }
            }
            ++sindex;
        }
        return start == -1? "" : S.substring(start, start + len);
    }
}
```
</p>


### Java Super Easy DP Solution (O(mn))
- Author: share2017
- Creation Date: Thu Nov 16 2017 02:57:53 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 08:30:52 GMT+0800 (Singapore Standard Time)

<p>
dp[i][j] stores the starting index of the substring where T has length i and S has length j.

So dp[i][j would be:
if T[i - 1] == S[j - 1], this means we could borrow the start index from dp[i - 1][j - 1] to make the current substring valid;
else, we only need to borrow the start index from dp[i][j - 1] which could either exist or not.

Finally, go through the last row to find the substring with min length and appears first.
```    
public String minWindow(String S, String T) {
    int m = T.length(), n = S.length();
    int[][] dp = new int[m + 1][n + 1];
    for (int j = 0; j <= n; j++) {
        dp[0][j] = j + 1;
    }
    for (int i = 1; i <= m; i++) {
        for (int j = 1; j <= n; j++) {
            if (T.charAt(i - 1) == S.charAt(j - 1)) {
                dp[i][j] = dp[i - 1][j - 1];
            } else {
                dp[i][j] = dp[i][j - 1];
            }
        }
    }

    int start = 0, len = n + 1;
    for (int j = 1; j <= n; j++) {
        if (dp[m][j] != 0) {
            if (j - dp[m][j] + 1 < len) {
                start = dp[m][j] - 1;
                len = j - dp[m][j] + 1;
            }
        }
    }
    return len == n + 1 ? "" : S.substring(start, start + len);
}
```
</p>


### C++, DP with explanation, O(ST) 53ms
- Author: zestypanda
- Creation Date: Mon Nov 13 2017 02:19:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 12:29:29 GMT+0800 (Singapore Standard Time)

<p>
```
For substring S[0, i] and T[0, j], 
dp[i][j] is starting index k of the shortest postfix of S[0, i], such that T[0, j] is a subsequence of S[k, i]. 
Here T[0] = S[k], T[j] = S[i]. Otherwise, dp[i][j] = -1.

The goal is the substring with length of min(i-dp[i][n-1]) for all i < m,  where m is S.size() and n is T.size() 
Initial condition: dp[i][0] = i if S[i] = T[0], else -1
Equations: If S[i] = T[j], dp[i][j] = max(dp[k][j-1]) for all k < i; else dp[i][j] = -1;      
```
O(mn) space 82 ms
```
class Solution {
public:
    string minWindow(string S, string T) {
        int m = S.size(), n = T.size();
        vector<vector<int>> dp(n, vector<int>(m, -1));
        for (int i = 0; i < m; i++) 
            if (S[i] == T[0]) dp[0][i] = i;
        for (int j = 1; j < n; j++) {
            int k = -1;
            for (int i = 0; i < m; i++) {
                if (k != -1 && S[i] == T[j]) dp[j][i] = k;
                if (dp[j-1][i] != -1) k = dp[j-1][i];
            }
        }
        int st = -1, len = INT_MAX;
        for (int i = 0; i < m; i++) {
            if (dp[n-1][i] != -1 && i-dp[n-1][i]+1 < len) {
                st = dp[n-1][i];
                len = i-dp[n-1][i]+1;
            }    
        }
        return st == -1? "":S.substr(st, len);
    }
};
```
O(m) space 53 ms
```
class Solution {
public:
    string minWindow(string S, string T) {
        int m = S.size(), n = T.size();
        vector<int> dp(m, -1);
        for (int i = 0; i < m; i++) 
            if (S[i] == T[0]) dp[i] = i;
        for (int j = 1; j < n; j++) {
            int k = -1;
            vector<int> tmp(m, -1);
            for (int i = 0; i < m; i++) {
                if (k != -1 && S[i] == T[j]) tmp[i] = k;
                if (dp[i] != -1) k = dp[i];
            }
            swap(dp, tmp);
        }
        int st = -1, len = INT_MAX;
        for (int i = 0; i < m; i++) {
            if (dp[i] != -1 && i-dp[i]+1 < len) {
                st = dp[i];
                len = i-dp[i]+1;
            }    
        }
        return st == -1? "":S.substr(st, len);
    }
};
```
</p>


