---
title: "Swap Nodes in Pairs"
weight: 24
#id: "swap-nodes-in-pairs"
---
## Description
<div class="description">
<p>Given a&nbsp;linked list, swap every two adjacent nodes and return its head.</p>

<p>You may <strong>not</strong> modify the values in the list&#39;s nodes. Only nodes itself may be changed.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/10/03/swap_ex1.jpg" style="width: 422px; height: 222px;" />
<pre>
<strong>Input:</strong> head = [1,2,3,4]
<strong>Output:</strong> [2,1,4,3]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> head = []
<strong>Output:</strong> []
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> head = [1]
<strong>Output:</strong> [1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the&nbsp;list&nbsp;is in the range <code>[0, 100]</code>.</li>
	<li><code>0 &lt;= Node.val &lt;= 100</code></li>
</ul>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Microsoft - 4 (taggedByAdmin: true)
- Amazon - 4 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Apple - 6 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- Oracle - 2 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---

#### Approach 1: Recursive Approach

**Intuition**

The problem doesn't ask for entire reversal of linked list. It's rather asking us to swap every two adjacent nodes of a linked list starting at the very first node.

<center>
<img src="../Figures/24/24_Swap_Nodes_0.png" width="500"/>
</center>

The basic intuition is to reach to the end of the linked list in steps of two using recursion.

<center>
<img src="../Figures/24/24_Swap_Nodes_1.png" width="500"/>
</center>

and while back tracking the nodes can be swapped.

<center>
<img src="../Figures/24/24_Swap_Nodes_2.png" width="500"/>
</center>

In every function call we take out two nodes which would be swapped and the remaining nodes are passed to the next recursive call. The reason we are adopting a recursive approach here is because a sub-list of the original list would still be a linked list and hence, it would adapt to our recursive strategy. Assuming the recursion would return the swapped `remaining` list of nodes, we just swap the current two nodes and attach the remaining list we get from recursion to these two swapped pairs.

<center>
<img src="../Figures/24/24_Swap_Nodes_3.png" width="500"/>
</center>

**Algorithm**

1. Start the recursion with `head` node of the original linked list.

2. Every recursion call is responsible for swapping a pair of nodes. Let's represent the two nodes to be swapped by `firstNode` and `secondNode`.

3. Next recursion is made by calling the function with head of the next pair of nodes. This call would swap the next two nodes and make further recursive calls if there are nodes left in the linked list.

4. Once we get the pointer to the remaining swapped list from the recursion call, we can swap the `firstNode` and `secondNode` i.e. the nodes in the current recursive call and then return the pointer to the `secondNode` since it will be the new head after swapping.

    <center>
    <img src="../Figures/24/24_Swap_Nodes_4.png" width="600"/>
    </center>

5. Once all the pairs are swapped in the backtracking step, we would eventually be returning the pointer to the head of the now `swapped` list. This head will essentially be the second node in the original linked list.
<br>

<iframe src="https://leetcode.com/playground/qqhGNcRg/shared" frameBorder="0" width="100%" height="500" name="qqhGNcRg"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ where $$N$$ is the size of the linked list.
* Space Complexity: $$O(N)$$ stack space utilized for recursion.
<br/>
<br/>

---

#### Approach 2: Iterative Approach

**Intuition**

The concept here is similar to the recursive approach. We break the linked list into pairs by jumping in steps of two. The only difference is, unlike recursion, we swap the nodes on the go. After swapping a pair of nodes, say `A` and `B`, we need to link the node `B` to the node that was right before `A`. To establish this linkage we save the previous node of node `A` in `prevNode`.

<center>
<img src="../Figures/24/24_Swap_Nodes_5.png" width="500"/>
</center>

**Algorithm**

1. We iterate the linked list with jumps in steps of two.

2. Swap the pair of nodes as we go, before we jump to the next pair. Let's represent the two nodes to be swapped by `firstNode` and `secondNode`.

    <center>
    <img src="../Figures/24/24_Swap_Nodes_6.png" width="500"/>
    </center>

3. Swap the two nodes. The swap step is

    <pre>
      firstNode.next = secondNode.next
      secondNode.next = firstNode
    </pre>

    <center>
    <img src="../Figures/24/24_Swap_Nodes_7.png" width="500"/>
    </center>

4. We also need to assign the `prevNode`'s next to the head of the swapped pair. This step would ensure the currently *swapped* pair is linked correctly to the end of the previously *swapped* list.

    <pre>
      prevNode.next = secondNode
    </pre>

    <center>
    <img src="../Figures/24/24_Swap_Nodes_8.png" width="500"/>
    </center>

    This is an iterative step, so the nodes are swapped on the go and attached to the previously swapped list. And in the end we get the final swapped list.

<iframe src="https://leetcode.com/playground/4ucP5grD/shared" frameBorder="0" width="100%" height="500" name="4ucP5grD"></iframe>

**Complexity Analysis**

* Time Complexity : $$O(N)$$ where N is the size of the linked list.

* Space Complexity : $$O(1)$$.
<br/>

## Accepted Submission (java)
```java
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
  // --- Copy from here onwards ---
  // swap 2 nodes and return the tail pointer
  private ListNode swapTwoNodes(ListNode pointer) {
    ListNode first = pointer.next;
    ListNode second = first.next;
    ListNode remaining = second.next;
    second.next = first;
    first.next = remaining;
    pointer.next = second;

    return first;
  }

  public ListNode swapPairs(ListNode head) {
    ListNode pointer = new ListNode(0);
    pointer.next = head;
    ListNode p = pointer;
    while (p.next != null && p.next.next != null) {
      p = swapTwoNodes(p);
    }
    return pointer.next;
  }
  // --- Copy to here ---
}
```

## Top Discussions
### My accepted java code. used recursion.
- Author: whoji
- Creation Date: Fri Oct 24 2014 07:12:22 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 06:33:41 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public ListNode swapPairs(ListNode head) {
            if ((head == null)||(head.next == null))
                return head;
            ListNode n = head.next;
            head.next = swapPairs(head.next.next);
            n.next = head;
            return n;
        }
    }
</p>


### 7-8 lines C++ / Python / Ruby
- Author: StefanPochmann
- Creation Date: Thu Jul 16 2015 06:56:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 02:29:05 GMT+0800 (Singapore Standard Time)

<p>
Three different implementations of the same algorithm, taking advantage of different strengths of the three languages. I suggest reading all three, even if you don't know all three languages.

All three of course work swap the current node with the next node by rearranging pointers, then move on to the next pair, and repeat until the end of the list.

---

**C++**

Pointer-pointer `pp`  points to the pointer to the current node. So at first, `pp` points to `head`, and later it points to the `next` field of ListNodes. Additionally, for convenience and clarity, pointers `a` and `b` point to the current node and the next node.

We need to go from `*pp == a -> b -> (b->next)` to `*pp == b -> a -> (b->next)`. The first three lines inside the loop do that, setting those three pointers (from right to left). The fourth line moves `pp` to the next pair.

    ListNode* swapPairs(ListNode* head) {
        ListNode **pp = &head, *a, *b;
        while ((a = *pp) && (b = a->next)) {
            a->next = b->next;
            b->next = a;
            *pp = b;
            pp = &(a->next);
        }
        return head;
    }

---

**Python**

Here, `pre` is the previous node. Since the head doesn't have a previous node, I just use `self` instead. Again, `a` is the current node and `b` is the next node.

To go from `pre -> a -> b -> b.next` to `pre -> b -> a -> b.next`, we need to change those three references. Instead of thinking about in what order I change them, I just change all three at once.

    def swapPairs(self, head):
        pre, pre.next = self, head
        while pre.next and pre.next.next:
            a = pre.next
            b = a.next
            pre.next, b.next, a.next = b, a, b.next
            pre = a
        return self.next

---

**Ruby**

Again, `pre` is the previous node, but here I create a dummy as previous node of the head. And again, `a` is the current node and `b` is the next node. This time I go one node further and call it `c`.

To go from `pre -> a -> b -> c` to `pre -> b -> a -> c`, we need to change those three references. Here I chain the assignments, pretty much directly saying "`pre` points to `b`, which points to `a`, which points to `c`".

    def swap_pairs(head)
        pre = dummy = ListNode.new 0
        pre.next = head
        while a = pre.next and b = a.next
            c = b.next
            ((pre.next = b).next = a).next = c
            pre = a
        end
        dummy.next
    end
</p>


### My simple JAVA solution for share
- Author: tusizi
- Creation Date: Mon Mar 23 2015 20:22:17 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 11:54:57 GMT+0800 (Singapore Standard Time)

<p>
    public ListNode swapPairs(ListNode head) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode current = dummy;
        while (current.next != null && current.next.next != null) {
            ListNode first = current.next;
            ListNode second = current.next.next;
            first.next = second.next;
            current.next = second;
            current.next.next = first;
            current = current.next.next;
        }
        return dummy.next;
    }
</p>


