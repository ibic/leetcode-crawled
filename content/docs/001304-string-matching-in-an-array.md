---
title: "String Matching in an Array"
weight: 1304
#id: "string-matching-in-an-array"
---
## Description
<div class="description">
<p>Given an array of string <code>words</code>. Return all strings in <code>words</code> which is substring of another word in <strong>any</strong> order.&nbsp;</p>

<p>String <code>words[i]</code> is substring of <code>words[j]</code>,&nbsp;if&nbsp;can be obtained removing some characters to left and/or right side of <code>words[j]</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> words = [&quot;mass&quot;,&quot;as&quot;,&quot;hero&quot;,&quot;superhero&quot;]
<strong>Output:</strong> [&quot;as&quot;,&quot;hero&quot;]
<strong>Explanation:</strong> &quot;as&quot; is substring of &quot;mass&quot; and &quot;hero&quot; is substring of &quot;superhero&quot;.
[&quot;hero&quot;,&quot;as&quot;] is also a valid answer.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> words = [&quot;leetcode&quot;,&quot;et&quot;,&quot;code&quot;]
<strong>Output:</strong> [&quot;et&quot;,&quot;code&quot;]
<strong>Explanation:</strong> &quot;et&quot;, &quot;code&quot; are substring of &quot;leetcode&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> words = [&quot;blue&quot;,&quot;green&quot;,&quot;bu&quot;]
<strong>Output:</strong> []
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= words.length &lt;= 100</code></li>
	<li><code>1 &lt;= words[i].length &lt;= 30</code></li>
	<li><code>words[i]</code> contains only lowercase English letters.</li>
	<li>It&#39;s <strong>guaranteed</strong>&nbsp;that <code>words[i]</code>&nbsp;will be unique.</li>
</ul>
</div>

## Tags
- String (string)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python3 Simple Code
- Author: nterpsec
- Creation Date: Mon Apr 13 2020 20:49:28 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 13 2020 20:49:28 GMT+0800 (Singapore Standard Time)

<p>
```
arr = \' \'.join(words)
subStr = [i for i in words if arr.count(i) >= 2]
        
return subStr
		
</p>


### Clean Python 3, suffix trie O(NlogN + N * S^2)
- Author: lenchen1112
- Creation Date: Sun Apr 12 2020 12:46:09 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 16 2020 12:34:35 GMT+0800 (Singapore Standard Time)

<p>
Sort `words` with length first.
Then add all suffix for each word to the trie after checking its existence in it.

Time: `O(NlogN + N * S^2)`, where `S` is the max length of all words.
NlogN for sorting and N * S^2 for build suffix trie.

Space: `O(N * S^2)` for suffix trie
```
class Solution:
    def stringMatching(self, words: List[str]) -> List[str]:
        def add(word: str):
            node = trie
            for c in word:
                node = node.setdefault(c, {})

        def get(word: str) -> bool:
            node = trie
            for c in word:
                if (node := node.get(c)) is None: return False
            return True

        words.sort(key=len, reverse=True)
        trie, result = {}, []
        for word in words:
            if get(word): result.append(word)
            for i in range(len(word)):
                add(word[i:])
        return result
```

--
Update:
There is [another `O(N * S^2)` approach](https://leetcode.com/problems/string-matching-in-an-array/discuss/575916/Python-3-Crazy-Trie-Implementation-O(N*S2)) proposed by [`@serdes`](https://leetcode.com/serdes/).
It utilizes counting on trie nodes in suffix trie and can prevent to perform sorting at first.
My implementation by referring to this idea:
```
class Solution:
    def stringMatching(self, words: List[str]) -> List[str]:
        def add(word: str):
            node = trie
            for c in word:
                node = node.setdefault(c, {})
                node[\'#\'] = node.get(\'#\', 0) + 1

        def get(word: str) -> bool:
            node = trie
            for c in word:
                if (node := node.get(c)) is None: return False
            return node[\'#\'] > 1

        trie = {}
        for word in words:
            for i in range(len(word)):
                add(word[i:])
        return [word for word in words if get(word)]
```
</p>


### C++ Minimal Solution
- Author: fa5trac3r
- Creation Date: Sun Apr 12 2020 13:19:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 12 2020 13:19:19 GMT+0800 (Singapore Standard Time)

<p>

```
class Solution {
public:
    vector<string> stringMatching(vector<string>& words) {
        vector<string> ans;
        for(auto i:words)
        {
            for(auto j: words)
            {
                if(i==j) continue;
                if(j.find(i)!=-1)
                {
                    ans.push_back(i);
                    break;                    
                }
            }
        }
        return ans;
    }
};

</p>


