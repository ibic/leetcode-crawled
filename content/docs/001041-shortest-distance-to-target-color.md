---
title: "Shortest Distance to Target Color"
weight: 1041
#id: "shortest-distance-to-target-color"
---
## Description
<div class="description">
<p>You are given an array <code>colors</code>, in which there are three colors: <code>1</code>, <code>2</code> and&nbsp;<code>3</code>.</p>

<p>You are also given some queries. Each query consists of two integers <code>i</code>&nbsp;and <code>c</code>, return&nbsp;the shortest distance between the given index&nbsp;<code>i</code> and the target color <code>c</code>. If there is no solution return <code>-1</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> colors = [1,1,2,1,3,2,2,3,3], queries = [[1,3],[2,2],[6,1]]
<strong>Output:</strong> [3,0,3]
<strong>Explanation: </strong>
The nearest 3 from index 1 is at index 4 (3 steps away).
The nearest 2 from index 2 is at index 2 itself (0 steps away).
The nearest 1 from index 6 is at index 3 (3 steps away).
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> colors = [1,2], queries = [[0,3]]
<strong>Output:</strong> [-1]
<strong>Explanation: </strong>There is no 3 in the array.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= colors.length &lt;= 5*10^4</code></li>
	<li><code>1 &lt;= colors[i] &lt;= 3</code></li>
	<li><code>1&nbsp;&lt;= queries.length &lt;= 5*10^4</code></li>
	<li><code>queries[i].length == 2</code></li>
	<li><code>0 &lt;= queries[i][0] &lt;&nbsp;colors.length</code></li>
	<li><code>1 &lt;= queries[i][1] &lt;= 3</code></li>
</ul>

</div>

## Tags
- Binary Search (binary-search)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A Novel Method using DP [O(n) | O(n)] [Explained]
- Author: Just__a__Visitor
- Creation Date: Sun Sep 08 2019 00:42:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 01:14:51 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Define 2 DP matrix, `left` and `right`. The DP definition is `left[color][index]` represents the minimum distance of the color `color` to the left of the index `index`. Similarly, `right[color][index]` represents the minimum distance of the color `color` to the right of the index `index`. Note that we also include the current element in the DP definition.

For each of the 3 colours, we can fill both the `dp` matrix in linear time. Let us fix any color, say 1, and fix a dp matrix, say `left`. Now, the minimum distance of red from the `i-th` index to the left will be zero if the `i-th` index is of red color. If the `i-th` color is not zero, we can look at the minimum distance of the `i-1 th` index. If the index is -1, it means that no red exists in left half. Hence, the current distance would be -1. If not, the current distance would be `oldDistance + 1`.  A similar argument can be made for `right`.

At the end, for each index, we can find the minimum distance to the left and right and return the minimum of both of them.


## Getting the Result from the pre-computed values
For any query, we are given the value of the color and the index. We find out the minimum distance of that color in the left as well as right part. If either of them is -1, the answer is the other one. If none of them is -1, then the answer is the minimum of the two. 

# Time Complexity 
O(n);

```
class Solution
{
public:
    vector<int> shortestDistanceColor(vector<int>& colors, vector<vector<int>>& queries);
};

vector<int> Solution :: shortestDistanceColor(vector<int>& colors, vector<vector<int>>& queries)
{
    int n = colors.size();
    
    vector<vector<int>> left (4, vector<int>(n,-1));
    vector<vector<int>> right(4, vector<int>(n,-1));
    
    for(int shade = 1; shade <= 3; shade++)
    {
        if(colors[0] == shade)
            left[shade][0] = 0;
        for(int i = 1; i < n; i++)
        {
            if(left[shade][i-1] != -1)
                left[shade][i] = left[shade][i-1] + 1;
            
            if(colors[i] == shade)
                left[shade][i] = 0;
        }
    }
    
    for(int shade = 1; shade <= 3; shade++)
    {
        if(colors[n-1] == shade)
            right[shade][n-1] = 0;
        
        for(int i = n-2; i >= 0; i--)
        {
           if(right[shade][i+1] != -1)
                right[shade][i] = right[shade][i+1] + 1;
            
            if(colors[i] == shade)
                right[shade][i] = 0;
        }
    }
    
    
    vector<int> result;
    
    for(auto query : queries)
    {
        int index = query[0];
        int req_color = query[1];
        
        int x = left[req_color][index];
        int y = right[req_color][index];
        
        int ans;
        if(x==-1 or y == -1)
            ans = max(x,y);
        else
            ans = min(x,y);
        
        result.push_back(ans);
    }
    
    
    return result;
}
```
</p>


### Java binary search
- Author: Chen_Xiang
- Creation Date: Wed Sep 18 2019 14:14:16 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 18 2019 14:15:00 GMT+0800 (Singapore Standard Time)

<p>
Use a map to store color and all of its positions in the array.
For each index and color in query, we use a binary search to find index\'s closest color\'s index in all of color\'s index list.
```
class Solution {
    public List<Integer> shortestDistanceColor(int[] colors, int[][] queries) {
        HashMap<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < colors.length; i++) {
            int c = colors[i];
            map.putIfAbsent(c, new ArrayList<>());
            map.get(c).add(i);
        }
        List<Integer> ans = new ArrayList<>();
        for (int[] query : queries) {
            int index = query[0];
            int c = query[1];
            if (!map.containsKey(c)) {
                ans.add(-1);
            } else {
                ans.add(binarySearch(index, map.get(c)));
            }
        }
        return ans;
    }
    
    public int binarySearch(int index, List<Integer> list) {
        int left = 0;
        int right = list.size() - 1;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (list.get(mid) < index) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        int res = left;
        if (left - 1 >= 0 && index - list.get(left - 1) < list.get(left) - index) {
            res = left - 1;
        }
        return Math.abs(list.get(res) - index);
    }
}
```
</p>


### Python binary search solution
- Author: CodeLenin
- Creation Date: Sun Sep 08 2019 00:37:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 00:41:00 GMT+0800 (Singapore Standard Time)

<p>
Basic idea is to group all indexes by color.
And for each index in the queries, do binary search on the color group. 
There are 3 possibilites 
1.  the index is before the list( list of indexes for a color group)
2.  the index is after the list
3.  the index is in the middle of the list

```
class Solution:
    def shortestDistanceColor(self, colors: List[int], queries: List[List[int]]) -> List[int]:
        maps = collections.defaultdict(list)
		# group all indexes by color
        for i, v in enumerate(colors): 
			maps[v].append(i)
        
        ans = []
        for i, c in queries:
            if c in maps:
				# search where the element can be inserted
                index = bisect.bisect_left(maps[c], i)
                if index == 0:
                    ans.append(abs(i-maps[c][0]))
                elif index >= len(maps[c]):
                    ans.append(i-maps[c][-1])
                else:
                    ans.append(min(abs(i-maps[c][index-1]), abs(maps[c][index]-i)))
            else:
                ans.append(-1)
        
        return ans
```
</p>


