---
title: "Max Consecutive Ones III"
weight: 955
#id: "max-consecutive-ones-iii"
---
## Description
<div class="description">
<p>Given an array <code>A</code>&nbsp;of 0s and 1s, we may change up to <code>K</code>&nbsp;values from 0 to 1.</p>

<p>Return the length of the longest (contiguous) subarray that contains only 1s.&nbsp;</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[1,1,1,0,0,0,1,1,1,1,0]</span>, K = <span id="example-input-1-2">2</span>
<strong>Output: </strong><span id="example-output-1">6</span>
<strong>Explanation: </strong>
[1,1,1,0,0,<u><strong>1</strong>,1,1,1,1,<strong>1</strong></u>]
Bolded numbers were flipped from 0 to 1.  The longest subarray is underlined.</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">[0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1]</span>, K = <span id="example-input-2-2">3</span>
<strong>Output: </strong><span id="example-output-2">10</span>
<strong>Explanation: </strong>
[0,0,<u>1,1,<b>1</b>,<strong>1</strong>,1,1,1,<strong>1</strong>,1,1</u>,0,0,0,1,1,1,1]
Bolded numbers were flipped from 0 to 1.  The longest subarray is underlined.
</pre>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 20000</code></li>
	<li><code>0 &lt;= K &lt;= A.length</code></li>
	<li><code>A[i]</code> is <code>0</code> or <code>1</code>&nbsp;</li>
</ol>
</div>
</div>
</div>

## Tags
- Two Pointers (two-pointers)
- Sliding Window (sliding-window)

## Companies
- Amazon - 7 (taggedByAdmin: false)
- Facebook - 6 (taggedByAdmin: true)
- Yandex - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

The problem has asked for longest contiguous subarray that contains only 1s. What makes this problem a little trickier is the `K` flips allowed from `0` --> `1`. This means a contiguous subarray of 1's might not just contain `1`'s but also may contain some `0`'s. The number of `0`'s allowed in a given subarray is given by `K`.

  <center>
  <img src="../Figures/1004/1004_Max_Consecutive_Ones_1.png" width="500"/>
  </center>
  <br>
The above diagram shows the `flip` which helped us to get the longest contiguous subarray if only one flip was allowed.

#### Approach: Sliding Window

**Intuition**

To find the longest subarray with contiguous `1`'s we might need to find all the subarrays first. But do we really need to do that? If we find all the subarrays we are essentially finding out so many unnecessary overlapping subarrays too.

We can use a simple sliding window approach to solve this problem.

In any sliding window based problem we have two pointers. One right pointer whose job is to expand the current window and then we have the left pointer whose job is to contract a given window. At any point in time only one of these pointers move and the other one remains fixed.

The solution is pretty intuitive. We keep expanding the window by moving the right pointer. When the window has reached the limit of `0`'s allowed, we contract (if possible) and save the longest window till now.

The answer is the longest desirable window.

**Algorithm**

1. Initialize two pointers. The two pointers help us to mark the left and right end of the window/subarray with contiguous `1`'s.

    >left = 0, right = 0, window_size = 0

2. We use the `right` pointer to expand the window until the window/subarray is desirable. i.e. number of `0`'s in the window are in the allowed range of [0, K].

3. Once we have a window which has more than the allowed number of `0`'s, we can move the left pointer ahead one by one until we encounter `0` on the left too. This step ensures we are throwing out the extra zero.

<center>
<img src="../Figures/1004/1004_Max_Consecutive_Ones_2.png" width="500"/>
</center>
<br>

> Note: As suggested in the [discussion forum](https://leetcode.com/problems/max-consecutive-ones-iii/discuss/247564/JavaC++Python-Sliding-Window). We can solve this problem a little efficiently. Since we have to find the MAXIMUM window, we never reduce the size of the window. We either increase the size of the window or remain same but never reduce the size.

<center>
<img src="../Figures/1004/1004_Max_Consecutive_Ones_3.png" width="500"/>
</center>
<br>

Observe we don't contract the window if it's not needed and thus save on some computation.

<iframe src="https://leetcode.com/playground/LVrnDwY7/shared" frameBorder="0" width="100%" height="361" name="LVrnDwY7"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of elements in the array. In worst case we might end up visiting every element of array twice, once by left pointer and once by right pointer.

* Space Complexity: $$O(1)$$. We do not use any extra space.
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Sliding Window
- Author: lee215
- Creation Date: Sun Mar 03 2019 12:05:37 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 08 2020 22:39:23 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Translation:
Find the longest subarray with at most `K` zeros.
<br>

# **Explanation**
For each `A[j]`, try to find the longest subarray.
If `A[i] ~ A[j]` has zeros <= `K`, we continue to increment `j`.
If `A[i] ~ A[j]` has zeros > `K`, we increment `i` (as well as `j`).
<br>

**Java:**
```
    public int longestOnes(int[] A, int K) {
        int i = 0, j;
        for (j = 0; j < A.length; ++j) {
            if (A[j] == 0) K--;
            if (K < 0 && A[i++] == 0) K++;
        }
        return j - i;
    }
```

**C++:**
```
    int longestOnes(vector<int>& A, int K) {
        int i = 0, j;
        for (j = 0; j < A.size(); ++j) {
            if (A[j] == 0) K--;
            if (K < 0 && A[i++] == 0) K++;
        }
        return j - i;
    }
```

**Python:**
```
    def longestOnes(self, A, K):
        i = 0
        for j in xrange(len(A)):
            K -= 1 - A[j]
            if K < 0:
                K += 1 - A[i]
                i += 1
        return j - i + 1
```
<br>

# More Similar Sliding Window Problems
Here are some similar sliding window problems.
Also find more explanations.
Good luck and have fun.

- 1248. [Count Number of Nice Subarrays](https://leetcode.com/problems/count-number-of-nice-subarrays/discuss/419378/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
- 1234. [Replace the Substring for Balanced String](https://leetcode.com/problems/replace-the-substring-for-balanced-string/discuss/408978/javacpython-sliding-window/367697)
- 1004. [Max Consecutive Ones III](https://leetcode.com/problems/max-consecutive-ones-iii/discuss/247564/javacpython-sliding-window/379427?page=3)
-  930. [Binary Subarrays With Sum](https://leetcode.com/problems/binary-subarrays-with-sum/discuss/186683/)
-  992. [Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/discuss/234482/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
-  904. [Fruit Into Baskets](https://leetcode.com/problems/fruit-into-baskets/discuss/170740/Sliding-Window-for-K-Elements)
-  862. [Shortest Subarray with Sum at Least K](https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/discuss/143726/C%2B%2BJavaPython-O(N)-Using-Deque)
-  209. [Minimum Size Subarray Sum](https://leetcode.com/problems/minimum-size-subarray-sum/discuss/433123/JavaC++Python-Sliding-Window)
<br>

</p>


### O(n) Java Solution using sliding window
- Author: avcd
- Creation Date: Sun Mar 03 2019 12:03:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 03 2019 12:03:00 GMT+0800 (Singapore Standard Time)

<p>
I think the question is a simplified version of [424. Longest Repeating Character Replacement](https://leetcode.com/problems/longest-repeating-character-replacement/)
My solution is changed from [Java 12 lines O(n) sliding window solution with explanation](https://leetcode.com/problems/longest-repeating-character-replacement/discuss/91271/Java-12-lines-O(n)-sliding-window-solution-with-explanation)
```
public int longestOnes(int[] A, int K) {
        int zeroCount=0,start=0,res=0;
        for(int end=0;end<A.length;end++){
            if(A[end] == 0) zeroCount++;
            while(zeroCount > K){
                if(A[start] == 0) zeroCount--;
                start++;
            }
            res=Math.max(res,end-start+1);
        }
        return res;
    }
```
</p>


### java sliding windows with comments in line
- Author: noteanddata
- Creation Date: Mon Mar 04 2019 06:09:39 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 04 2019 06:09:39 GMT+0800 (Singapore Standard Time)

<p>
http://www.noteanddata.com/leetcode-1004-Max-Consecutive-Ones-III-java-sliding-window-solution-note.html
below are a sliding windows version with comments inline, this might not be concise enough, but easy to understand 
```
public int longestOnes(int[] A, int K) {
    int max = 0;
    int zeroCount = 0; // zero count in current window
    int i = 0; // slow pointer
    for(int j = 0; j < A.length; ++j) {
        if(A[j] == 0) { // move forward j, if current is 0, increase the zeroCount
            zeroCount++;
        }
        
        // when current window has more than K, the window is not valid any more
        // we need to loop the slow pointer until the current window is valid
        while(zeroCount > K) {  
            if(A[i] == 0) {
                zeroCount--;
            }
            i++;
        }
        max = Math.max(max, j-i+1); // everytime we get here, the current window is valid 
    }
    return max;
}
```
</p>


