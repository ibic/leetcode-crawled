---
title: "Number of Substrings Containing All Three Characters"
weight: 1252
#id: "number-of-substrings-containing-all-three-characters"
---
## Description
<div class="description">
<p>Given a string <code>s</code>&nbsp;consisting only of characters <em>a</em>, <em>b</em> and <em>c</em>.</p>

<p>Return the number of substrings containing <b>at least</b>&nbsp;one occurrence of all these characters <em>a</em>, <em>b</em> and <em>c</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abcabc&quot;
<strong>Output:</strong> 10
<strong>Explanation:</strong> The substrings containing&nbsp;at least&nbsp;one occurrence of the characters&nbsp;<em>a</em>,&nbsp;<em>b</em>&nbsp;and&nbsp;<em>c are &quot;</em>abc<em>&quot;, &quot;</em>abca<em>&quot;, &quot;</em>abcab<em>&quot;, &quot;</em>abcabc<em>&quot;, &quot;</em>bca<em>&quot;, &quot;</em>bcab<em>&quot;, &quot;</em>bcabc<em>&quot;, &quot;</em>cab<em>&quot;, &quot;</em>cabc<em>&quot; </em>and<em> &quot;</em>abc<em>&quot; </em>(<strong>again</strong>)<em>. </em>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aaacb&quot;
<strong>Output:</strong> 3
<strong>Explanation:</strong> The substrings containing&nbsp;at least&nbsp;one occurrence of the characters&nbsp;<em>a</em>,&nbsp;<em>b</em>&nbsp;and&nbsp;<em>c are &quot;</em>aaacb<em>&quot;, &quot;</em>aacb<em>&quot; </em>and<em> &quot;</em>acb<em>&quot;.</em><em> </em>
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abc&quot;
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>3 &lt;= s.length &lt;= 5 x 10^4</code></li>
	<li><code>s</code>&nbsp;only consists of&nbsp;<em>a</em>, <em>b</em> or <em>c&nbsp;</em>characters.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- DE Shaw - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy and Concise
- Author: lee215
- Creation Date: Sun Feb 23 2020 00:03:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 00:44:28 GMT+0800 (Singapore Standard Time)

<p>
## Solution 1: Sliding Window
Find many similar sliding window problem at the end of post.

Time `O(N)`
Space `O(1)`
<br>

**Java:**
```java
    public int numberOfSubstrings(String s) {
        int count[] = {0, 0, 0}, res = 0 , i = 0, n = s.length();
        for (int j = 0; j < n; ++j) {
            ++count[s.charAt(j) - \'a\'];
            while (count[0] > 0 && count[1] > 0 && count[2] > 0)
                --count[s.charAt(i++) - \'a\'];
            res += i;
        }
        return res;
    }
```

**C++:**
```cpp
    int numberOfSubstrings(string s) {
        int count[3] = {0, 0, 0},res = 0 , i = 0, n = s.length();
        for (int j = 0; j < n; ++j) {
            ++count[s[j] - \'a\'];
            while (count[0] && count[1] && count[2])
                --count[s[i++] - \'a\'];
            res += i;
        }
        return res;
    }
```

**Python:**
```python
    def numberOfSubstrings(self, s):
        res = i = 0
        count = {c: 0 for c in \'abc\'}
        for j in xrange(len(s)):
            count[s[j]] += 1
            while all(count.values()):
                count[s[i]] -= 1
                i += 1
            res += i
        return res
```
<br>

# Solution II
`last` will record the position of last occurrence.
If the ending index of substring is `i`,
the starting position should be on the left of `min(last)`,
in order to have all 3 different letters.
And in this case, the starting index can be in range `[0, min(last)]`,
`min(last) + 1` in total.

Time `O(N)`
Space `O(1)`
<br>

**Java**
```java
    public int numberOfSubstrings(String s) {
        int last[] = {-1, -1, -1}, res = 0, n = s.length();
        for (int i = 0; i < n; ++i) {
            last[s.charAt(i) - \'a\'] = i;
            res += 1 + Math.min(last[0], Math.min(last[1], last[2]));
        }
        return res;
    }
```
**C++**
```cpp
    int numberOfSubstrings(string s) {
        int last[3] = {-1, -1, -1}, res = 0, n = s.length();
        for (int i = 0; i < n; ++i) {
            last[s[i] - \'a\'] = i;
            res += 1 + min({last[0], last[1], last[2]});
        }
        return res;
    }
```
**Python**
```py
    def numberOfSubstrings(self, s):
        res, last = 0, [-1] * 3
        for i, c in enumerate(s):
            last[ord(c) - 97] = i
            res += 1 + min(last)
        return res
```
<br>

# More Similar Sliding Window Problems
Here are some similar sliding window problems.
Also find more explanations.
Good luck and have fun.


- 1248. [Count Number of Nice Subarrays](https://leetcode.com/problems/count-number-of-nice-subarrays/discuss/419378/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
- 1234. [Replace the Substring for Balanced String](https://leetcode.com/problems/replace-the-substring-for-balanced-string/discuss/408978/javacpython-sliding-window/367697)
- 1004. [Max Consecutive Ones III](https://leetcode.com/problems/max-consecutive-ones-iii/discuss/247564/javacpython-sliding-window/379427?page=3)
-  930. [Binary Subarrays With Sum](https://leetcode.com/problems/binary-subarrays-with-sum/discuss/186683/)
-  992. [Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/discuss/234482/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
-  904. [Fruit Into Baskets](https://leetcode.com/problems/fruit-into-baskets/discuss/170740/Sliding-Window-for-K-Elements)
-  862. [Shortest Subarray with Sum at Least K](https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/discuss/143726/C%2B%2BJavaPython-O(N)-Using-Deque)
-  209. [Minimum Size Subarray Sum](https://leetcode.com/problems/minimum-size-subarray-sum/discuss/433123/JavaC++Python-Sliding-Window)
<br>
</p>


### [Java] ELEGANT No Sliding Window EXPLAINED (No of Sub-Strings Ending at Curr Index)
- Author: manrajsingh007
- Creation Date: Sun Feb 23 2020 00:02:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 00:22:24 GMT+0800 (Singapore Standard Time)

<p>
Okay so these questions look like they have sliding window, But in this one i figured out something else.
Take three pointers l1, l2, l3 for a, b and c respectively.
Now as you iterate over the string of length n, you can count the number of sub-strings ending at that particular index.
How can we do that is here ->
* Keep on updating l1, l2 and l3.
* And take the minimum of l1, l2 and l3.
* Now from this min-index (min(l1, l2, l3) to the curr index i this is the smallest possible sub-string ending at curr-index i which follows the constraint.
* If the smallest sub-string is from min-index to curr-index, then for every sub-string starting from index 0, 1, 2, 3, ......min-index and ending at curr-index is valid, (So how many are these...... So there are min-index + 1 sub-strings)
* Add this min-index + 1 to the count.
```
class Solution {
    public int numberOfSubstrings(String s) {
        int n = s.length(), count = 0;
        int l1 = -1, l2 = -1, l3 = -1;
        for(int i = 0; i < n; i++) {
            if(s.charAt(i) == \'a\') l1 = i;
            else if(s.charAt(i) == \'b\') l2 = i;
            else l3 = i;
            if(l1 == -1 || l2 == -1 || l3 == -1) continue;
            int min = Math.min(l1, Math.min(l2, l3));
            count += min + 1;
        }
        return count;
    }
}
</p>


### [Java/Python 3] Sliding Window O(n) O(1) code w/ explanation and analysis.
- Author: rock
- Creation Date: Sun Feb 23 2020 00:03:29 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 24 2020 02:05:05 GMT+0800 (Singapore Standard Time)

<p>
1. Maintain a sliding window `(lo, hi]`, where lower bound exclusively and upper bound inclusively;
2. Traverse string `s`, use upper bound `hi` to count the number of the 3 characters, `a, b, & c`; once the sliding window includes all of the 3, we find `s.length() - hi` substrings `(lo, hi], (lo, hi + 1], ..., (lo, s.length() - 1]`;
3. Increase the lower bound `lo` by 1 (denote it as `lo\'`), decrease the `count` accordingly, if the sliding window still includes all of the 3 characters, we count in substrings `(lo\', hi], (lo\', hi + 1], ..., (lo\', s.length() - 1]`;
4. Repeat 3 till the sliding window short of at least 1 of the 3 characters, go to step 2;
5. Repeat 2 - 4 till the end of the string `s`.

```java
    public int numberOfSubstrings(String s) {
        int[] count = new int[3];
        int ans = 0;
        for (int lo = -1, hi = 0; hi < s.length(); ++hi) {
            ++count[s.charAt(hi) - \'a\'];
            while (count[0] > 0 && count[1] > 0 && count[2] > 0) {
                ans += s.length() - hi; // number of valid substrings all start from lo + 1, but end at hi, hi + 1, ..., s.length() - 1, respectively.
                --count[s.charAt(++lo) - \'a\'];
            }
        } 
        return ans;        
    }
```
```python
    def numberOfSubstrings(self, s: str) -> int:
        ans, lo, cnt = 0, -1, {c : 0 for c in \'abc\'}
        for hi, c in enumerate(s):
            cnt[c] += 1
            while all(cnt.values()):
                ans += len(s) - hi
                lo += 1
                cnt[s[lo]] -= 1
        return ans
```
**Analysis:**

Time: O(n), space: O(1), where n = s.length().
</p>


