---
title: "List the Products Ordered in a Period"
weight: 1563
#id: "list-the-products-ordered-in-a-period"
---
## Description
<div class="description">
<p>Table: <code>Products</code></p>

<pre>
+------------------+---------+
| Column Name      | Type    |
+------------------+---------+
| product_id       | int     |
| product_name     | varchar |
| product_category | varchar |
+------------------+---------+
product_id is the primary key for this table.
This table contains data about the company&#39;s products.
</pre>

<p>Table: <code>Orders</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| product_id    | int     |
| order_date    | date    |
| unit          | int     |
+---------------+---------+
There is no primary key&nbsp;for this table. It&nbsp;may have&nbsp;duplicate rows.
product_id is a foreign key to Products table.
unit is the number of products ordered in order_date.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to get the names of products with greater than or equal to 100 units ordered in February 2020 and their amount.</p>

<p>Return result table in any order.</p>

<p>The query result format is in the following example:</p>

<p>&nbsp;</p>

<pre>
Products table:
+-------------+-----------------------+------------------+
| product_id  | product_name          | product_category |
+-------------+-----------------------+------------------+
| 1           | Leetcode Solutions    | Book             |
| 2           | Jewels of Stringology | Book             |
| 3           | HP                    | Laptop           |
| 4           | Lenovo                | Laptop           |
| 5           | Leetcode Kit          | T-shirt          |
+-------------+-----------------------+------------------+

Orders table:
+--------------+--------------+----------+
| product_id   | order_date   | unit     |
+--------------+--------------+----------+
| 1            | 2020-02-05   | 60       |
| 1            | 2020-02-10   | 70       |
| 2            | 2020-01-18   | 30       |
| 2            | 2020-02-11   | 80       |
| 3            | 2020-02-17   | 2        |
| 3            | 2020-02-24   | 3        |
| 4            | 2020-03-01   | 20       |
| 4            | 2020-03-04   | 30       |
| 4            | 2020-03-04   | 60       |
| 5            | 2020-02-25   | 50       |
| 5            | 2020-02-27   | 50       |
| 5            | 2020-03-01   | 50       |
+--------------+--------------+----------+

Result table:
+--------------------+---------+
| product_name       | unit    |
+--------------------+---------+
| Leetcode Solutions | 130     |
| Leetcode Kit       | 100     |
+--------------------+---------+

Products with product_id = 1 is ordered in February a total of (60 + 70) = 130.
Products with product_id = 2 is ordered in February a total of 80.
Products with product_id = 3 is ordered in February a total of (2 + 3) = 5.
Products with product_id = 4 was not ordered in February 2020.
Products with product_id = 5 is ordered in February a total of (50 + 50) = 100.
</pre>
</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Myql Using Month and Year function
- Author: meredithlou
- Creation Date: Mon Feb 03 2020 05:01:02 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 03 2020 05:01:02 GMT+0800 (Singapore Standard Time)

<p>
select product_name,sum(unit) as unit
from Products a
left join Orders b on a.product_id = b.product_id
where month(order_date) = 2 and year(order_date) = \'2020\'
group by a.product_id
Having unit >=100
</p>


### Description issue
- Author: abdulmusto
- Creation Date: Wed Mar 25 2020 23:59:14 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Mar 25 2020 23:59:14 GMT+0800 (Singapore Standard Time)

<p>
Problem says, orders table CAN contain dupicate rows. But notice, how I get erro when using second solution (I am filtering duplicate rows)

Correct one.
\'\'\'
select p.product_name, sum(o.unit) as unit
from Products p inner join (select * 
                                        from Orders 
                                            where Year(order_date)=2020 and
                                                  Month(order_date) = 2
                                        ) as o 
                            on p.product_id = o.product_id
    group by p.product_id
    having sum(o.unit) >=100
\'\'\'

This one is wrong.

\'\'\'
select p.product_name, sum(o.unit) as unit
from Products p inner join (select distinct * 
                                        from Orders 
                                            where Year(order_date)=2020 and
                                                  Month(order_date) = 2
                                        ) as o 
                            on p.product_id = o.product_id
    group by p.product_id
    having sum(o.unit) >=100
\'\'\'
</p>


### Easy MySQL Solution - 100% Faster
- Author: vertikau
- Creation Date: Fri Jan 24 2020 22:01:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jan 24 2020 22:01:45 GMT+0800 (Singapore Standard Time)

<p>
```
select p.product_name,
sum(o.unit) as unit
from Products p
join Orders o
on p.product_id = o.product_id
where Left(order_date, 7) = \'2020-02\'
group by p.product_name
having sum(o.unit) >= 100
```
</p>


