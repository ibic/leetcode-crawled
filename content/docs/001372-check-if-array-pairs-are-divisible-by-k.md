---
title: "Check If Array Pairs Are Divisible by k"
weight: 1372
#id: "check-if-array-pairs-are-divisible-by-k"
---
## Description
<div class="description">
<p>Given an array of integers <code>arr</code> of even length <code>n</code> and an integer <code>k</code>.</p>

<p>We want to divide the array into exactly <code>n /&nbsp;2</code> pairs such that the sum of each pair is divisible by <code>k</code>.</p>

<p>Return <em>True</em> If you can find a way to do that or <em>False</em> otherwise.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,3,4,5,10,6,7,8,9], k = 5
<strong>Output:</strong> true
<strong>Explanation:</strong> Pairs are (1,9),(2,8),(3,7),(4,6) and (5,10).
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,3,4,5,6], k = 7
<strong>Output:</strong> true
<strong>Explanation:</strong> Pairs are (1,6),(2,5) and(3,4).
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,3,4,5,6], k = 10
<strong>Output:</strong> false
<strong>Explanation:</strong> You can try all possible pairs to see that there is no way to divide arr into 3 pairs each with sum divisible by 10.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [-10,10], k = 2
<strong>Output:</strong> true
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> arr = [-1,1,-2,2,-3,3,-4,4], k = 3
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>arr.length == n</code></li>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
	<li><code>n</code> is even.</li>
	<li><code>-10^9 &lt;= arr[i] &lt;= 10^9</code></li>
	<li><code>1 &lt;= k &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Array (array)
- Math (math)
- Greedy (greedy)

## Companies
- Paypal - 7 (taggedByAdmin: false)
- Quble - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 7ms Simple Solution
- Author: harin_mehta
- Creation Date: Sun Jun 28 2020 17:30:16 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 29 2020 10:22:01 GMT+0800 (Singapore Standard Time)

<p>
***Idea :***  
Given 2 nums \'a\' and \'b\':
If **a % k == x**  and **b % k == k - x** :
then **(a + b) is divisible by k**

***Proof :*** 
1. a % k == x
2. b % k == k - x
3. (a + b) % k = ((a + b)%k)%k = (a%k + b%k)%k = (x + k - x)%k = k%k = 0 
4. Hence, (a + b) % k == 0 and (a + b) is divisible by k.

***Approach :***
* Keep count of remainders of all elements of arr
* frequency[0] keeps all elements divisible by k, and a divisible of k can only form a group with other divisible of k. Hence, total number of such divisibles must be even.
* for every element with remainder of **i** (i != 0) there should be a element with remainder **k-i**.
* Hence, **frequency[i]** should be equal to **frequency[k-i]**


```
class Solution {
    public boolean canArrange(int[] arr, int k) {
        int[] frequency = new int[k];
        for(int num : arr){
            num %= k;
            if(num < 0) num += k;
            frequency[num]++;
        }
        if(frequency[0]%2 != 0) return false;
        
        for(int i = 1; i <= k/2; i++)
            if(frequency[i] != frequency[k-i]) return false;
			
        return true;
    }
}
```
</p>


### Weak TC passes my 1 liner | Correct Solution Using Reminder Frequency
- Author: rajmc
- Creation Date: Sun Jun 28 2020 13:43:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 03 2020 01:16:30 GMT+0800 (Singapore Standard Time)

<p>
**Using Reminder Frequency** Correct solution
1. As we know we can get max - 0, 1, .... k - 1 reminders value.
2. In first pass of the array note down the reminder frequency if reminder -ve just add the k in.
3. In second  pass just check for all reminders 1 to k  if frequency is equl for i and k -  i
4. Just check  reminder 0 frequency should be even.

**Video Explanation** - https://youtu.be/dhFR_m1pZvs?t=0

```
class Solution {
    public boolean canArrange(int[] arr, int k) {
        int[] reminderFreq = new int[k];
        for(int a : arr) {
            int rmd = a % k;
            if(rmd < 0) {
                rmd += k;
            }
            reminderFreq[rmd]++;
        }
        
        for(int i = 1; i < k/2; i++) {
            if(reminderFreq[i] != reminderFreq[k - i])
                return false;
        }
        
        return reminderFreq[0] % 2 == 0;
    }
}
```

`TC - O(n)`
`SC - O(k)`

**Negative Mode Explanation Credit @yrq**
```
Explaination for Java negative mod
3 % -2 = 3 - (3 / -2) * -2 = 1
-3 % -2 = -3 - (-3 / -2) * -2 = -1
-3 % 2 = -3 - (-3 / 2) * 2 = -1

if we have case [1, -1, 3, -10] k = 7
1 % 7 = 1
-1 % 7 = -1 - (-1 / 7) * 7 = -1
-1 % 7 same as 6 % 7 (one is negative result, one is positive result)

3 % 7 = 3
-10 % 7 = -10 - (-10 / 7) * 7 = -3
-10 % 7 = -3 % 7 same as 4 % 7 = 4
```

****
This is  hacky  solution passes in contest b/c of weak tc.

```
class Solution {
    public boolean canArrange(int[] ar, int k) {
        return (Arrays.stream(ar).mapToLong(i -> i).sum()) % k == 0 ? true : false;
    }
}
```
****

</p>


### Short C++ Solution explained with comments
- Author: Zeldris
- Creation Date: Sun Jun 28 2020 13:05:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 14:07:20 GMT+0800 (Singapore Standard Time)

<p>
**Use a map to store the frequencies of remainders and then travel the array and check one by one.**
```
//(x%k + k)%k is done to counter negative integers in the array.

class Solution {
public:
    bool canArrange(vector<int>& arr, int k) {
        if(arr.size()&1) return false;
        unordered_map<int,int>m;
        for(auto x:arr) m[(x%k + k)%k]++;          //store the count of remainders in a map.
        for(auto x:arr)
        {
            int rem=(x%k + k)%k;
            if(rem==0)                         //if the remainder for an element is 0 then the count of numbers that give this remainder must be even.
            { 
                if(m[rem] & 1) return false;            //if count of numbers that give this remainder is odd all pairs can\'t be made hence return false.
            }         
            else if(m[rem] != m[k - rem]) return false;    //if the remainder rem and k-rem do not have the same count then pairs can not be made 
        }
        return true;
    }
};
```

**This solution works in the contest due to weak test cases**

```
class Solution {
public:
    bool canArrange(vector<int>& arr, int k) {
        long long res = 0;
        for(int x:arr) res +=x; 
        return (res % k == 0);  // if the sum of all elements is divisible by k then all pairs exists
    }
};
```

</p>


