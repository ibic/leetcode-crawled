---
title: "XOR Queries of a Subarray"
weight: 1231
#id: "xor-queries-of-a-subarray"
---
## Description
<div class="description">
Given the array <code>arr</code> of positive integers and the array <code>queries</code> where <code>queries[i] = [L<sub>i,&nbsp;</sub>R<sub>i</sub>]</code>,&nbsp;for each query <code>i</code> compute the <strong>XOR</strong> of elements from <code>L<sub>i</sub></code> to <code>Ri</code> (that is, <code>arr[L<sub>i</sub>] <strong>xor</strong> arr[L<sub>i+1</sub>] <strong>xor</strong> ... <strong>xor</strong> arr[R<sub>i</sub>]</code> ). Return an array containing the result for the given <code>queries</code>.
<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,3,4,8], queries = [[0,1],[1,2],[0,3],[3,3]]
<strong>Output:</strong> [2,7,14,8] 
<strong>Explanation:</strong> 
The binary representation of the elements in the array are:
1 = 0001 
3 = 0011 
4 = 0100 
8 = 1000 
The XOR values for queries are:
[0,1] = 1 xor 3 = 2 
[1,2] = 3 xor 4 = 7 
[0,3] = 1 xor 3 xor 4 xor 8 = 14 
[3,3] = 8
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [4,8,2,10], queries = [[2,3],[1,3],[0,0],[0,3]]
<strong>Output:</strong> [8,0,4,4]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 3 *&nbsp;10^4</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 10^9</code></li>
	<li><code>1 &lt;= queries.length &lt;= 3 * 10^4</code></li>
	<li><code>queries[i].length == 2</code></li>
	<li><code>0 &lt;= queries[i][0] &lt;= queries[i][1] &lt; arr.length</code></li>
</ul>
</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Airtel - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Straight Forward Solution
- Author: lee215
- Creation Date: Sun Jan 05 2020 12:10:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 05 2020 12:22:42 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
In-place calculate the prefix XOR of input `A`.

For each query `[i, j]`,
if `i == 0`, query result = `A[j]`
if `i != 0`, query result = `A[i - 1] ^ A[j]`
<br>

## **Complexity**
Time `O(N)`
`O(1)` extra space
`O(Q)` for output
<br>


**Java**
```java
    public int[] xorQueries(int[] A, int[][] queries) {
        int[] res = new int[queries.length], q;
        for (int i = 1; i < A.length; ++i)
            A[i] ^= A[i - 1];
        for (int i = 0; i < queries.length; ++i) {
            q = queries[i];
            res[i] = q[0] > 0 ? A[q[0] - 1] ^ A[q[1]] : A[q[1]];
        }
        return res;
    }
```

**C++**
```cpp
    vector<int> xorQueries(vector<int>& A, vector<vector<int>>& queries) {
        vector<int> res;
        for (int i = 1; i < A.size(); ++i)
            A[i] ^= A[i - 1];
        for (auto &q: queries)
            res.push_back(q[0] > 0 ? A[q[0] - 1] ^ A[q[1]] : A[q[1]]);
        return res;
    }
```
**Python:**
```python
    def xorQueries(self, A, queries):
        for i in xrange(len(A) - 1):
            A[i + 1] ^= A[i]
        return [A[j] ^ A[i - 1] if i else A[j] for i, j in queries]
```

</p>


### [C++] Prefix XORs
- Author: PhoenixDD
- Creation Date: Sun Jan 05 2020 12:01:44 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 06 2020 06:59:36 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
The result of even count of XORd integers is 0, eg `19^19^3^2` = `3^2` since 19 occurs 2 times.

We can simply store all the prefix XORs from `0-i` where `i` ranges from `0..arr.length-1` and use the above observation to calculate a range.
eg: `range = [4,10]`

Here we can use the XOR of range `0-10` and XOR it with result of range `0-3` which will give us the required range as XOR of `0-10` will have the XORs in range `0-3` and thus again XORing it with range `0-3` will give us the result for `[4-10]`.

**Solution**
```c++
class Solution {
public:
    vector<int> xorQueries(vector<int>& arr, vector<vector<int>>& queries) 
    {
        vector<int> prefixXor(arr.size()+1,0),result;
        for(int i=0;i<arr.size();i++)                         //Create Prefix Xors as mentioned above.
            prefixXor[i+1]=prefixXor[i]^arr[i];
        for(vector<int> &v:queries)
            result.push_back(prefixXor[v[1]+1]^prefixXor[v[0]]);  //Add result to the query according to the above observation.
        return result;
    }
};
```

**Complexity**
Space: `O(n)` for storing `prefixXor`.(This can be avoided if we use input as the auxilary array, however it won\'t reduce the "algorithm\'s" space complexity.)
Time: `O(n)` for calculating `prefixXor` and `O(1)` for each range query.

</p>


### Python Prefix Solution with Explanation
- Author: yuyingji
- Creation Date: Sun Jan 05 2020 12:05:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 05 2020 15:57:39 GMT+0800 (Singapore Standard Time)

<p>

	def xorQueries(self, arr, queries): 
			d = {}
			""" d[i] represents the xor from position 0 to position i"""
			d[-1] = 1
			res = []
			for i in range(len(arr)):
				d[i] = d[i - 1] ^ arr[i]
			for q in queries:
				s = q[0]
				e = q[1]
				""" see formula induction below"""
				res.append(d[e] ^ d[s - 1])
			return res
			
There are two formula needed:
1. **Xor[0, i] ^ Xor [i, j] = Xor [0, j]**
 Xor[x, y] means xor of all the number from A to B
2. **A Xor B Xor B = A**

if A Xor B = C , then A Xor B Xor B = C Xor B
So, Xor[i, j] = Xor[0, j] ^ Xor[0, i]

</p>


