---
title: "Convert Binary Search Tree to Sorted Doubly Linked List"
weight: 679
#id: "convert-binary-search-tree-to-sorted-doubly-linked-list"
---
## Description
<div class="description">
<p>Convert a <strong>Binary Search Tree</strong>&nbsp;to a sorted <strong>Circular Doubly-Linked List</strong>&nbsp;in place.</p>

<p>You can think of the left and right pointers as synonymous to the predecessor and successor pointers in a doubly-linked list. For a circular doubly linked list, the predecessor of the first element is the last element, and the successor of the last element is the first element.</p>

<p>We want to do the transformation <strong>in place</strong>. After the transformation, the left pointer of the tree node should point to its predecessor, and the right pointer should point to its successor. You should return the pointer to the smallest element of the linked list.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/12/bstdlloriginalbst.png" style="width: 100%; max-width: 300px;" /></p>

<pre>
<strong>Input:</strong> root = [4,2,5,1,3]

<img src="https://assets.leetcode.com/uploads/2018/10/12/bstdllreturndll.png" style="width: 100%; max-width: 450px;" />
<strong>Output:</strong> [1,2,3,4,5]

<strong>Explanation:</strong> The figure below shows the transformed BST. The solid line indicates the successor relationship, while the dashed line means the predecessor relationship.
<img src="https://assets.leetcode.com/uploads/2018/10/12/bstdllreturnbst.png" style="width: 100%; max-width: 450px;" />
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = [2,1,3]
<strong>Output:</strong> [1,2,3]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = []
<strong>Output:</strong> []
<strong>Explanation:</strong> Input is an empty tree. Output is also an empty Linked List.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root = [1]
<strong>Output:</strong> [1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>-1000 &lt;= Node.val &lt;= 1000</code></li>
	<li><code>Node.left.val &lt; Node.val &lt; Node.right.val</code></li>
	<li>All values of <code>Node.val</code> are unique.</li>
	<li><code>0 &lt;= Number of Nodes &lt;= 2000</code></li>
</ul>

</div>

## Tags
- Linked List (linked-list)
- Divide and Conquer (divide-and-conquer)
- Tree (tree)

## Companies
- Facebook - 11 (taggedByAdmin: true)
- Amazon - 5 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Databricks - 3 (taggedByAdmin: false)
- Lyft - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### How to traverse the tree

There are two general strategies to traverse a tree:
     
- *Depth First Search* (`DFS`)

    In this strategy, we adopt the `depth` as the priority, so that one
    would start from a root and reach all the way down to certain leaf,
    and then back to root to reach another branch.

    The DFS strategy can further be distinguished as
    `preorder`, `inorder`, and `postorder` depending on the relative order
    among the root node, left node and right node.
    
- *Breadth First Search* (`BFS`)

    We scan through the tree level by level, following the order of height,
    from top to bottom. The nodes on higher level would be visited before
    the ones with lower levels.
    
On the following figure the nodes are numerated in the order you visit them,
please follow `1-2-3-4-5` to compare different strategies.

![postorder](../Figures/426/dfs_bfs.png)

Here the problem is to implement DFS inorder traversal
in a textbook recursion way because of in-place
requirement.
<br /> 
<br />


---
#### Approach 1: Recursion

**Algorithm**

Standard inorder recursion follows `left -> node -> right` order,
where `left` and `right` parts are the recursion calls and
`node` part is where all processing is done.

Processing here is basically to link the previous node with the 
current one, and because of that one has to track the last node
which is the largest node in a new doubly linked list so far. 

![postorder](../Figures/426/process.png)

One more detail : one has to keep the first, or the smallest, node 
as well to close the ring of doubly linked list.

Here is the algorithm :

- Initiate the `first` and the `last` nodes as nulls.

- Call the standard inorder recursion `helper(root)` :

    - If node is not null :
        
        - Call the recursion for the left subtree `helper(node.left)`.
        
        - If the `last` node is not null, link the `last` and 
        the current `node` nodes.
        
        - Else initiate the `first` node.
        
        - Mark the current node as the last one : `last = node`.
        
        - Call the recursion for the right subtree `helper(node.right)`.

- Link the first and the last nodes to close DLL ring and then 
return the `first` node.

**Implementation**

!?!../Documents/426_LIS.json:1000,519!?!

<iframe src="https://leetcode.com/playground/JkKMRo5x/shared" frameBorder="0" width="100%" height="500" name="JkKMRo5x"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since each node is processed
exactly once.
 
* Space complexity : $$\mathcal{O}(N)$$. We have to keep a 
recursion stack of the size of the tree height, which is 
$$\mathcal{O}(\log N)$$ for the best case of completely balanced tree
and $$\mathcal{O}(N)$$ for the worst case of completely
unbalanced tree.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Divide and Conquer without Dummy Node Java Solution
- Author: FLAGbigoffer
- Creation Date: Sun Jul 29 2018 12:13:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 00:38:59 GMT+0800 (Singapore Standard Time)

<p>
Step 1: Divide:
We divide tree into three parts: left subtree, root node, right subtree.
Convert left subtree into a circular doubly linked list as well as the right subtree.
Be careful. You have to make the root node become a circular doubly linked list.

Step 2: Conquer:
Firstly, connect left circular doubly linked list with the root circular doubly linked list.
Secondly, connect them with the right circular doubly linked list. Here we go!

```
class Solution {
    public Node treeToDoublyList(Node root) {
        if (root == null) {
            return null;
        }
        
        Node leftHead = treeToDoublyList(root.left);
        Node rightHead = treeToDoublyList(root.right);
        root.left = root;
        root.right = root;
        return connect(connect(leftHead, root), rightHead);
    }
    
    // Used to connect two circular doubly linked lists. n1 is the head of circular DLL as well as n2.
    private Node connect(Node n1, Node n2) {
        if (n1 == null) {
            return n2;
        }
        if (n2 == null) {
            return n1;
        }
        
        Node tail1 = n1.left;
        Node tail2 = n2.left;
        
        tail1.right = n2;
        n2.left = tail1;
        tail2.right = n1;
        n1.left = tail2;
        
        return n1;
    }
}
````
</p>


### Concise Java solution  - Beats 100% 
- Author: Cris_Tang
- Creation Date: Fri Jul 13 2018 14:21:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 00:31:35 GMT+0800 (Singapore Standard Time)

<p>
step1: inorder tranversal by recursion to connect the original BST
step2: connect the head and tail to make it circular

Tips: Using dummy node to handle corner case

```
Node prev = null;
public Node treeToDoublyList(Node root) {
	if (root == null) return null;
	Node dummy = new Node(0, null, null);
	prev = dummy;
	helper(root);
	//connect head and tail
	prev.right = dummy.right;
	dummy.right.left = prev;
	return dummy.right;
}

private void helper (Node cur) {
	if (cur == null) return;
	helper(cur.left);
	prev.right = cur;
	cur.left = prev;
	prev = cur;
	helper(cur.right);
}
```
</p>


### Python Inorder Transverse 
- Author: liangyonghit
- Creation Date: Fri Sep 14 2018 06:40:42 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 16:50:57 GMT+0800 (Singapore Standard Time)

<p>
class Solution(object):


    def treeToDoublyList(self, root):
        if not root: return
        dummy = Node(0, None, None)
        prev = dummy
        stack, node = [], root
        while stack or node:
            while node:
                stack.append(node)
                node = node.left
            node = stack.pop()
            node.left, prev.right, prev = prev, node, node
            node = node.right
        dummy.right.left, prev.right = prev, dummy.right
        return dummy.right
</p>


