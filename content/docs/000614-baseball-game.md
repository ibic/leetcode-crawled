---
title: "Baseball Game"
weight: 614
#id: "baseball-game"
---
## Description
<div class="description">
<p>You are keeping score for a baseball game with strange rules. The game consists of several rounds, where the scores of past rounds may affect future rounds&#39; scores.</p>

<p>At the beginning of the game, you start with an empty record. You are given a list of strings <code>ops</code>, where <code>ops[i]</code> is the <code>i<sup>th</sup></code> operation you must apply to the record and is one of the following:</p>

<ol>
	<li>An integer <code>x</code> - Record a new score of <code>x</code>.</li>
	<li><code>&quot;+&quot;</code> - Record a new score that is the sum of the previous two scores. It is guaranteed there will always be two previous scores.</li>
	<li><code>&quot;D&quot;</code> - Record a new score that is double the previous score. It is guaranteed there will always be a previous score.</li>
	<li><code>&quot;C&quot;</code> - Invalidate the previous score, removing it from the record. It is guaranteed there will always be a previous score.</li>
</ol>

<p>Return <em>the sum of all the scores on the record</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> ops = [&quot;5&quot;,&quot;2&quot;,&quot;C&quot;,&quot;D&quot;,&quot;+&quot;]
<strong>Output:</strong> 30
<strong>Explanation:</strong>
&quot;5&quot; - Add 5 to the record, record is now [5].
&quot;2&quot; - Add 2 to the record, record is now [5, 2].
&quot;C&quot; - Invalidate and remove the previous score, record is now [5].
&quot;D&quot; - Add 2 * 5 = 10 to the record, record is now [5, 10].
&quot;+&quot; - Add 5 + 10 = 15 to the record, record is now [5, 10, 15].
The total sum is 5 + 10 + 15 = 30.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> ops = [&quot;5&quot;,&quot;-2&quot;,&quot;4&quot;,&quot;C&quot;,&quot;D&quot;,&quot;9&quot;,&quot;+&quot;,&quot;+&quot;]
<strong>Output:</strong> 27
<strong>Explanation:</strong>
&quot;5&quot; - Add 5 to the record, record is now [5].
&quot;-2&quot; - Add -2 to the record, record is now [5, -2].
&quot;4&quot; - Add 4 to the record, record is now [5, -2, 4].
&quot;C&quot; - Invalidate and remove the previous score, record is now [5, -2].
&quot;D&quot; - Add 2 * -2 = -4 to the record, record is now [5, -2, -4].
&quot;9&quot; - Add 9 to the record, record is now [5, -2, -4, 9].
&quot;+&quot; - Add -4 + 9 = 5 to the record, record is now [5, -2, -4, 9, 5].
&quot;+&quot; - Add 9 + 5 = 14 to the record, record is now [5, -2, -4, 9, 5, 14].
The total sum is 5 + -2 + -4 + 9 + 5 + 14 = 27.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> ops = [&quot;1&quot;]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= ops.length &lt;= 1000</code></li>
	<li><code>ops[i]</code> is <code>&quot;C&quot;</code>, <code>&quot;D&quot;</code>, <code>&quot;+&quot;</code>, or a string representing an integer in the range <code>[-3 * 10<sup>4</sup>, 3 * 10<sup>4</sup>]</code>.</li>
	<li>For operation <code>&quot;+&quot;</code>, there will always be at least two previous scores on the record.</li>
	<li>For operations <code>&quot;C&quot;</code> and <code>&quot;D&quot;</code>, there will always be at least one previous score on the record.</li>
</ul>

</div>

## Tags
- Stack (stack)

## Companies
- Amazon - 19 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)

## Official Solution
#### Approach #1: Stack [Accepted]

**Intuition and Algorithm**

Let's maintain the value of each valid round on a stack as we process the data.  A stack is ideal since we only deal with operations involving the last or second-last valid round.

<iframe src="https://leetcode.com/playground/FRAbgcgJ/shared" frameBorder="0" name="FRAbgcgJ" width="100%" height="462"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `ops`.  We parse through every element in the given array once, and do $$O(1)$$ work for each element.

* Space Complexity: $$O(N)$$, the space used to store our `stack`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Straightforward Python
- Author: cutesy_gal
- Creation Date: Sun Sep 24 2017 11:03:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 24 2017 11:03:55 GMT+0800 (Singapore Standard Time)

<p>
It not stated in the question but you can assume there is not invalid operations, such as `C` when the points history is empty, it still passed.

```
class Solution(object):
    def calPoints(self, ops):
        # Time: O(n)
        # Space: O(n)
        history = []
        for op in ops:
            if op == 'C':
                history.pop()
            elif op == 'D':
                history.append(history[-1] * 2)
            elif op == '+':
                history.append(history[-1] + history[-2])
            else:
                history.append(int(op))
        return sum(history)

```
</p>


### Verbose Java solution, LinkedList
- Author: shawngao
- Creation Date: Sun Sep 24 2017 11:07:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 15 2018 08:18:07 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int calPoints(String[] ops) {
        int sum = 0;
        LinkedList<Integer> list = new LinkedList<>();
        for (String op : ops) {
            if (op.equals("C")) {
                sum -= list.removeLast();
            }
            else if (op.equals("D")) {
                list.add(list.peekLast() * 2);
                sum += list.peekLast();
            }
            else if (op.equals("+")) {
                list.add(list.peekLast() + list.get(list.size() - 2));
                sum += list.peekLast();
            }
            else {
                list.add(Integer.parseInt(op));
                sum += list.peekLast();
            }
        }
        return sum;
    }
}
```
</p>


### ⭐️  [ Javascript / Python3 / C++ ] 🥞  Stack Accumulation
- Author: claytonjwong
- Creation Date: Sun Sep 24 2017 11:04:02 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 17 2020 00:52:57 GMT+0800 (Singapore Standard Time)

<p>
**Synopsis:**

Use a stack `s` to store previous rounds\' points.  Return the accumulated sum of what\'s leftover on the stack `s` after processing all operations `ops`.

---

**Note:** there is a typo in this problem description.  The **Example 1 output should be 30**, ```(15 is NOT correct)```

Example 1:
Input: ["5","2","C","D","+"]
```Output: 15```

---

*Javascript*
```
let calPoints = (ops, s = []) => {
    for (op of ops) {
        let n = s.length;
        if (op == \'+\')
            s.push(s[n - 2] + s[n - 1]);
        else if (op == \'D\')
            s.push(2 * s[n - 1]);
        else if (op == \'C\')
            s.pop();
        else
            s.push(+op);
    }
    return _.sum(s)
};
```

*Python3*
```
class Solution:
    def calPoints(self, ops: List[str]) -> int:
        s = []
        for op in ops:
            if op == "+":
                s.append(s[-2] + s[-1])
            elif op == "D":
                s.append(2 * s[-1])
            elif op == "C":
                s.pop()
            else:
                s.append(int(op))
        return sum(s)
```

*C++*
```
class Solution {
public:
    using VS = vector<string>;
    using VI = vector<int>;
    int calPoints(VS& ops, VI s = {}) {
        for (auto& op: ops) {
            int n = s.size();
            if (op == "+")
                s.push_back(s[n - 2] + s[n - 1]);
            else if (op == "D")
                s.push_back(2 * s[n - 1]);
            else if (op == "C")
                s.pop_back();
            else
                s.push_back(stoi(op));
        }
        return accumulate(s.begin(), s.end(), 0);
    }
};
```
</p>


