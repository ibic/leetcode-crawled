---
title: "Robot Room Cleaner"
weight: 779
#id: "robot-room-cleaner"
---
## Description
<div class="description">
<p>Given a robot cleaner in a room modeled as a grid.</p>

<p>Each cell in the grid can be empty or blocked.</p>

<p>The robot cleaner with 4 given APIs can move forward, turn left or turn right. Each turn it made is 90 degrees.</p>

<p>When it tries to move into a blocked cell, its bumper sensor detects the obstacle and it stays on the current cell.</p>

<p>Design an algorithm to clean the entire room using only the 4 given APIs shown below.</p>

<pre>
interface Robot {
&nbsp; // returns true if next cell is open and robot moves into the cell.
&nbsp; // returns false if next cell is obstacle and robot stays on the current cell.
&nbsp; boolean move();

  // Robot will stay on the same cell after calling turnLeft/turnRight.
&nbsp; // Each turn will be 90 degrees.
&nbsp; void turnLeft();
&nbsp; void turnRight();

  // Clean the current cell.
  void clean();
}
</pre>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong>
room = [
  [1,1,1,1,1,0,1,1],
  [1,1,1,1,1,0,1,1],
  [1,0,1,1,1,1,1,1],
  [0,0,0,1,0,0,0,0],
  [1,1,1,1,1,1,1,1]
],
row = 1,
col = 3

<strong>Explanation:</strong>
All grids in the room are marked by either 0 or 1.
0 means the cell is blocked, while 1 means the cell is accessible.
The robot initially starts at the position of row=1, col=3.
From the top left corner, its position is one row below and three columns right.
</pre>

<p><strong>Notes:</strong></p>

<ol>
	<li>The input is only given to initialize the room and the robot&#39;s position internally.&nbsp;You must solve this problem &quot;blindfolded&quot;. In other words, you must control the robot using only the mentioned 4 APIs, without knowing the room layout and the initial robot&#39;s position.</li>
	<li>The robot&#39;s initial position will always be in an accessible cell.</li>
	<li>The initial direction of the robot will be facing up.</li>
	<li>All accessible cells are connected, which means the all cells marked as 1 will be accessible by the robot.</li>
	<li>Assume all four edges of the grid are all surrounded by wall.</li>
</ol>

</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Google - 7 (taggedByAdmin: true)
- Amazon - 6 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Spiral Backtracking

**Concepts to use**

Let's use here two programming concepts.

> The first one is called _constrained programming_. 

That basically means
to put restrictions after each robot move. Robot moves, and the cell is marked as `visited`. 
That propagates 
_constraints_ and helps to reduce the number of combinations to consider.

![bla](../Figures/489/489_constraints.png)

> The second one called _backtracking_. 

Let's imagine that after several moves the robot is surrounded by the visited cells. 
But several steps before there was a cell which proposed an alternative path to go. 
That path wasn't used and hence the room is not yet cleaned up. 
What to do? _To backtrack_. 
That means to come back to that cell,
and to explore the alternative path. 

![bla](../Figures/489/489_backtrack.png)

**Intuition**

This solution is based on the same idea as maze solving algorithm called [right-hand rule](https://en.wikipedia.org/wiki/Maze_solving_algorithm#Wall_follower).
Go forward, cleaning and marking all the cells on the way
as visited. At the obstacle _turn right_, again go forward, _etc_. 
Always _turn right_ at the obstacles and then go forward. 
Consider already visited cells as 
virtual obstacles.

> What do do if after the right turn there is an obstacle just in front ?

_Turn right_ again.

> How to explore the alternative paths from the cell ? 

Go back to that cell
and then _turn right_ from your last explored direction.

> When to stop ?

Stop when you explored all possible paths, _i.e._ 
all `4` directions (up, right, down, and left) for each visited cell.

**Algorithm**

Time to write down the algorithm for the backtrack function 
`backtrack(cell = (0, 0), direction = 0)`.

- Mark the cell as visited and clean it up.

- Explore `4` directions : `up`, `right`, `down`, and `left` (the order
is important since the idea is always to turn right) : 

    - Check the next cell in the chosen direction :
    
        - If it's not visited yet and there is no obtacles :
            
            - Move forward.
            
            - Explore next cells `backtrack(new_cell, new_direction)`.
            
            - Backtrack, _i.e._ go back to the previous cell.
            
        - Turn right because now there is an obstacle 
        (or a virtual obstacle) just in front.


**Implementation**

![bla](../Figures/489/489_implementation.png)

<iframe src="https://leetcode.com/playground/WyV3pKEK/shared" frameBorder="0" width="100%" height="500" name="WyV3pKEK"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N - M)$$, where $$N$$ is a number of cells
in the room and $$M$$ is a number of obstacles.

  - We visit each non-obstacle cell once and only once.
  - At each visit, we will check 4 directions around the cell. Therefore, the total number of operations would be $$4 \cdot (N-M)$$.

* Space complexity : $$\mathcal{O}(N - M)$$, where $$N$$ is a number of cells
in the room and $$M$$ is a number of obstacles.

  - We employed a hashtable to keep track of whether a non-obstacle cell is visited or not.

## Accepted Submission (python3)
```python3
# """
# This is the robot's control interface.
# You should not implement it, or speculate about its implementation
# """
#class Robot:
#    def move(self):
#        """
#        Returns true if the cell in front is open and robot moves into the cell.
#        Returns false if the cell in front is blocked and robot stays in the current cell.
#        :rtype bool
#        """
#
#    def turnLeft(self):
#        """
#        Robot will stay in the same cell after calling turnLeft/turnRight.
#        Each turn will be 90 degrees.
#        :rtype void
#        """
#
#    def turnRight(self):
#        """
#        Robot will stay in the same cell after calling turnLeft/turnRight.
#        Each turn will be 90 degrees.
#        :rtype void
#        """
#
#    def clean(self):
#        """
#        Clean the current cell.
#        :rtype void
#        """

class Solution:
    def cleanRoom(self, robot):
        """
        :type robot: Robot
        :rtype: None
        """
        def _clean(x, y, dirX, dirY, cleaned: Set[Any]):
            robot.clean()
            cleaned.add((x, y))
            
            for _ in range(4):
                nextX = x + dirX
                nextY = y + dirY
                if (nextX, nextY) not in cleaned and robot.move():
                    _clean(nextX, nextY, dirX, dirY, cleaned)
                robot.turnLeft()
                dirX, dirY = -dirY, dirX
                
            robot.turnLeft();
            robot.turnLeft();
            robot.move()
            robot.turnLeft();
            robot.turnLeft();
        
        cleaned = set()
        _clean(0, 0, 0, 1, cleaned)
```

## Top Discussions
### Very easy to understand Java solution
- Author: xiaozhon
- Creation Date: Thu Jun 14 2018 05:33:35 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 11:19:14 GMT+0800 (Singapore Standard Time)

<p>
1. To track the cells the robot has cleaned, start a index pair (i, j) from (0, 0). When go up, i-1; go down, i+1; go left, j-1; go right: j+1.
2. Also use DIR to record the current direction of the robot
```
    public void cleanRoom(Robot robot) {
        // A number can be added to each visited cell
        // use string to identify the class
        Set<String> set = new HashSet<>();
        int cur_dir = 0;   // 0: up, 90: right, 180: down, 270: left
        backtrack(robot, set, 0, 0, 0);
    }

    public void backtrack(Robot robot, Set<String> set, int i, 
    			int j, int cur_dir) {
    	String tmp = i + "->" + j;
    	if(set.contains(tmp)) {
            return;
        }
        
    	robot.clean();
    	set.add(tmp);

    	for(int n = 0; n < 4; n++) {
        // the robot can to four directions, we use right turn
    		if(robot.move()) {
    			// can go directly. Find the (x, y) for the next cell based on current direction
    			int x = i, y = j;
    			switch(cur_dir) {
    				case 0:
    					// go up, reduce i
    					x = i-1;
    					break;
    				case 90:
    					// go right
    					y = j+1;
    					break;
    				case 180:
    					// go down
    					x = i+1;
    					break;
    				case 270:
    					// go left
    					y = j-1;
    					break;
    				default:
    					break;
    			}

    			backtrack(robot, set, x, y, cur_dir);
                       // go back to the starting position
			robot.turnLeft();
    			robot.turnLeft();
    			robot.move();
    			robot.turnRight();
    			robot.turnRight();

    		} 
    		// turn to next direction
    		robot.turnRight();
    		cur_dir += 90;
    		cur_dir %= 360;
    	}

    }
```
</p>


### DFS Logical Thinking
- Author: GraceMeng
- Creation Date: Wed Jul 25 2018 19:41:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 26 2020 23:50:30 GMT+0800 (Singapore Standard Time)

<p>
> To indicate whether a cell has been cleaned(visited), we assume the start poin is (0, 0) and initial orientation is 0 as follows
```
    0
3  -|-  1
    2
```
> each orientation is associated with a direction. directions = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};  indicates movement.
    
> We can try moving in 4 directions for each step (starting from orientation x)
```
move forward - move; orientation x
move right - turnRight, move; orientation (x + 1) % 4
move backward - turnRight, turnRight, move; orientation (x + 2) % 4
move left - turnRight, turnRight, turnRight, move;  orientation (x + 3) % 4
```

> Using DFS, we have to backtrack after we explore as far as possible along a branch, i.e. robot moves backward one step while maintaining its orientation.
****
```
class Solution {
    
    private static final int[][] directions = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
    
    public void cleanRoom(Robot robot) {
        clean(robot, 0, 0, 0, new HashSet<>());
    }
    
    private void clean(Robot robot, int x, int y, int curDirection, Set<String> visited) {
        // Cleans current cell.
        robot.clean();
        visited.add(x + " " + y);
        
        for (int nDirection = curDirection; 
             nDirection < curDirection + 4; 
             nDirection++) {
            int nx = directions[nDirection % 4][0] + x;
            int ny = directions[nDirection % 4][1] + y;
            if (!visited.contains(nx + " " + ny) && robot.move()) {
                clean(robot, nx, ny, nDirection % 4, visited);
            }
            // Changed orientation.
            robot.turnRight();
        }
        
        // Moves backward one step while maintaining the orientation.
        robot.turnRight();
        robot.turnRight();
        robot.move();
        robot.turnRight();
        robot.turnRight();
    }
}
```
**(\u4EBA \u2022\u0348\u1D17\u2022\u0348)** Thanks for voting!
</p>


### Very clear Python DFS code, beat 99% +
- Author: Water00
- Creation Date: Mon Jul 16 2018 03:20:39 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 10:40:17 GMT+0800 (Singapore Standard Time)

<p>
```python
class Solution(object):
    def cleanRoom(self, robot):
        """
        :type robot: Robot
        :rtype: None
        """
        self.dfs(robot, 0, 0, 0, 1, set())
    
    def dfs(self, robot, x, y, direction_x, direction_y, visited):
        robot.clean()
        visited.add((x, y))
        
        for k in range(4):
            neighbor_x = x + direction_x
            neighbor_y = y + direction_y
            if (neighbor_x, neighbor_y) not in visited and robot.move():
                self.dfs(robot, neighbor_x, neighbor_y, direction_x, direction_y, visited)
                robot.turnLeft()
                robot.turnLeft()
                robot.move()
                robot.turnLeft()
                robot.turnLeft()
            robot.turnLeft()
            direction_x, direction_y = -direction_y, direction_x   
```
</p>


