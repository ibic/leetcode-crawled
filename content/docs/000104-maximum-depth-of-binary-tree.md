---
title: "Maximum Depth of Binary Tree"
weight: 104
#id: "maximum-depth-of-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree, find its maximum depth.</p>

<p>The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.</p>

<p><strong>Note:</strong>&nbsp;A leaf is a node with no children.</p>

<p><strong>Example:</strong></p>

<p>Given binary tree <code>[3,9,20,null,null,15,7]</code>,</p>

<pre>
    3
   / \
  9  20
    /  \
   15   7</pre>

<p>return its depth = 3.</p>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 6 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: true)
- Adobe - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- Bloomberg - 3 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- LinkedIn - 12 (taggedByAdmin: true)
- Yahoo - 3 (taggedByAdmin: true)
- SAP - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

**Tree definition**

First of all, here is the definition of the ```TreeNode``` which we would use.

<iframe src="https://leetcode.com/playground/4PUaBALP/shared" frameBorder="0" width="100%" height="225" name="4PUaBALP"></iframe>
<br />
<br />


---
#### Approach 1: Recursion

**Intuition**
By definition, the maximum depth of a binary tree is the maximum number of steps to reach a leaf node from the root node.

>From the definition, an intuitive idea would be to traverse the tree and record the maximum depth during the traversal. 

**Algorithm**

<!--![LIS](../Figures/104/104_tr.gif)-->
!?!../Documents/104_LIS.json:1000,500!?!

One could traverse the tree either by Depth-First Search (DFS) strategy or Breadth-First Search (BFS) strategy.
For this problem, either way would do.
Here we demonstrate a solution that is implemented with the **DFS** strategy and **recursion**.

<iframe src="https://leetcode.com/playground/oZkJPDBk/shared" frameBorder="0" width="100%" height="259" name="oZkJPDBk"></iframe>

**Complexity analysis**

* Time complexity : we visit each node exactly once, 
thus the time complexity is $$\mathcal{O}(N)$$,
where $$N$$ is the number of nodes.

* Space complexity : in the worst case, the tree is completely unbalanced,
*e.g.* each node has only left child node, the recursion call would occur
 $$N$$ times (the height of the tree), 
 therefore the storage to keep the call stack would be $$\mathcal{O}(N)$$.
 But in the best case (the tree is completely balanced), 
 the height of the tree would be $$\log(N)$$.
 Therefore, the space complexity in this case would be $$\mathcal{O}(\log(N))$$.
<br />
<br />

---
#### Approach 2: Tail Recursion + BFS

One might have noticed that the above recursion solution is probably not the most optimal one in terms of the space complexity, and in the extreme case the overhead of call stack might even lead to *stack overflow*.

To address the issue, one can tweak the solution a bit to make it **tail recursion**, which is a specific form of recursion where the recursive call is the last action in the function. 

The benefit of having tail recursion, is that for certain programming languages (*e.g.* `C++`) the compiler could optimize the memory allocation of call stack by reusing the same space for every recursive call, rather than creating the space for each one. As a result, one could obtain the constant space complexity $$\mathcal{O}(1)$$ for the overhead of the recursive calls.

Here is a sample solution. Note that the optimization of tail recursion is not supported by Python or Java.

<iframe src="https://leetcode.com/playground/EUN4sJ4X/shared" frameBorder="0" width="100%" height="500" name="EUN4sJ4X"></iframe>


**Complexity analysis**

* Time complexity : $$\mathcal{O}(N)$$, still we visit each node once and only once.

* Space complexity : $$\mathcal{O}(2^{(log_2N-1)})=\mathcal{O}(N/2)=\mathcal{O}(N)$$, *i.e.* the maximum number of nodes at the same level (the number of leaf nodes in a full binary tree), since we traverse the tree in the **BFS** manner.
 
As one can see, this probably is not the best example to apply the *tail recursion* technique. Because though we did gain the constant space complexity for the recursive calls, we pay the price of $$\mathcal{O}(N)$$ complexity to maintain the state information for recursive calls. This defeats the purpose of applying tail recursion.

However, we would like to stress on the point that tail recursion is a useful form of recursion that could eliminate the space overhead incurred by the recursive function calls.

*Note: a function cannot be tail recursion if there are multiple occurrences of recursive calls in the function, even if the last action is the recursive call.* Because the system has to maintain the function call stack for the sub-function calls that occur within the same function. 
<br />
<br />

---
#### Approach 3: Iteration

**Intuition**

We could also convert the above recursion into iteration, with the help of the *stack* data structure.
Similar with the behaviors of the function call stack, the stack data structure follows the pattern of FILO 
(First-In-Last-Out), *i.e.* the last element that is added to a stack would come out first.

With the help of the *stack* data structure, one could mimic the behaviors of function call stack that is involved in the recursion, to convert a recursive function to a function with iteration.

**Algorithm**

>The idea is to keep the next nodes to visit in a stack.
Due to the FILO behavior of stack, one would get the order of visit same as the one in recursion.

We start from a stack which contains the root node and the corresponding depth which is ```1```.
Then we proceed to the iterations: pop the current node out of the stack and push the child nodes. The depth is updated at each step. 

<iframe src="https://leetcode.com/playground/UL6Txuee/shared" frameBorder="0" width="100%" height="497" name="UL6Txuee"></iframe>  

**Complexity analysis**

* Time complexity : $$\mathcal{O}(N)$$.

* Space complexity : in the worst case, the tree is completely unbalanced,
*e.g.* each node has only left child node, the recursion call would occur
 $$N$$ times (the height of the tree), 
 therefore the storage to keep the call stack would be $$\mathcal{O}(N)$$.
 But in the average case (the tree is balanced), 
 the height of the tree would be $$\log(N)$$.
 Therefore, the space complexity in this case would be $$\mathcal{O}(\log(N))$$.

## Accepted Submission (python3)
```python3
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def maxDepth(self, root: TreeNode) -> int:
        if not root:
            return 0
        return max(self.maxDepth(root.left), self.maxDepth(root.right)) + 1
```

## Top Discussions
### Simple solution using Java
- Author: ray050899
- Creation Date: Thu Oct 16 2014 05:10:15 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 02:10:26 GMT+0800 (Singapore Standard Time)

<p>
if the node does not exist, simply return 0. Otherwise, return the 1+the longer distance of its subtree.

    public int maxDepth(TreeNode root) {
            if(root==null){
                return 0;
            }
            return 1+Math.max(maxDepth(root.left),maxDepth(root.right));
        }
</p>


### Can leetcode share top performing solution(s) of problems for each supported language ?
- Author: rainhacker
- Creation Date: Mon Jan 12 2015 02:32:38 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 10:04:55 GMT+0800 (Singapore Standard Time)

<p>
After a solution is accepted it would be very helpful to know how to make it run faster looking at better performing solution(s).
</p>


### My code of C++, Depth-first-search and Breadth-first-search
- Author: makuiyu
- Creation Date: Tue Mar 17 2015 10:08:36 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 21:48:05 GMT+0800 (Singapore Standard Time)

<p>
1. Depth-first-search
======

Only one line code.

    int maxDepth(TreeNode *root)
    {
        return root == NULL ? 0 : max(maxDepth(root -> left), maxDepth(root -> right)) + 1;
    }

2. Breadth-first-search
======

Calculate the count of the last level.

    int maxDepth(TreeNode *root)
    {
        if(root == NULL)
            return 0;
        
        int res = 0;
        queue<TreeNode *> q;
        q.push(root);
        while(!q.empty())
        {
            ++ res;
            for(int i = 0, n = q.size(); i < n; ++ i)
            {
                TreeNode *p = q.front();
                q.pop();
                
                if(p -> left != NULL)
                    q.push(p -> left);
                if(p -> right != NULL)
                    q.push(p -> right);
            }
        }
        
        return res;
    }
</p>


