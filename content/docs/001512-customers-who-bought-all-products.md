---
title: "Customers Who Bought All Products"
weight: 1512
#id: "customers-who-bought-all-products"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Customer</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| customer_id | int     |
| product_key | int     |
+-------------+---------+
product_key is a foreign key to <code>Product</code> table.
</pre>

<p>Table:&nbsp;<code>Product</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| product_key | int     |
+-------------+---------+
product_key is the primary key column for this table.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query for a report that provides the customer ids from the&nbsp;<code>Customer</code>&nbsp;table&nbsp;that bought all the products in the <code>Product</code>&nbsp;table.</p>

<p>For example:</p>

<pre>
Customer table:
+-------------+-------------+
| customer_id | product_key |
+-------------+-------------+
| 1           | 5           |
| 2           | 6           |
| 3           | 5           |
| 3           | 6           |
| 1           | 6           |
+-------------+-------------+

Product table:
+-------------+
| product_key |
+-------------+
| 5           |
| 6           |
+-------------+

Result table:
+-------------+
| customer_id |
+-------------+
| 1           |
| 3           |
+-------------+
The customers who bought all the products (5 and 6) are customers with id 1 and 3.
</pre>

</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL subquery
- Author: ms25
- Creation Date: Sun May 19 2019 06:14:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 19 2019 06:14:13 GMT+0800 (Singapore Standard Time)

<p>
```
select customer_id
from customer c
group by customer_id
having count(distinct product_key)=(select count(distinct product_key) from product)


```
</p>


### My SQL solution considering if the customer buy things not in table 2
- Author: kirakira1992
- Creation Date: Mon May 20 2019 02:03:23 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 20 2019 02:03:23 GMT+0800 (Singapore Standard Time)

<p>
---The other solutions only count the numbers and check if the same, but if the customer buys product from not the second table, that solution is incorrect.

select customer_id
from
(
select c.customer_id,p.product_key
from
customer c inner join product p
on c.product_key = p.product_key

) c_p
group by customer_id
having count(distinct product_key) = 
(select count(distinct product_key) from product)
</p>


### Simple MySQL Solution
- Author: kothavade
- Creation Date: Sun May 19 2019 02:35:44 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 07 2019 06:18:52 GMT+0800 (Singapore Standard Time)

<p>
Simple join and group by, followed by selecting customers whose unique(distinct) product bought count is equal to total number of products in product table.

```
select customer_id
from Customer 
group by customer_id
having count(distinct(product_key)) = (select count(product_key) from Product)
```
</p>


