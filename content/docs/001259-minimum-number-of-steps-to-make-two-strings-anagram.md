---
title: "Minimum Number of Steps to Make Two Strings Anagram"
weight: 1259
#id: "minimum-number-of-steps-to-make-two-strings-anagram"
---
## Description
<div class="description">
<p>Given two equal-size strings <code>s</code> and <code>t</code>. In one step you can choose <strong>any character</strong> of <code>t</code> and replace it with <strong>another character</strong>.</p>

<p>Return <em>the minimum number of steps</em> to make <code>t</code>&nbsp;an anagram of <code>s</code>.</p>

<p>An&nbsp;<strong>Anagram</strong>&nbsp;of a&nbsp;string&nbsp;is a string that contains the same characters with a different (or the same) ordering.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;bab&quot;, t = &quot;aba&quot;
<strong>Output:</strong> 1
<strong>Explanation:</strong> Replace the first &#39;a&#39; in t with b, t = &quot;bba&quot; which is anagram of s.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;leetcode&quot;, t = &quot;practice&quot;
<strong>Output:</strong> 5
<strong>Explanation:</strong> Replace &#39;p&#39;, &#39;r&#39;, &#39;a&#39;, &#39;i&#39; and &#39;c&#39; from t with proper characters to make t anagram of s.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;anagram&quot;, t = &quot;mangaar&quot;
<strong>Output:</strong> 0
<strong>Explanation:</strong> &quot;anagram&quot; and &quot;mangaar&quot; are anagrams. 
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;xxyyzz&quot;, t = &quot;xxyyzz&quot;
<strong>Output:</strong> 0
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;friend&quot;, t = &quot;family&quot;
<strong>Output:</strong> 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 50000</code></li>
	<li><code>s.length == t.length</code></li>
	<li><code>s</code> and <code>t</code> contain lower-case English letters only.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Bloomberg - 3 (taggedByAdmin: false)
- Citrix - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Count occurrences and sum the difference w/ analysis.
- Author: rock
- Creation Date: Sun Feb 09 2020 12:02:41 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Feb 11 2020 20:06:14 GMT+0800 (Singapore Standard Time)

<p>
```java
    public int minSteps(String s, String t) {
        int[] count = new int[26];
        for (int i = 0; i < s.length(); ++i) {
            ++count[s.charAt(i) - \'a\']; // count the occurrences of chars in s.
            --count[t.charAt(i) - \'a\']; // compute the difference between s and t.
        }
        return Arrays.stream(count).map(Math::abs).sum() / 2; // sum the absolute of difference and divide it by 2.
    }
```
```python
    def minSteps(self, s: str, t: str) -> int:
        cnt1, cnt2 = map(collections.Counter, (s, t))
        return sum(abs(cnt1[c] - cnt2[c]) for c in string.ascii_lowercase) // 2
```
or simpler:
```python
    def minSteps(self, s: str, t: str) -> int:
        cnt1, cnt2 = map(collections.Counter, (s, t))
        return sum((cnt1 - cnt2).values())
```
**Analysis:**
Time: O(n), space: O(1), where n = s.length() + t.length().
</p>


### [Java] Maintain an array to record the occurrence of characters
- Author: manrajsingh007
- Creation Date: Sun Feb 09 2020 12:03:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 09 2020 12:03:21 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int minSteps(String s, String t) {
        int n = s.length(), ans = 0;
        int[] arr = new int[26];
        for(int i = 0; i < n; i++) {
            arr[s.charAt(i) - \'a\']++;
            arr[t.charAt(i) - \'a\']--;
        }
        for(int i = 0; i < 26; i++) if(arr[i] > 0) ans += arr[i];
        return ans;
    }
}
</p>


### Precise [C++] solution using map
- Author: soumyagarg
- Creation Date: Sun Feb 09 2020 13:34:19 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Feb 11 2020 02:23:31 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    int minSteps(string s, string t) {
        unordered_map<char,int>m;
        int sum=0;
        for(auto i:s)
            m[i]++;
        for(auto i:t)
            m[i]--;
        for(auto i:m){
            if(i.second<0)
                sum += i.second;
        }
        return abs(sum);
    }
};
```
</p>


