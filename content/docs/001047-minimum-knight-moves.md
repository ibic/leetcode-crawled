---
title: "Minimum Knight Moves"
weight: 1047
#id: "minimum-knight-moves"
---
## Description
<div class="description">
<p>In an <strong>infinite</strong> chess board with coordinates from <code>-infinity</code>&nbsp;to <code>+infinity</code>, you have a <strong>knight</strong> at square&nbsp;<code>[0, 0]</code>.</p>

<p>A&nbsp;knight has 8 possible moves it can make, as illustrated below. Each move is two squares in a cardinal direction, then one square in an orthogonal direction.</p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/12/knight.png" style="height: 200px; width: 200px;" /></p>

<p>Return the&nbsp;minimum number of steps needed to move the knight to the square <code>[x, y]</code>.&nbsp; It is guaranteed the answer exists.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> x = 2, y = 1
<strong>Output:</strong> 1
<strong>Explanation: </strong>[0, 0] &rarr; [2, 1]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> x = 5, y = 5
<strong>Output:</strong> 4
<strong>Explanation: </strong>[0, 0] &rarr; [2, 1] &rarr; [4, 2] &rarr; [3, 4] &rarr; [5, 5]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>|x| + |y| &lt;= 300</code></li>
</ul>

</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies
- Facebook - 10 (taggedByAdmin: false)
- Mathworks - 6 (taggedByAdmin: false)
- Amazon - 5 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: true)
- Indeed - 2 (taggedByAdmin: false)
- Qualtrics - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python3] Clear, short and easy DP solution, No Need for BFS or math
- Author: richardCorona
- Creation Date: Sun Sep 22 2019 01:20:17 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 23 2019 23:19:11 GMT+0800 (Singapore Standard Time)

<p>
The key observation is that we do not need to distinguish x and y, and we don\'t care whether x and y are positive or negative at all.

I believe it\'s easier than BFS or math solutions
```
from functools import lru_cache
class Solution:
    def minKnightMoves(self, x: int, y: int) -> int:
        @lru_cache(None) 
        def DP(x,y):
            if x + y == 0:
                return 0
            elif x + y == 2:
                return 2
            return min(DP(abs(x-1),abs(y-2)),DP(abs(x-2),abs(y-1)))+1
        return DP(abs(x),abs(y))
```

Thanks @kuznecpl for pointing out that we need to take care of the (1,1) and (2,0) cases properly.

</p>


### Here is how I get the formula (with graphs)
- Author: thinkersdream
- Creation Date: Sat Sep 28 2019 08:43:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 28 2019 08:52:08 GMT+0800 (Singapore Standard Time)

<p>
I manually draw this graph, although it is a lot of work and is perhaps too crazy for an intervew:
![image](https://assets.leetcode.com/users/thinkersdream/image_1569631925.png)

Since it is symmetric vertically, horizontally and diagonally, we can format x and y in the following way, so that every target node falls into the region enclosesd by the black lines:

```
x = Math.abs(x);
y = Math.abs(y);
if (x < y) {
    int tmp = x;
    x = y;
    y = tmp;
}
```

Then for all the white blocks, we can just hardcode their values, because they are sort of not regular.
```
......
private static int[][] localRegion = {
        {0, 3, 2},
        {3, 2, 1},
        {2, 1, 4}
};

......
if (x <= 2 && y <= 2)
    return localRegion[x][y];
```

For the colored blocks, they are apparently grouped and each group shares a same pattern. 

Lets say that the yellow blocks are Group 2, and the green blocks are Group 3, then the blocks in Group T has values T and (T + 1) interpolated. 

For a block (x, y),  its value should be:
```
groupId + ((x + y + groupId) % 2)
```

Then the only problem remained is how to determine the Group id of a block. The ways of computing Group id are actually different for the following two regions:

![image](https://assets.leetcode.com/users/thinkersdream/image_1569631335.png)

In the vertical region, you move a blockward right two times to enter a new group, so the group id should be this (notice that Group T should have blocks with value T and (T + 1)): 
```
groupId = (x - 1) / 2 + 1;
```
In the diagonal region, you move a block three times either upward or rightward , then you enter a new block, so the group id should be:
```
groupId = (x + y - 2) / 3 + 1;
```

That grey line has a slope 0.5 and pass through the block (3, 3), so to distinguish two regions,  we do:
```
int groupId;
if ((x - 3) >= (y - 3) * 2)
    groupId = (x - 1) / 2 + 1;
else 
    groupId = (x + y - 2) / 3 + 1;
```

Then we get the final code:
```
class Solution {
    private static int[][] localRegion = {
        {0, 3, 2},
        {3, 2, 1},
        {2, 1, 4}
    };
    
    public int minKnightMoves(int x, int y) {
        x = Math.abs(x);
        y = Math.abs(y);
        if (x < y) {
            int tmp = x;
            x = y;
            y = tmp;
        }
        if (x <= 2 && y <= 2)
            return localRegion[x][y];
        
        int groupId;
        if ((x - 3) >= (y - 3) * 2)
            groupId = (x - 1) / 2 + 1;
        else 
            groupId = (x + y - 2) / 3 + 1;
        
        return groupId + ((x + y + groupId) % 2);
    }
}
```

</p>


### Clean Java BFS solution
- Author: iammajian
- Creation Date: Thu Oct 10 2019 13:25:34 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 10 2019 13:25:34 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    private final int[][] DIRECTIONS = new int[][] {{2, 1}, {1, 2}, {-1, 2}, {-2, 1}, {-2, -1}, {-1, -2}, {1, -2}, {2, -1}};
    
    public int minKnightMoves(int x, int y) {
        x = Math.abs(x);
        y = Math.abs(y);
        
        Queue<int[]> queue = new LinkedList<>();
        queue.add(new int[] {0, 0});
        
        Set<String> visited = new HashSet<>();
        visited.add("0,0");
        
        int result = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                int[] cur = queue.remove();
                int curX = cur[0];
                int curY = cur[1];
                if (curX == x && curY == y) {
                    return result;
                }
                
                for (int[] d : DIRECTIONS) {
                    int newX = curX + d[0];
                    int newY = curY + d[1];
                    if (!visited.contains(newX + "," + newY) && newX >= -1 && newY >= -1) {
                        queue.add(new int[] {newX, newY});
                        visited.add(newX + "," + newY);
                    }
                }
            }
            result++;
        }
        return -1;
    }
}
```
</p>


