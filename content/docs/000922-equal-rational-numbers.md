---
title: "Equal Rational Numbers"
weight: 922
#id: "equal-rational-numbers"
---
## Description
<div class="description">
<p>Given two strings <code>S</code> and <code>T</code>, each of which represents a non-negative rational number, return <strong>True</strong> if and only if they represent the same number. The strings may use parentheses to denote the repeating part of the rational number.</p>

<p>In general a rational number can be represented using up to&nbsp;three parts: an&nbsp;<em>integer part</em>, a&nbsp;<em>non-repeating part,</em> and a&nbsp;<em>repeating part</em>. The number will be represented&nbsp;in one of the following three ways:</p>

<ul>
	<li><code>&lt;IntegerPart&gt;</code> (e.g. 0, 12, 123)</li>
	<li><code>&lt;IntegerPart&gt;&lt;.&gt;&lt;NonRepeatingPart&gt;</code> &nbsp;(e.g. 0.5, 1., 2.12, 2.0001)</li>
	<li><code>&lt;IntegerPart&gt;&lt;.&gt;&lt;NonRepeatingPart&gt;&lt;(&gt;&lt;RepeatingPart&gt;&lt;)&gt;</code> (e.g. 0.1(6), 0.9(9), 0.00(1212))</li>
</ul>

<p>The repeating portion of a decimal expansion is conventionally denoted within a pair of round brackets.&nbsp; For example:</p>

<p>1 / 6 = 0.16666666... = 0.1(6) = 0.1666(6) = 0.166(66)</p>

<p>Both 0.1(6) or 0.1666(6) or 0.166(66) are correct representations of 1 / 6.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-1-1">&quot;0.(52)&quot;</span>, T = <span id="example-input-1-2">&quot;0.5(25)&quot;</span>
<strong>Output: </strong><span id="example-output-1">true</span>
<strong>Explanation:
</strong>Because &quot;0.(52)&quot; represents 0.52525252..., and &quot;0.5(25)&quot; represents 0.52525252525..... , the strings represent the same number.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-2-1">&quot;0.1666(6)&quot;</span>, T = <span id="example-input-2-2">&quot;0.166(66)&quot;</span>
<strong>Output: </strong><span id="example-output-2">true</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>S = <span id="example-input-3-1">&quot;0.9(9)&quot;</span>, T = <span id="example-input-3-2">&quot;1.&quot;</span>
<strong>Output: </strong><span id="example-output-3">true</span>
<strong>Explanation: </strong>
&quot;0.9(9)&quot; represents 0.999999999... repeated forever, which equals 1.  [<a href="https://en.wikipedia.org/wiki/0.999..." target="_blank">See this link for an explanation.</a>]
&quot;1.&quot; represents the number 1, which is formed correctly: (IntegerPart) = &quot;1&quot; and (NonRepeatingPart) = &quot;&quot;.</pre>

<p>&nbsp;</p>
</div>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li>Each part consists only of digits.</li>
	<li>The <code>&lt;IntegerPart&gt;</code>&nbsp;will&nbsp;not begin with 2 or more zeros.&nbsp; (There is no other restriction on the digits of each part.)</li>
	<li><code>1 &lt;= &lt;IntegerPart&gt;.length &lt;= 4 </code></li>
	<li><code>0 &lt;= &lt;NonRepeatingPart&gt;.length &lt;= 4 </code></li>
	<li><code>1 &lt;= &lt;RepeatingPart&gt;.length &lt;= 4</code></li>
</ol>

</div>

## Tags
- Math (math)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Fraction Class

**Intuition**

As both numbers represent a fraction, we need a fraction class to handle fractions.  It should help us add two fractions together, keeping the answer in lowest terms.

**Algorithm**

We need to make sense of the fraction we are given.  The hard part is the repeating part.

Say we have a string like `S = "0.(12)"`.  It represents (for $$r = \frac{1}{100}$$):

$$
S = \frac{12}{100} + \frac{12}{10000} + \frac{12}{10^6} + \frac{12}{10^8} + \frac{12}{10^{10}} + \cdots
$$

$$
S = 12 * (r + r^2 + r^3 + \cdots)
$$

$$
S = 12 * \frac{r}{1-r}
$$

as the sum $$(r + r^2 + r^3 + \cdots)$$ is a geometric sum.

In general, for a repeating part $$x$$ with length $$k$$, we have $$r = 10^{-k}$$ and the contribution is $$\frac{xr}{1-r}$$.

The other two parts are easier, as it is just a literal interpretation of the value.

<iframe src="https://leetcode.com/playground/SoWcF93R/shared" frameBorder="0" width="100%" height="500" name="SoWcF93R"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(1)$$, if we take the length of $$S, T$$ as $$O(1)$$.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy Cheat
- Author: lee215
- Creation Date: Sun Jan 06 2019 12:06:16 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 13 2020 00:13:19 GMT+0800 (Singapore Standard Time)

<p>
Transform string to double

**Java:**
```
    public boolean isRationalEqual(String S, String T) {
        return f(S) == f(T);
    }

    public double f(String S) {
        int i = S.indexOf(\'(\');
        if (i > 0) {
            String base = S.substring(0, i);
            String rep = S.substring(i + 1, S.length() - 1);
            for (int j = 0; j < 20; ++j) base += rep;
            return Double.valueOf(base);
        }
        return Double.valueOf(S);
    }
```
**C++:**
```
    bool isRationalEqual(string S, string T) {
        return f(S) == f(T);
    }

    double f(string S) {
        auto i = S.find("(");
        if (i != string::npos) {
            string base = S.substr(0, i);
            string rep = S.substr(i + 1, S.length() - i - 2);
            for (int j = 0; j < 20; ++j) base += rep;
            return stod(base);
        }
        return stod(S);
    }
```

**Python:**
```
    def isRationalEqual(self, S, T):
        def f(s):
            i = s.find(\'(\')
            if i >= 0:
                s = s[:i] + s[i + 1:-1] * 20
            return float(s[:20])
        return f(S) == f(T)
```

</p>


### [Java] Math explained
- Author: 2499370956
- Creation Date: Sun Jan 06 2019 12:06:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 06 2019 12:06:55 GMT+0800 (Singapore Standard Time)

<p>
For 0 < q < 1, 1 + q + q^2 + ... = 1 / (1 - q)
Link: https://en.wikipedia.org/wiki/Geometric_progression#Infinite_geometric_series

0.(52) = 0 + 0.52 * (1 + 0.01 + 0.0001 + ...) = 0 + 0.52 / (1 - 0.01)
0.5(25) = 0.5 + 0.025 * (1 + 0.01 + 0.0001 + ...) = 0.5 + 0.025 / (1 - 0.01)

```
class Solution {

    private List<Double> ratios = Arrays.asList(1.0, 1.0 / 9, 1.0 / 99, 1.0 / 999, 1.0 / 9999);

    public boolean isRationalEqual(String S, String T) {
        return Math.abs(computeValue(S) - computeValue(T)) < 1e-9;
    }

    private double computeValue(String s) {
        if (!s.contains("(")) {
            return Double.valueOf(s);
        } else {
            double intNonRepeatingValue = Double.valueOf(s.substring(0, s.indexOf(\'(\')));
            int nonRepeatingLength = s.indexOf(\'(\') - s.indexOf(\'.\') - 1;
            int repeatingLength = s.indexOf(\')\') - s.indexOf(\'(\') - 1;
            int repeatingValue = Integer.parseInt(s.substring(s.indexOf(\'(\') + 1, s.indexOf(\')\')));
            return intNonRepeatingValue + repeatingValue * Math.pow(0.1, nonRepeatingLength) * ratios.get(repeatingLength);
        }
    }
}
```

</p>


### ruby 3 lines solution
- Author: pjincz
- Creation Date: Mon Jan 07 2019 07:36:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 07 2019 07:36:31 GMT+0800 (Singapore Standard Time)

<p>
I\'m solve this problem via C++ on contest.
Why I post ruby here, I just want to say, this problem is so unfair to C++ participant.

Just split number to 3 parts: a.b(c)
We known: x = a + b / (10^len(b)) + c / (10^len(c) - 1) / (10^len(b))
Ruby has built in regex support, (lots of languages too), so it\'s easy to split string.
And Ruby has built in rational number support, (python too).
But as a C++ participant, I need to implement them all by hand.

```ruby
def is_rational_equal(s, t)
    parse_num(s) == parse_num(t)
end

def parse_num(x)
    x =~ /(\d*)\.?(\d*)\(?(\d*)\)?/
    $1.to_r + $2.to_r / (10 ** $2.length) + ($3.length > 0 ? $3.to_r / (10 ** $3.length - 1) / (10 ** $2.length) : 0)
end
```
</p>


