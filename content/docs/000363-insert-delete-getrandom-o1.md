---
title: "Insert Delete GetRandom O(1)"
weight: 363
#id: "insert-delete-getrandom-o1"
---
## Description
<div class="description">
<p>Implement the&nbsp;<code>RandomizedSet</code> class:</p>

<ul>
	<li><code>bool insert(int val)</code> Inserts an item <code>val</code> into the set if not present. Returns <code>true</code> if the item was not present, <code>false</code> otherwise.</li>
	<li><code>bool remove(int val)</code> Removes an item <code>val</code> from the set if present. Returns <code>true</code> if the item was present, <code>false</code> otherwise.</li>
	<li><code>int getRandom()</code> Returns a random element from the current set of elements (it&#39;s guaranteed that at least one element exists when this method is called). Each element must have the <b>same probability</b> of being returned.</li>
</ul>

<p><strong>Follow up:</strong> Could you implement the functions of the class with each function works in <strong>average</strong> <code>O(1)</code> time?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;RandomizedSet&quot;, &quot;insert&quot;, &quot;remove&quot;, &quot;insert&quot;, &quot;getRandom&quot;, &quot;remove&quot;, &quot;insert&quot;, &quot;getRandom&quot;]
[[], [1], [2], [2], [], [1], [2], []]
<strong>Output</strong>
[null, true, false, true, 2, true, false, 2]

<strong>Explanation</strong>
RandomizedSet randomizedSet = new RandomizedSet();
randomizedSet.insert(1); // Inserts 1 to the set. Returns true as 1 was inserted successfully.
randomizedSet.remove(2); // Returns false as 2 does not exist in the set.
randomizedSet.insert(2); // Inserts 2 to the set, returns true. Set now contains [1,2].
randomizedSet.getRandom(); // getRandom() should return either 1 or 2 randomly.
randomizedSet.remove(1); // Removes 1 from the set, returns true. Set now contains [2].
randomizedSet.insert(2); // 2 was already in the set, so return false.
randomizedSet.getRandom(); // Since 2 is the only number in the set, getRandom() will always return 2.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>-2<sup>31</sup> &lt;= val &lt;= 2<sup>31</sup> - 1</code></li>
	<li>At most <code>10<sup>5</sup></code> calls will be made to <code>insert</code>, <code>remove</code>, and <code>getRandom</code>.</li>
	<li>There will be <strong>at least one</strong> element in the data structure when <code>getRandom</code>&nbsp;is called.</li>
</ul>

</div>

## Tags
- Array (array)
- Hash Table (hash-table)
- Design (design)

## Companies
- Facebook - 16 (taggedByAdmin: true)
- Amazon - 16 (taggedByAdmin: true)
- Bloomberg - 13 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: true)
- Microsoft - 5 (taggedByAdmin: false)
- Twitter - 5 (taggedByAdmin: true)
- Affirm - 5 (taggedByAdmin: false)
- Databricks - 4 (taggedByAdmin: false)
- Yandex - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Quora - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- AppDynamics - 2 (taggedByAdmin: false)
- LinkedIn - 14 (taggedByAdmin: false)
- Uber - 5 (taggedByAdmin: true)
- Two Sigma - 2 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Walmart Labs - 4 (taggedByAdmin: false)
- VMware - 4 (taggedByAdmin: false)
- Indeed - 4 (taggedByAdmin: false)
- Goldman Sachs - 3 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- Pure Storage - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Twilio - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Yelp - 0 (taggedByAdmin: true)
- Pocket Gems - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

We're asked to implement the structure 
which provides the following operations in _average_ $$\mathcal{O}(1)$$ time:

- Insert 

- Delete

- GetRandom

First of all - why this weird combination? 
The structure looks quite theoretical, 
but it's widely used in popular statistical algorithms like 
[Markov chain Monte Carlo](https://en.wikipedia.org/wiki/Markov_chain_Monte_Carlo) and 
[Metropolis–Hastings algorithm](https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm). 
These algorithms are for sampling from a probability distribution 
when it's difficult to compute the distribution itself. 

Let's figure out how to implement such a structure.
Starting from the Insert, we immediately have two good candidates with $$\mathcal{O}(1)$$
[average insert time](https://wiki.python.org/moin/TimeComplexity):

- Hashmap (or Hashset, the implementation is very similar): [Java HashMap](https://docs.oracle.com/javase/8/docs/api/java/util/HashMap.html) / [Python dictionary](https://docs.python.org/3/tutorial/datastructures.html#dictionaries)

- Array List: [Java ArrayList](https://docs.oracle.com/javase/8/docs/api/java/util/LinkedList.html) / [Python list](https://docs.python.org/3/tutorial/datastructures.html)

Let's consider them one by one. 

> Hashmap provides Insert and Delete in 
average constant time, although has problems with GetRandom. 

The idea of GetRandom is to choose a random index and then to retrieve an
element with that index. There is no indexes in hashmap, and hence
to get true random value, one has first to convert hashmap keys in a list,
that would take linear time. The solution here is to build a list of 
keys aside and to use this list to compute GetRandom in constant time.

> Array List has indexes and could provide Insert and GetRandom in 
average constant time, though has problems with Delete. 

To delete a value at arbitrary index takes linear time. 
The solution here is to always delete the last value:

- Swap the element to delete with the last one.

- Pop the last element out.

For that, one has to compute an index of each element in constant time, and hence
needs a hashmap which stores `element -> its index` dictionary.

Both ways converge into the same combination of data structures:

- Hashmap `element -> its index`.

- Array List of elements.

![fig](../Figures/380/structure2.png)
<br /> 
<br />


---
#### Approach 1: HashMap + ArrayList

**Insert**

- Add value -> its index into dictionary, average $$\mathcal{O}(1)$$ time.

- Append value to array list, average $$\mathcal{O}(1)$$ time as well.

![fig](../Figures/380/isert.png)

<iframe src="https://leetcode.com/playground/NBz6A7pX/shared" frameBorder="0" width="100%" height="208" name="NBz6A7pX"></iframe>

**Delete**

- Retrieve an index of element to delete from the hashmap.

- Move the last element to the place of the element to delete, $$\mathcal{O}(1)$$ time.

- Pop the last element out, $$\mathcal{O}(1)$$ time.

![fig](../Figures/380/delete.png)

<iframe src="https://leetcode.com/playground/BrKuitZ7/shared" frameBorder="0" width="100%" height="293" name="BrKuitZ7"></iframe>

**GetRandom**

GetRandom could be implemented in $$\mathcal{O}(1)$$ time with the help of standard
`random.choice` in Python and `Random` object in Java.

<iframe src="https://leetcode.com/playground/jVvSDTEV/shared" frameBorder="0" width="100%" height="140" name="jVvSDTEV"></iframe>

**Implementation**

<iframe src="https://leetcode.com/playground/cMXhQw7r/shared" frameBorder="0" width="100%" height="500" name="cMXhQw7r"></iframe>

**Complexity Analysis**

* Time complexity. GetRandom is always $$\mathcal{O}(1)$$.
Insert and Delete both have $$\mathcal{O}(1)$$ average time complexity, 
and $$\mathcal{O}(N)$$ in the worst-case scenario 
when the operation exceeds the capacity of
currently allocated array/hashmap and invokes space reallocation.

* Space complexity: $$\mathcal{O}(N)$$, to store N elements.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution using a HashMap and an ArrayList along with a follow-up. (131 ms)
- Author: yubad2000
- Creation Date: Thu Aug 04 2016 11:51:01 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 14:15:04 GMT+0800 (Singapore Standard Time)

<p>
I got a similar question for my phone interview. The difference is that the duplicated number is allowed. So, think that is a follow-up of this question.
How do you modify your code to allow duplicated number?
 
```
public class RandomizedSet {
    ArrayList<Integer> nums;
    HashMap<Integer, Integer> locs;
    java.util.Random rand = new java.util.Random();
    /** Initialize your data structure here. */
    public RandomizedSet() {
        nums = new ArrayList<Integer>();
        locs = new HashMap<Integer, Integer>();
    }
    
    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    public boolean insert(int val) {
        boolean contain = locs.containsKey(val);
        if ( contain ) return false;
        locs.put( val, nums.size());
        nums.add(val);
        return true;
    }
    
    /** Removes a value from the set. Returns true if the set contained the specified element. */
    public boolean remove(int val) {
        boolean contain = locs.containsKey(val);
        if ( ! contain ) return false;
        int loc = locs.get(val);
        if (loc < nums.size() - 1 ) { // not the last one than swap the last one with this val
            int lastone = nums.get(nums.size() - 1 );
            nums.set( loc , lastone );
            locs.put(lastone, loc);
        }
        locs.remove(val);
        nums.remove(nums.size() - 1);
        return true;
    }
    
    /** Get a random element from the set. */
    public int getRandom() {
        return nums.get( rand.nextInt(nums.size()) );
    }
}
```
</p>


### Simple solution in Python
- Author: agave
- Creation Date: Fri Aug 05 2016 15:20:15 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 08:14:44 GMT+0800 (Singapore Standard Time)

<p>
We just keep track of the index of the added elements, so when we remove them, we copy the last one into it.

From Python docs (https://wiki.python.org/moin/TimeComplexity) we know that `list.append()` takes O(1), both average and amortized. Dictionary `get` and `set` functions take O(1) average, so we are OK.

```
import random

class RandomizedSet(object):

    def __init__(self):
        self.nums, self.pos = [], {}
        
    def insert(self, val):
        if val not in self.pos:
            self.nums.append(val)
            self.pos[val] = len(self.nums) - 1
            return True
        return False
        

    def remove(self, val):
        if val in self.pos:
            idx, last = self.pos[val], self.nums[-1]
            self.nums[idx], self.pos[last] = last, idx
            self.nums.pop(); self.pos.pop(val, 0)
            return True
        return False
            
    def getRandom(self):
        return self.nums[random.randint(0, len(self.nums) - 1)]

# 15 / 15 test cases passed.
# Status: Accepted
# Runtime: 144 ms
```
</p>


### AC C++ Solution. Unordered_map + Vector
- Author: r_w
- Creation Date: Fri Aug 05 2016 00:14:16 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 22:44:52 GMT+0800 (Singapore Standard Time)

<p>
```
class RandomizedSet {
public:
    /** Initialize your data structure here. */
    RandomizedSet() {
        
    }
    
    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    bool insert(int val) {
        if (m.find(val) != m.end()) return false;
        nums.emplace_back(val);
        m[val] = nums.size() - 1;
        return true;
    }
    
    /** Removes a value from the set. Returns true if the set contained the specified element. */
    bool remove(int val) {
        if (m.find(val) == m.end()) return false;
        int last = nums.back();
        m[last] = m[val];
        nums[m[val]] = last;
        nums.pop_back();
        m.erase(val);
        return true;
    }
    
    /** Get a random element from the set. */
    int getRandom() {
        return nums[rand() % nums.size()];
    }
private:
    vector<int> nums;
    unordered_map<int, int> m;
};

```
</p>


