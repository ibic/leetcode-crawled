---
title: "Closest Divisors"
weight: 1141
#id: "closest-divisors"
---
## Description
<div class="description">
<p>Given an integer <code>num</code>, find the closest two integers in absolute difference whose product equals&nbsp;<code>num + 1</code>&nbsp;or <code>num + 2</code>.</p>

<p>Return the two integers in any order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> num = 8
<strong>Output:</strong> [3,3]
<strong>Explanation:</strong> For num + 1 = 9, the closest divisors are 3 &amp; 3, for num + 2 = 10, the closest divisors are 2 &amp; 5, hence 3 &amp; 3 is chosen.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> num = 123
<strong>Output:</strong> [5,25]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> num = 999
<strong>Output:</strong> [40,25]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= num &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy and Concise
- Author: lee215
- Creation Date: Sun Feb 23 2020 12:05:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 22:39:26 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
Iterate `a` from `sqrt(x+2)` to `1`, and check:
if `(x + 1) % a == 0`, we directly return the pair `[a, (x + 1) / a]`.
if `(x + 2) % a == 0`, we directly return the pair `[a, (x + 2) / a]`.
The first valid pair we meet will have be the closet pair.
<br>

# Complexity
Time `O(sqrtX)`, Space `O(1)`
<br>

**Java**
```java
    public int[] closestDivisors(int x) {
        for (int a = (int)Math.sqrt(x + 2); a > 0; --a) {
            if ((x + 1) % a == 0)
                return new int[]{a, (x + 1) / a};
            if ((x + 2) % a == 0)
                return new int[]{a, (x + 2) / a};
        }
        return new int[]{};
    }
```
**C++**
```cpp
    vector<int> closestDivisors(int x) {
        for (int a = sqrt(x + 2); a > 0; --a) {
            if ((x + 1) % a == 0)
                return {a, (x + 1) / a};
            if ((x + 2) % a == 0)
                return {a, (x + 2) / a};
        }
        return {};
    }
```
**Python:**
```py
    def closestDivisors(self, x):
        for a in xrange(int((x + 2)**0.5), 0, -1):
            if (x + 1) % a == 0:
                return [a, (x + 1) / a]
            if (x + 2) % a == 0:
                return [a, (x + 2) / a]
```
**1-line for fun**
```py
    def closestDivisors(self, x):
        return next([a, y / a] for a in xrange(int((x + 2)**0.5), 0, -1) for y in [x + 1, x + 2] if not y % a)
```
</p>


### [C++] Smart Brute Force
- Author: PhoenixDD
- Creation Date: Sun Feb 23 2020 12:01:34 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Feb 26 2020 11:36:11 GMT+0800 (Singapore Standard Time)

<p>
**Observation**

Since the limit is 10^9 we can find closest 2 numbers who\'s product is either `num+1` or `num+2` by iterating over all numbers and finding the possible combination/factor.
The key is that we only need to traverse from `sqrt(num+2)` and `sqrt(num+1)` and check find the closest factor pair.

The reason for doing it from `sqrt` is that the factors start repeating in the reverse order after the square root.
eg: `100` 
The table produced is:

```
1*100    ^
2*50     |
4*25     |
5*20     |  (Increasing distance as we go up)
10*10 <- sqrt, we see repetitions after this point (Also notice this is the closest)
20*5 
25*4 
2*50 
100*1
```

**Solution**

```c++
class Solution {
public:
    vector<int> closestDivisors(int num) 
    {
        int num1=num+1,num2=num+2,dist=INT_MAX;
        vector<int> result;
        for(int i=sqrt(num2);i>=1;i--)
            if(num2%i==0)	//Check if `i` is a factor.
            {
                result={i,num2/i};
		dist=num2/i-i;
                break;
            }
        for(int i=sqrt(num1);i>=1;i--)
	    if(num1%i==0) //Check the combination if `i` is a factor.
            {
                if(num1/i-i<dist)    //Check if this combination is closer than current result.
                    result={i,num1/i};
                break;
            }
        return result;
    }
};
```

Check out python version by [cool_shark](https://leetcode.com/cool_shark/) down below in the comments, make sure you upvote the comment if you find it useful :)
</p>


### [C++/Java] Sqrt(n)
- Author: votrubac
- Creation Date: Sun Feb 23 2020 12:10:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 16:41:29 GMT+0800 (Singapore Standard Time)

<p>
For each number (`num + 1` and `num + 2`), we start with the square root as our divisor `d` to aim for the closest divisors.

We then check if the number is divisible. If yes - we return `d`, if no - we decrease `d` (until it reaches 1).

That way, the returned divisor is closest to the other divisor. We compare those divisors for `num + 1` and `num + 2`, and return the closest pair.
**C++**
```cpp
int largestDivisor(int num) {
    int d = sqrt(num);
    while (num % d != 0) --d;
    return d;
}
vector<int> closestDivisors(int num) {
    auto d1 = largestDivisor(num + 1), d2 = largestDivisor(num + 2);
    if (abs(d1 - (num + 1) / d1) < abs(d2 - (num + 2) / d2))
        return {d1, (num + 1) / d1};
    return { d2, (num + 2) / d2 };
}
```
**Java**
```java
int largestDivisor(int num) {
    int d = (int)Math.sqrt(num);
    while (num % d != 0) --d;
    return d;
}    
public int[] closestDivisors(int num) {
    int d1 = largestDivisor(num + 1), d2 = largestDivisor(num + 2);
    if (Math.abs(d1 - (num + 1) / d1) < Math.abs(d2 - (num + 2) / d2))
        return new int[] { d1, (num + 1) / d1 };        
    return new int[] { d2, (num + 2) / d2 };
}
```
</p>


