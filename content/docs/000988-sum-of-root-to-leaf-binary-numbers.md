---
title: "Sum of Root To Leaf Binary Numbers"
weight: 988
#id: "sum-of-root-to-leaf-binary-numbers"
---
## Description
<div class="description">
<p>You are given the <code>root</code> of a binary tree where each node has a value <code>0</code>&nbsp;or <code>1</code>.&nbsp; Each root-to-leaf path represents a binary number starting with the most significant bit.&nbsp; For example, if the path is <code>0 -&gt; 1 -&gt; 1 -&gt; 0 -&gt; 1</code>, then this could represent <code>01101</code> in binary, which is <code>13</code>.</p>

<p>For all leaves in the tree, consider the numbers represented by the path&nbsp;from the root to that leaf.</p>

<p>Return <em>the sum of these numbers</em>. The answer is <strong>guaranteed</strong> to fit in a <strong>32-bits</strong> integer.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/04/04/sum-of-root-to-leaf-binary-numbers.png" style="width: 450px; height: 296px;" />
<pre>
<strong>Input:</strong> root = [1,0,1,0,1,0,1]
<strong>Output:</strong> 22
<strong>Explanation: </strong>(100) + (101) + (110) + (111) = 4 + 5 + 6 + 7 = 22
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = [0]
<strong>Output:</strong> 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [1]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root = [1,1]
<strong>Output:</strong> 3
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is in the range <code>[1, 1000]</code>.</li>
	<li><code>Node.val</code> is <code>0</code> or <code>1</code>.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Overview

**Prerequisites: Bitwise Trick**

If you work with decimal representation, the conversion of `1->2` into `12`
is easy. You start from `curr_number = 1`, then shift one register to the 
left and add the next digit: `curr_number = 1 * 10 + 2 = 12`.

If you work with binaries `1 -> 1` -> `3`, it's the same.
You start from `curr_number = 1`, then shift one register to the 
left and add the next digit: `curr_number = (1 << 1) | 1 = 3`.

**Prerequisites: Tree Traversals**

There are three DFS ways to traverse the tree: preorder, postorder and inorder.
Please check two minutes picture explanation, if you don't remember them quite well:
[here is Python version](https://leetcode.com/problems/binary-tree-inorder-traversal/discuss/283746/all-dfs-traversals-preorder-inorder-postorder-in-python-in-1-line) 
and 
[here is Java version](https://leetcode.com/problems/binary-tree-inorder-traversal/discuss/328601/all-dfs-traversals-preorder-postorder-inorder-in-java-in-5-lines).

**Optimal Strategy to Solve the Problem**

> Root-to-left traversal is so-called _DFS preorder traversal_. To implement it,
one has to follow straightforward strategy Root->Left->Right. 

Since one has to visit all nodes, the best possible time complexity here is 
linear. Hence all interest here is to improve the space complexity. 

> There are 3 ways to implement preorder traversal: iterative,
recursive and Morris. 

Iterative and recursive approaches here do the job in one pass, 
but they both need up to $$\mathcal{O}(H)$$ space to keep the stack, 
where $$H$$ is a tree height.

Morris approach is two-pass approach, but it's a constant-space one.

![diff](../Figures/1022/preorder2.png)

<br />
<br />


---
#### Approach 1: Iterative Preorder Traversal.

**Intuition**

Here we implement standard iterative preorder traversal with the stack:

- Push root into stack.

- While stack is not empty:

    - Pop out a node from stack and update the current number.
    
    - If the node is a leaf, update root-to-leaf sum.
    
    - Push right and left child nodes into stack.
    
- Return root-to-leaf sum.  

!?!../Documents/1022_LIS.json:1000,344!?!

**Implementation**

Note, that 
[Javadocs recommends to use ArrayDeque, and not Stack as a stack implementation](https://docs.oracle.com/javase/8/docs/api/java/util/ArrayDeque.html).

<iframe src="https://leetcode.com/playground/kcgp3Xtq/shared" frameBorder="0" width="100%" height="480" name="kcgp3Xtq"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, where $$N$$ is a number of nodes, 
since one has to visit each node. 
    
* Space complexity: up to $$\mathcal{O}(H)$$ to keep the stack,
where $$H$$ is a tree height.  
<br />
<br />


---
#### Approach 2: Recursive Preorder Traversal.

Iterative approach 1 could be converted into recursive one.

Recursive preorder traversal is extremely simple: 
follow Root->Left->Right direction, i.e. do all the business with the node 
(= update the current number and root-to-leaf sum), 
and then do the recursive calls for the left and right child nodes.

P.S.
Here is the difference between _preorder_ and the other DFS recursive traversals. 
On the following figure the nodes are enumerated in the order you visit them, 
please follow `1-2-3-4-5` to compare different DFS strategies implemented as
recursion.

![diff](../Figures/1022/ddfs2.png)

**Implementation**

<iframe src="https://leetcode.com/playground/nYBwUtQZ/shared" frameBorder="0" width="100%" height="395" name="nYBwUtQZ"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, where $$N$$ is a number of nodes, 
since one has to visit each node. 
    
* Space complexity: up to $$\mathcal{O}(H)$$ to keep the recursion stack,
where $$H$$ is a tree height.  
<br />
<br />


---
#### Approach 3: Morris Preorder Traversal.

We discussed already iterative and recursive preorder traversals, 
which both have great time complexity though use up to 
$$\mathcal{O}(H)$$ to keep the stack. We could trade in performance to save space.

The idea of Morris preorder traversal is simple: 
to use no space but to traverse the tree.

> How that could be even possible? At each node one has to decide where to go:
to the left or to the right, traverse the left subtree or traverse the right subtree. 
How one could know that the left subtree is already done if no 
additional memory is allowed?  

The idea of [Morris](https://www.sciencedirect.com/science/article/pii/0020019079900681)
algorithm is to set the _temporary link_ between the node and its 
[predecessor](https://leetcode.com/articles/delete-node-in-a-bst/):
`predecessor.right = root`.
So one starts from the node, computes its predecessor and
verifies if the link is present.

- There is no link? Set it and go to the left subtree.

- There is a link? Break it and go to the right subtree.  

There is one small issue to deal with : what if there is no
left child, i.e. there is no left subtree? 
Then go straightforward to the right subtree.

**Implementation**

<iframe src="https://leetcode.com/playground/7sG5T6LM/shared" frameBorder="0" width="100%" height="500" name="7sG5T6LM"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, where $$N$$ is a number of nodes.
    
* Space complexity: $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Recursive Solution
- Author: lee215
- Creation Date: Sun Apr 07 2019 12:11:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 03 2020 15:39:22 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Easily decompose this problem into 2 sub-problem:
1. Find all path from root to leaves. DFS recursion should help do that.
2. Transform a path from base to base 10.

You can do this separately, and it will be a `O(N^2)` solution.
In my solution, I combine them together.


## **Explanation**:
We recursively pass the current value of path to the children.
If `root == null`, no value, return 0.
If `root != null`,
we double the value from its parent and add the node\'s value,
like the process of transforming base 2 to base 10.

In the end of recursion,
if `root.left == root.right == null`,
return the current `val`.
<br>

## **Complexity**:
Time `O(N)`
Space `O(H)` for recursion.
<br>

**Java:**
```
    public int sumRootToLeaf(TreeNode root) {
        return dfs(root, 0);
    }

    public int dfs(TreeNode root, int val) {
        if (root == null) return 0;
        val = val * 2 + root.val;
        return root.left == root.right ? val : dfs(root.left, val) + dfs(root.right, val);
    }
```

**C++:**
```
    int sumRootToLeaf(TreeNode* root, int val = 0) {
        if (!root) return 0;
        val = (val * 2 + root->val);
        return root->left == root->right ? val : sumRootToLeaf(root->left, val) + sumRootToLeaf(root->right, val));
    }
```

**Python:**
```
    def sumRootToLeaf(self, root, val=0):
        if not root: return 0
        val = val * 2 + root.val
        if root.left == root.right: return val
        return self.sumRootToLeaf(root.left, val) + self.sumRootToLeaf(root.right, val)
```

**Update 2019-04-08**
In the previous version of problem,
the length of path may exceed 32 and it will be a huge integer.
So it request to return the number mod by 10^9+7.
Now it notes that The "answer will not exceed 2^31 - 1."
Now it removes this condition to keep this problem as a easy one.
</p>


### [Java] Simple DFS
- Author: gcarrillo
- Creation Date: Mon Apr 08 2019 05:10:28 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 08 2019 05:10:28 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int sumRootToLeaf(TreeNode root) {
        dfs(root, 0);
        return ans;
    }
    
    int ans = 0;
    void dfs(TreeNode root, int val){
        if(root == null) return;
        val = val << 1 | root.val;
        if(root.left == null && root.right == null) ans += val;
        dfs(root.left, val);
        dfs(root.right, val);
    }
}
```
</p>


### Python iterative and recursive with comments
- Author: srinivas11789
- Creation Date: Sat Jul 20 2019 13:45:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 20 2019 13:45:20 GMT+0800 (Singapore Standard Time)

<p>
```python
# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def sumRootToLeaf(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        
        # Logic 1: Recursive traverse - 100 pass 63% faster
        
        # Collect all the path in binary in a list
        self.path_to_leaf = []
        # Track total binary sum and return result
        self.total_binary_sum = 0
        
        # Traverse tree function
        def traverse(node, path):
            # If not leaf node, record the path left or right
            if node.left:
                traverse(node.left, path+str(node.val))
            if node.right:
                traverse(node.right, path+str(node.val))
            
            # Leaf node detect and record path
            if not node.left and not node.right:
                path += str(node.val)
                self.path_to_leaf.append(path)
                self.total_binary_sum += int(path, 2)
        
        traverse(root, "")
        print(self.path_to_leaf)
        return self.total_binary_sum
```

```python
        # Logic 2: Iterative method - 100 pass 83 percent faster
        # * Each element of the list has the node and the path to it
        
        stack = [[root, ""]] # iteration to keep track
        result = 0 # result holder
        
        # Traverse nodes by stacking them
        while stack:
            # current node and path tracking
            current, path = stack.pop(0)
            
            # traverse with path 
            if current.left:
                stack.append([current.left, path+str(current.val)])
            if current.right:
                stack.append([current.right, path+str(current.val)])
            
            # leaf node condition when we update result
            if not current.left and not current.right:
                path += str(current.val)
                result += int(path, 2)
                
        return result
```
</p>


