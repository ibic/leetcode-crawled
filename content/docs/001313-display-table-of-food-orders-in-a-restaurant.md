---
title: "Display Table of Food Orders in a Restaurant"
weight: 1313
#id: "display-table-of-food-orders-in-a-restaurant"
---
## Description
<div class="description">
<p>Given&nbsp;the array <code>orders</code>, which represents the orders that customers have done in a restaurant. More specifically&nbsp;<code>orders[i]=[customerName<sub>i</sub>,tableNumber<sub>i</sub>,foodItem<sub>i</sub>]</code> where <code>customerName<sub>i</sub></code> is the name of the customer, <code>tableNumber<sub>i</sub></code>&nbsp;is the table customer sit at, and <code>foodItem<sub>i</sub></code>&nbsp;is the item customer orders.</p>

<p><em>Return the restaurant&#39;s &ldquo;<strong>display table</strong>&rdquo;</em>. The &ldquo;<strong>display table</strong>&rdquo; is a table whose row entries denote how many of each food item each table ordered. The first column is the table number and the remaining columns correspond to each food item in alphabetical order. The first row should be a header whose first column is &ldquo;Table&rdquo;, followed by the names of the food items. Note that the customer names are not part of the table. Additionally, the rows should be sorted in numerically increasing order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> orders = [[&quot;David&quot;,&quot;3&quot;,&quot;Ceviche&quot;],[&quot;Corina&quot;,&quot;10&quot;,&quot;Beef Burrito&quot;],[&quot;David&quot;,&quot;3&quot;,&quot;Fried Chicken&quot;],[&quot;Carla&quot;,&quot;5&quot;,&quot;Water&quot;],[&quot;Carla&quot;,&quot;5&quot;,&quot;Ceviche&quot;],[&quot;Rous&quot;,&quot;3&quot;,&quot;Ceviche&quot;]]
<strong>Output:</strong> [[&quot;Table&quot;,&quot;Beef Burrito&quot;,&quot;Ceviche&quot;,&quot;Fried Chicken&quot;,&quot;Water&quot;],[&quot;3&quot;,&quot;0&quot;,&quot;2&quot;,&quot;1&quot;,&quot;0&quot;],[&quot;5&quot;,&quot;0&quot;,&quot;1&quot;,&quot;0&quot;,&quot;1&quot;],[&quot;10&quot;,&quot;1&quot;,&quot;0&quot;,&quot;0&quot;,&quot;0&quot;]] 
<strong>Explanation:
</strong>The displaying table looks like:
<strong>Table,Beef Burrito,Ceviche,Fried Chicken,Water</strong>
3    ,0           ,2      ,1            ,0
5    ,0           ,1      ,0            ,1
10   ,1           ,0      ,0            ,0
For the table 3: David orders &quot;Ceviche&quot; and &quot;Fried Chicken&quot;, and Rous orders &quot;Ceviche&quot;.
For the table 5: Carla orders &quot;Water&quot; and &quot;Ceviche&quot;.
For the table 10: Corina orders &quot;Beef Burrito&quot;. 
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> orders = [[&quot;James&quot;,&quot;12&quot;,&quot;Fried Chicken&quot;],[&quot;Ratesh&quot;,&quot;12&quot;,&quot;Fried Chicken&quot;],[&quot;Amadeus&quot;,&quot;12&quot;,&quot;Fried Chicken&quot;],[&quot;Adam&quot;,&quot;1&quot;,&quot;Canadian Waffles&quot;],[&quot;Brianna&quot;,&quot;1&quot;,&quot;Canadian Waffles&quot;]]
<strong>Output:</strong> [[&quot;Table&quot;,&quot;Canadian Waffles&quot;,&quot;Fried Chicken&quot;],[&quot;1&quot;,&quot;2&quot;,&quot;0&quot;],[&quot;12&quot;,&quot;0&quot;,&quot;3&quot;]] 
<strong>Explanation:</strong> 
For the table 1: Adam and Brianna order &quot;Canadian Waffles&quot;.
For the table 12: James, Ratesh and Amadeus order &quot;Fried Chicken&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> orders = [[&quot;Laura&quot;,&quot;2&quot;,&quot;Bean Burrito&quot;],[&quot;Jhon&quot;,&quot;2&quot;,&quot;Beef Burrito&quot;],[&quot;Melissa&quot;,&quot;2&quot;,&quot;Soda&quot;]]
<strong>Output:</strong> [[&quot;Table&quot;,&quot;Bean Burrito&quot;,&quot;Beef Burrito&quot;,&quot;Soda&quot;],[&quot;2&quot;,&quot;1&quot;,&quot;1&quot;,&quot;1&quot;]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;=&nbsp;orders.length &lt;= 5 * 10^4</code></li>
	<li><code>orders[i].length == 3</code></li>
	<li><code>1 &lt;= customerName<sub>i</sub>.length, foodItem<sub>i</sub>.length &lt;= 20</code></li>
	<li><code>customerName<sub>i</sub></code> and <code>foodItem<sub>i</sub></code> consist of lowercase and uppercase English letters and the space character.</li>
	<li><code>tableNumber<sub>i</sub>&nbsp;</code>is a valid integer between <code>1</code> and <code>500</code>.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- JP Morgan - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Clean Python 3, hashmap O(N + TlogT + FlogF + T * F)
- Author: lenchen1112
- Creation Date: Sun Apr 19 2020 12:08:30 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Apr 22 2020 10:20:55 GMT+0800 (Singapore Standard Time)

<p>
It\'s quit straightforward to use a hashmap to save meals that each table has.
Time: `O(N + TlogT + FlogF + T * F)`
`N`, `T`, `F` stands for amount of orders, tables and foods
Cost come from iterating orders and sorting tables and foods.
And the last `for` takes `T * F` to dump the result.
Space: `O(T * F)`
```
import collections
class Solution:
    def displayTable(self, orders: List[List[str]]) -> List[List[str]]:
        desk = collections.defaultdict(collections.Counter)
        meal = set()
        for _, table, food in orders:
            meal.add(food)
            desk[table][food] += 1
        foods = sorted(meal)
        result = [[\'Table\'] + [food for food in foods]]
        for table in sorted(desk, key=int):
            result.append([table] + [str(desk[table][food]) for food in foods])
        return result
```
</p>


### Java TreeMap And TreeSet
- Author: Poorvank
- Creation Date: Thu May 07 2020 23:57:45 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 07 2020 23:57:45 GMT+0800 (Singapore Standard Time)

<p>
```
public List<List<String>> displayTable(List<List<String>> orders) {
        List<List<String>> res = new ArrayList<>();
        List<String> firstRow = new ArrayList<>();
        firstRow.add("Table");
        TreeSet<String> set = new TreeSet<>();
        TreeMap<Integer,Map<String,Integer>> map = new TreeMap<>();
        for (List<String> order : orders) {
            String dish = order.get(2);
            set.add(dish);
            Integer table = Integer.parseInt(order.get(1));
            map.putIfAbsent(table,new HashMap<>());
            if(map.get(table).containsKey(dish)) {
                Map<String,Integer> m = map.get(table);
                m.put(dish,m.getOrDefault(dish,0)+1);
            } else {
                map.get(table).put(dish,1);
            }
        }

        firstRow.addAll(set);
        res.add(firstRow);
        for (Map.Entry<Integer,Map<String,Integer>> entry : map.entrySet()) {
            List<String> list = new ArrayList<>();
            list.add(entry.getKey()+"");
            Map<String,Integer> m = entry.getValue();
            for (String dish : set) {
                if(m.containsKey(dish)) {
                    list.add(m.get(dish)+"");
                } else {
                    list.add("0");
                }
            }
            res.add(list);
        }
        return res;

    }
```
</p>


### [C++] Hashmap and Set
- Author: votrubac
- Creation Date: Sun Apr 19 2020 12:04:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Apr 25 2020 01:57:06 GMT+0800 (Singapore Standard Time)

<p>
I missed that the food needs to be in the alphabetical order! A quick fix was to change `unordered_set` into `set`. Perhaps, it would can be more efficient to use hash set but sort food items before generating the table.

```cpp
vector<vector<string>> displayTable(vector<vector<string>>& orders) {
    vector<unordered_map<string, int>> tables(501);
    set<string> foods;
    for (auto &v : orders) {
        foods.insert(v[2]);
        ++tables[stoi(v[1])][v[2]];
    }
    vector<vector<string>> res;
    for (auto t = 0; t <= 500; ++t) {
        if (t > 0 && tables[t].empty())
            continue;
        res.push_back(vector<string>());
        res.back().push_back(t == 0 ? "Table" : to_string(t));
        for (auto it = begin(foods); it != end(foods); ++it) {
            res.back().push_back(t == 0 ? *it : to_string(tables[t][*it]));
        }
    }
    return res;
}
```
</p>


