---
title: "Logger Rate Limiter"
weight: 342
#id: "logger-rate-limiter"
---
## Description
<div class="description">
<p>Design a logger system that receive stream of messages along with its timestamps, each message should be printed if and only if it is <b>not printed in the last 10 seconds</b>.</p>

<p>Given a message and a timestamp (in seconds granularity), return true if the message should be printed in the given timestamp, otherwise returns false.</p>

<p>It is possible that several messages arrive roughly at the same time.</p>

<p><b>Example:</b></p>

<pre>
Logger logger = new Logger();

// logging string &quot;foo&quot; at timestamp 1
logger.shouldPrintMessage(1, &quot;foo&quot;); returns true; 

// logging string &quot;bar&quot; at timestamp 2
logger.shouldPrintMessage(2,&quot;bar&quot;); returns true;

// logging string &quot;foo&quot; at timestamp 3
logger.shouldPrintMessage(3,&quot;foo&quot;); returns false;

// logging string &quot;bar&quot; at timestamp 8
logger.shouldPrintMessage(8,&quot;bar&quot;); returns false;

// logging string &quot;foo&quot; at timestamp 10
logger.shouldPrintMessage(10,&quot;foo&quot;); returns false;

// logging string &quot;foo&quot; at timestamp 11
logger.shouldPrintMessage(11,&quot;foo&quot;); returns true;
</pre>
</div>

## Tags
- Hash Table (hash-table)
- Design (design)

## Companies
- Google - 14 (taggedByAdmin: true)
- Atlassian - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Queue + Set

**Intuition**

Before we tackle the problem, it is imperative to clarify the conditions of the problem, since it was not explicit in the problem description. Here is one important note:

>It is possible that several messages arrive roughly at the same time.

We could interpret that the input messages are in chronological order, _i.e._ the timestamps of the messages are monotonically increasing, though not _strictly_. This constraint is critical, since it would simplify the task, as one will see in the following solutions.

As a first solution, let us build a solution _intuitively_ following the tasks described in the problem.

>We keep the incoming messages in a **queue**. In addition, to accelerate the check of duplicates, we use a **set** data structure to index the messages.

![pic](../Figures/359/359_deque.png)

As one see can from the above example where the number indicates the timestamp of each message, the arrival of the message with the timestamp `18` would invalidate both the messages with the timestamp of `5` and `7` which go beyond the time window of 10 seconds.

**Algorithm**

- First of all, we use a queue as a sort of sliding window to keep all the printable messages in certain time frame (10 seconds).

- At the arrival of each incoming message, it comes with a `timestamp`. This timestamp implies the evolution of the sliding windows. Therefore, we should first invalidate those _expired_ messages in our queue. 

- Since the `queue` and `set` data structures should be in sync with each other, we would also remove those expired messages from our message set.

- After the updates of our message queue and set, we then simply check if there is any duplicate for the new incoming message. If not, we add the message to the queue as well as the set.

<iframe src="https://leetcode.com/playground/aqVc2Yfo/shared" frameBorder="0" width="100%" height="500" name="aqVc2Yfo"></iframe>

As one can see, the usage of set data structure is not _absolutely_ necessary. One could simply iterate the message queue to check if there is any duplicate.

Another important note is that if the messages are not chronologically ordered then we would have to iterate through the entire queue to remove the expired messages, rather than having _early stopping_. Or one could use some sorted queue such as **Priority Queue** to keep the messages.

**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the size of the queue. In the worst case, all the messages in the queue become obsolete. As a result, we need clean them up.

- Space Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the size of the queue. We keep the incoming messages in both the queue and set. The upper bound of the required space would be $$2N$$, if we have no duplicate at all.
<br/>
<br/>

---
#### Approach 2: Hashtable / Dictionary

**Intuition**

One could combine the queue and set data structure into a **hashtable** or **dictionary**, which gives us the capacity of keeping all unique messages as of queue as well as the capacity to quickly evaluate the duplication of messages as of set.

>The idea is that we keep a hashtable/dictionary with the message as key, and its timestamp as the value. The hashtable keeps all the unique messages along with the latest timestamp that the message was printed.

![pic](../Figures/359/359_hashtable.png)

As one can see from the above example, there is an entry in the hashtable with the message `m2` and the timestamp `2`. Then there comes another message `m2` with the timestamp `15`. Since the message was printed 13 seconds before (_i.e._ beyond the buffer window), it is therefore eligible to print again the message. As a result, the timestamp of the message `m2` would be updated to `15`.

**Algorithm**

- We initialize a hashtable/dictionary to keep the messages along with the timestamp.

- At the arrival of a new message, the message is eligible to be printed with either of the two conditions as follows:

    - case 1). we have never seen the message before.

    - case 2). we have seen the message before, and it was printed more than 10 seconds ago.

- In both of the above cases, we would then update the entry that is associated with the message in the hashtable, with the latest timestamp.

<iframe src="https://leetcode.com/playground/HX998GTQ/shared" frameBorder="0" width="100%" height="497" name="HX998GTQ"></iframe>

*Note: for clarity, we separate the two cases into two blocks. One could combine the two blocks together to have a more concise solution.*

The main difference between this approach with hashtable and the previous approach with queue is that in previous approach we do *proactive* cleaning, _i.e._ at each invocation of function, we first remove those expired messages. 

While in this approach, we keep all the messages even when they are expired. This characteristics might become problematic, since the usage of memory would keep on growing over the time. Sometimes it might be more desirable to have the _garbage collection_ property of the previous approach.

**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(1)$$. The lookup and update of the hashtable takes a constant time.

- Space Complexity: $$\mathcal{O}(M)$$ where $$M$$ is the size of all incoming messages. Over the time, the hashtable would have an entry for each unique message that has appeared.
<br/>
<br/>

## Accepted Submission (python3)
```python3
class Logger:
    Interval = 10

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.printed: Dict[str, int] = {}

    def shouldPrintMessage(self, timestamp: int, message: str) -> bool:
        """
        Returns true if the message should be printed in the given timestamp, otherwise returns false.
        If this method returns false, the message will not be printed.
        The timestamp is in seconds granularity.
        """
        if message in self.printed:
            lastTime = self.printed[message]
            if timestamp - lastTime >= Logger.Interval:
                self.printed[message] = timestamp
                return True
            else:
                return False
        else:
            self.printed[message] = timestamp
            return True
        


# Your Logger object will be instantiated and called as such:
# obj = Logger()
# param_1 = obj.shouldPrintMessage(timestamp,message)
```

## Top Discussions
### Short C++/Java/Python, bit different
- Author: StefanPochmann
- Creation Date: Thu Jun 16 2016 19:19:51 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 13:12:05 GMT+0800 (Singapore Standard Time)

<p>
Instead of logging print times, I store when it's ok for a message to be printed again. Should be slightly faster, because I don't always have to add or subtract (e.g., `timestamp < log[message] + 10`) but only do in the `true` case. Also, it leads to a shorter/simpler longest line of code. Finally, C++ has 0 as default, so I can just use `ok[message]`.

---

**C++**

    class Logger {
    public:
    
        map<string, int> ok;

        bool shouldPrintMessage(int timestamp, string message) {
            if (timestamp < ok[message])
                return false;
            ok[message] = timestamp + 10;
            return true;
        }
    };

---

**Python**

    class Logger(object):
    
        def __init__(self):
            self.ok = {}
    
        def shouldPrintMessage(self, timestamp, message):
            if timestamp < self.ok.get(message, 0):
                return False
            self.ok[message] = timestamp + 10
            return True

---

**Java**

    public class Logger {
    
        private Map<String, Integer> ok = new HashMap<>();
    
        public boolean shouldPrintMessage(int timestamp, String message) {
            if (timestamp < ok.getOrDefault(message, 0))
                return false;
            ok.put(message, timestamp + 10);
            return true;
        }
    }
</p>


### Review of four different solutions: HashMap, Two Sets, Queue with Set, Radix buckets (Java centric)
- Author: totsubo
- Creation Date: Fri Sep 27 2019 15:16:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2020 10:43:34 GMT+0800 (Singapore Standard Time)

<p>
I really liked this problem because even though it is simple there are various solutions each with different trade-offs. While reviewing other people\'s solutions I took some notes I figured I might has well post them so other people can profit also.

Please upvote if you find this article useful, it will encourage me to write similar summaries for other problems ^_^

**Simple HashMap**

The simplest solution is to use a HashMap.

A great example of such a solution is:
https://leetcode.com/problems/logger-rate-limiter/discuss/83273/Short-C%2B%2BJavaPython-bit-different

**Time complexity**
* `O(1)`

**Space complexity**
* `O(n)`

The disadvantage to this solution is that the memory usage never stops growing.

All the solutions below use different ways to manage the memory usage disadvantage mentionned above.

**Use Two Sets**

https://leetcode.com/problems/logger-rate-limiter/discuss/365306/Simple-Two-HashMap-Solution-with-O(1)-time-and-little-memory

This is my favorite. It\'s a simple implementation, it takes care of keeping memory usage low, and only requires swapping two HashMaps around. 

**Time complexity**
* `O(1)`

**Space complexity**
* `O(m)` where `m` is the maximum number of unique message that will be received in a 20 second period.


**Use a Queue and Set**

https://leetcode.com/problems/logger-rate-limiter/discuss/349733/Simple-Java-solution-using-Queue-and-Set-for-slow-learners-like-myself

1- Use a `Queue` that contains Object(timestamp, message)
2- When new message comes in, remove from Queue any message where timestamp + 10 < new message\'s timestamp. If any `Queue` element is deleted, remove that message from the `Set` (because the last occured of this message is now too old)
3- If new message is still in `Set`, return false (because the `Queue` still contains it, so it is not too old yet)
4- Add the message to the `Set`

One downside is that if many unique messages are received in the 10 second window (say 1 million), the `Queue` can become very large. When it comes time to cleanup the `Queue`, the method will spend a lot time polling all messages off the `Queue` before returning.

**Time complexity**
* `O(1)`

**Space complexity**
* `O(m)` where `m` is the maximum number of unique message that will be received in a 10 second period.

**Use Radix Sort and Buckets**

https://leetcode.com/problems/logger-rate-limiter/discuss/83256/Java-Circular-Buffer-Solution-similar-to-Hit-Counter

It\'s a neat idea and fun to think about. It also takes care of keeping memory low but compared to the other solutions it\'s a bit more code and slightly less efficient, it\'s `O(10n)`, which is the same as `O(n)` but still, that\'s up to `10x` the number of lookups compared to a plan `HashMap` implementation.

**Concurency / Thread safety**

https://leetcode.com/problems/logger-rate-limiter/discuss/83298/Thread-Safe-Solution

It\'s possible that the Logger is called with the same message at the same time from multiple sources. In that case the HashMap might not have been updated fast enough when the second duplicate message arrives.

For example:

1. New message m1 arrives at time 1
2. Same message m1 arrives again at time 1
3. Logger is called with (m1, 1)
4. Logger is called with (m1, 1)
5. The second call will possibly print the message if the first call (#3) hasn\'t had a chance to create the HashMap entry for m1 yet.

One possible solution is to use a lock


```
public boolean shouldPrintMessage(int timestamp, String message) {
		Integer ts = msgMap.get(message);
		if (ts == null || timestamp - ts >= 10) {
			synchronized (lock) {
				Integer ts2 = msgMap.get(message);
				if (ts == null || timestamp - ts2 >= 10) {
						msgMap.put(message, timestamp);
					return true;
				}
			}
		 } 
		return false;
}
```
</p>


### Java Circular Buffer  Solution, similar to Hit Counter
- Author: zhenh_leet
- Creation Date: Sun Oct 16 2016 04:26:52 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 18 2018 09:08:05 GMT+0800 (Singapore Standard Time)

<p>
We only need to store past 10s informtion
```
public class Logger {
    private int[] buckets;
    private Set[] sets;
    /** Initialize your data structure here. */
    public Logger() {
        buckets = new int[10];
        sets = new Set[10];
        for (int i = 0; i < sets.length; ++i) {
            sets[i] = new HashSet<String>();
        }
    }
    
    /** Returns true if the message should be printed in the given timestamp, otherwise returns false.
        If this method returns false, the message will not be printed.
        The timestamp is in seconds granularity. */
    public boolean shouldPrintMessage(int timestamp, String message) {
        int idx = timestamp % 10;
        if (timestamp != buckets[idx]) {
            sets[idx].clear();
            buckets[idx] = timestamp;
        }
        for (int i = 0; i < buckets.length; ++i) {
            if (timestamp - buckets[i] < 10) {
                if (sets[i].contains(message)) {
                    return false;
                }
            }
        } 
        sets[idx].add(message);
        return true;
    }
}
```
</p>


