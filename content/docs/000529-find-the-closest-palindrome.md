---
title: "Find the Closest Palindrome"
weight: 529
#id: "find-the-closest-palindrome"
---
## Description
<div class="description">
<p>Given an integer n, find the closest integer (not including itself), which is a palindrome. </p>

<p>The 'closest' is defined as absolute difference minimized between two integers.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> "123"
<b>Output:</b> "121"
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The input <b>n</b> is a positive integer represented by string, whose length will not exceed 18.</li>
<li>If there is a tie, return the smaller one as answer.</li>
</ol>
</p>
</div>

## Tags
- String (string)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- Nutanix - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Yelp - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force[Time Limit Exceeded]

The simplest solution is to consider every possible number smaller than the given number $$n$$, starting by decrementing 1 from the given number and go on in descending order. Similarly, we can consider every possible number greater than $$n$$ starting by incrementing 1 from the given number and going in ascending order. We can continue doing so in an alternate manner till we find a number which is a palindrome.

<iframe src="https://leetcode.com/playground/DvreVK8V/shared" frameBorder="0" name="DvreVK8V" width="100%" height="377"></iframe>

**Complexity Analysis**

* Time complexity : $$O(\sqrt{n})$$. Upto $$2*\sqrt{n}$$ numbers could be generated in the worst case.

* Space complexity : $$O(1)$$. Constant space is used.

---

#### Approach #2 Using Math[Accepted]

**Algorithm**

To understand this method, let's start with a simple illustration. Assume that the number given to us is "abcxy". One way to convert this number into a palindrome is to replicate one half of the string to the other half. If we try replicating the second half to the first half, the new palindrome obtained will be "yxcxy" which lies at an absolute of $$\left|10000(a-y) + 1000(b-x)\right|$$ from the original number. But, if we replicate the first half to the second half of the string, we obtain "abcba", which lies at an absolute difference of $$\left|10(x-b) + (y-a)\right|$$. Trying to change $$c$$ additionaly in either case would incur an additional value of atleast 100 in the absolute difference.

From the above illustration, we can conclude that if replication is used to generate the palindromic number, we should always replicate the first half to the second half. In this implementation, we've stored such a number in $$a$$ at a difference of $$diff1$$ from $$n$$.

But, there exists another case as well, where the digit at the middle index is incremented or decremented. In such cases, it could be profitable to make changes to the central digit only since such changes could lead to a palindrome formation nearer to the original digit. e.g. 10987. Using the above criteria, the palindrome obtained will be 10901 which is at a more difference from 10987 than 11011. A similar situation occurs if a 0 occurs at the middle digit. But, again as discussed previously, we need to consider only the first half digits to obtain the new palindrome. This special effect occurs with 0 or 9 at the middle digit since, only decrementing 0 and incrementing 9 at that digit place can lead to the change in the rest of the digits towards their left. In any other case, the situation boils down to the one discussed in the first paragraph.

Now, whenever we find a 0 near the middle index, in order to consider the palindromes which are lesser than $$n$$, we subtract a 1 from the first half of the number to obtain a new palindromic half e.g. If the given number $$n$$ is 20001, we subtract a 1 from 200 creating a number of the form 199xx. To obtain the new palindrome, we replicate the first half to obtain 19991. Taking another example of  10000, (with a 1 at the MSB), we subtract a 1 from 100 creating 099xx as the new number transforming to a 9999 as the new palindrome. This number is stored in $$b$$ having a difference of $$diff2$$ from $$n$$

Similar treatment needs to be done with a 9 at the middle digit, except that this time we need to consider the numbers larger than the current number. For this, we add a 1 to the first half. e.g. Taking the number 10987, we add a 1 to 109 creating a number of the form 110xx(11011 is the new palindrome). This palindrome is stored in $$c$$ having a difference of $$diff3$$ from $$n$$.

Out of these three palindromes, we can choose the one with a minimum difference from $$n$$. Further, in case of a tie, we need to return the smallest palindrome obtained. For resolving this tie's conflict, we can observe that a tie is possible only if one number is larger than $$n$$ and another is lesser than $$n$$. Further, we know that the number $$b$$ is obtained by decreasing $$n$$. Thus, in case of conflict between $$b$$ and any other number, we need to choose $$b$$. Similarly, $$c$$ is obtained by increasing $$n$$. Thus, in case of a tie between $$c$$ and any other number, we need to choose the number other than $$c$$.



<iframe src="https://leetcode.com/playground/Y6G9NDDf/shared" frameBorder="0" name="Y6G9NDDf" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(l)$$. Scanning, insertion, deletion,, mirroring takes $$O(l)$$, where $$l$$ is the length of the string.

* Space complexity : $$O(l)$$. Temporary variables are used to store the strings.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python, Simple with Explanation
- Author: awice
- Creation Date: Sun Apr 23 2017 11:34:37 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 20:43:06 GMT+0800 (Singapore Standard Time)

<p>
Let's build a list of candidate answers for which the final answer must be one of those candidates.  Afterwards, choosing from these candidates is straightforward.

If the final answer has the same number of digits as the input string ```S```, then the answer must be the middle digits + (-1, 0, or 1) flipped into a palindrome.  For example, ```23456``` had middle part ```234```, and ```233, 234, 235``` flipped into a palindrome yields ```23332, 23432, 23532```.  Given that we know the number of digits, the prefix ```235``` (for example) uniquely determines the corresponding palindrome ```23532```, so all palindromes with larger prefix like ```23732``` are strictly farther away from S than ```23532 >= S```.

If the final answer has a different number of digits, it must be of the form ```999....999``` or ```1000...0001```, as any palindrome smaller than ```99....99``` or bigger than ```100....001``` will be farther away from S.
 
```
def nearestPalindromic(self, S):
    K = len(S)
    candidates = [str(10**k + d) for k in (K-1, K) for d in (-1, 1)]
    prefix = S[:(K+1)/2]
    P = int(prefix)
    for start in map(str, (P-1, P, P+1)):
        candidates.append(start + (start[:-1] if K%2 else start)[::-1])
    
    def delta(x):
        return abs(int(S) - int(x))
    
    ans = None
    for cand in candidates:
        if cand != S and not cand.startswith('00'):
            if (ans is None or delta(cand) < delta(ans) or
                    delta(cand) == delta(ans) and int(cand) < int(ans)):
                ans = cand
    return ans
```
</p>


### Thinking Process
- Author: GraceMeng
- Creation Date: Tue Jul 10 2018 15:28:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 07 2019 07:26:15 GMT+0800 (Singapore Standard Time)

<p>
Take a palindrome number 12321 for example, the next bigger palindrome should be 12421, and the next smaller one should be 12221. 

That is, if we define 123 as `palindromeRoot`  of 12321, the next bigger palindrome number\'s palindromeRoot is`palindromeRoot + 1` (124), while the next smaller one\'s palindromeRoot is`palindromeRoot - 1`(122).

For palindrome numbers with even digits, e.g., 123321, the change of palindromeRoot follows the same pattern. 

The closest palindromic one should be among the palindromic numbers formed by these 2 palindromeRoots.

For numbers which are not palindromic, e.g., 12345, we still focus on the front half of the number, i.e., `palindromeRoot` as 123 in the example. Except for the bigger one formed by `palindromeRoot + 1`(124), the smaller one formed by `palindromeRoot - 1`(122), there should be one more possibility, i.e., the number formed by `palindromeRoot` (123). We chose the closest one among these 3 palindromic numbers formed.

There are some cases missing the rules above,
case 1. <= 10, OR equal to 100, 1000, 10000, ... We simply decrease n by 1.
case 2. 11, 101, 1001, 10001, 100001, ... We simply decrease n by 2.
case 3. 99, 999, 9999, 99999, ...    We simply increase n by 2.

****
```
    public String nearestPalindromic(String n) {
        long nl = Long.parseLong(n);
        int len = n.length();
        
        //
        // Corner cases
        //
        
        // <= 10 or equal to 100, 1000, 10000, ... 
        if (nl <= 10 || (nl % 10 == 0 
                         && nl != 0 
                         && Long.parseLong(n.substring(1)) == 0)) {
            return "" + (nl - 1);
        }
        
        // 11 or 101, 1001, 10001, 100001, ... 
        if (nl == 11 || (nl % 10 == 1 
                         && n.charAt(0) == \'1\' 
                         && Long.parseLong(n.substring(1, len - 1)) == 0)) {
            return "" + (nl - 2);
        }
        
        // 99, 999, 9999, 99999, ...  
        if (isAllDigitNine(n)) {
            return "" + (nl + 2);
        }
        
        //
        // Construct the closest palindrome and calculate absolute difference with n
        //
        boolean isEvenDigits = len % 2 == 0;
        
        String palindromeRootStr
            = (isEvenDigits) ? n.substring(0, len / 2) : n.substring(0, len / 2 + 1);
        
        int palindromeRoot = Integer.valueOf(palindromeRootStr); 
        long equal = toPalindromeDigits("" + palindromeRoot, isEvenDigits);
        long diffEqual = Math.abs(nl - equal);
            
        long bigger = toPalindromeDigits("" + (palindromeRoot + 1), isEvenDigits);
        long diffBigger = Math.abs(nl - bigger);
        
        long smaller = toPalindromeDigits("" + (palindromeRoot - 1), isEvenDigits);
        long diffSmaller = Math.abs(nl - smaller);
         
        //
        // Find the palindrome with minimum absolute differences
        // If tie, return the smaller one
        //
        long closest = (diffBigger < diffSmaller) ? bigger : smaller;
        long minDiff = Math.min(diffBigger, diffSmaller);
        
        if (diffEqual != 0) { // n is not a palindrome, diffEqual should be considered
            if (diffEqual == minDiff) { // if tie
                closest = Math.min(equal, closest);
            } else if (diffEqual < minDiff){
                closest = equal;
            }
        }
        
        return "" + closest;
    }
    
    private long toPalindromeDigits(String num, boolean isEvenDigits) {
        StringBuilder reversedNum = new StringBuilder(num).reverse();
        String palindromeDigits = isEvenDigits
            ? num + reversedNum.toString()
            : num + (reversedNum.deleteCharAt(0)).toString();
        return Long.parseLong(palindromeDigits);
    }
    
    private boolean isAllDigitNine(String n) {
        for (char ch : n.toCharArray()) {
            if (ch != \'9\') {
                return false;
            }
        }
        return true;
    }
```

</p>


### Java, compare five candidates, get result, easy to understand
- Author: jaywin
- Creation Date: Mon Apr 09 2018 12:25:05 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 01:03:25 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public String nearestPalindromic(String n) {
        // edge cases, no
        
        int len = n.length();
        int i = len % 2 == 0 ? len / 2 - 1: len / 2;
        long left = Long.parseLong(n.substring(0, i+1));
        
        // input: n 12345
        List<Long> candidate = new ArrayList<>();
        candidate.add(getPalindrome(left, len % 2 == 0)); // 12321
        candidate.add(getPalindrome(left+1, len % 2 == 0)); // 12421
        candidate.add(getPalindrome(left-1, len % 2 == 0)); // 12221
        candidate.add((long)Math.pow(10, len-1) - 1); // 9999
        candidate.add((long)Math.pow(10, len) + 1); // 100001
        
        long diff = Long.MAX_VALUE, res = 0, nl = Long.parseLong(n);
        for (long cand : candidate) {
            if (cand == nl) continue;
            if (Math.abs(cand - nl) < diff) {
                diff = Math.abs(cand - nl);
                res = cand;
            } else if (Math.abs(cand - nl) == diff) {
                res = Math.min(res, cand);
            }
        }
        
        return String.valueOf(res);
    }
    
    private long getPalindrome(long left, boolean even) {
        long res = left;
        if (!even) left = left / 10;
        while (left > 0) {
            res = res * 10 + left % 10;
            left /= 10;
        }
        return res;
    }
}

/*
get first half, then compare 5 cases: +0(itself not palindrome), +1 / -1 / 9...9 / 10..01 (itself palindrome)
*/
```
</p>


