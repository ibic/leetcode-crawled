---
title: "Rectangle Overlap"
weight: 780
#id: "rectangle-overlap"
---
## Description
<div class="description">
<p>An axis-aligned rectangle is represented as a list <code>[x1, y1, x2, y2]</code>, where <code>(x1, y1)</code> is the coordinate of its bottom-left corner, and <code>(x2, y2)</code> is the coordinate of its top-right corner. Its top and bottom edges are parallel to the X-axis, and its left and right edges are parallel to the Y-axis.</p>

<p>Two rectangles overlap if the area of their intersection is <strong>positive</strong>. To be clear, two rectangles that only touch at the corner or edges do not overlap.</p>

<p>Given two axis-aligned rectangles <code>rec1</code> and <code>rec2</code>, return <code>true</code><em> if they overlap, otherwise return </em><code>false</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> rec1 = [0,0,2,2], rec2 = [1,1,3,3]
<strong>Output:</strong> true
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> rec1 = [0,0,1,1], rec2 = [1,0,2,1]
<strong>Output:</strong> false
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> rec1 = [0,0,1,1], rec2 = [2,2,3,3]
<strong>Output:</strong> false
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>rect1.length == 4</code></li>
	<li><code>rect2.length == 4</code></li>
	<li><code>-10<sup>9</sup> &lt;= rec1[i], rec2[i] &lt;= 10<sup>9</sup></code></li>
	<li><code>rec1[0] &lt;= rec1[2]</code> and <code>rec1[1] &lt;= rec1[3]</code></li>
	<li><code>rec2[0] &lt;= rec2[2]</code> and <code>rec2[1] &lt;= rec2[3]</code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Apple - 4 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Check Position [Accepted]

**Intuition**

If the rectangles do not overlap, then `rec1` must either be higher, lower, to the left, or to the right of `rec2`.

**Algorithm**

The answer for whether they *don't* overlap is `LEFT OR RIGHT OR UP OR DOWN`, where `OR` is the logical OR, and `LEFT` is a boolean that represents whether `rec1` is to the left of `rec2`.  The answer for whether they do overlap is the negation of this.

The condition "`rec1` is to the left of `rec2`" is `rec1[2] <= rec2[0]`, that is the right-most x-coordinate of `rec1` is left of the left-most x-coordinate of `rec2`.  The other cases are similar.

<iframe src="https://leetcode.com/playground/nFMcM4Q2/shared" frameBorder="0" width="100%" height="191" name="nFMcM4Q2"></iframe>

**Complexity Analysis**

* Time and Space Complexity:  $$O(1)$$.

---
#### Approach #2: Check Area [Accepted]

**Intuition**

If the rectangles overlap, they have positive area.  This area must be a rectangle where both dimensions are positive, since the boundaries of the intersection are axis aligned.

Thus, we can reduce the problem to the one-dimensional problem of determining whether two line segments overlap.

**Algorithm**

Say the area of the intersection is `width * height`, where `width` is the intersection of the rectangles projected onto the x-axis, and `height` is the same for the y-axis.  We want both quantities to be positive.

The `width` is positive when `min(rec1[2], rec2[2]) > max(rec1[0], rec2[0])`, that is when the smaller of (the largest x-coordinates) is larger than the larger of (the smallest x-coordinates).  The `height` is similar.

<iframe src="https://leetcode.com/playground/dUKNq48o/shared" frameBorder="0" width="100%" height="157" name="dUKNq48o"></iframe>

**Complexity Analysis**

* Time and Space Complexity:  $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] 1-line Solution, 1D to 2D
- Author: lee215
- Creation Date: Sun May 20 2018 11:27:40 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 22:06:31 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**:
Before we do it in 2D plane, let\'s try it in 1D.
Given 2 segment `(left1, right1)`, `(left2, right2)`, how can we check whether they overlap?
If these two intervals overlap, it should exist an number `x`,

`left1 < x < right1 && left2 < x < right2`

`left1 < x < right2 && left2 < x < right1`

`left1 < right2 && left2 < right1`

This is the sufficient and necessary condition for two segments overlap.

**Explanation**:
For 2D, if two rectancle overlap both on `x` and `y`, they overlap in the plane.

**Time Complexity**:
O(1)

**C++/Java:**
```
return rec1[0] < rec2[2] && rec2[0] < rec1[2] && rec1[1] < rec2[3] && rec2[1] < rec1[3];
```
**Python:**
```
return rec1[0] < rec2[2] and rec2[0] < rec1[2] and rec1[1] < rec2[3] and rec2[1] < rec1[3]
```
</p>


### C++ Solution with easy explanation
- Author: jayesch
- Creation Date: Thu May 24 2018 05:16:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 04:19:57 GMT+0800 (Singapore Standard Time)

<p>
         Consider a 1D overlap (Interval problem).
         For an overlap to occur necessary condition is
         
                 x3,y2          x4,y2
                   |--------------|
         |--------------|
         x1,y1         x2,y1
                  
         x1 < x3 < x2  && x3 < x2 < x4 
         
         Or simplified - x1 < x4 && x3 < x2
         For 2D case use 1D conditions for both X and Y axes
         
         Case-1: Rec2 intersects with Rec1 on top right corner
    
                   ____________________x4,y4
                  |                   |
           _______|______x2,y2        |
          |       |______|____________|
          |      x3,y3   |
          |______________|
         x1,y1
                  
         Case-2: Rec2 intersects with Rec1 on top left corner
           ___________________  x4,y4
          |                   |
          |            _______|____________x2,y2
          |___________|_______|           |
        x3,y3         |                   | 
                      |___________________|
                    x1,y1

         Case-3: Rec2 intersects with Rec1 on bottom left corner
         
                   ____________________x2,y2
                  |                   |
           _______|______x4,y4        |
          |       |______|____________|
          |      x1,y1   |
          |______________|
         x3,y3
                  
         Case-4: Rec2 intersects with Rec1 on bottom right corner
         
           ___________________  x2,y2
          |                   |
          |            _______|____________x4,y4
          |___________|_______|           |
        x1,y1         |                   | 
                      |___________________|
                    x3,y3
        
        bool case1 = (x1 < x4 && x3 < x2 && y1 < y4 && y3 < y2); //top right intersection
        bool case2 = (x3 < x2 && x1 < x4 && y1 < y4 && y3 < y2); //top left intersection
        bool case3 = (x3 < x2 && x1 < x4 && y3 < y2 && y4 < y1); //bottom left intersection
        bool case4 = (x1 < x4 && x3 < x2 && y3 < y2 && y4 < y1); //bottom right intersection
        
        if you look carefully in all cases we have same 4 comparisons
```
bool isRectangleOverlap(vector<int>& rec1, vector<int>& rec2) {
	int x1 = rec1[0], y1 = rec1[1], x2 = rec1[2], y2 = rec1[3];
	int x3 = rec2[0], y3 = rec2[1], x4 = rec2[2], y4 = rec2[3];
	return (x1 < x4 && x3 < x2 && y1 < y4 && y3 < y2);
}
```
</p>


### De Morgan's Law and Boolean Algebra
- Author: bradshaw
- Creation Date: Wed May 23 2018 14:06:35 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 07:15:30 GMT+0800 (Singapore Standard Time)

<p>
De Morgan\'s Law tells us that the following are equivalent. An easy way to remember De Morgan\'s is by the saying to split the bar and change the operation; that is to distribute negation and change the boolean operator in this example.

```
    public boolean isRectangleOverlap(int[] rec1, int[] rec2) {
        return !(rec1[2] <= rec2[0] ||   // left
                 rec1[3] <= rec2[1] ||   // bottom
                 rec1[0] >= rec2[2] ||   // right
                 rec1[1] >= rec2[3]);    // top
    }
```

```
    public boolean isRectangleOverlap(int[] rec1, int[] rec2) {
        return   rec1[2] > rec2[0] &&   // left
                 rec1[3] > rec2[1] &&   // bottom
                 rec1[0] < rec2[2] &&   // right
                 rec1[1] < rec2[3];     // top
    }
```


</p>


