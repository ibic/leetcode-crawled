---
title: "Greatest Common Divisor of Strings"
weight: 1051
#id: "greatest-common-divisor-of-strings"
---
## Description
<div class="description">
<p>For two strings <code>s</code> and <code>t</code>, we say &quot;<code>t</code> divides <code>s</code>&quot; if and only if <code>s = t + ... + t</code>&nbsp; (<code>t</code> concatenated with itself 1 or more times)</p>

<p>Given two strings str1 and str2, return the largest string <code>x</code> such that <code>x</code> divides both&nbsp;<code><font face="monospace">str1</font></code>&nbsp;and <code><font face="monospace">str2</font></code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> str1 = "ABCABC", str2 = "ABC"
<strong>Output:</strong> "ABC"
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> str1 = "ABABAB", str2 = "ABAB"
<strong>Output:</strong> "AB"
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> str1 = "LEET", str2 = "CODE"
<strong>Output:</strong> ""
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> str1 = "ABCDEF", str2 = "ABC"
<strong>Output:</strong> ""
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= str1.length &lt;= 1000</code></li>
	<li><code>1 &lt;= str2.length &lt;= 1000</code></li>
	<li><code>str1</code>&nbsp;and <code>str2</code>&nbsp;consist of&nbsp;English uppercase letters.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Atlassian - 2 (taggedByAdmin: false)
- Capital One - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] 3 codes each: Recursive, iterative and regex w/ brief comments and analysis.
- Author: rock
- Creation Date: Sun Jun 02 2019 12:11:26 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 13 2020 01:41:36 GMT+0800 (Singapore Standard Time)

<p>
**Method 1: imitate gcd algorithm, recursive version.**

1. If longer string starts with shorter string, cut off the common prefix part of the longer string; repeat till one is empty, then the other is gcd string;
2. If the longer string does NOT start with the shorter one, there is no gcd string.

**Java**
```
    public String gcdOfStrings(String str1, String str2) {
        if (str1.length() < str2.length()) { // make sure str1 is not shorter than str2.
            return gcdOfStrings(str2, str1); 
        }else if (!str1.startsWith(str2)) { // shorter string is not common prefix.
            return ""; 
        }else if (str2.isEmpty()) { // gcd string found.
            return str1;
        }else { // cut off the common prefix part of str1.
            return gcdOfStrings(str1.substring(str2.length()), str2); 
        }
    }
```

----

**Python 3**

```
    def gcdOfStrings(self, str1: str, str2: str) -> str:
        if not str1 or not str2:
            return str1 if str1 else str2
        elif len(str1) < len(str2):
            return self.gcdOfStrings(str2, str1)
        elif str1[: len(str2)] == str2:
            return self.gcdOfStrings(str1[len(str2) :], str2)
        else:
            return \'\'

```
**Analysis:**

Time cost of `startsWith()` and `substring()` is the total length of the longer strings during all recursion calls;

Worst case: str1 is `n` repeated `\'A\'`, str2 is `\'A\'`, and the time cost is as follows:
`1 + 2 + 3 + ... + n = n * (n + 1) / 2`.

space cost of `substring()`  is similar.

recursion stack space cost is minor part and can be ignored in space complexity analysis.

**Time & space:** `O(n ^ 2)`, where `n = max(str1.length(), str2.length())`.

---

**Method 2: compare substring, iterative version.**

the length of gcd substring must be the gcd of the lengthes of both string.

check if the `substring(0, i)` of `str1` is the divisor string of
1. `str1`;
2. `str2`.
Increase `i` to find its maximum value for gcd string.

**Java**

```
    public String gcdOfStrings(String str1, String str2) {
        int i = 1, max = 0, len1 = str1.length(), len2 = str2.length();
        while (i <= len1 && i <= len2 && str1.charAt(i - 1) == str2.charAt(i - 1)) {
            boolean cd = true; // common divisor flag.
            if (len1 % i == 0 && len2 % i == 0) { // substring length i must be the common divisor of lengthes of str1 and str2.
                String commonDivisorStr = str1.substring(0, i);
                for (int j = 2 * i; cd && j <= len1; j += i) {
                    cd = commonDivisorStr.equals(str1.substring(j - i, j));
                }
                for (int j = 2 * i; cd && j <= len2; j += i) {
                    cd = commonDivisorStr.equals(str2.substring(j - i, j));
                }
                if (cd) max = i;
            }
            ++i;
        }
        return str1.substring(0, max);
    }
```
or make it simpler, credit to @Zzz_.

A common divisor must be prefix of the shorter string, and if `replace()` it in both strings, they end up with two empty strings.
```
    public String gcdOfStrings(String str1, String str2) {
        String s = str1.length() < str2.length() ? str1 : str2;
        for (int d = 1, end = s.length(); d <= end; ++d) {
            if (end % d != 0) { continue; } // only if length of s mod d is 0, can sub be common divisor.
            String sub = s.substring(0, end / d);
            if (str1.replace(sub, "").isEmpty() && str2.replace(sub, "").isEmpty()) {
                return sub;
            }
        }
        return "";
    }
```
A better version:
```
    public String gcdOfStrings(String str1, String str2) {
        int m = str1.length(), n = str2.length(), d = gcd(m, n);
        String s = str1.substring(0, d), str = str1 + str2;
        for (int i = 0; i < m + n; i += d) {
            if (!str.startsWith(s, i)) 
                return "";        
        }
        return s;
    }
    private int gcd(int a, int b) {
        return a == 0 ? b : gcd(b % a, a);
    }
```

----

**Python 3**

```
    def gcdOfStrings(self, str1: str, str2: str) -> str:
        def gcd(a: int, b: int) -> int:
            return b if a == 0 else gcd(b % a, a)
        d = gcd(len(str1), len(str2))
        return str1[: d] if str1[: d] * (len(str2) // d) == str2 and str2[: d] * (len(str1) // d) == str1 else \'\'
```
---

**Method 3: Regular Expression.**

Compute the gcd of the lengthes of the 2 strings, then confirm if the gcd size of the prefix of `str1` is actually the gcd of the strings.
Note for regex:
1. `()` to make the string inside as a group;
2. `+` is quantifier, which means 1 or more of the group ahead of the `+`.
. 
```
import java.util.regex.Pattern;

    public String gcdOfStrings(String str1, String str2) {
        int d = gcd(str1.length(), str2.length());
        String gcd = str1.substring(0, d), ptn = "(" + gcd + ")+";
        return Pattern.compile(ptn).matcher(str1).matches() && 
               Pattern.compile(ptn).matcher(str2).matches() ?
               gcd : "";
    }
    private int gcd(int a, int b) {
        return a == 0 ? b : gcd(b % a, a);
    }
```

----

**Python 3**

```
    def gcdOfStrings(self, str1: str, str2: str) -> str:
        def gcd(a: int, b: int) -> int:
            return b if a == 0 else gcd(b % a, a)
        d = gcd(len(str1), len(str2))
        gcd_str = str1[0 : d]
        ptn = \'(\' + gcd_str + \')+\'
        return gcd_str if re.fullmatch(ptn, str1) and re.fullmatch(ptn, str2) else \'\'
```

**Analysis:**

Time & space: O(n), where n = max(str1.length(), str2.length()).
</p>


### C++, 3 lines
- Author: ganz
- Creation Date: Fri Jun 07 2019 22:22:14 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 07 2019 22:52:51 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    string gcdOfStrings(const string& s1, const string& s2)
    {
        return (s1 + s2 == s2 + s1)  
		    ? s1.substr(0, gcd(size(s1), size(s2)))
			: "";
    }
};
```
</p>


### My 4 Lines C++ Solution
- Author: grandyang
- Creation Date: Sun Jun 02 2019 12:07:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 12:07:19 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    string gcdOfStrings(string str1, string str2) {
        if (str1.size() < str2.size()) return gcdOfStrings(str2, str1);
        if (str2.empty()) return str1;
        if (str1.substr(0, str2.size()) != str2) return "";
        return gcdOfStrings(str1.substr(str2.size()), str2);
    }
};
</p>


