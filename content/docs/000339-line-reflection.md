---
title: "Line Reflection"
weight: 339
#id: "line-reflection"
---
## Description
<div class="description">
<p>Given n points on a 2D plane, find if there is such a line parallel to y-axis that reflect the given points symmetrically, in other words, answer whether or not if there exists a line that after reflecting all points over the given line the set of the original points is the same that the reflected ones.</p>

<p>Note that there can be repeated points.</p>

<p><b>Follow up:</b><br />
Could you do better than O(<i>n</i><sup>2</sup>) ?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/04/23/356_example_1.PNG" style="width: 389px; height: 340px;" />
<pre>
<strong>Input:</strong> points = [[1,1],[-1,1]]
<strong>Output:</strong> true
<strong>Explanation:</strong> We can choose the line x = 0.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/04/23/356_example_2.PNG" style="width: 300px; height: 294px;" />
<pre>
<strong>Input:</strong> points = [[1,1],[-1,-1]]
<strong>Output:</strong> false
<strong>Explanation:</strong> We can&#39;t choose a line.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == points.length</code></li>
	<li><code>1 &lt;= n &lt;= 10^4</code></li>
	<li><code>-10^8&nbsp;&lt;= points[i][j] &lt;=&nbsp;10^8</code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Math (math)

## Companies
- Yandex - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple java hashset solution
- Author: juanren
- Creation Date: Wed Jun 15 2016 02:13:58 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 11:07:30 GMT+0800 (Singapore Standard Time)

<p>
       public boolean isReflected(int[][] points) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        HashSet<String> set = new HashSet<>();
        for(int[] p:points){
            max = Math.max(max,p[0]);
            min = Math.min(min,p[0]);
            String str = p[0] + "a" + p[1];
            set.add(str);
        }
        int sum = max+min;
        for(int[] p:points){
            //int[] arr = {sum-p[0],p[1]};
            String str = (sum-p[0]) + "a" + p[1];
            if( !set.contains(str))
                return false;
            
        }
        return true;
    }
</p>


### 11ms two-pass HashSet-based Java Solution
- Author: Erick111
- Creation Date: Sun Jun 12 2016 00:06:50 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 28 2018 08:26:18 GMT+0800 (Singapore Standard Time)

<p>
The idea is quite simple. If there exists a line reflecting the points, then each pair of symmetric points will have their x coordinates adding up to the same value, including the pair with the maximum and minimum x coordinates. So, in the first pass, I iterate through the array, adding each point to the hash set, and keeping record of the minimum and maximum x coordinates. Then, in the second pass, I check for every point to the left of the reflecting line, if its symmetric point is in the point set or not. If all points pass the test, then there exists a reflecting line. Otherwise, not.

By the way, here, to hash the content of an array, rather than the reference value, I use **Arrays.hashCode(int[])** first, and then re-hash this hash code. You can also use **Arrays.toString(int[])** to first convey the 2d array to a string, and then hash the string. But the second method is slower.

    public class Solution {
        public boolean isReflected(int[][] points) {
            HashSet<Integer> pointSet = new HashSet<>();
            int sum;
            int maxX, minX;
            
            minX = Integer.MAX_VALUE;
            maxX = Integer.MIN_VALUE;
            for(int[] point:points) {
                maxX = Math.max(maxX, point[ 0 ]);
                minX = Math.min(minX, point[ 0 ]);
                pointSet.add(Arrays.hashCode(point));
            }
            
            sum = maxX+minX;
            for(int[] point:points) {
                if(!pointSet.contains(Arrays.hashCode(new int[]{sum-point[ 0 ], point[ 1 ]}))) {
                    return false;
                }
            }
            return true;
        }
    }
</p>


### Bad problem description; come and read what it really means.
- Author: yuxiong
- Creation Date: Sat Dec 08 2018 02:57:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Dec 08 2018 02:57:29 GMT+0800 (Singapore Standard Time)

<p>
Problem description should read as following: 
    Given n points on a 2D plane, find if there is a single line L parallel to y-axis such that the reflection of every point amongst the input points, is also within points.
    In other words, see if every point in points can find its mirrored point with respect to L in points.
    Note: one point can be the mirrored point of multiple overlapped points.
    Note: when the points is empty, return true. Very weird, but this is the expected answer of the OJ..
    Note: any point on L is self-mirrored.

Optimal solution:
Running time: O(n)
Space: O(n)

```
class Solution:
    def isReflected(self, points):
        if not points: return True # very weird, but this is the expected answer of the OJ...
        points = set([ tuple(p) for p in points])
        mid = (min(x for x,y in points) + max(x for x,y in points))/2
        
        for x, y in points:
            mirror_x = mid + (mid-x)
            if (mirror_x, y) not in points: return False
        
        return True
```
</p>


