---
title: "Push Dominoes"
weight: 782
#id: "push-dominoes"
---
## Description
<div class="description">
<p>There are<font face="monospace">&nbsp;<code>N</code></font> dominoes in a line, and we place each domino vertically upright.</p>

<p>In the beginning, we simultaneously push&nbsp;some of the dominoes either to the left or to the right.</p>

<p><img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/05/18/domino.png" style="height: 160px;" /></p>

<p>After each second, each domino that is falling to the left pushes the adjacent domino on the left.</p>

<p>Similarly, the dominoes falling to the right push their adjacent dominoes standing on the right.</p>

<p>When a vertical domino has dominoes falling on it from both sides, it stays still due to the balance of the forces.</p>

<p>For the purposes of this question, we will consider that a falling domino&nbsp;expends no additional force to a falling or already fallen domino.</p>

<p>Given a string &quot;S&quot; representing the initial state.&nbsp;<code>S[i] = &#39;L&#39;</code>, if the i-th domino has been pushed to the left; <code>S[i] = &#39;R&#39;</code>, if the i-th domino has been pushed to the right; <code>S[i] = &#39;.&#39;</code>,&nbsp;if the <code>i</code>-th domino has not been pushed.</p>

<p>Return a string representing the final state.&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>&quot;.L.R...LR..L..&quot;
<strong>Output: </strong>&quot;LL.RR.LLRRLL..&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>&quot;RR.L&quot;
<strong>Output: </strong>&quot;RR.L&quot;
<strong>Explanation: </strong>The first domino expends no additional force on the second domino.
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= N&nbsp;&lt;= 10^5</code></li>
	<li>String&nbsp;<code>dominoes</code> contains only&nbsp;<code>&#39;L</code>&#39;, <code>&#39;R&#39;</code> and <code>&#39;.&#39;</code></li>
</ol>

</div>

## Tags
- Two Pointers (two-pointers)
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Adjacent Symbols [Accepted]

**Intuition**

Between every group of vertical dominoes (`'.'`), we have up to two non-vertical dominoes bordering this group.  Since additional dominoes outside this group do not affect the outcome, we can analyze these situations individually: there are 9 of them (as the border could be empty). Actually, if we border the dominoes by `'L'` and `'R'`, there are only 4 cases.  We'll write new letters between these symbols depending on each case.

**Algorithm**

Continuing our explanation, we analyze cases:

* If we have say `"A....B"`, where A = B, then we should write `"AAAAAA"`.

* If we have `"R....L"`, then we will write `"RRRLLL"`, or `"RRR.LLL"` if we have an odd number of dots.  If the initial symbols are at positions `i` and `j`, we can check our distance `k-i` and `j-k` to decide at position `k` whether to write `'L'`, `'R'`, or `'.'`.

* (If we have `"L....R"` we don't do anything.  We can skip this case.)

<iframe src="https://leetcode.com/playground/V4MvnMfa/shared" frameBorder="0" width="100%" height="500" name="V4MvnMfa"></iframe>

**Complexity Analysis**

* Time and Space Complexity:  $$O(N)$$, where $$N$$ is the length of `dominoes`.

---
#### Approach #2: Calculate Force [Accepted]

**Intuition**

We can calculate the net force applied on every domino.  The forces we care about are how close a domino is to a leftward `'R'`, and to a rightward `'L'`: the closer we are, the stronger the force.

**Algorithm**

Scanning from left to right, our force decays by 1 every iteration, and resets to `N` if we meet an `'R'`, so that `force[i]` is higher (than `force[j]`) if and only if `dominoes[i]` is closer (looking leftward) to `'R'` (than `dominoes[j]`).

Similarly, scanning from right to left, we can find the force going rightward (closeness to `'L'`).

For some domino `answer[i]`, if the forces are equal, then the answer is `'.'`.  Otherwise, the answer is implied by whichever force is stronger.

**Example**

Here is a worked example on the string `S = 'R.R...L'`:  We find the force going from left to right is `[7, 6, 7, 6, 5, 4, 0]`.  The force going from right to left is `[0, 0, 0, -4, -5, -6, -7]`.  Combining them (taking their vector addition), the combined force is `[7, 6, 7, 2, 0, -2, -7]`, for a final answer of `RRRR.LL`.

<iframe src="https://leetcode.com/playground/GRQpyP7Z/shared" frameBorder="0" width="100%" height="500" name="GRQpyP7Z"></iframe>

**Complexity Analysis**

* Time and Space Complexity:  $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Two Pointers
- Author: lee215
- Creation Date: Sun May 20 2018 11:24:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 14 2020 13:45:44 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**:
Whether be pushed or not, depend on the shortest distance to \'L\' and \'R\'.
Also the direction matters.
Base on this idea, you can do the same thing inspired by this problem.
https://leetcode.com/problems/shortest-distance-to-a-character/discuss/125788/

Here is another idea is focus on only \'L\' and \'R\'.
<br>

# **Complexity**:
Time `O(N)`
Space `O(N)`
<br>

**Java:**
```java
    public String pushDominoes(String d) {
        d = \'L\' + d + \'R\';
        StringBuilder res = new StringBuilder();
        for (int i = 0, j = 1; j < d.length(); ++j) {
            if (d.charAt(j) == \'.\') continue;
            int middle = j - i - 1;
            if (i > 0)
                res.append(d.charAt(i));
            if (d.charAt(i) == d.charAt(j))
                for (int k = 0; k < middle; k++)
                    res.append(d.charAt(i));
            else if (d.charAt(i) == \'L\' && d.charAt(j) == \'R\')
                for (int k = 0; k < middle; k++)
                    res.append(\'.\');
            else {
                for (int k = 0; k < middle / 2; k++)
                    res.append(\'R\');
                if (middle % 2 == 1)
                    res.append(\'.\');
                for (int k = 0; k < middle / 2; k++)
                    res.append(\'L\');
            }
            i = j;
        }
        return res.toString();
    }
```
**C++:**
```cpp
    string pushDominoes(string d) {
        d = \'L\' + d + \'R\';
        string res = "";
        for (int i = 0, j = 1; j < d.length(); ++j) {
            if (d[j] == \'.\') continue;
            int middle = j - i - 1;
            if (i > 0)
                res += d[i];
            if (d[i] == d[j])
                res += string(middle, d[i]);
            else if (d[i] == \'L\' && d[j] == \'R\')
                res += string(middle, \'.\');
            else
                res += string(middle / 2, \'R\') + string(middle % 2, \'.\') + string(middle / 2, \'L\');
            i = j;
        }
        return res;
    }
```
**Python:**
```py
    def pushDominoes(self, d):
        d = \'L\' + d + \'R\'
        res = ""
        i = 0
        for j in range(1, len(d)):
            if d[j] == \'.\':
                continue
            middle = j - i - 1
            if i:
                res += d[i]
            if d[i] == d[j]:
                res += d[i] * middle
            elif d[i] == \'L\' and d[j] == \'R\':
                res += \'.\' * middle
            else:
                res += \'R\' * (middle / 2) + \'.\' * (middle % 2) + \'L\' * (middle / 2)
            i = j
        return res
```

</p>


### Java, one pass, in-place, 13ms
- Author: climberig
- Creation Date: Sun May 20 2018 23:20:23 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 14:46:22 GMT+0800 (Singapore Standard Time)

<p>
Keep track of last seen \'L\' and \'R\' as indices (variables L and R).
1. If you see \'R\' and R > L, you have R....R, turn everything to \'R\'.
2. If you see \'R\' and R < L, you have L...R and you don\'t need to do anything.
3. If you see \'L\' and L > R, you have L....L, turn everything to \'L\'.
4. if you see \'L\' and L < R, you have R....L, have to pointers from both sides, lo and hi, turn a[lo]=\'R\' and a[hi] = \'L\', increment lo, decrement hi, make sure you do nothing when lo=hi
5. Watch out for edge cases. Note i<=dominoes.length(), this is to deal with L.. Also note L and R are initialized to -1, not 0.
```java
 public String pushDominoes(String dominoes) {
        char[] a = dominoes.toCharArray();
        int L = -1, R = -1;//positions of last seen L and R
        for (int i = 0; i <= dominoes.length(); i++)
            if (i == a.length || a[i] == \'R\') {
                if (R > L)//R..R, turn all to R
                    while (R < i)
                        a[R++] = \'R\';
                R = i;
            } else if (a[i] == \'L\')
                if (L > R || (R == -1 && L == -1))//L..L, turn all to L
                    while (++L < i)
                        a[L] = \'L\';
                else { //R...L
                    L = i;
                    int lo = R + 1, hi = L - 1;
                    while (lo < hi) { //one in the middle stays \'.\'
                        a[lo++] = \'R\';
                        a[hi--] = \'L\';
                    }
                }
        return new String(a);
    }
</p>


### funny idea but passed within time limit (Python)
- Author: yang130
- Creation Date: Sun May 20 2018 11:19:40 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 21 2018 15:22:44 GMT+0800 (Singapore Standard Time)

<p>
I think the code is self-explanatory: just imaging what would happen in each turn.
```
class Solution(object):
    def pushDominoes(self, dominoes):
        """
        :type dominoes: str
        :rtype: str
        """
        while(True):
            new = dominoes.replace(\'R.L\', \'S\')
            new = new.replace(\'.L\',\'LL\').replace(\'R.\',\'RR\')
            if new == dominoes:
                break
            else:
                dominoes = new
        return dominoes.replace(\'S\', \'R.L\')
 ```
</p>


