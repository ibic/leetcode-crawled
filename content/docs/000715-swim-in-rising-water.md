---
title: "Swim in Rising Water"
weight: 715
#id: "swim-in-rising-water"
---
## Description
<div class="description">
<p>On an N x N <code>grid</code>, each square <code>grid[i][j]</code> represents the elevation at that point <code>(i,j)</code>.</p>

<p>Now rain starts to fall. At time <code>t</code>, the depth of the water everywhere is <code>t</code>. You can swim from a square to another 4-directionally adjacent square if and only if the elevation of both squares individually are&nbsp;at most&nbsp;<code>t</code>. You can swim infinite distance in zero time. Of course, you must stay within the boundaries of the grid during your swim.</p>

<p>You start at the top left square <code>(0, 0)</code>. What is the least time until you can reach the bottom right square <code>(N-1, N-1)</code>?</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> [[0,2],[1,3]]
<strong>Output:</strong> 3
<strong>Explanation:</strong>
At time <code>0</code>, you are in grid location <code>(0, 0)</code>.
You cannot go anywhere else because 4-directionally adjacent neighbors have a higher elevation than t = 0.

You cannot reach point <code>(1, 1)</code> until time <code>3</code>.
When the depth of water is <code>3</code>, we can swim anywhere inside the grid.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> [[0,1,2,3,4],[24,23,22,21,5],[12,13,14,15,16],[11,17,18,19,20],[10,9,8,7,6]]
<strong>Output:</strong> 16
<strong>Explanation:</strong>
<strong> 0  1  2  3  4</strong>
24 23 22 21  <strong>5</strong>
<strong>12 13 14 15 16</strong>
<strong>11</strong> 17 18 19 20
<strong>10  9  8  7  6</strong>

The final route is marked in bold.
We need to wait until time 16 so that (0, 0) and (4, 4) are connected.
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li><code>2 &lt;= N &lt;= 50</code>.</li>
	<li>grid[i][j] is a permutation of [0, ..., N*N - 1].</li>
</ol>

</div>

## Tags
- Binary Search (binary-search)
- Heap (heap)
- Depth-first Search (depth-first-search)
- Union Find (union-find)

## Companies
- Google - 2 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Heap [Accepted]

**Intuition and Algorithm**

Let's keep a priority queue of which square we can walk in next.  We always walk in the smallest one that is 4-directionally adjacent to ones we've visited.

When we reach the target, the largest number we've visited so far is the answer.

<iframe src="https://leetcode.com/playground/9tu9wN4y/shared" frameBorder="0" width="100%" height="500" name="9tu9wN4y"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2 \log N)$$.  We may expand $$O(N^2)$$ nodes, and each one requires $$O(\log N)$$ time to perform the heap operations.

* Space Complexity:  $$O(N^2)$$, the maximum size of the heap.

<br/>
<br/>

---
#### Approach #2: Binary Search and DFS [Accepted]

**Intuition and Algorithm**

Whether the swim is possible is a monotone function with respect to time, so we can binary search this function for the correct time: the smallest `T` for which the swim is possible.

Say we guess that the correct time is `T`.  To check whether it is possible, we perform a simple depth-first search where we can only walk in squares that are at most `T`.

<iframe src="https://leetcode.com/playground/PwRcHTRt/shared" frameBorder="0" width="100%" height="500" name="PwRcHTRt"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2 \log N)$$.  Our depth-first search during a call to `possible` is $$O(N^2)$$, and we make up to $$O(\log N)$$ of them.

* Space Complexity:  $$O(N^2)$$, the maximum size of the stack.

<br/>
<br/>

---
#### Approach #3: Minimal Spanning Tree Algorithm (Union-Find)

**Intuition**

One can formulate the problem as building a _[minimum spanning tree](https://en.wikipedia.org/wiki/Minimum_spanning_tree)_, _i.e._ find _minimum_ edges to connect all nodes in a graph.

In our case, we would like to find the _fastest_ path (_i.e._ minimal waiting time) to reach from the top-left point to the bottom-right point of the grid.

>Rather than finding the _spanning tree_ to connect all cells in the grid, we are only interested to find the spanning that connect our starting and ending points.

One of the well-known algorithms for the minimal spanning tree problem is called [Kruskal's algorithm](https://en.wikipedia.org/wiki/Kruskal%27s_algorithm), proposed by [Joseph Kruskal](https://en.wikipedia.org/wiki/Joseph_Kruskal) in 1956.

So the idea of this solution is to adapt the Kruskal's algorithm to solve our problem here.

**Algorithm**

The essence of the Kruskal's algorithm relies on the [disjoint-set data structure](https://en.wikipedia.org/wiki/Disjoint-set_data_structure), also known as **_union-find_** data structure, which tracks a set of elements partitioned into a number of disjoint (non-overlapping) subsets.

>The Kruskal's algorithm starts from a list of sets, where each set contains only a single node, it then **_greedily_** merges the sets together until there is only one set left _i.e._ the minimal spanning tree.

Essentially, the algorithm can be implemented with two major functions: `union(a, b)` and `find(a)`.
With the `union(a, b)` function, we merge two sets together. And with the `find(a)` function, we find the _**representative**_ (also known as parent) of the set.

Here are some overall steps of an adapted Kruskal's algorithm:

- We consider each cell in the grid as a node in a graph, and the value in the cell represents its *weight*.

- We then sort the cells based on their weights, in the ascending order.

- We then iterate through the sorted cells.

    - At each iteration, we add the neighbor cells around the current cell to the spanning tree.

    - At any moment, if we find that the starting and ending points are connected thanks to the newly added nodes, we exit the loop. And the weight of the current cell would be the minimal waiting time, before we complete the spanning tree. 

Here is the solution proposed by [leet212](https://github.com/leet212).
Also, many users have proposed similar ideas and solutions in the discussion forum.

<iframe src="https://leetcode.com/playground/UG4gwmgH/shared" frameBorder="0" width="100%" height="500" name="UG4gwmgH"></iframe>

**Complexity Analysis**

Let $$N$$ be the width of the grid (N*N).

* Time Complexity:  $$O(N^2 \log N^2)$$.

    - First of all, we have in total $$N^2$$ cells in the grid.

    - Since we sort cells based on their weights, it would take $$\mathcal{O}(N^2 \log N^2)$$ time for this sorting operation.

    - We iterate through the cells, _i.e._ $$N^2$$ steps. At each step, the `union()` function requires $$\mathcal{O}(\log^{*} N^2)$$ complexity, which one can refer to [the proof](https://en.wikipedia.org/wiki/Proof_of_O(log*n)_time_complexity_of_union%E2%80%93find) for more details.

    - To sum up, the overall time complexity of the algorithm is $$\mathcal{O}(N^2 \log N^2) + \mathcal{O}(N^2 \log^{*} N^2) = \mathcal{O}(N^2 \log N^2)$$.

* Space Complexity:  $$O(N^2)$$. We use some auxiliary data structures that are proportional to the size of the grid, _i.e._ $$N^2$$.

<br/>

---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Python] PriorityQueue
- Author: lee215
- Creation Date: Fri Feb 09 2018 13:16:27 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 22:44:05 GMT+0800 (Singapore Standard Time)

<p>
Python:
```
    def swimInWater(self, grid):
        N, pq, seen, res = len(grid), [(grid[0][0], 0, 0)], set([(0, 0)]), 0
        while True:
            T, x, y = heapq.heappop(pq)
            res = max(res, T)
            if x == y == N - 1:
                return res
            for i, j in [(x + 1, y), (x, y + 1), (x - 1, y), (x, y - 1)]:
                if 0 <= i < N and 0 <= j < N and (i, j) not in seen:
                    seen.add((i, j))
                    heapq.heappush(pq, (grid[i][j], i, j))
```
C++
```
class Solution {
public:

    struct T {
        int t, x, y;
        T(int a, int b, int c) : t (a), x (b), y (c){}
        bool operator< (const T &d) const {return t > d.t;}
    };

    int swimInWater(vector<vector<int>>& grid) {
        int N = grid.size (), res = 0;
        priority_queue<T> pq;
        pq.push(T(grid[0][0], 0, 0));
        vector<vector<int>> seen(N, vector<int>(N, 0));
        seen[0][0] = 1;
        static int dir[4][2] = {{0, 1}, {0, -1}, {1, 0}, { -1, 0}};

        while (true) {
            auto p = pq.top ();
            pq.pop ();
            res = max(res, p.t);
            if (p.x == N - 1 && p.y == N - 1) return res;
            for (auto& d : dir) {
                int i = p.x + d[0], j = p.y + d[1];
                if (i >= 0 && i < N && j >= 0 && j < N && !seen[i][j]) {
                    seen[i][j] = 1;
                    pq.push (T(grid[i][j], i, j));
                }
            }
        }
    }
};
```
</p>


### C++ two solutions, Binary Search+DFS and Dijkstra+BFS, O(n^2logn), 11ms
- Author: zestypanda
- Creation Date: Mon Feb 05 2018 00:58:57 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 20:33:49 GMT+0800 (Singapore Standard Time)

<p>
Binary Search + DFS, O(n^2logn), 14ms
Binary Search range [0, n*n-1] to find the minimum feasible water level. For each water level, verification using DFS or BFS is O(n^2). DFS is slightly faster in practice.
```
class Solution {
public:
    int swimInWater(vector<vector<int>>& grid) {
        int n = grid.size();
        int low = grid[0][0], hi = n*n-1;
        while (low < hi) {
            int mid = low + (hi-low)/2;
            if (valid(grid, mid)) 
               hi = mid;
            else
               low = mid+1;
        }
        return low;
    }
private:
    bool valid(vector<vector<int>>& grid, int waterHeight) {
        int n = grid.size();
        vector<vector<int>> visited(n, vector<int>(n, 0));
        vector<int> dir({-1, 0, 1, 0, -1});
        return dfs(grid, visited, dir, waterHeight, 0, 0, n);
    }
    bool dfs(vector<vector<int>>& grid, vector<vector<int>>& visited, vector<int>& dir, int waterHeight, int row, int col, int n) {
        visited[row][col] = 1;
        for (int i = 0; i < 4; ++i) {
            int r = row + dir[i], c = col + dir[i+1];
            if (r >= 0 && r < n && c >= 0 && c < n && visited[r][c] == 0 && grid[r][c] <= waterHeight) {
                if (r == n-1 && c == n-1) return true;
                if (dfs(grid, visited, dir, waterHeight, r, c, n)) return true;
            }
        }
        return false;
    }
};
```
Dijkstra using Priority Queue, O(n^2logn), 20 ms;
In every step, find lowest water level to move forward, so using PQ rather than queue
```
class Solution {
public:
    int swimInWater(vector<vector<int>>& grid) {
        int n = grid.size(), ans = max(grid[0][0], grid[n-1][n-1]);
        priority_queue<vector<int>, vector<vector<int>>, greater<vector<int>>> pq;
        vector<vector<int>> visited(n, vector<int>(n, 0));
        visited[0][0] = 1;
        vector<int> dir({-1, 0, 1, 0, -1});
        pq.push({ans, 0, 0});
        while (!pq.empty()) {
            auto cur = pq.top();
            pq.pop();
            ans = max(ans, cur[0]);
            for (int i = 0; i < 4; ++i) {
                int r = cur[1] + dir[i], c = cur[2] + dir[i+1];
                if (r >= 0 && r < n && c >= 0 && c < n && visited[r][c] == 0) {
                    if (r == n-1 && c == n-1) return ans;
                    pq.push({grid[r][c], r, c});
                    visited[r][c] = 1;
                }
            }
        }
        return -1;
    }
};
```
Dijkstra with BFS optimization, O(n^2logn), 11 ms
Similar to above solution, but we can use BFS, which is more efficient, to expand reachable region.
```
class Solution {
public:
    int swimInWater(vector<vector<int>>& grid) {
        int n = grid.size(), ans = max(grid[0][0], grid[n-1][n-1]);
        priority_queue<vector<int>, vector<vector<int>>, greater<vector<int>>> pq;
        vector<vector<int>> visited(n, vector<int>(n, 0));
        visited[0][0] = 1;
        vector<int> dir({-1, 0, 1, 0, -1});
        pq.push({ans, 0, 0});
        while (!pq.empty()) {
            auto cur = pq.top();
            pq.pop();
            ans = max(ans, cur[0]);
            queue<pair<int, int>> myq;
            myq.push({cur[1], cur[2]});
            while (!myq.empty()) {
                auto p = myq.front();
                myq.pop();
                if (p.first == n-1 && p.second == n-1) return ans;
                for (int i = 0; i < 4; ++i) {
                    int r = p.first + dir[i], c = p.second + dir[i+1];
                    if (r >= 0 && r < n && c >= 0 && c < n && visited[r][c] == 0) {
                        visited[r][c] = 1;
                        if (grid[r][c] <= ans) 
                           myq.push({r, c});
                        else
                           pq.push({grid[r][c], r, c});
                    }
                }
            }
        }
        return -1;
    }
};
```
</p>


### Java - DFS and Union Find
- Author: AdaZhang
- Creation Date: Thu Mar 08 2018 10:42:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 04:25:33 GMT+0800 (Singapore Standard Time)

<p>
DFS:
```
class Solution {
    public int swimInWater(int[][] grid) {
        int time = 0;
        int N = grid.length;
        Set<Integer> visited = new HashSet<>();
        while(!visited.contains(N*N-1)) {
            visited.clear();
            dfs(grid, 0, 0, time, visited);
            time++;
        }
        return time - 1;
    }
    int[][] dirs = {{-1,0},{1,0},{0,1},{0,-1}};
    private void dfs(int[][] grid, int i, int j, int time, Set<Integer> visited) {
        if (i < 0 || i > grid.length - 1 || j < 0 || j > grid[0].length - 1 || grid[i][j] > time || visited.contains(i*grid.length+j)) return;
        visited.add(i*grid.length+j);
        for (int[] dir : dirs) {
            dfs(grid, i+dir[0], j+dir[1], time, visited);
        }
    }
}
```
Union Find:
```
class Solution {
    class UF {
        int[] id;
        public UF(int N) {
            id = new int[N];
            for (int i = 0; i < N; i++) {
                id[i] = i;
            }
        }
        public int root(int i) {
            while (i != id[i]) {
                id[i] = id[id[i]];
                i = id[i];
            }
            return i;
        }
        public boolean isConnected(int p, int q) {
            return root(p)==root(q);
        }
        public void union(int p, int q) {
            if (isConnected(p, q)) return;
            id[root(p)] = root(q);
        }
    }
    public int swimInWater(int[][] grid) {
        int N = grid.length;
        UF uf = new UF(N*N);
        int time = 0;
        while(!uf.isConnected(0, N*N-1)) {
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    if (grid[i][j] > time) continue;
                    if (i < N-1 && grid[i+1][j]<=time) uf.union(i*N+j, i*N+j+N);
                    if (j < N-1 && grid[i][j+1]<=time) uf.union(i*N+j, i*N+j+1);
                }
            }
            time++;
        }
        return time - 1;
    }
}
```
</p>


