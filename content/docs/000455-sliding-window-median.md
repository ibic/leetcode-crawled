---
title: "Sliding Window Median"
weight: 455
#id: "sliding-window-median"
---
## Description
<div class="description">
<p>Median is the middle value in an ordered integer list. If the size of the list is even, there is no middle value. So the median is the mean of the two middle value.</p>
Examples:

<p><code>[2,3,4]</code> , the median is <code>3</code></p>

<p><code>[2,3]</code>, the median is <code>(2 + 3) / 2 = 2.5</code></p>

<p>Given an array <i>nums</i>, there is a sliding window of size <i>k</i> which is moving from the very left of the array to the very right. You can only see the <i>k</i> numbers in the window. Each time the sliding window moves right by one position. Your job is to output the median array for each window in the original array.</p>

<p>For example,<br />
Given <i>nums</i> = <code>[1,3,-1,-3,5,3,6,7]</code>, and <i>k</i> = 3.</p>

<pre>
Window position                Median
---------------               -----
[1  3  -1] -3  5  3  6  7       1
 1 [3  -1  -3] 5  3  6  7       -1
 1  3 [-1  -3  5] 3  6  7       -1
 1  3  -1 [-3  5  3] 6  7       3
 1  3  -1  -3 [5  3  6] 7       5
 1  3  -1  -3  5 [3  6  7]      6
</pre>

<p>Therefore, return the median sliding window as <code>[1,-1,-1,3,5,6]</code>.</p>

<p><b>Note: </b><br />
You may assume <code>k</code> is always valid, ie: <code>k</code> is always smaller than input array&#39;s size for non-empty array.<br />
Answers within&nbsp;<code>10^-5</code>&nbsp;of the actual value will be accepted as correct.</p>

</div>

## Tags
- Sliding Window (sliding-window)

## Companies
- Facebook - 5 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Spotify - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Indeed - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

### A word of advice

This problem is a companion problem to [295. Find Median From Data Stream](https://leetcode.com/problems/find-median-from-data-stream/). This means that a lot of approaches to solve this problem are based on the [methods to solve 295. Find Median From Data Stream](https://leetcode.com/articles/find-median-from-data-stream). Perhaps try that problem before you approach this one.

## Solution
---
#### Approach 1: Simple Sorting

**Intuition**

Do what the question says.

**Algorithm**

Store the numbers in a window container of size $$k$$. The following operations must take place:

1. **Inserting** the incoming element.
2. **Deleting** the outgoing element.
3. **Sorting** the window to find the medians.

One primitive approach is to copy $$k$$ consecutive elements from the input to the window and keep sorting these every time. This constitutes duplication of effort.

We can do a bit better if we instead *insert* and *delete* one element per window shift. The challenge then is to maintain the window as **sorted**, before and after the *insert* and *delete* operations.

<iframe src="https://leetcode.com/playground/q9RnNZrD/shared" frameBorder="0" width="100%" height="395" name="q9RnNZrD"></iframe>

Python comes with an excellent `bisect` module to help perform efficient insert operations on lists while maintaining their sorted property.

**Complexity Analysis**

* Time complexity: $$O(n \cdot k \log k)$$ to $$O(n \cdot k)$$.

    + Copying elements into the container takes about $$O(k)$$ time each. This happens about $$(n-k)$$ times.
    + Sorting for each of the $$(n-k)$$ sliding window instances takes about $$O(k \log k)$$ time each.

    + Bisected insertion or deletion takes about $$O(\log k)$$ for searching and $$O(k)$$ for actual shifting of elements. This takes place about $$n$$ times.

* Space complexity: $$O(k)$$ extra linear space for the window container.

---
#### Approach 2: Two Heaps (Lazy Removal)

**Intuition**

The idea is the same as [Approach 3](https://leetcode.com/articles/find-median-from-data-stream/#approach-3-two-heaps) from [295. Find Median From Data Stream](https://leetcode.com/problems/find-median-from-data-stream/). The only additional requirement is removing the outgoing elements from the window.

Since the window elements are stored in heaps, deleting elements that are not at the top of the heaps is a pain.

Some languages (like **Java**) provide implementations of the `PriorityQueue` class that allow for removing *arbitrarily* placed elements. Generally, using such features is not efficient nor is their portability assured.

Assuming that only the tops of heaps (and by extension the `PriorityQueue` class) are accessible, we need to find a way to efficiently invalidate and remove elements that are moving out of the sliding window.

At this point, an important thing to notice is the fact that if the two heaps are balanced, only the top of the heaps are actually needed to find the medians. This means that as long as we can somehow keep the heaps balanced, we could also keep some extraneous elements.

Thus, we can use hash-tables to keep track of invalidated elements. Once they reach the heap tops, we remove them from the heaps. This is the lazy removal technique.

An immediate challenge at this point is balancing the heaps while keeping extraneous elements. This is done by actually moving some elements to the heap which has extraneous elements, from the other heap. This cancels out the effect of having extraneous elements and maintains the invariant that the heaps are balanced.

**NOTE:** When we talk about keeping the heaps balanced, we are **not** referring to the actual heap sizes. We are only concerned with valid elements and hence when we talk about balancing heaps, we are referring to count of such elements.

**Algorithm**

+ Two priority queues:

    1. A max-heap `lo` to store the smaller half of the numbers
    2. A min-heap `hi` to store the larger half of the numbers

+ A hash-map or hash-table `hash_table` for keeping track of invalid numbers. It holds the count of the occurrences of all such numbers that have been invalidated and yet remain in the heaps.

+ The max-heap `lo` is allowed to store, at worst, one more element more than the min-heap `hi`. Hence if we have processed $$k$$ elements:

    + If $$k = 2 \cdot n + 1 \quad (\forall \, n \in \mathbb{Z})$$, then `lo` is allowed to hold $$n+1$$ elements, while `hi` can hold $$n$$ elements.
    + If $$k = 2 \cdot n \quad (\forall \, n \in \mathbb{Z})$$, then both heaps are balanced and hold $$n$$ elements each.

    This gives us the nice property that when the heaps are perfectly balanced, the median can be derived from the tops of both heaps. Otherwise, the top of the max-heap `lo` holds the legitimate median.

**NOTE:** As mentioned before, when we are talking about keeping the heaps balanced, the actual sizes of the heaps are irrelevant. Only the count of valid elements in both heaps matter.

+ Keep a `balance` factor. It indicates three situations:

    + `balance` $$= 0$$: Both heaps are balanced or nearly balanced.
    + `balance` $$< 0$$: `lo` needs more valid elements. Elements from `hi` are moved to `lo`.
    + `balance` $$> 0$$: `hi` needs more valid elements. Elements from `lo` are moved to `hi`.

+ Inserting an incoming number `in_num`:

    + If `in_num` is less than or equal to the top element of `lo`, then it can be inserted to `lo`. However this unbalances `hi` (`hi` has lesser valid elements now). Hence `balance` is incremented.

    + Otherwise, `in_num` must be added to `hi`. Obviously, now `lo` is unbalanced. Hence `balance` is decremented.

+ Lazy removal of an outgoing number `out_num`:

    + If `out_num` is present in `lo`, then invalidating this occurrence will unbalance `lo` itself. Hence `balance` must be decremented.
    + If `out_num` is present in `hi`, then invalidating this occurrence will unbalance `hi` itself. Hence `balance` must be incremented.

    + We increment the count of this element in the hash_table table.
    + Once an invalid element reaches either of the heap tops, we remove them and decrement their counts in the hash_table table.

<iframe src="https://leetcode.com/playground/xbazQQ6F/shared" frameBorder="0" width="100%" height="500" name="xbazQQ6F"></iframe>

**Complexity Analysis**

* Time complexity: $$O(2 \cdot n \log k) + O(n-k) \approx O(n \log k)$$.

    + Either (or sometimes both) of the heaps gets every element inserted into it at least once. Collectively each of those takes about $$O(\log k)$$ time. That is $$n$$ such insertions.
    + About $$(n-k)$$ removals from the top of the heaps take place (the number of sliding window instances). Each of those takes about $$O(\log k)$$ time.
    + Hash table operations are assumed to take $$O(1)$$ time each. This happens roughly the same number of times as removals from heaps take place.

* Space complexity: $$O(k) + O(n) \approx O(n)$$ extra linear space.

    + The heaps collectively require $$O(k)$$ space.
    + The hash table needs about $$O(n-k)$$ space.

---
#### Approach 3: Two Multisets

**Intuition**

One can see that `multiset`s are a great way to keep elements sorted while providing efficient access to the first and last elements. Inserting and deleting arbitrary elements are also fairly efficient operations in a `multiset`. (Refer to [Approach 4 Intuition](https://leetcode.com/articles/find-median-from-data-stream/#approach-4-multiset-and-two-pointers) for [295. Find Median From Data Stream](https://leetcode.com/problems/find-median-from-data-stream/))

Thus, if the previous approach gives you too much heartburn, consider replacing the use of `priority_queue` with `multiset`.

**Algorithm**

Inserting or deleting an element is straight-forward. Balancing the heaps takes the same route as [Approach 3](https://leetcode.com/articles/find-median-from-data-stream/#approach-3-two-heaps) of [295. Find Median From Data Stream](https://leetcode.com/problems/find-median-from-data-stream/).

<iframe src="https://leetcode.com/playground/gmSYcxrV/shared" frameBorder="0" width="100%" height="500" name="gmSYcxrV"></iframe>

**Complexity Analysis**

* Time complexity: $$O((n-k) \cdot 6 \cdot \log k) \approx O(n \log k)$$.

    + At worst, there are three set insertions and three set deletions from the start or end. Each of these takes about $$O(\log k)$$ time.
    + Finding the mean takes constant $$O(1)$$ time since the start or ends of sets are directly accessible.
    + Each of these steps takes place about $$(n-k)$$ times (the number of sliding window instances).

* Space complexity: $$O(k)$$ extra linear space to hold contents of the window.

---
#### Approach 4: Multiset and Two Pointers

**Intuition**

This is same as [Approach 4](https://leetcode.com/articles/find-median-from-data-stream/#approach-4-multiset-and-two-pointers) for [295. Find Median From Data Stream](https://leetcode.com/problems/find-median-from-data-stream/).

> But, we don't actually need two pointers.

Median elements are derived using a single iterator position (when the window size $$k$$ is odd) or two consecutive iterator positions (when $$k$$ is even). Hence keeping track of *only* one pointer is sufficient. The other pointer can be *implicitly derived* when required.

**Algorithm**

+ A single iterator `mid`, which iterates over the `window` multiset. It is analogous to `upper_median` in [Approach 4](https://leetcode.com/articles/find-median-from-data-stream/#approach-4-multiset-and-two-pointers) for [295. Find Median From Data Stream](https://leetcode.com/problems/find-median-from-data-stream/). `lower_median` is *implicitly derived* from `mid`. It's either equal to `mid` (when the window size $$k$$ is odd) or `prev(mid)` [^note-1] \(when the window size $$k$$ is even\).

+ We start with populating our multiset `window` with the first $$k$$ elements. We set `mid` to the $$\lfloor k/2 \rfloor$$ indexed element in `window` (`0`-based indexing; Multisets always maintain their sorted property).

+ While inserting an element `num` into `window`, three cases arise:

    1. `num` is less than the value of upper median `mid`.

    2. `num` is greater than the value of upper median `mid`.

    3. `num` is equal to the value of upper median `mid`. This situation is often handled as language-dependent. Since C++ `multiset` insert elements at the end of their equal range, this situation is essentially the same as the previous case.

    + For the first case, `num` is inserted before the upper median element `mid`. Thus `mid` now, no longer points to the $$\lfloor k/2 \rfloor$$ indexed element. In fact it points to the $$\lfloor k/2 \rfloor + 1$$ indexed element. We fix that by decrementing `mid`.

    + For the second and third cases, `num` is inserted after the upper median element `mid` and hence does not spoil the `mid` iterator. It still points to the $$\lfloor k/2 \rfloor$$ indexed element.

    + Of course, the window size just increased to $$k + 1$$ in all three cases. That will easily be fixed by removing the element that is about to exit the window.

+ While removing an element `num`, the same three cases arise as when we wanted to insert an element:

    1. `num` is less than the value of upper median `mid`.

    2. `num` is greater than the value of upper median `mid`.

    3. `num` is equal to the value of upper median `mid`. Since `mid` will point to the first occurrence of `num` in the multiset `window` and we deterministically remove the first occurrence (take note that we use `std::multiset::lower_bound()` [^note-2] to find the correct occurrence to erase), this case is handled in the same manner as the first case.

    + In the first and third cases, removing `num` will spoil the `mid` iterator. Thus we need to fix that by incrementing `mid` before we remove that element.

    + For the second case, the `mid` iterator is not spoiled. No further action is required.

    + Once this element has been removed, the window size returns to being $$k$$.

+ After insertion of the incoming element and removal of the outgoing element, we are left again with some nice invariants:

    1. Window size is again $$k$$.
    2. The window is still fully sorted.
    3. `mid` still points to the $$\lfloor k/2 \rfloor$$ indexed element.

+ Finding the median of the window is easy! It is simply the **mean** of the elements pointed to by the two pointers `lo_median` and `hi_median`. In our case those are `mid` or `prev(mid)` (as decided by whether $$k$$ is odd or even) , and `mid` respectively.

[^note-3]

<iframe src="https://leetcode.com/playground/9yEsEfTi/shared" frameBorder="0" width="100%" height="500" name="9yEsEfTi"></iframe>

**Complexity Analysis**

* Time complexity: $$O((n-k) \log k) + O(k) \approx O(n \log k)$$.

    + Initializing `mid` takes about $$O(k)$$ time.
    + Inserting or deleting a number takes $$O(\log k)$$ time for a standard `multiset` scheme. [^note-4]
    + Finding the mean takes constant $$O(1)$$ time since the median elements are directly accessible from `mid` iterator.
    + The last two steps take place about $$(n-k)$$ times (the number of sliding window instances).

* Space complexity: $$O(k)$$ extra linear space to hold contents of the window.

---
#### Further Thoughts

As noted before, this problem is essentially an extension to [295. Find Median From Data Stream](https://leetcode.com/problems/find-median-from-data-stream/). That problem had a lot of ways to go about, that frankly, are not of much use in an interview. But they are interesting to follow all the same. If you are interested take a look [here.](https://leetcode.com/articles/find-median-from-data-stream#further-thoughts) Try extending those methods to this problem.



[^note-1]: `std::prev()` is a C++ method to find the previous element to the current one being pointed to by an iterator.
[^note-2]: Had we used `std::multiset::find()`, there was no guarantee that the first occurrence of `num` would be found. Although the contrary did happen in all of our tests, I don't recommend using it. Your mileage may vary.
[^note-3]: Shout-out to [@votrubac](https://leetcode.com/votrubac/) and [@StefanPochmann](https://leetcode.com/stefanpochmann)!
[^note-4]: [Hinting](http://en.cppreference.com/w/cpp/container/multiset/insert) can reduce that to amortized constant $$O(1)$$ time.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### O(n log k) C++ using multiset and updating middle-iterator
- Author: StefanPochmann
- Creation Date: Wed Jan 11 2017 09:58:51 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 18 2018 06:10:43 GMT+0800 (Singapore Standard Time)

<p>
Keep the window elements in a multiset and keep an iterator pointing to the middle value (to "index" k/2, to be precise). Thanks to [@votrubac's solution and comments](https://discuss.leetcode.com/topic/74739/c-95-ms-single-multiset-o-n-log-n).

    vector<double> medianSlidingWindow(vector<int>& nums, int k) {
        multiset<int> window(nums.begin(), nums.begin() + k);
        auto mid = next(window.begin(), k / 2);
        vector<double> medians;
        for (int i=k; ; i++) {
    
            // Push the current median.
            medians.push_back((double(*mid) + *prev(mid, 1 - k%2)) / 2);
    
            // If all done, return.
            if (i == nums.size())
                return medians;
                
            // Insert nums[i].
            window.insert(nums[i]);
            if (nums[i] < *mid)
                mid--;
    
            // Erase nums[i-k].
            if (nums[i-k] <= *mid)
                mid++;
            window.erase(window.lower_bound(nums[i-k]));
        }
    }
</p>


### Java using two Tree Sets - O(n logk)
- Author: KidOptimo
- Creation Date: Mon Feb 13 2017 15:04:26 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 06:34:06 GMT+0800 (Singapore Standard Time)

<p>
Inspired by this [solution](https://discuss.leetcode.com/topic/27521/short-simple-java-c-python-o-log-n-o-1). to the problem: [295. Find Median from Data Stream](https://leetcode.com/problems/find-median-from-data-stream/)

However instead of using two priority queue's we use two ```Tree Sets``` as we want ```O(logk)``` for ```remove(element)```. Priority Queue would have been ```O(k)``` for ```remove(element)``` giving us an overall time complexity of ```O(nk)``` instead of ```O(nlogk)```.

However there is an issue when it comes to duplicate values so to get around this we store the ```index``` into ```nums``` in our ```Tree Set```. To break ties in our Tree Set comparator we compare the index.

```
public double[] medianSlidingWindow(int[] nums, int k) {
    Comparator<Integer> comparator = (a, b) -> nums[a] != nums[b] ? Integer.compare(nums[a], nums[b]) : a - b;
    TreeSet<Integer> left = new TreeSet<>(comparator.reversed());
    TreeSet<Integer> right = new TreeSet<>(comparator);
    
    Supplier<Double> median = (k % 2 == 0) ?
        () -> ((double) nums[left.first()] + nums[right.first()]) / 2 :
        () -> (double) nums[right.first()];
    
    // balance lefts size and rights size (if not equal then right will be larger by one)
    Runnable balance = () -> { while (left.size() > right.size()) right.add(left.pollFirst()); };
    
    double[] result = new double[nums.length - k + 1];
    
    for (int i = 0; i < k; i++) left.add(i);
    balance.run(); result[0] = median.get();
    
    for (int i = k, r = 1; i < nums.length; i++, r++) {
        // remove tail of window from either left or right
        if(!left.remove(i - k)) right.remove(i - k);

        // add next num, this will always increase left size
        right.add(i); left.add(right.pollFirst());
        
        // rebalance left and right, then get median from them
        balance.run(); result[r] = median.get();
    }
    
    return result;
}
```
</p>


### Python Small & Large Heaps
- Author: WangQiuc
- Creation Date: Wed Mar 27 2019 07:22:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 03 2019 07:46:13 GMT+0800 (Singapore Standard Time)

<p>
To calculate the median, we can maintain divide array into subarray equally: **small** and **large**. All elements in **small** are no larger than any element in **large**. So median would be (largest in **small** + smallest in **large**) / 2 if **small**\'s size = **large**\'s size. If large\'s size = small\'s size + 1, median is smallest in **large**.

Thus, we can use heap here to maintain **small**(max heap) and **large**(min heap) so we can fetch smallest and largest element in logarithmic time.

We can also maintain "**large**\'s size - **small**\'s size <= 1" and "smallest in **large** >= largest in **small**" by heap\'s property: once **large**\'s size - **small**\'s size > 1, we pop one element from large and add it to small. And vice versa when **small**\'s size > **large**\'s size.

Besides, since its a sliding window median, we need to keep track of window ends. So we will also push element\'s index to the heap. So each element takes a form of (val, index). Since Python\'s heapq is a min heap, so we convert small to a max heap by pushing (-val, index).

Intially for first k elements, we push them all into **small** and then pop k/2 element from **small** and add them to **large**. 
Then we can intialize our answer array as ```[large[0][0] if k & 1 else (large[0][0]-small[0][0])/2]``` as we discussed above.

Then for rest iterations, each time we add a new element **x** whose index is **i+k**, and remove an old element **nums[i]** which is out of window scope. Then we calculate our median in current window as the same way before.
If right end\'s **x** is no smaller than **large[0]**, then it belongs to **large** heap. If left end\'s **nums[i]** is no larger than **large[0]**, then it belongs to **small** heap. So we will add one to **large** while remove one from **small** and heaps\' sizes will be unbalanced. So we will move **large[0]** to **small** to rebalance two heaps.
Vice versa when we have to add one to **small** while remove one from **large**.

But we don\'t have to hurry and remove element in each iteration. As long as **nums[i]** is neither **small[0]** nor **large[0]**, it has no effect to median calculation. So we wait later and use a while loop to remove those out-of-window **small[0]** or **large[0]** at one time. This also make whole logic clearer.
```
def medianSlidingWindow(nums, k):
	small, large = [], []
	for i, x in enumerate(nums[:k]): 
		heapq.heappush(small, (-x,i))
	for _ in range(k-(k>>1)): 
		move(small, large)
	ans = [get_med(small, large, k)]
	for i, x in enumerate(nums[k:]):
		if x >= large[0][0]:
			heapq.heappush(large, (x, i+k))
			if nums[i] <= large[0][0]: 
				move(large, small)
		else:
			heapq.heappush(small, (-x, i+k))
			if nums[i] >= large[0][0]: 
				move(small, large)
		while small and small[0][1] <= i: 
			heapq.heappop(small)
		while large and large[0][1] <= i: 
			heapq.heappop(large)
		ans.append(get_med(small, large, k))
	return ans

def move(h1, h2):
	x, i = heapq.heappop(h1)
	heapq.heappush(h2, (-x, i))
	
def get_med(h1, h2, k):
	return h2[0][0] * 1. if k & 1 else (h2[0][0]-h1[0][0]) / 2.
```
Since we are using k-size heap here, the time complexity is O(nlogk) and space complexity is O(logk).
</p>


