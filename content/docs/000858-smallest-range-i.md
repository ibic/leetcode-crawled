---
title: "Smallest Range I"
weight: 858
#id: "smallest-range-i"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of integers, for each integer <code>A[i]</code> we may choose any <code>x</code> with <code>-K &lt;= x &lt;= K</code>, and add <code>x</code> to <code>A[i]</code>.</p>

<p>After this process, we have some array <code>B</code>.</p>

<p>Return the smallest possible difference between the maximum value of <code>B</code>&nbsp;and the minimum value of <code>B</code>.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[1]</span>, K = <span id="example-input-1-2">0</span>
<strong>Output: </strong><span id="example-output-1">0
<strong>Explanation</strong>: B = [1]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">[0,10]</span>, K = <span id="example-input-2-2">2</span>
<strong>Output: </strong><span id="example-output-2">6
</span><span id="example-output-1"><strong>Explanation</strong>: B = [2,8]</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-3-1">[1,3,6]</span>, K = <span id="example-input-3-2">3</span>
<strong>Output: </strong><span id="example-output-3">0
</span><span id="example-output-1"><strong>Explanation</strong>: B = [3,3,3] or B = [4,4,4]</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 10000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 10000</code></li>
	<li><code>0 &lt;= K &lt;= 10000</code></li>
</ol>
</div>
</div>
</div>
</div>

## Tags
- Math (math)

## Companies
- Adobe - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Mathematical

**Intuition and Algorithm**

Let `A` be the original array, and `B` be the array after all our modifications.  Towards trying to minimize `max(B) - min(B)`, let's try to minimize `max(B)` and maximize `min(B)` separately.

The smallest possible value of `max(B)` is `max(A) - K`, as the value `max(A)` cannot go lower.  Similarly, the largest possible value of `min(B)` is `min(A) + K`.  So the quantity `max(B) - min(B)` is at least `ans = (max(A) - K) - (min(A) + K)`.

We can attain this value (if `ans >= 0`), by the following modifications:

* If $$A[i] \leq \min(A) + K$$, then $$B[i] = \min(A) + K$$
* Else, if $$A[i] \geq \max(A) - K$$, then $$B[i] = \max(A) - K$$
* Else, $$B[i] = A[i]$$.

If `ans < 0`, the best answer we could have is `ans = 0`, also using the same modification.

<iframe src="https://leetcode.com/playground/TQh3pLfK/shared" frameBorder="0" width="100%" height="225" name="TQh3pLfK"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Very confusing question!
- Author: abkosar
- Creation Date: Sun Oct 07 2018 07:54:24 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 03:57:57 GMT+0800 (Singapore Standard Time)

<p>
Sorry but I read the description 15 times and the solution also 2-3 times, and one of the worst stated questions I have ever seen.
</p>


### [C++/Java/Python] Check Max - Min
- Author: lee215
- Creation Date: Sun Sep 23 2018 11:01:46 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 16:33:25 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**:
If `min(A) + K < max(A) - K`, then return `max(A) - min(A) - 2 * K`
If `min(A) + K >= max(A) - K`, then return `0`

**Time Complexity**:
O(N)

**C++:**
```
    int smallestRangeI(vector<int> A, int K) {
        int mx = A[0], mn = A[0];
        for (int a : A) mx = max(mx, a), mn = min(mn, a);
        return max(0, mx - mn - 2 * K);
    }
```
**C#**
```
        return Math.Max(0, A.Max() - A.Min() - 2 * K);
```

**Java:**
```
    public int smallestRangeI(int[] A, int K) {
        int mx = A[0], mn = A[0];
        for (int a : A) {
            mx = Math.max(mx, a);
            mn = Math.min(mn, a);
        }
        return Math.max(0, mx - mn - 2 * K);
    }
```

**Python:**
```
        return max(0, max(A) - min(A) - 2 * K)
```

</p>


### Some clearfication, hope it helps.
- Author: atmosphere77777
- Creation Date: Wed Jan 30 2019 12:12:09 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 27 2020 08:57:12 GMT+0800 (Singapore Standard Time)

<p>
![image](https://assets.leetcode.com/users/atmosphere77777/image_1548821419.png)
Please correct me if I\'m wrong, also forgive my poor drawing skill, my passedArtClass = None.
</p>


