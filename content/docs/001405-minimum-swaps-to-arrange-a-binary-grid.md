---
title: "Minimum Swaps to Arrange a Binary Grid"
weight: 1405
#id: "minimum-swaps-to-arrange-a-binary-grid"
---
## Description
<div class="description">
<p>Given an <code>n&nbsp;x n</code>&nbsp;binary <code>grid</code>, in one step you can choose two <strong>adjacent rows</strong> of the grid and swap them.</p>

<p>A grid is said to be <strong>valid</strong> if all the cells above the main diagonal are <strong>zeros</strong>.</p>

<p>Return <em>the minimum number of steps</em> needed to make the grid valid, or <strong>-1</strong> if the grid cannot be valid.</p>

<p>The main diagonal of a grid is the diagonal that starts at cell <code>(1, 1)</code> and ends at cell <code>(n, n)</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/28/fw.jpg" style="width: 750px; height: 141px;" />
<pre>
<strong>Input:</strong> grid = [[0,0,1],[1,1,0],[1,0,0]]
<strong>Output:</strong> 3
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/16/e2.jpg" style="width: 270px; height: 270px;" />
<pre>
<strong>Input:</strong> grid = [[0,1,1,0],[0,1,1,0],[0,1,1,0],[0,1,1,0]]
<strong>Output:</strong> -1
<strong>Explanation:</strong> All rows are similar, swaps have no effect on the grid.
</pre>

<p><strong>Example 3:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/16/e3.jpg" style="width: 200px; height: 200px;" />
<pre>
<strong>Input:</strong> grid = [[1,0,0],[1,1,0],[1,1,1]]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == grid.length</code></li>
	<li><code>n == grid[i].length</code></li>
	<li><code>1 &lt;= n&nbsp;&lt;= 200</code></li>
	<li><code>grid[i][j]</code> is <code>0</code> or <code>1</code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Ajira - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Min Adjacent Swaps to Sort the array of INTEGERS with Proof
- Author: interviewrecipes
- Creation Date: Sun Aug 02 2020 12:18:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 08 2020 13:06:22 GMT+0800 (Singapore Standard Time)

<p>
**Key Idea**
This problem can be easily converted to a problem of finding number of swaps required to sort the array of integers when only adjacent swaps are allowed.

We all numbers above the main diagonal to be zero. This means that first row should at least have `n-1` zeros at the end, Second row should at least have `n-2` zeros at the end, and so on.

So first we find the zeros at the end of each row and make an integer array. Then simply find the number of swaps required to sort it based on the number of zeros in descending order.

**Note**
A thing to note here is that we don\'t necessarily want the array to be sorted. As long as index `i` has more than or equal to `n-i-1` zeros, we are good.

**Thanks**
Thanks for reading. If you find this helpful, please upvote so that -
1.  It encourages me to write more and share solutions on Leetcode and on my blog/website, if I have an easy, intuitive approach to solve the problem.
2.  Other learners get to see the post near the top.

**Update: Proof why greedy solution works**
(You can skip to the code if you are convinced that greedy approach works or just want to know how to solve the problem).
Since many of you were interested, I spent some time to put the informal proof into words. Sometimes you intuitively know that something is correct, but can\'t provide the proof. This is what is happening with me here, but still an attempt at it -
##### Assumptions -
Say, we have an array `a` of size `n` which we want to rearrange such that it satisfies the following conditions -
Condition #1. For all `i=0` to `i=n-1`, `a[i]` should be `>= n-i-1`.
Condition #2. If you want to bring a number from index `j` to index `k` where `k` < `j`, you must swap adjecent elements between index `k` and `j`, so that finally `j`th element becomes `k`th. 
Condition #3. The number of swaps required in above process will be, `j-k`. Assume that we store this value in an array, `numSwaps[]`. Sum of elements in `numSwaps[]` should be minimum.
##### Not-so-formal Proof -
In our solution, for each number , we always choose the **nearest index** that satisifies the condition 1. I\'m talking about this code -
```
if (a[i] < (n-i-1)) {
		int j=i;
		while (j < n && a[j] < (n-i-1)) { // we break at the first element that can satisfy our condition 1.
			j++;
		}
```
Then swap the adjacent numbers, satisfying the condition 2.
```
while (j>i) {
                    swap(a[j], a[j-1]); // Swap adjacent. Condition 2.
                    ans++;
                    j--;
                }
```
If we follow this, it is clear that each individual element in `numSwaps[]` will always have the smallest number possible for that index. (nearest --> smallest). 

Before the proof, one important observation to note is -
Observation #1. From condition 1, we can say that any number that we choose to bring from index `j` to index `i`, can also be used for index `i+1`instead. 
For example, say n = 7. So at index `0`(zero), we want to have a number that is `>= 6` to satisify the condition #1. What I am saying is that, we can use that number for index `1` as well and still satisfy the condition #1. Because for index `1`, we will want a number `>= 5` and the number for index `0` will anyway be `>= 6`, so condition #1 will automatically satisfy.

What we are not yet sure is that sum of all individual elements in `numSwaps[]` will yield the global minima.
There are two possible doubts -
Doubt #1. If we choose greedily, there might be some index `i` which can\'t have a number `>= n-i-1` because all those numbers were already used by previous indices (`< i`). Maybe if we had chosen some other arrangement, index `i` could have satisfied the condition #1.
Doubt #2. Although the individual number of swaps in `numSwaps[]` are minimum, there can be a different arrangement where individul number of swaps in `numSwaps[]` are not minimum, but total is less than the greedy approach.

Let\'s see how doubt #1 can not be true  -
Let\'s say, for index `i`, we chose to bring an element from index `j` which was the first element that could satisfy the condition #1. However, this led to a problem where index `p` (which is >`i`) couldn\'t find any appropriate number to satisfy condition #1.
Assume that index `j` was meant to be used for the index `p` in reality. So we would still want to satisfy the condition for index `i`. We will have to find out another index `k` which is different from `j` & which can be used for `i`. Assume we found such index `m`. From the Observation #1, we can confidently say that this will never be necessary. If there was an index `m` which can be used for index `i`, then it can very well be used for index `p` because `p` > `i`. Hence, the scenario where index `p` is not able to find the right index `j` because the number at index `j` was used by index `i`, is never going to happen.
So in reality, doubt #1 is never going to be true. 

Now talking about doubt #2.
In the example above, Finding the nearest number that can be used for index `i` was at index `j` and number of swaps in that case were `j-i`. But we chose the index `k` which is > `j` and that costed `k-i`. Now, this difference needs to be compensated somewhere in the future indices, otherwise `j` was certainly a better choice than `k` because `j-i` < `k-i`. 
In reality, this difference can never be compensated. Going back to the condition #1, if both `j` and `k` can be used for `i`, then they can also be used for say `i+1`. Now if we bring `j` to `i` and `k` to `i+1`, we will need `j-i` + `k-(i+1)` swaps i.e. `j+k-2*i-1`. OTOH, if we bring `j` to `i+1` and `k` to `i`, we will need `j-(i+1)` + `k-i` which is again `j+k-2*i-1`. 
Generalizing the above, if we bring `j` to `i` and `k` to `x` where `x` > `i`, we need `j-i+k-x` swaps. OTOH, if we bring `j` to `x` instead and `k` to `i`, we need `j-x+k-i`swaps. Both of which are equal. 
Hence, the doubt #2 will also never be true.

I understand this is a bit complicated, especially `i, j, k, m, x, p`, but then give it some time and it should be digestable. 
I really hope this makes sense. 
Certainly tell me about this in the comments.

**C++**
```
class Solution {
public:
    int zerosAtEnd(vector<int> &a) {
        int ans = 0;
        int n = a.size();
        int i=n-1;
        while (i >= 0 && a[i] == 0) {
            ans++;
            i--;
        } 
        return ans;
    }
    
    vector<int> gridToVec(vector<vector<int>>& grid) {
        vector<int> ans;
        for (auto &x: grid) {
            ans.push_back(zerosAtEnd(x));
        }
        return ans;
    }

    int minSwaps(vector<int> &a) {
        int n = a.size();
        int ans = 0;
        for (int i=0; i<n; i++) {
            if (a[i] < (n-i-1)) {
                int j=i;
                while (j < n && a[j] < (n-i-1)) {
                    j++;
                }
				
                if (j == n) {     // Did not find any number greater than or equal to n-i-1.
                    return -1;  // so its impossible to get the answer.
                }
                while (j>i) {
                    swap(a[j], a[j-1]);
                    ans++;
                    j--;
                }
            }
        }
        return ans;
    }
    
    int minSwaps(vector<vector<int>>& grid) {
        vector<int> a = gridToVec(grid);
        return minSwaps(a);
    }
};
```

**Java** (Courtesy of [@amv95](http://https://leetcode.com/amv95))
```
public int minSwaps(int[][] grid) {
        int[] a = gridToVec(grid);
        return minSwaps(a);
    }
    
    private int minSwaps(int[] a) {
        int n = a.length;
        int ans = 0;
        for (int i=0; i<n; i++) {
            if (a[i] < (n-i-1)) {
                int j=i;
                while (j < n && a[j] < (n-i-1)) {
                    j++;
                }
				
                if (j == n) {     // Did not find any number greater than or equal to n-i-1.
                    return -1;  // so its impossible to get the answer.
                }
                while (j>i) {
                    //swap(a[j], a[j-1]);
                    int temp = a[j];
                    a[j] = a[j-1];
                    a[j-1] = temp;
                    ans++;
                    j--;
                }
            }
        }
        return ans;
    }
    
    
    private int zerosAtEnd(int[] a) {
        int ans = 0;
        int n = a.length;
        int i=n-1;
        while (i >= 0 && a[i] == 0) {
            ans++;
            i--;
        } 
        return ans;
    }
    
     private int[] gridToVec(int[][] grid) {
        int[] ans = new int[grid.length];
         int i =0;
        for (int[] x: grid) {
            ans[i++] = zerosAtEnd(x);
        }
        return ans;
    }
```
</p>


### Java 16 lines bubble sort with line-by-line explanation easy to understand
- Author: FunBam
- Creation Date: Sun Aug 02 2020 12:00:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 02 2020 12:48:53 GMT+0800 (Singapore Standard Time)

<p>
![image](https://assets.leetcode.com/users/images/62ae8e60-d867-4e7d-8b37-06e5b5a15cc9_1596343724.4730008.png)

```
class Solution {
    public int minSwaps(int[][] grid) {
        int n = grid.length, res=0;
        List<Integer> row = new LinkedList<>();
        for (int i=0; i<n; i++){
            int trailingZeroCnt=0;
            for (int j=n-1; j>-1 && grid[i][j]==0; j--) trailingZeroCnt++;
            row.add(trailingZeroCnt);
        }
        for (int curRowIdx=0, minTrailingZeros=n-1; curRowIdx<n; curRowIdx++, minTrailingZeros--){
            int satisfiedRowIdx =curRowIdx;
            while (satisfiedRowIdx <n && row.get(satisfiedRowIdx)<minTrailingZeros) satisfiedRowIdx++;
            if (satisfiedRowIdx ==n) return -1;
            int toRemove = row.remove(satisfiedRowIdx);
            row.add(curRowIdx, toRemove);
            res+=satisfiedRowIdx -curRowIdx;
        }
        return res;
    }
}
```
Happy Coding!

PS: I\'m glad that someone thinks this solution is from a textbook, but I just wrote this down after solving it in contest :)
</p>


### [C++] easy solution = greedy + prove + example
- Author: codedayday
- Creation Date: Sun Aug 02 2020 12:08:35 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 03 2020 03:20:32 GMT+0800 (Singapore Standard Time)

<p>
Observation:
Let\'s use Example 1 for illustration:
[[0,0,1],[1,1,0],[1,0,0]]
tailing zero counts are:   [0, 1, 2]  (starting status)
The desired final goal is: [2,1, 0]   (ending status)

So, the problems is converted to find the minimal ajacent swaping from starting status to ending status:

Comment: If you think this post is helpful, please help upvote.
```
class Solution {
public:
    int minSwaps(vector<vector<int>>& grid) {
        const int n = grid.size();
        vector<int> t(n); //t[i]: tailing zero count for i-th row        
        for(int i=0;i<n;i++){
            int count = 0;
            for(int j = n;--j>=0 && !grid[i][j]; )  count++;                
            t[i] = count;
        }
        int ans = 0;        
        for(int i=0;i< n;i++){
            int k = i;
            int req = n-1 - i; // desired tailing zero count
            while(k<n && t[k]<req) k++; // greedily find first swaping candidate and log the result into k. Note1
            if(k==n) return -1; // k is out of range. Fail in searching

            //Core part of the question: moving up k-th row up, moving down [i, k-1
            //Part 1: move k-th row up to i-th row
            ans += k-i; // accumulate the operation cost of moving k to i
            
            //Part 2: move the rest involved row downward by offset 1
            while(k>i){ // simulate swaping operation of two adjacent rows in range of [i, k-1 ]
                t[k] = t[k-1];
                k--;
            }
        }    
        
        return ans;
    }
};
/*Note 1: why dose greedy approach work? 
Recall the row scanning is performed one by one from row 0, which ask most tailing 0; 
Suppose, current row (i-th row) asks for a row with at least 4 zeros, and the neaerst downward row (j-th) has 5 tailing zeros. You can greedily but aslo safely choose it as candidate. any rows after than i-th row will ask for less than 4 tailing rows. So you greedy approach will cause no trouble at all.
*/
```
reference:
[1] https://leetcode.com/problems/minimum-swaps-to-arrange-a-binary-grid/discuss/767974/C%2B%2B-Solution
</p>


