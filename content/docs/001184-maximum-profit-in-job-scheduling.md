---
title: "Maximum Profit in Job Scheduling"
weight: 1184
#id: "maximum-profit-in-job-scheduling"
---
## Description
<div class="description">
<p>We have <code>n</code> jobs, where every job&nbsp;is scheduled to be done from <code>startTime[i]</code> to <code>endTime[i]</code>, obtaining a profit&nbsp;of <code>profit[i]</code>.</p>

<p>You&#39;re given the&nbsp;<code>startTime</code>&nbsp;,&nbsp;<code>endTime</code>&nbsp;and <code>profit</code>&nbsp;arrays,&nbsp;you need to output the maximum profit you can take such that there are no 2 jobs in the subset&nbsp;with overlapping time range.</p>

<p>If you choose a job that ends at time <code>X</code>&nbsp;you&nbsp;will be able to start another job that starts at time <code>X</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/10/10/sample1_1584.png" style="width: 380px; height: 154px;" /></strong></p>

<pre>
<strong>Input:</strong> startTime = [1,2,3,3], endTime = [3,4,5,6], profit = [50,10,40,70]
<strong>Output:</strong> 120
<strong>Explanation:</strong> The subset chosen is the first and fourth job. 
Time range [1-3]+[3-6] , we get profit of 120 = 50 + 70.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/10/10/sample22_1584.png" style="width: 600px; height: 112px;" /> </strong></p>

<pre>
<strong>
Input:</strong> startTime = [1,2,3,4,6], endTime = [3,5,10,6,9], profit = [20,20,100,70,60]
<strong>Output:</strong> 150
<strong>Explanation:</strong> The subset chosen is the first, fourth and fifth job. 
Profit obtained 150 = 20 + 70 + 60.
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/10/10/sample3_1584.png" style="width: 400px; height: 112px;" /></strong></p>

<pre>
<strong>Input:</strong> startTime = [1,1,1], endTime = [2,3,4], profit = [5,6,4]
<strong>Output:</strong> 6
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= startTime.length == endTime.length ==&nbsp;profit.length&nbsp;&lt;= 5 * 10^4</code></li>
	<li><code>1 &lt;=&nbsp;startTime[i] &lt;&nbsp;endTime[i] &lt;= 10^9</code></li>
	<li><code>1 &lt;=&nbsp;profit[i] &lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Binary Search (binary-search)
- Dynamic Programming (dynamic-programming)
- Sort (sort)

## Companies
- Cisco - 4 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Arista Networks - 2 (taggedByAdmin: false)
- Airbnb - 4 (taggedByAdmin: false)
- Pony.ai - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] DP Solution
- Author: lee215
- Creation Date: Sun Oct 20 2019 12:05:08 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 02 2020 14:15:48 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Sort the jobs by endTime.

`dp[time] = profit` means that within the first `time` duration,
we cam make at most `profit` money.
Intial `dp[0] = 0`, as we make `profit = 0` at `time =  0`.

For each `job = [s, e, p]`, where `s,e,p` are its start time, end time and profit,
Then the logic is similar to the knapsack problem.
If we don\'t do this job, nothing will be changed.
If we do this job, binary search in the dp to find the largest profit we can make before start time `s`.
So we also know the maximum cuurent profit that we can make doing this job.

Compare with last element in the `dp`,
we make more money,
it worth doing this job,
then we add the pair of `[e, cur]` to the back of `dp`.
Otherwise, we\'d like not to do this job.
<br>

## **Complexity**
Time `O(NlogN)` for sorting
Time `O(NlogN)` for binary search for each job
Space `O(N)`
<br>

**C++, using map**
```cpp
public:
    int jobScheduling(vector<int>& startTime, vector<int>& endTime, vector<int>& profit) {
        int n = startTime.size();
        vector<vector<int>> jobs;
        for (int i = 0; i < n; ++i) {
            jobs.push_back({endTime[i], startTime[i], profit[i]});
        }
        sort(jobs.begin(), jobs.end());
        map<int, int> dp = {{0, 0}};
        for (auto& job : jobs) {
            int cur = prev(dp.upper_bound(job[1]))->second + job[2];
            if (cur > dp.rbegin()->second)
                dp[job[0]] = cur;
        }
        return dp.rbegin()->second;
    }
```

**Java solution using TreeMap**
```java
    public int jobScheduling(int[] startTime, int[] endTime, int[] profit) {
        int n = startTime.length;
        int[][] jobs = new int[n][3];
        for (int i = 0; i < n; i++) {
            jobs[i] = new int[] {startTime[i], endTime[i], profit[i]};
        }
        Arrays.sort(jobs, (a, b)->a[1] - b[1]);
        TreeMap<Integer, Integer> dp = new TreeMap<>();
        dp.put(0, 0);
        for (int[] job : jobs) {
            int cur = dp.floorEntry(job[0]).getValue() + job[2];
            if (cur > dp.lastEntry().getValue())
                dp.put(job[1], cur);
        }
        return dp.lastEntry().getValue();
    }
```

**Python:**
```python
    def jobScheduling(self, startTime, endTime, profit):
        jobs = sorted(zip(startTime, endTime, profit), key=lambda v: v[1])
        dp = [[0, 0]]
        for s, e, p in jobs:
            i = bisect.bisect(dp, [s + 1]) - 1
            if dp[i][1] + p > dp[-1][1]:
                dp.append([e, dp[i][1] + p])
        return dp[-1][1]
```

</p>


### C++ with picture
- Author: votrubac
- Creation Date: Sun Oct 20 2019 14:08:46 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 21 2019 05:03:12 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
For each time point, we can memoise the maximum profit we can get starting from that time. If we process our jobs from the most distant to the closest, we can calculate the maximum profit for the start time using the max profits for the job\'s end time.  

![image](https://assets.leetcode.com/users/votrubac/image_1571553014.png)

#### Algorithm
1. Use a map to store job start times and the maximum profit (zero, initially).
2. Use an unordered map to store start time and job information. 
	- We will use this unordered map to look-up jobs by their start time.
3. Since times in the map are sorted, process each time point right to left:
	- Lookup job information in the unordered map by the start time.
	- Set the maximum profit as the greater of:
		- Running maximum profit.
		- Job profit plus maximum profit at the end of the job.
	- Update the running maximum profit.
4. Return the running maximum profit.

> Instead of using a hash map to store job information, we could pack it to the same map, like `map<int, pair<int, vector<pair<int, int>>>> jobs;`. It would probably be a good idea to define some struct for the value type, or readability would suffer. 

```
int jobScheduling(vector<int>& startTime, vector<int>& endTime, vector<int>& profit) {
    map<int, int> times;
    unordered_map<int, vector<pair<int, int>>> jobs;
    for (auto i = 0; i < startTime.size(); ++i) {
        times[startTime[i]] = 0;
        jobs[startTime[i]].push_back({ endTime[i], profit[i] });
    }    
    int max_profit = 0;
    for (auto it = rbegin(times); it != rend(times); ++it) {
        for (auto job : jobs[it->first]) {
            auto it = times.lower_bound(job.first);
            max_profit = max(max_profit, (it == end(times) ? 0 : it->second) + job.second);
        }
        it->second = max_profit;
    }
    return max_profit;
}  
```
Instead of copying the job information, we can use an index array to look-up data in the original arrays in the requried order.
> An index array contains values `[0...n - 1]```, sorted based on the data from `startTime`. When you iterate `i` and access values as `startTime[idx[i]]`, the values will be in the sorted order. 
```
int jobScheduling(vector<int>& startTime, vector<int>& endTime, vector<int>& profit) {
    vector<int> idx(startTime.size());
    iota(begin(idx), end(idx), 0);
    sort(begin(idx), end(idx), [&](int i, int j) { return startTime[i] > startTime[j]; });
    map<int, int> memo;
    int max_profit = 0;
    for (auto i = 0; i < idx.size(); ++i) {
        auto it = memo.lower_bound(endTime[idx[i]]);
        max_profit = max(max_profit, (it == end(memo) ? 0 : it->second) + profit[idx[i]]);
        memo[startTime[idx[i]]] = max_profit;
    }
    return max_profit;
}
```
#### Complexity Analysis
- Time complexity: O(n log n), where n is the number of jobs. We sort jobs by the start time, and, for each job, we use a binary search to find the maximum profit for its end time.

- Space complexity: O(n). We memoise the maximum profit for each end time. We also copy job information so that we can access it in-order (this can be avoided though with a lengthier code).
</p>


### DP+Binary search (Java)
- Author: Poorvank
- Creation Date: Sun Oct 20 2019 12:01:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 12:01:39 GMT+0800 (Singapore Standard Time)

<p>
-> Create a Job array for ease of calculation.
-> Sort jobs according to finish time.
-> For the job array jobs
   maxprofit(int[] jobs, n){
     a) if (n == 1) return jobs[0];
     b) Return the maximum of following two profits.
         -> Maximum profit by excluding current job :  maxprofit(jobs, n-1)
         -> Maximum profit by including the current job
   }

Find Profit including current Job:
Find the latest job before the current job (in sorted array) that doesn\'t conflict with current job \'jobs[n-1]\'.
Once found, we recur for all jobs till that job and add profit of current job to result.

Complexity : O(n Log n)

```
public class JobScheduling {

    private class Job {
        int start, finish, profit;
        Job(int start, int finish, int profit) {
            this.start = start;
            this.finish = finish;
            this.profit = profit;
        }
    }

    public int jobScheduling(int[] startTime, int[] endTime, int[] profit) {
        int n = startTime.length;
        Job[] jobs = new Job[n];
        for(int i=0;i<n;i++) {
            jobs[i] = new Job(startTime[i],endTime[i],profit[i]);
        }
        return scheduleApt(jobs);
    }

    private int scheduleApt(Job[] jobs) {
        // Sort jobs according to finish time
        Arrays.sort(jobs, Comparator.comparingInt(a -> a.finish));
        // dp[i] stores the profit for jobs till jobs[i]
        // (including jobs[i])
        int n = jobs.length;
        int[] dp = new int[n];
        dp[0] = jobs[0].profit;
        for (int i=1; i<n; i++) {
            // Profit including the current job
            int profit = jobs[i].profit;
            int l = search(jobs, i);
            if (l != -1)
                profit += dp[l];
            // Store maximum of including and excluding
            dp[i] = Math.max(profit, dp[i-1]);
        }

        return dp[n-1];
    }

    private int search(Job[] jobs, int index) {
        int start = 0, end = index - 1;
        while (start <= end) {
            int mid = (start + end) / 2;
            if (jobs[mid].finish <= jobs[index].start) {
                if (jobs[mid + 1].finish <= jobs[index].start)
                    start = mid + 1;
                else
                    return mid;
            }
            else
                end = mid - 1;
        }
        return -1;
    }

}
```
</p>


