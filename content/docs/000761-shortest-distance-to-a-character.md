---
title: "Shortest Distance to a Character"
weight: 761
#id: "shortest-distance-to-a-character"
---
## Description
<div class="description">
<p>Given a string <code>S</code>&nbsp;and a character <code>C</code>, return an array of integers representing the shortest distance from the character <code>C</code> in the string.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> S = &quot;loveleetcode&quot;, C = &#39;e&#39;
<strong>Output:</strong> [3, 2, 1, 0, 1, 0, 0, 1, 2, 2, 1, 0]
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>S</code> string length is&nbsp;in&nbsp;<code>[1, 10000].</code></li>
	<li><code>C</code>&nbsp;is a single character, and guaranteed to be in string <code>S</code>.</li>
	<li>All letters in <code>S</code> and <code>C</code> are lowercase.</li>
</ol>

</div>

## Tags


## Companies
- Apple - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Atlassian - 5 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Min Array [Accepted]

**Intuition**

For each index `S[i]`, let's try to find the distance to the next character `C` going left, and going right.  The answer is the minimum of these two values.

**Algorithm**

When going left to right, we'll remember the index `prev` of the last character `C` we've seen.  Then the answer is `i - prev`.

When going right to left, we'll remember the index `prev` of the last character `C` we've seen.  Then the answer is `prev - i`.

We take the minimum of these two answers to create our final answer.

<iframe src="https://leetcode.com/playground/tMC6dWpk/shared" frameBorder="0" width="100%" height="395" name="tMC6dWpk"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.  We scan through the string twice.

* Space Complexity: $$O(N)$$, the size of `ans`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] 2-Pass with Explanation
- Author: lee215
- Creation Date: Sun Apr 22 2018 11:02:07 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Feb 04 2020 01:21:00 GMT+0800 (Singapore Standard Time)

<p>
# Solution 1: Record the Position

Initial result array.
Loop twice on the string `S`.
First forward pass to find shortest distant to character on left.
Second backward pass to find shortest distant to character on right.
<br>

In python solution,  I merged these two `for` statement.
We can do the same in C++/Java by:
```
for (int i = 0; i >= 0; res[n-1] == n ? ++i : --i)
```
But it will become less readable.
<br>

Time complexity `O(N)`
Space complexity `O(N)` for output
<br>

**C++**
```cpp
    vector<int> shortestToChar(string S, char C) {
        int n = S.size(), pos = -n;
        vector<int> res(n, n);
        for (int i = 0; i < n; ++i) {
            if (S[i] == C) pos = i;
            res[i] = i - pos;
        }
        for (int i = pos - 1; i >= 0; --i) {
            if (S[i] == C)  pos = i;
            res[i] = min(res[i], pos - i);
        }
        return res;
    }
```

**Java**
```java
    public int[] shortestToChar(String S, char C) {
        int n = S.length(), pos = -n, res[] = new int[n];
        for (int i = 0; i < n; ++i) {
            if (S.charAt(i) == C) pos = i;
            res[i] = i - pos;
        }
        for (int i = pos - 1; i >= 0; --i) {
            if (S.charAt(i) == C)  pos = i;
            res[i] = Math.min(res[i], pos - i);
        }
        return res;
    }
```
**Python**
```py
    def shortestToChar(self, S, C):
        n, pos = len(S), -float(\'inf\')
        res = [n] * n
        for i in range(n) + range(n)[::-1]:
            if S[i] == C:
                pos = i
            res[i] = min(res[i], abs(i - pos))
        return res
```
<br><br>

# Solution 2: DP
Another idea is quite similar and has a sense of DP.
<br>
**C++:**
```cpp
    vector<int> shortestToChar2(string S, char C) {
        int n = S.size();
        vector<int> res(n);
        for (int i = 0; i < n; ++i)
            res[i] = S[i] == C ? 0 : n;
        for (int i = 1; i < n; ++i)
            res[i] = min(res[i], res[i - 1] + 1);
        for (int i = n - 2; i >= 0; --i)
            res[i] = min(res[i], res[i + 1] + 1);
        return res;
    }
```

**Java:**
```java
    public int[] shortestToChar(String S, char C) {
        int n = S.length();
        int[] res = new int[n];
        for (int i = 0; i < n; ++i)
            res[i] = S.charAt(i) == C ? 0 : n;
        for (int i = 1; i < n; ++i)
            res[i] = Math.min(res[i], res[i - 1] + 1);
        for (int i = n - 2; i >= 0; --i)
            res[i] = Math.min(res[i], res[i + 1] + 1);
        return res;
    }
```
**Python:**
```py
    def shortestToChar(self, S, C):
        n = len(S)
        res = [0 if c == C else n for c in S]
        for i in range(1, n):
            res[i] = min(res[i], res[i - 1] + 1)
        for i in range(n - 2, -1, -1):
            res[i] = min(res[i], res[i + 1] + 1)
        return res
```
</p>


### Explanation of description without solution
- Author: 1creation
- Creation Date: Tue Apr 24 2018 15:09:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 12:54:48 GMT+0800 (Singapore Standard Time)

<p>
You need to find shortest distance for *EACH* Character in the String \'S\' to Character \'C\' 
</p>


### Concise java solution with detailed explanation. Easy understand!!!
- Author: Self_Learner
- Creation Date: Tue Apr 24 2018 14:07:54 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 05 2018 07:45:30 GMT+0800 (Singapore Standard Time)

<p>
```
/** "loveleetcode" "e"
 *  1. put 0 at all position equals to e, and max at all other position
 *     we will get [max, max, max, 0, max, 0, 0, max, max, max, max, 0]
 *  2. scan from left to right, if =max, skip, else dist[i+1] = Math.min(dp[i] + 1, dp[i+1]), 
 *     we can get [max, max, max, 0, 1, 0, 0, 1, 2, 3, 4, 0]
 *  3. scan from right to left, use dp[i-1] = Math.min(dp[i] + 1, dp[i-1])
 *     we will get[3, 2, 1, 0, 1, 0, 0, 1, 2, 2, 1, 0] 
 */
class Solution {
    public int[] shortestToChar(String s, char c) {
        int n = s.length();
        int[] dist = new int[n];
        for (int i = 0; i < n; i++) {
            if (s.charAt(i) == c) continue;
            dist[i] = Integer.MAX_VALUE;
        }
        for (int i = 0; i < n-1; i++) {
            if (dist[i] == Integer.MAX_VALUE) continue;
            else dist[i + 1] = Math.min(dist[i+1], dist[i] + 1);
        }
        for (int i = n-1; i > 0; i--) {
            dist[i-1] = Math.min(dist[i-1], dist[i] + 1);
        }
        return dist; 
    }
}

</p>


