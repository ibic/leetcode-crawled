---
title: "Largest Perimeter Triangle"
weight: 927
#id: "largest-perimeter-triangle"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of positive lengths, return the largest perimeter of a triangle with <strong>non-zero area</strong>, formed from 3 of these lengths.</p>

<p>If it is impossible to form any&nbsp;triangle of non-zero area, return <code>0</code>.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[2,1,2]</span>
<strong>Output: </strong><span id="example-output-1">5</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[1,2,1]</span>
<strong>Output: </strong><span id="example-output-2">0</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[3,2,3,4]</span>
<strong>Output: </strong><span id="example-output-3">10</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">[3,6,2,3]</span>
<strong>Output: </strong><span id="example-output-4">8</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>3 &lt;= A.length &lt;= 10000</code></li>
	<li><code>1 &lt;= A[i] &lt;= 10^6</code></li>
</ol>
</div>
</div>
</div>
</div>
</div>

## Tags
- Math (math)
- Sort (sort)

## Companies
- C3 IoT - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Sort

**Intuition**

Without loss of generality, say the sidelengths of the triangle are $$a \leq b \leq c$$.  The necessary and sufficient condition for these lengths to form a triangle of non-zero area is $$a + b > c$$.

Say we knew $$c$$ already.  There is no reason not to choose the largest possible $$a$$ and $$b$$ from the array.  If $$a + b > c$$, then it forms a triangle, otherwise it doesn't.

**Algorithm**

This leads to a simple algorithm:  Sort the array.  For any $$c$$ in the array, we choose the largest possible $$a \leq b \leq c$$:  these are just the two values adjacent to $$c$$.  If this forms a triangle, we return the answer.

<iframe src="https://leetcode.com/playground/GgqxkKqV/shared" frameBorder="0" width="100%" height="208" name="GgqxkKqV"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Sort and Try Biggest
- Author: lee215
- Creation Date: Sun Jan 13 2019 12:06:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 13 2019 12:06:26 GMT+0800 (Singapore Standard Time)

<p>
For `a >= b >= c`, `a,b,c` can form a triangle if `a < b + c`.

1. We sort the `A`
2. Try to get a triangle with 3 biggest numbers.
3. If `A[n-1] < A[n-2] + A[n-3]`, we get a triangle.
   If `A[n-1] >= A[n-2] + A[n-3] >= A[i] + A[j]`, we cannot get any triangle with `A[n-1]`
4. repeat step2 and step3 with the left numbers.


**Java:**
```
    public int largestPerimeter(int[] A) {
        Arrays.sort(A);
        for (int i = A.length - 1; i > 1; --i)
            if (A[i] < A[i - 1] + A[i - 2])
                return A[i] + A[i - 1] + A[i - 2];
        return 0;
    }
```

**C++:**
```
    int largestPerimeter(vector<int>& A) {
        sort(A.begin(), A.end());
        for (int i = A.size() - 1 ; i > 1; --i)
            if (A[i] < A[i - 1] + A[i - 2])
                return A[i] + A[i - 1] + A[i - 2];
        return 0;
    }
```

**Python:**
```
    def largestPerimeter(self, A):
        A = sorted(A)[::-1]
        for i in range(len(A) - 2):
            if A[i] < A[i + 1] + A[i + 2]:
                return A[i] + A[i + 1] + A[i + 2]
        return 0
```

</p>


### Python 2-liner [Comprehend my list comprehension]
- Author: cenkay
- Creation Date: Sun Jan 13 2019 22:38:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 13 2019 22:38:45 GMT+0800 (Singapore Standard Time)

<p>
**Hi fellas,
My laptop is broken and i can\'t participate Leetcode. I am writing this on my ipad and it\'s been more than 20 mins for two line code to write correctly, such a pain in the ass LC mobile not responsive. Happy coding!**
```
class Solution:
 def largestPerimeter(self, A):
  A.sort()
  return ([0] + [a + b + c for a, b, c in zip(A, A[1:], A[2:]) if c < a + b])[-1]
```
</p>


### Python Solution with Explanation
- Author: chakshu
- Creation Date: Sat Feb 02 2019 05:51:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Feb 02 2019 05:51:13 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def largestPerimeter(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        A.sort(reverse=True)
        for i, x in enumerate(A[:-2]):
            # Sum of any two sides should be greater than the third side.
            # Consider sides: A[i], A[i+1] and A[i+2] 
            # A[i] >= A[i+1] >= A[i+2] since the list is sorted
            # A[i] >= A[i+1] so A[i] + A[i+2] > A[i+1] and also A[i] >= A[i+2] so A[i] + A[i+1] > A[i+2] hence
            # the only condition we need to check is that A[i] < A[i+1] + A[i+2] 
            if x < A[i+1] + A[i+2]:
                return x + A[i+1] + A[i+2]

            # Another thing to note here is that: we have reached this point that means, A[i] > A[i+1] + A[i+2]
            # So there is no way that A[i] < A[i+1] and A[j] where j > i+2 so we need not consider all permutations
            # of A[i], A[i+1], A[i+3] or A[i], A[i+1], A[i+4] and so on.

            # Hence the next case to be considered would be A[i+1], A[i+2] and A[i+3]
            # hence just incrementing i would do the job
            
        return 0
```
</p>


