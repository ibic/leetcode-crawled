---
title: "Word Break"
weight: 139
#id: "word-break"
---
## Description
<div class="description">
<p>Given a <strong>non-empty</strong> string <em>s</em> and a dictionary <em>wordDict</em> containing a list of <strong>non-empty</strong> words, determine if <em>s</em> can be segmented into a space-separated sequence of one or more dictionary words.</p>

<p><strong>Note:</strong></p>

<ul>
	<li>The same word in the dictionary may be reused multiple times in the segmentation.</li>
	<li>You may assume the dictionary does not contain duplicate words.</li>
</ul>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;leetcode&quot;, wordDict = [&quot;leet&quot;, &quot;code&quot;]
<strong>Output:</strong> true
<strong>Explanation:</strong> Return true because <code>&quot;leetcode&quot;</code> can be segmented as <code>&quot;leet code&quot;</code>.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;applepenapple&quot;, wordDict = [&quot;apple&quot;, &quot;pen&quot;]
<strong>Output:</strong> true
<strong>Explanation:</strong> Return true because <code>&quot;</code>applepenapple<code>&quot;</code> can be segmented as <code>&quot;</code>apple pen apple<code>&quot;</code>.
&nbsp;            Note that you are allowed to reuse a dictionary word.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;catsandog&quot;, wordDict = [&quot;cats&quot;, &quot;dog&quot;, &quot;sand&quot;, &quot;and&quot;, &quot;cat&quot;]
<strong>Output:</strong> false
</pre>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 31 (taggedByAdmin: true)
- Facebook - 28 (taggedByAdmin: true)
- Apple - 14 (taggedByAdmin: false)
- Bloomberg - 6 (taggedByAdmin: true)
- Google - 4 (taggedByAdmin: true)
- Microsoft - 4 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Qualtrics - 3 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- GoDaddy - 2 (taggedByAdmin: false)
- Oracle - 6 (taggedByAdmin: false)
- Snapchat - 3 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: true)
- Hulu - 2 (taggedByAdmin: false)
- Zillow - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: true)
- VMware - 2 (taggedByAdmin: false)
- Audible - 2 (taggedByAdmin: false)
- Reddit - 2 (taggedByAdmin: false)
- Twilio - 4 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- Square - 2 (taggedByAdmin: true)
- Walmart Labs - 2 (taggedByAdmin: false)
- HBO - 2 (taggedByAdmin: false)
- TripAdvisor - 2 (taggedByAdmin: false)
- Pocket Gems - 0 (taggedByAdmin: true)
- Coupang - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force

**Algorithm**

The naive approach to solve this problem is to use recursion and backtracking.
For finding the solution, we check every possible prefix of that string in the dictionary of words, if it is found in the dictionary, then the recursive function is called for the remaining portion of that string. And, if in some function call it is found that the complete string is in dictionary, then it will return true.

<iframe src="https://leetcode.com/playground/bBjJUvXR/shared" frameBorder="0" width="100%" height="327" name="bBjJUvXR"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^n)$$. Consider the worst case where $$s$$ = "$$\text{aaaaaaa}$$" and every prefix of $$s$$ is present in the dictionary of words, then the recursion tree can grow upto $$n^n$$.

* Space complexity : $$O(n)$$. The depth of the recursion tree can go upto $$n$$.
<br />
<br />
---

#### Approach 2: Recursion with memoization

**Algorithm**

In the previous approach we can see that many subproblems were redundant, i.e we were calling the recursive function multiple times for a particular string. To avoid this we can use memoization method, where an array $$memo$$ is used to store the result of the subproblems. Now, when the function is called again for a particular string, value will be fetched and returned using the $$memo$$ array, if its value has been already evaluated.

With memoization many redundant subproblems are avoided and recursion tree is pruned and thus it reduces the time complexity by a large factor.

<iframe src="https://leetcode.com/playground/CVpEbHHA/shared" frameBorder="0" width="100%" height="378" name="CVpEbHHA"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Size of recursion tree can go up to $$n^2$$.

* Space complexity : $$O(n)$$. The depth of recursion tree can go up to $$n$$.
<br />
<br />
---
#### Approach 3: Using Breadth-First-Search

**Algorithm**

Another approach is to use Breadth-First-Search. Visualize the string as a tree where each node represents the prefix upto index $$end$$.
Two nodes are connected only if the substring between the indices linked with those nodes is also a valid string which is present in the dictionary.
In order to form such a tree, we start with the first character of the given string (say $$s$$) which acts as the root of the tree being formed and find every possible substring starting with that character which is a part of the dictionary. Further, the ending index (say $$i$$) of every such substring is pushed at the back of a queue which will be used for Breadth First Search. Now, we pop an element out from the front of the queue and perform the same process considering the string $$s(i+1,end)$$ to be the original string and the popped node as the root of the tree this time. This process is continued, for all the nodes appended in the queue during the course of the process. If we are able to obtain the last element of the given string as a node (leaf) of the tree, this implies that the given string can be partitioned into substrings which are all a part of the given dictionary.

The formation of the tree can be better understood with this example:
<!--![Word Break](../Figures/139_wordbreak.gif)-->
!?!../Documents/139_Word_Break.json:1000,563!?!

<iframe src="https://leetcode.com/playground/CrKuA3YW/shared" frameBorder="0" width="100%" height="446" name="CrKuA3YW"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. For every starting index, the search can continue till the end of the given string.

* Space complexity : $$O(n)$$. Queue of atmost $$n$$ size is needed.
<br />
<br />
---
#### Approach 4: Using Dynamic Programming

**Algorithm**

The intuition behind this approach is that the given problem ($$s$$) can be divided into subproblems $$s1$$ and $$s2$$. If these subproblems individually satisfy the required conditions, the complete problem, $$s$$ also satisfies the same. e.g. "$$\text{catsanddog}$$" can be split into two substrings "$$\text{catsand}$$", "$$\text{dog}$$". The subproblem "$$\text{catsand}$$" can be further divided into "$$\text{cats}$$","$$\text{and}$$", which individually are a part of the dictionary making "$$\text{catsand}$$" satisfy the condition. Going further backwards, "$$\text{catsand}$$", "$$\text{dog}$$" also satisfy the required criteria individually leading to the complete string "$$\text{catsanddog}$$" also to satisfy the criteria.

Now, we'll move onto the process of $$\text{dp}$$ array formation. We make use of $$\text{dp}$$ array of size $$n+1$$, where $$n$$ is the length of the given string. We also use two index pointers $$i$$ and $$j$$, where $$i$$ refers to the length of the substring ($$s'$$) considered currently starting from the beginning, and $$j$$ refers to the index partitioning the current substring ($$s'$$) into smaller substrings $$s'(0,j)$$ and $$s'(j+1,i)$$. To fill in the $$\text{dp}$$ array, we initialize the element $$\text{dp}[0]$$ as $$\text{true}$$, since the null string is always present in the dictionary, and the rest of the elements of $$\text{dp}$$ as $$\text{false}$$. We consider substrings of all possible lengths starting from the beginning by making use of index $$i$$. For every such substring, we partition the string into two further substrings $$s1'$$ and $$s2'$$ in all possible ways using the index $$j$$ (Note that the $$i$$ now refers to the ending index of $$s2'$$). Now, to fill in the entry $$\text{dp}[i]$$, we check if the $$\text{dp}[j]$$ contains $$\text{true}$$, i.e. if the substring $$s1'$$ fulfills the required criteria. If so, we further check if $$s2'$$ is present in the dictionary. If both the strings fulfill the criteria, we make $$\text{dp}[i]$$ as $$\text{true}$$, otherwise as $$\text{false}$$.

<iframe src="https://leetcode.com/playground/YnLdSbSZ/shared" frameBorder="0" width="100%" height="327" name="YnLdSbSZ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n^3)$$. There are two nested loops, 
and substring computation at each iteration. Overall that results in $$\mathcal{O}(n^3)$$
time complexity.

* Space complexity : $$\mathcal{O}(n)$$. Length of $$p$$ array is $$n+1$$.

## Accepted Submission (python3)
```python3
class Solution:
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: bool
        """
        lens = len(s)
        memo = [None for _ in range(lens)]
        def cached(fn):
            def wrapper(s, start):
                if memo[start] != None:
                    return memo[start]
                else:
                    r = fn(s, start)
                    memo[start] = r
                    return r
            return wrapper

        @cached
        def wordBreakRecur(s, begin):
            start = begin
            end = start + minlen
            minend = min(lens, start + maxlen)
            while end <= minend:
                word = s[start:end]
                if word in wordSet:
                    if end == lens:
                        if start == begin:
                            return True
                    else:
                        if wordBreakRecur(s, end):
                            return True
                end += 1
            return False

        wordSet = set(wordDict)
        minlen = float('inf')
        maxlen = float('-inf')
        for w in wordDict:
            lw = len(w)
            if lw < minlen:
                minlen = lw
            if lw > maxlen:
                maxlen = lw
        return wordBreakRecur(s, 0)


if __name__ == "XXX__main__":
    sol = Solution()
    #sentences = sol.wordBreak("aaaaaaa", ["aaaa","aa","a"])
    #sentences = sol.wordBreak("aaaaa", ["aa","a"])
    #sentences = sol.wordBreak("pineapplepenapple", ["apple","pen","applepen","pine","pineapple"])
    sentences = sol.wordBreak("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", ["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"])
    print(sentences)
```

## Top Discussions
### Java implementation using DP in two ways
- Author: segfault
- Creation Date: Thu Dec 18 2014 20:29:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 00:33:55 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public boolean wordBreak(String s, Set<String> dict) {
            
            boolean[] f = new boolean[s.length() + 1];
            
            f[0] = true;
            
            
            /* First DP
            for(int i = 1; i <= s.length(); i++){
                for(String str: dict){
                    if(str.length() <= i){
                        if(f[i - str.length()]){
                            if(s.substring(i-str.length(), i).equals(str)){
                                f[i] = true;
                                break;
                            }
                        }
                    }
                }
            }*/
            
            //Second DP
            for(int i=1; i <= s.length(); i++){
                for(int j=0; j < i; j++){
                    if(f[j] && dict.contains(s.substring(j, i))){
                        f[i] = true;
                        break;
                    }
                }
            }
            
            return f[s.length()];
        }
    }
</p>


### The Time Complexity of The Brute Force Method Should Be O(2^n) and Prove It Below
- Author: R0ckyY2
- Creation Date: Wed Sep 12 2018 13:41:36 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 05:13:12 GMT+0800 (Singapore Standard Time)

<p>
First I paste my code here which is equivalent to the code in the "Solution" tab:
```
class Solution {
    public boolean wordBreak(String s, List<String> wordDict) {
        // put all words into a hashset
        Set<String> set = new HashSet<>(wordDict);
        return wb(s, set);
    }
    private boolean wb(String s, Set<String> set) {
        int len = s.length();
        if (len == 0) {
            return true;
        }
        for (int i = 1; i <= len; ++i) {
            if (set.contains(s.substring(0, i)) && wb(s.substring(i), set)) {
                return true;
            }
        }
        return false;
    }
}
```
The time complexity depends on how many nodes the recursion tree has. In the worst case, the recursion tree has the most nodes, which means the program should not return in the middle and it should try as many possibilities as possible. So the branches and depth of the tree are as many as possible. For the worst case, for example, we take `s = "abcd"` and `wordDict = ["a", "b", "c", "bc", "ab", "abc"]`, the recursion tree is shown below:
<img src="https://s3-lc-upload.s3.amazonaws.com/users/r0cky2h/image_1536728871.png" width="500">
From the code `if (set.contains(s.substring(0, i)) && wb(s.substring(i), set)) { }`, we can see that only if the wordDict contains the prefix, the recursion function can go down to the next level. So on the figure above, string on the edge means the wordDict contains that string. All the gray node with empty string cannot be reached because if the program reaches one such node, the program will return, which lead to some nodes right to it will not be reached. So the conclusion is for a string with length 4, the recursion tree has 8 nodes (all black nodes), and 8 is 2^(4-1). So to generalize this, for a string with length n, the recursion tree wil have 2^(n-1) nodes, i.e., the time complexity is O(2^n). I will prove this generalization below using mathmatical induction:
<img src="https://s3-lc-upload.s3.amazonaws.com/users/r0cky2h/image_1536729678.png" width="500">
Explanation: the value of a node is the string length. We calculate the number of nodes in the recursion tree for string length=1, 2, ...., n respectively. 

For example, when string length=4, the second layer of the recursion tree has three nodes where the string length is 3, 2 and 1 respectively. And the number of subtree rooted at these three nodes have been calculated when we do the mathmatical induction.

So time complexity is O(2^n). 
</p>


### C++ Dynamic Programming simple and fast solution (4ms) with optimization
- Author: paul7
- Creation Date: Wed Jan 14 2015 14:10:49 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 09:10:27 GMT+0800 (Singapore Standard Time)

<p>
We use a boolean vector dp[]. dp[***i***] is set to true if a valid word (word sequence) ends there. The optimization is to look from current position ***i*** back and only substring and do dictionary look up in case the preceding position ***j*** with *dp[**j**] == true* is found.

    bool wordBreak(string s, unordered_set<string> &dict) {
            if(dict.size()==0) return false;
            
            vector<bool> dp(s.size()+1,false);
            dp[0]=true;
            
            for(int i=1;i<=s.size();i++)
            {
                for(int j=i-1;j>=0;j--)
                {
                    if(dp[j])
                    {
                        string word = s.substr(j,i-j);
                        if(dict.find(word)!= dict.end())
                        {
                            dp[i]=true;
                            break; //next i
                        }
                    }
                }
            }
            
            return dp[s.size()];
        }
</p>


