---
title: "Palindrome Partitioning III"
weight: 1212
#id: "palindrome-partitioning-iii"
---
## Description
<div class="description">
<p>You are given a string&nbsp;<code>s</code> containing lowercase letters and an integer <code>k</code>. You need to :</p>

<ul>
	<li>First, change some characters of <code>s</code>&nbsp;to other lowercase English letters.</li>
	<li>Then divide <code>s</code>&nbsp;into <code>k</code> non-empty disjoint substrings such that each substring is palindrome.</li>
</ul>

<p>Return the minimal number of characters that you need to change&nbsp;to divide the string.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abc&quot;, k = 2
<strong>Output:</strong> 1
<strong>Explanation:</strong>&nbsp;You can split the string into &quot;ab&quot; and &quot;c&quot;, and change 1 character in &quot;ab&quot; to make it palindrome.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aabbc&quot;, k = 3
<strong>Output:</strong> 0
<strong>Explanation:</strong>&nbsp;You can split the string into &quot;aa&quot;, &quot;bb&quot; and &quot;c&quot;, all of them are palindrome.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;leetcode&quot;, k = 8
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= k &lt;= s.length &lt;= 100</code>.</li>
	<li><code>s</code>&nbsp;only contains lowercase English letters.</li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Uber - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python3 Top-down DFS with Memoization
- Author: Sakata_Gintoki
- Creation Date: Sun Dec 01 2019 12:50:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 01 2019 12:50:19 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def palindromePartition(self, s: str, k: int) -> int:
        n = len(s)
        memo = {}
        
        def cost(s,i,j): #calculate the cost of transferring one substring into palindrome string
            r = 0
            while i < j:
                if s[i] != s[j]:
                    r += 1
                i += 1
                j -= 1
            return r
        
        def dfs(i, k):
            if (i, k) in memo: return memo[(i, k)] #case already in memo
            if n - i == k: #base case that each substring just have one character
                return 0
            if k == 1:    #base case that need to transfer whole substring into palidrome
                return cost(s, i, n - 1)
            res = float(\'inf\')
            for j in range(i + 1, n - k + 2): # keep making next part of substring into palidrome
                res = min(res, dfs(j, k - 1) + cost(s,i, j - 1)) #compare different divisions to get the minimum cost
            memo[(i, k)] = res
            return res
        return dfs(0 , k)
# I really appreciate it if u vote up! \uFF08\uFFE3\uFE36\uFFE3\uFF09\u2197
```
</p>


### Step by Step solution DP (Java)
- Author: qwerjkl112
- Creation Date: Tue Feb 04 2020 11:59:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Feb 04 2020 12:03:25 GMT+0800 (Singapore Standard Time)

<p>
There are a lot of these types of questions in DP Hard. If you learn to recognize the basic structure, you can come up with an answer fairly quickly.
Couple words that give away the DP nature of the problem
1. Palindrome 
1. Minimum 

If we break the question apart we get:
1. How many changes do I need to perform to make S.substring(i, j) to a palindrome.
1. How many ways can I split the string from i to j into groups and what are their costs.
1. What is the minimum combination of these groups.

First thing you should make is a map that contains all the cost from i to j
This is faily simple given the nature of substituion.
```
		...
        int[][] toPal = new int[s.length()][s.length()];
        for (int i = 0; i < s.length(); i++) {
            toPal[i][i] = 0;
        }
        for (int i = s.length()-1; i >= 0; i--) {
            for (int j = i + 1; j < s.length(); j++) {
                toPal[i][j] = getChanges(s, i, j);
            }
        }
		...
	}
    private int getChanges(String s, int start, int end) {
        int changes = 0;
        while(start < end) {
            if (s.charAt(start++) != s.charAt(end--)) {
                changes++;
            }
        }
        return changes;
    }
```
*So now if i want to know how many substitution I need to make s.substring(i,j) into an palindrome then I can just do toPal[i][j]*

Second I want to define my dp problem through some sort of an equation. 
Given: aabbcc, k = 3
The possible groups can be

(aabb)(c)(c), (aab)(bc)(c), (aa)(bbc)(c),  (a)(abbc)(c) -**(third group is (c))**
(aab)(b)(cc), (aa)(bb)(cc), (aa)(bb)(cc) --------------**(third group is (cc))**
(aa)(b)(bcc), (a)(ab)(bcc) ---------------------------**(third group is (bcc))**
(a)(a)(bbcc) ----------------------------------------**(third group is (bbcc))**


Third, notice that there is 4 different ways have third group be just (c). 3 ways for (cc). 2 ways for (bcc). and 1 way for (bbcc). 
```
dp[k][end]
min((aabb)(c), (aab)(bc), (aa)(bbc), (a)(abbc)) = dp[2][4]
or dp[2][4] = Math.min(toPal[0][3] + toPal[4][4], toPal[0][2] + toPal[3][4], toPal[0][1] + toPal[2][4], toPal[0][0] + toPal[1][4])
min((aab)(b), (aa)(bb), (aa)(bb)) = dp[2][3]
or dp[2][3] = Math.min(toPal[0][2] + toPal[3][3], toPal[0][1] + toPal[2][3], toPal[0][0] + toPal[1][3])
min((aa)(b), (a)(ab)) = dp[2][2]
or dp[2][2] = Math.min(toPal[0][1] + toPal[2][2], toPal[0][0] + toPal[1][2])
min((a)(a)) = dp[2][1]
or dp[2][1] = toPal[0][0] + toPal[1][1]
```
So:

```
dp[3][5] // this our answer because we want 3rd group to end at index 5
dp[3][5] = Math.min(dp[2][4] + toPal[5][5], dp[2][3] + toPal[4][5], dp[2][2] + toPal[3][5], dp[2][1] + toPal[2][5])
```
Ans:
```
class Solution {
    public int palindromePartition(String s, int k) {
        int[][] toPal = new int[s.length()][s.length()];
        int[][] dp = new int[k+1][s.length()];
        for (int i = 0; i < s.length(); i++) {
            toPal[i][i] = 0;
        }
        for (int i = s.length()-1; i >= 0; i--) {
            for (int j = i + 1; j < s.length(); j++) {
                toPal[i][j] = getChanges(s, i, j);
            }
        }
        for (int i = 0; i < s.length(); i++) {
            dp[1][i] = toPal[0][i];
        }
        for (int i = 2; i <= k; i++) {
            for (int end = i-1; end < s.length(); end++) {
                int min = s.length();
                for (int start = end-1; start >= 0; start--) {
                    min = Math.min(min, dp[i-1][start] + toPal[start+1][end]);
                }
                dp[i][end] = min;
            }
        }
        return dp[k][s.length()-1];
    }
    
    
    private int getChanges(String s, int start, int end) {
        int changes = 0;
        while(start < end) {
            if (s.charAt(start++) != s.charAt(end--)) {
                changes++;
            }
        }
        return changes;
    }
}
```



</p>


### Simple C++ Dp O(N^2K) Beats 100% with Explanation
- Author: mehuljain53
- Creation Date: Mon Dec 02 2019 08:45:38 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 02 2019 23:41:45 GMT+0800 (Singapore Standard Time)

<p>
We create 2 dp arrays

pd[i][j] = stores the minimum changes required to convert a string starting from i to j into palindrome (i.e for k=1)
dp[j][k] = min changes to divide the string into k palindromic substrings starting from 0th index to jth index.

Constructing pd:
1. pd[i][i] : always 0, since a single character is always palindrome.
2. pd[i][i+1] : 0 or 1 depending on whether both characters are same or not.
3. pd[i][j] :
	a. if s[i] == s[j] : pd[i][j] = pd[i+1][j-1]
	b. else pd[i][j] = pd[i+1][j-1]  + 1, because we can change character at either ith position or jth position.
	
Constructing dp:
1. dp[i][1] = pd[0][i] ; because pd is constructed for k = 1.
2. Now the **main recurrence intution**:
	dp[i][k] = min(dp[l][k-1] + pd[l+1][i]); where l varies from 0 to i.
	
```
class Solution {
public:
    int palindromePartition(string s, int K) {
        vector<vector<int>> dp(101,vector<int>(101,100));
        vector<vector<int>> pd(101,vector<int>(101,101));
		
        for(int i = 0; i < s.size(); i++) {
            pd[i][i] = 0;
        }
        for(int i = 0; i < s.size()-1;i++) {
            if(s[i] == s[i+1])
                pd[i][i+1] = 0;
            else
                pd[i][i+1] = 1;
        }
        
        for(int k = 2; k < s.size(); k++) {
            for(int i = 0; i < s.size(); i++) {
                if(i+k>=s.size())
                    continue;
                int j = i + k;
                pd[i][j] = pd[i+1][j-1];
                if(s[i]!=s[j])
                    pd[i][j] = pd[i+1][j-1] + 1;
            }
        }
        
        for(int j = 0; j < s.size(); j++) {
            dp[j][1] = pd[0][j];
        }
        
        for(int k = 2;  k <= K; k++ ) {
            for(int j = 1; j < s.size(); j++) {
                dp[j][k] = j+1;
                for(int l = 0; l <= j; l++) {
                    dp[j][k] = min(dp[l][k-1] + pd[l+1][j], dp[j][k]);
                }
            }
        }
        return dp[s.size()-1][K];
        
    }
};
```
</p>


