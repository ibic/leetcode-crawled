---
title: "Smallest String Starting From Leaf"
weight: 939
#id: "smallest-string-starting-from-leaf"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a binary tree, each node has a value from <code>0</code> to <code>25</code> representing the letters <code>&#39;a&#39;</code> to <code>&#39;z&#39;</code>: a value of <code>0</code> represents <code>&#39;a&#39;</code>, a value of <code>1</code> represents <code>&#39;b&#39;</code>, and so on.</p>

<p>Find the lexicographically smallest string that starts at a leaf of this tree and ends at the root.</p>

<p><em>(As a reminder, any shorter prefix of a string is lexicographically smaller: for example, <code>&quot;ab&quot;</code> is lexicographically smaller than <code>&quot;aba&quot;</code>.&nbsp; A leaf of a node is a node that has no children.)</em></p>

<div>
<div>
<p>&nbsp;</p>

<ol>
</ol>
</div>
</div>

<div>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/01/30/tree1.png" style="width: 160px; height: 107px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[0,1,2,3,4,3,4]</span>
<strong>Output: </strong><span id="example-output-1">&quot;dba&quot;</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/01/30/tree2.png" style="width: 160px; height: 107px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[25,1,3,1,3,0,2]</span>
<strong>Output: </strong><span id="example-output-2">&quot;adz&quot;</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/02/01/tree3.png" style="height: 170px; width: 172px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[2,2,1,null,1,0,null,0]</span>
<strong>Output: </strong><span id="example-output-3">&quot;abc&quot;</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>The number of nodes in the given tree will be between <code>1</code> and <code>8500</code>.</li>
	<li>Each node in the tree will have a value between <code>0</code> and <code>25</code>.</li>
</ol>
</div>
</div>
</div>
</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Brute Force

**Intuition**

Let's create every possible string - then we can compare them and choose the best one.

**Algorithm**

In our depth first search, we will maintain `sb` (or `A` in Python), the contents of a path from the root to this node.

When we reach a leaf, we will reverse this path to create a candidate answer.  If it is better than our current answer, we'll update our answer.

<iframe src="https://leetcode.com/playground/Qfz5ZcLd/shared" frameBorder="0" width="100%" height="480" name="Qfz5ZcLd"></iframe>

**Complexity Analysis**

* Time Complexity:  We use $$O(N)$$ to traverse the array and maintain `A` [Python] or `sb`.  Then, our reversal and comparison with the previous answer is $$O(L)$$, where $$L$$ is the size of the string we have when at the leaf.  For example, for a perfectly balanced tree, $$L = \log N$$ and the time complexity would be $$O(N \log N)$$.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Divide-and-conquer technique doesn't work for this problem
- Author: jamesPB
- Creation Date: Tue Feb 26 2019 05:50:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 31 2020 03:44:28 GMT+0800 (Singapore Standard Time)

<p>
Many people have solved this problem with the basic idea of divide and conquer, where the key strategy is something like this:
```
answer(root) = min(answer(left) + root.val, answer(right) + root.val)
```
which is incorrect. Surprisingly, these solutions got accepted.
Let me explain this by this case:
[25, 1, null, 0, 0, 1, null, null, null, 0].
![image](https://assets.leetcode.com/users/jamespb/image_1551131779.png)

The expected answer is "ababz", while by divide-and-conqure we would get "abz".
The problem here is 
```
string X < string Y
```
doesn\'t guarantee 
```
X + a < Y + a
```
where a is a character. e.g.:
```
"ab" < "abab", but "abz" > "ababz"
```
So maybe the only correct way is a full DFS with backtracking.
</p>


### C++ 3 lines
- Author: votrubac
- Creation Date: Sun Feb 03 2019 12:02:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 03 2019 12:02:34 GMT+0800 (Singapore Standard Time)

<p>
Traverse the tree, building up the string in the reverse order. Return the value when we find a leaf; track the smalest string of left and right subtrees.

The trick here is to ignore non-leaf nodes with one child. To do that, we can return "|", which is larger than \'z\'. 

Also thanks [@Ninja_fz](https://leetcode.com/Ninja_fz) for simplifying my original solution!
```
string smallestFromLeaf(TreeNode* r) {
  if (r == nullptr) return "|";
  auto s = string(1, \'a\' + r->val);
  return r->left == r->right ? s : min(smallestFromLeaf(r->left) + s, smallestFromLeaf(r->right) + s);
}
```
As [@jamesPB](https://leetcode.com/jamespb/) noted below, the bottom-up approach is accepted but it does not work for case like this:
```
[25, 1, null, 0, 0, 1, null, null, null, 0]
```
So, it looks like we do need to use a top-down approach instead:
```
string smallestFromLeaf(TreeNode* r, string s = "") {
  if (r == nullptr) return "|";
  s = string(1, \'a\' + r->val) + s;
  return r->left == r->right ? s : min(smallestFromLeaf(r->left, s), smallestFromLeaf(r->right, s));
}
```
</p>


### java dfs  O(N)
- Author: noteanddata
- Creation Date: Sun Feb 03 2019 12:04:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 03 2019 12:04:57 GMT+0800 (Singapore Standard Time)

<p>
update on 2019/03/10  with http://www.noteanddata.com/leetcode-988-Smallest-String-Starting-From-Leaf-java-solution-update.html   thanks for @ayyildiz pointing out my mistakes

below is the new version:
```
public String smallestFromLeaf(TreeNode root) {
    return dfs(root, "");
}

public String dfs(TreeNode node, String suffix) {
    if(null == node) {
        return suffix;
    }
    suffix = "" + (char)(\'a\' + node.val) + suffix;
    if(null == node.left && null == node.right) {
        return suffix;
    }
    if(null == node.left || null == node.right) {
        return (null == node.left)? dfs(node.right, suffix) :dfs(node.left, suffix);
    }
    
    String left = dfs(node.left, suffix);
    String right = dfs(node.right, suffix);
    
    return left.compareTo(right) <= 0? left: right;
}
```


**previous version can be accepted but is wrong**
http://www.noteanddata.com/leetcode-988-Smallest-String-Starting-From-Leaf-java-solution-note.html

```
public String smallestFromLeaf(TreeNode root) {
    return dfs(root);
}

public String dfs(TreeNode node) {
    if(null == node) {
        return null;
    }
    else {
        char ch = (char)(\'a\' + node.val);
        String left = dfs(node.left);
        String right = dfs(node.right);
        if(left == null && right == null) { // leaf
            return "" + ch;
        }
        else if(left == null || right == null) {
            return left != null ? (left + ch) : (right+ch);
        }
        else { // both are not null
            if(left.compareTo(right) < 0) {
                return left + ch;
            }
            else {
                return right + ch;
            }
        }
    }
}
```
</p>


