---
title: "Moving Average from Data Stream"
weight: 329
#id: "moving-average-from-data-stream"
---
## Description
<div class="description">
<p>Given a stream of integers and a window size, calculate the moving average of all integers in the sliding window.</p>

<p><strong>Example:</strong></p>

<pre>
MovingAverage m = new MovingAverage(3);
m.next(1) = 1
m.next(10) = (1 + 10) / 2
m.next(3) = (1 + 10 + 3) / 3
m.next(5) = (10 + 3 + 5) / 3
</pre>

<p>&nbsp;</p>

</div>

## Tags
- Design (design)
- Queue (queue)

## Companies
- Google - 8 (taggedByAdmin: true)
- Microsoft - 3 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- AppDynamics - 3 (taggedByAdmin: false)
- Uber - 6 (taggedByAdmin: false)
- Bloomberg - 5 (taggedByAdmin: false)
- Indeed - 3 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Array or List

**Intuition**

Following the description of the problem, we could simply keep track of all the incoming values with the data structure of *Array* or *List*. Then from the data structure, later we retrieve the necessary elements to calculate the average. 

![pic](../Figures/346/346_array.png)

**Algorithm**

- First, we initialize a variable `queue` to store the values from the data stream, and the variable `n` for the size of the moving window.

- At each invocation of `next(val)`, we first append the value to the queue. We then retrieve the last `n` values from the queue, in order to calculate the average. 

<iframe src="https://leetcode.com/playground/PX97ZUYb/shared" frameBorder="0" width="100%" height="344" name="PX97ZUYb"></iframe>



**Complexity**

- Time Complexity: $$\mathcal{O}(N)$$ where $$N$$ is the size of the moving window, since we need to retrieve $$N$$ elements from the queue at each invocation of `next(val)` function.
<br/>
- Space Complexity: $$\mathcal{O}(M)$$, where $$M$$ is the length of the queue which would grow at each invocation of the `next(val)` function.
<br/>
<br/>


---
#### Approach 2: Double-ended Queue

**Intuition**

We could do better than the first approach in both time and space complexity.

>First of all, one might notice that we do not need to keep all values from the data stream, but rather the last `n` values which falls into the moving window.

By definition of the moving window, at each step, we add a new element to the window, and at the same time we remove the oldest element from the window. Here, we could apply a data structure called *double-ended queue* (_a.k.a_ deque) to implement the moving window, which would have the constant time complexity ($$\mathcal{O}(1)$$) to add or remove an element from both its ends. With the deque, we could reduce the space complexity down to $$\mathcal{O}(N)$$ where $$N$$ is the size of the moving window.

![pic](../Figures/346/346_deque.png)

>Secondly, to calculate the sum, we do not need to reiterate the elements in the moving window.

We could keep the sum of the previous moving window, then in order to obtain the sum of the new moving window, we simply add the new element and deduce the oldest element. With this measure, we then can reduce the time complexity to constant.

**Algorithm**

Here is the definition of the _deque_ from Python. We have similar implementation of deque in other programming languages such as Java.

>Deques are a generalization of stacks and queues (the name is pronounced `deck` and is short for `double-ended queue`). Deques support thread-safe, memory efficient appends and pops from either side of the deque with approximately the same O(1) performance in either direction.

Follow the intuition, we replace the queue with the _deque_ and add a new variable `window_sum` in order to calculate the sum of moving window in constant time.

<iframe src="https://leetcode.com/playground/WZWyk4dv/shared" frameBorder="0" width="100%" height="378" name="WZWyk4dv"></iframe>



**Complexity**

- Time Complexity: $$\mathcal{O}(1)$$, as we explained in intuition.
<br/>
- Space Complexity: $$\mathcal{O}(N)$$, where $$N$$ is the size of the moving window.
<br/>
<br/>


---
#### Approach 3: Circular Queue with Array

**Intuition**

Other than the _deque_ data structure, one could also apply another fun data structure called `circular queue`, which is basically a queue with the circular shape.

![pic](../Figures/346/346_circular_queue.png)

- The major advantage of circular queue is that by adding a new element to a full circular queue, it automatically discards the oldest element. Unlike deque, we do not need to explicitly remove the oldest element.
<br/>
- Another advantage of circular queue is that a single index suffices to keep track of both ends of the queue, unlike deque where we have to keep a pointer for each end.

**Algorithm**

No need to resort to any library, one could easily implement a circular queue with a fixed-size array. The key to the implementation is the correlation between the index of `head` and `tail` elements, which we could summarize in the following formula:

$$
    \text{tail} = (\text{head} + 1) \mod \text{size}
$$

In other words, the `tail` element is right next to the `head` element. Once we move the head forward, we would overwrite the previous tail element.

![pic](../Figures/346/346_snake.png)

<iframe src="https://leetcode.com/playground/XQ5o5BZW/shared" frameBorder="0" width="100%" height="378" name="XQ5o5BZW"></iframe>



**Complexity**

- Time Complexity: $$\mathcal{O}(1)$$, as we can see that there is no loop in the `next(val)` function.

- Space Complexity: $$\mathcal{O}(N)$$, where $$N$$ is the size of the circular queue.


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(1) time solution.
- Author: sculd
- Creation Date: Sun May 01 2016 05:15:49 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 02:53:49 GMT+0800 (Singapore Standard Time)

<p>
The idea is to keep the sum so far and update the sum just by replacing the oldest number with the new entry.

    public class MovingAverage {
        private int [] window;
        private int n, insert;
        private long sum;
        
        /** Initialize your data structure here. */
        public MovingAverage(int size) {
            window = new int[size];
            insert = 0;
            sum = 0;
        }
        
        public double next(int val) {
            if (n < window.length)  n++;
            sum -= window[insert];
            sum += val;
            window[insert] = val;
            insert = (insert + 1) % window.length;
            
            return (double)sum / n;
        }
    }
</p>


### Java easy to understand solution
- Author: bdwalker
- Creation Date: Mon May 02 2016 14:37:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 03:16:09 GMT+0800 (Singapore Standard Time)

<p>
Essentially, we just need to keep track of the sum of the current window as we go. This prevents an O(n) traversal over the Queue as we add new numbers to get the new average.  If we need to evict then we just subtract that number off of our sum and divide by the size. 

    public class MovingAverage {
    private double previousSum = 0.0;
    private int maxSize;
    private Queue<Integer> currentWindow;
    
    public MovingAverage(int size) {
        currentWindow = new LinkedList<Integer>();
        maxSize = size;
    }
    
    public double next(int val) {
        if (currentWindow.size() == maxSize)
        {
            previousSum -= currentWindow.remove();
        }
        
        previousSum += val;
        currentWindow.add(val);
        return previousSum / currentWindow.size();
    }}
</p>


### 4-line Python Solution using deque
- Author: myliu
- Creation Date: Sun May 01 2016 07:07:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 01 2016 07:07:14 GMT+0800 (Singapore Standard Time)

<p>
    import collections
    
    class MovingAverage(object):
    
        def __init__(self, size):
            """
            Initialize your data structure here.
            :type size: int
            """
            self.queue = collections.deque(maxlen=size)
            
    
        def next(self, val):
            """
            :type val: int
            :rtype: float
            """
            queue = self.queue
            queue.append(val)
            return float(sum(queue))/len(queue)
            
    
    
    # Your MovingAverage object will be instantiated and called as such:
    # obj = MovingAverage(size)
    # param_1 = obj.next(val)
</p>


