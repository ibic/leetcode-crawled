---
title: "Find All Good Strings"
weight: 1296
#id: "find-all-good-strings"
---
## Description
<div class="description">
<p>Given the strings <code>s1</code> and <code>s2</code> of size <code>n</code>, and the string <code>evil</code>. <em>Return the number of <strong>good</strong> strings</em>.</p>

<p>A <strong>good</strong> string has size <code>n</code>, it is alphabetically greater than or equal to <code>s1</code>, it is alphabetically smaller than or equal to <code>s2</code>, and it does not contain the string <code>evil</code> as a substring. Since the answer can be a huge number, return this modulo 10^9 + 7.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 2, s1 = &quot;aa&quot;, s2 = &quot;da&quot;, evil = &quot;b&quot;
<strong>Output:</strong> 51 
<strong>Explanation:</strong> There are 25 good strings starting with &#39;a&#39;: &quot;aa&quot;,&quot;ac&quot;,&quot;ad&quot;,...,&quot;az&quot;. Then there are 25 good strings starting with &#39;c&#39;: &quot;ca&quot;,&quot;cc&quot;,&quot;cd&quot;,...,&quot;cz&quot; and finally there is one good string starting with &#39;d&#39;: &quot;da&quot;.&nbsp;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 8, s1 = &quot;leetcode&quot;, s2 = &quot;leetgoes&quot;, evil = &quot;leet&quot;
<strong>Output:</strong> 0 
<strong>Explanation:</strong> All strings greater than or equal to s1 and smaller than or equal to s2 start with the prefix &quot;leet&quot;, therefore, there is not any good string.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 2, s1 = &quot;gx&quot;, s2 = &quot;gz&quot;, evil = &quot;x&quot;
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>s1.length == n</code></li>
	<li><code>s2.length == n</code></li>
	<li><code>s1 &lt;= s2</code></li>
	<li><code>1 &lt;= n &lt;= 500</code></li>
	<li><code>1 &lt;= evil.length &lt;= 50</code></li>
	<li>All strings consist of lowercase English letters.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- dunzo - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### I will not apply the company giving the question
- Author: shiminlei
- Creation Date: Sun Mar 29 2020 12:07:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 29 2020 12:07:01 GMT+0800 (Singapore Standard Time)

<p>
>0< >0< >0< >0< >0<
</p>


### [Java/C++] Memoization DFS & KMP - with Picture - Clean code ~ 12ms
- Author: hiepit
- Creation Date: Sun Mar 29 2020 23:32:39 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 13 2020 23:25:34 GMT+0800 (Singapore Standard Time)

<p>
**Some words**
It\'s a really hard problem. For this problem I prefer to explain in the solution code. I also create a following example with explanation picture, hope you can understand a bit from that.

**Input:** n = 2, s1 = "db", s2 = "fe", evil = "e"
**Output:** 24+4=28
**Explanation:**
![image](https://assets.leetcode.com/users/hiepit/image_1585506505.png)


**Java ~ 12ms**
```java
    public int findGoodStrings(int n, String s1, String s2, String evil) {
        int[] dp = new int[1 << 17]; // Need total 17 bits, can check getKey() function
        return dfs(0, 0, true, true,
                n, s1.toCharArray(), s2.toCharArray(), evil.toCharArray(), computeLPS(evil.toCharArray()), dp);
    }
    int dfs(int i, int evilMatched, boolean leftBound, boolean rightBound,
            int n, char[] s1, char[] s2, char[] evil, int[] lps, int[] dp) {
        if (evilMatched == evil.length) return 0; // contain `evil` as a substring -> not good string
        if (i == n) return 1; // it\'s a good string
        int key = getKey(i, evilMatched, leftBound, rightBound);
        if (dp[key] != 0) return dp[key];
        char from = leftBound ? s1[i] : \'a\';
        char to = rightBound ? s2[i] : \'z\';
        int res = 0;
        for (char c = from; c <= to; c++) {
            int j = evilMatched; // j means the next match between current string (end at char `c`) and `evil` string
            while (j > 0 && evil[j] != c) j = lps[j - 1];
            if (c == evil[j]) j++;
            res += dfs(i + 1, j, leftBound && (c == from), rightBound && (c == to),
                    n, s1, s2, evil, lps, dp);
            res %= 1000000007;
        }
        return dp[key] = res;
    }
    int getKey(int n, int m, boolean b1, boolean b2) {
        // Need 9 bits store n (2^9=512), 6 bits store m (2^6=64), 1 bit store b1, 1 bit store b2
        return (n << 8) | (m << 2) | ((b1 ? 1 : 0) << 1) | (b2 ? 1 : 0);
    }
    int[] computeLPS(char[] str) { // Longest Prefix also Suffix
        int n = str.length;
        int[] lps = new int[n];
        for (int i = 1, j = 0; i < n; i++) {
            while (j > 0 && str[i] != str[j]) j = lps[j - 1];
            if (str[i] == str[j]) lps[i] = ++j;
        }
        return lps;
    }
```

**C++ ~ 20ms**
```c++
    int dp[501][51][2][2] = {};
    int findGoodStrings(int n, string s1, string s2, string evil) {
        return dfs(0, 0, true, true,
			n, s1, s2, evil, computeLPS(evil));
    }
    int dfs(int i, int evilMatched, bool leftBound, bool rightBound,
			int n, string& s1, string& s2, string& evil, const vector<int>& lps) {
        if (evilMatched == evil.size()) return 0; // contain `evil` as a substring -> not good string
        if (i == n) return 1; // it\'s a good string
        if (dp[i][evilMatched][leftBound][rightBound] != 0) return dp[i][evilMatched][leftBound][rightBound];
        char from = leftBound ? s1[i] : \'a\';
        char to = rightBound ? s2[i] : \'z\';
        int res = 0;
        for (char c = from; c <= to; c++) {
            int j = evilMatched; // j means the next match between current string (end at char `c`) and `evil` string
            while (j > 0 && evil[j] != c) j = lps[j - 1];
            if (c == evil[j]) j++;
            res += dfs(i + 1, j, leftBound && (c == from), rightBound && (c == to),
                    n, s1, s2, evil, lps);
            res %= 1000000007;
        }
        return dp[i][evilMatched][leftBound][rightBound] = res;
    }
    vector<int> computeLPS(const string& str) { // Longest Prefix also Suffix
        int n = str.size();
        vector<int> lps = vector<int>(n);
        for (int i = 1, j = 0; i < n; i++) {
            while (j > 0 && str[i] != str[j]) j = lps[j - 1];
            if (str[i] == str[j]) lps[i] = ++j;
        }
        return lps;
    }
```

Complexity
- Time: `O(n*m*2*2*26)`, where `n<=500`, `m<=50` is length of `evil`
- Space: `O(m*n*2*2)`

**Reference**
- KMP Table: [1392. Longest Happy Prefix](https://leetcode.com/problems/longest-happy-prefix/discuss/553594/java-6-lines-kmp-solution-clean-code-on)

Inspired by @plus2047 from this [post](https://leetcode.com/problems/find-all-good-strings/discuss/554806/)

If it helps you easy to understand, please help to **UPVOTE**.
If you have any questions, feel free to comment below.
Happy coding!
</p>


### O(N * len(evil)) Solution with memorized DP & KMP algorithm
- Author: plus2047
- Creation Date: Sun Mar 29 2020 12:01:46 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Apr 01 2020 19:35:02 GMT+0800 (Singapore Standard Time)

<p>
```py
def getKmpNext(pattern):
    """ get KMP next arrray
    
    next[i] is the biggest k s.t. pattern[:k] == pattern[:i + 1][-k:]
    """
    nextArr = [0] * len(pattern)
    i, j = 1, 0
    while i < len(pattern):
        while j and pattern[j] != pattern[i]:
            j = nextArr[j - 1]
        if pattern[i] == pattern[j]:
            j += 1
        nextArr[i] = j
        i += 1
    return nextArr

class Solution:
    def findGoodStrings(self, n: int, s1: str, s2: str, evil: str) -> int:
        from functools import lru_cache
        
        m = len(evil)
        mod = int(1E9 + 7)
        nextArr = getKmpNext(evil)
        
        @lru_cache(None)
        def cnt(idx, pre1, pre2, preE):
            """
            we try to build target string each char.
            
            the basic idea is that we can get the count of valid string which begin with a special prefix.
            but if we use the prefix as a parameter, the DP space will be too big.
            so we just extract three params from the prefix: if it\'s prefix of s1 or s2 or evil.
            
            idx(int) is the index char of target to build.
            
            pre1(bool) means if the prefix of current position is a prefix of s1.
                if it is, the char we put here cannot less than s1[idx].
            
            pre2(bool) means if the prefix of current position is a prefix of s2.
                if it is, the char we put here cannot greater than s2[idx].
                
            preE(int) is the max length of commom prefix between current position and evil.
                if it is len(evil), we cannot build any string begin with current prefix.
                we use KMP algorithm to calculate it.
            """
            if preE == m: return 0
            if idx == n: return 1
            
            total = 0
            first = ord(s1[idx]) if pre1 else ord(\'a\')
            last = ord(s2[idx]) if pre2 else ord(\'z\')
            
            for ci in range(first, last + 1):
                c = chr(ci)
                
                _pre1 = pre1 and ci == first
                _pre2 = pre2 and ci == last

                # using KMP to calculate _preE
                _preE = preE
                while _preE and c != evil[_preE]:
                    _preE = nextArr[_preE - 1]
                if c == evil[_preE]:
                    _preE += 1

                total += cnt(idx + 1, _pre1, _pre2, _preE)
                total %= mod
            
            return total
        
        return cnt(0, True, True, 0);
```
</p>


