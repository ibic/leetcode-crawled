---
title: "Find Right Interval"
weight: 413
#id: "find-right-interval"
---
## Description
<div class="description">
<p>You are given an array of&nbsp;<code>intervals</code>, where <code>intervals[i] = [start<sub>i</sub>, end<sub>i</sub>]</code>&nbsp;and each <code>start<sub>i</sub></code>&nbsp;is <strong>unique</strong>.</p>

<p>The <strong>r</strong><strong>ight</strong><strong>&nbsp;interval</strong>&nbsp;for an interval <code>i</code> is an interval&nbsp;<code>j</code>&nbsp;such that <code>start<sub>j</sub></code><code>&nbsp;&gt;= end<sub>i</sub></code>&nbsp;and&nbsp;<code>start<sub>j</sub></code>&nbsp;is&nbsp;<strong>minimized</strong>.</p>

<p>Return&nbsp;<em>an array of&nbsp;<strong>right interval</strong>&nbsp;indices for each interval&nbsp;<code>i</code></em>. If no&nbsp;<strong>right interval</strong>&nbsp;exists for interval&nbsp;<code>i</code>, then put&nbsp;<code>-1</code>&nbsp;at index <code>i</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[1,2]]
<strong>Output:</strong> [-1]
<strong>Explanation:</strong> There is only one interval in the collection, so it outputs -1.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[3,4],[2,3],[1,2]]
<strong>Output:</strong> [-1,0,1]
<strong>Explanation:</strong> There is no right interval for [3,4].
The right interval for [2,3] is [3,4] since start<sub>0</sub>&nbsp;= 3 is the smallest start that is &gt;= end<sub>1</sub>&nbsp;= 3.
The right interval for [1,2] is [2,3] since start<sub>1</sub>&nbsp;= 2 is the smallest start that is &gt;= end<sub>2</sub>&nbsp;= 2.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[1,4],[2,3],[3,4]]
<strong>Output:</strong> [-1,2,-1]
<strong>Explanation:</strong> There is no right interval for [1,4] and [3,4].
The right interval for [2,3] is [3,4] since start<sub>2</sub> = 3 is the smallest start that is &gt;= end<sub>1</sub>&nbsp;= 3.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;=&nbsp;intervals.length &lt;= 2 * 10<sup>4</sup></code></li>
	<li><code>intervals[i].length == 2</code></li>
	<li><code>-10<sup>6</sup> &lt;= start<sub>i</sub> &lt;= end<sub>i</sub> &lt;= 10<sup>6</sup></code></li>
	<li>The start point&nbsp;of each interval is <strong>unique</strong>.</li>
</ul>

</div>

## Tags
- Binary Search (binary-search)

## Companies


## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force

 The simplest solution consists of picking up every interval in the set and looking for the the interval whose start point is larger(by a
 minimum difference) than
 the chosen interval's end point by scanning the complete set for every interval chosen. While scanning, we keep a track of the interval
 with the minimum start point satisfying the given criteria along with its index. The result obtained for every interval chosen is
 stored at the corresponding index in the res array which is returned at the end.

 
<iframe src="https://leetcode.com/playground/3P2LgoE8/shared" frameBorder="0" width="100%" height="344" name="3P2LgoE8"></iframe>

 **Complexity Analysis**

 * Time complexity : $$\mathcal{O}(n^2)$$. The complete set of $$n$$ intervals is scanned for every($$n$$) interval chosen.

 * Space complexity : $$\mathcal{O}(n)$$. $$\text{res}$$ array of size $$n$$ is used.
</br>
</br>


---

#### Approach 2: Using Sorting + Scanning

We make use of a hashmap $$\text{hash}$$, which stores the data in the form of a $$\text{(Key, Value)}$$ pair. Here, the $$\text{Key}$$ corresponds to the interval chosen and the $$\text{Value}$$ corresponds to the index of the particular interval in the given $$\text{intervals}$$ array. We store every element of the $$\text{intervals}$$ array in the $$hash$$-map.

Now, we sort the $$\text{intervals}$$ array based on the starting points. We needed to store the indices of the array in the hashmap, so as to be able to obtain the indices even after the sorting.

Now, we pick up every interval of the sorted array, and find out the interval from the remaining ones whose start point comes just after
the end point of the interval chosen. How do we proceed? Say, we've picked up the $$i^{th}$$ interval right now. In order to find an
interval satisfying the given criteria, we need not search in the intervals behind it. This is because the $$\text{intervals}$$ array has been sorted based on the
starting points and the end point is always greater than the starting point for a given interval. Thus, we search in the intervals only with indices $$j$$, such that $$i+1< j < n$$. The first element encountered while scanning in the ascending order is the required result for the interval chosen, since all the intervals lying after this interval will have comparatively larger start points.

Then, we can obtain the index corresponding to the corresponding interval from the hashmap, which is stored in the corresponding entry of the $$res$$ array. If no interval satisfies the criteria, we put a $$\text{-1}$$ in the corresponding entry.


<iframe src="https://leetcode.com/playground/AoZ7bmfi/shared" frameBorder="0" width="100%" height="446" name="AoZ7bmfi"></iframe>

 **Complexity Analysis**

 * Time complexity : $$O(n^2)$$.
    - Sorting takes $$O\big(n\log{n}\big)$$ time.
    - For the first interval we need to search among $$n-1$$ elements.
    - For the second interval, the search is done among $$n-2$$ elements and so on leading to a total of: $$(n-1) + (n-2) + ... + 1 = \frac{n.(n-1)}{2} = O(n^2)$$ calculations.

 * Space complexity : $$O(n)$$. $$\text{res}$$ array of size $$n$$ is used. A hashmap $$\text{hash}$$ of size $$n$$ is used.
</br>
</br>


---

#### Approach 3: Using Sorting + Binary Search

We can optimize the above approach to some extent, since we can make use of the factor of the $$\text{intervals}$$ array being sorted. Instead of searching for the required interval in a linear manner, we can make use of Binary Search to find an interval whose start point is just larger than the end point of the current interval.

Again, if such an interval is found, we obtain its index from the hashmap and store the result in the appropriate $$\text{res}$$ entry. If not, we put a $$\text{-1}$$ at the corresponding entry.


<iframe src="https://leetcode.com/playground/5xwcoD8d/shared" frameBorder="0" width="100%" height="500" name="5xwcoD8d"></iframe>

 **Complexity Analysis**

 * Time complexity : $$O\big(n\log{n}\big)$$. Sorting takes $$O\big(n\log{n}\big)$$ time. Binary search takes $$O\big(\log{n}\big)$$ time for each of the $$n$$ intervals.

 * Space complexity : $$O(n)$$. $$\text{res}$$ array of size $$n$$ is used. A hashmap $$\text{hash}$$ of size $$O(n)$$ is used.
</br>
</br>


---

#### Approach 4: Using TreeMap

In this approach, instead of using a hashmap, we make use of a TreeMap $$\text{starts}$$, which is simply a Red-Black Tree(a kind of balanced Binary Search Tree) . This Treemap $$\text{start}$$ stores data in the form of $$\text{(Key, Value)}$$ pair and always remain sorted based on its keys.
In our case, we store the data such that the start point of an interval acts as the $$\text{Key}$$ and the index corresponding to the interval acts as the value, since we are concerned with data sorted based on the start points, as discussed in previous approaches. Every element of the $$\text{intervals}$$ array is stored in the TreeMap.

Now, we choose each element of the $$\text{intervals}$$ array and make use of a function `TreeMap.ceilingEntry(end_point)` to obtain the element in the TreeMap with its $$\text{Key}$$ just larger than the $$\text{end\_point}$$ of the currently chosen interval. The function `ceilingEntry(Key)` returns the element just with its $$\text{Key}$$ larger than the `Key`(passed as the argument) from amongst the elements of the TreeMap and returns `null` if no such element exists.

If non-null value is returned, we obtain the $$\text{Value}$$ from the $$\text{(Key, Value)}$$ pair obtained at the appropriate entry in the $$\text{res}$$ array. If a null value is returned, we simply store a $$\text{-1}$$ at the corresponding $$\text{res}$$ entry.

<iframe src="https://leetcode.com/playground/TYzf827L/shared" frameBorder="0" width="100%" height="310" name="TYzf827L"></iframe>

 **Complexity Analysis**

 * Time complexity : $$O\big(N \cdot \log{N}\big)$$. Inserting an element into TreeMap takes $$O\big(\log{N}\big)$$ time. $$N$$ such insertions are done. The search in TreeMap using `ceilingEntry` also takes $$O\big(\log{N}\big)$$ time. $$N$$ such searches are done.

 * Space complexity : $$O(N)$$. $$\text{res}$$ array of size $$n$$ is used. TreeMap $$\text{starts}$$ of size $$O(N)$$ is used.
</br>
</br>


---

#### Approach 5: Using Two Arrays without Binary Search

**Algorithm**

The intuition behind this approach is as follows: If we maintain two arrays,

1. $$\text{intervals}$$, which is sorted based on the start points.

2. $$\text{endIntervals}$$, which is sorted based on the end points.

Once we pick up the first interval(or, say the $$i^{th}$$ interval) from the $$\text{endIntervals}$$ array, we can determine the appropriate interval satisfying the right interval criteria by scanning the intervals in $$\text{intervals}$$ array from left towards the right, since the $$\text{intervals}$$ array is sorted based on the start points. Say, the index of the element chosen from the $$\text{intervals}$$ array happens to be $$j$$.

Now, when we pick up the next interval(say the $$(i+1)^{th}$$ interval) from the $$\text{endIntervals}$$ array, we need not start scanning the $$\text{intervals}$$ array from the first index. Rather, we can start off directly from the $$j^{th}$$ index where we left off last time in the $$\text{intervals}$$ array. This is because end point corresponding to $$\text{endIntervals[i+1]}$$ is larger than the one corresponding to $$\text{endIntervals[i]}$$ and none of the intervals from $$\text{intervals[k]}$$, such that $$0< k < j$$, satisfies the right neighbor criteria with $$\text{endIntervals[i]}$$, and hence not with $$\text{endIntervals[i+1]}$$ as well.

If at any moment, we reach the end of the array i.e. $$j=\text{intervals.length}$$ and no element satisfying the right interval criteria is available in the $$\text{intervals}$$ array, we put a $$\text{-1}$$ in the corresponding $$\text{res}$$ entry. The same holds for all the remaining elements of the $$\text{endIntervals}$$ array, whose end points are even larger than the previous interval encountered.

Also we make use of a hashmap $$\text{hash}$$ initially to preserve the indices corresponding to the intervals even after sorting.

For more understanding see the below animation:

<!--![Find_Right_Interval](../Figures/436_Find_Right_Interval.gif)-->
!?!../Documents/436_Find.json:1000,563!?!

<iframe src="https://leetcode.com/playground/96aZgaoZ/shared" frameBorder="0" width="100%" height="429" name="96aZgaoZ"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(N \cdot \log{N}\big)$$. Sorting takes $$O\big(N \cdot \log{N}\big)$$ time. A total of $$O(N)$$ time is spent on searching for the appropriate intervals, since the $$\text{endIntervals}$$ and $$\text{intervals}$$ array is scanned only once.

* Space complexity : $$O(N)$$. $$\text{endIntervals}$$, $$\text{intervals}$$ and $$\text{res}$$ array of size $$N$$ are used. A hashmap $$\text{hash}$$ of size $$O(N)$$ is used.
</br>
</br>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java clear O(n logn) solution based on TreeMap
- Author: ivtoskov
- Creation Date: Tue Nov 01 2016 18:27:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 03:46:42 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public int[] findRightInterval(Interval[] intervals) {
        int[] result = new int[intervals.length];
        java.util.NavigableMap<Integer, Integer> intervalMap = new TreeMap<>();
        
        for (int i = 0; i < intervals.length; ++i) {
            intervalMap.put(intervals[i].start, i);    
        }
        
        for (int i = 0; i < intervals.length; ++i) {
            Map.Entry<Integer, Integer> entry = intervalMap.ceilingEntry(intervals[i].end);
            result[i] = (entry != null) ? entry.getValue() : -1;
        }
        
        return result;
    }
}
```
</p>


### C++ map solution
- Author: Hanafubuki
- Creation Date: Mon Oct 31 2016 12:04:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 18 2018 02:07:14 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    vector<int> findRightInterval(vector<Interval>& intervals) {
        map<int, int> hash;
        vector<int> res;
        int n = intervals.size();
        for (int i = 0; i < n; ++i)
            hash[intervals[i].start] = i;
        for (auto in : intervals) {
            auto itr = hash.lower_bound(in.end);
            if (itr == hash.end()) res.push_back(-1);
            else res.push_back(itr->second);
        }
        return res;
    }
};
```
</p>


### Python O(nlogn) short solution with explanation
- Author: dalwise
- Creation Date: Mon Oct 31 2016 11:45:11 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 07:18:03 GMT+0800 (Singapore Standard Time)

<p>
My solution from contest with minimal cleanup. For each end point search for the first start point that is equal or higher in a previously constructed ordered list of start points. If there is one then return its index. If not return -1:
```
def findRightInterval(self, intervals):
    l = sorted((e.start, i) for i, e in enumerate(intervals))
    res = []
    for e in intervals:
        r = bisect.bisect_left(l, (e.end,))
        res.append(l[r][1] if r < len(l) else -1)
    return res
```
</p>


