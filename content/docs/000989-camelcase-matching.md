---
title: "Camelcase Matching"
weight: 989
#id: "camelcase-matching"
---
## Description
<div class="description">
<p>A query word matches a given <code>pattern</code> if we can insert <strong>lowercase</strong> letters to the pattern word so that it equals the <code>query</code>. (We may insert each character at any position, and may insert 0 characters.)</p>

<p>Given a list of <code>queries</code>, and a <code>pattern</code>, return an <code>answer</code> list of booleans, where <code>answer[i]</code> is true if and only if <code>queries[i]</code> matches the <code>pattern</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>queries = <span id="example-input-1-1">[&quot;FooBar&quot;,&quot;FooBarTest&quot;,&quot;FootBall&quot;,&quot;FrameBuffer&quot;,&quot;ForceFeedBack&quot;]</span>, pattern = <span id="example-input-1-2">&quot;FB&quot;</span>
<strong>Output: </strong><span id="example-output-1">[true,false,true,true,false]</span>
<strong>Explanation: </strong>
&quot;FooBar&quot; can be generated like this &quot;F&quot; + &quot;oo&quot; + &quot;B&quot; + &quot;ar&quot;.
&quot;FootBall&quot; can be generated like this &quot;F&quot; + &quot;oot&quot; + &quot;B&quot; + &quot;all&quot;.
&quot;FrameBuffer&quot; can be generated like this &quot;F&quot; + &quot;rame&quot; + &quot;B&quot; + &quot;uffer&quot;.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>queries = <span id="example-input-2-1">[&quot;FooBar&quot;,&quot;FooBarTest&quot;,&quot;FootBall&quot;,&quot;FrameBuffer&quot;,&quot;ForceFeedBack&quot;]</span>, pattern = <span id="example-input-2-2">&quot;FoBa&quot;</span>
<strong>Output: </strong><span id="example-output-2">[true,false,true,false,false]</span>
<strong>Explanation: </strong>
&quot;FooBar&quot; can be generated like this &quot;Fo&quot; + &quot;o&quot; + &quot;Ba&quot; + &quot;r&quot;.
&quot;FootBall&quot; can be generated like this &quot;Fo&quot; + &quot;ot&quot; + &quot;Ba&quot; + &quot;ll&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>queries = <span id="example-input-3-1">[&quot;FooBar&quot;,&quot;FooBarTest&quot;,&quot;FootBall&quot;,&quot;FrameBuffer&quot;,&quot;ForceFeedBack&quot;]</span>, pattern = <span id="example-input-3-2">&quot;FoBaT&quot;</span>
<strong>Output: </strong><span id="example-output-3">[false,true,false,false,false]</span>
<strong>Explanation: </strong>
&quot;FooBarTest&quot; can be generated like this &quot;Fo&quot; + &quot;o&quot; + &quot;Ba&quot; + &quot;r&quot; + &quot;T&quot; + &quot;est&quot;.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= queries.length &lt;= 100</code></li>
	<li><code>1 &lt;= queries[i].length &lt;= 100</code></li>
	<li><code>1 &lt;= pattern.length &lt;= 100</code></li>
	<li>All strings consists only of lower and upper case English letters.</li>
</ol>

</div>

## Tags
- String (string)
- Trie (trie)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Easy Two Pointers
- Author: yuanb10
- Creation Date: Sun Apr 07 2019 12:06:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 07 2019 12:06:18 GMT+0800 (Singapore Standard Time)

<p>
For each string, macth it with the pattern and pass the result.

The match process uses i for query pointer and j for pattern pointer, each iteration;
1. If current char query[i] matches pattern[j], increase pattern pointer;
2. if does not match and query[i] is lowercase, keep going;
3. if does not match and query[i] is captalized, we should return false.

If this pattern matches, j should equal length of pattern at the end.

Hope this helps.

```
class Solution {
    public List<Boolean> camelMatch(String[] queries, String pattern) {
        List<Boolean> res = new ArrayList<>();
        
        char[] patternArr = pattern.toCharArray();
        for (String query : queries) {
            boolean isMatch = match(query.toCharArray(), patternArr);
            res.add(isMatch);
        }
        
        return res;
    }
    
    private boolean match(char[] queryArr, char[] patternArr) {
        int j = 0;
        for (int i = 0; i < queryArr.length; i++) {
            if (j < patternArr.length && queryArr[i] == patternArr[j]) {
                j++;
            } else if (queryArr[i] >= \'A\' && queryArr[i] <= \'Z\') {
                return false;
            }
        }
        
        return j == patternArr.length;
    }
}
```

Updated naming convention and coding style per comments by destiny130@ . Thanks for your advice.

Possible improvement, special thanks to @Sithis:
1.  Use ```new ArrayList<>(queries.length)``` to allocate capacity up-front. This can avoid resizing and copying as the size of the array grows.

2. ```queryArr[i] >= \'A\' && queryArr[i] <= \'Z\'``` can be replaced by built-in static method ```Character.isUpperCase()```.


</p>


### [Java/C++/Python] Check Subsequence and Regax
- Author: lee215
- Creation Date: Sun Apr 07 2019 12:12:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 07 2019 12:12:09 GMT+0800 (Singapore Standard Time)

<p>
## **Solution 1: Check Subsequence**

**Java**
```
    public List<Boolean> camelMatch(String[] queries, String pattern) {
        List<Boolean> res = new ArrayList<>();
        for (String query : queries)
            res.add(isMatch(query, pattern));
        return res;
    }

    private boolean isMatch(String query, String pattern) {
        int i = 0;
        for (char c: query.toCharArray()) {
            if (i < pattern.length() && c == pattern.charAt(i))
                i++;
            else if (c < \'a\')
                return false;
        }
        return i == pattern.length();
    }
```

**C++**
```
    vector<bool> camelMatch(vector<string>& queries, string pattern) {
        vector<bool> res;
        for (string &query : queries) res.push_back(isMatch(query, pattern));
        return res;

    }

    bool isMatch(string query, string pattern) {
        int i = 0;
        for (char & c : query)
            if (i < pattern.length() && c == pattern[i]) i++;
            else if (c < \'a\') return false;
        return i == pattern.length();
    }
```

**Python:**
```
    def camelMatch(self, qs, p):
        def u(s):
            return [c for c in s if c.isupper()]

        def issup(s, t):
            it = iter(t)
            return all(c in it for c in s)
        return [u(p) == u(q) and issup(p, q) for q in qs]
```


## **Solution 2: Use Regax**

Join `"[a-z]*"` into `pattern` characters.
Slow compared with solution 1.

**Java**
```
    public List<Boolean> camelMatch(String[] queries, String pattern) {;
        return Arrays.stream(queries).map(q -> q.matches("[a-z]*" + String.join("[a-z]*", pattern.split("")) + "[a-z]*")).collect(Collectors.toList());
    }
```

**Python**
```
    def camelMatch(self, qs, p):
        return [re.match("^[a-z]*" + "[a-z]*".join(p) + "[a-z]*$", q) != None for q in qs]
```

**C++**
```
    vector<bool> camelMatch(vector<string>& queries, string pattern) {
        vector<bool> res;
        string p = "[a-z]*";
        for (char &c: pattern)
            p += string(1, c) + "[a-z]*";
        regex r(p);
        for (string &query : queries)
            res.push_back(regex_match(query, r) );
        return res;
    }
```
</p>


### [C++] [4ms] O(queries.length*pattern.length) No regex
- Author: PhoenixDD
- Creation Date: Mon Apr 08 2019 11:36:12 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 07 2019 03:48:58 GMT+0800 (Singapore Standard Time)

<p>
Regex solutions are very slow compared to a tailor-made solution in these kind of questions.

The solution is pretty straight forward. For each string in queries compare it with the pattern.

For comparison follow these steps
* Start with index=0 representing the character to match in pattern.
* Loop all characters in string to be matched with pattern.
* Whenever we encounter a character that is equal to pattern[index] , increase index=index+1.
* If the character encountered is uppercase and it doesnt match pattern[index] return false.
* Return true after all characters in string are parsed only if index=Pattern.length.

```c++
class Solution {
public:
    bool check(string &a,string &b)
    {
        int i=0;
        for(char &c:b)
            if(isupper(c)&&a[i]!=c)            //We are only allowed to add lower case characters, Upper case must match with pattern
                return false;
            else if(c==a[i])                        //If pattern[i] matches c we move forward to find the next matching character from pattern
                i++;
        return i==a.length();                   //Return true only if all characters from patern are matched.
    }
    vector<bool> camelMatch(vector<string>& queries, string pattern) 
    {
        vector<bool> result;
        for(string &q:queries)
            result.push_back(check(pattern,q));
        return result;
    }
};
```

**Note:** It might seem wrong to match the first character that matches pattern rather than one that occurs later, but it doesn\'t make a difference as we can add lowercase letters at any index thus we pick the first matched letter.
eg: pattern="NoT" query="NropoT"

We can pick either the first \'o\' from query to match the pattern or the second \'o\', it doesn\'t matter which one we choose in this case and hence the solution above works. We pick the first \'o\' and wait for the next letter to match the pattern and consider all other lowercase letters in beween as injected.
</p>


