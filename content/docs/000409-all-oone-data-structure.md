---
title: "All O`one Data Structure"
weight: 409
#id: "all-oone-data-structure"
---
## Description
<div class="description">
<p>Implement a data structure supporting the following operations:</p>

<p>
<ol>
<li>Inc(Key) - Inserts a new key <Key> with value 1. Or increments an existing key by 1. Key is guaranteed to be a <b>non-empty</b> string.</li>
<li>Dec(Key) - If Key's value is 1, remove it from the data structure. Otherwise decrements an existing key by 1. If the key does not exist, this function does nothing. Key is guaranteed to be a <b>non-empty</b> string.</li>
<li>GetMaxKey() - Returns one of the keys with maximal value. If no element exists, return an empty string <code>""</code>.</li>
<li>GetMinKey() - Returns one of the keys with minimal value. If no element exists, return an empty string <code>""</code>.</li>
</ol>
</p>

<p>
Challenge: Perform all these in O(1) time complexity.
</p>
</div>

## Tags
- Design (design)

## Companies
- Facebook - 6 (taggedByAdmin: false)
- LinkedIn - 7 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Uber - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java AC all strict O(1) not average O(1), easy to read
- Author: AaronLin1992
- Creation Date: Mon Oct 31 2016 15:12:15 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 05:55:43 GMT+0800 (Singapore Standard Time)

<p>
Main idea is to maintain a list of Bucket's, each Bucket contains all keys with the same count.
1. ```head``` and ```tail``` can ensure both ```getMaxKey()``` and ```getMaxKey()``` be done in O(1).
2. ```keyCountMap``` maintains the count of keys, ```countBucketMap``` provides O(1) access to a specific Bucket with given count. Deleting and adding a Bucket in the Bucket list cost O(1), so both ```inc()``` and ```dec()``` take strict O(1) time.

```
public class AllOne {
    // maintain a doubly linked list of Buckets
    private Bucket head;
    private Bucket tail;
    // for accessing a specific Bucket among the Bucket list in O(1) time
    private Map<Integer, Bucket> countBucketMap;
    // keep track of count of keys
    private Map<String, Integer> keyCountMap;

    // each Bucket contains all the keys with the same count
    private class Bucket {
        int count;
        Set<String> keySet;
        Bucket next;
        Bucket pre;
        public Bucket(int cnt) {
            count = cnt;
            keySet = new HashSet<>();
        }
    }

    /** Initialize your data structure here. */
    public AllOne() {
        head = new Bucket(Integer.MIN_VALUE);
        tail = new Bucket(Integer.MAX_VALUE);
        head.next = tail;
        tail.pre = head;
        countBucketMap = new HashMap<>();
        keyCountMap = new HashMap<>();
    }
    
    /** Inserts a new key <Key> with value 1. Or increments an existing key by 1. */
    public void inc(String key) {
        if (keyCountMap.containsKey(key)) {
            changeKey(key, 1);
        } else {
            keyCountMap.put(key, 1);
            if (head.next.count != 1) 
                addBucketAfter(new Bucket(1), head);
            head.next.keySet.add(key);
            countBucketMap.put(1, head.next);
        }
    }
    
    /** Decrements an existing key by 1. If Key's value is 1, remove it from the data structure. */
    public void dec(String key) {
        if (keyCountMap.containsKey(key)) {
            int count = keyCountMap.get(key);
            if (count == 1) {
                keyCountMap.remove(key);
                removeKeyFromBucket(countBucketMap.get(count), key);
            } else {
                changeKey(key, -1);
            }
        }
    }
    
    /** Returns one of the keys with maximal value. */
    public String getMaxKey() {
        return tail.pre == head ? "" : (String) tail.pre.keySet.iterator().next();
    }
    
    /** Returns one of the keys with Minimal value. */
    public String getMinKey() {
        return head.next == tail ? "" : (String) head.next.keySet.iterator().next();        
    }
    
    // helper function to make change on given key according to offset
    private void changeKey(String key, int offset) {
        int count = keyCountMap.get(key);
        keyCountMap.put(key, count + offset);
        Bucket curBucket = countBucketMap.get(count);
        Bucket newBucket;
        if (countBucketMap.containsKey(count + offset)) {
            // target Bucket already exists
            newBucket = countBucketMap.get(count + offset);
        } else {
            // add new Bucket
            newBucket = new Bucket(count + offset);
            countBucketMap.put(count + offset, newBucket);
            addBucketAfter(newBucket, offset == 1 ? curBucket : curBucket.pre);
        }
        newBucket.keySet.add(key);
        removeKeyFromBucket(curBucket, key);
    }
    
    private void removeKeyFromBucket(Bucket bucket, String key) {
        bucket.keySet.remove(key);
        if (bucket.keySet.size() == 0) {
            removeBucketFromList(bucket);
            countBucketMap.remove(bucket.count);
        }
    }
    
    private void removeBucketFromList(Bucket bucket) {
        bucket.pre.next = bucket.next;
        bucket.next.pre = bucket.pre;
        bucket.next = null;
        bucket.pre = null;
    }
    
    // add newBucket after preBucket
    private void addBucketAfter(Bucket newBucket, Bucket preBucket) {
        newBucket.pre = preBucket;
        newBucket.next = preBucket.next;
        preBucket.next.pre = newBucket;
        preBucket.next = newBucket;
    }
}
```
</p>


### Python solution with detailed comments
- Author: gabbu
- Creation Date: Thu Jan 05 2017 14:28:38 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 14:30:53 GMT+0800 (Singapore Standard Time)

<p>
**Solution** 

**All Oone Data Structure** https://leetcode.com/problems/all-oone-data-structure/

**Data-Structures**
https://goo.gl/photos/YLhF2qCcBwRpAn58A

Node: A node type to support a doubly linked list. It is a container to hold a bag of keys. It supports:
* add_key(key): Add a key to the bag
* remove_key(key): Remove a key from the bag
* get_any_key(): Returns any random key from the bag. Returns None if bag is empty.
* is_empty(): Returns true if the bag is empty

DoubleLinkedList
* The linked list is implemented using the idea of sentinel nodes, i.e. we have two dummy nodes to represent head and tail. Initially head.next points to tail and tail.prev points to head. Using two dummy nodes dramatically simplifies the implementation.
* insert_after(x): Add a node after node x
* insert_before(x): Add a node before node x
* remove(x): Remove the node from the list
* get_head(): Returns the reference to the real head node
* get_tail() Returns the reference to the real tail node

node_freq: Hashmap with key as frequency and value as Node.
key_counter: Hashmap with key as input key and value as frequency of the key.

**Algorithm Idea**

* A node in the doubly linked list represents a bucket containing a bag of words with a certain frequency. The doubly linked list is maintained in a sorted order with the head node containing words with the least frequency and the tail node containing words with maximum frequency.
* Using this list, getMaxKey and getMinKey can be implemented in O(1) by returning any word contained in the tail and head respectively.
* key_counter is hashmap which allows us to increment or decrement frequency of a key in O(1).
* node_freq is a hashmap which maps a frequency integer to the bucket node in the linked list. Note that we initialize frequency 0 to head sentinel node.
* Now, if we can maintain the sorted order of the linked list in O(1) while performing the increment and decrement operations, we would have a working solution!

**Increment Details**
* While incrementing a key, we first update the key_counter ro reflect the new frequency (cf) of the key.
* Then we test if there is already a bucket with cf using node_freq hashmap. If not, then we need to add a bucket to the linked list. 
* To maintain the sorted invariant, this new bucket must be after the bucket for frequency pf (cf-1). 
* Now unless pf is 0, we are guaranteed that a pf bucket already exists. Therefore, either we add the new bucket after the head node or after the pf bucket. Note that we initialize frequency 0 to head sentinel node. This allows us to use "insert_after" API when previous frequency were zero.
* pf bucket can be retrieved in O(1) using node_freq. insertion in doubly linked list can be done in O(1) as well. Once we have inserted, we add the key to the new bucket.
* Finally we need to remove the key from the previous bucket if pf > 0 (i.e. if a previous bucket exists). Again this can be done in O(1). If the previous bucket becomes empty after removing the key, then we need to also drop the entire bucket from the list.

**Decrement Details**
* While decrementing a key, we first check if the key exisits in key_counter or not. If not, then we simply return. if it does exist, we update the key_counter to reflect the new frequency (cf) of the key. If cf is 0, then we drop this key from the key counter.
* If cf is not in node_freq and cf is not 0, then we need to add a new bucket in the linked list such that the sorted invariant is maintained. Again we are guaranteed to have pf bucket!
* We add the key to the new bucket and remove it from the previous bucket - O(1) operations.

```
from collections import defaultdict
class Node(object):
    def __init__(self):
        self.key_set = set([])
        self.prev, self.nxt = None, None 

    def add_key(self, key):
        self.key_set.add(key)

    def remove_key(self, key):
        self.key_set.remove(key)        

    def get_any_key(self):
        if self.key_set:
            result = self.key_set.pop()
            self.add_key(result)
            return result
        else:
            return None
    
    def count(self):
        return len(self.key_set)

    def is_empty(self):
        return len(self.key_set) == 0


class DoubleLinkedList(object):
    def __init__(self):
        self.head_node, self.tail_node = Node(), Node()
        self.head_node.nxt, self.tail_node.prev = self.tail_node, self.head_node
        return

    def insert_after(self, x):
        node, temp = Node(), x.nxt
        x.nxt, node.prev = node, x
        node.nxt, temp.prev = temp, node
        return node
    
    def insert_before(self, x):
        return self.insert_after(x.prev)

    def remove(self, x):
        prev_node = x.prev
        prev_node.nxt, x.nxt.prev = x.nxt, prev_node
        return

    def get_head(self):
        return self.head_node.nxt
    
    def get_tail(self):
        return self.tail_node.prev

    def get_sentinel_head(self):
        return self.head_node

    def get_sentinel_tail(self):
        return self.tail_node
    
class AllOne(object):
    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.dll, self.key_counter = DoubleLinkedList(), defaultdict(int)
        self.node_freq = {0:self.dll.get_sentinel_head()}

    def _rmv_key_pf_node(self, pf, key):
        node = self.node_freq[pf]
        node.remove_key(key)
        if node.is_empty():
            self.dll.remove(node)
            self.node_freq.pop(pf)
        return

    def inc(self, key):
        """
        Inserts a new key <Key> with value 1. Or increments an existing key by 1.
        :type key: str
        :rtype: void
        """
        self.key_counter[key] += 1
        cf, pf = self.key_counter[key], self.key_counter[key]-1
        if cf not in self.node_freq:
            # No need to test if pf = 0 since frequency zero points to sentinel node
            self.node_freq[cf] = self.dll.insert_after(self.node_freq[pf])
        self.node_freq[cf].add_key(key)
        if pf > 0:
            self._rmv_key_pf_node(pf, key)

    def dec(self, key):
        """
        Decrements an existing key by 1. If Key's value is 1, remove it from the data structure.
        :type key: str
        :rtype: void
        """
        if key in self.key_counter:
            self.key_counter[key] -= 1
            cf, pf = self.key_counter[key], self.key_counter[key]+1
            if self.key_counter[key] == 0:
                self.key_counter.pop(key)
            if cf != 0:
                if cf not in self.node_freq:
                    self.node_freq[cf] = self.dll.insert_before(self.node_freq[pf])
                self.node_freq[cf].add_key(key)
            self._rmv_key_pf_node(pf, key)

    def getMaxKey(self):
        """
        Returns one of the keys with maximal value.
        :rtype: str
        """
        return self.dll.get_tail().get_any_key() if self.dll.get_tail().count() > 0 else ""

    def getMinKey(self):
        """
        Returns one of the keys with Minimal value.
        :rtype: str
        """
        return self.dll.get_head().get_any_key() if self.dll.get_tail().count() > 0 else ""
```
</p>


### C++ solution with comments
- Author: StefanPochmann
- Creation Date: Wed Oct 19 2016 20:24:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 06:59:20 GMT+0800 (Singapore Standard Time)

<p>
For each value, I have a bucket with all keys which have that value. The buckets are in a list, sorted by value. That allows constant time insertion/erasure and iteration to the next higher/lower value bucket. A bucket stores its keys in a hash set for easy constant time insertion/erasure/check (see [first two posts here](https://discuss.leetcode.com/topic/53193/are-hash-tables-ok-here-they-re-not-really-o-1-are-they) if you're worried). I also have one hash map to look up which bucket a given key is in.

Based on a previously flawed Python attempt (I just couldn't find a good way to get an arbitrary element from a set) but also influenced by an earlier version of @Ren.W's [solution](https://discuss.leetcode.com/topic/63741/c-o-1-0ms-ac-solution]). We ended up with quite similiar code, I guess there's not much room for creativity once you decide on the data types to hold the data.
```
class AllOne {
public:

    void inc(string key) {
        
        // If the key doesn't exist, insert it with value 0.
        if (!bucketOfKey.count(key))
            bucketOfKey[key] = buckets.insert(buckets.begin(), {0, {key}});
            
        // Insert the key in next bucket and update the lookup.
        auto next = bucketOfKey[key], bucket = next++;
        if (next == buckets.end() || next->value > bucket->value + 1)
            next = buckets.insert(next, {bucket->value + 1, {}});
        next->keys.insert(key);
        bucketOfKey[key] = next;
        
        // Remove the key from its old bucket.
        bucket->keys.erase(key);
        if (bucket->keys.empty())
            buckets.erase(bucket);
    }

    void dec(string key) {

        // If the key doesn't exist, just leave.
        if (!bucketOfKey.count(key))
            return;

        // Maybe insert the key in previous bucket and update the lookup.
        auto prev = bucketOfKey[key], bucket = prev--;
        bucketOfKey.erase(key);
        if (bucket->value > 1) {
            if (bucket == buckets.begin() || prev->value < bucket->value - 1)
                prev = buckets.insert(bucket, {bucket->value - 1, {}});
            prev->keys.insert(key);
            bucketOfKey[key] = prev;
        }
        
        // Remove the key from its old bucket.
        bucket->keys.erase(key);
        if (bucket->keys.empty())
            buckets.erase(bucket);
    }

    string getMaxKey() {
        return buckets.empty() ? "" : *(buckets.rbegin()->keys.begin());
    }
    
    string getMinKey() {
        return buckets.empty() ? "" : *(buckets.begin()->keys.begin());
    }

private:
    struct Bucket { int value; unordered_set<string> keys; };
    list<Bucket> buckets;
    unordered_map<string, list<Bucket>::iterator> bucketOfKey;
};
```
</p>


