---
title: "Lowest Common Ancestor of a Binary Tree"
weight: 220
#id: "lowest-common-ancestor-of-a-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.</p>

<p>According to the <a href="https://en.wikipedia.org/wiki/Lowest_common_ancestor" target="_blank">definition of LCA on Wikipedia</a>: &ldquo;The lowest common ancestor is defined between two nodes p&nbsp;and q&nbsp;as the lowest node in T that has both p&nbsp;and q&nbsp;as descendants (where we allow <b>a node to be a descendant of itself</b>).&rdquo;</p>

<p>Given the following binary tree:&nbsp; root =&nbsp;[3,5,1,6,2,0,8,null,null,7,4]</p>
<img alt="" src="https://assets.leetcode.com/uploads/2018/12/14/binarytree.png" style="width: 200px; height: 190px;" />
<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1
<strong>Output:</strong> 3
<strong>Explanation: </strong>The LCA of nodes <code>5</code> and <code>1</code> is <code>3.</code>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 4
<strong>Output:</strong> 5
<strong>Explanation: </strong>The LCA of nodes <code>5</code> and <code>4</code> is <code>5</code>, since a node can be a descendant of itself according to the LCA definition.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li>All of the nodes&#39; values will be unique.</li>
	<li>p and q are different and both values will&nbsp;exist in the binary tree.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Facebook - 22 (taggedByAdmin: true)
- Amazon - 17 (taggedByAdmin: true)
- Google - 5 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: true)
- ByteDance - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Zillow - 6 (taggedByAdmin: false)
- Apple - 5 (taggedByAdmin: true)
- Oracle - 4 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Airbnb - 2 (taggedByAdmin: false)
- Visa - 3 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Intuit - 3 (taggedByAdmin: false)
- Salesforce - 3 (taggedByAdmin: false)
- Splunk - 3 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

First the given nodes `p` and `q` are to be searched in a binary tree and then their lowest common ancestor is to be found. We can resort to a normal tree traversal to search for the two nodes. Once we reach the desired nodes `p` and `q`, we can backtrack and find the lowest common ancestor.

<center>
<img src="../Figures/236/236_LCA_Binary_1.png" width="600"/>
</center>

#### Approach 1: Recursive Approach

**Intuition**

The approach is pretty intuitive. Traverse the tree in a depth first manner. The moment you encounter either of the nodes `p` or `q`, return some boolean flag. The flag helps to determine if we found the required nodes in any of the paths. The least common ancestor would then be the node for which both the subtree recursions return a `True` flag. It can also be the node which itself is one of `p` or `q` and for which one of the subtree recursions returns a `True` flag.

Let us look at the formal algorithm based on this idea.

**Algorithm**

1. Start traversing the tree from the root node.
2. If the current node itself is one of `p` or `q`, we would mark a variable `mid` as `True` and continue the search for the other node in the left and right branches.
3. If either of the left or the right branch returns `True`, this means one of the two nodes was found below.
4. If at any point in the traversal, any two of the three flags `left`, `right` or `mid` become `True`, this means we have found the lowest common ancestor for the nodes `p` and `q`.

Let us look at a sample tree and we search for the lowest common ancestor of two nodes `9` and `11` in the tree.

<center>

!?!../Documents/236_LCA_Binary_Tree_1.json:770,460!?!

</center>

Following is the sequence of nodes that are followed in the recursion:

<pre>
1 --> 2 --> 4 --> 8
BACKTRACK 8 --> 4
4 --> 9 (ONE NODE FOUND, return True)
BACKTRACK 9 --> 4 --> 2
2 --> 5 --> 10
BACKTRACK 10 --> 5
5 --> 11 (ANOTHER NODE FOUND, return True)
BACKTRACK 11 --> 5 --> 2

2 is the node where we have left = True and right = True and hence it is the lowest common ancestor.
</pre>


<iframe src="https://leetcode.com/playground/SaUmkb92/shared" frameBorder="0" width="100%" height="500" name="SaUmkb92"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of nodes in the binary tree. In the worst case we might be visiting all the nodes of the binary tree.

* Space Complexity: $$O(N)$$. This is because the maximum amount of space utilized by the recursion stack would be $$N$$ since the height of a skewed binary tree could be $$N$$.
<br/>
<br/>

---

#### Approach 2: Iterative using parent pointers

**Intuition**

If we have parent pointers for each node we can traverse back from `p` and `q` to get their ancestors. The first common node we get during this traversal would be the LCA node. We can save the parent pointers in a dictionary as we traverse the tree.

**Algorithm**

1. Start from the root node and traverse the tree.
2. Until we find `p` and `q` both, keep storing the parent pointers in a dictionary.
3. Once we have found both `p` and `q`, we get all the ancestors for `p` using the parent dictionary and add to a set called `ancestors`.
4. Similarly, we traverse through ancestors for node `q`. If the ancestor is present in the ancestors set for `p`, this means this is the first ancestor common between `p` and `q` (while traversing upwards) and hence this is the LCA node.

<iframe src="https://leetcode.com/playground/uh78aTxV/shared" frameBorder="0" width="100%" height="500" name="uh78aTxV"></iframe>

**Complexity Analysis**

* Time Complexity : $$O(N)$$, where $$N$$ is the number of nodes in the binary tree. In the worst case we might be visiting all the nodes of the binary tree.

* Space Complexity : $$O(N)$$. In the worst case space utilized by the stack, the parent pointer dictionary and the ancestor set, would be $$N$$ each, since the height of a skewed binary tree could be $$N$$.
<br>
<br>

---

#### Approach 3: Iterative without parent pointers

**Intuition**

In the previous approach, we come across the LCA during the backtracking process. We can get rid of the backtracking process itself. In this approach we always have a pointer to the probable LCA and the moment we find both the nodes we return the pointer as the answer.

**Algorithm**

1. Start with root node.
2. Put the `(root, root_state)` on to the stack. `root_state` defines whether one of the children or both children of `root` are left for traversal.
3. While the stack is not empty, peek into the top element of the stack represented as `(parent_node, parent_state)`.
4. Before traversing any of the child nodes of `parent_node` we check if the `parent_node` itself is one of `p` or `q`.
5. First time we find either of `p` or `q`, set a boolean flag called `one_node_found` to `True`. Also start keeping track of the lowest common ancestors by keeping a note of the top index of the stack in the variable `LCA_index`. Since all the current elements of the stack are ancestors of the node we just found.
6. The second time `parent_node == p or parent_node == q` it means we have found both the nodes and we can return the `LCA node`.
7. Whenever we visit a child of a `parent_node` we push the `(parent_node, updated_parent_state)` onto the stack. We update the state of the parent since a child/branch has been visited/processed and accordingly the state changes.
8. A node finally gets popped off from the stack when the state becomes `BOTH_DONE` implying both left and right subtrees have been pushed onto the stack and processed. If `one_node_found` is `True` then we need to check if the top node being popped could be one of the ancestors of the found node. In that case we need to reduce `LCA_index` by one. Since one of the ancestors was popped off.

> Whenever both `p` and `q` are found, `LCA_index` would be pointing to an index in the stack which would contain all the common ancestors between `p` and `q`. And the `LCA_index` element has the `lowest` ancestor common between p and q.

<center>

!?!../Documents/236_LCA_Binary_Tree_2.json:770,460!?!

</center>

The animation above shows how a stack is used to traverse the binary tree and keep track of the common ancestors between nodes `p` and `q`.

<iframe src="https://leetcode.com/playground/j6C3VpYf/shared" frameBorder="0" width="100%" height="500" name="j6C3VpYf"></iframe>

**Complexity Analysis**

* Time Complexity : $$O(N)$$, where $$N$$ is the number of nodes in the binary tree. In the worst case we might be visiting all the nodes of the binary tree. The advantage of this approach is that we can prune backtracking. We simply return once both the nodes are found.

* Space Complexity : $$O(N)$$. In the worst case the space utilized by stack would be $$N$$ since the height of a skewed binary tree could be $$N$$.

<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 4 lines C++/Java/Python/Ruby
- Author: StefanPochmann
- Creation Date: Mon Jul 13 2015 01:51:01 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 09 2020 15:25:49 GMT+0800 (Singapore Standard Time)

<p>
Same solution in several languages. It\'s recursive and expands the meaning of the function. If the current (sub)tree contains both p and q, then the function result is their LCA. If only one of them is in that subtree, then the result is that one of them. If neither are in that subtree, the result is null/None/nil.

Update: I also wrote [two iterative solutions](https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/discuss/65245/Iterative-Solutions-in-PythonC%2B%2B) now, one of them being a version of the solution here. They\'re more complicated than this simple recursive solution, but I do find them interesting.

---

**C++**

    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        if (!root || root == p || root == q) return root;
        TreeNode* left = lowestCommonAncestor(root->left, p, q);
        TreeNode* right = lowestCommonAncestor(root->right, p, q);
        return !left ? right : !right ? left : root;
    }

---

**Python**

    def lowestCommonAncestor(self, root, p, q):
        if root in (None, p, q): return root
        left, right = (self.lowestCommonAncestor(kid, p, q)
                       for kid in (root.left, root.right))
        return root if left and right else left or right

Or using that `None` is considered smaller than any node:

    def lowestCommonAncestor(self, root, p, q):
        if root in (None, p, q): return root
        subs = [self.lowestCommonAncestor(kid, p, q)
                for kid in (root.left, root.right)]
        return root if all(subs) else max(subs)

---

**Ruby**

    def lowest_common_ancestor(root, p, q)
        return root if [nil, p, q].index root
        left = lowest_common_ancestor(root.left, p, q)
        right = lowest_common_ancestor(root.right, p, q)
        left && right ? root : left || right
    end

---

**Java**

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || root == p || root == q) return root;
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);
        return left == null ? right : right == null ? left : root;
    }
</p>


### My Java Solution which is easy to understand
- Author: yuhangjiang
- Creation Date: Mon Jul 13 2015 02:21:16 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 14:26:04 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
            if(root == null || root == p || root == q)  return root;
            TreeNode left = lowestCommonAncestor(root.left, p, q);
            TreeNode right = lowestCommonAncestor(root.right, p, q);
            if(left != null && right != null)   return root;
            return left != null ? left : right;
        }
    }
</p>


### Java/Python iterative solution
- Author: dietpepsi
- Creation Date: Mon Oct 19 2015 11:48:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 11:24:43 GMT+0800 (Singapore Standard Time)

<p>
**Python**

    def lowestCommonAncestor(self, root, p, q):
        stack = [root]
        parent = {root: None}
        while p not in parent or q not in parent:
            node = stack.pop()
            if node.left:
                parent[node.left] = node
                stack.append(node.left)
            if node.right:
                parent[node.right] = node
                stack.append(node.right)
        ancestors = set()
        while p:
            ancestors.add(p)
            p = parent[p]
        while q not in ancestors:
            q = parent[q]
        return q

    # 31 / 31 test cases passed.
    # Status: Accepted
    # Runtime: 108 ms
    # 99.14%


**Java**

    public class Solution {
        public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
            Map<TreeNode, TreeNode> parent = new HashMap<>();
            Deque<TreeNode> stack = new ArrayDeque<>();
            parent.put(root, null);
            stack.push(root);
    
            while (!parent.containsKey(p) || !parent.containsKey(q)) {
                TreeNode node = stack.pop();
                if (node.left != null) {
                    parent.put(node.left, node);
                    stack.push(node.left);
                }
                if (node.right != null) {
                    parent.put(node.right, node);
                    stack.push(node.right);
                }
            }
            Set<TreeNode> ancestors = new HashSet<>();
            while (p != null) {
                ancestors.add(p);
                p = parent.get(p);
            }
            while (!ancestors.contains(q))
                q = parent.get(q);
            return q;
        }
    }

To find the lowest common ancestor, we need to find where is `p` and `q` and a way to track their ancestors. A `parent` pointer for each node found is good for the job. After we found both `p` and `q`, we create a set of `p`'s `ancestors`. Then we travel through `q`'s `ancestors`, the first one appears in `p`'s is our answer.
</p>


