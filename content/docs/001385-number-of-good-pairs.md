---
title: "Number of Good Pairs"
weight: 1385
#id: "number-of-good-pairs"
---
## Description
<div class="description">
<p>Given an array of integers&nbsp;<code>nums</code>.</p>

<p>A pair&nbsp;<code>(i,j)</code>&nbsp;is called <em>good</em> if&nbsp;<code>nums[i]</code> == <code>nums[j]</code> and <code>i</code> &lt; <code>j</code>.</p>

<p>Return the number of <em>good</em> pairs.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,1,1,3]
<strong>Output:</strong> 4
<strong>Explanation: </strong>There are 4 good pairs (0,3), (0,4), (3,4), (2,5) 0-indexed.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,1,1,1]
<strong>Output:</strong> 6
<strong>Explanation: </strong>Each pair in the array are <em>good</em>.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 100</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 100</code></li>
</ul>
</div>

## Tags
- Array (array)
- Hash Table (hash-table)
- Math (math)

## Companies
- Activevideo - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Count
- Author: lee215
- Creation Date: Sun Jul 12 2020 12:01:37 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 13 2020 23:35:24 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
`count` the occurrence of the same elements.
For each new element `a`,
there will be more `count[a]` pairs,
with `A[i] == A[j]` and `i < j`
<br>

## **Complexity**
Time `O(N)`
Space `O(N)`
<br>

**Java:**
```java
    public int numIdenticalPairs(int[] A) {
        int res = 0, count[] = new int[101];
        for (int a: A) {
            res += count[a]++;
        }
        return res;
    }
```

**C++:**
```cpp
    int numIdenticalPairs(vector<int>& A) {
        int res = 0;
        unordered_map<int, int> count;
        for (int a: A) {
            res += count[a]++;
        }
        return res;
    }
```
**C++**
1-line from @generationx2020
```cpp
    int numIdenticalPairs(vector<int>& A) {
        return accumulate(A.begin(), A.end(), 0, [count = unordered_map<int, int> {}] (auto a, auto b) mutable {
            return a + count[b]++;
        });
    }
```
**Python:**
```py
    def numIdenticalPairs(self, A):
        return sum(k * (k - 1) / 2 for k in collections.Counter(A).values())
```

</p>


### [Python] Simple O(n) Solution
- Author: Eellaup
- Creation Date: Wed Jul 15 2020 07:29:16 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 17 2020 02:40:48 GMT+0800 (Singapore Standard Time)

<p>
**Solution Idea**
The idea is storing the number of repeated elements in a dictionary/hash table and using mathmatics to calculate the number of combinations.

**Fundamental Math Concept (Combinations)**
The "# of pairs" can be calculated by summing each value from range 0 to n-1, where n is the "# of times repeated". So the "# of pairs" for the 5 repeated values, would be 0+1+2+3+4 = 10.

Another way to think about it is:

Notice in the table below how the number of pairs increments by adding the previous "# of times repeated" to the previous "# of pairs."

For example: to get the "# of pairs" for 3 repeated values, you would add the previous "# of times repeated" (which is 2) with the previous "# of pairs" (which is 1). Therefore, the "# of pairs for 3 repeated values is 2+1=3. In this method, you don\'t peform the same computations multiple times.

Example Table of # of repeated items with their corresponding # of pairs
<table>
	<tr>
    <th># of times repeated</th>
    <th># of pairs</th>
  </tr>
  <tr>
    <td>2</td>
    <td>1</td>
  </tr>
  <tr>
    <td>3</td>
    <td>3</td>
  </tr>
  <tr>
    <td>4</td>
    <td>6</td>
  </tr>
  <tr>
    <td>5</td>
    <td>10</td>
  </tr>
  <tr>
    <td>6</td>
    <td>15</td>
  </tr>
</table>

**My Code Solution**
1. Dictionary/Hash Table to store the number of times an element is repeated
2. Record total number of pairs (num)
3. Iterate through the nums list
4. Check to see if each element has already been seen, if not add it to the Hash Table
5. If it has been seen, but only once, just add 1 to "num"
6. If it has been seen, multiple times. Add the number of repeated times to "num"
7. Increment the number of reapeated times by 1.
7. Move onto next element

```
class Solution:
    
    # search for duplicate numbers
    def numIdenticalPairs(self, nums: List[int]) -> int:
        
        # number of good pairs
        repeat = {}
        num = 0
        
        # for every element in nums
        for v in nums:
            
            # number of repeated digits
            if v in repeat:
                
                # count number of pairs based on duplicate values
                if repeat[v] == 1:
                    num += 1
                else:
                    num += repeat[v]
                
                # increment the number of counts
                repeat[v] += 1
            # number has not been seen before
            else:
                repeat[v] = 1
        # return
        return num
```
</p>


### 3-Solution O(n) with Map, O(nlogn) Sorting and O(n^2) | Detailed Explanation
- Author: karprabin1
- Creation Date: Sun Jul 26 2020 23:42:19 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 27 2020 15:02:59 GMT+0800 (Singapore Standard Time)

<p>
**Detailed Video Explanation** https://youtu.be/Qn_hJwwa-28?t=0

Task: Count Number of Good Pairs.
A pair (i,j) is called good if nums[i] == nums[j] and i < j.

**Solution1:** Brute Force and First comes in mind just loop two nest loop one for i from 0 to n - 1 and another for j from i + 1 to n and check if nums[i] == nums[j] => count++
In the end return count.

`TC - O(n^2)`
`SC - O(1)`

Can it optimize???

**Solution2:** Optimize using HashMap.
Whever we are at perticular j we just need to know how many times nums[j] occured before index j? and that many good pairs we can form with combining it with j correct.

Just pass through the element and check in the map if it contians get the value increment the count and update value by 1 in map.
If not yet in map just add into map with value 1.

`TC - O(n)`
`SC - O(n)`


**Solution3:** Sorting

`TC - O(nlogn)`
`SC - O(1)`


********
**Solution1: Code**
```
class Solution {
    public int numIdenticalPairs(int[] nums) {
        int count = 0;
        for(int i = 0; i < nums.length - 1; i++)
            for(int j = i + 1; j < nums.length; j++)
                if(nums[i] == nums[j])
                    count++;
        return count;
    }
}
```
********

**Solution2: Code**

```
class Solution {
    public int numIdenticalPairs(int[] nums) {
        Map<Integer, Integer> map = new HashMap();
        int count = 0;
        for (int num : nums) {
            count += map.getOrDefault(num, 0);
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        return count;
    }
}
```

********
**Solution3: Code**
```
class Solution {
    public int numIdenticalPairs(int[] nums) {
        Arrays.sort(nums);
        int count = 0;
        int i = 0;
        for (int j = 1; j < nums.length; j++) {
            if (nums[j] == nums[i])
                count += j - i;
            else i = j;
        }
        return count;
    }
}
```

********


</p>


