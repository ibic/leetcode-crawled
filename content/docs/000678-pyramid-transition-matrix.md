---
title: "Pyramid Transition Matrix"
weight: 678
#id: "pyramid-transition-matrix"
---
## Description
<div class="description">
<p>We are stacking blocks to form a pyramid. Each block has a color which is a one letter string.</p>

<p>We are allowed to place any color block <code>C</code> on top of two adjacent blocks of colors <code>A</code> and <code>B</code>, if and only if <code>ABC</code> is an allowed triple.</p>

<p>We start with a bottom row of <code>bottom</code>, represented as a single string. We also start with a list of allowed triples <code>allowed</code>. Each allowed triple is represented as a string of length 3.</p>

<p>Return true if we can build the pyramid all the way to the top, otherwise false.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> bottom = &quot;BCD&quot;, allowed = [&quot;BCG&quot;, &quot;CDE&quot;, &quot;GEA&quot;, &quot;FFF&quot;]
<b>Output:</b> true
<b>Explanation:</b>
We can stack the pyramid like this:
    A
   / \
  G   E
 / \ / \
B   C   D

We are allowed to place G on top of B and C because BCG is an allowed triple.  Similarly, we can place E on top of C and D, then A on top of G and E.</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> bottom = &quot;AABA&quot;, allowed = [&quot;AAA&quot;, &quot;AAB&quot;, &quot;ABA&quot;, &quot;ABB&quot;, &quot;BAC&quot;]
<b>Output:</b> false
<b>Explanation:</b>
We can&#39;t stack the pyramid to the top.
Note that there could be allowed triples (A, B, C) and (A, B, D) with C != D.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>bottom</code> will be a string with length in range <code>[2, 8]</code>.</li>
	<li><code>allowed</code> will have length in range <code>[0, 200]</code>.</li>
	<li>Letters in all strings will be chosen from the set <code>{&#39;A&#39;, &#39;B&#39;, &#39;C&#39;, &#39;D&#39;, &#39;E&#39;, &#39;F&#39;, &#39;G&#39;}</code>.</li>
</ul>

</div>

## Tags
- Bit Manipulation (bit-manipulation)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Airbnb - 2 (taggedByAdmin: true)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


#### Approach #1: State to State Transition [Wrong Answer]

**Intuition and Algorithm**

We model the states that blocks can be in.  Each state is a binary number where the `k`th bit is set if the `k`th type of block is a possibility.  Then, we create a transition map `T[state1][state2] -> state` that takes a left state and a right state and outputs all possible parent states.

At the end, applying these transitions is straightforward.  However, this approach is not correct, because the transitions are not independent.  If for example we have states in a row `A, {B or C}, A`, and allowed triples `(A, B, D)`, `(C, A, D)`, then regardless of the choice of `{B or C}` we cannot create the next row of the pyramid.

<iframe src="https://leetcode.com/playground/hc4Adora/shared" frameBorder="0" width="100%" height="429" name="hc4Adora"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(2^{2\mathcal{A}}A + N^2)$$, where $$N$$ is the length of `bottom`, $$A$$ is the length of `allowed`, and $$\mathcal{A}$$ is the size of the alphabet.

* Space Complexity: $$O(2^{2\mathcal{A}})$$ in additional space complexity.

---
#### Approach #2: Depth-First Search [Accepted]

**Intuition**

We exhaustively try every combination of blocks.

**Algorithm**

We can work in either strings or integers, but we need to create a transition map `T` from the list of allowed triples.  This map `T[x][y] = {set of z}` will be all possible parent blocks for a left child of `x` and a right child of `y`.  When we work in strings, we use `Set`, and when we work in integers, we will use the set bits of the result integer.

Afterwards, to `solve` a row, we generate every possible combination of the next row and solve them.  If any of those new rows are solvable, we return `True`, otherwise `False`.

We can also cache intermediate results, saving us time.  This is illustrated in the comments for Python.  For Java, all caching is done with lines of code that mention the integer `R`.

<iframe src="https://leetcode.com/playground/E5i9tMom/shared" frameBorder="0" width="100%" height="500" name="E5i9tMom"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(\mathcal{A}^{N})$$, where $$N$$ is the length of `bottom`, and $$\mathcal{A}$$ is the size of the alphabet, and assuming we cache intermediate results.  We might try every sequence of letters for each row.  [The total complexity is because $$O(\sum_{k}^n \mathcal{A}^{k})$$ is a geometric series equal to $$O(\frac{\mathcal{A^{n+1}}-1}{\mathcal{A}-1})$$.]  Without intermediate caching, this would be $$O(\mathcal{A}^{N^2})$$.

* Space Complexity: $$O(N^2)$$ additional space complexity.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution - map + backtracking
- Author: AdaZhang
- Creation Date: Fri Jan 05 2018 11:23:14 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 08:09:07 GMT+0800 (Singapore Standard Time)

<p>
A little long, but very straightforward.
```
class Solution {
    public boolean pyramidTransition(String bottom, List<String> allowed) {
        Map<String, List<String>> map = new HashMap<>();
        for (String s : allowed) {
            String key = s.substring(0,2);
            if (!map.containsKey(key)) map.put(key, new ArrayList<String>());
            map.get(key).add(s.substring(2));
        }
        
        return helper(bottom, map);
    }
    
    private boolean helper(String bottom, Map<String, List<String>> map) {
        if(bottom.length() == 1) return true;
        for (int i = 0; i<bottom.length()-1; i++) {
            if (!map.containsKey(bottom.substring(i,i+2))) return false;
        }
        List<String> ls = new ArrayList<>();
        getList(bottom, 0, new StringBuilder(), ls, map);
        for (String s : ls) {
            if (helper(s, map)) return true;
        }
        return false;
    }
    
    private void getList(String bottom, int idx, StringBuilder sb, List<String> ls, Map<String, List<String>> map) {
        if (idx == bottom.length() - 1) {
            ls.add(sb.toString());
            return;
        }
        for (String s : map.get(bottom.substring(idx, idx+2))) {
            sb.append(s);
            getList(bottom, idx + 1, sb, ls, map);
            sb.deleteCharAt(sb.length()-1);
        }
    }
}
```
</p>


### concise C++ recursive solution (10 line)
- Author: wxd_sjtu
- Creation Date: Sun Sep 16 2018 14:40:07 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 24 2018 10:20:26 GMT+0800 (Singapore Standard Time)

<p>
10 line C++ recursive solution
```C++
class Solution {
public:
  bool pyramidTransition(string bottom, vector<string>& allowed) {
    unordered_map<string, vector<char>> m;
    for(auto& s:allowed) m[s.substr(0, 2)].push_back(s.back());
    return helper(bottom, m, 0, "");
  }
  bool helper(string bottom, unordered_map<string, vector<char>>& m, int start, string next){
    if(bottom.size() == 1) return true;
    if(start == (int)bottom.size() - 1) return helper(next, m, 0, "");
    for(char c : m[bottom.substr(start, 2)])
      if(helper(bottom, m, start+1, next+c)) return true;
    return false;
  }
};
```
</p>


### Python Solution
- Author: lee215
- Creation Date: Wed Jan 03 2018 05:53:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 08 2018 23:28:55 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):

    def pyramidTransition(self, bottom, allowed):
        f = collections.defaultdict(lambda: defaultdict(list))
        for a, b, c in allowed: f[a][b].append(c)

        def pyramid(bottom):
            if len(bottom) == 1: return True
            for i in itertools.product(*(f[a][b] for a, b in zip(bottom, bottom[1:]))):
                if pyramid(i): return True
            return False
        return pyramid(bottom)
```
Make ``pyramid`` into one line if you like:
```
def pyramid(bottom):
            return len(bottom) == 1 or any(pyramid(i) for i in product(*(f[a][b] for a, b in zip(bottom, bottom[1:]))))
```
</p>


