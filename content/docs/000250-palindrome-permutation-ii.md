---
title: "Palindrome Permutation II"
weight: 250
#id: "palindrome-permutation-ii"
---
## Description
<div class="description">
<p>Given a string <code>s</code>, return all the palindromic permutations (without duplicates) of it. Return an empty list if no palindromic permutation could be form.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> <code>&quot;aabb&quot;</code>
<strong>Output:</strong> <code>[&quot;abba&quot;, &quot;baab&quot;]</code></pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> <code>&quot;abc&quot;</code>
<strong>Output:</strong> <code>[]</code></pre>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Uber - 8 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

The simplest solution is generate every possible permutation of the given string $$s$$ and check if the generated permutation is a palindrome. After this, the palindromic permuations can be added to a $$set$$ in order to eliminate the duplicates. At the end, we can return an array comprised of the elements of this $$set$$ as the resultant array.

Let's look at the way these permutations are generated. We make use of a recursive function `permute` which  takes the index of the current element $$current_index$$ as one of the arguments. Then, it swaps the current element with every other element in the array, lying towards its right, so as to generate a new ordering of the array elements. After the swapping has been done, it makes another call to `permute` but this time with the index of the next element in the array. While returning back, we reverse the swapping done in the current function call. Thus, when we reach the end of the array, a new ordering of the array's elements is generated.

The animation below depicts the ways the permutations are generated.

!?!../Documents/561_Array.json:1000,563!?!

<iframe src="https://leetcode.com/playground/6Ryph5jp/shared" frameBorder="0" name="6Ryph5jp" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O((n+1)!)$$. A total of $$n!$$ permutations are possible. For every permutation generated, we need to check if it is a palindrome, each of which requires $$O(n)$$ time.

* Space complexity : $$O(n)$$. The depth of the recursion tree can go upto $$n$$.

---
#### Approach #2 Backtracking [Accepted]

**Algorithm**

It might be possible that no palindromic permutation could be possible for the given string $$s$$. Thus, it is useless to generate the permutations in such a case. Taking this idea, firstly we check if a palindromic permutation is possible for $$s$$. If yes, then only we proceed further with generating the permutations. To check this, we make use of a hashmap $$map$$ which stores the number of occurences of each character(out of 128 ASCII characters possible). If the number of characters with odd number of occurences exceeds 1, it indicates that no palindromic permutation is possible for $$s$$. To look at this checking process in detail, look at Approach 4 of the [article](https://leetcode.com/articles/palindrome-permutation) for Palindrome Permutation.

Once we are sure that a palindromic permutation is possible for $$s$$, we go for the generation of the required permutations. But, instead of wildly generating all the permutations, we can include some smartness in the generation of permutations i.e. we can generate only those permutations which are already palindromes.

One idea to to do so is to generate only the first half of the palindromic string and to append its reverse string to itself to generate the full length palindromic string.

Based on this idea, by making use of the number of occurences of the characters in $$s$$ stored in $$map$$, we create a string $$st$$  which contains all the characters of $$s$$ but with the number of occurences of these characters in $$st$$ reduced to half their original number of occurences in $$s$$.

Thus, now we can generate all the permutations of this string $$st$$ and append the reverse of this permuted string to itself to create the palindromic permutations of $$s$$.

In case of a string $$s$$ with odd length, whose palindromic permutations are possible, one of the characters in $$s$$ must be occuring an odd number of times. We keep a track of this character, $$ch$$, and it is kept separte from the string $$st$$. We again generate the permutations for $$st$$ similarly and append the reverse of the generated permutation to itself, but we also place the character $$ch$$ at the middle of the generated string. 

In this way, only the required palindromic permutations will be generated. Even if we go with the above idea, a lot of duplicate strings will be generated.

In order to avoid generating duplicate palindromic permutations in the first place itself, as much as possible, we can make use of this idea. As discussed in the last approach, we swap the current element with all the elements lying towards its right to generate the permutations. Before swapping, we can check if the elements being swapped are equal. If so, the permutations generated even after swapping the two will be duplicates(redundant). Thus, we need not proceed further in such a case.

See this animation for a clearer understanding.

!?!../Documents/267_Palindrome_Permutation_II.json:1000,563!?!

<iframe src="https://leetcode.com/playground/Nfi5WWm2/shared" frameBorder="0" name="Nfi5WWm2" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big((\frac{n}{2}+1)!\big)$$. Atmost $$\frac{n}{2}!$$ permutations need to be generated in the worst case. Further, for each permutation generated, `string.reverse()` function will take $$n/4$$ time.

* Space complexity : $$O(n)$$. The depth of recursion tree can go upto $$n/2$$ in the worst case.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Backtrack Summary: General Solution for 10 Questions!!!!!!!! Python (Combination Sum, Subsets, Permutation,  Palindrome)
- Author: dichen001
- Creation Date: Sat Dec 24 2016 13:16:12 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 01:25:07 GMT+0800 (Singapore Standard Time)

<p>
For Java version, please refer to [isssac3's answer.](https://discuss.leetcode.com/topic/46162/a-general-approach-to-backtracking-questions-in-java-subsets-permutations-combination-sum-palindrome-partioning)

If you find it helpful, please vote to let more people see this post. Besides, it would be great if you find other questions could be solved use this general solution. Please make a comment below.

**39. Combination Sum**
https://leetcode.com/problems/combination-sum/
```
    def combinationSum(self, candidates, target):
        def backtrack(tmp, start, end, target):
            if target == 0:
                ans.append(tmp[:])
            elif target > 0:
                for i in range(start, end):
                    tmp.append(candidates[i])
                    backtrack(tmp, i, end, target - candidates[i])
                    tmp.pop()
        ans = [] 
        candidates.sort(reverse= True)
        backtrack([], 0, len(candidates), target)
        return ans
```

**40. Combination Sum II**
https://leetcode.com/problems/combination-sum-ii/
```
    def combinationSum2(self, candidates, target):
        def backtrack(start, end, tmp, target):
            if target == 0:
                ans.append(tmp[:])
            elif target > 0:
                for i in range(start, end):
                    if i > start and candidates[i] == candidates[i-1]:
                        continue
                    tmp.append(candidates[i])
                    backtrack(i+1, end, tmp, target - candidates[i])
                    tmp.pop()
        ans = []
        candidates.sort(reverse= True)
        backtrack(0, len(candidates), [], target)
        return ans
```

**78. Subsets**
https://leetcode.com/problems/subsets/
```
    def subsets(self, nums):
        def backtrack(start, end, tmp):
            ans.append(tmp[:])
            for i in range(start, end):
                tmp.append(nums[i])
                backtrack(i+1, end, tmp)
                tmp.pop()
        ans = []
        backtrack(0, len(nums), [])
        return ans
```

**90. Subsets II**
https://leetcode.com/problems/subsets-ii/
```
    def subsetsWithDup(self, nums):
        def backtrack(start, end, tmp):
            ans.append(tmp[:])
            for i in range(start, end):
                if i > start and nums[i] == nums[i-1]:
                    continue
                tmp.append(nums[i])
                backtrack(i+1, end, tmp)
                tmp.pop()
        ans = []
        nums.sort()
        backtrack(0, len(nums), [])
        return ans
```

**46. Permutations**
https://leetcode.com/problems/permutations/
```
    def permute(self, nums):
        def backtrack(start, end):
            if start == end:
                ans.append(nums[:])
            for i in range(start, end):
                nums[start], nums[i] = nums[i], nums[start]
                backtrack(start+1, end)
                nums[start], nums[i] = nums[i], nums[start]
                
        ans = []
        backtrack(0, len(nums))
        return ans
```

**47. Permutations II**
https://leetcode.com/problems/permutations-ii/
```
    def permuteUnique(self, nums):
        def backtrack(tmp, size):
            if len(tmp) == size:
                ans.append(tmp[:])
            else:
                for i in range(size):
                    if visited[i] or (i > 0 and nums[i-1] == nums[i] and not visited[i-1]):
                        continue
                    visited[i] = True
                    tmp.append(nums[i])
                    backtrack(tmp, size)
                    tmp.pop()
                    visited[i] = False
        ans = []
        visited = [False] * len(nums)
        nums.sort()
        backtrack([], len(nums))
        return ans
```

**60. Permutation Sequence**
https://leetcode.com/problems/permutation-sequence/
```
    def getPermutation(self, n, k):
        nums = [str(i) for i in range(1, n+1)]
        fact = [1] * n
        for i in range(1,n):
            fact[i] = i*fact[i-1]
        k -= 1
        ans = []
        for i in range(n, 0, -1):
            id = k / fact[i-1]
            k %= fact[i-1]
            ans.append(nums[id])
            nums.pop(id)
        return ''.join(ans)
```

**131. Palindrome Partitioning**
https://leetcode.com/problems/palindrome-partitioning/
```
    def partition(self, s):
        def backtrack(start, end, tmp):
            if start == end:
                ans.append(tmp[:])
            for i in range(start, end):
                cur = s[start:i+1]
                if cur == cur[::-1]:
                    tmp.append(cur)
                    backtrack(i+1, end, tmp)
                    tmp.pop()
        ans = []
        backtrack(0, len(s), [])
        return ans
```

****


**267. Palindrome Permutation II**
https://leetcode.com/problems/palindrome-permutation-ii/
Related to this two:
`31. Next Permutation`: https://leetcode.com/problems/next-permutation/
`266. Palindrome Permutation`: https://leetcode.com/problems/palindrome-permutation/

```
    def generatePalindromes(self, s):
        kv = collections.Counter(s)
        mid = [k for k, v in kv.iteritems() if v%2]
        if len(mid) > 1:
            return []
        mid = '' if mid == [] else mid[0]
        half = ''.join([k * (v/2) for k, v in kv.iteritems()])
        half = [c for c in half]
        
        def backtrack(end, tmp):
            if len(tmp) == end:
                cur = ''.join(tmp)
                ans.append(cur + mid + cur[::-1])
            else:
                for i in range(end):
                    if visited[i] or (i>0 and half[i] == half[i-1] and not visited[i-1]):
                        continue
                    visited[i] = True
                    tmp.append(half[i])
                    backtrack(end, tmp)
                    visited[i] = False
                    tmp.pop()
                    
        ans = []
        visited = [False] * len(half)
        backtrack(len(half), [])
        return ans
```
</p>


### AC Java solution with explanation
- Author: jeantimex
- Creation Date: Sun Aug 23 2015 09:06:34 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 18:21:46 GMT+0800 (Singapore Standard Time)

<p>
Basically, the idea is to perform permutation on half of the palindromic string and then form the full palindromic result.

    public List<String> generatePalindromes(String s) {
        int odd = 0;
        String mid = "";
        List<String> res = new ArrayList<>();
        List<Character> list = new ArrayList<>();
        Map<Character, Integer> map = new HashMap<>();
    
        // step 1. build character count map and count odds
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            map.put(c, map.containsKey(c) ? map.get(c) + 1 : 1);
            odd += map.get(c) % 2 != 0 ? 1 : -1;
        }
    
        // cannot form any palindromic string
        if (odd > 1) return res;
    
        // step 2. add half count of each character to list
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            char key = entry.getKey();
            int val = entry.getValue();
    
            if (val % 2 != 0) mid += key;
    
            for (int i = 0; i < val / 2; i++) list.add(key);
        }
    
        // step 3. generate all the permutations
        getPerm(list, mid, new boolean[list.size()], new StringBuilder(), res);
    
        return res;
    }
    
    // generate all unique permutation from list
    void getPerm(List<Character> list, String mid, boolean[] used, StringBuilder sb, List<String> res) {
        if (sb.length() == list.size()) {
            // form the palindromic string
            res.add(sb.toString() + mid + sb.reverse().toString());
            sb.reverse();
            return;
        }
    
        for (int i = 0; i < list.size(); i++) {
            // avoid duplication
            if (i > 0 && list.get(i) == list.get(i - 1) && !used[i - 1]) continue;
    
            if (!used[i]) {
                used[i] = true; sb.append(list.get(i));
                // recursion
                getPerm(list, mid, used, sb, res);
                // backtracking
                used[i] = false; sb.deleteCharAt(sb.length() - 1);
            }
        }
    }
</p>


### Short backtracking solution in Java (3 ms)
- Author: lianngg
- Creation Date: Mon Oct 26 2015 13:23:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 19 2018 04:56:41 GMT+0800 (Singapore Standard Time)

<p>
We only need to generate the first part of palindrome string, and the remaining part will be a middle character with the reverse of first part.



    private List<String> list = new ArrayList<>();

    public List<String> generatePalindromes(String s) {
        int numOdds = 0; // How many characters that have odd number of count
        int[] map = new int[256]; // Map from character to its frequency
        for (char c: s.toCharArray()) {
            map[c]++;
            numOdds = (map[c] & 1) == 1 ? numOdds+1 : numOdds-1;
        }
        if (numOdds > 1)   return list;
        
        String mid = "";
        int length = 0;
        for (int i = 0; i < 256; i++) {
            if (map[i] > 0) {
                if ((map[i] & 1) == 1) { // Char with odd count will be in the middle
                    mid = "" + (char)i;
                    map[i]--;
                }
                map[i] /= 2; // Cut in half since we only generate half string
                length += map[i]; // The length of half string
            }
        }
        generatePalindromesHelper(map, length, "", mid);
        return list;
    }
    private void generatePalindromesHelper(int[] map, int length, String s, String mid) {
        if (s.length() == length) {
            StringBuilder reverse = new StringBuilder(s).reverse(); // Second half
            list.add(s + mid + reverse);
            return;
        }
        for (int i = 0; i < 256; i++) { // backtracking just like permutation
            if (map[i] > 0) {
                map[i]--;
                generatePalindromesHelper(map, length, s + (char)i, mid);
                map[i]++;
            } 
        }
    }
</p>


