---
title: "Count Different Palindromic Subsequences"
weight: 651
#id: "count-different-palindromic-subsequences"
---
## Description
<div class="description">
<p>
Given a string S, find the number of different non-empty palindromic subsequences in S, and <b>return that number modulo <code>10^9 + 7</code>.</b>
</p><p>
A subsequence of a string S is obtained by deleting 0 or more characters from S.
</p><p>
A sequence is palindromic if it is equal to the sequence reversed.
</p><p>
Two sequences <code>A_1, A_2, ...</code> and <code>B_1, B_2, ...</code> are different if there is some <code>i</code> for which <code>A_i != B_i</code>.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> 
S = 'bccb'
<b>Output:</b> 6
<b>Explanation:</b> 
The 6 different non-empty palindromic subsequences are 'b', 'c', 'bb', 'cc', 'bcb', 'bccb'.
Note that 'bcb' is counted only once, even though it occurs twice.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> 
S = 'abcdabcdabcdabcdabcdabcdabcdabcddcbadcbadcbadcbadcbadcbadcbadcba'
<b>Output:</b> 104860361
<b>Explanation:</b> 
There are 3104860382 different non-empty palindromic subsequences, which is 104860361 modulo 10^9 + 7.
</pre>
</p>

<p><b>Note:</b>
<li>The length of <code>S</code> will be in the range <code>[1, 1000]</code>.</li>
<li>Each character <code>S[i]</code> will be in the set <code>{'a', 'b', 'c', 'd'}</code>.</li>
</p>
</div>

## Tags
- String (string)
- Dynamic Programming (dynamic-programming)

## Companies
- LinkedIn - 2 (taggedByAdmin: true)
- Pure Storage - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1 Dynamic Programming (using 3D array) [Accepted]

**Intuition and Algorithm**

Let `dp[x][i][j]` be the answer for the substring `S[i...j]` where
`S[i] == S[j] == 'a'+x`. Note that since we only have 4 characters `a,
b, c, d`, thus `0 <= x < 4`. The DP formula goes as follows:

* If `S[i] != 'a'+x`, then `dp[x][i][j] = dp[x][i+1][j]`, note that
  here we leave the first character `S[i]` in the window out due to
  our definition of `dp[x][i][j]`.

* If `S[j] != 'a'+x`, then `dp[x][i][j] = dp[x][i][j-1]`, leaving the
  last character `S[j]` out.

* If `S[i] == S[j] == 'a'+x`, then `dp[x][i][j] = 2 +
  dp[0][i+1][j-1] + dp[1][i+1][j-1] + dp[2][i+1][j-1] +
  dp[3][i+1][j-1]`. When the first and last characters are the same, we
  need to count all the distinct palindromes (for each of `a,b,c,d`) within
  the sub-window `S[i+1][j-1]` plus the `2` palindromes contributed by
  the first and last characters.

Let `n` be the length of the input string `S`, The final answer would
be `dp[0][0][n-1] + dp[1][0][n-1] + dp[2][0][n-1] + dp[3][0][n-1]`
mod `1000000007`.

<iframe src="https://leetcode.com/playground/xWG6DSxA/shared" frameBorder="0" width="100%" height="500" name="xWG6DSxA"></iframe>

**Example Walkthrough**

Indeed this is a hard problem to solve and thoroughly understanding
its solution is also challenging. Maybe the best way to understand the
above approach is to walkthrough some simple examples to help build up
intuitions.

Let's first look at the strategy we used to fill the DP table and then walkthrough a concrete example to see how it works.

![DP Table Filling Strategy](../Figures/730/730_Table_Fill.svg){:width="539px"}
{:align="center"}

!?!../Documents/730_Example_Walkthrough.json:1280,720!?!

**Complexity Analysis**

* Time complexity : $$O(N^2)$$ where $$N$$ is the length of the input
  string $$S$$. It takes quadratic time to fill up the DP table.

* Space complexity : $$O(N^2)$$ where $$N$$ is the length of the input
  string $$S$$. The DP table takes quadratic space.

Note that we ignore the constant factor $$4$$ in the above analysis.


**Conclusion**

As we look back, this problem reveals a key attribute which indicates
that dynamic programming might be a good fit: `overlapping
sub-problems` as we recall the DP formula. By practicing more
problems, we can build up this kind of intuition.


*Credit*: the above solution is inspired by
[this post](https://discuss.leetcode.com/topic/111241/c-o-n-2-time-o-n-memory-with-explanation)
written by [@elastico](https://discuss.leetcode.com/user/elastico). His solution is space optimized. However, I found
that my approach is relatively easy to understand for people who found
this problem hard to approach.

---

#### Approach #2: Dynamic Programming (using 2D array) [Accepted]

**Intuition**

Almost every palindrome is going to take one of these four forms: `a_a`, `b_b`, `c_c`, or `d_d`, where `_` represents a palindrome of zero or more characters.  (The only other palindromes are `a`, `b`, `c`, `d`, and the empty string.)

Let's try to count palindromes of the form `a_a` - the other types are similar.  Evidently, we should take the first and last `a`, then count all the palindromes that can be formed in between, as this provides us strictly more possibilities for `_` to be a palindrome.  This reveals an *optimal substructure* that is ideal for *dynamic programming*.

**Algorithm**

Let `dp(i, j)` be the number of palindromes (including the palindrome `''`) in the string `T = S[i], S[i+1], ..., S[j]`.  To count palindromes in `T` of the form `a_a`, we will need to know the first and last occurrence of `'a'` in this string.  This can be done by a precomputed dp: `next[i][0]` will be the next occurrence of `'a'` in `S[i:]`, `next[i][1]` will be the next occurrence of `'b'` in `S[i:]`, and so on.  

Also, we will need to know the number of unique letters in `T` to count the single letter palindromes.  We can use the information from `next` to deduce it: if `next[i][0]` is in the interval `[i, j]`, then `'a'` occurs in `T`, and so on.

As many states `dp(i, j)` do not need to be computed, the most natural approach is a *top-down* variation of dynamic programming.

<iframe src="https://leetcode.com/playground/QQa9kjvH/shared" frameBorder="0" width="100%" height="500" name="QQa9kjvH"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$, where $$N$$ is the size of the string `S`.  Our calculation of `prv` and `nxt` happens in $$O(N)$$ time, then our evaluation of `dp` with at most $$N^2$$ states is $$O(1)$$ work per state.

* Space Complexity: $$O(N^2)$$, the size of `memo`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 96ms DP Solution with Detailed Explanation
- Author: victorzhang21503
- Creation Date: Tue Nov 21 2017 10:40:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 06:59:25 GMT+0800 (Singapore Standard Time)

<p>
I am not able to pass this question one time but struggle a lot in the basic test cases like "a", "aa", "aaa", "aba", "aabb". Those test cases help my early rough idea to be flawless. The basic idea of DP is easy to understand, I maintain DP[i][j] to record in substring from i to j(included), the number of palindrome without duplicate. Then we consider two cases of the DP equation:

when s.charAt(i) != s.charAt(j):
    dp[i][j] =  dp[i][j] = dp[i][j - 1] + dp[i + 1][j] - dp[i + 1][j - 1];

When s.charAt(i) == s.charAt(j):
    **the situation get much more complex and I fix a lot the wrong answers. I have comment the branches where which kind of test cases are considered.**

```
class Solution {
    public int countPalindromicSubsequences(String s) {
        int len = s.length();
        int[][] dp = new int[len][len];

        char[] chs = s.toCharArray();
        for(int i = 0; i < len; i++){
            dp[i][i] = 1;   // Consider the test case "a", "b" "c"...
        }

        for(int distance = 1; distance < len; distance++){
            for(int i = 0; i < len - distance; i++){
                int j = i + distance;
                if(chs[i] == chs[j]){
                    int low = i + 1;
                    int high = j - 1;

              /* Variable low and high here are used to get rid of the duplicate*/

                    while(low <= high && chs[low] != chs[j]){
                        low++;
                    }
                    while(low <= high && chs[high] != chs[j]){
                        high--;
                    }
                    if(low > high){
                        // consider the string from i to j is "a...a" "a...a"... where there is no character 'a' inside the leftmost and rightmost 'a'
                       /* eg:  "aba" while i = 0 and j = 2:  dp[1][1] = 1 records the palindrome{"b"}, 
                         the reason why dp[i + 1][j  - 1] * 2 counted is that we count dp[i + 1][j - 1] one time as {"b"}, 
                         and additional time as {"aba"}. The reason why 2 counted is that we also count {"a", "aa"}. 
                         So totally dp[i][j] record the palindrome: {"a", "b", "aa", "aba"}. 
                         */ 

                        dp[i][j] = dp[i + 1][j - 1] * 2 + 2;  
                    } 
                    else if(low == high){
                        // consider the string from i to j is "a...a...a" where there is only one character 'a' inside the leftmost and rightmost 'a'
                       /* eg:  "aaa" while i = 0 and j = 2: the dp[i + 1][j - 1] records the palindrome {"a"}.  
                         the reason why dp[i + 1][j  - 1] * 2 counted is that we count dp[i + 1][j - 1] one time as {"a"}, 
                         and additional time as {"aaa"}. the reason why 1 counted is that 
                         we also count {"aa"} that the first 'a' come from index i and the second come from index j. So totally dp[i][j] records {"a", "aa", "aaa"}
                        */
                        dp[i][j] = dp[i + 1][j - 1] * 2 + 1;  
                    }
                    else{
                        // consider the string from i to j is "a...a...a... a" where there are at least two character 'a' close to leftmost and rightmost 'a'
                       /* eg: "aacaa" while i = 0 and j = 4: the dp[i + 1][j - 1] records the palindrome {"a",  "c", "aa", "aca"}. 
                          the reason why dp[i + 1][j  - 1] * 2 counted is that we count dp[i + 1][j - 1] one time as {"a",  "c", "aa", "aca"}, 
                          and additional time as {"aaa",  "aca", "aaaa", "aacaa"}.  Now there is duplicate :  {"aca"}, 
                          which is removed by deduce dp[low + 1][high - 1]. So totally dp[i][j] record {"a",  "c", "aa", "aca", "aaa", "aaaa", "aacaa"}
                          */
                        dp[i][j] = dp[i + 1][j - 1] * 2 - dp[low + 1][high - 1]; 
                    }
                }
                else{
                    dp[i][j] = dp[i][j - 1] + dp[i + 1][j] - dp[i + 1][j - 1];  //s.charAt(i) != s.charAt(j)
                }
                dp[i][j] = dp[i][j] < 0 ? dp[i][j] + 1000000007 : dp[i][j] % 1000000007;
            }
        }

        return dp[0][len - 1];
    }
}
```
</p>


### Accepted Java Solution using memoization
- Author: kay_deep
- Creation Date: Sun Nov 19 2017 13:08:01 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 08:57:42 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    int div=1000000007;
    public int countPalindromicSubsequences(String S) {    
        TreeSet[] characters = new TreeSet[26];
        int len = S.length();
        
        for (int i = 0; i < 26; i++) characters[i] = new TreeSet<Integer>();
        
        for (int i = 0; i < len; ++i) {
            int c = S.charAt(i) - 'a';
            characters[c].add(i);
        }
        Integer[][] dp = new Integer[len+1][len+1];
         return memo(S,characters,dp, 0, len);
    }
    
    public int memo(String S,TreeSet<Integer>[] characters,Integer[][] dp,int start,int end){
        if (start >= end) return 0;
        if(dp[start][end]!=null) return dp[start][end];
       
            long ans = 0;
            
            for(int i = 0; i < 26; i++) {
                Integer new_start = characters[i].ceiling(start);
                Integer new_end = characters[i].lower(end);
              if (new_start == null || new_start >= end) continue;
                 ans++;
                if (new_start != new_end) ans++;
                ans+= memo(S,characters,dp,new_start+1,new_end);
                
            }
            dp[start][end] = (int)(ans%div);
            return dp[start][end];
    }
    
}
```
</p>


### DP C++ Clear solution explained
- Author: hamlet_fiis
- Creation Date: Wed Apr 10 2019 18:17:45 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 19 2019 01:13:18 GMT+0800 (Singapore Standard Time)

<p>
<strong>Intuition and Algorithm</strong>
<strong>Not necessarily distinct palindromic</strong>
Assume that we are interested counting, not necessarily distinct palindromic subsequences. This is dynamic programming (DP) problem.

DP(L, R), is the number of palindromic subsequences in S[L .... R]. Its formulated as following.
* 0    , if L>R
* 1    , if L=R
* DP(L + 1, R) + DP(L, R-1) - DP(L+1, R-1)   , if S[L] != S[R]
* DP(L + 1, R) + DP(L, R-1) + 1  , if S[L] = S[R]

**Why the third one?**
Check the below image, if S[L] != S[R] then DP(L,R) is represented by blue (line) + green (line) - red (line) ,the last one is because of repeated elements between blue and green lines.

![image](https://assets.leetcode.com/users/hamlet_fiis/image_1554893505.png)

**Why the fourth one?**

First assume that S[L]!= S[R], the formula is:  *DP(L + 1, R) + DP(L, R-1) - DP(L+1, R-1)*
Consider S =   a.....b...a...b.....a
Now check when S[L]= S[R], we have to add all palindromes that contains *(L+1,R-1)* because they are bordered by <code>a </code> character + <code>1</code> (only the border).
The formula will be: *DP(L, R) =  (DP(L + 1, R) + DP(L, R-1) - DP(L+1, R-1))  +   (DP(L+1, R-1)+1 )*,  solving that , we have.
<code>DP(L, R) = DP(L + 1, R) + DP(L, R-1) + 1</code>
 
<strong>Distinct palindromic</strong>
DP(L, R, alpha), is the number of distinct palindromic subsequences in S[L .... R]  bordered by alpha. . Its formulated as following.

DP(L, R, alpha), is the number of palindromic subsequences in S[L .... R]. Its formulated as following.
* 0    , if L>R or (L=R and S[L]!=alpha)
* 1    , if L=R and S[L]=alpha
* DP(L + 1, R, alpha) + DP(L, R-1,alpha) - DP(L+1, R-1,alpha)   , if S[L] != S[R]
* 2+ SUM(DP(L+1,R-1, Betha) )  where Betha is all the alphabet , if S[L] = S[R] and S[L]=alpha

**Why the third one?**
Similar that , not necessarily distinct palindromic.
**Why the fourth one?**
Its easy to verify.
S[L......R] =  [a.......b......a]
 2+ SUM(DP(L+1,R-1, Betha) ), number 2 is because of getting ( **a** and **aa**)
the sum is the result of getting palindromes with border **a**.

**C++**
<code>

	int memo[1001][1001][4];
	class Solution {
	public:
		string S;
		int MOD = 1000000007;
	
		int dp(int start,int end,int alpha){
			//base case
			if(start>end)return 0;
			if(start==end){
				if(S[start] == (alpha+\'a\') )return 1;
				return 0;
			}
			
			if(memo[start][end][alpha]!=-1)return memo[start][end][alpha];
			
			int dev=0;
			if(S[start]==S[end] && S[start]==(alpha+\'a\')){
				dev=2;
				for(int i=0;i<4;i++)
					dev=(dev + dp(start+1,end-1,i) )%MOD;
			}else{
				dev= (dev + dp(start,end-1,alpha))%MOD;
				dev= (dev + dp(start+1,end,alpha))%MOD;
				dev= (dev - dp(start+1,end-1,alpha))%MOD;
				if(dev<0)dev+=MOD;
			}
			
			memo[start][end][alpha]=dev;
			return dev;
		}
		
		int countPalindromicSubsequences(string _S) {
			S=_S;
			memset(memo,-1,sizeof(memo));
			int ans=0;
			
			for(int i=0;i<4;i++)
				ans= (ans + dp(0, S.size()-1, i))%MOD;
			
			return ans;        
		}
	};
</code>


<p><strong>Complexity Analysis</strong></p>
<ul>
<li>
<p>Time Complexity:  <span class="katex"><span class="katex-mathml"><math><semantics><mrow><mi>O</mi><mo>(</mo><mi>N^2</mi><mo>)</mo></mrow><annotation encoding="application/x-tex">O(N^2)</annotation></semantics></math></span><span class="katex-html" aria-hidden="true"><span class="base"><span class="strut" style="height:1em;vertical-align:-0.25em;"></span><span class="mord mathit" style="margin-right:0.02778em;">O</span><span class="mopen">(</span><span class="mord mathit" style="margin-right:0.10903em;">N^2*alphabet</span><span class="mclose">)</span></span></span></span>, where <span class="katex"><span class="katex-mathml"><math><semantics><mrow><mi>N</mi></mrow><annotation encoding="application/x-tex">N</annotation></semantics></math></span><span class="katex-html" aria-hidden="true"><span class="base"><span class="strut" style="height:0.68333em;vertical-align:0em;"></span><span class="mord mathit" style="margin-right:0.10903em;">N</span></span></span></span> is length of <code>S</code> and alphabet is 4 (a,b,c,d).</p>
</li>
<li>
<p>Space Complexity:  <span class="katex"><span class="katex-mathml"><math><semantics><mrow><mi>O</mi><mo>(</mo><mi>N</mi><mo>)</mo></mrow><annotation encoding="application/x-tex">O(N)</annotation></semantics></math></span><span class="katex-html" aria-hidden="true"><span class="base"><span class="strut" style="height:1em;vertical-align:-0.25em;"></span><span class="mord mathit" style="margin-right:0.02778em;">O</span><span class="mopen">(</span><span class="mord mathit" style="margin-right:0.10903em;">N^2*alphabet</span><span class="mclose">)</span></span></span></span>.
<br>
<br></p>
</li>
</ul>
</p>


