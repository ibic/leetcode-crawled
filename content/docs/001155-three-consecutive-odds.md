---
title: "Three Consecutive Odds"
weight: 1155
#id: "three-consecutive-odds"
---
## Description
<div class="description">
Given an integer array <code>arr</code>, return <code>true</code>&nbsp;if there are three consecutive odd numbers in the array. Otherwise, return&nbsp;<code>false</code>.
<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,6,4,1]
<strong>Output:</strong> false
<b>Explanation:</b> There are no three consecutive odds.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,34,3,4,5,7,23,12]
<strong>Output:</strong> true
<b>Explanation:</b> [5,7,23] are three consecutive odds.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 1000</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- DJI - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Self Explanatory Code Linear Time
- Author: ngytlcdsalcp1
- Creation Date: Sun Aug 16 2020 12:04:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 16 2020 12:04:09 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public boolean threeConsecutiveOdds(int[] arr) {
        for(int i = 1; i < arr.length - 1; i++) {
            if(arr[i - 1] % 2 == 1 && arr[i] % 2 == 1 && arr[i + 1] % 2 == 1 )
                return true;
        }
        return false;
    }
}
```
</p>


### C++ Straightforward
- Author: votrubac
- Creation Date: Sun Aug 16 2020 12:03:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 16 2020 12:09:03 GMT+0800 (Singapore Standard Time)

<p>
Why do we even need questions like that?
```cpp
bool threeConsecutiveOdds(vector<int>& arr) {
    int odds = 0;
    for (auto i = 0; i < arr.size() && odds < 3; ++i)
        odds = arr[i] % 2 ? odds + 1 : 0;
    return odds == 3;
}
```
</p>


### Java Simple Loop
- Author: hobiter
- Creation Date: Sun Aug 16 2020 12:05:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 16 2020 12:05:39 GMT+0800 (Singapore Standard Time)

<p>
```
    public boolean threeConsecutiveOdds(int[] arr) {
        for (int i = 0, cnt = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) cnt = 0;
            else if (++cnt == 3) return true;
        }
        return false;
    }
```
</p>


