---
title: "Get Watched Videos by Your Friends"
weight: 1232
#id: "get-watched-videos-by-your-friends"
---
## Description
<div class="description">
<p>There are <code>n</code> people, each person has a unique <em>id</em> between <code>0</code> and <code>n-1</code>. Given the arrays <code>watchedVideos</code> and <code>friends</code>, where <code>watchedVideos[i]</code> and <code>friends[i]</code> contain the list of watched videos and the list of friends respectively for the person with <code>id = i</code>.</p>

<p>Level <strong>1</strong> of videos are all watched videos by your&nbsp;friends, level <strong>2</strong> of videos are all watched videos by the friends of your&nbsp;friends and so on. In general, the level <code>k</code> of videos are all&nbsp;watched videos by people&nbsp;with the shortest path <strong>exactly</strong> equal&nbsp;to&nbsp;<code>k</code> with you. Given your&nbsp;<code>id</code> and the <code>level</code> of videos, return the list of videos ordered by their frequencies (increasing). For videos with the same frequency order them alphabetically from least to greatest.&nbsp;</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/01/02/leetcode_friends_1.png" style="width: 144px; height: 200px;" /></strong></p>

<pre>
<strong>Input:</strong> watchedVideos = [[&quot;A&quot;,&quot;B&quot;],[&quot;C&quot;],[&quot;B&quot;,&quot;C&quot;],[&quot;D&quot;]], friends = [[1,2],[0,3],[0,3],[1,2]], id = 0, level = 1
<strong>Output:</strong> [&quot;B&quot;,&quot;C&quot;] 
<strong>Explanation:</strong> 
You have id = 0 (green color in the figure) and your friends are (yellow color in the figure):
Person with id = 1 -&gt; watchedVideos = [&quot;C&quot;]&nbsp;
Person with id = 2 -&gt; watchedVideos = [&quot;B&quot;,&quot;C&quot;]&nbsp;
The frequencies of watchedVideos by your friends are:&nbsp;
B -&gt; 1&nbsp;
C -&gt; 2
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/01/02/leetcode_friends_2.png" style="width: 144px; height: 200px;" /></strong></p>

<pre>
<strong>Input:</strong> watchedVideos = [[&quot;A&quot;,&quot;B&quot;],[&quot;C&quot;],[&quot;B&quot;,&quot;C&quot;],[&quot;D&quot;]], friends = [[1,2],[0,3],[0,3],[1,2]], id = 0, level = 2
<strong>Output:</strong> [&quot;D&quot;]
<strong>Explanation:</strong> 
You have id = 0 (green color in the figure) and the only friend of your friends is the person with id = 3 (yellow color in the figure).
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>n == watchedVideos.length ==&nbsp;friends.length</code></li>
	<li><code>2 &lt;= n&nbsp;&lt;= 100</code></li>
	<li><code>1 &lt;=&nbsp;watchedVideos[i].length &lt;= 100</code></li>
	<li><code>1 &lt;=&nbsp;watchedVideos[i][j].length &lt;= 8</code></li>
	<li><code>0 &lt;= friends[i].length &lt; n</code></li>
	<li><code>0 &lt;= friends[i][j]&nbsp;&lt; n</code></li>
	<li><code>0 &lt;= id &lt; n</code></li>
	<li><code>1 &lt;= level &lt; n</code></li>
	<li>if&nbsp;<code>friends[i]</code> contains <code>j</code>, then <code>friends[j]</code> contains <code>i</code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- String (string)
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] BFS + Sort
- Author: PhoenixDD
- Creation Date: Sun Jan 05 2020 12:01:20 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jan 10 2020 16:11:02 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
We can use a simple BFS to get to the required level of the graph nodes and any type of sorting to yeild resulting `watchedVideos` sorted by frequencies.

**Solution**
```c++
class Solution {
public:
    queue<int> q;
    vector<string> watchedVideosByFriends(vector<vector<string>>& watchedVideos, vector<vector<int>>& friends, int id, int level) 
    {
        vector<bool> visited(friends.size(),false);      //To track the visited `friends`
        unordered_map<string,int> count;        // Stores the frequency of all the  `watchedVideos` by a freind at required level
        vector<pair<int,string>> resultPairs;
        vector<string> result;
        q.push(id),visited[id]=true;            //Push initial node/friend id.
        while(!q.empty()&&level--)				//BFS to get to the level.
            for(int i=q.size();i;q.pop(),i--)
                for(int &i:friends[q.front()])
                    if(!visited[i])
                        visited[i]=true,q.push(i);
        for(;!q.empty();q.pop())                      //The queue at this moment will only have all the friends at required level
            for(string &s:watchedVideos[q.front()])
                count[s]++;
        for(auto it=count.begin();it!=count.end();it++) //Add results to the vector to sort by frequency first and then the string.
            resultPairs.push_back({it->second,it->first});
        sort(resultPairs.begin(),resultPairs.end());
        transform(resultPairs.begin(),resultPairs.end(),back_inserter(result),[](pair<int,string>&p){return p.second;}); //Transform the pairs to result
        return result;            
    }
};
```
</p>


### Please improve the description of the problem!
- Author: Taranovski
- Creation Date: Sun Jan 05 2020 12:34:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 05 2020 12:34:05 GMT+0800 (Singapore Standard Time)

<p>
I didn\'t find any "feedback" button, so I\'ll post here. 
The problem itself is good, I liked it.

BUT

PLEASE CLARIFY DESCRIPTION AND ADD APPROPRIATE UNIT TEST WITH A SMALL GRAPH!!!!

so that people can grasp the essence more clearly.
I can propose a small change - in addition to existing part of the description:

"In general, the level k of videos are all watched videos by people with the shortest path equal to k with you."

you can add a small example/clarification like:

"we want to find ONLY friends at the given level
that is: 
given level 1 - effectively means any people who are not the initial person
level 2 means that we are interested only in the friend\'s friends, EXCLUDING friends of level 1 and the initial person
level 3 means that we are interested only in the friend\'s friends\'s friends, EXCLUDING the initial person, friends of level 1 AND friends of level 2
and so on..."

I could not submit the successful solution before contest have ended.
I copied you big input for the unit test.
It is not super not fun to debug your large input to finaly get after half an hour that the problem lies in the very first step of how we find friends at the level.
</p>


### Python 6 lines bfs solution
- Author: Luolingwei
- Creation Date: Sun Jan 05 2020 12:03:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 05 2020 12:03:51 GMT+0800 (Singapore Standard Time)

<p>
```python
import collections
class Solution:
    def watchedVideosByFriends(self, watchedVideos: List[List[str]], friends: List[List[int]], id: int, level: int) -> List[str]:
        bfs,visited={id},{id}
        for _ in range(level):
            bfs={j for i in bfs for j in friends[i] if j not in visited}
            visited|=bfs
        freq=collections.Counter([v for idx in bfs for v in watchedVideos[idx]])
        return sorted(freq.keys(),key=lambda x:(freq[x],x))
```
</p>


