---
title: "Ternary Expression Parser"
weight: 416
#id: "ternary-expression-parser"
---
## Description
<div class="description">
<p>Given a string representing arbitrarily nested ternary expressions, calculate the result of the expression. You can always assume that the given expression is valid and only consists of digits <code>0-9</code>, <code>?</code>, <code>:</code>, <code>T</code> and <code>F</code> (<code>T</code> and <code>F</code> represent True and False respectively).

<p><b>Note:</b>
<ol>
<li>The length of the given string is &le; 10000.</li>
<li>Each number will contain only one digit.</li>
<li>The conditional expressions group right-to-left (as usual in most languages).</li>
<li>The condition will always be either <code>T</code> or <code>F</code>. That is, the condition will never be a digit.</li>
<li>The result of the expression will always evaluate to either a digit <code>0-9</code>, <code>T</code> or <code>F</code>.</li>
</ol>
</p>

<p>
<b>Example 1:</b>
<pre>
<b>Input:</b> "T?2:3"

<b>Output:</b> "2"

<b>Explanation:</b> If true, then result is 2; otherwise result is 3.
</pre>
</p>

<p>
<b>Example 2:</b>
<pre>
<b>Input:</b> "F?1:T?4:5"

<b>Output:</b> "4"

<b>Explanation:</b> The conditional expressions group right-to-left. Using parenthesis, it is read/evaluated as:

             "(F ? 1 : (T ? 4 : 5))"                   "(F ? 1 : (T ? 4 : 5))"
          -> "(F ? 1 : 4)"                 or       -> "(T ? 4 : 5)"
          -> "4"                                    -> "4"
</pre>
</p>

<p>
<b>Example 3:</b>
<pre>
<b>Input:</b> "T?T?F:5:3"

<b>Output:</b> "F"

<b>Explanation:</b> The conditional expressions group right-to-left. Using parenthesis, it is read/evaluated as:

             "(T ? (T ? F : 5) : 3)"                   "(T ? (T ? F : 5) : 3)"
          -> "(T ? F : 3)"                 or       -> "(T ? F : 5)"
          -> "F"                                    -> "F"
</pre>
</p>
</div>

## Tags
- Stack (stack)
- Depth-first Search (depth-first-search)

## Companies
- Snapchat - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Very easy 1 pass Stack Solution in JAVA (NO STRING CONCAT)
- Author: NathanNi
- Creation Date: Sun Oct 23 2016 14:23:32 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 18:07:46 GMT+0800 (Singapore Standard Time)

<p>
Iterate the expression from tail, whenever encounter a character before '?', calculate the right value and push back to stack.

P.S. this code is guaranteed only if "the given expression is valid" base on the requirement.

    public String parseTernary(String expression) {
        if (expression == null || expression.length() == 0) return "";
        Deque<Character> stack = new LinkedList<>();

        for (int i = expression.length() - 1; i >= 0; i--) {
            char c = expression.charAt(i);
            if (!stack.isEmpty() && stack.peek() == '?') {

                stack.pop(); //pop '?'
                char first = stack.pop();
                stack.pop(); //pop ':'
                char second = stack.pop();

                if (c == 'T') stack.push(first);
                else stack.push(second);
            } else {
                stack.push(c);
            }
        }

        return String.valueOf(stack.peek());
    }
</p>


### 5ms JAVA DFS Solution
- Author: binghont
- Creation Date: Thu Nov 17 2016 14:51:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 22 2018 02:17:09 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public String parseTernary(String expression) {
        if(expression == null || expression.length() == 0){
            return expression;
        }
        char[] exp = expression.toCharArray();
        
        return DFS(exp, 0, exp.length - 1) + "";
        
    }
    public char DFS(char[] c, int start, int end){
        if(start == end){
            return c[start];
        }
        int count = 0, i =start;
        for(; i <= end; i++){
            if(c[i] == '?'){
                count ++;
            }else if (c[i] == ':'){
                count --;
                if(count == 0){
                    break;
                }
            }
        }
        return c[start] == 'T'? DFS(c, start + 2, i - 1) : DFS(c, i+1,end);
    }
}
```
</p>


### Easy and Concise 5-lines Python/Java Solution
- Author: YJL1228
- Creation Date: Sun Oct 23 2016 13:21:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 23 2016 13:21:58 GMT+0800 (Singapore Standard Time)

<p>
In order to pick out useful "?" and ":", we can always begin with **the last "?" and the first ":" after the chosen "?"**.

Therefore, directly seek for the last "?" (or you can simply put all "?" into a stack) and update the string depending on T or F until no more "?"s.

e.g. 
"(F ? 1 : (T ? 4 : 5))" => "(F ? 1 : 4)" => "4"
"(T ? (T ? F : 5) : 3)" => "(T ? F : 3)" => "F"

**EDIT:**
Removed stack, added Java version.

**Python**:
```
class Solution(object):
    def parseTernary(self, expression):
        """
        :type expression: str
        :rtype: str
        """
        while len(expression) != 1:
            i = expression.rindex("?")    # begin with the last '?'.
            tmp = expression[i+1] if expression[i-1] == 'T' else expression[i+3]
            expression = expression[:i-1] + tmp + expression[i+4:]
        return expression
```

**Java** (It costs 7-lines):
```
public class Solution {
    public String parseTernary(String expression) {
        while (expression.length() != 1) {
            int i = expression.lastIndexOf("?");    // get the last shown '?'
            char tmp;
            if (expression.charAt(i-1) == 'T') { tmp = expression.charAt(i+1); }
            else { tmp = expression.charAt(i+3); }
            expression = expression.substring(0, i-1) + tmp + expression.substring(i+4);
        }
        return expression;
    }
}


```
</p>


