---
title: "Reverse Integer"
weight: 7
#id: "reverse-integer"
---
## Description
<div class="description">
<p>Given a 32-bit signed integer, reverse digits of an integer.</p>

<p><strong>Note:</strong><br />
Assume we are dealing with an environment that could only store integers within the 32-bit signed integer range: [&minus;2<sup>31</sup>,&nbsp; 2<sup>31&nbsp;</sup>&minus; 1]. For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> x = 123
<strong>Output:</strong> 321
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> x = -123
<strong>Output:</strong> -321
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> x = 120
<strong>Output:</strong> 21
</pre><p><strong>Example 4:</strong></p>
<pre><strong>Input:</strong> x = 0
<strong>Output:</strong> 0
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>-2<sup>31</sup> &lt;= x &lt;= 2<sup>31</sup> - 1</code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Facebook - 8 (taggedByAdmin: false)
- Apple - 6 (taggedByAdmin: true)
- Adobe - 4 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: true)
- JPMorgan - 5 (taggedByAdmin: false)
- Lyft - 3 (taggedByAdmin: false)
- Cisco - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Barclays - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---

#### Approach 1: Pop and Push Digits & Check before Overflow

**Intuition**

We can build up the reverse integer one digit at a time.
While doing so, we can check beforehand whether or not appending another digit would cause overflow.

**Algorithm**

Reversing an integer can be done similarly to reversing a string.

We want to repeatedly "pop" the last digit off of $$x$$ and "push" it to the back of the $$\text{rev}$$. In the end, $$\text{rev}$$ will be the reverse of the $$x$$.

To "pop" and "push" digits without the help of some auxiliary stack/array, we can use math.

```cpp
//pop operation:
pop = x % 10;
x /= 10;

//push operation:
temp = rev * 10 + pop;
rev = temp;
```

However, this approach is dangerous, because the statement $$\text{temp} = \text{rev} \cdot 10 + \text{pop}$$ can cause overflow.

Luckily, it is easy to check beforehand whether or this statement would cause an overflow.

To explain, lets assume that $$\text{rev}$$ is positive.

1. If $$temp = \text{rev} \cdot 10 + \text{pop}$$ causes overflow, then it must be that $$\text{rev} \geq \frac{INTMAX}{10}$$
2. If $$\text{rev} > \frac{INTMAX}{10}$$, then $$temp = \text{rev} \cdot 10 + \text{pop}$$ is guaranteed to overflow.
3. If $$\text{rev} == \frac{INTMAX}{10}$$, then $$temp = \text{rev} \cdot 10 + \text{pop}$$ will overflow if and only if $$\text{pop} > 7$$

Similar logic can be applied when $$\text{rev}$$ is negative.

<iframe src="https://leetcode.com/playground/Ufhk9yCy/shared" frameBorder="0" width="100%" height="293" name="Ufhk9yCy"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(\log(x))$$. There are roughly $$\log_{10}(x)$$ digits in $$x$$.
* Space Complexity: $$O(1)$$.

## Accepted Submission (python3)
```python3
class Solution:
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        stack = []
        ax = abs(x)
        while ax > 0:
            stack.append(ax % 10)
            ax = ax // 10
        #print(stack)
        ls = len(stack)
        r = 0
        for i in range(ls):
            r += pow(10, i) * stack[ls - 1 - i]
        r = r if x > 0 else -r
        if r > pow(2, 31) or r < -pow(2, 31) - 1:
            return 0
        else:
            return r
        
```

## Top Discussions
### My accepted 15 lines of code for Java
- Author: bitzhuwei
- Creation Date: Wed Dec 17 2014 11:07:03 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 18:10:24 GMT+0800 (Singapore Standard Time)

<p>
Only 15 lines.
If overflow exists, the new result will not equal previous one.
No flags needed. No hard code like 0xf7777777 needed.
Sorry for my bad english.

    public int reverse(int x)
    {
        int result = 0;

        while (x != 0)
        {
            int tail = x % 10;
            int newResult = result * 10 + tail;
            if ((newResult - tail) / 10 != result)
            { return 0; }
            result = newResult;
            x = x / 10;
        }

        return result;
    }
</p>


### Very Short (7 lines) and Elegant Solution
- Author: kbakhit
- Creation Date: Sun May 31 2015 13:44:58 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 06:17:59 GMT+0800 (Singapore Standard Time)

<p>
```    
public int reverse(int x) {
        long rev= 0;
        while( x != 0){
            rev= rev*10 + x % 10;
            x= x/10;
            if( rev > Integer.MAX_VALUE || rev < Integer.MIN_VALUE)
                return 0;
        }
        return (int) rev;
    }
```
		
**Update: Not using long:**

```
  public int reverse(int x) {
        int prevRev = 0 , rev= 0;
        while( x != 0){
            rev= rev*10 + x % 10;
            if((rev - x % 10) / 10 != prevRev){
                return 0;
            }
            prevRev = rev;
            x= x/10;
        }
        return rev;
    }
```
</p>


### Golfing in Python
- Author: StefanPochmann
- Creation Date: Wed Jun 10 2015 12:08:23 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 20:38:12 GMT+0800 (Singapore Standard Time)

<p>
Get the `s`ign, get the `r`eversed absolute integer, and return their product if `r` didn't "overflow".

    def reverse(self, x):
        s = cmp(x, 0)
        r = int(`s*x`[::-1])
        return s*r * (r < 2**31)

As compressed one-liner, for potential comparison:

    def reverse(self, x):
        s=cmp(x,0);r=int(`s*x`[::-1]);return(r<2**31)*s*r

Anybody got something shorter?
</p>


