---
title: "Fixed Point"
weight: 975
#id: "fixed-point"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of distinct integers sorted in ascending order, return the smallest index <code>i</code> that satisfies <code>A[i] == i</code>.&nbsp; Return <code>-1</code> if no such <code>i</code> exists.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[-10,-5,0,3,7]</span>
<strong>Output: </strong><span id="example-output-1">3</span>
<strong>Explanation: </strong>
For the given array, <code>A[0] = -10, A[1] = -5, A[2] = 0, A[3] = 3</code>, thus the output is 3.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[0,2,5,8,17]</span>
<strong>Output: </strong><span id="example-output-2">0</span>
<strong>Explanation: </strong>
<code>A[0] = 0</code>, thus the output is 0.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[-10,-5,3,4,7,9]</span>
<strong>Output: </strong><span id="example-output-3">-1</span>
<strong>Explanation: </strong>
There is no such <code>i</code> that <code>A[i] = i</code>, thus the output is -1.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt; 10^4</code></li>
	<li><code>-10^9 &lt;= A[i] &lt;= 10^9</code></li>
</ol>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Uber - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Binary Search 0 in A[i] - i
- Author: lee215
- Creation Date: Sun Jun 02 2019 01:09:33 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 28 2019 11:41:02 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
`A[i]` is distinct and ascending.
`A[i] - i` is non-descending array.
Binary search the first `0` in the array of `A[i] - i`.

## **Easy prove**
`A[i] < A[i + 1]`
`A[i] <= A[i + 1] - 1`
`A[i] - i <= A[i + 1] - i - 1`

## Test Cases
It said that "return the smallest index i that satisfies A[i] == i".
Many solution just "return an whatever index that satisfies A[i] == i".
Tough it can get accepted.

## **Update**
I used to return any index that `A[i] == i`.
It got a false accepted due to weak test cases.
Thanks to @dibdidib for reminding me and now I fixed it.

## **Complexity**
Time `O(logN)`, Space `O(1)`
<br>

**Java:**
```
    public int fixedPoint(int[] A) {
        int l = 0, r = A.length - 1;
        while (l < r) {
            int m = (l + r) / 2;
            if (A[m] - m < 0)
                l = m + 1;
            else
                r = m;
        }
        return A[l] == l ? l : -1;
    }
```

**C++:**
```
    int fixedPoint(vector<int>& A) {
        int l = 0, r = A.size() - 1, m;
        while (l < r) {
            m = (l + r) / 2;
            if (A[m] - m < 0)
                l = m + 1;
            else
                r = m;
        }
        return A[l] == l ? l : -1;
    }
```

**Python:**
```
    def fixedPoint(self, A):
        l, r = 0, len(A) - 1
        while l < r:
            m = (l + r) / 2
            if A[m] - m < 0:
                l = m + 1
            else:
                r = m
        return l if A[l] == l else -1
```
</p>


### You cannot find a more straight forward Binary Search, Java
- Author: yyvax
- Creation Date: Tue Jun 18 2019 00:03:38 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 18 2019 00:03:38 GMT+0800 (Singapore Standard Time)

<p>

```
class Solution {
    public int fixedPoint(int[] A) {
        int res = -1;
        int left = 0, right = A.length - 1;
        while (left <= right) {
            int mid = (left + right) / 2;
            if (A[mid] >= mid) {
                if (A[mid] == mid) res = mid;
                right = mid - 1;
            }
            else {
                left = mid + 1;
            }
        }
        return res;
    }
}
```
</p>


### Python Binary Search Solution
- Author: rexcancode
- Creation Date: Tue Jun 04 2019 11:07:59 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 05 2019 11:30:14 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def fixedPoint(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        
        if not A: return -1
        
        start, end = 0, len(A)-1
        
        while start + 1 < end:
            mid = (start + end) // 2
            
            if mid <= A[mid]:
                end = mid
            else:
                start = mid
        
        if A[start] == start:
            return start
        
        if A[end] == end:
            return end
        
        return -1
```
</p>


