---
title: "Minimum Number of Frogs Croaking"
weight: 1314
#id: "minimum-number-of-frogs-croaking"
---
## Description
<div class="description">
<p>Given the string <code>croakOfFrogs</code>, which represents a combination of the string &quot;croak&quot; from different frogs, that is, multiple frogs can croak at the same time, so multiple &ldquo;croak&rdquo; are mixed.&nbsp;<em>Return the minimum number of </em>different<em> frogs to finish all the croak in the given string.</em></p>

<p>A valid &quot;croak&quot;&nbsp;means a frog is printing 5 letters &lsquo;c&rsquo;, &rsquo;r&rsquo;, &rsquo;o&rsquo;, &rsquo;a&rsquo;, &rsquo;k&rsquo;&nbsp;<strong>sequentially</strong>.&nbsp;The frogs have to print all five letters to finish a croak.&nbsp;If the given string is not a combination of valid&nbsp;&quot;croak&quot;&nbsp;return -1.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> croakOfFrogs = &quot;croakcroak&quot;
<strong>Output:</strong> 1 
<strong>Explanation:</strong> One frog yelling &quot;croak<strong>&quot;</strong> twice.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> croakOfFrogs = &quot;crcoakroak&quot;
<strong>Output:</strong> 2 
<strong>Explanation:</strong> The minimum number of frogs is two.&nbsp;
The first frog could yell &quot;<strong>cr</strong>c<strong>oak</strong>roak&quot;.
The second frog could yell later &quot;cr<strong>c</strong>oak<strong>roak</strong>&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> croakOfFrogs = &quot;croakcrook&quot;
<strong>Output:</strong> -1
<strong>Explanation:</strong> The given string is an invalid combination of &quot;croak<strong>&quot;</strong> from different frogs.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> croakOfFrogs = &quot;croakcroa&quot;
<strong>Output:</strong> -1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;=&nbsp;croakOfFrogs.length &lt;= 10^5</code></li>
	<li>All characters in the string are: <code>&#39;c&#39;</code>, <code>&#39;r&#39;</code>, <code>&#39;o&#39;</code>, <code>&#39;a&#39;</code> or <code>&#39;k&#39;</code>.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- C3.ai - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java] with picture, simulation
- Author: votrubac
- Creation Date: Sun Apr 19 2020 12:05:50 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 23 2020 03:40:07 GMT+0800 (Singapore Standard Time)

<p>
We can track how many frogs are \'singing\' each letter in `cnt`:
- Increase number of frogs singing this letter, and decrease number singing previous letter.
- When a frog sings \'c\', we increase the number of (simultaneous) `frogs`.
- When a frog sings \'k\', we decrease the number of (simultaneous) `frogs`.
- If some frog is singing a letter, but no frog sang the previous letter, we return `-1`.

![image](https://assets.leetcode.com/users/votrubac/image_1587273232.png)

Track and return the maximum number of frogs ever signing together.

> Catch: if some frog hasn\'t finished croaking, we need to return `-1`.

**C++**
```cpp
int minNumberOfFrogs(string croakOfFrogs) {
    int cnt[5] = {}, frogs = 0, max_frogs = 0;
    for (auto ch : croakOfFrogs) {
        auto n = string("croak").find(ch);
        ++cnt[n];
        if (n == 0)
            max_frogs = max(max_frogs, ++frogs);
        else if (--cnt[n - 1] < 0)
            return -1;
        else if (n == 4)
            --frogs;
    }
    return frogs == 0 ? max_frogs : -1;
}
```

**Java**
```java
public int minNumberOfFrogs(String croakOfFrogs) {
    int cnt[] = new int[5];
    int frogs = 0, max_frogs = 0;
    for (var i = 0; i < croakOfFrogs.length(); ++i) {
        var ch = croakOfFrogs.charAt(i);
        var n = "croak".indexOf(ch);
        ++cnt[n];
        if (n == 0)
            max_frogs = Math.max(max_frogs, ++frogs);
        else if (--cnt[n - 1] < 0)
            return -1;
        else if (n == 4)
            --frogs;
    }
    return frogs == 0 ? max_frogs : -1;    
}
```
</p>


### [C++, Python, Java] Lucid code with documened comments + Visualization
- Author: interviewrecipes
- Creation Date: Sun Apr 19 2020 12:32:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 08 2020 13:24:58 GMT+0800 (Singapore Standard Time)

<p>
Whenever a \'c\' is encountered, you know that a frog has started to croak. This frog can be a new frog or one of the existing one. 
Whenever a \'k\' is encountered, you know that one of the frogs have stopped croaking, hence decrement the count of frogs  so that whenever a new \'c\' is encountered, we can reuse the same frog.

This is the basic idea. 
For \'c\', increment in use frogs by one.
For \'k\' decrement the in use count by one.
The maximum value reached by the in use count is the answer.
![image](https://assets.leetcode.com/users/images/22e5ee5a-1119-418b-be98-cf41192fe37f_1592936660.1869717.png)

Please upvote if you liked the solution, it would be encouraging.

**C++: Variation 1**
```
class Solution {
    unordered_map<char, int> frequency; // Stores how many times each sound has occured. Sounds are c, r, o, a, k. 
    /*
     *  At any time, for a sequence to be valid, number of \'c\' must not be less than \'r\'.
     *  Similarly, #\'r\' shouldn\'t  less than #\'o\', and so on.
     */
    bool isStateValid() {
         return (frequency[\'c\'] >= frequency[\'r\']) &&
                (frequency[\'r\'] >= frequency[\'o\']) &&
                (frequency[\'o\'] >= frequency[\'a\']) && 
                (frequency[\'a\'] >= frequency[\'k\']);
    }
    
public:
    /*
     *  Minimum number of frogs that we need is maximum number of frogs that are croaking
     *  simultaneously.
     *  Each "croaking" is a sequence of c, r, o, a, and k.
     *  Sound is a character in croakSequence.
     */
    int minNumberOfFrogs(string croakSequence) {
        int numCroakingFrogs = 0; // Number of distinct frogs that are croaking at any given time.
        int answer = 0; // Hold the final answer.
        for (char &sound: croakSequence) { // for each sound i.e. character.
            frequency[sound]++; // Note the sound.
            if (!isStateValid()) { // Make sure we are still in valid state.
                return -1;
            }
            if (sound == \'c\') { // New "croaking" always begins at \'c\'.
                numCroakingFrogs++; // Addional frog for the new "croaking".
            } else if (sound == \'k\') { // A "croaking" ends at \'k\'.
                numCroakingFrogs--; // Some frog has stopped croaking now.
            }
            answer = max(answer, numCroakingFrogs); // Maximum number of frogs that are croaking 
                                                    // simultaneously over a period.
        }
        return numCroakingFrogs == 0 ? answer : -1; // Make sure all frogs have completed the croaking.
    }
};
```

**C++ : Variation 2**
```
int minNumberOfFrogs(string croak) {
        int c = 0, r = 0, o = 0, a = 0, k = 0, in_use = 0, answer = 0;
        for (char d:croak) {
            switch(d) {
                case \'c\':
                    c++;
                    in_use++;
                    break;
                case \'r\':
                    r++;
                    break;
                case \'o\':
                    o++;
                    break;
                case \'a\':
                    a++;
                    break;
                case \'k\':
                    k++;
                    in_use--;
                    break;
            }
			answer = max(answer, in_use);
            if ((c < r) || (r < o) || (o < a) || (a < k)) 
                return -1;
        }
        if (in_use == 0 && c == r && c == o && c == a && c == k)
            return answer;
			
        return -1;
    }
```

**Python**
```
    def minNumberOfFrogs(self, croak):
        c, r, o, a, k, in_use, answer = 0, 0, 0, 0, 0, 0, 0
        for d in croak:
            if d == \'c\':
                c, in_use = c+1, in_use+1
            elif d == \'r\':
                r += 1
            elif d == \'o\':
                o += 1
            elif d == \'a\':
                a += 1
            else:
                k, in_use = k+1, in_use-1
                
            answer = max(answer, in_use)
            
            if c < r or r < o or o < a or a < k:
                return -1
            
        if in_use == 0 and c == r and r == o and o == a and a == k:
            return answer
        
        return -1
```

**Java**
```
    public int minNumberOfFrogs(String croak) {
        int c = 0, r = 0, o = 0, a = 0, k = 0, in_use = 0, answer = 0;
        for (char d:croak.toCharArray()) {
            switch(d) {
                case \'c\':
                    c++;
                    in_use++;
                    break;
                case \'r\':
                    r++;
                    break;
                case \'o\':
                    o++;
                    break;
                case \'a\':
                    a++;
                    break;
                case \'k\':
                    k++;
                    in_use--;
                    break;
            }
			answer = Math.max(answer, in_use);
            if ((c < r) || (r < o) || (o < a) || (a < k)) 
                return -1;
        }
        if (in_use == 0 && c == r && c == o && c == a && c == k)
            return answer;
			
        return -1;
    }
```


</p>


### Count maximum number of ongoing state machines
- Author: aiifabbf
- Creation Date: Sun Apr 19 2020 12:01:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 13:41:27 GMT+0800 (Singapore Standard Time)

<p>
This problem can be abstracted as counting how many ongoing state machines there are at most, of all times. 1 frog = 1 state machine replica.

Here is the state transfer graph for a machine that can detect string `croak`:

```
0 ---> 1 ---> 2 ---> 3 ---> 4 ---> 5
   c      r      o      a      k
````

0 is initial state, 5 is final state. If a machine is not at initial state, nor at final state, it is an ongoing machine.

When the stream has been consumed, if all machines are either at initial state, or at final state, and there are no ongoing machines, then the result is valid (thus return the result); otherwise, some machines did not make its way to final state (some frogs did not sing the whole song), then the result is not valid (thus return -1).

```python
class Solution:
    def minNumberOfFrogs(self, croakOfFrogs: str) -> int:
        states = {k: 0 for k in range(0, 6)}
        # states[k] == v means there are currently `v` machines at state `k`
        # there are infinite number of machines at state 0, waiting to be fed a "c"
        res = 0 # maximum number of ongoing machines

        for v in croakOfFrogs:
            if v == "c":
                states[1] += 1 # one machine goes from state 0 to state 1
            elif v == "r":
                if states[1] > 0:
                    states[1] -= 1 # one machine goes from state 1
                    states[2] += 1 # to state 2
                else:
                    return -1
                # unexpected char. No machine takes this. Early return. Thanks to @paanipoori
                # also prevents cases like croakk. Thanks to @ke755024
            elif v == "o":
                if states[2] > 0:
                    states[2] -= 1 # one machine goes from state 2
                    states[3] += 1 # to state 3
                else:
                    return -1
            elif v == "a":
                if states[3] > 0:
                    states[3] -= 1
                    states[4] += 1
                else:
                    return -1
            elif v == "k":
                if states[4] > 0:
                    states[4] -= 1
                    states[5] += 1
                else:
                    return -1

            res = max(res, sum(states[k] for k in range(1, 5))) # how many ongoing machines at this time

        if any(states[k] != 0 for k in range(1, 5)): # if there are still ongoing machines at the end
            return -1 # invalid
        else: # all machines are initial or final
            return res
```

Update: Thanks to @ke755024 and @paanipoori, I added early return to unexpected chars to deal with test cases like `croakk`.
</p>


