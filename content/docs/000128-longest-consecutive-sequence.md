---
title: "Longest Consecutive Sequence"
weight: 128
#id: "longest-consecutive-sequence"
---
## Description
<div class="description">
<p>Given an unsorted array of integers, find the length of the longest consecutive elements sequence.</p>

<p>Your algorithm should run in O(<em>n</em>) complexity.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong>&nbsp;[100, 4, 200, 1, 3, 2]
<strong>Output:</strong> 4
<strong>Explanation:</strong> The longest consecutive elements sequence is <code>[1, 2, 3, 4]</code>. Therefore its length is 4.
</pre>

</div>

## Tags
- Array (array)
- Union Find (union-find)

## Companies
- Bloomberg - 8 (taggedByAdmin: false)
- Amazon - 7 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: true)
- Uber - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Spotify - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Nutanix - 2 (taggedByAdmin: false)
- Qualtrics - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach 1: Brute Force

**Intuition**

Because a sequence could start at any number in `nums`, we can exhaust the
entire search space by building as long a sequence as possible from every
number.

**Algorithm**

The brute force algorithm does not do anything clever - it just considers
each number in `nums`, attempting to count as high as possible from that
number using only numbers in `nums`. After it counts too high (i.e.
`currentNum` refers to a number that `nums` does not contain), it records the
length of the sequence if it is larger than the current best. The algorithm
is necessarily optimal because it explores every possibility.

<iframe src="https://leetcode.com/playground/puxLaX5E/shared" frameBorder="0" width="100%" height="500" name="puxLaX5E"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^3)$$.

    The outer loop runs exactly $$n$$ times, and because `currentNum`
    increments by 1 during each iteration of the `while` loop, it runs in
    $$O(n)$$ time. Then, on each iteration of the `while` loop, an $$O(n)$$
    lookup in the array is performed. Therefore, this brute force algorithm
    is really three nested $$O(n)$$ loops, which compound multiplicatively to a
    cubic runtime.

* Space complexity : $$O(1)$$.

    The brute force algorithm only allocates a handful of integers, so it uses constant
    additional space.

<br />

---

#### Approach 2: Sorting

**Intuition**

If we can iterate over the numbers in ascending order, then it will be
easy to find sequences of consecutive numbers. To do so, we can sort the
array.

**Algorithm**

Before we do anything, we check for the base case input of the empty array.
The longest sequence in an empty array is, of course, 0, so we can simply
return that. For all other cases, we sort `nums` and consider each number
after the first (because we need to compare each number to its previous
number). If the current number and the previous are equal, then our current
sequence is neither extended nor broken, so we simply move on to the next
number. If they are unequal, then we must check whether the current number
extends the sequence (i.e. `nums[i] == nums[i-1] + 1`). If it does, then we
add to our current count and continue. Otherwise, the sequence is broken, so
we record our current sequence and reset it to 1 (to include the number that
broke the sequence). It is possible that the last element of `nums` is part
of the longest sequence, so we return the maximum of the current sequence and
the longest one.

![Sorting Example](../Figures/128/sorting.png)
{:align="center"}

Here, an example array is sorted before the linear scan identifies all consecutive sequences.
The longest sequence is colored in red.

<iframe src="https://leetcode.com/playground/M9Rxw5qk/shared" frameBorder="0" width="100%" height="497" name="M9Rxw5qk"></iframe>

**Complexity Analysis**

* Time complexity : $$O(nlgn)$$.

    The main `for` loop does constant work $$n$$ times, so the algorithm's time
    complexity is dominated by the invocation of `sort`, which will run in
    $$O(nlgn)$$ time for any sensible implementation.

* Space complexity : $$O(1)$$ (or $$O(n)$$).

    For the implementations provided here, the space complexity is constant
    because we sort the input array in place. If we are not allowed to modify
    the input array, we must spend linear space to store a sorted copy.

<br />

---

#### Approach 3: HashSet and Intelligent Sequence Building

**Intuition**

It turns out that our initial brute force solution was on the right track, but missing
a few optimizations necessary to reach $$O(n)$$ time complexity.

**Algorithm**

This optimized algorithm contains only two changes from the brute force
approach: the numbers are stored in a `HashSet` (or `Set`, in Python) to
allow $$O(1)$$ lookups, and we only attempt to build sequences from numbers
that are not already part of a longer sequence. This is accomplished by first
ensuring that the number that would immediately precede the current number in
a sequence is not present, as that number would necessarily be part of a
longer sequence.

<iframe src="https://leetcode.com/playground/KbUGJ84k/shared" frameBorder="0" width="100%" height="497" name="KbUGJ84k"></iframe>


**Complexity Analysis**

* Time complexity : $$O(n)$$.

    Although the time complexity appears to be quadratic due to the `while`
    loop nested within the `for` loop, closer inspection reveals it to be
    linear. Because the `while` loop is reached only when `currentNum` marks
    the beginning of a sequence (i.e. `currentNum-1` is not present in
    `nums`), the `while` loop can only run for $$n$$ iterations throughout the
    entire runtime of the algorithm. This means that despite looking like
    $$O(n \cdot n)$$ complexity, the nested loops actually run in $$O(n + n) = O(n)$$
    time. All other computations occur in constant time, so the overall
    runtime is linear.

* Space complexity : $$O(n)$$.

    In order to set up $$O(1)$$ containment lookups, we allocate linear space
    for a hash table to store the $$O(n)$$ numbers in `nums`. Other than that,
    the space complexity is identical to that of the brute force solution.

## Accepted Submission (python3)
```python3
class Solution:
    def longestConsecutiveBoundaries(self, nums: List[int]) -> int:
        r = 0
        map = {}
        for v in nums:
            if v in map:
                continue
            left = map[v - 1] if v - 1 in map else 0
            right = map[v + 1] if v + 1 in map else 0
            now = left + right + 1
            if now > r:
                r = now
            map[v] = now
            map[v - left] = now
            map[v + right] = now
        return r

    def longestConsecutive(self, nums: List[int]) -> int:
        r = 0
        nset = set(nums)
        for v in nums:
            if v - 1 in nset:
                continue
            sum = 0
            while v in nset:
                v += 1
                sum += 1
            if sum > r:
                r = sum
        return r

```

## Top Discussions
### My really simple Java O(n) solution - Accepted
- Author: dchen0215
- Creation Date: Thu Dec 18 2014 13:05:14 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 13:22:35 GMT+0800 (Singapore Standard Time)

<p>
We will use HashMap. The key thing is to keep track of the sequence length and store that in the boundary points of the sequence. For example, as a result, for sequence {1, 2, 3, 4, 5}, map.get(1) and map.get(5) should both return 5.

Whenever a new element **n** is inserted into the map, do two things:

 1. See if **n - 1** and **n + 1** exist in the map, and if so, it means there is an existing sequence next to **n**. Variables **left** and **right** will be the length of those two sequences, while **0** means there is no sequence and **n** will be the boundary point later. Store **(left + right + 1)** as the associated value to key **n** into the map.
 2. Use **left** and **right** to locate the other end of the sequences to the left and right of **n** respectively, and replace the value with the new length.


Everything inside the **for** loop is O(1) so the total time is O(n). Please comment if you see something wrong. Thanks.

    public int longestConsecutive(int[] num) {
        int res = 0;
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int n : num) {
            if (!map.containsKey(n)) {
                int left = (map.containsKey(n - 1)) ? map.get(n - 1) : 0;
                int right = (map.containsKey(n + 1)) ? map.get(n + 1) : 0;
                // sum: length of the sequence n is in
                int sum = left + right + 1;
                map.put(n, sum);
                
                // keep track of the max length 
                res = Math.max(res, sum);
                
                // extend the length to the boundary(s)
                // of the sequence
                // will do nothing if n has no neighbors
                map.put(n - left, sum);
                map.put(n + right, sum);
            }
            else {
                // duplicates
                continue;
            }
        }
        return res;
    }
</p>


### Simple O(n) with Explanation - Just walk each streak
- Author: StefanPochmann
- Creation Date: Thu Jun 04 2015 03:34:04 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 23:15:37 GMT+0800 (Singapore Standard Time)

<p>
First turn the input into a *set* of numbers. That takes O(n) and then we can ask in O(1) whether we have a certain number.

Then go through the numbers. If the number x is the start of a streak (i.e., x-1 is not in the set), then test y = x+1, x+2, x+3, ... and stop at the first number y *not* in the set. The length of the streak is then simply y-x and we update our global best with that. Since we check each streak only once, this is overall O(n). This ran in 44 ms on the OJ, one of the fastest Python submissions.

    def longestConsecutive(self, nums):
        nums = set(nums)
        best = 0
        for x in nums:
            if x - 1 not in nums:
                y = x + 1
                while y in nums:
                    y += 1
                best = max(best, y - x)
        return best
</p>


### Possibly shortest cpp solution, only 6 lines.
- Author: mzchen
- Creation Date: Fri Nov 21 2014 17:33:33 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 13:41:17 GMT+0800 (Singapore Standard Time)

<p>
use a hash map to store boundary information of consecutive sequence for each element; there are 4 cases when a new element `i` reached: 

1) neither `i+1` nor `i-1` has been seen: `m[i]=1`;

2) both `i+1` and `i-1` have been seen: extend `m[i+m[i+1]]` and `m[i-m[i-1]]` to each other;

3) only `i+1` has been seen: extend `m[i+m[i+1]]` and `m[i]` to each other;

4) only `i-1` has been seen: extend `m[i-m[i-1]]` and `m[i]` to each other.


```
int longestConsecutive(vector<int> &num) {
    unordered_map<int, int> m;
    int r = 0;
    for (int i : num) {
        if (m[i]) continue;
        r = max(r, m[i] = m[i + m[i + 1]] = m[i - m[i - 1]] = m[i + 1] + m[i - 1] + 1);
    }
    return r;
}
```
</p>


