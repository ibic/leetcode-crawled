---
title: "Valid Anagram"
weight: 226
#id: "valid-anagram"
---
## Description
<div class="description">
<p>Given two strings <em>s</em> and <em>t&nbsp;</em>, write a function to determine if <em>t</em> is an anagram of <em>s</em>.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> <em>s</em> = &quot;anagram&quot;, <em>t</em> = &quot;nagaram&quot;
<b>Output:</b> true
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> <em>s</em> = &quot;rat&quot;, <em>t</em> = &quot;car&quot;
<b>Output: </b>false
</pre>

<p><strong>Note:</strong><br />
You may assume the string contains only lowercase alphabets.</p>

<p><strong>Follow up:</strong><br />
What if the inputs contain unicode characters? How would you adapt your solution to such case?</p>

</div>

## Tags
- Hash Table (hash-table)
- Sort (sort)

## Companies
- Bloomberg - 16 (taggedByAdmin: false)
- Facebook - 7 (taggedByAdmin: false)
- Amazon - 6 (taggedByAdmin: true)
- Microsoft - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Docusign - 2 (taggedByAdmin: false)
- Oracle - 5 (taggedByAdmin: false)
- Zulily - 3 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- Yahoo - 4 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- Morgan Stanley - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- Yelp - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach #1 (Sorting) [Accepted]

**Algorithm**

An anagram is produced by rearranging the letters of $$s$$ into $$t$$. Therefore, if $$t$$ is an anagram of $$s$$, sorting both strings will result in two identical strings. Furthermore, if $$s$$ and $$t$$ have different lengths, $$t$$ must not be an anagram of $$s$$ and we can return early.

```java
public boolean isAnagram(String s, String t) {
    if (s.length() != t.length()) {
        return false;
    }
    char[] str1 = s.toCharArray();
    char[] str2 = t.toCharArray();
    Arrays.sort(str1);
    Arrays.sort(str2);
    return Arrays.equals(str1, str2);
}
```

**Complexity analysis**

* Time complexity : $$O(n \log n)$$.
Assume that $$n$$ is the length of $$s$$, sorting costs $$O(n \log n)$$ and comparing two strings costs $$O(n)$$. Sorting time dominates and the overall time complexity is $$O(n \log n)$$.

* Space complexity : $$O(1)$$.
Space depends on the sorting implementation which, usually, costs $$O(1)$$ auxiliary space if `heapsort` is used. Note that in Java, `toCharArray()` makes a copy of the string so it costs $$O(n)$$ extra space, but we ignore this for complexity analysis because:

    * It is a language dependent detail.
    * It depends on how the function is designed. For example, the function parameter types can be changed to `char[]`.

---
#### Approach #2 (Hash Table) [Accepted]

**Algorithm**

To examine if $$t$$ is a rearrangement of $$s$$, we can count occurrences of each letter in the two strings and compare them. Since both $$s$$ and $$t$$ contain only letters from $$a-z$$, a simple counter table of size 26 is suffice.

Do we need *two* counter tables for comparison? Actually no, because we could increment the counter for each letter in $$s$$ and decrement the counter for each letter in $$t$$, then check if the counter reaches back to zero.

```java
public boolean isAnagram(String s, String t) {
    if (s.length() != t.length()) {
        return false;
    }
    int[] counter = new int[26];
    for (int i = 0; i < s.length(); i++) {
        counter[s.charAt(i) - 'a']++;
        counter[t.charAt(i) - 'a']--;
    }
    for (int count : counter) {
        if (count != 0) {
            return false;
        }
    }
    return true;
}
```

Or we could first increment the counter for $$s$$, then decrement the counter for $$t$$. If at any point the counter drops below zero, we know that $$t$$ contains an extra letter not in $$s$$ and return false immediately.

```java
public boolean isAnagram(String s, String t) {
    if (s.length() != t.length()) {
        return false;
    }
    int[] table = new int[26];
    for (int i = 0; i < s.length(); i++) {
        table[s.charAt(i) - 'a']++;
    }
    for (int i = 0; i < t.length(); i++) {
        table[t.charAt(i) - 'a']--;
        if (table[t.charAt(i) - 'a'] < 0) {
            return false;
        }
    }
    return true;
}
```

**Complexity analysis**

* Time complexity : $$O(n)$$.
Time complexity is $$O(n)$$ because accessing the counter table is a constant time operation.

* Space complexity : $$O(1)$$.
Although we do use extra space, the space complexity is $$O(1)$$ because the table's size stays constant no matter how large $$n$$ is.

**Follow up**

What if the inputs contain unicode characters? How would you adapt your solution to such case?

**Answer**

Use a hash table instead of a fixed size counter. Imagine allocating a large size array to fit the entire range of unicode characters, which could go up to [more than 1 million](http://stackoverflow.com/a/5928054/490463). A hash table is a more generic solution and could adapt to any range of characters.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted Java O(n) solution in 5 lines
- Author: xhadfasd
- Creation Date: Sat Aug 01 2015 15:57:50 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 08:04:14 GMT+0800 (Singapore Standard Time)

<p>
The idea is simple. It creates a size 26 int arrays as buckets for each letter in alphabet. It increments the bucket value with String s and decrement with string t. So if they are anagrams, all buckets should remain with initial value which is zero. So just checking that and return

    public class Solution {
        public boolean isAnagram(String s, String t) {
            int[] alphabet = new int[26];
            for (int i = 0; i < s.length(); i++) alphabet[s.charAt(i) - 'a']++;
            for (int i = 0; i < t.length(); i++) alphabet[t.charAt(i) - 'a']--;
            for (int i : alphabet) if (i != 0) return false;
            return true;
        }
    }
</p>


### 2 C++ Solutions with Explanations
- Author: jianchao-li
- Creation Date: Sat Aug 01 2015 14:06:32 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 21:48:53 GMT+0800 (Singapore Standard Time)

<p>
----------
**Hash Table**

This idea uses a hash table to record the times of appearances of each letter in the two strings `s` and `t`. For each letter in `s`, it increases the counter by `1` while for each letter in `t`, it decreases the counter by `1`. Finally, all the counters will be `0` if they two are anagrams of each other.

The first implementation uses the built-in `unordered_map` and takes 36 ms.

    class Solution {
    public:
        bool isAnagram(string s, string t) {
            if (s.length() != t.length()) return false;
            int n = s.length();
            unordered_map<char, int> counts;
            for (int i = 0; i < n; i++) {
                counts[s[i]]++;
                counts[t[i]]--;
            }
            for (auto count : counts)
                if (count.second) return false;
            return true;
        }
    };

Since the problem statement says that "the string contains only lowercase alphabets", we can simply use an array to simulate the `unordered_map` and speed up the code. The following implementation takes 12 ms.

    class Solution {
    public:
        bool isAnagram(string s, string t) {
            if (s.length() != t.length()) return false;
            int n = s.length();
            int counts[26] = {0};
            for (int i = 0; i < n; i++) { 
                counts[s[i] - 'a']++;
                counts[t[i] - 'a']--;
            }
            for (int i = 0; i < 26; i++)
                if (counts[i]) return false;
            return true;
        }
    };

----------
**Sorting**

For two anagrams, once they are sorted in a fixed order, they will become the same. This code is much shorter (this idea can be done in just 1 line using Python as [here][1]). However, it takes much longer time --- 76 ms in C++.

    class Solution {
    public:
        bool isAnagram(string s, string t) { 
            sort(s.begin(), s.end());
            sort(t.begin(), t.end());
            return s == t; 
        }
    };

  [1]: https://leetcode.com/discuss/49372/python-1-line-solution-88ms
</p>


### Python solutions (sort and dictionary).
- Author: OldCodingFarmer
- Creation Date: Thu Aug 06 2015 23:35:29 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 14:55:58 GMT+0800 (Singapore Standard Time)

<p>
        
    def isAnagram1(self, s, t):
        dic1, dic2 = {}, {}
        for item in s:
            dic1[item] = dic1.get(item, 0) + 1
        for item in t:
            dic2[item] = dic2.get(item, 0) + 1
        return dic1 == dic2
        
    def isAnagram2(self, s, t):
        dic1, dic2 = [0]*26, [0]*26
        for item in s:
            dic1[ord(item)-ord('a')] += 1
        for item in t:
            dic2[ord(item)-ord('a')] += 1
        return dic1 == dic2
        
    def isAnagram3(self, s, t):
        return sorted(s) == sorted(t)
</p>


