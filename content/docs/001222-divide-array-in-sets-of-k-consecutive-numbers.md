---
title: "Divide Array in Sets of K Consecutive Numbers"
weight: 1222
#id: "divide-array-in-sets-of-k-consecutive-numbers"
---
## Description
<div class="description">
<p>Given an array of integers&nbsp;<code>nums</code>&nbsp;and a positive integer&nbsp;<code>k</code>, find whether it&#39;s possible to divide this array into&nbsp;sets of <code>k</code> consecutive numbers<br />
Return&nbsp;<code>True</code>&nbsp;if its possible<strong>&nbsp;</strong>otherwise&nbsp;return&nbsp;<code>False</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,3,4,4,5,6], k = 4
<strong>Output:</strong> true
<strong>Explanation:</strong> Array can be divided into [1,2,3,4] and [3,4,5,6].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,2,1,2,3,4,3,4,5,9,10,11], k = 3
<strong>Output:</strong> true
<strong>Explanation:</strong> Array can be divided into [1,2,3] , [2,3,4] , [3,4,5] and [9,10,11].
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [3,3,2,2,1,1], k = 3
<strong>Output:</strong> true
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4], k = 3
<strong>Output:</strong> false
<strong>Explanation:</strong> Each array should be divided in subarrays of size 3.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 10^9</code></li>
	<li><code>1 &lt;= k &lt;= nums.length</code></li>
</ul>
<strong>Note:</strong> This question is the same as&nbsp;846:&nbsp;<a href="https://leetcode.com/problems/hand-of-straights/">https://leetcode.com/problems/hand-of-straights/</a>
</div>

## Tags
- Array (array)
- Greedy (greedy)

## Companies
- Google - 7 (taggedByAdmin: true)
- Coupang - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Greedy
- Author: PhoenixDD
- Creation Date: Sun Dec 22 2019 12:01:18 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 23 2019 05:24:26 GMT+0800 (Singapore Standard Time)

<p>
**Observation**

We can use a greedy approach and start with the smallest number and see if the numbers from that number + k exist and then keep removing them from the numbers we have, if there is a case where it\'s not possible then we return false.

**Solution**
```c++
class Solution {
public:
    bool isPossibleDivide(vector<int>& nums, int k) 
    {
        if(nums.size()%k!=0)
            return false;
        map<int,int> count;
        map<int,int>::iterator it;
        int freq;
        for(int &i:nums)			//Store the count of all numbers sorted.
            count[i]++;
        for(it=count.begin();it!=count.end();it++)	//Start with the smallest number.
            if(it->second)		//If the count of smallest integer is non 0 check if next k numbers exist and have atleast same frequency.
            {
                freq=it->second;
                for(int i=0;i<k;i++)				//Checks for the next k-1 numbers.
                    if(count[it->first+i]<freq) //We are unable to find ith consecutive number to the smallest(starting number) with atleast same frequency.
                        return false;
                    else
                        count[it->first+i]-=freq;       //Reduce the count of the numbers used.
            }
        return true;
    }
};
```
**Complexity**
Space: `O(n)`. Since we store the count of all the numbers.
Time: `O(nlogn)`. Since we use map which is sorted and each lookup is `O(logn)`.
</p>


### [Java/C++/Python] Exactly Same as 846. Hand of Straights
- Author: lee215
- Creation Date: Sun Jan 05 2020 01:01:37 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 22 2020 14:15:14 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Exactly same as [846. Hand of Straights](https://leetcode.com/problems/hand-of-straights/discuss/135598/C++JavaPython-O(MlogM)-Complexity)
<br>

# **Solution 1**
1. Count number of different cards to a map `c`
2. Loop from the smallest card number.
3. Everytime we meet a new card `i`, we cut off `i` - `i + k - 1` from the counter.
<br>

# **Complexity**:
Time `O(MlogM + MK)`, where `M` is the number of different cards.
In Cpp and Java it\'s O(NlogM), which can also be improved.
<br>

**Java:**
```java
    public boolean isPossibleDivide(int[] A, int k) {
        Map<Integer, Integer> c = new TreeMap<>();
        for (int i : A) c.put(i, c.getOrDefault(i, 0)+1);
        for (int it : c.keySet())
            if (c.get(it) > 0)
                for (int i = k - 1; i >= 0; --i) {
                    if (c.getOrDefault(it + i, 0) < c.get(it)) return false;
                    c.put(it + i, c.get(it + i) - c.get(it));
                }
        return true;
    }
```

**C++:**
```cpp
    bool isPossibleDivide(vector<int> A, int k) {
        map<int, int> c;
        for (int i : A) c[i]++;
        for (auto it : c)
            if (c[it.first] > 0)
                for (int i = k - 1; i >= 0; --i)
                    if ((c[it.first + i] -= c[it.first]) < 0)
                        return false;
        return true;
    }
```
**Python:**
```py
    def isPossibleDivide(self, A, k):
        c = collections.Counter(A)
        for i in sorted(c):
            if c[i] > 0:
                for j in range(k)[::-1]:
                    c[i + j] -= c[i]
                    if c[i + j] < 0:
                        return False
        return True
```

# **Follow Up**
We just got lucky AC solution. Because `k <= 10000`.
What if k is huge, should we cut off card on by one?
<br>

# **Solution 2**
1. Count number of different cards to a map `c`
2. `Cur` represent current open straight groups.
3. In a deque `start`, we record the number of opened a straight group.
4. Loop from the smallest card number.

For example, A = [1,2,3,2,3,4], k = 3
We meet one 1:
    opened = 0, we open a new straight groups starting at 1, push (1,1) to `start`.
We meet two 2:
    opened = 1, we need open another straight groups starting at 1, push (2,1) to `start`.
We meet two 3:
    opened = 2, it match current opened groups.
    We open one group at 1, now we close it. opened = opened - 1 = 1
We meet one 4:
    opened = 1, it match current opened groups.
    We open one group at 2, now we close it. opened = opened - 1 = 0

5. return if no more open groups.
<br>

# **Complexity**
`O(N+MlogM)`, where `M` is the number of different cards.
Because I count and sort cards.
In Cpp and Java it\'s O(NlogM), which can also be improved.
<br>

**Java**
```java
    public boolean isPossibleDivide(int[] A, int k) {
        Map<Integer, Integer> c = new TreeMap<>();
        for (int i : A) c.put(i, c.getOrDefault(i, 0)+1);
        Queue<Integer> start = new LinkedList<>();
        int last_checked = -1, opened = 0;
        for (int i : c.keySet()) {
            if (opened > 0 && i > last_checked + 1 || opened > c.get(i)) return false;
            start.add(c.get(i) - opened);
            last_checked = i; opened = c.get(i);
            if (start.size() == k) opened -= start.remove();
        }
        return opened == 0;
    }
```

**C++:**
```cpp
    bool isPossibleDivide(vector<int> A, int k) {
        map<int, int> c;
        for (int i : A) c[i]++;
        queue<int> start;
        int last_checked = -1, opened = 0;
        for (auto it : c) {
            int i = it.first;
            if (opened > 0 && i > last_checked + 1 || opened > c[i]) return false;
            start.push(c[i] - opened);
            last_checked = i, opened = c[i];
            if (start.size() == k) {
                opened -= start.front();
                start.pop();
            }
        }
        return opened == 0;
    }
```

**Python:**
```py
    def isPossibleDivide(self, A, k):
        c = collections.Counter(A)
        start = collections.deque()
        last_checked, opened = -1, 0
        for i in sorted(c):
            if opened > c[i] or opened > 0 and i > last_checked + 1: return False
            start.append(c[i] - opened)
            last_checked, opened = i, c[i]
            if len(start) == k: opened -= start.popleft()
        return opened == 0
```
</p>


### Python Counter Solution
- Author: Luolingwei
- Creation Date: Sun Dec 22 2019 12:05:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 22 2019 12:05:47 GMT+0800 (Singapore Standard Time)

<p>
```python
import collections
class Solution:
    def isPossibleDivide(self, nums: List[int], k: int) -> bool:
        count=collections.Counter(nums)
        keys=sorted(count.keys())
        for n in keys:
            if count[n]>0:
                minus=count[n]
                for i in range(n,n+k):
                    if count[i]<minus:
                        return False
                    count[i]-=minus
        return True
```
</p>


