---
title: "Partition to K Equal Sum Subsets"
weight: 630
#id: "partition-to-k-equal-sum-subsets"
---
## Description
<div class="description">
<p>Given an array of integers <code>nums</code> and a positive integer <code>k</code>, find whether it&#39;s possible to divide this array into <code>k</code> non-empty subsets whose sums are all equal.</p>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> nums = [4, 3, 2, 3, 5, 2, 1], k = 4
<b>Output:</b> True
<b>Explanation:</b> It&#39;s possible to divide it into 4 subsets (5), (1, 4), (2,3), (2,3) with equal sums.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ul>
	<li><code>1 &lt;= k &lt;= len(nums) &lt;= 16</code>.</li>
	<li><code>0 &lt; nums[i] &lt; 10000</code>.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Recursion (recursion)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- LinkedIn - 9 (taggedByAdmin: true)
- VMware - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Houzz - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Search by Constructing Subset Sums [Accepted]

**Intuition**

As even when `k = 2`, the problem is a "Subset Sum" problem which is known to be NP-hard, (and because the given input limits are low,) our solution will focus on exhaustive search.

A natural approach is to simulate the `k` groups (disjoint subsets of nums).  For each number in `nums`, we'll check whether putting it in the `i`-th group solves the problem.  We can check those possibilities by recursively searching.

**Algorithm**

Firstly, we know that each of the `k` group-sums must be equal to `target = sum(nums) / k`.  (If this quantity is not an integer, the task is impossible.)

For each number in `nums`, we could add it into one of `k` group-sums, as long as the group's sum would not exceed the `target`.  For each of these choices, we recursively search with one less number to consider in `nums`.  If we placed every number successfully, then our search was successful.

One important speedup is that we can ensure all the 0 values of each group occur at the end of the array `groups`, by enforcing `if (groups[i] == 0) break;`.  This greatly reduces repeated work - for example, in the first run of search, we will make only 1 recursive call, instead of `k`.  Actually, we could do better by skipping any repeated values of groups[i], but it isn't necessary.

Another speedup is we could sort the array `nums`, so that we try to place the largest elements first.  When the answer is true and involves subsets with a low size, this method of placing elements will consider these lower size subsets sooner.  We can also handle elements `nums[i] >= target` appropriately.  These tricks are not necessary to solve the problem, but they are presented in the solutions below.

**Python**
```python
class Solution(object):
    def canPartitionKSubsets(self, nums, k):
        target, rem = divmod(sum(nums), k)
        if rem: return False

        def search(groups):
            if not nums: return True
            v = nums.pop()
            for i, group in enumerate(groups):
                if group + v <= target:
                    groups[i] += v
                    if search(groups): return True
                    groups[i] -= v
                if not group: break
            nums.append(v)
            return False

        nums.sort()
        if nums[-1] > target: return False
        while nums and nums[-1] == target:
            nums.pop()
            k -= 1

        return search([0] * k)
```

**Java**
```java
class Solution {
    public boolean search(int[] groups, int row, int[] nums, int target) {
        if (row < 0) return true;
        int v = nums[row--];
        for (int i = 0; i < groups.length; i++) {
            if (groups[i] + v <= target) {
                groups[i] += v;
                if (search(groups, row, nums, target)) return true;
                groups[i] -= v;
            }
            if (groups[i] == 0) break;
        }
        return false;
    }

    public boolean canPartitionKSubsets(int[] nums, int k) {
        int sum = Arrays.stream(nums).sum();
        if (sum % k > 0) return false;
        int target = sum / k;

        Arrays.sort(nums);
        int row = nums.length - 1;
        if (nums[row] > target) return false;
        while (row >= 0 && nums[row] == target) {
            row--;
            k--;
        }
        return search(new int[k], row, nums, target);
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(k^{N-k} k!)$$, where $$N$$ is the length of `nums`, and $$k$$ is as given.  As we skip additional zeroes in `groups`, naively we will make $$O(k!)$$ calls to `search`, then an additional $$O(k^{N-k})$$ calls after every element of `groups` is nonzero.

* Space Complexity: $$O(N)$$, the space used by recursive calls to `search` in our call stack.

---
#### Approach #2: Dynamic Programming on Subsets of Input [Accepted]

**Intuition and Algorithm**

As in *Approach #1*, we investigate methods of exhaustive search, and find `target = sum(nums) / k` in the same way.

Let `used` have the `i`-th bit set if and only if `nums[i]` has already been used.  Our goal is to use `nums` in some order so that placing them into groups in that order will be valid. `search(used, ...)` will answer the question: can we partition unused elements of `nums[i]` appropriately?

This will depend on `todo`, the sum of the remaining unused elements, not crossing multiples of `target` within one number.  If for example our target is `10`, and our elements to be placed in order are `[6, 5, 5, 4]`, this would not work as `6 + 5` "crosses" `10` prematurely.

If we could choose the order, then after placing `5`, our unused elements are `[4, 5, 6]`.  Using `6` would make `todo` go from `15` to `9`, which crosses `10` - something unwanted.  However, we could use `5` since `todo` goes from `15` to `10`; then later we could use `4` and `6` as those placements do not cross.

It turns out the maximum value that can be chosen so as to not cross a multiple of `target`, is `targ = (todo - 1) % target + 1`.  This is essentially `todo % target`, plus `target` if that would be zero.

Now for each unused number that doesn't cross, we'll search on that state, and we'll return `true` if any of those searches are `true`.

Notice that the state `todo` depends only on the state `used`, so when memoizing our search, we only need to make lookups by `used`.

In the solutions below, we present both a top-down dynamic programming solution, and a bottom-up one.  The bottom-up solution uses a different notion of state.

**Python**
```python
class Solution(object):
    def canPartitionKSubsets(self, nums, k):
        target, rem = divmod(sum(nums), k)
        if rem or max(nums) > target: return False

        memo = [None] * (1 << len(nums))
        memo[-1] = True
        def search(used, todo):
            if memo[used] is None:
                targ = (todo - 1) % target + 1
                memo[used] = any(search(used | (1<<i), todo - num)
                                 for i, num in enumerate(nums)
                                 if (used >> i) & 1 == 0 and num <= targ)
            return memo[used]

        return search(0, target * k)
```

**Java**
```java
enum Result { TRUE, FALSE }

class Solution {
    boolean search(int used, int todo, Result[] memo, int[] nums, int target) {
        if (memo[used] == null) {
            memo[used] = Result.FALSE;
            int targ = (todo - 1) % target + 1;
            for (int i = 0; i < nums.length; i++) {
                if ((((used >> i) & 1) == 0) && nums[i] <= targ) {
                    if (search(used | (1<<i), todo - nums[i], memo, nums, target)) {
                        memo[used] = Result.TRUE;
                        break;
                    }
                }
            }
        }
        return memo[used] == Result.TRUE;
    }
    public boolean canPartitionKSubsets(int[] nums, int k) {
        int sum = Arrays.stream(nums).sum();
        if (sum % k > 0) return false;

        Result[] memo = new Result[1 << nums.length];
        memo[(1 << nums.length) - 1] = Result.TRUE;
        return search(0, sum, memo, nums, sum / k);
    }
}
```

*Bottom-Up Variation*

**Python**

```python
class Solution(object):
    def canPartitionKSubsets(self, nums, k):
        nums.sort()
        target, rem = divmod(sum(nums), k)
        if rem or nums[-1] > target: return False

        dp = [False] * (1 << len(nums))
        dp[0] = True
        total = [0] * (1 << len(nums))

        for state in xrange(1 << len(nums)):
            if not dp[state]: continue
            for i, num in enumerate(nums):
                future = state | (1 << i)
                if state != future and not dp[future]:
                    if (num <= target - (total[state] % target)):
                        dp[future] = True
                        total[future] = total[state] + num
                    else:
                        break
        return dp[-1]
```

**Java**
```java
class Solution {
    public boolean canPartitionKSubsets(int[] nums, int k) {
        int N = nums.length;
        Arrays.sort(nums);
        int sum = Arrays.stream(nums).sum();
        int target = sum / k;
        if (sum % k > 0 || nums[N - 1] > target) return false;

        boolean[] dp = new boolean[1 << N];
        dp[0] = true;
        int[] total = new int[1 << N];

        for (int state = 0; state < (1 << N); state++) {
            if (!dp[state]) continue;
            for (int i = 0; i < N; i++) {
                int future = state | (1 << i);
                if (state != future && !dp[future]) {
                    if (nums[i] <= target - (total[state] % target)) {
                        dp[future] = true;
                        total[future] = total[state] + nums[i];
                    } else {
                        break;
                    }
                }
            }
        }
        return dp[(1 << N) - 1];
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(N * 2^N)$$, where $$N$$ is the length of `nums`.  There are $$2^N$$ states of `used` (or `state` in our bottom-up variant), and each state performs `O(N)` work searching through `nums`.

* Space Complexity: $$O(2^N)$$, the space used by `memo` (or `dp`, `total` in our bottom-up variant).

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Backtracking Thinking Process
- Author: GraceMeng
- Creation Date: Thu Oct 11 2018 10:04:04 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 18 2019 20:42:29 GMT+0800 (Singapore Standard Time)

<p>
>**canPartitionKSubsets()** is true when there are exactly k subsets with equal subset sum `sum / k`, and each element are only used once.

- comparison between `curSubsetSum` and `targetSubsetSum` indicates whether we find a valid subset.
- `k` restricts there are k subsets exactly.

> We can take it as a nested recursion. The graph below shows the control flow (not accurate):
```
Outer recursion on k subsets:
Base case: k == 0
Recursive case: k > 0 
				// Inner recursion on individual subset
				Base case: curSubsetSum == targetSubsetSum (return to outer recursion)
				Recursive case: curSubsetSum < targetSubsetSum
```
> Since the order of numbers within a subset doesn\'t matter, we add `nextIndexToCheck` for the inner recursion to avoid duplicate calculations.

****
```
class Solution {
    public boolean canPartitionKSubsets(int[] nums, int k) {
        int sum = 0, maxNum = 0;
        for (int num : nums) {
            sum += num;
            maxNum = Math.max(maxNum, num);
        }
        if (sum % k != 0 || maxNum > sum / k) {
            return false;
        }
        return canPartitionKSubsetsFrom(nums, k, new boolean[nums.length], sum / k, 0, 0);
    }
    
    private boolean canPartitionKSubsetsFrom(int[] nums, 
                                             int k,
                                             boolean[] visited, 
                                             int targetSubsetSum, 
                                             int curSubsetSum, 
                                             int nextIndexToCheck) {
        if (k == 0) {
            return true;
        }
        
        if (curSubsetSum == targetSubsetSum) {
            return canPartitionKSubsetsFrom(nums, 
                                            k - 1,
                                            visited,
                                            targetSubsetSum,
                                            0,
                                            0);
        }
        
        for (int i = nextIndexToCheck; i < nums.length; i++) {
            if (!visited[i] && curSubsetSum + nums[i] <= targetSubsetSum) {
                visited[i] = true;
                if (canPartitionKSubsetsFrom(nums, 
                                             k,
                                             visited,
                                             targetSubsetSum,
                                             curSubsetSum + nums[i],
                                             i + 1)) {
                    return true;
                }
                visited[i] = false;
            }
        }
        
        return false;
    }
}
```

**(\u4EBA \u2022\u0348\u1D17\u2022\u0348)** Thanks for voting!
</p>


### [Java/C++]Straightforward dfs solution
- Author: caihao0727mail
- Creation Date: Sun Oct 15 2017 14:48:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 02:07:12 GMT+0800 (Singapore Standard Time)

<p>
**Update:** This question has been changed after the contest. It added the special restriction ```0 < nums[i] < 10000```. My solution here is without that consideration.

Assume ```sum``` is the sum of ```nums[]``` . The dfs process is to find a subset of  ```nums[]``` which sum equals to ```sum/k```. We use an array ```visited[]``` to record which element in ```nums[]``` is used. Each time when we get a ```cur_sum = sum/k```, we will start from position ```0``` in ```nums[]``` to look up the elements that are not used yet and find another ```cur_sum = sum/k```.

An corner case is when ```sum = 0```, my method is to use ```cur_num``` to record the number of elements in the current subset. Only if   ```cur_sum = sum/k && cur_num >0```, we can start another look up process.

Some test cases may need to be added in:
```nums = {-1,1,0,0}, k = 4```
```nums = {-1,1}, k = 1```
```nums = {-1,1}, k = 2```
```nums = {-1,1,0}, k = 2```
...

Java version:
```
    public boolean canPartitionKSubsets(int[] nums, int k) {
        int sum = 0;
        for(int num:nums)sum += num;
        if(k <= 0 || sum%k != 0)return false;
        int[] visited = new int[nums.length];
        return canPartition(nums, visited, 0, k, 0, 0, sum/k);
    }
    
    public boolean canPartition(int[] nums, int[] visited, int start_index, int k, int cur_sum, int cur_num, int target){
        if(k==1)return true;
        if(cur_sum == target && cur_num>0)return canPartition(nums, visited, 0, k-1, 0, 0, target);
        for(int i = start_index; i<nums.length; i++){
            if(visited[i] == 0){
                visited[i] = 1;
                if(canPartition(nums, visited, i+1, k, cur_sum + nums[i], cur_num++, target))return true;
                visited[i] = 0;
            }
        }
        return false;
    }
```

C++ version:
```
    bool canPartitionKSubsets(vector<int>& nums, int k) {
        int sum = 0;
        for(int num:nums)sum+=num;
        if(k <= 0 || sum%k != 0)return false;
        vector<int> visited(nums.size(), 0);
        return canPartition(nums, visited, 0, k, 0, 0, sum/k);
    }
    
    bool canPartition(vector<int>& nums, vector<int>& visited, int start_index, int k, int cur_sum, int cur_num, int target){
        if(k==1)return true;
        if(cur_sum == target && cur_num >0 )return canPartition(nums, visited, 0, k-1, 0, 0, target);
        for(int i = start_index; i<nums.size(); i++){
            if(!visited[i]){
                visited[i] = 1;
                if(canPartition(nums, visited, i+1, k, cur_sum + nums[i], cur_num++, target))return true;
                visited[i] = 0;
            }
        }
        return false;
    }
```
</p>


### DP with Bit Masking Solution :- Best for Interviews
- Author: maverick009
- Creation Date: Mon Jul 15 2019 16:54:47 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 22 2019 17:48:22 GMT+0800 (Singapore Standard Time)

<p>
Many Posts have good solution using Backtracking and DFS. But the optimal solution to this problem using DP with Bit Masking.
Before diving into solution, Let\'s first try to understand what Bitmask means. Mask in Bitmask means hiding something. Bitmask is nothing but a binary number that represents something. Let\'s take an example. 

Consider the set A {1,2,3,4,5}. We can represent any subset of A using a bitmask of length 5, with an assumption that if ith (0<=i<=4) bit is set then it means ith element is present in subset. So the bitmask 01010 represents the subset {2,4}. 

Now the benefit of using bitmask. We can set the ith bit, unset the ith bit, check if ith bit is set in just one step each. Let\'s say the bitmask, b = 01010.

Set the ith bit: b(1<<i). Let i=0, so,
(1<<i) = 00001
01010 | 00001 = 01011

So now the subset includes the 0th element also, so the subset is {1,2,4}.

Unset the ith bit: b & !(1<<i). Let i=1, so,
(1<<i) = 00010
!(1<<i) = 11101
01010 & 11101 = 01000

Now the subset does not include the 1st element, so the subset is {4}.

Check if ith bit is set: b&(1<<i), doing this operation, if ith bit is set, we get a non zero integer otherwise, we get zero. Let i = 3
(1<<i) = 01000
01010 & 01000 = 01000

Clearly the result is non-zero, so that means 3rd element is present in the subset.

Now coming to our problem, we can represent each number and sum in binary.
We\'ll use dynamic programming to find whether the array can be partitioned into k subsets of equal sum. For this, we create two arrays of 
size = power set of array elements. why?

because, we need to consider all sum subsets.
dp[i] indicates whether array of length i can partitioned into k subsets of equal sum. Using this technique, the last index of this dp array
will tell whether the whole array can be partitioned into k subsets of equal sum.

total[i] stores the sum of subset with sum less than equal to target sum (total sum/k why? because we need to split array into k subset).
```
public boolean canPartitionKSubsets(int[] nums, int k) {
		if(nums == null || nums.length == 0)
			return false;
		
		int n = nums.length;
		//result array
		boolean dp[] = new boolean[1<<n];
		int total[] = new int[1<<n];
		dp[0] = true;
		
		int sum = 0;
		for(int num : nums)
			sum += num;
		Arrays.sort(nums);
		
		if(sum%k != 0) 
			return false;
		sum /= k;
		if(nums[n-1] > sum)
			return false;
		// Loop over power set
		for(int i = 0;i < (1<<n);i++) {
			if(dp[i]) {
				// Loop over each element to find subset
				for(int j = 0;j < n;j++) {
					// set the jth bit 
					int temp = i | (1 << j);
					if(temp != i) {
						// if total sum is less than target store in dp and total array
						if(nums[j] <= (sum- (total[i]%sum))) {
							dp[temp] = true;
							total[temp] = nums[j] + total[i];
						} else
							break;
					}
				}
			}
		}
		return dp[(1<<n) - 1];
	}
```
Time Complexity : 0(N*2^N)
Space COmplexity: 0(2^N)

Good Reads for DP with Masking:
[Codeforces](http://codeforces.com/blog/entry/337)
[hackerEarth](http://www.hackerearth.com/practice/algorithms/dynamic-programming/bit-masking/tutorial/)
[GeeksforGeeks](http://www.geeksforgeeks.org/partition-set-k-subsets-equal-sum/)
</p>


