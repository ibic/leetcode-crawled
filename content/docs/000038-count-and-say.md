---
title: "Count and Say"
weight: 38
#id: "count-and-say"
---
## Description
<div class="description">
<p>The count-and-say sequence is the sequence of integers with the first five terms as following:</p>

<pre>
1.     1
2.     11
3.     21
4.     1211
5.     111221
</pre>

<p><code>1</code> is read off as <code>&quot;one 1&quot;</code> or <code>11</code>.<br />
<code>11</code> is read off as <code>&quot;two 1s&quot;</code> or <code>21</code>.<br />
<code>21</code> is read off as <code>&quot;one 2</code>, then <code>one 1&quot;</code> or <code>1211</code>.</p>

<p>Given an integer <i>n</i>&nbsp;where 1 &le; <em>n</em> &le; 30, generate the <i>n</i><sup>th</sup> term of the count-and-say sequence. You can do so recursively, in other words from the previous member&nbsp;read off the digits, counting the number of digits in groups of the same digit.</p>

<p>Note: Each term of the sequence of integers will be represented as a string.</p>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> 1
<b>Output:</b> &quot;1&quot;
<b>Explanation:</b> This is the base case.
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> 4
<b>Output:</b> &quot;1211&quot;
<b>Explanation:</b> For n = 3 the term was &quot;21&quot; in which we have two groups &quot;2&quot; and &quot;1&quot;, &quot;2&quot; can be read as &quot;12&quot; which means frequency = 1 and value = 2, the same way &quot;1&quot; is read as &quot;11&quot;, so the answer is the concatenation of &quot;12&quot; and &quot;11&quot; which is &quot;1211&quot;.
</pre>

</div>

## Tags
- String (string)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: true)
- Apple - 3 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Epic Systems - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Overview

First of all, we would like to apologize to our audiences that the description of the problem is definitely not crystal clear, as many people have raised the issue in the discussion forum.

The problem should have been rather easy, as it is labeled, should it be stated clearly. That being said, let us consider the _"mysterious"_ nature of the problem description as part of the challenge.

>Imagine in a more challenging scenario, one might be given just a sequence of numbers without any explication, and one is asked to produce the next numbers, which would require one to figure out the hidden pattern about the generation of the sequence.

Now, the problem becomes more intriguing. And maybe some of you might want to pause for a moment to figure out the puzzle first before proceeding to the clarification. 

**Sequence Puzzle**

Before we proceed to the solutions, in this section, we would like to rephrase the problem.

Given two adjacent sequences of number, $$[S_{n}, S_{n+1}]$$, there exists a pattern that one can produce the sequence $$S_{n+1}$$ from its previous sequence $$S_{n}$$.

>More specifically, one can consider the sequence $$S_{n+1}$$ as a sort of **_summary_** to its previous sequence $$S_{n}$$, _i.e._ $$S_{n+1}$$ contains a list of pairs as $$|\text{count}, \text{digit}|$$ which **_encodes_** all the information about its previous sequence $$S_{n}$$.

Let us take the sequence $$S_{4} = 1211$$ as an example, from left to right, we then can divide the sequence into three sub-groups where each sub-group contains a list of identical and adjacent digit, _i.e._ $$S_{4} = \{1\}\{2\}\{11\}$$ , as shown in the following:

![pic](../Figures/38/group.png)

We then _count_ the number of digits in each sub-group, and then output the summary in the format of $$|\text{count}, \text{digit}|$$. As the end, we would obtain the exact sequence of $$S_5$$.

![pic](../Figures/38/demo.png)

With the generated sequence $$S_5$$, we then _**recursively**_ apply the above rule to generate the next sequence.

Now that the description of the problem is clear, one might dismiss it as yet another strange and artificial problem to solve. Well, it is not true in this case.

Actually, we could consider this problem as a **_naive compression algorithm_** for a sequence of numbers. Instead of storing repetitive adjacent digits as they are, we could summarize them a bit with the method presented in the problem, which could save us some space as long as there are indeed repetitive occurring patterns.
<br/>
<br/>


---
#### Approach 1: Sliding Window

**Intuition**

Now that the problem has been clarified, the solution should be intuitive.

Following the rule as we described above, in order to generate the next sequence, we could scan the current sequence with a sort of _**sliding window**_ which would hold the identical and adjacent digits. With the sliding window, we would divide the original sequence into a list of sub-sequences. We then count the number of digits within each sub-sequence and output the summary as pairs of $$|\text{count}, \text{digit}|$$.

**Algorithm**

Here we define a function `nextSequence()` to generate a following sequence from a previous sequence, and we _recursively_ call this function to get the desired sequence that is located at a specific index.

- Within the function, we scan the sequence with two contextual variables: `prevDigit` and `digitCnt` which refers to respectively the digit that we are expecting in the sub-sequence and the number of occurrence of the digit in the sub-sequence.
<br/>
- At the end of each sub-sequence, we append the summary to the result and then we reset the above two contextual variables for the next sub-sequence.
<br/>
- Note that, we use an artificial delimiter in the sequence to facilitate the iteration.


<iframe src="https://leetcode.com/playground/pa9GaRBL/shared" frameBorder="0" width="100%" height="500" name="pa9GaRBL"></iframe>



**Complexity**

- Time Complexity: $$\mathcal{O}(2^n)$$ where $$n$$ is the index of the desired sequence.
    - First of all, we would invoke the function `nextSequence()` $$n-1$$ times to get the desired sequence.
    <br/>
    - For each invocation of the function, we would scan the current sequence whose length is difficult to determine though.
    <br/>
    - Let us image in the worst scenario where any two adjacent digit in the sequence are not identical, then its next sequence would _**double**_ the length, rather than having a _reduced_ length. As a result, we could assume that in the worst case, the length of the sequence would grow _exponentially_.
    <br/>
    - As a result, the overall time complexity of the algorithm would be $$\mathcal{O}(\sum_{i=0}^{n-1}{2^i}) = \mathcal{O}(2^n)$$.
<br/>
- Space Complexity: $$\mathcal{O}(2^{n-1})$$.
    - Within each invocation of the `nextSequence()` function, we are using a container to keep the result of the next sequence. The memory consumption of the container is proportional to the length of the sequence that the function needs to process, _i.e_ $$2^{n-1}$$.
    </br>
    - Though we were applying the recursion function, which typically incurs some additional memory consumption in call stack. In our case though, the recursion is implemented in the form of _**tail recursion**_, and we assume that the compiler could optimize its execution which would not incur additional memory consumption.
    <br/>
    - One could also easily replace the recursion with the iteration in this case.
    <br/>
    - As a result, the overall space complexity of the algorithm would be dominated by the space needed to hold the final sequence, _i.e._ $$\mathcal{O}({2^{n-1}})$$.
<br/>
<br/>

---
#### Approach 2: Regular Expression

**Intuition**

This problem could be a good exercise to apply _**pattern matching**_, where in our case we need to _find_ out all those _repetitive_ groups of digits.

A [regular expression](https://en.wikipedia.org/wiki/Regular_expression) (_a.k.a_ _regex_) is a sequence of characters that defines a _**search pattern**_. The regex serves as a common tool for many pattern matching problems. And many programming languages provides regex capacities either with build-in constructs or via some libraries. 

Note that, although the syntax of regex is mostly universal across all programming languages, there might exist some subtile differences. Here we show two examples in Java and Python respectively.

>Java:  `regex = "(.)\\1*"` 

![pic](../Figures/38/java_regex.png)

We could break down the above regex expression into three 3 parts:

- "(.)": it defines a _group_ that contains a single character that could be of anything.

- "\\1": this part refers to the defined group with the index of 1.

- "*":  this qualifier followed by the group reference `\\1`, indicates that we would like to see the group repeats itself _zero_ or more times.

>Python: `regex = "((.)\2*)"`

![pic](../Figures/38/python_regex.png)

Slightly different than the above regex in Java, here we define two _groups_ instead of one.

- "(.)": again, this is a _group_ that contains a single character that could be of anything.

- "\2": this part refers to the second group (_i.e._ `(.)`) that we define.

- "((.)\2*)": the outer bracket defines the scope of the _first_ group, which contains the repetitive appearance of the second group above.

**Algorithm**

Based on the above definitions of regex, we then find all the matches to the regex and then concatenate the results together.

<iframe src="https://leetcode.com/playground/8AQdEMpV/shared" frameBorder="0" width="100%" height="497" name="8AQdEMpV"></iframe>



**Complexity**

- Time Complexity: $$\mathcal{O}(2^n)$$ where $$n$$ is the index of the desired sequence.
    <br/>
    - Similar with our sliding window approach, the overall algorithm consists of a nested loop.
    <br/>
    - We could assume that the time complexity of the regex matching is linear to the length of the input string.
    <br/>
    - As a result, the overall time complexity of the algorithm would be $$\mathcal{O}(\sum_{i=0}^{n-1}{2^i}) = \mathcal{O}(2^n)$$.
<br/>
- Space Complexity: $$\mathcal{O}(2^{n-1})$$.
    - Within the function, we are using a container to keep the result of the next sequence. The memory consumption of the container is proportional to the length of the sequence that the function needs to process, _i.e_ $$2^{n-1}$$.
    <br/>
    - As a result, the space complexity of the algorithm would be dominated by the space needed to hold the final sequence, _i.e._ $$\mathcal{O}({2^{n-1}})$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Please change the misleading description
- Author: boa1150
- Creation Date: Fri Mar 28 2014 13:52:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 20:38:21 GMT+0800 (Singapore Standard Time)

<p>
It seems not only me misunderstood the question. Please modify the description, since it's frustrating if you are solving a "different" question. Thanks.
</p>


### Examples of nth sequence
- Author: xin15
- Creation Date: Mon Jul 14 2014 21:24:28 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 09:23:19 GMT+0800 (Singapore Standard Time)

<p>
At the beginning, I got confusions about what is the nth sequence. Well, my solution is accepted now, so I'm going to give some examples of nth sequence here. The following are sequence from n=1 to n=10:

     1.     1
     2.     11
     3.     21
     4.     1211
     5.     111221 
     6.     312211
     7.     13112221
     8.     1113213211
     9.     31131211131221
     10.   13211311123113112211

From the examples you can see, the (i+1)th sequence is the "count and say" of the ith sequence!

Hope this helps!
</p>


### 4-5 lines Python solutions
- Author: StefanPochmann
- Creation Date: Mon Dec 21 2015 05:55:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 00:24:23 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1** ... using a regular expression

    def countAndSay(self, n):
        s = '1'
        for _ in range(n - 1):
            s = re.sub(r'(.)\1*', lambda m: str(len(m.group(0))) + m.group(1), s)
        return s

---

**Solution 2** ... using a regular expression

    def countAndSay(self, n):
        s = '1'
        for _ in range(n - 1):
            s = ''.join(str(len(group)) + digit
                        for group, digit in re.findall(r'((.)\2*)', s))
        return s

---

**Solution 3** ... using `groupby`

    def countAndSay(self, n):
        s = '1'
        for _ in range(n - 1):
            s = ''.join(str(len(list(group))) + digit
                        for digit, group in itertools.groupby(s))
        return s
</p>


