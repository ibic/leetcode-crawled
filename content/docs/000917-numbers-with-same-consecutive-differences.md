---
title: "Numbers With Same Consecutive Differences"
weight: 917
#id: "numbers-with-same-consecutive-differences"
---
## Description
<div class="description">
<p>Return all <strong>non-negative</strong> integers of length <code>n</code> such that the absolute difference between every two consecutive digits is <code>k</code>.</p>

<p>Note that <strong>every</strong> number in the answer <strong>must not</strong> have leading zeros <strong>except</strong> for the number <code>0</code> itself. For example, <code>01</code> has one leading zero and is invalid, but <code>0</code> is valid.</p>

<p>You may return the answer in <strong>any order</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 3, k = 7
<strong>Output:</strong> [181,292,707,818,929]
<strong>Explanation: </strong>Note that 070 is not a valid number, because it has leading zeroes.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2, k = 1
<strong>Output:</strong> [10,12,21,23,32,34,43,45,54,56,65,67,76,78,87,89,98]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 2, k = 0
<strong>Output:</strong> [11,22,33,44,55,66,77,88,99]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 2, k = 1
<strong>Output:</strong> [10,12,21,23,32,34,43,45,54,56,65,67,76,78,87,89,98]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 2, k = 2
<strong>Output:</strong> [13,20,24,31,35,42,46,53,57,64,68,75,79,86,97]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 9</code></li>
	<li><code>0 &lt;= k &lt;= 9</code></li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Flipkart - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Overview

The problem asks us to come up a list of digit combinations that follow the defined pattern.
Before jumping to the implementation, it is always helpful to _manually_ deduce some examples.

Suppose that we have `N=3` and `K=2`, _i.e._ we should come up a series of 3-digits numbers, where for each number the difference between each adjacent digits is 2.

Let us try to build the number _**digit by digit**_. Starting from the highest digit, we can pick the digit `1`.
Then for the next digit, we need to pick `3` (_i.e._ $$1+2$$).
Finally, for the last digit, we could have two choices: `5` and `1` (_i.e._ $$3+2, 3-2$$).
We illustrate the process in the following graph, where each **_node_** represents a digit that we pick, and the **_level_** of the node corresponds to the position that the digit situates in the final number.

![tree illustration](../Figures/967/967_tree_illustration.png)

>As one might notice that, we just converted the problem into a tree traversal problem, where each path from the root to a leaf forms a solution for the problem.

As we know, the common algorithms for the tree traversal problem would be _**DFS**_ (Depth-First Search) and _**BFS**_ (Breadth-First Search), which are exactly what we will present in the following sections.


---
#### Approach 1: DFS (Depth-First Search)

**Intuition**

If one is not familiar with the concepts of DFS and BFS, we have an Explore card called [Queue & Stack](https://leetcode.com/explore/learn/card/queue-stack/) where we cover the [DFS traversal](https://leetcode.com/explore/learn/card/queue-stack/232/practical-application-stack/) as well as the [BFS traversal](https://leetcode.com/explore/learn/card/queue-stack/231/practical-application-queue/).

In this section, we will start from the DFS strategy, which arguably is more intuitive for this problem.

As we stated in the overview section, we could build a valid digit combination _digit by digit_ or (node by node in terms of tree).

For a number consisting of `N` digits, we start from the highest digit and walk through to the lowest digit.
At each step, we might have several candidates that are eligible to be explored.

With the DFS strategy, we prioritize the _depth_ over the _breadth_, _i.e._ we pick one of the candidates and continue the exploration before moving on to the other candidates that are of the same level.

**Algorithm**

Intuitively we could implement the DFS algorithm with recursion. Here we define a recursive function `DFS(N, num)` (in Python) whose goal is to come up the combinations for the remaining `N` digits, starting from the current `num`.
Note that, the signature of the function is slightly different in our Java implementation. Yet, the semantics of the function remains the same.

![DFS example](../Figures/967/967_dfs_example.png)

For instance, in the previous examples, where `N=3` and `K=2`, and there is a moment we would invoke `DFS(1, 13)` which is to add another digit to the existing number `13` so that the final number meets the requirements.
If the DFS function works properly, we should have the numbers of `135` and `131` as results after the invocation.

We could implement the recursive function in the following steps:

- As a base case, when `N=0` _i.e._ no more remaining digits to complete, we could return the current `num` as the result.

- Otherwise, there are still some remaining digits to be added to the current number, _e.g._ `13`. There are two potential cases to explore, based on the last digit of the current number which we denote as `tail_digit`.

    - Adding the difference `K` to the last digit, _i.e._ `tail_digit + K`.

    - Deducting the difference `K` from the last digit, _i.e._ `tail_digit - K`.

- If the result of either above case falls into the valid digit range (_i.e._ $$[0, 9]$$), we then continue the exploration by invoking the function itself.

Once we implement the `DFS(N, num)` function, we then simply call this function over the scope of $$[1, 9]$$, _i.e._ the valid digits for the highest position. 

**Note**: _If we are asked to return numbers of a single digit (_i.e._ `N=1`), then regardless of `K`, all digits are valid, including zero._
We treat this as a special case in the code, since in our implementation of DFS function, we will never return zero as the result.

<iframe src="https://leetcode.com/playground/Piv3r8xH/shared" frameBorder="0" width="100%" height="500" name="Piv3r8xH"></iframe>



**Complexity Analysis**

Let $$N$$ be the number of digits for a valid combination, and $$K$$ be the difference between digits.

First of all, let us estimate the number of potential solutions.
For the highest digit, we could have 9 potential candidates.
Starting from the second highest position, we could have at most 2 candidates for each position.
Therefore, at most, we could have $$9 \cdot 2^{N-1}$$ solutions, for $$N > 1$$.

- Time Complexity: $$\mathcal{O}(N \cdot 2^{N})$$

    - For each final combination, we would invoke the recursive function $$N$$ times. The operations within each invocation takes a constant time $$\mathcal{O}(1)$$.

    - Therefore, for a total $$9 \cdot 2^{N-1}$$ number of potential combinations, a **loose** upper-bound on the time complexity of the algorithm would be $$\mathcal{O}(N \cdot 9 \cdot 2^{N-1}) = \mathcal{O}(N \cdot 2^{N}) $$, since different combinations could share some efforts during the construction.

    - Note that, when $$K = 0$$, at each position, there is only one possible candidate, _e.g._ $$333$$.
    In total, we would have 9 numbers in the result set, and each number is of $$N$$ digits. The time complexity would then be reduced down to $$\mathcal{O}(N)$$.

- Space Complexity: $$\mathcal{O}(2^{N})$$

    - Since we adopt a recursive solution, we would have some additional memory consumption on the function call stack. The maximum number of consecutive calls on the recursion function is $$N$$. Hence, the space complexity for the call stack is $$\mathcal{O}(N)$$.

    - We use a list to keep all the solutions, which could amount to $$9 \cdot 2^{N-1}$$ number of elements.

    - To sum up, the overall space complexity of the algorithm is $$\mathcal{O}(N) + \mathcal{O}(9 \cdot 2^{N-1}) = \mathcal{O}(2^{N})$$.


---
#### Approach 2: BFS (Breadth-First Search)

**Intuition**

It might be more intuitive to come up a DFS solution as we presented before.
However, it is also viable to solve this problem with _BFS_ (Breadth-First Search) traversal strategy.

>Rather than building the solution one by one, we could do it _batch by batch_, _i.e._ level by level.

Each level contains the numbers that are of the same amount of digits.
Also, each level corresponds to the solutions with a specific number of digits.

![BFS](../Figures/967/967_BFS.png)

For example, given `N=3` and `K=7`, at the first level, we would have potentially 9 candidates (_i.e._ `[1, 2, 3, 4, 5, 7, 8, 9]`).
When we move on to the second level, the candidates are reduced down to `[18, 29, 70, 81, 92]`.
Finally, at the last level, we would have the solutions as `[181, 292, 707, 818, 929]`.

**Algorithm**

Here are a few steps to implement the BFS algorithm for this problem.

- We could implement the algorithm with nested two-levels loops, where the outer loop iterates through levels and the inner loop handles the elements within each level.

- We could use a list data structure to keep the numbers for a single level, _i.e._ here we name the variable as `queue`.

- For each number in the queue, we could apply the same logics as in the DFS approach, except the last step, rather than making a recursive call for the next number we simply append the number to the queue for the next level.

<iframe src="https://leetcode.com/playground/7QWNRxrP/shared" frameBorder="0" width="100%" height="500" name="7QWNRxrP"></iframe>



**Complexity Analysis**

Let $$N$$ be the number of digits for a valid combination, and $$K$$ be the difference between digits.

- Time Complexity: $$\mathcal{O}(N \cdot 2^{N})$$

    - Similar with the DFS approach, it takes $$\mathcal{O}(N)$$ to build each solution.

    - Therefore, for a total $$9 \cdot 2^{N-1}$$ number of potential combinations as we estimated before, the time complexity of the algorithm would be $$\mathcal{O}(N \cdot 9 \cdot 2^{N-1}) = \mathcal{O}(N \cdot 2^{N}) $$.

- Space Complexity: $$\mathcal{O}(2^{N})$$

    - We use two queues to maintain the intermediate solutions, which contain no more than two levels of elements.
    The number of elements at the level of $$i$$ is up to $$9 \cdot 2^{i-1}$$.

    - To sum up, the space complexity of the algorithm would be $$\mathcal{O}(9 \cdot 2^{N-1} + 9 \cdot 2^{N-2}) = \mathcal{O}(2^N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Iterative Solution
- Author: lee215
- Creation Date: Sun Dec 30 2018 12:01:33 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 20 2020 04:53:15 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
We initial the current result with all 1-digit numbers,
like `cur = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]`.

Each turn, for each `x` in `cur`,
we get its last digit `y = x % 10`.
If `y + K < 10`, we add `x * 10 + y + K` to the new list.
If `y - K >= 0`, we add `x * 10 + y - K` to the new list.

We repeat this step `N - 1` times and return the final result.
<br>

# Complexity
If `K >= 5`, time and Space `O(N)`
If `K <= 4`, time and space `O(2^N)`
<br>

**Java:**
```java
    public int[] numsSameConsecDiff(int N, int K) {
        List<Integer> cur = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        for (int i = 2; i <= N; ++i) {
            List<Integer> cur2 = new ArrayList<>();
            for (int x : cur) {
                int y = x % 10;
                if (x > 0 && y + K < 10)
                    cur2.add(x * 10 + y + K);
                if (x > 0 && K > 0 && y - K >= 0)
                    cur2.add(x * 10 + y - K);
            }
            cur = cur2;
        }
        return cur.stream().mapToInt(j->j).toArray();
    }
```

**C++:**
```cpp
    vector<int> numsSameConsecDiff(int N, int K) {
        vector<int> cur = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        for (int i = 2; i <= N; ++i) {
            vector<int> cur2;
            for (int x : cur) {
                int y = x % 10;
                if (x > 0 && y + K < 10)
                    cur2.push_back(x * 10 + y + K);
                if (x > 0 && K > 0 && y - K >= 0)
                    cur2.push_back(x * 10 + y - K);
            }
            cur = cur2;
        }
        return cur;
    }
```

**Python:**
```py
    def numsSameConsecDiff(self, N, K):
        cur = range(10)
        for i in range(N - 1):
            cur = {x * 10 + y for x in cur for y in [x % 10 + K, x % 10 - K] if x and 0 <= y < 10}
        return list(cur)
```

</p>


### Why there are 92 test cases?
- Author: AnnieW
- Creation Date: Tue Jan 01 2019 08:39:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 01 2019 08:39:00 GMT+0800 (Singapore Standard Time)

<p>
As in the problem discripition:
1 <= N <= 9 
0 <= K <= 9
There are 9 eligible values for N and 10 values for K, so there are at most 90 combinations, just curious what could the other 2 test cases?

</p>


