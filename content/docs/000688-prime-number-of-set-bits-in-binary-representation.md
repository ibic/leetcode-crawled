---
title: "Prime Number of Set Bits in Binary Representation"
weight: 688
#id: "prime-number-of-set-bits-in-binary-representation"
---
## Description
<div class="description">
<p>
Given two integers <code>L</code> and <code>R</code>, find the count of numbers in the range <code>[L, R]</code> (inclusive) having a prime number of set bits in their binary representation.
</p><p>
(Recall that the number of set bits an integer has is the number of <code>1</code>s present when written in binary.  For example, <code>21</code> written in binary is <code>10101</code> which has 3 set bits.  Also, 1 is not a prime.)
</p><p>

<p><b>Example 1:</b><br /><pre>
<b>Input:</b> L = 6, R = 10
<b>Output:</b> 4
<b>Explanation:</b>
6 -> 110 (2 set bits, 2 is prime)
7 -> 111 (3 set bits, 3 is prime)
9 -> 1001 (2 set bits , 2 is prime)
10->1010 (2 set bits , 2 is prime)
</pre></p>

<p><b>Example 2:</b><br /><pre>
<b>Input:</b> L = 10, R = 15
<b>Output:</b> 5
<b>Explanation:</b>
10 -> 1010 (2 set bits, 2 is prime)
11 -> 1011 (3 set bits, 3 is prime)
12 -> 1100 (2 set bits, 2 is prime)
13 -> 1101 (3 set bits, 3 is prime)
14 -> 1110 (3 set bits, 3 is prime)
15 -> 1111 (4 set bits, 4 is not prime)
</pre></p>

<p><b>Note:</b><br><ol>
<li><code>L, R</code> will be integers <code>L <= R</code> in the range <code>[1, 10^6]</code>.</li>
<li><code>R - L</code> will be at most 10000.</li>
</ol></p>
</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]
#### Approach #1: Direct [Accepted]
**Intuition and Approach**

For each number from `L` to `R`, let's find out how many set bits it has.  If that number is `2, 3, 5, 7, 11, 13, 17`, or `19`, then we add one to our count.  We only need primes up to 19 because $$R \leq 10^6 < 2^{20}$$.

<iframe src="https://leetcode.com/playground/9vE8Hg77/shared" frameBorder="0" width="100%" height="276" name="9vE8Hg77"></iframe>

**Complexity Analysis**
    
* Time Complexity: $$O(D)$$, where $$D = R-L$$ is the number of integers considered.  In a bit complexity model, this would be $$O(D\log D)$$ as we have to count the bits in $$O(\log D)$$ time.

* Space Complexity: $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 665772
- Author: StefanPochmann
- Creation Date: Sun Jan 14 2018 15:03:16 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 06 2018 11:28:50 GMT+0800 (Singapore Standard Time)

<p>
Ruby:
```
def count_prime_set_bits(l, r)
  (l..r).sum { |i| 665772 >> i.digits(2).sum & 1 }
end
```
Python:

    def countPrimeSetBits(self, L, R):
        return sum(665772 >> bin(i).count('1') & 1 for i in range(L, R+1))

Java stream:

    public int countPrimeSetBits(int L, int R) {
        return IntStream.range(L, R+1).map(i -> 665772 >> Integer.bitCount(i) & 1).sum();
    }

Java:

    public int countPrimeSetBits(int L, int R) {
        int count = 0;
        while (L <= R)
            count += 665772 >> Integer.bitCount(L++) & 1;
        return count;
    }

C++:

    int countPrimeSetBits(int L, int R) {
        int count = 0;
        while (L <= R)
            count += 665772 >> __builtin_popcount(L++) & 1;
        return count;
    }
</p>


### [Java/C++] Clean Code
- Author: alexander
- Creation Date: Sun Jan 14 2018 12:02:46 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 22:51:54 GMT+0800 (Singapore Standard Time)

<p>
**Java**
```
class Solution {
    public int countPrimeSetBits(int l, int r) {
        Set<Integer> primes = new HashSet<>(Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19 /*, 23, 29 */ ));
        int cnt = 0;
        for (int i = l; i <= r; i++) {
            int bits = 0;
            for (int n = i; n > 0; n >>= 1)
                bits += n & 1;
            cnt += primes.contains(bits) ? 1 : 0;
        }
        return cnt;        
    }
}
```
**C++**
```
class Solution {
public:
    int countPrimeSetBits(int l, int r) {
        set<int> primes = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29 };
        int cnt = 0;
        for (int i = l; i <= r; i++) {
            int bits = 0;
            for (int n = i; n; n >>= 1)
                bits += n & 1;
            cnt += primes.count(bits);
        }
        return cnt;
    }
};
```
</p>


### Sort Easy Python
- Author: localhostghost
- Creation Date: Mon Feb 12 2018 12:56:46 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 22:23:48 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def countPrimeSetBits(self, L, R):
        """
        :type L: int
        :type R: int
        :rtype: int
        """
        primes = {2, 3, 5, 7, 11, 13, 17, 19}
        return sum(map(lambda x: bin(x).count('1') in primes, range(L, R+1)))
```
</p>


