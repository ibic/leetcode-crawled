---
title: "Magic Squares In Grid"
weight: 784
#id: "magic-squares-in-grid"
---
## Description
<div class="description">
<p>A <code>3 x 3</code> magic square is a <code>3 x 3</code> grid filled with distinct numbers <strong>from </strong><code>1</code><strong> to </strong><code>9</code> such that each row, column, and both diagonals all have the same sum.</p>

<p>Given a <code>row x col</code>&nbsp;<code>grid</code>&nbsp;of integers, how many <code>3 x 3</code> &quot;magic square&quot; subgrids are there?&nbsp; (Each subgrid is contiguous).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/11/magic_main.jpg" style="width: 322px; height: 242px;" />
<pre>
<strong>Input:</strong> grid = [[4,3,8,4],[9,5,1,9],[2,7,6,2]]
<strong>Output:</strong> 1
<strong>Explanation: </strong>
The following subgrid is a 3 x 3 magic square:
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/11/magic_valid.jpg" style="width: 242px; height: 242px;" />
while this one is not:
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/11/magic_invalid.jpg" style="width: 242px; height: 242px;" />
In total, there is only one magic square inside the given grid.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> grid = [[8]]
<strong>Output:</strong> 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> grid = [[4,4],[3,3]]
<strong>Output:</strong> 0
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> grid = [[4,7,8],[9,5,1],[2,3,6]]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>row == grid.length</code></li>
	<li><code>col == grid[i].length</code></li>
	<li><code>1 &lt;= row, col &lt;= 10</code></li>
	<li><code>0 &lt;= grid[i][j] &lt;= 15</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Google - 4 (taggedByAdmin: true)
- Wayfair - 8 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Brute Force [Accepted]

**Intuition and Algorithm**

Let's check every 3x3 grid individually.  For each grid, all numbers must be unique and between 1 and 9; plus every row, column, and diagonal must have the same sum.

**Extra Credit**

We could also include an `if grid[r+1][c+1] != 5: continue` check into our code, helping us skip over our `for r... for c...` for loops faster.  This is based on the following observations:

* The sum of the grid must be 45, as it is the sum of the distinct values from 1 to 9.
* Each horizontal and vertical line must add up to 15, as the sum of 3 of these lines equals the sum of the whole grid.
* The diagonal lines must also sum to 15, by definition of the problem statement.
* Adding the 12 values from the four lines that cross the center, these 4 lines add up to 60; but they also add up to the entire grid (45), plus 3 times the middle value.  This implies the middle value is 5.

<iframe src="https://leetcode.com/playground/dHJi8J8s/shared" frameBorder="0" width="100%" height="500" name="dHJi8J8s"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(R*C)$$, where $$R, C$$ are the number of rows and columns in the given `grid`.

* Space Complexity:  $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python, 5 and 43816729
- Author: lee215
- Creation Date: Sun May 27 2018 11:09:21 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 12:07:58 GMT+0800 (Singapore Standard Time)

<p>
The intuition is brute force, don\'t need any tricky. 
One thing to pay attention: A 3 x 3 magic square is a 3 x 3 grid filled with distinct numbers from **1 to 9**.
I just find many sumbmission ignoring this condition. 

Here I just want share two observatons with this **1-9** condition:

Assume a magic square:
a1,a2,a3
a4,a5,a6
a7,a8,a9

`a2 + a5 + a8 = 15`
`a4 + a5 + a6 = 15`
`a1 + a5 + a9 = 15`
`a3 + a5 + a7 = 15`

Accumulate all, then we have:
`sum(ai) + 3 * a5 = 60`
`3 * a5 = 15`
`a5 = 5`

The center of magic square must be 5.

Another observation for other 8 numbers:
The even must be in the corner, and the odd must be on the edge.
And it must be in a order like "43816729" \uFF08clockwise or anticlockwise)

```
    def numMagicSquaresInside(self, g):
        def isMagic(i, j):
            s = "".join(str(g[i + x / 3][j + x % 3]) for x in [0, 1, 2, 5, 8, 7, 6, 3])
            return g[i][j] % 2 == 0 and (s in "43816729" * 2 or s in "43816729"[::-1] * 2)
        return sum(isMagic(i, j) for i in range(len(g) - 2) for j in range(len(g[0]) - 2) if g[i + 1][j + 1] == 5)
```

</p>


### One misleading problem
- Author: sweetandbitter
- Creation Date: Sun May 27 2018 11:36:12 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 06 2018 13:55:25 GMT+0800 (Singapore Standard Time)

<p>
Problem said it is distinct numbers from 1 to 9, I think it indicate grid contain only 1 to 9 .
When I failed in test case [[10, 3, 5], [1, 6. 11], [7, 9, 2]], I got confused and tired more than 10 times.
0 <= grid[i][j] <= 15 should move to the start of the problem and highlight instead of pub it in note.
</p>


### Straightforward Java Solution
- Author: wangzi6147
- Creation Date: Sun May 27 2018 11:04:45 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 06 2018 13:57:43 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int numMagicSquaresInside(int[][] grid) {
        int m = grid.length, n = grid[0].length, result = 0;
        for (int i = 0; i < m - 2; i++) {
            for (int j = 0; j < n - 2; j++) {
                if (isMagic(grid, i, j)) {
                    result++;
                }
            }
        }
        return result;
    }
    private boolean isMagic(int[][] grid, int row, int col) {
        int[] record = new int[10];
        for (int i = row; i < row + 3; i++) {
            for (int j = col; j < col + 3; j++) {
                if (grid[i][j] < 1 || grid[i][j] > 9 || record[grid[i][j]] > 0) {
                    return false;
                }
                record[grid[i][j]] = 1;
            }
        }
        int sum1 = grid[row][col] + grid[row + 1][col + 1] + grid[row + 2][col + 2];
        int sum2 = grid[row][col + 2] + grid[row + 1][col + 1] + grid[row + 2][col];
        if (sum1 != sum2) {
            return false;
        }
        for (int i = 0; i < 3; i++) {
            if (grid[row + i][col] + grid[row + i][col + 1] + grid[row + i][col + 2] != sum1) {
                return false;
            }
            if (grid[row][col + i] + grid[row + 1][col + i] + grid[row + 2][col + i] != sum1) {
                return false;
            }
        }
        return true;
    }
}
```
</p>


