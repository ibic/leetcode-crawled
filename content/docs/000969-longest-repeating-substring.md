---
title: "Longest Repeating Substring"
weight: 969
#id: "longest-repeating-substring"
---
## Description
<div class="description">
<p>Given a string <code>S</code>, find out the length of the longest repeating substring(s). Return <code>0</code> if no repeating substring exists.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> S = &quot;abcd&quot;
<strong>Output:</strong> 0
<strong>Explanation: </strong>There is no repeating substring.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> S = &quot;abbaba&quot;
<strong>Output:</strong> 2
<strong>Explanation: </strong>The longest repeating substrings are &quot;ab&quot; and &quot;ba&quot;, each of which occurs twice.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> S = &quot;aabcaabdaab&quot;
<strong>Output:</strong> 3
<strong>Explanation: </strong>The longest repeating substring is &quot;aab&quot;, which occurs <code>3</code> times.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> S = &quot;aaaaa&quot;
<strong>Output:</strong> 4
<strong>Explanation: </strong>The longest repeating substring is &quot;aaaa&quot;, which occurs twice.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The string <code>S</code> consists of only lowercase English letters from <code>&#39;a&#39;</code> - <code>&#39;z&#39;</code>.</li>
	<li><code>1 &lt;= S.length &lt;= 1500</code></li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Google - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: true)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Split into two subtasks

Let's focus here on the solutions which are performing better
than naive $$\mathcal{O}(N^2)$$ at least in the best/average cases.  

Here we have "two in one" problem :

1. Perform a search by a substring length in the interval from 1 to N.

2. Check if there is a duplicate substring of a given length L.

**Subtask one : Binary search by a substring length**

A naive solution would be to check all possible string length 
one by one starting from N - 1: 
if there is a duplicate substring of length N - 1, then of length N - 2, etc. 
Note that if there is a duplicate substring of length k, that means
that there is a duplicate substring of length k - 1. 
Hence one could use a binary search by string length here,
and have the first problem solved in $$\mathcal{O}(\log N)$$ time. 

![pic](../Figures/1062/binary.png)

The binary search code is quite standard and we will use it here
for all approaches to focus on much more interesting subtask number two.

<iframe src="https://leetcode.com/playground/YW3x9Tbo/shared" frameBorder="0" width="100%" height="463" name="YW3x9Tbo"></iframe>

**Subtask two : Check if there is a duplicate substring of length L**

We will discuss here three different ideas how to proceed.
They are all based on sliding window + hashset, 
though their performance and space consumption are quite different. 

1. Linear-time slice + hashset of already seen strings.
$$\mathcal{O}((N - L) L)$$ time complexity and 
huge space consumption in the case of large strings. 

2. Linear-time slice + hashset of _hashes_ of already seen strings. 
$$\mathcal{O}((N - L) L)$$ time complexity and 
moderate space consumption even in the case of large strings. 

3. Rabin-Karp = constant-time slice + 
hashset of _hashes_ of already seen strings.
Hashes are computed with the rolling hash algorithm.
$$\mathcal{O}(N - L)$$ time complexity and 
moderate space consumption even in the case of large strings.

![pic](../Figures/1062/algorithms.png)

<br /> 
<br />


---
#### Approach 1: Binary Search + Hashset of Already Seen Strings

The idea is straightforward : 

- Move a sliding window of length L along the string of length N.
 
- Check if the string in the sliding window
is in the hashset of already seen strings. 

    - If yes, the duplicate substring is right here.
    
    - If not, save the string in the sliding window in the hashset.  

!?!../Documents/1062_LIS.json:1000,411!?!

Obvious drawback of this approach is a huge memory consumption
in the case of large strings. 

<iframe src="https://leetcode.com/playground/qz5FBzFQ/shared" frameBorder="0" width="100%" height="500" name="qz5FBzFQ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N \log N)$$ in the average case
and $$\mathcal{O}(N^2)$$ in the worst case. 
One needs $$\mathcal{O}((N - L)L)$$ for one duplicate check,
and one does up to $$\mathcal{O}(\log N)$$ checks. 
Together that results in $$\mathcal{O}(\sum\limits_{L}{(N - L)L})$$, 
i.e. in $$\mathcal{O}(N \log N)$$ in the average 
case and in $$\mathcal{O}(N^2)$$ in the worst case of L close to $$N/2$$. 

* Space complexity : $$\mathcal{O}(N^2)$$ to keep the hashset.
<br /> 
<br />


---
#### Approach 2: Binary Search + Hashset of _Hashes_ of Already Seen Strings

To reduce the memory consumption by the hashset structure,
one could store not the full strings, but their hashes.

The drawback of this approach is a time performance, 
which is still not always linear.  

![pic](../Figures/1062/dupe.png)

<iframe src="https://leetcode.com/playground/mG8KVRUW/shared" frameBorder="0" width="100%" height="500" name="mG8KVRUW"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N \log N)$$ in the average case
and $$\mathcal{O}(N^2)$$ in the worst case. 
One needs $$\mathcal{O}((N - L)L)$$ for one duplicate check,
and one does up to $$\mathcal{O}(\log N)$$ checks. 
Together that results in $$\mathcal{O}(\sum\limits_{L}{(N - L)L})$$, 
i.e. in $$\mathcal{O}(N \log N)$$ in the average 
case and in $$\mathcal{O}(N^2)$$ in the worst case of L close to $$N/2$$. 

* Space complexity : $$\mathcal{O}(N)$$ to keep the hashset.

<br /> 
<br />


---
#### Approach 3: Binary Search + Rabin-Karp

Rabin-Karp algorithm is used to perform a multiple pattern search 
in a linear time and with a moderate memory consumption suitable 
for the large strings. 

The linear time implementation of this idea is a bit
tricky because of two technical problems:

1. [How to implement a string slice in a constant time?](https://stackoverflow.com/questions/35180377/time-complexity-of-string-slice) 

2. Hashset memory consumption could be huge for very long strings. 
One could keep the string hash instead of string itself
but hash generation costs $$\mathcal{O}(L)$$ for the string of length L,
and the complexity of algorithm would be $$\mathcal{O}((N - L)L)$$,
N - L for the slice and L for the hash generation. 
One has to think how to generate hash in a constant time here.

Let's now address these problems.

**String slice in a constant time**

That's a very language-dependent problem. For the moment for 
Java and Python there is no straightforward solution, 
and to move sliding window in a constant time
one has to convert string to another data structure. 

The simplest solution both for Java and Python is to convert string to integer array of ascii-values.

**Rolling hash : hash generation in a constant time**

To generate hash of array of length L, one needs $$\mathcal{O}(L)$$ time.

> How to have constant time of hash generation? Use the advantage of 
slice: only one integer in, and only one - out. 

That's the idea of [rolling hash](https://en.wikipedia.org/wiki/Rolling_hash).
Here we'll implement the simplest one, polynomial rolling hash.
Beware that's polynomial rolling hash is NOT the [Rabin fingerprint](https://en.wikipedia.org/wiki/Rolling_hash#Rabin_fingerprint).

Since one deals here with lowercase English letters, all values 
in the integer array are between 0 and 25 :
`arr[i] = (int)S.charAt(i) - (int)'a'`.  
So one could consider string `abcd` -> `[0, 1, 2, 3]` as a number 
in a [numeral system](https://en.wikipedia.org/wiki/Numeral_system) with the base 26. 
Hence `abcd` -> `[0, 1, 2, 3]` could be hashed as 

$$
h_0 = 0 \times 26^3 + 1 \times 26^2 + 2 \times 26^1 + 3 \times 26^0
$$

Let's write the same formula in a generalised way, where $$c_i$$
is an integer array element and $$a = 26$$ is a system base.

$$
h_0 = c_0 a^{L - 1} + c_1 a^{L - 2} + ... + c_i a^{L - 1 - i} + ... + c_{L - 1} a^1 + c_L a^0
$$

$$
h_0 = \sum_{i = 0}^{L - 1}{c_i a^{L - 1 - i}}
$$

Now let's consider the slice `abcd` -> `bcde`. For int arrays that means
`[0, 1, 2, 3]` -> `[1, 2, 3, 4]`, to remove number 0 and to add number 4.

$$
h_1 = (h_0 - 0 \times 26^3) \times 26 + 4 \times 26^0
$$

In a generalised way

$$
h_1 = (h_0 a - c_0 a^L) + c_{L + 1}
$$

Now hash regeneration is perfect and fits in a constant time. 
There is one more issue to address: possible overflow problem. 

**How to avoid overflow**

$$a^L$$ could be a large number and hence
the idea is to set limits to avoid the overflow. 
To set limits means to limit a hash by a given number called modulus
and use everywhere not hash itself but `h % modulus`.

It's quite obvious that modulus should be large enough, but how 
large? [Here one could read more about the topic](https://en.wikipedia.org/wiki/Linear_congruential_generator#Parameters_in_common_use),
for the problem here $$2^{24}$$ is enough.

In a real life, when not all testcases are known in advance, 
one has to check if the strings with equal hashes are truly equal.
Such false-positive strings could happen 
because of a modulus which is too small and strings which are too long.
That leads to Rabin-Karp time complexity $$\mathcal{O}(NL)$$ 
in the worst case then almost all strings are false-positive. 
Here it's not the case because all testcases are known and 
one could adjust the modulus. 

Another one overflow issue here is purely Java related.
While in Python the hash regeneration goes perfectly fine, 
in Java the same thing is better to rewrite to avoid
long overflow. 

<iframe src="https://leetcode.com/playground/FzHGskkW/shared" frameBorder="0" width="100%" height="89" name="FzHGskkW"></iframe>

**Rabin-Karp algorithm**

search(L) :
    
- Compute the hash of substring `S.substring(0, L)`
    and initiate the hashset of already seen substring with this value.
        
- Iterate over the start position of substring : from 1 to $$N - L$$.
        
    - Compute rolling hash based on the previous hash value.
            
    - Return start position if the hash is in the hashset,
        because that means one met the duplicate. 
            
    - Otherwise, add hash in the hashset.
         
- Return -1, that means there is no duplicate string of length L.

**Implementation**

<iframe src="https://leetcode.com/playground/4uZaangG/shared" frameBorder="0" width="100%" height="500" name="4uZaangG"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N \log N)$$. $$\mathcal{O}(\log N)$$
for the binary search and $$\mathcal{O}(N)$$ for Rabin-Karp algorithm.
* Space complexity : $$\mathcal{O}(N)$$ to keep the hashset.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java]  2  O(n^3) -> 2  O(n^2 logn) ->  2*O (n^2) -> O (n logn) -> O(n)
- Author: happyleetcode
- Creation Date: Thu Jul 11 2019 20:40:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Apr 10 2020 09:07:12 GMT+0800 (Singapore Standard Time)

<p>
O (n^3)  Approach 1.
loop result from max to 1, once found a max, then return
```
public int longestRepeatingSubstring(String S) {
	Set<String> set = new HashSet<>();
	int max = S.length() - 1, i = 0;
	for (;i <= S.length();i++) {
		int j = i;
		if (j + max > S.length()) {
			if (--max == 0) break;
			i = -1;
			set.clear();
			continue;
		}
		String k = S.substring(j,j+max);
		if (!set.add(k)) {
			return max;
		}
	}
	return max;
}
```
O (n^3)  Approach 2.
loop result from 1 to max, once meet a ans with not found duplicate string, then return ans - 1
```
public int longestRepeatingSubstring(String S) {
	int l = S.length(), max = 0, i = 0;
	Set<String> s = new HashSet<>();
	while (i <= l) {
		int j = i;
		if (j + max == l) {
			return max;
		}
		String k = S.substring(j,j + max + 1);
		if (!s.add(k)) {
			i = 0;
			s.clear();
			max++;
		} else {
			i++;
		}
	}
	return max;
}
```
O (n^2 log n) Approach 1
get n suffix of the string. "abc" -> "abc","bc","c"
then sort them, if have two common prefix, they must be neighbors. for loop them find longest common prefix.
```
public int longestRepeatingSubstring(String S) {
	int l = S.length();
	String[] suffix = new String[l];
	for (int i = 0; i < l; i++) suffix[i] = S.substring(i);
	Arrays.sort(suffix);
	int max = 0;
	for (int i = 1; i < l; i++) {
		int j = 0;
		for (; j < Math.min(suffix[i].length(),suffix[i-1].length()); j++) {
			if (suffix[i].charAt(j) != suffix[i-1].charAt(j)) break;
		}
		max = Math.max(max,j);
	}
	return max;
}
```
O (n^2 log n) Approach 2
binary search -> if length 3 have duplicate pattern, length 2 must have.
so if we can search the answer,s =  mid + 1. if not , e =  mid - 1. 
max is \'s - 1\';
```
public int longestRepeatingSubstring(String S) {
	char[] cs = S.toCharArray();
	int s = 1, e = cs.length - 1;
	while (s <= e) {
		int mid = (s + e) / 2;
		if (search(cs,mid)) {
			s = mid + 1;
		} else {
			e = mid - 1;
		}
	}
	return s - 1;
}
boolean search(char[] cs,int k) {
	Set<String> s = new HashSet<>();
	for (int i = 0; i <= cs.length - k; i++) {
		if (!s.add(new String(cs,i,k))) 
			return true;
	}
	return false;
}
```
O (n^2 ) Approach 1
dp[i][j] means end with i, end with j , what\'s max length of common string.
abcbc. dp[2][4] = 2 because bc == bc, abc != cbc
```
public int longestRepeatingSubstring(String S) {
	int l = S.length();
	int[][] dp = new int[l+1][l+1];
	int res = 0;
	for (int i = 1; i <= l; i++) {
		for (int j = i + 1; j <= l; j++) {
			if (S.charAt(i - 1) == S.charAt(j - 1)) {
				dp[i][j] = dp[i - 1][j - 1] + 1;
				res = Math.max(dp[i][j],res);
			}
		}
	}
	return res;
}
```
O (n^2 ) Approach 2
we could use MSD radix sort, we can sort n string , in O(26 * N * N) 

```
public int longestRepeatingSubstring(String S) {
        int l = S.length();
        String[] suffix = new String[l];
        for (int i = 0; i < l; i++) suffix[i] = S.substring(i);
        msdRadixSort(suffix);
        int max = 0;
        for (int i = 1; i < l; i++) {
            int j = 0;
            for (; j < Math.min(suffix[i].length(),suffix[i-1].length()); j++) {
                if (suffix[i].charAt(j) != suffix[i-1].charAt(j)) break;
            }
            max = Math.max(max,j);
        }
        return max;
    }
    void msdRadixSort(String[] input) {
        sort(input, 0, input.length - 1, 0, new String[input.length]);
    }
    private void sort(String[] input, int lo, int hi, int depth, String[] aux) {
        if (lo >= hi) return;
        int[] cnt = new int[28];
        for (int i = lo; i <= hi; i++) {
            cnt[charAt(input[i], depth) + 1]++;
        }
        for (int i = 1; i < 28; i++) cnt[i] += cnt[i - 1];
        for (int i = lo; i <= hi; i++) {
            aux[cnt[charAt(input[i], depth)]++] = input[i];
        }
        for (int i = lo; i <= hi; i++) input[i] = aux[i - lo];
        for (int i = 0; i < 27; i++)
            sort(input, lo + cnt[i], lo + cnt[i + 1] - 1, depth + 1, aux);
    }
    private int charAt(String str, int i) {
        if (i >= str.length()) return 0;
        return str.charAt(i) - \'a\' + 1;
    }
```

O (n logn ) Approach
binary search with rolling hash monte carlo version
below version is Monte Carlo version. Return match if hash match.
there exists another version for 100% correctness.
Las Vegas version. Check for substring match if hash match;
continue search if false collision.
**Theory.** If Q is a sufficiently large random prime (about M * N ^ 2),
then the probability of a false collision is about 1 / N.
**Practice.** Choose Q to be a large prime (but not so large to cause overflow).
Under reasonable assumptions, probability of a collision is about 1 / Q

Monte Carlo version.
\u30FBAlways runs in n logn time.
\u30FBExtremely likely to return correct answer (but not always!).
Las Vegas version.
\u30FBAlways returns correct answer.
\u30FBExtremely likely to run in n logn time (but worst case is n^2 log n)

```
public int longestRepeatingSubstring(String S) {
	char[] cs = S.toCharArray();
	int s = 1, e = cs.length - 1;
	while (s <= e) {
		int mid = (s + e) / 2;
		if (search(cs,mid)) {
			s = mid + 1;
		} else {
			e = mid - 1;
		}
	}
	return s - 1;
}
boolean search(char[] cs,int k) {
	Set<Integer> s = new HashSet<>();
	long mod = 10000000007L, pow = 1, cur = 0;
	for (int i = 0; i < cs.length; i++) {
		cur = (cur * 26 + (cs[i] - \'a\')) % mod;
		if (i >= k) {
			cur = (cur - ((cs[i - k] - \'a\') * pow % mod) + mod) % mod;
		} else {
			 pow = pow * 26  % mod;
		}
		if (i >= k - 1) {
			if (!s.add((int)cur)) return true;
		} 
	}
	return false;
}
```
average O (n ) Approach
use Ukkonen\u2019s algorithm to build the suffix array in O(n)
then compare i and i+1, i from 0 to n - 2; in worst case, this algorithm toke O(n^2), because the time compare two neighbor suffix cost O(n), there are n neighbor, so is O(n^2), but if string is random, there have no this problem.
 
worst case O (n ) Approach
use Ukkonen\u2019s algorithm to build the suffix tree in O(n)
then find deepest internal node that is the answer, find deepest internal node cost O(n)
so totally O(n)
```
class Solution {
    public int longestRepeatingSubstring(String S) {
        SuffixTree st = new SuffixTree(S);
        return st.deepInternalNode();
    }
}
class SuffixTree {

    public static final int MAX_CHAR = 128;

    class Node {
        int start, index = -1, end;
        Node suffixLink;
        Node[] chds = new Node[MAX_CHAR];
        public Node(int start, int end) {
            this.start = start;
            this.end = end;
            if (end != inf) suffixLink = root;
        }
        int len() {
            return Math.min(end, globalEnd)  - start + 1;
        }
    }

    Node root = new Node(-1, -1);
    Node lastNewNode = null;
    int globalEnd = -1;
    int inf = Integer.MAX_VALUE / 2;
    char[] text;

    Node activeNode = root;
    int activeEdgeAsTextIndex = -1;
    int activeLength = 0;
    int remaining = 0;

    boolean tryWalkDown(Node cur) {
        int edgeLen = cur.len();
        if (activeLength >= edgeLen) {
            activeEdgeAsTextIndex += edgeLen;
            activeLength -= edgeLen;
            activeNode = cur;
            return true;
        }
        return false;
    }

    private void extend(char c) {
        text[++globalEnd] = c;
        remaining++;
        lastNewNode = null;
        while (remaining > 0) {
            if (activeLength == 0) activeEdgeAsTextIndex = globalEnd;
            if (activeNode.chds[text[activeEdgeAsTextIndex]] == null) {
                activeNode.chds[text[activeEdgeAsTextIndex]] = new Node(globalEnd, inf);
                addSuffixLinkIfLastNodeExists(activeNode);
            } else {
                Node chd = activeNode.chds[text[activeEdgeAsTextIndex]];
                if (tryWalkDown(chd)) continue;
                if (text[chd.start + activeLength] == c) { // do nothing
                    addSuffixLinkIfLastNodeExists(activeNode);
                    activeLength++;
                    break;
                }
                Node internalSplit = new Node(chd.start, chd.start + activeLength - 1);
                activeNode.chds[text[activeEdgeAsTextIndex]] = internalSplit;
                internalSplit.chds[c] = new Node(globalEnd, inf);
                chd.start += activeLength;
                internalSplit.chds[text[chd.start]] = chd;
                addSuffixLinkIfLastNodeExists(internalSplit);
            }
            remaining--;
            if (activeNode != root) activeNode = activeNode.suffixLink;
            else if (activeLength > 0) {
                activeLength--;
                activeEdgeAsTextIndex = globalEnd - remaining + 1;
            }
        }
    }

    private void addSuffixLinkIfLastNodeExists(Node node) {
        if (lastNewNode != null)
            lastNewNode.suffixLink = node;
        lastNewNode = node;
    }

    private void setSuffixIndexByDFS(Node cur, int labelHeight) {
        if (cur.suffixLink == null && cur.start != -1) {
            cur.index = globalEnd + 1 - labelHeight;
        } else {
            for (int i = 0; i < MAX_CHAR; i++) {
                if (cur.chds[i] == null) continue;
                setSuffixIndexByDFS(cur.chds[i], labelHeight + cur.chds[i].len());
            }
        }
    }

    public SuffixTree(String str) {
        this.text = new char[str.length() + 1];
        for (int i = 0; i <= str.length(); i++) {
            char c = (i == str.length() ?  \'$\' : str.charAt(i));
            extend(c);
        }
        setSuffixIndexByDFS(root, 0);
    }
    
    int maxDepth;
    public int deepInternalNode() {
        maxDepth = 0;
        help(root, 0);
        return maxDepth;
    }
    void help(Node cur, int dep) {
        if (cur == null) return;
        if (cur.index == -1) { // internal
            maxDepth = Math.max(maxDepth, dep);
            for (int i = 0; i < MAX_CHAR; i++) {
                if (cur.chds[i] == null) continue;
                help(cur.chds[i], dep + cur.chds[i].len());
            }
        } 
    }
```

</p>


### easy to understand N^2 solution
- Author: JagdishHiremath
- Creation Date: Sun Jun 02 2019 13:44:52 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 06 2019 16:47:50 GMT+0800 (Singapore Standard Time)

<p>
```

class Solution {//O(n^2)
    public int longestRepeatingSubstring(String S) {
        int n = S.length();
        int[][] dp = new int[n + 1][n + 1];//dp[i][j] means # of repeated chars for substrings ending at i and j
        int res = 0;
        for (int i = 1; i <= n; i++) {
            for (int j = i + 1; j <= n; j++) {
                if (S.charAt(i - 1) == S.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                    res = Math.max(res, dp[i][j]);
                }
            }
        }
        return res;
    }
}
```

```

class Solution {//O(n^2*logn)
	public int longestRepeatingSubstring(String s) {
		int n = s.length();
		String[] suffix = new String[n];// all strings in suffix[] has different starting position
		for (int i = 0; i < n; i++) {
			suffix[i] = s.substring(i);
		}
		Arrays.sort(suffix);//lexicographic order
		int res = 0;
		for (int i = 1; i < n; i++) {
			String a = suffix[i - 1], b = suffix[i];//!!!every adjacent pair since repeating substrings should be neighbors
			int len = Math.min(a.length(), b.length());
			for (int j = 0; j < len; j++) {
				if (a.charAt(j) != b.charAt(j)) break;
				res = Math.max(res, j + 1);
			}
		}
		return res;
    }
}
```
</p>


### [Java] Straightforward Trie solution O(N^2)
- Author: yuhwu
- Creation Date: Thu Jul 11 2019 08:42:17 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 11 2019 08:42:17 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    class TrieNode{
        TrieNode[] next;
        public TrieNode(){
            next = new TrieNode[26];
        }
    }
    
    public int longestRepeatingSubstring(String S) {
        char[] A = S.toCharArray();
        int res = 0;
        TrieNode root = new TrieNode();
        for(int i=0; i<S.length(); i++){
            TrieNode cur = root;
            for(int j=i; j<S.length(); j++){
                if(cur.next[A[j]-\'a\']==null){
                    TrieNode newNode = new TrieNode();
                    cur.next[A[j]-\'a\'] = newNode;
                    cur = newNode;
                }
                else{
                    res = Math.max(res,j-i+1);
                    cur = cur.next[A[j]-\'a\'];
                }
            }
        }
        return res;
    }
}
```
</p>


