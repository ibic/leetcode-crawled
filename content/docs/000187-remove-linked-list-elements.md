---
title: "Remove Linked List Elements"
weight: 187
#id: "remove-linked-list-elements"
---
## Description
<div class="description">
<p>Remove all elements from a linked list of integers that have value <b><i>val</i></b>.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b>  1-&gt;2-&gt;6-&gt;3-&gt;4-&gt;5-&gt;6, <em><b>val</b></em> = 6
<b>Output:</b> 1-&gt;2-&gt;3-&gt;4-&gt;5
</pre>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Facebook - 8 (taggedByAdmin: false)
- Pure Storage - 8 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Capital One - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Sentinel Node

**Intuition**

The problem seems to be very easy if one has to delete a node in the middle:

- Pick the node-predecessor `prev` of the node to delete. 

- Set its next pointer to point to the node next to the one to delete.

![bla](../Figures/203/middle2.png)

The things are more complicated when the node or nodes to delete
are in the head of linked list. 

![bla](../Figures/203/head_delete.png)

> How to deal with that? To reduce the problem 
to the deletion of middle nodes with the help of [sentinel node](https://en.wikipedia.org/wiki/Sentinel_node).

Sentinel nodes are widely used in trees and linked lists 
as pseudo-heads, pseudo-tails, markers of level end, etc.
They are purely functional, and usually does not hold any data.
Their main purpose is to standardize the situation, for example,
make linked list to be never empty and never headless and hence
simplify insert and delete.

Here are two standard examples: 

- [LRU Cache](https://leetcode.com/articles/lru-cache/),
sentinel nodes are used as pseudo-head and pseudo-tail.

- [Tree Level Order Traversal](https://leetcode.com/articles/maximum-level-sum-of-a-binary-tree/),
sentinel nodes are used to mark level end.

![bla](../Figures/203/to_delete2.png)

Here sentinel node will be used as pseudo-head. 

**Algorithm**

- Initiate sentinel node as `ListNode(0)` and set it to be the new head:
`sentinel.next = head`. 

- Initiate two pointers to track the current node and its predecessor:
`curr` and `prev`.

- While `curr` is not a null pointer:

    - Compare the value of the current node with the value to delete.
    
        - In the values are equal, delete the current node:
        `prev.next = curr.next`.
        
        - Otherwise, set predecessor to be equal to the current node.
        
    - Move to the next node: `curr = curr.next`.
    
- Return `sentinel.next`.
            
**Implementation**

<iframe src="https://leetcode.com/playground/CwQTGNgc/shared" frameBorder="0" width="100%" height="497" name="CwQTGNgc"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$, it's one pass solution. 
 
* Space complexity: $$\mathcal{O}(1)$$, it's a constant space
solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 3 line recursive solution
- Author: renzid
- Creation Date: Thu Apr 23 2015 11:36:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 22:23:31 GMT+0800 (Singapore Standard Time)

<p>
    public ListNode removeElements(ListNode head, int val) {
            if (head == null) return null;
            head.next = removeElements(head.next, val);
            return head.val == val ? head.next : head;
    }
</p>


### Simple Python solution with explanation (single pointer, dummy head).
- Author: Hai_dee
- Creation Date: Sat Aug 11 2018 10:55:56 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 07 2019 20:13:18 GMT+0800 (Singapore Standard Time)

<p>
Before writing any code, it\'s good to make a list of edge cases that we need to consider. This is so that we can be certain that we\'re not overlooking anything while coming up with our algorithm, and that we\'re testing all special cases when we\'re ready to test. These are the edge cases that I came up with.

1. The linked list is empty, i.e. the head node is None.
2. Multiple nodes with the target value in a row. 
3. The head node has the target value.
4. The head node, and any number of nodes immediately after it have the target value.
5. All of the nodes have the target value.
6. The last node has the target value.

So with that, this is the algorithm I came up with.

```
class Solution:
    def removeElements(self, head, val):
        """
        :type head: ListNode
        :type val: int
        :rtype: ListNode
        """
        
        dummy_head = ListNode(-1)
        dummy_head.next = head
        
        current_node = dummy_head
        while current_node.next != None:
            if current_node.next.val == val:
                current_node.next = current_node.next.next
            else:
                current_node = current_node.next
                
        return dummy_head.next
```

In order to save the need to treat the "head" as special, the algorithm uses a "dummy" head. This simplifies the code greatly, particularly in the case of needing to remove the head AND some of the nodes immediately after it.

Then, we keep track of the current node we\'re up to, and look ahead to its next node, as long as it exists. If ```current_node.next``` does need removing, then we simply replace it with ```current_node.next.next```. We know this is always "safe", because ```current_node.next``` is definitely not None (the loop condition ensures that), so we can safely access its ```next```.

Otherwise, we know that ```current_node.next``` should be kept, and so we move ```current_node``` on to be ```current_node.next```.

The loop condition only needs to check that ```current_node.next != None```. The reason it does **not** need to check that ```current_node != None``` is because this is an impossible state to reach. Think about it this way: The ONLY case that we ever do ```current_node = current_node.next``` in is immediately after the loop has already confirmed that ```current_node.next``` is not None. 

The algorithm requires ***```O(1)```*** extra space and takes ***```O(n)```*** time.
</p>


### AC Java solution
- Author: lx223
- Creation Date: Sat Apr 25 2015 09:06:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 07:40:28 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public ListNode removeElements(ListNode head, int val) {
            ListNode fakeHead = new ListNode(-1);
            fakeHead.next = head;
            ListNode curr = head, prev = fakeHead;
            while (curr != null) {
                if (curr.val == val) {
                    prev.next = curr.next;
                } else {
                    prev = prev.next;
                }
                curr = curr.next;
            }
            return fakeHead.next;
        }
    }
</p>


