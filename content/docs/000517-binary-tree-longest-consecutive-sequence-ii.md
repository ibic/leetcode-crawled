---
title: "Binary Tree Longest Consecutive Sequence II"
weight: 517
#id: "binary-tree-longest-consecutive-sequence-ii"
---
## Description
<div class="description">
<p>Given a binary tree, you need to find the length of Longest Consecutive Path in Binary Tree.</p>

<p>Especially, this path can be either increasing or decreasing. For example, [1,2,3,4] and [4,3,2,1] are both considered valid, but the path [1,2,4,3] is not valid. On the other hand, the path can be in the child-Parent-child order, where not necessarily be parent-child order.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b>
        1
       / \
      2   3
<b>Output:</b> 2
<b>Explanation:</b> The longest consecutive path is [1, 2] or [2, 1].
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b>
        2
       / \
      1   3
<b>Output:</b> 3
<b>Explanation:</b> The longest consecutive path is [1, 2, 3] or [3, 2, 1].
</pre>

<p>&nbsp;</p>

<p><b>Note:</b> All the values of tree nodes are in the range of [-1e7, 1e7].</p>

</div>

## Tags
- Tree (tree)

## Companies
- Google - 3 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary

Find the length of Longest Consecutive Path in Binary Tree. The path can be both increasing or decreasing i,e [1,2,3,4] and [4,3,2,1] are both considered valid. The path can be child-Parent-child not necessarily parent-child.

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

We can easily see that in a tree there is exactly one  unique path one from one node to another. So, the number of paths possible will be equal to number of pairs of nodes $${{N}\choose{2}}$$, where $$N$$ is the number of nodes.

Brute force solution of this problem is to find the path between every two nodes and check whether it is increasing or decreasing. In this way we can find maximum length increasing or decreasing sequence.

**Complexity Analysis**

* Time complexity : $$O(n^3)$$. Total possible number of paths are $$n^2$$ and checking every path whether it is increasing or decreasing will take $$O(n)$$ for one path.

* Space complexity : $$O(n^3)$$. $$n^2$$ paths each with $$O(n)$$ nodes.

---

#### Approach #2 Single traversal [Accepted]

**Algorithm**

This solution is very simple. With every node, we associate two values/variables named $$inr$$ and $$dcr$$, where $$incr$$ represents the length of the longest incrementing branch below the current node including itself, and $$dcr$$ represents the length of the longest decrementing branch below the current node including itself.

We make use of a recursive function `longestPath(node)` which returns an array of the form $$[inr, dcr]$$ for the calling node. We start off by assigning both $$inr$$ and $$dcr$$ as 1 for the current node. This is because the node itself always forms a consecutive increasing as well as decreasing path of length 1.

Then, we obtain the length of the longest path for the left child of the current node using `longestPath[root.left]`. Now, if the left child is just smaller than the current node, it forms a decreasing sequence with the current node. Thus, the $$dcr$$ value for the current node is stored as  the left child's $$dcr$$ value + 1. But, if the left child is just larger than the current node, it forms an incrementing sequence with the current node. Thus, we update the current node's $$inr$$ value as $$left\_child(inr) + 1$$.

Then, we do the same process with the right child as well. But, for obtaining the $$inr$$ and $$dcr$$ value for the current node, we need to consider the maximum value out of the two values obtained from the left and the right child for both $$inr$$ and $$dcr$$, since we need to consider the longest sequence possible.

Further, after we've obtained the final updated values of $$inr$$ and $$dcr$$ for a node, we update the length of the longest consecutive path found so far as $$maxval =  \text{max}(inr + dcr - 1)$$.

The following animation will make the process clear:

<!-- ![Longest_Sequence_Tree](../Figures/549_Binary_Tree_2.gif) -->

!?!../Documents/549_Binary_Tree_Longest_Sequence_ii.json:1000,563!?!


<iframe src="https://leetcode.com/playground/5zMV57sH/shared" frameBorder="0" name="5zMV57sH" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. The whole tree is traversed only once.
* Space complexity : $$O(n)$$. The recursion goes upto a depth of $$n$$ in the worst case.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Neat Java Solution Single pass O(n)
- Author: vinod23
- Creation Date: Sun Apr 09 2017 11:09:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 02:28:59 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    int maxval = 0;
    public int longestConsecutive(TreeNode root) {
        longestPath(root);
        return maxval;
    }
    public int[] longestPath(TreeNode root) {
        if (root == null)
            return new int[] {0,0};
        int inr = 1, dcr = 1;
        if (root.left != null) {
            int[] l = longestPath(root.left);
            if (root.val == root.left.val + 1)
                dcr = l[1] + 1;
            else if (root.val == root.left.val - 1)
                inr = l[0] + 1;
        }
        if (root.right != null) {
            int[] r = longestPath(root.right);
            if (root.val == root.right.val + 1)
                dcr = Math.max(dcr, r[1] + 1);
            else if (root.val == root.right.val - 1)
                inr = Math.max(inr, r[0] + 1);
        }
        maxval = Math.max(maxval, dcr + inr - 1);
        return new int[] {inr, dcr};
    }
}
</p>


### Java solution, Binary Tree Post Order Traversal
- Author: shawngao
- Creation Date: Sun Apr 09 2017 11:03:51 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 00:53:33 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    int max = 0;
    
    class Result {
        TreeNode node;
        int inc;
        int des;
    }
    
    public int longestConsecutive(TreeNode root) {
        traverse(root);
        return max;
    }
    
    private Result traverse(TreeNode node) {
        if (node == null) return null;
        
        Result left = traverse(node.left);
        Result right = traverse(node.right);
        
        Result curr = new Result();
        curr.node = node;
        curr.inc = 1;
        curr.des = 1;
        
        if (left != null) {
            if (node.val - left.node.val == 1) {
                curr.inc = Math.max(curr.inc, left.inc + 1);
            }
            else if (node.val - left.node.val == -1) {
                curr.des = Math.max(curr.des, left.des + 1);
            }
        }
        
        if (right != null) {
            if (node.val - right.node.val == 1) {
                curr.inc = Math.max(curr.inc, right.inc + 1);
            }
            else if (node.val - right.node.val == -1) {
                curr.des = Math.max(curr.des, right.des + 1);
            }
        }
        
        max = Math.max(max, curr.inc + curr.des - 1);
        
        return curr;
    }
}
```
</p>


### DFS C++, Python solutions
- Author: zqfan
- Creation Date: Sun Apr 09 2017 11:24:59 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 11:30:52 GMT+0800 (Singapore Standard Time)

<p>
c++ solution:
```
class Solution {
public:
    int longestConsecutive(TreeNode* root) {
        int longest = 0;
        dfs(root, root, longest);
        return longest;
    }

    pair<int, int> dfs(TreeNode * node, TreeNode * parent, int & longest) {
        if ( NULL == node ) {
            return make_pair(0, 0);
        }
        auto left = dfs(node->left, node, longest);
        auto right = dfs(node->right, node, longest);
        longest = max(longest, left.first + right.second + 1);
        longest = max(longest, left.second + right.first + 1);
        int inc = 0, dec = 0;
        if ( node->val == parent->val + 1 ) {
            inc = max(left.first, right.first) + 1;
        }
        if ( node->val == parent->val - 1 ) {
            dec = max(left.second, right.second) + 1;
        }
        return make_pair(inc, dec);
    }
};
```
python solution
```
class Solution(object):
    def longestConsecutive(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        def dfs(node, parent):
            if not node:
                return 0, 0
            li, ld = dfs(node.left, node)
            ri, rd = dfs(node.right, node)
            l[0] = max(l[0], li + rd + 1, ld + ri + 1)
            if node.val == parent.val + 1:
                return max(li, ri) + 1, 0
            if node.val == parent.val - 1:
                return 0, max(ld, rd) + 1
            return 0, 0
        l = [0]
        dfs(root, root)
        return l[0]
```
</p>


