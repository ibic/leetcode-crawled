---
title: "Number of Subarrays with Bounded Maximum"
weight: 732
#id: "number-of-subarrays-with-bounded-maximum"
---
## Description
<div class="description">
<p>We are given an array <code>A</code> of positive integers, and two positive integers <code>L</code> and <code>R</code> (<code>L &lt;= R</code>).</p>

<p>Return the number of (contiguous, non-empty) subarrays such that the value of the maximum array element in that subarray is at least <code>L</code> and at most <code>R</code>.</p>

<pre>
<strong>Example :</strong>
<strong>Input:</strong> 
A = [2, 1, 4, 3]
L = 2
R = 3
<strong>Output:</strong> 3
<strong>Explanation:</strong> There are three subarrays that meet the requirements: [2], [2, 1], [3].
</pre>

<p><strong>Note:</strong></p>

<ul>
	<li>L, R&nbsp; and <code>A[i]</code> will be an integer in the range <code>[0, 10^9]</code>.</li>
	<li>The length of <code>A</code> will be in the range of <code>[1, 50000]</code>.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Adobe - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Counting [Accepted]

**Intuition**

We can make the following logical steps to arrive at the solution:

As we only care about whether each element is less than, between, or greater than the interval `[L, R]`, let's pretend each element is either `0` if it is less than `L`; `1` if it is between `L` and `R`; or `2` if it is greater than `R`.

Then, we want the number of subarrays with no `2` and at least one `1`.  The `2`s split the array into groups of arrays with only `0`s and `1`s.  For example, if our array is `[0, 0, 1, 2, 2, 1, 0, 2, 0]`, then the answer is just the answer for `[0, 0, 1]` plus the answer for `[1, 0]` plus the answer for `[0]`.

For each such group (arrays of only `0`s or `1`s), to count the number of subarrays that have at least one `1`, let's count all the subarrays in the group, minus the subarrays that only have zeroes.  

For example, `[0, 0, 1]` has 6 subarrays, 3 of which are zero-only, which adds 3 to the answer; `[1, 0]` has 3 subarrays, 1 of which is zero-only, which adds 2 to the answer; and `[0]` has 1 subarray, 1 of which is zero-only, which adds 0 to the answer.  The final answer to the previous original example of `A = [0, 0, 1, 2, 2, 1, 0, 2, 0]` is `3 + 2 + 0 = 5`.

**Algorithm**

Say `count(B)` is the number of subarrays that have all elements less than or equal to `B`.  From the above reasoning, the answer is `count(R) - count(L-1)`.

How do we compute `count(B)`?  We remember `cur`, the number of contiguous, previous elements less than or equal to `B`.  When we see a new element less than or equal to `B`, the number of valid subarrays ending at this position is `cur + 1`.  When we see a new element greater than `B`, the number of valid subarrays ending at this position is 0.

<iframe src="https://leetcode.com/playground/ATULJnQR/shared" frameBorder="0" width="100%" height="293" name="ATULJnQR"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity: $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python , standard DP solution with explanation 
- Author: charleszhou327
- Creation Date: Mon Mar 05 2018 01:49:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 13:29:20 GMT+0800 (Singapore Standard Time)

<p>
Suppose __dp[i]__ denotes the max number of valid sub-array ending with __A[i]__. We use following example to illustrate the idea:

_A = [2, 1, 4, 2, 3], L = 2, R = 3_

1. if A[i] < L
        
For example, i = 1. We can only append A[i] to a valid sub-array ending with A[i-1] to create new sub-array. So we have __dp[i] = dp[i-1] (for i > 0)__
        
2. if A[i] > R:
        
For example, i = 2. No valid sub-array ending with A[i] exist. So we have __dp[i] = 0__. 
We also record the position of the invalid number 4 here as __prev__. 
        
3. if L <= A[i] <= R
        
For example, i = 4. In this case any sub-array starts after the previous invalid number to A[i] (A[prev+1..i], A[prev+2..i]) is a new valid sub-array. So __dp[i] += i - prev__
        
Finally the sum of the dp array is the solution. Meanwhile, notice  dp[i] only relies on dp[i-1] (and also __prev__), we can reduce the space complexity to O(1)


```python
class Solution(object):
    def numSubarrayBoundedMax(self, A, L, R):
        """
        :type A: List[int]
        :type L: int
        :type R: int
        :rtype: int
        """
        res, dp = 0, 0
        prev = -1
        for i in range(len(A)):
            if A[i] < L and i > 0:
                res += dp
            if A[i] > R:
                dp = 0
                prev = i
            if L <= A[i] <= R:
                dp = i - prev
                res += dp
        return res
```
</p>


### C++, O(n), <10 lines
- Author: JohnsonTau
- Creation Date: Sun Mar 04 2018 12:46:14 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 04:15:35 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    int numSubarrayBoundedMax(vector<int>& A, int L, int R) {
        int result=0, left=-1, right=-1;
        for (int i=0; i<A.size(); i++) {
            if (A[i]>R) left=i;
            if (A[i]>=L) right=i;
            result+=right-left;
        }
        return result;
    }
};
```
</p>


### Short Java O(n) Solution
- Author: kay_deep
- Creation Date: Sun Mar 04 2018 12:11:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 06:21:12 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int numSubarrayBoundedMax(int[] A, int L, int R) {
        int j=0,count=0,res=0;
        
        for(int i=0;i<A.length;i++){
            if(A[i]>=L && A[i]<=R){
                res+=i-j+1;count=i-j+1;
            }
            else if(A[i]<L){
                res+=count;
            }
            else{
                j=i+1;
                count=0;
            }
        }
        return res;
    }
}
```
</p>


