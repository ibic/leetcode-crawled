---
title: "Deepest Leaves Sum"
weight: 1127
#id: "deepest-leaves-sum"
---
## Description
<div class="description">
Given a binary tree, return the sum of values of its deepest leaves.
<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/07/31/1483_ex1.png" style="width: 273px; height: 265px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [1,2,3,4,5,null,6,7,null,null,null,null,8]
<strong>Output:</strong> 15
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is between&nbsp;<code>1</code>&nbsp;and&nbsp;<code>10^4</code>.</li>
	<li>The value of nodes is between&nbsp;<code>1</code>&nbsp;and&nbsp;<code>100</code>.</li>
</ul>
</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Overview

**DFS vs BFS**

There are two ways to traverse the tree: DFS _depth first search_ and 
BFS _breadth first search_. Here is a small summary 

![diff](../Figures/1302/traversals.png)

> Let's use this problem to discuss the difference between _iterative BFS traversal with
the queue_ and _iterative DFS preorder traversal with the stack_.

Both are starting from the root and going down, both are using additional structures, 
what's the difference? 

Here is how it looks at the big scale: BFS traverses level by level,
and DFS first goes to the leaves.

![diff](../Figures/1302/dfs_bfs2.png)

Now let's go down to the implementation. The idea is similar:

- Push root into queue (BFS) or stack (DFS).

- At each step pop out one node, and push its children into stack/queue. 

For BFS: pop out from the _left_, first push the _left_ child, and then 
the _right_ one. 

For DFS: pop out from the _right_, first push the _right_ child, and then 
the _left_ one. 

![diff](../Figures/1302/implem2.png)

<br />
<br />


---
#### Approach 1: Iterative DFS Preorder Traversal.

**Intuition**

Here we implement standard iterative preorder traversal with the stack:

- Push root into stack.

- While stack is not empty:

    - Pop out a node from stack and update the current number.
    
    - If the node is a leaf
    
        - Update the deepest leaves sum `deepest_sum`.
    
    - Push right and left child nodes into stack.
    
- Return `deepest_sum`.  


**Implementation**

Note, that 
[Javadocs recommends to use ArrayDeque, and not Stack as a stack implementation](https://docs.oracle.com/javase/8/docs/api/java/util/ArrayDeque.html).

<iframe src="https://leetcode.com/playground/gfP73Lj4/shared" frameBorder="0" width="100%" height="500" name="gfP73Lj4"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since one has to visit each node. 
    
* Space complexity: up to $$\mathcal{O}(H)$$ to keep the stack,
where $$H$$ is a tree height.  
<br />
<br />


---
#### Approach 2: Iterative BFS Traversal.

**Intuition**

Here we implement standard iterative preorder traversal with the stack:

- Add root into queue.

- While queue is not empty:

    - Pop out a node from queue and update the current number.
    
    - If the node is a leaf
    
        - Update the deepest leaves sum `deepest_sum`.
    
    - Add first _left_ and then _right_ child node into queue.
    
- Return `deepest_sum`.  


**Implementation**

<iframe src="https://leetcode.com/playground/RwC8YX8A/shared" frameBorder="0" width="100%" height="500" name="RwC8YX8A"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since one has to visit each node. 
    
* Space complexity: up to $$\mathcal{O}(N)$$ to keep the queue.
Let's use the last level to estimate the queue size. 
This level could contain up to $$N/2$$ tree nodes in the case of 
[complete binary tree](https://leetcode.com/problems/count-complete-tree-nodes/).   
<br />
<br />


---
#### Approach 3: Optimized Iterative BFS Traversal.

**Intuition**

The code in Approach 2 is not the optimal one. 
It's done this way to simplify DFS vs BFS comparison but now let's 
move further. Since we traverse level by level, it's enough 
just to check if this level is the last one. If it's the case,
return the sum of all nodes values.  

!?!../Documents/1302_LIS.json:1000,416!?!

**Implementation**

<iframe src="https://leetcode.com/playground/AytWXWu6/shared" frameBorder="0" width="100%" height="500" name="AytWXWu6"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since one has to visit each node. 
    
* Space complexity: up to $$\mathcal{O}(N)$$ to keep the queues.
Let's use the last level to estimate the queue size. 
This level could contain up to $$N/2$$ tree nodes in the case of 
[complete binary tree](https://leetcode.com/problems/count-complete-tree-nodes/). 
<br />
<br />


___

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Level Traversal
- Author: lee215
- Creation Date: Sun Dec 29 2019 00:10:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 05 2020 00:47:16 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
`pre` are nodes in the previous level.
`q` are node in the current level.

When current level are empty,
the previous level are the deepest leaves.
<br>

## **Complexity**
Time `O(N)`
Space `O(N)`
<br>

**Python:**
```python
    def deepestLeavesSum(self, root):
        q = [root]
        while q:
            pre, q = q, [child for p in q for child in [p.left, p.right] if child]
        return sum(node.val for node in pre)
```

**Java**
```java
    public int deepestLeavesSum(TreeNode root) {
        int res = 0, i;
        LinkedList<TreeNode> q = new LinkedList<TreeNode>();
        q.add(root);
        while (!q.isEmpty()) {
            for (i = q.size() - 1, res = 0; i >= 0; --i) {
                TreeNode node = q.poll();
                res += node.val;
                if (node.right != null) q.add(node.right);
                if (node.left  != null) q.add(node.left);
            }
        }
        return res;
    }
```

**C++**
```cpp
    int deepestLeavesSum(TreeNode* root) {
        int res = 0, i;
        queue<TreeNode*> q;
        q.push(root);
        while (q.size()) {
            for (i = q.size() - 1, res = 0; i >= 0; --i) {
                TreeNode* node = q.front(); q.pop();
                res += node->val;
                if (node->right) q.push(node->right);
                if (node->left)  q.push(node->left);
            }
        }
        return res;
    }
```
</p>


### [Java] BFS - Clean code - O(N)
- Author: hiepit
- Creation Date: Sun Dec 29 2019 00:12:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 29 2019 01:32:05 GMT+0800 (Singapore Standard Time)

<p>
**Complexity**
- Time: O(N), N is the number of nodes in the tree
- Space: O(N/2), size of the largest level

**Java**
```java
class Solution {
    public int deepestLeavesSum(TreeNode root) {
        if (root == null) return 0;
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        int sum = 0;
        while (!q.isEmpty()) {
            int size = q.size();
            sum = 0; // Reset for calculating the sum of elements of the next level
            while (size-- > 0) {
                TreeNode top = q.poll();
                sum += top.val;
                if (top.left != null) q.offer(top.left);
                if (top.right != null) q.offer(top.right);
            }
        }
        return sum;
    }
}
```
</p>


### [Java] Recursive, faster than 100.00%
- Author: tusharj227
- Creation Date: Sun Apr 05 2020 22:52:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 05 2020 22:54:16 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    
   private int maxLevel = 0;
    private int sum = 0;
    
    public int deepestLeavesSum(TreeNode root) {
        if(root == null) return 0;
        calculateSumAtLevel(root,0);
        return sum;
        
    }
    
    private void calculateSumAtLevel(TreeNode root,int level){
        
       if(root == null) return;
        if(level > maxLevel){
            sum = 0;
            maxLevel = level;
        }
        if(level == maxLevel){
            sum = sum + root.val;
        }
        calculateSumAtLevel(root.left,level+1);
        calculateSumAtLevel(root.right,level+1);
    }
}

```
</p>


