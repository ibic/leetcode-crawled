---
title: "Number of Nodes in the Sub-Tree With the Same Label"
weight: 1391
#id: "number-of-nodes-in-the-sub-tree-with-the-same-label"
---
## Description
<div class="description">
<p>Given a tree (i.e. a connected, undirected graph that has no cycles) consisting of <code>n</code> nodes numbered from <code>0</code> to <code>n - 1</code> and exactly <code>n - 1</code> <code>edges</code>. The <strong>root</strong> of the tree is the node <code>0</code>, and each node of the tree has <strong>a label</strong> which is a lower-case character given in the string <code>labels</code> (i.e. The node with the number <code>i</code> has the label <code>labels[i]</code>).</p>

<p>The <code>edges</code> array is given on the form <code>edges[i] = [a<sub>i</sub>, b<sub>i</sub>]</code>, which means there is an edge between nodes <code>a<sub>i</sub></code> and <code>b<sub>i</sub></code> in the tree.</p>

<p>Return <em>an array of size <code>n</code></em> where <code>ans[i]</code> is the number of nodes in the subtree of the&nbsp;<code>i<sup>th</sup></code>&nbsp;node which have the same label as node <code>i</code>.</p>

<p>A&nbsp;subtree&nbsp;of a tree&nbsp;<code>T</code> is the tree consisting of a node in <code>T</code> and all of its descendant&nbsp;nodes.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/01/q3e1.jpg" style="width: 441px; height: 321px;" />
<pre>
<strong>Input:</strong> n = 7, edges = [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]], labels = &quot;abaedcd&quot;
<strong>Output:</strong> [2,1,1,1,1,1,1]
<strong>Explanation:</strong> Node 0 has label &#39;a&#39; and its sub-tree has node 2 with label &#39;a&#39; as well, thus the answer is 2. Notice that any node is part of its sub-tree.
Node 1 has a label &#39;b&#39;. The sub-tree of node 1 contains nodes 1,4 and 5, as nodes 4 and 5 have different labels than node 1, the answer is just 1 (the node itself).
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/01/q3e2.jpg" style="width: 381px; height: 321px;" />
<pre>
<strong>Input:</strong> n = 4, edges = [[0,1],[1,2],[0,3]], labels = &quot;bbbb&quot;
<strong>Output:</strong> [4,2,1,1]
<strong>Explanation:</strong> The sub-tree of node 2 contains only node 2, so the answer is 1.
The sub-tree of node 3 contains only node 3, so the answer is 1.
The sub-tree of node 1 contains nodes 1 and 2, both have label &#39;b&#39;, thus the answer is 2.
The sub-tree of node 0 contains nodes 0, 1, 2 and 3, all with label &#39;b&#39;, thus the answer is 4.
</pre>

<p><strong>Example 3:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/01/q3e3.jpg" style="width: 381px; height: 321px;" />
<pre>
<strong>Input:</strong> n = 5, edges = [[0,1],[0,2],[1,3],[0,4]], labels = &quot;aabab&quot;
<strong>Output:</strong> [3,2,1,1,1]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 6, edges = [[0,1],[0,2],[1,3],[3,4],[4,5]], labels = &quot;cbabaa&quot;
<strong>Output:</strong> [1,2,1,1,2,1]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 7, edges = [[0,1],[1,2],[2,3],[3,4],[4,5],[5,6]], labels = &quot;aaabaaa&quot;
<strong>Output:</strong> [6,5,4,1,3,2,1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
	<li><code>edges.length == n - 1</code></li>
	<li><code>edges[i].length == 2</code></li>
	<li><code>0 &lt;= a<sub>i</sub>,&nbsp;b<sub>i</sub> &lt; n</code></li>
	<li><code>a<sub>i</sub> !=&nbsp;b<sub>i</sub></code></li>
	<li><code>labels.length == n</code></li>
	<li><code>labels</code> is consisting of only of lower-case English letters.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Samsung - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### How can this be a tree?
- Author: Shounak_ds
- Creation Date: Sun Jul 19 2020 12:02:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 19 2020 12:04:38 GMT+0800 (Singapore Standard Time)

<p>
[[0,2],[0,3],[1,2]]
</p>


### [Java/Python 3] DFS w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Jul 19 2020 12:01:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 19 2020 22:49:29 GMT+0800 (Singapore Standard Time)

<p>
The problem does NOT specify it is binary tree or not, so I treat it as a genearl tree.
1. Build the graph of the tree;
2. Use a HashSet `seen` to avoid duplicate visits, `ans` is the result array, and `cnt[i]` is the total number of corresponding char;
3. Use DFS to traverse the tree and count each label and sum it in the return array; At the same time update the result: `ans` array.

```java
    public int[] countSubTrees(int n, int[][] edges, String labels) {
        Map<Integer, List<Integer>> g = new HashMap<>();
        for (int[] e : edges) {
            g.computeIfAbsent(e[0], l -> new ArrayList<>()).add(e[1]);
            g.computeIfAbsent(e[1], l -> new ArrayList<>()).add(e[0]);
        }
        int[] ans = new int[n];
        Set<Integer> seen = new HashSet<>();
        dfs(g, 0, labels, ans, seen);
        return ans;
    }
    private int[] dfs(Map<Integer, List<Integer>> g, int node, String labels, int[] ans, Set<Integer> seen) {
        int[] cnt = new int[26];
        if (seen.add(node)) {
            char c = labels.charAt(node);
            for (int child : g.getOrDefault(node, Collections.emptyList())) {
                int[] sub = dfs(g, child, labels, ans, seen);
                for (int i = 0; i < 26; ++i) {
                    cnt[i] += sub[i];
                }
            }
            ++cnt[c - \'a\'];
            ans[node] = cnt[c - \'a\'];
        }
        return cnt;
    }
```
```python
    def countSubTrees(self, n: int, edges: List[List[int]], labels: str) -> List[int]:
        
        def dfs(node: int):
            cnt = Counter()
            if node not in seen:
                cnt[labels[node]] += 1 
                seen.add(node)
                for child in g.get(node, []):
                    cnt += dfs(child)
                ans[node] = cnt[labels[node]]
            return cnt
        
        g, ans, seen = defaultdict(list), [0] * n, set()
        for a, b in edges:
            g[a] += [b]
            g[b] += [a]
        dfs(0)
        return ans
```

----
Since it is a tree, we can avoid duplicate visiting by checking if  its neighbor is its parent; Therefore, the following code is simplified by removal of set `seen`. - credit to **@jaazzz**.
```java
    public int[] countSubTrees(int n, int[][] edges, String labels) {
        Map<Integer, List<Integer>> g = new HashMap<>();
        for (int[] e : edges) {
            g.computeIfAbsent(e[0], l -> new ArrayList<>()).add(e[1]);
            g.computeIfAbsent(e[1], l -> new ArrayList<>()).add(e[0]);
        }
        int[] ans = new int[n];
        dfs(g, 0, -1, labels, ans);
        return ans;
    }
    private int[] dfs(Map<Integer, List<Integer>> g, int node, int parent, String labels, int[] ans) {
        int[] cnt = new int[26];
        char c = labels.charAt(node);
        for (int child : g.getOrDefault(node, Collections.emptyList())) {
            if (child != parent) {
                int[] sub = dfs(g, child, node, labels, ans);
                for (int i = 0; i < 26; ++i) {
                    cnt[i] += sub[i];
                }
            }
        }
        ++cnt[c - \'a\'];
        ans[node] = cnt[c - \'a\'];
        return cnt;
    }
```
```python
    def countSubTrees(self, n: int, edges: List[List[int]], labels: str) -> List[int]:
        
        def dfs(node: int, parent: int):
            cnt = Counter(labels[node])
            for child in g.get(node, []):
                if child != parent:
                    cnt += dfs(child, node)
            ans[node] = cnt[labels[node]]
            return cnt
        
        g, ans = defaultdict(list), [0] * n
        for a, b in edges:
            g[a] += [b]
            g[b] += [a]
        dfs(0, -1)
        return ans    
```
**Analysis:**

Time & space: O(n).

</p>


### Clean Python 3, dfs with counter
- Author: lenchen1112
- Creation Date: Sun Jul 19 2020 12:02:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 19 2020 12:05:30 GMT+0800 (Singapore Standard Time)

<p>
Perform dfs from node 0 and return counter of each child.
Time: `O(N)`
Space: `O(N)`
```
import collections
class Solution:
    def countSubTrees(self, n: int, edges: List[List[int]], labels: str) -> List[int]:
        def dfs(node: int, parent: int):
            counter = collections.Counter()
            for child in tree[node]:
                if child == parent: continue
                counter += dfs(child, node)
            counter[labels[node]] += 1
            result[node] = counter[labels[node]]
            return counter

        tree = collections.defaultdict(list)
        for a, b in edges:
            tree[a].append(b)
            tree[b].append(a)
        result = [0] * n
        dfs(0, None)
        return result
```
</p>


