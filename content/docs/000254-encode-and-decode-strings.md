---
title: "Encode and Decode Strings"
weight: 254
#id: "encode-and-decode-strings"
---
## Description
<div class="description">
<p>Design an algorithm to encode <b>a list of strings</b> to <b>a string</b>. The encoded string is then sent over the network and is decoded back to the original list of strings.</p>

<p>Machine 1 (sender) has the function:</p>

<pre>
string encode(vector&lt;string&gt; strs) {
  // ... your code
  return encoded_string;
}</pre>
Machine 2 (receiver) has the function:

<pre>
vector&lt;string&gt; decode(string s) {
  //... your code
  return strs;
}
</pre>

<p>So Machine 1 does:</p>

<pre>
string encoded_string = encode(strs);
</pre>

<p>and Machine 2 does:</p>

<pre>
vector&lt;string&gt; strs2 = decode(encoded_string);
</pre>

<p><code>strs2</code> in Machine 2 should be the same as <code>strs</code> in Machine 1.</p>

<p>Implement the <code>encode</code> and <code>decode</code> methods.</p>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ul>
	<li>The string may contain any possible characters out of 256 valid ascii characters. Your algorithm should be generalized enough to work on any possible characters.</li>
	<li>Do not use class member/global/static variables to store states. Your encode and decode algorithms should be stateless.</li>
	<li>Do not rely on any library method such as <code>eval</code> or serialize methods. You should implement your own encode/decode algorithm.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Google - 2 (taggedByAdmin: true)
- Square - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Non-ASCII Delimiter

**Intuition**

Naive solution here is to join strings using delimiters.

> What to use as a delimiter? Each string may contain 
any possible characters out of 256 valid ascii characters.

Seems like one has to use non-ASCII unichar character, 
for example `unichr(257)` in Python and
`Character.toString((char)257)` in Java (it's character `ā`). 

![fig](../Figures/271/delimiter.png)

Here it's convenient to use two different non-ASCII characters,
to distinguish between situations of "empty array" and 
of "array of empty strings". 

**Implementation**

Use `split` in Java with a second argument `-1` to 
make it work as `split` in Python.

<iframe src="https://leetcode.com/playground/VNZA4dA2/shared" frameBorder="0" width="100%" height="463" name="VNZA4dA2"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ both for encode and decode,
where N is a number of strings in the input array.

* Space complexity : $$\mathcal{O}(1)$$ for encode to keep the output,
since the output is one string.
$$\mathcal{O}(N)$$ for decode keep the output,
since the output is an array of strings.
<br /> 
<br />


---
#### Approach 2: Chunked Transfer Encoding

Pay attention to this approach because last year Google likes
to ask that sort of low-level optimisation. 
[Serialize and deserialize BST problem](https://leetcode.com/articles/serialize-and-deserialize-bst/)
is a similar example. 

This approach is based on the 
[encoding used in HTTP v1.1](https://en.wikipedia.org/wiki/Chunked_transfer_encoding).
It doesn't depend on the set of input characters, and hence
is more versatile and effective than Approach 1.

> Data stream is divided into chunks.
Each chunk is preceded by its size in bytes.

**Encoding Algorithm**

![fig](../Figures/271/encodin.png)

- Iterate over the array of chunks, i.e. strings. 

    - For each chunk compute its length, and convert that length into
    4-bytes string.
    
    - Append to encoded string :
     
        - 4-bytes string with information about chunk size in bytes.
        
        - Chunk itself.
        
- Return encoded string.

**Decoding Algorithm**

![fig](../Figures/271/decodin.png)

- Iterate over the encoded string with a pointer `i` initiated as 0.
While `i < n`:

    - Read 4 bytes `s[i: i + 4]`. It's chunk size in bytes. 
    Convert this 4-bytes string to integer `length`.
    
    - Move the pointer by 4 bytes `i += 4`.
    
    - Append to the decoded array string `s[i: i + length]`.
    
    - Move the pointer by `length` bytes `i += length`.
    
- Return decoded array of strings.

**Implementation**

<iframe src="https://leetcode.com/playground/p2MyhHK7/shared" frameBorder="0" width="100%" height="500" name="p2MyhHK7"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ both for encode and decode,
where N is a number of strings in the input array.

* Space complexity : $$\mathcal{O}(1)$$ for encode to keep the output,
since the output is one string.
$$\mathcal{O}(N)$$ for decode keep the output,
since the output is an array of strings.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### AC Java Solution
- Author: qianzhige
- Creation Date: Sat Aug 29 2015 23:13:53 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 10:11:11 GMT+0800 (Singapore Standard Time)

<p>

        
    public class Codec {
		// Encodes a list of strings to a single string.
        public String encode(List<String> strs) {
            StringBuilder sb = new StringBuilder();
            for(String s : strs) {
                sb.append(s.length()).append(\'/\').append(s);
            }
            return sb.toString();
        }
    
        // Decodes a single string to a list of strings.
        public List<String> decode(String s) {
            List<String> ret = new ArrayList<String>();
            int i = 0;
            while(i < s.length()) {
                int slash = s.indexOf(\'/\', i);
                int size = Integer.valueOf(s.substring(i, slash));
                i = slash + size + 1;
                ret.add(s.substring(slash + 1, i));
            }
            return ret;
        }
    }
</p>


### Java with "escaping"
- Author: StefanPochmann
- Creation Date: Fri Sep 11 2015 23:24:38 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 07:54:33 GMT+0800 (Singapore Standard Time)

<p>
Double any hashes inside the strings, then use standalone hashes (surrounded by spaces) to mark string endings. For example:

    {"abc", "def"}    =>  "abc # def # "
    {'abc', '#def'}   =>  "abc # ##def # "
    {'abc##', 'def'}  =>  "abc#### # def # "

For decoding, just do the reverse: First split at standalone hashes, then undo the doubling in each string.

    public String encode(List<String> strs) {
        StringBuffer out = new StringBuffer();
        for (String s : strs)
            out.append(s.replace("#", "##")).append(" # ");
        return out.toString();
    }

    public List<String> decode(String s) {
        List strs = new ArrayList();
        String[] array = s.split(" # ", -1);
        for (int i=0; i<array.length-1; ++i)
            strs.add(array[i].replace("##", "#"));
        return strs;
    }

Or with streaming:

    public String encode(List<String> strs) {
        return strs.stream()
                   .map(s -> s.replace("#", "##") + " # ")
                   .collect(Collectors.joining());
    }

    public List<String> decode(String s) {
        List strs = Stream.of(s.split(" # ", -1))
                          .map(t -> t.replace("##", "#"))
                          .collect(Collectors.toList());
        strs.remove(strs.size() - 1);
        return strs;
    }
</p>


### 1+7 lines Python (length prefixes)
- Author: StefanPochmann
- Creation Date: Sun Sep 13 2015 21:33:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 07:27:26 GMT+0800 (Singapore Standard Time)

<p>
    class Codec:
    
        def encode(self, strs):
            return ''.join('%d:' % len(s) + s for s in strs)
    
        def decode(self, s):
            strs = []
            i = 0
            while i < len(s):
                j = s.find(':', i)
                i = j + 1 + int(s[i:j])
                strs.append(s[j+1:i])
            return strs
</p>


