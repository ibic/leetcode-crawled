---
title: "Minimum Subsequence in Non-Increasing Order"
weight: 1301
#id: "minimum-subsequence-in-non-increasing-order"
---
## Description
<div class="description">
<p>Given the array <code>nums</code>, obtain a subsequence of the array whose sum of elements is <strong>strictly greater</strong> than the sum of the non&nbsp;included elements in such subsequence.&nbsp;</p>

<p>If there are multiple solutions, return the subsequence with <strong>minimum size</strong> and if there still exist multiple solutions, return the subsequence with the <strong>maximum total sum</strong> of all its elements. A subsequence of an array can be obtained by erasing some (possibly zero) elements from the array.&nbsp;</p>

<p>Note that the solution with the given constraints is guaranteed to be&nbsp;<strong>unique</strong>. Also return the answer sorted in <strong>non-increasing</strong> order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [4,3,10,9,8]
<strong>Output:</strong> [10,9] 
<strong>Explanation:</strong> The subsequences [10,9] and [10,8] are minimal such that the sum of their elements is strictly greater than the sum of elements not included, however, the subsequence [10,9] has the maximum total sum of its elements.&nbsp;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [4,4,7,6,7]
<strong>Output:</strong> [7,7,6] 
<strong>Explanation:</strong> The subsequence [7,7] has the sum of its elements equal to 14 which is not strictly greater than the sum of elements not included (14 = 4 + 4 + 6). Therefore, the subsequence [7,6,7] is the minimal satisfying the conditions. Note the subsequence has to returned in non-decreasing order.  
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [6]
<strong>Output:</strong> [6]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 500</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 100</code></li>
</ul>
</div>

## Tags
- Greedy (greedy)
- Sort (sort)

## Companies
- mercari - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java Priority Queue
- Author: votrubac
- Creation Date: Sun Apr 05 2020 12:05:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 05 2020 15:01:03 GMT+0800 (Singapore Standard Time)

<p>
**Intuition:** pick the largest number from the array and add it to subsequence. Repeat till the subsequence sum `sub_sum` is greater than the half of the total sum `half_sum`. Priority queue is the natural choice to pull largest numbers.

**C++**
```cpp
vector<int> minSubsequence(vector<int>& nums) {
    vector<int> res;
    auto sub_sum = 0, half_sum = accumulate(begin(nums), end(nums), 0) / 2;
    priority_queue<int> pq(begin(nums), end(nums));
    while (sub_sum <= half_sum) {
        res.push_back(pq.top());
        sub_sum += res.back();
        pq.pop();
    }
    return res;
}
```

**Java**
```java
public List<Integer> minSubsequence(int[] nums) {
    List<Integer> res = new ArrayList<>();
    var pq = new PriorityQueue<Integer>(Collections.reverseOrder());
    int sub_sum = 0, total_sum = 0;
    for (var n : nums) {
        pq.offer(n);
        total_sum += n;
    }
    while (sub_sum <= total_sum / 2) {
        res.add(pq.peek());
        sub_sum += pq.poll();
    }    
    return res;
}
```
</p>


### why this is worded as subsequence?
- Author: ijaz20
- Creation Date: Tue Apr 07 2020 03:33:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 07 2020 18:25:30 GMT+0800 (Singapore Standard Time)

<p>
Subsquence means relative position of array shouldn\'t be spoiled right?

Or atleast add this example
Input:
[6,4,4,7,7]
Otput: 
[7,7,6]
</p>


### [Python3] Easy Solution with sort
- Author: localhostghost
- Creation Date: Sun Apr 05 2020 12:08:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 05 2020 12:43:49 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def minSubsequence(self, nums: List[int]) -> List[int]:
        nums.sort()
        res = []
        while sum(res) <= sum(nums):
            res.append(nums.pop())
        return res    
```

##### Without summing every iteration
```
class Solution:
    def minSubsequence(self, nums: List[int]) -> List[int]:
        nums.sort()
        res = []
        numSum = sum(nums)
        resSum = 0 
        while resSum <= numSum:
            v = nums.pop()
            res.append(v)
            resSum += v
            numSum -= v 
        return res   
```
</p>


