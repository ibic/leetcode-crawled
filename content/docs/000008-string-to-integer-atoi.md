---
title: "String to Integer (atoi)"
weight: 8
#id: "string-to-integer-atoi"
---
## Description
<div class="description">
<p>Implement <code><span>atoi</span></code> which&nbsp;converts a string to an integer.</p>

<p>The function first discards as many whitespace characters as necessary until the first non-whitespace character is found. Then, starting from this character takes an optional initial plus or minus sign followed by as many numerical digits as possible, and interprets them as a numerical value.</p>

<p>The string can contain additional characters after those that form the integral number, which are ignored and have no effect on the behavior of this function.</p>

<p>If the first sequence of non-whitespace characters in str is not a valid integral number, or if no such sequence exists because either str is empty or it contains only whitespace characters, no conversion is performed.</p>

<p>If no valid conversion could be performed, a zero value is returned.</p>

<p><strong>Note:</strong></p>

<ul>
	<li>Only the space character <code>&#39; &#39;</code> is considered a whitespace character.</li>
	<li>Assume we are dealing with an environment that could only store integers within the 32-bit signed integer range: [&minus;2<sup>31</sup>,&nbsp; 2<sup>31&nbsp;</sup>&minus; 1]. If the numerical value is out of the range of representable values, INT_MAX (2<sup>31&nbsp;</sup>&minus; 1) or INT_MIN (&minus;2<sup>31</sup>) is returned.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> str = &quot;42&quot;
<strong>Output:</strong> 42
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> str = &quot;   -42&quot;
<strong>Output:</strong> -42
<strong>Explanation:</strong> The first non-whitespace character is &#39;-&#39;, which is the minus sign. Then take as many numerical digits as possible, which gets 42.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> str = &quot;4193 with words&quot;
<strong>Output:</strong> 4193
<strong>Explanation:</strong> Conversion stops at digit &#39;3&#39; as the next character is not a numerical digit.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> str = &quot;words and 987&quot;
<strong>Output:</strong> 0
<strong>Explanation:</strong> The first non-whitespace character is &#39;w&#39;, which is not a numerical digit or a +/- sign. Therefore no valid conversion could be performed.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> str = &quot;-91283472332&quot;
<strong>Output:</strong> -2147483648
<strong>Explanation:</strong> The number &quot;-91283472332&quot; is out of the range of a 32-bit signed integer. Thefore INT_MIN (&minus;2<sup>31</sup>) is returned.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= s.length &lt;= 200</code></li>
	<li><code>s</code> consists of English letters (lower-case and upper-case), digits, <code>&#39; &#39;</code>, <code>&#39;+&#39;</code>, <code>&#39;-&#39;</code> and <code>&#39;.&#39;</code>.</li>
</ul>

</div>

## Tags
- Math (math)
- String (string)

## Companies
- Facebook - 20 (taggedByAdmin: false)
- Amazon - 10 (taggedByAdmin: true)
- Microsoft - 7 (taggedByAdmin: true)
- Apple - 7 (taggedByAdmin: false)
- Goldman Sachs - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Bloomberg - 6 (taggedByAdmin: true)
- Google - 5 (taggedByAdmin: false)
- VMware - 3 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- LinkedIn - 6 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: true)
- Zillow - 3 (taggedByAdmin: false)
- Tencent - 2 (taggedByAdmin: false)
- Redfin - 2 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---

#### Approach 1: Scan from left to right

**Intuition**

The problem looks simple at first glance. However, there are a few edge conditions that we must handle. The basic idea is to scan the string from left to right and build the result for numerical characters. In this process, we must carefully handle the integer overflow and underflow conditions.


**Algorithm**

We have to build numerical value by repeatedly shifting the result to left by one digit and adding the next number at the unit place. Since numerical value is a decimal number represented as base 10 in the number system, each digit can be expressed as multiples of powers of 10.

Example -
"142" can be represented as
$$1 * (10^2) + 4 * (10^1) + 2 * (10^0)$$

1 is at the hundredth place and left-shifted twice, 4 at tens place and shifted once, and so on.

![img](../Figures/8/atoi.png)

This can be formulated as :

```java
 result * 10 + (str[i] - '0')
```

However, this could lead to integer overflow or underflow conditions. Since integer must be within the 32-bit signed integer range.

_Handling overflow and underflow conditions_

If the integer is positive, for 32 bit signed integer, $$\text{INT\_MAX}$$ is $$2147483647 (2^{31}-1)$$
To avoid integer overflow, we must ensure that it doesn't exceed this value. This condition needs to be handled when the result is greater than or equal to $$\frac{\text{INT\_MAX}}{10}$$ (214748364)

- Case 1). If $$\text{result} = \frac{\text{INT\_MAX}}{10}$$, it would result in integer overflow if next integer character is greater than 7. (7 in this case is last digit of $$\text{INT\_MAX} (2147483647)$$).
We can use $$\text{INT\_MAX} \% 10$$ to generically represent the last digit.

- Case 2). If $$\text{result} > \frac{\text{INT\_MAX}}{10}$$, we are sure that adding next number would result in integer overflow.

This holds for negative integer as well. In the 32-bit integer, $$\text{INT\_MIN}$$ value is $$-2147483648 (-2^{31})$$. As the last digit is greater than 7 ($$\text{INT\_MAX} \% 10$$), integer underflow can also be handled using the above approach.

We must return $$\text{INT\_MAX}$$ in case of integer overflow and $$\text{INT\_MIN}$$ in case of integer underflow.

Also, it must be noted that in some languages like Python,  an integer value is not restricted by the number of bits. Handling of overflow and underflow won't be required in such cases. We could simply check if the value of an integer is out of specified range [$${−2}^{31}$$,  $${2}^{31} − 1$$]

**Steps**

The algorithm can be summarised as follows
1. Discard all the whitespaces at the beginning of the string.
2. There could be an optional sign of a numerical value $$+/-$$. It should be noted that the integer is positive by default if there is no sign present and there could be at most one sign character.
3. Build the result using the above algorithm until there exists a non-whitespace character that is a number ($$0$$ to $$9$$).
Simultaneously, check for overflow/underflow conditions at each step.

**Note:**
If there exists any non-integer character at step 3, we return 0 by default since it is not a valid integer.
Also, we have to just discard all the characters after the first numerical value.

<iframe src="https://leetcode.com/playground/3YL6vv8J/shared" frameBorder="0" width="100%" height="500" name="3YL6vv8J"></iframe>

**Complexity Analysis**

* Time Complexity: $$\mathcal{O}(N)$$. Here, N is the length of string str. We perform only one iteration over string str.
* Space Complexity: $$\mathcal{O}(1)$$  We use constant extra space for storing sign of the result.

## Accepted Submission (python3)
```python3
from collections import deque

class Solution:
    def myAtoi(self, str: str) -> int:
        INT_MAX = 2 ** 31 -1
        INT_MIN = -2 ** 31
        stripped = str.strip()
        lens = len(stripped)
        if lens == 0:
            return 0
        positive = True
        start = 0
        if stripped[0] == '-':
            positive = False
        if stripped[0] == '+' or stripped[0]== '-':
            start = 1
        num = 0
        stack = deque()
        i = start
        while i < lens and stripped[i] >= '0' and stripped[i] <= '9':
            stack.appendleft(ord(stripped[i]) - ord('0'))
            i += 1
        for i in range(0, len(stack)):
            num += stack[i] * 10 ** i
            if positive and num >= INT_MAX:
                return INT_MAX
            elif not positive and  -num <= INT_MIN:
                return INT_MIN
        return num if positive else -num
```

## Top Discussions
### Such a shitty problem
- Author: shuangling
- Creation Date: Mon Jan 25 2016 11:10:49 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 18:00:16 GMT+0800 (Singapore Standard Time)

<p>
The description is not clear not all unless you click on the hint. What's the point of testing all the "+-1" or "-+1" without any input spec nor any situation where input is obtained.
</p>


### My simple solution
- Author: yuruofeifei
- Creation Date: Sat Aug 09 2014 15:02:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 10:17:17 GMT+0800 (Singapore Standard Time)

<p>
I think we only need to handle four cases: 

 1. discards all leading whitespaces
 2. sign of the number
 3. overflow
 4. invalid input

Is there any better solution? Thanks for pointing out!

    int atoi(const char *str) {
        int sign = 1, base = 0, i = 0;
        while (str[i] == ' ') { i++; }
        if (str[i] == '-' || str[i] == '+') {
            sign = 1 - 2 * (str[i++] == '-'); 
        }
        while (str[i] >= '0' && str[i] <= '9') {
            if (base >  INT_MAX / 10 || (base == INT_MAX / 10 && str[i] - '0' > 7)) {
                if (sign == 1) return INT_MAX;
                else return INT_MIN;
            }
            base  = 10 * base + (str[i++] - '0');
        }
        return base * sign;
    }
</p>


### Java Solution with 4 steps explanations
- Author: lestrois
- Creation Date: Wed Apr 22 2015 04:17:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 07:17:15 GMT+0800 (Singapore Standard Time)

<p>
    public int myAtoi(String str) {
        int index = 0, sign = 1, total = 0;
        //1. Empty string
        if(str.length() == 0) return 0;

        //2. Remove Spaces
        while(str.charAt(index) == ' ' && index < str.length())
            index ++;

        //3. Handle signs
        if(str.charAt(index) == '+' || str.charAt(index) == '-'){
            sign = str.charAt(index) == '+' ? 1 : -1;
            index ++;
        }
        
        //4. Convert number and avoid overflow
        while(index < str.length()){
            int digit = str.charAt(index) - '0';
            if(digit < 0 || digit > 9) break;

            //check if total will be overflow after 10 times and add digit
            if(Integer.MAX_VALUE/10 < total || Integer.MAX_VALUE/10 == total && Integer.MAX_VALUE %10 < digit)
                return sign == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;

            total = 10 * total + digit;
            index ++;
        }
        return total * sign;
    }
</p>


