---
title: "Capacity To Ship Packages Within D Days"
weight: 965
#id: "capacity-to-ship-packages-within-d-days"
---
## Description
<div class="description">
<p>A conveyor belt has packages that must be shipped from one port to another within <code>D</code> days.</p>

<p>The <code>i</code>-th package on the conveyor belt has a weight of <code>weights[i]</code>.&nbsp; Each day, we load the ship with packages on the conveyor belt (in the order given by <code>weights</code>). We may not load more weight than the maximum weight capacity of the ship.</p>

<p>Return the least weight capacity of the ship that will result in all the packages on the conveyor belt being shipped within <code>D</code> days.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>weights = <span id="example-input-1-1">[1,2,3,4,5,6,7,8,9,10]</span>, D = <span id="example-input-1-2">5</span>
<strong>Output: </strong><span id="example-output-1">15</span>
<strong>Explanation: </strong>
A ship capacity of 15 is the minimum to ship all the packages in 5 days like this:
1st day: 1, 2, 3, 4, 5
2nd day: 6, 7
3rd day: 8
4th day: 9
5th day: 10

Note that the cargo must be shipped in the order given, so using a ship of capacity 14 and splitting the packages into parts like (2, 3, 4, 5), (1, 6, 7), (8), (9), (10) is not allowed. 
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>weights = <span id="example-input-2-1">[3,2,2,4,1,4]</span>, D = <span id="example-input-2-2">3</span>
<strong>Output: </strong><span id="example-output-2">6</span>
<strong>Explanation: </strong>
A ship capacity of 6 is the minimum to ship all the packages in 3 days like this:
1st day: 3, 2
2nd day: 2, 4
3rd day: 1, 4
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>weights = <span id="example-input-3-1">[1,2,3,1,1]</span>, D = 4
<strong>Output: </strong><span id="example-output-3">3</span>
<strong>Explanation: </strong>
1st day: 1
2nd day: 2
3rd day: 3
4th day: 1, 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= D &lt;= weights.length &lt;= 50000</code></li>
	<li><code>1 &lt;= weights[i] &lt;= 500</code></li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Amazon - 6 (taggedByAdmin: false)
- Google - 7 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Binary Search
- Author: lee215
- Creation Date: Sun Mar 17 2019 12:01:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 27 2020 00:50:54 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
Given the number of bags,
return the minimum capacity of each bag,
so that we can put items one by one into all bags.

We binary search the final result.
The `left` bound is `max(A)`,
The `right` bound is `sum(A)`.
<br>

# More Good Binary Search Problems
Here are some similar binary search problems.
Also find more explanations.
Good luck and have fun.

- 1482. [Minimum Number of Days to Make m Bouquets](https://leetcode.com/problems/minimum-number-of-days-to-make-m-bouquets/discuss/686316/javacpython-binary-search/578488)
- 1283. [Find the Smallest Divisor Given a Threshold](https://leetcode.com/problems/find-the-smallest-divisor-given-a-threshold/discuss/446376/javacpython-bianry-search/401806)
- 1231. [Divide Chocolate](https://leetcode.com/problems/divide-chocolate/discuss/408503/Python-Binary-Search)
- 1011. [Capacity To Ship Packages In N Days](https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/discuss/256729/javacpython-binary-search/351188?page=3)
- 875. [Koko Eating Bananas](https://leetcode.com/problems/koko-eating-bananas/discuss/152324/C++JavaPython-Binary-Search)
- 774. [Minimize Max Distance to Gas Station](https://leetcode.com/problems/minimize-max-distance-to-gas-station/discuss/113633/Easy-and-Concise-Solution-using-Binary-Search-C++JavaPython)
- 410. [Split Array Largest Sum](https://leetcode.com/problems/split-array-largest-sum/)
<br>

**Java**
```java
    public int shipWithinDays(int[] weights, int D) {
        int left = 0, right = 0;
        for (int w: weights) {
            left = Math.max(left, w);
            right += w\uFF1B
        }
        while (left < right) {
            int mid = (left + right) / 2, need = 1, cur = 0;
            for (int w: weights) {
                if (cur + w > mid) {
                    need += 1;
                    cur = 0;
                }
                cur += w;
            }
            if (need > D) left = mid + 1;
            else right = mid;
        }
        return left;
    }
```

**C++**
```cpp
    int shipWithinDays(vector<int>& weights, int D) {
        int left = 0, right = 25000000;
        for (int w: weights)
            left = max(left, w);
        while (left < right) {
            int mid = (left + right) / 2, need = 1, cur = 0;
            for (int i = 0; i < weights.size() && need <= D; cur += weights[i++])
                if (cur + weights[i] > mid)
                    cur = 0, need++;
            if (need > D) left = mid + 1;
            else right = mid;
        }
        return left;
    }
```

**Python:**
```py
    def shipWithinDays(self, weights, D):
        left, right = max(weights), sum(weights)
        while left < right:
            mid, need, cur = (left + right) / 2, 1, 0
            for w in weights:
                if cur + w > mid:
                    need += 1
                    cur = 0
                cur += w
            if need > D: left = mid + 1
            else: right = mid
        return left
```

</p>


### Python Binary search with detailed explanation
- Author: li-_-il
- Creation Date: Sun Mar 17 2019 12:09:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 17 2019 12:09:52 GMT+0800 (Singapore Standard Time)

<p>
The intuition for this problem, stems from the fact that

a) Without considering the limiting limiting days D, if we are to solve, the answer is simply  max(a)
b) If max(a) is the answer, we can still spend O(n) time and greedily find out how many partitions it will result in.

[1,2,3,4,5,6,7,8,9,10], D = 5

For this example, assuming the answer is max(a) = 10, disregarding D,
we can get the following number of days:
[1,2,3,4] [5] [6] [7] [8] [9] [10]

So by minimizing the cacpacity shipped on a day, we end up with 7 days, by greedily chosing the packages for a day limited by 10.

To get to exactly D days and minimize the max sum of any partition, we do binary search in the sum space which is bounded by  [max(a),  sum(a)]

Binary Search Update:
One thing to note in Binary Search for this problem, is even if we end up finding a weight, that gets us to D partitions, we still want to continue the space on the minimum side, because, there could be a better minimum sum that still passes <= D paritions.

In the code, this is achieved by:

```
if res <= d:
     hi = mid
```

With this check in place, when we narrow down on one element, lo == hi, we will end up with exactly the minimum sum that leads to <= D partitions.

```
def shipWithinDays(self, a: List[int], d: int) -> int:
        lo, hi = max(a), sum(a)   
        while lo < hi:
            mid = (lo + hi) // 2
            tot, res = 0, 1
            for wt in a:
                if tot + wt > mid:
                    res += 1
                    tot = wt
                else:
                    tot += wt
            if res <= d:
                hi = mid
            else:
                lo = mid+1
        return lo
```
</p>


### Binary classification Python. Detailed explanation [Turtle Code]
- Author: JiangHe0124
- Creation Date: Mon Jul 29 2019 23:17:12 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 02 2019 02:25:51 GMT+0800 (Singapore Standard Time)

<p>
\'\'\'

	def shipWithinDays(self, weights, D):
			low, high = max(weights), sum(weights)
			while low < high:
				# guess the capacity of ship
				mid = (low+high)//2

				cur_cap = 0 # loaded capacity of current ship
				num_ship = 1 # number of ship needed

				#----simulating loading the weight to ship one by one----#
				for w in weights:
					cur_cap += w
					if cur_cap > mid: # current ship meets its capacity
						cur_cap = w
						num_ship += 1
				#---------------simulation ends--------------------------#

				# we need too many ships, so we need to increase capacity to reduce num of ships needed
				if num_ship > D:
					low = mid+1
				# we are able to ship with good num of ships, but we still need to find the optimal max capacity
				else:
					high = mid

			return low
\'\'\'
</p>


