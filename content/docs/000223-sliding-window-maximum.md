---
title: "Sliding Window Maximum"
weight: 223
#id: "sliding-window-maximum"
---
## Description
<div class="description">
<p>You are given an array of integers&nbsp;<code>nums</code>, there is a sliding window of size <code>k</code> which is moving from the very left of the array to the very right. You can only see the <code>k</code> numbers in the window. Each time the sliding window moves right by one position.</p>

<p>Return <em>the max sliding window</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,3,-1,-3,5,3,6,7], k = 3
<strong>Output:</strong> [3,3,5,5,6,7]
<strong>Explanation:</strong> 
Window position                Max
---------------               -----
[1  3  -1] -3  5  3  6  7       <strong>3</strong>
 1 [3  -1  -3] 5  3  6  7       <strong>3</strong>
 1  3 [-1  -3  5] 3  6  7      <strong> 5</strong>
 1  3  -1 [-3  5  3] 6  7       <strong>5</strong>
 1  3  -1  -3 [5  3  6] 7       <strong>6</strong>
 1  3  -1  -3  5 [3  6  7]      <strong>7</strong>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1], k = 1
<strong>Output:</strong> [1]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,-1], k = 1
<strong>Output:</strong> [1,-1]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [9,11], k = 2
<strong>Output:</strong> [11]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> nums = [4,-2], k = 2
<strong>Output:</strong> [4]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10<sup>5</sup></code></li>
	<li><code>-10<sup>4</sup> &lt;= nums[i] &lt;= 10<sup>4</sup></code></li>
	<li><code>1 &lt;= k &lt;= nums.length</code></li>
</ul>

</div>

## Tags
- Heap (heap)
- Sliding Window (sliding-window)

## Companies
- Amazon - 21 (taggedByAdmin: true)
- Databricks - 6 (taggedByAdmin: false)
- Dropbox - 6 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: true)
- Uber - 4 (taggedByAdmin: false)
- Citadel - 3 (taggedByAdmin: false)
- Roblox - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- Microsoft - 7 (taggedByAdmin: false)
- Twitter - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Lyft - 3 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Nutanix - 2 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- Zenefits - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Use a Hammer (Bruteforce)

**Intuition**

The straightforward solution is to iterate over all sliding windows 
and find a maximum for each window. There are `N - k + 1` sliding windows 
and there are `k` elements in each window, that results in 
a quite bad time complexity $$\mathcal{O}(N k)$$.

As you can imagine, this straightforward solution would result in **TLE** (Time Limit Exceed) exception.

It is correct though, one could start with this solution during the interview and improve it later on.

**Implementation**

<iframe src="https://leetcode.com/playground/Q9GQ29Cp/shared" frameBorder="0" width="100%" height="310" name="Q9GQ29Cp"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N k)$$, 
where `N` is number of elements in the array.
 
* Space complexity : $$\mathcal{O}(N - k + 1)$$ for an output array.
<br />
<br />


---
#### Approach 2: Deque 

**Intuition**

How one could improve the time complexity? The first idea is to 
use a _heap_, since in a maximum heap `heap[0]` is always the largest element.
Though to add an element in a heap of size `k` costs 
$$\log(k)$$, that means $$\mathcal{O}(N \log(k))$$ time complexity
for the solution.

> Could we figure out $$\mathcal{O}(N)$$ solution? 

Let's use a _deque_ (double-ended queue), 
the structure which pops from / pushes to either side with the same $$\mathcal{O}(1)$$
performance.

It's more handy to store in the deque indexes instead of elements since
both are used during an array parsing.

**Algorithm**

The algorithm is quite straigthforward :

- Process the first `k` elements separately to initiate the deque.

- Iterate over the array. At each step :

    - Clean the deque :

        - Keep only the indexes of elements from 
        the current sliding window.
    
        - Remove indexes of all elements smaller than the current one, 
        since they will not be the maximum ones.
        
    - Append the current element to the deque.
    
    - Append `deque[0]` to the output.

- Return the output array.

**Implementation**

<iframe src="https://leetcode.com/playground/MvkK82eW/shared" frameBorder="0" width="100%" height="500" name="MvkK82eW"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$, since each element is processed 
exactly twice - it's index added and then removed from the deque.
 
* Space complexity : $$\mathcal{O}(N)$$, since 
$$\mathcal{O}(N - k + 1)$$ is used for an output array and
$$\mathcal{O}(k)$$ for a deque.
<br />
<br />


---
#### Approach 3: Dynamic programming

**Intuition**

Here is another $$\mathcal{O}(N)$$ solution. The good thing about this
solution is that you don't need any data structures but
`array / list`.

The idea is to split an input array into blocks of `k` elements.
The last block could contain less elements if `n % k != 0`.

![split](../Figures/239/split.png)

The current sliding window with the first element `i` and the last element `j`
could be placed inside one block, or in two different blocks. 

![split](../Figures/239/one_two.png)

The situation `1` is simple. 
Let's use an array `left`, where `left[j]` is a maximum element
from the beginning of the block to index `j`, direction `left->right`.

![split](../Figures/239/inside2.png)

To work with more complex situation `2`, let's introduce array `right`,
where `right[j]` is a maximum element from the end of the block to index `j`,
direction `right->left`. `right` is basically the same as `left`,
but in the other direction.

![split](../Figures/239/right_left2.png)

These two arrays together give all the information about
window elements in both blocks.
Let's consider a sliding window from index `i` to index `j`. 
By definition, element `right[i]` is a maximum element for window elements 
in the leftside block,
and element `left[j]` is a maximum element for window elements
in the rightside block.
Hence the maximum element in the sliding window is 
`max(right[i], left[j])`.

![split](../Figures/239/solution.png)

**Algorithm**

The algorithm is quite straightforward :

- Iterate along the array in the direction `left->right`
and build an array `left`.

- Iterate along the array in the direction `right->left`
and build an array `right`.

- Build an output array as `max(right[i], left[i + k - 1])` for `i`
in range `(0, n - k + 1)`.

**Implementation**

!?!../Documents/239_LIS.json:1000,468!?!

<iframe src="https://leetcode.com/playground/AK3oPFy3/shared" frameBorder="0" width="100%" height="500" name="AK3oPFy3"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$, 
since all we do is `3` passes along the array of length `N`.
 
* Space complexity : $$\mathcal{O}(N)$$ to keep `left` and `right` arrays
of length `N`, and output array of length `N - k + 1`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n) solution using deque with explanation
- Author: flyingpenguin
- Creation Date: Sat Jul 18 2015 09:41:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 09:52:11 GMT+0800 (Singapore Standard Time)

<p>
We scan the array from 0 to n-1, keep "promising" elements in the deque. The algorithm is amortized O(n) as each element is put and polled once.

At each i, we keep "promising" elements, which are potentially max number in window [i-(k-1),i] or any subsequent window. This means

1. If an element in the deque and it is out of i-(k-1), we discard them. We just need to poll from the head, as we are using a deque and elements are ordered as the sequence in the array

2. Now only those elements within [i-(k-1),i]  are in the deque. We then discard elements smaller than a[i] from the tail. This is because if a[x] <a[i] and x<i, then a[x] has no chance to be the "max" in [i-(k-1),i], or any other subsequent window: a[i] would always be a better candidate. 

3. As a result elements in the deque are ordered in both sequence in array and their value. At each step the head of the deque is the max element in [i-(k-1),i]


-----------------------------------
    public int[] maxSlidingWindow(int[] a, int k) {		
    		if (a == null || k <= 0) {
    			return new int[0];
    		}
    		int n = a.length;
    		int[] r = new int[n-k+1];
    		int ri = 0;
    		// store index
    		Deque<Integer> q = new ArrayDeque<>();
    		for (int i = 0; i < a.length; i++) {
    			// remove numbers out of range k
    			while (!q.isEmpty() && q.peek() < i - k + 1) {
    				q.poll();
    			}
    			// remove smaller numbers in k range as they are useless
    			while (!q.isEmpty() && a[q.peekLast()] < a[i]) {
    				q.pollLast();
    			}
    			// q contains index... r contains content
    			q.offer(i);
    			if (i >= k - 1) {
    				r[ri++] = a[q.peek()];
    			}
    		}
    		return r;
    	}
</p>


### O(n) solution in Java with two simple pass in the array
- Author: zahid2
- Creation Date: Wed Oct 07 2015 06:44:16 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 15:34:46 GMT+0800 (Singapore Standard Time)

<p>

For Example: A = [2,1,3,4,6,3,8,9,10,12,56],  w=4

1. partition the array in blocks of size w=4. The last block may have less then w.
2, 1, 3, 4 | 6, 3, 8, 9 | 10, 12, 56|

2. Traverse the list from start to end and calculate max_so_far. Reset max after each block boundary (of w elements).
left_max[] = 2, 2, 3, 4 | 6, 6, 8, 9 | 10, 12, 56

3. Similarly calculate max in future by traversing from end to start.
right_max[] = 4, 4, 4, 4 | 9, 9, 9, 9 | 56, 56, 56

4. now, sliding max at each position i in current window, sliding-max(i) = max{right_max(i), left_max(i+w-1)}
sliding_max = 4, 6, 6, 8, 9, 10, 12, 56

code:
   

     public static int[] slidingWindowMax(final int[] in, final int w) {
        final int[] max_left = new int[in.length];
        final int[] max_right = new int[in.length];
    
        max_left[0] = in[0];
        max_right[in.length - 1] = in[in.length - 1];
    
        for (int i = 1; i < in.length; i++) {
            max_left[i] = (i % w == 0) ? in[i] : Math.max(max_left[i - 1], in[i]);
    
            final int j = in.length - i - 1;
            max_right[j] = (j % w == 0) ? in[j] : Math.max(max_right[j + 1], in[j]);
        }
    
        final int[] sliding_max = new int[in.length - w + 1];
        for (int i = 0, j = 0; i + w <= in.length; i++) {
            sliding_max[j++] = Math.max(max_right[i], max_left[i + w - 1]);
        }
    
        return sliding_max;
    }
</p>


### This is a typical monotonic queue problem
- Author: fentoyal
- Creation Date: Tue Jul 21 2015 05:44:11 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 03 2019 04:34:59 GMT+0800 (Singapore Standard Time)

<p>
Sliding window minimum/maximum = monotonic queue. I smelled the solution just when I read the title.
This is essentially the same idea as others\' deque solution, but this is much more standardized and modulized. If you ever need to use it in your real product, this code is definitely more preferable. 

What does Monoqueue do here:

It has three basic options:

push: push an element into the queue;  O (1) (amortized)

pop: pop an element out of the queue; O(1) (pop = remove, it can\'t report this element)

max: report the max element in queue;O(1)

It takes only O(n) time to process a N-size sliding window minimum/maximum problem.

Note: different from a priority queue (which takes O(nlogk) to solve this problem),  it doesn\'t pop the max element: It pops the first element (in original order) in queue.

    class Monoqueue
    {
        deque<pair<int, int>> m_deque; //pair.first: the actual value, 
                                       //pair.second: how many elements were deleted between it and the one before it.
        public:
            void push(int val)
            {
                int count = 0;
                while(!m_deque.empty() && m_deque.back().first < val)
                {
                    count += m_deque.back().second + 1;
                    m_deque.pop_back();
                }
                m_deque.emplace_back(val, count);
            };
            int max()
            {
                return m_deque.front().first;
            }
            void pop ()
            {
                if (m_deque.front().second > 0)
                {
                    m_deque.front().second --;
                    return;
                }
                m_deque.pop_front();
            }
    };
    struct Solution {
        vector<int> maxSlidingWindow(vector<int>& nums, int k) {
            vector<int> results;
            Monoqueue mq;
            k = min(k, (int)nums.size());
            int i = 0;
            for (;i < k - 1; ++i) //push first k - 1 numbers;
            {
                mq.push(nums[i]);
            }
            for (; i < nums.size(); ++i)
            {
                mq.push(nums[i]);            // push a new element to queue;
                results.push_back(mq.max()); // report the current max in queue;
                mq.pop();                    // pop first element in queue;
            }
            return results;
        }
    };
</p>


