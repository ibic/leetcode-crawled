---
title: "Binary Tree Level Order Traversal"
weight: 102
#id: "binary-tree-level-order-traversal"
---
## Description
<div class="description">
<p>Given a binary tree, return the <i>level order</i> traversal of its nodes' values. (ie, from left to right, level by level).</p>

<p>
For example:<br />
Given binary tree <code>[3,9,20,null,null,15,7]</code>,<br />
<pre>
    3
   / \
  9  20
    /  \
   15   7
</pre>
</p>
<p>
return its level order traversal as:<br />
<pre>
[
  [3],
  [9,20],
  [15,7]
]
</pre>
</p>
</div>

## Tags
- Tree (tree)
- Breadth-first Search (breadth-first-search)

## Companies
- Facebook - 8 (taggedByAdmin: true)
- Microsoft - 8 (taggedByAdmin: true)
- Apple - 8 (taggedByAdmin: true)
- Amazon - 6 (taggedByAdmin: true)
- Bloomberg - 6 (taggedByAdmin: true)
- eBay - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- LinkedIn - 6 (taggedByAdmin: true)
- Cisco - 3 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- Walmart Labs - 4 (taggedByAdmin: false)
- VMware - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### How to traverse the tree

There are two general strategies to traverse a tree:
     
- *Depth First Search* (`DFS`)

    In this strategy, we adopt the `depth` as the priority, so that one
    would start from a root and reach all the way down to certain leaf,
    and then back to root to reach another branch.

    The DFS strategy can further be distinguished as
    `preorder`, `inorder`, and `postorder` depending on the relative order
    among the root node, left node and right node.
    
- *Breadth First Search* (`BFS`)

    We scan through the tree level by level, following the order of height,
    from top to bottom. The nodes on higher level would be visited before
    the ones with lower levels.
    
On the following figure the nodes are numerated in the order you visit them,
please follow ```1-2-3-4-5``` to compare different strategies.

![postorder](../Figures/145_transverse.png)

Here the problem is to implement split-level BFS traversal : `[[1], [2, 3], [4, 5]]`.
<br /> 
<br />


---
#### Approach 1: Recursion

**Algorithm**

The simplest way to solve the problem is to use a recursion. Let's 
first ensure that the tree is not empty, and then call recursively the function 
`helper(node, level)`, which takes the current node and its level as the arguments.

This function does the following :

- The output list here is called `levels`, and hence the current level is
just a length of this list `len(levels)`.
Compare the number of a current level `len(levels)` with a node level `level`.
If you're still on the previous level - add the new one by adding a 
new list into `levels`.

- Append the node value to the last list in `levels`.

- Process recursively child nodes if they are not `None` : 
`helper(node.left / node.right, level + 1)`.

**Implementation**

!?!../Documents/102_LIS.json:1000,509!?!

<iframe src="https://leetcode.com/playground/pRYovDJ4/shared" frameBorder="0" width="100%" height="497" name="pRYovDJ4"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since each node is processed
exactly once.
 
* Space complexity : $$\mathcal{O}(N)$$ to keep the output structure which
contains `N` node values.
<br />
<br />


---
#### Approach 2: Iteration

**Algorithm**

The recursion above could be rewritten in the iteration form.

Let's keep nodes of each tree level in the _queue_ structure,
which typically orders elements in a FIFO (first-in-first-out) manner.
In Java one could use [`LinkedList` implementation of the `Queue` interface](https://docs.oracle.com/javase/7/docs/api/java/util/Queue.html).
In Python using [`Queue` structure](https://docs.python.org/3/library/queue.html)
would be an overkill since it's designed for a safe exchange between multiple threads
and hence requires locking which leads to a performance loose. 
In Python the queue implementation with a fast atomic `append()`
and `popleft()` is [`deque`](https://docs.python.org/3/library/collections.html#collections.deque).

The zero level contains only one node `root`. The algorithm is simple :

- Initiate queue with a `root` and start from the level number `0` :
`level = 0`.

- While queue is not empty :

    * Start the current level by adding an empty list into output structure `levels`.
    
    * Compute how many elements should be on the current level : it's a 
    queue length.
    
    * Pop out all these elements from the queue and add them into the current
    level.
    
    * Push their child nodes into the queue for the next level.
    
    * Go to the next level `level++`.

**Implementation**

<iframe src="https://leetcode.com/playground/YjxL23CU/shared" frameBorder="0" width="100%" height="500" name="YjxL23CU"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since each node is processed
exactly once.
 
* Space complexity : $$\mathcal{O}(N)$$ to keep the output structure which
contains `N` node values.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution with a queue used
- Author: SOY
- Creation Date: Tue Jan 20 2015 21:00:24 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 20:37:12 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public List<List<Integer>> levelOrder(TreeNode root) {
            Queue<TreeNode> queue = new LinkedList<TreeNode>();
            List<List<Integer>> wrapList = new LinkedList<List<Integer>>();
            
            if(root == null) return wrapList;
            
            queue.offer(root);
            while(!queue.isEmpty()){
                int levelNum = queue.size();
                List<Integer> subList = new LinkedList<Integer>();
                for(int i=0; i<levelNum; i++) {
                    if(queue.peek().left != null) queue.offer(queue.peek().left);
                    if(queue.peek().right != null) queue.offer(queue.peek().right);
                    subList.add(queue.poll().val);
                }
                wrapList.add(subList);
            }
            return wrapList;
        }
    }
</p>


### Java Solution using DFS
- Author: gfei2
- Creation Date: Thu Jan 15 2015 06:13:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 08:40:58 GMT+0800 (Singapore Standard Time)

<p>
Nothing special. Just wanna provide a different way from BFS.

    public List<List<Integer>> levelOrder(TreeNode root) {
            List<List<Integer>> res = new ArrayList<List<Integer>>();
            levelHelper(res, root, 0);
            return res;
        }
        
        public void levelHelper(List<List<Integer>> res, TreeNode root, int height) {
            if (root == null) return;
            if (height >= res.size()) {
                res.add(new LinkedList<Integer>());
            }
            res.get(height).add(root.val);
            levelHelper(res, root.left, height+1);
            levelHelper(res, root.right, height+1);
        }
</p>


### One of C++ solutions (preorder)
- Author: nilath
- Creation Date: Sat Nov 08 2014 16:42:36 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 20:34:47 GMT+0800 (Singapore Standard Time)

<p>
    vector<vector<int>> ret;
    
    void buildVector(TreeNode *root, int depth)
    {
        if(root == NULL) return;
        if(ret.size() == depth)
            ret.push_back(vector<int>());
        
        ret[depth].push_back(root->val);
        buildVector(root->left, depth + 1);
        buildVector(root->right, depth + 1);
    }

    vector<vector<int> > levelOrder(TreeNode *root) {
        buildVector(root, 0);
        return ret;
    }
</p>


