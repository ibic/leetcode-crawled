---
title: "N-Queens II"
weight: 52
#id: "n-queens-ii"
---
## Description
<div class="description">
<p>The <em>n</em>-queens puzzle is the problem of placing <em>n</em> queens on an <em>n</em>&times;<em>n</em> chessboard such that no two queens attack each other.</p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/12/8-queens.png" style="width: 258px; height: 276px;" /></p>

<p>Given an integer&nbsp;<em>n</em>, return the number of&nbsp;distinct solutions to the&nbsp;<em>n</em>-queens puzzle.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> 4
<strong>Output:</strong> 2
<strong>Explanation:</strong> There are two distinct solutions to the 4-queens puzzle as shown below.
[
&nbsp;[&quot;.Q..&quot;, &nbsp;// Solution 1
&nbsp; &quot;...Q&quot;,
&nbsp; &quot;Q...&quot;,
&nbsp; &quot;..Q.&quot;],

&nbsp;[&quot;..Q.&quot;, &nbsp;// Solution 2
&nbsp; &quot;Q...&quot;,
&nbsp; &quot;...Q&quot;,
&nbsp; &quot;.Q..&quot;]
]
</pre>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Zenefits - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Intuition

This problem is a classical one and 
it's important to know the solution to feel classy.

The first idea is to use brute-force 
that means to generate all possible ways to put `N` queens on the board,
and then check them to keep only the combinations 
with no queen under attack. 
That means $$\mathcal{O}(N^N)$$ time complexity
and hence we're forced to think further how to optimize.

There are two programming conceptions here which could
help.

> The first one is called _constrained programming_. 

That basically means
to put restrictions after each queen placement. One puts a queen on the 
board and that immediately excludes one column, one row and 
two diagonals for the further queens placement. That propagates 
_constraints_ and helps to reduce the number of combinations to consider.

<img src="../Figures/52/52_pic.png" width="500">

> The second one called _backtracking_. 

Let's imagine that one 
puts several queens on the board so that they don't attack each other. 
But the combination chosen is not the optimal one and there is no place
for the next queen. What to do? _To backtrack_. That means to come back,
to change the position of the previously placed queen and try 
to proceed again. If that would not work either, _backtrack_ again.

<img src="../Figures/52/52_backtracking_.png" width="500">
<br />
<br />


---
#### Approach 1: Backtracking

Before to construct the algorithm, 
let's figure out two tips that could
help.

> There could be the only one queen in a row and the only one queen
in a column.

That means that there is no need to consider all squares on the 
board. One could just iterate over the columns.

> For all "hill" diagonals `row + column = const`, 
and for all "dale" diagonals `row - column = const`.  

That would allow us to mark the diagonals which are already under 
attack and to check if a given square `(row, column)` is under attack.

<img src="../Figures/52/52_diagonals.png" width="500">

Now everything is ready to write down the backtrack function 
`backtrack(row = 0, count = 0)`.

* Start from the first `row = 0`. 
* Iterate over the columns and try to put a queen in each `column`.

    * If square `(row, column)` is not under attack
        
        * Place the queen in `(row, column)` square.
        * Exclude one row, one column and two diagonals from further 
        consideration.
        * If all rows are filled up `row == N`
            * That means that we find out one more solution `count++`.
        * Else
            * Proceed to place further queens `backtrack(row + 1, count)`.
        * Now backtrack : remove the queen from `(row, column)` square.
        
Here is a straightforward implementation of the above algorithm.  

<!--![LIS](../Figures/72/72_tr.gif)-->
!?!../Documents/52_LIS.json:1000,705!?!

<iframe src="https://leetcode.com/playground/f3E6qBPs/shared" frameBorder="0" width="100%" height="500" name="f3E6qBPs"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N!)$$. There is `N` possibilities to put 
the first queen, not more than `N (N - 2)` to put the second one,
not more than `N(N - 2)(N - 4)` for the third one etc. In total that
results in $$\mathcal{O}(N!)$$ time complexity.
* Space complexity : $$\mathcal{O}(N)$$ to keep an information about 
diagonals and rows. 
<br />
<br />


---
#### Approach 2: Backtracking via bitmap

If you're on the interview - use the approach `1`. 

The next algorithm has the same time complexity $$\mathcal{O}(N!)$$
but works the way faster 
because of [bitwise operators usage](https://wiki.python.org/moin/BitwiseOperators). 
Kudos for this algorithm go to [takaken](http://www.ic-net.or.jp/home/takaken/e/queen/).

To facilitate the understanding of the algorithm,
here is the code with step by step explanations.  

<iframe src="https://leetcode.com/playground/4VeHH5bD/shared" frameBorder="0" width="100%" height="500" name="4VeHH5bD"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N!)$$. 
* Space complexity : $$\mathcal{O}(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted Java Solution
- Author: AlexTheGreat
- Creation Date: Sat Dec 13 2014 05:29:41 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 01:45:03 GMT+0800 (Singapore Standard Time)

<p>
    /**
     * don't need to actually place the queen,
     * instead, for each row, try to place without violation on
     * col/ diagonal1/ diagnol2.
     * trick: to detect whether 2 positions sit on the same diagnol:
     * if delta(col, row) equals, same diagnol1;
     * if sum(col, row) equals, same diagnal2.
     */
    private final Set<Integer> occupiedCols = new HashSet<Integer>();
    private final Set<Integer> occupiedDiag1s = new HashSet<Integer>();
    private final Set<Integer> occupiedDiag2s = new HashSet<Integer>();
    public int totalNQueens(int n) {
        return totalNQueensHelper(0, 0, n);
    }
    
    private int totalNQueensHelper(int row, int count, int n) {
        for (int col = 0; col < n; col++) {
            if (occupiedCols.contains(col))
                continue;
            int diag1 = row - col;
            if (occupiedDiag1s.contains(diag1))
                continue;
            int diag2 = row + col;
            if (occupiedDiag2s.contains(diag2))
                continue;
            // we can now place a queen here
            if (row == n-1)
                count++;
            else {
                occupiedCols.add(col);
                occupiedDiag1s.add(diag1);
                occupiedDiag2s.add(diag2);
                count = totalNQueensHelper(row+1, count, n);
                // recover
                occupiedCols.remove(col);
                occupiedDiag1s.remove(diag1);
                occupiedDiag2s.remove(diag2);
            }
        }
        
        return count;
    }
</p>


### Easiest Java Solution (1ms, 98.22%)
- Author: yavinci
- Creation Date: Sun Nov 15 2015 11:30:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 01:19:23 GMT+0800 (Singapore Standard Time)

<p>
This is a classic backtracking problem. 

Start row by row, and loop through columns. At each decision point, skip unsafe positions by using three boolean arrays.

Start going back when we reach row n.

Just FYI, if using HashSet, running time will be at least 3 times slower!

    public class Solution {
        int count = 0;
        public int totalNQueens(int n) {
            boolean[] cols = new boolean[n];     // columns   |
            boolean[] d1 = new boolean[2 * n];   // diagonals \
            boolean[] d2 = new boolean[2 * n];   // diagonals /
            backtracking(0, cols, d1, d2, n);
            return count;
        }
        
        public void backtracking(int row, boolean[] cols, boolean[] d1, boolean []d2, int n) {
            if(row == n) count++;
    
            for(int col = 0; col < n; col++) {
                int id1 = col - row + n;
                int id2 = col + row;
                if(cols[col] || d1[id1] || d2[id2]) continue;
                
                cols[col] = true; d1[id1] = true; d2[id2] = true;
                backtracking(row + 1, cols, d1, d2, n);
                cols[col] = false; d1[id1] = false; d2[id2] = false;
            }
        }
    }
</p>


### Share my Java code (beats 97.83% run times)
- Author: mach7
- Creation Date: Thu Mar 03 2016 06:15:18 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Mar 03 2016 06:15:18 GMT+0800 (Singapore Standard Time)

<p>
    /*
        \u5e38\u89c4n-queens\u89e3\u6cd5, \u6570\u7b54\u6848\u4e2a\u6570.
        \u7528column\u6807\u8bb0\u6b64\u884c\u4e4b\u524d\u7684\u54ea\u4e9bcolumn\u5df2\u7ecf\u653e\u7f6e\u4e86queen. \u68cb\u76d8\u5750\u6807(row, col)\u5bf9\u5e94column\u7684\u7b2ccol\u4f4d(LSB --> MSB, \u4e0b\u540c).
        \u7528diag\u6807\u8bb0\u6b64\u4f4d\u7f6e\u4e4b\u524d\u7684\u54ea\u4e9b\u4e3b\u5bf9\u89d2\u7ebf\u5df2\u7ecf\u653e\u7f6e\u4e86queen. \u68cb\u76d8\u5750\u6807(row, col)\u5bf9\u5e94diag\u7684\u7b2c(n - 1 + row - col)\u4f4d.
        \u7528antiDiag\u6807\u8bb0\u6b64\u4f4d\u7f6e\u4e4b\u524d\u7684\u54ea\u4e9b\u526f\u5bf9\u89d2\u7ebf\u5df2\u7ecf\u653e\u7f6e\u4e86queen. \u68cb\u76d8\u5750\u6807(row, col)\u5bf9\u5e94antiDiag\u7684\u7b2c(row + col)\u4f4d.
    */
    public class Solution {
        int count = 0;
        
        public int totalNQueens(int n) {
            dfs(0, n, 0, 0, 0);
            return count;
        }
        
        private void dfs(int row, int n, int column, int diag, int antiDiag) {
            if (row == n) {
                ++count;
                return;
            }
            for (int i = 0; i < n; ++i) {
                boolean isColSafe = ((1 << i) & column) == 0;
                boolean isDiagSafe = ((1 << (n - 1 + row - i)) & diag) == 0;
                boolean isAntiDiagSafe = ((1 << (row + i)) & antiDiag) == 0;
                if (isColSafe && isDiagSafe && isAntiDiagSafe) {
                    dfs(row + 1, n, (1 << i) | column, (1 << (n - 1 + row - i)) | diag, (1 << (row + i)) | antiDiag);
                }
            }
        }
    }
</p>


